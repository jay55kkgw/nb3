package tw.com.fstop.nnb.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.owasp.esapi.ESAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import fstop.orm.po.ADMBANK;
import fstop.orm.po.SYSPARAMDATA;
import fstop.orm.po.TXNFXRECORD;
import fstop.orm.po.TXNFXSCHPAY;
import fstop.orm.po.TXNFXSCHPAYDATA;
import fstop.orm.po.TXNUSER;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.custom.annotation.ACNSET1;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmBankDao;
import tw.com.fstop.tbb.nnb.dao.AdmHolidayDao;
import tw.com.fstop.tbb.nnb.dao.AdmMsgCodeDao;
import tw.com.fstop.tbb.nnb.dao.Nb3SysOpDao;
import tw.com.fstop.tbb.nnb.dao.SysParamDataDao;
import tw.com.fstop.tbb.nnb.dao.TxnFxRecordDao;
import tw.com.fstop.tbb.nnb.dao.TxnFxSchPayDao;
import tw.com.fstop.tbb.nnb.dao.TxnFxSchPayDataDao;
import tw.com.fstop.tbb.nnb.dao.TxnReqInfoDao;
import tw.com.fstop.tbb.nnb.dao.TxnUserDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.ObjectConvertUtil;
import tw.com.fstop.util.StrUtils;

@Service
public class Fcy_Reservation_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private TxnFxSchPayDao txnfxschpaydao;
	@Autowired
	private TxnFxSchPayDataDao txnfxschpaydatadao;
	@Autowired
	private TxnFxRecordDao txnfxrecorddao;
	@Autowired
	private AdmHolidayDao admholidaydao;
	@Autowired
	private AdmBankDao admbankdao;
	@Autowired
	private TxnReqInfoDao txnreqinfodao;
	@Autowired
	private TxnUserDao txnUserDao;
	@Autowired
	private Nb3SysOpDao nb3sysopdao;
	@Autowired
	private AdmMsgCodeDao admmsgcodedao;
	@Autowired
	private SysParamDataDao sysParamDataDao;
	@Autowired
	I18n i18n;
	/**
	 * (外幣)預約明細查詢
	 * @param cusidn
	 * @return
	 */
	public BaseResult f_reservation_detail(String cusidn,Map<String, String> reqParam)	{		
		
		log.trace("f_reservation_detail_service");
		BaseResult bs = null ;
		try {
		bs =new  BaseResult();
//		 call ws API
			List<TXNFXSCHPAY> TXNFXSCHPAY = txnfxschpaydao.getFxtxstatus(cusidn);
			List<Map<String,Object>> data = new ArrayList<>();
			if(TXNFXSCHPAY!=null) {
				for(TXNFXSCHPAY each:TXNFXSCHPAY) {
					//將po轉成map
					Map<String, Object> eachMap = ObjectConvertUtil.object2Map(each);
								
					//生效日
					String fxfdate = (String) eachMap.get("FXFDATE");
					eachMap.put("FXFDATE",DateUtil.convertDate(2,fxfdate ,"yyyyMMdd", "yyyy/MM/dd"));
					//截止日
					String fxtdate = (String) eachMap.get("FXTDATE");
					eachMap.put("FXTDATE",DateUtil.convertDate(2,fxtdate ,"yyyyMMdd", "yyyy/MM/dd"));
						//如果兩個相同則只顯示一個
						if(fxfdate.equals(fxtdate)) {
							eachMap.put("FXTDATE","");
						}
					//單次預約/循環預約
					String fxtxtype = (String) eachMap.get("FXTXTYPE");
//					if("S".equals(fxtxtype)) {					
//						eachMap.put("FXPERMTDATE", "特定日期");					
//					}else {
//						String fxpermtdate = (String) eachMap.get("FXPERMTDATE");
//						eachMap.put("FXPERMTDATE", "固定每月"+ fxpermtdate +"日");
//					}
					if("S".equals(fxtxtype)) {					
						eachMap.put("FXPERMTDATE", fxtxtype);					
					}else {
						String fxpermtdate = (String) eachMap.get("FXPERMTDATE");
						eachMap.put("FXPERMTDATE", fxpermtdate);
					}
					//下次轉帳日
					String fxschno = (String) eachMap.get("FXSCHNO");
					String fxschtxdate = txnfxschpaydatadao.getFxschtxdate(cusidn,fxschno);
						if(fxschtxdate == null || fxschtxdate == "") {
							//如果是空就是生效日
							eachMap.put("FXNEXTDATE", DateUtil.convertDate(2,fxfdate ,"yyyyMMdd", "yyyy/MM/dd"));
							eachMap.put("FXNEXTDATEchk", fxfdate);
						}else {
							DateFormat sdf = new SimpleDateFormat("yyyyMMdd");
							Date date = new Date();
							date = sdf.parse(fxschtxdate);
							//判斷是否為營業
							//若為假日，將下次轉帳日改為下個營業日
							for(int i=0; i<10; i++) {
								boolean holiday = admholidaydao.isAdmholiday(date);
								if(!holiday) {
									fxschtxdate = DateUtil.format("yyyyMMdd", date);
									break;
								}
								Calendar c = Calendar.getInstance(); 
								c.setTime(date); 
								c.add(Calendar.DATE, 1);
								date = c.getTime();
							}
							
							eachMap.put("FXNEXTDATE", DateUtil.convertDate(2,fxschtxdate,"yyyyMMdd", "yyyy/MM/dd"));									
							eachMap.put("FXNEXTDATEchk", fxschtxdate);									
						}
				//TODO 暫時沿用TW寫法，可能不是這樣做
					//比對bank名稱
					String fxsvbh = (String) eachMap.get("FXSVBH");
					String bank = "";
					//銀行名稱多語系
					String locale = LocaleContextHolder.getLocale().toString();
					if(locale.equals("zh_CN")) {
						bank = admbankdao.getBankChName(fxsvbh);
					}
					else if(locale.equals("en")) {
						bank = admbankdao.getBankEnName(fxsvbh);
					}
					else {
						bank = admbankdao.getBankName(fxsvbh);
					}
					
					if(bank != "") {
						fxsvbh = fxsvbh + "-" + bank;						
						eachMap.put("FXSVBH", fxsvbh);				
					}else {
						eachMap.put("FXSVBH", "");										
					}				
					//轉出金額格式處理
					String FXWDAMT = (String) eachMap.get("FXWDAMT");
					if("".equals(FXWDAMT) || " ".equals(FXWDAMT) || "0.00".equals(FXWDAMT) || "0".equals(FXWDAMT)) {
						eachMap.put("FXWDAMTS","");
					}else {
						eachMap.put("FXWDAMTS",NumericUtil.fmtAmount(FXWDAMT,2));						
					}
					//轉入金額幣別格式處理
					String FXSVAMT = (String) eachMap.get("FXSVAMT");
					if("".equals(FXSVAMT) || " ".equals(FXSVAMT) || "0.00".equals(FXSVAMT) || "0".equals(FXSVAMT)) {
						eachMap.put("FXSVAMTS","");					
					}else {
						eachMap.put("FXSVAMTS",NumericUtil.fmtAmount((String) eachMap.get("FXSVAMT"),2));						
					}
					//交易類別
					String adopid = (String) eachMap.get("ADOPID");
					String idtype = "";
					//交易類別多語系
					if(locale.equals("zh_CN")) {
						idtype = nb3sysopdao.findAdopidCN(adopid);
					}
					else if(locale.equals("en")) {
						idtype = nb3sysopdao.findAdopidEN(adopid);
					}
					else {
						idtype = nb3sysopdao.findAdopid(adopid);
					}
					
					if(idtype != "") {
						idtype = ESAPI.encoder().encodeForHTML(idtype);
						eachMap.put("TXTYPE", idtype);					
					}else {
						eachMap.put("TXTYPE", "");
					}
					//交易機制
					String fxtxcode = (String) eachMap.get("FXTXCODE");
					if("1".equals(fxtxcode)) {
						//電子簽章(i-key)
						eachMap.put("FXTXCODES","LB.Electronic_signature");									
					}else if("2".equals(fxtxcode)){			
						//晶片金融卡
						eachMap.put("FXTXCODES","LB.Financial_debit_card");	
					} else if ("3".equals(fxtxcode)) {
						// OTP
						eachMap.put("FXTXCODES", "LB.X2330");
					} else if ("4".equals(fxtxcode)) {
						// 軟體憑證(隨護神盾)
						eachMap.put("FXTXCODES", "LB.X2301");
					} else if ("5".equals(fxtxcode)) {
						// 快速交易
						eachMap.put("FXTXCODES", "LB.X2302");
					} else if ("6".equals(fxtxcode)) {
						// 小額交易
						eachMap.put("FXTXCODES", "LB.X2303");
					} else if ("7".equals(fxtxcode)) {
						// 裝置推播認證
						eachMap.put("FXTXCODES", "LB.X2501");
					}else {
						//交易密碼(SSL)
						eachMap.put("FXTXCODES","LB.SSL_password");									
					}
					//狀態
					String status = (String) eachMap.get("FXTXSTATUS");
					if("0".equals(status)) {
						//預約成功
						eachMap.put("STATUS",status);
						eachMap.put("FXTXSTATUS","LB.W0284");
					}else{
						//已取消預約
						eachMap.put("STATUS",status);
						eachMap.put("FXTXSTATUS", "LB.X1798");
					}
					
					data.add((Map<String, Object>) eachMap);
				}
					log.trace(ESAPIUtil.vaildLog("data>>{}"+CodeUtil.toJson(data)));
					bs.addData("REC", data);
				
					//筆數
					int COUNT = 0;
					COUNT = data.size();
					log.trace("COUNT>>{}",COUNT);
					bs.addData("COUNT", String.valueOf(COUNT));
			
					//現在時間
					bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
					bs.addData("TODAY", DateUtil.getCurentDateTime("yyyyMMdd"));
					log.trace("bsData>>{}",bs.getData());
		
					//通過
					bs.setResult(Boolean.TRUE);
//					if(COUNT == 0) {
//						bs.setResult(false);
//						bs.setMsgCode("ENRD");
//						getMessageByMsgCode(bs);
//					}
				}	
			
			} catch (Exception e) {
				// TODO Auto-generated catch block
//				e.printStackTrace();
				log.error("",e);
			}
		
			return  bs;
		}	

	/**
	 * (外幣)預約明細查詢確認頁
	 * @param reqParam
	 * @return
	 */
	public BaseResult f_reservation_confirm(Map<String, String> reqParam) {

		log.trace("f_reservation_confirm");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			log.debug(ESAPIUtil.vaildLog("ROWDATA: {}" + reqParam.get("ROWDATA")));
			
			String value = reqParam.get("ROWDATA");
			value = value.substring(1, value.length()-1);           //remove curly brackets
			log.trace(ESAPIUtil.vaildLog("ROWDATA.value: {}" + value));
			String[] keyValuePairs = value.split(",");              //split the string to creat key-value pairs
			log.trace(ESAPIUtil.vaildLog("ROWDATA.keyValuePairs: {}" + CodeUtil.toJson(keyValuePairs)));
			
			Map<String,String> map = new HashMap<>();               
			for(String pair : keyValuePairs)                        //iterate over the pairs
			{
			    String[] entry = pair.split("=");                   //split the pairs to get key and value
			    
			    String mapKey = "";
			    String MapValue = "";
			    log.trace("ROWDATA.entry.length: {}" , entry.length);
			    if(entry.length == 2) {
			    	mapKey = "".equals(entry[0].trim()) ? "" : entry[0].trim();
			    	MapValue = "".equals(entry[1].trim()) ? "" : entry[1].trim();
			    }else {
			    	mapKey = entry[0].trim();
			    }
			    
			    log.trace(ESAPIUtil.vaildLog("ROWDATA.mapKey: {}" + mapKey));
			    log.trace(ESAPIUtil.vaildLog("ROWDATA.MapValue: {}" + MapValue));
			    map.put(mapKey, MapValue);          //add them to the hashmap and trim whitespaces
			}
			//轉出金額格式處理
			String FXWDAMT = (String) map.get("FXWDAMT");
			if("".equals(FXWDAMT) || "0.00".equals(FXWDAMT) || "0".equals(FXWDAMT)) {
				map.put("FXWDAMTS","");
			}else {
				map.put("FXWDAMTS",NumericUtil.fmtAmount(FXWDAMT,2));						
			}
			//轉入金額幣別格式處理
			String FXSVAMT = (String) map.get("FXSVAMT");
			if("".equals(FXSVAMT) || "0.00".equals(FXSVAMT) || "0".equals(FXSVAMT)) {
				map.put("FXSVAMTS","");					
			}else {
				map.put("FXSVAMTS",NumericUtil.fmtAmount((String) map.get("FXSVAMT"),2));						
			}
			log.debug(ESAPIUtil.vaildLog("ROWDATA: {}" + CodeUtil.toJson(map)));
			
			bs.addData("dataSet", map);
			bs.setResult(true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}

		return bs;
	}
	
	/**
	 * (外幣)預約明細查詢結果頁
	 * @param cusidn
	 * @return
	 */
	public BaseResult f_reservation_result(String cusidn,Map<String, String> reqParam)	{		
		
		log.trace("f_reservation_result");
		BaseResult bs = null ;
		try {
		bs =new  BaseResult();
	//	 call ws API		
		String fxschid = reqParam.get("FXSCHID");
		log.trace(ESAPIUtil.vaildLog("fxschid"+fxschid));
		TXNFXSCHPAY TXNFXSCHPAY = txnfxschpaydao.updataFxtxstatus(fxschid);
			if(TXNFXSCHPAY != null) {
				bs.addData("dataSet", reqParam);
				bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				bs.setResult(Boolean.TRUE);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("",e);
		}
	
		return  bs;
	}
	
	/**
	 * F3000-轉出記錄查詢
	 * 
	 * @param cusidn
	 * @return
	 */
	@ACNSET1
	public BaseResult f_transfer_record_query(String cusidn) {
		log.trace("f_transfer_record_query");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// call REST API
			bs = N920_REST(cusidn, TRANSFER_RECORD);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error("f_transfer_record_query", e);
		}
		return bs;
	}

	/**
	 * 5/8 因為拆分，內容需要重新調整 F3000-轉出記錄查詢結果
	 * 
	 * @param cusidn
	 * @return
	 */
	public BaseResult f_transfer_record_query_result(String cusidn, Map<String, String> reqParam) {
		log.trace("f_transfer_record_query_result>>");
		BaseResult bs = null;
		try {
			int systemDefineResendTimes = 2;
			try {
				SYSPARAMDATA sysparam = sysParamDataDao.getAll().get(0);
				systemDefineResendTimes = new Integer(sysparam.getADRESENDTIMES());
			}
			catch(Exception e) {
				log.error("無法取得系統設定重發次數限制, 使用預設值 2 次.", e);
			}
			log.trace(ESAPIUtil.vaildLog("TIMES>>>"+systemDefineResendTimes));
			
			log.trace(ESAPIUtil.vaildLog("reqParam>>{}"+ CodeUtil.toJson(reqParam)));
			bs = new BaseResult();
			String acn = (String) reqParam.get("ACN");
			String stadate = (String) reqParam.get("CMSDATE");
			String enddate = (String) reqParam.get("CMEDATE");
			String uid = cusidn;
			// ---query
			List<TXNFXRECORD> FxList = null;
			TXNUSER UserList = null;// 重送MAIL用
			FxList = txnfxrecorddao.findByDateRange(uid, stadate, enddate, acn);
			List<Map<String,Object>> data = new ArrayList<>();
			List<Map<String,Object>> downloaddata = new ArrayList<>();
			log.trace(ESAPIUtil.vaildLog("FXRECORD>>{}"+ CodeUtil.toJson(FxList)));
//			UserList = txnUserDao.getNotifyById(uid);
			log.trace("UserList>>{}", UserList);
			log.trace(ESAPIUtil.vaildLog("FxList.isEmpty>>{}"+ CodeUtil.toJson(FxList.isEmpty())));
			if(!FxList.isEmpty()) {
				// FX處理
				for (TXNFXRECORD list : FxList) {
					Map<String, Object> row = new HashMap<String, Object>();
					String RequestInfo = null;
					// SET List資料
					row = CodeUtil.objectCovert(Map.class, list);
					row.put("FXEFEED"," " + row.get("FXEFEECCY").toString() + " " + (row.get("FXEFEE").toString().replaceAll("/", " ")));
					row.put("FXOURCHGD", row.get("FXOURCHG").toString()+" ");
					if(row.get("FXTELFEE").toString()!="")
						row.put("FXEFEED",row.get("FXEFEED").toString() + row.get("FXTELFEE").toString());
					row.put("FXEFEEDEX", row.get("FXEFEE").toString().replaceAll("/", "\n"));
					row.put("FXEFEE", row.get("FXEFEE").toString().replaceAll("/", "<BR>"));
					
					row.put("FXWDAMTD",  row.get("FXWDAMT").toString()+" ");
					log.trace(ESAPIUtil.vaildLog("ROWDATA>>{}"+CodeUtil.toJson(row)));
					//若為空放0，避免前端出錯
					row.put("FXREMAIL", (StrUtils.isEmpty((String)row.get("FXREMAIL")) || " ".equals((String)row.get("FXREMAIL"))) ? "0" : (String)row.get("FXREMAIL"));
					
					//取得交易上行資訊->銀行代碼與轉入帳號使用

					// 轉帳日期格式
					row.put("FXTXDATE", DateUtil.convertDate(2, (String) row.get("FXTXDATE"), "yyyyMMdd", "yyyy/MM/dd"));
					// 交易類別->(是否預約+功能名稱)
					Double schidFlag=(Double) row.get("FXSCHID");
					//row.put("FXTRANFUNC", "<spring:message code='LB.Booking'/>");
					/*
					if (schidFlag == 0) {
						row.put("FXTRANFUNCTW", nb3sysopdao.findAdopidTW((String) row.get("ADOPID")));
					    row.put("FXTRANFUNCCN", nb3sysopdao.findAdopidCN((String) row.get("ADOPID")));
					    row.put("FXTRANFUNCEN", nb3sysopdao.findAdopidEN((String) row.get("ADOPID")));
					}
					else
					{
						row.put("FXTRANFUNCTW", "預約"+nb3sysopdao.findAdopidTW((String) row.get("ADOPID")));
					    row.put("FXTRANFUNCCN", "预约"+nb3sysopdao.findAdopidCN((String) row.get("ADOPID")));
					    row.put("FXTRANFUNCEN", "Booking "+nb3sysopdao.findAdopidEN((String) row.get("ADOPID")));
						
					}
					*/
					Locale currentLocale = LocaleContextHolder.getLocale();
					String locale = currentLocale.toString();
					  switch (locale) {
						case "en":
							if (schidFlag == 0)
								row.put("FXTRANFUNC", nb3sysopdao.findAdopidEN((String) row.get("ADOPID")));
							else
								row.put("FXTRANFUNC", "Booking "+nb3sysopdao.findAdopidEN((String) row.get("ADOPID")));	
							break;
						case "zh_TW":
							if (schidFlag == 0)
								row.put("FXTRANFUNC", nb3sysopdao.findAdopidTW((String) row.get("ADOPID")));
							else
								row.put("FXTRANFUNC", "預約"+nb3sysopdao.findAdopidTW((String) row.get("ADOPID")));
							break;
						case "zh_CN":
							if (schidFlag == 0)
								row.put("FXTRANFUNC", nb3sysopdao.findAdopidCN((String) row.get("ADOPID")));
							else
								row.put("FXTRANFUNC", "预约"+nb3sysopdao.findAdopidCN((String) row.get("ADOPID")));
							break;
		                }
					
					
					//row.put("FXTRANFUNC", row.get("FXTRANFUNC") + get_function_NAME((String) row.get("ADOPID")));
					log.trace(ESAPIUtil.vaildLog("FXTRANFUNC>>{}"+ row.get("FXTRANFUNC")));
					//log.trace("FXTRANFUNCTW>>{}", row.get("FXTRANFUNCTW"));
					//log.trace("FXTRANFUNCCN>>{}", row.get("FXTRANFUNCCN"));
					//log.trace("FXTRANFUNCEN>>{}", row.get("FXTRANFUNCEN"));
					// 銀行代碼與轉入帳號
					String dpsvbh = (String) row.get("FXSVBH");
					String bank = admbankdao.getBankName(dpsvbh);
					if(bank != null || !bank.equals("")) {
						dpsvbh = dpsvbh + "-" + bank;
						row.put("FXSVBH", dpsvbh);					
					}else {
						row.put("FXSVBH", "");
					}
					log.trace(ESAPIUtil.vaildLog("FXSVBH>>{}" + row.get("FXSVBH")));
					// 國外費用
					//REMailFLAG
					if(Integer.parseInt((String) row.get("FXREMAIL"))>=systemDefineResendTimes)row.put("FXREMAILFLAG","disabled");
					else row.put("FXREMAILFLAG","");
					//交易單據重送FLAG
					if(row.get("ADOPID").equals("F001") || row.get("ADOPID").equals("F002") || row.get("ADOPID").equals("F003"))
						row.put("FXCERTFLAG","");
					else row.put("FXCERTFLAG","disabled");
					data.add(row);
					//下載不放FXCERT欄位
					row.put("FXCERT","");
					downloaddata.add(row);
				}
				bs.addData("REC", data);
				bs.addData("DownLoadREC",downloaddata);
				bs.addData("CMQTIME", DateUtil.getDate("/") + " " + DateUtil.getTheTime(":"));
				bs.addData("CMPERIOD", stadate+" ~ "+enddate);
				bs.addData("COUNTS",Integer.toString(FxList.size()));
				bs.addData("EMAILTIMES", systemDefineResendTimes);
				if(FxList.size()>0)bs.setResult(true);
				log.trace("f_transfer_record_query_result >>{}", bs.getData());

			}else {
				Map<String, Object> mapdata = new HashMap<String, Object>();
				mapdata.put("FXTXDATE", " ");
				mapdata.put("FXWDAC", " ");
				mapdata.put("FXSVBH", " ");
				mapdata.put("FXSVAC", " ");
				mapdata.put("FXWDCURR", " ");
				mapdata.put("FXWDAMT", " ");
				mapdata.put("FXSVCURR", " ");
				mapdata.put("FXSVAMT", " ");
				mapdata.put("FXEXRATE", " ");
				
				mapdata.put("FXEFEECCY", " ");
				mapdata.put("FXEFEE", " ");
				mapdata.put("FXTELFEE", " ");
				mapdata.put("FXOURCHG", " ");
				mapdata.put("FXTRANFUNC", " ");
				mapdata.put("FXTXMEMO", " ");
				downloaddata.add(mapdata);
				bs.addData("REC", data);
				bs.addData("DownLoadREC",downloaddata);
				bs.addData("CMQTIME", DateUtil.getDate("/") + " " + DateUtil.getTheTime(":"));
				bs.addData("CMPERIOD", stadate+" ~ "+enddate);
				bs.addData("COUNTS", 0);
				bs.setResult(true);
				log.debug("BSDATA >>{}",CodeUtil.toJson(bs));
				log.debug("f_transfer_record_query_result DONE >>");
			}
		} catch (Exception e) {
			log.error("f_transfer_record_query_result", e);
		}
		return bs;
	}
	
	/*
	 * 說明:在AMBANK資料表用銀行ID取得銀行名稱
	 * 
	 */
	public  String get_Bank_NAME(String BankId) {
		ADMBANK Bankname = admbankdao.getbank(BankId);
		Map<String, Object> infodata = CodeUtil.fromJson(CodeUtil.toJson(Bankname), Map.class);
		return (String) infodata.get("ADBANKNAME");
	}

	/*
	 * 說明:在SYSOP資料表用功能代碼取得功能名稱
	 * 
	 */
	public String get_function_NAME(String ADOPID) {
		String functionname = nb3sysopdao.findAdopid(ADOPID);
		return functionname;
	}

	public BaseResult transfer_record_query_mail(String cusidn,Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("reqParam>>{}"+ CodeUtil.toJson(reqParam)));
			bs = new BaseResult();
			String adtxno = reqParam.get("ADTXNO");
			String FXremail = txnfxrecorddao.updataDpremail(cusidn,adtxno);
			log.trace("FXRECORD>>{}", FXremail);
			if(FXremail != "") {
				bs.addData("success", FXremail);
				bs.setResult(true);
				//通過
				log.trace("transfer_record_query_result >>{}", bs.getData());
			}else {
				bs.addData("faild", i18n.getMsg("LB.X1799"));//發送失敗
				bs.setResult(false);
			}
		} catch (Exception e) {
			log.error("transfer_record_query_result", e);
		}
		return bs;
	}
	public BaseResult transfer_record_query_CERT(String adtxno) {
		BaseResult bs=new BaseResult();
		try {
			//拿FXTXMEMO當title
			//FXCERT為網頁內容
			log.trace(ESAPIUtil.vaildLog("adtxno>>{}"+adtxno));
			Map<String,Object> Fxrecordmap = CodeUtil.objectCovert(Map.class, txnfxrecorddao.findByADTXNO(adtxno));
			//交易單據
			bs.addData("FXCERT", Fxrecordmap.get("FXCERT"));
			// 交易類別->(是否預約+功能名稱)
			Double schidFlag=(Double) Fxrecordmap.get("FXSCHID");
			if (schidFlag == 0)
				Fxrecordmap.put("FXTRANFUNC", "");
			else
				Fxrecordmap.put("FXTRANFUNC", "<spring:message code='LB.Booking'/>");
			Fxrecordmap.put("FXTRANFUNC", Fxrecordmap.get("FXTRANFUNC") + get_function_NAME((String) Fxrecordmap.get("ADOPID")));
			log.trace(ESAPIUtil.vaildLog("FXTRANFUNC>>{}"+ Fxrecordmap.get("FXTRANFUNC")));
			bs.addData("FXTRANFUNC", Fxrecordmap.get("FXTRANFUNC"));
			
			if(bs!=null)bs.setResult(Boolean.TRUE);
			log.trace("FXCERT_FXRECORD>>{}",CodeUtil.toJson(bs));
		} catch (Exception e) {
			log.error("transfer_record_query_result", e);
		}
		return bs;
	}

	/**
	 * F3000直接下載
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult f_transfer_record_query_directDownload(String cusidn, Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			bs = f_transfer_record_query_result(cusidn, reqParam);
			if (bs != null && bs.getResult()) {
				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap={}", dataMap);
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("DownLoadREC");
				log.debug("rowListMap={}", rowListMap);
				Map<String, Object> parameterMap = new HashMap<String, Object>();

				parameterMap.put("CMQTIME", dataMap.get("CMQTIME"));
				parameterMap.put("CMPERIOD", dataMap.get("CMPERIOD"));
				parameterMap.put("COUNT", dataMap.get("COUNTS").toString());

				parameterMap.put("downloadFileName", reqParam.get("downloadFileName"));
				parameterMap.put("downloadType", reqParam.get("downloadType"));
				parameterMap.put("templatePath", reqParam.get("templatePath"));
				parameterMap.put("hasMultiRowData", reqParam.get("hasMultiRowData"));

				String downloadType = reqParam.get("downloadType");
				log.trace(ESAPIUtil.vaildLog("downloadType={}"+ downloadType));

				// EXCEL和OLDEXCEL下載
				if ("EXCEL".equals(downloadType) || "OLDEXCEL".equals(downloadType)) {
					parameterMap.put("headerRightEnd", reqParam.get("headerRightEnd"));
					parameterMap.put("headerBottomEnd", reqParam.get("headerBottomEnd"));
					parameterMap.put("rowStartIndex", reqParam.get("rowStartIndex"));
					parameterMap.put("rowRightEnd", reqParam.get("rowRightEnd"));
					parameterMap.put("footerStartIndex", reqParam.get("footerStartIndex"));
					parameterMap.put("footerEndIndex", reqParam.get("footerEndIndex"));
					parameterMap.put("footerRightEnd", reqParam.get("footerRightEnd"));
				}
				// TXT下載
				else {
					parameterMap.put("txtHeaderBottomEnd", reqParam.get("txtHeaderBottomEnd"));
					parameterMap.put("txtHasRowData", reqParam.get("txtHasRowData"));
					parameterMap.put("txtHasFooter", reqParam.get("txtHasFooter"));
				}
				bs.addData("parameterMap", parameterMap);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("f_transfer_record_query_directDownload error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * 預約交易結果查詢結果頁
	 * 
	 * @param cusidn
	 * @param reqParam 前端輸入變數
	 * @return
	 */
	public BaseResult f_reservation_trans_result(String cusidn, Map<String, String> reqParam) {

		log.trace("f_reservation_trans_result>>{}", cusidn);
		BaseResult bs = null;
		bs = new BaseResult();
		try {
			// call ws API
			List<Map<String, Object>> data = new ArrayList<>();
			List<Map<String, String>> countdata = new ArrayList<>();
			Map<String, String> mapdata = new HashMap<String, String>();
			String fgtxstatus = reqParam.get("FGTXSTATUS");
			String cmsdate = reqParam.get("CMSDATE");
			String cmedate = reqParam.get("CMEDATE");
			if ("All".equals(fgtxstatus)) {
				List<TXNFXSCHPAYDATA> TXNFXSCHPAYDATA = txnfxschpaydatadao.getAll(cusidn, reqParam);
				log.trace(ESAPIUtil.vaildLog("AllTXNFXSCHPAYDATA>>>{}"+ TXNFXSCHPAYDATA));
				if (TXNFXSCHPAYDATA != null) {
					for (TXNFXSCHPAYDATA each : TXNFXSCHPAYDATA) {
						// 將po轉成map
						Map<String, Object> eachMap = ObjectConvertUtil.object2Map(each);
						// 將復合主鍵的key由po轉成map
						Map<String, Object> pks = ObjectConvertUtil.object2Map(eachMap.get("pks"));
						// 把複合主鍵的key塞進eachMap
						eachMap.put("FXSCHNO", pks.get("FXSCHNO"));
						eachMap.put("FXSCHTXDATE", pks.get("FXSCHTXDATE"));

						// 日期格式處理
						String FXSCHTXDATE = (String) eachMap.get("FXSCHTXDATE");
						String FXTXDATE = (String) eachMap.get("FXTXDATE");
						FXTXDATE = FXTXDATE.trim();
						eachMap.put("FXSCHTXDATE", DateUtil.convertDate(2, FXSCHTXDATE, "yyyyMMdd", "yyyy/MM/dd"));
						if(FXTXDATE != null && !FXTXDATE.equals("")) {
							eachMap.put("FXTXDATE", DateUtil.convertDate(2, FXTXDATE, "yyyyMMdd", "yyyy/MM/dd"));
						}

						// 金額格式處理
						eachMap.put("FXWDAMT", NumericUtil.fmtAmount((String) eachMap.get("FXWDAMT"), 2));
						eachMap.put("FXEFEE", NumericUtil.fmtAmount((String) eachMap.get("FXEFEE"), 2));
						eachMap.put("FXTELFEE", NumericUtil.fmtAmount((String) eachMap.get("FXTELFEE"), 2));

						String locale = LocaleContextHolder.getLocale().toString();
						
						// 比對bank名稱
						String fxsvbh = (String) eachMap.get("FXSVBH");
						String bank = "";
						if(locale.equals("zh_CN")) {
							bank = admbankdao.getBankChName(fxsvbh);
						}
						else if(locale.equals("en")) {
							bank = admbankdao.getBankEnName(fxsvbh);
						}
						else {
							bank = admbankdao.getBankName(fxsvbh);
						}
						
						if(bank != "") {
							fxsvbh = fxsvbh + bank;
							eachMap.put("FXSVBH", fxsvbh);
						}
						else {
							eachMap.put("FXSVBH", "");
						}
						
						//交易類別
						String adopid = (String) eachMap.get("ADOPID");
						String adopidh = "";
						
						//多語系
						if(adopid != null) {
							if(locale.equals("zh_CN")) {
								adopid = nb3sysopdao.findAdopidCN(adopid);
							}
							else if(locale.equals("en")) {
								adopid = nb3sysopdao.findAdopidEN(adopid);
							}
							else {
								adopid = nb3sysopdao.findAdopid(adopid);
							}
							adopidh = ESAPI.encoder().encodeForHTML(adopid);
						}
						eachMap.put("ADOPID", adopid);
						eachMap.put("ADOPIDH", adopidh);
						
						//國外費用
						String str_OURCHG = (String) eachMap.get("FXOURCHG");
						if(str_OURCHG == null) {
							str_OURCHG = "";
						}
						eachMap.put("str_OURCHG", str_OURCHG);

						// 轉帳結果狀態翻譯
						String status = (String) eachMap.get("FXTXSTATUS");
						if ("0".equals(status)) {
							//成功
							eachMap.put("FXTXSTATUS", "LB.D1099");
//							eachMap.put("FXTXSTATUS_D", "i18n{LB.D1099}");
						} else if(" ".equals(status)) {
							//未執行
							eachMap.put("FXTXSTATUS", "LB.X0048");
//							eachMap.put("FXTXSTATUS_D", "i18n{LB.X0048}");
						} else if ("1".equals(status)) {
							//失敗
							eachMap.put("FXTXSTATUS", "LB.W0056");
//							eachMap.put("FXTXSTATUS_D", "i18n{LB.W0056}");
							// 比對錯誤代碼名稱
							String fxexcode = (String) eachMap.get("FXEXCODE");
							String excode = admmsgcodedao.getCodeMsg(fxexcode);
							if(excode != null && !"".equals(excode)) {
								eachMap.put("CODEMSG", excode);								
							}else {
								eachMap.put("CODEMSG", "");																
							}
						} else {
							//處理中
							eachMap.put("FXTXSTATUS", "LB.W0057");
//							eachMap.put("FXTXSTATUS_D", "i18n{LB.W0057}");
						}
						data.add((Map<String, Object>) eachMap);
					}
					log.trace(ESAPIUtil.vaildLog("data>>{}"+ data));
					bs.addData("REC", data);

					// 筆數
					int COUNT = 0;
					COUNT = data.size();
					log.trace("COUNT>>{}", COUNT);
					bs.addData("COUNT", String.valueOf(COUNT));

					// 處理空值顯示
					if (TXNFXSCHPAYDATA == null || data == null || COUNT == 0) {
						mapdata.put("FXSCHNO", " ");//預約編號
						mapdata.put("FXSCHTXDATE", " ");//轉帳日期
						mapdata.put("FXWDAC", " ");//轉出帳號
						mapdata.put("FXSVBH", " ");//轉出金額
						mapdata.put("FXSVAC", " ");//銀行名稱/轉入帳號
						mapdata.put("FXWDAMT", " ");//轉入金額
						mapdata.put("FXEXRATE", " ");//匯率
						mapdata.put("FXEFEE", " ");//手續費/郵電費
						mapdata.put("FXTELFEE", " ");
//						mapdata.put("", " ");//國外費用
//						mapdata.put("", " ");//交易類別
						mapdata.put("FXTXMEMO", " ");//備註
						mapdata.put("FXTXSTATUS", " ");//轉帳結果
						countdata.add(mapdata);
						bs.addData("REC", countdata);
					}
					log.trace("bsData>>{}", bs.getData());
					// 標題狀態翻譯
					if ("All".equals(fgtxstatus)) {
						bs.addData("FGTXSTATUS", fgtxstatus);
						bs.addData("FGTXSTATUS_D", "i18n{LB.All}");
					} else if ("0".equals(fgtxstatus)) {
						bs.addData("FGTXSTATUS", fgtxstatus);
						bs.addData("FGTXSTATUS_D", "i18n{LB.D1099}");
					} else if ("1".equals(fgtxstatus)) {
						bs.addData("FGTXSTATUS", fgtxstatus);
						bs.addData("FGTXSTATUS_D", "i18n{LB.W0056}");
					} else {
						bs.addData("FGTXSTATUS", fgtxstatus);
						bs.addData("FGTXSTATUS_D", "i18n{LB.W0057}");
					}
					// 日期區間
					if (cmsdate.equals(cmedate)) {
						bs.addData("cmedate", cmedate);
					} else {
						bs.addData("cmedate", cmsdate + " ～ " + cmedate);
					}
					// 現在時間
					bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
					// 通過
					bs.setResult(Boolean.TRUE);
//					if(COUNT == 0) {
//						bs.setResult(false);
//						bs.setMsgCode("ENRD");
//						getMessageByMsgCode(bs);
//					}
				}
			} else {
				List<TXNFXSCHPAYDATA> TXNFXSCHPAYDATA = txnfxschpaydatadao.getByUid(cusidn, reqParam);
				log.trace(ESAPIUtil.vaildLog("TXNFXSCHPAYDATA>>>{}"+ CodeUtil.toJson(TXNFXSCHPAYDATA)));
				if (TXNFXSCHPAYDATA != null) {
					for (TXNFXSCHPAYDATA each : TXNFXSCHPAYDATA) {
						// 將po轉成map
						Map<String, Object> eachMap = ObjectConvertUtil.object2Map(each);
						// 將復合主鍵的key由po轉成map
						Map<String, Object> pks = ObjectConvertUtil.object2Map(eachMap.get("pks"));
						// 把複合主鍵的key塞進eachMap
						eachMap.put("FXSCHNO", pks.get("FXSCHNO"));
						eachMap.put("FXSCHTXDATE", pks.get("FXSCHTXDATE"));

						// 日期格式處理
						String FXSCHTXDATE = (String) eachMap.get("FXSCHTXDATE");
						String FXTXDATE = (String) eachMap.get("FXTXDATE");
						FXTXDATE = FXTXDATE.trim();
						eachMap.put("FXSCHTXDATE", DateUtil.convertDate(2, FXSCHTXDATE, "yyyyMMdd", "yyyy/MM/dd"));
						if(FXTXDATE != null && !FXTXDATE.equals("")) {
							eachMap.put("FXTXDATE", DateUtil.convertDate(2, FXTXDATE, "yyyyMMdd", "yyyy/MM/dd"));
						}

						// 金額格式處理
						eachMap.put("FXWDAMT", NumericUtil.fmtAmount((String) eachMap.get("FXWDAMT"), 2));
						eachMap.put("FXEFEE", NumericUtil.fmtAmount((String) eachMap.get("FXEFEE"), 2));
						eachMap.put("FXTELFEE", NumericUtil.fmtAmount((String) eachMap.get("FXTELFEE"), 2));

						String locale = LocaleContextHolder.getLocale().toString();
						
						// 比對bank名稱
						String fxsvbh = (String) eachMap.get("FXSVBH");
						String bank = "";
						if(locale.equals("zh_CN")) {
							bank = admbankdao.getBankChName(fxsvbh);
						}
						else if(locale.equals("en")) {
							bank = admbankdao.getBankEnName(fxsvbh);
						}
						else {
							bank = admbankdao.getBankName(fxsvbh);
						}
						
						if(bank != "") {
							fxsvbh = fxsvbh + bank;
							eachMap.put("FXSVBH", fxsvbh);
						}
						else {
							eachMap.put("FXSVBH", "");
						}
						
						//交易類別
						String adopid = (String) eachMap.get("ADOPID");
						String adopidh = "";
						//多語系
						if(adopid != null) {
							if(locale.equals("zh_CN")) {
								adopid = nb3sysopdao.findAdopidCN(adopid);
							}
							else if(locale.equals("en")) {
								adopid = nb3sysopdao.findAdopidEN(adopid);
							}
							else {
								adopid = nb3sysopdao.findAdopid(adopid);
							}
							adopidh = ESAPI.encoder().encodeForHTML(adopid);
						}
						eachMap.put("ADOPID", adopid);
						eachMap.put("ADOPIDH", adopidh);
						
						//國外費用
						String str_OURCHG = (String) eachMap.get("FXOURCHG");
						if(str_OURCHG == null) {
							str_OURCHG = "";
						}
						eachMap.put("str_OURCHG", str_OURCHG);

						// 內容狀態翻譯
						String status = (String) eachMap.get("FXTXSTATUS");
						if ("0".equals(status)) {
							//成功
							eachMap.put("FXTXSTATUS", "LB.D1099");
//							eachMap.put("FXTXSTATUS_D", "i18n{LB.D1099}");
						} else if(" ".equals(status)) {
							//未執行
							eachMap.put("FXTXSTATUS", "LB.X0048");
//							eachMap.put("FXTXSTATUS_D", "i18n{LB.X0048}");
						} else if ("1".equals(status)) {
							//失敗
							eachMap.put("FXTXSTATUS", "LB.W0056");
//							eachMap.put("FXTXSTATUS_D", "i18n{LB.W0056}");
							// 比對錯誤代碼名稱
							String fxexcode = (String) eachMap.get("FXEXCODE");
							String excode = admmsgcodedao.getCodeMsg(fxexcode);
							if(excode != null && !"".equals(excode)) {
								eachMap.put("CODEMSG", excode);								
							}else {
								eachMap.put("CODEMSG", "");																
							}
						} else {
							//處理中
							eachMap.put("FXTXSTATUS", "LB.W0057");
//							eachMap.put("FXTXSTATUS_D", "i18n{LB.W0057}");
						}

						data.add((Map<String, Object>) eachMap);
					}
					bs.addData("REC", data);

					// 筆數
					int COUNT = 0;
					COUNT = data.size();
					bs.addData("COUNT", String.valueOf(COUNT));

					// 處理空值顯示
					if (TXNFXSCHPAYDATA == null || data == null || COUNT == 0) {
						mapdata.put("FXSCHNO", " ");//預約編號
						mapdata.put("FXSCHTXDATE", " ");//轉帳日期
						mapdata.put("FXWDAC", " ");//轉出帳號
						mapdata.put("FXSVBH", " ");//轉出金額
						mapdata.put("FXSVAC", " ");//銀行名稱/轉入帳號
						mapdata.put("FXWDAMT", " ");//轉入金額
						mapdata.put("FXEXRATE", " ");//匯率
						mapdata.put("FXEFEE", " ");//手續費/郵電費
						mapdata.put("FXTELFEE", " ");
//						mapdata.put("", " ");//國外費用
//						mapdata.put("", " ");//交易類別
						mapdata.put("FXTXMEMO", " ");//備註
						mapdata.put("FXTXSTATUS", " ");//轉帳結果
						countdata.add(mapdata);
						bs.addData("REC", countdata);
					}
					log.trace("bsData_count>>{}", bs.getData());

					// 標題狀態翻譯
					if ("All".equals(fgtxstatus)) {
						bs.addData("FGTXSTATUS", fgtxstatus);
						bs.addData("FGTXSTATUS_D", "i18n{LB.All}");
					} else if ("0".equals(fgtxstatus)) {
						bs.addData("FGTXSTATUS", fgtxstatus);
						bs.addData("FGTXSTATUS_D", "i18n{LB.D1099}");
					} else if ("1".equals(fgtxstatus)) {
						bs.addData("FGTXSTATUS", fgtxstatus);
						bs.addData("FGTXSTATUS_D", "i18n{LB.W0056}");
					} else {
						bs.addData("FGTXSTATUS", fgtxstatus);
						bs.addData("FGTXSTATUS_D", "i18n{LB.W0057}");
					}

					// 日期區間
					if (cmsdate.equals(cmedate)) {
						bs.addData("cmedate", cmedate);
					} else {
						bs.addData("cmedate", cmsdate + " ～ " + cmedate);
					}
					// 現在時間
					bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
					// 通過
					bs.setResult(Boolean.TRUE);
//					if(COUNT == 0) {
//						bs.setResult(false);
//						bs.setMsgCode("ENRD");
//						getMessageByMsgCode(bs);
//					}
				}
			}

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("f_reservation_trans_result error >> {}",e);
		}
		return bs;
	}

}
