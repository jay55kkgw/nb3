package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class F017T_REST_RQ extends BaseRestBean_FX implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3238340782724132819L;

	private String ISSUER; // 晶片卡發卡行庫

	private String FLAG;// 0：非約定，1：約定

	private String TRANSEQ;// 交易序號

	private String ACNNO;// 晶片卡帳號

	private String ICDTTM;// 晶片卡日期時間

	private String ICSEQ;// 晶片卡序號

	private String ICMEMO;// 晶片卡備註欄

	private String TAC_Length;// TAC DATA Length

	private String TAC;// TAC DATA

	private String TAC_120space;// C DATA Space

	private String TRMID;// 端末設備查核碼

	private String iSeqNo; // iSeqNo

	private String pkcs7Sign;// IKEY

	private String jsondc;// IKEY

	private String CUSIDN; // 付款人統一編號

	private String FYACN;// F017 付款帳號

	private String CRY;// 付款人轉出幣別

	private String AMTDHID;// 轉出金額 9(13)V9(2)

	private String ATRAMT; // 轉入金額 9(13)V9(2)

	private String PRCDT; // 付款日期

	private String SFXGLSTNO; // 議價編號1

	private String TSFAN; // 收款人帳號

	private String DISFEE; // 折扣後手續費

	private String DISTOTH; // 折扣後郵電費

	private String ADOPID;		// 電文代號

	private String PAIAFTX;//

	private String IN_CRY; // 轉入幣別

	private String CURCODE; // 轉入幣別

	private String ROE;

	private String INT;// 利息

	private String FYTAX;// 稅款

	private String FIXEDFLAG;//

	private String SRCFUND;//

	private String NULFLAG;//

	private String CUSTYPE;//

	private String NAME;//

	private String CURAMT;// 轉出金額

	private String CMTRDATE;// 付款日期

	public String getISSUER()
	{
		return ISSUER;
	}

	public void setISSUER(String iSSUER)
	{
		ISSUER = iSSUER;
	}

	public String getFLAG()
	{
		return FLAG;
	}

	public void setFLAG(String fLAG)
	{
		FLAG = fLAG;
	}

	public String getTRANSEQ()
	{
		return TRANSEQ;
	}

	public void setTRANSEQ(String tRANSEQ)
	{
		TRANSEQ = tRANSEQ;
	}

	public String getACNNO()
	{
		return ACNNO;
	}

	public void setACNNO(String aCNNO)
	{
		ACNNO = aCNNO;
	}

	public String getICDTTM()
	{
		return ICDTTM;
	}

	public void setICDTTM(String iCDTTM)
	{
		ICDTTM = iCDTTM;
	}

	public String getICSEQ()
	{
		return ICSEQ;
	}

	public void setICSEQ(String iCSEQ)
	{
		ICSEQ = iCSEQ;
	}

	public String getICMEMO()
	{
		return ICMEMO;
	}

	public void setICMEMO(String iCMEMO)
	{
		ICMEMO = iCMEMO;
	}

	public String getTAC_Length()
	{
		return TAC_Length;
	}

	public void setTAC_Length(String tAC_Length)
	{
		TAC_Length = tAC_Length;
	}

	public String getTAC()
	{
		return TAC;
	}

	public void setTAC(String tAC)
	{
		TAC = tAC;
	}

	public String getTAC_120space()
	{
		return TAC_120space;
	}

	public void setTAC_120space(String tAC_120space)
	{
		TAC_120space = tAC_120space;
	}

	public String getTRMID()
	{
		return TRMID;
	}

	public void setTRMID(String tRMID)
	{
		TRMID = tRMID;
	}

	public String getiSeqNo()
	{
		return iSeqNo;
	}

	public void setiSeqNo(String iSeqNo)
	{
		this.iSeqNo = iSeqNo;
	}

	public String getPkcs7Sign()
	{
		return pkcs7Sign;
	}

	public void setPkcs7Sign(String pkcs7Sign)
	{
		this.pkcs7Sign = pkcs7Sign;
	}

	public String getJsondc()
	{
		return jsondc;
	}

	public void setJsondc(String jsondc)
	{
		this.jsondc = jsondc;
	}

	public String getCUSIDN()
	{
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN)
	{
		CUSIDN = cUSIDN;
	}

	public String getFYACN()
	{
		return FYACN;
	}

	public void setFYACN(String fYACN)
	{
		FYACN = fYACN;
	}

	public String getCRY()
	{
		return CRY;
	}

	public void setCRY(String cRY)
	{
		CRY = cRY;
	}

	public String getAMTDHID()
	{
		return AMTDHID;
	}

	public void setAMTDHID(String aMTDHID)
	{
		AMTDHID = aMTDHID;
	}

	public String getATRAMT()
	{
		return ATRAMT;
	}

	public void setATRAMT(String aTRAMT)
	{
		ATRAMT = aTRAMT;
	}

	public String getPRCDT()
	{
		return PRCDT;
	}

	public void setPRCDT(String pRCDT)
	{
		PRCDT = pRCDT;
	}

	public String getSFXGLSTNO()
	{
		return SFXGLSTNO;
	}

	public void setSFXGLSTNO(String sFXGLSTNO)
	{
		SFXGLSTNO = sFXGLSTNO;
	}

	public String getTSFAN()
	{
		return TSFAN;
	}

	public void setTSFAN(String tSFAN)
	{
		TSFAN = tSFAN;
	}

	public String getDISFEE()
	{
		return DISFEE;
	}

	public void setDISFEE(String dISFEE)
	{
		DISFEE = dISFEE;
	}

	public String getDISTOTH()
	{
		return DISTOTH;
	}

	public void setDISTOTH(String dISTOTH)
	{
		DISTOTH = dISTOTH;
	}

	public String getADOPID()
	{
		return ADOPID;
	}

	public void setADOPID(String aDOPID)
	{
		ADOPID = aDOPID;
	}

	public String getPAIAFTX()
	{
		return PAIAFTX;
	}

	public void setPAIAFTX(String pAIAFTX)
	{
		PAIAFTX = pAIAFTX;
	}

	public String getIN_CRY()
	{
		return IN_CRY;
	}

	public void setIN_CRY(String iN_CRY)
	{
		IN_CRY = iN_CRY;
	}

	public String getCURCODE()
	{
		return CURCODE;
	}

	public void setCURCODE(String cURCODE)
	{
		CURCODE = cURCODE;
	}

	public String getROE()
	{
		return ROE;
	}

	public void setROE(String rOE)
	{
		ROE = rOE;
	}

	public String getINT()
	{
		return INT;
	}

	public void setINT(String iNT)
	{
		INT = iNT;
	}

	public String getFYTAX()
	{
		return FYTAX;
	}

	public void setFYTAX(String fYTAX)
	{
		FYTAX = fYTAX;
	}

	public String getFIXEDFLAG()
	{
		return FIXEDFLAG;
	}

	public void setFIXEDFLAG(String fIXEDFLAG)
	{
		FIXEDFLAG = fIXEDFLAG;
	}

	public String getSRCFUND()
	{
		return SRCFUND;
	}

	public void setSRCFUND(String sRCFUND)
	{
		SRCFUND = sRCFUND;
	}

	public String getNULFLAG()
	{
		return NULFLAG;
	}

	public void setNULFLAG(String nULFLAG)
	{
		NULFLAG = nULFLAG;
	}

	public String getCUSTYPE()
	{
		return CUSTYPE;
	}

	public void setCUSTYPE(String cUSTYPE)
	{
		CUSTYPE = cUSTYPE;
	}

	public String getNAME()
	{
		return NAME;
	}

	public void setNAME(String nAME)
	{
		NAME = nAME;
	}

	public String getCURAMT()
	{
		return CURAMT;
	}

	public void setCURAMT(String cURAMT)
	{
		CURAMT = cURAMT;
	}


	public String getCMTRDATE()
	{
		return CMTRDATE;
	}

	public void setCMTRDATE(String cMTRDATE)
	{
		CMTRDATE = cMTRDATE;
	}

}
