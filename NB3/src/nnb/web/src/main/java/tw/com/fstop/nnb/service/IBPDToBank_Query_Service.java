package tw.com.fstop.nnb.service;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;

@Service
public class IBPDToBank_Query_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());

	public Boolean checkMac(Map<String, Object> reqParam) {
		Boolean result = false;
		try {
			LinkedHashMap IBPD_Param = new LinkedHashMap();
			IBPD_Param.put("SrcID", reqParam.get("SrcID"));
			IBPD_Param.put("KeyID", reqParam.get("KeyID"));
			IBPD_Param.put("STAN", reqParam.get("STAN"));
			IBPD_Param.put("TxnDatetime", reqParam.get("TxnDatetime"));
			IBPD_Param.put("OrigTxnDatetime", reqParam.get("OrigTxnDatetime"));
			IBPD_Param.put("OrigSTAN", reqParam.get("OrigSTAN"));
			log.debug(ESAPIUtil.vaildLog("IBPD_Param >> " + CodeUtil.toJson(IBPD_Param)));
			reqParam.put("IBPD_Param", CodeUtil.toJson(IBPD_Param));
			BaseResult bs = N855checkmac_REST(reqParam);
			if(bs.getResult()) {
				result = true;
			}
		}catch(Exception e) {
			log.error("checkMac error >>{}", e);
		}
		return result;
	}
	public Map<String, String> query(Map<String, Object> reqParam) {
		Map<String, String> result = new HashMap<String, String>();
		try {
			BaseResult bs = N856_REST(reqParam);
			Map<String, Object> resultData = (Map<String, Object>)bs.getData();
			result.put("SrcID", (String)resultData.get("SrcID"));
			result.put("KeyID", (String)resultData.get("KeyID"));
			result.put("DivData", (String)resultData.get("DivData"));
			result.put("ICV", (String)resultData.get("ICV"));
			result.put("MAC", (String)resultData.get("MAC"));
			result.put("TxnDatetime", (String)resultData.get("TxnDatetime"));
			result.put("STAN", (String)resultData.get("STAN"));
			result.put("RCode", (String)resultData.get("RCode"));
			result.put("CustBillerAcnt", (String)resultData.get("CustBillerAcnt"));
			result.put("CustBankAcnt", (String)resultData.get("CustBankAcnt"));
		}catch(Exception e) {
			log.error("checkMac error >>{}", e);
		}
		return result;
	}
}
