package tw.com.fstop.nnb.service;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellCopyPolicy;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.DownloadUtil;

@Service
public class Excel_Download_Service extends AbstractXlsxView{
	private Logger log = LoggerFactory.getLogger(this.getClass().getName());
	
	@Autowired
	private I18n i18n;
	
	@SuppressWarnings("unchecked")
	public void buildExcelDocument(Map<String,Object> parameterMap,Workbook workbook,HttpServletRequest request,HttpServletResponse response){
		log.debug("IN buildExcelDocument");
		log.debug("parameterMap={}",parameterMap);
		
		InputStream inputStream = null;
		XSSFWorkbook xssfWorkbook = null;
		
		try{
			String templatePath = (String)parameterMap.get("templatePath");
			log.debug("templatePath={}",templatePath);
			
			Resource resource = new ClassPathResource(templatePath);
//			File file = resource.getFile();
			
			if(resource.exists()){
				inputStream = new BufferedInputStream(resource.getInputStream());
				
				xssfWorkbook = new XSSFWorkbook(inputStream);
				XSSFSheet xssfSheet = xssfWorkbook.getSheetAt(0);
				
				Boolean hasMultiRowDataBoolean = null;
				String hasMultiRowData = String.valueOf(parameterMap.get("hasMultiRowData"));
				log.debug("hasMultiRowData={}",hasMultiRowData);
				if(!"null".equals(hasMultiRowData)){
					hasMultiRowDataBoolean = Boolean.valueOf(hasMultiRowData);
				}
				log.debug("hasMultiRowDataBoolean={}",hasMultiRowDataBoolean);
				
				Integer headerRightEndInteger = null;
				String headerRightEnd = String.valueOf(parameterMap.get("headerRightEnd"));
				log.debug("headerRightEnd={}",headerRightEnd);
				if(!"null".equals(headerRightEnd)){
					headerRightEndInteger = Integer.valueOf(headerRightEnd);
				}
				log.debug("headerRightEndInteger={}",headerRightEndInteger);
				
				Integer headerBottomEndInteger = null;
				String headerBottomEnd = String.valueOf(parameterMap.get("headerBottomEnd"));
				log.debug("headerBottomEnd={}",headerBottomEnd);
				if(!"null".equals(headerBottomEnd)){
					headerBottomEndInteger = Integer.valueOf(headerBottomEnd);
				}
				log.debug("headerBottomEndInteger={}",headerBottomEndInteger);
				
				Integer rowStartIndexInteger = null;
				String rowStartIndex = String.valueOf(parameterMap.get("rowStartIndex"));
				log.debug("rowStartIndex={}",rowStartIndex);
				if(!"null".equals(rowStartIndex)){
					rowStartIndexInteger = Integer.valueOf(rowStartIndex);
				}
				log.debug("rowStartIndexInteger={}",rowStartIndexInteger);
				
				Integer multiRowStartIndexInteger = null;
				String multiRowStartIndex = String.valueOf(parameterMap.get("multiRowStartIndex"));
				log.debug("multiRowStartIndex={}",multiRowStartIndex);
				if(!"null".equals(multiRowStartIndex)){
					multiRowStartIndexInteger = Integer.valueOf(multiRowStartIndex);
				}
				log.debug("multiRowStartIndexInteger={}",multiRowStartIndexInteger);
				
				Integer multiRowEndIndexInteger = null;
				String multiRowEndIndex = String.valueOf(parameterMap.get("multiRowEndIndex"));
				log.debug("multiRowEndIndex={}",multiRowEndIndex);
				if(!"null".equals(multiRowEndIndex)){
					multiRowEndIndexInteger = Integer.valueOf(multiRowEndIndex);
				}
				log.debug("multiRowEndIndexInteger={}",multiRowEndIndexInteger);
				
				Integer multiRowCopyStartIndexInteger = null;
				String multiRowCopyStartIndex = String.valueOf(parameterMap.get("multiRowCopyStartIndex"));
				log.debug("multiRowCopyStartIndex={}",multiRowCopyStartIndex);
				if(!"null".equals(multiRowCopyStartIndex)){
					multiRowCopyStartIndexInteger = Integer.valueOf(multiRowCopyStartIndex);
				}
				log.debug("multiRowCopyStartIndexInteger={}",multiRowCopyStartIndexInteger);
				
				Integer multiRowCopyEndIndexInteger = null;
				String multiRowCopyEndIndex = String.valueOf(parameterMap.get("multiRowCopyEndIndex"));
				log.debug("multiRowCopyEndIndex={}",multiRowCopyEndIndex);
				if(!"null".equals(multiRowCopyEndIndex)){
					multiRowCopyEndIndexInteger = Integer.valueOf(multiRowCopyEndIndex);
				}
				log.debug("multiRowCopyEndIndexInteger={}",multiRowCopyEndIndexInteger);
				
				String multiRowDataListMapKeyString = null;
				String multiRowDataListMapKey = String.valueOf(parameterMap.get("multiRowDataListMapKey"));
				log.debug("multiRowDataListMapKey={}",multiRowDataListMapKey);
				if(!"null".equals(multiRowDataListMapKey)){
					multiRowDataListMapKeyString = multiRowDataListMapKey;
				}
				log.debug("multiRowDataListMapKeyString={}",multiRowDataListMapKeyString);
				
				Integer rowRightEndInteger = null;
				String rowRightEnd = String.valueOf(parameterMap.get("rowRightEnd"));
				log.debug("rowRightEnd={}",rowRightEnd);
				if(!"null".equals(rowRightEnd)){
					rowRightEndInteger = Integer.valueOf(rowRightEnd);
				}
				log.debug("rowRightEndInteger={}",rowRightEndInteger);
				
				Integer footerStartIndexInteger = null;
				String footerStartIndex = String.valueOf(parameterMap.get("footerStartIndex"));
				log.debug("footerStartIndex={}",footerStartIndex);
				if(!"null".equals(footerStartIndex)){
					footerStartIndexInteger = Integer.valueOf(footerStartIndex);
				}
				log.debug("footerStartIndexInteger={}",footerStartIndexInteger);
				
				Integer footerEndIndexInteger = null;
				String footerEndIndex = String.valueOf(parameterMap.get("footerEndIndex"));
				log.debug("footerEndIndex={}",footerEndIndex);
				if(!"null".equals(footerEndIndex)){
					footerEndIndexInteger = Integer.valueOf(footerEndIndex);
				}
				log.debug("footerEndIndexInteger={}",footerEndIndexInteger);
				
				Integer footerRightEndInteger = null;
				String footerRightEnd = String.valueOf(parameterMap.get("footerRightEnd"));
				log.debug("footerRightEnd={}",footerRightEnd);
				if(!"null".equals(footerRightEnd)){
					footerRightEndInteger = Integer.valueOf(footerRightEnd);
				}
				log.debug("footerRightEndInteger={}",footerRightEndInteger);
				
				//先把HEADER的變數換掉
				if(headerBottomEndInteger != null && headerRightEndInteger != null){
					for(int x=0;x<=headerBottomEndInteger;x++){
						log.debug("x={}",x);
						XSSFRow xssfRow = xssfSheet.getRow(x);
						
						if(xssfRow != null){
							for(int y=0;y<=headerRightEndInteger;y++){
								log.debug("y={}",y);
								
								replaceCellValue(xssfRow,y,parameterMap);
							}
						}
					}
				}
				//這裡要分成多個詳細資料和單個的情況
				//FOOTER向下位移的值
				int footerDownNum = 0;
				
				//多個詳細資料
				if(hasMultiRowDataBoolean != null && hasMultiRowDataBoolean == true){
					//把詳細資料的變數換掉
					if(multiRowStartIndexInteger != null && multiRowEndIndexInteger != null && multiRowCopyStartIndexInteger != null
							&& multiRowCopyEndIndexInteger != null && multiRowDataListMapKeyString != null
							&& rowRightEndInteger != null){
						
						List<Map<String,Object>> dataListMap = (List<Map<String,Object>>)parameterMap.get("dataListMap");
						log.debug("dataListMap={}",dataListMap);
						
						//算總共有多少組ROWDATA
						int rowGroupCount = 0;
						//算所有ROWDATA的筆數
						int totalRowDataCount = 0;
						
						for(Map<String,Object> firstMap : dataListMap){
							log.debug("firstMap={}",firstMap);
							
							rowGroupCount += 1;
							
							List<Map<String,String>> secondListMap = (List<Map<String,String>>)firstMap.get(multiRowDataListMapKeyString);
							log.debug("secondListMap={}",secondListMap);
							
							for(Map<String,String> secondMap : secondListMap){
								log.debug("secondMap={}",secondMap);
								
								totalRowDataCount += 1;
							}
						}
						log.debug("rowGroupCount={}",rowGroupCount);
						log.debug("totalRowDataCount={}",totalRowDataCount);
						
						//GROUP開始與ROW開始的差額
						int rangeA = multiRowStartIndexInteger - multiRowCopyStartIndexInteger;
						log.debug("rangeA={}",rangeA);
						//ROW開始與ROW結束的差額
						int rangeB = multiRowEndIndexInteger - multiRowStartIndexInteger;
						log.debug("rangeB={}",rangeB);
						//ROW結束與GROUP結束的差額
						int rangeC = multiRowCopyEndIndexInteger - multiRowEndIndexInteger;
						log.debug("rangeC={}",rangeC);
						
						//下一個複製的GROUP開始位置
						Integer nextRowCopyStartIndexInteger = null;
						//下一個複製的GROUP結束位置
						Integer nextRowCopyEndIndexInteger = null;
						
						for(int x=0;x<rowGroupCount;x++){
							log.debug("x={}",x);
							
							Map<String,Object> firstMap = dataListMap.get(x);
							log.debug("firstMap={}",firstMap);
							
							List<Map<String,String>> secondListMap = (List<Map<String,String>>)firstMap.get(multiRowDataListMapKeyString);
							log.debug("secondListMap={}",secondListMap);
							
							int secondListMapCount = 0;
							int secondListMapSize = secondListMap.size();
							log.debug("secondListMapSize={}",secondListMapSize);
							
							if(x > 0){
								//重新設定數值
								multiRowCopyStartIndexInteger = multiRowCopyEndIndexInteger + 1;
								log.debug("multiRowCopyStartIndexInteger={}",multiRowCopyStartIndexInteger);
								multiRowStartIndexInteger = multiRowCopyStartIndexInteger + rangeA;
								log.debug("multiRowStartIndexInteger={}",multiRowStartIndexInteger);
								multiRowEndIndexInteger = multiRowStartIndexInteger + rangeB;
								log.debug("multiRowEndIndexInteger={}",multiRowEndIndexInteger);
								multiRowCopyEndIndexInteger = multiRowEndIndexInteger + rangeC;
								log.debug("multiRowCopyEndIndexInteger={}",multiRowCopyEndIndexInteger);
							}
						
							if(x < rowGroupCount - 1){
								nextRowCopyStartIndexInteger = multiRowCopyStartIndexInteger;
								log.debug("nextRowCopyStartIndexInteger={}",nextRowCopyStartIndexInteger);
								nextRowCopyEndIndexInteger = multiRowCopyEndIndexInteger;
								log.debug("nextRowCopyEndIndexInteger={}",nextRowCopyEndIndexInteger);
								
								//要複製的GROUP的ROW的LIST
								List<XSSFRow> groupCopyList = new ArrayList<XSSFRow>();
								
								for(int m=nextRowCopyStartIndexInteger;m<=nextRowCopyEndIndexInteger;m++){
									log.debug("m={}",m);
									XSSFRow xssfRow = xssfSheet.getRow(m);
									
									//先把GROUP的ROW存起來，之後要複製到下面
									groupCopyList.add(xssfRow);
								}
								//如果有FOOTER的話
								if(footerStartIndexInteger != null && footerEndIndexInteger != null){
									//將FOOTER列的儲存格往下移
									for(int z=footerEndIndexInteger;z>=footerStartIndexInteger;z--){
										log.debug("z={}",z);
										moveRow(xssfWorkbook,xssfSheet,z,groupCopyList.size(),true);
									}
									footerStartIndexInteger += groupCopyList.size();
									log.debug("footerStartIndexInteger={}",footerStartIndexInteger);
									footerEndIndexInteger += groupCopyList.size();
									log.debug("footerEndIndexInteger={}",footerEndIndexInteger);
								}
								//複製GROUP的ROW
								xssfSheet.copyRows(groupCopyList,multiRowCopyEndIndexInteger + 1,new CellCopyPolicy());
							
								nextRowCopyStartIndexInteger += groupCopyList.size();
								log.debug("nextRowCopyStartIndexInteger={}",nextRowCopyStartIndexInteger);
								nextRowCopyEndIndexInteger += groupCopyList.size();
								log.debug("nextRowCopyEndIndexInteger={}",nextRowCopyEndIndexInteger);
							}
							else{
								nextRowCopyStartIndexInteger = null;
								nextRowCopyEndIndexInteger = null;
							}
							
							for(int y=multiRowCopyStartIndexInteger;y<=multiRowCopyEndIndexInteger;y++){
								log.debug("y={}",y);
								
								XSSFRow xssfRow = xssfSheet.getRow(y);
								
								if(xssfRow != null){
									log.debug("multiRowStartIndexInteger={}",multiRowStartIndexInteger);
									log.debug("multiRowEndIndexInteger={}",multiRowEndIndexInteger);
									//不是詳細資料列
									if(y < multiRowStartIndexInteger || y > multiRowEndIndexInteger){
										
										for(int z=0;z<=rowRightEndInteger;z++){
											log.debug("z={}",z);
											
											replaceCellValue(xssfRow,z,firstMap);
										}
									}
									//詳細資料列
									else{
										if(secondListMapCount < secondListMapSize){
											//還不是最後一筆
											if(secondListMapCount < secondListMapSize - 1){
												//如果ROW的資料大於一筆，把FOOTER往下移一列
												if(secondListMapSize > 1){
													//如果有FOOTER的話
													if(footerStartIndexInteger != null && footerEndIndexInteger != null){
														//將FOOTER列的儲存格往下移
														for(int z=footerEndIndexInteger;z>=footerStartIndexInteger;z--){
															log.debug("z={}",z);
															moveRow(xssfWorkbook,xssfSheet,z,+1,true);
														}
														footerStartIndexInteger += 1;
														log.debug("footerStartIndexInteger={}",footerStartIndexInteger);
														footerEndIndexInteger += 1;
														log.debug("footerEndIndexInteger={}",footerEndIndexInteger);
													}
													
													//如果有下一個GROUP的話
													if(nextRowCopyStartIndexInteger != null && nextRowCopyEndIndexInteger != null){
														for(int n=nextRowCopyEndIndexInteger;n>=nextRowCopyStartIndexInteger;n--){
															log.debug("n={}",n);
															
															//把下一個GROUP的ROW往下移
															moveRow(xssfWorkbook,xssfSheet,n,+1,true);
														}
														nextRowCopyStartIndexInteger += 1;
														log.debug("nextRowCopyStartIndexInteger={}",nextRowCopyStartIndexInteger);
														nextRowCopyEndIndexInteger += 1;
														log.debug("nextRowCopyEndIndexInteger={}",nextRowCopyEndIndexInteger);
													}
													
													//如果變數列下面還有GROUP的下半部的話，也要一起往下移
													if(multiRowCopyEndIndexInteger > multiRowEndIndexInteger){
														for(int u=multiRowCopyEndIndexInteger;u>multiRowEndIndexInteger;u--){
															log.debug("u={}",u);
															
															moveRow(xssfWorkbook,xssfSheet,u,+1,true);
														}
													}
													
													//把變數複製到下一列，到下一列時才能取代成值
													moveRow(xssfWorkbook,xssfSheet,y,+1,false);
													
													multiRowCopyEndIndexInteger += 1;
													log.debug("multiRowCopyEndIndexInteger={}",multiRowCopyEndIndexInteger);
													multiRowEndIndexInteger += 1;
													log.debug("multiRowEndIndexInteger={}",multiRowEndIndexInteger);
												}
											}
											//將詳細資料列的變數換成值
											for(int z=0;z<=rowRightEndInteger;z++){
												log.debug("z={}",z);
													
												replaceCellValue(xssfRow,z,secondListMap.get(secondListMapCount));
											}
											secondListMapCount += 1;
											log.debug("secondListMapCount={}",secondListMapCount);
										}
									}
								}
								else{
									log.debug("xssfRow == null");
									break;
								}
							}
						}
					}
				}
				//一個詳細資料
				else{
					int dataListMapSize = 0;
					//把詳細資料的變數換掉
					if(rowStartIndexInteger != null && rowRightEndInteger != null){
						List<Map<String,String>> dataListMap = (List<Map<String,String>>)parameterMap.get("dataListMap");
						log.debug("dataListMap={}",dataListMap);
						
						dataListMapSize = dataListMap.size();
						log.debug("dataListMapSize={}",dataListMapSize);
						
						footerDownNum = dataListMapSize - 1;
						
						int rowCount = 0;
						
						for(int x=rowStartIndexInteger;x<rowStartIndexInteger+dataListMapSize;x++){
							log.debug("x={}",x);
							XSSFRow xssfRow = xssfSheet.getRow(x);
							
							if(xssfRow != null){
								Map<String,String> dataMap = dataListMap.get(rowCount);
								log.debug("dataMap={}",dataMap);
								
								rowCount += 1;
								//還有資料
								if(x < rowStartIndexInteger + dataListMapSize - 1){
									//如果有FOOTER的話
									if(footerStartIndexInteger != null && footerEndIndexInteger != null){
										//將FOOTER列的儲存格往下移一列
										for(int z=footerEndIndexInteger;z>=footerStartIndexInteger;z--){
											moveRow(xssfWorkbook,xssfSheet,z,+1,true);
										}
									}
									//把變數複製到下一列，到下一列時才能取代成值
									moveRow(xssfWorkbook,xssfSheet,x,+1,false);
								}
								
								for(int y=0;y<=rowRightEndInteger;y++){
									log.debug("y={}",y);
									
									replaceCellValue(xssfRow,y,dataMap);
								}
							}
							else{
								log.debug("xssfRow == null");
								break;
							}
						}
					}
				}
				//把FOOTER的變數換掉
				if(footerStartIndexInteger != null && footerEndIndexInteger != null && footerRightEndInteger != null){
					//因為詳細資料大於一筆的話，FOOTER才會開始下移，所以要減1
					for(int x=footerStartIndexInteger+footerDownNum;x<=footerEndIndexInteger+footerDownNum;x++){
						log.debug("x={}",x);
						XSSFRow xssfRow = xssfSheet.getRow(x);
						
						if(xssfRow != null){
							for(int y=0;y<=footerRightEndInteger;y++){
								log.debug("y={}",y);
								
								replaceCellValue(xssfRow,y,parameterMap);
							}
						}
					}
				}
				
				response.setContentType("application/vnd.ms-excel;charset=UTF-8");
				
				String downloadFileName = (String)parameterMap.get("downloadFileName");
				log.debug("downloadFileName={}",downloadFileName);
				downloadFileName = i18n.getMsg(downloadFileName).replaceAll(" ","") + ".xlsx";
				log.debug("downloadFileName={}",downloadFileName);
				
				//response.setHeader("Content-Disposition","attachment;filename=" + URLEncoder.encode(downloadFileName,"UTF-8"));
				String header = request.getHeader("User-Agent").toUpperCase();
				 if (header.contains("FIREFOX")) {
					 response.setHeader("Content-Disposition","attachment;filename="+ URLEncoder.encode(downloadFileName,"UTF-8")+";filename*=utf-8'" + URLEncoder.encode(downloadFileName,"UTF-8"));
				 }else {
					 response.setHeader("Content-Disposition","attachment;filename=" + URLEncoder.encode(downloadFileName,"UTF-8"));
				 }
				xssfWorkbook.write(response.getOutputStream());
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("buildExcelDocument error >> {}",e);
		}
		try{
			if(xssfWorkbook != null){
				xssfWorkbook.close();
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("buildExcelDocument error >> {}",e);
		}
		try{
			if(inputStream != null){
				inputStream.close();
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("buildExcelDocument error >> {}",e);
		}
	}
	//將EXCEL裡的CELL的變數KEY換成值或是將I18N的KEY換成值
	private void replaceCellValue(XSSFRow xssfRow,int cellNum,Map<String,?> dataMap){
		XSSFCell xssfCell = xssfRow.getCell(cellNum);
		
		if(xssfCell != null){
			String cellValue = xssfCell.getStringCellValue();
			log.debug("cellValue={}",cellValue);
			
			List<String> variableKeysList = new ArrayList<String>();
			String leftSignal = "${";
			String rightSignal = "}";
			
			DownloadUtil.getVariableKeys(variableKeysList,leftSignal,rightSignal,cellValue);
			log.debug("variableKeysList={}",variableKeysList);
			
			//有變數KEY要取代
			for(String key : variableKeysList){
				String dataMapValue = (String)dataMap.get(key.trim());
				log.debug("dataMapValue={}",dataMapValue);
				
				String replaceValue = leftSignal + key + rightSignal;
				log.debug("replaceValue={}",replaceValue);
				
				//有對應的值
				if(dataMapValue != null){
					cellValue = cellValue.replace(replaceValue,dataMapValue);
					xssfCell.setCellValue(cellValue);
				}
				//沒有對應的值，代空值
				else{
					cellValue = cellValue.replace(replaceValue,"");
					xssfCell.setCellValue(cellValue);
				}
			}
			
			cellValue = xssfCell.getStringCellValue();
			log.debug("cellValue={}",cellValue);
			
			variableKeysList = new ArrayList<String>();
			leftSignal = "i18n{";
			rightSignal = "}";
			
			DownloadUtil.getVariableKeys(variableKeysList,leftSignal,rightSignal,cellValue);
			log.debug("variableKeysList={}",variableKeysList);
			
			//有I18NKEY要取代
			for(String key : variableKeysList){
				String i18nValue = i18n.getMsg(key.trim());
				log.debug("i18nValue={}",i18nValue);
				
				String replaceValue = leftSignal + key + rightSignal;
				log.debug("replaceValue={}",replaceValue);
				
				//有對應的值
				if(i18nValue != null){
					cellValue = cellValue.replace(replaceValue,i18nValue);
					xssfCell.setCellValue(cellValue);
				}
				//沒有對應的值，代空值
				else{
					cellValue = cellValue.replace(replaceValue,"");
					xssfCell.setCellValue(cellValue.replace(replaceValue,""));
				}
			}
		}
	}
	//找出這一列是否有合併儲存格的INDEX
	private List<Integer> getThisRowMergedRegionIndex(XSSFSheet xssfSheet,int thisRowNum){
		log.debug("xssfSheet={}",xssfSheet);
		log.debug("thisRowNum={}",thisRowNum);
		
		//找出所有合併儲存格的位置
		List<CellRangeAddress> allCellRangeAddressList = xssfSheet.getMergedRegions();
		
		List<Integer> mergedRegionIndexList = new ArrayList<Integer>();
		
		for(int k=0;k<allCellRangeAddressList.size();k++){
			int firstRow = allCellRangeAddressList.get(k).getFirstRow();
			log.debug("firstRow={}",firstRow);
			int lastRow = allCellRangeAddressList.get(k).getLastRow();
			log.debug("lastRow={}",lastRow);
			int firstColumn = allCellRangeAddressList.get(k).getFirstColumn();
			log.debug("firstColumn={}",firstColumn);
			int lastColumn = allCellRangeAddressList.get(k).getLastColumn();
			log.debug("lastColumn={}",lastColumn);
			
			//這一列有合併儲存格
			if(firstRow == thisRowNum){
				mergedRegionIndexList.add(k);
			}
		}
		return mergedRegionIndexList;
	}
	//刪除合併儲存格
	private void deleteMergedRegion(XSSFSheet xssfSheet,int deleteRowNum){
		//刪除列是否有合併儲存格
		List<Integer> deleteMergedRegionIndexList = getThisRowMergedRegionIndex(xssfSheet,deleteRowNum);
		
		for(int x=deleteMergedRegionIndexList.size()-1;x>=0;x--){
			//移除目的列合併儲存格
			xssfSheet.removeMergedRegion(deleteMergedRegionIndexList.get(x));
		}
	}
	//將EXCEL裡的ROW移動位置
	private void moveRow(XSSFWorkbook xssfWorkbook,XSSFSheet xssfSheet,int thisRowNum,int moveNum,boolean deleteOriginal){
		XSSFRow thisRow = xssfSheet.getRow(thisRowNum);
		
		if(thisRow != null){
			//另一列刪除合併儲存格
			deleteMergedRegion(xssfSheet,thisRowNum + moveNum);
			
			//把這一列的資料複製到另一列
			xssfSheet.copyRows(thisRowNum,thisRowNum,thisRowNum + moveNum,new CellCopyPolicy());
			
			//將CELL的格式複製過去
			XSSFRow thatRow = xssfSheet.getRow(thisRowNum + moveNum);
			
			for(int i=0;i<thisRow.getLastCellNum();i++){
				XSSFCell oldCell = thisRow.getCell(i);
				XSSFCell newCell = thatRow.getCell(i);

				if(oldCell == null){
					newCell = null;
					continue;
				}
				XSSFCellStyle newCellStyle = xssfWorkbook.createCellStyle();

				CellStyle oldCellStyle = oldCell.getCellStyle();
				newCellStyle.cloneStyleFrom(oldCellStyle);
				
				newCell.setCellStyle(newCellStyle);
		        
				CellType oldCellType = oldCell.getCellType();
				newCell.setCellType(oldCellType);
			}
			
			//把這一列的資料刪除
			if(deleteOriginal == true){
				xssfSheet.removeRow(thisRow);

				//把這一列的合併儲存格刪除
				deleteMergedRegion(xssfSheet,thisRowNum);
			}
		}
	}
}