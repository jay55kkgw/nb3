package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class F001R_REST_RQ extends BaseRestBean_FX implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -4231247672342402258L;
	
	private String TX_CODE;		// 交易代號
	private String LENGTH;		// LENGTH
	private String TRXCOD1;		// TNB0MBBT1
	private String FILL_X2;		// FILL_X2
	private String PGMID;		// 程式名稱
	private String ETY_LVL;		// ENTRY LEVEL
	private String BRH_CODE;	// 分行代碼 x(4)
	private String TC_NO;		// TC號碼
	private String WS_ID;		// 工作站代碼 R6000001
	private String TL_ID;		// 櫃員代號
	private String TERM_DATE;	// 日期YYYYMMDD
	private String TERM_TIME;	// 時間HHMMSS
	private String STATUS;		// 狀態
	private String LRGOUT_NUM;	// 空白
	private String PFKEY;		// PFKEY 2
	private String PROD;		// 空白
	private String DATA_LENGTH;	// 資料長度
	private String MSGHEADER;	// 訊息標頭
	private String PCODE;		// 交易代號
	private String STAN;		// 交易續號
	private String SEQNO;		// 交易流水號
	private String MSGID;		// 訊息代碼
	private String CUSTID;		// 付款人統一編號
	private String CUSTYPE;		// 客戶身分別
	private String CUSNM1;		// 付款人名稱1
	private String CUSNM2;		// 付款人名稱2
	private String CUSNM3;		// 付款人名稱3
	private String CUSNM4;		// 付款人名稱4
	private String RELREFNO;	// 匯入匯款交易編號
	private String ONLINEPAY;	// 線上解款註記
	private String CUSTACC;		// 轉出帳號
	private String PAYCCY;		// 轉出幣別
	private String REMITCY;		// 轉入幣別
	private String CURAMT;		// 議價金額 9(13)V9(2)
	private String PAYREMIT;	// 轉出轉入別
	private String PAYDATE;		// 付款日期
	private String DEALNO;		// 人工議價編號
	private String BENACC;		// 轉入帳號
	private String BENID;		// 收款人統編
	private String BENNM1;		// 收款人名稱1
	private String BENTYPE;		// 受款人身分別
	private String SRCFUND;		// 匯款分類編號
	private String COUNTRY;		// 交易國別
	private String DETCHG;		// 手續費分擔方式
	private String COMMACC;		// 手續費帳號
	private String COMMCCY;		// 手續費幣別
	private String MEMO1;		// 匯款附言1
	private String MEMO2;		// 匯款附言2
	private String MEMO3;		// 匯款附言3
	private String MEMO4;		// 匯款附言4
	private String MSGCOD;		// 訊息代碼
	
	private String NULFLAG;		
	private String FIXEDFLAG;	
	private String CUSIDN;		
	private String PINNEW;		
	private String CMPERIOD;
	private String CMEDATE;		
//	private String ADTXAMT;		
	private String NAME;		
	private String FGTRDATE;	
	private String SELFFLAG;	
	private String INACNO1;	
	private String INACNO2;	
	private String FGINACNO;	
	private String CMSDATE;	
	private String SRCFUNDDESC;	
	private String CMTRDATE;
//	private String ADCURRENCY;

	// 電文不需要但ms_tw需要的欄位
//	private String FGTXWAY;		// 交易機制
	private String iSeqNo;		// iSeqNo
	private String FGTXDATE;	// 即時或預約
	private String TRANSPASSUPDATE;// 即時或預約
	private String pkcs7Sign;	// IKEY
	private String jsondc;		// IKEY

	// 舊BEAN.writeLog需要的欄位
	private String UID;			// 身分證字號
	private String ADOPID;		// 電文代號
	private String CMTRMEMO;	// 交易備註
	private String DPMYEMAIL;	// 通訊錄
	private String CMTRMAIL;	// 通訊錄
	private String CMMAILMEMO;	// 摘要內容
	
	//IDGATE
	private String sessionID;
	private String idgateID;
	private String txnID;

		
	public String getTX_CODE() {
		return TX_CODE;
	}
	public void setTX_CODE(String tX_CODE) {
		TX_CODE = tX_CODE;
	}
	public String getLENGTH() {
		return LENGTH;
	}
	public void setLENGTH(String lENGTH) {
		LENGTH = lENGTH;
	}
	public String getTRXCOD1() {
		return TRXCOD1;
	}
	public void setTRXCOD1(String tRXCOD1) {
		TRXCOD1 = tRXCOD1;
	}
	public String getFILL_X2() {
		return FILL_X2;
	}
	public void setFILL_X2(String fILL_X2) {
		FILL_X2 = fILL_X2;
	}
	public String getPGMID() {
		return PGMID;
	}
	public void setPGMID(String pGMID) {
		PGMID = pGMID;
	}
	public String getETY_LVL() {
		return ETY_LVL;
	}
	public void setETY_LVL(String eTY_LVL) {
		ETY_LVL = eTY_LVL;
	}
	public String getBRH_CODE() {
		return BRH_CODE;
	}
	public void setBRH_CODE(String bRH_CODE) {
		BRH_CODE = bRH_CODE;
	}
	public String getTC_NO() {
		return TC_NO;
	}
	public void setTC_NO(String tC_NO) {
		TC_NO = tC_NO;
	}
	public String getWS_ID() {
		return WS_ID;
	}
	public void setWS_ID(String wS_ID) {
		WS_ID = wS_ID;
	}
	public String getTL_ID() {
		return TL_ID;
	}
	public void setTL_ID(String tL_ID) {
		TL_ID = tL_ID;
	}
	public String getTERM_DATE() {
		return TERM_DATE;
	}
	public void setTERM_DATE(String tERM_DATE) {
		TERM_DATE = tERM_DATE;
	}
	public String getTERM_TIME() {
		return TERM_TIME;
	}
	public void setTERM_TIME(String tERM_TIME) {
		TERM_TIME = tERM_TIME;
	}
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getLRGOUT_NUM() {
		return LRGOUT_NUM;
	}
	public void setLRGOUT_NUM(String lRGOUT_NUM) {
		LRGOUT_NUM = lRGOUT_NUM;
	}
	public String getPFKEY() {
		return PFKEY;
	}
	public void setPFKEY(String pFKEY) {
		PFKEY = pFKEY;
	}
	public String getPROD() {
		return PROD;
	}
	public void setPROD(String pROD) {
		PROD = pROD;
	}
	public String getDATA_LENGTH() {
		return DATA_LENGTH;
	}
	public void setDATA_LENGTH(String dATA_LENGTH) {
		DATA_LENGTH = dATA_LENGTH;
	}
	public String getMSGHEADER() {
		return MSGHEADER;
	}
	public void setMSGHEADER(String mSGHEADER) {
		MSGHEADER = mSGHEADER;
	}
	public String getPCODE() {
		return PCODE;
	}
	public void setPCODE(String pCODE) {
		PCODE = pCODE;
	}
	public String getSTAN() {
		return STAN;
	}
	public void setSTAN(String sTAN) {
		STAN = sTAN;
	}
	public String getSEQNO() {
		return SEQNO;
	}
	public void setSEQNO(String sEQNO) {
		SEQNO = sEQNO;
	}
	public String getMSGID() {
		return MSGID;
	}
	public void setMSGID(String mSGID) {
		MSGID = mSGID;
	}
	public String getCUSTID() {
		return CUSTID;
	}
	public void setCUSTID(String cUSTID) {
		CUSTID = cUSTID;
	}
	public String getCUSTYPE() {
		return CUSTYPE;
	}
	public void setCUSTYPE(String cUSTYPE) {
		CUSTYPE = cUSTYPE;
	}
	public String getCUSNM1() {
		return CUSNM1;
	}
	public void setCUSNM1(String cUSNM1) {
		CUSNM1 = cUSNM1;
	}
	public String getCUSNM2() {
		return CUSNM2;
	}
	public void setCUSNM2(String cUSNM2) {
		CUSNM2 = cUSNM2;
	}
	public String getCUSNM3() {
		return CUSNM3;
	}
	public void setCUSNM3(String cUSNM3) {
		CUSNM3 = cUSNM3;
	}
	public String getCUSNM4() {
		return CUSNM4;
	}
	public void setCUSNM4(String cUSNM4) {
		CUSNM4 = cUSNM4;
	}
	public String getRELREFNO() {
		return RELREFNO;
	}
	public void setRELREFNO(String rELREFNO) {
		RELREFNO = rELREFNO;
	}
	public String getONLINEPAY() {
		return ONLINEPAY;
	}
	public void setONLINEPAY(String oNLINEPAY) {
		ONLINEPAY = oNLINEPAY;
	}
	public String getCUSTACC() {
		return CUSTACC;
	}
	public void setCUSTACC(String cUSTACC) {
		CUSTACC = cUSTACC;
	}
	public String getPAYCCY() {
		return PAYCCY;
	}
	public void setPAYCCY(String pAYCCY) {
		PAYCCY = pAYCCY;
	}
	public String getREMITCY() {
		return REMITCY;
	}
	public void setREMITCY(String rEMITCY) {
		REMITCY = rEMITCY;
	}
	public String getCURAMT() {
		return CURAMT;
	}
	public void setCURAMT(String cURAMT) {
		CURAMT = cURAMT;
	}
	public String getPAYREMIT() {
		return PAYREMIT;
	}
	public void setPAYREMIT(String pAYREMIT) {
		PAYREMIT = pAYREMIT;
	}
	public String getPAYDATE() {
		return PAYDATE;
	}
	public void setPAYDATE(String pAYDATE) {
		PAYDATE = pAYDATE;
	}
	public String getDEALNO() {
		return DEALNO;
	}
	public void setDEALNO(String dEALNO) {
		DEALNO = dEALNO;
	}
	public String getBENACC() {
		return BENACC;
	}
	public void setBENACC(String bENACC) {
		BENACC = bENACC;
	}
	public String getBENID() {
		return BENID;
	}
	public void setBENID(String bENID) {
		BENID = bENID;
	}
	public String getBENNM1() {
		return BENNM1;
	}
	public void setBENNM1(String bENNM1) {
		BENNM1 = bENNM1;
	}
	public String getBENTYPE() {
		return BENTYPE;
	}
	public void setBENTYPE(String bENTYPE) {
		BENTYPE = bENTYPE;
	}
	public String getSRCFUND() {
		return SRCFUND;
	}
	public void setSRCFUND(String sRCFUND) {
		SRCFUND = sRCFUND;
	}
	public String getCOUNTRY() {
		return COUNTRY;
	}
	public void setCOUNTRY(String cOUNTRY) {
		COUNTRY = cOUNTRY;
	}
	public String getDETCHG() {
		return DETCHG;
	}
	public void setDETCHG(String dETCHG) {
		DETCHG = dETCHG;
	}
	public String getCOMMACC() {
		return COMMACC;
	}
	public void setCOMMACC(String cOMMACC) {
		COMMACC = cOMMACC;
	}
	public String getCOMMCCY() {
		return COMMCCY;
	}
	public void setCOMMCCY(String cOMMCCY) {
		COMMCCY = cOMMCCY;
	}
	public String getMEMO1() {
		return MEMO1;
	}
	public void setMEMO1(String mEMO1) {
		MEMO1 = mEMO1;
	}
	public String getMEMO2() {
		return MEMO2;
	}
	public void setMEMO2(String mEMO2) {
		MEMO2 = mEMO2;
	}
	public String getMEMO3() {
		return MEMO3;
	}
	public void setMEMO3(String mEMO3) {
		MEMO3 = mEMO3;
	}
	public String getMEMO4() {
		return MEMO4;
	}
	public void setMEMO4(String mEMO4) {
		MEMO4 = mEMO4;
	}
	public String getMSGCOD() {
		return MSGCOD;
	}
	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}
	public String getNULFLAG() {
		return NULFLAG;
	}
	public void setNULFLAG(String nULFLAG) {
		NULFLAG = nULFLAG;
	}
	public String getFIXEDFLAG() {
		return FIXEDFLAG;
	}
	public void setFIXEDFLAG(String fIXEDFLAG) {
		FIXEDFLAG = fIXEDFLAG;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public String getCMPERIOD() {
		return CMPERIOD;
	}
	public void setCMPERIOD(String cMPERIOD) {
		CMPERIOD = cMPERIOD;
	}
	public String getCMEDATE() {
		return CMEDATE;
	}
	public void setCMEDATE(String cMEDATE) {
		CMEDATE = cMEDATE;
	}
//	public String getADTXAMT() {
//		return ADTXAMT;
//	}
//	public void setADTXAMT(String aDTXAMT) {
//		ADTXAMT = aDTXAMT;
//	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getFGTRDATE() {
		return FGTRDATE;
	}
	public void setFGTRDATE(String fGTRDATE) {
		FGTRDATE = fGTRDATE;
	}
	public String getSELFFLAG() {
		return SELFFLAG;
	}
	public void setSELFFLAG(String sELFFLAG) {
		SELFFLAG = sELFFLAG;
	}
	public String getINACNO1() {
		return INACNO1;
	}
	public void setINACNO1(String iNACNO1) {
		INACNO1 = iNACNO1;
	}
	public String getFGINACNO() {
		return FGINACNO;
	}
	public void setFGINACNO(String fGINACNO) {
		FGINACNO = fGINACNO;
	}
	public String getCMSDATE() {
		return CMSDATE;
	}
	public void setCMSDATE(String cMSDATE) {
		CMSDATE = cMSDATE;
	}
	public String getSRCFUNDDESC() {
		return SRCFUNDDESC;
	}
	public void setSRCFUNDDESC(String sRCFUNDDESC) {
		SRCFUNDDESC = sRCFUNDDESC;
	}
	public String getCMTRDATE() {
		return CMTRDATE;
	}
	public void setCMTRDATE(String cMTRDATE) {
		CMTRDATE = cMTRDATE;
	}
//	public String getADCURRENCY() {
//		return ADCURRENCY;
//	}
//	public void setADCURRENCY(String aDCURRENCY) {
//		ADCURRENCY = aDCURRENCY;
//	}
//	public String getFGTXWAY() {
//		return FGTXWAY;
//	}
//	public void setFGTXWAY(String fGTXWAY) {
//		FGTXWAY = fGTXWAY;
//	}
	public String getiSeqNo() {
		return iSeqNo;
	}
	public void setiSeqNo(String iSeqNo) {
		this.iSeqNo = iSeqNo;
	}
	public String getFGTXDATE() {
		return FGTXDATE;
	}
	public void setFGTXDATE(String fGTXDATE) {
		FGTXDATE = fGTXDATE;
	}
	public String getTRANSPASSUPDATE() {
		return TRANSPASSUPDATE;
	}
	public void setTRANSPASSUPDATE(String tRANSPASSUPDATE) {
		TRANSPASSUPDATE = tRANSPASSUPDATE;
	}
	public String getPkcs7Sign() {
		return pkcs7Sign;
	}
	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}
	public String getJsondc() {
		return jsondc;
	}
	public void setJsondc(String jsondc) {
		this.jsondc = jsondc;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getCMTRMEMO() {
		return CMTRMEMO;
	}
	public void setCMTRMEMO(String cMTRMEMO) {
		CMTRMEMO = cMTRMEMO;
	}
	public String getDPMYEMAIL() {
		return DPMYEMAIL;
	}
	public void setDPMYEMAIL(String dPMYEMAIL) {
		DPMYEMAIL = dPMYEMAIL;
	}
	public String getCMTRMAIL() {
		return CMTRMAIL;
	}
	public void setCMTRMAIL(String cMTRMAIL) {
		CMTRMAIL = cMTRMAIL;
	}
	public String getCMMAILMEMO() {
		return CMMAILMEMO;
	}
	public void setCMMAILMEMO(String cMMAILMEMO) {
		CMMAILMEMO = cMMAILMEMO;
	}
	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	public String getIdgateID() {
		return idgateID;
	}
	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}
	public String getTxnID() {
		return txnID;
	}
	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}
	public String getINACNO2() {
		return INACNO2;
	}
	public void setINACNO2(String iNACNO2) {
		INACNO2 = iNACNO2;
	}
	
	
}