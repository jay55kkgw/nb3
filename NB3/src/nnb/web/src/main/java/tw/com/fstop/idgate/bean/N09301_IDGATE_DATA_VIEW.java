package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

public class N09301_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7943502484397034493L;

	@SerializedName(value = "TRANTITLE")
	private String TRANTITLE;

	@SerializedName(value = "TRANNAME")
	private String TRANNAME;
	
	@SerializedName(value = "SVACN")
	private String SVACN;
	@SerializedName(value = "ACN")
	private String ACN;
	@SerializedName(value = "AMT_06_1")
	private String AMT_06_1;
	@SerializedName(value = "AMT_16_1")
	private String AMT_16_1;
	@SerializedName(value = "AMT_26_1")
	private String AMT_26_1;
	
	@Override
	public Map<String, String> coverMap(){
		Map<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", this.TRANTITLE);
		result.put("交易類型", this.TRANNAME);
		result.put("約定扣款帳號", this.SVACN);
		result.put("黃金存摺帳號", this.ACN);
		result.put("每月投資日/金額 06日", "新臺幣 " + this.AMT_06_1 + " 元");
		result.put("每月投資日/金額 16日", "新臺幣 " + this.AMT_16_1 + " 元");
		result.put("每月投資日/金額 26日", "新臺幣 " + this.AMT_26_1 + " 元");
		return result;
	}

	public String getTRANTITLE() {
		return TRANTITLE;
	}

	public void setTRANTITLE(String tRANTITLE) {
		TRANTITLE = tRANTITLE;
	}

	public String getTRANNAME() {
		return TRANNAME;
	}

	public void setTRANNAME(String tRANNAME) {
		TRANNAME = tRANNAME;
	}

	public String getSVACN() {
		return SVACN;
	}

	public void setSVACN(String sVACN) {
		SVACN = sVACN;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	public String getAMT_06_1() {
		return AMT_06_1;
	}

	public void setAMT_06_1(String aMT_06_1) {
		AMT_06_1 = aMT_06_1;
	}

	public String getAMT_16_1() {
		return AMT_16_1;
	}

	public void setAMT_16_1(String aMT_16_1) {
		AMT_16_1 = aMT_16_1;
	}

	public String getAMT_26_1() {
		return AMT_26_1;
	}

	public void setAMT_26_1(String aMT_26_1) {
		AMT_26_1 = aMT_26_1;
	}

}
