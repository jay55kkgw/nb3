package tw.com.fstop.nnb.spring.controller;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.idgate.bean.N09001_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N09001_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N09101_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N092_IDGATE_DATA_VIEW;
import tw.com.fstop.nnb.custom.annotation.ISTXNLOG;
import tw.com.fstop.nnb.service.Gold_Passbook_Service;
import tw.com.fstop.nnb.service.IdGateService;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.DownloadUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({ SessionUtil.PD, SessionUtil.DOWNLOAD_DATALISTMAP_DATA,
		SessionUtil.PRINT_DATALISTMAP_DATA, SessionUtil.RESULT_LOCALE_DATA,
		SessionUtil.CONFIRM_LOCALE_DATA, SessionUtil.CUSIDN, SessionUtil.IDGATE_TRANSDATA })
@Controller
@RequestMapping(value = "/GOLD/PASSBOOK")
public class Gold_Passbook_Controller {

	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private Gold_Passbook_Service gold_passbook_Service;

	@Autowired		 
	IdGateService idgateservice;
	
	// 黃金存摺餘額查詢
	@RequestMapping(value = "/gold_balance_query")
	public String gold_balance_query(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {

		String target = "/error";
		BaseResult bs = null;
		log.trace("gold_balance_query~Start");
		try {
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
			}
			else{
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
				reqParam.put("CUSIDN",cusidn);
				bs=new BaseResult();
				bs = gold_passbook_Service.gold_balance_query(reqParam);
				log.debug("Bsdata >> {}", bs.getData());
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_balance_query error >> {}",e);
		}finally {
			log.debug("Bsresult >> {}", bs.getResult());
			if(bs!=null && bs.getResult()) {
			    Map<String,Object> dataMap = (Map<String, Object>) bs.getData();
				List<Map<String,String>> dataList = (List<Map<String,String>>)dataMap.get("REC");
				int count = dataList.size();
				log.trace("dataList>>{}",dataList);
				if(count == 0) {
					target = "/gold/noaccount_1";
					Map<String,String>NCdata=new HashMap<String,String>();
					//TODO i18N
					//NCdata.put("NC_title","黃金定期定額約定查詢");
					NCdata.put("NC_title","LB.W1430");
					//NCdata.put("NC_msg","您尚未申請黃金存摺帳戶，請洽往來分行辦理或線上立即申請。");
					NCdata.put("NC_msg","LB.X1772");
					//NCdata.put("NC_forwardmsg","線上立即申請");
					NCdata.put("NC_forwardmsg","LB.X0954");
					NCdata.put("NC_forward","/GOLD/APPLY/gold_account_apply");
					bs.addData("NCdata", NCdata);
					log.trace("bsData>>{}",bs.getData());
					model.addAttribute("bsdata", bs);
				}
				else {
					target = "/gold/gold_balance_query";
					SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataList);
					model.addAttribute("gold_balance_query",bs);
				}
				//SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataList);
				//model.addAttribute("gold_balance_query",bs);
				//target = "/gold/gold_balance_query";

			}else {
				bs.setPrevious("/INDEX/index"); 
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;		
	}
	// 黃金存摺餘額查詢-參考說明
	@RequestMapping(value = "/gold_balance_query_explan")
	public String gold_balance_query_explan(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {

		String target = "/error";
		log.trace("gold_balance_query~Start");
		try {
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_balance_query_explan error >> {}",e);
		}finally {
				target = "/gold/gold_balance_query_explanation";
			}
		return target;		
	}
	// 黃金存摺明細查詢查詢頁
	@RequestMapping(value = "/gold_detail_query")
	public String gold_detail_query(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("gold_detail_query Start!");
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		String urlACN = okMap.get("n190_ACN");
		String target = "/error";
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			// Get session value by session Key
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			// Get put page value
			bs = gold_passbook_Service.N920_REST(cusidn, Gold_Passbook_Service.GOLD_ACNO);
			
			bs.addData("TODAY",DateUtil.getDate("/"));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_detail_query error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				Map<String,Object>datamap=(Map<String, Object>) bs.getData();
				ArrayList<Map<String, String>> row = (ArrayList<Map<String, String>>) datamap.get("REC");
				log.debug("ROWDATA >> {}",CodeUtil.toJson(row));
				int count = row.size();
				if(count==0) {
					Map<String,String>NCdata=new HashMap<String,String>();
					//NCdata.put("NC_title","黃金存摺明細查詢");
					NCdata.put("NC_title","LB.W1442");
					//NCdata.put("NC_msg","您尚未申請黃金存摺帳戶，請洽往來分行辦理或線上立即申請。");
					NCdata.put("NC_msg","LB.X1772");
					//NCdata.put("NC_forwardmsg","線上立即申請");
					NCdata.put("NC_forwardmsg","LB.X0954");
					NCdata.put("NC_forward","/GOLD/APPLY/gold_account_apply");
					bs.addData("NCdata", NCdata);
					log.trace("bsData>>{}",bs.getData());
					model.addAttribute("bsdata", bs);
					target = "/gold/noaccount_1";
				}else {
					if(reqParam.get("FASTMENU")!=null)bs.addData("FASTMENU",reqParam.get("FASTMENU"));
					log.debug("BSDATA >> {}",CodeUtil.toJson(bs));
					bs.addData("alert_ACN", urlACN);
					model.addAttribute("gold_detail_query", bs);
					target = "/gold/gold_detail_query";
				}
				
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}		
		return target;
	}
	// 黃金存摺明細查詢結果頁
	@RequestMapping(value = "/gold_detail_query_result")
	public String gold_detail_query_result(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("gold_detail_query_result Start!");
		String target = "/error";
		BaseResult bs = new BaseResult();
		try {
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
			}
			else{
				// Get session value by session Key
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
				bs = gold_passbook_Service.gold_detail_query(cusidn, reqParam);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				log.debug("Bsdata >> {}", bs.getData());
			}
		}catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_detail_query_result error >> {}",e);
		}finally {
			log.debug("Bsresult >> {}", bs.getResult());
			if(bs!=null && bs.getResult()) {
				target = "/gold/gold_detail_query_result";
				Map<String, Object> dataMap = (Map<String, Object>) bs.getData();
				log.debug("dataMap={}", dataMap);
				List<Map<String, Object>> dataList = (List<Map<String, Object>>) dataMap.get("REC");
				log.debug("dataList={}", dataList);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataList);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, dataList);
				model.addAttribute("gold_detail_query_result", bs);
			}else {				
				bs.setPrevious("/GOLD/PASSBOOK/gold_detail_query");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	// 黃金存摺餘額查詢輸入頁直接下載
	@RequestMapping(value = "/gold_detail_query_ajaxDirectDownload")
	public String gold_detail_query_ajaxDirectDownload(HttpServletRequest request,HttpServletResponse response, @RequestBody(required = false) String serializeString, Model model) {
		log.trace("gold_detail_query_ajaxDirectDownload");
		String target = "/error";
		BaseResult bs = null;
		
		try{
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
						
			log.debug("cusidn {} ", cusidn);
			// 序列化傳入值
			Map<String, String> formMap = DownloadUtil.serializeStringToMap(serializeString);
			log.debug(ESAPIUtil.vaildLog("formMap={}" + formMap));
			for(Entry<String,String> entry : formMap.entrySet()){
			formMap.put(entry.getKey(),URLDecoder.decode(entry.getValue(),"UTF-8"));
			}
			bs =gold_passbook_Service.gold_detail_query_directDownload(cusidn, formMap);
		}catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_detail_query_ajaxDirectDownload error >> {}",e);
		}finally {
			if(bs != null && bs.getResult()){
				target = "forward:/directDownload";
				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap={}", dataMap);
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
				log.trace("rowListMap={}", rowListMap);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rowListMap);
				Map<String, Object> parameterMap = (Map<String,Object>)dataMap.get("parameterMap");
				request.setAttribute("parameterMap", parameterMap);
			}else {
				bs.setPrevious("/GOLD/PASSBOOK/gold_detail_query");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	// 黃金存摺當日價格查詢
	@ISTXNLOG(value="GD11TL")
	@RequestMapping(value = "/day_price_query")
	public String day_price_query_get_gold_list(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {

		String target = "/error";
		BaseResult bs = null;
		String systime="";
		int isGH=2;
		log.trace("day_price_query_get_gold_list");
		try {
			bs = gold_passbook_Service.getGoldList();
			systime = gold_passbook_Service.getSystemtime();
			isGH = gold_passbook_Service.isGoldHOLIDAY();
//			log.debug("branch_list size >> {}", bs.size());
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("day_price_query_get_gold_list error >> {}",e);
		}finally {
			if (bs != null && bs.getResult()) {
				log.debug("gold_list size >> {}", bs.getData());
			    Map<String,Object> dataMap = (Map<String, Object>) bs.getData();
				Map<String, Object> dataListMap = (Map<String, Object>)dataMap.get("REC");
				if(dataListMap.size() > 0) {
					List<Map<String,String>> dataList = (List<Map<String,String>>)dataListMap.get("data");
					dataMap.put("SIZE",dataList.size());
					log.debug("datalistmapp: >> {}", dataListMap);
					log.debug("dataList: >> {}", dataList);
					SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataList);
				}
				else {
					dataMap.put("SIZE",dataListMap.size());
				}
				target = "/gold/day_price_query";
				log.debug("gold_list size >> {}", bs.getData());
				log.debug("holiday_flag >> {}", isGH);
				log.debug("system_time >> {}", systime);
				model.addAttribute("gold_list", bs);
				model.addAttribute("system_time", systime);
				model.addAttribute("holiday_flag", isGH);
				model.addAttribute("gold_listJSON", CodeUtil.toJson(bs));
			}
			else{
				//新增回上一頁的路徑
				bs.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		//String type = reqParam.get("type");
		return target;		
	}



	// 黃金存摺歷史價格查詢(GD11HS)
	@RequestMapping(value = "/history_price_query")
	public String history_price_query(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		try {
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("history_price_query error >> {}",e);
		} finally {
			target = "/gold/history_price_query";
		}
		model.addAttribute("time_now", DateUtil.getDate("/") + " " + DateUtil.getTheTime(":"));
		return target;
	}

	// 黃金存摺歷史價格查詢(GD11PER、GD11LD)
	@RequestMapping(value = "/history_price_query_result")
	public String history_price_query_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("history_price_query_result>>");
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			bs = new BaseResult();
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				bs.reset();
				bs = gold_passbook_Service.history_price_query_result(okMap);
				bs.addData("QUERYTYPE", reqParam.get("QUERYTYPE"));
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("history_price_query_result error >> {}",e);
		} finally {
			if (bs != null && bs.getResult())
			{
				if( ((String)((Map<String, Object>)bs.getData()).get("QUERYTYPE")).equals("LASTDAY") ) {
					target = "/gold/history_price_query_result_LD";
				}else {
					target = "/gold/history_price_query_result_PER";
				}
				List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("result_data", bs);
			}
			else
			{
				//新增回上一頁的路徑
				bs.setPrevious("/GOLD/PASSBOOK/history_price_query");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	// 託收票據明細輸入頁直接下載
	@RequestMapping(value = "/history_price_query_ajaxDirectDownload")
	public String history_price_query_directDownload(HttpServletRequest request,HttpServletResponse response, @RequestBody(required = false) String serializeString, Model model) {
		log.trace("history_price_query_ajaxDirectDownload");
		String target = "/error";
		BaseResult bs = null;

		try {
			bs = new BaseResult();
			
			// 序列化傳入值
			Map<String, String> formMap = DownloadUtil.serializeStringToMap(serializeString);
			log.debug(ESAPIUtil.vaildLog("formMap={}" + formMap));
			for(Entry<String,String> entry : formMap.entrySet()){
			formMap.put(entry.getKey(),URLDecoder.decode(entry.getValue(),"UTF-8"));
			}
			bs =gold_passbook_Service.history_price_query_directDownload(formMap);

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("history_price_query_directDownload error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "forward:/directDownload";
				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap={}", dataMap);
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
				log.trace("rowListMap={}", rowListMap);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rowListMap);
				Map<String, Object> parameterMap = (Map<String,Object>)dataMap.get("parameterMap");
				request.setAttribute("parameterMap", parameterMap);
			} else {
				bs.setPrevious("/GOLD/PASSBOOK/history_price_query");
				model.addAttribute(BaseResult.ERROR, bs);

			}
		}
		return target;
	}

	// 預約黃金交易查詢/取消(N092)
	@RequestMapping(value = "/gold_reservation_query")
	public String gold_reservation_query(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("gold_reservation_query>>");
		try {
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			
			bs = new BaseResult();
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			bs.reset();
			bs = gold_passbook_Service.gold_reservation_query(cusidn, reqParam);
			

            //IDGATE身分
            String idgateUserFlag="N";		 
            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
            try {		 
        	    if(IdgateData==null) {
        	 	    IdgateData = new HashMap<String, Object>();
        	    }
                BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
                if(tmp.getResult()) {		 
                    idgateUserFlag="Y";                  		 
                }		 
                tmp.addData("idgateUserFlag",idgateUserFlag);		 
                IdgateData.putAll((Map<String, String>) tmp.getData());
                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
            }catch(Exception e) {		 
                log.debug("idgateUserFlag error {}",e);		 
            }		 
            model.addAttribute("idgateUserFlag",idgateUserFlag);
			           
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_reservation_query error >> {}",e);
		} finally {
			if (bs != null && bs.getResult())
			{
				if( ((Map<String, Object>) bs.getData()).get("ToGoFlag").equals("1")) {
					target = "/gold/gold_reservation_query";
				}else {
					target = "/gold/gold_reservation_query_confirm";
				}
				List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("result_data", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	

	// 預約黃金交易查詢/取消(N092)
	@RequestMapping(value = "/gold_reservation_query_detail")
	public String gold_reservation_query_detail(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		String jsondc = "";
		log.trace("gold_reservation_query_detail>>");
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		Map<String, String> result = new HashMap<String, String>();
		try {
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("pay_taxes_confirm.jsondc: " + jsondc));
			
			okMap.put("jsondc", jsondc);
			log.trace(ESAPIUtil.vaildLog("pay_taxes_confirm.okMap: {}"+ CodeUtil.toJson(okMap)));
			
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				result = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null);
			}
			if (!hasLocale) {
				result = okMap;
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, okMap);
			}

			target = "/gold/gold_reservation_query_detail";
			model.addAttribute("result_data", result);
			SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);

			// IDGATE transdata            		 
            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
    		String adopid = "N092";
    		String title = "您有一筆預約黃金交易查詢/取消待確認";
        	if(IdgateData != null) {
	            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
	            if(IdgateData.get("idgateUserFlag").equals("Y")) {
	            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
	            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
	            	result.put("TRANTITLE", "預約黃金交易查詢/取消");
	            	result.put("TRANNAME", "取消預約");
					IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N09101_IDGATE_DATA.class, N092_IDGATE_DATA_VIEW.class, result));

	                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	            }
        	}
            model.addAttribute("idgateAdopid", adopid);
            model.addAttribute("idgateTitle", title);
            
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_reservation_query_detail error >> {}",e);
		}
		return target;
	}
	

	// 預約黃金交易查詢/取消(N092)
	@RequestMapping(value = "/gold_reservation_query_result")
	public String gold_reservation_query_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("history_price_query_result>>");
		try {
			
			bs = new BaseResult();
//			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			Map<String, String> okMap = reqParam;
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				bs.reset();
                //IDgate驗證		 
                try {		 
                    if(okMap.get("FGTXWAY").equals("7")) {		 

        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N092_IDGATE");
                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
                    }		 
                }catch(Exception e) {		 
                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
                }  
				bs = gold_passbook_Service.gold_reservation_query_result(cusidn, okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			Map<String,Object>IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_reservation_query_result error >> {}",e);
		} finally {
			if (bs != null && bs.getResult())
			{
				target = "/gold/gold_reservation_query_result";
				List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("result_data", bs);
			}
			else
			{
				//新增回上一頁的路徑
				bs.setPrevious("/GOLD/PASSBOOK/gold_reservation_query");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
}