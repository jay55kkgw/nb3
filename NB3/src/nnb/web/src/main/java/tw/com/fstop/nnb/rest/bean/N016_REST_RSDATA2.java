package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N016_REST_RSDATA2 implements Serializable {

	private static final long serialVersionUID = -2739572910855200803L;
	private String LNREFNO;// 
	private String LNACCNO;//
	private String LNUPDATE;//
	private String CAPCCY;//
	private String CAPCOS;//
	private String INTCOS;//
	private String INTCCY;//
	private String CUSPYDT;
	
	public String getLNREFNO() {
		return LNREFNO;
	}
	public void setLNREFNO(String lNREFNO) {
		LNREFNO = lNREFNO;
	}
	public String getLNACCNO() {
		return LNACCNO;
	}
	public void setLNACCNO(String lNACCNO) {
		LNACCNO = lNACCNO;
	}
	public String getLNUPDATE() {
		return LNUPDATE;
	}
	public void setLNUPDATE(String lNUPDATE) {
		LNUPDATE = lNUPDATE;
	}
	public String getCAPCCY() {
		return CAPCCY;
	}
	public void setCAPCCY(String cAPCCY) {
		CAPCCY = cAPCCY;
	}
	public String getCAPCOS() {
		return CAPCOS;
	}
	public void setCAPCOS(String cAPCOS) {
		CAPCOS = cAPCOS;
	}
	public String getINTCOS() {
		return INTCOS;
	}
	public void setINTCOS(String iNTCOS) {
		INTCOS = iNTCOS;
	}
	public String getINTCCY() {
		return INTCCY;
	}
	public void setINTCCY(String iNTCCY) {
		INTCCY = iNTCCY;
	}
	public String getCUSPYDT() {
		return CUSPYDT;
	}
	public void setCUSPYDT(String cUSPYDT) {
		CUSPYDT = cUSPYDT;
	}

}
