package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N850A_REST_RS extends BaseRestBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8035636598012411463L;
	
	private String CMQTIME;
	private LinkedList<N850A_REST_RSDATA> REC;
	
	
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public LinkedList<N850A_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<N850A_REST_RSDATA> rEC) {
		REC = rEC;
	}
	
	
	
	
	

}
