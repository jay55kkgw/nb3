package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N074_REST_RS extends BaseRestBean implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3705466158144874584L;
	
	private String OFFSET;// 空白

	private String HEADER;// HEADER

	private String SYNC;// Sync.Check Item

	private String OUTACN;// 轉出帳號

	private String FDPACC;// 存單種類

	private String DUEDATE;// 到期日

	private String INTMTH;// 計息方式

	private String ITR;// 利率

	private String O_TOTBAL;// 轉出帳號帳上餘額

	private String O_AVLBAL;// 轉出帳號可用餘額

	private String AMOUNT;// 轉帳金額

	private String DATE;// 日期YYYMMDD

	private String TIME;// 時間HHMMSS

	private String INTSACN;// 轉入帳號

	private String FDPNUM;// 存單號碼

	private String SDT;// 起存日

	private String CODE;// 是否續存（’0’不續存，’1’續存）
	
    private String CMTXTIME;//交易時間

	private String MAC;// MAC

	public String getOFFSET()
	{
		return OFFSET;
	}

	public void setOFFSET(String oFFSET)
	{
		OFFSET = oFFSET;
	}

	public String getHEADER()
	{
		return HEADER;
	}

	public void setHEADER(String hEADER)
	{
		HEADER = hEADER;
	}

	public String getSYNC()
	{
		return SYNC;
	}

	public void setSYNC(String sYNC)
	{
		SYNC = sYNC;
	}

	public String getOUTACN()
	{
		return OUTACN;
	}

	public void setOUTACN(String oUTACN)
	{
		OUTACN = oUTACN;
	}

	public String getFDPACC()
	{
		return FDPACC;
	}

	public void setFDPACC(String fDPACC)
	{
		FDPACC = fDPACC;
	}

	public String getDUEDATE()
	{
		return DUEDATE;
	}

	public void setDUEDATE(String dUEDATE)
	{
		DUEDATE = dUEDATE;
	}

	public String getINTMTH()
	{
		return INTMTH;
	}

	public void setINTMTH(String iNTMTH)
	{
		INTMTH = iNTMTH;
	}

	public String getITR()
	{
		return ITR;
	}

	public void setITR(String iTR)
	{
		ITR = iTR;
	}

	public String getO_TOTBAL()
	{
		return O_TOTBAL;
	}

	public void setO_TOTBAL(String o_TOTBAL)
	{
		O_TOTBAL = o_TOTBAL;
	}

	public String getO_AVLBAL()
	{
		return O_AVLBAL;
	}

	public void setO_AVLBAL(String o_AVLBAL)
	{
		O_AVLBAL = o_AVLBAL;
	}

	public String getAMOUNT()
	{
		return AMOUNT;
	}

	public void setAMOUNT(String aMOUNT)
	{
		AMOUNT = aMOUNT;
	}

	public String getDATE()
	{
		return DATE;
	}

	public void setDATE(String dATE)
	{
		DATE = dATE;
	}

	public String getTIME()
	{
		return TIME;
	}

	public void setTIME(String tIME)
	{
		TIME = tIME;
	}

	public String getINTSACN()
	{
		return INTSACN;
	}

	public void setINTSACN(String iNTSACN)
	{
		INTSACN = iNTSACN;
	}

	public String getFDPNUM()
	{
		return FDPNUM;
	}

	public void setFDPNUM(String fDPNUM)
	{
		FDPNUM = fDPNUM;
	}

	public String getSDT()
	{
		return SDT;
	}

	public void setSDT(String sDT)
	{
		SDT = sDT;
	}

	public String getCODE()
	{
		return CODE;
	}

	public void setCODE(String cODE)
	{
		CODE = cODE;
	}

	public String getMAC()
	{
		return MAC;
	}

	public void setMAC(String mAC)
	{
		MAC = mAC;
	}

	public String getCMTXTIME()
	{
		return CMTXTIME;
	}

	public void setCMTXTIME(String cMTXTIME)
	{
		CMTXTIME = cMTXTIME;
	}
	
}
