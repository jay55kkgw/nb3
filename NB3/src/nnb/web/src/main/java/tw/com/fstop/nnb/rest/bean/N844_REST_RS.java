package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N844_REST_RS extends BaseRestBean implements Serializable {

	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7288492644844877278L;
	
	private String ABEND;
	private String CMQTIME;
	private String USERDATA;
	private String REC_NO;
	private String __OCCURS;
	private LinkedList<N844_REST_RSDATA> REC;
	
	public String getABEND() {
		return ABEND;
	}
	public void setABEND(String aBEND) {
		ABEND = aBEND;
	}
	public String getUSERDATA() {
		return USERDATA;
	}
	public void setUSERDATA(String uSERDATA) {
		USERDATA = uSERDATA;
	}
	public String getREC_NO() {
		return REC_NO;
	}
	public void setREC_NO(String rEC_NO) {
		REC_NO = rEC_NO;
	}
	public LinkedList<N844_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<N844_REST_RSDATA> rEC) {
		REC = rEC;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String get__OCCURS() {
		return __OCCURS;
	}
	public void set__OCCURS(String __OCCURS) {
		this.__OCCURS = __OCCURS;
	}
}
