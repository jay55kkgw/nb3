
/*
 * Copyright (c) 2017, FSTOP, Inc. All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tw.com.fstop.web;

import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public interface FilterProcessor
{
    void process(ServletRequest request, ServletResponse response) throws ProcessorException;
}
