package tw.com.fstop.nnb.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.rest.bean.N366_1_REST_RQ;
import tw.com.fstop.nnb.rest.bean.N366_REST_RQ;
import tw.com.fstop.nnb.rest.bean.N366_REST_RS;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmMsgCodeDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;

@Service
public class Account_Closing_Service extends Base_Service
{
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private DaoService daoService;

	@Autowired
	AdmMsgCodeDao admMsgCodeDao;

	@Autowired
	private I18n i18n;

	/**
	 * 依type取得可結清帳號、轉入帳號
	 * 
	 * @param cusidn
	 * @param type
	 * @return
	 */
	public BaseResult getAccounts(String cusidn, String type)
	{
		log.debug("getAccounts type >> {}", type);
		BaseResult bs = new BaseResult();
		try
		{
			switch (type)
			{
				case CLOSING_TW_ACCOUNT:
					// 取得可結清帳號
					bs = N920_REST(cusidn, CLOSING_TW_ACCOUNT);
					break;

				case ACNO:
					// 取得轉入帳號
					bs = N920_REST(cusidn, ACNO);
					break;

				default:
					log.error("輸入TYPE錯誤");
			}

			log.debug("getAccounts >> {}", CodeUtil.toJson(bs));

			Map<String, Object> bsData = (Map) bs.getData();
			List<Map> rec = (List) bsData.get("REC");
			if (rec.isEmpty())
			{
				bs.setResult(false);
				// 查無可結清帳號，回應Z999
				String msgCode = "Z999";
				bs.setMsgCode(msgCode);
				String message = admMsgCodeDao.errorMsg(msgCode);
				bs.setMessage(message);
			}
		}
		catch (Exception e)
		{
			log.error("getAccounts error");
		}
		return bs;
	}

	/**
	 * N366 取得欲結清帳號的餘額資訊
	 * 
	 * @param cusidn
	 * @param type
	 * @return
	 */
	public BaseResult getSettleAcnBalance(String cusidn, String acn)
	{
		log.debug(ESAPIUtil.vaildLog("getSettleAccountInfo ACN >> " + acn)); 
		BaseResult bs = new BaseResult();
		try
		{
			N366_REST_RQ rq = new N366_REST_RQ();
			rq.setACN(acn);
			rq.setCUSIDN(cusidn);
			rq.setDATE(DateUtil.getTWDate(""));
			rq.setTIME(DateUtil.getTheTime(""));
			rq.setADOPID("N366_1");

			bs = N366_REST(rq);
		}
		catch (Exception e)
		{
			log.error("getSettleAccountInfo error");
		}
		return bs;
	}

	/**
	 * 線上申請新臺幣存款帳戶結清銷戶-交易
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult n366_1(String cusidn, Map<String, String> reqParam)
	{
		log.debug(ESAPIUtil.vaildLog("n366_1 reqParam >> " + reqParam)); 
		BaseResult bs = new BaseResult();
		try
		{
			String json = CodeUtil.toJson(reqParam);
			N366_1_REST_RQ rq = CodeUtil.fromJson(json, N366_1_REST_RQ.class);
			//企業戶、法人壓MAC統編不滿10位後面要補0
			String _cusidn = (cusidn + "0000000000").substring(0, 10);
			log.debug(ESAPIUtil.vaildLog("ID GET>>>"+_cusidn));
			
			String newStr = "";
		    for ( int counter = 0; counter < _cusidn.length(); counter++ ) {
		      if (_cusidn.charAt(counter) >= 'A' && _cusidn.charAt(counter) <= 'Z' ) {
		        newStr = newStr + "0";
		      } else if ( _cusidn.charAt(counter) >= 'a' && _cusidn.charAt(counter) <= 'z' ) {
		        newStr = newStr + "0";
		      } else
		        newStr = newStr + _cusidn.substring(counter, counter + 1);
		    }
		    log.debug(ESAPIUtil.vaildLog("_CUSIDN>>>"+newStr));
			rq.set_CUSIDN(newStr);
			rq.setCUSIDN(cusidn);
			rq.setADOPID("N366");

			String accno = rq.getACNNO();
			if (accno.length() > 11)
			{
				// 去前端0
				accno = accno.substring(accno.length() - 11);
			}
			rq.setCHIP_ACN(accno);

			bs = N366_1_REST(rq);
		}
		catch (Exception e)
		{
			log.error("n366_1 error");
		}
		return bs;
	}

	public BaseResult closing_fcy_account_step1(String cusidn)
	{
		BaseResult bs = new BaseResult();
		try
		{
			bs = N920_REST(cusidn, Base_Service.CLOSING_FCY_ACCOUNT);
			// 後製把REC 裡面的資料轉成下拉選單所要的格式
			if (bs != null && bs.getResult())
			{
				Map<String, Object> b = (Map<String, Object>) bs.getData();
				List outAcnoList = (List<Map<String, String>>) b.get("REC");
				if (outAcnoList.size() == 0)
				{
					//轉出帳號無任何資料
					bs.setMessage("Z999", i18n.getMsg("LB.X1836"));
					bs.setResult(Boolean.FALSE);
					log.info("call bsN920 bs erro >>>{}", bs.getData());
				}

			}
		}
		catch (Exception e)
		{
			log.error("closing_fcy_account_step1 {}", e);
		}
		log.debug("closing_fcy_account_step1 >>>>>>>>>>{} ", bs.getData());
		return bs;
	}

	public BaseResult closing_fcy_account_step2(Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			bs = F015_REST(reqParam);
			// 後製把裡面的資料轉成所要的格式
			if (bs != null && bs.getResult())
			{

				// 電文回應
				Map<String, Object> callData = (Map<String, Object>) bs.getData();
				// 線上活存幣別結清-結清帳號- 外匯活存帳號
				callData.put("FYACN", reqParam.get("FYACN"));
				// 線上活存幣別結清-幣別
				String OUT_CRY = reqParam.get("CRY") == null ? "" : reqParam.get("CRY");
				callData.put("OUT_CRY", OUT_CRY);
				// 後製電文回應
				//// 使用匯率
				String ROE = callData.get("ROE") == null ? "" : (String) callData.get("ROE");
				callData.put("ROE", ROE);
				//// 結清金額
				String AMTCLS = callData.get("AMTCLS") == null ? "" : (String) callData.get("AMTCLS");
				callData.put("AMTCLS", AMTCLS);
				//// // 臺幣補充保費
				String NTNHITAX = callData.get("NTNHITAX") == null ? "" : (String) callData.get("NTNHITAX");
				callData.put("NTNHITAX", NTNHITAX);
				//// // 免扣取對象
				String NHICOD = callData.get("NHICOD") == null ? "" : (String) callData.get("NHICOD");
				callData.put("NHICOD", NHICOD);
				//// // 臺幣利息
				String NTINT = callData.get("NTINT") == null ? "" : (String) callData.get("NTINT");
				callData.put("NTINT", NTINT);
				//// // 臺幣稅款
				String NTTAX = callData.get("NTTAX") == null ? "" : (String) callData.get("NTTAX");
				callData.put("NTTAX", NTTAX);
				//
				String SFXGLSTNO = "";
				callData.put("SFXGLSTNO", SFXGLSTNO);
				// 利息
				String INT = callData.get("INT") == null ? "" : (String) callData.get("INT");
				callData.put("INT", INT);
				// 稅款
				String FYTAX = callData.get("FYTAX") == null ? "" : (String) callData.get("FYTAX");
				callData.put("FYTAX", FYTAX);
				// 補充保險費
				String FYNHITAX = callData.get("FYNHITAX") == null ? "" : (String) callData.get("FYNHITAX");
				callData.put("FYNHITAX", FYNHITAX);
				//// // 稅後本息
				String PAIAFTX = callData.get("PAIAFTX") == null ? "" : (String) callData.get("PAIAFTX");
				callData.put("PAIAFTX", PAIAFTX);
				// 轉出金額
				String AMTDHID = PAIAFTX;
				callData.put("AMTDHID", AMTDHID);
				//
				float tempAMTDHID = Float.parseFloat(NumericUtil.deleteComma(NumericUtil.formatNumberString(AMTDHID, 2)));
				callData.put("tempAMTDHID", tempAMTDHID);

				// 頁面顯示_使用匯率
				callData.put("display_ROE", NumericUtil.formatNumberString(ROE, 7));
				// 頁面顯示_轉出金額 OR 頁面顯示_稅後本息
				callData.put("display_AMTDHID", NumericUtil.formatNumberString(AMTDHID, 2));
				// 頁面顯示_金額
				callData.put("display_AMTCLS", NumericUtil.formatNumberString(String.valueOf(callData.get("AMTCLS")), 2));
				// 頁面顯示_利息
				callData.put("display_INT", NumericUtil.formatNumberString(String.valueOf(callData.get("INT")), 2));
				// 頁面顯示_稅款
				callData.put("display_FYTAX", NumericUtil.formatNumberString(String.valueOf(callData.get("FYTAX")), 2));
				// 頁面顯示_補充保險費
				callData.put("display_FYNHITAX", NumericUtil.formatNumberString(String.valueOf(callData.get("FYNHITAX")), 2));
				//
				bs.addData("previousPageJson", CodeUtil.toJson(callData));
			}
		}
		catch (Exception e)
		{
			log.error("closing_fcy_account_step2 {}", e);
		}
		log.debug("closing_fcy_account_step2 out Data >>>>>>>>>>{}", bs.getData());
		return bs;
	}

	/**
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult closing_fcy_account_step3(String cusidn, Map<String, String> reqParam)
	{
		log.debug("closing_fcy_account_step3 start ~~~~");
		BaseResult bs = new BaseResult();
		BaseResult bsagreeAcnos = new BaseResult();
		BaseResult bsN920 = new BaseResult();
		List<Map<String, String>> agreeAcnosList = new LinkedList<>();
		List<Map<String, String>> acnos1 = new LinkedList<>();
		try
		{

			boolean agreeOnly = true;
			// 轉入台幣帳號
			bsagreeAcnos = daoService.getAgreeAcnoList(cusidn, CLOSING_FCY_ACCOUNT_AGREEACNOS, agreeOnly);
			Map<String, Object> bsagreeAcnosData = (Map<String, Object>) bsagreeAcnos.getData();
			ArrayList<Map<String, String>> bsagreeAcnosList = (ArrayList<Map<String, String>>) bsagreeAcnosData.get("REC");
			log.debug("bsagreeAcnosList >>>>>{}", bsagreeAcnosList);
			// 轉入外幣帳號
			bs = daoService.getAgreeAcnoList(cusidn, CLOSING_FCY_ACCOUNT_ACNOS1, agreeOnly);
			Map<String, Object> callData = (Map<String, Object>) bs.getData();
			ArrayList<Map<String, String>> bsacnos1List = (ArrayList<Map<String, String>>) callData.get("REC");
			log.debug("bsacnos1List >>>>>{}", bsacnos1List);
			// 檢查是否有可結清帳號
			if (bsagreeAcnosList.size() == 0 && bsacnos1List.size() == 0)
			{
				bs.setMessage("Z999", i18n.getMsg("LB.X1836"));
				bs.setResult(Boolean.FALSE);
				log.info("call  bsagreeAcnos erro >>>{}", bsagreeAcnos.getData());
				log.info("call  bs erro >>>{}", bs.getData());
			}
			else
			{
				// 轉入台幣帳號
				for (Map<String, String> bsagreeAcnosListMap : bsagreeAcnosList)
				{
					String value = bsagreeAcnosListMap.get("VALUE");
					String acn = "";
					String bnkcod = "";
					String acnType = "";
					if (value != "#")
					{
						Map<String, String> valueMap = CodeUtil.fromJson(value, Map.class);
						acn = valueMap.get("ACN");
						bnkcod = valueMap.get("BNKCOD");
						acnType = "";
						if (acn.length() >= 2)
							acnType = acn.substring(0, 2);
						if (bnkcod.equals("050"))
						{
							if (acnType.equals("98") || acnType.equals("99"))
								continue;
						}
						//
						agreeAcnosList.add(valueMap);
					}
					else if (bsagreeAcnosListMap.get("TEXT").indexOf(i18n.getMsg("LB.Designated_account")) >= 0)
					{
						continue;
					}

				}
				log.debug("agreeAcnosList out>>>>>>>>>>>>>{}", agreeAcnosList);
				// 轉入外幣帳號
				for (Map<String, String> bsMap : bsacnos1List)
				{
					String value = bsMap.get("VALUE");
					String acn1 = "";
					String bnkcod1 = "";
					if (value != "#")
					{
						Map<String, String> valueMap = new HashMap<String, String>();
						valueMap = CodeUtil.fromJson(value, Map.class);
						acn1 = valueMap.get("ACN");
						bnkcod1 = valueMap.get("BNKCOD");
						if (!bnkcod1.equals("050"))
							continue;
						if (bnkcod1.equals("050")
								&& (acn1.substring(3, 5).equals("51") || acn1.substring(3, 5).equals("54")))
							continue;
						if (bnkcod1.equals("050")
								&& (!acn1.substring(3, 4).equals("5") && !acn1.substring(3, 5).equals("12")
										&& !acn1.substring(3, 5).equals("60") && !acn1.substring(3, 5).equals("61")
										&& !acn1.substring(3, 5).equals("62") && !acn1.substring(3, 5).equals("64")))
							continue;
						acnos1.add(valueMap);
					}
					else if (bsMap.get("TEXT").indexOf(i18n.getMsg("LB.Designated_account")) >= 0)
					{
						continue;
					}

				}
				log.debug("acnos1 out >>>>>>>>>>>{}", acnos1);

				// CALL N920電文 取得轉出帳號
				String str_CUSTYPE = "";
				String name = "";
				bsN920 = N920_REST(cusidn, Base_Service.CLOSING_FCY_ACCOUNT_IN);
				// 後製把REC 裡面的資料轉成下拉選單所要的格式
				if (bsN920 != null && bsN920.getResult())
				{
					Map<String, Object> callDataN920 = (Map<String, Object>) bsN920.getData();
					String str_CUSTIDF = (String) callDataN920.get("CUSTIDF");
					if (str_CUSTIDF.equals("1"))
					{
						str_CUSTYPE = "1";
					}
					else if (str_CUSTIDF.equals("2"))
					{
						str_CUSTYPE = "2";
					}
					else if (str_CUSTIDF.equals("3") || str_CUSTIDF.equals("5"))
					{
						str_CUSTYPE = "3";
					}
					else if (str_CUSTIDF.equals("4") || str_CUSTIDF.equals("6"))
					{
						str_CUSTYPE = "4";
					}
					name = (String) callDataN920.get("NAME");
					// OutAcno
					// outAcnoList = (List<Map<String, String>>) callDataN920.get("REC");
				}
				reqParam = CodeUtil.fromJson(reqParam.get("previousPageJson"), Map.class);
				reqParam.put("str_CUSTYPE", str_CUSTYPE);
				reqParam.put("name", name);
				//
				String AMTDHID = reqParam.get("PAIAFTX") == null ? "0" : reqParam.get("PAIAFTX");
				float tempAMTDHID = Float.parseFloat(NumericUtil.deleteComma(NumericUtil.formatNumberString(AMTDHID, 2)));
				log.debug("closing_fcy_account_step3  tempAMTDHID: {}", tempAMTDHID);

				bs.addAllData(reqParam);
				bs.addData("previousPageJson", CodeUtil.toJson(reqParam));
				bs.addData("agreeAcnosList", agreeAcnosList);
				bs.addData("acnos1", acnos1);
				bs.addData("tempAMTDHID", tempAMTDHID);
			}
			log.debug(" Account_Closing_Service closing_fcy_account_step3 getData()>> {}", bs.getData());
		}
		catch (Exception e)
		{
			log.error("closing_fcy_account_step3 {}", e);
		}
		return bs;
	}

	/**
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult closing_fcy_account_confirm(Map<String, String> reqParam)
	{
		log.debug("closing_fcy_account_confirm start ~~~~");
		BaseResult bs = new BaseResult();
		try
		{
			Map<String, String> previousPageJsonMap = CodeUtil.fromJson(reqParam.get("previousPageJson"), Map.class);

			reqParam.remove("previousPageJson");

			previousPageJsonMap.putAll(reqParam);

			bs.addAllData(previousPageJsonMap);
			bs.addData("previousPageJson", CodeUtil.toJson(previousPageJsonMap));
			bs.setResult(true);
		}
		catch (Exception e)
		{
			log.error("closing_fcy_account_confirm {}", e);
		}
		return bs;
	}

	/**
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult closing_fcy_account_step4_r(String cusidn, Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			Map<String, String> previousPageJsonMap = CodeUtil.fromJson(reqParam.get("previousPageJson"), Map.class);

			reqParam.remove("previousPageJson");
			//
			previousPageJsonMap.putAll(reqParam);

			bs = F017R_REST(previousPageJsonMap);
			// 後製把裡面的資料轉成所要的格式
			if (bs != null && bs.getResult())
			{
				// 電文回應
				Map<String, Object> callData = (Map<String, Object>) bs.getData();
				// 轉出金額 -幣別
				String str_Curr = String.valueOf(callData.get("OUT_CRY"));
				// 轉出金額
				String str_OutAmt = String.valueOf(callData.get("CURAMT"));
				if (!str_OutAmt.equals(""))
				{
					int i_Digit = 0;
					if (str_OutAmt.indexOf(".") != -1)
					{
						i_Digit = 2;
					}
					str_OutAmt = NumericUtil.formatNumberStringByCcy(str_OutAmt, i_Digit, str_Curr);
				}
				// 轉入金額 -幣別
				String CURCODE = String.valueOf(callData.get("IN_CRY"));
				// 轉入金額
				String str_InAmt = String.valueOf(callData.get("ATRAMT"));
				if (!str_InAmt.equals(""))
				{
					int i_Digit = 0;
					if (str_InAmt.indexOf(".") != -1)
						i_Digit = 2;
					str_InAmt = NumericUtil.formatNumberString(NumericUtil.deleteComma(NumericUtil.formatNumberStringByCcy(str_InAmt, i_Digit, CURCODE)), 0);
				} // end if
				String display_CMTRDATE = DateUtil.formatDate(String.valueOf(callData.get("CMTRDATE")));

				callData.putAll(previousPageJsonMap);
				callData.put("display_CMTRDATE", display_CMTRDATE);
				callData.put("str_InAmt", str_InAmt);
				callData.put("str_OutAmt", str_OutAmt);
				callData.put("previousPageJson", CodeUtil.toJson(callData));
				log.debug(ESAPIUtil.vaildLog("closing_fcy_account_step4R callData >> " + callData)); 
			}
		}
		catch (Exception e)
		{
			log.error("closing_fcy_account_step4_r {}", e);
		}

		return bs;
	}

	/**
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult closing_fcy_account_confirm_r(Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			Map<String, String> previousPageJsonMap = CodeUtil.fromJson(reqParam.get("previousPageJson"), Map.class);

			reqParam.remove("previousPageJson");

			previousPageJsonMap.putAll(reqParam);

			bs.addAllData(previousPageJsonMap);
			bs.addData("previousPageJson", CodeUtil.toJson(previousPageJsonMap));
			bs.setResult(true);
		}
		catch (Exception e)
		{
			log.error("closing_fcy_account_confirm_r {}", e);
		}
		return bs;
	}

	/**
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult closing_fcy_account_result(String cusidn, Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			String txid = reqParam.get("TXID");

			Map<String, String> previousPageJsonMap = CodeUtil.fromJson(reqParam.get("previousPageJson"), Map.class);
			reqParam.remove("previousPageJson");

			//
			previousPageJsonMap.putAll(reqParam);
			previousPageJsonMap.put("CUSIDN", cusidn);

			if ("F017T".equals(txid))
			{
				bs = F017T_REST(previousPageJsonMap);
			}
			else
			{
				bs = F017_REST(previousPageJsonMap);
			}

			// 後製把裡面的資料轉成所要的格式
			if (bs != null && bs.getResult())
			{
				// 電文回應
				Map<String, Object> callData = (Map<String, Object>) bs.getData();
				// // 轉出帳號
				callData.put("FYACN", previousPageJsonMap.get("FYACN"));
				// // 轉入帳號
				callData.put("TSFAN", previousPageJsonMap.get("TSFAN"));
				// // 轉出金額-幣別
				String CRY = previousPageJsonMap.get("CRY");
				callData.put("CRY", CRY);
				// // 轉入金額-幣別
				String CURCODE = previousPageJsonMap.get("CURCODE");
				callData.put("CURCODE", CURCODE);
				// 轉出金額
				String TDBAMT = NumericUtil.formatNumberString(String.valueOf(callData.get("TDBAMT")), 2);
				TDBAMT = NumericUtil.formatNumberStringByCcy(TDBAMT, 2, CRY);
				callData.put("TDBAMT", TDBAMT);
				// 轉入金額
				String ATRAMT = NumericUtil.formatNumberStringByCcy(String.valueOf(callData.get("ATRAMT")), 2, CURCODE);
				callData.put("ATRAMT", ATRAMT);
				// 使用匯率
				String CHGROE = NumericUtil.formatNumberString(String.valueOf(callData.get("CHGROE")), 6);
				callData.put("CHGROE", CHGROE);
				// 結清帳面餘額
				String CURAMT = NumericUtil.formatNumberString(String.valueOf(callData.get("CURAMT")), 2);
				callData.put("CURAMT", CURAMT);

			}

			log.debug("closing_fcy_account_result >>>>>>>>>>>>>>>{}", bs.getData());

		}
		catch (Exception e)
		{
			log.error("closing_fcy_account_step1 {}", e);
		}
		return bs;
	}

	/**
	 * 由session取得Ikey參數
	 * 
	 * @param model
	 * @return
	 */
	public Map<String, String> getIkeyParams(Model model, N366_REST_RS rs, String inAcn)
	{
		Map<String, String> result = new HashMap<>();
		String json = CodeUtil.toJson(rs);
		result = CodeUtil.fromJson(json, Map.class);

		String HKCODE = SessionUtil.getAttribute(model, SessionUtil.HKCODE, null) == null ? ""
				: SessionUtil.getAttribute(model, SessionUtil.HKCODE, null).toString();
		String MBOPNTM = SessionUtil.getAttribute(model, SessionUtil.MBOPNTM, null) == null ? ""
				: SessionUtil.getAttribute(model, SessionUtil.MBOPNTM, null).toString();
		String MBOPNDT = SessionUtil.getAttribute(model, SessionUtil.MBOPNDT, null) == null ? ""
				: SessionUtil.getAttribute(model, SessionUtil.MBOPNDT, null).toString();
		String MBSTAT = SessionUtil.getAttribute(model, SessionUtil.MBSTAT, null) == null ? ""
				: SessionUtil.getAttribute(model, SessionUtil.MBSTAT, null).toString();
		String LOGINTM = SessionUtil.getAttribute(model, SessionUtil.LOGINTM, null) == null ? ""
				: SessionUtil.getAttribute(model, SessionUtil.LOGINTM, null).toString();
		String LOGINDT = SessionUtil.getAttribute(model, SessionUtil.LOGINDT, null) == null ? ""
				: SessionUtil.getAttribute(model, SessionUtil.LOGINDT, null).toString();
		String XMLCOD = SessionUtil.getAttribute(model, SessionUtil.XMLCOD, null) == null ? ""
				: SessionUtil.getAttribute(model, SessionUtil.XMLCOD, null).toString();
		String USERIP = SessionUtil.getAttribute(model, SessionUtil.USERIP, null) == null ? ""
				: SessionUtil.getAttribute(model, SessionUtil.USERIP, null).toString();
		String USERID = SessionUtil.getAttribute(model, SessionUtil.USERID, null) == null ? ""
				: SessionUtil.getAttribute(model, SessionUtil.USERID, null).toString();
		String STAFF = SessionUtil.getAttribute(model, SessionUtil.STAFF, null) == null ? ""
				: SessionUtil.getAttribute(model, SessionUtil.STAFF, null).toString();
		String DPUSERNAME = SessionUtil.getAttribute(model, SessionUtil.DPUSERNAME, null) == null ? ""
				: SessionUtil.getAttribute(model, SessionUtil.DPUSERNAME, null).toString();
		String DPMYEMAIL = SessionUtil.getAttribute(model, SessionUtil.DPMYEMAIL, null) == null ? ""
				: SessionUtil.getAttribute(model, SessionUtil.DPMYEMAIL, null).toString();
		String AUTHORITY = SessionUtil.getAttribute(model, SessionUtil.AUTHORITY, null) == null ? ""
				: SessionUtil.getAttribute(model, SessionUtil.AUTHORITY, null).toString();
		result.put("HKCODE", HKCODE);
		result.put("MBOPNTM", MBOPNTM);
		result.put("MBOPNDT", MBOPNDT);
		result.put("MBSTAT", MBSTAT);
		result.put("LOGINTM", LOGINTM);
		result.put("LOGINDT", LOGINDT);
		result.put("XMLCOD", XMLCOD);
		result.put("USERIP", USERIP);
		result.put("USERID", USERID);
		result.put("STAFF", STAFF);
		result.put("DPUSERNAME", DPUSERNAME);
		result.put("DPMYEMAIL", DPMYEMAIL);
		result.put("AUTHORITY", AUTHORITY);
		result.put("INACN", inAcn);
		return result;
	}

	/**
	 * 清除暫存session的資料，不可清除使用者身份資料
	 * 
	 * @param model
	 */
	public void cleanSession(Model model)
	{
		// 清除切換語系
		SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null);
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null);
		// 清除回上一頁
		SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, null);
		// 清除print
		SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, null);
		// 換頁暫存資料
		SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, null);
		// 線上申請新臺幣存款帳戶結清銷戶暫存資料
		SessionUtil.addAttribute(model, SessionUtil.CLOSING_TW_ACCOUNT, null);
	}

	/**
	 * 檢查TxToken
	 * 
	 * @param pageTxToken 頁面TxToken
	 * @param SessionFinshToken
	 * @return false 表示有問題
	 */
	public BaseResult validateToken(String pageTxToken, String SessionFinshToken)
	{
		BaseResult bs = new BaseResult();
		try
		{
			if (pageTxToken == null)
			{
				log.debug(" reqTxToken==null");
				bs.setMessage("FE002", "reqTxToken空白");
				log.debug("validateToken getResult:{}", bs.getMsgCode());
				log.debug("validateToken getResult:{}", bs.getMessage());
			}
			else if (StrUtil.isNotEmpty(SessionFinshToken) && SessionFinshToken.equals(pageTxToken))
			{
				log.debug("validateToken2 getResult:{}", bs.getMsgCode());
				log.debug("validateToken2 getResult:{}", bs.getMessage());
				log.debug(" pagToken SessionFinshToken &&  一樣 重複交易");
				bs.setMessage("FE002", i18n.getMsg("LB.X1804"));//重複交易
			}
			else
			{
				bs.setResult(true);
			}
		}
		catch (Exception e)
		{
			log.error("getTransferToken Error", e);
		}
		return bs;
	}

}
