package tw.com.fstop.nnb.service;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import fstop.orm.po.ADMCURRENCY;
import fstop.orm.po.TXNBONDDATA;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmCurrencyDao;
import tw.com.fstop.tbb.nnb.dao.AdmHolidayDao;
import tw.com.fstop.tbb.nnb.dao.SysParamDataDao;
import tw.com.fstop.tbb.nnb.dao.TxnBondDataDao;
import tw.com.fstop.util.NumericUtil;

/**
 * 海外債贖回 Service
 */
@Service
public class Bond_Sell_Service extends Base_Service{
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	
	@Autowired
	private AdmHolidayDao admHolidayDao;
	
	@Autowired
	private SysParamDataDao sysParamDataDao;
	
	@Autowired
	private AdmCurrencyDao admCurrencyDao;
	
	@Autowired
	private TxnBondDataDao txnBondDataDao;
	
	@Autowired
	private I18n i18n;
	

	public BaseResult bond_sell_query_page(BaseResult bs) {
		Map<String,Object> bsData = (Map<String,Object>)bs.getData();
		
		List<Map<String, String>> recData = (List<Map<String, String>>) bsData.get("REC");
		
		String locale = LocaleContextHolder.getLocale().toString();
		
		
		for( Map<String, String> eachData:recData) {
			
			ADMCURRENCY crydata= admCurrencyDao.getByPK( eachData.get("O04") );
			
			if(crydata != null) {
				if(locale.equals("zh_CN")) {
					eachData.put("O04_FMT", crydata.getADCCYCHSNAME());
				}
				else if(locale.equals("en")) {
					eachData.put("O04_FMT", crydata.getADCCYENGNAME());
				}
				else {
					eachData.put("O04_FMT", crydata.getADCCYNAME());
				}
			}
			
			eachData.put("O06_FMT",NumericUtil.formatNumberString(eachData.get("O06"), 2));
			eachData.put("O07_FMT",NumericUtil.formatNumberString(eachData.get("O07"), 0));
			eachData.put("O08_FMT",NumericUtil.formatNumberString(eachData.get("O08"), 4));
			
			if(Integer.parseInt(eachData.get("O08")) == 0 ) {
				//尚未報價 >> 尚未報價
				eachData.put("KIND","1");
			}else if(Integer.parseInt(eachData.get("O08")) > 0 && "Y".equals(eachData.get("O09"))) {
				//可部贖 >> 全部贖回/部分贖回
				eachData.put("KIND","2");
			}else if(Integer.parseInt(eachData.get("O08")) > 0 && "".equals(eachData.get("O09"))){
				//不可部贖 >> 全部贖回
				eachData.put("KIND","3");
			}
		}
		
		
		return bs;
	}
	
	
	public BaseResult bond_sell_input_page(BaseResult bs) {
		
		Map<String,Object> bsData = (Map<String,Object>)bs.getData();
		
		String locale = LocaleContextHolder.getLocale().toString();
		
		TXNBONDDATA selected_bond =  null;
		selected_bond = txnBondDataDao.getBondDateById((String) bsData.get("O02"));
		setCRY_SHOW(selected_bond,locale);
		
		if("2".equals((String)bsData.get("SELLWAY"))) {
			String MINSPRICE = selected_bond.getMINSPRICE();
			String PROGRESSIVESPRICE = selected_bond.getPROGRESSIVESPRICE();
			bsData.put("MINSPRICE", MINSPRICE);
			bsData.put("PROGRESSIVESPRICE", PROGRESSIVESPRICE);
			bsData.put("MINSPRICE_FMT", NumericUtil.formatNumberString(MINSPRICE, 0));
			bsData.put("PROGRESSIVESPRICE_FMT", NumericUtil.formatNumberString(PROGRESSIVESPRICE, 0));
		}
		
		bsData.put("BONDCRY_SHOW", selected_bond.getBONDCRY_SHOW());
		
		//委賣價格 O08
		bsData.put("O08_FMT",NumericUtil.formatNumberString((String) bsData.get("O08"),4) );
		
		//全贖的委賣面額 , 部贖的持有面額
		bsData.put("O07_FMT",NumericUtil.formatNumberString((String) bsData.get("O07"),0) );
		
		//信託本金 O06  
		bsData.put("O06_FMT",NumericUtil.formatNumberString((String) bsData.get("O06"),2) );
		
		return bs;
	}
	
	
public BaseResult bond_sell_confirm_data( BaseResult bs , BaseResult bsB022){
		
		Map<String,Object> bsData = (Map<String,Object>)bs.getData();
		Map<String,Object> bsB022Data = (Map<String,Object>)bsB022.getData();
		
		//轉換bs到bsB022
		bsB022Data.put("SELLWAY", (String)bsData.get("SELLWAY"));
		bsB022Data.put("CUSIDN", (String)bsData.get("CUSIDN"));
		bsB022Data.put("BRHCOD", (String)bsData.get("BRHCOD"));
		bsB022Data.put("IP", (String)bsData.get("IP"));
		bsB022Data.put("hiddenNAME", (String)bsData.get("hiddenNAME"));
		bsB022Data.put("BONDNAME", (String)bsData.get("O02")+" "+(String)bsData.get("O03"));
		bsB022Data.put("BONDCODE", (String)bsData.get("O02"));
		bsB022Data.put("BONDCRY", (String)bsData.get("O04"));
		bsB022Data.put("BONDCRY_SHOW", (String)bsData.get("BONDCRY_SHOW"));
		bsB022Data.put("ACN4", (String)bsData.get("ACN4"));
		
		
		//委託賣價 是B022的O04
		bsB022Data.put("O04_FMT",NumericUtil.formatNumberString((String) bsB022Data.get("O04"),4) );
		
		//委賣面額 是B022的O03
		bsB022Data.put("O03_FMT",NumericUtil.formatNumberString((String) bsB022Data.get("O03"),0) );
		
//		//前手利息 +/- O08+O09
		bsB022Data.put("O09_FMT",NumericUtil.formatNumberString((String) bsB022Data.get("O09"),2) );
		
		//保管費 是B022的O06
		bsB022Data.put("O07_FMT",NumericUtil.formatNumberString((String) bsB022Data.get("O07"),2) );
		
		//信託本金是B022的O04
		bsB022Data.put("O05_FMT",NumericUtil.formatNumberString((String) bsB022Data.get("O05"),2) );
//		
//		//應收付金額 
		bsB022Data.put("O10_FMT",NumericUtil.formatNumberString((String) bsB022Data.get("O10"),2) );
//		
		return bsB022;
	}
	


public BaseResult bond_sell_result_data( BaseResult bs){
	Map<String,Object> bsData = (Map<String,Object>)bs.getData();
		
		// 放入頁面需要的值 數值轉換2,3,4,5,6,7,9,10
		// 庫存面額
		bsData.put("O02_FMT", NumericUtil.formatNumberString((String) bsData.get("O02"), 0));
		// 贖回面額
		bsData.put("O03_FMT", NumericUtil.formatNumberString((String) bsData.get("O03"), 0));
		// 贖回價格
		bsData.put("O04_FMT", NumericUtil.formatNumberString((String) bsData.get("O04"), 4));
		// 信託本金
		bsData.put("O05_FMT", NumericUtil.formatNumberString((String) bsData.get("O05"), 2));
		// 保管費費率
		bsData.put("O06_FMT", NumericUtil.formatNumberString((String) bsData.get("O06"), 4));
		// 保管費 
		bsData.put("O07_FMT", NumericUtil.formatNumberString((String) bsData.get("O07"), 2));
		// //前手利息 O09
		bsData.put("O09_FMT", NumericUtil.formatNumberString((String) bsData.get("O09"), 2));
		// //應收付金額
		bsData.put("O10_FMT", NumericUtil.formatNumberString((String) bsData.get("O10"), 2));
		
	return bs;
}
	
	
	/**
	 * 取得幣別的資料
	 */
	public void setCRY_SHOW(TXNBONDDATA bondData,String locale){
		
		ADMCURRENCY admCurrency = admCurrencyDao.getByPK(bondData.getBONDCRY());
		if(admCurrency != null){
			String ADCCYNAME = "";
			if(locale.equals("zh_CN")) {
				ADCCYNAME = admCurrency.getADCCYCHSNAME();
			}
			else if(locale.equals("en")) {
				ADCCYNAME = admCurrency.getADCCYENGNAME();
			}
			else {
				ADCCYNAME = admCurrency.getADCCYNAME();
			}
			bondData.setBONDCRY_SHOW(ADCCYNAME);
		}
		
	}
	
}
