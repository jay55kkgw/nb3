package tw.com.fstop.nnb.spring.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.TxnTwSchPayData_Service;
import tw.com.fstop.nnb.service.TxnTwSchPay_Service;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;

@RestController
@RequestMapping(value = "/MB/TXNTWSCHPAYDATA")

public class TxnTwSchPayData_Controller {
	
	Logger log = LoggerFactory.getLogger(this.getClass());
	@Autowired
	TxnTwSchPayData_Service txnTwSchPayData_Service;
	@Autowired
	I18n i18n;
	
	@RequestMapping(value = "/query" , method = {RequestMethod.POST} , produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult query(HttpServletRequest request, HttpServletResponse response, @RequestBody Map<String,String> reqParam, Model model) {
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+ CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			bs = txnTwSchPayData_Service.query(reqParam);
		}catch(Throwable e) {
			log.error("Throwable>>{}",e.toString());
		}
		return bs;
	}
}
