package tw.com.fstop.nnb.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fstop.orm.po.ADMBANK;
import fstop.orm.po.ADMBHCONTACT;
import fstop.orm.po.TXNTRACCSET;
import fstop.orm.po.TXNUSER;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.custom.annotation.ACNSET1;
import tw.com.fstop.tbb.nnb.dao.AdmBankDao;
import tw.com.fstop.tbb.nnb.dao.AdmBhContactDao;
import tw.com.fstop.tbb.nnb.dao.AdmMsgCodeDao;
import tw.com.fstop.tbb.nnb.dao.TxnTrAccSetDao;
import tw.com.fstop.tbb.nnb.dao.TxnUserDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.ObjectConvertUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtil;
@Service
public class Personal_Serving_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	AdmBhContactDao admBhContactDao;
	
	@Autowired
	AdmBankDao admBankDao;
	
	@Autowired
	TxnTrAccSetDao txnTrAccSetDao;
	
	@Autowired
	TxnUserDao txnUserDao;
	
	@Autowired
	AdmMsgCodeDao admMsgCodeDao;
	
	@Autowired
	DaoService daoService;
	
	
	/**
	 * 使用者名稱變更結果頁
	 * @param sessionId
	 * @return
	 */
	public BaseResult username_alter_result(String cusidn,Map<String, String> reqParam)	{		
		
		log.trace(ESAPIUtil.vaildLog("username_alter_result>>{}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = null;
		try {
			bs = new BaseResult();
		//	 call ws API		
			bs = N9422_REST(cusidn,reqParam);
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("",e);
		}	
		return  bs;
	}	
	
	/**
	 * pw變更結果頁
	 * @param sessionId
	 * @return
	 */
	public BaseResult pw_alter_result(String cusidn, Map<String, String> reqParam)	{		
		log.trace("pw_alter_result>>");
		
		BaseResult bs = null ;
		try {
			bs =new  BaseResult();
			// n944簽入密碼變更
			bs = N944_REST(cusidn,reqParam);
		
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("pw_alter_result error >> {}",e);
		}	
		return  bs;
	}	
	
	/**
	 * ssl變更結果頁
	 * @param cusidn
	 * @return
	 */
	public BaseResult pw_alter_ssl_result(String cusidn,Map<String, String> reqParam) {		
		log.trace("pw_alter_sslresult>>");
		
		BaseResult bs = null ;
		try {
			bs =new  BaseResult();
			// 交易密碼變更
			bs = N945_REST(cusidn,reqParam);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("pw_alter_ssl_result error >> {}",e);
		}	
		return  bs;
	}
	
	/**
	 * 常用帳號設定(N997)-取得約定轉入帳號
	 * 
	 * @param cusidn
	 * @param action
	 * @return
	 */
	public BaseResult getAgInAcno(Map<String, String> reqParam) {
		log.trace("Personal_Serving_Service_N997_getAgInAcno start");
		BaseResult bs = new BaseResult();
		try {
			bs = new BaseResult();
			// call N921_REST
			bs = N921_REST(reqParam.get("CUSIDN"),ACCSET);
		} catch (Exception e) {
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * 常用帳號設定(N997)-輸入頁
	 * 
	 * @param cusidn
	 * @param action
	 * @return
	 */
	public BaseResult common_acct_setting(String cusidn, String action) {
		log.trace("common_acct_setting start");
		BaseResult bs = new BaseResult();
		List<Map<String,Object>> data = new ArrayList<>();
		if("QUERY".equalsIgnoreCase(action)){
			List<TXNTRACCSET> txntraccset = txnTrAccSetDao.getByUid(cusidn);
			for(TXNTRACCSET each:txntraccset) {
				//將po轉成map
				Map<String, Object> eachMap = ObjectConvertUtil.object2Map(each);
				//找到銀行名稱
				ADMBANK bankName = admBankDao.getbank((String) eachMap.get("DPTRIBANK"));
				//新增顯示欄位
				if(null!=bankName) {
					eachMap.put("ADBANKNAME",bankName.getADBANKNAME());
				}else {
					eachMap.put("ADBANKNAME","");
				}
				
				
				data.add((Map<String, Object>) eachMap);
			}
			bs.addData("txntraccset", data);
			bs.addData("txntraccsetListSize", txntraccset.size());
			bs.setResult(true);
		}
		
		return bs;
	}
	
	/**
	 * 常用帳號設定-結果頁
	 * 
	 * @param reqParam
	 * @return BaseResult
	 */
	public BaseResult common_acct_setting_action(Map<String, String> reqParam) {
		BaseResult bs = null;
		Map<String,String> result = new HashMap<String,String>();
		ObjectMapper objectMapper = new ObjectMapper();
		String dptribank ="";
		String dptrdacno ="";
		try {
			bs = new BaseResult();
			
			if("INSERT".equalsIgnoreCase(reqParam.get("EXECUTEFUNCTION"))) {
				
				String dpuserid = reqParam.get("DPUSERID"); //id
				String dpgoname = reqParam.get("DPGONAME");
				String dptracno = reqParam.get("DPTRACNO");
				if("1".equals(dptracno)) { //約定帳號新增值從option的JSON字串
					log.trace(ESAPIUtil.vaildLog("INSERT DATA >>{}"+CodeUtil.toJson(reqParam.get("DPAGACNO"))));
					JsonNode jsonNode = objectMapper.readTree(reqParam.get("DPAGACNO"));
					 dptribank = jsonNode.get("BNKCOD").asText();
					 dptrdacno = jsonNode.get("ACN").asText();
				}else {
					dptribank = reqParam.get("DPTRIBANK");   
					dptrdacno = reqParam.get("DPTRDACNO");
				}
				
				
				
				log.trace("Insert Data Record");
				
				log.trace(ESAPIUtil.vaildLog("dpuserid >>{}"+dpuserid));
				log.trace(ESAPIUtil.vaildLog("dpgoname  >>{}"+dpgoname));
				log.trace(ESAPIUtil.vaildLog("dptracno  >>{}"+dptracno));
				log.trace(ESAPIUtil.vaildLog("dptribank  >>{}"+dptribank));
				log.trace(ESAPIUtil.vaildLog("dptrdacno  >>{}"+dptrdacno));
				
				if(StrUtil.isNotEmpty(dptrdacno) && 
						txnTrAccSetDao.findDptrdacnoIsExists(dpuserid,dptribank, dptrdacno)) {
						bs.setErrorMessage("Z997",admMsgCodeDao.errorMsg("Z997"));
						bs.setPrevious("/PERSONAL/SERVING//common_acct_setting");
						return bs;
				}
				
				
				TXNTRACCSET po = new TXNTRACCSET();
				po.setDPUSERID(dpuserid);
				po.setDPGONAME(dpgoname);
				po.setDPTRACNO(dptracno);
				po.setDPTRIBANK(dptribank);
				po.setDPTRDACNO(dptrdacno);
				po.setLASTDATE(DateUtil.getDate(""));
				po.setLASTTIME(DateUtil.getTheTime(""));
				txnTrAccSetDao.save(po);
				//同步NB2.5
				daoService.syncTxntraccset(po,reqParam.get("EXECUTEFUNCTION"));
				
				bs.addData("DPGONAME",dpgoname);
				bs.addData("INBANK",dptribank+"-"+admBankDao.getbank(dptribank).getADBANKNAME());
				bs.addData("INACNO",dptrdacno);
				
			}else if("UPDATE".equalsIgnoreCase(reqParam.get("EXECUTEFUNCTION"))) {
				
				String dpaccsetid = reqParam.get("DPACCSETID"); //pk
				String dpuserid = reqParam.get("DPUSERID"); //id
				String dpgoname = reqParam.get("DPGONAME");   
				String dptracno = reqParam.get("DPTRACNO");   
				if("1".equals(dptracno)) { //約定帳號新增值從option的JSON字串
					log.trace(ESAPIUtil.vaildLog("INSERT DATA >>{}"+CodeUtil.toJson(reqParam.get("DPAGACNO"))));
					JsonNode jsonNode = objectMapper.readTree(reqParam.get("DPAGACNO"));
					 dptribank = jsonNode.get("BNKCOD").asText();
					 dptrdacno = jsonNode.get("ACN").asText();
				}else {
					dptribank = reqParam.get("DPTRIBANK");   
					dptrdacno = reqParam.get("DPTRDACNO");
				}
				
				log.trace("Upate Data Record");
				
				log.trace(ESAPIUtil.vaildLog("dpuserid >>{}"+dpuserid));
				log.trace(ESAPIUtil.vaildLog("dpaccsetid  >>{}"+dpaccsetid));
				log.trace(ESAPIUtil.vaildLog("dpgoname  >>{}"+dpgoname));
				log.trace(ESAPIUtil.vaildLog("dptracno  >>{}"+dptracno));
				log.trace(ESAPIUtil.vaildLog("dptribank  >>{}"+dptribank));
				log.trace(ESAPIUtil.vaildLog("dptrdacno  >>{}"+dptrdacno));
				
				
				TXNTRACCSET po = txnTrAccSetDao.get(TXNTRACCSET.class,new Integer(dpaccsetid));
				if(StrUtil.isNotEmpty(dptrdacno) && 
						!StrUtil.trim(dptrdacno).equalsIgnoreCase(po.getDPTRDACNO())) {

					if(txnTrAccSetDao.findDptrdacnoIsExists(dpuserid,dptribank, dptrdacno)) {
						bs.setErrorMessage("Z997",admMsgCodeDao.errorMsg("Z997"));
						return bs;
					}
				}
				
				po.setDPGONAME(dpgoname);
				po.setDPTRACNO(dptracno);
				po.setDPTRIBANK(dptribank);
				po.setDPTRDACNO(dptrdacno);
				po.setLASTDATE(DateUtil.getDate(""));
				po.setLASTTIME(DateUtil.getTheTime(""));
				
				txnTrAccSetDao.update(po);
				//同步NB2.5
				daoService.syncTxntraccset(po , reqParam.get("EXECUTEFUNCTION"));
				bs.addData("DPGONAME",dpgoname);
				bs.addData("INBANK",dptribank+"-"+admBankDao.getbank(dptribank).getADBANKNAME());
				bs.addData("INACNO",dptrdacno);
			}else if("DELETE".equalsIgnoreCase(reqParam.get("EXECUTEFUNCTION"))) {
				String dpaccsetid = reqParam.get("DPACCSETID");
				TXNTRACCSET po = new TXNTRACCSET();
				po.setDPACCSETID(new Integer(dpaccsetid));
//				這行是為了NB2.5的刪除
				po = txnTrAccSetDao.get(TXNTRACCSET.class, po.getDPACCSETID());
				log.debug("ppo>>{}",CodeUtil.toJson(po));
				txnTrAccSetDao.remove(po);
				//同步NB2.5
				daoService.syncTxntraccset(po,reqParam.get("EXECUTEFUNCTION"));
				bs.addData("DPGONAME",reqParam.get("DPGONAME"));
				bs.addData("INBANK",reqParam.get("DPTRIBANK")+"-"+admBankDao.getbank(reqParam.get("DPTRIBANK")).getADBANKNAME());
				bs.addData("INACNO",reqParam.get("DPTRDACNO"));
				
			}
			
		} catch (DataAccessException e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("common_acct_setting_action error >> {}",e);
		} catch (NumberFormatException e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("common_acct_setting_action error >> {}",e);
		} catch (JsonParseException e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("common_acct_setting_action error >> {}",e);
		} catch (JsonMappingException e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("common_acct_setting_action error >> {}",e);
		} catch (IOException e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("common_acct_setting_action error >> {}",e);
		}
		bs.addData("ACTION",reqParam.get("EXECUTEFUNCTION"));
		bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
		bs.setResult(true);
		return bs;
		
	}
	
	/**
	 * 通知設定查詢
	 * 
	 * @param cusidn
	 * @return
	 */
	public BaseResult getNotifyInfo(String cusidn) {
		log.trace("notify_service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs.setResult(true);
			TXNUSER list = txnUserDao.get(TXNUSER.class,cusidn);
			String notifyData = list.getDPNOTIFY();
			String[] tmp = notifyData.split(",");
			for(int i = 1 ; i <= 21 ; i++) {
				if(Arrays.asList(tmp).contains(String.valueOf(i))) {
					bs.addData("DPNOTIFY"+i, "1");
				}else {
					bs.addData("DPNOTIFY"+i, "0");
				}
				
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	public BaseResult setNotify(Map<String, String> reqParam) {
		log.trace("set_notify_service");
		log.trace(ESAPIUtil.vaildLog("notify_setting reParam >> "+CodeUtil.toJson(reqParam)));
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			String cusidn = reqParam.get("CUSIDN");
			reqParam.put("EXECUTEFUNCTION", "UPDATE");
			reqParam.put("COLUMN", "NOTIFYDATA");
			reqParam.put("DPUSERID", cusidn);
			daoService.txnUserModify(reqParam);
//			TXNUSER txnuser = txnUserDao.get(TXNUSER.class,cusidn);
//			txnuser.setDPNOTIFY(reqParam.get("NOTIFYDATA"));
//			txnUserDao.update(txnuser);
			
			bs = this.getNotifyInfo(cusidn);
			bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("setNotify error >> {}",e);
		}

		return bs;
	}
	
	/**
	 * N935_國內臺幣匯入匯款通知設定輸入頁
	 * 
	 * @param reqParam
	 * @return
	 */
	@ACNSET1
	public BaseResult inward_notice_setting(Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			reqParam.put("ADOPID", "N935_1");
			bs = N935_REST(reqParam);
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("inward_notice_setting error >> {}",e);
		}
		return bs;
	}

	/**
	 * N935_國內臺幣匯入匯款通知設定結果頁
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult inward_notice_setting_result(Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			reqParam.put("ADOPID", "N935");
			bs = N934_REST(reqParam);
			if (bs.getResult()){
				String setacn = reqParam.get("ACNINFO") == null ? "" : reqParam.get("ACNINFO");
				int iAdd = setacn.length() / 21;
				String data = setacn;
				String[][] result = new String[iAdd][3];
				String temp = "";
				log.debug(ESAPIUtil.vaildLog("N935 all data: {}"+ data));
				for (int i = 0; i < iAdd; i++)
				{
					temp = data.substring(i * 21, 21 * (i + 1));
					log.debug(ESAPIUtil.vaildLog("N935 temp : {} "+ temp));
					result[i][0] = temp.substring(0, 11);
					result[i][1] = temp.substring(11, 12);
					result[i][2] = temp.substring(12, 21);
				}
				bs.addData("result", result);
			}else{
				log.error("N934_REST_RS bs is error");
			}
			log.debug("inward_notice_setting_result ba Data >>> {}", bs.getData());
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("inward_notice_setting_result error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult account_settings(String cusidn ,Map<String, String> queryData) {
		BaseResult bs = new BaseResult();
		try
		{
		   	queryData.put("TABLE","TxnUser");
	      	queryData.put("DPUSERID",cusidn);
	      	queryData.put("COLUMN","DPOVERVIEW" );
	      	queryData.put("EXECUTEFUNCTION","QUERY");
	      	queryData.put("ADOPID","N999");
	      	Map<String, String> result = daoService.txnUserModify(queryData);
	      	bs.addAllData(result);
	      	bs.addData("CUSIDN", cusidn);
			bs.setResult(Boolean.TRUE);
			if (bs.getResult()){
				
			}else{
				log.error("account_settings bs is error");
			}
			log.debug("account_settings ba Data >>> {}", bs.getData());
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("account_settings error >> {}",e);
		}
		return bs;
	}

	
	/**
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult account_settings_result(String cusidn ,Map<String, String> reqParam) {
		BaseResult bs = new BaseResult();
		try
		{
			reqParam.put("TABLE","TxnUser");
	      	Map<String, String> result = daoService.txnUserModify(reqParam);
	      	bs.addAllData(result);
	      	bs.addData("CUSIDN", cusidn);
	      	bs.addData("DATETIME", DateUtil.getDate("/") + " " + DateUtil.getTheTime(":"));
			bs.setResult(Boolean.TRUE);
			if (bs.getResult()){
				
			}else{
				log.error("account_settings_result bs is error");
			}
			log.debug("account_settings_result ba Data >>> {}", bs.getData());
		}
		catch (Exception e)
		{
			log.error("account_settings_result bs is error");
		}
		return bs;
	}
	
	public BaseResult vouchers_print_list(Map<String, String> reqParam)	{		
		log.trace("vouchers_print_list>>");
		
		BaseResult bs = null ;
		try {
			bs =new  BaseResult();
			// 帶入參數 TXNTYPE 00 >>列表
			reqParam.put("TXNTYPE", "00");
			bs = N106_REST(reqParam);
			//值轉換  
			Map<String, Object> mapN106 = (Map<String,Object>)bs.getData();
			List<Map<String, String>> datas =(List<Map<String, String>>) mapN106.get("REC");
			for(Map<String,String> eachData:datas) {
				eachData.put("NETPAY_SHOW", NumericUtil.formatNumberString(eachData.get("NETPAY"),0));
				eachData.put("TAMPAY_SHOW", NumericUtil.formatNumberString(eachData.get("TAMPAY"),0));
				eachData.put("AMTTAX_SHOW", NumericUtil.formatNumberString(eachData.get("AMTTAX"),0));
				ADMBHCONTACT admBH = admBhContactDao.findByAdbranchid(eachData.get("BRHCOD"));
				Locale currentLocale = LocaleContextHolder.getLocale();
	    		log.debug("errorMsg.locale >> {}" , currentLocale);
	    		String locale = currentLocale.toString();
	    		if(admBH!=null) {
	    		 switch (locale) {
					case "en":
						eachData.put("BRHNAME_DB",admBH.getADBRANENGNAME());
						break;
					case "zh_CN":
						eachData.put("BRHNAME_DB",admBH.getADBRANCHSNAME());
						break;
					default:
						eachData.put("BRHNAME_DB",admBH.getADBRANCHNAME());
						break;
	                }
	    		 }
	    		else {
	    			eachData.put("BRHNAME_DB",eachData.get("BRHNAME"));
	    		}
				
			}
			
			mapN106.put("REC", datas);
			bs.setData(mapN106);
		 
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("vouchers_print_list error >> {}",e);
		}	
		return  bs;
	}
	
	public BaseResult vouchers_print_detail(Map<String, String> reqParam)	{		
		log.trace("vouchers_print_detail>>");
		
		BaseResult bs = null ;
		try {
			bs =new  BaseResult();
			// 帶入參數 TXNTYPE 01 >>明細
			reqParam.put("TXNTYPE", "01");
//			reqParam.put("isTxnlLog", "N");
			bs = N106_REST(reqParam);
			((Map<String,String>)bs.getData()).put("TRANS_FORM",this.getBeanMapValue("N106.FORM", ((Map<String,String>)bs.getData()).get("FORM")));
			//如有原住民姓名,以原住民姓名優先顯示
			if(StrUtil.isNotEmpty(((Map<String,String>)bs.getData()).get("CUSNAME"))) {
				((Map<String,String>)bs.getData()).put("NAME",((Map<String,String>)bs.getData()).get("CUSNAME"));
			}
			log.debug("N1061 bs >>{}",bs.getData());
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("vouchers_print_detail error >> {}",e);
		}	
		return  bs;
	}
	
	
	/**
	 * 輸入beanId && beanMapKey Get beanMapValue
	 * 
	 * @param beanId
	 * @param beanMapKey
	 * @see spring-arg.xml
	 *  @return get spring-arg.xml beanMapValue By beanId && beanMapKey, if any (or
	 *         empty String otherwise)
	 */
	public String getBeanMapValue(String beanId, String beanMapKey)
	{
		// 取得Bean中的Map
		Map<String, String> getTypeBean = SpringBeanFactory.getBean(beanId);
		// 取得Map中的Value
		String getTypeBeanValue = getTypeBean.get(beanMapKey);
		return getTypeBeanValue;
	}
}
