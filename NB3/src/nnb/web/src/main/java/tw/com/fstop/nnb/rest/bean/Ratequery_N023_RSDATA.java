package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class Ratequery_N023_RSDATA extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5036394801592100118L;

	private String FILL;
	private String ITR12;
	private String ITR1;
	private String ITR2;
	private String HEADER;
	private String ITR10;
	private String TIME;
	private String CRY;
	private String ITR11;
	private String CRYNAME;
	private String ITR5;
	private String ITR6;
	private String ITR3;
	private String ITR4;
	private String ITR9;
	private String DATE;
	private String ITR7;
	private String RECNO;
	private String ITR8;
	private String COLOR;
	private String COUNT;
	private String SEQ;

	public String getFILL() {
		return FILL;
	}

	public String getITR12() {
		return ITR12;
	}

	public String getITR1() {
		return ITR1;
	}

	public String getITR2() {
		return ITR2;
	}

	public String getHEADER() {
		return HEADER;
	}

	public String getITR10() {
		return ITR10;
	}

	public String getTIME() {
		return TIME;
	}

	public String getCRY() {
		return CRY;
	}

	public String getITR11() {
		return ITR11;
	}

	public String getCRYNAME() {
		return CRYNAME;
	}

	public String getITR5() {
		return ITR5;
	}

	public String getITR6() {
		return ITR6;
	}

	public String getITR3() {
		return ITR3;
	}

	public String getITR4() {
		return ITR4;
	}

	public String getITR9() {
		return ITR9;
	}

	public String getDATE() {
		return DATE;
	}

	public String getITR7() {
		return ITR7;
	}

	public String getRECNO() {
		return RECNO;
	}

	public String getITR8() {
		return ITR8;
	}

	public String getCOLOR() {
		return COLOR;
	}

	public String getCOUNT() {
		return COUNT;
	}

	public String getSEQ() {
		return SEQ;
	}

	public void setFILL(String fILL) {
		FILL = fILL;
	}

	public void setITR12(String iTR12) {
		ITR12 = iTR12;
	}

	public void setITR1(String iTR1) {
		ITR1 = iTR1;
	}

	public void setITR2(String iTR2) {
		ITR2 = iTR2;
	}

	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}

	public void setITR10(String iTR10) {
		ITR10 = iTR10;
	}

	public void setTIME(String tIME) {
		TIME = tIME;
	}

	public void setCRY(String cRY) {
		CRY = cRY;
	}

	public void setITR11(String iTR11) {
		ITR11 = iTR11;
	}

	public void setCRYNAME(String cRYNAME) {
		CRYNAME = cRYNAME;
	}

	public void setITR5(String iTR5) {
		ITR5 = iTR5;
	}

	public void setITR6(String iTR6) {
		ITR6 = iTR6;
	}

	public void setITR3(String iTR3) {
		ITR3 = iTR3;
	}

	public void setITR4(String iTR4) {
		ITR4 = iTR4;
	}

	public void setITR9(String iTR9) {
		ITR9 = iTR9;
	}

	public void setDATE(String dATE) {
		DATE = dATE;
	}

	public void setITR7(String iTR7) {
		ITR7 = iTR7;
	}

	public void setRECNO(String rECNO) {
		RECNO = rECNO;
	}

	public void setITR8(String iTR8) {
		ITR8 = iTR8;
	}

	public void setCOLOR(String cOLOR) {
		COLOR = cOLOR;
	}

	public void setCOUNT(String cOUNT) {
		COUNT = cOUNT;
	}

	public void setSEQ(String sEQ) {
		SEQ = sEQ;
	}
}
