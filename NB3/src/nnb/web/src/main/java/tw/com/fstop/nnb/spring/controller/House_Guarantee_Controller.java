package tw.com.fstop.nnb.spring.controller;

import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.idgate.bean.N105_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N105_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N830_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N830_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N831_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N831_IDGATE_DATA_VIEW;
import tw.com.fstop.nnb.service.House_Guarantee_Service;
import tw.com.fstop.nnb.service.IdGateService;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({ SessionUtil.CUSIDN, SessionUtil.USERID, SessionUtil.CONFIRM_LOCALE_DATA,SessionUtil.TRANSFER_DATA,SessionUtil.TRANSFER_CONFIRM_TOKEN,
	SessionUtil.TRANSFER_RESULT_FINSH_TOKEN,SessionUtil.RESULT_LOCALE_DATA,SessionUtil.DPMYEMAIL,SessionUtil.TRANSFER_RESULT_TOKEN,SessionUtil.PRINT_DATALISTMAP_DATA,SessionUtil.IDGATE_TRANSDATA })
@Controller
@RequestMapping(value = "/HOUSE/GUARANTEE")
public class House_Guarantee_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private House_Guarantee_Service house_guarantee_service;
	
	@Autowired
	IdGateService idgateservice;


	//房屋擔保借款繳息清單輸入
	@RequestMapping(value = "/interest_list")
	public String interest_list(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		Map<String, String> result = new HashMap<String, String>();
		log.trace("interest_list>>");
		
//		清除切換語系時暫存的資料
		SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");		
		
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			bs = house_guarantee_service.getTxToken();
			log.trace("interest_list.getTxToken: " + bs.getResult());
			log.trace("interest_list.TXTOKEN: " + ((Map<String, String>) bs.getData()).get("TXTOKEN"));
			
			//IDGATE身分
			String idgateUserFlag = "N";
			try {
				BaseResult tmp = idgateservice.checkIdentity(cusidn);
				if(tmp.getResult()) {
					idgateUserFlag = "Y";
				}
				tmp.addData("idgateUserFlag",idgateUserFlag);
				log.debug("get tmp>>>"+tmp.getData());
				SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA,((Map<String, String>) tmp.getData()));
				model.addAttribute("idgateUserFlag",idgateUserFlag);
			}catch(Exception e) {
				log.debug("idgateUserFlag error {}",e);
			}
			
			if(idgateUserFlag.equals("Y")) {
				// IDGATE transdata            		 
	            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	    		log.debug("IDGATE DATA>>>"+IdgateData.toString());
	            String adopid = "N105";
	    		String title = "您有一筆房屋擔保借款繳息清單查詢待確認";
	        	if(IdgateData != null) {
		            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
		            	
		            	result.put("CUSIDN", cusidn);
		            	result.put("ADOPID", adopid);
		            	result.put("TYPE", "00");
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N105_IDGATE_DATA.class, N105_IDGATE_DATA_VIEW.class, result));
		                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
		            }
	        	}
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
			}

			
			if (bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN,
						((Map<String, String>) bs.getData()).get("TXTOKEN"));
			}
			bs.addData("CUSIDN", cusidn);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("interest_list error >> {}",e);
		}finally {
			target = "/house_guarantee/interest_list";

			model.addAttribute("interest_list", bs);			
		}
		return target;
	}
	
	// 房屋擔保借款繳息清單
	@RequestMapping(value = "/interest_list_confirm")
	public String interest_list_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("interest_list_confirm>>");
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			bs = new BaseResult();
			// 解決 Trust Boundary Violation
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>{}" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				} else {
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			} else {
				// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
				String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
				log.trace("interest_list_confirm.token: {}", token);
				if (StrUtil.isNotEmpty(okMap.get("TOKEN")) && okMap.get("TOKEN").equals(token)) {
					bs.setResult(Boolean.TRUE);
				}
				log.trace("bs.getResult(): {}", bs.getResult());
				if (!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				}
				// TOKEN驗證成功，資料處理後進入確認頁
				bs.reset();
				String cusidn = new String(
						Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
						"utf-8");
				 //IDgate驗證		 
                try {		 
                    if(okMap.get("FGTXWAY").equals("7")) {		 

        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N105_IDGATE");
                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
                    }		 
                }catch(Exception e) {		 
                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
                }
				
				bs = house_guarantee_service.interest_list_confirm(cusidn,okMap);
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
			}						
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("interest_list_confirm error >> {}",e);
		}finally {
			if (bs != null && bs.getResult()) {
				target = "/house_guarantee/interest_list_confirm";
				
				model.addAttribute("interest_list_confirm", bs);
				model.addAttribute(SessionUtil.TRANSFER_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN,((Map<String, Object>) bs.getData()).get("TXTOKEN"));
			} else {
				bs.setPrevious("/HOUSE/GUARANTEE/interest_list");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		
		return target;
	}

	// 房屋擔保借款繳息清單結果頁
	@RequestMapping(value = "/interest_list_result")
	public String interest_list_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		String next = "/HOUSE/GUARANTEE/interest_list";
		BaseResult bs = null;
		log.trace("interest_list_result>>");
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			// 解決 Trust Boundary Violation

			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>{}" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if (StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && okMap.get("TXTOKEN")
						.equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null))) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("interest_list_result.bs.step1 is successful...");
				}
				if (!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("interest_list_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if (StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && !okMap.get("TXTOKEN")
						.equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null))) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("interest_list_result.bs.step2 is successful...");
				}
				if (!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("interest_list_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+ okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();

				// 取得從輸入頁輸入存在session中的輸入資料
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String, String> interest_list_data = (Map<String, String>) bs.getData();

				// 取得SESSION中的輸入資料，加入到確認頁的交易機制資料
				okMap.putAll(interest_list_data);

				// 重置bs，進行交易
				bs.reset();
				bs = house_guarantee_service.interest_list_result(cusidn, okMap);

				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("interest_list_result error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/house_guarantee/interest_list_result";				

				List<Map<String,String>> dataListMap = ((List<Map<String,String>>)((Map<String,Object>)bs.getData()).get("REC"));
				SessionUtil.addAttribute(model,SessionUtil.PRINT_DATALISTMAP_DATA,dataListMap);
				
				//回上一頁用
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN,okMap.get("TXTOKEN"));
				
				model.addAttribute("interest_list_result", bs);
			} else {
				bs.setNext(next);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	
}
