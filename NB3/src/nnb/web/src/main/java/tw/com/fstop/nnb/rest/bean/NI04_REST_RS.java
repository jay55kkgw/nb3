package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class NI04_REST_RS extends BaseRestBean implements Serializable {
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = -55460393947218105L;
	
	private String CUSIDN;
	private String TSFACN;
	private String PYMT_FLAG;
	private String TRNDATE;
	private String TRNTIME;
	public String getCUSIDN() {
		return CUSIDN;
	}
	public String getTSFACN() {
		return TSFACN;
	}
	public String getPYMT_FLAG() {
		return PYMT_FLAG;
	}
	public String getTRNDATE() {
		return TRNDATE;
	}
	public String getTRNTIME() {
		return TRNTIME;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public void setTSFACN(String tSFACN) {
		TSFACN = tSFACN;
	}
	public void setPYMT_FLAG(String pYMT_FLAG) {
		PYMT_FLAG = pYMT_FLAG;
	}
	public void setTRNDATE(String tRNDATE) {
		TRNDATE = tRNDATE;
	}
	public void setTRNTIME(String tRNTIME) {
		TRNTIME = tRNTIME;
	}
	
	
}
	

