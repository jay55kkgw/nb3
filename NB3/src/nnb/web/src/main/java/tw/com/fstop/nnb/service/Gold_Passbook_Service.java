package tw.com.fstop.nnb.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import fstop.orm.po.ADMKEYVALUE;
import fstop.orm.po.GOLDPRICE;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmHolidayDao;
import tw.com.fstop.tbb.nnb.dao.AdmKeyValueDao;
import tw.com.fstop.tbb.nnb.dao.AdmMsgCodeDao;
import tw.com.fstop.tbb.nnb.dao.GoldPriceDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SpringBeanFactory;


@Service
public class Gold_Passbook_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private I18n i18n;
	
	@Autowired
	private GoldPriceDao goldPrice_Dao;

	@Autowired
	private AdmHolidayDao admHolidayDao;
	
	@Autowired
	private AdmKeyValueDao admKeyValueDao;
	
	@Autowired
	private AdmMsgCodeDao admMsgCodeDao;
	public BaseResult gold_balance_query(Map<String, String> reqParam) {
		log.trace("gold_balance_query service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// call REST API
			reqParam.put("ACN","");
			bs = N190_REST(reqParam);
			if(bs!=null) {
				Map<String, Object> dataMap = (Map) bs.getData();
				List<Map<String, String>> rowListMap = (List<Map<String, String>>)dataMap.get("REC");//滾詳細資料	
				for(Map<String, String> map: rowListMap ) {
					BigDecimal tempTRANSLATE=new BigDecimal(map.get("MKBAL"));
					map.put("MKBAL",NumericUtil.fmtAmount(tempTRANSLATE.toString(),0));
					tempTRANSLATE=new BigDecimal(map.get("AVGCOST"));
					map.put("AVGCOST",NumericUtil.fmtAmount(tempTRANSLATE.toString(),0));
					tempTRANSLATE=new BigDecimal(map.get("TSFBAL"));
					map.put("TSFBAL",NumericUtil.fmtAmount(tempTRANSLATE.toString(),2));
					tempTRANSLATE=new BigDecimal(map.get("GDBAL"));
					map.put("GDBAL",NumericUtil.fmtAmount(tempTRANSLATE.toString(),2));
				}
				String temp=(String) dataMap.get("TOTAL_TSFBAL");
				dataMap.put("TOTAL_TSFBAL",NumericUtil.fmtAmount(temp, 2));
				temp=(String) dataMap.get("TOTAL_GDBAL");
				dataMap.put("TOTAL_GDBAL",NumericUtil.fmtAmount(temp, 2));
				temp=(String) dataMap.get("TOTAL_MKBAL");
				dataMap.put("TOTAL_MKBAL",NumericUtil.fmtAmount(temp, 0));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error("gold_balance_query service", e);
		}
		finally {
			
		}
		return bs;
	}
	
	public BaseResult gold_detail_query(String cusidn, Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			log.trace("gold_detail_query Start >> ");
			log.trace(ESAPIUtil.vaildLog("ReqParam >> "+CodeUtil.toJson(reqParam)));
			// 取得傳入帳號
			String acn = "ALL".equals(reqParam.get("ACN")) ? "" : reqParam.get("ACN");
			// if acn==""查詢全部 else查詢單一帳號
			bs = N191_REST(cusidn, acn, reqParam);
			//處理回傳的BS
			if (bs != null) {
				Map<String , Object> callRow = (Map) bs.getData();
				ArrayList<Map<String , Object>> callTable = (ArrayList<Map<String , Object>>) callRow.get("REC");
				int okdatacounter=0;
				for (Map<String, Object> table : callTable) {
				
					ArrayList<Map<String , String>> callTableList = (ArrayList<Map<String , String>>) table.get("TABLE");
					if(table.get("TOPMSG").equals("Z032"))
					{
						Map<String, String> newdata = new HashMap<String, String>();
						newdata.put("LSTLTD", (String) table.get("TOPMSG"));
						String r="";
						r = admMsgCodeDao.errorMsg((String) table.get("TOPMSG"));
						newdata.put("MEMO", r);
						callTableList.add(newdata);
				
					}
					for (Map<String, String> data : callTableList) {
						if(table.get("TOPMSG").equals("OKLR"))
						{//欄位格式化							
						if(data.get("PRICE")!=null)		data.put("PRICE",NumericUtil.fmtAmount(data.get("PRICE"),0));
						if(data.get("TRNAMT")!=null)	data.put("TRNAMT",NumericUtil.fmtAmount(data.get("TRNAMT"),0));
						if(data.get("DPWDAMT")!=null)	data.put("DPWDAMT",NumericUtil.fmtAmount(data.get("DPWDAMT"),2));
						if(data.get("DPSVAMT")!=null)	data.put("DPSVAMT",NumericUtil.fmtAmount(data.get("DPSVAMT"),2));
						if(data.get("GDBAL")!=null)		data.put("GDBAL",NumericUtil.fmtAmount(data.get("GDBAL"),2));
						data.put("TIME", DateUtil.formatTime(data.get("TIME")));
						data.put("LSTLTD",DateUtil.convertDate(0,data.get("LSTLTD"),"yyyMMdd","yyy/MM/dd"));
						//
						Locale currentLocale = LocaleContextHolder.getLocale();
			    		
			    		String locale = currentLocale.toString();
			    		String r = "";
			    		log.debug("locale >> {}" , locale);
			    		 switch (locale) {
							case "en":
								r = admKeyValueDao.translatorEN("GOLDMEMO", data.get("MEMO"));		
								break;
							case "zh_CN":
								r = admKeyValueDao.translatorCN("GOLDMEMO", data.get("MEMO"));		
								break;
							default:
								r = admKeyValueDao.translatorTW("GOLDMEMO", data.get("MEMO"));		
								break;
			    		 }
			    		 log.debug(ESAPIUtil.vaildLog("memo >> " + r));
						data.put("MEMO", r);	
						okdatacounter++;
						}
						else {
							data.put("LSTLTD", (String) table.get("TOPMSG"));
							String r="";
							r = admMsgCodeDao.errorMsg((String) table.get("TOPMSG"));
							data.put("MEMO", r);
							data.put("DPWDAMT", "");
							data.put("DPSVAMT", "");
							data.put("GDBAL", "");
							
							data.put("PRICE", "");
							data.put("TRNAMT", "");
							data.put("TIME", "");
						}
					}
				}
				callRow.put("CMRECNUM",okdatacounter);
				bs.setResult(true);
				log.debug("gold_detail_query BaseResult RS :{}", bs.getResult());
				log.debug("callTable.size :{}", callTable.size());
				log.debug("CMRECNUM :{}", callRow.get("CMRECNUM"));
				
			}
		} catch (Exception e) {
			log.error("getTwDepositDetail {}", e);
			log.error("bs getData : {} ", bs.getData());
		}
		return bs;
	}
	
	//黃金存摺餘額查詢輸入頁直接下載
	public BaseResult gold_detail_query_directDownload(String cusidn,Map<String, String> reqParam){
		BaseResult bs = null;
		try {
			bs = gold_detail_query(cusidn, reqParam);
			if (bs != null && bs.getResult()) {
				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap={}", dataMap);
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
				log.debug("rowListMap={}", rowListMap);
				Map<String, Object> parameterMap = new HashMap<String, Object>();
					
				parameterMap.put("CMQTIME", dataMap.get("CMQTIME"));
				parameterMap.put("CMPERIOD", dataMap.get("CMPERIOD"));
				parameterMap.put("CMRECNUM", dataMap.get("CMRECNUM").toString());

				parameterMap.put("downloadFileName", reqParam.get("downloadFileName"));
				parameterMap.put("downloadType", reqParam.get("downloadType"));
				parameterMap.put("templatePath", reqParam.get("templatePath"));
				parameterMap.put("hasMultiRowData", reqParam.get("hasMultiRowData"));

				String downloadType = reqParam.get("downloadType");
				log.trace(ESAPIUtil.vaildLog("downloadType={}"+ downloadType));

				// EXCEL和OLDEXCEL下載
				if("EXCEL".equals(downloadType) || "OLDEXCEL".equals(downloadType)) {
					parameterMap.put("headerRightEnd", reqParam.get("headerRightEnd"));
					parameterMap.put("headerBottomEnd", reqParam.get("headerBottomEnd"));
					parameterMap.put("multiRowStartIndex", reqParam.get("multiRowStartIndex"));
					parameterMap.put("multiRowEndIndex", reqParam.get("multiRowEndIndex"));
					parameterMap.put("multiRowCopyStartIndex", reqParam.get("multiRowCopyStartIndex"));
					parameterMap.put("multiRowCopyEndIndex", reqParam.get("multiRowCopyEndIndex"));
					parameterMap.put("multiRowDataListMapKey", reqParam.get("multiRowDataListMapKey"));
					parameterMap.put("rowRightEnd", reqParam.get("rowRightEnd"));
				}
				// TXT下載
				else{
					parameterMap.put("txtHeaderBottomEnd", reqParam.get("txtHeaderBottomEnd"));
					parameterMap.put("txtMultiRowStartIndex", reqParam.get("txtMultiRowStartIndex"));
					parameterMap.put("txtMultiRowEndIndex", reqParam.get("txtMultiRowEndIndex"));
					parameterMap.put("txtMultiRowCopyStartIndex", reqParam.get("txtMultiRowCopyStartIndex"));
					parameterMap.put("txtMultiRowCopyEndIndex", reqParam.get("txtMultiRowCopyEndIndex"));
					parameterMap.put("txtMultiRowDataListMapKey", reqParam.get("txtMultiRowDataListMapKey"));
				}
				bs.addData("parameterMap", parameterMap);
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_detail_query_directDownload error >> {}",e);
		}
		return bs;
	}
	
	public BaseResult getGoldList() {
		log.trace("get gold list start");
		BaseResult bs = new BaseResult();
		List<GOLDPRICE> branchList = null;
		try {
			branchList = goldPrice_Dao.findByQDATE(DateUtil.getTWDate(""));
//			branchList = goldPrice_Dao.findByQDATE("1080422");
			Map<String, Object> data = new HashMap<String, Object>();
			ArrayList<Object> allData = new ArrayList<Object>();
			log.debug("branch list size >> {}",branchList.size());
			
			//只取最新資料
			Map<String,String> row = new HashMap<String, String>();
			if(branchList.size() > 0) {
				row.put("GOLDPRICEID", branchList.get(0).getGOLDPRICEID());
				row.put("QDATE", DateUtil.convertDate(2,branchList.get(0).getQDATE(),"yyyMMdd","yyy/MM/dd"));
				row.put("QTIME",DateUtil.convertDate(2,branchList.get(0).getQTIME(),"HHmmss","HH:mm:ss"));
				row.put("BPRICE", NumericUtil.fmtAmount(branchList.get(0).getBPRICE(),0));
				row.put("SPRICE", NumericUtil.fmtAmount(branchList.get(0).getSPRICE(),0));
				row.put("PRICE1", NumericUtil.fmtAmount(branchList.get(0).getPRICE1(),0));
				row.put("PRICE2", NumericUtil.fmtAmount(branchList.get(0).getPRICE2(),0));
				row.put("PRICE3", NumericUtil.fmtAmount(branchList.get(0).getPRICE3(),0));
				row.put("PRICE4", NumericUtil.fmtAmount(branchList.get(0).getPRICE4(),0));
				row.put("LASTDATE", branchList.get(0).getLASTDATE());
				row.put("LASTTIME", branchList.get(0).getLASTTIME());
				
				allData.add(row);
				data.put("data", allData);
			}

			bs.addData("REC", data);
			bs.setResult(true);
		} catch (Exception e) {
			log.error("gold_list_result",e);
		}	
		return bs;
	}
	
	public BaseResult history_price_query_result(Map<String, String> reqParam) {
		log.trace("history_price_query_result start");
		BaseResult bs = new BaseResult();
		try {
			bs = new BaseResult();
			bs = GD11HS_REST(reqParam);
			if(bs!=null && bs.getResult()) {
				Map<String , Object> callRow = (Map) bs.getData();
				ArrayList<Map<String , String>> callTable = (ArrayList<Map<String , String>>) callRow.get("REC");
				
				for (Map<String, String> data : callTable) {
					data.put("QDATE",DateUtil.convertDate(1,data.get("QDATE"),"yyyMMdd","yyyy/MM/dd"));
					data.put("QTIME",DateUtil.formatTime(data.get("QTIME")));
					data.put("SPRICEP",NumericUtil.fmtAmount(data.get("SPRICE"), 0));
					data.put("BPRICEP",NumericUtil.fmtAmount(data.get("BPRICE"), 0));
				}
				callRow.put("CMQTIME", getSystemtime());
				if(reqParam.get("QUERYTYPE").equals("LASTDAY")) {
					if(callTable.size() > 0) {
						callRow.put("PERIODFROM", callTable.get(0).get("QDATE"));
						callRow.put("PERIODTO", callTable.get(0).get("QDATE"));
					}
				}
			}
		}catch(Exception e) {
//			e.printStackTrace();
			log.error("time_deposit",e);
		}	
		return  bs;
		
	}
	

	//託收票據明細直接下載
	public BaseResult history_price_query_directDownload(Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			bs = history_price_query_result(reqParam);
			if (bs != null && bs.getResult()) {
				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap={}", dataMap);
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
				log.debug("rowListMap={}", rowListMap);
				Map<String, Object> parameterMap = new HashMap<String, Object>();
				
				parameterMap.put("CMQTIME", dataMap.get("CMQTIME"));
				parameterMap.put("PERIODFROM", dataMap.get("PERIODFROM"));
				parameterMap.put("PERIODTO", dataMap.get("PERIODTO"));

				parameterMap.put("downloadFileName", reqParam.get("downloadFileName"));
				parameterMap.put("downloadType", reqParam.get("downloadType"));
				if(reqParam.get("QUERYTYPE").equals("LASTDAY")) {
					parameterMap.put("templatePath", reqParam.get("templatePath") + "_LD.xls");
				}else {
					parameterMap.put("templatePath", reqParam.get("templatePath") + "_PER.xls");
				}
				parameterMap.put("hasMultiRowData", reqParam.get("hasMultiRowData"));

				String downloadType = reqParam.get("downloadType");
				log.trace(ESAPIUtil.vaildLog("downloadType={}"+ downloadType));

				// EXCEL和OLDEXCEL下載
				if ("EXCEL".equals(downloadType) || "OLDEXCEL".equals(downloadType)) {
					parameterMap.put("headerRightEnd", reqParam.get("headerRightEnd"));
					parameterMap.put("headerBottomEnd", reqParam.get("headerBottomEnd"));
					parameterMap.put("rowRightEnd", reqParam.get("rowRightEnd"));
					parameterMap.put("rowStartIndex", reqParam.get("rowStartIndex"));
				}
				bs.addData("parameterMap", parameterMap);
				if(rowListMap.size() <= 0) {
					bs.setErrorMessage("ENRD", i18n.getMsg("LB.Check_no_data"));//查無資料
					bs.setResult(Boolean.FALSE);
				}
			}
		
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("history_price_query_directDownload error >> {}",e);
		}
		return bs;
	}
	
	public BaseResult gold_reservation_query(String cusidn,Map<String, String> reqParam) {
		log.trace("gold_reservation_query start");
		BaseResult bs = new BaseResult();
		try {
			BaseResult bsN920 = new BaseResult();
			BaseResult bsN926 = new BaseResult();
			bsN920 = N920_REST(cusidn, GOLD_RESERVATION_QOERY_N920);
			bsN926 = N926_REST(cusidn, GOLD_RESERVATION_QOERY);
			
			if(bsN920!=null && bsN920.getResult() && bsN926!=null && bsN926.getResult()) {
				Map<String , Object> callDataN920 = (Map) bsN920.getData();
				ArrayList<Map<String , String>> callTableN920 = (ArrayList<Map<String , String>>) callDataN920.get("REC");
				Map<String , Object> callDataN926 = (Map) bsN926.getData();
				ArrayList<Map<String , String>> callTableN926 = (ArrayList<Map<String , String>>) callDataN926.get("REC");
				

				for(int i = 0;i < callTableN926.size();i++) {
					if(callTableN926.get(i).get("ACN").length() != 11) {
						callTableN926.remove(i);
						i--;
					}
				}
				
				if(callTableN920.size() > 0) {
					if(callTableN926.size() > 0) {
						ArrayList<Map<String , String>> bsREC = new ArrayList<Map<String , String>>();
						int cmrecnum = 0;
						int countApptypeBuy = 0;
						int countApptypeSell = 0;
						int errorFlag = 0;
						for(Map<String , String> callRowN926: callTableN926) {
							if(callRowN926.get("ACN").length() == 11) {
								BaseResult bsN092 = new BaseResult();
								bsN092 = N092_REST(cusidn, callRowN926.get("ACN"));
								Map<String , Object> callDataN092 = (Map) bsN092.getData();

//								if(((String)callDataN092.get("MSGCOD")).equals("0000") || ((String)callDataN092.get("MSGCOD")).equals("OKLR")) {
								if(bsN092.getMsgCode().equals("0") ||bsN092.getMsgCode().equals("0000") || bsN092.getMsgCode().equals("OKLR")) {
									ArrayList<Map<String , String>> callTableN092 = (ArrayList<Map<String , String>>) callDataN092.get("REC");
									for(Map<String , String> callRowN092: callTableN092) {
										if(!callRowN092.get("ACN2").equals("")) {
											String memo = callRowN092.get("STATUS");
											Map<String, String> argBean = SpringBeanFactory.getBean("GOLD_RESERVATION_STATU");
											if(argBean.containsKey(memo)) {
												String MEMO_C = argBean.get(memo);
												callRowN092.put("STATUS",i18n.getMsg(MEMO_C));
											}
											callRowN092.put("ACN", callRowN926.get("ACN"));
											callRowN092.put("APPDATE_str",DateUtil.convertDate(2,callRowN092.get("APPDATE"),"yyymmdd","yyy/mm/dd"));
											callRowN092.put("TRNGD_str",NumericUtil.fmtAmount(callRowN092.get("TRNGD"),2));
											bsREC.add(callRowN092);
											cmrecnum++;
											log.debug("cmrecnum >> {}", cmrecnum);
											log.debug("callRowN092 >> {}", callRowN092);
											if(callRowN092.get("APPTYPE").equals("01")) {
												countApptypeBuy++;
											}else if(callRowN092.get("APPTYPE").equals("02")) {
												countApptypeSell++;
											}
										}
									}
									bs.setData(callDataN092);
								}else if(bsN092.getMsgCode().equals("ENRD")) {
//								}else if(((String)callDataN092.get("MSGCOD")).equals("ENRD")) {
									continue;
								}else {
									bs.setErrorMessage(bsN092.getMsgCode(), bsN092.getMessage());
									errorFlag = 1;
									log.debug("error bsN092");
									break;
								}
							}
						}
						bs.addData("CMQTIME", DateUtil.getDate("/") + " " + DateUtil.getTheTime(":"));
						bs.addData("REC", bsREC);
						bs.addData("CMRECNUM", cmrecnum);
						bs.addData("COUNT_APPTYPE_BUY", countApptypeBuy);
						bs.addData("COUNT_APPTYPE_SELL", countApptypeSell);
						bs.addData("CMRECNUM_TOTAL", countApptypeBuy + countApptypeSell);
						bs.addData("ToGoFlag", "1");
						if(errorFlag == 0) {
							bs.setSYSMessage("0");
							bs.setResult(Boolean.TRUE);
						}else {
							bs.setResult(Boolean.FALSE);
						}
					}else {
						bs.addData("MsgDesc", i18n.getMsg("LB.X1776"));//您尚未申請黃金存摺網路交易，請洽往來分行辦理或線上立即申請本功能。
						bs.addData("TxType", "NA60");
						bs.addData("Trancode", "NA60");
						bs.addData("ToGoFlag", "0");
						bs.setSYSMessage("0");
						bs.setResult(Boolean.TRUE);
					}
				}else {
					bs.addData("MsgDesc", i18n.getMsg("LB.X1772"));//您尚未申請黃金存摺帳戶，請洽往來分行辦理或線上立即申請。
					bs.addData("TxType", "NA50");
					bs.addData("Trancode", "NA50");
					bs.addData("ToGoFlag", "0");
					bs.setSYSMessage("0");
					bs.setResult(Boolean.TRUE);
					
				}
			}
		}catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_reservation_query error >> {}",e);
		}	
		return  bs;
	}
	
	public BaseResult gold_reservation_query_result(String cusidn,Map<String, String> reqParam) {
		log.trace("gold_reservation_query_result start");
		BaseResult bs = new BaseResult();
		try {
			bs = new BaseResult();
			bs = N091_03_REST(cusidn, reqParam);
			if(bs!=null && bs.getResult()) {
				Map<String , Object> callRow = (Map) bs.getData();
				callRow.put("APPDATE",DateUtil.convertDate(2,(String)callRow.get("APPDATE"),"yyymmdd","yyy/mm/dd"));
//				callRow.put("CMQTIME", getSystemtime());
			}
		}catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_reservation_query_result error >> {}",e);
		}	
		return  bs;
	}
	
	public String getSystemtime() {
		
		return DateUtil.getTWDate("/")+"  "+DateUtil.getTheTime(":");
	}
		
	
	public int isGoldHOLIDAY(){
		boolean holiday = admHolidayDao.isAdmholiday();						
		//若為假日  
		if(holiday){
			return 1;				
		}
		//若非假日			
		else{
			return 0;				
		}
	}
}