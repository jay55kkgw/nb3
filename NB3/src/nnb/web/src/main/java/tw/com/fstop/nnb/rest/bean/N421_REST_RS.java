package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N421_REST_RS extends BaseRestBean implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2233051008833856590L;

	private String ABEND;// 結束代碼

	private String USERDATA_X50;// 暫存空間區

	private String REC_NO;// 筆數

	private String ENDCOD;//結束註記

	private String CMQTIME;

	private String CMRECNUM;

	private String DPTAMT;

	private String __OCCURS;
	
	private LinkedList<N421_REST_RSDATA> REC;


	public String getABEND()
	{
		return ABEND;
	}

	public void setABEND(String aBEND)
	{
		ABEND = aBEND;
	}

	public String getUSERDATA_X50()
	{
		return USERDATA_X50;
	}

	public void setUSERDATA_X50(String uSERDATA_X50)
	{
		USERDATA_X50 = uSERDATA_X50;
	}

	public String getREC_NO()
	{
		return REC_NO;
	}

	public void setREC_NO(String rEC_NO)
	{
		REC_NO = rEC_NO;
	}

	public String getENDCOD()
	{
		return ENDCOD;
	}

	public void setENDCOD(String eNDCOD)
	{
		ENDCOD = eNDCOD;
	}

	public String getCMQTIME()
	{
		return CMQTIME;
	}

	public void setCMQTIME(String cMQTIME)
	{
		CMQTIME = cMQTIME;
	}

	public String getCMRECNUM()
	{
		return CMRECNUM;
	}

	public void setCMRECNUM(String cMRECNUM)
	{
		CMRECNUM = cMRECNUM;
	}

	public String getDPTAMT()
	{
		return DPTAMT;
	}

	public void setDPTAMT(String dPTAMT)
	{
		DPTAMT = dPTAMT;
	}

	public String get__OCCURS()
	{
		return __OCCURS;
	}

	public void set__OCCURS(String __OCCURS)
	{
		this.__OCCURS = __OCCURS;
	}

	public LinkedList<N421_REST_RSDATA> getREC()
	{
		return REC;
	}

	public void setREC(LinkedList<N421_REST_RSDATA> rEC)
	{
		REC = rEC;
	}
	

}
