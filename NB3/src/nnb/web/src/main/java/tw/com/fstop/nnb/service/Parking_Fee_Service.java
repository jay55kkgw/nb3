package tw.com.fstop.nnb.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.util.DateUtil;
import tw.com.fstop.util.DownloadUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SpringBeanFactory;

@Service
public class Parking_Fee_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private I18n i18n;
	
	public BaseResult parking_withholding_apply_result(String cusidn,Map<String, String> reqParam) {
		log.trace("other_labor_insurance_result");
		BaseResult bs = null ;
		try {
			//reqParam.put("ACNNO",reqParam.get("ACNNO").substring(reqParam.get("ACNNO").length() - 11));
			String txnDate = DateUtil.getDate("");
			String txnTime = DateUtil.getTheTime("");
			String txnDateTime = txnDate + txnTime;
			String serialNo = "";
			if(txnDateTime.length() > 6) {
				serialNo = txnDateTime.substring(6, txnDateTime.length());
			}
			reqParam.put("TxnDateTime", txnDateTime);
			reqParam.put("SerialNo", serialNo);
			String StartDate = DateUtil.format("yyyyMMdd", new Date());
			reqParam.put("StartDate", StartDate);
			bs = new BaseResult();
			bs = P001_REST(cusidn, reqParam);
			if(bs!=null) {
//				bs.addData("CMQTIME", DateUtil.getDate("/") + " " + DateUtil.getTheTime(":"));
				Map<String,Object> callData = (Map<String,Object>)bs.getData();
				if(callData.get("TOPMSG").equals("R000")) {
					log.debug("result message>>{}",callData.get("msgName"));
					callData.put("msgCode", "0");
					bs.setResult(Boolean.TRUE);
				}
				
			}
		}catch(Exception e) {
//			e.printStackTrace();
			log.error("time_deposit",e);
		}	
		return  bs;
	}
	
	public BaseResult parking_withholding_query_result(String cusidn,Map<String, String> reqParam) {
		log.trace("other_labor_insurance_result");
		BaseResult bs = null ;
		try {
			//reqParam.put("ACNNO",reqParam.get("ACNNO").substring(reqParam.get("ACNNO").length() - 11));
			String txnDate = DateUtil.getDate("");
			String txnTime = DateUtil.getTheTime("");
			String txnDateTime = txnDate + txnTime;
			String serialNo = "";
			if(txnDateTime.length() > 6) {
				serialNo = txnDateTime.substring(6, txnDateTime.length());
			}
			reqParam.put("TxnDateTime", txnDateTime);
			reqParam.put("SerialNo", serialNo);
			reqParam.put("StartSearchDate",reqParam.get("STARTSERACHDATE").replaceAll("/", ""));   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
			reqParam.put("EndSearchDate",reqParam.get("ENDSERACHDATE").replaceAll("/", ""));   // 將 yyyy/MM/dd 的 "/" 去掉, 變成 yyyyMMdd
			bs = new BaseResult();
			bs = P004_REST(cusidn, reqParam);
			if(bs!=null) {
//				bs.addData("CMQTIME", DateUtil.getDate("/") + " " + DateUtil.getTheTime(":"));
				Map<String,Object> callData = (Map<String,Object>)bs.getData();
				if(callData.get("TOPMSG").equals("R000")) {
					log.debug("result message>>{}",callData.get("msgName"));
					callData.put("msgCode", "0");
					bs.setResult(Boolean.TRUE);
				}
				List<Map<String,String>> callRow = (List<Map<String,String>>)callData.get("REC");
				for(Map<String,String> row:callRow) {
					Map<String, String> argBean = SpringBeanFactory.getBean("N614_ZONECODE");
					if(argBean.containsKey((String)row.get("ZoneCodeZip")))
					{
						String ZONECODE = argBean.get((String)row.get("ZoneCodeZip"));
						row.put("ZoneCode",i18n.getMsg(ZONECODE));
					}
					argBean = SpringBeanFactory.getBean("N614_CARKIND");
					if(argBean.containsKey((String)row.get("CarKindZip")))
					{
						String CARKIND = argBean.get((String)row.get("CarKindZip"));
						row.put("CarKind",i18n.getMsg(CARKIND));
					}
					argBean = SpringBeanFactory.getBean("N614_SESSIONCODE");
					if(argBean.containsKey((String)row.get("SessionCodeZip")))
					{
						String SESSIONCODE = argBean.get((String)row.get("SessionCodeZip"));
						row.put("SessionCode",i18n.getMsg(SESSIONCODE));
					}
				}
				callData.put("CarId",reqParam.get("CarId"));
				callData.put("Account",reqParam.get("Account"));
				callData.put("StartRecordNo",NumericUtil.fmtAmount((String)callData.get("StartRecordNo"), 0));
				callData.put("TotalRecord",NumericUtil.fmtAmount((String)callData.get("TotalRecord"), 0));
				callData.put("WaitPayRecord",NumericUtil.fmtAmount((String)callData.get("WaitPayRecord"), 0));
				callData.put("PaySuccessRecord",NumericUtil.fmtAmount((String)callData.get("PaySuccessRecord"), 0));
				callData.put("PayFailRecord",NumericUtil.fmtAmount((String)callData.get("PayFailRecord"), 0));
				
			}
		}catch(Exception e) {
//			e.printStackTrace();
			log.error("time_deposit",e);
		}	
		return  bs;
	}
	
	public BaseResult parking_withholding_modify(String cusidn,Map<String, String> reqParam) {
		log.trace("parking_withholding_modify");
		BaseResult bs = null ;
		try {
			//reqParam.put("ACNNO",reqParam.get("ACNNO").substring(reqParam.get("ACNNO").length() - 11));
			String txnDate = DateUtil.getDate("");
			String txnTime = DateUtil.getTheTime("");
			String txnDateTime = txnDate + txnTime;
			String serialNo = "";
			if(txnDateTime.length() > 6) {
				serialNo = txnDateTime.substring(6, txnDateTime.length());
			}
			reqParam.put("TxnDateTime", txnDateTime);
			reqParam.put("SerialNo", serialNo);
			reqParam.put("TxnCode", "P003");
			bs = new BaseResult();
			bs = P003_REST(cusidn, reqParam);
			if(bs!=null) {
//				bs.addData("CMQTIME", DateUtil.getDate("/") + " " + DateUtil.getTheTime(":"));
				Map<String,Object> callData = (Map<String,Object>)bs.getData();
				List<Map<String, String>> rowListMap = (List<Map<String, String>>)callData.get("REC");//滾詳細資料	
				for(Map<String,String> callRow:rowListMap) {
					String accountMark = callRow.get("Account").substring(0, 5)+"****"+callRow.get("Account").substring(9, 11);
					callRow.put("MaskAccount",accountMark);
					if(!callRow.get("RegistDate").equals("00000000")) {
						callRow.put("RegistDate", DateUtil.convertDate(2, callRow.get("RegistDate"), "yyyyMMdd", "yyyy/MM/dd") );
					}
					Map<String, String> argBean = SpringBeanFactory.getBean("N614_CARKIND");
					if(argBean.containsKey((String)callRow.get("CarKind")))
					{
						String CARKIND = argBean.get((String)callRow.get("CarKind"));
						callRow.put("CarKind_str",i18n.getMsg(CARKIND));
					}
					argBean = SpringBeanFactory.getBean("N614_ZONECODE");
					if(argBean.containsKey((String)callRow.get("ZoneCode")))
					{
						String ZONECODE = argBean.get((String)callRow.get("ZoneCode"));
						callRow.put("ZoneCode_str",i18n.getMsg(ZONECODE));
					}
					argBean = SpringBeanFactory.getBean("N614_STATUS");
					if(argBean.containsKey((String)callRow.get("Status")))
					{
						String ZONECODE = argBean.get((String)callRow.get("Status"));

						List<String> variableKeysList = new ArrayList<String>();
						String leftSignal = "i18n{";
						String rightSignal = "}";
						
						DownloadUtil.getVariableKeys(variableKeysList,leftSignal,rightSignal,ZONECODE);
						log.debug("variableKeysList={}",variableKeysList);
						
						//有I18NKEY要取代
						for(String key : variableKeysList){
							String i18nValue = i18n.getMsg(key.trim());
							log.debug("i18nValue={}",i18nValue);
							
							String replaceValue = leftSignal + key + rightSignal;
							log.debug("replaceValue={}",replaceValue);
							
							//有對應的值
							if(i18nValue != null){
								ZONECODE = ZONECODE.replace(replaceValue,i18nValue);
							}
							//沒有對應的值，代空值
							else{
								ZONECODE = ZONECODE.replace(replaceValue,"");
							}
						}
						callRow.put("Status_str",ZONECODE);
					}
				}
				if(null!=callData.get("TOPMSG") && callData.get("TOPMSG").equals("R000")) {
					log.debug("result message>>{}",callData.get("msgName"));
					callData.put("msgCode", "0");
					bs.setResult(Boolean.TRUE);
				}
				
			}
		}catch(Exception e) {
//			e.printStackTrace();
			log.error("time_deposit",e);
		}	
		return  bs;
	}
	
	public BaseResult parking_withholding_modify_update_result(String cusidn,Map<String, String> reqParam) {
		log.trace("other_labor_insurance_result");
		BaseResult bs = null ;
		try {
			//reqParam.put("ACNNO",reqParam.get("ACNNO").substring(reqParam.get("ACNNO").length() - 11));
			String txnDate = DateUtil.getDate("");
			String txnTime = DateUtil.getTheTime("");
			String txnDateTime = txnDate + txnTime;
			String serialNo = "";
			if(txnDateTime.length() > 6) {
				serialNo = txnDateTime.substring(6, txnDateTime.length());
			}
			reqParam.put("TxnDateTime", txnDateTime);
			reqParam.put("SerialNo", serialNo);
			bs = new BaseResult();
			bs = P005_REST(cusidn, reqParam);
			if(bs!=null) {
//				bs.addData("CMQTIME", DateUtil.getDate("/") + " " + DateUtil.getTheTime(":"));
				Map<String,Object> callData = (Map<String,Object>)bs.getData();
				if(callData.get("TOPMSG").equals("R000")) {
					log.debug("result message>>{}",callData.get("msgName"));
					callData.put("msgCode", "0");
					bs.setResult(Boolean.TRUE);
				}
				
			}
		}catch(Exception e) {
//			e.printStackTrace();
			log.error("time_deposit",e);
		}	
		return  bs;
	}
	
	public BaseResult parking_withholding_modify_delete_result(String cusidn,Map<String, String> reqParam) {
		log.trace("other_labor_insurance_result");
		BaseResult bs = null ;
		try {
			//reqParam.put("ACNNO",reqParam.get("ACNNO").substring(reqParam.get("ACNNO").length() - 11));
			String txnDate = DateUtil.getDate("");
			String txnTime = DateUtil.getTheTime("");
			String txnDateTime = txnDate + txnTime;
			String serialNo = "";
			if(txnDateTime.length() > 6) {
				serialNo = txnDateTime.substring(6, txnDateTime.length());
			}
			reqParam.put("TxnDateTime", txnDateTime);
			reqParam.put("SerialNo", serialNo);
			reqParam.put("StartDate", txnDate);
			bs = new BaseResult();
			bs = P002_REST(cusidn, reqParam);
			if(bs!=null) {
//				bs.addData("CMQTIME", DateUtil.getDate("/") + " " + DateUtil.getTheTime(":"));
				Map<String,Object> callData = (Map<String,Object>)bs.getData();
				if(callData.get("TOPMSG").equals("R000")) {
					log.debug("result message>>{}",callData.get("msgName"));
					callData.put("msgCode", "0");
					bs.setResult(Boolean.TRUE);
				}
				
			}
		}catch(Exception e) {
//			e.printStackTrace();
			log.error("time_deposit",e);
		}	
		return  bs;
	}
}
