package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N094_REST_RS extends BaseRestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -374385470327523032L;
	
	private String TRNDATE;		//日期YYYMMDD
	private String TRNTIME;		//時間HHMMSS
	private String TRNSRC;		//交易來源
	private String TRNTYP;		//交易種類
	private String TRNCOD;		//交易註記碼
	private String TRNBDT;		//交易日期
	private String ACN;			//黃金存摺帳號
	private String CUSIDN;		//客戶統一編號
	private String SVACN;		//台幣存款帳號
	private String KIND;		//手續費種類 01-定期定額
	private String TRNAMT;		//應繳款總金額
	private String O_TOTBAL;	//帳戶餘額
	private String O_AVLBAL;	//可用餘額
	private String ARY_CNT;		//明細陣列筆數
	private String ABEND;		//回應代碼
	private String USERDATA_X50;//暫存空間區
	private String CMTXTIME;	//交易時間
	private LinkedList<N094_REST_RSDATA> REC;
	
	public String getCMTXTIME() {
		return CMTXTIME;
	}
	public void setCMTXTIME(String cMTXTIME) {
		CMTXTIME = cMTXTIME;
	}
	public String getTRNDATE() {
		return TRNDATE;
	}
	public void setTRNDATE(String tRNDATE) {
		TRNDATE = tRNDATE;
	}
	public String getTRNTIME() {
		return TRNTIME;
	}
	public void setTRNTIME(String tRNTIME) {
		TRNTIME = tRNTIME;
	}
	public String getTRNSRC() {
		return TRNSRC;
	}
	public void setTRNSRC(String tRNSRC) {
		TRNSRC = tRNSRC;
	}
	public String getTRNTYP() {
		return TRNTYP;
	}
	public void setTRNTYP(String tRNTYP) {
		TRNTYP = tRNTYP;
	}
	public String getTRNCOD() {
		return TRNCOD;
	}
	public void setTRNCOD(String tRNCOD) {
		TRNCOD = tRNCOD;
	}
	public String getTRNBDT() {
		return TRNBDT;
	}
	public void setTRNBDT(String tRNBDT) {
		TRNBDT = tRNBDT;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getSVACN() {
		return SVACN;
	}
	public void setSVACN(String sVACN) {
		SVACN = sVACN;
	}
	public String getKIND() {
		return KIND;
	}
	public void setKIND(String kIND) {
		KIND = kIND;
	}
	public String getTRNAMT() {
		return TRNAMT;
	}
	public void setTRNAMT(String tRNAMT) {
		TRNAMT = tRNAMT;
	}
	public String getO_TOTBAL() {
		return O_TOTBAL;
	}
	public void setO_TOTBAL(String o_TOTBAL) {
		O_TOTBAL = o_TOTBAL;
	}
	public String getO_AVLBAL() {
		return O_AVLBAL;
	}
	public void setO_AVLBAL(String o_AVLBAL) {
		O_AVLBAL = o_AVLBAL;
	}
	public String getARY_CNT() {
		return ARY_CNT;
	}
	public void setARY_CNT(String aRY_CNT) {
		ARY_CNT = aRY_CNT;
	}
	public String getABEND() {
		return ABEND;
	}
	public void setABEND(String aBEND) {
		ABEND = aBEND;
	}
	public String getUSERDATA_X50() {
		return USERDATA_X50;
	}
	public void setUSERDATA_X50(String uSERDATA_X50) {
		USERDATA_X50 = uSERDATA_X50;
	}
	public LinkedList<N094_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<N094_REST_RSDATA> rEC) {
		REC = rEC;
	}
}
