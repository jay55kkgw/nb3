package tw.com.fstop.nnb.spring.controller;

import java.io.File;
import java.io.InputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.poi.util.IOUtils;
import org.apache.xpath.operations.Bool;
import org.owasp.esapi.ESAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

import fstop.orm.po.ADMCOUNTRY;
import fstop.orm.po.TXNCARDAPPLY;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.custom.annotation.IPVERIFY;
import tw.com.fstop.nnb.custom.annotation.ISTXNLOG;
import tw.com.fstop.nnb.service.Apply_CreditLoan_Service;
import tw.com.fstop.nnb.service.Change_Data_Service;
import tw.com.fstop.nnb.service.Creditcard_Apply_Service;
import tw.com.fstop.nnb.service.DaoService;
import tw.com.fstop.nnb.service.Online_Apply_Service;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmCountryDao;
import tw.com.fstop.tbb.nnb.dao.TxnCardApplyDao;
import tw.com.fstop.tbb.nnb.dao.TxnUserDao;
import tw.com.fstop.tbb.nnb.util.StrUtils;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.ObjectConvertUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.PDFUtil;
import tw.com.fstop.web.util.WebUtil;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import tw.com.fstop.util.SpringBeanFactory;
import org.springframework.beans.factory.annotation.Value;
import tw.com.fstop.nnb.service.Online_Apply_Service;

@SessionAttributes({ 
	SessionUtil.CUSIDN, 
	SessionUtil.PD,
	SessionUtil.USERIP,
	SessionUtil.ISIKEYUSER,
	SessionUtil.DOWNLOAD_DATALISTMAP_DATA,
	SessionUtil.PRINT_DATALISTMAP_DATA, 
	SessionUtil.CONFIRM_LOCALE_DATA,
	SessionUtil.RESULT_LOCALE_DATA, 
	SessionUtil.TRANSFER_CONFIRM_TOKEN,	
	SessionUtil.TRANSFER_RESULT_TOKEN, 
	SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, 
	SessionUtil.TRANSFER_DATA,
	SessionUtil.STEP1_LOCALE_DATA,
	SessionUtil.STEP2_LOCALE_DATA,
	SessionUtil.STEP3_LOCALE_DATA,
	SessionUtil.STEP4_LOCALE_DATA,
	SessionUtil.STEP4_2_LOCALE_DATA,
	SessionUtil.STEP4_3_LOCALE_DATA,
	SessionUtil.UPLOAD_CREDITCARD_IDENTITY_P2_DATA,
	SessionUtil.CUSIDN_ONLINE_APPLY,
	SessionUtil.YNFLG,
	SessionUtil.RCVNO,
	SessionUtil.PFLG,
	SessionUtil.MOBILE,
	SessionUtil.ONLINE_APPLY_DATA,
	SessionUtil.DCCSTA,
	SessionUtil.OLDCARDOWNERCHK,
	SessionUtil.PASSAUTH
	})
@Controller
@RequestMapping(value = "/CREDIT/APPLY")
public class Creditcard_Apply_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Creditcard_Apply_Service creditcard_apply_service;
	@Autowired
	private Apply_CreditLoan_Service apply_CreditLoan_Service;
	@Autowired
	private Online_Apply_Service online_apply_service;
	@Autowired
	private Change_Data_Service change_data_service;
	
	@Autowired
	private DaoService daoservice;
	@Autowired
	private TxnCardApplyDao txnCardApplyDao;
	@Autowired
	private TxnUserDao txnUserDao;
	@Autowired
	private I18n i18n;
	@Autowired
	private AdmCountryDao admcountrydao;
	@Autowired
	ServletContext context; 
	
	@Value("${eln_uri}")
	String eln_uri;

	// 信用卡開卡_輸入頁
		@RequestMapping(value = "/card_activate")
		public String activate(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			log.trace("creditcard_controller_card_activate...");
			String target = "/error";
			// 處理結果
			target = "/creditcard/activate";
			model.addAttribute("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
			log.trace("target >>{}", target);
			return target;
		}

		// 信用卡開卡_查詢頁
		@RequestMapping(value = "/card_activate_step1")
		public String activate_step1(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			log.trace("creditcard_controller_card_activate_step1...");
			String target = "/error";
			// 處理結果
			BaseResult bs = null;
			try {
				// 解決 Trust Boundary Violation
				Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

				// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
				if (hasLocale) {
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
					if (!bs.getResult()) {
						log.trace("bs.getResult() >>{}", bs.getResult());
						throw new Exception();
					}
				}
				if (!hasLocale) {
					String cusidn = new String(
							Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
							"utf-8");
					okMap.put("CUSIDN", cusidn);
					log.trace("card_activate_cusdin >> {}", cusidn);
					// 信用卡開卡結果
					bs = creditcard_apply_service.activate(okMap);
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
					bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				}
				model.addAttribute("card_activate_step1", bs);
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("activate_step1 error >> {}",e);
			} finally {
				if (bs != null && bs.getResult()) {
					target = "/creditcard/activate_step1";
					model.addAttribute("activate_result", bs);
				} else {
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}
			log.trace("target >> {} ", target);
			return target;
		}

		// 信用卡掛失_輸入頁
		@RequestMapping(value = "/card_loss")
		public String loss(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			log.trace("creditcard_controller_card_loss...");

			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			String target = "/error";
			// 處理結果
			BaseResult bs = null;
			try {
				// portal物件
				String cusidn = new String(
						Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
						"utf-8");
				String sessionpw = (String) SessionUtil.getAttribute(model, SessionUtil.PD, null);
				log.trace("card_loss_sessionId >>{} ", cusidn);
				// 信用卡可掛失列表查詢結果
				bs = creditcard_apply_service.loss_getCardList(cusidn);
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("loss error >> {}",e);
			} finally {
				if (bs != null && bs.getResult()) {
					target = "/creditcard/loss";
					model.addAttribute("card_loss", bs);
				} else {
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}
			log.trace("target >>{}", target);
			return target;
		}

		// 信用卡掛失_查詢頁
		@RequestMapping(value = "/card_loss_step1")
		public String loss_step1(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			log.trace("creditcard_controller_card_loss_step1...");
			String target = "/error";
			String previous = "/CREDIT/APPLY/card_loss";
			// 處理結果
			BaseResult bs = null;
			try {
				// 解決 Trust Boundary Violation
				Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
				// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
				if (hasLocale) {
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
					if (!bs.getResult()) {
						log.trace("bs.getResult() >>{}", bs.getResult());
						throw new Exception();
					}
				}
				if (!hasLocale) {
					// portal物件
					String cusidn = new String(
							Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
							"utf-8");
					log.trace("card_activate_cusidn >>{}", cusidn);
					String adguid = (String) request.getAttribute(SessionUtil.ADGUID);
					String aduserip = WebUtil.getIpAddr(request);
					okMap.put("ADUSERIP", aduserip);
					okMap.put("ADGUID", adguid);
					// 信用卡掛失結果
					bs = creditcard_apply_service.loss_action(cusidn,okMap);
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
				
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("loss_step1 error >> {}",e);
			} finally {
				if (bs != null && bs.getResult()) {
					target = "/creditcard/loss_step1";
					model.addAttribute("card_loss_step1", bs);
					//讓列印可以讀取資料
					List<List<Map<String,String>>> printList = new ArrayList<>();
					List<Map<String,String>> rowListMap = ((List<Map<String, String>>) ((Map<String, Object>) bs.getData()).get("SUCCEED"));
					List<Map<String,String>> rowListMap2 = ((List<Map<String, String>>) ((Map<String, Object>) bs.getData()).get("FAILED"));
					printList.add(rowListMap);
					printList.add(rowListMap2);
					log.trace("rowListMap={}",rowListMap);
					SessionUtil.addAttribute(model,SessionUtil.PRINT_DATALISTMAP_DATA,printList);
				} else {
					bs.setPrevious(previous);
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}
			log.trace("target >>{}", target);
			return target;
		}

	// // 申請信用卡_輸入頁
	// @RequestMapping(value = "/card_apply_p1")
	// public String card_apply_p1(HttpServletRequest request, HttpServletResponse
	// response,
	// @RequestParam Map<String, String> reqParam, Model model) {
	// log.trace("Controller_card_apply_p1...");
	// String target = "/error";
	// BaseResult bs = null;
	// try {
	// bs = new BaseResult();
	// // 解決Trust Boundary Violation
	// Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
	// // 若登入者為公司統編 則無法使用直接導入錯誤頁
	// String custidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN,
	// null);
	// custidn = new String(Base64.getDecoder().decode(custidn), "utf-8");
	// if (custidn.length() != 10 && custidn.length() == 8) {
	// log.trace("Can't do credit card apply");
	// bs.setErrorMessage("E078", "公司統編不可執行本交易");
	// model.addAttribute(BaseResult.ERROR, bs);
	// return target;
	// }
	// // 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
	// boolean hasLocale = okMap.containsKey("locale");
	// // 判斷BACK KEY 是否有帶值表示按下上一步，抓取之前的資料顯示
	// boolean back = "Y".equals(okMap.get("back"));
	// if (hasLocale || back) {
	// log.trace("locale>>" + okMap.get("locale"));
	// bs = WebUtil.toBs(SessionUtil.getAttribute(model,
	// SessionUtil.STEP1_LOCALE_DATA, null));
	// }else{
	// // portal物件
	// String sessionId = (String) SessionUtil.getAttribute(model,
	// SessionUtil.PORTAL_SESSION_ID, null);
	// String sessionpw = (String) SessionUtil.getAttribute(model, SessionUtil.PWD,
	// null);
	// // call N810 判斷是否已擁有本行正卡
	// Boolean cardowner ;
	// bs = creditcard_apply_service.card_apply_step1(sessionId, sessionpw,
	// reqParam);
	//
	// }
	//
	// } catch (Exception e) {
	// e.printStackTrace();
	// log.error("{}", e);
	// } finally {
	// if (bs != null && bs.getResult()) {
	// target = "/creditcard/card_apply_p1";
	// } else {
	// model.addAttribute(BaseResult.ERROR, bs);
	// }
	// }
	// return target;
	// }

		/**
		 * N821(輸入頁)
		 * 
		 * @param request
		 * @param response
		 * @param reqParam
		 * @param model
		 * @return
		 */
		@RequestMapping(value = "/entity_bill_resend")
		public String entity_bill_resend(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			String target = "/error";

			BaseResult bs = null;
			BaseResult bs1 = null;
			try {
				bs = new BaseResult();
				bs1 = new BaseResult();

				// 登入時的身分證字號
				String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
				// session有無CUSIDN
				if (!"".equals(cusidn) && cusidn != null) {
					// 解密成明碼
					cusidn = new String(Base64.getDecoder().decode(cusidn));
					reqParam.put("CUSIDN", cusidn);

				} else {
					log.error("session no cusidn!!!");
				}

				Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
				// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
				if (hasLocale) {
					bs1 = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null));
					if (!bs1.getResult()) {
						// TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
						log.trace("bs.getResult() >>{}", bs.getResult());
						throw new Exception();
					} else {
						// session 資料放入 map 讓後續 model 和回上一頁取值
						okMap.putAll((Map<String, String>) bs.getData());
					}
				} else {
					// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
					bs = creditcard_apply_service.getTxToken();
					log.trace("entity_bill_resend.TXTOKEN: " + ((Map<String, String>) bs.getData()).get("TXTOKEN"));
					if (bs.getResult()) {
						SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN,
								((Map<String, String>) bs.getData()).get("TXTOKEN"));
					}

					bs1 = creditcard_apply_service.entity_bill_resend(okMap);
					SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, bs1);

				}

			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("entity_bill_resend error >> {}",e);
			} finally {
				if (bs1 != null && bs1.getResult()) {
					model.addAttribute("entity_bill_resend", bs1);
					target = "/credit_apply/entity_bill_resend";
				} else {
					model.addAttribute(BaseResult.ERROR, bs1);
				}
			}
			return target;
		}

		/**
		 * N821(確認頁)
		 * 
		 * @param request
		 * @param response
		 * @param reqParam
		 * @param model
		 * @return target String
		 */
		@RequestMapping(value = "/entity_bill_resend_confirm")
		public String entity_bill_resend_confirm(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			String target = "/error";
			log.trace("entity_bill_resend_confirm...");
			BaseResult bs = null;

			try {
				bs = new BaseResult();
				// 查詢結果走RestService
				// Base64解碼
				String cusidn = new String(
						Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
						"utf-8");
				log.trace("entity_bill_resend_confirm >>{} ", cusidn);

				reqParam.put("hideID", WebUtil.hideID(cusidn));
				reqParam.put("CUSIDN", cusidn);
				reqParam.put("CARDNUM", cusidn);
				// 解決Trust Boundary Violation
				Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

				boolean hasLocale = okMap.containsKey("locale");
				if (hasLocale) {
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP2_LOCALE_DATA, null));
					if (!bs.getResult()) {
						// TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
						log.trace("bs.getResult(): {}", bs.getResult());
						throw new Exception();
					} else {
						// session 資料放入 map 讓後續 model 和回上一頁取值
						okMap.putAll((Map<String, String>) bs.getData());
					}
				} else {
					// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
					String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
					log.trace("entity_bill_resend_confirm.token: {}", token);
					if (StrUtil.isNotEmpty(okMap.get("TOKEN")) && okMap.get("TOKEN").equals(token)) {
						bs.setResult(Boolean.TRUE);
					}
					log.trace("bs.getResult(): {}", bs.getResult());
					if (!bs.getResult()) {
						log.trace("throw new Exception()");
						throw new Exception();
					}

					// TOKEN驗證成功，資料處理後進入確認頁
					bs.reset();
					bs = creditcard_apply_service.entity_bill_resend_confirm(okMap);

					// 將查過的資料放入session，待切換 locale 時讀取
					SessionUtil.addAttribute(model, SessionUtil.STEP2_LOCALE_DATA, bs);
				}

			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("entity_bill_resend_confirm error >> {}",e);
			} finally {
				if (bs != null && bs.getResult()) {
					target = "/credit_apply/entity_bill_resend_confirm";
					model.addAttribute(SessionUtil.TRANSFER_DATA, bs);
					SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN,
							((Map<String, Object>) bs.getData()).get("TXTOKEN"));
				} else {
					model.addAttribute(BaseResult.ERROR, bs);
				}
				log.trace("target >> {}", target);
				return target;
			}
		}

		/**
		 * N821結果頁
		 * 
		 * @return
		 */
		@RequestMapping(value = "/entity_bill_resend_result")
		public String card_autopay_apply_result(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			String target = "/error";
			String next = "/CREDIT/APPLY/entity_bill_resend";
			log.trace(ESAPIUtil.vaildLog("reqParam >> " + CodeUtil.toJson(reqParam)));
			BaseResult bs = null;
			// 驗證 result token 如果存在 且相同表示F5 或i18n
			try {
				bs = new BaseResult();
				// 解決 Trust Boundary Violation
				Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

				// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
				if (hasLocale) {
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP3_LOCALE_DATA, null));
					if (!bs.getResult()) {
						log.trace("bs.getResult() >>{}", bs.getResult());
						throw new Exception();
					}
				}

				if (!hasLocale) {
					log.trace("entity_bill_resend_result.validate TXTOKEN...");
					log.trace("entity_bill_resend_result.TRANSFER_RESULT_TOKEN: "
							+ SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
					log.trace("entity_bill_resend_result.TRANSFER_RESULT_FINSH_TOKEN: "
							+ SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));

					// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
					if (StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && okMap.get("TXTOKEN")
							.equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null))) {
						// TXTOKEN第一階段驗證成功
						bs.setResult(Boolean.TRUE);
						log.trace("entity_bill_resend_result.bs.step1 is successful...");
					}

					if (!bs.getResult()) {
						log.error(ESAPIUtil.vaildLog("entity_bill_resend_result TXTOKEN 不正確，TXTOKEN >> " + okMap.get("TXTOKEN")));
						throw new Exception();
					}
					// 重置bs，繼續往下驗證
					bs.reset();

					// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
					if (StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && !okMap.get("TXTOKEN")
							.equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null))) {
						// TXTOKEN第二階段驗證成功
						bs.setResult(Boolean.TRUE);
						log.trace("entity_bill_resend_result.bs.step2 is successful...");
					}
					if (!bs.getResult()) {
						log.error(ESAPIUtil.vaildLog("entity_bill_resend_result TXTOKEN 不正確，TXTOKEN >> " + okMap.get("TXTOKEN")));
						throw new Exception();
					}
					// 重置bs，準備進行交易
					bs.reset();

					// 取得從輸入頁輸入存在session中的輸入資料
					bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
					Map<String, String> apply_data = (Map<String, String>) bs.getData();

					// 重置bs，進行交易
					bs.reset();

					okMap.putAll(apply_data);

					bs = creditcard_apply_service.entity_bill_resend_result(okMap);

					SessionUtil.addAttribute(model, SessionUtil.STEP3_LOCALE_DATA, bs);
				}

				log.trace("entity_bill_resend_result bs >>{}", bs.getData());

			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("card_autopay_apply_result error >> {}",e);
			} finally {
				// TODO 要清除必要的SESSION

				Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
				// 交易成功要存之前的token 在Session 以利下次比對是否重複交易
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TXTOKEN"));
				if (bs.getResult()) {
					target = "/credit_apply/entity_bill_resend_result";
					model.addAttribute("entity_bill_resend_result", bs);

				} else {
					// E004 是密碼錯誤 要讓使用者可以回上一步
					bs.setNext(next);
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}

			log.trace("target >> {}", target);
			return target;
		}

	/**
	 * NA021 申請信用卡進度查詢
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
    @ISTXNLOG(value="NA021")
	@RequestMapping(value = "/apply_creditcard_progress")
	public String apply_creditcard_progress(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.debug("apply_creditcard_progress start");

		String target = "/error";
		// 閱覽申請書btn路徑
		String openApplicationUrl = "/CREDIT/APPLY/openApplication";
		BaseResult bs = new BaseResult();
		try {
			cleanSession(model);
			
			String cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null));
			cusidn = new String(Base64.getDecoder().decode(cusidn), "utf-8");
			bs = creditcard_apply_service.apply_creditcard_progress(cusidn);

			if (bs.getResult()) {
				model.addAttribute("BaseResult", bs);
				Map<String, Object> bsData = (Map<String, Object>) bs.getData();
				List rows = (List) bsData.get("REC");
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, rows);
			}
			model.addAttribute("openApplicationUrl", openApplicationUrl);
		} catch (Exception e) {
			log.error("apply_creditcard_progress error", e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/credit_apply/apply_creditcard_progress";
			} else {
				bs.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * NA021 申請信用卡進度查詢-申請書閱覽
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/openApplication")
	public String openApplication(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.debug("openApplication start");
		BaseResult bs = new BaseResult();
		try {
			String rcvno = reqParam.get("rcvno");
			log.debug(ESAPIUtil.vaildLog("rcvno >> " + rcvno));
			bs = creditcard_apply_service.openApplication(rcvno);

			model.addAttribute("BaseResult", bs);
		} catch (Exception e) {
			log.error("openApplication error", e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/credit_apply/creditcard_application";
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/*
	 * 下載申請書PDF
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/downloadPDF")
	public void downloadPDF(@RequestParam Map<String, String> requestParam, HttpServletRequest request,
			HttpServletResponse response, Model model) {
		log.debug("IN downloadPDF");
		try {
			String baseURL = String.valueOf(request.getRequestURL()).replace("/CREDIT/APPLY/downloadPDF", "");
			log.debug("baseURL={}", baseURL);
			requestParam.put("baseURL", baseURL);
			Map<String,String> okMap = ESAPIUtil.validStrMap(requestParam);
			List<Map<String, Object>> listMap = (List<Map<String, Object>>) SessionUtil.getAttribute(model,
					SessionUtil.PRINT_DATALISTMAP_DATA, null);
			Map<String, Object> map = listMap.get(0);
			map = (Map<String, Object>)creditcard_apply_service.openApplication((String)map.get("RCVNO")).getData();
			log.debug("map={}", map);

			String templatePath = okMap.get("templatePath");
			log.debug(ESAPIUtil.vaildLog("templatePath >> " + templatePath));
			map.put("templatePath", templatePath);

			String logoPath = okMap.get("logoPath");
			log.debug(ESAPIUtil.vaildLog("logoPath >> " + logoPath));

			String requestURL = okMap.get("baseURL");
			log.debug(ESAPIUtil.vaildLog("requestURL >> " + requestURL));
			logoPath = requestURL + logoPath;

			log.debug(ESAPIUtil.vaildLog("logoPath >> " + logoPath));
			map.put("logoPath", logoPath);

//			TXNCARDAPPLY txnCardApply = (TXNCARDAPPLY) map.get("PO");
//			Map<String, String> poMap = BeanUtils.describe(txnCardApply);
//			log.debug("poMap={}", poMap);
			
			TXNCARDAPPLY txnCardApply = (TXNCARDAPPLY) map.get("PO");
			Map<String, String> poMap = BeanUtils.describe(txnCardApply);
			log.debug(ESAPIUtil.vaildLog("poMap >> " + CodeUtil.toJson(poMap)));
			
			if ("11".equals(poMap.get("VARSTR3"))) {
				poMap.put("view_VARSTR3", "■");
			} else {
				poMap.put("view_VARSTR3", "□");
			}
			if ("1".equals(poMap.get("CPRIMJOBTYPE"))) {
				poMap.put("view_CPRIMJOBTYPE1", "■");
				poMap.put("view_CPRIMJOBTYPE2", "□");
				poMap.put("view_CPRIMJOBTYPE3", "□");
				poMap.put("view_CPRIMJOBTYPE4", "□");
			} else if ("2".equals(poMap.get("CPRIMJOBTYPE"))) {
				poMap.put("view_CPRIMJOBTYPE1", "□");
				poMap.put("view_CPRIMJOBTYPE2", "■");
				poMap.put("view_CPRIMJOBTYPE3", "□");
				poMap.put("view_CPRIMJOBTYPE4", "□");
			} else if ("3".equals(poMap.get("CPRIMJOBTYPE"))) {
				poMap.put("view_CPRIMJOBTYPE1", "□");
				poMap.put("view_CPRIMJOBTYPE2", "□");
				poMap.put("view_CPRIMJOBTYPE3", "■");
				poMap.put("view_CPRIMJOBTYPE4", "□");
			} else if ("4".equals(poMap.get("CPRIMJOBTYPE"))) {
				poMap.put("view_CPRIMJOBTYPE1", "□");
				poMap.put("view_CPRIMJOBTYPE2", "□");
				poMap.put("view_CPRIMJOBTYPE3", "□");
				poMap.put("view_CPRIMJOBTYPE4", "■");
			} else {
				poMap.put("view_CPRIMJOBTYPE1", "□");
				poMap.put("view_CPRIMJOBTYPE2", "□");
				poMap.put("view_CPRIMJOBTYPE3", "□");
				poMap.put("view_CPRIMJOBTYPE4", "□");
			}
			if ("1".equals(poMap.get("CNOTE2"))) {
				poMap.put("view_CNOTE21", "■");
				poMap.put("view_CNOTE22", "□");
			} else {
				poMap.put("view_CNOTE21", "□");
				poMap.put("view_CNOTE22", "■");
			}
			if ("1".equals(poMap.get("MCASH"))) {
				poMap.put("view_MCASH1", "■");
				poMap.put("view_MCASH2", "□");
			} else {
				poMap.put("view_MCASH1", "□");
				poMap.put("view_MCASH2", "■");
			}
			if ("1".equals(poMap.get("CNOTE1"))) {
				poMap.put("view_CNOTE11", "■");
				poMap.put("view_CNOTE12", "□");
			} else {
				poMap.put("view_CNOTE11", "□");
				poMap.put("view_CNOTE12", "■");
			}
			if ("1".equals(poMap.get("CNOTE3"))) {
				poMap.put("view_CNOTE31", "■");
				poMap.put("view_CNOTE32", "□");
			} else {
				poMap.put("view_CNOTE31", "□");
				poMap.put("view_CNOTE32", "■");
			}

			for (Entry<String, String> entry : poMap.entrySet()) {
				map.put("po_" + entry.getKey(), entry.getValue());
			}

			PDFUtil pdfUtil = new PDFUtil();

			InputStream inputStream = pdfUtil.HTMLtoPDF(pdfUtil.getPDFHTML(map));

			if (inputStream != null) {
				response.setContentType("application/pdf;charset=UTF-8");

				String pdfName = i18n.getMsg("LB.X1651");
				pdfName = pdfName + ".pdf";
				log.debug("pdfName={}", pdfName);

//				String i18nFileName = i18n.getMsg("LB.X1651");
//				log.debug("i18nFileName={}", i18nFileName);
//
//				if (i18nFileName != null && !"".equals(i18nFileName)) {
//					pdfName = i18nFileName.replaceAll(" ", "") + ".pdf";
//				} else {
//					pdfName = pdfName + ".pdf";
//				}
				log.debug("pdfName={}", pdfName);

				response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(pdfName, "UTF-8"));

				IOUtils.copy(inputStream, response.getOutputStream());
			}
		} catch (Exception e) {
			log.error("", e);
		}
	}

	/**
	 * 取得轉帳之轉出帳號
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getOutAcno_aj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult getOutACNO(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String str = "getOutAcno_aj";
		log.trace("getOutAcno_aj >> {}", str);
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		// 要取得使用者相關帳號
		BaseResult bs = new BaseResult();
		try {
			// String sessionID = (String) SessionUtil.getAttribute(model,
			// SessionUtil.PORTAL_SESSION_ID, null);
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			String type = reqParam.get("type");
			bs = creditcard_apply_service.getOutAcnoList(cusidn, type);

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getOutACNO error >> {}",e);
		}
		return bs;

	}

	/**
	 * NI02預借現金密碼函補寄(輸入頁)
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/cash_pw_resend")
	public String cash_pw_resend(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {

		String target = "/error";
		// 處理結果
		BaseResult bs = null;
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
				// 清除切換語系時暫存的資料
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");

				// 登入時的身分證字號
				String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
				// session有無CUSIDN
				if (!"".equals(cusidn) && cusidn != null) {
					// 解密成明碼
					cusidn = new String(Base64.getDecoder().decode(cusidn));
					okMap.put("CUSIDN", cusidn);

				} else {
					log.error("session no cusidn!!!");
				}

				bs = new BaseResult();

				// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
				bs = creditcard_apply_service.getTxToken();
				log.trace("entity_bill_resend.TXTOKEN: " + ((Map<String, String>) bs.getData()).get("TXTOKEN"));
				if (bs.getResult()) {
					SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN,
							((Map<String, String>) bs.getData()).get("TXTOKEN"));
				}

				bs.reset();

				Boolean isIkeyUserB = (Boolean) SessionUtil.getAttribute(model, SessionUtil.ISIKEYUSER, null);
				String isIkeyUserS = "";
				if (isIkeyUserB) {
					isIkeyUserS = "true";
				} else {
					isIkeyUserS = "false";
				}
				okMap.put("ISIKEYUSER", isIkeyUserS);

				bs = creditcard_apply_service.cash_pw_resend(okMap);

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("cash_pw_resend error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/credit_apply/cash_pw_resend";
				model.addAttribute("cash_pw_resend", bs);
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		log.trace("target >>{} ", target);
		return target;
	}
    
	/**
	 * NI02預借現金密碼函補寄(確認頁)
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/cash_pw_resend_confirm")
	public String cash_pw_resend_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {

		log.trace("cash_pw_resend_confirm...");
		String target = "/error";
		String jsondc = "";
		String next = "/CREDIT/APPLY/cash_pw_resend";
		log.trace(ESAPIUtil.vaildLog("cash_pw_resend_confirm_reqParam >> " + CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		String flag = "0";
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

		try {
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if (!bs.getResult()) {
					// TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				} else {
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			} else {
				
				String cusidn = new String(
						Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
						"utf-8");
				okMap.put("CUSIDN", cusidn);
				
				bs = daoservice.getAgreeAcnoListSep(okMap.get("CUSIDN"), "creditcard_pay_inacno",false);
				if (bs.getResult()) {
					Map<String, Object> data = (Map<String, Object>) bs.getData();
					
					List<Map<String, String>> table_list = (List<Map<String, String>>) data.get("REC2");

					for (Map compareMap : table_list) {
						log.debug("compareMap >>{}" , compareMap);
						if (okMap.get("CARDNUM").equals(
								new Gson().fromJson((String)compareMap.get("VALUE"), Map.class)
								.get("ACN"))) {
							flag = "1";
						}
					}
				}
				okMap.put("FLAG",flag );
				bs.reset();
				
				// IKEY要使用的JSON:DC
				jsondc = URLEncoder.encode(CodeUtil.toJson(okMap), "UTF-8");
				log.trace(ESAPIUtil.vaildLog("cash_pw_resend_confirm.jsondc >> " + jsondc));
				okMap.put("jsondc", jsondc);
				log.trace(ESAPIUtil.vaildLog("cash_pw_resend_confirm.okMap >> " + CodeUtil.toJson(okMap)));
				// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
				String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
				log.trace("creditcard_pay_confirm.token: {}", token);
				if (StrUtil.isNotEmpty(okMap.get("TOKEN")) && okMap.get("TOKEN").equals(token)) {
					bs.setResult(Boolean.TRUE);
				}
				log.trace("bs.getResult(): {}", bs.getResult());
				if (!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				}

				// TOKEN驗證成功，資料處理後進入確認頁
				bs.reset();
				bs = creditcard_apply_service.cash_pw_resend_confirm(okMap);

				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("cash_pw_resend_confirm error >> {}",e);
		} finally {
			if (bs.getResult()) {
				target = "/credit_apply/cash_pw_resend_confirm";
				model.addAttribute(SessionUtil.TRANSFER_DATA, bs);
				//因為切換語系需要
				model.addAttribute("cash_pw_resend_confirm", bs);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, ((Map<String,Object>)bs.getData()).get("TXTOKEN"));
			} else {
				bs.setNext(next);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * NI02預借現金密碼函補寄(結果頁)
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/cash_pw_resend_result")
	public String cash_pw_resend_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		
		String target = "/error";
		String next = "/CREDIT/APPLY/cash_pw_resend";
		log.trace(ESAPIUtil.vaildLog("reqParam >> " + CodeUtil.toJson(reqParam)));
		BaseResult bs = null;
		// 驗證 result token 如果存在 且相同表示F5 或i18n
		try {
			bs = new BaseResult();
			// 解決 Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		//	Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI

			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}else {
				log.trace("cash_pw_resend_result.validate TXTOKEN...");
				log.trace("cash_pw_resend_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("cash_pw_resend_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("cash_pw_resend_result.bs.step1 is successful...");
				}
				if (!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("cash_pw_resend_result TXTOKEN 不正確，或重複交易，TXTOKEN >> " + okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						!okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("cash_pw_resend_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("cash_pw_resend_result TXTOKEN 不正確，或重複交易，TXTOKEN >> " + okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				
				// 取得從輸入頁輸入存在session中的輸入資料
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String, String> apply_data = (Map<String, String>) bs.getData();
				
				// 重置bs，進行交易
				bs.reset();
				
				okMap.putAll(apply_data);
				
				bs = creditcard_apply_service.cash_pw_resend_result(okMap);
				
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			
			log.trace("cash_pw_resend_result bs >>{}", bs.getData());

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("cash_pw_resend_result error >> {}",e);
		} finally {
			// TODO 要清除必要的SESSION
			
			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			// 交易成功要存之前的token 在Session 以利下次比對是否重複交易
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TXTOKEN"));
			if (bs.getResult()) {
				target = "/credit_apply/cash_pw_resend_result";
				model.addAttribute("cash_pw_resend_result", bs);
				
			} else {
				// E004 是密碼錯誤 要讓使用者可以回上一步
				bs.setNext(next);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		log.trace("target >> {}", target);
		return target;
	}
	
	/**
	 * 申請信用卡卡片QRCODE頁
	 */
	@RequestMapping(value = "/creditcard_apply_qrcode")
	public String creditcard_apply_qrcode(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> requestParam, Model model) {
		log.debug("creditcard_apply_qrcode");
		BaseResult bs = null;
		String target = "/credit_apply/creditcard_apply_qrcode";
		return target;
	}
	
	/**
	 * 申請信用卡獅友會頁
	 */
	@RequestMapping(value = "/creditcard_apply_lcard")
	public String creditcard_apply_lcard(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> requestParam, Model model) {
		log.debug("creditcard_apply_lcard");
		BaseResult bs = null;
		String target = "/credit_apply/creditcard_apply_lcard";
		return target;
	}

//  OLD 信用卡申請流程
//	/**
//	 * 申請信用卡元件頁
//	 */
//	@RequestMapping(value = "/apply_creditcard")
//	public String apply_creditcard_outside(HttpServletRequest request, HttpServletResponse response,
//			@RequestParam Map<String, String> requestParam, Model model) {
//		log.debug("apply_creditcard");
//		BaseResult bs = null;
//		String target = "/online_apply/ErrorWithoutMenu";
//		String cusidn = "";
//		try {
//			log.trace(ESAPIUtil.vaildLog("requestParam >> " + CodeUtil.toJson(requestParam)));
//			// 解決Trust Boundary Violation
//			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
//			
//			try {
//				cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
//			}
//			catch (Exception e) {
//				// TODO: handle exception
//			}
//			
//			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
//			boolean hasLocale = okMap.containsKey("locale");
//			if (hasLocale)
//			{
//				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null));
//			}else {
//				bs = new BaseResult();
//				
//				//防止使用者不經輸入頁進入
//				bs = creditcard_apply_service.getTxToken();
//				if (bs.getResult()) {
//					SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN,
//							((Map<String, String>) bs.getData()).get("TXTOKEN"));
//				}
//				bs.reset();
//				
//				String QRCODE = requestParam.get("QRCODE");
//				log.debug(ESAPIUtil.vaildLog("QRCODE >> " + QRCODE));
//				if (QRCODE == null) {
//					QRCODE = "";
//				}
//				
//				if(null!=requestParam.get("branch")) {
//					bs.addData("branch", requestParam.get("branch"));
//				}
//				
//				bs.addData("QRCODE", QRCODE);
//				
//				if(StrUtil.isNotEmpty(cusidn)) {
//					bs.addData("CUSIDN", cusidn);
//				}
//	
//				// IKEY要使用的JSON:DC
//				String jsondc = URLEncoder.encode(CodeUtil.toJson(requestParam), "UTF-8");
//				log.debug(ESAPIUtil.vaildLog("jsondc" + jsondc));
//				bs.addData("jsondc", jsondc);
//	
//				//判斷瀏覽器會用到
//				String userAgent = request.getHeader("user-agent");
//				log.debug(ESAPIUtil.vaildLog("userAgent >> " + userAgent)); 
//				userAgent = userAgent.replace("\"","").replace("<","").replace(">","").replace("%","").replace("&","").replace("[","").replace("]","");
//				userAgent = ESAPIUtil.validInput(userAgent,"GeneralString",true);
//				log.debug(ESAPIUtil.vaildLog("userAgent >> " + userAgent)); 
//				bs.addData("userAgent",userAgent);
//				bs.setResult(true);
//				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, bs);
//			}
//		} catch (Exception e) {
//			//Avoid Information Exposure Through an Error Message
//			//e.printStackTrace();
//			log.error("apply_creditcard_outside error >> {}",e);
//		}finally {
//			if(bs != null && bs.getResult()) {
//				model.addAttribute("result_data",bs);
//				target = "/credit_apply/creditcard_apply_p1";
//			}else {
////				bs.setPrevious("/DIGITAL/ACCOUNT/apply_digital_account_p1");
//				model.addAttribute(BaseResult.ERROR, bs);
//			}
//		}
//		return target;
//	}
	
//	/**
//	 * 申請信用卡卡片選擇頁
//	 */
//	@IPVERIFY(cusidnKey="CUSIDN")
//	@RequestMapping(value = "/apply_creditcard_p2")
//	public String apply_creditcard_outside_p2(HttpServletRequest request, HttpServletResponse response,
//			@RequestParam Map<String, String> requestParam, Model model) {
//		log.debug("apply_creditcard_p2");
//		BaseResult bs = null;
//		String target = "/online_apply/ErrorWithoutMenu";
//		try {
//			log.trace(ESAPIUtil.vaildLog("P2requestParam >> " + CodeUtil.toJson(requestParam)));
//			bs = new BaseResult();
//			// 解決Trust Boundary Violation
////			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
//			Map<String, String> okMap = requestParam;
//			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
//			boolean hasLocale = okMap.containsKey("locale");
//			String BACK = requestParam.get("back");
//			if (hasLocale || (StrUtil.isNotEmpty(BACK) && BACK.equals("Y")))
//			{
//				bs = creditcard_apply_service.getTxToken();
//				if (bs.getResult()) {
//					SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN,((Map<String, String>) bs.getData()).get("TXTOKEN"));
//				}
//				bs.reset();
//				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP2_LOCALE_DATA, null));
//			}
//			else {
//				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null))) {
//					SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, "");
//					bs = creditcard_apply_service.getTxToken();
//					if (bs.getResult()) {
//						SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN,
//								((Map<String, String>) bs.getData()).get("TXTOKEN"));
//					}
//					bs.reset();
//					
//					String cusidn = requestParam.get("CUSIDN");
//					log.debug(ESAPIUtil.vaildLog("cusidn >> " + cusidn));
//					requestParam.put("UID", cusidn);
//					
//					bs = creditcard_apply_service.query_creditCard_data(requestParam);
//					log.debug("bs.getData={}", bs.getData());
//					SessionUtil.addAttribute(model, SessionUtil.STEP2_LOCALE_DATA, bs);
//				}
//				else {
//					log.trace("throw new Exception()");
//					throw new Exception();
//				}
//				
//			}
//		} catch (Exception e) {
//			//Avoid Information Exposure Through an Error Message
//			//e.printStackTrace();
//			log.error("apply_creditcard_outside_p2 error >> {}",e);
//		}finally {
//			if(bs != null && bs.getResult()) {
//				model.addAttribute("result_data",bs);
//				target = "/credit_apply/creditcard_apply_p2";
//			}else {
//				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, "");
//				bs.setPrevious("/CREDIT/APPLY/apply_creditcard"+null!=requestParam.get("BRANCH")?"?BRANCH="+requestParam.get("BRANCH"):"");
//				model.addAttribute(BaseResult.ERROR, bs);
//			}
//		}
//		return target;
//	}
	
//	/**
//	 * 申請信用卡約定條款同意頁
//	 */
//	@RequestMapping(value = "/apply_creditcard_p3")
//	public String apply_creditcard_p3(HttpServletRequest request, HttpServletResponse response,
//			@RequestParam Map<String, String> requestParam, Model model) {
//		log.debug("apply_creditcard_p3");
//		BaseResult bs = null;
//		String target = "/online_apply/ErrorWithoutMenu";
//		try {
//			log.trace(ESAPIUtil.vaildLog("P3requestParam >> " + CodeUtil.toJson(requestParam)));
//			bs = new BaseResult();
//			// 解決Trust Boundary Violation
//			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
//			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
//			boolean hasLocale = okMap.containsKey("locale");
//			String BACK = requestParam.get("back");
//			if (hasLocale || (StrUtil.isNotEmpty(BACK) && BACK.equals("Y")))
//			{
//				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP3_LOCALE_DATA, null));
//			}else {
//				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null))) {
//					bs = creditcard_apply_service.prepareContactData(requestParam);
//					SessionUtil.addAttribute(model, SessionUtil.STEP3_LOCALE_DATA, bs);
//				}
//			}
//		} catch (Exception e) {
//			//Avoid Information Exposure Through an Error Message
//			//e.printStackTrace();
//			log.error("apply_creditcard_p3 error >> {}",e);
//		}finally {
//			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, "");
//			if(bs != null && bs.getResult()) {
//				model.addAttribute("result_data",bs);
//				target = "/credit_apply/creditcard_apply_p3";
//			}else {
//				bs.setPrevious("/CREDIT/APPLY/apply_creditcard"+null!=requestParam.get("BRANCH")?"?BRANCH="+requestParam.get("BRANCH"):"");
//				model.addAttribute(BaseResult.ERROR, bs);
//			}
//		}
//		return target;
//	}
	
	/**
	 * 申請信用卡列表頁
	 */
	@RequestMapping(value = "/apply_creditcard")
	public String apply_creditcard_card_select(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> requestParam, Model model) {

		log.info("apply_creditcard_card_select req >> {} ", requestParam);
		String target = "/online_apply/ErrorWithoutMenu";
		BaseResult bs = new BaseResult();
		try {
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			
			
			
			//URL 傳回來的值
			// 是否通過身分驗證
			String passAuth = StrUtil.isNotEmpty(okMap.get("passAuth"))?"Y":"N";
			log.info("passAuth:{}",passAuth);
			// 是否重複申請
			boolean doubleApply = StrUtil.isNotEmpty(okMap.get("doubleApply"))?true:false;
			log.info("doubleApply:{}",doubleApply);
			// 是否持有卡片
			boolean hasThisCard = StrUtil.isNotEmpty(okMap.get("hasThisCard"))?true:false;
			log.info("hasThisCard:{}",hasThisCard);
			//
			
			// 是否為上一頁按鈕跳轉 
			String back = StrUtil.isNotEmpty(okMap.get("back"))?"Y":"N";
			log.info("back:{}",back);
			
			//從頭開始
			//如果不是從上一頁或驗證過轉導回來卡片選擇頁 , 把 OLDCARDOWNERCHK session 清空
			if("N".equals(back) && "N".equals(passAuth)) {
				SessionUtil.addAttribute(model, SessionUtil.OLDCARDOWNERCHK, null);
			}
			
			bs = creditcard_apply_service.getTxToken();
			if (bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN,
						((Map<String, String>) bs.getData()).get("TXTOKEN"));
			}
			bs.reset();
			
			switch (passAuth + back) {
			//未通過身分驗證 , 非上一頁回來 = >>>>> 第一次進入
			case "NN":
			//未通過身分驗證 , 由上一頁回來 = >>>>> 等同第一次進入
			case "NY":
				// QRCODE , 目前有 Y,L 其餘都轉成空
				// QRCODE = L , 只顯示獅子卡
				// QRCODE = Y , QRCODE
				String QRCODE = requestParam.get("QRCODE");
				log.debug(ESAPIUtil.vaildLog("QRCODE >> " + QRCODE));
				if (QRCODE == null || !"Y".equals(QRCODE) || !"L".equals(QRCODE) ) {
					QRCODE = "";
				}

				// 特殊branch , 目前只有040 其餘都轉成空
				String BRANCH = requestParam.get("branch");
				log.debug(ESAPIUtil.vaildLog("BRANCH >> " + BRANCH));
				if (BRANCH == null || !"040".equals(BRANCH)) {
					BRANCH = "";
				}
				
				//如果兩個同時來  當作錯誤導向普通頁面
				if(StrUtils.isNotEmpty(QRCODE) && StrUtils.isNotEmpty(BRANCH)) {
					BRANCH = "";
					QRCODE = "";
				}
				
				requestParam.put("BRANCH", BRANCH);
				
				bs = creditcard_apply_service.cardSelectData(requestParam, bs);
				
				//session值是全卡片列表
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, bs);
				
				break;
				//通過身分驗證 , 不是由上一頁回來 = URL轉導 >>>>> 濾掉已持有的卡片及在頁面上放上已申請的列表供按鈕判斷
			case "YN":
				//取得  apply_creditcard_terms 所記的STEP1_LOCALE_DATA (有 已申請跟已持有 )
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null));
				//參數1 >> 直接用 ({CMQTIME=2022/03/16 17:38:20, CMRECNUM=25, CRLIMIT=000800000, CSHLIMIT=000160000, ACCBAL=-00338949, CSHBAL=00000000, INT_RATE=15.000, PAYMT_ACCT=99999999999, CURR_BAL=02569630, TOTL_DUE=02506801, STOP_DAY=1060121, STMT_DAY=1060107, DBCODE=01, RECNUM=05, REC=[{CARDTYPE=200, TYPENAME= VISA 普卡, CR_NAME=歐峯峯, BLK_CODE=, CARDNUM=4938250000728113, PT_FLG=1, LOGINTYPE=NB}, {CARDTYPE=400, TYPENAME= MASTER普卡, CR_NAME=黃ＯＯ, BLK_CODE=, CARDNUM=5437700040614106, PT_FLG=1, LOGINTYPE=NB}, {CARDTYPE=888, TYPENAME=COMBO鈦金卡, CR_NAME=江ＯＯ, BLK_CODE=, CARDNUM=5242638800730107, PT_FLG=1, LOGINTYPE=NB}, {CARDTYPE=906, TYPENAME= VISA故宮御璽悠遊卡, CR_NAME=古ＯＯ, BLK_CODE=, CARDNUM=4712350000002105, PT_FLG=1, LOGINTYPE=NB}, {CARDTYPE=607, TYPENAME=信保基金111關懷商務卡, CR_NAME=洪ＯＯ, BLK_CODE=, CARDNUM=4516975010004101, PT_FLG=2, LOGINTYPE=NB}, {CARDTYPE=, TYPENAME=, CR_NAME=, BLK_CODE=, CARDNUM=, PT_FLG=, LOGINTYPE=NB}, {CARDTYPE=, TYPENAME=, CR_NAME=, BLK_CODE=, CARDNUM=, PT_FLG=, LOGINTYPE=NB}, {CARDTYPE=, TYPENAME=, CR_NAME=, BLK_CODE=, CARDNUM=, PT_FLG=, LOGINTYPE=NB}, {CARDTYPE=, TYPENAME=, CR_NAME=, BLK_CODE=, CARDNUM=, PT_FLG=, LOGINTYPE=NB}, {CARDTYPE=, TYPENAME=, CR_NAME=, BLK_CODE=, CARDNUM=, PT_FLG=, LOGINTYPE=NB}, {CARDTYPE=, TYPENAME=, CR_NAME=, BLK_CODE=, CARDNUM=, PT_FLG=, LOGINTYPE=NB}, {CARDTYPE=, TYPENAME=, CR_NAME=, BLK_CODE=, CARDNUM=, PT_FLG=, LOGINTYPE=NB}, {CARDTYPE=, TYPENAME=, CR_NAME=, BLK_CODE=, CARDNUM=, PT_FLG=, LOGINTYPE=NB}, {CARDTYPE=, TYPENAME=, CR_NAME=, BLK_CODE=, CARDNUM=, PT_FLG=, LOGINTYPE=NB}, {CARDTYPE=, TYPENAME=, CR_NAME=, BLK_CODE=, CARDNUM=, PT_FLG=, LOGINTYPE=NB}, {CARDTYPE=, TYPENAME=, CR_NAME=, BLK_CODE=, CARDNUM=, PT_FLG=, LOGINTYPE=NB}, {CARDTYPE=, TYPENAME=, CR_NAME=, BLK_CODE=, CARDNUM=, PT_FLG=, LOGINTYPE=NB}, {CARDTYPE=, TYPENAME=, CR_NAME=, BLK_CODE=, CARDNUM=, PT_FLG=, LOGINTYPE=NB}, {CARDTYPE=, TYPENAME=, CR_NAME=, BLK_CODE=, CARDNUM=, PT_FLG=, LOGINTYPE=NB}, {CARDTYPE=, TYPENAME=, CR_NAME=, BLK_CODE=, CARDNUM=, PT_FLG=, LOGINTYPE=NB}, {CARDTYPE=, TYPENAME=, CR_NAME=, BLK_CODE=, CARDNUM=, PT_FLG=, LOGINTYPE=NB}, {CARDTYPE=, TYPENAME=, CR_NAME=, BLK_CODE=, CARDNUM=, PT_FLG=, LOGINTYPE=NB}, {CARDTYPE=, TYPENAME=, CR_NAME=, BLK_CODE=, CARDNUM=, PT_FLG=, LOGINTYPE=NB}, {CARDTYPE=, TYPENAME=, CR_NAME=, BLK_CODE=, CARDNUM=, PT_FLG=, LOGINTYPE=NB}, {CARDTYPE=, TYPENAME=, CR_NAME=, BLK_CODE=, CARDNUM=, PT_FLG=, LOGINTYPE=NB}], LOGINTYPE=NB, msgCode=0, TOPMSG=, ApplingCardListStr=38,51,13,27,11,31,32,41,45,69,46,52,80,68,81,26,85,48,82,66,65,63,61, doubleApply=true, oldcardowner=Y, CFU2=2, owncardStr=200,400,888,906, owncardSet=[200, 400, 888, 906], DD=01, ATTIBK=004, capCode=UFNKCF, CARDNUM=, jsondc=, branch=#, ADOPID=NA03, CPRIMBIRTHDAYshow=2002年 01月 01日, DPBHNO=004-台灣銀行, TOKEN=e6a0183fbdcf489a8b3d7e94f047ee62, AUTHCODE=, ACNNO=, CPRIMBIRTHDAY=0910101, memberId=, MM=01, YY=091, EXPIREDYEAR=, CUSIDN4=, VARSTR2=1, CARDNUM2=, CARDNAME=41, passAuth=, CARDNUM3=, CARDNUM4=, CertB64=, TRMID=, smartcard=, CARDNUM1=, HiCertType=, UID=A123456814, ATMOBI=, NPCPIN=, ATTIAC=, CertNotAfter=, ICSEQ=, pkcs7Sign=, CVCTWO=, ENV=D, FGTXWAY=2, partition=#, QRCODE=, EXPIREDYM=, BIRTH=, ReadFlag=on, AUTHCODE1=, OUTACN=, CertNotBefore=, BRANCH=, CertSerial=, userAgent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36, CN=永續生活悠遊鈦金卡, ACN=, EXPIREDDATEMONTHS=, CertIssuer=, CertSubject=, CertFinger=, ISSUER=, NOTIEINSTALL=, TAC=, BACK=, iSeqNo=, CUSIDN=A123456814}
				bs = creditcard_apply_service.cardSelectData2(bs,"1");
				bs.addData("passAuth", passAuth);
				if(doubleApply) bs.addData( "doubleApply" , "Y" );
				if(hasThisCard) bs.addData( "hasThisCard" , "Y" );
				
				//session值是過濾過的列表
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, bs);
				
				break;
				
				//通過身分驗證 , 由上一頁回來 = 條款頁按上一頁>>>>> 要跳過身分驗證頁 , 要判斷已持有的卡片及申請中的卡片 
			case "YY":
				//取得  apply_creditcard_terms 所記的STEP3_LOCALE_DATA (有 已申請跟已持有 )
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP3_LOCALE_DATA, null));
				//參數2 >> 要從 requestParam 取得 已申請跟已持有
				bs = creditcard_apply_service.cardSelectData2(bs,"2");
				bs.addData("passAuth", passAuth);
				
				//session值是過濾過的列表 加上  已申請跟已持有 加上 passAuth 
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, bs);
				
			default:
				log.error("passAuth+back condition error >> {} ", passAuth+back);
				break;
			}
			
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("apply_creditcard_card_select error >> {}", e.toString());
		} finally {
			if (bs != null && bs.getResult()) {
				
				model.addAttribute("result_data", bs);
				target = "/credit_apply/creditcard_apply_card_select";
			} else {
				// bs.setPrevious("/DIGITAL/ACCOUNT/apply_digital_account_p1");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 申請信用卡身份驗證頁
	 */
	@IPVERIFY(cusidnKey="CUSIDN")
	@RequestMapping(value = "/creditcard_apply_authentication")
	public String apply_creditcard_card_authentication(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> requestParam, Model model) {
		log.info("apply_creditcard_card_authentication req >> {} ", requestParam);
		BaseResult bs = new BaseResult();
		String target = "/online_apply/ErrorWithoutMenu";
		try {
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			String back = StrUtil.isNotEmpty(okMap.get("back"))?"Y":"N";
			if ("Y".equals(back))
			{
				bs = creditcard_apply_service.getTxToken();
				if (bs.getResult()) {
					SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN,((Map<String, String>) bs.getData()).get("TXTOKEN"));
				}
				bs.reset();
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP2_LOCALE_DATA, null));
			}
			else {
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null))) {
					
					SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, "");
					bs = creditcard_apply_service.getTxToken();
					if (bs.getResult()) {
						SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN,
								((Map<String, String>) bs.getData()).get("TXTOKEN"));
					}
					bs.reset();
					
					
					//判斷瀏覽器會用到
					String userAgent = request.getHeader("user-agent");
					log.debug(ESAPIUtil.vaildLog("userAgent >> " + userAgent)); 
					userAgent = userAgent.replace("\"","").replace("<","").replace(">","").replace("%","").replace("&","").replace("[","").replace("]","");
					userAgent = ESAPIUtil.validInput(userAgent,"GeneralString",true);
					
					okMap.put("userAgent", userAgent);
					bs.addAllData(okMap);
					log.info("apply_creditcard_card_authentication bs.getData={}", bs.getData());
					
					
				}
				else {
					log.trace("throw new Exception()");
					throw new Exception();
				}
				bs.setResult(true);
			}
		} catch (Exception e) {
			log.error("apply_creditcard_card_authentication error >> {}",e);
		}finally {
			if(bs != null && bs.getResult()) {
//				//開發跳過晶片卡用 , 測試套正試不該生效
//				String env= SpringBeanFactory.getBean("MAIL_SUBJECT_ENV");
//				bs.addData("ENV",env);
//				//
				model.addAttribute("result_data",bs);
				target = "/credit_apply/creditcard_apply_authentication";
				//session 值是所選卡片
				SessionUtil.addAttribute(model, SessionUtil.STEP2_LOCALE_DATA, bs);
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * 申請信用卡約定條款同意頁
	 */
	@RequestMapping(value = "/apply_creditcard_terms")
	public String apply_creditcard_p3(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> requestParam, Model model) {
		log.info("apply_creditcard_terms req >> {}", requestParam);
		BaseResult bs = new BaseResult();
		String target = "/online_apply/ErrorWithoutMenu";
		Map<String, String> okMap = new HashMap<String, String>();
		try {
			okMap = ESAPIUtil.validStrMap(requestParam);
			String back = okMap.get("back");
			String passAuth = okMap.get("passAuth");
			
			if (StrUtil.isNotEmpty(back) && back.equals("Y"))
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP3_LOCALE_DATA, null));
			} else {
				if (StrUtil.isNotEmpty(okMap.get("TOKEN")) && okMap.get("TOKEN")
						.equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null))) {

					if (StrUtil.isNotEmpty(passAuth) && passAuth.equals("Y")) {
						// 如果是從通過驗證來的 , 要先取STEP1_LOCALE_DATA 然後用新選擇的卡片資料蓋過去去產生第三頁的條款
						bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null));
						if(null == ((Map)bs.getData()).get("requestParam")) {
							((Map)bs.getData()).putAll(okMap);
							bs = creditcard_apply_service.prepareContactData((Map<String, String>) bs.getData());
						}else {
							Map bsData = (Map<String, Object>)bs.getData();
							Map reqmap= (Map) bsData.get("requestParam");
							reqmap.putAll(okMap);
							bsData.putAll(reqmap);
							bs = creditcard_apply_service.prepareContactData((Map<String,String>)bsData);
						}
						
						// session 所記的是 新所選卡片 及已持有已申請 ( 是放在 requestParam ... 考慮移出來中)
						SessionUtil.addAttribute(model, SessionUtil.STEP3_LOCALE_DATA, bs);
						
					} else {
						// 取出第一頁內容所選的卡片資訊
						bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP2_LOCALE_DATA, null));
						Map<String, String> bsData = (Map) bs.getData();
						okMap.putAll(bsData);
						
						//進行驗證
						bs = creditcard_apply_service.card_apply_authentication_applyCheck(okMap);
						bsData = (Map) bs.getData();
						
						// 設定舊戶檢核結果session
						if( "Y".equals((String)bsData.get("oldcardowner"))) {
							SessionUtil.addAttribute(model, SessionUtil.OLDCARDOWNERCHK, (String)bsData.get("oldcardowner"));
						}
						
						okMap.putAll(bsData);
						// pass card_apply_authentication_doubleApplyCheck
						if (bs.getResult()) {
							//成功通過驗證且沒有重複申請或已持有
							bs = creditcard_apply_service.prepareContactData(okMap);
							//session 存放條款頁資料
							SessionUtil.addAttribute(model, SessionUtil.STEP3_LOCALE_DATA, bs);
						
						//有重複申請或已持有  (已持有 > 重複申請)
						} else {
							switch (bs.getMsgCode()) {
							case "doubleApply":
								//session包含所選卡片資料 及 驗證結果 ( 已申請列表 及 已持有卡片 set )
								SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, bs);
								target = "redirect:/CREDIT/APPLY/apply_creditcard?passAuth=Y&doubleApply=Y";
								break;
							case "hasThisCard":
								//session包含所選卡片資料 及 驗證結果 ( 已申請列表 及 已持有卡片 set )
								SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, bs);
								target = "redirect:/CREDIT/APPLY/apply_creditcard?passAuth=Y&hasThisCard=Y";
								break;
							default:
								break;
							}
						}
					}
				}
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("apply_creditcard_terms error >> {}",e);
		}finally {
			if(bs != null && bs.getResult()) {
				model.addAttribute("result_data",bs);
				target = "/credit_apply/apply_creditcard_terms";
			}else {
				bs.setPrevious("/CREDIT/APPLY/apply_creditcard"+null!=okMap.get("BRANCH")?"?BRANCH="+okMap.get("BRANCH"):"");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
//	/**
//	 * 申請信用卡約定條款同意頁不同意或回上一步
//	 */
//	@RequestMapping(value = "/apply_creditcard_p3_notagree")
//	public String apply_creditcard_p3_notagree(HttpServletRequest request, HttpServletResponse response,
//			@RequestParam Map<String, String> requestParam, Model model) {
//		log.debug("apply_creditcard_p3_notagree");
//		BaseResult bs = null;
//		String target = "/online_apply/ErrorWithoutMenu";
//		try {
//			bs = new BaseResult();
//			// 解決Trust Boundary Violation
//			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
//			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
//			boolean hasLocale = okMap.containsKey("locale");
//			if (hasLocale)
//			{
//				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP2_LOCALE_DATA, null));
//			}else {
//				String cusidn = requestParam.get("CUSIDN");
//				log.debug(ESAPIUtil.vaildLog("cusidn >> " + cusidn));
//				requestParam.put("UID", cusidn);
//	
//				bs = creditcard_apply_service.query_creditCard_data2(requestParam, model);
//				log.debug("bs.getData={}", bs.getData());
//				SessionUtil.addAttribute(model, SessionUtil.STEP2_LOCALE_DATA, bs);
//			}
//		} catch (Exception e) {
//			//Avoid Information Exposure Through an Error Message
//			//e.printStackTrace();
//			log.error("apply_creditcard_p3_notagree error >> {}",e);
//		}finally {
//			if(bs != null && bs.getResult()) {
//				model.addAttribute("result_data",bs);
//				target = "/credit_apply/creditcard_apply_p2";
//			}else {
//				bs.setPrevious("/CREDIT/APPLY/apply_creditcard"+null!=requestParam.get("BRANCH")?"?BRANCH="+requestParam.get("BRANCH"):"");
//				model.addAttribute(BaseResult.ERROR, bs);
//			}
//		}
//		return target;
//	}

	/**
	 * 申請信用卡輸入頁
	 */
	@RequestMapping(value = "/apply_creditcard_p4")
	public String apply_creditcard_p4(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> requestParam, Model model) {
		log.debug("apply_creditcard_p4");
		BaseResult bs = null;
		Map<String,Object> bsData = null;
		Map<String,Object> bsData1 = null;
		String target = "/online_apply/ErrorWithoutMenu";
		try {
			log.trace(ESAPIUtil.vaildLog("P4requestParam >> " + CodeUtil.toJson(requestParam)));
			bs = new BaseResult();
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP4_LOCALE_DATA, null));
			}
			else if(requestParam.get("back").equals("Y")) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP4_2_LOCALE_DATA, null));
				Map<String, String> bsdata = (Map) bs.getData();
				log.debug("bsdata==>"+bsdata);
				
				bs.addData("back", "Y");
				BaseResult bs1 = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP4_LOCALE_DATA, null));
				Map<String, String> bs1data = (Map) bs1.getData();
				bs.addData("cityList", bs1data.get("cityList"));
				bs.addData("CTTADRList", bs1data.get("CTTADRList"));
				bs.addData("PMTADRList", bs1data.get("PMTADRList"));
				try {
					bs.addData("lionList", bs1data.get("lionList"));
				}catch (Exception e) {
					// TODO: handle exception
				}
				log.trace("sdfdsf>>>{}",bs.getData());
				bs.setResult(true);
			}
			else {
				String passAuth = (String) SessionUtil.getAttribute(model, SessionUtil.OLDCARDOWNERCHK, null);
				if("Y".equals(passAuth)) {
					//要注意拿出來的資料格式   新資料放在 requestParam 內
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP3_LOCALE_DATA, null));
					requestParam.putAll((Map)bs.getData());
				}
				bs = creditcard_apply_service.prepareInputData(requestParam);

				log.debug("UID==>" + okMap.get("CUSIDN"));

				log.debug("bsGatdata==>" + bs.getData());

				bsData = (Map<String, Object>) bs.getData();
				bsData1 = (Map<String, Object>) bsData.get("requestParam");
				log.debug("bsData1==>" + bsData1);
				log.debug("session oldcardowner==>" + (String) bsData1.get("oldcardowner"));
				SessionUtil.addAttribute(model, SessionUtil.STEP4_LOCALE_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.OLDCARDOWNERCHK, (String) bsData1.get("oldcardowner"));
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("apply_creditcard_p4 error >> {}",e);
		}finally {
			if(bs != null && bs.getResult()) {
				if(("Y").equals(SessionUtil.getAttribute(model, SessionUtil.OLDCARDOWNERCHK, null))) {
					model.addAttribute("result_data",bs);
					target = "/credit_apply/creditcard_apply_p4_old_owner";
				}else {
					model.addAttribute("result_data",bs);
					target = "/credit_apply/creditcard_apply_p4";
				}
			}else {
				bs.setPrevious("/CREDIT/APPLY/apply_creditcard"+null!=requestParam.get("BRANCH")?"?BRANCH="+requestParam.get("BRANCH"):"");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	/**
	 * 申請信用卡輸入頁2
	 */
	@RequestMapping(value = "/apply_creditcard_p4_2")
	public String apply_creditcard_p4_2(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> requestParam, Model model) {
		log.debug("apply_creditcard_p4_2");
		BaseResult bs = null;
		BaseResult bs1 = null;
		String target = "/online_apply/ErrorWithoutMenu";
		try {
			log.trace(ESAPIUtil.vaildLog("P4_2requestParam >> " + CodeUtil.toJson(requestParam)));
			bs = new BaseResult();
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			//取得客戶端的IP
			String IP = WebUtil.getIpAddr(request); // 登入 IP-Address
			log.debug(ESAPIUtil.vaildLog("IP >> " + IP));
			okMap.put("IP",IP);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP4_2_LOCALE_DATA, null));
			}
			else if(okMap.get("back").equals("Y")) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP4_3_LOCALE_DATA, null));
				bs.addData("back", "Y");
				log.trace("BSBACK>>>{}",bs.getData());
			}
			else if((SessionUtil.getAttribute(model, SessionUtil.OLDCARDOWNERCHK, null)).equals("Y")) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP4_LOCALE_DATA, null));
				bs.addAllData(okMap);
				bs.setResult(true);
				if(okMap.get("CHENAME").equals("2")) {
					bs.addData("CHENAMEChinese", "否");
				}else {
					bs.addData("CHENAMEChinese", "是");
				}
				log.trace("BSoldcardowner>>>{}",bs.getData());
				SessionUtil.addAttribute(model, SessionUtil.STEP4_2_LOCALE_DATA, bs);
				
				
				bs1 = creditcard_apply_service.prepareConfirmDataOldcard(okMap);
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs1);
				
				
				
				
			}
			else {
				// 取得所有分行
//				List<Map<String, String>> branchList = creditcard_apply_service.getAllBranchList();
//				bs.addData("branchList", branchList);
				bs.addAllData(requestParam);
				bs.setResult(true);
				log.trace("bs data log >>>{}",bs.getData());
				SessionUtil.addAttribute(model, SessionUtil.STEP4_2_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("apply_creditcard_p4_2 error >> {}",e);
		}finally {
			if(bs != null && bs.getResult()) {
				model.addAttribute("result_data",bs);
				if(("Y").equals(SessionUtil.getAttribute(model, SessionUtil.OLDCARDOWNERCHK, null))) {
					target = "/credit_apply/creditcard_apply_p5_old_owner";
				}else {
					target = "/credit_apply/creditcard_apply_p4_2";
				}
			}else {
				bs.setPrevious("/CREDIT/APPLY/apply_creditcard");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	/**
	 * 申請信用卡輸入頁3
	 */
	@RequestMapping(value = "/apply_creditcard_p4_3")
	public String apply_creditcard_p4_3(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> requestParam, Model model) {
		log.debug("apply_creditcard_p4_3");
		BaseResult bs = null;
		BaseResult bs4_2 = null;
		String target = "/online_apply/ErrorWithoutMenu";
		try {
			log.trace(ESAPIUtil.vaildLog("P4_3requestParam" + CodeUtil.toJson(requestParam)));
			bs = new BaseResult();
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP4_3_LOCALE_DATA, null));
			}
			else if(requestParam.get("back").equals("Y")) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
//				Map<String, String> bsData = (Map) bs1.getData();
//				String alldata = bsData.get("requestParam").toString();
//				Map map = (Map<String, Object>)getVal(alldata);
//				bs.addAllData(map);
				
				bs.addData("back", "Y");
				log.trace("BSBACK>>>{}",bs.getData());
			}
			else {
//				String alldata = requestParam.get("ALLdata");
//				Map map = (Map<String, Object>)getVal(alldata);
//				log.trace("MAP>>>>>>>>>>>{}",map.toString());
//				requestParam.remove("ALLdata");
				Date nowDate = new Date();
				String filetime = new SimpleDateFormat("yyyyMMddHHmmss").format(nowDate);
				requestParam.put("filetime", filetime);
				
				bs4_2 = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP4_2_LOCALE_DATA, null));
				
				bs.addAllData((Map)bs4_2.getData());
				bs.addAllData(requestParam);
				bs.setResult(true);
				log.trace("bs data log >>>{}",bs.getData());
				SessionUtil.addAttribute(model, SessionUtil.STEP4_3_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("apply_creditcard_p4_3 error >> {}",e);
		}finally {
			if(bs != null && bs.getResult()) {
				model.addAttribute("result_data",bs);
				target = "/credit_apply/creditcard_apply_p4_3";
			}else {
				bs.setPrevious("/CREDIT/APPLY/apply_creditcard");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	/**
	 * 申請信用卡確認頁
	 */
	@RequestMapping(value = "/apply_creditcard_p5")
	public String apply_creditcard_p5(HttpServletRequest request,HttpServletResponse response,@RequestParam Map<String,String> requestParam,Model model){
		log.debug("apply_creditcard_p5");
		BaseResult bs = null;
		BaseResult bs4_3 = null;
		String target = "/online_apply/ErrorWithoutMenu";
		try{
			log.trace(ESAPIUtil.vaildLog("P5requestParam >> " + CodeUtil.toJson(requestParam)));
			bs = new BaseResult();
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
			}else {
				//取得客戶端的IP
				String IP = WebUtil.getIpAddr(request); // 登入 IP-Address
				log.debug(ESAPIUtil.vaildLog("IP >> " + IP));
				requestParam.put("IP",IP);
				
				bs4_3 = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP4_3_LOCALE_DATA, null));
				
				requestParam.putAll((Map)bs4_3.getData());
				log.trace(ESAPIUtil.vaildLog("requestParam >> " + requestParam));
				bs = creditcard_apply_service.prepareConfirmData(requestParam);
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				
				BaseResult bs5 = null;
				bs5 = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				Map<String, String> bstest = (Map<String, String>)bs5.getData();
				log.debug("bstest==>"+bstest);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("apply_creditcard_p5 error >> {}",e);
		}finally {
			if(bs != null && bs.getResult()) {
				model.addAttribute("result_data",bs);
			 	target = "/credit_apply/creditcard_apply_p5";
			}else {
				bs.setPrevious("/CREDIT/APPLY/apply_creditcard");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * 申請信用卡結果頁
	 */
	@RequestMapping(value = "/apply_creditcard_p6")
	public String apply_creditcard_p6(HttpServletRequest request,HttpServletResponse response,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/online_apply/ErrorWithoutMenu";
		log.debug("apply_creditcard_p6");
		BaseResult bs = null;
		BaseResult bs5 = null;
		BaseResult bs6 = null;
		Map<String, String> okMap = null;
		try{
			bs = new BaseResult();
			log.trace(ESAPIUtil.vaildLog("P6requestParam>>" + CodeUtil.toJson(requestParam)));
			//okMap = requestParam;
			okMap = ESAPIUtil.validStrMap(requestParam);
			boolean hasLocale = okMap.containsKey("locale");
			
			if (hasLocale) {
				okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null);
				model.addAttribute("requestParam",okMap);
			}
			else {
//				String alldata = requestParam.get("alldata");
//				Map map = (Map<String, Object>)getVal(alldata);
//				requestParam.remove("alldata");
//				requestParam.putAll(map);
				
				bs5 = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				Map<String, String> bstest = (Map<String, String>)bs5.getData();
				log.debug("bstest==>"+bstest);
				requestParam.putAll((Map)bs5.getData());
				log.trace(ESAPIUtil.vaildLog("requestParam6>>>>>"+CodeUtil.toJson(requestParam)));
				
				Date nowDate = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				SimpleDateFormat sdf2 = new SimpleDateFormat("HHmmss");
				SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				requestParam.put("DATE",sdf.format(nowDate));
				requestParam.put("TIME",sdf2.format(nowDate));
				requestParam.put("LASTDATE",sdf.format(nowDate));
				requestParam.put("LASTTIME",sdf2.format(nowDate));
				requestParam.put("DATETIME",sdf3.format(nowDate));
				
				//String SEQNO = String.valueOf(txnCardApplyDao.countCreditRecord(nowDate) + 1);
				//Avoid Privacy Violation
				//String SEQNO = String.valueOf(txnCardApplyDao.countCdRecord(nowDate) + 1);
				//if(SEQNO.length() == 1){
				//	SEQNO = "10" + SEQNO;
				//}
				//else if(SEQNO.length() == 2){
				//	SEQNO = "1" + SEQNO;
				//}
				//20220330更新 , 修正取號程式
				String SEQNO = String.format("%03d", txnCardApplyDao.countCdRecord(nowDate) + 1);
				log.debug("SEQNO={}",SEQNO);
				
				String RCVNO = "CARD" + new SimpleDateFormat("yyyyMMdd").format(nowDate) + SEQNO;
				log.debug("RCVNO={}",RCVNO);
				requestParam.put("RCVNO",RCVNO);
				
				if((SessionUtil.getAttribute(model, SessionUtil.OLDCARDOWNERCHK, null)).equals("N")) {				
					if(!requestParam.get("FILE1").isEmpty()) {
	//					requestParam.put("FILE1name","1" + RCVNO + ".jpg");
						requestParam.put("FILE1name", requestParam.get("FILE1"));
						creditcard_apply_service.moveFile(requestParam.get("FILE1"));
					}
					if(!requestParam.get("FILE2").isEmpty()) {
	//					requestParam.put("FILE2name","2" + RCVNO + ".jpg");
						requestParam.put("FILE2name", requestParam.get("FILE2"));
						creditcard_apply_service.moveFile(requestParam.get("FILE2"));
					}
					if(!requestParam.get("FILE3").isEmpty()) {
	//					requestParam.put("FILE3name","3" + RCVNO + ".jpg");
						requestParam.put("FILE3name", requestParam.get("FILE3"));
						creditcard_apply_service.moveFile(requestParam.get("FILE3"));
					}
					if(!requestParam.get("FILE4").isEmpty()) {
	//					requestParam.put("FILE4name","4" + RCVNO + ".jpg");
						requestParam.put("FILE4name", requestParam.get("FILE4"));
						creditcard_apply_service.moveFile(requestParam.get("FILE4"));
					}
					if(!requestParam.get("FILE5").isEmpty()) {
	//					requestParam.put("FILE5name","5" + RCVNO + ".jpg");
						requestParam.put("FILE5name", requestParam.get("FILE5"));
						creditcard_apply_service.moveFile(requestParam.get("FILE5"));
					}
					
					requestParam.put("FILE6",RCVNO + ".pdf");
					try {
						Boolean result = false;
						String separ = File.separator;
						String uploadPath = context.getRealPath("") + separ +"com" + separ + "updateTmp" + separ;
						result = creditcard_apply_service.prepareCreditCardApplyP6Data(requestParam, uploadPath,model);
						if(result) {
							SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, requestParam);
							bs.addAllData(requestParam);
							bs.setResult(true);						
						}
						else {
							bs.setResult(false);
						}
					}
					catch(Exception e) {
						//Avoid Information Exposure Through an Error Message
						//e.printStackTrace();
						log.error("apply_creditcard_p6 error >> {}",e);
					}
				}
					
				
				
				if((SessionUtil.getAttribute(model, SessionUtil.OLDCARDOWNERCHK, null)).equals("Y")) {
					bs6 = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP4_LOCALE_DATA, null));
					Map<String, String> bs6map = (Map<String, String>)bs6.getData();
					log.debug("bs6map==>"+bs6map);
					requestParam.put("MBILLTO",bs6map.get("MBILLTO"));
					requestParam.put("strZIP",bs6map.get("strZIP"));
					requestParam.put("CPRIMADDR0",bs6map.get("CPRIMADDR0"));
					
					
					requestParam.put("FILE6",RCVNO + ".pdf");
					try {
						Boolean result = false;
						String separ = File.separator;
						String uploadPath = context.getRealPath("") + separ +"com" + separ + "updateTmp" + separ;
						result = creditcard_apply_service.prepareCreditCardApplyP6DataOldcard(requestParam, uploadPath,model);
						if(result) {
							SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, requestParam);
							bs.addAllData(requestParam);
							bs.setResult(true);						
						}
						else {
							bs.setResult(false);
						}
					}
					catch(Exception e) {
						//Avoid Information Exposure Through an Error Message
						//e.printStackTrace();
						log.error("apply_creditcard_p6 error >> {}",e);
					}
				}
				
				//清除除了 RESULT_LOCALE_DATA 之外的session
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null);
				SessionUtil.addAttribute(model, SessionUtil.STEP2_LOCALE_DATA, null);
				SessionUtil.addAttribute(model, SessionUtil.STEP3_LOCALE_DATA, null);
				SessionUtil.addAttribute(model, SessionUtil.STEP4_LOCALE_DATA, null);
				SessionUtil.addAttribute(model, SessionUtil.STEP4_2_LOCALE_DATA, null);
				SessionUtil.addAttribute(model, SessionUtil.STEP4_3_LOCALE_DATA, null);
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null);
				
				log.debug("SMS Service Start");
				//發送簡訊通知
				BaseResult bsSms = null;
				try {
					try 
					{
						String cusidn = null;
						String option = null;
						String type = null;
						String otp = null;
						String adopid = null;
						String phone = null;
						String cn = null;
						
						bsSms = new BaseResult();
						//取得session的身分證
						cusidn = requestParam.get("CUSIDN");
						log.debug("cusidn==>"+cusidn);
						adopid = requestParam.get("ADOPID");
						log.debug("adopid==>"+adopid);
						phone = requestParam.get("CPRIMCELLULANO1");
						log.debug("phone==>"+phone);
						cn = requestParam.get("CN");
						log.debug("cn==>"+cn);
						
						//發送簡訊通知
						bsSms = creditcard_apply_service.SmsService(cusidn, phone, option, type, otp, adopid, cn);
					}
					catch (Exception e) 
					{
						log.error("SmsOTPService error>>{}", e);
					}
					finally 
					{
						if (bsSms != null && bsSms.getResult()){
							//簡訊驗證通過
							log.debug("簡訊發送成功");
						} else {
							log.error("簡訊發送失敗");
						}
					}
					
				}catch(Exception e) {
					log.error("SmsService error >> {}", e.toString());
				}finally {

				}				
				log.debug("SMS Service End");
			}
		}
		catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("apply_creditcard_p6 error >> {}",e);
		}finally {
			if(bs != null && bs.getResult()) {
				model.addAttribute("result_data",bs);
				if((SessionUtil.getAttribute(model, SessionUtil.OLDCARDOWNERCHK, null)).equals("Y")) {
					target = "/credit_apply/creditcard_apply_p6_old_owner";
				}else {
					target = "/credit_apply/creditcard_apply_p6";
				}
			}else {
				bs.setPrevious("/CREDIT/APPLY/apply_creditcard");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/*
	 * 下載信用卡申請書PDF
	 */
	@RequestMapping("/downloadCreditCardPDF")
	public void downloadCreditCardPDF(@RequestParam Map<String, String> requestParam, HttpServletRequest request,
			HttpServletResponse response, Model model) {
		log.debug("IN downloadCreditCardPDF");
		try {
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			creditcard_apply_service.getCreditCardApplyPDFParameter(okMap);
			log.debug(ESAPIUtil.vaildLog("requestParam= "+ CodeUtil.toJson(okMap)));
			
			
			PDFUtil pdfUtil = new PDFUtil();

			InputStream inputStream = pdfUtil.HTMLtoPDF(pdfUtil.getPDFHTML(okMap));

			if (inputStream != null) {
				response.setContentType("application/pdf;charset=UTF-8");

				String pdfName = (String) okMap.get("pdfName");
				pdfName = pdfName.replace(" ", "");
				log.debug("pdfName={}", pdfName);

				String i18nFileName = i18n.getMsg(pdfName);
				log.debug("i18nFileName={}", i18nFileName);

				if (i18nFileName != null && !"".equals(i18nFileName)) {
					pdfName = i18nFileName.replaceAll(" ", "") + ".pdf";
				} else {
					pdfName = pdfName + ".pdf";
				}
				log.debug("pdfName={}", pdfName);

				response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(pdfName, "UTF-8"));

				IOUtils.copy(inputStream, response.getOutputStream());
			}
		} catch (Exception e) {
			log.error("", e);
		}
	}
	/**
	 * 自然人憑證驗證的AJAX
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/vaCheckAjax",produces={"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult vaCheckAjax(@RequestParam Map<String,String> requestParam){
		log.debug("IN vaCheckAjax");
		
		BaseResult baseResult = new BaseResult();
		baseResult.setResult(Boolean.FALSE);
		
		try{
			String AUTHCODE = requestParam.get("AUTHCODE");
			log.debug(ESAPIUtil.vaildLog("AUTHCODE=" + AUTHCODE));
			
			BaseResult vaBS = new BaseResult();
			vaBS = creditcard_apply_service.VACheck_REST(AUTHCODE);
			
			if(vaBS != null && vaBS.getResult()){
				baseResult.setResult(Boolean.TRUE);
				
				Map<String,Object> vaBSData = (Map<String,Object>)vaBS.getData();
				String RESPONSE = (String)vaBSData.get("RESPONSE");
				log.debug("RESPONSE={}",RESPONSE);
				
				baseResult.setData(RESPONSE);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("vaCheckAjax error >> {}",e);
		}
		return baseResult;
	}
	
	/**
	 * CK01 長期使用循環信用申請分期還款  條款頁
	 *  
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/instalment_repayment")
	public String instalment_repayment(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		
		String target = "/error";
		
		BaseResult bs = null;
		// 驗證 result token 如果存在 且相同表示F5 或i18n
		try {
			bs = new BaseResult();
			
			// 登入時的身分證字號
			String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if (!"".equals(cusidn) && cusidn != null) {
				// 解密成明碼
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				reqParam.put("CUSIDN", cusidn);

			} else {
				log.error("session no cusidn!!!");
			}
			log.trace(ESAPIUtil.vaildLog("reqParam = " +  CodeUtil.toJson(reqParam)));
			// 解決 Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}else {
				
				
				bs = creditcard_apply_service.instalment_repayment_p1(okMap);
				
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
			}
			
			log.trace("CK01 bs >>{}", bs.getData());

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("instalment_repayment error >> {}",e);
		} finally {
			if (bs.getResult()) {
				target = "/credit_apply/instalment_repayment";
				model.addAttribute("instalment_repayment", bs);
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		log.trace("target >> {}", target);
		return target;
	}
	
	
	/**
	 * CK01 長期使用循環信用申請分期還款  輸入頁
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/instalment_repayment_input")
	public String instalment_repayment_input(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		
		String target = "/error";
		
		BaseResult bs = null;
		// 驗證 result token 如果存在 且相同表示F5 或i18n
		try {
			bs = new BaseResult();
			
			log.trace(ESAPIUtil.vaildLog("reqParam = " +  CodeUtil.toJson(reqParam)));
			// 解決 Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}else {
				
				
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				 SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
			}
			
			log.trace("instalment_repayment_input bs >>{}", bs.getData());

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("instalment_repayment_input error >> {}",e);
		} finally {
			if (bs.getResult()) {
				target = "/credit_apply/instalment_repayment_input";
				model.addAttribute("instalment_repayment_input", bs);
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		log.trace("target >> {}", target);
		return target;
	}
	
	/**
	 * CK01 長期使用循環信用申請分期還款  輸入頁
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/instalment_repayment_result")
	public String instalment_repayment_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		
		String target = "/error";
		String next="/CREDIT/APPLY/instalment_repayment";
		BaseResult bs = null;
		// 驗證 result token 如果存在 且相同表示F5 或i18n
		try {
			bs = new BaseResult();
			
			log.trace(ESAPIUtil.vaildLog("reqParam= "+ CodeUtil.toJson(reqParam)));
			// 解決 Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}else {
				
				 bs = new BaseResult();
				
				String cusidn = new String(
						Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
						"utf-8");
				String userip = (String) SessionUtil.getAttribute(model, SessionUtil.USERIP, null);
				okMap.put("CUSIDN", cusidn);
				okMap.put("USERIP", userip);
				 bs = creditcard_apply_service.instalment_repayment_result(okMap);
				 
				 SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			
			log.trace("instalment_repayment_result bs >>{}", bs.getData());

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("instalment_repayment_result error >> {}",e);
		} finally {
			if (bs.getResult()) {
				target = "/credit_apply/instalment_repayment_result";
				model.addAttribute("instalment_repayment_result", bs);
			} else {
				bs.setNext(next);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		log.trace("target >> {}", target);
		return target;
	}
	
	// 取得帳號資料Ajax
	@RequestMapping(value = "/get_bank_ajax", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody List<Map<String, String>> goldacn_ajax(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("get_bank_ajax...");
		SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, "");
		List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
		try {
			resultList = creditcard_apply_service.getAllBranchList();
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("goldacn_ajax error >> {}",e);
		}
		return resultList;
	}
	public static Object getVal(String param) {
        Map map = new HashMap();
        String str = "";
        String key = "";
        Object value = "";
        char[] charList = param.toCharArray();
        boolean valueBegin = false;
        for (int i = 0; i < charList.length; i++) {
            char c = charList[i];
            if (c == '{') {
                if (valueBegin == true) {
                    value = getVal(param.substring(i, param.length()));
                    i = param.indexOf('}', i) + 1;
                    map.put(key, value);
                }
            } else if (c == '=') {
                valueBegin = true;
                key = str;
                str = "";
            } else if (c == ',') {
                valueBegin = false;
                value = str;
                str = "";
                map.put(key, value);
            } else if (c == '}') {
                if (str != "") {
                    value = str;
                }
                map.put(key, value);
                return map;
            }
            else if (c != ' ') {
                str += c;
            }
        }
        return map;
    }
	
	//國籍Ajax
	@RequestMapping(value = "/country_ajax", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody List<Map<String, String>> country_ajax(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("do country_ajax...");
		List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
		
		try {
			List<ADMCOUNTRY> ADMCOUNTRY = admcountrydao.getAllCountry();
			if (ADMCOUNTRY != null) {
				for (ADMCOUNTRY each : ADMCOUNTRY) {
					// 將po轉成map
					Map<String, Object> eachMap = ObjectConvertUtil.object2Map(each);
					Map<String, String> result = new HashMap();
					String country = (String)eachMap.get("ADCTRYNAME");
					String countryen = (String)eachMap.get("ADCTRYENGNAME");
					String countrycn = (String)eachMap.get("ADCTRYCHSNAME");
					String countryabb = (String)eachMap.get("ADCTRYCODE");
					if(!countryabb.equals(null) && !countryabb.equals("")) {
						countryabb = countryabb.replace(" ", "");
					}
					if(countryabb.equals("TW")) {
						country = "中華民國";
						countryen = "Republic of China";
						countrycn = "中华民国";
					}
					result.put("COUNTRY", country);
					result.put("COUNTRYEN", countryen);
					result.put("COUNTRYCN", countrycn);
					result.put("COUNTRYABB", countryabb);
					resultList.add(result);
				}
			}
			log.trace(ESAPIUtil.vaildLog("resultList>>>"+resultList));
		} 
		catch (Exception e) {
			log.error("get_country_name.error: {}", e);
		}
		
		return resultList;
	}
	
	/**
	 * 取得獅友會分會
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getLionBranchAjax", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody List<Map<String, String>> getLionBranchAjax(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		//log.trace("getLionBranchAjax..." + reqParam);
		List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
		// 要取得使用者相關帳號
		BaseResult bs = new BaseResult();
		try {
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			String zone = okMap.get("ZONE");
			resultList = creditcard_apply_service.getLionBranch(zone);

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getLionBranch error >> {}",e);
		}
		return resultList;

	}
	
	/**
	 * 清除暫存session的資料，不可清除使用者身份資料
	 * 
	 * @param model
	 */
	public void cleanSession(Model model)
	{
		// 清除切換語系
		SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null);
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null);
		// 清除回上一頁
		SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, null);
		// 清除print
		SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, null);
		// 換頁暫存資料
		SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, null);
	}
	
	/************************************信用卡進度查詢+線上補件*******************************************************/
	/**
	 * 清除線上申請所有暫存session的資料
	 * 
	 * @param model
	 */
	public void cleanSessionOnlineApply(Model model)
	{
		SessionUtil.addAttribute(model, SessionUtil.CUSIDN_ONLINE_APPLY, null);
		SessionUtil.addAttribute(model, SessionUtil.YNFLG, null);
		SessionUtil.addAttribute(model, SessionUtil.RCVNO, null);
		SessionUtil.addAttribute(model, SessionUtil.PFLG, null);
		SessionUtil.addAttribute(model, SessionUtil.MOBILE, null);
	}
	
	
	
	/**
	 * 信用卡進度查詢+線上補件  -> 首頁
	 * 作者:曾勁順
	 * 日期:110.04.09
	 * @param model
	 */
	@RequestMapping(value = "/onlineapply_creditcard_progress")
	public String onlineapply_creditcard_progress(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model, HttpSession session) {
		String target = "/creditcard/onlineapply_creditcard_progress";
		return target;
	}
	
	
	/**
	 * 信用卡進度查詢+線上補件 -> OTP
	 * 作者:曾勁順
	 * 日期:110.04.09
	 * @param model
	 */	
	//@IPVERIFY(cusidnKey="CUSIDN")
	@RequestMapping(value = "/onlineapply_creditcard_progress_p1")
	public String onlineapply_creditcard_progress_p1(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model, HttpSession session) {
		
		log.trace(ESAPIUtil.vaildLog("onlineapply_creditcard_progress_p1.reqParam: " + CodeUtil.toJson(reqParam)));
		String target = "/online_apply/ErrorWithoutMenu";
		
		BaseResult bs = null;
		String cusidn = "";
		try {
			cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN_ONLINE_APPLY, null)), "utf-8");
		}catch(Exception e) {
			
		}
		
		// 解決Trust Boundary Violation
		Map<String, String> okMaptmp = ESAPIUtil.validStrMap(reqParam);
		log.debug("okMaptmp==>"+okMaptmp);
		if(!okMaptmp.get("CUSIDN").equals(cusidn)) {
			cleanSessionOnlineApply(model);
		}

		try {
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			String jsondc = URLEncoder.encode(CodeUtil.toJson(okMap), "UTF-8");
			okMap.put("jsondc", jsondc);
			
			//先打第一道詢問是否需要補件，要，取得登錄的行動電話
			bs = new BaseResult();			
			bs = creditcard_apply_service.upload_creditcard_identity_p1(okMap);
			
		}catch(Exception e) {
			log.error("upload_creditcard_identity_p1 error >> {}", e.toString());
		}finally {
			
			if(bs != null && bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.ONLINE_APPLY_DATA, bs);
				log.debug("bs.getData()==>"+bs.getData());
			
				Map<String,String> bsData =(Map<String, String>) bs.getData();
				String YNFLG=(String)bsData.get("YNFLG");
				log.debug("YNFLG==>"+YNFLG);
				String PFLG=(String)bsData.get("PFLG");
				log.debug("PFLG==>"+PFLG);
				
				LinkedTreeMap bsdata = new LinkedTreeMap();
				Gson gson = new Gson();
				String data = bsData.get("data").toString();
				Map map = gson.fromJson(data, Map.class);
				log.debug("data==>"+map.get("data"));
				List list = (List)map.get("data");
				for(int i = 0; i<list.size(); i++) {
					bsdata = (LinkedTreeMap)list.get(i);
				}
//				Map<String,String> bsdata = CodeUtil.objectCovert(Map.class, bsData.get("data"));
				
				log.debug("MOBILE==>"+(String)bsdata.get("MOBILE"));
				
				
				if(!"".equals(bsData.get("YNFLG")) && bsData.get("YNFLG")!=null) {
					switch(YNFLG){
						case "E":
							//查無資料
							bs.setPrevious("/CREDIT/APPLY/onlineapply_creditcard_progress");//設定返回按鈕動作，進入Creditcard_Apply_Controller裡面的/onlineapply_creditcard_progress區塊
							model.addAttribute(BaseResult.ERROR, bs);
							break;
						case "N":
						case "Y":
							switch(PFLG){
								case "5":
									bs.setPrevious("/CREDIT/APPLY/onlineapply_creditcard_progress");//設定返回按鈕動作，進入Creditcard_Apply_Controller裡面的/onlineapply_creditcard_progress區塊
									model.addAttribute(BaseResult.ERROR, bs);
									break;
//								case "4":
//									bs.setPrevious("/CREDIT/APPLY/onlineapply_creditcard_progress");//設定返回按鈕動作，進入Creditcard_Apply_Controller裡面的/onlineapply_creditcard_progress區塊
//									model.addAttribute(BaseResult.ERROR, bs);
//									break;	
								default:
									SessionUtil.addAttribute(model, SessionUtil.MOBILE,bsdata.get("MOBILE"));						//有資料，但無論是否需要補件，者先取得行動電話進行otp驗證
									SessionUtil.addAttribute(model, SessionUtil.CUSIDN_ONLINE_APPLY,Base64.getEncoder().encodeToString(reqParam.get("CUSIDN").toUpperCase().getBytes()) );	// 使用者統編加密後存取
									SessionUtil.addAttribute(model, SessionUtil.RCVNO,bsData.get("RCVNO"));
									target = "/creditcard/onlineapply_creditcard_progress_p1";
									break;
							}
						default:
							break;
					}
				}else {
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}else {
				bs.setNext("/ONLINE/APPLY/online_apply_menu");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
			
		log.info("onlineapply_creditcard_progress_p1 end");
		return target;
	}		
	
	
	
	/**
	 * 信用卡進度查詢+線上補件 -> 進度查詢結果資料
	 * 作者:曾勁順
	 * 日期:110.04.09
	 * @param model
	 */	
	@RequestMapping(value = "/onlineapply_creditcard_progress_p2")
	public String onlineapply_creditcard_progress_p2(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model, HttpSession session) {
		
		String target = "/online_apply/ErrorWithoutMenu";
		log.trace(ESAPIUtil.vaildLog("onlineapply_creditcard_progress_p2.reqParam: " + CodeUtil.toJson(reqParam)));
		
		BaseResult bs = null;
		try {
			
			try 
			{
				String cusidn = null;
				String option = null;
				String type = null;
				String otp = null;
				String adopid = null;
				
				bs = new BaseResult();
				//取得session的身分證
				cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN_ONLINE_APPLY, null)), "utf-8");
				option = reqParam.get("option");
				type = reqParam.get("type");
				otp = reqParam.get("OTP");
				adopid = reqParam.get("ADOPID");
				
				log.debug("cusidn==>"+cusidn);
				log.debug("option==>"+option);
				log.debug("type==>"+type);
				log.debug("otp==>"+otp);
				log.debug("adopid==>"+adopid);
				
				
				
				//驗證簡訊OTP
				bs = creditcard_apply_service.SmsOTPService(cusidn, option, type, otp, adopid);
			}
			catch (Exception e) 
			{
				log.error("SmsOTPService error>>{}", e);
			}
			finally 
			{
			
				if (bs != null && bs.getResult()){
					//簡訊驗證通過
					log.debug("簡訊驗證碼檢核通過");
					bs = new BaseResult();
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.ONLINE_APPLY_DATA, null));
					
					Map<String , String> callRow = (Map) bs.getData();
					Map<String, String> okMap = new HashMap<>();
					okMap.putAll((Map<String, String>)callRow);
					log.debug("okMapData==>"+okMap.get("data"));
					
					String json = okMap.get("data").toString();
					Gson gson = new Gson();
					Map map = gson.fromJson(json, Map.class);
					log.debug("data==>"+map.get("data"));
					List list = (List)map.get("data");
					String YNFLG = "N";
					for(int i = 0; i<list.size(); i++) {
						LinkedTreeMap jsonMap = (LinkedTreeMap)list.get(i);
						log.debug("jsonMap==>"+jsonMap);
						//SessionUtil.addAttribute(model, SessionUtil.PFLG, (String)jsonMap.get("PFLG"));
						SessionUtil.addAttribute(model, SessionUtil.RCVNO, (String)jsonMap.get("RCVNO"));
						log.debug("YNFLG=>"+jsonMap.get("YNFLG"));
						if("Y".equals((String)jsonMap.get("YNFLG"))) {
							YNFLG = "Y";
						}
						
					}
					
					model.addAttribute("YNFLG", YNFLG);
					model.addAttribute("listProducts", list);
					model.addAttribute("TOTALCOUNT", list.size());
					model.addAttribute("time_now", DateUtil.getDate("/") + " " + DateUtil.getTheTime(":"));					
					target = "/creditcard/onlineapply_creditcard_progress_p2";
				} else {
					log.debug("簡訊驗證碼檢核失敗");
					bs.setPrevious("/CREDIT/APPLY/onlineapply_creditcard_progress");//設定返回按鈕動作
					model.addAttribute(BaseResult.ERROR, bs);
				}
				
				
			}	
		}catch(Exception e) {
			log.error("onlineapply_creditcard_progress_p2 error >> {}", e.toString());
		}finally {

		}				
			
		log.info("onlineapply_creditcard_progress_p2 end");
		return target;
	}	
	
	
	
	
	
	
	/**
	 * 信用卡進度查詢+線上補件 -> 線上補件上傳證件資料
	 * 作者:曾勁順
	 * 日期:110.04.09
	 * @param model
	 */	
	//信用卡補上傳資料(身份證件/財力證明)
	@RequestMapping(value = "/upload_creditcard_identity_p2")
	public String upload_creditcard_identity_p2(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model, HttpSession session) {
		
		log.info("upload_creditcard_identity_p2 start");
		String target = "/online_apply/ErrorWithoutMenu";
		String path = request.getSession().getServletContext().getRealPath("");
		log.debug(ESAPIUtil.vaildLog("request Path(s) upload_creditcard_identity_p2 >> " + path));
		log.trace(ESAPIUtil.vaildLog("upload_creditcard_identity_p2.reqParam: " + CodeUtil.toJson(reqParam)));
		
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			//判斷是否 從確認頁 返回到 上傳頁，
			String back = reqParam.get("back") == null ? "" : reqParam.get("back");
			log.debug(ESAPIUtil.vaildLog("back >> " + back));
			
			if("Y".equals(back)) {
				log.debug("從確認頁回來的");
				//從確認頁回到上一步到  上傳頁時，將確認頁存的session塞回bs帶到上傳頁
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.UPLOAD_CREDITCARD_IDENTITY_P2_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
				
				target = "/creditcard/upload_creditcard_identity_p2";
				model.addAttribute("result_data", bs);
			}else {
				log.debug("從信用卡查詢結果頁面過來的");
				log.debug("FLG==>"+reqParam.get("FLG"));
				SessionUtil.addAttribute(model, SessionUtil.PFLG, reqParam.get("FLG"));
				SessionUtil.addAttribute(model, SessionUtil.RCVNO, reqParam.get("RCVNO"));
				target = "/creditcard/upload_creditcard_identity_p2";
				model.addAttribute("result_data", bs);
			}			
			
			
		}catch(Exception e) {
			log.error("upload_creditcard_identity_p2 error >> {}", e.toString());
		}finally {

		}	
		
		log.info("upload_creditcard_identity_p2 end");
		return target;
	}		
	
	
	/**
	 * 信用卡進度查詢+線上補件 -> 線上補件確認頁
	 * 作者:曾勁順
	 * 日期:110.04.09
	 * @param model
	 */	
	//信用卡補上傳資料(身份證件/財力證明)
	@RequestMapping(value = "/upload_creditcard_identity_p3")
	public String upload_creditcard_identity_p3(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model, HttpSession session) {
		
		String target = "/online_apply/ErrorWithoutMenu";
		String path = request.getSession().getServletContext().getRealPath("");
		log.debug(ESAPIUtil.vaildLog("upload_creditcard_identity_p3 request Path >> " + path));
		log.trace(ESAPIUtil.vaildLog("upload_creditcard_identity_p3 reqParam: {}" + CodeUtil.toJson(reqParam)));
		
		BaseResult bs = null;
		bs = new BaseResult();

		if (!"".equals(SessionUtil.getAttribute(model, SessionUtil.CUSIDN_ONLINE_APPLY, null)) && SessionUtil.getAttribute(model, SessionUtil.CUSIDN_ONLINE_APPLY, null) != null) { 
		
			try {
				bs.addAllData(reqParam);
				bs.setResult(true);
				log.trace("bs data log >>>{}",bs.getData());
				SessionUtil.addAttribute(model, SessionUtil.UPLOAD_CREDITCARD_IDENTITY_P2_DATA, bs);
			}catch(Exception e) {
				log.error("upload_creditcard_identity_p3 error >> {}", e.toString());
			}finally {
				if(bs != null && bs.getResult()) {
					target = "/creditcard/upload_creditcard_identity_p3";
					model.addAttribute("result_data", bs);
				}else {
					bs.setNext("/CREDIT/APPLY/upload_creditcard_identity_p3");
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}

		}else {
			bs.setPrevious("/CREDIT/APPLY/upload_creditcard_identity");
			bs.setErrorMessage("99009", "操作逾時，請重新登入");
			model.addAttribute(BaseResult.ERROR, bs);
		}
		
		return target;
	}	
	
	

	/**
	 * 信用卡進度查詢+線上補件 -> 線上補件結果頁
	 * 作者:曾勁順
	 * 日期:110.04.09
	 * @param model
	 */	
	//信用卡補上傳資料(身份證件/財力證明) 確認頁 -> 結果頁
	@RequestMapping(value = "/upload_creditcard_identity_result")
	public String upload_creditcard_identity_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model, HttpSession session) {
		
		String target = "/online_apply/ErrorWithoutMenu";
		String path = request.getSession().getServletContext().getRealPath("");
		String separ = File.separator;
		log.debug(ESAPIUtil.vaildLog("request Path(s) upload_creditcard_identity_result >> {}" + path));
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		BaseResult bs = null;
		try {
			
			bs = new BaseResult();
			
			if (!"".equals(SessionUtil.getAttribute(model, SessionUtil.CUSIDN_ONLINE_APPLY, null)) && SessionUtil.getAttribute(model, SessionUtil.CUSIDN_ONLINE_APPLY, null) != null) { 
				
				Date nowDate = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				SimpleDateFormat sdf2 = new SimpleDateFormat("HHmmss");
				SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				okMap.put("DATE",sdf.format(nowDate));
				okMap.put("TIME",sdf2.format(nowDate));
				okMap.put("LASTDATE",sdf.format(nowDate));
				okMap.put("LASTTIME",sdf2.format(nowDate));
				okMap.put("DATETIME",sdf3.format(nowDate));
				
				okMap.put("path",path+"\\com\\updateTmp\\");
				okMap.put("imgMovePath", SpringBeanFactory.getBean("imgMovePath"));
				okMap.put("ucidSN", sdf.format(nowDate)+sdf2.format(nowDate));
				
				
				String pflg = (String) SessionUtil.getAttribute(model, SessionUtil.PFLG, null);
				log.debug("pflg==>"+pflg);
				
				
				int firstNum = 0;
				int lastNum = 0;
				switch(pflg) {
					case "1":// 1缺身分證
						firstNum = 1;
						lastNum = 2;
						break;
					case "2":// 2缺財力證明
						firstNum = 3;
						lastNum = 5;
						break;
					case "3":// 3兩者都缺
						firstNum = 1;
						lastNum = 5;
						break;
					default:
						break;
				}
							
				/*將上傳的檔案搬到container有mount出的位置留存*/
				for(int i = firstNum ;i <= lastNum ; i++) {
					if(!okMap.get("FILE"+i).isEmpty()) {
						log.debug("FILE"+i+"==>"+okMap.get("FILE"+i));
						okMap.put("FILE"+i+"name", okMap.get("FILE"+i));
						creditcard_apply_service.moveFile(okMap.get("FILE"+i));
					}
				}
				

				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN_ONLINE_APPLY, null)), "utf-8");
				String rcvno = (String) SessionUtil.getAttribute(model, SessionUtil.RCVNO, null);
				
				try {
					Boolean result = false;
					String uploadPath = context.getRealPath("") + separ +"com" + separ + "updateTmp" + separ;
					//將上傳檔案壓成zip並傳給ELOAN
					result = creditcard_apply_service.FileZipOutputStream(okMap, pflg, cusidn, rcvno,uploadPath,eln_uri);	
					if(result) {
						log.debug("bs result==>true");
						SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, okMap);
						bs.addAllData(okMap);
						bs.setResult(true);		
						target = "/creditcard/upload_creditcard_identity_result";
						model.addAttribute("result_data", bs);
					}
					else {
						log.debug("bs result==>false");
						bs.setResult(false);
						bs.setErrorMessage("99300", "系統連線異常");
						model.addAttribute(BaseResult.ERROR, bs);
					}
				}
				catch(Exception e) {
					//Avoid Information Exposure Through an Error Message
					log.error("apply_creditcard_p6 error >> {}",e);
				}
				
			}else {
				bs.setPrevious("/CREDIT/APPLY/upload_creditcard_identity");
				bs.setErrorMessage("99009", "操作逾時，請重新登入");
				model.addAttribute(BaseResult.ERROR, bs);
			}
				
		}catch(Exception e) {
			log.error("upload_creditcard_identity_result error >> {}", e.toString());
		}
		
		return target;
	}
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	/************************************線上補件上傳處理區*******************************************************/

	/**
	 * 信用卡補上傳資料(身份證件/財力證明)-上傳圖檔
	 * 作者:曾勁順
	 * 日期:110.04.12
	 * @param model
	 */		
	@PostMapping("/upload_creditcard_identity_p2_get_zip_fileupload")
    public @ResponseBody BaseResult upload_creditcard_identity_p2_get_zip_fileupload(HttpServletRequest request, HttpServletResponse response, 
    		@RequestParam("picFile") MultipartFile picFile, @RequestParam Map<String, String> reqParam, Model model) {
		
    	BaseResult bs = new BaseResult();
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		log.debug(ESAPIUtil.vaildLog("upload_creditcard_identity_p2_get_zip_fileupload.reqParam: {}" + CodeUtil.toJson(reqParam)));
		
		String cusidn = "";
		String rcvno = "";
		try {
			cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN_ONLINE_APPLY, null)), "utf-8");
			rcvno = (String) SessionUtil.getAttribute(model, SessionUtil.RCVNO, null);
			rcvno = "TestTBB";
		} catch (Exception e) {
			log.error("", e);
		}
		
		String separ = File.separator;
		String uploadPath = context.getRealPath("") + separ +"com" + separ + "updateTmp" + separ;
    	log.debug("update_Path >> {}",uploadPath);
    	//圖示允許的類型
		String[] allowImageType = {"image/jpeg","image/bmp","image/png","image/x-png","image/jpg","image/pjpeg"};
		//圖示允許的長寬
		int allowImageWidth = 640;
		int allowImageHeight = 480;
		//圖示允許的檔案大小
		int allowImageSize = 3300 * 1024;//3300KB
//		String contentType;
//		String originalFilename;
		try {
            log.debug(ESAPIUtil.vaildLog(okMap.get("oldPath")));
			if(!okMap.get("oldPath").equals("")) {
				online_apply_service.removefile( uploadPath , okMap.get("oldPath"), "", "");
			}
		}catch(Exception e) {
			log.error(e.toString());
		}
        try {
            MultipartFile multipartFile = picFile;
            log.debug(ESAPIUtil.vaildLog(okMap.get("type")));

            Date nowDate = new Date();
			String filetime1 = new SimpleDateFormat("yyyyMMddHHmmss").format(nowDate);
            
			String fileTyle = multipartFile.getOriginalFilename().substring(multipartFile.getOriginalFilename().lastIndexOf("."),multipartFile.getOriginalFilename().length()).toLowerCase(); 
			String fileName = cusidn + "_" + filetime1 +"_"+ okMap.get("type") + fileTyle;
            try {
            	String filetime = okMap.get("filetime");
            	if(StrUtil.isNotEmpty(filetime)) {
            		fileName = okMap.get("type") + okMap.get("filetime") + fileTyle;
            	}
            }
            catch (Exception e) {
				// TODO: handle exception
			}
            log.debug(ESAPIUtil.vaildLog("fileName={}"+ fileName));

            bs.addAllData(online_apply_service.checkImage(	multipartFile,
															multipartFile.getOriginalFilename(),
															multipartFile.getContentType(),
															allowImageType,
															allowImageWidth,
															allowImageHeight,
															allowImageSize,
															uploadPath, fileName));

            if( ((Map<String,Object>)bs.getData()).get("result").equals("TRUE") ) {
                bs.addData("validated", Boolean.TRUE);
            }else {
                bs.addData("validated", Boolean.FALSE);
            }
            bs.addData("url", fileName);
        } catch (Exception e) {
            log.error("saveFile Error", e);
            Map<String, String> errors = new HashMap<String, String>();
            bs.addData("summary", i18n.getMsg("LB.X2131"));
            bs.addData("validated", Boolean.FALSE);

        }
        return bs;
    }		

	
	/**
	 * 信用卡補上傳資料(身份證件/財力證明)-上傳圖檔-純jpg類
	 * 作者:曾勁順
	 * 日期:110.04.12
	 * @param model
	 */	
	@PostMapping("/upload_creditcard_identity_p2_get_zip_fileupload_only_jpg")
    public @ResponseBody BaseResult upload_creditcard_identity_p2_get_zip_fileupload_only_jpg(HttpServletRequest request, HttpServletResponse response, 
    		@RequestParam("picFile") MultipartFile picFile, @RequestParam Map<String, String> reqParam, Model model) {
		
    	log.info("upload_creditcard_identity_p2_get_zip_fileupload_only_jpg start");
		BaseResult bs = new BaseResult();
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
		String cusidn = "";
		String rcvno = "";
		try {
			cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN_ONLINE_APPLY, null)), "utf-8");
			rcvno = (String) SessionUtil.getAttribute(model, SessionUtil.RCVNO, null);
		} catch (Exception e) {
			log.error("", e);
		}
		
		String separ = File.separator;
		String uploadPath = context.getRealPath("") + separ +"com" + separ + "updateTmp" + separ;
    	log.debug("update_Path >> {}",uploadPath);
    	//圖示允許的類型
		String[] allowImageType = {"image/jpeg","image/jpg","image/pjpeg"};
		//圖示允許的長寬
		int allowImageWidth = 640;
		int allowImageHeight = 480;
		//圖示允許的檔案大小
		int allowImageSize = 3300 * 1024;//3300KB
		String contentType;
		String originalFilename;
		try {
            log.debug(ESAPIUtil.vaildLog(okMap.get("oldPath")));
			if(!okMap.get("oldPath").equals("")) {
				online_apply_service.removefile( uploadPath , okMap.get("oldPath"), "", "");
			}
		}catch(Exception e) {
			log.error(e.toString());
		}
        try {
            MultipartFile multipartFile = picFile;
            log.debug(ESAPIUtil.vaildLog(okMap.get("type")));

            Date nowDate = new Date();
			String filetime1 = new SimpleDateFormat("yyyyMMddHHmmss").format(nowDate);
            
			String fileTyle = multipartFile.getOriginalFilename().substring(multipartFile.getOriginalFilename().lastIndexOf("."),multipartFile.getOriginalFilename().length()).toLowerCase(); 
            String fileName = rcvno + "_" + cusidn + "_" + filetime1 + "_" + okMap.get("type") + fileTyle;
            try {
            	String filetime = okMap.get("filetime");
            	if(StrUtil.isNotEmpty(filetime)) {
            		fileName = okMap.get("type") + okMap.get("filetime") + fileTyle;
            	}
            }
            catch (Exception e) {
				// TODO: handle exception
			}
            log.info(ESAPIUtil.vaildLog("fileName={}"+ fileName));

            bs.addAllData(online_apply_service.checkImageOnlyImg(	multipartFile,
															multipartFile.getOriginalFilename(),
															multipartFile.getContentType(),
															allowImageType,
															allowImageWidth,
															allowImageHeight,
															allowImageSize,
															uploadPath, fileName));

            if( ((Map<String,Object>)bs.getData()).get("result").equals("TRUE") ) {
                bs.addData("validated", Boolean.TRUE);
            }else {
                bs.addData("validated", Boolean.FALSE);
            }
            bs.addData("url", fileName);
        } catch (Exception e) {
            log.error("saveFile Error", e);
            Map<String, String> errors = new HashMap<String, String>();
            bs.addData("summary", i18n.getMsg("LB.X2131"));
            bs.addData("validated", Boolean.FALSE);

        }
        log.info("upload_creditcard_identity_p2_get_zip_fileupload_only_jpg end");
        return bs;
    }	
	
	
	
	/**
	 * 信用卡補上傳資料(身份證件/財力證明)-檔案刪除
	 * 作者:曾勁順
	 * 日期:110.04.12
	 * @param model
	 */	
	@PostMapping("/upload_creditcard_identity_p2_fileupload_delete")
	public @ResponseBody BaseResult upload_creditcard_identity_p2_fileupload_delete(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam Map<String, String> reqParam, Model model) {
		
		log.info("upload_creditcard_identity_p2_fileupload_delete start");
		BaseResult bs = new BaseResult();
		String separ = File.separator;
		String uploadPath = context.getRealPath("") + separ + "com" + separ + "updateTmp" + separ;
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
            log.debug(ESAPIUtil.vaildLog(okMap.get("Path")));
			if(!okMap.get("Path").equals("")) {
				online_apply_service.removefile( uploadPath , okMap.get("Path"), "", "");
			}
			bs.addData("sessulte", true);
		}catch(Exception e) {
			log.error(e.toString());
		}
		log.info("upload_creditcard_identity_p2_fileupload_delete end");
        return bs;
	}	
}