package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class C015_REST_RQ extends BaseRestBean_FUND implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8336944954913920688L;
	private String CUSIDN;			// 身份證號

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	
	
}