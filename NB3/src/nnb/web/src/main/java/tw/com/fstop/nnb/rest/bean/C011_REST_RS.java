package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * C011_REST_RS
 */
public class C011_REST_RS extends BaseRestBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3281529933482031600L;
	
	private String DATE;
	private String TIME;
	private String CUSIDN;
	private String FLAG;
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getFLAG() {
		return FLAG;
	}
	public void setFLAG(String fLAG) {
		FLAG = fLAG;
	}
}