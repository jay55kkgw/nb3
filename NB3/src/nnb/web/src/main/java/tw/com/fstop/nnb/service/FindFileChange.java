package tw.com.fstop.nnb.service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.SpringBeanFactory;

@Service
public class FindFileChange extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());
	public DateTime initdate;
	@Autowired
	DaoService daoservice ;
	
//	@Qualifier
	@Autowired
	String scanPath;
	
	private String skipList[] = {"nnb-web.war/com/login/","nnb-web.war/com/updateTmp/","nnb-web.war/com/updateBytes/"};
	
	/**
	 * 初始化基本日期時間，來當作基準
	 */
	@PostConstruct
	public void init() {
		initdate = new DateTime();
		log.info("initdate>>{}", initdate.toString("yyyy-MM-dd HH:mm:ss"));
	}

	public BaseResult findChange(String contextPath) {
		log.info("scanPath>>{}",scanPath);
		contextPath = contextPath.replace("/", "").trim().toUpperCase();

		// was 路徑
		// /opt/IBM/WebSphere/AppServer/profiles/AppSrv01/installedApps/DefaultCell01/nnb-ear.ear
		// liberty 路徑
		// /opt/ibm/wlp/usr/servers/defaultServer/apps/expanded/
//		String scanPath =  spring "D:\\PROJ\\ox-wk-dev\\ms_batch_main\\src\\";
		List<String> list = new LinkedList<String>();

		BaseResult bs = null;
		try {
			bs = new BaseResult();
			listDir(new File(scanPath), list);
			log.info("異動list>>{}",list);
			bs.setResult(Boolean.TRUE);
			bs.setMsgCode("0");
			bs.setMessage("異動檔案共" + list.size() + "筆");
			sendMail(contextPath, list);
		} catch (Exception e) {
			log.error("{}", e.toString());
		}

		// map.put("result", Boolean.TRUE);
		return bs;
	}

	public void sendMail(String contextPath, List<String> list) {
		
		String mail_subject_env = SpringBeanFactory.getBean("MAIL_SUBJECT_ENV");
		String subject = "";
		
		switch (mail_subject_env) {
		case "D":
			subject = contextPath + "檔案異動 [新世代網路銀行 開發環境]";
			break;
		case "T":
			subject = contextPath + "檔案異動 [新世代網路銀行 測試環境]";
			break;
		case "P":
			subject = contextPath + "檔案異動 [新世代網路銀行]";
			break;
		}
		
		HashMap<String, Object> map;
		String content = "";
		String mails = "";
		
		List<String> mailList = new ArrayList<String>();
		try {
			if (list != null && !list.isEmpty()) {
				mails = daoservice.getAPmail();
				log.info("mails>>{}",mails);
				mailList.add(mails);
				content = getMailContent(list, contextPath);
//				map = new HashMap<String, Object>();
//				HashMap<String, Object> reqParam = new HashMap<String, Object>();
//				reqParam.put("Subject", contextPath + "檔案異動");
////				reqParam.put("Content", "檔案異動清單:" + CodeUtil.toJson(list));
//				reqParam.put("Content", getMailContent(mailList, contextPath));
//				reqParam.put("ContentType", "1");
//				reqParam.put("Receivers", mailList);
//				log.info("reqParam>>{}",reqParam);
				SendAdMail_REST(subject, content, mailList,"1");
			}
		} catch (Exception e) {
			log.error("{}", e.toString());
		}
	}
	
	
	public String getMailContent(List<String> list,String contextPath) {
		StringBuffer sb = new StringBuffer();
		sb.append("<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF8\" />\n");
		sb.append("<title>新個網檔案異動通知</title>\n");
		sb.append("</head>\n");
		sb.append("<body>\n");
		sb.append("主機名稱:"+ contextPath+"<BR>");
		sb.append("檔案異動清單:"+"<BR>");
		for(String eachdata:list) {
			sb.append(eachdata + "<BR>");
		}
		sb.append("</body>\n");
		sb.append("</html>\n");
		log.info("sb>>{}",sb.toString());
		
		return sb.toString();
	}

	public Boolean isRange(DateTime filedate, Integer range) {
		Boolean result = Boolean.FALSE;
		// DateTime date = new DateTime();
		// date = date.minusHours(range);
		log.debug("base date time>>{}", initdate.toString("yyyy-MM-dd HH:mm:ss"));
		DateTime date = initdate.minusMinutes(range);
		log.info("filedate>>{}", filedate.toString("yyyy-MM-dd HH:mm:ss"));
		result = filedate.isAfter(date);
		log.info("isRange>>{}", result);
		return result;

	}

	/**
	 * https://www.itread01.com/content/1547205846.html
	 * 
	 * @param file
	 */
	public void listDir(File file, List<String> list) {
		// List<String> list = new LinkedList<String>();
		log.trace("file.getName()>>{}", file.getName());
		boolean compareFlag = true;
		if (file.isDirectory()) { // 是一個目錄
			// 列出目錄中的全部內容
			File results[] = file.listFiles();
			if (results != null) {
				for (int i = 0; i < results.length; i++) {
					listDir(results[i], list); // 繼續一次判斷
				}
			}
		} else { // 是檔案
			
			for(String skipPath :skipList) {
				//如果不是特定路徑  >> compare 
				if(file.getPath().indexOf(skipPath)>-1) {
					//有找到  不比較  >> break
					compareFlag = false;
					break;
				}else {
					//沒找到 要比較
					compareFlag = true;
				}
			}
			
			if(compareFlag) {
				DateTime filedate = new DateTime(file.lastModified());
				if (isRange(filedate, 1)) {
					list.add(file.getPath());
				}
			}else {
				log.trace("SKIP >> {} ",  file.getName());
			}
		}
		// file.delete(); //刪除!!!!! 根目錄,慎操作
		// 獲取完整路徑
		log.trace("list>>{}", list);
	}

}
