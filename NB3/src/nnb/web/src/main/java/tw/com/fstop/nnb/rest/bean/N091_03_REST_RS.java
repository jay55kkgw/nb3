package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N091_03_REST_RS extends BaseRestBean_GOLD implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4847888482154902163L;

	private String TRNDATE;//日期YYYMMDD
	private String TRNTIME;//時間HHMMSS
	private String TRNSRC;//交易來源
	private String TRNTYP;//交易種類
	private String TRNCOD;//交易註記碼
	private String TRNBDT;//交易日期
	private String ACN;//黃金存摺帳號
	private String CUSIDN;//客戶統一編號
	private String SVACN;//台幣存款帳號
	private String APPTYPE;//預約交易種類
	private String TRNGD;//黃金交易公克數
	private String APPDATE;//預約日期
	private String APPTIME;//預約時間
	
	public String getTRNDATE() {
		return TRNDATE;
	}
	public void setTRNDATE(String tRNDATE) {
		TRNDATE = tRNDATE;
	}
	public String getTRNTIME() {
		return TRNTIME;
	}
	public void setTRNTIME(String tRNTIME) {
		TRNTIME = tRNTIME;
	}
	public String getTRNSRC() {
		return TRNSRC;
	}
	public void setTRNSRC(String tRNSRC) {
		TRNSRC = tRNSRC;
	}
	public String getTRNTYP() {
		return TRNTYP;
	}
	public void setTRNTYP(String tRNTYP) {
		TRNTYP = tRNTYP;
	}
	public String getTRNCOD() {
		return TRNCOD;
	}
	public void setTRNCOD(String tRNCOD) {
		TRNCOD = tRNCOD;
	}
	public String getTRNBDT() {
		return TRNBDT;
	}
	public void setTRNBDT(String tRNBDT) {
		TRNBDT = tRNBDT;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getSVACN() {
		return SVACN;
	}
	public void setSVACN(String sVACN) {
		SVACN = sVACN;
	}
	public String getAPPTYPE() {
		return APPTYPE;
	}
	public void setAPPTYPE(String aPPTYPE) {
		APPTYPE = aPPTYPE;
	}
	public String getTRNGD() {
		return TRNGD;
	}
	public void setTRNGD(String tRNGD) {
		TRNGD = tRNGD;
	}
	public String getAPPDATE() {
		return APPDATE;
	}
	public void setAPPDATE(String aPPDATE) {
		APPDATE = aPPDATE;
	}
	public String getAPPTIME() {
		return APPTIME;
	}
	public void setAPPTIME(String aPPTIME) {
		APPTIME = aPPTIME;
	}
}
