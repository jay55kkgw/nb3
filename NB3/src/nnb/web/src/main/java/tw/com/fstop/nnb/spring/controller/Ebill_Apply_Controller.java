package tw.com.fstop.nnb.spring.controller;

import java.util.Arrays;
import java.util.Base64;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hsqldb.lib.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.google.gson.Gson;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.common.ResultCode;
import tw.com.fstop.nnb.service.DaoService;
import tw.com.fstop.nnb.service.Ebill_Apply_Service;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

@Controller
@RequestMapping(value = "/EBILL/APPLY")
@SessionAttributes({SessionUtil.CUSIDN_EBILLAPPLY,SessionUtil.KAPTCHA_SESSION_KEY,SessionUtil.DPMYEMAIL,
	SessionUtil.MOBTEL,SessionUtil.EBILLAPPLY_DATA})
public class Ebill_Apply_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Ebill_Apply_Service ebill_Apply_Service;

	@Autowired
	private DaoService daoservice;

	@Autowired
	I18n i18n;
	
	private String IBPD_DEMO;
	
	
	//---------- CONTROLLER AREA START----------
	/**
	 * 網銀線上約定作業平台 進入頁
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/accountBinding_p", method = {RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,headers = "content-type=application/json")
	public String accountBinding(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam,@RequestBody Map<String, String> reqParam2, Model model) {
		log.debug("<<<<< Start accountBinding >>>>>");

		String target = "/error";
		BaseResult bs = new BaseResult();
//		Map<String, String> reqParam2=null;
		log.debug(ESAPIUtil.vaildLog("REQ requestparam >>"+CodeUtil.toJson(reqParam)));
		log.debug(ESAPIUtil.vaildLog("REQ3 requestbody >>"+CodeUtil.toJson(reqParam2.get("IBPD_Param"))));
		//確認對方資料格式
		Enumeration<String> headerNames = request.getHeaderNames();
	    if (headerNames != null) {
            while (headerNames.hasMoreElements()) {
            	log.debug("Header: " + request.getHeader(headerNames.nextElement()));
            }
	    }
	    Map<String, String> okMap = null;
	    if(reqParam.isEmpty()) {
	    	okMap = ESAPIUtil.validStrMap(reqParam2);
	    }else {
	    	okMap = ESAPIUtil.validStrMap(reqParam);
	    }
		try {			
			//DEMO模式開關--Start
			IBPD_DEMO=SpringBeanFactory.getBean("IBPD_DEMO")!=null?SpringBeanFactory.getBean("IBPD_DEMO"):"N";
			Boolean checkMacResult;
			if(!IBPD_DEMO.equals("Y"))checkMacResult=ebill_Apply_Service.checkMac(okMap);//驗MAC
			else checkMacResult=true;
			//DEMO模式--End
			if(checkMacResult) {
				bs=ebill_Apply_Service.accountBinding(okMap,model);
			}else {
				bs.setResult(false);
				bs.setMsgCode("0302");
				bs.setMessage("0302 : 訊息押碼錯誤");
			}			
		} catch (Exception e) {
			log.error("accountBinding error{}", e);
			bs.setSYSMessage(ResultCode.SYS_ERROR);
		} finally {
			if (bs.getResult()) {
				target = "ebill_apply/accountBinding";
				model.addAttribute("accountBinding", bs);
			} else {
				target="ebill_apply/accountBinding_resultErr";
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
		/**
		 * 網銀線上約定作業平台 進入頁
		 * 
		 * @param reqParam
		 * @param model
		 * @return
		 */
		@RequestMapping(value = "/accountBinding")
		public String accountBinding_p(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
			log.debug("<<<<< Start accountBinding >>>>>");

			String target = "/error";
			BaseResult bs = new BaseResult();
			log.debug(ESAPIUtil.vaildLog("REQ requestparam >>"+CodeUtil.toJson(reqParam)));
			//財金對測用
//			Map<String, String> reqParam2=null;			
//			log.debug(ESAPIUtil.vaildLog("REQ3 requestbody >>"+CodeUtil.toJson(reqParam2)));
			//確認對方資料格式
			Enumeration<String> headerNames = request.getHeaderNames();
		    if (headerNames != null) {
	            while (headerNames.hasMoreElements()) {
	            	log.debug("Header: " + request.getHeader(headerNames.nextElement()));
	            }
		    }
		    Map<String, String> okMap = null;
		    okMap = ESAPIUtil.validStrMap(reqParam);
		    //財金對測用
//		    if(reqParam.isEmpty()) {
//		    	okMap = ESAPIUtil.validStrMap(reqParam2);
//		    }else {
//		    	okMap = ESAPIUtil.validStrMap(reqParam);
//		    }
			try {			
				//DEMO模式開關--Start
				IBPD_DEMO=SpringBeanFactory.getBean("IBPD_DEMO")!=null?SpringBeanFactory.getBean("IBPD_DEMO"):"N";
				Boolean checkMacResult;
				if(!IBPD_DEMO.equals("Y"))checkMacResult=ebill_Apply_Service.checkMac(okMap);//驗MAC
				else checkMacResult=true;
				//本地測試
//				checkMacResult=true;
				//DEMO模式--End
				if(checkMacResult) {
					bs=ebill_Apply_Service.accountBinding(okMap,model);
				}else {
					bs.setResult(false);
					bs.setMsgCode("0302");
					bs.setMessage("0302 : 訊息押碼錯誤");
					//平台URL
					String BackUrl="https://"+SpringBeanFactory.getBean("IBPD_HOST")+"/contract/BankToIBPD";
					bs.addData("BackUrl",BackUrl);
					if(!StringUtil.isEmpty(reqParam.get("IBPD_Param")))bs.addData("IBPD_Param",reqParam.get("IBPD_Param"));
				}			
			} catch (Exception e1) {
				log.error("accountBinding error{}", e1);
				bs.setSYSMessage(ResultCode.SYS_ERROR);
			} finally {
				if (bs.getResult()) {
					target = "ebill_apply/accountBinding";
					model.addAttribute("accountBinding", bs);
				} else {
					target="ebill_apply/accountBinding_resultErr";
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}
			return target;
		}	
	/**
	 * 網銀線上約定作業平台 身分驗證頁
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/accountBinding_step2")
	public String accountBinding_step2(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.debug("<<<<< Start accountBinding_step2 >>>>>");

		String target = "/error";
		BaseResult bs = new BaseResult();
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.EBILLAPPLY_DATA, null));
			log.trace("REQPARAM >> "+CodeUtil.toJson(bs));
			
			bs=ebill_Apply_Service.accountBinding_step2(okMap,model,bs);
			log.trace("BSDATA >> "+CodeUtil.toJson(bs));
		} catch (Exception e) {
			log.error("accountBinding error{}", e);
			bs.setSYSMessage(ResultCode.SYS_ERROR);
		} finally {
			if (bs.getResult()) {
				IBPD_DEMO=SpringBeanFactory.getBean("IBPD_DEMO")!=null?SpringBeanFactory.getBean("IBPD_DEMO"):"N";
				bs.addData("IBPD_DEMO",IBPD_DEMO);
				target = "ebill_apply/accountBinding_step2";
				model.addAttribute("accountBinding_step2", bs);
			} else {
				target="ebill_apply/accountBinding_resultErr";
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	/**
	 * 網銀線上約定作業平台  資料確認頁
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/accountBinding_step3")
	public String accountBinding_step3(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.debug("<<<<< Start accountBinding_step3 >>>>>");
		String target = "/error";
		BaseResult bs = new BaseResult();
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.EBILLAPPLY_DATA, null));
			log.trace("REQPARAM >> "+CodeUtil.toJson(bs));
			
			bs=ebill_Apply_Service.accountBinding_step3(okMap,model,bs);
			
		} catch (Exception e) {
			log.error("accountBinding error{}", e);
			bs.setSYSMessage(ResultCode.SYS_ERROR);
		} finally {
			if (bs.getResult()) {
				IBPD_DEMO=SpringBeanFactory.getBean("IBPD_DEMO")!=null?SpringBeanFactory.getBean("IBPD_DEMO"):"N";
				bs.addData("IBPD_DEMO",IBPD_DEMO);
				target = "ebill_apply/accountBinding_step3";
				model.addAttribute("accountBinding_step3", bs);
			} else {
				//返回資料
				try {
					BaseResult backdata = new BaseResult();
					Map<String,String>bsdata=(Map<String, String>) bs.getData();
					log.trace("Ebill errorCode>>"+bs.getMsgCode());
					if(StringUtil.isEmpty(bs.getMsgCode()))bsdata.put("RCode", "9902");
					else bsdata.put("RCode",bs.getMsgCode());
					backdata=ebill_Apply_Service.N855canceldata_REST(bsdata);
					bs.addData("IBPD_Param",((Map<String,String>) backdata.getData()).get("IBPD_Param"));
					log.trace("backdata >>"+CodeUtil.toJson(bs.getData()));
				}catch(Exception e) {
					log.error("Backdata error!");
				}
				target="ebill_apply/accountBinding_resultErr";
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 網銀線上約定作業平台  資料確認頁
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/accountBinding_result")
	public String accountBinding_result(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.info("<<<<< Start accountBinding_result >>>>>");

		String target = "ebill_apply/accountBinding_resultErr";
		BaseResult bs = new BaseResult();
		BaseResult reqbs = new BaseResult();
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		/*
		 * form_smscode 輸入otp
		 * form_account 選擇帳號
		 * */
		log.debug("OKMAP >>>>> "+CodeUtil.toJson(okMap));
		reqbs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.EBILLAPPLY_DATA, null));				
		try {
			//驗證帳號是不是本人
			LinkedHashMap<String,String>accountlist= (LinkedHashMap<String, String>) (((Map<String, Object>) reqbs.getData()).get("ACCLIST"));
			log.debug("ACCLIST >>>>> "+CodeUtil.toJson(accountlist));
			String selecAcn=okMap.get("form_account");
			if(accountlist.containsKey(selecAcn)) {
				log.info("是本人帳號");
			}else {
				log.info("非本人帳號");
				String errmsg="I2999";
				reqbs.setErrorMessage(errmsg, ebill_Apply_Service.getMessageByMsgCode(errmsg));
				model.addAttribute(BaseResult.ERROR, reqbs);
				log.debug("BSDATA >>>>> "+CodeUtil.toJson(reqbs.getData()));
				return target;
			}
		}catch(Exception e) {
			log.info("帳號比對失敗");
			log.error("accountBinding result error{}", e);
			String errmsg="FE0001";
			reqbs.setErrorMessage(errmsg, ebill_Apply_Service.getMessageByMsgCode(errmsg));
			model.addAttribute(BaseResult.ERROR, reqbs);
			log.debug("BSDATA >>>>> "+CodeUtil.toJson(reqbs.getData()));
			return target;
		}
		
		try {
			
			log.debug("SESDATA >>>>> "+CodeUtil.toJson(reqbs));
			/*驗OTP驗證碼*/
			bs=ebill_Apply_Service.accountBinding_otpvf(((Map<String, String>) reqbs.getData()).get("CUSIDN"),okMap.get("form_smscode"));

		}catch(Exception e) {
			log.error("accountBinding result error{}", e);
			bs.setSYSMessage(ResultCode.SYS_ERROR);
		}finally {
			if(bs.getResult()) {
				log.info("簡訊驗證通過...");				
			}else {
				//OTP錯誤頁面
				log.error("簡訊驗證失敗...");
				target="ebill_apply/accountBinding_resultErr";
				reqbs.setPrevious("/EBILL/APPLY/accountBinding_step3");
				String errmsg=((Map<String, String>) bs.getData()).get("MSGCOD")+":"+((Map<String, String>) bs.getData()).get("MSGSTR");
				reqbs.setMessage(bs.getMsgCode(),errmsg);
				model.addAttribute(BaseResult.ERROR, reqbs);
				log.debug("BSDATA >>>>> "+CodeUtil.toJson(reqbs.getData()));
				return target;
			}
		}
		
		try {
			/*發電文*/
			//帳號換成使用者選擇的帳號
			String acn = ebill_Apply_Service.paddingLeft(16, "0", (String) okMap.get("form_account"));
			reqbs.addData("CustBankAcnt_Dec",acn);
			reqbs.addData("form_account",(String) okMap.get("form_account"));
			reqbs.addData("CustIdnBan_Dec",((Map<String, String>) reqbs.getData()).get("CUSIDN"));
			bs=ebill_Apply_Service.accountBinding_result((Map<String, String>) reqbs.getData());
			bs.addData("BackUrl",((Map<String, String>) reqbs.getData()).get("BackUrl"));
			/*
			 * 
			 * */

			log.debug("BSDATA >>>>> "+CodeUtil.toJson(bs));
			

		} catch (Exception e) {
			log.error("accountBinding result error{}", e);
			bs.setSYSMessage(ResultCode.SYS_ERROR);
		} finally {
			log.debug("FIN BSDATA >>>>> "+CodeUtil.toJson(bs.getData()));
			//TODO Rcode=4001才是成功
			Map<String,String>datamap=(Map<String, String>) bs.getData();
			if (bs.getResult()&& datamap.get("FORCOD").equals("4001")) {
				bs.addData("BindingAccount", okMap.get("form_account"));
				target = "ebill_apply/accountBinding_result";
				model.addAttribute("accountBinding_result", bs);
			}else if (datamap.get("FORCOD").equals("4509"))  {
				//202112_wei Rcode=4509 已約定之轉帳帳號(之前已經約定成功)
				bs.setMessage(datamap.get("FORCOD")+":"+bs.getMessage());
				log.debug("FIN 4509_BSDATA >>>>> "+CodeUtil.toJson(bs.getData()));
				target="ebill_apply/accountBinding_result4509";
				model.addAttribute(BaseResult.ERROR, bs);
			}else {
				if(!StringUtil.isEmpty(datamap.get("FORCOD"))) {
					bs.setMessage(datamap.get("FORCOD")+":"+bs.getMessage());
				}else if(!StringUtil.isEmpty(datamap.get("FOSRID"))){
					bs.setMessage(datamap.get("FOSRID")+":"+bs.getMessage());
				}else {
					bs.setMessage(bs.getMsgCode()+":"+bs.getMessage());
				}
				target="ebill_apply/accountBinding_resultErr";
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	//---------- CONTROLLER AREA END----------
	
	//---------- AJAX AREA START----------
	/**
	 * 網銀線上約定作業平台 身分驗證ajax
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/login_ajax", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult login_ajax(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.debug("<<<<< Start accountBinding_login_ajax >>>>>");
		BaseResult bs = null;
		BaseResult bs_cap = null;
		try {			
			// 先驗證驗證碼
			bs_cap = validedCapCode(request, response, reqParam, model);
			if (!bs_cap.getResult()) {
				return bs_cap;
			}
			String CUSIDN = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN_EBILLAPPLY, null);
			bs = new BaseResult();
			String userid=reqParam.get("form_username");
			String pinnew=reqParam.get("form_password");					
			bs=ebill_Apply_Service.login(CUSIDN,userid,pinnew);
			
			// 成功登入
			String msgcod = "";
			Map<String,Object> dataMap = (Map<String,Object>) bs.getData();
			if(dataMap!=null) {
				msgcod = dataMap.get("TOPMSG")!=null
						? String.valueOf(dataMap.get("TOPMSG")) : String.valueOf(dataMap.get("msgCode"));				
			} else {
				msgcod = bs.getMsgCode();
			}
			log.trace("LOGIN.MSGCODE: {}", msgcod);

			List<String> loginList = Arrays.asList("","0","O000","0000");			
			// 成功登入初始化登入資訊
			if( bs.getResult() && loginList.contains(msgcod)) {
				// EMAIL、MobildeTel
				String MOBTEL=(String) dataMap.get("MOBTEL");
				String MAILADDR=(String) dataMap.get("MAILADDR");
				if(!StrUtil.isEmpty(MOBTEL))SessionUtil.addAttribute(model, SessionUtil.MOBTEL, MOBTEL);
				if(!StrUtil.isEmpty(MAILADDR))SessionUtil.addAttribute(model, SessionUtil.DPMYEMAIL, MAILADDR);
			}
			log.debug("AJAX BSDATA >>>>> "+CodeUtil.toJson(bs));
		} catch (Exception e) {
			log.error("", e);
		}
		return bs;
	}
	//---------- AJAX AREA END----------
	
	//---------- FUNCTION AREA START----------
	
	// 驗證驗證碼
    public BaseResult validedCapCode(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) throws Exception {
		log.debug("<<<<< Ebill_Apply Capcode Validate START >>>>> ");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// 取得request的驗證碼
			String capCode = StrUtil.isEmpty((reqParam.get("capCode"))) ? "" : reqParam.get("capCode");
			log.trace(ESAPIUtil.vaildLog("capCode >> " + capCode));
			// 取得session的驗證碼
			String code = (String) SessionUtil.getAttribute(model, SessionUtil.KAPTCHA_SESSION_KEY, null);
			log.trace(ESAPIUtil.vaildLog("sessionCapCode >> " + code));
			if(code == null) {
				bs.setResult(Boolean.FALSE);
				bs.setMessage(i18n.getMsg("LB.X1758"));
				log.trace("SESSION TIMEOUT!!!");
				return bs;
			}
			
			// 通過驗證
			log.debug("captcha_valided: " + capCode.equalsIgnoreCase(code) );
			if(capCode.equalsIgnoreCase(code)) {
				bs.setResult(Boolean.TRUE);
				bs.setMessage(i18n.getMsg("LB.X1760"));
				log.trace("capCode.mapping...success!!!");
			} else {
				bs.setResult(Boolean.FALSE);
				bs.setMessage(i18n.getMsg("LB.X1082"));
				log.trace("capCode.mapping...fail!!!");
			}

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			log.error("validedCapCode error >> {}",e);
		}
		log.debug("<<<<< Ebill_Apply Capcode Validate END >>>>> ");
		return bs;
	}
    
	//---------- FUNCTION AREA END----------
}
