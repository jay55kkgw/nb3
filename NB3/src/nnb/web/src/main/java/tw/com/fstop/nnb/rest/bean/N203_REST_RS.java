package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N203_REST_RS  extends BaseRestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4043945665418831783L;

	private String occurMsg;
	private String N203MSGCOD;
	private String NA30MSGCOD;
	private String N913MSGCODE;
	private String CHECKNB;
	private String ACN;
	private String BRHCOD;
	private String EMPLOYER;
	private String ACNTYPE;
	private String TYPE;
	
	public String getOccurMsg() {
		return occurMsg;
	}
	public void setOccurMsg(String occurMsg) {
		this.occurMsg = occurMsg;
	}
	public String getN203MSGCOD() {
		return N203MSGCOD;
	}
	public void setN203MSGCOD(String n203msgcod) {
		N203MSGCOD = n203msgcod;
	}
	public String getNA30MSGCOD() {
		return NA30MSGCOD;
	}
	public void setNA30MSGCOD(String nA30MSGCOD) {
		NA30MSGCOD = nA30MSGCOD;
	}
	public String getN913MSGCODE() {
		return N913MSGCODE;
	}
	public void setN913MSGCODE(String n913msgcode) {
		N913MSGCODE = n913msgcode;
	}
	public String getCHECKNB() {
		return CHECKNB;
	}
	public void setCHECKNB(String cHECKNB) {
		CHECKNB = cHECKNB;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getBRHCOD() {
		return BRHCOD;
	}
	public void setBRHCOD(String bRHCOD) {
		BRHCOD = bRHCOD;
	}
}
