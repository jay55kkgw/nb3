package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N948_REST_RQ extends BaseRestBean_TW implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -549345821136929106L;
	
	private String CUSIDN;

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	
	
}
