package tw.com.fstop.nnb.spring.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import fstop.orm.po.OLD_TXNADDRESSBOOK;
import fstop.orm.po.TXNADDRESSBOOK;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.idgate.bean.N930EE_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N930EE_IDGATE_DATA_VIEW;
import tw.com.fstop.nnb.custom.annotation.ISTXNLOG;
import tw.com.fstop.nnb.service.DaoService;
import tw.com.fstop.nnb.service.IdGateService;
import tw.com.fstop.nnb.service.Personal_ChgEmail_Service;
import tw.com.fstop.tbb.nnb.dao.AdmEmpInfoDao;
import tw.com.fstop.tbb.nnb.dao.Old_TxnAddressBookDao;
import tw.com.fstop.tbb.nnb.dao.TxnAddressBookDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({ SessionUtil.PD, SessionUtil.DPMYEMAIL, SessionUtil.DPUSERNAME, SessionUtil.CONFIRM_LOCALE_DATA,SessionUtil.IDGATE_TRANSDATA,
		SessionUtil.RESULT_LOCALE_DATA, SessionUtil.AUTHORITY, SessionUtil.CUSIDN, SessionUtil.TRANSFER_DATA , SessionUtil.DOUBLEMAIL, SessionUtil.EMAILALERT })
@Controller
@RequestMapping(value = "/PERSONAL/SERVING")
public class Personal_mailChg_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	DaoService daoService;

	@Autowired
	private AdmEmpInfoDao admEmpInfoDao;

	@Autowired
	private Personal_ChgEmail_Service personal_chgEmail_service;

	@Autowired
	private TxnAddressBookDao txnAddressBookDao;
	
	@Autowired
	private String isSyncNNB;
	
	@Autowired
	private Old_TxnAddressBookDao old_txnAddressBookDao;

	@Autowired		 
	IdGateService idgateservice;

	@RequestMapping(value = "/mail_setting")
	public String go_menupage(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String cusidn;
		String target = "/error";
		try {
			cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			if (cusidn != null) {
				target = "/personal_serving/mail_setting_menu";
			}
		} catch (UnsupportedEncodingException e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("go_menupage error >> {}", e);
		}
		return target;
	}

	@RequestMapping(value = "/mail_setting_choose")
	public String menupagechoose(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace(ESAPIUtil.vaildLog("reqParam {}" + CodeUtil.toJson(reqParam)));
		String target = "/error";
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		BaseResult bs = new BaseResult();
		boolean hasLocale = okMap.containsKey("locale");
		String cusidn="";
		String jsondc = "";
		try {
			//回到第一頁 , 清空紀錄資料的session 
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.DOUBLEMAIL, "");
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				} else {
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			}

			if (!hasLocale) {
				if ("DPSETUPE".equals(okMap.get("type"))) {
					log.trace("get_oldEmail");
					// IKEY要使用的JSON:DC
					jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam), "UTF-8");
					log.trace(ESAPIUtil.vaildLog("Email jsondc: " + jsondc));

					cusidn = new String(Base64.getDecoder()
							.decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
					String notify = daoService.get_notifyInfo(cusidn);
					log.debug("ISCUSIDN >> {}", admEmpInfoDao.isEmpCUSID(cusidn));
					bs.addData("ISCUSIDN", admEmpInfoDao.isEmpCUSID(cusidn) ? "Y" : "N");
					bs.setResult(true);
					// 2019/10/16新增將CUSIDN(UID)送到前端在更改時餘後端驗證是否有資料竄改
					bs.addData("UID", cusidn);
					bs.addData("notify", notify);
					bs.addData("type", okMap.get("type"));
					bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
					bs.addData("DPMYEMAIL", (String) SessionUtil.getAttribute(model, SessionUtil.DPMYEMAIL, null));
					bs.addData("jsondc", jsondc);
					
					SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				}
				if ("DPSETUPA".equals(okMap.get("type"))) {
					log.trace("get_addbkInfo");
					bs = new BaseResult();
					cusidn = new String(Base64.getDecoder()
							.decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
					List<TXNADDRESSBOOK> list = daoService.findByDPUserID(cusidn);
					log.trace(ESAPIUtil.vaildLog("addressBook >>" + list));
					bs.setResult(true);
					bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
					bs.addData("ADDRESSBOOK", list);
					bs.addData("ADDRESSBOOKSIZE", list.size());
					bs.addData("type", okMap.get("type"));
					// bs.addData("DPUSERID", cusidn);
					log.trace("bs >>{}", bs.getData());
					SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
					
				}
				
				//IDGATE身分
				String idgateUserFlag="N";		 
				Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
				try {		 
					if(IdgateData==null) {
						IdgateData = new HashMap<String, Object>();
					}
					BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
					if(tmp.getResult()) {		 
						idgateUserFlag="Y";                  		 
					}		 
					tmp.addData("idgateUserFlag",idgateUserFlag);		 
					IdgateData.putAll((Map<String, String>) tmp.getData());
					SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);
					
				}catch(Exception e) {		 
					log.debug("idgateUserFlag error {}",e);		 
				}		 
				model.addAttribute("idgateUserFlag",idgateUserFlag); 

				
				
			}
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("menupagechoose error >> {}", e);
		} finally {
			if (bs != null && bs.getResult() && "DPSETUPE".equals(((Map<String, String>) bs.getData()).get("type"))) {
				log.trace("bs >>{}", bs);
				model.addAttribute("orgInfo", bs);
				target = "/personal_serving/mail_set";

			} else if (bs != null && bs.getResult()
					&& "DPSETUPA".equals(((Map<String, String>) bs.getData()).get("type"))) {
				log.trace("bs >>{}", bs);
				model.addAttribute("addbkInfo", bs);
				target = "/personal_serving/mail_myaddbk_detail";
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		return target;
	}
	
	@RequestMapping(value = "/mailChg_comfirm")
	public String mailChg_comfirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		String previous = "";
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		BaseResult bs = new BaseResult();
		try {
			//如果從聲明書頁來 , 資料從 TRANSFER_DATA 拿
			if (StrUtil.isNotEmpty(okMap.get("READST"))) {
					previous = "/VERIFYMAIL/statement_page";
					bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
					okMap.putAll((Map<String, String>) bs.getData());
				
			} else {
			//如果從輸入頁來 , 資料直接從 	okMap 拿
				previous = "/PERSONAL/SERVING/mail_setting_choose?type=DPSETUPE";
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, okMap);
			}
			
			String jsondc = URLEncoder.encode(CodeUtil.toJson(okMap), "UTF-8");
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			okMap.put("DPUSERID", cusidn);
			okMap.put("jsondc", jsondc);
			okMap.put("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
			okMap.put("DPMYEMAIL", (String) SessionUtil.getAttribute(model, SessionUtil.DPMYEMAIL, null));
			okMap.put("PREVIOUS", previous);
			
			
			// IDGATE transdata            		 
			Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			log.trace("IdgateData() >>{}",IdgateData);
			String adopid = "N930EE";
			String title = "您有一筆我的Email設定更新待確認";
			if(IdgateData != null) {
				model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
				if(IdgateData.get("idgateUserFlag").equals("Y")) {
					okMap.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
					okMap.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
					IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N930EE_IDGATE_DATA.class, N930EE_IDGATE_DATA_VIEW.class, okMap));
					SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
				}
			}
			model.addAttribute("idgateAdopid", adopid);
			model.addAttribute("idgateTitle", title);
			
		} catch (Exception e) {
			log.error("mailChg_comfirm error >> {}", e);
		} finally {
			target = "/personal_serving/mailChg_comfirm";
			model.addAttribute("mailChg_comfirm",okMap);
		}
		return target;
	}

	@ISTXNLOG(value = "N930E_E")
	@RequestMapping(value = "/mailChg")
	public String chg_mail_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace(ESAPIUtil.vaildLog("reqParam {}" + CodeUtil.toJson(reqParam)));
		String target = "/error";
		String jsondc = "";
		String pkcs7Sign = "";
		BaseResult bs = null;
		String doubleMail = "";
		try {
			doubleMail = (String) SessionUtil.getAttribute(model, SessionUtil.DOUBLEMAIL, null);
			bs = new BaseResult();
			jsondc = reqParam.get("jsondc");
			pkcs7Sign = reqParam.get("pkcs7Sign");
			reqParam.remove("jsondc");
			reqParam.remove("pkcs7Sign");
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			if("Y".equals(doubleMail)) {
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				okMap.putAll((Map<String, String>) bs.getData());
			}else {
				 Map<String,String> dataMap =(Map<String,String>)SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				 okMap.putAll(dataMap);
			}
			
			okMap.put("jsondc", jsondc);
			okMap.put("pkcs7Sign", pkcs7Sign);
			// pretendXSS
			//log.trace("BEFORE　XSSFilter >>" + CodeUtil.toJson(okMap));
			okMap.put("NEW_EMAIL", ESAPIUtil.pretendNameColumnFromXSS(okMap.get("NEW_EMAIL")));
			okMap.put("EXECUTEFUNCTION", "UPDATE_TX");
			//log.trace("AFTER　XSSFilter >>" + CodeUtil.toJson(okMap));
			// 2019/10/16修改 防止竄改資料
			String UID = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			okMap.put("UID", UID);
			okMap.put("DPUSERID", UID);
			okMap.put("CUSIDN", UID);

			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}

			if (!hasLocale) {

				bs = new BaseResult();
				// log.debug("sessionId >>{}", sessionId);
				if(okMap.get("FGTXWAY").equals("7")) {		 
					Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
					Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N930EE_IDGATE");
					okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
					okMap.put("txnID", (String)IdgateData.get("txnID"));		 
					okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
				}
				
				String IP = WebUtil.getIpAddr(request);
				log.debug(ESAPIUtil.vaildLog("IP >> " + IP));
				okMap.put("IP",IP);
				
				bs = personal_chgEmail_service.chg_mail(okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("chg_mail_result error >> {}", e);
		} finally {
			if (bs != null && bs.getResult()) {
				log.trace("bs >>{}", bs);
				model.addAttribute("chg_result", bs);
				if("Y".equals(doubleMail)) {
					model.addAttribute("DOUBLEMAIL","Y");
				}else {
					SessionUtil.addAttribute(model, SessionUtil.DPMYEMAIL,
							((Map<String, String>) bs.getData()).get("DPMYEMAIL"));
				}
				target = "/personal_serving/mail_result";
				SessionUtil.addAttribute(model, SessionUtil.EMAILALERT, "");
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		return target;

	}

	@ISTXNLOG(value = "N930E_A")
	@RequestMapping(value = "/ADDRESSBOOK")
	public String addressbook_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace(ESAPIUtil.vaildLog("reqParam {}" + CodeUtil.toJson(reqParam)));
		String target = "/error";
		BaseResult bs = null;
		String action = "";
		Map<String, String> result = new HashMap<String, String>();
		TXNADDRESSBOOK newAddrBook = null;
		OLD_TXNADDRESSBOOK old_po = null;
		String dpuserid = "", dpgoname = "", dpabmail = "" , dpaddbkid ="";
		Integer i_dpaddbkid = null;
		// 解決 Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			okMap.put("DPUSERID", cusidn);

			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}

			if (!hasLocale) {

				bs = new BaseResult();
				// pretendXSS
				// log.trace("BEFORE XSSFilter >>" + CodeUtil.toJson(okMap));
				okMap.put("DPGONAME", ESAPIUtil.pretendNameColumnFromXSS(okMap.get("DPGONAME")));
				okMap.put("DPABMAIL", ESAPIUtil.pretendNameColumnFromXSS(okMap.get("DPABMAIL")));
				// log.trace("AFTER XSSFilter >>" + CodeUtil.toJson(okMap));
				// pretendXSS--end
				// Map result = daoService.action(okMap);
				switch (okMap.get("EXECUTEFUNCTION")) {
				case "INSERT":
					newAddrBook = new TXNADDRESSBOOK();
					DateTime d = new DateTime();

					newAddrBook.setDPUSERID(okMap.get("DPUSERID"));
					newAddrBook.setDPGONAME(okMap.get("DPGONAME"));
					newAddrBook.setDPABMAIL(okMap.get("DPABMAIL"));
					newAddrBook.setLASTDATE(d.toString("yyyyMMdd"));
					newAddrBook.setLASTTIME(d.toString("HHmmss"));
					txnAddressBookDao.save(newAddrBook);
					log.trace(ESAPIUtil.vaildLog("newAddrBook >>{}" + newAddrBook));
					action = "INSERT";
					result.put("DPGONAME", okMap.get("DPGONAME"));
					result.put("DPABMAIL", okMap.get("DPABMAIL"));
					try {
						if("Y".equals(isSyncNNB)) {
							dpuserid = newAddrBook.getDPUSERID();
							dpgoname = newAddrBook.getDPGONAME();
							dpabmail = newAddrBook.getDPABMAIL();
							old_po = old_txnAddressBookDao.findByInput(dpuserid, dpgoname, dpabmail);
							if(old_po ==null) {
								old_po = CodeUtil.objectCovert(OLD_TXNADDRESSBOOK.class, newAddrBook);
								old_po.setDPADDBKID(null);
								old_txnAddressBookDao.save(old_po);
							}else {
								log.warn("has  same data  nothing to do ");
//								log.warn("has  same data  nothing to do >>{}", CodeUtil.toJson(old_po));
							}
						}
					} catch (Throwable e) {
						log.error("isSyncNNB.OLD_TXNADDRESSBOOK.INSERT.ERROR>>{}",e);
					}
					
					break;
				case "UPDATE":
					dpaddbkid = okMap.get("DPADDBKID");
					
					
					TXNADDRESSBOOK addrBook = txnAddressBookDao.get(TXNADDRESSBOOK.class, new Integer(dpaddbkid));
					dpuserid = addrBook.getDPUSERID();
					dpgoname = addrBook.getDPGONAME();
					dpabmail = addrBook.getDPABMAIL();
					
					addrBook.setDPUSERID(okMap.get("DPUSERID"));
					addrBook.setDPABMAIL(okMap.get("DPABMAIL"));
					addrBook.setDPGONAME(okMap.get("DPGONAME"));
					txnAddressBookDao.saveOrUpdate(addrBook);
					action = "UPDATE";
					result.put("DPGONAME", okMap.get("DPGONAME"));
					result.put("DPABMAIL", okMap.get("DPABMAIL"));
					try {
						try {
							if("Y".equals(isSyncNNB)) {
								
								old_po = old_txnAddressBookDao.findByInput(dpuserid, dpgoname, dpabmail);
//								log.debug("old_po>>{}",old_po);
								if(old_po ==null) {
									old_po = CodeUtil.objectCovert(OLD_TXNADDRESSBOOK.class, addrBook);
									old_po.setDPADDBKID(null);
									old_txnAddressBookDao.save(old_po);
								}else {
									i_dpaddbkid = old_po.getDPADDBKID();
									old_po = CodeUtil.objectCovert(OLD_TXNADDRESSBOOK.class, addrBook);
									old_po.setDPADDBKID(i_dpaddbkid);
									old_txnAddressBookDao.update(old_po);
								}
							}
						} catch (Throwable e) {
							log.error("isSyncNNB.OLD_TXNADDRESSBOOK.UPDATE.ERROR>>{}",e);
						}
					} catch (Throwable e) {
						log.error("isSyncNNB.OLD_TXNADDRESSBOOK.UPDATE.ERROR>>{}",e);
					}
					
					break;
					
				case "DELETE":

					dpaddbkid = okMap.get("DPADDBKID");
					dpuserid = okMap.get("DPUSERID");
					dpgoname = okMap.get("DPGONAME");
					dpabmail = okMap.get("DPABMAIL");
					newAddrBook =  txnAddressBookDao.get(TXNADDRESSBOOK.class, new Integer(dpaddbkid));
					txnAddressBookDao.remove(newAddrBook);
					action = "DELETE";
					result.put("DPGONAME", okMap.get("DPGONAME"));
					result.put("DPABMAIL", okMap.get("DPABMAIL"));
					try {
						if("Y".equals(isSyncNNB)) {
							dpuserid = newAddrBook.getDPUSERID();
							dpgoname = newAddrBook.getDPGONAME();
							dpabmail = newAddrBook.getDPABMAIL();
							old_po = old_txnAddressBookDao.findByInput(dpuserid, dpgoname, dpabmail);
							if(old_po != null) {
								old_txnAddressBookDao.remove(old_po);
							}else {
								log.warn("no data  nothing to do...");
							}
						}
					} catch (Throwable e) {
						log.error("isSyncNNB.OLD_TXNADDRESSBOOK.DELETE.ERROR>>{}",e);
					}
					break;
				}
				
				 SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				 bs.setResult(true);
				 bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				 bs.addData("result", action);
				 bs.addData("DPGONAME", result.get("DPGONAME"));
				 bs.addData("DPABMAIL", result.get("DPABMAIL"));
				// }
			}
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("addressbook_result error >> {}", e);
		} finally {
			if (bs != null && bs.getResult()) {
				log.trace("bs >>{}", bs);
				model.addAttribute("addressbook_result", bs);
				target = "/personal_serving/addressbook_result";
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		return target;
	}
}
