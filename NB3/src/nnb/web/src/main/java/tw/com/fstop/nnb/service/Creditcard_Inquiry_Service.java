package tw.com.fstop.nnb.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmMsgCodeDao;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.web.util.WebUtil;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

/**
 * 
 * 功能說明 : 信用卡查詢類功能Service
 *
 */
@Service
public class Creditcard_Inquiry_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	AdmMsgCodeDao admMsgCodeDao;
	@Autowired
	private I18n i18n;

	/**
	 * N810走Rest
	 * 
	 * @param custid
	 * @param
	 * @return
	 */
	public BaseResult overview_rest(Map<String, String> reqParam) {
		log.trace("creditcard_service_overview...");
		log.trace(ESAPIUtil.vaildLog("custid: " + reqParam.get("CUSIDN")));
		// 處理結果
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// 電文N810
			bs = N810_REST(reqParam);

			// 判斷TOPMSG來決定導向正確頁or錯誤訊息頁

			if ("".equals(((Map<String, String>) bs.getData()).get("TOPMSG"))) {
				// 正確頁
				// 把Client端需要的資料重新封裝
				overview_processing_rest(bs);
				log.debug("N810_REST_RESULT: " + bs.getData());

			} else {
				// 錯誤訊息頁
				bs.setResult(false);
				bs.setErrorMessage(((Map<String, String>) bs.getData()).get("TOPMSG"),
						((Map<String, String>) bs.getData()).get("msgName"));
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("overview_rest error >> {}",e);
		}
		// 回傳處理結果
		return bs;
	}

	// /**
	// * 信用卡總覽_BaseResult封裝Client端所需資料
	// *
	// * @param bs
	// * @return
	// */
	public void overview_processing(BaseResult bs) {
		Map<String, Object> mapN810 = null;
		int count = 0;
		int bcardcount = 0;
		String bcardflag = "false";

		try {
			// 定義商務卡代碼種類
			Set<String> set_BizCardType = new HashSet<String>();
			set_BizCardType.add("600");
			set_BizCardType.add("601");
			set_BizCardType.add("602");
			set_BizCardType.add("603");
			set_BizCardType.add("604");
			set_BizCardType.add("606");

			// N810電文資料
			mapN810 = (Map<String, Object>) bs.getData();
			// 將_DBCODE做處理 01=>全繳 02=>最低
			if ("01".equals(mapN810.get("_DBCODE"))) {
				mapN810.put("_DBCODE", i18n.getMsg("LB.Full_payment"));
			} else if ("02".equals(mapN810.get("_DBCODE"))) {
				mapN810.put("_DBCODE", i18n.getMsg("LB.Mini_payment"));
			}
			if (null != mapN810.get("_PAYMT_ACCT") && !"".equals(mapN810.get("_PAYMT_ACCT"))) {
				String acctNum = (String) mapN810.get("_PAYMT_ACCT");
				String coverNum = acctNum.substring(0, 6) + "***" + acctNum.substring(9);
				mapN810.put("_PAYMT_ACCT_COVER", coverNum);
			}

			// 將金額類format
			mapN810.put("_CRLIMITFMT", NumericUtil.fmtAmount((String) mapN810.get("_CRLIMIT"), 2));
			mapN810.put("_CSHLIMITFMT", NumericUtil.fmtAmount((String) mapN810.get("_CSHLIMIT"), 2));
			mapN810.put("_ACCBALFMT", NumericUtil.fmtAmount((String) mapN810.get("_ACCBAL"), 2));
			mapN810.put("_CSHBALFMT", NumericUtil.fmtAmount((String) mapN810.get("_CSHBAL"), 2));
			mapN810.put("_INT_RATEFMT", NumericUtil.fmtAmount((String) mapN810.get("_INT_RATE"), 3));
			mapN810.put("_CURR_BALFMT", NumericUtil.fmtAmount((String) mapN810.get("_CURR_BAL"), 2));
			mapN810.put("_TOTL_DUEFMT", NumericUtil.fmtAmount((String) mapN810.get("_TOTL_DUE"), 2));
			// 兩個特別轉給轉帳功能使用
			mapN810.put("_CURR_BALVAL",
					NumericUtil.fmtAmount((String) mapN810.get("_CURR_BAL"), 0).replaceAll(",", ""));
			mapN810.put("_TOTL_DUEVAL",
					NumericUtil.fmtAmount((String) mapN810.get("_TOTL_DUE"), 0).replaceAll(",", ""));

			if (null != mapN810.get("_Table") && !"".equals(mapN810.get("_Table"))) {
				ArrayList<Map<String, String>> callTable = (ArrayList<Map<String, String>>) mapN810.get("_Table");
				for (Map<String, String> map : callTable) {
					String cardNum = map.get("_CARDNUM");
					String coverNum = "**** **** **** " + cardNum.substring(12);
					map.put("CCNCOVER", coverNum);
				}
			}
			// CCN COVER rule : **** **** **** xxxx
			log.trace("rtnMap_n810: " + mapN810);
			log.trace("rtnMap_n810_Table: " + mapN810.get("_Table"));
			// _Table資料
			Gson gs = new Gson();
			List<Map<String, Object>> tabletoList = gs.fromJson(gs.toJson(mapN810.get("_Table")), ArrayList.class);
			log.trace("tabletoList: " + tabletoList);
			// _Table逐筆取得資料
			for (Map<String, Object> obj : tabletoList) {
				// 計算信用卡總數
				count++;
				// 取得Data裡的_Table的資料
				log.trace("obj" + count + ": " + obj);
				Map<String, Object> map = gs.fromJson(gs.toJson(obj), Map.class);
				log.trace("tabletoMap" + count + ": " + map);
				// 計算商務卡數量
				if (set_BizCardType.contains(mapN810.get("_CARDTYPE"))) {
					bcardcount++;
				}
			}
			// Client端要發N820需要用到
			if (bcardcount != 0) {
				bcardflag = "true";
			}
			// 信用卡數量&商務卡數量運算結果決定Client端畫面呈現
			String count_result = "";
			if (count - bcardcount > 0 && bcardcount > 0) {
				count_result = "a";
			} else if (count - bcardcount > 0 && bcardcount == 0) {
				count_result = "b";
			} else if (count - bcardcount > 0 && bcardcount == 0) {
				count_result = "c";
			}
			// 將Client需要的結果封裝進bs
			mapN810.put("count", count);
			mapN810.put("count_result", count_result);
			mapN810.put("bcardflag", bcardflag);
			bs.setData(mapN810);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("overview_processing error >> {}",e);
		}
	}

	/**
	 * 信用卡總覽_BaseResult封裝Client端所需資料
	 * 
	 * @param bs
	 * @return
	 */
	public void overview_processing_rest(BaseResult bs) {
		Map<String, Object> mapN810 = null;
		int count = 0;
		int bcardcount = 0;
		String bcardflag = "false";

		try {

			// 定義商務卡代碼種類
			Set<String> set_BizCardType = new HashSet<String>();
			set_BizCardType.add("600");
			set_BizCardType.add("601");
			set_BizCardType.add("602");
			set_BizCardType.add("603");
			set_BizCardType.add("604");
			set_BizCardType.add("606");
			List<Map<String, String>> cardList = new ArrayList<Map<String, String>>();
			Map<String, String> cnt = new HashMap<String, String>();

			// N810電文資料
			mapN810 = (Map<String, Object>) bs.getData();
			if (mapN810.get("RECNUM") != null && mapN810.get("RECNUM") != "") {
				mapN810.put("RECNUM", NumericUtil.fmtAmount((String) mapN810.get("RECNUM"), 0));
			}
			// 將_DBCODE做處理 01=>全繳 02=>最低
			if ("01".equals(mapN810.get("DBCODE"))) {
				mapN810.put("DBCODE", i18n.getMsg("LB.Full_payment"));
			} else if ("02".equals(mapN810.get("DBCODE"))) {
				mapN810.put("DBCODE", i18n.getMsg("LB.Mini_payment"));
			}
			if (null != mapN810.get("PAYMT_ACCT") && !"".equals(mapN810.get("PAYMT_ACCT"))) {
				String acctNum = (String) mapN810.get("PAYMT_ACCT");
				String coverNum = acctNum.substring(0, 6) + "***" + acctNum.substring(9);
				mapN810.put("PAYMT_ACCT_COVER", coverNum);
			}
			// // 日期format
			String stop_day=(String) mapN810.get("STOP_DAY");
			int i_Month = Integer.parseInt(stop_day.substring(3, 5)) - 1;	  		
	  		int i_Year = Integer.parseInt(stop_day.substring(0, 3)); 
	  		String str_Month;
	  		if (i_Month == 0)
	  		{	
	  			str_Month = "12";
	  			i_Year = i_Year - 1;		
	  		}	
	  		else
	  		{
		  		if (i_Month < 10)	  			
	  				str_Month = "0" + i_Month;
		  		else
		  			str_Month = "" + i_Month;
	  		}
	  		mapN810.put("BILL_DAY", String.valueOf(i_Year) + "/" + str_Month);
			mapN810.put("STOP_DAY", DateUtil.convertDate(2, (String) mapN810.get("STOP_DAY"), "yyyMMdd", "yyy/MM/dd"));
			mapN810.put("STMT_DAY", DateUtil.convertDate(2, (String) mapN810.get("STMT_DAY"), "yyyMMdd", "yyy/MM/dd"));
			// 將金額類format
			mapN810.put("CRLIMITFMT", NumericUtil.fmtAmount((String) mapN810.get("CRLIMIT"), 2));
			mapN810.put("CSHLIMITFMT", NumericUtil.fmtAmount((String) mapN810.get("CSHLIMIT"), 2));
			mapN810.put("ACCBALFMT", NumericUtil.fmtAmount((String) mapN810.get("ACCBAL"), 2));
			mapN810.put("CSHBALFMT", NumericUtil.fmtAmount((String) mapN810.get("CSHBAL"), 2));
			mapN810.put("INT_RATEFMT", NumericUtil.fmtAmount((String) mapN810.get("INT_RATE"), 3));
			mapN810.put("CURR_BALFMT", NumericUtil.fmtAmount((String) mapN810.get("CURR_BAL"), 2));
			mapN810.put("TOTL_DUEFMT", NumericUtil.fmtAmount((String) mapN810.get("TOTL_DUE"), 2));
			// 兩個特別轉給轉帳功能使用
			mapN810.put("CURR_BALVAL", NumericUtil.fmtAmount((String) mapN810.get("CURR_BAL"), 0).replaceAll(",", ""));
			mapN810.put("TOTL_DUEVAL", NumericUtil.fmtAmount((String) mapN810.get("TOTL_DUE"), 0).replaceAll(",", ""));
			
			if (null != mapN810.get("REC") && !"".equals(mapN810.get("REC"))) {
				ArrayList<Map<String, String>> callTable = (ArrayList<Map<String, String>>) mapN810.get("REC");
				int countRec = 0;
				for (Map<String, String> map : callTable) {
					String cardNum = map.get("CARDNUM");
					log.trace("cardNum");
					String coverNum = "**** **** **** " + cardNum.substring(12);
					map.put("CCNCOVER", coverNum);
					cnt.put(map.get("TYPENAME"), map.get("CARDNUM"));
					if (null != map.get("CR_NAME") && !"".equals(map.get("CR_NAME"))) {
						map.put("CR_NAME", WebUtil.hideusername1Convert(map.get("CR_NAME")));
					}
					if(countRec==0 && callTable.size()>1) {
						mapN810.put("OnlyOneCard", coverNum);
					}
					countRec++;
				}
				cardList.add(cnt);
			}
			// CCN COVER rule : **** **** **** xxxx
			log.trace("rtnMap_n810: " + mapN810);
			log.trace("rtnMap_n810_Table: " + mapN810.get("REC"));
			// _Table資料
			Gson gs = new Gson();
			List<Map<String, Object>> tabletoList = gs.fromJson(gs.toJson(mapN810.get("REC")), ArrayList.class);
			log.trace("tabletoList: " + tabletoList);
			// _Table逐筆取得資料
			for (Map<String, Object> obj : tabletoList) {
				// 計算信用卡總數
				count++;
				// 取得Data裡的_Table的資料
				log.trace("obj" + count + ": " + obj);
				Map<String, Object> map = gs.fromJson(gs.toJson(obj), Map.class);
				log.trace("tabletoMap" + count + ": " + map);
				// 計算商務卡數量
				if (set_BizCardType.contains(map.get("CARDTYPE"))) {
					bcardcount++;
				}
			}
			// Client端要發N820需要用到
			if (bcardcount != 0) {
				bcardflag = "true";
			}
			// 信用卡數量&商務卡數量運算結果決定Client端畫面呈現
			String count_result = "";
			if (count - bcardcount > 0 && bcardcount > 0) {
				count_result = "a";
			} else if (count - bcardcount > 0 && bcardcount == 0) {
				count_result = "b";
			} else if (count - bcardcount == 0 && bcardcount > 0) {
				count_result = "c";
			}
			// 將Client需要的結果封裝進bs
			mapN810.put("count", count);
			mapN810.put("count_result", count_result);
			mapN810.put("bcardflag", bcardflag);
			mapN810.put("cardList", cardList);
			mapN810.put("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
			bs.setData(mapN810);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("overview_processing_rest error >> {}",e);
		}
	}

	/**
	 * 信用卡歷史帳單明細查詢_輸入頁
	 * 
	 * @param sessionId
	 * @return
	 */
	public BaseResult history_p1(Map<String, String> reqParam) {
		log.trace("creditcard_service_history...");

		// 處理結果
		BaseResult n810result = null;
		BaseResult n813result = null;
		BaseResult bs = null;
		try {
			n810result = new BaseResult();
			n813result = new BaseResult();
			bs = new BaseResult();
			//Avoid TXNLOG
			reqParam.put("ADOPID", "__N810");
			n810result = N810_REST(reqParam);
			n813result = N813_REST(reqParam);

			Map<String, String> mapN810 = (Map<String, String>) n810result.getData();
			Map<String, String> mapN813 = (Map<String, String>) n813result.getData();

			String n810msgcode = mapN810.get("msgCode");
			String n813msgcode = mapN813.get("msgCode");
			// part1 兩道電文都E091 導錯誤頁
			if ("E091".equals(n810msgcode) && "E091".equals(n813msgcode)) {
				bs.setResult(Boolean.FALSE);
				bs.setErrorMessage("E091", mapN810.get("msgName"));
				return bs;
			}

			// part2 判斷是否有商務卡
			// 定義商務卡代碼種類
			Set set_BizCardType = new HashSet();
			set_BizCardType.add("600");
			set_BizCardType.add("601");
			set_BizCardType.add("602");
			set_BizCardType.add("603");
			set_BizCardType.add("604");
			set_BizCardType.add("606");

			String bcardflag = "";
			String cardtype = "";
			int bcardcount = 0;
			int n810counter = 0;

			if ("0".equals(n810msgcode)) {
				List<Map<String, String>> n810cardList = ((List<Map<String, String>>) ((Map<String, Object>) n810result
						.getData()).get("REC"));
				for (Map<String, String> eachMap : n810cardList) {
					if (set_BizCardType.contains(eachMap.get("CARDTYPE"))) {
						bcardflag = "TRUE";
						bcardcount++;
					}
					if (!"".equals(eachMap.get("CARDNUM"))) {
						n810counter++;
					}
				}

				if (n810counter > 0 && (n810counter - bcardcount > 0) && bcardcount > 0 && bcardflag.equals("TRUE")) {
					cardtype = "BOTH";
				} else if (n810counter > 0 && (n810counter - bcardcount == 0) && bcardflag.equals("TRUE")) {
					cardtype = "BUSINESS";
				} else if (n810counter > 0 && bcardcount == 0 && !bcardflag.equals("TRUE")) {
					cardtype = "NORMAL";
				}
			} else {
				bs.addData("n810MsgName", mapN810.get("msgName"));
			}

			bs.addData("n810MsgCode", n810msgcode);
			bs.addData("n813MsgCode", n813msgcode);
			bs.addData("n813MsgName", mapN813.get("msgName"));
			bs.addData("cardtype", cardtype);
			bs.addData("bcardflag", bcardflag);
			bs.setResult(Boolean.TRUE);

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("history_p1 error >> {}",e);
		}
		// 回傳處理結果
		return bs;
	}

	/**
	 * 信用卡歷史帳單明細查詢_step1
	 * 
	 * @param sessionId
	 * @return
	 */
	public BaseResult history_step1(Map<String, String> reqParam) {
		log.trace("creditcard_service_history_step1...");

		// 處理結果
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs.setData(reqParam);
			bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
			bs.setResult(true);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("history_step1 error >> {}",e);
		}

		// 回傳處理結果
		return bs;
	}

	/**
	 * 信用卡歷史帳單明細查詢_網頁顯示
	 * 
	 * @param
	 * @return
	 */
	public BaseResult history_query(Map<String, String> reqParam) {
		log.trace("creditcard_service_history_online...");

		// 處理結果
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = BHWS_REST(reqParam);
			bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
			bs.addData("FGPERIOD", reqParam.get("FGPERIOD"));
			bs.addData("CARDTYPE", reqParam.get("CARDTYPE"));
			bs.addData("BILL_NO", reqParam.get("BILL_NO"));
			bs.addData("FUNCTION", reqParam.get("FUNCTION"));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("history_query error >> {}",e);
		}

		// 回傳處理結果
		return bs;
	}

	public BaseResult card_unbilled_p1(Map<String, String> reqParam) {
		// 取得n810
		log.trace("card_unbilled_p1_info...");
		log.trace(ESAPIUtil.vaildLog("custid >>{} " + reqParam.get("CUSIDN")));
		// 處理結果
		BaseResult bs = null;
		BaseResult res810 = null;
		BaseResult res813 = null;
		try {

			bs = new BaseResult();
			res810 = new BaseResult();
			res813 = new BaseResult();
			// 電文N810
			//Avoid TXNLOG
			reqParam.put("ADOPID", "__N810");
			res810 = N810_REST(reqParam);
			// 電文N813
			res813 = N813_REST(reqParam);

			Map<String, Object> N810 = (Map<String, Object>) res810.getData();
			String n810MsgCode = (String) N810.get("TOPMSG");
			log.trace("N810 TOPMSG >> {}", n810MsgCode);
			Map<String, Object> N813 = (Map<String, Object>) res813.getData();
			String n813MsgCode = (String) N813.get("TOPMSG");
			log.trace("N813 TOPMSG >> {}", n813MsgCode);
			bs.addData("n810MsgCode", n810MsgCode);
			bs.addData("n813MsgCode", n813MsgCode);

			// 如果 N810錯誤代碼及 N813錯誤代碼都等於"E091" 導向錯誤頁面
			if ("E091".equals(n810MsgCode) && "E091".equals(n813MsgCode)) {
				log.trace("Both E091");
				bs.setErrorMessage("E091", (String) N810.get("msgName"));
				bs.setResult(false);
				return bs;
			}

			// 如果 N810錯誤代碼不等於"" 及 "E091" 把錯誤訊息找出
			if (!"".equals(n810MsgCode) && !"E091".equals(n810MsgCode)) {
				log.trace("N810 ERROR BUT NOT E091");
				bs.addData("N810Message", (String) N810.get("msgName"));
			}

			// 如果 N813錯誤代碼不等於"" 及 "E091" 把錯誤訊息找出
			if (!"".equals(n813MsgCode) && !"E091".equals(n813MsgCode)) {
				log.trace("N813 ERROR BUT NOT E091");
				bs.addData("N813Message", (String) N813.get("msgName"));
			}
			
			List<Map<String, Object>> data = (List<Map<String, Object>>) N810.get("REC");
			List<Map<String, Object>> data2 = (List<Map<String, Object>>) N813.get("REC");
			
			if(null!=data2 && data2.size()!=0) {
				for (Map<String, Object> map : data2) {
					map.put("CARDNUM", WebUtil.hideCardNum((String) map.get("CARDNUM")));
				}
				
			}
			bs.addData("N810Record", data.size());
			bs.addData("N813Record", data2.size());
			bs.addData("N813REC", data2);
			bs.addData("CARDLIST", N813.get("CARDLIST"));
			bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
			bs.addData("TODAY", DateUtil.getCurentDateTime("yyyy/MM/dd"));
			bs.setResult(true);

		} catch (Exception e) {
			// TODO: handle exception
		}
		// 回傳處理結果
		return bs;
	}

	/**
	 * 信用卡未出帳單明細查詢_結果頁_REST路線
	 *
	 * @param sessionId
	 * @return
	 */
	public BaseResult unbilled_result_rest(Map<String, String> reqParam) {
		log.trace("creditcard_service_unbilled_result...");

		// 處理結果
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			if ("0".equals(reqParam.get("CARDTYPE1"))) {
				reqParam.put("CARDNUM", "");
			}
			// 電文TD01
			bs = TD01_REST(reqParam);
			log.trace("bs >>{}", bs.getData());
			Map<String, Object> callRow = (Map) bs.getData();
			callRow.put("CUSNAME", WebUtil.hideusername1Convert((String) callRow.get("CUSNAME")));
			ArrayList<Map<String, Object>> callTable = (ArrayList<Map<String, Object>>) callRow.get("REC");
			for (Map<String, Object> table : callTable) {
				ArrayList<Map<String, String>> callTableList = (ArrayList<Map<String, String>>) table.get("TABLE");
				for (Map<String, String> data : callTableList) {
					// 金額轉
					data.put("SRCAMNT", NumericUtil.formatNumberString((String) data.get("SRCAMNT"), 2));
					data.put("CURAMNT", NumericUtil.formatNumberString((String) data.get("CURAMNT"), 2));
					// //姓名轉
					data.put("CRDNAME", WebUtil.hideusername1Convert((String) data.get("CRDNAME")).trim());
					// //日期轉
					data.put("PURDATE", DateUtil.addSlash2((String) data.get("PURDATE")));
					data.put("POSTDATE", DateUtil.addSlash2((String) data.get("POSTDATE")));
				}
			}
			// for(List<Map> eachCard:td01rec) {
			// for(Map eachData:eachCard) {
			// //金額轉
			// eachData.put("SRCAMNT",NumericUtil.formatNumberString((String)eachData.get("SRCAMNT"),
			// 2));
			// eachData.put("CURAMNT",NumericUtil.formatNumberString((String)eachData.get("CURAMNT"),
			// 2));
			// //姓名轉
			// eachData.put("CRDNAME",
			// WebUtil.hideUsername((String)eachData.get("CRDNAME")).trim());
			// //日期轉
			// eachData.put("PURDATE", DateUtil.addSlash2((String)eachData.get("PURDATE")));
			// eachData.put("POSTDATE",
			// DateUtil.addSlash2((String)eachData.get("POSTDATE")));
			// }
			// }
			bs.setData(callRow);
			bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("unbilled_result_rest error >> {}",e);
		}
		// 回傳處理結果
		return bs;
	}

	/**
	 * 信用卡未出帳單明細查詢_EMAIL 路線
	 *
	 * @param
	 * @return
	 */
	public BaseResult get_Result_String(Map<String, String> reqParam) {
		log.trace("creditcard_service_unbilled_result_MAIL...");
		// 處理結果
		BaseResult bs = null;
		String htmlString = "";
		try {
			bs = new BaseResult();
			if ("0".equals(reqParam.get("CARDTYPE1"))) {
				reqParam.put("CARDNUM", "");
			}
			// 電文TD01
			bs = TD01_REST(reqParam);
			if (!bs.getResult()) {
				return bs;
			} else {

				log.trace("bs >>{}", bs.getData());
				Map<String, Object> callRow = (Map) bs.getData();
				callRow.put("CUSNAME", WebUtil.hideusername1Convert((String) callRow.get("CUSNAME")));
				ArrayList<Map<String, Object>> callTable = (ArrayList<Map<String, Object>>) callRow.get("REC");
				for (Map<String, Object> table : callTable) {
					ArrayList<Map<String, String>> callTableList = (ArrayList<Map<String, String>>) table.get("TABLE");
					for (Map<String, String> data : callTableList) {
						// 金額轉
						data.put("SRCAMNT", NumericUtil.formatNumberString((String) data.get("SRCAMNT"), 2));
						data.put("CURAMNT", NumericUtil.formatNumberString((String) data.get("CURAMNT"), 2));
						// //姓名轉
						data.put("CRDNAME", WebUtil.hideusername1Convert((String) data.get("CRDNAME")).trim());
						// //日期轉
						data.put("PURDATE", DateUtil.addSlash2((String) data.get("PURDATE")));
						data.put("POSTDATE", DateUtil.addSlash2((String) data.get("POSTDATE")));
					}
				}
				// 頭
//				String startString = "<table class=\"table\">\n" + "	<thead>\n" + "		<tr>\n"
//						+ "			<th>卡片種類</th>\n" + "			<th>消費日期 </th>\n" + "			<th>入帳日期</th>\n"
//						+ "			<th>交易說明 </th>\n" + "			<th>持卡人</th>\n" + "			<th>幣別</th >\n"
//						+ "			<th>外幣金額</th>\n" + "			<th>台幣金額</th>\n" + "		</tr>\n"
//						+ "	</thead>\n" + "	<tbody>\n";
				String startString = "<table class=\"table\">\n" + "	<thead>\n" + "		<tr>\n"
						+ "			<th>"+i18n.getMsg("LB.Card_kind")+"</th>\n" + "			<th>"+i18n.getMsg("LB.Consumption_date")+"</th>\n" + "			<th>"+i18n.getMsg("LB.The_account_credited_date")+"</th>\n"
						+ "			<th>"+i18n.getMsg("LB.Description_of_transaction")+" </th>\n" + "			<th>"+i18n.getMsg("LB.Card_holder")+"</th>\n" + "			<th>"+i18n.getMsg("LB.Currency")+"</th >\n"
						+ "			<th>"+i18n.getMsg("LB.Foreign_currency_amount")+"</th>\n" + "			<th>"+i18n.getMsg("LB.NTD_amount")+"</th>\n" + "		</tr>\n"
						+ "	</thead>\n" + "	<tbody>\n";

				// 尾
				String endString = "    </tbody>\n" + "</table><br>";
				// 中
				String middleString = "";

				// 單總和
				String TOTALEACHTABLE = "";

				// 總總和
				String TOTALTABLE = "";

				for (Map<String, Object> table : callTable) {
					ArrayList<Map<String, String>> callTableList = (ArrayList<Map<String, String>>) table.get("TABLE");
					for (Map<String, String> data : callTableList) {
						String rowString = "		<tr>\n" + "			<td>" + data.get("CARDTYPE") + "</td>\n"
								+ "			<td>" + data.get("PURDATE") + "</td>\n" + "			<td>"
								+ data.get("POSTDATE") + "</td>\n" + "			<td>" + data.get("DESCTXT") + "</td>\n"
								+ "			<td>" + data.get("CRDNAME") + "</td>\n" + "			<td>"
								+ data.get("CURRENCY") + "</td>\n" + "			<td>" + data.get("SRCAMNT") + "</td>\n"
								+ "			<td>" + data.get("CURAMNT") + "</td>\n" + "	   </tr>\n";
						middleString += rowString;
					}
					TOTALEACHTABLE = startString + middleString + endString;
					TOTALTABLE += TOTALEACHTABLE;
					TOTALEACHTABLE = "";
					middleString = "";
				}

				Map<String, Object> forEmail = new HashMap();
				forEmail.put("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				forEmail.put("CUSNAME", callRow.get("CUSNAME"));
				forEmail.put("TOTCNT", callRow.get("TOTCNT"));
				forEmail.put("CYCLE", callRow.get("CYCLE"));
				forEmail.put("TABLECONTENT", TOTALTABLE);
				forEmail.put("jspTitle", i18n.getMsg("LB.Credit_Card_Un-billed_Transactions"));//未出帳單明細查詢

				htmlString = HtmlToString("/htmlTemplate/unbilled_result.html", forEmail);
				bs.reset();
				bs.setResult(true);
				bs.addData("htmlString", htmlString);
			}

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("get_Result_String error >> {}",e);
		}
		// 回傳處理結果
		return bs;
	}

	/**
	 * 信用卡未出帳單明細查詢_結果頁_REST路線
	 *
	 * @param sessionId
	 * @return
	 */
	public BaseResult send_mail(Map<String, String> reqParam) {
		log.trace("creditcard_service_unbilled_result...");

		// 處理結果
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			if ("0".equals(reqParam.get("CARDTYPE1"))) {
				reqParam.put("CARDNUM", "");
			}
			// 電文TD01
			bs = TD01_MAIL_REST(reqParam);
		} catch (Exception e) {
			// TODO: handle exception
		}
		// 回傳處理結果
		return bs;
	}

	/**
	 * 根據模板靜態頁面ToString
	 * 
	 * @param filePath
	 *            路徑/模板名稱 <br>
	 *            模板中要替換的key要用"##" + key + "##"
	 * 
	 * @param valuesMap傳入要替換的map
	 * @return
	 */
	public String HtmlToString(String filePath, Map<String, Object> valuesMap) {
		String str = "";
		InputStream input = null;
		InputStreamReader inputReader = null;
		try {
			log.debug("HtmlToString filePath >>>>>>>>{}", filePath);
			log.debug("HtmlToString input >>>>>>>>{}", valuesMap);
			String tempStr = "";
			Resource resource = new ClassPathResource(filePath);
//			File file = resource.getFile();
//			input = new FileInputStream(file);
			input = resource.getInputStream();
			inputReader = new InputStreamReader(input, "UTF-8");
			BufferedReader br = new BufferedReader(inputReader);
			while ((tempStr = br.readLine()) != null) {
				str = str + tempStr;
			}

			// 傳入要替換的map
			log.debug("valuesMap >>>>>>>> {}", valuesMap);
			// 替換掉模塊中的地方
			for (String key : valuesMap.keySet()) {
				if (valuesMap.get(key) != null) {
					str = str.replace("##" + key + "##", String.valueOf(valuesMap.get(key)));
				} else {
					str = str.replace(key, "");
				}
			}
			input.close();
			inputReader.close();
		} catch (IOException e) {
			log.error("JspToHtmlFile :", e);
			return str;
		} finally {
			// ** 無論如何都必須釋放連接.
			try {
				if (input != null) {
					input.close();
				}
				if (inputReader != null) {
					inputReader.close();
				}
			} catch (IOException e) {
				log.error("JspToHtmlFile finally:" + e, e);
			}
		}

		log.debug(ESAPIUtil.vaildLog("HtmlToString >> " + str)); 
		
		return str;
	}

	// /**
	// * 信用卡未出帳單明細查詢_結果頁_資料筆數
	// *
	// * @param sessionId
	// * @return
	// */
	// public int getTotcnt(BaseResult bs) {
	// Map<String, Object> mapTD01 = null;
	// int totcnt = 0;
	//
	// try {
	// // TD01電文資料
	// mapTD01 = (Map<String, Object>) bs.getData();
	// log.trace("rtnMap_n810: " + mapTD01);
	// log.trace("rtnMap_n810_Table: " + mapTD01.get("_Table"));
	// // _Table資料
	// Gson gs = new Gson();
	// List<Map<String, Object>> tabletoList =
	// gs.fromJson(gs.toJson(mapTD01.get("_Table")), ArrayList.class);
	// log.trace("tabletoList: " + tabletoList);
	//
	// // _Table取得資料筆數
	// totcnt = tabletoList.size();
	//
	// } catch (Exception e) {
	// e.printStackTrace();
	// log.error("", e);
	// }
	//
	// return totcnt;
	// }

	public String coverName(String name) {
		String coverName = "";
		if (name.length() >= 3) {
			coverName = name.substring(0, 1) + coverCount(name.length() - 2) + name.substring(name.length() - 1);
		}
		if (name.length() < 3) {
			coverName = name.substring(0, 1) + coverCount(name.length() - 1);
		}
		return coverName;
	}

	public String coverCount(int digits) {
		String cover = "";
		for (int i = 0; i < digits; i++) {
			cover += "０";
		}
		return cover;
	}

	public BaseResult card_paid_history_p1_info(Map<String, String> reqParam) {
		// 取得n810
		log.trace("card_paid_history_p1_info...");
		log.trace(ESAPIUtil.vaildLog("custid >>{} " + reqParam.get("CUSIDN")));
		// 處理結果
		BaseResult bs = null;
		BaseResult res810 = null;
		BaseResult res813 = null;
		try {

			bs = new BaseResult();
			res810 = new BaseResult();
			res813 = new BaseResult();
			// 電文N810
			//Avoid TXNLOG
			reqParam.put("ADOPID", "__N810");
			res810 = N810_REST(reqParam);
			// 電文N813
			res813 = N813_REST(reqParam);

			Map<String, Object> N810 = (Map<String, Object>) res810.getData();
			String n810MsgCode = (String) N810.get("TOPMSG");
			log.trace("N810 TOPMSG >> {}", n810MsgCode);
			Map<String, Object> N813 = (Map<String, Object>) res813.getData();
			String n813MsgCode = (String) N813.get("TOPMSG");
			log.trace("N813 TOPMSG >> {}", n813MsgCode);
			bs.addData("n810MsgCode", n810MsgCode);
			bs.addData("n813MsgCode", n813MsgCode);

			// 如果 N810錯誤代碼及 N813錯誤代碼都等於"E091" 導向錯誤頁面
			if ("E091".equals(n810MsgCode) && "E091".equals(n813MsgCode)) {
				log.trace("Both E091");
				bs.setErrorMessage("E091", (String) N810.get("msgName"));
				bs.setResult(false);
				return bs;
			}

			// 如果 N810錯誤代碼不等於"" 及 "E091" 把錯誤訊息找出
			if (!"".equals(n810MsgCode) && !"E091".equals(n810MsgCode)) {
				log.trace("N810 ERROR BUT NOT E091");
				bs.addData("N810Message", (String) N810.get("msgName"));
			}

			// 如果 N813錯誤代碼不等於"" 及 "E091" 把錯誤訊息找出
			if (!"".equals(n813MsgCode) && !"E091".equals(n813MsgCode)) {
				log.trace("N813 ERROR BUT NOT E091");
				bs.addData("N813Message", (String) N813.get("msgName"));
			}

			List<Map<String, Object>> data = (List<Map<String, Object>>) N810.get("REC");
			List<Map<String, Object>> data2 = (List<Map<String, Object>>) N813.get("REC");
			
			if(null!=data2 && data2.size()!=0) {
				for (Map<String, Object> map : data2) {
					map.put("CARDNUM", WebUtil.hideCardNum((String) map.get("CARDNUM")));
				}
			}
			
			bs.addData("N810Record", data.size());
			bs.addData("N813Record", data2.size());
			bs.addData("N813REC", data2);
			bs.addData("CARDLIST", N813.get("CARDLIST"));
			bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
			bs.addData("TODAY", DateUtil.getCurentDateTime("yyyy/MM/dd"));
			bs.setResult(true);

		} catch (Exception e) {
			// TODO: handle exception
		}
		// 回傳處理結果
		return bs;
	}

	public BaseResult card_paid_history_result(Map<String, String> reqParam) {
		log.trace("card_paid_history_result...");
		log.trace(ESAPIUtil.vaildLog("custid >>{} "+ reqParam.get("CUSIDN")));
		// 處理結果
		BaseResult bs = null;
		try {

			bs = new BaseResult();
			bs = TD02_REST(reqParam);
			bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));

		} catch (Exception e) {
			// TODO: handle exception
		}
		// 回傳處理結果
		return bs;
	}

}
