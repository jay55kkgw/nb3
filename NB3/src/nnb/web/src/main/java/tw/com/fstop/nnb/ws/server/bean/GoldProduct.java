package tw.com.fstop.nnb.ws.server.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

//@XmlRootElement(namespace = "http://ws.fstop", name="ArrayOfGoldProduct")
//@XmlRootElement(namespace = "http://ws.fstop", name="GoldProduct")
//@XmlRootElement(namespace = "http://ws.fstop")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GoldProduct {
//	@XmlElement(namespace = "http://ws.fstop")
	@XmlElement
	public String currec;
//	@XmlElement(namespace = "http://ws.fstop")
	public String totrec;
//	@XmlElement(namespace = "http://ws.fstop")
	public String goodno;
//	@XmlElement(namespace = "http://ws.fstop")
	public String sell;
//	@XmlElement(namespace = "http://ws.fstop")
	public String sellt;
//	@XmlElement(namespace = "http://ws.fstop")
	public String buy;
//	@XmlElement(namespace = "http://ws.fstop")
	public String dpdiff;

	public String getCurrec() {
		return currec;
	}

	public void setCurrec(String currec) {
		this.currec = currec;
	}

	public String getTotrec() {
		return totrec;
	}

	public void setTotrec(String totrec) {
		this.totrec = totrec;
	}

	public String getGoodno() {
		return goodno;
	}

	public void setGoodno(String goodno) {
		this.goodno = goodno;
	}

	public String getSell() {
		return sell;
	}

	public void setSell(String sell) {
		this.sell = sell;
	}

	public String getSellt() {
		return sellt;
	}

	public void setSellt(String sellt) {
		this.sellt = sellt;
	}

	public String getBuy() {
		return buy;
	}

	public void setBuy(String buy) {
		this.buy = buy;
	}

	public String getDpdiff() {
		return dpdiff;
	}

	public void setDpdiff(String dpdiff) {
		this.dpdiff = dpdiff;
	}
}
