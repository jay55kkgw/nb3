package tw.com.fstop.nnb.spring.controller;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import fstop.orm.po.ADMCOUNTRY;
import fstop.orm.po.ADMMSGCODE;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.Acct_Service;
import tw.com.fstop.tbb.nnb.dao.AdmCountryDao;
import tw.com.fstop.tbb.nnb.dao.AdmMsgCodeDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.DownloadUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.ObjectConvertUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;
import java.util.Locale;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.ClassPathResource;

@SessionAttributes({ SessionUtil.PD, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, SessionUtil.PRINT_DATALISTMAP_DATA,
		SessionUtil.CONFIRM_LOCALE_DATA, SessionUtil.RESULT_LOCALE_DATA, SessionUtil.CUSIDN,
		SessionUtil.TRANSFER_CONFIRM_TOKEN, SessionUtil.TRANSFER_RESULT_TOKEN, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN,
		SessionUtil.TRANSFER_DATA })
@Controller
@RequestMapping(value = "/CUSTOMER/SERVICE")
public class Customer_Service
{
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private AdmMsgCodeDao admmsgcode;
	
	@Autowired
   	ServletContext servletContext;

	/**
	 * 常見問題
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/common_problem")
	public String common_problem(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		//pretendXSS
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		okMap.put("tag", ESAPIUtil.pretendNameColumnFromXSS(okMap.get("tag")));
		//log.trace("XSSTag >>"+okMap.get("tag"));
		String target = "/error";
		BaseResult bsdata = null;
		bsdata = new BaseResult();
		bsdata.addAllData(okMap);
		log.debug("bsdata>>>>>"+bsdata.getData());
	
		//bs = get_error_code();
		Locale currentLocale = LocaleContextHolder.getLocale();
		List<Map<String, String>> bs = get_error_code(currentLocale.toString());
		log.trace("currentLocale>>>>>{}",currentLocale);
		log.trace("common_problem");
		Map getCODE = new HashMap();
		model.addAttribute("error_code",bs);
		if(currentLocale.toString().equals("en")) {
			log.trace("environment_setting>>>>>>en");
			target = "/customer_service/common_problem_EN";
		}
		else if(currentLocale.toString().equals("zh_CN")) {
			log.trace("environment_setting>>>>>>CN");
			target = "/customer_service/common_problem_CN";
		}
		else {
			log.trace("environment_setting>>>>>>TW");
			//model.addAttribute("error_code",bs);
			target = "/customer_service/common_problem";
		}
		model.addAttribute("common_problem", bsdata);
		return target;
	}
	
	/**
	 * 環境設定
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/environment_setting")
	public String environment_setting(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/error";
		BaseResult bs = null;
		Locale currentLocale = LocaleContextHolder.getLocale();
		log.trace("currentLocale>>>>>{}",currentLocale);
		log.trace("environment_setting");
		if(currentLocale.toString().equals("en")) {
			log.trace("environment_setting>>>>>>en");
			target = "/customer_service/environment_setting_EN";
		}
		else if(currentLocale.toString().equals("zh_CN")) {
			log.trace("environment_setting>>>>>>CN");
			target = "/customer_service/environment_setting_CN";
		}
		else {
			log.trace("environment_setting>>>>>>TW");
			target = "/customer_service/environment_setting";
		}
		return target;
	}
	
	/**
	 * 網路安全
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/network_security")
	public String network_security(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/error";
		BaseResult bs = null;
		log.trace("network_security");
		target = "/customer_service/network_security";		
		return target;
	}
	
	/**
	 * 新手上路
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/novice")
	public String novice(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/error";
		BaseResult bs = null;
		log.trace("novice");
		Locale currentLocale = LocaleContextHolder.getLocale();
		log.trace("currentLocale>>>>>{}",currentLocale);
		if(currentLocale.toString().equals("en")) {
			log.trace("novice>>>>>>en");
			target = "/customer_service/novice_en";
		}
		else if(currentLocale.toString().equals("zh_CN")) {
			log.trace("novice>>>>>>CN");
			target = "/customer_service/novice_cn";
		}
		else {
			log.trace("novice>>>>>>TW");
			target = "/customer_service/novice";
		}
		return target;
	}
	
	/**
	 * 網站導覽
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/site_guide")
	public String site_guide(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/error";
		BaseResult bs = null;
		log.trace("site_guide");
		target = "/customer_service/site_guide";		
		return target;
	}
	
	/**
	 * 網站導覽
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/site_login")
	public String site_login(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/error";
		BaseResult bs = null;
		log.trace("site_login");
		target = "/customer_service/site_login";		
		return target;
	}
	
	/**
	 * 取得錯誤訊息
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	
	public List<Map<String, String>> get_error_code(String locale) {
		log.trace("get_error_code...>>>");
		
		List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
		
		BaseResult bs = new BaseResult();
		try {
			List<ADMMSGCODE> ADMMSGCODE = admmsgcode.getMsg();
			//log.trace("ADMMSGCODE {}",ADMMSGCODE);
			if (ADMMSGCODE != null) {
				for (ADMMSGCODE each : ADMMSGCODE) {
					// 將po轉成map
					Map<String, Object> eachMap = ObjectConvertUtil.object2Map(each);
					Map<String, String> result = new HashMap();
					//String admcode = (String)eachMap.get("ADMCODE");
					//String admsign = (String)eachMap.get("ADMSGIN");
					String admsgout = each.getADMSGOUT();
					log.trace(ESAPIUtil.vaildLog("ADMSGIN "+admsgout));
					String admcode = each.getADMCODE();
					String admsgoutchs = each.getADMSGOUTCHS();
					String admsgouteng = each.getADMSGOUTENG();
					log.trace(ESAPIUtil.vaildLog("ADMCODE "+admcode));
				
					result.put("ADMCODE", admcode);
					if(locale.equals("en")) {
						result.put("ADMSGIN", admsgouteng);
					}
					else if(locale.equals("zh_CN")) {
						result.put("ADMSGIN", admsgoutchs);
					}
					else {
						result.put("ADMSGIN", admsgout);
					}
					resultList.add(result);
					
				}
			}
			log.trace(ESAPIUtil.vaildLog("resultList>>>"+resultList));
		} 
		catch (Exception e) {
			log.error("get_error_code.error: {}", e);
		}
		
		return resultList;
	}
	
	/**
	 * 費率表
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/rate_table")
	public String rate_table(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/error";
		log.trace("rate_table");
		target = "/customer_service/rate_table";		
		return target;
	}
	
	/**
	 * 最低申購投資金額
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/purchase_table")
	public String purchase_table(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/error";
		log.trace("purchase_table");
		target = "/customer_service/purchase_table";		
		return target;
	}
	
	/**
	 * 部分贖回及帳面剩餘金額之限制
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/redeem_table")
	public String redeem_table(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/error";
		log.trace("redeem_table");
		target = "/customer_service/redeem_table";		
		return target;
	}
	
	
	/**
	 * 基金理財網
	 * TYPE
	 * 空(預設) = 基金理財網
	 * D = 國內淨值 (登入頁 > 金融資訊 > 基金淨值 > 國內淨值)
	 * F = 國外淨值 (登入頁 > 金融資訊 > 基金淨值 > 國外淨值)
	 * ( 參考 基金餘額/損益查詢 (fund_profitloss_data.jsp) 之下拉選單類別 , 以下類別會連到基金理財網)
	 * 1,2,7,8,9 = TRANSCODE
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/FundWeb")
	public String fund_web(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/error";
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			String type =  StrUtil.isEmpty(okMap.get("TYPE"))?"":okMap.get("TYPE");
			String transcode = "";
			model.addAttribute("TYPE", type);
			switch (type) {
			case "1":
			case "2":
			case "7":
			case "8":
			case "9":
				transcode =  StrUtil.isEmpty(okMap.get("TRANSCODE"))?"":okMap.get("TRANSCODE");
				model.addAttribute("TRANSCODE", transcode);
				break;
			default:
				break;
			}
			target = "/customer_service/redirect";		
		}catch (Exception e) {
			log.error("type error",e);
		}
		
		return target;
	}
	
	/**
	 * PDF_下載
	 * 
	 * @return
	 */
	@RequestMapping(value = "/pdf_download", produces = { "text/plain;charset=UTF-8" })
	public void componentDownload(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model, SessionStatus status) {
		log.trace("pdfDownload...");
		try {
			String relativePath = "";
			String pdftype = reqParam.get("pdf");
			//修改 HTTP Response Splitting
			if(pdftype.equals("login")) {
				relativePath = "/term/NBLoginOperateBook.pdf";
			}else if(pdftype.equals("Myhome")) {
				relativePath = "/term/NBMyhomeOperateBook.pdf";
			}else if(pdftype.equals("AccountOverview")) {
				relativePath = "/term/NBAccountOverviewOperateBook.pdf";
			}else if(pdftype.equals("NTDService")) {
				relativePath = "/term/NBNTDServiceOperateBook.pdf";
			}else if(pdftype.equals("ForeignCurrency")) {
				relativePath = "/term/NBForeignCurrencyOperateBook.pdf";
			}else if(pdftype.equals("PaymentofTaxes")) {
				relativePath = "/term/NBPaymentofTaxesOperateBook.pdf";
			}else if(pdftype.equals("LoanService")) {
				relativePath = "/term/NBLoanServiceOperateBook.pdf";
			}else if(pdftype.equals("FundsBond")) {
				relativePath = "/term/NBFundsBondOperateBook.pdf";
			}else if(pdftype.equals("GoldPassbook")) {
				relativePath = "/term/NBGoldPassbookOperateBook.pdf";
			}else if(pdftype.equals("CreditCard")) {
				relativePath = "/term/NBCreditCardOperateBook.pdf";
			}else if(pdftype.equals("OnlineServices")) {
				relativePath = "/term/NBOnlineServicesOperateBook.pdf";
			}else if(pdftype.equals("PersonalServices")) {
				relativePath = "/term/NBPersonalServicesOperateBook.pdf";
			}else if(pdftype.equals("Register")) {
				relativePath = "/term/NBRegisterOperateBook.pdf";
			}else if(pdftype.equals("Attachment")) {
				relativePath = "/term/NBAttachmentOperateBook.pdf";
			}
			log.debug("relativePath=" + relativePath);
			
			org.springframework.core.io.Resource resource = new ClassPathResource(relativePath);
			if(resource.exists()){
				InputStream fileInputStream = resource.getInputStream();

				String mimeType = servletContext.getMimeType(relativePath);
				if (mimeType == null) {
					mimeType = "application/octet-stream";
				}
				log.debug("mimeType=" + mimeType);

				response.setContentType(mimeType);
				response.setContentLength((int) resource.contentLength());

				String headerKey = "Content-Disposition";
				String headerValue = String.format("attachment; filename=\"%s\"", resource.getFilename());
				response.setHeader(headerKey, headerValue);
				OutputStream outStream = response.getOutputStream();

				byte[] buffer = new byte[4096];
				int bytesRead = -1;

				while ((bytesRead = fileInputStream.read(buffer)) != -1) {
					outStream.write(buffer, 0, bytesRead);
				}
				fileInputStream.close();
				outStream.close();
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("pdfDownload error >> {}",e);
		}
		
		
		return;
	}
}
