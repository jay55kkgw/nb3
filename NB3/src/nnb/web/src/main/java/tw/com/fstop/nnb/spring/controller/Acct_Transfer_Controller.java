package tw.com.fstop.nnb.spring.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.common.ResultCode;
import tw.com.fstop.idgate.bean.N070_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N070_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N830_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N830_IDGATE_DATA_VIEW;
import tw.com.fstop.nnb.aop.Dialog;
import tw.com.fstop.nnb.custom.annotation.ACNSET;
import tw.com.fstop.nnb.custom.annotation.ACNVERIFY;
import tw.com.fstop.nnb.custom.annotation.ACNVERIFY0;
import tw.com.fstop.nnb.service.Acct_Transfer_Service;
import tw.com.fstop.nnb.service.DaoService;
import tw.com.fstop.nnb.service.IdGateService;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

@Controller
@RequestMapping(value = "/NT/ACCT/TRANSFER")
@SessionAttributes({ SessionUtil.PD, SessionUtil.CUSIDN, SessionUtil.DPMYEMAIL, SessionUtil.TRANSFER_DATA,
		SessionUtil.TRANSFER_CONFIRM_TOKEN, SessionUtil.TRANSFER_RESULT_TOKEN, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN,
		SessionUtil.BACK_DATA, SessionUtil.PRINT_DATALISTMAP_DATA, SessionUtil.CONFIRM_LOCALE_DATA,
		SessionUtil.RESULT_LOCALE_DATA,SessionUtil.AUTHORITY,SessionUtil.IDGATE_TRANSDATA })
public class Acct_Transfer_Controller {

	Logger log = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private Acct_Transfer_Service acct_transfer_service;

	@Autowired
	private DaoService daoservice;
	
	@Autowired
	IdGateService idgateservice;
	
	@Autowired
	String isSessionID;

	/**
	 * 通訊錄email選單進入點
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/showAddressbook")
	public String showAddressbook(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("showAddressbook");
		BaseResult bs = new BaseResult();
		try {
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			bs = acct_transfer_service.getAddressbook(cusidn);

			if (bs.getResult()) {
				model.addAttribute("Addressbook", bs);
			}
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("showAddressbook error >> {}", e);
			bs.setMessage(ResultCode.SYS_ERROR);
		} finally {
			// 無論是否有資料都顯示頁面
			target = "/acct/addressbookMenu";
		}
		return target;
	}

	/**
	 * 查詢轉入帳號選單進入點
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/searchDagacno")
	public String searchDagacno(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("searchDagacno");
		BaseResult bs = new BaseResult();
		try {
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			bs = acct_transfer_service.getSearchDagacno(cusidn);

			if (bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				List<Map<String, String>> rows = (List<Map<String, String>>) bsData.get("REC");
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, rows);
				model.addAttribute("searchDagacno", bs);
			}
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("searchDagacno error >> {}", e);
			bs.setMessage(ResultCode.SYS_ERROR);
		} finally {
			// 無論是否有資料都顯示頁面
			target = "/acct/search_dagacno";
		}
		return target;
	}

	/**
	 * 取得銀行代號名稱
	 * 
	 * @param request
	 * @param response
	 * @param reqParama
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getDpBHNO_aj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult getDpBHNO(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String str = "getInAcno_aj";
		log.trace(ESAPIUtil.vaildLog("hi >> " + str));
		log.trace(ESAPIUtil.vaildLog("reqParam >> " + CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			bs = daoservice.getDpBHNO_List(cusidn);
		} catch (UnsupportedEncodingException e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("getDpBHNO error >> {}", e);
		}

		return bs;

	}

	/**
	 * 取得轉帳之轉入帳號
	 * 
	 * @param request
	 * @param response
	 * @param reqParama
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getInAcno_aj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult getInACNO(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String str = "getInAcno_aj";
		log.trace(ESAPIUtil.vaildLog("hi >> " + str));
		log.trace(ESAPIUtil.vaildLog("reqParam >> " + CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			String type = reqParam.get("type");
			bs = acct_transfer_service.getAcnoList(cusidn, type);
		} catch (UnsupportedEncodingException e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("getInACNO error >> {}", e);
		}

		return bs;

	}

	/**
	 * 取得轉帳之轉出帳號
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@ACNSET
	@RequestMapping(value = "/getOutAcno_aj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult getOutACNO(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String str = "getOutAcno_aj";
		log.trace(ESAPIUtil.vaildLog("hi >> " + str));
		log.trace(ESAPIUtil.vaildLog("reqParam >> " + CodeUtil.toJson(reqParam)));
		// TODO 要取得使用者相關帳號
		BaseResult bs = new BaseResult();
		try {
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			bs = acct_transfer_service.getOutACNO_List(cusidn);
		} catch (UnsupportedEncodingException e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("getOutACNO error >> {}", e);
		}

		return bs;

	}

	/**
	 * 要取得轉出帳號相關資料
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getACNO_Data_aj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult getACNO_Data(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String str = "getACNO_Data";
		log.trace(ESAPIUtil.vaildLog("hi >> " + str));
		log.trace(ESAPIUtil.vaildLog("reqParam >> " + CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			String acno = reqParam.get("acno");
			bs = acct_transfer_service.findOenByN110(cusidn, acno);
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("getACNO_Data error >> {}", e);
		}

		return bs;

	}

	/**
	 * 轉帳輸入頁
	 * 
	 * @return
	 */
	@RequestMapping(value = "/transfer")
	public String transfer(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("transfer ");
		BaseResult bs = new BaseResult();
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		// ctr_path
		if ("Y".equals(okMap.get("back"))) {
			model.addAttribute("bk_key", "Y");
		}

		// 清除切換語系時暫存的資料
		SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");

		// 設定頁面預約日期為明天，如果當前時間太晚則預約後天
		model.addAttribute("tmr", DateUtil.getTomorrowOrAfterTomorrowDate());
		String sMinDate = "";
		String sMaxDate = "";
		String today = "";
		try {
			String getAcn = okMap.get("Acn");
			log.trace(ESAPIUtil.vaildLog("GET ACN >>> " + getAcn));
			
			bs = acct_transfer_service.getTxToken();
			today = DateUtil.getCurentDateTime("yyyy/MM/dd");
			sMinDate = DateUtil.getNextDate("yyyy/MM/dd");
			sMaxDate = DateUtil.getNextTwoYearsDay();
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			//黑名單特殊註記
			String RISKFLAG=daoservice.getRiskFlag(cusidn);
			bs.addData("RISKFLAG",RISKFLAG);		
			//IDGATE身分
			String idgateUserFlag="N";
			try {
				BaseResult tmp=idgateservice.checkIdentity(cusidn);
				if(tmp.getResult()) {
					idgateUserFlag="Y";					
				}
				tmp.addData("idgateUserFlag",idgateUserFlag);
				SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA,((Map<String, String>) tmp.getData()));
			}catch(Exception e) {
				log.debug("idgateUserFlag error {}",e);
			}			
			//第三階段說明
			String isProd = SpringBeanFactory.getBean("isProd");
			bs.addData("isProd",isProd);			
			bs.setResult(true);
			log.trace("BS DATA >> "+CodeUtil.toJson(bs.getData()));
			if (bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN,
						((Map<String, String>) bs.getData()).get("TXTOKEN"));
				SessionUtil.addAttribute(model, "today", today);
				SessionUtil.addAttribute(model, "sMinDate", sMinDate);
				SessionUtil.addAttribute(model, "sMaxDate", sMaxDate);
				SessionUtil.addAttribute(model, "Acn", getAcn);
				model.addAttribute("RISKFLAG",((Map<String, String>) bs.getData()).get("RISKFLAG"));
				model.addAttribute("idgateUserFlag",idgateUserFlag);
				model.addAttribute("isProd",isProd);
			}
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("transfer error >> {}", e);
			bs.setSYSMessage(ResultCode.SYS_ERROR);
		} finally {
			if (bs.getResult()) {
				target = "/acct/transfer";
			} else {
				bs.setNext("/NT/ACCT/TRANSFER/transfer");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		return target;

	}

	/**
	 * 轉帳確認頁
	 * 
	 * @return
	 */
	@ACNVERIFY(acn="ACNO")
	@ACNVERIFY0(acn="OUTACN_NPD")
	@RequestMapping(value = "/transfer_confirm")
	public String transfer_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("transfer_confirm>>");
		String target = "/error";
		String next = "/NT/ACCT/TRANSFER/transfer";
		String jsondc = "";
		BaseResult bs = new BaseResult();
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

		try {
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam), "UTF-8");
			log.trace(ESAPIUtil.vaildLog("electricity_fee_confirm.jsondc >> " + jsondc));

			okMap.put("jsondc", jsondc);
			log.trace(ESAPIUtil.vaildLog("electricity_fee_confirm.okMap >> " + CodeUtil.toJson(okMap)));

			// 回上一頁重新賦值
			if (okMap.containsKey("previous")) {
				Map m = (Map) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				okMap = m;
			}

			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if (!bs.getResult()) {
					// TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				} else {
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			}

			if (!hasLocale) {
//				TODO 壓力測試後 移除
				if (!"T".equals(isSessionID)) {
					if (StrUtil.isNotEmpty(okMap.get("TOKEN")) && okMap.get("TOKEN")
							.equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null))) {
						bs.setResult(Boolean.TRUE);
					}
					log.trace("bs.getResult() >>{}", bs.getResult());
					if (!bs.getResult()) {
						log.trace("throw new Exception()");
						throw new Exception();
					}
				} // isSessionID END
				bs.reset();
				String cusidn = new String(
						Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
						"utf-8");
				bs = acct_transfer_service.transfer_confirm(okMap, cusidn);
				log.trace("IDGATE DATA >>"+CodeUtil.toJson(bs.getData()));
				// IDGATE transdata
	    		String adopid = "N070";
	    		String title = "您有一筆臺幣轉帳交易待確認，金額"+((Map<String, String>) bs.getData()).get("AMOUNT");				
				Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
				Map<String, String> result = null;
				model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
				if(IdgateData.get("idgateUserFlag").equals("Y")) {
					//TODO 用bean方法改寫
					result=(Map<String, String>) bs.getData();
	            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
	            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
	            	IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N070_IDGATE_DATA.class, N070_IDGATE_DATA_VIEW.class, result));
	            	acct_transfer_service.customIdgateData(IdgateData,adopid,result);
	            	SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);
//					IdgateData=acct_transfer_service.createIdgateData(bs,IdgateData);
//					Map<String,Object>tmp=new HashMap();
//					tmp.put("N070_IDGATE",IdgateData);
//					SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA,tmp);
					log.trace("N070_IDGATE >>"+CodeUtil.toJson(IdgateData));
				}
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				
			}

		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("transfer_confirm error >> {}", e);
		} finally {
			if (bs.getResult()) {
				target = "/acct/transfer_confirm";
				// TODO 這邊還要調整 不能存物件，要存成JSNO字串
				model.addAttribute(SessionUtil.TRANSFER_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN,
						((Map<String, Object>) bs.getData()).get("TXTOKEN"));

				/*
				 * 20190522 hugo 移到service //約定非約定 String fgsvacno = okMap.get("FGSVACNO");
				 * log.trace("fgsvacno>>{}",fgsvacno); String transferInAccount = "";
				 * if("1".equals(fgsvacno)) { //約定轉入帳號 String DPAGACNO = okMap.get("DPAGACNO");
				 * Map<String,String> DPAGACNOMap = new Gson().fromJson(DPAGACNO,Map.class);
				 * log.trace("DPAGACNO={}",DPAGACNO); transferInAccount =
				 * DPAGACNOMap.get("ACN"); }else { String DPBHNO =
				 * okMap.get("DPBHNO").split("-")[0]; okMap.put("DPBHNO", DPBHNO);
				 * log.trace("dpbhno>>{}",DPBHNO); //非約定轉入帳號 String DPACNO =
				 * okMap.get("DPACNO"); log.trace("DPACNO={}", DPACNO); transferInAccount =
				 * okMap.get("DPACNO"); } //將轉入帳號存起來
				 * 
				 * log.trace("transferInAccount={}",transferInAccount);
				 * model.addAttribute("transferInAccount",transferInAccount);
				 * 
				 */
				// 設定回上一頁的url,配合confirm頁面的js
				model.addAttribute("previous", "/NT/ACCT/TRANSFER/transfer");
				// 回填使用DATA
				log.debug(ESAPIUtil.vaildLog("okMap.AMOUNT >> " + okMap.get("AMOUNT")));
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
				// target = "redirect:/transfer";
			} else {
				bs.setNext(next);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;

	}

	/**
	 * 轉帳結果頁
	 * 
	 * @return
	 */
	@RequestMapping(value = "/transfer_result")
	@Dialog(ADOPID = "N070")
	public String transfer_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("transfer_result>>");
		String target = "/error";
		String next = "/NT/ACCT/TRANSFER/transfer";
		String previous = "/NT/ACCT/TRANSFER/transfer_confirm";
		BaseResult bs = new BaseResult();
		// TODO 驗證 result token 如果存在 且相同表示F5 或i18n
		// TODO 要CALL電文進行轉帳 注意要分預約非預約 約轉非約轉
		String jsondc = "";
		String pkcs7Sign = "";
		try {
			log.debug(ESAPIUtil.vaildLog("transfer_result.reqParam >> " + CodeUtil.toJson(reqParam)));
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			// TODO 1.要先驗證TOKEN的正確性
			jsondc = reqParam.get("jsondc");
			pkcs7Sign = reqParam.get("pkcs7Sign");
			reqParam.remove("jsondc");
			reqParam.remove("pkcs7Sign");

			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			okMap.put("jsondc", jsondc);
			okMap.put("pkcs7Sign", pkcs7Sign);
			log.debug(ESAPIUtil.vaildLog("transfer_result.okMap >> " + CodeUtil.toJson(okMap)));
			// Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}

			if (!hasLocale) {
//				TODO 壓力測試後 移除
				if (!"T".equals(isSessionID)) {
					if (StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && okMap.get("TXTOKEN")
							.equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null))) {
						bs.setResult(Boolean.TRUE);
					}

					if (!bs.getResult()) {
						log.error(ESAPIUtil.vaildLog("transfer_result TXTOKEN 不正確，TXTOKEN >> " + okMap.get("TXTOKEN")));
						throw new Exception();
					}
					bs.reset();

					// 2.要驗證token是否與完成交易的TOKEN重複
					if (StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && !okMap.get("TXTOKEN")
							.equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null))) {
						bs.setResult(Boolean.TRUE);
					}
					if (!bs.getResult()) {
						log.error(ESAPIUtil
								.vaildLog("transfer_result TXTOKEN 不正確，或重複交易，TXTOKEN >> " + okMap.get("TXTOKEN")));
						throw new Exception();
					}
				} // isSessionID END
				bs.reset();
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String, String> transfer_data = (Map<String, String>) bs.getData();

				okMap.putAll(transfer_data);
				//----防止密碼錯誤欄位被覆蓋---start
				okMap.put("jsondc",jsondc);
				okMap.put("pkcs7Sign",pkcs7Sign);
				okMap.put("PINNEW",reqParam.get("PINNEW"));
				okMap.put("ISSUER",reqParam.get("ISSUER"));
				okMap.put("ACNNO",reqParam.get("ACNNO"));
				okMap.put("TRMID",reqParam.get("TRMID"));
				okMap.put("iSeqNo",reqParam.get("iSeqNo"));
				okMap.put("ICSEQ",reqParam.get("ICSEQ"));
				okMap.put("TAC",reqParam.get("TAC"));
				okMap.put("FGTXWAY",reqParam.get("FGTXWAY"));
				//----防止密碼錯誤欄位被覆蓋---end
				bs.reset();
				//IDgate驗證
				try {
					if(okMap.get("FGTXWAY").equals("7")) {
						Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
						IdgateData=(Map<String, Object>) IdgateData.get("N070_IDGATE");
						okMap.put("sessionID",(String) IdgateData.get("sessionid"));
						okMap.put("txnID",(String) IdgateData.get("txnID"));
						okMap.put("idgateID",(String) IdgateData.get("IDGATEID"));
					}
				}catch(Exception e) {
					log.error("IDGATE ValidateFail>>{}",bs.getResult());
				}				
				Map<String,Object>IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
				model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
				bs = acct_transfer_service.transfer_result(okMap, cusidn);

				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			model.addAttribute("transfer_result_data", bs);

			// E004 是密碼錯誤 要讓使用者可以回上一步
			if ("E004".equals(bs.getMsgCode())) {
				bs.setPrevious(previous);
				model.addAttribute(BaseResult.ERROR, bs);
				// 移除舊的錯誤密碼
				okMap.remove("PINNEW");
				// 回填使用DATA
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
			}
			

		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("transfer_result error >> {}", e);
		} finally {
			// TODO 要清除必要的SESSION

			// 解決Trust Boundary Violation
			log.debug(ESAPIUtil.vaildLog("finally.reqParam >> " + CodeUtil.toJson(reqParam)));
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

			if (bs.getResult()) {
				// 交易成功要存之前的token 在Session 以利下次比對是否重複交易
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TXTOKEN"));
				target = "/acct/transfer_result";
			} else {
				bs.setNext(next);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;

	}
	@RequestMapping(value = "/getA106_Data_aj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult getA106_Data(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = new BaseResult();
		try {
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			Map<String,String> req = new HashMap<String, String>();
			req.put("FUN_TYPE","R");//R:查詢
			bs = acct_transfer_service.A106_REST(cusidn, req);
			log.trace("A106Ajax >> "+CodeUtil.toJson(bs.getData()));
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("getA106_Data error >> {}", e);
		}

		return bs;

	}
	public Acct_Transfer_Service getAcct_transfer_service() {
		return acct_transfer_service;
	}

	public void setAcct_transfer_service(Acct_Transfer_Service acct_transfer_service) {
		this.acct_transfer_service = acct_transfer_service;
	}

}
