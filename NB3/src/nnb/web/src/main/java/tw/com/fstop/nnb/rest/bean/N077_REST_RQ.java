package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N077_REST_RQ extends BaseRestBean_TW implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7474945981729810715L;
	
	private String ISSUER; // 晶片卡發卡行庫

	private String NeedSHA1;

	private String FLAG;// 0：非約定，1：約定

	private String TRANSEQ;// 交易序號

	private String ACNNO;// 晶片卡帳號

	private String ICDTTM;// 晶片卡日期時間

	private String ICSEQ;// 晶片卡序號

	private String ICMEMO;// 晶片卡備註欄

	private String TAC_Length;// TAC DATA Length

	private String TAC;// TAC DATA

	private String TAC_120space;// C DATA Space

	private String TRMID;// 端末設備查核碼

	private String iSeqNo; // iSeqNo

	private String pkcs7Sign;// IKEY

	private String jsondc;// IKEY

	private String PINNEW;// 網路銀行密碼（新）SSL 用

	private String FGTXWAY; // 交易機制 0:SSL ,1:ikey, 2:晶片金融卡 

	private String CUSIDN;// 統一編號

	private String FDPACN;// 存單帳號

	private String FDPNUM;// 存單號碼

	private String TRNCNT;// 轉期次數

	private String CHGFLG;// 變更轉帳帳號註記
	
	private String FGRENCNT;// 轉期次數

	private String TSFACN;// 轉帳帳號

	private String DPRENCNT;//// 轉期次數

    private String AMTFDP;//金額
 
    private String FGSVTYPE;//頁面顯示的續存方式
 
    
    private String DPSVACNO;
	
    //IDGATE
	private String sessionID;
	private String idgateID;
	private String txnID;
	public String getAMTFDP() {
		return AMTFDP;
	}

	public void setAMTFDP(String aMTFDP) {
		AMTFDP = aMTFDP;
	}



	public String getDPSVACNO() {
		return DPSVACNO;
	}

	public void setDPSVACNO(String dPSVACNO) {
		DPSVACNO = dPSVACNO;
	}

	public String getISSUER()
	{
		return ISSUER;
	}

	public void setISSUER(String iSSUER)
	{
		ISSUER = iSSUER;
	}

	public String getNeedSHA1()
	{
		return NeedSHA1;
	}

	public void setNeedSHA1(String needSHA1)
	{
		NeedSHA1 = needSHA1;
	}

	public String getFGRENCNT()
	{
		return FGRENCNT;
	}

	public void setFGRENCNT(String fGRENCNT)
	{
		FGRENCNT = fGRENCNT;
	}

	public String getFLAG()
	{
		return FLAG;
	}

	public void setFLAG(String fLAG)
	{
		FLAG = fLAG;
	}

	public String getTRANSEQ()
	{
		return TRANSEQ;
	}

	public void setTRANSEQ(String tRANSEQ)
	{
		TRANSEQ = tRANSEQ;
	}

	public String getACNNO()
	{
		return ACNNO;
	}

	public void setACNNO(String aCNNO)
	{
		ACNNO = aCNNO;
	}

	public String getICDTTM()
	{
		return ICDTTM;
	}

	public void setICDTTM(String iCDTTM)
	{
		ICDTTM = iCDTTM;
	}

	public String getICSEQ()
	{
		return ICSEQ;
	}

	public void setICSEQ(String iCSEQ)
	{
		ICSEQ = iCSEQ;
	}

	public String getICMEMO()
	{
		return ICMEMO;
	}

	public void setICMEMO(String iCMEMO)
	{
		ICMEMO = iCMEMO;
	}

	public String getTAC_Length()
	{
		return TAC_Length;
	}

	public void setTAC_Length(String tAC_Length)
	{
		TAC_Length = tAC_Length;
	}

	public String getTAC()
	{
		return TAC;
	}

	public void setTAC(String tAC)
	{
		TAC = tAC;
	}

	public String getTAC_120space()
	{
		return TAC_120space;
	}

	public void setTAC_120space(String tAC_120space)
	{
		TAC_120space = tAC_120space;
	}

	public String getTRMID()
	{
		return TRMID;
	}

	public void setTRMID(String tRMID)
	{
		TRMID = tRMID;
	}

	public String getCUSIDN()
	{
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN)
	{
		CUSIDN = cUSIDN;
	}

	public String getFDPACN()
	{
		return FDPACN;
	}

	public void setFDPACN(String fDPACN)
	{
		FDPACN = fDPACN;
	}

	public String getFDPNUM()
	{
		return FDPNUM;
	}

	public void setFDPNUM(String fDPNUM)
	{
		FDPNUM = fDPNUM;
	}

	public String getTRNCNT()
	{
		return TRNCNT;
	}

	public void setTRNCNT(String tRNCNT)
	{
		TRNCNT = tRNCNT;
	}

	public String getCHGFLG()
	{
		return CHGFLG;
	}

	public void setCHGFLG(String cHGFLG)
	{
		CHGFLG = cHGFLG;
	}

	public String getTSFACN()
	{
		return TSFACN;
	}

	public void setTSFACN(String tSFACN)
	{
		TSFACN = tSFACN;
	}

	public String getFGTXWAY()
	{
		return FGTXWAY;
	}

	public void setFGTXWAY(String fGTXWAY)
	{
		FGTXWAY = fGTXWAY;
	}

	public String getiSeqNo()
	{
		return iSeqNo;
	}

	public void setiSeqNo(String iSeqNo)
	{
		this.iSeqNo = iSeqNo;
	}

	public String getPkcs7Sign()
	{
		return pkcs7Sign;
	}

	public void setPkcs7Sign(String pkcs7Sign)
	{
		this.pkcs7Sign = pkcs7Sign;
	}

	public String getJsondc()
	{
		return jsondc;
	}

	public void setJsondc(String jsondc)
	{
		this.jsondc = jsondc;
	}

	public String getPINNEW()
	{
		return PINNEW;
	}

	public void setPINNEW(String pINNEW)
	{
		PINNEW = pINNEW;
	}

	public String getDPRENCNT()
	{
		return DPRENCNT;
	}

	public void setDPRENCNT(String dPRENCNT)
	{
		DPRENCNT = dPRENCNT;
	}


	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public String getIdgateID() {
		return idgateID;
	}

	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}

	public String getTxnID() {
		return txnID;
	}

	public void setTxnID(String txnID) {
		this.txnID = txnID;
    }
    
	public String getFGSVTYPE() {
		return FGSVTYPE;
	}

	public void setFGSVTYPE(String fGSVTYPE) {
		FGSVTYPE = fGSVTYPE;
	}

	

}
