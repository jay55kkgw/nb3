package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N924_REST_RS extends BaseRestBean implements Serializable {
	

    /**
	 * 
	 */
	private static final long serialVersionUID = -4282906736265512201L;


    private String OFFSET;

    private String __OCCURS;

    private String COUNT;

    private String HEADER;

    private String MSGCOD;

	private LinkedList<N924_REST_RSDATA> REC;


	public String getOFFSET() {
		return OFFSET;
	}

	public void setOFFSET(String oFFSET) {
		OFFSET = oFFSET;
	}

	public String get__OCCURS() {
		return __OCCURS;
	}

	public void set__OCCURS(String __OCCURS) {
		this.__OCCURS = __OCCURS;
	}

	public String getCOUNT() {
		return COUNT;
	}

	public void setCOUNT(String cOUNT) {
		COUNT = cOUNT;
	}

	public String getHEADER() {
		return HEADER;
	}

	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}

	public String getMSGCOD() {
		return MSGCOD;
	}

	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}

	public LinkedList<N924_REST_RSDATA> getREC() {
		return REC;
	}

	public void setREC(LinkedList<N924_REST_RSDATA> rEC) {
		REC = rEC;
	}
	

}
