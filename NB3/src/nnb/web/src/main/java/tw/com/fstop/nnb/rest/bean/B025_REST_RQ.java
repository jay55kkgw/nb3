package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class B025_REST_RQ extends BaseRestBean_FUND implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2588495944013644815L;
	
	private String CUSIDN ;
	private String BRHCOD ;
	private String RECDATE ;
	private String IP ;
	
	
	private String ADOPID = "B025";
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getBRHCOD() {
		return BRHCOD;
	}
	public void setBRHCOD(String bRHCOD) {
		BRHCOD = bRHCOD;
	}
	public String getRECDATE() {
		return RECDATE;
	}
	public void setRECDATE(String rECDATE) {
		RECDATE = rECDATE;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	} 
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
}
