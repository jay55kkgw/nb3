package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class NA70_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5185427099980002016L;

	private String RSPCOD; // 回應代碼

	
	public String getRSPCOD() {
		return RSPCOD;
	}

	public void setRSPCOD(String rSPCOD) {
		RSPCOD = rSPCOD;
	}
	
	
}
