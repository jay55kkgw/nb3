package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class TD01_MAIL_REST_RQ extends BaseRestBean_CC implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1413175607578796241L;
	
	String CUSIDN;
	String RESULT;
	String CMMAIL;
	
	public String getRESULT() {
		return RESULT;
	}
	public String getCMMAIL() {
		return CMMAIL;
	}
	public void setRESULT(String rESULT) {
		RESULT = rESULT;
	}
	public void setCMMAIL(String cMMAIL) {
		CMMAIL = cMMAIL;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	
}
