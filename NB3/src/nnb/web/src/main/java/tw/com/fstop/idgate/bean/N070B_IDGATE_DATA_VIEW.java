package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

public class N070B_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1301326804721580597L;
	/**
	 * 
	 */
	
	@SerializedName(value = "FGTXDATE") //即時or預約
	private String FGTXDATE;
	@SerializedName(value = "CMDD") // 日
	private String CMDD;
	@SerializedName(value = "CMSDATE") //起日
	private String CMSDATE;
	@SerializedName(value = "CMEDATE") //迄日
	private String CMEDATE;
	@SerializedName(value = "transfer_date") //轉帳日期
	private String transfer_date;
	@SerializedName(value = "OUTACN") //轉出帳號
	private String OUTACN;
	@SerializedName(value = "DPAGACNO_TEXT") //轉入帳號
	private String DPAGACNO_TEXT;
	@SerializedName(value = "AMOUNT_FMT") //轉帳金額
	private String AMOUNT_FMT;
	

	@Override
	public Map<String, String> coverMap() {
		
		LinkedHashMap result = new LinkedHashMap();
		result.put("交易名稱", "繳費");
		result.put("交易類型", "1".equals(this.FGTXDATE)?"即時":"預約");

		switch (this.FGTXDATE) {
		case "1":
			result.put("轉帳日期", this.transfer_date);
			break;
		case "2":
			result.put("轉帳日期", this.transfer_date);
			break;
		case "3":
			result.put("轉帳日期", "每月"+this.CMDD+"日，期間起日："+this.CMSDATE +" , 迄日："+this.CMSDATE);
			break;
		}
		result.put("轉出帳號", this.OUTACN);
		result.put("轉入帳號", this.DPAGACNO_TEXT);
		result.put("轉帳金額", this.AMOUNT_FMT);
		
		return result;
	}


	public String getFGTXDATE() {
		return FGTXDATE;
	}


	public void setFGTXDATE(String fGTXDATE) {
		FGTXDATE = fGTXDATE;
	}


	public String getCMDD() {
		return CMDD;
	}


	public void setCMDD(String cMDD) {
		CMDD = cMDD;
	}


	public String getCMSDATE() {
		return CMSDATE;
	}


	public void setCMSDATE(String cMSDATE) {
		CMSDATE = cMSDATE;
	}


	public String getCMEDATE() {
		return CMEDATE;
	}


	public void setCMEDATE(String cMEDATE) {
		CMEDATE = cMEDATE;
	}


	public String getTransfer_date() {
		return transfer_date;
	}


	public void setTransfer_date(String transfer_date) {
		this.transfer_date = transfer_date;
	}


	public String getOUTACN() {
		return OUTACN;
	}


	public void setOUTACN(String oUTACN) {
		OUTACN = oUTACN;
	}


	public String getDPAGACNO_TEXT() {
		return DPAGACNO_TEXT;
	}


	public void setDPAGACNO_TEXT(String dPAGACNO_TEXT) {
		DPAGACNO_TEXT = dPAGACNO_TEXT;
	}


	public String getAMOUNT_FMT() {
		return AMOUNT_FMT;
	}


	public void setAMOUNT_FMT(String aMOUNT_FMT) {
		AMOUNT_FMT = aMOUNT_FMT;
	}

}
