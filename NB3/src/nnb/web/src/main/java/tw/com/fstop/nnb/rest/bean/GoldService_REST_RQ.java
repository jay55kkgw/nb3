package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.List;

/**
 * GoldService電文RQ
 */
public class GoldService_REST_RQ extends BaseRestBean_GOLD implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3076340133338091560L;

	public String date;

	public String cnt;

	public String time;

	public String kind;

	public List<GoldProduct> goldProduct;

	@Override
	public String toString() {
		return "GoldService_REST_RQ [date=" + date + ", cnt=" + cnt + ", time=" + time + ", kind=" + kind
				+ ", goldProducts=" + goldProduct + "]";
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getCnt() {
		return cnt;
	}

	public void setCnt(String cnt) {
		this.cnt = cnt;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public List<GoldProduct> getGoldProduct() {
		return goldProduct;
	}

	public void setGoldProducts(List<GoldProduct> goldProduct) {
		this.goldProduct = goldProduct;
	}

}