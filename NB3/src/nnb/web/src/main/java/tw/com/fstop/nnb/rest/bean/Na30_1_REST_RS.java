package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class Na30_1_REST_RS extends BaseRestBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2796966415534479000L;
	private String MSGCOD;
	private String occurMsg;
	private String CCTXTIME;
	private String CCBIRTHDATE;
	public String getMSGCOD() {
		return MSGCOD;
	}

	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}

	public String getOccurMsg() {
		return occurMsg;
	}

	public void setOccurMsg(String occurMsg) {
		this.occurMsg = occurMsg;
	}

	public String getCCTXTIME() {
		return CCTXTIME;
	}

	public void setCCTXTIME(String cCTXTIME) {
		CCTXTIME = cCTXTIME;
	}

	public String getCCBIRTHDATE() {
		return CCBIRTHDATE;
	}

	public void setCCBIRTHDATE(String cCBIRTHDATE) {
		CCBIRTHDATE = cCBIRTHDATE;
	}
}
