package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class Ratequery_N027_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5349427559816292854L;

	private String UPDATEDATE;
	// 資料陣列
	private LinkedList<Ratequery_N027_RSDATA> REC;

	public String getUPDATEDATE() {
		return UPDATEDATE;
	}

	public LinkedList<Ratequery_N027_RSDATA> getREC() {
		return REC;
	}

	public void setUPDATEDATE(String uPDATEDATE) {
		UPDATEDATE = uPDATEDATE;
	}

	public void setREC(LinkedList<Ratequery_N027_RSDATA> rEC) {
		REC = rEC;
	}

}
