package tw.com.fstop.nnb.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import fstop.orm.po.ADMMYOP;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.tbb.nnb.dao.AdmHotopDao;
import tw.com.fstop.tbb.nnb.dao.AdmMyOpDao;
import tw.com.fstop.tbb.nnb.dao.Nb3SysOpDao;
import tw.com.fstop.util.DateUtil;

@Service
public class Personalize_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private AdmMyOpDao admmyopdao;
	@Autowired
	private AdmHotopDao admhotopdao;
	@Autowired
	private Nb3SysOpDao nb3sysopdao;
	
	public BaseResult addMyOp(String cusidn, String adopid) {
		log.trace("addMyOp...");
		
		BaseResult result = new BaseResult();
		
		try {
			if(admmyopdao.getMyOpList(cusidn).size() < 10) {
				String lastdt = DateUtil.getCurentDateTime("yyyyMMddHHmmss");
				
				ADMMYOP admmyop = new ADMMYOP(cusidn, adopid, lastdt);
				
				admmyopdao.save(admmyop);
				result.setResult(true);
				result.addData("overTen", false);
			}else {
				result.setResult(true);
				result.addData("overTen", true);
			}
				
		} catch (Exception e) {
			log.error("",e);
		}
		
		return result;
	}
	
	public Boolean deleteMyOp(String cusidn, String adopid) {
		log.trace("deleteMyOp...");
		
		boolean result = false;
		
		try {
			admmyopdao.deleteById(cusidn, adopid);
			
		} catch (Exception e) {
			log.error("",e);
		}
		
		return result;
	}
	
	
	/**
	 * 取得menu adopid list
	 */
	@Cacheable(value="menuAdopidCache", key="#root.methodName", unless="#result == null")
	public List getAdopidList() {
		List qresult = null;
		try {
			qresult = nb3sysopdao.getAdopidList();
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		return qresult;
	}
	
	
	/**
	 * 取得menu url list
	 */
	@Cacheable(value="menuUrlCache", key="#root.methodName", unless="#result == null")
	public List<String> getUrlList() {
		List<String> qresult = null;
		try {
			qresult = nb3sysopdao.getUrlList();
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		return qresult;
	}
	
	/**
	 * 是否是我的最愛的功能
	 */
	public boolean isFavorite(String cusidn, String adopid) {
		boolean result = false;
		try {
			List<String> list = admmyopdao.getMyOpList(cusidn);
			return list.contains(adopid);
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		return result;
	}
	
	public void SaveUsuallyList(String cusidn, String adopid) {
		try {
			admhotopdao.saveADMHOTOP(cusidn, adopid);
			admhotopdao.deleteOutOfLimitFive(cusidn);
		} catch (Exception e) {
			log.error("{}", e);
		}
		
	}
}
