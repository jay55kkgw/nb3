package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N927_REST_RS extends BaseRestBean implements Serializable{
	private static final long serialVersionUID = 9047844481338101277L;
	
	private String RISK;
	private String GETLTD;
	private String BRHCOD2;
	private String LSTLTD2;
	private String RISK_FLAG;
	private String APLBRH;
	private String DEGREE;
	private String CAREER;
	private String SALARY;
	private String BRTHDY;
	private String MARK1;
	private String EXTRCE;
	private String RECNUM;
	private String NAME;
	private String CUSNAME;
	
	public String getRISK(){
		return RISK;
	}
	public void setRISK(String rISK){
		RISK = rISK;
	}
	public String getGETLTD(){
		return GETLTD;
	}
	public void setGETLTD(String gETLTD){
		GETLTD = gETLTD;
	}
	public String getBRHCOD2(){
		return BRHCOD2;
	}
	public void setBRHCOD2(String bRHCOD2){
		BRHCOD2 = bRHCOD2;
	}
	public String getLSTLTD2(){
		return LSTLTD2;
	}
	public void setLSTLTD2(String lSTLTD2){
		LSTLTD2 = lSTLTD2;
	}
	public String getRISK_FLAG(){
		return RISK_FLAG;
	}
	public void setRISK_FLAG(String rISK_FLAG){
		RISK_FLAG = rISK_FLAG;
	}
	public String getAPLBRH(){
		return APLBRH;
	}
	public void setAPLBRH(String aPLBRH){
		APLBRH = aPLBRH;
	}
	public String getDEGREE(){
		return DEGREE;
	}
	public void setDEGREE(String dEGREE){
		DEGREE = dEGREE;
	}
	public String getCAREER(){
		return CAREER;
	}
	public void setCAREER(String cAREER){
		CAREER = cAREER;
	}
	public String getSALARY(){
		return SALARY;
	}
	public void setSALARY(String sALARY){
		SALARY = sALARY;
	}
	public String getBRTHDY(){
		return BRTHDY;
	}
	public void setBRTHDY(String bRTHDY){
		BRTHDY = bRTHDY;
	}
	public String getMARK1(){
		return MARK1;
	}
	public void setMARK1(String mARK1){
		MARK1 = mARK1;
	}
	public String getEXTRCE(){
		return EXTRCE;
	}
	public void setEXTRCE(String eXTRCE){
		EXTRCE = eXTRCE;
	}
	public String getRECNUM(){
		return RECNUM;
	}
	public void setRECNUM(String rECNUM){
		RECNUM = rECNUM;
	}
	public String getNAME(){
		return NAME;
	}
	public void setNAME(String nAME){
		NAME = nAME;
	}
	public String getCUSNAME() {
		return CUSNAME;
	}
	public void setCUSNAME(String cUSNAME) {
		CUSNAME = cUSNAME;
	}
}