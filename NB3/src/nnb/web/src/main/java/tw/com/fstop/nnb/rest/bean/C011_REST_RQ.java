package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class C011_REST_RQ extends BaseRestBean_FUND implements Serializable {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -855719225417441841L;
	
	private String CUSIDN;	// 統一編號
	private String EMAIL;	// IP位置
	
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getEMAIL() {
		return EMAIL;
	}
	public void setEMAIL(String eMAIL) {
		EMAIL = eMAIL;
	}
}
