package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N530_REST_RSDATA extends BaseRestBean implements Serializable {

	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1305565614430302334L;
	//利息
	String ITR;
	//計息方式
	String INTMTH;
	String NTDBAL;
	//帳號
	String ACN;
	String DPISDT;
	//幣別
	String CUID;
	String TSFACN;
	String AUTXFTM;
	String DUEDAT;
	//餘額
	String BALANCE;
	String ILAZLFTM;
	String FDPNO;
	
	public String getITR() {
		return ITR;
	}
	public void setITR(String iTR) {
		ITR = iTR;
	}
	public String getINTMTH() {
		return INTMTH;
	}
	public void setINTMTH(String iNTMTH) {
		INTMTH = iNTMTH;
	}
	public String getNTDBAL() {
		return NTDBAL;
	}
	public void setNTDBAL(String nTDBAL) {
		NTDBAL = nTDBAL;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getDPISDT() {
		return DPISDT;
	}
	public void setDPISDT(String dPISDT) {
		DPISDT = dPISDT;
	}
	public String getCUID() {
		return CUID;
	}
	public void setCUID(String cUID) {
		CUID = cUID;
	}
	public String getTSFACN() {
		return TSFACN;
	}
	public void setTSFACN(String tSFACN) {
		TSFACN = tSFACN;
	}
	public String getAUTXFTM() {
		return AUTXFTM;
	}
	public void setAUTXFTM(String aUTXFTM) {
		AUTXFTM = aUTXFTM;
	}
	public String getDUEDAT() {
		return DUEDAT;
	}
	public void setDUEDAT(String dUEDAT) {
		DUEDAT = dUEDAT;
	}
	public String getBALANCE() {
		return BALANCE;
	}
	public void setBALANCE(String bALANCE) {
		BALANCE = bALANCE;
	}
	public String getILAZLFTM() {
		return ILAZLFTM;
	}
	public void setILAZLFTM(String iLAZLFTM) {
		ILAZLFTM = iLAZLFTM;
	}
	public String getFDPNO() {
		return FDPNO;
	}
	public void setFDPNO(String fDPNO) {
		FDPNO = fDPNO;
	}


}
