package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N843_REST_RSDATA implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6804316838411623344L;
	
	private String ACN;
	private String EVTMARK;
	private String RESERVE;
	
	
	
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getEVTMARK() {
		return EVTMARK;
	}
	public void setEVTMARK(String eVTMARK) {
		EVTMARK = eVTMARK;
	}
	public String getRESERVE() {
		return RESERVE;
	}
	public void setRESERVE(String rESERVE) {
		RESERVE = rESERVE;
	}
}
