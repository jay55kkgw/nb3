package tw.com.fstop.nnb.spring.controller;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.Acct_Bond_Service;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DownloadUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({ SessionUtil.PD, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, SessionUtil.PRINT_DATALISTMAP_DATA,
		SessionUtil.RESULT_LOCALE_DATA, SessionUtil.CONFIRM_LOCALE_DATA, SessionUtil.RESULT_LOCALE_DATA,
		SessionUtil.CUSIDN })
@Controller
@RequestMapping(value = "/NT/ACCT/BOND")
public class Acct_Bond_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Acct_Bond_Service acct_bond_service;

	// 中央登錄債券餘額
	@RequestMapping(value = "/bond_balance")
	public String bond_balance(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("bond_balance >>");
		String cusidn = new String(
				Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
		try {
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			bs = acct_bond_service.N920_REST(cusidn, Acct_Bond_Service.BOND_BOLANCE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.toString());
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/acct/bond_balance";
				// target = "/acct/bond_balance1";
				model.addAttribute("username_alter_result", bs);
			}else {
				bs.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 外匯匯入匯款查詢結果
	@RequestMapping(value = "/bond_balance_result")
	public String bond_balance_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("bond_balance_result>>");
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		String cusidn = new String(
				Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
		try {
			bs = new BaseResult();
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				bs.reset();
				bs = acct_bond_service.bond_balance_result(cusidn, okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.toString());
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/acct/bond_balance_result";
				// target = "/acct/bond_balance2";
				List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs
						.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("loan_detail", bs);
			} else {
				// 新增回上一頁的路徑
				bs.setPrevious("/NT/ACCT/BOND/bond_balance");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 中央登錄債券明細
	@RequestMapping(value = "/bond_detail")
	public String bond_detail(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("bond_detail >>");
		String cusidn = new String(
				Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
		try {
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			bs = acct_bond_service.N920_REST(cusidn, Acct_Bond_Service.BOND_BOLANCE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.toString());
		} finally {
			target = "/acct/bond_detail";
			model.addAttribute("bond_detail", bs);
		}
		return target;
	}

	// 中央登錄債券明細結果頁
	@RequestMapping(value = "/bond_detail_result")
	public String bond_detail_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("bond_detail_result>>");
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		String cusidn = new String(
				Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
		try {
			bs = new BaseResult();
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				bs.reset();
				bs = acct_bond_service.bond_detail_result(cusidn, okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.toString());
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/acct/bond_detail_result";
				Map<String, Object> dataMap = (Map<String, Object>) bs.getData();
				log.trace("dataMap={}", dataMap);

				List<Map<String, Object>> labelListMap = (List<Map<String, Object>>) dataMap.get("labelList");
				log.trace("labelListMap={}", labelListMap);

				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, labelListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, labelListMap);
				model.addAttribute("bond_detail_result", bs);
			} else {
				bs.setPrevious("/NT/ACCT/BOND/bond_detail");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 中央登錄債券明細ajax直接下載
//	@RequestMapping(value = "/bond_detail_ajaxDirectDownload")
//	public String bond_detail_ajaxDirectDownload(HttpServletRequest request, HttpServletResponse response,
//			@RequestBody(required = false) String serializeString, Model model) {
//		String target = "/error";
//		BaseResult bs = null;
//		log.trace("bond_detail_ajaxDirectDownload");
//		log.trace("serializeString={}", serializeString);
//		// 序列化傳入值
//		Map<String, String> formMap = DownloadUtil.serializeStringToMap(serializeString);
//		log.debug("formMap={}" + formMap);
//		try {
//			bs = new BaseResult();
//			String cusidn = new String(
//					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
//			log.trace("cusidn={}", cusidn);
//			for (Entry<String, String> entry : formMap.entrySet()) {
//				formMap.put(entry.getKey(), URLDecoder.decode(entry.getValue(), "UTF-8"));
//			}
//			bs = acct_bond_service.bondDetailDirectDownload(cusidn, formMap);
//		} catch (Exception e) {
//			log.error(e.toString());
//			log.error("", e);
//		} finally {
//			if (bs != null && bs.getResult()) {
//				target = "forward:/ajaxDirectDownload";
//
//				Map<String, Object> dataMap = (Map) bs.getData();
//				log.trace("dataMap={}", dataMap);
//				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("labelList");
//				log.trace("rowListMap={}", rowListMap);
//				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rowListMap);
//				Map<String, Object> parameterMap = (Map<String, Object>) dataMap.get("parameterMap");
//				parameterMap.put("CMQTIME", dataMap.get("CMQTIME"));
//				parameterMap.put("CMPERIOD", dataMap.get("CMPERIOD"));
//				parameterMap.put("CMRECNUM", dataMap.get("CMRECNUM"));
//				request.setAttribute("parameterMap", parameterMap);
//			} else {
//				String errorCode = "";
//				String errorMessage = "";
//
//				try {
//					errorCode = URLEncoder.encode(bs.getMsgCode(), "UTF-8");
//					errorMessage = URLEncoder.encode(bs.getMessage(), "UTF-8");
//				} catch (Exception e) {
//					e.printStackTrace();
//					log.error("", e);
//				}
//				response.addHeader("errorCode", errorCode);
//				response.addHeader("errorMessage", errorMessage);
//			}
//		}
//		return target;
//	}

	// 中央登錄債券明細直接下載
	@RequestMapping(value = "/bond_detail_directDownload")
	public String bond_detail_directDownload(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("bond_detail_directDownload");
		// 序列化傳入參數
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		log.trace(ESAPIUtil.vaildLog("formMap >> " + CodeUtil.toJson(okMap))); 
		try {
			bs = new BaseResult();
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.trace("cusidn={}", cusidn);
			for (Entry<String, String> entry : okMap.entrySet()) {
				okMap.put(entry.getKey(), URLDecoder.decode(entry.getValue(), "UTF-8"));
			}
			bs = acct_bond_service.bondDetailDirectDownload(cusidn, okMap);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("bond_detail_directDownload error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "forward:/directDownload";
				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap={}", dataMap);
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("labelList");
				log.debug("rowListMap={}", rowListMap);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rowListMap);
				Map<String, Object> parameterMap = (Map<String, Object>) dataMap.get("parameterMap");
				parameterMap.put("CMQTIME", dataMap.get("CMQTIME"));
				parameterMap.put("CMPERIOD", dataMap.get("CMPERIOD"));
				parameterMap.put("CMRECNUM", dataMap.get("CMRECNUM"));
				request.setAttribute("parameterMap", parameterMap);
			} else {
				bs.setPrevious("/NT/ACCT/BOND/bond_detail");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
}
