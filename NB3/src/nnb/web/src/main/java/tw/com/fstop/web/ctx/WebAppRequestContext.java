
/*
 * Copyright (c) 2017, FSTOP, Inc. All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tw.com.fstop.web.ctx;

import tw.com.fstop.ctx.MappedThreadLocalAccessor;


/**
 * Context for web application.
 * Provide functions to access context information through out 
 * web application request life cycle. 
 *
 * @since 1.0.1
 */
public class WebAppRequestContext
{
    public enum Key
    {
        IP_ADDRESS("IP_ADDRESS"),
        USER_AGENT("USER_AGENT"),
        ACCEPT_LANGUAGE("ACCEPT_LANGUAGE"),        
        PREFERRED_LOCALE("PREFERRED_LOCALE"),        
        START_TIME("START_TIME"),
        END_TIME("END_TIME"),
        REQ_RESOURCE("REQ_RESOURCE"),
        
        CONTEXT_PATH("CONTEXT_PATH"),
        HTTP_SESSION("HTTP_SESSION"),
        USER_INPUT("USER_INPUT"),
        DECODED_USER_INPUT("DECODED_USER_INPUT"),
        
        INVALID_VALUE("")
        ;
        
        private final String text;
        
        private Key(String text)
        {
            this.text = text;
        }
        
        public String getText()
        {
            return this.text;
        }
        
        public static Key fromIndex(int i) 
        {
            try 
            {
                return values()[i];
            } 
            catch (Exception e) 
            {
                return INVALID_VALUE;
            }
        }
        
    }  //Key
    
    public static void setValue(Key key, String value)
    {
        setValue(key.getText(), value);
    }

    public static void setValue(Key key, Object value)
    {
        MappedThreadLocalAccessor.set(key, value);
    }
    
    public static void setValue(String key, String value)
    {
        MappedThreadLocalAccessor.set(key, value);
    }

    public static void setValue(String key, Object value)
    {
        MappedThreadLocalAccessor.set(key, value);
    }
    
    public static Object getValue(Key key)
    {
        return getValue(key.getText());
    }

    public static Object getValue(String key)
    {
        return MappedThreadLocalAccessor.get(key);
    }
    
    public static Object remove(Key key)
    {
        return remove(key.getText());
    }
    
    public static Object remove(String key)
    {
        return MappedThreadLocalAccessor.remove(key);
    }
    
    public static void remove()
    {
        MappedThreadLocalAccessor.remove();
    }
    
    
}
