package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class SmsotpWithphone_REST_RQ extends BaseRestBean_LOAN implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3327209899986847270L;
	
	private String UID;
	private String CUSIDN;
	private String PHONE;
	private String FuncType;
	private String OTP;
	//取得簡訊密碼時，不寫TXNLOG所以改SMSADOPID
	private String SMSADOPID;
	private String MSGTYPE;
	private String CN;
	
	
	public String getMSGTYPE() {
		return MSGTYPE;
	}
	public void setMSGTYPE(String mSGTYPE) {
		MSGTYPE = mSGTYPE;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getFuncType() {
		return FuncType;
	}
	public void setFuncType(String funcType) {
		FuncType = funcType;
	}
	public String getOTP() {
		return OTP;
	}
	public void setOTP(String oTP) {
		OTP = oTP;
	}
	public String getSMSADOPID() {
		return SMSADOPID;
	}
	public void setSMSADOPID(String sMSADOPID) {
		SMSADOPID = sMSADOPID;
	}
	public String getPHONE() {
		return PHONE;
	}
	public void setPHONE(String pHONE) {
		PHONE = pHONE;
	}
	public String getCN() {
		return CN;
	}
	public void setCN(String cN) {
		CN = cN;
	}
	
}
