package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N1010_CP_REST_RQ extends BaseRestBean_CC implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5051219122081175767L;
	
	
	
	//ms_tw需要
	private String PINNEW;
	private String FGTXWAY;
	private String CUSIDN;
	
	//BillHunter 需要
	private String Cust_id;
	private String Status;
	private String Email;
	//修正Heap Inspection
	private String Oldpw;
	private String Newpw;
	private String sys_ip;
	
	public String getPINNEW() {
		return PINNEW;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public String getCust_id() {
		return Cust_id;
	}
	public String getStatus() {
		return Status;
	}
	public String getEmail() {
		return Email;
	}
	public String getSys_ip() {
		return sys_ip;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public void setCust_id(String cust_id) {
		Cust_id = cust_id;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public void setSys_ip(String sys_ip) {
		this.sys_ip = sys_ip;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getOldpw() {
		return Oldpw;
	}
	public String getNewpw() {
		return Newpw;
	}
	public void setOldpw(String oldpw) {
		Oldpw = oldpw;
	}
	public void setNewpw(String newpw) {
		Newpw = newpw;
	}
	
}
