package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N816A_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8111928052113225298L;
	
	private String EXP_DATE;
	private String CARDNUM ;
	private String STATUS;
	
	
	public String getEXP_DATE() {
		return EXP_DATE;
	}
	public String getCARDNUM() {
		return CARDNUM;
	}
	public String getSTATUS() {
		return STATUS;
	}
	public void setEXP_DATE(String eXP_DATE) {
		EXP_DATE = eXP_DATE;
	}
	public void setCARDNUM(String cARDNUM) {
		CARDNUM = cARDNUM;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	
	
}