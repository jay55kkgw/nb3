package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * 自然人憑證驗證RQ
 */
public class Vacheck_REST_RQ extends BaseRestBean_CC implements Serializable{
	private static final long serialVersionUID = 6868861910004819383L;
	
	private String AUTHCODE;

	public String getAUTHCODE(){
		return AUTHCODE;
	}
	public void setAUTHCODE(String aUTHCODE){
		AUTHCODE = aUTHCODE;
	}
}