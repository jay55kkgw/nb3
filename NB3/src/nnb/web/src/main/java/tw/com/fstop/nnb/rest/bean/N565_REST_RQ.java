package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N565_REST_RQ extends BaseRestBean_FX implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -546789830841032671L;

	String ACCTNO;			//帳號
	String CMSDATE;			//查詢起期
	String CMEDATE;			//查詢迄期
	String CUSIDN;			//統一編號
	String TRNSRC = "NB";	//交易來源
	String USERDATA;		//本次未完資料KEY值
	

	String OKOVNEXT;
	
	
	public String getOKOVNEXT() {
		return OKOVNEXT;
	}
	public void setOKOVNEXT(String oKOVNEXT) {
		OKOVNEXT = oKOVNEXT;
	}
	
	public String getACCTNO() {
		return ACCTNO;
	}

	public void setACCTNO(String aCCTNO) {
		ACCTNO = aCCTNO;
	}

	public String getCMSDATE() {
		return CMSDATE;
	}

	public void setCMSDATE(String cMSDATE) {
		CMSDATE = cMSDATE;
	}
	
	public String getCMEDATE() {
		return CMEDATE;
	}

	public void setCMEDATE(String cMEDATE) {
		CMEDATE = cMEDATE;
	}
	
	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getTRNSRC() {
		return TRNSRC;
	}

	public void setTRNSRC(String tRNSRC) {
		TRNSRC = tRNSRC;
	}
	
	public String getUSERDATA() {
		return USERDATA;
	}

	public void setUSERDATA(String uSERDATA) {
		USERDATA = uSERDATA;
	}
}
