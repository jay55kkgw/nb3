
/*
 * Copyright (c) 2017, FSTOP, Inc. All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tw.com.fstop.web;

import java.io.BufferedReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import tw.com.fstop.util.ESAPIUtil;

/**
 * Basic servlet input and output processor.
 * 
 * ServletProcessor should throws ProcessorException, but 
 * currently not implement yet.
 * 
 * <pre>
 * Do the flowing things:
 * 1. Set servlet reponse encoding to "UTF-8".
 * 2. Get request parameters.
 * 3. Decode request parameters. 
 * 
 * </pre>
 *
 * @since 1.0.1
 */
public class BaseServletProcessor implements ServletProcessor
{

    private final static Logger log = LoggerFactory.getLogger(BaseServletProcessor.class);
    static final String ENCODING = "UTF-8";

    protected Map<String, String> inputParam;
    protected Map<String, String> decodedInputParam;
    protected String contextPath;
            
    public String getContextPath()
    {
        return contextPath;
    }

    public void setContextPath(String contextPath)
    {
        this.contextPath = contextPath;
    }

    public Map<String, String> getInputParam()
    {
        return inputParam;
    }

    public void setInputParam(Map<String, String> input)
    {
        this.inputParam = input;
    }

    public Map<String, String> getDecodedInputParam()
    {
        return decodedInputParam;
    }

    public void setDecodedUserInput(Map<String, String> decodedInput)
    {
        this.decodedInputParam = decodedInput;
    }
    
    public String removeUrlContextPath(String url)
    {
        String c = getContextPath();
        String ret = url.replace(c, "");        
        return ret;
    }
    

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) throws ProcessorException
    {
        response.setCharacterEncoding(ENCODING);
        contextPath = request.getContextPath();
        
        inputParam = getParameterMap(request);
        decodedInputParam = decodeMap(inputParam);              
    }
    
    
    /**
     * 另外產生經過  URLDecoder.decode 的 decodedUserInput
     * @param map user input parameters map
     * @return decoded result
     */
    public Map<String, String> decodeMap(Map<String, String> map)
    {
        Map<String, String> ret = new HashMap<String, String>();
         
        for(String key : map.keySet())
        {
            try
            {
                String input = map.get(key);
                if (input == null)
                {
                    ret.put(key, input);
                }
                else
                {
                    String s = URLDecoder.decode(input, ENCODING);
                    ret.put(key, s);                   
                }
                
            }
            catch(Exception e)
            {
                //e.printStackTrace();
                log.error("decodeMap error=", e);
            }
        }
        
        return ret;
    }
    
    /**
     * for html encodeURIComponent
     * @param s input string
     * @return result string
     */
    public String convertJsonEncode(String s)
    {
        //試著轉 JSON {}
        if (s.startsWith("%7B%22") && s.endsWith("%22%7D"))
        {
            try 
            {
                s = URLDecoder.decode(s, ENCODING);
            } 
            catch (UnsupportedEncodingException e) 
            {
                //e.printStackTrace();
                log.error("checkJsonEncode error=", e);
            }   
        }
        
        return s;
    }
    
    public Hashtable<String, String> getParameterMap(HttpServletRequest request)
    {
        Gson gson = new Gson();
        //將Request的參數全部放到userInput裡。
        Hashtable<String, String> userInput = new Hashtable<String, String>();      
        Enumeration<String> paramNames = request.getParameterNames();
        
        //取得一般 get 的 parameter
        while (paramNames.hasMoreElements())  
        {
            String paramName = (String) paramNames.nextElement();
            //System.out.println("paramName=" + paramName);
            //對於多選的網頁元件，例如 checkbox 需再進一步檢查多選狀態
            String[] paramValues =request.getParameterValues(paramName);
            if (paramValues.length == 1) 
            {
                //log.debug("{0} = {1}", paramName, paramValues[0]);
                log.debug(ESAPIUtil.vaildLog(paramName + "=" + paramValues[0]));
                paramValues[0] = convertJsonEncode(paramValues[0]);
                
                userInput.put(paramName, paramValues[0]);
            } 
            else 
            {
                //多選元件的值以 arraylist 轉成 json 字串後放入 hashmap 中
                List<String> list = new ArrayList<String>();
                for(int i=0; i < paramValues.length; i++) 
                {
                    paramValues[i] = convertJsonEncode(paramValues[i]);
                    
                    list.add(paramValues[i]);
                    log.debug(ESAPIUtil.vaildLog("multi select = " + paramValues[i])); 
                }
                String values = gson.toJson(list);
                userInput.put(paramName, values);
               // log.debug("{0} = {1}", paramName, values);
                log.debug(ESAPIUtil.vaildLog(paramName + "=" + values));
            }
        }       

        //取得 post 的 parameter
        try
        {
            //由 post data 中取得的資料為 key=value，必需加以處理
            BufferedReader reader = request.getReader();
            String line;
            while ((line = reader.readLine()) != null)
            {
                String [] data = line.split("=");
                if (data.length > 1)
                {
                    userInput.put(data[0], data[1]);                    
                }
            }
            
        }
        catch(Exception e)
        {
            
        }
        
        
        return userInput;
        
    }//getParameterMap

    
}
