package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N103_REST_RQ extends BaseRestBean_OLA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7833649410207856814L;

	private String FGTXWAY;
	private String CUSIDN;
	
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
}
