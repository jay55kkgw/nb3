package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N821_REST_RQ extends BaseRestBean_CC implements Serializable {

	
	private static final long serialVersionUID = 8019822747111651798L;
	/**
	 * 
	 */
	
	
	String	CUSIDN	;
	String  PINNEW  ;
	String  CARDNUM ; 
	String  PRTYM;
	
	String FGTXWAY;
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public String getCARDNUM() {
		return CARDNUM;
	}
	public String getPRTYM() {
		return PRTYM;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public void setCARDNUM(String cARDNUM) {
		CARDNUM = cARDNUM;
	}
	public void setPRTYM(String pRTYM) {
		PRTYM = pRTYM;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	
}
