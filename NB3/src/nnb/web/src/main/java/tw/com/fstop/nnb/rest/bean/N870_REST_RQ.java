package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N870_REST_RQ extends BaseRestBean_TW implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -123147079713032031L;
	String ACN1; // 帳號
	String CUSIDN; // 統一編號
	String USERDATA_X50; // 暫存空間區

	public String getACN1() {
		return ACN1;
	}

	public void setACN1(String aCN1) {
		ACN1 = aCN1;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getUSERDATA_X50() {
		return USERDATA_X50;
	}

	public void setUSERDATA_X50(String uSERDATA_X50) {
		USERDATA_X50 = uSERDATA_X50;
	}
}
