package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N550_REST_RSDATA implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8546109277562505814L;
	private String RREFNO;
	private String RMARK;
	private String RISSBK;
	private String REXPDATE;
	private String RLCCCY;
	private String RLCTXAMT;
	private String RLCNO;
	private String RLCOSBAL;
	private String RRCVDATE;
	public String getRREFNO() {
		return RREFNO;
	}
	public void setRREFNO(String rREFNO) {
		RREFNO = rREFNO;
	}
	public String getRMARK() {
		return RMARK;
	}
	public void setRMARK(String rMARK) {
		RMARK = rMARK;
	}
	public String getRISSBK() {
		return RISSBK;
	}
	public void setRISSBK(String rISSBK) {
		RISSBK = rISSBK;
	}
	public String getREXPDATE() {
		return REXPDATE;
	}
	public void setREXPDATE(String rEXPDATE) {
		REXPDATE = rEXPDATE;
	}
	public String getRLCCCY() {
		return RLCCCY;
	}
	public void setRLCCCY(String rLCCCY) {
		RLCCCY = rLCCCY;
	}
	public String getRLCTXAMT() {
		return RLCTXAMT;
	}
	public void setRLCTXAMT(String rLCTXAMT) {
		RLCTXAMT = rLCTXAMT;
	}
	public String getRLCNO() {
		return RLCNO;
	}
	public void setRLCNO(String rLCNO) {
		RLCNO = rLCNO;
	}
	public String getRLCOSBAL() {
		return RLCOSBAL;
	}
	public void setRLCOSBAL(String rLCOSBAL) {
		RLCOSBAL = rLCOSBAL;
	}
	public String getRRCVDATE() {
		return RRCVDATE;
	}
	public void setRRCVDATE(String rRCVDATE) {
		RRCVDATE = rRCVDATE;
	}
	
	
	
}
