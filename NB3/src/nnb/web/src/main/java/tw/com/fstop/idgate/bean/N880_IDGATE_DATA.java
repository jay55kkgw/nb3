package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N880_IDGATE_DATA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3771959226005986124L;
	
	@SerializedName(value = "CUSIDN")
	private String CUSIDN;
	
	@SerializedName(value = "ACN")
	private String ACN;

//	@SerializedName(value = "COUNT")
//	private String COUNT;

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

//	public String getCOUNT() {
//		return COUNT;
//	}
//
//	public void setCOUNT(String cOUNT) {
//		COUNT = cOUNT;
//	}
}
