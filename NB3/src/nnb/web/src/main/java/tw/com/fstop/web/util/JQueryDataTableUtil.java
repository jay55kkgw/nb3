
/*
 * Copyright (c) 2017, FSTOP, Inc. All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tw.com.fstop.web.util;

import java.util.HashMap;
import java.util.Map;

public class JQueryDataTableUtil
{

    static Integer zeroWhenEmpty(String value)
    {
        if (value == null || value.trim().isEmpty())
        {
            return 0;
        }
        else
        {
            return Integer.valueOf(value);
        }
    }
    
    public static JQueryDataTableParamModel getMapParam(Map<String, Object> params)
    {
        Map<String, String> param = null;
        
        param = new HashMap<String, String>();
        for(String key : params.keySet())
        {
            param.put(key, (String) params.get(key));
        }
        return getParam(param);
    }
        
    public static JQueryDataTableParamModel getParam(Map<String, String> params)
    {
        if (params.get("sEcho") != null && params.get("sEcho") != "")
        {
            JQueryDataTableParamModel param = new JQueryDataTableParamModel();
            param.sEcho = params.get("sEcho");
            param.sSearch = params.get("sSearch");
            param.sColumns = params.get("sColumns");
            param.iDisplayStart = zeroWhenEmpty(params.get("iDisplayStart"));
            param.iDisplayLength = zeroWhenEmpty(params.get("iDisplayLength"));

            param.iColumns = zeroWhenEmpty(params.get("iColumns"));
            param.iSortingCols = zeroWhenEmpty(params.get("iSortingCols"));
            param.iSortColumnIndex = zeroWhenEmpty(params.get("iSortCol_0"));
            param.sSortDirection = params.get("sSortDir_0");
            if (param.sSortDirection == null || param.sSortDirection.isEmpty())
            {
                param.sSortDirection = "asc";
            }

            String aSortColumnNames[] = new String[param.iSortingCols];
            for (int i = 0; i < param.iSortingCols; i++)
            {
                if (params.containsKey("iSortCol_" + i) && params.containsKey("mDataProp_" + i))
                {
                    aSortColumnNames[i] = params.get("mDataProp_" + params.get("iSortCol_" + i))
                            + (params.containsKey("sSortDir_" + i) ? " " + params.get("sSortDir_" + i) : " asc");
                }
            }
            param.aSortColumnNames = aSortColumnNames;
            return param;
        }
        else
            return null;
    }

    public static class JQueryDataTableParamModel
    {
        // Database table name
        public String tableName;

        // Request sequence number sent by DataTable, same value must be
        // returned in response
        // sEcho
        public String sEcho;

        // Text used for filtering
        // sSearch
        public String sSearch;

        // Number of records that should be shown in table
        // iDisplayLength
        public int iDisplayLength;

        // First record that should be shown(used for paging)
        // iDisplayStart
        public int iDisplayStart;

        // Number of columns in table
        public int iColumns;

        // Number of columns that are used in sorting
        // iSortingCols
        public int iSortingCols;

        // Index of the column that is used for sorting
        // iSortCol_0
        public int iSortColumnIndex;

        // Sorting direction "asc" or "desc"
        // sSortDir_0
        public String sSortDirection;

        // Comma separated list of column names
        // sColumns
        // iColumns
        public String sColumns;

        // Array of column names that are used for sorting
        // sColumns
        // iColumns
        public String[] aSortColumnNames;

    } // JQueryDataTableParamModel

}
