package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.Map;

/**
 * 通知手機啟動驗證
 * 
 * @author Leo
 *
 */
public class svCancel_Txn_REST_RQ extends BaseRestBean_IDGATE implements Serializable {
	
	private static final long serialVersionUID = -8105756062267887930L;

	private String idgateID; //IDGATE ID
	private String txnID; //
	public String getIdgateID() {
		return idgateID;
	}
	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}
	public String getTxnID() {
		return txnID;
	}
	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}
	
	
	
	
	

}
