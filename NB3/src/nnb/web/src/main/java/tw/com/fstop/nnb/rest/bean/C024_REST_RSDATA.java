package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * C024電文RSDATA
 */
public class C024_REST_RSDATA implements Serializable{
	private static final long serialVersionUID = 7127169312136862734L;
	
	private String TRANSCODE;
	private String CDNO;
	private String CRY;
	private String FUNDAMT;
	private String UNIT;
	private String FUNDACN;
	private String BANKID;
	private String BANKNAME;
	private String TRADEDATE;
	private String TRADETIMEMK;
	private String SHORTTRADE;
	private String SHORTTUNIT;
	private String FUSMON;
	private String TYPE10;
	
	public String getTRANSCODE(){
		return TRANSCODE;
	}
	public void setTRANSCODE(String tRANSCODE){
		TRANSCODE = tRANSCODE;
	}
	public String getCRY(){
		return CRY;
	}
	public void setCRY(String cRY){
		CRY = cRY;
	}
	public String getFUNDAMT(){
		return FUNDAMT;
	}
	public void setFUNDAMT(String fUNDAMT){
		FUNDAMT = fUNDAMT;
	}
	public String getUNIT(){
		return UNIT;
	}
	public void setUNIT(String uNIT){
		UNIT = uNIT;
	}
	public String getFUNDACN(){
		return FUNDACN;
	}
	public void setFUNDACN(String fUNDACN){
		FUNDACN = fUNDACN;
	}
	public String getBANKID(){
		return BANKID;
	}
	public void setBANKID(String bANKID){
		BANKID = bANKID;
	}
	public String getBANKNAME(){
		return BANKNAME;
	}
	public void setBANKNAME(String bANKNAME){
		BANKNAME = bANKNAME;
	}
	public String getTRADEDATE(){
		return TRADEDATE;
	}
	public void setTRADEDATE(String tRADEDATE){
		TRADEDATE = tRADEDATE;
	}
	public String getTRADETIMEMK(){
		return TRADETIMEMK;
	}
	public void setTRADETIMEMK(String tRADETIMEMK){
		TRADETIMEMK = tRADETIMEMK;
	}
	public String getSHORTTRADE(){
		return SHORTTRADE;
	}
	public void setSHORTTRADE(String sHORTTRADE){
		SHORTTRADE = sHORTTRADE;
	}
	public String getSHORTTUNIT(){
		return SHORTTUNIT;
	}
	public void setSHORTTUNIT(String sHORTTUNIT){
		SHORTTUNIT = sHORTTUNIT;
	}
	public String getCDNO() {
		return CDNO;
	}
	public void setCDNO(String cDNO) {
		CDNO = cDNO;
	}
	public String getFUSMON() {
		return FUSMON;
	}
	public void setFUSMON(String fUSMON) {
		FUSMON = fUSMON;
	}
	public String getTYPE10() {
		return TYPE10;
	}
	public void setTYPE10(String tYPE10) {
		TYPE10 = tYPE10;
	}
}