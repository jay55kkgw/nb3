package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N015_REST_RSDATA implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7283990829505452827L;
	
	
	String CHKDATE;	//支票發票日/本票到期日
	String AMTACC;	//票據金額
	String CHKNUM;	//票據號碼
	
	public String getCHKDATE() {
		return CHKDATE;
	}
	public void setCHKDATE(String cHKDATE) {
		CHKDATE = cHKDATE;
	}
	public String getAMTACC() {
		return AMTACC;
	}
	public void setAMTACC(String aMTACC) {
		AMTACC = aMTACC;
	}
	public String getCHKNUM() {
		return CHKNUM;
	}
	public void setCHKNUM(String cHKNUM) {
		CHKNUM = cHKNUM;
	}



}
