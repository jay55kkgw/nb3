package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N926_REST_RSDATA extends BaseRestBean implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5333023174760674466L;

	private String SVACN;
	private String ACN;
	private String BNKCOD;
	private String NAME;
	
	public String getSVACN() {
		return SVACN;
	}
	public void setSVACN(String sVACN) {
		SVACN = sVACN;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getBNKCOD() {
		return BNKCOD;
	}
	public void setBNKCOD(String bNKCOD) {
		BNKCOD = bNKCOD;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	
}
