package tw.com.fstop.nnb.service;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.Fund_Transfer_Service;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.util.StrUtils;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SessionUtil;

/**
 * 黃金存摺-定期定額的Service
 */
@Service
public class Gold_Averaging_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private Fund_Transfer_Service fund_transfer_service;
	
	@Autowired
	private I18n i18n;

	/**
	 * 定期定額查詢輸入頁
	 * 
	 * @param cusidn
	 * @return
	 */
	public BaseResult averaging_query(String cusidn) {
		log.trace("averaging_query Service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// call rest API
			bs = N920_REST(cusidn, GOLD_ACNO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("averaging_query", e);
		}
		return bs;
	}
	/**
	 * 定期定額查詢結果
	 * @param cusidn	統一編號
	 * @param reqParam
	 * @return
	 */
	public BaseResult averaging_query_result(String cusidn, Map<String, String> reqParam) {
		log.trace("averaging_query_result Service(");
		BaseResult bs = null ;
		try {
			bs = new  BaseResult();
			reqParam.put("UID",cusidn);
			//call rest API
			bs = N192_REST(cusidn, reqParam);
			//後製
			if(bs!=null && bs.getResult()) {	
				bs.addData("ACN",reqParam.get("ACN"));
				Map<String, Object> dataMap = (Map) bs.getData();
				List<Map<String, String>> rowListMap = (List<Map<String, String>>)dataMap.get("REC");//滾詳細資料	
				for(Map<String, String> map: rowListMap ) {
					//NumericUtil.fmtAmount(tempTRANSLATE.toString(),2)
					//金額、手續費格式
					BigInteger tempTRANSLATE=new BigInteger(map.get("BFEEAMT"));
					map.put("BFEEAMT",NumericUtil.fmtAmount(tempTRANSLATE.toString(),0));
					tempTRANSLATE=new BigInteger(map.get("AFEEAMT"));
					map.put("AFEEAMT",NumericUtil.fmtAmount(tempTRANSLATE.toString(),0));
					tempTRANSLATE=new BigInteger(map.get("BQTAMT1"));
					map.put("BQTAMT1",NumericUtil.fmtAmount(tempTRANSLATE.toString(),0));
					tempTRANSLATE=new BigInteger(map.get("AQTAMT1"));
					map.put("AQTAMT1",NumericUtil.fmtAmount(tempTRANSLATE.toString(),0));
					tempTRANSLATE=new BigInteger(map.get("BQTAMT2"));
					map.put("BQTAMT2",NumericUtil.fmtAmount(tempTRANSLATE.toString(),0));
					tempTRANSLATE=new BigInteger(map.get("AQTAMT2"));
					map.put("AQTAMT2",NumericUtil.fmtAmount(tempTRANSLATE.toString(),0));
					tempTRANSLATE=new BigInteger(map.get("BQTAMT3"));
					map.put("BQTAMT3",NumericUtil.fmtAmount(tempTRANSLATE.toString(),0));
					tempTRANSLATE=new BigInteger(map.get("AQTAMT3"));
					map.put("AQTAMT3",NumericUtil.fmtAmount(tempTRANSLATE.toString(),0));
					//時間格式
					String temp=DateUtil.convertDate(1,map.get("LTD"),"yyyMMdd","yyyy/MM/dd");
					map.put("LTD",temp);
					temp=DateUtil.formatTime(map.get("TIME"));
					map.put("TIME",temp);
					//來源格式
					if(!((map.get("SOURCE")).equals("")))
					map.put("SOURCE", 	N094translater(Integer.parseInt(map.get("SOURCE"))));
					//交易狀態格式
					if(!((map.get("BSTATUS1")).equals("")))
					map.put("BSTATUS1", N192translater(Integer.parseInt(map.get("BSTATUS1"))));
					if(!((map.get("ASTATUS1")).equals("")))
					map.put("ASTATUS1", N192translater(Integer.parseInt(map.get("ASTATUS1"))));
					if(!((map.get("BSTATUS2")).equals("")))
					map.put("BSTATUS2", N192translater(Integer.parseInt(map.get("BSTATUS2"))));
					if(!((map.get("ASTATUS2")).equals("")))
					map.put("ASTATUS2", N192translater(Integer.parseInt(map.get("ASTATUS2"))));
					if(!((map.get("BSTATUS3")).equals("")))
					map.put("BSTATUS3", N192translater(Integer.parseInt(map.get("BSTATUS3"))));
					if(!((map.get("ASTATUS3")).equals("")))
					map.put("ASTATUS3", N192translater(Integer.parseInt(map.get("ASTATUS3"))));
				}
				dataMap.put("ACN",reqParam.get("ACN"));
				bs.addData("RECJSON",CodeUtil.toJson(dataMap.get("REC")));
			}
			
		} catch (Exception e) {
			log.error("averaging_query_result",e);
		}	
		return  bs;
	}
	public String N094translater(int Status) {
		
		switch(Status) {
		case 1:
			return i18n.getMsg("LB.X1883");//臨櫃
		case 2:
			return i18n.getMsg("LB.X1884");//網銀
		default:
			return "";
		}
	}
	public String N192translater(int Status) {
		switch(Status) {
		case 1:
			return i18n.getMsg("LB.W1573");//LB.W1573 正常扣款
		case 2:
			return i18n.getMsg("LB.W1163");//LB.W1163暫停扣款
		case 3:
			return i18n.getMsg("LB.W1164");//LB.W1164 終止扣款
		case 4:
			return i18n.getMsg("LB.X1885");//連續3次扣款失敗終止
		default:
			return "";
		}
	}
	
	/**
	 * N09301定期定額申購(輸入頁)
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult averaging_purchase(String cusidn, Map<String, String> reqParam) {
		log.trace("averaging_purchase  service");
		BaseResult bs = null;
		BaseResult bs926 = null;
		try {
			bs = new BaseResult();
			bs926 = N926_REST(cusidn, GOLD_RESERVATION_QOERY);
			if(bs926 != null && bs926.getResult()) {
				Map<String, Object> bs926Data = (Map) bs926.getData();
				ArrayList<Map<String, String>> bs926rows = (ArrayList<Map<String, String>>) bs926Data.get("REC");
				
				List<Map<String,String>> recList = new ArrayList();
				List<Map<String,String>> selectList = new ArrayList();
				Map<String,String> svacnMap = new HashMap();
				Map<String,String> processSvacn = new HashMap();
				int count = 0;	
				//過濾異常帳號
				for(int i = 0;i < bs926rows.size();i++) {
					if(bs926rows.get(i).get("SVACN").length() != 11) {
						bs926rows.remove(i);
						i--;
					}
					else {
						if(bs926rows.get(i).get("ACN").length() == 11) {
							count++;
						}
					}
				}
				bs.addData("COUNT", count);
				//檢查是否有黃金存摺網路交易帳號
				if(count <= 0) {
					bs.setResult(true);
					return bs;
				}
				if(StrUtils.isEmpty(reqParam.get("agreeflag"))) {
					bs.setResult(true);
					return bs;
				}
				processSvacn.put(bs926rows.get(0).get("SVACN"), bs926rows.get(0).get("SVACN"));
				//處理重複帳號
				for(int i = 0;i < count; i++ ) {
					if(!processSvacn.containsKey(bs926rows.get(i).get("SVACN"))) {
						processSvacn.put(bs926rows.get(i).get("SVACN"), bs926rows.get(i).get("SVACN"));
					}
				}
				log.trace("Processed SVACN>>>>>{}",processSvacn);
				//將處理後的帳號放入List
				for ( String key : processSvacn.keySet()) {
					svacnMap = new HashMap();
					svacnMap.put("SVACN", processSvacn.get(key));
					selectList.add(svacnMap);
				}
				bs.addData("SVACN", selectList);
				bs.setResult(true);
			}	
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * N09301定期定額申購(確認頁)
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult averaging_purchase_confirm(String cusidn, Map<String, String> reqParam) {
		log.trace("averaging_purchase_confirm  service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs.addData("FGTXWAY", reqParam.get("FGTXWAY"));
			bs.addData("SVACN", reqParam.get("SVACN"));
			bs.addData("ACN", reqParam.get("ACN"));
			bs.addData("action", reqParam.get("action"));
			bs.addData("DPMYEMAIL", reqParam.get("DPMYEMAIL"));
			bs.addData("ADOPID", reqParam.get("ADOPID"));
			bs.addData("urlPath", reqParam.get("urlPath"));
			bs.addData("AMT_06", reqParam.get("AMT_06"));
			bs.addData("AMT_16", reqParam.get("AMT_16"));
			bs.addData("AMT_26", reqParam.get("AMT_26"));
			
			if(reqParam.get("AMT_06").equals("")) {
				bs.addData("AMT_06_1", "0");
			}
			else {
				bs.addData("AMT_06_1", NumericUtil.formatNumberString((String)reqParam.get("AMT_06"), 0));
			}
			
			if(reqParam.get("AMT_16").equals("")) {
				bs.addData("AMT_16_1", "0");
			}
			else {
				bs.addData("AMT_16_1", NumericUtil.formatNumberString((String)reqParam.get("AMT_16"), 0));
			}
			
			if(reqParam.get("AMT_26").equals("")) {
				bs.addData("AMT_26_1", "0");
			}
			else {
				bs.addData("AMT_26_1", NumericUtil.formatNumberString((String)reqParam.get("AMT_26"), 0));
			}
			
			bs.setResult(true);
			log.trace("bsData Result>>>{}", bs.getData());
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * N09301定期定額申購(結果頁)
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult averaging_purchase_result(String cusidn, Map<String, String> reqParam) {
		log.trace(ESAPIUtil.vaildLog("averaging_purchase_result service>>>{}"+ CodeUtil.toJson(reqParam)));
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = N093_01_REST(cusidn, reqParam);
			log.trace("bsData>>>{}",bs.getData());
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				try {
					bs.addData("AMT_06_1", NumericUtil.formatNumberString((String)bsData.get("AMT06"),0));
					bs.addData("AMT_16_1", NumericUtil.formatNumberString((String)bsData.get("AMT16"),0));
					bs.addData("AMT_26_1", NumericUtil.formatNumberString((String)bsData.get("AMT26"),0));
				}
				catch(Exception e) {
					log.error("do fmtAmount error", e);
				}
			}	
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("averaging_purchase_result service error", e);
		}
		return bs;
	}
	
	/**
	 * N09302定期定額變更(輸入頁)
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult averaging_alter(String cusidn, Map<String, String> reqParam) {
		log.trace("averaging_alter  service");
		BaseResult bs = null;
		BaseResult bs920 = null;
		try {
			bs = new BaseResult();
			bs920 = N920_REST(cusidn, GOLD_RESERVATION_QOERY_N920);
			if(bs920 != null && bs920.getResult()) {
				Map<String, Object> bs920Data = (Map) bs920.getData();
				ArrayList<Map<String, String>> bs920rows = (ArrayList<Map<String, String>>) bs920Data.get("REC");
				
				List<Map<String,String>> recList = new ArrayList();
				List<Map<String,String>> selectList = new ArrayList();
				Map<String,String> svacnMap = new HashMap();
				Map<String,String> processSvacn = new HashMap();
				int count = 0;	
				//過濾異常帳號
				for(int i = 0;i < bs920rows.size();i++) {
					if(bs920rows.get(i).get("ACN").length() != 11) {
						bs920rows.remove(i);
						i--;
					}
					else {
						count++;
					}
				}
				bs.addData("COUNT", count);
				//檢查是否有黃金存摺網路交易帳號
				if(count <= 0) {
					bs.setResult(true);
					return bs;
				}
				processSvacn.put(bs920rows.get(0).get("ACN"), bs920rows.get(0).get("ACN"));
				//處理重複帳號
				for(int i = 0;i < count; i++ ) {
					if(!processSvacn.containsKey(bs920rows.get(i).get("ACN"))) {
						processSvacn.put(bs920rows.get(i).get("ACN"), bs920rows.get(i).get("ACN"));
					}
				}
				log.trace("Processed ACN>>>>>{}",processSvacn);
				//將處理後的帳號放入List
				for ( String key : processSvacn.keySet()) {
					svacnMap = new HashMap();
					svacnMap.put("ACN", processSvacn.get(key));
					selectList.add(svacnMap);
				}
				bs.addData("ACN", selectList);
				bs.setResult(true);
			}	
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * N09302定期定額變更(確認頁)
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult averaging_alter_confirm(String cusidn, Map<String, String> reqParam) {
		log.trace("averaging_alter_confirm  service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs.addData("urlPath", reqParam.get("urlPath"));
			bs.addData("ADOPID", reqParam.get("ADOPID"));
			bs.addData("action", reqParam.get("action"));
			bs.addData("DPMYEMAIL", reqParam.get("DPMYEMAIL"));
			bs.addData("ACN", reqParam.get("ACN"));
			bs.addData("SVACN", reqParam.get("SVACN"));
			bs.addData("FGTXWAY", reqParam.get("FGTXWAY"));
			bs.addData("AMT_06_N", reqParam.get("AMT_06_N"));
			bs.addData("AMT_16_N", reqParam.get("AMT_16_N"));
			bs.addData("AMT_26_N", reqParam.get("AMT_26_N"));
			bs.addData("AMT_06_O", reqParam.get("AMT_06_O"));
			bs.addData("AMT_16_O", reqParam.get("AMT_16_O"));
			bs.addData("AMT_26_O", reqParam.get("AMT_26_O"));
			bs.addData("FLAG_06_O", reqParam.get("FLAG_06_O"));
			bs.addData("FLAG_16_O", reqParam.get("FLAG_16_O"));
			bs.addData("FLAG_26_O", reqParam.get("FLAG_26_O"));
			bs.addData("PAYSTATUS_06", reqParam.get("PAYSTATUS_06"));
			bs.addData("PAYSTATUS_16", reqParam.get("PAYSTATUS_16"));
			bs.addData("PAYSTATUS_26", reqParam.get("PAYSTATUS_26"));
			
			if(reqParam.get("FLAG_06_O").equals("1")){
				//正常扣款
				bs.addData("FLAG_06_O1", "LB.W1573");
			}
			else if(reqParam.get("FLAG_06_O").equals("2")){
				//暫停扣款
				bs.addData("FLAG_06_O1", "LB.W1163");
			}
			else if(reqParam.get("FLAG_06_O").equals("3")){
				//終止扣款
				bs.addData("FLAG_06_O1", "LB.W1164");
			}
			else {
				bs.addData("FLAG_06_O1", "");
			}
			if(reqParam.get("FLAG_16_O").equals("1")){
				//正常扣款
				bs.addData("FLAG_16_O1", "LB.W1573");
			}
			else if(reqParam.get("FLAG_16_O").equals("2")){
				//暫停扣款
				bs.addData("FLAG_16_O1", "LB.W1163");
			}
			else if(reqParam.get("FLAG_16_O").equals("3")){
				//終止扣款
				bs.addData("FLAG_16_O1", "LB.W1164");
			}
			else {
				bs.addData("FLAG_16_O1", "");
			}
			if(reqParam.get("FLAG_26_O").equals("1")){
				//正常扣款
				bs.addData("FLAG_26_O1", "LB.W1573");
			}
			else if(reqParam.get("FLAG_26_O").equals("2")){
				//暫停扣款
				bs.addData("FLAG_26_O1", "LB.W1163");
			}
			else if(reqParam.get("FLAG_26_O").equals("3")){
				//終止扣款
				bs.addData("FLAG_26_O1", "LB.W1164");
			}
			else {
				bs.addData("FLAG_26_O1", "");
			}
			if(StrUtils.isEmpty(reqParam.get("AMT_06_N"))) {
				bs.addData("AMT_06_N1", "0");
			}
			else {
				bs.addData("AMT_06_N1", NumericUtil.formatNumberString(reqParam.get("AMT_06_N"),0));
			}
			if(StrUtils.isEmpty(reqParam.get("AMT_16_N"))) {
				bs.addData("AMT_16_N1", "0");
			}
			else {
				bs.addData("AMT_16_N1", NumericUtil.formatNumberString(reqParam.get("AMT_16_N"),0));
			}
			if(StrUtils.isEmpty(reqParam.get("AMT_26_N"))) {
				bs.addData("AMT_26_N1", "0");
			}
			else {
				bs.addData("AMT_26_N1", NumericUtil.formatNumberString(reqParam.get("AMT_26_N"),0));
			}
			if(StrUtils.isEmpty(reqParam.get("AMT_06_O"))) {
				bs.addData("AMT_06_O1", "0");
			}
			else {
				bs.addData("AMT_06_O1", NumericUtil.formatNumberString(reqParam.get("AMT_06_O"),0));
			}
			if(StrUtils.isEmpty(reqParam.get("AMT_16_O"))) {
				bs.addData("AMT_16_O1", "0");
			}
			else {
				bs.addData("AMT_16_O1", NumericUtil.formatNumberString(reqParam.get("AMT_16_O"),0));
			}
			if(StrUtils.isEmpty(reqParam.get("AMT_26_O"))) {
				bs.addData("AMT_26_O1", "0");
			}
			else {
				bs.addData("AMT_26_O1", NumericUtil.formatNumberString(reqParam.get("AMT_26_O"),0));
			}
			if(StrUtils.isEmpty(reqParam.get("CHA_06"))) {
				bs.addData("CHA_06", "");
			}
			else {
				bs.addData("CHA_06", reqParam.get("CHA_06"));
			}
			if(StrUtils.isEmpty(reqParam.get("CHA_16"))) {
				bs.addData("CHA_16", "");
			}
			else {
				bs.addData("CHA_16", reqParam.get("CHA_16"));
			}
			if(StrUtils.isEmpty(reqParam.get("CHA_26"))) {
				bs.addData("CHA_26", "");
			}
			else {
				bs.addData("CHA_26", reqParam.get("CHA_26"));
			}
			if(StrUtils.isEmpty(reqParam.get("DATE_06"))) {
				bs.addData("DATE_06", "");
			}
			else {
				bs.addData("DATE_06", reqParam.get("DATE_06"));
			}
			if(StrUtils.isEmpty(reqParam.get("DATE_16"))) {
				bs.addData("DATE_16", "");
			}
			else {
				bs.addData("DATE_16", reqParam.get("DATE_16"));
			}
			if(StrUtils.isEmpty(reqParam.get("DATE_26"))) {
				bs.addData("DATE_26", "");
			}
			else {
				bs.addData("DATE_26", reqParam.get("DATE_26"));
			}
			if(StrUtils.isEmpty(reqParam.get("PAYSTATUS_06"))){
				bs.addData("PAYSTATUS_061", "");
			}
			else if(reqParam.get("PAYSTATUS_06").equals("1")){
				//正常扣款
				bs.addData("PAYSTATUS_061", "LB.W1573");
			}
			else if(reqParam.get("PAYSTATUS_06").equals("2")){
				//暫停扣款
				bs.addData("PAYSTATUS_061", "LB.W1163");
			}
			else if(reqParam.get("PAYSTATUS_06").equals("3")){
				//終止扣款
				bs.addData("PAYSTATUS_061", "LB.W1164");
			}
			if(StrUtils.isEmpty(reqParam.get("PAYSTATUS_16"))){
				bs.addData("PAYSTATUS_161", "");
			}
			else if(reqParam.get("PAYSTATUS_16").equals("1")){
				//正常扣款
				bs.addData("PAYSTATUS_161", "LB.W1573");
			}
			else if(reqParam.get("PAYSTATUS_16").equals("2")){
				//暫停扣款
				bs.addData("PAYSTATUS_161", "LB.W1163");
			}
			else if(reqParam.get("PAYSTATUS_16").equals("3")){
				//終止扣款
				bs.addData("PAYSTATUS_161", "LB.W1164");
			}
			if(StrUtils.isEmpty(reqParam.get("PAYSTATUS_26"))){
				bs.addData("PAYSTATUS_261", "");
			}
			else if(reqParam.get("PAYSTATUS_26").equals("1")){
				//正常扣款
				bs.addData("PAYSTATUS_261", "LB.W1573");
			}
			else if(reqParam.get("PAYSTATUS_26").equals("2")){
				//暫停扣款
				bs.addData("PAYSTATUS_261", "LB.W1163");
			}
			else if(reqParam.get("PAYSTATUS_26").equals("3")){
				//終止扣款
				bs.addData("PAYSTATUS_261", "LB.W1164");
			}
			
			bs.setResult(true);
			log.trace("bsData Result>>>{}", bs.getData());
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * N09302定期定額變更(結果頁)
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult averaging_alter_result(String cusidn, Map<String, String> reqParam) {
		log.trace(ESAPIUtil.vaildLog("averaging_alter_result service>>>{}"+ CodeUtil.toJson(reqParam)));
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = N093_02_REST(cusidn, reqParam, "N09302");
			log.trace("bsData>>>{}",bs.getData());
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				try {
					bs.addData("AMT_06_O", reqParam.get("AMT_06_O"));
					bs.addData("AMT_16_O", reqParam.get("AMT_16_O"));
					bs.addData("AMT_26_O", reqParam.get("AMT_26_O"));
					bs.addData("FLAG_06_O", reqParam.get("FLAG_06_O"));
					bs.addData("FLAG_16_O", reqParam.get("FLAG_16_O"));
					bs.addData("FLAG_26_O", reqParam.get("FLAG_26_O"));
					bs.addData("AMT_06_N", NumericUtil.formatNumberString((String)bsData.get("AMT06"),0));
					bs.addData("AMT_16_N", NumericUtil.formatNumberString((String)bsData.get("AMT16"),0));
					bs.addData("AMT_26_N", NumericUtil.formatNumberString((String)bsData.get("AMT26"),0));
					if(bsData.get("DATE_06").equals("N")) {
						bs.addData("DATE_06", "");
					}
					if(bsData.get("DATE_16").equals("N")) {
						bs.addData("DATE_16", "");
					}
					if(bsData.get("DATE_26").equals("N")) {
						bs.addData("DATE_26", "");
					}
					if(StrUtils.isEmpty(reqParam.get("PAYSTATUS_06"))){
						bs.addData("PAYSTATUS_06", "");
					}
					else if(reqParam.get("PAYSTATUS_06").equals("1")){
						//正常扣款
						bs.addData("PAYSTATUS_06", "LB.W1573");
					}
					else if(reqParam.get("PAYSTATUS_06").equals("2")){
						//暫停扣款
						bs.addData("PAYSTATUS_06", "LB.W1163");
					}
					else if(reqParam.get("PAYSTATUS_06").equals("3")){
						//終止扣款
						bs.addData("PAYSTATUS_06", "LB.W1164");
					}
					if(StrUtils.isEmpty(reqParam.get("PAYSTATUS_16"))){
						bs.addData("PAYSTATUS_16", "");
					}
					else if(reqParam.get("PAYSTATUS_16").equals("1")){
						//正常扣款
						bs.addData("PAYSTATUS_16", "LB.W1573");
					}
					else if(reqParam.get("PAYSTATUS_16").equals("2")){
						//暫停扣款
						bs.addData("PAYSTATUS_16", "LB.W1163");
					}
					else if(reqParam.get("PAYSTATUS_16").equals("3")){
						//終止扣款
						bs.addData("PAYSTATUS_16", "LB.W1164");
					}
					if(StrUtils.isEmpty(reqParam.get("PAYSTATUS_26"))){
						bs.addData("PAYSTATUS_26", "");
					}
					else if(reqParam.get("PAYSTATUS_26").equals("1")){
						//正常扣款
						bs.addData("PAYSTATUS_26", "LB.W1573");
					}
					else if(reqParam.get("PAYSTATUS_26").equals("2")){
						//暫停扣款
						bs.addData("PAYSTATUS_26", "LB.W1163");
					}
					else if(reqParam.get("PAYSTATUS_26").equals("3")){
						//終止扣款
						bs.addData("PAYSTATUS_26", "LB.W1164");
					}
					log.trace("BSDATA BACK >>>>>{}",bs.getData());
				}
				catch(Exception e) {
					log.error("do fmtAmount error", e);
				}
			}	
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("averaging_alter_result service error", e);
		}
		return bs;
	}
	
	/**
	 * 取得帳號資料
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	public List<Map<String, String>> get_acn_data(String acn, String cusidn) {
		log.trace(ESAPIUtil.vaildLog("get_acn_data...>>>" + acn));
		
		List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
		BaseResult bs = new BaseResult();
		try {
			Map<String, String> redata = new HashMap();
			redata.put("UID", cusidn);
			redata.put("ACN", acn);
			redata.put("TRNCOD", "00");			
			redata.put("FGTXWAY", "0");
			redata.put("N950PASSWORD", "");
			redata.put("AMT_06", "0");
			redata.put("AMT_16", "0");
			redata.put("AMT_26", "0");
			
			bs = N093_02_REST(cusidn, redata, "N09302_Q");
			log.trace("Get bsdata N093_02>>>{}",bs.getData());
			Map<String, String>datamap = new HashMap();
			if(bs!=null && bs.getResult()) {
				datamap = (Map) bs.getData();
				log.trace("DATAMAP>>>>>{}",datamap.toString());
				bs.addData("AMT_06",datamap.get("AMT06"));
				bs.addData("AMT_16",datamap.get("AMT16"));
				bs.addData("AMT_26",datamap.get("AMT26"));
				bs.addData("AMT_06_DESC",NumericUtil.formatNumberString(datamap.get("AMT06"),0));
				bs.addData("AMT_16_DESC",NumericUtil.formatNumberString(datamap.get("AMT16"),0));
				bs.addData("AMT_26_DESC",NumericUtil.formatNumberString(datamap.get("AMT26"),0));
				if(StrUtils.isEmpty(datamap.get("FLAG_06"))) {
					bs.addData("FLAG_06_DESC"," ");
				}
				else if(datamap.get("FLAG_06").equals("1")) {
					//正常扣款
					bs.addData("FLAG_06_DESC",i18n.getMsg("LB.W1573"));
				}
				else if(datamap.get("FLAG_06").equals("2")) {
					//暫停扣款
					bs.addData("FLAG_06_DESC",i18n.getMsg("LB.W1163"));
				}
				else {
					//終止扣款
					bs.addData("FLAG_06_DESC",i18n.getMsg("LB.W1164"));
				}
				
				if(StrUtils.isEmpty(datamap.get("FLAG_16"))) {
					bs.addData("FLAG_16_DESC"," ");
				}
				else if(datamap.get("FLAG_16").equals("1")) {
					//正常扣款
					bs.addData("FLAG_16_DESC",i18n.getMsg("LB.W1573"));
				}
				else if(datamap.get("FLAG_16").equals("2")) {
					//暫停扣款
					bs.addData("FLAG_16_DESC",i18n.getMsg("LB.W1163"));
				}
				else {
					//終止扣款
					bs.addData("FLAG_16_DESC",i18n.getMsg("LB.W1164"));
				}
				
				if(StrUtils.isEmpty(datamap.get("FLAG_26"))) {
					bs.addData("FLAG_26_DESC"," ");
				}
				else if(datamap.get("FLAG_26").equals("1")) {
					//正常扣款
					bs.addData("FLAG_26_DESC",i18n.getMsg("LB.W1573"));
				}
				else if(datamap.get("FLAG_26").equals("2")) {
					//暫停扣款
					bs.addData("FLAG_26_DESC",i18n.getMsg("LB.W1163"));
				}
				else {
					//終止扣款
					bs.addData("FLAG_26_DESC",i18n.getMsg("LB.W1164"));
				}
				log.trace("BS DATA>>>>>{}",bs.getData());
				
				
			}
			else {
				bs.addData("MSGCOD", bs.getMsgCode());
			}
			Map<String, String>listmap = (Map) bs.getData();
			resultList.add(listmap);
			log.trace("resultList>>>{}",resultList);
		} 
		catch (Exception e) {
			log.error("getGoldAcn.error: {}", e);
		}
		
		return resultList;
	}
	
	/**
	 * 檢核KYC
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	public BaseResult checkKYC(String cusidn, Map<String, String> reqParam, String kyc,Model model) {
		log.trace("checkKYC...>>>" + kyc);
		
		BaseResult bs = new BaseResult();
		BaseResult n927BS = new BaseResult();
		
		try {
			n927BS = fund_transfer_service.getN927Data(cusidn);
			
			if(n927BS == null || !n927BS.getResult()){
				return n927BS;
			}
			Map<String,Object> n927BSData = (Map<String,Object>)n927BS.getData();
			log.debug("n927BSData={}",n927BSData);
			
			String RISK = n927BSData.get("RISK") == null ? "" : (String)n927BSData.get("RISK");
			log.debug("RISK={}",RISK);
			
			//問卷填寫日期
			String GETLTD = n927BSData.get("GETLTD") == null ? "" : (String)n927BSData.get("GETLTD");
			log.debug("GETLTD={}",GETLTD);
			
			String time1 = "";
			String time2 = "";
			long day = 0;
			if(!GETLTD.equals("")){
				time1 = new SimpleDateFormat("yyyy/MM/dd").format(new Date());
				log.debug("time1={}",time1);
				String GETLTDString = Integer.valueOf(GETLTD.substring(0,3)) + 1911 + "";
				GETLTDString = GETLTDString + GETLTD.substring(3);
				log.debug("GETLTDString={}",GETLTDString);
				time2 = GETLTDString.substring(0,4) + "/" + GETLTDString.substring(4,6) + "/" + GETLTDString.substring(6,8);
				log.debug("time2={}",time2);
				
				//算出距離上次問卷填寫天數
				day = fund_transfer_service.getRangeDay(time1,time2);
			}
			
			String KYC = reqParam.get("KYC");
			log.debug(ESAPIUtil.vaildLog("KYC={}"+KYC));
			
			if("PASS".equals(KYC)){
				SessionUtil.addAttribute(model,SessionUtil.KYC,KYC);
			}
			
			//找出是否有KYC PASS的值
			String KYCSession = (String)SessionUtil.getAttribute(model,SessionUtil.KYC,null);
			log.debug("KYCSession={}",KYCSession);
			
			//問卷版本日期
			String KYCDate = fund_transfer_service.getKYCDate();
			
			if(KYCDate == null){
				KYCDate = "";
			}
			n927BS.addData("Gd_InvAttr", "0");
			if(KYCSession == null){
				if((RISK.equals("") || (!GETLTD.equals("") && day >= 365) || (!KYCDate.equals("") && Integer.parseInt(KYCDate) > Integer.parseInt(GETLTD)))){
					//自然人、法人版，無投資屬性或問卷填寫日期已滿一年或問卷版本日期大於問卷填寫日期	
					if(day >= 365){//滿一年
						model.addAttribute("ALERTTYPE","2");
					}
					else if(RISK.equals("")){//無投資屬性
						model.addAttribute("ALERTTYPE","3");
					}
					n927BS.addData("Gd_InvAttr", "N");
		    	}
			}
		} 
		catch (Exception e) {
			log.error("checkKYC.error: {}", e);
		}
		return n927BS;
	}
}
		
