package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;


public class N100_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5370773529503299761L;
	
	@SerializedName(value = "POSTCOD2")
	private String POSTCOD2;
	@SerializedName(value = "CTTADR")
	private String CTTADR;	
	@SerializedName(value = "ARACOD2")
	private String ARACOD2;
	@SerializedName(value = "TELNUM2")
	private String TELNUM2;
	@SerializedName(value = "TELEXT")
	private String TELEXT;
	
	@SerializedName(value = "ARACOD")
	private String ARACOD;
	@SerializedName(value = "TELNUM")
	private String TELNUM;
	@SerializedName(value = "MOBTEL")
	private String MOBTEL;
	
	@Override
	public Map<String, String> coverMap(){
		LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", "變更通訊地址／電話");
		result.put("交易類型", "-");
		result.put("郵遞區號", this.POSTCOD2);
		result.put("通訊地址", this.CTTADR);
		result.put("公司電話", this.ARACOD2+"-"+this.TELNUM2);
		result.put("居住電話", this.ARACOD+"-"+this.TELNUM);
		result.put("行動電話", this.MOBTEL);
		return result;
	}

}
