package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N070A_IDGATE_DATA implements Serializable{
	
	// 應繳總金額
	@SerializedName(value = "AMOUNT")
	private String AMOUNT;

	// 銷帳帳號
	@SerializedName(value = "TSFACN")
	private String TSFACN;
	
	// 銷帳帳號
	@SerializedName(value = "ACN")
	private String ACN;
	
	public String getAMOUNT() {
		return AMOUNT;
	}

	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}

	public String getTSFACN() {
		return TSFACN;
	}

	public void setTSFACN(String tSFACN) {
		TSFACN = tSFACN;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	
}
