package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N100_REST_RQ extends BaseRestBean_OLS implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4533866679017586227L;
	
	
	private String CUSIDN;//使用者帳號
	private String ACN;//帳號
	private String TYPE;//類別
	private String UPDATE;//是否更新聯行資料
	private String KIND;//交易機制	
	private String SelectedRecord;//勾選資料
	private String ModifyData;//勾選資料
	private String ADOPID;

	//晶片金融卡
	private	String	CHIP_ACN;
	private	String	ACNNO;
	private	String	FGTXWAY;
	private	String	iSeqNo;
	private	String	TAC;
	private	String	ISSUER;
	private	String	TRMID;
	private	String	pkcs7Sign;
	
	// 20210201 同步NB2.5 MS層 N100.java 需打C113 , 要有IP,Email , 固新增
	private String IP ;
	private String EMAIL ;
	//IDGATE
	private String sessionID;
	private String idgateID;
	private String txnID;
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}	
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getUPDATE() {
		return UPDATE;
	}
	public void setUPDATE(String uPDATE) {
		UPDATE = uPDATE;
	}
	public String getKIND() {
		return KIND;
	}
	public void setKIND(String kIND) {
		KIND = kIND;
	}
	public String getSelectedRecord() {
		return SelectedRecord;
	}
	public void setSelectedRecord(String selectedRecord) {
		SelectedRecord = selectedRecord;
	}
	public String getModifyData() {
		return ModifyData;
	}
	public void setModifyData(String modifyData) {
		ModifyData = modifyData;
	}	
	public String getCHIP_ACN() {
		return CHIP_ACN;
	}
	public void setCHIP_ACN(String cHIP_ACN) {
		CHIP_ACN = cHIP_ACN;
	}
	public String getACNNO() {
		return ACNNO;
	}
	public void setACNNO(String aCNNO) {
		ACNNO = aCNNO;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getiSeqNo() {
		return iSeqNo;
	}
	public void setiSeqNo(String iSeqNo) {
		this.iSeqNo = iSeqNo;
	}
	public String getTAC() {
		return TAC;
	}
	public void setTAC(String tAC) {
		TAC = tAC;
	}
	public String getISSUER() {
		return ISSUER;
	}
	public void setISSUER(String iSSUER) {
		ISSUER = iSSUER;
	}
	public String getTRMID() {
		return TRMID;
	}
	public void setTRMID(String tRMID) {
		TRMID = tRMID;
	}
	public String getPkcs7Sign() {
		return pkcs7Sign;
	}
	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	public String getEMAIL() {
		return EMAIL;
	}
	public void setEMAIL(String eMAIL) {
		EMAIL = eMAIL;
	}
	public String getSessionID() {
		return sessionID;
	}
	public String getIdgateID() {
		return idgateID;
	}
	public String getTxnID() {
		return txnID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}
	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}
}
