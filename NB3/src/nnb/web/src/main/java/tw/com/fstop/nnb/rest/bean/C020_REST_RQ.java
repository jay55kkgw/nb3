package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * C020REST，含C020及C026電文RQ
 */
public class C020_REST_RQ extends BaseRestBean_FUND implements Serializable{
	private static final long serialVersionUID = 6868861910004819383L;
	private String CUSIDN;
	private String FGTXWAY;
	private String PINNEW;
	private String TYPE;
	private String TRANSCODE;
	private String COUNTRYTYPE;
	private String TRADEDATE;
	private String AMT3;
	private String FCA2;
	private String AMT5;
	private String PAYTYPE;
	//信用卡付款卡號
	private String FUNDACN;
	private String OUTACN;
	private String INTSACN;
	private String BILLSENDMODE;
	private String FCAFEE;
	private String SSLTXNO;
	private String PAYDAY1;
	private String PAYDAY2;
	private String PAYDAY3;
	private String BRHCOD;
	private String CUTTYPE;
	private String CRY1;
	private String HTELPHONE;
	private String DBDATE;
	private String SALESNO;
	private String STOP;
	private String YIELD;
	private String PAYDAY4;
	private String PAYDAY5;
	private String PAYDAY6;
	private String MIP;
	private String PRO;
	private String KYCDATE;
	private String WEAK;
	private String RSKATT;
	private String RRSK;
	private String PAYDAY7;
	private String PAYDAY8;
	private String PAYDAY9;
	private String FDAGREEFLAG;
	private String FDNOTICETYPE;
	private String FDPUBLICTYPE;
	private String DPMYEMAIL;
	private String CMTRMAIL;
	private String XFLAG;
	private String IP;
	private String NUM;
	private String SLSNO; //轉介人員代號
	private String KYCNO; //KYC評估人員代號
	private String OFLAG; //是否填高齡聲明書(_/Y)
	
	public String getXFLAG() {
		return XFLAG;
	}
	public void setXFLAG(String xFLAG) {
		XFLAG = xFLAG;
	}

	public String getNUM() {
		return NUM;
	}
	public void setNUM(String nUM) {
		NUM = nUM;
	}
	//for txnlog
	private String ADOPID = "C017";
	
	public String getCUSIDN(){
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN){
		CUSIDN = cUSIDN;
	}
	public String getFGTXWAY(){
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY){
		FGTXWAY = fGTXWAY;
	}
	public String getPINNEW(){
		return PINNEW;
	}
	public void setPINNEW(String pINNEW){
		PINNEW = pINNEW;
	}
	public String getTYPE(){
		return TYPE;
	}
	public void setTYPE(String tYPE){
		TYPE = tYPE;
	}
	public String getTRANSCODE(){
		return TRANSCODE;
	}
	public void setTRANSCODE(String tRANSCODE){
		TRANSCODE = tRANSCODE;
	}
	public String getCOUNTRYTYPE(){
		return COUNTRYTYPE;
	}
	public void setCOUNTRYTYPE(String cOUNTRYTYPE){
		COUNTRYTYPE = cOUNTRYTYPE;
	}
	public String getTRADEDATE(){
		return TRADEDATE;
	}
	public void setTRADEDATE(String tRADEDATE){
		TRADEDATE = tRADEDATE;
	}
	public String getAMT3(){
		return AMT3;
	}
	public void setAMT3(String aMT3){
		AMT3 = aMT3;
	}
	public String getFCA2(){
		return FCA2;
	}
	public void setFCA2(String fCA2){
		FCA2 = fCA2;
	}
	public String getAMT5(){
		return AMT5;
	}
	public void setAMT5(String aMT5){
		AMT5 = aMT5;
	}
	public String getPAYTYPE(){
		return PAYTYPE;
	}
	public void setPAYTYPE(String pAYTYPE){
		PAYTYPE = pAYTYPE;
	}
	public String getFUNDACN(){
		return FUNDACN;
	}
	public void setFUNDACN(String fUNDACN){
		FUNDACN = fUNDACN;
	}
	public String getOUTACN(){
		return OUTACN;
	}
	public void setOUTACN(String oUTACN){
		OUTACN = oUTACN;
	}
	public String getINTSACN(){
		return INTSACN;
	}
	public void setINTSACN(String iNTSACN){
		INTSACN = iNTSACN;
	}
	public String getBILLSENDMODE(){
		return BILLSENDMODE;
	}
	public void setBILLSENDMODE(String bILLSENDMODE){
		BILLSENDMODE = bILLSENDMODE;
	}
	public String getFCAFEE(){
		return FCAFEE;
	}
	public void setFCAFEE(String fCAFEE){
		FCAFEE = fCAFEE;
	}
	public String getSSLTXNO(){
		return SSLTXNO;
	}
	public void setSSLTXNO(String sSLTXNO){
		SSLTXNO = sSLTXNO;
	}
	public String getPAYDAY1(){
		return PAYDAY1;
	}
	public void setPAYDAY1(String pAYDAY1){
		PAYDAY1 = pAYDAY1;
	}
	public String getPAYDAY2(){
		return PAYDAY2;
	}
	public void setPAYDAY2(String pAYDAY2){
		PAYDAY2 = pAYDAY2;
	}
	public String getPAYDAY3(){
		return PAYDAY3;
	}
	public void setPAYDAY3(String pAYDAY3){
		PAYDAY3 = pAYDAY3;
	}
	public String getBRHCOD(){
		return BRHCOD;
	}
	public void setBRHCOD(String bRHCOD){
		BRHCOD = bRHCOD;
	}
	public String getCUTTYPE(){
		return CUTTYPE;
	}
	public void setCUTTYPE(String cUTTYPE){
		CUTTYPE = cUTTYPE;
	}
	public String getCRY1(){
		return CRY1;
	}
	public void setCRY1(String cRY1){
		CRY1 = cRY1;
	}
	public String getHTELPHONE(){
		return HTELPHONE;
	}
	public void setHTELPHONE(String hTELPHONE){
		HTELPHONE = hTELPHONE;
	}
	public String getDBDATE(){
		return DBDATE;
	}
	public void setDBDATE(String dBDATE){
		DBDATE = dBDATE;
	}
	public String getSALESNO(){
		return SALESNO;
	}
	public void setSALESNO(String sALESNO){
		SALESNO = sALESNO;
	}
	public String getSTOP(){
		return STOP;
	}
	public void setSTOP(String sTOP){
		STOP = sTOP;
	}
	public String getYIELD(){
		return YIELD;
	}
	public void setYIELD(String yIELD){
		YIELD = yIELD;
	}
	public String getPAYDAY4(){
		return PAYDAY4;
	}
	public void setPAYDAY4(String pAYDAY4){
		PAYDAY4 = pAYDAY4;
	}
	public String getPAYDAY5(){
		return PAYDAY5;
	}
	public void setPAYDAY5(String pAYDAY5){
		PAYDAY5 = pAYDAY5;
	}
	public String getPAYDAY6(){
		return PAYDAY6;
	}
	public void setPAYDAY6(String pAYDAY6){
		PAYDAY6 = pAYDAY6;
	}
	public String getMIP(){
		return MIP;
	}
	public void setMIP(String mIP){
		MIP = mIP;
	}
	public String getPRO(){
		return PRO;
	}
	public void setPRO(String pRO){
		PRO = pRO;
	}
	public String getKYCDATE(){
		return KYCDATE;
	}
	public void setKYCDATE(String kYCDATE){
		KYCDATE = kYCDATE;
	}
	public String getWEAK(){
		return WEAK;
	}
	public void setWEAK(String wEAK){
		WEAK = wEAK;
	}
	public String getRSKATT(){
		return RSKATT;
	}
	public void setRSKATT(String rSKATT){
		RSKATT = rSKATT;
	}
	public String getRRSK(){
		return RRSK;
	}
	public void setRRSK(String rRSK){
		RRSK = rRSK;
	}
	public String getPAYDAY7(){
		return PAYDAY7;
	}
	public void setPAYDAY7(String pAYDAY7){
		PAYDAY7 = pAYDAY7;
	}
	public String getPAYDAY8(){
		return PAYDAY8;
	}
	public void setPAYDAY8(String pAYDAY8){
		PAYDAY8 = pAYDAY8;
	}
	public String getPAYDAY9(){
		return PAYDAY9;
	}
	public void setPAYDAY9(String pAYDAY9){
		PAYDAY9 = pAYDAY9;
	}
	public String getFDAGREEFLAG(){
		return FDAGREEFLAG;
	}
	public void setFDAGREEFLAG(String fDAGREEFLAG){
		FDAGREEFLAG = fDAGREEFLAG;
	}
	public String getFDNOTICETYPE(){
		return FDNOTICETYPE;
	}
	public void setFDNOTICETYPE(String fDNOTICETYPE){
		FDNOTICETYPE = fDNOTICETYPE;
	}
	public String getFDPUBLICTYPE(){
		return FDPUBLICTYPE;
	}
	public void setFDPUBLICTYPE(String fDPUBLICTYPE){
		FDPUBLICTYPE = fDPUBLICTYPE;
	}
	public String getDPMYEMAIL(){
		return DPMYEMAIL;
	}
	public void setDPMYEMAIL(String dPMYEMAIL){
		DPMYEMAIL = dPMYEMAIL;
	}
	public String getCMTRMAIL(){
		return CMTRMAIL;
	}
	public void setCMTRMAIL(String cMTRMAIL){
		CMTRMAIL = cMTRMAIL;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	public String getSLSNO() {
		return SLSNO;
	}
	public void setSLSNO(String sLSNO) {
		SLSNO = sLSNO;
	}
	public String getKYCNO() {
		return KYCNO;
	}
	public void setKYCNO(String kYCNO) {
		KYCNO = kYCNO;
	}
	public String getOFLAG() {
		return OFLAG;
	}
	public void setOFLAG(String oFLAG) {
		OFLAG = oFLAG;
	}
}