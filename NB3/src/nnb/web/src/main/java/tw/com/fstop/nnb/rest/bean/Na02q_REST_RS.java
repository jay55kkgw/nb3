package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * NA02Q電文RS
 */
public class Na02q_REST_RS extends BaseRestBean implements Serializable{
	private static final long serialVersionUID = 6791228050721094802L;
	
	private String occurMsg;
	private String CMQTIME;
	private String CMRECNUM;
	private String CRLIMIT;
	private String CSHLIMIT;
	private String ACCBAL;
	private String CSHBAL;
	private String INT_RATE;
	private String PAYMT_ACCT;
	private String CURR_BAL;
	private String TOTL_DUE;
	private String STOP_DAY;
	private String STMT_DAY;
	private String DBCODE;
	private String RECNUM;
	LinkedList<N810_REST_RSDATA> REC;
	
	public String getOccurMsg() {
		return occurMsg;
	}
	public void setOccurMsg(String occurMsg) {
		this.occurMsg = occurMsg;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getCMRECNUM() {
		return CMRECNUM;
	}
	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}
	public String getCRLIMIT() {
		return CRLIMIT;
	}
	public void setCRLIMIT(String cRLIMIT) {
		CRLIMIT = cRLIMIT;
	}
	public String getCSHLIMIT() {
		return CSHLIMIT;
	}
	public void setCSHLIMIT(String cSHLIMIT) {
		CSHLIMIT = cSHLIMIT;
	}
	public String getACCBAL() {
		return ACCBAL;
	}
	public void setACCBAL(String aCCBAL) {
		ACCBAL = aCCBAL;
	}
	public String getCSHBAL() {
		return CSHBAL;
	}
	public void setCSHBAL(String cSHBAL) {
		CSHBAL = cSHBAL;
	}
	public String getINT_RATE() {
		return INT_RATE;
	}
	public void setINT_RATE(String iNT_RATE) {
		INT_RATE = iNT_RATE;
	}
	public String getPAYMT_ACCT() {
		return PAYMT_ACCT;
	}
	public void setPAYMT_ACCT(String pAYMT_ACCT) {
		PAYMT_ACCT = pAYMT_ACCT;
	}
	public String getCURR_BAL() {
		return CURR_BAL;
	}
	public void setCURR_BAL(String cURR_BAL) {
		CURR_BAL = cURR_BAL;
	}
	public String getTOTL_DUE() {
		return TOTL_DUE;
	}
	public void setTOTL_DUE(String tOTL_DUE) {
		TOTL_DUE = tOTL_DUE;
	}
	public String getSTOP_DAY() {
		return STOP_DAY;
	}
	public void setSTOP_DAY(String sTOP_DAY) {
		STOP_DAY = sTOP_DAY;
	}
	public String getSTMT_DAY() {
		return STMT_DAY;
	}
	public void setSTMT_DAY(String sTMT_DAY) {
		STMT_DAY = sTMT_DAY;
	}
	public String getDBCODE() {
		return DBCODE;
	}
	public void setDBCODE(String dBCODE) {
		DBCODE = dBCODE;
	}
	public String getRECNUM() {
		return RECNUM;
	}
	public void setRECNUM(String rECNUM) {
		RECNUM = rECNUM;
	}
	public LinkedList<N810_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<N810_REST_RSDATA> rEC) {
		REC = rEC;
	}
}