package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N830_REST_RS  extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6389777885154991747L;

	private String TSFACN;//轉帳帳號
	private String TYPE;//查詢類別
	private String CUSNUM;//用戶編號
	private String ITMNUM;//項目代號1:申請2:註銷3:變更
	private String DATE;//日期YYYMMDD
	private String TIME;//時間HHMMSS
	private String CMQTIME;
	
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getTSFACN() {
		return TSFACN;
	}
	public void setTSFACN(String tSFACN) {
		TSFACN = tSFACN;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getCUSNUM() {
		return CUSNUM;
	}
	public void setCUSNUM(String cUSNUM) {
		CUSNUM = cUSNUM;
	}
	public String getITMNUM() {
		return ITMNUM;
	}
	public void setITMNUM(String iTMNUM) {
		ITMNUM = iTMNUM;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
}
