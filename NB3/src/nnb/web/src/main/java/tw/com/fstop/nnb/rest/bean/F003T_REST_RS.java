package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * 
 * 功能說明 :外匯匯入匯款線上解款回應
 *
 */
public class F003T_REST_RS extends BaseRestBean implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3761615151410999591L;

	private String ADTXNO;// 資料庫編號
	
	private String STAN;//交易序號
	
	private String M1SBBIC;//SWIFT CODE：
	
	private String ORGCCY;// 匯款幣別

	private String ORGAMT;// 匯款金額

	private String COMMCCY2;// 手續費幣別

	private String COMMAMT2;// 手續費

	private String PMTCCY;// 解款幣別

	private String PMTAMT;//// 解款金額
	
	private String EXRATE;//匯率

	public String getADTXNO()
	{
		return ADTXNO;
	}

	public void setADTXNO(String aDTXNO)
	{
		ADTXNO = aDTXNO;
	}

	public String getSTAN()
	{
		return STAN;
	}

	public void setSTAN(String sTAN)
	{
		STAN = sTAN;
	}

	public String getM1SBBIC()
	{
		return M1SBBIC;
	}

	public void setM1SBBIC(String m1sbbic)
	{
		M1SBBIC = m1sbbic;
	}

	public String getORGCCY()
	{
		return ORGCCY;
	}

	public void setORGCCY(String oRGCCY)
	{
		ORGCCY = oRGCCY;
	}

	public String getORGAMT()
	{
		return ORGAMT;
	}

	public void setORGAMT(String oRGAMT)
	{
		ORGAMT = oRGAMT;
	}

	public String getCOMMCCY2()
	{
		return COMMCCY2;
	}

	public void setCOMMCCY2(String cOMMCCY2)
	{
		COMMCCY2 = cOMMCCY2;
	}

	public String getCOMMAMT2()
	{
		return COMMAMT2;
	}

	public void setCOMMAMT2(String cOMMAMT2)
	{
		COMMAMT2 = cOMMAMT2;
	}

	public String getPMTCCY()
	{
		return PMTCCY;
	}

	public void setPMTCCY(String pMTCCY)
	{
		PMTCCY = pMTCCY;
	}

	public String getPMTAMT()
	{
		return PMTAMT;
	}

	public void setPMTAMT(String pMTAMT)
	{
		PMTAMT = pMTAMT;
	}

	public String getEXRATE()
	{
		return EXRATE;
	}

	public void setEXRATE(String eXRATE)
	{
		EXRATE = eXRATE;
	}
		
	

}
