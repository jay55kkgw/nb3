package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N322_IDGATE_DATA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -762951082705400880L;

	@SerializedName(value = "CUSIDN")
	private String CUSIDN;//統一編號

	@SerializedName(value = "FDINVTYPE")
	private String FDINVTYPE;//資料內容

	@SerializedName(value = "UPD_FLG")
	private String UPD_FLG;//異動註記
	
	@SerializedName(value = "DEGREE")
	private String DEGREE;//學歷
	
	@SerializedName(value = "CAREER")
	private String CAREER;//職業
	
	@SerializedName(value = "SALARY")
	private String SALARY;//收入
	
	@SerializedName(value = "EDITON")
	private String EDITON;//測試問券版次
	
	@SerializedName(value = "ANSWER")
	private String ANSWER;//答案
	
	@SerializedName(value = "FDMARK1")
	private String FDMARK1;//重大傷病證明
	
	@SerializedName(value = "KIND")
	private String KIND;//交易機制

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getFDINVTYPE() {
		return FDINVTYPE;
	}

	public void setFDINVTYPE(String fDINVTYPE) {
		FDINVTYPE = fDINVTYPE;
	}

	public String getUPD_FLG() {
		return UPD_FLG;
	}

	public void setUPD_FLG(String uPD_FLG) {
		UPD_FLG = uPD_FLG;
	}

	public String getDEGREE() {
		return DEGREE;
	}

	public void setDEGREE(String dEGREE) {
		DEGREE = dEGREE;
	}

	public String getCAREER() {
		return CAREER;
	}

	public void setCAREER(String cAREER) {
		CAREER = cAREER;
	}

	public String getSALARY() {
		return SALARY;
	}

	public void setSALARY(String sALARY) {
		SALARY = sALARY;
	}

	public String getEDITON() {
		return EDITON;
	}

	public void setEDITON(String eDITON) {
		EDITON = eDITON;
	}

	public String getANSWER() {
		return ANSWER;
	}

	public void setANSWER(String aNSWER) {
		ANSWER = aNSWER;
	}

	public String getFDMARK1() {
		return FDMARK1;
	}

	public void setFDMARK1(String fDMARK1) {
		FDMARK1 = fDMARK1;
	}

	public String getKIND() {
		return KIND;
	}

	public void setKIND(String kIND) {
		KIND = kIND;
	}
	
}
