package tw.com.fstop.web.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerFontProvider;
import com.itextpdf.tool.xml.XMLWorkerHelper;

import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;

/**
 * PDF的方法
 */
public class PDFUtil{
	//記LOG
	private Logger log = LoggerFactory.getLogger(PDFUtil.class);
	
	/**
	 * HTML轉成PDF
	 */
	public InputStream HTMLtoPDF(String HTML){
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		Document document = new Document();
		try{
			PdfWriter pdfWriter = PdfWriter.getInstance(document,byteArrayOutputStream);
			
			document.open();
			
			XMLWorkerHelper xmlWorkerHelper = XMLWorkerHelper.getInstance();
			xmlWorkerHelper.parseXHtml(pdfWriter,document,new ByteArrayInputStream(HTML.getBytes("UTF-8")),Charset.forName("UTF-8"),new AsianFontProvider());
			document.close();
		}
		catch(Exception e){
			log.error("",e);
			document.close();
			return null;
		}
		return new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
	}
	public class AsianFontProvider extends XMLWorkerFontProvider{
		public Font getFont(final String fontname,final String encoding,final boolean embedded,final float size,final int style,final BaseColor color){
			BaseFont bf = null;
			try{
				String locale = LocaleContextHolder.getLocale().toString();
				if(locale.equals("zh_CN")) {
					//簡體中文使用的預設字型
					bf = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H",BaseFont.NOT_EMBEDDED);
				}
				else {
					//繁體中文使用的預設字型，不支援簡體
					bf = BaseFont.createFont("MHei-Medium","UniCNS-UCS2-H",BaseFont.NOT_EMBEDDED);
				}
			}
			catch(Exception e){
				log.error("",e);
			}
			Font font = new Font(bf,size,style,color);
			font.setColor(color);
			return font;
		}
	}
	/**
	 * 取得HTML的樣版，將變數換掉
	 */
	public String getPDFHTML(Map<String,?> parameterMap){
		String HTML = "";
		InputStream inputStream = null;
		BufferedReader bufferedReader = null;
		
		try{
			//讀取HTML的TEMPLATE
			String templatePath = (String)parameterMap.get("templatePath");
			log.debug(ESAPIUtil.vaildLog("templatePath >> " + templatePath));
			
			Resource resource = new ClassPathResource(templatePath);
//			File file = resource.getFile();
			
			if(resource.exists()){
				inputStream =  new BufferedInputStream(resource.getInputStream());
				bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
				StringBuilder stringBuilder = new StringBuilder();
				//HTML的字串
				String line;
				line = bufferedReader.readLine();
				while(line != null){
					line = ESAPIUtil.validInput(line,"GeneralString",true);
					log.debug(ESAPIUtil.vaildLog("line >> " + line));
					stringBuilder.append(line);
					line = bufferedReader.readLine();
				}
				
				HTML = stringBuilder.toString();
			
				log.debug(ESAPIUtil.vaildLog("HTML >> " + HTML)); 	
				//將HTML裡面的變數取代掉
				Set<String> set = parameterMap.keySet();
				Iterator<String> iterator = set.iterator();
				while(iterator.hasNext()){
					String key = iterator.next();  
					log.debug(ESAPIUtil.vaildLog("key >> " + key));
					Object value = parameterMap.get(key);  
					log.debug(ESAPIUtil.vaildLog("value >> " + value));
					if(value instanceof String){
						HTML = HTML.replace("${" + key + "}",String.valueOf(value));
					}
				}
				log.debug(ESAPIUtil.vaildLog("HTML >> " + HTML));
			}
		}
		catch(Exception e){
			log.error("",e);
		}
		finally{
			try{
				if(bufferedReader != null){
					bufferedReader.close();
				}
			}
			catch(Exception e){
				log.error("",e);
			}
			try{
				if(inputStream != null){
					inputStream.close();
				}
			}
			catch(Exception e){
				log.error("",e);
			}
		}
		return HTML;
	}
}