package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class Ratequery_N025_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3370460295315092495L;

	// 資料陣列
	private LinkedList<Ratequery_N025_RSDATA> REC;

	public LinkedList<Ratequery_N025_RSDATA> getREC() {
		return REC;
	}

	public void setREC(LinkedList<Ratequery_N025_RSDATA> rEC) {
		REC = rEC;
	}

}
