package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N930EE_REST_RQ extends BaseRestBean_PS implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1065505469195494796L;

	private String FGTXWAY; // 舊bean用來判斷
	private String DPUSERID;
	private String EXECUTEFUNCTION;
	private String NEW_EMAIL;
	private String PINNEW;
	private String CUSIDN;
	private String FLAG;
	// n930ee會去打N931 931需要
	private String CARDAP;
	// IDGATE
	private String sessionID;
	private String idgateID;
	private String txnID;

	// EMAIL驗證新增欄位
	private String BRHCOD;
	private String TLRNUM;
	private String TRNNUM;
	
	//FOR C113 增加IP欄位
	private String IP;

	public String getFGTXWAY() {
		return FGTXWAY;
	}

	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}

	public String getDPUSERID() {
		return DPUSERID;
	}

	public void setDPUSERID(String dPUSERID) {
		DPUSERID = dPUSERID;
	}

	public String getEXECUTEFUNCTION() {
		return EXECUTEFUNCTION;
	}

	public void setEXECUTEFUNCTION(String eXECUTEFUNCTION) {
		EXECUTEFUNCTION = eXECUTEFUNCTION;
	}

	public String getPINNEW() {
		return PINNEW;
	}

	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}

	public String getNEW_EMAIL() {
		return NEW_EMAIL;
	}

	public void setNEW_EMAIL(String nEW_EMAIL) {
		NEW_EMAIL = nEW_EMAIL;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getFLAG() {
		return FLAG;
	}

	public void setFLAG(String fLAG) {
		FLAG = fLAG;
	}

	public String getCARDAP() {
		return CARDAP;
	}

	public void setCARDAP(String cARDAP) {
		CARDAP = cARDAP;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public String getIdgateID() {
		return idgateID;
	}

	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}

	public String getTxnID() {
		return txnID;
	}

	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}

	public String getBRHCOD() {
		return BRHCOD;
	}

	public void setBRHCOD(String bRHCOD) {
		BRHCOD = bRHCOD;
	}

	public String getTLRNUM() {
		return TLRNUM;
	}

	public void setTLRNUM(String tLRNUM) {
		TLRNUM = tLRNUM;
	}

	public String getTRNNUM() {
		return TRNNUM;
	}

	public void setTRNNUM(String tRNNUM) {
		TRNNUM = tRNNUM;
	}

	public String getIP() {
		return IP;
	}

	public void setIP(String iP) {
		IP = iP;
	}
	
}
