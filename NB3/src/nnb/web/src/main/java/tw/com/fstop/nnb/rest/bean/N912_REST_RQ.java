package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N912_REST_RQ extends BaseRestBean_TW implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -803633574207411113L;

	private String HEADER;		// HEADER
	private String DATE;		// 日期YYYMMDD
	private String TIME;		// 時間HHMMSS
	private String CUSIDN;		// 統一編號
	
	
	public String getHEADER() {
		return HEADER;
	}
	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	
}
