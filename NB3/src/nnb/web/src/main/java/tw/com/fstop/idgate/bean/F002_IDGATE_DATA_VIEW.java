package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;


public class F002_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5370773529503299761L;
	
	// 付款日期
	@SerializedName(value = "PAYDATE")
	private String PAYDATE;

	// 收款銀行資料
	@SerializedName(value = "FXRCVADDR")
	private String FXRCVADDR;
	
	// 付款帳號
	@SerializedName(value = "CUSTACC")
	private String CUSTACC;
	
	// 收款銀行資料
	@SerializedName(value = "display_PAYCCY")
	private String display_PAYCCY;
	
	// 付款幣別
	@SerializedName(value = "BENACC")
	private String BENACC;
	
	// 收款帳號
	@SerializedName(value = "display_REMITCY")
	private String display_REMITCY;
	
	// 收款幣別 
	@SerializedName(value = "display_PAYREMIT")
	private String display_PAYREMIT;
	
	// 收款人身份別
	@SerializedName(value = "display_BENTYPE")
	private String display_BENTYPE;
	
	// 匯款分類項目
	@SerializedName(value = "SRCFUNDDESC")
	private String SRCFUNDDESC;
	
	// 匯款分類說明 
	@SerializedName(value = "FXRMTDESC")
	private String FXRMTDESC;
	
	// Email通知
	@SerializedName(value = "CMTRMAIL")
	private String CMTRMAIL;
	
	// Email通知
	@SerializedName(value = "CMMAILMEMO")
	private String CMMAILMEMO;
	
	// 匯款附言
	@SerializedName(value = "MEMO1")
	private String MEMO1;
	
	// 手續費負擔別 
	@SerializedName(value = "DETCHG")
	private String DETCHG;
	
	// 費用扣款帳號
	@SerializedName(value = "COMMACC")
	private String COMMACC;
	
	// 費用扣款幣別
	@SerializedName(value = "display_COMMCCY")
	private String display_COMMCCY;
	
	
	@Override
	public Map<String, String> coverMap(){
		Map<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", "外匯匯出匯款");
		result.put("交易類型", "即時交易");
		result.put("付款日期", this.PAYDATE);
		result.put("付款帳號", this.CUSTACC);
		result.put("付款幣別", this.display_PAYCCY);
		result.put("收款帳號", this.BENACC);
		result.put("收款幣別", this.display_REMITCY);
		result.put("收款人資料", "名稱/地址   "+this.FXRCVADDR);
		result.put("匯款金額", this.display_PAYREMIT);
		result.put("收款人身份別", this.display_BENTYPE);
		result.put("匯款分類項目", this.SRCFUNDDESC);	
		result.put("匯款分類說明", this.FXRMTDESC);
		result.put("Email通知", "電子信箱:"+this.CMTRMAIL +"，摘要內容:"+ CMMAILMEMO);
		result.put("匯款附言", this.MEMO1);
		result.put("手續費負擔別", this.DETCHG);
		result.put("費用扣款帳號", this.COMMACC);
		result.put("費用扣款幣別", this.display_COMMCCY);
		return result;
	}


	public String getPAYDATE() {
		return PAYDATE;
	}


	public void setPAYDATE(String pAYDATE) {
		PAYDATE = pAYDATE;
	}


	public String getFXRCVADDR() {
		return FXRCVADDR;
	}


	public void setFXRCVADDR(String fXRCVADDR) {
		FXRCVADDR = fXRCVADDR;
	}


	public String getCUSTACC() {
		return CUSTACC;
	}


	public void setCUSTACC(String cUSTACC) {
		CUSTACC = cUSTACC;
	}


	public String getDisplay_PAYCCY() {
		return display_PAYCCY;
	}


	public void setDisplay_PAYCCY(String display_PAYCCY) {
		this.display_PAYCCY = display_PAYCCY;
	}


	public String getBENACC() {
		return BENACC;
	}


	public void setBENACC(String bENACC) {
		BENACC = bENACC;
	}


	public String getDisplay_REMITCY() {
		return display_REMITCY;
	}


	public void setDisplay_REMITCY(String display_REMITCY) {
		this.display_REMITCY = display_REMITCY;
	}


	public String getDisplay_PAYREMIT() {
		return display_PAYREMIT;
	}


	public void setDisplay_PAYREMIT(String display_PAYREMIT) {
		this.display_PAYREMIT = display_PAYREMIT;
	}


	public String getDisplay_BENTYPE() {
		return display_BENTYPE;
	}


	public void setDisplay_BENTYPE(String display_BENTYPE) {
		this.display_BENTYPE = display_BENTYPE;
	}


	public String getSRCFUNDDESC() {
		return SRCFUNDDESC;
	}


	public void setSRCFUNDDESC(String sRCFUNDDESC) {
		SRCFUNDDESC = sRCFUNDDESC;
	}


	public String getFXRMTDESC() {
		return FXRMTDESC;
	}


	public void setFXRMTDESC(String fXRMTDESC) {
		FXRMTDESC = fXRMTDESC;
	}


	public String getCMTRMAIL() {
		return CMTRMAIL;
	}


	public void setCMTRMAIL(String cMTRMAIL) {
		CMTRMAIL = cMTRMAIL;
	}


	public String getCMMAILMEMO() {
		return CMMAILMEMO;
	}


	public void setCMMAILMEMO(String cMMAILMEMO) {
		CMMAILMEMO = cMMAILMEMO;
	}


	public String getMEMO1() {
		return MEMO1;
	}


	public void setMEMO1(String mEMO1) {
		MEMO1 = mEMO1;
	}


	public String getDETCHG() {
		return DETCHG;
	}


	public void setDETCHG(String dETCHG) {
		DETCHG = dETCHG;
	}


	public String getCOMMACC() {
		return COMMACC;
	}


	public void setCOMMACC(String cOMMACC) {
		COMMACC = cOMMACC;
	}


	public String getDisplay_COMMCCY() {
		return display_COMMCCY;
	}


	public void setDisplay_COMMCCY(String display_COMMCCY) {
		this.display_COMMCCY = display_COMMCCY;
	}

	
}
