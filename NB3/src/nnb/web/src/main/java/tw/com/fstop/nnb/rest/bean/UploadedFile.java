package tw.com.fstop.nnb.rest.bean;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

/**
 * 多檔上傳 Model
 */
public class UploadedFile implements Serializable, MultipartFile {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2907084675112675564L;
	private byte[] bytes;
	private InputStream inputStream;
	private String name;
	private String originalFilename;
	private String contentType;
	private boolean isEmpty;
	private long size;

	public UploadedFile(byte[] bytes,String name,String originalFilename,String contentType,long size){
	    this.bytes = bytes;
	    
	    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
	    this.inputStream = byteArrayInputStream;
	    
	    this.name = name;
	    this.originalFilename = originalFilename;
	    this.contentType = contentType;
	    this.size = size;
	    this.isEmpty = false;
	}
	@Override
	public String getName(){
	    return name;
	}
	@Override
	public String getOriginalFilename(){
	    return originalFilename;
	}
	@Override
	public String getContentType(){
	    return contentType;
	}
	@Override
	public boolean isEmpty(){
	    return isEmpty;
	}
	@Override
	public long getSize(){
	    return size;
	}
	@Override
	public byte[] getBytes() throws IOException{
	    return bytes;
	}
	@Override
	public InputStream getInputStream() throws IOException{
	    return inputStream;
	}
	@Override
	public void transferTo(File dest) throws IOException,IllegalStateException{
		
	}
}