package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * 
 * 功能說明 :線上申請外匯存款帳戶結清銷戶結清資料
 *
 */
public class F015_REST_RQ extends BaseRestBean_FX implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6581308143803242452L;

	private String SPV_ID;// 主管代號

	private String TX_SEQ;// 交易序號

	private String ETY_LVL;// ENTRY LEVEL

	private String FYACN;// 轉出帳號

	private String CRY;// 轉出幣別

	public String getSPV_ID()
	{
		return SPV_ID;
	}

	public void setSPV_ID(String sPV_ID)
	{
		SPV_ID = sPV_ID;
	}

	public String getTX_SEQ()
	{
		return TX_SEQ;
	}

	public void setTX_SEQ(String tX_SEQ)
	{
		TX_SEQ = tX_SEQ;
	}

	public String getETY_LVL()
	{
		return ETY_LVL;
	}

	public void setETY_LVL(String eTY_LVL)
	{
		ETY_LVL = eTY_LVL;
	}

	public String getFYACN()
	{
		return FYACN;
	}

	public void setFYACN(String fYACN)
	{
		FYACN = fYACN;
	}

	public String getCRY()
	{
		return CRY;
	}

	public void setCRY(String cRY)
	{
		CRY = cRY;
	}

}
