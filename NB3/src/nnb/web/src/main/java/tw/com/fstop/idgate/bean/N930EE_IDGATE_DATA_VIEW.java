package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;


public class N930EE_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5370773529503299761L;
	
	@SerializedName(value = "OLDEMAIL")
	private String OLDEMAIL;

	@SerializedName(value = "NEW_EMAIL")
	private String NEW_EMAIL;
	
	@Override
	public Map<String, String> coverMap(){
		LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
        result.put("交易名稱", "我的Email設定");
        result.put("交易類型", "更新");
		result.put("目前電子郵箱", this.OLDEMAIL);
		result.put("新的電子郵箱", this.NEW_EMAIL);
		return result;
	}

	public String getOLDEMAIL() {
		return OLDEMAIL;
	}

	public void setOLDEMAIL(String oLDEMAIL) {
		OLDEMAIL = oLDEMAIL;
	}

	public String getNEW_EMAIL() {
		return NEW_EMAIL;
	}

	public void setNEW_EMAIL(String nEW_EMAIL) {
		NEW_EMAIL = nEW_EMAIL;
	}
}
