package tw.com.fstop.nnb.service;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletContext;

import java.util.Set;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;

import fstop.orm.po.ADMBRANCH;
import fstop.orm.po.ADMCARDDATA;
import fstop.orm.po.ADMCOUNTRY;
import fstop.orm.po.TXNCARDAPPLY;
import fstop.orm.po.TXNUSER;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmBranchDao;
import tw.com.fstop.tbb.nnb.dao.AdmCardDataDao;
import tw.com.fstop.tbb.nnb.dao.AdmCountryDao;
import tw.com.fstop.tbb.nnb.dao.AdmLionCardDataDao;
import tw.com.fstop.tbb.nnb.dao.AdmMsgCodeDao;
import tw.com.fstop.tbb.nnb.dao.CreditUtils;
import tw.com.fstop.tbb.nnb.dao.TxnCardApplyDao;
import tw.com.fstop.tbb.nnb.dao.TxnUserDao;
import tw.com.fstop.tbb.nnb.util.StrUtils;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.FTPUtil;
import tw.com.fstop.web.util.I18nHelper;
import tw.com.fstop.web.util.PDFUtil;
import tw.com.fstop.web.util.WebUtil;

import org.springframework.web.bind.annotation.RequestParam;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.commons.net.ftp.FTPSClient;
import org.joda.time.DateTime;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.security.cert.X509Certificate;
import java.security.cert.CertificateException;
import java.text.SimpleDateFormat;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;


@Service
public class Creditcard_Apply_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	TxnCardApplyDao txnCardApplyDao;
	@Autowired
	AdmCardDataDao admCardDataDao;
	@Autowired
	AdmMsgCodeDao admMsgCodeDao;
	@Autowired
	AdmLionCardDataDao admLionCardDataDao;
	@Autowired
	CreditUtils creditUtils;
	@Autowired
	TxnUserDao txnUserDao;
	@Autowired
	AdmBranchDao admBranchDao;
	@Autowired
	AdmCountryDao admcountrydao;
	@Autowired
	LinkedHashMap<String, Map<String, String>> NA01;
	@Autowired
	Apply_CreditLoan_Service apply_creditLoan_service;
	@Autowired
	Change_Data_Service change_data_service;
	@Autowired
	I18n i18n;
	@Autowired
	ServletContext context; 

	/**
	 * 信用卡開卡
	 * 
	 * @param 
	 * @return
	 */
	public BaseResult activate(Map<String, String> reqParam) {
		log.trace("creditcard_service_activate...");
		log.trace(ESAPIUtil.vaildLog("reqParam >> " + CodeUtil.toJson(reqParam)));
		// 處理結果
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = N816A_REST(reqParam);
			switch(((Map<String,String>)bs.getData()).get("STATUS")) {
			case "Z700":
				((Map<String,String>)bs.getData()).put("STATUS", admMsgCodeDao.errorMsg("Z700"));
				break;
			case "Z704":
				((Map<String,String>)bs.getData()).put("STATUS", admMsgCodeDao.errorMsg("Z704"));
				break;
			case "Z705":
				((Map<String,String>)bs.getData()).put("STATUS", admMsgCodeDao.errorMsg("Z705"));
				break;
			case "Z706":
				((Map<String,String>)bs.getData()).put("STATUS", admMsgCodeDao.errorMsg("Z706"));
				break;
			case "Z707":
				((Map<String,String>)bs.getData()).put("STATUS", admMsgCodeDao.errorMsg("Z707"));
				break;
			case "Z712":
				((Map<String,String>)bs.getData()).put("STATUS", admMsgCodeDao.errorMsg("Z712"));
				break;
			case "Z716":
				((Map<String,String>)bs.getData()).put("STATUS", admMsgCodeDao.errorMsg("Z716"));
				break;
			case "Z757":
				((Map<String,String>)bs.getData()).put("STATUS", admMsgCodeDao.errorMsg("Z757"));
				break;
			case "FE0012":
				((Map<String,String>)bs.getData()).put("STATUS", admMsgCodeDao.errorMsg("FE0012"));
				break;
			}
			((Map<String,String>)bs.getData()).put("CARDNUM",WebUtil.hideCardNum2(((Map<String,String>)bs.getData()).get("CARDNUM")));
			
			List<String> statusCode = Arrays.asList("Z700","Z704","Z705","Z706","Z707","Z712","Z716","Z757","FE0012");
			if(statusCode.contains(bs.getMsgCode())) {
				bs.setResult(true);
			}
			
		} catch (Exception e) {
			//e.printStackTrace();
			//Avoid Information Exposure Through an Error Message
			log.error("activate error >> {}",e); 
		}
		// 回傳處理結果
		return bs;
	}

	/**
	 * 信用卡掛失(輸入頁)
	 * 
	 * @param cusidn
	 * @return
	 */
	public BaseResult loss_getCardList(String cusidn) {
		log.trace("creditcard_service_loss_getCardList...");
		// 處理結果
		BaseResult bs = null;
		boolean isHaveValidCard = false;
		boolean isHaveApplyLostCard = false;
		try {
			bs = N816L_REST(cusidn);
			Map<String, Object> bsData = (Map) bs.getData();

			List<Map<String, String>> rows = (List<Map<String, String>>) bsData.get("REC");
			rows.removeIf(Map::isEmpty);// 移除空白 OR null的Map
			rows.removeIf(Map -> null == Map.get("CARDNUM") || "".equals(Map.get("CARDNUM")));// 移除CARDNUM==空白 OR
																								// null的Map

			int count = 0;
			for (Map eachrow : rows) {
				// 卡號遮罩
				String cardnum = (String) eachrow.get("CARDNUM");
				log.trace("cardnum >>{}", cardnum);
				String cardnum_cover = "**** **** **** " + cardnum.substring(12);
				eachrow.put("CARDNUM_COVER", cardnum_cover);

				// 姓名遮罩
				String cr_name = (String) eachrow.get("CR_NAME");
				String cr_name_cover = WebUtil.hideusername1Convert(cr_name);
				eachrow.put("CR_NAME_COVER", cr_name_cover);

				// TODO: i18n
				if ("0".equals(eachrow.get("STATUS"))) {
					eachrow.put("STATUS_STR", "");
					isHaveValidCard = true;
				} else if ("1".equals(eachrow.get("STATUS"))) {
					eachrow.put("STATUS_STR", i18n.getMsg("LB.X0461"));//已掛失
					isHaveApplyLostCard = true;
				} else if ("2".equals(eachrow.get("STATUS"))) {
					eachrow.put("STATUS_STR",i18n.getMsg("LB.X1959"));//已註記
				}
			}
			bsData.put("REC", rows);
			bs.setData(bsData);
			bs.addData("isHaveValidCard", isHaveValidCard);
			bs.addData("isHaveApplyLostCard", isHaveApplyLostCard);
		} catch (Exception e) {
			//e.printStackTrace();
			//Avoid Information Exposure Through an Error Message
			log.error("loss_getCardList error >> {}",e); 
		}
		// 回傳處理結果
		log.trace("BS >>{}", bs.getData());
		return bs;
	}

	/**
	 * 信用卡掛失
	 * 
	 * @param sessionId
	 * @return
	 */
	public BaseResult loss_action(String cusidn, Map<String, String> reqParam) {
		log.trace("creditcard_service_loss");
		// 處理結果
		BaseResult bs = null;
		try {
			bs = N816L_1_REST(cusidn, reqParam);
			log.trace("BS >>{}",bs);
			Map<String, Object> bsData = (Map) bs.getData();
			List<Map<String, String>> rows = (List<Map<String, String>>) bsData.get("SUCCEED");
			for (Map eachrow : rows) {
				// 卡號遮罩
				String cardnum = (String) eachrow.get("CARDNUM");
				log.trace("cardnum >>{}", cardnum);
				String cardnum_cover = "**** **** **** " + cardnum.substring(12);
				eachrow.put("CARDNUM_COVER", cardnum_cover);


				// TODO: i18n
				if ("掛失成功".equals((String) eachrow.get("STATUS"))) {
					eachrow.put("STATUS_STR", i18n.getMsg("LB.Report_successful"));
				} else if ("掛失失敗".equals((String) eachrow.get("STATUS"))) {
					eachrow.put("STATUS_STR",  i18n.getMsg("LB.Report_loss_fail"));
				}
			}
			
			List<Map<String, String>> rows2 = (List<Map<String, String>>) bsData.get("FAILED");
			for (Map eachrow : rows2) {
				// 卡號遮罩
				String cardnum = (String) eachrow.get("CARDNUM");
				log.trace("cardnum >>{}", cardnum);
				String cardnum_cover = "**** **** **** " + cardnum.substring(12);
				eachrow.put("CARDNUM_COVER", cardnum_cover);

				// TODO: i18n
				if ("掛失成功".equals((String) eachrow.get("STATUS"))) {
					eachrow.put("STATUS_STR", i18n.getMsg("LB.Report_successful"));
				} else if ("掛失失敗".equals((String) eachrow.get("STATUS"))) {
					eachrow.put("STATUS_STR",  i18n.getMsg("LB.Report_loss_fail"));
				}
			}
			
			bsData.put("SUCCEED", rows);
			bsData.put("FAILED", rows2);
			bs.setData(bsData);
			bs.addData("SUCCEEDCOUNT",rows.size());
			bs.addData("FAILEDCOUNT",rows2.size());
			bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
			
		} catch (Exception e) {
			//e.printStackTrace();
			//Avoid Information Exposure Through an Error Message
			log.error("loss_action error >> {}",e); 
		}
		// 回傳處理結果
		return bs;
	}
	
	/**
	 * CK01	長期使用循環信用申請分期還款
	 * 
	 * @param sessionId
	 * @return
	 */
	public BaseResult instalment_repayment_p1(Map<String, String> reqParam) {
		log.trace("instalment_repayment_p1...");
		log.trace(ESAPIUtil.vaildLog("reqParam= "+CodeUtil.toJson(reqParam)));
		
		// 處理結果
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = CK01_REST_EAI(reqParam);
			//整理bs成頁面上所需 (for 輸入頁)
			Map<String, Object> bsData = (Map) bs.getData();
			bsData.put("CURRBAL_SHOW",NumericUtil.formatNumberString(String.format("%d", new Object[] {new Integer(Integer.parseInt(bsData.get("CURRBAL").toString()))}),0));
			bsData.put("CRLIMIT_SHOW",NumericUtil.formatNumberString(String.format("%d", new Object[] {new Integer(Integer.parseInt(bsData.get("CRLIMIT").toString()))}),0));
			bsData.put("CRLIMIT",String.format("%d", new Object[] {new Integer(Integer.parseInt(bsData.get("CRLIMIT").toString()))}));
			bsData.put("RATE_SHOW",NumericUtil.formatNumberString((String) bsData.get("RATE"),2)); 
			//CK01_1.jsp 內的邏輯,要過濾掉特殊狀況的卡號
			String[] cardList=((String)bsData.get("CARDNUMS")).split(",");
			StringBuilder sb = new StringBuilder();
			for(int i = 0; i <cardList.length;i++) {
				if(!(cardList[i].substring(13,14)).equals("1")) {
					log.trace("CARDNUM deny >>{}",cardList[i]);
				}
				if(i!=(cardList.length-1)) {
					sb.append(cardList[i]+",");
				}else {
					sb.append(cardList[i]); 
				}
			}
			bsData.put("CARDNUMS", sb.toString());
			log.trace("CARDNUM CHECKED >> {}",  sb.toString());
			if(((String) bsData.get("POT")).length()>=2) {
				switch(Integer.parseInt(((String)bsData.get("POT")).substring(1,2))) 
				{ 
		    		case 1: 
		    			bsData.put("POTDESC","（"+ i18n.getMsg("LB.X0875") +"）"); 
		        	break; 	
		    		case 2: 
		    			bsData.put("POTDESC","（"+ i18n.getMsg("LB.X0876") +"）");
		        	break; 
		    		case 3: 
		    			bsData.put("POTDESC","（"+ i18n.getMsg("LB.X0877") +"）"); 
		        	break;
		    		case 4: 
		    			bsData.put("POTDESC","（"+ i18n.getMsg("LB.X0878") +"）");
		        	break; 
		    		case 5: 
		    			bsData.put("POTDESC","（"+ i18n.getMsg("LB.X0879") +"）");
		        	break;
		    		default: 
		    			bsData.put("POTDESC","");
				} 		
			}
			bs.setData(bsData);

		} catch (Exception e) {
			//e.printStackTrace();
			//Avoid Information Exposure Through an Error Message
			log.error("instalment_repayment_p1 error >> {}",e); 
		}
		// 回傳處理結果
		return bs;
	}
	
	/**
	 * CK01	長期使用循環信用申請分期還款
	 * 
	 * @param sessionId
	 * @return
	 */
	public BaseResult instalment_repayment_result(Map<String, String> reqParam) {
		log.trace("instalment_repayment_p1...");
		log.trace(ESAPIUtil.vaildLog("reqParam= "+ CodeUtil.toJson(reqParam)));
		// 處理結果
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			//For TXNLOG
			reqParam.put("ADTXACNO", reqParam.get("CARDNUM"));
			bs = CK01_EAI_RESULT(reqParam);
			//數字顯示轉
			((Map<String,String>)bs.getData()).put("CURRBAL",NumericUtil.formatNumberString(((Map<String,String>)bs.getData()).get("CURRBAL"),0));
			((Map<String,String>)bs.getData()).put("FIRST_AMOUNT",NumericUtil.formatNumberString(((Map<String,String>)bs.getData()).get("FIRST_AMOUNT"),0));
			((Map<String,String>)bs.getData()).put("FIRST_INTEREST",NumericUtil.formatNumberString(((Map<String,String>)bs.getData()).get("FIRST_INTEREST"),0));
			((Map<String,String>)bs.getData()).put("PERIOD_AMOUNT",NumericUtil.formatNumberString(((Map<String,String>)bs.getData()).get("PERIOD_AMOUNT"),0));
			((Map<String,String>)bs.getData()).put("CRLIMIT",NumericUtil.formatNumberString(((Map<String,String>)bs.getData()).get("CRLIMIT"),0));
			((Map<String,String>)bs.getData()).put("AMOUNT",NumericUtil.formatNumberString(((Map<String,String>)bs.getData()).get("AMOUNT"),0));
			((Map<String,String>)bs.getData()).put("RATE",NumericUtil.formatNumberString(((Map<String,String>)bs.getData()).get("RATE"),2));
			((Map<String,String>)bs.getData()).put("APPLY_RATE",NumericUtil.formatNumberString(((Map<String,String>)bs.getData()).get("APPLY_RATE"),2));
			if(((Map<String,String>)bs.getData()).get("POT").length()>=2) {
				switch(Integer.parseInt(((Map<String,String>)bs.getData()).get("POT").substring(1,2))) 
				{ 
		    		case 1: 
		    			((Map<String,String>)bs.getData()).put("POTDESC","（"+i18n.getMsg("LB.X0875")+"）"); 
		        	break; 	
		    		case 2: 
		    			((Map<String,String>)bs.getData()).put("POTDESC","（"+i18n.getMsg("LB.X0876")+"）");
		        	break; 
		    		case 3: 
		    			((Map<String,String>)bs.getData()).put("POTDESC","（"+i18n.getMsg("LB.X0877")+"）"); 
		        	break;
		    		case 4: 
		    			((Map<String,String>)bs.getData()).put("POTDESC","（"+i18n.getMsg("LB.X0878")+"）");
		        	break; 
		    		case 5: 
		    			((Map<String,String>)bs.getData()).put("POTDESC","（"+i18n.getMsg("LB.X0879")+"）");
		        	break;
		    		default: 
		    			((Map<String,String>)bs.getData()).put("POTDESC","");
				} 		
			}
		} catch (Exception e) {
			//e.printStackTrace();
			//Avoid Information Exposure Through an Error Message
			log.error("instalment_repayment_result error >> {}",e); 
		}
		// 回傳處理結果
		return bs;
	}
	

	/**
	 * 信用卡實體帳單補寄申請
	 * 
	 * @param reqParam
	 * @return BaseResult
	 */
	public BaseResult entity_bill_resend(Map<String, String> reqParam) {
		log.trace("entity_bill_resend_service");
		log.trace(ESAPIUtil.vaildLog("cid >> " + reqParam.get("CUSIDN")));
		BaseResult bs = null;
		try {

			bs = new BaseResult();

			String now = DateUtil.getTWDate("");
			String str_Year = now.substring(0, 3);
			String str_Month = now.substring(3, 5);
			String str_LastYear = String.valueOf((Integer.parseInt(now.substring(0, 3)) - 1));
			String[] month = { "12", "11", "10", "09", "08", "07", "06", "05", "04", "03", "02", "01" };
			int i_Begin = 0;
			ArrayList dataList = new ArrayList();
			for (int i = 0; i < 12; i++) {
				if (month[i].equals(str_Month)) {
					i_Begin = i;
					break;
				}
			}
			for (int k = 2; k >= 0; k--) {
				Map data = new LinkedHashMap<>();
				data.put("value", ((i_Begin + k) > 11 ? str_LastYear : str_Year) + month[(i_Begin + k) % 12]);
				data.put("text", month[(i_Begin + k) % 12]);
				dataList.add(data);
			}
			bs.addData("selectData", dataList);
			bs.setResult(true);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			log.error("entity_bill_resend_service_error >> {}", e);
		}
		// return retMap;
		return bs;
	}

	/**
	 * 信用卡實體帳單補寄申請確認頁
	 * 
	 * @param reqParam
	 * @param
	 * @return
	 */
	public BaseResult entity_bill_resend_confirm(Map<String, String> reqParam) {
		log.trace("entity_bill_resend_confirm_service");
		BaseResult bs = null;
		try {
			// 放入TXTOKEN
			bs = getTxToken();
			if (bs.getResult()) {
				reqParam.put("TXTOKEN", ((Map<String, String>) bs.getData()).get("TXTOKEN"));
				String str_Year = reqParam.get("PRTYM").substring(0, 3);
				String str_Month = reqParam.get("PRTYM").substring(3);
				bs.setData(reqParam);
				bs.addData("YEAR", str_Year);
				bs.addData("MONTH", str_Month);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			log.error("entity_bill_resend_confirm_service_error >>{}", e);
		}
		return bs;
	}

	/**
	 * 信用卡實體帳單補寄申請 結果頁
	 * 
	 * @param reqParam
	 * @param sessionId
	 * @return
	 */
	public BaseResult entity_bill_resend_result(Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("entity_bill_resend_result_service_reqParam >> " + CodeUtil.toJson(reqParam)));
			bs = new BaseResult();

			bs = N821_REST(reqParam);
			String year = ((Map<String, String>) bs.getData()).get("PRTYM").substring(0, 3);
			String month = ((Map<String, String>) bs.getData()).get("PRTYM").substring(3);
			bs.addData("MONTH", month);
			bs.addData("YEAR", year);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			log.error("entity_bill_resend_result error >>", e);
		}
		return bs;
	}

	/**
	 * NI02 現金密碼函補寄申請輸入頁
	 * 
	 * @param reqParam
	 * @return BaseResult
	 */
	public BaseResult cash_pw_resend(Map<String, String> reqParam) {
		log.trace("cash_pw_resend_service");
		BaseResult bs = null;
		try {

			bs = new BaseResult();
			// if not Ikey User , direct to error page
			if ("false".equals(reqParam.get("ISIKEYUSER"))) {
				bs.setResult(false);
				bs.setErrorMessage("Z103", admMsgCodeDao.errorMsg("Z103"));
			} else {
				//Avoid TXNLOG
				reqParam.put("ADOPID", "__N810");
				bs = N810_REST(reqParam);
				if ("".equals(((Map<String, String>) bs.getData()).get("TOPMSG"))) {

					ArrayList selectList = new ArrayList();
					List<Map<String, String>> cardListDataMap = ((List<Map<String, String>>) ((Map<String, Object>) bs
							.getData()).get("REC"));
					for (Map eachCardData : cardListDataMap) {
						Map data = new LinkedHashMap<>();
						if ("".equals(((String) eachCardData.get("CARDNUM")).substring(13, 14))) {
							continue;
						} else if (Integer.parseInt(((String) eachCardData.get("CARDNUM")).substring(13, 14)) > 1) {
							continue;
						} else {
							data.put("VALUE", eachCardData.get("CARDNUM"));
							data.put("TEXT", WebUtil.hideCardNum((String) eachCardData.get("CARDNUM")));
							selectList.add(data);
						}
					}
					bs.addData("SELECTLIST", selectList);
					bs.setResult(true);
				}else {
					bs.setResult(false);
				}
			}

		} catch (Exception e) {
			//e.printStackTrace();
			log.error("cash_pw_resend_service_error >> {}", e);
		}
		return bs;
	}

	/**
	 * NI02 現金密碼函補寄申請確認頁
	 * 
	 * @param reqParam
	 * @return BaseResult
	 */
	public BaseResult cash_pw_resend_confirm(Map<String, String> reqParam) {
		log.trace("cash_pw_resend_confirm_service");
		BaseResult bs = null;
		BaseResult bs1 = null;
		try {

			bs = new BaseResult();


			bs = getTxToken();
			if (bs.getResult()) {
				reqParam.put("TXTOKEN", ((Map<String, String>) bs.getData()).get("TXTOKEN"));
				bs.setData(reqParam);
				bs.addData("hideCardNum", WebUtil.hideCardNum(reqParam.get("CARDNUM")));
			}

		} catch (Exception e) {
			//e.printStackTrace();
			log.error("cash_pw_resend_confirm_service_error >> {}", e);
		}
		return bs;
	}

	/**
	 * NI02 現金密碼函補寄申請結果頁
	 * 
	 * @param reqParam
	 * @param sessionId
	 * @return
	 */
	public BaseResult cash_pw_resend_result(Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("cash_pw_resend_result_service_reqParam >> " + CodeUtil.toJson(reqParam)));
			bs = new BaseResult();

			bs = NI02_REST(reqParam);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			log.error("cash_pw_resend_result>>", e);
		}
		return bs;
	}

	/**
	 * 申請信用卡進度查詢
	 * 
	 * @param cusidn
	 * @return
	 */
	public BaseResult apply_creditcard_progress(String cusidn) {
		log.trace("apply_creditcard_progress start");
		log.trace("cusidn = {} ", cusidn);

		BaseResult bs = null;
		try {
			bs = new BaseResult();
			List<TXNCARDAPPLY> applyCases = txnCardApplyDao.findByCUSID(cusidn);
			if (applyCases.size() > 0) {
				I18nHelper i18nHelper = new I18nHelper();
				List<Map<String, String>> rec = new ArrayList<>();

				for (TXNCARDAPPLY po : applyCases) {
					// 利用json將null設為空字串
					String json = CodeUtil.toJson(po);
					Map<String, String> row = CodeUtil.fromJson(json, Map.class);

					// 審核狀況 TODO i18n
					String status = i18nHelper.getNA021_Status(po.getSTATUS());
					row.put("STATUS", status);

					// 卡片中文 TODO i18n
					row.put("CARDNAME1", getCardName(row.get("CARDNAME1")));
					row.put("CARDNAME2", getCardName(row.get("CARDNAME2")));
					row.put("CARDNAME3", getCardName(row.get("CARDNAME3")));
					rec.add(row);
				}

				String CMQTIME = DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss");
				String CMRECNUM = String.valueOf(applyCases.size());
				Map<String, Object> bsData = new HashMap<>();
				bsData.put("CMQTIME", CMQTIME);
				bsData.put("CMRECNUM", CMRECNUM);
				bsData.put("REC", rec);
				bs.setData(bsData);
				bs.setResult(true);
				bs.setMessage("0000", i18n.getMsg("X1805"));//查詢成功
			} else {
				bs.setErrorMessage("ENRD", i18n.getMsg("LB.Check_no_data"));//查無資料
			}
		} catch (Exception e) {
			log.error("apply_creditcard_progress error", e);
		}
		return bs;
	}

	/**
	 * 取得卡片中文 TODO i18n
	 * 
	 * @param cardNameNo
	 * @return
	 */
	public String getCardName(String cardNameNo) {
		String result = cardNameNo;
		try {
			result = StrUtils.isEmpty(cardNameNo) ? "" : creditUtils.getCardNameByCardno(cardNameNo);
		} catch (Exception e) {
			log.error("getCardName error", e);
		}
		return result;
	}

	/**
	 * 申請書閱覽
	 * 
	 * @param rcvno
	 * @return
	 */
	public BaseResult openApplication(String rcvno) {
		log.trace("openApplication");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			List<TXNCARDAPPLY> rows = txnCardApplyDao.findByRCVNO(rcvno);
			log.trace(ESAPIUtil.vaildLog("List<TXNCARDAPPLY> >> " + CodeUtil.toJson(rows)));

			if (rows.size() > 0) {
				TXNCARDAPPLY po = covertNulltoEmpty(TXNCARDAPPLY.class, rows.get(0));

				Map<String, Object> bsData = new HashMap<>();

				// 在bsData內，設定判斷參數，決定顯示的文字內容
				setDisplayValue(bsData, po);

				// 設定申請信用卡卡別
				setCardType(bsData, po);

				// 客戶狀態
				bsData.put("CFU2", getCFU2(po.getCFU2()));

				// 教育程度
				bsData.put("MPRIMEDUCATION", getMPRIMEDUCATION(po.getMPRIMEDUCATION()));

				// 婚姻狀況
				bsData.put("MPRIMMARRIAGE", getMPRIMMARRIAGE(po.getMPRIMMARRIAGE()));

				// 戶籍電話A 區碼
				bsData.put("CPRIMHOMETELNO2A", getCPRIMHOMETELNO2A(po.getCPRIMHOMETELNO2A()));
				// 戶籍電話B
				bsData.put("CPRIMHOMETELNO2B", getCPRIMHOMETELNO2B(po.getCPRIMHOMETELNO2B()));

				// 居住電話A 區碼
				bsData.put("CPRIMHOMETELNO1A", getCPRIMHOMETELNO1A(po.getCPRIMHOMETELNO1A()));
				// 居住電話B
				bsData.put("CPRIMHOMETELNO1B", getCPRIMHOMETELNO1B(po.getCPRIMHOMETELNO1B()));

				// 現居房屋
				bsData.put("MPRIMADDR1COND", getMPRIMADDR1COND(po.getMPRIMADDR1COND()));

				// 帳單地址
				bsData.put("MBILLTO", getMBILLTO(po.getMBILLTO()));

				// 卡片領取方式
				bsData.put("MCARDTO", getMCARDTO(po.getMCARDTO()));

				// 職稱
				bsData.put("CPRIMJOBTITLE", getCPRIMJOBTITLE(po.getCPRIMJOBTITLE()));

				// 年資
				bsData.put("WORKYEARS", getWORKYEARS(po.getWORKYEARS()));
				bsData.put("WORKMONTHS", getWORKMONTHS(po.getWORKMONTHS()));

				// 公司電話
				bsData.put("CPRIMOFFICETELNO1B", getCPRIMOFFICETELNO1B(po.getCPRIMOFFICETELNO1B()));
				// 公司電話-分機
				bsData.put("CPRIMOFFICETELNO1C", getCPRIMOFFICETELNO1C(po.getCPRIMOFFICETELNO1C()));

				String CMQTIME = DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss");
				String CMRECNUM = String.valueOf(rows.size());
				bsData.put("CMQTIME", CMQTIME);
				bsData.put("CMRECNUM", CMRECNUM);
				bsData.put("PO", po);
				bs.setData(bsData);
				bs.setResult(true);
				bs.setMessage("0000", i18n.getMsg("LB.X1805"));//查詢成功
			} else {
				bs.setErrorMessage("TNRD", i18n.getMsg("LN.Check_no_data"));//查無資料
			}
		} catch (Exception e) {
			log.error("openApplication error", e);
		}
		return bs;
	}

	/**
	 * 利用gson將null設為空字串
	 * 
	 * @param to
	 * @param from
	 * @return
	 */
	public <T> T covertNulltoEmpty(Class<T> to, Object from) {
		T t = CodeUtil.objectCovert(to, from);
		return t;
	}

	/**
	 * 客戶狀態 TODO i18n
	 * 
	 * @param CFU2
	 * @return
	 */
	public String getCFU2(String CFU2) {
		return CFU2.equals("2") ? i18n.getMsg("LB.D0134") : i18n.getMsg("LB.X2143");
	}

	/**
	 * 戶籍電話A 區碼
	 * 
	 * @param CPRIMHOMETELNO2A
	 * @return
	 */
	public String getCPRIMHOMETELNO2A(String CPRIMHOMETELNO2A) {
		return CPRIMHOMETELNO2A.length() > 0 ? CPRIMHOMETELNO2A : "";
	}

	/**
	 * 戶籍電話B
	 * 
	 * @param CPRIMHOMETELNO2B
	 * @return
	 */
	public String getCPRIMHOMETELNO2B(String CPRIMHOMETELNO2B) {
		return CPRIMHOMETELNO2B.length() > 0 ? ("－" + CPRIMHOMETELNO2B) : "";
	}

	/**
	 * 居住電話A 區碼
	 * 
	 * @param CPRIMHOMETELNO1A
	 * @return
	 */
	public String getCPRIMHOMETELNO1A(String CPRIMHOMETELNO1A) {
		return CPRIMHOMETELNO1A.length() > 0 ? CPRIMHOMETELNO1A : "";
	}

	/**
	 * 居住電話B
	 * 
	 * @param CPRIMHOMETELNO1B
	 * @return
	 */
	public String getCPRIMHOMETELNO1B(String CPRIMHOMETELNO1B) {
		return CPRIMHOMETELNO1B.length() > 0 ? ("－" + CPRIMHOMETELNO1B) : "";
	}

	/**
	 * 現居房屋 TODO i18n
	 * 
	 * @param MPRIMADDR1COND
	 * @return
	 */
	public String getMPRIMADDR1COND(String MPRIMADDR1COND) {
		String result = MPRIMADDR1COND;
		switch (MPRIMADDR1COND) {
		case "#":
			result = "";
			break;
		case "1":
			result = i18n.getMsg("LB.X0548");//本人或配偶持有
			break;
		case "2":
			result = i18n.getMsg("LB.D0606");//直系親屬持有
			break;
		case "3":
			result = i18n.getMsg("LB.D0068");//租賃或宿舍
			break;
		default:
		}
		return result;
	}

	/**
	 * 帳單地址 TODO i18n
	 * 
	 * @param MBILLTO
	 * @return
	 */
	public String getMBILLTO(String MBILLTO) {
		String result = MBILLTO;
		switch (MBILLTO) {
		case "#":
			result = "";
			break;
		case "1":
			result = i18n.getMsg("LB.D1125");//同戶籍地址
			break;
		case "2":
			result = i18n.getMsg("LB.X0552");//同居住地址
			break;
		case "3":
			result = i18n.getMsg("LB.X0553");//同公司地址
			break;
		default:
		}
		return result;
	}

	/**
	 * 卡片領取方式 TODO i18n
	 * 
	 * @param MCARDTO
	 * @return
	 */
	public String getMCARDTO(String MCARDTO) {
		String result = MCARDTO;
		switch (MCARDTO) {
		case "#":
			result = "";
			break;
		case "1":
			result = i18n.getMsg("LB.D0072");//親領	
			break;
		case "2":
			result = i18n.getMsg("LB.X1819");//掛號郵寄
			break;
		default:
		}
		return result;
	}

	/**
	 * 婚姻狀況 TODO i18n
	 * 
	 * @param MPRIMMARRIAGE
	 * @return
	 */
	public String getMPRIMMARRIAGE(String MPRIMMARRIAGE) {
		String result = MPRIMMARRIAGE;
		switch (MPRIMMARRIAGE) {
		case "#":
			result = "";
			break;
		case "1":
			result = i18n.getMsg("LB.D0595");//已婚
			break;
		case "2":
			result = i18n.getMsg("LB.D0596");//未婚
			break;
		default:
		}
		return result;
	}

	/**
	 * 教育程度 TODO i18n
	 * 
	 * @param MPRIMEDUCATION
	 * @return
	 */
	public String getMPRIMEDUCATION(String MPRIMEDUCATION) {
		String result = MPRIMEDUCATION;
		switch (MPRIMEDUCATION) {
		case "#":
			result = "";
			break;
		case "1":
			result = i18n.getMsg("LB.D0588");//博士
			break;
		case "2":
			result = i18n.getMsg("LB.D0589");//碩士
			break;
		case "3":
			result = i18n.getMsg("LB.D0590");//大學
			break;
		case "4":
			result = i18n.getMsg("LB.D0591");//專科
			break;
		case "5":
			result = i18n.getMsg("LB.X1820");//高中、高職
			break;
		case "6":
			result = i18n.getMsg("LB.D0572");//其他
			break;
		default:
		}
		return result;
	}

	/**
	 * 職稱 TODO i18n
	 * 
	 * @param CPRIMJOBTITLE
	 * @return
	 */
	public String getCPRIMJOBTITLE(String CPRIMJOBTITLE) {
		String result = CPRIMJOBTITLE;
		Map<String, String> profNo = NA01.get("PROFNO");
		if (profNo.containsKey(CPRIMJOBTITLE)) {
			// TODO 待messages_zh_TW.properties新增相應的字串後打開
			// I18n i18n = SpringBeanFactory.getBean("i18n");
			// result = i18n.getMsg(LB.XXX);

			// TODO 待messages_zh_TW.properties新增相應的字串後移除
			result = profNo.get(CPRIMJOBTITLE);
		}
		return result;
	}

	/**
	 * 年資-年
	 * 
	 * @param WORKYEARS
	 * @return
	 */
	public String getWORKYEARS(String WORKYEARS) {
		String year = i18n.getMsg("LB.Year");
		return (WORKYEARS.length() > 0 && !WORKYEARS.equals("#")) ? (WORKYEARS + year) : "";
	}

	/**
	 * 年資-月
	 * 
	 * @param WORKMONTHS
	 * @return
	 */
	public String getWORKMONTHS(String WORKMONTHS) {
		String month = i18n.getMsg("LB.Month");
		return (WORKMONTHS.length() > 0 && !WORKMONTHS.equals("#")) ? (WORKMONTHS + month) : "";
	}

	/**
	 * 公司電話
	 * 
	 * @param CPRIMOFFICETELNO1B
	 * @return
	 */
	public String getCPRIMOFFICETELNO1B(String CPRIMOFFICETELNO1B) {
		return CPRIMOFFICETELNO1B.length() > 0 ? ("－" + CPRIMOFFICETELNO1B) : "";
	}

	/**
	 * 公司電話-分機 TODO i18n
	 * 
	 * @param CPRIMOFFICETELNO1C
	 * @return
	 */
	public String getCPRIMOFFICETELNO1C(String CPRIMOFFICETELNO1C) {
		return CPRIMOFFICETELNO1C.length() > 0 ? (i18n.getMsg("LB.D0095")+"：" + CPRIMOFFICETELNO1C) : "";//分機
	}

	/**
	 * 申請信用卡卡別 欄位文字處理
	 * 
	 * @param bsData
	 * @param po
	 */
	public void setCardType(Map<String, Object> bsData, TXNCARDAPPLY po) {
		String CN1 = String.valueOf(bsData.get("CN1"));
		String CN2 = String.valueOf(bsData.get("CN2"));
		String CN3 = String.valueOf(bsData.get("CN3"));
		String CARDDESC1 = String.valueOf(bsData.get("CARDDESC1"));
		String CARDDESC2 = String.valueOf(bsData.get("CARDDESC2"));
		String CARDDESC3 = String.valueOf(bsData.get("CARDDESC3"));
		String cardType1 = CN1.equals("") ? "" : CN1 + CARDDESC1;
		String cardType2 = CN2.equals("") ? "" : CN2 + CARDDESC2;
		String cardType3 = CN3.equals("") ? "" : CN3 + CARDDESC3;
		bsData.put("cardType1", cardType1);
		bsData.put("cardType2", cardType2);
		bsData.put("cardType3", cardType3);
	}

	/**
	 * 設定JSP頁面顯示文字，要用來判斷的參數
	 * 
	 * @param bsData
	 * @param po
	 */
	public void setDisplayValue(Map<String, Object> bsData, TXNCARDAPPLY po) {
		setCARDMEMO1(bsData, po);
		setCARDMEMO2(bsData, po);
		setCARDMEMO3(bsData, po);

		setCN1(bsData, po);
		setCN2(bsData, po);
		setCN3(bsData, po);

		setCARDDESC1(bsData, po);
		setCARDDESC2(bsData, po);
		setCARDDESC3(bsData, po);
	}

	public void setCARDMEMO1(Map<String, Object> bsData, TXNCARDAPPLY po) {
		String CARDNAME1 = po.getCARDNAME1();
		String CARDMEMO1 = CARDNAME1.trim().equals("") || CARDNAME1.equals("#") ? "0"
				: creditUtils.getCardMemoByCardno(CARDNAME1);
		bsData.put("CARDMEMO1", CARDMEMO1);
		log.trace(ESAPIUtil.vaildLog("setCARDMEMO1 >> " + CARDMEMO1));
	}

	public void setCARDMEMO2(Map<String, Object> bsData, TXNCARDAPPLY po) {
		String CARDNAME2 = po.getCARDNAME2();
		String CARDMEMO2 = CARDNAME2.trim().equals("") || CARDNAME2.equals("#") ? "0"
				: creditUtils.getCardMemoByCardno(CARDNAME2);
		bsData.put("CARDMEMO2", CARDMEMO2);
		log.trace(ESAPIUtil.vaildLog("setCARDMEMO2 >> " + CARDMEMO2));
	}

	public void setCARDMEMO3(Map<String, Object> bsData, TXNCARDAPPLY po) {
		String CARDNAME3 = po.getCARDNAME3();
		String CARDMEMO3 = CARDNAME3.trim().equals("") || CARDNAME3.equals("#") ? "0"
				: creditUtils.getCardMemoByCardno(CARDNAME3);
		bsData.put("CARDMEMO3", CARDMEMO3);
		log.trace(ESAPIUtil.vaildLog("setCARDMEMO3 >> " + CARDMEMO3));
	}

	public void setCN1(Map<String, Object> bsData, TXNCARDAPPLY po) {
		String CARDNAME1 = po.getCARDNAME1();
		String CN1 = CARDNAME1.trim().equals("") || CARDNAME1.equals("#") ? ""
				: creditUtils.getCardNameByCardno(CARDNAME1);
		bsData.put("CN1", CN1);
		log.trace(ESAPIUtil.vaildLog("setCN1 >> " + CN1));
	}

	public void setCN2(Map<String, Object> bsData, TXNCARDAPPLY po) {
		String CARDNAME2 = po.getCARDNAME2();
		String CN2 = CARDNAME2.trim().equals("") || CARDNAME2.equals("#") ? ""
				: creditUtils.getCardNameByCardno(CARDNAME2);
		bsData.put("CN2", CN2);
		log.trace(ESAPIUtil.vaildLog("setCN2 >> " + CN2));
	}

	public void setCN3(Map<String, Object> bsData, TXNCARDAPPLY po) {
		String CARDNAME3 = po.getCARDNAME2();
		String CN3 = CARDNAME3.trim().equals("") || CARDNAME3.equals("#") ? ""
				: creditUtils.getCardNameByCardno(CARDNAME3);
		bsData.put("CN3", CN3);
		log.trace(ESAPIUtil.vaildLog("setCN3 >> " + CN3));
	}

	// TODO i18n
	public void setCARDDESC1(Map<String, Object> bsData, TXNCARDAPPLY po) {
		String CARDDESC1 = "";
		String CARDMEMO1 = String.valueOf(bsData.get("CARDMEMO1"));
		String VARSTR2 = po.getVARSTR2();
		if (CARDMEMO1.equals("1") && VARSTR2.equals("1")) {
			CARDDESC1 = "("+i18n.getMsg("LB.X1821")+")";//悠遊卡自動加值功能：同意預設開啟
		}
		if (CARDMEMO1.equals("1") && VARSTR2.equals("2")) {
			CARDDESC1 = "("+i18n.getMsg("LB.X1822")+")";//悠遊卡自動加值功能：不同意預設開啟
		}
		if (CARDMEMO1.equals("2")) {
			CARDDESC1 = "("+i18n.getMsg("LB.X1823")+")";//一卡通自動加值功能：預設開啟
		}
		bsData.put("CARDDESC1", CARDDESC1);
	}

	// TODO i18n
	public void setCARDDESC2(Map<String, Object> bsData, TXNCARDAPPLY po) {
		String CARDDESC2 = "";
		String CARDMEMO2 = String.valueOf(bsData.get("CARDMEMO2"));
		String VARSTR2 = po.getVARSTR2();

		if (CARDMEMO2.equals("1") && VARSTR2.equals("1")) {
			CARDDESC2 = "("+i18n.getMsg("LB.X1821")+")";//悠遊卡自動加值功能：同意預設開啟
		}
		if (CARDMEMO2.equals("1") && VARSTR2.equals("2")) {
			CARDDESC2 = "("+i18n.getMsg("LB.X1822")+")";//悠遊卡自動加值功能：不同意預設開啟
		}
		if (CARDMEMO2.equals("2")) {
			CARDDESC2 = "("+i18n.getMsg("LB.X1823")+")";//一卡通自動加值功能：預設開啟
		}
		bsData.put("CARDDESC2", CARDDESC2);
	}

	// TODO i18n
	public void setCARDDESC3(Map<String, Object> bsData, TXNCARDAPPLY po) {
		String CARDDESC3 = "";
		String CARDMEMO3 = String.valueOf(bsData.get("CARDMEMO3"));
		String VARSTR2 = po.getVARSTR2();

		if (CARDMEMO3.equals("1") && VARSTR2.equals("1")) {
			CARDDESC3 = "("+i18n.getMsg("LB.X1821")+")";//悠遊卡自動加值功能：同意預設開啟
		}
		if (CARDMEMO3.equals("1") && VARSTR2.equals("2")) {
			CARDDESC3 = "("+i18n.getMsg("LB.X1822")+")";//悠遊卡自動加值功能：不同意預設開啟
		}
		if (CARDMEMO3.equals("2")) {
			CARDDESC3 ="("+i18n.getMsg("LB.X1823")+")";//一卡通自動加值功能：預設開啟
		}
		bsData.put("CARDDESC3", CARDDESC3);
	}

	/**
	 * 取得信用卡之轉出帳號
	 * 
	 * @param cusidn
	 * @param type
	 * @return
	 */
	public BaseResult getOutAcnoList(String cusidn, String type) {
		log.trace("acno>>{} ", cusidn);
		log.trace("cusidn>>{} ", cusidn);
		BaseResult bs = null;
		Map<String, Object> tmpData = null;
		List<String> acnoList = new ArrayList<String>();
		List<Map<String, String>> acnoList2 = null;
		try {
			bs = new BaseResult();
			bs = N920_REST(cusidn, type);

			if (bs != null && bs.getResult()) {
				tmpData = (Map) bs.getData();
				acnoList2 = (List<Map<String, String>>) tmpData.get("REC");

				for (Map<String, String> acMap : acnoList2) {
					String ACN = acMap.get("ACN");
					acnoList.add(ACN);
				}

				bs.setData(acnoList);

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			log.error("getOutAcnoList error >> {}", e);
		}
		return bs;

	}

	/**
	 * 信用卡申請的信用卡查詢
	 */
	@SuppressWarnings("unchecked")
	public BaseResult query_creditCard_data(Map<String, String> requestParam) {
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = NA03Q_REST(requestParam);
//			log.trace("BSRESULT>>>>>{}",bs.getResult());
			Map<String, String> bsData = (Map<String, String>) bs.getData();
			String occurMsg = (bsData.get("occurMsg") == null ? "" : bsData.get("occurMsg"));
			log.trace("occurMsg>>>>>{}",occurMsg);
			log.trace("msgCode>>>>>{}",bsData.get("msgCode"));
			
			if(occurMsg.equals("") || occurMsg.equals("0000")) {
				if(bsData.get("msgCode").startsWith("Z") || (bsData.get("msgCode").startsWith("FE") && bsData.get("msgCode").length() == 6)) {
					bs.setResult(false);
					bs.setMsgCode(bsData.get("msgCode"));
					getMessageByMsgCode(bs);
					return bs;
				}
				else {
					bs = prepareCardSelectData(requestParam, bs);
					bs.setResult(true);
				}
			}
			else {
				bs.setResult(false);
				bs.setMsgCode(bsData.get("occurMsg"));
				getMessageByMsgCode(bs);
				return bs;
			}
			
			//20210126 新增   重複申請新用卡判斷 START 
			StringBuffer applyNameList = new StringBuffer();
			String cusidn = requestParam.get("CUSIDN");
			List<TXNCARDAPPLY> applyList = txnCardApplyDao.findByCUSID(cusidn);
			if(applyList.size()>0) {
				for(TXNCARDAPPLY eachCard:applyList) {
					if("012".contains(eachCard.getSTATUS())) {
						if(applyNameList.toString().length()==0) {
							applyNameList.append(eachCard.getCARDNAME1());
						}else {
							applyNameList.append(","+eachCard.getCARDNAME1());
						}
					}
				}
			}
			bs.addData("ApplingCardListStr", applyNameList.toString());
			//20210126 新增   重複申請新用卡判斷 END
			
			
		} catch (Exception e) {
			//e.printStackTrace();
			//Avoid Information Exposure Through an Error Message
			log.error("query_creditCard_data error >> {}",e); 
		}
//		bs = prepareCardSelectData(requestParam, bs);
		return bs;
	}

	/**
	 * 準備申請信用卡卡片選擇頁的資料
	 */
	@SuppressWarnings("unchecked")
	public BaseResult prepareCardSelectData(Map<String, String> requestParam, BaseResult bs) {
		try {
			Map<String, Object> bsData = (Map<String, Object>) bs.getData();
			Map<String, String> n810row = (Map<String, String>) bs.getData();
			List<Map<String, String>> rows = (List<Map<String, String>>) bsData.get("REC");
	
			// 定義活卡條件代碼
			Set<String> set_ACTCardType = new HashSet<String>();
			set_ACTCardType.add("P");
			set_ACTCardType.add("V");
			set_ACTCardType.add("C");
			set_ACTCardType.add("N");
			set_ACTCardType.add("H");
			set_ACTCardType.add("");
	
			// 是否有活卡
			boolean oldcardowner = false;
	
			Set<String> set_CardType = new HashSet<String>();
			int n810cnt = (n810row.get("RECNUM") == null || n810row.get("RECNUM").equals("") ? 0 : Integer.parseInt(n810row.get("RECNUM")));
			log.trace("RECNUM>>>{}",n810cnt);
			
			for (int i = 0; i < n810cnt; i++) {
				String CARDTYPE = rows.get(i).get("CARDTYPE");
				log.debug("CARDTYPE={}", CARDTYPE);
				String PT_FLG = rows.get(i).get("PT_FLG");
				log.debug("PT_FLG={}", PT_FLG);
				String BLK_CODE = rows.get(i).get("BLK_CODE");
				log.debug("BLK_CODE={}", BLK_CODE);
	
				// 需為正卡
				if ("1".equals(PT_FLG)) {
					set_CardType.add(CARDTYPE);
					if (set_ACTCardType.contains(BLK_CODE.trim())) {
						oldcardowner = true;
					}
				}
//				else {
//					set_CardType.add("");
//				}
			}
//			for (int i = 0; i < rows.size(); i++) {
//				String CARDTYPE = rows.get(i).get("CARDTYPE");
//				log.debug("CARDTYPE={}", CARDTYPE);
//				String PT_FLG = rows.get(i).get("PT_FLG");
//				log.debug("PT_FLG={}", PT_FLG);
//				String BLK_CODE = rows.get(i).get("BLK_CODE");
//				log.debug("BLK_CODE={}", BLK_CODE);
//				
//				set_CardType.add(CARDTYPE);
//				
//				// 需為正卡
//				if ("1".equals(PT_FLG)) {
//					if (set_ACTCardType.contains(BLK_CODE.trim())) {
//						oldcardowner = true;
//					}
//				}
//			}
	
			requestParam.put("oldcardownerBoolean", String.valueOf(oldcardowner));
	
			if (oldcardowner == true) {
				requestParam.put("oldcardowner", "Y");
				requestParam.put("CFU2", "2");
			} else {
				requestParam.put("oldcardowner", "N");
				requestParam.put("CFU2", "1");
			}
	
			String CARDTYPE = rows.get(0).get("CARDTYPE");
//			String CARDTYPE = "200";
			log.debug(ESAPIUtil.vaildLog("CARDTYPE >> " + CARDTYPE));
			String QRCODE = requestParam.get("QRCODE");
			log.debug(ESAPIUtil.vaildLog("QRCODE >> " + QRCODE));
			
			String BRANCH = requestParam.get("BRANCH");
			log.debug(ESAPIUtil.vaildLog("BRANCH >> " + BRANCH));
	
			bs.addData("requestParam", requestParam);
	
			// 判斷手機從QRCODE或官網連結進入，再判斷新戶或舊戶以來顯示不同卡片
	
			// 找一般卡卡別
			List<ADMCARDDATA> list = admCardDataDao.getNormalCard();
	
			List<Map<String, String>> normalCardList = new ArrayList<Map<String, String>>();
			normalCardList = CodeUtil.objectCovert(normalCardList.getClass(), list);
			log.debug(ESAPIUtil.vaildLog("normalCardList="+ normalCardList));
			bs.addData("normalCardList", normalCardList);
	
			// 新戶
			if (oldcardowner == false) {
				// 假的
				// if(true){
				// 從QRCODE連結進入，只能顯示銀色之愛信用卡
				if ("Y".equals(QRCODE)) {
					
					// 假的
					// if(false){
					list = admCardDataDao.getNormalSilverCard("68", "69");
	
					List<Map<String, String>> normalSilverCardList = new ArrayList<Map<String, String>>();
					normalSilverCardList = CodeUtil.objectCovert(normalSilverCardList.getClass(), list);
					log.debug(ESAPIUtil.vaildLog("normalSilverCardList="+ normalSilverCardList));
					bs.addData("normalSilverCardList", CodeUtil.toJson(normalSilverCardList));
	
				}
				//獅友會
				else if ("L".equals(QRCODE)) {
					list = admCardDataDao.getNormalSilverCard("85", "86");
					List<Map<String, String>> normalSilverCardList = new ArrayList<Map<String, String>>();
					normalSilverCardList = CodeUtil.objectCovert(normalSilverCardList.getClass(), list);
					log.debug(ESAPIUtil.vaildLog("normalSilverCardList="+ normalSilverCardList));
					bs.addData("normalSilverCardList", CodeUtil.toJson(normalSilverCardList));
				}
				//FOR 特定BRANCH
				else if ("040".equals(BRANCH)) {
					//20220117 移除永續生活悠遊普卡類別
//					List<ADMCARDDATA> list2 = admCardDataDao.getNormalSilverCard("26", "41");
					List<ADMCARDDATA> list1 = admCardDataDao.getNormalSilverCard("38", "48");
					List<Map<String, String>> normalSilverCardList = new ArrayList<Map<String, String>>();
					normalSilverCardList = CodeUtil.objectCovert(normalSilverCardList.getClass(), list1);
//					normalSilverCardList.addAll(CodeUtil.objectCovert(normalSilverCardList.getClass(), list2));
					log.debug(ESAPIUtil.vaildLog("normalSilverCardList=" + normalSilverCardList));
					bs.addData("normalSilverCardList", CodeUtil.toJson(normalSilverCardList));
				}
				// 從官網連結進入，不能顯示已申請信用卡的群組
				else {
					// 顯示所有卡片
					set_CardType.add("0");
					// 找一般卡卡別
					List<Map<String, String>> otherCardList = admCardDataDao.getOtherCardGroupDATA(set_CardType);
					log.debug(ESAPIUtil.vaildLog("otherCardList=" + otherCardList));
					bs.addData("otherCardList", CodeUtil.toJson(otherCardList));
				}
			}
			// 舊戶，不能顯示已申請信用卡的群組
			else {
				//獅友會
				if ("L".equals(QRCODE)) {
					list = admCardDataDao.getNormalSilverCard("85", "86");
					List<Map<String, String>> normalSilverCardList = new ArrayList<Map<String, String>>();
					normalSilverCardList = CodeUtil.objectCovert(normalSilverCardList.getClass(), list);
					log.debug(ESAPIUtil.vaildLog("normalSilverCardList="+ normalSilverCardList));
					bs.addData("normalSilverCardList", CodeUtil.toJson(normalSilverCardList));
				}
				//FOR 特定BRANCH
				else if ("040".equals(BRANCH)) {
					List<ADMCARDDATA> list2 = admCardDataDao.getNormalSilverCard("26", "41");
					List<ADMCARDDATA> list1 = admCardDataDao.getNormalSilverCard("38", "48");
					List<Map<String, String>> normalSilverCardList = new ArrayList<Map<String, String>>();
					normalSilverCardList = CodeUtil.objectCovert(normalSilverCardList.getClass(), list1);
					normalSilverCardList.addAll(CodeUtil.objectCovert(normalSilverCardList.getClass(), list2));
					log.debug(ESAPIUtil.vaildLog("normalSilverCardList=" + normalSilverCardList));
					bs.addData("normalSilverCardList", CodeUtil.toJson(normalSilverCardList));
				}
				else {
					// 找一般卡卡別
					List<Map<String, String>> otherCardList = admCardDataDao.getOtherCardGroupDATA(set_CardType);
					log.debug(ESAPIUtil.vaildLog("otherCardList=" + otherCardList));
					bs.addData("otherCardList", CodeUtil.toJson(otherCardList));
				}
			}
			
			//獅友卡分會資料
//			if("L".equals(QRCODE)) {
				List<Map<String, String>> lionList = getLionList();
				if (!lionList.isEmpty()) {
					bs.addData("lionList", lionList);
				}
//			}
		}catch(Exception e) {
			log.debug(e.toString());
		}
		return bs;
	}

	/**
	 * 準備申請信用卡約定條款同意頁的資料
	 */
	public BaseResult prepareContactData(Map<String, String> requestParam) {
		log.info("prepareContactData req >> {}", requestParam);
		BaseResult bs = new BaseResult();
		try {
			
			String cusidn = requestParam.get("CUSIDN");
			String CARDNAME = requestParam.get("CARDNAME");

			String CARDMEMO = admCardDataDao.findCARDMEMO(CARDNAME);
			requestParam.put("CARDMEMO", CARDMEMO);

			List<TXNUSER> txnUserList = txnUserDao.findByUserId(cusidn);

			String useremail = "";
			if (!txnUserList.isEmpty()) {
				useremail = txnUserList.get(0).getDPMYEMAIL();
			}
			requestParam.put("DPMYEMAIL", useremail);

			boolean smartcard1 = false;// 悠遊卡
			boolean smartcard2 = false;// 一卡通

			if ("1".equals(CARDMEMO)) {
				smartcard1 = true;
			}
			if ("2".equals(CARDMEMO)) {
				smartcard2 = true;
			}
			requestParam.put("smartcard1", String.valueOf(smartcard1));
			requestParam.put("smartcard2", String.valueOf(smartcard2));
			bs.addData("requestParam", requestParam);
			bs.setResult(true);
		} catch (Exception e) {
			//e.printStackTrace();
			//Avoid Information Exposure Through an Error Message
			log.error("prepareContactData error >> {}",e); 
		}
		return bs;
	}

	/**
	 * 回到信用卡申請的選擇頁
	 */
	@SuppressWarnings("unchecked")
	public BaseResult query_creditCard_data2(Map<String, String> requestParam, Model model) {
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = NA02Q_REST(requestParam);
			if (bs != null && bs.getResult()) {
//				Map<String, Object> bsData = (Map<String, Object>) bs.getData();

				bs = prepareCardSelectData(requestParam, bs);
				bs.setResult(true);
			}
		} catch (Exception e) {
			//e.printStackTrace();
			//Avoid Information Exposure Through an Error Message
			log.error("query_creditCard_data2 error >> {}",e); 
		}
		return bs;
	}

	/**
	 * 準備申請信用卡輸入頁的資料
	 */
	@SuppressWarnings("unchecked")
	public BaseResult prepareInputData(Map<String, String> requestParam) {
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			String birthday = requestParam.get("CPRIMBIRTHDAY");
			log.debug(ESAPIUtil.vaildLog("birthday >> " + birthday));

			String oldcardowner = requestParam.get("oldcardowner");
			log.debug(ESAPIUtil.vaildLog("oldcardowner >> " + oldcardowner));

			String CARDMEMO = requestParam.get("CARDMEMO");
			log.debug(ESAPIUtil.vaildLog("CARDMEMO >> " + CARDMEMO));

			String VARSTR2 = requestParam.get("VARSTR2");
			log.debug(ESAPIUtil.vaildLog("VARSTR2 >> " + VARSTR2));

			String USERNAME = "";
			String BRTHDY = "";
			String BIRTHY = "";
			String BIRTHM = "";
			String BIRTHD = "";
			String POSTCOD1 = "";
			String PMTADR = "";
			String PMTADR1 = "";
			String POSTCOD2 = "";
			String CTTADR = "";
			String CTTADR1 = "";
			String TEL11 = "";
			String TEL12 = "";
			String TEL13 = "";
			String MOBTEL = "";
			String SMSA = "";

			// 為舊戶 改打C900拿值
			if ("Y".equals(oldcardowner)) {
				bs = change_data_service.credit_data_detail(requestParam.get("CUSIDN"), requestParam);
				Map<String, String> bsMap = (Map<String, String>) bs.getData();
				
				//去掉半形全形空白
				for(String key:bsMap.keySet()) {
					bsMap.put(key, bsMap.get(key).replaceAll(" ","").replaceAll("　", ""));
				}
				
				//處理姓名
				if (StrUtils.isNotEmpty(bsMap.get("CUSTNM"))) {
					USERNAME = bsMap.get("CUSTNM");
					// 姓名長度超過5，只抓五位
					if (bsMap.get("CUSTNM").length() > 5) {
						USERNAME = bsMap.get("CUSTNM").substring(0, 5);
					}
					bsMap.put("CPRIMCHNAME", bsMap.get("CUSTNM"));
				}
				//處理郵件寄送地址
				if (StrUtils.isNotEmpty(bsMap.get("SMSA"))) {
					SMSA = bsMap.get("SMSA");
					if (bsMap.get("SMSA").equals("0001")) {
						// 戶籍地址
						bsMap.put("MBILLTO", "1");
						bsMap.put("strBILLTITLE", "LB.D0143");
						bsMap.put("CPRIMADDR0", bsMap.get("ADDR1") + bsMap.get("ADDR2"));
						bsMap.put("strZIP", bsMap.get("ZIPCODE").substring(0, 3));
					} else if (bsMap.get("SMSA").equals("0002")) {
						// 居住地址
						bsMap.put("MBILLTO", "2");
						bsMap.put("strBILLTITLE", "LB.D0063");
						bsMap.put("CPRIMADDR0", bsMap.get("CITYADDR1").replaceAll(" ", ""));
						bsMap.put("strZIP", bsMap.get("ZIPCODE").substring(3, 6));
					} else {
						// 公司地址
						bsMap.put("MBILLTO", "3");
						bsMap.put("strBILLTITLE", "LB.D0090");
						bsMap.put("CPRIMADDR0",
								(bsMap.get("OFFICEADDR1") + bsMap.get("OFFICEADDR2")).replaceAll(" ", ""));
						bsMap.put("strZIP", bsMap.get("ZIPCODE").substring(6, 9));
					}
				}
				
				bs.addAllData(bsMap);
				//處理生日
				if (StrUtils.isNotEmpty(bsMap.get("BIRTHDAY"))) {
					BRTHDY = bsMap.get("BIRTHDAY"); 
					BIRTHY = BRTHDY.substring(4, 8);
					BIRTHM = BRTHDY.substring(2, 4);
					BIRTHD = BRTHDY.substring(0, 2);
					
					requestParam.put("BIRTHY", BIRTHY);
					requestParam.put("BIRTHM", BIRTHM);
					requestParam.put("BIRTHD", BIRTHD);
					requestParam.put("CPRIMBIRTHDAY", BIRTHY + BIRTHM + BIRTHD);
					
					String YY = BIRTHY;
					String MM = "";
					String DD = BIRTHD;
					
					//語系判斷
					String locale = LocaleContextHolder.getLocale().toString();
					
					if(locale.equals("en")) {
						MM = Month(BIRTHM);
						requestParam.put("CPRIMBIRTHDAYshow",YY + " " + MM + " " + DD);
					}
					else {
						MM = BIRTHM;
						requestParam.put("CPRIMBIRTHDAYshow",YY + "年 " + MM + "月 " + DD + "日");
					}
					log.trace("date>>>>" + YY + MM + DD);
				}
//				requestParam.put("NCHECKNB", "Y");
//				bs = N960_REST_UPDATE_CUSNAME(requestParam);
//				log.debug("N960 msgcode>>>" + bs.getMsgCode());
//				if (bs != null && bs.getResult()) {
//					Map<String, Object> bsData = (Map<String, Object>) bs.getData();
//
//					USERNAME = (String) bsData.get("NAME");
//					log.debug("USERNAME={}", USERNAME);
//
//					// 姓名長度超過5，只抓五位
//					if (USERNAME.length() > 5) {
//						USERNAME = USERNAME.substring(0, 5);
//					}
//					log.debug("USERNAME={}", USERNAME);
//					requestParam.put("USERNAME", USERNAME);
//
//					BRTHDY = (String) bsData.get("BRTHDY");
//					log.debug("BRTHDY={}", BRTHDY);
//					requestParam.put("BRTHDY", BRTHDY);
//
//					if (BRTHDY.length() >= 7) {
//						BIRTHY = BRTHDY.substring(0, 3);
//						BIRTHM = BRTHDY.substring(3, 5);
//						BIRTHD = BRTHDY.substring(5, 7);
//					}
//					log.debug("BIRTHY={}", BIRTHY);
//					log.debug("BIRTHM={}", BIRTHM);
//					log.debug("BIRTHD={}", BIRTHD);
//					requestParam.put("BIRTHY", BIRTHY);
//					requestParam.put("BIRTHM", BIRTHM);
//					requestParam.put("BIRTHD", BIRTHD);
//					requestParam.put("CPRIMBIRTHDAY", BIRTHY + BIRTHM + BIRTHD);
//					
//					int year = Integer.valueOf(BIRTHY);
//					String YY = String.valueOf(year + 1911);
//					String MM = "";
//					String DD = BIRTHD;
//					
//					//語系判斷
//					String locale = LocaleContextHolder.getLocale().toString();
//					if(locale.equals("en")) {
//						MM = Month(BIRTHM);
//						requestParam.put("CPRIMBIRTHDAYshow",YY + " " + MM + " " + DD);
//					}
//					else {
//						MM = BIRTHM;
//						requestParam.put("CPRIMBIRTHDAYshow",YY + "年 " + MM + "月 " + DD + "日");
//					}
//					log.trace("date>>>>" + YY + MM + DD);
//
//					POSTCOD1 = (String) bsData.get("POSTCOD1");
//					log.debug("POSTCOD1={}", POSTCOD1);
//
//					if (POSTCOD1.length() < 3) {
//						POSTCOD1 = "";
//					} else {
//						POSTCOD1 = POSTCOD1.substring(0, 3);
//					}
//					log.debug("POSTCOD1={}", POSTCOD1);
//					requestParam.put("POSTCOD1", POSTCOD1);
//
//					PMTADR = (String) bsData.get("PMTADR");
//					log.debug("PMTADR={}", PMTADR);
//					requestParam.put("PMTADR", PMTADR);
//
//					if (PMTADR.length() > 6) {
//						PMTADR1 = PMTADR.substring(6);
//					} else {
//						PMTADR1 = "";
//					}
//					log.debug("PMTADR1={}", PMTADR1);
//					requestParam.put("PMTADR1", PMTADR1);
//
//					POSTCOD2 = (String) bsData.get("POSTCOD2");
//					log.debug("POSTCOD2={}", POSTCOD2);
//
//					if (POSTCOD2.length() < 3) {
//						POSTCOD2 = "";
//					} else {
//						POSTCOD2 = POSTCOD2.substring(0, 3);
//					}
//					log.debug("POSTCOD2={}", POSTCOD2);
//					requestParam.put("POSTCOD2", POSTCOD2);
//
//					CTTADR = (String) bsData.get("CTTADR");
//					log.debug("CTTADR={}", CTTADR);
//
//					if (CTTADR == null) {
//						CTTADR = "";
//					}
//					log.debug("CTTADR={}", CTTADR);
//					requestParam.put("CTTADR", CTTADR);
//
//					if (CTTADR.length() > 6) {
//						CTTADR1 = CTTADR.substring(6);
//					} else {
//						CTTADR1 = "";
//					}
//					log.debug("CTTADR1={}", CTTADR1);
//					requestParam.put("CTTADR1", CTTADR1);
//
//					TEL11 = (String) bsData.get("ARACOD");
//					log.debug("TEL11={}", TEL11);
//					requestParam.put("TEL11", TEL11);
//
//					TEL12 = (String) bsData.get("TELNUM");
//					log.debug("TEL12={}", TEL12);
//
//					TEL13 = "";
//					if (TEL12.indexOf("#") > -1) {
//						TEL13 = TEL12.substring(TEL12.indexOf("#") + 1);
//						TEL12 = TEL12.substring(0, TEL12.indexOf("#"));
//					}
//					log.debug("TEL12={}", TEL12);
//					requestParam.put("TEL12", TEL12);
//					log.debug("TEL13={}", TEL13);
//					requestParam.put("TEL13", TEL13);
//
//					MOBTEL = (String) bsData.get("MOBTEL");
//					log.debug("MOBTEL={}", MOBTEL);
//					requestParam.put("MOBTEL", MOBTEL);
//				}
//				//若為舊戶，有臺企信用卡但沒有網銀資料，要繼續
//				else if(bs.getMsgCode().equals("E007") || bs.getMsgCode().equals("E002") || bs.getMsgCode().equals("D002")) {
//					log.debug("N960 get " + bs.getMsgCode());
//					requestParam.put("CPRIMBIRTHDAYshow",requestParam.get("CPRIMBIRTHDAYshow"));
//					BRTHDY = birthday.replaceAll("/","");
//					log.debug("BRTHDY={}", BRTHDY);
//					requestParam.put("BRTHDY", BRTHDY);
//
//					if (BRTHDY.length() >= 7) {
//						BIRTHY = BRTHDY.substring(0, 3);
//						BIRTHM = BRTHDY.substring(3, 5);
//						BIRTHD = BRTHDY.substring(5, 7);
//					}
//					log.debug("BIRTHY={}", BIRTHY);
//					log.debug("BIRTHM={}", BIRTHM);
//					log.debug("BIRTHD={}", BIRTHD);
//					requestParam.put("BIRTHY", BIRTHY);
//					requestParam.put("BIRTHM", BIRTHM);
//					requestParam.put("BIRTHD", BIRTHD);
//					bs.setResult(true);
//				}
			}
			// 為新戶
			else if ("N".equals(oldcardowner)) {
				requestParam.put("CPRIMBIRTHDAYshow",requestParam.get("CPRIMBIRTHDAYshow"));
				BRTHDY = birthday.replaceAll("/","");
				log.debug("BRTHDY={}", BRTHDY);
				requestParam.put("BRTHDY", BRTHDY);

				if (BRTHDY.length() >= 7) {
					BIRTHY = BRTHDY.substring(0, 3);
					BIRTHM = BRTHDY.substring(3, 5);
					BIRTHD = BRTHDY.substring(5, 7);
				}
				log.debug("BIRTHY={}", BIRTHY);
				log.debug("BIRTHM={}", BIRTHM);
				log.debug("BIRTHD={}", BIRTHD);
				requestParam.put("BIRTHY", BIRTHY);
				requestParam.put("BIRTHM", BIRTHM);
				requestParam.put("BIRTHD", BIRTHD);
				bs.setResult(true);
			}
			List<Map<String, String>> cityList = apply_creditLoan_service.getCityList();

			if (!cityList.isEmpty()) {
				bs.addData("cityList", cityList);
			}

			List<Map<String, String>> areaList;
			if (CTTADR.length() >= 3) {
				areaList = apply_creditLoan_service.getAreaList(CTTADR.substring(0, 3));
			} else {
				areaList = apply_creditLoan_service.getAreaList("");
			}
			if (!areaList.isEmpty()) {
				bs.addData("CTTADRList", areaList);
			}

			if (PMTADR.length() >= 3) {
				areaList = apply_creditLoan_service.getAreaList(PMTADR.substring(0, 3));
			} else {
				areaList = apply_creditLoan_service.getAreaList("");
			}
			if (!areaList.isEmpty()) {
				bs.addData("PMTADRList", areaList);
			}
			// 取得所有分行
			List<Map<String, String>> branchList = getAllBranchList();
			bs.addData("branchList", branchList);

			String cusidn = requestParam.get("CUSIDN");
			log.debug(ESAPIUtil.vaildLog("cusidn >> " + cusidn));
			int branchnum = branchList.size();
			log.debug("branchnum={}", branchnum);
			requestParam.put("branchnum", String.valueOf(branchnum));

			String bchbkname = "";
			String branchNo = "";

			for (int j = 0; j < branchnum; j++) {
				bchbkname += branchList.get(j).get("ADBRANCHNAME") + ",";// 取得所有分行名稱
				branchNo += branchList.get(j).get("ADBRANCHID") + ",";// 取得所有分行代號
			}
			log.debug(ESAPIUtil.vaildLog("bchbkname=" + bchbkname));
			log.debug(ESAPIUtil.vaildLog("branchNo=" + branchNo));
			requestParam.put("bchbkname", bchbkname);
			requestParam.put("branchNo", branchNo);

			bs.addData("requestParam", requestParam);
		} catch (Exception e) {
			//e.printStackTrace();
			//Avoid Information Exposure Through an Error Message
			log.error("prepareInputData error >> {}",e); 
		}
		return bs;
	}

	/**
	 * 取得所有分行資料
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, String>> getAllBranchList() {
		List<Map<String, String>> branchList = new ArrayList<Map<String, String>>();

		List<ADMBRANCH> list = admBranchDao.getAll();

		branchList = CodeUtil.objectCovert(branchList.getClass(), list);

		return branchList;
	}
	/**
	 * 準備申請信用卡確認頁的資料
	 */
	public BaseResult prepareConfirmData(Map<String,String> requestParam){
		BaseResult bs = null;
		try{
			bs = new BaseResult();
			for(Entry<String,String> entry : requestParam.entrySet()){
				String key = entry.getKey();
				log.debug(ESAPIUtil.vaildLog("key >> " + key));
				String value = entry.getValue();;
				log.debug(ESAPIUtil.vaildLog("value >> " + value));

				if(value ==  null){
					requestParam.put(key,"");
				}
				else if("WORKYEARS".equals(key) || "WORKMONTHS".equals(key) || "CPRIMJOBTITLE".equals(key) || "MPRIMADDR1COND".equals(key) || "MPRIMMARRIAGE".equals(key) || "MPRIMEDUCATION".equals(key) || "CHENAME".equals(key) || "CITY1".equals(key) || "ZONE1".equals(key) || "CITY2".equals(key) || "ZONE2".equals(key) || "CITY4".equals(key) || "ZONE4".equals(key)){
					if("#".equals(value)){
						requestParam.put(key,"");
					}
				}
			}
			
			String CARDMEMO = requestParam.get("CARDMEMO");
			log.debug(ESAPIUtil.vaildLog("CARDMEMO >> " + CARDMEMO));
			String VARSTR2 = requestParam.get("VARSTR2");
			log.debug(ESAPIUtil.vaildLog("VARSTR2 >> " + VARSTR2));

			String CARDDESC = "";
			String CARDDESCpdf = "";
			if(CARDMEMO.equals("1") && VARSTR2.equals("1")){
				//悠遊卡自動加值功能：同意預設開啟
				CARDDESCpdf = "（悠遊卡自動加值功能：同意預設開啟）";
				CARDDESC = "（" + i18n.getMsg("LB.X1821") + "）";
			}
				
			if(CARDMEMO.equals("1") && VARSTR2.equals("2")){
				//悠遊卡自動加值功能：不同意預設開啟
				CARDDESCpdf ="（悠遊卡自動加值功能：不同意預設開啟）";
				CARDDESC ="（" + i18n.getMsg("LB.X1822") + "）";
			}
				
			if(CARDMEMO.equals("2")){
				//一卡通自動加值功能：預設開啟
				CARDDESCpdf = "（一卡通自動加值功能：預設開啟）";
				CARDDESC = "（" + i18n.getMsg("LB.X1823") + "）";
			}
			log.debug("CARDDESC={}",CARDDESC);
			requestParam.put("CARDDESC",CARDDESC);
			requestParam.put("CARDDESCpdf",CARDDESCpdf);
			
			String FGTXWAY = requestParam.get("FGTXWAY");
			log.debug(ESAPIUtil.vaildLog("FGTXWAY >> " + FGTXWAY));

			
			String CHENAME = requestParam.get("CHENAME");
			log.debug(ESAPIUtil.vaildLog("CHENAME >> " + CHENAME));

			String CHENAMEChinese = "";
			String CHENAMEpdf = "";
			if("1".equals(CHENAME)){
				CHENAMEpdf = "是";
//				CHENAMEChinese = "LB.D0034_2";
				CHENAMEChinese = i18n.getMsg("LB.D0034_2");
			}
			else if("2".equals(CHENAME)){
				CHENAMEpdf = "否";
//				CHENAMEChinese = "LB.D0034_3";
				CHENAMEChinese = i18n.getMsg("LB.D0034_3");
			}
			log.debug("CHENAMEChinese={}",CHENAMEChinese);
			requestParam.put("CHENAMEChinese",CHENAMEChinese);
			requestParam.put("CHENAMEpdf",CHENAMEpdf);
			
			String MPRIMEDUCATION = requestParam.get("MPRIMEDUCATION");
			log.debug(ESAPIUtil.vaildLog("MPRIMEDUCATION >> " + MPRIMEDUCATION));

			String MPRIMEDUCATIONChinese = "";
			String MPRIMEDUCATIONpdf = "";
			if("1".equals(MPRIMEDUCATION)){
				//博士
				MPRIMEDUCATIONpdf = "博士";
				MPRIMEDUCATIONChinese = i18n.getMsg("LB.D0588");
			}
			else if("2".equals(MPRIMEDUCATION)){
				//碩士
				MPRIMEDUCATIONpdf = "碩士";
				MPRIMEDUCATIONChinese = i18n.getMsg("LB.D0589");
			}
			else if("3".equals(MPRIMEDUCATION)){
				//大學
				MPRIMEDUCATIONpdf = "大學";
				MPRIMEDUCATIONChinese = i18n.getMsg("LB.D0590");
			}
			else if("4".equals(MPRIMEDUCATION)){
				//專科
				MPRIMEDUCATIONpdf = "專科";
				MPRIMEDUCATIONChinese = i18n.getMsg("LB.D0591");
			}
			else if("5".equals(MPRIMEDUCATION)){
				//高中職
				MPRIMEDUCATIONpdf = "高中職";
				MPRIMEDUCATIONChinese = i18n.getMsg("LB.D0592");
			}
			else if("6".equals(MPRIMEDUCATION)){
				//其他
				MPRIMEDUCATIONpdf = "其他";
				MPRIMEDUCATIONChinese = i18n.getMsg("LB.D0572");
			}
			log.debug("MPRIMEDUCATIONChinese={}",MPRIMEDUCATIONChinese);
			requestParam.put("MPRIMEDUCATIONChinese",MPRIMEDUCATIONChinese);
			requestParam.put("MPRIMEDUCATIONpdf",MPRIMEDUCATIONpdf);
			
			String MPRIMMARRIAGE = requestParam.get("MPRIMMARRIAGE");
			log.debug(ESAPIUtil.vaildLog("MPRIMMARRIAGE >> " + MPRIMMARRIAGE));
			String MPRIMMARRIAGEChinese = "";
			String MPRIMMARRIAGEpdf = "";
			if("1".equals(MPRIMMARRIAGE)){
				//已婚
				MPRIMMARRIAGEpdf = "已婚";
				MPRIMMARRIAGEChinese = i18n.getMsg("LB.D0595");
			}
			else if("2".equals(MPRIMMARRIAGE)){
				//未婚
				MPRIMMARRIAGEpdf = "未婚";
				MPRIMMARRIAGEChinese = i18n.getMsg("LB.D0596");
			}
			else if("3".equals(MPRIMMARRIAGE)){
				//其他
				MPRIMMARRIAGEpdf = "其他";
				MPRIMMARRIAGEChinese = i18n.getMsg("LB.D0572");
			}
			log.debug(ESAPIUtil.vaildLog("MPRIMMARRIAGEChinese >> " + MPRIMMARRIAGEChinese));
			requestParam.put("MPRIMMARRIAGEChinese",MPRIMMARRIAGEChinese);
			requestParam.put("MPRIMMARRIAGEpdf",MPRIMMARRIAGEpdf);
			
			
			String MPRIMADDR1COND = requestParam.get("MPRIMADDR1COND");
			log.debug(ESAPIUtil.vaildLog("MPRIMADDR1COND >> " + MPRIMADDR1COND));
			String MPRIMADDR1CONDChinese = "";
			String MPRIMADDR1CONDpdf = "";
			if("1".equals(MPRIMADDR1COND)){
				//本人或配偶持有
				MPRIMADDR1CONDpdf = "本人或配偶持有";
				MPRIMADDR1CONDChinese = i18n.getMsg("LB.X0548");
			}
			else if("2".equals(MPRIMADDR1COND)){
				//直系親屬持有
				MPRIMADDR1CONDpdf = "直系親屬持有";
				MPRIMADDR1CONDChinese = i18n.getMsg("LB.X0549");
			}
			else if("3".equals(MPRIMADDR1COND)){
				//租賃或宿舍
				MPRIMADDR1CONDpdf = "租賃或宿舍";
				MPRIMADDR1CONDChinese = i18n.getMsg("LB.X0550");
			}
			log.debug("MPRIMADDR1CONDChinese={}",MPRIMADDR1CONDChinese);
			requestParam.put("MPRIMADDR1CONDChinese",MPRIMADDR1CONDChinese);
			requestParam.put("MPRIMADDR1CONDpdf",MPRIMADDR1CONDpdf);
			
			String MBILLTO = requestParam.get("MBILLTO");
			log.debug(ESAPIUtil.vaildLog("MBILLTO >> " + MBILLTO));
			String MBILLTOChinese = "";
			String MBILLTOpdf = "";
			if("1".equals(MBILLTO)){
				//同戶籍地址
				MBILLTOpdf = "同戶籍地址";
				MBILLTOChinese = i18n.getMsg("LB.D1125");
			}
			else if("2".equals(MBILLTO)){
				//同居住地址
				MBILLTOpdf = "同居住地址";
				MBILLTOChinese = i18n.getMsg("LB.X0552");
			}
			else if("3".equals(MBILLTO)){
				//同公司地址
				MBILLTOpdf = "同公司地址";
				MBILLTOChinese = i18n.getMsg("LB.X0553");
			}
			
			log.debug("MBILLTOChinese={}",MBILLTOChinese);
			requestParam.put("MBILLTOChinese",MBILLTOChinese);
			requestParam.put("MBILLTOpdf",MBILLTOpdf);
			
			String MCARDTO = requestParam.get("MCARDTO");
			log.debug(ESAPIUtil.vaildLog("MCARDTO >> " + MCARDTO));
			String MCARDTOChinese = "";
			String MCARDTOpdf = "";
			if("1".equals(MCARDTO)){
				//親領
				MCARDTOpdf = "親領";
				MCARDTOChinese = i18n.getMsg("LB.D0072");
			}
			else if("2".equals(MCARDTO)){
				//寄送
				MCARDTOpdf = "寄送";
				MCARDTOChinese = i18n.getMsg("LB.D0073");
			}
			
//			if("Y".equals(requestParam.get("oldcardowner"))) {
//				MCARDTOpdf = "掛號郵寄";
//			}
			
			log.debug("MCARDTOChinese={}",MCARDTOChinese);
			requestParam.put("MCARDTOChinese",MCARDTOChinese);
			requestParam.put("MCARDTOpdf",MCARDTOpdf);
			
			String CPRIMJOBTYPE = requestParam.get("CPRIMJOBTYPE");
			log.debug(ESAPIUtil.vaildLog("CPRIMJOBTITLE" + CPRIMJOBTYPE));

			
			String CPRIMJOBTYPEChinese = "";
			String CPRIMJOBTYPEpdf = "";
			//軍官、軍人
			if("061100".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "軍官、軍人";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.X2439");
			}
			//警官、員警
			else if("061200".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "警官、員警";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.X2440");
			}
			//其它公共行政業
			else if("061300".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "其它公共行政業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0873");
			}
			//教育業
			else if("061400".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "教育業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0874");
			}
			//學生
			else if("061410".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "學生";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0081");
			}
			//工、商及服務業
			else if("061500".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "工、商及服務業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0876");
			}
			//農林漁牧業
			else if("0615A0".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "農林漁牧業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0877");
			}
			//礦石及土石採取業
			else if("0615B0".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "礦石及土石採取業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0878");
			}
			//製造業
			else if("0615C0".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "製造業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0879");
			}
			//水電燃氣業
			else if("0615D0".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "水電燃氣業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0880");
			}
			//營造業
			else if("0615E0".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "營造業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0881");
			}
			//批發及零售業
			else if("0615F0".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "批發及零售業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0882");
			}
			//住宿及餐飲業
			else if("0615G0".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "住宿及餐飲業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0883");
			}
			//運輸、倉儲及通信業
			else if("0615H0".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "運輸、倉儲及通信業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0884");
			}
			//金融及保險業
			else if("0615I0".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "金融及保險業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0885");
			}
			//不動產及租賃業
			else if("0615J0".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "不動產及租賃業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0886");
			}
			//其他專業服務業 (建築、電腦資訊、設計、顧問、研發、醫護、社服等服務業)
			else if("061610".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "其他專業服務業 (建築、電腦資訊、設計、顧問、研發、醫護、社服等服務業)";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.X2441");
			}
			//技術服務業 (出版、廣告、影視、休閒、保全、環保、維修、宗教、團體、美髮、殯葬、停車場…等)
			else if("061620".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "技術服務業 (出版、廣告、影視、休閒、保全、環保、維修、宗教、團體、美髮、殯葬、停車場…等)";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.X2442");
			}
			//非法人組織授信戶負責人
			else if("069999".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "非法人組織授信戶負責人";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0899");
			}
			//特定專業服務業(律師、會計師、地政士、公證人、記帳士、記帳及報稅代理人)
			else if("061630".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "特定專業服務業(律師、會計師、地政士、公證人、記帳士、記帳及報稅代理人)";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0889");
			}
			//特定專業服務業(受聘於專業服務業之行政事務職員)
			else if("061640".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "特定專業服務業(受聘於專業服務業之行政事務職員)";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0890");
			}
			//銀樓業 (包含珠寶、鐘錶及貴金屬之製造、批發及零售)
			else if("061500".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "銀樓業 (包含珠寶、鐘錶及貴金屬之製造、批發及零售)";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0891");
			}
			//虛擬貨幣交易服務業
			else if("061660".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "虛擬貨幣交易服務業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0892");
			}
			//博弈業
			else if("061670".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "博弈業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0893");
			}
			//國防武器或戰爭設備相關行業(軍火)
			else if("061680".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "國防武器或戰爭設備相關行業(軍火)";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0894");
			}
			//家管
			else if("061690".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "家管";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0082");
			}
			//自由業
			else if("061691".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "自由業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0083");
			}
			//無業
			else if("061692".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "無業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.X0571");
			}
			//其他
			else if("061700".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "其他";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0572");
			}
			log.debug("CPRIMJOBTYPEChinese={}",CPRIMJOBTYPEChinese);
			requestParam.put("CPRIMJOBTYPEChinese",CPRIMJOBTYPEChinese);
			requestParam.put("CPRIMJOBTYPEpdf",CPRIMJOBTYPEpdf);
			
			
			String CPRIMJOBTITLE = requestParam.get("CPRIMJOBTITLE");
			log.debug(ESAPIUtil.vaildLog("CPRIMJOBTITLE >> " + CPRIMJOBTITLE));
			String CPRIMJOBTITLEChinese = "";
			String CPRIMJOBTITLEpdf = "";
			//董事長/負責人
			if("01".equals(CPRIMJOBTITLE)){CPRIMJOBTITLEChinese = i18n.getMsg("LB.X2443");CPRIMJOBTITLEpdf = "董事長/負責人";}
			//總經理
			if("02".equals(CPRIMJOBTITLE)){CPRIMJOBTITLEChinese = i18n.getMsg("LB.X2444");CPRIMJOBTITLEpdf = "總經理";}
			//主管
			if("03".equals(CPRIMJOBTITLE)){CPRIMJOBTITLEChinese = i18n.getMsg("LB.X0561");CPRIMJOBTITLEpdf = "主管";}
			//專業人員
			if("04".equals(CPRIMJOBTITLE)){CPRIMJOBTITLEChinese = i18n.getMsg("LB.X0588");CPRIMJOBTITLEpdf = "專業人員";}
			//職員
			if("05".equals(CPRIMJOBTITLE)){CPRIMJOBTITLEChinese = i18n.getMsg("LB.X2445");CPRIMJOBTITLEpdf = "職員";}
			//業務/服務人員
			if("06".equals(CPRIMJOBTITLE)){CPRIMJOBTITLEChinese = i18n.getMsg("LB.X2446");CPRIMJOBTITLEpdf = "業務/服務人員";}
			//其他
			if("07".equals(CPRIMJOBTITLE)){CPRIMJOBTITLEChinese = i18n.getMsg("LB.D0572");CPRIMJOBTITLEpdf = "其他";}
			
			log.debug("CPRIMJOBTITLEChinese={}",CPRIMJOBTITLEChinese);
			requestParam.put("CPRIMJOBTITLEChinese",CPRIMJOBTITLEChinese);
			requestParam.put("CPRIMJOBTITLEpdf",CPRIMJOBTITLEpdf);
			
			String countrycode = requestParam.get("CTRYDESC1");
			String countryname = "";
			ADMCOUNTRY ADMCOUNTRY = admcountrydao.getCountry(countrycode);
			countryname = ADMCOUNTRY.getADCTRYNAME();
			if(countrycode.equals("TW")) {
				countryname = "中華民國";
			}
			log.debug(ESAPIUtil.vaildLog("countryname={}"+countryname));
			requestParam.put("COUNTRYNAME",countryname);
			
			String BRANCHNAME = requestParam.get("BRANCHNAME");
			log.debug(ESAPIUtil.vaildLog("BRANCHNAME={}"+BRANCHNAME));
			
			String BRHNAME = "";
			String BRHNAMEEN = "";
			String BRHNAMECH = "";
			String bankAddr = "";
			String bankAddrEN = "";
			String bankAddrCH = "";
			String bankTel = "";
			if(!"".equals(BRANCHNAME)){
				//轉換成分行名稱
				List<ADMBRANCH> admBranchList = admBranchDao.findByBhID(BRANCHNAME);
				log.debug(ESAPIUtil.vaildLog("admBranchList="+admBranchList));
				
				BRHNAME = admBranchList.get(0).getADBRANCHNAME();
				BRHNAMEEN = admBranchList.get(0).getADBRANENGNAME();
				BRHNAMECH = admBranchList.get(0).getADBRANCHSNAME();
				bankAddr = admBranchList.get(0).getADDRESS();
				bankAddrEN = admBranchList.get(0).getADDRESSENG();
				bankAddrCH = admBranchList.get(0).getADDRESSCHS();
				bankTel = admBranchList.get(0).getTELNUM();
			}
			log.debug(ESAPIUtil.vaildLog("BRHNAME="+BRHNAME));
			log.debug(ESAPIUtil.vaildLog("BRHNAMEEN="+BRHNAMEEN));
			log.debug(ESAPIUtil.vaildLog("BRHNAMECH="+BRHNAMECH));
			requestParam.put("BRHNAME",BRHNAME);
			requestParam.put("BRHNAMEEN",BRHNAMEEN);
			requestParam.put("BRHNAMECH",BRHNAMECH);
			requestParam.put("bankAddr",bankAddr);
			requestParam.put("bankAddrEN",bankAddrEN);
			requestParam.put("bankAddrCH",bankAddrCH);
			requestParam.put("bankTel",bankTel);
			
			//身分證補換發資訊
			String chantype = requestParam.get("CHANTYPE");
			String CHANTYPEpdf = "";
			String CHANTYPEChinese = "";
			if(chantype.equals("1")) {
				CHANTYPEpdf = "初領";
				CHANTYPEChinese = i18n.getMsg("LB.D1113");
			}
			else if(chantype.equals("2")) {
				CHANTYPEpdf = "補發";
				CHANTYPEChinese = i18n.getMsg("LB.W1665");
			}
			else if(chantype.equals("3")) {
				CHANTYPEpdf = "換發";
				CHANTYPEChinese = i18n.getMsg("LB.W1664");
			}
			requestParam.put("CHANTYPEChinese",CHANTYPEChinese);
			
			String CHDATEY = "";
			String CHDATEM = "";
			String CHDATED = "";
			String CHANCITY = "";
			if(StrUtils.isNotEmpty(requestParam.get("CHDATEY")) && !requestParam.get("CHDATEY").equals("#")) {
				CHDATEY = requestParam.get("CHDATEY");
			}
			if(StrUtils.isNotEmpty(requestParam.get("CHDATEM")) && !requestParam.get("CHDATEM").equals("#")) {
				CHDATEM = requestParam.get("CHDATEM");
			}
			if(StrUtils.isNotEmpty(requestParam.get("CHDATED")) && !requestParam.get("CHDATED").equals("#")) {
				CHDATED = requestParam.get("CHDATED");
			}
			if(StrUtils.isNotEmpty(requestParam.get("CHANCITY")) && !requestParam.get("CHANCITY").equals("#")) {
				CHANCITY = requestParam.get("CHANCITY");
			}
			String chandata = i18n.getMsg("LB.D0583") + CHDATEY + i18n.getMsg("LB.Year") + CHDATEM + i18n.getMsg("LB.Month") + CHDATED + i18n.getMsg("LB.D0586") + " " +
					CHANCITY + " " + CHANTYPEChinese;
			requestParam.put("CHDATA",chandata);
			
			bs.addAllData(requestParam);
//			bs.addData("requestParam",requestParam);
			bs.setResult(true);
		}
		catch(Exception e){
			//e.printStackTrace();
			//Avoid Information Exposure Through an Error Message
			log.error("prepareConfirmData error >> {}",e); 
		}
		return bs;
	}
	/**
	 * 準備前往申請信用卡結果頁P6的資料
	 */
	@SuppressWarnings("unchecked")
	public Boolean prepareCreditCardApplyP6Data(Map<String,String> reqParam, String uploadPath, Model model){
		try{
			String CPRIMCHNAMEFull = CodeUtil.convertFullorHalf(reqParam.get("CPRIMCHNAME"),1);
			log.debug(ESAPIUtil.vaildLog("CPRIMCHNAMEFull >> " + CPRIMCHNAMEFull));
			reqParam.put("CPRIMCHNAMEFull",CPRIMCHNAMEFull);
					
			String CPRIMADDR2Full = CodeUtil.convertFullorHalf(reqParam.get("CPRIMADDR2"),1);
			log.debug(ESAPIUtil.vaildLog("CPRIMADDR2Full >> " + CPRIMADDR2Full));
			reqParam.put("CPRIMADDR2Full",CPRIMADDR2Full);
			
			String CPRIMADDRFull = CodeUtil.convertFullorHalf(reqParam.get("CPRIMADDR"),1);
			log.debug(ESAPIUtil.vaildLog("CPRIMADDRFull >> " + CPRIMADDRFull));
			reqParam.put("CPRIMADDRFull",CPRIMADDRFull);
			
			String CPRIMCOMPANYFull = CodeUtil.convertFullorHalf(reqParam.get("CPRIMCOMPANY"),1);
			log.debug(ESAPIUtil.vaildLog("CPRIMCOMPANYFull >> " + CPRIMCOMPANYFull));
			reqParam.put("CPRIMCOMPANYFull",CPRIMCOMPANYFull);
			
			String CPRIMADDR3Full = CodeUtil.convertFullorHalf(reqParam.get("CPRIMADDR3"),1);
			log.debug(ESAPIUtil.vaildLog("CPRIMADDR3Full >> " + CPRIMADDR3Full));
			reqParam.put("CPRIMADDR3Full",CPRIMADDR3Full);
			
			String CTRYDESCFull = CodeUtil.convertFullorHalf(reqParam.get("COUNTRYNAME"),1);
			log.trace(ESAPIUtil.vaildLog("CTRYDESCFull >> " + CTRYDESCFull));
			reqParam.put("CTRYDESCFull",CTRYDESCFull);
			
			String CFU2 = reqParam.get("CFU2");
			log.debug(ESAPIUtil.vaildLog("CFU2 >> " + CFU2));
			
			String QRCODE = reqParam.get("QRCODE").isEmpty() ? "" : reqParam.get("QRCODE");
			log.debug(ESAPIUtil.vaildLog("QRCODE >> " + QRCODE));
			
			String partition = reqParam.get("partition").isEmpty() ? "" : reqParam.get("partition");
			log.debug(ESAPIUtil.vaildLog("partition >> " + partition));
			
			String branch = reqParam.get("branch").isEmpty() ? "" : reqParam.get("branch");
			log.debug(ESAPIUtil.vaildLog("branch >> " + branch));
			
			String memberId = reqParam.get("memberId").isEmpty() ? "" : reqParam.get("memberId");
			log.debug(ESAPIUtil.vaildLog("memberId >> " + memberId));
			
			String BRNO = "";
			if(CFU2.equals("1")){
				String CPRIMID = reqParam.get("CPRIMID");
				log.debug(ESAPIUtil.vaildLog("CPRIMID >> " + CPRIMID));

				
				List<TXNUSER> listtxnUser = txnUserDao.findByUserId(CPRIMID);
				
				if(!listtxnUser.isEmpty()){
					TXNUSER txnuser = listtxnUser.get(0);
					BRNO = txnuser.getADBRANCHID();
					if(BRNO != null){
						BRNO = BRNO.trim();
					}
					else{
						BRNO = "";
					}
				}
			}
			log.debug(ESAPIUtil.vaildLog("BRNO={}"+BRNO));
			reqParam.put("BRNO",BRNO);
			
			//原住民姓名轉全型
			String CUSNAME = reqParam.get("CUSNAME").isEmpty() ? "" : CodeUtil.convertFullorHalf(reqParam.get("CUSNAME"), 1);
			reqParam.put("CUSNAME", CUSNAME);
			//羅馬拼音姓名轉全型
			String ROMANAME = reqParam.get("ROMANAME").isEmpty() ? "" : CodeUtil.convertFullorHalf(reqParam.get("ROMANAME"), 1);
			reqParam.put("ROMANAME", ROMANAME);
	
			TXNCARDAPPLY attrib = new TXNCARDAPPLY();
			try {
				attrib = CodeUtil.objectCovert(attrib.getClass(),reqParam);
				
				attrib.setCARDNAME2("");
				attrib.setCARDNAME3("");
				attrib.setCPRIMCHNAME(CPRIMCHNAMEFull);//中文姓名，必填
				attrib.setCPRIMADDR2(CPRIMADDR2Full);
				attrib.setCPRIMADDR(CPRIMADDRFull);
				attrib.setCPRIMCOMPANY(CPRIMCOMPANYFull);
				attrib.setCPRIMADDR3(CPRIMADDR3Full);
				attrib.setCTRYDESC(CTRYDESCFull);
				if(reqParam.get("CARDNAME").equals("85") || reqParam.get("CARDNAME").equals("86")) {
					attrib.setZONE(partition);
					attrib.setTEAM(branch);
					attrib.setMID(memberId);
				}
				
				//出生日期
				String CPRIMBIRTHDAY = reqParam.get("CPRIMBIRTHDAY");
				String FGTXWAY = reqParam.get("FGTXWAY");
				log.debug(ESAPIUtil.vaildLog("FGTXWAY={}"+FGTXWAY));
				
				//不是晶片金融卡
				if(!"2".equals(FGTXWAY)){
					CPRIMBIRTHDAY = CPRIMBIRTHDAY.replaceAll("/","");
					if(CPRIMBIRTHDAY.length() >= 7){
						CPRIMBIRTHDAY = CPRIMBIRTHDAY.substring(0,3) + "/" + CPRIMBIRTHDAY.substring(3,5) + "/" + CPRIMBIRTHDAY.substring(5,7);
					}
				}
		        log.debug(ESAPIUtil.vaildLog("CPRIMBIRTHDAY>>" + CPRIMBIRTHDAY));
				reqParam.put("CPRIMBIRTHDAY",CPRIMBIRTHDAY);
				attrib.setCPRIMBIRTHDAY(CPRIMBIRTHDAY);//出生年月日，必填
				
				attrib.setCARDNAME1(reqParam.get("CARDNAME"));//卡片名稱，必填
				
				attrib.setCUSNAME(reqParam.get("CUSNAME"));//原住民姓名
				attrib.setROMANAME(reqParam.get("ROMANAME"));//羅馬拼音姓名
				attrib.setPAYMONEY(reqParam.get("PAYMONEY"));//往來產品之預期金額
			
				txnCardApplyDao.save(attrib);
			}
			catch(Exception e) {
				//e.printStackTrace();
				log.error("DAO SAVE ERROR",e);
				return false;
			}
//			Locale currentLocale = LocaleContextHolder.getLocale();
//			log.trace("GET LOCALE>>>{}",currentLocale);
//			LocaleContextHolder.setLocale(Locale.TAIWAN);
//			currentLocale = LocaleContextHolder.getLocale();
//			log.trace("GET LOCALE>>>{}",currentLocale);
			
			getCreditCardApplyPDFParameter(reqParam);
			log.debug(ESAPIUtil.vaildLog("reqParam={}"+reqParam));
			
			String isSessionID = SpringBeanFactory.getBean("isSessionID");
			log.debug("isSessionID={}",isSessionID);
			
			FTPUtil ftputil = new FTPUtil();
			Base64 base64 = new Base64();
			
			
			//最後上傳TXT檔的<CNT>欄位的值
			int txtCount = 0;
			
			PDFUtil pdfUtil = new PDFUtil();
			InputStream inputStream = pdfUtil.HTMLtoPDF(pdfUtil.getPDFHTML(reqParam));
			downloadFile(inputStream, reqParam.get("FILE6"));
		
			//PDF上傳
			//TODO 20191029 eloan FTP 暫時不連，上線要記得開啟
			try {
				if(inputStream != null && !reqParam.get("FILE6").isEmpty()){
					//LOCAL
					String filename = Paths.get(reqParam.get("FILE6")).getFileName().toString();
					String validFileName = ESAPIUtil.GetValidPathPart(uploadPath,filename);
					byte[] bytes = Files.readAllBytes(Paths.get(validFileName));
					if("N".equals(isSessionID)){
						FTPClient ftpclient = ftputil.connect("192.168.50.191",21,"ftpuser3","25082201",30000);
						boolean result = ftputil.uploadFile(ftpclient,reqParam.get("FILE6"),bytes);
						
						log.debug("result={}",result);
						ftputil.finish(ftpclient);
						
						if(result){
							txtCount++;
						}
					}
					else{
						Map<String,String> ftp = SpringBeanFactory.getBean("eloanFTP");
						String use = ftp.get("use");
						String ip = ftp.get("ip");
						int port = Integer.parseInt(ftp.get("port"));
						String name = ftp.get("name");
						String pw = ftp.get("password");
						String path = ftp.get("path");
						String decodeString = new String(base64.decode(pw.getBytes()));
						int timeout = Integer.parseInt(ftp.get("timeout"));
						log.debug("Get ip>>>" + ip + " pa>>" + path);
						if("Y".equals(use)){
				        	FTPClient ftpclient = ftputil.connect(ip,port,name,decodeString,timeout);
							ftputil.ChangeToDirectory(ftpclient,path);
							
							ftpclient.setBufferSize(1024);
							//重要	
							ftpclient.enterLocalPassiveMode();
//							boolean result = ftputil.uploadFile(ftpclient,reqParam.get("FILE6"),IOUtils.toByteArray(inputStream));
							boolean result = ftputil.uploadFile(ftpclient,reqParam.get("FILE6"),bytes);
							
							log.debug("result={}" + result);
							ftputil.finish(ftpclient);
							
							if(result){
								txtCount++;
							}
						}
					}
				}
				//身分證正面圖片
				if(reqParam.get("FILE1") != null && !reqParam.get("FILE1").isEmpty()){
					//修改 Absolute Path Traversal
					
					
				
					String filename = Paths.get(reqParam.get("FILE1")).getFileName().toString();
					String validFileName = ESAPIUtil.GetValidPathPart(uploadPath,filename);
					byte[] bytes = Files.readAllBytes(Paths.get(validFileName));
					//LOCAL
					if("N".equals(isSessionID)){
						FTPClient ftpclient = ftputil.connect("192.168.50.191",21,"ftpuser","ftpuser",30000);
						boolean result = ftputil.uploadFile(ftpclient,reqParam.get("FILE1name"),bytes);
						ftputil.finish(ftpclient);
						
						if(result){
							txtCount++;
						}
					}
					else{
						Map<String, String> ftp = SpringBeanFactory.getBean("eloanFTP");
						String use = ftp.get("use");
						String ip = ftp.get("ip");
						int port = Integer.parseInt(ftp.get("port"));
						String name = ftp.get("name");
						String pw = ftp.get("password");
						String path = ftp.get("path");
						String decodeString = new String(base64.decode(pw.getBytes()));
						int timeout = Integer.parseInt(ftp.get("timeout"));
						if("Y".equals(use)){
				        	FTPClient ftpclient = ftputil.connect(ip,port,name,decodeString,timeout);
							ftputil.ChangeToDirectory(ftpclient,path);
	//						String FILE1 = attrib.getFILE1();
							String FILE1 = reqParam.get("FILE1name");
							log.debug("FILE1={}",ESAPIUtil.vaildLog(FILE1));
							
							ftpclient.setBufferSize(1024);
							//重要	
							ftpclient.enterLocalPassiveMode();
							boolean result = ftputil.uploadFile(ftpclient,FILE1,bytes);
							log.debug(ESAPIUtil.vaildLog("「"+FILE1+"」上傳"+ result));
							ftputil.finish(ftpclient);
			
							if(result){
								txtCount++;
							}
				        }
					}
				}
				//身分證反面圖片
				if(reqParam.get("FILE2") != null && !reqParam.get("FILE2").isEmpty()){
					//byte[] bytes = Files.readAllBytes(new File(uploadPath+reqParam.get("FILE2")).toPath());
					//fix APT		
					String filename = Paths.get(reqParam.get("FILE2")).getFileName().toString();
					String validFileName = ESAPIUtil.GetValidPathPart(uploadPath,filename);
					byte[] bytes = Files.readAllBytes(Paths.get(validFileName));
					
					
					
					//LOCAL
					if("N".equals(isSessionID)){
						FTPClient ftpclient = ftputil.connect("192.168.50.191",21,"ftpuser","ftpuser",30000);
						boolean result = ftputil.uploadFile(ftpclient,reqParam.get("FILE2name"),bytes);
						ftputil.finish(ftpclient);
						
						if(result){
							txtCount++;
						}
					}
					else{
						Map<String, String> ftp = SpringBeanFactory.getBean("eloanFTP");
						String use = ftp.get("use");
						String ip = ftp.get("ip");
						int port = Integer.parseInt(ftp.get("port"));
						String name = ftp.get("name");
						String pw = ftp.get("password");
						String path = ftp.get("path");
						String decodeString = new String(base64.decode(pw.getBytes()));
						int timeout = Integer.parseInt(ftp.get("timeout"));
						if("Y".equals(use)){
				        	FTPClient ftpclient = ftputil.connect(ip,port,name,decodeString,timeout);
							ftputil.ChangeToDirectory(ftpclient,path);
	//						String FILE2 = attrib.getFILE2();
							String FILE2 = reqParam.get("FILE2name");
							log.debug("FILE2={}",ESAPIUtil.vaildLog(FILE2));
							
							ftpclient.setBufferSize(1024);
							//重要	
							ftpclient.enterLocalPassiveMode();
							boolean result = ftputil.uploadFile(ftpclient,FILE2,bytes);
							log.debug(ESAPIUtil.vaildLog("「"+FILE2+"」上傳"+ result));
							ftputil.finish(ftpclient);
							
							if(result){
								txtCount++;
							}
				        }
					}
				}
				//財力證明圖片1
				if(reqParam.get("FILE3") != null && !reqParam.get("FILE3").isEmpty()){
					//byte[] bytes = Files.readAllBytes(new File(uploadPath+reqParam.get("FILE3")).toPath());
					//fix APT
				
					String filename = Paths.get(reqParam.get("FILE3")).getFileName().toString();
					String validFileName = ESAPIUtil.GetValidPathPart(uploadPath,filename);
					byte[] bytes = Files.readAllBytes(Paths.get(validFileName));
					//LOCAL
					if("N".equals(isSessionID)){
						FTPClient ftpclient = ftputil.connect("192.168.50.191",21,"ftpuser","ftpuser",30000);
						boolean result = ftputil.uploadFile(ftpclient,reqParam.get("FILE3name"),bytes);
						ftputil.finish(ftpclient);
						
						if(result){
							txtCount++;
						}
					}
					else{
						Map<String, String> ftp = SpringBeanFactory.getBean("eloanFTP");
						String use = ftp.get("use");
						String ip = ftp.get("ip");
						int port = Integer.parseInt(ftp.get("port"));
						String name = ftp.get("name");
						String pw = ftp.get("password");
						String path = ftp.get("path");
						String decodeString = new String(base64.decode(pw.getBytes()));
						int timeout = Integer.parseInt(ftp.get("timeout"));
						if("Y".equals(use)){
				        	FTPClient ftpclient = ftputil.connect(ip,port,name,decodeString,timeout);
							ftputil.ChangeToDirectory(ftpclient,path);
	//						String FILE3 = attrib.getFILE3();
							String FILE3 = reqParam.get("FILE3name");
							log.debug("FILE3={}",ESAPIUtil.vaildLog(FILE3));
							
							ftpclient.setBufferSize(1024);
							//重要	
							ftpclient.enterLocalPassiveMode();
							boolean result = ftputil.uploadFile(ftpclient,FILE3,bytes);
							log.debug("「"+FILE3+"」上傳"+ result);
							ftputil.finish(ftpclient);
							
							if(result){
								txtCount++;
							}
				        }
					}
				}
				//財力證明圖片2
				if(reqParam.get("FILE4") != null && !reqParam.get("FILE4").isEmpty()){
					//byte[] bytes = Files.readAllBytes(new File(uploadPath+reqParam.get("FILE4")).toPath());
					//fix APT
					String filename = Paths.get(reqParam.get("FILE4")).getFileName().toString();
					String validFileName = ESAPIUtil.GetValidPathPart(uploadPath,filename);
					byte[] bytes = Files.readAllBytes(Paths.get(validFileName));
					//LOCAL
					if("N".equals(isSessionID)){
						FTPClient ftpclient = ftputil.connect("192.168.50.191",21,"ftpuser","ftpuser",30000);
						boolean result = ftputil.uploadFile(ftpclient,reqParam.get("FILE4name"),bytes);
						ftputil.finish(ftpclient);
						
						if(result){
							txtCount++;
						}
					}
					else{
						Map<String, String> ftp = SpringBeanFactory.getBean("eloanFTP");
						String use = ftp.get("use");
						String ip = ftp.get("ip");
						int port = Integer.parseInt(ftp.get("port"));
						String name = ftp.get("name");
						String pw = ftp.get("password");
						String path = ftp.get("path");
						String decodeString = new String(base64.decode(pw.getBytes()));
						int timeout = Integer.parseInt(ftp.get("timeout"));
						if("Y".equals(use)){
				        	FTPClient ftpclient = ftputil.connect(ip,port,name,decodeString,timeout);
							ftputil.ChangeToDirectory(ftpclient,path);
	//						String FILE4 = attrib.getFILE4();
							String FILE4 = reqParam.get("FILE4name");
							log.debug("FILE4={}",FILE4);
							
							ftpclient.setBufferSize(1024);
							//重要	
							ftpclient.enterLocalPassiveMode();
							boolean result = ftputil.uploadFile(ftpclient,FILE4,bytes);
							log.debug("「"+FILE4+"」上傳"+ result);
							ftputil.finish(ftpclient);
							
							if(result){
								txtCount++;
							}
				        }
					}
				}
				//財力證明圖片3
				if(reqParam.get("FILE5") != null && !reqParam.get("FILE5").isEmpty()){
					//byte[] bytes = Files.readAllBytes(new File(uploadPath+reqParam.get("FILE5")).toPath());
					// fix APT
					String filename = Paths.get(reqParam.get("FILE5")).getFileName().toString();
					String validFileName = ESAPIUtil.GetValidPathPart(uploadPath,filename);
					byte[] bytes = Files.readAllBytes(Paths.get(validFileName));
					//LOCAL
					if("N".equals(isSessionID)){
						FTPClient ftpclient = ftputil.connect("192.168.50.191",21,"ftpuser","ftpuser",30000);
						boolean result = ftputil.uploadFile(ftpclient,reqParam.get("FILE5name"),bytes);
						ftputil.finish(ftpclient);
						
						if(result){
							txtCount++;
						}
					}
					else{
						Map<String, String> ftp = SpringBeanFactory.getBean("eloanFTP");
						String use = ftp.get("use");
						String ip = ftp.get("ip");
						int port = Integer.parseInt(ftp.get("port"));
						String name = ftp.get("name");
						String pw = ftp.get("password");
						String path = ftp.get("path");
						String decodeString = new String(base64.decode(pw.getBytes()));
						int timeout = Integer.parseInt(ftp.get("timeout"));
						if("Y".equals(use)){
				        	FTPClient ftpclient = ftputil.connect(ip,port,name,decodeString,timeout);
							ftputil.ChangeToDirectory(ftpclient,path);
	//						String FILE5 = attrib.getFILE5();
							String FILE5 = reqParam.get("FILE5name");
							log.debug("FILE5={}",FILE5);
							
							ftpclient.setBufferSize(1024);
							//重要	
							ftpclient.enterLocalPassiveMode();
							boolean result = ftputil.uploadFile(ftpclient,FILE5,bytes);
							log.debug("「"+FILE5+"」上傳"+ result);
							ftputil.finish(ftpclient);
							
							if(result){
								txtCount++;
							}
				        }
					}
				}
			}
			catch(Exception e) {
				log.error("",e);
			}
			log.debug("txtCount={}",txtCount);
			
			Map<String,String> newParamMap = new HashMap<String,String>();
			
			newParamMap = CodeUtil.objectCovert(newParamMap.getClass(),attrib);
			log.debug("newParamMap={}",newParamMap);
			
			//將所有newparamMap裡面的null值換成空值
			Set<String> set = newParamMap.keySet();
			Iterator<String> iterator = set.iterator();
			while(iterator.hasNext()){
				String key = iterator.next();
				log.debug("key={}",key);
				String value = newParamMap.get(key);
				log.debug("value={}",value);
				if(value == null){
					newParamMap.put(key,"");
				}
			}
			//將國名轉成國家代碼
//			String ctrycode = compareCTRY(attrib.getCTRYDESC());
			String ctrycode = reqParam.get("CTRYDESC1");
			log.trace(ESAPIUtil.vaildLog("ctrycode>>>>>>{}"+ctrycode));
			
			newParamMap.put("CTRYDESC",ctrycode);
			newParamMap.put("CNT",String.valueOf(txtCount));
			newParamMap.put("WEBSITE","1");
			//職位名稱要傳中文
			newParamMap.put("CPRIMJOBTITLE", reqParam.get("CPRIMJOBTITLEpdf"));
			//身分證補換發資料
			String dateY = (StrUtils.isNotEmpty(reqParam.get("CHDATEY")) && !reqParam.get("CHDATEY").equals("#")) ? reqParam.get("CHDATEY") : "";
			String dateM = (StrUtils.isNotEmpty(reqParam.get("CHDATEM")) && !reqParam.get("CHDATEM").equals("#")) ? reqParam.get("CHDATEM") : "";
			String dateD = (StrUtils.isNotEmpty(reqParam.get("CHDATED")) && !reqParam.get("CHDATED").equals("#")) ? reqParam.get("CHDATED") : "";
			
			newParamMap.put("CHANDATE", dateY + dateM + dateD);
			newParamMap.put("CHANCITY", StrUtils.isNotEmpty(reqParam.get("CHANCITY")) ? reqParam.get("CHANCITY") : "");
			newParamMap.put("CHANTYPE", StrUtils.isNotEmpty(reqParam.get("CHANTYPE")) ? reqParam.get("CHANTYPE") : "");
			
			String templatePath = "/uploadTemplate/ApplyCreditCard.txt";
			log.debug("templatePath={}",templatePath);
			
			Resource resource = new ClassPathResource(templatePath);
//			File file = resource.getFile();
			
			//信用卡申請TXT
			//修改 Incorrect Permission Assignment For Critical Resources
			if(resource.exists()){
				inputStream = new BufferedInputStream(resource.getInputStream());
			
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
				StringBuilder stringBuilder = new StringBuilder();
				String line;
				line = bufferedReader.readLine();
				while(line != null){
					log.debug("line={}",line);
					ESAPIUtil.validInput(line,"GeneralString",true);
					
					String tagName = line.replace("<","").replace(">","");
					log.debug("tagName={}",tagName);
					String value = newParamMap.get(tagName);
					log.debug("value={}",value);
					
					if(value != null && !"".equals(value)){
						line = line + value;
					}
					
					stringBuilder.append(line).append("\n");
					line = bufferedReader.readLine();
				}
				log.debug("stringBuilder.toString()={}",stringBuilder.toString());
				
				try {
					log.debug("檔案落地>" + newParamMap.get("RCVNO") + ".TXT");
					String separ = File.separator;
					String skinPath = context.getRealPath("") + separ + "com" + separ + "updateTmp" + separ + newParamMap.get("RCVNO") + ".TXT";
					byte[] bytes = (stringBuilder.toString()).getBytes("UTF-8");
					
					// 修正Incorrect Permission Assignment For Critical Resources
					BufferedOutputStream downloadFile = new BufferedOutputStream(new FileOutputStream(skinPath));
//					FileOutputStream downloadFile = new FileOutputStream(skinPath);
					downloadFile.write(bytes);
					downloadFile.close();
					try {
						String movePath = SpringBeanFactory.getBean("imgMovePath") + separ + newParamMap.get("RCVNO") + ".TXT";
						// 修正Incorrect Permission Assignment For Critical Resources
						BufferedOutputStream imgmove = new BufferedOutputStream(new FileOutputStream(movePath));
//						FileOutputStream imgmove = new FileOutputStream(movePath);
						imgmove.write(bytes);
						imgmove.close();
					}
					catch (Exception e) {
						log.error("imgmove error>>>"+e);
					}
				}
				catch (Exception e) {
					log.debug("檔案落地失敗>>>"+e);
				}
			
				//LOCAL
				if("N".equals(isSessionID)){
					FTPClient ftpclient = ftputil.connect("192.168.50.191",21,"ftpuser","ftpuser",30000);
					boolean result = ftputil.uploadFile(ftpclient,newParamMap.get("RCVNO") + ".TXT",stringBuilder.toString().getBytes("UTF-8"));
					
					log.debug("result={}",result);
					ftputil.finish(ftpclient);
				}
				else{
					Map<String,String> ftp = SpringBeanFactory.getBean("eloanFTP");
					String use = ftp.get("use");
					String ip = ftp.get("ip");
					int port = Integer.parseInt(ftp.get("port"));
					String name = ftp.get("name");
					String pw = ftp.get("password");
					String path = ftp.get("path");
					String decodeString = new String(base64.decode(pw.getBytes()));
					int timeout = Integer.parseInt(ftp.get("timeout"));
					if("Y".equals(use)){
			        	FTPClient ftpclient = ftputil.connect(ip,port,name,decodeString,timeout);
						ftputil.ChangeToDirectory(ftpclient,path);
						
						boolean result = ftputil.uploadFile(ftpclient,newParamMap.get("RCVNO") + ".TXT",stringBuilder.toString().getBytes("BIG5"));
						log.debug("result={}" + result);
						ftputil.finish(ftpclient);
					}
				}
				try{
					bufferedReader.close();
				}
				catch(Exception e){
					log.error("",e);
				}
			}
			//處理N930，N931，TXNUSER，寄電子郵件
			NA03_REST(reqParam);
			
			model.addAttribute("requestParam",reqParam);
		}
		catch(Exception e){
			//e.printStackTrace();
			//Avoid Information Exposure Through an Error Message
			log.error("prepareCreditCardApplyP6Data error >> {}",e); 
		}
		return true;
	}
	/*
	 * 線上申請信用卡取得PDFHTML的變數
	 */
	public void getCreditCardApplyPDFParameter(Map<String,String> reqParam){
		String CFU2DESC = reqParam.get("CFU2").equals("2") ? i18n.getMsg("LB.D0107") : i18n.getMsg("LB.X1640");
		log.debug("CFU2DESC={}",CFU2DESC);
		reqParam.put("CFU2DESC",CFU2DESC);
		
		String CN = reqParam.get("CN");
		log.debug(ESAPIUtil.vaildLog("CN = "+CN));
		String CARDDESC = reqParam.get("CARDDESC");
		if(StrUtils.isEmpty(CARDDESC)) {
			CARDDESC = "";
		}
		log.debug(ESAPIUtil.vaildLog("CARDDESC = "+CARDDESC));
		
		String CNDESC = CARDDESC.equals("") ? CN : CN + "<br/>" + CARDDESC;
		log.debug(ESAPIUtil.vaildLog("CNDESC = "+CNDESC));
		reqParam.put("CNDESC",CNDESC);
		
		String VARSTR3 = reqParam.get("VARSTR3");
		log.debug(ESAPIUtil.vaildLog("VARSTR3 = "+VARSTR3));
		
		if("11".equals(VARSTR3)){
			reqParam.put("view_VARSTR3","■");
		}
		else{
			reqParam.put("view_VARSTR3","□");
		}
		
//		String CPRIMJOBTYPE = reqParam.get("CPRIMJOBTYPE");
//		log.debug(ESAPIUtil.vaildLog("CPRIMJOBTYPE = "+CPRIMJOBTYPE));
//		if("1".equals(CPRIMJOBTYPE)){
//			reqParam.put("view_CPRIMJOBTYPE1","■");
//			reqParam.put("view_CPRIMJOBTYPE2","□");
//			reqParam.put("view_CPRIMJOBTYPE3","□");
//			reqParam.put("view_CPRIMJOBTYPE4","□");
//		}
//		else if("2".equals(CPRIMJOBTYPE)){
//			reqParam.put("view_CPRIMJOBTYPE1","□");
//			reqParam.put("view_CPRIMJOBTYPE2","■");
//			reqParam.put("view_CPRIMJOBTYPE3","□");
//			reqParam.put("view_CPRIMJOBTYPE4","□");
//		}
//		else if("3".equals(CPRIMJOBTYPE)){
//			reqParam.put("view_CPRIMJOBTYPE1","□");
//			reqParam.put("view_CPRIMJOBTYPE2","□");
//			reqParam.put("view_CPRIMJOBTYPE3","■");
//			reqParam.put("view_CPRIMJOBTYPE4","□");
//		}
//		else if("4".equals(CPRIMJOBTYPE)){
//			reqParam.put("view_CPRIMJOBTYPE1","□");
//			reqParam.put("view_CPRIMJOBTYPE2","□");
//			reqParam.put("view_CPRIMJOBTYPE3","□");
//			reqParam.put("view_CPRIMJOBTYPE4","■");
//		}
//		else{
//			reqParam.put("view_CPRIMJOBTYPE1","□");
//			reqParam.put("view_CPRIMJOBTYPE2","□");
//			reqParam.put("view_CPRIMJOBTYPE3","□");
//			reqParam.put("view_CPRIMJOBTYPE4","□");
//		}
		String MCASH = reqParam.get("MCASH");
		log.debug(ESAPIUtil.vaildLog("MCASH = "+MCASH));
		if("1".equals(MCASH)){
			reqParam.put("view_MCASH1","■");
			reqParam.put("view_MCASH2","□");
		}
		else{
			reqParam.put("view_MCASH1","□");
			reqParam.put("view_MCASH2","■");
		}
		String CNOTE1 = reqParam.get("CNOTE1");
		log.debug(ESAPIUtil.vaildLog("CNOTE1 = "+CNOTE1));
		if("1".equals(CNOTE1)){
			reqParam.put("view_CNOTE11","■");
			reqParam.put("view_CNOTE12","□");
		}
		else{
			reqParam.put("view_CNOTE11","□");
			reqParam.put("view_CNOTE12","■");
		}
		String CNOTE3 = reqParam.get("CNOTE3");
		log.debug(ESAPIUtil.vaildLog("CNOTE3 = "+CNOTE3));
		if("1".equals(CNOTE3)){
			reqParam.put("view_CNOTE31","■");
			reqParam.put("view_CNOTE32","□");
		}
		else{
			reqParam.put("view_CNOTE31","□");
			reqParam.put("view_CNOTE32","■");
		}
		String CNOTE4 = reqParam.get("CNOTE4");
		log.debug(ESAPIUtil.vaildLog("CNOTE4 = "+CNOTE4));
		
		if("2".equals(CNOTE4)){
			reqParam.put("view_CNOTE4","■");
		}
		else{
			reqParam.put("view_CNOTE4","□");
		}
	}
	//傳給eloan時國家名稱轉成國碼
	public String compareCTRY(String ctryName){
		String afterCge = "";		
			
		String [][] ctrylist = {{"阿富汗","AF"},{"阿爾巴尼亞","AL"},{"波士尼亞與赫塞哥維納","BA"},{"蒲隆地","BI"},{"汶萊","BN"},{"玻利維亞","BO"},{"巴哈馬","BS"},{"波扎那","BW"},{"白俄羅斯","BY"},{"貝里斯","BZ"},
		   {"剛果民主共和國","CD"},{"中非共和國","CF"},{"中國大陸","CN"},{"哥倫比亞","CO"},{"哥斯大黎加","CR"},{"古巴","CU"},{"多明尼加","DO"},{"厄瓜多爾","EC"},{"埃及","EG"},{"厄利垂亞","ER"},
		   {"衣索比亞","ET"},{"瓜地馬拉","GT"},{"幾內亞比索","GW"},{"宏都拉斯","HN"},{"海地","HT"},{"印度","IN"},{"伊拉克","IQ"},{"伊朗","IR"},{"牙買加","JM"},{"北韓","KP"},
		   {"寮國","LA"},{"黎巴嫩","LB"},{"利比亞","LY"},{"蒙特尼哥羅","ME"},{"馬紹爾群島","MH"},{"馬其頓","MK"},{"緬甸","MM"},{"墨西哥","MX"},{"尼加拉瓜","NI"},{"諾魯","NR"},
		   {"紐威島","NU"},{"巴拿馬","PA"},{"秘魯","PE"},{"巴基斯坦","PK"},{"塞爾維亞","RS"},{"俄羅斯","RU"},{"蘇丹","SD"},{"索馬利亞","SO"},{"南蘇丹","SS"},{"薩爾瓦多","SV"},
		   {"敘利亞","SY"},{"突尼西亞","TN"},{"烏克蘭","UA"},{"烏干達","UG"},{"委內瑞拉","VE"},{"萬那杜","VU"},{"葉門","YE"},{"辛巴威","ZW"},{"安道爾","AD"},{"阿拉伯聯合大公國","AE"},
		   {"安地卡及巴布達","AG"},{"英屬安圭拉","AI"},{"亞美尼亞","AM"},{"安哥拉","AO"},{"南極洲","AQ"},{"阿根廷","AR"},{"美屬薩摩亞","AS"},{"奧地利","AT"},{"澳大利亞","AU"},{"阿魯巴","AW"},
		   {"奧蘭群島","AX"},{"亞塞拜然","AZ"},{"巴貝多","BB"},{"孟加拉","BD"},{"比利時","BE"},{"上伏塔(布吉納法索)","BF"},{"保加利亞","BG"},{"巴林","BH"},{"貝南(達荷美)","BJ"},{"聖巴瑟米","BL"},
		   {"百慕達","BM"},{"波奈、聖佑達修斯及沙巴","BQ"},{"巴西","BR"},{"不丹","BT"},{"波維特島","BV"},{"加拿大","CA"},{"可可斯群島","CC"},{"剛果","CG"},{"瑞士","CH"},{"象牙海岸","CI"},
		   {"科克群島","CK"},{"智利","CL"},{"喀麥隆","CM"},{"佛德角","CV"},{"古拉索","CW"},{"聖誕島","CX"},{"塞普路斯","CY"},{"捷克","CZ"},{"德國","DE"},{"吉布地","DJ"},
		   {"丹麥","DK"},{"多米尼克","DM"},{"阿爾及利亞","DZ"},{"愛沙尼亞","EE"},{"西撒哈拉","EH"},{"西班牙","ES"},{"芬蘭","FI"},{"斐濟","FJ"},{"福克蘭群島","FK"},{"密克羅尼西亞","FM"},
		   {"法羅群島","FO"},{"法國","FR"},{"加彭","GA"},{"英國","GB"},{"格瑞那達","GD"},{"喬治亞","GE"},{"法屬圭亞那","GF"},{"格恩西島","GG"},{"迦納","GH"},{"直布羅陀","GI"},
		   {"格陵蘭","GL"},{"甘比亞","GM"},{"幾內亞","GN"},{"瓜德魯普島","GP"},{"赤道幾內亞","GQ"},{"希臘","GR"},{"南喬治亞及南三明治群島","GS"},{"關島","GU"},{"圭亞那","GY"},{"香港","HK"},
		   {"赫德及麥當勞群島","HM"},{"克羅埃西亞","HR"},{"匈牙利","HU"},{"印尼","ID"},{"愛爾蘭","IE"},{"以色列","IL"},{"曼島","IM"},{"英屬印度洋地區","IO"},{"冰島","IS"},{"義大利","IT"},
		   {"澤西島","JE"},{"約旦","JO"},{"日本","JP"},{"肯亞","KE"},{"吉爾吉斯","KG"},{"高棉","KH"},{"吉里巴斯","KI"},{"葛摩","KM"},{"聖克里斯多福","KN"},{"大韓民國","KR"},
		   {"科威特","KW"},{"開曼群島","KY"},{"哈薩克","KZ"},{"聖露西亞","LC"},{"賴比瑞亞","LR"},{"列支敦斯堡","LI"},{"斯里蘭卡","LK"},{"賴索托","LS"},{"立陶宛","LT"},{"盧森堡","LU"},
		   {"拉脫維亞","LV"},{"摩洛哥","MA"},{"摩納哥","MC"},{"摩爾多瓦","MD"},{"聖馬丁(法屬)","MF"},{"馬達加斯加","MG"},{"馬利","ML"},{"蒙古","MN"},{"澳門","MO"},{"北馬里亞納群島","MP"},
		   {"法屬馬丁尼克","MQ"},{"茅利塔尼亞","MR"},{"蒙瑟拉特島","MS"},{"馬爾他","MT"},{"模里西斯","MU"},{"馬爾地夫","MV"},{"馬拉威","MW"},{"馬來西亞","MY"},{"莫三鼻給","MZ"},{"納米比亞","NA"},
		   {"新喀里多尼亞","NC"},{"尼日","NE"},{"諾福克群島","NF"},{"奈及利亞","NG"},{"荷蘭","NL"},{"挪威","NO"},{"尼伯爾","NP"},{"紐西蘭","NZ"},{"阿曼","OM"},{"法屬玻里尼西亞","PF"},
		   {"巴布亞新幾內亞","PG"},{"菲律賓","PH"},{"波蘭","PL"},{"聖匹及密啟倫群島","PM"},{"皮特康島","PN"},{"波多黎各","PR"},{"葡萄牙","PT"},{"帛琉","PW"},{"巴拉圭","PY"},{"巴拿馬運河區","PZ"},
		   {"庫達(卡達)","QA"},{"留尼旺","RE"},{"羅馬尼亞","RO"},{"盧安達","RW"},{"沙烏地阿拉伯","SA"},{"索羅門群島","SB"},{"塞席爾","SC"},{"瑞典","SE"},{"新加坡","SG"},{"聖赫勒拿島","SH"},
		   {"斯洛凡尼亞","SI"},{"斯瓦巴及尖棉島","SJ"},{"斯洛伐克","SK"},{"獅子山","SL"},{"聖馬利諾","SM"},{"塞內加爾","SN"},{"蘇利南","SR"},{"聖托馬-普林斯浦","ST"},{"聖馬丁(荷屬)","SX"},{"史瓦濟蘭","SZ"},
		   {"土克斯及開科斯群島","TC"},{"查德","TD"},{"法屬南部屬地","TF"},{"多哥","TG"},{"泰國","TH"},{"塔吉克","TJ"},{"托克勞群島","TK"},{"東帝汶","TL"},{"土庫曼","TM"},{"東加","TO"},
		   {"土耳其","TR"},{"千里達-托貝哥","TT"},{"吐瓦魯","TV"},{"中華民國","TW"},{"坦尚尼亞","TZ"},{"美屬邊疆群島","UM"},{"美國","US"},{"烏拉圭","UY"},{"烏玆別克","UZ"},{"教廷","VA"},
		   {"聖文森","VC"},{"英屬維爾京群島","VG"},{"美屬維爾京群島","VI"},{"越南","VN"},{"沃里斯及伏塔那島","WF"},{"薩摩亞群島","WS"},{"中華民國(OBU)","XA"},{"美亞特","YT"},{"南非共和國","ZA"},{"尚比亞","ZM"}
		};
		
		for(int i=0;i<ctrylist.length;i++){
			if(ctrylist[i][0].equals(ctryName)){
				afterCge = ctrylist[i][1];				
			}
		}			
		log.debug("afterCge={}",afterCge);
		
		return afterCge;
	}
	
	public String Month(String month) {
		String MM = "";
		switch (month) {
		case "01":
			MM = "January";
			break;
		case "02":
			MM = "February";
			break;
		case "03":
			MM = "March";
			break;
		case "04":
			MM = "April";
			break;
		case "05":
			MM = "May";
			break;
		case "06":
			MM = "June";
			break;
		case "07":
			MM = "July";
			break;
		case "08":
			MM = "August";
			break;
		case "09":
			MM = "September";
			break;
		case "10":
			MM = "October";
			break;
		case "11":
			MM = "November";
			break;
		case "12":
			MM = "December";
			break;

		default:
			MM = "";
			break;
		}
		return MM;
	}
	
	public void downloadFile(InputStream inputStream, String filename) {
		try {
			log.debug("檔案落地>"+filename);
			String separ = File.separator;
			String skinPath = context.getRealPath("") + separ + "com" + separ + "updateTmp" + separ + filename;
			int index;
			byte[] bytes = new byte[1024];
			// 修正Incorrect Permission Assignment For Critical Resources
			BufferedOutputStream fw = new BufferedOutputStream(new FileOutputStream(skinPath));
//			FileOutputStream downloadFile = new FileOutputStream(skinPath);
			while ((index = inputStream.read(bytes)) != -1) {
				fw.write(bytes, 0, index);
		        fw.flush();
//				downloadFile.write(bytes, 0, index);
//				downloadFile.flush();
			}
			fw.close();
//			downloadFile.close();
			inputStream.close();
			moveFile(filename);
		}
		catch (Exception e) {
			log.debug("檔案落地失敗>>>"+e);
		}
	}
	
	public void moveFile(String filename) {
		String validatePath = "";
		String validateMovePath = "";
		String separ = File.separator;
		String rootPath = context.getRealPath("") + separ + "com" + separ + "updateTmp" + separ;
		String movePath = SpringBeanFactory.getBean("imgMovePath") + separ;
		String moveFileName = filename;
		try {
			validatePath = ESAPIUtil.GetValidPathPart(rootPath,filename);
			//log.trace("movePath >> {}, moveFileName >> {}",movePath, moveFileName);
			if(!movePath.equals("") && !moveFileName.equals("")) {
				validateMovePath = ESAPIUtil.vaildPathTraversal2(movePath,moveFileName);
			}
			log.trace(ESAPIUtil.vaildLog("validateMovePath >> {}" + validateMovePath));
			
			try {
				//修改 Incorrect Permission Assignment For Critical Resources
				if(!validateMovePath.equals("")) {
//					Files.copy(Paths.get(validatePath), Paths.get(validateMovePath));
					// 修正Incorrect Permission Assignment For Critical Resources
					// 創建母目錄如果目錄不存在
					File file = new File(movePath);
					file.setWritable(true,true);
					file.setReadable(true,true);
					file.setExecutable(true,true);
					file.mkdirs();
					
					// 讀取source檔案
					byte[] srcFile = Files.readAllBytes(Paths.get(validatePath));
					
					// 產生目標檔案
					BufferedOutputStream fw = new BufferedOutputStream(new FileOutputStream(validateMovePath));
			        fw.write(srcFile);
			        fw.flush();
			        fw.close();
					log.trace(ESAPIUtil.vaildLog("複製檔案成功:"+validatePath));
				}
			}
			catch (Exception e) {
				log.error("imgmove error>>>"+e);
			}
		}
		catch (Exception e) {
			log.debug("複製檔案失敗>>>"+e);
		}
	}
	
	/**
	 * 取得獅友會分區資料
	 */
	public List<Map<String,String>> getLionList(){
		List<Map<String,String>> lionList = admLionCardDataDao.getLionData();
		
		log.debug(ESAPIUtil.vaildLog("lionList=" + lionList));
		
		return lionList;
	}
	
	/**
	 * 取得獅友會分會
	 * @param zone
	 * @return
	 */
	public List<Map<String, String>> getLionBranch(String zone) {
		//log.trace("zone>>{} ", zone);
		
		List<Map<String,String>> lionList = admLionCardDataDao.getLionBranch(zone);
		log.debug(ESAPIUtil.vaildLog("lionList=" + lionList));
		
		return lionList;

	}
	
	/**
	 * 信用卡補上傳資料_step1
	 * @param cusidn
	 * @return 
	 */
	public BaseResult upload_creditcard_identity_p1(Map<String,String> reqParam)	{		
		log.trace("upload_creditcard_identity_p1 start");
		//打到MS的服務去打ELOAN的API取得身分證所屬電話與是否需補件狀態
		BaseResult bs = new BaseResult();
		try {
			/*
			 * 0無須補件 1缺身分證 2缺財力證明 3兩者都缺 4超過當日補件次數 5需補件，但缺少電話
			 */
			log.debug("ELOAN_REST start");
			bs = ELOAN_REST(reqParam);
			log.debug("ELOAN_REST end");
			
			if(bs != null && bs.getResult()) {
					Map<String,String> ELOAN_RESTMap =(Map<String, String>) bs.getData();
					Map<String, String> okMap = ESAPIUtil.validStrMap(ELOAN_RESTMap);
					log.trace(ESAPIUtil.vaildLog("upload_creditcard_identity_p1.reqParam: "+ okMap));
					
					String json = okMap.get("data").toString();
					Gson gson = new Gson();
					Map map = gson.fromJson(json, Map.class);
					log.debug("data==>"+map.get("data"));
					List list = (List)map.get("data");
					for(int i = 0; i<list.size(); i++) {
						LinkedTreeMap jsonMap = (LinkedTreeMap)list.get(i);
						
						if(!"".equals(jsonMap.get("YNFLG")) && jsonMap.get("YNFLG") != null) {
							bs.addData("YNFLG", (String)jsonMap.get("YNFLG"));
							if("E".equals((String)jsonMap.get("YNFLG"))) {
								//ENRD
								log.debug("YNFLG==>"+jsonMap.get("YNFLG"));
								bs.setMessage("ENRD", i18n.getMsg("LB.Check_no_data"));//查無身分證件/財力證明需補件紀錄
							}else {
								bs.addData("PFLG", (String)jsonMap.get("PFLG"));
								switch(String.valueOf(jsonMap.get("PFLG"))) {
									case "4":
										//4超過當日補件次數
										bs.setMessage("FE0015", i18n.getMsg("LB.onlineapply_creditcard_FE0015"));//超過當日允許補件次數
										break;
									case "5":
										//5需補件，但缺少電話
										bs.setMessage("FE0014", i18n.getMsg("LB.onlineapply_creditcard_FE0014"));//查無行動電話資訊
										break;
									default:
										// 新世代網路銀行修改，為了讓呼叫的系統能判別回應結果是否正確，加入 MSGCOD
										//需要補件(Y)(PFLG：1缺身分證 2缺財力證明 3兩者都缺)
										
										bs.addData("MOBILE", (String)jsonMap.get("MOBILE"));		//行動電話
										bs.addData("YNFLG", (String)jsonMap.get("YNFLG"));			//查有案件，無須區分是否需要補件
										bs.addData("CUSTID", (String)jsonMap.get("CUSTID"));		//取得身分證字號，otp需要
										
										log.debug("PFLG==>"+(String)jsonMap.get("PFLG"));
										log.debug("MOBILE==>"+(String)jsonMap.get("MOBILE"));
										log.debug("RCVDATE==>"+(String)jsonMap.get("RCVDATE"));
										log.debug("RCVNO==>"+(String)jsonMap.get("RCVNO"));
										log.debug("DOCSTATUS==>"+(String)jsonMap.get("DOCSTATUS"));
										log.debug("CENCARD==>"+(String)jsonMap.get("CENCARD"));
										log.debug("YNFLG==>"+(String)jsonMap.get("YNFLG"));
										log.debug("CUSTID==>"+(String)jsonMap.get("CUSTID"));
										
										bs.addData("MSGCOD", "0000");
										break;
								}
							}
						}
					}
				
				bs.addAllData(okMap);
				bs.setResult(true);
				log.trace("service_bs >>{}", bs.getData());
			}
			
			

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			log.error("upload_creditcard_identity_p1 error >> {}",e);
		}		
		log.trace("upload_creditcard_identity_p1 end");
		return bs;
	}	
	
	

	
	
	 /**
	 * 取得OTP
	 * @param cusidn
	 * @param option
	 * @param type
	 * @param otp
	 * @param adopid
	 * @return 
	 */	
	public BaseResult SmsOTPService(String cusidn ,String option ,String type,String otp,String adopid)
	{
		log.trace("SmsOTPService service start");
		BaseResult bs = new BaseResult();
		Map <String,String> reqParam = null;
		try
		{
//			要先驗證簡訊
			reqParam = new HashMap<String,String>();
			reqParam.put("CUSIDN", cusidn);
			reqParam.put("FuncType", "1");
			reqParam.put("OTP", otp);
			reqParam.put("SMSADOPID", adopid);
//			TODO 本機開發可先註解 設定為true
//			bs.setResult(Boolean.TRUE);
			
			bs=SmsOtpWithphone_REST(reqParam);
			if(bs.getResult()) {
				log.info("簡訊驗證通過...");				
			}else {
				log.error("簡訊驗證失敗...");
			}
			
		}
		catch (Exception e)
		{
			log.error("SmsOTPService Error >>{}", e);
		}
		log.trace("SmsOTPService service end");
		return bs;
	}	
	
	
	

	
	
	
	     
	public Boolean FileZipOutputStream(@RequestParam Map<String, String> reqParam, String pflg, String cusidn, String rcvno, String uploadPath, String eln_uri) {
  		log.debug("FileZipOutputStream start");
		log.trace(ESAPIUtil.vaildLog("reqParam>>" + CodeUtil.toJson(reqParam)));
		String separ = File.separator;
		
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		String fileName = rcvno.substring(1)+"_"+DateUtil.getTWDate("")+DateUtil.getTheTime("")+".zip";
		String zipFile = okMap.get("imgMovePath")+separ+fileName;
		okMap.put("zipFile", rcvno.substring(1)+".zip");
		okMap.put("rcvno", rcvno);

		String isSessionID = SpringBeanFactory.getBean("isSessionID");
		log.debug("isSessionID={}",isSessionID);
		
		String srcFiles[] = new String[5];
		String srcFilestmp[] = new String[5];
		
		Boolean result = false;
		
		int firstNum = 0;
		int lastNum = 0;
		switch(pflg) {
			case "1":// 1缺身分證
				firstNum = 1;
				lastNum = 2;
				break;
			case "2":// 2缺財力證明
				firstNum = 3;
				lastNum = 5;
				break;
			case "3":// 3兩者都缺
				firstNum = 1;
				lastNum = 5;
				break;
			default:
				break;
		}		

		int k=0;
		for(int j = firstNum ; j <= lastNum ; j++) {
			if(!"".equals(okMap.get("FILE"+j))) {
				srcFiles[k] = okMap.get("imgMovePath")+separ+okMap.get("FILE"+j);
				srcFilestmp[k] = okMap.get("FILE"+j);
				k++;
			}
		}
		if(k!=0) {
	        try {
	            // create byte buffer
	            byte[] buffer = new byte[1024];
	            FileOutputStream fos = new FileOutputStream(zipFile);
	            ZipOutputStream zos = new ZipOutputStream(fos);
	            for (int i=0; i < k; i++) {
	                File srcFile = new File(srcFiles[i]);
	                FileInputStream fis = new FileInputStream(srcFile);
	                // begin writing a new ZIP entry, positions the stream to the start of the entry data
	                zos.putNextEntry(new ZipEntry(srcFile.getName()));
	                int length;
	                while ((length = fis.read(buffer)) > 0) {
	                    zos.write(buffer, 0, length);
	                }
	                zos.closeEntry();
	                // close the InputStream
	                fis.close();
	            }
	            // close the ZipOutputStream
	            zos.close();
	            
	        }
	        catch (Exception e) {
	        	log.debug("檔案壓縮失敗>>>"+e);
	        }
	        
	        log.debug("檔案壓縮成功");
          //成功壓縮成zip，刪除原有單檔
          for (int m=0; m < k; m++) {
	            removefile(okMap.get("imgMovePath")+separ, srcFilestmp[m], "", "");
          }
          for (int n=0; n < k; n++) {
	            removefile(okMap.get("path"), srcFilestmp[n], "", "");
        }
          
        log.debug("uploadPath==>"+okMap.get("imgMovePath")+separ); 
        log.debug("fileName==>"+fileName);
        
    	try {
    		//將壓縮檔上傳至eln
    		result = UploadtoELN(rcvno,okMap.get("imgMovePath")+separ,fileName,eln_uri,reqParam);
		} catch (Throwable e) {
			
		}
	}
  log.debug("FileZipOutputStream end");
  return result;
  }

  
  
  
  
  
  
  
  
  
  
  public Boolean UploadtoELN(String rcvno, String path, String fName, String eln_uri, @RequestParam Map<String, String> reqParam) throws Throwable {
	  log.debug("UploadtoELN start");
	  log.debug("path==>"+path);
	  log.debug("fName==>"+fName);
	  log.debug("eln_uri==>"+eln_uri);
	  String boundary = "*****";
	  String crlf = "\r\n";
	  String twoHyphens = "--";
	  String transCode = "UTF-8";
	  int bufferSize = 25000;
      URL url = new URL(eln_uri+"?method=fileUpload&uploader=CCRT");
      File file = new File(path + fName); //local file
      HttpsURLConnection conn = null;
      String output = "";
      Boolean status = false;
      try {

    	  
  		// 建立SSLContext物件，並使用我們指定的信任管理器初始化
			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				public void checkClientTrusted(X509Certificate[] certs,	String authType) {
				}

				public void checkServerTrusted(X509Certificate[] certs,	String authType) {
				}

				public void checkClientTrusted(java.security.cert.X509Certificate[] arg0, String arg1) throws CertificateException {
					// TODO 自動產生方法 Stub
					
				}

				public void checkServerTrusted(java.security.cert.X509Certificate[] arg0, String arg1) throws CertificateException {
					// TODO 自動產生方法 Stub
					
				}
			} };        	
      	
      	
      	
		  SSLContext sslContext = SSLContext.getInstance("SSL");
		  sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
		  // 從上述SSLContext物件中得到SSLSocketFactory物件
		  SSLSocketFactory ssf = sslContext.getSocketFactory();
			
          conn = (HttpsURLConnection) url.openConnection();
          conn.setConnectTimeout(30000);
          conn.setReadTimeout(600000);
          conn.setRequestMethod("POST");
          conn.setSSLSocketFactory(ssf);
          conn.setDoOutput(true);
          conn.setDoInput(true);
          conn.setRequestProperty("Accept", "application/json");
          conn.setRequestProperty("Accept-Charset", transCode);
          conn.setRequestProperty("Charsert", transCode);
          conn.setRequestProperty("Content-Type",
                  "multipart/form-data;boundary=" + boundary);
          OutputStream request = new DataOutputStream(conn.getOutputStream());
          byte[] end_data = ("\r\n--" + boundary + "--\r\n").getBytes();//
          StringBuffer sb = new StringBuffer();
          DataInputStream in = null;
          byte[] data = null;
          byte[] bufferOut = null;
          sb.setLength(0);
          sb.append(twoHyphens).append(boundary).append(crlf);
          sb.append("Content-Disposition: form-data;name=\"file\";filename=\""
                  + file.getName() + "\\" + crlf);
          sb.append("Content-Type:application/octet-stream").append(crlf)
                  .append(crlf);
          data = sb.toString().getBytes();
          request.write(data);
          in = new DataInputStream(new FileInputStream(file));
          int bytes = 0;
          bufferOut = new byte[1024];
          while ((bytes = in.read(bufferOut)) != -1) {
              request.write(bufferOut, 0, bytes);
          }
          in.close();
          request.write(end_data);
          request.flush();
          request.close();
          int connCode = conn.getResponseCode();
          log.debug("connCode==>"+connCode);
          if (connCode != HttpURLConnection.HTTP_OK) {
              if (connCode == 404) {
                  log.debug("Cannot open url : " + url);
              }
//              throw new RuntimeException("Failed : HTTP error code : "
//                      + connCode);
          }else {
        	  log.debug("連線成功，開始上傳");
        	  BaseResult bs = new BaseResult();
        	  Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
        	  okMap.put("RCVNO", rcvno);
        	  okMap.put("FNAME", fName.replace(".zip", ""));
        	  log.debug("ELOAN_REST start");
        	  bs = ELOAN_REST(okMap);
        	  log.debug("ELOAN_REST end");
        	  status = true;
          }            
          BufferedReader br = new BufferedReader(new InputStreamReader(
              (conn.getInputStream()), transCode), bufferSize);

          String line = null;
          while ((line = br.readLine()) != null) {
              log.debug("line==>"+line);
          }
          
      } finally {
          conn.disconnect();
          conn = null;
      }
      return status;
  }  
  
  
  
  
  
  
  
  
  
  
  

  
	/**
	 * 刪除圖片
	 * @param localPath
	 * @return
	 */
	public boolean removefile(String rootPath,String fileName, String movePath, String moveFileName) {
		String validatePath = "";
		String validateMovePath = "";
		try
		{
			//修改 Absolute Path Traversal
			validatePath = ESAPIUtil.GetValidPathPart(rootPath,fileName);
			//log.trace("movePath >> "+movePath+", moveFileName >> "+moveFileName);
			if(!movePath.equals("") && !moveFileName.equals("")) {
				validateMovePath = ESAPIUtil.vaildPathTraversal2(movePath,moveFileName);
			}
			//log.trace("validateMovePath >> {}",validateMovePath);
			
			try {
				//修改 Incorrect Permission Assignment For Critical Resources
				if(!validateMovePath.equals("")) {
					// CheckMax版本提升後又被掃出Incorrect Permission Assignment For Critical Resources
//					Files.copy(Paths.get(validatePath), Paths.get(validateMovePath));
					
					// 修正Incorrect Permission Assignment For Critical Resources
					// 創建母目錄如果目錄不存在
					File file = new File(movePath);
					file.setWritable(true,true);
					file.setReadable(true,true);
					file.setExecutable(true,true);
					file.mkdirs();
					
					// 讀取source檔案
					byte[] srcFile = Files.readAllBytes(Paths.get(validatePath));
					
					// 產生目標檔案
					BufferedOutputStream fw = new BufferedOutputStream(new FileOutputStream(validateMovePath));
			        fw.write(srcFile);
			        fw.flush();
			        fw.close();
					
					log.trace(ESAPIUtil.vaildLog("複製圖檔成功:"+validatePath));
				}
				Files.delete(Paths.get(validatePath));
				log.warn(ESAPIUtil.vaildLog("刪除圖檔成功:"+validatePath));
				return(true);
			}
			catch(Exception RemoveFileEx)
			{
				log.error(RemoveFileEx.getMessage());
				log.warn("刪除圖檔失敗"+validatePath);
				return(false);
			}		
		}
		catch(Exception RemoveFileEx)
		{
			log.error(RemoveFileEx.getMessage());
			log.warn("刪除圖檔失敗"+validatePath);
			return(false);
		}		
	}
	
	/**
	 * 準備申請信用卡卡片選擇頁的資料
	 */
	@SuppressWarnings("unchecked")
	public BaseResult cardSelectData(Map<String, String> requestParam, BaseResult bs) {
		log.info("Function cardSelectData");
		try {

			String QRCODE = requestParam.get("QRCODE");
			log.debug(ESAPIUtil.vaildLog("QRCODE >> " + QRCODE));

			String BRANCH = requestParam.get("BRANCH");
			log.debug(ESAPIUtil.vaildLog("BRANCH >> " + BRANCH));

			bs.addData("requestParam", requestParam);

			// 找一般卡卡別
			List<ADMCARDDATA> list = admCardDataDao.getNormalCard();

			List<Map<String, String>> normalCardList = new ArrayList<Map<String, String>>();
			normalCardList = CodeUtil.objectCovert(normalCardList.getClass(), list);
			log.debug(ESAPIUtil.vaildLog("normalCardList=" + normalCardList));
			bs.addData("normalCardList", normalCardList);

			// 獅友卡分會資料
			// if("L".equals(QRCODE)) {
			List<Map<String, String>> lionList = getLionList();
			if (!lionList.isEmpty()) {
				bs.addData("lionList", lionList);
			}
			// }

			// 如果有特殊清況 ( QRCODE , BRANCH ) , 就用 "normalSilverCardList"
			// 如果是普通情況進入 , 就用"normalCardList" , 也不判斷客戶有沒有申請過

			// 從QRCODE連結進入，只能顯示銀色之愛信用卡
			if ("Y".equals(QRCODE)) {
				// 假的
				// if(false){
				list = admCardDataDao.getNormalSilverCard("68", "69");

				List<Map<String, String>> normalSilverCardList = new ArrayList<Map<String, String>>();
				normalSilverCardList = CodeUtil.objectCovert(normalSilverCardList.getClass(), list);
				log.debug(ESAPIUtil.vaildLog("QRCODE=Y , normalSilverCardList=" + normalSilverCardList));
				bs.addData("normalSilverCardList", CodeUtil.toJson(normalSilverCardList));
//				return bs;
			}
			// 獅友會
			else if ("L".equals(QRCODE)) {
				list = admCardDataDao.getNormalSilverCard("85", "86");
				List<Map<String, String>> normalSilverCardList = new ArrayList<Map<String, String>>();
				normalSilverCardList = CodeUtil.objectCovert(normalSilverCardList.getClass(), list);
				log.debug(ESAPIUtil.vaildLog("QRCODE=L , normalSilverCardList=" + normalSilverCardList));
				bs.addData("normalSilverCardList", CodeUtil.toJson(normalSilverCardList));
//				return bs;
			}
			// FOR 特定BRANCH
			else if ("040".equals(BRANCH)) {
				// 20220117 移除永續生活悠遊普卡類別
				// List<ADMCARDDATA> list2 = admCardDataDao.getNormalSilverCard("26", "41");
				List<ADMCARDDATA> list1 = admCardDataDao.getNormalSilverCard("48", "38");
				List<Map<String, String>> normalSilverCardList = new ArrayList<Map<String, String>>();
				normalSilverCardList = CodeUtil.objectCovert(normalSilverCardList.getClass(), list1);
				// normalSilverCardList.addAll(CodeUtil.objectCovert(normalSilverCardList.getClass(),
				// list2));
				log.debug(ESAPIUtil.vaildLog("BRANCH=" + BRANCH + " , normalSilverCardList=" + normalSilverCardList));
				bs.addData("normalSilverCardList", CodeUtil.toJson(normalSilverCardList));
			} else {
				Set<String> set_CardType = new HashSet<String>();
				// 顯示所有卡片
				set_CardType.add("0");
				// 找一般卡卡別
				List<Map<String, String>> otherCardList = admCardDataDao.getOtherCardGroupDATA(set_CardType);
				log.debug(ESAPIUtil.vaildLog("ALL card , otherCardList=" + otherCardList));
				bs.addData("otherCardList", CodeUtil.toJson(otherCardList));
			}
			
			bs.setResult(true);
		} catch (Exception e) {
			bs.setResult(false);
			log.error( "Function cardSelectData Error >> {}" , e.toString());
		}
		return bs;
	}
	
	/**
	 * 通過身分驗證 >> 申請信用卡卡片選擇頁的資料
	 */
	@SuppressWarnings("unchecked")
	public BaseResult cardSelectData2(BaseResult bs,String way) {
		try {
			Map<String, Object> bsData = (Map) bs.getData();
			switch (way) {
			case "1":
				String owncardStr = (String) bsData.get("owncardStr");
				String ApplingCardListStr = (String) bsData.get("ApplingCardListStr");
				Set<String> set_CardType = (Set<String>) bsData.get("owncardSet");
				if(null==set_CardType) {
					set_CardType = new HashSet<String>();
					set_CardType.add("0");
				}
				List<Map<String, String>> otherCardList = admCardDataDao.getOtherCardGroupDATA(set_CardType);
				log.debug(ESAPIUtil.vaildLog("過濾過的卡片列表 , otherCardList=" + otherCardList));
				bs.addData("otherCardList", CodeUtil.toJson(otherCardList));
				bs.setResult(true);
				break;
			case "2":
				String owncardStr2 = (String) ((Map) bsData.get("requestParam")).get("owncardStr");
				String oldcardowner = (String) ((Map) bsData.get("requestParam")).get("oldcardowner");
				String ApplingCardListStr2 = (String) ((Map) bsData.get("requestParam")).get("ApplingCardListStr");
				Set<String> set_CardType2 = (Set<String>) ((Map) bsData.get("requestParam")).get("owncardSet");
				bs.addData("ApplingCardListStr", ((Map) bsData.get("requestParam")).get("ApplingCardListStr"));
				bs.addData("oldcardowner", ((Map) bsData.get("requestParam")).get("oldcardowner"));
				if(null==set_CardType2) {
					set_CardType2 = new HashSet<String>();
					set_CardType2.add("0");
				}
				List<Map<String, String>> otherCardList2 = admCardDataDao.getOtherCardGroupDATA(set_CardType2);
				log.debug(ESAPIUtil.vaildLog("過濾過的卡片列表 , otherCardList=" + otherCardList2));
				bs.addData("otherCardList", CodeUtil.toJson(otherCardList2));
				bs.setResult(true);
				break;
			}
			
		} catch (Exception e) {
			log.debug(e.toString());
		}
		return bs;
	}
	
	/**
	 * 信用卡申請身分驗證及申請卡片狀態取得
	 */
	@SuppressWarnings("unchecked")
	public BaseResult card_apply_authentication_applyCheck(Map<String, String> requestParam) {
		log.info("card_apply_authentication_applyCheck req >> {}", requestParam);
		BaseResult bs =  new BaseResult();
		try {
			bs = NA03Q_REST(requestParam);
			Map<String, Object> bsData = (Map<String, Object>) bs.getData();
			String occurMsg = (bsData.get("occurMsg") == null ? "" : (String) bsData.get("occurMsg"));
			log.trace("occurMsg>>>>>{}", occurMsg);
			log.trace("msgCode>>>>>{}", bsData.get("msgCode"));
			if (occurMsg.equals("") || occurMsg.equals("0000")) {
				if (((String) bsData.get("msgCode")).startsWith("Z")
						|| (((String) bsData.get("msgCode")).startsWith("FE")
								&& ((String) bsData.get("msgCode")).length() == 6)) {
					bs.setResult(false);
					bs.setMsgCode((String) bsData.get("msgCode"));
					getMessageByMsgCode(bs);
					return bs;
					// n810成功查詢 及 未持有本行信用卡 專門挑出來當作成功
				} else if (((String) bsData.get("msgCode")).equals("0")
						|| ((String) bsData.get("msgCode")).equals("E091")) {
					bs.setResult(true);
					// NA03Q pass , check 已持有 or 正在申請中
					boolean doubleApply = false;
					
					// 20220215 新增 重複申請新用卡判斷 START
					StringBuffer applyNameList = new StringBuffer();
					String cusidn = requestParam.get("CUSIDN");
					List<TXNCARDAPPLY> applyList = txnCardApplyDao.findByCUSID(cusidn);
					if (applyList.size() > 0) {
						// 是否重複申請 ? + 申請列表
						for (TXNCARDAPPLY eachCard : applyList) {
							if ("012".contains(eachCard.getSTATUS())) {
								if (eachCard.getCARDNAME1().equals(requestParam.get("CARDNAME"))) {
									//只擷取一次 如果他是true , 就不做事
									if(!doubleApply) {
										doubleApply = true;
									}
								}
								if (applyNameList.toString().length() == 0) {
									applyNameList.append(eachCard.getCARDNAME1());
								} else {
									//如果找不到再加入 ( distinct 概念 )
									if(applyNameList.indexOf(eachCard.getCARDNAME1()) == -1) {
										applyNameList.append("," + eachCard.getCARDNAME1());
									}
								}
							}
						}
					}

					// 申請列表存入bs
					bs.addData("ApplingCardListStr", applyNameList.toString());

					// 重複申請導回卡片選擇
					if (doubleApply) {
						bs.setResult(false);
						bs.setMsgCode("doubleApply");
						bs.addData("doubleApply", doubleApply);
					}
					// 20220215 新增 重複申請信用卡判斷 END

					// 20220218 新增 新舊戶判斷 START
					// 取出NA03Q中n810電文資料
					Map<String, String> n810row = (Map<String, String>) bs.getData();
					List<Map<String, String>> rows = (List<Map<String, String>>) bsData.get("REC");
					// 判斷是否為舊戶
					// 擁有活卡 = 舊戶
					// ================================================
					// 定義活卡條件代碼
					Set<String> set_ACTCardType = new HashSet<String>();
					set_ACTCardType.add("P");
					set_ACTCardType.add("V");
					set_ACTCardType.add("C");
					set_ACTCardType.add("N");
					set_ACTCardType.add("H");
					set_ACTCardType.add("");
					
					boolean oldcardowner = false;
					boolean hasThisCard = false;
					// 已持有卡片
					StringBuffer owncardStr = new StringBuffer();
					Set<String> owncardSet = new HashSet<String>();
					
					int n810cnt = (n810row.get("RECNUM") == null || n810row.get("RECNUM").equals("") ? 0
							: Integer.parseInt(n810row.get("RECNUM")));
					log.trace("RECNUM>>>{}", n810cnt);
					for (int i = 0; i < n810cnt; i++) {
						String CARDTYPE = rows.get(i).get("CARDTYPE");
						log.debug("CARDTYPE={}", CARDTYPE);
						String PT_FLG = rows.get(i).get("PT_FLG");
						log.debug("PT_FLG={}", PT_FLG);
						String BLK_CODE = rows.get(i).get("BLK_CODE");
						log.debug("BLK_CODE={}", BLK_CODE);

						// 正卡 && 活卡 >>> 舊戶
						if ("1".equals(PT_FLG)) {
							// 活卡
							if (set_ACTCardType.contains(BLK_CODE.trim())) {
								//只擷取一次 如果他是true , 就不做事
								if(!oldcardowner) {
									oldcardowner = true;
								}
							}
							String cardno = admCardDataDao.findCARDNO(CARDTYPE);
							//新增已持有標記
							if(requestParam.get("CARDNAME").equals(cardno)) {
								//只擷取一次 如果他是true , 就不做事
								if(!hasThisCard) {
									hasThisCard = true;
								}
							}
							owncardSet.add(CARDTYPE);
							if (owncardStr.toString().length() == 0) {
								owncardStr.append(CARDTYPE);
							} else {
								owncardStr.append("," + CARDTYPE);
							}
							
						}
					}
					
					// 已持有導回卡片選擇
					if (hasThisCard) {
						bs.setResult(false);
						bs.setMsgCode("hasThisCard");
						bs.addData("hasThisCard", hasThisCard);
					}

					if (oldcardowner == true) {
						bs.addData("oldcardowner", "Y");
						bs.addData("CFU2", "2");
						bs.addData("owncardStr", owncardStr.toString());
						bs.addData("owncardSet", owncardSet);
					} else {
						bs.addData("oldcardowner", "N");
						bs.addData("CFU2", "1");
					}
					// set requestParam into bs.data
					bs.addAllData(requestParam);

				} else {
					// 其他錯誤訊息 like E099
					bs.setResult(false);
					bs.setMsgCode((String) bsData.get("msgCode"));
					getMessageByMsgCode(bs);
					return bs;
				}
			} else {
				// 身分驗證NA03Q 有電文之外的錯誤
				bs.setResult(false);
				bs.setMsgCode((String) bsData.get("occurMsg"));
				getMessageByMsgCode(bs);
				return bs;
			}

		} catch (Exception e) {
			log.error("card_apply_authentication error >> {}", e.toString());
		}
		return bs;
	}
	
	/**
	 * 準備申請信用卡確認頁的資料
	 */
	public BaseResult prepareConfirmDataOldcard(Map<String,String> requestParam){
		BaseResult bs = null;
		try{
			bs = new BaseResult();
			for(Entry<String,String> entry : requestParam.entrySet()){
				String key = entry.getKey();
				log.debug(ESAPIUtil.vaildLog("key >> " + key));
				String value = entry.getValue();
				log.debug(ESAPIUtil.vaildLog("value >> " + value));

				if(value ==  null){
					requestParam.put(key,"");
				}
				else if("WORKYEARS".equals(key) || "WORKMONTHS".equals(key) || "CPRIMJOBTITLE".equals(key) || "MPRIMADDR1COND".equals(key) || "MPRIMMARRIAGE".equals(key) || "MPRIMEDUCATION".equals(key) || "CHENAME".equals(key) || "CITY1".equals(key) || "ZONE1".equals(key) || "CITY2".equals(key) || "ZONE2".equals(key) || "CITY4".equals(key) || "ZONE4".equals(key)){
					if("#".equals(value)){
						requestParam.put(key,"");
					}
				}
			}
			
			String CARDMEMO = requestParam.get("CARDMEMO");
			log.debug(ESAPIUtil.vaildLog("CARDMEMO >> " + CARDMEMO));
			String VARSTR2 = requestParam.get("VARSTR2");
			log.debug(ESAPIUtil.vaildLog("VARSTR2 >> " + VARSTR2));

			String CARDDESC = "";
			String CARDDESCpdf = "";
			if(CARDMEMO.equals("1") && VARSTR2.equals("1")){
				//悠遊卡自動加值功能：同意預設開啟
				CARDDESCpdf = "（悠遊卡自動加值功能：同意預設開啟）";
				CARDDESC = "（" + i18n.getMsg("LB.X1821") + "）";
			}
				
			if(CARDMEMO.equals("1") && VARSTR2.equals("2")){
				//悠遊卡自動加值功能：不同意預設開啟
				CARDDESCpdf ="（悠遊卡自動加值功能：不同意預設開啟）";
				CARDDESC ="（" + i18n.getMsg("LB.X1822") + "）";
			}
				
			if(CARDMEMO.equals("2")){
				//一卡通自動加值功能：預設開啟
				CARDDESCpdf = "（一卡通自動加值功能：預設開啟）";
				CARDDESC = "（" + i18n.getMsg("LB.X1823") + "）";
			}
			log.debug("CARDDESC={}",CARDDESC);
			requestParam.put("CARDDESC",CARDDESC);
			requestParam.put("CARDDESCpdf",CARDDESCpdf);
			
			String FGTXWAY = requestParam.get("FGTXWAY");
			log.debug(ESAPIUtil.vaildLog("FGTXWAY >> " + FGTXWAY));

			
			String CHENAME = requestParam.get("CHENAME");
			log.debug(ESAPIUtil.vaildLog("CHENAME >> " + CHENAME));

			String CHENAMEChinese = "";
			String CHENAMEpdf = "";
			if("1".equals(CHENAME)){
				CHENAMEpdf = "是";
//				CHENAMEChinese = "LB.D0034_2";
				CHENAMEChinese = i18n.getMsg("LB.D0034_2");
			}
			else if("2".equals(CHENAME)){
				CHENAMEpdf = "否";
//				CHENAMEChinese = "LB.D0034_3";
				CHENAMEChinese = i18n.getMsg("LB.D0034_3");
			}
			log.debug("CHENAMEChinese={}",CHENAMEChinese);
			requestParam.put("CHENAMEChinese",CHENAMEChinese);
			requestParam.put("CHENAMEpdf",CHENAMEpdf);
			
			String MPRIMEDUCATION = requestParam.get("MPRIMEDUCATION");
			log.debug(ESAPIUtil.vaildLog("MPRIMEDUCATION >> " + MPRIMEDUCATION));

			String MPRIMEDUCATIONChinese = "";
			String MPRIMEDUCATIONpdf = "";
			if("1".equals(MPRIMEDUCATION)){
				//博士
				MPRIMEDUCATIONpdf = "博士";
				MPRIMEDUCATIONChinese = i18n.getMsg("LB.D0588");
			}
			else if("2".equals(MPRIMEDUCATION)){
				//碩士
				MPRIMEDUCATIONpdf = "碩士";
				MPRIMEDUCATIONChinese = i18n.getMsg("LB.D0589");
			}
			else if("3".equals(MPRIMEDUCATION)){
				//大學
				MPRIMEDUCATIONpdf = "大學";
				MPRIMEDUCATIONChinese = i18n.getMsg("LB.D0590");
			}
			else if("4".equals(MPRIMEDUCATION)){
				//專科
				MPRIMEDUCATIONpdf = "專科";
				MPRIMEDUCATIONChinese = i18n.getMsg("LB.D0591");
			}
			else if("5".equals(MPRIMEDUCATION)){
				//高中職
				MPRIMEDUCATIONpdf = "高中職";
				MPRIMEDUCATIONChinese = i18n.getMsg("LB.D0592");
			}
			else if("6".equals(MPRIMEDUCATION)){
				//其他
				MPRIMEDUCATIONpdf = "其他";
				MPRIMEDUCATIONChinese = i18n.getMsg("LB.D0572");
			}
			log.debug("MPRIMEDUCATIONChinese={}",MPRIMEDUCATIONChinese);
			requestParam.put("MPRIMEDUCATIONChinese",MPRIMEDUCATIONChinese);
			requestParam.put("MPRIMEDUCATIONpdf",MPRIMEDUCATIONpdf);
			
			String MPRIMMARRIAGE = requestParam.get("MPRIMMARRIAGE");
			log.debug(ESAPIUtil.vaildLog("MPRIMMARRIAGE >> " + MPRIMMARRIAGE));
			String MPRIMMARRIAGEChinese = "";
			String MPRIMMARRIAGEpdf = "";
			if("1".equals(MPRIMMARRIAGE)){
				//已婚
				MPRIMMARRIAGEpdf = "已婚";
				MPRIMMARRIAGEChinese = i18n.getMsg("LB.D0595");
			}
			else if("2".equals(MPRIMMARRIAGE)){
				//未婚
				MPRIMMARRIAGEpdf = "未婚";
				MPRIMMARRIAGEChinese = i18n.getMsg("LB.D0596");
			}
			else if("3".equals(MPRIMMARRIAGE)){
				//其他
				MPRIMMARRIAGEpdf = "其他";
				MPRIMMARRIAGEChinese = i18n.getMsg("LB.D0572");
			}
			log.debug(ESAPIUtil.vaildLog("MPRIMMARRIAGEChinese >> " + MPRIMMARRIAGEChinese));
			requestParam.put("MPRIMMARRIAGEChinese",MPRIMMARRIAGEChinese);
			requestParam.put("MPRIMMARRIAGEpdf",MPRIMMARRIAGEpdf);
			
			
			String MPRIMADDR1COND = requestParam.get("MPRIMADDR1COND");
			log.debug(ESAPIUtil.vaildLog("MPRIMADDR1COND >> " + MPRIMADDR1COND));
			String MPRIMADDR1CONDChinese = "";
			String MPRIMADDR1CONDpdf = "";
			if("1".equals(MPRIMADDR1COND)){
				//本人或配偶持有
				MPRIMADDR1CONDpdf = "本人或配偶持有";
				MPRIMADDR1CONDChinese = i18n.getMsg("LB.X0548");
			}
			else if("2".equals(MPRIMADDR1COND)){
				//直系親屬持有
				MPRIMADDR1CONDpdf = "直系親屬持有";
				MPRIMADDR1CONDChinese = i18n.getMsg("LB.X0549");
			}
			else if("3".equals(MPRIMADDR1COND)){
				//租賃或宿舍
				MPRIMADDR1CONDpdf = "租賃或宿舍";
				MPRIMADDR1CONDChinese = i18n.getMsg("LB.X0550");
			}
			log.debug("MPRIMADDR1CONDChinese={}",MPRIMADDR1CONDChinese);
			requestParam.put("MPRIMADDR1CONDChinese",MPRIMADDR1CONDChinese);
			requestParam.put("MPRIMADDR1CONDpdf",MPRIMADDR1CONDpdf);
			
			String MBILLTO = requestParam.get("MBILLTO");
			log.debug(ESAPIUtil.vaildLog("MBILLTO >> " + MBILLTO));
			String MBILLTOChinese = "";
			String MBILLTOpdf = "";
			if("1".equals(MBILLTO)){
				//同戶籍地址
				MBILLTOpdf = "同戶籍地址";
				MBILLTOChinese = i18n.getMsg("LB.D1125");
			}
			else if("2".equals(MBILLTO)){
				//同居住地址
				MBILLTOpdf = "同居住地址";
				MBILLTOChinese = i18n.getMsg("LB.X0552");
			}
			else if("3".equals(MBILLTO)){
				//同公司地址
				MBILLTOpdf = "同公司地址";
				MBILLTOChinese = i18n.getMsg("LB.X0553");
			}
			log.debug("MBILLTOChinese={}",MBILLTOChinese);
			requestParam.put("MBILLTOChinese",MBILLTOChinese);
			requestParam.put("MBILLTOpdf",MBILLTOpdf);
			
			String MCARDTO = requestParam.get("MCARDTO");
			log.debug(ESAPIUtil.vaildLog("MCARDTO >> " + MCARDTO));
			String MCARDTOChinese = "";
			String MCARDTOpdf = requestParam.get("CPRIMADDR0");
			log.debug("MCARDTOpdf====>"+MCARDTOpdf);
//			if("1".equals(MCARDTO)){
//				//親領
//				MCARDTOpdf = "親領";
//				MCARDTOChinese = i18n.getMsg("LB.D0072");
//			}
//			else if("2".equals(MCARDTO)){
//				//寄送
//				MCARDTOpdf = "寄送";
//				MCARDTOChinese = i18n.getMsg("LB.D0073");
//			}
			log.debug("MCARDTOChinese={}",MCARDTOChinese);
			requestParam.put("MCARDTOChinese",MCARDTOChinese);
			requestParam.put("MCARDTOpdf",MCARDTOpdf);
			
			String CPRIMJOBTYPE = requestParam.get("CPRIMJOBTYPE");
			log.debug(ESAPIUtil.vaildLog("CPRIMJOBTITLE" + CPRIMJOBTYPE));

			
			String CPRIMJOBTYPEChinese = "";
			String CPRIMJOBTYPEpdf = "";
			//軍官、軍人
			if("061100".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "軍官、軍人";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.X2439");
			}
			//警官、員警
			else if("061200".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "警官、員警";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.X2440");
			}
			//其它公共行政業
			else if("061300".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "其它公共行政業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0873");
			}
			//教育業
			else if("061400".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "教育業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0874");
			}
			//學生
			else if("061410".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "學生";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0081");
			}
			//工、商及服務業
			else if("061500".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "工、商及服務業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0876");
			}
			//農林漁牧業
			else if("0615A0".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "農林漁牧業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0877");
			}
			//礦石及土石採取業
			else if("0615B0".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "礦石及土石採取業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0878");
			}
			//製造業
			else if("0615C0".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "製造業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0879");
			}
			//水電燃氣業
			else if("0615D0".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "水電燃氣業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0880");
			}
			//營造業
			else if("0615E0".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "營造業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0881");
			}
			//批發及零售業
			else if("0615F0".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "批發及零售業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0882");
			}
			//住宿及餐飲業
			else if("0615G0".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "住宿及餐飲業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0883");
			}
			//運輸、倉儲及通信業
			else if("0615H0".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "運輸、倉儲及通信業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0884");
			}
			//金融及保險業
			else if("0615I0".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "金融及保險業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0885");
			}
			//不動產及租賃業
			else if("0615J0".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "不動產及租賃業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0886");
			}
			//其他專業服務業 (建築、電腦資訊、設計、顧問、研發、醫護、社服等服務業)
			else if("061610".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "其他專業服務業 (建築、電腦資訊、設計、顧問、研發、醫護、社服等服務業)";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.X2441");
			}
			//技術服務業 (出版、廣告、影視、休閒、保全、環保、維修、宗教、團體、美髮、殯葬、停車場…等)
			else if("061620".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "技術服務業 (出版、廣告、影視、休閒、保全、環保、維修、宗教、團體、美髮、殯葬、停車場…等)";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.X2442");
			}
			//非法人組織授信戶負責人
			else if("069999".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "非法人組織授信戶負責人";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0899");
			}
			//特定專業服務業(律師、會計師、地政士、公證人、記帳士、記帳及報稅代理人)
			else if("061630".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "特定專業服務業(律師、會計師、地政士、公證人、記帳士、記帳及報稅代理人)";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0889");
			}
			//特定專業服務業(受聘於專業服務業之行政事務職員)
			else if("061640".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "特定專業服務業(受聘於專業服務業之行政事務職員)";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0890");
			}
			//銀樓業 (包含珠寶、鐘錶及貴金屬之製造、批發及零售)
			else if("061500".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "銀樓業 (包含珠寶、鐘錶及貴金屬之製造、批發及零售)";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0891");
			}
			//虛擬貨幣交易服務業
			else if("061660".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "虛擬貨幣交易服務業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0892");
			}
			//博弈業
			else if("061670".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "博弈業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0893");
			}
			//國防武器或戰爭設備相關行業(軍火)
			else if("061680".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "國防武器或戰爭設備相關行業(軍火)";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0894");
			}
			//家管
			else if("061690".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "家管";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0082");
			}
			//自由業
			else if("061691".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "自由業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0083");
			}
			//無業
			else if("061692".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "無業";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.X0571");
			}
			//其他
			else if("061700".equals(CPRIMJOBTYPE)){
				CPRIMJOBTYPEpdf = "其他";
				CPRIMJOBTYPEChinese = i18n.getMsg("LB.D0572");
			}
			log.debug("CPRIMJOBTYPEChinese={}",CPRIMJOBTYPEChinese);
			requestParam.put("CPRIMJOBTYPEChinese",CPRIMJOBTYPEChinese);
			requestParam.put("CPRIMJOBTYPEpdf",CPRIMJOBTYPEpdf);
			
			
			String CPRIMJOBTITLE = requestParam.get("CPRIMJOBTITLE");
			log.debug(ESAPIUtil.vaildLog("CPRIMJOBTITLE >> " + CPRIMJOBTITLE));
			String CPRIMJOBTITLEChinese = "";
			String CPRIMJOBTITLEpdf = "";
			//董事長/負責人
			if("01".equals(CPRIMJOBTITLE)){CPRIMJOBTITLEChinese = i18n.getMsg("LB.X2443");CPRIMJOBTITLEpdf = "董事長/負責人";}
			//總經理
			if("02".equals(CPRIMJOBTITLE)){CPRIMJOBTITLEChinese = i18n.getMsg("LB.X2444");CPRIMJOBTITLEpdf = "總經理";}
			//主管
			if("03".equals(CPRIMJOBTITLE)){CPRIMJOBTITLEChinese = i18n.getMsg("LB.X0561");CPRIMJOBTITLEpdf = "主管";}
			//專業人員
			if("04".equals(CPRIMJOBTITLE)){CPRIMJOBTITLEChinese = i18n.getMsg("LB.X0588");CPRIMJOBTITLEpdf = "專業人員";}
			//職員
			if("05".equals(CPRIMJOBTITLE)){CPRIMJOBTITLEChinese = i18n.getMsg("LB.X2445");CPRIMJOBTITLEpdf = "職員";}
			//業務/服務人員
			if("06".equals(CPRIMJOBTITLE)){CPRIMJOBTITLEChinese = i18n.getMsg("LB.X2446");CPRIMJOBTITLEpdf = "業務/服務人員";}
			//其他
			if("07".equals(CPRIMJOBTITLE)){CPRIMJOBTITLEChinese = i18n.getMsg("LB.D0572");CPRIMJOBTITLEpdf = "其他";}
			
			log.debug("CPRIMJOBTITLEChinese={}",CPRIMJOBTITLEChinese);
			requestParam.put("CPRIMJOBTITLEChinese",CPRIMJOBTITLEChinese);
			requestParam.put("CPRIMJOBTITLEpdf",CPRIMJOBTITLEpdf);
			
			String countrycode = requestParam.get("CTRYDESC1");
			String countryname = "";
			ADMCOUNTRY ADMCOUNTRY = admcountrydao.getCountry(countrycode);
			countryname = ADMCOUNTRY.getADCTRYNAME();
			if(countrycode.equals("TW")) {
				countryname = "中華民國";
			}
			log.debug(ESAPIUtil.vaildLog("countryname={}"+countryname));
			requestParam.put("COUNTRYNAME",countryname);
			
			String BRANCHNAME = requestParam.get("BRANCHNAME");
			log.debug(ESAPIUtil.vaildLog("BRANCHNAME={}"+BRANCHNAME));
			
//			String BRHNAME = "";
//			String BRHNAMEEN = "";
//			String BRHNAMECH = "";
//			String bankAddr = "";
//			String bankAddrEN = "";
//			String bankAddrCH = "";
//			String bankTel = "";
//			if(!"".equals(BRANCHNAME)){
//				//轉換成分行名稱
//				List<ADMBRANCH> admBranchList = admBranchDao.findByBhID(BRANCHNAME);
//				log.debug(ESAPIUtil.vaildLog("admBranchList="+admBranchList));
//				
//				BRHNAME = admBranchList.get(0).getADBRANCHNAME();
//				BRHNAMEEN = admBranchList.get(0).getADBRANENGNAME();
//				BRHNAMECH = admBranchList.get(0).getADBRANCHSNAME();
//				bankAddr = admBranchList.get(0).getADDRESS();
//				bankAddrEN = admBranchList.get(0).getADDRESSENG();
//				bankAddrCH = admBranchList.get(0).getADDRESSCHS();
//				bankTel = admBranchList.get(0).getTELNUM();
//			}
//			log.debug(ESAPIUtil.vaildLog("BRHNAME="+BRHNAME));
//			log.debug(ESAPIUtil.vaildLog("BRHNAMEEN="+BRHNAMEEN));
//			log.debug(ESAPIUtil.vaildLog("BRHNAMECH="+BRHNAMECH));
//			requestParam.put("BRHNAME",BRHNAME);
//			requestParam.put("BRHNAMEEN",BRHNAMEEN);
//			requestParam.put("BRHNAMECH",BRHNAMECH);
//			requestParam.put("bankAddr",bankAddr);
//			requestParam.put("bankAddrEN",bankAddrEN);
//			requestParam.put("bankAddrCH",bankAddrCH);
//			requestParam.put("bankTel",bankTel);
//			
//			//身分證補換發資訊
//			String chantype = requestParam.get("CHANTYPE");
//			String CHANTYPEpdf = "";
//			String CHANTYPEChinese = "";
//			if(chantype.equals("1")) {
//				CHANTYPEpdf = "初領";
//				CHANTYPEChinese = i18n.getMsg("LB.D1113");
//			}
//			else if(chantype.equals("2")) {
//				CHANTYPEpdf = "補發";
//				CHANTYPEChinese = i18n.getMsg("LB.W1665");
//			}
//			else if(chantype.equals("3")) {
//				CHANTYPEpdf = "換發";
//				CHANTYPEChinese = i18n.getMsg("LB.W1664");
//			}
//			requestParam.put("CHANTYPEChinese",CHANTYPEChinese);
//			
//			String CHDATEY = "";
//			String CHDATEM = "";
//			String CHDATED = "";
//			String CHANCITY = "";
//			if(StrUtils.isNotEmpty(requestParam.get("CHDATEY")) && !requestParam.get("CHDATEY").equals("#")) {
//				CHDATEY = requestParam.get("CHDATEY");
//			}
//			if(StrUtils.isNotEmpty(requestParam.get("CHDATEM")) && !requestParam.get("CHDATEM").equals("#")) {
//				CHDATEM = requestParam.get("CHDATEM");
//			}
//			if(StrUtils.isNotEmpty(requestParam.get("CHDATED")) && !requestParam.get("CHDATED").equals("#")) {
//				CHDATED = requestParam.get("CHDATED");
//			}
//			if(StrUtils.isNotEmpty(requestParam.get("CHANCITY")) && !requestParam.get("CHANCITY").equals("#")) {
//				CHANCITY = requestParam.get("CHANCITY");
//			}
//			String chandata = i18n.getMsg("LB.D0583") + CHDATEY + i18n.getMsg("LB.Year") + CHDATEM + i18n.getMsg("LB.Month") + CHDATED + i18n.getMsg("LB.D0586") + " " +
//					CHANCITY + " " + CHANTYPEChinese;
//			requestParam.put("CHDATA",chandata);
			
			bs.addAllData(requestParam);
//			bs.addData("requestParam",requestParam);
			bs.setResult(true);
		}
		catch(Exception e){
			//e.printStackTrace();
			//Avoid Information Exposure Through an Error Message
			log.error("prepareConfirmData error >> {}",e); 
		}
		return bs;
	}
	
	/**
	 * 準備前往申請信用卡結果頁P6的資料(舊戶)
	 */
	@SuppressWarnings("unchecked")
	public Boolean prepareCreditCardApplyP6DataOldcard(Map<String,String> reqParam, String uploadPath, Model model){
		try{
			for(Entry<String,String> entry : reqParam.entrySet()){
				String key = entry.getKey();
				String value = entry.getValue();
				log.debug(ESAPIUtil.vaildLog(key+"==>"+value));
			}
			
			
			String MBILLTO = reqParam.get("MBILLTO");
			log.debug(ESAPIUtil.vaildLog("MBILLTO >> " + MBILLTO));
			
			String strZIP = reqParam.get("strZIP");
			log.debug(ESAPIUtil.vaildLog("strZIP >> " + strZIP));
			
			String CPRIMADDR0 = reqParam.get("CPRIMADDR0");
			log.debug(ESAPIUtil.vaildLog("CPRIMADDR0 >> " + CPRIMADDR0));
			reqParam.put("MCARDTOpdf","掛號郵寄");
			reqParam.put("MCARDTOChinese","掛號郵寄");
			reqParam.put("MCARDTO","2");
			reqParam.put("MBILLTOpdf",strZIP+CPRIMADDR0);
			reqParam.put("MBILLTOChinese",strZIP+CPRIMADDR0);
			reqParam.put("CPRIMADDR2","");
			reqParam.put("CPRIMADDR","");
			reqParam.put("CPRIMADDR3","");
			
			
			String CPRIMCHNAMEFull = CodeUtil.convertFullorHalf(reqParam.get("CPRIMCHNAME"),1);
			log.debug(ESAPIUtil.vaildLog("CPRIMCHNAMEFull >> " + CPRIMCHNAMEFull));
			reqParam.put("CPRIMCHNAMEFull",CPRIMCHNAMEFull);
					
			String CPRIMADDR2Full = CodeUtil.convertFullorHalf(reqParam.get("CPRIMADDR2"),1);
			log.debug(ESAPIUtil.vaildLog("CPRIMADDR2Full >> " + CPRIMADDR2Full));
			reqParam.put("CPRIMADDR2Full",CPRIMADDR2Full);
			
			String CPRIMADDRFull = CodeUtil.convertFullorHalf(reqParam.get("CPRIMADDR"),1);
			log.debug(ESAPIUtil.vaildLog("CPRIMADDRFull >> " + CPRIMADDRFull));
			reqParam.put("CPRIMADDRFull",CPRIMADDRFull);
			
			String CPRIMCOMPANYFull = CodeUtil.convertFullorHalf(reqParam.get("CPRIMCOMPANY"),1);
			log.debug(ESAPIUtil.vaildLog("CPRIMCOMPANYFull >> " + CPRIMCOMPANYFull));
			reqParam.put("CPRIMCOMPANYFull",CPRIMCOMPANYFull);
			
			String CPRIMADDR3Full = CodeUtil.convertFullorHalf(reqParam.get("CPRIMADDR3"),1);
			log.debug(ESAPIUtil.vaildLog("CPRIMADDR3Full >> " + CPRIMADDR3Full));
			reqParam.put("CPRIMADDR3Full",CPRIMADDR3Full);
			
			String CTRYDESCFull = CodeUtil.convertFullorHalf(reqParam.get("COUNTRYNAME"),1);
			log.trace(ESAPIUtil.vaildLog("CTRYDESCFull >> " + CTRYDESCFull));
			reqParam.put("CTRYDESCFull",CTRYDESCFull);
			
			String CFU2 = reqParam.get("CFU2");
			log.debug(ESAPIUtil.vaildLog("CFU2 >> " + CFU2));
			
			String QRCODE = reqParam.get("QRCODE").isEmpty() ? "" : reqParam.get("QRCODE");
			log.debug(ESAPIUtil.vaildLog("QRCODE >> " + QRCODE));
			
			String partition = reqParam.get("partition").isEmpty() ? "" : reqParam.get("partition");
			log.debug(ESAPIUtil.vaildLog("partition >> " + partition));
			
			String branch = reqParam.get("branch").isEmpty() ? "" : reqParam.get("branch");
			log.debug(ESAPIUtil.vaildLog("branch >> " + branch));
			
			String memberId = reqParam.get("memberId").isEmpty() ? "" : reqParam.get("memberId");
			log.debug(ESAPIUtil.vaildLog("memberId >> " + memberId));
			
			String BRNO = "";
			if(CFU2.equals("1")){
				String CPRIMID = reqParam.get("CPRIMID");
				log.debug(ESAPIUtil.vaildLog("CPRIMID >> " + CPRIMID));

				
				List<TXNUSER> listtxnUser = txnUserDao.findByUserId(CPRIMID);
				
				if(!listtxnUser.isEmpty()){
					TXNUSER txnuser = listtxnUser.get(0);
					BRNO = txnuser.getADBRANCHID();
					if(BRNO != null){
						BRNO = BRNO.trim();
					}
					else{
						BRNO = "";
					}
				}
			}
			log.debug(ESAPIUtil.vaildLog("BRNO={}"+BRNO));
			reqParam.put("BRNO",BRNO);
			
//			//原住民姓名轉全型
//			String CUSNAME = reqParam.get("CUSNAME").isEmpty() ? "" : CodeUtil.convertFullorHalf(reqParam.get("CUSNAME"), 1);
//			reqParam.put("CUSNAME", CUSNAME);
//			//羅馬拼音姓名轉全型
//			String ROMANAME = reqParam.get("ROMANAME").isEmpty() ? "" : CodeUtil.convertFullorHalf(reqParam.get("ROMANAME"), 1);
//			reqParam.put("ROMANAME", ROMANAME);
	
			TXNCARDAPPLY attrib = new TXNCARDAPPLY();
			try {
				attrib = CodeUtil.objectCovert(attrib.getClass(),reqParam);
				
				attrib.setCARDNAME2("");
				attrib.setCARDNAME3("");
				attrib.setCPRIMCHNAME(CPRIMCHNAMEFull);//中文姓名，必填
				attrib.setCPRIMADDR2(CPRIMADDR2Full);
				attrib.setCPRIMADDR(CPRIMADDRFull);
				attrib.setCPRIMCOMPANY(CPRIMCOMPANYFull);
				attrib.setCPRIMADDR3(CPRIMADDR3Full);
				attrib.setCTRYDESC(CTRYDESCFull);
				if(reqParam.get("CARDNAME").equals("85") || reqParam.get("CARDNAME").equals("86")) {
					attrib.setZONE(partition);
					attrib.setTEAM(branch);
					attrib.setMID(memberId);
				}
				
				//出生日期
				String CPRIMBIRTHDAY = reqParam.get("CPRIMBIRTHDAY");
				
				//轉成民國
				if(StrUtil.isNotEmpty(CPRIMBIRTHDAY) && CPRIMBIRTHDAY.length()==8) {
					String yyyy = CPRIMBIRTHDAY.substring(0, 4);
					String MM = CPRIMBIRTHDAY.substring(4, 6);
					String dd = CPRIMBIRTHDAY.substring(6, 8);
					String yyy = String.valueOf(Integer.valueOf(yyyy)-1911);
					if(yyy.length()==2) {
						yyy = "0"+yyy;
					}
					CPRIMBIRTHDAY=yyy+MM+dd;
				}
				
				String FGTXWAY = reqParam.get("FGTXWAY");
				log.debug(ESAPIUtil.vaildLog("FGTXWAY={}"+FGTXWAY));
				
				//不是晶片金融卡
				if(!"2".equals(FGTXWAY)){
					CPRIMBIRTHDAY = CPRIMBIRTHDAY.replaceAll("/","");
					if(CPRIMBIRTHDAY.length() >= 7){
						CPRIMBIRTHDAY = CPRIMBIRTHDAY.substring(0,3) + "/" + CPRIMBIRTHDAY.substring(3,5) + "/" + CPRIMBIRTHDAY.substring(5,7);
					}
				}
		        log.debug(ESAPIUtil.vaildLog("CPRIMBIRTHDAY>>" + CPRIMBIRTHDAY));
				reqParam.put("CPRIMBIRTHDAY",CPRIMBIRTHDAY);
				attrib.setCPRIMBIRTHDAY(CPRIMBIRTHDAY);//出生年月日，必填
				
				attrib.setCARDNAME1(reqParam.get("CARDNAME"));//卡片名稱，必填
				
				attrib.setCUSNAME(reqParam.get("CUSNAME"));//原住民姓名
				attrib.setROMANAME(reqParam.get("ROMANAME"));//羅馬拼音姓名
				attrib.setPAYMONEY(reqParam.get("PAYMONEY"));//往來產品之預期金額
			
				txnCardApplyDao.save(attrib);
			}
			catch(Exception e) {
				//e.printStackTrace();
				log.error("DAO SAVE ERROR",e);
				return false;
			}
//			Locale currentLocale = LocaleContextHolder.getLocale();
//			log.trace("GET LOCALE>>>{}",currentLocale);
//			LocaleContextHolder.setLocale(Locale.TAIWAN);
//			currentLocale = LocaleContextHolder.getLocale();
//			log.trace("GET LOCALE>>>{}",currentLocale);
			
			getCreditCardApplyPDFParameter(reqParam);
			log.debug(ESAPIUtil.vaildLog("reqParam={}"+reqParam));
			
			String isSessionID = SpringBeanFactory.getBean("isSessionID");
			log.debug("isSessionID={}",isSessionID);
			
			FTPUtil ftputil = new FTPUtil();
			Base64 base64 = new Base64();
			
			
			//最後上傳TXT檔的<CNT>欄位的值
			int txtCount = 0;
			
			PDFUtil pdfUtil = new PDFUtil();
			InputStream inputStream = pdfUtil.HTMLtoPDF(pdfUtil.getPDFHTML(reqParam));
			downloadFile(inputStream, reqParam.get("FILE6"));
		
			//PDF上傳
			//TODO 20191029 eloan FTP 暫時不連，上線要記得開啟
			try {
				if(inputStream != null && !reqParam.get("FILE6").isEmpty()){
					//LOCAL
					String filename = Paths.get(reqParam.get("FILE6")).getFileName().toString();
					String validFileName = ESAPIUtil.GetValidPathPart(uploadPath,filename);
					byte[] bytes = Files.readAllBytes(Paths.get(validFileName));
					if("N".equals(isSessionID)){
						FTPClient ftpclient = ftputil.connect("192.168.50.191",21,"ftpuser3","25082201",30000);
						boolean result = ftputil.uploadFile(ftpclient,reqParam.get("FILE6"),bytes);
						
						log.debug("result={}",result);
						ftputil.finish(ftpclient);
						
						if(result){
							txtCount++;
						}
					}
					else{
						Map<String,String> ftp = SpringBeanFactory.getBean("eloanFTP");
						String use = ftp.get("use");
						String ip = ftp.get("ip");
						int port = Integer.parseInt(ftp.get("port"));
						String name = ftp.get("name");
						String pw = ftp.get("password");
						String path = ftp.get("path");
						String decodeString = new String(base64.decode(pw.getBytes()));
						int timeout = Integer.parseInt(ftp.get("timeout"));
						log.debug("Get ip>>>" + ip + " pa>>" + path);
						if("Y".equals(use)){
				        	FTPClient ftpclient = ftputil.connect(ip,port,name,decodeString,timeout);
							ftputil.ChangeToDirectory(ftpclient,path);
							
							ftpclient.setBufferSize(1024);
							//重要	
							ftpclient.enterLocalPassiveMode();
//							boolean result = ftputil.uploadFile(ftpclient,reqParam.get("FILE6"),IOUtils.toByteArray(inputStream));
							boolean result = ftputil.uploadFile(ftpclient,reqParam.get("FILE6"),bytes);
							
							log.debug("result={}" + result);
							ftputil.finish(ftpclient);
							
							if(result){
								txtCount++;
							}
						}
					}
				}
//				//身分證正面圖片
//				if(reqParam.get("FILE1") != null && !reqParam.get("FILE1").isEmpty()){
//					//修改 Absolute Path Traversal
//					
//					
//				
//					String filename = Paths.get(reqParam.get("FILE1")).getFileName().toString();
//					String validFileName = ESAPIUtil.GetValidPathPart(uploadPath,filename);
//					byte[] bytes = Files.readAllBytes(Paths.get(validFileName));
//					//LOCAL
//					if("N".equals(isSessionID)){
//						FTPClient ftpclient = ftputil.connect("192.168.50.191",21,"ftpuser","ftpuser",30000);
//						boolean result = ftputil.uploadFile(ftpclient,reqParam.get("FILE1name"),bytes);
//						ftputil.finish(ftpclient);
//						
//						if(result){
//							txtCount++;
//						}
//					}
//					else{
//						Map<String, String> ftp = SpringBeanFactory.getBean("eloanFTP");
//						String use = ftp.get("use");
//						String ip = ftp.get("ip");
//						int port = Integer.parseInt(ftp.get("port"));
//						String name = ftp.get("name");
//						String pw = ftp.get("password");
//						String path = ftp.get("path");
//						String decodeString = new String(base64.decode(pw.getBytes()));
//						int timeout = Integer.parseInt(ftp.get("timeout"));
//						if("Y".equals(use)){
//				        	FTPClient ftpclient = ftputil.connect(ip,port,name,decodeString,timeout);
//							ftputil.ChangeToDirectory(ftpclient,path);
//	//						String FILE1 = attrib.getFILE1();
//							String FILE1 = reqParam.get("FILE1name");
//							log.debug("FILE1={}",ESAPIUtil.vaildLog(FILE1));
//							
//							ftpclient.setBufferSize(1024);
//							//重要	
//							ftpclient.enterLocalPassiveMode();
//							boolean result = ftputil.uploadFile(ftpclient,FILE1,bytes);
//							log.debug(ESAPIUtil.vaildLog("「"+FILE1+"」上傳"+ result));
//							ftputil.finish(ftpclient);
//			
//							if(result){
//								txtCount++;
//							}
//				        }
//					}
//				}
//				//身分證反面圖片
//				if(reqParam.get("FILE2") != null && !reqParam.get("FILE2").isEmpty()){
//					//byte[] bytes = Files.readAllBytes(new File(uploadPath+reqParam.get("FILE2")).toPath());
//					//fix APT		
//					String filename = Paths.get(reqParam.get("FILE2")).getFileName().toString();
//					String validFileName = ESAPIUtil.GetValidPathPart(uploadPath,filename);
//					byte[] bytes = Files.readAllBytes(Paths.get(validFileName));
//					
//					
//					
//					//LOCAL
//					if("N".equals(isSessionID)){
//						FTPClient ftpclient = ftputil.connect("192.168.50.191",21,"ftpuser","ftpuser",30000);
//						boolean result = ftputil.uploadFile(ftpclient,reqParam.get("FILE2name"),bytes);
//						ftputil.finish(ftpclient);
//						
//						if(result){
//							txtCount++;
//						}
//					}
//					else{
//						Map<String, String> ftp = SpringBeanFactory.getBean("eloanFTP");
//						String use = ftp.get("use");
//						String ip = ftp.get("ip");
//						int port = Integer.parseInt(ftp.get("port"));
//						String name = ftp.get("name");
//						String pw = ftp.get("password");
//						String path = ftp.get("path");
//						String decodeString = new String(base64.decode(pw.getBytes()));
//						int timeout = Integer.parseInt(ftp.get("timeout"));
//						if("Y".equals(use)){
//				        	FTPClient ftpclient = ftputil.connect(ip,port,name,decodeString,timeout);
//							ftputil.ChangeToDirectory(ftpclient,path);
//	//						String FILE2 = attrib.getFILE2();
//							String FILE2 = reqParam.get("FILE2name");
//							log.debug("FILE2={}",ESAPIUtil.vaildLog(FILE2));
//							
//							ftpclient.setBufferSize(1024);
//							//重要	
//							ftpclient.enterLocalPassiveMode();
//							boolean result = ftputil.uploadFile(ftpclient,FILE2,bytes);
//							log.debug(ESAPIUtil.vaildLog("「"+FILE2+"」上傳"+ result));
//							ftputil.finish(ftpclient);
//							
//							if(result){
//								txtCount++;
//							}
//				        }
//					}
//				}
//				//財力證明圖片1
//				if(reqParam.get("FILE3") != null && !reqParam.get("FILE3").isEmpty()){
//					//byte[] bytes = Files.readAllBytes(new File(uploadPath+reqParam.get("FILE3")).toPath());
//					//fix APT
//				
//					String filename = Paths.get(reqParam.get("FILE3")).getFileName().toString();
//					String validFileName = ESAPIUtil.GetValidPathPart(uploadPath,filename);
//					byte[] bytes = Files.readAllBytes(Paths.get(validFileName));
//					//LOCAL
//					if("N".equals(isSessionID)){
//						FTPClient ftpclient = ftputil.connect("192.168.50.191",21,"ftpuser","ftpuser",30000);
//						boolean result = ftputil.uploadFile(ftpclient,reqParam.get("FILE3name"),bytes);
//						ftputil.finish(ftpclient);
//						
//						if(result){
//							txtCount++;
//						}
//					}
//					else{
//						Map<String, String> ftp = SpringBeanFactory.getBean("eloanFTP");
//						String use = ftp.get("use");
//						String ip = ftp.get("ip");
//						int port = Integer.parseInt(ftp.get("port"));
//						String name = ftp.get("name");
//						String pw = ftp.get("password");
//						String path = ftp.get("path");
//						String decodeString = new String(base64.decode(pw.getBytes()));
//						int timeout = Integer.parseInt(ftp.get("timeout"));
//						if("Y".equals(use)){
//				        	FTPClient ftpclient = ftputil.connect(ip,port,name,decodeString,timeout);
//							ftputil.ChangeToDirectory(ftpclient,path);
//	//						String FILE3 = attrib.getFILE3();
//							String FILE3 = reqParam.get("FILE3name");
//							log.debug("FILE3={}",ESAPIUtil.vaildLog(FILE3));
//							
//							ftpclient.setBufferSize(1024);
//							//重要	
//							ftpclient.enterLocalPassiveMode();
//							boolean result = ftputil.uploadFile(ftpclient,FILE3,bytes);
//							log.debug("「"+FILE3+"」上傳"+ result);
//							ftputil.finish(ftpclient);
//							
//							if(result){
//								txtCount++;
//							}
//				        }
//					}
//				}
//				//財力證明圖片2
//				if(reqParam.get("FILE4") != null && !reqParam.get("FILE4").isEmpty()){
//					//byte[] bytes = Files.readAllBytes(new File(uploadPath+reqParam.get("FILE4")).toPath());
//					//fix APT
//					String filename = Paths.get(reqParam.get("FILE4")).getFileName().toString();
//					String validFileName = ESAPIUtil.GetValidPathPart(uploadPath,filename);
//					byte[] bytes = Files.readAllBytes(Paths.get(validFileName));
//					//LOCAL
//					if("N".equals(isSessionID)){
//						FTPClient ftpclient = ftputil.connect("192.168.50.191",21,"ftpuser","ftpuser",30000);
//						boolean result = ftputil.uploadFile(ftpclient,reqParam.get("FILE4name"),bytes);
//						ftputil.finish(ftpclient);
//						
//						if(result){
//							txtCount++;
//						}
//					}
//					else{
//						Map<String, String> ftp = SpringBeanFactory.getBean("eloanFTP");
//						String use = ftp.get("use");
//						String ip = ftp.get("ip");
//						int port = Integer.parseInt(ftp.get("port"));
//						String name = ftp.get("name");
//						String pw = ftp.get("password");
//						String path = ftp.get("path");
//						String decodeString = new String(base64.decode(pw.getBytes()));
//						int timeout = Integer.parseInt(ftp.get("timeout"));
//						if("Y".equals(use)){
//				        	FTPClient ftpclient = ftputil.connect(ip,port,name,decodeString,timeout);
//							ftputil.ChangeToDirectory(ftpclient,path);
//	//						String FILE4 = attrib.getFILE4();
//							String FILE4 = reqParam.get("FILE4name");
//							log.debug("FILE4={}",FILE4);
//							
//							ftpclient.setBufferSize(1024);
//							//重要	
//							ftpclient.enterLocalPassiveMode();
//							boolean result = ftputil.uploadFile(ftpclient,FILE4,bytes);
//							log.debug("「"+FILE4+"」上傳"+ result);
//							ftputil.finish(ftpclient);
//							
//							if(result){
//								txtCount++;
//							}
//				        }
//					}
//				}
//				//財力證明圖片3
//				if(reqParam.get("FILE5") != null && !reqParam.get("FILE5").isEmpty()){
//					//byte[] bytes = Files.readAllBytes(new File(uploadPath+reqParam.get("FILE5")).toPath());
//					// fix APT
//					String filename = Paths.get(reqParam.get("FILE5")).getFileName().toString();
//					String validFileName = ESAPIUtil.GetValidPathPart(uploadPath,filename);
//					byte[] bytes = Files.readAllBytes(Paths.get(validFileName));
//					//LOCAL
//					if("N".equals(isSessionID)){
//						FTPClient ftpclient = ftputil.connect("192.168.50.191",21,"ftpuser","ftpuser",30000);
//						boolean result = ftputil.uploadFile(ftpclient,reqParam.get("FILE5name"),bytes);
//						ftputil.finish(ftpclient);
//						
//						if(result){
//							txtCount++;
//						}
//					}
//					else{
//						Map<String, String> ftp = SpringBeanFactory.getBean("eloanFTP");
//						String use = ftp.get("use");
//						String ip = ftp.get("ip");
//						int port = Integer.parseInt(ftp.get("port"));
//						String name = ftp.get("name");
//						String pw = ftp.get("password");
//						String path = ftp.get("path");
//						String decodeString = new String(base64.decode(pw.getBytes()));
//						int timeout = Integer.parseInt(ftp.get("timeout"));
//						if("Y".equals(use)){
//				        	FTPClient ftpclient = ftputil.connect(ip,port,name,decodeString,timeout);
//							ftputil.ChangeToDirectory(ftpclient,path);
//	//						String FILE5 = attrib.getFILE5();
//							String FILE5 = reqParam.get("FILE5name");
//							log.debug("FILE5={}",FILE5);
//							
//							ftpclient.setBufferSize(1024);
//							//重要	
//							ftpclient.enterLocalPassiveMode();
//							boolean result = ftputil.uploadFile(ftpclient,FILE5,bytes);
//							log.debug("「"+FILE5+"」上傳"+ result);
//							ftputil.finish(ftpclient);
//							
//							if(result){
//								txtCount++;
//							}
//				        }
//					}
//				}
			}
			catch(Exception e) {
				log.error("",e);
			}
			log.debug("txtCount={}",txtCount);
			
			Map<String,String> newParamMap = new HashMap<String,String>();
			
			newParamMap = CodeUtil.objectCovert(newParamMap.getClass(),attrib);
			log.debug("newParamMap={}",newParamMap);
			
			//將所有newparamMap裡面的null值換成空值
			Set<String> set = newParamMap.keySet();
			Iterator<String> iterator = set.iterator();
			while(iterator.hasNext()){
				String key = iterator.next();
				log.debug("key={}",key);
				String value = newParamMap.get(key);
				log.debug("value={}",value);
				if(value == null){
					newParamMap.put(key,"");
				}
			}
			//將國名轉成國家代碼
//			String ctrycode = compareCTRY(attrib.getCTRYDESC());
			String ctrycode = reqParam.get("CTRYDESC1");
			log.trace(ESAPIUtil.vaildLog("ctrycode>>>>>>{}"+ctrycode));
			
			newParamMap.put("CTRYDESC",ctrycode);
			newParamMap.put("CNT",String.valueOf(txtCount));
			newParamMap.put("WEBSITE","1");
			//職位名稱要傳中文
			newParamMap.put("CPRIMJOBTITLE", reqParam.get("CPRIMJOBTITLEpdf"));
			//身分證補換發資料
			String dateY = (StrUtils.isNotEmpty(reqParam.get("CHDATEY")) && !reqParam.get("CHDATEY").equals("#")) ? reqParam.get("CHDATEY") : "";
			String dateM = (StrUtils.isNotEmpty(reqParam.get("CHDATEM")) && !reqParam.get("CHDATEM").equals("#")) ? reqParam.get("CHDATEM") : "";
			String dateD = (StrUtils.isNotEmpty(reqParam.get("CHDATED")) && !reqParam.get("CHDATED").equals("#")) ? reqParam.get("CHDATED") : "";
			
			newParamMap.put("CHANDATE", dateY + dateM + dateD);
			newParamMap.put("CHANCITY", StrUtils.isNotEmpty(reqParam.get("CHANCITY")) ? reqParam.get("CHANCITY") : "");
			newParamMap.put("CHANTYPE", StrUtils.isNotEmpty(reqParam.get("CHANTYPE")) ? reqParam.get("CHANTYPE") : "");
			
			String templatePath = "/uploadTemplate/ApplyCreditCard.txt";
			log.debug("templatePath={}",templatePath);
			
			Resource resource = new ClassPathResource(templatePath);
//			File file = resource.getFile();
			
			//信用卡申請TXT
			//修改 Incorrect Permission Assignment For Critical Resources
			if(resource.exists()){
				inputStream = new BufferedInputStream(resource.getInputStream());
			
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
				StringBuilder stringBuilder = new StringBuilder();
				String line;
				line = bufferedReader.readLine();
				while(line != null){
					log.debug("line={}",line);
					ESAPIUtil.validInput(line,"GeneralString",true);
					
					String tagName = line.replace("<","").replace(">","");
					log.debug("tagName={}",tagName);
					String value = newParamMap.get(tagName);
					log.debug("value={}",value);
					
					if(value != null && !"".equals(value)){
						line = line + value;
					}
					
					stringBuilder.append(line).append("\n");
					line = bufferedReader.readLine();
				}
				log.debug("stringBuilder.toString()={}",stringBuilder.toString());
				
				try {
					log.debug("檔案落地>" + newParamMap.get("RCVNO") + ".TXT");
					String separ = File.separator;
					String skinPath = context.getRealPath("") + separ + "com" + separ + "updateTmp" + separ + newParamMap.get("RCVNO") + ".TXT";
					byte[] bytes = (stringBuilder.toString()).getBytes("UTF-8");
					
					// 修正Incorrect Permission Assignment For Critical Resources
					BufferedOutputStream downloadFile = new BufferedOutputStream(new FileOutputStream(skinPath));
//					FileOutputStream downloadFile = new FileOutputStream(skinPath);
					downloadFile.write(bytes);
					downloadFile.close();
					try {
						String movePath = SpringBeanFactory.getBean("imgMovePath") + separ + newParamMap.get("RCVNO") + ".TXT";
						// 修正Incorrect Permission Assignment For Critical Resources
						BufferedOutputStream imgmove = new BufferedOutputStream(new FileOutputStream(movePath));
//						FileOutputStream imgmove = new FileOutputStream(movePath);
						imgmove.write(bytes);
						imgmove.close();
					}
					catch (Exception e) {
						log.error("imgmove error>>>"+e);
					}
				}
				catch (Exception e) {
					log.debug("檔案落地失敗>>>"+e);
				}
			
				//LOCAL
				if("N".equals(isSessionID)){
					FTPClient ftpclient = ftputil.connect("192.168.50.191",21,"ftpuser","ftpuser",30000);
					boolean result = ftputil.uploadFile(ftpclient,newParamMap.get("RCVNO") + ".TXT",stringBuilder.toString().getBytes("UTF-8"));
					
					log.debug("result={}",result);
					ftputil.finish(ftpclient);
				}
				else{
					Map<String,String> ftp = SpringBeanFactory.getBean("eloanFTP");
					String use = ftp.get("use");
					String ip = ftp.get("ip");
					int port = Integer.parseInt(ftp.get("port"));
					String name = ftp.get("name");
					String pw = ftp.get("password");
					String path = ftp.get("path");
					String decodeString = new String(base64.decode(pw.getBytes()));
					int timeout = Integer.parseInt(ftp.get("timeout"));
					if("Y".equals(use)){
			        	FTPClient ftpclient = ftputil.connect(ip,port,name,decodeString,timeout);
						ftputil.ChangeToDirectory(ftpclient,path);
						
						boolean result = ftputil.uploadFile(ftpclient,newParamMap.get("RCVNO") + ".TXT",stringBuilder.toString().getBytes("BIG5"));
						log.debug("result={}" + result);
						ftputil.finish(ftpclient);
					}
				}
				try{
					bufferedReader.close();
				}
				catch(Exception e){
					log.error("",e);
				}
			}
			//處理N930，N931，TXNUSER，寄電子郵件
			NA03_REST(reqParam);
			
			model.addAttribute("requestParam",reqParam);
		}
		catch(Exception e){
			//e.printStackTrace();
			//Avoid Information Exposure Through an Error Message
			log.error("prepareCreditCardApplyP6DataOldcard error >> {}",e);   
		}
		return true;
	}	
	
	 /**
		 * 只發簡訊通知
		 * @param cusidn
		 * @param option
		 * @param type
		 * @param otp
		 * @param adopid
		 * @return 
		 */	
		public BaseResult SmsService(String cusidn , String phone ,String option ,String type,String otp,String adopid, String cn)
		{
			log.trace("SmsService service start");
			BaseResult bs = new BaseResult();
			Map <String,String> reqParam = null;
			try
			{
				reqParam = new HashMap<String,String>();
				reqParam.put("CUSIDN", cusidn);
				reqParam.put("FuncType", "2");
				reqParam.put("OTP", otp);
				reqParam.put("SMSADOPID", adopid);
				reqParam.put("PHONE", phone);
				reqParam.put("CN", cn);
//				TODO 本機開發可先註解 設定為true
//				bs.setResult(Boolean.TRUE);
				
				bs=SmsOtpWithphone_REST(reqParam);
				if(bs.getResult()) {
					log.info("簡訊發送成功...");				
				}else {
					log.error("簡訊發送失敗...");
				}
				
			}
			catch (Exception e)
			{
				log.error("SmsService Error >>{}", e);
			}
			log.trace("SmsService service end");
			return bs;
		}
		
}