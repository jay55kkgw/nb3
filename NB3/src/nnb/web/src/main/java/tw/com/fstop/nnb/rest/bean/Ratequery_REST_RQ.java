package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class Ratequery_REST_RQ extends BaseRestBean_PS implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -7087026345949712853L;

	private String QUERYTYPE;// 查詢代號

	public String getQUERYTYPE() {
		return QUERYTYPE;
	}

	public void setQUERYTYPE(String qUERYTYPE) {
		QUERYTYPE = qUERYTYPE;
	}
}
