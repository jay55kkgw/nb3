package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class Ratequery_N024_RSDATA extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 119085759046566512L;

	private String FILL;
	private String ITR1;
	private String ITR2;
	private String HEADER;
	private String TIME;
	private String CRY;
	private String CRYNAME;
	private String DATE;
	private String RECNO;
	private String COLOR;
	private String COUNT;
	private String SEQ;

	public String getFILL() {
		return FILL;
	}

	public String getITR1() {
		return ITR1;
	}

	public String getITR2() {
		return ITR2;
	}

	public String getHEADER() {
		return HEADER;
	}

	public String getTIME() {
		return TIME;
	}

	public String getCRY() {
		return CRY;
	}

	public String getCRYNAME() {
		return CRYNAME;
	}

	public String getDATE() {
		return DATE;
	}

	public String getRECNO() {
		return RECNO;
	}

	public String getCOLOR() {
		return COLOR;
	}

	public String getCOUNT() {
		return COUNT;
	}

	public String getSEQ() {
		return SEQ;
	}

	public void setFILL(String fILL) {
		FILL = fILL;
	}

	public void setITR1(String iTR1) {
		ITR1 = iTR1;
	}

	public void setITR2(String iTR2) {
		ITR2 = iTR2;
	}

	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}

	public void setTIME(String tIME) {
		TIME = tIME;
	}

	public void setCRY(String cRY) {
		CRY = cRY;
	}

	public void setCRYNAME(String cRYNAME) {
		CRYNAME = cRYNAME;
	}

	public void setDATE(String dATE) {
		DATE = dATE;
	}

	public void setRECNO(String rECNO) {
		RECNO = rECNO;
	}

	public void setCOLOR(String cOLOR) {
		COLOR = cOLOR;
	}

	public void setCOUNT(String cOUNT) {
		COUNT = cOUNT;
	}

	public void setSEQ(String sEQ) {
		SEQ = sEQ;
	}

}
