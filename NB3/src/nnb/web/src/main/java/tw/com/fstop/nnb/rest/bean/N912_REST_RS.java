package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N912_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 42957618990505153L;

	private String OFFSET;		// 空白
	private String HEADER;		// HEADER
	private String MSGCOD;		// 訊息代碼
	private String DATAPL;		// 申請日期
	private String TRNCNT;		// 業務權限筆數
	private String TRNATR;		// 業務權限
	private String APCNT;		// 約定功能筆數
	private String APATR;		// 約定功能
	private String CHGYON;		// 更新與否
	private String AREA;		// 地區
	private String IDNCOD;		// 模式
	private String TWSTA;		// TWSTA
	private String HKSTA;		// HKSTA
	private String OPCODE;		// 企業網路系統使用方式
	private String CHIYON;		// 子公司授權供應商是否更新
	
	
	public String getOFFSET() {
		return OFFSET;
	}
	public void setOFFSET(String oFFSET) {
		OFFSET = oFFSET;
	}
	public String getHEADER() {
		return HEADER;
	}
	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}
	public String getMSGCOD() {
		return MSGCOD;
	}
	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}
	public String getDATAPL() {
		return DATAPL;
	}
	public void setDATAPL(String dATAPL) {
		DATAPL = dATAPL;
	}
	public String getTRNCNT() {
		return TRNCNT;
	}
	public void setTRNCNT(String tRNCNT) {
		TRNCNT = tRNCNT;
	}
	public String getTRNATR() {
		return TRNATR;
	}
	public void setTRNATR(String tRNATR) {
		TRNATR = tRNATR;
	}
	public String getAPCNT() {
		return APCNT;
	}
	public void setAPCNT(String aPCNT) {
		APCNT = aPCNT;
	}
	public String getAPATR() {
		return APATR;
	}
	public void setAPATR(String aPATR) {
		APATR = aPATR;
	}
	public String getCHGYON() {
		return CHGYON;
	}
	public void setCHGYON(String cHGYON) {
		CHGYON = cHGYON;
	}
	public String getAREA() {
		return AREA;
	}
	public void setAREA(String aREA) {
		AREA = aREA;
	}
	public String getIDNCOD() {
		return IDNCOD;
	}
	public void setIDNCOD(String iDNCOD) {
		IDNCOD = iDNCOD;
	}
	public String getTWSTA() {
		return TWSTA;
	}
	public void setTWSTA(String tWSTA) {
		TWSTA = tWSTA;
	}
	public String getHKSTA() {
		return HKSTA;
	}
	public void setHKSTA(String hKSTA) {
		HKSTA = hKSTA;
	}
	public String getOPCODE() {
		return OPCODE;
	}
	public void setOPCODE(String oPCODE) {
		OPCODE = oPCODE;
	}
	public String getCHIYON() {
		return CHIYON;
	}
	public void setCHIYON(String cHIYON) {
		CHIYON = cHIYON;
	}
	
}