package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.Map;

/**
 * 註冊客戶綁定
 * 
 * @author Vincenthuang
 *
 */
public class MtpUser_REST_RQ extends BaseRestBean_QR implements Serializable {

	//=========其餘參數===========
	private String ADOPID = "MtpUser";	//紀錄TXNLOG用
	private String FGTXWAY;				//交易類別，讓ms_qr判斷是否要驗證
	
	
	//=========API參數===========
	private String mobilephone;			//手機門號
	private String txacn;				//綁定轉入帳號
	private String binddefault;			//是否綁定預設
	private String usermail;

	//=========IDGATE參數===========
	private String txnID;
	private String idgateID;
	private String sessionID;
	private Map IDGATEDATA;
	
	
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public Map getIDGATEDATA() {
		return IDGATEDATA;
	}
	public void setIDGATEDATA(Map iDGATEDATA) {
		IDGATEDATA = iDGATEDATA;
	}
	public String getTxnID() {
		return txnID;
	}
	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}
	public String getIdgateID() {
		return idgateID;
	}
	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}
	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	public String getMobilephone() {
		return mobilephone;
	}
	public void setMobilephone(String mobilephone) {
		this.mobilephone = mobilephone;
	}
	public String getTxacn() {
		return txacn;
	}
	public void setTxacn(String txacn) {
		this.txacn = txacn;
	}
	public String getBinddefault() {
		return binddefault;
	}
	public void setBinddefault(String binddefault) {
		this.binddefault = binddefault;
	}
	public String getUsermail() {
		return usermail;
	}
	public void setUsermail(String usermail) {
		this.usermail = usermail;
	}
	
	
	
}
