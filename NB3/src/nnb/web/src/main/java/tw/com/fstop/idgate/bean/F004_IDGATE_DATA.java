package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class F004_IDGATE_DATA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -762951082705400880L;

	@SerializedName(value = "TXTYPE")
	private String TXTYPE;
	@SerializedName(value = "CUSIDN")
	private String CUSIDN;

	public String getTXTYPE() {
		return TXTYPE;
	}

	public void setTXTYPE(String tXTYPE) {
		TXTYPE = tXTYPE;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	
}
