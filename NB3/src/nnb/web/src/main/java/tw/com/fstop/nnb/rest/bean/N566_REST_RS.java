package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N566_REST_RS extends BaseRestBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7120138072591703920L;
	private String  LENGTH  ;
	private String  CMRECNUM;
	private String  ABEND   ;
	private String  REC_NO  ;
	private String  ACN     ;
	private String  CMPERIOD;
	private LinkedList<N566_REST_RSDATA> REC;
	private String  CMQTIME ;//查詢時間
    private String  __OCCURS;
    private String  USERDATA;
	
	public String getLENGTH() {
		return LENGTH;
	}
	public void setLENGTH(String lENGTH) {
		LENGTH = lENGTH;
	}
	public String getCMRECNUM() {
		return CMRECNUM;
	}
	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}
	public String getABEND() {
		return ABEND;
	}
	public void setABEND(String aBEND) {
		ABEND = aBEND;
	}
	public String getREC_NO() {
		return REC_NO;
	}
	public void setREC_NO(String rEC_NO) {
		REC_NO = rEC_NO;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getCMPERIOD() {
		return CMPERIOD;
	}
	public void setCMPERIOD(String cMPERIOD) {
		CMPERIOD = cMPERIOD;
	}
	public LinkedList<N566_REST_RSDATA> getREC() {
		return REC;
	}

	public void setREC(LinkedList<N566_REST_RSDATA> rEC) {
		REC = rEC;
	}
	public String getUSERDATA() {
		return USERDATA;
	}
	public void setUSERDATA(String uSERDATA) {
		USERDATA = uSERDATA;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String get__OCCURS() {
		return __OCCURS;
	}
	public void set__OCCURS(String __OCCURS) {
		this.__OCCURS = __OCCURS;
	}

	

}
