package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.text.DecimalFormat;

import com.google.gson.annotations.SerializedName;

public class N078_IDGATE_DATA implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -7563461897718788937L;
	@SerializedName(value = "FDPNUM")
	private String FDPNUM;
	@SerializedName(value = "INTMTH")
	private String INTMTH;
	@SerializedName(value = "DUEDAT")
	private String DUEDAT;
	@SerializedName(value = "FGSVTYPE")
	private String FGSVTYPE;
	@SerializedName(value = "DPSVACNO")
	private String DPSVACNO;
	@SerializedName(value = "AMTFDP")
	private String AMTFDP;
	public String getFDPNUM() {
		return FDPNUM;
	}
	public void setFDPNUM(String fDPNUM) {
		FDPNUM = fDPNUM;
	}
	public String getINTMTH() {
		return INTMTH;
	}
	public void setINTMTH(String iNTMTH) {
		INTMTH = iNTMTH;
	}
	public String getDUEDAT() {
		return DUEDAT;
	}
	public void setDUEDAT(String dUEDAT) {
		DUEDAT = dUEDAT;
	}

	public String getFGSVTYPE() {
		return FGSVTYPE;
	}
	public void setFGSVTYPE(String fGSVTYPE) {
		FGSVTYPE = fGSVTYPE;
	}
	public String getDPSVACNO() {
		return DPSVACNO;
	}
	public void setDPSVACNO(String dPSVACNO) {
		DPSVACNO = dPSVACNO;
	}
	public String getAMTFDP() {
		return AMTFDP;
	}
	public void setAMTFDP(String aMTFDP) {
		AMTFDP = aMTFDP;
	}

	
	
}
