package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;


public class F004_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5370773529503299761L;
	
	@SerializedName(value = "TXTYPE")
	private String TXTYPE;
	
	@Override
	public Map<String, String> coverMap(){
		Map<String, String> result = new LinkedHashMap<String, String>();
		String txtype = "";
		if("A".equals(this.TXTYPE))
			txtype = "申請匯入匯款線上解款";
		else
			txtype = "取消匯入匯款線上解款";
		result.put("交易名稱","外匯匯入匯款線上解款申請/註銷");
		result.put("申請項目",txtype);
		return result;
	}
}
