package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * C115電文RSDATA
 */
public class C115_REST_RSDATA implements Serializable{

	private static final long serialVersionUID = 260600753199114741L;
	
	
	private String PAYDAY1 ;
	private String PAYDAY2 ;
	private String PAYDAY3 ;
	private String PAYDAY4 ;
	private String PAYDAY5 ;
	private String PAYDAY6 ;
	private String PAYDAY7 ;
	private String PAYDAY8;
	private String PAYDAY9;
	
	private String REMARK1;
	private String REMARK2;
	private String CONDAY;
	private String PAYACNO;
	private String PAYAMT;
	private String DBTTYPE ;
	private String STPDAY;
	private String PAYTAG;
	private String MIP;
	private String CDNO;
	private String PAYCUR;
	private String FSTDAY;
	private String STOPBEGDAY;
	private String STOPENDDAY;
	private String SALFLG; //薪轉戶註記
	
	public String getPAYDAY1() {
		return PAYDAY1;
	}
	public void setPAYDAY1(String pAYDAY1) {
		PAYDAY1 = pAYDAY1;
	}
	public String getPAYDAY2() {
		return PAYDAY2;
	}
	public void setPAYDAY2(String pAYDAY2) {
		PAYDAY2 = pAYDAY2;
	}
	public String getPAYDAY3() {
		return PAYDAY3;
	}
	public void setPAYDAY3(String pAYDAY3) {
		PAYDAY3 = pAYDAY3;
	}
	public String getPAYDAY4() {
		return PAYDAY4;
	}
	public void setPAYDAY4(String pAYDAY4) {
		PAYDAY4 = pAYDAY4;
	}
	public String getPAYDAY5() {
		return PAYDAY5;
	}
	public void setPAYDAY5(String pAYDAY5) {
		PAYDAY5 = pAYDAY5;
	}
	public String getPAYDAY6() {
		return PAYDAY6;
	}
	public void setPAYDAY6(String pAYDAY6) {
		PAYDAY6 = pAYDAY6;
	}
	public String getPAYDAY7() {
		return PAYDAY7;
	}
	public void setPAYDAY7(String pAYDAY7) {
		PAYDAY7 = pAYDAY7;
	}
	public String getPAYDAY8() {
		return PAYDAY8;
	}
	public void setPAYDAY8(String pAYDAY8) {
		PAYDAY8 = pAYDAY8;
	}
	public String getPAYDAY9() {
		return PAYDAY9;
	}
	public void setPAYDAY9(String pAYDAY9) {
		PAYDAY9 = pAYDAY9;
	}
	public String getREMARK1() {
		return REMARK1;
	}
	public void setREMARK1(String rEMARK1) {
		REMARK1 = rEMARK1;
	}
	public String getREMARK2() {
		return REMARK2;
	}
	public void setREMARK2(String rEMARK2) {
		REMARK2 = rEMARK2;
	}
	public String getCONDAY() {
		return CONDAY;
	}
	public void setCONDAY(String cONDAY) {
		CONDAY = cONDAY;
	}
	public String getPAYACNO() {
		return PAYACNO;
	}
	public void setPAYACNO(String pAYACNO) {
		PAYACNO = pAYACNO;
	}
	public String getPAYAMT() {
		return PAYAMT;
	}
	public void setPAYAMT(String pAYAMT) {
		PAYAMT = pAYAMT;
	}
	public String getDBTTYPE() {
		return DBTTYPE;
	}
	public void setDBTTYPE(String dBTTYPE) {
		DBTTYPE = dBTTYPE;
	}
	public String getSTPDAY() {
		return STPDAY;
	}
	public void setSTPDAY(String sTPDAY) {
		STPDAY = sTPDAY;
	}
	public String getPAYTAG() {
		return PAYTAG;
	}
	public void setPAYTAG(String pAYTAG) {
		PAYTAG = pAYTAG;
	}
	public String getMIP() {
		return MIP;
	}
	public void setMIP(String mIP) {
		MIP = mIP;
	}
	public String getPAYCUR() {
		return PAYCUR;
	}
	public void setPAYCUR(String pAYCUR) {
		PAYCUR = pAYCUR;
	}
	public String getFSTDAY() {
		return FSTDAY;
	}
	public void setFSTDAY(String fSTDAY) {
		FSTDAY = fSTDAY;
	}
	public String getSTOPBEGDAY() {
		return STOPBEGDAY;
	}
	public void setSTOPBEGDAY(String sTOPBEGDAY) {
		STOPBEGDAY = sTOPBEGDAY;
	}
	public String getSTOPENDDAY() {
		return STOPENDDAY;
	}
	public void setSTOPENDDAY(String sTOPENDDAY) {
		STOPENDDAY = sTOPENDDAY;
	}
	public String getCDNO() {
		return CDNO;
	}
	public void setCDNO(String cDNO) {
		CDNO = cDNO;
	}
	public String getSALFLG() {
		return SALFLG;
	}
	public void setSALFLG(String sALFLG) {
		SALFLG = sALFLG;
	}
	
}