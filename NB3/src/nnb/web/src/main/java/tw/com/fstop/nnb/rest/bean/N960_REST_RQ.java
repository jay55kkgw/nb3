package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N960_REST_RQ  extends BaseRestBean_GOLD implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1415660895796586875L;
	
	String CUSIDN;
	String NCHECKNB;	// 不檢查網銀身份

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getNCHECKNB() {
		return NCHECKNB;
	}

	public void setNCHECKNB(String nCHECKNB) {
		NCHECKNB = nCHECKNB;
	}
	
}
