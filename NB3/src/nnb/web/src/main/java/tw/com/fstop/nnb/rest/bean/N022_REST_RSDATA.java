package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N022_REST_RSDATA extends BaseRestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2293599576836188111L;
	String ITRX; 
    String CRY;
    String CRYNAME;
    String COLOR;
	public String getITRX() {
		return ITRX;
	}
	public void setITRX(String iTRX) {
		ITRX = iTRX;
	}
	public String getCRY() {
		return CRY;
	}
	public void setCRY(String cRY) {
		CRY = cRY;
	}
	public String getCRYNAME() {
		return CRYNAME;
	}
	public void setCRYNAME(String cRYNAME) {
		CRYNAME = cRYNAME;
	}
	public String getCOLOR() {
		return COLOR;
	}
	public void setCOLOR(String cOLOR) {
		COLOR = cOLOR;
	}

}
