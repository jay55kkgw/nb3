package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * C022電文RS
 */
public class C022_Public_REST_RS extends BaseRestBean implements Serializable{
	private static final long serialVersionUID = 6791228050721094802L;
	
	private String DATE;
	private String TIME;
	private String CUSIDN;
	private String CUSNAME;
	private String TRANSCODE;
	private String CDNO;
	private String TRADEDATE;
	private String INTRANSCODE;
	private String UNIT;
	private String FCA1;
	private String AMT3;
	private String FCA2;
	private String AMT5;
	private String HTELPHONE;
	private String OTELPHONE;
	private String BILLSENDMODE;
	private String SSLTXNO;
	private String FUNDAMT;
	private String FUNCUR;
	private String SHORTTRADE;
	private String SHORTTUNIT;
	private String LINK_FUND_HOUSE;
	private String LINK_FUND_CODE;
	private String LINK_FEE_01;
	private String LINK_FEE_02;
	private String LINK_FEE_03;
	private String LINK_FEE_04;
	private String LINK_FEE_05;
	private String LINK_FEE_07;
	private String LINK_SLS_01;
	private String LINK_SLS_02;
	private String LINK_SLS_03;
	private String LINK_SLS_04;
	private String LINK_SLS_05;
	private String LINK_SLS_06;
	private String LINK_SLS_07;
	private String LINK_TRN_01;
	private String LINK_TRN_02;
	private String LINK_OTHER_04;
	private String LINK_OTHER_05;
	private String LINK_SLS_08;
	private String LINK_BASE_01;
	private String LINK_RESULT_01;
	private String LINK_RESULT_02;
	private String LINK_RESULT_03;
	private String LINK_RESULT_04;
	private String LINK_RESULT_05;
	private String LINK_RESULT_06;
	private String LINK_FEE_BSHARE;
	private String LINK_FUND_TYPE;
	private String LINK_FH_NAME;
	private String LINK_FUND_NAME;
	private String LINK_DATE;
	private String SHWD;//視窗註記 2020/07/02新增
	private String XFLAG;
		
	public String getDATE(){
		return DATE;
	}
	public void setDATE(String dATE){
		DATE = dATE;
	}
	public String getTIME(){
		return TIME;
	}
	public void setTIME(String tIME){
		TIME = tIME;
	}
	public String getCUSIDN(){
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN){
		CUSIDN = cUSIDN;
	}
	public String getCUSNAME(){
		return CUSNAME;
	}
	public void setCUSNAME(String cUSNAME){
		CUSNAME = cUSNAME;
	}
	public String getTRANSCODE(){
		return TRANSCODE;
	}
	public void setTRANSCODE(String tRANSCODE){
		TRANSCODE = tRANSCODE;
	}
	public String getTRADEDATE(){
		return TRADEDATE;
	}
	public void setTRADEDATE(String tRADEDATE){
		TRADEDATE = tRADEDATE;
	}
	public String getINTRANSCODE(){
		return INTRANSCODE;
	}
	public void setINTRANSCODE(String iNTRANSCODE){
		INTRANSCODE = iNTRANSCODE;
	}
	public String getUNIT(){
		return UNIT;
	}
	public void setUNIT(String uNIT){
		UNIT = uNIT;
	}
	public String getFCA1(){
		return FCA1;
	}
	public void setFCA1(String fCA1){
		FCA1 = fCA1;
	}
	public String getAMT3(){
		return AMT3;
	}
	public void setAMT3(String aMT3){
		AMT3 = aMT3;
	}
	public String getFCA2(){
		return FCA2;
	}
	public void setFCA2(String fCA2){
		FCA2 = fCA2;
	}
	public String getAMT5(){
		return AMT5;
	}
	public void setAMT5(String aMT5){
		AMT5 = aMT5;
	}
	public String getHTELPHONE(){
		return HTELPHONE;
	}
	public void setHTELPHONE(String hTELPHONE){
		HTELPHONE = hTELPHONE;
	}
	public String getOTELPHONE(){
		return OTELPHONE;
	}
	public void setOTELPHONE(String oTELPHONE){
		OTELPHONE = oTELPHONE;
	}
	public String getBILLSENDMODE(){
		return BILLSENDMODE;
	}
	public void setBILLSENDMODE(String bILLSENDMODE){
		BILLSENDMODE = bILLSENDMODE;
	}
	public String getSSLTXNO(){
		return SSLTXNO;
	}
	public void setSSLTXNO(String sSLTXNO){
		SSLTXNO = sSLTXNO;
	}
	public String getFUNDAMT(){
		return FUNDAMT;
	}
	public void setFUNDAMT(String fUNDAMT){
		FUNDAMT = fUNDAMT;
	}
	public String getFUNCUR(){
		return FUNCUR;
	}
	public void setFUNCUR(String fUNCUR){
		FUNCUR = fUNCUR;
	}
	public String getSHORTTRADE(){
		return SHORTTRADE;
	}
	public void setSHORTTRADE(String sHORTTRADE){
		SHORTTRADE = sHORTTRADE;
	}
	public String getSHORTTUNIT(){
		return SHORTTUNIT;
	}
	public void setSHORTTUNIT(String sHORTTUNIT){
		SHORTTUNIT = sHORTTUNIT;
	}
	public String getLINK_FUND_HOUSE(){
		return LINK_FUND_HOUSE;
	}
	public void setLINK_FUND_HOUSE(String lINK_FUND_HOUSE){
		LINK_FUND_HOUSE = lINK_FUND_HOUSE;
	}
	public String getLINK_FUND_CODE(){
		return LINK_FUND_CODE;
	}
	public void setLINK_FUND_CODE(String lINK_FUND_CODE){
		LINK_FUND_CODE = lINK_FUND_CODE;
	}
	public String getLINK_FEE_01(){
		return LINK_FEE_01;
	}
	public void setLINK_FEE_01(String lINK_FEE_01){
		LINK_FEE_01 = lINK_FEE_01;
	}
	public String getLINK_FEE_02(){
		return LINK_FEE_02;
	}
	public void setLINK_FEE_02(String lINK_FEE_02){
		LINK_FEE_02 = lINK_FEE_02;
	}
	public String getLINK_FEE_03(){
		return LINK_FEE_03;
	}
	public void setLINK_FEE_03(String lINK_FEE_03){
		LINK_FEE_03 = lINK_FEE_03;
	}
	public String getLINK_FEE_04(){
		return LINK_FEE_04;
	}
	public void setLINK_FEE_04(String lINK_FEE_04){
		LINK_FEE_04 = lINK_FEE_04;
	}
	public String getLINK_FEE_05(){
		return LINK_FEE_05;
	}
	public void setLINK_FEE_05(String lINK_FEE_05){
		LINK_FEE_05 = lINK_FEE_05;
	}
	public String getLINK_FEE_07(){
		return LINK_FEE_07;
	}
	public void setLINK_FEE_07(String lINK_FEE_07){
		LINK_FEE_07 = lINK_FEE_07;
	}
	public String getLINK_SLS_01(){
		return LINK_SLS_01;
	}
	public void setLINK_SLS_01(String lINK_SLS_01){
		LINK_SLS_01 = lINK_SLS_01;
	}
	public String getLINK_SLS_02(){
		return LINK_SLS_02;
	}
	public void setLINK_SLS_02(String lINK_SLS_02){
		LINK_SLS_02 = lINK_SLS_02;
	}
	public String getLINK_SLS_03(){
		return LINK_SLS_03;
	}
	public void setLINK_SLS_03(String lINK_SLS_03){
		LINK_SLS_03 = lINK_SLS_03;
	}
	public String getLINK_SLS_04(){
		return LINK_SLS_04;
	}
	public void setLINK_SLS_04(String lINK_SLS_04){
		LINK_SLS_04 = lINK_SLS_04;
	}
	public String getLINK_SLS_05(){
		return LINK_SLS_05;
	}
	public void setLINK_SLS_05(String lINK_SLS_05){
		LINK_SLS_05 = lINK_SLS_05;
	}
	public String getLINK_SLS_06(){
		return LINK_SLS_06;
	}
	public void setLINK_SLS_06(String lINK_SLS_06){
		LINK_SLS_06 = lINK_SLS_06;
	}
	public String getLINK_SLS_07(){
		return LINK_SLS_07;
	}
	public void setLINK_SLS_07(String lINK_SLS_07){
		LINK_SLS_07 = lINK_SLS_07;
	}
	public String getLINK_TRN_01(){
		return LINK_TRN_01;
	}
	public void setLINK_TRN_01(String lINK_TRN_01){
		LINK_TRN_01 = lINK_TRN_01;
	}
	public String getLINK_TRN_02(){
		return LINK_TRN_02;
	}
	public void setLINK_TRN_02(String lINK_TRN_02){
		LINK_TRN_02 = lINK_TRN_02;
	}
	public String getLINK_OTHER_04(){
		return LINK_OTHER_04;
	}
	public void setLINK_OTHER_04(String lINK_OTHER_04){
		LINK_OTHER_04 = lINK_OTHER_04;
	}
	public String getLINK_OTHER_05(){
		return LINK_OTHER_05;
	}
	public void setLINK_OTHER_05(String lINK_OTHER_05){
		LINK_OTHER_05 = lINK_OTHER_05;
	}
	public String getLINK_SLS_08(){
		return LINK_SLS_08;
	}
	public void setLINK_SLS_08(String lINK_SLS_08){
		LINK_SLS_08 = lINK_SLS_08;
	}
	public String getLINK_BASE_01(){
		return LINK_BASE_01;
	}
	public void setLINK_BASE_01(String lINK_BASE_01){
		LINK_BASE_01 = lINK_BASE_01;
	}
	public String getLINK_RESULT_01(){
		return LINK_RESULT_01;
	}
	public void setLINK_RESULT_01(String lINK_RESULT_01){
		LINK_RESULT_01 = lINK_RESULT_01;
	}
	public String getLINK_RESULT_02(){
		return LINK_RESULT_02;
	}
	public void setLINK_RESULT_02(String lINK_RESULT_02){
		LINK_RESULT_02 = lINK_RESULT_02;
	}
	public String getLINK_RESULT_03(){
		return LINK_RESULT_03;
	}
	public void setLINK_RESULT_03(String lINK_RESULT_03){
		LINK_RESULT_03 = lINK_RESULT_03;
	}
	public String getLINK_RESULT_04(){
		return LINK_RESULT_04;
	}
	public void setLINK_RESULT_04(String lINK_RESULT_04){
		LINK_RESULT_04 = lINK_RESULT_04;
	}
	public String getLINK_RESULT_05(){
		return LINK_RESULT_05;
	}
	public void setLINK_RESULT_05(String lINK_RESULT_05){
		LINK_RESULT_05 = lINK_RESULT_05;
	}
	public String getLINK_RESULT_06(){
		return LINK_RESULT_06;
	}
	public void setLINK_RESULT_06(String lINK_RESULT_06){
		LINK_RESULT_06 = lINK_RESULT_06;
	}
	public String getLINK_FEE_BSHARE(){
		return LINK_FEE_BSHARE;
	}
	public void setLINK_FEE_BSHARE(String lINK_FEE_BSHARE){
		LINK_FEE_BSHARE = lINK_FEE_BSHARE;
	}
	public String getLINK_FUND_TYPE(){
		return LINK_FUND_TYPE;
	}
	public void setLINK_FUND_TYPE(String lINK_FUND_TYPE){
		LINK_FUND_TYPE = lINK_FUND_TYPE;
	}
	public String getLINK_FH_NAME(){
		return LINK_FH_NAME;
	}
	public void setLINK_FH_NAME(String lINK_FH_NAME){
		LINK_FH_NAME = lINK_FH_NAME;
	}
	public String getLINK_FUND_NAME(){
		return LINK_FUND_NAME;
	}
	public void setLINK_FUND_NAME(String lINK_FUND_NAME){
		LINK_FUND_NAME = lINK_FUND_NAME;
	}
	public String getLINK_DATE(){
		return LINK_DATE;
	}
	public void setLINK_DATE(String lINK_DATE){
		LINK_DATE = lINK_DATE;
	}
	public String getCDNO() {
		return CDNO;
	}
	public void setCDNO(String cDNO) {
		CDNO = cDNO;
	}
	public String getSHWD() {
		return SHWD;
	}
	public void setSHWD(String sHWD) {
		SHWD = sHWD;
	}
	public String getXFLAG() {
		return XFLAG;
	}
	public void setXFLAG(String xFLAG) {
		XFLAG = xFLAG;
	}
}