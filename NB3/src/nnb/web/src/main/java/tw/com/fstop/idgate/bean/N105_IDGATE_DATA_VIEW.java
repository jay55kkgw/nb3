package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;


public class N105_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4135394421263918791L;

	@SerializedName(value = "CUSIDN")
	private String CUSIDN;
	
	@Override
	public Map<String, String> coverMap(){
		LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", "房屋擔保借款繳息清單");
		result.put("交易類型", "查詢及列印");
		return result;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
}
