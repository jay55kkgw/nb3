package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N104_1_REST_RS  extends BaseRestBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1103319908354445073L;

	private String CUSIDN;//統一編號
	private String NAME;//戶名
	private String BIRTHDAY;//出生日期 ( 民國 )
	private String POSTCOD1;//戶籍或登記郵遞區號
	private String PMTADR;//戶籍地址或登記地址
	private String POSTCOD2;//通訊地郵遞區號
	private String CTTADR;//通訊地址
	private String HOMEZIP;//電話區域碼
	private String HOMETEL;//電話號碼（住 )
	private String BUSZIP;//電話區域碼
	private String BUSTEL;//電話號碼 ( 公 )
	private String CELPHONE;//行動電話
	private String ARACOD3;//傳真區域碼
	private String FAX;//傳真號碼
	private String MAILADDR;//電子郵件地址
	private String DEGREE;//學歷
	private String SALARY;//年收入仟元
	private String MARRY;//婚姻狀況
	private String CHILD;//子女數
	private String CAREER1;//職業（聯徵）
	private String CAREER2;//職稱
	private String MAINFUND;//主要資金/財產來源
	private String SAMT;
	private String RENAME;
	
	private String ABEND;
	private String FGTXWAY;
	private String PURPOSE;
	private String PREASON;
	private String BRHCOD;
	private String SMONEY;
	private String UID;
	private String EMPLOYER;
	private String BREASON;
	private String BDEALING;
	private String CUSNAME;//原住民姓名
	
	public String getCUSNAME() {
		return CUSNAME;
	}
	public void setCUSNAME(String cUSNAME) {
		CUSNAME = cUSNAME;
	}
	public String getPURPOSE() {
		return PURPOSE;
	}
	public void setPURPOSE(String pURPOSE) {
		PURPOSE = pURPOSE;
	}
	public String getPREASON() {
		return PREASON;
	}
	public void setPREASON(String pREASON) {
		PREASON = pREASON;
	}
	public String getBRHCOD() {
		return BRHCOD;
	}
	public void setBRHCOD(String bRHCOD) {
		BRHCOD = bRHCOD;
	}
	public String getSMONEY() {
		return SMONEY;
	}
	public void setSMONEY(String sMONEY) {
		SMONEY = sMONEY;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getEMPLOYER() {
		return EMPLOYER;
	}
	public void setEMPLOYER(String eMPLOYER) {
		EMPLOYER = eMPLOYER;
	}
	public String getBREASON() {
		return BREASON;
	}
	public void setBREASON(String bREASON) {
		BREASON = bREASON;
	}
	public String getBDEALING() {
		return BDEALING;
	}
	public void setBDEALING(String bDEALING) {
		BDEALING = bDEALING;
	}
	public String getABEND() {
		return ABEND;
	}
	public void setABEND(String aBEND) {
		ABEND = aBEND;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getBIRTHDAY() {
		return BIRTHDAY;
	}
	public void setBIRTHDAY(String bIRTHDAY) {
		BIRTHDAY = bIRTHDAY;
	}
	public String getPOSTCOD1() {
		return POSTCOD1;
	}
	public void setPOSTCOD1(String pOSTCOD1) {
		POSTCOD1 = pOSTCOD1;
	}
	public String getPMTADR() {
		return PMTADR;
	}
	public void setPMTADR(String pMTADR) {
		PMTADR = pMTADR;
	}
	public String getPOSTCOD2() {
		return POSTCOD2;
	}
	public void setPOSTCOD2(String pOSTCOD2) {
		POSTCOD2 = pOSTCOD2;
	}
	public String getCTTADR() {
		return CTTADR;
	}
	public void setCTTADR(String cTTADR) {
		CTTADR = cTTADR;
	}
	public String getHOMEZIP() {
		return HOMEZIP;
	}
	public void setHOMEZIP(String hOMEZIP) {
		HOMEZIP = hOMEZIP;
	}
	public String getHOMETEL() {
		return HOMETEL;
	}
	public void setHOMETEL(String hOMETEL) {
		HOMETEL = hOMETEL;
	}
	public String getBUSZIP() {
		return BUSZIP;
	}
	public void setBUSZIP(String bUSZIP) {
		BUSZIP = bUSZIP;
	}
	public String getBUSTEL() {
		return BUSTEL;
	}
	public void setBUSTEL(String bUSTEL) {
		BUSTEL = bUSTEL;
	}
	public String getCELPHONE() {
		return CELPHONE;
	}
	public void setCELPHONE(String cELPHONE) {
		CELPHONE = cELPHONE;
	}
	public String getARACOD3() {
		return ARACOD3;
	}
	public void setARACOD3(String aRACOD3) {
		ARACOD3 = aRACOD3;
	}
	public String getFAX() {
		return FAX;
	}
	public void setFAX(String fAX) {
		FAX = fAX;
	}
	public String getMAILADDR() {
		return MAILADDR;
	}
	public void setMAILADDR(String mAILADDR) {
		MAILADDR = mAILADDR;
	}
	public String getDEGREE() {
		return DEGREE;
	}
	public void setDEGREE(String dEGREE) {
		DEGREE = dEGREE;
	}
	public String getSALARY() {
		return SALARY;
	}
	public void setSALARY(String sALARY) {
		SALARY = sALARY;
	}
	public String getMARRY() {
		return MARRY;
	}
	public void setMARRY(String mARRY) {
		MARRY = mARRY;
	}
	public String getCHILD() {
		return CHILD;
	}
	public void setCHILD(String cHILD) {
		CHILD = cHILD;
	}
	public String getCAREER1() {
		return CAREER1;
	}
	public void setCAREER1(String cAREER1) {
		CAREER1 = cAREER1;
	}
	public String getCAREER2() {
		return CAREER2;
	}
	public void setCAREER2(String cAREER2) {
		CAREER2 = cAREER2;
	}
	public String getMAINFUND() {
		return MAINFUND;
	}
	public void setMAINFUND(String mAINFUND) {
		MAINFUND = mAINFUND;
	}
	public String getSAMT() {
		return SAMT;
	}
	public void setSAMT(String sAMT) {
		SAMT = sAMT;
	}
	public String getRENAME() {
		return RENAME;
	}
	public void setRENAME(String rENAME) {
		RENAME = rENAME;
	}
}
