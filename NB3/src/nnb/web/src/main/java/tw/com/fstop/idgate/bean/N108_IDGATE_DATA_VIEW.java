package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;


public class N108_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5370773529503299761L;
	
	@SerializedName(value = "FDPACN")
	private String FDPACN;

	
	@Override
	public Map<String, String> coverMap(){
		LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
        result.put("交易名稱", "質借功能取消");
        result.put("交易類型", "取消");  
		result.put("帳號", this.FDPACN);
		return result;
	}

	public String getFDPACN() {
		return FDPACN;
	}

	public void setFDPACN(String fDPACN) {
		FDPACN = fDPACN;
	}
}
