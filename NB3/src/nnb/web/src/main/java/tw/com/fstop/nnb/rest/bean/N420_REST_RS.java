package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N420_REST_RS extends BaseRestBean implements Serializable {
	
	private static final long serialVersionUID = 3559155523437424918L;
	String ABEND;
	String USERDATA;
	String CMQTIME;			// 查詢時間
	String CMRECNUM;		// 資料總筆數
	String REC_NO;
	String __OCCURS;
	String USERDATA_X50;
	String DPTAMT;			// 總計金額
	LinkedList<N420_REST_RSDATA> REC;
	
	
	public String getABEND() {
		return ABEND;
	}
	public void setABEND(String aBEND) {
		ABEND = aBEND;
	}
	public String getUSERDATA() {
		return USERDATA;
	}
	public void setUSERDATA(String uSERDATA) {
		USERDATA = uSERDATA;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getCMRECNUM() {
		return CMRECNUM;
	}
	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}
	public String getREC_NO() {
		return REC_NO;
	}
	public void setREC_NO(String rEC_NO) {
		REC_NO = rEC_NO;
	}
	public String get__OCCURS() {
		return __OCCURS;
	}
	public void set__OCCURS(String __OCCURS) {
		this.__OCCURS = __OCCURS;
	}
	public String getUSERDATA_X50() {
		return USERDATA_X50;
	}
	public void setUSERDATA_X50(String uSERDATA_X50) {
		USERDATA_X50 = uSERDATA_X50;
	}
	public LinkedList<N420_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<N420_REST_RSDATA> rEC) {
		REC = rEC;
	}

	

}
