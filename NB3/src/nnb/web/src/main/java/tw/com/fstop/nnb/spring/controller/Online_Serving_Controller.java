package tw.com.fstop.nnb.spring.controller;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.common.ResultCode;
import tw.com.fstop.idgate.bean.F037_IDGATE_DATA;
import tw.com.fstop.idgate.bean.F037_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N880_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N880_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N215A_1_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N215A_1_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N215D_1_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N215D_1_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.NA71_IDGATE_DATA;
import tw.com.fstop.idgate.bean.NA71_IDGATE_DATA_VIEW;
import tw.com.fstop.nnb.custom.annotation.ACNVERIFY;
import tw.com.fstop.nnb.custom.annotation.ACNVERIFY1;
import tw.com.fstop.nnb.custom.annotation.ISTXNLOG;
import tw.com.fstop.nnb.service.Base_Service;
import tw.com.fstop.nnb.service.DaoService;
import tw.com.fstop.nnb.service.IdGateService;
import tw.com.fstop.nnb.service.Online_Serving_Service;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.util.StrUtils;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({ 
	SessionUtil.PD,
	SessionUtil.CUSIDN,
	SessionUtil.HKCODE,
	SessionUtil.MBOPNTM,
	SessionUtil.MBOPNDT,
	SessionUtil.MBSTAT,
	SessionUtil.LOGINTM,
	SessionUtil.LOGINDT,
	SessionUtil.XMLCOD,
	SessionUtil.USERIP,
	SessionUtil.USERID,
	SessionUtil.STAFF,
	SessionUtil.DPUSERNAME,
	SessionUtil.DPMYEMAIL,
	SessionUtil.AUTHORITY,
	SessionUtil.DOWNLOAD_DATALISTMAP_DATA,
	SessionUtil.PRINT_DATALISTMAP_DATA, 
	SessionUtil.CONFIRM_LOCALE_DATA, 
	SessionUtil.RESULT_LOCALE_DATA,
	SessionUtil.TRANSFER_CONFIRM_TOKEN,
	SessionUtil.TRANSFER_RESULT_TOKEN,
	SessionUtil.BACK_DATA,
	SessionUtil.TRANSFER_DATA,
	SessionUtil.CLOSING_TW_ACCOUNT,
	SessionUtil.IDGATE_TRANSDATA})
@Controller
@RequestMapping(value = "/ONLINE/SERVING")
public class Online_Serving_Controller extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());


	@Autowired
	private Online_Serving_Service online_serving_service;
	@Autowired
	private DaoService daoService;
	@Autowired
	I18n i18n;
	@Autowired		 
	IdGateService idgateservice;
	


	//存款餘額輸入頁
	@RequestMapping(value = "/deposit_balance_proof")
	public String deposit_balance_proof(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		BaseResult bs1 = null;
		BaseResult bs2 = null;
		log.trace("deposit_balance_proof>>");
		
//		清除切換語系時暫存的資料
		SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		
		try {
			bs = new BaseResult();
			bs1 = new BaseResult();
			bs2 = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			//2022/03/04 H571110000090 排除公司戶並顯示錯誤訊息
			if  (cusidn.length()==8) {
				String msgCode = "Z999";
				String msgName = i18n.getMsg("LB.B0033");//本服務限個人戶申請
				bs.setMessage(msgCode, msgName);
				bs.setResult(Boolean.FALSE);
				return target;
			}
			
//			bs1 = online_serving_service.getFeeACNO_List(cusidn);
//			bs2 = online_serving_service.getACNO_List(cusidn);
			bs1 = online_serving_service.getAPPLYACNO_List(cusidn); // 申請帳號--20200111修改N920 type: 1234567 Y->N， by Sox
			bs2 = online_serving_service.getFeeACNO_List(cusidn); // 手續費扣帳帳號(只能用台幣)--20200111修改N920 type: 1237 Y， by Sox
			Map<String,Object> callRow1 = (Map) bs1.getData();
			Map<String,Object> callRow2 = (Map) bs2.getData();
			ArrayList<Map<String, String>> REC1 = (ArrayList<Map<String, String>>) callRow1.get("REC");
			ArrayList<Map<String, String>> REC2 = (ArrayList<Map<String, String>>) callRow2.get("REC");
			bs.addData("REC1", REC1);	
			bs.addData("REC2", REC2);	
			bs.addData("CUSIDN", cusidn);
			//用當日前一天
			bs.addData("TODAY", DateUtil.getNext_N_Date("yyyy/MM/dd",-1));
			//new DateTime().plusDays(1).toString("yyyy/MM/dd")
			bs.addData("TOMAROW", DateUtil.getNext_N_Date("yyyy/MM/dd",-1));
			bs.addData("nextTwoYearDay", new DateTime().plusYears(-2).toString("yyyy/MM/dd"));
			String count1 = String.valueOf(REC1.size());
			String count2 = String.valueOf(REC2.size());
			String msgCode = "Z999";
			String msgName = i18n.getMsg("LB.X1780");//查無交易帳號，無法進行此交易，請洽分行申請辦理約定轉出帳號。
			if("".equals(count1) || count1 == null) {
				bs.setMessage(msgCode, msgName);
				bs.setResult(Boolean.FALSE);
			}else if("".equals(count2) || count2 == null) {
				bs.setMessage(msgCode, msgName);
				bs.setResult(Boolean.FALSE);
			}else {
				bs.setResult(Boolean.TRUE);				
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("deposit_balance_proof error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				target = "/online_serving/deposit_balance_proof";
				model.addAttribute("deposit_balance_proof", bs);
			}else {
				model.addAttribute(BaseResult.ERROR, bs);				
			}
		}
		return target;
	}
	
	//存款餘額確認頁
	@ACNVERIFY(acn="ACN")
	@ACNVERIFY1(acn="FEEACN")
	@RequestMapping(value = "/deposit_balance_proof_confirm")
	public String deposit_balance_proof_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("deposit_balance_proof_confirm>>");
		try {
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			bs = new BaseResult();

			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			// 回上一頁重新賦值
			if(okMap.containsKey("previous")) 
			{
				Map m = (Map) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				okMap = m;
			}
			
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				bs = online_serving_service.deposit_balance_proof_confirm(okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			bs.addData("CUSIDN", cusidn);
		} catch (Exception e) {			
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("deposit_balance_proof_confirm error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				target = "/online_serving/deposit_balance_proof_confirm";
				model.addAttribute("deposit_balance_proof_confirm", bs);
				
				Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
				//設定回上一頁的url,配合confirm頁面的js
				model.addAttribute("previous","/ONLINE/SERVING/deposit_balance_proof");
				//回填使用DATA
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
			}else {
				model.addAttribute(BaseResult.ERROR, bs);				
			}
		}		
		return target;
	}
	
	//存款餘額結果頁
	@ACNVERIFY(acn="ACN")
	@ACNVERIFY1(acn="OUTACN")
	@RequestMapping(value = "/deposit_balance_proof_result")
	public String deposit_balance_proof_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("deposit_balance_proof_result>>");
		try {
			bs = new BaseResult();

// 			解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale) 
			{
				log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
			bs.reset();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			log.debug("deposit_balance_proof_result>>{}", cusidn);
			// 解決Trust Boundary Violation
			bs = online_serving_service.deposit_balance_proof_result(cusidn,okMap);
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}	
		} catch (Exception e) {			
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("deposit_balance_proof_result error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				target = "/online_serving/deposit_balance_proof_result";				
				model.addAttribute("deposit_balance_proof_result", bs);

			}else {
				bs.setPrevious("/ONLINE/SERVING/deposit_balance_proof");
				model.addAttribute(BaseResult.ERROR, bs);				
			}
		}			
		return target;
	}
	
	//支票存款開戶申請
	@RequestMapping(value = "/bill_acn_open")
	public String bill_acn_open(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("bill_acn_open>>");
		
//		清除切換語系時暫存的資料
		SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			bs = online_serving_service.getACNO_List(cusidn);
			bs.addData("CUSIDN", cusidn);
			Map<String,Object> callRow = (Map) bs.getData();
			ArrayList<Map<String, String>> REC = (ArrayList<Map<String, String>>) callRow.get("REC");
			
			List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
			// 國別資料
			resultList = online_serving_service.get_country_name();
			bs.addData("COUNTRYNAME", resultList);
			
			bs.addData("CUSIDN", StrUtils.hideaccount(cusidn));
			String count = String.valueOf(REC.size());
			String msgCode = "Z999";
			String msgName = i18n.getMsg("LB.X1780");//查無交易帳號，無法進行此交易，請洽分行申請辦理約定轉出帳號。
			if("".equals(count) || count == null) {
				bs.setMessage(msgCode, msgName);
				bs.setResult(Boolean.FALSE);
			}else {
				bs.setResult(Boolean.TRUE);
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("bill_acn_open error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				target = "/online_serving/bill_acn_open";

				model.addAttribute("bill_acn_open", bs);
			}else {
				model.addAttribute(BaseResult.ERROR, bs);				
			}
		}
		return target;
	}
	
	//支票存款開戶申請結果頁
	@RequestMapping(value = "/bill_acn_open_result")
	public String bill_acn_open_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace(ESAPIUtil.vaildLog("bill_acn_open_result>>{}"+CodeUtil.toJson(reqParam)));
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");

// 			解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale) 
			{
				log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("bill_acn_open_result>>{}", cusidn);
				// 解決Trust Boundary Violation
				bs = online_serving_service.bill_acn_open_result(cusidn,okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			
		} catch (Exception e) {			
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("bill_acn_open_result error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				target = "/online_serving/bill_acn_open_result";				
				model.addAttribute("bill_acn_open_result", bs);
			}else {
				bs.setPrevious("/ONLINE/SERVING/bill_acn_open");
				model.addAttribute(BaseResult.ERROR, bs);				
			}
		}			
		return target;
	}
	
	
	/**
	 * 取得輸入頁下拉選單之銀行代碼列表
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getBankList_aj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody List getInACNO(HttpServletRequest request, HttpServletResponse response, Model model) {
		String str = "getBankList_aj";
		log.trace("getBankList_aj >> {}", str);
		List bankList = null;
		try {
			bankList = daoService.getBankList();
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getInACNO error >> {}",e);
		}
		return bankList;
	}
	
	

	
	//N215約定轉入帳號 選項頁
	@RequestMapping(value = "/predesignated_account")
		public String predesignated_account(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("predesignated_account-->");
		String target = "/online_serving/predesignated_account";
		
		//清除切換語系時暫存的資料
		SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");		
				
		return target;
	}
	
	// 線上約定轉入帳號申請
	@RequestMapping(value = "/predesignated_account_apply")
	public String predesignated_account_apply(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("predesignated_account_apply_Start~~~~~~~");
		String target = "/error";
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			
			// Get session value by session Key
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			// Get put page value
			// 打N932 取得線上約定帳號  要轉成約定轉入帳號 REC是資料  COUNT只是頁面用於判斷
			bs = online_serving_service.predesignated_account_apply(cusidn);
			//IDGATE身分
			String idgateUserFlag = "N";
			Map<String, Object> IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model,
					SessionUtil.IDGATE_TRANSDATA, null);
			try {
				if (IdgateData == null) {
					IdgateData = new HashMap<String, Object>();
				}
				BaseResult tmp = idgateservice.checkIdentity(cusidn);
				if (tmp.getResult()) {
					idgateUserFlag = "Y";
				}
				tmp.addData("idgateUserFlag", idgateUserFlag);
				IdgateData.putAll((Map<String, String>) tmp.getData());
				SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);
			} catch (Exception e) {
				log.debug("idgateUserFlag error {}", e);
			}
			model.addAttribute("idgateUserFlag", idgateUserFlag);
			
//			bs.setResult(true);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("predesignated_account_apply error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				target = "/online_serving/predesignated_account_apply";
				model.addAttribute("predesignated_account_apply", bs);
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}		
		return target;
	}
	
	// 線上約定轉入帳號申請_確認
		@RequestMapping(value = "/predesignated_account_apply_s")
		public String predesignated_account_apply_s(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			log.trace(ESAPIUtil.vaildLog("predesignated_account_apply_s_Start~~~~~~~{}"+CodeUtil.toJson(reqParam)));
			//{"addCount":"","RowData":"00162100025;00162123882;00162222228","INTRACN1":"00162100025","DPBHNO1":"050","INTRACN2":"00162123882","DPACGONAME1":"","INTRACN3":"00162222228","DPACNO1":""}
			String target = "/error";
			BaseResult bs = null;
			try {
				bs = new BaseResult();
				// Get session value by session Key
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
//	 			解決Trust Boundary Violation
				Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

				//修改XSS 攻擊
//				for (Map.Entry<String, String> entry : okMap.entrySet())
//				{
//					okMap.put(entry.getKey(), StringEscapeUtils.escapeHtml(entry.getValue()));
//				}
				
				// IKEY要使用的JSON:DC
				String jsondc = null;
				jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
				log.trace(ESAPIUtil.vaildLog("reservation_confirm.jsondc: {}"+ jsondc));
//				判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
				if(hasLocale) 
				{
					log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
					if( ! bs.getResult())
					{
						log.trace("bs.getResult() >>{}",bs.getResult());
						throw new Exception();
					}
				}
				if( ! hasLocale){
					bs.reset();
					bs = online_serving_service.predesignated_account_apply_s(cusidn,okMap);
					bs.addData("jsondc", jsondc);
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
					
					
					// IDGATE transdata 
					Map<String, Object> objResult = (Map<String, Object>)bs.getData();
					Map<String, String> result = (Map<String, String>)bs.getData();
					String ACNOINFO = online_serving_service.getACNINFO_apply(objResult);
					result.put("CUSIDN", cusidn);
					result.put("ACNINFO", ACNOINFO);
		            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
		    		String adopid = "N215A_1";
		    		String title = "您有一筆約定轉入帳號設定交易待確認";
		        	if(IdgateData != null) {
			            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
			            if(IdgateData.get("idgateUserFlag").equals("Y")) {
			            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
			            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
							IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N215A_1_IDGATE_DATA.class, N215A_1_IDGATE_DATA_VIEW.class, result));
			                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
			            }
		        	}
		            model.addAttribute("idgateAdopid", adopid);
		            model.addAttribute("idgateTitle", title);
					
					
				}
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("predesignated_account_apply_s error >> {}",e);
			}finally {
				if(bs!=null && bs.getResult()) {
					target = "/online_serving/predesignated_account_apply_s";
					model.addAttribute("predesignated_account_apply", bs);
				}else {
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}		
			return target;
		}
		
		// 線上約定轉入帳號申請_end
		@RequestMapping(value = "/predesignated_account_apply_r")
		public String predesignated_account_apply_r(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			log.trace(ESAPIUtil.vaildLog("predesignated_account_apply_r_Start~~~~~~~{}"+CodeUtil.toJson(reqParam)));
			String target = "/error";
			BaseResult bs = null;
			try {
				bs = new BaseResult();
				// Get session value by session Key
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
//	 			解決Trust Boundary Violation
				Map<String, String> okMap = reqParam;

				//修改XSS 攻擊
//				for (Map.Entry<String, String> entry : okMap.entrySet())
//				{
//					okMap.put(entry.getKey(), StringEscapeUtils.escapeHtml(entry.getValue()));
//				}
				
//				判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
					if(hasLocale) 
					{
						log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
						bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
						if( ! bs.getResult())
						{
							log.trace("bs.getResult() >>{}",bs.getResult());
							throw new Exception();
						}
					}
					if( ! hasLocale){
						bs.reset();
						
						 //IDgate驗證		 
		                try {		 
		                    if(okMap.get("FGTXWAY").equals("7")) {		 

		        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
		        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N215A_1_IDGATE");
		                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
		                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
		                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
		                    }		 
		                }catch(Exception e) {		 
		                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
		                }  
						
						bs = online_serving_service.predesignated_account_apply_r(cusidn,okMap);
						SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
					}
				} catch (Exception e) {
					//Avoid Information Exposure Through an Error Message
					//e.printStackTrace();
					log.error("predesignated_account_apply_r error >> {}",e);
				}finally {
					if(bs!=null && bs.getResult()) {
						target = "/online_serving/predesignated_account_apply_r";
						model.addAttribute("predesignated_account_apply", bs);
					}else {
						model.addAttribute(BaseResult.ERROR, bs);
					}
				}		
				return target;
		}
	
	// 線上約定轉入帳號註銷
	@RequestMapping(value = "/predesignated_account_logout")
	public String predesignated_account_logout(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("predesignated_account_logout_Start~~~~~~~");
		String target = "/error";
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			// Get session value by session Key
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));			
			bs = online_serving_service.predesignated_account_logout(cusidn);
			
			//IDGATE身分
			String idgateUserFlag = "N";
			Map<String, Object> IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model,
					SessionUtil.IDGATE_TRANSDATA, null);
			try {
				if (IdgateData == null) {
					IdgateData = new HashMap<String, Object>();
				}
				BaseResult tmp = idgateservice.checkIdentity(cusidn);
				if (tmp.getResult()) {
					idgateUserFlag = "Y";
				}
				tmp.addData("idgateUserFlag", idgateUserFlag);
				IdgateData.putAll((Map<String, String>) tmp.getData());
				SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);
			} catch (Exception e) {
				log.debug("idgateUserFlag error {}", e);
			}
			model.addAttribute("idgateUserFlag", idgateUserFlag);
			
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("predesignated_account_logout error >> {}",e);
			}finally {
				if(bs!=null && bs.getResult()) {
					target = "/online_serving/predesignated_account_logout";
					model.addAttribute("predesignated_account_logout", bs);
				}else {
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}		
			return target;
	}
	
	// 線上約定轉入帳號註銷_確認
	@RequestMapping(value = "/predesignated_account_logout_s")
	public String predesignated_account_logout_s(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace(ESAPIUtil.vaildLog("predesignated_account_logout_s_Start~~~~~~~{}"+CodeUtil.toJson(reqParam)));
		String target = "/error";
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// Get session value by session Key
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
// 			解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

			//修改XSS 攻擊
//			for (Map.Entry<String, String> entry : okMap.entrySet())
//			{
//				okMap.put(entry.getKey(), StringEscapeUtils.escapeHtml(entry.getValue()));
//			}
			
			// IKEY要使用的JSON:DC
			String jsondc = null;
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("reservation_confirm.jsondc: {}"+ jsondc));
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
				if(hasLocale) 
				{
					log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
					if( ! bs.getResult())
					{
						log.trace("bs.getResult() >>{}",bs.getResult());
						throw new Exception();
					}
				}
				if( ! hasLocale){
					bs.reset();
					bs = online_serving_service.predesignated_account_logout_s(cusidn,okMap);
					bs.addData("jsondc", jsondc);
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
					
					// IDGATE transdata 
					Map<String, Object> objResult = (Map<String, Object>)bs.getData();
					Map<String, String> result = (Map<String, String>)bs.getData();
					String ACNOINFO = online_serving_service.getACNINFO_logout(objResult);
					result.put("CUSIDN", cusidn);
					result.put("ACNINFO", ACNOINFO);
		            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
		    		String adopid = "N215D_1";
		    		String title = "您有一筆約定轉入帳號設定交易待確認";
		        	if(IdgateData != null) {
			            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
			            if(IdgateData.get("idgateUserFlag").equals("Y")) {
			            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
			            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
							IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N215D_1_IDGATE_DATA.class, N215D_1_IDGATE_DATA_VIEW.class, result));
			                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
			            }
		        	}
		            model.addAttribute("idgateAdopid", adopid);
		            model.addAttribute("idgateTitle", title);
					
					
				}
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("predesignated_account_logout_s error >> {}",e);
			}finally {
				if(bs!=null && bs.getResult()) {
					target = "/online_serving/predesignated_account_logout_s";
					model.addAttribute("predesignated_account_logout", bs);
				}else {
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}		
			return target;
	}
	
	// 線上約定轉入帳號註銷_end
	@RequestMapping(value = "/predesignated_account_logout_r")
	public String predesignated_account_logout_r(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace(ESAPIUtil.vaildLog("predesignated_account_logout_r_Start~~~~~~~{}"+CodeUtil.toJson(reqParam)));
		String target = "/error";
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// Get session value by session Key
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
// 			解決Trust Boundary Violation
			Map<String, String> okMap = reqParam;

			//修改XSS 攻擊
//			for (Map.Entry<String, String> entry : okMap.entrySet())
//			{
//				okMap.put(entry.getKey(), StringEscapeUtils.escapeHtml(entry.getValue()));
//			}
			
			
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
				if(hasLocale) 
				{
					log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
					if( ! bs.getResult())
					{
						log.trace("bs.getResult() >>{}",bs.getResult());
						throw new Exception();
					}
				}
				if( ! hasLocale){
					bs.reset();
					
					 //IDgate驗證		 
	                try {		 
	                    if(okMap.get("FGTXWAY").equals("7")) {		 

	        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
	        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N215D_1_IDGATE");
	                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
	                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
	                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
	                    }		 
	                }catch(Exception e) {		 
	                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
	                }  
					
					bs = online_serving_service.predesignated_account_logout_r(cusidn,okMap);
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("predesignated_account_logout_r error >> {}",e);
			}finally {
				if(bs!=null && bs.getResult()) {
					target = "/online_serving/predesignated_account_logout_r";
					model.addAttribute("predesignated_account_logout", bs);
				}else {
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}		
			return target;
	}
	
	// 線上約定轉入帳號好記名稱修改
	@RequestMapping(value = "/predesignated_account_modify")
	public String predesignated_account_modify(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("predesignated_account_modify_Start~~~~~~~");
		String target = "/error";
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			// Get session value by session Key
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			// Get put page value
			bs = online_serving_service.predesignated_account_modify(cusidn);
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("predesignated_account_modify error >> {}",e);
			}finally {
				if(bs!=null && bs.getResult()) {
					target = "/online_serving/predesignated_account_modify";
					model.addAttribute("predesignated_account_modify", bs);
				}else {
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}		
			return target;
	}
	
	// 線上約定轉入帳號好記名稱修改_確認
	@RequestMapping(value = "/predesignated_account_modify_s")
	public String predesignated_account_modify_s(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("predesignated_account_modify_s_Start~~~~~~~");
		String target = "/error";
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
// 			解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

			//修改XSS 攻擊
//			for (Map.Entry<String, String> entry : okMap.entrySet())
//			{
//				okMap.put(entry.getKey(), StringEscapeUtils.escapeHtml(entry.getValue()));
//			}
			
			// IKEY要使用的JSON:DC
			String jsondc = null;
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("reservation_confirm.jsondc: {}"+ jsondc));
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
				if(hasLocale) 
				{
					log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
					if( ! bs.getResult())
					{
						log.trace("bs.getResult() >>{}",bs.getResult());
						throw new Exception();
					}
				}
				if( ! hasLocale){
					bs.reset();
					bs = online_serving_service.predesignated_account_modify_s(cusidn,okMap);
					bs.addData("jsondc", jsondc);
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("predesignated_account_modify_s error >> {}",e);
			}finally {
				if(bs!=null && bs.getResult()) {
					target = "/online_serving/predesignated_account_modify_s";
					model.addAttribute("predesignated_account_modify", bs);
				}else {
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}		
			return target;
	}
	
	// 線上約定轉入帳號好記名稱修改_結果
    @ISTXNLOG(value="N215")
	@RequestMapping(value = "/predesignated_account_modify_r")
	public String predesignated_account_modify_r(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("predesignated_account_modify_r_Start~~~~~~~");
		String target = "/error";
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
// 			解決Trust Boundary Violation
//			reqParam.remove("jsondc");
//			reqParam.remove("pkcs7Sign");
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

			//修改XSS 攻擊
//			for (Map.Entry<String, String> entry : okMap.entrySet())
//			{
//				okMap.put(entry.getKey(), StringEscapeUtils.escapeHtml(entry.getValue()));
//			}
			
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
				if(hasLocale) 
				{
					log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
					if( ! bs.getResult())
					{
						log.trace("bs.getResult() >>{}",bs.getResult());
						throw new Exception();
					}
				}
				if( ! hasLocale){
					bs.reset();
					bs = online_serving_service.predesignated_account_modify_r(cusidn,okMap);
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("predesignated_account_modify_r error >> {}",e);
			}finally {
				if(bs!=null && bs.getResult()) {
					target = "/online_serving/predesignated_account_modify_r";
					model.addAttribute("predesignated_account_modify", bs);
				}else {
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}		
			return target;
	}

	/**
	 * N1012_空白票據申請輸入頁
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/blank_bill")
	public String blank_bill(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = new BaseResult();
		try {
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				} else {
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			} else {
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
				bs = online_serving_service.blank_bill(cusidn);
				// 防止重送代碼TxToken
				bs.addData("TXTOKEN", ((Map<String, String>) online_serving_service.getTxToken().getData()).get("TXTOKEN"));
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("blank_bill error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				log.trace("bs >>{}", bs.getData());
				model.addAttribute("blank_bill", bs);
				target = "/personal_serving/blank_bill";
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * N1012_空白票據申請結果頁
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/blank_bill_result")
	public String blank_bill_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = new BaseResult();
		String pageToken = "";
		try {
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				} else {
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			} else {
				// Check TXToken
				String SessionFinshToken = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null);
				pageToken = okMap.get("TXTOKEN");
				BaseResult checkToken = online_serving_service.validateToken(pageToken, SessionFinshToken);
				if (checkToken.getResult())
				{
					log.debug("SessionFinshToken && pagToken 一樣 表示重複交易");
					model.addAttribute(BaseResult.ERROR, checkToken);
					return target;
				}
				else
				{
					// 解決 Trust Boundary Violation
					String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
					bs = online_serving_service.blank_bill_result(cusidn, okMap);
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("blank_bill_result error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				log.trace("bs >>{}", bs.getData());
				// session add TxToken
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, pageToken);
				model.addAttribute("blank_bill_result", bs);
				target = "/personal_serving/blank_bill_result";
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
				//新增回上一頁的路徑
				bs.setPrevious("/ONLINE/SERVING/blank_bill");
			}
		}
		return target;
	}
	
	
	/**
	 * N913 啟用行動銀行服務 功能進入點
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/enable_mobile")
	public String enable_mobile(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		BaseResult bs = null;
		log.trace("enable_mobile start");
		
		
		try 
		{
			// 從session取得使用者行動銀行狀態相關資料
			// 行動銀行啟用狀態
			String currentStatus = (String) SessionUtil.getAttribute(model, SessionUtil.MBSTAT, null);
			// 行動銀行上次啟用/關閉日期
			String modifyDateVal = (String) SessionUtil.getAttribute(model, SessionUtil.MBOPNDT, null);
			// 行動銀行上次啟用/關閉時間
			String modifyTimeVal = (String) SessionUtil.getAttribute(model, SessionUtil.MBOPNTM, null);
			
			if(currentStatus == null || modifyDateVal == null || modifyTimeVal == null)
			{
				throw new NullPointerException("參數是 Null，請檢查登入時取參數的電文");
			}
						
			log.trace("MBSTAT >> {}", currentStatus);
			log.trace("MBOPNDT >> {}", modifyDateVal);
			log.trace("MBOPNTM >> {}", modifyTimeVal);
			
			Map<String, String> session = new HashMap<>();
			session.put("currentStatus", currentStatus);
			session.put("modifyDateVal", modifyDateVal);
			session.put("modifyTimeVal", modifyTimeVal);
			
			bs = online_serving_service.enable_mobile(session);
			Map<String, String> bsData = (Map<String, String>) bs.getData();
			log.trace("{}", bs.getData());
			
			// 顯示在網頁
			model.addAttribute("enable_mobile", bsData);
		}
		catch (Exception e) 
		{
			log.error("enable_mobile error", e);
		}
		finally 
		{
			if (bs != null && bs.getResult()) 
			{
				target = "/online_serving/enable_mobile";
			}
			else 
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	@RequestMapping(value = "/enable_mobile_confirm")
	public String enable_mobile_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		BaseResult bs = new BaseResult();
		log.trace("enable_mobile_confirm start");
		
		try 
		{
			String cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null));
			cusidn = new String(Base64.getDecoder().decode(cusidn));
			Map<String, String> okMap = reqParam;
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				String oriStatus = reqParam.get("OriStatus");
				String mbSwitch = reqParam.get("MB_Switch");
//			bs = online_serving_service.enable_mobile_confirm(oriStatus, "");
				bs.addData("OriStatus", oriStatus);
				bs.addData("MB_Switch", mbSwitch);
				bs.setResult(true);
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
			}
			Map<String, String> bsData = (Map<String, String>) bs.getData();
			log.trace("{}", bs.getData());
			
			// 顯示在網頁
			model.addAttribute("enable_mobile_confirm", bsData);
			
			
		}
		catch (Exception e) 
		{
			log.error("enable_mobile_confirm error", e);
		}
		finally 
		{
			if (bs != null && bs.getResult()) 
			{
				target = "/online_serving/enable_mobile_confirm";
			}
			else 
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	@RequestMapping(value = "/enable_mobile_result")
	public String enable_mobile_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		BaseResult bs = new BaseResult();
		log.trace("enable_mobile_result start");
		
		try 
		{
			String cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null));
			cusidn = new String(Base64.getDecoder().decode(cusidn));
			
			// 解決Trust Boundary Violation
		//	Map<String, String> okMap = reqParam;
			 Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs = online_serving_service.enable_mobile_result(cusidn, reqParam);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			
			Map<String, String> bsData = (Map<String, String>) bs.getData();
			log.trace("bs.getData() >>> {}", bs.getData());
			// 顯示在網頁
			model.addAttribute("enable_mobile_result", bsData);
			SessionUtil.addAttribute(model, SessionUtil.MBOPNDT, bsData.get("MBOPNDT"));
			SessionUtil.addAttribute(model, SessionUtil.MBOPNTM, bsData.get("MBOPNTM"));
		}
		catch (Exception e) 
		{
			log.error("enable_mobile_result error", e);
		}
		finally 
		{
			if (bs != null && bs.getResult()) 
			{
				String CurrentStatus = reqParam.get("MB_Switch");
				SessionUtil.addAttribute(model, SessionUtil.MBSTAT, CurrentStatus);
				target = "/online_serving/enable_mobile_result";
			}
			else 
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * NA80 金融卡線上申請/取消轉帳功能 進入點
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/financial_card_apply")
	public String financial_card_apply(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/online_serving/financial_card_apply";
		online_serving_service.cleanSession(model);
		
		return target;
	}
	
	
	/**
	 * NA80 金融卡線上申請/取消轉帳功能 輸入頁
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/financial_card_apply_select")
	public String financial_card_apply_select(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		log.debug("financial_card_apply start");
		String target = "/error";
		BaseResult bs = new BaseResult();
		
		try 
		{
			online_serving_service.cleanSession(model);
			
			// 從session取得使用者行動銀行狀態相關資料
			String authority = (String) SessionUtil.getAttribute(model, SessionUtil.AUTHORITY, null);
			log.debug("authority >> {}", authority);

			if(authority == null)
			{
				log.error("AUTHORITY 參數是 Null，請檢查登入時的N912電文");
				throw new NullPointerException("AUTHORITY 參數是 Null，請檢查登入時的N912電文");
			}
			
			String flag = "";
			String msgCode = "E241";
			if ("ATM".equals(authority))
			{
				flag = "ATM";
			}
			else if ("ATMT".equals(authority))
			{
				flag = "ATMT";
			}
			else
			{
				// AUTHORITY不是ATM或ATMT時，顯示E241錯誤訊息
				bs = online_serving_service.getMsgName(msgCode);
				throw new Exception();
			}
			
			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			BaseResult token = online_serving_service.getTxToken();
			String txToken = ((Map<String, String>) token.getData()).get("TXTOKEN");
			log.debug("financial_card_apply.TXTOKEN >> {}", txToken);
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN , txToken);
			
			// 頁面按鈕控制
			String applyC = flag.equals("ATM") ? "checked": "";
			String applyD = flag.equals("ATMT") ? "disabled": "";
			String cancleC = flag.equals("ATMT") ? "checked": "";
			String cancleD = flag.equals("ATM") ? "disabled": "";
			Map<String, String> bsData = (Map<String, String>) bs.getData();
			bs.addData("applyC", applyC);
			bs.addData("applyD", applyD);
			bs.addData("cancleC", cancleC);
			bs.addData("cancleD", cancleD);
			bs.setResult(true);
			bs.setMessage("0", ResultCode.SYS_SUCCESS);
			log.debug("bs data >> {}", bs.getData());
			
			// 顯示在網頁
			model.addAttribute("financial_card_apply", bs);
		}
		catch (Exception e) 
		{
			log.error("financial_card_apply error", e);
		}
		finally 
		{
			if (bs != null && bs.getResult()) 
			{
				target = "/online_serving/financial_card_apply_select";
			}
			else 
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * NA80 金融卡線上申請/取消轉帳功能 確認頁
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/financial_card_apply_confirm")
	public String financial_card_apply_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		log.debug("financial_card_apply_confirm start");
		log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+CodeUtil.toJson(reqParam)));
		String target = "/error";
		BaseResult bs = new BaseResult();
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try 
		{	
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null));
				bs.setResult(true);
			}
			// 驗證頁面 TOKEN 和 SESSION TOKEN 是否一致，頁面沒 TOKEN 會到錯誤頁，TOKEN 不同會到錯誤頁
			if(!hasLocale)
			{
				String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
				log.error(ESAPIUtil.vaildLog("Page TOKEN 錯誤 >> {}"+ okMap.get("TOKEN")));
				if(StrUtil.isNotEmpty(okMap.get("TOKEN"))  &&  okMap.get("TOKEN").equals(token)) {
					bs.setResult(Boolean.TRUE);
				}
				if(!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				}
				// 類別
				String TYPE = okMap.get("TYPE");
				// 讓結果頁使用
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, TYPE);
				bs.addData("TYPE", TYPE);
				bs.setResult(true);
			}
		}
		catch (Exception e) 
		{
			log.error("financial_card_apply_confirm error", e);
		}
		finally 
		{
			if (bs != null && bs.getResult()) 
			{
				model.addAttribute("transfer_data", bs);
				target = "/online_serving/financial_card_apply_confirm";
			}
			else 
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * NA80 金融卡線上申請/取消轉帳功能 結果頁
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/financial_card_apply_result")
	public String financial_card_apply_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		log.debug("financial_card_apply_result start");
		log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+CodeUtil.toJson(reqParam)));
		String target = "/error";
		BaseResult bs = new BaseResult();
		try 
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			String cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null));
			cusidn = new String(Base64.getDecoder().decode(cusidn));
//			String type = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
			String type = okMap.get("TYPE");
			String acn = okMap.get("ACNNO");
			String icSeq = okMap.get("ICSEQ");
			log.debug("CUSIDN >> {}", cusidn);
			log.debug(ESAPIUtil.vaildLog("TYPE >> {}"+ type));
			log.debug(ESAPIUtil.vaildLog("ACN >> {}"+ acn));
			log.debug(ESAPIUtil.vaildLog("ICSEQ >> {}"+ icSeq));
			
			bs = online_serving_service.financial_card_apply_result(cusidn, type, acn, icSeq);
			// 顯示在網頁
			model.addAttribute("financial_card_apply_result", bs);
		}
		catch (Exception e) 
		{
			log.error("financial_card_apply_result error", e);
		}
		finally 
		{
			if (bs != null && bs.getResult()) 
			{
				target = "/online_serving/financial_card_apply_result";
			}
			else 
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * NA71 網路銀行交易密碼線上解鎖
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/unlock_ssl_pw")
	public String unlock_ssl_pw(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{	
		String target = "/error";
		BaseResult bs = new BaseResult();
		log.trace("unlock_ssl_pw start");
		try {
			online_serving_service.cleanSession(model);
			String cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null));
			cusidn = new String(Base64.getDecoder().decode(cusidn));
			log.trace("CUSIDN>>>{}",cusidn);
			// IKEY要使用的JSON:DC
			String jsondc = null;
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("unlock_ssl_pw.jsondc: {}"+ jsondc));
			
			bs.addData("CUSIDN", cusidn); 
			bs.addData("jsondc", jsondc);
			bs.setResult(true);
			Map<String, String> bsData = (Map<String, String>) bs.getData();
			log.trace(">>>{}", bs.getData());
			
			// 顯示在網頁
			model.addAttribute("unlock_ssl_pw", bsData);
			

			  //IDGATE身分
	           String idgateUserFlag="N";	
	           BaseResult tmp = new BaseResult();
	           try {		 
	               tmp= idgateservice.checkIdentity(cusidn);		 
	               if(tmp.getResult()) {		 
	                   idgateUserFlag="Y";                  		 
	               }		 
	           }catch(Exception e) {		 
	               log.debug("idgateUserFlag error {}",e);		 
	           }		 
	           
			
			// IDGATE transdata   
			Map<String,String> result = new HashMap();
			result.putAll((Map< String,String>) bs.getData());
            Map<String,Object>IdgateData=(Map<String, Object>) tmp.getData();	
    		String adopid = "NA71";
    		String title = "您有一筆 網路銀行交易密碼線上解鎖  待確認";
        	if(IdgateData != null) {
	            model.addAttribute("idgateUserFlag",idgateUserFlag);
	            if(idgateUserFlag.equals("Y")) {
	            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
	            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
	            	result.put("CUSIDN", cusidn);
					IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(NA71_IDGATE_DATA.class, NA71_IDGATE_DATA_VIEW.class, result));
	                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	            }
        	}
            model.addAttribute("idgateAdopid", adopid);
            model.addAttribute("idgateTitle", title);
            
		}
		catch(Exception e) {
			log.error("unlock_ssl_pw error", e);
		}
		finally 
		{
			if (bs != null && bs.getResult()) 
			{
				target = "/online_serving/unlock_ssl_pw";
			}
			else 
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * NA71 網路銀行交易密碼線上解鎖結果頁
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/unlock_ssl_pw_r")
	public String unlock_ssl_pw_r(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{	
		String target = "/error";
		BaseResult bs = null;
		log.trace("unlock_ssl_pw_r start");
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			Map<String, String> okMap = reqParam;
			// 判斷LOCAL KEY是否有帶值，有帶表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			log.trace("unlock_ssl_pw_r.locale: " + hasLocale);
			if(hasLocale){
				log.trace("unlock_ssl_pw_r.hasLocale...");
				// 判斷從session取出的資料物件是否為BaseResult物件
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( !bs.getResult() ){
					log.trace("unlock_ssl_pw_r.bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				 //IDgate驗證		 
                try {		 
                    if(okMap.get("FGTXWAY").equals("7")) {		 
        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("NA71_IDGATE");
                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
                    }		 
                }catch(Exception e) {		 
                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
                }  
				
				
				bs = online_serving_service.unlock_ssl_pw_r(cusidn,okMap);
				bs.addData("CUSIDN", cusidn);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		}
		catch(Exception e) {
			log.error("unlock_ssl_pw_r error", e);
		}
		finally 
		{
			if (bs != null && bs.getResult()) 
			{
				target = "/online_serving/unlock_ssl_pw_r";
				model.addAttribute("unlock_ssl_pw_r", bs);
			}
			else 
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	// 取消約定轉入帳號
	// N880
	@RequestMapping(value = "/predesignated_account_cancellation")
	public String predesignated_account_cancellation(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("predesignated_account_cancellation_Start~~~~~~~");
		String target = "/error";
		BaseResult bs = null;
		BaseResult bs2 = null;
		try {
			bs = new BaseResult();
			bs2 = new BaseResult();
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			// Get session value by session Key
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			// Get put page value
			// 台幣電文
			bs = online_serving_service.predesignated_account_cancellation(cusidn);
			// 外幣電文
			bs2 = online_serving_service.F031_REST(cusidn);
			
			//IDGATE身分
			String idgateUserFlag="N";
			try {
				BaseResult tmp=idgateservice.checkIdentity(cusidn);
				if(tmp.getResult()) {
					idgateUserFlag="Y";					
				}
				tmp.addData("idgateUserFlag",idgateUserFlag);
				SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA,((Map<String, String>) tmp.getData()));
			}catch(Exception e) {
				log.debug("idgateUserFlag error {}",e);
			}			
			model.addAttribute("idgateUserFlag",idgateUserFlag);
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("predesignated_account_cancellation error >> {}", e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/online_serving/predesignated_account_cancellation";
				model.addAttribute("predesignated_account_cancellation", bs);
				model.addAttribute("f_predesignated_account_cancellation", bs2);
				
				//
				Map<String, Object> bsData = (Map) bs.getData();
				Map<String, Object> bsData2 = (Map) bs2.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				ArrayList<Map<String, String>> rows2 = (ArrayList<Map<String, String>>) bsData2.get("REC");
				if (rows.size() + rows2.size() == 0) {
					target = "/error";
					bs.setPrevious("/INDEX/index");
					bs.setErrorMessage("", (String) bsData.get("nodataMsg"));
					bs.setResult(false);
					model.addAttribute(BaseResult.ERROR, bs);
				}
			} else {
				bs.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}		
			return target;
	}
	
	// 取消約定轉入帳號_確認
	@RequestMapping(value = "/predesignated_account_cancellation_s")
	public String predesignated_account_cancellation_s(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace(ESAPIUtil.vaildLog("predesignated_account_cancellation_s_Start~~~~~~~{}"+CodeUtil.toJson(reqParam)));
		String target = "/error";
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
// 			解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// IKEY要使用的JSON:DC
			String jsondc = null;
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("predesignated_account_cancellation_s.jsondc: {}"+ jsondc));
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
				if(hasLocale) 
				{
					log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
					if( ! bs.getResult())
					{
						log.trace("bs.getResult() >>{}",bs.getResult());
						throw new Exception();
					}
				}
				if( ! hasLocale){
					bs.reset();
					bs = online_serving_service.predesignated_account_cancellation_s(cusidn,okMap);
					bs.addData("jsondc", jsondc);
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
				
				// IDGATE transdata 
				Map<String,String> result = (Map<String, String>) bs.getData();
	            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	            
	            String adopid = "";
	            if(StrUtil.isEmpty(result.get("CCY"))) {
	            	adopid="N880";
	            }else {
	            	adopid="F037";
	            }
	    		String title = "您有一筆約定轉入帳號取消交易待確認";
	    		
	        	if(IdgateData != null) {
		            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
		            	result.put("CUSIDN", cusidn);
		            	log.debug("IDGATE ADOPID >> {}" , adopid);
		            	log.debug("IDGATE TITLE >> {}" , title);
		            	log.debug("IDGATE VALUE >> {}" , result);
		            	Map<String, String> idgateMap = new HashMap<String, String>();
		            	idgateMap.putAll(result);
		            	//臺幣
		            	if(StrUtil.isEmpty(idgateMap.get("CCY"))) {
		            		//臺幣的N880.java 會補0 這邊先把原來的值放到ACN_SHOW , 放到VIEW
		            		//然後ACN補0放到DATA裡 ( 為了跟MS層電文上行一樣 , 才能過IDGateCommand() )
		            		
		            		//新增一個ACN_SHOW給頁面顯示
			            	String acn = idgateMap.get("ACN");
			            	idgateMap.put("ACN_SHOW", acn);
			            	//調整ACN符合電文上傳格式
			            	while(acn.length() < 16)acn = "0" + acn;
			            	idgateMap.put("ACN", acn);
		            		
		            		IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N880_IDGATE_DATA.class, N880_IDGATE_DATA_VIEW.class, idgateMap));
			                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);	
		            	//外幣
		            	}else {
		            		//VIEW一樣放ACN_SHOW
		            		//外幣的F037 會在結果頁把ACN轉成BENACC
		            		//所以在這先把ACN轉成BENACC放進DATA ( 為了跟MS層電文上行一樣 , 才能過IDGateCommand() )
		            		String acn = idgateMap.get("ACN");
		            		idgateMap.put("ACN_SHOW", acn);
		            		idgateMap.put("BENACC",idgateMap.get("ACN"));
		            		IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(F037_IDGATE_DATA.class, F037_IDGATE_DATA_VIEW.class, idgateMap));
			                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);
		            	}
		            }
	        	}
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
				
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("predesignated_account_cancellation_s error >> {}",e);
			}finally {
				if(bs!=null && bs.getResult()) {
					target = "/online_serving/predesignated_account_cancellation_s";
					model.addAttribute("predesignated_account_cancellation", bs);
				}else {
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}		
			return target;
	}
	
	// 取消約定轉入帳號_結果
	@RequestMapping(value = "/predesignated_account_cancellation_r")
	public String predesignated_account_cancellation_r(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("predesignated_account_cancellation_r_Start~~~~~~~");
		String target = "/error";
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
// 			解決Trust Boundary Violation
			Map<String, String> okMap = reqParam;
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
				if(hasLocale) 
				{
					log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
					if( ! bs.getResult())
					{
						log.trace("bs.getResult() >>{}",bs.getResult());
						throw new Exception();
					}
				}
				if( ! hasLocale){
					bs.reset();
					
					//IDgate驗證		 
	                try {		 
	                    if(okMap.get("FGTXWAY").equals("7")) {		 

	        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
	        				Map<String,Object>IdgateData = new HashMap<>();
	        				
	        				if(StrUtil.isEmpty(okMap.get("CCY"))) {
	        					IdgateData = (Map<String, Object>) IdgateDataSession.get("N880_IDGATE");
	        				}else {
	        					IdgateData = (Map<String, Object>) IdgateDataSession.get("F037_IDGATE");
	        				}
	        				
	                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
	                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
	                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
	                    }		 
	                }catch(Exception e) {		 
	                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
	                }  
					
					bs = online_serving_service.predesignated_account_cancellation_r(cusidn,okMap);
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("predesignated_account_cancellation_r error >> {}",e);
			}finally {
				if(bs!=null && bs.getResult()) {
					target = "/online_serving/predesignated_account_cancellation_r";
					model.addAttribute("predesignated_account_cancellation", bs);
				}else {
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}		
			return target;
	}
	
	
	
	@RequestMapping(value = "/idgate")
	public String idGate(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		BaseResult bs = null;
		log.trace("idGate start");
		
		
		try 
		{
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			bs = online_serving_service.idGate(cusidn);
			if(bs != null && bs.getResult()) {
				model.addAttribute("idGateData", bs.getData());
			}
		}
		catch (Exception e) 
		{
			log.error("idgate_confirm error", e);
		}
		finally 
		{
			if (bs != null && bs.getResult()) 
			{
				target = "/online_serving/idGate";
			}
			else 
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	@RequestMapping(value = "/idgate_confirm")
	public String idGate_Confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		BaseResult bs = null;
		log.trace("idgate_confirm start");
		
		
		try 
		{
			bs = new BaseResult();
			bs.setResult(Boolean.TRUE);
		}
		catch (Exception e) 
		{
			log.error("idgate_confirm error>>{}", e);
		}
		finally 
		{
			if (bs != null && bs.getResult()) 
			{
				target = "/online_serving/idGate_confirm";
			}
			else 
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	@RequestMapping(value = "/idgate_result")
	public String idGate_Result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		BaseResult bs = null;
		log.trace("idgate_result start");
		
		String cusidn = null;
		try 
		{
			bs = new BaseResult();
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			bs = online_serving_service.IdGateN915(cusidn, okMap);
		}
		catch (Exception e) 
		{
			log.error("idgate_result error>>{}", e);
		}
		finally 
		{
			if (bs != null && bs.getResult()) 
			{
				target = "/online_serving/idGate_result";
				model.addAttribute("idGate_result",bs);
			}
			else 
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	@RequestMapping(value = "/idgate_cancel")
	public String idgate_cancel(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		BaseResult bs = null;
		log.trace("idgate_cancel start>>>"+reqParam);
		
		
		try 
		{
			bs = new BaseResult();
			bs.addAllData(reqParam);
			bs.setResult(Boolean.TRUE);
		}
		catch (Exception e) 
		{
			log.error("idgate_cancel error>>{}", e);
		}
		finally 
		{
			if (bs != null && bs.getResult()) 
			{
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				model.addAttribute("idGate_cancel", bs.getData());
				target = "/online_serving/idGate_cancel";
			}
			else 
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	@RequestMapping(value = "/idgate_cancel_result")
	public String idgate_cancel_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		BaseResult bs = null;
		log.trace("idgate_cancel_result start>>>");
		
		try 
		{
			bs = new BaseResult();
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			else {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				Map<String, Object> bsData = (Map<String, Object>)bs.getData();
				String DEVICENAME = (String)bsData.get("DEVICENAME");
				bs = online_serving_service.changeIDGateStatus(cusidn, (String)bsData.get("IDGATEID"), (String)bsData.get("DEVICEID"), "2", "");
				bs.addData("DEVICENAME", DEVICENAME);
			}
		}
		catch (Exception e) 
		{
			log.error("idgate_cancel_result error>>{}", e);
		}
		finally 
		{
			if (bs != null && bs.getResult()) 
			{
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				model.addAttribute("idGate_cancel", bs.getData());
				target = "/online_serving/idGate_cancel_result";
			}
			else 
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	@RequestMapping(value = "/idGateChange_aj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody Object idGateChange(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("idGateChange_aj ...");
		String pks = "",cusidn="",idGateId="",deviceId="" ,action = "" ,deviceName="";
		String[] pksArray = null;
		BaseResult bs = null;
		try {
			action =  reqParam.get("action");
			pks = reqParam.get("pk");
			deviceName = reqParam.get("NEWNAME");
			log.info("idGateChange input>>{}",ESAPIUtil.vaildLog(CodeUtil.toJson(reqParam))  );
			pksArray = pks.split("#");
//			pksArray = pks.split("-");
//			cusidn = Arrays.asList(pksArray).get(1);
			cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			idGateId = Arrays.asList(pksArray).get(0);
			deviceId = Arrays.asList(pksArray).get(1);
			bs = online_serving_service.changeIDGateStatus(cusidn, idGateId, deviceId, action, deviceName);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("idGateChange error >> {}",e);
		}
		return bs;
	}
	
	@RequestMapping(value = "/idGateCancel_aj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody Object idGateCancel(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("idGateCancel_aj ...");
		String cusidn = "";
		BaseResult bs = null;
		try {
			cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			
			bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
			Map<String, Object> bsData = (Map<String, Object>)bs.getData();
			bs = online_serving_service.changeIDGateStatus(cusidn, (String)bsData.get("IDGATEID"), (String)bsData.get("DEVICEID"), "2", "");
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("idGateChange error >> {}",e);
		}
		return bs;
	}
	
	@RequestMapping(value = "/idgate_more")
	public String idgate_more(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		BaseResult bs = null;
		log.trace("idgate_more start");
		target = "/online_serving/idGate_more";
		return target;
	}
	
	//數三轉數一申請 https://localhost:9443/nb3/ONLINE/SERVING/upgrade_digital_acc_p1
		@RequestMapping(value = "/upgrade_digital_acc_p1")
		public String upgrade_digital_acc_p1(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			String target = "/error";
			BaseResult bs = new BaseResult();
			log.debug("upgrade_digital_acc_p1>>");
			
			try {
				// 解決Trust Boundary Violation
				Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
				// 判斷是否按上一頁
				boolean isback = "Y".equals(okMap.get("back"));
				if (isback) {
					bs.addData("CUSIDN", okMap.get("CUSIDN"));
					bs.addData("DIGACN", okMap.get("DIGACN"));
					bs.setResult(Boolean.TRUE);
				} else {
					String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
					
					bs = online_serving_service.getDigitalAccount(cusidn, "03") ;
					
					log.debug("getDigitalAccount >> " + bs.getResult());
					
					if (bs.getResult()) {
						bs.addData("CUSIDN", cusidn);					
					} else {
						String msgCode = "Z999";
						String msgName = i18n.getMsg("LB.B0025");//查無交易帳號，無法進行此交易，請洽分行申請辦理約定轉出帳號。
						bs.setMessage(msgCode, msgName);
					}			
				}
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("upgrade_digital_acc_p1 error >> {}",e);
			}finally {
				if(bs!=null && bs.getResult()) {
					target = "/digital_account/upgrade_digital_acc_p1";
					Map<String, String> bsData = (Map<String, String>) bs.getData();
					model.addAttribute("digital_acc", bsData);
				}else {
					model.addAttribute(BaseResult.ERROR, bs);				
				}
			}
			return target;
		}
		
		@RequestMapping(value = "/upgrade_digital_acc_p2")
		public String upgrade_digital_acc_p2(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			String target = "/error";
			BaseResult bs = new BaseResult();
			log.debug("upgrade_digital_acc_p2>>");
			
			try {
				// 解決Trust Boundary Violation
				Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
				
				String digacn = okMap.get("DIGACN");
				String cusidn = okMap.get("CUSIDN");
				
				if (StringUtils.hasText(digacn)  && StringUtils.hasText(cusidn)) {
					bs.addData("CUSIDN", cusidn);
					bs.addData("DIGACN", digacn);
					bs.setResult(Boolean.TRUE);
				} else {
					String msgCode = "Z999";
					String msgName = i18n.getMsg("LB.X1665");//取得帳號資料失敗。
					bs.setMessage(msgCode, msgName);
					bs.setResult(Boolean.FALSE);
				}		

			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("upgrade_digital_acc_p2 error >> {}",e);
			} finally {
				if(bs!=null && bs.getResult()) {
					target = "/digital_account/upgrade_digital_acc_p2";
					Map<String, String> bsData = (Map<String, String>) bs.getData();
					model.addAttribute("digital_acc", bsData);
				}else {
					model.addAttribute(BaseResult.ERROR, bs);				
				}
			}
			return target;
		}
		
		@RequestMapping(value = "/upgrade_digital_acc_p3")
		public String upgrade_digital_acc_p3(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			String target = "/error";
			BaseResult bs = new BaseResult();
			log.debug("upgrade_digital_acc_p3>>");
			
			String uDate = DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss");
			
			try {
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				//String cusidn = "T120124053";
				// 解決Trust Boundary Violation
				Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
				
				String digacn = okMap.get("DIGACN");
				//String digacn = "01064600458";
				if (!StringUtils.hasText(cusidn)) {
					cusidn = okMap.get("CUSIDN");
				}
				
				if (StringUtils.hasText(digacn)  && StringUtils.hasText(cusidn)) {
					bs = online_serving_service.upgradeDigitalAccountClass3(cusidn,digacn);
				} else {
					String msgCode = "Z999";
					String msgName = i18n.getMsg("LB.X1665");//取得帳號資料失敗。
					bs.setMessage(msgCode, msgName);
					bs.setResult(Boolean.FALSE);
				}		
				
				log.debug("upgrade_digital_acc_p3 >> " + bs.getResult());

			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("upgrade_digital_acc_p3 error >> {}",e);
			} finally {
				if(bs!=null && bs.getResult()) {
					target = "/digital_account/upgrade_digital_acc_p3";
					Map<String, String> bsData = (Map<String, String>) bs.getData();
					model.addAttribute("DIGACN", bsData.get("DIGACN"));
					model.addAttribute("upgrade_date", uDate);
				}else {
					model.addAttribute(BaseResult.ERROR, bs);				
				}
			}
			return target;
		}

}
