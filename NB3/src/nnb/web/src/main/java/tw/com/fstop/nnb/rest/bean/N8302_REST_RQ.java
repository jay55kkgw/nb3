package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N8302_REST_RQ extends BaseRestBean_PAY implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9110272854835696650L;

	private String DATE;//日期YYYMMDD
	private String TIME;//時間HHMMSS
	private String SYNC;//Sync.Check Item
	private String PPSYNC;//P.P.Key Sync.Check Item
	private String PINKEY;//網路銀行密碼
	private String CERTACN;//憑證帳號
	private String FLAG;//0：非約定，1：約定
	private String BNKRA;//行庫別
	private String XMLCA;//CA 識別碼
	private String XMLCN;//憑證ＣＮ
	private String PPSYNCN;//P.P.Key Sync.Check Item 16
	private String PINNEW;//網路銀行密碼（新）
	private String CUSIDN;//使用者代號
	private String TSFACN;//轉帳帳號
	private String TYPE;//查詢類別
	private String UNTNUM;//投保單位代號
	private String CUSIDNUN;//統一編號
	private String UNTTEL;//投保單位電話號碼
	private String ITMNUM;//項目代號1:申請2:註銷3:變更
	private String CUSNUM;//勞工退休準備金金額(除舊制勞退準備金外其他的同類電文不用此欄)
	private String TRANSEQ;//交易序號
	private String ISSUER;//晶片卡發卡行庫
	private String ACNNO;//晶片卡帳號
	private String ICDTTM;//晶片卡日期時間
	private String ICSEQ;//晶片卡序號
	private String ICMEMO;//晶片卡備註欄
	private String TAC_Length;//TAC DATA Length
	private String TAC;//TAC DATA
	private String TAC_120space;//TAC DATA Space
	private String TRMID;//端末設備查核碼
	
	private String icSeq;
	private String accNo;
	private String UID;
	private String FGTXWAY;
	private String ACN;
	private String pkcs7Sign;	// IKEY
	private String jsondc;		// IKEY
	private String CMTRANPAGE;
	private String ADOPID;
	private String braCode;
	private String chk;
	private String NeedSHA1;
	private String OUTACN;
//	private String ADSVBH;
//	private String ADAGREEF;
	//IDGATE
	private String sessionID;
	private String idgateID;
	private String txnID;
	
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getSYNC() {
		return SYNC;
	}
	public void setSYNC(String sYNC) {
		SYNC = sYNC;
	}
	public String getPPSYNC() {
		return PPSYNC;
	}
	public void setPPSYNC(String pPSYNC) {
		PPSYNC = pPSYNC;
	}
	public String getPINKEY() {
		return PINKEY;
	}
	public void setPINKEY(String pINKEY) {
		PINKEY = pINKEY;
	}
	public String getCERTACN() {
		return CERTACN;
	}
	public void setCERTACN(String cERTACN) {
		CERTACN = cERTACN;
	}
	public String getFLAG() {
		return FLAG;
	}
	public void setFLAG(String fLAG) {
		FLAG = fLAG;
	}
	public String getBNKRA() {
		return BNKRA;
	}
	public void setBNKRA(String bNKRA) {
		BNKRA = bNKRA;
	}
	public String getXMLCA() {
		return XMLCA;
	}
	public void setXMLCA(String xMLCA) {
		XMLCA = xMLCA;
	}
	public String getXMLCN() {
		return XMLCN;
	}
	public void setXMLCN(String xMLCN) {
		XMLCN = xMLCN;
	}
	public String getPPSYNCN() {
		return PPSYNCN;
	}
	public void setPPSYNCN(String pPSYNCN) {
		PPSYNCN = pPSYNCN;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getTSFACN() {
		return TSFACN;
	}
	public void setTSFACN(String tSFACN) {
		TSFACN = tSFACN;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getUNTNUM() {
		return UNTNUM;
	}
	public void setUNTNUM(String uNTNUM) {
		UNTNUM = uNTNUM;
	}
	public String getCUSIDNUN() {
		return CUSIDNUN;
	}
	public void setCUSIDNUN(String cUSIDNUN) {
		CUSIDNUN = cUSIDNUN;
	}
	public String getUNTTEL() {
		return UNTTEL;
	}
	public void setUNTTEL(String uNTTEL) {
		UNTTEL = uNTTEL;
	}
	public String getITMNUM() {
		return ITMNUM;
	}
	public void setITMNUM(String iTMNUM) {
		ITMNUM = iTMNUM;
	}
	public String getCUSNUM() {
		return CUSNUM;
	}
	public void setCUSNUM(String cUSNUM) {
		CUSNUM = cUSNUM;
	}
	public String getTRANSEQ() {
		return TRANSEQ;
	}
	public void setTRANSEQ(String tRANSEQ) {
		TRANSEQ = tRANSEQ;
	}
	public String getISSUER() {
		return ISSUER;
	}
	public void setISSUER(String iSSUER) {
		ISSUER = iSSUER;
	}
	public String getACNNO() {
		return ACNNO;
	}
	public void setACNNO(String aCNNO) {
		ACNNO = aCNNO;
	}
	public String getICDTTM() {
		return ICDTTM;
	}
	public void setICDTTM(String iCDTTM) {
		ICDTTM = iCDTTM;
	}
	public String getICSEQ() {
		return ICSEQ;
	}
	public void setICSEQ(String iCSEQ) {
		ICSEQ = iCSEQ;
	}
	public String getICMEMO() {
		return ICMEMO;
	}
	public void setICMEMO(String iCMEMO) {
		ICMEMO = iCMEMO;
	}
	public String getTAC_Length() {
		return TAC_Length;
	}
	public void setTAC_Length(String tAC_Length) {
		TAC_Length = tAC_Length;
	}
	public String getTAC() {
		return TAC;
	}
	public void setTAC(String tAC) {
		TAC = tAC;
	}
	public String getTAC_120space() {
		return TAC_120space;
	}
	public void setTAC_120space(String tAC_120space) {
		TAC_120space = tAC_120space;
	}
	public String getTRMID() {
		return TRMID;
	}
	public void setTRMID(String tRMID) {
		TRMID = tRMID;
	}
	public String getIcSeq() {
		return icSeq;
	}
	public void setIcSeq(String icSeq) {
		this.icSeq = icSeq;
	}
	public String getAccNo() {
		return accNo;
	}
	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getPkcs7Sign() {
		return pkcs7Sign;
	}
	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}
	public String getJsondc() {
		return jsondc;
	}
	public void setJsondc(String jsondc) {
		this.jsondc = jsondc;
	}
	public String getCMTRANPAGE() {
		return CMTRANPAGE;
	}
	public void setCMTRANPAGE(String cMTRANPAGE) {
		CMTRANPAGE = cMTRANPAGE;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getBraCode() {
		return braCode;
	}
	public void setBraCode(String braCode) {
		this.braCode = braCode;
	}
	public String getChk() {
		return chk;
	}
	public void setChk(String chk) {
		this.chk = chk;
	}
	public String getNeedSHA1() {
		return NeedSHA1;
	}
	public void setNeedSHA1(String needSHA1) {
		NeedSHA1 = needSHA1;
	}
	public String getOUTACN() {
		return OUTACN;
	}
	public void setOUTACN(String oUTACN) {
		OUTACN = oUTACN;
	}
	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	public String getIdgateID() {
		return idgateID;
	}
	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}
	public String getTxnID() {
		return txnID;
	}
	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}
//	public String getADSVBH() {
//		return ADSVBH;
//	}
//	public void setADSVBH(String aDSVBH) {
//		ADSVBH = aDSVBH;
//	}
//	public String getADAGREEF() {
//		return ADAGREEF;
//	}
//	public void setADAGREEF(String aDAGREEF) {
//		ADAGREEF = aDAGREEF;
//	}
	
}
