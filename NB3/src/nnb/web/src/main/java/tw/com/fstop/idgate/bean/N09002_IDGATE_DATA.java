package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N09002_IDGATE_DATA implements Serializable{

	@SerializedName(value = "TRNCOD")
	private String TRNCOD;

	@SerializedName(value = "PRICE")
	private String PRICE;
	
	@SerializedName(value = "SVACN")
	private String SVACN;
	
	@SerializedName(value = "ACN")
	private String ACN;
	
	@SerializedName(value = "TRNGD")
	private String TRNGD;
	
	@SerializedName(value = "TRNAMT")
	private String TRNAMT;
	
	@SerializedName(value = "FEEAMT1")
	private String FEEAMT1;
	
	@SerializedName(value = "FEEAMT2")
	private String FEEAMT2;
	
	@SerializedName(value = "TRNAMT2")
	private String TRNAMT2;
	
	@SerializedName(value = "TRNAMT_SIGN")
	private String TRNAMT_SIGN;
	
	@SerializedName(value = "FEEAMT1_SIGN")
	private String FEEAMT1_SIGN;
	
	@SerializedName(value = "FEEAMT2_SIGN")
	private String FEEAMT2_SIGN;
	
	@SerializedName(value = "TRNAMT2_SIGN")
	private String TRNAMT2_SIGN;
}
