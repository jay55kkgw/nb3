package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class B023_REST_RQ  extends BaseRestBean_FUND implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6674183224293116119L;
	
	private String CUSIDN;
	private String IP;
	private String BRHCOD;		//網路開戶分行碼
	private String NEXT;		//資料起始位置
	private String RECNO;		//讀取筆數
	
	private String ADOPID = "B023";

	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	public String getBRHCOD() {
		return BRHCOD;
	}
	public void setBRHCOD(String bRHCOD) {
		BRHCOD = bRHCOD;
	}
	public String getNEXT() {
		return NEXT;
	}
	public void setNEXT(String nEXT) {
		NEXT = nEXT;
	}
	public String getRECNO() {
		return RECNO;
	}
	public void setRECNO(String rECNO) {
		RECNO = rECNO;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
}
