package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;


public class N079_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{



	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8674196669371614147L;
	@SerializedName(value = "FDPACN")
	private String FDPACN;
	@SerializedName(value = "FDPTYPENAME")
	private String FDPTYPENAME;
	@SerializedName(value = "FDPNUM")
	private String FDPNUM;
	@SerializedName(value = "AMT")
	private String AMT;
	@SerializedName(value = "INTMTHNAME")
	private String INTMTHNAME;
	
	@SerializedName(value = "DPISDTSHOW")
	private String DPISDTSHOW;
	@SerializedName(value = "DUEDATSHOW")
	private String DUEDATSHOW;
	@SerializedName(value = "INTPAY")
	private String INTPAY;
	@SerializedName(value = "TAX")
	private String TAX;
	@SerializedName(value = "INTRCV")
	private String INTRCV;
	
	@SerializedName(value = "PAIAFTX")
	private String PAIAFTX;
	@SerializedName(value = "NHITAX")
	private String NHITAX;

	
	@Override
	public Map<String, String> coverMap(){
		LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", "臺幣綜存定存解約");
		result.put("帳號", this.FDPACN);
		result.put("存單種類", this.FDPTYPENAME);
		result.put("存單號碼", this.FDPNUM);
		result.put("存單金額", "新臺幣 "+this.AMT+" 元");
		
		result.put("計息方式", this.INTMTHNAME);
		result.put("起存日", this.DPISDTSHOW);
		result.put("到期日", this.DUEDATSHOW);
		result.put("利息", this.INTPAY);
		result.put("所得稅", "新臺幣 "+this.TAX+" 元");
		
		result.put("存單種類", this.FDPTYPENAME);
		result.put("帳號", this.FDPACN);
		result.put("存單種類", this.FDPTYPENAME);
		return result;
	}


	public String getFDPACN() {
		return FDPACN;
	}


	public void setFDPACN(String fDPACN) {
		FDPACN = fDPACN;
	}


	public String getFDPTYPENAME() {
		return FDPTYPENAME;
	}


	public void setFDPTYPENAME(String fDPTYPENAME) {
		FDPTYPENAME = fDPTYPENAME;
	}


	public String getFDPNUM() {
		return FDPNUM;
	}


	public void setFDPNUM(String fDPNUM) {
		FDPNUM = fDPNUM;
	}


	public String getAMT() {
		return AMT;
	}


	public void setAMT(String aMT) {
		AMT = aMT;
	}


	public String getINTMTHNAME() {
		return INTMTHNAME;
	}


	public void setINTMTHNAME(String iNTMTHNAME) {
		INTMTHNAME = iNTMTHNAME;
	}


	public String getDPISDTSHOW() {
		return DPISDTSHOW;
	}


	public void setDPISDTSHOW(String dPISDTSHOW) {
		DPISDTSHOW = dPISDTSHOW;
	}


	public String getDUEDATSHOW() {
		return DUEDATSHOW;
	}


	public void setDUEDATSHOW(String dUEDATSHOW) {
		DUEDATSHOW = dUEDATSHOW;
	}


	public String getINTPAY() {
		return INTPAY;
	}


	public void setINTPAY(String iNTPAY) {
		INTPAY = iNTPAY;
	}


	public String getTAX() {
		return TAX;
	}


	public void setTAX(String tAX) {
		TAX = tAX;
	}


	public String getINTRCV() {
		return INTRCV;
	}


	public void setINTRCV(String iNTRCV) {
		INTRCV = iNTRCV;
	}


	public String getPAIAFTX() {
		return PAIAFTX;
	}


	public void setPAIAFTX(String pAIAFTX) {
		PAIAFTX = pAIAFTX;
	}


	public String getNHITAX() {
		return NHITAX;
	}


	public void setNHITAX(String nHITAX) {
		NHITAX = nHITAX;
	}


}
