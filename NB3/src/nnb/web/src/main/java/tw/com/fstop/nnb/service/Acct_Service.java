package tw.com.fstop.nnb.service;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;

import fstop.orm.po.ADMBANK;
import fstop.orm.po.ADMBRANCH;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmBankDao;
import tw.com.fstop.tbb.nnb.dao.AdmBranchDao;
import tw.com.fstop.tbb.nnb.dao.AdmHolidayDao;
import tw.com.fstop.tbb.nnb.dao.AdmKeyValueDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.I18nHelper;
import tw.com.fstop.web.util.WebCoreUtil;

@Service
public class Acct_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	//TODO:測試其他DAO用
	@Autowired
	private AdmBankDao admBankDao;

	@Autowired
	private AdmHolidayDao admHolidayDao;
	
	@Autowired
	private AdmBranchDao admBranch_Dao;

	@Autowired
	private AdmKeyValueDao admKeyValueDao;
	
	@Autowired
	private I18n i18n;
	
	@Autowired
	ServletContext context;

	
	/**
	 * 資產負債圖
	 * 
	 * @param params
	 * @return
	 */
	public BaseResult assets_query(String cusidn) {
		
		log.trace("assets_query");
		BaseResult bs = null;
		try {
			bs = new BaseResult(); 
			bs = A1000_REST(cusidn, null, null);
			
		} catch (Exception e) {
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * 台幣餘額
	 * 
	 * @param params
	 * @return
	 */
	public BaseResult balance_query(String cusidn) {

		log.trace("balance_query");
		BaseResult bs = null;
		BaseResult ck = null;
		try {
			bs = new BaseResult(); 
			bs = N110_REST(cusidn, null, null);
			ck = N920_REST(cusidn, OUT_ACNO);
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				
				Map<String, Object> ckData = (Map) ck.getData();
				ArrayList<Map<String, String>> ckrows = (ArrayList<Map<String, String>>) ckData.get("REC");
				
				String isoutACN = null;
				
				for (Map<String, String> row : rows) {
					
					if(row.get("ACN") != null)
						row.put("SHOWSELECT", "Y");
					row.put("ACNGROUP", WebCoreUtil.getACNgroup(row.get("ACN")));
					log.debug("WebCoreUtil.getACNgroup={}",WebCoreUtil.getACNgroup(row.get("ACN")));
				 	row.put("ADPIBAL", getFormatedAmount(row.get("ADPIBAL")));
				 	row.put("BDPIBAL", getFormatedAmount(row.get("BDPIBAL")));
				 	row.put("CLR", NumericUtil.fmtAmount(row.get("CLR"), 2));
				 	
				 	for (Map<String, String> ckrow : ckrows) {
//						log.trace("ACN={}",ckrow.get("ACN"));
						if(ckrow.get("ACN") != null)
							isoutACN = "Y";
						else isoutACN = "N";
					}
//					log.debug("isoutACN={}",isoutACN);
					row.put("OUTACN", isoutACN);
				}
								
				if(Integer.valueOf(bsData.get("CMRECNUM").toString()) > 1)
				{
					Map<String, String> total = getTotal(bsData);
					rows.add(total);
				}
				
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("balance_query error>> {}", e);
		}
		return bs;
	}

	/**
	 * 薪轉戶優惠次數查詢
	 * 
	 * @param params
	 * @return
	 */
	public BaseResult dep_salary_query(String cusidn) {

		log.trace("dep_salary_query");
		BaseResult bs = null;
		try {
			bs = new BaseResult();

			bs = N861_REST(cusidn);
			
			
			if(bs != null && bs.getResult()) {
				Map<String, String> bsData = (Map<String,String>) bs.getData();

				bsData.put("SHOWSELECT", "Y");
				bsData.put("ACNGROUP", WebCoreUtil.getACNgroup(bsData.get("ACN")));
				bsData.put("ADPIBAL", getFormatedAmount(bsData.get("ADPIBAL")));
				bsData.put("BDPIBAL", getFormatedAmount(bsData.get("BDPIBAL")));
				bsData.put("CLR", NumericUtil.fmtAmount(bsData.get("CLR"), 2));
				
				
				bsData.put("OUTACN", WebCoreUtil.getACNgroup(bsData.get("ACN")));

				bs.setData(bsData);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("balance_query error>> {}", e);
		}
		return bs;
	}
	
	/**
	 * 取得台幣轉出帳號
	 * 
	 * @param cusidn
	 * @return
	 
	public List<String> getOutAcn(String cusidn) {
		log.trace("Acct_Service.getOutAcn");
		BaseResult bs = null;
		List<String> acnList = new ArrayList<String>();
		try {
			bs = new BaseResult();
			bs = N920_REST(cusidn, OUT_ACNO);
			
			Map<String,Object> dataMap = (Map<String, Object>) bs.getData();
			List<Map<String,String>> recList = (List<Map<String,String>>) dataMap.get("REC");
			
			// 只回傳台幣轉出帳號
			//List<String> acnList = new ArrayList<String>();
			
			for (Map<String, String> map : recList) {
				String acn = map.get("ACN");
				acnList.add(acn);
			}
			//bs.setData(acnList);
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
		}
		log.trace("acnList >> ",acnList);
		return acnList;
	}*/

	public BaseResult time_deposit_details(String cusidn) {
		return time_deposit_details(cusidn, null);
	}
	
	/**
	 * Rest 定存明細
	 * 
	 * @param cusidn
	 * @return
	 */
//	@Transactional(value = "nnb_transactionManager")
	public BaseResult time_deposit_details(String cusidn, String isTxnlLog) {

		log.trace("time_deposit_details");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = N420_REST(cusidn, isTxnlLog);
			if(bs!=null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				// 總計金額格式化
				bsData.put("TOTAMT", NumericUtil.fmtAmount((String) bsData.get("DPTAMT"), 2));
				// Table內容修改
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				for (Map<String, String> row : rows) 
				{
					// 存款種類
					row.put("TYPENAME", getTYPE(row.get("TYPE")));
					// 轉存方式
					row.put("TYPE1", I18nHelper.getTYPE1(row.get("TYPE1")));
					// 起存日
					row.put("DPISDT", DateUtil.convertDate(2, row.get("DPISDT"), "yyyMMdd", "yyy/MM/dd"));
					// 到期日
					String DUEDAT = row.get("DUEDAT");
					row.put("DUEDAT", DateUtil.convertDate(2, DUEDAT, "yyyMMdd", "yyy/MM/dd"));
					// 存款金額
					row.put("AMTFDPFMT", NumericUtil.fmtAmount(row.get("AMTFDP"), 2));
					// 利率格式化
					row.put("ITR", row.get("ITR"));
					// 計息方式
					row.put("INTMTH", I18nHelper.getINTMTH(row.get("INTMTH")));

					// 民國年轉西元年
					DUEDAT = DateUtil.convertDate(1, DUEDAT, "yyyMMdd", "yyyyMMdd");
					// 備註欄依今天是否假日及定存單到期日，決定要否提示適逢例假日說明連結
					row.put("REMIND", getRemind(Integer.valueOf(DUEDAT)).toString());
					
					// 自動轉期未轉次數
					row.put("AUTXFTM", getAUTXFTM(row.get("AUTXFTM")));
					//新增判斷快速選單顯示欄位用 ACN之第4跟5位 = [10,12,60,62,64] 可做解約
					ArrayList<String> conditionList = new ArrayList<String>();
					List list = Arrays.asList("10","12","60","62","64");
					conditionList.addAll(list);
					if (!"5".equals(row.get("TYPE"))) {
						if(conditionList.contains(row.get("ACN").substring(3,5))) {
							//可做解約
							row.put("OPTIONTYPE", "0");
						}else {
							//不可做解約
							row.put("OPTIONTYPE", "1");
						}
					}else {
						//零存整付
						row.put("OPTIONTYPE", "2");
					}
					
					//for 快速選單用
					String jsonString = CodeUtil.toJson(row);
					jsonString.replace("\"", "&acute;");
					row.put("MapValue",jsonString.replace("\"", "&quot;"));
				}
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("time_deposit_details error >> {}", e);
		}
		return bs;
	}
	
	
	/**
	 * 取得計息方式i18n
	 * 
	 * @param INTMTH
	 * @return
	 */
//	public String getINTMTH(String intmth)
//	{
//		String result = intmth;
//		try
//		{
//			if("0".equals(intmth)) 
//			{
//				// 機動
//				result = i18n.getMsg("LB.Floating");
//			}
//			else if("1".equals(intmth) || "2".equals(intmth))
//			{
//				// 固定
//				result = i18n.getMsg("LB.Fixed");
//			}
//		}
//		catch (Exception e)
//		{
//			log.error("getINTMTH_i18n error. parameter >> {}", intmth, e);
//		}
//		return result;
//	}
	
	
	/**
	 * 自動轉期未轉次數，若次數為999顯示無限次數
	 * 未來如果ms_tw有做i18n，可考慮移到N420_Tel_Service.data_Retouch
	 * 
	 * @param AUTXFTM
	 * @return
	 */
	public String getAUTXFTM(String AUTXFTM)
	{
		String result = AUTXFTM;
		try
		{
			if("999".equals(AUTXFTM))
			{
				// 無限次數
				result = i18n.getMsg("LB.Unlimited");
			}
		}
		catch (Exception e) 
		{
			log.error("getAUTXFTM error. parameter >> {}", AUTXFTM, e);
		}
		return result;
	}
	
	
	/**
	 * 利率格式化 01090 > 1.090
	 * 
	 * @param rate
	 * @return
	 */
	public String getFormatedRate(String rate)
	{
		String result = rate;
		try
		{
			BigDecimal b = new BigDecimal(new BigInteger(rate), 3);
			result = NumericUtil.fmtAmount(b.toPlainString(), 3);
		}
		catch (Exception e) 
		{
			log.error("getFormatedRate error. parameter >> {}", rate, e);
		}
		return result;
	}
	
	
	/**
	 * 如果輸入值無小數點，將後兩位視為小數點後數字，先把小數點往前移兩位，再補千分位，若結果尾數為0再補.00
	 * 如果尾數有負號移到開頭
	 * 如果輸入值有小數點，則不移動小數點位置，僅加千分位
	 * 
	 * @param num
	 * @return
	 */
	public String getFormatedAmount(String num) throws NumberFormatException 
	{
		log.trace("getFormatedAmount start");
		String result = num;
		try
		{
			boolean tailNegative = (num.lastIndexOf("-") > 0) ? true : false;
			
			if(tailNegative)
			{
				num = num.substring(0, num.lastIndexOf("-") );
				num = "-" + num;
			}
			
			int scale = new BigDecimal(num).scale();
			BigDecimal amount;
			int resultScale = scale;
			if(scale == 0)
			{
				// 無小數點
				amount = new BigDecimal(new BigInteger(num), 2);
				resultScale = 2;
			}
			else
			{
				// 輸入值有小數點，不改變小數點位數
				amount = new BigDecimal(num);
			}
			
			result = NumericUtil.fmtAmount(amount.toPlainString(), resultScale);
		}
		catch (NumberFormatException e)
		{
			log.error("輸入數值格式錯誤 >> {}", num, e);
			throw e;
		}
		catch (Exception e)
		{
			log.error("getFormatedAmount error. parameter >> {}", num, e);
		}
		log.trace("getFormatedAmount result number >> {}", result);
		return result;
	}
	
	
	/**
	 * 依存單到期日期判斷是否需要提醒客戶
	 * 
	 * @param date
	 * @return
	 */
	public Boolean getRemind(int date)
	{
		log.trace("remind start");
		Boolean remind = false;
		boolean holiday = false;
		String d = DateUtil.getCurentDateTime("yyyyMMdd");
		holiday = admHolidayDao.isAdmholiday();
		
		int today = Integer.valueOf(d);
		log.trace("{} is Holiday ? >> {}", date, holiday);
		
		if(holiday)
		{
			// 今天是假日
			if(date <= today)
			{
				// 到期日未過今天，提醒
				remind = true;
			}
			else
			{
				// 到期日已過今天，不提醒
				remind = false;
			}
		}
		else
		{
			// 今天非假日，不提醒
			remind = false;
		}
		log.trace("remind >> {}", holiday);
		return remind;
	}
	
	
	/**
	 * 取得總計數值
	 * 
	 * @param bsData
	 * @return
	 */
	public Map<String, String> getTotal(Map bsData) throws Exception
	{
		Map<String, String> result = new HashMap<>();
		try
		{
			int size = Integer.valueOf(bsData.get("CMRECNUM").toString());
			if(size <= 1)
			{
				throw new Exception("資料總數必須大於一筆");
			}
			result.put("ACN", i18n.getMsg("LB.X0923"));	//TODO i18n
			result.put("KIND", "");
			result.put("ADPIBAL", NumericUtil.fmtAmount(bsData.get("TOTAL_ADPIBAL").toString(), 2));
			result.put("BDPIBAL", NumericUtil.fmtAmount(bsData.get("TOTAL_BDPIBAL").toString(), 2));
			result.put("CLR", NumericUtil.fmtAmount(bsData.get("TOTAL_CLR").toString(), 2));
			result.put("SHOWSELECT", "N");
		}
		catch (Exception e)
		{
			log.error("getTotal error", e);
		}
		return result;
	}
	
	
	/**
	 * 存款種類i18n
	 * 
	 * @param type
	 * @return
	 */
	public String getTYPE(String type)
	{
		String result = type;
		try
		{
			// 存款種類
			String[] depositType = {"1", "2", "3", "4", "5"};
			Map<String, String> map = new HashMap<>();
			for(int i = 0; i < depositType.length; i++)
			{
				map.put(String.valueOf((i + 1)), depositType[i]);
			}
		
			String tail = type;
			if(map.containsKey(type))
			{
				tail = type;
			}
			else
			{
				// 其他
				tail = "0";
			}
			result = i18n.getMsg("LB.NTD_deposit_type_" + tail);
		}
		catch (Exception e)
		{
			log.error("getTYPE_i18n error. type >> {}", type,  e);
		}
		return result;
	}
	
	
	/**
	 * 轉存方式i18n
	 * 
	 * @param type1
	 * @return
	 */
//	public String getTYPE1(String type1)
//	{
//		String result = type1;
//		switch(type1) 
//		{
//		    case "1" : 
//		    	// 本息
//		    	result = i18n.getMsg("LB.Principal_and_interest");
//		        break; 
//		    case "2" :
//		    	// 本金
//		    	result = i18n.getMsg("LB.Principal");
//		        break; 
//		    default: 
//		} 
//		return result;
//	}
	
	
	/**
	 * 取得活期明細後製
	 * 
	 */
	public BaseResult demand_deposit_detail_Retouch(BaseResult bs) {
		log.debug("demand_deposit_detail_Retouch RQ = {}", bs.getData());
		// 建立回傳
		LinkedList<Object> labelList = new LinkedList<>();
		// 取得回傳bs
		Map<String, Object> dataMap = (Map) bs.getData();
		// 取得 REC
		List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
		// 取得Map中的值後製
		for (Map<String, String> map : rowListMap) {
			String showTrnTime = DateUtil.convertDate(2, map.get("TRNTIME"), "HHmmssSSS", "HH:mm:ss");
			String showLstltd = DateUtil.convertDate(2, map.get("LSTLTD"), "yyyMMdd", "yyy/MM/dd");
			String DPWDAMTfmt = NumericUtil.fmtAmount(map.get("DPWDAMT"), 2);
			String DPSVAMTfmt = NumericUtil.fmtAmount(map.get("DPSVAMT"), 2);
			String BALfmt = NumericUtil.fmtAmount(map.get("BAL"), 2);
			String downloadDPWDAMT = DPWDAMTfmt;
			log.debug("stringstringstring",downloadDPWDAMT);
			String downloadBAL = BALfmt;
			String showSVAMT = DPSVAMTfmt;
			if(showSVAMT.equals("0.00")) {
				showSVAMT = "";
			}
			//支出收入皆0時留支出
			if(DPWDAMTfmt.equals("0.00") && (!DPSVAMTfmt.equals("0.00"))) {
				DPWDAMTfmt = "";
			}
			if(DPSVAMTfmt.equals("0.00")) {
				DPSVAMTfmt = "";
			}
			downloadDPWDAMT = format(downloadDPWDAMT,1);
			
			log.debug("stringstringstring",downloadDPWDAMT);
			downloadBAL += " ";
			// 覆蓋原本的Map
			map.put("SHOWTRNTIME", showTrnTime);
			map.put("SHOWLSTLTD", showLstltd);
			map.put("SHOWSVAMT", showSVAMT);
			// 新增 fmt的值到Map
			map.put("DLDPWDAMT",downloadDPWDAMT);
			map.put("DLBAL", downloadBAL);
			map.put("DPWDAMTfmt", DPWDAMTfmt);
			map.put("DPSVAMTfmt", DPSVAMTfmt);
			map.put("BALfmt", BALfmt);
			Locale currentLocale = LocaleContextHolder.getLocale();
	 		String locale = currentLocale.toString();
    		String memo = map.get("MEMO");
    		String r = "";
    		log.debug("locale >> {}" , locale);
    		if("ACH".equalsIgnoreCase(map.get("TRNBRH")) && memo.matches("\\d{3}"))
			{
    			switch (locale) {
				case "en":
					r = admKeyValueDao.translatorEN("MEMOACH", memo);		
					break;
				case "zh_CN":
					r = admKeyValueDao.translatorCN("MEMOACH", memo);		
					break;
				default:
					r = admKeyValueDao.translatorTW("MEMOACH", memo);		
					break;
    		 }
			}
			else {
				switch (locale) {
				case "en":
					r = admKeyValueDao.translatorEN("MEMO", memo);		
					break;
				case "zh_CN":
					r = admKeyValueDao.translatorCN("MEMO", memo);		
					break;
				default:
					r = admKeyValueDao.translatorTW("MEMO", memo);		
					break;
			  }
		}
    		map.put("MEMO_C", r);
		}
		// 總筆數
		int COUNT = rowListMap.size();
		// 取得全部的ACN並放入List
		LinkedHashSet<String> acnAllList = new LinkedHashSet<String>();
		for (Map<String, String> map : rowListMap) {
			map.get("ACN");
			acnAllList.add(map.get("ACN"));
		}
		log.debug("acnAllList : {}", acnAllList);
		// 根據acnAllList整理REC並放入listAcnMap
		BigDecimal allIncomeAmount=new BigDecimal("0.00");
		BigDecimal allOutcomeAmount=new BigDecimal("0.00");
		for (String strAcn : acnAllList) {
			LinkedHashMap<String, Object> labelMap = new LinkedHashMap<String, Object>();
			LinkedList<Object> listAcnMap = new LinkedList<>();
			BigDecimal incomeAmount=new BigDecimal("0.00");
			BigDecimal outcomeAmount=new BigDecimal("0.00");
			for (Map map : rowListMap) {
				if (strAcn.equals(map.get("ACN"))) {
					listAcnMap.add(map);
					try {
						//收入總計DPSVAMT
						if(null !=map.get("DPSVAMT")){
							incomeAmount=incomeAmount.add(new BigDecimal((String)map.get("DPSVAMT")));
							allIncomeAmount=allIncomeAmount.add(new BigDecimal((String)map.get("DPSVAMT")));
						}
						//支出總計DPWDAMT
						if(null !=map.get("DPWDAMT")){
							outcomeAmount=outcomeAmount.add(new BigDecimal((String)map.get("DPWDAMT")));
							allOutcomeAmount=allOutcomeAmount.add(new BigDecimal((String)map.get("DPWDAMT")));
						}
					}
					catch(Exception e) {
						log.error("加總錯誤!!!");
						log.error(e.toString());
					}
				}
			}
			labelMap.put("SUMDPSVAMT", incomeAmount.toString());
			labelMap.put("SUMDPWDAMT", outcomeAmount.toString());
			labelMap.put("ACN", strAcn);
			labelMap.put("rowListMap", listAcnMap);
			labelList.add(labelMap);
		}
		log.debug("rowListMap RS: {}", rowListMap);
		// 移除REC
		dataMap.remove("REC");
		bs.addData("labelList", labelList);
		bs.addData("COUNT", String.valueOf(COUNT));
		bs.addData("SUMALLIN", allIncomeAmount.toString());
		bs.addData("SUMALLOUT", allOutcomeAmount.toString());
		log.debug("SUM >>"+CodeUtil.toJson(bs.getData()));
		log.debug("demand_deposit_detail_Retouch RS = {}", bs.getData());
		return bs;
	}

	/**
	 * 取得活期明細
	 * 
	 * @param cusidn
	 * @param reqParam
	 *            前端輸入變數
	 * @return
	 */
	public BaseResult getTwDepositDetail(String cusidn, Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			// 取得傳入帳號
			String acn = "ALL".equals(reqParam.get("ACN")) ? "" : reqParam.get("ACN");
			
			// ACN must be "" or Number，2021/05/27 頻繁收到帶有中文之帳號導致TMRA出Z012，故加入檢核。
			if (!"".equals(acn) && !NumberUtils.isDigits(acn)) {
				bs = new BaseResult();
				bs.setResult(false);
				bs.setMsgCode("BV001");
				bs.setMessage(i18n.getMsg("LB.X1482") + i18n.getMsg("LB.D1552")); // 檢核輸入帳號錯誤
				log.error("N130 acn is not valid {}", acn);
				return bs;
			}
			
			// if acn==""查詢全部 else查詢單一帳號
			bs = N130_REST(cusidn, acn, reqParam);
			//處理回傳的BS
			if (bs != null && bs.getResult()) {
				demand_deposit_detail_Retouch(bs);
				// 查詢區間
				String cmsdate = reqParam.get("cmsdate"); // 日期(起)
				String cmedate = reqParam.get("cmedate"); // 日期(迄)
				log.debug(ESAPIUtil.vaildLog("cmsdate >> " + cmsdate)); 
				log.debug(ESAPIUtil.vaildLog("cmedate >> " + cmedate));
				String cmperiod = cmsdate + "~" + cmedate;
				bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));// 由前端傳入放入查詢時間
				bs.addData("CMPERIOD", cmperiod);
				log.debug("demand_deposit_detail BaseResult RS :{}", bs.getData());
			}
		} catch (Exception e) {
			log.error("getTwDepositDetail {}", e);
			log.error("bs getData : {} ", bs.getData());
		}
		return bs;
	}

	/**
	 * 取得活期明細直接下載
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult twDepositDetaildirectDownload(String cusidn, Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			bs = getTwDepositDetail(cusidn, reqParam);
			if (bs != null && bs.getResult()) {
				Map<String, Object> dataMap = (Map) bs.getData();
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
			
				
				log.trace("dataMap={}", dataMap);
				
				log.debug("rowListMap={}", rowListMap);
				Map<String, Object> parameterMap = new HashMap<String, Object>();

				parameterMap.put("CMQTIME", dataMap.get("CMQTIME"));
				parameterMap.put("CMPERIOD", dataMap.get("CMPERIOD"));
				parameterMap.put("COUNT", dataMap.get("COUNT"));
				parameterMap.put("SUMALLIN", dataMap.get("SUMALLIN"));
				parameterMap.put("SUMALLOUT", dataMap.get("SUMALLOUT"));

				parameterMap.put("downloadFileName", reqParam.get("downloadFileName"));
				parameterMap.put("downloadType", reqParam.get("downloadType"));
				parameterMap.put("templatePath", reqParam.get("templatePath"));
				parameterMap.put("hasMultiRowData", reqParam.get("hasMultiRowData"));

				String downloadType = reqParam.get("downloadType");
				log.trace(ESAPIUtil.vaildLog("downloadType >> " + downloadType)); 

				// EXCEL和OLDEXCEL下載
				if ("EXCEL".equals(downloadType) || "OLDEXCEL".equals(downloadType)) {
					parameterMap.put("headerRightEnd", reqParam.get("headerRightEnd"));
					parameterMap.put("headerBottomEnd", reqParam.get("headerBottomEnd"));
					parameterMap.put("multiRowStartIndex", reqParam.get("multiRowStartIndex"));
					parameterMap.put("multiRowEndIndex", reqParam.get("multiRowEndIndex"));
					parameterMap.put("multiRowCopyStartIndex", reqParam.get("multiRowCopyStartIndex"));
					parameterMap.put("multiRowCopyEndIndex", reqParam.get("multiRowCopyEndIndex"));
					parameterMap.put("multiRowDataListMapKey", reqParam.get("multiRowDataListMapKey"));
					parameterMap.put("rowRightEnd", reqParam.get("rowRightEnd"));
				}
				// TXT下載
				else {
					parameterMap.put("txtHeaderBottomEnd", reqParam.get("txtHeaderBottomEnd"));
					parameterMap.put("txtMultiRowStartIndex", reqParam.get("txtMultiRowStartIndex"));
					parameterMap.put("txtMultiRowEndIndex", reqParam.get("txtMultiRowEndIndex"));
					parameterMap.put("txtMultiRowCopyStartIndex", reqParam.get("txtMultiRowCopyStartIndex"));
					parameterMap.put("txtMultiRowCopyEndIndex", reqParam.get("txtMultiRowCopyEndIndex"));
					parameterMap.put("txtMultiRowDataListMapKey", reqParam.get("txtMultiRowDataListMapKey"));
				}
				bs.addData("parameterMap", parameterMap);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("twDepositDetaildirectDownload error >> {}", e);
		}
		return bs;
	}
	
	/**
	 * N015
	 * 支存不足輸入頁
	 * 
	 * @param cusidn
	 * @return
	 */
	public BaseResult checking_insufficient(String cusidn) {
		log.trace("checking_insufficient");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = N920_REST(cusidn, CHECK_ACNO);	
			Map<String, Object> bsData = (Map) bs.getData();
			ArrayList<Map<String, String>> row = (ArrayList<Map<String, String>>) bsData.get("REC");
			String count = String.valueOf(row.size());
			log.trace("count>>>{}",count);
			String msgCode = "Z999";
//			String msgName = "查無交易帳號，無法進行此交易，請洽分行申請辦理約定轉出帳號。";
			String msgName = i18n.getMsg("LB.X1780");
			if(count == null || "0".equals(count)) {
				bs.setMessage(msgCode, msgName);
				bs.setResult(Boolean.FALSE);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}

	/**
	 * 支存不足
	 * 
	 * @param cusidn
	 * @return
	 */
	public BaseResult checking_insufficient_result(String cusidn, Map<String, String> reqParam) {

		log.trace("checking_insufficient_result>>{}", cusidn);
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// call ws API
			String acn = reqParam.get("ACN");
			String USERDATA_X50 = reqParam.get("USERDATA_X50");
			if(USERDATA_X50 != null && !"".equals(USERDATA_X50)) {
				bs = call_N015_REST(cusidn , acn ,USERDATA_X50);
				
			}else {
				bs = call_N015_REST(cusidn , acn ,"");				
			}
			if(bs!=null && bs.getResult()) {
				bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				//暫時先做去掉加號
				Map<String,Object> callRow = (Map) bs.getData();
				//不足扣票據總金額
				String sntamt = (String) callRow.get("SNTAMT");
				bs.addData("SNTAMT",NumericUtil.formatNumberString(sntamt,2));
				//帳上餘額
				String totbal = (String) callRow.get("TOTBAL");
				bs.addData("TOTBAL",NumericUtil.formatNumberString(totbal,2));
				//可用餘額(含透支)
				String avlbal = (String) callRow.get("AVLBAL");
				bs.addData("AVLBAL",NumericUtil.formatNumberString(avlbal,2));
				
				ArrayList<Map<String, String>> callTable = (ArrayList<Map<String, String>>) callRow.get("REC");
				ArrayList<Map<String , String>> callRec = new ArrayList<Map<String , String>>();
				log.trace(ESAPIUtil.vaildLog("callTable >> " + CodeUtil.toJson(callTable))); 
				for (Map<String, String> map : callTable) {
					if((!"".equals(map.get("CHKDATE"))) && map.get("CHKDATE") != null) {
						String downloadTOTABAL = format(totbal,2);
						String downloadAVLBAL = format(avlbal,2);
						String downloadSNTAMT = format(sntamt,2);
						
						map.put("ACN",acn);
						//票據金額
						map.put("AMTACC",NumericUtil.formatNumberString(map.get("AMTACC"),2));
						//票據號碼
						map.put("CHKNUM", NumericUtil.fmtAmount(map.get("CHKNUM"),0).replaceAll(",",""));
						map.put("DOWNLOADTOTBAL", downloadTOTABAL);
						map.put("DOWNLOADAVLBAL", downloadAVLBAL);
						map.put("DOWNLOADSNTCNT", (String) callRow.get("SNTCNT"));
						map.put("DOWNLOADSNTAMT", downloadSNTAMT);
						callRec.add(map);
					}					
				}
				callRow.put("REC", callRec);
				//帳號
				bs.addData("ACN", acn);
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return bs;
	}
	

	/**
	 * 支存已兌現票據輸入頁
	 * 
	 * @param sessionId
	 * @param psw
	 *            登入密碼(base64)
	 * @return
	 */
	public BaseResult checking_cashed(String cusidn) {
		log.trace("checking_cashed>>");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = N920_REST(cusidn, CHECK_ACNO);
			Map<String, Object> dataMap = (Map) bs.getData();
			dataMap.put("JSON", CodeUtil.toJson(dataMap.get("ACN")));
			log.debug("getChecking_cashedAcn_dataMap>>{}", dataMap);
			ArrayList<Map<String, String>> row = (ArrayList<Map<String, String>>) dataMap.get("REC");
			String count = String.valueOf(row.size());
			log.trace("count>>>{}",count);
			String msgCode = "Z999";
//			String msgName = "查無交易帳號，無法進行此交易，請洽分行申請辦理約定轉出帳號。";
			String msgName = i18n.getMsg("LB.X1780");
			if(count == null || "0".equals(count)) {
				bs.setMessage(msgCode, msgName);
				bs.setResult(Boolean.FALSE);
			}
		} catch (Exception e) {
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}

	/**
	 * 支存已兌現票據明細
	 * 
	 * @param cusidn
	 * @param reqParam
	 *            前端輸入變數
	 * @return
	 */
	public BaseResult checking_cashed_result(String cusidn, Map<String, String> reqParam) {
		log.trace("checking_cashed_result_service>>");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			String acn = "AllAcn".equals(reqParam.get("ACN")) ? "99999999999" : reqParam.get("ACN");
			String dpwdamt = new String();
			String dpsvamt = new String();
			String dpibal = new String();
			Float total_dpwdamt = (float) 0;
			Float total_dpsvamt = (float) 0;
			Float dpwdamt_f = (float) 0;
			Float dpsvamt_f = (float) 0;
			DecimalFormat df = new DecimalFormat("#,##0.00");
			
			// TODO查詢全部
			LinkedList<Object> labelList = new LinkedList<>();
			if ("99999999999".equals(acn)){
				
				bs = N625_REST(cusidn, acn, reqParam);
				if(bs!=null && bs.getResult()) {
					
					Map<String, Object> dataMap = (Map) bs.getData();
					ArrayList<Map<String, String>> rowListMap = (ArrayList<Map<String, String>>) dataMap.get("REC");
					ArrayList<Map<String , String>> callRec = new ArrayList<Map<String , String>>();
					
					for (Map<String, String> map : rowListMap) {
						if((!"".equals(map.get("LTD"))) && map.get("LTD") != null) {
						// 取最後餘額
						dpibal = map.get("DPIBAL");
						// 支出與收入加總計算
						dpwdamt = map.get("DPWDAMT");
						dpsvamt = map.get("DPSVAMT");
						dpwdamt_f = StrUtil.isEmpty(dpwdamt) ? 0 : Float.parseFloat(dpwdamt.replace(",", ""));
						dpsvamt_f = StrUtil.isEmpty(dpsvamt) ? 0 : Float.parseFloat(dpsvamt.replace(",", ""));
						total_dpwdamt = total_dpwdamt + dpwdamt_f;
						total_dpsvamt = total_dpsvamt + dpsvamt_f;

							
						

						String downloadAMT = format(map.get("AMT"),1);
						String downloadDPIBAL = format(map.get("DPIBAL"),1);
						
						String downloadLTD = map.get("LTD");
						map.put("DOWNLOADLTD",downloadLTD.replaceAll("/",""));
						map.put("DOWNLOADAMT",downloadAMT);
						map.put("DOWNLOADDPIBAL",downloadDPIBAL);
						
						//異動日處理
						map.put("LTD",DateUtil.addSlash2(map.get("LTD")));	
						//時間處理
						map.put("SHOWTIME",DateUtil.formatTime(map.get("TIME")));						
						//餘額格式處理
						map.put("DPIBAL",NumericUtil.fmtAmount(NumericUtil.addDot(map.get("DPIBAL"), 2),2));												
						//篩選空資料
						callRec.add(map);
						}
					}					
					rowListMap = callRec;
					
					// 總筆數
					int COUNT = 0;
					
					// 取得全部的ACN並放入List
					Set<String> acnAllList = new HashSet<String>();
					for (Map<String, String> map : rowListMap) {
						map.get("ACN");
						acnAllList.add(map.get("ACN"));
					}
					
					log.trace("acnAllList>>{}", acnAllList);
					
					for (String strAcn : acnAllList){
						LinkedHashMap<String, Object> labelMap = new LinkedHashMap<String, Object>();
						LinkedList<Object> listAcnMap = new LinkedList<>();
						for (Map map : rowListMap) {
							if (strAcn.equals(map.get("ACN"))) {
								listAcnMap.add(map);
							}
						}
						labelMap.put("ACN", strAcn);
						labelMap.put("rowListMap", listAcnMap);
						labelMap.put("TOTAL_DPWDAMT", df.format(total_dpwdamt));
						labelMap.put("TOTAL_DPSVAMT", df.format(total_dpsvamt));
						labelMap.put("LASTDPIBAL", NumericUtil.fmtAmount(NumericUtil.addDot(dpibal, 2),2));
						labelList.add(labelMap);
						//總筆數計算
						COUNT = COUNT + listAcnMap.size();
					}					
					
					bs.addData("COUNT", String.valueOf(COUNT));
					bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));					
					bs.addData("labelList", labelList);

					reqParam = DateUtil.periodProcessing(reqParam);
					String cmsdate = reqParam.get("cmsdate"); // 日期(起)
					String cmedate = reqParam.get("cmedate"); // 日期(迄)
					// 日期一樣就只顯示一個
					if (cmsdate != cmedate) {
						bs.addData("cmedate", cmsdate + " ～ " + cmedate);
					} else {
						bs.addData("cmedate", cmedate);
					}	
					//遇到繼續查詢
					String FGPERIOD = reqParam.get("FGPERIOD");
					String CMSDATE = cmsdate;
					String CMEDATE = cmedate;
					String USERDATA_X50 = reqParam.get("USERDATA_X50");
					bs.addData("FGPERIOD",FGPERIOD);
					bs.addData("CMSDATE",CMSDATE);
					bs.addData("CMEDATE",CMEDATE);
					bs.addData("USERDATA_X50",USERDATA_X50);
				}
			}
			//一般選擇
			else {
					LinkedHashMap<String, Object> labelMap = new LinkedHashMap<String, Object>();
					bs = N625_REST(cusidn, acn, reqParam);
					if(bs!=null && bs.getResult()) {
					Map<String, Object> dataMap = (Map) bs.getData();
					ArrayList<Map<String, String>> rowListMap = (ArrayList<Map<String, String>>) dataMap.get("REC");
					ArrayList<Map<String , String>> callRec = new ArrayList<Map<String , String>>();

					for (Map<String, String> map : rowListMap) {
						if((!"".equals(map.get("LTD"))) && map.get("LTD") != null) {
						// 取最後餘額
						dpibal = map.get("DPIBAL");
						// 支出與收入加總計算
						dpwdamt = map.get("DPWDAMT");
						dpsvamt = map.get("DPSVAMT");
						dpwdamt_f = StrUtil.isEmpty(dpwdamt) ? 0 : Float.parseFloat(dpwdamt.replace(",", ""));
						dpsvamt_f = StrUtil.isEmpty(dpsvamt) ? 0 : Float.parseFloat(dpsvamt.replace(",", ""));
						total_dpwdamt = total_dpwdamt + dpwdamt_f;
						total_dpsvamt = total_dpsvamt + dpsvamt_f;
				
						String downloadAMT = format(map.get("AMT"),1);
						String downloadDPIBAL = format(map.get("DPIBAL"),1);
						String downloadLTD = map.get("LTD");
						map.put("DOWNLOADLTD",downloadLTD.replaceAll("/",""));
						map.put("DOWNLOADAMT",downloadAMT);
						map.put("DOWNLOADDPIBAL",downloadDPIBAL);
						
						//異動日處理
						map.put("LTD",DateUtil.addSlash2(map.get("LTD")));
						//時間處理
						map.put("SHOWTIME",DateUtil.formatTime(map.get("TIME")));						
						//餘額格式處理
						map.put("DPIBAL",NumericUtil.fmtAmount(NumericUtil.addDot(dpibal, 2),2));												
						//篩選空資料
						callRec.add(map);
						}
					}
						rowListMap = callRec;
						labelMap.put("ACN",acn);
						labelMap.put("rowListMap",rowListMap);
						labelMap.put("TOTAL_DPWDAMT", df.format(total_dpwdamt));
						labelMap.put("TOTAL_DPSVAMT", df.format(total_dpsvamt));
						labelMap.put("LASTDPIBAL", NumericUtil.fmtAmount(NumericUtil.addDot(dpibal, 2),2));
						labelList.add(labelMap);
						// 總筆數
						int COUNT = rowListMap.size();
						bs.addData("COUNT", String.valueOf(COUNT));
						bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
						bs.addData("labelList", labelList);
						reqParam = DateUtil.periodProcessing(reqParam);
						String cmsdate = reqParam.get("cmsdate");
						String cmedate = reqParam.get("cmedate");
						// 日期一樣就只顯示一個
						if (cmsdate != cmedate) {
							bs.addData("cmedate", cmsdate + " ～ " + cmedate);
						} else {
							bs.addData("cmedate", cmedate);
						}
						//遇到繼續查詢
						String FGPERIOD = reqParam.get("FGPERIOD");
						String CMSDATE = cmsdate;
						String CMEDATE = cmedate;
						String USERDATA_X50 = reqParam.get("USERDATA_X50");
						bs.addData("FGPERIOD",FGPERIOD);
						bs.addData("CMSDATE",CMSDATE);
						bs.addData("CMEDATE",CMEDATE);
						bs.addData("USERDATA_X50",USERDATA_X50);
					}
			}
		} catch (Exception e) {
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	/**
	 * 已兌現票據明細直接下載
	 * 
	 * @param cusidn
	 * @param reqParam 前端輸入變數
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public BaseResult checkingCashedDirectDownload(String cusidn,Map<String,String> reqParam){
		BaseResult bs = null;
		try{
			bs = checking_cashed_result(cusidn,reqParam);

			if(bs != null && bs.getResult()){
				Map<String,Object> dataMap = (Map<String,Object>)bs.getData();
				log.trace("dataMap={}",dataMap);
				
				Map<String, Object> parameterMap = new HashMap<String, Object>();

				parameterMap.put("CMQTIME", dataMap.get("CMQTIME"));
				
				
				String cmdate = "";
				if(dataMap.get("cmsdate") == null){
					cmdate = (String) dataMap.get("cmedate");
				}
				else {
					cmdate = dataMap.get("cmsdate") + "~" + dataMap.get("cmedate");
				}
				log.trace("cmdate={}",cmdate);
				parameterMap.put("cmdate",cmdate);
				
				List<Map<String,Object>> labelListMap = (List<Map<String,Object>>)dataMap.get("labelList");
				log.trace("labelListMap={}",labelListMap);
				
				//總筆數
				int TOTALCOUNT = 0;
					
				for(Map<String,Object> map : labelListMap){
					List<Map<String,String>> rowListMap = (List<Map<String,String>>)map.get("rowListMap");
					TOTALCOUNT += rowListMap.size();
					
					//總計支出金額
					int TOTAL_DPWDAMT = 0;
					//總計收入金額
					int TOTAL_DPSVAMT = 0;
					//總計餘額
					int LASTDPIBAL = 0;
					//小數點位置
					int dotPosition = 0;
					//轉金額千分位格式
					DecimalFormat df = new DecimalFormat();
					
					for(int x=0;x<rowListMap.size();x++){
						Map<String,String> detailMap = rowListMap.get(x);
						
						//支出
						String DPWDAMT = detailMap.get("DPWDAMT");
						if("".equals(DPWDAMT) || DPWDAMT == null) {
							DPWDAMT = "0";
						}
						log.trace("DPWDAMT={}",DPWDAMT);
						dotPosition = DPWDAMT.indexOf(".");
						log.trace("dotPosition={}",dotPosition);
						if(dotPosition > -1){
							DPWDAMT = DPWDAMT.substring(0,dotPosition);
							log.trace("DPWDAMT={}",DPWDAMT);
						}
						DPWDAMT = DPWDAMT.replace(",","");
						log.trace("DPWDAMT={}",DPWDAMT);
						try{
							TOTAL_DPWDAMT += Integer.valueOf(DPWDAMT);
						}
						catch(Exception e){
							log.error("",e);
						}
						detailMap.put("DPWDAMT",NumericUtil.fmtAmount(DPWDAMT,2));
						
						//收入
						String DPSVAMT = detailMap.get("DPSVAMT");
						if("".equals(DPSVAMT) || DPSVAMT == null) {
							DPSVAMT = "0";
						}
						log.trace("DPSVAMT={}",DPSVAMT);
						dotPosition = DPWDAMT.indexOf(".");
						log.trace("dotPosition={}",dotPosition);
						if(dotPosition > -1){
							DPSVAMT = DPSVAMT.substring(0,dotPosition);
							log.trace("DPSVAMT={}",DPSVAMT);
						}
						DPSVAMT = DPSVAMT.replace(",","");
						log.trace("DPSVAMT={}",DPSVAMT);
						try{
							TOTAL_DPSVAMT += Integer.valueOf(DPSVAMT);
						}
						catch(Exception e){
							log.error("",e);
						}
						detailMap.put("DPSDAMT",NumericUtil.fmtAmount(DPSVAMT,2));
						
						//餘額
						String DPIBAL = detailMap.get("DPIBAL");
						if("".equals(DPIBAL)) {
							DPIBAL = "0";
						}
						log.trace("DPIBAL={}",DPIBAL);
						dotPosition = DPIBAL.indexOf(".");
						log.trace("dotPosition={}",dotPosition);
						if(dotPosition > -1){
							DPIBAL = DPIBAL.substring(0,dotPosition);
							log.trace("DPIBAL={}",DPIBAL);
						}
						DPIBAL = DPIBAL.replaceAll(",","");
						log.trace("DPIBAL={}",DPIBAL);
						try{
							LASTDPIBAL = Integer.valueOf(DPIBAL);
						}
						catch(Exception e){
							log.error("",e);
						}
						if(x == rowListMap.size() - 1){
							map.put("TOTAL_DPWDAMT",df.format(TOTAL_DPWDAMT) + ".00");
							map.put("TOTAL_DPSVAMT",df.format(TOTAL_DPSVAMT) + ".00");
							map.put("LASTDPIBAL",df.format(LASTDPIBAL) + ".00");
						}
					}
				}
				
				parameterMap.put("COUNT",String.valueOf(TOTALCOUNT));

				parameterMap.put("downloadFileName", reqParam.get("downloadFileName"));
				parameterMap.put("downloadType", reqParam.get("downloadType"));
				parameterMap.put("templatePath", reqParam.get("templatePath"));
				parameterMap.put("hasMultiRowData", reqParam.get("hasMultiRowData"));
				
				String downloadType = reqParam.get("downloadType");
				log.trace(ESAPIUtil.vaildLog("downloadType >> " + downloadType)); 

				// EXCEL和OLDEXCEL下載
				if ("EXCEL".equals(downloadType) || "OLDEXCEL".equals(downloadType))
				{
					parameterMap.put("headerRightEnd",reqParam.get("headerRightEnd"));
					parameterMap.put("headerBottomEnd",reqParam.get("headerBottomEnd"));
					parameterMap.put("multiRowStartIndex",reqParam.get("multiRowStartIndex"));
					parameterMap.put("multiRowEndIndex",reqParam.get("multiRowEndIndex"));
					parameterMap.put("multiRowCopyStartIndex",reqParam.get("multiRowCopyStartIndex"));
					parameterMap.put("multiRowCopyEndIndex",reqParam.get("multiRowCopyEndIndex"));
					parameterMap.put("multiRowDataListMapKey",reqParam.get("multiRowDataListMapKey"));
					parameterMap.put("rowRightEnd", reqParam.get("rowRightEnd"));
				}
				// TXT下載
				else
				{
					parameterMap.put("txtHeaderBottomEnd",reqParam.get("txtHeaderBottomEnd"));
					parameterMap.put("txtMultiRowStartIndex",reqParam.get("txtMultiRowStartIndex"));
					parameterMap.put("txtMultiRowEndIndex",reqParam.get("txtMultiRowEndIndex"));
					parameterMap.put("txtMultiRowCopyStartIndex",reqParam.get("txtMultiRowCopyStartIndex"));
					parameterMap.put("txtMultiRowCopyEndIndex",reqParam.get("txtMultiRowCopyEndIndex"));
					parameterMap.put("txtMultiRowDataListMapKey",reqParam.get("txtMultiRowDataListMapKey"));
					parameterMap.put("txtHasFooter",reqParam.get("txtHasFooter"));
				}
				bs.addData("parameterMap", parameterMap);
			}
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("checkingCashedDirectDownload error >> {}", e);
		}
		return bs;
	}
		
	/**
	 * 取得綜合存款帳號
	 * 
	 * @param cusidn
	 * @return
	 */
	public BaseResult getCollateralAcn(String cusidn) {
		log.trace("Acct_Service.getCollateralAcn...");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = N920_REST(cusidn, COLLATER);
			
			Map<String,Object> dataMap = (Map<String, Object>) bs.getData();
			List<Map<String,String>> recList = (List<Map<String,String>>) dataMap.get("REC");
			
			// 只回傳綜合存款帳號帳號
			List<String> acnList = new ArrayList<String>();
			
			for (Map<String, String> map : recList) {
				if(!map.get("ACN").substring(3, 6).equals("646") && !map.get("ACN").substring(3, 6).equals("647")) {
					String acn = map.get("ACN");
					acnList.add(acn);
				}
			}
			bs.setData(acnList);
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getCollateralAcn error >> {}", e);
		}
		return bs;
	}
	
	/**
	 * 台幣綜存質借取消輸入頁
	 * 
	 * @param sessionId
	 * @return
	 */
	public BaseResult collateral(Map<String, String> reqParam) {
		log.trace("collateral");
		BaseResult bs = null;
		try {
			bs = new BaseResult();			
			bs = N108_REST(reqParam); 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	
	/**
	 * 台幣綜存質借申請/取消確認頁
	 * 
	 * @param sessionId
	 * @return
	 */
	public BaseResult collateral_confirm(String cusidn, Map<String, String> reqParam) {
		log.trace(ESAPIUtil.vaildLog("collateral_confirm.reParam>>" + CodeUtil.toJson(reqParam))); 
		BaseResult bs = null;
		
		try {
			bs = new BaseResult();
			bs = getTxToken();
			if(bs.getResult()) {
				reqParam.put("TXTOKEN",((Map<String,String>) bs.getData()).get("TXTOKEN"));
				bs.setData(reqParam);
				bs.setResult(Boolean.TRUE);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}

	/**
	 * 台幣綜存質借申請/取消結果頁
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult collateral_result(Map<String, String> reqParam) {

		log.trace(ESAPIUtil.vaildLog("collateral_result.cusidn >> " + reqParam.get("CUSIDN"))); 
		BaseResult bs = null;
		try {
			
			bs = new BaseResult();
		
			bs = N108_REST(reqParam);		
		} catch (Exception e) {
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	/**
	 * 虛擬帳號入帳明細
	 * 
	 * @param params
	 * @return
	 */
	public BaseResult virtual_account_detail_result(String cusidn, Map<String, String> reqParam) {

		log.trace("virtual_account_detail_result");
		BaseResult bs = null;
		try {
			String acn = "ALL".equals(reqParam.get("ACN")) ? "" : reqParam.get("ACN");
			bs = new BaseResult();
			bs = N615_REST(cusidn, acn, reqParam);
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				for (Map<String, String> row : rows) 
				{
					// 交易日期
					row.put("LSTLTD_c", DateUtil.convertDate(2, row.get("LSTLTD"), "yyyMMdd", "yyy/MM/dd"));
					// 金額
					row.put("AMTTRN_c", NumericUtil.fmtAmount(row.get("AMTTRN"), 2));
					//入帳時間
					row.put("LSTIME_c",DateUtil.formatTime(row.get("LSTIME")));
					//備註
					if(row.get("TRNSRC").equals("NB")) {
						row.put("TRNSRC_C",i18n.getMsg("LB.Internet_banking"));//網路銀行
					}
					else if(row.get("TRNSRC").equals("VO")) {
						row.put("TRNSRC_C",i18n.getMsg("LB.X1800"));//電話銀行
					}
					else if(row.get("TRNSRC").equals("RM")) {
						row.put("TRNSRC_C",i18n.getMsg("LB.X1801"));//匯款
					}
					else if(row.get("TRNSRC").equals("ATM")) {
						row.put("TRNSRC_C",i18n.getMsg("LB.X1802"));//自動櫃員機
					}
					else if(row.get("TRNSRC").equals("TABS")) {
						row.put("TRNSRC_C",i18n.getMsg("LB.X1803"));//臨櫃繳款
					}
					else {
						row.put("TRNSRC_C"," ");
					}
				}
				//移除空入帳帳號
				for (int i = 0;i < rows.size();i++) {
					log.debug("ACN >> {}",rows.get(i).get("ACN"));
					if(rows.get(i).get("ACN").equals("")) {
						rows.remove(i);
						i--;
					}
				}
				//遇到繼續查詢
				String CMSDATE = reqParam.get("CMSDATE");
				String CMEDATE = reqParam.get("CMEDATE");
				String USERDATA_X100 = reqParam.get("USERDATA_X100");
				bs.addData("CMSDATE",CMSDATE);
				bs.addData("CMEDATE",CMEDATE);
				bs.addData("USERDATA_X100",USERDATA_X100);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("virtual_account_detail_result error >> {}", e);
		}
		return bs;
	}
	
	/**
	 * 取得臺幣存款帳號
	 * @param cusidn
	 * @param type
	 * @return
	 */
	public BaseResult getACNO_List(String cusidn) {
		log.trace("cusidn>>{} ",cusidn);
		BaseResult bs = null;
		Map<String,Object> tmpData = null;
		List<String> acnoList = new ArrayList<String>();
		List<Map<String,String>> acnoList2 = null;
		try {
			//  CALL WS api
			bs =new  BaseResult();
			
			//N920的臺幣存款帳號清單
			bs = N920_REST(cusidn,ACNO);
			
			//把REC轉出帳號轉成ajax需要的格式
			if(bs!=null && bs.getResult()) {
				tmpData = (Map) bs.getData();
				acnoList2 = (List<Map<String,String>>) tmpData.get("REC");

				for(Map<String,String> acMap :acnoList2) {
					String ACN = acMap.get("ACN");
					acnoList.add(ACN);
				}
				
				bs.setData(acnoList);

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			//Avoid Information Exposure Through an Error Message
			log.error("getACNO_List error >> {}",e);
		}
		//return retMap;
		return bs;
	}

	public BaseResult getBranchList() {
		log.trace("common_acct_setting start");
		BaseResult bs = new BaseResult();
		List<ADMBRANCH> branchList = null;
		try {
			branchList = admBranch_Dao.getAll();
//			//TODO:測試其他DAO用
//			List<ADMBANK> bankList = admBankDao.getAll();
			Map<String, Object> data = new HashMap<String, Object>();
			ArrayList<Object> allData = new ArrayList<Object>();
			log.debug("branch list size >> {}",branchList.size());
			for(ADMBRANCH list:branchList) {
				Map<String,String> row = new HashMap<String, String>();
				row.put("ADBRANCHID", list.getADBRANCHID());
	        	Locale currentLocale = LocaleContextHolder.getLocale();
	    		String locale = currentLocale.toString();
                switch (locale) {
				case "en":
					row.put("ADBRANCHNAME", list.getADBRANENGNAME());
					break;
				case "zh_TW":
					row.put("ADBRANCHNAME", list.getADBRANCHNAME());
					break;
				case "zh_CN":
					row.put("ADBRANCHNAME", list.getADBRANCHSNAME());
					break;
                }
				allData.add(row);
			}
			data.put("data", allData);
//			log.debug("bankList list size >> {}",bankList.size());
//			CodeUtil.convert2BaseResult(bs, data);
			bs.addData("data", data);
			bs.setResult(true);
		} catch (Exception e) {
			log.error("export_bill_query_result",e);
		}	
		return bs;
	}
	
	//託收票據明細結果頁
	public BaseResult collection_bills_result(String cusidn, Map<String, String> reqParam) {
		log.trace("collection_bills_result_service");
		BaseResult bs = null ;
		try {
			bs = new  BaseResult();
			//	 call ws API
			bs = N860_REST(cusidn, reqParam);
			log.trace("TEST"+bs);
			if(bs!=null) {
				Map<String , Object> callRow = (Map) bs.getData();
				ArrayList<Map<String , Object>> callTable = (ArrayList<Map<String , Object>>) callRow.get("REC");
				
				for (Map<String, Object> table : callTable) {
					//如果msgCode =="0" 表示TABLE正常
					if ("0".equals(table.get("msgCode"))) {
						ArrayList<Map<String , String>> callTableList = (ArrayList<Map<String , String>>) table.get("TABLE");
	
						for (Map<String, String> data : callTableList) {
						//欄位格式化
							data.put("AMOUNT",NumericUtil.fmtAmount(data.get("AMOUNT"),2));
							data.put("REMAMT",NumericUtil.fmtAmount(data.get("REMAMT"),2));
							data.put("VALAMT",NumericUtil.fmtAmount(data.get("VALAMT"),2));
							data.put("SHOWDUEDATE",DateUtil.convertDate(0,data.get("DUEDATE"),"yyyMMdd","yyy/MM/dd"));
							data.put("SHOWVALDATE",DateUtil.convertDate(0,data.get("VALDATE"),"yyyMMdd","yyy/MM/dd"));
							data.put("SHOWREMDATE",DateUtil.convertDate(0,data.get("REMDATE"),"yyyMMdd","yyy/MM/dd"));
							data.put("SPACE","");
							
						}
					}else {
						Map<String, String> tableMap = new LinkedHashMap<>();
						List<Map<String, String>> tableListMap = new LinkedList<Map<String, String>>();
						tableMap.put("COLLBRH", String.valueOf(table.get("msgCode")));
						tableMap.put("SHOWREMDATE", String.valueOf(getMessageByMsgCode((String)table.get("msgCode"))));
						tableListMap.add(tableMap);
						table.put("TABLE", tableListMap);
					}
				}
				log.trace(ESAPIUtil.vaildLog("ACN >> " + reqParam.get("ACN")));
				callRow.put("REMCNT", NumericUtil.fmtAmount((String)callRow.get("REMCNT"),0));
				callRow.put("REMAMT", NumericUtil.fmtAmount((String)callRow.get("REMAMT"),2));
				callRow.put("VALCNT", NumericUtil.fmtAmount((String)callRow.get("VALCNT"),0));
				callRow.put("VALAMT", NumericUtil.fmtAmount((String)callRow.get("VALAMT"),2));
				bs.addData("ACN", reqParam.get("ACN"));
				bs.addData("CMSDATE2", reqParam.get("CMSDATE2"));
				bs.addData("CMEDATE2", reqParam.get("CMEDATE2"));
				bs.addData("CMSDATE3", reqParam.get("CMSDATE3"));
				bs.addData("CMEDATE3", reqParam.get("CMEDATE3"));
				bs.addData("CMSDATE4", reqParam.get("CMSDATE4"));
				bs.addData("CMEDATE4", reqParam.get("CMEDATE4"));
				bs.addData("FGBRHCOD", reqParam.get("FGBRHCOD"));
				bs.addData("DPBHNO", reqParam.get("DPBHNO"));
				bs.addData("FGTYPE", reqParam.get("FGTYPE"));
				bs.addData("FGPERIOD", reqParam.get("FGPERIOD"));
				Map<String, String> argBean = SpringBeanFactory.getBean("N860_TYPE");
				if(argBean.containsKey((String)callRow.get("TYPE")))
				{
					String TYPE = argBean.get((String)callRow.get("TYPE"));
					bs.addData("TYPE_STR",i18n.getMsg(TYPE));
				}
				bs.addData("ACN_TEXT", reqParam.get("ACN").equals("")? i18n.getMsg("LB.All"):reqParam.get("ACN"));
			}
		} catch (Exception e) {
			log.error("export_bill_query_result >> {}",e);
		}	
		return  bs;
	}
	
	//託收票據明細直接下載
	public BaseResult CollectionBillsdirectDownload(String cusidn,Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			bs = collection_bills_result(cusidn, reqParam);
			if (bs != null) {
				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap={}", dataMap);
				ArrayList<Map<String, Object>> rowListMap = (ArrayList<Map<String, Object>>) dataMap.get("REC");
				log.debug("rowListMap={}", rowListMap);
				Map<String, Object> parameterMap = new HashMap<String, Object>();
				
				parameterMap.put("CMQTIME", dataMap.get("CMQTIME"));
				parameterMap.put("ACN", dataMap.get("ACN"));
				parameterMap.put("ACN_TEXT", dataMap.get("ACN_TEXT"));
				parameterMap.put("REMCNT", dataMap.get("REMCNT"));
				parameterMap.put("REMAMT", dataMap.get("REMAMT"));
				parameterMap.put("VALCNT", dataMap.get("VALCNT"));
				parameterMap.put("VALAMT", dataMap.get("VALAMT"));
				parameterMap.put("TYPE", dataMap.get("TYPE"));
				parameterMap.put("TYPE_STR", dataMap.get("TYPE_STR"));
				parameterMap.put("CMPERIOD", dataMap.get("CMPERIOD"));

				parameterMap.put("downloadFileName", reqParam.get("downloadFileName"));
				parameterMap.put("downloadType", reqParam.get("downloadType"));
				parameterMap.put("templatePath", reqParam.get("templatePath"));
				parameterMap.put("hasMultiRowData", reqParam.get("hasMultiRowData"));

				String downloadType = reqParam.get("downloadType");
				log.trace(ESAPIUtil.vaildLog("downloadType >> " + downloadType));

				// EXCEL和OLDEXCEL下載
				if ("EXCEL".equals(downloadType) || "OLDEXCEL".equals(downloadType)) {
					parameterMap.put("headerRightEnd", reqParam.get("headerRightEnd"));
					parameterMap.put("headerBottomEnd", reqParam.get("headerBottomEnd"));
					parameterMap.put("multiRowStartIndex", reqParam.get("multiRowStartIndex"));
					parameterMap.put("multiRowEndIndex", reqParam.get("multiRowEndIndex"));
					parameterMap.put("multiRowCopyStartIndex", reqParam.get("multiRowCopyStartIndex"));
					parameterMap.put("multiRowCopyEndIndex", reqParam.get("multiRowCopyEndIndex"));
					parameterMap.put("multiRowDataListMapKey", reqParam.get("multiRowDataListMapKey"));
					parameterMap.put("rowRightEnd", reqParam.get("rowRightEnd"));
				}
				// OLDTXT
				else if("OLDTXT".equals(downloadType)) {
					

					for (Map<String, Object> table : rowListMap) {
						//如果msgCode =="0" 表示TABLE正常
						if (!"0".equals(table.get("msgCode"))) {
							LinkedList<Map<String , String>> callTableList = (LinkedList<Map<String , String>>) table.get("TABLE");
							callTableList.clear();
						}
					}
					parameterMap.put("txtMultiRowDataListMapKey", reqParam.get("txtMultiRowDataListMapKey"));
					if(dataMap.get("TYPE").equals("01") || dataMap.get("TYPE").equals("03")) {
						parameterMap.put("templatePath", reqParam.get("templatePath") + "_1.txt");
					}else if( dataMap.get("TYPE").equals("02") || dataMap.get("TYPE").equals("05") ||
								dataMap.get("TYPE").equals("06") || dataMap.get("TYPE").equals("07") ) {
						parameterMap.put("templatePath", reqParam.get("templatePath") + "_2.txt");
					}else if( dataMap.get("TYPE").equals("04") || dataMap.get("TYPE").equals("08") ||
							dataMap.get("TYPE").equals("09") || dataMap.get("TYPE").equals("10") ) {
						parameterMap.put("templatePath", reqParam.get("templatePath") + "_3.txt");
					}
				}
				// TXT下載
				else {
					parameterMap.put("txtHeaderBottomEnd", reqParam.get("txtHeaderBottomEnd"));
					parameterMap.put("txtMultiRowStartIndex", reqParam.get("txtMultiRowStartIndex"));
					parameterMap.put("txtMultiRowEndIndex", reqParam.get("txtMultiRowEndIndex"));
					parameterMap.put("txtMultiRowCopyStartIndex", reqParam.get("txtMultiRowCopyStartIndex"));
					parameterMap.put("txtMultiRowCopyEndIndex", reqParam.get("txtMultiRowCopyEndIndex"));
					parameterMap.put("txtMultiRowDataListMapKey", reqParam.get("txtMultiRowDataListMapKey"));
				}
				dataMap.put("parameterMap", parameterMap);
			}
		
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("CollectionBillsdirectDownload error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * 初步測試用 ，之後會移除
	 * 
	 * @return
	 */
	@Transactional(value = "nnb_transactionManager")
	public List<ADMBANK> getAll() {

		// List list_1 = admbank_Dao.getBySql("SELECT * FROM DB2INST1.ADMBANK");
		// log.trace("list_1>>"+list_1);
		List<ADMBANK> list_2 = admBankDao.getAll();
		for (ADMBANK po : list_2) {
			try {
				log.trace(ESAPIUtil.vaildLog("po>>>" + BeanUtils.describe(po)));
			} catch (IllegalAccessException e) {
				//Avoid Information Exposure Through an Error Message
				// TODO Auto-generated catch block
				//e.printStackTrace();
				log.error("getAll errror >> {}",e);
			} catch (InvocationTargetException e) {
				//Avoid Information Exposure Through an Error Message
				// TODO Auto-generated catch block
				//e.printStackTrace();
				log.error("getAll errror >> {}",e);
			} catch (NoSuchMethodException e) {
				//Avoid Information Exposure Through an Error Message
				// TODO Auto-generated catch block
				//e.printStackTrace();
				log.error("getAll errror >> {}",e);
			}
		}
		return list_2;
	}

	@Transactional(value = "nnb_transactionManager")
	public ADMBANK get() {
		ADMBANK po = new ADMBANK();
		po.setADBANKID("612");
		po = admBankDao.get(ADMBANK.class, po.getADBANKID());
		log.trace("po ADBANKNAME {}", po.getADBANKNAME());
		return po;
	}
	//下載舊txt檔，將數字負號移至後面
	//type 1為如果數字為正，後面加空白，type 2為如果數字為正，後面加加號
	public String format(String input, int type) {
		log.trace("input",input);
		log.trace("type",type);
		if(type == 1) {
			if(input.indexOf("-")!=-1) {
				input = input.replaceAll("-","");
				input+="-";
			}
			else {
				input+=" ";
			}
		}
		else if(type == 2) {
			if(input.indexOf("-")!=-1) {
				input = input.replaceAll("-","");
				input+="-";
			}
			else {
				input+="+";
			}
		}
		
		return input;
	}
	
	/**
	 * 數位存款帳戶存摺封面下載
	 * 
	 * @param params
	 * @return
	 */
	public BaseResult digital_deposit(String cusidn) {
		
		log.trace("digital_deposit Service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = N205_REST(cusidn);
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("virtual_account_detail_result error >> {}", e);
		}
		return bs;
	}
	
	/**
	 * 數位存款帳戶存摺封面下載
	 * 
	 * @param params
	 * @return
	 */
	public BaseResult digital_deposit_result(String acn, Map<String, Object> reqParam) {

		log.trace("digital_deposit_result");
		List<Map<String,String>> rows = (List<Map<String,String>>) reqParam.get("REC");
		log.debug("rows" + rows);
		BaseResult bs = null;
		try {
			String BRANCH = "";
			bs = new BaseResult();
			for (int i = 0;i < rows.size();i++) {
				if(rows.get(i).get("ACN").equals(acn)) {
					bs.addAllData(rows.get(i));
					bs.setResult(true);
				}
			}
			log.debug("BS DATA>>>"+bs.getData());
//			bs = N205_REST(reqParam);
			if(bs != null && bs.getResult()) {
				Map<String, String> bsData = (Map) bs.getData();
				log.debug("find ADMBRANCH");
				List<ADMBRANCH> branch = admBranch_Dao.findByBhID(bsData.get("BRHACC"));
				if (branch != null) {
					BRANCH = branch.get(0).getADBRANCHNAME();
				}
				bs.addData("BRANCH", BRANCH);
				String Acn = bsData.get("ACN");
				String acno = Acn.substring(0,3) + "-" + Acn.substring(3,5) + "-" + Acn.substring(5);
				bs.addData("ACNO", acno);
				String sdt = DateUtil.convertDate(1, bsData.get("SDT"), "yyyMMdd", "yyyy/MM/dd");
				bs.addData("SDTformat", sdt);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("virtual_account_detail_result error >> {}", e);
		}
		return bs;
	}
	
	/**
	 * 數位存款帳戶存摺封面下載
	 * @param requestParam
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/downloadDigitalDepositPDF")
	public void downloadDigitalDepositPDF(Map<String, String> requestParam, HttpServletResponse response) {
		log.debug("Do downloadDigitalDepositPDF");
		String filename = "Digital_Deposit.pdf";
		
		try {
			
			String name = requestParam.get("NAME");
			String name1 = "";
			if(name.length() > 25) {
				name1 = name.substring(25);
				name = name.substring(0, 25);
			}
			int i = 685;
			
			String mimeType = "application/octet-stream";
			response.setContentType(mimeType);
			response.setHeader("Content-Disposition","attachment;filename="+filename);
			
			BaseFont baseFont = BaseFont.createFont("MHei-Medium","UniCNS-UCS2-H",BaseFont.NOT_EMBEDDED);
			Font fontBig = new Font(baseFont, 12, Font.BOLD);
		    //Step 1—Create a Document.  
			Document document = new Document();  
			//Step 2—Get a PdfWriter instance.  
			ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
			PdfWriter writer = PdfWriter.getInstance(document, byteStream);
			//Step 3—Open the Document.  
			document.open();  
			
			PdfContentByte cb = writer.getDirectContent();
			
			//Step 4—Add content.  
			String headPicture = context.getRealPath("") + "/com/images/digital_deposit_demo.png";
			Image image = Image.getInstance(headPicture);
			float documentWidth = document.getPageSize().getWidth() - document.leftMargin() - document.rightMargin();
			float documentHeight = documentWidth / 580 * 320;//重新設定寬高
			image.scaleAbsolute(documentWidth, documentHeight);//重新設定寬高
			image.setAlignment(Image.UNDERLYING);
			document.add(image);
			
			cb.saveState();
			cb.beginText();
			cb.moveText(92, i); //設置文字的位置685
			cb.setFontAndSize(baseFont, 11); //設置文字的大小
			cb.setColorFill(BaseColor.WHITE); //設置文字顏色
			cb.showText("開  戶   日  期：" + requestParam.get("SDTformat")); //設置文字內容
			cb.endText();
			cb.restoreState();
			
			i = i - 15;
			cb.saveState();
			cb.beginText();
			cb.moveText(92, i); //設置文字的位置670
			cb.setFontAndSize(baseFont, 11); //設置文字的大小
			cb.setColorFill(BaseColor.WHITE); //設置文字顏色
			cb.showText("戶　　　　名：" + name); //設置文字內容
			cb.endText();
			cb.restoreState();
			
			if(!name1.equals("")) {
				i = i - 15;
				cb.saveState();
				cb.beginText();
				cb.moveText(92, i); //設置文字的位置655
				cb.setFontAndSize(baseFont, 11); //設置文字的大小
				cb.setColorFill(BaseColor.WHITE); //設置文字顏色
				cb.showText("　　　　　　　" + name1); //設置文字內容
				cb.endText();
				cb.restoreState();
			}
			
			i = i - 30;
			cb.saveState();
			cb.beginText();
			cb.moveText(92, i); //設置文字的位置640
			cb.setFontAndSize(baseFont, 11); //設置文字的大小
			cb.setColorFill(BaseColor.WHITE); //設置文字顏色
			cb.showText("銀  行   代  號：050"); //設置文字內容
			cb.endText();
			cb.restoreState();
			
			i = i - 15;
			cb.saveState();
			cb.beginText();
			cb.moveText(92, i); //設置文字的位置625
			cb.setFontAndSize(baseFont, 11); //設置文字的大小
			cb.setColorFill(BaseColor.WHITE); //設置文字顏色
			cb.showText("分支機構代號：" + requestParam.get("TAXGVID") + "（" + requestParam.get("BRANCH") + "）"); //設置文字內容
			cb.endText();
			cb.restoreState();
			
			i = i - 15;
			cb.saveState();
			cb.beginText();
			cb.moveText(92, i); //設置文字的位置610
			cb.setFontAndSize(baseFont, 11); //設置文字的大小
			cb.setColorFill(BaseColor.WHITE); //設置文字顏色
			cb.showText("帳　　　　號：" + requestParam.get("ACNO")); //設置文字內容
			cb.endText();
			cb.restoreState();
			
			//Step 5—Close the Document.  
			document.close();
			
			byte[] buffer = byteStream.toByteArray();
			OutputStream fos = response.getOutputStream();
			fos.write(buffer);
			fos.close();
			
		} catch (Exception e) {
			log.error("", e);
		}
	}
	
	/**
	 * 帳戶明細查詢PDF下載
	 * @param requestParam
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/downloadDemandDepositDetailPDF")
	public void downloadDemandDepositDetailPDF(HttpServletResponse response, List<Map<String,Object>> dataListMap, Map<String, String> reqParam) {
		log.debug("IN DemandDepositDetail");
		String filename = "Demand_Deposit_Detail.pdf";
		try {
			
			String mimeType = "application/octet-stream";
			response.setContentType(mimeType);
			response.setHeader("Content-Disposition","attachment;filename="+filename);
			
			BaseFont baseFont = BaseFont.createFont("MHei-Medium","UniCNS-UCS2-H",BaseFont.NOT_EMBEDDED);
			Font fontBig = new Font(baseFont, 10, Font.NORMAL);
			Font fontC = new Font(baseFont, 8, Font.NORMAL);
			log.debug("DOCUMENT");
			Document document = new Document(PageSize.A4);  
			ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
			PdfWriter writer = PdfWriter.getInstance(document, byteStream);
			//表格欄位比較寬 內容較不會被折行
			document.setMargins(20, 20, 105, 60);
			//文件要求的間距 內容容易折行
//			document.setMargins(50, 50, 105, 60);
			document.open(); 
			
			Paragraph T = new Paragraph();//新增一個段落，類似html的<p>
			T.add(new Chunk(i18n.getMsg("LB.X2475"), fontBig)); //新增一個字串然後加到Paragraph
			T.setSpacingAfter(10); //設定Space
			document.add(T);
			
			T = new Paragraph();//新增一個段落，類似html的<p>
			T.add(new Chunk(i18n.getMsg("LB.Inquiry_time") + "：" + reqParam.get("CMQTIME").toString(), fontC)); //新增一個字串然後加到Paragraph
			T.setSpacingAfter(10); //設定Space
			document.add(T);
			
			T = new Paragraph();
			T.add(new Chunk(i18n.getMsg("LB.Inquiry_period") + "：" + reqParam.get("CMPERIOD").toString(), fontC));
			T.setSpacingAfter(10);
			document.add(T);
			
			T = new Paragraph();
			T.add(new Chunk(i18n.getMsg("LB.Total_records") + "：" + reqParam.get("COUNT").toString() + " " +i18n.getMsg("LB.Rows"), fontC));
			T.setSpacingAfter(30);
			document.add(T);
			
			log.debug("Insert Table Data Start");
			for(int i=0;i<dataListMap.size();i++) {
				Paragraph p=new Paragraph();//新增一個段落，類似html的<p>
				p.add(new Chunk(i18n.getMsg("LB.Account") + "：" + dataListMap.get(i).get("ACN").toString(), fontC)); //新增一個字串然後加到Paragraph
//				p.setSpacingBefore(30); //設定Space
				p.setSpacingAfter(10); //設定Space
				document.add(p);
				
				PdfPTable table = new PdfPTable(9);//初始化Table然後指定欄位數目
				
				table.setWidthPercentage(100.0f); //設定Table填滿至頁面Margin
				table.setWidths(new int[] {1,1,2,2,2,2,2,1,1});//設定相對欄寬
//				table.setWidths(new int[] {1,1,1,1,1,1,1,1,1});//設定相對欄寬
				
				PdfPCell cell = new PdfPCell(); //初始化Cell(欄位)
				cell.setPhrase(new Phrase(i18n.getMsg("LB.Change_date"), fontC)); //填入Cell欄位字串，要new一個Phrase
				cell.setHorizontalAlignment(Element.ALIGN_CENTER); //置中對齊
				table.addCell(cell); //cell加到table中
				
				cell.setPhrase(new Phrase(i18n.getMsg("LB.Summary_1"), fontC)); //填入Cell欄位字串，要new一個Phrase
				cell.setHorizontalAlignment(Element.ALIGN_CENTER); //置中對齊
				table.addCell(cell); //cell加到table中
				
				cell.setPhrase(new Phrase(i18n.getMsg("LB.Withdrawal_amount"), fontC)); //填入Cell欄位字串，要new一個Phrase
				cell.setHorizontalAlignment(Element.ALIGN_CENTER); //置中對齊
				table.addCell(cell); //cell加到table中
				
				cell.setPhrase(new Phrase(i18n.getMsg("LB.Deposit_amount"), fontC)); //填入Cell欄位字串，要new一個Phrase
				cell.setHorizontalAlignment(Element.ALIGN_CENTER); //置中對齊
				table.addCell(cell); //cell加到table中
				
				cell.setPhrase(new Phrase(i18n.getMsg("LB.Account_balance_2"), fontC)); //填入Cell欄位字串，要new一個Phrase
				cell.setHorizontalAlignment(Element.ALIGN_CENTER); //置中對齊
				table.addCell(cell); //cell加到table中
				
				cell.setPhrase(new Phrase(i18n.getMsg("LB.Checking_account"), fontC)); //填入Cell欄位字串，要new一個Phrase
				cell.setHorizontalAlignment(Element.ALIGN_CENTER); //置中對齊
				table.addCell(cell); //cell加到table中
				
				cell.setPhrase(new Phrase(i18n.getMsg("LB.Data_content"), fontC)); //填入Cell欄位字串，要new一個Phrase
				cell.setHorizontalAlignment(Element.ALIGN_CENTER); //置中對齊
				table.addCell(cell); //cell加到table中
				
				cell.setPhrase(new Phrase(i18n.getMsg("LB.Receiving_Bank"), fontC)); //填入Cell欄位字串，要new一個Phrase
				cell.setHorizontalAlignment(Element.ALIGN_CENTER); //置中對齊
				table.addCell(cell); //cell加到table中
				
				cell.setPhrase(new Phrase(i18n.getMsg("LB.Trading_time"), fontC)); //填入Cell欄位字串，要new一個Phrase
				cell.setHorizontalAlignment(Element.ALIGN_CENTER); //置中對齊
				table.addCell(cell); //cell加到table中
			
				List<Map<String, String>> list = (List<Map<String, String>>) dataListMap.get(i).get("rowListMap");
				for(int j=0; j<list.size();j++) {
					cell.setPhrase(new Phrase(list.get(j).get("SHOWLSTLTD"), fontC));
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					table.addCell(cell);
					
					cell.setPhrase(new Phrase(list.get(j).get("MEMO_C"), fontC));
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					table.addCell(cell);
					
					cell.setPhrase(new Phrase(list.get(j).get("DPWDAMTfmt"), fontC));
					cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					table.addCell(cell);
					
					cell.setPhrase(new Phrase(list.get(j).get("DPSVAMTfmt"), fontC));
					cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					table.addCell(cell);
					
					cell.setPhrase(new Phrase(list.get(j).get("BALfmt"), fontC));
					cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					table.addCell(cell);
					
					cell.setPhrase(new Phrase(list.get(j).get("CHKNUM"), fontC));
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					table.addCell(cell);
					
					cell.setPhrase(new Phrase(list.get(j).get("DATA16"), fontC));
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					table.addCell(cell);
					
					cell.setPhrase(new Phrase(list.get(j).get("TRNBRH"), fontC));
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					table.addCell(cell);
					
					cell.setPhrase(new Phrase(list.get(j).get("SHOWTRNTIME"), fontC));
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					table.addCell(cell);
				}
				
				document.add(table);
			}
			
			//Step 5—Close the Document.  
			document.close();
			log.debug("Insert Table Data End");
			
			byte[] buffer = byteStream.toByteArray();
			
			PdfReader reader = new PdfReader(buffer);
			OutputStream fos = response.getOutputStream();
			PdfStamper stamp = new PdfStamper(reader, fos);
	 
			PdfContentByte under;
			
			//插入背景圖片
			log.debug("Do add image");
			String headPicture = context.getRealPath("") + "/com/images/pdfbackground.jpg";
			Image image = Image.getInstance(headPicture);
			float documentWidth = document.getPageSize().getWidth();
			float documentHeight = document.getPageSize().getHeight();//重新設定寬高
			image.scaleAbsolute(documentWidth, documentHeight);//重新設定寬高
			image.setAlignment(Image.UNDERLYING);
			image.setAbsolutePosition(0, 0);
			
			int pageSize = reader.getNumberOfPages();// 原pdf文件的总页数
			//迴圈將所有頁面加入背景圖
			for (int i = 1; i <= pageSize; i++) {
				under = stamp.getUnderContent(i);// 水印在之前文本下
				under.addImage(image);// 图片水印
			}
	 
			stamp.close();
			
		} catch (Exception e) {
			log.error("", e);
		}
	}
}
