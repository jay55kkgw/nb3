package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N09302_IDGATE_DATA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -469307378902247253L;

	@SerializedName(value = "CUSIDN")
	private String CUSIDN;
	
	@SerializedName(value = "TRNTYP")
	private String TRNTYP;
	
	@SerializedName(value = "TRNCOD")
	private String TRNCOD;
	
	@SerializedName(value = "ACN")
	private String ACN;
	
	@SerializedName(value = "SVACN")
	private String SVACN;
	
	@SerializedName(value = "AMT_06_N")
	private String AMT_06_N;
	
	@SerializedName(value = "AMT_16_N")
	private String AMT_16_N;
	
	@SerializedName(value = "AMT_26_N")
	private String AMT_26_N;

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getTRNTYP() {
		return TRNTYP;
	}

	public void setTRNTYP(String tRNTYP) {
		TRNTYP = tRNTYP;
	}

	public String getTRNCOD() {
		return TRNCOD;
	}

	public void setTRNCOD(String tRNCOD) {
		TRNCOD = tRNCOD;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	public String getSVACN() {
		return SVACN;
	}

	public void setSVACN(String sVACN) {
		SVACN = sVACN;
	}

	public String getAMT_06_N() {
		return AMT_06_N;
	}

	public void setAMT_06_N(String aMT_06_N) {
		AMT_06_N = aMT_06_N;
	}

	public String getAMT_16_N() {
		return AMT_16_N;
	}

	public void setAMT_16_N(String aMT_16_N) {
		AMT_16_N = aMT_16_N;
	}

	public String getAMT_26_N() {
		return AMT_26_N;
	}

	public void setAMT_26_N(String aMT_26_N) {
		AMT_26_N = aMT_26_N;
	}

	
}
