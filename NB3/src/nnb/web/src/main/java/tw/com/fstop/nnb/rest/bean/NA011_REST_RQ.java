package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class NA011_REST_RQ extends BaseRestBean_LOAN implements Serializable {

	private static final long serialVersionUID = 8859733676256041186L;
	
	private String UID;
	private String CUSIDN;

	
	public String getUID() {
		return UID;
	}

	public void setUID(String uID) {
		UID = uID;
	}
	
	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
}
