package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N557_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3258067595406942396L;
	
	String CMRECNUM;		//查詢筆數
	String ABEND;			//結束代碼
	String REC_NO;			//筆數
	String CMPERIOD;		//查詢期間
	String CMQTIME;			//查詢時間
	String USERDATA_X50;	//本次未完資料KEY值

	//開狀金額
	//幣別
    String AMTRLGCCY_7;
    String AMTRLGCCY_6;
    String AMTRLGCCY_5;
    String AMTRLGCCY_4;
    String AMTRLGCCY_3;
    String AMTRLGCCY_2;
    String AMTRLGCCY_1;
    //該幣別筆數
    String FXSUBAMTRECNUM_7;
    String FXSUBAMTRECNUM_6;
    String FXSUBAMTRECNUM_5;
    String FXSUBAMTRECNUM_4;
    String FXSUBAMTRECNUM_3;
    String FXSUBAMTRECNUM_2;
    String FXSUBAMTRECNUM_1;
    //該幣別加總
    String FXSUBAMT_7;
    String FXSUBAMT_6;
    String FXSUBAMT_5;
    String FXSUBAMT_4;
    String FXSUBAMT_3;
    String FXSUBAMT_2;
    String FXSUBAMT_1;
    
    //信用狀餘額
	//幣別
    String BALRLGCCY_7;
    String BALRLGCCY_6;
    String BALRLGCCY_5;
    String BALRLGCCY_4;
    String BALRLGCCY_3;
    String BALRLGCCY_2;
    String BALRLGCCY_1;
    //該幣別總數
    String FXSUBBALRECNUM_7;
    String FXSUBBALRECNUM_6;
    String FXSUBBALRECNUM_5;
    String FXSUBBALRECNUM_4;
    String FXSUBBALRECNUM_3;
    String FXSUBBALRECNUM_2;
    String FXSUBBALRECNUM_1;
    //該幣別加總
    String FXSUBBAL_7;
    String FXSUBBAL_6;
    String FXSUBBAL_5;
    String FXSUBBAL_4;
    String FXSUBBAL_3;
    String FXSUBBAL_2;
    String FXSUBBAL_1;

    String QUERYNEXT;
    
    LinkedList<N557_REST_RSDATA> REC;

    public String getQUERYNEXT() {
		return QUERYNEXT;
	}
	public void setQUERYNEXT(String qUERYNEXT) {
		QUERYNEXT = qUERYNEXT;
	}
	public LinkedList<N557_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<N557_REST_RSDATA> rEC) {
		REC = rEC;
	}
	public String getCMRECNUM() {
		return CMRECNUM;
	}
	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}
	public String getABEND() {
		return ABEND;
	}
	public void setABEND(String aBEND) {
		ABEND = aBEND;
	}
	public String getREC_NO() {
		return REC_NO;
	}
	public void setREC_NO(String rEC_NO) {
		REC_NO = rEC_NO;
	}
	public String getCMPERIOD() {
		return CMPERIOD;
	}
	public void setCMPERIOD(String cMPERIOD) {
		CMPERIOD = cMPERIOD;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getUSERDATA_X50() {
		return USERDATA_X50;
	}
	public void setUSERDATA_X50(String uSERDATA_X50) {
		USERDATA_X50 = uSERDATA_X50;
	}
}
