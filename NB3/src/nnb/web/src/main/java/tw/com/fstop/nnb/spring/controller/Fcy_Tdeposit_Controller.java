package tw.com.fstop.nnb.spring.controller;

import java.net.URLEncoder;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.google.gson.Gson;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.idgate.bean.N178_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N178_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N177_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N177_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N175_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N175_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N174_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N174_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N830_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N830_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N831_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N831_IDGATE_DATA_VIEW;
import tw.com.fstop.nnb.aop.Dialog;
import tw.com.fstop.nnb.rest.bean.N178_REST_RQ;
import tw.com.fstop.nnb.rest.bean.N177_REST_RQ;
import tw.com.fstop.nnb.rest.bean.N174_REST_RQ;
import tw.com.fstop.nnb.service.Acct_Tdeposit_Service;
import tw.com.fstop.nnb.service.Fcy_Tdeposit_Service;
import tw.com.fstop.nnb.service.IdGateService;
import tw.com.fstop.nnb.service.Pay_Expense_Service;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({
		SessionUtil.PD, 
		SessionUtil.CUSIDN, 
		SessionUtil.XMLCOD, 
		SessionUtil.DPMYEMAIL,
		SessionUtil.FCY_DEPOSIT_TRANSFER_FINSH_TOKEN, 
		SessionUtil.FCY_TDEPOSIT_CANCEL_FINSH_TOKEN,
		SessionUtil.FCY_RENEWAL_APPLY_FINSH_TOKEN, 
		SessionUtil.FCY_ORDER_RENEWAL_FINSH_TOKEN,
		SessionUtil.STEP1_LOCALE_DATA, 
		SessionUtil.CONFIRM_LOCALE_DATA, 
		SessionUtil.RESULT_LOCALE_DATA,
		SessionUtil.BACK_DATA,
		SessionUtil.IDGATE_TRANSDATA
	})
@Controller
@RequestMapping(value = "/FCY/ACCT/TDEPOSIT")
public class Fcy_Tdeposit_Controller
{

	// jsp資料夾路徑 acct
	private final String JSPFOLDERPATH = "acct_fcy";

	// 錯誤頁路徑
	private final String ERRORJSP = "/error";

	Logger log = LoggerFactory.getLogger(this.getClass());

	// 外幣定存相關邏輯
	@Autowired
	private Fcy_Tdeposit_Service fcy_Tdeposit_Service;
	@Autowired
	private Pay_Expense_Service pay_Expense_Service;
	@Autowired
	private Acct_Tdeposit_Service acct_Tdeposit_Service;

	// idgate用
	@Autowired		 
	IdGateService idgateservice;
	
	/**
	 * 設定外幣定存解約，預約的系統日期
	 * @param model
	 */
	public void setSysDate(Model model)
	{
		model.addAttribute("nextSystemDate", DateUtil.getNextDay());
		model.addAttribute("nextYearDay", 	 DateUtil.getNextYearDay());
	}
	
	
	/**
	 * 取得傳出帳號幣別相關資料
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getCurrency_aj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult getCurrency(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		String str = "getCurrency_aj";
		log.trace("hi {}", str);
		log.trace(ESAPIUtil.vaildLog("reqParam>>" + CodeUtil.toJson(reqParam)));
		String acno = reqParam.get("acno");
		BaseResult bs = new BaseResult();
		bs.setResult(Boolean.FALSE);
		
		List<Map<String,String>> fxDepCRYList = fcy_Tdeposit_Service.getFxDepCRYList(acno);
		
		bs.setResult(Boolean.TRUE);
		String dataString = new Gson().toJson(fxDepCRYList,fxDepCRYList.getClass());
		log.debug(ESAPIUtil.vaildLog("dataString=" + dataString));
		
		bs.setData(dataString);
		return bs;
	}

	/**
	 * 要取得傳出帳號幣別餘額相關資料
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getACNO_Currency_Data_aj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult getACNO_Data(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		String str = "getACNO_Data";
		log.trace("hi {}", str);
		log.trace(ESAPIUtil.vaildLog("reqParam>>" + CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try
		{
			String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
			String acno = reqParam.get("acno");
			String cry = reqParam.get("cry");
			bs = fcy_Tdeposit_Service.findOenByN510(cusidn,acno,cry);
			log.trace("getACNO_Currency_Data_aj bs.getData>>{}", bs.getData());
		}
		catch (Exception e)
		{
			log.error("getACNO_Currency_Data_aj Error ", e);
		}

		return bs;

	}

	/**
	 * 轉入 外幣綜存定存進入頁!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_deposit_transfer")
	public String f_deposit_transfer(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("f_deposit_transfer Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		String urlId ="";
		try
		{
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			//if(okMap.containsKey("URLID")) {
			urlId = okMap.get("ACN");
			log.trace(ESAPIUtil.vaildLog("urlId: " + urlId));
			//}
			//回上頁的Flag
			if ("Y".equals(okMap.get("back"))) {
				model.addAttribute("bk_key", "Y");
			}else model.addAttribute("bk_key", "N");
			// 取得轉出帳號、取得轉入帳號
			bs = fcy_Tdeposit_Service.getOutAcnoAndAgreeAcno(cusidn);
			//是否在營業時間內
			bs.addData("IsHoliday",fcy_Tdeposit_Service.isFXHoliday());
			//是否通知本人信箱
			bs.addData("sendMe",pay_Expense_Service.chkContains(cusidn));
			// 預約日期預設為明天
			bs.addData("TOMORROW", DateUtil.getNextDay());
			bs.addData("NextYearDay", DateUtil.getNextYearDay());
			//IDGATE身分
	        String idgateUserFlag="N";		 
	        Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
	        try {		 
	     	   if(IdgateData==null) {
	     		   IdgateData = new HashMap<String, Object>();
	     	   }
	            BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
	            if(tmp.getResult()) {		 
	                idgateUserFlag="Y";                  		 
	            }		 
	            tmp.addData("idgateUserFlag",idgateUserFlag);		 
	            IdgateData.putAll((Map<String, String>) tmp.getData());
	            SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	        }catch(Exception e) {		 
	            log.debug("idgateUserFlag error {}",e);		 
	        }		 
	        model.addAttribute("idgateUserFlag",idgateUserFlag);
		}
		catch (Exception e)
		{
			log.error("f_deposit_transfer Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				bs.addData("urlID", urlId);
				model.addAttribute("f_deposit_transfer", bs);
				target = JSPFOLDERPATH + "/f_deposit_transfer";
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * ********************轉入 外幣綜存定存確認頁*******************
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_deposit_transfer_confirm")
	public String f_deposit_transfer_confirm(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("f_deposit_transfer_confirm Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		try
		{
			String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
			log.debug("cusidn={}",cusidn);
			reqParam.put("cusidn",cusidn);
			
			String XMLCOD = (String)SessionUtil.getAttribute(model,SessionUtil.XMLCOD,null);
			log.debug("XMLCOD={}",XMLCOD);
			reqParam.put("XMLCOD",XMLCOD);
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
			}
			else
			{
				bs = fcy_Tdeposit_Service.f_deposit_transfer_confirm(okMap);
				// 取得防止重送代碼
				Map<String, String> mapGetToken = (Map) fcy_Tdeposit_Service.getTxToken().getData();
				// 圖形驗證碼
				String cAPRCHA = mapGetToken.get("CAPTCHA");
				// 防止重送代碼TxToken
				String tXToken = mapGetToken.get("TXTOKEN");
				bs.addData("TXTOKEN", tXToken);
				bs.addData("CAPTCHA", cAPRCHA);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				// IDGATE transdata
				Map<String, String> result =(Map<String, String>) bs.getData();
	            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	    		String adopid = "N174";
	    		String amount =result.get("OUTCRY_TEXT")+NumericUtil.fmtAmount(result.get("AMOUNT"),0)+"."+result.get("AMOUNT_DIG");
	    		String title = "您有一筆-轉入 外幣綜存定存交易待確認,金額"+amount;
	        	if(IdgateData != null) {
		            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
		            	result.put("ADCURRENCY", result.get("OUTCRY_TEXT").substring(0, 3));//幣別帶入
		            	result.put("ADTXAMT", result.get("AMOUNT") + "." + result.get("AMOUNT_DIG"));// 轉出金額
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N174_IDGATE_DATA.class, N174_IDGATE_DATA_VIEW.class, result));
		                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);
		                log.trace("IdgateData >>"+CodeUtil.toJson(IdgateData));
		            }
	        	}
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
			}
			//回上頁用
			SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
		}
		catch (Exception e)
		{
			log.error("f_deposit_transfer Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{

				model.addAttribute("f_deposit_transfer_confirm", bs);
				target = JSPFOLDERPATH + "/f_deposit_transfer_confirm";
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * ********************轉入外幣綜存定存結果頁*******************
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_deposit_transfer_result")
	@Dialog(ADOPID = "N174")
	public String f_deposit_transfer_result(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("f_deposit_transfer_result Start ~~~~~~~~~~~~~~~~");
		
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		String pageToken = "";
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
//			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale){
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
			}
			else{
				String SessionFinshToken = (String) SessionUtil.getAttribute(model, SessionUtil.FCY_DEPOSIT_TRANSFER_FINSH_TOKEN, null);
				// Check Session Token
				pageToken = okMap.get("TXTOKEN");
				BaseResult checkToken = acct_Tdeposit_Service.validateToken(pageToken, SessionFinshToken);
				if (!checkToken.getResult()){
					bs = checkToken;
				}
				else{
					String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
					reqParam.put("CUSIDN",cusidn);
					reqParam.put("UID",cusidn);
					String DPMYEMAIL = (String) SessionUtil.getAttribute(model, SessionUtil.DPMYEMAIL, null);
					reqParam.put("DPMYEMAIL",DPMYEMAIL);
					//IDgate驗證		 
	                try {		 
	                    if(reqParam.get("FGTXWAY").equals("7")) {		 
	        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
	        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N174_IDGATE");
	        				reqParam.put("sessionID", (String)IdgateData.get("sessionid"));		 
	        				reqParam.put("txnID", (String)IdgateData.get("txnID"));
	        				reqParam.put("idgateID", (String)IdgateData.get("IDGATEID"));
	        				log.trace("IDGATE TRANS data >>"+CodeUtil.toJson(reqParam));
	                    }		 
	                }catch(Exception e) {		 
	                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
	                }  
					
					Map<String,Object>IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
					model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
	                
					bs = fcy_Tdeposit_Service.f_deposit_transfer_result(reqParam);

					// 將查過的資料放入session，待切換 locale 時讀取
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
				log.trace("bs.getResult>> {}", bs.getResult());
			}
		}
		catch (Exception e)
		{
			log.error("f_deposit_transfer_result Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				SessionUtil.addAttribute(model, SessionUtil.FCY_DEPOSIT_TRANSFER_FINSH_TOKEN, pageToken);
				model.addAttribute("f_deposit_transfer_result", bs);
				target = JSPFOLDERPATH + "/f_deposit_transfer_result";
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	
	/**
	 * N175 外幣綜存定存解約進入頁
	 * 在頁面使用hidden儲存電文查回的資料，資料型態為json字串，json字串代表此交易的帳號資料
	 * 當進行到下一流程(確認頁)會打新的電文，手動把新的值塞入json字串，其他頁面上格式化的值不必放到json字串
	 * 然後繼續往結果頁帶，直到交易完成
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_deposit_cancel")
	public String f_deposit_cancel(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.debug("f_deposit_cancel Start");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		try
		{
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			log.debug(ESAPIUtil.vaildLog("FDPNO >> {}" + okMap.get("FDPNO")));
			log.debug(ESAPIUtil.vaildLog("ACN >> {}"+ okMap.get("ACN530")));
			// 清除切換語系時暫存的資料
			cleanSession(model);
			// Get session value by session Key
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			// 取得解約清單
			bs = fcy_Tdeposit_Service.f_deposit_cancel(cusidn,okMap);		
			// 設定系統時間
			setSysDate(model);
			//IDGATE身分
			String idgateUserFlag="N";
			Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			try {		 
	       	    if(IdgateData==null) {
	       	 	    IdgateData = new HashMap<String, Object>();
	       	    }
	            BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
	            if(tmp.getResult()) {		 
	                idgateUserFlag="Y";                  		 
	            }		 
	            tmp.addData("idgateUserFlag",idgateUserFlag);		 
	            IdgateData.putAll((Map<String, String>) tmp.getData());
	            SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	        }catch(Exception e) {		 
	            log.debug("idgateUserFlag error {}",e);		 
	        }		 
	        model.addAttribute("idgateUserFlag",idgateUserFlag);			
		}
		catch (Exception e)
		{
			log.error("f_renewal_apply Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				model.addAttribute("f_deposit_cancel", bs);
				target = JSPFOLDERPATH + "/f_deposit_cancel";
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	
	/**
	 * N175 外幣綜存定存解約確認頁
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_deposit_cancel_confirm")
	public String f_deposit_cancel_confirm(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.debug("f_deposit_cancel_confirm Start");
		String target = ERRORJSP;
		String jsondc = "";
		BaseResult bs = new BaseResult();
		String fgtrate = "";
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.debug(ESAPIUtil.vaildLog("f_renewal_apply_confirm.jsondc: {}"+jsondc));
			
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
			}
			else
			{
				String cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null));
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				// 即時或預約flag
				fgtrate = okMap.get("FGTRDATE");
				log.debug(ESAPIUtil.vaildLog("f_renewal_apply_confirm.fgtrate: {}"+ fgtrate));
				
				if("0".equals(fgtrate))
				{
					// 即時
					// 取得解約資料
					bs = fcy_Tdeposit_Service.f_deposit_cancel_confirm(cusidn, okMap);
				}
				else if("1".equals(fgtrate))
				{
					// 預約
					// 將輸入頁資料帶到確認頁，不發電文
					bs = fcy_Tdeposit_Service.f_deposit_cancel_confirm_booking(okMap);
				}
				
				// 取得防止重送代碼
				String tXToken = ((Map<String, String>) fcy_Tdeposit_Service.getTxToken().getData()).get("TXTOKEN");
				// 防止重送代碼TxToken
				bs.addData("TXTOKEN", tXToken);
				bs.addData("jsondc", jsondc);
				// 即時、預約判斷flag放入session，供切換locale時使用
				bs.addData("FGTRDATE", fgtrate);
				
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);				
			}
			Map<String,String>bsdata=(Map<String, String>) bs.getData();
			// IDGATE transdata            		 
            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
    		String adopid = "N175";
    		String title = "您有一筆-外幣綜存定存解約交易待確認,金額"+bsdata.get("SHOW_AMT");
        	if(IdgateData != null) {
	            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
	            if(IdgateData.get("idgateUserFlag").equals("Y")) {
	            	bsdata.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
	            	bsdata.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
					IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N175_IDGATE_DATA.class, N175_IDGATE_DATA_VIEW.class, bsdata));
	                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	            }
        	}
            model.addAttribute("idgateAdopid", adopid);
            model.addAttribute("idgateTitle", title);
		}
		catch (Exception e)
		{
			log.error("f_renewal_apply Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				Map<String, String> bsData = (Map<String, String>) bs.getData();
				// 即時、預約判斷flag，供切換locale時使用
				String bsFgtrate = bsData.get("FGTRDATE");
				
				if("0".equals(bsFgtrate))
				{
					// 即時
					target = JSPFOLDERPATH + "/f_deposit_cancel_confirm";
				}
				else if("1".equals(bsFgtrate))
				{
					// 預約
					target = JSPFOLDERPATH + "/f_deposit_cancel_confirm_booking";
				}
				model.addAttribute("f_deposit_cancel_confirm", bs);
				log.debug("target >> {}", target);
			}
			else
			{
				target = ERRORJSP;
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	

	/**
	 * N175 外幣綜存定存解約結果頁
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_deposit_cancel_result")
	public String f_deposit_cancel_result(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.debug("f_deposit_cancel_result Start");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		String pagToken = "";
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
			}
			else
			{
				// Get Session Value
				String dpMyEmail = (String) SessionUtil.getAttribute(model, SessionUtil.DPMYEMAIL, null);
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
				String uid = cusidn;
				okMap.put("UID",uid);
				okMap.put("CUSIDN",cusidn);
				// Check Session Token
				String SessionFinshToken = (String) SessionUtil.getAttribute(model, SessionUtil.FCY_TDEPOSIT_CANCEL_FINSH_TOKEN, null);
				pagToken = reqParam.get("TXTOKEN");
				if (fcy_Tdeposit_Service.validateToken(pagToken, SessionFinshToken))
				{
					log.error("SessionFinshToken && pagToken 一樣 表示重複交易");
				}
				else
				{
	                //IDgate驗證		 
	                try {		 
	                    if(okMap.get("FGTXWAY").equals("7")) {		 

	        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
	        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N175_IDGATE");
	                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
	                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
	                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
	                    }		 
	                }catch(Exception e) {		 
	                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
	                }  
	                
					Map<String,Object>IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
					model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
	                
					// Call Cancel Result
					bs = fcy_Tdeposit_Service.f_deposit_cancel_result(dpMyEmail, okMap);
					// 將查過的資料放入session，待切換 locale 時讀取
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
			}
		}
		catch (Exception e)
		{
			log.error("f_deposit_cancel_result Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				SessionUtil.addAttribute(model, SessionUtil.FCY_TDEPOSIT_CANCEL_FINSH_TOKEN, pagToken);
				model.addAttribute("f_deposit_cancel_result", bs);
				target = JSPFOLDERPATH + "/f_deposit_cancel_result";
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	

	/**
	 * N177 外幣定存自動轉期申請/變更進入頁
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_renewal_apply")
	public String f_renewal_apply(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.debug("f_renewal_apply Start");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		try
		{
			// 清除切換語系時暫存的資料
			cleanSession(model);
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

			String cusidn = SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null).toString();
			cusidn = new String(Base64.getDecoder().decode(cusidn));
			okMap.put("CUSIDN", cusidn);
			okMap.put("ADOPID", "N177I1");//// 電文代號表示輸入頁
			
			// get N530
			bs = fcy_Tdeposit_Service.getRestN530Data(okMap);
			log.debug("bs.getResult>> {}", bs.getResult());
		}
		catch (Exception e)
		{
			log.error("f_renewal_apply Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				model.addAttribute("f_renewal_apply", bs);
				target = JSPFOLDERPATH + "/f_renewal_apply";
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	

	/**
	 * 
	 * N177 外幣定存自動轉期申請/變更輸入頁
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_renewal_apply_step1")
	public String f_renewal_apply_step1(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.debug("f_renewal_apply_step1 Start");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			
			//判斷是否按上一頁
			boolean isback ="Y".equals(okMap.get("back"));
			
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String cusidn = SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null).toString();
			cusidn = new String(Base64.getDecoder().decode(cusidn));			
			if (hasLocale || isback)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null));
				
				// 回上一頁流程
				if (isback) 
				{
					// 讓頁面知道要回填資料
					model.addAttribute("bk_key", "Y");
					Map back = (Map) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
					// 以json字串儲存回上一頁資料
					SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, CodeUtil.toJson(back));
				}
			}
			else
			{				
				// 取得定存到期轉入帳號
				bs = fcy_Tdeposit_Service.f_renewal_apply_step1(cusidn, okMap);
				
				if (bs != null && bs.getResult())
				{
					// 將查過的資料放入session，待切換 locale 時讀取
					SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, bs);
				}
				
			}
	        //IDGATE身分
	        String idgateUserFlag="N";		 
	        Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
	        try {		 
	       	   if(IdgateData==null) {
	       		   IdgateData = new HashMap<String, Object>();
	       	   }
	            BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
	            if(tmp.getResult()) {		 
	                idgateUserFlag="Y";                  		 
	            }		 
	            tmp.addData("idgateUserFlag",idgateUserFlag);		 
	            IdgateData.putAll((Map<String, String>) tmp.getData());
	            SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	        }catch(Exception e) {		 
	            log.debug("idgateUserFlag error {}",e);		 
	        }		 
	        model.addAttribute("idgateUserFlag",idgateUserFlag);
	        
			model.addAttribute("f_renewal_apply_step1", bs);
		}
		catch (Exception e)
		{
			log.error("f_renewal_apply_step1 Error", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = JSPFOLDERPATH + "/f_renewal_apply_step1";
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	

	/**
	 * N177 外幣定存自動轉期申請/變更確認頁
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_renewal_apply_confirm")
	public String f_renewal_apply_confirm(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.debug("f_renewal_apply_confirm Start");
		String target = ERRORJSP;
		String jsondc = "";
		BaseResult bs = new BaseResult();
		try
		{
			// IKEY要使用的JSON:DC，ESAPI驗不過jsondc，故先不做ESAPI
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.debug(ESAPIUtil.vaildLog("f_renewal_apply_confirm.jsondc: {}"+ jsondc));
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
			}
			else
			{
				bs = fcy_Tdeposit_Service.f_renewal_apply_confirm(okMap);
				
				// 取得防止重送代碼
				String tXToken = ((Map<String, String>) fcy_Tdeposit_Service.getTxToken().getData()).get("TXTOKEN");
				
				bs.addData("TXTOKEN", tXToken);
				bs.addData("jsondc", jsondc);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				// IDGATE transdata block ===== start =====      		 
	            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	    		Map<String,String>result =(Map<String, String>) bs.getData();
	            String adopid = "N177";
	    		String title = "您有一筆-外幣定存自動轉期申請/變更交易待確認,金額"+result.get("CUID")+" "+result.get("display_BALANCE")+"元";
	        	if(IdgateData != null) {
		            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
		            	result.put("FYACN", result.get("ACN"));
		            	result.put("FDPNUM", result.get("FDPNO"));
		            	result.put("AMTFDP", result.get("BALANCE"));
		            	result.put("CRY", result.get("CUID"));
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N177_IDGATE_DATA.class, N177_IDGATE_DATA_VIEW.class, result));
		                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);
		                log.trace("IDGATE confirmData >>"+CodeUtil.toJson(IdgateData));
		            }
	        	}
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
	        	// IDGATE transdata block ===== END =====   
				
				// 回上一頁用來回填的資料
				Map<String, String> back = new HashMap<>();
				// 自動轉期次數
				back.put("AUTXFTM", okMap.get("AUTXFTM"));
				// 轉存方式
				back.put("CODE", okMap.get("CODE"));
				// 取出下拉選單的利息轉入帳號 FYTSFAN
				back.put("FYTSFAN1", reqParam.get("FYTSFAN1"));
				back.put("FYTSFAN2", reqParam.get("FYTSFAN2"));
				back.put("FYTSFAN3", reqParam.get("FYTSFAN3"));
				back.put("FYTSFAN4", reqParam.get("FYTSFAN4"));
				
				// 儲存回上一頁資料
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, back);
			}
			log.debug("bs.getResult >> {}", bs.getResult());
		}
		catch (Exception e)
		{
			log.error("f_renewal_apply_confirm Error", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = JSPFOLDERPATH + "/f_renewal_apply_confirm";
				model.addAttribute("f_renewal_apply_confirm", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * N177 外幣定存自動轉期申請/變更結果頁
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_renewal_apply_result")
	public String f_renewal_apply_result(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.debug("f_renewal_apply_result Start");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		String pagToken = "";
		try
		{
			// 解決Trust Boundary Violation
 			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			//Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI

			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
			}
			else
			{
				// Get Session Value
				String cusidn = SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null).toString();
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				String SessionFinshToken = (String) SessionUtil.getAttribute(model, SessionUtil.FCY_RENEWAL_APPLY_FINSH_TOKEN, null);

				// 交易成功後，防止重新整理，造成重複交易，利用 Token 實作
				/*
				首次交易前，confirm 頁面帶來的 token 有值，session token = null
				交易成功後，將confirm token塞入 session token，使兩 token 相同
				當使用者按 refresh，頁面會重帶 confirm token
				程式會發現 confirm token 和 session token 相同，判斷為重複交易
				*/
				pagToken = okMap.get("TXTOKEN");
				if (fcy_Tdeposit_Service.validateToken(pagToken, SessionFinshToken))
				{
					log.error("SessionFinshToken && pagToken 一樣 表示重複交易");
					return ERRORJSP;
				}
				else
				{
	                //IDgate驗證		 
	                try {		 
	                    if(okMap.get("FGTXWAY").equals("7")) {		 

	        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
	        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N177_IDGATE");
	                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
	                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
	                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
	                    }		 
	                }catch(Exception e) {		 
	                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
	                }
	                
					Map<String,Object>IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
					model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
	                
					bs = fcy_Tdeposit_Service.f_renewal_apply_result(cusidn, okMap);
					// 將查過的資料放入session，待切換 locale 時讀取
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
				log.debug("bs.getResult >> {}", bs.getResult());
			}
		}
		catch (Exception e)
		{
			log.error("f_renewal_apply_result Error", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				SessionUtil.addAttribute(model, SessionUtil.FCY_RENEWAL_APPLY_FINSH_TOKEN, pagToken);
				target = JSPFOLDERPATH + "/f_renewal_apply_result";
				model.addAttribute("f_renewal_apply_result", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * N178 外幣定存單到期續存 進入頁
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_order_renewal")
	public String f_order_renewal(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.debug("f_order_renewal Start");
		log.trace(ESAPIUtil.vaildLog("reqParam>>" + CodeUtil.toJson(reqParam)));
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		try
		{
			// 清除切換語系時暫存的資料
			cleanSession(model);
			
			String cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null));
			cusidn = new String(Base64.getDecoder().decode(cusidn));
			
			log.trace(ESAPIUtil.vaildLog("cusidn >> {}"+ reqParam.get("CUSIDN")));
			// 得到定存到期的清單 TYPE ==>N178_ACNO
			reqParam.put("ADOPID", "N178I1");//// 電文代號表示輸入頁
			bs = fcy_Tdeposit_Service.getcallN530Data(cusidn, reqParam);
			log.trace("bs.getResult>> {}", bs.getResult());
		}
		catch (Exception e)
		{
			log.error("f_order_renewal Error", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = JSPFOLDERPATH + "/f_order_renewal";
				model.addAttribute("f_order_renewal", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	

	/**
	 * ******************** 外幣定存單到期續存輸入頁********************
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_order_renewal_step1")
	public String f_order_renewal_step1(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.debug("f_order_renewal_step1 Start");
		log.trace(ESAPIUtil.vaildLog("reqParam>>" + CodeUtil.toJson(reqParam)));
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		try
		{
			// 解決Trust Boundary Violation
			//Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			//判斷是否按上一頁
			boolean back ="Y".equals(okMap.get("back"));
			String cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null));
			cusidn = new String(Base64.getDecoder().decode(cusidn));
			
			if (hasLocale||back)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null));
			}
			else
			{				
				// 取得利息轉入帳號清單、輸入頁資料
				bs = fcy_Tdeposit_Service.f_order_renewal_step1(cusidn, okMap);
				
				log.trace("bs.getResult>> {}", bs.getResult());
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, bs);
			}
			//IDGATE身分
	        String idgateUserFlag="N";		 
	        Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
	        try {		 
	       	   if(IdgateData==null) {
	       		   IdgateData = new HashMap<String, Object>();
	       	   }
	            BaseResult tmp=idgateservice.checkIdentity(cusidn);
	            if(tmp.getResult()) {		 
	                idgateUserFlag="Y";                  		 
	            }		 
	            tmp.addData("idgateUserFlag",idgateUserFlag);		 
	            IdgateData.putAll((Map<String, String>) tmp.getData());
	            SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	        }catch(Exception e) {		 
	            log.debug("idgateUserFlag error {}",e);		 
	        }		 
	        model.addAttribute("idgateUserFlag",idgateUserFlag);
		}
		catch (Exception e)
		{
			log.error("f_order_renewal_step1 Error", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = JSPFOLDERPATH + "/f_order_renewal_step1";
				model.addAttribute("f_order_renewal_step1", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * ******************** 外幣定存單到期續存確認頁********************
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_order_renewal_confirm")
	public String f_order_renewal_confirm(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.debug("f_order_renewal_confirm Start");
		log.trace(ESAPIUtil.vaildLog("reqParam>>" + CodeUtil.toJson(reqParam)));
		String target = ERRORJSP;
		String jsondc = "";
		BaseResult bs = new BaseResult();
		
		try
		{
			// 解決Trust Boundary Violation
//			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("f_order_renewal_confirm.jsondc: " + jsondc));
			
			okMap.put("jsondc", jsondc);
			log.trace(ESAPIUtil.vaildLog("f_order_renewal_confirm.okMap: {}"+ CodeUtil.toJson(okMap)));
						
						
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
			}
			else
			{

				String cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null));
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				// 取得前一頁的資料
				bs = fcy_Tdeposit_Service.f_order_renewal_confirm(cusidn, okMap);
				// 取得防止重送代碼
				Map<String, String> mapGetToken = (Map) fcy_Tdeposit_Service.getTxToken().getData();
				// 防止重送代碼TxToken
				String tXToken = mapGetToken.get("TXTOKEN");
				bs.addData("TXTOKEN", tXToken);
				bs.addData("jsondc", jsondc);
				log.trace("bs.getResult>> {}", bs.getResult());
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
			}
			// IDGATE transdata            		 
            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
    		Map<String,String>result=new HashMap<String,String>();
        	result.putAll((Map<String, String>) bs.getData());
            String adopid = "N178";
    		String title = "您有一筆-外幣定存單到期續存待確認,金額"+result.get("CRYNAME")+" "+ result.get("SHOW_AMTTSF")+"元";
        	if(IdgateData != null) {
	            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
	            if(IdgateData.get("idgateUserFlag").equals("Y")) {
	            	result.put("AMTFDP", result.get("AMTTSF"));
	            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
	            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
					IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N178_IDGATE_DATA.class, N178_IDGATE_DATA_VIEW.class, result));
	                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	            }
        	}
            model.addAttribute("idgateAdopid", adopid);
            model.addAttribute("idgateTitle", title);
		}
		catch (Exception e)
		{
			log.error("f_order_renewal_confirm Error", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = JSPFOLDERPATH + "/f_order_renewal_confirm";
				model.addAttribute("f_order_renewal_confirm", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * N178 外幣定存單到期續存結果頁
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_order_renewal_result")
	public String f_order_renewal_result(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.debug("f_order_renewal_result Start");
		log.trace(ESAPIUtil.vaildLog("reqParam>>" + CodeUtil.toJson(reqParam)));
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		String pagToken = "";
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		//	Map<String, String> okMap = reqParam; // ESAPI驗不過jsondc，故先不做ESAPI
			
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
			}
			else
			{
				// Get Session Value
				String cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null));
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				String SessionFinshToken = (String) SessionUtil.getAttribute(model, SessionUtil.FCY_ORDER_RENEWAL_FINSH_TOKEN, null);
				// Get page Value
				pagToken = reqParam.get("TXTOKEN");
				// Check Session Token
				if (fcy_Tdeposit_Service.validateToken(pagToken, SessionFinshToken))
				{
					log.debug("SessionFinshToken && pagToken 一樣 表示重複交易");
					return ERRORJSP;
				}
				else
				{
					//IDgate驗證		 
	                try {		 
	                    if(okMap.get("FGTXWAY").equals("7")) {		 

	        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
	        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N178_IDGATE");
	                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
	                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
	                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));
	                        log.trace("IDGATE TRANSDATA >>"+CodeUtil.toJson(okMap));
	                    }		 
	                }catch(Exception e) {		 
	                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
	                }  
	                
					Map<String,Object>IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
					model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
	                
					// Call f_order_renewal_result
					bs = fcy_Tdeposit_Service.f_order_renewal_result(cusidn, okMap);
					// 將查過的資料放入session，待切換 locale 時讀取
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
				log.trace("bs.getResult>> {}", bs.getResult());

			}

		}
		catch (Exception e)
		{
			log.error("f_order_renewal_result Error", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				SessionUtil.addAttribute(model, SessionUtil.FCY_ORDER_RENEWAL_FINSH_TOKEN, pagToken);
				model.addAttribute("f_order_renewal_result", bs);
				target = JSPFOLDERPATH + "/f_order_renewal_result";
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * 清除暫存session的資料
	 * 
	 * @param model
	 */
	public void cleanSession(Model model)
	{
		// 清除切換語系
		SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null);
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null);
		SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null);
		// 清除回上一頁
		SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, null);
	}
	
}
