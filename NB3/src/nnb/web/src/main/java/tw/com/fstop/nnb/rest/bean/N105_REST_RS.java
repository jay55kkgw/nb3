package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N105_REST_RS extends BaseRestBean implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9144600800349332501L;

	private String MSGFLG; // 訊息註記

	private String CURDATE; // 資料日期

	private String REC_NO; // 筆數

	private String BRHNUM; // 分行名稱

	private String BRHADR; // 分行地址

	private String TELNUM1; // 電話

	private String NAME; // 借戶姓名

	private String CUSNAME;// 原住民姓名

	private String ENDCOD; // 結束註記

	private String USERDATA;

	private String TXTOKEN;

	private LinkedList<N105_REST_RSDATA> REC;

	public String getMSGFLG()
	{
		return MSGFLG;
	}

	public void setMSGFLG(String mSGFLG)
	{
		MSGFLG = mSGFLG;
	}

	public String getCURDATE()
	{
		return CURDATE;
	}

	public void setCURDATE(String cURDATE)
	{
		CURDATE = cURDATE;
	}

	public String getREC_NO()
	{
		return REC_NO;
	}

	public void setREC_NO(String rEC_NO)
	{
		REC_NO = rEC_NO;
	}

	public String getBRHNUM()
	{
		return BRHNUM;
	}

	public void setBRHNUM(String bRHNUM)
	{
		BRHNUM = bRHNUM;
	}

	public String getBRHADR()
	{
		return BRHADR;
	}

	public void setBRHADR(String bRHADR)
	{
		BRHADR = bRHADR;
	}

	public String getTELNUM1()
	{
		return TELNUM1;
	}

	public void setTELNUM1(String tELNUM1)
	{
		TELNUM1 = tELNUM1;
	}

	public String getNAME()
	{
		return NAME;
	}

	public void setNAME(String nAME)
	{
		NAME = nAME;
	}

	public String getCUSNAME()
	{
		return CUSNAME;
	}

	public void setCUSNAME(String cUSNAME)
	{
		CUSNAME = cUSNAME;
	}

	public String getENDCOD()
	{
		return ENDCOD;
	}

	public void setENDCOD(String eNDCOD)
	{
		ENDCOD = eNDCOD;
	}

	public String getUSERDATA()
	{
		return USERDATA;
	}

	public void setUSERDATA(String uSERDATA)
	{
		USERDATA = uSERDATA;
	}

	public String getTXTOKEN()
	{
		return TXTOKEN;
	}

	public void setTXTOKEN(String tXTOKEN)
	{
		TXTOKEN = tXTOKEN;
	}

	public LinkedList<N105_REST_RSDATA> getREC()
	{
		return REC;
	}

	public void setREC(LinkedList<N105_REST_RSDATA> rEC)
	{
		REC = rEC;
	}

}
