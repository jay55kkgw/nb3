package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class F017R_REST_RQ extends BaseRestBean_FX implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4954937197513548727L;
	
	private String CUSIDN;// 客戶統編
	
	private String FYACN;// 轉出帳號

	private String TSFAN;// 轉入帳號

	private String OUT_CRY;// 轉出幣別

	private String IN_CRY;// 轉入幣別

	private String RATE;//

	private String CURAMT;// 轉出金額

	private String CMTRDATE;// 交易日期

	private String SRCFUND;// 匯款分類

	private String NAME;// 使用者姓名

	private String CUSTYPE;// 客戶身分別
	
	private String PAIAFTX;
	
	

	public String getCUSIDN()
	{
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN)
	{
		CUSIDN = cUSIDN;
	}

	public String getFYACN()
	{
		return FYACN;
	}

	public void setFYACN(String fYACN)
	{
		FYACN = fYACN;
	}

	public String getTSFAN()
	{
		return TSFAN;
	}

	public void setTSFAN(String tSFAN)
	{
		TSFAN = tSFAN;
	}

	public String getOUT_CRY()
	{
		return OUT_CRY;
	}

	public void setOUT_CRY(String oUT_CRY)
	{
		OUT_CRY = oUT_CRY;
	}

	public String getIN_CRY()
	{
		return IN_CRY;
	}

	public void setIN_CRY(String iN_CRY)
	{
		IN_CRY = iN_CRY;
	}

	public String getRATE()
	{
		return RATE;
	}

	public void setRATE(String rATE)
	{
		RATE = rATE;
	}

	public String getCURAMT()
	{
		return CURAMT;
	}

	public void setCURAMT(String cURAMT)
	{
		CURAMT = cURAMT;
	}

	public String getCMTRDATE()
	{
		return CMTRDATE;
	}

	public void setCMTRDATE(String cMTRDATE)
	{
		CMTRDATE = cMTRDATE;
	}

	public String getSRCFUND()
	{
		return SRCFUND;
	}

	public void setSRCFUND(String sRCFUND)
	{
		SRCFUND = sRCFUND;
	}

	public String getNAME()
	{
		return NAME;
	}

	public void setNAME(String nAME)
	{
		NAME = nAME;
	}

	public String getCUSTYPE()
	{
		return CUSTYPE;
	}

	public void setCUSTYPE(String cUSTYPE)
	{
		CUSTYPE = cUSTYPE;
	}

	public String getPAIAFTX()
	{
		return PAIAFTX;
	}

	public void setPAIAFTX(String pAIAFTX)
	{
		PAIAFTX = pAIAFTX;
	}
	

}
