package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * C012電文RS
 */
public class C012_REST_RS extends BaseRestBean implements Serializable{
	private static final long serialVersionUID = 6791228050721094802L;
	
	//共用
	private String CMQTIME;
	//約當台幣總報酬
	private String TOTREFVALUE;
	private String TOTCRY;
	private String TOTLCYRAT;
	private String TOTFCYRAT;
	private String TOTDIFAMT;
	private String TOTTWDAMT;
	private String TOTDIFAMT2;
	
	private LinkedList<C012_REST_RSDATA> REC;
	private LinkedList<C012_REST_RSDATA2> REC2;
	private LinkedList<C012_REST_RSDATA3> REC3;
	
	private String SHWD;//視窗註記 2020/07/02新增
	
	public String getCMQTIME(){
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME){
		CMQTIME = cMQTIME;
	}
	public String getTOTCRY(){
		return TOTCRY;
	}
	public void setTOTCRY(String tOTCRY){
		TOTCRY = tOTCRY;
	}
	public String getTOTFCYRAT(){
		return TOTFCYRAT;
	}
	public void setTOTFCYRAT(String tOTFCYRAT){
		TOTFCYRAT = tOTFCYRAT;
	}
	public String getTOTDIFAMT(){
		return TOTDIFAMT;
	}
	public void setTOTDIFAMT(String tOTDIFAMT){
		TOTDIFAMT = tOTDIFAMT;
	}
	public String getTOTDIFAMT2(){
		return TOTDIFAMT2;
	}
	public void setTOTDIFAMT2(String tOTDIFAMT2){
		TOTDIFAMT2 = tOTDIFAMT2;
	}
	public String getTOTREFVALUE(){
		return TOTREFVALUE;
	}
	public void setTOTREFVALUE(String tOTREFVALUE){
		TOTREFVALUE = tOTREFVALUE;
	}
	public String getTOTLCYRAT(){
		return TOTLCYRAT;
	}
	public void setTOTLCYRAT(String tOTLCYRAT){
		TOTLCYRAT = tOTLCYRAT;
	}
	public String getTOTTWDAMT(){
		return TOTTWDAMT;
	}
	public void setTOTTWDAMT(String tOTTWDAMT){
		TOTTWDAMT = tOTTWDAMT;
	}
	public LinkedList<C012_REST_RSDATA> getREC(){
		return REC;
	}
	public void setREC(LinkedList<C012_REST_RSDATA> rEC){
		REC = rEC;
	}
	public LinkedList<C012_REST_RSDATA2> getREC2(){
		return REC2;
	}
	public void setREC2(LinkedList<C012_REST_RSDATA2> rEC2){
		REC2 = rEC2;
	}
	public LinkedList<C012_REST_RSDATA3> getREC3(){
		return REC3;
	}
	public void setREC3(LinkedList<C012_REST_RSDATA3> rEC3){
		REC3 = rEC3;
	}
	public String getSHWD() {
		return SHWD;
	}
	public void setSHWD(String sHWD) {
		SHWD = sHWD;
	}
}