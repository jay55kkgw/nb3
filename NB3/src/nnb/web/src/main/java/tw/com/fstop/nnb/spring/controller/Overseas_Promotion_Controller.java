package tw.com.fstop.nnb.spring.controller;

import java.util.Base64;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.Overseas_Promotion_Service;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({ SessionUtil.PD,SessionUtil.DOWNLOAD_DATALISTMAP_DATA,SessionUtil.PRINT_DATALISTMAP_DATA,SessionUtil.RESULT_LOCALE_DATA, SessionUtil.CONFIRM_LOCALE_DATA, SessionUtil.RESULT_LOCALE_DATA,SessionUtil.CUSIDN })
@Controller
@RequestMapping(value = "/OVERSEAS/PROMOTION")
public class Overseas_Promotion_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private Overseas_Promotion_Service overseas_promotion_service;

	
	// 申請境外信託商品推介
	@RequestMapping(value = "/apply_trust_product")
	public String apply_trust_product(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("apply_trust_product >>");
		String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
		try {
			bs = new BaseResult();
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			bs = overseas_promotion_service.apply_trust_product(cusidn);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("apply_trust_product error >> {}",e);
		} finally {
			if (bs != null && bs.getResult())
			{
				target = "/fund/apply_trust_product";
				model.addAttribute("result_data", bs);
			}else {
				bs.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	// 申請境外信託商品推介
	@RequestMapping(value = "/apply_trust_product_result")
	public String apply_trust_product_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("apply_trust_product_result >>");
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
		try {
	   		bs =new  BaseResult();
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
//				log.trace("locale>>" + okMap.get("locale"));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				bs.reset();
				bs = overseas_promotion_service.apply_trust_product_result(cusidn, reqParam);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("apply_trust_product_result error >> {}",e);
		} finally {
			if (bs != null && bs.getResult())
			{
				target = "/fund/apply_trust_product_result";
				model.addAttribute("result_data", bs);
			}else {
				bs.setPrevious("/OVERSEAS/PROMOTION/apply_trust_product");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	

	// 終止境外信託商品推介
	@RequestMapping(value = "/cancel_trust_product")
	public String cancel_trust_product(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("apply_trust_product >>");
		String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
		try {
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			bs = overseas_promotion_service.cancel_trust_product(cusidn);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("cancel_trust_product error >> {}",e);
		} finally {
			if (bs != null && bs.getResult())
			{
				target = "/fund/cancel_trust_product";
				model.addAttribute("result_data", bs);
			}else {
				bs.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	// 終止境外信託商品推介
	@RequestMapping(value = "/cancel_trust_product_result")
	public String cancel_trust_product_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("cancel_trust_product_result >>");
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
		try {
	   		bs =new  BaseResult();
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
//				log.trace("locale>>" + okMap.get("locale"));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				bs.reset();
				bs = overseas_promotion_service.cancel_trust_product_result(cusidn, reqParam);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("cancel_trust_product_result error >> {}",e);
		} finally {
			if (bs != null && bs.getResult())
			{
				target = "/fund/cancel_trust_product_result";
				model.addAttribute("result_data", bs);
			}else {
				bs.setPrevious("/OVERSEAS/PROMOTION/cancel_trust_product");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
}
