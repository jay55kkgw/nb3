package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N816L_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8111928052113225298L;
	
	//自動扣繳帳號
		String PAYMT_ACCT ;
		//本期最低金額
		String TOTL_DUE;
		//統編/ID
		String CUSIDN ;
		//繳款截止日
		String STOP_DAY;
		//已動用預借現金
		String CSHBAL;
		//本期循環信用利率
		String INT_RATE ;
		//信用額度
		String CRLIMIT;
		//錯誤代碼 空白表示成功
		String RSPCOD ;
		//目前已使用額度
		String ACCBAL;
		//結帳日
		String STMT_DAY;
		//應繳總金額
		String CURR_BAL;
		//序號
		String SEQ;
		//預借現金額度
		String CSHLIMIT;
		//扣繳方式 01全額 02最低
		String DBCODE;
		//傳輸筆數
		String RECNUM;
		//交易時間
		String CMQTIME;
		//???
		String CMRECNUM;
		
		//電文內陣列
		LinkedList<N816L_REST_RSDATA> REC;

		public String getPAYMT_ACCT() {
			return PAYMT_ACCT;
		}

		public void setPAYMT_ACCT(String pAYMT_ACCT) {
			PAYMT_ACCT = pAYMT_ACCT;
		}

		public String getTOTL_DUE() {
			return TOTL_DUE;
		}

		public void setTOTL_DUE(String tOTL_DUE) {
			TOTL_DUE = tOTL_DUE;
		}

		public String getCUSIDN() {
			return CUSIDN;
		}

		public void setCUSIDN(String cUSIDN) {
			CUSIDN = cUSIDN;
		}

		public String getSTOP_DAY() {
			return STOP_DAY;
		}

		public void setSTOP_DAY(String sTOP_DAY) {
			STOP_DAY = sTOP_DAY;
		}

		public String getCSHBAL() {
			return CSHBAL;
		}

		public void setCSHBAL(String cSHBAL) {
			CSHBAL = cSHBAL;
		}

		public String getINT_RATE() {
			return INT_RATE;
		}

		public void setINT_RATE(String iNT_RATE) {
			INT_RATE = iNT_RATE;
		}

		public String getCRLIMIT() {
			return CRLIMIT;
		}

		public void setCRLIMIT(String cRLIMIT) {
			CRLIMIT = cRLIMIT;
		}

		public String getRSPCOD() {
			return RSPCOD;
		}

		public void setRSPCOD(String rSPCOD) {
			RSPCOD = rSPCOD;
		}

		public String getACCBAL() {
			return ACCBAL;
		}

		public void setACCBAL(String aCCBAL) {
			ACCBAL = aCCBAL;
		}

		public String getSTMT_DAY() {
			return STMT_DAY;
		}

		public void setSTMT_DAY(String sTMT_DAY) {
			STMT_DAY = sTMT_DAY;
		}

		public String getCURR_BAL() {
			return CURR_BAL;
		}

		public void setCURR_BAL(String cURR_BAL) {
			CURR_BAL = cURR_BAL;
		}

		public String getSEQ() {
			return SEQ;
		}

		public void setSEQ(String sEQ) {
			SEQ = sEQ;
		}

		public String getCSHLIMIT() {
			return CSHLIMIT;
		}

		public void setCSHLIMIT(String cSHLIMIT) {
			CSHLIMIT = cSHLIMIT;
		}

		public String getDBCODE() {
			return DBCODE;
		}

		public void setDBCODE(String dBCODE) {
			DBCODE = dBCODE;
		}

		public String getRECNUM() {
			return RECNUM;
		}

		public void setRECNUM(String rECNUM) {
			RECNUM = rECNUM;
		}

		public LinkedList<N816L_REST_RSDATA> getREC() {
			return REC;
		}

		public void setREC(LinkedList<N816L_REST_RSDATA> rEC) {
			REC = rEC;
		}

		public String getCMQTIME() {
			return CMQTIME;
		}

		public void setCMQTIME(String cMQTIME) {
			CMQTIME = cMQTIME;
		}

		public String getCMRECNUM() {
			return CMRECNUM;
		}

		public void setCMRECNUM(String cMRECNUM) {
			CMRECNUM = cMRECNUM;
		}
	
}