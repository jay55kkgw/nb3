package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class B021_REST_RQ  extends BaseRestBean_FUND implements Serializable{
	

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9126045977629199098L;
	
	private String CUSIDN;
	private String IP;
	private String BRHCOD;		//網路開戶分行碼

	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	public String getBRHCOD() {
		return BRHCOD;
	}
	public void setBRHCOD(String bRHCOD) {
		BRHCOD = bRHCOD;
	}
}
