package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N557_REST_RSDATA implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 659760339252735209L;

	String RADVNM;		//通知銀行名稱
	String RLGAMT;		//擔保信用狀/保證函金額
	String RLGNO;		//擔保信用狀/保證函號碼
	String RBENADD3;	//受益人地址3
	String RBENADD2;	//受益人地址2
	String RBENADD1;	//受益人地址1
	String RBENNAME;	//受益人
	String RLGCCY;		//幣別
	String RLGOS;		//擔保信用狀/保證函餘額
	String RMARK;		//備註
	String REXPDATE;	//信用狀到期日
	String ROPENDAT;	//開狀日期
	
	public String getRADVNM() {
		return RADVNM;
	}
	public void setRADVNM(String rADVNM) {
		RADVNM = rADVNM;
	}
	public String getRLGAMT() {
		return RLGAMT;
	}
	public void setRLGAMT(String rLGAMT) {
		RLGAMT = rLGAMT;
	}
	public String getRLGNO() {
		return RLGNO;
	}
	public void setRLGNO(String rLGNO) {
		RLGNO = rLGNO;
	}
	public String getRBENADD3() {
		return RBENADD3;
	}
	public void setRBENADD3(String rBENADD3) {
		RBENADD3 = rBENADD3;
	}
	public String getRBENADD2() {
		return RBENADD2;
	}
	public void setRBENADD2(String rBENADD2) {
		RBENADD2 = rBENADD2;
	}
	public String getRBENADD1() {
		return RBENADD1;
	}
	public void setRBENADD1(String rBENADD1) {
		RBENADD1 = rBENADD1;
	}
	public String getRBENNAME() {
		return RBENNAME;
	}
	public void setRBENNAME(String rBENNAME) {
		RBENNAME = rBENNAME;
	}
	public String getRLGCCY() {
		return RLGCCY;
	}
	public void setRLGCCY(String rLGCCY) {
		RLGCCY = rLGCCY;
	}
	public String getRLGOS() {
		return RLGOS;
	}
	public void setRLGOS(String rLGOS) {
		RLGOS = rLGOS;
	}
	public String getRMARK() {
		return RMARK;
	}
	public void setRMARK(String rMARK) {
		RMARK = rMARK;
	}
	public String getREXPDATE() {
		return REXPDATE;
	}
	public void setREXPDATE(String rEXPDATE) {
		REXPDATE = rEXPDATE;
	}
	public String getROPENDAT() {
		return ROPENDAT;
	}
	public void setROPENDAT(String rOPENDAT) {
		ROPENDAT = rOPENDAT;
	}
}
