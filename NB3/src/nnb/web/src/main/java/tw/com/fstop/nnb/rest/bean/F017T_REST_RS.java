package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class F017T_REST_RS extends BaseRestBean implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1226810981178547166L;

	private String DATA_LL; // 資料長度

	private String BRH_COD; // 分行別

	private String TC_NO; // TC號碼

	private String WK_ID; // 工作站代號

	private String TX_ID; // 程式代號

	private String STATUS; // 狀態

	private String PF_KEY; // PK KEY

	private String MSG_CODE; // 訊息代碼

	private String INDICTOR; // 燈號控制碼

	private String TL_ID; // 櫃員代號

	private String TX_SEQ; // 交易序號

	private String MSG_HEAD; // 訊息標頭

	private String PCODE; // 交易代號

	private String STAN; // 交易續號

	private String SEQNO; // 交易流水號

	private String STATUS1; // 狀態

	private String AVAAMT; // 可用餘額

	private String CURAMT; // 議價金額 9(13)V9(2)

	private String TDBAMT; // 本交金額

	private String CHGROE; // 匯率 9(6)V9(6)

	private String CHGROERL; // 換匯規則

	private String CMQTIME;// 交易日期

	private String ATRAMT;//ATRAMT

	public String getDATA_LL()
	{
		return DATA_LL;
	}

	public void setDATA_LL(String dATA_LL)
	{
		DATA_LL = dATA_LL;
	}

	public String getBRH_COD()
	{
		return BRH_COD;
	}

	public void setBRH_COD(String bRH_COD)
	{
		BRH_COD = bRH_COD;
	}

	public String getTC_NO()
	{
		return TC_NO;
	}

	public void setTC_NO(String tC_NO)
	{
		TC_NO = tC_NO;
	}

	public String getWK_ID()
	{
		return WK_ID;
	}

	public void setWK_ID(String wK_ID)
	{
		WK_ID = wK_ID;
	}

	public String getTX_ID()
	{
		return TX_ID;
	}

	public void setTX_ID(String tX_ID)
	{
		TX_ID = tX_ID;
	}

	public String getSTATUS()
	{
		return STATUS;
	}

	public void setSTATUS(String sTATUS)
	{
		STATUS = sTATUS;
	}

	public String getPF_KEY()
	{
		return PF_KEY;
	}

	public void setPF_KEY(String pF_KEY)
	{
		PF_KEY = pF_KEY;
	}

	public String getMSG_CODE()
	{
		return MSG_CODE;
	}

	public void setMSG_CODE(String mSG_CODE)
	{
		MSG_CODE = mSG_CODE;
	}

	public String getINDICTOR()
	{
		return INDICTOR;
	}

	public void setINDICTOR(String iNDICTOR)
	{
		INDICTOR = iNDICTOR;
	}

	public String getTL_ID()
	{
		return TL_ID;
	}

	public void setTL_ID(String tL_ID)
	{
		TL_ID = tL_ID;
	}

	public String getTX_SEQ()
	{
		return TX_SEQ;
	}

	public void setTX_SEQ(String tX_SEQ)
	{
		TX_SEQ = tX_SEQ;
	}

	public String getMSG_HEAD()
	{
		return MSG_HEAD;
	}

	public void setMSG_HEAD(String mSG_HEAD)
	{
		MSG_HEAD = mSG_HEAD;
	}

	public String getPCODE()
	{
		return PCODE;
	}

	public void setPCODE(String pCODE)
	{
		PCODE = pCODE;
	}

	public String getSTAN()
	{
		return STAN;
	}

	public void setSTAN(String sTAN)
	{
		STAN = sTAN;
	}

	public String getSEQNO()
	{
		return SEQNO;
	}

	public void setSEQNO(String sEQNO)
	{
		SEQNO = sEQNO;
	}

	public String getSTATUS1()
	{
		return STATUS1;
	}

	public void setSTATUS1(String sTATUS1)
	{
		STATUS1 = sTATUS1;
	}

	public String getAVAAMT()
	{
		return AVAAMT;
	}

	public void setAVAAMT(String aVAAMT)
	{
		AVAAMT = aVAAMT;
	}

	public String getCURAMT()
	{
		return CURAMT;
	}

	public void setCURAMT(String cURAMT)
	{
		CURAMT = cURAMT;
	}

	public String getTDBAMT()
	{
		return TDBAMT;
	}

	public void setTDBAMT(String tDBAMT)
	{
		TDBAMT = tDBAMT;
	}

	public String getCHGROE()
	{
		return CHGROE;
	}

	public void setCHGROE(String cHGROE)
	{
		CHGROE = cHGROE;
	}

	public String getCHGROERL()
	{
		return CHGROERL;
	}

	public void setCHGROERL(String cHGROERL)
	{
		CHGROERL = cHGROERL;
	}

	public String getCMQTIME()
	{
		return CMQTIME;
	}

	public void setCMQTIME(String cMQTIME)
	{
		CMQTIME = cMQTIME;
	}

	public String getATRAMT()
	{
		return ATRAMT;
	}

	public void setATRAMT(String aTRAMT)
	{
		ATRAMT = aTRAMT;
	}

	
	
}
