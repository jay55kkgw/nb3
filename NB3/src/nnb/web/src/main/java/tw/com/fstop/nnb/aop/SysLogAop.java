package tw.com.fstop.nnb.aop;

import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import brave.Tracer;
import fstop.orm.po.SYSLOG;
import fstop.orm.po.TXNLOG;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.custom.annotation.ISTXNLOG;
import tw.com.fstop.tbb.nnb.dao.SysLogDao;
import tw.com.fstop.tbb.nnb.dao.TxnLogDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;


//https://blog.csdn.net/qq_27093465/article/details/78800100
//https://matthung0807.blogspot.com/2018/02/spring-aop.html
@Aspect
@Component
public class SysLogAop {

	Logger log = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private HttpServletRequest req; 
	@Autowired Tracer tracer;
	@Autowired TxnLogDao txnlogdao;
	@Autowired SysLogDao syslogdao;
	
	
//	@Before("within(tw.com.fstop.nnb.spring.controller..*)")
//	public Object doBeforeLogin(JoinPoint joinPoint) {
//		log.info("SysLogAop.doBeforeLogin...");
//		return Boolean.TRUE;
//		
//	}
//	@Before("within(org.apache.logging.slf4j.Log4jLogger..*)")
//	@Before("within(org.apache.logging.slf4j.Log4jLogger) && execution(* trace(..))")
	
//	@Before("within(tw.com.fstop.nnb.spring.controller.TxnUser_Controller)")
//	@Before("within(org.apache.logging.slf4j.Log4jLogger) && execution(* trace(..)) && execution(* debug(..)) && execution(* info(..))  ")
	
//	@Before("within(org.apache.logging.slf4j.Log4jLogger) && ( execution(* trace(..)) || execution(* debug(..)) || execution(* info(..)) )")
//	public Object logtest(JoinPoint joinPoint) {
////		log.info("SysLogAop.logtest...");
//		System.out.println("xxzz...");
//		return Boolean.TRUE;
//		
//	}
	
//	ProceedingJoinPoint pjp
	@Around("within(tw.com.fstop.nnb.spring.controller..*)")
	public Object timediff(ProceedingJoinPoint pjp) {
		log.info("SysLogAop.timediff...");
		Object obj = null;
		Object[] args = null;
		DateTime d1 = null;
		DateTime d2 = null;
		String pstimediff = "";
		String adexcode = "";
		String adopid = "";
		Map<String,String> data = null;
		String api =  "";
		Boolean isTxnLog = Boolean.FALSE;
		try {
			log.info("Before doAround.proceed..");
			
			try {
				BaseResult bs =  verifyMBIP();
				if(!bs.getResult()) {
					return bs;
				}
				args = pjp.getArgs();
				if(args !=null) {
					for(int i = 0 ; i < args.length ; i++) {
						if( args[i]  instanceof List) {
							List l = (List) args[i];
						}
						if( args[i]  instanceof Map) {
//							data = (Map) args[i];
						}
						if( args[i]  instanceof LinkedHashMap && !(args[i] instanceof BindingAwareModelMap)) {
							data = (Map) args[i];
						}
						if( args[i]  instanceof BindingAwareModelMap ) {
//							data = (Map) args[i];
						}
					}
				}
			} catch (Exception e) {
				log.error("{}",e);
			}
				
			
			MethodSignature signature = (MethodSignature) pjp.getSignature();
//			RequestMapping rqm = signature.getMethod().getAnnotation(RequestMapping.class);
//			log.trace("rqm>>{}",rqm);
			ISTXNLOG txn = signature.getMethod().getAnnotation(ISTXNLOG.class);
			log.info("txnlog>>{}",txn);
			if(txn!=null && StrUtil.isNotEmpty(txn.value())) {
				
				adopid = txn.value();
				log.info("adopid>>{}",adopid);
				isTxnLog = Boolean.TRUE;
			}
			
			d1 = new DateTime();
			obj = pjp.proceed();
			d2 = new DateTime();
			log.info("After doAround.proceed..");
			pstimediff = DateUtil.getDiffTimeMills(d1, d2);
			log.warn("pstimediff>>{}",pstimediff);
			if(obj !=null) {
				
				if(obj instanceof String) {
					log.debug("obj>>{}",obj);
				}
				if(obj instanceof BaseResult) {
					
					BaseResult bs =  (BaseResult) obj;
					adexcode = bs.getMsgCode();
//					BaseResult bs = CodeUtil.objectCovert(BaseResult.class, obj);
				}
			}else {
				log.warn("obj is null...");
				return obj;
			}
			if(isTxnLog) {
				try {
					writeTxnLog(data, adopid, adexcode, pstimediff);
				} catch (Exception e) {
					log.error("{}",e);
				}
			}
				
//				writeSysLog(data, adopid, adexcode,d1,d2, pstimediff);
		} catch (Throwable e) {
			log.error("SysLogAop.timediff.error>>{}",e);
		}
		log.info("SysLogAop.timediff end...");
		return obj;
		
	}
	
	
	/**
	 * 驗證MB API白名單
	 * @return
	 */
	public BaseResult verifyMBIP() {

		String pathInfo = req.getRequestURI().replaceFirst(req.getContextPath(), "");
		BaseResult bs = new BaseResult();
		log.trace(ESAPIUtil.vaildLog("pathInfo>>"+ pathInfo));
//		log.trace("pathInfo>>{}", pathInfo);
		try {
			
			if(pathInfo.equals("/MB/GETPK/getPublicKey")) {
				bs.setResult(Boolean.TRUE);
				bs.setMsgCode("0");
				bs.setMessage("ok");
				return bs;
			}
			if (StrUtil.isNotEmpty(pathInfo) && ( pathInfo.startsWith("/MB/") || pathInfo.startsWith("/EXTERNAL/API/") )) {
				String ip = WebUtil.getIpAddr(req);
				String set_ip = SpringBeanFactory.getBean("mb_pass_ip");
//				log.trace("ip>>{}", ip);
				log.trace("set_ip>>{}", set_ip);
				if (StrUtil.isEmpty(ip)) {
					bs.setMsgCode("1");
					bs.setMessage("ip not found");
					return bs;
				}
				List<String> set_ip_List = Arrays.asList(set_ip.split(","));
				// 判斷IP
				if (!set_ip_List.contains(ip)) {
					ip = ip.substring(0, ip.lastIndexOf("."));
					// 判斷網段
					if (!set_ip_List.contains(ip)) {
						bs.setMsgCode("1");
						bs.setMessage("ip 不合法");
						return bs;
					}
				}
			}
			bs.setResult(Boolean.TRUE);
			bs.setMsgCode("0");
			bs.setMessage("ok");
		} catch (Exception e) {
			log.error("{}", e);
			bs.setMsgCode("1");
			bs.setMessage("ip 驗證異常");
		}
		return bs;
	}
	
	public void writeTxnLog(Map<String,String> data ,String adopid ,String adexcode ,String pstimediff  ) {
		String aduserid = "";
		String aduserip = "";
		String adguid = "";
		TXNLOG po =null;
		try {
			po = new TXNLOG();
			aduserid = (String) req.getSession().getAttribute(SessionUtil.CUSIDN);
			aduserid = StrUtil.isNotEmpty(aduserid)?aduserid:"";
			if(StrUtil.isNotEmpty(aduserid)) {
				aduserid = new String( Base64.getDecoder().decode(aduserid) ,"utf-8");
			}
			adguid = (String) req.getSession().getAttribute(SessionUtil.ADGUID) ;
			adguid = StrUtil.isNotEmpty(adguid)?adguid:"";
//			因後台查詢成功條件是"";
			adexcode = "0".equals(adexcode) ?"":adexcode;
			
//			TODO 測試用
//			aduserid="A123456814";
			
			aduserip = WebUtil.getIpAddr(req);
			log.trace(ESAPIUtil.vaildLog("aduserid >> " + aduserid )); 
			log.trace(ESAPIUtil.vaildLog("adguid >> " + adguid )); 
//			po = CodeUtil.objectCovert(TXNLOG.class, data);
			po = txnlogFileMaping(data, adopid, po);
			
			po.setADTXNO(UUID.randomUUID().toString());
			po.setADUSERID(aduserid);
			po.setADOPID(adopid);
			po.setADUSERIP(aduserip);
			po.setADGUID(adguid);
			po.setADEXCODE(adexcode);
			po.setADCONTENT(CodeUtil.toJsonCustom(data));
//			po.setADCONTENT(CodeUtil.toJson(data));
			po.setLASTDATE(new DateTime().toString("yyyyMMdd"));
			po.setLASTTIME(new DateTime().toString("HHmmss"));
//			TODO 等欄位確定開之後，在打開
			po.setPSTIMEDIFF(pstimediff);
//			if(!adopid.startsWith("F00")) {
//				po = loadTransferParam(data, adopid, po);
//			}
			po = loadSPParam(data, adopid, po);
			log.debug("TXNLOG>>{}",po.toString());
			txnlogdao.save(po);
		} catch (Exception e) {
			log.error(e.toString());
		}
		
	}
	
	
	
	
	
	
	
	/**
	 * 針對轉入銀行代碼 、約定非約定
	 * @param data
	 * @param adopid
	 * @param po
	 * @return
	 */
	public TXNLOG loadSPParam(Map<String,String> data ,String adopid ,TXNLOG po) {
		String dpagacnoStr = "" ,adcurrency = "" ,adtxacno ,logintype;
		String adsvbh = "" , adagreef ="" ,adreqtype ="" ,adtxamt ="" ,fgtxdate ,fgtxway;
		try {
			
			dpagacnoStr = data.get("DPAGACNO");
			
			if(StrUtil.isNotEmpty(dpagacnoStr)) {
				Map<String,String> tmp =  CodeUtil.fromJson(dpagacnoStr, Map.class);
				adagreef = StrUtil.isNotEmpty(tmp.get("AGREE")) ?tmp.get("AGREE") :"" ;
				adsvbh = StrUtil.isNotEmpty(tmp.get("BNKCOD")) ?tmp.get("BNKCOD") :"ADSVBH" ;
				po.setADAGREEF(adagreef);
				po.setADSVBH(adsvbh);
			}
			fgtxdate = StrUtil.isNotEmpty(data.get("FGTXDATE"))?data.get("FGTXDATE"):"";
			if("2".equals(fgtxdate) || "3".equals(fgtxdate) ) {
//				外幣預約是B
				if(!adopid.startsWith("F00")) {
					adreqtype = "S";
					po.setADREQTYPE(adreqtype);
				}
			}
			
		} catch (Exception e) {
			log.error(e.toString());
		}
		return po;
	}
	
	

	
	/**
	 * 把送出的資料
	 * @param data
	 * @param adopid
	 * @param po
	 * @return
	 */
	public TXNLOG txnlogFileMaping(Map<String,String> data ,String adopid ,TXNLOG po) {
		Map<String ,String> map = null;
		try {
			try {
				map = SpringBeanFactory.getBean(adopid.toLowerCase()+"-tx");
			} catch (Exception e) {
				log.error("cant not find Bean user d4...{}",adopid.toLowerCase()+"-tx");
				map = SpringBeanFactory.getBean("d4-tx");
			}
			Map<String ,String> map2 = new HashMap<String,String>();
			for(String key :map.keySet()) {
				map2.put(key, data.get(map.get(key)));
			}
			log.debug("map2>>{}",map2);
			data.putAll(map2);
			log.debug("data>>{}",data);
			po = CodeUtil.objectCovert(TXNLOG.class, data);
			log.debug("po>>{}",po.toString());
		} catch (Exception e) {
			log.error(e.toString());
		}
		return po;
	}
	
	
	public void writeSysLog(Map<String,String> data ,String adopid ,String adexcode,DateTime d1,DateTime d2 ,String pstimediff  ) {
		String aduserid = "";
		String aduserip = "";
		String adguid = "";
		SYSLOG po =null;
		try {
			po = new SYSLOG();
			aduserid = (String) req.getSession().getAttribute(SessionUtil.CUSIDN);
			aduserid = StrUtil.isNotEmpty(aduserid)?aduserid:"";
			if(StrUtil.isNotEmpty(aduserid)) {
				aduserid = new String( Base64.getDecoder().decode(aduserid) ,"utf-8");
			}
			adguid = (String) req.getSession().getAttribute(SessionUtil.ADGUID) ;
			adguid = StrUtil.isNotEmpty(adguid)?adguid:"";
//			因後台查詢成功條件是"";
			adexcode = "0".equals(adexcode) ?"":adexcode;
			
//			TODO 測試用
//			aduserid="A123456814";
			
			aduserip = WebUtil.getIpAddr(req);
			log.trace(ESAPIUtil.vaildLog("aduserid >>" + aduserid )); 
			log.trace(ESAPIUtil.vaildLog("adguid >> " + adguid )); 
			po = CodeUtil.objectCovert(SYSLOG.class, data);
			po.setADTXNO(UUID.randomUUID().toString());
			po.setADUSERID(aduserid);
			po.setADOPID(adopid);
			po.setADUSERIP(aduserip);
			po.setADGUID(adguid);
			po.setADEXCODE(adexcode);
			po.setADCONTENT(CodeUtil.toJsonCustom(data));
//			po.setADCONTENT(CodeUtil.toJson(data));
			po.setADFDATE(d1.toString("yyyyMMdd"));
			po.setADTTIME(d1.toString("HHmmssSSS"));
			po.setADTDATE(d2.toString("yyyyMMdd"));
			po.setADFTIME(d2.toString("HHmmssSSS"));
			po.setADUSERIP(aduserip);
			po.setADSEQ(1);
			po.setADLOGTYPE("1");
//			TODO 等欄位確定開之後，在打開
//			po.setPSTIMEDIFF(pstimediff);
			log.debug("SYSLOG>>{}",ESAPIUtil.vaildLog(po.toString()));
			syslogdao.save(po);
		} catch (Exception e) {
			log.error(e.toString());
		}
		
		
	}
	
	
	
	/**
	 * 取得部分交易電文參數
	 * @param data
	 * @param adopid
	 * @param po
	 * @return
	 */
	public TXNLOG loadTransferParam(Map<String,String> data ,String adopid ,TXNLOG po) {
		String dpagacnoStr = "" ,adcurrency = "" ,adtxacno ,logintype;
		String adsvbh = "" , adagreef ="" ,adreqtype ="" ,adtxamt ="" ,fgtxdate ,fgtxway;
		try {
			dpagacnoStr = data.get("DPAGACNO");
			if(StrUtil.isNotEmpty(dpagacnoStr)) {
				Map<String,String> tmp =  CodeUtil.fromJson(dpagacnoStr, Map.class);
//				data.putAll(tmp);
				adagreef = StrUtil.isNotEmpty(tmp.get("AGREE")) ?tmp.get("AGREE") :"" ;
				adsvbh = StrUtil.isNotEmpty(tmp.get("BNKCOD")) ?tmp.get("BNKCOD") :"ADSVBH" ;
				
				po.setADAGREEF(adagreef);
				po.setADSVBH(adsvbh);
				
			}
			fgtxdate = StrUtil.isNotEmpty(data.get("FGTXDATE"))?data.get("FGTXDATE"):"";
			if("2".equals(fgtxdate) || "3".equals(fgtxdate) ) {
				adreqtype = "S";
			}
			
			//TODO 要求每個轉帳交易都要加  (ADCURRENCY)
			adcurrency = StrUtil.isNotEmpty(data.get("ADCURRENCY"))?data.get("ADCURRENCY"):"";
			adtxamt = StrUtil.isNotEmpty(data.get("AMOUNT"))?data.get("AMOUNT"):"";
			fgtxway = StrUtil.isNotEmpty(data.get("FGTXWAY"))?data.get("FGTXWAY"):"";
			adtxacno = StrUtil.isNotEmpty(data.get("ACN"))?data.get("ACN"):"";
			logintype = StrUtil.isNotEmpty(data.get("LOGINTYPE"))?data.get("LOGINTYPE"):"";
			
			po.setADCURRENCY(adcurrency);
			po.setADREQTYPE(adreqtype);
			po.setADTXAMT(adtxamt);
			po.setFGTXWAY(fgtxway);
			po.setADTXACNO(adtxacno);
			po.setLOGINTYPE(logintype);
		} catch (Exception e) {
			log.error(e.toString());
		}
		return po;
	}
	
	
	
	/**
	 * 把API名稱調成跟NNB一致以利後台查詢
	 * 
	 * @param api
	 * @return
	 */
	public String apiNameRetouch(String api) {
		
		if(StrUtil.isEmpty(api)) {
			return api;
		}
		
		
		switch (api) {
		case "C016_Public":
			api = "C016";
//			data.put("ADOPID", "C016");
			break;

		default:
			break;
		}
		//F001T >> F001
		if(api.startsWith("F00") && api.length()>4) {
			api = api.substring(0,4);
		}
		
		
		return api;
		
	}
}
