package tw.com.fstop.nnb.rest.bean;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="BlueStar")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"TXID","ABEND","AppId","TXBANK","TXACN","TXIDN","TXMTEL","RTCHK","RTACN","RTOPS","RTSEQ","RCODE"})
public class N194_REST_RS {
	
	private String TXID;	//電文編號
    private String ABEND;	//回應代碼
    private String AppId;	//系統別
    private String TXBANK;	//行庫別
    private String TXACN;	//帳號
    private String TXIDN;	//統一編號
    private String TXMTEL;	//行動電話
    private String RTCHK;	//核驗結果
    private String RTACN;	//帳號核驗結果
    private String RTOPS;	//開戶狀態
    private String RTSEQ;	//交易序號
    private String RCODE;	//回應代碼
    
    
	public String getTXID() {
		return TXID;
	}
	public void setTXID(String tXID) {
		TXID = tXID;
	}
	public String getABEND() {
		return ABEND;
	}
	public void setABEND(String aBEND) {
		ABEND = aBEND;
	}
	public String getAppId() {
		return AppId;
	}
	public void setAppId(String appId) {
		AppId = appId;
	}
	public String getTXBANK() {
		return TXBANK;
	}
	public void setTXBANK(String tXBANK) {
		TXBANK = tXBANK;
	}
	public String getTXACN() {
		return TXACN;
	}
	public void setTXACN(String tXACN) {
		TXACN = tXACN;
	}
	public String getTXIDN() {
		return TXIDN;
	}
	public void setTXIDN(String tXIDN) {
		TXIDN = tXIDN;
	}
	public String getTXMTEL() {
		return TXMTEL;
	}
	public void setTXMTEL(String tXMTEL) {
		TXMTEL = tXMTEL;
	}
	public String getRTCHK() {
		return RTCHK;
	}
	public void setRTCHK(String rTCHK) {
		RTCHK = rTCHK;
	}
	public String getRTACN() {
		return RTACN;
	}
	public void setRTACN(String rTACN) {
		RTACN = rTACN;
	}
	public String getRTOPS() {
		return RTOPS;
	}
	public void setRTOPS(String rTOPS) {
		RTOPS = rTOPS;
	}
	public String getRTSEQ() {
		return RTSEQ;
	}
	public void setRTSEQ(String rTSEQ) {
		RTSEQ = rTSEQ;
	}
	public String getRCODE() {
		return RCODE;
	}
	public void setRCODE(String rCODE) {
		RCODE = rCODE;
	}
	
	
	@Override
	public String toString() {
		return "N194_REST_RS [TXID=" + TXID + ", ABEND=" + ABEND + ", AppId=" + AppId + ", TXBANK=" + TXBANK
				+ ", TXACN=" + TXACN + ", TXIDN=" + TXIDN + ", TXMTEL=" + TXMTEL + ", RTCHK=" + RTCHK + ", RTACN="
				+ RTACN + ", RTOPS=" + RTOPS + ", RTSEQ=" + RTSEQ + ", RCODE=" + RCODE + "]";
	}
    
}
