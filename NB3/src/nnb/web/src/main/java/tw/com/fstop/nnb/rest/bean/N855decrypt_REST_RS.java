package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N855decrypt_REST_RS extends BaseRestBean_TW implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1593067062030722332L;
	private String	DivData ;
	private String	CustIdnBan ;
	private String	CustBillerAcnt ;
	private String	CustBankAcnt ;
	private String	CustIdnBan_Dec ;
	private String	CustBillerAcnt_Dec ;
	private String	CustBankAcnt_Dec ;
	public String getDivData() {
		return DivData;
	}
	public void setDivData(String divData) {
		this.DivData = divData;
	}
	public String getCustIdnBan() {
		return CustIdnBan;
	}
	public void setCustIdnBan(String custIdnBan) {
		CustIdnBan = custIdnBan;
	}
	public String getCustBillerAcnt() {
		return CustBillerAcnt;
	}
	public void setCustBillerAcnt(String custBillerAcnt) {
		CustBillerAcnt = custBillerAcnt;
	}
	public String getCustBankAcnt() {
		return CustBankAcnt;
	}
	public void setCustBankAcnt(String custBankAcnt) {
		CustBankAcnt = custBankAcnt;
	}
	public String getCustIdnBan_Dec() {
		return CustIdnBan_Dec;
	}
	public void setCustIdnBan_Dec(String custIdnBan_Dec) {
		CustIdnBan_Dec = custIdnBan_Dec;
	}
	public String getCustBillerAcnt_Dec() {
		return CustBillerAcnt_Dec;
	}
	public void setCustBillerAcnt_Dec(String custBillerAcnt_Dec) {
		CustBillerAcnt_Dec = custBillerAcnt_Dec;
	}
	public String getCustBankAcnt_Dec() {
		return CustBankAcnt_Dec;
	}
	public void setCustBankAcnt_Dec(String custBankAcnt_Dec) {
		CustBankAcnt_Dec = custBankAcnt_Dec;
	}
}
