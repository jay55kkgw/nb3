package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class NA40_1_REST_RS extends BaseRestBean implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 6338133490522859916L;
	
	private String OFFSET;
	private String HEADER;
	private String occurMsg;
	private String CCTXTIME;
	
	public String getOFFSET() {
		return OFFSET;
	}
	public void setOFFSET(String oFFSET) {
		OFFSET = oFFSET;
	}
	public String getHEADER() {
		return HEADER;
	}
	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}
	public String getOccurMsg() {
		return occurMsg;
	}
	public void setOccurMsg(String occurMsg) {
		this.occurMsg = occurMsg;
	}
	public String getCCTXTIME() {
		return CCTXTIME;
	}
	public void setCCTXTIME(String cCTXTIME) {
		CCTXTIME = cCTXTIME;
	}
}
