package tw.com.fstop.nnb.spring.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.common.ResultCode;
import tw.com.fstop.idgate.bean.N070C_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N070C_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N070_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N070_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N830_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N830_IDGATE_DATA_VIEW;
import tw.com.fstop.nnb.aop.Dialog;
import tw.com.fstop.nnb.custom.annotation.ACNSET;
import tw.com.fstop.nnb.custom.annotation.ACNVERIFY;
import tw.com.fstop.nnb.custom.annotation.ACNVERIFY0;
import tw.com.fstop.nnb.service.Acct_Phone_Transfer_Service;
import tw.com.fstop.nnb.service.Acct_Transfer_Service;
import tw.com.fstop.nnb.service.DaoService;
import tw.com.fstop.nnb.service.IdGateService;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

@Controller
@RequestMapping(value = "/NT/ACCT/PHONE_TRANSFER")
@SessionAttributes({ SessionUtil.PD, SessionUtil.CUSIDN, SessionUtil.DPMYEMAIL, SessionUtil.TRANSFER_DATA,
		SessionUtil.TRANSFER_CONFIRM_TOKEN, SessionUtil.TRANSFER_RESULT_TOKEN, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN,
		SessionUtil.BACK_DATA, SessionUtil.PRINT_DATALISTMAP_DATA, SessionUtil.CONFIRM_LOCALE_DATA,
		SessionUtil.RESULT_LOCALE_DATA,SessionUtil.AUTHORITY,SessionUtil.IDGATE_TRANSDATA })
public class Acct_Phone_Transfer_Controller {

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private DaoService daoservice;
	
	@Autowired
	IdGateService idgateservice;
	
	@Autowired
	String isSessionID;

	@Autowired
	private Acct_Phone_Transfer_Service acct_phone_transfer_service;
	
	/**
	 * 手機轉帳輸入頁
	 */
	@RequestMapping(value = "/p_transfer")
	public String transfer(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
			//預設回傳值
			BaseResult bs = new BaseResult();
			//預設錯誤頁
			String target = "/error";
			
		try {
			//取得目前時間(view需要)
			String today = DateUtil.getCurentDateTime("yyyy/MM/dd");;
			
			//源碼
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			//判斷是否點了回上一頁
			if ("Y".equals(okMap.get("back"))) 
				model.addAttribute("bk_key", "Y");
			
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			
			//拿Token，避免直接跳過輸入頁進入確認頁
			bs = acct_phone_transfer_service.getTxToken();

			//取得統一編號
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
				
			//黑名單特殊註記 
			String RISKFLAG=daoservice.getRiskFlag(cusidn);
			bs.addData("RISKFLAG",RISKFLAG);		
			
			//IDGATE身分驗證
			String idgateUserFlag="N";
			Map<String, Object> IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model,SessionUtil.IDGATE_TRANSDATA, null);
			try {
				if (IdgateData == null) {
					IdgateData = new HashMap<String, Object>();
				}
				BaseResult tmp=idgateservice.checkIdentity(cusidn);
				if (tmp.getResult()) {
					idgateUserFlag = "Y";
				}
				tmp.addData("idgateUserFlag",idgateUserFlag);
				IdgateData.putAll((Map<String, String>) tmp.getData());
				SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);
			}catch(Exception e) {
				log.debug("idgateUserFlag error {}",e);
			}			
			
			//第三階段說明
			String isProd = SpringBeanFactory.getBean("isProd");
			bs.addData("isProd",isProd);			
			bs.setResult(true);
			log.trace("BS DATA >> "+CodeUtil.toJson(bs.getData()));
			if (bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN,
						((Map<String, String>) bs.getData()).get("TXTOKEN"));
				SessionUtil.addAttribute(model, "today", today);
				model.addAttribute("RISKFLAG",((Map<String, String>) bs.getData()).get("RISKFLAG"));
				model.addAttribute("idgateUserFlag",idgateUserFlag);
				model.addAttribute("isProd",isProd);
			}
		} catch (Exception e) {
			log.error("p_transfer error >> {}", e);
			bs.setSYSMessage(ResultCode.SYS_ERROR);
		} finally {
			if (bs.getResult()) {
				target = "/acct/p_transfer";
			} else {
				bs.setNext("/NT/ACCT/PHONE_TRANSFER/p_transfer");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * 轉帳確認頁
	 * 
	 * @return
	 */
	@RequestMapping(value = "/p_transfer_confirm")
	public String p_transfer_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = new BaseResult();
		//預設錯誤頁面
		String target = "/error";
		//預設錯誤頁的下一頁
		String next = "/NT/ACCT/PHONE_TRANSFER/p_transfer";
		//初始化ikey所需的參數
		String jsondc = "";
		//初始化源碼的Map
		Map<String, String> okMap = new HashMap<String, String>();

		try {
			okMap = ESAPIUtil.validStrMap(reqParam);
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam), "UTF-8");
			log.trace(ESAPIUtil.vaildLog("p_transfer_confirm.jsondc >> " + jsondc));
			okMap.put("jsondc", jsondc);
			log.trace(ESAPIUtil.vaildLog("p_transfer_confirm.okMap >> " + CodeUtil.toJson(okMap)));

			// 回上一頁重新賦值
			if (okMap.containsKey("previous")) {
				Map m = (Map) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				okMap = m;
			}

			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if (!bs.getResult()) {
					// TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				} else {
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			}

			if (!hasLocale) {
//				TODO 壓力測試後 移除
				if (!"T".equals(isSessionID)) {
					if (StrUtil.isNotEmpty(okMap.get("TOKEN")) && okMap.get("TOKEN")
							.equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null))) {
						bs.setResult(Boolean.TRUE);
					}
					log.trace("bs.getResult() >>{}", bs.getResult());
					if (!bs.getResult()) {
						log.trace("throw new Exception()");
						throw new Exception();
					}
				} // isSessionID END
				//初始化bs
				bs.reset();
				//取統一編號
				String cusidn = new String(
						Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
						"utf-8");
				
				bs = acct_phone_transfer_service.transfer_confirm(okMap, cusidn);
				log.trace("IDGATE DATA >>"+CodeUtil.toJson(bs.getData()));
				
				// IDGATE 交易資料加工後放進Session用以之後驗證
	    		String adopid = "N070C";
	    		String title = "您有一筆-手機號碼轉帳交易-金額"+((Map<String, String>) bs.getData()).get("AMOUNT");				
				Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
				Map<String,String> result = new HashMap((Map< String,String>) bs.getData());
				model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
				if("Y".equals(IdgateData.get("idgateUserFlag"))) {
	            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
	            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
	            	result.put("DPBHNO", result.get("DPBHNO").split("-")[0]);
	            	result.put("AMOUNT", NumericUtil.unfmtAmount(result.get("AMOUNT")));
	            	IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N070C_IDGATE_DATA.class, N070C_IDGATE_DATA_VIEW.class, result));
	            	SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);
					log.trace("N070C_IDGATE >>"+CodeUtil.toJson(IdgateData));
				}
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
	            
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
			}

		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("transfer_confirm error >> {}", e);
		} finally {
			if (bs.getResult()) {
				target = "/acct/p_transfer_confirm";
				model.addAttribute(SessionUtil.TRANSFER_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN,
						((Map<String, Object>) bs.getData()).get("TXTOKEN"));

				// 設定回上一頁的url,配合confirm頁面的js
				model.addAttribute("previous", "/NT/ACCT/PHONE_TRANSFER/p_transfer");
				// 回填使用DATA
				log.debug(ESAPIUtil.vaildLog("okMap.AMOUNT >> " + okMap.get("AMOUNT")));
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
			} else {
				bs.setNext(next);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;

	}

	/**
	 * 轉帳結果頁
	 * 
	 * @return
	 */
	@RequestMapping(value = "/p_transfer_result")
	public String p_transfer_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		String next = "/NT/ACCT/PHONE_TRANSFER/p_transfer";
		String previous = "/NT/ACCT/PHONE_TRANSFER/p_transfer_confirm";
		BaseResult bs = new BaseResult();
		String jsondc = "";
		String pkcs7Sign = "";
		try {
			log.debug(ESAPIUtil.vaildLog("transfer_result.reqParam >> " + CodeUtil.toJson(reqParam)));
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			// TODO 1.要先驗證TOKEN的正確性
			jsondc = reqParam.get("jsondc");
			pkcs7Sign = reqParam.get("pkcs7Sign");
			reqParam.remove("jsondc");
			reqParam.remove("pkcs7Sign");

			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			okMap.put("jsondc", jsondc);
			okMap.put("pkcs7Sign", pkcs7Sign);
			log.debug(ESAPIUtil.vaildLog("transfer_result.okMap >> " + CodeUtil.toJson(okMap)));

			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}

			if (!hasLocale) {
//				TODO 壓力測試後 移除
				if (!"T".equals(isSessionID)) {
					if (StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && okMap.get("TXTOKEN")
							.equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null))) {
						bs.setResult(Boolean.TRUE);
					}

					if (!bs.getResult()) {
						log.error(ESAPIUtil.vaildLog("transfer_result TXTOKEN 不正確，TXTOKEN >> " + okMap.get("TXTOKEN")));
						throw new Exception();
					}
					bs.reset();

					// 2.要驗證token是否與完成交易的TOKEN重複
					if (StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && !okMap.get("TXTOKEN")
							.equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null))) {
						bs.setResult(Boolean.TRUE);
					}
					if (!bs.getResult()) {
						log.error(ESAPIUtil
								.vaildLog("transfer_result TXTOKEN 不正確，或重複交易，TXTOKEN >> " + okMap.get("TXTOKEN")));
						throw new Exception();
					}
				} // isSessionID END
				bs.reset();
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String, String> transfer_data = (Map<String, String>) bs.getData();

				okMap.putAll(transfer_data);
				//----防止密碼錯誤欄位被覆蓋---start
				okMap.put("jsondc",jsondc);
				okMap.put("pkcs7Sign",pkcs7Sign);
				okMap.put("PINNEW",reqParam.get("PINNEW"));
				okMap.put("ISSUER",reqParam.get("ISSUER"));
				okMap.put("ACNNO",reqParam.get("ACNNO"));
				okMap.put("TRMID",reqParam.get("TRMID"));
				okMap.put("iSeqNo",reqParam.get("iSeqNo"));
				okMap.put("ICSEQ",reqParam.get("ICSEQ"));
				okMap.put("TAC",reqParam.get("TAC"));
				okMap.put("FGTXWAY",reqParam.get("FGTXWAY"));
				//----防止密碼錯誤欄位被覆蓋---end
				bs.reset();
				
				//IDgate驗證
				try {
					if(okMap.get("FGTXWAY").equals("7")) {
						Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
						IdgateData=(Map<String, Object>) IdgateData.get("N070C_IDGATE");
						okMap.put("sessionID",(String) IdgateData.get("sessionid"));
						okMap.put("txnID",(String) IdgateData.get("txnID"));
						okMap.put("idgateID",(String) IdgateData.get("IDGATEID"));
					}
				}catch(Exception e) {
					log.error("IDGATE ValidateFail>>{}",bs.getResult());
				}	
				
				//用於結果頁顯示idgate的dialog
				Map<String,Object>IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
				model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
				
				bs = acct_phone_transfer_service.transfer_result(okMap, cusidn);

				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			model.addAttribute("transfer_result_data", bs);

			// E004 是密碼錯誤 要讓使用者可以回上一步
			if ("E004".equals(bs.getMsgCode())) {
				bs.setPrevious(previous);
				model.addAttribute(BaseResult.ERROR, bs);
				// 移除舊的錯誤密碼
				okMap.remove("PINNEW");
				// 回填使用DATA
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
			}

		} catch (Exception e) {
			log.error("transfer_result error >> {}", e);
		} finally {
			// 解決Trust Boundary Violation
			log.debug(ESAPIUtil.vaildLog("finally.reqParam >> " + CodeUtil.toJson(reqParam)));
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

			if (bs.getResult()) {
				// 交易成功要存之前的token 在Session 以利下次比對是否重複交易
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TXTOKEN"));
				target = "/acct/p_transfer_result";
			} else {
				bs.setNext(next);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
}
