package tw.com.fstop.nnb.service;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.common.ResultCode;
import tw.com.fstop.nnb.rest.bean.Publickey_REST_RQ;
import tw.com.fstop.nnb.rest.bean.Publickey_REST_RS;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtil;

@Service
public class GetPublicKey_Service extends Base_Service {

	@Autowired
	String ms_tw_host ;
	@Autowired
	String ms_tw_port ;
	@Autowired
	String ms_tw_path ;
	
	String publicKey = null;

	@PostConstruct
	public void initPublicKey() {
//		BaseResult bs = null;
		log.info("initPublicKey...");
		
		try {
//			bs = getPublickey_REST_NoCache();
			Map <String,Object> bs = getPublickey_REST_NoCacheII();
			if(bs != null && (Boolean)bs.get("result") ) {
//				if(bs != null && bs.getResult() ) {
				log.info("getPublicKey...");
				publicKey = ((Publickey_REST_RS)bs.get("data")).getPublicKey();
//				publicKey = (String) ((Map<String,Object>)bs.get("data")).get("publicKey");
			}
		} catch (Throwable e) {
			log.error("initPublicKey.error>>{}",e);
		}
		
	}
	
	public BaseResult updatePublicKey() {
		log.info("updatePublicKey...");
		BaseResult bs = new BaseResult();
		
		try {
			bs = getPublickey_REST_NoCache();
			if(bs != null && bs.getResult() ) {
				log.info("updatePublicKey Result...");
				publicKey = (String) ((Map<String,Object>)bs.getData()).get("publicKey");
//				publicKey = ((Publickey_REST_RS)bs.getData()).getPublicKey();;
			}
		} catch (Throwable e) {
			log.error("initPublicKey.error>>{}",e);
		}
		return bs;
		
	}
	
	
    @Cacheable(value="E2ECache",key="'E2E_Publickey'",unless="#result.result == false")
    public BaseResult getPublickey_REST() {
    	
    	log.info("getPubkicKey_REST ..");
    	BaseResult bs = null;
    	Publickey_REST_RS rs = null;
    	Publickey_REST_RQ rq = null;
    	Map <String,String>temp=new HashMap<String,String>();
    	try {
    		bs = new BaseResult();
    		if(StrUtil.isNotEmpty(publicKey)) {
    			log.info("publicKey not null");
    			Map<String,String> data = new HashMap<String,String>();
    			bs.setResult(Boolean.TRUE);
    			bs.setMessage("0", "ok");
    			data.put("publicKey", publicKey);
    			bs.setData(data);
    			return bs;
    		}
    		rq = CodeUtil.objectCovert(Publickey_REST_RQ.class,temp);
    		rs = restutil.send(rq, Publickey_REST_RS.class,rq.getAPI_Name(rq.getClass(), rq.getMs_Channel()));
    		log.debug(ESAPIUtil.vaildLog("Publickey_REST_RS>>{}"+ rs));
    		if (rs != null) {
    			CodeUtil.convert2BaseResult(bs, rs);
    			if(bs !=null && bs.getResult()) {
    				publicKey = rs.getPublicKey();
    			}
    		} else {
    			log.error("Publickey_REST_RS is null");
    			bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
    		}
    	} catch (Exception e) {
    		log.error("{}", e);
    		bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
    	}
    	this.getMessageByMsgCode(bs);
    	return bs;
    }
    /**
     * 
     * @return
     */
    public BaseResult getPublickey_REST_NoCache() {
    	
    	log.info("getPubkicKey_REST ..");
    	BaseResult bs = null;
    	Publickey_REST_RS rs = null;
    	Publickey_REST_RQ rq = null;
    	Map <String,String>temp=new HashMap<String,String>();
    	try {
    		bs = new BaseResult();
    		rq = CodeUtil.objectCovert(Publickey_REST_RQ.class,temp);
    		rs = restutil.send(rq, Publickey_REST_RS.class,rq.getAPI_Name(rq.getClass(), rq.getMs_Channel()));
    		log.debug(ESAPIUtil.vaildLog("Publickey_REST_RS>>{}"+ rs));
    		if (rs != null) {
    			CodeUtil.convert2BaseResult(bs, rs);
    		} else {
    			log.error("Publickey_REST_RS is null");
    			bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
    		}
    	} catch (Exception e) {
    		log.error("{}", e);
    		bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
    	}
    	this.getMessageByMsgCode(bs);
    	return bs;
    }
    
    
    public Map<String ,Object> getPublickey_REST_NoCacheII() {
    	
    	log.info("getPublickey_REST_NoCacheII ..");
    	Publickey_REST_RS rs = null;
    	Publickey_REST_RQ rq = null;
    	Map <String,String>temp=new HashMap<String,String>();
    	Map <String,Object> rtMap = new HashMap<String,Object>();
    	try {
    		String api = getUrl()+  Publickey_REST_RQ.class.getSimpleName().replace("_REST_RQ", "");
    		rs = restutil.sendForPostConstruct(new HashMap<String,String>(), Publickey_REST_RS.class,api);
    		log.debug(ESAPIUtil.vaildLog("Publickey_REST_RS>>{}"+ rs));
    		if (rs != null) {
    			rtMap.put("msgCode", rs.getMsgCode());
    			rtMap.put("msgName", rs.getMsgName());
    			Boolean result = "0".equals(rs.getMsgCode()) ? Boolean.TRUE:Boolean.FALSE;
    			rtMap.put("result", result);
    			rtMap.put("data", rs);
    		} else {
    			log.error("Publickey_REST_RS is null");
    			rtMap.put("msgCode", ResultCode.SYS_ERROR_TEL);
    		}
    	} catch (Exception e) {
    		log.error("{}", e);
    		rtMap.put("msgCode", ResultCode.SYS_ERROR_TEL);
    	}
    	return rtMap;
    }


	public String getUrl() {
		String url = "";
		url = ms_tw_host +":"+ms_tw_port+"/"+ms_tw_path;
		if(StrUtil.isNotEmpty(url) && url.endsWith("/") == false) {
			url = url+"/";
		}
		
		return url;
	}


}