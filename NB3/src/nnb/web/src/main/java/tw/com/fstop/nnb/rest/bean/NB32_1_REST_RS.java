package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class NB32_1_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3595151196091474660L;
	private String BRHCOD;
	private String BRHNAME;
	private String ACNTYPE;
	private String CUSIDN;
	private String UID;
	private String ACN;
	private String MSGCOD;
	public String getBRHCOD() {
		return BRHCOD;
	}
	public void setBRHCOD(String bRHCOD) {
		BRHCOD = bRHCOD;
	}
	public String getACNTYPE() {
		return ACNTYPE;
	}
	public void setACNTYPE(String aCNTYPE) {
		ACNTYPE = aCNTYPE;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getMSGCOD() {
		return MSGCOD;
	}
	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}
	public String getBRHNAME() {
		return BRHNAME;
	}
	public void setBRHNAME(String bRHNAME) {
		BRHNAME = bRHNAME;
	}
}
