package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fstop.orm.po.ADMCURRENCY;
import fstop.orm.po.TXNFUNDDATA;
import tw.com.fstop.nnb.service.DaoService;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.TxnFundDataDao;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

public class C014_REST_RS extends BaseRestBean implements Serializable
{
	Logger log = LoggerFactory.getLogger(this.getClass());

	private static final long serialVersionUID = 8600126206338254151L;
	
	private String CUSIDN;			// 身份證號
	private String DATE;			// 付款日期
	private String CUSNAME;			// 姓名
	private String TRANSCODE;		// 基金代碼
	private String CDNO;		// 信託號碼
	private String CRY;				// 信託金額幣別
	private String TRANSCRY;		// 基金計價幣別
	private String TRADEDATE;		// 生效日期(交易日期)
	private String FUNDTYPE;		// 交易類別
	private String FUNDFLAG;		// 是否未分配
	private String INTRANSCODE;		// 轉入基金代碼
	private String FUNDAMT;			// 信託金額
	private String PRICE1;			// 價格(申購單位淨值、轉出基金單位淨值)
	private String PRICE2;			// 轉入價格(轉入基金單位淨值)
	private String EXRATE;			// 匯率
	private String EXAMT;			// 外幣金額
	private String UNIT;			// 單位數
	private String OKCOUNT;		 	// 扣款成功次數
	private String TXUNIT;		 	// 轉入單位數
	private String FCA1;			// 內扣手續費
	private String AMT1;			// 轉出金額
	private String AMT2;		 	// 兌換後金額
	private String AMT3;		 	// 申購金額(轉換手續費-本行)
	private String FCA2;			// 手續費
	private String FXFEE;		 	// 匯費
	private String FUNDACN;		 	// 帳號
	private String AMT4;		 	// 每單位分配金額
	private String AMT5;		 	// 代扣稅款
	private String AMT6;		 	// 單位數乘碼
	private String INTRANSCRY;		// 轉入計價幣別
	private String AMT7;		 	// 給付淨額
	private String TRUSTCURRENCY;	// 轉入信託幣別
	private String SHORTTRADEFEE;	// 短線費用
	private String NHITAX;		 	// 健保費
	private String AUTOR;		 	// 自動贖回註記
	
	// 電文沒回但ms_tw回應的資料
	private String CMQTIME;			// 查詢時間
	private String __OCCURS;		// __OCCURS

	// 電文回應但目前用不到的資料
	private String SNID;			// System Network Identifier
	private String TXNDIID;			// TXN Destination Institute ID
	private String TXNSIID;			// TXN Source Institute ID
	private String TXNIDT;			// TXN Initiate Date and Time
	private String SSUP;			// System Supervisory
	private String SYNCCHK;			// Sync. Check Item
	private String ACF;				// Address Control Field
	private String MSGTYPE;			// Message Type
	private String SYSTRACE;		// System Trace Audit
	private String BITMAPCFG;		// Bit Map Configuration
	private String PROCCODE;		// Processing Code
	private String RESPCOD;			// Response Code
	private String MAC;				// MAC

	// ***電文沒回應但要給頁面使用的資料***
	private String redemptionIncome;		// 贖回收益
	private String exRightUnit;				// 除權後信託單位數
	
	// 中文幣別
	private String CRY_CRYNAME;				// 用 CRY 		查詢中文幣別
	private String TRANSCRY_CRYNAME;		// 用 TRANSCRY 	查詢中文幣別
	private String INTRANSCRY_CRYNAME;		// 用 INTRANSCRY	查詢中文幣別
	private String NEWCRY_CRYNAME;			// 用 NEWCRY 	查詢中文幣別

	// 基金名稱
	private String TRANSCODE_FUNDLNAME;		// 用 TRANSCODE	   查詢基金名稱
	private String INTRANSCODE_FUNDLNAME;	// 用 INTRANSCODE 查詢基金名稱

	// ***電文沒回應但要給頁面使用的資料 END***
	
	/**
	 * 查詢中文幣別
	 * 
	 * @param cry
	 * @return
	 */
	public String getCryName(String cry)
	{
		String result = cry;
		try
		{
			log.debug("CRY >> {}", cry);
			DaoService daoService = null;
			ADMCURRENCY po = null;
			// 檢查查詢條件
			if(StrUtil.isNotEmpty(cry))
			{
				// get Dao bean
				daoService = SpringBeanFactory.getBean(DaoService.class);
				log.debug("DaoService >>> {}", daoService);
			}
			
			if(daoService != null)
			{
				// query DB
				po = daoService.getCRY(cry);
				log.debug("ADMCURRENCY >>> {}", po);
			}
			
			// 檢查PK確認是否查到資料
			if(po != null && StrUtil.isNotEmpty(po.getADCURRENCY()))
			{
				result = po.getADCCYNAME();
			}
			else
			{
				log.warn("查無幣別中文名稱 CRY >> {}", cry);
			}
		}
		catch (Exception e)
		{
			log.error("getCryName error. CRY >> {}", cry, e);
		}
		return result;
	}
	
	
	/**
	 * 取得基金全名
	 * 
	 * @param TRANSCODE
	 * @return
	 */
	public String getFundLName(String TRANSCODE)
	{
		String result = TRANSCODE;
		try
		{
			TxnFundDataDao txnFundDataDao = null;
			TXNFUNDDATA po = null;
			// get Dao bean
			txnFundDataDao = SpringBeanFactory.getBean(TxnFundDataDao.class);
			log.debug("TxnFundDataDao >>> {}", txnFundDataDao);
			
			if(txnFundDataDao != null)
			{
				// query DB
				po = txnFundDataDao.getByPK(TRANSCODE);
				log.debug("TXNFUNDDATA >>> {}", po);
			}
			
			if(po != null && StrUtil.isNotEmpty(po.getTRANSCODE()))
			{
				result = po.getFUNDLNAME();
			}
				
		}
		catch (Exception e)
		{
			log.error("getFUNDLNAME error. TRANSCODE >> {}", TRANSCODE, e);
		}
		return result;
	}
	
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	
	// W100089655 to W100089***
	public String getCUSIDN_F() 
	{
		String result = this.CUSIDN;
		try
		{
			result = WebUtil.hideID(result);
		}
		catch (Exception e)
		{
			log.error("getCUSIDN_F error. CUSIDN >> {}", CUSIDN, e);
		}
		return result;
	}
	
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	
	public String getDATE() {
		return DATE;
	}
	
	// 00000000 to 000/00/00
	public String getDATE_F() 
	{
		String result = this.DATE;
		String date = result;
		try
		{
			if(date.length() == 8 && date.startsWith("0"))
			{
				date.replaceFirst("0", "");
				result = DateUtil.addSlash2(date);
			}
		}
		catch (Exception e)
		{
			log.error("getDATE_F error. date >> {}", date, e);
		}
		return result;
	}
	
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	
	public String get__OCCURS() {
		return __OCCURS;
	}
	
	public void set__OCCURS(String __OCCURS) {
		this.__OCCURS = __OCCURS;
	}
	
	public String getCUSNAME() {
		return CUSNAME;
	}
	
	// hideUsername
	public String getCUSNAME_F() 
	{
		String result = this.CUSNAME;
		try
		{
			result = WebUtil.hideusername1Convert(CUSNAME);
		}
		catch (Exception e)
		{
			log.error("getCUSNAME_F error. CUSNAME >> {}", CUSNAME, e);
		}
		return result;
	}
	
	public void setCUSNAME(String cUSNAME) {
		CUSNAME = cUSNAME;
	}
	
	public String getSNID() {
		return SNID;
	}
	
	public void setSNID(String sNID) {
		SNID = sNID;
	}
	
	public String getTXNDIID() {
		return TXNDIID;
	}
	
	public void setTXNDIID(String tXNDIID) {
		TXNDIID = tXNDIID;
	}
	
	public String getTXNSIID() {
		return TXNSIID;
	}
	
	public void setTXNSIID(String tXNSIID) {
		TXNSIID = tXNSIID;
	}
	
	public String getTXNIDT() {
		return TXNIDT;
	}
	
	public void setTXNIDT(String tXNIDT) {
		TXNIDT = tXNIDT;
	}
	
	public String getSSUP() {
		return SSUP;
	}
	
	public void setSSUP(String sSUP) {
		SSUP = sSUP;
	}
	
	public String getSYNCCHK() {
		return SYNCCHK;
	}
	
	public void setSYNCCHK(String sYNCCHK) {
		SYNCCHK = sYNCCHK;
	}
	
	public String getACF() {
		return ACF;
	}
	
	public void setACF(String aCF) {
		ACF = aCF;
	}
	
	public String getMSGTYPE() {
		return MSGTYPE;
	}
	
	public void setMSGTYPE(String mSGTYPE) {
		MSGTYPE = mSGTYPE;
	}
	
	public String getSYSTRACE() {
		return SYSTRACE;
	}
	
	public void setSYSTRACE(String sYSTRACE) {
		SYSTRACE = sYSTRACE;
	}
	
	public String getBITMAPCFG() {
		return BITMAPCFG;
	}
	
	public void setBITMAPCFG(String bITMAPCFG) {
		BITMAPCFG = bITMAPCFG;
	}
	
	public String getPROCCODE() {
		return PROCCODE;
	}
	
	public void setPROCCODE(String pROCCODE) {
		PROCCODE = pROCCODE;
	}
	
	public String getRESPCOD() {
		return RESPCOD;
	}
	
	public void setRESPCOD(String rESPCOD) {
		RESPCOD = rESPCOD;
	}

	public Logger getLog() {
		return log;
	}

	public void setLog(Logger log) {
		this.log = log;
	}

	public String getTRANSCODE() {
		return TRANSCODE;
	}

	public void setTRANSCODE(String tRANSCODE) {
		TRANSCODE = tRANSCODE;
	}

	public String getCDNO() {
		return CDNO;
	}

	// 01019044834 to 010190***34
	public String getCDNO_F() 
	{
		String result = this.CDNO;
		try
		{
			result = WebUtil.hideAccount(CDNO);
		}
		catch (Exception e)
		{
			log.error("getCDNO_F error. CDNO >> {}", CDNO, e);
		}
		return result;
	}

	public void setCDNO(String cDNO) {
		CDNO = cDNO;
	}

	public String getCRY() {
		return CRY;
	}

	public void setCRY(String cRY) {
		CRY = cRY;
	}

	public String getTRANSCRY() {
		return TRANSCRY;
	}

	public void setTRANSCRY(String tRANSCRY) {
		TRANSCRY = tRANSCRY;
	}

	public String getTRADEDATE() {
		return TRADEDATE;
	}
	
	// 01080502 to 108/05/02
	public String getTRADEDATE_F() 
	{
		String result = this.TRADEDATE;
		String tradeDate = result;
		try
		{
			if(tradeDate.length() == 8 && tradeDate.startsWith("0"))
			{
				tradeDate.replaceFirst("0", "");
				result = DateUtil.addSlash2(tradeDate);
			}
		}
		catch (Exception e)
		{
			log.error("getTRADEDATE_F error. TRADEDATE >> {}", TRADEDATE, e);
		}
		return result;
	}

	public void setTRADEDATE(String tRADEDATE) {
		TRADEDATE = tRADEDATE;
	}

	public String getFUNDTYPE() {
		return FUNDTYPE;
	}

	public void setFUNDTYPE(String fUNDTYPE) {
		FUNDTYPE = fUNDTYPE;
	}

	public String getFUNDFLAG() {
		return FUNDFLAG;
	}
	
	/**
	 * 是否未分配 TODO i18n
	 * 
	 * @return
	 */
	public String getFUNDFLAG_F() 
	{
		String result = FUNDFLAG;
		I18n i18n = SpringBeanFactory.getBean("i18n");
		switch(FUNDFLAG) 
		{
			case "Y":
//				result = "否";
				result = i18n.getMsg("LB.D0034_3");
				break;
			case "N":
//				result = "是";
				result = i18n.getMsg("LB.D0034_2");
				break;
			default:
				log.warn("getFUNDFLAG_F error. FUNDFLAG >> {}", FUNDFLAG);
		}
		return result;
	}

	public void setFUNDFLAG(String fUNDFLAG) {
		FUNDFLAG = fUNDFLAG;
	}

	public String getINTRANSCODE() {
		return INTRANSCODE;
	}

	public void setINTRANSCODE(String iNTRANSCODE) {
		INTRANSCODE = iNTRANSCODE;
	}

	public String getFUNDAMT() {
		return FUNDAMT;
	}
	
	// 100000.00 to 100,000.00
	public String getFUNDAMT_F() 
	{
		String result = this.FUNDAMT;
		try
		{
			result = NumericUtil.fmtAmount(result, 2);
		}
		catch (Exception e)
		{
			log.error("getFUNDAMT_F error. FUNDAMT >> {}", FUNDAMT, e);
		}
		return result;
	}

	public void setFUNDAMT(String fUNDAMT) {
		FUNDAMT = fUNDAMT;
	}

	public String getPRICE1() {
		return PRICE1;
	}
	
	// 0000000000 to 0.0000
	public String getPRICE1_F() 
	{
		String result = this.PRICE1;
		try
		{
			BigDecimal b = new BigDecimal(new BigInteger(result), 4);
			result = NumericUtil.fmtAmount(b.toPlainString(), 4);
		}
		catch (Exception e)
		{
			log.error("getPRICE1_F error. PRICE1 >> {}", PRICE1, e);
		}
		return result;
	}

	public void setPRICE1(String pRICE1) {
		PRICE1 = pRICE1;
	}

	public String getPRICE2() {
		return PRICE2;
	}
	
	// 0000000000 to 0.0000
	public String getPRICE2_F() 
	{
		String result = this.PRICE2;
		try
		{
			BigDecimal b = new BigDecimal(new BigInteger(result), 4);
			result = NumericUtil.fmtAmount(b.toPlainString(), 4);
		}
		catch (Exception e)
		{
			log.error("getPRICE2_F error. PRICE2 >> {}", PRICE2, e);
		}
		return result;
	}

	public void setPRICE2(String pRICE2) {
		PRICE2 = pRICE2;
	}

	public String getEXRATE() {
		return EXRATE;
	}
	
	// 000309350 to 30.9350
	public String getEXRATE_F() 
	{
		String result = this.EXRATE;
		try
		{
			BigDecimal b = new BigDecimal(new BigInteger(result), 4);
			result = NumericUtil.fmtAmount(b.toPlainString(), 4);
		}
		catch (Exception e)
		{
			log.error("getEXRATE_F error. EXRATE >> {}", EXRATE, e);
		}
		return result;
	}

	public void setEXRATE(String eXRATE) {
		EXRATE = eXRATE;
	}

	public String getEXAMT() {
		return EXAMT;
	}
	
	// 0000001000000 to 10,000.00
	public String getEXAMT_F() 
	{
		String result = EXAMT;
		try
		{
			BigDecimal b = new BigDecimal(new BigInteger(result), 2);
			result = NumericUtil.fmtAmount(b.toPlainString(), 2);
		}
		catch (Exception e)
		{
			log.error("getEXAMT_F error. EXAMT >> {}", EXAMT, e);
		}
		return result;
	}

	public void setEXAMT(String eXAMT) {
		EXAMT = eXAMT;
	}

	public String getUNIT() {
		return UNIT;
	}
	
	// 000020000000 to 2,000.0000
	public String getUNIT_F() 
	{
		String result = this.UNIT;
		try
		{
			BigDecimal b = new BigDecimal(new BigInteger(result), 4);
			result = NumericUtil.fmtAmount(b.toPlainString(), 4);
		}
		catch (Exception e)
		{
			log.error("getUNIT_F error. UNIT >> {}", UNIT, e);
		}
		return result;
	}

	public void setUNIT(String uNIT) {
		UNIT = uNIT;
	}

	public String getOKCOUNT() {
		return OKCOUNT;
	}
	
	// 0000 to 0
	public String getOKCOUNT_F() 
	{
		String result = OKCOUNT;
		try
		{
			BigDecimal b = new BigDecimal(OKCOUNT);
			result = b.toPlainString();
		}
		catch (Exception e)
		{
			log.error("getOKCOUNT_F error. OKCOUNT >> {}", OKCOUNT, e);
		}
		return result;
	}

	public void setOKCOUNT(String oKCOUNT) {
		OKCOUNT = oKCOUNT;
	}

	public String getTXUNIT() {
		return TXUNIT;
	}

	// 000000000000 to 0.0000
	public String getTXUNIT_F() 
	{
		String result = this.TXUNIT;
		try
		{
			BigDecimal b = new BigDecimal(new BigInteger(result), 4);
			result = NumericUtil.fmtAmount(b.toPlainString(), 4);
		}
		catch (Exception e)
		{
			log.error("getTXUNIT_F error. TXUNIT >> {}", TXUNIT, e);
		}
		return result;
	}
	
	public void setTXUNIT(String tXUNIT) {
		TXUNIT = tXUNIT;
	}

	public String getFCA1() {
		return FCA1;
	}

	public void setFCA1(String fCA1) {
		FCA1 = fCA1;
	}

	public String getAMT1() {
		return AMT1;
	}
	
	// 0000000000000 to 0.00
	public String getAMT1_F() 
	{
		String result = AMT1;
		BigDecimal b = new BigDecimal(new BigInteger(result), 2);
		result = b.toPlainString();
		return result;
	}

	public void setAMT1(String aMT1) {
		AMT1 = aMT1;
	}

	public String getAMT2() {
		return AMT2;
	}

	public void setAMT2(String aMT2) {
		AMT2 = aMT2;
	}

	public String getAMT3() {
		return AMT3;
	}
	
	// 000000500 to 500
	public String getAMT3_F() 
	{
		String result = AMT3;
		BigDecimal b = new BigDecimal(AMT3);
		result = b.toPlainString();
		return result;
	}
	
	public void setAMT3(String aMT3) {
		AMT3 = aMT3;
	}

	public String getFCA2() {
		return FCA2;
	}
	
	// 00000210000 to 2,100.00
	public String getFCA2_F() 
	{
		String result = this.FCA2;
		try
		{
			BigDecimal b = new BigDecimal(new BigInteger(result), 2);
			result = NumericUtil.fmtAmount(b.toPlainString(), 2);
		}
		catch (Exception e)
		{
			log.error("getFCA2_F error. FCA2 >> {}", FCA2, e);
		}
		return result;
	}

	public void setFCA2(String fCA2) {
		FCA2 = fCA2;
	}

	public String getFXFEE() {
		return FXFEE;
	}

	public void setFXFEE(String fXFEE) {
		FXFEE = fXFEE;
	}

	public String getFUNDACN() {
		return FUNDACN;
	}

	public void setFUNDACN(String fUNDACN) {
		FUNDACN = fUNDACN;
	}

	public String getAMT4() {
		return AMT4;
	}

	// 
	public String getAMT4_F() 
	{
		String result = this.AMT4;
		try
		{
			BigDecimal b = new BigDecimal(new BigInteger(result), 6);
			result = NumericUtil.fmtAmount(b.toPlainString(), 6);
		}
		catch (Exception e)
		{
			log.error("getAMT4_F error. AMT4 >> {}", AMT4, e);
		}
		return result;
	}
	
	public void setAMT4(String aMT4) {
		AMT4 = aMT4;
	}

	public String getAMT5() {
		return AMT5;
	}
	
	// 
	public String getAMT5_F() 
	{
		String result = this.AMT5;
		try
		{
			BigDecimal b = new BigDecimal(new BigInteger(result), 2);
			result = NumericUtil.fmtAmount(b.toPlainString(), 2);
		}
		catch (Exception e)
		{
			log.error("getAMT5_F error. AMT5 >> {}", AMT5, e);
		}
		return result;
	}

	public void setAMT5(String aMT5) {
		AMT5 = aMT5;
	}

	public String getAMT6() {
		return AMT6;
	}

	public String getAMT6_F() 
	{
		String result = AMT6;
		BigDecimal b = new BigDecimal(new BigInteger(result), 4);
		result = b.toPlainString();
		return result;
	}
	
	public void setAMT6(String aMT6) {
		AMT6 = aMT6;
	}

	public String getINTRANSCRY() {
		return INTRANSCRY;
	}

	public void setINTRANSCRY(String iNTRANSCRY) {
		INTRANSCRY = iNTRANSCRY;
	}

	public String getAMT7() {
		return AMT7;
	}
	
	// 0000000000000 to 0.00
	public String getAMT7_F() 
	{
		String result = AMT7;
		BigDecimal b = new BigDecimal(new BigInteger(result), 2);
		result = b.toPlainString();
		return result;
	}

	public void setAMT7(String aMT7) {
		AMT7 = aMT7;
	}

	public String getTRUSTCURRENCY() {
		return TRUSTCURRENCY;
	}

	public void setTRUSTCURRENCY(String tRUSTCURRENCY) {
		TRUSTCURRENCY = tRUSTCURRENCY;
	}

	public String getSHORTTRADEFEE() {
		return SHORTTRADEFEE;
	}
	
	// 00000000000 to 0.00
	public String getSHORTTRADEFEE_F() 
	{
		String result = "".equals(SHORTTRADEFEE) ? "0" : SHORTTRADEFEE;
		try
		{
			BigDecimal b = new BigDecimal(new BigInteger(result), 2);
			result = NumericUtil.fmtAmount(b.toPlainString(), 2);
		}
		catch (Exception e)
		{
			log.error("getSHORTTRADEFEE_F error. SHORTTRADEFEE >> {}", SHORTTRADEFEE, e);
		}
		return result;
	}
	
	public void setSHORTTRADEFEE(String sHORTTRADEFEE) {
		SHORTTRADEFEE = sHORTTRADEFEE;
	}

	public String getNHITAX() {
		return NHITAX;
	}
	
	// 
	public String getNHITAX_F() 
	{
		String result = this.NHITAX;
		try
		{
			BigDecimal b = new BigDecimal(new BigInteger(result), 0);
			result = NumericUtil.fmtAmount(b.toPlainString(), 0);
		}
		catch (Exception e)
		{
			log.error("getNHITAX_F error. NHITAX >> {}", NHITAX, e);
		}
		return result;
	}

	public void setNHITAX(String nHITAX) {
		NHITAX = nHITAX;
	}

	public String getAUTOR() {
		return AUTOR;
	}

	// TODO I18N
	public String getAUTOR_F() 
	{
		String result = AUTOR;
		I18n i18n = SpringBeanFactory.getBean("i18n");
//		result = "Y".equals(AUTOR) ? "啟用" : "不啟用"; 
		result = "Y".equals(AUTOR) ? i18n.getMsg("LB.D0324") : i18n.getMsg("LB.D0995"); 
		return result;
	}
	
	public void setAUTOR(String aUTOR) {
		AUTOR = aUTOR;
	}

	public String getMAC() {
		return MAC;
	}

	public void setMAC(String mAC) {
		MAC = mAC;
	}
	
	/**
	 * 用 TRANSCODE 查詢基金名稱
	 * @return
	 */
	public String getTRANSCODE_FUNDLNAME() 
	{
		String result = getFundLName(this.TRANSCODE);
		setTRANSCODE_FUNDLNAME(result);
		return TRANSCODE_FUNDLNAME;
	}

	public void setTRANSCODE_FUNDLNAME(String tRANSCODE_FUNDLNAME) 
	{
		TRANSCODE_FUNDLNAME = tRANSCODE_FUNDLNAME;
	}
	
	/**
	 * 用 INTRANSCODE 查詢基金名稱
	 * @return
	 */
	public String getINTRANSCODE_FUNDLNAME() 
	{
		String result = getFundLName(this.INTRANSCODE);
		setINTRANSCODE_FUNDLNAME(result);
		return INTRANSCODE_FUNDLNAME;
	}

	public void setINTRANSCODE_FUNDLNAME(String iNTRANSCODE_FUNDLNAME) {
		INTRANSCODE_FUNDLNAME = iNTRANSCODE_FUNDLNAME;
	}
	
	/**
	 * 用 CRY 查詢中文幣別
	 * @return
	 */
	public String getCRY_CRYNAME() 
	{
		String result = getCryName(this.CRY);
		setCRY_CRYNAME(result);
		return CRY_CRYNAME;
	}

	public void setCRY_CRYNAME(String cRY_CRYNAME) {
		CRY_CRYNAME = cRY_CRYNAME;
	}


	/**
	 * 用 TRANSCRY 查詢中文幣別
	 * @return
	 */
	public String getTRANSCRY_CRYNAME() 
	{
		String result = getCryName(this.TRANSCRY);
		setTRANSCRY_CRYNAME(result);
		return TRANSCRY_CRYNAME;
	}


	public void setTRANSCRY_CRYNAME(String tRANSCRY_CRYNAME) {
		TRANSCRY_CRYNAME = tRANSCRY_CRYNAME;
	}


	/**
	 * 用 INTRANSCRY	查詢中文幣別
	 * @return
	 */
	public String getINTRANSCRY_CRYNAME() 
	{
		String result = getCryName(this.INTRANSCRY);
		setINTRANSCRY_CRYNAME(result);
		return INTRANSCRY_CRYNAME;
	}


	public void setINTRANSCRY_CRYNAME(String iNTRANSCRY_CRYNAME) {
		INTRANSCRY_CRYNAME = iNTRANSCRY_CRYNAME;
	}


	public String getNEWCRY_CRYNAME() {
		return NEWCRY_CRYNAME;
	}


	public void setNEWCRY_CRYNAME(String nEWCRY_CRYNAME) {
		NEWCRY_CRYNAME = nEWCRY_CRYNAME;
	}
	
	
	/**
	 * 贖回收益邏輯
	 * @return
	 */
	public String getRedemptionIncome() 
	{
		if(StrUtil.isNotEmpty(redemptionIncome))
		{
			return redemptionIncome;
		}
		
		String amt = "0";
		int digit = 0;
		try
		{
			if (EXAMT.trim().equals("")) 
			{
				amt = AMT3;
				digit = 0;
			}
			else 
			{
				amt = EXAMT;				
				digit = 2;
			}	
			BigDecimal b = new BigDecimal(new BigInteger(amt), digit);
			amt = NumericUtil.fmtAmount(b.toPlainString(), digit);
			
			setRedemptionIncome(amt);
		}
		catch (Exception e)
		{
			log.error("EXAMT >> {}, AMT3 >> {}", EXAMT, AMT3);
			log.error("getRedemptionIncome error", e);
		}
		return amt;
	}

	public void setRedemptionIncome(String redemptionIncome) {
		this.redemptionIncome = redemptionIncome;
	}

	/**
	 * 除權後信託單位數
	 * @return
	 */
	public String getExRightUnit() 
	{
		int digit = 4;
		String result = "";
		try
		{
			BigInteger u = new BigInteger(UNIT);
			BigInteger txu = new BigInteger(TXUNIT);
			BigInteger total = u.add(txu);
			BigDecimal b = new BigDecimal(total, digit);
			result = NumericUtil.fmtAmount(b.toPlainString(), digit);
			setExRightUnit(result);
		}
		catch (Exception e)
		{
			log.error("UNIT >> {}, TXUNIT >> {}", UNIT, TXUNIT);
			log.error("getExRightUnit error", e);
		}
		return exRightUnit;
	}

	public void setExRightUnit(String exRightUnit) {
		this.exRightUnit = exRightUnit;
	}


	public static void main(String[] args) {
		C014_REST_RS rs = new C014_REST_RS();
		rs.setTRADEDATE("01080502");
		rs.setCDNO("01019044834");
		rs.setFUNDAMT("100000.00");
		rs.setPRICE1("0000000000");
		rs.setEXRATE("000309350");
		rs.setUNIT("000020000000");
		rs.setFCA2("00000210000");
		rs.setOKCOUNT("0000");
		rs.setFUNDFLAG("Y");
		rs.setDATE("00000000");
//		System.out.println(rs.getTRADEDATE_F());
//		System.out.println(rs.getCREDITNO_F());
//		System.out.println(rs.getFUNDAMT_F());
//		System.out.println(rs.getPRICE1_F());
//		System.out.println(rs.getEXRATE_F());
//		System.out.println(rs.getUNIT_F());
//		System.out.println(rs.getFCA2_F());
//		System.out.println(rs.getOKCOUNT_F());
//		System.out.println(rs.getFUNDFLAG_F());
//		System.out.println(rs.getDATE_F());
//		System.out.println(rs);
	}
}