package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N945_REST_RQ extends BaseRestBean_PS implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8803700557011104661L;
	
	private String CUSIDN;
	private String PINOLD;
	private String PINNEW;
	private String ADOPID;
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getPINOLD() {
		return PINOLD;
	}

	public void setPINOLD(String pINOLD) {
		PINOLD = pINOLD;
	}

	public String getPINNEW() {
		return PINNEW;
	}

	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	
	
	
}
