package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * 信用卡查詢RSDATA
 */
public class N812_REST_RSDATA extends BaseRestBean implements Serializable{
	private static final long serialVersionUID = 3588725544057128396L;
	
	private String CARDNUM;
	private String EXPDTA;
	private String PT_FLG;
	private String MEMO;
	private String TYPENAME;
	
	public String getCARDNUM(){
		return CARDNUM;
	}
	public void setCARDNUM(String cARDNUM){
		CARDNUM = cARDNUM;
	}
	public String getEXPDTA(){
		return EXPDTA;
	}
	public void setEXPDTA(String eXPDTA){
		EXPDTA = eXPDTA;
	}
	public String getPT_FLG(){
		return PT_FLG;
	}
	public void setPT_FLG(String pT_FLG){
		PT_FLG = pT_FLG;
	}
	public String getMEMO(){
		return MEMO;
	}
	public void setMEMO(String mEMO){
		MEMO = mEMO;
	}
	public String getTYPENAME(){
		return TYPENAME;
	}
	public void setTYPENAME(String tYPENAME){
		TYPENAME = tYPENAME;
	}
}