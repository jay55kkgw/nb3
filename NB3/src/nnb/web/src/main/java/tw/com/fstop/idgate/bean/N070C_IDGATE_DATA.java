package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

public class N070C_IDGATE_DATA implements Serializable{


	@SerializedName(value = "DPACNO")
	private String DPACNO;
	@SerializedName(value = "AMOUNT")
	private String AMOUNT;
	@SerializedName(value = "DPBHNO")
	private String DPBHNO;

	public String getDPACNO() {
		return DPACNO;
	}
	public void setDPACNO(String dPACNO) {
		DPACNO = dPACNO;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	public String getDPBHNO() {
		return DPBHNO;
	}
	public void setDPBHNO(String dPBHNO) {
		DPBHNO = dPBHNO;
	}

	
}
