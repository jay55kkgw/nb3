package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * 
 * 功能說明 :停損停利設定作業
 *
 */
public class C117_REST_RQ extends BaseRestBean_FUND implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6618798027466799466L;


	private String CUSIDN;// 統一編號

	private String CDNO;// 信託號碼

	private String PAYTAG;// 扣款標的

	private String FUNDAMT;// 信託金額

	private String FUNDCUR;// 贖回信託金額幣別

	private String PAYAMT;// 扣款金額

	private String PAYCUR;// 扣款幣別

	private String STOPLOSS;// 停損

	private String STOPPROF;// 停利

	private String NSTOPLOSS;// 新停損設定

	private String NSTOPPROF;// 新停利設定

	private String AC202;// 信託業務別
	
	private String FGTXWAY;
	
	private String PINNEW;


	public String getCUSIDN()
	{
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN)
	{
		CUSIDN = cUSIDN;
	}

	public String getPAYTAG()
	{
		return PAYTAG;
	}

	public void setPAYTAG(String pAYTAG)
	{
		PAYTAG = pAYTAG;
	}

	public String getFUNDAMT()
	{
		return FUNDAMT;
	}

	public void setFUNDAMT(String fUNDAMT)
	{
		FUNDAMT = fUNDAMT;
	}

	public String getFUNDCUR()
	{
		return FUNDCUR;
	}

	public void setFUNDCUR(String fUNDCUR)
	{
		FUNDCUR = fUNDCUR;
	}

	public String getPAYAMT()
	{
		return PAYAMT;
	}

	public void setPAYAMT(String pAYAMT)
	{
		PAYAMT = pAYAMT;
	}

	public String getPAYCUR()
	{
		return PAYCUR;
	}

	public void setPAYCUR(String pAYCUR)
	{
		PAYCUR = pAYCUR;
	}

	public String getSTOPLOSS()
	{
		return STOPLOSS;
	}

	public void setSTOPLOSS(String sTOPLOSS)
	{
		STOPLOSS = sTOPLOSS;
	}

	public String getSTOPPROF()
	{
		return STOPPROF;
	}

	public void setSTOPPROF(String sTOPPROF)
	{
		STOPPROF = sTOPPROF;
	}

	public String getNSTOPLOSS()
	{
		return NSTOPLOSS;
	}

	public void setNSTOPLOSS(String nSTOPLOSS)
	{
		NSTOPLOSS = nSTOPLOSS;
	}

	public String getNSTOPPROF()
	{
		return NSTOPPROF;
	}

	public void setNSTOPPROF(String nSTOPPROF)
	{
		NSTOPPROF = nSTOPPROF;
	}

	public String getAC202()
	{
		return AC202;
	}

	public void setAC202(String aC202)
	{
		AC202 = aC202;
	}

	public String getCDNO() {
		return CDNO;
	}

	public void setCDNO(String cDNO) {
		CDNO = cDNO;
	}

	public String getFGTXWAY() {
		return FGTXWAY;
	}

	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}

	public String getPINNEW() {
		return PINNEW;
	}

	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	
	
	
}
