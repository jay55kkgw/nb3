package tw.com.fstop.nnb.service;

import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.formosoft.sso.tbb.SSOGen;

import tw.com.fstop.util.SpringBeanFactory;


@Service
public class Portal_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * 確認使用者portal連線狀態
	 * @param cusidn 統編/身分證
	 * @param account 使用者名稱
	 * @param sysid 系統別
	 * 
	 * @return
	 */
	public boolean getPortalStatus(String cusidn, String userName, String sysid) {
		boolean result = false;
		
//		String str_PORTALURL = "http://10.16.22.71/TBBPortalServlet/portal"; // 開發
//		String str_PORTALURL = "http://10.16.22.69/TBBPortalServlet/portal"; // 測試
		
//		String str_APIUSERNAME = "TBBNB3";
//		String str_APIUSERPD = "nb147258";
//		String str_APNAME = "TBBNB3";
		
		String str_PORTALURL = SpringBeanFactory.getBean("PORTAL_URL");
		String str_APIUSERNAME = SpringBeanFactory.getBean("PORTAL_APIUSERNAME");
		String str_APIUSERPD = SpringBeanFactory.getBean("PORTAL_APIUSERPD");
		String str_APNAME = SpringBeanFactory.getBean("PORTAL_APNAME");
		
		try {
			SSOGen ssogen = new SSOGen(str_PORTALURL);
			
			// 密碼解密
			String str_APIUSERPD_show = new String(Base64.getDecoder().decode(str_APIUSERPD), "utf-8");
			int iRtn = ssogen.init(str_APIUSERNAME, str_APIUSERPD_show, str_APNAME);
			log.debug("iRtn: " + iRtn);
			
			if (iRtn == 0) {
				result = ssogen.checkSession(cusidn, userName, sysid);
				log.debug("getPortalStatus.result: " + result);
			}
			
		} catch (Exception e) {
			log.error("", e);
		}
		return result;
	}
	
	/**
	 * 確認使用者portal連線狀態
	 * @param cusidn 統編/身分證
	 * @param account 使用者名稱
	 * @param sysid 系統別
	 * 
	 * @return
	 */
	public void kickPortal(String cusidn, String account, String sysid) {
		
//		String str_PORTALURL = "http://10.16.22.71/TBBPortalServlet/portal"; // 開發
//		String str_PORTALURL = "http://10.16.22.69/TBBPortalServlet/portal"; // 測試
		
//		String str_APIUSERNAME = "TBBNB3";
//		String str_APIUSERPD = "nb147258";
//		String str_APNAME = "TBBNB3";
		
		String str_PORTALURL = SpringBeanFactory.getBean("PORTAL_URL");
		String str_APIUSERNAME = SpringBeanFactory.getBean("PORTAL_APIUSERNAME");
		String str_APIUSERPD = SpringBeanFactory.getBean("PORTAL_APIUSERPD");
		String str_APNAME = SpringBeanFactory.getBean("PORTAL_APNAME");
		
		try {
			SSOGen ssogen = new SSOGen(str_PORTALURL);
			
			// 密碼解密
			String str_APIUSERPD_show = new String(Base64.getDecoder().decode(str_APIUSERPD), "utf-8");
			int iRtn = ssogen.init(str_APIUSERNAME, str_APIUSERPD_show, str_APNAME);
			log.debug("iRtn: " + iRtn);
			
			if (iRtn == 0) {
				boolean result = ssogen.checkSession(cusidn, account, sysid);
				log.debug("getPortalStatus.result: " + result);
				
				// 檢查有登入再進行是否清除session動作
			    if(result) {
					ssogen.kickSession(cusidn, account, sysid);   //執行時沒有回傳值，一徑視為清除成功，需詢問客戶是否清除，如要清除，在執行此method
					log.debug("kickPortal finish...");
				}	
			}
			
		} catch (Exception e) {
			log.error("", e);
		}
	}
	
}
