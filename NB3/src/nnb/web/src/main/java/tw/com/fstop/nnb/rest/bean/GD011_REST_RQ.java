package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class GD011_REST_RQ extends BaseRestBean_GOLD implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5631644612291403287L;

	String QUERYTYPE;
	String CMSDATE;
	String CMEDATE;
	String ADOPID;
	
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getQUERYTYPE() {
		return QUERYTYPE;
	}
	public void setQUERYTYPE(String qUERYTYPE) {
		QUERYTYPE = qUERYTYPE;
	}
	public String getCMSDATE() {
		return CMSDATE;
	}
	public void setCMSDATE(String cMSDATE) {
		CMSDATE = cMSDATE;
	}
	public String getCMEDATE() {
		return CMEDATE;
	}
	public void setCMEDATE(String cMEDATE) {
		CMEDATE = cMEDATE;
	}
}
