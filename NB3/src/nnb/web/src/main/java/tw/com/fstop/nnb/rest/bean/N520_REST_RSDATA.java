package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * 外匯活期存款交易明細查詢
 * 
 * @author Ian
 *
 */
public class N520_REST_RSDATA extends BaseRestBean implements Serializable {

	private static final long serialVersionUID = -7015628686361485399L;

	private String CORD;// 借貸別

	private String ORDCUST;// 查詢匯款人名稱

	private String FXRECAMT;//收入金額
	
	private String FXPAYAMT;

	private String AMT;// 交易金額

	private String FEXRATE;// 查詢匯率

	private String TEXT;// 備註

	private String ACN;// 帳號

	private String BALANCE;// 帳戶餘額

	private String DATA12;// 資料內容

	private String CUID;// 幣別名稱

	private String TRNTIME;// 交易時間

	private String TXDATE;// 異動日

	private String MEMO;// 摘要

	public String getCORD() {
		return CORD;
	}

	public void setCORD(String cORD) {
		CORD = cORD;
	}

	public String getORDCUST() {
		return ORDCUST;
	}

	public void setORDCUST(String oRDCUST) {
		ORDCUST = oRDCUST;
	}

	public String getFXRECAMT() {
		return FXRECAMT;
	}

	public void setFXRECAMT(String fXRECAMT) {
		FXRECAMT = fXRECAMT;
	}

	public String getFXPAYAMT() {
		return FXPAYAMT;
	}

	public void setFXPAYAMT(String fXPAYAMT) {
		FXPAYAMT = fXPAYAMT;
	}

	public String getAMT() {
		return AMT;
	}

	public void setAMT(String aMT) {
		AMT = aMT;
	}

	public String getFEXRATE() {
		return FEXRATE;
	}

	public void setFEXRATE(String fEXRATE) {
		FEXRATE = fEXRATE;
	}

	public String getTEXT() {
		return TEXT;
	}

	public void setTEXT(String tEXT) {
		TEXT = tEXT;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	public String getBALANCE() {
		return BALANCE;
	}

	public void setBALANCE(String bALANCE) {
		BALANCE = bALANCE;
	}

	public String getDATA12() {
		return DATA12;
	}

	public void setDATA12(String dATA12) {
		DATA12 = dATA12;
	}

	public String getCUID() {
		return CUID;
	}

	public void setCUID(String cUID) {
		CUID = cUID;
	}

	public String getTRNTIME() {
		return TRNTIME;
	}

	public void setTRNTIME(String tRNTIME) {
		TRNTIME = tRNTIME;
	}

	public String getTXDATE() {
		return TXDATE;
	}

	public void setTXDATE(String tXDATE) {
		TXDATE = tXDATE;
	}

	public String getMEMO() {
		return MEMO;
	}

	public void setMEMO(String mEMO) {
		MEMO = mEMO;
	}


}
