package tw.com.fstop.nnb.spring.controller;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import fstop.orm.po.ADMADS;
import fstop.orm.po.ADMANN;
import fstop.orm.po.ADMLOGIN;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.First_Login_Service;
import tw.com.fstop.nnb.service.Login_out_Service;
import tw.com.fstop.nnb.service.Mobile_Service;
import tw.com.fstop.nnb.service.Personal_Serving_Service;
import tw.com.fstop.nnb.service.Portal_Service;
import tw.com.fstop.nnb.service.Reset_Service;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

@Controller
@SessionAttributes({SessionUtil.TOKENID , SessionUtil.CUSIDN, SessionUtil.PD ,
					SessionUtil.LOGINTIME, SessionUtil.LOGINDT, SessionUtil.LOGINTM, 
					SessionUtil.DPMYEMAIL, SessionUtil.DPUSERNAME, SessionUtil.AUTHORITY,
					SessionUtil.XMLCOD, SessionUtil.ISIKEYUSER, SessionUtil.USERIP, 
					SessionUtil.MBSTAT, SessionUtil.MBOPNDT, SessionUtil.MBOPNTM, 
					SessionUtil.STAFF, SessionUtil.HKCODE, SessionUtil.USERID, SessionUtil.MENU,
					SessionUtil.FIRSTLOGIN, SessionUtil.KAPTCHA_SESSION_KEY, SessionUtil.REMAILID })
public class Login_out_Controller  {
	static Logger log = LoggerFactory.getLogger(Login_out_Controller.class);
	
	@Autowired
	private Login_out_Service login_out_service ;
	@Autowired
	private Reset_Service reset_service;
	@Autowired
	private Personal_Serving_Service personal_serving_service;
	@Autowired
	private First_Login_Service first_login_service;
	@Autowired
	private Portal_Service portal_service;
	@Autowired
	private Mobile_Service mobile_service;

	
	@Autowired
	I18n i18n;
	@Autowired
	ServletContext context; 
	
	@Value("${clear_pass_ip}")
	String clear_pass_ip;
	
	@Value("${logincheck}")
	String logincheck;
	
	
	/**
	 * 禁止進入
	 */
	@RequestMapping(value = "/errorCloss")
	public String errorClose(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("Login_out_Controller.errorCloss...");
		return "/login/errorCloss";
	}
	
	/**
	 * 資料驗證沒過
	 */
	@RequestMapping(value = "/noPass")
	public String noPass(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("Login_out_Controller.noPass...");
		return "/error_noPass";
	}
	
	/**
	 * 資料庫downAds圖檔落地 when server on start
	 */
	@PostConstruct
	private void init(){
		try {
			// 資料庫廣告圖檔落地
			boolean downResult = login_out_service.downAds();
			log.debug("login.downAds.downResult: {}", downResult);
		} catch (Exception e) {
			log.error("", e);
		}
	}

	/**
	 * 資料庫廣告圖檔落地
	 */
	@RequestMapping(value = "/downAds", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult downAds(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			
			// 資料庫廣告圖檔落地
			boolean downResult = login_out_service.downAds();
			log.debug("login.downAds.downResult: {}", downResult);
			bs.setMsgCode(downResult ? "0" : "1");
			bs.setMessage(downResult ? "downAds success" : "downAds fail");;
			bs.setResult(downResult);
			
		} catch (Exception e) {
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * 只輸入contextPath進入登入頁
	 */
	@RequestMapping(value = "")
	public String loginPage0(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam Map<String, String> reqParam, Model model, SessionStatus status) {
		String target = "/login/login";
		log.debug("Login_out_Controller.loginPage0...");
		try {
			// 原本進入登入頁就直接清除Session資料；2020/10/22--改成如果有登入資料，就跳出提示訊息詢問使用者是否登入，若否則關閉視窗
//			logout_aj(request, response, reqParam, model, status);
			String loggedId = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			log.trace("enter login page loggedId: {}", loggedId);
			if (StrUtil.isNotEmpty(loggedId)) {
				log.debug("alert logout!!!");
				model.addAttribute("logged", "true");
			}
			
			// 資料庫廣告圖檔落地
//			boolean downResult = login_out_service.downAds();
//			log.debug("login.downAds.downResult: {}", downResult);
			
			// Banner落地圖檔廣告數量
			int bannerImgs = login_out_service.countAds("banner", 1);
			log.debug("login.loadAds.bannerImgs: {}", bannerImgs);
			bannerImgs = bannerImgs>5 ? 5 : bannerImgs;
			model.addAttribute("bannerImgs", bannerImgs);
			
			// News廣告資料
			String imgSrc = login_out_service.compareNewsTS(true);
			model.addAttribute("imgSrc", imgSrc);
			List<Map<String, String>> newsTitleList = login_out_service.loadNewsData(imgSrc);
			
			model.addAttribute("newsTitle", ESAPIUtil.validList(newsTitleList));
			
			// 進入登入頁初始環境變數
			loginInit(model);
			
		} catch (Exception e) {
			log.error("", e);
		}
		return target;
	}
	
	/**
	 * 進入登入頁
	 */
	@RequestMapping(value = "/login")
	public String login(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam Map<String, String> reqParam, Model model, SessionStatus status) {
		String target = "/login/login";
		log.trace("Login_out_Controller.login...");
		try {
			// 原本進入登入頁就直接清除Session資料；2020/10/22--改成如果有登入資料，就跳出提示訊息詢問使用者是否登入，若否則關閉視窗
//			logout_aj(request, response, reqParam, model, status);
			String loggedId = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			log.trace("enter login page loggedId: {}", loggedId);
			if (StrUtil.isNotEmpty(loggedId)) {
				log.debug("alert logout!!!");
				model.addAttribute("logged", "true");
			}
			
			// 資料庫廣告圖檔落地
//			boolean downResult = login_out_service.downAds();
//			log.debug("login.downAds.downResult: {}", downResult);
			
			// Banner落地圖檔廣告數量
			int bannerImgs = login_out_service.countAds("banner", 1);
			log.debug("login.loadAds.bannerImgs: {}", bannerImgs);
			bannerImgs = bannerImgs>5 ? 5 : bannerImgs;
			model.addAttribute("bannerImgs", bannerImgs);
			
			// Banner廣告落地更新時間
			String adsudt = login_out_service.getAdsUpdateDT("B");
			log.warn(ESAPIUtil.vaildLog("login.adsudt: " + adsudt));
			model.addAttribute("adsudt", adsudt);
			
			// News廣告資料
			String imgSrc = login_out_service.compareNewsTS(true);
			model.addAttribute("imgSrc", imgSrc);
			List<Map<String, String>> newsTitleList = login_out_service.loadNewsData(imgSrc);
			
			model.addAttribute("newsTitle", ESAPIUtil.validList(newsTitleList));
			
			// News廣告落地更新時間
			String adsnewsudt = login_out_service.getAdsUpdateDT("C");
			log.warn(ESAPIUtil.vaildLog("login.adsnewsudt: " + adsnewsudt));
			model.addAttribute("adsnewsudt", adsnewsudt);
			
			// 進入登入頁初始環境變數
			loginInit(model);
			
		} catch (Exception e) {
			log.error("", e);
		}
		return target;
	}
	
	/**
	 * 資料庫登入狀態不合法，重新登入並顯示被踢退訊息
	 */
	@RequestMapping(value = "/reLogin")
	public String reLogin(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam Map<String, String> reqParam, Model model, SessionStatus status) {
		String target = "/login/login";
		log.trace("Login_out_Controller.reLogin...");
		try {
			// 原本進入登入頁就直接清除Session資料；2020/10/22--改成如果有登入資料，就跳出提示訊息詢問使用者是否登入，若否則關閉視窗
//			logout_aj(request, response, reqParam, model, status);
			String loggedId = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			log.trace("enter login page loggedId: {}", loggedId);
			if (StrUtil.isNotEmpty(loggedId)) {
				log.debug("alert logout!!!");
				model.addAttribute("logged", "true");
			}
			
			// 資料庫廣告圖檔落地
//			boolean downResult = login_out_service.downAds();
//			log.debug("login.downAds.downResult: {}", downResult);
			
			// Banner落地圖檔廣告數量
			int bannerImgs = login_out_service.countAds("banner", 1);
			log.debug("login.loadAds.bannerImgs: {}", bannerImgs);
			bannerImgs = bannerImgs>5 ? 5 : bannerImgs;
			model.addAttribute("bannerImgs", bannerImgs);
			
			// News廣告資料
			String imgSrc = login_out_service.compareNewsTS(true);
			model.addAttribute("imgSrc", imgSrc);
			List<Map<String, String>> newsTitleList = login_out_service.loadNewsData(imgSrc);
			
			model.addAttribute("newsTitle", ESAPIUtil.validList(newsTitleList));

			// 進入登入頁初始環境變數
			loginInit(model);

			// 另外要加入剔退提示訊息
			model.addAttribute("errorMsg", "logout");
			
		} catch (Exception e) {
			log.error("", e);
		}
		return target;
	}
	
	/**
	 * 進登入頁需要的環境變數
	 */
	private void loginInit(Model model) {
		// Local開發不打電文，只存Session
		String isSessionID = SpringBeanFactory.getBean("isSessionID");
		if("N".equals(isSessionID) || "T".equals(isSessionID)) {
			// 本機開發登入
			model.addAttribute("isTest", "Y");
		}
		
		// 前端顯示主機名稱方便DEBUG
		String hostname = SpringBeanFactory.getBean("HOSTNAME")!=null ? SpringBeanFactory.getBean("HOSTNAME") : "HOSTNAME";
		log.info("login.hostname: {}", hostname);
		model.addAttribute("hostname", hostname);
		
	}
	
	
	/**
	 * 登入時間逾時
	 */
	@RequestMapping(value = "/timeout_logout")
	public String timeout_logout(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("Login_out_Controller.timeout_logout...");
		return "/login/timeout";
	}
	
	/**
	 * 網站導覽
	 */
	@RequestMapping(value = "/login/sitemap")
	public String toSiteMap() {
		log.trace("sitemap...");
		return "/login/sitemap";
	}
	
	/**
	 * 登入前網站導覽
	 */
	@RequestMapping(value = "/login/nolink_sitemap")
	public String toNoLinkSiteMap() {
		log.trace("nolink_sitemap...");
		return "/login/nolink_sitemap";
	}
	
	/**
	 * 網銀登入前檢查
	 */
	@PostMapping(value = "/checkLogin_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult checkLogin_aj(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			// 檢查登入狀態
			boolean checkLoginStatus = login_out_service.checkLogin(okMap, WebUtil.getIpAddr(request));
			log.debug("checkLogin_aj.checkLoginStatus: {}", checkLoginStatus);
			
			// 若已登入且IP相同或是未登入可執行登入
			if(checkLoginStatus) {
				bs.setResult(true);
				bs.setMsgCode("0");
				bs.setMessage("check Login OK!!!");
				
			} // 重複登入且IP不相同，需先後踢前
			else {
				bs.setResult(false);
				bs.setMsgCode("H003");
				bs.setMessage("check Login Fail!!!");
				
			}
			
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}

	/**
	 * 網銀登入
	 */
	@PostMapping(value = "/login_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult login_aj(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model, @RequestHeader Map<String, String> headers) {
		BaseResult bs_cap = null;
		BaseResult bs = null;
		try {
			login_info(request, headers);
			
			log.info("login_aj...");
			
			// 先驗證驗證碼
			bs_cap = validedCapCode(request, response, reqParam, model);
			if (!bs_cap.getResult()) {
				return bs_cap;
			}
			
			// avoid Session fixation
			request.getSession().invalidate(); // 立即清除request的session
			log.trace("login_aj.request.getSession.invalidate...");
			
			bs = new BaseResult();
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			// 20210810-TMRA會去空白後送電文，導致企業戶加上空白也會通過N911成功登入，但NB3.session會存進有空白的資料，故在處理前就先去空白
			if (okMap.containsKey("cusidn") && StringUtils.isNotBlank(okMap.get("cusidn"))) {
				okMap.put("cusidn", okMap.get("cusidn").trim().toUpperCase());
			}
			
			// 登入所需基本資訊
			String tokenId = UUID.randomUUID().toString(); // 使用者識別
			Date logintime = new Date(); // 本次登入日期時間

			// 解決Trust Boundary Violation
//			String userip = WebUtil.getIpAddr(request);
			String userip = ESAPIUtil.validInput(WebUtil.getIpAddr(request), "GeneralString", true) ; // 登入 IP-Address
			
			// Local開發不打電文，只存Session
			if("N".equals(SpringBeanFactory.getBean("isSessionID"))) {
				// 本機開發登入
				bs = isLocalLogin(okMap, tokenId, logintime, userip, model);
				// 直接回傳bs，不做網銀登入
				return bs;
			}
			
			// Local開發不打電文，只存Session
			String isSessionID = SpringBeanFactory.getBean("isSessionID");
			if("Y".equals(isSessionID)) {
				// NP10
				BaseResult bsNP10 = login_out_service.ipVerifyNP10(okMap.get("cusidn").toUpperCase(), userip);
				if( !bsNP10.getResult() ) {
					log.warn(ESAPIUtil.vaildLog("login_aj.bsNP10.fail.cusidn: " + okMap.get("cusidn").toUpperCase()));
					log.warn(ESAPIUtil.vaildLog("login_aj.bsNP10.fail.userip: " + userip));
					
					// 如果登入失敗紀錄ADMLOGOUT
					login_out_service.setLoginExCode(bsNP10, okMap.get("cusidn").toUpperCase(), userip, logintime, tokenId);
					return bsNP10;
				}
			}
			
			
			// 阻擋重複IP登入不同統編，開發環境不驗證
//			if("Y".equals(SpringBeanFactory.getBean("isSessionID"))) {
//				boolean isLoginIp = login_out_service.isLoginIp(okMap.get("cusidn").toUpperCase(), userip);
//				log.trace(ESAPIUtil.vaildLog("checkLogin.isLoginIp: " + isLoginIp));
//				if( isLoginIp ) {
//					// 驗證失敗剔退
//					bs.setResult(false);
//					bs.setMsgCode("FE0013");
//					bs.setMessage("同一IP不可同時登入不同使用者統編");
//					return bs;
//				}
//			}
			
			
			// 正式上線階段
			String isProd = SpringBeanFactory.getBean("isProd");
			log.info("login_aj.isProd: {}", isProd);
			
			// 第一階段: 只有白名單人員可登入、即時轉檔
			if("1".equals(isProd)) {
				// 正式上線第一階段驗證白名單
				if(!login_out_service.isProdAcl(okMap.get("cusidn").toUpperCase())) {
					// 驗證失敗剔退
					bs.setResult(false);
					bs.setMsgCode("FE0011");
					bs.setMessage("非正式上線第一階段白名單");
					return bs;
				}
				
				// 判斷使用者是否曾經登入過
				boolean everLoginResult = first_login_service.everLogin(okMap.get("cusidn").toUpperCase());
				log.debug("everLogin.everLoginResult: {}", everLoginResult);
				
				// 判斷使用者第一次登入轉檔結果是否皆為成功
				boolean firstCheckResult = first_login_service.firstCheck(okMap.get("cusidn").toUpperCase());
				log.debug("everLogin.firstCheckResult: {}", firstCheckResult);
				
				// 需做資料轉檔 ( 使用者第一次登入新個網 或 第一次登入轉檔結果有失敗 )
				if ( !everLoginResult || !firstCheckResult) {
					// 判斷舊網銀是否有正在進行的預約交易排程
					boolean nnbSchResult = first_login_service.nnbSchCheck(okMap.get("cusidn").toUpperCase());
					log.debug("everLogin.nnbSchResult: {}", nnbSchResult);
					if(!nnbSchResult) {
						// 驗證失敗剔退
						bs.setResult(false);
						bs.setMsgCode("FE0012");
						bs.setMessage("正式上線第一階段轉檔前驗證失敗，舊網銀排程正在執行");
						return bs;
					}
				}
				
			} // 第二階段: 只有白名單人員可登入、批次轉檔
			else if("2".equals(isProd)) {
				// 正式上線第一階段驗證白名單
				if(!login_out_service.isProdAcl(okMap.get("cusidn").toUpperCase())) {
					// 驗證失敗剔退
					bs.setResult(false);
					bs.setMsgCode("FE0011");
					bs.setMessage("非正式上線第二階段白名單");
					return bs;
				}
			
			} // 第三階段: 都可登入、即時轉檔
			else if("3".equals(isProd)) {
				// 判斷使用者是否曾經登入過
				boolean everLoginResult = first_login_service.everLogin(okMap.get("cusidn").toUpperCase());
				log.debug("everLogin.everLoginResult: {}", everLoginResult);
				
				// 判斷使用者第一次登入轉檔結果是否皆為成功
				boolean firstCheckResult = first_login_service.firstCheck(okMap.get("cusidn").toUpperCase());
				log.debug("everLogin.firstCheckResult: {}", firstCheckResult);
				
				// 需做資料轉檔 ( 使用者第一次登入新個網 或 第一次登入轉檔結果有失敗 )
				if ( !everLoginResult || !firstCheckResult) {
					// 判斷是否不是即時轉檔合法時間
					boolean nb3SchResult = first_login_service.nb3SchCheck(okMap.get("cusidn").toUpperCase());
					log.debug("everLogin.nb3SchResult: {}", nb3SchResult);
					if(!nb3SchResult) {
						log.info("everLogin.nb3SchResult.fail");
						// 驗證失敗剔退
						bs.setResult(false);
						bs.setMsgCode("SFE0012");
						bs.setMessage("目前本行一般網路銀行正進行預約批次轉帳(預計時間:07:00~10:00)，為確保您的交易權益，請您先使用舊版一般網路銀行，造成您的不便，敬請見諒。");
						return bs;
					}
					
					// 判斷舊網銀是否有正在進行的預約交易排程
					boolean nnbSchResult = first_login_service.nnbSchCheck(okMap.get("cusidn").toUpperCase());
					log.debug("everLogin.nnbSchResult: {}", nnbSchResult);
					if(!nnbSchResult) {
						log.info("everLogin.nnbSchResult.fail");
						// 驗證失敗剔退
						bs.setResult(false);
						bs.setMsgCode("FE0012");
//						bs.setMessage("目前本行一般網路銀行正進行預約批次轉帳(預計時間:07:00~10:00)，為確保您的交易權益，請您先使用舊版一般網路銀行，造成您的不便，敬請見諒。");
						bs.setMessage("目前本行一般網路銀行正進行預約批次轉帳，為確保您的交易權益，請您先使用舊版一般網路銀行，造成您的不便，敬請見諒。");
						return bs;
					}
				}
			
			} // 第四階段: 都可登入、批次轉檔
			else if("4".equals(isProd)) {
				// 不檢核不轉檔
			}
			
			// 正式環境登入-N911 網路通行碼檢核
			bs = isProdLogin(okMap, tokenId, logintime, userip, model, request);
			log.debug("login_aj.isProdLogin.bs: {}", BeanUtils.describe(bs) );
			
			// 寄信通知登入結果
			boolean loginResut = "0".equals(bs.getMsgCode());
			login_out_service.sendMail(okMap.get("cusidn").toUpperCase(), loginResut);
			
			// 如果登入失敗紀錄ADMLOGOUT
			login_out_service.setLoginExCode(bs, okMap.get("cusidn").toUpperCase(), userip, logintime, tokenId);
			
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	
	/**
	 * 正式環境登入
	 * @param cusidn 身分證字號
	 * @param userid 使用者帳號
	 * @param pinnew 簽入密碼
	 * @return
	 */
	public BaseResult isProdLogin(Map<String, String> okMap, String tokenId, Date logintime, String userip, Model model, HttpServletRequest request) {
		log.debug("Prod login!!!");
		BaseResult bs = null;
		try {
			// 執行登入流程
			bs = login_out_service.login(okMap.get("cusidn").toUpperCase(), okMap.get("userName"), okMap.get("password"));
			log.trace("bs.getResult: {}", bs.getResult());
			
			// 網路通行碼檢核結果:bs.getData錯誤訊息沒有MSGCOD欄位故改抓TOPMSG，若ms沒回應FE0002則bs.getData()為null，改抓bs.getMsgCode
			String msgcod = "";
			Map<String,Object> dataMap = (Map<String,Object>) bs.getData();
			if(dataMap!=null) {
				msgcod = dataMap.get("TOPMSG")!=null
						? String.valueOf(dataMap.get("TOPMSG")) : String.valueOf(dataMap.get("msgCode"));
			} else {
				msgcod = bs.getMsgCode();
			}
			log.trace("isProdLogin.msgcod: {}", msgcod);
			
			// 登入成功 && 非登入失敗之訊息代號
			ArrayList<String> arrlist = SpringBeanFactory.getBean("msgcode");
			log.trace("isProdLogin.arrlist: {}", arrlist);
			log.trace("isProdLogin.result: {}", arrlist.contains(msgcod));
			
			// 成功登入
			List<String> loginList = Arrays.asList("","0","O000","0000");
			
			// 成功登入初始化登入資訊
			if( bs.getResult() && loginList.contains(msgcod)) {
				// 成功登入需發N912取得登入需要的資訊
				login_out_service.loginN912(okMap.get("cusidn").toUpperCase(), bs);
				
				// N912失敗
				if(!bs.getResult()) {
					return bs;
				}
				
				// 成功登入後驗證是否為行內IP非行員身分
				String staff = "";
				if( bs.getData() != null ) {
					staff = ((Map<String, Object>) bs.getData()).get("STAFF") != null
							? String.valueOf(((Map<String, Object>) bs.getData()).get("STAFF")) : "";
				}
				boolean ipVerifyResult = login_out_service.ipVerify(okMap.get("cusidn").toUpperCase(), userip, staff);
				log.debug("login_aj.ipVerifyResult: {}", ipVerifyResult);
				if( !ipVerifyResult ) {
					// 驗證失敗剔退
					bs.setResult(false);
					bs.setMsgCode("99204");
					bs.setMessage(i18n.getMsg("LB.X2151"));
					return bs;
				}
			}
			
			// 成功初始化將資訊存進session及DB
			if( bs.getResult() && loginList.contains(msgcod)) {
				// 生成UUID做登入後request檢核
				SessionUtil.addAttribute(model, SessionUtil.TOKENID, 
												tokenId );
				// 使用者統編加密後存取
				SessionUtil.addAttribute(model, SessionUtil.CUSIDN,
												Base64.getEncoder().encodeToString(okMap.get("cusidn").toUpperCase().getBytes()) );
				// 使用者名稱加密後存取
				SessionUtil.addAttribute(model, SessionUtil.USERID, 
												Base64.getEncoder().encodeToString(okMap.get("userName").getBytes()) );
				// 登入時間
				SessionUtil.addAttribute(model, SessionUtil.LOGINTIME, 
												logintime );
				// 上次登入日期
				String logindt = StringUtils.isNotBlank(((Map<String, String>) bs.getData()).get("LOGINDT"))
									? String.format("%08d", Long.parseLong(String.valueOf(((Map<String,String>) bs.getData()).get("LOGINDT")))) : "";
				SessionUtil.addAttribute(model, SessionUtil.LOGINDT, 
												DateUtil.convertDate(1, logindt, "yyyyMMdd", "yyyy-MM-dd"));
				// 上次登入時間
				String logintm = StringUtils.isNotBlank(((Map<String, String>) bs.getData()).get("LOGINTM"))
									? String.valueOf(((Map<String,String>) bs.getData()).get("LOGINTM")) : "";
				SessionUtil.addAttribute(model, SessionUtil.LOGINTM, 
												DateUtil.convertDate(0, logintm, "HHmmss", "HH:mm:ss"));
				// 資料庫-TXNUSER-我的Email
				SessionUtil.addAttribute(model, SessionUtil.DPMYEMAIL, 
												((Map<String,Object>) bs.getData()).get("DPMYEMAIL") );
				
				// 資料庫-TXNUSER-使用者姓名--移到首頁發Ajax更新並存進Session
//				String dpusername = ((Map<String,Object>) bs.getData()).get("DPUSERNAME")!=null
//										? ((Map<String,Object>) bs.getData()).get("DPUSERNAME").toString().replaceAll("　","") : "";
//				SessionUtil.addAttribute(model, SessionUtil.DPUSERNAME, dpusername);
				
				// N912業務權限
				String authority = ((Map<String,Object>) bs.getData()).get("AUTHORITY")!=null
										? ((Map<String,Object>) bs.getData()).get("AUTHORITY").toString() : "";
				SessionUtil.addAttribute(model, SessionUtil.AUTHORITY, authority );
				
				// 憑證代碼
				SessionUtil.addAttribute(model, SessionUtil.XMLCOD, 
												((Map<String,Object>) bs.getData()).get("XMLCOD") );
				// 是否是IKEY使用者
				boolean xmlcod = ((Map<String,Object>) bs.getData()).get("XMLCOD")!=null
						? login_out_service.isIKeyUser(((Map<String,Object>) bs.getData()).get("XMLCOD").toString()) : false;
				SessionUtil.addAttribute(model, SessionUtil.ISIKEYUSER, xmlcod);
				
				// Client端IP
				SessionUtil.addAttribute(model, SessionUtil.USERIP, 
												userip );
				// 行動銀行啟用狀態
				SessionUtil.addAttribute(model, SessionUtil.MBSTAT, 
												((Map<String,Object>) bs.getData()).get("MBSTAT") );
				// 行動銀行上次啟用/關閉日期
				SessionUtil.addAttribute(model, SessionUtil.MBOPNDT, 
												((Map<String,Object>) bs.getData()).get("MBOPNDT") );
				// 行動銀行上次啟用/關閉時間
				SessionUtil.addAttribute(model, SessionUtil.MBOPNTM, 
												((Map<String,Object>) bs.getData()).get("MBOPNTM"));
				// 是否為行員
				SessionUtil.addAttribute(model, SessionUtil.STAFF, 
												((Map<String,Object>) bs.getData()).get("STAFF") );
				// 全球金融網是否轉置
				SessionUtil.addAttribute(model, SessionUtil.HKCODE, 
												((Map<String,Object>) bs.getData()).get("HKCODE") );
				// 對帳單退件類別
				SessionUtil.addAttribute(model, SessionUtil.REMAILID, 
												((Map<String,Object>) bs.getData()).get("REMAILID") );
				
				
				// 正式上線階段
				String isProd = SpringBeanFactory.getBean("isProd");
				log.info("login_aj.isProd: {}", isProd);
				
				// 在記錄使用者登入資訊前，先查詢，判斷使用者是否為第一次登入
				boolean everLoginResult = first_login_service.everLogin(okMap.get("cusidn").toUpperCase());
				log.debug("everLogin.everLoginResult: {}", everLoginResult);
				
				// 第一階段: 只有白名單人員可登入、即時轉檔
				if("1".equals(isProd)) {
					
					// 判斷使用者第一次登入轉檔結果是否皆為成功
					boolean firstCheckResult = first_login_service.firstCheck(okMap.get("cusidn").toUpperCase());
					log.debug("everLogin.firstCheckResult: {}", firstCheckResult);
					
					// 使用者曾經登入新個網
					if(everLoginResult && firstCheckResult) {
						SessionUtil.addAttribute(model, SessionUtil.FIRSTLOGIN, false );
						
					} // 需做資料轉檔 ( 使用者第一次登入新個網 或 第一次登入轉檔結果有失敗 )
					else {
						SessionUtil.addAttribute(model, SessionUtil.FIRSTLOGIN, true );
						
						// 避免前一次登入轉檔失敗的可能，轉檔前先清除原本的資料
						first_login_service.firstClear(okMap.get("cusidn").toUpperCase());
						
						// 使用者設定檔 需要先轉檔，其他等到首頁再處理
						boolean result = first_login_service.txnuser_tx(okMap.get("cusidn").toUpperCase());
						log.debug("txnuser_tx.result: {}", result);
					}
					
				} // 第二階段: 只有白名單人員可登入、批次轉檔
				else if("2".equals(isProd)) {
					// 不轉檔
					SessionUtil.addAttribute(model, SessionUtil.FIRSTLOGIN, false );
				
				} // 第三階段: 都可登入、即時轉檔
				else if("3".equals(isProd)) {
					// 判斷使用者第一次登入轉檔結果是否皆為成功
					boolean firstCheckResult = first_login_service.firstCheck(okMap.get("cusidn").toUpperCase());
					log.debug("everLogin.firstCheckResult: {}", firstCheckResult);
					
					// 使用者曾經登入新個網
					if(everLoginResult && firstCheckResult) {
						SessionUtil.addAttribute(model, SessionUtil.FIRSTLOGIN, false );
						
					} // 需做資料轉檔 ( 使用者第一次登入新個網 或 第一次登入轉檔結果有失敗 )
					else {
						SessionUtil.addAttribute(model, SessionUtil.FIRSTLOGIN, true );
						
						// 避免前一次登入轉檔失敗的可能，轉檔前先清除原本的資料
						first_login_service.firstClear(okMap.get("cusidn").toUpperCase());
						
						// 使用者設定檔 需要先轉檔，其他等到首頁再處理
						boolean result = first_login_service.txnuser_tx(okMap.get("cusidn").toUpperCase());
						log.debug("txnuser_tx.result: {}", result);
					}
				
				} // 第四階段: 都可登入、批次轉檔
				else if("4".equals(isProd)) {
					// 不轉檔
					SessionUtil.addAttribute(model, SessionUtil.FIRSTLOGIN, false );
				}
				
				// 20200401--bugtracker no.11 新增需求: 2.5轉至3.0及新開戶使用者初次登入設定，通知服務預設皆通知
				if( !everLoginResult ) {
					log.debug(ESAPIUtil.vaildLog("txnuser_tx.firstLogin.cusidn: {}"+ okMap.get("cusidn").toUpperCase()));
//					log.debug("txnuser_tx.firstLogin.cusidn: {}", okMap.get("cusidn").toUpperCase());
					first_login_service.txnuser_first_setting(okMap.get("cusidn").toUpperCase());
				}
				
				// for duck test by Hugo, avoid to dead lock
				String isSessionID = SpringBeanFactory.getBean("isSessionID")!=null
						? SpringBeanFactory.getBean("isSessionID").toString() : "Y";
				if("Y".equals(isSessionID)) {
					// N911驗證通過--登入成功--登入資訊存資料庫
					boolean admloginresult = login_out_service.setAdmLogin(okMap.get("cusidn").toUpperCase(), tokenId, logintime, userip, (Map<String, Object>) bs.getData());
					if (!admloginresult) {
						bs.setMsgCode("FE0009");
						bs.setMessage("登入資訊處理異常");
						bs.setResult(false);
						return bs;
					}
					
					log.debug("login_aj.setAdmLogin.end");
				}
				
			}
			
			// 成功登入之外的特殊MSGCODE，在頁面alert訊息後做相對應的處理，其他則alert登入失敗之錯誤訊息
			if( arrlist.contains(msgcod) ) {
				log.trace("isProdLogin.arrlist.msgcod: {}", msgcod);
				
				String target = ""; // 決定下一頁導向
				String acradio = ""; // 變更簽入密碼或是交易密碼

				switch(msgcod) { 
		            case "": // 成功
		            	target = "/INDEX/index";
		                break;
		            case "0": // 成功
		            	target = "/INDEX/index";
		            	break;
		            case "O000": // 成功
		            	target = "/INDEX/index";
		            	break;
		            case "0000": // 成功
		            	target = "/INDEX/index";
		            	break;
		            case "E048": // 首次登入，請變更密碼
		            	target = "/doublepw";
		            	break;
		            case "E178": // 需變更簽入密碼
		            	acradio = "pw";
		            	target = "/pw_alter_in";
		            	break;
		            case "E310": // 密碼超過一年，且簽入密碼與交易密碼不得相同，請變更密碼
		            	target = "/doublepw";
		            	break;
		            case "E179": // 需變更交易密碼
		            	acradio = "ssl";
		            	target = "/pw_alter_in";
		            	break;
		            case "E225": // 簽入密碼已變更成功，需變更交易密碼
		            	acradio = "ssl";
		            	target = "/pw_alter_in";
		            	break;
		            case "E226": // 交易密碼已變更成功，需變更簽入密碼
		            	acradio = "pw";
		            	target = "/pw_alter_in";
		            	break;
		            case "E152": // 請變更使用者名稱
		            	target = "/username_reset";
		            	break;
		            case "E161": // 密碼使用已超過一年，請變更密碼
		            	target = "/expiredpw";
		            	break;
		            case "E238": // 同時強迫變更簽入及交易密碼
		            	target = "/doublepw";
		            	break;
		            default:
		            	log.trace("isProdLogin.default.msgcod: {}", msgcod);
				}
				
				// 使用者統編加密後存取
				SessionUtil.addAttribute(model, SessionUtil.CUSIDN,
												Base64.getEncoder().encodeToString(okMap.get("cusidn").toUpperCase().getBytes()) );
				// 使用者名稱加密後存取
				SessionUtil.addAttribute(model, SessionUtil.USERID, 
												Base64.getEncoder().encodeToString(okMap.get("userName").getBytes()) );
				// 對帳單退件類別
				SessionUtil.addAttribute(model, SessionUtil.REMAILID, 
												((Map<String,Object>) bs.getData()).get("REMAILID") );
				
				// N911特殊案例--下行資訊存資料庫
				boolean admloginresult = login_out_service.setAdmLogin(okMap.get("cusidn").toUpperCase(), tokenId, logintime, userip, (Map<String, Object>) bs.getData());
				if (!admloginresult) {
					bs.setMsgCode("FE0009");
					bs.setMessage("登入資訊處理異常");
					bs.setResult(false);
					return bs;
				}
				
				log.debug("login_aj.setAdmLogin.finish!");
				
				// 成功登入
				bs.setResult(true);
				bs.setMsgCode("0");
				bs.setMessage("正式環境登入");
				bs.setPrevious(acradio); // 變更簽入密碼或是交易密碼
				bs.setNext(target); // 決定下一頁導向
				
			} // 電文正常查詢有錯誤代碼，且錯誤代碼不為特殊案例
			else if (bs.getNext() != "error"){
				// 訊息代號
				bs.setMsgCode(msgcod);
				log.trace("isProdLogin.getMsgCode: {}", bs.getMsgCode());
				
				// 回傳訊息escapeHtml
				log.trace("isProdLogin.Message.before: {}", bs.getMessage());
				bs.setMessage(bs.getMessage().replaceAll("\\<.*?\\>", ""));
				log.trace("isProdLogin.Message.escape: {}", bs.getMessage());
				
			} // 程式異常
			else {
				log.error("isProdLogin.error: {}", bs.getMsgCode() + ":" + bs.getMessage());
			}
			
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	
	/**
	 * 本機開發登入，假資料存Session
	 * @param cusidn 身分證字號
	 * @param userid 使用者帳號
	 * @param pinnew 簽入密碼
	 * @return
	 */
	public BaseResult isLocalLogin(Map<String, String> okMap, String tokenId, Date logintime, String userip, Model model) {
		log.trace("isLocalLogin...");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			
			bs.setResult(true);
			bs.setMsgCode("local");
			bs.setMessage("本地端開發登入");
			
			// 生成UUID做登入後request檢核
			SessionUtil.addAttribute(model, SessionUtil.TOKENID, 
											"LOCAL");
			// 使用者統編加密後存取
			SessionUtil.addAttribute(model, SessionUtil.CUSIDN,
											Base64.getEncoder().encodeToString(okMap.get("cusidn").toUpperCase().getBytes()));
			// 使用者名稱加密後存取
			SessionUtil.addAttribute(model, SessionUtil.USERID, 
											Base64.getEncoder().encodeToString(okMap.get("userName").getBytes()));
			// 簽入密碼加密後存取
			SessionUtil.addAttribute(model, SessionUtil.PD, 
											Base64.getEncoder().encodeToString(okMap.get("password").getBytes()));
			// 登入時間
			SessionUtil.addAttribute(model, SessionUtil.LOGINTIME, 
											logintime);
			// 我的Email
			SessionUtil.addAttribute(model, SessionUtil.DPMYEMAIL,
											"LocalTestEmail@testMail.com");
			// 業務權限
			SessionUtil.addAttribute(model, SessionUtil.AUTHORITY,
											"ALL");
			// 憑證代碼
			SessionUtil.addAttribute(model, SessionUtil.XMLCOD, 
											"2");
			// 是否是IKEY使用者
			SessionUtil.addAttribute(model, SessionUtil.ISIKEYUSER, 
											true);
			// Client端IP
			SessionUtil.addAttribute(model, SessionUtil.USERIP, 
											userip );
			// 行動銀行啟用狀態
			SessionUtil.addAttribute(model, SessionUtil.MBSTAT, 
											"A" );
			// 行動銀行上次啟用/信用日期
			SessionUtil.addAttribute(model, SessionUtil.MBOPNDT, 
											"1080508");
			// 行動銀行上次啟用/信用時間
			SessionUtil.addAttribute(model, SessionUtil.MBOPNTM, 
											"155353");
			// IP
			SessionUtil.addAttribute(model, SessionUtil.USERIP, 
											"172.22.11.23");
			
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	
	
	/**
	 * 網銀登入refresh
	 */
	@PostMapping(value = "/login_refresh", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult login_refresh(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("login.refresh...");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs.setResult(true);
			bs.setMsgCode("0");
			bs.setMessage("refresh success");
			
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	
	/**
	 * API:強迫登出
	 */
	@PostMapping(value = "/logoutforce_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult logoutforce_aj(HttpServletRequest request, HttpServletResponse response, 
													@RequestParam Map<String, String> reqParam, Model model, SessionStatus status) {
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			log.trace(ESAPIUtil.vaildLog("logoutforce_aj.reqParam: {}"+CodeUtil.toJson(reqParam)));
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			String cusidn = okMap.get("cusidn").toUpperCase();
			log.trace(ESAPIUtil.vaildLog("logoutforce_aj.cusidn: {}"+ cusidn));
			
			login_out_service.setAdmForceLogout(cusidn);
			
			bs.setResult(true);
			bs.setMsgCode("0");
			bs.setMessage("logout force success");
			
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	
	/**
	 * 網銀登出
	 */
	@PostMapping(value = "/logout_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult logout_aj(HttpServletRequest request, HttpServletResponse response, 
													@RequestParam Map<String, String> reqParam, Model model, SessionStatus status) {
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			
			// 登出統編
			String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn), "utf-8");
				
			} else {
				// 若統編為null則不需要登出
				bs.setResult(true);
				bs.setMsgCode("0");
				bs.setMessage("dont need success");
				return bs;
			}
			
			log.debug("logout_aj.cusidn: {}", cusidn);
			String isSessionID = SpringBeanFactory.getBean("isSessionID")!=null
					? SpringBeanFactory.getBean("isSessionID").toString() : "Y";
			if("Y".equals(isSessionID)) {
				login_out_service.setAdmLogout(cusidn.toUpperCase());
			}
			// 登出清Session
//			SessionUtil.removeAll(model); // 無法清除SESSION，改用status.setComplete()，之後需要移至SessionUtil
			
			status.setComplete(); // SpringMVC清除session的API，會在request結束後清除
			log.trace("logout_aj.status.setComplete...");
			
			request.getSession().invalidate(); // 立即清除request的session
			log.trace("logout_aj.request.getSession.invalidate...");
			
			bs.setResult(true);
			bs.setMsgCode("0");
			bs.setMessage("logout success");
			
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	
	/**
	 * 變更簽入密碼、交易密碼
	 */
	@RequestMapping(value = "/doublepw")
	public String doublepw(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		
		log.trace("doublepw>>");
		String target = null;
		target = "/login/reset";

		return target;
	}
	
	/**
	 * 變更簽入密碼、交易密碼結果頁
	 */
	@RequestMapping(value = "/doublepw_result")
	public String doublepw_result(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/login/reset_error";
		BaseResult bs = null;
		log.trace("doublepw_result");
		try {    
			Map<String,String> okMap = ESAPIUtil.validStrMap(reqParam);
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			log.trace("doublepw_result.cusidn: {}", cusidn);
			
			// 重設簽入、交易密碼
			bs = reset_service.Reset(cusidn, okMap);
			log.debug("doublepw_result bs.getResult: {}", bs.getResult());
			
		} catch (Exception e) {
			log.error("doublepw_result", e);
		} finally {
			// 變更簽入及交易密碼至少其一成功
			if(bs!=null && bs.getResult()) {
				String acradio = ""; // 需變更簽入密碼或是交易密碼，提示用
				
				// 若都成功才導到成功頁
				if( "0".equals(bs.getMsgCode()) ) {
					target = "/login/reset_success";
					
				} // 交易密碼已變更成功，需變更簽入密碼
				else if ( "E178".equals(bs.getMsgCode()) ){
					acradio = "pw";
	            	target = "/login/chk_pw_alter";
	            	
				} // 簽入密碼已變更成功，需變更交易密碼
				else if ( "E225".equals(bs.getMsgCode()) ){
	            	acradio = "ssl";
	            	target = "/login/ssl_pw_alter";
				}
				
				model.addAttribute("ACRADIO", acradio);
				
			} else {
				model.addAttribute(BaseResult.ERROR, bs);				
			}
		}
		return target;
	}
	
	/**
	 * 密碼過期需要變更密碼
	 */
	@RequestMapping(value = "/expiredpw")
	public String expiredpw(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("expiredpw");
		return "/login/expiredpw";
	}
	
	/**
	 * 稍後變更
	 */
	@PostMapping(value = "/lateralter", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult lateralter(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		try {
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			log.trace("useoldpw.cusidn: {}", cusidn);
			
			// 登入 IP-Address
			String userip = ESAPIUtil.validInput(WebUtil.getIpAddr(request), "GeneralString", true);
			
			// 成功登入需發N912取得登入需要的資訊
			bs = new BaseResult();
			login_out_service.expiredpwN912(cusidn, bs);
			
			// N912失敗
			if (!bs.getResult()) {
				return bs;
			}
			
			// 登入所需基本資訊
			String tokenId = UUID.randomUUID().toString(); // 使用者識別
			Date logintime = new Date(); // 本次登入日期時間
			
			// 取得ADMLOGIN上次N911驗證成功的資料
			ADMLOGIN po = login_out_service.getAdmLogin(cusidn);
			
			// 成功登入後驗證是否為行內IP非行員身分
			String staff = "Y".equals(po.getSTAFF()) ? "Y" : "N";
			boolean ipVerifyResult = login_out_service.ipVerify(cusidn, userip, staff);
			log.debug("login_aj.ipVerifyResult: {}", ipVerifyResult);
			if (!ipVerifyResult) {
				// 驗證失敗剔退
				bs.setResult(false);
				bs.setMsgCode("99204");
				bs.setMessage(i18n.getMsg("LB.X2151"));
				return bs;
			}
			
			// 成功初始化將資訊存進session及DB
			if( bs.getResult() ) {
				// 生成UUID做登入後request檢核
				SessionUtil.addAttribute(model, SessionUtil.TOKENID, 
						tokenId );
				// 登入時間
				SessionUtil.addAttribute(model, SessionUtil.LOGINTIME, 
						logintime );
				// 上次登入日期
				String login_dt_tm = po.getLOGINTIME();
				SessionUtil.addAttribute(model, SessionUtil.LOGINDT, 
						DateUtil.convertDate(1, login_dt_tm, "yyyyMMddHHmmss", "yyyy-MM-dd"));
				// 上次登入時間
				SessionUtil.addAttribute(model, SessionUtil.LOGINTM, 
						DateUtil.convertDate(0, login_dt_tm, "yyyyMMddHHmmss", "HH:mm:ss"));
				// 資料庫-TXNUSER-我的Email
				SessionUtil.addAttribute(model, SessionUtil.DPMYEMAIL, 
						((Map<String,Object>) bs.getData()).get("DPMYEMAIL") );
				// N912業務權限
				String authority = ((Map<String,Object>) bs.getData()).get("AUTHORITY")!=null
						? ((Map<String,Object>) bs.getData()).get("AUTHORITY").toString() : "";
				SessionUtil.addAttribute(model, SessionUtil.AUTHORITY, authority );
				
				// 憑證代碼
				SessionUtil.addAttribute(model, SessionUtil.XMLCOD, 
						po.getXMLCOD() );
				// 是否是IKEY使用者
				boolean xmlcod = po.getXMLCOD()!=null ? login_out_service.isIKeyUser(po.getXMLCOD()) : false;
				SessionUtil.addAttribute(model, SessionUtil.ISIKEYUSER, xmlcod);
				
				// Client端IP
				SessionUtil.addAttribute(model, SessionUtil.USERIP, 
						userip );
				// 行動銀行啟用狀態
				SessionUtil.addAttribute(model, SessionUtil.MBSTAT, 
						po.getMBSTAT() );
				// 行動銀行上次啟用/關閉日期
				SessionUtil.addAttribute(model, SessionUtil.MBOPNDT, 
						po.getMBOPNDT() );
				// 行動銀行上次啟用/關閉時間
				SessionUtil.addAttribute(model, SessionUtil.MBOPNTM, 
						po.getMBOPNTM() );
				// 是否為行員
				SessionUtil.addAttribute(model, SessionUtil.STAFF, 
						po.getSTAFF() );
				// 全球金融網是否轉置
				SessionUtil.addAttribute(model, SessionUtil.HKCODE, 
												po.getHKCODE() );
				
				
				// 第一、三階段上線
				if("1".equals(SpringBeanFactory.getBean("isProd")) || "3".equals(SpringBeanFactory.getBean("isProd"))) {
					// 在記錄使用者登入資訊前，先查詢，判斷使用者是否為第一次登入
					boolean everLoginResult = first_login_service.everLogin(cusidn);
					log.debug("everLogin.everLoginResult: {}", everLoginResult);
					
					// 判斷使用者第一次登入轉檔結果是否皆為成功
					boolean firstCheckResult = first_login_service.firstCheck(cusidn);
					log.debug("everLogin.firstCheckResult: {}", firstCheckResult);
					
					// 使用者曾經登入新個網
					if(everLoginResult && firstCheckResult) {
						SessionUtil.addAttribute(model, SessionUtil.FIRSTLOGIN, false );
						
					} // 需做資料轉檔 ( 使用者第一次登入新個網 或 第一次登入轉檔結果有失敗 )
					else {
						SessionUtil.addAttribute(model, SessionUtil.FIRSTLOGIN, true );
						
						// 避免前一次登入轉檔失敗的可能，轉檔前先清除原本的資料
						first_login_service.firstClear(cusidn);
						
						// 使用者設定檔 需要先轉檔，其他等到首頁再處理
						boolean result = first_login_service.txnuser_tx(cusidn);
						log.debug("txnuser_tx.result: {}", result);
					}
				}
				
				
				// 登入成功--登入資訊存資料庫
				boolean admloginresult = login_out_service.setAdmLogin(cusidn, tokenId, logintime, userip, (Map<String, Object>) bs.getData());
				if (!admloginresult) {
					bs.setMsgCode("FE0009");
					bs.setMessage("登入資訊處理異常");
					bs.setResult(false);
					return bs;
				}
				log.debug("login_aj.setAdmLogin.end");
				
			}
			
		} catch (Exception e) {
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * 沿用舊密碼
	 */
	@PostMapping(value = "/useoldpw", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult useoldpw(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			log.trace("useoldpw.cusidn: {}", cusidn);
			
			bs = reset_service.keepPW(cusidn);
			log.trace("useoldpw.bs.getResult: {}", bs.getResult());
			
			// 登入 IP-Address
			String userip = ESAPIUtil.validInput(WebUtil.getIpAddr(request), "GeneralString", true);
			
			// 成功沿用舊密碼後登入，初始化登入資訊
			if (bs.getResult()) {
				// 成功登入需發N912取得登入需要的資訊
				login_out_service.expiredpwN912(cusidn, bs);
				
				// N912失敗
				if(!bs.getResult()) {
					return bs;
				}
				
			}
			
			// 登入所需基本資訊
			String tokenId = UUID.randomUUID().toString(); // 使用者識別
			Date logintime = new Date(); // 本次登入日期時間
			
			// 取得ADMLOGIN上次N911驗證成功的資料
			ADMLOGIN po = login_out_service.getAdmLogin(cusidn);
			
			// 驗證是否為行內IP非行員身分
			String staff = "Y".equals(po.getSTAFF()) ? "Y" : "N";
			boolean ipVerifyResult = login_out_service.ipVerify(cusidn, userip, staff);
			log.debug("login_aj.ipVerifyResult: {}", ipVerifyResult);
			if( !ipVerifyResult ) {
				// 驗證失敗剔退
				bs.setResult(false);
				bs.setMsgCode("99204");
				bs.setMessage(i18n.getMsg("LB.X2151"));
				return bs;
			}
			
			// 成功初始化將資訊存進session及DB
			if( bs.getResult() ) {
				// 生成UUID做登入後request檢核
				SessionUtil.addAttribute(model, SessionUtil.TOKENID, 
												tokenId );
				// 登入時間
				SessionUtil.addAttribute(model, SessionUtil.LOGINTIME, 
												logintime );
				// 上次登入日期
				String login_dt_tm = po.getLOGINTIME();
				SessionUtil.addAttribute(model, SessionUtil.LOGINDT, 
												DateUtil.convertDate(1, login_dt_tm, "yyyyMMddHHmmss", "yyyy-MM-dd"));
				// 上次登入時間
				SessionUtil.addAttribute(model, SessionUtil.LOGINTM, 
												DateUtil.convertDate(0, login_dt_tm, "yyyyMMddHHmmss", "HH:mm:ss"));
				// 資料庫-TXNUSER-我的Email
				SessionUtil.addAttribute(model, SessionUtil.DPMYEMAIL, 
												((Map<String,Object>) bs.getData()).get("DPMYEMAIL") );
				// N912業務權限
				String authority = ((Map<String,Object>) bs.getData()).get("AUTHORITY")!=null
										? ((Map<String,Object>) bs.getData()).get("AUTHORITY").toString() : "";
				SessionUtil.addAttribute(model, SessionUtil.AUTHORITY, authority );
				
				// 憑證代碼
				SessionUtil.addAttribute(model, SessionUtil.XMLCOD, 
												po.getXMLCOD() );
				// 是否是IKEY使用者
				boolean xmlcod = po.getXMLCOD()!=null ? login_out_service.isIKeyUser(po.getXMLCOD()) : false;
				SessionUtil.addAttribute(model, SessionUtil.ISIKEYUSER, xmlcod);
				
				// Client端IP
				SessionUtil.addAttribute(model, SessionUtil.USERIP, 
												userip );
				// 行動銀行啟用狀態
				SessionUtil.addAttribute(model, SessionUtil.MBSTAT, 
												po.getMBSTAT() );
				// 行動銀行上次啟用/關閉日期
				SessionUtil.addAttribute(model, SessionUtil.MBOPNDT, 
												po.getMBOPNDT() );
				// 行動銀行上次啟用/關閉時間
				SessionUtil.addAttribute(model, SessionUtil.MBOPNTM, 
												po.getMBOPNTM() );
				// 是否為行員
				SessionUtil.addAttribute(model, SessionUtil.STAFF, 
												po.getSTAFF() );
				// 全球金融網是否轉置
				SessionUtil.addAttribute(model, SessionUtil.HKCODE, 
												po.getHKCODE() );
				
				
				// 第一階段上線或第三階段上線要判斷是否首次登入、是否首登轉檔失敗
				if("1".equals(SpringBeanFactory.getBean("isProd")) || "3".equals(SpringBeanFactory.getBean("isProd"))) {
					// 在記錄使用者登入資訊前，先查詢，判斷使用者是否為第一次登入
					boolean everLoginResult = first_login_service.everLogin(cusidn);
					log.debug("everLogin.everLoginResult: {}", everLoginResult);
					
					// 判斷使用者第一次登入轉檔結果是否皆為成功
					boolean firstCheckResult = first_login_service.firstCheck(cusidn);
					log.debug("everLogin.firstCheckResult: {}", firstCheckResult);
					
					// 使用者曾經登入新個網
					if(everLoginResult && firstCheckResult) {
						SessionUtil.addAttribute(model, SessionUtil.FIRSTLOGIN, false );
						
					} // 需做資料轉檔 ( 使用者第一次登入新個網 或 第一次登入轉檔結果有失敗 )
					else {
						SessionUtil.addAttribute(model, SessionUtil.FIRSTLOGIN, true );
						
						// 避免前一次登入轉檔失敗的可能，轉檔前先清除原本的資料
						first_login_service.firstClear(cusidn);
						
						// 使用者設定檔 需要先轉檔，其他等到首頁再處理
						boolean result = first_login_service.txnuser_tx(cusidn);
						log.debug("txnuser_tx.result: {}", result);
					}
				}
				
				// 登入成功--登入資訊存資料庫
				boolean admloginresult = login_out_service.setAdmLogin(cusidn, tokenId, logintime, userip, (Map<String, Object>) bs.getData());
				if (!admloginresult) {
					bs.setMsgCode("FE0009");
					bs.setMessage("登入資訊處理異常");
					bs.setResult(false);
					return bs;
				}
				log.debug("login_aj.setAdmLogin.end");
				
			}
			
		} catch (Exception e) {
			log.error("", e);
		}
		return bs;
	}

	/**
	 * 密碼變更一成功一失敗後要登出再登入變更另一個
	 */
	@RequestMapping(value = "/pw_alter_in")
	public String pw_alter_in(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		String target = "/login/reset_error";
		log.trace("pw_alter_in...");
		
		BaseResult bs = null;
		try {
			String ACRADIO = reqParam.get("ACRADIO");
	
			if (ACRADIO.equals("pw")) {
				// 簽入密碼變更更改頁
				target = "/login/chk_pw_alter";
				
			} else if (ACRADIO.equals("ssl")) {
				// 交易密碼變更更改頁
				target = "/login/ssl_pw_alter";
				
			} else {
				// 重設密碼失敗頁
				bs = new BaseResult();
				bs.setMsgCode("E296");
				bs.setMessage(i18n.getMsg("LB.X1777"));
				model.addAttribute(BaseResult.ERROR, bs);
			}
			
		} catch (Exception e) {
			log.error("", e);
		}
		return target;
	}
	
	/**
	 * 簽入密碼變更
	 */
	@RequestMapping(value = "/pw_alter")
	public String pw_alter(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/login/reset_error";
		BaseResult bs = null;
		log.trace("pw_alter");
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			log.debug("pw_alter.cusidn >>{}", cusidn);
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			bs = personal_serving_service.pw_alter_result(cusidn, okMap);
			log.trace("pw_alter.bs.getResult>> {}",bs.getResult());
			
		} catch (Exception e) {
			log.error("pw_alter", e);
		}finally {
			if(bs!=null && bs.getResult()) {
				target = "/login/reset_success";				
							
			}else {
				model.addAttribute(BaseResult.ERROR, bs);				
			}
		}
		return target;
	}
	
	/**
	 * 交易密碼變更
	 */
	@RequestMapping(value = "/ssl_pw_alter")
	public String ssl_pw_alter(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/login/reset_error";
		BaseResult bs = null;
		log.trace("ssl_pw_alter...");
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			log.debug("ssl_pw_alter.cusidn >>{}", cusidn);
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			bs = personal_serving_service.pw_alter_ssl_result(cusidn, okMap);
			log.trace("ssl_pw_alter.bs.getResult>> {}",bs.getResult());
			
		} catch (Exception e) {
			log.error("ssl_pw_alter", e);
			
		} finally {
			if(bs!=null && bs.getResult()) {
				target = "/login/reset_success";				
				
			} else {
				model.addAttribute(BaseResult.ERROR, bs);				
			}
		}
		return target;
	}
	
	/**
	 * 密碼鎖定解除--輸入頁
	 */
	@RequestMapping(value = "/lock_reset")
	public String lock_reset(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("lock_reset...");
		return "/login/lock_reset";
	}
	
	/**
	 * 密碼鎖定解除--確認頁
	 */
	@RequestMapping(value = "/lock_reset_confirm")
	public String lock_reset_confirm(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("lock_reset_confirm...");
		String cusidn = "";
		String jsondc = "";
		String target = "/login/reset_error";
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam); 
		try {
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("tuition_fee_confirm.jsondc: " + jsondc));
						
			cusidn = okMap.get("CUSIDN");
			log.debug(ESAPIUtil.vaildLog("lock_reset_confirm.userId: {}"+ cusidn));
			
		} catch (Exception e) {
			log.error("", e);
			
		} finally {
			model.addAttribute("cusidn", cusidn);
			model.addAttribute("jsondc", jsondc);
			target = "/login/lock_reset_confirm";
		}
		
		return target;
	}
	
	/**
	 * 密碼鎖定解除--結果頁
	 */
	@RequestMapping(value = "/lock_reset_result")
	public String lock_reset_result(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("lock_reset_result...");
		String cusidn = "";
		String target = "/login/reset_error";
		
		BaseResult bs = new BaseResult();
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam); 
		try {
			cusidn = okMap.get("CUSIDN");
			log.debug(ESAPIUtil.vaildLog("lock_reset_result.cusidn: {}"+ cusidn));
			
			bs = login_out_service.lock_reset_result(cusidn, okMap);
			
		} catch (Exception e) {
			log.error("", e);
			
		} finally {
			if(bs!=null && bs.getResult()) {
				model.addAttribute("cusidn", cusidn);
				target = "/login/lock_reset_result";				
				
			} else {
				model.addAttribute(BaseResult.ERROR, bs);				
			}
		}
		
		return target;
	}
	
	/**
	 * 使用者名稱變更
	 */
	@RequestMapping(value = "/username_reset")
	public String username_reset(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("username_reset...");
		String userId = "";
		String target = "/login/reset_error";
		try {
			userId = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.USERID, null)), "utf-8");
			log.debug("username_reset.userId: {}", userId);
			
		} catch (Exception e) {
			log.error("", e);
			
		} finally {
			model.addAttribute("userId", userId);
			target = "/login/username_reset";
		}
		
		return target;
	}
	
	/**
	 * 使用者名稱變更
	 */
	@RequestMapping(value = "/username_reset_result")
	public String username_reset_result(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/login/reset_error";
		BaseResult bs = null;
		String cusidn = "";
		log.trace("username_reset...");
		try {
			bs = new BaseResult();
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			bs = personal_serving_service.username_alter_result(cusidn, okMap);
			
			log.trace("username_reset.bs.getResult: {}", bs.getResult());
			
		} catch (Exception e) {
			log.error("", e);
			
		} finally {
			if(bs!=null && bs.getResult()) {
				target = "/login/reset_success";				
				
			} else {
				model.addAttribute(BaseResult.ERROR, bs);				
			}
		}
		return target;
	}
	
	// 取得公告訊息
	@PostMapping(value = "/bulletin_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult getBulletin(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("getBulletin...");
		// 公告訊息
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// 取得公告訊息s
			List<ADMANN> bulletin_List = login_out_service.getByAll();
			if(bulletin_List != null && !bulletin_List.isEmpty()) {
				bs.setResult(true);
				bs.setMsgCode("0");
				bs.setMessage("getBulletin success");
				bs.setData(bulletin_List);
			}
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	
	// 取得公告訊息--檔案
	@GetMapping(value = "/getAnn/{id}")
	public void getAnn(@PathVariable String id, HttpServletRequest request, HttpServletResponse response) {
		log.trace("getAnn...");
		try {
			byte[] imageContent = null;
			String fileName = "";

			// 取得公告訊息s
			ADMANN po = login_out_service.getById(id);
			fileName = po.getSRCFILENAME();
            imageContent = po.getSRCCONTENT();
	        
	        String fileExt="";
	        int lastIndexOf = fileName.lastIndexOf(".");
	        if (lastIndexOf != -1) {
	            fileExt = fileName.substring(lastIndexOf+1);
	        }
	        
	        if ( fileExt.compareToIgnoreCase("jpg") == 0 || fileExt.compareToIgnoreCase("jpeg") == 0 ) {
	            response.setContentType(MediaType.IMAGE_JPEG_VALUE);
	        } else if ( fileExt.compareToIgnoreCase("png") == 0 ) {
	            response.setContentType(MediaType.IMAGE_PNG_VALUE);
	        } else if ( fileExt.compareToIgnoreCase("gif") == 0 ) {
	            response.setContentType(MediaType.IMAGE_GIF_VALUE);
	        } else if ( fileExt.compareToIgnoreCase("pdf") == 0 ) {
	            response.setContentType(MediaType.APPLICATION_PDF_VALUE);
	        }
	        
	        InputStream in = new ByteArrayInputStream(imageContent);
	        IOUtils.copy(in, response.getOutputStream());
	        
		} catch (Exception e) {
			log.error("",e);
		}
	}
	
	// 取得廣告訊息
	@PostMapping(value = "/banner_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult getbanner(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("getbanner...");
		// 廣告訊息
		BaseResult bs = null;
		String num = "";
		try {
			bs = new BaseResult();
			num = reqParam.get("num");
			// 取得廣告訊息s
			ADMADS bulletin_List = login_out_service.getAds(num);
			if(bulletin_List != null) {
				bs.setResult(true);
				bs.setMsgCode("0");
				bs.setMessage("getBulletin success");
				String text = new String(bulletin_List.getTARGETCONTENT(),"UTF-8");
				text = text.replaceAll("\n\r","<br/>");
				text = text.replaceAll("\r\n","<br/>");
				text = text.replaceAll("\n","<br/>");
				text = text.replaceAll("\r","<br/>");
				bulletin_List.setShowText(text);
				bs.setData(bulletin_List);
			}
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	
	// 取得廣告訊息--檔案
	@GetMapping(value = "/getAds/{id}")
	public void getAds(@PathVariable String id, HttpServletRequest request, HttpServletResponse response) {
		log.trace("getAds...");
		try {
			byte[] imageContent = null;
			String fileName = "";
			
			// 取得公告訊息s
			ADMADS po = login_out_service.getAdsById(id);
			fileName = po.getTARGETFILENAME();
			imageContent = po.getTARGETCONTENT();
			
			String fileExt="";
			int lastIndexOf = fileName.lastIndexOf(".");
			if (lastIndexOf != -1) {
				fileExt = fileName.substring(lastIndexOf+1);
			}
			
			if ( fileExt.compareToIgnoreCase("jpg") == 0 || fileExt.compareToIgnoreCase("jpeg") == 0 ) {
				response.setContentType(MediaType.IMAGE_JPEG_VALUE);
			} else if ( fileExt.compareToIgnoreCase("png") == 0 ) {
				response.setContentType(MediaType.IMAGE_PNG_VALUE);
			} else if ( fileExt.compareToIgnoreCase("gif") == 0 ) {
				response.setContentType(MediaType.IMAGE_GIF_VALUE);
			} else if ( fileExt.compareToIgnoreCase("pdf") == 0 ) {
				response.setContentType(MediaType.APPLICATION_PDF_VALUE);
			}
			
			InputStream in = new ByteArrayInputStream(imageContent);
			IOUtils.copy(in, response.getOutputStream());
			
		} catch (Exception e) {
			log.error("",e);
		}
	}

	@PostMapping(value = "/clearMenuList")
	public @ResponseBody BaseResult clearMenuList(HttpServletRequest request, HttpServletResponse response, 
													@RequestParam Map<String, String> reqParam, Model model, SessionStatus status) {
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			String ip = WebUtil.getIpAddr(request);
			String set_ip = clear_pass_ip;
			log.debug(ESAPIUtil.vaildLog("ip address >> "+ ip));
			if (StrUtil.isEmpty(ip)) {
				bs.setResult(false);
				return bs;
			}
			List<String> set_ip_List = Arrays.asList(set_ip.split(","));
			log.debug("ip list >> {}", set_ip_List);
			// 判斷IP
			if (set_ip_List.contains(ip)) {
				login_out_service.deleteMenuList("ALL");
				login_out_service.deleteMenuList("ATM");
				login_out_service.deleteMenuList("ATMT");
				login_out_service.deleteMenuList("CRD");
				login_out_service.deleteMenuList("null");
				bs.setResult(true);
				bs.setMessage("0", "");
			}else {
				bs.setResult(false);
			}
		} catch (Exception e) {
			bs = new BaseResult();
			bs.setResult(false);
			log.error(e.getMessage());
		}
		return bs;
	}
	@PostMapping(value = "/clearAdmNbStatu")
	public @ResponseBody BaseResult clearAdmNbStatu(HttpServletRequest request, HttpServletResponse response, 
													@RequestParam Map<String, String> reqParam, Model model, SessionStatus status) {
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			String ip = WebUtil.getIpAddr(request);
			String set_ip = clear_pass_ip;
			log.debug(ESAPIUtil.vaildLog("ip address >> "+ip ));
			if (StrUtil.isEmpty(ip)) {
				bs.setResult(false);
				return bs;
			}
			List<String> set_ip_List = Arrays.asList(set_ip.split(","));
			log.debug("ip list >> {}", set_ip_List);
			// 判斷IP
			if (set_ip_List.contains(ip)) {
				login_out_service.deleteAdmNbStatu();
				bs.setResult(true);
				bs.setMessage("0", "");
			}else {
				bs.setResult(false);
			}
		} catch (Exception e) {
			bs = new BaseResult();
			bs.setResult(false);
			log.error(e.getMessage());
		}
		return bs;
	}
	
	
	/**
	 * 網銀登入前檢查
	 */
	@PostMapping(value = "/checkSys_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult checkSys_aj(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			// 必須檢查狀態為未登入之系統
//			String[] sys = {"NB3", "PORTAL", "MB3"};
			String[] sys = logincheck.split(",");
			log.info("checkSys_aj.sys: {}", Arrays.toString(sys));
			
			String resultType = "";
			boolean checkResult = true;
			
			for (String system : sys) {
				
				switch(system) {
					case "NB3":
						try {
							String isCheckNB3 = SpringBeanFactory.getBean("isCheckNB3");
							// NB3須檢核Portal是否登入
							if("Y".equals(isCheckNB3)) {
								boolean checkLoginStatus = login_out_service.checkLogin(okMap, WebUtil.getIpAddr(request));
								log.debug("checkLogin_aj.checkLoginStatus: {}", checkLoginStatus);
								// NB3是登入狀態且登入ip不同，需要後踢前才可登入
								if(!checkLoginStatus) {
									checkResult = false;
									resultType = "1";
								}
							}
						} catch (Exception e) {
							log.error("",e);
						}
						
						log.debug("checkSys_aj.NB3 OK!!!");
						break;
						
					case "PORTAL":
						try {
							String isCheckPortal = SpringBeanFactory.getBean("isCheckPortal");
							// NB3須檢核Portal是否登入
							if("Y".equals(isCheckPortal)) {
								// 使用者統編
								String cusidn = okMap.get("cusidn").toUpperCase();
								log.debug(ESAPIUtil.vaildLog("getportalstatus.cusidn: {}"+ cusidn));
								
								// 使用者名稱
								String userName = okMap.get("userName");
								log.debug(ESAPIUtil.vaildLog("getportalstatus.userName: {}"+ userName));
								
								bs = new BaseResult();
								boolean result = portal_service.getPortalStatus(cusidn, userName, "NB3");
								// Portal登入中
								if(result) {
									checkResult = false;
									resultType = "2";
								}
							}
						} catch (Exception e) {
							log.error("",e);
						}
						
						log.debug("checkSys_aj.PORTAL OK!!!");
						break;
					
					case "MB3":
						try {
							String isCheckMB3 = SpringBeanFactory.getBean("isCheckMB3");
							// NB3須檢核MB3是否登入
							if("Y".equals(isCheckMB3)) {
	//							String cusidn = Base64.getEncoder().encodeToString(okMap.get("cusidn").toUpperCase().getBytes());
								String cusidn = okMap.get("cusidn").toUpperCase();
								log.debug("kick MB3...");
								boolean result = mobile_service.getMB3StatusDB(cusidn); // MB3是否是登入狀態
								// MB3登入中
								if(result) {
									checkResult = false;
									resultType = "3";
								}
							}
						} catch (Exception e) {
							log.error("",e);
						}
						
						log.debug("checkSys_aj.MB3 OK!!!");
						break;
					
					default:
						log.debug("checkSys_aj default OK!!!");
				}
				
				if (!checkResult) {
					break;
				}
			}
			
			// 全部系統檢查完畢，NB3可直接登入，不需要後踢前
			if(checkResult) {
				bs.setResult(true);
				bs.setMsgCode("0");
				bs.setMessage("check Login OK!!!");
				
			} else {
				bs.setResult(false);
				bs.setMsgCode("H003");
				bs.setMessage("check Login Fail!!!");
				
				HashMap<String, String> data = new HashMap<String, String>();
				data.put("resultType", resultType);
				bs.setData(data);
				
			}
			
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	
	
	// 驗證驗證碼
    public BaseResult validedCapCode(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) throws Exception {
		log.debug("login.validedCapCode...");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// 取得request的驗證碼
			String capCode = StrUtil.isEmpty((reqParam.get("capCode"))) ? "" : reqParam.get("capCode");
			log.debug(ESAPIUtil.vaildLog("capCode >> " + capCode));
			// 取得session的驗證碼
			String code = (String) SessionUtil.getAttribute(model, SessionUtil.KAPTCHA_SESSION_KEY, null);
			log.debug(ESAPIUtil.vaildLog("sessionCapCode >> " + code));
			if(code == null) {
				bs.setResult(Boolean.FALSE);
				bs.setMsgCode("99348");
				bs.setMessage(i18n.getMsg("LB.X1758"));
				log.warn("SESSION TIMEOUT!!!");
				return bs;
			}
			
			// 通過驗證
			log.debug("captcha_valided: " + capCode.equalsIgnoreCase(code) );
			if(capCode.equalsIgnoreCase(code)) {
				bs.setResult(Boolean.TRUE);
				bs.setMsgCode("0");
				bs.setMessage(i18n.getMsg("LB.X1760"));
				log.debug("capCode.mapping...success!!!");
			} else {
				bs.setResult(Boolean.FALSE);
				bs.setMsgCode("99348");
				bs.setMessage(i18n.getMsg("LB.X1082"));
				log.warn("capCode.mapping...fail!!!");
			}
			
			if (!bs.getResult()) {
				log.warn("login.validedCapCode...fail!!!");
//				log.warn("login.validedCapCode.reqParam: " + reqParam); // 會有加密後的密碼
				log.warn(ESAPIUtil.vaildLog("login.validedCapCode.cusidn: " + reqParam.get("cusidn")));
				log.warn(ESAPIUtil.vaildLog("login.validedCapCode.capCode: " + capCode));
				log.warn(ESAPIUtil.vaildLog("login.validedCapCode.Session.capCode: " + code));
				log.warn(ESAPIUtil.vaildLog("login.validedCapCode.sessionId: " + request.getSession().getId()));
			}

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			bs.setResult(Boolean.FALSE);
			bs.setMsgCode("99348");
			bs.setMessage(i18n.getMsg("LB.X1082"));
			log.error("validedCapCode error >> {}",e);
		}

		return bs;
	}
    
    // 取得廣告訊息
    @PostMapping(value = "/txt_aj", produces = {"application/json;charset=UTF-8"})
 	public @ResponseBody BaseResult gettxt(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
 		log.trace("gettxt...");
 		// 廣告訊息
 		BaseResult bs = null;
 		String num = "";
 		try {
 			bs = new BaseResult();
 			num = reqParam.get("num");
 			int i = StrUtil.isEmpty(num) ? 0 : Integer.parseInt(num);
 			i = i > 5 ? 5 : i;
 			// 取得廣告訊息s
 			try {
 				List<Map<String, Object>> data = new ArrayList<>();
 				for(int s = 1; s <= i; s++) {
 					Map<String, Object> eachMap = new HashMap<String, Object>();
 					String pathname = context.getRealPath("") +"/com/login/banner/bs/banner-lg-" + s + ".txt"; 
 					String line = "";
 					InputStreamReader isr = new InputStreamReader(new FileInputStream(pathname), "UTF-8");
 					BufferedReader br = new BufferedReader(isr);
 					while (br.ready()) {
 						line = line + br.readLine();
 					}
 					br.close();
 					isr.close();
 					eachMap.put("TITLE", line);
 					data.add(eachMap);
 				}
 				bs.addData("ADST", data);
 			}
 			catch (Exception e) {
 				log.debug("ERROR>>>"+e);
 			}
 		} catch (Exception e) {
 			log.error("",e);
 		}
 		return bs;
 	}
 	
 	
	/**
	 * 20201209--登入時記錄使用者請求資訊
	 */
 	public void login_info(HttpServletRequest request, Map<String, String> headers) {
 		try {
 			log.info("-------------------------- login_info.start --------------------------");
 			
 			// cookies
 			log.info("-------------------------- login_info-Cookie --------------------------");
 			for (Cookie c : request.getCookies()) {
 				log.info(ESAPIUtil.vaildLog(c.getName() + " = " + c.getValue()));
//            	log.info(c.getName() + " = " + c.getValue()); // Log Forging
 			}

 			// headers
 			log.info("-------------------------- login_info-Header --------------------------");
	 		headers.forEach((key, value) -> {
	 			log.info(ESAPIUtil.vaildLog(String.format("Header '%s' = %s", key, value)));
//	 	        log.info(String.format("Header '%s' = %s", key, value)); // Log Forging
	 	    });
	 		
 			// remote address
 			log.info("-------------------------- login_info-Remote Address --------------------------");
 			log.info(ESAPIUtil.vaildLog(request.getRemoteAddr()));
// 			log.info(request.getRemoteAddr()); // Log Forging
 			
 			// client ip
 			log.info("-------------------------- login_info-Client IP --------------------------");
 			log.info(ESAPIUtil.vaildLog(WebUtil.getIpAddr(request)));
// 			log.info(WebUtil.getIpAddr(request)); // Log Forging
 			
 			log.info("-------------------------- login_info.NB2.5 Header API --------------------------");

			// NB2.5 Header API
			Enumeration<String> headerNames = request.getHeaderNames();
			while(headerNames.hasMoreElements()) {
			  String headerName = headerNames.nextElement();
			  log.info(ESAPIUtil.vaildLog("Header Name - " + headerName + ", Value - " + request.getHeader(headerName)));
			}
			
			log.info("-------------------------- login_info.end --------------------------");
			
 		} catch (Throwable e) {
 			log.error("",e);
 		}
 	}

}
