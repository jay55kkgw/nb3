package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class Ratequery_N026_RSDATA extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5519551743383530409L;

	private String FILL;
	private String ITR1;
	private String ITR2;
	private String HEADER;
	private String TIME;
	private String TERM;
	private String ITR5;
	private String ITR6;
	private String ITR3;
	private String ITR4;
	private String DATE;
	private String RECNO;
	private String COLOR;
	private String COUNT;
	private String SEQ;

	public String getFILL() {
		return FILL;
	}

	public String getITR1() {
		return ITR1;
	}

	public String getITR2() {
		return ITR2;
	}

	public String getHEADER() {
		return HEADER;
	}

	public String getTIME() {
		return TIME;
	}

	public String getTERM() {
		return TERM;
	}

	public String getITR5() {
		return ITR5;
	}

	public String getITR6() {
		return ITR6;
	}

	public String getITR3() {
		return ITR3;
	}

	public String getITR4() {
		return ITR4;
	}

	public String getDATE() {
		return DATE;
	}

	public String getRECNO() {
		return RECNO;
	}

	public String getCOLOR() {
		return COLOR;
	}

	public String getCOUNT() {
		return COUNT;
	}

	public String getSEQ() {
		return SEQ;
	}

	public void setFILL(String fILL) {
		FILL = fILL;
	}

	public void setITR1(String iTR1) {
		ITR1 = iTR1;
	}

	public void setITR2(String iTR2) {
		ITR2 = iTR2;
	}

	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}

	public void setTIME(String tIME) {
		TIME = tIME;
	}

	public void setTERM(String tERM) {
		TERM = tERM;
	}

	public void setITR5(String iTR5) {
		ITR5 = iTR5;
	}

	public void setITR6(String iTR6) {
		ITR6 = iTR6;
	}

	public void setITR3(String iTR3) {
		ITR3 = iTR3;
	}

	public void setITR4(String iTR4) {
		ITR4 = iTR4;
	}

	public void setDATE(String dATE) {
		DATE = dATE;
	}

	public void setRECNO(String rECNO) {
		RECNO = rECNO;
	}

	public void setCOLOR(String cOLOR) {
		COLOR = cOLOR;
	}

	public void setCOUNT(String cOUNT) {
		COUNT = cOUNT;
	}

	public void setSEQ(String sEQ) {
		SEQ = sEQ;
	}

}
