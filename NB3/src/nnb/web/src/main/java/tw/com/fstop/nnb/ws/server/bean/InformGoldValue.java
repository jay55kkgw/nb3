package tw.com.fstop.nnb.ws.server.bean;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

//@XmlRootElement(namespace = "http://ws.fstop", name="Gold")
@XmlRootElement(namespace = "http://ws.fstop", name="informGoldValue")
@XmlAccessorType(XmlAccessType.FIELD)
public class InformGoldValue {

	public Gold getIn0() {
		return in0;
	}

	public void setIn0(Gold in0) {
		this.in0 = in0;
	}

	@XmlElement(namespace = "http://ws.fstop")
	public Gold in0;

	
	
	
	
}
