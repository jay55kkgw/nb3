package tw.com.fstop.nnb.ws.server;

import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import tw.com.fstop.nnb.ws.server.bean.InformGoldValue;
import tw.com.fstop.nnb.ws.server.bean.Gold;
import tw.com.fstop.nnb.ws.server.bean.GoldSell;
import tw.com.fstop.nnb.ws.server.bean.InformGoldValueResponse;
@Endpoint
public interface GoldService {
	
//	public  GoldSell informGoldValue( Gold gold);
	public InformGoldValueResponse informGoldValue(InformGoldValue gold);
//	public InformGoldValueResponse informGoldValue(Gold gold);
//	public @ResponsePayload GoldSell informGoldValue(@RequestPayload Gold gold);
//	public InformGoldValueResponse informGoldValue(Gold gold);

}
