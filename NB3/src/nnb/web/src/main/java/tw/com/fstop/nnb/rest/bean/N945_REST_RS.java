package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N945_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6617993723549341328L;
	
	private String MSGCOD;
    
	public String getMSGCOD() {
		return MSGCOD;
	}

	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}

}