package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N8301_IDGATE_DATA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -762951082705400880L;

	@SerializedName(value = "TSFACN")
	private String TSFACN;

	@SerializedName(value = "CUSIDN1")
	private String CUSIDN1;
	
	@SerializedName(value = "ADOPID")
	private String ADOPID;
	
	@SerializedName(value = "TYPE")
	private String TYPE;
	
	@SerializedName(value = "ITMNUM")
	private String ITMNUM;
	
	@SerializedName(value = "HLHBRH")
	private String HLHBRH;
	
	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}

	public String getTYPE() {
		return TYPE;
	}

	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}

	public String getITMNUM() {
		return ITMNUM;
	}

	public void setITMNUM(String iTMNUM) {
		ITMNUM = iTMNUM;
	}

	public String getTSFACN() {
		return TSFACN;
	}

	public void setTSFACN(String tSFACN) {
		TSFACN = tSFACN;
	}

	public String getCUSNUM1() {
		return CUSIDN1;
	}

	public void setCUSIDN1(String cUSIDN1) {
		CUSIDN1 = cUSIDN1;
	}

	public String getHLHBRH() {
		return HLHBRH;
	}

	public void setHLHBRH(String hLHBRH) {
		HLHBRH = hLHBRH;
	}

}
