package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

import tw.com.fstop.util.StrUtils;

public class N833_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1476589265099000197L;

	@SerializedName(value = "TSFACN")
	private String TSFACN;

	@SerializedName(value = "MEMO_C")
	private String MEMO_C;
	
	@SerializedName(value = "CARDNUM")
	private String CARDNUM;

	@SerializedName(value = "UNTNUM")
	private String UNTNUM;

	@Override
	public Map<String, String> coverMap() {
		LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
		if(StrUtils.isEmpty(this.CARDNUM)) {
			result.put("交易名稱", "自動扣繳取消");
			result.put("交易類型", "取消自動扣繳");
			result.put("扣繳方式", "存款帳號");
			result.put("扣帳帳號", this.TSFACN);
			result.put("代繳類別", this.MEMO_C);
			result.put("用戶編號", this.UNTNUM);
		}else {
			result.put("交易名稱", "自動扣繳取消");
			result.put("交易類型", "取消自動扣繳");
			result.put("扣繳方式", "信用卡");
			result.put("卡號", this.CARDNUM);
			result.put("業務項目", this.MEMO_C);
			result.put("用戶編號", this.UNTNUM);
		}
		return result;
	}

	public String getTSFACN() {
		return TSFACN;
	}

	public void setTSFACN(String tSFACN) {
		TSFACN = tSFACN;
	}

	public String getMEMO_C() {
		return MEMO_C;
	}

	public void setMEMO_C(String mEMO_C) {
		MEMO_C = mEMO_C;
	}

	public String getUNTNUM() {
		return UNTNUM;
	}

	public void setUNTNUM(String uNTNUM) {
		UNTNUM = uNTNUM;
	}

	public String getCARDNUM() {
		return CARDNUM;
	}

	public void setCARDNUM(String cARDNUM) {
		CARDNUM = cARDNUM;
	}
}
