package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N073_IDGATE_DATA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -762951082705400880L;

	// 0：非約定，1：約定
	@SerializedName(value = "FLAG")
	private String FLAG;
	
	// 轉出帳號
	@SerializedName(value = "ACN")
	private String ACN;
	
	// 轉出帳號
	@SerializedName(value = "DPACNO")
	private String DPACNO;
	
	// 轉帳金額
	@SerializedName(value = "AMOUNT")
	private String AMOUNT;

	public String getFLAG() {
		return FLAG;
	}

	public void setFLAG(String fLAG) {
		FLAG = fLAG;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	public String getDPACNO() {
		return DPACNO;
	}

	public void setDPACNO(String dPACNO) {
		DPACNO = dPACNO;
	}

	public String getAMOUNT() {
		return AMOUNT;
	}

	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	
	
}
