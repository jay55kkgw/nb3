package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * N174電文RS
 */
public class N174_REST_RS extends BaseRestBean implements Serializable{
	private static final long serialVersionUID = 6791228050721094802L;
	
	private String TSFACN;
	private String OUTCRY;
	private String TOTBAL;
	private String AVLBAL;
	private String FDPACC;
	private String TYPCOD;
	private String SDT;
	private String DUEDAT;
	private String AMOUNT;
	private String INTMTH;
	private String ITR;
	private String CODE;
	private String TRNDATE;
	private String TRNTIME;
	private String FDPNUM;
	private String MAC;
	private String LOTCNT;

	public String getTSFACN() {
		return TSFACN;
	}
	public void setTSFACN(String tSFACN) {
		TSFACN = tSFACN;
	}
	public String getOUTCRY() {
		return OUTCRY;
	}
	public void setOUTCRY(String oUTCRY) {
		OUTCRY = oUTCRY;
	}
	public String getTOTBAL() {
		return TOTBAL;
	}
	public void setTOTBAL(String tOTBAL) {
		TOTBAL = tOTBAL;
	}
	public String getAVLBAL() {
		return AVLBAL;
	}
	public void setAVLBAL(String aVLBAL) {
		AVLBAL = aVLBAL;
	}
	public String getFDPACC() {
		return FDPACC;
	}
	public void setFDPACC(String fDPACC) {
		FDPACC = fDPACC;
	}
	public String getTYPCOD() {
		return TYPCOD;
	}
	public void setTYPCOD(String tYPCOD) {
		TYPCOD = tYPCOD;
	}
	public String getSDT() {
		return SDT;
	}
	public void setSDT(String sDT) {
		SDT = sDT;
	}
	public String getDUEDAT() {
		return DUEDAT;
	}
	public void setDUEDAT(String dUEDAT) {
		DUEDAT = dUEDAT;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	public String getINTMTH() {
		return INTMTH;
	}
	public void setINTMTH(String iNTMTH) {
		INTMTH = iNTMTH;
	}
	public String getITR() {
		return ITR;
	}
	public void setITR(String iTR) {
		ITR = iTR;
	}
	public String getCODE() {
		return CODE;
	}
	public void setCODE(String cODE) {
		CODE = cODE;
	}
	public String getTRNDATE() {
		return TRNDATE;
	}
	public void setTRNDATE(String tRNDATE) {
		TRNDATE = tRNDATE;
	}
	public String getTRNTIME() {
		return TRNTIME;
	}
	public void setTRNTIME(String tRNTIME) {
		TRNTIME = tRNTIME;
	}
	public String getFDPNUM() {
		return FDPNUM;
	}
	public void setFDPNUM(String fDPNUM) {
		FDPNUM = fDPNUM;
	}
	public String getMAC() {
		return MAC;
	}
	public void setMAC(String mAC) {
		MAC = mAC;
	}
	public String getLOTCNT() {
		return LOTCNT;
	}
	public void setLOTCNT(String lOTCNT) {
		LOTCNT = lOTCNT;
	}
}