package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class F002T_REST_RS extends BaseRestBean implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3761615151410999591L;

	private String M257BIC;

	private String TERM_DATE;

	private String DATA_LENGTH;

	private String M272NA2;

	private String M155OPT;

	private String M272NA1;

	private String IND_TABLE;

	private String M258AD2;

	private String ERR_CODE;

	private String M258AD1;

	private String M157BIC;

	private String ORGAMT;//付款金額

	private String MSGHEADER;

	private String MSG_HEAD;

	private String M258AD4;

	private String HOST_TIME;

	private String M153ACC;

	private String M258AD3;

	private String BENTEL;

	private String BENFAX;

	private String LRGOUT_NUM;

	private String M177NA;

	private String M132AMT;

	private String M155AD2;

	private String M232DAT;

	private String M155AD1;

	private String M150AD4;

	private String M155AD4;

	private String COMMAMT2;

	private String M155AD3;

	private String TC_NO;

	private String M252BIC;

	private String M150AD2;

	private String M150AD3;

	private String M150AD1;

	private String ADTXNO;//資料庫編號

	private String M154BIC;

	private String M2REFNO;

	private String M258OPT;

	private String M150ACC;

	private String TXBRH;

	private String PMTCCY;// 收款幣別

	private String LENGTH;

	private String MSGCOD;

	private String M157OPT;

	private String M159BIC;

	private String CABCHG;//郵電費

	private String PGMID;

	private String M155ACC;

	private String TX_SEQ;

	private String M258BIC;

	private String STAN;

	private String M157AD3;

	private String M157AD4;

	private String M157AD1;

	private String M157AD2;

	private String TERM_TIME;

	private String COMMAMT;//手續費

	private String M154OPT;

	private String M272NA4;

	private String M272NA3;

	private String M272NA6;

	private String M272NA5;

	private String OURCHG;//國外費用

	private String M156BIC;

	private String M132DAT;

	private String M1SWIFT;

	private String __OCCURS;

	private String TX_ID;

	private String M133AMT;

	private String TL_ID;

	private String M150BIC;

	private String M1OPCOD;

	private String M159OPT;

	private String CHGROE;

	private String M2SBBIC;

	private String WK_ID;

	private String M154AD1;

	private String M257ACC;

	private String M154AD2;

	private String M154AD3;

	private String M154AD4;

	private String STATUS;

	private String M170NA3;

	private String M157ACC;

	private String M170NA2;

	private String M153BIC;

	private String M170NA1;

	private String M170NA4;

	private String M171CHG;

	private String CURAMT;

	private String M123TEL;

	private String TDBAMT;

	private String M1SBBIC;

	private String M159AD2;

	private String M159AD1;

	private String M159AD4;

	private String M252ACC;

	private String M159AD3;

	private String M232CCY;

	private String EXRATE;// 匯率

	private String M156OPT;

	private String WS_ID;

	private String M257AD1;

	private String PMTAMT;//收款金額

	private String AVAAMT;

	private String ETY_LVL;

	private String PCODE;

	private String M257AD4;

	private String M257AD2;

	private String M154ACC;

	private String M257AD3;

	private String M2SWIFT;

	private String ORGCCY;// 付款幣別

	private String STATUS1;

	private String M156AD1;

	private String BENEML;

	private String M156AD3;

	private String M150OPT;

	private String M156AD2;

	private String M156AD4;

	private String M153OPT;

	private String ORGCOMM;

	private String M221INF;

	private String BRH_CODE;

	private String REFNO;//銀行交易編號

	private String FORMAT_NAME;

	private String TX_CODE;

	private String M172NA6;

	private String M252AD1;

	private String M252AD2;

	private String SWTKIND;

	private String COMMCCY2;

	private String M155BIC;

	private String M252AD3;

	private String M159ACC;

	private String M257OPT;

	private String M252AD4;

	private String M172NA2;

	private String M172NA3;

	private String M172NA4;

	private String CHGROERL;

	private String M172NA5;

	private String M172NA1;

	private String DATA_LL;

	private String HIGH_IND;

	private String M252OPT;

	private String MSG_CODE;

	private String FILLER;

	private String ORGCABLE;

	private String M153AD1;

	private String M153AD2;

	private String M153AD3;

	private String M258ACC;

	private String M153AD4;

	private String M1REFNO;

	private String SEQNO;

	private String M132CCY;

	private String INDICTOR;

	private String BRH_COD;

	private String PF_KEY;

	private String M232AMT;

	private String M156ACC;

	public String getM257BIC()
	{
		return M257BIC;
	}

	public void setM257BIC(String m257bic)
	{
		M257BIC = m257bic;
	}

	public String getTERM_DATE()
	{
		return TERM_DATE;
	}

	public void setTERM_DATE(String tERM_DATE)
	{
		TERM_DATE = tERM_DATE;
	}

	public String getDATA_LENGTH()
	{
		return DATA_LENGTH;
	}

	public void setDATA_LENGTH(String dATA_LENGTH)
	{
		DATA_LENGTH = dATA_LENGTH;
	}

	public String getM272NA2()
	{
		return M272NA2;
	}

	public void setM272NA2(String m272na2)
	{
		M272NA2 = m272na2;
	}

	public String getM155OPT()
	{
		return M155OPT;
	}

	public void setM155OPT(String m155opt)
	{
		M155OPT = m155opt;
	}

	public String getM272NA1()
	{
		return M272NA1;
	}

	public void setM272NA1(String m272na1)
	{
		M272NA1 = m272na1;
	}

	public String getIND_TABLE()
	{
		return IND_TABLE;
	}

	public void setIND_TABLE(String iND_TABLE)
	{
		IND_TABLE = iND_TABLE;
	}

	public String getM258AD2()
	{
		return M258AD2;
	}

	public void setM258AD2(String m258ad2)
	{
		M258AD2 = m258ad2;
	}

	public String getERR_CODE()
	{
		return ERR_CODE;
	}

	public void setERR_CODE(String eRR_CODE)
	{
		ERR_CODE = eRR_CODE;
	}

	public String getM258AD1()
	{
		return M258AD1;
	}

	public void setM258AD1(String m258ad1)
	{
		M258AD1 = m258ad1;
	}

	public String getM157BIC()
	{
		return M157BIC;
	}

	public void setM157BIC(String m157bic)
	{
		M157BIC = m157bic;
	}

	public String getORGAMT()
	{
		return ORGAMT;
	}

	public void setORGAMT(String oRGAMT)
	{
		ORGAMT = oRGAMT;
	}

	public String getMSGHEADER()
	{
		return MSGHEADER;
	}

	public void setMSGHEADER(String mSGHEADER)
	{
		MSGHEADER = mSGHEADER;
	}

	public String getMSG_HEAD()
	{
		return MSG_HEAD;
	}

	public void setMSG_HEAD(String mSG_HEAD)
	{
		MSG_HEAD = mSG_HEAD;
	}

	public String getM258AD4()
	{
		return M258AD4;
	}

	public void setM258AD4(String m258ad4)
	{
		M258AD4 = m258ad4;
	}

	public String getHOST_TIME()
	{
		return HOST_TIME;
	}

	public void setHOST_TIME(String hOST_TIME)
	{
		HOST_TIME = hOST_TIME;
	}

	public String getM153ACC()
	{
		return M153ACC;
	}

	public void setM153ACC(String m153acc)
	{
		M153ACC = m153acc;
	}

	public String getM258AD3()
	{
		return M258AD3;
	}

	public void setM258AD3(String m258ad3)
	{
		M258AD3 = m258ad3;
	}

	public String getBENTEL()
	{
		return BENTEL;
	}

	public void setBENTEL(String bENTEL)
	{
		BENTEL = bENTEL;
	}

	public String getBENFAX()
	{
		return BENFAX;
	}

	public void setBENFAX(String bENFAX)
	{
		BENFAX = bENFAX;
	}

	public String getLRGOUT_NUM()
	{
		return LRGOUT_NUM;
	}

	public void setLRGOUT_NUM(String lRGOUT_NUM)
	{
		LRGOUT_NUM = lRGOUT_NUM;
	}

	public String getM177NA()
	{
		return M177NA;
	}

	public void setM177NA(String m177na)
	{
		M177NA = m177na;
	}

	public String getM132AMT()
	{
		return M132AMT;
	}

	public void setM132AMT(String m132amt)
	{
		M132AMT = m132amt;
	}

	public String getM155AD2()
	{
		return M155AD2;
	}

	public void setM155AD2(String m155ad2)
	{
		M155AD2 = m155ad2;
	}

	public String getM232DAT()
	{
		return M232DAT;
	}

	public void setM232DAT(String m232dat)
	{
		M232DAT = m232dat;
	}

	public String getM155AD1()
	{
		return M155AD1;
	}

	public void setM155AD1(String m155ad1)
	{
		M155AD1 = m155ad1;
	}

	public String getM150AD4()
	{
		return M150AD4;
	}

	public void setM150AD4(String m150ad4)
	{
		M150AD4 = m150ad4;
	}

	public String getM155AD4()
	{
		return M155AD4;
	}

	public void setM155AD4(String m155ad4)
	{
		M155AD4 = m155ad4;
	}

	public String getCOMMAMT2()
	{
		return COMMAMT2;
	}

	public void setCOMMAMT2(String cOMMAMT2)
	{
		COMMAMT2 = cOMMAMT2;
	}

	public String getM155AD3()
	{
		return M155AD3;
	}

	public void setM155AD3(String m155ad3)
	{
		M155AD3 = m155ad3;
	}

	public String getTC_NO()
	{
		return TC_NO;
	}

	public void setTC_NO(String tC_NO)
	{
		TC_NO = tC_NO;
	}

	public String getM252BIC()
	{
		return M252BIC;
	}

	public void setM252BIC(String m252bic)
	{
		M252BIC = m252bic;
	}

	public String getM150AD2()
	{
		return M150AD2;
	}

	public void setM150AD2(String m150ad2)
	{
		M150AD2 = m150ad2;
	}

	public String getM150AD3()
	{
		return M150AD3;
	}

	public void setM150AD3(String m150ad3)
	{
		M150AD3 = m150ad3;
	}

	public String getM150AD1()
	{
		return M150AD1;
	}

	public void setM150AD1(String m150ad1)
	{
		M150AD1 = m150ad1;
	}

	public String getADTXNO()
	{
		return ADTXNO;
	}

	public void setADTXNO(String aDTXNO)
	{
		ADTXNO = aDTXNO;
	}

	public String getM154BIC()
	{
		return M154BIC;
	}

	public void setM154BIC(String m154bic)
	{
		M154BIC = m154bic;
	}

	public String getM2REFNO()
	{
		return M2REFNO;
	}

	public void setM2REFNO(String m2refno)
	{
		M2REFNO = m2refno;
	}

	public String getM258OPT()
	{
		return M258OPT;
	}

	public void setM258OPT(String m258opt)
	{
		M258OPT = m258opt;
	}

	public String getM150ACC()
	{
		return M150ACC;
	}

	public void setM150ACC(String m150acc)
	{
		M150ACC = m150acc;
	}

	public String getTXBRH()
	{
		return TXBRH;
	}

	public void setTXBRH(String tXBRH)
	{
		TXBRH = tXBRH;
	}

	public String getPMTCCY()
	{
		return PMTCCY;
	}

	public void setPMTCCY(String pMTCCY)
	{
		PMTCCY = pMTCCY;
	}

	public String getLENGTH()
	{
		return LENGTH;
	}

	public void setLENGTH(String lENGTH)
	{
		LENGTH = lENGTH;
	}

	public String getMSGCOD()
	{
		return MSGCOD;
	}

	public void setMSGCOD(String mSGCOD)
	{
		MSGCOD = mSGCOD;
	}

	public String getM157OPT()
	{
		return M157OPT;
	}

	public void setM157OPT(String m157opt)
	{
		M157OPT = m157opt;
	}

	public String getM159BIC()
	{
		return M159BIC;
	}

	public void setM159BIC(String m159bic)
	{
		M159BIC = m159bic;
	}

	public String getCABCHG()
	{
		return CABCHG;
	}

	public void setCABCHG(String cABCHG)
	{
		CABCHG = cABCHG;
	}

	public String getPGMID()
	{
		return PGMID;
	}

	public void setPGMID(String pGMID)
	{
		PGMID = pGMID;
	}

	public String getM155ACC()
	{
		return M155ACC;
	}

	public void setM155ACC(String m155acc)
	{
		M155ACC = m155acc;
	}

	public String getTX_SEQ()
	{
		return TX_SEQ;
	}

	public void setTX_SEQ(String tX_SEQ)
	{
		TX_SEQ = tX_SEQ;
	}

	public String getM258BIC()
	{
		return M258BIC;
	}

	public void setM258BIC(String m258bic)
	{
		M258BIC = m258bic;
	}

	public String getSTAN()
	{
		return STAN;
	}

	public void setSTAN(String sTAN)
	{
		STAN = sTAN;
	}

	public String getM157AD3()
	{
		return M157AD3;
	}

	public void setM157AD3(String m157ad3)
	{
		M157AD3 = m157ad3;
	}

	public String getM157AD4()
	{
		return M157AD4;
	}

	public void setM157AD4(String m157ad4)
	{
		M157AD4 = m157ad4;
	}

	public String getM157AD1()
	{
		return M157AD1;
	}

	public void setM157AD1(String m157ad1)
	{
		M157AD1 = m157ad1;
	}

	public String getM157AD2()
	{
		return M157AD2;
	}

	public void setM157AD2(String m157ad2)
	{
		M157AD2 = m157ad2;
	}

	public String getTERM_TIME()
	{
		return TERM_TIME;
	}

	public void setTERM_TIME(String tERM_TIME)
	{
		TERM_TIME = tERM_TIME;
	}

	public String getCOMMAMT()
	{
		return COMMAMT;
	}

	public void setCOMMAMT(String cOMMAMT)
	{
		COMMAMT = cOMMAMT;
	}

	public String getM154OPT()
	{
		return M154OPT;
	}

	public void setM154OPT(String m154opt)
	{
		M154OPT = m154opt;
	}

	public String getM272NA4()
	{
		return M272NA4;
	}

	public void setM272NA4(String m272na4)
	{
		M272NA4 = m272na4;
	}

	public String getM272NA3()
	{
		return M272NA3;
	}

	public void setM272NA3(String m272na3)
	{
		M272NA3 = m272na3;
	}

	public String getM272NA6()
	{
		return M272NA6;
	}

	public void setM272NA6(String m272na6)
	{
		M272NA6 = m272na6;
	}

	public String getM272NA5()
	{
		return M272NA5;
	}

	public void setM272NA5(String m272na5)
	{
		M272NA5 = m272na5;
	}

	public String getOURCHG()
	{
		return OURCHG;
	}

	public void setOURCHG(String oURCHG)
	{
		OURCHG = oURCHG;
	}

	public String getM156BIC()
	{
		return M156BIC;
	}

	public void setM156BIC(String m156bic)
	{
		M156BIC = m156bic;
	}

	public String getM132DAT()
	{
		return M132DAT;
	}

	public void setM132DAT(String m132dat)
	{
		M132DAT = m132dat;
	}

	public String getM1SWIFT()
	{
		return M1SWIFT;
	}

	public void setM1SWIFT(String m1swift)
	{
		M1SWIFT = m1swift;
	}

	public String get__OCCURS()
	{
		return __OCCURS;
	}

	public void set__OCCURS(String __OCCURS)
	{
		this.__OCCURS = __OCCURS;
	}

	public String getTX_ID()
	{
		return TX_ID;
	}

	public void setTX_ID(String tX_ID)
	{
		TX_ID = tX_ID;
	}

	public String getM133AMT()
	{
		return M133AMT;
	}

	public void setM133AMT(String m133amt)
	{
		M133AMT = m133amt;
	}

	public String getTL_ID()
	{
		return TL_ID;
	}

	public void setTL_ID(String tL_ID)
	{
		TL_ID = tL_ID;
	}

	public String getM150BIC()
	{
		return M150BIC;
	}

	public void setM150BIC(String m150bic)
	{
		M150BIC = m150bic;
	}

	public String getM1OPCOD()
	{
		return M1OPCOD;
	}

	public void setM1OPCOD(String m1opcod)
	{
		M1OPCOD = m1opcod;
	}

	public String getM159OPT()
	{
		return M159OPT;
	}

	public void setM159OPT(String m159opt)
	{
		M159OPT = m159opt;
	}

	public String getCHGROE()
	{
		return CHGROE;
	}

	public void setCHGROE(String cHGROE)
	{
		CHGROE = cHGROE;
	}

	public String getM2SBBIC()
	{
		return M2SBBIC;
	}

	public void setM2SBBIC(String m2sbbic)
	{
		M2SBBIC = m2sbbic;
	}

	public String getWK_ID()
	{
		return WK_ID;
	}

	public void setWK_ID(String wK_ID)
	{
		WK_ID = wK_ID;
	}

	public String getM154AD1()
	{
		return M154AD1;
	}

	public void setM154AD1(String m154ad1)
	{
		M154AD1 = m154ad1;
	}

	public String getM257ACC()
	{
		return M257ACC;
	}

	public void setM257ACC(String m257acc)
	{
		M257ACC = m257acc;
	}

	public String getM154AD2()
	{
		return M154AD2;
	}

	public void setM154AD2(String m154ad2)
	{
		M154AD2 = m154ad2;
	}

	public String getM154AD3()
	{
		return M154AD3;
	}

	public void setM154AD3(String m154ad3)
	{
		M154AD3 = m154ad3;
	}

	public String getM154AD4()
	{
		return M154AD4;
	}

	public void setM154AD4(String m154ad4)
	{
		M154AD4 = m154ad4;
	}

	public String getSTATUS()
	{
		return STATUS;
	}

	public void setSTATUS(String sTATUS)
	{
		STATUS = sTATUS;
	}

	public String getM170NA3()
	{
		return M170NA3;
	}

	public void setM170NA3(String m170na3)
	{
		M170NA3 = m170na3;
	}

	public String getM157ACC()
	{
		return M157ACC;
	}

	public void setM157ACC(String m157acc)
	{
		M157ACC = m157acc;
	}

	public String getM170NA2()
	{
		return M170NA2;
	}

	public void setM170NA2(String m170na2)
	{
		M170NA2 = m170na2;
	}

	public String getM153BIC()
	{
		return M153BIC;
	}

	public void setM153BIC(String m153bic)
	{
		M153BIC = m153bic;
	}

	public String getM170NA1()
	{
		return M170NA1;
	}

	public void setM170NA1(String m170na1)
	{
		M170NA1 = m170na1;
	}

	public String getM170NA4()
	{
		return M170NA4;
	}

	public void setM170NA4(String m170na4)
	{
		M170NA4 = m170na4;
	}

	public String getM171CHG()
	{
		return M171CHG;
	}

	public void setM171CHG(String m171chg)
	{
		M171CHG = m171chg;
	}

	public String getCURAMT()
	{
		return CURAMT;
	}

	public void setCURAMT(String cURAMT)
	{
		CURAMT = cURAMT;
	}

	public String getM123TEL()
	{
		return M123TEL;
	}

	public void setM123TEL(String m123tel)
	{
		M123TEL = m123tel;
	}

	public String getTDBAMT()
	{
		return TDBAMT;
	}

	public void setTDBAMT(String tDBAMT)
	{
		TDBAMT = tDBAMT;
	}

	public String getM1SBBIC()
	{
		return M1SBBIC;
	}

	public void setM1SBBIC(String m1sbbic)
	{
		M1SBBIC = m1sbbic;
	}

	public String getM159AD2()
	{
		return M159AD2;
	}

	public void setM159AD2(String m159ad2)
	{
		M159AD2 = m159ad2;
	}

	public String getM159AD1()
	{
		return M159AD1;
	}

	public void setM159AD1(String m159ad1)
	{
		M159AD1 = m159ad1;
	}

	public String getM159AD4()
	{
		return M159AD4;
	}

	public void setM159AD4(String m159ad4)
	{
		M159AD4 = m159ad4;
	}

	public String getM252ACC()
	{
		return M252ACC;
	}

	public void setM252ACC(String m252acc)
	{
		M252ACC = m252acc;
	}

	public String getM159AD3()
	{
		return M159AD3;
	}

	public void setM159AD3(String m159ad3)
	{
		M159AD3 = m159ad3;
	}

	public String getM232CCY()
	{
		return M232CCY;
	}

	public void setM232CCY(String m232ccy)
	{
		M232CCY = m232ccy;
	}

	public String getEXRATE()
	{
		return EXRATE;
	}

	public void setEXRATE(String eXRATE)
	{
		EXRATE = eXRATE;
	}

	public String getM156OPT()
	{
		return M156OPT;
	}

	public void setM156OPT(String m156opt)
	{
		M156OPT = m156opt;
	}

	public String getWS_ID()
	{
		return WS_ID;
	}

	public void setWS_ID(String wS_ID)
	{
		WS_ID = wS_ID;
	}

	public String getM257AD1()
	{
		return M257AD1;
	}

	public void setM257AD1(String m257ad1)
	{
		M257AD1 = m257ad1;
	}

	public String getPMTAMT()
	{
		return PMTAMT;
	}

	public void setPMTAMT(String pMTAMT)
	{
		PMTAMT = pMTAMT;
	}

	public String getAVAAMT()
	{
		return AVAAMT;
	}

	public void setAVAAMT(String aVAAMT)
	{
		AVAAMT = aVAAMT;
	}

	public String getETY_LVL()
	{
		return ETY_LVL;
	}

	public void setETY_LVL(String eTY_LVL)
	{
		ETY_LVL = eTY_LVL;
	}

	public String getPCODE()
	{
		return PCODE;
	}

	public void setPCODE(String pCODE)
	{
		PCODE = pCODE;
	}

	public String getM257AD4()
	{
		return M257AD4;
	}

	public void setM257AD4(String m257ad4)
	{
		M257AD4 = m257ad4;
	}

	public String getM257AD2()
	{
		return M257AD2;
	}

	public void setM257AD2(String m257ad2)
	{
		M257AD2 = m257ad2;
	}

	public String getM154ACC()
	{
		return M154ACC;
	}

	public void setM154ACC(String m154acc)
	{
		M154ACC = m154acc;
	}

	public String getM257AD3()
	{
		return M257AD3;
	}

	public void setM257AD3(String m257ad3)
	{
		M257AD3 = m257ad3;
	}

	public String getM2SWIFT()
	{
		return M2SWIFT;
	}

	public void setM2SWIFT(String m2swift)
	{
		M2SWIFT = m2swift;
	}

	public String getORGCCY()
	{
		return ORGCCY;
	}

	public void setORGCCY(String oRGCCY)
	{
		ORGCCY = oRGCCY;
	}

	public String getSTATUS1()
	{
		return STATUS1;
	}

	public void setSTATUS1(String sTATUS1)
	{
		STATUS1 = sTATUS1;
	}

	public String getM156AD1()
	{
		return M156AD1;
	}

	public void setM156AD1(String m156ad1)
	{
		M156AD1 = m156ad1;
	}

	public String getBENEML()
	{
		return BENEML;
	}

	public void setBENEML(String bENEML)
	{
		BENEML = bENEML;
	}

	public String getM156AD3()
	{
		return M156AD3;
	}

	public void setM156AD3(String m156ad3)
	{
		M156AD3 = m156ad3;
	}

	public String getM150OPT()
	{
		return M150OPT;
	}

	public void setM150OPT(String m150opt)
	{
		M150OPT = m150opt;
	}

	public String getM156AD2()
	{
		return M156AD2;
	}

	public void setM156AD2(String m156ad2)
	{
		M156AD2 = m156ad2;
	}

	public String getM156AD4()
	{
		return M156AD4;
	}

	public void setM156AD4(String m156ad4)
	{
		M156AD4 = m156ad4;
	}

	public String getM153OPT()
	{
		return M153OPT;
	}

	public void setM153OPT(String m153opt)
	{
		M153OPT = m153opt;
	}

	public String getORGCOMM()
	{
		return ORGCOMM;
	}

	public void setORGCOMM(String oRGCOMM)
	{
		ORGCOMM = oRGCOMM;
	}

	public String getM221INF()
	{
		return M221INF;
	}

	public void setM221INF(String m221inf)
	{
		M221INF = m221inf;
	}

	public String getBRH_CODE()
	{
		return BRH_CODE;
	}

	public void setBRH_CODE(String bRH_CODE)
	{
		BRH_CODE = bRH_CODE;
	}

	public String getREFNO()
	{
		return REFNO;
	}

	public void setREFNO(String rEFNO)
	{
		REFNO = rEFNO;
	}

	public String getFORMAT_NAME()
	{
		return FORMAT_NAME;
	}

	public void setFORMAT_NAME(String fORMAT_NAME)
	{
		FORMAT_NAME = fORMAT_NAME;
	}

	public String getTX_CODE()
	{
		return TX_CODE;
	}

	public void setTX_CODE(String tX_CODE)
	{
		TX_CODE = tX_CODE;
	}

	public String getM172NA6()
	{
		return M172NA6;
	}

	public void setM172NA6(String m172na6)
	{
		M172NA6 = m172na6;
	}

	public String getM252AD1()
	{
		return M252AD1;
	}

	public void setM252AD1(String m252ad1)
	{
		M252AD1 = m252ad1;
	}

	public String getM252AD2()
	{
		return M252AD2;
	}

	public void setM252AD2(String m252ad2)
	{
		M252AD2 = m252ad2;
	}

	public String getSWTKIND()
	{
		return SWTKIND;
	}

	public void setSWTKIND(String sWTKIND)
	{
		SWTKIND = sWTKIND;
	}

	public String getCOMMCCY2()
	{
		return COMMCCY2;
	}

	public void setCOMMCCY2(String cOMMCCY2)
	{
		COMMCCY2 = cOMMCCY2;
	}

	public String getM155BIC()
	{
		return M155BIC;
	}

	public void setM155BIC(String m155bic)
	{
		M155BIC = m155bic;
	}

	public String getM252AD3()
	{
		return M252AD3;
	}

	public void setM252AD3(String m252ad3)
	{
		M252AD3 = m252ad3;
	}

	public String getM159ACC()
	{
		return M159ACC;
	}

	public void setM159ACC(String m159acc)
	{
		M159ACC = m159acc;
	}

	public String getM257OPT()
	{
		return M257OPT;
	}

	public void setM257OPT(String m257opt)
	{
		M257OPT = m257opt;
	}

	public String getM252AD4()
	{
		return M252AD4;
	}

	public void setM252AD4(String m252ad4)
	{
		M252AD4 = m252ad4;
	}

	public String getM172NA2()
	{
		return M172NA2;
	}

	public void setM172NA2(String m172na2)
	{
		M172NA2 = m172na2;
	}

	public String getM172NA3()
	{
		return M172NA3;
	}

	public void setM172NA3(String m172na3)
	{
		M172NA3 = m172na3;
	}

	public String getM172NA4()
	{
		return M172NA4;
	}

	public void setM172NA4(String m172na4)
	{
		M172NA4 = m172na4;
	}

	public String getCHGROERL()
	{
		return CHGROERL;
	}

	public void setCHGROERL(String cHGROERL)
	{
		CHGROERL = cHGROERL;
	}

	public String getM172NA5()
	{
		return M172NA5;
	}

	public void setM172NA5(String m172na5)
	{
		M172NA5 = m172na5;
	}

	public String getM172NA1()
	{
		return M172NA1;
	}

	public void setM172NA1(String m172na1)
	{
		M172NA1 = m172na1;
	}

	public String getDATA_LL()
	{
		return DATA_LL;
	}

	public void setDATA_LL(String dATA_LL)
	{
		DATA_LL = dATA_LL;
	}

	public String getHIGH_IND()
	{
		return HIGH_IND;
	}

	public void setHIGH_IND(String hIGH_IND)
	{
		HIGH_IND = hIGH_IND;
	}

	public String getM252OPT()
	{
		return M252OPT;
	}

	public void setM252OPT(String m252opt)
	{
		M252OPT = m252opt;
	}

	public String getMSG_CODE()
	{
		return MSG_CODE;
	}

	public void setMSG_CODE(String mSG_CODE)
	{
		MSG_CODE = mSG_CODE;
	}

	public String getFILLER()
	{
		return FILLER;
	}

	public void setFILLER(String fILLER)
	{
		FILLER = fILLER;
	}

	public String getORGCABLE()
	{
		return ORGCABLE;
	}

	public void setORGCABLE(String oRGCABLE)
	{
		ORGCABLE = oRGCABLE;
	}

	public String getM153AD1()
	{
		return M153AD1;
	}

	public void setM153AD1(String m153ad1)
	{
		M153AD1 = m153ad1;
	}

	public String getM153AD2()
	{
		return M153AD2;
	}

	public void setM153AD2(String m153ad2)
	{
		M153AD2 = m153ad2;
	}

	public String getM153AD3()
	{
		return M153AD3;
	}

	public void setM153AD3(String m153ad3)
	{
		M153AD3 = m153ad3;
	}

	public String getM258ACC()
	{
		return M258ACC;
	}

	public void setM258ACC(String m258acc)
	{
		M258ACC = m258acc;
	}

	public String getM153AD4()
	{
		return M153AD4;
	}

	public void setM153AD4(String m153ad4)
	{
		M153AD4 = m153ad4;
	}

	public String getM1REFNO()
	{
		return M1REFNO;
	}

	public void setM1REFNO(String m1refno)
	{
		M1REFNO = m1refno;
	}

	public String getSEQNO()
	{
		return SEQNO;
	}

	public void setSEQNO(String sEQNO)
	{
		SEQNO = sEQNO;
	}

	public String getM132CCY()
	{
		return M132CCY;
	}

	public void setM132CCY(String m132ccy)
	{
		M132CCY = m132ccy;
	}

	public String getINDICTOR()
	{
		return INDICTOR;
	}

	public void setINDICTOR(String iNDICTOR)
	{
		INDICTOR = iNDICTOR;
	}

	public String getBRH_COD()
	{
		return BRH_COD;
	}

	public void setBRH_COD(String bRH_COD)
	{
		BRH_COD = bRH_COD;
	}

	public String getPF_KEY()
	{
		return PF_KEY;
	}

	public void setPF_KEY(String pF_KEY)
	{
		PF_KEY = pF_KEY;
	}

	public String getM232AMT()
	{
		return M232AMT;
	}

	public void setM232AMT(String m232amt)
	{
		M232AMT = m232amt;
	}

	public String getM156ACC()
	{
		return M156ACC;
	}

	public void setM156ACC(String m156acc)
	{
		M156ACC = m156acc;
	}
	
	
	
}
