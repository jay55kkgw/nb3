package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class NA72_REST_RS extends BaseRestBean implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8121527157083713773L;
	
	private String OFFSET;
	private String SYNC;
	private String RSPCOD;
	
	
	public String getOFFSET() {
		return OFFSET;
	}
	public void setOFFSET(String oFFSET) {
		OFFSET = oFFSET;
	}
	public String getSYNC() {
		return SYNC;
	}
	public void setSYNC(String sYNC) {
		SYNC = sYNC;
	}
	public String getRSPCOD() {
		return RSPCOD;
	}
	public void setRSPCOD(String rSPCOD) {
		RSPCOD = rSPCOD;
	}
}
