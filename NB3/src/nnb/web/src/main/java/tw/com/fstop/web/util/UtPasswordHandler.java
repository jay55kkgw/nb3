package tw.com.fstop.web.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.security.auth.callback.UnsupportedCallbackException;
import javax.servlet.http.HttpServletRequest;

import org.apache.wss4j.common.ext.WSPasswordCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.ws.soap.security.wss4j2.Wss4jSecuritySecurementException;
import org.springframework.ws.soap.security.wss4j2.callback.AbstractWsPasswordCallbackHandler;
import org.springframework.ws.transport.context.TransportContext;
import org.springframework.ws.transport.context.TransportContextHolder;
import org.springframework.ws.transport.http.HttpServletConnection;

import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

//@Component
public class UtPasswordHandler extends  AbstractWsPasswordCallbackHandler
	implements InitializingBean {
	static Logger log = LoggerFactory.getLogger(UtPasswordHandler.class);
	private Map<String, String> users = new HashMap<String, String>();
	private Map<String, String> gold_arg ;

	/** Sets the users to validate against. Property names are usernames, property values are passwords. */
	public void setUsers(Properties users) {
		for (Map.Entry<Object, Object> entry : users.entrySet()) {
			if (entry.getKey() instanceof String && entry.getValue() instanceof String) {
				this.users.put((String) entry.getKey(), (String) entry.getValue());
			}
		}
		log.debug("setUsers.users>>{}", users);
	}

	public void setUsersMap(Map<String, String> users) {
		this.users = users;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(users, "users is required");
	}
	
	@Override
	public void handleUsernameToken(WSPasswordCallback callback) throws IOException, UnsupportedCallbackException
	{
		byte [] sws_array = null;
		Map<String, String> gold_arg =null;
		
		if(!VerifyIP()) {
			throw new Wss4jSecuritySecurementException(" illegal ip");
		}
		gold_arg = SpringBeanFactory.getBean("gold_arg");
		log.debug("gold_arg>>{}",gold_arg);
		users.putAll(gold_arg);
		log.debug("users>>{}",users);
		
		
		String username = callback.getIdentifier();
		log.debug("username>>{}",username);
		String sws = users.get(username);
		log.debug("sws>>{}",sws);
		sws_array = Base64.getDecoder().decode(sws);
//		sws = Base64.getEncoder().encodeToString(sws_array);
		sws =  new String(sws_array,"utf-8");
		log.debug("sws2>>{}",sws);
		callback.setPassword(sws);
	}

	/**
	 * 取得 Http request
	 * @return
	 */
	protected HttpServletRequest getHttpServletRequest() {
	    TransportContext ctx = TransportContextHolder.getTransportContext();
	    return ( null != ctx ) ? ((HttpServletConnection ) ctx.getConnection()).getHttpServletRequest() : null;
	}
	
	/**
	 * 取得IP
	 * @return
	 */
	protected String getUserIP() {
		String ip = null;
		String errMsg = "gold web service can't get remote address";
		
		try {
			HttpServletRequest request = getHttpServletRequest();
			ip = WebUtil.getIpAddr(request);
			
			if(StrUtil.isEmpty(ip)) {
				log.error(errMsg);
				throw new Wss4jSecuritySecurementException(errMsg);
			}
		} catch (Exception e) {
			log.error(e.toString());
			throw new Wss4jSecuritySecurementException(errMsg);
		}
		return ip;
	}
	

	/**
	 * 白名單驗證
	 * @return
	 */
	protected Boolean VerifyIP() {
		String ip = null;
		String set_ip = null;
		List<String> set_ip_List = null; 
		String errMsg = "gold web service can't get remote address";
		HttpServletRequest request = null;
		Boolean result = Boolean.FALSE;
		String isverifyip = "Y";
		try {
			isverifyip = SpringBeanFactory.getBean("isverifyip");
			if("N".equals(isverifyip)) {
				result=Boolean.TRUE;
				return result;
			}
			
			request = getHttpServletRequest();
			ip = WebUtil.getIpAddr(request);
			set_ip = SpringBeanFactory.getBean("gold_pass_ip") ;
			if(StrUtil.isEmpty(ip)) {
				log.error(errMsg);
				throw new Wss4jSecuritySecurementException(errMsg);
			}
			if(StrUtil.isEmpty(set_ip)) {
				errMsg = "gold ip not found";
				log.error(errMsg);
				throw new Wss4jSecuritySecurementException(errMsg);
			}
			set_ip_List = Arrays.asList(set_ip.split(",")) ;
			
			if(set_ip_List.contains(ip)) {
				result = Boolean.TRUE;
				return result;
			}
//			if(set_ip.equalsIgnoreCase(ip)) {
//				result = Boolean.TRUE;
//				return result;
//			}
			log.warn(ESAPIUtil.vaildLog("ip area not mapping , ip >> " + ip + ",set_ip>>" +set_ip )); 
			//網段判斷
			ip=ip.substring(0,ip.lastIndexOf("."));  //先抓取ip網段
//			set_ip=set_ip.substring(0,set_ip.lastIndexOf("."));  
			
			if(set_ip_List.contains(ip)) {
				result = Boolean.TRUE;
				return result;
			}
//			if(set_ip.equalsIgnoreCase(ip)) {
//				result = Boolean.TRUE;
//				return result;
//			}
			log.warn(ESAPIUtil.vaildLog("ip area not mapping , ip >> " + ip + ",set_ip>>" +set_ip )); 
			
		} catch (Exception e) {
			log.error(e.toString());
			throw new Wss4jSecuritySecurementException(errMsg);
		}
		return result;
	}
	
	
	
}
