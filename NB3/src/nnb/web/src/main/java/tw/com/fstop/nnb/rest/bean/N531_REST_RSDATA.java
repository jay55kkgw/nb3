package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N531_REST_RSDATA extends BaseRestBean implements Serializable
{

	private static final long serialVersionUID = 1584584241427000526L;

	private String CUID;//幣別名稱

	private String EVTMARK;//存單狀態

	private String DPISDT;// 起存日

	private String DUEDAT;// 到期日

	private String INTMTH;// 計息方式

	private String ITR;// 利率

	private String FDPNO;//存單號碼

	private String BALANCE;//存單金額	

	private String ILAZLFTM;//已轉期數

	private String AUTXFTM;//未轉期數

	private String ACN;//帳號

	public String getCUID()
	{
		return CUID;
	}

	public void setCUID(String cUID)
	{
		CUID = cUID;
	}

	public String getEVTMARK()
	{
		return EVTMARK;
	}

	public void setEVTMARK(String eVTMARK)
	{
		EVTMARK = eVTMARK;
	}

	public String getDUEDAT()
	{
		return DUEDAT;
	}

	public void setDUEDAT(String dUEDAT)
	{
		DUEDAT = dUEDAT;
	}

	public String getDPISDT()
	{
		return DPISDT;
	}

	public void setDPISDT(String dPISDT)
	{
		DPISDT = dPISDT;
	}

	public String getINTMTH()
	{
		return INTMTH;
	}

	public void setINTMTH(String iNTMTH)
	{
		INTMTH = iNTMTH;
	}

	public String getITR()
	{
		return ITR;
	}

	public void setITR(String iTR)
	{
		ITR = iTR;
	}

	public String getFDPNO()
	{
		return FDPNO;
	}

	public void setFDPNO(String fDPNO)
	{
		FDPNO = fDPNO;
	}

	public String getBALANCE()
	{
		return BALANCE;
	}

	public void setBALANCE(String bALANCE)
	{
		BALANCE = bALANCE;
	}

	public String getILAZLFTM()
	{
		return ILAZLFTM;
	}

	public void setILAZLFTM(String iLAZLFTM)
	{
		ILAZLFTM = iLAZLFTM;
	}

	public String getAUTXFTM()
	{
		return AUTXFTM;
	}

	public void setAUTXFTM(String aUTXFTM)
	{
		AUTXFTM = aUTXFTM;
	}

	public String getACN()
	{
		return ACN;
	}

	public void setACN(String aCN)
	{
		ACN = aCN;
	}
}
