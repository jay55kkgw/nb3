package tw.com.fstop.nnb.spring.controller;

import java.net.URLEncoder;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.idgate.bean.N570_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N570_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N100_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N100_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N830_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N830_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N831_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N831_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N900_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N900_IDGATE_DATA_VIEW;
import tw.com.fstop.nnb.service.Acct_Transfer_Service;
import tw.com.fstop.nnb.service.Change_Data_Service;
import tw.com.fstop.nnb.service.IdGateService;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({ 
	SessionUtil.PD,
	SessionUtil.CUSIDN,
	SessionUtil.DOWNLOAD_DATALISTMAP_DATA,
	SessionUtil.PRINT_DATALISTMAP_DATA, 
	SessionUtil.CONFIRM_LOCALE_DATA, 
	SessionUtil.RESULT_LOCALE_DATA,
	SessionUtil.TRANSFER_RESULT_TOKEN,
	SessionUtil.TRANSFER_CONFIRM_TOKEN,
	SessionUtil.MBSTAT,
	SessionUtil.MBOPNDT,
	SessionUtil.MBOPNTM,
	SessionUtil.BACK_DATA,
	SessionUtil.DPMYEMAIL,
	SessionUtil.TRANSFER_DATA,
	SessionUtil.IDGATE_TRANSDATA,
	SessionUtil.STEP1_LOCALE_DATA
})
@Controller
@RequestMapping(value = "/CHANGE/DATA")
public class Change_Data_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());


	@Autowired
	private Change_Data_Service change_data_service;

	@Autowired		 
	IdGateService idgateservice;
	
	@Autowired
	private Acct_Transfer_Service acct_transfer_service;
	
	//N100變更通訊地址/電話 選項頁
	@RequestMapping(value = "/communication_data")
		public String communication_data(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("communication_data>>>>");
		BaseResult bs = null;
		String target = "/change_data/communication_data";
		
		try {
			bs = new BaseResult();
		
			//清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");		
			
			// IKEY要使用的JSON:DC
			String jsondc = null;
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("communication_data_plural.jsondc >> " + jsondc));
			
			bs = change_data_service.getTxToken();
			bs.addData("jsondc", jsondc);
			log.trace("change_data_service.getTxToken: {}" , bs.getResult());
			log.trace("change_data_service.TXTOKEN: {}" , ((Map<String, String>) bs.getData()).get("TXTOKEN"));
			if (bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN,
						((Map<String, String>) bs.getData()).get("TXTOKEN"));
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("communication_data error >> {}",e);
		}finally {
			model.addAttribute("communication_data", bs);			
		}
				
		return target;
	}
	
	// N100變更通訊地址/電話_資料頁
	@RequestMapping(value = "/communication_data_confirm")
	public String communication_data_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace(ESAPIUtil.vaildLog("communication_data_confirm_Start >> " + CodeUtil.toJson(reqParam)));
		String target = "/error";
		BaseResult bs = null;
		BaseResult bs900 = null;
		try {
			bs = new BaseResult();
			bs900 = new BaseResult();
			// Get session value by session Key
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
// 			解決Trust Boundary Violation
			Map<String, String> okMap = reqParam;
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
				if(hasLocale) 
				{
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
					if( ! bs.getResult())
					{
						log.trace("bs.getResult() >>{}",bs.getResult());
						throw new Exception();
					}
				}
				if( ! hasLocale){
					// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
					String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
					log.trace("communication_data_confirm.token: {}", token);
					if (StrUtil.isNotEmpty(okMap.get("TOKEN")) && okMap.get("TOKEN").equals(token)) {
						bs.setResult(Boolean.TRUE);
					}
					log.trace("bs.getResult(): {}", bs.getResult());
					if (!bs.getResult()) {
						log.trace("throw new Exception()");
						throw new Exception();
					}
					bs.reset();
					bs = change_data_service.communication_data_confirm(cusidn,okMap);
					
					try {
						bs900 = change_data_service.credit_data_detail(cusidn, okMap);
						if(bs900 != null && bs900.getResult()) {
							Map<String, Object> bsData = (Map)bs.getData();
							int count = Integer.parseInt(bsData.get("COUNT").toString());
							count = count + 1;
							bs.addData("COUNT", count);
							bs.addData("show900", "Y");
							model.addAttribute("communication_data900", bs900);
							SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, bs900);
						}
					}
					catch (Exception e) {
						log.error("C900 ERROR : " + e);
					}
					bs.addData("DATAJSON",CodeUtil.toJson(bs));
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
					SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				}
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("communication_data_confirm error >> {}",e);
			}finally {
				if(bs!=null && bs.getResult()) {
					target = "/change_data/communication_data_confirm";
					model.addAttribute("communication_data", bs);
				}else {
					bs.setPrevious("/CHANGE/DATA/communication_data");
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}		
			return target;
	}
	
	// N100變更通訊地址/電話_plural
		@RequestMapping(value = "/communication_data_plural")
		public String communication_data_plural(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			log.trace(ESAPIUtil.vaildLog("communication_data_plural_Start >> " + CodeUtil.toJson(reqParam)));
			String target = "/error";
			BaseResult bs = null;
			BaseResult bs900 = null;
			try {
				bs = new BaseResult();
				bs900 = new BaseResult();
				// Get session value by session Key
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
//	 			解決Trust Boundary Violation
				Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
				
				if("Y".equals(okMap.get("back"))) {
					bs.setResult(true);
					model.addAttribute("bk_key","Y");
				}
				
//				判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
					if(hasLocale) 
					{
						bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
						if( ! bs.getResult())
						{
							log.trace("bs.getResult() >>{}",bs.getResult());
							throw new Exception();
						}
					}
					if( ! hasLocale){
						bs.reset();
						bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
						
						bs900 = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null);
						if(bs900 != null && bs900.getResult()) {
							model.addAttribute("communication_data900", bs900);
							bs.addData("show900", "Y");
						}
						
						SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
					}
					//IDGATE身分
			        String idgateUserFlag="N";		 
			        Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			        try {		 
			       	   if(IdgateData==null) {
			       		   IdgateData = new HashMap<String, Object>();
			       	   }
			            BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
			            if(tmp.getResult()) {		 
			                idgateUserFlag="Y";                  		 
			            }		 
			            tmp.addData("idgateUserFlag",idgateUserFlag);		 
			            IdgateData.putAll((Map<String, String>) tmp.getData());
			            SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
			        }catch(Exception e) {		 
			            log.debug("idgateUserFlag error {}",e);		 
			        }		 
			        model.addAttribute("idgateUserFlag",idgateUserFlag);
				} catch (Exception e) {
					//Avoid Information Exposure Through an Error Message
					//e.printStackTrace();
					log.error("communication_data_plural error >> {}",e);
				}finally {
					if(bs!=null && bs.getResult()) {
						target = "/change_data/communication_data_plural";
						model.addAttribute("communication_data_plural", bs);
					}else {
						model.addAttribute(BaseResult.ERROR, bs);
					}
				}		
				return target;
		}
		
		// N100變更通訊地址/電話_plural_確認
		@RequestMapping(value = "/communication_data_plural_c")
		public String communication_data_plural_c(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			log.trace(ESAPIUtil.vaildLog("communication_data_plural_c_Start >> " + CodeUtil.toJson(reqParam)));
			String target = "/error";
			BaseResult bs = null;
			BaseResult bs900 = null;
			
//	 		解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			// 回上一頁重新賦值
			if(okMap.containsKey("previous")) 
			{
				Map m = (Map) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				okMap = m;
			}
			
			try {
				bs = new BaseResult();
				bs900 = new BaseResult();
				// Get session value by session Key
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
				// IKEY要使用的JSON:DC
				String jsondc = null;
				jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
				log.trace(ESAPIUtil.vaildLog("communication_data_plural.jsondc >> " + jsondc));
//				判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
					if(hasLocale) 
					{
						bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
						if( ! bs.getResult())
						{
							log.trace("bs.getResult() >>{}",bs.getResult());
							throw new Exception();
						}else {
							// session 資料放入 map 讓後續 model 和回上一頁取值
							okMap.putAll((Map<String, String>) bs.getData());
						}
					}
					if( ! hasLocale){
						bs.reset();
						//保留之前的值
						bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
						Map sessionRec = (Map<String,String>)bs.getData();
						bs.reset();
						//新值
						String addr = okMap.get("CTTADR");
						addr = acct_transfer_service.halfToFull(addr);
						okMap.put("CTTADR", addr);
						bs = change_data_service.communication_data_plural_c(cusidn,okMap);	
						
						bs900 = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null);
						if(bs900 != null && bs900.getResult()) {
							model.addAttribute("communication_data900", bs900);
							bs.addData("show900", "Y");
						}
						
						bs.addData("sessionRec",sessionRec);
						bs.addData("jsondc", jsondc);
						SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
					}
					// IDGATE transdata
					log.trace("IDGATE TRNDATA >>"+CodeUtil.toJson(okMap));
		            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
		    		String adopid = "N100";
		    		String title = "您有一筆，變更通訊地址／電話交易待確認";
		    		Map<String, String> result = new HashMap<String, String>();
		    		result.putAll(okMap);
		    		if(!result.get("TELEXT").equals(""))result.put("TELNUM2",result.get("TELNUM2") + "#" + result.get("TELEXT"));
		        	if(IdgateData != null) {
			            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
			            if(IdgateData.get("idgateUserFlag").equals("Y")) {
			            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
			            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
							IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N100_IDGATE_DATA.class, N100_IDGATE_DATA_VIEW.class, result));
							log.trace("IDGATE TRNDATA2 >>"+CodeUtil.toJson(IdgateData));
							SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
			            }
		        	}
		            model.addAttribute("idgateAdopid", adopid);
		            model.addAttribute("idgateTitle", title);
				} catch (Exception e) {
					//Avoid Information Exposure Through an Error Message
					//e.printStackTrace();
					log.error("communication_data_plural_c error >> {}",e);
				}finally {
					if(bs!=null && bs.getResult()) {
						target = "/change_data/communication_data_plural_c";
						model.addAttribute("communication_data_plural", bs);
						//設定回上一頁的url,配合confirm頁面的js
						model.addAttribute("previous","/CHANGE/DATA/communication_data_plural");
						//回填使用DATA
						SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
					}else {
						model.addAttribute(BaseResult.ERROR, bs);
					}
				}		
				return target;
		}
		
		// N100變更通訊地址/電話_plural_結果
		@RequestMapping(value = "/communication_data_plural_r")
		public String communication_data_plural_r(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			log.trace(ESAPIUtil.vaildLog("communication_data_plural_r_Start >> " + CodeUtil.toJson(reqParam)));
			String target = "/error";
			BaseResult bs = null;
			BaseResult bs900 = null;
			try {
				bs = new BaseResult();
				bs900 = new BaseResult();
				// Get session value by session Key
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
//	 			解決Trust Boundary Violation
				Map<String, String> okMap = reqParam;
//				判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
					if(hasLocale) 
					{
						bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
						if( ! bs.getResult())
						{
							log.trace("bs.getResult() >>{}",bs.getResult());
							throw new Exception();
						}
					}
					if( ! hasLocale){
						bs.reset();
						
						//20210201 同步NB2.5 MS層 N100.java 需打C113 , 要有 IP,EMAIL 固新增
						okMap.put("IP", WebUtil.getIpAddr(request));
						okMap.put("EMAIL", (String) SessionUtil.getAttribute(model, SessionUtil.DPMYEMAIL, null));
		                //IDgate驗證		 
		                try {		 
		                    if(okMap.get("FGTXWAY").equals("7")) {		 

		        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
		        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N100_IDGATE");
		                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
		                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
		                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
		                    }		 
		                }catch(Exception e) {		 
		                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
		                }
						
						bs = change_data_service.communication_data_plural_r(cusidn,okMap);
						
						// 2021/10/13調整，若N100有成功才做N900，N900成功&失敗都正常顯示結果頁，但失敗結果頁要顯示Alert; 若N100失敗直接導至錯誤頁
						if(bs!=null && bs.getResult()) {
							bs900 = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null);
							if(bs900 != null && bs900.getResult()) {
								Map<String, String> dataMap = (Map)bs900.getData();
								log.trace(ESAPIUtil.vaildLog("dataMap={}" + dataMap));
								if("0003".equals(dataMap.get("SMSA"))) {
									log.info("SMSA = 0003");
									bs900.reset();
									bs900 = change_data_service.communication_data_plural_r_900(cusidn,okMap);
									if(bs900 != null && bs900.getResult()) {
										model.addAttribute("communication_data900", bs900);
									}
									else {
										log.info(ESAPIUtil.vaildLog("N900 error!"));
										bs900.addData("err900", "true");
										model.addAttribute("ERROR_900", bs900);
									}
								}
							}
						}
						SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
					}
				} catch (Exception e) {
					//Avoid Information Exposure Through an Error Message
					//e.printStackTrace();
					log.error("communication_data_plural_r error >> {}",e);
				}finally {
					if(bs!=null && bs.getResult()) {
						target = "/change_data/communication_data_plural_r";
						model.addAttribute("communication_data_plural", bs);
					}else {
						model.addAttribute(BaseResult.ERROR, bs);
					}
				}		
				return target;
		}
		
		//N570 變更外匯進口／出口／匯兌通訊地址／電話 機制選擇
		@RequestMapping(value = "/fcy_data")
			public String fcy_data(HttpServletRequest request, HttpServletResponse response,
					@RequestParam Map<String, String> reqParam, Model model) {
			log.trace("fcy_data>>>>");
			BaseResult bs = null;
			String target = "/change_data/fcy_data";
			
			try {
				bs = new BaseResult();
			
				//清除切換語系時暫存的資料
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");		
				
				// IKEY要使用的JSON:DC
				String jsondc = null;
				jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
				log.trace(ESAPIUtil.vaildLog("fcy_data.jsondc >> " + jsondc));
				
				bs = change_data_service.getTxToken();
				bs.addData("jsondc", jsondc);
				log.trace("fcy_data.getTxToken: {}" , bs.getResult());
				log.trace("fcy_data.TXTOKEN: {}" , ((Map<String, String>) bs.getData()).get("TXTOKEN"));
				if (bs.getResult()) {
					SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN,
							((Map<String, String>) bs.getData()).get("TXTOKEN"));
				}
				//TODO 暫時通過
//				bs.setResult(true);
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("fcy_data error >> {}",e);
			}finally {
				model.addAttribute("fcy_data", bs);			
			}
					
			return target;
		}
		
		// N570 變更外匯進口／出口／匯兌通訊地址／電話_變更頁
		@RequestMapping(value = "/fcy_data_query")
		public String fcy_data_query(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			log.trace(ESAPIUtil.vaildLog("fcy_data_query >> " + CodeUtil.toJson(reqParam)));
			String target = "/error";
			BaseResult bs = null;
			try {
				bs = new BaseResult();
				// Get session value by session Key
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
//	 			解決Trust Boundary Violation
				Map<String, String> okMap = reqParam;
//				判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
					if(hasLocale) 
					{
						bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
						if( ! bs.getResult())
						{
							log.trace("bs.getResult() >>{}",bs.getResult());
							throw new Exception();
						}
					}
					if( ! hasLocale){
						// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
						String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
						log.trace("fcy_data_query.token: {}", token);
						if (StrUtil.isNotEmpty(okMap.get("TOKEN")) && okMap.get("TOKEN").equals(token)) {
							bs.setResult(Boolean.TRUE);
						}
						log.trace("bs.getResult(): {}", bs.getResult());
						if (!bs.getResult()) {
							log.trace("throw new Exception()");
							throw new Exception();
						}
						bs.reset();
						bs = change_data_service.fcy_data_query(cusidn,okMap);
						bs.addData("fgtxway", reqParam.get("FGTXWAY"));
						log.trace("BSDATA>>>{}",bs.getData());
						SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
						SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
						//TODO 暫時通過
//						bs.setResult(true);
					}
					//IDGATE身分
			        String idgateUserFlag="N";		 
			        Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			        try {		 
			       	   if(IdgateData==null) {
			       		   IdgateData = new HashMap<String, Object>();
			       	   }
			            BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
			            if(tmp.getResult()) {		 
			                idgateUserFlag="Y";                  		 
			            }		 
			            tmp.addData("idgateUserFlag",idgateUserFlag);		 
			            IdgateData.putAll((Map<String, String>) tmp.getData());
			            SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
			        }catch(Exception e) {		 
			            log.debug("idgateUserFlag error {}",e);		 
			        }		 
			        model.addAttribute("idgateUserFlag",idgateUserFlag);
				} catch (Exception e) {
					//Avoid Information Exposure Through an Error Message
					//e.printStackTrace();
					log.error("fcy_data_query error >> {}",e);
				}finally {
					if(bs!=null && bs.getResult()) {
						target = "/change_data/fcy_data_query";
						model.addAttribute("fcy_data_query", bs);
					}else {
						bs.setPrevious("/CHANGE/DATA/fcy_data");
						model.addAttribute(BaseResult.ERROR, bs);
					}
				}		
				return target;
		}
		
		// N570 變更外匯進口／出口／匯兌通訊地址／電話_確認
		@RequestMapping(value = "/fcy_data_confirm")
		public String fcy_data_confirm(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			log.trace(ESAPIUtil.vaildLog("fcy_data_confirm_Start >> " + CodeUtil.toJson(reqParam)));
			String target = "/error";
			BaseResult bs = null;
			BaseResult bs1 = null;
			
//	 		解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			// 回上一頁重新賦值
			if(okMap.containsKey("previous")) 
			{
				Map m = (Map) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				okMap = m;
			}
			
			try {
				bs = new BaseResult();
				bs1 = new BaseResult();
				// Get session value by session Key
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
				// IKEY要使用的JSON:DC
				String jsondc = null;
				jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
				log.trace(ESAPIUtil.vaildLog("communication_data_plural.jsondc >> " + jsondc));
//				判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
					if(hasLocale) 
					{
						bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
						//保留之前的值
						bs1 = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null));
						Map sessionRec = (Map<String,String>)bs1.getData();
						bs1.setData(sessionRec);
						
						if( ! bs.getResult())
						{
							log.trace("bs.getResult() >>{}",bs.getResult());
							throw new Exception();
						}else {
							// session 資料放入 map 讓後續 model 和回上一頁取值
							okMap.putAll((Map<String, String>) bs.getData());
						}
						
					}
					if( ! hasLocale){
						bs.reset();
						//保留之前的值
						bs1 = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null));
						Map sessionRec = (Map<String,String>)bs1.getData();
						bs1.setData(sessionRec);
						//新值
						bs = change_data_service.fcy_data_confirm(cusidn,okMap);			
						bs.addData("fgtxway", reqParam.get("fgtxway"));
						log.trace("BSDATA>>>{}",bs.getData());
						bs.addData("jsondc", jsondc);
						SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs1);
						log.trace("SessionUtil.TRANSFER_DATA>>{}", CodeUtil.toJson(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, sessionRec)));

						SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
					}
					// IDGATE transdata            		 
		            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
		    		String adopid = "N570";
		    		String title = "您有一筆,變更外匯進口／出口／匯兌通訊地址／電話交易待確認";
		    		Map<String, String> result = okMap;
		        	if(IdgateData != null) {
			            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
			            if(IdgateData.get("idgateUserFlag").equals("Y")) {
			            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
			            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
							IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N570_IDGATE_DATA.class, N570_IDGATE_DATA_VIEW.class, result));
			                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
			            }
		        	}
		            model.addAttribute("idgateAdopid", adopid);
		            model.addAttribute("idgateTitle", title);
					//TODO 暫時通過
//					bs.setResult(true);
				} catch (Exception e) {
					//Avoid Information Exposure Through an Error Message
					//e.printStackTrace();
					log.error("fcy_data_confirm error >> {}",e);
				}finally {
					if(bs!=null && bs.getResult()) {
						target = "/change_data/fcy_data_confirm";
						model.addAttribute("fcy_data_confirm", bs);
						model.addAttribute("fcy_data_confirm_s", bs1);
						//設定回上一頁的url,配合confirm頁面的js
						model.addAttribute("previous","/CHANGE/DATA/fcy_data_query");
						//回填使用DATA
						SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
					}else {
						model.addAttribute(BaseResult.ERROR, bs);
					}
				}		
				return target;
		}
		
		// N570 變更外匯進口／出口／匯兌通訊地址／電話_結果
		@RequestMapping(value = "/fcy_data_result")
		public String fcy_data_result(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			log.trace(ESAPIUtil.vaildLog("fcy_data_result_Start >> " + CodeUtil.toJson(reqParam)));
			String target = "/error";
			BaseResult bs = null;
			
//	 		解決Trust Boundary Violation
			Map<String, String> okMap = reqParam;

			try {
				bs = new BaseResult();
				// Get session value by session Key
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
//				判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
					if(hasLocale) 
					{
						bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
						//保留之前的值
//						BaseResult bs2 = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null));
//						Map sessionRec =  (Map<String,String>) bs2.getData();
//						bs.addData("sessionRec",sessionRec);

						if( ! bs.getResult())
						{
							log.trace("bs.getResult() >>{}",bs.getResult());
							throw new Exception();
						}
					}
					if( ! hasLocale){
						bs.reset();
						//保留之前的值
						bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null));
						Map sessionRec =  (Map<String,String>) bs.getData();
						bs.reset();
		                //IDgate驗證		 
		                try {		 
		                    if(okMap.get("FGTXWAY").equals("7")) {		 

		        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
		        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N570_IDGATE");
		                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
		                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
		                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
		                    }		 
		                }catch(Exception e) {		 
		                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
		                }
						//新值
						bs = change_data_service.fcy_data_result(cusidn,okMap);						
						bs.addData("sessionRec",sessionRec);
						SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
					}
					//TODO 暫時通過
//					bs.setResult(true);
				} catch (Exception e) {
					//Avoid Information Exposure Through an Error Message
					//e.printStackTrace();
					log.error("fcy_data_result error >> {}",e);
				}finally {
					if(bs!=null && bs.getResult()) {
						target = "/change_data/fcy_data_result";
						model.addAttribute("fcy_data", bs);
					}else {
						model.addAttribute(BaseResult.ERROR, bs);
					}
				}		
				return target;
		}
		
		// N900變更信用卡帳單地址／電話 交易機制 選擇頁
		@RequestMapping(value = "/credit_data")
		public String credit_data(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			log.trace("credit_data>>>>");
			String target = "/error";
			BaseResult bs = null;
			try {
				bs = new BaseResult();
				Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
				String jsondc = null;
				jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam), "UTF-8");
				log.trace(ESAPIUtil.vaildLog("change_data.jsondc >> " + jsondc));
				bs.addData("jsondc", jsondc);
				model.addAttribute("cd", bs);
				target = "/change_data/credit_data";
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("credit_data error >> {}",e);
			}

			return target;
		}
		
		// N900變更信用卡帳單地址／電話 交易機制 資料頁
		@RequestMapping(value = "/credit_data_detail")
		public String credit_data_detail(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			log.trace(ESAPIUtil.vaildLog("credit_data_detail >> " + CodeUtil.toJson(reqParam)));
			String target = "/error";
			BaseResult bs = null;
			try {
				bs = new BaseResult();
				// Get session value by session Key
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
				// 解決Trust Boundary Violation
				Map<String, String> okMap = reqParam;
				// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
				if (hasLocale) {
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
					if (!bs.getResult()) {
						log.trace("bs.getResult() >>{}", bs.getResult());
						throw new Exception();
					}
				}
				if (!hasLocale) {
					bs.reset();
					bs = change_data_service.credit_data_detail(cusidn, okMap);
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
					log.trace("SERVICE RETURN BS DATA>>>{}",bs.getData());
				}
				
		        //IDGATE身分
		        String idgateUserFlag="N";		 
		        Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
		        try {		 
		       	   if(IdgateData==null) {
		       		   IdgateData = new HashMap<String, Object>();
		       	   }
		            BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
		            if(tmp.getResult()) {		 
		                idgateUserFlag="Y";                  		 
		            }		 
		            tmp.addData("idgateUserFlag",idgateUserFlag);		 
		            IdgateData.putAll((Map<String, String>) tmp.getData());
		            SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
		        }catch(Exception e) {		 
		            log.debug("idgateUserFlag error {}",e);		 
		        }		 
		        model.addAttribute("idgateUserFlag",idgateUserFlag);
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("credit_data_detail error >> {}",e);
			} finally {
				if (bs != null && bs.getResult()) {
					target = "/change_data/credit_data_detail";
					bs.addData("CardPassPassed", true);
					model.addAttribute("cd_detail", bs);
				} else {
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}
			return target;
		}

		// N900變更信用卡帳單地址／電話 交易機制 確認頁
		@RequestMapping(value = "/credit_data_confirm")
		public String credit_data_confirm(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			log.trace(ESAPIUtil.vaildLog("credit_data_confirm >> " + CodeUtil.toJson(reqParam)));
			String target = "/error";
			BaseResult bs = null;
			try {
				bs = new BaseResult();
				String jsondc = null;
//				jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam), "UTF-8");
//				log.trace("change_data.jsondc: {}", jsondc);
				// Get session value by session Key
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
				// 解決Trust Boundary Violation
				Map<String, String> okMap = reqParam;
				// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
				if (hasLocale) {
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
					if (!bs.getResult()) {
						log.trace("bs.getResult() >>{}", bs.getResult());
						throw new Exception();
					}
				}
				if (!hasLocale) {
					jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam), "UTF-8");
					log.trace(ESAPIUtil.vaildLog("change_data.jsondc >> " + jsondc));
					bs.reset();
					bs = change_data_service.credit_data_confirm(cusidn, okMap);
					bs.addData("jsondc", jsondc);
					log.trace("GET BACK BSDATA FROM SERVICE >>>{}", bs.getData());
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
				// IDGATE transdata            		 
	            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	            Map<String,String> bsdata=(Map<String, String>) bs.getData();
	            Map<String, String> result = new HashMap<>();
	            String adopid = "N900";
	    		String title = "您有一筆，變更信用卡帳單地址／電話交易待確認";
	        	if(IdgateData != null) {
		            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	result.putAll(bsdata);
		            	result.put("POSTCOD", bsdata.get("newZIP"));
		            	result.put("ADDRESS", bsdata.get("newADDR1"));
		            	result.put("HOME_TEL", bsdata.get("newHOMEPHONE"));
		            	result.put("OFFICE_TEL", bsdata.get("OFFICETEL"));
		            	result.put("MOBILE_TEL", bsdata.get("newMOBILEPHONE"));
		            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N900_IDGATE_DATA.class, N900_IDGATE_DATA_VIEW.class, result));
		                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
		            }
	        	}
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("credit_data_confirm error >> {}",e);
			} finally {
				if (bs != null && bs.getResult()) {
					target = "/change_data/credit_data_confirm";
					model.addAttribute("cd_confirm", bs);
				} else {
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}

			return target;
		}

		// N900變更信用卡帳單地址／電話 交易機制 結果頁
		@RequestMapping(value = "/credit_data_result")
		public String credit_data_result(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			log.trace(ESAPIUtil.vaildLog("credit_data_result >> " + CodeUtil.toJson(reqParam)));
			String target = "/error";
			BaseResult bs = null;
			try {
				bs = new BaseResult();
				// Get session value by session Key
				String cusidn = new String(
						Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
				// 解決Trust Boundary Violation
				Map<String, String> okMap = reqParam;
				// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
				if (hasLocale) {
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
					if (!bs.getResult()) {
						log.trace("bs.getResult() >>{}", bs.getResult());
						throw new Exception();
					}
				}
				if (!hasLocale) {
					bs.reset();
	                //IDgate驗證		 
	                try {		 
	                    if(okMap.get("FGTXWAY").equals("7")) {		 

	        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
	        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N900_IDGATE");
	                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
	                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
	                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
	                    }		 
	                }catch(Exception e) {		 
	                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
	                }  
					bs = change_data_service.credit_data_result(cusidn, okMap);
					log.trace("GET BACK BSDATA FROM SERVICE >>>{}", bs.getData());
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("credit_data_result error >> {}",e);
			} finally {
				if (bs != null && bs.getResult()) {
					target = "/change_data/credit_data_result";
					model.addAttribute("cd_result", bs);
				} else {
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}

			return target;
		}
	
	
	
	/**
	 * 清除暫存session的資料
	 * 
	 * @param model
	 */
	public void cleanSession(Model model)
	{
		// 清除切換語系
		SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null);
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null);
		// 清除回上一頁
		SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, null);
		// 清除print
		SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, null);
		// 換頁暫存資料
		SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, null);
	}
}
