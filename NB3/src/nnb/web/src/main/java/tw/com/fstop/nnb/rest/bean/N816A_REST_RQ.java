package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N816A_REST_RQ extends BaseRestBean_CC implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3533299751725538183L;
	
	private String CUSIDN ;
	private String FGTXWAY;  //交易機制 0 SSL
	private String PINNEW;	 //
	private String CARDNUM;  //卡號
	private String EXP_MON;  //到期月
	private String EXP_YEAR; //到期年
	private String ACT_PASS; //開卡密碼
	private String EXP_DATE; //到期年月日
	
	
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public String getCARDNUM() {
		return CARDNUM;
	}
	public String getEXP_MON() {
		return EXP_MON;
	}
	public String getEXP_YEAR() {
		return EXP_YEAR;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public void setCARDNUM(String cARDNUM) {
		CARDNUM = cARDNUM;
	}
	public void setEXP_MON(String eXP_MON) {
		EXP_MON = eXP_MON;
	}
	public void setEXP_YEAR(String eXP_YEAR) {
		EXP_YEAR = eXP_YEAR;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getACT_PASS() {
		return ACT_PASS;
	}
	public String getEXP_DATE() {
		return EXP_DATE;
	}
	public void setACT_PASS(String aCT_PASS) {
		ACT_PASS = aCT_PASS;
	}
	public void setEXP_DATE(String eXP_DATE) {
		EXP_DATE = eXP_DATE;
	}
	
}
