package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N530_REST_RS extends BaseRestBean implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 705845637240551098L;


	//??
	private String ABEND;
	//??
	private String USERDATA_X50;

	private String COUNT;		
	
	//TODO 暫時先這樣寫
	//NTD總計金額幣別
	private String NTDBALCUID_7;

	private String NTDBALCUID_3;

	private String NTDBALCUID_4;

	private String NTDBALCUID_5;

	private String NTDBALCUID_6;
    
	private String NTDBALCUID_1;
    
	private String NTDBALCUID_2;

	private String NTDBALCUID_8;
	
	private String NTDBALCUID_9;
	
	private String NTDBALCUID_10;

    //總計金額
	private String FXTOTAMT_1;
    
	private String FXTOTAMT_2;   
    
	private String FXTOTAMT_3;
    
	private String FXTOTAMT_4;
    
	private String FXTOTAMT_5;
    
	private String FXTOTAMT_6;
    
	private String FXTOTAMT_7;
	
	private String FXTOTAMT_8;
	
	private String FXTOTAMT_9;
	
	private String FXTOTAMT_10;

    //幣別
	private String AMTCUID_4;

	private String AMTCUID_5;

	private String AMTCUID_2;

	private String AMTCUID_3;

	private String AMTCUID_6;

	private String AMTCUID_7;

	private String AMTCUID_8;
	
	private String AMTCUID_9;
	
	private String AMTCUID_10;
	
	private String AMTCUID_1;

    //NTD總計金額
	private String FXTOTNTDBAL_10;
	
	private String FXTOTNTDBAL_9;

	private String FXTOTNTDBAL_8;
	
	private String FXTOTNTDBAL_7;

	private String FXTOTNTDBAL_6;

	private String FXTOTNTDBAL_5;

	private String FXTOTNTDBAL_4;

	private String FXTOTNTDBAL_3;

	private String FXTOTNTDBAL_2;

	private String FXTOTNTDBAL_1;
    //查詢時間
	private String CMQTIME;
    
	private String __OCCURS;
    
    //總筆數
	private String CMRECNUM;

	
	//rowdata用
	LinkedList<N530_REST_RSDATA> REC;


	public String getABEND() {
		return ABEND;
	}


	public void setABEND(String aBEND) {
		ABEND = aBEND;
	}


	public String getUSERDATA_X50() {
		return USERDATA_X50;
	}


	public void setUSERDATA_X50(String uSERDATA_X50) {
		USERDATA_X50 = uSERDATA_X50;
	}


	public String getCOUNT() {
		return COUNT;
	}


	public void setCOUNT(String cOUNT) {
		COUNT = cOUNT;
	}


	public String getNTDBALCUID_7() {
		return NTDBALCUID_7;
	}


	public void setNTDBALCUID_7(String nTDBALCUID_7) {
		NTDBALCUID_7 = nTDBALCUID_7;
	}


	public String getNTDBALCUID_3() {
		return NTDBALCUID_3;
	}


	public void setNTDBALCUID_3(String nTDBALCUID_3) {
		NTDBALCUID_3 = nTDBALCUID_3;
	}


	public String getNTDBALCUID_4() {
		return NTDBALCUID_4;
	}


	public void setNTDBALCUID_4(String nTDBALCUID_4) {
		NTDBALCUID_4 = nTDBALCUID_4;
	}


	public String getNTDBALCUID_5() {
		return NTDBALCUID_5;
	}


	public void setNTDBALCUID_5(String nTDBALCUID_5) {
		NTDBALCUID_5 = nTDBALCUID_5;
	}


	public String getNTDBALCUID_6() {
		return NTDBALCUID_6;
	}


	public void setNTDBALCUID_6(String nTDBALCUID_6) {
		NTDBALCUID_6 = nTDBALCUID_6;
	}


	public String getNTDBALCUID_1() {
		return NTDBALCUID_1;
	}


	public void setNTDBALCUID_1(String nTDBALCUID_1) {
		NTDBALCUID_1 = nTDBALCUID_1;
	}


	public String getNTDBALCUID_2() {
		return NTDBALCUID_2;
	}


	public void setNTDBALCUID_2(String nTDBALCUID_2) {
		NTDBALCUID_2 = nTDBALCUID_2;
	}


	public String getNTDBALCUID_8() {
		return NTDBALCUID_8;
	}


	public void setNTDBALCUID_8(String nTDBALCUID_8) {
		NTDBALCUID_8 = nTDBALCUID_8;
	}


	public String getNTDBALCUID_9() {
		return NTDBALCUID_9;
	}


	public void setNTDBALCUID_9(String nTDBALCUID_9) {
		NTDBALCUID_9 = nTDBALCUID_9;
	}


	public String getNTDBALCUID_10() {
		return NTDBALCUID_10;
	}


	public void setNTDBALCUID_10(String nTDBALCUID_10) {
		NTDBALCUID_10 = nTDBALCUID_10;
	}


	public String getFXTOTAMT_1() {
		return FXTOTAMT_1;
	}


	public void setFXTOTAMT_1(String fXTOTAMT_1) {
		FXTOTAMT_1 = fXTOTAMT_1;
	}


	public String getFXTOTAMT_2() {
		return FXTOTAMT_2;
	}


	public void setFXTOTAMT_2(String fXTOTAMT_2) {
		FXTOTAMT_2 = fXTOTAMT_2;
	}


	public String getFXTOTAMT_3() {
		return FXTOTAMT_3;
	}


	public void setFXTOTAMT_3(String fXTOTAMT_3) {
		FXTOTAMT_3 = fXTOTAMT_3;
	}


	public String getFXTOTAMT_4() {
		return FXTOTAMT_4;
	}


	public void setFXTOTAMT_4(String fXTOTAMT_4) {
		FXTOTAMT_4 = fXTOTAMT_4;
	}


	public String getFXTOTAMT_5() {
		return FXTOTAMT_5;
	}


	public void setFXTOTAMT_5(String fXTOTAMT_5) {
		FXTOTAMT_5 = fXTOTAMT_5;
	}


	public String getFXTOTAMT_6() {
		return FXTOTAMT_6;
	}


	public void setFXTOTAMT_6(String fXTOTAMT_6) {
		FXTOTAMT_6 = fXTOTAMT_6;
	}


	public String getFXTOTAMT_7() {
		return FXTOTAMT_7;
	}


	public void setFXTOTAMT_7(String fXTOTAMT_7) {
		FXTOTAMT_7 = fXTOTAMT_7;
	}


	public String getFXTOTAMT_8() {
		return FXTOTAMT_8;
	}


	public void setFXTOTAMT_8(String fXTOTAMT_8) {
		FXTOTAMT_8 = fXTOTAMT_8;
	}


	public String getFXTOTAMT_9() {
		return FXTOTAMT_9;
	}


	public void setFXTOTAMT_9(String fXTOTAMT_9) {
		FXTOTAMT_9 = fXTOTAMT_9;
	}


	public String getFXTOTAMT_10() {
		return FXTOTAMT_10;
	}


	public void setFXTOTAMT_10(String fXTOTAMT_10) {
		FXTOTAMT_10 = fXTOTAMT_10;
	}


	public String getAMTCUID_4() {
		return AMTCUID_4;
	}


	public void setAMTCUID_4(String aMTCUID_4) {
		AMTCUID_4 = aMTCUID_4;
	}


	public String getAMTCUID_5() {
		return AMTCUID_5;
	}


	public void setAMTCUID_5(String aMTCUID_5) {
		AMTCUID_5 = aMTCUID_5;
	}


	public String getAMTCUID_2() {
		return AMTCUID_2;
	}


	public void setAMTCUID_2(String aMTCUID_2) {
		AMTCUID_2 = aMTCUID_2;
	}


	public String getAMTCUID_3() {
		return AMTCUID_3;
	}


	public void setAMTCUID_3(String aMTCUID_3) {
		AMTCUID_3 = aMTCUID_3;
	}


	public String getAMTCUID_6() {
		return AMTCUID_6;
	}


	public void setAMTCUID_6(String aMTCUID_6) {
		AMTCUID_6 = aMTCUID_6;
	}


	public String getAMTCUID_7() {
		return AMTCUID_7;
	}


	public void setAMTCUID_7(String aMTCUID_7) {
		AMTCUID_7 = aMTCUID_7;
	}


	public String getAMTCUID_8() {
		return AMTCUID_8;
	}


	public void setAMTCUID_8(String aMTCUID_8) {
		AMTCUID_8 = aMTCUID_8;
	}


	public String getAMTCUID_9() {
		return AMTCUID_9;
	}


	public void setAMTCUID_9(String aMTCUID_9) {
		AMTCUID_9 = aMTCUID_9;
	}


	public String getAMTCUID_10() {
		return AMTCUID_10;
	}


	public void setAMTCUID_10(String aMTCUID_10) {
		AMTCUID_10 = aMTCUID_10;
	}


	public String getAMTCUID_1() {
		return AMTCUID_1;
	}


	public void setAMTCUID_1(String aMTCUID_1) {
		AMTCUID_1 = aMTCUID_1;
	}


	public String getFXTOTNTDBAL_10() {
		return FXTOTNTDBAL_10;
	}


	public void setFXTOTNTDBAL_10(String fXTOTNTDBAL_10) {
		FXTOTNTDBAL_10 = fXTOTNTDBAL_10;
	}


	public String getFXTOTNTDBAL_9() {
		return FXTOTNTDBAL_9;
	}


	public void setFXTOTNTDBAL_9(String fXTOTNTDBAL_9) {
		FXTOTNTDBAL_9 = fXTOTNTDBAL_9;
	}


	public String getFXTOTNTDBAL_8() {
		return FXTOTNTDBAL_8;
	}


	public void setFXTOTNTDBAL_8(String fXTOTNTDBAL_8) {
		FXTOTNTDBAL_8 = fXTOTNTDBAL_8;
	}


	public String getFXTOTNTDBAL_7() {
		return FXTOTNTDBAL_7;
	}


	public void setFXTOTNTDBAL_7(String fXTOTNTDBAL_7) {
		FXTOTNTDBAL_7 = fXTOTNTDBAL_7;
	}


	public String getFXTOTNTDBAL_6() {
		return FXTOTNTDBAL_6;
	}


	public void setFXTOTNTDBAL_6(String fXTOTNTDBAL_6) {
		FXTOTNTDBAL_6 = fXTOTNTDBAL_6;
	}


	public String getFXTOTNTDBAL_5() {
		return FXTOTNTDBAL_5;
	}


	public void setFXTOTNTDBAL_5(String fXTOTNTDBAL_5) {
		FXTOTNTDBAL_5 = fXTOTNTDBAL_5;
	}


	public String getFXTOTNTDBAL_4() {
		return FXTOTNTDBAL_4;
	}


	public void setFXTOTNTDBAL_4(String fXTOTNTDBAL_4) {
		FXTOTNTDBAL_4 = fXTOTNTDBAL_4;
	}


	public String getFXTOTNTDBAL_3() {
		return FXTOTNTDBAL_3;
	}


	public void setFXTOTNTDBAL_3(String fXTOTNTDBAL_3) {
		FXTOTNTDBAL_3 = fXTOTNTDBAL_3;
	}


	public String getFXTOTNTDBAL_2() {
		return FXTOTNTDBAL_2;
	}


	public void setFXTOTNTDBAL_2(String fXTOTNTDBAL_2) {
		FXTOTNTDBAL_2 = fXTOTNTDBAL_2;
	}


	public String getFXTOTNTDBAL_1() {
		return FXTOTNTDBAL_1;
	}


	public void setFXTOTNTDBAL_1(String fXTOTNTDBAL_1) {
		FXTOTNTDBAL_1 = fXTOTNTDBAL_1;
	}


	public String getCMQTIME() {
		return CMQTIME;
	}


	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}


	public String get__OCCURS() {
		return __OCCURS;
	}


	public void set__OCCURS(String __OCCURS) {
		this.__OCCURS = __OCCURS;
	}


	public String getCMRECNUM() {
		return CMRECNUM;
	}


	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}


	public LinkedList<N530_REST_RSDATA> getREC() {
		return REC;
	}


	public void setREC(LinkedList<N530_REST_RSDATA> rEC) {
		REC = rEC;
	}
	

}
