package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * N373電文RS
 */
public class N373_REST_RS extends BaseRestBean implements Serializable{
	private static final long serialVersionUID = 6791228050721094802L;
	
	private String TRNDATE;
	private String TRNTIME;
	private String TYPE;
	private String TRANSCODE;
	private String CDNO;
	private String TRADEDATE;
	private String INTRANSCODE;
	private String UNIT;
	private String FCA1;
	private String AMT3;
	private String FCA2;
	private String AMT5;
	private String OUTACN;
	private String INTSACN;
	private String BILLSENDMODE;
	private String SSLTXNO;
	private String FUNDAMT;
	private String RSKATT;
	private String GETLTD;
	private String RRSK;
	private String RISK7;
	private String GETLTD7;
	private String CMQTIME;
	
	public String getTRNDATE(){
		return TRNDATE;
	}
	public void setTRNDATE(String tRNDATE){
		TRNDATE = tRNDATE;
	}
	public String getTRNTIME(){
		return TRNTIME;
	}
	public void setTRNTIME(String tRNTIME){
		TRNTIME = tRNTIME;
	}
	public String getTYPE(){
		return TYPE;
	}
	public void setTYPE(String tYPE){
		TYPE = tYPE;
	}
	public String getTRANSCODE(){
		return TRANSCODE;
	}
	public void setTRANSCODE(String tRANSCODE){
		TRANSCODE = tRANSCODE;
	}
	public String getTRADEDATE(){
		return TRADEDATE;
	}
	public void setTRADEDATE(String tRADEDATE){
		TRADEDATE = tRADEDATE;
	}
	public String getINTRANSCODE(){
		return INTRANSCODE;
	}
	public void setINTRANSCODE(String iNTRANSCODE){
		INTRANSCODE = iNTRANSCODE;
	}
	public String getUNIT(){
		return UNIT;
	}
	public void setUNIT(String uNIT){
		UNIT = uNIT;
	}
	public String getFCA1(){
		return FCA1;
	}
	public void setFCA1(String fCA1){
		FCA1 = fCA1;
	}
	public String getAMT3(){
		return AMT3;
	}
	public void setAMT3(String aMT3){
		AMT3 = aMT3;
	}
	public String getFCA2(){
		return FCA2;
	}
	public void setFCA2(String fCA2){
		FCA2 = fCA2;
	}
	public String getAMT5(){
		return AMT5;
	}
	public void setAMT5(String aMT5){
		AMT5 = aMT5;
	}
	public String getOUTACN(){
		return OUTACN;
	}
	public void setOUTACN(String oUTACN){
		OUTACN = oUTACN;
	}
	public String getINTSACN(){
		return INTSACN;
	}
	public void setINTSACN(String iNTSACN){
		INTSACN = iNTSACN;
	}
	public String getBILLSENDMODE(){
		return BILLSENDMODE;
	}
	public void setBILLSENDMODE(String bILLSENDMODE){
		BILLSENDMODE = bILLSENDMODE;
	}
	public String getSSLTXNO(){
		return SSLTXNO;
	}
	public void setSSLTXNO(String sSLTXNO){
		SSLTXNO = sSLTXNO;
	}
	public String getFUNDAMT(){
		return FUNDAMT;
	}
	public void setFUNDAMT(String fUNDAMT){
		FUNDAMT = fUNDAMT;
	}
	public String getRSKATT(){
		return RSKATT;
	}
	public void setRSKATT(String rSKATT){
		RSKATT = rSKATT;
	}
	public String getGETLTD(){
		return GETLTD;
	}
	public void setGETLTD(String gETLTD){
		GETLTD = gETLTD;
	}
	public String getRRSK(){
		return RRSK;
	}
	public void setRRSK(String rRSK){
		RRSK = rRSK;
	}
	public String getRISK7(){
		return RISK7;
	}
	public void setRISK7(String rISK7){
		RISK7 = rISK7;
	}
	public String getGETLTD7(){
		return GETLTD7;
	}
	public void setGETLTD7(String gETLTD7){
		GETLTD7 = gETLTD7;
	}
	public String getCMQTIME(){
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME){
		CMQTIME = cMQTIME;
	}
	public String getCDNO() {
		return CDNO;
	}
	public void setCDNO(String cDNO) {
		CDNO = cDNO;
	}
}