package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N171_IDGATE_DATA implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -4310361797533125231L;
	
	@SerializedName(value = "CUSIDN")
	private String CUSIDN; // 使用者統編

	@SerializedName(value = "FGTXDATE")
	private String FGTXDATE; // 請確認轉帳資料

//	@SerializedName(value = "transfer_date")
//	private String transfer_date; // 轉帳日期
	
	@SerializedName(value = "OUTACN")
	private String OUTACN; // 轉出帳號
	
//	@SerializedName(value = "TaxTypeText")
//	private String TaxTypeText; // 繳稅類別
	
	@SerializedName(value = "INTSACN1")
	private String INTSACN1; // 稽徵單位代號
	
	@SerializedName(value = "INTSACN2")
	private String INTSACN2; // 身分證字號/統一編號
	
	@SerializedName(value = "INTSACN3")
	private String INTSACN3; // 銷帳編號
	
	@SerializedName(value = "PAYDUED")
	private String PAYDUED; // 繳納截止日
	
	@SerializedName(value = "AMOUNT")
	private String AMOUNT; // 繳款金額

	
	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getFGTXDATE() {
		return FGTXDATE;
	}

	public void setFGTXDATE(String fGTXDATE) {
		FGTXDATE = fGTXDATE;
	}

	public String getOUTACN() {
		return OUTACN;
	}

	public void setOUTACN(String oUTACN) {
		OUTACN = oUTACN;
	}

	public String getINTSACN1() {
		return INTSACN1;
	}

	public void setINTSACN1(String iNTSACN1) {
		INTSACN1 = iNTSACN1;
	}

	public String getINTSACN2() {
		return INTSACN2;
	}

	public void setINTSACN2(String iNTSACN2) {
		INTSACN2 = iNTSACN2;
	}

	public String getINTSACN3() {
		return INTSACN3;
	}

	public void setINTSACN3(String iNTSACN3) {
		INTSACN3 = iNTSACN3;
	}

	public String getPAYDUED() {
		return PAYDUED;
	}

	public void setPAYDUED(String pAYDUED) {
		PAYDUED = pAYDUED;
	}

	public String getAMOUNT() {
		return AMOUNT;
	}

	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	
}
