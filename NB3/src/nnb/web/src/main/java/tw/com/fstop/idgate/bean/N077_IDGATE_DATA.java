package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N077_IDGATE_DATA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7034793384714222458L;
	/**
	 * 
	 */
	@SerializedName(value = "CUSIDN")
	private String CUSIDN;// 統一編號
	@SerializedName(value = "FDPACN")
	private String FDPACN;// 存單帳號
	@SerializedName(value = "FDPNUM")
	private String FDPNUM;// 存單號碼
	@SerializedName(value = "FGRENCNT")
	private String FGRENCNT;// 轉期次數
	@SerializedName(value = "DPSVACNO")
	private String DPSVACNO;// 轉帳帳號
	@SerializedName(value = "DPRENCNT")
	private String DPRENCNT;//// 轉期次數
	@SerializedName(value = "AMTFDP")
    private String AMTFDP;//金額
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getFDPACN() {
		return FDPACN;
	}
	public void setFDPACN(String fDPACN) {
		FDPACN = fDPACN;
	}
	public String getFDPNUM() {
		return FDPNUM;
	}
	public void setFDPNUM(String fDPNUM) {
		FDPNUM = fDPNUM;
	}
	public String getFGRENCNT() {
		return FGRENCNT;
	}
	public void setFGRENCNT(String fGRENCNT) {
		FGRENCNT = fGRENCNT;
	}
	public String getDPSVACNO() {
		return DPSVACNO;
	}
	public void setDPSVACNO(String dPSVACNO) {
		DPSVACNO = dPSVACNO;
	}
	public String getDPRENCNT() {
		return DPRENCNT;
	}
	public void setDPRENCNT(String dPRENCNT) {
		DPRENCNT = dPRENCNT;
	}
	public String getAMTFDP() {
		return AMTFDP;
	}
	public void setAMTFDP(String aMTFDP) {
		AMTFDP = aMTFDP;
	}
    
	
	
}
