package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N215D_1_REST_RSDATA extends BaseRestBean implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7794332561098163486L;
	
	private String ACN;
	private String BNKCOD;//行庫代號
	private String RAFLAG;//約定註記

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	public String getBNKCOD() {
		return BNKCOD;
	}

	public void setBNKCOD(String bNKCOD) {
		BNKCOD = bNKCOD;
	}

	public String getRAFLAG() {
		return RAFLAG;
	}

	public void setRAFLAG(String rAFLAG) {
		RAFLAG = rAFLAG;
	}
	
	
	
	
}
