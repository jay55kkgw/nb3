package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class P003_REST_RSDATA extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1923948096797824249L;

	private String Status;//申請代收車號狀態
	private String Account;//扣款帳號
	private String RespCode2;//回應代碼
	private String CardCVC2;//信用卡CVC2
	private String CanPay;//是否可扣款
	private String RegistDate;//註冊日期
	private String NeedConfirm;//需客戶確認扣款
	private String source;//申請來源別
	private String StartDate;//啟用日期
	private String CarId;//牌照號碼
	private String MailFlag;//可發通知email
	private String CardType;//扣款方式
	private String SMSFlag;//可發簡訊通知
	private String ZoneCode;//縣市別
	private String CarKind;//車種
	private String RespString2;//回應代碼敘述
	private String CardValidDate;//信用卡有效日期

	private String ZoneCode_str;
	private String Status_str;
	private String CarKind_str;
	
	public String getZoneCode_str() {
		return ZoneCode_str;
	}
	public void setZoneCode_str(String zoneCode_str) {
		ZoneCode_str = zoneCode_str;
	}
	public String getStatus_str() {
		return Status_str;
	}
	public void setStatus_str(String status_str) {
		Status_str = status_str;
	}
	public String getCarKind_str() {
		return CarKind_str;
	}
	public void setCarKind_str(String carKind_str) {
		CarKind_str = carKind_str;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getAccount() {
		return Account;
	}
	public void setAccount(String account) {
		Account = account;
	}
	public String getRespCode2() {
		return RespCode2;
	}
	public void setRespCode2(String respCode2) {
		RespCode2 = respCode2;
	}
	public String getCardCVC2() {
		return CardCVC2;
	}
	public void setCardCVC2(String cardCVC2) {
		CardCVC2 = cardCVC2;
	}
	public String getCanPay() {
		return CanPay;
	}
	public void setCanPay(String canPay) {
		CanPay = canPay;
	}
	public String getRegistDate() {
		return RegistDate;
	}
	public void setRegistDate(String registDate) {
		RegistDate = registDate;
	}
	public String getNeedConfirm() {
		return NeedConfirm;
	}
	public void setNeedConfirm(String needConfirm) {
		NeedConfirm = needConfirm;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		source = source;
	}
	public String getStartDate() {
		return StartDate;
	}
	public void setStartDate(String startDate) {
		StartDate = startDate;
	}
	public String getCarId() {
		return CarId;
	}
	public void setCarId(String carId) {
		CarId = carId;
	}
	public String getMailFlag() {
		return MailFlag;
	}
	public void setMailFlag(String mailFlag) {
		MailFlag = mailFlag;
	}
	public String getCardType() {
		return CardType;
	}
	public void setCardType(String cardType) {
		CardType = cardType;
	}
	public String getSMSFlag() {
		return SMSFlag;
	}
	public void setSMSFlag(String sMSFlag) {
		SMSFlag = sMSFlag;
	}
	public String getZoneCode() {
		return ZoneCode;
	}
	public void setZoneCode(String zoneCode) {
		ZoneCode = zoneCode;
	}
	public String getCarKind() {
		return CarKind;
	}
	public void setCarKind(String carKind) {
		CarKind = carKind;
	}
	public String getRespString2() {
		return RespString2;
	}
	public void setRespString2(String respString2) {
		RespString2 = respString2;
	}
	public String getCardValidDate() {
		return CardValidDate;
	}
	public void setCardValidDate(String cardValidDate) {
		CardValidDate = cardValidDate;
	}	
}
