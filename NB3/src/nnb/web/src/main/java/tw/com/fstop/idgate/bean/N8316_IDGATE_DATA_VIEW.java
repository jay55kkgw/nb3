package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

public class N8316_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5556555905527940420L;

	@SerializedName(value = "CARDNUM")
	private String CARDNUM;

	@SerializedName(value = "CUSNUM")
	private String CUSNUM;
	
	@Override
	public Map<String, String> coverMap(){
		LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", "中華電信電信費代扣繳申請");
		result.put("交易類型", "申請");
		result.put("信用卡號", this.CARDNUM);
		result.put("用戶編號（電號）", this.CUSNUM);
		return result;
	}

	public String getCARDNUM() {
		return CARDNUM;
	}

	public void setCARDNUM(String cARDNUM) {
		CARDNUM = cARDNUM;
	}

	public String getCUSNUM() {
		return CUSNUM;
	}

	public void setCUSNUM(String cUSNUM) {
		CUSNUM = cUSNUM;
	}
	
}
