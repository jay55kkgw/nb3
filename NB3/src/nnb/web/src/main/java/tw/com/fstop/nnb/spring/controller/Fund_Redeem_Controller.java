package tw.com.fstop.nnb.spring.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.google.gson.Gson;

import fstop.orm.po.TXNFUNDDATA;
import org.springframework.beans.factory.annotation.Autowired;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.aop.Dialog;
import tw.com.fstop.nnb.service.Fund_Redeem_Service;
import tw.com.fstop.nnb.service.Fund_Transfer_Service;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.web.util.WebUtil;

/**
 * 基金贖回交易的Controller
 */
@SessionAttributes({SessionUtil.CUSIDN,SessionUtil.XMLCOD,SessionUtil.DPMYEMAIL,SessionUtil.RESULT_LOCALE_DATA})
@Controller
@RequestMapping(value = "/FUND/REDEEM")
public class Fund_Redeem_Controller{
	Logger log = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private Fund_Transfer_Service fund_transfer_service;
	@Autowired
	private Fund_Redeem_Service fund_redeem_service;
	
	/**
	 * 前往基金贖回查詢頁
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/fund_redeem_data")
	public String fund_redeem_data(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		BaseResult bs = null;
		log.debug("FUND_REDEEM_DATA START");
		try{
			log.debug(ESAPIUtil.vaildLog("REQPARAM >> {}"+requestParam));
			log.debug("SESSION >> {}",SessionUtil.DPMYEMAIL);
			// 解決 Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
			log.debug("cusidn={}",cusidn);
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				log.debug("LOCALE BSDATA >> {}",CodeUtil.toJson(bs));
			}else {
//				//20220318 移除此導頁功能 , 改由頁面提示
//				//檢核Email
//				String WANAEMAIL = okMap.get("WANAEMAIL");
//				log.debug(ESAPIUtil.vaildLog("WANAEMAIL={}"+WANAEMAIL));
//				if(!"NONEED".equals(WANAEMAIL)){
//					String DPMYEMAIL = (String)SessionUtil.getAttribute(model,SessionUtil.DPMYEMAIL,null);
//					log.debug("DPMYEMAIL={}",DPMYEMAIL);
//					if(DPMYEMAIL == null || "".equals(DPMYEMAIL)){
//						//設定EMAIL
//						target = "forward:/FUND/TRANSFER/fundemail?ADOPID=FundEmail&TXID=C024";
//						return target;
//					}
//				}
				//清空session
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA,"");
				//do something
				bs = fund_redeem_service.fund_redeem_data(cusidn,okMap);
				//存入session
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				log.debug("BS DATA >>{}",CodeUtil.toJson(bs));
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_redeem_data error >> {}",e);
		}
		finally {
			if(bs!=null &&bs.getResult()) {
				model.addAttribute("bs",bs);
				target = "/fund/fund_redeem_data";
			}else {
				bs.setPrevious("");
				model.addAttribute(BaseResult.ERROR,bs);
			}
		}
		return target;
	}
	/**
	 * 前往基金贖回選擇頁
	 */
	@RequestMapping(value = "/fund_redeem_select")
	public String fund_redeem_select(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		BaseResult bs=null;
		log.debug("FUND_REDEEM_SELECT START");
		try{			
			log.debug("SESSION >> {}",SessionUtil.DPMYEMAIL);
			// 解決 Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			log.debug(ESAPIUtil.vaildLog("REQPARAM >> {}"+okMap));
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			bs = new BaseResult();
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				log.debug("LOCALE BSDATA >> {}",CodeUtil.toJson(bs));
			}else {
				//用來判斷即時或預約交易
				boolean isFundTradeTime = fund_transfer_service.isFundTradeTime();
				log.debug("isFundTradeTime={}",isFundTradeTime);
				//預約交易
				if(isFundTradeTime == false){
					target = "forward:/FUND/RESERVE/REDEEM/fund_reserve_redeem_select";
					return target;
				}
//				//測試用
//				if(false){
//					target = "forward:/FUND/RESERVE/REDEEM/fund_reserve_redeem_select";
//					return target;
//				}
				//清空session
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA,"");
				//do something
				fund_redeem_service.prepareFundRedeemSelectData(okMap,bs);	
				if(bs.getData()!=null)bs.setResult(Boolean.TRUE);
				//存入session
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				log.debug("BS DATA >>{}",CodeUtil.toJson(bs));
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_redeem_select error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				model.addAttribute("bs",bs);
				target = "/fund/fund_redeem_select";
			}else {
				bs.setPrevious("/FUND/REDEEM/fund_redeem_data");
				model.addAttribute(BaseResult.ERROR,bs);
			}
		}
		return target;
	}
	/**
	 * 前往基金贖回確認頁
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/fund_redeem_confirm")
	public String fund_redeem_confirm(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		BaseResult bs=null;
		log.debug("FUND_REDEEM_CONFIRM START");
		try{
			// 解決 Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			log.debug(ESAPIUtil.vaildLog("REQPARAM >> {}"+okMap));
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			bs = new BaseResult();
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				log.debug("LOCALE BSDATA >> {}",CodeUtil.toJson(bs));
			}else {
				String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
				log.debug("cusidn={}",cusidn);
				//清空session
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA,"");
				//do something *內有new baseresult
				bs=fund_redeem_service.prepareFundRedeemConfirmData(okMap,cusidn);
				//存入session
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				log.debug("BS DATA >>{}",CodeUtil.toJson(bs));
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_redeem_confirm error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				model.addAttribute("bs",bs);
				target = "fund/fund_redeem_confirm";
			}else {
				bs.setPrevious("/FUND/REDEEM/fund_redeem_data");
				model.addAttribute(BaseResult.ERROR,bs);
			}
		}
		return target;
	}
	/**
	 * 前往基金贖回結果頁
	 */
	@RequestMapping(value = "/fund_redeem_result")
	@Dialog(ADOPID = "C024")
	public String fund_redeem_result(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		BaseResult bs=null;
		log.debug("FUND_REDEEM_RESULT START");
		try{
			// 解決 Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			log.debug(ESAPIUtil.vaildLog("REQPARAM >> {}"+okMap));
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			bs = new BaseResult();
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				log.debug("LOCALE BSDATA >> {}",CodeUtil.toJson(bs));
			}else {
				String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
				log.debug("cusidn={}",cusidn);
				okMap.put("CUSIDN",cusidn);
				String hiddenCUSIDN = WebUtil.hideID(cusidn);

				//寄件者信箱
				String DPMYEMAIL = (String)SessionUtil.getAttribute(model,SessionUtil.DPMYEMAIL,null);
				log.debug("DPMYEMAIL={}",DPMYEMAIL);
				okMap.put("DPMYEMAIL",DPMYEMAIL);
				
				//收件者信箱
				okMap.put("CMTRMAIL",DPMYEMAIL);
				String IP = WebUtil.getIpAddr(request);
				log.debug(ESAPIUtil.vaildLog("IP >> " + IP));
				okMap.put("IP",IP);
				bs = fund_redeem_service.processFundRedeem(okMap);
				bs.addData("hiddenCUSIDN",hiddenCUSIDN);
				
				//20210322
				bs.addData("SKIP_P", "Y");
				
				log.debug("BS DATA >>{}",CodeUtil.toJson(bs));
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_redeem_result error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				model.addAttribute("bs",bs);
				target = "fund/fund_redeem_result";
			}else {
				bs.setPrevious("/FUND/REDEEM/fund_redeem_data");
				model.addAttribute(BaseResult.ERROR,bs);
			}
		}
		return target;
	}
	/**
	 * 執行終止扣款的AJAX
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/fundStopPayAjax",produces={"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult fundStopPayAjax(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		log.debug("IN fundStopPayAjax");
		
		BaseResult baseResult = new BaseResult();
		baseResult.setResult(Boolean.FALSE);
		
		try{
			String IP = WebUtil.getIpAddr(request);
			log.debug(ESAPIUtil.vaildLog("IP >> " + IP));
			requestParam.put("IP",IP);
			
			baseResult = fund_redeem_service.fundStopPay(requestParam,model);
			
			if(baseResult != null && baseResult.getResult()){
				baseResult.setResult(Boolean.TRUE);
				Map<String,Object> map = (Map<String,Object>)baseResult.getData();
				
				String dataString = new Gson().toJson(map,map.getClass());
				log.debug("dataString={}",dataString);
				
				baseResult.setData(dataString);
			}
			log.debug("BSDATA >>",CodeUtil.toJson(baseResult));
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fundStopPayAjax error >> {}",e);
		}finally {
			
		}
		return baseResult;
	}
}