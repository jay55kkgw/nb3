package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N201_REST_RQ extends BaseRestBean_OLS implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */

	private String DATE;//日期YYYMMDD
	private String TIME;//時間HHMMSS
	private String UID;//統一編號
	private String CUSIDN;//統一編號
	private String USERID;//網銀使用者名稱
	private String IBHPW;//全行收付密碼
	private String ACN;//預備開戶帳號
	private String NAME;//戶名
	private String BIRTHDAY;//出生日期
	private String POSTCOD1;//戶籍地郵遞區號
	private String PMTADR;//戶籍地址
	private String POSTCOD2;//通訊地郵遞區號
	private String CTTADR;//通訊地址
	private String ARACOD1;//連絡電話區域碼
	private String TELNUM1;//連絡電話號碼(住)
	private String ARACOD2;//連絡電話區域碼
	private String TELNUM2;//連絡電話號碼(公)
	private String CELPHONE;//手機號碼
	private String ARACOD3;//傳真區域碼
	private String FAX;//傳真號碼
	private String MAILADDR;//電子郵件地址
	private String MARRY;//婚姻狀況
	private String CHILD;//小孩數
	private String DEGREE;//教育程度
	private String CAREER1;//職業(聯徵)
	private String CAREER2;//職稱
	private String SALARY;//年收入
	private String ODFTYN;//綜存質借
	private String LMTTYN;//ATM非約轉
	private String HAS_PIC;//身分證是否有照片
	private String ID_CHGE;//身分證異動型態
	private String CHGE_DT;//身分證異動日
	private String CHGE_CY;//身分證補換縣市
	private String AGREE;//同意合作推廣
	private String BRHCOD;
	private String CUSNAME;//原住民姓名
	private String STAEDYN;//是否申請為起家金帳戶
	private String STAEDSQ;//起家金帳戶 序號

	public String getCUSNAME() {
		return CUSNAME;
	}
	public void setCUSNAME(String cUSNAME) {
		CUSNAME = cUSNAME;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getUSERID() {
		return USERID;
	}
	public void setUSERID(String uSERID) {
		USERID = uSERID;
	}
	public String getIBHPW() {
		return IBHPW;
	}
	public void setIBHPW(String iBHPW) {
		IBHPW = iBHPW;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getBIRTHDAY() {
		return BIRTHDAY;
	}
	public void setBIRTHDAY(String bIRTHDAY) {
		BIRTHDAY = bIRTHDAY;
	}
	public String getPOSTCOD1() {
		return POSTCOD1;
	}
	public void setPOSTCOD1(String pOSTCOD1) {
		POSTCOD1 = pOSTCOD1;
	}
	public String getPMTADR() {
		return PMTADR;
	}
	public void setPMTADR(String pMTADR) {
		PMTADR = pMTADR;
	}
	public String getPOSTCOD2() {
		return POSTCOD2;
	}
	public void setPOSTCOD2(String pOSTCOD2) {
		POSTCOD2 = pOSTCOD2;
	}
	public String getCTTADR() {
		return CTTADR;
	}
	public void setCTTADR(String cTTADR) {
		CTTADR = cTTADR;
	}
	public String getARACOD1() {
		return ARACOD1;
	}
	public void setARACOD1(String aRACOD1) {
		ARACOD1 = aRACOD1;
	}
	public String getTELNUM1() {
		return TELNUM1;
	}
	public void setTELNUM1(String tELNUM1) {
		TELNUM1 = tELNUM1;
	}
	public String getARACOD2() {
		return ARACOD2;
	}
	public void setARACOD2(String aRACOD2) {
		ARACOD2 = aRACOD2;
	}
	public String getTELNUM2() {
		return TELNUM2;
	}
	public void setTELNUM2(String tELNUM2) {
		TELNUM2 = tELNUM2;
	}
	public String getCELPHONE() {
		return CELPHONE;
	}
	public void setCELPHONE(String cELPHONE) {
		CELPHONE = cELPHONE;
	}
	public String getARACOD3() {
		return ARACOD3;
	}
	public void setARACOD3(String aRACOD3) {
		ARACOD3 = aRACOD3;
	}
	public String getFAX() {
		return FAX;
	}
	public void setFAX(String fAX) {
		FAX = fAX;
	}
	public String getMAILADDR() {
		return MAILADDR;
	}
	public void setMAILADDR(String mAILADDR) {
		MAILADDR = mAILADDR;
	}
	public String getMARRY() {
		return MARRY;
	}
	public void setMARRY(String mARRY) {
		MARRY = mARRY;
	}
	public String getCHILD() {
		return CHILD;
	}
	public void setCHILD(String cHILD) {
		CHILD = cHILD;
	}
	public String getDEGREE() {
		return DEGREE;
	}
	public void setDEGREE(String dEGREE) {
		DEGREE = dEGREE;
	}
	public String getCAREER1() {
		return CAREER1;
	}
	public void setCAREER1(String cAREER1) {
		CAREER1 = cAREER1;
	}
	public String getCAREER2() {
		return CAREER2;
	}
	public void setCAREER2(String cAREER2) {
		CAREER2 = cAREER2;
	}
	public String getSALARY() {
		return SALARY;
	}
	public void setSALARY(String sALARY) {
		SALARY = sALARY;
	}
	public String getODFTYN() {
		return ODFTYN;
	}
	public void setODFTYN(String oDFTYN) {
		ODFTYN = oDFTYN;
	}
	public String getLMTTYN() {
		return LMTTYN;
	}
	public void setLMTTYN(String lMTTYN) {
		LMTTYN = lMTTYN;
	}
	public String getHAS_PIC() {
		return HAS_PIC;
	}
	public void setHAS_PIC(String hAS_PIC) {
		HAS_PIC = hAS_PIC;
	}
	public String getID_CHGE() {
		return ID_CHGE;
	}
	public void setID_CHGE(String iD_CHGE) {
		ID_CHGE = iD_CHGE;
	}
	public String getCHGE_DT() {
		return CHGE_DT;
	}
	public void setCHGE_DT(String cHGE_DT) {
		CHGE_DT = cHGE_DT;
	}
	public String getCHGE_CY() {
		return CHGE_CY;
	}
	public void setCHGE_CY(String cHGE_CY) {
		CHGE_CY = cHGE_CY;
	}
	public String getAGREE() {
		return AGREE;
	}
	public void setAGREE(String aGREE) {
		AGREE = aGREE;
	}
	public String getBRHCOD() {
		return BRHCOD;
	}
	public void setBRHCOD(String bRHCOD) {
		BRHCOD = bRHCOD;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getSTAEDYN() {return STAEDYN;}
	public void setSTAEDYN(String STAEDYN) {this.STAEDYN = STAEDYN;}
	public String getSTAEDSQ() {return STAEDSQ;}
	public void setSTAEDSQ(String STAEDSQ) {this.STAEDSQ = STAEDSQ;}
}
