package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N175_REST_RS extends BaseRestBean implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2496323371576608491L;

	private String INTMTH;

	private String FILLER;

	private String INT;

	private String DPISDT;

	private String ITR;

	private String FDPNUM;

	private String MAC;

	private String TRNTIME;

	private String TRMTYP;

	private String DUEDAT;

	private String AMT;

	private String CUID;

	private String NHITAX;

	private String MSGCOD;

	private String TAX;

	private String OFFSET;

	private String PAIAFTX;

	private String HEADER;

	private String TRNDATE;

	private String FDPACN;

	private String TERM;

	private String CMQTIME;

	private String __OCCURS;

	public String getINTMTH()
	{
		return INTMTH;
	}

	public void setINTMTH(String iNTMTH)
	{
		INTMTH = iNTMTH;
	}

	public String getFILLER()
	{
		return FILLER;
	}

	public void setFILLER(String fILLER)
	{
		FILLER = fILLER;
	}

	public String getINT()
	{
		return INT;
	}

	public void setINT(String iNT)
	{
		INT = iNT;
	}

	public String getDPISDT()
	{
		return DPISDT;
	}

	public void setDPISDT(String dPISDT)
	{
		DPISDT = dPISDT;
	}

	public String getITR()
	{
		return ITR;
	}

	public void setITR(String iTR)
	{
		ITR = iTR;
	}

	public String getFDPNUM()
	{
		return FDPNUM;
	}

	public void setFDPNUM(String fDPNUM)
	{
		FDPNUM = fDPNUM;
	}

	public String getMAC()
	{
		return MAC;
	}

	public void setMAC(String mAC)
	{
		MAC = mAC;
	}

	public String getTRNTIME()
	{
		return TRNTIME;
	}

	public void setTRNTIME(String tRNTIME)
	{
		TRNTIME = tRNTIME;
	}

	public String getTRMTYP()
	{
		return TRMTYP;
	}

	public void setTRMTYP(String tRMTYP)
	{
		TRMTYP = tRMTYP;
	}

	public String getDUEDAT()
	{
		return DUEDAT;
	}

	public void setDUEDAT(String dUEDAT)
	{
		DUEDAT = dUEDAT;
	}

	public String getAMT()
	{
		return AMT;
	}

	public void setAMT(String aMT)
	{
		AMT = aMT;
	}

	public String getCUID()
	{
		return CUID;
	}

	public void setCUID(String cUID)
	{
		CUID = cUID;
	}

	public String getNHITAX()
	{
		return NHITAX;
	}

	public void setNHITAX(String nHITAX)
	{
		NHITAX = nHITAX;
	}

	public String getMSGCOD()
	{
		return MSGCOD;
	}

	public void setMSGCOD(String mSGCOD)
	{
		MSGCOD = mSGCOD;
	}

	public String getTAX()
	{
		return TAX;
	}

	public void setTAX(String tAX)
	{
		TAX = tAX;
	}

	public String getOFFSET()
	{
		return OFFSET;
	}

	public void setOFFSET(String oFFSET)
	{
		OFFSET = oFFSET;
	}

	public String getPAIAFTX()
	{
		return PAIAFTX;
	}

	public void setPAIAFTX(String pAIAFTX)
	{
		PAIAFTX = pAIAFTX;
	}

	public String getHEADER()
	{
		return HEADER;
	}

	public void setHEADER(String hEADER)
	{
		HEADER = hEADER;
	}

	public String getTRNDATE()
	{
		return TRNDATE;
	}

	public void setTRNDATE(String tRNDATE)
	{
		TRNDATE = tRNDATE;
	}

	public String getFDPACN()
	{
		return FDPACN;
	}

	public void setFDPACN(String fDPACN)
	{
		FDPACN = fDPACN;
	}

	public String getTERM()
	{
		return TERM;
	}

	public void setTERM(String tERM)
	{
		TERM = tERM;
	}

	// public String getREC() {
	// return REC;
	// }
	// public void setREC(String rEC) {
	// REC = rEC;
	// }
	public String getCMQTIME()
	{
		return CMQTIME;
	}

	public void setCMQTIME(String cMQTIME)
	{
		CMQTIME = cMQTIME;
	}

	public String get__OCCURS()
	{
		return __OCCURS;
	}

	public void set__OCCURS(String __OCCURS)
	{
		this.__OCCURS = __OCCURS;
	}

}
