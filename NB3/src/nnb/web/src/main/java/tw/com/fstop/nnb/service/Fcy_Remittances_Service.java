package tw.com.fstop.nnb.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import fstop.orm.po.ADMMSGCODE;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmMsgCodeDao;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.StrUtil;

/**
 * 
 * 功能說明 :外匯匯出匯款
 *
 */
@Service
public class Fcy_Remittances_Service extends Base_Service
{
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	I18n i18n;
	
    @Autowired
    private AdmMsgCodeDao admMsgCodeDao;
	
	/**
	 * 取得轉出轉入帳號
	 * 
	 * @param cusidn
	 * @return
	 */
	public BaseResult getOutAcnoAndAgreeAcno(String cusidn)
	{
		BaseResult bs = new BaseResult();
		BaseResult bsN920 = new BaseResult();
		List<Map<String, String>> inAcnoList = new ArrayList<>();
		List<Map<String, String>> outAcnoList = new ArrayList<>();
		ADMMSGCODE admmsgcode = null;
		try
		{
	         Locale currentLocale = LocaleContextHolder.getLocale();
	         log.debug("errorMsg.locale >> {}" , currentLocale);
	         String locale = currentLocale.toString();
			// CALL N920電文 取得轉出帳號
			bsN920 = N920_REST(cusidn, Fcy_Remittances_Service.FX_OUT_ACNO);
			
			if ( null == bsN920  || !bsN920.getResult()) {
				log.error( " N920 Error ");
				return bsN920;
			}
			
			// 後製把REC 裡面的資料轉成下拉選單所要的格式
			if (bsN920 != null && bsN920.getResult())
			{
				Map<String, Object> callDataN920 = (Map<String, Object>) bsN920.getData();

				String str_CUSTIDF = (String) callDataN920.get("CUSTIDF");
				String str_CUSTYPE = "";
				if (str_CUSTIDF.equals("1"))
				{
					str_CUSTYPE = "1";
				}
				else if (str_CUSTIDF.equals("2"))
				{
					str_CUSTYPE = "2";
				}
				else if (str_CUSTIDF.equals("3") || str_CUSTIDF.equals("5"))
				{
					str_CUSTYPE = "3";
				}
				else if (str_CUSTIDF.equals("4") || str_CUSTIDF.equals("6"))
				{
					str_CUSTYPE = "4";
				}
				String name = (String) callDataN920.get("NAME");
				// OutAcno
				outAcnoList = (List<Map<String, String>>) callDataN920.get("REC");

				if (outAcnoList.size() == 0)
				{
                    String msgCode = "Z999";
                    String message = "";
                    if (admMsgCodeDao.hasMsg(msgCode, null)) {
                        admmsgcode = admMsgCodeDao.get(ADMMSGCODE.class, msgCode);
                        switch (locale) {
                            case "en":
                                message = admmsgcode.getADMSGOUTENG();
                                break;
                            case "zh_TW":
                                message = admmsgcode.getADMSGOUT();
                                break;
                            case "zh_CN":
                                message = admmsgcode.getADMSGOUTCHS();
                                break;
                        }
                    }
                    bs.setMessage(msgCode, message);//轉出帳號無任何資料
					bs.setResult(Boolean.FALSE);
					log.info("call bsN920 bs erro >>>", bsN920.getData());

				}
				else
				{
					// 取得轉入帳號
					bs = F031_REST(cusidn);
					if (bs != null && bs.getResult())
					{
						Map<String, Object> callDataF031 = (Map) bs.getData();
						ArrayList<Map<String, String>> callF031REC = (ArrayList<Map<String, String>>) callDataF031.get("REC");
						for (Map<String, String> map : callF031REC)
						{
							// 檢查 F031 下行電文中,若有非本人之明細則不帶出
							if (cusidn.equals(map.get("CUSTID")))
							{
								inAcnoList.add(map);
							}
							else
							{

							}
						}
						// 判斷收款帳號
						if (inAcnoList != null && inAcnoList.size() > 0)
						{

							bs.addData("REC", inAcnoList);
							bs.addData("outAcnoList", outAcnoList);
							bs.addData("str_CUSTYPE", str_CUSTYPE);
							bs.addData("NAME", name);
							//現在日期
							bs.addData("nowDate", new DateTime().toString("yyyy/MM/dd"));
							//明天日期
							bs.addData("tmr", new DateTime().plusDays(1).toString("yyyy/MM/dd"));
							// 可預約次日起一年內之交易
							String nextYearDay = new DateTime().plusYears(1).toString("yyyy/MM/dd");
							bs.addData("nextYearDay", nextYearDay);
						}
						else
						{
                            String msgCode = "W085";
                            String message = "";
                            if (admMsgCodeDao.hasMsg(msgCode, null)) {
                                admmsgcode = admMsgCodeDao.get(ADMMSGCODE.class, msgCode);
                                switch (locale) {
                                    case "en":
                                        message = admmsgcode.getADMSGOUTENG();
                                        break;
                                    case "zh_TW":
                                        message = admmsgcode.getADMSGOUT();
                                        break;
                                    case "zh_CN":
                                        message = admmsgcode.getADMSGOUTCHS();
                                        break;
                                }
                            }

                            bs.setMessage(msgCode, message);// 收款帳號無任何資料
							bs.setResult(Boolean.FALSE);
							log.info("call F031_REST bs erro >>>", bs.getData());
						}
					}
				}

			}

			log.trace(" Fcy_Remittances_Service getOutAcnoAndAgreeAcno getData()>> {}", bs.getData());
		}
		catch (Exception e)
		{
			log.error("Fcy_Remittances_Service{}", e);
		}
		return bs;
	}

	/**
	 * 匯出匯款第2頁邏輯處理
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult f_outward_remittances_step2(Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			bs.setData(reqParam);
			bs.setResult(true);
		}
		catch (Exception e)
		{
			log.error("Fcy_Remittances_Service{}", e);
		}
		return bs;
	}

	/**
	 * 匯出匯款第2頁邏輯處理(預約)
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult f_outward_remittances_step2_S(Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			bs.setData(reqParam);
			bs.setResult(true);
		}
		catch (Exception e)
		{
			log.error("Fcy_Remittances_Service{}", e);
		}
		return bs;
	}

	/**
	 * 匯出匯款第3頁邏輯處理
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult f_outward_remittances_step3(String cusidn, Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			reqParam.put("CUSIDN", cusidn);
			bs = F002R_REST(reqParam);
			String PAYCCY = reqParam.get("PAYCCY");
			String REMITCY = reqParam.get("REMITCY");
			// 處理回應電文
			Map<String, Object> callDataF002 = (Map<String, Object>) bs.getData();
			// 付款金額
			int i_Digit = 0;
			String str_curamt = String.valueOf(callDataF002.get("CURAMT"));
			if (str_curamt.indexOf(".") != -1)
			{
				i_Digit = 2;
			}
			str_curamt = NumericUtil.formatNumberStringByCcy(str_curamt, i_Digit, PAYCCY);
			// 收款金額
			i_Digit = 0;
			String str_atramt = String.valueOf(callDataF002.get("ATRAMT"));
			if (str_atramt.indexOf(".") != -1)
			{
				i_Digit = 2;

			}
			str_atramt = NumericUtil.formatNumberStringByCcy(str_atramt, i_Digit, REMITCY);
			callDataF002.put("str_curamt", str_curamt);// 付款金額
			callDataF002.put("str_atramt", str_atramt);// 收款金額
			// 將傳入的參數加入回應的Map
			callDataF002.putAll(reqParam);

		}
		catch (Exception e)
		{
			log.error("Fcy_Remittances_Service{}", e);
		}
		return bs;
	}

	/**
	 * 匯出匯款確認頁邏輯處理
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult f_outward_remittances_confirm(Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			bs.setData(reqParam);
			bs.setResult(true);
		}
		catch (Exception e)
		{
			log.error("Fcy_Remittances_Service{}", e);
		}
		return bs;
	}

	/**
	 * show transfer_data 加工
	 * 
	 * @param transfer_data
	 * @return 新增<br>
	 *         無換行的收款銀行資料名稱/地址 :fxRcvBkaddr_download<br>
	 *         無換行的收款人資料名稱/地址:fxRcvBkaddr_download<br>
	 *         有換行的摘要內容:str_MailMemo<br>
	 *         有換行的匯款附言 :str_Memo1
	 */
	public Map<String, String> transfer_data_Retouch(Map<String, String> transfer_data)
	{

		// 收款銀行資料名稱/地址 去除換行符號,並新增變數 fxRcvBkaddr_download
		String fxRcvBkaddr_download = transfer_data.get("FXRCVBKADDR") == null ? ""
				: transfer_data.get("FXRCVBKADDR").replaceAll("\r|\n", " ");
		transfer_data.put("fxRcvBkaddr_download", fxRcvBkaddr_download);
		// 收款人資料名稱/地址 去除換行符號,並新增變數 fxrcvadd_download
		String fxRcvAdd_download = transfer_data.get("FXRCVADDR") == null ? ""
				: transfer_data.get("FXRCVADDR").replaceAll("\r|\n", " ");
		transfer_data.put("fxRcvAdd_download", fxRcvAdd_download);

		// ****摘要內容:
		String str_MailMemo = transfer_data.get("CMMAILMEMO");
		StringBuffer buffer = new StringBuffer();
		if (str_MailMemo.length() <= 35)
		{
			buffer.append(str_MailMemo);
		}
		else if (str_MailMemo.length() > 35 && str_MailMemo.length() <= 70)
		{
			buffer.append(str_MailMemo.substring(0, 35));
			buffer.append("<br>");
			buffer.append(str_MailMemo.substring(35));
		}
		else if (str_MailMemo.length() > 70 && str_MailMemo.length() <= 105)
		{
			buffer.append(str_MailMemo.substring(0, 35));
			buffer.append("<br>");
			buffer.append(str_MailMemo.substring(35, 70));
			buffer.append("<br>");
			buffer.append(str_MailMemo.substring(70));
		}
		// 摘要內容<br>
		transfer_data.put("str_MailMemo", buffer.toString());
		// ****匯款附言 :
		String str_Memo1 = transfer_data.get("MEMO1");
		StringBuffer buf = new StringBuffer();
		if (str_Memo1.length() <= 35)
		{
			buf.append(str_Memo1);
		}
		else if (str_Memo1.length() > 35 && str_Memo1.length() <= 70)
		{
			buf.append(str_Memo1.substring(0, 35));
			buf.append("<br>");
			buf.append(str_Memo1.substring(35));
		}
		else if (str_Memo1.length() > 70 && str_Memo1.length() <= 105)
		{
			buf.append(str_Memo1.substring(0, 35));
			buf.append("<br>");
			buf.append(str_Memo1.substring(35, 70));
			buf.append("<br>");
			buf.append(str_Memo1.substring(70));
		}

		// 匯款附言 有<br>:
		transfer_data.put("str_Memo1", buf.toString());
		return transfer_data;

	}

	/**
	 * 匯出匯款結果頁輯處理
	 * 
	 * @param cusidn
	 * @param transfer_data
	 * @param reqParam
	 * @return
	 */
	public BaseResult f_outward_remittances_result(String cusidn, Map<String, String> transfer_data,
			Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			// 取得交易代號 TXID == F002時為及時 ;F002S 為預約
			String TXID = reqParam.get("TXID");

			transfer_data.put("ADOPID", "F002");
			//txnLog 所需的欄位
			reqParam.put("ADOPID", "N178");
			reqParam.put("ADTXACNO", reqParam.get("BENACC"));// 轉入帳號 
//			reqParam.put("ADSVBH", "050");// 轉入服務銀行 
			reqParam.put("ADTXAMT", reqParam.get("CURAMT"));// 轉出金額
			reqParam.put("ADCURRENCY", reqParam.get("REMITCY"));// 轉出幣別 
			reqParam.put("ADAGREEF", "1");// 轉入帳號約定或非約定註記>>0:非約定，1:約定//固定為 1：約定
			
			transfer_data.put("CUSIDN", cusidn);
			transfer_data.put("nowDate", new DateTime().toString("yyyy/MM/dd HH:mm:ss"));
			this.transfer_data_Retouch(transfer_data);
			reqParam.putAll(transfer_data);
			// F002T
			if ("F002T".equals(TXID))
			{
				bs = F002T_REST(reqParam);
				// 後製把 裡面的資料轉成所要的格式
				if (bs != null && bs.getResult())
				{
					Map<String, Object> callDataF002T = (Map<String, Object>) bs.getData();
					// 英文姓名
					StringBuffer sb = new StringBuffer();
					StringBuffer enName = new StringBuffer();
					sb.append(callDataF002T.get("M150AD1"));
					if (!String.valueOf(callDataF002T.get("M150AD2")).trim().equals(""))
					{
						sb.append("<br>");
						sb.append(callDataF002T.get("M150AD2"));
						enName.append(callDataF002T.get("M150AD2"));
					}

					if (!String.valueOf(callDataF002T.get("M150AD3")).trim().equals(""))
					{
						sb.append("<br>");
						sb.append(callDataF002T.get("M150AD3"));
						enName.append(callDataF002T.get("M150AD3"));
					}

					if (!String.valueOf(callDataF002T.get("M150AD4")).trim().equals(""))
					{
						sb.append("<br>");
						sb.append(callDataF002T.get("M150AD4"));
						enName.append(callDataF002T.get("M150AD4"));
					}
					// 英文姓名<br>
					bs.addData("ENNAMEbr", sb);
					// 英文姓名
					bs.addData("ENNAME", enName);
					// 匯率
					String str_Rate = String.valueOf(callDataF002T.get("EXRATE"));
					if (str_Rate == null)
					{
						str_Rate = "";
					}
					else if (str_Rate != null && str_Rate.replace("0", "").equals(""))
					{
						str_Rate = "";
					}
					else
					{
						str_Rate = NumericUtil.formatNumberString(String.valueOf(callDataF002T.get("EXRATE")), 8);
					}
					// 付款金額
					String str_orgAmt = String.valueOf(callDataF002T.get("ORGAMT"));
					// 付款幣別
					String str_orgCcy = String.valueOf(callDataF002T.get("ORGCCY"));
					if (str_orgCcy.equals("TWD") || str_orgCcy.equals("JPY"))
					{
						str_orgAmt = str_orgAmt.substring(0, str_orgAmt.length() - 2);
					}
					str_orgAmt = NumericUtil.formatNumberStringByCcy(str_orgAmt, 2, str_orgCcy);
					// 收款金額
					String str_pmtAmt = String.valueOf(callDataF002T.get("PMTAMT"));
					// 收款幣別
					String str_pmtCcy = String.valueOf(callDataF002T.get("PMTCCY"));
					if (str_pmtCcy.equals("TWD") || str_pmtCcy.equals("JPY"))
					{
						str_pmtAmt = str_pmtAmt.substring(0, str_pmtAmt.length() - 2);
					}
					str_pmtAmt = NumericUtil.formatNumberStringByCcy(str_pmtAmt, 2, str_pmtCcy);
					// 手續費
					String str_commAmt = String.valueOf(callDataF002T.get("COMMAMT"));
					// 手續費幣別
					String str_commCcy = reqParam.get("COMMCCY");
					if (str_commCcy.equals("TWD") || str_commCcy.equals("JPY"))
					{
						str_commAmt = str_commAmt.substring(0, str_commAmt.length() - 2);
					}
					str_commAmt = NumericUtil.formatNumberStringByCcy(str_commAmt, 2, str_commCcy);
					// 郵電費
					String str_cabAmt = String.valueOf(callDataF002T.get("CABCHG"));
					if (str_commCcy.equals("TWD") || str_commCcy.equals("JPY"))
					{
						str_cabAmt = str_cabAmt.substring(0, str_cabAmt.length() - 2);
					}
					str_cabAmt = NumericUtil.formatNumberStringByCcy(str_cabAmt, 2, str_commCcy);
					// 國外費用
					String str_ourAmt = String.valueOf(callDataF002T.get("OURCHG"));
					if (str_commCcy.equals("TWD") || str_commCcy.equals("JPY"))
					{
						str_ourAmt = str_ourAmt.substring(0, str_ourAmt.length() - 2);
					}
					str_ourAmt = NumericUtil.formatNumberStringByCcy(str_ourAmt, 2, str_commCcy);

					bs.addData("str_Rate", str_Rate);
					bs.addData("str_orgAmt", str_orgAmt);
					bs.addData("str_pmtAmt", str_pmtAmt);
					bs.addData("str_commAmt", str_commAmt);
					bs.addData("str_cabAmt", str_cabAmt);
					bs.addData("str_ourAmt", str_ourAmt);
					bs.addData("jspTitle", i18n.getMsg("LB.W0286"));//外匯匯出匯款
					bs.addAllData(transfer_data);
				}
			}
			// F002S
			else if ("F002S".equals(TXID))
			{
				reqParam.put("ADREQTYPE", "B");
				bs = F002S_REST(reqParam);
				bs.addAllData(transfer_data);
			}

			log.debug("f_outward_remittances_result bs data {}", bs.getData());

		}
		catch (Exception e)
		{
			log.error("Fcy_Remittances_Service{}", e);
		}
		return bs;
	}

	/**
	 * 檢查TxToken
	 * 
	 * @param pageTxToken 頁面TxToken
	 * @param SessionFinshToken
	 * @return false 表示有問題
	 */
	public BaseResult validateToken(String pageTxToken, String SessionFinshToken)
	{
		BaseResult bs = new BaseResult();
		log.debug(ESAPIUtil.vaildLog("validateToken pageTxToken:{}"+pageTxToken));
		log.debug(ESAPIUtil.vaildLog("validateToken SessionFinshToken:{}"+ SessionFinshToken));
		try
		{
			if (pageTxToken == null)
			{
				log.debug(" reqTxToken==null");
				bs.setMessage("FE002", "reqTxToken空白");
				log.debug("validateToken getResult:{}", bs.getMsgCode());
				log.debug("validateToken getResult:{}", bs.getMessage());
			}
			else if (StrUtil.isNotEmpty(SessionFinshToken) && SessionFinshToken.equals(pageTxToken))
			{
				log.debug("validateToken2 getResult:{}", bs.getMsgCode());
				log.debug("validateToken2 getResult:{}", bs.getMessage());
				log.debug(" pagToken SessionFinshToken &&  一樣 重複交易");
				bs.setMessage("FE002", "重複交易");
			}
			else
			{
				bs.setResult(true);
			}
		}
		catch (Exception e)
		{
			log.error("getTransferToken Error", e);
		}
		return bs;
	}

}
