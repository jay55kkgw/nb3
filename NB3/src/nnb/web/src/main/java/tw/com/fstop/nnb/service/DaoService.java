package tw.com.fstop.nnb.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.hibernate.ObjectNotFoundException;
import org.hsqldb.lib.StringUtil;
import org.joda.time.DateTime;
import org.joda.time.base.AbstractDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fstop.orm.po.ADMBANK;
import fstop.orm.po.ADMBHCONTACT;
import fstop.orm.po.ADMCURRENCY;
import fstop.orm.po.ADMMSGCODE;
import fstop.orm.po.ADMMYREMITMENU;
import fstop.orm.po.ADMREMITMENU;
import fstop.orm.po.OLD_TXNADDRESSBOOK;
import fstop.orm.po.OLD_TXNTRACCSET;
import fstop.orm.po.OLD_TXNUSER;
import fstop.orm.po.SYSPARAMDATA;
import fstop.orm.po.TXNADDRESSBOOK;
import fstop.orm.po.TXNCUSINVATTRHIST;
import fstop.orm.po.TXNFUNDDATA;
import fstop.orm.po.TXNFXRECORD;
import fstop.orm.po.TXNTRACCSET;
import fstop.orm.po.TXNUSER;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.common.ResultCode;
import tw.com.fstop.nnb.rest.bean.N920_REST_RQ;
import tw.com.fstop.nnb.rest.bean.N920_REST_RS;
import tw.com.fstop.nnb.rest.bean.N921_REST_RQ;
import tw.com.fstop.nnb.rest.bean.N921_REST_RS;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmBankDao;
import tw.com.fstop.tbb.nnb.dao.AdmBhContactDao;
import tw.com.fstop.tbb.nnb.dao.AdmCardDataDao;
import tw.com.fstop.tbb.nnb.dao.AdmCurrencyDao;
import tw.com.fstop.tbb.nnb.dao.AdmHolidayDao;
import tw.com.fstop.tbb.nnb.dao.AdmMsgCodeDao;
import tw.com.fstop.tbb.nnb.dao.AdmMyRemitMenuDao;
import tw.com.fstop.tbb.nnb.dao.AdmRemitMenuDao;
import tw.com.fstop.tbb.nnb.dao.Old_TxnAddressBookDao;
import tw.com.fstop.tbb.nnb.dao.Old_TxnTrAccSetDao;
import tw.com.fstop.tbb.nnb.dao.Old_TxnUserDao;
import tw.com.fstop.tbb.nnb.dao.SysParamDataDao;
import tw.com.fstop.tbb.nnb.dao.TxnAddressBookDao;
import tw.com.fstop.tbb.nnb.dao.TxnCusInvAttrHistDao;
import tw.com.fstop.tbb.nnb.dao.TxnFundDataDao;
import tw.com.fstop.tbb.nnb.dao.TxnFxRecordDao;
import tw.com.fstop.tbb.nnb.dao.TxnRiskUsrDao;
import tw.com.fstop.tbb.nnb.dao.TxnTrAccSetDao;
import tw.com.fstop.tbb.nnb.dao.TxnUserDao;
import tw.com.fstop.tbb.nnb.util.StrUtils;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.ObjectConvertUtil;
import tw.com.fstop.util.StrUtil;

/**
 * 
 * @author Ben 共用DAO SERVICE
 *
 */
@Service
public class DaoService extends Base_Service {
	@Autowired
	private TxnAddressBookDao txnAddressBookDao;

	@Autowired
	private TxnUserDao txnUserDao;

	@Autowired
	private AdmBankDao admBankDao;

	@Autowired
	private AdmBhContactDao admBhContactDao;

	@Autowired
	private AdmCardDataDao admCardDataDao;

	@Autowired
	private TxnTrAccSetDao txnTrAccSetDao;

	@Autowired
	private AdmMsgCodeDao admMsgCodeDao;

	@Autowired
	private AdmCurrencyDao admCurrencyDao;

	@Autowired
	private AdmRemitMenuDao admRemitMenuDao;

	@Autowired
	private AdmMyRemitMenuDao admMyRemitMenuDao;

	@Autowired
	private SysParamDataDao sysParamDataDao;

	@Autowired
	private AdmHolidayDao admholidayDao;

	@Autowired
	private TxnFxRecordDao txnFxRecordDa;

	@Autowired
	private TxnFundDataDao txnFundDataDao;

	@Autowired
	private TxnCusInvAttrHistDao txnCusInvAttrHistDao;

	@Autowired
	private I18n i18n;

	@Autowired
	private String isSyncNNB;
	@Autowired
	private Old_TxnUserDao old_txnUserDao;
	@Autowired
	private Old_TxnTrAccSetDao old_txntraccsetDao;

	@Autowired
	private Old_TxnAddressBookDao old_txnAddressBookDao;

	@Autowired
	private TxnRiskUsrDao txnRiskUsrDao;

	public String getAPmail() {
		SYSPARAMDATA po = sysParamDataDao.get(SYSPARAMDATA.class, "NBSYS");
		return po.getADAPMAIL();
	}

	/**
	 * 取得台企銀行的分行的名稱
	 * 
	 * @param brhcod
	 * @return
	 */
	public String getBankName(String adbranchid) {
		String bankName = "";
		try {
			// i18n
			Locale currentLocale = LocaleContextHolder.getLocale();
			log.debug("admbanl.locale >> {}", currentLocale);
			String locale = currentLocale.toString();
			// 取得po
			ADMBHCONTACT admbhcontact = admBhContactDao.findByAdbranchid(adbranchid);
			switch (locale) {
			case "en":
				bankName = admbhcontact.getADBRANENGNAME();
				break;
			case "zh_CN":
				bankName = admbhcontact.getADBRANCHSNAME();
				break;
			default:
				bankName = admbhcontact.getADBRANCHNAME();
				break;
			}
		} catch (Exception e) {
			log.debug("getBankName err {} ", e);
			bankName = "";
		}

		return bankName;
	}

	public boolean chkContains(String chkNo, String DBStr) {
		int count = 0;
		int chNo = 0;
		String str = "";
		while (true) {
			chNo = DBStr.indexOf(',', count);
			if (chNo == -1) {
				str = DBStr.substring(count, DBStr.length());
				if (str.equals(chkNo))
					return true;
				else
					break;
			}
			str = DBStr.substring(count, chNo);
			if (str.equals(chkNo))
				return true;
			count = chNo + 1;
		}
		return false;
	}

	/**
	 * 修改TxUser
	 * 
	 * @param params
	 * @return
	 */
	public Map<String, String> txnUserModify(Map<String, String> params) {
		TXNUSER txnUser = null;
		String column = params.get("COLUMN");
		if ("QUERY".equalsIgnoreCase(params.get("EXECUTEFUNCTION"))) {
			String dpuserid = params.get("DPUSERID");
			txnUser = txnUserDao.findByUserId(dpuserid).get(0);
			if ("DPOVERVIEW".equals(column))
				params.put("DPOVERVIEW", txnUser.getDPOVERVIEW());
			if ("DPMENU".equals(column))
				params.put("DPMENU", txnUser.getDPMENU());
		}
		if ("UPDATE".equalsIgnoreCase(params.get("EXECUTEFUNCTION"))) {
			String dpuserid = params.get("DPUSERID");
			txnUser = txnUserDao.findByUserId(dpuserid).get(0);
			if ("DPOVERVIEW".equals(column)) {
				txnUser.setDPOVERVIEW(params.get("DPOVERVIEW"));
				log.debug(ESAPIUtil.vaildLog("txnUser >> {}" + txnUser.getADBRANCHID()));
				log.debug(ESAPIUtil.vaildLog("txnUser >> {}" + txnUser.getDPOVERVIEW()));
				txnUserDao.update(txnUser);
				params.put("DPOVERVIEW", txnUser.getDPOVERVIEW());
			}
			if ("DPMENU".equals(column)) {
				txnUser.setDPMENU(params.get("DPMENU"));
				txnUserDao.update(txnUser);
				params.put("DPMENU", txnUser.getDPMENU());
			}
			if ("NOTIFYDATA".equals(column)) {
				txnUser.setDPNOTIFY(params.get(column));
				txnUserDao.update(txnUser);
				params.put("NOTIFYDATA", txnUser.getDPNOTIFY());
			}

			if ("Y".equalsIgnoreCase(isSyncNNB)) {
				sync_Old_TxnUserModify(txnUser);
			}

		}
		return params;
	}

	/**
	 * 同步NNB2.5
	 * 
	 * @param params
	 * @return
	 */
	public void sync_Old_TxnUserModify(TXNUSER po) {
		OLD_TXNUSER old_txnUser = null;
		Integer schcount = 0;
		String schdate = "";
		try {
			log.info("sync_Old_TxnUserModify...");
			old_txnUser = old_txnUserDao.get(OLD_TXNUSER.class, po.getDPSUERID());
			if (old_txnUser != null) {
				schcount = old_txnUser.getSCHCOUNT(); // 如果nb2.5 有資料以其為主
				schdate = old_txnUser.getSCHCNTDATE();
				old_txnUser = null; // 清空
			}

			old_txnUser = CodeUtil.objectCovert(OLD_TXNUSER.class, po);
//			log.debug("OLD_TXNUSER from TXNUSER data >>{}", CodeUtil.toJson(old_txnUser));
			old_txnUser.setSCHCOUNT(schcount);
			old_txnUser.setSCHCNTDATE(schdate);
			old_txnUserDao.saveOrUpdate(old_txnUser);

//			String dpuserid = params.get("DPUSERID");
//			List<TXNUSER> list = txnUserDao.findByUserId(dpuserid);
//			if(list==null || list.isEmpty()) {
//				log.warn("TXNUSER is Empty  by userid = {}" , dpuserid);
//			}else {
//				old_txnUser = CodeUtil.objectCovert(OLD_TXNUSER.class, po);
//				log.debug("OLD_TXNUSER from TXNUSER data >>", CodeUtil.toJson(old_txnUser));
//				old_txnUserDao.saveOrUpdate(old_txnUser);
//			}

		} catch (DataAccessException e) {
			log.error("sync_Old_TxnUserModify.ERROR>>{}", e);
		}
	}

	/**
	 * 查詢USER 資料如果不存在就新增一筆
	 * 
	 * @param DPSUERID
	 * @return
	 */
	public TXNUSER chkRecord(String DPSUERID) {

		TXNUSER user = null;
		try {
			List<TXNUSER> txnusers = txnUserDao.findByUserId(DPSUERID);
			if (txnusers != null && !txnusers.isEmpty()) {
				user = txnusers.get(0);
				return user;
			}
			log.debug("txnusers is null ");
			user = new TXNUSER();
			user.setDPSUERID(DPSUERID);
			user.setSCHCOUNT(new Integer(0));
			user.setASKERRTIMES(new Integer(0));
			user.setCCARDFLAG("0");
			txnUserDao.save(user);

			// 同步NB2.5
			try {
				if ("Y".equalsIgnoreCase(isSyncNNB)) {
					OLD_TXNUSER olduser = new OLD_TXNUSER();
					olduser.setDPSUERID(DPSUERID);
					olduser.setSCHCOUNT(new Integer(0));
					olduser.setASKERRTIMES(new Integer(0));
					olduser.setCCARDFLAG("0");
					old_txnUserDao.save(olduser);
				}
			} catch (Throwable e) {
				log.error("DaoService.chkRecord.isSyncNNB.ERROR>>{}", e.toString());
			}
		} catch (Throwable e) {
			log.error("DaoService.chkRecord.ERROR>>{}", e);
		}

		return user;
	}

	/**
	 * 查詢USER 資料如果不存在就新增一筆
	 * 
	 * @param DPSUERID
	 * @return
	 */
	public TXNUSER chkRecord(String DPSUERID, String DPUSERNAME) {
		String dfNotify = "16,17,18,19";// 黃金存摺相關的要預設啟用通知
		TXNUSER user = null;
		try {
			List<TXNUSER> txnusers = txnUserDao.findByUserId(DPSUERID);
			if (txnusers != null && !txnusers.isEmpty()) {
				user = txnusers.get(0);
				user.setDPUSERNAME(DPUSERNAME);
				txnUserDao.update(user);

				try {
					if ("Y".equalsIgnoreCase(isSyncNNB)) {
						OLD_TXNUSER old = old_txnUserDao.get(OLD_TXNUSER.class, user.getDPSUERID());
						if (null != old) {
							old.setDPUSERNAME(DPUSERNAME);
						}
						old_txnUserDao.update(old);
					}
				} catch (Throwable e) {
					log.error("DaoService.chkRecord.isSyncNNB.update OLD_TXNUSER fail...", e.toString());
				}

				return user;
			}
			log.debug("txnusers is null ");
			user = new TXNUSER();
			user.setDPSUERID(DPSUERID);
			user.setSCHCOUNT(new Integer(0));
			user.setASKERRTIMES(new Integer(0));
			user.setDPUSERNAME(DPUSERNAME);
			user.setDPNOTIFY(dfNotify);
			user.setCCARDFLAG("0");
			txnUserDao.save(user);

			// 同步NB2.5
			try {
				if ("Y".equalsIgnoreCase(isSyncNNB)) {
					OLD_TXNUSER olduser = new OLD_TXNUSER();
					olduser.setDPSUERID(DPSUERID);
					olduser.setSCHCOUNT(new Integer(0));
					olduser.setASKERRTIMES(new Integer(0));
					olduser.setDPUSERNAME(DPUSERNAME);
					olduser.setDPNOTIFY(dfNotify);
					olduser.setCCARDFLAG("0");
					old_txnUserDao.save(olduser);
				}
			} catch (Throwable e) {
				log.error("DaoService.chkRecord.isSyncNNB.ERROR>>{}", e.toString());
			}
		} catch (Throwable e) {
			log.error("DaoService.chkRecord.ERROR>>{}", e);
		}

		return user;
	}

	/**
	 * 查詢USER 資料如果不存在就新增一筆
	 * 
	 * @param DPSUERID
	 * @return
	 */
	public TXNUSER loginChk(String DPSUERID, String MAILADDR) {
		String dfNotify = "16,17,18,19";// 黃金存摺相關的要預設啟用通知
		TXNUSER user = null;
		OLD_TXNUSER olduser = null;
		try {
			List<TXNUSER> txnusers = txnUserDao.findByUserId(DPSUERID);
			if (txnusers != null && !txnusers.isEmpty()) {
				user = txnusers.get(0);
				user.setDPMYEMAIL(MAILADDR);
				txnUserDao.update(user);
				try {
					if ("Y".equalsIgnoreCase(isSyncNNB)) {
						log.debug("loginChk OLD_TXNUSER update ");
//						olduser = CodeUtil.objectCovert(OLD_TXNUSER.class, user);
						olduser = old_txnUserDao.get(OLD_TXNUSER.class, DPSUERID);
						log.debug(ESAPIUtil.vaildLog("olduser>>" + CodeUtil.toJson(olduser)));
						if (null != olduser) {
							olduser.setDPMYEMAIL(MAILADDR);
							old_txnUserDao.saveOrUpdate(olduser);
						}
					}
				} catch (Throwable e) {
					log.error("DaoService.loginChk.isSyncNNB.saveOrUpdate.ERROR>>{}", e.toString());
				}

				return user;
			}
			log.debug("txnusers is null ");
			user = new TXNUSER();
			user.setDPSUERID(DPSUERID);
			user.setSCHCOUNT(new Integer(0));
			user.setASKERRTIMES(new Integer(0));
			user.setDPMYEMAIL(MAILADDR);
			user.setDPNOTIFY(dfNotify);
			user.setCCARDFLAG("0");
			txnUserDao.save(user);

			// 同步NB2.5
			try {
				if ("Y".equalsIgnoreCase(isSyncNNB)) {
					olduser = new OLD_TXNUSER();
					olduser.setDPSUERID(DPSUERID);
					olduser.setSCHCOUNT(new Integer(0));
					olduser.setASKERRTIMES(new Integer(0));
					olduser.setDPMYEMAIL(MAILADDR);
					olduser.setDPNOTIFY(dfNotify);
					olduser.setCCARDFLAG("0");
					old_txnUserDao.save(olduser);
				}
			} catch (Throwable e) {
				log.error("DaoService.chkRecord.isSyncNNB.ERROR>>{}", e.toString());
			}
		} catch (Throwable e) {
			log.error("DaoService.chkRecord.ERROR>>{}", e);
		}

		return user;
	}

	/**
	 * 
	 * @param DPSUERID
	 * @param MAILADDR
	 * @return
	 */
	public TXNUSER updateMail(String DPSUERID, String MAILADDR) {
		String dfNotify = "16,17,18,19";// 黃金存摺相關的要預設啟用通知
		TXNUSER user = null;
		OLD_TXNUSER olduser = null;
		try {
			List<TXNUSER> txnusers = txnUserDao.findByUserId(DPSUERID);
			if (txnusers != null && !txnusers.isEmpty()) {
				user = txnusers.get(0);
				user.setDPMYEMAIL(MAILADDR);
				txnUserDao.update(user);
				try {
					if ("Y".equalsIgnoreCase(isSyncNNB)) {
						log.debug("loginChk OLD_TXNUSER update ");
//						olduser = CodeUtil.objectCovert(OLD_TXNUSER.class, user);
						olduser = old_txnUserDao.get(OLD_TXNUSER.class, DPSUERID);
						if (null != olduser) {
							olduser.setDPMYEMAIL(MAILADDR);
							old_txnUserDao.saveOrUpdate(olduser);
						}
					}
				} catch (Throwable e) {
					log.error("DaoService.loginChk.isSyncNNB.saveOrUpdate.ERROR>>{}", e);
				}

				return user;
			}
			log.debug("txnusers is null ");
			user = new TXNUSER();
			user.setDPSUERID(DPSUERID);
			user.setSCHCOUNT(new Integer(0));
			user.setASKERRTIMES(new Integer(0));
			user.setDPMYEMAIL(MAILADDR);
			user.setDPNOTIFY(dfNotify);
			user.setCCARDFLAG("0");
			txnUserDao.save(user);

			// 同步NB2.5
			try {
				if ("Y".equalsIgnoreCase(isSyncNNB)) {
					olduser = new OLD_TXNUSER();
					olduser.setDPSUERID(DPSUERID);
					olduser.setSCHCOUNT(new Integer(0));
					olduser.setASKERRTIMES(new Integer(0));
					olduser.setDPMYEMAIL(MAILADDR);
					olduser.setDPNOTIFY(dfNotify);
					olduser.setCCARDFLAG("0");
					old_txnUserDao.save(olduser);
				}
			} catch (Throwable e) {
				log.error("DaoService.chkRecord.isSyncNNB.ERROR>>{}", e);
			}
		} catch (Throwable e) {
			log.error("DaoService.chkRecord.ERROR>>{}", e);
		}

		return user;
	}

	/**
	 * 根據訊息代碼取得訊息
	 * 
	 * @param msgCode
	 * @return
	 */
	public String getMessageByMsgCode(String msgCode) {
		String message = "";
		ADMMSGCODE po = null;
		try {
			// 如果 大於6碼 或是前2位 =="FE" 則不去查DB
			if (msgCode.length() > 6 || "FE".equals(msgCode.substring(0, 2))) {
				log.debug("getMessageByMsgCode msgCode >>>{}", msgCode);
				msgCode = message;
			} else {
				po = admMsgCodeDao.get(ADMMSGCODE.class, msgCode);
				message = po.getADMSGIN();
			}
		} catch (Exception e) {
			log.error("getMessageByMsgCode error", e);
		}
		log.trace("message>>{}", message);
		return message;
	}

	public boolean isErrorCode(String topmsg) {
		boolean b_Result = false;

		try {
			String msg = getMessageByMsgCode(topmsg);

			if (StrUtil.isEmpty(msg)) {
				b_Result = false;
			} else {
				b_Result = true;
			}
		} catch (Exception e) {
		}

		return b_Result;
	}

	public BaseResult getDpBHNO_List(String cusidn) {
		log.trace("cusidn>>{} ", cusidn);
		BaseResult bs = null;
		List<Map<String, Object>> data = new ArrayList<>();
		List<String> bhnoList = new ArrayList<String>();
		try {
			bs = new BaseResult();
			List<ADMBANK> ADMBANK = getBankList();
			log.trace(ESAPIUtil.vaildLog("ADMBANK>>>" + ADMBANK));
			// 把REC轉出帳號轉成ajax需要的格式
			if (ADMBANK != null) {
				for (ADMBANK each : ADMBANK) {
					// 將po轉成map
					Map<String, Object> eachMap = ObjectConvertUtil.object2Map(each);
					log.trace(ESAPIUtil.vaildLog("eachMap>>" + eachMap));
					String BANKID = each.getADBANKID();
					// i18n
					Locale currentLocale = LocaleContextHolder.getLocale();
					log.debug("admbanl.locale >> {}", currentLocale);
					String locale = currentLocale.toString();
					String BANKNAME = "";
					switch (locale) {
					case "en":
						BANKNAME = each.getADBANKENGNAME();
						break;
					case "zh_CN":
						BANKNAME = each.getADBANKCHSNAME();
						break;
					default:
						BANKNAME = each.getADBANKNAME();
						break;

					}

					String DPBHNO = BANKID + "-" + BANKNAME;
					bhnoList.add(DPBHNO);
				}
				bs.setData(bhnoList);
				bs.setMessage("0", i18n.getMsg("LB.X1805"));// 查詢成功
				bs.setResult(true);
			}

		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("getDpBHNO_List error >> {}", e);
		}
		return bs;
	}

	/**
	 * 將外匯交易單據之網頁內容寫入 TXNFXRECORD
	 * 
	 * @param adtxno
	 * @param fxCertContent //交易單據網頁內容
	 */
	public void updateFxCert(String adtxno, String fxCertContent) {
		TXNFXRECORD fr = null;

		try {
			fr = txnFxRecordDa.findByADTXNO(adtxno);
		} catch (ObjectNotFoundException e) {
			fr = null;
		}

		if (fr != null) {
			fr.setFXCERT(fxCertContent); // 交易單據網頁內容
		}
		txnFxRecordDa.update(fr);
	}

	public List<ADMREMITMENU> getRemitMenu(String ADRMTTYPE, String ADLKINDID, String ADMKINDID, String ADRMTID,
			String custype) {
		List<ADMREMITMENU> result = new LinkedList<ADMREMITMENU>();
		// 由 DB 來
		if (StrUtils.isNotEmpty(ADLKINDID) || !("00".equals((ADMKINDID))) || StrUtils.isNotEmpty(ADRMTID)
				|| StrUtils.isNotEmpty(custype)) {
			result = admRemitMenuDao.getRemitMenu(ADRMTTYPE, ADLKINDID, ADMKINDID, ADRMTID, custype);

		} else {
			result = admRemitMenuDao.getAllAdlKind(ADRMTTYPE);
		}
		log.debug(ESAPIUtil.vaildLog("getRemitMenu.result in >>>{} " + CodeUtil.toJson(result)));

		// 取得現在語系
		Locale currentLocale = LocaleContextHolder.getLocale();
		log.debug("errorMsg.locale >> {}", currentLocale);
		String locale = currentLocale.toString();
		log.debug("getRemitMenu.locale2 >> {}", locale);
		// 切換項目語系
		for (ADMREMITMENU admremitmenu : result) {
			switch (locale) {
			case "en":
				admremitmenu.setADRMTITEM(admremitmenu.getADRMTENGITEM());// 項目(英)
				admremitmenu.setADRMTDESC(admremitmenu.getADRMTENGDESC());// 說明(英)
				break;
			case "zh_TW":
				break;
			case "zh_CN":
				admremitmenu.setADRMTITEM(admremitmenu.getADRMTCHSITEM());// 項目(英)
				admremitmenu.setADRMTDESC(admremitmenu.getADRMTCHSDESC());// 說明(英)
				break;
			}
		}

		log.debug(ESAPIUtil.vaildLog("getRemitMenu.result out>>>{} " + CodeUtil.toJson(result)));

		return result;
	}

	// 找出這個人的所有 REMITMENU
	public List<Map<String, String>> getMyRemitMenu(final String uid) {
		LinkedList<Map<String, String>> returnMyRemitMenuList = new LinkedList<Map<String, String>>();
		// 找出這個人的所有 REMITMENU
		List<ADMMYREMITMENU> result = admMyRemitMenuDao.findByUid(uid);
		log.debug(ESAPIUtil.vaildLog("getMyRemitMenu result >>>>>> " + result));
		// 取得現在語系
		Locale currentLocale = LocaleContextHolder.getLocale();
		log.debug("errorMsg.locale >> {}", currentLocale);
		String locale = currentLocale.toString();
		log.debug("getRemitMenu.locale2 >> {}", locale);

		for (ADMMYREMITMENU admmyremitmenu : result) {
			Map<String, String> r = CodeUtil.objectCovert(Map.class, admmyremitmenu);

			String adrmttype = r.get("ADRMTTYPE").trim();
			String adrmtid = r.get("ADRMTID").trim();
			r.put("ADRMTTYPE", adrmttype);
			r.put("ADRMTID", adrmtid);

			// 中文名稱對應
			// ADMREMITMENU.ADRMTID=ADMMYREMITMENU.ADRMTID
			ADMREMITMENU admremitmenu = admRemitMenuDao.findUniqueByRmtid(adrmttype, adrmtid);
			// 切換項目語系
			if (admremitmenu != null) {
				switch (locale) {
				case "en":
					r.put("ADRMTITEM", admremitmenu.getADRMTENGITEM());// 項目(英)
					r.put("ADRMTDESC", admremitmenu.getADRMTENGDESC());// 說明(英)
					break;
				case "zh_TW":
					r.put("ADRMTITEM", admremitmenu.getADRMTITEM());// 項目(英)
					r.put("ADRMTDESC", admremitmenu.getADRMTDESC());// 說明(英)
					break;
				case "zh_CN":
					r.put("ADRMTITEM", admremitmenu.getADRMTCHSITEM());// 項目(英)
					r.put("ADRMTDESC", admremitmenu.getADRMTCHSDESC());// 說明(英)
					break;
				}
			}

			if (adrmtid.equals("NUL") && adrmttype.equals("1")) {
				r.put("ADRMTITEM", i18n.getMsg("LB.X2463"));// 結購入外匯存款(台轉外)
				r.put("ADRMTDESC", i18n.getMsg("LB.X2463"));// 結購入外匯存款(台轉外)
			} else if (adrmtid.equals("NUL") && adrmttype.equals("2")) {
				r.put("ADRMTITEM", i18n.getMsg("LB.X2465"));// 結售外匯存款(外轉台)
				r.put("ADRMTDESC", i18n.getMsg("LB.X2465"));// 結售外匯存款(外轉台)
			}

			// 2.5既有的bug修正--20210421資料庫新增資料修正故註解
//			if ("R".equals(adrmtid)) {
//				r.put("ADRMTITEM", ""); // 國內貨款之收入or支付
//				r.put("ADRMTDESC", ""); // 國內貨款之收入or支付
//			}
//			if ("S".equals(adrmtid)) {
//				r.put("ADRMTITEM", ""); // 國內外幣保單、基金、債券等投資款項之收入or支付
//				r.put("ADRMTDESC", ""); // 國內外幣保單、基金、債券等投資款項之收入or支付
//			}
//			if ("T".equals(adrmtid)) {
//				r.put("ADRMTITEM", ""); // 國內贍家移轉收入or支出
//				r.put("ADRMTDESC", ""); // 國內贍家移轉收入or支出
//			}

			returnMyRemitMenuList.add(r);
		}
		return returnMyRemitMenuList;
	}

	public List<TXNADDRESSBOOK> findByDPUserID(String dpuserid) {
		List<TXNADDRESSBOOK> list = txnAddressBookDao.findByDPUserID(dpuserid);
		return list;
	}

	public List<ADMBANK> getBankList() {
		List<ADMBANK> bankList = admBankDao.getAll();
		return bankList;
	}

	public String get_notifyInfo(String cusidn) {
		TXNUSER list = txnUserDao.get(TXNUSER.class, cusidn);

		return list.getDPNOTIFY();
	}

	public String get_dpoverview(String cusidn) {
		TXNUSER list = txnUserDao.get(TXNUSER.class, cusidn);
		return list.getDPOVERVIEW();
	}

	public Map<String, String> action(Map<String, String> reqParam) {
		String action = "";
		Map<String, String> result = new HashMap<String, String>();
		OLD_TXNADDRESSBOOK old_po = null;
		String dpuserid = "", dpgoname = "", dpabmail = "";
		Integer i_dpaddbkid = null;
		try {
			log.info(ESAPIUtil.vaildLog("EXECUTEFUNCTION>>" + reqParam.get("EXECUTEFUNCTION")));
//			log.info("EXECUTEFUNCTION>>{}", reqParam.get("EXECUTEFUNCTION"));

			if ("INSERT".equalsIgnoreCase(reqParam.get("EXECUTEFUNCTION"))) {
				TXNADDRESSBOOK newAddrBook = new TXNADDRESSBOOK();
				newAddrBook.setDPUSERID(reqParam.get("DPUSERID"));
				newAddrBook.setDPGONAME(reqParam.get("DPGONAME"));
				newAddrBook.setDPABMAIL(reqParam.get("DPABMAIL"));
				Date d = new Date();
				newAddrBook.setLASTDATE(DateUtil.format("yyyyMMdd", d));
				newAddrBook.setLASTTIME(DateUtil.format("HHmmss", d));
				txnAddressBookDao.save(newAddrBook);
				action = "INSERT";
				result.put("DPGONAME", reqParam.get("DPGONAME"));
				result.put("DPABMAIL", reqParam.get("DPABMAIL"));

				try {
					if ("Y".equals(isSyncNNB)) {
						dpuserid = newAddrBook.getDPUSERID();
						dpgoname = newAddrBook.getDPGONAME();
						dpabmail = newAddrBook.getDPABMAIL();
						old_po = old_txnAddressBookDao.findByInput(dpuserid, dpgoname, dpabmail);
						if (old_po == null) {
							old_po = CodeUtil.objectCovert(OLD_TXNADDRESSBOOK.class, newAddrBook);
							old_po.setDPADDBKID(null);
							old_txnAddressBookDao.save(old_po);
						} else {
							log.warn("has  same data  nothing to do ");
//							log.warn("has  same data  nothing to do >>{}", CodeUtil.toJson(old_po));
						}
					}
				} catch (Throwable e) {
					log.error("isSyncNNB.OLD_TXNADDRESSBOOK.INSERT.ERROR>>{}", e);
				}
			} else if ("UPDATE".equalsIgnoreCase(reqParam.get("EXECUTEFUNCTION"))) {
				String dpaddbkid = reqParam.get("DPADDBKID");
				TXNADDRESSBOOK addrBook = txnAddressBookDao.get(TXNADDRESSBOOK.class, new Integer(dpaddbkid));

				dpuserid = addrBook.getDPUSERID();
				dpgoname = addrBook.getDPGONAME();
				dpabmail = addrBook.getDPABMAIL();

				addrBook.setDPABMAIL(reqParam.get("DPABMAIL"));
				addrBook.setDPGONAME(reqParam.get("DPGONAME"));
				txnAddressBookDao.update(addrBook);
				action = "UPDATE";
				result.put("DPGONAME", reqParam.get("DPGONAME"));
				result.put("DPABMAIL", reqParam.get("DPABMAIL"));

				try {
					if ("Y".equals(isSyncNNB)) {

						old_po = old_txnAddressBookDao.findByInput(dpuserid, dpgoname, dpabmail);
//						log.debug("old_po>>{}",old_po);
						if (old_po == null) {
							old_po = CodeUtil.objectCovert(OLD_TXNADDRESSBOOK.class, addrBook);
							old_po.setDPADDBKID(null);
							old_txnAddressBookDao.save(old_po);
						} else {
							i_dpaddbkid = old_po.getDPADDBKID();
							old_po = CodeUtil.objectCovert(OLD_TXNADDRESSBOOK.class, addrBook);
							old_po.setDPADDBKID(i_dpaddbkid);
							old_txnAddressBookDao.update(old_po);
						}
					}
				} catch (Throwable e) {
					log.error("isSyncNNB.OLD_TXNADDRESSBOOK.UPDATE.ERROR>>{}", e);
				}
			} else if ("DELETE".equalsIgnoreCase(reqParam.get("EXECUTEFUNCTION"))) {
				String dpaddbkid = reqParam.get("DPADDBKID");
				TXNADDRESSBOOK newAddrBook = null;
//				newAddrBook.setDPADDBKID(new Integer(dpaddbkid));
				newAddrBook = txnAddressBookDao.get(TXNADDRESSBOOK.class, new Integer(dpaddbkid));
				txnAddressBookDao.remove(newAddrBook);
				action = "DELETE";
				result.put("DPGONAME", reqParam.get("DPGONAME"));
				result.put("DPABMAIL", reqParam.get("DPABMAIL"));
				try {
					if ("Y".equals(isSyncNNB)) {
						dpuserid = newAddrBook.getDPUSERID();
						dpgoname = newAddrBook.getDPGONAME();
						dpabmail = newAddrBook.getDPABMAIL();
						old_po = old_txnAddressBookDao.findByInput(dpuserid, dpgoname, dpabmail);
						if (old_po != null) {
							old_txnAddressBookDao.remove(old_po);
						} else {
							log.warn("no data  nothing to do...");
						}
					}
				} catch (Throwable e) {
					log.error("isSyncNNB.OLD_TXNADDRESSBOOK.DELETE.ERROR>>{}", e);
				}
			}

		} catch (DataAccessException e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("action error >> {}", e);
		} catch (NumberFormatException e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("action error >> {}", e);
		}
		result.put("action", action);
		return result;

	}

	/**
	 * 判斷外匯是否可執行即時交易
	 * 
	 * @return
	 */
	public boolean isFxBizTime() {

		Date d = new Date();
		String str_CurrentDate = new DateTime().toString("yyyy/MM/dd");
		log.debug("str_CurrentDate={}", str_CurrentDate);
		String str_CurrentTime = new DateTime().toString("HHmmss");

		log.debug("str_CurrentTime={}", str_CurrentTime);

		SYSPARAMDATA syPparam = new SYSPARAMDATA();
		try {
			syPparam = sysParamDataDao.getByPK("NBSYS");

			String str_BizStart = syPparam.getADFXSHH() + syPparam.getADFXSMM() + syPparam.getADFXSSS();
			log.debug("str_BizStart={}", str_BizStart);
			String str_BizEnd = syPparam.getADFXEHH() + syPparam.getADFXEMM() + syPparam.getADFXESS();
			log.debug("str_BizEnd={}", str_BizEnd);

			// ADMHOLIDAY holiday = null;
			// try {
			// holiday = (ADMHOLIDAY) admholidayDao.findById(str_CurrentDate);
			// }
			// catch (Exception e) {
			// holiday = null;
			// }
			// log.debug("holiday={}",holiday);
			//
			// //若非假日 && 系統時間 in 營業時間內
			// if ((holiday == null) && (str_CurrentTime.compareTo(str_BizStart) > 0) &&
			// (str_CurrentTime.compareTo(str_BizEnd) < 0)) {
			// log.debug("business date");
			// return true;
			// }
			// //若為假日 or 系統時間 not in 營業時間內
			// else {
			// log.debug("not business date");
			// return false;
			// }

			// 換一個寫法
			boolean isHoliday = false;
			isHoliday = isHoliday(); // 判斷系統時間日期是否是假日
			// 若非假日 && 系統時間 in 營業時間內
			if ((isHoliday == false) && (str_CurrentTime.compareTo(str_BizStart) > 0)
					&& (str_CurrentTime.compareTo(str_BizEnd) < 0)) {
				log.debug("business date");
				return true;
			}
			// 若為假日 or 系統時間 not in 營業時間內
			else {
				log.debug("not business date");
				return false;
			}

		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("isFxBizTime error >> {}", e);
		}

		return false;
	}
	/**
	 * 判斷"外匯結構售" 是否可執行即時交易
	 * 2021061#需求單H1100000029 可交易時間調整為09:10 ~ 19:00
	 *
	 * @author TBB
	 * @return boolean
	 */
	public boolean isFxBizFixedTime() {
		boolean isFxBizTime = Boolean.FALSE;
		
		//20210812 修正時間格式錯誤，導致判斷邏輯問題
		String str_CurrentDate= LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
		String str_CurrentTime= LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss"));
		String str_BizStart = "091000";
		String str_BizEnd = "190000";

		log.debug("Fcy_CurrentDate={} ~ Fcy_CurrentTime={}",str_CurrentDate, str_CurrentTime);
		log.info("Fcy Transaction Start Time={} ~ Fcy Transaction End Time={}", str_BizStart, str_BizEnd);

		try {
			// 換一個寫法
			boolean isHoliday = isHoliday(); // 判斷系統時間日期是否是假日
			// 若非假日 && 系統時間 in 營業時間內
			if ((!isHoliday) && (str_CurrentTime.compareTo(str_BizStart) > 0)&& (str_CurrentTime.compareTo(str_BizEnd) < 0)) {
				log.debug("Business date");
				isFxBizTime = Boolean.TRUE;
			} else {
				// 若為假日 or 系統時間 not in 營業時間內
				log.info("Today not business date");
				isFxBizTime = Boolean.FALSE;
			}
		} catch (Exception e) {
			log.error("Get isFxBizTime Failure >> {}", e);
		}

		return isFxBizTime;
	}
	/**
	 * 判斷系統時間日期是否是假日
	 * 
	 * @return
	 */
	public boolean isHoliday() {
		return admholidayDao.isAdmholiday();
	}

	/**
	 * 依照TRANSCODE取得基金的資料
	 */
	public TXNFUNDDATA getFundData(String TRANSCODE) {
		TXNFUNDDATA txnFundData = null;
		try {
			txnFundData = txnFundDataDao.getByPK(TRANSCODE);
		} catch (Exception e) {
			log.error("DaoService getFundData Exception", e);
		}
		return txnFundData;
	}

	/**
	 * 申請信用卡進度查詢 取得卡片名稱，依據CARDNO
	 * 
	 * @param
	 * @return
	 */
	public String getCardNameByCardno(final String cardno) {
		String result = admCardDataDao.findCARDNAME(cardno);
		return result;
	}

	/**
	 * 取得卡片註記
	 * 
	 * @param
	 * @return
	 */
	public String getCardMemoByCardno(final String cardno) {
		String result = admCardDataDao.findCARDMEMO(cardno);
		return result;
	}

	/**
	 * 台企分行聯絡方式資料檔擷取
	 * 
	 * @param bhid
	 * @return
	 */
	public List<ADMBHCONTACT> getAdmBhContact(String bhid) {

		return admBhContactDao.findByBhID(bhid);
	}

	public void editMyRemitMenu(final String uid, final String type, final String rmtid, final String rmttype) {
		// 刪除一筆
		if ("D".equals(type)) {
			admMyRemitMenuDao.removeByUidRmtid(uid, rmtid, rmttype);
		}

		// 新增一筆
		if ("A".equals(type)) {
			if (admMyRemitMenuDao.findByUidRmtid(uid, rmtid, rmttype) == null) {
				ADMMYREMITMENU po = new ADMMYREMITMENU();
				po.setADUID(uid);
				po.setADRMTTYPE(rmttype);
				po.setADRMTID(rmtid);
				Date d = new Date();
				po.setLASTDATE(DateUtil.format("yyyyMMdd", d));
				po.setLASTTIME(DateUtil.format("HHmmss", d));
				po.setLASTUSER("SYS");
				admMyRemitMenuDao.save(po);
			}
		}
	}

	/**
	 * 取得 幣別
	 * 
	 * @return List<ADMCURRENCY>
	 */
	public List<ADMCURRENCY> getCRYList() {

		List<ADMCURRENCY> admCurrencyList = new LinkedList<>();
		try {
			admCurrencyList = admCurrencyDao.getAll();
			// i18
			Locale currentLocale = LocaleContextHolder.getLocale();
			log.debug("admbanl.locale >> {}", currentLocale);
			String locale = currentLocale.toString();
			// 根據 i18 取代幣別顯示名稱
			for (ADMCURRENCY admcurrency : admCurrencyList) {
				switch (locale) {
				case "en":
					admcurrency.setADCCYNAME(admcurrency.getADCCYENGNAME());
					break;
				case "zh_CN":
					admcurrency.setADCCYNAME(admcurrency.getADCCYCHSNAME());
					break;
				default:
					break;
				}
			}
		} catch (Exception e) {
			log.error("getCRYList err {}", e);
		}
		log.trace(ESAPIUtil.vaildLog("admCurrencyList >> " + admCurrencyList));
		return admCurrencyList;
	}

	public ADMCURRENCY getCRY(String cry) {
		ADMCURRENCY po = null;
		try {
			if ("".equals(cry)) {
				return new ADMCURRENCY();
			} else if ("NTD".equals(cry)) {
				cry = "TWD";
			}
			po = admCurrencyDao.getByPK(cry);
			log.trace("ADMCURRENCY in>>>>> {}", po.toString());
			// i18n
			Locale currentLocale = LocaleContextHolder.getLocale();
			log.debug("getCRY.locale >> {}", currentLocale);
			String locale = currentLocale.toString();
			switch (locale) {
			case "en":
				po.setADCCYNAME(po.getADCCYENGNAME());
				break;
			case "zh_CN":
				po.setADCCYNAME(po.getADCCYCHSNAME());
				break;
			default:
				break;
			}
			log.trace("ADMCURRENCY out>>>>> {}", po.toString());

		} catch (Exception e) {
			log.error("DaoService Exception", e);
		}

		return po;
	}

	/**
	 * 取得常用帳號
	 * 
	 * @param uid
	 * @return
	 */
	public List<Map<String, String>> getCustomAcnoList(String uid) {
		List<Map<String, String>> result = new ArrayList<>();
		try {
			List<TXNTRACCSET> dqr = txnTrAccSetDao.getByUid(uid);
			log.trace(ESAPIUtil.vaildLog("dqr >> " + dqr.toArray().toString()));
			for (TXNTRACCSET row : dqr) {
				try {
					String json = CodeUtil.toJson(row);
					Map<String, String> bean = CodeUtil.fromJson(json, Map.class);

					String bankCode = row.getDPTRIBANK();
					log.trace(ESAPIUtil.vaildLog("bankCode >> {}" + bankCode));

					// String bankName = admBankDao.getBankName(bankCode);
					// i18n
					String bankName = "";
					Locale currentLocale = LocaleContextHolder.getLocale();
					log.debug("admbanl.locale >> {}", currentLocale);
					String locale = currentLocale.toString();

					switch (locale) {
					case "en":
						bankName = admBankDao.getBankEnName(bankCode);
						break;
					case "zh_CN":
						bankName = admBankDao.getBankChName(bankCode);
						break;
					default:
						bankName = admBankDao.getBankName(bankCode);
						break;

					}
					log.trace(ESAPIUtil.vaildLog("bankName >> {}" + bankName));

					bean.put("DPTRIBANK", bankCode);
					bean.put("DPTRDACNO", row.getDPTRDACNO());

					// 統一使用 BNKCOD, 及 ACN
					Map<String, String> mr = new LinkedHashMap<>();
					mr.put("BNKCOD", (String) bean.get("DPTRIBANK"));
					log.trace(ESAPIUtil.vaildLog("bean.DPTRIBANK >> {}" + (String) bean.get("DPTRIBANK")));
					log.trace(ESAPIUtil.vaildLog("bean.DPTRDACNO >> {}" + (String) bean.get("DPTRDACNO")));

					mr.put("ACN", (String) bean.get("DPTRDACNO"));

					String agree = row.getDPTRACNO().equals("1") ? "1" : "0"; // 資料庫中常用帳號 1:約定 2:非約定
					mr.put("AGREE", agree); // 0: 非約定, 1: 約定
					log.trace("Agree >> {}", agree);

					bean.put("VALUE", CodeUtil.toJson(mr));
					log.trace(ESAPIUtil.vaildLog("VALUE >> {}" + CodeUtil.toJson(mr)));

					StringBuffer sb = new StringBuffer();
					sb.append(bean.get("DPTRIBANK"));
					sb.append("-");
					sb.append(bankName);
					sb.append(" ");
					sb.append(bean.get("DPTRDACNO"));
					sb.append(" ");
					sb.append(StrUtils.trim((String) bean.get("DPGONAME")));
					String txt = sb.toString();
					bean.put("TEXT", txt);
					log.trace(ESAPIUtil.vaildLog("TEXT >> {}" + txt));

					result.add(bean);
				} catch (Exception e) {
					log.error("無法將 PO 轉換成 Map.", e);
				}
			}
		} catch (Exception e) {
			log.error(ESAPIUtil.vaildLog("無法取得 >> " + uid + "常用帳號." + e));
		}
		return result;
	}

	/**
	 * 合併N921 約定加常用帳號, 限定 約定或非約定
	 * 
	 * @param uid
	 * @param type AcnoTypeEnum屬性
	 * @return
	 */
	public BaseResult getAgreeAcnoList(final String uid, String type) {
		return this.getAgreeAcnoList(uid, type, false);
	}

	/**
	 * 合併N921 約定加常用帳號, 限定 約定或非約定
	 * 
	 * @param uid
	 * @param type      AcnoTypeEnum屬性
	 * @param agreeOnly 只取約定帳號
	 * @return
	 */
	public BaseResult getAgreeAcnoList(final String uid, String type, boolean agreeOnly) {
		BaseResult resultBs = new BaseResult();
		List<Map<String, String>> resultRows = new ArrayList<>();
		List<Map<String, String>> resultRowsNotAgree = new ArrayList<>();

		final Map<String, Object> inDbAcc = new HashMap<>();
		final Map<String, Object> notAgreeAcc = new HashMap<>();
		log.debug("uid >> {}", uid);
		log.trace(ESAPIUtil.vaildLog("type >> " + type));
		log.debug("agreeOnly >> {}", agreeOnly);
		String designated = "";
		String non_designated = "";
		// 取得常用帳號
		try {

			designated = i18n.getMsg("LB.Designated_account");
			non_designated = i18n.getMsg("LB.Common_non-designated_account");
			List<Map<String, String>> customAcnos = getCustomAcnoList(uid);
			if (customAcnos.isEmpty()) {
				log.debug("{} 查無常用帳號", uid);
			}
			log.debug(ESAPIUtil.vaildLog("取得常用帳號 getCustomAcnoList result >> {}" + customAcnos));

			for (Map<String, String> row : customAcnos) {
				StringBuffer sb = new StringBuffer();
				sb.append(row.get("DPTRIBANK"));
				sb.append("_");
				sb.append(row.get("DPTRDACNO"));
				String keyName = sb.toString();
				// 資料庫中常用帳號 1:約定 2:非約定
				if ("1".equals(row.get("DPTRACNO"))) {
					// 約定帳號
					inDbAcc.put(keyName, row);
				} else if ("2".equals(row.get("DPTRACNO"))) {
					// 非約定帳號
					notAgreeAcc.put(keyName, row);
				}
			}

			log.debug(ESAPIUtil.vaildLog("inDbAcc >>> {}" + inDbAcc));
			log.debug(ESAPIUtil.vaildLog("notAgreeAcc >>> {}" + notAgreeAcc));

			BaseResult bs = new BaseResult();
			bs = N921_REST(uid, type);
			if (bs.getResult() == false) {
				log.error("N921_REST error. MsgCode:{} Message:{}", bs.getMsgCode(), bs.getMessage());
			}
			Map<String, Map> bsData = (Map<String, Map>) bs.getData();
			log.debug("N921_REST >> {}", bsData);
			List<Map<String, String>> rows = (List<Map<String, String>>) bsData.get("REC");

			// 先加入N921約定帳號
			// VALUE=0:非約定 VALUE=1:約定
			if (rows.size() > 0) {
				Map<String, String> firstRow = new HashMap<>();
				firstRow.put("VALUE", "#");
				// firstRow.put("TEXT", "--約定帳號--");
				firstRow.put("TEXT", "--" + designated + "--");
				firstRow.put("BNKCOD", "");
				resultRows.add(firstRow);

				// 約定帳號排序
				rows.stream()
				.filter(c->StringUtils.isNumeric(c.get("BNKCOD")))//過濾銀行代碼非數字資料
				.sorted(new Comparator<Map<String, String>>() {
					@Override
					public int compare(Map<String, String> o1, Map<String, String> o2) {
						try {
							String O1BNK = (String) o1.get("BNKCOD");
							String O2BNK = (String) o2.get("BNKCOD");
							
							if (StringUtil.isEmpty(O1BNK) && !StringUtil.isEmpty(O2BNK))
								return 1;
							else if (StringUtil.isEmpty(O2BNK))
								return -1;
							else if (O1BNK.equals("050") && !O2BNK.equals("050"))
								return -1;
							else if (O2BNK.equals("050"))
								return 1;
							else if (Integer.parseInt(O1BNK) > Integer.parseInt(O2BNK))
								return 1;
							else if (Integer.parseInt(O1BNK) < Integer.parseInt(O2BNK))
								return -1;
							else
								return 0;
						}catch(Exception e) {
							log.error("Account Sort error!!");
						}
						return 0;
					}
				}).forEach(i -> {//排序完走訪
					int addCount = 0;
					Map<String, String> rx = new HashMap<>();
					String acno = StrUtils.trim(i.get("ACN"));
					String key = i.get("BNKCOD") + "_" + acno;

					String bankName = "";
					Locale currentLocale = LocaleContextHolder.getLocale();
					log.debug("admbanl.locale >> {}", currentLocale);
					String locale = currentLocale.toString();

					switch (locale) {
					case "en":
						bankName = admBankDao.getBankEnName(i.get("BNKCOD"));
						break;
					case "zh_CN":
						bankName = admBankDao.getBankChName(i.get("BNKCOD"));
						break;
					default:
						bankName = admBankDao.getBankName(i.get("BNKCOD"));
						break;

					}
					i.put("TEXT", i.get("BNKCOD") + "-" + bankName + " " + i.get("ACN"));
					log.trace(ESAPIUtil.vaildLog("bankName >> {}" + bankName));

					if (inDbAcc.containsKey(key)) {
						// N921與DB皆有設定此轉入帳號
						Map<String, String> dbRow = (Map<String, String>) inDbAcc.get(key);
						rx.put("VALUE", i.get("VALUE"));
						rx.put("TEXT", dbRow.get("TEXT")); // 使用DB的TEXT
						rx.put("BNKCOD", i.get("BNKCOD"));
						resultRows.add(rx);
						log.debug(ESAPIUtil.vaildLog("{} 好記名稱 >> {}" + rx.get(key) + rx.get("TEXT")));
					} else {
						// DB裡的常用帳號可能是16位的(舊網銀轉入)
						String tempAcno = StrUtils.repeat("0", 16) + acno;
						String key16 = i.get("BNKCOD") + "_" + tempAcno.substring(tempAcno.length() - 16);

						if (inDbAcc.containsKey(key16)) {
							// N921與DB皆有設定此轉入帳號
							Map<String, String> dbRow = (Map<String, String>) inDbAcc.get(key16);
							rx.put("VALUE", i.get("VALUE"));
							rx.put("TEXT", i.get("TEXT") + " " + dbRow.get("DPGONAME")); // 使用N921的TEXT + DB的好記名稱
							rx.put("BNKCOD", i.get("BNKCOD"));
							resultRows.add(rx);
							log.debug(ESAPIUtil.vaildLog("{} 好記名稱 >> {}" + rx.get(key16) + rx.get("TEXT")));
						} else {
							// 只有N921有此轉入帳號，DB沒有
							rx.put("VALUE", i.get("VALUE"));
							rx.put("TEXT", i.get("TEXT"));
							rx.put("BNKCOD", i.get("BNKCOD"));
							resultRows.add(rx);
							log.debug(ESAPIUtil.vaildLog("{} 此帳號DB查無好記名稱" + rx.get(key)));
						}
					}

					if (StrUtil.isNotEmpty(i.get("VALUE"))) {
						Map<String, String> m = CodeUtil.fromJson(i.get("VALUE"), Map.class);
						if ("1".equals(m.get("AGREE"))) {
							addCount++;
						}
					}

					if (addCount == 0) {
						resultRows.remove(firstRow);
					}
				});
			}
			// 再加入DB 裡設定的常用非約定帳號
			if (!agreeOnly && notAgreeAcc.size() > 0) {
				Map<String, String> firstRow = new HashMap<>();
				firstRow.put("VALUE", "#");
				// firstRow.put("TEXT", "--常用非約定帳號--");
				// firstRow.put("TEXT", "--"+non_designated+"--");
				firstRow.put("BNKCOD", "");
				resultRowsNotAgree.add(firstRow);

				// 排序常用非約定帳號資料
				notAgreeAcc.entrySet().stream()
				.filter(c->checkBnkcod(c))//過濾銀行代碼非數字資料
				.sorted(new Comparator<Map.Entry<String, Object>>() {
					@Override
					public int compare(Map.Entry<String, Object> o1, Map.Entry<String, Object> o2) {
						try {
							String O1BNKjson = (String) ((Map<String, Object>) o1.getValue()).get("VALUE");
							String O1BNK = (String) CodeUtil.fromJson(O1BNKjson, Map.class).get("BNKCOD");
							String O2BNKjson = (String) ((Map<String, Object>) o2.getValue()).get("VALUE");
							String O2BNK = (String) CodeUtil.fromJson(O2BNKjson, Map.class).get("BNKCOD");
							
							if (StringUtil.isEmpty(O1BNK) && !StringUtil.isEmpty(O2BNK))
								return 1;
							else if (StringUtil.isEmpty(O2BNK))
								return -1;
							else if (O1BNK.equals("050") && !O2BNK.equals("050"))
								return -1;
							else if (O2BNK.equals("050"))
								return 1;
							else if (Integer.parseInt(O1BNK) > Integer.parseInt(O2BNK))
								return 1;
							else if (Integer.parseInt(O1BNK) < Integer.parseInt(O2BNK))
								return -1;
							else
								return 0;
						}catch(Exception e) {
							log.error("Account Sort error!!");
						}
						return 0;
					}
				}).forEach(i -> {
					Map<String, String> rx = new HashMap<>();
					Map<String, String> dbRow = (Map<String, String>) i.getValue();
					rx.put("VALUE", dbRow.get("VALUE"));
					rx.put("TEXT", dbRow.get("TEXT"));
					rx.put("BNKCOD", dbRow.get("TEXT").substring(0, 3));
					resultRowsNotAgree.add(rx);
				});

				if (resultRowsNotAgree.size() > 1) {
					resultRowsNotAgree.remove(firstRow);
				}
			}

			if (!resultRows.isEmpty() && resultRowsNotAgree.isEmpty()) {
				resultBs.addData("REC", resultRows);
				log.trace("resultBs.data >> {}", resultBs.getData());
				resultBs.setResult(true);
				resultBs.setMessage("0", i18n.getMsg("LB.X1805"));// 查詢成功
			} else if (resultRows.isEmpty() && !resultRowsNotAgree.isEmpty()) {
				resultBs.addData("REC", resultRowsNotAgree);
				log.trace("resultBs.data >> {}", resultBs.getData());
				resultBs.setResult(true);
				resultBs.setMessage("0", i18n.getMsg("LB.X1805"));// 查詢成功
			} else if (!resultRows.isEmpty() && !resultRowsNotAgree.isEmpty()) {
				resultRows.addAll(resultRowsNotAgree);
				resultBs.addData("REC", resultRows);
				log.trace("resultBs.data >> {}", resultBs.getData());
				resultBs.setResult(true);
				resultBs.setMessage("0", i18n.getMsg("LB.X1805"));// 查詢成功
			} else {
				log.debug("ERROR");
				log.trace("resultBs.data >> {}", resultBs.getData());
				resultBs.setMessage("ENRD", i18n.getMsg("LB.Check_no_data"));// 查無資料
			}

		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("getAgreeAcnoList error >> {}", e);
		}
		log.debug("resultBs.data >> {}", resultBs.getData());
		log.debug("resultBs msgCode: {} Message: {}", resultBs.getMsgCode(), resultBs.getMessage());
		return resultBs;
	}

	/**
	 * 頁面上的checkbox加入常用帳號 注意:參數內需要Key : DPUSERID (CUSIDN)
	 * 
	 * @author BenChien
	 * @param reqParam
	 * @return
	 */
	public BaseResult add_common_acount(Map<String, String> reqParam) {
		BaseResult bs = null;
		Map<String, String> result = new HashMap<String, String>();
		ObjectMapper objectMapper = new ObjectMapper();
		String dptribank = "";
		String dptrdacno = "";
		try {
			bs = new BaseResult();

			String dpuserid = reqParam.get("DPUSERID"); // id
			String dpgoname = reqParam.get("DPGONAME");
			String dptracno = reqParam.get("DPTRACNO");
			if ("1".equals(dptracno)) { // 約定帳號新增值從option的JSON字串
				log.trace(ESAPIUtil.vaildLog("INSERT DATA >> " + reqParam.get("DPAGACNO")));
				JsonNode jsonNode = objectMapper.readTree(reqParam.get("DPAGACNO"));
				dptribank = jsonNode.get("BNKCOD").asText();
				dptrdacno = jsonNode.get("ACN").asText();
			} else {
				dptribank = reqParam.get("DPTRIBANK");
				dptrdacno = reqParam.get("DPTRDACNO");
			}

			log.trace("Insert Data Record");
			log.trace(ESAPIUtil.vaildLog("dpuserid >> " + dpuserid));
			log.trace(ESAPIUtil.vaildLog("dpgoname >> " + dpgoname));
			log.trace(ESAPIUtil.vaildLog("dptracno >> " + dptracno));
			log.trace(ESAPIUtil.vaildLog("dptribank >> " + dptribank));
			log.trace(ESAPIUtil.vaildLog("dptrdacno >> " + dptrdacno));

			if (StrUtil.isNotEmpty(dptrdacno) && txnTrAccSetDao.findDptrdacnoIsExists(dpuserid, dptribank, dptrdacno)) {
				bs.setErrorMessage("Z997", admMsgCodeDao.errorMsg("Z997"));
				return bs;
			}

			TXNTRACCSET po = new TXNTRACCSET();
			po.setDPUSERID(dpuserid);
			po.setDPGONAME(dpgoname);
			po.setDPTRACNO(dptracno);
			po.setDPTRIBANK(dptribank);
			po.setDPTRDACNO(dptrdacno);
			po.setLASTDATE(DateUtil.getDate(""));
			po.setLASTTIME(DateUtil.getTheTime(""));
			txnTrAccSetDao.save(po);
			try {
				Boolean isExists = Boolean.FALSE;
				log.debug("isSyncNNB>>{}", isSyncNNB);
				if ("Y".equals(isSyncNNB)) {
					isExists = old_txntraccsetDao.findDptrdacnoIsExists(po.getDPUSERID(), po.getDPTRIBANK(),
							po.getDPTRDACNO());
					log.debug("isExists>>{}", isExists);
					if (!isExists) {
						OLD_TXNTRACCSET old_po = CodeUtil.objectCovert(OLD_TXNTRACCSET.class, po);
						old_po.setDPACCSETID(null);
						old_txntraccsetDao.save(old_po);
					}
				}
			} catch (Throwable e) {
				log.error("isSyncNNB.OLD_TXNTRACCSET.ERROR>>{}", e);
			}

		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("add_common_acount error >> {}", e);
			bs.setErrorMessage("Z997", i18n.getMsg("LB.X1828"));// 加入常用帳號失敗
		} finally {
			bs.setResult(true);

			bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
		}
		return bs;
	}

	/**
	 * 
	 * @param cusidn   使用者統編
	 * @param acn      轉入帳號
	 * @param bnkcod   銀行代碼
	 * @param dpgoname 好記名稱
	 * @param dptracno 帳號類型 1:約定 2:非約定
	 * @return
	 */
	public String updateDpgoName(String cusidn, String acn, String bnkcod, String dpgoname, String dptracno) {
		log.debug("DaoService >> updateDpgoName");
		String result = "";
		TXNTRACCSET po = null;
		bnkcod = bnkcod.substring(0, 3);
		log.trace(ESAPIUtil.vaildLog("bnkcod>>>{}" + bnkcod));
		try {
//			String sql = "FROM fstop.orm.po.TXNTRACCSET where DPUSERID = :cusidn AND DPTRDACNO = :acn AND DPTRIBANK = :bank";
//			Query<TXNTRACCSET> query = getSession().createQuery(sql);
//			query.setParameter("cusidn", cusidn);
//			query.setParameter("acn", acn);
//			query.setParameter("bank",bnkcod);
//			po = query.uniqueResult();
			po = txnTrAccSetDao.getDpgoNameII(cusidn, acn, bnkcod);
			if (StrUtil.isNotEmpty(dpgoname)) {
				po.setDPGONAME(dpgoname);
			}
			if (StrUtil.isNotEmpty(dptracno)) {
				po.setDPTRACNO(dptracno);
			}

			// 更新最後時間
			DateTime d = new DateTime();
//			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
//			SimpleDateFormat hms = new SimpleDateFormat("hhmmss");
//			String today = sdf.format(d);
//			String hours = hms.format(d);
			po.setLASTDATE(d.toString("yyyyMMdd"));
			po.setLASTTIME(d.toString("HHmmss"));
			txnTrAccSetDao.update(po);
			result = "update";
			try {
				if ("Y".equals(isSyncNNB)) {
					OLD_TXNTRACCSET old_po = null;
					old_po = old_txntraccsetDao.getDpgoNameII(cusidn, acn, bnkcod);
					if (old_po == null) {
						log.debug("save old txntraccset");
						old_po = CodeUtil.objectCovert(OLD_TXNTRACCSET.class, po);
						old_po.setDPACCSETID(null);
						old_txntraccsetDao.save(old_po);
					} else {
						log.debug("update old txntraccset");
						if (StrUtil.isNotEmpty(dptracno)) {
							old_po.setDPTRACNO(dptracno);
						}
						if (StrUtil.isNotEmpty(dpgoname)) {
							old_po.setDPGONAME(dpgoname);
						}
						old_po.setLASTDATE(d.toString("yyyyMMdd"));
						old_po.setLASTTIME(d.toString("HHmmss"));
						old_txntraccsetDao.update(old_po);
					}
//					old_txntraccsetDao.saveOrUpdate(old_po);
				}
			} catch (Throwable e) {
				log.error("isSyncNNB.updateDpgoName.ERROR>>{}", e);
			}

		} catch (Exception e) {
			log.error(ESAPIUtil.vaildLog("好記名稱更新失敗({})." + acn + e));
			result = "";
		}
		return result;
	}

	/**
	 * 新增約定帳號
	 * 
	 * @param cusidn
	 * @param acn
	 * @param bnkcod
	 * @param dpgoname
	 * @return
	 */
	public String createDpgoname(String cusidn, String acn, String bnkcod, String dpgoname) {
		log.debug("DaoService >> createDpgoname");
		String result = "";
//		TXNTRACCSET po = null;
		TXNTRACCSET txntraccset = null;
		bnkcod = bnkcod.substring(0, 3);
		log.trace("bnkcod>>>{}", bnkcod);
		// 更新最後時間
		DateTime d = new DateTime();
		try {

			txntraccset = new TXNTRACCSET();
			txntraccset.setDPUSERID(cusidn);
			txntraccset.setDPGONAME(dpgoname);
			txntraccset.setDPTRACNO("1");
			txntraccset.setDPTRIBANK(bnkcod);
			txntraccset.setDPTRDACNO(acn);
			txntraccset.setLASTDATE(DateUtil.getDate(""));
			txntraccset.setLASTTIME(DateUtil.getTheTime(""));
			txnTrAccSetDao.save(txntraccset);
			result = "create";
			try {
				if ("Y".equals(isSyncNNB)) {
					OLD_TXNTRACCSET old_po = null;
					old_po = old_txntraccsetDao.getDpgoNameII(cusidn, acn, bnkcod);
					if (old_po == null) {
						old_po = CodeUtil.objectCovert(OLD_TXNTRACCSET.class, txntraccset);
						old_po.setDPACCSETID(null);
						old_txntraccsetDao.save(old_po);
					} else {
						log.debug("update old txntraccset");
						Integer id = old_po.getDPACCSETID();
						old_po = CodeUtil.objectCovert(OLD_TXNTRACCSET.class, txntraccset);
//						id 不可以用新個網
						old_po.setDPACCSETID(id);
						old_txntraccsetDao.update(old_po);
					}
//					old_txntraccsetDao.saveOrUpdate(old_po);
				}
			} catch (Throwable e) {
				log.error("isSyncNNB.updateDpgoName.ERROR>>{}", e);
			}

		} catch (Exception e) {
			log.error("新增約定帳號失敗({}).", acn, e);
			result = "";
		}
		return result;
	}

	public void syncTxntraccset(TXNTRACCSET po, String isRemove) {
		Integer id = null;
		DateTime d = new DateTime();
		try {
			log.info("isSyncNNB>>{}", isSyncNNB);
			if ("Y".equals(isSyncNNB)) {
				OLD_TXNTRACCSET old_po = null;
				old_po = old_txntraccsetDao.getDpgoNameII(po.getDPUSERID(), po.getDPTRDACNO(), po.getDPTRIBANK());
				log.debug(ESAPIUtil.vaildLog("isRemove>>{}" + isRemove));
//				log.debug("old_po>>{} ",CodeUtil.toJson(old_po) );
				if ("DELETE".equals(isRemove) && old_po == null) {
					return;
				}
				if ("DELETE".equals(isRemove) && old_po != null) {
					old_txntraccsetDao.remove(old_po);
					return;
				}

				if (old_po == null) {
					log.debug("save old txntraccset");
					old_po = CodeUtil.objectCovert(OLD_TXNTRACCSET.class, po);
					old_po.setDPACCSETID(id);
					old_txntraccsetDao.save(old_po);
				} else {
					log.debug("update old txntraccset");
					id = old_po.getDPACCSETID();
					old_po = CodeUtil.objectCovert(OLD_TXNTRACCSET.class, po);
					old_po.setDPACCSETID(id);
					old_po.setLASTDATE(d.toString("yyyyMMdd"));
					old_po.setLASTTIME(d.toString("HHmmss"));
					old_txntraccsetDao.update(old_po);
				}
//				old_txntraccsetDao.saveOrUpdate(old_po);
			}
		} catch (Throwable e) {
			log.error("syncTxntraccset.ERROR>>{}", e);
		}
	}

	/**
	 * 台幣轉帳用
	 * 
	 * @param reqParam
	 * @return
	 */
	/**
	 * 
	 * 加入常用帳號 台幣轉帳用
	 * 
	 * @param dpuserid  uid
	 * @param dpgoname  好記名稱(EX 005-土地銀行)
	 * @param dptribank 銀行代號
	 * @param dptracno  1:約定 2:非約定
	 * @param dptrdacno 帳號
	 * @return
	 */
	public BaseResult add_common_acount(String dpuserid, String dpgoname, String dptribank, String dptracno,
			String dptrdacno) {
		BaseResult bs = null;
		Map<String, String> result = new HashMap<String, String>();
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			bs = new BaseResult();

			log.trace("Insert Data Record");
			log.trace(ESAPIUtil.vaildLog("dpuserid >> " + dpuserid));
			log.trace(ESAPIUtil.vaildLog("dpgoname >> " + dpgoname));
			log.trace(ESAPIUtil.vaildLog("dptracno >> " + dptracno));
			log.trace(ESAPIUtil.vaildLog("dptribank >> " + dptribank));
			log.trace(ESAPIUtil.vaildLog("dptrdacno >> " + dptrdacno));
			if (StrUtil.isNotEmpty(dptrdacno) && txnTrAccSetDao.findDptrdacnoIsExists(dpuserid, dptribank, dptrdacno)) {
				// bs.setErrorMessage("Z997",admMsgCodeDao.errorMsg("Z997"));
				bs.addData("errorMsg", admMsgCodeDao.errorMsg("Z997"));
				log.debug("return bs Z997");
				return bs;
			}

			TXNTRACCSET po = new TXNTRACCSET();
			po.setDPUSERID(dpuserid);
			po.setDPGONAME(dpgoname);
			po.setDPTRACNO(dptracno);
			po.setDPTRIBANK(dptribank);
			po.setDPTRDACNO(dptrdacno);
			po.setLASTDATE(DateUtil.getDate(""));
			po.setLASTTIME(DateUtil.getTheTime(""));
			txnTrAccSetDao.save(po);
			bs.setMessage("", "");
			try {
				Boolean isExists = Boolean.FALSE;
				log.debug("TW.transer.isSyncNNB>>{}", isSyncNNB);
				if ("Y".equals(isSyncNNB)) {
					isExists = old_txntraccsetDao.findDptrdacnoIsExists(po.getDPUSERID(), po.getDPTRIBANK(),
							po.getDPTRDACNO());
					log.debug("TW.transer.isExists>>{}", isExists);
					if (!isExists) {
						OLD_TXNTRACCSET old_po = CodeUtil.objectCovert(OLD_TXNTRACCSET.class, po);
						old_po.setDPACCSETID(null);
						old_txntraccsetDao.save(old_po);
					}
				}
			} catch (Throwable e) {
				log.error("TW.transer.isSyncNNB.OLD_TXNTRACCSET.ERROR>>{}", e);
			}
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("add_common_acount error >> {}", e);
			bs.addData("errorMsg", i18n.getMsg("LB.X1828"));// 加入常用帳號失敗
			// bs.setErrorMessage("Z997","加入常用帳號失敗");
		} finally {
			bs.setResult(true);

			bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
		}
		return bs;
	}

	/**
	 * 合併N921 約定加常用帳號, 限定 約定或非約定
	 * 
	 * @param uid
	 * @param type      AcnoTypeEnum屬性
	 * @param agreeOnly 只取約定帳號
	 * @return
	 */
	public BaseResult getAgreeAcnoListSep(final String uid, String type, boolean agreeOnly) {
		BaseResult resultBs = new BaseResult();
		List<Map<String, String>> resultRows = new ArrayList<>();
		List<Map<String, String>> resultRowsNotAgree = new ArrayList<>();

		final Map<String, Object> inDbAcc = new HashMap<>();
		final Map<String, Object> notAgreeAcc = new HashMap<>();
		log.debug(ESAPIUtil.vaildLog("uid >> " + uid));
		log.debug(ESAPIUtil.vaildLog("type >> " + type));
		log.debug(ESAPIUtil.vaildLog("agreeOnly >> " + agreeOnly));
		// String designated = "";
		// String non_designated = "";
		// 取得常用帳號
		try {

			// designated = i18n.getMsg("LB.Designated_account");
			// non_designated = i18n.getMsg("LB.Common_non-designated_account");
			List<Map<String, String>> customAcnos = getCustomAcnoList(uid);
			if (customAcnos.isEmpty()) {
				log.debug(ESAPIUtil.vaildLog("查無常用帳號 >> " + uid));
			}
			log.debug(ESAPIUtil.vaildLog("取得常用帳號 getCustomAcnoList result >> {}" + customAcnos));

			for (Map<String, String> row : customAcnos) {
				StringBuffer sb = new StringBuffer();
				sb.append(row.get("DPTRIBANK"));
				sb.append("_");
				sb.append(row.get("DPTRDACNO"));
				String keyName = sb.toString();
				// 資料庫中常用帳號 1:約定 2:非約定
				if ("1".equals(row.get("DPTRACNO"))) {
					// 約定帳號
					inDbAcc.put(keyName, row);
				} else if ("2".equals(row.get("DPTRACNO"))) {
					// 非約定帳號
					notAgreeAcc.put(keyName, row);
				}
			}
			log.debug(ESAPIUtil.vaildLog("inDbAcc >>> {}" + inDbAcc));
			log.debug(ESAPIUtil.vaildLog("notAgreeAcc >>> {}" + notAgreeAcc));

			BaseResult bs = new BaseResult();
			bs = N921_REST(uid, type);
			if (bs.getResult() == false) {
				log.error("N921_REST error. MsgCode:{} Message:{}", bs.getMsgCode(), bs.getMessage());
			}
			Map<String, Map> bsData = (Map<String, Map>) bs.getData();
			log.debug("N921_REST >> {}", bsData);
			List<Map<String, String>> rows = (List<Map<String, String>>) bsData.get("REC");

			// 先加入N921約定帳號
			// VALUE=0:非約定 VALUE=1:約定
			if (rows.size() > 0) {
				Map<String, String> firstRow = new HashMap<>();
				// firstRow.put("VALUE", "#");
				// firstRow.put("TEXT", "--約定帳號--");
				// firstRow.put("TEXT", "--"+designated+"--");
				// firstRow.put("BNKCOD", "");
				// resultRows.add(firstRow);

				// int addCount = 0;

				for (Map<String, String> r : rows) {
					Map<String, String> rx = new HashMap<>();
					String acno = StrUtils.trim(r.get("ACN"));
					String key = r.get("BNKCOD") + "_" + acno;

					String bankName = "";
					Locale currentLocale = LocaleContextHolder.getLocale();
					log.debug("admbanl.locale >> {}", currentLocale);
					String locale = currentLocale.toString();

					switch (locale) {
					case "en":
						bankName = admBankDao.getBankEnName(r.get("BNKCOD"));
						break;
					case "zh_CN":
						bankName = admBankDao.getBankChName(r.get("BNKCOD"));
						break;
					default:
						bankName = admBankDao.getBankName(r.get("BNKCOD"));
						break;

					}
					r.put("TEXT", r.get("BNKCOD") + "-" + bankName + " " + r.get("ACN"));
					log.trace(ESAPIUtil.vaildLog("bankName >> {}" + bankName));

					if (inDbAcc.containsKey(key)) {
						// N921與DB皆有設定此轉入帳號
						Map<String, String> dbRow = (Map<String, String>) inDbAcc.get(key);
						rx.put("VALUE", r.get("VALUE"));
						rx.put("TEXT", dbRow.get("TEXT")); // 使用DB的TEXT
						rx.put("BNKCOD", r.get("BNKCOD"));
						resultRows.add(rx);
						log.debug(ESAPIUtil.vaildLog("{} 好記名稱 >> {}" + rx.get(key) + rx.get("TEXT")));
					} else {
						// DB裡的常用帳號可能是16位的(舊網銀轉入)
						String tempAcno = StrUtils.repeat("0", 16) + acno;
						String key16 = r.get("BNKCOD") + "_" + tempAcno.substring(tempAcno.length() - 16);

						if (inDbAcc.containsKey(key16)) {
							// N921與DB皆有設定此轉入帳號
							Map<String, String> dbRow = (Map<String, String>) inDbAcc.get(key16);
							rx.put("VALUE", r.get("VALUE"));
							rx.put("TEXT", r.get("TEXT") + " " + dbRow.get("DPGONAME")); // 使用N921的TEXT + DB的好記名稱
							rx.put("BNKCOD", r.get("BNKCOD"));
							resultRows.add(rx);
							log.debug(ESAPIUtil.vaildLog("{} 好記名稱 >> {}" + rx.get(key16) + rx.get("TEXT")));
						} else {
							// 只有N921有此轉入帳號，DB沒有
							rx.put("VALUE", r.get("VALUE"));
							rx.put("TEXT", r.get("TEXT"));
							rx.put("BNKCOD", r.get("BNKCOD"));
							resultRows.add(rx);
							log.debug(ESAPIUtil.vaildLog("{} 此帳號DB查無好記名稱" + rx.get(key)));
						}
					}

					// if (StrUtil.isNotEmpty(r.get("VALUE")))
					// {
					// Map<String, String> m = CodeUtil.fromJson(r.get("VALUE"), Map.class);
					// if ("1".equals(m.get("AGREE")))
					// {
					// addCount ++;
					// }
					// }
				}

				// if (addCount == 0)
				// {
				// resultRows.remove(firstRow);
				// }
			}
			// 再加入DB 裡設定的常用非約定帳號
			if (!agreeOnly && notAgreeAcc.size() > 0) {
				// Map<String, String> firstRow = new HashMap<>();
				// firstRow.put("VALUE", "#");
				// firstRow.put("TEXT", "--常用非約定帳號--");
				// firstRow.put("TEXT", "--"+non_designated+"--");
				// firstRow.put("BNKCOD", "");
				// resultRowsNotAgree.add(firstRow);

				// int addCount = 0;

				Iterator pairs = notAgreeAcc.entrySet().iterator();
				while (pairs.hasNext()) {
					Map<String, String> rx = new HashMap<>();
					Map.Entry<String, Map> entry = (Map.Entry<String, Map>) pairs.next();
					Map<String, String> dbRow = (Map<String, String>) notAgreeAcc.get(entry.getKey());
					rx.put("VALUE", dbRow.get("VALUE"));
					rx.put("TEXT", dbRow.get("TEXT"));
					rx.put("BNKCOD", dbRow.get("TEXT").substring(0, 3));
					resultRowsNotAgree.add(rx);
					// addCount ++;
				}
				// if (addCount == 0)
				// {
				// resultRowsNotAgree.remove(firstRow);
				// }
			}

			if (!resultRows.isEmpty() && resultRowsNotAgree.isEmpty()) {
				resultBs.addData("REC", resultRows);
				log.trace("resultBs.data >> {}", resultBs.getData());
				resultBs.setResult(true);
				resultBs.setMessage("0", i18n.getMsg("LB.X1805"));// 查詢成功
			} else if (resultRows.isEmpty() && !resultRowsNotAgree.isEmpty()) {
				resultBs.addData("REC2", resultRowsNotAgree);
				log.trace("resultBs.data >> {}", resultBs.getData());
				resultBs.setResult(true);
				resultBs.setMessage("0", i18n.getMsg("LB.X1805"));// 查詢成功
			} else if (!resultRows.isEmpty() && !resultRowsNotAgree.isEmpty()) {
				resultBs.addData("REC", resultRows);
				resultBs.addData("REC2", resultRowsNotAgree);
				log.trace("resultBs.data >> {}", resultBs.getData());
				resultBs.setResult(true);
				resultBs.setMessage("0", i18n.getMsg("LB.X1805"));// 查詢成功
			} else {
				log.debug("ERROR");
				log.trace("resultBs.data >> {}", resultBs.getData());
				resultBs.setMessage("ENRD", i18n.getMsg("Check_no_data"));// 查無資料
			}

		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("getAgreeAcnoListSep error >> {}", e);
		}
		log.debug("resultBs.data >> {}", resultBs.getData());
		log.debug("resultBs msgCode: {} Message: {}", resultBs.getMsgCode(), resultBs.getMessage());
		return resultBs;
	}

	/**
	 * 根據傳入Type查詢帳號
	 * 
	 * @param cusidn
	 * @param type
	 * @return
	 */
	@Cacheable(value = "RESTCache", key = "'N920' + #cusidn + '_' + #type", unless = "#result.result == false")
	public BaseResult N920_REST_Ehcache(String cusidn, String type) {
		log.debug("N920_REST_Ehcache start");
		log.debug(ESAPIUtil.vaildLog("cusidn >> " + cusidn));
		log.debug(ESAPIUtil.vaildLog("type >> " + type));
		log.debug("is in Cache");
		BaseResult bs = null;
		N920_REST_RQ rq = null;
		N920_REST_RS rs = null;
		try {
			bs = new BaseResult();
			rq = new N920_REST_RQ();
			// call REST API
			rq.setCUSIDN(cusidn);
			rq.setTYPE(type.toUpperCase());
			rs = restutil.send(rq, N920_REST_RS.class, rq.getAPI_Name(rq.getClass()));
			log.debug(ESAPIUtil.vaildLog("N920_REST_RS >> {}" + CodeUtil.toJson(rs)));
			if (rs != null) {
				CodeUtil.convert2BaseResult(bs, rs);
			} else {
				log.error("N920_REST_RS is null");
				bs.setMsgCode(ResultCode.SYS_ERROR_TEL);

			}
		} catch (Exception e) {
			log.error("", e);
			bs.setMsgCode(ResultCode.SYS_ERROR_TEL);

		}
		return bs;
	}

	/**
	 * 根據傳入Type查詢帳號
	 * 
	 * @param cusidn
	 * @param type
	 * @return
	 */
	@Cacheable(value = "RESTCache", key = "'N921' + #cusidn + '_' + #type", unless = "#result.result == false")
	public BaseResult N921_REST_Ehcache(String cusidn, String type) {
		log.trace("N921_REST_Ehcache start");
		log.trace(ESAPIUtil.vaildLog("cusidn >> " + cusidn));
		log.trace(ESAPIUtil.vaildLog("type >> " + type));
		BaseResult bs = null;
		N921_REST_RQ rq = null;
		N921_REST_RS rs = null;
		try {
			bs = new BaseResult();
			rq = new N921_REST_RQ();
			// call REST API
			rq.setCUSIDN(cusidn);
			rq.setTYPE(type);
			rs = restutil.send(rq, N921_REST_RS.class, rq.getAPI_Name(rq.getClass()));
			log.trace(ESAPIUtil.vaildLog("N921_REST_RS >>{}" + rs));
			if (rs != null) {
				CodeUtil.convert2BaseResult(bs, rs);
			} else {
				log.error("N921_REST_RS is null");
				bs.setMsgCode(ResultCode.SYS_ERROR_TEL);

			}
		} catch (Exception e) {
			log.error("", e);
			bs.setMsgCode(ResultCode.SYS_ERROR_TEL);

		}
		return bs;
	}

	/**
	 * 根據傳入Type查詢帳號
	 * 
	 * @param cusidn
	 * @param type
	 * @return
	 */
	public String getRiskFlag(String cusidn) {
		String RiskFlag = null;
		try {
			RiskFlag = txnRiskUsrDao.getRISKLOG(cusidn);
		} catch (Exception e) {
			log.error("RiskFlag error >>{}", e);
		}
		return RiskFlag;
	}

	public void updateCusInvAttrHistAgree(String FDHISTID , String agree) {
		
		TXNCUSINVATTRHIST po = txnCusInvAttrHistDao.get(TXNCUSINVATTRHIST.class, Long.parseLong(FDHISTID));
		po.setAGREE(agree);
		txnCusInvAttrHistDao.update(po);
	
	}
	
	public boolean checkBnkcod(Entry<String, Object> c) {
		boolean result=false;
		String BNKjson = (String) ((Map<String, Object>) c.getValue()).get("VALUE");
		String BNK = (String) CodeUtil.fromJson(BNKjson, Map.class).get("BNKCOD");
		if(StringUtils.isNumeric(BNK))result=true;
		return result;
	}

}
