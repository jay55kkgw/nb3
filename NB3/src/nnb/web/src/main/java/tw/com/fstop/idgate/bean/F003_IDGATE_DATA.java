package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class F003_IDGATE_DATA implements Serializable{

	@SerializedName(value = "BGROENO")
	private String BGROENO;
	@SerializedName(value = "BENACC")
	private String BENACC;
	@SerializedName(value = "BENTYPE")
	private String BENTYPE;
	@SerializedName(value = "CNTRY")
	private String CNTRY;
	@SerializedName(value = "CURAMT")
	private String CURAMT;
	@SerializedName(value = "CUSIDN")
	private String CUSIDN;
	@SerializedName(value = "CUSTID")
	private String CUSTID;
	@SerializedName(value = "CUSTYPE")
	private String CUSTYPE;
	@SerializedName(value = "ORDACC")
	private String ORDACC;
	@SerializedName(value = "REFNO")
	private String REFNO;
	@SerializedName(value = "RETCCY")
	private String RETCCY;
	@SerializedName(value = "REMITAMT")
	private String REMITAMT;
	@SerializedName(value = "SRCFUND")
	private String SRCFUND;
	@SerializedName(value = "TXCCY")
	private String TXCCY;
	@SerializedName(value = "TXAMT")
	private String TXAMT;
	@SerializedName(value = "TXDATE")
	private String TXDATE;
	@SerializedName(value = "VALDATE")
	private String VALDATE;
	@SerializedName(value = "CMTRANPAGE")
	private String CMTRANPAGE;
	@SerializedName(value = "ACBRH")
	private String ACBRH;
	@SerializedName(value = "BRH_CODE")
	private String BRH_CODE;
	@SerializedName(value = "DEPT")
	private String DEPT;
	@SerializedName(value = "TXBRH")
	private String TXBRH;
}
