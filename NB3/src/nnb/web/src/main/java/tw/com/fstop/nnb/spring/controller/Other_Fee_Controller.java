package tw.com.fstop.nnb.spring.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.idgate.bean.N8302_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N830_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N830_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N8311_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N8301_1_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N8301_2_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N8301_2_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N8301_3_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N8301_4_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N8301_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N8305_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N8315_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N8306_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N830_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N830_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N8302_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N831_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N831_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N8310_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N8302_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N8312_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N8316_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N8304_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N831_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N831_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N8314_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N8316_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N833_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N833_IDGATE_DATA_VIEW;
import tw.com.fstop.nnb.service.IdGateService;
import tw.com.fstop.nnb.service.Other_Fee_Service;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({ SessionUtil.CUSIDN, SessionUtil.PRINT_DATALISTMAP_DATA, SessionUtil.DOWNLOAD_DATALISTMAP_DATA,
		SessionUtil.PRINT_DATALISTMAP_DATA, SessionUtil.CONFIRM_LOCALE_DATA, SessionUtil.IDGATE_TRANSDATA,
		SessionUtil.RESULT_LOCALE_DATA, SessionUtil.CUSIDN, SessionUtil.TRANSFER_CONFIRM_TOKEN,
		SessionUtil.TRANSFER_RESULT_TOKEN, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, SessionUtil.RESULT_LOCALE_DATA,
		SessionUtil.STEP1_LOCALE_DATA, SessionUtil.BACK_DATA })
@Controller
@RequestMapping(value = "/OTHER/FEE")
public class Other_Fee_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());
	@Autowired
	Other_Fee_Service other_fee_service;
	@Autowired
	I18n i18n;
	@Autowired
	IdGateService idgateservice;

	@RequestMapping(value = "/withholding_cancel")
	public String withholding_cancel(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
	
			String target = "/other_fee/withholding_cancel";
		return target;
	}

	@RequestMapping(value = "/withholding_cancel_C")	
	public String withholding_cancel_C(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("withholding_cancel_C>>");
		
//		清除切換語系時暫存的資料
		SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			log.debug("withholding_cancel.cusidn >>{}", cusidn);
// 			解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			bs = other_fee_service.withholding_cancel_C(cusidn);
			
			 //IDGATE身分
			String idgateUserFlag = "N";
			Map<String, Object> IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model,SessionUtil.IDGATE_TRANSDATA, null);
			try {
				if (IdgateData == null) {
					IdgateData = new HashMap<String, Object>();
				}
				BaseResult tmp = idgateservice.checkIdentity(cusidn);
				if (tmp.getResult()) {
					idgateUserFlag = "Y";
				}
				tmp.addData("idgateUserFlag", idgateUserFlag);
				IdgateData.putAll((Map<String, String>) tmp.getData());
				SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);
			} catch (Exception e) {
				log.debug("idgateUserFlag error {}", e);
			}
			model.addAttribute("idgateUserFlag", idgateUserFlag);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("withholding_cancel error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				target = "/other_fee/withholding_cancel_C";
				
				model.addAttribute("withholding_cancel", bs);
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	@RequestMapping(value = "/withholding_cancel_C_confirm")	
	public String withholding_cancel_C_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("withholding_cancel_C_confirm>>");
		String jsondc = "";
		BaseResult bs = null;
		try {
			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = other_fee_service.getTxToken();
			log.trace(ESAPIUtil.vaildLog("electricity_fee.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN")));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("electricity_fee_confirm.jsondc: " + jsondc));
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			okMap.put("jsondc", jsondc);
			okMap.put("TOKEN", ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			model.addAttribute("withholding_cancel", okMap);
			
			// IDGATE transdata            		 
            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
            okMap.put("MEMO_C",i18n.getMsg(okMap.get("TYPE_str")));
            okMap.put("ADOPID", "N815");
            String adopid = "N815";
    		String title = "您有一筆自動扣繳取消交易待確認";
        	if(IdgateData != null) {
	            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
	            if(IdgateData.get("idgateUserFlag").equals("Y")) {
	            	okMap.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
	            	okMap.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
	            	
	            	log.debug("IDGATE ADOPID >> {}" , adopid);
	            	log.debug("IDGATE TITLE >> {}" , title);
	            	log.debug("IDGATE VALUE >> {}" , okMap);
	            	
					IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N833_IDGATE_DATA.class, N833_IDGATE_DATA_VIEW.class, okMap));
	                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	            }
        	}
            model.addAttribute("idgateAdopid", adopid);
            model.addAttribute("idgateTitle", title);
			
			
		} catch (Exception e) {
			log.error("withholding_cancel error >> {}",e);
		}finally {
			target = "/other_fee/withholding_cancel_C_confirm";
		}
		return target;
	}
	
	@RequestMapping(value = "/withholding_cancel_C_result")
	public String withholding_cancel_C_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {		
		String target = "/error";
		
		BaseResult bs = null;
		log.trace("withholding_cancel_C_result>>");
		Map<String, String> okMap = null;
		try {
			
			bs = new BaseResult();
            okMap = ESAPIUtil.validStrMap(reqParam);
			//okMap = reqParam;
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			
			log.trace("withholding_cancel_C_result.validate TXTOKEN...");
			log.trace("withholding_cancel_C_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
			log.trace("withholding_cancel_C_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
			
			// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
			if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
					okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
				// TXTOKEN第一階段驗證成功
				bs.setResult(Boolean.TRUE);
				log.trace("withholding_cancel_C_result.bs.step1 is successful...");
			}
			if(!bs.getResult()) {
				log.error(ESAPIUtil.vaildLog("withholding_cancel_C_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TOKEN")));
				throw new Exception();
			}
			// 重置bs，繼續往下驗證
			bs.reset();
			
			// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
			if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
					!okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
				// TXTOKEN第二階段驗證成功
				bs.setResult(Boolean.TRUE);
				log.trace("withholding_cancel_C_result.bs.step2 is successful...");
			}
			if(!bs.getResult()) {
				log.error(ESAPIUtil.vaildLog("withholding_cancel_C_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TOKEN")));
				throw new Exception();
			}
			// 重置bs，準備進行交易
			bs.reset();
			
			//IDgate驗證		 
            try {		 
                if(okMap.get("FGTXWAY").equals("7")) {		 
    				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
    				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N815_IDGATE");
    				okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
                    okMap.put("txnID", (String)IdgateData.get("txnID"));		 
                    okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
                }		 
            }catch(Exception e) {		 
                log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
            }
			
			
			
			bs =  other_fee_service.other_telecom_fee_C_result(cusidn, okMap);
			Map<String,Object>IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_health_insurance_result error >> {}",e);
		} finally {
			log.debug(ESAPIUtil.vaildLog("okMap>>{}"+okMap));
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TOKEN"));
			if (bs != null && bs.getResult())
			{
				target = "other_fee/withholding_cancel_C_result";
				model.addAttribute("result_data", bs);
				model.addAttribute("input_data", okMap);
			}
			else
			{
				//新增回上一頁的路徑
				bs.setPrevious("/OTHER/FEE/withholding_cancel_C");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	@RequestMapping(value = "/withholding_cancel_X")	
	public String withholding_cancel_X(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("withholding_cancel>>");
		
//		清除切換語系時暫存的資料
		SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, "");
		SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			log.debug("withholding_cancel.cusidn >>{}", cusidn);
// 			解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			bs = other_fee_service.withholding_cancel(cusidn);
			
			 //IDGATE身分
			String idgateUserFlag = "N";
			Map<String, Object> IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model,SessionUtil.IDGATE_TRANSDATA, null);
			try {
				if (IdgateData == null) {
					IdgateData = new HashMap<String, Object>();
				}
				BaseResult tmp = idgateservice.checkIdentity(cusidn);
				if (tmp.getResult()) {
					idgateUserFlag = "Y";
				}
				tmp.addData("idgateUserFlag", idgateUserFlag);
				IdgateData.putAll((Map<String, String>) tmp.getData());
				SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);
			} catch (Exception e) {
				log.debug("idgateUserFlag error {}", e);
			}
			model.addAttribute("idgateUserFlag", idgateUserFlag);
			
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("withholding_cancel error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				target = "/other_fee/withholding_cancel_X";
				
				model.addAttribute("withholding_cancel", bs);
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	@RequestMapping(value = "/withholding_cancel_X_step1")
	public String withholding_cancel_X_step1(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace(ESAPIUtil.vaildLog("withholding_cancel_confirm>>{}"+reqParam));
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// IKEY要使用的JSON:DC
			String jsondc = null;
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("withholding_cancel_X_step1.jsondc: {}"+ jsondc));
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				bs = other_fee_service.withholding_cancel_step1(cusidn,okMap);
				bs.addData("jsondc", jsondc);
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, bs);
			}			
		} catch (Exception e) {			
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("withholding_cancel_X_step1 error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				target = "/other_fee/withholding_cancel_X_step1";
				model.addAttribute("withholding_cancel_X_step1", bs);
			}else {
				model.addAttribute(BaseResult.ERROR, bs);				
			}
		}		
		return target;
	}
	
	@RequestMapping(value = "/withholding_cancel_X_confirm")
	public String withholding_cancel_X_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace(ESAPIUtil.vaildLog("withholding_cancel_confirm>>{}"+reqParam));
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// IKEY要使用的JSON:DC
			String jsondc = null;
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("withholding_cancel_confirm.jsondc: {}"+ jsondc));
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				bs = other_fee_service.withholding_cancel_confirm(cusidn,okMap);
				bs.addData("jsondc", jsondc);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}

				bs.addData("CUSIDN", cusidn);
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
			
				// IDGATE transdata            		 
	            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	    		String adopid = "N833";
	    		String title = "您有一筆自動扣繳取消交易待確認";
	    		
	    		// 拆出所選資料
	    		String rowdata = ((Map<String, String>) bs.getData()).get("ROWDATA");
	    		rowdata = rowdata.substring(1, rowdata.length()-1);
	    		String[] keyValuePairs = rowdata.split(",");             
	    		Map<String,String> result = new HashMap<>();               
	    		for(String pair : keyValuePairs)                        
	    		{
	    			String mapKey = "";
	    			String MapValue = "";
	    		    String[] entry = pair.split("=");                   
	    		    if (entry.length == 2) {
	    				mapKey = "".equals(entry[0].trim()) ? "" : entry[0].trim();
	    				MapValue = "".equals(entry[1].trim()) ? "" : entry[1].trim();
	    			} else {
	    				mapKey = entry[0].trim();
	    			}
	    		    result.put(mapKey, MapValue);
	    		}
	    		
	    		result = other_fee_service.withholding_cancel_idgatedata_trns(cusidn,result);
	    		
	    		if(IdgateData != null) {
		            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
		            	
		            	if("IHD".equals(result.get("MEMO"))) {
		        			//健保 
		            		//N8301 CUSIDN,TSFACN,TYPE,TYPNUM,HLHBRH,UNTNUM1,CUSIDN1,UNTNUM6,CUSIDN6,ITMNUM
//		        				reqParam.put("TYPE","01");
//		        				reqParam.put("CUSNUM", UNTNUM);
		            		log.debug("IDGATE ADOPID >> {}" , adopid);
			            	log.debug("IDGATE TITLE >> {}" , title);
			            	log.debug("IDGATE VALUE >> {}" , result);

		        				IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N833_IDGATE_DATA.class, N833_IDGATE_DATA_VIEW.class, result));
		        		}else if("ELE".equals(result.get("MEMO")) || "WAC".equals(result.get("MEMO")) || "WAT".equals(result.get("MEMO")) || "TLL".equals(result.get("MEMO"))) {
		        			//台電電費,台北市水費,台灣省水費,中華電信電話費 
		        			//N830 CUSIDN,TSFACN,TYPE,CUSNUM,ITMNUM
//		        				reqParam.put("CUSNUM", UNTNUM);
		        			log.debug("IDGATE ADOPID >> {}" , adopid);
			            	log.debug("IDGATE TITLE >> {}" , title);
			            	log.debug("IDGATE VALUE >> {}" , result);

		        				IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N833_IDGATE_DATA.class, N833_IDGATE_DATA_VIEW.class, result));
		        		}else if("ILD".equals(result.get("MEMO")) || "IKD".equals(result.get("MEMO")) || "CKD".equals(result.get("MEMO"))) {
		        			//勞保,新制勞退,舊制勞退 
		        			//N8302   CUSIDN,TSFACN,TYPE,UNTNUM,CUSIDNUN,UNTTEL,ITMNUM
		        			//N8302_1 CUSIDN,TSFACN,TYPE,UNTNUM,CUSIDNUN,UNTTEL,ITMNUM,CUSNUM
//		        				reqParam.put("AMOUNT", AMOUNT);
//		        				reqParam.put("CUSIDNUN",CUSNUM);
		        			log.debug("IDGATE ADOPID >> {}" , adopid);
			            	log.debug("IDGATE TITLE >> {}" , title);
			            	log.debug("IDGATE VALUE >> {}" , result);

		        				IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N833_IDGATE_DATA.class, N833_IDGATE_DATA_VIEW.class, result));
		        		}else {
		        				// ????????? 
//		        				bs.setResult(true);
//		        				MEMO = "";
		        		}
		                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
		            }
	        	}
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
			} catch (Exception e) {			
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("withholding_cancel_confirm error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				target = "/other_fee/withholding_cancel_X_confirm";
				model.addAttribute("withholding_cancel_confirm", bs);
			}else {
				model.addAttribute(BaseResult.ERROR, bs);				
			}
		}		
		return target;
	}
	
	@RequestMapping(value = "/withholding_cancel_X_result")
	public String withholding_cancel_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("withholding_cancel_result>>");
		try {
			bs = new BaseResult();
			
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			log.debug("withholding_cancel_result_cusidn >>{}", cusidn);
// 			解決Trust Boundary Violation			
			Map<String, String> okMap = reqParam;
			log.trace(ESAPIUtil.vaildLog("withholding_cancel_result_okMap >>{}"+ okMap));

//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale) 
			{
				log.trace(ESAPIUtil.vaildLog("locale>>{}" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				
				//IDgate驗證		 
                try {		 
                    if(okMap.get("FGTXWAY").equals("7")) {		 

        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N833_IDGATE");
                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
                    }		 
                }catch(Exception e) {		 
                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
                }
				
				bs = other_fee_service.withholding_cancel_result(cusidn,okMap);			
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			Map<String,Object>IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
		} catch (Exception e) {			
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("withholding_cancel_result error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				try {
					Map<String,Object> dataMap = (Map)bs.getData();
					log.trace("dataMap={}",dataMap);
					String MEMO = (String) dataMap.get("MEMO");					
					log.trace("MEMO_finally>>{}",MEMO);
					if("IHD".equals(MEMO)) {
						//健保
						target = "/other_fee/withholding_cancel_X_r_1";
						model.addAttribute("withholding_cancel_result", bs);
					}else if("ELE".equals(MEMO) || "WAC".equals(MEMO) || "WAT".equals(MEMO) || "TLL".equals(MEMO)) {
						//台電電費,台北市水費,台灣省水費,中華電信電話費
						target = "/other_fee/withholding_cancel_X_r_2";
						model.addAttribute("withholding_cancel_result", bs);
					}else if("ILD".equals(MEMO) || "IKD".equals(MEMO) || "CKD".equals(MEMO)) {
						//勞保,新制勞退,舊制勞退
						target = "/other_fee/withholding_cancel_X_r_3";
						model.addAttribute("withholding_cancel_result", bs);
					}else {
						bs.setMessage("",i18n.getMsg("LB.Check_no_data"));
						model.addAttribute(BaseResult.ERROR, bs);
						bs.setPrevious("/OTHER/FEE/withholding_cancel");
					}
				} catch (Exception e) {
					//Avoid Information Exposure Through an Error Message
					//e.printStackTrace();
					log.error("withholding_cancel_result error >> {}",e);
				}
								
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
				bs.setPrevious("/OTHER/FEE/withholding_cancel");
			}
		}			
		return target;
	}
	
	//(N8306)中華電信費 輸入頁
	@RequestMapping(value = "/other_telecom_fee")
	public String other_telecom_fee(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("other_telecom_fee start");
		BaseResult bsAcnos = null;
		BaseResult bsN810 = null;
		Map<String, String> back_data = null;
		try {
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			if(okMap.containsKey("back")) {
				if(okMap.get("back").equals("Y")) {
					back_data = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				}
			}
			bsAcnos = new BaseResult();
			bsN810 = new BaseResult();
			
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");

			// 登入時的身分證字號
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			bsAcnos = other_fee_service.N920_REST(cusidn, other_fee_service.OUT_ACNO);
			bsN810 = other_fee_service.other_telecom_fee_N810(cusidn, okMap);
			//IDGATE身分
			String idgateUserFlag="N";		 
			Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			try {		 
			 if(IdgateData==null) {
			  IdgateData = new HashMap<String, Object>();
			 }
			    BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
			    if(tmp.getResult()) {		 
			        idgateUserFlag="Y";                  		 
			    }		 
			    tmp.addData("idgateUserFlag",idgateUserFlag);		 
			    IdgateData.putAll((Map<String, String>) tmp.getData());
			    SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
			}catch(Exception e) {		
			    log.debug("idgateUserFlag error {}",e);		 
			}		 
			model.addAttribute("idgateUserFlag",idgateUserFlag);	
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_telecom_fee error >> {}",e);
		} finally {
			if(bsAcnos!=null && bsAcnos.getResult()) {
				if(bsN810!=null && bsN810.getResult()) {
					target = "other_fee/other_telecom_fee";
					model.addAttribute("result_data_acnos", bsAcnos);
					model.addAttribute("result_data_n810", bsN810);
					model.addAttribute("backenData", back_data);
				}else {
					bsN810.setPrevious("/INDEX/index");
					model.addAttribute(BaseResult.ERROR, bsN810);
				}
			}else {
				bsAcnos.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bsAcnos);
			}
		}
		return target;
	}

	//(N8306)中華電信費 確認頁
	@RequestMapping(value = "/other_telecom_fee_confirm")
	public String other_telecom_fee_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("other_telecom_fee_confirm start");
		BaseResult bs = null;
		String jsondc = "";
		Map<String, String> result = null;
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("electricity_fee_confirm.jsondc: " + jsondc));
			
			
			bs = new BaseResult();

			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = other_fee_service.getTxToken();
			log.trace(ESAPIUtil.vaildLog("electricity_fee.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN")));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			
			// 登入時的身分證字號
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			result = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null);
			log.debug("session result >> {}", result);
			result.put("CUSNUM", other_fee_service.getCUSNUM(result.get("CUSNUM1"),result.get("CUSNUM2")));
			result.put("TOKEN", ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			result.put("jsondc", jsondc); // IKEY要用不經過ESAPIUtil，會出錯
			log.trace(ESAPIUtil.vaildLog("electricity_fee_confirm.result: {}"+CodeUtil.toJson(result)));
			// IDGATE transdata            		 
            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
    		String adopid = "N8306";
    		String title = "您有一筆中華電信電信費代扣繳申請待確認";
        	if(IdgateData != null) {
	            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
	            if(IdgateData.get("idgateUserFlag").equals("Y")) {
	            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
	            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
					if(result.get("R1").equals("V1")) {
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N830_IDGATE_DATA.class, N8306_IDGATE_DATA_VIEW.class, result));
	                }else {
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N831_IDGATE_DATA.class, N8316_IDGATE_DATA_VIEW.class, result));
	                }
	                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	            }
        	}
            model.addAttribute("idgateAdopid", adopid);
            model.addAttribute("idgateTitle", title);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_telecom_fee_confirm error >> {}",e);
		} finally {
			if(result!=null) {
				target = "other_fee/other_telecom_fee_confirm";
				model.addAttribute("result_data", result);
			}else {
				bs.setPrevious("/OTHER/FEE/other_telecom_fee");
				model.addAttribute(BaseResult.ERROR, result);
			}
		}
		return target;
	}

	//(N8306)中華電信費 結果頁
	@RequestMapping(value = "/other_telecom_fee_result")
	public String other_telecom_fee_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {		
		String target = "/error";
		
		BaseResult bs = null;
		log.trace("other_telecom_fee_result>>");
		Map<String, String> okMap = null;
		try {
			
			bs = new BaseResult();
            okMap = ESAPIUtil.validStrMap(reqParam);
			//okMap = reqParam;
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null);
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				log.trace("other_telecom_fee_result.validate TXTOKEN...");
				log.trace("other_telecom_fee_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("other_telecom_fee_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_telecom_fee_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_telecom_fee_result TXTOKEN 不正確，TXTOKEN>>{}"+okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						!okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_telecom_fee_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_telecom_fee_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				//IDgate驗證		 
                try {		 
                    if(okMap.get("FGTXWAY").equals("7")) {		 

        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N8306_IDGATE");
                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
                    }		 
                }catch(Exception e) {		 
                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
                }
				log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+reqParam));
				if(reqParam.get("R1").equals("V1")) {
					bs = other_fee_service.other_telecom_fee_X_result(cusidn, okMap);
				}else {
					bs = other_fee_service.other_telecom_fee_C_result(cusidn, okMap);
				}
				bs.addData("CUSIDN", cusidn);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, okMap);
				
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_telecom_fee_result error >> {}",e);
		} finally {
			log.debug(ESAPIUtil.vaildLog("okMap>>{}"+okMap));
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TOKEN"));
			if (bs != null && bs.getResult())
			{
				target = "other_fee/other_telecom_fee_result";
				List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("result_data", bs);
				model.addAttribute("input_data", okMap);
			}
			else
			{
				//新增回上一頁的路徑
				bs.setPrevious("/OTHER/FEE/other_telecom_fee");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	//(N8301)健保費 輸入頁
	@RequestMapping(value = "/other_health_insurance")
	public String other_health_insurance(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("other_health_insurance start");
		BaseResult bsAcnos = null;
		Map<String, String> back_data = null;
		try {
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			if(okMap.containsKey("back")) {
				if(okMap.get("back").equals("Y")) {
					back_data = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				}
			}
			bsAcnos = new BaseResult();
			
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");

			// 登入時的身分證字號
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			bsAcnos = other_fee_service.N920_REST(cusidn, other_fee_service.OUT_ACNO);
			 //IDGATE身分
			 String idgateUserFlag="N";		 
			 Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			 try {		 
				 if(IdgateData==null) {
					 IdgateData = new HashMap<String, Object>();
				 }
				 BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
				 if(tmp.getResult()) {		 
					 idgateUserFlag="Y";                  		 
				 }		 
				 tmp.addData("idgateUserFlag",idgateUserFlag);		 
				 IdgateData.putAll((Map<String, String>) tmp.getData());
				 SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
			 }catch(Exception e) {		 
				 log.debug("idgateUserFlag error {}",e);		 
			 }		 
			 model.addAttribute("idgateUserFlag",idgateUserFlag);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_health_insurance error >> {}",e);
		} finally {
			if(bsAcnos!=null && bsAcnos.getResult()) {
				target = "other_fee/other_health_insurance";
				model.addAttribute("result_data", bsAcnos);
				model.addAttribute("backenData", back_data);
			}else {
				bsAcnos.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bsAcnos);
			}
		}
		return target;
	}

	
	/**
	 * (N8301)健保費 條約頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/other_health_insurance_detail")
	public String other_health_insurance_detail(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		// 解決Trust Boundary Violation 
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam); 
		SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, okMap);
		SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
		model.addAttribute("result_data", okMap);
		return "other_fee/other_health_insurance_detail";
	}
	
	
	//(N8301)健保費 確認頁
	@RequestMapping(value = "/other_health_insurance_confirm")
	public String other_health_insurance_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("other_health_insurance_confirm start");
		BaseResult bs = null;
		String jsondc = "";
		Map<String, String> result = null;
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("other_health_insurance_confirm.jsondc: " + jsondc));
			
			
			bs = new BaseResult();

			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = other_fee_service.getTxToken();
			log.trace("other_health_insurance_confirm.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			
			// 登入時的身分證字號
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			result = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null);
			log.debug("session result >> {}", result);
			result.put("TOKEN", ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			result.put("jsondc", jsondc); // IKEY要用不經過ESAPIUtil，會出錯
			log.trace(ESAPIUtil.vaildLog("other_health_insurance_confirm.result: {}"+ result));
			// IDGATE transdata            		 
            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
    		String adopid = "N8301";
    		String title = "您有一筆健保費代扣繳申請待確認";
        	if(IdgateData != null) {
	            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
	            if(IdgateData.get("idgateUserFlag").equals("Y")) {
	            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
	            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
					if(result.get("TYPNUM").equals("1")){
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N8301_IDGATE_DATA.class, N8301_1_IDGATE_DATA_VIEW.class, result));
					}else{
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N8301_2_IDGATE_DATA.class, N8301_2_IDGATE_DATA_VIEW.class, result));
					}
	               
	                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	            }
        	}
            model.addAttribute("idgateAdopid", adopid);
            model.addAttribute("idgateTitle", title);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_health_insurance_confirm error >> {}",e);
		} finally {
			if(result!=null) {
				target = "other_fee/other_health_insurance_confirm";
				model.addAttribute("result_data", result);
			}else {
				bs.setPrevious("/OTHER/FEE/other_health_insurance");
				model.addAttribute(BaseResult.ERROR, result);
			}
		}
		return target;
	}

	//(N8301)健保費 結果頁
	@RequestMapping(value = "/other_health_insurance_result")
	public String other_health_insurance_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {		
		String target = "/error";
		
		BaseResult bs = null;
		log.trace("other_telecom_fee_result>>");
		Map<String, String> okMap = null;
		try {
			
			bs = new BaseResult();
            okMap = ESAPIUtil.validStrMap(reqParam);
			//okMap = reqParam;
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null);
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				log.trace("other_health_insurance_result.validate TXTOKEN...");
				log.trace("other_health_insurance_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("other_health_insurance_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_health_insurance_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_health_insurance_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						!okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_health_insurance_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_health_insurance_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+reqParam));
				if(okMap.get("FGTXWAY").equals("7")) {		 

					Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
					Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N8301_IDGATE");
					okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
					okMap.put("txnID", (String)IdgateData.get("txnID"));		 
					okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
				}	

				bs = other_fee_service.other_health_insurance_result(cusidn, okMap);
				bs.addData("CUSIDN", cusidn);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, okMap);
				
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_health_insurance_result error >> {}",e);
		} finally {
			log.debug(ESAPIUtil.vaildLog("okMap>>{}"+okMap));
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TOKEN"));
			if (bs != null && bs.getResult())
			{
				target = "other_fee/other_health_insurance_result";
				List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("result_data", bs);
				model.addAttribute("input_data", okMap);
			}
			else
			{
				//新增回上一頁的路徑
				bs.setPrevious("/OTHER/FEE/other_health_insurance");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * N8303 電費代扣繳申請(輸入頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/other_electricity_fee")
	public String other_electricity_fee(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		log.trace("other_electricity_fee start");
		BaseResult bsAcnos = null;
		BaseResult bsN810 = null;
		Map<String, String> back_data = null;
		try {
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			if(okMap.containsKey("back")) {
				if(okMap.get("back").equals("Y")) {
					back_data = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				}
			}
			bsAcnos = new BaseResult();
			bsN810 = new BaseResult();
			
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");

			// 登入時的身分證字號
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			bsAcnos = other_fee_service.N920_REST(cusidn, other_fee_service.OUT_ACNO);
			bsN810 = other_fee_service.other_telecom_fee_N810(cusidn, okMap);
			
           //IDGATE身分
           String idgateUserFlag="N";		 
           Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
           try {		 
        	   if(IdgateData==null) {
        		   IdgateData = new HashMap<String, Object>();
        	   }
               BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
               if(tmp.getResult()) {		 
                   idgateUserFlag="Y";                  		 
               }		 
               tmp.addData("idgateUserFlag",idgateUserFlag);		 
               IdgateData.putAll((Map<String, String>) tmp.getData());
               SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
           }catch(Exception e) {		 
               log.debug("idgateUserFlag error {}",e);		 
           }		 
           model.addAttribute("idgateUserFlag",idgateUserFlag);
           
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_electricity_fee error >> {}",e);
		} finally {
			if(bsAcnos!=null && bsAcnos.getResult()) {
				if(bsN810!=null && bsN810.getResult()) {
					target = "other_fee/other_electricity_fee";
					model.addAttribute("result_data_acnos", bsAcnos);
					model.addAttribute("result_data_n810", bsN810);
					model.addAttribute("backenData", back_data);
				}else {
					bsN810.setPrevious("/INDEX/index");
					model.addAttribute(BaseResult.ERROR, bsN810);
				}
			}else {
				bsAcnos.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bsAcnos);
			}
		}
		return target;
	}	
	
	/**
	 * N8303 電費代扣繳申請(確認頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/other_electricity_fee_confirm")
	public String other_electricity_fee_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		log.trace("other_electricity_fee_confirm start");
		BaseResult bs = null;
		String jsondc = "";
		Map<String, String> result = null;
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("electricity_fee_confirm.jsondc: " + jsondc));
			
			
			bs = new BaseResult();

			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = other_fee_service.getTxToken();
			log.trace("other_electricity_fee_confirm.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			
			// 登入時的身分證字號
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			result = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null);
			log.debug("session result >> {}", result);
//			result.put("CUSNUM", other_fee_service.getCUSNUM(result.get("CUSNUM1"),result.get("CUSNUM2")));
			result.put("TOKEN", ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			result.put("jsondc", jsondc); // IKEY要用不經過ESAPIUtil，會出錯
			log.trace(ESAPIUtil.vaildLog("other_electricity_fee_confirm.result: {}"+ result));
			
			// IDGATE transdata            		 
            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
    		String adopid = "N8303";
    		String title = "您有一筆臺灣電力公司代扣繳申請待確認";
        	if(IdgateData != null) {
	            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
	            if(IdgateData.get("idgateUserFlag").equals("Y")) {
	            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
	            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
	            	result.put("TITLE", "臺灣電力公司代扣繳申請");
	            	result.put("TRANNAME", "申請");
					if(result.get("R1").equals("V1")) {
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N830_IDGATE_DATA.class, N8305_IDGATE_DATA_VIEW.class, result));
	                }else {
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N831_IDGATE_DATA.class, N8315_IDGATE_DATA_VIEW.class, result));
	                }
	                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	            }
        	}
            model.addAttribute("idgateAdopid", adopid);
            model.addAttribute("idgateTitle", title);
            
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_electricity_fee_confirm error >> {}",e);
		} finally {
			if(result!=null) {
				target = "other_fee/other_electricity_fee_confirm";
				model.addAttribute("result_data", result);
			}else {
				bs.setPrevious("/OTHER/FEE/other_telecom_fee");
				model.addAttribute(BaseResult.ERROR, result);
			}
		}
		return target;
	}
	
	/**
	 * N8303 電費代扣繳申請(結果頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/other_electricity_fee_result")
	public String other_electricity_fee_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		
		BaseResult bs = null;
		log.trace("other_telecom_fee_result>>");
		Map<String, String> okMap = null;
		try {
			
			bs = new BaseResult();
            okMap = ESAPIUtil.validStrMap(reqParam);
			//okMap = reqParam;
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null);
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				log.trace("other_electricity_fee_result.validate TXTOKEN...");
				log.trace("other_electricity_fee_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("other_electricity_fee_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_electricity_fee_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_electricity_fee_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						!okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_electricity_fee_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_electricity_fee_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+reqParam));
                //IDgate驗證		 
                try {		 
                    if(okMap.get("FGTXWAY").equals("7")) {		 

        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N8303_IDGATE");
                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
                    }		 
                }catch(Exception e) {		 
                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
                }  
				
				if(reqParam.get("R1").equals("V1")) {
					bs = other_fee_service.other_telecom_fee_X_result(cusidn, okMap);
				}else {
					bs = other_fee_service.other_telecom_fee_C_result(cusidn, okMap);
				}
				bs.addData("CUSIDN", cusidn);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, okMap);
				
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_electricity_fee_result error >> {}",e);
		} finally {
			log.debug(ESAPIUtil.vaildLog("okMap>>{}"+okMap));
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TOKEN"));
			if (bs != null && bs.getResult())
			{
				target = "other_fee/other_electricity_fee_result";
				List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("result_data", bs);
				model.addAttribute("input_data", okMap);
			}
			else
			{
				//新增回上一頁的路徑
				bs.setPrevious("/OTHER/FEE/other_electricity_fee");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * N8304 台北市水費代扣繳申請(輸入頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/other_water_tpe_fee")
	public String other_water_tpe_fee(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		log.trace("other_water_tpe_fee start");
		BaseResult bsAcnos = null;
		BaseResult bsN810 = null;
		Map<String, String> back_data = null;
		try {		
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			if(okMap.containsKey("back")) {
				if(okMap.get("back").equals("Y")) {
					back_data = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				}
			}
			bsAcnos = new BaseResult();
			bsN810 = new BaseResult();
			
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");

			// 登入時的身分證字號
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			bsAcnos = other_fee_service.N920_REST(cusidn, other_fee_service.OUT_ACNO);
			bsN810 = other_fee_service.other_telecom_fee_N810(cusidn, okMap);
			//IDGATE身分
           String idgateUserFlag="N";		 
           Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
           try {		 
        	   if(IdgateData==null) {
        		   IdgateData = new HashMap<String, Object>();
        	   }
               BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
               if(tmp.getResult()) {		 
                   idgateUserFlag="Y";                  		 
               }		 
               tmp.addData("idgateUserFlag",idgateUserFlag);		 
               IdgateData.putAll((Map<String, String>) tmp.getData());
               SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
           }catch(Exception e) {		 
               log.debug("idgateUserFlag error {}",e);		 
           }		 
           model.addAttribute("idgateUserFlag",idgateUserFlag);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_water_tpe_fee error >> {}",e);
		} finally {
			if(bsAcnos!=null && bsAcnos.getResult()) {
				if(bsN810!=null && bsN810.getResult()) {
					target = "other_fee/other_water_tpe_fee";
					model.addAttribute("result_data_acnos", bsAcnos);
					model.addAttribute("result_data_n810", bsN810);
					model.addAttribute("backenData", back_data);
				}else {
					bsN810.setPrevious("/INDEX/index");
					model.addAttribute(BaseResult.ERROR, bsN810);
				}
			}else {
				bsAcnos.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bsAcnos);
			}
		}
		return target;
	}
	
	/**
	 * N8304 台北市水費代扣繳申請(確認頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/other_water_tpe_fee_confirm")
	public String other_water_tpe_fee_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{

		String target = "/error";
		log.trace("other_water_tpe_fee_confirm start");
		BaseResult bs = null;
		String jsondc = "";
		Map<String, String> result = null;
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("other_water_tpe_fee_confirm.jsondc: " + jsondc));
			
			
			bs = new BaseResult();

			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = other_fee_service.getTxToken();
			log.trace("other_water_tpe_fee_confirm.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			
			// 登入時的身分證字號
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			result = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null);
			log.debug("session result >> {}", result);
//			result.put("CUSNUM", other_fee_service.getCUSNUM(result.get("CUSNUM1"),result.get("CUSNUM2")));
			result.put("TOKEN", ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			result.put("jsondc", jsondc); // IKEY要用不經過ESAPIUtil，會出錯
			log.trace(ESAPIUtil.vaildLog("other_water_tpe_fee_confirm.result: {}"+ result));
			// IDGATE transdata            		 
            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
    		String adopid = "N8304";
    		String title = "您有一筆臺北市水費代扣繳申請待申請";
        	if(IdgateData != null) {
	            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
	            if(IdgateData.get("idgateUserFlag").equals("Y")) {
	            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
	            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));	
					if(result.get("R1").equals("V1")) {
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N830_IDGATE_DATA.class, N8304_IDGATE_DATA_VIEW.class, result));
	                }else {
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N831_IDGATE_DATA.class, N8314_IDGATE_DATA_VIEW.class, result));
	                }
	                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	            }
        	}
            model.addAttribute("idgateAdopid", adopid);
            model.addAttribute("idgateTitle", title);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_water_tpe_fee_confirm error >> {}",e);
		} finally {
			if(result!=null) {
				target = "other_fee/other_water_tpe_fee_confirm";
				model.addAttribute("result_data", result);
			}else {
				bs.setPrevious("/OTHER/FEE/other_water_tpe_fee");
				model.addAttribute(BaseResult.ERROR, result);
			}
		}
		return target;
	}
	
	/**
	 * N8304 台北市水費代扣繳申請(結果頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/other_water_tpe_fee_result")
	public String other_water_tpe_fee_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{

		String target = "/error";
		
		BaseResult bs = null;
		log.trace("other_water_tpe_fee_result>>");
		Map<String, String> okMap = null;
		try {
			
			bs = new BaseResult();
            okMap = ESAPIUtil.validStrMap(reqParam);
			//okMap = reqParam;
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null);
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				log.trace("other_water_tpe_fee_result.validate TXTOKEN...");
				log.trace("other_water_tpe_fee_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("other_water_tpe_fee_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_water_tpe_fee_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_water_tpe_fee_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						!okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_water_tpe_fee_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_water_tpe_fee_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				//IDgate驗證		 
                try {		 
                    if(okMap.get("FGTXWAY").equals("7")) {		 

        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N8304_IDGATE");
                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
                    }		 
                }catch(Exception e) {		 
                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
                }  
				
				log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+reqParam));
				if(reqParam.get("R1").equals("V1")) {
					bs = other_fee_service.other_telecom_fee_X_result(cusidn, okMap);
				}else {
					bs = other_fee_service.other_telecom_fee_C_result(cusidn, okMap);
				}
				bs.addData("CUSIDN", cusidn);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, okMap);
				
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_water_tpe_fee_result error >> {}",e);
		} finally {
			log.debug(ESAPIUtil.vaildLog("okMap>>{}"+okMap));
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TOKEN"));
			if (bs != null && bs.getResult())
			{
				target = "other_fee/other_water_tpe_fee_result";
				List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("result_data", bs);
				model.addAttribute("input_data", okMap);
			}
			else
			{
				//新增回上一頁的路徑
				bs.setPrevious("/OTHER/FEE/other_water_tpe_fee");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * N8305 臺灣省水費代扣繳申請(輸入頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/other_water_tw_fee")
	public String other_water_tw_fee(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		log.trace("other_water_tw_fee start");
		BaseResult bsAcnos = null;
		BaseResult bsN810 = null;
		Map<String, String> back_data = null;
		try {
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			log.trace(ESAPIUtil.vaildLog("Map >> {}"+CodeUtil.toJson(okMap)));
			if(okMap.containsKey("back")) {
				log.trace(ESAPIUtil.vaildLog("back >> {}"+okMap.get("back")));
				log.trace(ESAPIUtil.vaildLog("back >> {}"+okMap.get("back").equals("Y")));
				if(okMap.get("back").equals("Y")) {
					back_data = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				}
			}
			log.trace("back_data >> {}",back_data);
			bsAcnos = new BaseResult();
			bsN810 = new BaseResult();
			
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");

			// 登入時的身分證字號
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			bsAcnos = other_fee_service.N920_REST(cusidn, other_fee_service.OUT_ACNO);
			bsN810 = other_fee_service.other_telecom_fee_N810(cusidn, okMap);

			//IDGATE身分		 
			String idgateUserFlag="N";		 
			SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA,new HashMap<>());		 
			try {		 
				BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
				if(tmp.getResult()) {		 
					idgateUserFlag="Y";                  		 
				}		 
				tmp.addData("idgateUserFlag",idgateUserFlag);		 
				SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA,((Map<String, String>) tmp.getData()));		 
			}catch(Exception e) {		 
				log.debug("idgateUserFlag error {}",e);		 
			}		 
			model.addAttribute("idgateUserFlag",idgateUserFlag);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_water_tw_fee error >> {}",e);
		} finally {
			if(bsAcnos!=null && bsAcnos.getResult()) {
				if(bsN810!=null && bsN810.getResult()) {
					target = "other_fee/other_water_tw_fee";
					model.addAttribute("result_data_acnos", bsAcnos);
					model.addAttribute("result_data_n810", bsN810);
					model.addAttribute("backenData", back_data);
				}else {
					bsN810.setPrevious("/INDEX/index");
					model.addAttribute(BaseResult.ERROR, bsN810);
				}
			}else {
				bsAcnos.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bsAcnos);
			}
		}
		return target;
	}
	
	/**
	 * N8305 臺灣省水費代扣繳申請(確認頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/other_water_tw_fee_confirm")
	public String other_water_tw_fee_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		log.trace("other_water_tw_fee_confirm start");
		BaseResult bs = null;
		String jsondc = "";
		Map<String, String> result = null;
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("other_water_tw_fee_confirm.jsondc: " + jsondc));
			
			
			bs = new BaseResult();

			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = other_fee_service.getTxToken();
			log.trace("other_water_tw_fee_confirm.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			
			// 登入時的身分證字號
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			result = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null);
			log.debug("session result >> {}", result);
//			result.put("CUSNUM", other_fee_service.getCUSNUM(result.get("CUSNUM1"),result.get("CUSNUM2")));
			result.put("TOKEN", ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			result.put("jsondc", jsondc); // IKEY要用不經過ESAPIUtil，會出錯
			log.trace(ESAPIUtil.vaildLog("other_water_tw_fee_confirm.result: {}"+ result));
			// IDGATE transdata            		 
            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
    		String adopid = "N8305";
    		String title = "您有一筆臺灣自來水公司代扣繳申請待確認";
        	if(IdgateData != null) {
	            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
	            if(IdgateData.get("idgateUserFlag").equals("Y")) {
	            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
	            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
					if(result.get("R1").equals("V1")) {
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N830_IDGATE_DATA.class, N8305_IDGATE_DATA_VIEW.class, result));
	                }else {
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N831_IDGATE_DATA.class, N8315_IDGATE_DATA_VIEW.class, result));
	                }
	                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	            }
        	}
            model.addAttribute("idgateAdopid", adopid);
            model.addAttribute("idgateTitle", title);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_water_tw_fee_confirm error >> {}",e);
		} finally {
			if(result!=null) {
				target = "other_fee/other_water_tw_fee_confirm";
				model.addAttribute("result_data", result);
			}else {
				bs.setPrevious("/OTHER/FEE/other_water_tw_fee");
				model.addAttribute(BaseResult.ERROR, result);
			}
		}
		return target;
	}
	
	/**
	 * N8305 臺灣省水費代扣繳申請(結果頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/other_water_tw_fee_result")
	public String other_water_tw_fee_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{

		String target = "/error";
		
		BaseResult bs = null;
		log.trace("other_telecom_fee_result>>");
		Map<String, String> okMap = null;
		try {
			
			bs = new BaseResult();
			okMap = ESAPIUtil.validStrMap(reqParam);
			//okMap = reqParam;
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null);
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				log.trace("other_water_tw_fee_result.validate TXTOKEN...");
				log.trace("other_water_tw_fee_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("other_water_tw_fee_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_water_tw_fee_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_water_tw_fee_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				//IDgate驗證		 
                try {		 
                    if(okMap.get("FGTXWAY").equals("7")) {		 

        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N8305_IDGATE");
                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
                    }		 
                }catch(Exception e) {		 
                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
                }
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						!okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_water_tw_fee_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_water_tw_fee_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+reqParam));
				if(reqParam.get("R1").equals("V1")) {
					bs = other_fee_service.other_telecom_fee_X_result(cusidn, okMap);
				}else {
					bs = other_fee_service.other_telecom_fee_C_result(cusidn, okMap);
				}
				bs.addData("CUSIDN", cusidn);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, okMap);
				
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_water_tw_fee_result error >> {}",e);
		} finally {
			log.debug(ESAPIUtil.vaildLog("okMap>>{}"+okMap));
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TOKEN"));
			if (bs != null && bs.getResult())
			{
				target = "other_fee/other_water_tw_fee_result";
				List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("result_data", bs);
				model.addAttribute("input_data", okMap);
			}
			else
			{
				//新增回上一頁的路徑
				bs.setPrevious("/OTHER/FEE/other_water_tw_fee");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * N8302 勞保費用代扣繳申請(輸入頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/other_labor_insurance")
	public String other_labor_insurance(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		log.trace("other_health_insurance start");
		BaseResult bsAcnos = null;
		Map<String, String> back_data = null;
		try {
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			if(okMap.containsKey("back")) {
				if(okMap.get("back").equals("Y")) {
					back_data = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				}
			}
			bsAcnos = new BaseResult();
			
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");

			// 登入時的身分證字號
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			bsAcnos = other_fee_service.N920_REST(cusidn, other_fee_service.OUT_ACNO);
			//IDGATE身分
			String idgateUserFlag="N";		 
			Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			try {		 
				if(IdgateData==null) {
					IdgateData = new HashMap<String, Object>();
				}
				BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
				if(tmp.getResult()) {		 
					idgateUserFlag="Y";                  		 
				}		 
				tmp.addData("idgateUserFlag",idgateUserFlag);		 
				IdgateData.putAll((Map<String, String>) tmp.getData());
				SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
			}catch(Exception e) {		 
				log.debug("idgateUserFlag error {}",e);		 
			}		 
			model.addAttribute("idgateUserFlag",idgateUserFlag);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_labor_insurance error >> {}",e);
		} finally {
			if(bsAcnos!=null && bsAcnos.getResult()) {
				target = "other_fee/other_labor_insurance";
				model.addAttribute("result_data", bsAcnos);
				model.addAttribute("backenData", back_data);
			}else {
				bsAcnos.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bsAcnos);
			}
		}
		return target;
	}
	
	/**
	 * N8302 勞保費用代扣繳申請(確認頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/other_labor_insurance_confirm")
	public String other_labor_insurance_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		log.trace("other_labor_insurance_confirm start");
		BaseResult bs = null;
		String jsondc = "";
		Map<String, String> result = null;
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("other_labor_insurance_confirm.jsondc: " + jsondc));
			
			
			bs = new BaseResult();

			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = other_fee_service.getTxToken();
			log.trace("other_labor_insurance_confirm.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			
			// 登入時的身分證字號
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			result = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null);
			log.debug("session result >> {}", result);
			result.put("TOKEN", ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			result.put("jsondc", jsondc); // IKEY要用不經過ESAPIUtil，會出錯
			log.trace(ESAPIUtil.vaildLog("other_labor_insurance_confirm.result: {}"+ result));
			// IDGATE transdata            		 
            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
    		String adopid = "N8302";
    		String title = "您有一筆勞保費用代扣繳申請待確認";
        	if(IdgateData != null) {
	            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
	            if(IdgateData.get("idgateUserFlag").equals("Y")) {
	            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
	            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
					IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N8302_IDGATE_DATA.class, N8302_IDGATE_DATA_VIEW.class, result));
	                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	            }
        	}
			model.addAttribute("idgateAdopid", adopid);
            model.addAttribute("idgateTitle", title);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_labor_insurance_confirm error >> {}",e);
		} finally {
			if(result!=null) {
				target = "other_fee/other_labor_insurance_confirm";
				model.addAttribute("result_data", result);
			}else {
				bs.setPrevious("/OTHER/FEE/other_labor_insurance");
				model.addAttribute(BaseResult.ERROR, result);
			}
		}
		return target;
	}
	
	/**
	 * N8302 勞保費用代扣繳申請(結果頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/other_labor_insurance_result")
	public String other_labor_insurance_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		
		BaseResult bs = null;
		log.trace("other_labor_insurance_result>>");
		Map<String, String> okMap = null;
		try {
			
			bs = new BaseResult();
            okMap = ESAPIUtil.validStrMap(reqParam);
		//	okMap = reqParam;
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null);
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				log.trace("other_labor_insurance_result.validate TXTOKEN...");
				log.trace("other_labor_insurance_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("other_labor_insurance_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_labor_insurance_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_labor_insurance_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						!okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_labor_insurance_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_labor_insurance_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+reqParam));
				if(okMap.get("FGTXWAY").equals("7")) {		 
					Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
					Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N8302_IDGATE");
					okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
					okMap.put("txnID", (String)IdgateData.get("txnID"));		 
					okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
				}
				bs = other_fee_service.other_labor_insurance_result(cusidn, okMap);
				bs.addData("CUSIDN", cusidn);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, okMap);
				
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_labor_insurance_result error >> {}",e);
		} finally {
			log.debug(ESAPIUtil.vaildLog("okMap>>{}"+okMap));
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TOKEN"));
			if (bs != null && bs.getResult())
			{
				target = "other_fee/other_labor_insurance_result";
				List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("result_data", bs);
				model.addAttribute("input_data", okMap);
			}
			else
			{
				//新增回上一頁的路徑
				bs.setPrevious("/OTHER/FEE/other_labor_insurance");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * N8310 新制勞工退休提繳費(輸入頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/other_new_retire")
	public String other_new_retire(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		log.trace("other_new_retire start");
		BaseResult bsAcnos = null;
		Map<String, String> back_data = null;
		try {
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			if(okMap.containsKey("back")) {
				if(okMap.get("back").equals("Y")) {
					back_data = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				}
			}
			bsAcnos = new BaseResult();
			
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");

			// 登入時的身分證字號
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			bsAcnos = other_fee_service.N920_REST(cusidn, other_fee_service.OUT_ACNO);
			 //IDGATE身分
			 String idgateUserFlag="N";		 
			 Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			 try {		 
				 if(IdgateData==null) {
					 IdgateData = new HashMap<String, Object>();
				 }
				 BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
				 if(tmp.getResult()) {		 
					 idgateUserFlag="Y";                  		 
				 }		 
				 tmp.addData("idgateUserFlag",idgateUserFlag);		 
				 IdgateData.putAll((Map<String, String>) tmp.getData());
				 SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
			 }catch(Exception e) {		 
				 log.debug("idgateUserFlag error {}",e);		 
			 }		 
			 model.addAttribute("idgateUserFlag",idgateUserFlag);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_new_retire error >> {}",e);
		} finally {
			if(bsAcnos!=null && bsAcnos.getResult()) {
				target = "other_fee/other_new_retire";
				model.addAttribute("result_data", bsAcnos);
				model.addAttribute("backenData", back_data);
			}else {
				bsAcnos.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bsAcnos);
			}
		}
		return target;
	}
	
	/**
	 * N8310 新制勞工退休提繳費(確認頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/other_new_retire_confirm")
	public String other_new_retire_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		log.trace("other_new_retire_confirm start");
		BaseResult bs = null;
		String jsondc = "";
		Map<String, String> result = null;
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("other_new_retire_confirm.jsondc: " + jsondc));
			
			
			bs = new BaseResult();

			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = other_fee_service.getTxToken();
			log.trace("other_new_retire_confirm.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			
			// 登入時的身分證字號
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			result = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null);
			log.debug("session result >> {}", result);
			result.put("TOKEN", ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			result.put("jsondc", jsondc); // IKEY要用不經過ESAPIUtil，會出錯
			log.trace(ESAPIUtil.vaildLog("other_new_retire_confirm.result: {}"+ result));
			// IDGATE transdata            		 
            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
    		String adopid = "N8310";
    		String title = "您有一筆新制勞工退休提繳費代扣繳申請待確認";
        	if(IdgateData != null) {
	            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
	            if(IdgateData.get("idgateUserFlag").equals("Y")) {
	            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
	            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
					IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N8302_IDGATE_DATA.class, N8310_IDGATE_DATA_VIEW.class, result));
	                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	            }
        	}
            model.addAttribute("idgateAdopid", adopid);
            model.addAttribute("idgateTitle", title);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_new_retire_confirm error >> {}",e);
		} finally {
			if(result!=null) {
				target = "other_fee/other_new_retire_confirm";
				model.addAttribute("result_data", result);
			}else {
				bs.setPrevious("/OTHER/FEE/other_new_retire");
				model.addAttribute(BaseResult.ERROR, result);
			}
		}
		return target;
	}
	
	/**
	 * N8310 新制勞工退休提繳費(結果頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/other_new_retire_result")
	public String other_new_retire_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		
		BaseResult bs = null;
		log.trace("other_labor_insurance_result>>");
		Map<String, String> okMap = null;
		try {
			
			bs = new BaseResult();
            okMap = ESAPIUtil.validStrMap(reqParam);
			//okMap = reqParam;
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null);
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				log.trace("other_labor_insurance_result.validate TXTOKEN...");
				log.trace("other_labor_insurance_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("other_labor_insurance_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_labor_insurance_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_labor_insurance_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						!okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_labor_insurance_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_labor_insurance_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+reqParam));
				if(okMap.get("FGTXWAY").equals("7")) {		 
					Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
					Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N8310_IDGATE");
					okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
					okMap.put("txnID", (String)IdgateData.get("txnID"));		 
					okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
				}
				bs = other_fee_service.other_labor_insurance_result(cusidn, okMap);
				bs.addData("CUSIDN", cusidn);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, okMap);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_new_retire_result error >> {}",e);
		} finally {
			log.debug(ESAPIUtil.vaildLog("okMap>>{}"+okMap));
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TOKEN"));
			if (bs != null && bs.getResult())
			{
				target = "other_fee/other_new_retire_result";
				List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("result_data", bs);
				model.addAttribute("input_data", okMap);
			}
			else
			{
				//新增回上一頁的路徑
				bs.setPrevious("/OTHER/FEE/other_new_retire");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * N8311 舊制勞工退休提繳費(輸入頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/other_old_retire")
	public String other_old_retire(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		log.trace("other_new_retire start");
		BaseResult bsAcnos = null;
		Map<String, String> back_data = null;
		try {
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			if(okMap.containsKey("back")) {
				if(okMap.get("back").equals("Y")) {
					back_data = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				}
			}
			bsAcnos = new BaseResult();
			
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");

			// 登入時的身分證字號
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			bsAcnos = other_fee_service.N920_REST(cusidn, other_fee_service.OUT_ACNO);
			//IDGATE身分
			String idgateUserFlag="N";		 
			Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			try {		 
				if(IdgateData==null) {
					IdgateData = new HashMap<String, Object>();
				}
				BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
				if(tmp.getResult()) {		 
					idgateUserFlag="Y";                  		 
				}		 
				tmp.addData("idgateUserFlag",idgateUserFlag);		 
				IdgateData.putAll((Map<String, String>) tmp.getData());
				SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
			}catch(Exception e) {		 
				log.debug("idgateUserFlag error {}",e);		 
			}		 
			model.addAttribute("idgateUserFlag",idgateUserFlag);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_old_retire error >> {}",e);
		} finally {
			if(bsAcnos!=null && bsAcnos.getResult()) {
				target = "other_fee/other_old_retire";
				model.addAttribute("result_data", bsAcnos);
				model.addAttribute("backenData", back_data);
			}else {
				bsAcnos.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bsAcnos);
			}
		}
		return target;
	}
	
	/**
	 * N8311 舊制勞工退休提繳費(確認頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/other_old_retire_confirm")
	public String other_old_retire_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		log.trace("other_old_retire_confirm start");
		BaseResult bs = null;
		String jsondc = "";
		Map<String, String> result = null;
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			
			
			bs = new BaseResult();

			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = other_fee_service.getTxToken();
			log.trace("other_old_retire_confirm.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			
			// 登入時的身分證字號
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			result = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null);
			
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(result),"UTF-8");
			log.trace("other_old_retire_confirm.jsondc: " + jsondc);
			
			log.debug("session result >> {}", result);
			result.put("TOKEN", ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			result.put("jsondc", jsondc); // IKEY要用不經過ESAPIUtil，會出錯
			log.trace("other_old_retire_confirm.result: {}", result);
			// IDGATE transdata            		 
            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
    		String adopid = "N8311";
    		String title = "您有一筆舊制勞工退休提繳費代扣繳申請待確認";
        	if(IdgateData != null) {
	            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
	            if(IdgateData.get("idgateUserFlag").equals("Y")) {
	            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
	            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
					IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N8302_IDGATE_DATA.class, N8311_IDGATE_DATA_VIEW.class, result));
	                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	            }
        	}
            model.addAttribute("idgateAdopid", adopid);
            model.addAttribute("idgateTitle", title);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_old_retire_confirm error >> {}",e);
		} finally {
			if(result!=null) {
				target = "other_fee/other_old_retire_confirm";
				model.addAttribute("result_data", result);
			}else {
				bs.setPrevious("/OTHER/FEE/other_old_retire");
				model.addAttribute(BaseResult.ERROR, result);
			}
		}
		return target;
	}
	
	/**
	 * N8311 舊制勞工退休提繳費(結果頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/other_old_retire_result")
	public String other_old_retire_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		
		BaseResult bs = null;
		log.trace("other_old_retire_result>>");
		Map<String, String> okMap = null;
		try {
			bs = new BaseResult();
            okMap = ESAPIUtil.validStrMap(reqParam);
			//okMap = reqParam;
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null);
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				log.trace("other_old_retire_result.validate TXTOKEN...");
				log.trace("other_old_retire_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("other_old_retire_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_old_retire_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_old_retire_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						!okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_old_retire_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_old_retire_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+reqParam));
				if(okMap.get("FGTXWAY").equals("7")) {		 
					Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
					Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N8311_IDGATE");
					okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
					okMap.put("txnID", (String)IdgateData.get("txnID"));		 
					okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
				}
				bs = other_fee_service.other_labor_insurance_result(cusidn, okMap);
				bs.addData("CUSIDN", cusidn);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, okMap);
				
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_old_retire_result error >> {}",e);
		} finally {
			log.debug(ESAPIUtil.vaildLog("okMap>>{}"+okMap));
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TOKEN"));
			if (bs != null && bs.getResult())
			{
				target = "other_fee/other_old_retire_result";
				List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("result_data", bs);
				model.addAttribute("input_data", okMap);
			}
			else
			{
				//新增回上一頁的路徑
				bs.setPrevious("/OTHER/FEE/other_old_retire");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 約定條款
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/x_article")
	public String x_article(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		
		BaseResult bs = null;
		log.trace("other_old_retire_result>>");
		Map<String, String> okMap = null;
		try {
			bs = new BaseResult();
			// 解決Trust Boundary Violation
			okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
//				log.trace("locale>>" + okMap.get("locale"));
				okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null);
			}
			if (!hasLocale) {
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, okMap);
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
			}
			model.addAttribute("result_data", okMap);
			target = "other_fee/other_x_article";
		}catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("x_article error >> {}",e);
		}
		return target;
	}
	
	/**
	 * 信用卡約定條款
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/c_article")
	public String c_article(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		
		BaseResult bs = null;
		log.trace("other_old_retire_result>>");
		Map<String, String> okMap = null;
		try {
			bs = new BaseResult();
			// 解決Trust Boundary Violation
			okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
//				log.trace("locale>>" + okMap.get("locale"));
				okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null);
			}
			if (!hasLocale) {
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, okMap);
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
			}
			model.addAttribute("result_data", okMap);
			target = "other_fee/other_c_article";
		}catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("c_article error >> {}",e);
		}
		return target;
	}
	
	
	//委託轉帳代繳公用事業費用約定條款
	@RequestMapping(value = "/other_x_article")
	public String other_x_article(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		
		BaseResult bs = null;
		log.trace("other_old_retire_result>>");
		Map<String, String> okMap = null;
		try {
			bs = new BaseResult();
			// 解決Trust Boundary Violation
			okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
//				log.trace("locale>>" + okMap.get("locale"));
				okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null);
			}
			if (!hasLocale) {
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, okMap);
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
			}
			model.addAttribute("result_data", okMap);
			target = "other_fee/other_x_article";
		}catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_x_article error >> {}",e);
		}
		return target;
	}
	//委託信用卡代繳公用事業費用約定條款
	@RequestMapping(value = "/other_c_article")
	public String other_c_article(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		
		BaseResult bs = null;
		log.trace("other_old_retire_result>>");
		Map<String, String> okMap = null;
		try {
			bs = new BaseResult();
			// 解決Trust Boundary Violation
			okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
//				log.trace("locale>>" + okMap.get("locale"));
				okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null);
			}
			if (!hasLocale) {
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, okMap);
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
			}
			model.addAttribute("result_data", okMap);
			target ="other_fee/other_c_article";
		}catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_c_article error >> {}",e);
		}
		return target;
	}
	
	
	/**
	 * 勞保條款
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/labor_terms")
	public String labor_terms(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		
		BaseResult bs = null;
		log.trace("other_old_retire_result>>");
		Map<String, String> okMap = null;
		try {
			bs = new BaseResult();
			// 解決Trust Boundary Violation
			okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
//				log.trace("locale>>" + okMap.get("locale"));
				okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null);
			}
			if (!hasLocale) {
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, okMap);
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
			}
			target = "/other_fee/labor_terms";
			model.addAttribute("result_data", okMap);
		}catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("labor_terms error >> {}",e);
		}
		return target;
	}
	
	/**
	 * 新制勞退條款
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/new_retire_terms")
	public String new_retire_terms(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		
		BaseResult bs = null;
		log.trace("other_old_retire_result>>");
		Map<String, String> okMap = null;
		try {
			bs = new BaseResult();
			// 解決Trust Boundary Violation
			okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
//				log.trace("locale>>" + okMap.get("locale"));
				okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null);
			}
			if (!hasLocale) {
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, okMap);
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
			}
			target = "/other_fee/new_retire_terms";
			model.addAttribute("result_data", okMap);
		}catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("new_retire_terms error >> {}",e);
		}
		return target;
	}
	
	/**
	 * 舊制勞退條款
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/old_retire_terms")
	public String old_retire_terms(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		
		BaseResult bs = null;
		log.trace("other_old_retire_result>>");
		Map<String, String> okMap = null;
		try {
			bs = new BaseResult();
			// 解決Trust Boundary Violation
			okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
//				log.trace("locale>>" + okMap.get("locale"));
				okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null);
			}
			if (!hasLocale) {
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, okMap);
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
			}
			target = "/other_fee/old_retire_terms";
			model.addAttribute("result_data", okMap);
		}catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("old_retire_terms error >> {}",e);
		}
		return target;
	}
}
