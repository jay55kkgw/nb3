package tw.com.fstop.nnb.spring.controller;

import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.custom.annotation.ISTXNLOG;
import tw.com.fstop.nnb.service.Apply_CreditLoan_Service;
import tw.com.fstop.nnb.service.Fcy_Reservation_Service;
import tw.com.fstop.tbb.nnb.dao.AdmMsgCodeDao;
import tw.com.fstop.tbb.nnb.dao.TxnLoanApplyDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.web.util.WebUtil;

/**
 * 申請小額信貸
 * 
 * @author USER
 *
 */
@SessionAttributes({ SessionUtil.DOWNLOAD_DATALISTMAP_DATA, SessionUtil.PRINT_DATALISTMAP_DATA,
		SessionUtil.RESULT_LOCALE_DATA, SessionUtil.CONFIRM_LOCALE_DATA, SessionUtil.RESULT_LOCALE_DATA,SessionUtil.LOANTYPE,
		SessionUtil.CUSIDN,SessionUtil.DPMYEMAIL})
@Controller
@RequestMapping(value = "/APPLY/CREDIT")
public class Apply_CreditLoan_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Apply_CreditLoan_Service apply_CreditLoan_Service;
	@Autowired
	private Fcy_Reservation_Service fcy_reservation_service;
	@Autowired
	private TxnLoanApplyDao txnLoanApplyDao;
    @Autowired
    private AdmMsgCodeDao admMsgCodeDao;
	/**
	 * NA011 小額信貸申請資料查詢
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
    @ISTXNLOG(value="NA011")
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/apply_creditloan_query")
	public String apply_creditloan_query(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = new BaseResult();
		try {
			String cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null));
			cusidn = new String(Base64.getDecoder().decode(cusidn), "utf-8");

			bs = apply_CreditLoan_Service.applyCreditLoanQuery(cusidn);

			if (bs.getResult()) {
				// 列印資料
				Map<String, String> bsData = (Map<String, String>) bs.getData();
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, bsData.get("REC"));
				model.addAttribute("BaseResult", bs);
			}
			
		} catch (Exception e) {
			log.error("apply_creditloan_query error", e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/apply_credit/apply_creditloan_query";
			} else {
				 // 新增回上一頁的路徑 
				bs.setPrevious("/INDEX/index"); 
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * NA01 小額信貸申請_p1
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/apply_creditloan")
	public String apply_creditloan(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				// 登入時的身分證字號
				String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
				// session有無CUSIDN
				if (!"".equals(cusidn) && cusidn != null) {
					// 解密成明碼
					cusidn = new String(Base64.getDecoder().decode(cusidn));
					log.trace("cusidn: " + cusidn);
				} else {
					log.error("session no cusidn!!!");
				}
			} else {
				// 登入時的身分證字號
				String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
				// session有無CUSIDN
				if (!"".equals(cusidn) && cusidn != null) {
					// 解密成明碼
					cusidn = new String(Base64.getDecoder().decode(cusidn));
					log.trace("cusidn: " + cusidn);
				} else {
					log.error("session no cusidn!!!");
				}
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("apply_creditloan error >> {}",e);
		} finally {
			target = "/apply_credit/apply_creditloan_p1";
		}
		return target;
	}

	/**
	 * NA01 小額信貸申請_p2
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/apply_creditloan_p2")
	public String apply_creditloan_p2(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs=new BaseResult();
		try {
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.LOANTYPE, null));
			}
			else
			{
				// 登入時的身分證字號
				String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
				// session有無CUSIDN
				if (!"".equals(cusidn) && cusidn != null) {
					// 解密成明碼
					cusidn = new String(Base64.getDecoder().decode(cusidn));
					log.trace("cusidn: " + cusidn);
					log.trace(ESAPIUtil.vaildLog("okMap.LOANTYPE >> " + okMap.get("LOANTYPE")));
					SessionUtil.addAttribute(model, SessionUtil.LOANTYPE, okMap.get("LOANTYPE"));
					bs.addData("LOANTYPE",okMap.get("LOANTYPE"));
				} else {
					log.error("session no cusidn!!!");
				}
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("apply_creditloan_p2 error >> {}",e);
		} finally {
			model.addAttribute("bs",bs);
			target = "/apply_credit/apply_creditloan_p2";
		}
		return target;
	}

	/**
	 * NA01 小額信貸申請_p3
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/apply_creditloan_p3")
	public String apply_creditloan_p3(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = new BaseResult();
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {	
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.LOANTYPE, null));
			} else {
				bs.addData("LOANTYPE", reqParam.get("LOANTYPE"));
				//SessionUtil.addAttribute(model, SessionUtil.LOANTYPE, okMap.get("LOANTYPE"));
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("apply_creditloan_p3 error >> {}",e);
		} finally {
			model.addAttribute("bs",bs);
			target = "/apply_credit/apply_creditloan_p3";
		}
		return target;
	}

	/**
	 * NA01 小額信貸申請_p4
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/apply_creditloan_p4")
	public String apply_creditloan_p4(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs=new BaseResult();
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.LOANTYPE, null));
			} else {
				bs.addData("LOANTYPE", reqParam.get("LOANTYPE"));
				//SessionUtil.addAttribute(model, SessionUtil.LOANTYPE, okMap.get("LOANTYPE"));
			}
		} catch (Exception e) {
			log.error("apply_creditloan error", e);
		} finally {
			model.addAttribute("bs", bs);
			target = "/apply_credit/apply_creditloan_p4";
		}
		return target;
	}

	/**
	 * NA01 小額信貸申請_p5
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/apply_creditloan_p5")
	public String apply_creditloan_p5(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				// 登入時的身分證字號
				String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
				// session有無CUSIDN
				if (!"".equals(cusidn) && cusidn != null) {
					// 解密成明碼
					cusidn = new String(Base64.getDecoder().decode(cusidn));
					log.trace("cusidn: " + cusidn);
				} else {
					log.error("session no cusidn!!!");
				}
			} else {
				// 登入時的身分證字號
				String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
				// session有無CUSIDN
				if (!"".equals(cusidn) && cusidn != null) {
					// 解密成明碼
					cusidn = new String(Base64.getDecoder().decode(cusidn));
					log.trace("cusidn: " + cusidn);
				} else {
					log.error("session no cusidn!!!");
				}
			}
		} catch (Exception e) {
			log.error("apply_creditloan error", e);
		} finally {
			model.addAttribute("LOANTYPE", okMap.get("LOANTYPE"));
			target = "/apply_credit/apply_creditloan_p5";
		}
		return target;
	}
	/**
	 * NA01 小額信貸申請_p6
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/apply_creditloan_p6")
	public String apply_creditloan_p6(HttpServletRequest request,HttpServletResponse response,@RequestParam Map<String,String> reqParam,Model model){
		String target = "/error";
		BaseResult bs=null;
		log.debug("apply_creditloan_p6");
		try{
			log.debug("apply_creditloan_p6 controller start");
			log.debug(ESAPIUtil.vaildLog("REQPARAM >> " + CodeUtil.toJson(reqParam)));
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			bs = new BaseResult();
			if (hasLocale) {
				//todo 確認塞在哪個session
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
			} else {
				String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
				okMap.put("cusidn",cusidn);
				String hiddenCUSIDN = WebUtil.hideID(cusidn);
				String DPMYEMAIL = (String)SessionUtil.getAttribute(model,SessionUtil.DPMYEMAIL,null);
				log.debug("DPMYEMAIL ={}",DPMYEMAIL);
				bs.addAllData(okMap);
				bs = apply_CreditLoan_Service.getN960Data(okMap);
				bs.addData("hiddenCUSIDN",hiddenCUSIDN);
				bs.addData("DPMYEMAIL",DPMYEMAIL);
				bs.addData("cusidn",cusidn);
				log.debug("N960 DATA >>{}",CodeUtil.toJson(bs));
				if(bs == null || !bs.getResult()){
					model.addAttribute(BaseResult.ERROR,bs);
					return target;
				}	
				apply_CreditLoan_Service.prepareApplyCreditloanP6Data(okMap,bs);
				log.debug("BSDATA >> {}",CodeUtil.toJson(bs));
				//存入session todo確認放哪頁
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
			}		
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("apply_creditloan_p6 error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				model.addAttribute("bs",bs);
				target = "/apply_credit/apply_creditloan_p6";
			}else {
				bs.setPrevious("/APPLY/CREDIT/apply_creditloan");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	/**
	 * NA01 小額信貸申請_p7
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/apply_creditloan_p7")
	public String apply_creditloan_p7(HttpServletRequest request,HttpServletResponse response,@RequestParam Map<String,String> reqParam,Model model){
		String target = "/error";
		BaseResult bs=null;
		try{
			log.debug("apply_creditloan_p7 controller start");
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			bs = new BaseResult();
			if (hasLocale) {
				//todo 確認塞在哪個session
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
			} else {
				String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
				okMap.put("cusidn",cusidn);
				log.trace(ESAPIUtil.vaildLog("REQPARAM >> " + CodeUtil.toJson(okMap)));
				//稱謂#取代為空白
				String[] reltypes = {"RELTYPE1","RELTYPE2","RELTYPE3","RELTYPE4"};
				for (String reltype : reltypes)
				{			
					if("#".equals(okMap.get(reltype)))
					{
						okMap.put(reltype, "");
					}		
				}
				
				//使用者的IP
				String IP = WebUtil.getIpAddr(request);
				log.debug(ESAPIUtil.vaildLog("IP >> " + IP)); 
				okMap.put("IP",IP);
				bs.addAllData(okMap);
				// 現在時間
				bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				apply_CreditLoan_Service.prepareApplyCreditloanP7Data(okMap,bs);
				log.debug("BSDATA >>{}",CodeUtil.toJson(bs));
				//存入session todo確認放哪頁
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
			}	
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("apply_creditloan_p7 error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				model.addAttribute("bs",bs);
				target = "/apply_credit/apply_creditloan_p7";
			}else {
				bs.setPrevious("/APPLY/CREDIT/apply_creditloan");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	/**
	 * NA01 小額信貸申請_p8
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@ISTXNLOG(value="NA01")
	@PostMapping(value = "/apply_creditloan_p8")
	public String apply_creditloan_p8(HttpServletRequest request,HttpServletResponse response,@RequestParam("picFile1") MultipartFile picFile1,@RequestParam("picFile11") MultipartFile picFile11,@RequestParam("picFile2") MultipartFile picFile2,@RequestParam("picFile3") MultipartFile picFile3,@RequestParam Map<String,String> reqParam,Model model){
		String target = "/error";
		BaseResult bs=new BaseResult();
		log.debug("apply_creditloan_p8");
		
		try{
			log.debug("apply_creditloan_p8 controller start");
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			bs = new BaseResult();
			if (hasLocale) {
				//todo 確認塞在哪個session
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
			} else {
				//清空session
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA,"");
				
				String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
				okMap.put("CUSIDN",cusidn);
				log.debug(ESAPIUtil.vaildLog("REQPARAM >> " + CodeUtil.toJson(okMap)));
				//交易密碼要打N951電文驗證
				bs = fcy_reservation_service.N951_REST(cusidn,okMap);										
				if(!"0".equals(bs.getMsgCode())){
					bs.setPrevious("/APPLY/CREDIT/apply_creditloan");
					model.addAttribute(BaseResult.ERROR,bs);
					return target;
				}
				//圖片檢核及處理
				target=apply_CreditLoan_Service.FormValidation(okMap,bs,picFile1,picFile11,picFile2,picFile3);
				if(!bs.getResult()) {
					model.addAttribute("bs",bs);
					//為了不跳錯誤頁，讓他返回第七頁所以設成True
					bs.setResult(Boolean.TRUE);
					return target;
				}
				
				int result = apply_CreditLoan_Service.prepareApplyCreditloanP8Data(okMap,picFile1,picFile11,picFile2,picFile3,bs);
				//存入session todo確認放哪頁
				if(result == 1) { //DB ERROR
					bs.setResult(false);
					bs.setMsgCode("M350");
					bs.setErrorMessage("M350",admMsgCodeDao.getCodeMsg("M350"));
				}
				else if(result != 0) { //上傳錯誤
					bs.setResult(false);
					bs.setMsgCode("FE0007");
					bs.setErrorMessage("FE0007",admMsgCodeDao.getCodeMsg("FE0007"));
				}
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				// 現在時間
				bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				log.debug("BSDATA >>{}",CodeUtil.toJson(bs));
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("apply_creditloan_p8 error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				model.addAttribute("bs",bs);
				target = "/apply_credit/apply_creditloan_p8";
			}else {
				bs.setPrevious("/APPLY/CREDIT/apply_creditloan");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	/**
	 * 吾乃為了多語系而生
	 * NA01 小額信貸申請_p8
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@GetMapping(value = "/apply_creditloan_p8")
	public String apply_creditloan_p8_i8n(HttpServletRequest request,HttpServletResponse response,@RequestParam Map<String,String> reqParam,Model model){
		String target = "/error";
		BaseResult bs=new BaseResult();
		log.debug("apply_creditloan_p8");
		
		try{
			log.debug("apply_creditloan_p8 controller start");
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			bs = new BaseResult();
			if (hasLocale) {
				//todo 確認塞在哪個session
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("apply_creditloan_p8_i8n error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				model.addAttribute("bs",bs);
				target = "/apply_credit/apply_creditloan_p8";
			}else {
				bs.setPrevious("/APPLY/CREDIT/apply_creditloan");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	/**
	 * 取得市區鄉鎮資料的AJAX
	 */
	@RequestMapping(value="/getAreaListAjax",produces={"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult getAreaListAjax(@RequestParam Map<String,String> requestParam){
		log.debug("IN getAreaListAjax");
		
		BaseResult baseResult = new BaseResult();
		baseResult.setResult(Boolean.FALSE);
		
		try{
			String CITY = requestParam.get("CITY");
			log.debug(ESAPIUtil.vaildLog("CITY >> " + CITY));
			List<Map<String,String>> areaList = apply_CreditLoan_Service.getAreaList(CITY);
			
			if(!areaList.isEmpty()){
				baseResult.setResult(Boolean.TRUE);
				String dataString = new Gson().toJson(areaList,areaList.getClass());
				log.debug(ESAPIUtil.vaildLog("dataString >> " + dataString));
				baseResult.setData(dataString);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getAreaListAjax error >> {}",e);
		}
		return baseResult;
	}
	/**
	 * 取得郵遞區號資料的AJAX
	 */
	@RequestMapping(value="/getZipCodeByCityAreaAjax",produces={"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult getZipCodeByCityAreaAjax(@RequestParam Map<String,String> requestParam){
		log.debug("IN getZipCodeByCityAreaAjax");
		
		BaseResult baseResult = new BaseResult();
		baseResult.setResult(Boolean.FALSE);
		
		try{
			String CITY = requestParam.get("CITY");
			log.debug(ESAPIUtil.vaildLog("CITY >> " + CITY));
			String AREA = requestParam.get("AREA");
			log.debug(ESAPIUtil.vaildLog("AREA >> " + AREA));
			
			String zipCode = apply_CreditLoan_Service.getZipCodeByCityArea(CITY,AREA);
			
			if(!"".equals(zipCode)){
				baseResult.setResult(Boolean.TRUE);
				
				baseResult.setData(zipCode);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getZipCodeByCityAreaAjax error >> {}",e);
		}
		return baseResult;
	}
}