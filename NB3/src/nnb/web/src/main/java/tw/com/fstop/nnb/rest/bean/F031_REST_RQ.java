package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * 
 * 功能說明 :一般網銀外幣匯出匯款受款人約定檔擷取
 *
 */
public class F031_REST_RQ extends BaseRestBean_FX implements Serializable 
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5432039036289696028L;
	
	private String CUSIDN ;

	public String getCUSIDN()
	{
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN)
	{
		CUSIDN = cUSIDN;
	}
	
	

}
