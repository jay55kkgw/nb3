package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N855canceldata_REST_RQ  extends BaseRestBean_TW implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2768592258122963365L;
	private String	SrcID ;
	private String	KeyID ;
	private String	DivData ;
	private String	ICV ;
	private String	MAC ;
	private String	TxnDatetime ;
	private String	STAN ;
	private String	BillerBan ;
	private String	BusType ;
	private String	BillerID ;
	private String	PaymentType ;
	private String	FeeID ;
	private String	CustIdnBan ;
	private String	CustBillerAcnt ;
	private String	CustBankAcnt ;
	private String	CustIdnBan_Dec ;
	private String	CustBillerAcnt_Dec ;
	private String	CustBankAcnt_Dec ;
	private String	ReservedField ;
	private String  RCode;
	public String getSrcID() {
		return SrcID;
	}
	public void setSrcID(String srcID) {
		SrcID = srcID;
	}
	public String getKeyID() {
		return KeyID;
	}
	public void setKeyID(String keyID) {
		KeyID = keyID;
	}
	public String getDivData() {
		return DivData;
	}
	public void setDivData(String divData) {
		DivData = divData;
	}
	public String getICV() {
		return ICV;
	}
	public void setICV(String iCV) {
		ICV = iCV;
	}
	public String getMAC() {
		return MAC;
	}
	public void setMAC(String mAC) {
		MAC = mAC;
	}
	public String getTxnDatetime() {
		return TxnDatetime;
	}
	public void setTxnDatetime(String txnDatetime) {
		TxnDatetime = txnDatetime;
	}
	public String getSTAN() {
		return STAN;
	}
	public void setSTAN(String sTAN) {
		STAN = sTAN;
	}
	public String getBillerBan() {
		return BillerBan;
	}
	public void setBillerBan(String billerBan) {
		BillerBan = billerBan;
	}
	public String getBusType() {
		return BusType;
	}
	public void setBusType(String busType) {
		BusType = busType;
	}
	public String getBillerID() {
		return BillerID;
	}
	public void setBillerID(String billerID) {
		BillerID = billerID;
	}
	public String getPaymentType() {
		return PaymentType;
	}
	public void setPaymentType(String paymentType) {
		PaymentType = paymentType;
	}
	public String getFeeID() {
		return FeeID;
	}
	public void setFeeID(String feeID) {
		FeeID = feeID;
	}
	public String getCustIdnBan() {
		return CustIdnBan;
	}
	public void setCustIdnBan(String custIdnBan) {
		CustIdnBan = custIdnBan;
	}
	public String getCustBillerAcnt() {
		return CustBillerAcnt;
	}
	public void setCustBillerAcnt(String custBillerAcnt) {
		CustBillerAcnt = custBillerAcnt;
	}
	public String getCustBankAcnt() {
		return CustBankAcnt;
	}
	public void setCustBankAcnt(String custBankAcnt) {
		CustBankAcnt = custBankAcnt;
	}
	public String getCustIdnBan_Dec() {
		return CustIdnBan_Dec;
	}
	public void setCustIdnBan_Dec(String custIdnBan_Dec) {
		CustIdnBan_Dec = custIdnBan_Dec;
	}
	public String getCustBillerAcnt_Dec() {
		return CustBillerAcnt_Dec;
	}
	public void setCustBillerAcnt_Dec(String custBillerAcnt_Dec) {
		CustBillerAcnt_Dec = custBillerAcnt_Dec;
	}
	public String getCustBankAcnt_Dec() {
		return CustBankAcnt_Dec;
	}
	public void setCustBankAcnt_Dec(String custBankAcnt_Dec) {
		CustBankAcnt_Dec = custBankAcnt_Dec;
	}
	public String getReservedField() {
		return ReservedField;
	}
	public void setReservedField(String reservedField) {
		ReservedField = reservedField;
	}
	public String getRCode() {
		return RCode;
	}
	public void setRCode(String rCode) {
		RCode = rCode;
	}


}
