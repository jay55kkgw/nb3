package tw.com.fstop.nnb.ws.server.bean;

import java.util.Arrays;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

//@XmlRootElement(namespace = "http://ws.fstop", name="Gold")
@XmlRootElement(namespace = "http://ws.fstop", name="in0")
//@XmlRootElement(namespace = "http://ws.fstop", name="informGoldValue")
@XmlAccessorType(XmlAccessType.FIELD)
public class Gold {
	
//@Override
//	public String toString() {
//		return "Gold [date=" + date + ", cnt=" + cnt + ", time=" + time + ", kind=" + kind + ", goldProduct="
//				+ Arrays.toString(goldProduct) + "]";
//	}

//	public List<tw.com.fstop.nb3.ws.GoldProduct> getGoldProduct() {
//		return goldProduct;
//	}
//
//	public void setGoldProduct(List<tw.com.fstop.nb3.ws.GoldProduct> goldProduct) {
//		this.goldProduct = goldProduct;
//	}

	
	@XmlElement(namespace = "http://ws.fstop")
	public String date;
	@XmlElement(namespace = "http://ws.fstop")
	public String cnt;
	@XmlElement(namespace = "http://ws.fstop")
	public String time;
	@XmlElement(namespace = "http://ws.fstop")
	public String kind;
	
	@XmlElementWrapper(namespace = "http://ws.fstop" ,name="goldProduct")
	@XmlElement(name="GoldProduct")
	public List<GoldProduct> goldProduct;
//	public GoldProduct[] goldProduct;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getCnt() {
		return cnt;
	}

	public void setCnt(String cnt) {
		this.cnt = cnt;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	
	
	public List<GoldProduct> getGoldProduct() {
		return goldProduct;
	}

	public void setGoldProduct(List<GoldProduct> goldProduct) {
		this.goldProduct = goldProduct;
	}

	

	
	
	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	
}
