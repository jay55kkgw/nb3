package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N560_REST_RQ extends BaseRestBean_FX implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6533273584875197642L;
	
	
	private String	CUSIDN	; //統一編號
	private String	CMSDATE	; //查詢起期
	private String	CMEDATE	; //查詢迄期
	private String	REFNO	; //信用狀通知號碼
	private String	LCNO	; //信用狀號碼
	private String  USERDATA; //回打用
	private String  OKOVNEXT; //下載用
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getCMSDATE() {
		return CMSDATE;
	}
	public void setCMSDATE(String cMSDATE) {
		CMSDATE = cMSDATE;
	}
	public String getCMEDATE() {
		return CMEDATE;
	}
	public void setCMEDATE(String cMEDATE) {
		CMEDATE = cMEDATE;
	}
	public String getUSERDATA() {
		return USERDATA;
	}
	public void setUSERDATA(String uSERDATA) {
		USERDATA = uSERDATA;
	}
	public String getOKOVNEXT() {
		return OKOVNEXT;
	}
	public void setOKOVNEXT(String oKOVNEXT) {
		OKOVNEXT = oKOVNEXT;
	}
	public String getREFNO() {
		return REFNO;
	}
	public void setREFNO(String rEFNO) {
		REFNO = rEFNO;
	}
	public String getLCNO() {
		return LCNO;
	}
	public void setLCNO(String lCNO) {
		LCNO = lCNO;
	}
	
}
