package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

public class N09002_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6478284873609704611L;

	@SerializedName(value = "TRANTITLE")
	private String TRANTITLE;

	@SerializedName(value = "TRANNAME")
	private String TRANNAME;

	@SerializedName(value = "CMQTIME")
	private String CMQTIME;

	@SerializedName(value = "ACN")
	private String ACN;
	
	@SerializedName(value = "SVACN")
	private String SVACN;
	
	@SerializedName(value = "TRNGDFormat")
	private String TRNGDFormat;
	
	@SerializedName(value = "PRICEFormat")
	private String PRICEFormat;
	
	@SerializedName(value = "TRNAMTFormat")
	private String TRNAMTFormat;

	@SerializedName(value = "FEEAMT1Format")
	private String FEEAMT1Format;
	
	@SerializedName(value = "FEEAMT2Format")
	private String FEEAMT2Format;
	
	@SerializedName(value = "TRNAMT2Format")
	private String TRNAMT2Format;

	@Override
	public Map<String, String> coverMap(){		
		Map<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", this.TRANTITLE);
		result.put("交易類型", this.TRANNAME);
		result.put("交易日期", this.CMQTIME);
		result.put("黃金轉入帳號", this.ACN);
		result.put("臺幣轉出帳號", this.SVACN);
		result.put("買進公克數", this.TRNGDFormat + "公克");
		result.put("牌告單價", "新臺幣" + this.PRICEFormat + "元／公克");
		result.put("回售金額", "新臺幣" + this.TRNAMTFormat + "元");
		if(!this.FEEAMT1Format.equals("0")) {
			result.put("定期投資扣款失敗累計手續費", "新臺幣" + this.FEEAMT1Format + "元");
		}
		if(!this.FEEAMT2Format.equals("0")) {
			result.put("黃金撲滿扣款失敗累計手續費", "新臺幣" + this.FEEAMT2Format + "元");
		}
		result.put("存入臺幣金額", "新臺幣" + this.TRNAMT2Format + "元");
		return result;
	}

	public String getTRANTITLE() {
		return TRANTITLE;
	}

	public void setTRANTITLE(String tRANTITLE) {
		TRANTITLE = tRANTITLE;
	}

	public String getTRANNAME() {
		return TRANNAME;
	}

	public void setTRANNAME(String tRANNAME) {
		TRANNAME = tRANNAME;
	}

	public String getCMQTIME() {
		return CMQTIME;
	}

	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	public String getSVACN() {
		return SVACN;
	}

	public void setSVACN(String sVACN) {
		SVACN = sVACN;
	}

	public String getTRNGDFormat() {
		return TRNGDFormat;
	}

	public void setTRNGDFormat(String tRNGDFormat) {
		TRNGDFormat = tRNGDFormat;
	}

	public String getPRICEFormat() {
		return PRICEFormat;
	}

	public void setPRICEFormat(String pRICEFormat) {
		PRICEFormat = pRICEFormat;
	}

	public String getTRNAMTFormat() {
		return TRNAMTFormat;
	}

	public void setTRNAMTFormat(String tRNAMTFormat) {
		TRNAMTFormat = tRNAMTFormat;
	}

	public String getFEEAMT1Format() {
		return FEEAMT1Format;
	}

	public void setFEEAMT1Format(String fEEAMT1Format) {
		FEEAMT1Format = fEEAMT1Format;
	}

	public String getFEEAMT2Format() {
		return FEEAMT2Format;
	}

	public void setFEEAMT2Format(String fEEAMT2Format) {
		FEEAMT2Format = fEEAMT2Format;
	}

	public String getTRNAMT2Format() {
		return TRNAMT2Format;
	}

	public void setTRNAMT2Format(String tRNAMT2Format) {
		TRNAMT2Format = tRNAMT2Format;
	}
}
