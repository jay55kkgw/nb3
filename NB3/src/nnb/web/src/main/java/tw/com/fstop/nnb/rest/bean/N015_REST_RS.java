package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N015_REST_RS extends BaseRestBean implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 775129110323679813L;
	
	private String AMTACC; //未知
	private String HEADER; //n015
	private String USERDATA_X50;
	private String OFFSET; //未知
	private String ACN;		//帳號
	private String TOTBAL; //帳上餘額
	private String SNTAMT; //不足扣票據總金額
	private String SEQ;    //未知
	private String SNTCNT; //不足扣票據總張
	private String AVLBAL; //可用餘額(含透支)
	private String CNT;    //總比數
	private String OCCURS_; //電文陣列

	
	LinkedList<N015_REST_RSDATA> REC;


	public String getAMTACC() {
		return AMTACC;
	}


	public void setAMTACC(String aMTACC) {
		AMTACC = aMTACC;
	}

	public String getHEADER() {
		return HEADER;
	}


	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}


	public String getUSERDATA_X50() {
		return USERDATA_X50;
	}


	public void setUSERDATA_X50(String uSERDATA_X50) {
		USERDATA_X50 = uSERDATA_X50;
	}


	public String getOFFSET() {
		return OFFSET;
	}


	public void setOFFSET(String oFFSET) {
		OFFSET = oFFSET;
	}


	public String getACN() {
		return ACN;
	}


	public void setACN(String aCN) {
		ACN = aCN;
	}


	public String getTOTBAL() {
		return TOTBAL;
	}


	public void setTOTBAL(String tOTBAL) {
		TOTBAL = tOTBAL;
	}


	public String getSNTAMT() {
		return SNTAMT;
	}


	public void setSNTAMT(String sNTAMT) {
		SNTAMT = sNTAMT;
	}


	public String getSEQ() {
		return SEQ;
	}


	public void setSEQ(String sEQ) {
		SEQ = sEQ;
	}


	public String getSNTCNT() {
		return SNTCNT;
	}


	public void setSNTCNT(String sNTCNT) {
		SNTCNT = sNTCNT;
	}


	public String getAVLBAL() {
		return AVLBAL;
	}


	public void setAVLBAL(String aVLBAL) {
		AVLBAL = aVLBAL;
	}


	public String getCNT() {
		return CNT;
	}


	public void setCNT(String cNT) {
		CNT = cNT;
	}


	public String getOCCURS_() {
		return OCCURS_;
	}


	public void setOCCURS_(String oCCURS_) {
		OCCURS_ = oCCURS_;
	}


	public LinkedList<N015_REST_RSDATA> getREC() {
		return REC;
	}


	public void setREC(LinkedList<N015_REST_RSDATA> rEC) {
		REC = rEC;
	}
	
	

	

}
