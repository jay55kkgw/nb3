package tw.com.fstop.nnb.service;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import tw.com.fstop.nnb.comm.bean.PMTInData;

@Service
public class Financial_Trial_Service  extends Base_Service
{
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * 提供精確的小數位四捨五入處理。`
	 * @param scale 小數點後保留幾位
	 * @return 四捨五入後的結果
	 */
	public double round(double v, int scale){
		if(scale<0){
			throw new IllegalArgumentException("The scale must be a positive integer or zero");
		}
		BigDecimal b = new BigDecimal(Double.toString(v));
		BigDecimal one = new BigDecimal("1");
		return b.divide(one, scale,BigDecimal.ROUND_HALF_UP).doubleValue();
	}

	/**
	 * R1 = sum(cashFlow) / (Total_i * cashFlowStart); Total_i:總期數 cashFlowStart:現金流量起始(負數)
	 * NPV = sum(cashFlow(i) / (1 + IRR)^i); i:期數(1..N)
	 * 內插法 IRR = (R2*(NPV1-cashFlowStart) + R1*(cashFlowStart-NPV2))/(NPV1-NPV2);
	 * 先估IRR預測值->用內插法得到近似值->用逼近法得到最近似值
	 * @param cashFlows 現金流量
	 * @return
	 */
	 public double getIRR(double[] cashFlow) {
		final int maxIteration = 35;
		//終止誤差值
		final double minDeviation = 0.01;
		//現金流量初值
		final double cashFlowStart = -1*cashFlow[0];
		//變動量
		double variation = 0.01;
		//IRR預測值
		double irrGuess = 0.0;
		//第1期IRR預測值
		double irrGuess1 = 0.0;
		//第2期IRR預測值
		double irrGuess2 = 0.0;
		//第1期現金流量值總合
		double npv1 = 0.0;
		//第2期現金流量值總合
		double npv2 = 0.0;
		//當期現金流量值總合
		double npv = 0.0;
		//誤差值
		double deviation = 0.0;

		try {
			//第1期IRR預測值
			irrGuess1 = getSUM(cashFlow)/((cashFlow.length-1)*cashFlowStart);
			//第2期IRR預測值
			irrGuess2 = irrGuess1 + 0.001211178;
			//第1期現金流量值總合
			npv1 = getNPV(cashFlow, irrGuess1);
			//第2期現金流量值總合
			npv2 = getNPV(cashFlow, irrGuess2);

			//內插法，找到近似值
			irrGuess = (irrGuess2*(npv1-cashFlowStart) + irrGuess1*(cashFlowStart-npv2))/(npv1-npv2);
			//System.out.println("內插法 IRR=" + irrGuess*12*100 + " /irrGuess=" + irrGuess);

			//以近似值再使用逼近法取更精確的值
			for (int i=1; i<=maxIteration; i++){
				npv = getNPV(cashFlow, irrGuess);

				deviation = round(Math.abs(cashFlowStart - npv), 2);
				//System.out.println("誤差值=" + deviation);
				if (deviation <= minDeviation){
					//System.out.println("irrGuess=" + irrGuess + " /npv=" + npv + " /variation=" + variation);
					break;
				}

				variation = variation/2;
				if (cashFlowStart < npv){
					irrGuess = irrGuess + variation;
				} else {
					irrGuess = irrGuess - variation;
				}

				//System.out.println("irrGuess=" + irrGuess + " /npv=" + npv + " /variation=" + variation);
			}
			irrGuess = round(irrGuess*12*100, 2);
			//System.out.println("IRR=" + irrGuess);

		} catch (Exception e){
			//System.out.println("[TotalCostRate.getIRR] occur Exception: " + e.toString());
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getIRR error >> {}",e);
		}

		return irrGuess;
	}

	/**
	 * 加總
	 * @param cashFlow
	 * @return
	 * @throws Exception
	 */
	 private double getSUM(double[] cashFlow) throws Exception {
		double sum = 0.0;
		for (int i=0; i<cashFlow.length; i++){
			sum = sum + cashFlow[i];
		}

		return sum;
	}

	/**
	 * 現金流量值總合
	 * @param cashFlow
	 * @param irrGuess
	 * @return
	 * @throws Exception
	 */
	 private double getNPV(double[] cashFlow, double irrGuess) throws Exception {
		double npv = 0.0;

		for (int i=1; i<cashFlow.length; i++) {
			npv = npv + (cashFlow[i] / Math.pow(1.0 + irrGuess, i));
		}

		return npv;
	}

	/**
	 * 年金值
	 * PMT = loan * mRate * (1 + mRate)^期數 / ((1 + mRate)^期數-1) 
	 * @param yRate 年利率%(mRate = 年利率/12/100)
	 * @param period 期數(剩餘期數)
	 * @param loan 本金(剩餘本金)
	 * @return
	 */
	 private double getPMT(double yRate, double period, double loan) throws Exception {
		double PMT = 0.0;
		double mRate = yRate/12/100;
		double pow = 0.0;

		if (mRate != 0 ){
			pow = Math.pow(1+mRate, period);
			PMT = Math.round(loan*mRate*pow/(pow-1));
		} else {
			PMT = Math.round(loan/period);
		}
		//System.out.println(yRate + " | " + period + " | " + loan + " | " + PMT);
		return PMT;
	}
	
	/**
	 * 本息平均攤還
	 * @param inData
	 * @throws Exception
	 */
	 public void averageAmortizationOfPrincipalAndInterest(PMTInData inData) throws Exception {
		//月份(期數)
		double[] periods = inData.getPeriods();
		//貸款結餘`
		double[] loanBalance = inData.getLoanBalance();
		//償還本金
		double[] principal = inData.getPrincipal();
		//利息
		double[] interest = inData.getInterest();
		//貸款利率
		double[] rate = inData.getRate();
		//每月繳款(現金流量)
		double[] cashFlow = inData.getCashFlow();
		//總利息
		double totalInterest = 0.0;

		//總期間
		int totalPeriod = inData.getPeriod1() + inData.getPeriod2() + inData.getPeriod3();
		//現金流量起始值
		cashFlow[0] = inData.getOtherFee() - inData.getLoan();
		//貸款結餘起始值
		loanBalance[0] = inData.getLoan();

		int i;
		if (inData.getPeriod1() > 0){
			for (i=1; i<=inData.getPeriod1(); i++){
				periods[i] = (double)i;
				rate[i] = inData.getYRate1();
				interest[i] = round(round(loanBalance[i-1]*rate[i]/12,0)/100,0);
				//目前期數大於寬限期->本息平均攤還，小於->只還息
				if (periods[i] > inData.getPrincipalGrace()){
					cashFlow[i] = getPMT(inData.getYRate1(), totalPeriod-(i-1), loanBalance[i-1]);
					principal[i] = cashFlow[i]-interest[i];
				} else {
					cashFlow[i] = interest[i];
					principal[i] = 0;
				}
				loanBalance[i] = loanBalance[i-1] - principal[i];
				totalInterest = totalInterest + interest[i];
			}
		}

		if (inData.getPeriod2() > 0){
			for (i=inData.getPeriod1()+1; i<=inData.getPeriod1()+inData.getPeriod2(); i++){
				periods[i] = (double)i;
				rate[i] = inData.getYRate2();
				interest[i] = round(round(loanBalance[i-1]*rate[i]/12,0)/100,0);
				//目前期數大於寬限期->本息平均攤還，小於->只還息
				if (periods[i] > inData.getPrincipalGrace()){
					cashFlow[i] = getPMT(inData.getYRate2(), totalPeriod-(i-1), loanBalance[i-1]);
					principal[i] = cashFlow[i]-interest[i];
				} else {
					cashFlow[i] = interest[i];
					principal[i] = 0;
				}
				loanBalance[i] = loanBalance[i-1] - principal[i];
				totalInterest = totalInterest + interest[i];
			}
		}

		if (inData.getPeriod3() > 0){
			for (i=inData.getPeriod1()+inData.getPeriod2()+1; i<=inData.getPeriod1()+inData.getPeriod2()+inData.getPeriod3(); i++){
				periods[i] = (double)i;
				rate[i] = inData.getYRate3();
				interest[i] = round(round(loanBalance[i-1]*rate[i]/12,0)/100,0);
				//目前期數大於寬限期->本息平均攤還，小於->只還息
				if (periods[i] > inData.getPrincipalGrace()){
					cashFlow[i] = getPMT(inData.getYRate3(), totalPeriod-(i-1), loanBalance[i-1]);
					principal[i] = cashFlow[i]-interest[i];
				} else {
					cashFlow[i] = interest[i];
					principal[i] = 0;					
				}
				loanBalance[i] = loanBalance[i-1] - principal[i];
				totalInterest = totalInterest + interest[i];
			}
		}
		//回寫總利息
		inData.setTotalInterest(totalInterest);
		/*
		for (int j=0; j<=totalPeriod; j++){
			System.out.println(periods[j] + " | " + loanBalance[j] + " | " + principal[j] + " | " + interest[j] + " | " + rate[j] + " | " + cashFlow[j]);
		}*/
	}

	/**
	 * 本金平均攤還
	 * @param inData
	 * @throws Exception
	 */
	 public void averageAmortizationOfPrincipal(PMTInData inData) throws Exception {
		//月份(期數)
		double[] periods = inData.getPeriods();
		//貸款結餘
		double[] loanBalance = inData.getLoanBalance();
		//償還本金
		double[] principal = inData.getPrincipal();
		//利息
		double[] interest = inData.getInterest();
		//貸款利率
		double[] rate = inData.getRate();
		//每月繳款(現金流量)
		double[] cashFlow = inData.getCashFlow();
		//總利息
		double totalInterest = 0.0;

		//總期間
		int totalPeriod = inData.getPeriod1() + inData.getPeriod2() + inData.getPeriod3();
		//現金流量起始值
		cashFlow[0] = inData.getOtherFee() - inData.getLoan();
		//貸款結餘起始值
		loanBalance[0] = inData.getLoan();

		int i;
		if (inData.getPeriod1() > 0){
			for (i=1; i<=inData.getPeriod1(); i++){
				periods[i] = (double)i;
				rate[i] = inData.getYRate1();
				interest[i] = round(round(loanBalance[i-1]*rate[i]/12,0)/100,0);
				//目前期數大於寬限期->本金平均攤還，小於->只還息
				if (periods[i] > inData.getPrincipalGrace()){
					principal[i] = inData.getLoan()/(totalPeriod-inData.getPrincipalGrace());
					cashFlow[i] = principal[i] + interest[i];
				} else {
					principal[i] = 0;
					cashFlow[i] = interest[i];
				}
				
				loanBalance[i] = loanBalance[i-1] - principal[i];
				totalInterest = totalInterest + interest[i];
				
				//  log.trace("i={},periods={},principal={},interest={},cashFlow={},loanBalance={}",i,periods[i],principal[i] ,interest[i],cashFlow[i],loanBalance[i]);
				//  log.trace("i_int={},periods_int={},principal_int={},interest_int={},cashFlow_int={},loanBalance_int={}",i,(int)periods[i],(int)principal[i] ,(int)interest[i],(int)cashFlow[i],(int)loanBalance[i]);

			}
		}

		if (inData.getPeriod2() > 0){
			for (i=inData.getPeriod1()+1; i<=inData.getPeriod1()+inData.getPeriod2(); i++){
				periods[i] = (double)i;
				rate[i] = inData.getYRate2();
				interest[i] = round(round(loanBalance[i-1]*rate[i]/12,0)/100,0);
				//目前期數大於寬限期->本金平均攤還，小於->只還息
				if (periods[i] > inData.getPrincipalGrace()){
					principal[i] = inData.getLoan()/(totalPeriod-inData.getPrincipalGrace());
					cashFlow[i] = principal[i] + interest[i];
				} else {
					cashFlow[i] = interest[i];
					principal[i] = 0;
				}
				loanBalance[i] = loanBalance[i-1] - principal[i];
				totalInterest = totalInterest + interest[i];
			}
		}

		if (inData.getPeriod3() > 0){
			for (i=inData.getPeriod1()+inData.getPeriod2()+1; i<=inData.getPeriod1()+inData.getPeriod2()+inData.getPeriod3(); i++){
				periods[i] = (double)i;
				rate[i] = inData.getYRate3();
				interest[i] = round(round(loanBalance[i-1]*rate[i]/12,0)/100,0);
				//目前期數大於寬限期->本金平均攤還，小於->只還息
				if (periods[i] > inData.getPrincipalGrace()){
					principal[i] = inData.getLoan()/(totalPeriod-inData.getPrincipalGrace());
					cashFlow[i] = principal[i] + interest[i];
				} else {
					cashFlow[i] = interest[i];
					principal[i] = 0;					
				}
				loanBalance[i] = loanBalance[i-1] - principal[i];
				totalInterest = totalInterest + interest[i];
			}
		}
		//回寫總利息
		inData.setTotalInterest(totalInterest);
		/*
		for (int j=0; j<=totalPeriod; j++){
			System.out.println(periods[j] + " | " + loanBalance[j] + " | " + principal[j] + " | " + interest[j] + " | " + rate[j] + " | " + cashFlow[j]);
		}*/
	}
	
	/**
	 * 到期一次還本(最長期限3年)
	 * @param inData
	 * @throws Exception
	 */
	 public void principalRepaidAtMaturity(PMTInData inData) throws Exception {
		//月份(期數)
		double[] periods = inData.getPeriods();
		//貸款結餘
		double[] loanBalance = inData.getLoanBalance();
		//償還本金
		double[] principal = inData.getPrincipal();
		//利息
		double[] interest = inData.getInterest();
		//貸款利率
		double[] rate = inData.getRate();
		//每月繳款(現金流量)
		double[] cashFlow = inData.getCashFlow();
		//總利息
		double totalInterest = 0.0;

		//總期間
		int totalPeriod = inData.getPeriod1() + inData.getPeriod2() + inData.getPeriod3();
		//現金流量起始值
		cashFlow[0] = inData.getOtherFee() - inData.getLoan();
		//貸款結餘起始值
		loanBalance[0] = inData.getLoan();
		//到期一次還本的寬限期為總期數-1
		int principalGrace = totalPeriod - 1;

		int i;
		if (inData.getPeriod1() > 0){
			for (i=1; i<=inData.getPeriod1(); i++){
				periods[i] = (double)i;
				rate[i] = inData.getYRate1();
				interest[i] = round(round(loanBalance[i-1]*rate[i]/12,0)/100,0);
				//目前期數大於寬限期->本金平均攤還，小於->只還息
				if (periods[i] > principalGrace){
					principal[i] = inData.getLoan();
					cashFlow[i] = principal[i] + interest[i];
				} else {
					principal[i] = 0;
					cashFlow[i] = interest[i];
				}
				loanBalance[i] = loanBalance[i-1] - principal[i];
				totalInterest = totalInterest + interest[i];
			}
		}

		if (inData.getPeriod2() > 0){
			for (i=inData.getPeriod1()+1; i<=inData.getPeriod1()+inData.getPeriod2(); i++){
				periods[i] = (double)i;
				rate[i] = inData.getYRate2();
				interest[i] = round(round(loanBalance[i-1]*rate[i]/12,0)/100,0);
				//目前期數大於寬限期->本金平均攤還，小於->只還息
				if (periods[i] > principalGrace){
					principal[i] = inData.getLoan();
					cashFlow[i] = principal[i] + interest[i];
				} else {
					cashFlow[i] = interest[i];
					principal[i] = 0;
				}
				loanBalance[i] = loanBalance[i-1] - principal[i];
				totalInterest = totalInterest + interest[i];
			}
		}

		if (inData.getPeriod3() > 0){
			for (i=inData.getPeriod1()+inData.getPeriod2()+1; i<=inData.getPeriod1()+inData.getPeriod2()+inData.getPeriod3(); i++){
				periods[i] = (double)i;
				rate[i] = inData.getYRate3();
				interest[i] = round(round(loanBalance[i-1]*rate[i]/12,0)/100,0);
				//目前期數大於寬限期->本金平均攤還，小於->只還息
				if (periods[i] > principalGrace){
					principal[i] = inData.getLoan();
					cashFlow[i] = principal[i] + interest[i];
				} else {
					cashFlow[i] = interest[i];
					principal[i] = 0;					
				}
				loanBalance[i] = loanBalance[i-1] - principal[i];
				totalInterest = totalInterest + interest[i];
			}
		}
		//回寫總利息
		inData.setTotalInterest(totalInterest);
		/*
		for (int j=0; j<=totalPeriod; j++){
			System.out.println((int)periods[j] + " | " + (int)loanBalance[j] + " | " + (int)principal[j] + " | " + (int)interest[j] + " | " + rate[j] + " | " + (int)cashFlow[j]);
		}*/
	}
}


