package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class B012_REST_RQ  extends BaseRestBean_FUND implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1415660895796586875L;
	
	String CUSIDN;

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
}
