package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N921_REST_RSDATA implements Serializable {

	private static final long serialVersionUID = 5325764787100029115L;

	private String NAME;//

	private String ACN;//帳號

	private String BNKCOD;
	
	private String TEXT;
	
	private String VALUE;

	public String getNAME() {
		return NAME;
	}

	public void setNAME(String nAME) {
		NAME = nAME;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	public String getBNKCOD() {
		return BNKCOD;
	}

	public void setBNKCOD(String bNKCOD) {
		BNKCOD = bNKCOD;
	}

	public String getTEXT() {
		return TEXT;
	}

	public void setTEXT(String tEXT) {
		TEXT = tEXT;
	}

	public String getVALUE() {
		return VALUE;
	}

	public void setVALUE(String vALUE) {
		VALUE = vALUE;
	}
	
	
}
