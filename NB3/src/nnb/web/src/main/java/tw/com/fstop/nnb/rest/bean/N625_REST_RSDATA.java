package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N625_REST_RSDATA implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3542623596525050046L;

	private String DPIBAL;	//帳戶餘額
	private String TIME;	//時間
	private String CODDBCR;	//借貸別
	private String LTD;		//異動日
	private String ACN;		//帳號
	private String ORNBRH;	//收付行
	private String DATA1;	//補充資料1
	private String DATA2;	//補充資料2
	private String CHKNUM;	//支票號碼
	private String MEMO;	//摘要
	private String MEMO_C;	//摘要
	private String AMT;		//支出金額
	private String FILLER_X4;	//???
	private String DPWDAMT;	//收入
	private String DPSVAMT;	//支出
	
	
	public String getDPIBAL() {
		return DPIBAL;
	}
	public void setDPIBAL(String dPIBAL) {
		DPIBAL = dPIBAL;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getCODDBCR() {
		return CODDBCR;
	}
	public void setCODDBCR(String cODDBCR) {
		CODDBCR = cODDBCR;
	}
	public String getLTD() {
		return LTD;
	}
	public void setLTD(String lTD) {
		LTD = lTD;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getORNBRH() {
		return ORNBRH;
	}
	public void setORNBRH(String oRNBRH) {
		ORNBRH = oRNBRH;
	}
	public String getDATA1() {
		return DATA1;
	}
	public void setDATA1(String dATA1) {
		DATA1 = dATA1;
	}
	public String getDATA2() {
		return DATA2;
	}
	public void setDATA2(String dATA2) {
		DATA2 = dATA2;
	}
	public String getCHKNUM() {
		return CHKNUM;
	}
	public void setCHKNUM(String cHKNUM) {
		CHKNUM = cHKNUM;
	}
	public String getMEMO() {
		return MEMO;
	}
	public void setMEMO(String mEMO) {
		MEMO = mEMO;
	}	
	public String getMEMO_C() {
		return MEMO_C;
	}
	public void setMEMO_C(String mEMO_C) {
		MEMO_C = mEMO_C;
	}
	public String getAMT() {
		return AMT;
	}
	public void setAMT(String aMT) {
		AMT = aMT;
	}
	public String getFILLER_X4() {
		return FILLER_X4;
	}
	public void setFILLER_X4(String fILLER_X4) {
		FILLER_X4 = fILLER_X4;
	}
	public String getDPWDAMT() {
		return DPWDAMT;
	}
	public void setDPWDAMT(String dPWDAMT) {
		DPWDAMT = dPWDAMT;
	}
	public String getDPSVAMT() {
		return DPSVAMT;
	}
	public void setDPSVAMT(String dPSVAMT) {
		DPSVAMT = dPSVAMT;
	}
	
	


}
