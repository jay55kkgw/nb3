package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N178_REST_RQ extends BaseRestBean_FX implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3228583479117451208L;

	private String ADOPID; // 電文代號
	
	private String CUSIDN; // 身分證號

	private String UID; // 同CUSIDN

	private String FYACN; // 存單帳號

	private String FDPNUM; // 存單號碼

	private String INTMTH; // 計息方式

	private String TYPE1; // 續存方式

	private String FYTSFAN; // 外匯活存帳號

	private String TYPCOD; // 期別種類

	private String TERM; // 期別

	private String AMTFDP; // 原存單金額

	private String INT; // 原存單利息

	private String FYTAX; // 所得稅

	private String NHITAX; // 健保費

	private String PINNEW; // 交易密碼SHA1值

	private String TRNSRC = "NB";

	private String iSeqNo; // iSeqNo

	private String FGTXDATE; // 即時或預約

	private String TRANSPASSUPDATE; // 即時或預約

	private String pkcs7Sign; // IKEY

	private String jsondc; // IKEY
	
	//IDGATE
	private String sessionID;
	private String idgateID;
	private String txnID;

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getUID() {
		return UID;
	}

	public void setUID(String uID) {
		UID = uID;
	}

	public String getFYACN() {
		return FYACN;
	}

	public void setFYACN(String fYACN) {
		FYACN = fYACN;
	}

	public String getFDPNUM() {
		return FDPNUM;
	}

	public void setFDPNUM(String fDPNUM) {
		FDPNUM = fDPNUM;
	}

	public String getINTMTH() {
		return INTMTH;
	}

	public void setINTMTH(String iNTMTH) {
		INTMTH = iNTMTH;
	}

	public String getTYPE1() {
		return TYPE1;
	}

	public void setTYPE1(String tYPE1) {
		TYPE1 = tYPE1;
	}

	public String getFYTSFAN() {
		return FYTSFAN;
	}

	public void setFYTSFAN(String fYTSFAN) {
		FYTSFAN = fYTSFAN;
	}

	public String getTYPCOD() {
		return TYPCOD;
	}

	public void setTYPCOD(String tYPCOD) {
		TYPCOD = tYPCOD;
	}

	public String getTERM() {
		return TERM;
	}

	public void setTERM(String tERM) {
		TERM = tERM;
	}

	public String getAMTFDP() {
		return AMTFDP;
	}

	public void setAMTFDP(String aMTFDP) {
		AMTFDP = aMTFDP;
	}

	public String getINT() {
		return INT;
	}

	public void setINT(String iNT) {
		INT = iNT;
	}

	public String getFYTAX() {
		return FYTAX;
	}

	public void setFYTAX(String fYTAX) {
		FYTAX = fYTAX;
	}

	public String getNHITAX() {
		return NHITAX;
	}

	public void setNHITAX(String nHITAX) {
		NHITAX = nHITAX;
	}

	public String getPINNEW() {
		return PINNEW;
	}

	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}

	public String getTRNSRC() {
		return TRNSRC;
	}

	public void setTRNSRC(String tRNSRC) {
		TRNSRC = tRNSRC;
	}

	public String getiSeqNo() {
		return iSeqNo;
	}

	public void setiSeqNo(String iSeqNo) {
		this.iSeqNo = iSeqNo;
	}

	public String getFGTXDATE() {
		return FGTXDATE;
	}

	public void setFGTXDATE(String fGTXDATE) {
		FGTXDATE = fGTXDATE;
	}

	public String getTRANSPASSUPDATE() {
		return TRANSPASSUPDATE;
	}

	public void setTRANSPASSUPDATE(String tRANSPASSUPDATE) {
		TRANSPASSUPDATE = tRANSPASSUPDATE;
	}

	public String getPkcs7Sign() {
		return pkcs7Sign;
	}

	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}

	public String getJsondc() {
		return jsondc;
	}

	public void setJsondc(String jsondc) {
		this.jsondc = jsondc;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public String getIdgateID() {
		return idgateID;
	}

	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}

	public String getTxnID() {
		return txnID;
	}

	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}

	
}
