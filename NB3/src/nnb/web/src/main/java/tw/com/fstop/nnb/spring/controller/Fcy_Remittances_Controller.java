package tw.com.fstop.nnb.spring.controller;

import java.net.URLEncoder;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.idgate.bean.F002_IDGATE_DATA;
import tw.com.fstop.idgate.bean.F002_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.F002S_IDGATE_DATA_VIEW;
import tw.com.fstop.nnb.aop.Dialog;
import tw.com.fstop.nnb.service.Fcy_Remittances_Service;
import tw.com.fstop.nnb.service.IdGateService;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

/**
 * 
 * 功能說明 :外匯匯出匯款
 *
 */
@SessionAttributes({ SessionUtil.CUSIDN, SessionUtil.DPMYEMAIL, SessionUtil.STEP1_LOCALE_DATA,
		SessionUtil.STEP2_LOCALE_DATA, SessionUtil.STEP3_LOCALE_DATA, SessionUtil.CONFIRM_LOCALE_DATA,SessionUtil.IDGATE_TRANSDATA,
		SessionUtil.RESULT_LOCALE_DATA, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, SessionUtil.TRANSFER_DATA,
		SessionUtil.FXAGREEFLAG })
@Controller
@RequestMapping(value = "/FCY/REMITTANCES")
public class Fcy_Remittances_Controller
{
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Fcy_Remittances_Service fcy_Remittances_Service;

	@Autowired		 
	IdGateService idgateservice;
	
	// 錯誤頁路徑
	private final String ERRORJSP = "/error";

	/**
	 * F002外匯匯出匯款說明通知頁!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_outward_remittances")
	public String f_outward_remittances(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("f_outward_remittances Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		String urlId = okMap.get("ACN");
		log.trace(ESAPIUtil.vaildLog("urlId: " + urlId));
		BaseResult bs = null;
		try
		{
			// get session value
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			String fxagreeflag = (String) SessionUtil.getAttribute(model, SessionUtil.FXAGREEFLAG, null);
			log.debug("fxagreeflag >>>>>{}", fxagreeflag);
			bs = new BaseResult();
			bs.addData("urlID", urlId);
			if (StrUtil.isEmpty(fxagreeflag))
			{
				target = "acct_fcy/f_outward_remittances";
				model.addAttribute("f_outward_remittances", bs);
			}
			else
			{
				target = "forward:/FCY/REMITTANCES/f_outward_remittances_step1";
				model.addAttribute("f_outward_remittances_step1_r", bs);
			}
		}
		catch (Exception e)
		{
			log.error("f_outward_remittances Error >>>> {} ", e);
		}
		return target;
	}

	/**
	 * F002外匯匯出匯款進入頁!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_outward_remittances_step1")
	public String f_outward_remittances_step1(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("f_outward_remittances_step1 Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		String urlId ="";
		BaseResult bs = new BaseResult();
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			if(okMap.containsKey("URLID")) {
				urlId = okMap.get("URLID");
			}
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			// get session value
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			// 將點選確認的值放入session
			String fxagreeflag = okMap.get("FXAGREEFLAG");
			SessionUtil.addAttribute(model, SessionUtil.FXAGREEFLAG, fxagreeflag);
			//
			bs = fcy_Remittances_Service.getOutAcnoAndAgreeAcno(cusidn);
			
           //IDGATE身分
           String idgateUserFlag="N";		 
           Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
           try {		 
        	   if(IdgateData==null) {
        		   IdgateData = new HashMap<String, Object>();
        	   }
               BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
               if(tmp.getResult()) {		 
                   idgateUserFlag="Y";                  		 
               }		 
               tmp.addData("idgateUserFlag",idgateUserFlag);		 
               IdgateData.putAll((Map<String, String>) tmp.getData());
               SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
           }catch(Exception e) {		 
               log.debug("idgateUserFlag error {}",e);		 
           }		 
           model.addAttribute("idgateUserFlag",idgateUserFlag);
		}
		catch (Exception e)
		{
			log.error("f_outward_remittances_step1 Error >>>> {} ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				bs.addData("urlID", urlId);
				target = "acct_fcy/f_outward_remittances_step1";
				model.addAttribute("f_outward_remittances_step1", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * F002外匯匯出匯款第2頁
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_outward_remittances_step2")
	public String f_outward_remittances_step2(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("f_outward_remittances_step2 Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		try
		{
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			// 解決Trust Boundary Violation
//			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			//先註解 會被ESAPI擋掉
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam); 
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP2_LOCALE_DATA, null));
			}
			else
			{
				bs = fcy_Remittances_Service.f_outward_remittances_step2(okMap);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.STEP2_LOCALE_DATA, bs);
			}
		}
		catch (Exception e)
		{
			log.error("f_outward_remittances_step2 Error >>>>{}", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				target = "acct_fcy/f_outward_remittances_step2";
				model.addAttribute("f_outward_remittances_step2", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * F002外匯匯出匯款第3頁
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_outward_remittances_step3")
	public String f_outward_remittances_step3(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("f_outward_remittances_step2 Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();

		try
		{
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			// 解決Trust Boundary Violation
//			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			//先註解 會被ESAPI擋掉
			Map<String, String> okMap = reqParam;

			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP3_LOCALE_DATA, null));
			}
			else
			{
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				BaseResult bsdata = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String, String> transfer_data = (Map<String, String>) bsdata.getData();
				log.debug("transfer_data {}", transfer_data);
				// call F002R_REST
				bs = fcy_Remittances_Service.f_outward_remittances_step3(cusidn, transfer_data);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.STEP3_LOCALE_DATA, bs);
			}
		}
		catch (Exception e)
		{
			log.error("f_outward_remittances_step3 Error {}", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				// TODO 這邊還要調整 不能存物件，要存成JSNO字串
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				target = "acct_fcy/f_outward_remittances_step3";
				model.addAttribute("f_outward_remittances_step3", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * F002外匯匯出匯款預約確認頁
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_outward_remittances_step2_S")
	public String f_outward_remittances_step2_S(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("f_outward_remittances_step2_S Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		String jsondc = "";
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			Map<String, String> okMap = reqParam;
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(okMap), "UTF-8");
			log.trace(ESAPIUtil.vaildLog("f_outward_remittances_confirm.jsondc: {}"+jsondc));

			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
			}
			else
			{
				log.debug(ESAPIUtil.vaildLog("f_outward_remittances_confirm okMap >>> {}"+ CodeUtil.toJson(okMap)));
				bs = fcy_Remittances_Service.f_outward_remittances_step2_S(okMap);
				// 防止重送代碼TxToken
				bs.addData("TXTOKEN", ((Map<String, String>) fcy_Remittances_Service.getTxToken().getData()).get("TXTOKEN"));
				// IKEY要使用的JSON:DC
				bs.addData("jsondc", jsondc);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				
				// IDGATE transdata     
				Map<String,String> result = new HashMap();
				result.putAll((Map< String,String>) bs.getData());
	            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	    		String adopid = "F002";
	    		String title = "您有一筆 外匯匯出匯款 待確認，匯款金額  "+result.get("display_PAYREMIT");
	        	if(IdgateData != null) {
		            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
		            	
		            	// 付款人轉出幣別
		            	result.put("ACCCUR", (String)IdgateData.get("ORGCCY"));
		            	// 付款帳號
		            	result.put("ACCTID", (String)IdgateData.get("CUSTACC"));
		            	// 轉出金額
		            	result.put("ACCAMT", (String)IdgateData.get("ORGAMT"));
		            	// 付款人轉入幣別
		            	result.put("CURCODE", (String)IdgateData.get("PMTCCY"));
		            	// 付款日期
		            	result.put("PRCDT", (String)IdgateData.get("PAYDATE"));
		            	
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(F002_IDGATE_DATA.class, F002S_IDGATE_DATA_VIEW.class, result));
		                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
		            }
	        	}
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
			}
		}
		catch (Exception e)
		{
			log.error("f_outward_remittances_step2_S Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				// TODO 這邊還要調整 不能存物件，要存成JSNO字串
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				target = "acct_fcy/f_outward_remittances_step2_S";
				model.addAttribute("f_outward_remittances_step2_S", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * F002外匯匯出匯款確認頁
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_outward_remittances_confirm")
	public String f_outward_remittances_confirm(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("f_outward_remittances_confirm Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		String jsondc = "";
		try
		{

			// 判斷是否為同幣別
			if ("Y".equals(reqParam.get("SameCurrency")))
			{
				// 同幣別
			}
			else
			{
				BaseResult bsdata = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				reqParam = (Map<String, String>) bsdata.getData();
			}
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// Map<String, String> okMap = reqParam;
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(okMap), "UTF-8");
			log.trace(ESAPIUtil.vaildLog("f_outward_remittances_confirm.jsondc: {}"+ jsondc));

			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
			}
			else
			{
				log.debug(ESAPIUtil.vaildLog("f_outward_remittances_confirm okMap >>> {}"+ CodeUtil.toJson(okMap)));
				bs = fcy_Remittances_Service.f_outward_remittances_confirm(okMap);
				// 防止重送代碼TxToken
				bs.addData("TXTOKEN", ((Map<String, String>) fcy_Remittances_Service.getTxToken().getData()).get("TXTOKEN"));
				// IKEY要使用的JSON:DC
				bs.addData("jsondc", jsondc);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				
				// IDGATE transdata     
				Map<String,String> result = new HashMap();
				result.putAll((Map< String,String>) bs.getData());
	            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	    		String adopid = "F002";
	    		String title = "您有一筆 外匯匯出匯款 待確認，匯款金額  "+result.get("display_PAYREMIT");
	        	if(IdgateData != null) {
		            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(F002_IDGATE_DATA.class, F002_IDGATE_DATA_VIEW.class, result));
		                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
		            }
	        	}
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
			}

		}
		catch (Exception e)
		{
			log.error("f_outward_remittances_confirm Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				target = "acct_fcy/f_outward_remittances_confirm";
				model.addAttribute("f_outward_remittances_confirm", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * F002外匯匯出匯款結果頁!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_outward_remittances_result")
	@Dialog(ADOPID = "F002")
	public String f_outward_remittances_result(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("f_outward_remittances_result Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		String pageToken = "";
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			//Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
			}
			else
			{
				log.debug(ESAPIUtil.vaildLog("okMap>>" + CodeUtil.toJson(okMap)));
				// get Session cusidn
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				// Check Session Token
				String SessionFinshToken = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null);
				pageToken = okMap.get("TXTOKEN");
				BaseResult checkToken = fcy_Remittances_Service.validateToken(pageToken, SessionFinshToken);
				if (!checkToken.getResult())
				{
					bs = checkToken;
				}
				else
				{
					// get Sesssion transfer_data
					BaseResult bsdata = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
					Map<String, String> transfer_data = (Map<String, String>) bsdata.getData();
					log.debug("f_outward_remittances_result transfer_data >>> {}", transfer_data);
					
					try {		 
	                    if(okMap.get("FGTXWAY").equals("7")) {	
	        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
	        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("F002_IDGATE");
	                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
	                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
	                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
	                    }
	                }catch(Exception e) {		 
	                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
	                }
					
					Map<String,Object>IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
					model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
					
					bs = fcy_Remittances_Service.f_outward_remittances_result(cusidn, transfer_data, okMap);
					
					// 將查過的資料放入session，待切換 locale 時讀取
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
			}
		}
		catch (Exception e)
		{
			log.error("f_outward_remittances_result Error ", e);
		}
		finally
		{
			log.debug("bs.getResult()>>" + bs.getResult());
			log.debug("bs.getData()>>" + bs.getData());
			if (bs != null && bs.getResult())
			{
				// session add TxToken
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, pageToken);
				target = "acct_fcy/f_outward_remittances_result";
				model.addAttribute("f_outward_remittances_result", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

}
