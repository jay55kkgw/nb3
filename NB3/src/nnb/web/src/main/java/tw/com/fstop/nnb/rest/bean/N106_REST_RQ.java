package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N106_REST_RQ extends BaseRestBean_TW implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8076807635724584634L;

	private String ADOPID;
	private String CUSIDN;
	private String TXNTYPE;
	private String SEQCLM;
	private String FORM;
	private String FGTXWAY;

	//IDGATE
	private String sessionID;
	private String idgateID;
	private String txnID;
	
	
	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	public String getIdgateID() {
		return idgateID;
	}
	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}
	public String getTxnID() {
		return txnID;
	}
	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public String getTXNTYPE() {
		return TXNTYPE;
	}
	public String getSEQCLM() {
		return SEQCLM;
	}
	public String getFORM() {
		return FORM;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public void setTXNTYPE(String tXNTYPE) {
		TXNTYPE = tXNTYPE;
	}
	public void setSEQCLM(String sEQCLM) {
		SEQCLM = sEQCLM;
	}
	public void setFORM(String fORM) {
		FORM = fORM;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	
}
