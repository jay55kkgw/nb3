package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class TD01_REST_RSDATA extends BaseRestBean implements Serializable {
	
	private static final long serialVersionUID = 5150640688730965034L;
	/**
	 * 
	 */
	private LinkedList<TD01_REST_RSDATA2> TABLE;
	
	
	public LinkedList<TD01_REST_RSDATA2> getTABLE() {
		return TABLE;
	}
	public void setTABLE(LinkedList<TD01_REST_RSDATA2> tABLE) {
		TABLE = tABLE;
	}
	 
}
