package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

public class F003_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5059806468916425895L;

	@SerializedName(value = "TRANTITLE")
	private String TRANTITLE;

	@SerializedName(value = "TRANNAME")
	private String TRANNAME;
	
	@SerializedName(value = "SameCurrency")
	private String SameCurrency;

	@SerializedName(value = "BGROENO")
	private String BGROENO;

	@SerializedName(value = "RATE")
	private String RATE;

	@SerializedName(value = "TXCCY")
	private String TXCCY;

	@SerializedName(value = "showCURAMT")
	private String showCURAMT;

	@SerializedName(value = "showTXAMT")
	private String showTXAMT;

	@SerializedName(value = "BENACC")
	private String BENACC;

	@SerializedName(value = "RETCCY")
	private String RETCCY;

	@Override
	public Map<String, String> coverMap(){
		Map<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", this.TRANTITLE);
		result.put("交易類型", this.TRANNAME);
		if(this.SameCurrency.equals("N")) {
			result.put("議價編號", this.BGROENO);
			result.put("匯率", this.RATE);
			result.put("匯款金額", this.TXCCY + " " + this.showCURAMT);
		}else {
			result.put("匯款金額", this.TXCCY + " " + this.showTXAMT);
		}
		result.put("解款帳號", this.BENACC);
		result.put("解款金額", this.RETCCY + " " + this.showTXAMT + "元");
		return result;
	}

	public String getTRANTITLE() {
		return TRANTITLE;
	}

	public void setTRANTITLE(String tRANTITLE) {
		TRANTITLE = tRANTITLE;
	}

	public String getTRANNAME() {
		return TRANNAME;
	}

	public void setTRANNAME(String tRANNAME) {
		TRANNAME = tRANNAME;
	}

	public String getSameCurrency() {
		return SameCurrency;
	}

	public void setSameCurrency(String sameCurrency) {
		SameCurrency = sameCurrency;
	}

	public String getBGROENO() {
		return BGROENO;
	}

	public void setBGROENO(String bGROENO) {
		BGROENO = bGROENO;
	}

	public String getRATE() {
		return RATE;
	}

	public void setRATE(String rATE) {
		RATE = rATE;
	}

	public String getTXCCY() {
		return TXCCY;
	}

	public void setTXCCY(String tXCCY) {
		TXCCY = tXCCY;
	}

	public String getShowCURAMT() {
		return showCURAMT;
	}

	public void setShowCURAMT(String showCURAMT) {
		this.showCURAMT = showCURAMT;
	}

	public String getShowTXAMT() {
		return showTXAMT;
	}

	public void setShowTXAMT(String showTXAMT) {
		this.showTXAMT = showTXAMT;
	}

	public String getBENACC() {
		return BENACC;
	}

	public void setBENACC(String bENACC) {
		BENACC = bENACC;
	}

	public String getRETCCY() {
		return RETCCY;
	}

	public void setRETCCY(String rETCCY) {
		RETCCY = rETCCY;
	}
}
