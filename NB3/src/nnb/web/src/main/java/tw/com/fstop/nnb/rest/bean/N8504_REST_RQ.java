package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N8504_REST_RQ extends BaseRestBean_PS implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -1494236205389499224L;
	
	private String PINNEW;
	private String CUSIDN ;
	private String LOSTYPE;
	private String BalFlag;
	private String CRY;
	private String ArrayParam;
	private String ADOPID;
	private String FGTXWAY;
	//TXNLOG
	private String ADGUID;
	private String ADUSERIP;
	
	
	
	
	public String getArrayParam() {
		return ArrayParam;
	}
	public void setArrayParam(String arrayParam) {
		ArrayParam = arrayParam;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getLOSTYPE() {
		return LOSTYPE;
	}
	public void setLOSTYPE(String lOSTYPE) {
		LOSTYPE = lOSTYPE;
	}
	public String getBalFlag() {
		return BalFlag;
	}
	public void setBalFlag(String balFlag) {
		BalFlag = balFlag;
	}
	public String getCRY() {
		return CRY;
	}
	public void setCRY(String cRY) {
		CRY = cRY;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getADGUID() {
		return ADGUID;
	}
	public void setADGUID(String aDGUID) {
		ADGUID = aDGUID;
	}
	public String getADUSERIP() {
		return ADUSERIP;
	}
	public void setADUSERIP(String aDUSERIP) {
		ADUSERIP = aDUSERIP;
	}
}
