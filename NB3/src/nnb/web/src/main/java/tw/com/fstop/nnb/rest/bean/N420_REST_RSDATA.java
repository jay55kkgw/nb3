package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N420_REST_RSDATA extends BaseRestBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7161381041694848075L;
	
	String ACN;			// 帳號
	String FD_LOST;		// 存單掛失
	String SV_LOST;		// 存摺掛失
	String SIGN_LOST;	// 印鑑掛失
	String FILLER;		// 預留空間
	String TEXT;		// 好記名稱
	
	
	String FDPNUM;		// 存單號碼
	String AMTFDP;		// 存單金額
	String ITR;			// 利率
	String INTMTH;		// 計息方式
	String DPISDT;		// 起存日
	String DUEDAT;		// 到期日
	String TSFACN;		// 轉帳帳號
	String MMACOD;		// 理財專戶註記
	String ILAZLFTM;	// 自動轉期已轉次數
	String AUTXFTM;		// 自動轉期未轉次數
	String DPINTTYPE;	// 計息方式(中文)
	String TYPE;		// 存款種類
	String TYPE1;		// 轉存方式
	String TYPE2;		// 申請不轉期
	
	
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getFD_LOST() {
		return FD_LOST;
	}
	public void setFD_LOST(String fD_LOST) {
		FD_LOST = fD_LOST;
	}
	public String getSV_LOST() {
		return SV_LOST;
	}
	public void setSV_LOST(String sV_LOST) {
		SV_LOST = sV_LOST;
	}
	public String getSIGN_LOST() {
		return SIGN_LOST;
	}
	public void setSIGN_LOST(String sIGN_LOST) {
		SIGN_LOST = sIGN_LOST;
	}
	public String getFILLER() {
		return FILLER;
	}
	public void setFILLER(String fILLER) {
		FILLER = fILLER;
	}

}
