package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N880_REST_RQ extends BaseRestBean_OLS implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2807113079970558062L;
	
	private String CUSIDN;//使用者帳號
	private String BNKCOD;//銀行代號
	private String ACN;//台幣帳戶
	private	String PINNEW;
	private	String FGTXWAY;//交易機制
	private	String TYPE;//交易機制
	//晶片金融卡
	private	String	ACNNO;
	private	String	iSeqNo;
	private	String	TAC;
	private	String	ISSUER;
	private	String	TRMID;
	private	String	pkcs7Sign;
	private	String	jsondc;
	
	//IDGATE
	private String sessionID;
	private String idgateID;
	private String txnID;
	
	
	public String getJsondc() {
		return jsondc;
	}
	public void setJsondc(String jsondc) {
		this.jsondc = jsondc;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getBNKCOD() {
		return BNKCOD;
	}
	public void setBNKCOD(String bNKCOD) {
		BNKCOD = bNKCOD;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getACNNO() {
		return ACNNO;
	}
	public void setACNNO(String aCNNO) {
		ACNNO = aCNNO;
	}
	public String getiSeqNo() {
		return iSeqNo;
	}
	public void setiSeqNo(String iSeqNo) {
		this.iSeqNo = iSeqNo;
	}
	public String getTAC() {
		return TAC;
	}
	public void setTAC(String tAC) {
		TAC = tAC;
	}
	public String getISSUER() {
		return ISSUER;
	}
	public void setISSUER(String iSSUER) {
		ISSUER = iSSUER;
	}
	public String getTRMID() {
		return TRMID;
	}
	public void setTRMID(String tRMID) {
		TRMID = tRMID;
	}
	public String getPkcs7Sign() {
		return pkcs7Sign;
	}
	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}
	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	public String getIdgateID() {
		return idgateID;
	}
	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}
	public String getTxnID() {
		return txnID;
	}
	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}
	
}
