package tw.com.fstop.nnb.spring.controller;

import java.net.URLEncoder;
import java.util.Base64;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.common.ResultCode;
import tw.com.fstop.nnb.service.Bond_Deal_Service;
import tw.com.fstop.nnb.service.Component_Service;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.util.DateUtil;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;


/**
 * 海外債特別約定事項同意書 Controller
 */
@SessionAttributes({ SessionUtil.CUSIDN, SessionUtil.KYCDATE, SessionUtil.WEAK, SessionUtil.XMLCOD,
		SessionUtil.DPMYEMAIL, SessionUtil.KYC, SessionUtil.STEP1_LOCALE_DATA, SessionUtil.STEP2_LOCALE_DATA,
		SessionUtil.STEP3_LOCALE_DATA, SessionUtil.CONFIRM_LOCALE_DATA, SessionUtil.IDGATE_TRANSDATA, SessionUtil.RESULT_LOCALE_DATA,
		SessionUtil.USERIP, SessionUtil.TRANSFER_DATA ,SessionUtil.TRANSFER_RESULT_TOKEN,SessionUtil.TRANSFER_RESULT_FINSH_TOKEN })
@Controller
@RequestMapping(value = "/BOND/DEAL")
public class Bond_Deal_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	I18n i18n;
	
	@Autowired
	Bond_Deal_Service bond_deal_service ;
	
	@Autowired
	Component_Service component_service ;
	
	/**
	 * 
	 * @param request
	 * @param requestParam
	 * @param model
	 * @return 
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/bond_sign")
	public String deal_sign(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		log.info("bond_sign");
		String jsondc = "";
		
		BaseResult bsN922 = new BaseResult();
		BaseResult bsB017 = new BaseResult();
		BaseResult bs = new BaseResult();
		
		SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, "");
		SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, "");
		
		try {

			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			
			if(StrUtil.isEmpty(cusidn)) {
				model.addAttribute(BaseResult.ERROR, bs);
				return target;
			}
			
			//取得今日民國日期 8位 前補0
			String signDate = DateUtil.getTWDate("");
			signDate = String.format("%08d", Integer.parseInt(signDate));
			log.info(" SIGN DATE  >> {}" , signDate);
			
			String ip = WebUtil.getIpAddr(request);
			
			okMap.put("CUSIDN", cusidn);
			okMap.put("RECDATE", signDate);
			okMap.put("IP", ip);
			
			bsN922 = bond_deal_service.N922_REST(cusidn);
			
			//N922 失敗情況 : return 
			if(bsN922 == null || !bsN922.getResult()){
				bs = bsN922;
				bs.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR,bsN922);
				return target;
			}
			
			Map<String,Object> dataN922 = (Map<String,Object>)bsN922.getData();
			okMap.put("BRHCOD", (String) dataN922.get("APLBRH"));
			
			log.info( "B017 Req >> {}" , okMap);
			
			bsB017 = bond_deal_service.B017_REST(okMap);
			
			if(bsB017 != null && bsB017.getResult()){
				Map<String,Object> dataB017 = (Map<String,Object>)bsB017.getData();
				//對帳單退件或e-mail與他人相同之客戶，其使用網銀申購海外債券交易比照基金網銀交易時出示提醒視窗管控。
				okMap.put("EMAILMSG", (String) dataB017.get("EMAILMSG"));
				// 測試用 EMAILMSG有值
//				okMap.put("EMAILMSG", "08");
				
				//測試用 Start
				//進入約定事項簽署頁
//				dataB017.put("AGREE", "N");
				//測試用 end
				if("Y".equals(dataB017.get("AGREE"))) {
					bs.setErrorMessage("ZX99", i18n.getMsg("LB.X2584"));
					bs.setResult(false);
					model.addAttribute(BaseResult.ERROR,bs);
					return target;
				}
			}else {
				//B017 失敗情況 : return 
				bs = bsB017;
				bs.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR,bsB017);
				return target;
			}
			
			
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(okMap), "UTF-8");
			
			okMap.put("jsondc", jsondc);
			
			bs = bond_deal_service.getTxToken();
			log.trace("bond_sign.TXTOKEN: " + ((Map<String, String>) bs.getData()).get("TXTOKEN"));
			if (bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN,
						((Map<String, String>) bs.getData()).get("TXTOKEN"));
			}
			
			bs.addAllData(okMap);
		}catch (Exception e) {
			log.error("bond_sign error >> {}", e);
		} finally {
			if (bs.getResult()) {
				target = "/bond/bond_sign";
				model.addAttribute(SessionUtil.TRANSFER_DATA, bs);
			}
		}
		
		return target;
	}
	
	/**
	 * 
	 * @param request
	 * @param requestParam
	 * @param model
	 * @return 
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/bond_sign_result")
	public String deal_sign_result(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		log.info("bond_sign_result");
		BaseResult bs = new BaseResult();
		String jsondc = "";
		String pkcs7Sign = "";
		
		jsondc = requestParam.get("jsondc");
		pkcs7Sign = requestParam.get("pkcs7Sign");
		requestParam.remove("jsondc");
		requestParam.remove("pkcs7Sign");
		
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
		okMap.put("jsondc", jsondc);
		okMap.put("pkcs7Sign", pkcs7Sign);
		
		try {
			
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			
			if(StrUtil.isEmpty(cusidn)) {
				model.addAttribute(BaseResult.ERROR, bs);
				return target;
			}
			
			
			log.info(ESAPIUtil.vaildLog("bond_sign_result.okMap >> " + CodeUtil.toJson(okMap)));
			
			log.info("QQQ >> {}" , SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
			
			if (StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && okMap.get("TXTOKEN")
					.equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null))) {
				bs.setResult(Boolean.TRUE);
			}

			if (!bs.getResult()) {
				log.error(ESAPIUtil.vaildLog("bond_sign_result TXTOKEN 不正確，TXTOKEN >> " + okMap.get("TXTOKEN")));
				throw new Exception();
			}
			bs.reset();

			// 2.要驗證token是否與完成交易的TOKEN重複
			if (StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && !okMap.get("TXTOKEN")
					.equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null))) {
				bs.setResult(Boolean.TRUE);
			}
			if (!bs.getResult()) {
				log.error(ESAPIUtil
						.vaildLog("bond_sign_result TXTOKEN 不正確，或重複交易，TXTOKEN >> " + okMap.get("TXTOKEN")));
				throw new Exception();
			}
			bs.reset();
			
			bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
			Map<String, String> transfer_data = (Map<String, String>) bs.getData();
			
			okMap.putAll(transfer_data);
			
			bs.reset();
			
			log.info( "B025 Req >> {}" , okMap);
			
			bs = bond_deal_service.B025_REST(okMap);
			
		}catch (Exception e) {
			
			log.error("bond_sign_result error >> {}",e);
			bs.setMessage(ResultCode.SYS_ERROR);
		
		}finally {
			
			if (bs.getResult()) {
				// 交易成功要存之前的token 在Session 以利下次比對是否重複交易
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TXTOKEN"));
				model.addAttribute("bond_sign_result", bs);
				target =  "/bond/bond_sign_result";
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		
		}
		
		return target;
	}
	
	
	/**
	 * 
	 * @param request
	 * @param requestParam
	 * @param model
	 * @return 
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/mail_sign")
	public String mail_sign(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		log.info("mail_sign");
		String jsondc = "";
		
		BaseResult bs = new BaseResult();
		try {

			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			
			if(StrUtil.isEmpty(cusidn)) {
				model.addAttribute(BaseResult.ERROR, bs);
				return target;
			}
			okMap.put("CUSIDN", cusidn);
			
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(okMap), "UTF-8");
			
			okMap.put("jsondc", jsondc);
			
			bs.addAllData(okMap);
			bs.setResult(true);
		}catch (Exception e) {
			log.error("bond_sign error >> {}", e);
		} finally {
			if (bs.getResult()) {
				model.addAttribute("mail_sign_data", bs);
				target = "/bond/mail_sign";
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
				return target;
			}
		}
		
		return target;
	}
}
