package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N072_REST_RQ extends BaseRestBean_CC implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7786850093558236892L;

	// n072Telcomm need
	private String SYNC;
	private String PPSYNC;
	private String FLAG;
	private String BNKRA = "050";
	private String XMLCA;
	private String XMLCN;
	private String PPSYNCN;
	private String PINNEW;
	private String CUSIDN;
	private String ACN;
	private String CARDNUM;
	private String AMOUNT;
	private String MAC;
	private String TRANSEQ;
	private String ISSUER;
	private String ACNNO;
	private String ICDTTM;
	private String ICSEQ;
	private String ICMEMO;
	private String TAC_Length;
	private String TAC;
	private String TAC_120space;
	private String TRMID;

	// ms_tw交易機制需要的欄位
	private String FGTXWAY; // 交易機制
	private String iSeqNo; // iSeqNo
	private String FGTXDATE; // 即時或預約
	private String pkcs7Sign; // IKEY
	private String jsondc; // IKEY
	
	
	//N072.java need
	private String CMTRDATE; //預約日
	private String ADOPID ="N072"; //功能代號
	private String CMTRMEMO; //交易備註
	private String CMTRMAIL; // mail列表
	private String CMMAILMEMO; //mail備註
	
	//IDGATE
	private String sessionID;
	private String idgateID;
	private String txnID;


	public String getSYNC() {
		return SYNC;
	}

	public String getPPSYNC() {
		return PPSYNC;
	}

	public String getFLAG() {
		return FLAG;
	}

	public String getBNKRA() {
		return BNKRA;
	}

	public String getXMLCA() {
		return XMLCA;
	}

	public String getXMLCN() {
		return XMLCN;
	}

	public String getPPSYNCN() {
		return PPSYNCN;
	}

	public String getPINNEW() {
		return PINNEW;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public String getACN() {
		return ACN;
	}

	public String getCARDNUM() {
		return CARDNUM;
	}

	public String getAMOUNT() {
		return AMOUNT;
	}

	public String getMAC() {
		return MAC;
	}

	public String getTRANSEQ() {
		return TRANSEQ;
	}

	public String getISSUER() {
		return ISSUER;
	}

	public String getACNNO() {
		return ACNNO;
	}

	public String getICDTTM() {
		return ICDTTM;
	}

	public String getICSEQ() {
		return ICSEQ;
	}

	public String getICMEMO() {
		return ICMEMO;
	}

	public String getTAC_Length() {
		return TAC_Length;
	}

	public String getTAC() {
		return TAC;
	}

	public String getTAC_120space() {
		return TAC_120space;
	}

	public String getTRMID() {
		return TRMID;
	}

	public String getFGTXDATE() {
		return FGTXDATE;
	}

	public String getFGTXWAY() {
		return FGTXWAY;
	}

	public void setSYNC(String sYNC) {
		SYNC = sYNC;
	}

	public void setPPSYNC(String pPSYNC) {
		PPSYNC = pPSYNC;
	}

	public void setFLAG(String fLAG) {
		FLAG = fLAG;
	}

	public void setBNKRA(String bNKRA) {
		BNKRA = bNKRA;
	}

	public void setXMLCA(String xMLCA) {
		XMLCA = xMLCA;
	}

	public void setXMLCN(String xMLCN) {
		XMLCN = xMLCN;
	}

	public void setPPSYNCN(String pPSYNCN) {
		PPSYNCN = pPSYNCN;
	}

	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	public void setCARDNUM(String cARDNUM) {
		CARDNUM = cARDNUM;
	}

	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}

	public void setMAC(String mAC) {
		MAC = mAC;
	}

	public void setTRANSEQ(String tRANSEQ) {
		TRANSEQ = tRANSEQ;
	}

	public void setISSUER(String iSSUER) {
		ISSUER = iSSUER;
	}

	public void setACNNO(String aCNNO) {
		ACNNO = aCNNO;
	}

	public void setICDTTM(String iCDTTM) {
		ICDTTM = iCDTTM;
	}

	public void setICSEQ(String iCSEQ) {
		ICSEQ = iCSEQ;
	}

	public void setICMEMO(String iCMEMO) {
		ICMEMO = iCMEMO;
	}

	public void setTAC_Length(String tAC_Length) {
		TAC_Length = tAC_Length;
	}

	public void setTAC(String tAC) {
		TAC = tAC;
	}

	public void setTAC_120space(String tAC_120space) {
		TAC_120space = tAC_120space;
	}

	public void setTRMID(String tRMID) {
		TRMID = tRMID;
	}

	public void setFGTXDATE(String fGTXDATE) {
		FGTXDATE = fGTXDATE;
	}

	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}

	public String getiSeqNo() {
		return iSeqNo;
	}

	public void setiSeqNo(String iSeqNo) {
		this.iSeqNo = iSeqNo;
	}

	public String getPkcs7Sign() {
		return pkcs7Sign;
	}

	public String getJsondc() {
		return jsondc;
	}

	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}

	public void setJsondc(String jsondc) {
		this.jsondc = jsondc;
	}

	public String getCMTRDATE() {
		return CMTRDATE;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setCMTRDATE(String cMTRDATE) {
		CMTRDATE = cMTRDATE;
	}

	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}

	public String getCMTRMEMO() {
		return CMTRMEMO;
	}

	public String getCMTRMAIL() {
		return CMTRMAIL;
	}

	public String getCMMAILMEMO() {
		return CMMAILMEMO;
	}

	public void setCMTRMEMO(String cMTRMEMO) {
		CMTRMEMO = cMTRMEMO;
	}

	public void setCMTRMAIL(String cMTRMAIL) {
		CMTRMAIL = cMTRMAIL;
	}

	public void setCMMAILMEMO(String cMMAILMEMO) {
		CMMAILMEMO = cMMAILMEMO;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public String getIdgateID() {
		return idgateID;
	}

	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}

	public String getTxnID() {
		return txnID;
	}

	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}
}
