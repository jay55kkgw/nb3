package tw.com.fstop.nnb.service;

import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import fstop.orm.po.ADMCURRENCY;
import fstop.orm.po.ADMHOLIDAY;
import fstop.orm.po.SYSPARAMDATA;
import fstop.orm.po.TXNCUSINVATTRHIST;
import fstop.orm.po.TXNFUNDCOMPANY;
import fstop.orm.po.TXNFUNDDATA;
import fstop.orm.po.TXNFUNDTRACELOG;
import fstop.orm.po.TXNUSER;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.common.ResultCode;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmCurrencyDao;
import tw.com.fstop.tbb.nnb.dao.AdmHolidayDao;
import tw.com.fstop.tbb.nnb.dao.SysParamDataDao;
import tw.com.fstop.tbb.nnb.dao.TxnCusInvAttrHistDao;
import tw.com.fstop.tbb.nnb.dao.TxnFundCompanyDao;
import tw.com.fstop.tbb.nnb.dao.TxnFundDataDao;
import tw.com.fstop.tbb.nnb.dao.TxnFundTraceLogDao;
import tw.com.fstop.tbb.nnb.dao.TxnUserDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.StrUtils;
import tw.com.fstop.web.util.WebUtil;

/**
 * 基金轉換交易的Service
 */
@Service
public class Fund_Transfer_Service extends Base_Service{
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private I18n i18n;
	@Autowired
	private SysParamDataDao sysParamDataDao;
	@Autowired
	private AdmHolidayDao admHolidayDao;
	@Autowired
	private TxnFundDataDao txnFundDataDao;
	@Autowired
	private AdmCurrencyDao admCurrencyDao;
	@Autowired
	private TxnCusInvAttrHistDao txnCusInvAttrHistDao;
	@Autowired
	private TxnFundCompanyDao txnFundCompanyDao;
	@Autowired
	private TxnFundTraceLogDao txnFundTraceLogDao;
	@Autowired
	private TxnUserDao txnUserDao;
	
	/**
	 * 基金轉換交易查詢
	 */
	@SuppressWarnings("unchecked")
	public BaseResult query_fund_transfer_data(String cusidn){
		BaseResult bs = null;
		try{
			bs = new BaseResult();
			bs = C021_REST(cusidn);
			if(bs != null && bs.getResult()){
				Map<String,Object> bsData = (Map<String,Object>)bs.getData();
				List<Map<String,String>> rows = (List<Map<String,String>>)bsData.get("REC");
				
				String CDNO;
				String hiddenCDNO;
				String CRY;
				ADMCURRENCY admCurrency;
				String ADCCYNAME;
				String ADCCYENGNAME;
				String ADCCYCHSNAME;
				String FUNDAMT;
				String formatFUNDAMT;
				String UNIT;
				String formatUNIT;
				String FUSMON;
				
				
//				//Fake Data
//				Map<String, String> fakeData = new HashMap<>();
//				fakeData.put("TRANSCODE", "C481");
//				fakeData.put("CREDITNO", "99999999999");
//				fakeData.put("CDNO", "99999999999");
//				fakeData.put("CRY", "USD");
//				fakeData.put("FUNDAMT", "3500000.00");
//				fakeData.put("UNIT", "000131910950");
//				fakeData.put("SHORTTRADE", "00000000");
//				fakeData.put("SHORTTUNIT", "000000000000");
//				fakeData.put("FUSMON", "036");
//				rows.add(fakeData);
				
				for(int x=0;x<rows.size();x++){
					Map<String,String> rowMap = rows.get(x);
					
					CDNO = rowMap.get("CDNO");
					log.debug("CREDITNO={}",CDNO);
					hiddenCDNO = WebUtil.hideAccount(CDNO);
//					log.debug("hiddenCREDITNO={}",hiddenCREDITNO);
					rowMap.put("hiddenCDNO",hiddenCDNO);
					
					CRY = rowMap.get("CRY");
					log.debug("CRY={}",CRY);
					admCurrency = getCRYData(CRY);
					
					if(admCurrency != null){
						ADCCYNAME = admCurrency.getADCCYNAME();
						log.debug("ADCCYNAME={}",ADCCYNAME);
						rowMap.put("ADCCYNAME",ADCCYNAME);
						ADCCYENGNAME = admCurrency.getADCCYENGNAME();
						log.debug("ADCCYENGNAME={}",ADCCYENGNAME);
						rowMap.put("ADCCYENGNAME",ADCCYENGNAME);
						ADCCYCHSNAME = admCurrency.getADCCYCHSNAME();
						log.debug("ADCCYCHSNAME={}",ADCCYCHSNAME);
						rowMap.put("ADCCYCHSNAME",ADCCYCHSNAME);
					}
					FUNDAMT = rowMap.get("FUNDAMT");
					log.debug("FUNDAMT={}",FUNDAMT);
					FUNDAMT = FUNDAMT.replaceAll("\\.","").replaceAll("\\,","");
					log.debug("FUNDAMT={}",FUNDAMT);
					
					formatFUNDAMT = formatNumberString(FUNDAMT,2);
					log.debug("formatFUNDAMT={}",formatFUNDAMT);
					rowMap.put("formatFUNDAMT",formatFUNDAMT);
					
					UNIT = rowMap.get("UNIT");
					log.debug("UNIT={}",UNIT);
					UNIT = UNIT.replaceAll("\\.","").replaceAll("\\,","");
					log.debug("UNIT={}",UNIT);
					
					formatUNIT = formatNumberString(UNIT,4);
					log.debug("formatUNIT={}",formatUNIT);
					rowMap.put("formatUNIT",formatUNIT);
					
					FUSMON = rowMap.get("FUSMON");
					if("000".equals(FUSMON)) {
						rowMap.put("FEE_TYPE", "B");
					}else {
						rowMap.put("FEE_TYPE", "A");
					}
					
				}
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("query_fund_transfer_data error >> {}",e);
		}
		return bs;
	}
	/**
	 * 將字串加上千分位和截取後面的小數位數
	 */
	public String formatNumberString(String value,int dotNum){
		String result = value;
		
		try{
			while(value.length() < dotNum){
				value = "0" + value;
			}
			log.debug(ESAPIUtil.vaildLog("value= >>" + value));
			
			String firstPart = value.substring(0,value.length() - dotNum);
			log.debug(ESAPIUtil.vaildLog("firstPart= >>" + firstPart));
			
			String secondPart = value.substring(value.length() - dotNum,value.length());
			log.debug(ESAPIUtil.vaildLog("secondPart={}"+secondPart));
			
			if(firstPart.length() > 0){
				firstPart = String.valueOf(new DecimalFormat().format(Long.valueOf(firstPart)));
			}
			else{
				firstPart = "0";
			}
			log.debug("firstPart={}",firstPart);
			
			if(dotNum > 0){
				result = firstPart + "." + secondPart;
			}
			else{
				result = firstPart;
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("formatNumberString error >> {}",e);
		}
		log.debug(ESAPIUtil.vaildLog("result={}"+result));
		
		return result;
	}
	/**
	 * 判斷基金執行即時交易或預約交易（基金預約轉換會用到）
	 * true即時交易
	 * false預約交易
	 */
	public boolean isFundTradeTime(){
		boolean result = false;
		
		Date date = new Date();
		String currentDate = new SimpleDateFormat("yyyyMMdd").format(date);
		log.debug("currentDate={}",currentDate);
		String currentTime = new SimpleDateFormat("HHmmss").format(date);
		log.debug("currentTime={}",currentTime);
		
		SYSPARAMDATA sysParamData = sysParamDataDao.getByPK("NBSYS");

		if(sysParamData != null && sysParamData.getADPK() != null){
			String tradeStart = sysParamData.getADFDSHH() + sysParamData.getADFDSMM() + sysParamData.getADFDSSS();
			log.debug("tradeStart={}",tradeStart);
			String tradeEnd = sysParamData.getADFDEHH() + sysParamData.getADFDEMM() + sysParamData.getADFDESS();			
			log.debug("tradeEnd={}",tradeEnd);
			
			ADMHOLIDAY admHoliday = admHolidayDao.getByPK(currentDate);
			log.debug("admHoliday={}",admHoliday);
			//非假日且在營業時間內
			if(admHoliday != null && admHoliday.getADHOLIDAYID() == null && currentTime.compareTo(tradeStart) > 0 && currentTime.compareTo(tradeEnd) < 0){
				result = true;
			}
		}
		return result;
	}
	/**
	 * 判斷基金執行即時交易或預約交易或不能交易（基金預約單筆申購會用到）
	 * A即時交易
	 * B預約交易
	 * C不能交易
	 */
	public String isFundTradeTimeType2(){
		String result = "C";
		
		Date date = new Date();
		String currentDate = new SimpleDateFormat("yyyyMMdd").format(date);
		log.debug("currentDate={}",currentDate);
		String currentTime = new SimpleDateFormat("HHmmss").format(date);
		log.debug("currentTime={}",currentTime);
		
		SYSPARAMDATA sysParamData = sysParamDataDao.getByPK("NBSYS");

		if(sysParamData != null && sysParamData.getADPK() != null){
			String tradeStart = sysParamData.getADFDSHH() + sysParamData.getADFDSMM() + sysParamData.getADFDSSS();
			log.debug("tradeStart={}",tradeStart);
			String tradeEnd = sysParamData.getADFDEHH() + sysParamData.getADFDEMM() + sysParamData.getADFDESS();			
			log.debug("tradeEnd={}",tradeEnd);
			
			ADMHOLIDAY admHoliday = admHolidayDao.getByPK(currentDate);
			log.debug("admHoliday={}",admHoliday);
			//非假日且在營業時間內
			if(admHoliday != null && admHoliday.getADHOLIDAYID() == null && currentTime.compareTo(tradeStart) > 0 && currentTime.compareTo(tradeEnd) < 0){
				result = "A";
			}
			//非假日但不在營業時間內
			else if(admHoliday != null && admHoliday.getADHOLIDAYID() == null && (currentTime.compareTo(tradeStart) < 0 || currentTime.compareTo(tradeEnd) > 0)){
				result = "B";
			}
			//20210915修改 , 若為假日 , 則導向預約頁
			else if(admHoliday != null && admHoliday.getADHOLIDAYID() != null) {
				result = "B";
			}
		}
		return result;
	}
	/**
	 * 取得問卷版本日期
	 */
	public String getKYCDate(){
		String KYCDate = null;
		
		SYSPARAMDATA po = null; 
		try{
			po = sysParamDataDao.getByPK("NBSYS");
			
			KYCDate = po.getKYCDATE().trim();
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getKYCDate error >> {}",e);
		}
		return KYCDate;
	}
	/**
	 * 依照TRANSCODE取得基金的資料
	 */
	public TXNFUNDDATA getFundData(String TRANSCODE){
		TXNFUNDDATA txnFundData = txnFundDataDao.getByPK(TRANSCODE);
		
		return txnFundData;
	}
	/**
	 * 取得幣別的資料
	 */
	public ADMCURRENCY getCRYData(String CRY){
		ADMCURRENCY admCurrency = admCurrencyDao.getByPK(CRY);
		
		return admCurrency;
	}
	/**
	 * 依據出生日期，算出實際年齡 
	 */	
	public int getAge(String yyyMMdd){
		int age = 0;
		Date date = new Date();
		String today = getROCDate(date);
		int nowyear = Integer.parseInt(today.substring(0,3));
		int nowmonth = Integer.parseInt(today.substring(3,5));
		int nowday = Integer.parseInt(today.substring(5,7));
		int year = Integer.parseInt(yyyMMdd.substring(0,3));
		int month = Integer.parseInt(yyyMMdd.substring(3,5));
		int day = Integer.parseInt(yyyMMdd.substring(5,7));
		//計算實際年齡
		if(nowmonth > month){
			age = nowyear - year;
		}
		else if(nowmonth == month && nowday >= day){
			age = nowyear - year;
		}
		else{
			age = nowyear - year - 1;
		}
		log.debug("age={}",age);
		
	  	return age;
	}
	/**
	 * 取得ROC日期
	 */
	public String getROCDate(Date date){
		String dateString = new SimpleDateFormat("yyyyMMdd").format(date);
		String result = dateString.substring(4);
		result = new Integer(dateString.substring(0,4)) - 1911 + result;
		if(result.length() == 6){
			result = "0" + result;
		}
		log.debug("result={}",result);
		
		return result;
	}
	/**
	 * 算出time1跟time2間隔幾日 
	 */	
	public long getRangeDay(String time1,String time2){
		long rangeDay = 0;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		try{
			Date date1 = sdf.parse(time1);
			Date date2 = sdf.parse(time2);
			rangeDay = date1.getTime() - date2.getTime();
			rangeDay = rangeDay / 1000 / 60 / 60 / 24;
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getRangeDay error >> {}",e);
		}
		return rangeDay;
	}
	/**
	 * 準備前往基金風險警告頁的資料
	 */
	public void prepareFundRiskAlertData(Map<String,String> requestParam,String GETLTD,String RISK7,Model model){
		String INTRANSCODE = requestParam.get("INTRANSCODE");
	    log.debug(ESAPIUtil.vaildLog("INTRANSCODE={}"+INTRANSCODE));
	    
	    if(INTRANSCODE != null){
	    	model.addAttribute("INTRANSCODE",INTRANSCODE);
	    	
			String INFUNDSNAME = requestParam.get("INFUNDSNAME");
			log.debug(ESAPIUtil.vaildLog("INFUNDSNAME={}"+INFUNDSNAME));
			model.addAttribute("INFUNDSNAME",INFUNDSNAME);
			
			String FUNDAMT = requestParam.get("FUNDAMT");
			log.debug(ESAPIUtil.vaildLog("FUNDAMT={}"+FUNDAMT));
			if(FUNDAMT != null){
				model.addAttribute("FUNDAMT",FUNDAMT);
			}
			String RRSK = requestParam.get("RRSK");
			log.debug(ESAPIUtil.vaildLog("RRSK={}"+RRSK));
			if(RRSK != null){
				model.addAttribute("RRSK",RRSK);
			}
			String RSKATT = requestParam.get("RSKATT");
			log.debug(ESAPIUtil.vaildLog("RSKATT={}"+RSKATT));
			if(RSKATT != null){
				model.addAttribute("RSKATT",RSKATT);
			}
			if(GETLTD != null){
				model.addAttribute("GETLTD",GETLTD);
			}
			if(RISK7 != null){
				model.addAttribute("RISK7",RISK7);
			}
			String GETLTD7 = requestParam.get("GETLTD7");
			log.debug(ESAPIUtil.vaildLog("GETLTD7={}"+GETLTD7));
			if(GETLTD7 != null){
				model.addAttribute("GETLTD7",GETLTD7);
			}
		}
	}
	/**
	 * 查詢客戶當日KYC已填寫次數
	 */
	public boolean todayUserKYCCount(String UID){
		boolean result = false;
		
		String nowDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		log.debug("nowDate={}",nowDate);
		
		List<TXNCUSINVATTRHIST> TXNCUSINVATTRHISTList = txnCusInvAttrHistDao.findUserKycCount(UID,nowDate);
		log.debug(ESAPIUtil.vaildLog("TXNCUSINVATTRHISTList={}"+TXNCUSINVATTRHISTList));
		
		if(TXNCUSINVATTRHISTList.size() <= 2){
			result = true;
		}
		log.debug("result={}",result);
		
		return result;
	}
	/**
	 * 基金下單歸戶帳號查詢
	 */
	public BaseResult getN922Data(String UID){
		BaseResult bs = null;
		try{
			bs = new BaseResult();
			bs = N922_REST(UID);
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getN922Data error >> {}",e);
		}
		return bs;
	}
	/**
	 * 投資屬性查詢
	 */
	public BaseResult getN927Data(String UID){
		BaseResult bs = null;
		try{
			bs = new BaseResult();
			bs = N927_REST(UID);
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getN927Data error >> {}",e);
		}
		return bs;
	}
	/**
	 * 準備前往個人KYC輸入頁的資料
	 */
	@SuppressWarnings("unchecked")
	public void prepareInvestAttr1Data(BaseResult bs,String TXID,Model model){
		Map<String,Object> bsData = (Map<String,Object>)bs.getData();
		//投資屬性
		String RISK;
		if(TXID != null && !"".equals(TXID) && TXID.substring(0,1).equals("C")){
			RISK = bsData.get("FDINVTYPE") == null ? "" : (String)bsData.get("FDINVTYPE");
		}
		else{
			RISK = bsData.get("RISK") == null ? "" : (String)bsData.get("RISK");
		}
		log.debug("RISK={}",RISK);
		model.addAttribute("RISK",RISK);
		//生日
		String BRTHDY = bsData.get("BRTHDY") == null ? "" : (String)bsData.get("BRTHDY");
		log.debug("BRTHDY={}",BRTHDY);
		model.addAttribute("BRTHDY",BRTHDY);
		//學歷
		String DEGREE = bsData.get("DEGREE") == null ? "" : (String)bsData.get("DEGREE");
		log.debug("DEGREE={}",DEGREE);
		model.addAttribute("DEGREE",DEGREE);
		//職業
		String CAREER = bsData.get("CAREER") == null ? "" : (String)bsData.get("CAREER");
		log.debug("CAREER={}",CAREER);
		model.addAttribute("CAREER",CAREER);
		//年收入
		String SALARY = bsData.get("SALARY") == null ? "0" : (String)bsData.get("SALARY");
		log.debug("SALARY={}",SALARY);
		model.addAttribute("SALARY",SALARY);
		//重大傷病註記
		String MARK1 = bsData.get("MARK1") == null ? "" : (String)bsData.get("MARK1");
		log.debug("MARK1={}",MARK1);
		model.addAttribute("MARK1",MARK1);
		
		int salary = Integer.parseInt(SALARY) * 1000;
		log.debug("salary={}",salary);
		model.addAttribute("salary",salary);
		
	    int age = getAge(BRTHDY);
	    log.debug("age={}",age);
		model.addAttribute("age",age);
	    
		String Q1A = "";
	    String Q2A = "";
	    
	    if(age < 20){
	    	Q1A = "A";
	    }
	    else if(age >= 20 && age < 40){
	    	Q1A = "B";
	    }
	    else if(age >= 40 && age < 55){
	    	Q1A = "C";
	    }
	    else if(age >= 50 && age < 70){
	    	Q1A = "D";
	    }
	    else{
	        Q1A = "E";
	    }
	    log.debug("Q1A={}",Q1A);
	    model.addAttribute("Q1A",Q1A);
	    
	    if(DEGREE.equals("1")){
	    	Q2A = "A";
	    }
	    else if(DEGREE.equals("2")){ 
	    	Q2A = "B";
	    }
	    else if(DEGREE.equals("3")){	    
	    	Q2A = "C";
	    }
	    else if(DEGREE.equals("4")){   
	    	Q2A = "D";
	    }
	    else if(DEGREE.equals("5")){   
	    	Q2A = "E";
	    }
	    else if(DEGREE.equals("6")){  
	    	Q2A = "F";
	    }
	    log.debug("Q2A={}",Q2A);
	    model.addAttribute("Q2A",Q2A);
	}
	/**
	 * 準備前往個人KYC確認頁的資料
	 */
	public BaseResult prepareInvestAttr2Data(Map<String,String> requestParam,Model model){
		
	    BaseResult bs = new BaseResult();
	    
	    try{
		    
			// 通用處理
			for(String key : requestParam.keySet()) {
				String value = requestParam.get(key);
				log.debug(ESAPIUtil.vaildLog("prepareInvestAttr2Data.data: "+ key +" = "+ value));
				model.addAttribute(key, value);
			}
						
			String RTC = requestParam.get("RTC");
			log.debug(ESAPIUtil.vaildLog("RTC={}"+RTC));
			model.addAttribute("RTC",RTC);
			
			//判斷是不是從線上申請進來，如果是的話，KYC頁不顯示選單及麵包屑
			String ONLINEAPPLY = requestParam.get("ONLINEAPPLY");
			log.debug(ESAPIUtil.vaildLog("ONLINEAPPLY={}"+ONLINEAPPLY));
			model.addAttribute("ONLINEAPPLY",ONLINEAPPLY);
			
			String UPD_FLG = requestParam.get("UPD_FLG");
			log.debug(ESAPIUtil.vaildLog("UPD_FLG={}"+UPD_FLG));
			model.addAttribute("UPD_FLG",UPD_FLG);
			model.addAttribute("UPD_FLG1",UPD_FLG.substring(0,1));
			model.addAttribute("UPD_FLG2",UPD_FLG.substring(1,2));
			model.addAttribute("UPD_FLG3",UPD_FLG.substring(2,3));
			
			String RISK = requestParam.get("RISK");
			log.debug(ESAPIUtil.vaildLog("RISK={}"+RISK));
			model.addAttribute("RISK",RISK);
			
			String OMARK1 = requestParam.get("OMARK1");
			log.debug(ESAPIUtil.vaildLog("OMARK1={}"+OMARK1));
			model.addAttribute("OMARK1",OMARK1);
			
			String MARK1 = requestParam.get("MARK1");
			log.debug(ESAPIUtil.vaildLog("MARK1={}"+MARK1));
			model.addAttribute("MARK1",MARK1);
			
			String DEGREEString = requestParam.get("Q2A");
			log.debug(ESAPIUtil.vaildLog("DEGREEString={}"+DEGREEString));
			model.addAttribute("DEGREEString",DEGREEString);
			
			String Q2String = requestParam.get("Q2");
			log.debug(ESAPIUtil.vaildLog("Q2String={}"+Q2String));
			model.addAttribute("Q2String",Q2String);
			
			String Q3 = requestParam.get("Q3");
			log.debug(ESAPIUtil.vaildLog("Q3={}"+Q3));
			model.addAttribute("Q3",Q3);
			
			String CAREER = requestParam.get("Q3A");
			log.debug(ESAPIUtil.vaildLog("CAREER={}"+CAREER));
			model.addAttribute("CAREER",CAREER);
			String CAREERCHINESE = careerSwitch(CAREER);
			log.debug("CAREERCHINESE={}",CAREERCHINESE);
			model.addAttribute("CAREERCHINESE",CAREERCHINESE);
			
			String SRCFUND = requestParam.get("SRCFUND");
			log.debug(ESAPIUtil.vaildLog("SRCFUND={}"+SRCFUND));
			model.addAttribute("SRCFUND",SRCFUND);
			String SRCFUNDCHINESE = careerSwitch(SRCFUND);
			log.debug("SRCFUNDCHINESE={}",SRCFUNDCHINESE);
			model.addAttribute("SRCFUNDCHINESE",SRCFUNDCHINESE);
			
			String Q4 = requestParam.get("Q4");
			log.debug(ESAPIUtil.vaildLog("Q4={}"+Q4));
			model.addAttribute("Q4",Q4);
			
			String Q5 = requestParam.get("Q5");
			log.debug(ESAPIUtil.vaildLog("Q5={}"+Q5));
			model.addAttribute("Q5",Q5);
			
			String Q6 = requestParam.get("Q6");
			log.debug(ESAPIUtil.vaildLog("Q6={}"+Q6));
			model.addAttribute("Q6",Q6);
			
			String Q7 = requestParam.get("Q7");
			log.debug(ESAPIUtil.vaildLog("Q7={}"+Q7));
			model.addAttribute("Q7",Q7);
			
			String Q8 = requestParam.get("Q8");
			log.debug(ESAPIUtil.vaildLog("Q8={}"+Q8));
			model.addAttribute("Q8",Q8);
			
			String Q9 = requestParam.get("Q9");
			log.debug(ESAPIUtil.vaildLog("Q9={}"+Q9));
			model.addAttribute("Q9",Q9);
			
			String Q10 = requestParam.get("Q10");
			log.debug(ESAPIUtil.vaildLog("Q10={}"+Q10));
			model.addAttribute("Q10",Q10);
			
			String SALARYString = requestParam.get("Q4A");
			log.debug(ESAPIUtil.vaildLog("SALARYString={}"+SALARYString));
			model.addAttribute("SALARYString",SALARYString);
			int oldsalary = Integer.valueOf(SALARYString) / 10000;
			log.debug("oldsalary={}",oldsalary);
			model.addAttribute("oldsalary",oldsalary);
			
			
			String newsalary =requestParam.get("newsalary");
			log.debug(ESAPIUtil.vaildLog("newsalary={}"+newsalary));
			model.addAttribute("newsalary",newsalary);
			
			int SALARY = (Integer.parseInt(newsalary) * 10000) / 1000;
			log.debug("SALARY={}",SALARY);
			model.addAttribute("SALARY",SALARY);
			
			String DEGREE = "";
			String DEGREE_DESC = "";
			String NEWDEGREE_DESC = "";
		    
			if(DEGREEString.equals("A")){
				DEGREE_DESC = "博士";
			}
		    else if(DEGREEString.equals("B")){ 
		    	DEGREE_DESC = "碩士";
		    }
		    else if(DEGREEString.equals("C")){
		    	DEGREE_DESC = "大學";
		    }
		    else if(DEGREEString.equals("D")){	    
		    	DEGREE_DESC = "專科";
		    }
		    else if(DEGREEString.equals("E")){	    
		    	DEGREE_DESC = "高中職";
		    }
		    else if(DEGREEString.equals("F")){	    
		    	DEGREE_DESC = "國中以下";
		    }
			log.debug("DEGREE_DESC={}",DEGREE_DESC);
			model.addAttribute("DEGREE_DESC",DEGREE_DESC);
		    
		    if(Q2String.equals("A")){
		    	NEWDEGREE_DESC = "博士";
		    	DEGREE = "1";
		    }
		    else if(Q2String.equals("B")){
		    	NEWDEGREE_DESC = "碩士";
		    	DEGREE = "2";
		    }
		    else if(Q2String.equals("C")){
		    	NEWDEGREE_DESC = "大學";
		    	DEGREE = "3";
		    }
		    else if(Q2String.equals("D")){
		    	NEWDEGREE_DESC = "專科";
		    	DEGREE = "4";        
		    }
		    else if(Q2String.equals("E")){
		    	NEWDEGREE_DESC = "高中職";
		    	DEGREE = "5";         
		    }
		    else if(Q2String.equals("F")){
		    	NEWDEGREE_DESC = "國中以下";
		    	DEGREE = "6";    		
			}
			log.debug("NEWDEGREE_DESC={}",NEWDEGREE_DESC);
			model.addAttribute("NEWDEGREE_DESC",NEWDEGREE_DESC);
			
		    log.debug("DEGREE={}",DEGREE);
		    model.addAttribute("DEGREE",DEGREE);
		    //判斷弱勢群組
		    boolean weakpeople = false;
		    String Q1 = requestParam.get("Q1");
		    log.debug(ESAPIUtil.vaildLog("Q1={}"+Q1));
		    model.addAttribute("Q1",Q1);
		    
			if(MARK1.equals("Y") || DEGREE.equals("6") || "D".equals(Q1)){
				weakpeople = true;
			}
			log.debug("weakpeople={}",weakpeople);
			model.addAttribute("weakpeople",weakpeople);
			
			String Q11 = "";
			String Q13 = "";
			String str = "";
			String ANSWER ="";
			
			
			//以下為計分過程 ,依然有寫但最後不用  , 改用電文的回傳值
			//只留下要放進model的判斷
			for (int i = 1; i <= 15; i++) {
				if (i == 11 || i == 13) {
					for (int j = 1; j <= 4; j++) {
						if (i == 11 && requestParam.get("Q" + i + j) != null) {
							Q11 += requestParam.get("Q" + i + j);
							model.addAttribute("Q" + i + j, requestParam.get("Q" + i + j));
						}
						if (i == 13 && requestParam.get("Q" + i + j) != null) {
							Q13 += requestParam.get("Q" + i + j);
							model.addAttribute("Q" + i + j, requestParam.get("Q" + i + j));
						}
					}
					if (i == 11) {
						str = Q11.substring(0, 1);
					}
					if (i == 13) {
						str = Q13.substring(0, 1);
					}
					log.debug(ESAPIUtil.vaildLog("str={}" + str));
				}

				ANSWER += str;
				log.debug(ESAPIUtil.vaildLog("ANSWER={}" + ANSWER));
			}
			log.debug(ESAPIUtil.vaildLog("Q11={}"+Q11));
			model.addAttribute("Q11",Q11);
			
			String Q12 = requestParam.get("Q12");
			log.debug(ESAPIUtil.vaildLog("Q12={}"+Q12));
			model.addAttribute("Q12",Q12);
			
			log.debug(ESAPIUtil.vaildLog("Q13={}"+Q13));
			model.addAttribute("Q13",Q13);
			
		    log.debug(ESAPIUtil.vaildLog("ANSWER={}"+ANSWER));
		    model.addAttribute("ANSWER",ANSWER);
		    
		    String Q14 = requestParam.get("Q14");
		    log.debug(ESAPIUtil.vaildLog("Q14={}"+Q14));
		    model.addAttribute("Q14",Q14);
		    
		    String Q15 = requestParam.get("Q15");
		    log.debug(ESAPIUtil.vaildLog("Q15={}"+Q15));
		    model.addAttribute("Q15",Q15);
		    
		    //IKEY要使用的JSON:DC
		 	String jsondc = URLEncoder.encode(CodeUtil.toJson(requestParam),"UTF-8");
		 	log.debug(ESAPIUtil.vaildLog("jsondc={}"+jsondc));
		 	model.addAttribute("jsondc",jsondc);
		 	bs.setResult(true);
	    }
		catch(Exception e){
		    bs.setResult(false);
		    log.error("prepareInvestAttr2Data Exception");
			log.error("prepareInvestAttr2Data error >> {}",e);
		}
	    return bs;
	}
	/**
	 * 執行個人KYC變更
	 */
	public BaseResult fund_invest_attr_s(Map<String,String> requestParam){
		BaseResult bs = null;
		try{
			bs = new BaseResult();
			bs = InvAttribS_REST(requestParam);
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_invest_attr_s error >> {}",e);
		}
		return bs;
	}
	/**
	 * 準備前往法人KYC輸入頁的資料
	 */
	@SuppressWarnings("unchecked")
	public void prepareInvestAttr3Data(BaseResult bs,String TXID,Model model){
		Map<String,Object> bsData = (Map<String,Object>)bs.getData();
		//投資屬性
		String RISK;
		if(TXID != null && !"".equals(TXID) && TXID.substring(0,1).equals("C")){
			RISK = bsData.get("FDINVTYPE") == null ? "" : (String)bsData.get("FDINVTYPE");
		}
		else{
			RISK = bsData.get("RISK") == null ? "" : (String)bsData.get("RISK");
		}
		log.debug("RISK={}",RISK);
		model.addAttribute("RISK",RISK);
	}
	/**
	 * 準備前往法人KYC確認頁的資料
	 */
	public void prepareInvestAttr4Data(Map<String,String> requestParam,Model model){
		try{
			String RTC = requestParam.get("RTC");
			log.debug(ESAPIUtil.vaildLog("RTC={}"+RTC));
			model.addAttribute("RTC",RTC);
			
			String RISK = requestParam.get("RISK");
			log.debug(ESAPIUtil.vaildLog("RISK={}"+RISK));
			model.addAttribute("RISK",RISK);
			
			String Q1 = requestParam.get("Q1");
			log.debug(ESAPIUtil.vaildLog("Q1={}"+Q1));
			model.addAttribute("Q1",Q1);
			
			String Q2 = requestParam.get("Q2");
			log.debug(ESAPIUtil.vaildLog("Q2={}"+Q2));
			model.addAttribute("Q2",Q2);
			
			String Q3 = requestParam.get("Q3");
			log.debug(ESAPIUtil.vaildLog("Q3={}"+Q3));
			model.addAttribute("Q3",Q3);
			
			String Q4 = requestParam.get("Q4");
			log.debug(ESAPIUtil.vaildLog("Q4={}"+Q4));
			model.addAttribute("Q4",Q4);
			
			String Q5 = requestParam.get("Q5");
			log.debug(ESAPIUtil.vaildLog("Q5={}"+Q5));
			model.addAttribute("Q5",Q5);
			
			String Q6 = requestParam.get("Q6");
			log.debug(ESAPIUtil.vaildLog("Q6={}"+Q6));
			model.addAttribute("Q6",Q6);
			
			String Q8 = requestParam.get("Q8");
			log.debug(ESAPIUtil.vaildLog("Q8={}"+Q8));
			model.addAttribute("Q8",Q8);
			
			String Q10 = requestParam.get("Q10");
			log.debug(ESAPIUtil.vaildLog("Q10={}"+Q10));
			model.addAttribute("Q10",Q10);
			
			String Q11 = requestParam.get("Q11");
			log.debug(ESAPIUtil.vaildLog("Q11={}"+Q11));
			model.addAttribute("Q11",Q11);
			
			String Q12 = requestParam.get("Q12");
			log.debug(ESAPIUtil.vaildLog("Q12={}"+Q12));
			model.addAttribute("Q12",Q12);
			
			String Q13 = requestParam.get("Q13");
			log.debug(ESAPIUtil.vaildLog("Q13={}"+Q13));
			model.addAttribute("Q13",Q13);
			
		    String Q14 = requestParam.get("Q14");
		    log.debug(ESAPIUtil.vaildLog("Q14={}"+Q14));
		    model.addAttribute("Q14",Q14);
			
			int iScore = 0;
			String FDINVTYPE = "";
			String Q7 = "";
			String Q9 = "";
			String str = "";
			String ANSWER ="";
			
			for(int i=1;i<=14;i++){
				if(i != 7 && i != 9 && i != 1 && i != 2 && i != 4 && i != 5 && i != 8 && i != 11 && i != 12 && i != 13){
					str = requestParam.get("Q"+i);
					log.debug(ESAPIUtil.vaildLog("str={}"+str));
					
					if(str.equals("A")){
						iScore += 3;
					}
					else if(str.equals("B")){
						iScore += 2;
					}
					else if(str.equals("C")){
						iScore += 1;
					}
					else{
						iScore += 0;
					}
				}
				else{
					if(i == 7 || i == 9){
						for(int j=1;j<=4;j++){
							if(i == 7 && requestParam.get("Q" + i + j) != null){
								Q7 += requestParam.get("Q" + i + j);
								model.addAttribute("Q" + i + j,requestParam.get("Q" + i + j));
							}
				     		if(i == 9 && requestParam.get("Q" + i + j) != null){
				     			Q9 += requestParam.get("Q" + i + j);
				     			model.addAttribute("Q" + i + j,requestParam.get("Q" + i + j));
				     		}
						}
				     	if(i == 7){
				     		str = Q7.substring(0,1);
				     		model.addAttribute("Q7",Q7);
				     	}
				     	if(i == 9){
				     		str = Q9.substring(0,1);
				     		model.addAttribute("Q9",Q9);
				     	}
				     	log.debug(ESAPIUtil.vaildLog("str={}"+str));
				     	
				     	if(str.equals("A")){
				     		iScore += 3;
				     	}
				     	else if(str.equals("B")){
				     		iScore += 2;
				     	}
				     	else if(str.equals("C")){
				     		iScore += 1;
				     	}
				     	else{
				     		iScore += 0;
				     	}
					}
					if(i == 1 || i == 2 || i == 11 || i == 12 || i == 13){
						str = requestParam.get("Q" + i);		
						log.debug(ESAPIUtil.vaildLog("str={}"+str));
						
						if(str.equals("A")){
							iScore += 0;
						}
						else if(str.equals("B")){
							iScore += 1;
						}
						else if(str.equals("C")){
							iScore += 2;
						}
						else{
							iScore += 3;
						}
					}
					if(i == 4 || i == 5 || i == 8){
						str = requestParam.get("Q" + i);
						log.debug(ESAPIUtil.vaildLog("str={}"+str));
						
						if(str.equals("A")){
							iScore += 1;
						}
						else if(str.equals("B")){
							iScore += 2;
						}
						else if(str.equals("C")){
							iScore += 3;
						}
						else{
							iScore += 0;
						}
					}					    	   		       
				}
				log.debug("iScore={}",iScore);
				ANSWER += str;
                log.debug(ESAPIUtil.vaildLog("ANSWER={}"+ANSWER));
			}
		    
			if(iScore >= 26 && iScore <= 42 && !Q13.equals("A") && !Q14.equals("D")){	
				//積極型
				FDINVTYPE = "1";
			}	
			else if(iScore >= 14 && iScore <= 25 && !Q13.equals("A") && !Q14.equals("D")){	
				//穩健型
				FDINVTYPE = "2";
			}	
			else if(iScore <= 13 || Q13.equals("A") || Q14.equals("D")){
				//保守型
				FDINVTYPE = "3";
				iScore = 13;
			}	
			log.debug("FDINVTYPE={}",FDINVTYPE);
		    model.addAttribute("FDINVTYPE",FDINVTYPE);
			
		    
			log.debug("iScore={}",iScore);
		    model.addAttribute("iScore",iScore);
	        log.debug(ESAPIUtil.vaildLog("ANSWER={}"+ANSWER));
	        model.addAttribute("ANSWER",ANSWER);
			
		    //IKEY要使用的JSON:DC
		 	String jsondc = URLEncoder.encode(CodeUtil.toJson(requestParam),"UTF-8");
		 	log.debug(ESAPIUtil.vaildLog("jsondc={}"+jsondc));
		 	model.addAttribute("jsondc",jsondc);
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("prepareInvestAttr4Data error >> {}",e);
		}
	}
	/**
	 * 轉換職業中文
	 */
	public String careerSwitch(String careerID){
		String careerChinese = "";
		
		switch(careerID){
			case "061100":
				careerChinese = "國防事業";
				break;
			case "061200":
				careerChinese = "警察單位";
				break;
			case "061300":
				careerChinese = "其他公共行政類";
				break;
			case "061400":
				careerChinese = "教育業";
				break;
			case "061410":
				careerChinese = "學生";
				break;
			case "061500":
				careerChinese = "工、商及服務業";
				break;
			case "0615A0":
				careerChinese = "農林漁牧業";
				break;
			case "0615B0":
				careerChinese = "礦石及土石採取業";
				break;
			case "0615C0":
				careerChinese = "製造業";
				break;
			case "0615D0":
				careerChinese = "水電燃氣業";
				break;
			case "0615E0":
				careerChinese = "營造業";
				break;
			case "0615F0":
				careerChinese = "批發及零售業";
				break;
			case "0615G0":
				careerChinese = "住宿及餐飲業";
				break;
			case "0615H0":
				careerChinese = "運輸、倉儲及通信業";
				break;
			case "0615I0":
				careerChinese = "金融及保險業";
				break;
			case "0615J0":
				careerChinese = "不動產及租賃業";
				break;
			case "061610":
				careerChinese = "其他專業服務業";
				break;
			case "061620":
				careerChinese = "技術服務業";
				break;
			case "061630":
				careerChinese = "特定專業服務業(律師、會計師、地政士、公證人、記帳士、記帳及報稅代理人)";
				break;
			case "061640":
				careerChinese = "特定專業服務業(受聘於專業服務業之行政事務職員)";
				break;
			case "061650":
				careerChinese = "銀樓業(含珠寶、鐘錶及貴金屬之製造、批發及零售)";
				break;
			case "061660":
				careerChinese = "虛擬貨幣交易服務業";
				break;
			case "061670":
				careerChinese = "博弈業";
				break;
			case "061680":
				careerChinese = "國防武器或戰爭設備相關行業(軍火)";
				break;
			case "061690":
				careerChinese = "家管";
				break;
			case "061691":
				careerChinese = "自由業";
				break;
			case "061692":
				careerChinese = "無業";
				break;
			case "061700":
				careerChinese = "其他(無業家管退休)";
				break;
			case "069999":
				careerChinese = "非法人組織授信戶負責人";
				break;
		}
		log.debug("careerChinese={}",careerChinese);
		
		return careerChinese;
	}
	/**
	 * 查詢相同集團代碼註記之基金公司下的所有基金
	 * 集團代碼註記
	 * 同一集團表示
	 * 集團代碼不為空白且相同集團代碼，表示可以做轉換交易
	 */
	public List<TXNFUNDDATA> getFundDataByGroupMark(String feeType,String companyCode,String fusmon,String countrytype,String transcry){
		List<TXNFUNDDATA> finalFundDataList = new ArrayList<TXNFUNDDATA>();
		TXNFUNDCOMPANY company = txnFundCompanyDao.getByPK(companyCode.substring(0,2));
		
		if(company != null){
			String groupMark = company.getFUNDGROUPMARK();
			log.debug(ESAPIUtil.vaildLog("company ={}"+ company.getCOMPANYCODE()));
			log.debug(ESAPIUtil.vaildLog("groupMark={}"+groupMark));
			
			List<TXNFUNDDATA> fundDataList;
			if (groupMark.trim().equals("")){
				fundDataList = txnFundDataDao.findByCompany( feeType ,companyCode.substring(0,2), false);	
			}
			else{
				fundDataList = txnFundDataDao.findByFundGroupMark( feeType ,groupMark);						
			}
			log.debug(ESAPIUtil.vaildLog("fundDataList={}"+CodeUtil.toJson(fundDataList)));
			
			if(fundDataList != null){
				for(int x=0;x<fundDataList.size();x++){
					String FUNDMARK = fundDataList.get(x).getFUNDMARK();
					String TRANSCODE =  fundDataList.get(x).getTRANSCODE();
					String dataFUSMON =  fundDataList.get(x).getFUSMON();
					String dataTRANSCRY =  fundDataList.get(x).getTRANSCRY();
					log.debug(ESAPIUtil.vaildLog("FUNDMARK={}"+FUNDMARK));
					
					if("Y".equals(FUNDMARK) || "B".equals(FUNDMARK)){
						if("A".equals(feeType)) {
							//過濾掉自己 , 避免造成轉入轉出相同基金
							//========================================
							//境外基金 (COUNTRYTYPE = '' ) 只可轉"同期間" 不限幣別
							if(StrUtils.isEmpty(countrytype.trim())) {
								if(!TRANSCODE.equals(companyCode) && dataFUSMON.equals(fusmon)) {
									finalFundDataList.add(fundDataList.get(x));
								}
							//境內基金 (COUNTRYTYPE = B or C ) 只可轉"同期間" "同幣別"
							}else {
								if(!TRANSCODE.equals(companyCode) && dataFUSMON.equals(fusmon) && dataTRANSCRY.equals(transcry)) {
									finalFundDataList.add(fundDataList.get(x));
								}
							}
						}else{
							//過濾相同的一筆 ,防止轉出轉入相同基金
							// 過濾掉自己 , 避免造成轉入轉出相同基金
							if(!TRANSCODE.equals(companyCode)) {
								finalFundDataList.add(fundDataList.get(x));
							}
						}
						
					}
				}
				log.debug(ESAPIUtil.vaildLog("finalFundDataList={}"+CodeUtil.toJson(finalFundDataList)));
			}
		}
		return finalFundDataList;
	}
	/**
	 * 準備前往基金轉換選擇頁的資料
	 */
	public BaseResult prepareFundTransferSelectData(Map<String,String> requestParam){
		BaseResult bs = new BaseResult();
		try {
			bs.addAllData(requestParam);
			
			String CUSIDN = requestParam.get("CUSIDN");
			String hiddenCUSIDN = WebUtil.hideID(CUSIDN);
			bs.addData("hiddenCUSIDN",hiddenCUSIDN);
			
			String NAME = requestParam.get("NAME");
			String hiddenNAME = WebUtil.hideusername1Convert(NAME);
			bs.addData("hiddenNAME",hiddenNAME);
			
			String CDNO = requestParam.get("CDNO");
			String hiddenCDNO = WebUtil.hideAccount(CDNO);
			bs.addData("hiddenCDNO",hiddenCDNO);
			
			String FDINVTYPE = requestParam.get("FDINVTYPE");
			
			String FDINVTYPEChinese;
			if("1".equals(FDINVTYPE)){
				//積極型
				FDINVTYPEChinese = i18n.getMsg("LB.D0945");
			}
			else if("2".equals(FDINVTYPE)){
				//穩健型
				FDINVTYPEChinese = i18n.getMsg("LB.X1766");
			}
			else{
				//保守型
				FDINVTYPEChinese = i18n.getMsg("LB.X1767");
			}
			log.debug("FDINVTYPEChinese={}",FDINVTYPEChinese);
			bs.addData("FDINVTYPEChinese",FDINVTYPEChinese);
			
			String CRY = requestParam.get("CRY");
			ADMCURRENCY admCurrency = getCRYData(CRY);
			
			if(admCurrency != null){
				String ADCCYNAME = admCurrency.getADCCYNAME();
				log.debug(ESAPIUtil.vaildLog("ADCCYNAME={}"+ADCCYNAME));
				bs.addData("ADCCYNAME",ADCCYNAME);
				bs.addData("ADCCYENGNAME", admCurrency.getADCCYENGNAME());
				bs.addData("ADCCYCHSNAME", admCurrency.getADCCYCHSNAME());
			}
			
			String UNIT = requestParam.get("UNIT");
			log.debug(ESAPIUtil.vaildLog("UNIT={}"+UNIT));
			UNIT = formatNumberString(UNIT,4);
			log.debug(ESAPIUtil.vaildLog("UNIT={}"+UNIT));
			bs.addData("UNIT",UNIT);
			
			String RISK7 = requestParam.get("PRO");
			log.debug(ESAPIUtil.vaildLog("RISK7={}"+RISK7));
			bs.addData("RISK7",RISK7);
			
			String SHORTTRADE = requestParam.get("SHORTTRADE");
			log.debug(ESAPIUtil.vaildLog("SHORTTRADE={}"+SHORTTRADE));
			
			int shortDays = 0;
			
			if(!"00000000".equals(SHORTTRADE)){
				String systemDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
				log.debug("systemDate={}",systemDate);
				
				int year = Integer.parseInt(SHORTTRADE.substring(0,4)) + 1911;
				String shortMonth = SHORTTRADE.substring(4,6);
				String shortDay = SHORTTRADE.substring(6);
				SHORTTRADE = String.valueOf(year) + shortMonth + shortDay;
				
				shortDays = caculateDaysBetweenTwoDate(systemDate,SHORTTRADE);
			}
			log.debug(ESAPIUtil.vaildLog("SHORTTRADE={}"+SHORTTRADE));
			bs.addData("SHORTTRADE",SHORTTRADE);
			
			log.debug("shortDays={}",shortDays);
			bs.addData("shortDays",shortDays);
			
			String BILLSENDMODE = requestParam.get("BILLSENDMODE");
			
			String OFUNDAMT = requestParam.get("OFUNDAMT");
			log.debug(ESAPIUtil.vaildLog("OFUNDAMT={}"+OFUNDAMT));
			
			String OFUNDAMTDotTwo = OFUNDAMT.replaceAll("\\.","");
			log.debug("OFUNDAMTDotTwo={}",OFUNDAMTDotTwo);
			
			OFUNDAMTDotTwo = formatNumberString(OFUNDAMTDotTwo,2);
			log.debug("OFUNDAMTDotTwo={}",OFUNDAMTDotTwo);
			bs.addData("OFUNDAMTDotTwo",OFUNDAMTDotTwo);
			
			String BILLSENDMODEChinese;
			//全部
			if("1".equals(BILLSENDMODE)){
				BILLSENDMODEChinese = i18n.getMsg("LB.All");
				bs.addData("BILLSENDMODEChinese",BILLSENDMODEChinese);
				
				if (OFUNDAMT.indexOf(".") != -1){
					OFUNDAMT = OFUNDAMT.substring(0,OFUNDAMT.indexOf("."));		
				}
				log.debug(ESAPIUtil.vaildLog("OFUNDAMT={}"+OFUNDAMT));
				OFUNDAMT = formatNumberString(OFUNDAMT,0);
				log.debug(ESAPIUtil.vaildLog("OFUNDAMT={}"+OFUNDAMT));
				
				bs.addData("FUNDAMT",OFUNDAMT);
			}
			//部分
			else{
				BILLSENDMODEChinese = i18n.getMsg("LB.X0383");
				bs.addData("BILLSENDMODEChinese",BILLSENDMODEChinese);
			}
			
			String FEE_TYPE = requestParam.get("FEE_TYPE");
			log.debug(ESAPIUtil.vaildLog("FEE_TYPE={}"+FEE_TYPE));
			bs.addData("FEE_TYPE",FEE_TYPE);
			
			bs.setResult(Boolean.TRUE);
		}catch(Exception e) {
			log.debug(e.getMessage());
		}
		return bs;
	}
	/**
	 * 準備前往基金風險確認頁的資料
	 */
	public BaseResult prepareFundRiskConfirmData(Map<String,String> requestParam){
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			log.debug("Fund_Transfer_Service requestParam = {}", requestParam);
			
			String INTRANSCODE = requestParam.get("INTRANSCODE");
			log.debug(ESAPIUtil.vaildLog("INTRANSCODE={}"+INTRANSCODE));
			if(INTRANSCODE != null){
				bs.addData("INTRANSCODE",INTRANSCODE);
			}
				
			String TRANSCODE = requestParam.get("TRANSCODE");
			log.debug(ESAPIUtil.vaildLog("TRANSCODE={}"+TRANSCODE));
			bs.addData("TRANSCODE",TRANSCODE);
			
			String CDNO = requestParam.get("CDNO");
			log.debug(ESAPIUtil.vaildLog("CDNO={}"+CDNO));
			if(CDNO != null){
				bs.addData("CDNO",CDNO);
			}
			
			String UNIT = requestParam.get("UNIT");
			log.debug(ESAPIUtil.vaildLog("UNIT={}"+UNIT));
			if(UNIT != null){
				bs.addData("UNIT",UNIT);
			}
			
			String BILLSENDMODE = requestParam.get("BILLSENDMODE");
			log.debug(ESAPIUtil.vaildLog("BILLSENDMODE={}"+BILLSENDMODE));
			bs.addData("BILLSENDMODE",BILLSENDMODE);
			
			String SHORTTRADE = requestParam.get("SHORTTRADE");
			log.debug(ESAPIUtil.vaildLog("SHORTTRADE={}"+SHORTTRADE));
			if(SHORTTRADE != null){
				bs.addData("SHORTTRADE",SHORTTRADE);
			}
			
			String SHORTTUNIT = requestParam.get("SHORTTUNIT");
			log.debug(ESAPIUtil.vaildLog("SHORTTUNIT={}"+SHORTTUNIT));
			if(SHORTTUNIT != null){
				bs.addData("SHORTTUNIT",SHORTTUNIT);
			}
			
			String INFUNDSNAME = requestParam.get("INFUNDSNAME");
			log.debug(ESAPIUtil.vaildLog("INFUNDSNAME={}"+INFUNDSNAME));
			if(INFUNDSNAME != null){
				bs.addData("INFUNDSNAME",INFUNDSNAME);
			}
			
			String FUNDAMT = requestParam.get("FUNDAMT");
			log.debug(ESAPIUtil.vaildLog("FUNDAMT={}"+FUNDAMT));
			if(FUNDAMT != null){
				bs.addData("FUNDAMT",FUNDAMT);
			}
			
			String RRSK = requestParam.get("RRSK");
			log.debug(ESAPIUtil.vaildLog("RRSK={}"+RRSK));
			if(RRSK != null){
				bs.addData("RRSK",RRSK);
			}
			
			String RSKATT = requestParam.get("RSKATT");
			log.debug(ESAPIUtil.vaildLog("RSKATT={}"+RSKATT));
			if(RSKATT != null){
				bs.addData("RSKATT",RSKATT);
			}
			
			String GETLTD = requestParam.get("GETLTD");
			log.debug(ESAPIUtil.vaildLog("GETLTD={}"+GETLTD));
			if(GETLTD != null){
				bs.addData("GETLTD",GETLTD);
			}
			
			String RISK7 = requestParam.get("RISK7");
			log.debug(ESAPIUtil.vaildLog("RISK7={}"+RISK7));
			if(RISK7 != null){
				bs.addData("RISK7",RISK7);
			}
			
			String GETLTD7 = requestParam.get("GETLTD7");
			log.debug(ESAPIUtil.vaildLog("GETLTD7={}"+GETLTD7));
			if(GETLTD7 != null){
				bs.addData("GETLTD7",GETLTD7);
			}
			
			String FDINVTYPE = requestParam.get("FDINVTYPE");
			log.debug(ESAPIUtil.vaildLog("FDINVTYPE={}"+FDINVTYPE));
			if(FDINVTYPE != null){
				bs.addData("FDINVTYPE",FDINVTYPE);
			}
			
			String ACN1 = requestParam.get("ACN1");
			log.debug(ESAPIUtil.vaildLog("ACN1={}"+ACN1));
			if(ACN1 != null){
				bs.addData("ACN1",ACN1);
			}
			
			String ACN2 = requestParam.get("ACN2");
			log.debug(ESAPIUtil.vaildLog("ACN2={}"+ACN2));
			if(ACN2 != null){
				bs.addData("ACN2",ACN2);
			}
			
			String HTELPHONE = requestParam.get("HTELPHONE");
			log.debug(ESAPIUtil.vaildLog("HTELPHONE={}"+HTELPHONE));
			if(HTELPHONE != null){
				bs.addData("HTELPHONE",HTELPHONE);
			}

			String COUNTRYTYPE = requestParam.get("COUNTRYTYPE");
			log.debug(ESAPIUtil.vaildLog("COUNTRYTYPE={}"+COUNTRYTYPE));
			if(COUNTRYTYPE != null){
				bs.addData("COUNTRYTYPE",COUNTRYTYPE);
			}

			String COUNTRYTYPE1 = requestParam.get("COUNTRYTYPE1");
			log.debug(ESAPIUtil.vaildLog("COUNTRYTYPE1={}"+COUNTRYTYPE1));
			if(COUNTRYTYPE1 != null){
				bs.addData("COUNTRYTYPE1",COUNTRYTYPE1);
			}

			String CUTTYPE = requestParam.get("CUTTYPE");
			log.debug(ESAPIUtil.vaildLog("CUTTYPE={}"+CUTTYPE));
			if(CUTTYPE != null){
				bs.addData("CUTTYPE",CUTTYPE);
			}

			String BRHCOD = requestParam.get("BRHCOD");
			log.debug(ESAPIUtil.vaildLog("BRHCOD={}"+BRHCOD));
			if(BRHCOD != null){
				bs.addData("BRHCOD",BRHCOD);
			}

			String RISK = requestParam.get("RISK");
			log.debug(ESAPIUtil.vaildLog("RISK={}"+RISK));
			if(RISK != null){
				bs.addData("RISK",RISK);
			}

			String SALESNO = requestParam.get("SALESNO");
			log.debug(ESAPIUtil.vaildLog("SALESNO={}"+SALESNO));
			if(SALESNO != null){
				bs.addData("SALESNO",SALESNO);
			}

			String PRO = requestParam.get("PRO");
			log.debug(ESAPIUtil.vaildLog("PRO={}"+PRO));
			if(PRO != null){
				bs.addData("PRO",PRO);
			}

			String TYPE = requestParam.get("TYPE");
			log.debug(ESAPIUtil.vaildLog("TYPE={}"+TYPE));
			if(TYPE != null){
				bs.addData("TYPE",TYPE);
			}

			String COMPANYCODE = requestParam.get("COMPANYCODE");
			log.debug(ESAPIUtil.vaildLog("COMPANYCODE={}"+COMPANYCODE));
			if(COMPANYCODE != null){
				bs.addData("COMPANYCODE",COMPANYCODE);
			}

			String AMT3 = requestParam.get("AMT3");
			log.debug(ESAPIUtil.vaildLog("AMT3={}"+AMT3));
			if(AMT3 != null){
				bs.addData("AMT3",AMT3);
			}

			String YIELD = requestParam.get("YIELD");
			log.debug(ESAPIUtil.vaildLog("YIELD={}"+YIELD));
			if(YIELD != null){
				bs.addData("YIELD",YIELD);
			}

			String STOP = requestParam.get("STOP");
			log.debug(ESAPIUtil.vaildLog("STOP={}"+STOP));
			if(STOP != null){
				bs.addData("STOP",STOP);
			}
			
			String COUNTRY = requestParam.get("COUNTRY");
			log.debug(ESAPIUtil.vaildLog("COUNTRY={}"+COUNTRY));
			if(COUNTRY != null){
				bs.addData("COUNTRY",COUNTRY);
			}
			
			String FUS98E = requestParam.get("FUS98E");
			log.debug(ESAPIUtil.vaildLog("FUS98E={}"+FUS98E));
			if(FUS98E != null){
				bs.addData("FUS98E",FUS98E);
			}
			
			String FUNDLNAME = requestParam.get("FUNDLNAME");
			log.debug(ESAPIUtil.vaildLog("FUNDLNAME={}"+FUNDLNAME));
			if(FUNDLNAME != null){
				bs.addData("FUNDLNAME",FUNDLNAME);
			}
			
			String PAYTYPE = requestParam.get("PAYTYPE");
			log.debug(ESAPIUtil.vaildLog("PAYTYPE={}"+PAYTYPE));
			if(PAYTYPE != null){
				bs.addData("PAYTYPE",PAYTYPE);
			}
			
			String FUNDACN = requestParam.get("FUNDACN");
			log.debug(ESAPIUtil.vaildLog("FUNDACN={}"+FUNDACN));
			if(FUNDACN != null){
				bs.addData("FUNDACN",FUNDACN);
			}
			
			String MIP = requestParam.get("MIP");
			log.debug(ESAPIUtil.vaildLog("MIP={}"+MIP));
			if(MIP != null){
				bs.addData("MIP",MIP);
			}
			
			String PAYDAY1 = requestParam.get("PAYDAY1");
			log.debug(ESAPIUtil.vaildLog("PAYDAY1={}"+PAYDAY1));
			if(PAYDAY1 != null){
				bs.addData("PAYDAY1",PAYDAY1);
			}
			
			String PAYDAY2 = requestParam.get("PAYDAY2");
			log.debug(ESAPIUtil.vaildLog("PAYDAY2={}"+PAYDAY2));
			if(PAYDAY2 != null){
				bs.addData("PAYDAY2",PAYDAY2);
			}
			
			String PAYDAY3 = requestParam.get("PAYDAY3");
			log.debug(ESAPIUtil.vaildLog("PAYDAY3={}"+PAYDAY3));
			if(PAYDAY3 != null){
				bs.addData("PAYDAY3",PAYDAY3);
			}
			
			String PAYDAY4 = requestParam.get("PAYDAY4");
			log.debug(ESAPIUtil.vaildLog("PAYDAY4={}"+PAYDAY4));
			if(PAYDAY4 != null){
				bs.addData("PAYDAY4",PAYDAY4);
			}
			
			String PAYDAY5 = requestParam.get("PAYDAY5");
			log.debug(ESAPIUtil.vaildLog("PAYDAY5={}"+PAYDAY5));
			if(PAYDAY5 != null){
				bs.addData("PAYDAY5",PAYDAY5);
			}
			
			String PAYDAY6 = requestParam.get("PAYDAY6");
			log.debug(ESAPIUtil.vaildLog("PAYDAY6={}"+PAYDAY6));
			if(PAYDAY6 != null){
				bs.addData("PAYDAY6",PAYDAY6);
			}
			
			String PAYDAY7 = requestParam.get("PAYDAY7");
			log.debug(ESAPIUtil.vaildLog("PAYDAY7={}"+PAYDAY7));
			if(PAYDAY7 != null){
				bs.addData("PAYDAY7",PAYDAY7);
			}
			
			String PAYDAY8 = requestParam.get("PAYDAY8");
			log.debug(ESAPIUtil.vaildLog("PAYDAY8={}"+PAYDAY8));
			if(PAYDAY8 != null){
				bs.addData("PAYDAY8",PAYDAY8);
			}
			
			String PAYDAY9 = requestParam.get("PAYDAY9");
			log.debug(ESAPIUtil.vaildLog("PAYDAY9={}"+PAYDAY9));
			if(PAYDAY9 != null){
				bs.addData("PAYDAY9",PAYDAY9);
			}
			
			String INVTYPE = requestParam.get("INVTYPE");
			log.debug(ESAPIUtil.vaildLog("INVTYPE={}"+INVTYPE));
			if(INVTYPE != null){
				bs.addData("INVTYPE",INVTYPE);
			}
			
			String FEE_TYPE = requestParam.get("FEE_TYPE");
			log.debug(ESAPIUtil.vaildLog("FEE_TYPE={}"+FEE_TYPE));
			if(FEE_TYPE != null){
				bs.addData("FEE_TYPE",FEE_TYPE);
			}
			
			String TXID = requestParam.get("TXID");
			log.debug(ESAPIUtil.vaildLog("TXID={}"+TXID));
			bs.addData("TXID",TXID);
			
			String SLSNO = requestParam.get("SLSNO");
			log.debug(ESAPIUtil.vaildLog("SLSNO = {}" + SLSNO));
			bs.addData("SLSNO", SLSNO);
			
			String KYCNO = requestParam.get("KYCNO");
			log.debug(ESAPIUtil.vaildLog("KYCNO = {}" + KYCNO));
			bs.addData("KYCNO", KYCNO);
			
			String SAL01 = requestParam.get("SAL01");
			log.debug(ESAPIUtil.vaildLog("SAL01={}"+SAL01));
			bs.addData("SAL01",SAL01);
			
			String SAL03 = requestParam.get("SAL03");
			log.debug(ESAPIUtil.vaildLog("SAL03={}"+SAL03));
			bs.addData("SAL03",SAL03);
			
			String FUNDT = requestParam.get("FUNDT");
			log.debug(ESAPIUtil.vaildLog("FUNDT={}"+FUNDT));
			bs.addData("FUNDT",FUNDT);
			
			bs.setResult(Boolean.TRUE);
			
			log.debug("Fund_Transfer_Service bs.getData = {}", bs.getData());
		}catch(Exception e) {
			log.debug(e.getMessage());
		}
		return bs;
	}
	/**
	 * 記錄基金軌跡
	 */
	public void insertFundTraceLog(Map<String,String> paramMap){
		TXNFUNDTRACELOG txnFundTraceLog = new TXNFUNDTRACELOG();
		try{
			String UID = paramMap.get("UID");
			log.debug(ESAPIUtil.vaildLog("UID={}"+UID));
			txnFundTraceLog.setFDUSERID(UID);
			
			String FDTXTIME = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
			log.debug("FDTXTIME={}",FDTXTIME);
			txnFundTraceLog.setFDTXTIME(FDTXTIME);
			
			String FDAGREEFLAG = paramMap.get("FDAGREEFLAG");
			log.debug(ESAPIUtil.vaildLog("FDAGREEFLAG={}"+FDAGREEFLAG));
			FDAGREEFLAG = FDAGREEFLAG == null ? "" : FDAGREEFLAG;
			txnFundTraceLog.setFDAGREEFLAG(FDAGREEFLAG);
			
			String FDNOTICETYPE = paramMap.get("FDNOTICETYPE");
			log.debug(ESAPIUtil.vaildLog("FDNOTICETYPE={}"+FDNOTICETYPE));
			FDNOTICETYPE = FDNOTICETYPE == null ? "" : FDNOTICETYPE;
			txnFundTraceLog.setFDNOTICETYPE(FDNOTICETYPE);
			
			String FDPUBLICTYPE = paramMap.get("FDPUBLICTYPE");
			log.debug(ESAPIUtil.vaildLog("FDPUBLICTYPE={}"+FDPUBLICTYPE));
			FDPUBLICTYPE = FDPUBLICTYPE == null ? "" : FDPUBLICTYPE;
			txnFundTraceLog.setFDPUBLICTYPE(FDPUBLICTYPE);
			
			String FDTXSTATUS = paramMap.get("FDTXSTATUS");
			log.debug(ESAPIUtil.vaildLog("FDTXSTATUS={}"+FDTXSTATUS));
			FDTXSTATUS = FDTXSTATUS == null ? "" : FDTXSTATUS;
			txnFundTraceLog.setFDTXSTATUS(FDTXSTATUS);
			
			String FDTXLOG = paramMap.get("FDTXLOG");
			log.debug(ESAPIUtil.vaildLog("FDTXLOG={}"+FDTXLOG));
			FDTXLOG = FDTXLOG == null ? "" : FDTXLOG;
			txnFundTraceLog.setFDTXLOG(FDTXLOG);
			
			String ADTXNO = paramMap.get("ADTXNO");
			log.debug(ESAPIUtil.vaildLog("ADTXNO={}"+ADTXNO));
			ADTXNO = ADTXNO == null ? "" : ADTXNO;
			txnFundTraceLog.setADTXNO(ADTXNO);
			
			String UUID = getUUID();
			log.debug("UUID={}",UUID);
			txnFundTraceLog.setADGUID(UUID);
			
			txnFundTraceLogDao.save(txnFundTraceLog);
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("insertFundTraceLog error >> {}",e);
		}
	}
	/**
	 * 準備前往基金公開說明書頁的資料
	 */
	public BaseResult prepareFundPublicData(Map<String,String> requestParam){
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			String INTRANSCODE = requestParam.get("INTRANSCODE");
			log.debug(ESAPIUtil.vaildLog("INTRANSCODE={}"+INTRANSCODE));
			if(INTRANSCODE != null){
				bs.addData("INTRANSCODE",INTRANSCODE);
			}	
				
			String FDAGREEFLAG = requestParam.get("FDAGREEFLAG");
			log.debug(ESAPIUtil.vaildLog("FDAGREEFLAG={}"+FDAGREEFLAG));
			bs.addData("FDAGREEFLAG",FDAGREEFLAG);
				
			String TRANSCODE = requestParam.get("TRANSCODE");
			log.debug(ESAPIUtil.vaildLog("TRANSCODE={}"+TRANSCODE));
			bs.addData("TRANSCODE",TRANSCODE);
				
			String CDNO = requestParam.get("CDNO");
			log.debug(ESAPIUtil.vaildLog("CDNO={}"+CDNO));
			if(CDNO != null){
				bs.addData("CDNO",CDNO);
			}
				
			String UNIT = requestParam.get("UNIT");
			log.debug(ESAPIUtil.vaildLog("UNIT={}"+UNIT));
			if(UNIT != null){
				bs.addData("UNIT",UNIT);
			}
			
			String BILLSENDMODE = requestParam.get("BILLSENDMODE");
			log.debug(ESAPIUtil.vaildLog("BILLSENDMODE={}"+BILLSENDMODE));
			bs.addData("BILLSENDMODE",BILLSENDMODE);
				
			String SHORTTRADE = requestParam.get("SHORTTRADE");
			log.debug(ESAPIUtil.vaildLog("SHORTTRADE={}"+SHORTTRADE));
			if(SHORTTRADE != null){
				bs.addData("SHORTTRADE",SHORTTRADE);
			}
				
			String SHORTTUNIT = requestParam.get("SHORTTUNIT");
			log.debug(ESAPIUtil.vaildLog("SHORTTUNIT={}"+SHORTTUNIT));
			if(SHORTTUNIT != null){
				bs.addData("SHORTTUNIT",SHORTTUNIT);
			}
				
			String INFUNDSNAME = requestParam.get("INFUNDSNAME");
			log.debug(ESAPIUtil.vaildLog("INFUNDSNAME={}"+INFUNDSNAME));
			if(INFUNDSNAME != null){
				bs.addData("INFUNDSNAME",INFUNDSNAME);
			}
			
			String FUNDAMT = requestParam.get("FUNDAMT");
			log.debug(ESAPIUtil.vaildLog("FUNDAMT={}"+FUNDAMT));
			if(FUNDAMT != null){
				bs.addData("FUNDAMT",FUNDAMT);
			}
			
			String RRSK = requestParam.get("RRSK");
			log.debug(ESAPIUtil.vaildLog("RRSK={}"+RRSK));
			if(RRSK != null){
				bs.addData("RRSK",RRSK);
			}
			
			String RSKATT = requestParam.get("RSKATT");
			log.debug(ESAPIUtil.vaildLog("RSKATT={}"+RSKATT));
			if(RSKATT != null){
				bs.addData("RSKATT",RSKATT);
			}
			
			String GETLTD = requestParam.get("GETLTD");
			log.debug(ESAPIUtil.vaildLog("GETLTD={}"+GETLTD));
			if(GETLTD != null){
				bs.addData("GETLTD",GETLTD);
			}
			
			String RISK7 = requestParam.get("RISK7");
			log.debug(ESAPIUtil.vaildLog("RISK7={}"+RISK7));
			if(RISK7 != null){
				bs.addData("RISK7",RISK7);
			}
			
			String GETLTD7 = requestParam.get("GETLTD7");
			log.debug(ESAPIUtil.vaildLog("GETLTD7={}"+GETLTD7));
			if(GETLTD7 != null){
				bs.addData("GETLTD7",GETLTD7);
			}
			
			String FDINVTYPE = requestParam.get("FDINVTYPE");
			log.debug(ESAPIUtil.vaildLog("FDINVTYPE={}"+FDINVTYPE));
			if(FDINVTYPE != null){
				bs.addData("FDINVTYPE",FDINVTYPE);
			}
			
			String ACN1 = requestParam.get("ACN1");
			log.debug(ESAPIUtil.vaildLog("ACN1={}"+ACN1));
			if(ACN1 != null){
				bs.addData("ACN1",ACN1);
			}
			
			String ACN2 = requestParam.get("ACN2");
			log.debug(ESAPIUtil.vaildLog("ACN2={}"+ACN2));
			if(ACN2 != null){
				bs.addData("ACN2",ACN2);
			}
			
			String HTELPHONE = requestParam.get("HTELPHONE");
			log.debug(ESAPIUtil.vaildLog("HTELPHONE={}"+HTELPHONE));
			if(HTELPHONE != null){
				bs.addData("HTELPHONE",HTELPHONE);
			}

			String COUNTRYTYPE = requestParam.get("COUNTRYTYPE");
			log.debug(ESAPIUtil.vaildLog("COUNTRYTYPE={}"+COUNTRYTYPE));
			if(COUNTRYTYPE != null){
				bs.addData("COUNTRYTYPE",COUNTRYTYPE);
			}

			String COUNTRYTYPE1 = requestParam.get("COUNTRYTYPE1");
			log.debug(ESAPIUtil.vaildLog("COUNTRYTYPE1={}"+COUNTRYTYPE1));
			if(COUNTRYTYPE1 != null){
				bs.addData("COUNTRYTYPE1",COUNTRYTYPE1);
			}

			String CUTTYPE = requestParam.get("CUTTYPE");
			log.debug(ESAPIUtil.vaildLog("CUTTYPE={}"+CUTTYPE));
			if(CUTTYPE != null){
				bs.addData("CUTTYPE",CUTTYPE);
			}

			String BRHCOD = requestParam.get("BRHCOD");
			log.debug(ESAPIUtil.vaildLog("BRHCOD={}"+BRHCOD));
			if(BRHCOD != null){
				bs.addData("BRHCOD",BRHCOD);
			}

			String RISK = requestParam.get("RISK");
			log.debug(ESAPIUtil.vaildLog("RISK={}"+RISK));
			if(RISK != null){
				bs.addData("RISK",RISK);
			}

			String SALESNO = requestParam.get("SALESNO");
			log.debug(ESAPIUtil.vaildLog("SALESNO={}"+SALESNO));
			if(SALESNO != null){
				bs.addData("SALESNO",SALESNO);
			}

			String PRO = requestParam.get("PRO");
			log.debug(ESAPIUtil.vaildLog("PRO={}"+PRO));
			if(PRO != null){
				bs.addData("PRO",PRO);
			}

			String TYPE = requestParam.get("TYPE");
			log.debug(ESAPIUtil.vaildLog("TYPE={}"+TYPE));
			if(TYPE != null){
				bs.addData("TYPE",TYPE);
			}

			String COMPANYCODE = requestParam.get("COMPANYCODE");
			log.debug(ESAPIUtil.vaildLog("COMPANYCODE={}"+COMPANYCODE));
			if(COMPANYCODE != null){
				bs.addData("COMPANYCODE",COMPANYCODE);
			}

			String AMT3 = requestParam.get("AMT3");
			log.debug(ESAPIUtil.vaildLog("AMT3={}"+AMT3));
			if(AMT3 != null){
				bs.addData("AMT3",AMT3);
			}

			String YIELD = requestParam.get("YIELD");
			log.debug(ESAPIUtil.vaildLog("YIELD={}"+YIELD));
			if(YIELD != null){
				bs.addData("YIELD",YIELD);
			}

			String STOP = requestParam.get("STOP");
			log.debug(ESAPIUtil.vaildLog("STOP={}"+STOP));
			if(STOP != null){
				bs.addData("STOP",STOP);
			}
			
			String COUNTRY = requestParam.get("COUNTRY");
			log.debug(ESAPIUtil.vaildLog("COUNTRY={}"+COUNTRY));
			if(COUNTRY != null){
				bs.addData("COUNTRY",COUNTRY);
			}
			
			String FUS98E = requestParam.get("FUS98E");
			log.debug(ESAPIUtil.vaildLog("FUS98E={}"+FUS98E));
			if(FUS98E != null){
				bs.addData("FUS98E",FUS98E);
			}
			
			String FUNDLNAME = requestParam.get("FUNDLNAME");
			log.debug(ESAPIUtil.vaildLog("FUNDLNAME={}"+FUNDLNAME));
			if(FUNDLNAME != null){
				bs.addData("FUNDLNAME",FUNDLNAME);
			}
			
			String PAYTYPE = requestParam.get("PAYTYPE");
			log.debug(ESAPIUtil.vaildLog("PAYTYPE={}"+PAYTYPE));
			if(PAYTYPE != null){
				bs.addData("PAYTYPE",PAYTYPE);
			}
			
			String FUNDACN = requestParam.get("FUNDACN");
			log.debug(ESAPIUtil.vaildLog("FUNDACN={}"+FUNDACN));
			if(FUNDACN != null){
				bs.addData("FUNDACN",FUNDACN);
			}
			
			String MIP = requestParam.get("MIP");
			log.debug(ESAPIUtil.vaildLog("MIP={}"+MIP));
			if(MIP != null){
				bs.addData("MIP",MIP);
			}
			
			String PAYDAY1 = requestParam.get("PAYDAY1");
			log.debug(ESAPIUtil.vaildLog("PAYDAY1={}"+PAYDAY1));
			if(PAYDAY1 != null){
				bs.addData("PAYDAY1",PAYDAY1);
			}
			
			String PAYDAY2 = requestParam.get("PAYDAY2");
			log.debug(ESAPIUtil.vaildLog("PAYDAY2={}"+PAYDAY2));
			if(PAYDAY2 != null){
				bs.addData("PAYDAY2",PAYDAY2);
			}
			
			String PAYDAY3 = requestParam.get("PAYDAY3");
			log.debug(ESAPIUtil.vaildLog("PAYDAY3={}"+PAYDAY3));
			if(PAYDAY3 != null){
				bs.addData("PAYDAY3",PAYDAY3);
			}
			
			String PAYDAY4 = requestParam.get("PAYDAY4");
			log.debug(ESAPIUtil.vaildLog("PAYDAY4={}"+PAYDAY4));
			if(PAYDAY4 != null){
				bs.addData("PAYDAY4",PAYDAY4);
			}
			
			String PAYDAY5 = requestParam.get("PAYDAY5");
			log.debug(ESAPIUtil.vaildLog("PAYDAY5={}"+PAYDAY5));
			if(PAYDAY5 != null){
				bs.addData("PAYDAY5",PAYDAY5);
			}
			
			String PAYDAY6 = requestParam.get("PAYDAY6");
			log.debug(ESAPIUtil.vaildLog("PAYDAY6={}"+PAYDAY6));
			if(PAYDAY6 != null){
				bs.addData("PAYDAY6",PAYDAY6);
			}
			
			String PAYDAY7 = requestParam.get("PAYDAY7");
			log.debug(ESAPIUtil.vaildLog("PAYDAY7={}"+PAYDAY7));
			if(PAYDAY7 != null){
				bs.addData("PAYDAY7",PAYDAY7);
			}
			
			String PAYDAY8 = requestParam.get("PAYDAY8");
			log.debug(ESAPIUtil.vaildLog("PAYDAY8={}"+PAYDAY8));
			if(PAYDAY8 != null){
				bs.addData("PAYDAY8",PAYDAY8);
			}
			
			String PAYDAY9 = requestParam.get("PAYDAY9");
			log.debug(ESAPIUtil.vaildLog("PAYDAY9={}"+PAYDAY9));
			if(PAYDAY9 != null){
				bs.addData("PAYDAY9",PAYDAY9);
			}
			
			String INVTYPE = requestParam.get("INVTYPE");
			log.debug(ESAPIUtil.vaildLog("INVTYPE={}"+INVTYPE));
			if(INVTYPE != null){
				bs.addData("INVTYPE",INVTYPE);
			}
			
			
			String FEE_TYPE = requestParam.get("FEE_TYPE");
			log.debug(ESAPIUtil.vaildLog("FEE_TYPE={}"+FEE_TYPE));
			if(FEE_TYPE != null){
				bs.addData("FEE_TYPE",FEE_TYPE);
			}
			
			String TXID = requestParam.get("TXID");
			log.debug(ESAPIUtil.vaildLog("TXID={}"+TXID));
			bs.addData("TXID",TXID);
			
			String SLSNO = requestParam.get("SLSNO");
			log.debug(ESAPIUtil.vaildLog("SLSNO={}"+SLSNO));
			bs.addData("SLSNO",SLSNO);
			
			String KYCNO = requestParam.get("KYCNO");
			log.debug(ESAPIUtil.vaildLog("KYCNO={}"+KYCNO));
			bs.addData("KYCNO",KYCNO);
			
			String SAL01 = requestParam.get("SAL01");
			log.debug(ESAPIUtil.vaildLog("SAL01={}"+SAL01));
			bs.addData("SAL01",SAL01);
			
			String SAL03 = requestParam.get("SAL03");
			log.debug(ESAPIUtil.vaildLog("SAL03={}"+SAL03));
			bs.addData("SAL03",SAL03);

			String FUNDT = requestParam.get("FUNDT");
			log.debug(ESAPIUtil.vaildLog("FUNDT={}"+FUNDT));
			bs.addData("FUNDT",FUNDT);
			
			bs.setResult(Boolean.TRUE);
		}catch(Exception e) {
			log.debug(e.getMessage());
		}
		return bs;
	}
	/**
	 * 準備前往基金轉換手續費頁的資料
	 */
	@SuppressWarnings("unchecked")
	public BaseResult prepareFundFeeExposeData(Map<String,String> requestParam){
		log.debug("prepareFundFeeExposeData_requestParam = {}", requestParam);
		BaseResult bs = null;
		try{
			String FUNDAMT = requestParam.get("FUNDAMT");
			log.debug(ESAPIUtil.vaildLog("FUNDAMT={}"+FUNDAMT));
			
			//將轉換金額的逗號拿掉
			String FUNDAMTNoComma = FUNDAMT.replaceAll(",","");
			log.debug("FUNDAMTNoComma={}",FUNDAMTNoComma);
			requestParam.put("FUNDAMTNoComma",FUNDAMTNoComma);
			
			String UNIT = requestParam.get("UNIT");
			log.debug(ESAPIUtil.vaildLog("UNIT={}"+UNIT));
			//將單位數的逗號拿掉
			String UNITNoComma = UNIT.replaceAll(",","");
			log.debug("UNITNoComma={}",UNITNoComma);
			//將持有基金單位數還原成主機下來的值
			String UNITNoDot = UNITNoComma.replaceAll("\\.","");
			log.debug("UNITNoDot={}",UNITNoDot);
			requestParam.put("UNITNoDot",UNITNoDot);

			bs = new BaseResult();
			bs = C022Public_REST(requestParam);
			
			String TRANSCODE = requestParam.get("TRANSCODE");
			log.debug(ESAPIUtil.vaildLog("TRANSCODE={}"+TRANSCODE));
			bs.addData("TRANSCODE",TRANSCODE);
			
			String CDNO = requestParam.get("CDNO");
			log.debug(ESAPIUtil.vaildLog("CDNO={}"+CDNO));
			bs.addData("CDNO",CDNO);
			
			String INTRANSCODE = requestParam.get("INTRANSCODE");
			log.debug(ESAPIUtil.vaildLog("INTRANSCODE={}"+INTRANSCODE));
			bs.addData("INTRANSCODE",INTRANSCODE);
			
			String BILLSENDMODE = requestParam.get("BILLSENDMODE");
			log.debug(ESAPIUtil.vaildLog("BILLSENDMODE={}"+BILLSENDMODE));
			bs.addData("BILLSENDMODE",BILLSENDMODE);
			
			String RRSK = requestParam.get("RRSK");
			log.debug(ESAPIUtil.vaildLog("RRSK={}"+RRSK));
			bs.addData("RRSK",RRSK);
			
			String RISK7 = requestParam.get("RISK7");
			log.debug(ESAPIUtil.vaildLog("RISK7={}"+RISK7));
			bs.addData("RISK7",RISK7);
			
			String FDINVTYPE = requestParam.get("FDINVTYPE");
			log.debug(ESAPIUtil.vaildLog("FDINVTYPE={}"+FDINVTYPE));
			bs.addData("FDINVTYPE",FDINVTYPE);
			
			String SHORTTRADE = requestParam.get("SHORTTRADE");
			log.debug(ESAPIUtil.vaildLog("SHORTTRADE={}"+SHORTTRADE));
			bs.addData("SHORTTRADE",SHORTTRADE);
			
			String SHORTTUNIT = requestParam.get("SHORTTUNIT");
			log.debug(ESAPIUtil.vaildLog("SHORTTUNIT={}"+SHORTTUNIT));
			bs.addData("SHORTTUNIT",SHORTTUNIT);
			
			String FDAGREEFLAG = requestParam.get("FDAGREEFLAG");
			log.debug(ESAPIUtil.vaildLog("FDAGREEFLAG={}"+FDAGREEFLAG));
			bs.addData("FDAGREEFLAG",FDAGREEFLAG);
			
			String FDNOTICETYPE = requestParam.get("FDNOTICETYPE");
			log.debug(ESAPIUtil.vaildLog("FDNOTICETYPE={}"+FDNOTICETYPE));
			bs.addData("FDNOTICETYPE",FDNOTICETYPE);
			
			String FDPUBLICTYPE = requestParam.get("FDPUBLICTYPE");
			log.debug(ESAPIUtil.vaildLog("FDPUBLICTYPE={}"+FDPUBLICTYPE));
			bs.addData("FDPUBLICTYPE",FDPUBLICTYPE);
			
			String FEE_TYPE = requestParam.get("FEE_TYPE");
			log.debug(ESAPIUtil.vaildLog("FEE_TYPE={}"+FEE_TYPE));
			bs.addData("FEE_TYPE",FEE_TYPE);
			
			if(bs != null && bs.getResult()){
				Map<String,Object> bsData = (Map<String,Object>)bs.getData();
				
				String TRADEDATE = (String)bsData.get("TRADEDATE");
				log.debug("TRADEDATE={}",TRADEDATE);
				bs.addData("TRADEDATE",TRADEDATE);
				
				String FCA1 = (String)bsData.get("FCA1");
				log.debug("FCA1={}",FCA1);
				bs.addData("FCA1",FCA1);
				
				String FCA2 = (String)bsData.get("FCA2");
				log.debug("FCA2={}",FCA2);
				bs.addData("FCA2",FCA2);
				
				String AMT3 = (String)bsData.get("AMT3");
				log.debug("AMT3={}",AMT3);
				bs.addData("AMT3",AMT3);
				
				String AMT5 = (String)bsData.get("AMT5");
				log.debug("AMT5={}",AMT5);
				bs.addData("AMT5",AMT5);
				
				String SSLTXNO = (String)bsData.get("SSLTXNO");
				log.debug("SSLTXNO={}",SSLTXNO);
				bs.addData("SSLTXNO",SSLTXNO);
				
				String FUNCUR = (String)bsData.get("FUNCUR");
				log.debug("FUNCUR={}",FUNCUR);
				bs.addData("FUNCUR",FUNCUR);
				
				FUNDAMT = (String)bsData.get("FUNDAMT");
				log.debug("FUNDAMT={}",FUNDAMT);
				bs.addData("FUNDAMT",FUNDAMT);
				
				String FUNDAMTFormat = formatNumberString(FUNDAMT.replaceAll("\\.",""),2);
				log.debug("FUNDAMTFormat={}",FUNDAMTFormat);
				bs.addData("FUNDAMTFormat",FUNDAMTFormat);
				
				//將轉換金額的逗號拿掉，保留小數點
				FUNDAMTNoComma = FUNDAMTFormat.replaceAll(",","");
				log.debug("FUNDAMTNoComma={}",FUNDAMTNoComma);
				bs.addData("FUNDAMTNoComma",FUNDAMTNoComma);
				
				UNIT = (String)bsData.get("UNIT");
				log.debug("UNIT={}",UNIT);
				bs.addData("UNIT",UNIT);
				
				String UNITDot = formatNumberString(UNIT,4);
				log.debug("UNITDot={}",UNITDot);
				bs.addData("UNITDot",UNITDot);
				
				Long UNITInteger = Long.valueOf(UNIT);
				log.debug("UNITInteger={}",UNITInteger);
				bs.addData("UNITInteger",UNITInteger);
				
				Map<String,String> fundFeeExposeTextMap = getFundFeeExposeInfo(bsData,"C021");
				bs.addData("fundFeeExposeTextMap",fundFeeExposeTextMap);
				
				String TXID = requestParam.get("TXID");
				log.debug(ESAPIUtil.vaildLog("TXID={}"+TXID));
				bs.addData("TXID",TXID);
				
				String SLSNO = requestParam.get("SLSNO");
				log.debug(ESAPIUtil.vaildLog("SLSNO={}"+SLSNO));
				bs.addData("SLSNO",SLSNO);
				
				String SAL01 = requestParam.get("SAL01");
				log.debug(ESAPIUtil.vaildLog("SAL01={}"+SAL01));
				bs.addData("SAL01",SAL01);
				
				String SAL03 = requestParam.get("SAL03");
				log.debug(ESAPIUtil.vaildLog("SAL03={}"+SAL03));
				bs.addData("SAL03",SAL03);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("prepareFundFeeExposeData error >> {}",e);
		}
		return bs;
	}
	/**
	 * 準備前往基金轉換確認頁的資料
	 */
	public BaseResult prepareFundTransferConfirmData(Map<String,String> requestParam,String cusidn){
		BaseResult bs = null;
		try{
			bs = getN922Data(cusidn);
			if(bs != null && bs.getResult()){
				String hiddenCUSIDN = WebUtil.hideID(cusidn);
				bs.addData("hiddenCUSIDN",hiddenCUSIDN);
			
				Map<String,Object> bsData = (Map<String,Object>)bs.getData();
				//新台幣扣帳帳號
				log.debug("ACN1={}",(String)bsData.get("ACN1"));
				bs.addData("OUTACN",(String)bsData.get("ACN1"));
				//20210617修正  若有CUSNAME取CUSNAME 無則為NAME , 不然會是空的 //N922原住民姓名修改由NAME->CUSNAME  
				String NAME = "";
				if(StrUtils.isNotEmpty((String)bsData.get("CUSNAME"))) {
					NAME = (String)bsData.get("CUSNAME");
				}else {
					NAME = (String)bsData.get("NAME");
				}
				String hiddenNAME = WebUtil.hideusername1Convert(NAME);
				bs.addData("hiddenNAME",hiddenNAME);
				
				String TRANSCODE = requestParam.get("TRANSCODE");
				log.debug(ESAPIUtil.vaildLog("TRANSCODE={}"+TRANSCODE));
				bs.addData("TRANSCODE",TRANSCODE);
				
				String CDNO = requestParam.get("CDNO");
				log.debug(ESAPIUtil.vaildLog("CREDITNO={}"+CDNO));
				bs.addData("CDNO",CDNO);
				
				String hiddenCDNO = WebUtil.hideAccount(CDNO);
//				log.debug("hiddenCREDITNO={}",hiddenCREDITNO);
				bs.addData("hiddenCDNO",hiddenCDNO);
				
				String INTRANSCODE = requestParam.get("INTRANSCODE");
				log.debug(ESAPIUtil.vaildLog("INTRANSCODE={}"+INTRANSCODE));
				bs.addData("INTRANSCODE",INTRANSCODE);
				
				String UNIT = requestParam.get("UNIT");
				log.debug(ESAPIUtil.vaildLog("UNIT={}"+UNIT));
				bs.addData("UNIT",UNIT);
				
				String UNITInteger = requestParam.get("UNITInteger");
				log.debug(ESAPIUtil.vaildLog("UNITInteger={}"+UNITInteger));
				bs.addData("UNITInteger",UNITInteger);
				
				String UNITDot = requestParam.get("UNITDot");
				log.debug(ESAPIUtil.vaildLog("UNITDot={}"+UNITDot));
				bs.addData("UNITDot",UNITDot);
				
				String BILLSENDMODE = requestParam.get("BILLSENDMODE");
				log.debug(ESAPIUtil.vaildLog("BILLSENDMODE={}"+BILLSENDMODE));
				bs.addData("BILLSENDMODE",BILLSENDMODE);
				
				String BILLSENDMODEChinese;
				//全部
				if("1".equals(BILLSENDMODE)){
					BILLSENDMODEChinese = i18n.getMsg("LB.All");
					bs.addData("BILLSENDMODEChinese",BILLSENDMODEChinese);
				}
				//部分
				else{
					BILLSENDMODEChinese = i18n.getMsg("LB.X0383");
					bs.addData("BILLSENDMODEChinese",BILLSENDMODEChinese);
				}
				
				String FUNDAMT = requestParam.get("FUNDAMT");
				log.debug(ESAPIUtil.vaildLog("FUNDAMT={}"+FUNDAMT));
				bs.addData("FUNDAMT",FUNDAMT);
				
				String FUNDAMTFormat = requestParam.get("FUNDAMTFormat");
				log.debug(ESAPIUtil.vaildLog("FUNDAMTFormat={}"+FUNDAMTFormat));
				bs.addData("FUNDAMTFormat",FUNDAMTFormat);
				
				String FUNDAMTNoComma = requestParam.get("FUNDAMTNoComma");
				log.debug(ESAPIUtil.vaildLog("FUNDAMTNoComma={}"+FUNDAMTNoComma));
				bs.addData("FUNDAMTNoComma",FUNDAMTNoComma);
				
				String RRSK = requestParam.get("RRSK");
				log.debug(ESAPIUtil.vaildLog("RRSK={}"+RRSK));
				bs.addData("RRSK",RRSK);
				
				String TRADEDATE = requestParam.get("TRADEDATE");
				log.debug(ESAPIUtil.vaildLog("TRADEDATE={}"+TRADEDATE));
				bs.addData("TRADEDATE",TRADEDATE);
				
				String TRADEDATEFormat = TRADEDATE.substring(1,4) + "/" + TRADEDATE.substring(4,6) + "/" + TRADEDATE.substring(6);
				log.debug(ESAPIUtil.vaildLog("TRADEDATEFormat={}"+TRADEDATEFormat));
				bs.addData("TRADEDATEFormat",TRADEDATEFormat);
				
				String FCA1 = requestParam.get("FCA1");
				log.debug(ESAPIUtil.vaildLog("FCA1={}"+FCA1));
				bs.addData("FCA1",FCA1);
				
				String FCA1Format = formatNumberString(FCA1,2);
				log.debug(ESAPIUtil.vaildLog("FCA1Format={}"+FCA1Format));
				bs.addData("FCA1Format",FCA1Format);
				
				String AMT3 = requestParam.get("AMT3");
				log.debug(ESAPIUtil.vaildLog("AMT3={}"+AMT3));
				bs.addData("AMT3",AMT3);
				
				Integer AMT3Integer = Integer.valueOf(AMT3);
				log.debug("AMT3Integer={}",AMT3Integer);
				bs.addData("AMT3Integer",AMT3Integer);
				
				String FCA2 = requestParam.get("FCA2");
				log.debug(ESAPIUtil.vaildLog("FCA2={}"+FCA2));
				bs.addData("FCA2",FCA2);
				
				String FCA2Format = formatNumberString(FCA2,2);
				log.debug(ESAPIUtil.vaildLog("FCA2Format={}"+FCA2Format));
				bs.addData("FCA2Format",FCA2Format);
				
				String AMT5 = requestParam.get("AMT5");
				log.debug(ESAPIUtil.vaildLog("AMT5={}"+AMT5));
				bs.addData("AMT5",AMT5);

			    String SSLTXNO = requestParam.get("SSLTXNO");
				log.debug(ESAPIUtil.vaildLog("SSLTXNO={}"+SSLTXNO));
				bs.addData("SSLTXNO",SSLTXNO);
				
				String FUNCUR = requestParam.get("FUNCUR");
				log.debug(ESAPIUtil.vaildLog("FUNCUR={}"+FUNCUR));
				bs.addData("CRY",FUNCUR);
				
				String RISK7 = requestParam.get("RISK7");
				log.debug(ESAPIUtil.vaildLog("RISK7={}"+RISK7));
				bs.addData("RISK7",RISK7);
				bs.addData("PRO",RISK7);
				
				String FDINVTYPE = requestParam.get("FDINVTYPE");
				log.debug(ESAPIUtil.vaildLog("FDINVTYPE={}"+FDINVTYPE));
				bs.addData("FDINVTYPE",FDINVTYPE);
				bs.addData("RSKATT",FDINVTYPE);
				
				String SHORTTRADE = requestParam.get("SHORTTRADE");
				log.debug(ESAPIUtil.vaildLog("SHORTTRADE={}"+SHORTTRADE));
				bs.addData("SHORTTRADE",SHORTTRADE);
				
				String SHORTTUNIT = requestParam.get("SHORTTUNIT");
				log.debug(ESAPIUtil.vaildLog("SHORTTUNIT={}"+SHORTTUNIT));
				bs.addData("SHORTTUNIT",SHORTTUNIT);
				
				String FDAGREEFLAG = requestParam.get("FDAGREEFLAG");
				log.debug(ESAPIUtil.vaildLog("FDAGREEFLAG={}"+FDAGREEFLAG));
				bs.addData("FDAGREEFLAG",FDAGREEFLAG);
				
				String FDNOTICETYPE = requestParam.get("FDNOTICETYPE");
				log.debug(ESAPIUtil.vaildLog("FDNOTICETYPE={}"+FDNOTICETYPE));
				bs.addData("FDNOTICETYPE",FDNOTICETYPE);
				
				String FDPUBLICTYPE = requestParam.get("FDPUBLICTYPE");
				log.debug(ESAPIUtil.vaildLog("FDPUBLICTYPE={}"+FDPUBLICTYPE));
				bs.addData("FDPUBLICTYPE",FDPUBLICTYPE);
				
				String SHWD = requestParam.get("SHWD");
				log.debug(ESAPIUtil.vaildLog("SHWD= >>" + SHWD));
				bs.addData("SHWD",SHWD);
			
				
				String XFLAG = requestParam.get("XFLAG");
				log.debug(ESAPIUtil.vaildLog("XFLAG= >>" + XFLAG));
				bs.addData("XFLAG",XFLAG);
				
				String FEE_TYPE = requestParam.get("FEE_TYPE");
				log.debug(ESAPIUtil.vaildLog("FEE_TYPE= >>" + XFLAG));
				bs.addData("FEE_TYPE",FEE_TYPE);
				
				String SLSNO = requestParam.get("SLSNO");
				log.debug(ESAPIUtil.vaildLog("SLSNO= >>" + SLSNO));
				bs.addData("SLSNO",SLSNO);
				
				String SAL01 = requestParam.get("SAL01");
				log.debug(ESAPIUtil.vaildLog("SAL01= >>" + SAL01));
				bs.addData("SAL01",SAL01);
				
				String SAL03 = requestParam.get("SAL03");
				log.debug(ESAPIUtil.vaildLog("SAL03= >>" + SAL03));
				bs.addData("SAL03",SAL03);
			
				
				TXNFUNDDATA txnFundData = getFundData(TRANSCODE);
				String fundName = "";
				
				if(txnFundData != null){
					fundName = txnFundData.getFUNDLNAME();
					
					ADMCURRENCY admCurrency = getCRYData(FUNCUR);
					
					if(admCurrency != null){
						String ADCCYNAME = admCurrency.getADCCYNAME();
						log.debug("ADCCYNAME={}",ADCCYNAME);
						bs.addData("ADCCYNAME",ADCCYNAME);
						bs.addData("ADCCYENGNAME", admCurrency.getADCCYENGNAME());
						bs.addData("ADCCYCHSNAME", admCurrency.getADCCYCHSNAME());
					}
				}
				log.debug(ESAPIUtil.vaildLog("fundName={}"+fundName));
				bs.addData("fundName",fundName);
				
				txnFundData = getFundData(INTRANSCODE);
				String inFundName = "";
				
				if(txnFundData != null){
					inFundName = txnFundData.getFUNDLNAME();
				}
				log.debug(ESAPIUtil.vaildLog("inFundName={}"+inFundName));
				bs.addData("inFundName",inFundName);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("prepareFundTransferConfirmData error >> {}",e);
		}
		return bs;
	}
	/**
	 * 基金轉換交易
	 */
	@SuppressWarnings("unchecked")
	public BaseResult processFundTransfer(Map<String,String> requestParam){
		BaseResult bs = null;
		try{
			bs = new BaseResult();
//			String unit = NumericUtil.unfmtAmount(requestParam.get("UNIT"));
//			for(int i = 0; i < 12 - unit.length();i++) {
//				unit = "0" + unit;
//			}
//			requestParam.put("UNIT", unit);
			bs = N371_REST(requestParam);
			if(bs != null && bs.getResult()){
				Map<String,Object> bsData = (Map<String,Object>)bs.getData();
				log.debug("bsData={}",bsData);

				String cusidn = requestParam.get("CUSIDN");
				String hiddenCUSIDN = WebUtil.hideID(cusidn);
				log.debug(ESAPIUtil.vaildLog("CUSIDN={}"+cusidn));
				bs.addData("hiddenCUSIDN",hiddenCUSIDN);
				
				String OUTACN = requestParam.get("OUTACN");
				log.debug(ESAPIUtil.vaildLog("OUTACN={}"+OUTACN));
				bs.addData("OUTACN",OUTACN);
				
				String CDNO = (String)bsData.get("CDNO");
				log.debug("CREDITNO={}",CDNO);
				String hiddenCDNO = WebUtil.hideAccount(CDNO);
//				log.debug("hiddenCREDITNO={}",hiddenCREDITNO);
				bs.addData("hiddenCDNO",hiddenCDNO);
				
				String TRANSCODE = (String)bsData.get("TRANSCODE");
				log.debug("TRANSCODE={}",TRANSCODE);
				
				String FUNCUR = (String)bsData.get("FUNCUR");
				log.debug("FUNCUR={}",FUNCUR);
				
				TXNFUNDDATA txnFundData = getFundData(TRANSCODE);
				String fundName = "";
				
				if(txnFundData != null){
					fundName = txnFundData.getFUNDLNAME();
					
					ADMCURRENCY admCurrency = getCRYData(FUNCUR);
					
					if(admCurrency != null){
						String ADCCYNAME = admCurrency.getADCCYNAME();
						log.debug("ADCCYNAME={}",ADCCYNAME);
						bs.addData("ADCCYNAME",ADCCYNAME);
						bs.addData("ADCCYENGNAME", admCurrency.getADCCYENGNAME());
						bs.addData("ADCCYCHSNAME", admCurrency.getADCCYCHSNAME());
					}
				}
				log.debug("fundName={}",fundName);
				bs.addData("fundName",fundName);
				
				String INTRANSCODE = (String)bsData.get("INTRANSCODE");
				log.debug("INTRANSCODE={}",INTRANSCODE);
				
				txnFundData = getFundData(INTRANSCODE);
				String inFundName = "";
				
				if(txnFundData != null){
					inFundName = txnFundData.getFUNDLNAME();
				}
				log.debug("inFundName={}",inFundName);
				bs.addData("inFundName",inFundName);
				
				String BILLSENDMODE = (String)bsData.get("BILLSENDMODE");
				log.debug("BILLSENDMODE={}",BILLSENDMODE);
				
				String BILLSENDMODEChinese;
				//全部
				if("1".equals(BILLSENDMODE)){
					BILLSENDMODEChinese = i18n.getMsg("LB.All");
					bs.addData("BILLSENDMODEChinese",BILLSENDMODEChinese);
				}
				//部分
				else{
					BILLSENDMODEChinese = i18n.getMsg("LB.X0383");
					bs.addData("BILLSENDMODEChinese",BILLSENDMODEChinese);
				}
				
				String UNIT = (String)bsData.get("UNIT");
				log.debug("UNIT={}",UNIT);
				String UNITFormat = formatNumberString(UNIT,4);
				log.debug("UNITFormat={}",UNITFormat);
				bs.addData("UNITFormat",UNITFormat);
				
				String FUNDAMT = (String)bsData.get("FUNDAMT");
				log.debug("FUNDAMT={}",FUNDAMT);
				String FUNDAMTNoDot = FUNDAMT.replaceAll("\\.","");
				log.debug("FUNDAMTNoDot={}",FUNDAMTNoDot);
				
				String FUNDAMTFormat = formatNumberString(FUNDAMTNoDot,2);
				log.debug("FUNDAMTFormat={}",FUNDAMTFormat);
				bs.addData("FUNDAMTFormat",FUNDAMTFormat);
				
				String AMT3 = (String)bsData.get("AMT3");
				log.debug("AMT3={}",AMT3);
				Integer AMT3Integer = Integer.valueOf(AMT3);
				log.debug("AMT3Integer={}",AMT3Integer);
				bs.addData("AMT3Integer",AMT3Integer);
				
				String FCA2 = (String)bsData.get("FCA2");
				log.debug("FCA2={}",FCA2);
				String FCA2Format = formatNumberString(FCA2,2);
				log.debug("FCA2Format={}",FCA2Format);
				bs.addData("FCA2Format",FCA2Format);
				
				String FCA1 = (String)bsData.get("FCA1");
				log.debug("FCA1={}",FCA1);
				String FCA1Format = formatNumberString(FCA1,2);
				log.debug("FCA1Format={}",FCA1Format);
				bs.addData("FCA1Format",FCA1Format);
				
				String AMT5 = (String)bsData.get("AMT5");
				log.debug("AMT5={}",AMT5);
				String AMT5NoDot = AMT5.replaceAll("\\.","");
				log.debug("AMT5NoDot={}",AMT5NoDot);
				
				String AMT5Format = formatNumberString(AMT5NoDot,2);
				log.debug("AMT5Format={}",AMT5Format);
				bs.addData("AMT5Format",AMT5Format);
				
				String SLSNO = (String)bsData.get("SLSNO");
				log.debug("SLSNO={}",SLSNO);
				bs.addData("SLSNO",SLSNO);
				
				String TRADEDATE = requestParam.get("TRADEDATE");
				log.debug(ESAPIUtil.vaildLog("TRADEDATE={}"+TRADEDATE));
				
				String TRADEDATEFormat = TRADEDATE.substring(1,4) + "/" + TRADEDATE.substring(4,6) + "/" + TRADEDATE.substring(6);
				log.debug(ESAPIUtil.vaildLog("TRADEDATEFormat={}"+TRADEDATEFormat));
				bs.addData("TRADEDATEFormat",TRADEDATEFormat);
				
				BaseResult n922BS = new BaseResult();
				n922BS = getN922Data(requestParam.get("CUSIDN"));
				
				if(n922BS != null && n922BS.getResult()){
					Map<String,Object> n922BSData = (Map<String,Object>)n922BS.getData();
					log.debug("n922BSData={}",n922BSData);
					//20210617修正  若有CUSNAME取CUSNAME 無則為NAME , 不然會是空的 //N922原住民姓名修改由NAME->CUSNAME  
					String NAME = "";
					if(StrUtils.isNotEmpty((String)bsData.get("CUSNAME"))) {
						NAME = (String)bsData.get("CUSNAME");
					}else {
						NAME = (String)bsData.get("NAME");
					}
					
					String hiddenNAME = WebUtil.hideusername1Convert(NAME);
					log.debug("hiddenNAME={}",hiddenNAME);
					bs.addData("hiddenNAME",hiddenNAME);
				}
				
//				bs.addData("bsData",bsData);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("processFundTransfer error >> {}",e);
		}
		return bs;
	}
	/**
	 * 計算兩個日期之間的天數
	 */
	public int caculateDaysBetweenTwoDate(String beginDate,String endDate){
		int days;

        if(beginDate.indexOf("/") != -1){ 		 
        	beginDate = beginDate.replaceAll("/","");      		 
        }

        if(endDate.indexOf("/") != -1){
        	beginDate = endDate.replaceAll("/","");
        }        		 

        Calendar beginCalendar = Calendar.getInstance();
        Calendar endCalendar = Calendar.getInstance();
        Calendar tempCalendar = Calendar.getInstance();

        beginCalendar.set(Integer.parseInt(beginDate.substring(0,4)),Integer.parseInt(beginDate.substring(4,6)) - 1,Integer.parseInt(beginDate.substring(6,8)));
        endCalendar.set(Integer.parseInt(endDate.substring(0,4)),Integer.parseInt(endDate.substring(4,6)) - 1,Integer.parseInt(endDate.substring(6,8)));

        int beginYear = Integer.parseInt(beginDate.substring(0,4));
        int endYear = Integer.parseInt(endDate.substring(0,4));
             
        if(beginYear == endYear){
        	days = endCalendar.get(Calendar.DAY_OF_YEAR) - beginCalendar.get(Calendar.DAY_OF_YEAR);
        }
        else{  
        	tempCalendar.set(Integer.parseInt(beginDate.substring(0,4)),12 - 1,31);
        	days = tempCalendar.get(Calendar.DAY_OF_YEAR) - beginCalendar.get(Calendar.DAY_OF_YEAR);
        	for(beginYear ++;beginYear<endYear;beginYear++){  
        		tempCalendar.set(beginYear,12 - 1,31);
        		days += tempCalendar.get(Calendar.DAY_OF_YEAR);
        	}
        	days += endCalendar.get(Calendar.DAY_OF_YEAR);
        }
        return days;
    }
	/**
	 * 取UUID
	 */
	public String getUUID(){
		String uuid = UUID.randomUUID().toString(); 
		log.debug("uuid={}",uuid);
		return uuid;
	}
	/**
	 * 將基金電文取得之基金報酬揭露資訊，組成網頁顯示時所需的條文內容
	 * 
	 *  LINK_FUND_HOUSE	X	2	基金公司代號	&A
	 *	LINK_FUND_CODE	X	2	基金代號	&B
	 *	LINK_FEE_01	9	3.3	基金申購手續費費率	&3
	 *	LINK_FEE_02	9	3.3	基金申購手續費費率 銀行分成	&4
	 *	LINK_FEE_03	9	2.4	基金轉換手續費費率	&5
	 *	LINK_FEE_04	9	2.4	基金轉換手續費費率 銀行分成	&6
	 *	LINK_FEE_05	9	2.4	基金經理費年率	&7
	 *	LINK_FEE_07	9	2.4	基金經理費年率 銀行分成	&8
	 *	LINK_SLS_01	9	3.0	銷售獎勵金（一：年度）	&9
	 *	LINK_SLS_02	X	12	銷售獎勵金（一： 期間）	&10
	 *	LINK_SLS_03	9	2.4	銷售獎勵金（一： 費率）	&11
	 *	LINK_SLS_04	9	2.4	銷售獎勵金（二： 費率）	&12
	 *	LINK_SLS_05	9	3.0	銷售獎勵金（三：年度）	&13
	 *	LINK_SLS_06	X	12	銷售獎勵金（三： 期間）	&14
	 *	LINK_SLS_07	9	4.0	銷售獎勵金（三：金額）	&15
	 *	LINK_TRN_01	9	3	贊助金，訓練費：年度	&16
	 *	LINK_TRN_02	9	9. 0	贊助金，訓練費：金額	&17
	 *	LINK_OTHER_04	9	3.0	其他報酬一： 行銷贊助年度 	&18
	 *	LINK_OTHER_05	9	9. 0	其他報酬一： 行銷贊助金額	&19
	 *	LINK_SLS_08	9	2.4	銷售獎勵金（總費率）	&20
	 *	LINK_BASE_01	9	 9 .0	範例試算之基礎金額	&21
	 *	LINK_RESULT_01	9	 6 .0	試算出之申購手續費 前收	&22
	 *	LINK_RESULT_02	9	 6 .0	試算出之申購手續費 銀行	&23
	 *	LINK_RESULT_03	9	 6 .0	試算出之轉換費	&24
	 *	LINK_RESULT_04	9	 6 .0	試算出之轉換費分成	&25
	 *	LINK_RESULT_05	9	 6 .0	試算出之經理費分成	&26
	 *	LINK_RESULT_06	9	 6 .0	試算出之銷售獎勵金	&27
	 *	LINK_FEE_BSHARE	X	1	後收型基金識別碼	&28
	 *	LINK_FH_NAME	X	22	基金公司名稱	&1
	 *  LINK_FUND_NAME	X	22	基金名稱	&2
	 *  
	 */
	public Map<String,String> getFundFeeExposeInfo(Map<String,Object> bsData,String TXID){

		Map<String,String> fundFeeExposeTextMap = new HashMap<String,String>();
		
		String LINK_FUND_HOUSE = (String)bsData.get("LINK_FUND_HOUSE");
		String LINK_FUND_CODE = (String)bsData.get("LINK_FUND_CODE");
		String LINK_FEE_01 = formatNumberString((String)bsData.get("LINK_FEE_01"),3);
		String LINK_FEE_02 = formatNumberString((String)bsData.get("LINK_FEE_02"),3);
		Double dou_LINK_FEE_02_1 = Double.parseDouble(LINK_FEE_02);
		String LINK_FEE_03 = formatNumberString((String)bsData.get("LINK_FEE_03"),4);
		String LINK_FEE_04 = formatNumberString((String)bsData.get("LINK_FEE_04"),4);
		String LINK_FEE_05 = formatNumberString((String)bsData.get("LINK_FEE_05"),4);
		String LINK_FEE_07 = formatNumberString((String)bsData.get("LINK_FEE_07"),4);
		Double dou_LINK_FEE_07_1 = Double.parseDouble(LINK_FEE_07);
		String LINK_SLS_01 = formatNumberString((String)bsData.get("LINK_SLS_01"),0);
		String LINK_SLS_02 = (String)bsData.get("LINK_SLS_02");
		String LINK_SLS_03 = formatNumberString((String)bsData.get("LINK_SLS_03"),4);
		String LINK_SLS_04 = formatNumberString((String)bsData.get("LINK_SLS_04"),4);
		String LINK_SLS_05 = formatNumberString((String)bsData.get("LINK_SLS_05"),0);
		String LINK_SLS_06 = (String)bsData.get("LINK_SLS_06");
		String LINK_SLS_07 = formatNumberString((String)bsData.get("LINK_SLS_07"),0);
		String LINK_TRN_01 = formatNumberString((String)bsData.get("LINK_TRN_01"),0);
		String LINK_TRN_02 = formatNumberString((String)bsData.get("LINK_TRN_02"),0);
		String LINK_OTHER_04 = formatNumberString((String)bsData.get("LINK_OTHER_04"),0);
		String LINK_OTHER_05 = formatNumberString((String)bsData.get("LINK_OTHER_05"),0);
		String LINK_SLS_08 = formatNumberString((String)bsData.get("LINK_SLS_08"),4);
		String LINK_BASE_01 = formatNumberString((String)bsData.get("LINK_BASE_01"),0);
		String str_LINK_BASE_01_1 = "100000";
	    Double dou_LINK_BASE_01_1 = 100000.0;
		String LINK_RESULT_01 = formatNumberString((String)bsData.get("LINK_RESULT_01"),0);
		String LINK_RESULT_02 = formatNumberString((String)bsData.get("LINK_RESULT_02"),0);
		String str_LINK_RESULT_02_1 = "";
        Double dou_LINK_RESULT_02_1 = 0.0;
		String LINK_RESULT_03 = formatNumberString((String)bsData.get("LINK_RESULT_03"),0);
		String LINK_RESULT_04 = formatNumberString((String)bsData.get("LINK_RESULT_04"),0);
		String LINK_RESULT_05 = formatNumberString((String)bsData.get("LINK_RESULT_05"),0);
	    Double dou_LINK_RESULT_05_1=0.0;
	    String str_LINK_RESULT_05_1="";
		String LINK_RESULT_06 = formatNumberString((String)bsData.get("LINK_RESULT_06"),0);
		String LINK_FEE_BSHARE = (String)bsData.get("LINK_FEE_BSHARE");
		String LINK_FUND_TYPE = (String)bsData.get("LINK_FUND_TYPE");				
		String LINK_FH_NAME = (String)bsData.get("LINK_FH_NAME");
		String LINK_FUND_NAME = (String)bsData.get("LINK_FUND_NAME");
		String LINK_DATE = (String)bsData.get("LINK_DATE");			
		
		fundFeeExposeTextMap.put("LINK_FUND_HOUSE",LINK_FUND_HOUSE);
		fundFeeExposeTextMap.put("LINK_FUND_CODE",LINK_FUND_CODE);
		fundFeeExposeTextMap.put("LINK_FEE_01",LINK_FEE_01);
		fundFeeExposeTextMap.put("LINK_FEE_02",LINK_FEE_02);
		fundFeeExposeTextMap.put("LINK_FEE_03",LINK_FEE_03);
		fundFeeExposeTextMap.put("LINK_FEE_04",LINK_FEE_04);
		fundFeeExposeTextMap.put("LINK_FEE_05",LINK_FEE_05);
		fundFeeExposeTextMap.put("LINK_FEE_07",LINK_FEE_07);
		fundFeeExposeTextMap.put("LINK_SLS_01",LINK_SLS_01);
		fundFeeExposeTextMap.put("LINK_SLS_02",LINK_SLS_02);
		fundFeeExposeTextMap.put("LINK_SLS_03",LINK_SLS_03);
		fundFeeExposeTextMap.put("LINK_SLS_04",LINK_SLS_04);
		fundFeeExposeTextMap.put("LINK_SLS_05",LINK_SLS_05);
		fundFeeExposeTextMap.put("LINK_SLS_06",LINK_SLS_06);
		fundFeeExposeTextMap.put("LINK_SLS_07",LINK_SLS_07);
		fundFeeExposeTextMap.put("LINK_TRN_01",LINK_TRN_01);
		fundFeeExposeTextMap.put("LINK_TRN_02",LINK_TRN_02);
		fundFeeExposeTextMap.put("LINK_OTHER_04",LINK_OTHER_04);
		fundFeeExposeTextMap.put("LINK_OTHER_05",LINK_OTHER_05);
		fundFeeExposeTextMap.put("LINK_SLS_08",LINK_SLS_08);
		fundFeeExposeTextMap.put("LINK_BASE_01",LINK_BASE_01);
		fundFeeExposeTextMap.put("LINK_RESULT_01",LINK_RESULT_01);
		fundFeeExposeTextMap.put("LINK_RESULT_02",LINK_RESULT_02);
		fundFeeExposeTextMap.put("LINK_RESULT_03",LINK_RESULT_03);
		fundFeeExposeTextMap.put("LINK_RESULT_04",LINK_RESULT_04);
		fundFeeExposeTextMap.put("LINK_RESULT_05",LINK_RESULT_05);
		fundFeeExposeTextMap.put("LINK_RESULT_06",LINK_RESULT_06);
		fundFeeExposeTextMap.put("LINK_FEE_BSHARE",LINK_FEE_BSHARE);
		fundFeeExposeTextMap.put("LINK_FUND_TYPE",LINK_FUND_TYPE);
		fundFeeExposeTextMap.put("LINK_FH_NAME",LINK_FH_NAME);
		fundFeeExposeTextMap.put("LINK_FUND_NAME",LINK_FUND_NAME);
		fundFeeExposeTextMap.put("LINK_DATE",LINK_DATE);
		fundFeeExposeTextMap.put("LINK_BASE_01_1", str_LINK_BASE_01_1);
		try{
            dou_LINK_RESULT_02_1=dou_LINK_BASE_01_1*dou_LINK_FEE_02_1*0.01;
            int int_LINK_RESULT_02_1=(int)Math.round(dou_LINK_RESULT_02_1);
            str_LINK_RESULT_02_1=NumericUtil.formatNumberString(Integer.toString(int_LINK_RESULT_02_1), 0);
            fundFeeExposeTextMap.put("LINK_RESULT_02_1", str_LINK_RESULT_02_1);
        }catch(Exception e)
        {
        	log.error("getFundFeeExposeInfo error >> {}",e);
        }
		try{
	        dou_LINK_RESULT_05_1=dou_LINK_BASE_01_1*dou_LINK_FEE_07_1*0.01;
	        str_LINK_RESULT_05_1=NumericUtil.formatNumberString(dou_LINK_RESULT_05_1.toString(), 0);
	        fundFeeExposeTextMap.put("LINK_RESULT_05_1", str_LINK_RESULT_05_1);
	    }catch(Exception e)
	    {
	    	log.error("getFundFeeExposeInfo error >> {}",e);
	    }
		try{
            Calendar cal=Calendar.getInstance();
            cal.setTime(new Date());
            int int_year=cal.get(Calendar.YEAR);
            int_year=int_year-1911;
            String str_year=Integer.toString(int_year);
            fundFeeExposeTextMap.put("str_year", str_year);
        }catch(Exception e)
        {
        	log.error("str_year error >> {}",e);
        }
		//STR1
		if(TXID.equals("C021") || TXID.equals("C032")){			
			fundFeeExposeTextMap.put("STR1","轉入");
		}
		else{
			fundFeeExposeTextMap.put("STR1","銷售");
		}
		
		//STR2
		fundFeeExposeTextMap.put("STR2","1.台端支付的基金申購手續費為" + LINK_FEE_01 + "％，其中本銀行收取不多於" + LINK_FEE_02 + "％。(或不多於" + LINK_FEE_02 +"%)。<br>"+"2.台端支付的基金轉換手續費為" + LINK_FEE_03 + "%。");
//		if(LINK_FEE_BSHARE.equals("1") && (TXID.equals("C016") || TXID.equals("C017") || TXID.equals("C031"))){
//			fundFeeExposeTextMap.put("STR2","台端支付的基金申購手續費為" + LINK_FEE_01 + "％，其中本行收取不多於" + LINK_FEE_02 + "％。");
//		}
//		else if(LINK_FEE_BSHARE.equals("2") && (TXID.equals("C016") || TXID.equals("C017") || TXID.equals("C031"))){
//			fundFeeExposeTextMap.put("STR2","本基金手續費遞延至贖回時收取，申購時無需支付，本公司將自基金公司收取" + LINK_FEE_02 + "％。");
//		}
//		else if(TXID.equals("C021") || TXID.equals("C032")){
//			fundFeeExposeTextMap.put("STR2","台端支付的基金轉換手續費為" + LINK_FEE_03 + "％，其中本銀行收取不多於"+ LINK_FEE_04 +"％。");
//		}
//		else{
//			fundFeeExposeTextMap.put("STR2","");
//		}
		
		//STR3
		StringBuffer sb = new StringBuffer();
		if(Double.parseDouble(LINK_SLS_03) > 0){
			sb.append("<li>本行" + LINK_SLS_01 + "年" + LINK_SLS_02 + "精選基金活動期間，" + LINK_FH_NAME + "依本行銷售金額支付獎勵金不多於" + LINK_SLS_03 + "％。</li>");
		}
		
		if(Double.parseDouble(LINK_SLS_04) > 0){
			sb.append("<li>本基金募集期間，" + LINK_FH_NAME + "依本行銷售金額支付獎勵金不多於" + LINK_SLS_04 + "％。</li>");
		}
		
		if(Integer.parseInt((String)bsData.get("LINK_SLS_07")) > 0){
			sb.append("<li>本行" + LINK_SLS_05 + "年" + LINK_SLS_06 + "精選基金活動期間，" + LINK_FH_NAME + "依本行定期定額之銷售金額，新開戶一筆且達基金公司約定之成功扣款次數支付獎勵金不多於新台幣" + LINK_SLS_07 + "元。</li>");
		}
		
		if(Double.parseDouble(LINK_SLS_03) == 0 && Double.parseDouble(LINK_SLS_04) == 0 && Integer.parseInt((String)bsData.get("LINK_SLS_07")) == 0){
			sb.append("</ol>「未收取」。<ol>");
		}
		
		fundFeeExposeTextMap.put("STR3",sb.toString());
		
		//STR4
		if(Integer.parseInt((String)bsData.get("LINK_TRN_02")) >= 2000000){
			fundFeeExposeTextMap.put("STR4","本行" + LINK_TRN_01 + "年度銷售" + LINK_FH_NAME + "基金，預計可收取新台幣" + LINK_TRN_02 + "元之產品說明會及員工教育訓練贊助金");
		}
		else if(Integer.parseInt((String)bsData.get("LINK_TRN_02")) < 2000000 && Integer.parseInt((String)bsData.get("LINK_TRN_02")) >= 1){
			fundFeeExposeTextMap.put("STR4","「未達2百萬元揭露門檻」");
		}
		else if(Integer.parseInt((String)bsData.get("LINK_TRN_02")) == 0){
			fundFeeExposeTextMap.put("STR4", "「未收取」");
		}		
		else{
			fundFeeExposeTextMap.put("STR4","");
		}		
		
		//STR5
		if(Integer.parseInt((String)bsData.get("LINK_OTHER_05")) >= 1000000){
			fundFeeExposeTextMap.put("STR5","本銀行" + LINK_OTHER_04 + "年度銷售" + LINK_FH_NAME + "基金<br/>" + LINK_OTHER_04 + "年度行銷贊助、經銷費：本行全年自" + LINK_FH_NAME +"獲得行銷贊助及經銷費為新台幣" + LINK_OTHER_05 + "元"); 
		}
		else if(Integer.parseInt((String)bsData.get("LINK_OTHER_05")) > 0 && Integer.parseInt((String)bsData.get("LINK_OTHER_05")) < 1000000){
			fundFeeExposeTextMap.put("STR5", "未達1百萬元揭露門檻");
		}
		else{
			fundFeeExposeTextMap.put("STR5","「未收取」");
		}
		
		//STR6
		if(Integer.parseInt((String)bsData.get("LINK_TRN_02")) > 2000000){
			fundFeeExposeTextMap.put("STR6","，另本行" + LINK_TRN_01 + "年度銷售" + LINK_FH_NAME + "基金，該公司預計贊助產品說明會及員工教育訓練之金額合計為" + LINK_TRN_02 + "元");
		}
		else{
			fundFeeExposeTextMap.put("STR6","");
		}
						
		//STR7
		fundFeeExposeTextMap.put("STR7","由　台端所支付之" + str_LINK_RESULT_02_1 + "元申購手續費中收取不多於" + str_LINK_RESULT_02_1 + "元（" + str_LINK_BASE_01_1  + "＊" + dou_LINK_FEE_02_1  + "％＝" + str_LINK_RESULT_02_1  + "元）");
//		if(LINK_FEE_BSHARE.equals("1")){
//			fundFeeExposeTextMap.put("STR7","由　台端所支付之" + LINK_RESULT_01 + "元申購手續費中收取不多於" + LINK_RESULT_02 + "元（" + LINK_BASE_01 + "＊" + LINK_FEE_02 + "％＝" + LINK_RESULT_02 + "元）");
//		}
//		else if(LINK_FEE_BSHARE.equals("2")){
//			fundFeeExposeTextMap.put("STR7","本基金手續費遞延至贖回時收取，申購時無需支付，本公司將自基金公司收取" + LINK_RESULT_02 + "元（" + LINK_BASE_01 + "＊" + LINK_FEE_02 + "％＝" + LINK_RESULT_02 + "元）");
//		}
//		else{
//			fundFeeExposeTextMap.put("STR7","");
//		}		
						
		//STR8
		fundFeeExposeTextMap.put("STR8","(" + str_LINK_BASE_01_1 + "＊" + LINK_FEE_07 + "%=" + str_LINK_RESULT_05_1 + "元)");
//		if(Integer.parseInt((String)bsData.get("LINK_TRN_02")) > 2000000){
//			fundFeeExposeTextMap.put("STR8","（3）年度產品說明會及員工教育訓練贊助金： " + LINK_TRN_02 + "元");									
//		}
//		else{
//			fundFeeExposeTextMap.put("STR8","");
//		}		
		
		//STR9
		if(Integer.parseInt((String)bsData.get("LINK_OTHER_05")) >= 1000000){
			fundFeeExposeTextMap.put("STR9","行銷贊助：本行全年自" + LINK_FH_NAME + "獲得行銷贊助及經銷費為新台幣" + LINK_OTHER_05 + "元");
		}
		else{
			fundFeeExposeTextMap.put("STR9","");
		}		
		log.debug("fundFeeExposeTextMap={}",fundFeeExposeTextMap);
		
		return fundFeeExposeTextMap;
	}
	/**
	 * 更改基金EMAIL
	 */
	public BaseResult change_fundemail(Map<String,String> requestParam,Model model){
		BaseResult bs = null;
		try{
			bs = new BaseResult();
			bs = FundEmail_REST(requestParam);
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("change_fundemail error >> {}",e);
		}
		return bs;
	}
	/**
	 * 取得TXNUSER的LIST
	 */
	public List<TXNUSER> getTxnUserList(String CUSIDN){
		List<TXNUSER> txnUserList = txnUserDao.findByUserId(CUSIDN);
		log.debug(ESAPIUtil.vaildLog("txnUserList={}"+txnUserList));
		
		return txnUserList;
	}
	
	/**
	 * INSERT_TXNCUSINVATTRHIST_aj
	 */
	public BaseResult aj_insert_txncusinvattrhist(Map<String,String> params){
		BaseResult bs = null;
		
		TXNCUSINVATTRHIST attrhist = new TXNCUSINVATTRHIST();
		Date date = new Date();
		String currentDate = new SimpleDateFormat("yyyyMMdd").format(date);
		String currentTime = new SimpleDateFormat("HHmmss").format(date);
		
		if (((String) params.get("RTC")).substring(0, 1).equals("C")) {
			bs = getN922Data((String) params.get("CUSIDN"));
		} else {
			bs = getN927Data((String) params.get("CUSIDN"));
		}
		Map<String, Object> bsData = (Map) bs.getData();
		
		attrhist.setFDUSERID((String) params.get("CUSIDN"));
		attrhist.setFDQ1((String) params.get("FDQ1"));
		attrhist.setFDQ2((String) params.get("FDQ2"));
		attrhist.setFDQ3((String) params.get("FDQ3"));
		attrhist.setFDQ4((String) params.get("FDQ4"));
		attrhist.setFDQ5((String) params.get("FDQ5"));
		attrhist.setFDSCORE((String) params.get("FDSCORE"));
		attrhist.setFDINVTYPE((String) params.get("FDINVTYPE"));
		attrhist.setLASTUSER((String) bsData.get("APLBRH")); // 簽約行
		attrhist.setLASTDATE(currentDate);
		attrhist.setLASTTIME(currentTime);
		attrhist.setFDQ6((String) params.get("FDQ6"));
		attrhist.setFDQ7((String) params.get("FDQ7"));
		attrhist.setFDQ8((String) params.get("FDQ8"));
		attrhist.setFDQ9((String) params.get("FDQ9"));
		attrhist.setFDQ10((String) params.get("FDQ10"));
		attrhist.setFDQ11((String) params.get("FDQ11"));
		attrhist.setFDQ12((String) params.get("FDQ12"));
		attrhist.setFDQ13((String) params.get("FDQ13"));
		attrhist.setFDQ14((String) params.get("FDQ14"));
		attrhist.setADUSERIP((String) params.get("ADUSERIP"));
		attrhist.setAGREE((String) params.get("AGREE"));
		if (((String) params.get("CUSIDN")).length() == 10) {
			attrhist.setFDQ15((String) params.get("FDQ15"));
			attrhist.setMARK1((String) params.get("FDMARK1"));
		} else {
			attrhist.setFDQ15("");
			attrhist.setMARK1("");
		}

		try{
			bs.reset();
			bs = new BaseResult();
			int insert_result = txnCusInvAttrHistDao.saveLimit3Time(attrhist);
			if(insert_result <= 0) {
				log.debug("insert_result >> {}", insert_result);
				bs = new BaseResult();
				bs.setResult(false);
				bs.setMsgCode(ResultCode.SYS_KYC_ERROR_3TIME);
				bs.setMessage(i18n.getMsg("LB.Alert093"));
				return bs;
			}
			List<TXNCUSINVATTRHIST> lastestPo = txnCusInvAttrHistDao.findByIDGetLastestData(params.get("CUSIDN"));
//			bs.setData(lastestPo.get(0).getFDHISTID());
			Map<String,Object> rtndata = new HashMap<String,Object>();
			rtndata.put("FDHISTID", lastestPo.get(0).getFDHISTID());
			bs.setData(rtndata);
			bs.setResult(true);
		}catch (Exception e) {
			bs.setResult(false);
			bs.setMsgCode("M350");
			getMessageByMsgCode(bs);
		}		
		
		return bs;
	}
	
	
	public BaseResult getKycScore(Map<String,String> params){
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = InvAttribS_REST(params);
		}catch (Exception e) {
			// TODO: handle exception
			log.error("getKycScore error >> {}",e);
		}
		
		return bs;
	}
}