package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * 定期投資約定變更
 */
public class C111_REST_RS extends BaseRestBean implements Serializable
{
	private static final long serialVersionUID = 6791228050721094802L;

	private String SSUP;// System Supervisory

	private String SNID;// System Network Identifier

	private String ACF;// Address Control Field

	private String MSGTYPE;// Message Type

	private String PROCCODE;// Processing Code

	private String SYSTRACE;// System Trace Audit

	private String TXNDIID;// TXN Destination Institute ID

	private String TXNSIID;// TXN Source Institute ID

	private String TXNIDT;// TXN Initiate Date and Time

	private String RESPCOD;// Response Code

	private String SYNCCHK;// Sync. Check Item

	private String BITMAPCFG;// Bit Map Configuration

	private String CMRECNUM;

	private String TIME;

	private String DATE;

	private String CMQTIME;

	private String __OCCURS;

	private String NEXT;// 資料起始位置

	private String RECNO;// 讀取筆數

	private String CUSIDN;// 統一編號

	private String ALTERTYPE;// 變更種類

	private String LOOPINFO;// LOOPINFO

	private String FILLER_X3;// FILLER_X3

	private String TOTRECNO;// 總筆數

	private LinkedList<C111_REST_RSDATA> REC;

	private String SHWD;//視窗註記 2020/07/02新增
	
	public String getSSUP()
	{
		return SSUP;
	}

	public void setSSUP(String sSUP)
	{
		SSUP = sSUP;
	}

	public String getSNID()
	{
		return SNID;
	}

	public void setSNID(String sNID)
	{
		SNID = sNID;
	}

	public String getACF()
	{
		return ACF;
	}

	public void setACF(String aCF)
	{
		ACF = aCF;
	}

	public String getMSGTYPE()
	{
		return MSGTYPE;
	}

	public void setMSGTYPE(String mSGTYPE)
	{
		MSGTYPE = mSGTYPE;
	}

	public String getPROCCODE()
	{
		return PROCCODE;
	}

	public void setPROCCODE(String pROCCODE)
	{
		PROCCODE = pROCCODE;
	}

	public String getSYSTRACE()
	{
		return SYSTRACE;
	}

	public void setSYSTRACE(String sYSTRACE)
	{
		SYSTRACE = sYSTRACE;
	}

	public String getTXNDIID()
	{
		return TXNDIID;
	}

	public void setTXNDIID(String tXNDIID)
	{
		TXNDIID = tXNDIID;
	}

	public String getTXNSIID()
	{
		return TXNSIID;
	}

	public void setTXNSIID(String tXNSIID)
	{
		TXNSIID = tXNSIID;
	}

	public String getTXNIDT()
	{
		return TXNIDT;
	}

	public void setTXNIDT(String tXNIDT)
	{
		TXNIDT = tXNIDT;
	}

	public String getRESPCOD()
	{
		return RESPCOD;
	}

	public void setRESPCOD(String rESPCOD)
	{
		RESPCOD = rESPCOD;
	}

	public String getSYNCCHK()
	{
		return SYNCCHK;
	}

	public void setSYNCCHK(String sYNCCHK)
	{
		SYNCCHK = sYNCCHK;
	}

	public String getBITMAPCFG()
	{
		return BITMAPCFG;
	}

	public void setBITMAPCFG(String bITMAPCFG)
	{
		BITMAPCFG = bITMAPCFG;
	}

	public String getCMRECNUM()
	{
		return CMRECNUM;
	}

	public void setCMRECNUM(String cMRECNUM)
	{
		CMRECNUM = cMRECNUM;
	}

	public String getTIME()
	{
		return TIME;
	}

	public void setTIME(String tIME)
	{
		TIME = tIME;
	}

	public String getDATE()
	{
		return DATE;
	}

	public void setDATE(String dATE)
	{
		DATE = dATE;
	}

	public String getCMQTIME()
	{
		return CMQTIME;
	}

	public void setCMQTIME(String cMQTIME)
	{
		CMQTIME = cMQTIME;
	}

	public String get__OCCURS()
	{
		return __OCCURS;
	}

	public void set__OCCURS(String __OCCURS)
	{
		this.__OCCURS = __OCCURS;
	}

	public String getNEXT()
	{
		return NEXT;
	}

	public void setNEXT(String nEXT)
	{
		NEXT = nEXT;
	}

	public String getRECNO()
	{
		return RECNO;
	}

	public void setRECNO(String rECNO)
	{
		RECNO = rECNO;
	}

	public String getCUSIDN()
	{
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN)
	{
		CUSIDN = cUSIDN;
	}


	public String getALTERTYPE()
	{
		return ALTERTYPE;
	}

	public void setALTERTYPE(String aLTERTYPE)
	{
		ALTERTYPE = aLTERTYPE;
	}

	public String getLOOPINFO()
	{
		return LOOPINFO;
	}

	public void setLOOPINFO(String lOOPINFO)
	{
		LOOPINFO = lOOPINFO;
	}

	public String getFILLER_X3()
	{
		return FILLER_X3;
	}

	public void setFILLER_X3(String fILLER_X3)
	{
		FILLER_X3 = fILLER_X3;
	}

	public String getTOTRECNO()
	{
		return TOTRECNO;
	}

	public void setTOTRECNO(String tOTRECNO)
	{
		TOTRECNO = tOTRECNO;
	}

	public LinkedList<C111_REST_RSDATA> getREC()
	{
		return REC;
	}

	public void setREC(LinkedList<C111_REST_RSDATA> rEC)
	{
		REC = rEC;
	}

	public String getSHWD() {
		return SHWD;
	}

	public void setSHWD(String sHWD) {
		SHWD = sHWD;
	}
	
	

}