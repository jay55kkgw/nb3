package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N320_REST_RSDATA_CRY extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7445173263967145749L;

	private String AMTLNCCY;
	private String NTDBALLNCCY;
	private String FXTOTAMT;
	private String FXTOTNTDBAL;
	
	public String getAMTLNCCY() {
		return AMTLNCCY;
	}
	public void setAMTLNCCY(String aMTLNCCY) {
		AMTLNCCY = aMTLNCCY;
	}
	public String getNTDBALLNCCY() {
		return NTDBALLNCCY;
	}
	public void setNTDBALLNCCY(String nTDBALLNCCY) {
		NTDBALLNCCY = nTDBALLNCCY;
	}
	public String getFXTOTAMT() {
		return FXTOTAMT;
	}
	public void setFXTOTAMT(String fXTOTAMT) {
		FXTOTAMT = fXTOTAMT;
	}
	public String getFXTOTNTDBAL() {
		return FXTOTNTDBAL;
	}
	public void setFXTOTNTDBAL(String fXTOTNTDBAL) {
		FXTOTNTDBAL = fXTOTNTDBAL;
	}
	
	
}
