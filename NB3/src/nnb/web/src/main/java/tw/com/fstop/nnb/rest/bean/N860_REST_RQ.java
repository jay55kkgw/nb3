package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N860_REST_RQ  extends BaseRestBean_TW implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -706116422066509715L;
	
	private String ACN; //帳號
	private String CUSIDN; //身分證字號
	private String DPBHNO; //代收行
	private String FGBRHCOD; //查詢項目
	private String FGTYPE; //查詢類別
	private String FGPERIOD; //查詢項目
	
	private String CMSDATE2; //開始日期
	private String CMEDATE2; //結束日期
	private String CMSDATE3; //開始日期
	private String CMEDATE3; //結束日期
	private String CMSDATE4; //開始日期
	private String CMEDATE4; //結束日期
	
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getDPBHNO() {
		return DPBHNO;
	}
	public void setDPBHNO(String dPBHNO) {
		DPBHNO = dPBHNO;
	}
	public String getFGBRHCOD() {
		return FGBRHCOD;
	}
	public void setFGBRHCOD(String fGBRHCOD) {
		FGBRHCOD = fGBRHCOD;
	}
	public String getFGTYPE() {
		return FGTYPE;
	}
	public void setFGTYPE(String fGTYPE) {
		FGTYPE = fGTYPE;
	}
	public String getFGPERIOD() {
		return FGPERIOD;
	}
	public void setFGPERIOD(String fGPERIOD) {
		FGPERIOD = fGPERIOD;
	}
	public String getCMSDATE2() {
		return CMSDATE2;
	}
	public void setCMSDATE2(String cMSDATE2) {
		CMSDATE2 = cMSDATE2;
	}
	public String getCMEDATE2() {
		return CMEDATE2;
	}
	public void setCMEDATE2(String cMEDATE2) {
		CMEDATE2 = cMEDATE2;
	}
	public String getCMSDATE3() {
		return CMSDATE3;
	}
	public void setCMSDATE3(String cMSDATE3) {
		CMSDATE3 = cMSDATE3;
	}
	public String getCMEDATE3() {
		return CMEDATE3;
	}
	public void setCMEDATE3(String cMEDATE3) {
		CMEDATE3 = cMEDATE3;
	}
	public String getCMSDATE4() {
		return CMSDATE4;
	}
	public void setCMSDATE4(String cMSDATE4) {
		CMSDATE4 = cMSDATE4;
	}
	public String getCMEDATE4() {
		return CMEDATE4;
	}
	public void setCMEDATE4(String cMEDATE4) {
		CMEDATE4 = cMEDATE4;
	}
}
