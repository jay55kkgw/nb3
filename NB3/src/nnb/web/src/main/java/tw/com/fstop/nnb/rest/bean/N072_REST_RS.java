package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N072_REST_RS extends BaseRestBean implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 775129110323679813L;
	
	//transaction
	private String TRADAY;
	private String TRADATE;
	private String AMOUNT;
	private String CARDNUM;
	private String O_AVLBAL;
	private String OUTACN;
	private String O_TOTBAL;
	public String getTRADAY() {
		return TRADAY;
	}
	public String getTRADATE() {
		return TRADATE;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public String getCARDNUM() {
		return CARDNUM;
	}
	public String getO_AVLBAL() {
		return O_AVLBAL;
	}
	public String getOUTACN() {
		return OUTACN;
	}
	public String getO_TOTBAL() {
		return O_TOTBAL;
	}
	public void setTRADAY(String tRADAY) {
		TRADAY = tRADAY;
	}
	public void setTRADATE(String tRADATE) {
		TRADATE = tRADATE;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	public void setCARDNUM(String cARDNUM) {
		CARDNUM = cARDNUM;
	}
	public void setO_AVLBAL(String o_AVLBAL) {
		O_AVLBAL = o_AVLBAL;
	}
	public void setOUTACN(String oUTACN) {
		OUTACN = oUTACN;
	}
	public void setO_TOTBAL(String o_TOTBAL) {
		O_TOTBAL = o_TOTBAL;
	}
}
