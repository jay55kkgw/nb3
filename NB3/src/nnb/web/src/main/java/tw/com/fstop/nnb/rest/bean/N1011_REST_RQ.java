package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N1011_REST_RQ extends BaseRestBean_OLS implements Serializable {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5408679473710633441L;
	
	private	String CUSIDN;	
	private	String ACN;
	private	String TYPE; //查詢類別
	private	String NAME; //申請人
	private	String ADDRESS;//地址
	private	String BRHDAY;//生日
	private	String BUSINESS;//營業種類或職業
	private	String BUSIDN;//營利事業統一編號
	private	String COUNTRY;//國別
	private	String BIRTHPLA;//出生地
	private	String TEL_O;//電話(公)
	private	String TEL_H;//電話(私)
	private	String EDUSRID;//使用者代號
	private	String PINNEW;
	private	String FGTXWAY;
	private	String CUSNAME; //原住民姓名
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getADDRESS() {
		return ADDRESS;
	}
	public void setADDRESS(String aDDRESS) {
		ADDRESS = aDDRESS;
	}
	public String getBRHDAY() {
		return BRHDAY;
	}
	public void setBRHDAY(String bRHDAY) {
		BRHDAY = bRHDAY;
	}
	public String getBUSINESS() {
		return BUSINESS;
	}
	public void setBUSINESS(String bUSINESS) {
		BUSINESS = bUSINESS;
	}
	public String getBUSIDN() {
		return BUSIDN;
	}
	public void setBUSIDN(String bUSIDN) {
		BUSIDN = bUSIDN;
	}
	public String getCOUNTRY() {
		return COUNTRY;
	}
	public void setCOUNTRY(String cOUNTRY) {
		COUNTRY = cOUNTRY;
	}
	public String getBIRTHPLA() {
		return BIRTHPLA;
	}
	public void setBIRTHPLA(String bIRTHPLA) {
		BIRTHPLA = bIRTHPLA;
	}
	public String getTEL_O() {
		return TEL_O;
	}
	public void setTEL_O(String tEL_O) {
		TEL_O = tEL_O;
	}
	public String getTEL_H() {
		return TEL_H;
	}
	public void setTEL_H(String tEL_H) {
		TEL_H = tEL_H;
	}
	public String getEDUSRID() {
		return EDUSRID;
	}
	public void setEDUSRID(String eDUSRID) {
		EDUSRID = eDUSRID;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getCUSNAME() {
		return CUSNAME;
	}
	public void setCUSNAME(String cUSNAME) {
		CUSNAME = cUSNAME;
	}
}
