package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N567_REST_RSDATA extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1634901758454705664L;

	private String RREFNO;// 匯入匯款編號
	private String RCREDATE;// 申請託收日
	private String RCHQNO;// 票據編號
	private String RBILLCCY;// 幣別
	private String RBILLAMT;// 到單金額
	private String RRETAMT;// 國外入帳金額
	private String RVALDATE;// 結案日期
	private String RSTATE;// 狀態
	private String CUSIDN;// 統一編號

	public String getRBILLAMT() {
		return RBILLAMT;
	}

	public void setRBILLAMT(String rBILLAMT) {
		RBILLAMT = rBILLAMT;
	}

	public String getRREFNO() {
		return RREFNO;
	}

	public void setRREFNO(String rREFNO) {
		RREFNO = rREFNO;
	}

	public String getRCHQNO() {
		return RCHQNO;
	}

	public void setRCHQNO(String rCHQNO) {
		RCHQNO = rCHQNO;
	}

	public String getRRETAMT() {
		return RRETAMT;
	}

	public void setRRETAMT(String rRETAMT) {
		RRETAMT = rRETAMT;
	}

	public String getRCREDATE() {
		return RCREDATE;
	}

	public void setRCREDATE(String rCREDATE) {
		RCREDATE = rCREDATE;
	}

	public String getRSTATE() {
		return RSTATE;
	}

	public void setRSTATE(String rSTATE) {
		RSTATE = rSTATE;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getRVALDATE() {
		return RVALDATE;
	}

	public void setRVALDATE(String rVALDATE) {
		RVALDATE = rVALDATE;
	}

	public String getRBILLCCY() {
		return RBILLCCY;
	}

	public void setRBILLCCY(String rBILLCCY) {
		RBILLCCY = rBILLCCY;
	}

}
