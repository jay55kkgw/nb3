package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

public class N077_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1301326804721580597L;
	/**
	 * 
	 */

	@SerializedName(value = "ACN")
	private String ACN;
	@SerializedName(value = "FDPNUM")
	private String FDPNUM;
	@SerializedName(value = "AMTFDP")
	private String AMTFDP;
	@SerializedName(value = "TYPENAME")
	private String TYPENAME;
	@SerializedName(value = "DPINTTYPE")
	private String DPINTTYPE;
	@SerializedName(value = "FGRENNAME")
	private String FGRENNAME;

	@Override
	public Map<String, String> coverMap() {
		LinkedHashMap<String,String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", "臺幣定存自動轉期申請/變更");
		result.put("交易類型", "-");
		result.put("存單帳號", this.ACN);
		result.put("存單號碼", this.FDPNUM);
		result.put("存單金額", "新台幣" + this.AMTFDP + "元");
		result.put("存單種類", this.TYPENAME);
		result.put("計息方式", this.DPINTTYPE);

		result.put("轉期次數", this.FGRENNAME);
		return result;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	public String getFDPNUM() {
		return FDPNUM;
	}

	public void setFDPNUM(String fDPNUM) {
		FDPNUM = fDPNUM;
	}

	public String getAMTFDP() {
		return AMTFDP;
	}

	public void setAMTFDP(String aMTFDP) {
		AMTFDP = aMTFDP;
	}

	public String getTYPENAME() {
		return TYPENAME;
	}

	public void setTYPENAME(String tYPENAME) {
		TYPENAME = tYPENAME;
	}

	public String getDPINTTYPE() {
		return DPINTTYPE;
	}

	public void setDPINTTYPE(String dPINTTYPE) {
		DPINTTYPE = dPINTTYPE;
	}

	public String getFGRENNAME() {
		return FGRENNAME;
	}

	public void setFGRENNAME(String fGRENNAME) {
		FGRENNAME = fGRENNAME;
	}

}
