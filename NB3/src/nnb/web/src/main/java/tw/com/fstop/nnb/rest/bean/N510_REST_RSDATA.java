package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N510_REST_RSDATA extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7445173263967145749L;

	String NTDBAL;
	String AVAILABLE;
	String BALANCE;
	String ACN;
	String RESERVE;
	String CUID;
	String COUNT;


	public String getNTDBAL() {
		return NTDBAL;
	}
	public void setNTDBAL(String nTDBAL) {
		NTDBAL = nTDBAL;
	}
	public String getAVAILABLE() {
		return AVAILABLE;
	}
	public void setAVAILABLE(String aVAILABLE) {
		AVAILABLE = aVAILABLE;
	}
	public String getBALANCE() {
		return BALANCE;
	}
	public void setBALANCE(String bALANCE) {
		BALANCE = bALANCE;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getRESERVE() {
		return RESERVE;
	}
	public void setRESERVE(String rESERVE) {
		RESERVE = rESERVE;
	}
	public String getCUID() {
		return CUID;
	}
	public void setCUID(String cUID) {
		CUID = cUID;
	}
	public String getCOUNT() {
		return COUNT;
	}
	public void setCOUNT(String cOUNT) {
		COUNT = cOUNT;
	}
}
