package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N215M_1_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5128341408169869526L;

	private String SelectedRecord;
	private String COUNT;
	public String getSelectedRecord() {
		return SelectedRecord;
	}
	public String getCOUNT() {
		return COUNT;
	}
	public void setSelectedRecord(String selectedRecord) {
		SelectedRecord = selectedRecord;
	}
	public void setCOUNT(String cOUNT) {
		COUNT = cOUNT;
	}
	
}
