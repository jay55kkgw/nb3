package tw.com.fstop.nnb.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.po.TXNUSER;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.tbb.nnb.dao.TxnUserDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SpringBeanFactory;

@Service
public class Send_Mail_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private TxnUserDao txnUserDao;
	
	
	/* 取得TXNUSER用戶email */
	public BaseResult getTxnUser(String cusidn) {
		log.trace("getTxnUser...");
		BaseResult bs = null;

		try {
			Map<String,Object> resultMap = new HashMap<String,Object>();
			
			bs = new BaseResult();
			// TXNUSER
			TXNUSER txnUser = txnUserDao.getNotifyById(cusidn);
			if(txnUser!=null){
				resultMap.put("DPMYEMAIL",  txnUser.getDPMYEMAIL() !=null ? txnUser.getDPMYEMAIL() : "");
				log.trace(ESAPIUtil.vaildLog("loginInit.resultMap: {}"+ resultMap));
				
				bs.setMsgCode("0");
				bs.setMessage("success");
				bs.setResult(true);
				bs.setData(resultMap);
			}
			
		} catch (Exception e) {
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * SEND MAIL
	 * Subject 信件標題
	 * Content 內容
	 * Receivers 收信者
	 */
	public BaseResult SendMail(String cusidn, String type, boolean result, List<String> Receivers) {
		
		String mail_subject_env = SpringBeanFactory.getBean("MAIL_SUBJECT_ENV");
		
		log.trace("SendMail.Receivers: {}", Receivers);
		
		String subject = "";
		String content = "";
		
		String cidn = cusidn!=null&&cusidn.length()==8 ? cusidn.substring(0, 4) + "***" : cusidn; // 企業戶
		cidn = cusidn!=null&&cusidn.length()==10 ? cusidn.substring(0, 4) + "xxx" + cusidn.substring(7, 10) : cusidn; // 一般用戶
		
		// 網銀登入通知
		if("login".equals(type)) {
			// 登入成功
			if(result) {
				StringBuffer sb = new StringBuffer();
				sb.append("<table cellSpacing=0 cellPadding=0 width=600 border=0 frame=border >").append("\n");
		        sb.append("<tbody>").append("\n");
		        
		        sb.append("<tr><td align=left>");
		        
//		        sb.append("親愛的客戶您好：<p>");
//		        sb.append("本行依據轉出人的交易指示，發送下列通知，本通知僅供參考，不作為交易憑證，實際交易結果依本行系統為準！<BR><BR>");
		        sb.append("<br>");
		        sb.append("您以身分證字號：" + cidn + "<br>");
		        sb.append("於" + DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss") + "至本行新世代網路銀行登入成功。<br>");
		        sb.append("煩請先確認您是否於此期間進行簽入作業，若您並未執行相關動作，請立即與本行聯絡，以確保您的權益。<br>");
		        sb.append("臺灣企銀貼心提醒您！<p>");
		        sb.append("臺灣企銀新世代網路銀行 敬上<p>");
		        sb.append("※本電子郵件之內容及其附件係由臺灣企銀所傳送，");
		        sb.append("所含之資料僅供指定之收件人使用。除了本電子郵件所指定之收件人外，");
		        sb.append("任何人或公司不得就本電子郵件全部或部份之內容為閱讀、複製、轉寄、公開、保存或為其他使用。<br>");
		        sb.append("※臺灣企銀不會主動發信要求您填寫使用者名稱或是簽入密碼，");
		        sb.append("特別提醒您不要將您的個人隱私資料或密碼回覆給任何人，亦請勿直接回覆此信。<br<br>");
		        
		        sb.append("</td></tr>");
		        
		        sb.append("</tbody>").append("\n");
		        sb.append("</table>").append("\n");
		        
		        content = sb.toString();
		        
				switch (mail_subject_env) {
				case "D":
					subject = "[開發環境] 臺灣企銀 新世代網路銀行 登入成功通知";
					break;
				case "T":
					subject = "[測試環境] 臺灣企銀 新世代網路銀行 登入成功通知";
					break;
				case "P":
					subject = "臺灣企銀 新世代網路銀行 登入成功通知";
					break;
				}
		        
		        
			} // 登入失敗
			else {
				StringBuffer sb = new StringBuffer();
				sb.append("<table cellSpacing=0 cellPadding=0 width=600 border=0 frame=border >").append("\n");
		        sb.append("<tbody>").append("\n");
		        
		        sb.append("<tr><td align=left>");
		        
//		        sb.append("親愛的客戶您好：<p>");
//		        sb.append("本行依據轉出人的交易指示，發送下列通知，本通知僅供參考，不作為交易憑證，實際交易結果依本行系統為準！<BR><BR>");
		        sb.append("<br>");
		        sb.append("您以身分證字號：" + cidn + "<br>");
		        sb.append("於" + DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss") + "至本行新世代網路銀行輸入【簽入密碼】（或【使用者名稱】）錯誤、登入失敗。<br>");
		        sb.append("煩請先確認您是否於此期間進行簽入作業，若您並未執行相關動作，請立即與本行聯絡，以確保您的權益。<br>");
		        sb.append("臺灣企銀貼心提醒您！<p>");
		        sb.append("臺灣企銀新世代網路銀行 敬上<p>");
		        sb.append("※本電子郵件之內容及其附件係由臺灣企銀所傳送，");
		        sb.append("所含之資料僅供指定之收件人使用。除了本電子郵件所指定之收件人外，");
		        sb.append("任何人或公司不得就本電子郵件全部或部份之內容為閱讀、複製、轉寄、公開、保存或為其他使用。<br>");
		        sb.append("※臺灣企銀不會主動發信要求您填寫使用者名稱或是簽入密碼，");
		        sb.append("特別提醒您不要將您的個人隱私資料或密碼回覆給任何人，亦請勿直接回覆此信。<br><br>");
		        
		        sb.append("</td></tr>");
		        
		        sb.append("</tbody>").append("\n");
		        sb.append("</table>").append("\n");
		        
		        content = sb.toString();
		        
		        switch (mail_subject_env) {
				case "D":
					subject = "[開發環境] 臺灣企銀 新世代網路銀行 登入失敗通知";
					break;
				case "T":
					subject = "[測試環境] 臺灣企銀 新世代網路銀行 登入失敗通知";
					break;
				case "P":
					subject = "臺灣企銀 新世代網路銀行 登入失敗通知";
					break;
				}
			}
			
		}
		
		BaseResult bs = SendAdMail_REST(subject, content, Receivers,null);
		log.debug("SendMail.bs: {}", CodeUtil.toJson(bs));
		return bs;
	}
	
}
