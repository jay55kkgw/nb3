package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class OLA_OTP_REST_RQ extends BaseRestBean_OLA implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3742888336148523910L;
	private String UID;
	private String CUSIDN;
	private String FuncType;
	private String OTP;
	private String MOBTEL;
	//取得簡訊密碼時，不寫TXNLOG所以改SMSADOPID
	private String SMSADOPID;
	private String MSGTYPE;
	
	
	public String getMSGTYPE() {
		return MSGTYPE;
	}
	public void setMSGTYPE(String mSGTYPE) {
		MSGTYPE = mSGTYPE;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getFuncType() {
		return FuncType;
	}
	public void setFuncType(String funcType) {
		FuncType = funcType;
	}
	public String getOTP() {
		return OTP;
	}
	public void setOTP(String oTP) {
		OTP = oTP;
	}
	public String getMOBTEL() {
		return MOBTEL;
	}
	public void setMOBTEL(String mOBTEL) {
		MOBTEL = mOBTEL;
	}
	public String getSMSADOPID() {
		return SMSADOPID;
	}
	public void setSMSADOPID(String sMSADOPID) {
		SMSADOPID = sMSADOPID;
	}
}
