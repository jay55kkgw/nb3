package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class F002R_REST_RS extends BaseRestBean implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4903829604679503064L;
	
	private String BGROENO; //議價編號
	
	private String RATE;//匯率
	
	private String CURAMT;  // 轉出金額
	
	private String ATRAMT; // 轉入金額

	public String getBGROENO()
	{
		return BGROENO;
	}

	public void setBGROENO(String bGROENO)
	{
		BGROENO = bGROENO;
	}

	public String getRATE()
	{
		return RATE;
	}

	public void setRATE(String rATE)
	{
		RATE = rATE;
	}

	public String getCURAMT()
	{
		return CURAMT;
	}

	public void setCURAMT(String cURAMT)
	{
		CURAMT = cURAMT;
	}

	public String getATRAMT()
	{
		return ATRAMT;
	}

	public void setATRAMT(String aTRAMT)
	{
		ATRAMT = aTRAMT;
	}
	
	
	
	

}
