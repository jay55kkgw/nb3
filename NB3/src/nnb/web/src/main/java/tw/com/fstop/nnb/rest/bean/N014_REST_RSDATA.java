package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N014_REST_RSDATA implements Serializable {

	private static final long serialVersionUID = -2739572910855200803L;
	private  String ITRLTD;//
	private String RAT;//
	private String ITR;//
	//修改 Serializable Class Containing Sensitive Data
	private String ITRUS;//
	private String FILL9;//
	private String ACN;//
	private String BAL;//
	private String AMTAPY;//
	private String AMTORLN;//
	private String DATFSLN;//
	private String DDT;//
	private String SEQ;//
	private String SEQ2;//
	private String AWT;//
	private String DATITPY;//
	
	public String getITRLTD() {
		return ITRLTD;
	}
	public void setITRLTD(String iTRLTD) {
		ITRLTD = iTRLTD;
	}
	public String getRAT() {
		return RAT;
	}
	public void setRAT(String rAT) {
		RAT = rAT;
	}
	public String getITR() {
		return ITR;
	}
	public void setITR(String iTR) {
		ITR = iTR;
	}
	public String getITRUS() {
		return ITRUS;
	}
	public void setITRUS(String iTRUS) {
		ITRUS = iTRUS;
	}
	public String getFILL9() {
		return FILL9;
	}
	public void setFILL9(String fILL9) {
		FILL9 = fILL9;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getBAL() {
		return BAL;
	}
	public void setBAL(String bAL) {
		BAL = bAL;
	}
	public String getAMTAPY() {
		return AMTAPY;
	}
	public void setAMTAPY(String aMTAPY) {
		AMTAPY = aMTAPY;
	}
	public String getAMTORLN() {
		return AMTORLN;
	}
	public void setAMTORLN(String aMTORLN) {
		AMTORLN = aMTORLN;
	}
	public String getDATFSLN() {
		return DATFSLN;
	}
	public void setDATFSLN(String dATFSLN) {
		DATFSLN = dATFSLN;
	}
	public String getDDT() {
		return DDT;
	}
	public void setDDT(String dDT) {
		DDT = dDT;
	}
	public String getSEQ() {
		return SEQ;
	}
	public void setSEQ(String sEQ) {
		SEQ = sEQ;
	}
	public String getSEQ2() {
		return SEQ2;
	}
	public void setSEQ2(String sEQ2) {
		SEQ2 = sEQ2;
	}
	public String getAWT() {
		return AWT;
	}
	public void setAWT(String aWT) {
		AWT = aWT;
	}
	public String getDATITPY() {
		return DATITPY;
	}
	public void setDATITPY(String dATITPY) {
		DATITPY = dATITPY;
	}
	
}
