package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N857_REST_RS extends BaseRestBean_OLA implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String MSGNAME;	
	private String DFH;
	private String OFFSET;
	private String HEADER;
	private String MSGCOD;
	
	public String getMSGNAME() {
		return MSGNAME;
	}
	public void setMSGNAME(String mSGNAME) {
		MSGNAME = mSGNAME;
	}
	public String getDFH() {
		return DFH;
	}
	public void setDFH(String dFH) {
		DFH = dFH;
	}
	public String getOFFSET() {
		return OFFSET;
	}
	public void setOFFSET(String oFFSET) {
		OFFSET = oFFSET;
	}
	public String getHEADER() {
		return HEADER;
	}
	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}
	public String getMSGCOD() {
		return MSGCOD;
	}
	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}
	
}
