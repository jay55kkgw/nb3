package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * C024電文RQ
 */
public class C024_REST_RQ extends BaseRestBean_FUND implements Serializable{
	private static final long serialVersionUID = 6868861910004819383L;
	
	private String NEXT;
	private String RECNO;
	private String CUSIDN;
	private String MAC;
	//for txnlog
	private String ADOPID = "C024_0";
	
	public String getNEXT(){
		return NEXT;
	}
	public void setNEXT(String nEXT){
		NEXT = nEXT;
	}
	public String getRECNO(){
		return RECNO;
	}
	public void setRECNO(String rECNO){
		RECNO = rECNO;
	}
	public String getCUSIDN(){
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN){
		CUSIDN = cUSIDN;
	}
	public String getMAC(){
		return MAC;
	}
	public void setMAC(String mAC){
		MAC = mAC;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
}