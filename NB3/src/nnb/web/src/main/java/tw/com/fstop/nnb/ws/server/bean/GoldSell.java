package tw.com.fstop.nnb.ws.server.bean;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

//@XmlRootElement(namespace = "http://ws.fstop", name="informGoldValueResponse")
@XmlRootElement(namespace = "http://ws.fstop", name="GoldSell")
@XmlAccessorType(XmlAccessType.FIELD)
public class GoldSell {

	@XmlElement(namespace = "http://ws.fstop")
	public String msg_cod;
	
    @XmlElement(namespace = "http://ws.fstop")
	public String msg_desc;
	
    @XmlElement(namespace = "http://ws.fstop")
	public String date;
	
    @XmlElement(namespace = "http://ws.fstop")
	public String time;
	
    @XmlElement(namespace = "http://ws.fstop")
	public String bank_no;
	
    @XmlElementWrapper(namespace = "http://ws.fstop" ,name="goldSellProduct")
	@XmlElement(name="GoldSellProduct")
	public List<GoldSellProduct> goldSellProduct;
//    public GoldSellProduct[] goldSellProduct;

	public String getMsg_cod() {
		return msg_cod;
	}

	public void setMsg_cod(String msg_cod) {
		this.msg_cod = msg_cod;
	}

	public String getMsg_desc() {
		return msg_desc;
	}

	public void setMsg_desc(String msg_desc) {
		this.msg_desc = msg_desc;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getBank_no() {
		return bank_no;
	}

	public void setBank_no(String bank_no) {
		this.bank_no = bank_no;
	}

//	public GoldSellProduct[] getGoldSellProduct() {
//		return goldSellProduct;
//	}
//
//	public void setGoldSellProduct(GoldSellProduct[] goldSellProduct) {
//		this.goldSellProduct = goldSellProduct;
//	}
	
	public List<GoldSellProduct> getGoldSellProduct() {
		return goldSellProduct;
	}
	
	public void setGoldSellProduct(List<GoldSellProduct> goldSellProduct) {
		this.goldSellProduct = goldSellProduct;
	}
}
