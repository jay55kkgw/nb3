package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N215A_1_REST_RQ extends BaseRestBean_OLS implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2083396198191201490L;
	
	private String UID;//使用者帳號
	private String COUNT;//筆數
	private String TYPE;//類別
	private String SelectedRecord;//勾選資料
	private String ACNINFO;

	//晶片金融卡
	private	String	ACNNO;
	private	String	ICSEQ;
	private	String	FGTXWAY;
	private	String	iSeqNo;
	private	String	TAC;
	private	String	ISSUER;
	private	String	TRMID;
	private	String	pkcs7Sign;
	private	String	jsondc;
	private String ADOPID;
	
	//IDGATE
	private String sessionID;
	private String idgateID;
	private String txnID;
	
	public String getICSEQ() {
		return ICSEQ;
	}

	public void setICSEQ(String iCSEQ) {
		ICSEQ = iCSEQ;
	}

	public String getJsondc() {
		return jsondc;
	}

	public void setJsondc(String jsondc) {
		this.jsondc = jsondc;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}

	public String getUID() {
		return UID;
	}

	public void setUID(String uID) {
		UID = uID;
	}

	public String getCOUNT() {
		return COUNT;
	}

	public void setCOUNT(String cOUNT) {
		COUNT = cOUNT;
	}

	public String getTYPE() {
		return TYPE;
	}

	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}

	public String getSelectedRecord() {
		return SelectedRecord;
	}

	public void setSelectedRecord(String selectedRecord) {
		SelectedRecord = selectedRecord;
	}
	
	public String getACNINFO() {
		return ACNINFO;
	}

	public void setACNINFO(String aCNINFO) {
		ACNINFO = aCNINFO;
	}

	public String getACNNO() {
		return ACNNO;
	}

	public void setACNNO(String aCNNO) {
		ACNNO = aCNNO;
	}

	public String getFGTXWAY() {
		return FGTXWAY;
	}

	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}

	public String getiSeqNo() {
		return iSeqNo;
	}

	public void setiSeqNo(String iSeqNo) {
		this.iSeqNo = iSeqNo;
	}

	public String getTAC() {
		return TAC;
	}

	public void setTAC(String tAC) {
		TAC = tAC;
	}

	public String getISSUER() {
		return ISSUER;
	}

	public void setISSUER(String iSSUER) {
		ISSUER = iSSUER;
	}

	public String getTRMID() {
		return TRMID;
	}

	public void setTRMID(String tRMID) {
		TRMID = tRMID;
	}

	public String getPkcs7Sign() {
		return pkcs7Sign;
	}

	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public String getIdgateID() {
		return idgateID;
	}

	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}

	public String getTxnID() {
		return txnID;
	}

	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}
}
