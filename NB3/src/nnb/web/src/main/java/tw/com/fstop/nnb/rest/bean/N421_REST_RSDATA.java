package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N421_REST_RSDATA implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3021832556505292635L;

	private String ACN;// 帳號

	private String FDPNUM;// 存單號碼

	private String AMTFDP;// 存單金額

	private String ITR;// 利率

	private String INTMTH;// 計息方式

	private String DPISDT;// 起存日

	private String DUEDAT;// 到期日

	private String TSFACN;// 轉帳帳號

	private String FD_LOST;// 存單掛失

	private String SV_LOST;// 存摺掛失

	private String SIGN_LOST;// 印鑑掛失

	private String MMA;// FOR MMA

	private String ILAZLFTM;// 已轉期數

	private String AUTXFTM;// 未轉期數

	private String TYPE;// 存單種類

	private String STATUS1;// 質押註記

	private String STATUS2;// 質借註記

	private String STATUS3;// 存款人死亡

	private String STATUS4;// 終止存提

	private String STATUS5;// 暫停存提

	private String FILLER_X1;// FILLER_X1

	private String DPINTTYPE;

	private String STATUS;

	public String getACN()
	{
		return ACN;
	}

	public void setACN(String aCN)
	{
		ACN = aCN;
	}

	public String getFDPNUM()
	{
		return FDPNUM;
	}

	public void setFDPNUM(String fDPNUM)
	{
		FDPNUM = fDPNUM;
	}

	public String getAMTFDP()
	{
		return AMTFDP;
	}

	public void setAMTFDP(String aMTFDP)
	{
		AMTFDP = aMTFDP;
	}

	public String getITR()
	{
		return ITR;
	}

	public void setITR(String iTR)
	{
		ITR = iTR;
	}

	public String getINTMTH()
	{
		return INTMTH;
	}

	public void setINTMTH(String iNTMTH)
	{
		INTMTH = iNTMTH;
	}

	public String getDPISDT()
	{
		return DPISDT;
	}

	public void setDPISDT(String dPISDT)
	{
		DPISDT = dPISDT;
	}

	public String getDUEDAT()
	{
		return DUEDAT;
	}

	public void setDUEDAT(String dUEDAT)
	{
		DUEDAT = dUEDAT;
	}

	public String getTSFACN()
	{
		return TSFACN;
	}

	public void setTSFACN(String tSFACN)
	{
		TSFACN = tSFACN;
	}

	public String getFD_LOST()
	{
		return FD_LOST;
	}

	public void setFD_LOST(String fD_LOST)
	{
		FD_LOST = fD_LOST;
	}

	public String getSV_LOST()
	{
		return SV_LOST;
	}

	public void setSV_LOST(String sV_LOST)
	{
		SV_LOST = sV_LOST;
	}

	public String getSIGN_LOST()
	{
		return SIGN_LOST;
	}

	public void setSIGN_LOST(String sIGN_LOST)
	{
		SIGN_LOST = sIGN_LOST;
	}

	public String getMMA()
	{
		return MMA;
	}

	public void setMMA(String mMA)
	{
		MMA = mMA;
	}

	public String getILAZLFTM()
	{
		return ILAZLFTM;
	}

	public void setILAZLFTM(String iLAZLFTM)
	{
		ILAZLFTM = iLAZLFTM;
	}

	public String getAUTXFTM()
	{
		return AUTXFTM;
	}

	public void setAUTXFTM(String aUTXFTM)
	{
		AUTXFTM = aUTXFTM;
	}

	public String getTYPE()
	{
		return TYPE;
	}

	public void setTYPE(String tYPE)
	{
		TYPE = tYPE;
	}

	public String getSTATUS1()
	{
		return STATUS1;
	}

	public void setSTATUS1(String sTATUS1)
	{
		STATUS1 = sTATUS1;
	}

	public String getSTATUS2()
	{
		return STATUS2;
	}

	public void setSTATUS2(String sTATUS2)
	{
		STATUS2 = sTATUS2;
	}

	public String getSTATUS3()
	{
		return STATUS3;
	}

	public void setSTATUS3(String sTATUS3)
	{
		STATUS3 = sTATUS3;
	}

	public String getSTATUS4()
	{
		return STATUS4;
	}

	public void setSTATUS4(String sTATUS4)
	{
		STATUS4 = sTATUS4;
	}

	public String getSTATUS5()
	{
		return STATUS5;
	}

	public void setSTATUS5(String sTATUS5)
	{
		STATUS5 = sTATUS5;
	}

	public String getFILLER_X1()
	{
		return FILLER_X1;
	}

	public void setFILLER_X1(String fILLER_X1)
	{
		FILLER_X1 = fILLER_X1;
	}

	public String getDPINTTYPE()
	{
		return DPINTTYPE;
	}

	public void setDPINTTYPE(String dPINTTYPE)
	{
		DPINTTYPE = dPINTTYPE;
	}

	public String getSTATUS()
	{
		return STATUS;
	}

	public void setSTATUS(String sTATUS)
	{
		STATUS = sTATUS;
	}


}
