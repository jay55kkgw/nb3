package tw.com.fstop.nnb.spring.controller;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.owasp.esapi.SafeFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.common.ResultCode;
import tw.com.fstop.nnb.custom.annotation.IPVERIFY;
import tw.com.fstop.nnb.rest.bean.N360_REST_RS;
import tw.com.fstop.nnb.service.Base_Service;
import tw.com.fstop.nnb.service.DaoService;
import tw.com.fstop.nnb.service.Fund_Transfer_Service;
import tw.com.fstop.nnb.service.Online_Apply_Service;
import tw.com.fstop.nnb.service.Update_File_Service;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.util.StrUtils;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({ 
	SessionUtil.CUSIDN,
	SessionUtil.CUSIDN_N361,
	SessionUtil.DPMYEMAIL,
	SessionUtil.DOWNLOAD_DATALISTMAP_DATA,
	SessionUtil.PRINT_DATALISTMAP_DATA, 
	SessionUtil.TRANSFER_DATA,
	SessionUtil.N361,
	SessionUtil.N361_1,
	SessionUtil.N361_2,
	SessionUtil.N361_3,
	SessionUtil.N361_4,
	SessionUtil.N361_5,
	SessionUtil.N361_KYC,
	SessionUtil.N361_KYC_CONFIRM,
	SessionUtil.N201_CONFIRM_DATA,
	SessionUtil.N201_CONFIRM_DATA_2,
	SessionUtil.N201_CONFIRM_DATA_3,
	SessionUtil.TRANSFER_CONFIRM_TOKEN,
	SessionUtil.TRANSFER_RESULT_TOKEN,
	SessionUtil.TRANSFER_RESULT_FINSH_TOKEN,
	SessionUtil.RESULT_LOCALE_DATA,
	SessionUtil.CONFIRM_LOCALE_DATA,
	SessionUtil.N361_JSONDC,
	SessionUtil.TRANSFER_DATA,
	SessionUtil.STEP1_LOCALE_DATA,
	SessionUtil.STEP2_LOCALE_DATA,
	SessionUtil.STEP3_LOCALE_DATA,
	SessionUtil.UPDATE_IMAGE_DATA,
	SessionUtil.KYC_INSERT_HIST_PASS, 
	SessionUtil.KYC_INSERT_HIST_PASS_FINSH
	})
@Controller
@RequestMapping(value = "/ONLINE/APPLY")
public class Online_Apply_Controller {
	
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private Online_Apply_Service online_apply_service;

	@Autowired
	Fund_Transfer_Service fund_transfer_service;

	@Autowired
	Update_File_Service update_file_service;
	
	@Autowired
	DaoService daoService ;
	
	@Autowired
	I18n i18n;

	@Autowired
	ServletContext context; 
	
	// 線上申請首頁
	@RequestMapping(value = "/online_apply_menu")
	public String apply_menu(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/online_apply/online_apply_menu";
		return target;
	}
	
	// 線上申請說明連結
	@RequestMapping(value = "/online_apply_menu_sub")
	public String apply_menu_sub(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/online_apply/online_apply_menu_sub";
		return target;
	}
	
	
	// 線上申請導向功能頁
	@RequestMapping(value = "/online_apply")
	public String online_apply_gopage(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		boolean hasLocale = okMap.containsKey("locale");
		if (hasLocale) {
//			log.trace("locale>>" + okMap.get("locale"));
			bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
			String type = ((Map<String,String>)bs.getData()).get("TYPE");
			target = targetString(type, model);
		} else {
			bs = new BaseResult();
			String type = reqParam.get("TYPE");
			target = targetString(type, model);
			bs.addAllData(reqParam);
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
		}
		log.trace("target >>{} ", target);
		return target;
	}
	
	public String targetString(String TYPE,Model model) {
		Locale currentLocale = LocaleContextHolder.getLocale();
		String locale = currentLocale.toString();
		String target = "";
		switch (TYPE) {
		case "0":
			// 使用臺灣企銀晶片金融卡+讀卡機
			target = "/online_apply/use_component";
			break;
		case "1":
			// 使用臺灣企銀信用卡
			target = "/online_apply/use_creditcard";
			break;
		case "3":
			// 申請黃金存摺帳戶(含黃金網路交易功能)
			break;
		case "4":
			// 申請黃金網路交易功能—已有臺灣企銀實體黃金存摺者
			break;
		case "5":
			// 線上預約開立存款戶 進入點
    		if(locale.equals("zh_TW")) {
    			target = "/online_apply/apply_deposit_account";
			}else {
    			target = "/online_apply/turn_to_zhTW";
    			model.addAttribute("rev_url", "/ONLINE/APPLY/apply_deposit_account");
			}
			// 清除session暫存
			cleanSession(model);
			break;
		case "6":
    		if(locale.equals("zh_TW")) {
				// 線上預約開立基金戶 進入點
				target = "/online_apply/apply_fund_account";
				// 清除session暫存
				cleanSession(model);
				// 清除身分證號
				SessionUtil.addAttribute(model, SessionUtil.CUSIDN_N361, "");
				// 清除使用者email
				SessionUtil.addAttribute(model, SessionUtil.DPMYEMAIL, "");
				// 下一步路徑
				model.addAttribute("next", "/ONLINE/APPLY/apply_fund_account_1");
			}else {
    			target = "/online_apply/turn_to_zhTW";
				model.addAttribute("rev_url", "/ONLINE/APPLY/apply_fund_account");
			}
			break;
		case "7":
			target = "redirect:/CREDIT/APPLY/apply_creditcard";
			// 線上申請信用卡
			break;
		case "8":
			// 線上簽署信用卡來電分期專案申請書約款
			break;
		case "9":
			// 長期使用循環信用客戶線上申請分期還款
			break;
		case "10":
			// 線上開立數位存款帳戶
    		if(locale.equals("zh_TW")) {
    			model.addAttribute("outside_source", "");
    			target = "/digital_account/apply_digital_account";
			}else {
    			target = "/online_apply/turn_to_zhTW";
				model.addAttribute("rev_url", "/DIGITAL/ACCOUNT/apply_digital_account");
			}
			break;
		case "11":
			// 線上數位存款帳戶補上傳身分證件
    		if(locale.equals("zh_TW")) {
    			target = "/digital_account/upload_digital_identity";
			}else {
    			target = "/online_apply/turn_to_zhTW";
    			model.addAttribute("rev_url", "/DIGITAL/ACCOUNT/upload_digital_identity");
			}
			break;
		case "12":
			// 修改數位存款帳戶資料
    		if(locale.equals("zh_TW")) {
    			target = "/digital_account/modify_digital_data";
			}else {
    			target = "/online_apply/turn_to_zhTW";
    			model.addAttribute("rev_url", "/DIGITAL/ACCOUNT/modify_digital_data");
			}
			break;
		case "13":
			// 數位存款帳戶晶片金融卡確認領用申請及變更密碼
			target = "/digital_account/financial_card_confirm";
			break;
		case "14":
			// 申請隨護神盾
			target = "redirect:/ONLINE/APPLY/shield_apply_p1";
//			target = "/online_apply/CN19_1";
			break;
		case "15":
			// 線上註銷無卡提款功能 
			target = "forward:/ONLINE/APPLY/logout_without_card";
			break;
		case "16":
			// 數位存款帳戶補申請晶片金融卡
    		if(locale.equals("zh_TW")) {
    			target = "forward:/DIGITAL/ACCOUNT/financial_card_renew";
			}else {
    			target = "/online_apply/turn_to_zhTW";
    			model.addAttribute("rev_url", "/DIGITAL/ACCOUNT/financial_card_renew");
			}
			break;
		case "17":
			//線上註銷台灣Pay掃碼提款功能
			target = "forward:/ONLINE/APPLY/logout_taiwan_pay";
			break;
		case "19":
			//信用卡額度調升
			target = "forward:/ONLINE/APPLY/creditcard_limit_increase";
			break;
		case "20":
			// 申請信用卡進度查詢
    		target = "redirect:/CREDIT/APPLY/onlineapply_creditcard_progress";
			break;
			
		}
		return target;
	}
	
	// 隨護神盾p1
	@RequestMapping(value = "/shield_apply_p1")
	public String shield_apply_p1(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/online_apply/CN19_1";
		return target;
	}
	
	// 隨護神盾Q&A
		@RequestMapping(value = "/shield_apply_Q&A")
		public String shield_apply_qa(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			
			Locale currentLocale = LocaleContextHolder.getLocale();
			String locale = currentLocale.toString();
			String target = "/error";
			switch (locale) {
			case "en":
				target = "/online_apply/CN19_QA_ENG";
				break;
			case "zh_TW":
				target = "/online_apply/CN19_QA";
				break;
			case "zh_CN":
				target = "/online_apply/CN19_QA_CHS";
				break;
	        }
			
			return target;
		}

	// 申請隨護神盾_條款頁
	@RequestMapping(value = "/shield_apply_p2")
	public String shield_apply_p2(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		target = "/online_apply/CN19_2";
		return target;
	}

	// 申請隨護神盾_設定下載密碼頁
	@RequestMapping(value = "/shield_apply_p3")
	public String shield_apply_p3(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		String cusidn = "";
		BaseResult bs = null;
		try {
			cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			if(StrUtil.isNotEmpty(cusidn)) {
				bs = new BaseResult();
				bs.addData("CUSIDN", cusidn);
				model.addAttribute("shield_apply",bs);
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		target = "/online_apply/CN19_3";
		return target;
	}
	
	// 申請隨護神盾_結果頁
	@IPVERIFY(cusidnKey="CUSIDN")
	@RequestMapping(value = "/shield_apply_result")
	public String shield_apply_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		String previous = "/ONLINE/APPLY/shield_apply_p3";
		BaseResult bs = null;
		try {
			log.trace("start shield_apply_result_controller");
			 bs = new BaseResult();
			 Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			 
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
//				log.trace("locale: {}", okMap.get("locale"));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					// TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				} else {
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			}else {
				String userip = ESAPIUtil.validInput(WebUtil.getIpAddr(request), "GeneralString", true) ; // 登入 IP-Address
				log.info(ESAPIUtil.vaildLog("userip >> " + userip));
				okMap.put("IP", userip);
				bs = online_apply_service.shield_apply_result(okMap);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("shield_apply_result error >> {}",e);
		} finally {
			if (bs.getResult()) {
				target = "/online_apply/CN19_result";
				model.addAttribute("CN19_result", bs);
			}else {
				//bs.setPrevious(previous);
				target="/online_apply/ErrorWithoutMenu";
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * N361 線上開立基金戶
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/apply_fund_account")
	public String apply_fund_account(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		// 線上預約開立基金戶 進入點
		String target = "/online_apply/apply_fund_account";
		// 清除session暫存
		cleanSession(model);
		// 清除身分證號
		SessionUtil.addAttribute(model, SessionUtil.CUSIDN_N361, "");
		// 清除使用者email
		SessionUtil.addAttribute(model, SessionUtil.DPMYEMAIL, "");
		// 下一步路徑
		model.addAttribute("next", "/ONLINE/APPLY/apply_fund_account_1");
		
		return target;
	}
	
//	/**
//	 * N361 線上開立基金戶 同意書
//	 * 
//	 * @param request
//	 * @param response
//	 * @param reqParam
//	 * @param model
//	 * @return
//	 */
//	@IPVERIFY(cusidnKey="CUSIDN")
//	@RequestMapping(value = "/apply_fund_account_1")
//	public String apply_fund_account_1(HttpServletRequest request, HttpServletResponse response,
//			@RequestParam Map<String, String> reqParam, Model model) 
//	{
//		log.debug("apply_fund_account_1 start");
//		log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+ reqParam));
//		String target = "/online_apply/apply_fund_account_error";
//		String errorPrevious = "/ONLINE/APPLY/online_apply";
//		// 下一步路徑
//		String next = "/ONLINE/APPLY/apply_fund_account_KYC";
//		BaseResult bs = new BaseResult();
//		try 
//		{
//			// 解決Trust Boundary Violation
//			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			// 判斷是否按上一頁
//			boolean isback = "Y".equals(okMap.get("back"));
//			String cusidn = "";
//			if (isback)
//			{
//				// 從session取值
//				cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN_N361, null));
//				cusidn = new String(Base64.getDecoder().decode(cusidn)).toUpperCase();
//				
//				// 回上一頁塞回資料
//				Map<String, String> previousData = (Map) SessionUtil.getAttribute(model, SessionUtil.N361_1, null);
//				model.addAttribute("previousdata", CodeUtil.toJson(previousData));
//				log.trace("previousdata: {}", CodeUtil.toJson(previousData));
//			}
//			else
//			{
//				cusidn = okMap.get("CUSIDN").toUpperCase();
//				// 使用者統編加密後存session
//				String base64 = Base64.getEncoder().encodeToString(cusidn.getBytes());
//				SessionUtil.addAttribute(model, SessionUtil.CUSIDN_N361, base64);
//			}
//			
//			String referer = request.getHeader("Referer");
//			//log.debug("Referer >> {}", referer);
//			log.trace(ESAPIUtil.vaildLog("referer >> " + referer)); 
//			// Referer >> http://localhost:9081/nb3/ONLINE/APPLY/online_apply
//
//			// 未登入網銀之線上申請功能  判斷 新增 apply_fund_account_result 如果是從未登入的結果頁轉導
//			if(referer.contains("online_apply")||referer.contains("apply_fund_account_result"))
//			{
//				// 驗使用者身份
//				bs = online_apply_service.n360(cusidn);
//				// 存session
//				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
//			}
//			else
//			{
//				// 從session取值
//				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null));
//			}
//			
//			// 判斷要顯示的頁面
//			Map<String, Object> bsData = (Map<String, Object>) bs.getData();
//			N360_REST_RS rs = (N360_REST_RS) bsData.get("RS");
//			String occurMsg = rs.getOccurMsg();
//			boolean amlmsg = (boolean) bsData.get("amlmsg");
////			boolean amlmsg = false ;
//			if(("0000".equals(occurMsg) || "    ".equals(occurMsg)) && amlmsg == false) 
//			{
//				target = "/online_apply/apply_fund_account_1";
//				model.addAttribute("N361_1", bs);
//				
//				Map<String, String> ikeyParams = new HashMap<>();
//				// IKEY要使用的JSON:DC參數
//				ikeyParams.put("ADOPID", "N361");
//				ikeyParams.put("CUSIDN", cusidn);
//				ikeyParams.put("UID", 	 cusidn);
//				ikeyParams.put("SVYON",   rs.getSVYON());
//				ikeyParams.put("FATCA",   rs.getFATCA());
//				ikeyParams.put("NBYON",   rs.getNBYON());
//				ikeyParams.put("FNDYON",  rs.getFNDYON());
//				ikeyParams.put("ORDYON",  rs.getORDYON());
//				ikeyParams.put("NBFLAG",  rs.getNBFLAG());
//				ikeyParams.put("CUSTYPE", rs.getCUSTYPE());
//				SessionUtil.addAttribute(model, SessionUtil.N361_JSONDC, ikeyParams);
//				log.debug(ESAPIUtil.vaildLog("N361_JSONDC >> {}"+ ikeyParams));
//			}
//			else if(amlmsg == true) 
//			{
//				bs.setResult(false);
//			}
//			else
//			{
//				// 其他occurMsg
//				String serverName = request.getServerName();
//				log.debug(ESAPIUtil.vaildLog("ServerName >> {}"+ serverName));
//				String url = "";
//				if("10.16.22.17".equals(serverName))
//				{
//					url = "http://10.16.22.71/tbbportal/";
//				}
//				else
//				{
//					// 登入頁
//					url = "/login";
//				}
//				if(occurMsg.equals("Z611"))
//				{
//					// 登入頁
//					url = "/login"; 
//				}
//				if(occurMsg.equals("Z612"))
//				{
//					// to 使用臺灣企銀晶片金融卡 + 讀卡機申請網路銀行
//					url = "/ONLINE/APPLY/use_component";
//					// 讓後續 submit 判斷導向哪支 Controller
//					model.addAttribute("TYPE", "0");
//				}
//				if(occurMsg.equals("Z613"))
//				{
//					// to 線上預約開立存款戶
//					url = "/ONLINE/APPLY/online_apply";
//					// 讓後續 submit 判斷導向哪支 Controller
//					model.addAttribute("TYPE", "5");
//				}
//				log.debug("URL >> {}", url);
//		       	bs.addData("URL", url);
//		       	bs.setResult(false);
//			}
//			
//			// 設定下一步的url
//			model.addAttribute("next", next);
//		}
//		catch (Exception e) 
//		{
//			log.error("apply_fund_account_1 error", e);
//		} 
//		finally 
//		{
//			if(bs == null)
//			{
//				bs = new BaseResult();
//			}
//			if(bs.getResult() == false)
//			{
//				// 錯誤頁回上一頁url
//				bs.setPrevious(errorPrevious);
//				model.addAttribute(BaseResult.ERROR, bs);
//				// 讓重新輸入btn 回線上預約開立基金戶首頁
//				model.addAttribute("PREVIOUS_TYPE", "6");
//			}
//		}
//		return target;
//	}
	
	
	/**
	 * N361 線上開立基金戶 同意書
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@IPVERIFY(cusidnKey = "CUSIDN")
	@RequestMapping(value = "/apply_fund_account_1")
	public String apply_fund_account_1(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.debug("apply_fund_account_1 start");		
		log.debug(ESAPIUtil.vaildLog("reqParam >> {}" + reqParam));
		String target = "/online_apply/apply_fund_account_error";
		String errorPrevious = "/ONLINE/APPLY/online_apply";
		// 下一步路徑
		String next = "/ONLINE/APPLY/apply_fund_account_KYC";
		BaseResult bs = new BaseResult();
		String cusidn = "";
		try {
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷是否按上一頁
			boolean isback = "Y".equals(okMap.get("back"));

			if (isback) {
				// 從session取值
				cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN_N361, null));
				cusidn = new String(Base64.getDecoder().decode(cusidn)).toUpperCase();

				// 回上一頁塞回資料
				Map<String, String> previousData = (Map) SessionUtil.getAttribute(model, SessionUtil.N361_1, null);
				model.addAttribute("previousdata", CodeUtil.toJson(previousData));
				log.trace("previousdata: {}", CodeUtil.toJson(previousData));
			} else {
				cusidn = okMap.get("CUSIDN").toUpperCase();
				// 使用者統編加密後存session
				String base64 = Base64.getEncoder().encodeToString(cusidn.getBytes());
				SessionUtil.addAttribute(model, SessionUtil.CUSIDN_N361, base64);
			}

			String referer = request.getHeader("Referer");
			// log.debug("Referer >> {}", referer);
			log.trace(ESAPIUtil.vaildLog("referer >> " + referer));
			// Referer >> http://localhost:9081/nb3/ONLINE/APPLY/online_apply

			// 未登入網銀之線上申請功能 判斷 新增 apply_fund_account_result 如果是從未登入的結果頁轉導
			if (referer.contains("online_apply") || referer.contains("apply_fund_account_result")) {
				// 驗使用者身份
				bs = online_apply_service.n360(cusidn);
				// 存session
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
			} else {
				// 從session取值
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null));
			}

		} catch (Exception e) {
			//如果發生到這裡整個程式會 ? ...
			log.error("apply_fund_account_1 error", e);
		} finally {// 判斷要顯示的頁面
			Map<String, Object> bsData = (Map<String, Object>) bs.getData();
			if(bs.getResult()) {
				//boolean amlmsg = (boolean) bsData.get("amlmsg");
				// boolean amlmsg = false ;
				//if (amlmsg == false) {
					target = "/online_apply/apply_fund_account_1";
					model.addAttribute("N361_1", bs);
					//未填寫辨識過FATCA，導向FATCA資料填寫頁
					if(bsData.get("FATCA").equals("N")) {
						model.addAttribute("CUSIDN", cusidn);
						next = "/ONLINE/APPLY/FATCA/fatca_and_crs_consent";
					}

					Map<String, String> ikeyParams = new HashMap<>();
					// IKEY要使用的JSON:DC參數
					ikeyParams.put("ADOPID", "N361");
					ikeyParams.put("CUSIDN", cusidn);
					ikeyParams.put("UID", cusidn);
					ikeyParams.put("SVYON", (String) bsData.get("SVYON"));
					ikeyParams.put("FATCA", (String) bsData.get("FATCA"));
					ikeyParams.put("NBYON", (String) bsData.get("NBYON"));
					ikeyParams.put("FNDYON", (String) bsData.get("FNDYON"));
					ikeyParams.put("ORDYON", (String) bsData.get("ORDYON"));
					ikeyParams.put("NBFLAG", (String) bsData.get("NBFLAG"));
					ikeyParams.put("CUSTYPE", (String) bsData.get("CUSTYPE"));
					ikeyParams.put("AMLCOD", (String) bsData.get("amlcode"));
					SessionUtil.addAttribute(model, SessionUtil.N361_JSONDC, ikeyParams);
					log.debug(ESAPIUtil.vaildLog("N361_JSONDC >> {}" + ikeyParams));
				//} else  {
				//	bs.setResult(false);
				//} 
			}else {
				// 其他occurMsg
				String serverName = request.getServerName();
				log.debug(ESAPIUtil.vaildLog("ServerName >> {}" + serverName));
				String url = "";
				if ("10.16.22.17".equals(serverName)) {
					url = "http://10.16.22.71/tbbportal/";
					bs.addData("URL",url);
					
				} else {
					// 登入頁
					url = "/login";
					bs.addData("URL",url);
				}
				if (bsData.get("TOPMSG").equals("Z611")) {
					// 登入頁
					url = "/login";
					bs.addData("URL",url);
				}
				else if (bsData.get("TOPMSG").equals("Z612")) {
					// to 使用臺灣企銀晶片金融卡 + 讀卡機申請網路銀行
					url = "/ONLINE/APPLY/use_component";
					bs.addData("URL",url);
					// 讓後續 submit 判斷導向哪支 Controller
					model.addAttribute("TYPE", "0");
				}
				else if (bsData.get("TOPMSG").equals("Z613")) {
					// to 線上預約開立存款戶
					url = "/ONLINE/APPLY/online_apply";
					bs.addData("URL",url);
					// 讓後續 submit 判斷導向哪支 Controller
					model.addAttribute("TYPE", "5");
				}
				
				// 設定上一步的url
				bs.setPrevious(errorPrevious);
				model.addAttribute(BaseResult.ERROR, bs);
				// 讓重新輸入btn 回線上預約開立基金戶首頁
				model.addAttribute("PREVIOUS_TYPE", "6");
				
			}
			
			// 設定下一步的url
			model.addAttribute("next", next);
		}
		return target;
	 }
	
	
	
	/**
	 * N361 線上開立基金戶  填寫KYC問卷
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/apply_fund_account_KYC")
	public String apply_fund_account_KYC(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		log.debug("apply_fund_account_KYC start");
		log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+ reqParam));
		String target = "/online_apply/apply_fund_account_error";
		// 上一步路徑
		String previous = "/ONLINE/APPLY/apply_fund_account_1";
		// 下一步路徑
		String next = "/ONLINE/APPLY/apply_fund_account_KYC_confirm";
		
		BaseResult bs = new BaseResult();
		String cusidn="";
		Map<String, String> okMap  = new HashMap<String, String>();
		try 
		{
			// 解決Trust Boundary Violation
			okMap = ESAPIUtil.validStrMap(reqParam);
			
			//FATCA所需欄位
			model.addAttribute("FATCA_CONSENT", okMap.get("FATCA_CONSENT"));
			model.addAttribute("CITY", okMap.get("CITY"));

			if ("Y".equals(okMap.get("FATCA_CONSENT"))) {
				previous = "/ONLINE/APPLY/FATCA/fatca_and_crs_consent";
			}
			
			if(okMap.containsKey("SKIP1PAGE")) {
				//這邊不做事
			}else {
				boolean isback = "Y".equals(okMap.get("back"));
				if(isback == false)
				{
					// 回上一頁用
					SessionUtil.addAttribute(model, SessionUtil.N361_1, okMap);
					log.debug("N361_1.json: {}", CodeUtil.toJson(SessionUtil.getAttribute(model, SessionUtil.N361_1, null)));
					
					// N3611flag1,2 加入 JSON:DC
	//				Map<String, String> ikeyParams = (Map) SessionUtil.getAttribute(model, SessionUtil.N361_JSONDC, null);
	//				ikeyParams.putAll(okMap);
	//				log.debug("N361_JSONDC >> {}", ikeyParams);
	//				SessionUtil.addAttribute(model, SessionUtil.N361_JSONDC, ikeyParams);
					model.addAllAttributes(okMap);
					
				} else {
					// 回上一頁塞回資料
					Map<String, String> previousData = (Map) SessionUtil.getAttribute(model, SessionUtil.N361_KYC, null);
					model.addAttribute("previousdata", CodeUtil.toJson(previousData));
					log.trace("previousdata: {}", CodeUtil.toJson(previousData));
				}
			}
			// 指定KYC頁 submit 到哪個 Controller
			model.addAttribute("next", next);
			
			model.addAttribute("previous", previous);
			
			
			// Fund_Transfer_Controller fund_invest_attr1 邏輯區 Start
			cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN_N361, null));
			cusidn = new String(Base64.getDecoder().decode(cusidn));
			
			// 判斷KYC每日僅能填寫三次條件
			boolean todayUserKYCCountBoolean = fund_transfer_service.todayUserKYCCount(cusidn);
			model.addAttribute("todayUserKYCCountBoolean", todayUserKYCCountBoolean);
			
			reqParam.put("CUSIDN", cusidn);
			
			bs = fund_transfer_service.N960_REST(reqParam);
			
			Map<String, Object> bsData = (Map<String, Object>) bs.getData();
			
			if(bs.getResult()) {
				model.addAttribute("nologinMail", bsData.get("MAILADDR"));
			}
			
			bs = fund_transfer_service.getN927Data(cusidn);
			
			fund_transfer_service.prepareInvestAttr1Data(bs, null, model);
			
			// Fund_Transfer_Controller fund_invest_attr1 邏輯區 End
			
		}
		catch (Exception e) 
		{
			log.error("apply_fund_account_KYC error", e);
		} 
		finally 
		{
			if(bs == null)
			{
				bs = new BaseResult();
			}
			if(bs.getResult())
			{
				Map<String, Object> bsData = (Map) bs.getData();
				String str_RISK = String.valueOf(bsData.get("RISK")); // 投資屬性
				if (StrUtil.isEmpty(str_RISK) && !okMap.containsKey("SKIP1PAGE")) {
					//首次提示
					target = "/online_apply/apply_fund_invest_attr";
				} else {
					// 到 KYC 填寫問卷
					target = "/online_apply/apply_fund_account_KYC";
				}
				
			}
			else 
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * N361 線上開立基金戶 KYC問卷確認頁
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/apply_fund_account_KYC_confirm")
	public String apply_fund_account_KYC_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		log.debug("apply_fund_account_KYC_confirm start");
		log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+ reqParam));
		String target = "/online_apply/apply_fund_account_error";
		// 上一步路徑
		String previous = "/ONLINE/APPLY/apply_fund_account_KYC";
		// 下一步路徑
		String next = "/ONLINE/APPLY/apply_fund_account_2";
		
		BaseResult bs = new BaseResult();
		try 
		{
			// 指定KYC 確認頁 submit 到哪個 Controller
			model.addAttribute("next", next);
			model.addAttribute("previous", previous);
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			//FATCA所需欄位
			model.addAttribute("FATCA_CONSENT", okMap.get("FATCA_CONSENT"));
			model.addAttribute("CITY", okMap.get("CITY"));
			
			boolean isback = "Y".equals(okMap.get("back"));
			if(isback == false)
			{
				// 回上一頁用
				SessionUtil.addAttribute(model, SessionUtil.N361_KYC, okMap);
				log.debug("N361_KYC.json: {}", CodeUtil.toJson(SessionUtil.getAttribute(model, SessionUtil.N361_KYC, null)));
				
				// 將 KYC 問卷資料加入 JSON:DC
//				Map<String, String> ikeyParams = (Map) SessionUtil.getAttribute(model, SessionUtil.N361_JSONDC, null);
//				ikeyParams.putAll(okMap);
//				SessionUtil.addAttribute(model, SessionUtil.N361_JSONDC, ikeyParams);
//				log.debug("N361_JSONDC >> {}", ikeyParams);
			}
			
			// Fund_Transfer_Controller fund_invest_attr2 邏輯區 Start
			// 將 KYC 問卷的資料送到 KYC 確認頁
//			Map<String, String> kycParams = (Map) SessionUtil.getAttribute(model, SessionUtil.N361_KYC, null);
			fund_transfer_service.prepareInvestAttr2Data(okMap, model);
			
			// Fund_Transfer_Controller fund_invest_attr2 邏輯區 End
			
			bs.setResult(true);
			bs.setMessage(ResultCode.SYS_SUCCESS, ResultCode.SYS_SUCCESS);
		}
		catch (Exception e) 
		{
			log.error("apply_fund_account_KYC_confirm error", e);
		} 
		finally 
		{
			if(bs == null)
			{
				bs = new BaseResult();
			}
			if(bs.getResult())
			{
				target = "/online_apply/apply_fund_account_KYC_confirm";
			}
			else 
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * N361 線上開立基金戶 約定事項
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/apply_fund_account_2")
	public String apply_fund_account_2(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		log.debug("apply_fund_account_2 start");
		log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+ reqParam));
		String target = "/online_apply/apply_fund_account_error";
		// 上一步路徑
		String previous = "/ONLINE/APPLY/apply_fund_account_KYC_confirm";
		// 下一步路徑
		String next = "/ONLINE/APPLY/apply_fund_account_confirm";
		
		BaseResult bs = new BaseResult();
		try 
		{
			model.addAttribute("previous", previous);
			model.addAttribute("next", next);
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			//FATCA所需欄位
			model.addAttribute("FATCA_CONSENT", okMap.get("FATCA_CONSENT"));
			model.addAttribute("CITY", okMap.get("CITY"));
			
			boolean isback = "Y".equals(okMap.get("back"));
			if(isback == false)
			{
				// 儲存 KYC 確認頁資料，讓最後的N361電文使用
				SessionUtil.addAttribute(model, SessionUtil.N361_KYC_CONFIRM, okMap);
				
				// 棄用apply_fund_account_5的jsondc，改用 session 收集的 jsondc
				reqParam.remove("jsondc");

				// 將所有參數加入JSONDC
				Map<String, String> ikeyParams = (Map) SessionUtil.getAttribute(model, SessionUtil.N361_JSONDC, null);
				ikeyParams.putAll(reqParam); 	// IKEY要用不經過 ESAPIUtil 的參數，會出錯
				ikeyParams.putAll((Map<String, String>) SessionUtil.getAttribute(model, SessionUtil.N361_1, null));
				ikeyParams.putAll((Map<String, String>) SessionUtil.getAttribute(model, SessionUtil.N361_KYC, null));
				log.debug(ESAPIUtil.vaildLog("final JSONDC params >> {}"+ ikeyParams));
				SessionUtil.addAttribute(model, SessionUtil.N361_JSONDC, ikeyParams);
				
				// IKEY要使用的JSON:DC
				String jsondc = URLEncoder.encode(CodeUtil.toJson(ikeyParams), "UTF-8");
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, jsondc);
				
			} else {
				// 回上一頁塞回資料
				Map<String, String> previousData = (Map) SessionUtil.getAttribute(model, SessionUtil.N361_2, null);
				model.addAttribute("previousdata", CodeUtil.toJson(previousData));
				model.addAttribute("FDINVTYPE", okMap.get("FDINVTYPE"));
				log.trace("previousdata: {}", CodeUtil.toJson(previousData));
			}

			String cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN_N361, null));
			cusidn = new String(Base64.getDecoder().decode(cusidn));
			
			// 取新臺幣活期性存款帳戶
			List<String> acn_ssv1 = online_apply_service.getAccounts(cusidn, Base_Service.ACNO);
			// 取外幣活期性存款帳戶
			List<String> acn_fud1 = online_apply_service.getAccounts(cusidn, Base_Service.FX_DEMAND_DEPOSIT_ACCOUNT);
			// 取得使用者email
			String email = null;
			Object sessionE = SessionUtil.getAttribute(model, SessionUtil.DPMYEMAIL, null);
			log.debug(ESAPIUtil.vaildLog("SessionUtil.DPMYEMAIL >> "+sessionE.toString()));
			if(sessionE == null || "".equals(sessionE.toString()))
			{
				email = online_apply_service.getDPMYEMAIL(cusidn);
			}
			else
			{
				email = sessionE.toString();
			}
			
			bs.addData("ACN_SSV1", acn_ssv1);
			bs.addData("ACN_FUD1", acn_fud1);
			bs.addData("DPMYEMAIL", email);
			model.addAttribute("BaseResult", bs);
			model.addAttribute("FDINVTYPE", okMap.get("FDINVTYPE"));
			bs.setResult(true);
			bs.setMessage(ResultCode.SYS_SUCCESS, ResultCode.SYS_SUCCESS);
			
		}
		catch (Exception e) 
		{
			log.error("apply_fund_account_2 error", e);
		} 
		finally 
		{
			if(bs == null)
			{
				bs = new BaseResult();
			}
			if(bs.getResult())
			{
				target = "/online_apply/apply_fund_account_2";
			}
			else 
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * N361 線上開立基金戶 約定事項內容確認
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/apply_fund_account_confirm")
	public String apply_fund_account_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		log.debug("apply_fund_account_confirm start");
		log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+ reqParam));
		String target = "/online_apply/apply_fund_account_error";
		// 上一步路徑
		String previous = "/ONLINE/APPLY/apply_fund_account_2";
		// 下一步路徑
		String next = "/ONLINE/APPLY/apply_fund_account_result";
		
		BaseResult bs = new BaseResult();
		try 
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			//FATCA所需欄位
			model.addAttribute("FATCA_CONSENT", okMap.get("FATCA_CONSENT"));
			model.addAttribute("CITY", okMap.get("CITY"));
			
			boolean isback = "Y".equals(okMap.get("back"));
			if(isback == false)
			{
				// 回上一頁
				SessionUtil.addAttribute(model, SessionUtil.N361_2, okMap);
				log.debug("N361_2.json: {}", CodeUtil.toJson(SessionUtil.getAttribute(model, SessionUtil.N361_2, null)));
				
				// 儲存約定事項頁面資料，讓約定事項內容確認頁使用
				SessionUtil.addAttribute(model, SessionUtil.N361_4, okMap);
				log.debug("N361_4.json: {}", CodeUtil.toJson(SessionUtil.getAttribute(model, SessionUtil.N361_4, null)));
				
				// 儲存通知email
				String DPMYEMAIL = okMap.get("DPMYEMAIL") == null ? "" : okMap.get("DPMYEMAIL");
				SessionUtil.addAttribute(model, SessionUtil.DPMYEMAIL, DPMYEMAIL);
				okMap.put("DPMYEMAIL", DPMYEMAIL);
				
				// 將約定事項參數加入 JSON:DC
				Map<String, String> ikeyParams = (Map) SessionUtil.getAttribute(model, SessionUtil.N361_JSONDC, null);
				ikeyParams.putAll(okMap);
				ikeyParams.remove("ACN_SSV1");
				ikeyParams.remove("ACN_FUD1");
				SessionUtil.addAttribute(model, SessionUtil.N361_JSONDC, ikeyParams);
				log.debug(ESAPIUtil.vaildLog("N361_JSONDC: {}"+ CodeUtil.toJson(ikeyParams)));

//20211126註解此區  往後移				
//				//20211108新增 在此頁先打 N322取得分數及投資屬性等級於頁面顯示並回存KYC PARAM
//				//獲取 中心計算之KYC分數及投資屬性
//				// KYC確認頁資料
//				Map<String, String> kycConfirmParams = (Map) SessionUtil.getAttribute(model, SessionUtil.N361_KYC_CONFIRM, null);
//				log.debug("get kycScore Params >> {}", kycConfirmParams);
//				String cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN_N361, null));
//				cusidn = new String(Base64.getDecoder().decode(cusidn));
//				kycConfirmParams.put("CUSIDN", cusidn);
//				bs = fund_transfer_service.getKycScore(kycConfirmParams);
//				String SCORE =StrUtil.isNotEmpty((String) ((Map<String,Object>)bs.getData()).get("SCORE"))?(String) ((Map<String,Object>)bs.getData()).get("SCORE"):"";
//				String RISK =StrUtil.isNotEmpty((String) ((Map<String,Object>)bs.getData()).get("RISK"))?(String) ((Map<String,Object>)bs.getData()).get("RISK"):"";
//				kycConfirmParams.put("FDINVTYPE", RISK);
//				kycConfirmParams.put("FDSCORE", SCORE);
//				
//				SessionUtil.addAttribute(model, SessionUtil.N361_KYC_CONFIRM, kycConfirmParams);
//				
//				model.addAttribute("FDINVTYPE", RISK);
			
			}
			
			model.addAttribute("next", next);
			model.addAttribute("previous", previous);
			bs.setResult(true);
			bs.setMessage(ResultCode.SYS_SUCCESS, ResultCode.SYS_SUCCESS);
		}
		catch (Exception e) 
		{
			log.error("apply_fund_account_confirm error", e);
		} 
		finally 
		{
			if(bs == null)
			{
				bs = new BaseResult();
			}
			if(bs.getResult())
			{
				target = "/online_apply/apply_fund_account_confirm";
			}
			else 
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * N361 線上開立基金戶  結果頁 
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/apply_fund_account_result")
	public String apply_fund_account_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		log.debug("apply_fund_account_result start");
		log.debug(ESAPIUtil.vaildLog("交易機制參數 >> {}"+ reqParam));
		String target = "/online_apply/apply_fund_account_error";
		BaseResult bs = new BaseResult();
		try 
		{     
			Map<String,String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			//FATCA所需欄位
			model.addAttribute("FATCA_CONSENT", okMap.get("FATCA_CONSENT"));
			
			if ("N".equals(okMap.get("AGREE"))) {
				//不同意  更新txnCusInvAttrHist
				target = "forward:/ONLINE/APPLY/apply_fund_account";
				daoService.updateCusInvAttrHistAgree(okMap.get("FDHISTID"), okMap.get("AGREE"));
				return target;
			}

			String kyc_insert_hist_pass = (String) SessionUtil.getAttribute(model, SessionUtil.KYC_INSERT_HIST_PASS, null);		
			if(!okMap.containsKey("kyc_insert_uuid") || kyc_insert_hist_pass == null || !okMap.get("kyc_insert_uuid").equals(kyc_insert_hist_pass)) {
				bs.setResult(false);
				throw new Exception("kyc_insert_hist_pass check error");
			}
			String kyc_insert_hist_pass_finsh = (String) SessionUtil.getAttribute(model, SessionUtil.KYC_INSERT_HIST_PASS_FINSH, null);		
			if(!okMap.containsKey("kyc_insert_uuid") || okMap.get("kyc_insert_uuid").equals(kyc_insert_hist_pass_finsh)) {
				bs.setResult(false);
				throw new Exception("kyc_insert_hist_pass check error");
			}
			SessionUtil.addAttribute(model, SessionUtil.KYC_INSERT_HIST_PASS_FINSH, okMap.get("kyc_insert_uuid"));
			
			
			// KYC確認頁資料
			Map<String, String> kycConfirmParams = (Map) SessionUtil.getAttribute(model, SessionUtil.N361_KYC_CONFIRM, null);
			log.debug("kycConfirmParams >> {}", kycConfirmParams);
			
			//將ajax回來的FDINVTYPE放到kycConfirmParams 內去轉換xls需要的值
			kycConfirmParams.put("FDINVTYPE", okMap.get("FDINVTYPE"));
			
			// 約定事項資料
			Map<String, String> n361_4_params = (Map) SessionUtil.getAttribute(model, SessionUtil.N361_4, null);
			log.debug("n361_4_params >> {}", n361_4_params);
			
			// 使用者統編
			String cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN_N361, null));
			cusidn = new String(Base64.getDecoder().decode(cusidn));
			
			// E-MAIL
			String email = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.DPMYEMAIL, null));
			
			// 將約定事項參數加入 JSON:DC
			Map<String, String> ikeyParams = (Map) SessionUtil.getAttribute(model, SessionUtil.N361_JSONDC, null);
			String amlcode = ikeyParams.get("AMLCOD");
			log.debug("N361_JSONDC: {}",ESAPIUtil.vaildLog(CodeUtil.toJson(ikeyParams)));
			log.debug("AMLCOD >> {}", amlcode);
			
			// N361電文資料
			Map<String, String> n361Params = new HashMap<>();
			// 加入 KYC 確認資料
			n361Params.putAll(kycConfirmParams);
			// 加入 約定事項確認資料
			n361Params.putAll(n361_4_params);
			// 交易機制參數
			n361Params.putAll(okMap);
			n361Params.put("CUSIDN", cusidn);
			n361Params.put("DPMYEMAIL", email);
			
			n361Params.put("PCMAC", "");
			
			String devName = n361Params.get("DEVNAME");
			if (devName.length() > 30) {
				devName = devName.substring(0, 30);
			}
			log.info("devName.length() : " + devName.length());
			log.info(ESAPIUtil.vaildLog("devName : " + devName));
			n361Params.put("DEVNAME", devName);
			
			String aduserip = WebUtil.getIpAddr(request);
//			//TEST
//			aduserip = "172.22.11.23";
			n361Params.put("ADUSERIP", aduserip);
			n361Params.put("AMLCOD", amlcode);

			//FATCA個人客戶身份識別聲明出生地城市
			n361Params.put("CITY", okMap.get("CITY"));

			// N361電文
			bs = online_apply_service.n361(n361Params);
			
			// 取得 KYC 選項描述i18n
			kycConfirmParams = online_apply_service.addKycOptionDescription(kycConfirmParams);
			SessionUtil.addAttribute(model, SessionUtil.N361_KYC_CONFIRM, kycConfirmParams);
			
			model.addAttribute("n361_8", bs);
		}
		catch (Exception e) 
		{
			log.error("apply_fund_account_result error", e);
		} 
		finally 
		{
			if(bs == null)
			{
				bs = new BaseResult();
			}
			if(bs.getResult())
			{
				target = "/online_apply/apply_fund_account_result";
			}
			else 
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * 檢查使用者是否超過KYC填寫次數，KYC每日僅能填寫三次
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@PostMapping(value = "/getKycCanDo_aj" ,produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult getKycCanDo(HttpServletRequest request, 
			HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) 
	{
		log.debug("getKycCanDo_aj start");
		log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+ reqParam));
		BaseResult bs = new BaseResult();
		try 
		{
			String cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null));
			cusidn = new String(Base64.getDecoder().decode(cusidn));
			
			// 判斷KYC每日僅能填寫三次條件，true 表示小於3次
			boolean kycCanDo = fund_transfer_service.todayUserKYCCount(cusidn);
			bs.addData("KycCanDo", kycCanDo);
			bs.setResult(true);
			bs.setMessage(ResultCode.SYS_SUCCESS, i18n.getMsg("LB.X1778"));
			log.debug("getKycCanDo_aj result >> {}", CodeUtil.toJson(bs));
		} 
		catch (Exception e) 
		{
			log.error("getKycCanDo_aj error", e);
		}
		return bs;
	}
	
	/**
	 * 檢查使用者是否超過KYC填寫次數，KYC每日僅能填寫三次--未登入
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@PostMapping(value = "/getKycCanDo_notlogin_aj" ,produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult getKycCanDo_notlogin_aj(HttpServletRequest request, 
			HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) 
	{
		log.debug("getKycCanDo_aj start");
		log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+ reqParam));
		BaseResult bs = new BaseResult();
		try 
		{
			String cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN_N361, null));
			cusidn = new String(Base64.getDecoder().decode(cusidn));
			
			// 判斷KYC每日僅能填寫三次條件，true 表示小於3次
			boolean kycCanDo = fund_transfer_service.todayUserKYCCount(cusidn);
			bs.addData("KycCanDo", kycCanDo);
			bs.setResult(true);
			bs.setMessage(ResultCode.SYS_SUCCESS, i18n.getMsg("LB.X1778"));
			log.debug("getKycCanDo_aj result >> {}", CodeUtil.toJson(bs));
		} 
		catch (Exception e) 
		{
			log.error("getKycCanDo_aj error", e);
		}
		return bs;
	}
	
	
	/**
	 * 清除暫存session的資料
	 * 
	 * @param model
	 */
	public void cleanSession(Model model)
	{
		// 清除換頁暫存資料
		SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, null);
		// 清除N361 flag
		SessionUtil.addAttribute(model, SessionUtil.N361, null);
		SessionUtil.addAttribute(model, SessionUtil.N361_1, null);
		SessionUtil.addAttribute(model, SessionUtil.N361_2, null);
		SessionUtil.addAttribute(model, SessionUtil.N361_3, null);
		// KYC 問卷
		SessionUtil.addAttribute(model, SessionUtil.N361_KYC, null);
		// 使用者確認過的 KYC 問卷
		SessionUtil.addAttribute(model, SessionUtil.N361_KYC_CONFIRM, null);
		// 約定事項
		SessionUtil.addAttribute(model, SessionUtil.N361_4, null);
		SessionUtil.addAttribute(model, SessionUtil.N361_5, null);
		// 清除 JSONDC
		SessionUtil.addAttribute(model, SessionUtil.N361_JSONDC, null);
		// 清除暫存
		SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, null);
	}
	
	
//	/**
//	 * 申請信託開戶(同apply_fund_account_1 線上預約開立基金戶，但是要登入網銀做)
//	 * 
//	 * @param request
//	 * @param response
//	 * @param reqParam
//	 * @param model
//	 * @return
//	 */
//	@RequestMapping(value = "/apply_trust_account_1")
//	public String apply_trust_account_1(HttpServletRequest request, HttpServletResponse response,
//			@RequestParam Map<String, String> reqParam, Model model) 
//	{
//		Locale currentLocale = LocaleContextHolder.getLocale();
//		String locale = currentLocale.toString();
//		if(!locale.equals("zh_TW")) {
//			model.addAttribute("back_url", "/INDEX/index");
//			model.addAttribute("rev_url", "/ONLINE/APPLY/apply_trust_account_1");
//			return "/online_apply/turn_to_zhTW_with_menu";
//		}
//		log.debug("apply_trust_account_1 start");
//		log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+ reqParam));
//		String target = "/online_apply/apply_trust_account_error";
//		String errorPrevious = "/INDEX/index";
//		// 下一步路徑
//		String next = "/ONLINE/APPLY/apply_trust_account_KYC";
//		BaseResult bs = new BaseResult();
//		try 
//		{
//			String cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null));
//			cusidn = new String(Base64.getDecoder().decode(cusidn)).toUpperCase();
//			
//			if(cusidn.length() == 8) {
//				model.addAttribute("checkCusidn", false);
//			}
//			
//			// 解決Trust Boundary Violation
//			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			// 判斷是否按上一頁
//			boolean isback = "Y".equals(okMap.get("back"));
//
//			// 判斷LOCAL KEY 若有帶值表示切換語系
//			boolean hasLocale = okMap.containsKey("locale");
//			
//			if (isback)
//			{
//				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.N361, null));
//				
//				// 回上一頁塞回資料
//				Map<String, String> previousData = (Map) SessionUtil.getAttribute(model, SessionUtil.N361_1, null);
//				model.addAttribute("previousdata", CodeUtil.toJson(previousData));
//				log.trace("previousdata: {}", CodeUtil.toJson(previousData));
//			}
//			else
//			{
//				cleanSession(model);
//				// 驗使用者身份
//				bs = online_apply_service.n360(cusidn);
//				SessionUtil.addAttribute(model, SessionUtil.N361, bs);
//			}
//			
//			
//			// 判斷要顯示的頁面
//			Map<String, Object> bsData = (Map<String, Object>) bs.getData();
//			N360_REST_RS rs = (N360_REST_RS) bsData.get("RS");
//			String occurMsg = rs.getOccurMsg();
//			boolean amlmsg = (boolean) bsData.get("amlmsg");
////			boolean amlmsg = false ;
//			if(("0000".equals(occurMsg) || "    ".equals(occurMsg)) && amlmsg == false) 
//			{
//				target = "/online_apply/apply_trust_account_1";
//				model.addAttribute("N361_1", bs);
//				
//				Map<String, String> ikeyParams = new HashMap<>();
//				// IKEY要使用的JSON:DC參數
//				ikeyParams.put("ADOPID", "N361");
//				ikeyParams.put("CUSIDN", cusidn);
//				ikeyParams.put("UID", 	 cusidn);
//				ikeyParams.put("SVYON",   rs.getSVYON());
//				ikeyParams.put("FATCA",   rs.getFATCA());
//				ikeyParams.put("NBYON",   rs.getNBYON());
//				ikeyParams.put("FNDYON",  rs.getFNDYON());
//				ikeyParams.put("ORDYON",  rs.getORDYON());
//				ikeyParams.put("NBFLAG",  rs.getNBFLAG());
//				ikeyParams.put("CUSTYPE", rs.getCUSTYPE());
//				SessionUtil.addAttribute(model, SessionUtil.N361_JSONDC, ikeyParams);
//				log.debug("N361_JSONDC >> {}", ikeyParams);
//			}
//			else if(amlmsg == true) 
//			{
//				bs.setResult(false);
//			}
//			else
//			{
//				// 其他occurMsg
//				String serverName = request.getServerName();
//				log.debug(ESAPIUtil.vaildLog("ServerName >> {}"+ serverName));
//				String url = "";
//				if("10.16.22.17".equals(serverName))
//				{
//					url = "http://10.16.22.71/tbbportal/";
//				}
//				else
//				{
//					// 登入頁
//					url = "/login";
//				}
//				if(occurMsg.equals("Z611"))
//				{
//					// to 金融卡線上申請/取消轉帳功能
//					url = "/ONLINE/SERVING/financial_card_apply_contract";
//				}
//				if(occurMsg.equals("Z612"))
//				{
//					// to 使用臺灣企銀晶片金融卡 + 讀卡機申請網路銀行
//					url = "/ONLINE/APPLY/use_component";
//					// 讓後續 submit 判斷導向哪支 Controller
//					model.addAttribute("TYPE", "0");
//				}
//				if(occurMsg.equals("Z613"))
//				{
//					// to 線上預約開立存款戶
//					url = "/ONLINE/APPLY/online_apply";
//					// 讓後續 submit 判斷導向哪支 Controller
//					model.addAttribute("TYPE", "5");
//				}
//				log.debug("URL >> {}", url);
//		       	bs.addData("URL", url);
//		       	bs.setResult(false);
//			}
//			
//			// 設定下一步的url
//			model.addAttribute("next", next);
//		}
//		catch (Exception e) 
//		{
//			log.error("apply_trust_account_1 error", e);
//		} 
//		finally 
//		{
//			if(bs == null)
//			{
//				bs = new BaseResult();
//			}
//			if(bs.getResult() == false)
//			{
//				// 錯誤頁回上一頁url
//				bs.setPrevious(errorPrevious);
//				model.addAttribute(BaseResult.ERROR, bs);
//			}
//		}
//		return target;
//	}
	
	
	/**
	 * 申請信託開戶(同apply_fund_account_1 線上預約開立基金戶，但是要登入網銀做)
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/apply_trust_account_1")
	public String apply_trust_account_1(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		Locale currentLocale = LocaleContextHolder.getLocale();
		String locale = currentLocale.toString();
		if (!locale.equals("zh_TW")) {
			model.addAttribute("back_url", "/INDEX/index");
			model.addAttribute("rev_url", "/ONLINE/APPLY/apply_trust_account_1");
			return "/online_apply/turn_to_zhTW_with_menu";
		}
		log.debug("apply_trust_account_1 start");
		log.debug(ESAPIUtil.vaildLog("reqParam >> {}" + reqParam));
		String target = "/online_apply/apply_trust_account_error";
		String errorPrevious = "/INDEX/index";
		// 下一步路徑
		String next = "/ONLINE/APPLY/apply_trust_account_KYC";
		BaseResult bs = new BaseResult();
		String cusidn = "";
		try {
			cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null));
			cusidn = new String(Base64.getDecoder().decode(cusidn)).toUpperCase();

			if (cusidn.length() == 8) {
				model.addAttribute("checkCusidn", false);
			}

			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷是否按上一頁
			boolean isback = "Y".equals(okMap.get("back"));

			// 判斷LOCAL KEY 若有帶值表示切換語系
			boolean hasLocale = okMap.containsKey("locale");

			if (isback) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.N361, null));

				// 回上一頁塞回資料
				Map<String, String> previousData = (Map) SessionUtil.getAttribute(model, SessionUtil.N361_1, null);
				model.addAttribute("previousdata", CodeUtil.toJson(previousData));
				log.trace("previousdata: {}", CodeUtil.toJson(previousData));
			} else {
				cleanSession(model);
				// 驗使用者身份
				bs = online_apply_service.n360(cusidn);
				log.debug("bs.getData() = {}", bs.getData());
				SessionUtil.addAttribute(model, SessionUtil.N361, bs);
			}
		} catch (Exception e) {
			log.error("apply_trust_account_1 error", e);
		} finally {
			Map<String, Object> bsData = (Map<String, Object>) bs.getData();
			if(bs.getResult()) {
//				boolean amlmsg = (boolean) bsData.get("amlmsg");
				// boolean amlmsg = false ;
//				if (amlmsg == false) {
					target = "/online_apply/apply_trust_account_1";
					model.addAttribute("N361_1", bs);
					//未填寫辨識過FATCA，導向FATCA資料填寫頁
					if(bsData.get("FATCA").equals("N")) {
						model.addAttribute("CUSIDN", cusidn);
						next = "/ONLINE/APPLY/FATCA/fatca_and_crs_consent";
					}

					Map<String, String> ikeyParams = new HashMap<>();
					// IKEY要使用的JSON:DC參數
					ikeyParams.put("ADOPID", "N361");
					ikeyParams.put("CUSIDN", cusidn);
					ikeyParams.put("UID", cusidn);
					ikeyParams.put("SVYON", (String) bsData.get("SVYON"));
					ikeyParams.put("FATCA", (String) bsData.get("FATCA"));
					ikeyParams.put("NBYON", (String) bsData.get("NBYON"));
					ikeyParams.put("FNDYON", (String) bsData.get("FNDYON"));
					ikeyParams.put("ORDYON", (String) bsData.get("ORDYON"));
					ikeyParams.put("NBFLAG", (String) bsData.get("NBFLAG"));
					ikeyParams.put("CUSTYPE", (String) bsData.get("CUSTYPE"));
					ikeyParams.put("AMLCOD", (String) bsData.get("amlcode"));
					SessionUtil.addAttribute(model, SessionUtil.N361_JSONDC, ikeyParams);
					log.debug(ESAPIUtil.vaildLog("N361_JSONDC >> {}" + ikeyParams));
//				} else  {
//					bs.setResult(false);
//				}
			}else {
				log.debug("else");
				// 其他occurMsg
				String serverName = request.getServerName();
				log.debug(ESAPIUtil.vaildLog("ServerName >> {}" + serverName));
				String url = "";
				if ("10.16.22.17".equals(serverName)) {
					url = "http://10.16.22.71/tbbportal/";
					bs.addData("URL",url);
					
				} else {
					// 登入頁
					url = "/login";
					bs.addData("URL",url);
				}
				if (bsData.get("TOPMSG").equals("Z611")) {
					// 登入頁
					url = "/login";
					bs.addData("URL",url);
				}
				else if (bsData.get("TOPMSG").equals("Z612")) {
					// to 使用臺灣企銀晶片金融卡 + 讀卡機申請網路銀行
					url = "/ONLINE/APPLY/use_component";
					bs.addData("URL",url);
					// 讓後續 submit 判斷導向哪支 Controller
					model.addAttribute("TYPE", "0");
				}
				else if (bsData.get("TOPMSG").equals("Z613")) {
					// to 線上預約開立存款戶
					url = "/ONLINE/APPLY/online_apply";
					bs.addData("URL",url);
					// 讓後續 submit 判斷導向哪支 Controller
					model.addAttribute("TYPE", "5");
				}
				
				// 設定上一步的url
				bs.setPrevious(errorPrevious);
				model.addAttribute(BaseResult.ERROR, bs);
				// 讓重新輸入btn 回線上預約開立基金戶首頁
				model.addAttribute("PREVIOUS_TYPE", "6");
			}
			// 設定下一步的url
			model.addAttribute("next", next);
		}
		return target;
	}
	
	/**
	 * 申請信託開戶_KYC(同apply_fund_account_KYC 填寫問卷)
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/apply_trust_account_KYC")
	public String apply_trust_account_KYC(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		log.debug("apply_trust_account_KYC start");
		log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+ reqParam));
		String target = "/online_apply/apply_trust_account_error";
		String errorPrevious = "/ONLINE/APPLY/apply_trust_account_1";
		// 上一步路徑
		String previous = "/ONLINE/APPLY/apply_trust_account_1";
		// 下一步路徑
		String next = "/ONLINE/APPLY/apply_trust_account_KYC_confirm";
		
		BaseResult bs = new BaseResult();
		String cusidn="";
		Map<String, String> okMap  = new HashMap<String, String>();
		try 
		{
			// 解決Trust Boundary Violation
			okMap = ESAPIUtil.validStrMap(reqParam);
			
			//FATCA所需欄位
			model.addAttribute("FATCA_CONSENT", okMap.get("FATCA_CONSENT"));
			model.addAttribute("CITY", okMap.get("CITY"));

			if ("Y".equals(okMap.get("FATCA_CONSENT"))) {
				previous = "/ONLINE/APPLY/FATCA/fatca_and_crs_consent";
			}
			
			// 判斷LOCAL KEY 若有帶值表示切換語系
			boolean hasLocale = okMap.containsKey("locale");
			
			if(hasLocale == false)
			{
				
				
				if(okMap.containsKey("SKIP1PAGE")) {
					//這邊不做事
				}else {
					// 判斷是否按上一頁
					boolean isback = "Y".equals(okMap.get("back"));
					if(isback == false)
					{	
						// 回上一頁用
						SessionUtil.addAttribute(model, SessionUtil.N361_1, okMap);
						log.debug("N361_1.json: {}", CodeUtil.toJson(SessionUtil.getAttribute(model, SessionUtil.N361_1, null)));
						
					} else {
						// 回上一頁塞回資料
						Map<String, String> previousData = (Map) SessionUtil.getAttribute(model, SessionUtil.N361_KYC, null);
						model.addAttribute("previousdata", CodeUtil.toJson(previousData));
						log.trace("previousdata: {}", CodeUtil.toJson(previousData));
					}
				}
					
				// Fund_Transfer_Controller fund_invest_attr1 邏輯區 Start
				cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null));
				cusidn = new String(Base64.getDecoder().decode(cusidn));

				// 判斷KYC每日僅能填寫三次條件
				boolean todayUserKYCCountBoolean = fund_transfer_service.todayUserKYCCount(cusidn);
				model.addAttribute("todayUserKYCCountBoolean", todayUserKYCCountBoolean);

				bs = fund_transfer_service.getN927Data(cusidn);
				
				fund_transfer_service.prepareInvestAttr1Data(bs, null, model);

				// Fund_Transfer_Controller fund_invest_attr1 邏輯區 End
				
				// 指定KYC頁 submit 到哪個 Controller
				model.addAttribute("next", next);
				
				model.addAttribute("previous", previous);
			}
			
			// 指定KYC頁 submit 到哪個 Controller
			model.addAttribute("next", next);
			
			model.addAttribute("previous", previous);
			
			
			// Fund_Transfer_Controller fund_invest_attr1 邏輯區 Start
			cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null));
			cusidn = new String(Base64.getDecoder().decode(cusidn));
			
			// 判斷KYC每日僅能填寫三次條件
			boolean todayUserKYCCountBoolean = fund_transfer_service.todayUserKYCCount(cusidn);
			model.addAttribute("todayUserKYCCountBoolean", todayUserKYCCountBoolean);
			
			bs = fund_transfer_service.getN927Data(cusidn);
			
			fund_transfer_service.prepareInvestAttr1Data(bs, null, model);
			
			// Fund_Transfer_Controller fund_invest_attr1 邏輯區 End
			
		}
		catch (Exception e) 
		{
			log.error("apply_trust_account_KYC error", e);
		} 
		finally 
		{
			if(bs == null)
			{
				bs = new BaseResult();
			}
			if(bs.getResult())
			{
				Map<String, Object> bsData = (Map) bs.getData();
				String str_RISK = String.valueOf(bsData.get("RISK")); // 投資屬性
				if (StrUtil.isEmpty(str_RISK) && !okMap.containsKey("SKIP1PAGE")) {
					//首次提示
					target = "/online_apply/apply_trust_invest_attr";
				} else {
					// 到 KYC 填寫問卷
					target = "/online_apply/apply_trust_account_KYC";
				}
				
			}
			else 
			{
				bs.setPrevious(errorPrevious);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * N361 線上開立基金戶 KYC問卷確認頁
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/apply_trust_account_KYC_confirm")
	public String apply_trust_account_KYC_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		log.debug("apply_trust_account_KYC_confirm start");
		log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+ reqParam));
		String target = "/online_apply/apply_trust_account_error";
		// 上一步路徑
		String previous = "/ONLINE/APPLY/apply_trust_account_KYC";
		// 下一步路徑
		String next = "/ONLINE/APPLY/apply_trust_account_2";
		
		BaseResult bs = new BaseResult();
		try 
		{
			// 指定KYC 確認頁 submit 到哪個 Controller
			model.addAttribute("next", next);
			model.addAttribute("previous", previous);
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

			//FATCA所需欄位
			model.addAttribute("FATCA_CONSENT", okMap.get("FATCA_CONSENT"));
			model.addAttribute("CITY", okMap.get("CITY"));
			
			boolean isback = "Y".equals(okMap.get("back"));
			if(isback == false)
			{
				// 回上一頁用
				SessionUtil.addAttribute(model, SessionUtil.N361_KYC, okMap);
				log.debug("N361_KYC.json: {}", CodeUtil.toJson(SessionUtil.getAttribute(model, SessionUtil.N361_KYC, null)));
				
				// 將 KYC 問卷資料加入 JSON:DC
//				Map<String, String> ikeyParams = (Map) SessionUtil.getAttribute(model, SessionUtil.N361_JSONDC, null);
//				ikeyParams.putAll(okMap);
//				SessionUtil.addAttribute(model, SessionUtil.N361_JSONDC, ikeyParams);
//				log.debug("N361_JSONDC >> {}", ikeyParams);
			}
			
			// Fund_Transfer_Controller fund_invest_attr2 邏輯區 Start
			// 將 KYC 問卷的資料送到 KYC 確認頁
//			Map<String, String> kycParams = (Map) SessionUtil.getAttribute(model, SessionUtil.N361_KYC, null);
			fund_transfer_service.prepareInvestAttr2Data(okMap, model);
			
			// Fund_Transfer_Controller fund_invest_attr2 邏輯區 End
			
			bs.setResult(true);
			bs.setMessage(ResultCode.SYS_SUCCESS, ResultCode.SYS_SUCCESS);
		}
		catch (Exception e) 
		{
			log.error("apply_trust_account_KYC_confirm error", e);
		} 
		finally 
		{
			if(bs == null)
			{
				bs = new BaseResult();
			}
			if(bs.getResult())
			{
				target = "/online_apply/apply_trust_account_KYC_confirm";
			}
			else 
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * N361 線上開立基金戶 約定事項
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/apply_trust_account_2")
	public String apply_trust_account_2(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		log.debug("apply_trust_account_2 start");
		log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+ reqParam));
		String target = "/online_apply/apply_trust_account_error";
		// 上一步路徑
		String previous = "/ONLINE/APPLY/apply_trust_account_KYC_confirm";
		// 下一步路徑
		String next = "/ONLINE/APPLY/apply_trust_account_confirm";
		
		BaseResult bs = new BaseResult();
		try 
		{
			model.addAttribute("previous", previous);
			model.addAttribute("next", next);
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			//FATCA所需欄位
			model.addAttribute("FATCA_CONSENT", okMap.get("FATCA_CONSENT"));
			model.addAttribute("CITY", okMap.get("CITY"));
			
			boolean isback = "Y".equals(okMap.get("back"));
			if(isback == false)
			{
				// 儲存 KYC 確認頁資料，讓最後的N361電文使用
				SessionUtil.addAttribute(model, SessionUtil.N361_KYC_CONFIRM, okMap);
				
				// 棄用apply_trust_account_5的jsondc，改用 session 收集的 jsondc
				reqParam.remove("jsondc");

				// 將所有參數加入JSONDC
				Map<String, String> ikeyParams = (Map) SessionUtil.getAttribute(model, SessionUtil.N361_JSONDC, null);
				ikeyParams.putAll(reqParam); 	// IKEY要用不經過 ESAPIUtil 的參數，會出錯
				ikeyParams.putAll((Map<String, String>) SessionUtil.getAttribute(model, SessionUtil.N361_1, null));
				ikeyParams.putAll((Map<String, String>) SessionUtil.getAttribute(model, SessionUtil.N361_KYC, null));
				log.debug(ESAPIUtil.vaildLog("final JSONDC params >> {}"+ ikeyParams));
				SessionUtil.addAttribute(model, SessionUtil.N361_JSONDC, ikeyParams);
				
				// IKEY要使用的JSON:DC
				String jsondc = URLEncoder.encode(CodeUtil.toJson(ikeyParams), "UTF-8");
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, jsondc);
				
			} else {
				// 回上一頁塞回資料
				Map<String, String> previousData = (Map) SessionUtil.getAttribute(model, SessionUtil.N361_2, null);
				model.addAttribute("previousdata", CodeUtil.toJson(previousData));
				model.addAttribute("FDINVTYPE", okMap.get("FDINVTYPE"));
				log.trace("previousdata: {}", CodeUtil.toJson(previousData));
			}

			String cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null));
			cusidn = new String(Base64.getDecoder().decode(cusidn));
			
			// 取新臺幣活期性存款帳戶
			List<String> acn_ssv1 = online_apply_service.getAccounts(cusidn, Base_Service.ACNO);
			// 取外幣活期性存款帳戶
			List<String> acn_fud1 = online_apply_service.getAccounts(cusidn, Base_Service.FX_DEMAND_DEPOSIT_ACCOUNT);
			// 取得使用者email
			String email = null;
			Object sessionE = SessionUtil.getAttribute(model, SessionUtil.DPMYEMAIL, null);
			log.debug(ESAPIUtil.vaildLog("SessionUtil.DPMYEMAIL >> "+sessionE.toString()));
			if(sessionE == null || "".equals(sessionE.toString()))
			{
				email = online_apply_service.getDPMYEMAIL(cusidn);
			}
			else
			{
				email = sessionE.toString();
			}
			
			bs.addData("ACN_SSV1", acn_ssv1);
			bs.addData("ACN_FUD1", acn_fud1);
			bs.addData("DPMYEMAIL", email);
			model.addAttribute("BaseResult", bs);
			model.addAttribute("FDINVTYPE", okMap.get("FDINVTYPE"));
			bs.setResult(true);
			bs.setMessage(ResultCode.SYS_SUCCESS, ResultCode.SYS_SUCCESS);
			
		}
		catch (Exception e) 
		{
			log.error("apply_trust_account_2 error", e);
		} 
		finally 
		{
			if(bs == null)
			{
				bs = new BaseResult();
			}
			if(bs.getResult())
			{
				target = "/online_apply/apply_trust_account_2";
			}
			else 
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * N361 線上開立基金戶 約定事項內容確認
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/apply_trust_account_confirm")
	public String apply_trust_account_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		log.debug("apply_trust_account_confirm start");
		log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+ reqParam));
		String target = "/online_apply/apply_trust_account_error";
		// 上一步路徑
		String previous = "/ONLINE/APPLY/apply_trust_account_2";
		// 下一步路徑
		String next = "/ONLINE/APPLY/apply_trust_account_result";
		
		BaseResult bs = new BaseResult();
		try 
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			//FATCA所需欄位
			model.addAttribute("FATCA_CONSENT", okMap.get("FATCA_CONSENT"));
			model.addAttribute("CITY", okMap.get("CITY"));
			
			boolean isback = "Y".equals(okMap.get("back"));
			if(isback == false)
			{
				// 回上一頁
				SessionUtil.addAttribute(model, SessionUtil.N361_2, okMap);
				log.debug("N361_2.json: {}", CodeUtil.toJson(SessionUtil.getAttribute(model, SessionUtil.N361_2, null)));
				
				// 儲存約定事項頁面資料，讓約定事項內容確認頁使用
				SessionUtil.addAttribute(model, SessionUtil.N361_4, okMap);
				
				// 儲存通知email
				String DPMYEMAIL = okMap.get("DPMYEMAIL") == null ? "" : okMap.get("DPMYEMAIL");
				SessionUtil.addAttribute(model, SessionUtil.DPMYEMAIL, DPMYEMAIL);
				okMap.put("DPMYEMAIL", DPMYEMAIL);
				
				// 將約定事項參數加入 JSON:DC
				Map<String, String> ikeyParams = (Map) SessionUtil.getAttribute(model, SessionUtil.N361_JSONDC, null);
				ikeyParams.putAll(okMap);
				ikeyParams.remove("ACN_SSV1");
				ikeyParams.remove("ACN_FUD1");
				SessionUtil.addAttribute(model, SessionUtil.N361_JSONDC, ikeyParams);
				log.debug(ESAPIUtil.vaildLog("N361_JSONDC >> {}"+ ikeyParams));
				
//20211126註解此段				
//				//20211108新增 在此頁先打 N322取得分數及投資屬性等級於頁面顯示並回存KYC PARAM
//				//獲取 中心計算之KYC分數及投資屬性
//				// KYC確認頁資料
//				Map<String, String> kycConfirmParams = (Map) SessionUtil.getAttribute(model, SessionUtil.N361_KYC_CONFIRM, null);
//				log.debug("get kycScore Params >> {}", kycConfirmParams);
//				String cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null));
//				cusidn = new String(Base64.getDecoder().decode(cusidn));
//				kycConfirmParams.put("CUSIDN", cusidn);
//				bs = fund_transfer_service.getKycScore(kycConfirmParams);
//				String SCORE =StrUtil.isNotEmpty((String) ((Map<String,Object>)bs.getData()).get("SCORE"))?(String) ((Map<String,Object>)bs.getData()).get("SCORE"):"";
//				String RISK =StrUtil.isNotEmpty((String) ((Map<String,Object>)bs.getData()).get("RISK"))?(String) ((Map<String,Object>)bs.getData()).get("RISK"):"";
//				kycConfirmParams.put("FDINVTYPE", RISK);
//				kycConfirmParams.put("FDSCORE", SCORE);
//				
//				SessionUtil.addAttribute(model, SessionUtil.N361_KYC_CONFIRM, kycConfirmParams);
//				
//				model.addAttribute("FDINVTYPE", RISK);
			}
			
			model.addAttribute("next", next);
			model.addAttribute("previous", previous);
			bs.setResult(true);
			bs.setMessage(ResultCode.SYS_SUCCESS, ResultCode.SYS_SUCCESS);
		}
		catch (Exception e) 
		{
			log.error("apply_trust_account_confirm error", e);
		} 
		finally 
		{
			if(bs == null)
			{
				bs = new BaseResult();
			}
			if(bs.getResult())
			{
				target = "/online_apply/apply_trust_account_confirm";
			}
			else 
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * N361 線上開立基金戶  結果頁 
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/apply_trust_account_result")
	public String apply_trust_account_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		log.debug("apply_trust_account_result start");
		log.debug(ESAPIUtil.vaildLog("交易機制參數 >> {}"+ reqParam));
		String target = "/online_apply/apply_trust_account_error";
		BaseResult bs = new BaseResult();
		try 
		{	
			Map<String,String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			//FATCA所需欄位
			model.addAttribute("FATCA_CONSENT", okMap.get("FATCA_CONSENT"));
			
			if ("N".equals(okMap.get("AGREE"))) {
				//不同意  更新txnCusInvAttrHist
				target = "forward:/ONLINE/APPLY/apply_trust_account_1";
				daoService.updateCusInvAttrHistAgree(okMap.get("FDHISTID"), okMap.get("AGREE"));
				return target;
			}
			String kyc_insert_hist_pass = (String) SessionUtil.getAttribute(model, SessionUtil.KYC_INSERT_HIST_PASS, null);		
			if(!okMap.containsKey("kyc_insert_uuid") || kyc_insert_hist_pass == null || !okMap.get("kyc_insert_uuid").equals(kyc_insert_hist_pass)) {
				bs.setResult(false);
				throw new Exception("kyc_insert_hist_pass check error");
			}
			String kyc_insert_hist_pass_finsh = (String) SessionUtil.getAttribute(model, SessionUtil.KYC_INSERT_HIST_PASS_FINSH, null);		
			if(!okMap.containsKey("kyc_insert_uuid") || okMap.get("kyc_insert_uuid").equals(kyc_insert_hist_pass_finsh)) {
				bs.setResult(false);
				throw new Exception("kyc_insert_hist_pass check error");
			}
			SessionUtil.addAttribute(model, SessionUtil.KYC_INSERT_HIST_PASS_FINSH, okMap.get("kyc_insert_uuid"));
			
			// KYC確認頁資料
			Map<String, String> kycConfirmParams = (Map) SessionUtil.getAttribute(model, SessionUtil.N361_KYC_CONFIRM, null);
			log.debug("kycConfirmParams >> {}", kycConfirmParams);
			
			//將ajax回來的FDINVTYPE放到kycConfirmParams 內去轉換xls需要的值
			kycConfirmParams.put("FDINVTYPE", okMap.get("FDINVTYPE"));
			
			// 約定事項資料
			Map<String, String> n361_4_params = (Map) SessionUtil.getAttribute(model, SessionUtil.N361_4, null);
			log.debug("n361_4_params >> {}", n361_4_params);
			
			// 約定事項
			Map<String, String> ikeyParams = (Map) SessionUtil.getAttribute(model, SessionUtil.N361_JSONDC, null);
			
			// 使用者統編
			String cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null));
			cusidn = new String(Base64.getDecoder().decode(cusidn));
			
			// E-MAIL
			String email = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.DPMYEMAIL, null));
			
			// N361電文資料
			Map<String, String> n361Params = new HashMap<>();
			// 加入 KYC 確認資料
			n361Params.putAll(kycConfirmParams);
			// 加入 約定事項確認資料
			n361Params.putAll(n361_4_params);
			// 交易機制參數
			n361Params.putAll(reqParam);
			n361Params.put("CUSIDN", cusidn);
			n361Params.put("DPMYEMAIL", email);
			
			n361Params.put("PCMAC", "");
			
			String devName = n361Params.get("DEVNAME");
			if (devName.length() > 30) {
				devName = devName.substring(0, 30);
			}
			log.info("devName.length() : " + devName.length());
			log.info(ESAPIUtil.vaildLog("devName : " + devName));
			n361Params.put("DEVNAME", devName);
			
			String aduserip = WebUtil.getIpAddr(request);
//			//TEST
//			aduserip = "172.22.11.23";
			n361Params.put("ADUSERIP", aduserip);
			
			//FATCA個人客戶身份識別聲明出生地城市
			n361Params.put("CITY", okMap.get("CITY"));
			
			//AMLCOD
			n361Params.put("AMLCOD", ikeyParams.get("AMLCOD"));
			
			// N361電文
			bs = online_apply_service.n361(n361Params);
			
			// 取得 KYC 選項描述i18n
			kycConfirmParams = online_apply_service.addKycOptionDescription(kycConfirmParams);
			SessionUtil.addAttribute(model, SessionUtil.N361_KYC_CONFIRM, kycConfirmParams);
			
			model.addAttribute("n361_8", bs);
		}
		catch (Exception e) 
		{
			log.error("apply_trust_account_result error", e);
		} 
		finally 
		{
			if(bs == null)
			{
				bs = new BaseResult();
			}
			if(bs.getResult())
			{
				target = "/online_apply/apply_trust_account_result";
			}
			else 
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	
	/**
	 * 線上註銷無卡提款
	 * 
	 * @param model
	 */
	@RequestMapping(value = "/logout_without_card")
	public String logout_without_card(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/online_apply/ErrorWithoutMenu";
		target = "/online_apply/logout_without_card";
		return target;
	}	
	/**
	 * 線上註銷無卡提款
	 * 
	 * @param model
	 */
	@PostMapping(value = "/logout_without_card_AcnCheck", produces = {"application/json;charset=UTF-8"})
	public  @ResponseBody BaseResult logout_without_card_AcnCheck(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace(ESAPIUtil.vaildLog("ReqParam >> {}"+reqParam));
		BaseResult bs = new BaseResult();
		try {
			//防止空值打N953
			if(reqParam.get("ACN")!=null){	
				bs = online_apply_service.logout_without_card_AcnCheck(reqParam);
				log.debug("Bsdata >> {}", bs.getData());
			}else {
				bs.setMsgCode("");
			}
		}catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("logout_without_card_AcnCheck error >> {}",e);
		}finally {

		}
		return bs;

	}
	/**
	 * 線上註銷無卡提款
	 * 
	 * @param model
	 */
	@IPVERIFY(cusidnKey="CUSIDN")
	@RequestMapping(value = "/logout_without_card_step1")
	public String logout_without_card_step1(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "";
		BaseResult bs = null;
		try {
			log.trace("start logout_without_card_step1");
			 bs = new BaseResult();
			 Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
				// 判斷LOCAL KEY 若有帶值表示切換語系
				boolean hasLocale = okMap.containsKey("locale");
				if(hasLocale)
				{
//					log.debug("locale >> {}", okMap.get("locale"));
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				}
				else
				{
					//清除 session
					SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
					//do something
					bs = online_apply_service.logout_without_card_step1(okMap);
					//存入session
					SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("logout_without_card_step1 error >> {}",e);
		} finally {
			if(bs.getResult()) {
				target = "/online_apply/logout_without_card_step1";
				model.addAttribute("logout_without_card_step1", bs);

			}else {
				target = "/online_apply/ErrorWithoutMenu";
				model.addAttribute(BaseResult.ERROR, bs);
			}
		
		}
		return target;
	}
	/**
	 * 線上註銷無卡提款
	 * 
	 * @param model
	 */
	@RequestMapping(value = "/logout_without_card_step2")
	public String logout_without_card_step2(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "";
		BaseResult bs = null;
		try {
			log.trace("start logout_without_card_step2");
			bs = new BaseResult();

			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 若有帶值表示切換語系
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale)
			{
//				log.debug("locale >> {}", okMap.get("locale"));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
			}
			else
			{
				//清除 session
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
				//do something
				bs = online_apply_service.logout_without_card_step2(okMap);
				//存入session
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("logout_without_card_step2 error >> {}",e);
		} finally {
			if(bs.getData()!=null && bs.getResult()) {
				target = "/online_apply/logout_without_card_step2";
				model.addAttribute("logout_without_card_step2", bs);
			}
			else {
				target = "/online_apply/ErrorWithoutMenu";
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * N580 線上註銷台灣Pay掃碼提款功能
	 * 
	 * @param model
	 */
	@RequestMapping(value = "/logout_taiwan_pay")
	public String logout_taiwan_pay(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/online_apply/ErrorWithoutMenu";
		target = "/online_apply/logout_taiwan_pay";
		return target;
	}	
	
	/**
	 * N580 線上註銷台灣Pay掃碼提款功能
	 * 
	 * @param model
	 */
	@IPVERIFY(cusidnKey="CUSIDN")
	@RequestMapping(value = "/logout_taiwan_pay_step1")
	public String logout_taiwan_pay_step1(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "";
		BaseResult bs = null;
		try {
			log.trace("start N580_step1");
			 bs = new BaseResult();
			 Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
				// 判斷LOCAL KEY 若有帶值表示切換語系
				boolean hasLocale = okMap.containsKey("locale");
				if(hasLocale)
				{
//					log.debug("locale >> {}", okMap.get("locale"));
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				}
				else
				{
					//清除 session
					SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
					//do something
					bs = online_apply_service.logout_taiwan_pay_step1(okMap);
					//存入session
					SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("logout_taiwan_pay_step1 error >> {}",e);
		} finally {
			if(bs.getResult()) {
				target = "/online_apply/logout_taiwan_pay_1";
				model.addAttribute("N580_step1", bs);

			}else {
				target = "/online_apply/ErrorWithoutMenu";
				model.addAttribute(BaseResult.ERROR, bs);
			}
		
		}
		return target;
	}
	
	
	/**
	 * N580 線上註銷台灣Pay掃碼提款功能
	 * 
	 * @param model
	 */
	@RequestMapping(value = "/logout_taiwan_pay_step2")
	public String logout_taiwan_pay_step2(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "";
		BaseResult bs = null;
		try {
			log.trace("start N580_step2");
			 bs = new BaseResult();
			 Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
				// 判斷LOCAL KEY 若有帶值表示切換語系
				boolean hasLocale = okMap.containsKey("locale");
				if(hasLocale)
				{
//					log.debug("locale >> {}", okMap.get("locale"));
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				}
				else
				{
					//清除 session
					SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
					//do something
					bs = online_apply_service.logout_taiwan_pay_step2(okMap);
					//存入session
					SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("logout_taiwan_pay_step2 error >> {}",e);
		} finally {
			if(bs.getResult()) {
				target = "/online_apply/logout_taiwan_pay_2";
				model.addAttribute("N580_step2", bs);

			}else {
				target = "/online_apply/ErrorWithoutMenu";
				model.addAttribute(BaseResult.ERROR, bs);
			}
		
		}
		return target;
	}
	
	/**
	 * N201 線上預約開立存款戶
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/apply_deposit_account")
	public String apply_deposit_account(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		return "/online_apply/apply_deposit_account";
	}
	
	/**
	 * N201 線上預約開立存款戶
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/apply_deposit_account_p1")
	public String apply_deposit_account_p1(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		SessionUtil.addAttribute(model, SessionUtil.N201_CONFIRM_DATA, new HashMap<String,String>());
		log.trace("N201_CONFIRM_DATA>>{}",(Map<String,String>)SessionUtil.getAttribute(model, SessionUtil.N201_CONFIRM_DATA, null));
		SessionUtil.addAttribute(model, SessionUtil.N201_CONFIRM_DATA_2, new HashMap<String,String>());
		log.trace("N201_CONFIRM_DATA_2>>{}",(Map<String,String>)SessionUtil.getAttribute(model, SessionUtil.N201_CONFIRM_DATA_2, null));
		SessionUtil.addAttribute(model, SessionUtil.N201_CONFIRM_DATA_3, new HashMap<String,String>());
		log.trace("N201_CONFIRM_DATA_3>> {}",(Map<String,String>)SessionUtil.getAttribute(model, SessionUtil.N201_CONFIRM_DATA_3, null));
		return "/online_apply/apply_deposit_account_p1";
	}
	
	/**
	 * N201 線上預約開立存款戶
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/apply_deposit_account_p2")
	public String apply_deposit_account_p2(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/online_apply/ErrorWithoutMenu";
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String back = reqParam.get("back") == null ? "" : reqParam.get("back");
			log.debug(ESAPIUtil.vaildLog("back >> {}"+ back));
			if (hasLocale || back.equals("Y"))
			{
				//檢測SessionUtil.N201_CONFIRM_DATA是否已被清空
				Boolean has_session = getSessionHas(SessionUtil.N201_CONFIRM_DATA,"CUSIDN",model);
				
				if(has_session) {
					reqParam = (Map<String,String>)SessionUtil.getAttribute(model, SessionUtil.N201_CONFIRM_DATA, null);
					bs.setResult(true);
				}else {
					bs.setErrorMessage("", i18n.getMsg("LB.X1779"));
				}
			}else {
				bs.setResult(true);
			}
			model.addAttribute("bh_data", online_apply_service.getAllBh());
			model.addAttribute("city_data", online_apply_service.getCity());
			model.addAttribute("today", DateUtil.getTWDate(""));
		}catch(Exception e) {
			log.error("apply_digital_account_p3 error >> {}", e.toString());
			
		}finally {
			if(bs!=null && bs.getResult()) {
				model.addAttribute("result_data", reqParam);
				target = "/online_apply/apply_deposit_account_p2";
			}else {
				bs.setPrevious("/online_apply/apply_deposit_account_p1");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * N201 線上預約開立存款戶
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@IPVERIFY(cusidnKey="CUSIDN")
	@RequestMapping(value = "/apply_deposit_account_p3")
	public String apply_deposit_account_p3(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/online_apply/ErrorWithoutMenu";
		BaseResult bs = null;
		try{
			bs = new BaseResult();
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String back = reqParam.get("back") == null ? "" : reqParam.get("back");
			log.debug(ESAPIUtil.vaildLog("back >> {}"+ back));
			if (hasLocale || back.equals("Y"))
			{
				//檢測SessionUtil.N201_CONFIRM_DATA是否已被清空
				Boolean has_session = getSessionHas(SessionUtil.N201_CONFIRM_DATA,"CUSIDN",model);
				if(has_session) {
					reqParam = (Map<String,String>)SessionUtil.getAttribute(model, SessionUtil.N201_CONFIRM_DATA, null);
					model.addAttribute("CUSIDN", reqParam.get("CUSIDN"));
					bs.setResult(true);
				}else {
					bs.setErrorMessage("", i18n.getMsg("LB.X1779"));
				}
			}
			else
			{
				SessionUtil.addAttribute(model, SessionUtil.N201_CONFIRM_DATA, reqParam);
				model.addAttribute("CUSIDN", okMap.get("CUSIDN"));
				bs.setResult(true);
			}
			Map<String,String> old_data_2 = (Map<String,String>)SessionUtil.getAttribute(model, SessionUtil.N201_CONFIRM_DATA_2, null);
			if(old_data_2 != null)
				if(!old_data_2.isEmpty())
					reqParam.putAll(old_data_2);
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("apply_deposit_account_p3 error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				model.addAttribute("result_data", reqParam);
				target = "/online_apply/apply_deposit_account_p3";
			}else {
				bs.setPrevious("/online_apply/apply_deposit_account_p1");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	

	/**
	 * N201 線上預約開立存款戶
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/apply_deposit_account_p4")
	public String apply_deposit_account_p4(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/online_apply/ErrorWithoutMenu";
		//防止時間過長session被清空
		try
		{
			Boolean has_session = getSessionHas(SessionUtil.N201_CONFIRM_DATA,"CUSIDN",model);
			if(has_session) {
		        // 解決Trust Boundary Violation 
		        Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam); 
		        //塞入P2資料
				Map<String,String> inputData = (Map<String,String>)SessionUtil.getAttribute(model, SessionUtil.N201_CONFIRM_DATA, null);

		        //儲存P3資料
				SessionUtil.addAttribute(model, SessionUtil.N201_CONFIRM_DATA_2, okMap);
		        //塞入P3資料
				inputData.putAll(okMap);
		        //塞入P4資料
				Map<String,String> old_data_3 = (Map<String,String>)SessionUtil.getAttribute(model, SessionUtil.N201_CONFIRM_DATA_3, null);
				if(old_data_3 != null)
					if(!old_data_3.isEmpty())
						inputData.putAll(old_data_3);

		        log.debug(ESAPIUtil.vaildLog("okMap>>{}"+okMap));
		        log.debug(ESAPIUtil.vaildLog("reqParam>>{}"+inputData));
		        log.debug(ESAPIUtil.vaildLog("inputData>>{}"+inputData));
				BaseResult bs = new BaseResult();
				try {
					bs = online_apply_service.N960_REST_UPDATE_CUSNAME(inputData);
					if(bs.getMsgCode().equals("E002")) {
						inputData.put("checknb", "N");
					}else {
						inputData.put("checknb", "Y");
					}
				}catch(Exception e) {
					log.error(e.toString());
				}finally{
					model.addAttribute("input_data", inputData);
					target =  "/online_apply/apply_deposit_account_p4";
				}
			}else {
				BaseResult bs = new BaseResult();
				bs.setErrorMessage("", i18n.getMsg("LB.X1779"));
				//新增回上一頁的路徑
				bs.setPrevious("/ONLINE/APPLY/apply_deposit_account");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("apply_deposit_account_p4 error >> {}",e);
		}
		return target;

	}
	
	/**
	 * N201 線上預約開立存款戶
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/apply_deposit_account_p5")
	public String apply_deposit_account_p5(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/online_apply/ErrorWithoutMenu";
		//防止時間過長session被清空
		Boolean has_session = getSessionHas(SessionUtil.N201_CONFIRM_DATA,"CUSIDN",model);
		if(has_session) {
			BaseResult bs = null;
			try {
				bs = new BaseResult();
	
				// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
				bs = online_apply_service.getTxToken();
				log.trace("electricity_fee.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
				if(bs.getResult()) {
					SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
				}
		        // 解決Trust Boundary Violation 
		        Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam); 
		        //儲存P4資料
				SessionUtil.addAttribute(model, SessionUtil.N201_CONFIRM_DATA_3, okMap);
				
				//塞入P2資料
		        Map<String,String> inputData = (Map<String,String>)SessionUtil.getAttribute(model, SessionUtil.N201_CONFIRM_DATA, null);
		        //塞入P3資料
		        Map<String,String> inputData2 = (Map<String,String>)SessionUtil.getAttribute(model, SessionUtil.N201_CONFIRM_DATA_2, null);
		        inputData.putAll(inputData2);		
		        //塞入P4資料
		        inputData.putAll(okMap);			
		        inputData.put("TOKEN", ((Map<String,String>) bs.getData()).get("TXTOKEN"));
		        
		        log.debug(ESAPIUtil.vaildLog("reqParam>>{}"+inputData));
				model.addAttribute("input_data", inputData);
			}catch(Exception e) {
				log.error(e.toString());
			}
			target = "/online_apply/apply_deposit_account_p5";
		}else {
			BaseResult bs = new BaseResult();
			bs.setErrorMessage("", i18n.getMsg("LB.X1779"));
			//新增回上一頁的路徑
			bs.setPrevious("/ONLINE/APPLY/apply_deposit_account");
			model.addAttribute(BaseResult.ERROR, bs);
		}
		return target;
	}
	
	/**
	 * N201 線上預約開立存款戶
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/apply_deposit_account_result")
	public String apply_deposit_account_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {		
		String target = "/online_apply/ErrorWithoutMenu";
		
		BaseResult bs = null;
		log.trace("apply_deposit_account_result>>");
		Map<String, String> okMap = null;
		try {
			
			bs = new BaseResult();
			okMap = ESAPIUtil.validStrMap(reqParam);
//			okMap = reqParam;
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
//				log.trace("locale>>" + okMap.get("locale"));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.N201_CONFIRM_DATA, null);
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				log.trace("apply_deposit_account_result.validate TXTOKEN...");
				log.trace("apply_deposit_account_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("other_telecom_fee_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_telecom_fee_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_telecom_fee_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						!okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("other_telecom_fee_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("other_telecom_fee_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+reqParam));
				String separ = File.separator;
				String uploadPath_N201 = context.getRealPath("") + separ +"com" + separ + "updateBytes" + separ;
				bs = online_apply_service.apply_deposit_account_result(okMap, uploadPath_N201);
				if(bs.getResult()) {

					Map<String, String> sesstiondata = (Map<String,String>)SessionUtil.getAttribute(model, SessionUtil.UPDATE_IMAGE_DATA, null);
					if(sesstiondata == null) {
						sesstiondata = new HashMap<String,String>();
					}
					if(!okMap.get("FILE1").equals("") && sesstiondata.containsKey(okMap.get("FILE1"))) {
						String randomName = sesstiondata.get(okMap.get("FILE1"));
						String result = update_file_service.readBinryFile(uploadPath_N201, randomName);
						if(!result.equals("fail")) {
							String filetype = okMap.get("FILE1").substring(okMap.get("FILE1").lastIndexOf(".") ,okMap.get("FILE1").length());
							update_file_service.uploadImage(result,  okMap.get("UID") + "_01" + filetype);
							update_file_service.movefile(result, SpringBeanFactory.getBean("imgMovePath") + separ, okMap.get("UID") + "_01_" + DateUtil.getDate("") + filetype);
							update_file_service.removefile(uploadPath_N201, randomName);
						}
					}
					if(!okMap.get("FILE2").equals("") && sesstiondata.containsKey(okMap.get("FILE2"))) {
						String randomName = sesstiondata.get(okMap.get("FILE2"));
						String result = update_file_service.readBinryFile(uploadPath_N201, randomName);
						if(!result.equals("fail")) {
							String filetype = okMap.get("FILE2").substring(okMap.get("FILE2").lastIndexOf(".") ,okMap.get("FILE2").length());
							update_file_service.uploadImage(result,  okMap.get("UID") + "_02" + filetype);
							update_file_service.movefile(result, SpringBeanFactory.getBean("imgMovePath") + separ, okMap.get("UID") + "_02_" + DateUtil.getDate("") + filetype);
							update_file_service.removefile(uploadPath_N201, randomName);
						}
					}
					if(!okMap.get("FILE3").equals("") && sesstiondata.containsKey(okMap.get("FILE3"))) {
						String randomName = sesstiondata.get(okMap.get("FILE3"));
						String result = update_file_service.readBinryFile(uploadPath_N201, randomName);
						if(!result.equals("fail")) {
							String filetype = okMap.get("FILE1").substring(okMap.get("FILE3").lastIndexOf(".") ,okMap.get("FILE3").length());
							update_file_service.uploadImage(result,  okMap.get("UID") + "_03" + filetype);
							update_file_service.movefile(result, SpringBeanFactory.getBean("imgMovePath") + separ, okMap.get("UID") + "_03_" + DateUtil.getDate("") + filetype);
							update_file_service.removefile(uploadPath_N201, randomName);
						}
					}
					if(!okMap.get("FILE4").equals("") && sesstiondata.containsKey(okMap.get("FILE4"))) {
						String randomName = sesstiondata.get(okMap.get("FILE4"));
						String result = update_file_service.readBinryFile(uploadPath_N201, randomName);
						if(!result.equals("fail")) {
							String filetype = okMap.get("FILE4").substring(okMap.get("FILE4").lastIndexOf(".") ,okMap.get("FILE4").length());
							update_file_service.uploadImage(result,  okMap.get("UID") + "_04" + filetype);
							update_file_service.movefile(result, SpringBeanFactory.getBean("imgMovePath") + separ, okMap.get("UID") + "_04_" + DateUtil.getDate("") + filetype);
							update_file_service.removefile(uploadPath_N201, randomName);
						}
					}
				}
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.N201_CONFIRM_DATA, okMap);
				
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("apply_deposit_account_result error >> {}",e);
		} finally {
			log.debug(ESAPIUtil.vaildLog("okMap>>{}"+okMap));
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TOKEN"));
			if (bs != null && bs.getResult())
			{
				target = "online_apply/apply_deposit_account_result";
				model.addAttribute("result_data", bs);
				model.addAttribute("input_data", okMap);
			}
			else
			{
				//新增回上一頁的路徑
				bs.setPrevious("/ONLINE/APPLY/apply_deposit_account_p1");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * N201 線上預約開立存款戶
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/apply_deposit_account_job_name")
	public String apply_deposit_account_job_name(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		return "/online_apply/apply_deposit_account_job_name";
	}
	
	@PostMapping("/apply_deposit_account_get_area")
	public @ResponseBody BaseResult apply_deposit_account_get_area(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		BaseResult bs = null;
		bs = online_apply_service.getArea(reqParam.get("CITY"));
		model.addAttribute("bs",bs);
		return bs;
	}
	
	@PostMapping("/apply_deposit_account_get_zip_code")
	public @ResponseBody BaseResult apply_deposit_account_get_zip_code(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		BaseResult bs = null;
		bs = online_apply_service.getZipCodeByCityArea(reqParam.get("CITY"),reqParam.get("AREA"));
		model.addAttribute("bs",bs);
		return bs;
	}
	
	@PostMapping("/apply_deposit_account_get_zip_bh")
	public @ResponseBody BaseResult apply_deposit_account_get_zip_bh(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		BaseResult bs = null;
		bs = online_apply_service.getZipBh(reqParam.get("zip"));
		model.addAttribute("bs",bs);
		return bs;
	}
	
	@PostMapping("/apply_deposit_account_get_zip_fileupload")
    public @ResponseBody BaseResult apply_deposit_account_get_zip_fileupload(HttpServletRequest request, HttpServletResponse response, @RequestParam("picFile") MultipartFile picFile, @RequestParam Map<String, String> reqParam, Model model) {
    	BaseResult bs = new BaseResult();
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//    	String uploadPath_N201 = request.getSession().getServletContext().getRealPath("/com/updateTmp/") + "";
		String separ = File.separator;
		String uploadPath_N201 = context.getRealPath("") + separ +"com" + separ + "updateTmp" + separ;
    	log.debug("update_Path >> {}",uploadPath_N201);
    	//圖示允許的類型
    	String[] allowImageType = {"image/jpeg","image/png","image/jpg"};
		//圖示允許的長寬
		int allowImageWidth = 640;
		int allowImageHeight = 480;
//		//圖示允許的檔案大小
//		int allowImageSize = 3300 * 1024;//3300KB
		//20220223更新 圖示允許的檔案大小放大成5mb by Ben ( 經查找只有信用卡申請使用這個ajax , 故直接改不會影響到其他功能 
		int allowImageSize = 5120 * 1024;//5120KB
		String contentType;
		String originalFilename;
		try {
            log.debug(ESAPIUtil.vaildLog(okMap.get("oldPath")));
			if(!okMap.get("oldPath").equals("")) {
				online_apply_service.removefile( uploadPath_N201 , okMap.get("oldPath"), "", "");
			}
		}catch(Exception e) {
			log.error(e.toString());
		}
        try {
            MultipartFile multipartFile = picFile;
            log.debug(ESAPIUtil.vaildLog(okMap.get("type")));

			String fileTyle = multipartFile.getOriginalFilename().substring(multipartFile.getOriginalFilename().lastIndexOf("."),multipartFile.getOriginalFilename().length()).toLowerCase(); 
            String fileName = okMap.get("CUSIDN") + "_" + okMap.get("type") + fileTyle;
            try {
            	String filetime = okMap.get("filetime");
            	if(StrUtil.isNotEmpty(filetime)) {
            		fileName = okMap.get("type") + okMap.get("filetime") + fileTyle;
            	}
            }
            catch (Exception e) {
				// TODO: handle exception
			}
            log.debug(ESAPIUtil.vaildLog("fileName={}"+ fileName));

//            String fileContent = new String(multipartFile.getBytes());
//            String[] lines = fileContent.split("\\r?\\n");
//			String filePath = uploadPath_N201 + fileName;
//			log.trace("filePath >> {}",filePath);
//			multipartFile.transferTo(new File(filePath));
            bs.addAllData(online_apply_service.checkImage(	multipartFile,
															multipartFile.getOriginalFilename(),
															multipartFile.getContentType(),
															allowImageType,
															allowImageWidth,
															allowImageHeight,
															allowImageSize,
															uploadPath_N201, fileName));

//            online_apply_service.uploadImage(multipartFile);
//            log.debug("fileContent >> {}", fileContent);
//            bs.addData("filePath", fileContent);
            if( ((Map<String,Object>)bs.getData()).get("result").equals("TRUE") ) {
                bs.addData("validated", Boolean.TRUE);
            }else {
                bs.addData("validated", Boolean.FALSE);
            }
            bs.addData("url", fileName);
        } catch (Exception e) {
            log.error("saveFile Error", e);
            Map<String, String> errors = new HashMap<String, String>();
            bs.addData("summary", i18n.getMsg("LB.X2131"));
            bs.addData("validated", Boolean.FALSE);

        }
        return bs;
    }	

	@PostMapping("/apply_deposit_account_get_zip_fileupload_only_jpg")
    public @ResponseBody BaseResult apply_deposit_account_get_zip_fileupload_only_jpg(HttpServletRequest request, HttpServletResponse response, @RequestParam("picFile") MultipartFile picFile, @RequestParam Map<String, String> reqParam, Model model) {
    	log.info("apply_deposit_account_get_zip_fileupload_only_jpg");
		BaseResult bs = new BaseResult();
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//    	String uploadPath_N201 = request.getSession().getServletContext().getRealPath("/com/updateTmp/") + "";
		String separ = File.separator;
		String uploadPath_N201 = context.getRealPath("") + separ +"com" + separ + "updateTmp" + separ;
    	log.debug("update_Path >> {}",uploadPath_N201);
    	//圖示允許的類型
		String[] allowImageType = {"image/jpeg","image/jpg","image/pjpeg"};
		//圖示允許的長寬
		int allowImageWidth = 640;
		int allowImageHeight = 480;
		//圖示允許的檔案大小
		int allowImageSize = 3300 * 1024;//3300KB
		String contentType;
		String originalFilename;
		try {
            log.debug(ESAPIUtil.vaildLog(okMap.get("oldPath")));
			if(!okMap.get("oldPath").equals("")) {
				online_apply_service.removefile( uploadPath_N201 , okMap.get("oldPath"), "", "");
			}
		}catch(Exception e) {
			log.error(e.toString());
		}
        try {
            MultipartFile multipartFile = picFile;
            log.debug(ESAPIUtil.vaildLog(okMap.get("type")));

			String fileTyle = multipartFile.getOriginalFilename().substring(multipartFile.getOriginalFilename().lastIndexOf("."),multipartFile.getOriginalFilename().length()).toLowerCase(); 
            String fileName = okMap.get("CUSIDN") + "_" + okMap.get("type") + fileTyle;
            try {
            	String filetime = okMap.get("filetime");
            	if(StrUtil.isNotEmpty(filetime)) {
            		fileName = okMap.get("type") + okMap.get("filetime") + fileTyle;
            	}
            }
            catch (Exception e) {
				// TODO: handle exception
			}
            log.info(ESAPIUtil.vaildLog("fileName={}"+ fileName));

//            String fileContent = new String(multipartFile.getBytes());
//            String[] lines = fileContent.split("\\r?\\n");
//			String filePath = uploadPath_N201 + fileName;
//			log.trace("filePath >> {}",filePath);
//			multipartFile.transferTo(new File(filePath));
            bs.addAllData(online_apply_service.checkImageOnlyImg(	multipartFile,
															multipartFile.getOriginalFilename(),
															multipartFile.getContentType(),
															allowImageType,
															allowImageWidth,
															allowImageHeight,
															allowImageSize,
															uploadPath_N201, fileName));

//            online_apply_service.uploadImage(multipartFile);
//            log.debug("fileContent >> {}", fileContent);
//            bs.addData("filePath", fileContent);
            if( ((Map<String,Object>)bs.getData()).get("result").equals("TRUE") ) {
                bs.addData("validated", Boolean.TRUE);
            }else {
                bs.addData("validated", Boolean.FALSE);
            }
            bs.addData("url", fileName);
        } catch (Exception e) {
            log.error("saveFile Error", e);
            Map<String, String> errors = new HashMap<String, String>();
            bs.addData("summary", i18n.getMsg("LB.X2131"));
            bs.addData("validated", Boolean.FALSE);

        }
        return bs;
    }	
	
	@PostMapping("/apply_deposit_account_fileupload_base64")
    public @ResponseBody BaseResult apply_deposit_account_get_zip_fileupload_only_jpg(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {

		BaseResult bs = new BaseResult();
		try
        {
			String separ = File.separator;
			String uploadPath_N201 = context.getRealPath("") + separ +"com" + separ + "updateBytes" + separ;
	    	log.debug("update_Path >> {}",uploadPath_N201);
        	String picFile = reqParam.get("picFile");
        	log.trace("picFile >> {}", picFile);

        	//圖示允許的類型
    		String[] allowImageType = {"image/jpeg","image/jpg","image/pjpeg", "image/png", "image/heic", "image/heif", "image/tiff"};
    		//圖示允許的長寬
    		int allowImageWidth = 640;
    		int allowImageHeight = 480;
    		//圖示允許的檔案大小
    		int allowImageSize = 3300 * 1024;//3300KB
    		
        	Map<String,String> result = update_file_service.checkImageBase64(picFile, allowImageType, allowImageWidth, allowImageHeight, allowImageSize);
        	
			Map<String, String> sesstiondata = (Map<String,String>)SessionUtil.getAttribute(model, SessionUtil.UPDATE_IMAGE_DATA, null);
			if(sesstiondata == null) {
				sesstiondata = new HashMap<String,String>();
			}
            String fileName = reqParam.get("CUSIDN") + "_" + reqParam.get("type") + ".jpg";
            try {
            	String filetime = reqParam.get("filetime");
            	if(StrUtil.isNotEmpty(filetime)) {
            		fileName = reqParam.get("type") + reqParam.get("filetime") + ".jpg";
            	}
            }
            catch (Exception e) {
				// TODO: handle exception
			}
            String randomName = UUID.randomUUID().toString();
            sesstiondata.put(fileName, randomName);
			SessionUtil.addAttribute(model, SessionUtil.UPDATE_IMAGE_DATA, sesstiondata);
            if( result.get("result").equals("TRUE") ) {
            	String result2 = update_file_service.writeBinryFile(result.get("base64"), uploadPath_N201, randomName);
            	if(result2.equals("sesscue")) {
            		bs.addData("validated", Boolean.TRUE);
            	}else {
                    bs.addData("validated", Boolean.FALSE);
                    result.put("summary",i18n.getMsg("LB.X2131"));
            	}
            }else {
                bs.addData("validated", Boolean.FALSE);
            }
            bs.addData("url", fileName);
            bs.addData("summary", result.get("summary"));
        }
        catch(Exception e)
        {
        	log.error("{}", e);
        }
        return bs;
	}
	
	@PostMapping("/apply_deposit_account_get_image")
	public @ResponseBody BaseResult apply_deposit_account_get_image(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {

		BaseResult bs = new BaseResult();
		String separ = File.separator;
		String uploadPath_N201 = context.getRealPath("") + separ +"com" + separ + "updateBytes" + separ;
    	log.debug("update_Path >> {}",uploadPath_N201);
		try
        {
			log.info("apply_deposit_account_get_image start");
			Map<String, String> sesstiondata = (Map<String,String>)SessionUtil.getAttribute(model, SessionUtil.UPDATE_IMAGE_DATA, null);
			if(sesstiondata == null) {
				sesstiondata = new HashMap<String,String>();
			}
			if(sesstiondata.containsKey(reqParam.get("filename"))) {
				String randomName = sesstiondata.get(reqParam.get("filename"));
				String result = update_file_service.readBinryFile(uploadPath_N201, randomName);
				if(result.equals("fail")) {
					log.error("no serch data : {}", reqParam.get("filename"));
					bs.addData("validated", Boolean.FALSE);
					bs.addData("summary",i18n.getMsg("LB.X2131"));
				}else {
					bs.addData("validated", Boolean.TRUE);
					bs.addData("picFile", "data:image/jpeg;base64," + result);
					bs.addData("summary", "");
				}
			}else {
				log.error("no serch data : {}", reqParam.get("filename"));
				bs.addData("validated", Boolean.FALSE);
				bs.addData("summary",i18n.getMsg("LB.X2131"));
			}
//			Map<String, String> sesstiondata = (Map<String,String>)SessionUtil.getAttribute(model, SessionUtil.UPDATE_IMAGE_DATA, null);
//			if(sesstiondata == null) {
//				sesstiondata = new HashMap<String,String>();
//			}
//			if(sesstiondata.containsKey(reqParam.get("filename"))) {
//                bs.addData("validated", Boolean.TRUE);
//                bs.addData("picFile", sesstiondata.get(reqParam.get("filename")));
//                bs.addData("summary", "");
//			}else {

//			}
        }
        catch(Exception e)
        {
        	log.error("{}", e);
            bs.addData("validated", Boolean.FALSE);
			bs.addData("summary",i18n.getMsg("LB.X2131"));
        }
        return bs;
	}
	
	@PostMapping("/apply_deposit_account_fileupload_delete")
	public @ResponseBody BaseResult apply_deposit_account_fileupload_delete(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = new BaseResult();
		String separ = File.separator;
		String uploadPath_N201 = context.getRealPath("") + separ + "com" + separ + "updateTmp" + separ;
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
            log.debug(ESAPIUtil.vaildLog(okMap.get("Path")));
			if(!okMap.get("Path").equals("")) {
				online_apply_service.removefile( uploadPath_N201 , okMap.get("Path"), "", "");
			}
			bs.addData("sessulte", true);
		}catch(Exception e) {
			log.error(e.toString());
		}
        return bs;
	}
	
	@PostMapping("/apply_deposit_account_fileupload_delete_base64")
	public @ResponseBody BaseResult apply_deposit_account_fileupload_delete_base64(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = new BaseResult();
		String separ = File.separator;
		String uploadPath_N201 = context.getRealPath("") + separ + "com" + separ + "updateBytes" + separ;
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
            log.debug(ESAPIUtil.vaildLog(okMap.get("Path")));
			if(!okMap.get("Path").equals("")) {
				Map<String, String> sesstiondata = (Map<String,String>)SessionUtil.getAttribute(model, SessionUtil.UPDATE_IMAGE_DATA, null);
				if(sesstiondata == null) {
					sesstiondata = new HashMap<String,String>();
				}
				if(sesstiondata.containsKey(reqParam.get("Path"))) {
					String randomName = sesstiondata.get(reqParam.get("Path"));
					boolean result = update_file_service.removefile(uploadPath_N201, randomName);
					if(result) {
						bs.addData("sessulte", true);
						bs.addData("summary", "");
					}else {
						log.error("no serch data : {}", reqParam.get("Path"));
						bs.addData("sessulte", false);
						bs.addData("summary",i18n.getMsg("LB.X2131"));
					}
					
				}else {
					bs.addData("sessulte", false);
					bs.addData("summary",i18n.getMsg("LB.X2131"));
				}
			}
		}catch(Exception e) {
			log.error(e.toString());
			bs.addData("sessulte", false);
			bs.addData("summary",i18n.getMsg("LB.X2131"));
		}
        return bs;
	}
    
    /**
	 * NA30 使用臺灣企銀晶片金融卡+讀卡機
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/use_component")
	public String use_component(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target ="/online_apply/ErrorWithoutMenu";
		try {
			log.trace(ESAPIUtil.vaildLog("reqParam >> "+reqParam));
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("use_component error >> {}",e);
		}
		finally {
			target="/online_apply/use_component";
		}
		return target;
	}
	@RequestMapping(value = "/use_component_step1")
	public String use_component_step1(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target ="/online_apply/ErrorWithoutMenu";
		BaseResult bs = new BaseResult();
		try {
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
//				log.trace("locale>>" + okMap.get("locale"));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
			}else {
				log.trace(ESAPIUtil.vaildLog("reqParam >> "+reqParam));
				bs = online_apply_service.use_component_step1(reqParam);
				log.trace("use_component_step1.data >> "+CodeUtil.toJson(bs));
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("use_component_step1 error >> {}",e);
		}
		finally {
			if(bs!=null && bs.getResult()) {
				target="/online_apply/use_component_step1";
				model.addAttribute("use_component_step1",bs);
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		return target;
	}
	@PostMapping(value = "/use_component_step1_idcheck",produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult use_component_step1_idcheck(HttpServletRequest request, HttpServletResponse response,
			 Map<String, String> reqParam, Model model) {
		
		Map<String,String[]>originreq=request.getParameterMap();
		Map<String,String> reqmap=new HashMap<String,String>();
		BaseResult bs = null;
		String result="";
		try {	
			bs=new BaseResult();
			reqmap.put("CUSIDN",TakeOfCoat(originreq.get("CUSIDN")));
			reqmap.put("NCHECKNB", "Y");
			log.trace("ReqMap >> {}",reqmap);
			bs = online_apply_service.use_component_step1_idcheck(reqmap);
			log.debug("result >> {}", result);
		}catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("use_component_step1_idcheck error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				
			}
		}
		return bs;

	}
	@RequestMapping(value = "/use_component_ARTICLE")
	public String use_component_ARTICLE(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target ="/online_apply/ErrorWithoutMenu";
		try {
			log.trace(ESAPIUtil.vaildLog("reqParam >> "+reqParam));
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("use_component_ARTICLE error >> {}",e);
		}
		finally {
			target="/online_apply/use_component_ARTICLE";
		}
		return target;
	}
	@PostMapping(value = "/use_component_step1_AUTH",produces = { "application/json;charset=UTF-8" })
	public @ResponseBody String use_component_step1_AUTH(HttpServletRequest request, HttpServletResponse response,
			 Map<String, String> reqParam, Model model) {
		
		Map<String,String[]>originreq=request.getParameterMap();
		Map<String,String> reqmap=new HashMap<String,String>();
		BaseResult bs = null;
		String result="";
		try {	
			bs=new BaseResult();
			
			reqmap.put("CUSIDN",TakeOfCoat(originreq.get("CUSIDN")));
			log.trace("ReqMap >> {}",reqmap);
			result = online_apply_service.use_component_step1_AUTH(reqmap);
			log.debug(ESAPIUtil.vaildLog("result >> {}"+ result));
		}catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("use_component_step1_AUTH error >> {}",e);
		}finally {

		}
		return result;

	}
	
	@IPVERIFY(cusidnKey="CUSIDN")
	@RequestMapping(value = "/use_component_confirm")
	public String use_component_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = new BaseResult();
		String target ="/online_apply/ErrorWithoutMenu";
		try {
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
//				log.trace("locale>>" + okMap.get("locale"));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
			}else {
				// 解決Trust Boundary Violation
				String userip = ESAPIUtil.validInput(WebUtil.getIpAddr(request),"GeneralString",true) ; // 登入 IP-Address
				bs = online_apply_service.use_component_confirm(okMap);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		}catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("use_component_confirm error >> {}",e);
		}finally{
			if(bs!=null && bs.getResult()) {
				target="/online_apply/use_component_confirm";
				model.addAttribute("use_component_confirm",bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	@RequestMapping(value = "/use_component_result")
	public String use_component_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target ="/online_apply/ErrorWithoutMenu";
		BaseResult bs = new BaseResult();
		try {
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
//				log.trace("locale>>" + okMap.get("locale"));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
			}else {
				String userip = ESAPIUtil.validInput(WebUtil.getIpAddr(request),"GeneralString",true) ; // 登入 IP-Address
				reqParam.put("USERIP", userip);
				bs = online_apply_service.use_component_result(reqParam);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		}catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("use_component_result error >> {}",e);
		}finally{
			if(bs!=null && bs.getResult()) {
				target="/online_apply/use_component_result";
				model.addAttribute("use_component_result",bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}

		}
		return target;
	}
	
	/**
	 * NA40 使用臺灣企銀信用卡
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/use_creditcard")
	public String use_creditcard(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		try {
			bs = new  BaseResult();
//			20191206 by hugo from Cleartext Submission of    Sensitive Information
//			bs.addData("USERIP",ESAPIUtil.validInput(WebUtil.getIpAddr(request),"GeneralString",true));
			model.addAttribute("use_creditcard",bs);
		}
		catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("use_creditcard error >> {}",e);
		}
		log.trace("BSDATA>>>>>{}",bs.getData());
		return "/online_apply/use_creditcard";
	}
	
	/**
	 * NA40 使用臺灣企銀信用卡
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/use_creditcard_step1")
	public String use_creditcard_step1(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace(ESAPIUtil.vaildLog("use_creditcard_step1 reqParam>>>>>{}"+reqParam));
		BaseResult bs = null;
		try {
			bs = new  BaseResult();
//			20191206 by hugo from Cleartext Submission of    Sensitive Information
//			bs.addData("USERIP",ESAPIUtil.validInput(WebUtil.getIpAddr(request),"GeneralString",true));
			model.addAttribute("use_creditcard_step1",bs);
		}
		catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("use_creditcard_step1 error >> {}",e);
		}
		log.trace("BSDATA>>>>>{}",bs.getData());
		return "/online_apply/use_creditcard_step1";
	}
	
	/**
	 * NA40 使用臺灣企銀信用卡
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@IPVERIFY(cusidnKey="_CUSIDN")
	@RequestMapping(value = "/use_creditcard_step2")
	public String use_creditcard_step2(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace(ESAPIUtil.vaildLog("use_creditcard_step2 reqParam>>>>>{}"+CodeUtil.toJson(reqParam)));
		String target = "/online_apply/ErrorWithoutMenu";
		BaseResult bs = null;
		
		try {
			bs = new BaseResult();
			String cusidn = reqParam.get("_CUSIDN");
			log.trace(ESAPIUtil.vaildLog("CUSIDN>>{}"+ cusidn));
			String MSGTYPE = reqParam.get("BACKTYPE");
			log.debug(ESAPIUtil.vaildLog("MSGTYPE={}"+MSGTYPE));
			if(StrUtils.isNotEmpty(MSGTYPE)&&MSGTYPE.equals("Y")) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null));
				log.trace("BSGETDATA>>>>>{}",bs.getData());
			}
			else {
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		  //Map<String, String> okMap = reqParam;
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
				if(hasLocale){
//					log.trace("locale>>" + okMap.get("locale"));
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
					if( ! bs.getResult())
					{
						log.trace("bs.getResult() >>{}",bs.getResult());
						throw new Exception();
					}
				}
				if( ! hasLocale){
					bs.reset();
					log.debug(ESAPIUtil.vaildLog("use_creditcard_step2.cusidn >>{}"+ cusidn));
					try {
						reqParam.put("USERIP",ESAPIUtil.validInput(WebUtil.getIpAddr(request),"GeneralString",true));
						bs = online_apply_service.use_creditcard_step2(cusidn, reqParam);
						if(bs != null && bs.getResult()) {
							log.trace("GET BS DATA>>>{}", bs.getData());
							SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
							SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
						}
					}
					catch(Exception e) {
						//Avoid Information Exposure Through an Error Message
						//e.printStackTrace();
						log.error("use_creditcard_step2 error >> {}",e);
					}
				}
			}
		}
		catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("use_creditcard_step2 error >> {}",e);
		}
		finally {
			if (bs != null && bs.getResult())
			{
				target = "/online_apply/use_creditcard_step2";
				model.addAttribute("use_creditcard_step2", bs);
			}
			else
			{
				bs.setPrevious("/ONLINE/APPLY/use_creditcard_step1");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * NA40 使用臺灣企銀信用卡
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/use_creditcard_step3")
	public String use_creditcard_step3(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace(ESAPIUtil.vaildLog("use_creditcard_step3 reqParam>>>>>{}"+CodeUtil.toJson(reqParam)));
		try {
			BaseResult bs = new BaseResult();
			//Map<String, String> okMap = reqParam;
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//		判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				try {
					bs.addAllData(reqParam);
					log.trace("get bs data>>>>>{}",bs.getData());
					Map<String,String>datamap=(Map<String, String>) bs.getData();
					if(StrUtils.isEmpty(datamap.get("NOTIFY_CCARDPAY"))) {
						datamap.put("NOTIFY_CCARDPAY", "");
					}
					if(StrUtils.isEmpty(datamap.get("NOTIFY_ACTIVE"))) {
						datamap.put("NOTIFY_ACTIVE", "");
					}
					if(StrUtils.isEmpty(datamap.get("NOTIFY_CCARDBILL"))) {
						datamap.put("NOTIFY_CCARDBILL", "");
					}
					log.trace("BSDATA>>>>>{}",bs.getData());
					bs.setResult(true);
					model.addAttribute("use_creditcard_step3", bs);
					SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				}
				catch(Exception e) {
					//Avoid Information Exposure Through an Error Message
					//e.printStackTrace();
					log.error("use_creditcard_step3 error >> {}",e);
				}
			}
		}
		catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("use_creditcard_step3 error >> {}",e);
		}
		return "/online_apply/use_creditcard_step3";
	}
	
	/**
	 * NA40 使用臺灣企銀信用卡
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/use_creditcard_result")
	public String use_creditcard_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace(ESAPIUtil.vaildLog("use_creditcard_result reqParam>>>>>{}"+CodeUtil.toJson(reqParam)));
		String target = "/online_apply/ErrorWithoutMenu";
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			String cusidn = reqParam.get("_CUSIDN");
			log.trace(ESAPIUtil.vaildLog("CUSIDN>>{}"+ cusidn));
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			//Map<String, String> okMap = reqParam;
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.trace(ESAPIUtil.vaildLog("use_creditcard_result.cusidn>>{}"+ cusidn));
				try {
					bs = online_apply_service.use_creditcard_result(cusidn, reqParam);
					if(bs != null && bs.getResult()) {
						log.trace("GET BS DATA>>>{}", bs.getData());
						SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
					}
				}
				catch(Exception e) {
					//Avoid Information Exposure Through an Error Message
					//e.printStackTrace();
					log.error("use_creditcard_result error >> {}",e);
				}
			}
		}
		catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("use_creditcard_result error >> {}",e);
			
		}
		finally {
			if (bs != null && bs.getResult())
			{
				target = "/online_apply/use_creditcard_result";
				model.addAttribute("use_creditcard_result", bs);
			}
			else
			{
				bs.setPrevious("/ONLINE/APPLY/use_creditcard_step2?BACKTYPE=Y");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	public String TakeOfCoat(String[] originreq) {
		String temp=Arrays.toString(originreq).replace("[","");
		temp=temp.replace("]","");
		return temp;
	}

	/**
	 * 檢測session是否還存在
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	public Boolean getSessionHas(String sessionName, String checkColName, Model model) {
        Boolean flag = Boolean.FALSE;
		try {
	        Map<String,String> inputData = (Map<String,String>)SessionUtil.getAttribute(model, sessionName, null);
	        if(inputData != null) {
	        	if(inputData.get(checkColName) != null) {
		        	if(!inputData.get(checkColName).equals("")) {
		        		flag = Boolean.TRUE;
		        	}
	        	}
	        }
		}catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getSessionHas error >> {}",e);
		}
		return flag;
	}
	
	/**
	 * 檢測session是否還存在
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	public Boolean getSessionHas(String sessionName, String checkColName, Model model, BaseResult inputData) {
        Boolean flag = Boolean.FALSE;
		try {
	        inputData = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.ONLINE_APPLY_DATA, null));
	        if(inputData != null) {
	        	Map<String,Object> callTable = (Map<String,Object>)inputData.getData();
	        	if(callTable.get(checkColName) != null) {
		        	if(!((String)callTable.get(checkColName)).equals("")) {
		        		flag = Boolean.TRUE;
		        	}
	        	}
	        }
		}catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getSessionHas error >> {}",e);
		}
		return flag;
	}
	
	/**
	 * INSERT_TXNCUSINVATTRHIST_aj
	 * 
	 * @return
	 */
	@PostMapping(value = "/online_apply_insert_hist", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult insert_hist(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.info("insert_hist_aj...");
		BaseResult bs = null;
		String cusidn ="";
		try {
			Map<String, String> okMap =  ESAPIUtil.validStrMap(reqParam);
			Map<String, String> insertMap = new HashMap<>();
			
			// 從session取值 登入取CUSIDN 未登入取CUSIDN_N361
			if("Y".equals(okMap.get("LoginYN"))) {
				cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null));
				cusidn = new String(Base64.getDecoder().decode(cusidn)).toUpperCase();
			}else {
				cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN_N361, null));
				cusidn = new String(Base64.getDecoder().decode(cusidn)).toUpperCase();
			}
			Map<String, String> kycConfirmParams = (Map) SessionUtil.getAttribute(model, SessionUtil.N361_KYC_CONFIRM, null);
			
			String aduserip = WebUtil.getIpAddr(request);
			//TEST
//			aduserip = "172.22.11.23";
			
			
			insertMap.put("CUSIDN", cusidn);
			insertMap.put("ADUSERIP", aduserip);
			insertMap.putAll(kycConfirmParams);
			insertMap.put("RTC", "S");
			insertMap.put("AGREE", "");
			//獲取 中心計算之KYC分數及投資屬性
			bs = fund_transfer_service.getKycScore(insertMap);
			String SCORE =StrUtil.isNotEmpty((String) ((Map<String,Object>)bs.getData()).get("SCORE"))?(String) ((Map<String,Object>)bs.getData()).get("SCORE"):"";
			String RISK =StrUtil.isNotEmpty((String) ((Map<String,Object>)bs.getData()).get("RISK"))?(String) ((Map<String,Object>)bs.getData()).get("RISK"):"";
			insertMap.put("FDINVTYPE", RISK);
			insertMap.put("FDSCORE", SCORE);
			

			log.info(ESAPIUtil.vaildLog("online_apply_insert_hist okMap" + CodeUtil.toJson(insertMap)));
			
			bs = fund_transfer_service.aj_insert_txncusinvattrhist(insertMap);
			bs.addData("FDINVTYPE", RISK);
			bs.addData("iScore", SCORE);
			if(bs.getResult()) {
				String uuid = UUID.randomUUID().toString();
				SessionUtil.addAttribute(model, SessionUtil.KYC_INSERT_HIST_PASS, uuid);
				bs.addData("kyc_insert_uuid", uuid);
			}
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("insert_hist error >> {}", e);
		}
		return bs;
	}
	
	
	/**
	 * check_mail_aj
	 * 
	 * @return
	 */
	@PostMapping(value = "/check_mail_aj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult check_mail_aj(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("check_mail_aj...");
		BaseResult bs = null;
		try {
			Map<String, String> okMap =  ESAPIUtil.validStrMap(reqParam);
			String cusidn = new String(Base64.getDecoder().decode(okMap.get("CUSIDN")), "utf-8");
			String email = okMap.get("EMAIL");
			// C011 xml 及  DB check
			bs = online_apply_service.checkEmpMail(cusidn, email);
		}catch (Exception e) {
			log.error("check_mail_aj error >> {}",e);
		}
		return bs ;
	}
	
	/**
	 * 信用卡額度調升線上申請P1
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/creditcard_limit_increase")
	public String creditcard_limit_increase(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace(ESAPIUtil.vaildLog("creditcard_limit_increase start"));
		String target = "/online_apply/creditcard_limit_increase_p1";
		BaseResult bs = new BaseResult();
		
		try {
			SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.STEP2_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.STEP3_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			// 辨別是從NB3網路銀行進入頁面，還是從台企銀官網進入頁面
			model.addAttribute("FROM_NB3", reqParam.get("FROM_NB3"));
		}
		catch (Exception e) {
			// TODO: handle exception
			log.error("error >> {}", e);
		}
		
		return target;
	}
	
	/**
	 * 信用卡額度調升線上申請P2
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/creditcard_limit_increase_otp")
	public String creditcard_limit_increase_otp(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace(ESAPIUtil.vaildLog("creditcard_limit_increase_p2 start"));
		String target = "/online_apply/creditcard_limit_increase_error";
		BaseResult bs = new BaseResult();
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		log.trace("okMap data>>>"+okMap);
		BaseResult bs1 = new BaseResult();
		
		try {
			String back = okMap.get("back") == null ? "" : okMap.get("back");
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
				}
			}
			else if(back.equals("Y")){
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
				}
			}
			else if(!hasLocale){
				String IP = WebUtil.getIpAddr(request); // 登入 IP-Address
				log.debug(ESAPIUtil.vaildLog("IP >> " + IP));
				okMap.put("IP",IP);
				
				bs1.addAllData(okMap);
				bs = online_apply_service.creditcard_limit_increase_otp(okMap);
				bs.addAllData((Map<String, String>)bs1.getData());
				log.debug("All bs data>>>"+bs.getData());
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, bs);
			}
		}
		catch (Exception e) {
			// TODO: handle exception
			log.error("creditcard_limit_increase_p2 error >> {}",e);
		}
		finally {
			if (bs != null && bs.getResult())
			{
				target = "/online_apply/creditcard_limit_increase_p2";
				model.addAttribute("limit_increase", bs);
				model.addAttribute("FROM_NB3", okMap.get("FROM_NB3"));
			}
			else
			{
				bs.setPrevious("/ONLINE/APPLY/creditcard_limit_increase");
				model.addAttribute(BaseResult.ERROR, bs);
			}
			model.addAttribute("FROM_NB3", okMap.get("FROM_NB3"));
		}
		return target;
	}
	
	/**
	 * 信用卡額度調升線上申請P3
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/creditcard_limit_increase_p3")
	public String creditcard_limit_increase_p3(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace(ESAPIUtil.vaildLog("creditcard_limit_increase_p3 start"));
		String target = "/online_apply/creditcard_limit_increase_error";
		BaseResult bs = null;
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		log.trace("okMap data>>>"+okMap);
		
		try {
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP2_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			else if(okMap.get("back").equals("Y")) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP3_LOCALE_DATA, null));
			}
			else if(!hasLocale){
				bs = new BaseResult();
				bs = online_apply_service.creditcard_limit_increase_p3(okMap);
				//test
//				bs.addData("CHKID", "");
//				bs.setResult(true);
				log.debug("otp data>>>"+bs.getData());
				
				//取得N822 & 個人資料
				BaseResult bs1 = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null));
				bs.addAllData((Map<String, String>)bs1.getData());
				bs.addData("Oday", DateUtil.getNext_N_Date("yyyy/MM/dd", 3));
				bs.addData("maxDate", DateUtil.getNext_N_Date("yyyy/MM/dd", 93));
				log.debug("ALL bs data>>>"+bs.getData());
				SessionUtil.addAttribute(model, SessionUtil.STEP2_LOCALE_DATA, bs);
			}
			
		}
		catch (Exception e) {
			// TODO: handle exception
			log.error("creditcard_limit_increase_p3 error >> {}",e);
		}
		
		finally {
			if (bs != null && bs.getResult())
			{
				target = "/online_apply/creditcard_limit_increase_p3";
				model.addAttribute("limit_increase", bs);
				model.addAttribute("FROM_NB3", okMap.get("FROM_NB3"));
			}
			else
			{
				bs.setPrevious("/ONLINE/APPLY/creditcard_limit_increase");
				model.addAttribute(BaseResult.ERROR, bs);
			}
			model.addAttribute("FROM_NB3", okMap.get("FROM_NB3"));
		}		
		return target;
	}
	
	/**
	 * 信用卡額度調升線上申請P3_1
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/creditcard_limit_increase_p3_1")
	public String creditcard_limit_increase_p3_1(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace(ESAPIUtil.vaildLog("creditcard_limit_increase_p3_1 start"));
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		log.trace("reqParam >> {}"+okMap);
		BaseResult bs = new BaseResult();
		String target = "/online_apply/creditcard_limit_increase_error";
		try {
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP3_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			else if(okMap.get("back").equals("Y")) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
			}
			else if(!hasLocale){
				bs = online_apply_service.creditcard_limit_increase_p3_1(okMap);
				//test
//				bs.addData("RESULT","N");//非預審客戶
				
				String filetime = DateUtil.getCurentDateTime("yyyyMMdd");
				bs.addData("filetime", filetime);
				bs.addAllData(okMap);
				log.debug("p31 bs data >>>"+bs.getData());
			}
		}
		catch (Exception e) {
			// TODO: handle exception
			log.error("creditcard_limit_increase_p3_1 error >> {}",e);
		}
		finally {
			if (bs != null && bs.getResult())
			{
				String RESULT = ((Map<String, String>)bs.getData()).get("RESULT").toString();
				
				target = "/online_apply/creditcard_limit_increase_p3_1";
				
				switch(RESULT) {
				case "Y11":
					target="/online_apply/creditcard_limit_increase_p4";
					break;
				case "Y21":
					target="/online_apply/creditcard_limit_increase_p4";
					break;
				}
				BaseResult bs1 = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP2_LOCALE_DATA, null));
				bs.addAllData((Map<String, String>)bs1.getData());
				log.debug("get bs data 3 >>>"+bs.getData());
				SessionUtil.addAttribute(model, SessionUtil.STEP3_LOCALE_DATA, bs);
				model.addAttribute("limit_increase", bs);
				model.addAttribute("FROM_NB3", okMap.get("FROM_NB3"));
			}
			else
			{
				bs.setPrevious("/ONLINE/APPLY/creditcard_limit_increase");
				model.addAttribute(BaseResult.ERROR, bs);
			}
			model.addAttribute("FROM_NB3", okMap.get("FROM_NB3"));
		}		
		return target;
	}
	
	/**
	 * 信用卡額度調升返回
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/creditcard_limit_increase_back")
	public String creditcard_limit_increase_back(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace(ESAPIUtil.vaildLog("creditcard_limit_increase_back start"));
		String target = "/online_apply/creditcard_limit_increase_p3_1";
		BaseResult bs = new BaseResult();
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		log.trace("reqParam >> {}"+okMap);
		
		switch(okMap.get("RESULT")) {
			case "Y11":
				target = "/online_apply/creditcard_limit_increase_p3";
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP3_LOCALE_DATA, null));
				break;
			case "Y21":
				target = "/online_apply/creditcard_limit_increase_p3";
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP3_LOCALE_DATA, null));
				break;
			default:
				target = "/online_apply/creditcard_limit_increase_p3_1";
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				break;
		}
		model.addAttribute("limit_increase", bs);
		model.addAttribute("FROM_NB3", okMap.get("FROM_NB3"));
		log.debug("back bs >>"+bs.getData());
		return target;
	}
	
	/**
	 * 信用卡額度調升線上申請P4
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/creditcard_limit_increase_confirm")
	public String creditcard_limit_increase_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace(ESAPIUtil.vaildLog("creditcard_limit_increase_p4 start"));
		BaseResult bs = new BaseResult();
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		log.trace("reqParam >> {}"+okMap);
		
		String target = "/online_apply/creditcard_limit_increase_error";
		try {
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if(!hasLocale){
				bs.addAllData(okMap);
				bs.setResult(true);
			}
		}
		catch (Exception e) {
			// TODO: handle exception
			log.error("creditcard_limit_increase_p4 error >> {}",e);
		}
		finally {
			if (bs != null && bs.getResult())
			{
				target="/online_apply/creditcard_limit_increase_p4";
				BaseResult bs1 = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP3_LOCALE_DATA, null));
				bs.addAllData((Map<String, String>)bs1.getData());
				log.debug("get bs data 4 >>>"+bs.getData());
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				model.addAttribute("limit_increase", bs);
				model.addAttribute("FROM_NB3", okMap.get("FROM_NB3"));
			}
			else
			{
				bs.setPrevious("/ONLINE/APPLY/creditcard_limit_increase");
				model.addAttribute(BaseResult.ERROR, bs);
			}
			model.addAttribute("FROM_NB3", okMap.get("FROM_NB3"));
		}	
		return target;
	}
	
	/**
	 * 信用卡額度調升線上申請P5
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/creditcard_limit_increase_result")
	public String creditcard_limit_increase_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace(ESAPIUtil.vaildLog("creditcard_limit_increase_p5 start"));
		
		BaseResult bs = new BaseResult();
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		log.trace("reqParam >> {}"+okMap);
		
		String target = "/online_apply/creditcard_limit_increase_error";
		try {
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if(!hasLocale){
				BaseResult bs1 = new BaseResult();
				bs1 = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				Map<String, String> bs1data = (Map<String, String>)bs1.getData();
				log.debug("bsdata 1>>>"+bs1data);
				if(bs1data == null) {
					bs1.reset();
					bs1 = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP3_LOCALE_DATA, null));
					bs1data = (Map<String, String>)bs1.getData();
					log.debug("bsdata 2>>>"+bs1data);
				}
				okMap.putAll(bs1data);
				bs = online_apply_service.creditcard_limit_increase_result(okMap);
//				bs.setResult(true);
				
//				okMap.putAll((Map<String, String>)bs.getData());
			}
		}
		catch (Exception e) {
			// TODO: handle exception
			log.error("creditcard_limit_increase_p5 error >> {}",e);
		}
		finally {
			if (bs != null && bs.getResult())
			{
				target="/online_apply/creditcard_limit_increase_p5";
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				model.addAttribute("limit_increase", bs);
				model.addAttribute("FROM_NB3", okMap.get("FROM_NB3"));
			}
			else
			{
				bs.setPrevious("/ONLINE/APPLY/creditcard_limit_increase");
				model.addAttribute(BaseResult.ERROR, bs);
			}
			model.addAttribute("FROM_NB3", okMap.get("FROM_NB3"));
		}
		return target;
	}
	
	/**
	 * 信用卡額度調升線上申請error
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/creditcard_limit_increase_error")
	public String creditcard_limit_increase_error(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace(ESAPIUtil.vaildLog("creditcard_limit_increase_error start"));
		String target = "/online_apply/creditcard_limit_increase_error";
		model.addAttribute("FROM_NB3", reqParam.get("FROM_NB3"));
		return target;
	}
	
	/**
	 * 信用卡額度調升線上申請條款
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/creditcard_limit_article")
	public String creditcard_limit_article(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace(ESAPIUtil.vaildLog("creditcard_limit_article start"));
		String target = "/online_apply/creditcard_limit_increase_error";
		String article = reqParam.get("article");
		if(article.equals("1")) {
			target = "/online_apply/creditcard_limit_article_1";
		}
		else if(article.equals("2")) {
			target = "/online_apply/creditcard_limit_article_2";
		}
		return target;
	}
	
	@PostMapping("/apply_creditcard_limit_fileupload")
    public @ResponseBody BaseResult apply_creditcard_limit_fileupload(HttpServletRequest request, HttpServletResponse response, @RequestParam() MultipartFile picFile, @RequestParam Map<String, String> reqParam, Model model) {
    	BaseResult bs = new BaseResult();
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//    	String uploadPath_N201 = request.getSession().getServletContext().getRealPath("/com/updateTmp/") + "";
		String separ = File.separator;
		String uploadPath_N201 = context.getRealPath("") + separ +"com" + separ + "updateTmp" + separ;
    	log.debug("update_Path >> {}",uploadPath_N201);
    	//圖示允許的類型
    	String[] allowImageType = {"image/jpeg","image/jpg","image/gif"};
		//圖示允許的長寬
		int allowImageWidth = 640;
		int allowImageHeight = 480;
		//圖示允許的檔案大小
		int allowImageSize = 5000 * 1024;//3300KB
		String contentType;
		String originalFilename;
		try {
            log.debug(ESAPIUtil.vaildLog(okMap.get("oldPath")));
			if(!okMap.get("oldPath").equals("")) {
				online_apply_service.removefile( uploadPath_N201 , okMap.get("oldPath"), "", "");
			}
		}catch(Exception e) {
			log.error(e.toString());
		}
        try {
            MultipartFile multipartFile = picFile;
            log.debug(ESAPIUtil.vaildLog(okMap.get("type")));

			String fileTyle = multipartFile.getOriginalFilename().substring(multipartFile.getOriginalFilename().lastIndexOf("."),multipartFile.getOriginalFilename().length()).toLowerCase(); 
            String fileName = okMap.get("CUSIDN") + okMap.get("filetime") + okMap.get("type") + fileTyle;
            try {
            	String pre = okMap.get("PRE");
            	if(StrUtil.isNotEmpty(pre)) {
            		fileName = okMap.get("CUSIDN") + okMap.get("filetime") + okMap.get("type") + pre + fileTyle;
            	}
            }
            catch (Exception e) {
				// TODO: handle exception
			}
            log.debug(ESAPIUtil.vaildLog("fileName={}"+ fileName));

            if(multipartFile.getContentType().equals("application/pdf")) {
            	bs.addAllData(online_apply_service.checkFile(	multipartFile,
								            			multipartFile.getOriginalFilename(),
								            			multipartFile.getContentType(),
								            			allowImageType,
								            			allowImageWidth,
								            			allowImageHeight,
								            			allowImageSize,
								            			uploadPath_N201, fileName));
            }
            else {
            	bs.addAllData(online_apply_service.checkImage(	multipartFile,
									            			multipartFile.getOriginalFilename(),
									            			multipartFile.getContentType(),
									            			allowImageType,
									            			allowImageWidth,
									            			allowImageHeight,
									            			allowImageSize,
									            			uploadPath_N201, fileName));
            }

            if( ((Map<String,Object>)bs.getData()).get("result").equals("TRUE") ) {
                bs.addData("validated", Boolean.TRUE);
            }else {
                bs.addData("validated", Boolean.FALSE);
            }
            bs.addData("url", fileName);
        } catch (Exception e) {
            log.error("saveFile Error", e);
            Map<String, String> errors = new HashMap<String, String>();
            bs.addData("summary", i18n.getMsg("LB.X2131"));
            bs.addData("validated", Boolean.FALSE);

        }
        return bs;
    }	
	
	@PostMapping("/apply_deposit_account_get_city_bh")
	public @ResponseBody BaseResult apply_deposit_account_get_city_bh(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		BaseResult bs = null;
		bs = online_apply_service.getCityBh(reqParam.get("zip"));
		model.addAttribute("bs",bs);
		return bs;
	}
}

