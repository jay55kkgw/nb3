package tw.com.fstop.nnb.spring.controller;


import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.nnb.custom.annotation.ISTXNLOG;
import tw.com.fstop.util.SessionUtil;


@Controller
@RequestMapping(value ="/FINANCIAL/TRIAL/INVEST")
public class Financial_Trial_Invest_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());

	
	// 定期定額投資試算_選擇頁
	@RequestMapping(value = "/fixed_investment")
		public String predesignated_account(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("fixed_investment-->");
		
		String target = "/financial_trial_invest/fixed_investment";			

		//清除切換語系時暫存的資料
		SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");		
				
		return target;
	}
	
	/**
	 * 投資目標試算
	 */
	@ISTXNLOG(value="A4061")
	@RequestMapping(value ="/investment_target_trial")
	public String investment_target_trial(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		return "/financial_trial_invest/investment_target_trial";
	}
	
	/**
	 * 每月投資金額試算
	 */
	@ISTXNLOG(value="A4062")
	@RequestMapping(value ="/monthly_investment_trial")
	public String monthly_investment_trial(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		return "/financial_trial_invest/monthly_investment_trial";
	}
	
	/**
	 * 投資報酬率試算
	 */
	@ISTXNLOG(value="A4063")
	@RequestMapping(value ="/investment_rrate_trial")
	public String nvestment_rrate_trial(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		return "/financial_trial_invest/investment_rrate_trial";
	}
}
