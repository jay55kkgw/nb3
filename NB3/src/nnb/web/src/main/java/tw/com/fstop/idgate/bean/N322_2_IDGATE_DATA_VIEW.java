package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

public class N322_2_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -1568671015374302095L;

	/**
	 * 
	 */

	@SerializedName(value = "Q1")
	private String Q1;

	@SerializedName(value = "Q2")
	private String Q2;

	@SerializedName(value = "Q3")
	private String Q3;

	@SerializedName(value = "Q4")
	private String Q4;

	@SerializedName(value = "Q5")
	private String Q5;

	@SerializedName(value = "Q6")
	private String Q6;
	//Q7
	@SerializedName(value = "Q71")
	private String Q71;
	
	@SerializedName(value = "Q72")
	private String Q72;
	
	@SerializedName(value = "Q73")
	private String Q73;
	
	@SerializedName(value = "Q74")
	private String Q74;
	//Q7
	@SerializedName(value = "Q8")
	private String Q8;

	//Q9
	@SerializedName(value = "Q91")
	private String Q91;
	
	@SerializedName(value = "Q92")
	private String Q92;
	
	@SerializedName(value = "Q93")
	private String Q93;
	
	@SerializedName(value = "Q94")
	private String Q94;
	//Q9

	@SerializedName(value = "Q10")
	private String Q10;

	@SerializedName(value = "Q11")
	private String Q11;

	@SerializedName(value = "Q12")
	private String Q12;
	
	@SerializedName(value = "Q13")
	private String Q13;

	@SerializedName(value = "Q14")
	private String Q14;

	@Override
	public Map<String, String> coverMap() {
		LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
        result.put("交易名稱", "您有一筆投資屬性評估調查表－（法人版）問卷結果更新");
        result.put("交易類型", "更新");

		// switch (Q1) {
		// 	case "A":
		// 		result.put("貴公司已設立", "未達3年");
		// 		break;
		// 	case "B":
		// 		result.put("貴公司已設立", "3年以上～未達5年");
		// 		break;
		// 	case "C":
		// 		result.put("貴公司已設立", "5年以上～未達10年");
		// 		break;
		// 	case "D":
		// 		result.put("貴公司已設立", "10年以上");
		// 		break;
		// }

		// switch (Q2) {
		// 	case "A":
		// 		result.put("貴公司的資本額", "未達500萬");
		// 		break;
		// 	case "B":
		// 		result.put("貴公司的資本額", "500萬以上～未達1000萬");
		// 		break;
		// 	case "C":
		// 		result.put("貴公司的資本額", "1000萬以上～未達3000萬");
		// 		break;
		// 	case "D":
		// 		result.put("貴公司的資本額", "3000萬以上");
		// 		break;
		// }

		// switch (Q3) {
		// 	case "A":
		// 		result.put("貴公司的行業", "投資公司");
		// 		break;
		// 	case "B":
		// 		result.put("貴公司的行業", "金融相關行業");
		// 		break;
		// 	case "C":
		// 		result.put("貴公司的行業", "其他");
		// 		break;
		// }

		// switch (Q4) {
		// 	case "A":
		// 		result.put("貴公司之年營收", "未達3000萬");
		// 		break;
		// 	case "B":
		// 		result.put("貴公司之年營收", "3000萬以上～未達5000萬");
		// 		break;
		// 	case "C":
		// 		result.put("貴公司之年營收", "5000萬以上");
		// 		break;
		// }

		// switch (Q5) {
		// 	case "A":
		// 		result.put("目前擁有的資產（存款＋投資）金額總計有", "未達200萬");
		// 		break;
		// 	case "B":
		// 		result.put("目前擁有的資產（存款＋投資）金額總計有", "200萬以上～未達500萬");
		// 		break;
		// 	case "C":
		// 		result.put("目前擁有的資產（存款＋投資）金額總計有", "500萬以上");
		// 		break;
		// }

		// switch (Q6) {
		// 	case "A":
		// 		result.put("貴公司可供投資的資金主要來源", "長期可運用資金");
		// 		break;
		// 	case "B":
		// 		result.put("貴公司可供投資的資金主要來源", "短期週轉金");
		// 		break;
		// 	case "C":
		// 		result.put("貴公司可供投資的資金主要來源", "借貸資金");
		// 		break;
		// }

		// //Q7 START
		// StringBuilder Q7 = new StringBuilder();
		// if (Q71 !=null) {
		// 	Q7.append("期貨、連動債、投資型保單、衍生性金融產品");
		// }

		// if (Q72 !=null) {
		// 	if (Q7.length() > 0) {
		// 		Q7.append(",");
		// 	}
		// 	Q7.append("組合式商品");
		// }

		// if (Q73 !=null) {
		// 	if (Q7.length() > 0) {
		// 		Q7.append(",");
		// 	}
		// 	Q7.append("國內外基金、股票");
		// }

		// if (Q74 !=null) {
		// 	if (Q7.length() > 0) {
		// 		Q7.append(",");
		// 	}
		// 	Q7.append("存款、RP、定存或保險");
		// }

		// result.put("貴公司曾經投資下列商品", Q7.toString());
		// //Q7 END

		// switch (Q8) {
		// 	case "A":
		// 		result.put("貴公司從事投資理財時間", "未達3年");
		// 		break;
		// 	case "B":
		// 		result.put("貴公司從事投資理財時間", "3年以上～未達5年");
		// 		break;
		// 	case "C":
		// 		result.put("貴公司從事投資理財時間", "10年以上");
		// 		break;
		// }

		// //Q9 START
		// StringBuilder Q9 = new StringBuilder();
		// if (Q91 != null) {
		// 	Q9.append("對連動債等衍生性商品了解");
		// }

		// if (Q92 != null) {
		// 	if (Q9.length() > 0) {
		// 		Q9.append(",");
		// 	}
		// 	Q9.append("對股票了解");
		// }

		// if (Q93 != null) {
		// 	if (Q9.length() > 0) {
		// 		Q9.append(",");
		// 	}
		// 	Q9.append("對國內外共同基金了解");
		// }

		// if (Q94 != null) {
		// 	if (Q9.length() > 0) {
		// 		Q9.append(",");
		// 	}
		// 	Q9.append("對金融商品不了解");
		// }

		// result.put("貴公司對金融商品的知識", Q9.toString());
		// //Q9 END

		// switch (Q10) {
		// 	case "A":
		// 		result.put("貴公司的投資需求", "閒置資金自由運用");
		// 		break;
		// 	case "B":
		// 		result.put("貴公司的投資需求", "掌握資本利得增加的機會");
		// 		break;
		// 	case "C":
		// 		result.put("貴公司的投資需求", "賺取較為穩定的收益");
		// 		break;
		// }
		
		// switch (Q11) {
		// 	case "A":
		// 		result.put("期望年報酬率", "未達5％");
		// 		break;
		// 	case "B":
		// 		result.put("期望年報酬率", "5％以上～未達10％");
		// 		break;
		// 	case "C":
		// 		result.put("期望年報酬率", "10％以上～未達20％");
		// 		break;
		// 	case "D":
		// 		result.put("期望年報酬率", "20％以上");
		// 		break;
		// }

		// switch (Q12) {
		// 	case "A":
		// 		result.put("可忍受的損失為何，超過就考慮執行停損", "未達5％");
		// 		break;
		// 	case "B":
		// 		result.put("可忍受的損失為何，超過就考慮執行停損", "5％以上～未達10％");
		// 		break;
		// 	case "C":
		// 		result.put("可忍受的損失為何，超過就考慮執行停損", "10％以上～未達20％");
		// 		break;
		// 	case "D":
		// 		result.put("可忍受的損失為何，超過就考慮執行停損", "20％以上");
		// 		break;
		// }

		// switch (Q13) {
		// 	case "A":
		// 		result.put("交易目的", "保障規劃（不能接受虧損）");
		// 		break;
		// 	case "B":
		// 		result.put("交易目的", "節稅");
		// 		break;
		// 	case "C":
		// 		result.put("交易目的", "避險");
		// 		break;
		// 	case "D":
		// 		result.put("交易目的", "投資");
		// 		break;
		// }

		// switch (Q14) {
		// 	case "A":
		// 		result.put("貴公司認為下列商品者風險最高", "期貨、連動債、投資型保單、衍生性金融產品");
		// 		break;
		// 	case "B":
		// 		result.put("貴公司認為下列商品者風險最高", "組合式商品");
		// 		break;
		// 	case "C":
		// 		result.put("貴公司認為下列商品者風險最高", "國內外基金、股票");
		// 		break;
		// 	case "D":
		// 		result.put("貴公司認為下列商品者風險最高", "存款、RP、定存或保險");
		// 		break;
		// }

		return result;
	}

	public String getQ1() {
		return Q1;
	}

	public void setQ1(String q1) {
		Q1 = q1;
	}

	public String getQ2() {
		return Q2;
	}

	public void setQ2(String q2) {
		Q2 = q2;
	}

	public String getQ3() {
		return Q3;
	}

	public void setQ3(String q3) {
		Q3 = q3;
	}

	public String getQ4() {
		return Q4;
	}

	public void setQ4(String q4) {
		Q4 = q4;
	}

	public String getQ5() {
		return Q5;
	}

	public void setQ5(String q5) {
		Q5 = q5;
	}

	public String getQ6() {
		return Q6;
	}

	public void setQ6(String q6) {
		Q6 = q6;
	}

	public String getQ71() {
		return Q71;
	}

	public void setQ71(String q71) {
		Q71 = q71;
	}

	public String getQ72() {
		return Q72;
	}

	public void setQ72(String q72) {
		Q72 = q72;
	}

	public String getQ73() {
		return Q73;
	}

	public void setQ73(String q73) {
		Q73 = q73;
	}

	public String getQ74() {
		return Q74;
	}

	public void setQ74(String q74) {
		Q74 = q74;
	}

	public String getQ8() {
		return Q8;
	}

	public void setQ8(String q8) {
		Q8 = q8;
	}

	public String getQ91() {
		return Q91;
	}

	public void setQ91(String q91) {
		Q91 = q91;
	}

	public String getQ92() {
		return Q92;
	}

	public void setQ92(String q92) {
		Q92 = q92;
	}

	public String getQ93() {
		return Q93;
	}

	public void setQ93(String q93) {
		Q93 = q93;
	}

	public String getQ94() {
		return Q94;
	}

	public void setQ94(String q94) {
		Q94 = q94;
	}

	public String getQ10() {
		return Q10;
	}

	public void setQ10(String q10) {
		Q10 = q10;
	}

	public String getQ11() {
		return Q11;
	}

	public void setQ11(String q11) {
		Q11 = q11;
	}

	public String getQ12() {
		return Q12;
	}

	public void setQ12(String q12) {
		Q12 = q12;
	}

	public String getQ13() {
		return Q13;
	}

	public void setQ13(String q13) {
		Q13 = q13;
	}

	public String getQ14() {
		return Q14;
	}

	public void setQ14(String q14) {
		Q14 = q14;
	}

}
