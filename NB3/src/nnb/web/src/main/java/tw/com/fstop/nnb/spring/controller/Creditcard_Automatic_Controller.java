package tw.com.fstop.nnb.spring.controller;

import java.net.URLEncoder;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.common.ResultCode;
import tw.com.fstop.idgate.bean.NI04_IDGATE_DATA;
import tw.com.fstop.idgate.bean.NI04_IDGATE_DATA_VIEW;
import tw.com.fstop.nnb.service.Creditcard_Automatic_Service;
import tw.com.fstop.nnb.service.IdGateService;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({SessionUtil.CUSIDN,
	SessionUtil.RESULT_LOCALE_DATA, SessionUtil.CONFIRM_LOCALE_DATA, 
	SessionUtil.TRANSFER_CONFIRM_TOKEN, SessionUtil.TRANSFER_RESULT_TOKEN, 
	SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, SessionUtil.TRANSFER_DATA,SessionUtil.IDGATE_TRANSDATA })
@Controller
@RequestMapping(value = "/CREDIT/AUTOMATIC")
public class Creditcard_Automatic_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private Creditcard_Automatic_Service creditcard_automatic_service;
	@Autowired
	private I18n i18n;
//	
	@Autowired
	IdGateService idgateservice;
	
	/**
	 * 信用卡款自動扣繳申請/取消
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/debit_apply")
	public String card_autopay_apply_p1(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		log.trace("debit_apply_controller start");
		
		String target = "/error";
		BaseResult bs = null;
		BaseResult bs1 = null;
		try
		{
			bs = new BaseResult();
			bs1 = new BaseResult();
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");

			// 登入時的身分證字號
			String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if (!"".equals(cusidn) && cusidn != null) {
				// 解密成明碼
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				reqParam.put("CUSIDN", cusidn);
				
			}else {
				log.error("session no cusidn!!!");
			}
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = creditcard_automatic_service.getTxToken();
			log.trace("debit_apply.TXTOKEN: " + ((Map<String, String>) bs.getData()).get("TXTOKEN"));
			if (bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN,
						((Map<String, String>) bs.getData()).get("TXTOKEN"));
			}
			
			//IDGATE身分
			String idgateUserFlag="N";
			try {
				BaseResult tmp=idgateservice.checkIdentity(cusidn);
				if(tmp.getResult()) {
					idgateUserFlag="Y";					
				}
				tmp.addData("idgateUserFlag",idgateUserFlag);
				SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA,((Map<String, String>) tmp.getData()));
			}catch(Exception e) {
				log.debug("idgateUserFlag error {}",e);
			}
			model.addAttribute("idgateUserFlag",idgateUserFlag);
			
			bs1 = creditcard_automatic_service.debit_apply(okMap);
		}catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("card_autopay_apply_p1 error >> {}",e);
		} finally {
			if (bs1 !=null && bs1.getResult()) {
				model.addAttribute("debit_apply", bs1);
				target = "/credit_automatic/debit_apply";

			} else {
				model.addAttribute(BaseResult.ERROR, bs1);
			}
		}

		log.trace("target >> {}", target);
		return target;
	}
	
	
	/**
	 * 信用卡款自動扣繳申請/取消
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/debit_apply_confirm")
	public String card_autopay_apply_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		log.trace("debit_apply_confirm_controller start");
		
		String target = "/error";
		String jsondc = "";
		BaseResult bs = new BaseResult();
		reqParam.put("FLAG", "1");
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try
		{	
			
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam), "UTF-8");
			log.trace(ESAPIUtil.vaildLog("debit_apply_confirm.jsondc: " + jsondc));
			
			okMap.put("jsondc", jsondc);
			
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if (!bs.getResult()) {
					// TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				} else {
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			} else {
				// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
				String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
				log.trace("debit_apply_confirm.token: {}", token);
				if (StrUtil.isNotEmpty(okMap.get("TOKEN")) && okMap.get("TOKEN").equals(token)) {
					bs.setResult(Boolean.TRUE);
				}
				log.trace("bs.getResult(): {}", bs.getResult());
				if (!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				}
				
				// TOKEN驗證成功，資料處理後進入確認頁
				bs.reset();
				bs = creditcard_automatic_service.debit_apply_confirm(okMap);
				
				Map<String,String> result = new HashMap<String,String>();
				Map<String, String> dataMap = (Map<String,String>)bs.getData();
				// IDGATE transdata            		 
	            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	    		String adopid = "NI04";
	    		String title ="";
	    		title = "您有一筆信用卡款自動扣款申請/取消交易待確認";
	        	if(IdgateData != null) {
		            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
		            	result.putAll(dataMap);
						//2021/09/15修正
		            	if("2".equals(dataMap.get("APPLYFLAG"))) {
		            		//在MS層NI04 若APPLYFLAG 為2 會補上 TSFACN = 99999999999 , 造成IDGATE驗不過 , 故在此補上
		            		result.put("TSFACN", "99999999999");
		            	}
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(NI04_IDGATE_DATA.class, NI04_IDGATE_DATA_VIEW.class, result));
		                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
		            }
	        	}
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
				
				
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
			}
		}catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("card_autopay_apply_confirm error >> {}",e);
			bs.setSYSMessage(ResultCode.SYS_ERROR);
		} finally {
			if (null!=bs && bs.getResult()) {
				target = "/credit_automatic/debit_apply_confirm";
				// 這邊還要調整 不能存物件，要存成JSNO字串
				model.addAttribute(SessionUtil.TRANSFER_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, ((Map<String,Object>)bs.getData()).get("TXTOKEN"));

			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		log.trace("target >> {}", target);
		return target;
	}
	
	/**
	 * 信用卡款自動扣繳申請/取消
	 * 
	 * @return
	 */
	@RequestMapping(value = "/debit_apply_result")
	public String card_autopay_apply_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		String next = "/CREDIT/AUTOMATIC/debit_apply";
		log.trace(ESAPIUtil.vaildLog("reqParam {}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = null;
		// 驗證 result token 如果存在 且相同表示F5 或i18n
		try {
			bs = new BaseResult();
			// 解決 Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI

			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}

			if (!hasLocale) {
				log.trace("debit_apply_result.validate TXTOKEN...");
				log.trace("debit_apply_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("debit_apply_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("debit_apply_result.bs.step1 is successful...");
				}

				if (!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("debit_apply_result TXTOKEN 不正確，TXTOKEN>>{}" + okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						!okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("debit_apply_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("debit_apply_result TXTOKEN 不正確，或重複交易，TXTOKEN>>" + okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				
				// 取得從輸入頁輸入存在session中的輸入資料
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String, String> apply_data = (Map<String, String>) bs.getData();
				
				// 重置bs，進行交易
				bs.reset();
				
				//IDgate驗證		 
                try {		 
                    if(okMap.get("FGTXWAY").equals("7")) {		 
        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("NI04_IDGATE");
                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));	 
                    }
                }catch(Exception e) {		 
                    log.error("IDGATE TRANS error>>{}",e);		 
                }  
				
				okMap.putAll(apply_data);
				
				okMap.put("CUSIDN", new String(
						Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
						"utf-8"));
				
				bs = creditcard_automatic_service.debit_apply_result(okMap);
				
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			
			log.trace("debit_apply_result bs >>{}", bs.getData());

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("card_autopay_apply_result error >> {}",e);
		} finally {
			// TODO 要清除必要的SESSION
			
			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			// 交易成功要存之前的token 在Session 以利下次比對是否重複交易
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TXTOKEN"));
			if (bs.getResult()) {
				target = "/credit_automatic/debit_apply_result";
				model.addAttribute("debit_apply_result", bs);
				
			} else {
				// E004 是密碼錯誤 要讓使用者可以回上一步
				bs.setNext(next);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		log.trace("target >> {}", target);
		return target;
	}
		
		/**
		 * 取得轉帳之轉出帳號
		 * 
		 * @param request
		 * @param response
		 * @param reqParam
		 * @param model
		 * @return
		 */
		@RequestMapping(value = "/getOutAcno_aj", produces = { "application/json;charset=UTF-8" })
		public @ResponseBody BaseResult getOutACNO(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			String str = "getOutAcno_aj";
			log.trace("getOutAcno_aj >> {}", str);
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 要取得使用者相關帳號
			BaseResult bs = new BaseResult();
			try {
				// String sessionID = (String) SessionUtil.getAttribute(model,
				// SessionUtil.PORTAL_SESSION_ID, null);
				String cusidn = new String(
						Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
						"utf-8");
				String type = reqParam.get("type");
				bs = creditcard_automatic_service.getOutAcnoList(cusidn, type);

			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("getOutACNO error >> {}",e);
			}
			return bs;

		}
}
