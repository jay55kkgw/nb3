package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N855_REST_RS extends BaseRestBean_TW implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9148221417810826665L;

	private String	IBPD_Param ;
	private String	FOSRID ;
	private String	FOSTAN ;
	private String	FORCOD ;
	
	public String getIBPD_Param() {
		return IBPD_Param;
	}
	public void setIBPD_Param(String iBPD_Param) {
		IBPD_Param = iBPD_Param;
	}
	public String getFOSRID() {
		return FOSRID;
	}
	public void setFOSRID(String fOSRID) {
		FOSRID = fOSRID;
	}
	public String getFOSTAN() {
		return FOSTAN;
	}
	public void setFOSTAN(String fOSTAN) {
		FOSTAN = fOSTAN;
	}
	public String getFORCOD() {
		return FORCOD;
	}
	public void setFORCOD(String fORCOD) {
		FORCOD = fORCOD;
	}
}
