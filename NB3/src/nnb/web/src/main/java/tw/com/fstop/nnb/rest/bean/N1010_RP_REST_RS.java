package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N1010_RP_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4152363686746387737L;
	
    private String OFFSET;

    private String HEADER;

    private String BHRPResult;

	public String getOFFSET() {
		return OFFSET;
	}

	public String getHEADER() {
		return HEADER;
	}

	public String getBHRPResult() {
		return BHRPResult;
	}

	public void setOFFSET(String oFFSET) {
		OFFSET = oFFSET;
	}

	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}

	public void setBHRPResult(String bHRPResult) {
		BHRPResult = bHRPResult;
	}
    
    
	

}
