package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N175_IDGATE_DATA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -762951082705400880L;

	@SerializedName(value = "ACN")
	private String ACN;// 存單帳號
	@SerializedName(value = "FDPNUM")
	private String FDPNUM; // 存單號碼
	@SerializedName(value = "CUID")
	private String CUID;// 幣別
	@SerializedName(value = "AMT")
	private String AMT;// 存單金額
	@SerializedName(value = "ITR")
	private String ITR;// 利率
	
	@SerializedName(value = "INTMTH")
	private String INTMTH;// 計息方式
	@SerializedName(value = "DPISDT")
	private String DPISDT;// 起存日
	@SerializedName(value = "DUEDAT")
	private String DUEDAT;// 到期日
	@SerializedName(value = "INT")
	private String INT;// 利息
	@SerializedName(value = "TAX")
	private String TAX;// 所得稅
	
	@SerializedName(value = "PAIAFTX")
	private String PAIAFTX;// 稅後本息
	@SerializedName(value = "NHITAX")
	private String NHITAX;// 健保費
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getFDPNUM() {
		return FDPNUM;
	}
	public void setFDPNUM(String fDPNUM) {
		FDPNUM = fDPNUM;
	}
	public String getCUID() {
		return CUID;
	}
	public void setCUID(String cUID) {
		CUID = cUID;
	}
	public String getAMT() {
		return AMT;
	}
	public void setAMT(String aMT) {
		AMT = aMT;
	}
	public String getITR() {
		return ITR;
	}
	public void setITR(String iTR) {
		ITR = iTR;
	}
	public String getINTMTH() {
		return INTMTH;
	}
	public void setINTMTH(String iNTMTH) {
		INTMTH = iNTMTH;
	}
	public String getDPISDT() {
		return DPISDT;
	}
	public void setDPISDT(String dPISDT) {
		DPISDT = dPISDT;
	}
	public String getDUEDAT() {
		return DUEDAT;
	}
	public void setDUEDAT(String dUEDAT) {
		DUEDAT = dUEDAT;
	}
	public String getINT() {
		return INT;
	}
	public void setINT(String iNT) {
		INT = iNT;
	}
	public String getTAX() {
		return TAX;
	}
	public void setTAX(String tAX) {
		TAX = tAX;
	}
	public String getPAIAFTX() {
		return PAIAFTX;
	}
	public void setPAIAFTX(String pAIAFTX) {
		PAIAFTX = pAIAFTX;
	}
	public String getNHITAX() {
		return NHITAX;
	}
	public void setNHITAX(String nHITAX) {
		NHITAX = nHITAX;
	}
	
	
	




}
