package tw.com.fstop.nnb.spring.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fstop.orm.po.TXNADDRESSBOOK;
import fstop.orm.po.TXNTRACCSET;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.DaoService;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.TxnAddressBookDao;
import tw.com.fstop.tbb.nnb.dao.TxnTrAccSetDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.StrUtil;

@RestController
@RequestMapping(value = "/MB/TXNTRACCSET")
public class TxnTrAccSet_Controller {

	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	DaoService daoService;
	
	@Autowired
	private TxnTrAccSetDao txnTrAccSetDao;
	
	@Autowired
	I18n i18n;
	
	
	@RequestMapping(value = "/query" , method = {RequestMethod.POST} , produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult query(HttpServletRequest request, HttpServletResponse response, @RequestBody Map<String,String> reqParam, Model model) {
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			String cusidn = reqParam.get("cusidn");
			List<TXNTRACCSET> list = txnTrAccSetDao.getByUid(cusidn);
			log.trace(ESAPIUtil.vaildLog("accset >>{}"+ list));
			
			bs.reset();			
			bs.setData(list);
			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", i18n.getMsg("LB.X1805"));;
		} catch (Exception e) {
			log.error(e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", i18n.getMsg("LB.X2132"));
		}
		return bs;
		
	}
	
	@RequestMapping(value = "/add" , method = {RequestMethod.POST} , produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult add(HttpServletRequest request, HttpServletResponse response, @RequestBody Map<String,String> reqParam, Model model) {
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		
		try {
			TXNTRACCSET newAccSet = new TXNTRACCSET();
			DateTime d = new DateTime();
			
			newAccSet.setDPUSERID(reqParam.get("dpuserid"));
			newAccSet.setDPGONAME(reqParam.get("dpgoname"));
			newAccSet.setDPTRACNO(reqParam.get("dptracno"));
			newAccSet.setDPTRDACNO(reqParam.get("dptrdacno"));
			newAccSet.setDPTRIBANK(reqParam.get("dptribank"));
			//newAddrBook = CodeUtil.objectCovert(TXNADDRESSBOOK.class,reqParam);
			
			newAccSet.setLASTDATE(d.toString("yyyyMMdd"));
			newAccSet.setLASTTIME(d.toString("HHmmss"));
			txnTrAccSetDao.save(newAccSet);
			log.trace(ESAPIUtil.vaildLog("newAddrBook >>{}" + newAccSet));
			bs.reset();
			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", i18n.getMsg("LB.X0435"));
		} catch (Exception e) {
			log.error(e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", i18n.getMsg("LB.X2133"));
		}		
		return bs;
		
	}
	
	@RequestMapping(value = "/update" , method = {RequestMethod.POST} , produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult update(HttpServletRequest request, HttpServletResponse response, @RequestBody Map<String,String> reqParam, Model model) {
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			String dpaccsetid = reqParam.get("dpaccsetid").toString();
			TXNTRACCSET accSet = txnTrAccSetDao.get(TXNTRACCSET.class, new Integer(dpaccsetid));
			accSet.setDPUSERID(reqParam.get("dpuserid"));
			accSet.setDPGONAME(reqParam.get("dpgoname"));
			accSet.setDPTRACNO(reqParam.get("dptracno"));
			accSet.setDPTRDACNO(reqParam.get("dptrdacno"));
			accSet.setDPTRIBANK(reqParam.get("dptribank"));
			txnTrAccSetDao.saveOrUpdate(accSet);
			bs.reset();
			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", i18n.getMsg("LB.X0293"));
		} catch (Exception e) {
			log.error(e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", i18n.getMsg("LB.X2134"));
		}		
		return bs;
		
	}
	
	/**
	 * 網銀專用
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/updateNNB" , method = {RequestMethod.POST} , produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult updateNNB(HttpServletRequest request, HttpServletResponse response, @RequestBody Map<String,Object> reqParam, Model model) {
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		Map<String,String> old_map = null;
		Map<String,String> new_map = null;
		TXNTRACCSET accSet = null;
		String dptracno = null;
		try {
			old_map = (Map<String, String>) reqParam.get("old_data") ;
			new_map = (Map<String, String>) reqParam.get("new_data") ;
			accSet = txnTrAccSetDao.findByInput(old_map.get("dpuserid")  ,old_map.get("dptrdacno")  ,old_map.get("dptribank") );
//			轉大寫
			new_map.remove("dpaccsetid");
			toUpperCaseKey(new_map);
			toUpperCaseKey(old_map);
			if(accSet !=null) {
//				特殊狀況
				dptracno = old_map.get("DPTRACNO");
				old_map.clear();
				old_map = CodeUtil.objectCovert(Map.class,accSet);
				old_map.put("DPTRACNO", dptracno);
				old_map.putAll(new_map);
				log.debug("update.old_map>>{}",ESAPIUtil.vaildLog(CodeUtil.toJson(old_map)));
				accSet = CodeUtil.objectCovert(TXNTRACCSET.class,old_map);
				accSet.setLASTDATE(DateTime.now().toString("yyyyMMdd"));
				accSet.setLASTTIME(DateTime.now().toString("HHmmss"));
				log.debug("accSet>>{}",ESAPIUtil.vaildLog(CodeUtil.toJson(accSet)) );
				txnTrAccSetDao.update(accSet);
			}else{
				old_map.putAll(new_map);
				log.debug("save.old_map>>{}",ESAPIUtil.vaildLog(CodeUtil.toJson(old_map)));
				accSet=CodeUtil.objectCovert(TXNTRACCSET.class,old_map);
				accSet.setDPACCSETID(null);
				accSet.setLASTDATE(DateTime.now().toString("yyyyMMdd"));
				accSet.setLASTTIME(DateTime.now().toString("HHmmss"));
				log.debug("save.accSet>>{}",ESAPIUtil.vaildLog(CodeUtil.toJson(accSet)) );
				txnTrAccSetDao.save(accSet);
			}
			bs.reset();
			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", i18n.getMsg("LB.X0293"));
		} catch (Exception e) {
			log.error(e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", i18n.getMsg("LB.X2134"));
		}		
		return bs;
		
	}
	
	@RequestMapping(value = "/delete" , method = {RequestMethod.POST} , produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult delete(HttpServletRequest request, HttpServletResponse response, @RequestBody Map<String,String> reqParam, Model model) {
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			String dpaccsetid = reqParam.get("dpaccsetid");
			TXNTRACCSET delAccSet = new TXNTRACCSET();
			delAccSet.setDPACCSETID(new Integer(dpaccsetid));
			txnTrAccSetDao.remove(delAccSet);
			bs.reset();
			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", i18n.getMsg("LB.X0436"));
		} catch (Exception e) {
			log.error(e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", i18n.getMsg("LB.X2135"));
		}	
		return bs;
		
	}
	@RequestMapping(value = "/deleteNNB" , method = {RequestMethod.POST} , produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult deleteNNB(HttpServletRequest request, HttpServletResponse response, @RequestBody Map<String,String> reqParam, Model model) {
		log.info(ESAPIUtil.vaildLog("reqParam>>{}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		String dpuserid = null;
		String dpgoname = null;
		String dptracno = null;
		String dptrdacno = null;
		String dptribank = null;
		TXNTRACCSET accSet = null;
		
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			String[] reqKey = {"dpuserid","dptracno","dptrdacno","dptribank"};
			List<String> keyList = Arrays.asList(reqKey);
			for(String key :okMap.keySet()) {
				if(keyList.contains(key) &&  StrUtil.isEmpty(okMap.get(key)) ) {
					bs.setResult(Boolean.FALSE);
					bs.setMessage("1", key+"為必填。");
				}
			}
			dpuserid = okMap.get("dpuserid");
			dptracno = okMap.get("dptracno");
			dptrdacno = okMap.get("dptrdacno");
			dptribank = okMap.get("dptribank");
			
			accSet = txnTrAccSetDao.findByInput(dpuserid, dptracno, dptrdacno, dptribank);
			
			if(accSet ==null ) {
				bs.setResult(Boolean.FALSE);
				bs.setMessage("1", "查無資料，未執行刪除。");
				return bs;
			}
			txnTrAccSetDao.remove(accSet);
			bs.reset();
			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", i18n.getMsg("LB.X0436"));
		} catch (Exception e) {
			log.error(e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", i18n.getMsg("LB.X2135"));
		}	
		return bs;
		
	}
	
	
	/**
	 * map key 轉大寫
	 * @param map
	 */
	public void toUpperCaseKey(Map<String,String> map) {
		Map<String,String> tmpMap = new HashMap<String, String>();
		try {
			for(String key:map.keySet()) {
				tmpMap.put(key.toUpperCase(), map.get(key));
			}
			map.clear();
			map.putAll(tmpMap);
		} catch (Exception e) {
			log.error("{}",e);
		}
		
	}
	
}
