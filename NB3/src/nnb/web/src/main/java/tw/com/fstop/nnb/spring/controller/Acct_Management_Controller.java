package tw.com.fstop.nnb.spring.controller;

import java.util.Base64;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.Acct_Bond_Service;
import tw.com.fstop.nnb.service.Acct_Management_Service;
import tw.com.fstop.tbb.nnb.dao.AdmMsgCodeDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({ SessionUtil.PD, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, SessionUtil.PRINT_DATALISTMAP_DATA,
		SessionUtil.RESULT_LOCALE_DATA, SessionUtil.CONFIRM_LOCALE_DATA, SessionUtil.RESULT_LOCALE_DATA,
		SessionUtil.CUSIDN })
@Controller
@RequestMapping(value = "/NT/ACCT/MANAGEMENT")
public class Acct_Management_Controller
{

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Acct_Management_Service acct_Management_Service;

	@Autowired
	private AdmMsgCodeDao admMsgCodeDao;

	// N150-輕鬆理財存摺明細查詢
	@RequestMapping(value = "/passbook_details")
	public String passbook_details(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		// 清除切換語系時暫存的資料
		SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		String target = "/error";
		BaseResult bs = null;
		log.trace("passbook_details start~~~");
		try
		{
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.debug("cusidn: {} ", cusidn);
			bs = acct_Management_Service.passbook_details(cusidn);
			bs.addData("TODAY", DateUtil.getCurentDateTime("yyyy/MM/dd"));
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("passbook_details error >> {}",e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "/acct/passbook_details";
				log.debug("passbook_details bs Data {}", bs.getData());
				model.addAttribute("pbook_details", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 輕鬆理財存摺明細查詢結果
	@RequestMapping(value = "/passbook_details_result")
	public String passbook_details_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/error";
		BaseResult bs = null;
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		log.trace("passbook_details_result start~~~");
		try
		{
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult())
				{
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale)
			{
				bs.reset();
				bs = acct_Management_Service.passbook_details_result(cusidn, okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			log.debug(ESAPIUtil.vaildLog("reqParam >> " + CodeUtil.toJson(reqParam)));
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("passbook_details_result error >> {}",e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "/acct/passbook_details_result";
				log.debug("bs_get_data={}", bs.getData());
				List<Map<String, String>> dataListMap = ((List<Map<String, String>>) ((Map<String, Object>) bs.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("pbook_details_result", bs);
			}
			else
			{
				// 新增回上一頁的路徑
				bs.setPrevious("/NT/ACCT/MANAGEMENT/passbook_details");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 輕鬆理財存摺明細直接下載
	@RequestMapping(value = "/passbook_details_DirectDownload")
	public String passbook_details_directDownload(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("passbook_details_DirectDownload");
		String target = "/error";
		BaseResult bs = null;
		try
		{
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			bs = acct_Management_Service.PassbookDetailsdirectDownload(cusidn, okMap);
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("passbook_details_directDownload error >> {}",e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "forward:/directDownload";
				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap={}", dataMap);
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
				log.trace("rowListMap={}", rowListMap);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rowListMap);
				Map<String, Object> parameterMap = (Map<String, Object>) dataMap.get("parameterMap");
				request.setAttribute("parameterMap", parameterMap);
			}
			else
			{
				bs.setPrevious("/NT/ACCT/MANAGEMENT/passbook_details");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// N640-輕鬆理財證券交割明細
	@RequestMapping(value = "/passbook_securities_details")
	public String passbook_securities_details(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/error";
		BaseResult bs = null;
		log.trace("passbook_securities_details start~~~");
		try
		{
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.debug("cusidn: {} ", cusidn);
			bs = acct_Management_Service.passbook_securities_details(cusidn);
			bs.addData("TODAY", DateUtil.getCurentDateTime("yyyy/MM/dd"));
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("passbook_securities_details error >> {}",e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "/acct/passbook_securities_details";
				log.debug("passbook_securities_details bs getData {}", bs.getData());
				model.addAttribute("pbook_securities_details", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 輕鬆理財證券交割明細結果頁
	@RequestMapping(value = "/passbook_securities_details_result")
	public String passbook_securities_details_q_r(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/error";
		BaseResult bs = null;
		log.trace("passbook_securities_details_q_r start~~~");
		try
		{
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.debug(ESAPIUtil.vaildLog("reqParam >> " + CodeUtil.toJson(reqParam)));
			log.debug("cusidn" + cusidn);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult())
				{
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale)
			{
				bs.reset();
				bs = acct_Management_Service.passbook_securities_details_result(cusidn, reqParam);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("passbook_securities_details_q_r error >> {}",e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "/acct/passbook_securities_details_result";
				log.debug("bs_get_data={}", bs.getData());
				List<Map<String, String>> dataListMap = ((List<Map<String, String>>) ((Map<String, Object>) bs.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("pbook_securities_details_result", bs);
			}
			else
			{
				// 新增回上一頁的路徑
				bs.setPrevious("/NT/ACCT/MANAGEMENT/passbook_securities_details");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 輕鬆理財存摺明細直接下載
	@RequestMapping(value = "/passbook_securities_details_DirectDownload")
	public String passbook_securities_details_DirectDownload(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("passbook_securities_details_DirectDownload start");
		String target = "/error";
		BaseResult bs = null;
		try
		{
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			bs = acct_Management_Service.passbook_securities_details_DirectDownload(cusidn, okMap);
		}
		catch (Exception e)
		{
			log.error("passbook_securities_details_DirectDownload Exception", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "forward:/directDownload";
				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap={}", dataMap);
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
				log.trace("rowListMap={}", rowListMap);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rowListMap);
				Map<String, Object> parameterMap = (Map<String, Object>) dataMap.get("parameterMap");
				request.setAttribute("parameterMap", parameterMap);
			}
			else
			{
				bs.setPrevious("/NT/ACCT/MANAGEMENT/passbook_securities_details");
				model.addAttribute(BaseResult.ERROR, bs);
			}

		}
		return target;
	}

	// 輕鬆理財自動申購基金明細
	@RequestMapping(value = "/purchase_fund_details")
	public String purchase_fund_details(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/error";
		BaseResult bs = null;
		log.trace("purchase_fund_details >>");
		String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
		try
		{
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			bs = acct_Management_Service.N920_REST(cusidn, Acct_Bond_Service.PURCHASE_FUND_DETAILS);
			Map<String, Object> dataMap = (Map) bs.getData();
			List<String> rowListMap = (List<String>) dataMap.get("REC");
			log.debug("REC size >> {}", rowListMap.size());
			if (rowListMap.size() == 0)
			{
				bs.setErrorMessage("Z999", admMsgCodeDao.errorMsg("Z999"));
				bs.setResult(Boolean.FALSE);
			}
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("purchase_fund_details error >> {}",e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "/acct/purchase_fund_details";
				model.addAttribute("username_alter_result", bs);
			}
			else
			{
				bs.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 輕鬆理財自動申購基金明細結果
	@RequestMapping(value = "/purchase_fund_details_result")
	public String purchase_fund_details_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/error";
		BaseResult bs = null;
		log.trace("purchase_fund_details_result>>");
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
		try
		{
			bs = new BaseResult();
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult())
				{
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale)
			{
				bs.reset();
				bs = acct_Management_Service.purchase_fund_details_result(cusidn, okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("purchase_fund_details_result error >> {}",e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "/acct/purchase_fund_details_result";
				List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("loan_detail", bs);
			}
			else
			{
				// 新增回上一頁的路徑
				bs.setPrevious("/NT/ACCT/MANAGEMENT/purchase_fund_details");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
}