package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * C020REST，含C020及C026電文RS
 */
public class C020_REST_RS extends BaseRestBean implements Serializable{
	private static final long serialVersionUID = 6791228050721094802L;
	
	private String TRNDATE;
	private String TRNTIME;
	private String TYPE;
	private String TRANSCODE;
	private String COUNTRYTYPE;
	private String TRADEDATE;
	private String AMT3;
	private String FCA2;
	private String AMT5;
	private String OUTACN;
	private String INTSACN;
	private String BILLSENDMODE;
	private String FCAFEE;
	private String SSLTXNO;
	private String PAYDAY1;
	private String PAYDAY2;
	private String PAYDAY3;
	private String BRHCOD;
	private String CUTTYPE;
	private String CRY1;
	private String CRY;
	private String B_TOTBAL;
	private String B_TOTBAL_S;
	private String B_AVLBAL;
	private String B_AVLBAL_S;
	private String AMOUNT;
	private String A_TOTBAL;
	private String A_TOTBAL_S;
	private String A_AVLBAL;
	private String A_AVLBAL_S;
	private String PRIZETIMES;
	private String DATE;
	private String TIME;
	private String CUSIDN;
	private String CUSNAME;
	private String FUNDACN;
	private String CUR;
	private String DBDATE;
	private String SALESNO;
	private String STOP;
	private String YIELD;
	private String PAYDAY4;
	private String PAYDAY5;
	private String PAYDAY6;
	private String MIP;
	private String RSKATT;
	private String RRSK;
	private String PAYDAY7;
	private String PAYDAY8;
	private String PAYDAY9;
	private String MAC;
	private String CMQTIME;
	
	public String getTRNDATE(){
		return TRNDATE;
	}
	public void setTRNDATE(String tRNDATE){
		TRNDATE = tRNDATE;
	}
	public String getTRNTIME(){
		return TRNTIME;
	}
	public void setTRNTIME(String tRNTIME){
		TRNTIME = tRNTIME;
	}
	public String getTYPE(){
		return TYPE;
	}
	public void setTYPE(String tYPE){
		TYPE = tYPE;
	}
	public String getTRANSCODE(){
		return TRANSCODE;
	}
	public void setTRANSCODE(String tRANSCODE){
		TRANSCODE = tRANSCODE;
	}
	public String getCOUNTRYTYPE(){
		return COUNTRYTYPE;
	}
	public void setCOUNTRYTYPE(String cOUNTRYTYPE){
		COUNTRYTYPE = cOUNTRYTYPE;
	}
	public String getTRADEDATE(){
		return TRADEDATE;
	}
	public void setTRADEDATE(String tRADEDATE){
		TRADEDATE = tRADEDATE;
	}
	public String getAMT3(){
		return AMT3;
	}
	public void setAMT3(String aMT3){
		AMT3 = aMT3;
	}
	public String getFCA2(){
		return FCA2;
	}
	public void setFCA2(String fCA2){
		FCA2 = fCA2;
	}
	public String getAMT5(){
		return AMT5;
	}
	public void setAMT5(String aMT5){
		AMT5 = aMT5;
	}
	public String getOUTACN(){
		return OUTACN;
	}
	public void setOUTACN(String oUTACN){
		OUTACN = oUTACN;
	}
	public String getINTSACN(){
		return INTSACN;
	}
	public void setINTSACN(String iNTSACN){
		INTSACN = iNTSACN;
	}
	public String getBILLSENDMODE(){
		return BILLSENDMODE;
	}
	public void setBILLSENDMODE(String bILLSENDMODE){
		BILLSENDMODE = bILLSENDMODE;
	}
	public String getFCAFEE(){
		return FCAFEE;
	}
	public void setFCAFEE(String fCAFEE){
		FCAFEE = fCAFEE;
	}
	public String getSSLTXNO(){
		return SSLTXNO;
	}
	public void setSSLTXNO(String sSLTXNO){
		SSLTXNO = sSLTXNO;
	}
	public String getPAYDAY1(){
		return PAYDAY1;
	}
	public void setPAYDAY1(String pAYDAY1){
		PAYDAY1 = pAYDAY1;
	}
	public String getPAYDAY2(){
		return PAYDAY2;
	}
	public void setPAYDAY2(String pAYDAY2){
		PAYDAY2 = pAYDAY2;
	}
	public String getPAYDAY3(){
		return PAYDAY3;
	}
	public void setPAYDAY3(String pAYDAY3){
		PAYDAY3 = pAYDAY3;
	}
	public String getBRHCOD(){
		return BRHCOD;
	}
	public void setBRHCOD(String bRHCOD){
		BRHCOD = bRHCOD;
	}
	public String getCUTTYPE(){
		return CUTTYPE;
	}
	public void setCUTTYPE(String cUTTYPE){
		CUTTYPE = cUTTYPE;
	}
	public String getCRY1(){
		return CRY1;
	}
	public void setCRY1(String cRY1){
		CRY1 = cRY1;
	}
	public String getPRIZETIMES(){
		return PRIZETIMES;
	}
	public void setPRIZETIMES(String pRIZETIMES){
		PRIZETIMES = pRIZETIMES;
	}
	public String getCRY(){
		return CRY;
	}
	public void setCRY(String cRY){
		CRY = cRY;
	}
	public String getB_TOTBAL(){
		return B_TOTBAL;
	}
	public void setB_TOTBAL(String b_TOTBAL){
		B_TOTBAL = b_TOTBAL;
	}
	public String getB_TOTBAL_S(){
		return B_TOTBAL_S;
	}
	public void setB_TOTBAL_S(String b_TOTBAL_S){
		B_TOTBAL_S = b_TOTBAL_S;
	}
	public String getB_AVLBAL(){
		return B_AVLBAL;
	}
	public void setB_AVLBAL(String b_AVLBAL){
		B_AVLBAL = b_AVLBAL;
	}
	public String getB_AVLBAL_S(){
		return B_AVLBAL_S;
	}
	public void setB_AVLBAL_S(String b_AVLBAL_S){
		B_AVLBAL_S = b_AVLBAL_S;
	}
	public String getAMOUNT(){
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT){
		AMOUNT = aMOUNT;
	}
	public String getA_TOTBAL(){
		return A_TOTBAL;
	}
	public void setA_TOTBAL(String a_TOTBAL){
		A_TOTBAL = a_TOTBAL;
	}
	public String getA_TOTBAL_S(){
		return A_TOTBAL_S;
	}
	public void setA_TOTBAL_S(String a_TOTBAL_S){
		A_TOTBAL_S = a_TOTBAL_S;
	}
	public String getA_AVLBAL(){
		return A_AVLBAL;
	}
	public void setA_AVLBAL(String a_AVLBAL){
		A_AVLBAL = a_AVLBAL;
	}
	public String getA_AVLBAL_S(){
		return A_AVLBAL_S;
	}
	public void setA_AVLBAL_S(String a_AVLBAL_S){
		A_AVLBAL_S = a_AVLBAL_S;
	}
	public String getDATE(){
		return DATE;
	}
	public void setDATE(String dATE){
		DATE = dATE;
	}
	public String getTIME(){
		return TIME;
	}
	public void setTIME(String tIME){
		TIME = tIME;
	}
	public String getCUSIDN(){
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN){
		CUSIDN = cUSIDN;
	}
	public String getCUSNAME(){
		return CUSNAME;
	}
	public void setCUSNAME(String cUSNAME){
		CUSNAME = cUSNAME;
	}
	public String getFUNDACN(){
		return FUNDACN;
	}
	public void setFUNDACN(String fUNDACN){
		FUNDACN = fUNDACN;
	}
	public String getCUR(){
		return CUR;
	}
	public void setCUR(String cUR){
		CUR = cUR;
	}
	public String getDBDATE(){
		return DBDATE;
	}
	public void setDBDATE(String dBDATE){
		DBDATE = dBDATE;
	}
	public String getSALESNO(){
		return SALESNO;
	}
	public void setSALESNO(String sALESNO){
		SALESNO = sALESNO;
	}
	public String getSTOP(){
		return STOP;
	}
	public void setSTOP(String sTOP){
		STOP = sTOP;
	}
	public String getYIELD(){
		return YIELD;
	}
	public void setYIELD(String yIELD){
		YIELD = yIELD;
	}
	public String getPAYDAY4(){
		return PAYDAY4;
	}
	public void setPAYDAY4(String pAYDAY4){
		PAYDAY4 = pAYDAY4;
	}
	public String getPAYDAY5(){
		return PAYDAY5;
	}
	public void setPAYDAY5(String pAYDAY5){
		PAYDAY5 = pAYDAY5;
	}
	public String getPAYDAY6(){
		return PAYDAY6;
	}
	public void setPAYDAY6(String pAYDAY6){
		PAYDAY6 = pAYDAY6;
	}
	public String getMIP(){
		return MIP;
	}
	public void setMIP(String mIP){
		MIP = mIP;
	}
	public String getMAC(){
		return MAC;
	}
	public void setMAC(String mAC){
		MAC = mAC;
	}
	public String getRSKATT(){
		return RSKATT;
	}
	public void setRSKATT(String rSKATT){
		RSKATT = rSKATT;
	}
	public String getRRSK(){
		return RRSK;
	}
	public void setRRSK(String rRSK){
		RRSK = rRSK;
	}
	public String getPAYDAY7(){
		return PAYDAY7;
	}
	public void setPAYDAY7(String pAYDAY7){
		PAYDAY7 = pAYDAY7;
	}
	public String getPAYDAY8(){
		return PAYDAY8;
	}
	public void setPAYDAY8(String pAYDAY8){
		PAYDAY8 = pAYDAY8;
	}
	public String getPAYDAY9(){
		return PAYDAY9;
	}
	public void setPAYDAY9(String pAYDAY9){
		PAYDAY9 = pAYDAY9;
	}
	public String getCMQTIME(){
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME){
		CMQTIME = cMQTIME;
	}
}