package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N815_REST_RSDATA extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6545619543989700259L;

	private String CUSNUM;
	private String TYPE;
	private String CARDNUM;
	
	public String getCUSNUM() {
		return CUSNUM;
	}
	public String getTYPE() {
		return TYPE;
	}
	public String getCARDNUM() {
		return CARDNUM;
	}
	public void setCUSNUM(String cUSNUM) {
		CUSNUM = cUSNUM;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public void setCARDNUM(String cARDNUM) {
		CARDNUM = cARDNUM;
	}
}
