package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N566_REST_RSDATA extends BaseRestBean implements Serializable{

	private String RREMITAM;	//匯款金額
	private String MEMO3;
	private String MEMO2;
	private String MEMO4;
	private String RORDCUS1;	//收款人名稱
	private String MEMO1;		//附言
	private String RVALDATE;	//付款日
	private String DETCHG;		//手續費負擔別
	private String RREFNO;		//交易編號
	private String RAWB1AD1;	//收款人銀行名稱
	private String RREMITCY;	//幣別
	private String RBENAC;		//收款人帳號
	private String RREGDATE;	//匯款日
	private String CUSIDN;
	private String RRATE;		//匯率
	public String getRREMITAM() {
		return RREMITAM;
	}
	public void setRREMITAM(String rREMITAM) {
		RREMITAM = rREMITAM;
	}
	public String getMEMO3() {
		return MEMO3;
	}
	public void setMEMO3(String mEMO3) {
		MEMO3 = mEMO3;
	}
	public String getMEMO2() {
		return MEMO2;
	}
	public void setMEMO2(String mEMO2) {
		MEMO2 = mEMO2;
	}
	public String getMEMO4() {
		return MEMO4;
	}
	public void setMEMO4(String mEMO4) {
		MEMO4 = mEMO4;
	}
	public String getRORDCUS1() {
		return RORDCUS1;
	}
	public void setRORDCUS1(String rORDCUS1) {
		RORDCUS1 = rORDCUS1;
	}
	public String getMEMO1() {
		return MEMO1;
	}
	public void setMEMO1(String mEMO1) {
		MEMO1 = mEMO1;
	}
	public String getRVALDATE() {
		return RVALDATE;
	}
	public void setRVALDATE(String rVALDATE) {
		RVALDATE = rVALDATE;
	}
	public String getDETCHG() {
		return DETCHG;
	}
	public void setDETCHG(String dETCHG) {
		DETCHG = dETCHG;
	}
	public String getRREFNO() {
		return RREFNO;
	}
	public void setRREFNO(String rREFNO) {
		RREFNO = rREFNO;
	}
	public String getRAWB1AD1() {
		return RAWB1AD1;
	}
	public void setRAWB1AD1(String rAWB1AD1) {
		RAWB1AD1 = rAWB1AD1;
	}
	public String getRREMITCY() {
		return RREMITCY;
	}
	public void setRREMITCY(String rREMITCY) {
		RREMITCY = rREMITCY;
	}
	public String getRBENAC() {
		return RBENAC;
	}
	public void setRBENAC(String rBENAC) {
		RBENAC = rBENAC;
	}
	public String getRREGDATE() {
		return RREGDATE;
	}
	public void setRREGDATE(String rREGDATE) {
		RREGDATE = rREGDATE;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getRRATE() {
		return RRATE;
	}
	public void setRRATE(String rRATE) {
		RRATE = rRATE;
	}
}
