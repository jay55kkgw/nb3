package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N559_REST_RSDATA extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2496154579991130006L;

	private String RBILLAMT; //到單金額
	private String RCFMDATE; //承兌到期日
	private String RDWRNM; //出口商名稱
	private String RMARK; //備註
	private String RICNO; //託收編號
	private String RDRFTDAY; //匯票天期
	private String RCADATE; //託收日期
	private String RPAYTERM; //類別
	private String RRMTBKNM; //託收銀行名稱
	private String RBILLCCY; //幣別
	
	
	public String getRBILLAMT() {
		return RBILLAMT;
	}
	public void setRBILLAMT(String rBILLAMT) {
		RBILLAMT = rBILLAMT;
	}
	public String getRCFMDATE() {
		return RCFMDATE;
	}
	public void setRCFMDATE(String rCFMDATE) {
		RCFMDATE = rCFMDATE;
	}
	public String getRDWRNM() {
		return RDWRNM;
	}
	public void setRDWRNM(String rDWRNM) {
		RDWRNM = rDWRNM;
	}
	public String getRMARK() {
		return RMARK;
	}
	public void setRMARK(String rMARK) {
		RMARK = rMARK;
	}
	public String getRICNO() {
		return RICNO;
	}
	public void setRICNO(String rICNO) {
		RICNO = rICNO;
	}
	public String getRDRFTDAY() {
		return RDRFTDAY;
	}
	public void setRDRFTDAY(String rDRFTDAY) {
		RDRFTDAY = rDRFTDAY;
	}
	public String getRCADATE() {
		return RCADATE;
	}
	public void setRCADATE(String rCADATE) {
		RCADATE = rCADATE;
	}
	public String getRPAYTERM() {
		return RPAYTERM;
	}
	public void setRPAYTERM(String rPAYTERM) {
		RPAYTERM = rPAYTERM;
	}
	public String getRRMTBKNM() {
		return RRMTBKNM;
	}
	public void setRRMTBKNM(String rRMTBKNM) {
		RRMTBKNM = rRMTBKNM;
	}
	public String getRBILLCCY() {
		return RBILLCCY;
	}
	public void setRBILLCCY(String rBILLCCY) {
		RBILLCCY = rBILLCCY;
	}
	
}
