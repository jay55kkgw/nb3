package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N077_REST_RS extends BaseRestBean implements Serializable 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8653196513279301606L;
	
	private String CMQTIME;//交易時間
	
	private String OFFSET;//空白
	
	private String HEADER;
	
	private String SYNC;//Sync.Check Item
	
	private String MSGCOD;//MSGCOD
	
	private String FDPACN;//存單帳號
	
	private String FDPNUM;//存單號碼
	
	private String AMOUNT;//存單金額
	
	private String FDPACC;//存單種類 1.定期存款 2.存本取息 3.整存整付
	
	private String TERM;//存單期別
	
	private String INTMTH;//計息方式（’0’機動，’1’固定）	
	
	private String TRNCNT;//轉期次數
	
	private String CODE;//本息處理方式
	
	private String TSFACN;//轉帳帳號
	
	private String MAC;

	public String getOFFSET()
	{
		return OFFSET;
	}

	public void setOFFSET(String oFFSET)
	{
		OFFSET = oFFSET;
	}

	public String getHEADER()
	{
		return HEADER;
	}

	public void setHEADER(String hEADER)
	{
		HEADER = hEADER;
	}

	public String getSYNC()
	{
		return SYNC;
	}

	public void setSYNC(String sYNC)
	{
		SYNC = sYNC;
	}

	public String getMSGCOD()
	{
		return MSGCOD;
	}

	public void setMSGCOD(String mSGCOD)
	{
		MSGCOD = mSGCOD;
	}

	public String getFDPACN()
	{
		return FDPACN;
	}

	public void setFDPACN(String fDPACN)
	{
		FDPACN = fDPACN;
	}

	public String getFDPNUM()
	{
		return FDPNUM;
	}

	public void setFDPNUM(String fDPNUM)
	{
		FDPNUM = fDPNUM;
	}

	public String getAMOUNT()
	{
		return AMOUNT;
	}

	public void setAMOUNT(String aMOUNT)
	{
		AMOUNT = aMOUNT;
	}

	public String getFDPACC()
	{
		return FDPACC;
	}

	public void setFDPACC(String fDPACC)
	{
		FDPACC = fDPACC;
	}

	public String getTERM()
	{
		return TERM;
	}

	public void setTERM(String tERM)
	{
		TERM = tERM;
	}

	public String getINTMTH()
	{
		return INTMTH;
	}

	public void setINTMTH(String iNTMTH)
	{
		INTMTH = iNTMTH;
	}

	public String getTRNCNT()
	{
		return TRNCNT;
	}

	public void setTRNCNT(String tRNCNT)
	{
		TRNCNT = tRNCNT;
	}

	public String getCODE()
	{
		return CODE;
	}

	public void setCODE(String cODE)
	{
		CODE = cODE;
	}

	public String getTSFACN()
	{
		return TSFACN;
	}

	public void setTSFACN(String tSFACN)
	{
		TSFACN = tSFACN;
	}

	public String getCMQTIME()
	{
		return CMQTIME;
	}

	public void setCMQTIME(String cMQTIME)
	{
		CMQTIME = cMQTIME;
	}

	public String getMAC()
	{
		return MAC;
	}

	public void setMAC(String mAC)
	{
		MAC = mAC;
	}
	

}
