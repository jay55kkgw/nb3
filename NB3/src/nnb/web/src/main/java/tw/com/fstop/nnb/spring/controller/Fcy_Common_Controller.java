package tw.com.fstop.nnb.spring.controller;

import java.util.Base64;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.common.ResultCode;
import tw.com.fstop.nnb.service.Fcy_Common_Service;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.web.util.WebUtil;

/**
 * 
 * 功能說明 : 外匯共用
 *
 */
@SessionAttributes({ SessionUtil.CUSIDN, SessionUtil.DPMYEMAIL, SessionUtil.STEP1_LOCALE_DATA,
		SessionUtil.CONFIRM_LOCALE_DATA, SessionUtil.RESULT_LOCALE_DATA, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN,
		SessionUtil.TRANSFER_DATA, SessionUtil.FXCERTPREVIEW_DATA, SessionUtil.NULFLAG_FX })
@Controller
@RequestMapping(value = "/FCY/COMMON")
public class Fcy_Common_Controller
{

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Fcy_Common_Service fcy_Common_Service;

	/**
	 * 判斷是否為營業時間---Ajax
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/isFxBizTime_aj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult isFxBizTime_aj(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("isFxBizTime start");
		log.trace(ESAPIUtil.vaildLog("isFxBizTime >>{}" + CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try
		{
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "UTF-8");
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			bs = fcy_Common_Service.isFxBizTime();
			log.trace("isFxBizTime bs.getData>>{}", bs.getData());
		}
		catch (Exception e)
		{
			log.error("isFxBizTime Error ", e);
		}

		return bs;
	}

	/**
	 * 要取得傳出帳號幣別餘額相關資料 ---Ajax
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getACNO_Currency_Data_aj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult getACNO_Data(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("getACNO_Currency_Data_aj start");
		log.trace(ESAPIUtil.vaildLog("reqParam>>{}"+ CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try
		{
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "UTF-8");
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			String acno = okMap.get("acno");
			String cry = okMap.get("cry");
			
			log.trace(ESAPIUtil.vaildLog("getACNO_Data.acno: {}" + acno));
			log.trace(ESAPIUtil.vaildLog("getACNO_Data.cry: {}" + cry));
			
			// 幣別為新臺幣發N110
			if("TWD".equals(cry)) {
				bs = fcy_Common_Service.findOenByN110(cusidn, acno, null);
				Map<String, Object> dataMap = (Map<String, Object>) bs.getData();
				
				Map<String, Object> accno_data = (Map<String, Object>) dataMap.get("accno_data");
				log.trace("getACNO_Currency_Data_aj.accno_data: {}", accno_data);
				
				String adpibal = String.valueOf( accno_data.get("ADPIBAL") );
				log.trace("getACNO_Currency_Data_aj.adpibal: {}", adpibal);
				
				accno_data.put("AVAILABLE", adpibal);
				dataMap.replace("accno_data", accno_data);
				bs.setData(dataMap);
				
			} // 其他皆為外幣發N510
			else {
				bs = fcy_Common_Service.findOenByN510(cusidn, acno, cry);
			}
			log.trace("getACNO_Currency_Data_aj bs.getData>>{}", bs.getData());
		}
		catch (Exception e)
		{
			log.error("getACNO_Currency_Data_aj Error ", e);
		}

		return bs;
	}

	/**
	 * 
	 * 
	 * 取得幣別清單---Ajax
	 * 
	 * @param reqParama
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getCRYList", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult getCRYList(Model model)
	{
		BaseResult bs = new BaseResult();
		try
		{
			bs = fcy_Common_Service.getCRYList();
		}
		catch (Exception e)
		{
			log.error("getCRYList Error ", e);
		}

		return bs;
	}

	/**
	 * 
	 * 
	 * 一般網銀分行聯絡方式資料檔擷取---Ajax
	 * 
	 * @param reqParama
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getBhContactResult", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult getBhContactResult(@RequestParam Map<String, String> reqParam, Model model)
	{
		BaseResult bs = new BaseResult();
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			String bhid = okMap.get("bhid");

			bs = fcy_Common_Service.getBhContactResult(bhid);
		}
		catch (Exception e)
		{
			log.error("getBhContactResult Error ", e);
		}

		return bs;
	}

	/**
	 * 取得收款行(人)相關資訊----Ajax
	 * 
	 * @param reqParama
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getRcvInfo", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult getRcvInfo(@RequestParam Map<String, String> reqParam, Model model)
	{
		BaseResult bs = new BaseResult();
		try
		{
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			log.debug(ESAPIUtil.vaildLog("getRcvInforeqParam>>>>{}"+ CodeUtil.toJson(reqParam)));
			String acn_val = reqParam.get("acn_val");
			log.debug(ESAPIUtil.vaildLog("getRcvInforeacn_val>>>>{}"+ acn_val));
			bs = fcy_Common_Service.getRcvInfo(cusidn, acn_val);
		}
		catch (Exception e)
		{
			log.error("getCRYList Error ", e);
		}

		return bs;
	}

	/**
	 * 修改匯款項目常用選項----Ajax
	 * 
	 * @param reqParama
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/fxRemitQueryAj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult fxRemitQueryAj(@RequestParam Map<String, String> reqParam, Model model)
	{
		BaseResult bs = new BaseResult();
		try
		{
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			log.debug(ESAPIUtil.vaildLog("fxRemitQueryAj  okMap>>>>{}" + CodeUtil.toJson(okMap)));
			//
			bs = fcy_Common_Service.fxRemitQueryAj(cusidn, okMap);
		}
		catch (Exception e)
		{
			log.error("fxRemitQueryAj Error ", e);
		}

		return bs;
	}

	/**
	 * 匯款分類編號查詢 (大項分類)
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/FxRemitQuery")
	public String FxRemitQuery(@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/acct_fcy/FxRemitQuery";
		log.trace("FxRemitQuery");
		BaseResult bs = new BaseResult();
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			String nulflag = okMap.get("NULFLAG");
			SessionUtil.addAttribute(model, SessionUtil.NULFLAG_FX, nulflag);
			
			bs = fcy_Common_Service.FxRemitQuery(okMap);
			if (bs.getResult())
			{
				model.addAttribute("FxRemitQuery", bs);
				//新增FROM_FCY_REMITTANCES欄位，分辨是不是從外幣的匯出匯款頁面進入此頁面
				model.addAttribute("FROM_FCY_REMITTANCES", okMap.get("FROM_FCY_REMITTANCES"));
			}
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("FxRemitQuery error >> {}",e);
			bs.setMessage(ResultCode.SYS_ERROR);
		}
		finally
		{
			// target = "/acct_fcy/FxRemitQuery";
		}
		return target;
	}

	/**
	 * 匯款分類編號查詢 (中項分類)
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/FxRemitQuery_1")
	public String FxRemitQuery_1(@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/acct_fcy/FxRemitQuery_1";
		log.trace("FxRemitQuery_1");
		BaseResult bs = new BaseResult();
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			//
			bs = fcy_Common_Service.FxRemitQuery_1(okMap);
			if (bs.getResult())
			{
				Map<String, Object> callDataadmCurrencyList = (Map) bs.getData();
				int i_Count = (int) callDataadmCurrencyList.get("i_Count");
				if (i_Count == 0)
				{
					reqParam.put("ADMKINDID", "00");
					bs = fcy_Common_Service.FxRemitQuery_2(reqParam);
					target = "/acct_fcy/FxRemitQuery_2";
					if (bs.getResult())
					{
						model.addAttribute("FxRemitQuery_2", bs);
					}
				}
				else
				{
					model.addAttribute("FxRemitQuery_1", bs);
				}
			}
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("FxRemitQuery_1 error >> {}",e);
			bs.setMessage(ResultCode.SYS_ERROR);
		}
		finally
		{
			// target = "/acct_fcy/FxRemitQuery_1";
		}
		return target;
	}

	/**
	 * 匯款分類編號查詢 (小項分類)
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/FxRemitQuery_2")
	public String FxRemitQuery_2(@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/acct_fcy/FxRemitQuery_2";
		log.trace("FxRemitQuery_2");
		BaseResult bs = new BaseResult();
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			String nulflag = (String) SessionUtil.getAttribute(model, SessionUtil.NULFLAG_FX, null);
			okMap.put("NULFLAG", nulflag);

			bs = fcy_Common_Service.FxRemitQuery_2(okMap);
			if (bs.getResult())
			{
				model.addAttribute("FxRemitQuery_2", bs);
			}
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("FxRemitQuery_2 error >> {}",e);
			bs.setMessage(ResultCode.SYS_ERROR);
		}
		finally
		{
			// target = "/acct_fcy/FxRemitQuery_2";
		}
		return target;
	}

	/**
	 *
	 * 常用 匯款分類編號查詢
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/MyRemitMenu")
	public String MyRemitMenu(@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/acct_fcy/MyRemitMenu";
		log.trace("MyRemitMenu start");
		BaseResult bs = new BaseResult();
		try
		{
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			bs = fcy_Common_Service.MyRemitMenu(cusidn);
			if (bs.getResult())
			{
				model.addAttribute("MyRemitMenu", bs);
			}
		}
		catch (Exception e)
		{
			log.error("MyRemitMenu() error >>>> {}", e);
			bs.setMessage(ResultCode.SYS_ERROR);
		}
		finally
		{
		}
		return target;
	}

	/**
	 * 交易單據
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/fxcertpreview")
	public String fxcertpreview(@RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/error";
		log.trace("fxcertpreview...");
		BaseResult bs = new BaseResult();
		try
		{

			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			log.debug(ESAPIUtil.vaildLog("fxcertpreview in  >>>>> {}" + CodeUtil.toJson(okMap)));

			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.FXCERTPREVIEW_DATA, null));
			}
			else
			{
				// 交易編號
				String adtxno = okMap.get("ADTXNO");
				// 功能代碼
				String txid = okMap.get("TXID");
				// 登入時的身分證字號
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");

				// 外匯結構售
				if ("F001".equals(txid))
				{
					// 交易資料
					bs = fcy_Common_Service.getFxcertJson(adtxno, cusidn);
					model.addAttribute("fxcertpreview", bs);
					target = "/acct_fcy/F001CertPreview";
				}
				if ("F002".equals(txid))
				{
					// 交易資料
					bs = fcy_Common_Service.getFxcertJson(adtxno, cusidn);
					model.addAttribute("fxcertpreview", bs);
					target = "/acct_fcy/F002CertPreview";
				}
				if ("F003".equals(txid))
				{
					// 交易資料
					bs = fcy_Common_Service.getFxcertJson(adtxno, cusidn);
					model.addAttribute("fxcertpreview", bs);
					target = "/acct_fcy/F003CertPreview";
				}
				else
				{
					model.addAttribute(BaseResult.ERROR, bs);
				}
				
				
				// 2.5資料另外顯示
				if ("1".equals(bs.getMsgCode())) {
					model.addAttribute("fxcertpreview", bs);
					if ("F001".equals(txid)) {
						target = "/acct_fcy/F001CertPreview_25";
						
					} else if ("F002".equals(txid)) {
						target = "/acct_fcy/F002CertPreview_25";
						
					} else if ("F003".equals(txid)) {
						target = "/acct_fcy/F003CertPreview_25";
					}
				}
				
				
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.FXCERTPREVIEW_DATA, bs);
				
				log.debug("fxcertpreview target {}",target);
				log.debug("fxcertpreview bs {}",bs.getData());
			}
		}
		catch (Exception e)
		{
			log.error("fxcertpreview {}", e);
		}
		return target;
	}
}
