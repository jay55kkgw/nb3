package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class NP10_REST_RQ extends BaseRestBean_TW implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4089917798346175585L;
	
	
	private String CUSIDN;	// 統一編號
	private String IP;	// IP位置
	
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}

	
}
