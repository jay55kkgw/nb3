package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N855canceldata_REST_RS  extends BaseRestBean_TW implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6066457951560312122L;
	/**
	 * 
	 */

	private String	IBPD_Param ;
	public String getIBPD_Param() {
		return IBPD_Param;
	}
	public void setIBPD_Param(String iBPD_Param) {
		IBPD_Param = iBPD_Param;
	}
}
