package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N931_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4152363686746387737L;
	
    private String OFFSET;

    private String HEADER;

    private String MSGCOD;
    
    private String TYPE;

	public String getOFFSET() {
		return OFFSET;
	}

	public String getHEADER() {
		return HEADER;
	}

	public String getMSGCOD() {
		return MSGCOD;
	}

	public String getTYPE() {
		return TYPE;
	}

	public void setOFFSET(String oFFSET) {
		OFFSET = oFFSET;
	}

	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}

	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}

	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}

}
