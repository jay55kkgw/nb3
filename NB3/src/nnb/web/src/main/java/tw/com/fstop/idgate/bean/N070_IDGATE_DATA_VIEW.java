package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

public class N070_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5556555905527940420L;

	@SerializedName(value = "FGTXDATE")
	private String FGTXDATE;
	@SerializedName(value = "transferdate")
	private String transferdate;
	@SerializedName(value = "CMDATE")
	private String CMDATE;
	@SerializedName(value = "CMDD")
	private String CMDD;
	@SerializedName(value = "CMSDATE")
	private String CMSDATE;
	
	@SerializedName(value = "CMEDATE")
	private String CMEDATE;
	@SerializedName(value = "ACNO")
	private String ACNO;
	@SerializedName(value = "OUTACN_NPD")
	private String OUTACN_NPD;
	@SerializedName(value = "TransferType")
	private String TransferType;
	@SerializedName(value = "DPAGACNO_TEXT")
	private String DPAGACNO_TEXT;
	
	@SerializedName(value = "AMOUNT")
	private String AMOUNT;
	
		
	@Override
	public Map<String, String> coverMap(){
		LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", "臺幣轉帳");
		if(this.FGTXDATE.equals("1")) {		
			result.put("交易類型","即時");
			result.put("轉帳日期",this.transferdate);
		}else if(this.FGTXDATE.equals("2")){
			result.put("交易類型","預約");
			result.put("轉帳日期",this.CMDATE);
		}else if(this.FGTXDATE.equals("3")) {
			result.put("交易類型","預約");
			result.put("轉帳日期","每月"+this.CMDD+"日。起日:"+
					this.CMSDATE+"迄日"+this.CMEDATE);
		}		
		if(this.TransferType.equals("NPD")) {
			result.put("轉出帳號",this.OUTACN_NPD);
		}else {
			result.put("轉出帳號",this.ACNO);
		}		
		result.put("轉入帳號",this.DPAGACNO_TEXT);
		result.put("轉帳金額",this.AMOUNT);
		return result;
	}


	public String getFGTXDATE() {
		return FGTXDATE;
	}


	public void setFGTXDATE(String fGTXDATE) {
		FGTXDATE = fGTXDATE;
	}


	public String getTransferdate() {
		return transferdate;
	}


	public void setTransferdate(String transferdate) {
		this.transferdate = transferdate;
	}


	public String getCMDATE() {
		return CMDATE;
	}


	public void setCMDATE(String cMDATE) {
		CMDATE = cMDATE;
	}


	public String getCMDD() {
		return CMDD;
	}


	public void setCMDD(String cMDD) {
		CMDD = cMDD;
	}


	public String getCMSDATE() {
		return CMSDATE;
	}


	public void setCMSDATE(String cMSDATE) {
		CMSDATE = cMSDATE;
	}


	public String getCMEDATE() {
		return CMEDATE;
	}


	public void setCMEDATE(String cMEDATE) {
		CMEDATE = cMEDATE;
	}


	public String getACNO() {
		return ACNO;
	}


	public void setACNO(String aCNO) {
		ACNO = aCNO;
	}


	public String getOUTACN_NPD() {
		return OUTACN_NPD;
	}


	public void setOUTACN_NPD(String oUTACN_NPD) {
		OUTACN_NPD = oUTACN_NPD;
	}


	public String getTransferType() {
		return TransferType;
	}


	public void setTransferType(String transferType) {
		TransferType = transferType;
	}


	public String getDPAGACNO_TEXT() {
		return DPAGACNO_TEXT;
	}


	public void setDPAGACNO_TEXT(String dPAGACNO_TEXT) {
		DPAGACNO_TEXT = dPAGACNO_TEXT;
	}


	public String getAMOUNT() {
		return AMOUNT;
	}


	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}

	
}
