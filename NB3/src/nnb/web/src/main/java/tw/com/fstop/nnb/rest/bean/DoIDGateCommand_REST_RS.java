package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class DoIDGateCommand_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8548823238844093203L;
	private Boolean result = Boolean.FALSE;
	public Boolean getResult() {
		return result;
	}
	public void setResult(Boolean result) {
		this.result = result;
	}
}
