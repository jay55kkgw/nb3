package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N110_REST_RS extends BaseRestBean implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 775129110323679813L;
	
	
	private String ABEND;
	private String USERDATA;
	private String CMQTIME;
	private String CMRECNUM;
	private String TOTAL_BDPIBAL;
	private String REC_NO;
	private String __OCCURS;
	private String USERDATA_X50;
	private String TOTAL_CLR;
	private String TOTAL_ADPIBAL;
	private LinkedList<N110_REST_RSDATA> REC;
	
	
	public String getABEND() {
		return ABEND;
	}
	public void setABEND(String aBEND) {
		ABEND = aBEND;
	}
	public String getUSERDATA() {
		return USERDATA;
	}
	public void setUSERDATA(String uSERDATA) {
		USERDATA = uSERDATA;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getCMRECNUM() {
		return CMRECNUM;
	}
	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}
	public String getTOTAL_BDPIBAL() {
		return TOTAL_BDPIBAL;
	}
	public void setTOTAL_BDPIBAL(String tOTAL_BDPIBAL) {
		TOTAL_BDPIBAL = tOTAL_BDPIBAL;
	}
	public String getREC_NO() {
		return REC_NO;
	}
	public void setREC_NO(String rEC_NO) {
		REC_NO = rEC_NO;
	}
	public String get__OCCURS() {
		return __OCCURS;
	}
	public void set__OCCURS(String __OCCURS) {
		this.__OCCURS = __OCCURS;
	}
	public String getUSERDATA_X50() {
		return USERDATA_X50;
	}
	public void setUSERDATA_X50(String uSERDATA_X50) {
		USERDATA_X50 = uSERDATA_X50;
	}
	public String getTOTAL_CLR() {
		return TOTAL_CLR;
	}
	public void setTOTAL_CLR(String tOTAL_CLR) {
		TOTAL_CLR = tOTAL_CLR;
	}
	public String getTOTAL_ADPIBAL() {
		return TOTAL_ADPIBAL;
	}
	public void setTOTAL_ADPIBAL(String tOTAL_ADPIBAL) {
		TOTAL_ADPIBAL = tOTAL_ADPIBAL;
	}
	public LinkedList<N110_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<N110_REST_RSDATA> rEC) {
		REC = rEC;
	}

	

}
