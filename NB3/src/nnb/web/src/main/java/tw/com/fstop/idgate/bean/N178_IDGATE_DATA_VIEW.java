package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.util.StringUtils;

import com.google.gson.annotations.SerializedName;


public class N178_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5698117037165267738L;
	/**
	 * 
	 */

	
	@SerializedName(value = "FYACN")
	private String FYACN;
	@SerializedName(value = "FDPNUM")
	private String FDPNUM;
	@SerializedName(value = "CRYNAME")
	private String CRYNAME;
	@SerializedName(value = "SHOW_AMTTSF")
	private String SHOW_AMTTSF;
	@SerializedName(value = "SHOW_TERM")
	private String SHOW_TERM;
	
	@SerializedName(value = "SHOW_DPISDT")
	private String SHOW_DPISDT;
	@SerializedName(value = "SHOW_DUEDAT")
	private String SHOW_DUEDAT;
	@SerializedName(value = "SHOW_INTMTH")
	private String SHOW_INTMTH;
	@SerializedName(value = "ITR")
	private String ITR;
	@SerializedName(value = "FYTSFAN")
	private String FYTSFAN;
	
	@SerializedName(value = "SHOW_AMTFDP")
	private String SHOW_AMTFDP;
	@SerializedName(value = "SHOW_INT")
	private String SHOW_INT;
	@SerializedName(value = "SHOW_FYTAX")
	private String SHOW_FYTAX;
	@SerializedName(value = "SHOW_NHITAX")
	private String SHOW_NHITAX;

	
	@Override
	public Map<String, String> coverMap(){
		LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", "外幣定存單到期續存");
		result.put("交易類型", "-");
		result.put("存單帳號", this.FYACN);
		result.put("存單號碼", this.FDPNUM);
		result.put("存單金額", this.CRYNAME+" "+ this.SHOW_AMTTSF+"元");
		result.put("存單種類", "定期存款");
		result.put("存款期別", this.SHOW_TERM);
		
		result.put("起存日", this.SHOW_DPISDT);
		result.put("到期日", this.SHOW_DUEDAT);
		result.put("計息方式", this.SHOW_INTMTH);
		result.put("利率", this.ITR);
		if(StringUtils.isEmpty(this.FYTSFAN))result.put("利息轉入帳號", "-");
		else result.put("利息轉入帳號", this.FYTSFAN);		
		
		result.put("原存單金額", this.CRYNAME + " "+this.SHOW_AMTFDP +"元");
		result.put("原存單利息", this.SHOW_INT);
		result.put("代扣利息所得稅", this.SHOW_FYTAX);
		result.put("健保費", this.SHOW_NHITAX);
		return result;
	}


	public String getFYACN() {
		return FYACN;
	}


	public void setFYACN(String fYACN) {
		FYACN = fYACN;
	}


	public String getFDPNUM() {
		return FDPNUM;
	}


	public void setFDPNUM(String fDPNUM) {
		FDPNUM = fDPNUM;
	}


	public String getCRYNAME() {
		return CRYNAME;
	}


	public void setCRYNAME(String cRYNAME) {
		CRYNAME = cRYNAME;
	}


	public String getSHOW_AMTTSF() {
		return SHOW_AMTTSF;
	}


	public void setSHOW_AMTTSF(String sHOW_AMTTSF) {
		SHOW_AMTTSF = sHOW_AMTTSF;
	}


	public String getSHOW_TERM() {
		return SHOW_TERM;
	}


	public void setSHOW_TERM(String sHOW_TERM) {
		SHOW_TERM = sHOW_TERM;
	}


	public String getSHOW_DPISDT() {
		return SHOW_DPISDT;
	}


	public void setSHOW_DPISDT(String sHOW_DPISDT) {
		SHOW_DPISDT = sHOW_DPISDT;
	}


	public String getSHOW_DUEDAT() {
		return SHOW_DUEDAT;
	}


	public void setSHOW_DUEDAT(String sHOW_DUEDAT) {
		SHOW_DUEDAT = sHOW_DUEDAT;
	}


	public String getSHOW_INTMTH() {
		return SHOW_INTMTH;
	}


	public void setSHOW_INTMTH(String sHOW_INTMTH) {
		SHOW_INTMTH = sHOW_INTMTH;
	}


	public String getITR() {
		return ITR;
	}


	public void setITR(String iTR) {
		ITR = iTR;
	}


	public String getFYTSFAN() {
		return FYTSFAN;
	}


	public void setFYTSFAN(String fYTSFAN) {
		FYTSFAN = fYTSFAN;
	}


	public String getSHOW_AMTFDP() {
		return SHOW_AMTFDP;
	}


	public void setSHOW_AMTFDP(String sHOW_AMTFDP) {
		SHOW_AMTFDP = sHOW_AMTFDP;
	}


	public String getSHOW_INT() {
		return SHOW_INT;
	}


	public void setSHOW_INT(String sHOW_INT) {
		SHOW_INT = sHOW_INT;
	}


	public String getSHOW_FYTAX() {
		return SHOW_FYTAX;
	}


	public void setSHOW_FYTAX(String sHOW_FYTAX) {
		SHOW_FYTAX = sHOW_FYTAX;
	}


	public String getSHOW_NHITAX() {
		return SHOW_NHITAX;
	}


	public void setSHOW_NHITAX(String sHOW_NHITAX) {
		SHOW_NHITAX = sHOW_NHITAX;
	}

}
