package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class EBPay1_REST_RS  extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8186978928097295034L;

	private String CMQTIME;
	private String FEE;
	private String VrfyMacRslt;
	private String TrnsCode;
	private String Rcode;
	private String SessionId;
	private String CMDATE2;
	private String RDESC;
	
	public String getRDESC() {
		return RDESC;
	}
	public void setRDESC(String rDESC) {
		RDESC = rDESC;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getFEE() {
		return FEE;
	}
	public void setFEE(String fEE) {
		FEE = fEE;
	}
	public String getVrfyMacRslt() {
		return VrfyMacRslt;
	}
	public void setVrfyMacRslt(String vrfyMacRslt) {
		VrfyMacRslt = vrfyMacRslt;
	}
	public String getTrnsCode() {
		return TrnsCode;
	}
	public void setTrnsCode(String trnsCode) {
		TrnsCode = trnsCode;
	}
	public String getRcode() {
		return Rcode;
	}
	public void setRcode(String rcode) {
		Rcode = rcode;
	}
	public String getSessionId() {
		return SessionId;
	}
	public void setSessionId(String sessionId) {
		SessionId = sessionId;
	}
	public String getCMDATE2() {
		return CMDATE2;
	}
	public void setCMDATE2(String cMDATE2) {
		CMDATE2 = cMDATE2;
	}
}
