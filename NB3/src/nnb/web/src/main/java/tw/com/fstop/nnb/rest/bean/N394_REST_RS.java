package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N394_REST_RS extends BaseRestBean implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4768735658206547435L;
	
	private String CMQTIME;
	private String TRANSCODE;
	private String CDNO;
	private String TRADEDATE;
	private String BILLSENDMODE;
	private String UNIT;
	private String FUNDACN;
	private String BANKID;
	private String FUNDAMT;
	private String SSLTXNO;
	private String CRY;
	
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getTRANSCODE() {
		return TRANSCODE;
	}
	public void setTRANSCODE(String tRANSCODE) {
		TRANSCODE = tRANSCODE;
	}
	public String getTRADEDATE() {
		return TRADEDATE;
	}
	public void setTRADEDATE(String tRADEDATE) {
		TRADEDATE = tRADEDATE;
	}
	public String getBILLSENDMODE() {
		return BILLSENDMODE;
	}
	public void setBILLSENDMODE(String bILLSENDMODE) {
		BILLSENDMODE = bILLSENDMODE;
	}
	public String getUNIT() {
		return UNIT;
	}
	public void setUNIT(String uNIT) {
		UNIT = uNIT;
	}
	public String getFUNDACN() {
		return FUNDACN;
	}
	public void setFUNDACN(String fUNDACN) {
		FUNDACN = fUNDACN;
	}
	public String getBANKID() {
		return BANKID;
	}
	public void setBANKID(String bANKID) {
		BANKID = bANKID;
	}
	public String getFUNDAMT() {
		return FUNDAMT;
	}
	public void setFUNDAMT(String fUNDAMT) {
		FUNDAMT = fUNDAMT;
	}
	public String getSSLTXNO() {
		return SSLTXNO;
	}
	public void setSSLTXNO(String sSLTXNO) {
		SSLTXNO = sSLTXNO;
	}
	public String getCRY() {
		return CRY;
	}
	public void setCRY(String cRY) {
		CRY = cRY;
	}
	public String getCDNO() {
		return CDNO;
	}
	public void setCDNO(String cDNO) {
		CDNO = cDNO;
	}
}
