package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N559_REST_RQ extends BaseRestBean_FX implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7728355628362254793L;
	
	private String	CUSIDN	;
	private String	CMSDATE	;
	private String	CMEDATE	;
	private String	INCO	;
	private String  USERDATA;
	private String  OKOVNEXT; //下載用
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getCMSDATE() {
		return CMSDATE;
	}
	public void setCMSDATE(String cMSDATE) {
		CMSDATE = cMSDATE;
	}
	public String getCMEDATE() {
		return CMEDATE;
	}
	public void setCMEDATE(String cMEDATE) {
		CMEDATE = cMEDATE;
	}
	public String getINCO() {
		return INCO;
	}
	public void setINCO(String iNCO) {
		INCO = iNCO;
	}
	public String getUSERDATA() {
		return USERDATA;
	}
	public void setUSERDATA(String uSERDATA) {
		USERDATA = uSERDATA;
	}
	public String getOKOVNEXT() {
		return OKOVNEXT;
	}
	public void setOKOVNEXT(String oKOVNEXT) {
		OKOVNEXT = oKOVNEXT;
	}
	
}
