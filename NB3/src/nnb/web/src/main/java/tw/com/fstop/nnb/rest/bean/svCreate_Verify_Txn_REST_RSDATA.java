package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * 通知手機啟動驗證
 * 
 * @author Leo
 *
 */
public class svCreate_Verify_Txn_REST_RSDATA extends BaseRestBean_IDGATE implements Serializable {
	
	private static final long serialVersionUID = -8105756062267887930L;

	private String returnCode; //
	private String returnMsg; //
	private String txnID; //
	private String enTxnID; //
	private String sessionid; //
	private String returnJson; //
	public String getReturnCode() {
		return returnCode;
	}
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}
	public String getReturnMsg() {
		return returnMsg;
	}
	public void setReturnMsg(String returnMsg) {
		this.returnMsg = returnMsg;
	}
	public String getTxnID() {
		return txnID;
	}
	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}
	public String getEnTxnID() {
		return enTxnID;
	}
	public void setEnTxnID(String enTxnID) {
		this.enTxnID = enTxnID;
	}
	public String getSessionid() {
		return sessionid;
	}
	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}
	public String getReturnJson() {
		return returnJson;
	}
	public void setReturnJson(String returnJson) {
		this.returnJson = returnJson;
	}
	
	

}
