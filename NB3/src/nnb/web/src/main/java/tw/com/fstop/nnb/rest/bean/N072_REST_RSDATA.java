package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N072_REST_RSDATA implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7283990829505452827L;
	
	//outAcno
	String TRFLAG;
	String ATTRBUE;
	String ACN;
	
	
	public String getTRFLAG() {
		return TRFLAG;
	}
	public void setTRFLAG(String tRFLAG) {
		TRFLAG = tRFLAG;
	}
	public String getATTRBUE() {
		return ATTRBUE;
	}
	public void setATTRBUE(String aTTRBUE) {
		ATTRBUE = aTTRBUE;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}	
	
}
