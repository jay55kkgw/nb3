package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N810_REST_RSDATA extends BaseRestBean implements Serializable {


	private static final long serialVersionUID = 3588725544057128396L;
	/**
	 * 
	 */
	//信用卡卡別代號
	String CARDTYPE;
	//信用卡卡別中文
	String TYPENAME;
	//信用卡持卡人
	String CR_NAME;
	//??? 待查 應該是某種狀態 like 過期遺失掛失中之類的
	String BLK_CODE;
	//信用卡卡號
	String CARDNUM;
	//判斷正附卡 0:附卡 1:正卡
	String PT_FLG;
	
	
	public String getCARDTYPE() {
		return CARDTYPE;
	}
	public void setCARDTYPE(String cARDTYPE) {
		CARDTYPE = cARDTYPE;
	}
	public String getTYPENAME() {
		return TYPENAME;
	}
	public void setTYPENAME(String tYPENAME) {
		TYPENAME = tYPENAME;
	}
	public String getCR_NAME() {
		return CR_NAME;
	}
	public void setCR_NAME(String cR_NAME) {
		CR_NAME = cR_NAME;
	}
	public String getBLK_CODE() {
		return BLK_CODE;
	}
	public void setBLK_CODE(String bLK_CODE) {
		BLK_CODE = bLK_CODE;
	}
	public String getCARDNUM() {
		return CARDNUM;
	}
	public void setCARDNUM(String cARDNUM) {
		CARDNUM = cARDNUM;
	}
	public String getPT_FLG() {
		return PT_FLG;
	}
	public void setPT_FLG(String pT_FLG) {
		PT_FLG = pT_FLG;
	}
	
}
