package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N913_REST_RS extends BaseRestBean implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2109674885599917456L;
	
	private String MBOPNDT;		// 啟用/關閉日期
	private String MBOPNTM;		// 啟用/關閉時間
	private String LOTCNT;		// 累積抽獎點數
	
	public String getMBOPNDT() {
		return MBOPNDT;
	}
	
	public void setMBOPNDT(String mBOPNDT) {
		MBOPNDT = mBOPNDT;
	}
	
	public String getMBOPNTM() {
		return MBOPNTM;
	}
	
	public void setMBOPNTM(String mBOPNTM) {
		MBOPNTM = mBOPNTM;
	}
	
	public String getLOTCNT() {
		return LOTCNT;
	}
	
	public void setLOTCNT(String lOTCNT) {
		LOTCNT = lOTCNT;
	}

}
