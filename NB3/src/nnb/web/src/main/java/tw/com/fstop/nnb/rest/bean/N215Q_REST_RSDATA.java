package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N215Q_REST_RSDATA implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4272649675802479813L;
	
	private String ACN;//帳號
	private String BNKCOD;//行庫別
	private String TRAFLAG;//約定註記
	
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getBNKCOD() {
		return BNKCOD;
	}
	public void setBNKCOD(String bNKCOD) {
		BNKCOD = bNKCOD;
	}
	public String getTRAFLAG() {
		return TRAFLAG;
	}
	public void setTRAFLAG(String tRAFLAG) {
		TRAFLAG = tRAFLAG;
	}
	
	

}
