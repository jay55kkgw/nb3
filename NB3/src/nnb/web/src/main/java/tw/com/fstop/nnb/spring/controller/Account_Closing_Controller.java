package tw.com.fstop.nnb.spring.controller;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.rest.bean.N366_1_REST_RS;
import tw.com.fstop.nnb.rest.bean.N366_REST_RS;
import tw.com.fstop.nnb.service.Account_Closing_Service;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({ SessionUtil.PD, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, SessionUtil.PRINT_DATALISTMAP_DATA,
		SessionUtil.CONFIRM_LOCALE_DATA, SessionUtil.RESULT_LOCALE_DATA, SessionUtil.CUSIDN, SessionUtil.CLOSING_TW_ACCOUNT,
		SessionUtil.TRANSFER_CONFIRM_TOKEN, SessionUtil.TRANSFER_RESULT_TOKEN, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN,
		SessionUtil.TRANSFER_DATA })
@Controller
@RequestMapping(value = "/ACCOUNT/CLOSING")
public class Account_Closing_Controller
{
	Logger log = LoggerFactory.getLogger(this.getClass());

	// TODO 測試用
	// int cnt = 0;

	@Autowired
	private Account_Closing_Service account_Closing_Service;

	/**
	 * N366 線上申請新臺幣存款帳戶結清銷戶
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/closing_tw_account")
	public String closing_tw_account(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		BaseResult bs = new BaseResult();
		log.debug("closing_tw_account start");
		// 下一頁
		String next = "/ACCOUNT/CLOSING/closing_tw_account_agreement";
		try 
		{
			String cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null));
			cusidn = new String(Base64.getDecoder().decode(cusidn));
			
			bs = account_Closing_Service.getAccounts(cusidn, "closing_tw_account");
			
			model.addAttribute("next", next);
		}
		catch (Exception e) 
		{
			log.error("closing_tw_account error", e);
		}
		finally 
		{
			if (bs != null && bs.getResult()) 
			{
				target = "/online_serving/closing_tw_account";
			}
			else 
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * N366 線上申請新臺幣存款帳戶結清銷戶
	 * 條款頁
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/closing_tw_account_agreement")
	public String closing_tw_account_agreement(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		BaseResult bs = new BaseResult();
		log.debug("closing_tw_account_agreement start");
		// 上一頁
		String previous = "/ACCOUNT/CLOSING/closing_tw_account";
		// 下一頁
		String next = "/ACCOUNT/CLOSING/closing_tw_account_input";
		try 
		{
			bs.setResult(true);
			model.addAttribute("closing_tw_account_agreement", bs);
			model.addAttribute("previous", previous);
			model.addAttribute("next", next);
		}
		catch (Exception e) 
		{
			log.error("closing_tw_account_agreement error", e);
		}
		finally 
		{
			if (bs != null && bs.getResult()) 
			{
				target = "/online_serving/closing_tw_account_agreement";
			}
			else 
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * N366 線上申請新臺幣存款帳戶結清銷戶
	 * 選擇欲結清帳號
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/closing_tw_account_input")
	public String closing_tw_account_input(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		BaseResult bs = new BaseResult();
		log.debug("closing_tw_account_input start");
		// 下一頁
		String next = "/ACCOUNT/CLOSING/closing_tw_account_input2";
		// 錯誤頁回功能首頁
		String errorNext = "/ACCOUNT/CLOSING/closing_tw_account_input";
		try 
		{
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			account_Closing_Service.cleanSession(model);
			
			String cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null));
			cusidn = new String(Base64.getDecoder().decode(cusidn));
			
			// 取得可結清帳號
			bs = account_Closing_Service.getAccounts(cusidn, "closing_tw_account");
			Map<String, Object> bsData = (Map) bs.getData();
			model.addAttribute("closing_tw_account_input", bsData);
			
			// 從別頁的快速選項而來
			String forwardAcn = okMap.get("ACN");
			forwardAcn = forwardAcn == null ? "" : forwardAcn;
			model.addAttribute("forwardAcn", forwardAcn);
			
			model.addAttribute("next", next);
		}
		catch (Exception e) 
		{
			log.error("closing_tw_account_input error", e);
		}
		finally 
		{
			if (bs != null && bs.getResult()) 
			{
				target = "/online_serving/closing_tw_account_input";
			}
			else 
			{
				bs.setNext(errorNext);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * N366 線上申請新臺幣存款帳戶結清銷戶
	 * 選擇餘額轉入帳號
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/closing_tw_account_input2")
	public String closing_tw_account_input2(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		String target = "/error";
		BaseResult bs = new BaseResult();
		log.debug("closing_tw_account_input start");
		// 下一頁
		String next = "/ACCOUNT/CLOSING/closing_tw_account_confirm";
		// 錯誤頁回功能首頁
		String errorNext = "/ACCOUNT/CLOSING/closing_tw_account_input";
		try 
		{
			String cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null));
			cusidn = new String(Base64.getDecoder().decode(cusidn));
			
			// 取得轉入帳號
			bs = account_Closing_Service.getAccounts(cusidn, "acno");
			
			if(bs.getResult())
			{
				ArrayList<String> inAcn = new ArrayList<>();
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map> rec = (ArrayList) bsData.get("REC");
				for(Map<String, String> map : rec)
				{
					inAcn.add(map.get("ACN"));
				}
				model.addAttribute("INACN", inAcn);
			}
			else
			{
				throw new Exception();
			}
			
			// 判斷LOCAL KEY 若有帶值表示切換語系
			boolean hasLocale = okMap.containsKey("locale");
			// 欲結清帳號
			String acn = "";
			Map<String, Object> sessionMap = new HashMap<>();
			if (hasLocale)
			{
				Map<String, Object> map = (Map) SessionUtil.getAttribute(model, SessionUtil.CLOSING_TW_ACCOUNT, null);
				acn = map.get("ACN").toString();
			}
			else
			{
				acn = okMap.get("ACN") == null ? "" : okMap.get("ACN");
				sessionMap.put("ACN", acn);
			}
			
			// 取得欲結清帳號的餘額資訊
			bs = account_Closing_Service.getSettleAcnBalance(cusidn, acn);
			
			if(bs.getResult())
			{
				Map<String, Object> bsData = (Map) bs.getData();
				N366_REST_RS rs = (N366_REST_RS) bsData.get("RS");
				model.addAttribute("closing_tw_account_input2", rs);
				
				// 清除無用資訊
				rs.setTOPMSG(null);
				rs.setMsgCode(null);
				rs.setMSGCOD(null);
				
				// 儲存欲結清帳號的餘額資訊
				sessionMap.put("N366_REST_RS", rs);
				SessionUtil.addAttribute(model, SessionUtil.CLOSING_TW_ACCOUNT, sessionMap);
			}
			else
			{
				throw new Exception();
			}

			model.addAttribute("next", next);
		}
		catch (Exception e) 
		{
			log.error("closing_tw_account_input2 error", e);
		}
		finally 
		{
			if (bs != null && bs.getResult()) 
			{
				target = "/online_serving/closing_tw_account_input2";
			}
			else 
			{
				bs.setNext(errorNext);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * N366 線上申請新臺幣存款帳戶結清銷戶 確認頁
	 * 選擇交易機制
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/closing_tw_account_confirm")
	public String closing_tw_account_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		BaseResult bs = new BaseResult();
		log.debug(ESAPIUtil.vaildLog("closing_tw_account_confirm start >> " + reqParam.toString())); 
		// 下一頁
		String next = "/ACCOUNT/CLOSING/closing_tw_account_result";
		// 錯誤頁回功能首頁
		String errorNext = "/ACCOUNT/CLOSING/closing_tw_account_input";
		try 
		{
			String cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null));
			cusidn = new String(Base64.getDecoder().decode(cusidn));
			
			//修改 Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam); 
			
			// 判斷LOCAL KEY 若有帶值表示切換語系
			boolean hasLocale = okMap.containsKey("locale");
			
			Map<String, Object> sessionMap = new HashMap();
			sessionMap = (Map) SessionUtil.getAttribute(model, SessionUtil.CLOSING_TW_ACCOUNT, null);
			
			// 轉入帳號
			String inAcn = "";
			if (hasLocale)
			{
				inAcn = sessionMap.get("INACN").toString();
			}
			else
			{
				inAcn = okMap.get("INACN");
				// 儲存轉入帳號
				sessionMap.put("INACN", inAcn);
			}
			sessionMap.put("CUSIDN", cusidn);
			
			SessionUtil.addAttribute(model, SessionUtil.CLOSING_TW_ACCOUNT, sessionMap);
			N366_REST_RS rs = (N366_REST_RS) sessionMap.get("N366_REST_RS");
			
			// 取得Ikey參數
			Map<String, String> ikeyParams = account_Closing_Service.getIkeyParams(model, rs, inAcn);
			
			// IKEY要使用的JSON:DC
			String jsondc = URLEncoder.encode(CodeUtil.toJson(ikeyParams), "UTF-8");
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, jsondc);
			
			// 載入自然人憑證元件，判斷瀏覽器會用到
			String userAgent = request.getHeader("user-agent");
		
			log.debug(ESAPIUtil.vaildLog("userAgent >> " + userAgent)); 
			userAgent = userAgent.replace("\"","").replace("<","").replace(">","").replace("%","").replace("&","").replace("[","").replace("]","");
			log.debug(ESAPIUtil.vaildLog("userAgent >> " + userAgent)); 
			//修改 Trust Boundary Violation
			String validateUserAgent = ESAPIUtil.validHeader(userAgent);
			model.addAttribute("userAgent", validateUserAgent);
			
			bs.setResult(true);
			model.addAttribute("next", next);
		}
		catch (Exception e) 
		{
			log.error("closing_tw_account_confirm error", e);
		}
		finally 
		{
			if (bs != null && bs.getResult()) 
			{
				target = "/online_serving/closing_tw_account_confirm";
			}
			else 
			{
				bs.setNext(errorNext);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * N366 線上申請新臺幣存款帳戶結清銷戶 結果頁
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/closing_tw_account_result")
	public String closing_tw_account_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		BaseResult bs = new BaseResult();
		log.debug("closing_tw_account_result start");
		// 錯誤頁回功能首頁
		String errorNext = "/ACCOUNT/CLOSING/closing_tw_account_input";
		try 
		{
			String cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null));
			cusidn = new String(Base64.getDecoder().decode(cusidn));
			
			// 取出欲結清帳號的餘額參數
			Map<String, Object> sessionMap = (Map) SessionUtil.getAttribute(model, SessionUtil.CLOSING_TW_ACCOUNT, null);
			N366_REST_RS rs = (N366_REST_RS) sessionMap.get("N366_REST_RS");
			String json = CodeUtil.toJson(rs);
			Map<String, String> settleAcn = CodeUtil.fromJson(json, Map.class);
			log.debug("settle ACN reqParam >> {}", settleAcn);
			
			// 取出轉入帳號
			String inAcn = sessionMap.get("INACN").toString();
			
			// 與交易機制參數合併
			reqParam.putAll(settleAcn);
			reqParam.put("INACN", inAcn);
			log.debug(ESAPIUtil.vaildLog("final reqParam >> " + reqParam)); 
			
			// 判斷LOCAL KEY 若有帶值表示切換語系
			boolean hasLocale = reqParam.containsKey("locale");
			
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
			}
			else
			{
				bs = account_Closing_Service.n366_1(cusidn, reqParam);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			
			if(bs.getResult())
			{
				Map<String, Object> bsData = (Map) bs.getData();
				N366_1_REST_RS n366_1_rs = (N366_1_REST_RS) bsData.get("RS");
				model.addAttribute("closing_tw_account_result", n366_1_rs);
				
				String cmqTime = DateUtil.getDate("/") + " " + DateUtil.getTheTime(":");
				model.addAttribute("CMQTIME", cmqTime);
			}
		}
		catch (Exception e) 
		{
			log.error("closing_tw_account_result error", e);
		}
		finally 
		{
			if (bs != null && bs.getResult()) 
			{
				target = "/online_serving/closing_tw_account_result";
			}
			else 
			{
				bs.setNext(errorNext);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	/**
	 * F017 線上申請外匯存款帳戶結清銷戶!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/closing_fcy_account")
	public String closing_fcy_account(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("closing_fcy_account Start ~~~~~~~~~~~~~~~~");
		String target = "online_serving/closing_fcy_account";
		return target;
	}

	/**
	 * F017 線上申請外匯存款帳戶結清銷戶輸入頁!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/closing_fcy_account_step1")
	public String closing_fcy_account_step1(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("closing_fcy_account_step1 Start ~~~~~~~~~~~~~~~~");
		String target = "/error";
		BaseResult bs = new BaseResult();
		try
		{
			// get session value
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			bs = account_Closing_Service.closing_fcy_account_step1(cusidn);
		}
		catch (Exception e)
		{
			log.error("closing_fcy_account_step1 Error >>>> {} ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "online_serving/closing_fcy_account_step1";

				model.addAttribute("closing_fcy_account_step1", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * F017 線上申請外匯存款帳戶結清銷戶進入頁!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/closing_fcy_account_step2")
	public String closing_fcy_account_step2(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("closing_fcy_account_step2 Start ~~~~~~~~~~~~~~~~");
		String target = "/error";
		BaseResult bs = new BaseResult();
		try
		{
			// get session value
			bs = account_Closing_Service.closing_fcy_account_step2(reqParam);
		}
		catch (Exception e)
		{
			log.error("closing_fcy_account_step2 Error >>>> {} ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "online_serving/closing_fcy_account_step2";

				model.addAttribute("closing_fcy_account_step2", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * F017 線上申請外匯存款帳戶結清銷戶進入頁!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/closing_fcy_account_step3")
	public String closing_fcy_account_step3(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("closing_fcy_account_step3 Start ~~~~~~~~~~~~~~~~");
		String target = "/error";
		BaseResult bs = new BaseResult();
		try
		{
			// get session value
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			bs = account_Closing_Service.closing_fcy_account_step3(cusidn, reqParam);
		}
		catch (Exception e)
		{
			log.error("closing_fcy_account_step3 Error >>>> {} ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "online_serving/closing_fcy_account_step3";

				model.addAttribute("closing_fcy_account_step3", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * F017 線上申請外匯存款帳戶結清銷戶進入頁!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/closing_fcy_account_confirm")
	public String closing_fcy_account_confirm(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("closing_fcy_account_confirm Start ~~~~~~~~~~~~~~~~");
		String jsondc = "";
		String target = "/error";
		BaseResult bs = new BaseResult();
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam), "UTF-8");
			log.trace(ESAPIUtil.vaildLog("deposit_cancel_confirm.jsondc >> " + jsondc)); 
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
			}
			else
			{
				// 取得資料
				bs = account_Closing_Service.closing_fcy_account_confirm(okMap);
				// 防止重送代碼TxToken
				bs.addData("TXTOKEN", ((Map<String, String>) account_Closing_Service.getTxToken().getData()).get("TXTOKEN"));
				// IKEY要使用的JSON:DC
				bs.addData("jsondc", jsondc);
				log.trace("deposit_cancel_confirm bs.getData {}", bs.getData());
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
			}

		}
		catch (Exception e)
		{
			log.error("closing_fcy_account_confirm Error >>>> {} ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "online_serving/closing_fcy_account_confirm";

				model.addAttribute("closing_fcy_account_confirm", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * F017 線上申請外匯存款帳戶結清銷戶取得議價頁!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/closing_fcy_account_step4_r")
	public String closing_fcy_account_step4_r(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("closing_fcy_account_step4_r Start ~~~~~~~~~~~~~~~~");
		String target = "/error";
		BaseResult bs = new BaseResult();
		try
		{
			// get session value
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			bs = account_Closing_Service.closing_fcy_account_step4_r(cusidn, reqParam);
		}
		catch (Exception e)
		{
			log.error("closing_fcy_account_step4_r Error >>>> {} ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "online_serving/closing_fcy_account_step4_r";
				model.addAttribute("closing_fcy_account_step4_r", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * F017 線上申請外匯存款帳戶結清銷戶議價頁確認頁!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/closing_fcy_account_confirm_r")
	public String closing_fcy_account_confirm_r(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("closing_fcy_account_confirm_r Start ~~~~~~~~~~~~~~~~");
		String jsondc = "";
		String target = "/error";
		BaseResult bs = new BaseResult();
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam), "UTF-8");
			log.trace(ESAPIUtil.vaildLog("deposit_cancel_confirm.jsondc >> " + jsondc)); 
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
			}
			else
			{
				// 取得資料
				bs = account_Closing_Service.closing_fcy_account_confirm_r(okMap);
				// 防止重送代碼TxToken
				bs.addData("TXTOKEN", ((Map<String, String>) account_Closing_Service.getTxToken().getData()).get("TXTOKEN"));
				// IKEY要使用的JSON:DC
				bs.addData("jsondc", jsondc);
			}
		}
		catch (Exception e)
		{
			log.error("closing_fcy_account_confirm_r Error >>>> {} ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "online_serving/closing_fcy_account_confirm_r";

				model.addAttribute("closing_fcy_account_confirm_r", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * F017 線上申請外匯存款帳戶結清銷戶結果頁!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/closing_fcy_account_result")
	public String closing_fcy_account_result(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("closing_fcy_account_result Start ~~~~~~~~~~~~~~~~");
		String target = "/error";
		BaseResult bs = new BaseResult();
		String pagToken = "";
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			//Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
			}
			else
			{
				// Check Session Token
				String SessionFinshToken = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null);
				pagToken = okMap.get("TXTOKEN");
				BaseResult checkToken = account_Closing_Service.validateToken(pagToken, SessionFinshToken);
				if (!checkToken.getResult())
				{
					bs = checkToken;
				}
				else
				{
					// get session value
					String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
					bs = account_Closing_Service.closing_fcy_account_result(cusidn, reqParam);
					// 將查過的資料放入session，待切換 locale 時讀取
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}

			}

		}
		catch (Exception e)
		{
			log.error("closing_fcy_account_result Error >>>> {} ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				target = "online_serving/closing_fcy_account_result";

				model.addAttribute("closing_fcy_account_result", bs);
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

}
