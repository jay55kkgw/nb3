package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N840_REST_RSDATA implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 516213655058098675L;
	
	private String ACN;
	private String DPIBAL;
	private String CLR;
	private String AMTICHD;
	private String FD_LOST;
	private String SV_LOST;
	private String SING_LOST;
	private String FILLER;
	
	
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getDPIBAL() {
		return DPIBAL;
	}
	public void setDPIBAL(String dPIBAL) {
		DPIBAL = dPIBAL;
	}
	public String getCLR() {
		return CLR;
	}
	public void setCLR(String cLR) {
		CLR = cLR;
	}
	public String getAMTICHD() {
		return AMTICHD;
	}
	public void setAMTICHD(String aMTICHD) {
		AMTICHD = aMTICHD;
	}
	public String getFD_LOST() {
		return FD_LOST;
	}
	public void setFD_LOST(String fD_LOST) {
		FD_LOST = fD_LOST;
	}
	public String getSV_LOST() {
		return SV_LOST;
	}
	public void setSV_LOST(String sV_LOST) {
		SV_LOST = sV_LOST;
	}
	public String getSING_LOST() {
		return SING_LOST;
	}
	public void setSING_LOST(String sING_LOST) {
		SING_LOST = sING_LOST;
	}
	public String getFILLER() {
		return FILLER;
	}
	public void setFILLER(String fILLER) {
		FILLER = fILLER;
	}
}
