package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N079_IDGATE_DATA implements Serializable{


	private static final long serialVersionUID = 206496827015786478L;
	/**
	 * 
	 */
	@SerializedName(value = "FDPACN")
	private String FDPACN;// 存單帳號
	@SerializedName(value = "FDPNUM")
	private String FDPNUM;// 存單號碼
	@SerializedName(value = "FDPTYPE")
	private String FDPTYPE;// 存單種類
	@SerializedName(value = "AMT")
	private String AMT;// 交易金額
	@SerializedName(value = "ITR")
	private String ITR;// 利率
	
	@SerializedName(value = "INTMTH")
	private String INTMTH;// 計息方式
	@SerializedName(value = "DPISDT")
	private String DPISDT;// 起存日
	@SerializedName(value = "DUEDAT")
	private String DUEDAT;// 到期日
	@SerializedName(value = "INTPAY")
	private String INTPAY;// 利息
	@SerializedName(value = "INTPAYS")
	private String INTPAYS;// 利息正負號
	
	@SerializedName(value = "TAX")
	private String TAX;// 所得稅
	@SerializedName(value = "INTRCV")
	private String INTRCV;// 透支息
	@SerializedName(value = "PAIAFTX")
	private String PAIAFTX;// 稅後本息
	@SerializedName(value = "NHITAX")
	private String NHITAX;// 健保費
	public String getFDPACN() {
		return FDPACN;
	}
	public void setFDPACN(String fDPACN) {
		FDPACN = fDPACN;
	}
	public String getFDPNUM() {
		return FDPNUM;
	}
	public void setFDPNUM(String fDPNUM) {
		FDPNUM = fDPNUM;
	}
	public String getFDPTYPE() {
		return FDPTYPE;
	}
	public void setFDPTYPE(String fDPTYPE) {
		FDPTYPE = fDPTYPE;
	}
	public String getAMT() {
		return AMT;
	}
	public void setAMT(String aMT) {
		AMT = aMT;
	}
	public String getITR() {
		return ITR;
	}
	public void setITR(String iTR) {
		ITR = iTR;
	}
	public String getINTMTH() {
		return INTMTH;
	}
	public void setINTMTH(String iNTMTH) {
		INTMTH = iNTMTH;
	}
	public String getDPISDT() {
		return DPISDT;
	}
	public void setDPISDT(String dPISDT) {
		DPISDT = dPISDT;
	}
	public String getDUEDAT() {
		return DUEDAT;
	}
	public void setDUEDAT(String dUEDAT) {
		DUEDAT = dUEDAT;
	}
	public String getINTPAY() {
		return INTPAY;
	}
	public void setINTPAY(String iNTPAY) {
		INTPAY = iNTPAY;
	}
	public String getINTPAYS() {
		return INTPAYS;
	}
	public void setINTPAYS(String iNTPAYS) {
		INTPAYS = iNTPAYS;
	}
	public String getTAX() {
		return TAX;
	}
	public void setTAX(String tAX) {
		TAX = tAX;
	}
	public String getINTRCV() {
		return INTRCV;
	}
	public void setINTRCV(String iNTRCV) {
		INTRCV = iNTRCV;
	}
	public String getPAIAFTX() {
		return PAIAFTX;
	}
	public void setPAIAFTX(String pAIAFTX) {
		PAIAFTX = pAIAFTX;
	}
	public String getNHITAX() {
		return NHITAX;
	}
	public void setNHITAX(String nHITAX) {
		NHITAX = nHITAX;
	}
	
}
