package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N816L_1_REST_RSDATA_SUCCEED extends BaseRestBean implements Serializable {


	private static final long serialVersionUID = 3588725544057128396L;
	/**
	 * 
	 */
	private String CARDNUM;
	private String TYPENAME;
	private String EPC_FLAG;
	private String EXP_DATE;
	private String STATUS;
	
	
	public String getCARDNUM() {
		return CARDNUM;
	}
	public String getTYPENAME() {
		return TYPENAME;
	}
	public String getEPC_FLAG() {
		return EPC_FLAG;
	}
	public String getEXP_DATE() {
		return EXP_DATE;
	}
	public String getSTATUS() {
		return STATUS;
	}
	public void setCARDNUM(String cARDNUM) {
		CARDNUM = cARDNUM;
	}
	public void setTYPENAME(String tYPENAME) {
		TYPENAME = tYPENAME;
	}
	public void setEPC_FLAG(String ePC_FLAG) {
		EPC_FLAG = ePC_FLAG;
	}
	public void setEXP_DATE(String eXP_DATE) {
		EXP_DATE = eXP_DATE;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	
}
