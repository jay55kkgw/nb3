package tw.com.fstop.nnb.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;

@Service
public class Other_Fee_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	I18n i18n;	
	/**
	 * 	其他費用代扣繳取消(信用卡)
	 * @param cusidn
	 * @return
	 */
	public BaseResult withholding_cancel_C(String cusidn)	{		
		
		log.trace("withholding_cancel_service");
		BaseResult bs = null ;
		try {
			Map<String,String> reqParam = new HashMap<String,String>();
			bs = new BaseResult();
			bs = N815_REST(cusidn, reqParam);	
			Map<String,Object> bsData = (Map<String,Object>)bs.getData();
			List<Map<String,Object>> bsRows = (List<Map<String,Object>>)bsData.get("REC");
			reqParam.put("SEQ", ((String)bsData.get("SEQ")));
			while(true) {
				if(reqParam.get("SEQ").equals("99") || reqParam.get("SEQ").length() > 3) {
					break;
				}
				BaseResult bs2 = new BaseResult();
				bs2 = N815_REST(cusidn, reqParam);	
				Map<String,Object> bs2Data = (Map<String,Object>)bs2.getData();
				List<Map<String,Object>> bs2Rows = (List<Map<String,Object>>)bs2Data.get("REC");
				reqParam.put("SEQ", ((String)bs2Data.get("SEQ")));
				bsRows.addAll(bs2Rows);
			}
			for(Map<String,Object> row :bsRows) {
				Map<String, String> argBean = SpringBeanFactory.getBean("OTHER_FEE_TYPE2");
				if(argBean.containsKey(row.get("TYPE")))
				{
					String TYPE_str = argBean.get(row.get("TYPE"));
					row.put("TYPE_str",TYPE_str);
				}
			}
		} catch (Exception e) {
//			e.printStackTrace();
			log.error("{}",e);
		}
	
		return  bs;
	}	
	
	/**
	 * 	其他費用代扣繳取消
	 * @param cusidn
	 * @return
	 */
	public BaseResult withholding_cancel(String cusidn)	{		
		
		log.trace("withholding_cancel_service");
		BaseResult bs = null ;
		try {
		bs = new BaseResult();
		bs = N920_REST(cusidn, ACNO);	
		Map<String, Object> bsData = (Map) bs.getData();
		ArrayList<Map<String, String>> row = (ArrayList<Map<String, String>>) bsData.get("REC");
		String count = String.valueOf(row.size());
		log.trace("count>>>{}",count);
		} catch (Exception e) {
//			e.printStackTrace();
			log.error("{}",e);
		}
	
		return  bs;
	}	

	/**
	 * 	其他費用代扣繳取消_存款帳號選擇頁
	 * @param cusidn
	 * @return
	 */
	public BaseResult withholding_cancel_step1(String cusidn, Map<String, String> reqParam) {
		log.trace(ESAPIUtil.vaildLog("withholding_cancel_step1_service>>{}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = N8330_REST(cusidn, reqParam);	
			Map<String, Object> bsData = (Map) bs.getData();
			ArrayList<Map<String, String>> row = (ArrayList<Map<String, String>>) bsData.get("REC");
			ArrayList<Map<String, String>> rowData = new ArrayList<Map<String,String>>();
			String memo = "";
			for (Map<String, String> map : row) {
				if((map.get("ACN") != null && !"".equals(map.get("ACN")))) {
					memo = map.get("MEMO");
					Map<String, String> argBean = SpringBeanFactory.getBean("OTHER_FEE_TYPE");
					if(argBean.containsKey(memo))
					{
						String MEMO_C = argBean.get(memo);
						map.put("MEMO_C",MEMO_C);
					}
					//顯示值
					switch (memo) {
					case "ELE":
//						map.put("MEMO_C","電費");
						map.put("TYPE", "03");
						break;
					case "WAT":
//						map.put("MEMO_C","台灣省水費");
						map.put("TYPE", "05");
						break;
					case "WAC":
//						map.put("MEMO_C","台北市水費");
						map.put("TYPE", "04");
						break;
					case "TLL":
//						map.put("MEMO_C","中華電信費");
						map.put("TYPE", "06");
						break;
					case "IHD":
//						map.put("MEMO_C","健保費");
						map.put("TYPE", "01");
						break;
					case "ILD":
//						map.put("MEMO_C","勞保費");
						map.put("TYPE", "02");
						break;
					case "IKD":
//						map.put("MEMO_C","新制勞工退休提繳費");
						map.put("TYPE", "12");
						break;
					case "CKD":
//						map.put("MEMO_C","舊制勞工退休提繳費");
						map.put("TYPE", "14");
						break;
					case "GST":
//						map.put("MEMO_C","大台北區瓦斯費");
						map.put("TYPE", "07");
						break;
					case "GSU":
//						map.put("MEMO_C","欣桃瓦斯費");
						map.put("TYPE", "08");
						break;
					case "GSK":
//						map.put("MEMO_C","欣高瓦斯費");
						map.put("TYPE", "09");
						break;

					default:
						break;
					}
					rowData.add(map);
				}			
			}
			bsData.put("REC", rowData);
			
			String count = String.valueOf(rowData.size());
			log.trace("count>>>{}",count);
			String msgname = i18n.getMsg("LB.Check_no_data")+"!!!";//查無資料 
			if(count == null || "0".equals(count)) {
//				bs.setMsgCode("");
//				bs.setMessage(msgname);
				bs.setSYSMessage("ENRD");
				getMessageByMsgCode(bs);
				bs.setResult(Boolean.FALSE);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("{}", e);
		}

		return bs;
	}
	
	
	/**
	 * 	其他費用代扣繳取消_存款帳號確認頁
	 * @param cusidn
	 * @return
	 */
	public BaseResult withholding_cancel_confirm(String cusidn, Map<String, String> reqParam) {
		log.trace(ESAPIUtil.vaildLog("withholding_cancel_confirm_service>>{}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// call ws API
			// 拆出選擇的值
			// log.debug("ROWDATA: {}", reqParam.get("ROWDATA"));

			String value = reqParam.get("ROWDATA");
			
			bs.addData("ROWDATA", reqParam.get("ROWDATA"));
			
			value = value.substring(1, value.length() - 1); // remove curly brackets
			log.trace(ESAPIUtil.vaildLog("ROWDATA.value: " + value));
			String[] keyValuePairs = value.split(","); // split the string to creat key-value pairs
			log.trace(ESAPIUtil.vaildLog("ROWDATA.keyValuePairs: " + keyValuePairs));

			Map<String, String> map = new HashMap<>();
			for (String pair : keyValuePairs) // iterate over the pairs
			{
				String[] entry = pair.split("="); // split the pairs to get key and value

				String mapKey = "";
				String MapValue = "";
				log.trace("ROWDATA.entry.length: " + entry.length);
				if (entry.length == 2) {
					mapKey = "".equals(entry[0].trim()) ? "" : entry[0].trim();
					MapValue = "".equals(entry[1].trim()) ? "" : entry[1].trim();
				} else {
					mapKey = entry[0].trim();
				}
				// reqParam.remove("jsondc");
				// reqParam.remove("pkcs7Sign");
				reqParam.remove("ROWDATA");
				log.trace(ESAPIUtil.vaildLog("ROWDATA.mapKey:{} " + mapKey));
				log.trace(ESAPIUtil.vaildLog("ROWDATA.MapValue:{} " + MapValue));
				map.put(mapKey, MapValue); // add them to the hashmap and trim whitespaces
			}
			
			Map<String, String> argBean = SpringBeanFactory.getBean("MEMO2ADOPID");
			
			if(argBean.containsKey(map.get("MEMO")))
			{
				String ADOPID = argBean.get(map.get("MEMO"));
				bs.addData("ADOPID",ADOPID);
			}

			bs.addData("ACN", map.get("ACN"));
			bs.addData("UNTNUM", map.get("UNTNUM"));
			bs.addData("MEMO", map.get("MEMO"));
			bs.addData("MEMO_C", map.get("MEMO_C"));
			bs.setResult(true);
		}catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("",e);
		}
	
		return  bs;
	}
	
	
	
	/**
	 * 	for IDGATE DATA and VIEW
	 *  將結果頁的function拔掉電文
	 * @param cusidn
	 * @return
	 */
	public Map<String, String> withholding_cancel_idgatedata_trns(String cusidn , Map<String, String> map){		
		
		log.trace("<<<withholding_cancel_idgatedata_trns>>>");
		Map<String, String> rtnmap = new  HashMap<String, String>();
		try {
		String MEMO = map.get("MEMO");
		log.trace(ESAPIUtil.vaildLog("MEMO>>{}"+MEMO));
		String UNTNUM = map.get("UNTNUM");
		String UNTTEL = map.get("UNTTEL");
		String CUSNUM = map.get("CUSNUM");
		String AMOUNT = map.get("AMOUNT");
		String UNTNUM6 = "";
		
		rtnmap.put("CUSIDN",cusidn);
		rtnmap.put("MEMO",MEMO);
		rtnmap.put("TSFACN",map.get("ACN"));
		rtnmap.put("TYPE",map.get("TYPE"));
		rtnmap.put("BRHBDT",map.get("BRHBDT"));
		rtnmap.put("TIME",map.get("TIME"));
		rtnmap.put("UNTNUM", UNTNUM);
		rtnmap.put("UNTTEL", UNTTEL);
		rtnmap.put("UNTNUM1", UNTNUM);
		rtnmap.put("CUSIDN1", CUSNUM);
		rtnmap.put("TYPNUM",map.get("TYPNUM"));
		rtnmap.put("HLHBRH",map.get("HLHBRH"));		
		Map<String, String> argBean = SpringBeanFactory.getBean("MEMO2ADOPID");
		if(argBean.containsKey(MEMO))
		{
			String ADOPID = argBean.get(MEMO);
			rtnmap.put("ADOPID",ADOPID);
		}
				
		if(UNTNUM.length() < 10) {	
			if(CUSNUM.trim() != null && !"".equals(CUSNUM.trim())  && CUSNUM.length() >= 9) {
				UNTNUM6 = CUSNUM.substring(0,9);
			}else if(CUSNUM.length() < 9) {
				UNTNUM6 = CUSNUM;
			}
			rtnmap.put("UNTNUM6",UNTNUM6);
		}
		rtnmap.put("CUSIDN6",UNTNUM);

		//取消
			if("IHD".equals(MEMO)) {
			//健保
				rtnmap.put("TYPE","01");
//				reqParam.put("TYPNUM","1");
				rtnmap.put("CUSNUM", UNTNUM);
			}else if("ELE".equals(MEMO) || "WAC".equals(MEMO) || "WAT".equals(MEMO) || "TLL".equals(MEMO)) {
			//台電電費,台北市水費,台灣省水費,中華電信電話費
				rtnmap.put("CUSNUM", UNTNUM);
//				bs.setResult(true);
			}else if("ILD".equals(MEMO) || "IKD".equals(MEMO) || "CKD".equals(MEMO)) {
			//勞保,新制勞退,舊制勞退
				rtnmap.put("AMOUNT", AMOUNT);
				rtnmap.put("CUSIDNUN",CUSNUM);
			}else {
			}
			
			Map<String, String> argBean2 = SpringBeanFactory.getBean("OTHER_FEE_TYPE");
			if(argBean2.containsKey(MEMO))
			{
				String MEMO_C = argBean2.get(MEMO);
				rtnmap.put("MEMO_C",i18n.getMsg(MEMO_C));
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("",e);
		}
		return  rtnmap;
	}	
	
	/**
	 * 	其他費用代扣繳取消_結果
	 * @param cusidn
	 * @return
	 */
	public BaseResult withholding_cancel_result(String cusidn,Map<String, String> reqParam){		
		
		log.trace("withholding_cancel_result_service>>>");
		BaseResult bs = null ;
		try {
		bs = new BaseResult();
	//	 call ws API		
		//拆出選擇的值
//		log.debug("ROWDATA: {}", reqParam.get("ROWDATA"));

		String value = reqParam.get("ROWDATA");
		value = value.substring(1, value.length() - 1); // remove curly brackets
		log.trace(ESAPIUtil.vaildLog("ROWDATA.value: " + value));
		String[] keyValuePairs = value.split(","); // split the string to creat key-value pairs
		log.trace(ESAPIUtil.vaildLog("ROWDATA.keyValuePairs: " + keyValuePairs));

		Map<String, String> map = new HashMap<>();
		for (String pair : keyValuePairs) // iterate over the pairs
		{
			String[] entry = pair.split("="); // split the pairs to get key and value

			String mapKey = "";
			String MapValue = "";
			log.trace("ROWDATA.entry.length: " + entry.length);
			if (entry.length == 2) {
				mapKey = "".equals(entry[0].trim()) ? "" : entry[0].trim();
				MapValue = "".equals(entry[1].trim()) ? "" : entry[1].trim();
			} else {
				mapKey = entry[0].trim();
			}
//			reqParam.remove("jsondc");
//			reqParam.remove("pkcs7Sign");
			reqParam.remove("ROWDATA");
			log.trace(ESAPIUtil.vaildLog("ROWDATA.mapKey:{} "+ mapKey));
			log.trace(ESAPIUtil.vaildLog("ROWDATA.MapValue:{} "+ MapValue));
			map.put(mapKey, MapValue); // add them to the hashmap and trim whitespaces
		}
		//依據不同電文更改值
		String MEMO = map.get("MEMO");
		log.trace(ESAPIUtil.vaildLog("MEMO>>{}"+MEMO));
		String UNTNUM = map.get("UNTNUM");
		String UNTTEL = map.get("UNTTEL");
		String CUSNUM = map.get("CUSNUM");
		String AMOUNT = map.get("AMOUNT");
		String UNTNUM6 = "";
		
		reqParam.put("CUSIDN",cusidn);
		reqParam.put("MEMO",MEMO);
		reqParam.put("TSFACN",map.get("ACN"));
		reqParam.put("TYPE",map.get("TYPE"));
		reqParam.put("BRHBDT",map.get("BRHBDT"));
		reqParam.put("TIME",map.get("TIME"));
		reqParam.put("UNTNUM", UNTNUM);
		reqParam.put("UNTTEL", UNTTEL);
		reqParam.put("UNTNUM1", UNTNUM);
		reqParam.put("CUSIDN1", CUSNUM);
		reqParam.put("TYPNUM",map.get("TYPNUM"));
		reqParam.put("HLHBRH",map.get("HLHBRH"));		
		Map<String, String> argBean = SpringBeanFactory.getBean("MEMO2ADOPID");
		if(argBean.containsKey(MEMO))
		{
			String ADOPID = argBean.get(MEMO);
			reqParam.put("ADOPID",ADOPID);
		}
				
		if(UNTNUM.length() < 10) {	
			if(CUSNUM.trim() != null && !"".equals(CUSNUM.trim())  && CUSNUM.length() >= 9) {
				UNTNUM6 = CUSNUM.substring(0,9);
			}else if(CUSNUM.length() < 9) {
				UNTNUM6 = CUSNUM;
			}
			reqParam.put("UNTNUM6",UNTNUM6);
		}
		reqParam.put("CUSIDN6",UNTNUM);

		log.debug(ESAPIUtil.vaildLog("ROWDATA:{} "+ reqParam));
		
		//取消
			if("IHD".equals(MEMO)) {
			//健保
				reqParam.put("TYPE","01");
//				reqParam.put("TYPNUM","1");
				reqParam.put("CUSNUM", UNTNUM);
				bs = N8301_REST(cusidn, reqParam);
			}else if("ELE".equals(MEMO) || "WAC".equals(MEMO) || "WAT".equals(MEMO) || "TLL".equals(MEMO)) {
			//台電電費,台北市水費,台灣省水費,中華電信電話費
				reqParam.put("CUSNUM", UNTNUM);
				bs = N830_REST(cusidn, reqParam);	
//				bs.setResult(true);
			}else if("ILD".equals(MEMO) || "IKD".equals(MEMO) || "CKD".equals(MEMO)) {
			//勞保,新制勞退,舊制勞退
				reqParam.put("AMOUNT", AMOUNT);
				reqParam.put("CUSIDNUN",CUSNUM);
				bs = N8302_REST(cusidn, reqParam);
			}else {
				
				bs.setResult(true);
				MEMO = "";
			}
			bs.addData("MEMO",MEMO);
			bs.addData("dataSet", map);
			
			Map<String, String> argBean2 = SpringBeanFactory.getBean("OTHER_FEE_TYPE");
			if(argBean2.containsKey(MEMO))
			{
				String MEMO_C = argBean2.get(MEMO);
				bs.addData("MEMO_C",MEMO_C);
			}
			
			//顯示值
//			switch (MEMO) {
//			case "ELE":
//				bs.addData("MEMO_C","LB.W0425");//LB.W0425=電費
//				break;
//			case "WAT":
//				bs.addData("MEMO_C","LB.W0451");//LB.W0451=臺灣省水費
//				break;
//			case "WAC":
//				bs.addData("MEMO_C","LB.W0468");//LB.W0468=臺北市水費
//				break;
//			case "TLL":
//				bs.addData("MEMO_C","LB.W0683");//LB.W0683=中華電信費
//				break;
//			case "IHD":
//				bs.addData("MEMO_C","LB.W0699");//LB.W0699=健保費
//				break;
//			case "ILD":
//				bs.addData("MEMO_C","LB.W0484");//LB.W0484=勞保費
//				break;
//			case "IKD":
//				bs.addData("MEMO_C","LB.W0736");//LB.W0736=新制勞工退休提繳費
//				break;
//			case "CKD":
//				bs.addData("MEMO_C","LB.W0752");//LB.W0752=舊制勞工退休提繳費
//				break;
//			case "GST":
//				bs.addData("MEMO_C","LB.X1891");//LB.X1891=大臺北區瓦斯費
//				break;
//			case "GSU":
//				bs.addData("MEMO_C","LB.X1892");//LB.X1892=欣桃瓦斯費
//				break;
//			case "GSK":
//				bs.addData("MEMO_C","LB.X1893");//LB.X1893=欣高瓦斯費
//				break;
//
//			default:
//				break;
//			}			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("",e);
		}
	
		return  bs;
	}	
	
	public BaseResult other_telecom_fee_N810(String cusidn, Map<String, String> reqParam) {

		BaseResult bsN810 = null;
		try {
			bsN810 = new BaseResult();
			reqParam.put("CUSIDN", cusidn);
			reqParam.put("CF", "TD02");
			//Avoid TXNLOG
			reqParam.put("ADOPID", "__N810");
			bsN810 = N810_REST(reqParam);
			bsN810.addData("i_Record", ((Map<String, Object>)bsN810.getData()).get("RECNUM") == null ? "0": ((Map<String, Object>)bsN810.getData()).get("RECNUM"));
			//2021/06/17 新增 E520 錯誤代碼 by 承翰
			if(bsN810.getMsgCode().equals("E091") || bsN810.getMsgCode().equals("E099") || bsN810.getMsgCode().equals("ENRD") || bsN810.getMsgCode().equals("E520")) {
				bsN810.setResult(Boolean.TRUE);
				bsN810.addData("i_Record", "0");
			}
		}catch(Exception e) {
//			e.printStackTrace();
			log.error("time_deposit",e);
		}	
		return bsN810;
	}
	
	public String getCUSNUM(String CUSNUM1,String CUSNUM2) {
		
		//alert("strCH1==="+strCH1+"==="+strCH1.length+"===");
		if(CUSNUM1.length() < 4)
		{
			int intS = 4 - CUSNUM1.length();
			for(int count = 0; count<intS; count++)
				CUSNUM1 += " ";
		}
		//alert("strCH1==="+strCH1+"==="+strCH1.length+"===");
		if(CUSNUM2.length() < 12)
		{
			int intS2 = 12-CUSNUM2.length();
			for(int count2 = 0; count2 < intS2; count2++)
				CUSNUM2 = " " + CUSNUM2;
		}
		return CUSNUM1 + CUSNUM2;
	} 
	
	public BaseResult other_telecom_fee_X_result(String cusidn,Map<String, String> reqParam) {
		log.trace("gold_account_apply_result");
		BaseResult bs = null ;
		try {
//			reqParam.put("ACNNO",reqParam.get("ACNNO").substring(reqParam.get("ACNNO").length() - 11));
			bs = new BaseResult();
			bs = N830_REST(cusidn, reqParam);
			if(bs!=null && bs.getResult()) {
				
			}
		}catch(Exception e) {
//			e.printStackTrace();
			log.error("time_deposit",e);
		}	
		return  bs;
	}
	
	public BaseResult other_telecom_fee_C_result(String cusidn,Map<String, String> reqParam) {
		log.trace("gold_account_apply_result");
		BaseResult bs = null ;
		try {
			//reqParam.put("ACNNO",reqParam.get("ACNNO").substring(reqParam.get("ACNNO").length() - 11));
			bs = new BaseResult();
			bs = N831_REST(cusidn, reqParam);
			if(bs!=null && bs.getResult()) {
				bs.addData("CMQTIME", DateUtil.getDate("/") + " " + DateUtil.getTheTime(":"));
				
			}
		}catch(Exception e) {
//			e.printStackTrace();
			log.error("time_deposit",e);
		}	
		return  bs;
	}
		
	public BaseResult other_health_insurance_result(String cusidn,Map<String, String> reqParam) {
		log.trace("other_health_insurance_result");
		BaseResult bs = null ;
		try {
			//reqParam.put("ACNNO",reqParam.get("ACNNO").substring(reqParam.get("ACNNO").length() - 11));
			bs = new BaseResult();
			String TYPNUM = reqParam.get("TYPNUM");
			bs = N8301_REST(cusidn, reqParam);
			Map<String, String> argBean = SpringBeanFactory.getBean("OTHER_HEALTH_TYPNUM");
			if(argBean.containsKey(TYPNUM))
			{
				TYPNUM = argBean.get(TYPNUM);
				bs.addData("TYPNUM_str",TYPNUM);
			}
			if(bs!=null && bs.getResult()) {
//				bs.addData("CMQTIME", DateUtil.getDate("/") + " " + DateUtil.getTheTime(":"));
				
			}
		}catch(Exception e) {
//			e.printStackTrace();
			log.error("time_deposit",e);
		}	
		return  bs;
	}
	
	public BaseResult other_labor_insurance_result(String cusidn,Map<String, String> reqParam) {
		log.trace("other_labor_insurance_result");
		BaseResult bs = null ;
		try {
			//reqParam.put("ACNNO",reqParam.get("ACNNO").substring(reqParam.get("ACNNO").length() - 11));
			bs = new BaseResult();
			bs = N8302_REST(cusidn, reqParam);
			if(bs!=null && bs.getResult()) {
//				bs.addData("CMQTIME", DateUtil.getDate("/") + " " + DateUtil.getTheTime(":"));
				
			}
		}catch(Exception e) {
//			e.printStackTrace();
			log.error("time_deposit",e);
		}	
		return  bs;
	}
}
