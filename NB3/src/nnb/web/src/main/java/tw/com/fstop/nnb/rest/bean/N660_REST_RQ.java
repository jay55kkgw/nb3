package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N660_REST_RQ extends BaseRestBean_PS implements Serializable {
//	public class N110_REST_RQ extends BaseRestBean implements Serializable {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8584334328121072921L;
	private String ACN	;
	private String CUSIDN;
	private String FGTXWAY;
	private String PINNEW;
	
	
	
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	
}
