package tw.com.fstop.nnb.service;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.view.AbstractView;

import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.DownloadUtil;
import tw.com.fstop.util.DownloadUtil.Variable;
import tw.com.fstop.util.ESAPIUtil;

@Service
public class OldTxt_Download_Service extends AbstractView{
	private Logger log = LoggerFactory.getLogger(this.getClass().getName());
	
	@Autowired
	private I18n i18n;
	
	@SuppressWarnings({"unchecked"})
	public void renderMergedOutputModel(Map<String,Object> parameterMap,HttpServletRequest request,HttpServletResponse response){
		log.debug("IN renderMergedOutputModel");
		log.debug("parameterMap={}",parameterMap);
		
		InputStream inputStream = null;
		InputStreamReader inputStreamReader = null;
		BufferedReader bufferedReader = null;
		
		try{
			String templatePath = (String)parameterMap.get("templatePath");
			log.debug("templatePath={}",templatePath);
			
			Resource resource = new ClassPathResource(templatePath);
//			File file = resource.getFile();

				
			if(resource.exists()){
				inputStream = new BufferedInputStream(resource.getInputStream());
				inputStreamReader = new InputStreamReader(inputStream,"UTF-8");
				bufferedReader = new BufferedReader(inputStreamReader);
				
				Boolean hasMultiRowDataBoolean = null;
				String hasMultiRowData = String.valueOf(parameterMap.get("hasMultiRowData"));
				log.debug("hasMultiRowData={}",hasMultiRowData);
				if(!"null".equals(hasMultiRowData)){
					hasMultiRowDataBoolean = Boolean.valueOf(hasMultiRowData);
				}
				log.debug("hasMultiRowDataBoolean={}",hasMultiRowDataBoolean);
				
				String txtMultiRowDataListMapKeyString = null;
				String txtMultiRowDataListMapKey = String.valueOf(parameterMap.get("txtMultiRowDataListMapKey"));
				log.debug("txtMultiRowDataListMapKey={}",txtMultiRowDataListMapKey);
				if(!"null".equals(txtMultiRowDataListMapKey)){
					txtMultiRowDataListMapKeyString = txtMultiRowDataListMapKey;
				}
				log.debug("txtMultiRowDataListMapKeyString={}",txtMultiRowDataListMapKeyString);
				
				Locale locale = LocaleContextHolder.getLocale();
				String languageTag = locale.toLanguageTag();
				log.debug("languageTag={}",languageTag);
				
				StringBuilder stringBuilder = new StringBuilder();
				//詳細資料
				List<Map<String,Object>> dataListMap = (List<Map<String,Object>>)parameterMap.get("dataListMap");
				
				//變數列
				String originalInputLine = "";
				
				for(int x=0;x<dataListMap.size();x++){
					log.debug("x={}",x);
					
					if(x == 0){
						originalInputLine = bufferedReader.readLine();
					}
					log.debug(ESAPIUtil.vaildLog("originalInputLine >> " + originalInputLine)); 
					
					// CGI Reflected XSS All Clients
					if(!ESAPIUtil.isValidInput(originalInputLine,"GeneralString",true)) {
						return;
					};
					originalInputLine = ESAPIUtil.validInput(originalInputLine,"GeneralString",true);
					log.debug(ESAPIUtil.vaildLog("originalInputLine >> " + originalInputLine)); 
					
					List<Variable> variableList = DownloadUtil.getVariables(originalInputLine,languageTag);
					
					//這裡要分成多個詳細資料和單個的情況
					//多個詳細資料
					if(hasMultiRowDataBoolean != null && hasMultiRowDataBoolean == true){
						Map<String,Object> firstMap = dataListMap.get(x);
						log.debug("firstMap={}",firstMap);
						
						List<Map<String,String>> secondListMap = (List<Map<String,String>>)firstMap.get(txtMultiRowDataListMapKeyString);
						log.debug("secondListMap={}",secondListMap);
						
						for(int y=0;y<secondListMap.size();y++){
							Map<String,String> secondMap = secondListMap.get(y);
							log.debug("secondMap={}",secondMap);
							
							String inputLine = DownloadUtil.getResultLine(variableList,secondMap);
							log.debug("inputLine={}",inputLine);
							
							// CGI Reflected XSS All Clients
							if(!ESAPIUtil.isValidInput(inputLine,"GeneralString",true)) {
								return;
							};
							inputLine = ESAPIUtil.validInput(inputLine,"GeneralString",true);
							log.debug("inputLine={}",inputLine);
							
							stringBuilder.append(inputLine + "\r\n");
						}
					}
					//一個詳細資料
					else{
						Map<String,Object> dataMap = dataListMap.get(x);
						log.debug("dataMap={}",dataMap);
						
						String inputLine = DownloadUtil.getResultLine(variableList,dataMap);
						log.debug("inputLine={}",inputLine);
						
						stringBuilder.append(inputLine + "\r\n");
					}
				}
				response.setContentType("text/plain;charset=UTF-8");
				
				String downloadFileName = (String)parameterMap.get("downloadFileName");
				log.debug("downloadFileName={}",downloadFileName);
				
				String i18nFileName = i18n.getMsg(downloadFileName);
				log.debug("i18nFileName={}",i18nFileName);
				
				if(i18nFileName != null && !"".equals(i18nFileName)){
					downloadFileName = i18nFileName.replaceAll(" ","") + ".txt";
				}
				else{
					downloadFileName = downloadFileName + ".txt";
				}
				log.debug("downloadFileName={}",downloadFileName);
				
				//response.setHeader("Content-Disposition","attachment;filename=" + URLEncoder.encode(downloadFileName,"UTF-8"));
				String header = request.getHeader("User-Agent").toUpperCase();
				 if (header.contains("FIREFOX")) {
					 response.setHeader("Content-Disposition","attachment;filename="+ URLEncoder.encode(downloadFileName,"UTF-8")+";filename*=utf-8'" + URLEncoder.encode(downloadFileName,"UTF-8"));
				 }else {
					 response.setHeader("Content-Disposition","attachment;filename=" + URLEncoder.encode(downloadFileName,"UTF-8"));
				 }
				//解決Exposure of System Data
				BufferedOutputStream buff = null;
				ServletOutputStream outSTr = null;
				try {
					outSTr = response.getOutputStream();// 建立     
					buff = new BufferedOutputStream(outSTr);
					buff.write(ESAPIUtil.validLongInput(stringBuilder.toString(), "GeneralString", true).getBytes("UTF-8"));
					buff.flush();
					buff.close();
					outSTr.close();
				}catch(Exception e) {
					
				}
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("renderMergedOutputModel error >> {}",e);
		}
		try{
			if(bufferedReader != null){
				bufferedReader.close();
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("renderMergedOutputModel error >> {}",e);
		}
		try{
			if(inputStreamReader != null){
				inputStreamReader.close();
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("renderMergedOutputModel error >> {}",e);
		}
		try{
			if(inputStream != null){
				inputStream.close();
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("renderMergedOutputModel error >> {}",e);
		}
	}
	public I18n getI18n(){
		return i18n;
	}
	public void setI18n(I18n i18n){
		this.i18n = i18n;
	}
}