package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class A106_REST_RS extends BaseRestBean implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1887589429197086253L;
	
	private String ABEND;		// 結束代碼
	private String CUSIDN;		// 統一編號
	private String APLBRH;		// 網銀申請行
	private String FUN_TYPE;	// 類別
	private String CAREER1;		// 職業
	private String CAREER2;		// 職稱
	private String CONPNAM;		// 任職機構名稱
	private String CDDCOD;		// 客戶洗錢風險等級
	
	
	public String getABEND() {
		return ABEND;
	}
	public void setABEND(String aBEND) {
		ABEND = aBEND;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getAPLBRH() {
		return APLBRH;
	}
	public void setAPLBRH(String aPLBRH) {
		APLBRH = aPLBRH;
	}
	public String getFUN_TYPE() {
		return FUN_TYPE;
	}
	public void setFUN_TYPE(String fUN_TYPE) {
		FUN_TYPE = fUN_TYPE;
	}
	public String getCAREER1() {
		return CAREER1;
	}
	public void setCAREER1(String cAREER1) {
		CAREER1 = cAREER1;
	}
	public String getCAREER2() {
		return CAREER2;
	}
	public void setCAREER2(String cAREER2) {
		CAREER2 = cAREER2;
	}
	public String getCONPNAM() {
		return CONPNAM;
	}
	public void setCONPNAM(String cONPNAM) {
		CONPNAM = cONPNAM;
	}
	public String getCDDCOD() {
		return CDDCOD;
	}
	public void setCDDCOD(String cDDCOD) {
		CDDCOD = cDDCOD;
	}
	

}
