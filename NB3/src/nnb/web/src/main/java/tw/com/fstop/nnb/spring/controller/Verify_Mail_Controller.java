package tw.com.fstop.nnb.spring.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.Verify_Mail_Service;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtils;
import tw.com.fstop.web.util.WebUtil;

@Controller
@RequestMapping(value = "/VERIFYMAIL")
@SessionAttributes({ SessionUtil.VERIFYURL , SessionUtil.CUSIDN , SessionUtil.TRANSFER_DATA , SessionUtil.DOUBLEMAIL})
public class Verify_Mail_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private Verify_Mail_Service verify_mail_service;


	@RequestMapping(value = "/verify_page")
	public String verify_page(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {

		String target = "/error";
		String next = "/VERIFYMAIL/verify_result";
		try {
			 Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			String url_encode= URLEncoder.encode(okMap.get("url"),"UTF-8");
			SessionUtil.addAttribute(model, SessionUtil.VERIFYURL, url_encode );
			log.info("enc_url : {}" , url_encode);
			
			model.addAttribute("next", next);
			
			target = "personal_serving/verify_page";

		} catch (Exception e) {
			log.debug("verify_page error >> {}", e.getCause());
		}

		return target;
	}
	
	@RequestMapping(value = "/verify_result")
	public String verify_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {

		String target = "online_apply/ErrorWithoutMenu";
		BaseResult bsVerify = new BaseResult();
		BaseResult bs = new BaseResult();
		String url = "";
		try {
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			url =  (String) SessionUtil.getAttribute(model, SessionUtil.VERIFYURL, null);
			okMap.put("url", url);
			
			String IP = WebUtil.getIpAddr(request);
			log.debug(ESAPIUtil.vaildLog("IP >> " + IP));
			okMap.put("IP",IP);
			
			if(StrUtils.isNotEmpty(url)) {
				bsVerify = verify_mail_service.verify(okMap);
			}
			//驗證成功 , 要打N930電文
			if(bsVerify.getResult()) {
				Map n930reqMap = (Map)bsVerify.getData();
				bs = verify_mail_service.N930_TX_REST(n930reqMap);
				Map bsdata = (Map)bs.getData();
				bsdata.put("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				bs.addAllData(bsdata);
			}else {
				bs=bsVerify;
			}
			
		} catch (Exception e) {
			log.debug("verify_result error >> {}", e.getCause());
		} finally {
			
			if(bs.getResult())
			{
				model.addAttribute("verify_result", bs);
				target = "personal_serving/verify_result";
			}
			else 
			{
				bs.setPrevious("/VERIFYMAIL/verify_page?url="+url);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		return target;
	}
	
	/**
	 * N930_aj
	 * 
	 * @return
	 */
	@RequestMapping(value = "/N930_check_double_aj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult N930_aj(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.info("N930_check_double_aj...");
		BaseResult bs = null;
		try {
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			reqParam.put("EXECUTEFUNCTION", "UPDATE_TX");
			reqParam.put("CUSIDN", cusidn);
			reqParam.put("FLAG", "07");
			
			String IP = WebUtil.getIpAddr(request);
			log.debug(ESAPIUtil.vaildLog("IP >> " + IP));
			reqParam.put("IP",IP);
			
			bs = verify_mail_service.N930_TX_REST(reqParam);
		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("n951_aj error >> {}", e);
		}
		return bs;
	}
	
	@RequestMapping(value = "/statement_page")
	public String statement_page(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String cusidn = "";
		String target = "/error";
		BaseResult bs = new BaseResult();
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			
			if(StrUtils.isNotEmpty(cusidn)) {
				if("Y".equals(okMap.get("isBack"))) {
					okMap = (Map<String, String>)((BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null)).getData();
				}
				bs.addAllData(okMap);
				bs.setResult(true);
			}
			

		} catch (Exception e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("statement_page error >> {}", e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/personal_serving/statement_page";
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.DOUBLEMAIL, "Y");
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	

}
