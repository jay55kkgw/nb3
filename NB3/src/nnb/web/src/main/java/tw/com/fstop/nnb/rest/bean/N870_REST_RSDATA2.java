package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N870_REST_RSDATA2  extends BaseRestBean implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = -582661553722394514L;
	
	String DPIBAL;		//餘額
	String LMTSFI;		//限制性轉入餘額
	String RPBAL;		//附條件簽發餘額
	String BONCOD;		//債券代號
	String ACN1;		//帳號
	String LMTSFO;		//限制性轉出餘額
	String TSFBAL_N870;	//可動支餘額
	
	public String getDPIBAL() {
		return DPIBAL;
	}
	public void setDPIBAL(String dPIBAL) {
		DPIBAL = dPIBAL;
	}
	public String getLMTSFI() {
		return LMTSFI;
	}
	public void setLMTSFI(String lMTSFI) {
		LMTSFI = lMTSFI;
	}
	public String getRPBAL() {
		return RPBAL;
	}
	public void setRPBAL(String rPBAL) {
		RPBAL = rPBAL;
	}
	public String getBONCOD() {
		return BONCOD;
	}
	public void setBONCOD(String bONCOD) {
		BONCOD = bONCOD;
	}
	public String getACN1() {
		return ACN1;
	}
	public void setACN1(String aCN1) {
		ACN1 = aCN1;
	}
	public String getLMTSFO() {
		return LMTSFO;
	}
	public void setLMTSFO(String lMTSFO) {
		LMTSFO = lMTSFO;
	}
	public String getTSFBAL_N870() {
		return TSFBAL_N870;
	}
	public void setTSFBAL_N870(String tSFBAL_N870) {
		TSFBAL_N870 = tSFBAL_N870;
	}
}
