package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N570_REST_RS extends BaseRestBean implements Serializable {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4922733988107847890L;
	
	
	private String CUSIDN;//使用者帳號
	private String ACCNO;//帳號
	
	private String RIDNO;//回傳統編
	private String BCHID;//分行別
	private String CUSAD1C;//中文地址1
	private String CUSAD2C;//中文地址2	
	private String CUSADD1;//英文地址1
	private String CUSADD2;//英文地址2
	private String CUSADD3;//英文地址3
	private String CUSTEL1;//電話號碼
	private String CUSTEL2;//傳真號碼
	private String RMARK;//備註
	
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getACCNO() {
		return ACCNO;
	}
	public void setACCNO(String aCCNO) {
		ACCNO = aCCNO;
	}
	public String getRIDNO() {
		return RIDNO;
	}
	public void setRIDNO(String rIDNO) {
		RIDNO = rIDNO;
	}
	public String getBCHID() {
		return BCHID;
	}
	public void setBCHID(String bCHID) {
		BCHID = bCHID;
	}
	public String getCUSAD1C() {
		return CUSAD1C;
	}
	public void setCUSAD1C(String cUSAD1C) {
		CUSAD1C = cUSAD1C;
	}
	public String getCUSAD2C() {
		return CUSAD2C;
	}
	public void setCUSAD2C(String cUSAD2C) {
		CUSAD2C = cUSAD2C;
	}
	public String getCUSADD1() {
		return CUSADD1;
	}
	public void setCUSADD1(String cUSADD1) {
		CUSADD1 = cUSADD1;
	}
	public String getCUSADD2() {
		return CUSADD2;
	}
	public void setCUSADD2(String cUSADD2) {
		CUSADD2 = cUSADD2;
	}
	public String getCUSADD3() {
		return CUSADD3;
	}
	public void setCUSADD3(String cUSADD3) {
		CUSADD3 = cUSADD3;
	}
	public String getCUSTEL1() {
		return CUSTEL1;
	}
	public void setCUSTEL1(String cUSTEL1) {
		CUSTEL1 = cUSTEL1;
	}
	public String getCUSTEL2() {
		return CUSTEL2;
	}
	public void setCUSTEL2(String cUSTEL2) {
		CUSTEL2 = cUSTEL2;
	}
	public String getRMARK() {
		return RMARK;
	}
	public void setRMARK(String rMARK) {
		RMARK = rMARK;
	}

	
	
	
	
	
	
}
