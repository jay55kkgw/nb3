package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N842_REST_RSDATA implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5558276628049904703L;
	

	private String CUID;
	private String EVTMARK;
	private String DUEDAT;
	private String DPISDT;
	private String INTMTH;
	private String ITR;
	private String FDPNO;
	private String ACN;
	private String ILAZLFTM;
	private String AUTXFTM;
	private String BALANCE;
	
	
	public String getCUID() {
		return CUID;
	}
	public void setCUID(String cUID) {
		CUID = cUID;
	}
	public String getEVTMARK() {
		return EVTMARK;
	}
	public void setEVTMARK(String eVTMARK) {
		EVTMARK = eVTMARK;
	}
	public String getDUEDAT() {
		return DUEDAT;
	}
	public void setDUEDAT(String dUEDAT) {
		DUEDAT = dUEDAT;
	}
	public String getDPISDT() {
		return DPISDT;
	}
	public void setDPISDT(String dPISDT) {
		DPISDT = dPISDT;
	}
	public String getINTMTH() {
		return INTMTH;
	}
	public void setINTMTH(String iNTMTH) {
		INTMTH = iNTMTH;
	}
	public String getITR() {
		return ITR;
	}
	public void setITR(String iTR) {
		ITR = iTR;
	}
	public String getFDPNO() {
		return FDPNO;
	}
	public void setFDPNO(String fDPNO) {
		FDPNO = fDPNO;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getILAZLFTM() {
		return ILAZLFTM;
	}
	public void setILAZLFTM(String iLAZLFTM) {
		ILAZLFTM = iLAZLFTM;
	}
	public String getAUTXFTM() {
		return AUTXFTM;
	}
	public void setAUTXFTM(String aUTXFTM) {
		AUTXFTM = aUTXFTM;
	}
	public String getBALANCE() {
		return BALANCE;
	}
	public void setBALANCE(String bALANCE) {
		BALANCE = bALANCE;
	}
}
