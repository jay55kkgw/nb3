package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N394_REST_RQ extends BaseRestBean_FUND implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4811942304535395484L;
	
	private String DATE;
	private String TIME;
	private String CUSIDN;
	private String PINNEW;
	private String TRANSCODE;
	private String CDNO;
	private String TRADEDATE;
	private String BILLSENDMODE;
	private String UNIT;
	private String FUNDACN;
	private String BANKID;
	private String FUNDAMT;
	private String SSLTXNO;
	private String CRY;
	
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public String getTRANSCODE() {
		return TRANSCODE;
	}
	public void setTRANSCODE(String tRANSCODE) {
		TRANSCODE = tRANSCODE;
	}
	public String getTRADEDATE() {
		return TRADEDATE;
	}
	public void setTRADEDATE(String tRADEDATE) {
		TRADEDATE = tRADEDATE;
	}
	public String getBILLSENDMODE() {
		return BILLSENDMODE;
	}
	public void setBILLSENDMODE(String bILLSENDMODE) {
		BILLSENDMODE = bILLSENDMODE;
	}
	public String getUNIT() {
		return UNIT;
	}
	public void setUNIT(String uNIT) {
		UNIT = uNIT;
	}
	public String getFUNDACN() {
		return FUNDACN;
	}
	public void setFUNDACN(String fUNDACN) {
		FUNDACN = fUNDACN;
	}
	public String getBANKID() {
		return BANKID;
	}
	public void setBANKID(String bANKID) {
		BANKID = bANKID;
	}
	public String getFUNDAMT() {
		return FUNDAMT;
	}
	public void setFUNDAMT(String fUNDAMT) {
		FUNDAMT = fUNDAMT;
	}
	public String getSSLTXNO() {
		return SSLTXNO;
	}
	public void setSSLTXNO(String sSLTXNO) {
		SSLTXNO = sSLTXNO;
	}
	public String getCRY() {
		return CRY;
	}
	public void setCRY(String cRY) {
		CRY = cRY;
	}
	public String getCDNO() {
		return CDNO;
	}
	public void setCDNO(String cDNO) {
		CDNO = cDNO;
	}
}
