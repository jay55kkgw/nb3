package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N650_REST_RSDATA extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4738957718981828813L;
	
	String ACN; // 帳號
	LinkedList<N650_REST_RSDATA2> Table;

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	public LinkedList<N650_REST_RSDATA2> getTable() {
		return Table;
	}

	public void setTable(LinkedList<N650_REST_RSDATA2> table) {
		Table = table;
	}
}
