package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

public class N070_IDGATE_DATA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3750893419815329920L;

	@SerializedName(value = "FGTXDATE")
	private String FGTXDATE;
	@SerializedName(value = "CMDATE")
	private String CMDATE;
	@SerializedName(value = "CMDD")
	private String CMDD;
	@SerializedName(value = "CMSDATE")
	private String CMSDATE;
	
	@SerializedName(value = "CMEDATE")
	private String CMEDATE;
	@SerializedName(value = "ACN")
	private String ACN;
	@SerializedName(value = "TXTOKEN")
	private String TXTOKEN;
	@SerializedName(value = "DPAGACNO")
	private String DPAGACNO;
	@SerializedName(value = "DPBHNO")
	private String DPBHNO;
	
	@SerializedName(value = "DPACNO")
	private String DPACNO;
	@SerializedName(value = "AMOUNT")
	private String AMOUNT;
	
	
	public String getFGTXDATE() {
		return FGTXDATE;
	}
	public void setFGTXDATE(String fGTXDATE) {
		FGTXDATE = fGTXDATE;
	}
	public String getCMDATE() {
		return CMDATE;
	}
	public void setCMDATE(String cMDATE) {
		CMDATE = cMDATE;
	}
	public String getCMDD() {
		return CMDD;
	}
	public void setCMDD(String cMDD) {
		CMDD = cMDD;
	}
	public String getCMSDATE() {
		return CMSDATE;
	}
	public void setCMSDATE(String cMSDATE) {
		CMSDATE = cMSDATE;
	}
	public String getCMEDATE() {
		return CMEDATE;
	}
	public void setCMEDATE(String cMEDATE) {
		CMEDATE = cMEDATE;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getTXTOKEN() {
		return TXTOKEN;
	}
	public void setTXTOKEN(String tXTOKEN) {
		TXTOKEN = tXTOKEN;
	}
	public String getDPAGACNO() {
		return DPAGACNO;
	}
	public void setDPAGACNO(String dPAGACNO) {
		DPAGACNO = dPAGACNO;
	}
	public String getDPBHNO() {
		return DPBHNO;
	}
	public void setDPBHNO(String dPBHNO) {
		DPBHNO = dPBHNO;
	}
	public String getDPACNO() {
		return DPACNO;
	}
	public void setDPACNO(String dPACNO) {
		DPACNO = dPACNO;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
