package tw.com.fstop.nnb.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.owasp.esapi.ESAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import fstop.orm.po.ADMMAILLOG;
import fstop.orm.po.SYSPARAMDATA;
import fstop.orm.po.TXNFXRECORD;
import fstop.orm.po.TXNRECMAILLOGRELATION;
import fstop.orm.po.TXNTWRECORD;
import fstop.orm.po.TXNTWSCHPAY;
import fstop.orm.po.TXNTWSCHPAYDATA;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.custom.annotation.ACNSET1;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmBankDao;
import tw.com.fstop.tbb.nnb.dao.AdmMailContentDao;
import tw.com.fstop.tbb.nnb.dao.AdmMailLogDao;
import tw.com.fstop.tbb.nnb.dao.AdmMsgCodeDao;
import tw.com.fstop.tbb.nnb.dao.Nb3SysOpDao;
import tw.com.fstop.tbb.nnb.dao.SysParamDataDao;
import tw.com.fstop.tbb.nnb.dao.TxnFxRecordDao;
import tw.com.fstop.tbb.nnb.dao.TxnRecMailLogRelationDao;
import tw.com.fstop.tbb.nnb.dao.TxnTwRecordDao;
import tw.com.fstop.tbb.nnb.dao.TxnTwSchPayDao;
import tw.com.fstop.tbb.nnb.dao.TxnTwSchPayDataDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.ObjectConvertUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtils;

@Service
public class Acct_Reservation_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private TxnTwSchPayDao txntwschpaydao;
	@Autowired
	private TxnTwSchPayDataDao txntwschpaydatadao;
	@Autowired
	private TxnTwRecordDao txntwrecorddao;
	@Autowired
	private TxnFxRecordDao txnfxrecorddao;
	@Autowired
	private Nb3SysOpDao nb3sysopdao;
	@Autowired
	private AdmBankDao admbankdao;
	@Autowired
	private AdmMsgCodeDao admmsgcodedao;
	@Autowired
	private SysParamDataDao sysParamDataDao;
	@Autowired
	private TxnRecMailLogRelationDao txnRecMailLogRelationDao;
	@Autowired
	private AdmMailLogDao admMailLogDao;
	@Autowired
	private AdmMailContentDao admMailContentDao;
	@Autowired
	I18n i18n;

	/**
	 * (臺幣)預約明細查詢
	 * 
	 * @param cusidn
	 * @return
	 */
	public BaseResult reservation_detail(String cusidn) {

		log.trace("reservation_detail_service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// call ws API
			List<TXNTWSCHPAY> TXNTWSCHPAY = txntwschpaydao.getDptxstatus(cusidn);
			List<Map<String, Object>> data = new ArrayList<>();
			if (TXNTWSCHPAY != null) {
				for (TXNTWSCHPAY each : TXNTWSCHPAY) {
					// 將po轉成map
					Map<String, Object> eachMap = ObjectConvertUtil.object2Map(each);

					// 生效日
					String dpfdate = (String) eachMap.get("DPFDATE");
					eachMap.put("DPFDATE", DateUtil.convertDate(2, dpfdate, "yyyyMMdd", "yyyy/MM/dd"));
					// 截止日
					String dptdate = (String) eachMap.get("DPTDATE");
					eachMap.put("DPTDATE", DateUtil.convertDate(2, dptdate, "yyyyMMdd", "yyyy/MM/dd"));
					// 如果兩個相同則只顯示一個
					if (dpfdate.equals(dptdate)) {
						eachMap.put("DPTDATE", "");
					}
					// 單次預約/循環預約
					String dptxtype = (String) eachMap.get("DPTXTYPE");
					if ("S".equals(dptxtype)) {
						// 特定日期
						eachMap.put("DPPERMTDATE", I18n.getMessage("LB.X1796"));
					} else {
						// 每月 日
						String dppermtdate = (String) eachMap.get("DPPERMTDATE");
						eachMap.put("DPPERMTDATE",
								I18n.getMessage("LB.Monthly") + dppermtdate + I18n.getMessage("LB.D0586"));
					}
					// 下次轉帳日
					String dpschno = (String) eachMap.get("DPSCHNO");
					String dpnextdate = txntwschpaydatadao.getDpschtxdate(cusidn, dpschno);
					if (dpnextdate == null || dpnextdate == "") {
						// 如果是空就是生效日
						eachMap.put("DPNEXTDATE", DateUtil.convertDate(2, dpfdate, "yyyyMMdd", "yyyy/MM/dd"));
						eachMap.put("DPNEXTDATEchk", dpfdate);
					} else {
						eachMap.put("DPNEXTDATE", DateUtil.convertDate(2, dpnextdate, "yyyyMMdd", "yyyy/MM/dd"));
						eachMap.put("DPNEXTDATEchk", dpnextdate);
					}
					// 比對bank名稱
					String dpsvbh = ((String) eachMap.get("DPSVBH")).replace(" ", "");
					String bank = "";
					// 銀行名稱多語系
					String locale = LocaleContextHolder.getLocale().toString();
					if (locale.equals("zh_CN")) {
						bank = admbankdao.getBankChName(dpsvbh);
					} else if (locale.equals("en")) {
						bank = admbankdao.getBankEnName(dpsvbh);
					} else {
						bank = admbankdao.getBankName(dpsvbh);
					}

					if (StrUtils.isNotEmpty(dpsvbh)) {
						dpsvbh = dpsvbh + "-" + bank;
					} else {
						dpsvbh = "";
					}
					eachMap.put("DPSVBH", dpsvbh);
					// 金額格式處理
					eachMap.put("DPTXAMTS", NumericUtil.fmtAmount((String) eachMap.get("DPTXAMT"), 2));
					// 交易類別
					String adopid = (String) eachMap.get("ADOPID");
					String idtype = "";
					// 交易類別多語系
					if (locale.equals("zh_CN")) {
						idtype = nb3sysopdao.findAdopidCN(adopid);
					} else if (locale.equals("en")) {
						idtype = nb3sysopdao.findAdopidEN(adopid);
					} else {
						idtype = nb3sysopdao.findAdopid(adopid);
					}

					if (idtype != "") {
						idtype = ESAPI.encoder().encodeForHTML(idtype);
						eachMap.put("TXTYPE", idtype);
					} else {
						eachMap.put("TXTYPE", "");
					}
					// 交易機制
					String dptxcode = (String) eachMap.get("DPTXCODE");
					eachMap.get("DPTXCODE");
					if ("1".equals(dptxcode)) {
						// 電子簽章(i-key)
						eachMap.put("DPTXCODES", I18n.getMessage("LB.Electronic_signature"));
					} else if ("2".equals(dptxcode)) {
						// 晶片金融卡
						eachMap.put("DPTXCODES", I18n.getMessage("LB.Financial_debit_card"));
					} else if ("3".equals(dptxcode)) {
						// OTP
						eachMap.put("DPTXCODES", I18n.getMessage("LB.X2330"));
					} else if ("4".equals(dptxcode)) {
						// 軟體憑證(隨護神盾)
						eachMap.put("DPTXCODES", I18n.getMessage("LB.X2301"));
					} else if ("5".equals(dptxcode)) {
						// 快速交易
						eachMap.put("DPTXCODES", I18n.getMessage("LB.X2302"));
					} else if ("6".equals(dptxcode)) {
						// 小額交易
						eachMap.put("DPTXCODES", I18n.getMessage("LB.X2303"));
					} else if ("7".equals(dptxcode)) {
						// IDGATE
						eachMap.put("DPTXCODES", I18n.getMessage("LB.X2501"));
					} else {
						// 交易密碼(SSL)
						eachMap.put("DPTXCODES", I18n.getMessage("LB.SSL_password"));
					}
					// 狀態
					String status = (String) eachMap.get("DPTXSTATUS");
					if ("0".equals(status)) {
						// 預約成功
						eachMap.put("STATUS", status);
						eachMap.put("DPTXSTATUS", I18n.getMessage("LB.W0284"));
					} else {
						// 已取消預約
						eachMap.put("STATUS", status);
						eachMap.put("DPTXSTATUS", I18n.getMessage("LB.X1798"));
					}

					data.add((Map<String, Object>) eachMap);
				}
				log.trace(ESAPIUtil.vaildLog("data>>" + data));
				bs.addData("REC", data);

				// 筆數
				int COUNT = 0;
				COUNT = data.size();
				log.trace("COUNT>>{}", COUNT);
				bs.addData("COUNT", String.valueOf(COUNT));

				// 現在時間
				bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				bs.addData("TODAY", DateUtil.getCurentDateTime("yyyyMMdd"));
				log.trace("bsData>>{}", bs.getData());

				// 通過
				bs.setResult(Boolean.TRUE);
				// if(COUNT == 0) {
				// bs.setResult(false);
				// bs.setMsgCode("ENRD");
				// getMessageByMsgCode(bs);
				// }
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}

		return bs;
	}

	/**
	 * (臺幣)預約明細查詢確認頁
	 * 
	 * @param cusidn
	 * @return
	 */
	public BaseResult reservation_confirm(Map<String, String> reqParam) {

		log.trace("reservation_confirm_service>>");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			log.debug(ESAPIUtil.vaildLog("ROWDATA >> " + reqParam.get("ROWDATA")));
			String value = reqParam.get("ROWDATA");
			value = value.substring(1, value.length() - 1); // remove curly brackets
			log.trace(ESAPIUtil.vaildLog("ROWDATA.value >> " + value));
			String[] keyValuePairs = value.split(","); // split the string to creat key-value pairs
			log.trace(ESAPIUtil.vaildLog("ROWDATA.keyValuePairs: >> " + CodeUtil.toJson(keyValuePairs)));
			Map<String, String> map = new HashMap<>();
			for (String pair : keyValuePairs) // iterate over the pairs
			{
				String[] entry = pair.split("="); // split the pairs to get key and value

				String mapKey = "";
				String MapValue = "";
				log.trace("ROWDATA.entry.length: " + entry.length);
				if (entry.length == 2) {
					mapKey = "".equals(entry[0].trim()) ? "" : entry[0].trim();
					MapValue = "".equals(entry[1].trim()) ? "" : entry[1].trim();
				} else {
					mapKey = entry[0].trim();
				}
				// 金額處理
				map.put("DPTXAMT", NumericUtil.fmtAmount(map.get("DPTXAMT"), 2));
				log.trace(ESAPIUtil.vaildLog("ROWDATA.mapKey >> " + mapKey));
				log.trace(ESAPIUtil.vaildLog("ROWDATA.MapValue >> " + MapValue));
				map.put(mapKey, MapValue); // add them to the hashmap and trim whitespaces
			}
			log.debug(ESAPIUtil.vaildLog("ROWDATA >> " + CodeUtil.toJson(map)));
			bs.addData("dataSet", map);
			bs.setResult(true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}

		return bs;
	}

	/**
	 * (臺幣)預約明細查詢結果頁
	 * 
	 * @param cusidn
	 * @return
	 */
	public BaseResult reservation_result(String cusidn, Map<String, String> reqParam) {

		log.trace("reservation_result_service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// call ws API
			String dpschid = reqParam.get("DPSCHID");
			TXNTWSCHPAY txntwschpay = txntwschpaydao.updataDptxstatus(dpschid);
			if (txntwschpay != null) {
				bs.addData("dataSet", reqParam);
				bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				bs.setResult(Boolean.TRUE);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}

		return bs;
	}

	/**
	 * 轉出記錄查詢
	 * 
	 * @param cusidn
	 * @return
	 */
	@ACNSET1
	public BaseResult transfer_record_query(String cusidn) {
		log.trace("transfer_record_query");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// call REST API
			bs = N920_REST(cusidn, TRANSFER_RECORD);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error("transfer_record_query", e);
		}
		return bs;
	}

	/**
	 * 轉出記錄查詢結果
	 * 
	 * @param cusidn
	 * @return
	 */
	public BaseResult transfer_record_query_result(String cusidn, Map<String, String> reqParam) {
		log.trace("transfer_record_query_result ");
		BaseResult bs = null;
		try {
			int systemDefineResendTimes = 2;
			try {
				SYSPARAMDATA sysparam = sysParamDataDao.getAll().get(0);
				systemDefineResendTimes = new Integer(sysparam.getADRESENDTIMES());
			} catch (Exception e) {
				log.error("無法取得系統設定重發次數限制, 使用預設值 2 次.", e);
			}
			log.trace(ESAPIUtil.vaildLog("TIMES>>>" + systemDefineResendTimes));

			log.trace(ESAPIUtil.vaildLog("reqParam >> " + CodeUtil.toJson(reqParam)));
			bs = new BaseResult();
			String cmsdate = (String) reqParam.get("CMSDATE");
			String cmedate = (String) reqParam.get("CMEDATE");
			List<TXNTWRECORD> TwList = txntwrecorddao.findByDateRange(cusidn, reqParam);
			
			
			//20211103更新 : 先找出 "同時段同ID" 的ADMMAILLOG資料  
			//ADMMAILLOG的ADSENDTIME欄位有建INDEX , 串起始日 xxxxxxxx000000 , 結束日 xxxxxxxx235959
			List<ADMMAILLOG> mailList = admMailLogDao.findByDateRange(cusidn,"Y",cmsdate.replace("/", "")+"000000",cmedate.replace("/", "")+"235959");
			
			
			List<Map<String, Object>> data = new ArrayList<>();
			List<Map<String, String>> countdata = new ArrayList<>();
			Map<String, String> mapdata = new HashMap<String, String>();
			log.trace(ESAPIUtil.vaildLog("TWRECORD >> " + CodeUtil.toJson(TwList)));
			// TW處理
			if (TwList != null) {
				for (TXNTWRECORD each : TwList) {
					// 將po轉成map
					Map<String, Object> eachMap = ObjectConvertUtil.object2Map(each);
					
					//20211103更新 比對資料
					String ADSENDTIME = (String)eachMap.get("DPTXDATE")+(String)eachMap.get("DPTXTIME");
					boolean show_button= false;
					for(ADMMAILLOG eachMail:mailList) {
						if( StrUtils.isNotEmpty((String)eachMail.getADSENDSTATUS()) && StrUtils.isNotEmpty(ADSENDTIME) 
							&& ((String)eachMail.getADSENDTIME()).equals(ADSENDTIME) //ADSENDTIME FROM TXNTWRECORD DPTXDATE+DPTXTIME
							&& ((String)eachMail.getADACNO()).equals(each.getDPWDAC())) { //比對相同帳號
							show_button = true ;
							break;
						}
					}
					eachMap.put("show_button", show_button);
					eachMap.remove("DPTITAINFO");
					eachMap.remove("DPTOTAINFO");
					eachMap.remove("LASTDATE");
					eachMap.remove("LASTTIME");
					// 若為空放0，避免前端出錯
					eachMap.put("DPREMAIL",
							(StrUtils.isEmpty((String) eachMap.get("DPREMAIL"))
									|| " ".equals((String) eachMap.get("DPREMAIL"))) ? "0"
											: (String) eachMap.get("DPREMAIL"));
					// 轉帳日期格式
					eachMap.put("DPTXDATE",
							DateUtil.convertDate(2, (String) eachMap.get("DPTXDATE"), "yyyyMMdd", "yyyy/MM/dd"));
					// 銀行代碼與轉入帳號
					String dpsvbh = (String) eachMap.get("DPSVBH");
					String bank = "";
					// 銀行名稱多語系
					String locale = LocaleContextHolder.getLocale().toString();
					if (locale.equals("zh_CN")) {
						bank = admbankdao.getBankChName(dpsvbh);
					} else if (locale.equals("en")) {
						bank = admbankdao.getBankEnName(dpsvbh);
					} else {
						bank = admbankdao.getBankName(dpsvbh);
					}

					if (!bank.equals("")) {
						dpsvbh = dpsvbh + "-" + bank;
						eachMap.put("DPSVBH", dpsvbh);
					} else {
						eachMap.put("DPSVBH", "");
					}
					// 轉帳金額
					eachMap.put("DPTXAMT", NumericUtil.fmtAmount((String) eachMap.get("DPTXAMT"), 2));
					// 手續費
					eachMap.put("DPEFEE", NumericUtil.fmtAmount((String) eachMap.get("DPEFEE"), 2));
					// 交易類別
					String dpschid = eachMap.get("DPSCHID").toString();
					String adopid = (String) eachMap.get("ADOPID");
					String idtype = "";
					if (locale.equals("zh_CN")) {
						idtype = nb3sysopdao.findAdopidCN(adopid);
					} else if (locale.equals("en")) {
						idtype = nb3sysopdao.findAdopidEN(adopid);
					} else {
						idtype = nb3sysopdao.findAdopid(adopid);
					}

					if (!idtype.equals("")) {
						// idtype = ESAPI.encoder().encodeForHTML(idtype);
						// 是否即時
						if (!dpschid.equals("0")) {
							eachMap.put("ADOPID", I18n.getMessage("LB.Booking") + idtype);
						} else {
							eachMap.put("ADOPID", idtype);
						}
					} else {
						eachMap.put("ADOPID", "");
					}
					
					// 資料存入
					data.add(eachMap);
				}

				log.trace(ESAPIUtil.vaildLog("data >> " + CodeUtil.toJson(data)));
				bs.addData("REC", data);
				// 日期區間
				if (cmsdate.equals(cmedate)) {
					bs.addData("cmedate", cmedate);
				} else {
					bs.addData("cmedate", cmsdate + " ～ " + cmedate);
				}

				// 筆數
				int COUNT = 0;
				COUNT = data.size();
				log.trace("COUNT>>{}", COUNT);
				bs.addData("COUNT", String.valueOf(COUNT));

				// 處理空值顯示
				if (TwList == null || data == null || COUNT == 0) {
					mapdata.put("DPTXDATE", " ");
					mapdata.put("DPWDAC", " ");
					mapdata.put("DPSVBH", " ");
					mapdata.put("DPSVAC", " ");
					mapdata.put("DPTXAMT", " ");
					mapdata.put("DPEFEE", " ");
					mapdata.put("DPTXNO", " ");
					mapdata.put("ADOPID", " ");
					mapdata.put("DPTXMEMO", " ");
					countdata.add(mapdata);
					bs.addData("REC", countdata);
				}

				// 現在時間
				bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				// Email可重發次數
				bs.addData("EMAILTIMES", systemDefineResendTimes);
				// 通過
				bs.setResult(true);
				log.trace("transfer_record_query_result >>{}", bs.getData());
				// if(COUNT == 0) {
				// bs.setResult(false);
				// bs.setMsgCode("ENRD");
				// getMessageByMsgCode(bs);
				// }
			}
		} catch (Exception e) {
			log.error("transfer_record_query_result", e);
		}
		return bs;
	}

	public BaseResult transfer_record_query_mail(String cusidn, Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("Mail reqParam>>>>" + reqParam));
			bs = new BaseResult();
			String rectype = reqParam.get("RECTYPE"); // tw or fx
			String adtxno = reqParam.get("ADTXNO");

			int systemDefineResendTimes = Integer.parseInt(reqParam.get("TIMES"));
			log.trace(ESAPIUtil.vaildLog("Get systemDefineResendTimes>>>>" + systemDefineResendTimes));

			int sendNum = 0;
			Object __record = null;
			if ("TW".equalsIgnoreCase(rectype)) {
				try {
					TXNTWRECORD record = txntwrecorddao.get(TXNTWRECORD.class, adtxno);
					log.trace(ESAPIUtil.vaildLog("TW record = " + record.getADTXNO()));
					__record = record;
					sendNum = new Integer(StrUtils.isEmpty(record.getDPREMAIL()) ? "0" : record.getDPREMAIL());
				} catch (Exception e) {
					log.error(ESAPIUtil.vaildLog("TXNTWRECORD ERROR >>>" + e));
				}
			}
			if ("FX".equalsIgnoreCase(rectype)) {
				try {
					TXNFXRECORD record = txnfxrecorddao.get(TXNFXRECORD.class, adtxno);
					log.trace(ESAPIUtil.vaildLog("FX record = " + record.getADTXNO()));
					__record = record;
					sendNum = new Integer(StrUtils.isEmpty(record.getFXREMAIL()) ? "0" : record.getFXREMAIL());
				} catch (Exception e) {
					log.error(ESAPIUtil.vaildLog("TXNFXRECORD ERROR >>>" + e));
				}
			}

			if (sendNum > systemDefineResendTimes) {
				bs.addData("faild", i18n.getMsg("LB.Alert122"));
				bs.setResult(false);
			}

			boolean isSendSuccess = false;

			/*
			 * 外匯 F002 會發給收款人及付款人兩封 EMAIL, 要一起發送
			 */
			List<TXNRECMAILLOGRELATION> relations = txnRecMailLogRelationDao.findByTypeAndAdtxno(rectype, adtxno);
			log.trace(ESAPIUtil.vaildLog("relations = " + relations.size()));
			if (relations.size() != 0) {
				for (TXNRECMAILLOGRELATION relation : relations) {
					ADMMAILLOG maillog = admMailLogDao.get(ADMMAILLOG.class, relation.getADMAILLOGID());
					log.trace("maillog data = " + maillog.toString());
					log.trace("maillog getADMAILLOGID data = " + maillog.getADMAILLOGID());
					log.trace("maillog getADMAILCONTENT data = " + maillog.getADMAILCONTENT());
					log.trace("maillog getADMAILACNO data = " + maillog.getADMAILACNO());
					log.trace("maillog getADMSUBJECT data = " + maillog.getADMSUBJECT());

					reqParam.put("ADMAILCONTENT", maillog.getADMAILCONTENT());
					reqParam.put("ADMAILACNO", maillog.getADMAILACNO());
					// reqParam.put("ADMSUBJECT", maillog.getADMSUBJECT());

					String subject = maillog.getADMSUBJECT();
					String mail_subject_env = SpringBeanFactory.getBean("MAIL_SUBJECT_ENV");
					switch (mail_subject_env) {
					case "D":
						if (subject.indexOf("[新世代網路銀行 開發環境] ") < 0) {
							subject = "[新世代網路銀行 開發環境] " + subject;
						}
						break;
					case "T":
						if (subject.indexOf("[新世代網路銀行 測試環境] ") < 0) {
							subject = "[新世代網路銀行 測試環境] " + subject;
						}
						break;
					case "P":
						//因為有新舊資料 故先比對 舊資料才做替換
						if (subject.indexOf("臺灣企銀 新世代網路銀行 ") < 0) {
							
							// subject 有4種 >> "臺灣企銀網路銀行" "網路銀行" "臺灣企銀" "無顯示"
							// ====無顯示 判別=====
							if (subject.indexOf("臺灣企銀") < 0 && subject.indexOf("網路銀行") < 0) {
								subject = "臺灣企銀 新世代網路銀行 " + subject;
								// ====臺灣企銀網路銀行 判別====
							} else if (subject.indexOf("臺灣企銀網路銀行") >= 0) {
								subject = subject.replace("臺灣企銀網路銀行", "臺灣企銀 新世代網路銀行 ");
								// ====網路銀行 判別====
							} else if (subject.indexOf("臺灣企銀") < 0 && subject.indexOf("網路銀行") >= 0) {
								subject = subject.replace("網路銀行", "臺灣企銀 新世代網路銀行 ");
								// ====臺灣企銀 判別====
							} else if (subject.indexOf("臺灣企銀") >= 0 && subject.indexOf("網路銀行") < 0) {
								subject = subject.replace("臺灣企銀", "臺灣企銀 新世代網路銀行 ");
							}
						
						}
						break;
					}
					reqParam.put("ADMSUBJECT", subject);

					log.trace(ESAPIUtil.vaildLog("maillog reqParam" + reqParam));
				}
				try {
					bs = reSend_REST(reqParam);
				} catch (Exception e) {
					isSendSuccess = false;
				}
			}
			log.trace(ESAPIUtil.vaildLog("Get bs data>>>>" + bs.getData()));
			if (bs.getData() != null) {
				Map<String, Object> dataMap = (Map) bs.getData();
				isSendSuccess = "0".equals(dataMap.get("sedMailResult").toString()) ? true : false;
				log.trace(ESAPIUtil.vaildLog("isSendSuccess : " + isSendSuccess));
				if (isSendSuccess) {
					if ("TW".equalsIgnoreCase(rectype)) {
						String TwDpremail = txntwrecorddao.updataDpremail(cusidn, adtxno);
						log.trace(ESAPIUtil.vaildLog("TWRECORDupdata>>{}" + TwDpremail));

						bs.addData("success", TwDpremail);
						log.trace(ESAPIUtil.vaildLog("transfer_record_query_result >>{}" + bs.getData()));
						bs.setResult(true);
					} else if ("FX".equalsIgnoreCase(rectype)) {
						String FxDpremail = txnfxrecorddao.updataDpremail(cusidn, adtxno);
						log.trace(ESAPIUtil.vaildLog("FXRECORDupdata>>{}" + FxDpremail));

						bs.addData("success", FxDpremail);
						log.trace(ESAPIUtil.vaildLog("transfer_record_query_result >>{}" + bs.getData()));
						bs.setResult(true);
					} else {
						bs.addData("faild", i18n.getMsg("LB.X1799"));
						bs.setResult(false);
					}
				} else {
					bs.addData("faild", i18n.getMsg("LB.X1799"));
					bs.setResult(false);
				}
			} else {
				bs.addData("faild", i18n.getMsg("LB.X1799"));
				bs.setResult(false);
			}
			// 通過
		} catch (Exception e) {
			log.error("transfer_record_query_result", e);
		}
		return bs;
	}

	/**
	 * 預約交易結果查詢結果頁
	 * 
	 * @param cusidn
	 * @param reqParam
	 *            前端輸入變數
	 * @return
	 */
	public BaseResult reservation_trans_result(String cusidn, Map<String, String> reqParam) {

		log.trace("reservation_trans_result>>{}", cusidn);
		BaseResult bs = null;
		bs = new BaseResult();
		try {
			// call ws API
			List<Map<String, Object>> data = new ArrayList<>();
			List<Map<String, String>> countdata = new ArrayList<>();
			Map<String, String> mapdata = new HashMap<String, String>();
			String fgtxstatus = reqParam.get("FGTXSTATUS");
			String cmsdate = reqParam.get("CMSDATE");
			String cmedate = reqParam.get("CMEDATE");
			if ("All".equals(fgtxstatus)) {
				List<TXNTWSCHPAYDATA> TXNTWSCHPAYDATA = txntwschpaydatadao.getAll(cusidn, reqParam);
				log.trace(ESAPIUtil.vaildLog("AllTXNTWSCHPAYDATA>>>" + TXNTWSCHPAYDATA));
				if (TXNTWSCHPAYDATA != null) {
					for (TXNTWSCHPAYDATA each : TXNTWSCHPAYDATA) {
						// 將po轉成map
						Map<String, Object> eachMap = ObjectConvertUtil.object2Map(each);
						// 將復合主鍵的key由po轉成map
						Map<String, Object> pks = ObjectConvertUtil.object2Map(eachMap.get("pks"));
						// 把複合主鍵的key塞進eachMap
						eachMap.put("DPSCHNO", pks.get("DPSCHNO"));
						eachMap.put("DPSCHTXDATE", pks.get("DPSCHTXDATE"));

						// 日期格式處理
						String DPSCHTXDATE = (String) eachMap.get("DPSCHTXDATE");
						eachMap.put("DPSCHTXDATE", DateUtil.convertDate(2, DPSCHTXDATE, "yyyyMMdd", "yyyy/MM/dd"));

						// 金額格式處理
						eachMap.put("DPTXAMT", NumericUtil.fmtAmount((String) eachMap.get("DPTXAMT"), 2));
						eachMap.put("DPEFEE", NumericUtil.fmtAmount((String) eachMap.get("DPEFEE"), 2));

						// 比對bank名稱
						String dpsvbh = ((String) eachMap.get("DPSVBH")).replace(" ", "");
						String bank = "";
						// 銀行名稱多語系
						String locale = LocaleContextHolder.getLocale().toString();
						if (locale.equals("zh_CN")) {
							bank = admbankdao.getBankChName(dpsvbh);
						} else if (locale.equals("en")) {
							bank = admbankdao.getBankEnName(dpsvbh);
						} else {
							bank = admbankdao.getBankName(dpsvbh);
						}
						if (StrUtils.isNotEmpty(dpsvbh)) {
							dpsvbh = dpsvbh + bank;
						} else {
							dpsvbh = "";
						}
						eachMap.put("DPSVBH", dpsvbh);

						String adopid = (String) eachMap.get("ADOPID");
						String idtype = "";
						// 交易類別多語系
						if (locale.equals("zh_CN")) {
							idtype = nb3sysopdao.findAdopidCN(adopid);
						} else if (locale.equals("en")) {
							idtype = nb3sysopdao.findAdopidEN(adopid);
						} else {
							idtype = nb3sysopdao.findAdopid(adopid);
						}

						if (idtype != "") {
							String idtypeh = ESAPI.encoder().encodeForHTML(idtype);
							eachMap.put("DPSEQ", idtype);
							eachMap.put("DPSEQH", idtypeh);
						} else {
							eachMap.put("DPSEQ", "");
							eachMap.put("DPSEQH", "");
						}

						// 轉帳結果狀態翻譯
						String status = (String) eachMap.get("DPTXSTATUS");
						if ("0".equals(status)) {
							// 成功
							eachMap.put("DPTXSTATUS", I18n.getMessage("LB.D1099"));
						} else if (" ".equals(status)) {
							// 未執行
							eachMap.put("DPTXSTATUS", I18n.getMessage("LB.X0048"));
						} else if ("1".equals(status)) {
							// 失敗
							eachMap.put("DPTXSTATUS", I18n.getMessage("LB.W0056"));
							// 比對錯誤代碼名稱
							String dpexcode = (String) eachMap.get("DPEXCODE");
							String codemsg = admmsgcodedao.getCodeMsg(dpexcode);
							if (codemsg != null && !"".equals(codemsg)) {
								eachMap.put("CODEMSG", codemsg);
							} else {
								eachMap.put("CODEMSG", "");
							}
						} else {
							// 處理中
							eachMap.put("DPTXSTATUS", I18n.getMessage("LB.W0057"));
						}
						data.add((Map<String, Object>) eachMap);
					}
					log.trace(ESAPIUtil.vaildLog("data>>" + data));
					bs.addData("REC", data);

					// 筆數
					int COUNT = 0;
					COUNT = data.size();
					log.trace("COUNT>>{}", COUNT);
					bs.addData("COUNT", String.valueOf(COUNT));

					// 處理空值顯示
					if (TXNTWSCHPAYDATA == null || data == null || COUNT == 0) {
						mapdata.put("DPSCHNO", " ");
						mapdata.put("DPSCHTXDATE", " ");
						mapdata.put("DPWDAC", " ");
						mapdata.put("DPSVBH", " ");
						mapdata.put("DPSVAC", " ");
						mapdata.put("DPTXAMT", " ");
						mapdata.put("DPEFEE", " ");
						mapdata.put("DPSTANNO", " ");
						mapdata.put("DPSEQ", " ");
						mapdata.put("DPTXMEMO", " ");
						mapdata.put("DPTXSTATUS", " ");
						countdata.add(mapdata);
						bs.addData("REC", countdata);
					}
					log.trace("bsData>>{}", bs.getData());
					// 標題狀態翻譯
					if ("All".equals(fgtxstatus)) {
						// 全部
						bs.addData("FGTXSTATUS", I18n.getMessage("LB.All"));
					} else if ("0".equals(fgtxstatus)) {
						// 成功
						bs.addData("FGTXSTATUS", I18n.getMessage("LB.D1099"));
					} else if ("1".equals(fgtxstatus)) {
						// 失敗
						bs.addData("FGTXSTATUS", I18n.getMessage("LB.W0056"));
					} else {
						// 處理中
						bs.addData("FGTXSTATUS", I18n.getMessage("LB.W0057"));
					}

					// 日期區間
					if (cmsdate.equals(cmedate)) {
						bs.addData("cmedate", cmedate);
					} else {
						bs.addData("cmedate", cmsdate + " ～ " + cmedate);
					}
					// 現在時間
					bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
					// 通過
					bs.setResult(Boolean.TRUE);
					// if(COUNT == 0) {
					// bs.setResult(false);
					// bs.setMsgCode("ENRD");
					// getMessageByMsgCode(bs);
					// }
				}
			} else {
				List<TXNTWSCHPAYDATA> TXNTWSCHPAYDATA = txntwschpaydatadao.getByUid(cusidn, reqParam);
				log.trace(ESAPIUtil.vaildLog("TXNTWSCHPAYDATA >> " + CodeUtil.toJson(TXNTWSCHPAYDATA)));
				if (TXNTWSCHPAYDATA != null) {
					for (TXNTWSCHPAYDATA each : TXNTWSCHPAYDATA) {
						// 將po轉成map
						Map<String, Object> eachMap = ObjectConvertUtil.object2Map(each);
						// 將復合主鍵的key由po轉成map
						Map<String, Object> pks = ObjectConvertUtil.object2Map(eachMap.get("pks"));
						// 把複合主鍵的key塞進eachMap
						eachMap.put("DPSCHNO", pks.get("DPSCHNO"));
						eachMap.put("DPSCHTXDATE", pks.get("DPSCHTXDATE"));

						// 日期格式處理
						String DPSCHTXDATE = (String) eachMap.get("DPSCHTXDATE");
						eachMap.put("DPSCHTXDATE", DateUtil.convertDate(2, DPSCHTXDATE, "yyyyMMdd", "yyyy/MM/dd"));

						// 金額格式處理
						eachMap.put("DPTXAMT", NumericUtil.fmtAmount((String) eachMap.get("DPTXAMT"), 2));
						eachMap.put("DPEFEE", NumericUtil.fmtAmount((String) eachMap.get("DPEFEE"), 2));

						// 比對bank名稱
						String dpsvbh = ((String) eachMap.get("DPSVBH")).replace(" ", "");
						String bank = "";
						// 銀行名稱多語系
						String locale = LocaleContextHolder.getLocale().toString();
						if (locale.equals("zh_CN")) {
							bank = admbankdao.getBankChName(dpsvbh);
						} else if (locale.equals("en")) {
							bank = admbankdao.getBankEnName(dpsvbh);
						} else {
							bank = admbankdao.getBankName(dpsvbh);
						}
						if (StrUtils.isNotEmpty(dpsvbh)) {
							dpsvbh = dpsvbh + bank;
						} else {
							dpsvbh = "";
						}
						eachMap.put("DPSVBH", dpsvbh);

						String adopid = (String) eachMap.get("ADOPID");
						String idtype = "";
						// 交易類別多語系
						if (locale.equals("zh_CN")) {
							idtype = nb3sysopdao.findAdopidCN(adopid);
						} else if (locale.equals("en")) {
							idtype = nb3sysopdao.findAdopidEN(adopid);
						} else {
							idtype = nb3sysopdao.findAdopid(adopid);
						}

						if (idtype != "") {
							String idtypeh = ESAPI.encoder().encodeForHTML(idtype);
							eachMap.put("DPSEQ", idtype);
							eachMap.put("DPSEQH", idtypeh);
						} else {
							eachMap.put("DPSEQ", "");
							eachMap.put("DPSEQH", "");
						}

						// 內容狀態翻譯
						String status = (String) eachMap.get("DPTXSTATUS");
						if ("0".equals(status)) {
							// 成功
							eachMap.put("DPTXSTATUS", I18n.getMessage("LB.D1099"));
						} else if (" ".equals(status)) {
							// 未執行
							eachMap.put("DPTXSTATUS", I18n.getMessage("LB.X0048"));
						} else if ("1".equals(status)) {
							// 失敗
							// 比對錯誤代碼名稱
							String dpexcode = (String) eachMap.get("DPEXCODE");
							String codemsg = admmsgcodedao.getCodeMsg(dpexcode);
							if (codemsg != null && !"".equals(codemsg)) {
								eachMap.put("CODEMSG", codemsg);
							} else {
								eachMap.put("CODEMSG", "");
							}
							eachMap.put("DPTXSTATUS", I18n.getMessage("LB.W0056"));
						} else {
							// 處理中
							eachMap.put("DPTXSTATUS", I18n.getMessage("LB.W0057"));
						}

						data.add((Map<String, Object>) eachMap);
					}
					bs.addData("REC", data);

					// 筆數
					int COUNT = 0;
					COUNT = data.size();
					bs.addData("COUNT", String.valueOf(COUNT));

					// 處理空值顯示
					if (TXNTWSCHPAYDATA == null || data == null || COUNT == 0) {
						mapdata.put("DPSCHNO", " ");
						mapdata.put("DPSCHTXDATE", " ");
						mapdata.put("DPWDAC", " ");
						mapdata.put("DPSVBH", " ");
						mapdata.put("DPSVAC", " ");
						mapdata.put("DPTXAMT", " ");
						mapdata.put("DPEFEE", " ");
						mapdata.put("DPSTANNO", " ");
						mapdata.put("DPSEQ", " ");
						mapdata.put("DPTXMEMO", " ");
						mapdata.put("DPTXSTATUS", " ");
						countdata.add(mapdata);
						bs.addData("REC", countdata);
					}
					log.trace("bsData_count>>{}", bs.getData());

					// 標題狀態翻譯
					if ("All".equals(fgtxstatus)) {
						// 全部
						bs.addData("FGTXSTATUS", I18n.getMessage("LB.All"));
					} else if ("0".equals(fgtxstatus)) {
						// 成功
						bs.addData("FGTXSTATUS", I18n.getMessage("LB.D1099"));
					} else if ("1".equals(fgtxstatus)) {
						// 失敗
						bs.addData("FGTXSTATUS", I18n.getMessage("LB.W0056"));
					} else {
						// 處理中
						bs.addData("FGTXSTATUS", I18n.getMessage("LB.W0057"));
					}

					// 日期區間
					if (cmsdate.equals(cmedate)) {
						bs.addData("cmedate", cmedate);
					} else {
						bs.addData("cmedate", cmsdate + " ～ " + cmedate);
					}
					// 現在時間
					bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
					// 通過
					bs.setResult(Boolean.TRUE);
					// if(COUNT == 0) {
					// bs.setResult(false);
					// bs.setMsgCode("ENRD");
					// getMessageByMsgCode(bs);
					// }
				}
			}

		} catch (Exception e) {
			// e.printStackTrace();
			log.trace("reservation_trans_result error >> {}", e);
		}
		return bs;
	}

}
