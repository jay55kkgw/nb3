package tw.com.fstop.nnb.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.tbb.nnb.dao.TxnTwSchPayDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.StrUtils;
import fstop.orm.po.TXNTWRECORD;
import fstop.orm.po.TXNTWSCHPAY;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class TxnTwSchPay_Service {
	private Logger log = LoggerFactory.getLogger(this.getClass().getName());
	
	@Autowired
	private TxnTwSchPayDao txnTwSchPayDao;
	
	/*
	 * 以顧客統編(cusidn)當作條件去查詢DB，回傳結果
	 */
	public BaseResult query(Map<String, String> reqParam) {
		BaseResult bs = new BaseResult();
		List<TXNTWSCHPAY> list = null;
		
		try {
			String cusidn = reqParam.get("cusidn");
			log.trace(ESAPIUtil.vaildLog("統編cusidn = " + cusidn));
			if(cusidn.trim()==null || cusidn.trim().length() == 0) {
				bs.setMessage("1", "統編為空值");
				bs.setResult(Boolean.FALSE);
				return bs;
			}
			
			list = txnTwSchPayDao.getDptxstatus_2rd(cusidn);
			bs.setData(list);
			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", "查詢成功");
			log.info("TwSchPay.list.size >> {}", list.size());
		} catch (Exception e) {
			log.error("{}", e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", "查詢異常");
		}
		return bs;
	}

	/*
	 * 新增預約明細進去DB，回傳結果
	 */
	public BaseResult add(Map<String, String> reqParam) {
		BaseResult bs = new BaseResult();
		TXNTWSCHPAY txnTwSchPay = new TXNTWSCHPAY();
		//各電文代號的微服務
		Map<String,String> adopMap = SpringBeanFactory.getBean("ADOPIDMSA");
		List<String> errorMsg = new ArrayList<String>();
		//必須填入的欄位
		String[] notNull = {"adopid","dpuserid","dpfdate","dptdate","dpwdac","dpschno","dpsvac","dptxamt","logintype","dptxinfo"};
		//簡易日期格式檢查
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		sdf.setLenient(false);
		
		try {
			if(StrUtils.isNull(reqParam.get("logintype"))) {
				reqParam.put("logintype", "MB");
			}
			
			//檢查必填欄位是否為空值
			for(String str:notNull) {
				if(reqParam.get(str)== null || reqParam.get(str).length() == 0) {
					bs.setResult(Boolean.FALSE);
					bs.setMessage("1",str+" 欄位為必填");
					return bs;
				}
			}
			//判斷dppermtdate大於0時dptxtype = "C" else dptxtype = "S"
			if(reqParam.get("dppermtdate") == null ||reqParam.get("dppermtdate").length() == 0) {
				reqParam.put("dptxtype", "S");
			}else {
				reqParam.put("dptxtype", "C");
			}
			
			if(sdf.parse(reqParam.get("dpfdate")).getTime() > sdf.parse(reqParam.get("dptdate")).getTime()) {
				bs.setResult(Boolean.FALSE);
				bs.setMessage("1","生效日不得大於截止日");
				return bs;
			}
			
			if(Integer.parseInt(reqParam.get("dptxamt").trim()) < 1) {
				bs.setResult(Boolean.FALSE);
				bs.setMessage("1","匯款金額不正確");
				return bs;
			}
			
			if(adopMap.containsKey(reqParam.get("adopid").trim()) == false) {
				bs.setResult(Boolean.FALSE);
				bs.setMessage("1","電文不正確");
				return bs;
			}

			//填入電文所代入的微服務
			reqParam.put("msaddr",adopMap.get(reqParam.get("adopid")));
//			//將req所有的key轉成大寫
			Set keySet = reqParam.keySet();
			List<String> keyList = new ArrayList();
			Iterator it = keySet.iterator();
				
			while (it.hasNext()) {
				String str = (String) it.next();
				keyList.add(str);
			}

			for (String key : keyList) {
				if(!"msaddr".equals(key)) {
					reqParam.put(key.toUpperCase(), reqParam.get(key).toUpperCase());
				}else {
					reqParam.put(key.toUpperCase(), reqParam.get(key));
				}
					reqParam.remove(key);
				}
				//取得PO的所有變數名稱，序列化代碼不用檢查
				Field[] fs = TXNTWSCHPAY.class.getDeclaredFields();
	 			List<String> listName = new ArrayList<String>();
	 			for(Field f : fs) {
	 				if(!("serialVersionUID".equals(f.getName()))) {
	 					if(reqParam.get(f.getName()) == null) {
	 						reqParam.put(f.getName(), "");
	 					}
	 					listName.add(f.getName());
	 				}
	 			}
	 			
				log.trace(ESAPIUtil.vaildLog("test>>>>>>>>>{}"+ CodeUtil.toJson(reqParam)));
				
				reqParam.remove("DPSCHID");
				//將req轉成po
				txnTwSchPay = CodeUtil.objectCovert(TXNTWSCHPAY.class, reqParam);
				log.debug(ESAPIUtil.vaildLog("txnTwSchPay PO" + CodeUtil.toJson(txnTwSchPay)));
				txnTwSchPayDao.save(txnTwSchPay);
				bs.setResult(Boolean.TRUE);
				bs.setMessage("0", "新增成功");
//			}
		
		} catch (Exception e) {
			log.error("{}", e.toString());
			log.error("{}", e.getMessage());
			log.error("{}", e.getStackTrace());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", "新增異常");
		}
		return bs;
	}
	
	/*
	 * 以流水號、電文代號、使用者統編取消預約
	 */
	public BaseResult cancel(Map<String, String> reqParam) {
		BaseResult bs = new BaseResult();
		TXNTWSCHPAY txnTwSchPay = null;
		List<String> errorMsg = new ArrayList<String>();
		//電文代表的微服務
		Map<String,String> adopMap = SpringBeanFactory.getBean("ADOPIDMSA");
		try {
			
			//檢查req是否為空值、是否正確
			Integer dpschid = Integer.valueOf(reqParam.get("dpschid"));
			log.trace("流水號dpschid = " + dpschid);
			if(dpschid == null || reqParam.get("dpschid").trim().length() == 0) {
				bs.setMessage("1", "流水號為空值");
				bs.setResult(Boolean.FALSE);
				return bs;
			}else if(reqParam.get("dpuserid") == null ||reqParam.get("dpuserid").trim().length() == 0) {
				bs.setMessage("1", "使用者id為空值");
				bs.setResult(Boolean.FALSE);
				return bs;
			}else {
				TXNTWSCHPAY dptxstatus = txnTwSchPayDao.cancelAcn(dpschid,reqParam.get("dpuserid").trim());
				bs.setResult(Boolean.TRUE);
				bs.setMessage("0", "取消成功");
			}
		} catch (Exception e) {
			log.error("{}", e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", "取消異常");
		}
		return bs;
	}
	//ADD測試
//	public BaseResult addTest(Map<String, String> reqParam) {
//		BaseResult bs = new BaseResult();
//		try {
//		
//		TXNTWSCHPAY txntwnschpay = new TXNTWSCHPAY();
//		txntwnschpay.setADOPID("N075");
//		txntwnschpay.setDPUSERID("N075");
//		txntwnschpay.setDPTXTYPE("C");
//		txntwnschpay.setDPPERMTDATE("1");
//		txntwnschpay.setDPFDATE("20300201");
//		txntwnschpay.setDPTDATE("20301130");
//		txntwnschpay.setDPWDAC("A123456814");
//		txntwnschpay.setDPSVBH("050");
//		txntwnschpay.setDPSVAC("A123456814");
//		txntwnschpay.setDPTXAMT("8");
//		txntwnschpay.setDPTXMEMO("MSADDRTEST");
//		txntwnschpay.setDPTXMAILS("111");
//		txntwnschpay.setDPTXMAILMEMO("111");
//		txntwnschpay.setDPTXCODE("0");
//		txntwnschpay.setDPSDATE("20201201");
//		txntwnschpay.setDPSTIME("222222");
//		txntwnschpay.setDPTXSTATUS("0");
//		txntwnschpay.setXMLCA("1");
//		txntwnschpay.setXMLCN("111");
//		txntwnschpay.setMAC("1");
//		txntwnschpay.setLASTDATE("20300922");
//		txntwnschpay.setLASTTIME("      ");
//		txntwnschpay.setDPSCHNO("9899");
//		txntwnschpay.setLOGINTYPE("NB");
//		txntwnschpay.setDPTXINFO("111");
//		txntwnschpay.setMSADDR("ms_pay");
//		txnTwSchPayDao.save(txntwnschpay);
//		
//		bs.setResult(Boolean.TRUE);
//		bs.setMessage("0", "測試新增成功");
//		}catch(Exception e) {
//			log.error("{}", e.toString());
//			bs.setResult(Boolean.FALSE);
//			bs.setMessage("1", "取消異常");
//		}
//		return bs;
//	}
	
}

