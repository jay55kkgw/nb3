package tw.com.fstop.nnb.service;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import com.google.gson.JsonSyntaxException;
import fstop.orm.po.ADMCURRENCY;
import fstop.orm.po.ADMHOLIDAY;
import fstop.orm.po.ADMMSGCODE;
import fstop.orm.po.SYSPARAMDATA;
import fstop.ws.CmUserQry;
import fstop.ws.WsN531Out;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmCurrencyDao;
import tw.com.fstop.tbb.nnb.dao.AdmHolidayDao;
import tw.com.fstop.tbb.nnb.dao.AdmMsgCodeDao;
import tw.com.fstop.tbb.nnb.dao.SysParamDataDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.I18nHelper;

@Service
public class Fcy_Tdeposit_Service extends Base_Service {

    Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    DaoService daoService;

    @Autowired
    private SysParamDataDao sysParamDataDao;

    @Autowired
    private AdmCurrencyDao admCurrencyDao;

    @Autowired
    private AdmHolidayDao admHolidayDao;

    @Autowired
    private Acct_Service acct_Service;

    @Autowired
    private AdmMsgCodeDao admMsgCodeDao;

    @Autowired
    private I18n i18n;

    // 判斷外匯交易是否在營業時間內
    public boolean isFXHoliday() {
        boolean result = true;

        Date date = new Date();
        String currentDate = new SimpleDateFormat("yyyyMMdd").format(date);
        log.debug("currentDate={}", currentDate);
        String currentTime = new SimpleDateFormat("HHmmss").format(date);
        log.debug("currentTime={}", currentTime);

        SYSPARAMDATA sysParamData = sysParamDataDao.getByPK("NBSYS");

        if (sysParamData != null && sysParamData.getADPK() != null) {
            String tradeStart = sysParamData.getADFXSHH() + sysParamData.getADFXSMM()
                    + sysParamData.getADFXSSS();
            log.debug("tradeStart={}", tradeStart);
            String tradeEnd = sysParamData.getADFXEHH() + sysParamData.getADFXEMM()
                    + sysParamData.getADFXESS();
            log.debug("tradeEnd={}", tradeEnd);

            ADMHOLIDAY admHoliday = admHolidayDao.getByPK(currentDate);
            log.debug("admHoliday={}", admHoliday);
            // 非假日且在營業時間內
            if (admHoliday != null && admHoliday.getADHOLIDAYID() == null
                    && currentTime.compareTo(tradeStart) > 0
                    && currentTime.compareTo(tradeEnd) < 0) {
                result = false;
            }
        }
        return result;
    }

    /**
     * 轉存方式i18n
     * 
     * @param code
     * @return
     */
    public String getCode(String code) {
        String result = code;
        try {
            switch (code) {
                case "A":
                    // 本金利息一併轉期
                    result = i18n.getMsg("LB.Principal_and_Interest_rollover1");
                    break;
                case "B":
                    // 本金轉期－利息按月轉入活存，轉入帳號
                    result = i18n.getMsg("LB.Principal_rollover") + "－" + i18n
                            .getMsg("LB.Upon_maturity_or_terminating_credit_Interest_to_Account");
                    break;
                case "E":
                    // 本金轉期－利息於定存到期或解約轉入活存，轉入帳號
                    result = i18n.getMsg(
                            "LB.Principal_rollover-interest_to_demand_deposit_at_maturity_or_termination_input");
                    break;
                case "C":
                    // 利息按月轉入活存，轉入帳號
                    result = i18n
                            .getMsg("LB.Upon_maturity_or_terminating_credit_Interest_to_Account ");
                    break;
                case "D":
                    // 利息於定存到期或解約轉入活存，轉入帳號
                    result = i18n.getMsg(
                            "LB.Interest_to_demand_deposit_at_maturity_or_termination_input");
                    break;
                default:
                    log.warn("轉存方式代號不存在 parameter >> {}", code);
                    break;
            }
        } catch (Exception e) {
            log.error("getCode error. parameter >> {}", code, e);
        }
        return result;
    }

    /**
     * REST 取得外幣定存轉出帳號、 外幣轉帳之約定/常用非約定帳號
     *
     * @param cusidn
     * @return
     */
    public BaseResult getOutAcnoAndAgreeAcno(String cusidn) {
        BaseResult bs = new BaseResult();
        ADMMSGCODE admmsgcode = null;
        try {
            Locale currentLocale = LocaleContextHolder.getLocale();
            log.debug("errorMsg.locale >> {}", currentLocale);
            String locale = currentLocale.toString();
            // OutAcno
            List<String> outAcnoList = null;
            String vipNumf = null;// 外幣匯款人VIP註記
            // 取得外幣定存轉出帳號
            bs = N920_REST(cusidn, FX_OUT_DEP_ACNO);
            // 後製把table 裡面的資料轉成下拉選單所要的格式
            if (bs.getResult()) {
                outAcnoList = getAcnos(bs);
                // 轉出帳號無任何資料時,產生 Z999 錯誤訊息 ,查無交易帳號，無法進行此交易，請洽分行申請辦理約定轉出帳號
                if (outAcnoList.size() == 0) {
                    String msgCode = "Z999";
                    String message = "";
                    if (admMsgCodeDao.hasMsg(msgCode, null)) {
                        admmsgcode = admMsgCodeDao.get(ADMMSGCODE.class, msgCode);
                        switch (locale) {
                            case "en":
                                message = admmsgcode.getADMSGOUTENG();
                                break;
                            case "zh_TW":
                                message = admmsgcode.getADMSGOUT();
                                break;
                            case "zh_CN":
                                message = admmsgcode.getADMSGOUTCHS();
                                break;
                        }
                    }


                    bs.setMessage(msgCode, message);
                    bs.setResult(Boolean.FALSE);
                    return bs;
                }
                Map<String, Object> tmpData = (Map<String, Object>) bs.getData();
                vipNumf = String.valueOf(tmpData.get("VIPNUMF"));// 外幣匯款人VIP註記

                // 轉出帳號
                bs.addData("outAcnoList", outAcnoList);
                bs.addData("VIPNUMF", vipNumf);
                // 取得外幣轉帳之約定/常用非約定帳號
                // bs = getFxDepAgreeAcnoList(sessionId);
                // 後製把table 裡面的資料轉成下拉選單所要的格式
                // 轉入帳號
                List<Map<String, String>> inAcnoList = new ArrayList<Map<String, String>>();

                BaseResult n920BS = new BaseResult();
                n920BS = N920_REST(cusidn, FX_DEPOSIT_N920);
                if (n920BS.getResult()) {
                    Map<String, Object> n920BSData = (Map<String, Object>) n920BS.getData();
                    List<Map> n920rec = (List<Map>) n920BSData.get("REC");

                    log.debug("n920rec={}", n920rec);

                    BaseResult bs2 = new BaseResult();
                    bs2 = daoService.getAgreeAcnoList(cusidn, FX_DEPOSIT_AGREEACNOLIST);

                    if (bs2.getResult()) {
                        Map<String, Object> bs2Data = (Map<String, Object>) bs2.getData();
                        List<Map> bs2rec = (List<Map>) bs2Data.get("REC");

                        log.debug("bs2rec={}", bs2rec);

                        boolean useFlag = false;
                        for (int i = 0; i < bs2rec.size(); i++) {
                            String BNKCOD = (String) bs2rec.get(i).get("BNKCOD");

                            if (!"050".equals(BNKCOD) && !"".equals(BNKCOD)) {
                                continue;
                            }
                            useFlag = false;

                            String VALUE = (String) bs2rec.get(i).get("VALUE");

                            if (!"#".equals(VALUE)) {
                                Map<String, String> map = new HashMap<String, String>();
                                map = CodeUtil.fromJson(VALUE, map.getClass());

                                String ACN = map.get("ACN");
                                log.debug("ACN={}", ACN);

                                for (int j = 0; j < n920rec.size(); j++) {
                                    String n920ACN = (String) n920rec.get(j).get("ACN");
                                    log.debug("n920ACN={}", n920ACN);

                                    if (n920ACN.equals(ACN)) {
                                        useFlag = true;
                                        break;
                                    }
                                }
                            } else {
                                useFlag = true;
                            }

                            if (useFlag) {
                                inAcnoList.add(bs2rec.get(i));
                            }
                        }
                    }
                }
                log.debug("inAcnoList={}", inAcnoList);
                bs.addData("inAcnoList", inAcnoList);
            } else {
                log.error("N920_REST error return {} ", bs.getData());
                return bs;
            }
        } catch (Exception e) {
            // TODO: handle exception
            log.error("{}", e);
        }
        return bs;
    }

    /**
     * 從BaseResult取出ACN list
     * 
     * @param bs
     * @return
     */
    public List<String> getAcnos(BaseResult bs) {
        List<String> result = new ArrayList<>();

        try {
            if (bs == null) {
                throw new NullPointerException();
            }

            Map<String, Object> bsData = (Map<String, Object>) bs.getData();
            List<Map> rec = (List<Map>) bsData.get("REC");
            if (rec.size() > 0) {
                for (Map<String, String> m : rec) {
                    result.add(m.get("ACN"));
                }
            }
        } catch (Exception e) {
            log.error("getAcns error", e);
        }
        return result;
    }

    /**
     * 取得外幣定存轉出幣別
     */
    public List<Map<String, String>> getFxDepCRYList(String acno) {
        List<Map<String, String>> resultListMap = new ArrayList<Map<String, String>>();
        try {
            List<ADMCURRENCY> admCurrencyList = admCurrencyDao.getAll();
            log.trace(ESAPIUtil.vaildLog("admCurrencyList=" + admCurrencyList));

            String acno4th = acno.substring(3, 4);
            log.debug(ESAPIUtil.vaildLog("acno4th={}" + acno4th));

            for (int x = 0; x < admCurrencyList.size(); x++) {
                String ADCURRENCY = admCurrencyList.get(x).getADCURRENCY();
                log.debug(ESAPIUtil.vaildLog("ADCURRENCY=" + ADCURRENCY));

                // 外幣帳號
                if (acno4th.equals("5")) {
                    if (ADCURRENCY.equals("TWD")) {
                        continue;
                    } else {
                        Map<String, String> map = new HashMap<String, String>();
                        map.put("ADCURRENCY", ADCURRENCY);
                        map.put("ADCCYNO", admCurrencyList.get(x).getADCCYNO());
                        map.put("ADCCYNAME", admCurrencyList.get(x).getADCCYNAME());

                        resultListMap.add(map);
                    }
                } else {
                    Map<String, String> map = new HashMap<String, String>();
                    map.put("ADCURRENCY", ADCURRENCY);
                    map.put("ADCCYNO", admCurrencyList.get(x).getADCCYNO());
                    map.put("ADCCYNAME", admCurrencyList.get(x).getADCCYNAME());

                    resultListMap.add(map);
                }
            }
        } catch (Exception e) {
            // Avoid Information Exposure Through an Error Message
            // e.printStackTrace();
            log.error("String>> error >> {}", e);
        }
        log.debug(ESAPIUtil.vaildLog("resultListMap=" + resultListMap));

        return resultListMap;
    }

    /**
     * rest N530 取得查詢資料及後加工
     * 
     * @param reqParam
     * @return
     */
    public BaseResult getRestN530Data(Map<String, String> reqParam) {
        BaseResult bs = new BaseResult();
        try {
            String txid = "N177";
            bs = N177_REST(txid, reqParam);
            Map<String, ArrayList<Map<String, String>>> bsData =
                    (Map<String, ArrayList<Map<String, String>>>) bs.getData();
            ArrayList<Map<String, String>> rows =
                    (ArrayList<Map<String, String>>) bsData.get("REC");
            for (Map<String, String> row : rows) {
                // 起存日
                row.put("DPISDT",
                        DateUtil.convertDate(2, row.get("DPISDT"), "yyyMMdd", "yyy/MM/dd"));
                // 到期日
                row.put("DUEDAT",
                        DateUtil.convertDate(2, row.get("DUEDAT"), "yyyMMdd", "yyy/MM/dd"));
                // 計息方式
                row.put("INTMTH_view", I18nHelper.getINTMTH(row.get("INTMTH")));
                // 存款種類-定期存款
                row.put("DEPTYPE", i18n.getMsg("LB.Time_deposit"));
                // 自動轉期未轉次數
                row.put("AUTXFTM", acct_Service.getAUTXFTM(row.get("AUTXFTM")));

                row.put("JSONSTR", CodeUtil.toJson(row));
            }
            log.debug("getcallN530Data BaseResult getData :{}", bs.getData());
        } catch (Exception e) {
            log.error("getcallN530Data Error : ", e);
        }
        return bs;
    }

    /**
     * 取得定存到期的清單 REST N178
     * 
     * @param cudisn
     * @param reqParam
     * @return
     */
    public BaseResult getcallN530Data(String cudisn, Map<String, String> reqParam) {
        BaseResult bs = new BaseResult();
        try {
            String txid = "N178";
            reqParam.put("CUSIDN", cudisn);


            // 取得定存到期清單
            bs = N178_REST(txid, reqParam);

            Map<String, ArrayList<Map<String, String>>> getDataMap =
                    (Map<String, ArrayList<Map<String, String>>>) bs.getData();
            ArrayList<Map<String, String>> rec =
                    (ArrayList<Map<String, String>>) getDataMap.get("REC");
            for (Map<String, String> row : rec) {

                String showDpisdt = this.dateAddSlash(row.get("DPISDT"));
                String showDuedat = this.dateAddSlash(row.get("DUEDAT"));
                // 覆蓋原本的Map
                row.put("SHOWDPISDT", showDpisdt);
                row.put("SHOWDUEDAT", showDuedat);
                row.put("JSONSTR", CodeUtil.toJson(row));
                // 存單金額
                String balance = NumericUtil.fmtAmount(row.get("BALANCE"), 2);
                row.put("BALANCE", balance);
                // 自動轉期未轉次數
                String autxftm = "999".equals(row.get("AUTXFTM")) ? i18n.getMsg("LB.Unlimited")
                        : row.get("AUTXFTM");
                row.put("AUTXFTM", autxftm);
                // 計息方式
                row.put("INTMTH", I18nHelper.getINTMTH(row.get("INTMTH")));
            }
            log.debug("getcallN530Data BaseResult getData :{}", bs.getData());
        } catch (Exception e) {
            log.error("getcallN530Data Error : ", e);
        }
        return bs;
    }

    /**
     * 轉期次數i18n
     * 
     * @param FGAUTXFTM 是否轉期
     * @param AUTXFTM 轉期次數
     * @return
     */
    public String getDisplayAUTXFTM(String FGAUTXFTM, String AUTXFTM) {
        String result = AUTXFTM;
        // 轉期次數i18n
        if ("Y".equals(FGAUTXFTM)) {
            if ("99".equals(AUTXFTM)) {
                // 無限次
                result = i18n.getMsg("LB.Unlimited_1") + " " + i18n.getMsg("LB.Time_s");
            } else {
                result = AUTXFTM + " " + i18n.getMsg("LB.Time_s");
            }
        } else {
            // 取消自動轉期
            result = i18n.getMsg("LB.Termination_of_the_automatic_renewal");
        }
        return result;
    }

    /**
     * 轉入外幣綜存定存
     */
    @SuppressWarnings("unchecked")
    public BaseResult f_deposit_transfer_confirm(Map<String, String> reqParam) {
        BaseResult bs = new BaseResult();
        try {
            log.debug(
                    ESAPIUtil.vaildLog("f_deposit_transfer_confirm{}" + CodeUtil.toJson(reqParam)));
            // [TRDATE]欄位請輸入系統日期，民國年YYY/MM/DD
            if (StrUtil.isNotEmpty(reqParam.get("FGTRDATE"))
                    && reqParam.get("FGTRDATE").equals("0")) {
                reqParam.put("TRDATE", DateUtil.getTWDate("/"));// TRDATE 民國年YYY/MM/DD
                reqParam.put("transfer_date", DateUtil.getDate("/"));// 顯示日期 西元年 YYYY/MM/DD
            } else if (StrUtil.isNotEmpty(reqParam.get("FGTRDATE"))
                    && reqParam.get("FGTRDATE").equals("1")) {
                reqParam.put("TRDATE", reqParam.get("CMDATE"));// TRDATE
                reqParam.put("CMTRDATE", reqParam.get("CMDATE"));// 預約單筆轉帳日期
                reqParam.put("transfer_date", reqParam.get("CMDATE"));// 顯示日期
            } else if (StrUtil.isNotEmpty(reqParam.get("FGTRDATE"))
                    && reqParam.get("FGTRDATE").equals("2")) {
                String CMSDATE = reqParam.get("CMSDATE");// 預約週期起日
                String CMEDATE = reqParam.get("CMEDATE");// 預約週期迄日
                String CMPERIOD = reqParam.get("CMPERIOD");// 預約每月轉帳日期
                // reqParam.put("transfer_date", "預約自" + CMSDATE + " ~ " + CMEDATE + "期間，固定於每月 "
                // + CMPERIOD + "日轉帳");
                reqParam.put("transfer_date", i18n.getMsg("LB.X1457") + CMSDATE + " ~ " + CMEDATE
                        + i18n.getMsg("LB.X1458") + CMPERIOD + i18n.getMsg("LB.X1459"));
            }

            // 判斷議價編號
            String BGROENO = reqParam.get("BGROENO");
            log.debug(ESAPIUtil.vaildLog("BGROENO={}" + BGROENO));
            boolean BGROENOflag = false;

            if (BGROENO == null || "".equals(BGROENO)) {
                reqParam.put("BGROENO", "");
            } else {
                BGROENOflag = true;
                reqParam.put("BGROENOflag", String.valueOf(BGROENOflag));
            }
            log.debug("BGROENOflag={}", BGROENOflag);

            // 決定是否顯示交易密碼選項
            String FGINACNO = reqParam.get("FGINACNO");
            log.debug(ESAPIUtil.vaildLog("FGINACNO={}" + FGINACNO));

            String addAcnErr = "";
            String adagreef = "0";
            boolean hiddenCMSSL = false;
            // 約定/常用帳號
            if (FGINACNO.equals("CMDAGREE")) {
                Map<String, String> map = new HashMap<String, String>();
                map = CodeUtil.fromJson(reqParam.get("INACNO1"), map.getClass());
                // AGREE=0表非約定
                hiddenCMSSL = map.get("AGREE").equals("0");
                adagreef = map.get("AGREE");

                String INACN = map.get("ACN");
                log.debug(ESAPIUtil.vaildLog("INACN={}" + INACN));
                reqParam.put("INACN", INACN);
            }
            // 非約定帳號
            else {
                hiddenCMSSL = true;

                // 加入常用帳號
                String ADDACN = reqParam.get("ADDACN");
                log.debug(ESAPIUtil.vaildLog("ADDACN={}" + ADDACN));

                if (ADDACN != null && ADDACN.equals("checked")) {
                    reqParam.put("EXECUTEFUNCTION", "INSERT");
                    reqParam.put("DPUSERID", reqParam.get("cusidn"));
                    reqParam.put("DPGONAME", "050-" + i18n.getMsg("LB.W0605"));// 臺灣企銀
                    // 2為非約定
                    reqParam.put("DPTRACNO", "2");
                    reqParam.put("DPTRIBANK", "050");
                    reqParam.put("DPTRDACNO", reqParam.get("INACNO2"));

                    N997_REST(reqParam);
                }
            }
            log.debug("hiddenCMSSL={}", hiddenCMSSL);

            if (hiddenCMSSL == true) {
                reqParam.put("FLAG", "0");
            } else {
                reqParam.put("FLAG", "1");
            }

            // get Page input value
            String intmth = reqParam.get("INTMTH");// 計息方式
            String autcftm = reqParam.get("AUTXFTM");// 轉期次數
            String code = reqParam.get("CODE");// 轉存方式
            String intmthName = "";
            String ShowfgAutxftm = "";
            String codeName = "";
            // 計息方式，0機動，1固定
            if ("0".equals(intmth)) {
                intmthName = i18n.getMsg("LB.Floating");// 0:機動
            } else if ("1".equals(intmth)) {
                intmthName = i18n.getMsg("LB.Fixed");// 1:固定
            }
            //
            if ("99".equals(autcftm)) {
                ShowfgAutxftm = I18n.getMessage("LB.Rollover")
                        + i18n.getMsg("LB.Unlimited_number_of_times");// 轉期無限次數
            } else {
                ShowfgAutxftm =
                        I18n.getMessage("LB.Rollover") + autcftm + I18n.getMessage("LB.Time_s");// "轉期"+
                                                                                                // +"次"
            }
            // 轉存方式
            // A:本金利息一併轉期;
            // B:本金轉期－利息按月轉入活存;
            // E : 本金轉期－利息於定存到期或解約轉入活存
            // 不轉期 :
            // C 利息按月轉入活存，轉入帳號
            // D 利息於定存到期或解約轉入活存，轉入帳號
            switch (code) {
                case "A":
                    codeName = i18n.getMsg("LB.Principal_and_Interest_rollover1");// "本金利息一併轉期";
                    break;
                case "B":
                    codeName =
                            i18n.getMsg("LB.Principal_rollover_interest_monthly_to_demand_deposit");// "本金轉期－利息按月轉入活存";
                    break;
                case "E":
                    codeName = i18n.getMsg("LB.Principal_rollover") + "－" + i18n
                            .getMsg("LB.Interest_to_demand_deposit_at_maturity_or_termination");// "本金轉期－利息於定存到期或解約轉入活存";
                    break;
                // 不轉期 :
                case "C":
                    codeName = i18n.getMsg("LB.Interest_monthly_to_demand_deposit");// "利息按月轉入活存";
                    ShowfgAutxftm = i18n.getMsg("LB.No_rollover");// "不轉期";
                    break;
                case "D":
                    codeName =
                            i18n.getMsg("LB.Interest_to_demand_deposit_at_maturity_or_termination");// "利息於定存到期或解約轉入活存";
                    ShowfgAutxftm = i18n.getMsg("LB.No_rollover");// "不轉期";
                    break;
                default:
                    break;
            }
            reqParam.put("INTMTHNAME", intmthName);
            reqParam.put("CODENAME", ShowfgAutxftm);
            reqParam.put("AUTXFTMNAME", codeName);

            // IKEY要使用的JSON:DC
            String jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam), "UTF-8");
            log.debug(ESAPIUtil.vaildLog("jsondc={}" + jsondc));
            reqParam.put("jsondc", jsondc);

            bs.setData(reqParam);
            bs.setResult(true);
            log.trace(ESAPIUtil.vaildLog("bs getData {}" + CodeUtil.toJson(bs.getData())));
        } catch (Exception e) {
            // Avoid Information Exposure Through an Error Message
            // e.printStackTrace();
            log.error("f_deposit_transfer_confirm error >> {}", e);
        }
        return bs;
    }

    /**
     * 轉入外幣定存
     * 
     * @param reqParam
     * @return
     */
    public BaseResult f_deposit_transfer_result(Map<String, String> reqParam) {
        BaseResult bs = new BaseResult();
        try {

            log.debug("f_deposit_transfer_result init");

            // 處理AOP 所需的欄位
            // 轉入帳號
            String fginacno = reqParam.get("FGINACNO"); // 1 約定帳號, 2 非約定帳號
            String trin_bnkcod = "";
            String trin_acn = "";
            String trin_agree = "";
            if ("CMDAGREE".equals(fginacno)) {
                Map<String, String> jm = CodeUtil.fromJson(reqParam.get("INACNO1"), Map.class);
                log.debug(ESAPIUtil.vaildLog("jm {}" + CodeUtil.toJson(jm)));
                trin_bnkcod = jm.get("BNKCOD");
                trin_acn = jm.get("ACN");
                trin_agree = jm.get("AGREE");
            } else if ("CMDISAGREE".equals(fginacno)) { // 非約定, 只能是台企銀 050
                trin_bnkcod = "050";
                trin_acn = reqParam.get("INACNO2");
                trin_agree = "0"; // 0: 非約定, 1: 約定
            }
            // 預約
            String fgtrdate = reqParam.get("FGTRDATE");
            String ADREQTYPE = "";
            if ("0".equals(fgtrdate)) {

            } else {
                // 預約
                ADREQTYPE = "S";
            }

            log.debug(ESAPIUtil.vaildLog("trin_acn >>> {}" + trin_acn));

            reqParam.put("ADOPID", "N174");
            reqParam.put("ADSVBH", trin_bnkcod);// 轉入服務銀行
            reqParam.put("ADREQTYPE", ADREQTYPE);// 預約>>S ，其他就給空字串
            reqParam.put("ADTXAMT", reqParam.get("AMOUNT") + "." + reqParam.get("AMOUNT_DIG"));// 轉出金額
            reqParam.put("ADCURRENCY", reqParam.get("OUTCRY_TEXT").substring(0, 3));// 轉出幣別
            reqParam.put("ADAGREEF", trin_agree);// 轉入帳號約定或非約定註記>>0:非約定，1:約定
            log.debug(ESAPIUtil
                    .vaildLog("f_deposit_transfer_result reqParam {}" + CodeUtil.toJson(reqParam)));
            // 處理AOP 所需的欄位 END
            // Call REST
            bs = N174_REST(reqParam);
            // 後製
            Map<String, String> getDataMap = (Map<String, String>) bs.getData();
            String showDpisdt = this.dateAddSlash(getDataMap.get("SDT"));
            String showDuedat = this.dateAddSlash(getDataMap.get("DUEDAT"));
            getDataMap.put("SDT", showDpisdt);
            getDataMap.put("DUEDAT", showDuedat);
            getDataMap.put("TOTBAL", moveDecimal(getDataMap.get("TOTBAL")));// 轉帳帳號帳戶餘額
            getDataMap.put("AVLBAL", moveDecimal(getDataMap.get("AVLBAL")));// 轉帳帳號可用餘額
            String FGTXDATE = reqParam.get("FGTRDATE");
            getDataMap.put("FGTXDATE", FGTXDATE);
            String MsgName;
            if ("0".equals(FGTXDATE)) {
                MsgName = i18n.getMsg("LB.Transfer_successful");// 轉帳成功
            } else {
                MsgName = i18n.getMsg("LB.X1838");// 預約轉帳成功
            }
            getDataMap.put("MsgName", MsgName);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            getDataMap.put("CMQTIME", sdf.format(new Date()));
            getDataMap.put("FYTSFAN", reqParam.get("FYTSFAN"));
            getDataMap.put("OUTCRY_TEXT", reqParam.get("OUTCRY_TEXT"));
            getDataMap.put("TYPCOD_TEXT", reqParam.get("TYPCOD_TEXT"));
            getDataMap.put("INTMTHNAME", reqParam.get("INTMTHNAME"));
            getDataMap.put("AUTXFTMNAME", reqParam.get("AUTXFTMNAME"));
            getDataMap.put("CODENAME", reqParam.get("CODENAME"));
            getDataMap.put("CMTRMEMO", reqParam.get("CMTRMEMO"));
            getDataMap.put("TRDATE", reqParam.get("TRDATE"));
            getDataMap.put("AMOUNT", reqParam.get("AMOUNT"));
            getDataMap.put("AMOUNT_DIG", reqParam.get("AMOUNT_DIG"));



            log.trace("call_N174 bs.getData : {} ", bs.getData());

        } catch (Exception e) {
            // Avoid Information Exposure Through an Error Message
            // e.printStackTrace();
            log.error("f_deposit_transfer_result error >> {}", e);
        }
        return bs;
    }

    /**
     * 外匯綜存定存解約取得第一頁所需的資料
     * 
     * @param sessionId
     * @param pwd
     * @see CmUserQry
     * @see WsN531Out
     * @return
     */
    public BaseResult f_deposit_cancel(String cusidn, Map<String, String> reqParam) {
        BaseResult bs = new BaseResult();
        try {
            log.debug("f_deposit_cancel in cusidn>>>> {} ", cusidn);
            // 得到解約清單
            bs = N531_REST(cusidn);
            if (bs.getResult()) {
                // return data 資料加工
                Map<String, ArrayList<Map<String, String>>> bsData =
                        (Map<String, ArrayList<Map<String, String>>>) bs.getData();
                ArrayList<Map<String, String>> rec =
                        (ArrayList<Map<String, String>>) bsData.get("REC");
                ArrayList<Map<String, String>> returnMapList = new ArrayList<>();

                Iterator<Map<String, String>> iterator = rec.iterator();
                while (iterator.hasNext()) {
                    Map<String, String> row = iterator.next(); // must be called before you can call

                    if (null != reqParam.get("FDPNO")) {
                        if (row.get("FDPNO").equals(reqParam.get("FDPNO"))
                                && row.get("ACN").equals(reqParam.get("ACN530"))) {
                            bs.addData("defaultCheck", "TRUE");
                        } else {
                            iterator.remove();
                            continue;
                        }
                    }

                    // 存單金額
                    String amt = row.get("BALANCE");
                    // 存單金額格式化
                    String show_amt = "";
                    // 解決格式化的問題
                    try {
                        show_amt = NumericUtil.fmtAmount(amt, 2);
                    } catch (IllegalArgumentException e) {
                        log.error("~~~f_deposit_cancel~~Error  IllegalArgumentException: ", e);
                    }

                    // 依條件移除資料
                    if ("".equals(show_amt) || "0.00".equals(show_amt)) {
                        iterator.remove();
                    } else {
                        row.put("AMT", amt);
                        row.put("SHOW_AMT", show_amt);
                        // 新增顯示原本的Map
                        String showDpisdt = this.dateAddSlash(row.get("DPISDT"));
                        String showDuedat = this.dateAddSlash(row.get("DUEDAT"));
                        String intmth = row.get("INTMTH");
                        String intmthName = "";
                        // N174.INTMTH.0=機動
                        // N174.INTMTH.1=固定
                        // N174.INTMTH.2=固定
                        // 計息方式，0機動，1固定
                        if ("0".equals(intmth)) {
                            intmthName = i18n.getMsg("LB.Floating");// 0:機動
                        } else {
                            intmthName = i18n.getMsg("LB.Fixed");// 1:固定
                        }
                        row.put("SHOW_DPISDT", showDpisdt);
                        row.put("SHOW_DUEDAT", showDuedat);
                        row.put("SHOW_INTMTH", intmthName);
                        // 存單號碼
                        row.put("FDPNUM", row.get("FDPNO"));
                        row.remove("FDPNO");
                        row.put("JSON", CodeUtil.toJson(row));
                    }
                }
                bs.addData("COUNT", String.valueOf(rec.size()));

                log.debug("f_deposit_cancel out >>>> {} ", bs.getData());
            }
        } catch (Exception e) {
            log.error("~~~f_deposit_cancel~~Error : ", e);
        }
        return bs;
    }

    /**
     * 外幣綜存定存解約取得確認頁所需的資料
     * 
     * @param reqParam
     * @return
     */
    public BaseResult f_deposit_cancel_confirm(String cusidn, Map<String, String> reqParam) {
        log.debug("f_deposit_cancel_confirm Service start !!!~~~");
        BaseResult bs = new BaseResult();
        try {
            log.trace("cusidn >> {}", cusidn);
            log.trace(ESAPIUtil.vaildLog("reqParam >> {}" + CodeUtil.toJson(reqParam)));

            // 取出頁面上該筆帳號儲存的hidden FGSELECT，內容是json字串
            // 將json字串內部分key replace成新的，新key與rest 回傳的bs key相同
            Map<String, String> json = this.StrJsonConvertMap(reqParam.get("FGSELECT"));
            json.put("TXID", "N175_1");

            // 加入CUSIDN
            json.put("CUSIDN", cusidn);
            json.put("ADOPID", "N175C");
            log.trace(ESAPIUtil.vaildLog("FGSELECT >> {}" + CodeUtil.toJson(json)));

            bs = N175_REST(WSN175BEFOROUT, json);
            log.trace("N175_1_back >> {}", bs.getData());

            // 將頁面值放入json字串
            json.put("CMTRMEMO", reqParam.get("CMTRMEMO")); // 交易備註
            json.put("CMTRMAIL", reqParam.get("CMTRMAIL")); // 另通知email
            json.put("CMMAILMEMO", reqParam.get("CMMAILMEMO")); // 摘要
            json.put("FGTRDATE", reqParam.get("FGTRDATE")); // 即時或預約

            if (bs.getResult()) {
                // 將輸入頁內容加入bs內，及bs回傳值格式化
                bs = f_deposit_cancel_confirm_PostProcessing(bs, json);
            }
            log.trace("f_deposit_cancel_confirm_PostProcessing result >> {}", bs.getData());
        } catch (Exception e) {
            log.error("f_deposit_cancel_confirm error : ", e);
        }
        return bs;
    }

    /**
     * 將輸入頁內容加入bs內，及bs回傳值格式化
     * 
     * @param bs
     * @param json
     * @return
     */
    public BaseResult f_deposit_cancel_confirm_PostProcessing(BaseResult bs,
            Map<String, String> json) {
        // 顯示在頁面的資料result
        BaseResult result = bs;
        try {
            Map<String, String> bsData = (Map<String, String>) bs.getData();
            log.debug("電文 bs >> {}", bs);
            log.debug(ESAPIUtil.vaildLog("頁面 json >> {}" + CodeUtil.toJson(json)));
            String showDpisdt = DateUtil.addSlash2(bsData.get("DPISDT"));
            String showDuedat = DateUtil.addSlash2(bsData.get("DUEDAT"));
            result.addData("SHOW_DPISDT", showDpisdt); // 起存日
            result.addData("SHOW_DUEDAT", showDuedat); // 到期日

            // 轉帳日期
            String showTrDate = dateAddSlash(bsData.get("TRNDATE"));
            result.addData("SHOW_TRDATE", showTrDate);

            result.addData("ACN", json.get("ACN")); // 帳號
            result.addData("CMTRMEMO", json.get("CMTRMEMO")); // 交易備註
            result.addData("CMTRMAIL", json.get("CMTRMAIL")); // Email信箱
            result.addData("CMMAILMEMO", json.get("CMMAILMEMO")); // Email摘要內容
            String fgtrDate = json.get("FGTRDATE");
            result.addData("FGTRDATE", fgtrDate); // 即時/預約記號
            result.addData("TXID", "N175_2");

            // 存單金額 電文 10000.00
            result.addData("SHOW_AMT", NumericUtil.fmtAmount(bsData.get("AMT"), 2));

            // 稅後本息、利息、健保費後兩位為小數點後數字
            // 稅後本息 電文 PAIAFTX=000000001042312
            result.addData("SHOW_PAIAFTX", moveDecimal(bsData.get("PAIAFTX")));

            // 利息 電文 INT=000000000042312
            result.addData("SHOW_INT", moveDecimal(bsData.get("INT")));

            // 健保費 電文 NHITAX=000000000000000
            result.addData("SHOW_NHITAX", moveDecimal(bsData.get("NHITAX")));

            // 所得稅 電文 TAX=0，不確定有值的格式，先加.00
            result.addData("SHOW_TAX", moveDecimal(bsData.get("TAX")));

            // 計息方式
            result.addData("SHOW_INTMTH", I18nHelper.getINTMTH(bsData.get("INTMTH")));

            json.put("AMT", bsData.get("AMT"));
            json.put("DPISDT", bsData.get("DPISDT"));
            json.put("DUEDAT", bsData.get("DUEDAT"));
            json.put("PAIAFTX", bsData.get("PAIAFTX"));
            json.put("NHITAX", bsData.get("NHITAX"));
            json.put("TAX", bsData.get("TAX"));
            json.put("INT", bsData.get("INT"));
            json.put("FGTRDATE", fgtrDate);
            json.put("TXID", "N175_2");
            // 儲存要帶到結果頁的資料
            result.addData("JSONSTR", CodeUtil.toJson(json));
            log.trace(ESAPIUtil.vaildLog("post json > {}" + CodeUtil.toJson(json)));
            log.trace(ESAPIUtil.vaildLog("post result bs >> {}" + CodeUtil.toJson(result)));
        } catch (Exception e) {
            log.error("f_deposit_cancel_confirm_PostProcessing", e);
        }
        return result;
    }

    /**
     * N175外幣綜存定存解約，將輸入頁資料帶到確認頁，未發電文
     * 
     * @param reqParam
     * @return
     */
    public BaseResult f_deposit_cancel_confirm_booking(Map<String, String> reqParam) {
        log.trace(ESAPIUtil.vaildLog("reqParam >> {}" + CodeUtil.toJson(reqParam)));
        BaseResult bs = new BaseResult();
        Map<String, String> bsData = new HashMap<>();
        try {
            // 從輸入頁帶來的未格式化的原始資料
            Map<String, String> json = this.StrJsonConvertMap(reqParam.get("FGSELECT"));

            // 交易備註
            String cmtrMemo = reqParam.get("CMTRMEMO");
            // 另通知mail
            String cmtrMail = reqParam.get("CMTRMAIL");
            // 摘要內容
            String cmmailMemo = reqParam.get("CMMAILMEMO");
            // 轉帳日期，取預約欄位
            String cmtrDate = reqParam.get("CMTRDATE");

            // 即時/預約flag
            json.put("FGTRDATE", reqParam.get("FGTRDATE"));
            json.put("CMTRMEMO", cmtrMemo);
            json.put("CMTRMAIL", cmtrMail);
            json.put("CMMAILMEMO", cmmailMemo);
            json.put("CMTRDATE", cmtrDate);

            // 西元轉民國
            String trDate = DateUtil.convertDate(1, cmtrDate, "yyyy/MM/dd", "yyy/MM/dd");
            json.put("SHOW_TRDATE", trDate);
            bsData.put("SHOW_TRDATE", trDate);
            // 帳號
            bsData.put("ACN", json.get("ACN"));
            // 存單號碼
            bsData.put("FDPNUM", json.get("FDPNUM"));
            // 幣別
            bsData.put("CUID", json.get("CUID"));
            // 存單金額
            bsData.put("SHOW_AMT", json.get("SHOW_AMT"));
            // 計息方式
            bsData.put("SHOW_INTMTH", json.get("SHOW_INTMTH"));
            // 起存日
            bsData.put("SHOW_DPISDT", json.get("SHOW_DPISDT"));
            // 到期日
            bsData.put("SHOW_DUEDAT", json.get("SHOW_DUEDAT"));
            bsData.put("CMTRMEMO", cmtrMemo);
            bsData.put("CMTRMAIL", cmtrMail);
            bsData.put("CMMAILMEMO", cmmailMemo);

            // 儲存要帶到結果頁的資料
            bsData.put("JSONSTR", CodeUtil.toJson(json));
            log.trace(ESAPIUtil.vaildLog("FGSELECT >> {}" + CodeUtil.toJson(json)));

            // 顯示在頁面的資料bsData
            CodeUtil.convert2BaseResult(bs, bsData, "0", i18n.getMsg("LB.X1839"));// 取得預約確認頁資料成功
            log.trace("bs >> {}", CodeUtil.toJson(bs));
        } catch (Exception e) {
            log.error("f_deposit_cancel_confirm_booking error", e);
        }
        return bs;
    }

    /**
     * 
     * N175 外匯綜存定存解約
     * 
     * @param sessionid
     * @param pwd
     * @param reqParam
     * @return
     */
    public BaseResult f_deposit_cancel_result(String dpMyEmail, Map<String, String> reqParam) {
        BaseResult bs = new BaseResult();
        try {
            log.trace(ESAPIUtil.vaildLog("dpMyEmail >> {}" + dpMyEmail));
            log.trace(ESAPIUtil.vaildLog("pageReqParam >> {}" + CodeUtil.toJson(reqParam)));
            // OtpKey
            String getOtpKey = getMockOtpKey();
            // Necessary Mock data
            // newReqMap.put("OTPKEY", getOtpKey); // OTP動態密碼

            // Get Page JsonString Value creation newReqMap
            Map<String, String> json = this.StrJsonConvertMap(reqParam.get("JSONSTR"));
            log.trace(ESAPIUtil.vaildLog("JSONSTR >> {}" + CodeUtil.toJson(json)));

            // 從Session取出我的信箱
            if (dpMyEmail != null) {
                reqParam.put("DPMYEMAIL", dpMyEmail);
            }

            reqParam.putAll(json);
            log.trace(ESAPIUtil.vaildLog("REST reqParam >> {}" + CodeUtil.toJson(reqParam)));

            String fgtrDate = json.get("FGTRDATE");
            reqParam.put("ADOPID", "N175");
            // reqParam.put("ADSVBH", "");// 轉入服務銀行
            // reqParam.put("ADAGREEF","1");// 轉入帳號約定或非約定註記>>0:非約定，1:約定

            if ("0".equals(fgtrDate)) {
                // 即時
                bs = N175_REST(WSN175ACTION, reqParam);
                bs = onlinePostProcessing(bs, reqParam);
            } else if ("1".equals(fgtrDate)) {
                reqParam.put("ADREQTYPE", "S");// 預約>>S，其他就給空字串
                // 預約
                bs = N175_REST(WSN175ACTIONS, reqParam);
                bs = bookingPostProcessing(bs, reqParam);
            }

        } catch (Exception e) {
            log.error("deposit_cancel_result Error : ", e);
        }
        return bs;
    }

    /**
     * N175外匯綜存定存解約 即時交易後處理
     * 
     * @param bs
     * @param reqParam
     * @return
     */
    public BaseResult onlinePostProcessing(BaseResult bs, Map<String, String> reqParam) {
        log.debug("電文 bs data >> {}", bs.getData());
        BaseResult resultBs = bs;
        // 顯示在頁面的資料格式化
        Map<String, String> bsData = (Map<String, String>) bs.getData();
        String showDpisdt = DateUtil.addSlash2(bsData.get("DPISDT"));
        String showDuedat = DateUtil.addSlash2(bsData.get("DUEDAT"));
        bsData.put("SHOWDPISDT", showDpisdt); // 起存日
        bsData.put("SHOWDUEDAT", showDuedat); // 到期日
        bsData.put("FGTRDATE", reqParam.get("FGTRDATE")); // 即時或預約
        bsData.put("CMTRMEMO", reqParam.get("CMTRMEMO")); // 交易備註

        String intmth = I18nHelper.getINTMTH(bsData.get("INTMTH"));
        bsData.put("INTMTH", intmth); // 計息方式
        bsData.put("ACN", reqParam.get("ACN")); // 帳號
        bsData.put("MsgName", i18n.getMsg("LB.Terminate_successful")); // 解約成功

        // 存單金額 電文 AMT=1000.00
        bsData.put("AMT", NumericUtil.fmtAmount(bsData.get("AMT"), 2));

        // 稅後本息、利息、健保費後兩位為小數點後數字
        // 稅後本息 電文 PAIAFTX=000000000104279
        bsData.put("PAIAFTX", moveDecimal(bsData.get("PAIAFTX")));
        // 利息 電文 INT=000000000004279
        bsData.put("INT", moveDecimal(bsData.get("INT")));
        // 健保費 電文 NHITAX=000000000000000
        bsData.put("NHITAX", moveDecimal(bsData.get("NHITAX")));

        // 所得稅 電文 TAX=0，不確定有值的格式，先加.00
        bsData.put("TAX", moveDecimal(bsData.get("TAX")));

        log.debug("onlinePostProcessing bs data >> {} ", bs.getData());
        return resultBs;
    }

    /**
     * N175外匯綜存定存解約 預約交易後處理
     * 
     * @param bs
     * @param reqParam
     * @return
     */
    public BaseResult bookingPostProcessing(BaseResult bs, Map<String, String> reqParam) {
        log.debug("電文 bs data >> {} ", bs.getData());
        BaseResult resultBs = bs;
        // return data 資料加工
        Map<String, String> bsData = (Map<String, String>) bs.getData();
        // 顯示在頁面的資料格式化
        String showDpisdt = DateUtil.addSlash2(bsData.get("DPISDT"));
        String showDuedat = DateUtil.addSlash2(bsData.get("DUEDAT"));
        bsData.put("SHOW_TRDATE", reqParam.get("SHOW_TRDATE")); // 轉帳日期
        bsData.put("SHOWDPISDT", showDpisdt); // 起存日
        bsData.put("SHOWDUEDAT", showDuedat); // 到期日
        bsData.put("FGTRDATE", reqParam.get("FGTRDATE")); // 即時或預約
        bsData.put("CMTRMEMO", reqParam.get("CMTRMEMO")); // 交易備註
        String intmth = I18nHelper.getINTMTH(bsData.get("INTMTH"));
        bsData.put("INTMTH", intmth); // 計息方式
        bsData.put("ACN", reqParam.get("ACN")); // 帳號
        bsData.put("AMT", NumericUtil.fmtAmount(bsData.get("AMT"), 2)); // 存單金額
        bsData.put("MsgName", i18n.getMsg("LB.W0284")); // 預約成功 TODO i18n

        log.debug("bookingPostProcessing bs data >> {} ", bs.getData());
        return resultBs;
    }

    /**
     * N177 取得外幣定存到期轉入帳號
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult f_renewal_apply_step1(String cusidn, Map<String, String> reqParam) {
        BaseResult resultBs = new BaseResult();
        Map<String, Object> resultMap = new HashMap<>();

        try {
            log.debug(ESAPIUtil.vaildLog(
                    "f_renewal_apply_step1 start parameter: {}" + CodeUtil.toJson(reqParam)));
            // get radio button value
            String fgselectJson = reqParam.get("FGSELECT");
            if (StrUtil.isNotEmpty(reqParam.get("FGSELECT530"))) {
                fgselectJson = reqParam.get("FGSELECT530");
            }
            // JsonString TO Map
            // {FGSELECT={"ACN":"04152000095","DEPTYPE":"定期存款","FDPNO":"0000005","CUID":"USD","BALANCE":"10000.00","ITR":"0.290","INTMTH":"固定","DPISDT":"1071211","DUEDAT":"1071218","TSFACN":"","ILAZLFTM":"000","AUTXFTM":"無限次數","SHOWDPISDT":"107/12/11","SHOWDUEDAT":"107/12/18"}}
            Map<String, String> fgselectMap = this.StrJsonConvertMap(fgselectJson);
            // 存單金額格式化
            resultMap.put("display_BALANCE", moveDecimal(fgselectMap.get("BALANCE")));
            resultMap.put("fgselectJson", fgselectJson);
            resultMap.put("fgselectMap", fgselectMap);

            // 若存單帳號之科目為51時,取用"n921歸戶帳號"; 否則取用"存單帳號"
            String fyAcn = fgselectMap.get("ACN");
            boolean isSubject51 = false;
            if (fyAcn.substring(3, 5).equals("51")) {
                isSubject51 = true;
            }

            if (isSubject51) {
                // 取得利息轉入帳號清單
                resultBs = getAcnosList(cusidn, fyAcn);
                // 讓頁面知道存單帳號之科目
                resultBs.addData("isSubject51", isSubject51);

                Map bsData = (Map) resultBs.getData();
                // 加入輸入頁帶來的資料
                bsData.putAll(resultMap);
            } else {
                resultMap.put("ACN", fyAcn);
                resultMap.put("TEXT", fyAcn);
                // 讓頁面知道存單帳號之科目
                resultMap.put("isSubject51", isSubject51);
                CodeUtil.convert2BaseResult(resultBs, resultMap, "0", i18n.getMsg("LB.X1805"));// 查詢成功
            }
            log.debug("f_renewal_apply_step1 result >> {}", resultBs.getData());
        } catch (Exception e) {
            log.error("f_renewal_apply_step1 Error", e);
        }
        return resultBs;
    }

    /**
     * 從JSON字串中取出指定的值
     * 
     * @param get 取值的key
     * @param json
     * @return
     */
    public String getFromJson(String get, String json) {
        String result = "";
        Map<String, String> map = CodeUtil.fromJson(json, Map.class);
        if (map != null && map.containsKey(get)) {
            result = map.get(get);
        } else {
            log.warn("JSON字串中查無指定資料 >> {}", get);
            log.warn("JSON字串 >> {}", json);
        }
        return result;
    }

    /**
     * 
     * N177 取得 外匯定存自動轉期申請/變更輸入頁的值
     * 
     * @param sessionId
     * @param reqParam
     * @return
     */
    public BaseResult f_renewal_apply_confirm(Map<String, String> reqParam) {
        BaseResult bs = new BaseResult();
        try {
            log.debug(ESAPIUtil.vaildLog(
                    "getRenewalApplyStep1Data reqParamInput : {} " + CodeUtil.toJson(reqParam)));
            // get Page input value
            // 轉期次數
            String autxftm = reqParam.containsKey("AUTXFTM") ? reqParam.get("AUTXFTM") : "";
            String ShowfgAutxftm = reqParam.get("AUTXFTM"); // 頁面顯示_轉期次數i18n
            String code = reqParam.get("CODE"); // 轉存方式
            String fytsfAn = ""; // 利息轉入帳號
            String fgAutxftm = ""; // 轉期次數註記
            StringBuffer codeName = new StringBuffer();

            // 轉存方式
            // A:本金利息一併轉期;
            // B:本金轉期－利息按月轉入活存，轉入帳號;
            // E : 本金轉期－利息於定存到期或解約轉入活存，轉入帳號
            // 不轉期 :
            // C 利息按月轉入活存，轉入帳號
            // D 利息於定存到期或解約轉入活存，轉入帳號
            switch (code) {
                case "A":
                    // 本金利息一併轉期
                    codeName.append(i18n.getMsg("LB.Principal_and_Interest_rollover1"));
                    fgAutxftm = "Y";
                    break;
                case "B":
                    // 本金轉期－利息按月轉入活存，轉入帳號
                    codeName.append(i18n.getMsg("LB.Principal_rollover"));
                    codeName.append("－");
                    codeName.append(i18n
                            .getMsg("LB.Upon_maturity_or_terminating_credit_Interest_to_Account"));
                    codeName.append(" ");
                    fytsfAn = reqParam.get("FYTSFAN1");// 利息轉入帳號
                    codeName.append(fytsfAn);
                    fgAutxftm = "Y";
                    break;
                case "E":
                    // 本金轉期－利息於定存到期或解約轉入活存，轉入帳號
                    codeName.append(i18n.getMsg(
                            "LB.Interest_to_demand_deposit_at_maturity_or_termination_input"));
                    codeName.append(" ");
                    fytsfAn = reqParam.get("FYTSFAN2");// 利息轉入帳號
                    codeName.append(fytsfAn);
                    fgAutxftm = "Y";
                    break;
                case "C":
                    // 不轉期，利息按月轉入活存，轉入帳號
                    codeName.append(i18n.getMsg("LB.No_rollover"));
                    codeName.append("，");
                    codeName.append(i18n
                            .getMsg("LB.Upon_maturity_or_terminating_credit_Interest_to_Account"));
                    codeName.append(" ");
                    fytsfAn = reqParam.get("FYTSFAN3");// 利息轉入帳號
                    codeName.append(fytsfAn);
                    fgAutxftm = "N";
                    break;
                case "D":
                    // 不轉期，利息於定存到期或解約轉入活存，轉入帳號
                    codeName.append(i18n.getMsg("LB.No_rollover"));
                    codeName.append("，");
                    codeName.append(i18n.getMsg(
                            "LB.Interest_to_demand_deposit_at_maturity_or_termination_input"));
                    codeName.append(" ");
                    fytsfAn = reqParam.get("FYTSFAN4");// 利息轉入帳號
                    codeName.append(fytsfAn);
                    fgAutxftm = "N";
                    break;
                default:
                    break;
            }
            log.debug(ESAPIUtil.vaildLog("code >> {}, codeName >> {}" + code + codeName));

            // 轉期次數i18n
            ShowfgAutxftm = getDisplayAUTXFTM(fgAutxftm, autxftm);

            // jsonString To Map
            Map<String, String> resParam = this.StrJsonConvertMap(reqParam.get("REQPARAMJSON"));
            resParam.put("AUTXFTM", autxftm);// 轉期次數
            resParam.put("SHOWAUTXFTM", ShowfgAutxftm);// 顯示轉期次數
            resParam.put("FGAUTXFTM", fgAutxftm);// 轉期次數註記
            resParam.put("FYTSFAN", fytsfAn);// 利息轉入帳號
            resParam.put("CODE", code);// 轉存方式 參數
            resParam.put("CODENAME", codeName.toString());// 顯示轉存方式
            resParam.put("display_BALANCE", NumericUtil.fmtAmount(resParam.get("BALANCE"), 2));
            resParam.put("INTMTH", ConvertintcmthToNumber(resParam.get("INTMTH")));//傳送數字
            resParam.put("INTMTH_view", I18nHelper.getINTMTH(resParam.get("INTMTH")));//顯示文字
            // resParam ={ACN=01052555562, DEPTYPE=定期存款, FDPNO=0000002, CUID=USD,
            // BALANCE=1000.00, ITR=0.290, INTMTH=固定,
            // DPISDT=1070927, DUEDAT=1071004, TSFACN=, ILAZLFTM=000, AUTXFTM=09,
            // SHOWDPISDT=107/09/27,
            // SHOWDUEDAT=107/10/04, FYTSFAN=, CODE=A, CODENAME=本金利息一併轉期}
            bs.setData(resParam);
            bs.setResult(true);
//            log.debug("f_renewal_apply_confirm bs.getData : {}", bs.getData());
        } catch (Exception e) {
            log.error("f_renewal_apply_confirmError : ", e);
        }
        return bs;
    }

    /**
     * N177 取得 外匯定存自動轉期申請/變更回應資料
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult f_renewal_apply_result(String cusidn, Map<String, String> reqParam) {
        BaseResult bs = new BaseResult();
        try {
            log.debug("f_renewal_apply_result Start");
            // OtpKey
            String getOtpKey = getMockOtpKey();
            reqParam.put("CUSIDN", cusidn);
            reqParam.put("PINNEW", reqParam.get("PINNEW")); // 交易密碼SHA1值
            reqParam.put("OTPKEY", getOtpKey); // OTP動態密碼
            // txnLog 所需的欄位
            reqParam.put("ADOPID", "N177");
            reqParam.put("ADSVBH", "050");// 轉入服務銀行
            reqParam.put("ADAGREEF", "1");// 轉入帳號約定或非約定註記>>0:非約定，1:約定//固定為 1：約定

            // call N077
            String txid = "N177_1";

            bs = N177_REST(txid, reqParam);

            Map<String, String> map = (Map<String, String>) bs.getData();
            // 轉期期數
            map.put("AUTXFTM", getDisplayAUTXFTM(reqParam.get("FGAUTXFTM"), map.get("AUTXFTM")));
            // 轉存方式
            map.put("CODE", getCode(map.get("CODE")));
            // 存單種類-定期存款
            map.put("DEPTYPE", i18n.getMsg("LB.Time_deposit"));
            // 計息方式
            map.put("INTMTH", I18nHelper.getINTMTH((map.get("INTMTH"))));
            // 幣別，因電文回應的是代號，所以使用前一頁帶來的幣別英文
            map.put("CRY", reqParam.get("CRY"));

            map.put("display_AMTFDP", NumericUtil.fmtAmount(map.get("AMTFDP"), 2));

            log.debug("getRenewalApplyResult getData >> {}", bs.getData());
        } catch (Exception e) {
            log.error("getOrderRenewalResultError", e);
        }
        return bs;
    }

    /**
     * N178 外幣定存單到期續存 取得利息轉入帳號清單、輸入頁資料
     * 
     * @param cusidn
     * @param reqParam
     * @return
     */
    public BaseResult f_order_renewal_step1(String cusidn, Map<String, String> reqParam) {
        BaseResult bs = new BaseResult();
        try {
            log.trace("cusidn : {}", cusidn);
            log.trace(ESAPIUtil.vaildLog("reqParam >> {}" + CodeUtil.toJson(reqParam)));
            // get radio button value
            String fgselectJson = reqParam.get("FGSELECT");
            // Get Page JsonString Value creation resParam
            Map<String, String> fgselectMap = StrJsonConvertMap(fgselectJson);

            // 存單帳號
            String fyAcn = fgselectMap.get("ACN");
            // 取得利息轉入帳號清單
            bs = getAcnosList(cusidn, fyAcn);

            if (bs.getResult()) {
                bs.addData("fgselectJson", fgselectJson);
                bs.addData("fgselectMap", fgselectMap);
                bs.addData("TSFACN", fgselectMap.get("TSFACN")); // 輸入頁的利息轉入帳號
            }
            log.debug("f_order_renewal_step1 baData : {} ", bs.getData());
        } catch (Exception e) {
            log.error("f_order_renewal_step1 Error", e);
        }
        return bs;
    }

    /**
     * N178確認頁，取得轉入帳號清單 取得 N920 REST、N921 getAgreeAcnoList 資料 如果 N920 ACN 與 N921 ACN 相同，再比對 N921 ACN
     * 前3碼 與 存單帳號前3碼 是否相同 通過的就是要顯示在下拉選單的帳號
     * 
     * @param cusidn
     * @param FYACN 存單帳號
     * @return
     */
    public BaseResult getAcnosList(String cusidn, String FYACN) {
        BaseResult resultBs = new BaseResult();
        LinkedList<Map> resultRec = new LinkedList<>();
        Map<String, Object> resultMap = new HashMap<>();
        try {
            // N920 REST
            BaseResult n920bs = N920_REST(cusidn, FX_DEPOSIT_N920);
            if (n920bs.getResult() == false) {
                log.error("N920 REST error. MsgCode:{} Message:{}", n920bs.getMsgCode(),
                        n920bs.getMessage());
                resultBs = n920bs;
                throw new Exception();
            }
            Map n920bsData = (Map) n920bs.getData();
            log.debug("n920 bsData >> {}", n920bsData);
            List<Map> n920Acnos = (List<Map>) n920bsData.get("REC");

            // N921 取得約定帳號
            BaseResult n921bs = daoService.getAgreeAcnoList(cusidn, FX_DEPOSIT, true);
            if (n921bs.getResult() == false) {
                log.error("N921 getAgreeAcnoList error. MsgCode:{} Message:{}", n921bs.getMsgCode(),
                        n921bs.getMessage());
                resultBs = n921bs;
                throw new Exception();
            }
            Map n921bsData = (Map) n921bs.getData();
            log.debug("n921 getAgreeAcnoList bsData >> {}", n921bsData);
            // 約定帳號存在List REC，非約定帳號存在List REC2，此方法只取REC
            List<Map> n921Rec = (List<Map>) n921bsData.get("REC");

            // 該筆帳號是否要顯示在下拉選單
            boolean b_PrintFlag = false;
            // 約定帳號筆數
            int acnos_count = n921Rec.size();
            // 存單帳號 前3碼
            String str_FYACN = FYACN.substring(0, 3);

            // 挑出要顯示的帳號
            for (int i = 0; i < acnos_count; i++) {
                Map<String, String> n921Row = n921Rec.get(i);
                if (!"050".equals(n921Row.get("BNKCOD")) && !"".equals(n921Row.get("BNKCOD"))) {
                    continue;
                }
                b_PrintFlag = false;

                String n921json = String.valueOf(n921Rec.get(i).get("VALUE"));
                String n921Acn = getFromJson("ACN", n921json);

                // 排除請選擇option
                if (!n921Row.get("VALUE").equals("#")) {
                    // 檢查 N920 ACN 與 N921 ACN 是否相同
                    for (int j = 0; j < n920Acnos.size(); j++) {
                        String n920Acn = String.valueOf(n920Acnos.get(j).get("ACN"));
                        if (n920Acn.equals(n921Acn)) {
                            log.debug("n920 Acn >> {}, n921 Acn >> {}", n920Acn, n921Acn);
                            // 檢查 N921 ACN 前3碼 與 存單帳號前3碼 是否相同
                            if (!"".equals(n921Acn) && str_FYACN.equals(n921Acn.substring(0, 3))) {
                                log.debug("顯示在下拉選單的帳號 >> {}, {}", n920Acn, n921Acn);
                                // 在下拉選單，顯示此筆帳號
                                b_PrintFlag = true;
                                break;
                            }
                        }
                    }
                } // #
                else {
                    b_PrintFlag = true;
                }

                if (b_PrintFlag) {
                    // 將JSON的ACN，另外複製一份到map，方便頁面下拉選單的value取值
                    n921Row.put("ACN", n921Acn);
                    // 產生下拉選單選項
                    resultRec.add(n921Rec.get(i));
                }
            }

            // 有約定帳號時，REC加入第一筆title，--約定帳號--
            if (resultRec.isEmpty() == false) {
                Map<String, String> firstRow = new HashMap<>();
                firstRow.put("VALUE", "#");
                firstRow.put("ACN", "");
                String designated = i18n.getMsg("LB.Designated_account");
                firstRow.put("TEXT", "--" + designated + "--");
                resultRec.addFirst(firstRow);
            }

            resultMap.put("REC", resultRec);
            CodeUtil.convert2BaseResult(resultBs, resultMap, "0", i18n.getMsg("LB.X1805"));// 查詢成功
        } catch (Exception e) {
            log.error("getAcnosList Error", e);
        }
        return resultBs;
    }

    /**
     * N178 取得 外幣定存單到期續存輸入頁的值
     * 
     * @param reqParam
     * @return
     */
    public BaseResult f_order_renewal_confirm(String cusidn, Map<String, String> reqParam) {
        BaseResult bs = new BaseResult();
        try {
            log.trace(ESAPIUtil
                    .vaildLog("f_order_renewal_confirm reqParam : {}" + CodeUtil.toJson(reqParam)));
            // Get Page JsonString Value creation resParam
            Map<String, String> resParam = StrJsonConvertMap(reqParam.get("REQPARAMJSON"));
            // 取得外匯存款清單
            Map<String, String> reqMap = new HashMap<>();
            reqMap.put("CUSIDN", cusidn); // 身份識別
            reqMap.put("FYACN", resParam.get("ACN")); // 存單帳號
            reqMap.put("FDPNUM", resParam.get("FDPNO")); // 存單號碼
            reqMap.put("AMTFDP", resParam.get("BALANCE")); // 存單金額
            reqMap.put("CRY", resParam.get("CUID")); // 幣別
            reqMap.put("TYPE1", reqParam.get("TYPE1")); // 續存方式 0: 本金續存，利息轉入帳號1: 本金及利息一併續存
            reqMap.put("FYTSFAN", reqParam.get("FYTSFAN")); // 利息轉入帳號
            reqMap.put("INTMTH", reqParam.get("INTMTH")); // 計息方式
            reqMap.put("ADOPID", "N178C");
            log.trace(ESAPIUtil.vaildLog("reqMap >> {}" + CodeUtil.toJson(reqMap)));

            String txid = "N178_1";
            // 取得確認頁資料
            bs = N178_REST(txid, reqMap);

            // 後製
            Map<String, String> bsData = (Map<String, String>) bs.getData();
            // 顯示日期格式化
            String showDpisdt = this.dateAddSlash(bsData.get("DPISDT_N"));
            String showDuedat = this.dateAddSlash(bsData.get("DUEDAT_N"));
            bsData.put("SHOW_DPISDT", showDpisdt);
            bsData.put("SHOW_DUEDAT", showDuedat);

            bsData.put("SHOW_AMTTSF", NumericUtil.fmtAmount(bsData.get("AMTTSF"), 2)); // 存單金額(續轉存金額)
            bsData.put("SHOW_INTMTH", I18nHelper.getINTMTH(bsData.get("INTMTH"))); // 計息方式
            bsData.put("SHOW_AMTFDP", NumericUtil.fmtAmount(bsData.get("AMTFDP"), 2)); // 原存單金額
            bsData.put("SHOW_INT", NumericUtil.fmtAmount(bsData.get("INT"), 2)); // 原存單利息
            bsData.put("SHOW_FYTAX", NumericUtil.fmtAmount(bsData.get("FYTAX"), 2)); // 所得稅
            bsData.put("SHOW_NHITAX", NumericUtil.fmtAmount(bsData.get("NHITAX"), 2)); // 健保費
            String term = bsData.get("TERM"); // 存款期別
            String typcod = bsData.get("TYPCOD"); // 期別種類
            bsData.put("SHOW_TERM", getTerm(term, typcod)); // 存款期別+月日

            log.debug("f_order_renewal_confirm  BaseResult getData: {} ", bs.getData());
        } catch (Exception e) {
            log.error("f_order_renewal_confirm error", e);
        }
        return bs;
    }

    /**
     * 
     * @param term 存款期別
     * @param typcod 期別種類
     * @return
     */
    public String getTerm(String term, String typcod) {
        String result = term;
        if ("W".equals(typcod)) {
            term = Integer.parseInt(term) + i18n.getMsg("LB.X2145");
        } else if ("M".equals(typcod)) {
            if (term.substring(1, 2).equals("1")) {
                if (term.substring(1).equals("12")) {
                    result = "1" + i18n.getMsg("LB.Year");
                } else {
                    result = term.substring(1) + i18n.getMsg("LB.Month");
                }
            } else {
                result = term.substring(2) + i18n.getMsg("LB.Month");
            }
        }
        return result;
    }

    /**
     * N178 取得 外匯定存單到期續存 結果頁
     * 
     * @param sessionId
     * @param cUSIDN
     * @param reqParam
     * @return
     */
    public BaseResult f_order_renewal_result(String cUSIDN, Map<String, String> reqParam) {
        BaseResult bs = new BaseResult();
        try {
            // OtpKey
            // String getOtpKey = getMockOtpKey();
            // reqParam.put("OTPKEY", getOtpKey); // OTP動態密碼

            reqParam.put("CUSIDN", cUSIDN);
            // txnLog 所需的欄位
            reqParam.put("ADOPID", "N178");
            reqParam.put("ADSVBH", "050");// 轉入服務銀行
            reqParam.put("ADAGREEF", "1");// 轉入帳號約定或非約定註記>>0:非約定，1:約定//固定為 1：約定

            String txid = "N178_2";
            bs = N178_REST(txid, reqParam);

            // 後製
            Map<String, String> bsData = (Map<String, String>) bs.getData();
            // 顯示日期格式化
            String showDpisdt = DateUtil.addSlash2(bsData.get("DPISDT_N"));
            String showDuedat = DateUtil.addSlash2(bsData.get("DUEDAT_N"));

            bsData.put("SHOWDPISDT", showDpisdt);
            bsData.put("SHOWDUEDAT", showDuedat);
            bsData.put("SHOW_INTMTH", I18nHelper.getINTMTH(bsData.get("INTMTH")));
            String term = bsData.get("TERM"); // 存款期別
            String typcod = bsData.get("TYPCOD"); // 期別種類
            bsData.put("SHOW_TERM", getTerm(term, typcod)); // 存款期別+年月

            bsData.put("MsgName", i18n.getMsg("LB.Renewal_successful")); // 續存成功

            log.debug("f_order_renewal_result >> {}", bs.getData());
        } catch (Exception e) {
            log.error("f_order_renewal_result error", e);
        }
        return bs;
    }

    /**
     * 轉換電文回應的日期格式
     * 
     * @param reqDate
     * @return
     */
    public String dateAddSlash(String reqDate) {
        // 民國年日期格式
        String MINGUOFORMAT = "yyyMMdd";
        String MINGUOFORMATSLASH = "yyy/MM/dd";
        String resDate = DateUtil.convertDate(2, reqDate, MINGUOFORMAT, MINGUOFORMATSLASH);
        return resDate;
    }

    /**
     * 檢查TXToken
     * 
     * @param reqTxToken
     * @param sessionTxToken
     * @return true 表示一樣
     */
    public boolean validateToken(String reqTxToken, String SessionFinshToken) {
        boolean falseFlag = false;
        try {
            log.trace(ESAPIUtil.vaildLog("validateToken reqTxToken : {}" + reqTxToken));
            log.trace(
                    ESAPIUtil.vaildLog("validateToken SessionFinshToken : {}" + SessionFinshToken));
            if (reqTxToken == null) {
                log.debug(" reqTxToken==null");
                falseFlag = true;
            }
            if (StrUtil.isNotEmpty(SessionFinshToken) && SessionFinshToken.equals(reqTxToken)) {
                log.debug(" pagToken SessionFinshToken &&  一樣 表示重複交易");
                falseFlag = true;
            }
        } catch (Exception e) {
            log.error("getTransferToken Error", e);
        }
        return falseFlag;
    }

    /**
     * TODO getMockOtpKey
     * 
     * @return
     */
    public String getMockOtpKey() {
        return "No Check";
    }

    /**
     * Json Convert Map<String,String>
     * 
     * @param strJson EX : {"ACN":"00162288288","TYPE":"1","FDPNUM":"0000068","AMTFDP":"15000.00"}
     * @return Map EX :: {ACN=00162288288, TYPE=1, FDPNUM=0000068, AMTFDP=15000.00}
     */
    public Map<String, String> StrJsonConvertMap(String strJson) {
        Map<String, String> map = new HashMap<>();
        try {
            log.debug(ESAPIUtil.vaildLog("strJson In {}" + strJson));
            map = CodeUtil.fromJson(strJson, Map.class);
            log.debug(ESAPIUtil.vaildLog("fromJson Out{}" + CodeUtil.toJson(map)));
        } catch (JsonSyntaxException jsonE) {
            log.error("StrJsonConvertMap Exception ", jsonE);
        }
        return map;
    }

    /**
     * 小數點往前移兩位、加千分位
     * 
     * @param num
     * @return
     */
    public String moveDecimal(String num) {
        String result = num;
        try {
            result = NumericUtil.formatNumberString(num, 2);
        } catch (Exception e) {
            log.error(ESAPIUtil.vaildLog("decimalOffset num >> {}" + num));
            log.error("decimalOffset >> {}", e);
        }
        return result;
    }



    /**
     * 計息方式i18n 轉成 數字
     * 
     * @param INTMTH
     * @return
     */
    public String ConvertintcmthToNumber(String intmth) {
        String result = intmth;
        try {
            if (i18n.getMsg("LB.Floating").equals(intmth)) {
                // 機動
                result = "0";
            } else if (i18n.getMsg("LB.Fixed").equals(intmth)) {
                // 固定
                result = "1";
            }
        } catch (Exception e) {
            log.error(ESAPIUtil.vaildLog("getINTMTH_i18n error. parameter >> {}" + intmth + e));
        }
        return result;
    }
}
