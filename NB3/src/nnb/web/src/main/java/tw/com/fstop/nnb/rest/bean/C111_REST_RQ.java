package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * 
 * 功能說明 : 定期投資約定變更
 *
 */
public class C111_REST_RQ extends BaseRestBean_FUND implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6618798027466799466L;

	private String pkcs7Sign;// IKEY

	private String jsondc;// IKEY

	private String PINNEW;// 網路銀行密碼（新）SSL 用

	private String FGTXWAY; // 交易機制 0:SSL ,1:ikey, 2:晶片金融卡

	private String NEXT;// 資料起始位置

	private String RECNO;// 讀取筆數

	private String CUSIDN;// 統一編號

	private String ALTERTYPE;// 變更種類

	public String getPkcs7Sign() {
		return pkcs7Sign;
	}

	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}

	public String getJsondc() {
		return jsondc;
	}

	public void setJsondc(String jsondc) {
		this.jsondc = jsondc;
	}

	public String getPINNEW() {
		return PINNEW;
	}

	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}

	public String getFGTXWAY() {
		return FGTXWAY;
	}

	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}

	public String getNEXT() {
		return NEXT;
	}

	public void setNEXT(String nEXT) {
		NEXT = nEXT;
	}

	public String getRECNO() {
		return RECNO;
	}

	public void setRECNO(String rECNO) {
		RECNO = rECNO;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getALTERTYPE() {
		return ALTERTYPE;
	}

	public void setALTERTYPE(String aLTERTYPE) {
		ALTERTYPE = aLTERTYPE;
	}

	
}
