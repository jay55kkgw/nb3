package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

public class F283_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8776496183033347291L;
	
	@SerializedName(value = "TRANTITLE")
	private String TRANTITLE;

	@SerializedName(value = "TRANNAME")
	private String TRANNAME;
	
	@SerializedName(value = "FXSCHNO")
	private String FXSCHNO;

	@SerializedName(value = "FXPERMTDATE")
	private String FXPERMTDATE;

	@SerializedName(value = "FXFDATE")
	private String FXFDATE;

	@SerializedName(value = "FXTDATE")
	private String FXTDATE;

	@SerializedName(value = "FXNEXTDATE")
	private String FXNEXTDATE;

	@SerializedName(value = "FXWDAC")
	private String FXWDAC;

	@SerializedName(value = "FXWDCURR")
	private String FXWDCURR;

	@SerializedName(value = "FXWDAMT")
	private String FXWDAMT;

	@SerializedName(value = "FXSVBH")
	private String FXSVBH;

	@SerializedName(value = "FXSVAC")
	private String FXSVAC;
	
	@SerializedName(value = "FXSVCURR")
	private String FXSVCURR;

	@SerializedName(value = "FXSVAMT")
	private String FXSVAMT;
	
	@SerializedName(value = "TXTYPE")
	private String TXTYPE;

	@SerializedName(value = "FXTXMEMO")
	private String FXTXMEMO;
	
	@Override
	public Map<String, String> coverMap(){
		Map<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", this.TRANTITLE);
		result.put("交易類型", this.TRANNAME);
		result.put("預約編號", this.FXSCHNO);
		if(this.FXPERMTDATE != null && this.FXPERMTDATE.equals("S")) {
			result.put("週期", "特定日期");
		}else {
			result.put("週期", "固定每月" + this.FXPERMTDATE + "日");
		}
		if(this.FXTDATE == null || this.FXFDATE.isEmpty() || this.FXFDATE.equals("")) {
			result.put("生效日-截止日", this.FXFDATE);
		}else {
			result.put("生效日-截止日", this.FXFDATE + "-" + this.FXTDATE);
		}
		result.put("下次轉帳日", this.FXNEXTDATE);
		result.put("轉出帳號", this.FXWDAC);
		result.put("轉出金額", this.FXWDCURR + " " + this.FXWDAMT);
		result.put("銀行名稱/轉入帳號", this.FXSVBH + " " + this.FXSVAC);
		result.put("轉入金額", this.FXSVCURR + " " + this.FXSVAMT);
		result.put("交易類別", this.TXTYPE);
		result.put("備註", this.FXTXMEMO);
		return result;
	}

	public String getTRANTITLE() {
		return TRANTITLE;
	}

	public void setTRANTITLE(String tRANTITLE) {
		TRANTITLE = tRANTITLE;
	}

	public String getTRANNAME() {
		return TRANNAME;
	}

	public void setTRANNAME(String tRANNAME) {
		TRANNAME = tRANNAME;
	}

	public String getFXSCHNO() {
		return FXSCHNO;
	}

	public void setFXSCHNO(String fXSCHNO) {
		FXSCHNO = fXSCHNO;
	}

	public String getFXPERMTDATE() {
		return FXPERMTDATE;
	}

	public void setFXPERMTDATE(String fXPERMTDATE) {
		FXPERMTDATE = fXPERMTDATE;
	}

	public String getFXFDATE() {
		return FXFDATE;
	}

	public void setFXFDATE(String fXFDATE) {
		FXFDATE = fXFDATE;
	}

	public String getFXTDATE() {
		return FXTDATE;
	}

	public void setFXTDATE(String fXTDATE) {
		FXTDATE = fXTDATE;
	}

	public String getFXNEXTDATE() {
		return FXNEXTDATE;
	}

	public void setFXNEXTDATE(String fXNEXTDATE) {
		FXNEXTDATE = fXNEXTDATE;
	}

	public String getFXWDAC() {
		return FXWDAC;
	}

	public void setFXWDAC(String fXWDAC) {
		FXWDAC = fXWDAC;
	}

	public String getFXWDCURR() {
		return FXWDCURR;
	}

	public void setFXWDCURR(String fXWDCURR) {
		FXWDCURR = fXWDCURR;
	}

	public String getFXWDAMT() {
		return FXWDAMT;
	}

	public void setFXWDAMT(String fXWDAMT) {
		FXWDAMT = fXWDAMT;
	}

	public String getFXSVBH() {
		return FXSVBH;
	}

	public void setFXSVBH(String fXSVBH) {
		FXSVBH = fXSVBH;
	}

	public String getFXSVAC() {
		return FXSVAC;
	}

	public void setFXSVAC(String fXSVAC) {
		FXSVAC = fXSVAC;
	}

	public String getFXSVCURR() {
		return FXSVCURR;
	}

	public void setFXSVCURR(String fXSVCURR) {
		FXSVCURR = fXSVCURR;
	}

	public String getFXSVAMT() {
		return FXSVAMT;
	}

	public void setFXSVAMT(String fXSVAMT) {
		FXSVAMT = fXSVAMT;
	}

	public String getTXTYPE() {
		return TXTYPE;
	}

	public void setTXTYPE(String tXTYPE) {
		TXTYPE = tXTYPE;
	}

	public String getFXTXMEMO() {
		return FXTXMEMO;
	}

	public void setFXTXMEMO(String fXTXMEMO) {
		FXTXMEMO = fXTXMEMO;
	}
	
}
