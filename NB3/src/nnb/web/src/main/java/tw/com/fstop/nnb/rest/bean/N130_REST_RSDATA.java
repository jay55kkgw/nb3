package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * 活期性存款交易明細查詢
 * 
 * @author Ian
 *
 */
public class N130_REST_RSDATA implements Serializable {

	private static final long serialVersionUID = -4867530238464226683L;

	private String REC_NO;// 筆數

	private String ACN;// 帳號

	private String LSTLTD;// 異動日期

	private String MEMO_C;// 摘要

	private String CODDBCR;// 借貸別（D.支出，C.收入）

	private String AMTTRN;// 交易金額

	private String BAL;// 餘額

	private String CHKNUM;// 票據號碼

	private String DATA16;// 資料內容

	private String TRNBRH;// 收付行

	private String TRNTIME;// 交易時間

	private String ENDDATE;// 結束日期

	private String STADATE;// 最後異動日期

	private String LSTIME;// 最後異動時間

	private String FILLER_X5;

	private String DPSVAMT;
	
	private String DPWDAMT;

	private String MEMO;

	public String getREC_NO() {
		return REC_NO;
	}

	public void setREC_NO(String rEC_NO) {
		REC_NO = rEC_NO;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	public String getLSTLTD() {
		return LSTLTD;
	}

	public void setLSTLTD(String lSTLTD) {
		LSTLTD = lSTLTD;
	}

	public String getMEMO_C() {
		return MEMO_C;
	}

	public void setMEMO_C(String mEMO_C) {
		MEMO_C = mEMO_C;
	}

	public String getCODDBCR() {
		return CODDBCR;
	}

	public void setCODDBCR(String cODDBCR) {
		CODDBCR = cODDBCR;
	}

	public String getAMTTRN() {
		return AMTTRN;
	}

	public void setAMTTRN(String aMTTRN) {
		AMTTRN = aMTTRN;
	}

	public String getBAL() {
		return BAL;
	}

	public void setBAL(String bAL) {
		BAL = bAL;
	}

	public String getCHKNUM() {
		return CHKNUM;
	}

	public void setCHKNUM(String cHKNUM) {
		CHKNUM = cHKNUM;
	}

	public String getDATA16() {
		return DATA16;
	}

	public void setDATA16(String dATA16) {
		DATA16 = dATA16;
	}

	public String getTRNBRH() {
		return TRNBRH;
	}

	public void setTRNBRH(String tRNBRH) {
		TRNBRH = tRNBRH;
	}

	public String getTRNTIME() {
		return TRNTIME;
	}

	public void setTRNTIME(String tRNTIME) {
		TRNTIME = tRNTIME;
	}

	public String getENDDATE() {
		return ENDDATE;
	}

	public void setENDDATE(String eNDDATE) {
		ENDDATE = eNDDATE;
	}

	public String getSTADATE() {
		return STADATE;
	}

	public void setSTADATE(String sTADATE) {
		STADATE = sTADATE;
	}

	public String getLSTIME() {
		return LSTIME;
	}

	public void setLSTIME(String lSTIME) {
		LSTIME = lSTIME;
	}

	public String getFILLER_X5() {
		return FILLER_X5;
	}

	public void setFILLER_X5(String fILLER_X5) {
		FILLER_X5 = fILLER_X5;
	}

	public String getDPSVAMT() {
		return DPSVAMT;
	}

	public void setDPSVAMT(String dPSVAMT) {
		DPSVAMT = dPSVAMT;
	}

	public String getDPWDAMT() {
		return DPWDAMT;
	}

	public void setDPWDAMT(String dPWDAMT) {
		DPWDAMT = dPWDAMT;
	}

	public String getMEMO() {
		return MEMO;
	}

	public void setMEMO(String mEMO) {
		MEMO = mEMO;
	}



}
