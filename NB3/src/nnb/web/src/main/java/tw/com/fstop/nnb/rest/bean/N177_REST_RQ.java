package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N177_REST_RQ extends BaseRestBean_FX implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5697535993530767348L;

	private String ACN;

	private String CUSIDN;

	private String UID;

	private String TXNLO;

	private String ADOPID;		// 電文代號

	private String FYACN;//存單帳號

	private String FDPNUM;//存單號碼 

	private String CRY;//幣別 

	private String ITR;

	private String AMTFDP;//存單金額 

	private String INTMTH;

	private String CODE;

	private String DATE;

	private String TIME;

	private String TSFACN;

	private String ILAZLFTM;

	private String AUTXFTM;

	private String FYTSFAN;//轉入帳號 

	private String FGAUTXFTM;

	private String NeedSHA1;

	private String PINNEW;

	private String TRNSRC = "NB";

	private String iSeqNo; // iSeqNo

	private String FGTXDATE; // 即時或預約


	private String pkcs7Sign; // IKEY

	private String jsondc; // IKEY
	//IDGATE
	private String sessionID;
	private String idgateID;
	private String txnID;
	
	public String getACN()
	{
		return ACN;
	}

	public void setACN(String aCN)
	{
		ACN = aCN;
	}

	public String getCUSIDN()
	{
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN)
	{
		CUSIDN = cUSIDN;
	}

	public String getUID()
	{
		return UID;
	}

	public void setUID(String uID)
	{
		UID = uID;
	}

	public String getTXNLO()
	{
		return TXNLO;
	}

	public void setTXNLO(String tXNLO)
	{
		TXNLO = tXNLO;
	}

	public String getADOPID()
	{
		return ADOPID;
	}

	public void setADOPID(String aDOPID)
	{
		ADOPID = aDOPID;
	}

	public String getFYACN()
	{
		return FYACN;
	}

	public void setFYACN(String fYACN)
	{
		FYACN = fYACN;
	}

	public String getFDPNUM()
	{
		return FDPNUM;
	}

	public void setFDPNUM(String fDPNUM)
	{
		FDPNUM = fDPNUM;
	}

	public String getCRY()
	{
		return CRY;
	}

	public void setCRY(String cRY)
	{
		CRY = cRY;
	}

	public String getITR()
	{
		return ITR;
	}

	public void setITR(String iTR)
	{
		ITR = iTR;
	}

	public String getAMTFDP()
	{
		return AMTFDP;
	}

	public void setAMTFDP(String aMTFDP)
	{
		AMTFDP = aMTFDP;
	}

	public String getINTMTH()
	{
		return INTMTH;
	}

	public void setINTMTH(String iNTMTH)
	{
		INTMTH = iNTMTH;
	}

	public String getCODE()
	{
		return CODE;
	}

	public void setCODE(String cODE)
	{
		CODE = cODE;
	}

	public String getDATE()
	{
		return DATE;
	}

	public void setDATE(String dATE)
	{
		DATE = dATE;
	}

	public String getTIME()
	{
		return TIME;
	}

	public void setTIME(String tIME)
	{
		TIME = tIME;
	}

	public String getTSFACN()
	{
		return TSFACN;
	}

	public void setTSFACN(String tSFACN)
	{
		TSFACN = tSFACN;
	}

	public String getILAZLFTM()
	{
		return ILAZLFTM;
	}

	public void setILAZLFTM(String iLAZLFTM)
	{
		ILAZLFTM = iLAZLFTM;
	}

	public String getAUTXFTM()
	{
		return AUTXFTM;
	}

	public void setAUTXFTM(String aUTXFTM)
	{
		AUTXFTM = aUTXFTM;
	}

	public String getFYTSFAN()
	{
		return FYTSFAN;
	}

	public void setFYTSFAN(String fYTSFAN)
	{
		FYTSFAN = fYTSFAN;
	}

	public String getFGAUTXFTM()
	{
		return FGAUTXFTM;
	}

	public void setFGAUTXFTM(String fGAUTXFTM)
	{
		FGAUTXFTM = fGAUTXFTM;
	}

	public String getNeedSHA1()
	{
		return NeedSHA1;
	}

	public void setNeedSHA1(String needSHA1)
	{
		NeedSHA1 = needSHA1;
	}

	public String getPINNEW()
	{
		return PINNEW;
	}

	public void setPINNEW(String pINNEW)
	{
		PINNEW = pINNEW;
	}

	public static long getSerialversionuid()
	{
		return serialVersionUID;
	}

	public String getTRNSRC()
	{
		return TRNSRC;
	}

	public void setTRNSRC(String tRNSRC)
	{
		TRNSRC = tRNSRC;
	}

	public String getiSeqNo()
	{
		return iSeqNo;
	}

	public void setiSeqNo(String iSeqNo)
	{
		this.iSeqNo = iSeqNo;
	}

	public String getFGTXDATE()
	{
		return FGTXDATE;
	}

	public void setFGTXDATE(String fGTXDATE)
	{
		FGTXDATE = fGTXDATE;
	}


	public String getPkcs7Sign()
	{
		return pkcs7Sign;
	}

	public void setPkcs7Sign(String pkcs7Sign)
	{
		this.pkcs7Sign = pkcs7Sign;
	}

	public String getJsondc()
	{
		return jsondc;
	}

	public void setJsondc(String jsondc)
	{
		this.jsondc = jsondc;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public String getIdgateID() {
		return idgateID;
	}

	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}

	public String getTxnID() {
		return txnID;
	}

	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}
}
