package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N104_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 104095090272475963L;
	private String UID;
	private String ACNTYPE;
	private String CUSIDN;
	
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getACNTYPE() {
		return ACNTYPE;
	}
	public void setACNTYPE(String aCNTYPE) {
		ACNTYPE = aCNTYPE;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
}
