package tw.com.fstop.nnb.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.po.TXNFUNDDATA;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

@Service
public class Fund_Alter_Service extends Base_Service
{
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private I18n i18n;

	@Autowired
	private DaoService daoService;

	/**
	 * 使用者進入後會先call C111 查詢使用者 定期投資約定變更 清單
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult regular_investment(String cusidn, Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{

			reqParam.put("CUSIDN", cusidn);
			reqParam.put("HOST", "FUND");
			reqParam.put("ALTERTYPE", "00");
			bs = C111_REST(reqParam);
			// 將call N922 轉成需回傳的值
			Map<String, String> getn922rsmap = this.getN922RSMap(cusidn);
			
			//判斷 N922 是否成功  , 失敗要導錯誤頁
			if(!"".equals(getn922rsmap.get("TOPMSG"))) {
				bs.setResult(false);
				bs.setMsgCode(getn922rsmap.get("TOPMSG"));
				bs.setMessage(getn922rsmap.get("msgName"));
				return bs;
			}
			
			// 投資屬性
			String RSKATT = getn922rsmap.get("RSKATT");
			// 隱藏姓名 call 字霸
			String hideid_NAME = getn922rsmap.get("hideid_NAME");

			// 後製把 裡面的資料轉成所要的格式
			if (bs != null && bs.getResult())
			{
				Map<String, Object> callData = (Map<String, Object>) bs.getData();
				String str_CDNO = reqParam.get("CDNO");
				String str_TRANSCODE = reqParam.get("TRANSCODE");
				boolean flag = true;
				ArrayList<Map<String, Object>> callREC = (ArrayList<Map<String, Object>>) callData.get("REC");
				if (null != str_CDNO && !"".equals(str_CDNO))
				{
					for (Map<String, Object> map : callREC)
					{
						if (str_CDNO.equals(map.get("CDNO")))
						{
							callREC.clear();
							callREC.add(map);
							flag = false;
							break;
						}

					}
					if (flag)
					{
						callREC.clear();
					}
				}

				if (callREC.size() != 0)
				{
					for (Map<String, Object> map : callREC)
					{

						// 開始處理
						String b_BreakFlag = "N";
						String b_displayFlag = "Y";
						//
						// if ((str_CREDITNO != null && !str_CREDITNO.equals(map.get("CREDITNO")))
						// || (str_TRANSCODE != null && !(str_TRANSCODE.equals(map.get("PAYTAG")))))
						// continue;
						//
						// if ((str_CREDITNO != null && str_CREDITNO.equals(map.get("CREDITNO")))
						// && (str_TRANSCODE != null && str_TRANSCODE.equals(map.get("PAYTAG"))))
						// b_BreakFlag = true;
						//

						if (!"1".equals(map.get("TYPE1")) && !"1".equals(map.get("TYPE2"))
								&& !"1".equals(map.get("TYPE3")) && !"1".equals(map.get("TYPE4"))
								&& !"1".equals(map.get("TYPE5")))
						{
							b_displayFlag = "N";
						}
						//
						map.put("b_BreakFlag", b_BreakFlag);
						map.put("b_displayFlag", b_displayFlag);
						//
						String str_Type = String.valueOf(map.get("TYPE1")) + map.get("TYPE2") + map.get("TYPE3")
								+ map.get("TYPE4") + map.get("TYPE5");
						map.put("TYPEFLAG", str_Type);
						// 信託帳號
						map.put("hide_CDNO", WebUtil.hideAccount(String.valueOf(map.get("CDNO"))));
						//
						TXNFUNDDATA txnFundData = daoService.getFundData(String.valueOf(map.get("PAYTAG")));
						// 扣款標的
						String FUNDLNAME = txnFundData == null ? "" : txnFundData.getFUNDLNAME();
						map.put("FUNDLNAME", FUNDLNAME);
						// 商品風險等級
						String RISK = txnFundData == null ? "" : txnFundData.getRISK();
						map.put("RRSK", RISK);
						// 每次定額申購金額＋<br>不定額基準申購金額
						// 幣別
						String ADCCYNAME = daoService.getCRY((String) map.get("PAYCUR")) == null ? ""
								: daoService.getCRY((String) map.get("PAYCUR")).getADCCYNAME();
						map.put("ADCCYNAME", ADCCYNAME);
						// 金額
						map.put("display_OPAYAMT", NumericUtil.formatNumberString((String) map.get("PAYAMT"), 0));
						// 暫停扣款起日 民國年轉西元年
						map.put("display_STOPBEGDAY", ("00000000".equals(map.get("STOPBEGDAY"))) ? ""
								: DateUtil.formatDate((String) map.get("STOPBEGDAY")));
						// 暫停扣款迄日 民國年轉西元年
						map.put("display_STOPENDDAY", ("00000000".equals(map.get("STOPENDDAY"))) ? ""
								: DateUtil.formatDate((String) map.get("STOPENDDAY")));
						// 類別
						String MIP = (String) map.get("MIP");
						// 扣款方式
						String DAMT = (String) map.get("DAMT");
						String str_MIP = "";
						String str_DAMT = "";
						if ("Y".equals(MIP))
						{
							// 定期<br>不定<br>額
							str_MIP = "LB.X1840";
						}
						else
						{
							// 定期<br>定額
							str_MIP = "LB.X1841";
						}
						if ("2".equals(DAMT))
						{
							// 信用卡
							str_DAMT = "LB.Credit_Card";
						}
						else
						{
							// 存款
							str_DAMT = "LB.D1393";
						}
						//// 類別
						map.put("str_MIP", str_MIP);
						map.put("str_DAMT", str_DAMT);
						// 扣款日
						if (DAMT.equals("2"))
						{
							map.put("OPAYDAY1", ("00".equals(map.get("PAYDAY1")) ? "" : map.get("PAYDAY1")));
							map.put("OPAYDAY2", ("00".equals(map.get("PAYDAY2")) ? "" : map.get("PAYDAY2")));
							map.put("OPAYDAY3", ("00".equals(map.get("PAYDAY3")) ? "" : map.get("PAYDAY3")));
							map.put("OPAYDAY4", ("00".equals(map.get("PAYDAY4")) ? "" : map.get("PAYDAY4")));
							map.put("OPAYDAY5", ("00".equals(map.get("PAYDAY5")) ? "" : map.get("PAYDAY5")));
							map.put("OPAYDAY6", ("00".equals(map.get("PAYDAY6")) ? "" : map.get("PAYDAY6")));
						}
						else
						{
							map.put("OPAYDAY1", ("00".equals(map.get("PAYDAY1")) ? "" : map.get("PAYDAY1")));
							map.put("OPAYDAY2", ("00".equals(map.get("PAYDAY2")) ? "" : map.get("PAYDAY2")));
							map.put("OPAYDAY3", ("00".equals(map.get("PAYDAY3")) ? "" : map.get("PAYDAY3")));
							map.put("OPAYDAY4", ("00".equals(map.get("PAYDAY4")) ? "" : map.get("PAYDAY4")));
							map.put("OPAYDAY5", ("00".equals(map.get("PAYDAY5")) ? "" : map.get("PAYDAY5")));
							map.put("OPAYDAY6", ("00".equals(map.get("PAYDAY6")) ? "" : map.get("PAYDAY6")));
							map.put("OPAYDAY7", ("00".equals(map.get("PAYDAY7")) ? "" : map.get("PAYDAY7")));
							map.put("OPAYDAY8", ("00".equals(map.get("PAYDAY8")) ? "" : map.get("PAYDAY8")));
							map.put("OPAYDAY9", ("00".equals(map.get("PAYDAY9")) ? "" : map.get("PAYDAY9")));
						}

						// 投資屬性
						map.put("RSKATT", RSKATT);
						// 處理END

						// 將map轉成json
						map.put("helperjson", CodeUtil.toJson(map));
					}
					// 隱藏ID
					bs.addData("hideid_CUSIDN", WebUtil.hideID(String.valueOf(callData.get("CUSIDN"))));
					// 隱藏姓名 call 字霸
					bs.addData("hideid_NAME", hideid_NAME);
					// 筆數
					bs.addData("COUNT", String.valueOf(callREC.size()));

					log.debug("regular_investment bs Data out>>>>>>>>>> {}", bs.getData());

				}
				else
				{

					// 隱藏ID
					bs.addData("hideid_CUSIDN", WebUtil.hideID(String.valueOf(callData.get("CUSIDN"))));
					// 隱藏姓名 call 字霸
					bs.addData("hideid_NAME", hideid_NAME);
					// 筆數
					bs.addData("COUNT", String.valueOf(callREC.size()));

					log.debug("regular_investment bs Data out>>>>>>>>>> {}", bs.getData());
				}

			}
		}
		catch (Exception e)
		{
			log.error("regular_investment error >>>{}", e);
		}
		return bs;
	}

	/**
	 * 
	 * 清單點選確定後跳往下一頁前製
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult regular_investment_step1(Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{

			log.debug(ESAPIUtil.vaildLog("regular_investment_step1 Service in>>>>{}"+CodeUtil.toJson(reqParam)));
			//
			String cusidn = reqParam.get("CUSIDN");
			String hideid_CUSIDN = reqParam.get("hideid_CUSIDN");
			String cusName = reqParam.get("CUSNAME");
			// 將隱藏欄位轉成Map
			Map<String, String> newMap = CodeUtil.fromJson(reqParam.get("helperjson"), Map.class);
			// 後製
			String str_TypeFlag = newMap.get("TYPEFLAG");

			String str_Type1 = str_TypeFlag.substring(0, 1); // 扣款金額

			String str_Type2 = str_TypeFlag.substring(1, 2); // 扣款日期

			String str_Type3 = str_TypeFlag.substring(2, 3); // 終止扣款

			String str_Type4 = str_TypeFlag.substring(3, 4); // 暫停扣款

			String str_Type5 = str_TypeFlag.substring(4); // 恢復扣款
			// 原扣款狀態
			String str_Before = "";
			if (!"1".equals(str_Type5))
			{
				// 恢復扣款
				str_Before = "LB.W1162";
			}
			else if (!"1".equals(str_Type4))
			{
				// 暫停扣款
				str_Before = "LB.W1163";
			}
			else if (!"1".equals(str_Type3))
			{
				// 終止扣款
				str_Before = "LB.W1164";
			}
			//// 扣款方式
			String MIP = newMap.get("MIP");
			String str_MIP = "";
			String str_OPAYAMT = "";
			String str_NPAYAMT = "";
			if ("Y".equals(MIP))
			{
				// 定期不定額
				str_MIP = "LB.W1087";
				// 原不定額基準申購金額
				str_OPAYAMT = "LB.X1842";
				// 變更後不定額基準申購金額
				str_NPAYAMT = "LB.X1843";
			}
			else
			{
				// 定期定額
				str_MIP = "LB.W1086";
				// 原每次定額申購金額
				str_OPAYAMT = "LB.X1844";
				// 變更後每次定額申購金額
				str_NPAYAMT = "LB.X1845";
			}

			newMap.put("str_TypeFlag", str_TypeFlag);
			newMap.put("str_MIP", str_MIP);// 扣款方式
			newMap.put("str_OPAYAMT", str_OPAYAMT);// 扣款方式
			newMap.put("str_NPAYAMT", str_NPAYAMT);// 扣款方式
			newMap.put("str_Type1", str_Type1);// 原扣款狀態
			newMap.put("str_Type2", str_Type2);// 原扣款狀態
			newMap.put("str_Type3", str_Type3);// 原扣款狀態
			newMap.put("str_Type4", str_Type4);// 原扣款狀態
			newMap.put("str_Type5", str_Type5);// 原扣款狀態
			newMap.put("str_Before", str_Before);// 原扣款狀態
			newMap.put("CUSIDN", cusidn);
			newMap.put("hideid_CUSIDN", hideid_CUSIDN);
			newMap.put("cusName", cusName);
			bs.setData(newMap);
			bs.setResult(true);
			log.debug(ESAPIUtil.vaildLog("regular_investment_step1 Service out>>>>{}"+ CodeUtil.toJson(bs.getData())));
		}
		catch (Exception e)
		{
			log.error("regular_investment_step1 Service error >>>{}", e);
		}
		return bs;
	}

	/**
	 * 輸入頁輸入完後點選確定
	 * 
	 * @param transfer_data
	 * @param reqParam
	 * @return
	 */
	public BaseResult regular_investment_confirm(Map<String, String> transfer_data, Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			log.debug(ESAPIUtil.vaildLog("regular_investment_confirm transfer_data>>>>{}"+ CodeUtil.toJson(transfer_data)));
			log.debug(ESAPIUtil.vaildLog("regular_investment_confirm in reqParam>>>>{}"+ CodeUtil.toJson(reqParam)));
			// 變更後扣款狀態
			String str_ALTERTYPE = reqParam.get("ALTERTYPE") != null ? reqParam.get("ALTERTYPE") : "";
			String str_ALTERTYPE1 = reqParam.get("ALTERTYPE1") != null ? reqParam.get("ALTERTYPE1") : "";
			String str_ALTERTYPE2 = reqParam.get("ALTERTYPE2") != null ? reqParam.get("ALTERTYPE2") : "";
			String str_ALTERTYPE3 = reqParam.get("ALTERTYPE3") != null ? reqParam.get("ALTERTYPE3") : "";

			reqParam.put("ALTERTYPE", str_ALTERTYPE);
			reqParam.put("ALTERTYPE1", str_ALTERTYPE1);
			reqParam.put("ALTERTYPE2", str_ALTERTYPE2);
			reqParam.put("ALTERTYPE3", str_ALTERTYPE3);

			reqParam.put("str_ALTERTYPE", str_ALTERTYPE);
			reqParam.put("str_ALTERTYPE1", str_ALTERTYPE1);
			reqParam.put("str_ALTERTYPE2", str_ALTERTYPE2);
			reqParam.put("str_ALTERTYPE3", str_ALTERTYPE3);
			//
			// 變更後扣款狀態
			String str_After = "";
			String str_STOPBEGDAY = "";
			String str_STOPENDDAY = "";
			String str_RESTOREDAY = "";
			String str_SystemDate = DateUtil.getTWDate("/");
			// 若有變更扣款狀態
			if (!str_ALTERTYPE3.equals(""))
			{
				// 恢復扣款
				if (str_ALTERTYPE.equals("B2"))
				{
					str_After = "LB.W1162";
					str_STOPBEGDAY = "";
					str_STOPENDDAY = "";
					str_RESTOREDAY = str_SystemDate;
				}
				// 暫停扣款
				else if (str_ALTERTYPE.equals("B1"))
				{
					str_After = "LB.W1163";
					str_STOPBEGDAY = str_SystemDate;
					str_STOPENDDAY = "9991231";
					str_RESTOREDAY = "";
				}
				// 終止扣款
				else if (str_ALTERTYPE.equals("B0"))
				{
					str_After = "LB.W1164";
					str_STOPBEGDAY = str_SystemDate;
					str_STOPENDDAY = "99999999";
					str_RESTOREDAY = "0";
				}
			}
			reqParam.put("str_After", str_After);
			reqParam.put("str_STOPBEGDAY", str_STOPBEGDAY);
			reqParam.put("str_STOPENDDAY", str_STOPENDDAY);
			reqParam.put("str_RESTOREDAY", str_RESTOREDAY);

			// 變更後日期
			reqParam.put("PAYDAY1", removeundefined(reqParam.get("PAYDAY1")));
			reqParam.put("PAYDAY2", removeundefined(reqParam.get("PAYDAY2")));
			reqParam.put("PAYDAY3", removeundefined(reqParam.get("PAYDAY3")));
			reqParam.put("PAYDAY4", removeundefined(reqParam.get("PAYDAY4")));
			reqParam.put("PAYDAY5", removeundefined(reqParam.get("PAYDAY5")));
			reqParam.put("PAYDAY6", removeundefined(reqParam.get("PAYDAY6")));
			reqParam.put("PAYDAY7", removeundefined(reqParam.get("PAYDAY7")));
			reqParam.put("PAYDAY8", removeundefined(reqParam.get("PAYDAY8")));
			reqParam.put("PAYDAY9", removeundefined(reqParam.get("PAYDAY9")));

			// 金額
			reqParam.put("display_PAYAMT", NumericUtil.formatNumberString(reqParam.get("PAYAMT"), 0));
			// 將 輸入取代交易的變數
			log.debug(" regular_investment_confirm reqParam out {}", reqParam);
			transfer_data.putAll(reqParam);
			bs.addAllData(transfer_data);
			bs.setResult(true);
		}
		catch (Exception e)
		{
			log.error("regular_investment_confirm error >>>{}", e);
		}
		log.debug("regular_investment_confirmg getData >>>>>> {}", bs.getData());
		return bs;
	}

	/**
	 * 確認頁輸入完後點選確定
	 * 
	 * @param transfer_data
	 * @param reqParam
	 * @return
	 */
	public BaseResult regular_investment_result(Map<String, String> transfer_data, Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			log.debug("regular_investment_result transfer_data in >>>> {}", transfer_data);
			transfer_data.putAll(reqParam);
			bs = C112_REST(transfer_data);
			// 後製把 裡面的資料轉成所要的格式
			if (bs != null && bs.getResult())
			{
				transfer_data.remove("PINNEW");
				Map<String, String> callData = (Map<String, String>) bs.getData();
				// 每次定額申購金額＋<br>不定額基準申購金額
				// 幣別
				String ADCCYNAME = daoService.getCRY(callData.get("PAYCUR")) == null ? ""
						: daoService.getCRY(callData.get("PAYCUR")).getADCCYNAME();
				transfer_data.put("ADCCYNAME", ADCCYNAME);
				// 金額
				transfer_data.put("display_PAYAMT", NumericUtil.formatNumberString(callData.get("PAYAMT"), 0));
				// 信託帳號
				transfer_data.put("hide_CDNO", WebUtil.hideAccount(callData.get("CDNO")));
				// 變更後日期
				callData.put("PAYDAY1", !"00".equals(callData.get("PAYDAY1")) ? callData.get("PAYDAY1") : "");
				callData.put("PAYDAY2", !"00".equals(callData.get("PAYDAY2")) ? callData.get("PAYDAY2") : "");
				callData.put("PAYDAY3", !"00".equals(callData.get("PAYDAY3")) ? callData.get("PAYDAY3") : "");
				callData.put("PAYDAY4", !"00".equals(callData.get("PAYDAY4")) ? callData.get("PAYDAY4") : "");
				callData.put("PAYDAY5", !"00".equals(callData.get("PAYDAY5")) ? callData.get("PAYDAY5") : "");
				callData.put("PAYDAY6", !"00".equals(callData.get("PAYDAY6")) ? callData.get("PAYDAY6") : "");
				callData.put("PAYDAY7", !"00".equals(callData.get("PAYDAY7")) ? callData.get("PAYDAY7") : "");
				callData.put("PAYDAY8", !"00".equals(callData.get("PAYDAY8")) ? callData.get("PAYDAY8") : "");
				callData.put("PAYDAY9", !"00".equals(callData.get("PAYDAY9")) ? callData.get("PAYDAY9") : "");
				callData.putAll(transfer_data);

			}
		}
		catch (Exception e)
		{
			log.error("regular_investment_result error >>>{}", e);
		}

		log.debug("regular_investment_result >>>>>>>>>>>>>>> {}", bs.getData());
		return bs;
	}

	/**
	 * C116
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult stop_loss_notice(String cusidn, Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{

			// 將call N922 轉成需回傳的值
			Map<String, String> getn922rsmap = this.getN922RSMap(cusidn);
			
			//判斷 N922 是否成功  , 失敗要導錯誤頁
			if(!"".equals(getn922rsmap.get("TOPMSG"))) {
				bs.setResult(false);
				bs.setMessage(getn922rsmap.get("msgName"));
				return bs;
			}
			
			
			// 投資屬性
			String RSKATT = getn922rsmap.get("RSKATT");
			// 隱藏姓名 call 字霸
			String hideid_NAME = getn922rsmap.get("hideid_NAME");
			//
			reqParam.put("CUSIDN", cusidn);
			bs = C116_REST(reqParam);
			// 後製把 裡面的資料轉成所要的格式
			if (bs != null && bs.getResult())
			{
				Map<String, Object> callData = (Map<String, Object>) bs.getData();
				ArrayList<Map<String, String>> callREC = (ArrayList<Map<String, String>>) callData.get("REC");
				for (Map<String, String> map : callREC)
				{
					map.put("hideid_CDNO", WebUtil.hideAccount(String.valueOf(map.get("CDNO"))));
					// 每次定額申購金額
					map.put("display_PAYCUR", daoService.getCRY(map.get("PAYCUR")).getADCCYNAME());
					map.put("display_PAYAMT", NumericUtil.formatNumberString(map.get("PAYAMT"), 0));
					// 不定額基準申購金額
					map.put("display_FUNDCUR", daoService.getCRY(map.get("PAYCUR")).getADCCYNAME());
					map.put("display_FUNDAMT", NumericUtil.formatNumberString(map.get("FUNDAMT"), 2));
					// 扣款標的
					map.put("FUNDLNAME", daoService.getFundData(map.get("PAYTAG")) == null ? ""
							: daoService.getFundData(map.get("PAYTAG")).getFUNDLNAME());
					// 停利點
					map.put("display_STOPPROF", "000".equals(map.get("STOPPROF")) ? ""
							: Integer.parseInt(map.get("STOPPROF")) + "%");
					// 停損點
					map.put("display_STOPLOSS", "000".equals(map.get("STOPLOSS")) ? ""
							: "-" + Integer.parseInt(map.get("STOPLOSS")) + "%");
					// 類別
					// AC202.1=單筆
					// AC202.2=定期定額
					String str_AC202 = AC202Convert(map.get("AC202"));
					String MIP = map.get("MIP");
					if (MIP.equals("Y"))
					{
						// str_AC202 = "定期不定額";
						str_AC202 = i18n.getMsg("LB.W1087");
					}
					map.put("str_AC202", str_AC202);
					// 將map轉成json
					map.put("helperjson", CodeUtil.toJson(map));
				} // for END
					// 用整理後的筆數COUNT
				bs.addData("COUNT", String.valueOf(callREC.size()));
				// 隱藏ID
				bs.addData("hideid_CUSIDN", WebUtil.hideID(String.valueOf(callData.get("CUSIDN"))));
				// 隱藏姓名 call 字霸
				bs.addData("hideid_NAME", hideid_NAME);
			}
		}
		catch (Exception e)
		{
			log.error("stop_loss_notice error >>>{}", e);
		}
		return bs;
	}

	/**
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult stop_loss_notice_step1(Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			String cusidn = reqParam.get("CUSIDN");
			String hideid_CUSIDN = reqParam.get("hideid_CUSIDN");
			String hideid_NAME = reqParam.get("hideid_NAME");
			String helperjson = reqParam.get("helperjson");
			reqParam = CodeUtil.fromJson(helperjson, Map.class);
			// 後製
			String str_AC202 = reqParam.get("str_AC202");
			String show_PAYAMT = "";
			String MIP = (String) reqParam.get("MIP");
			// 頁面顯示表頭
			if (MIP.equals("Y"))
			{
				// str_AC202 = "定期不定額";
				str_AC202 = i18n.getMsg("LB.W1087");
			}
			if (str_AC202.length() == 2)
			{
				// show_PAYAMT = "定期定額每次申購金額";
				show_PAYAMT = i18n.getMsg("LB.X1846");

			}
			else if (str_AC202.length() == 4)
			{
				// show_PAYAMT = "每次定額申購金額";
				show_PAYAMT = i18n.getMsg("LB.W1043");
			}
			else if (str_AC202.length() == 5)
			{
				// show_PAYAMT = "不定額基準申購金額";
				show_PAYAMT = i18n.getMsg("LB.W1044");
			}

			reqParam.put("show_PAYAMT", show_PAYAMT);
			reqParam.put("CUSIDN", cusidn);
			reqParam.put("hideid_CUSIDN", hideid_CUSIDN);
			reqParam.put("hideid_NAME", hideid_NAME);
			bs.setData(reqParam);
			bs.setResult(true);
		}
		catch (Exception e)
		{
			log.error("stop_loss_notice_step1 error >>>{}", e);
		}
		return bs;
	}

	/**
	 * 
	 * @param transfer_data
	 * @param reqParam
	 * @return
	 */
	public BaseResult stop_loss_notice_confirm(Map<String, String> transfer_data, Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			reqParam.putAll(transfer_data);
			bs.setData(reqParam);
			bs.setResult(true);
		}
		catch (Exception e)
		{
			log.error("stop_loss_notice_confirm error >>>{}", e);
		}
		return bs;
	}

	/**
	 * 
	 * @param transfer_data
	 * @param reqParam
	 * @return
	 */
	public BaseResult stop_loss_notice_result(Map<String, String> transfer_data, Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			//
			reqParam.putAll(transfer_data);
			bs = C117_REST(reqParam);
			// 後製把 裡面的資料轉成所要的格式
			if (bs != null && bs.getResult())
			{
				Map<String, Object> callData = (Map<String, Object>) bs.getData();
				bs.addData("PAYAMT1", NumericUtil.formatNumberString(String.valueOf(callData.get("PAYAMT1")), 0));
				bs.addAllData(transfer_data);
			}
			log.debug("f_outward_remittances_result bs data {}", bs.getData());
		}
		catch (Exception e)
		{
			log.error("stop_loss_notice_result error >>>{}", e);
		}
		return bs;
	}

	/**
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult smart_fund_step1(String cusidn, Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			reqParam.put("CUSIDN", cusidn);
			bs = C118_REST(reqParam);

			// 將call N922 轉成需回傳的值
			Map<String, String> getn922rsmap = this.getN922RSMap(cusidn);
			
			
			//判斷 N922 是否成功  , 失敗要導錯誤頁
			if(!"".equals(getn922rsmap.get("TOPMSG"))) {
				bs.setResult(false);
				bs.setMessage(getn922rsmap.get("msgName"));
				return bs;
			}
			
			
			// 投資屬性
			String RSKATT = getn922rsmap.get("RSKATT");
			// 隱藏姓名 call 字霸
			String hideid_NAME = getn922rsmap.get("hideid_NAME");

			// 後製把 裡面的資料轉成所要的格式
			if (bs != null && bs.getResult())
			{
				Map<String, Object> callData = (Map<String, Object>) bs.getData();
				ArrayList<Map<String, String>> callREC = (ArrayList<Map<String, String>>) callData.get("REC");
				for (Map<String, String> map : callREC)
				{
					// 信託帳號
					map.put("hideid_CDNO", WebUtil.hideAccount(map.get("CDNO")));
					// 扣款標的
					map.put("FUNDLNAME", daoService.getFundData(map.get("PAYTAG")) == null ? ""
							: daoService.getFundData(map.get("PAYTAG")).getFUNDLNAME());
					// 單位數
					map.put("display_ACUCOUNT", NumericUtil.formatNumberString(map.get("ACUCOUNT"), 4));
					// 幣別
					String ADCCYNAME = daoService.getCRY((String) map.get("PAYCUR")) == null ? ""
							: daoService.getCRY((String) map.get("PAYCUR")).getADCCYNAME();
					map.put("ADCCYNAME", ADCCYNAME);
					// 未分配<br>金額
					map.put("display_NAMT", NumericUtil.formatNumberString(map.get("NAMT"), 2));
					// 參考現值
					map.put("display_AMT", NumericUtil.formatNumberString(map.get("AMT"), 2));
					// 基準<br>報酬率
					map.put("display_RTNRATE", NumericUtil.formatNumberString(map.get("RTNRATE"), 2));
					// 參考匯率<br>參考淨值
					map.put("display_FXRATE", NumericUtil.formatNumberString(map.get("FXRATE"), 4));
					map.put("display_REFVALUE1", NumericUtil.formatNumberString(map.get("REFVALUE1"), 4));
					// 原自動贖回設定點
					map.put("display_STOPPROF", "000".equals(map.get("STOPPROF")) ? ""
							: "+" + Integer.parseInt(map.get("STOPPROF")) + "%");
					// 原自動贖回設定點
					map.put("display_STOPLOSS", "000".equals(map.get("STOPLOSS")) ? ""
							: "-" + Integer.parseInt(map.get("STOPLOSS")) + "%");
					// 類別
					// AC202.1=單筆
					// AC202.2=定期定額
					String str_AC202 = AC202Convert(map.get("AC202"));
					if (str_AC202.length() == 4)
					{
						str_AC202 = str_AC202.substring(0, 2) + str_AC202.substring(2);
					}
					String MIP = map.get("MIP");
					if (MIP.equals("Y"))
					{
						// str_AC202 = "定期不定額";
						str_AC202 = i18n.getMsg("LB.X1840");
					}
					map.put("str_AC202", str_AC202);
					// 信託金額
					if (!"0.00".equals(map.get("FUNDAMT")))
					{
						// 幣別
						map.put("ADCCYNAME", daoService.getCRY(map.get("FUNDCUR")) == null ? ""
								: daoService.getCRY(map.get("FUNDCUR")).getADCCYNAME());
						map.put("display_FUNDAMT", NumericUtil.formatNumberString(map.get("FUNDAMT"), 2));
					} // end if
						// 將map轉成json
					map.put("helperjson", CodeUtil.toJson(map));
				} // for END
					// 隱藏ID
				bs.addData("hideid_CUSIDN", WebUtil.hideID(String.valueOf(callData.get("CUSIDN"))));
				// 隱藏姓名 call 字霸
				bs.addData("hideid_NAME", hideid_NAME);
				// 用整理後的筆數COUNT
				bs.addData("COUNT", String.valueOf(callREC.size()));
			}
			log.debug("smart_fund bs get >>>>>>>>>>>>>>>{}", bs.getData());

		}
		catch (Exception e)
		{
			log.error("smart_fund error >>>{}", e);
		}
		return bs;
	}

	/**
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult smart_fund_step2(Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			log.debug(ESAPIUtil.vaildLog("smart_fund_step2 reqParam>>>{}"+CodeUtil.toJson(reqParam)));
			String cusidn = reqParam.get("CUSIDN");
			String hideid_CUSIDN = reqParam.get("hideid_CUSIDN");
			String hideid_NAME = reqParam.get("hideid_NAME");
			// 將隱藏欄位轉成Map
			reqParam = CodeUtil.fromJson(reqParam.get("helperjson"), Map.class);
			String str_AC202 = AC202Convert(reqParam.get("AC202"));
			String MIP = reqParam.get("MIP");
			if (MIP.equals("Y"))
			{
				// str_AC202 = "定期不定額";
				str_AC202 = i18n.getMsg("LB.W1087");
			}
			// 啟用不啟用開關
			String NOOPTFLAG = (("000".equals(reqParam.get("STOPPROF"))) && "000".equals(reqParam.get("STOPLOSS")))
					? "disabled"
					: "";
			reqParam.put("str_AC202", str_AC202);
			reqParam.put("NOOPTFLAG", NOOPTFLAG);
			reqParam.put("CUSIDN", cusidn);
			reqParam.put("hideid_CUSIDN", hideid_CUSIDN);
			reqParam.put("hideid_NAME", hideid_NAME);
			bs.setData(reqParam);
			bs.setResult(true);
		}
		catch (Exception e)
		{
			log.error("smart_fund_step2 error >>>{}", e);
		}

		log.debug(ESAPIUtil.vaildLog("smart_fund_step2 >>>>>>>>>>>>>>>>>{}"+CodeUtil.toJson(bs.getData())));
		return bs;
	}

	/**
	 * 
	 * @param transfer_data
	 * @param reqParam
	 * @return
	 */
	public BaseResult smart_fund_confirm(Map<String, String> transfer_data, Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			// 自動贖回設定
			String display_on = i18n.getMsg("LB.D0324");
			String display_off = i18n.getMsg("LB.D0995");
			reqParam.put("display_NOOPT", ("".equals(reqParam.get("NOOPT"))) ? display_on : display_off);
			transfer_data.putAll(reqParam);
			bs.setData(transfer_data);
			bs.setResult(true);
		}
		catch (Exception e)
		{
			log.error("smart_fund_confirm error >>>{}", e);
		}
		log.debug(ESAPIUtil.vaildLog("smart_fund_confirm getData>>>>>>>>>{}"+CodeUtil.toJson(bs.getData())));
		return bs;
	}

	/**
	 * 
	 * @param transfer_data
	 * @param reqParam
	 * @return
	 */
	public BaseResult smart_fund_result(String cusidn, String dpmyemail, Map<String, String> transfer_data,
			Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			//
			reqParam.putAll(transfer_data);
			log.debug("transfer_data >>>> {}", transfer_data);
			bs = C119_REST(reqParam);
			// 後製把 裡面的資料轉成所要的格式
			if (bs != null && bs.getResult())
			{

				Map<String, Object> callData = (Map<String, Object>) bs.getData();
				callData.putAll(transfer_data);

				//
				Map queryData = new HashMap<>();
				queryData.put("TABLE", "TxnUser");
				queryData.put("DPUSERID", cusidn);
				queryData.put("COLUMN", "DPNOTIFY");
				queryData.put("EXECUTEFUNCTION", "query");
				queryData.put("ADOPID", "N998");
				Map<String, String> queryresult = daoService.txnUserModify(queryData);
				String dpnotify = queryresult.get("DPNOTIFY");
				boolean setNotify = false;
				boolean setMyemail = false;
				// 未開啟基金自動贖回通知服務，請點選按鈕
				if (daoService.chkContains("21", dpnotify))
				{
					setNotify = true;
				}
				// 未設定我的EMAIL，請點選按鈕
				if (dpmyemail.length() > 0 && dpmyemail.indexOf("@") > -1)
				{
					setMyemail = true;
				}
				callData.put("setNotify", setNotify);
				callData.put("setMyemail", setMyemail);

			}
		}
		catch (Exception e)
		{
			log.error("smart_fund_result error >>>{}", e);
		}
		return bs;
	}

	/**
	 * 取得N922RS 轉成Map
	 * 
	 * @param cusidn
	 * @return Map<String, String>
	 */
	public Map<String, String> getN922RSMap(String cusidn)
	{

		log.debug("getN922RSMap cusidn >>> {} ", cusidn);
		BaseResult bsN922 = N922_REST(cusidn);
		Map<String, Object> callN922Data = (Map<String, Object>) bsN922.getData();
		// 建立回傳
		Map<String, String> returnMap = new HashMap<String, String>();
		
		// 20210312更新  如果bsN922 出現錯誤 回傳出去
		if(!"".equals(callN922Data.get("TOPMSG"))) {
			returnMap.put("TOPMSG", (String) callN922Data.get("TOPMSG"));
			returnMap.put("msgName", (String) callN922Data.get("msgName"));
			return returnMap;
		}
		// 在方法外判斷電文是否成功執行
		returnMap.put("TOPMSG", (String) callN922Data.get("TOPMSG"));
		
		// 投資屬性
		String RSKATT = String.valueOf(callN922Data.get("FDINVTYPE"));
		// 原住民姓名CUSNAME
		String CUSNAME = String.valueOf(callN922Data.get("CUSNAME"));
		// 姓名
		String NAME = String.valueOf(callN922Data.get("NAME"));
		// 隱藏姓名 call 字霸
		String hideid_NAME = StrUtil.isNotEmpty(CUSNAME) ? WebUtil.hideusername1Convert(CUSNAME)
				: WebUtil.hideusername1Convert(NAME);
		returnMap.put("RSKATT", RSKATT);
		returnMap.put("NAME", NAME);
		returnMap.put("CUSNAME", CUSNAME);
		returnMap.put("hideid_NAME", hideid_NAME);

		log.debug("getN922RSMap returnMap >>> {} ", returnMap);

		return returnMap;

	}

	/**
	 * 移除網頁傳來undefined
	 * 
	 * @param checkValue
	 * @return
	 */
	public String removeundefined(String checkValue)
	{
		checkValue = StrUtil.isEmpty(checkValue) ? "" : checkValue;

		if ("undefined".equals(checkValue))
		{
			checkValue = "";
		}

		return checkValue;
	}

	/**
	 * 檢查TxToken
	 * 
	 * @param pageTxToken 頁面TxToken
	 * @param SessionFinshToken
	 * @return false 表示有問題
	 */
	public BaseResult validateToken(String pageTxToken, String SessionFinshToken)
	{
		BaseResult bs = new BaseResult();
		try
		{
			if (pageTxToken == null)
			{
				log.debug(" reqTxToken==null");
				bs.setMessage("FE002", "reqTxToken空白");
				log.debug("validateToken getResult:{}", bs.getMsgCode());
				log.debug("validateToken getResult:{}", bs.getMessage());
			}
			else if (StrUtil.isNotEmpty(SessionFinshToken) && SessionFinshToken.equals(pageTxToken))
			{
				log.debug("validateToken2 getResult:{}", bs.getMsgCode());
				log.debug("validateToken2 getResult:{}", bs.getMessage());
				log.debug(" pagToken SessionFinshToken &&  一樣 重複交易");
				String repeatTransaction = i18n.getMsg("LB.X1804");
				bs.setMessage("FE002", repeatTransaction);
			}
			else
			{
				bs.setResult(true);
			}
		}
		catch (Exception e)
		{
			log.error("getTransferToken Error", e);
		}
		return bs;
	}

	// 將電文回應的數字轉換為顯示
	// AC202.1=單筆
	// AC202.2=定期定額
	public String AC202Convert(String AC202)
	{
		switch (AC202)
		{
			case "1":
				// AC202 = "單筆";
				AC202 = i18n.getMsg("LB.X1847");
				break;
			case "2":
				// AC202 = "定期定額";
				AC202 = i18n.getMsg("LB.W1086");
				break;
			default:
				break;
		}

		return AC202;

	}
}
