package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N215Q_REST_RQ extends BaseRestBean_OLS implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8964015687788928082L;
	
	private String	CUSIDN	;
	private String	UID	;
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	
	
	
	

}
