package tw.com.fstop.nnb.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import fstop.orm.po.ADMCOUNTRY;
import fstop.orm.po.QUICKLOGINMAPPING;
import fstop.orm.po.TXNFXSCHPAY;
import fstop.orm.po.TXNTRACCSET;
import fstop.orm.po.TXNTWSCHPAY;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.common.ResultCode;
import tw.com.fstop.nnb.custom.annotation.ACNSET1;
import tw.com.fstop.nnb.custom.annotation.ACNSET2;
import tw.com.fstop.nnb.rest.bean.AML_REST_RQ;
import tw.com.fstop.nnb.rest.bean.AML_REST_RS;
import tw.com.fstop.nnb.rest.bean.IdGateMappingDelete_REST_RQ;
import tw.com.fstop.nnb.rest.bean.IdGateMappingDelete_REST_RS;
import tw.com.fstop.nnb.rest.bean.IdGateMappingUpdate_REST_RQ;
import tw.com.fstop.nnb.rest.bean.IdGateMappingUpdate_REST_RS;
import tw.com.fstop.nnb.rest.bean.IdGateN915_REST_RQ;
import tw.com.fstop.nnb.rest.bean.IdGateN915_REST_RS;
import tw.com.fstop.nnb.rest.bean.NA80_REST_RQ;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmBankDao;
import tw.com.fstop.tbb.nnb.dao.AdmCountryDao;
import tw.com.fstop.tbb.nnb.dao.AdmMsgCodeDao;
import tw.com.fstop.tbb.nnb.dao.Nb3SysOpDao;
import tw.com.fstop.tbb.nnb.dao.QuickLoginMappingDao;
import tw.com.fstop.tbb.nnb.dao.TxnFxSchPayDao;
import tw.com.fstop.tbb.nnb.dao.TxnFxSchPayDataDao;
import tw.com.fstop.tbb.nnb.dao.TxnTrAccSetDao;
import tw.com.fstop.tbb.nnb.dao.TxnTwSchPayDao;
import tw.com.fstop.tbb.nnb.dao.TxnTwSchPayDataDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
import tw.com.fstop.tbb.nnb.util.StrUtils;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.ObjectConvertUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtil;

@Service
public class Online_Serving_Service extends Base_Service {
	
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private AdmBankDao admbankdao;

	@Autowired
	private AdmCountryDao admcountrydao;

	@Autowired
	private TxnTrAccSetDao txntraccsetdao;
		
	@Autowired
	private TxnTwSchPayDao txntwschpaydao;
	
	@Autowired
	private TxnFxSchPayDao txnfxschpaydao;
	
	@Autowired
	private TxnFxSchPayDataDao txnfxschpaydatadao;
	
	@Autowired
	private Nb3SysOpDao nb3sysopdao;
	
	@Autowired
	private TxnTwSchPayDataDao txntwschpaydatadao;
	
	@Autowired
	private I18n i18n;
	
	@Autowired
	AdmMsgCodeDao admMsgCodeDao;
	
	@Autowired
	QuickLoginMappingDao quickLoginMappingDao;
	
	
	@Autowired
	private String idgate_mode;

	/**
	 * 存款餘額證明_取得帳號
	 * @param cusidn
	 * @return
	 */
	@ACNSET1
	public BaseResult getACNO_List(String cusidn) {
		log.trace("getACNO_List>>{} ",cusidn);
		BaseResult bs = null;

		try {
			//  CALL WS api
			bs =new  BaseResult();
			
//			N920取出帳號清單
			bs = N920_REST(cusidn,ACNO);

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getACNO_List error >> {}",e);
		}
//		return retMap;
		return bs;
	}
	
	/**
	 * 存款餘額證明_手續費扣帳帳號
	 * @param cusidn
	 * @return
	 */
	@ACNSET2
	public BaseResult getFeeACNO_List(String cusidn) {
		log.trace("getFeeACNO_List>>{} ",cusidn);
		BaseResult bs = null;

		try {
			//  CALL WS api
			bs =new  BaseResult();
			
//			N920取出手續費帳號清單
			bs = N920_REST(cusidn, FEEACNO);

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getFeeACNO_List error >> {}",e);
		}
		return bs;
	}

	
	/**
	 * 存款餘額證明_申請帳號
	 * @param cusidn
	 * @return
	 */
	@ACNSET1
	public BaseResult getAPPLYACNO_List(String cusidn) {
		log.trace("getACNO_List>>{} ",cusidn);
		BaseResult bs = null;

		try {
			//  CALL WS api
			bs = new BaseResult();
			
//			N920取出帳號清單
			bs = N920_REST(cusidn, APPLYACNO);

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getACNO_List error >> {}",e);
		}
//		return retMap;
		return bs;
	}
	
	
	/**
	 * 存款餘額證明確認頁
	 * @param Cusidn
	 * @return
	 */
	public BaseResult deposit_balance_proof_confirm(Map<String, String> reqParam) {

		log.trace(ESAPIUtil.vaildLog("deposit_balance_proof_confirm_service>>{}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = null;
		try {
			bs = new BaseResult();
	
			bs.setData(reqParam);
			bs.setResult(Boolean.TRUE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}

		return bs;
	}
	
	/**
	 * 存款餘額證明結果頁
	 * @param Cusidn
	 * @return
	 */
	public BaseResult deposit_balance_proof_result(String cusidn,Map<String, String> reqParam)	{		
		
		log.trace(ESAPIUtil.vaildLog("deposit_balance_proof_result_service>>{}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = null ;
		try {
		bs = new BaseResult();
	//	 call ws API		
		String date = reqParam.get("DATE");
		String date1 = DateUtil.convertDate(1,date,"yyyy/MM/dd","yyyMMdd");
		reqParam.put("DATE1", date1);
		bs = N1014_REST(cusidn, reqParam);
		bs.addData("DATE", date);
		bs.addData("CUSIDN", cusidn);
		bs.addData("COPY", reqParam.get("COPY"));
		bs.addData("AMT", reqParam.get("AMT"));
		bs.addData("FEAMT", reqParam.get("FEAMT"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("{}",e);
		}
	
		return  bs;
	}
	
	/**
	 * 支票存款開戶申請結果頁
	 * @param Cusidn
	 * @return
	 */
	public BaseResult bill_acn_open_result(String cusidn,Map<String, String> reqParam)	{		
		
		log.trace(ESAPIUtil.vaildLog("bill_acn_open_result_service>>{}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = null ;
		try {
		bs = new BaseResult();
	//	 call ws API
		String brhday = reqParam.get("BRHDAY");
		String date1 = DateUtil.convertDate(1,brhday,"yyyy/MM/dd","yyyMMdd");
		if(date1.length() > 7) {
			date1 = date1.substring(1, 8);
		}
		reqParam.put("BRHDAY", date1);
		bs = N1011_REST(cusidn, reqParam);
		bs.addData("BIRHDAY", brhday);
		bs.addData("CUSIDN", StrUtils.hideaccount(cusidn));
		bs.addData("dataSet", reqParam);
		ADMCOUNTRY ADMCOUNTRY = admcountrydao.getCountry(reqParam.get("COUNTRY"));
		if(StrUtils.isEmpty(ADMCOUNTRY.getADCTRYNAME())) {
			bs.addData("COUNTRYNAME", "");
			bs.addData("COUNTRYNAMEEN", "");
			bs.addData("COUNTRYNAMECN", "");
		}
		else {
			bs.addData("COUNTRYNAME", ADMCOUNTRY.getADCTRYNAME());
			bs.addData("COUNTRYNAMEEN", ADMCOUNTRY.getADCTRYENGNAME());
			bs.addData("COUNTRYNAMECN", ADMCOUNTRY.getADCTRYCHSNAME());
		}
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			log.error("{}",e);
		}
	
		return  bs;
	}
	
	
	/**
	 * N215約定轉入帳號申請
	 * 
	 * @param params
	 * @return
	 */
	public BaseResult predesignated_account_apply(String cusidn) {

		log.trace("predesignated_account_apply");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = N215A_REST(cusidn);
			Map<String, Object> bsData = (Map) bs.getData();
			ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
			int COUNT = rows.size();
			bs.addData("COUNT", COUNT);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("predesignated_account_apply error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * N215約定轉入帳號申請_s
	 * 
	 * @param params
	 * @return
	 */
	public BaseResult predesignated_account_apply_s(String cusidn,Map<String, String> reqParam) {

		log.trace(ESAPIUtil.vaildLog("predesignated_account_apply_s~~~{}"+CodeUtil.toJson(reqParam)));
		//{"DPBHNO1":"050","RowData":"00162100025;00162123882;00162222228","addCount":"","DPACGONAME1":"","INTRACN2":"00162123882","INTRACN1":"00162100025","DPACNO1":"","INTRACN3":"00162222228"}
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			List<Map<String,String>> REC = new ArrayList<Map<String,String>>();
			//選取的電文帳號
			String RowData = reqParam.get("RowData");
			log.trace(ESAPIUtil.vaildLog("RowData>>{}"+ RowData));
			String[] ACNROW = RowData.split(";");
			String ACN = "";
			log.debug("local in server >> {}",LocaleContextHolder.getLocale().toString());
			if(RowData != null && !"".equals(RowData)) {
				for(int i=0;i<ACNROW.length;i++) {
					ACN = ACNROW[i];
					log.trace(ESAPIUtil.vaildLog("ACN>>{}"+ ACN));
					TXNTRACCSET po = txntraccsetdao.findByACN(cusidn, ACN);
					if(po != null) {					
						Map<String,String> data = new HashMap<String,String>();					
							String DPGONAME = po.getDPGONAME();
							String bnkcod = po.getDPTRIBANK();		
							String locale = LocaleContextHolder.getLocale().toString();
							String bank = "";
							// 比對bank名稱
							if(locale.equals("zh_CN")) {
								bank = admbankdao.getBankChName(bnkcod);
							}
							else if(locale.equals("en")) {
								bank = admbankdao.getBankEnName(bnkcod);
							}
							else {
								bank = admbankdao.getBankName(bnkcod);
							}
							if(bank != "") {
								bnkcod = bnkcod + "-" + bank;						
							}else {
								bnkcod = "050-"+i18n.getMsg("LB.W0605");
							}
							//銀行
							data.put("DPBHNO", bnkcod);
							//轉入帳號
							data.put("DPACNO", ACN);
							//好記名稱
							data.put("DPACGONAME", DPGONAME);
							//加到REC
							REC.add(data);
							log.trace(ESAPIUtil.vaildLog("bnkcod+acn>>>{}"+CodeUtil.toJson(data)));
					}else {
						Map<String,String> data = new HashMap<String,String>();
						//銀行
						data.put("DPBHNO", "050-"+i18n.getMsg("LB.W0605"));
						//轉入帳號
						data.put("DPACNO", ACN);
						//好記名稱
						data.put("DPACGONAME", "");
						//加到REC
						REC.add(data);
						log.trace(ESAPIUtil.vaildLog("no_bnkcod+acn>>>{}"+CodeUtil.toJson(data)));
					}
				}
			}
			//是否空值
			String addDpacno = reqParam.get("DPACNO1");
			//新增欄位的筆數
			String COUNT = reqParam.get("addCount");
			log.trace(ESAPIUtil.vaildLog("COUNT>>{}"+ COUNT));
			if(addDpacno != null && !"".equals(addDpacno)) {
				//自己約定帳號
				for(int j=1;j<= Integer.valueOf(COUNT);j++) {
					Map<String,String> data = new HashMap<String,String>();
					//銀行
					String DPBHNO = reqParam.get("DPBHNO"+j);
					// 比對bank名稱
//					String bank = admbankdao.getBankName(DPBHNO);
					String locale = LocaleContextHolder.getLocale().toString();
					String bank = "";
					// 比對bank名稱
					if(locale.equals("zh_CH")) {
						bank = admbankdao.getBankChName(DPBHNO);
					}
					else if(locale.equals("en")) {
						bank = admbankdao.getBankEnName(DPBHNO);
					}
					else {
						bank = admbankdao.getBankName(DPBHNO);
					}
					if(bank != "") {
						DPBHNO = DPBHNO + "-" + bank;						
					}else {
						DPBHNO = "050-"+i18n.getMsg("LB.W0605");
					}
					log.trace(ESAPIUtil.vaildLog("DPBHNO>>{}"+ DPBHNO));
					data.put("DPBHNO", DPBHNO);
					//轉入帳號
					String DPACNO = reqParam.get("DPACNO"+j);
					log.trace(ESAPIUtil.vaildLog("DPACNO>>{}"+ DPACNO));
					data.put("DPACNO", DPACNO);
					//好記名稱
					String DPACGONAME = reqParam.get("DPACGONAME"+j);
					log.trace(ESAPIUtil.vaildLog("DPACGONAME>>{}"+ DPACGONAME));
					bs.addData("COUNT", COUNT);
					data.put("DPACGONAME", DPACGONAME);
					//加到Rec
					REC.add(data);
				}
				if(!RowData.equals(""))
					COUNT = String.valueOf(Integer.valueOf(COUNT) + ACNROW.length);	
				else
					COUNT = String.valueOf(Integer.valueOf(COUNT));	
			}else {
				COUNT = String.valueOf(ACNROW.length);				
			}
			log.trace("COUNT>>{}",COUNT);
			log.trace(ESAPIUtil.vaildLog("REC>>{}"+CodeUtil.toJson(REC)));
			bs.addData("COUNT", COUNT);
			bs.addData("REC", REC);
			if(REC != null) {
				bs.setResult(true);				
			}			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("predesignated_account_apply_s error >> {}",e);
		}
		return bs;
	}
	
	
	/**
	 * N215約定轉入帳號申請_IDGATE_資料轉
	 * 
	 * @param params
	 * @return
	 */
	public String getACNINFO_apply(Map<String, Object> reqParam) {

		log.trace(ESAPIUtil.vaildLog("getACNINFO_apply~~~{}"+CodeUtil.toJson(reqParam)));
		
		String SelectedRecord = "";
		try {
			//處理值
			List<Map<String,String>> recdata = (List<Map<String, String>>) reqParam.get("REC");
			String data = recdata.toString();
			log.trace(ESAPIUtil.vaildLog("data>>>{}"+ data));
			//筆數
			int COUNT = Integer.parseInt((String)reqParam.get("COUNT"));
			log.trace("COUNT>>>{}",COUNT);
			//TODO 要調整寫法
			String DPBHNO = "";
			String BANK = "";
			String DPACNO = "";
			String DPGONAME = "";
			String ACNBHNO = "";
			String SPACE = "";
			int j = 2;
			//字串去掉符號之後切割加入陣列
			String dataReplace = data.replace("[","").replace("{","").replace("}","").replace("]","");
			log.trace("dataReplace>>>{}",dataReplace);
			String[] DPNAME = dataReplace.split(",");
			log.trace("DPNAME>>>{}",DPNAME.length);
			String[] DPGONAMES = new String[COUNT];
			
			for(int i=0;i < COUNT;i++) {
				Map<String,String> recMap = new HashMap<String,String>();
				if(i == 0) {
					//帳號
					DPACNO = DPNAME[i].trim().replace("DPACNO=", "");
					log.trace("DPACNO>>>{}",DPACNO);
					recMap.put("DPACNO",DPACNO);
					//好記名稱
					DPGONAME = DPNAME[i+1].trim().replace("DPACGONAME=", "");
					log.trace("DPGONAME>>>{}",DPGONAME);
					recMap.put("DPGONAME",DPGONAME);
					//銀行
					DPBHNO = DPNAME[i+2].trim().replace("DPBHNO=", "").substring(0,3);				
					log.trace("DPBHNO>>>{}",DPBHNO);
					//保留銀行名稱
					BANK = DPNAME[i+2].replace("DPBHNO=", "").trim();
					log.trace("BANK>>>{}",BANK);
					recMap.put("DPBHNO",BANK);
					//組合成Bean所需字串
					//沒滿19長度補空格到19
//					if(DPACNO.length() != 0) {
						ACNBHNO = DPBHNO + DPACNO;
						for(int k = 0;k < 19 - ACNBHNO.length();k++) {
							SPACE = SPACE + " ";
						}
						SelectedRecord = SelectedRecord + DPBHNO + DPACNO + SPACE;
						SPACE = "";						
//					}
				}else {
					//帳號
					DPACNO = DPNAME[i+j].trim().replace("DPACNO=", "");
					log.trace("DPACNO>>>{}",DPACNO);
					recMap.put("DPACNO",DPACNO);
					//好記名稱
					DPGONAME = DPNAME[i+j+1].trim().replace("DPACGONAME=", "");
					log.trace("DPGONAME>>>{}",DPGONAME);
					recMap.put("DPGONAME",DPGONAME);
					//銀行
					DPBHNO = DPNAME[i+j+2].trim().replace("DPBHNO=", "").substring(0,3);	
					log.trace("DPBHNO>>>{}",DPBHNO);
					//保留銀行名稱
					BANK = DPNAME[i+j+2].replace("DPBHNO=", "").trim();
					log.trace("BANK>>>{}",BANK);
					recMap.put("DPBHNO",BANK);
					//組合成Bean所需字串
					//沒滿19長度補空格到19
					ACNBHNO = DPBHNO + DPACNO;
//					if(DPACNO.length() != 0) {
						ACNBHNO = DPBHNO + DPACNO;
						for(int k = 0;k < 19 - ACNBHNO.length();k++) {
							SPACE = SPACE + " ";
						}
						SelectedRecord = SelectedRecord + DPBHNO + DPACNO + SPACE;
						SPACE = "";						
//					}
					j = j + 2;
				}
			}
			log.trace("SelectedRecord>>>{}",SelectedRecord);
		} catch (Exception e) {
			log.error("predesignated_account_apply_r_trns error >> {}",e);
		}
		return SelectedRecord;
	}
	
	/**
	 * N215約定轉入帳號申請_r
	 * 
	 * @param params
	 * @return
	 */
	public BaseResult predesignated_account_apply_r(String cusidn,Map<String, String> reqParam) {

		log.trace(ESAPIUtil.vaildLog("predesignated_account_apply_r~~~{}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			String SelectedRecord = "";
			//處理值
			String data = reqParam.get("data");
			log.trace(ESAPIUtil.vaildLog("data>>>{}"+ data));
			//筆數
			int COUNT = Integer.parseInt(reqParam.get("COUNT"));
			log.trace("COUNT>>>{}",COUNT);
			List<Map<String,String>> REC = new ArrayList<Map<String,String>>();
			//TODO 要調整寫法
			String DPBHNO = "";
			String BANK = "";
			String DPACNO = "";
			String DPGONAME = "";
			String ACNBHNO = "";
			String SPACE = "";
			int j = 2;
			//字串去掉符號之後切割加入陣列
			String dataReplace = data.replace("[","").replace("{","").replace("}","").replace("]","");
			log.trace("dataReplace>>>{}",dataReplace);
			String[] DPNAME = dataReplace.split(",");
			log.trace("DPNAME>>>{}",DPNAME.length);
			String[] DPGONAMES = new String[COUNT];
			
			for(int i=0;i < COUNT;i++) {
				Map<String,String> recMap = new HashMap<String,String>();
				if(i == 0) {
					//帳號
					DPACNO = DPNAME[i].trim().replace("DPACNO=", "");
					log.trace("DPACNO>>>{}",DPACNO);
					recMap.put("DPACNO",DPACNO);
					//好記名稱
					DPGONAME = DPNAME[i+1].trim().replace("DPACGONAME=", "");
					log.trace("DPGONAME>>>{}",DPGONAME);
					recMap.put("DPGONAME",DPGONAME);
					//寫入資料庫用
					if( DPGONAME != null && !"".equals(DPGONAME)) {
						DPGONAMES[i] = DPGONAME;						
					}else {
						if(reqParam.get("Favorite"+i).trim() != null && !"".equals(reqParam.get("Favorite"+i).trim())) {
							DPGONAMES[i] = reqParam.get("Favorite"+i).trim();		
							recMap.put("DPGONAME",DPGONAMES[i]);					
						}else {
							DPGONAMES[i] = "";
						}
					}
					//銀行
					DPBHNO = DPNAME[i+2].trim().replace("DPBHNO=", "").substring(0,3);				
					log.trace("DPBHNO>>>{}",DPBHNO);
					//保留銀行名稱
					BANK = DPNAME[i+2].replace("DPBHNO=", "").trim();
					log.trace("BANK>>>{}",BANK);
					recMap.put("DPBHNO",BANK);
					//組合成Bean所需字串
					//沒滿19長度補空格到19
//					if(DPACNO.length() != 0) {
						ACNBHNO = DPBHNO + DPACNO;
						for(int k = 0;k < 19 - ACNBHNO.length();k++) {
							SPACE = SPACE + " ";
						}
						SelectedRecord = SelectedRecord + DPBHNO + DPACNO + SPACE;
						SPACE = "";						
//					}
				}else {
					//帳號
					DPACNO = DPNAME[i+j].trim().replace("DPACNO=", "");
					log.trace("DPACNO>>>{}",DPACNO);
					recMap.put("DPACNO",DPACNO);
					//好記名稱
					DPGONAME = DPNAME[i+j+1].trim().replace("DPACGONAME=", "");
					log.trace("DPGONAME>>>{}",DPGONAME);
					recMap.put("DPGONAME",DPGONAME);
					//寫入資料庫用
					if( DPGONAME != null && !"".equals(DPGONAME)) {
						DPGONAMES[i] = DPGONAME;						
					}else {
						if(reqParam.get("Favorite"+i).trim() != null && !"".equals(reqParam.get("Favorite"+i).trim())) {
							DPGONAMES[i] = reqParam.get("Favorite"+i).trim();							
						}else {
							DPGONAMES[i] = "";
						}
					}
					//銀行
					DPBHNO = DPNAME[i+j+2].trim().replace("DPBHNO=", "").substring(0,3);	
					log.trace("DPBHNO>>>{}",DPBHNO);
					//保留銀行名稱
					BANK = DPNAME[i+j+2].replace("DPBHNO=", "").trim();
					log.trace("BANK>>>{}",BANK);
					recMap.put("DPBHNO",BANK);
					//組合成Bean所需字串
					//沒滿19長度補空格到19
					ACNBHNO = DPBHNO + DPACNO;
//					if(DPACNO.length() != 0) {
						ACNBHNO = DPBHNO + DPACNO;
						for(int k = 0;k < 19 - ACNBHNO.length();k++) {
							SPACE = SPACE + " ";
						}
						SelectedRecord = SelectedRecord + DPBHNO + DPACNO + SPACE;
						SPACE = "";						
//					}
					j = j + 2;
				}
				REC.add(recMap);
			}
			log.trace("SelectedRecord>>>{}",SelectedRecord);
			
			bs = N215A_1_REST(cusidn,SelectedRecord,String.valueOf(COUNT),reqParam.get("FGTXWAY"),reqParam);				
			
			if(bs != null) {
				//更新資料庫好記名稱
				String[] BANKS = new String[COUNT];
				String[] ACN = new String[COUNT];
				int position1=0;
				int position2=0;
				//先取出bank跟acn
				for(int i=0;i<COUNT;i++)
				{								
					position2=position1+19;			
					BANKS[i]= (SelectedRecord.substring(position1,position2)).substring(0,3);
					ACN[i]=	(SelectedRecord.substring(position1,position2)).substring(3).trim();
					position1=position2;				
				}
				//比對資料庫好記名稱並約定更新
				String txnttraccset = "";
				for(int i=0;i<COUNT;i++)
				{					
					log.debug("ACN >> {} ",ACN[i]);
					log.debug("BANKS >> {}",BANKS[i]);
					log.debug(ESAPIUtil.vaildLog("DPGONAMES >> {}"+ DPGONAMES[i]));
					if(txntraccsetdao.findDptrdacnoIsExists(cusidn,BANKS[i],ACN[i])) {
						txnttraccset = daoService.updateDpgoName(cusidn,ACN[i],BANKS[i],DPGONAMES[i], "1");
//						txnttraccset = txntraccsetdao.updateDpgoname(cusidn,ACN[i],BANKS[i],DPGONAMES[i]);			
					}else {
						txnttraccset = daoService.createDpgoname(cusidn,ACN[i],BANKS[i],DPGONAMES[i]);
//						txnttraccset = txntraccsetdao.createDpgoname(cusidn,ACN[i],BANKS[i],DPGONAMES[i]);	
					}
				}
				if(!"".equals(txnttraccset)) {
					bs.addData("rec", REC);
					bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
					bs.addData("MSG",i18n.getMsg("LB.Transaction_successful"));//交易成功					
				}
			}else{
				bs.setResult(false);
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("predesignated_account_apply_r error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * N215約定轉入帳號註銷
	 * 
	 * @param params
	 * @return
	 */
	public BaseResult predesignated_account_logout(String cusidn) {

		log.trace("predesignated_account_logout");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = N215D_REST(cusidn);
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				Map<String, String> argBean = SpringBeanFactory.getBean("TRAFLAG");
								
				for (Map<String, String> row : rows) {
					
					if(argBean.containsKey((String)row.get("TRAFLAG")))
					{
						String TRAFLAG = argBean.get((String)row.get("TRAFLAG"));
						row.put("RAFLAG",i18n.getMsg(TRAFLAG));
					}
					
					String acn = row.get("ACN");
					String bnkcod = row.get("BNKCOD");
					
					//取得好記名稱
					String dpgoname = txntraccsetdao.getDpgoName(cusidn,acn,bnkcod);
					row.put("DPGONAME",dpgoname);
					
					// 比對bank名稱
//					String bank = admbankdao.getBankName(bnkcod);
					String locale = LocaleContextHolder.getLocale().toString();
					String bank = "";
					// 比對bank名稱
					if(locale.equals("zh_CH")) {
						bank = admbankdao.getBankChName(bnkcod);
					}
					else if(locale.equals("en")) {
						bank = admbankdao.getBankEnName(bnkcod);
					}
					else {
						bank = admbankdao.getBankName(bnkcod);
					}
					
					if(bank != "") {
						bnkcod = bnkcod + "_" + bank;						
					}
					row.put("BNKCOD", bnkcod);
					
				}
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("predesignated_account_logout error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * N215約定轉入帳號註銷_s
	 * 
	 * @param params
	 * @return
	 */
	public BaseResult predesignated_account_logout_s(String cusidn,Map<String, String> reqParam) {

		log.trace("predesignated_account_logout_s");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			ArrayList<Map<String,String>> REC = new ArrayList<Map<String,String>>();
			int COUNT = 0;
			String value = "";
			String[] DPNAME = null;
			//將checkbox整理成單一字串
			for(String key : reqParam.keySet()){
				Map<String,String> data = new HashMap<String, String>();
				value = reqParam.get(key);
				log.trace(ESAPIUtil.vaildLog("key>>>{}"+ key+"  "+value));
//				DPNAME = value.substring(1, value.length()-1).split(",");
//				//帳號
//				data.put("ACN", DPNAME[0].replace("ACN=", "").trim());
////				REC.add(data);
//				//銀行
//				data.put("BNKCOD", DPNAME[1].replace("BNKCOD=", "").trim());
////				REC.add(data);
//				//好記名稱
//				data.put("DPGONAME", DPNAME[3].replace("DPGONAME=", "").trim());
				
				DPNAME = value.split(",",-1);
				data.put("ACN", DPNAME[0].trim());
				data.put("BNKCOD", DPNAME[1].trim());
				data.put("DPGONAME", DPNAME[2].trim());
				
				REC.add(data);
				//計算筆數
				COUNT = COUNT + 1;
			}
			//log.trace("COUNT>>{}",COUNT);
			//log.trace("REC>>{}",REC);
			bs.addData("COUNT", COUNT);
			bs.addData("REC", REC);
			if(REC != null) {
				bs.setResult(true);				
			}			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("predesignated_account_logout_s error >> {}",e);
		}
		return bs;
	}
	
	
	/**
	 * N215約定轉入帳號註銷_IDGATE_資料轉換
	 * 
	 * @param params
	 * @return
	 */
	public String getACNINFO_logout(Map<String, Object> reqParam) {

		log.trace(ESAPIUtil.vaildLog("getACNINFO_logout~~~{}"+CodeUtil.toJson(reqParam)));
		String SelectedRecord = "";
		try {
			List<Map<String,String>> recdata = (List<Map<String, String>>) reqParam.get("REC");
			String data = recdata.toString();
			log.trace(ESAPIUtil.vaildLog("data>>>{}"+ data));
			//筆數
			int COUNT = recdata.size();
			log.trace("COUNT>>>{}",COUNT);
			//TODO 要調整寫法
			String BNKCOD = "";
			String BANK = "";
			String ACN = "";
			String ACNBHNO = "";
			String SPACE = "";
			int j = 2;
			//字串去掉符號之後切割加入陣列
			String dataReplace = data.replace("[","").replace("{","").replace("}","").replace("]","");
			log.trace("dataReplace>>>{}",dataReplace);
			String[] DPNAME = dataReplace.split(",");
			
			for(int i=0;i < COUNT;i++) {
				if(i == 0) {
					//帳號
					ACN = DPNAME[i+2].trim().replace("ACN=", "");
					log.trace("ACN>>>{}",ACN);
					//銀行
					BNKCOD = DPNAME[i].trim().replace("BNKCOD=", "").substring(0,3);				
					log.trace("BNKCOD>>>{}",BNKCOD);
					//保留銀行名稱
					BANK = DPNAME[i].replace("BNKCOD=", "").trim();
					log.trace("BANK>>>{}",BANK);
					//組合成Bean所需字串
					//沒滿19長度補空格到19
//					if(ACN.length() != 0) {
						ACNBHNO = BNKCOD + ACN;
						for(int k = 0;k < 19 - ACNBHNO.length();k++) {
							SPACE = SPACE + " ";
						}
						SelectedRecord = SelectedRecord + BNKCOD + ACN + SPACE;
						SPACE = "";						
//					}
				}else {
					//帳號
					ACN = DPNAME[i+j+2].trim().replace("ACN=", "");
					log.trace("ACN>>>{}",ACN);
					//銀行
					BNKCOD = DPNAME[i+j].trim().replace("BNKCOD=", "").substring(0,3);				
					log.trace("BNKCOD>>>{}",BNKCOD);
					//保留銀行名稱
					BANK = DPNAME[i+j].replace("BNKCOD=", "").trim();
					log.trace("BANK>>>{}",BANK);
					//組合成Bean所需字串
					//沒滿19長度補空格到19
//					if(ACN.length() != 0) {
						ACNBHNO = BNKCOD + ACN;
						for(int k = 0;k < 19 - ACNBHNO.length();k++) {
							SPACE = SPACE + " ";
						}
						SelectedRecord = SelectedRecord + BNKCOD + ACN + SPACE;
						SPACE = "";						
//					}
					j = j + 2;
				}
			}			
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getACNINFO_logout error >> {}",e);
		}
		return SelectedRecord;
	}
	
	
	/**
	 * N215約定轉入帳號註銷_r
	 * 
	 * @param params
	 * @return
	 */
	public BaseResult predesignated_account_logout_r(String cusidn,Map<String, String> reqParam) {

		log.trace(ESAPIUtil.vaildLog("predesignated_account_logout_r~~~{}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			String SelectedRecord = "";
			//處理上一頁帶來的值
			String data = reqParam.get("data");
			log.trace(ESAPIUtil.vaildLog("data>>>{}"+ data));
			//筆數
			int COUNT = Integer.parseInt(reqParam.get("COUNT"));
			log.trace("COUNT>>>{}",COUNT);
			List<Map<String,String>> REC = new ArrayList<Map<String,String>>();			
			//TODO 要調整寫法
			String BNKCOD = "";
			String BANK = "";
			String ACN = "";
			String ACNBHNO = "";
			String SPACE = "";
			int j = 2;
			//字串去掉符號之後切割加入陣列
			String dataReplace = data.replace("[","").replace("{","").replace("}","").replace("]","");
			log.trace("dataReplace>>>{}",dataReplace);
			String[] DPNAME = dataReplace.split(",");
			
			for(int i=0;i < COUNT;i++) {
				Map<String,String> recMap = new HashMap<String,String>();
				if(i == 0) {
					//帳號
					ACN = DPNAME[i+2].trim().replace("ACN=", "");
					log.trace("ACN>>>{}",ACN);
					recMap.put("ACN",ACN);
					//銀行
					BNKCOD = DPNAME[i].trim().replace("BNKCOD=", "").substring(0,3);				
					log.trace("BNKCOD>>>{}",BNKCOD);
					//保留銀行名稱
					BANK = DPNAME[i].replace("BNKCOD=", "").trim();
					log.trace("BANK>>>{}",BANK);
					recMap.put("BNKCOD",BANK);
					//組合成Bean所需字串
					//沒滿19長度補空格到19
//					if(ACN.length() != 0) {
						ACNBHNO = BNKCOD + ACN;
						for(int k = 0;k < 19 - ACNBHNO.length();k++) {
							SPACE = SPACE + " ";
						}
						SelectedRecord = SelectedRecord + BNKCOD + ACN + SPACE;
						SPACE = "";						
//					}
				}else {
					//帳號
					ACN = DPNAME[i+j+2].trim().replace("ACN=", "");
					log.trace("ACN>>>{}",ACN);
					recMap.put("ACN",ACN);
					//銀行
					BNKCOD = DPNAME[i+j].trim().replace("BNKCOD=", "").substring(0,3);				
					log.trace("BNKCOD>>>{}",BNKCOD);
					//保留銀行名稱
					BANK = DPNAME[i+j].replace("BNKCOD=", "").trim();
					log.trace("BANK>>>{}",BANK);
					recMap.put("BNKCOD",BANK);
					//組合成Bean所需字串
					//沒滿19長度補空格到19
//					if(ACN.length() != 0) {
						ACNBHNO = BNKCOD + ACN;
						for(int k = 0;k < 19 - ACNBHNO.length();k++) {
							SPACE = SPACE + " ";
						}
						SelectedRecord = SelectedRecord + BNKCOD + ACN + SPACE;
						SPACE = "";						
//					}
					j = j + 2;
				}
				REC.add(recMap);
			}			
			
			bs = N215D_1_REST(cusidn,SelectedRecord,String.valueOf(COUNT),reqParam);				
			
			if(bs != null && bs.getResult()) {
					bs.addData("rec", REC);
					bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
					bs.addData("MSG",i18n.getMsg("LB.Transaction_successful"));//交易成功
					bs.setResult(true);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("predesignated_account_logout_r error >> {}",e);
		}
		return bs;
	}

	/**
	 * N215約定轉入帳號好記名稱
	 * 
	 * @param params
	 * @return
	 */
	public BaseResult predesignated_account_modify(String cusidn) {

		log.trace("predesignated_account_modify");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = N215M_REST(cusidn);
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				Map<String, String> argBean = SpringBeanFactory.getBean("TRAFLAG");
				
								
				for (Map<String, String> row : rows) {
					
					if(argBean.containsKey((String)row.get("TRAFLAG")))
					{
						String TRAFLAG = argBean.get((String)row.get("TRAFLAG"));
						row.put("RAFLAG",i18n.getMsg(TRAFLAG));
					}
					
					String acn = row.get("ACN");
					String bnkcod = row.get("BNKCOD");
					
					//取得好記名稱
					String DpgoName = txntraccsetdao.getDpgoName(cusidn,acn,bnkcod);
					row.put("DPGONAME",DpgoName);

					List<TXNTRACCSET> r = txntraccsetdao.findByDPGONAME(cusidn,bnkcod, acn);
					if(r.size() > 0) {
						TXNTRACCSET r1 = r.get(0);            
						String DPGONAME=r1.getDPGONAME();			    
						int DPACCSETID=r1.getDPACCSETID();
						row.put("DPGONAME", DPGONAME);
						row.put("DPACCSETID", String.valueOf(DPACCSETID));
					}
					
					// 比對bank名稱
//					String bank = admbankdao.getBankName(bnkcod);
					
					String locale = LocaleContextHolder.getLocale().toString();
					String bank = "";
					// 比對bank名稱
					if(locale.equals("zh_CH")) {
						bank = admbankdao.getBankChName(bnkcod);
					}
					else if(locale.equals("en")) {
						bank = admbankdao.getBankEnName(bnkcod);
					}
					else {
						bank = admbankdao.getBankName(bnkcod);
					}
					bnkcod = bnkcod + "_" + bank;
					row.put("BNKCOD_C", bnkcod);
					
					
				}
				int COUNT = 0;
				COUNT = rows.size();
				bs.addData("COUNT", COUNT);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("predesignated_account_modify error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * N215約定轉入帳號好記名稱_確認
	 * 
	 * @param params
	 * @return
	 */
	public BaseResult predesignated_account_modify_s(String cusidn, Map<String, String> reqParam) {

		log.trace("predesignated_account_modify_s");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			log.debug(ESAPIUtil.vaildLog("ROWDATA: {}"+ reqParam.get("ROWDATA")));

			String value = reqParam.get("ROWDATA");
			if(value != null) {
				value = value.substring(1, value.length() - 1); // remove curly brackets
				log.trace(ESAPIUtil.vaildLog("ROWDATA.value: "+ value));
				String[] keyValuePairs = value.split(","); // split the string to creat key-value pairs
				log.trace(ESAPIUtil.vaildLog("ROWDATA.keyValuePairs: "+ keyValuePairs));
	
				Map<String, String> map = new HashMap<>();
				for (String pair : keyValuePairs) // iterate over the pairs
				{
					String[] entry = pair.split("="); // split the pairs to get key and value
	
					String mapKey = "";
					String MapValue = "";
					log.trace("ROWDATA.entry.length: " + entry.length);
					if (entry.length == 2) {
						mapKey = "".equals(entry[0].trim()) ? "" : entry[0].trim();
						MapValue = "".equals(entry[1].trim()) ? "" : entry[1].trim();
					} else {
						mapKey = entry[0].trim();
					}
					//好記名稱處理
					map.put("DPGONAME", map.get("DPGONAME"));
					log.trace(ESAPIUtil.vaildLog("ROWDATA.mapKey:{}"+ mapKey));
					log.trace(ESAPIUtil.vaildLog("ROWDATA.MapValue:{}"+ MapValue));
					map.put(mapKey, MapValue); // add them to the hashmap and trim whitespaces
				}
			    String DPBHNO = map.get("BNKCOD");
			    String DPACNO = map.get("ACN");
				String SelectedRecord="";
				String ACN1="";
				String ACNINFO = "";
				for(int i=0;i<16-DPACNO.length();i++)
				{
					ACN1+=" ";
				}
				ACN1=DPACNO+ACN1;
				SelectedRecord=DPBHNO+ACN1;
				String Padding = "";
				for(int i=0;i<190-SelectedRecord.length();i++)
				{
					Padding+=" ";
				}
				ACNINFO = SelectedRecord+Padding;
				map.put("ACNINFO", ACNINFO);
	
				log.debug(ESAPIUtil.vaildLog("ROWDATA:{}"+CodeUtil.toJson(map)));
				bs.addData("dataSet", map);
				bs.addData("COUNT", reqParam.get("COUNT"));
				bs.setResult(true);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("predesignated_account_modify_s error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * N215約定轉入帳號好記名稱_結果
	 * 
	 * @param params
	 * @return
	 */
	public BaseResult predesignated_account_modify_r(String cusidn , Map<String, String> reqParam) {

		log.trace("predesignated_account_modify_r");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			//約定轉入帳號
			String acn = reqParam.get("ACN");
			log.trace(ESAPIUtil.vaildLog("acn>>{}"+ acn));
			//銀行
			String bnkcod = reqParam.get("BNKCOD");
			log.trace(ESAPIUtil.vaildLog("bnkcod>>{}"+ bnkcod));
			//好記名稱
			String dpgoname = reqParam.get("DPACGONAME2");
			log.trace(ESAPIUtil.vaildLog("dpgoname>>{}"+ dpgoname));
			//更新好記名稱
//			String updateDpgoName = txntraccsetdao.updateDpgoName(cusidn,acn,bnkcod,dpgoname);
			bs = N215M_1_REST(cusidn,reqParam);
			if(bs != null && bs.getResult()) {
				bs.addData("BNKCOD", bnkcod);
				bs.addData("ACN", acn);
				bs.addData("DPGONAME", dpgoname);
				bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				bs.addData("MSG",i18n.getMsg("LB.Transaction_successful"));//交易成功
				bs.setResult(true);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("predesignated_account_modify_r error >> {}",e);
		}
		return bs;
	}
	
	/**
	 *  N1012 空白票據
	 * @param CUSIDN
	 * @return
	 */
	public BaseResult blank_bill(String CUSIDN)
	{
		BaseResult bs = new BaseResult();
		try
		{
			bs = N920_REST(CUSIDN, CHECK_ACNO);
			if(bs!=null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> row = (ArrayList<Map<String, String>>) bsData.get("REC");
				String count = String.valueOf(row.size());
				log.trace("count>>>{}",count);
				if(count == null || "0".equals(count)) {
					String msgCode = "Z999";
					String msgName = i18n.getMsg("LB.X1889");//查無交易帳號
					bs.setMessage(msgCode, msgName);
					bs.setResult(Boolean.FALSE);
				}else {
					bs.addData("CUSIDN", CUSIDN);
				}
			}
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("blank_bill error >> {}",e);
		}
		return bs;
	}

	/**
	 * N1012 空白票據
	 * @param CUSIDN
	 * @param reqParam
	 * @return
	 */
	public BaseResult blank_bill_result(String CUSIDN, Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			//20200903 新增 分機若不為空，電話+#分機
			if(StrUtil.isNotEmpty(reqParam.get("TEL2"))) {
				String tel = reqParam.get("TEL") + "#" + reqParam.get("TEL2");
				reqParam.put("TEL", tel);
			}
			reqParam.put("CUSIDN", CUSIDN);
			bs = N1012_REST(reqParam);
			if (bs.getResult()){
				// 後置回應電文
				Map<String, String> dataMap = (Map<String, String>) bs.getData();
				dataMap.put("CUSIDN", CUSIDN);
				dataMap.put("APPLYNUM", reqParam.get("APPLYNUM"));
				dataMap.put("CHKCNT", reqParam.get("CHKCNT"));
				dataMap.put("TEL", reqParam.get("TEL"));
				dataMap.put("CHKTYPE", chkTypeConvert(dataMap.get("CHKTYPE")));
				dataMap.put("CHKFORM", chkFormConvert(dataMap.get("CHKFORM")));
				log.debug("blank_bill_result >>>{}", bs.getData());
			}
		}
		catch (Exception e)
		{
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("blank_bill_result error >> {}",e);
		}
		return bs;
	}
	/**
	 * 轉換票據種類
	 * @param CHKTYPE
	 * @return
	 */
	public String chkTypeConvert(String CHKTYPE) {
		// 票據種類 ，1:支票，2:擔當付款本票 
		switch (CHKTYPE)
		{
			case "1":
				CHKTYPE = i18n.getMsg("LB.D0533");//支票
				break;
			case "2":
				CHKTYPE = i18n.getMsg("LB.D0534");//擔當付款本票
				break;
			default:
				break;
		}
		return CHKTYPE;
	}
	/**
	 * 轉換票據格式
	 * @param CHKFORM
	 * @return
	 */
	public String chkFormConvert(String CHKFORM) {
		// 票據種類 ，1:一般，2:專戶支票
		switch (CHKFORM)
		{
			case "1":
				CHKFORM = i18n.getMsg("LB.D0535");//一般
				break;
			case "2":
				CHKFORM = i18n.getMsg("LB.D0536");//專戶支票
				break;
			default:
				break;
		}
		return CHKFORM;
	}
	
	/**
	 * 檢查TxToken
	 * 
	 * @param pageTxToken 頁面TxToken
	 * @param sessionTxToken 
	 * @return FALSE 表示有問題
	 */
	public BaseResult validateToken(String pageTxToken, String SessionFinshToken)
	{
		BaseResult bs = new BaseResult();
		try
		{
			log.trace(ESAPIUtil.vaildLog("validateToken pageTxToken : {}"+ pageTxToken));
			log.trace("validateToken SessionFinshToken : {}", SessionFinshToken);

			if (pageTxToken == null)
			{
				log.debug(" reqTxToken==null");
				bs.setMessage("FE003", "reqTxToken空白");
				bs.setResult(Boolean.TRUE);
			}
			if (StrUtil.isNotEmpty(SessionFinshToken) && SessionFinshToken.equals(pageTxToken))
			{
				log.debug(" pagToken SessionFinshToken &&  一樣 表示重複交易");
				bs.setMessage("FE003", "重複交易");
				bs.setResult(Boolean.TRUE);
			}
		}
		catch (Exception e)
		{
			log.error("getTransferToken Error", e);
		}
		return bs;
	}
	
	
	/**
	 * N913 啟用行動銀行服務 輸入頁
	 * @param session
	 * @return
	 */
	public BaseResult enable_mobile(Map<String, String> session)
	{
		log.trace("MBSTAT, MBOPNDT, MBOPNTM >> {}", session);
		BaseResult bs = new BaseResult();
		try
		{
			// 行動銀行啟用狀態
			String currentStatus = session.get("currentStatus");
			// 行動銀行上次啟用/關閉日期
			String modifyDateVal = session.get("modifyDateVal");
			// 行動銀行上次啟用/關閉時間
			String modifyTimeVal = session.get("modifyTimeVal");
			
			if(currentStatus == null || modifyDateVal == null || modifyTimeVal == null)
			{
				throw new NullPointerException("參數是 Null");
			}
			
			// 判斷目前設定狀態
			String statusWording = "";
			if("A".equals(currentStatus))
			{
				// TODO i18n
				statusWording = i18n.getMsg("LB.D0329");//已啟用
			}
			else if("D".equals(currentStatus))
			{
				statusWording = i18n.getMsg("LB.D0328");//已停用
			}
			else
			{
				statusWording = i18n.getMsg("LB.D0322");//未啟用
			}
			log.trace("行動銀行啟用狀態：{}", statusWording);
			
			
			// 啟用/停用日期及時間
			String modifyDT = "";
			// 未啟用狀態 MBOPNDT, MBOPNTM 可能回空字串
			if( StrUtil.isNotEmpty(modifyDateVal) && StrUtil.isNotEmpty(modifyTimeVal) )
			{
				String modifyDate = i18n.getMsg("LB.D0583") + modifyDateVal.substring(0, 3);//民國
				modifyDate += i18n.getMsg("LB.Year") + modifyDateVal.substring(3, 5);//年
				modifyDate += i18n.getMsg("LB.Month") + modifyDateVal.substring(5, 7) + i18n.getMsg("LB.Day");//月 日
				
				
				String modifyTime = modifyTimeVal.substring(0, 2);
				modifyTime += i18n.getMsg("LB.X1723") + modifyTimeVal.substring(2, 4); //時
				modifyTime += i18n.getMsg("LB.Minute") + modifyTimeVal.substring(4, 6) + i18n.getMsg("LB.Second"); //分 秒
				
				String modifyYear = modifyDateVal.substring(0, 3);
				String modifyMonth = modifyDateVal.substring(3, 5);
				String modifyDay = modifyDateVal.substring(5, 7);
				String modifyHH = modifyTimeVal.substring(0, 2);
				String modifyMM = modifyTimeVal.substring(2, 4);
				String modifySS = modifyTimeVal.substring(4, 6);
				if(!modifyYear.equals("")) {
					String modifyYearEn = Integer.toString(Integer.parseInt(modifyYear) + 1911);					
					bs.addData("modifyYearEn", modifyYearEn);
				}
				else {
					bs.addData("modifyYearEn", "");
				}
				
				bs.addData("modifyYear", modifyYear);
				bs.addData("modifyMonth", modifyMonth);
				bs.addData("modifyDay", modifyDay);
				bs.addData("modifyHH", modifyHH);
				bs.addData("modifyMM", modifyMM);
				bs.addData("modifySS", modifySS);
				
				
				modifyDT = modifyDate + "--" + modifyTime;
			}
			log.trace("modifyDT >> {}", modifyDT);
			
			// button 控制
			String enableStatus  = "A".equals(currentStatus) ? "checked" : "";	// 啟用
			String disableStatus = "";
			if("D".equals(currentStatus) )
			{
				disableStatus = "checked";	// 停用
			}
			else if("".equals(currentStatus))
			{
				disableStatus = "checked";	// 未啟用
			}
			log.trace("enableStatus >> {}", enableStatus);
			log.trace("disableStatus >> {}", disableStatus);
			
			
			bs.addData("currentStatus", currentStatus);
			bs.addData("statusWording", statusWording);
			bs.addData("modifyDT", modifyDT);
			bs.addData("enableStatus", enableStatus);
			bs.addData("disableStatus", disableStatus);
			bs.setResult(true);
			bs.setMessage("0", i18n.getMsg("LB.X1805"));//查詢成功
			
		}
		catch (Exception e)
		{
			log.error("enable_mobile Error", e);
		}
		return bs;
	}
	
	
	public BaseResult enable_mobile_confirm(String oriStatus, String cusidn, Map<String, String> reqParam)
	{
		log.trace("enable_mobile_confirm >> {}", oriStatus);
		BaseResult bs = new BaseResult();
		try
		{
			
		}
		catch (Exception e)
		{
			log.error("enable_mobile Error", e);
		}
		return bs;
	}
	
	public BaseResult enable_mobile_result(String cusidn, Map<String, String> reqParam)
	{
		log.trace(ESAPIUtil.vaildLog("enable_mobile_result >> {}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = N913_REST(cusidn, reqParam);
			if(bs != null && bs.getResult()) {
				try
				{
					Map<String, Object> bsData = (Map) bs.getData();
					String modifyDateVal = bsData.get("MBOPNDT").toString();
					String modifyTimeVal = bsData.get("MBOPNTM").toString();
					String modifyDT = "";
					
					String modifyDate = i18n.getMsg("LB.D0583") + modifyDateVal.substring(0, 3); //民國
					modifyDate += i18n.getMsg("LB.Year") + modifyDateVal.substring(3, 5); //年
					modifyDate += i18n.getMsg("LB.Month") + modifyDateVal.substring(5, 7) + i18n.getMsg("LB.Day"); //月 日
					
					
					String modifyTime = modifyTimeVal.substring(0, 2);
					modifyTime += i18n.getMsg("LB.X1723") + modifyTimeVal.substring(2, 4); //時
					modifyTime += i18n.getMsg("LB.Minute") + modifyTimeVal.substring(4, 6) + i18n.getMsg("LB.Second"); //分 秒
					
					modifyDT = modifyDate + "--" + modifyTime;
					
					String modifyYear = modifyDateVal.substring(0, 3);
					String modifyMonth = modifyDateVal.substring(3, 5);
					String modifyDay = modifyDateVal.substring(5, 7);
					String modifyHH = modifyTimeVal.substring(0, 2);
					String modifyMM = modifyTimeVal.substring(2, 4);
					String modifySS = modifyTimeVal.substring(4, 6);
					if(!modifyYear.equals("")) {
						String modifyYearEn = Integer.toString(Integer.parseInt(modifyYear) + 1911);					
						bs.addData("modifyYearEn", modifyYearEn);
					}
					else {
						bs.addData("modifyYearEn", "");
					}
					
					bs.addData("modifyYear", modifyYear);
					bs.addData("modifyMonth", modifyMonth);
					bs.addData("modifyDay", modifyDay);
					bs.addData("modifyHH", modifyHH);
					bs.addData("modifyMM", modifyMM);
					bs.addData("modifySS", modifySS);
					
					bs.addData("modifyDT", modifyDT);
					String CurrentStatus = reqParam.get("MB_Switch");
					String statusWording ="";
					if("A".equals(CurrentStatus))
					{
						// TODO i18n
						statusWording = i18n.getMsg("LB.D0329");//已啟用
					}
					else if("D".equals(CurrentStatus))
					{
						statusWording = i18n.getMsg("LB.D0328");//已停用
					}
					else
					{
						statusWording = i18n.getMsg("LB.D0322");//未啟用
					}
					bs.addData("currentStatus", CurrentStatus);
					bs.addData("statusWording", statusWording);
				}
				catch (Exception e)
				{
					log.error(" ", e);
				}
			}
		}
		catch(Exception e)
		{
			log.error("enable_mobile_result ERROR", e);
		}
		return bs;
	}
	
	
	public BaseResult getMsgName(String msgCode)
	{
		BaseResult bs = new BaseResult();
		try 
		{
			String msgName = "";
			boolean hasMsg = admMsgCodeDao.hasMsg(msgCode, null);
			log.debug("hasMsg >> {}", hasMsg);
			if(hasMsg)
			{
				// TODO E241做i18n
				msgName = admMsgCodeDao.errorMsg(msgCode);
			}
			log.debug("msgName >> {}", msgName);
			CodeUtil.convert2BaseResult(bs, null, msgCode, msgName);
		}
		catch(Exception e)
		{
			log.error("getMsgName error", e);
		}
		return bs;
	}
	
	
	/**
	 * NA80 金融卡線上申請/取消轉帳功能 結果頁
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult financial_card_apply_result(String cusidn, String type, String acn, String icSeq)
	{
		log.debug("financial_card_apply_result start");
		BaseResult bs = new BaseResult();
		
		// 處理晶片卡主帳號長度
		if(acn.length() > 11)
		{
			acn = acn.substring(acn.length() - 11);
		}
		
		NA80_REST_RQ rq = new NA80_REST_RQ();
		rq.setCUSIDN(cusidn);
		rq.setTYPE(type);
		rq.setACN(acn);
		rq.setICSEQ(icSeq);
		
		log.debug(ESAPIUtil.vaildLog("financial_card_apply_result params >> {}"+ CodeUtil.toJson(rq)));
		
		try 
		{
			bs = NA80_REST(rq);
			
			if(bs != null && bs.getResult()) 
			{
				// 頁面顯示文字	申請或取消
				String typeName = "01".equals(type) ? i18n.getMsg("LB.Apply") : i18n.getMsg("LB.Cancel");
				bs.addData("typeName", typeName);
			}
		}
		catch(Exception e)
		{
			log.error("financial_card_apply_result error", e);
		}
		return bs;
	}
	
	/**
	 * NA71 網路銀行交易密碼線上解鎖結果頁
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult unlock_ssl_pw_r(String cusidn, Map<String, String> reqParam)
	{
		log.trace(ESAPIUtil.vaildLog("unlock_ssl_pw_r >> {}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			String ACN = reqParam.get("ACNNO");
			if(ACN.length() > 11){
				ACN = ACN.substring(ACN.length() - 11);
			}
			log.trace(ESAPIUtil.vaildLog("ACN>>>>>{}"+ ACN));
			reqParam.put("CHIP_ACN", ACN);
			
			bs = NA71_REST(cusidn, reqParam);
			if(bs != null && bs.getResult()) {
				try
				{
					
				}
				catch (Exception e)
				{
					log.error(" ", e);
				}
			}
		}
		catch(Exception e)
		{
			log.error("unlock_ssl_pw_r ERROR", e);
		}
		return bs;
	}
	
	/**
	 * 	取消約定轉入帳號
	 * 
	 * @param params
	 * @return
	 */
	public BaseResult predesignated_account_cancellation(String cusidn) {

		log.trace("predesignated_account_cancellation");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			//轉入帳號
			bs = N921_REST(cusidn,ACCSET);

			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
								
				for (Map<String, String> row : rows) {
					String bnkcod = row.get("BNKCOD");
					String acn = row.get("ACN");

					//取得好記名稱
					String dpgoname = txntraccsetdao.getDpgoName(cusidn,acn,bnkcod);
					row.put("NAME",dpgoname);
					
					// 比對bank名稱
//					String bank = admbankdao.getBankName(bnkcod);
					String locale = LocaleContextHolder.getLocale().toString();
					String bank = "";
					// 比對bank名稱
					if(locale.equals("zh_CH")) {
						bank = admbankdao.getBankChName(bnkcod);
					}
					else if(locale.equals("en")) {
						bank = admbankdao.getBankEnName(bnkcod);
					}
					else {
						bank = admbankdao.getBankName(bnkcod);
					}
					bnkcod = bnkcod + "-" + bank;
					row.put("BNKCOD", bnkcod);
					
				}
			}
			//
			
			bs.addData("nodataMsg", i18n.getMsg("LB.Check_no_data"));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("predesignated_account_cancellation error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * 	取消約定轉入帳號_確認
	 * 
	 * @param params
	 * @return
	 */
	public BaseResult predesignated_account_cancellation_s(String cusidn, Map<String, String> reqParam) {

		log.trace(ESAPIUtil.vaildLog("predesignated_account_cancellation_s~~~{}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			log.trace(ESAPIUtil.vaildLog("ROWDATA: {}"+ reqParam.get("r1")));
		//拆解頁面值
			String value = reqParam.get("r1");
			if(value != null) {
			value = value.substring(1, value.length() - 1); // remove curly brackets
			log.trace(ESAPIUtil.vaildLog("ROWDATA.value:"+ value));
			String[] keyValuePairs = value.split(","); // split the string to creat key-value pairs
			log.trace(ESAPIUtil.vaildLog("ROWDATA.keyValuePairs:"+ keyValuePairs));

			Map<String, String> map = new HashMap<>();
			for (String pair : keyValuePairs) // iterate over the pairs
			{
				String[] entry = pair.split("="); // split the pairs to get key and value

				String mapKey = "";
				String MapValue = "";
				log.trace("ROWDATA.entry.length: " + entry.length);
				if (entry.length == 2) {
					mapKey = "".equals(entry[0].trim()) ? "" : entry[0].trim();
					MapValue = "".equals(entry[1].trim()) ? "" : entry[1].trim();
				} else {
					mapKey = entry[0].trim();
				}
				log.trace(ESAPIUtil.vaildLog("ROWDATA.mapKey:{}"+ mapKey));
				log.trace(ESAPIUtil.vaildLog("ROWDATA.MapValue:{}"+ MapValue));
				map.put(mapKey, MapValue); // add them to the hashmap and trim whitespaces
			}

			log.debug(ESAPIUtil.vaildLog("ROWDATA:{}"+CodeUtil.toJson(map)));
			//取出第一頁的值
			//銀行代號
			String bankcod = map.get("BNKCOD");
			if(bankcod != null) {
				bankcod = bankcod.substring(0,3);
			}
			bs.addData("BANKCOD", bankcod);
			//幣別
			String CCY = map.get("CCY");
			bs.addData("CCY", CCY);
			
			//日期
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String today = sdf.format(date);
			log.trace("today>>>{}",today);
			
			//控制是否今日，0是今日1是今日之後
			int t = 0;
			// 筆數紀錄
			int COUNT = 0;
			
			//取得台/外幣預約資料
			//台幣轉入帳號
			String acn = map.get("ACN");
			log.trace(ESAPIUtil.vaildLog("TWACN>>{}"+ acn));
			//外幣跨行帳號
			String f_acn = map.get("BENACC");
			log.trace(ESAPIUtil.vaildLog("f_acn>>{}"+ f_acn));
			
			List<Map<String, Object>> data = new ArrayList<>();
			//台幣轉入帳號
			if(acn != null && !"".equals(acn)) {
				//取台幣預約Table
				List<TXNTWSCHPAY> TXNTWSCHPAY = txntwschpaydao.getAcnDptxstatus(cusidn,acn);
				if (TXNTWSCHPAY != null) {
					for (TXNTWSCHPAY each : TXNTWSCHPAY) {
						// 將po轉成map
						Map<String, Object> eachMap = ObjectConvertUtil.object2Map(each);

						// 生效日
						String dpfdate = (String) eachMap.get("DPFDATE");
						eachMap.put("DPFDATE", DateUtil.convertDate(2, dpfdate, "yyyyMMdd", "yyyy/MM/dd"));
						// 截止日
						String dptdate = (String) eachMap.get("DPTDATE");
						eachMap.put("DPTDATE", DateUtil.convertDate(2, dptdate, "yyyyMMdd", "yyyy/MM/dd"));
						// 如果兩個相同則只顯示一個
						if (dpfdate.equals(dptdate)) {
							eachMap.put("DPTDATE", "");
						}
						// 單次預約/循環預約
						String dptxtype = (String) eachMap.get("DPTXTYPE");
						if ("S".equals(dptxtype)) {
							eachMap.put("DPPERMTDATE", i18n.getMsg("LB.X1796"));//特定日期
						} else {
							String dppermtdate = (String) eachMap.get("DPPERMTDATE");
							eachMap.put("DPPERMTDATE", i18n.getMsg("LB.X1797") + dppermtdate + i18n.getMsg("LB.Day"));//固定每月  日
						}
						// 下次轉帳日
						String dpschno = (String) eachMap.get("DPSCHNO");
						String dpnextdate = "";
						if(today.equals(dpschno)) {
							dpnextdate = txntwschpaydatadao.getDpschtxToday(cusidn, dpschno);
							t = 1;
						}else {
							dpnextdate = txntwschpaydatadao.getDpschtxdate(cusidn, dpschno);
							t = 0;
						}
						if (dpnextdate == null || dpnextdate == "") {
							// 如果是空就是生效日
							eachMap.put("DPNEXTDATE", DateUtil.convertDate(2, dpfdate, "yyyyMMdd", "yyyy/MM/dd"));
						} else {
							eachMap.put("DPNEXTDATE", DateUtil.convertDate(2, dpnextdate, "yyyyMMdd", "yyyy/MM/dd"));
						}
						// 比對bank名稱
						String dpsvbh = (String) eachMap.get("DPSVBH");
//						String bank = admbankdao.getBankName(dpsvbh);
						String locale = LocaleContextHolder.getLocale().toString();
						String bank = "";
						// 比對bank名稱
						if(locale.equals("zh_CH")) {
							bank = admbankdao.getBankChName(dpsvbh);
						}
						else if(locale.equals("en")) {
							bank = admbankdao.getBankEnName(dpsvbh);
						}
						else {
							bank = admbankdao.getBankName(dpsvbh);
						}
						dpsvbh = dpsvbh + "-" + bank;
						eachMap.put("DPSVBH", dpsvbh);
						// 金額格式處理
						eachMap.put("DPTXAMTS", NumericUtil.fmtAmount((String) eachMap.get("DPTXAMT"), 2));
						// 交易類別
						String adopid = (String) eachMap.get("ADOPID");
						String idtype = nb3sysopdao.findAdopid(adopid);
						if (idtype != "") {
							eachMap.put("TXTYPE", idtype);
						} else {
							eachMap.put("TXTYPE", "");
						}
						// 交易機制
						String dptxcode = (String) eachMap.get("DPTXCODE");
						eachMap.get("DPTXCODE");
						if ("1".equals(dptxcode)) {
							eachMap.put("DPTXCODES", i18n.getMsg("LB.D0180"));	//電子簽章(i-key)
						}else if("7".equals(dptxcode)) {
							eachMap.put("DPTXCODES", "裝置推播認證");	//裝置推播認證(IDGATE)	
						} else {
							eachMap.put("DPTXCODES", i18n.getMsg("LB.D0179"));//交易密碼(SSL)
						}

						data.add((Map<String, Object>) eachMap);
					}
					log.trace(ESAPIUtil.vaildLog("data>>{}"+data));
					bs.addData("REC", data);
				}
				bs.addData("twAcn", acn);
				bs.setResult(true);
			//外幣轉入帳號
			}else if(f_acn != null && !"".equals(f_acn)){
				//取外幣預約Table
				List<TXNFXSCHPAY> TXNFXSCHPAY = txnfxschpaydao.getAcnFxtxstatus(cusidn,f_acn);
				if(TXNFXSCHPAY!=null) {
					for(TXNFXSCHPAY each:TXNFXSCHPAY) {
						//將po轉成map
						Map<String, Object> eachMap = ObjectConvertUtil.object2Map(each);
									
						//生效日
						String fxfdate = (String) eachMap.get("FXFDATE");
						eachMap.put("FXFDATE",DateUtil.convertDate(2,fxfdate ,"yyyyMMdd", "yyyy/MM/dd"));
						//截止日
						String fxtdate = (String) eachMap.get("FXTDATE");
						eachMap.put("FXTDATE",DateUtil.convertDate(2,fxtdate ,"yyyyMMdd", "yyyy/MM/dd"));
							//如果兩個相同則只顯示一個
							if(fxfdate.equals(fxtdate)) {
								eachMap.put("FXTDATE","");
							}
						//單次預約/循環預約
						String fxtxtype = (String) eachMap.get("FXTXTYPE");
						if("S".equals(fxtxtype)) {					
							eachMap.put("FXPERMTDATE", i18n.getMsg("LB.X1796"));//特定日期					
						}else {
							String fxpermtdate = (String) eachMap.get("FXPERMTDATE");
							eachMap.put("FXPERMTDATE", i18n.getMsg("LB.X1797")+ fxpermtdate +i18n.getMsg("LB.Day"));//固定每月 日
						}
						//下次轉帳日
						String fxschno = (String) eachMap.get("FXSCHNO");
						String fxschtxdate = "";
						if(today.equals(fxschno)) {
							fxschtxdate = txnfxschpaydatadao.getFxschtxToday(cusidn,fxschno);	
							t = 1;
						}else {
							fxschtxdate = txnfxschpaydatadao.getFxschtxOther(cusidn,fxschno);
							t = 0;
						}
							if(fxschtxdate == null || fxschtxdate == "") {
								//如果是空就是生效日
								eachMap.put("FXNEXTDATE", DateUtil.convertDate(2,fxfdate ,"yyyyMMdd", "yyyy/MM/dd"));
							}else {
								eachMap.put("FXNEXTDATE", DateUtil.convertDate(2,fxschtxdate,"yyyyMMdd", "yyyy/MM/dd"));									
							}
					//TODO 暫時沿用TW寫法，可能不是這樣做
						//比對bank名稱
						String fxsvbh = (String) eachMap.get("FXSVBH");
//						String bank = admbankdao.getBankName(fxsvbh);
						String locale = LocaleContextHolder.getLocale().toString();
						String bank = "";
						// 比對bank名稱
						if(locale.equals("zh_CH")) {
							bank = admbankdao.getBankChName(fxsvbh);
						}
						else if(locale.equals("en")) {
							bank = admbankdao.getBankEnName(fxsvbh);
						}
						else {
							bank = admbankdao.getBankName(fxsvbh);
						}
						if(bank != "") {
							fxsvbh = fxsvbh + "-" + bank;						
							eachMap.put("FXSVBH", fxsvbh);				
						}else {
							eachMap.put("FXSVBH", "");										
						}				
						//轉出金額格式處理
						String FXWDAMT = (String) eachMap.get("FXWDAMT");
						if("".equals(FXWDAMT) || "0.00".equals(FXWDAMT) || "0".equals(FXWDAMT)) {
							eachMap.put("FXWDAMTS","");
						}else {
							eachMap.put("FXWDAMTS",NumericUtil.fmtAmount(FXWDAMT,2));						
						}
						//轉入金額幣別格式處理
						String FXSVAMT = (String) eachMap.get("FXSVAMT");
						if("".equals(FXSVAMT) || "0.00".equals(FXSVAMT) || "0".equals(FXSVAMT)) {
							eachMap.put("FXSVAMTS","");					
						}else {
							eachMap.put("FXSVAMTS",NumericUtil.fmtAmount((String) eachMap.get("FXSVAMT"),2));						
						}
						//交易類別
						String adopid = (String) eachMap.get("ADOPID");
						String idtype = nb3sysopdao.findAdopid(adopid);
						if(idtype != "") {
							eachMap.put("TXTYPE", idtype);					
						}else {
							eachMap.put("TXTYPE", "");
						}
						//交易機制
						String fxtxcode = (String) eachMap.get("FXTXCODE");
						if("1".equals(fxtxcode)) {
							eachMap.put("FXTXCODES",i18n.getMsg("LB.D0180"));//電子簽章(i-key)		
						}else if("7".equals(fxtxcode)) {
							eachMap.put("FXTXCODES", "裝置推播認證");	//裝置推播認證(IDGATE)	
						}else {
							eachMap.put("FXTXCODES",i18n.getMsg("LB.D0179"));//交易密碼(SSL)								
						}
						//狀態
						String status = (String) eachMap.get("FXTXSTATUS");
						if("0".equals(status)) {
							eachMap.put("STATUS",status);
							eachMap.put("FXTXSTATUS",i18n.getMsg("LB.X1798"));//已取消預約
						}else if("1".equals(status)) {
							eachMap.put("STATUS",status);
							eachMap.put("FXTXSTATUS", i18n.getMsg("LB.reservation"));//預約中
						}
						
						data.add((Map<String, Object>) eachMap);
					}
					log.trace(ESAPIUtil.vaildLog("data>>{}"+data));
					bs.addData("REC", data);
				}
					bs.addData("fxAcn", f_acn);
					bs.setResult(true);
			}else {
				bs.setResult(false);
			}
			
			COUNT = data.size();
			log.trace("COUNT>>{}", COUNT);
			map.put("COUNT", String.valueOf(COUNT));
			//控制是台幣或外幣
			if(acn != null) {
				bs.addData("ACN", acn);
			}else {
				bs.addData("ACN", f_acn);				
			}
			//控制是否今天
			bs.addData("isToday", t);
			bs.addData("dataSet", map);
			
			bs.addData("COUNT", COUNT);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("predesignated_account_cancellation_s error >> {}",e);	
		}
		return bs;
	}
	
	/**
	 * 	取消約定轉入帳號_結果
	 * 
	 * @param params
	 * @return
	 */
	public BaseResult predesignated_account_cancellation_r(String cusidn , Map<String, String> reqParam) {

		log.trace(ESAPIUtil.vaildLog("predesignated_account_cancellation_r~~~{}"+CodeUtil.toJson(reqParam)));
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			//約定轉入帳號
			String acn = reqParam.get("ACN");
			log.trace(ESAPIUtil.vaildLog("Acn>>{}"+ acn));
			//銀行
			String bnkcod = reqParam.get("BNKCOD");
			log.trace(ESAPIUtil.vaildLog("bnkcod>>{}"+ bnkcod));
			//幣別
			String ccy = reqParam.get("CCY");
			log.trace(ESAPIUtil.vaildLog("ccy>>{}"+ ccy));
			//判斷台/外幣帳戶
			if(ccy == null || "".equals(ccy)) {				
				bs = N880_REST(cusidn ,acn, reqParam);
				if("0".equals(bs.getMsgCode())) {
					TXNTRACCSET qresult1 = txntraccsetdao.findByACN(cusidn,acn);
					List<TXNTWSCHPAY> qresult2 = txntwschpaydao.getAcnDptxstatus(cusidn,acn);
					try{   
						//取消資料庫轉入帳號
						if(qresult1 != null) {
							daoService.updateDpgoName(cusidn, acn, bnkcod, null, "0");
//							txntraccsetdao.cancelAcn(cusidn,acn,bnkcod);
							log.debug("取消約定帳號");	
						}else {
							log.debug("無該轉入約定帳號");
						}
						//取消該帳號預約交易資料
						if(qresult2 != null && qresult2.size() != 0) {
							txntwschpaydao.cancelAcn(cusidn,acn,bnkcod);
							log.debug(ESAPIUtil.vaildLog("更新成功"+qresult2));
						}else {
							log.debug("無該預約資料{}");
						}
					}catch(Exception e){
						log.trace("{}",e);
					}			
				}				
			}else {
				bs = F037_REST(cusidn, acn, ccy, reqParam);
				if("0".equals(bs.getMsgCode())) {
					TXNTRACCSET qresult1 = txntraccsetdao.findByACN(cusidn,acn);
					List<TXNFXSCHPAY> qresult2 = txnfxschpaydao.getAcnFxtxstatus(cusidn,acn);
					try{   
						//取消資料庫轉入帳號
						if(qresult1 != null) {
							txntraccsetdao.cancelAcn(cusidn,acn,bnkcod);
							log.debug("取消約定帳號");	
						}else {
							log.debug("無該轉入約定帳號");
						}
						//取消該帳號預約交易資料
						if(qresult2 != null && qresult2.size() != 0) {
							txnfxschpaydao.cancelAcn(cusidn,acn,bnkcod);
							log.debug(ESAPIUtil.vaildLog("更新成功{}"+CodeUtil.toJson(qresult2)));
						}else {
							log.debug("無該預約資料{}");
						}
					}catch(Exception e){
						log.trace("{}",e);
					}			
				}	
			}
			bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
			bs.addData("MSG","LB.D0237");
			bs.addData("CUSIDN", cusidn);
			bs.addData("ACN", acn);
			bs.addData("BNKCOD", bnkcod);
			bs.addData("CCY", ccy);


			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("predesignated_account_cancellation_r error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * 取得國別
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	public List<Map<String, String>> get_country_name() {
		log.trace("get_country_name...>>>");
		
		List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
		
		BaseResult bs = new BaseResult();
		try {
			List<ADMCOUNTRY> ADMCOUNTRY = admcountrydao.getAllCountry();
			if (ADMCOUNTRY != null) {
				for (ADMCOUNTRY each : ADMCOUNTRY) {
					// 將po轉成map
					Map<String, Object> eachMap = ObjectConvertUtil.object2Map(each);
					Map<String, String> result = new HashMap();
					String country = (String)eachMap.get("ADCTRYNAME");
					String countryen = (String)eachMap.get("ADCTRYENGNAME");
					String countrycn = (String)eachMap.get("ADCTRYCHSNAME");
					String countryabb = (String)eachMap.get("ADCTRYCODE");
					if(!countryabb.equals(null) && !countryabb.equals("")) {
						countryabb = countryabb.replace(" ", "");
					}
					result.put("COUNTRY", country);
					result.put("COUNTRYEN", countryen);
					result.put("COUNTRYCN", countrycn);
					result.put("COUNTRYABB", countryabb);
					resultList.add(result);
				}
			}
			log.trace(ESAPIUtil.vaildLog("resultList>>>"+resultList));
		} 
		catch (Exception e) {
			log.error("get_country_name.error: {}", e);
		}
		
		return resultList;
	}
	
	/**
	 * 清除暫存session的資料，不可清除使用者身份資料
	 * 
	 * @param model
	 */
	public void cleanSession(Model model)
	{
		// 清除切換語系
		SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null);
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null);
//		20191009 hugo 避免轉帳或繳納本行信用卡款等相關功能 且有用到SessionUtil.BACK_DATA
//		, 對按下去此功能再回到轉帳輸入頁面導致空白畫面問題
		// 清除回上一頁
		SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, null);
//		SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, "");
		// 清除print
		SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, null);
		// 換頁暫存資料
		SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, null);
		// 線上申請新臺幣存款帳戶結清銷戶暫存資料
		SessionUtil.addAttribute(model, SessionUtil.CLOSING_TW_ACCOUNT, null);
	}
	
	
	
	public BaseResult idGate(String cusidn)
	{
		log.trace("idGate service ");
		BaseResult bs = new BaseResult();
		try
		{
			
//			查詢tabel 秀出目前綁定或申請的裝置 
			List<QUICKLOGINMAPPING> data = quickLoginMappingDao.getDataByCusidn(cusidn);
			bs.setResult(true);
			bs.setMessage("0", i18n.getMsg("LB.X1805"));//查詢成功
			bs.setData(data);
		}
		catch (Exception e)
		{
			log.error("idGate Error >>{}", e);
		}
		return bs;
	}
	
	
	
	public BaseResult IdGateN915(String cusidn, Map<String, String> reqParam)
	{
		log.trace("IdGateN915 service ");
		BaseResult bs = new BaseResult();
		try
		{
			log.info("idgate_mode>>{}",idgate_mode);
			if("0".equals(idgate_mode)) {
				bs.setResult(Boolean.TRUE);
				bs.setMsgCode("0");
				bs.setMessage("ok");
				bs.setData(new HashMap<String,String>().put("opncode", CodeUtil.getStringRandom(8)));
//				bs.setData(CodeUtil.getStringRandom(8));
				return bs;
			}
//			要先驗證簡訊
			reqParam.put("CUSIDN", cusidn);
			reqParam.put("FuncType", "1");
//			簡訊移除
			bs.setResult(Boolean.TRUE);
			
//			bs=SmsOtp_REST(reqParam);
			if(bs.getResult()) {
				log.info("簡訊驗證通過...");
				String applyTime = DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss");
				bs = IdGateN915_REST(reqParam, null);
				if(bs.getResult()) {
					Map map = (Map) bs.getData();
					bs.setData(map.get("data"));
					bs.addData("applyTime", applyTime);
				}
			}else {
				log.error("簡訊驗證失敗...");
			}
			
		}
		catch (Exception e)
		{
			log.error("IdGateN915 Error >>{}", e);
		}
		return bs;
	}

	public BaseResult changeIDGateStatus(String cusidn, String idGateId , String deviceId ,String action ,String deviceName ) {
		QUICKLOGINMAPPING po = null;
		BaseResult bs = null;
		Boolean result = Boolean.FALSE;
		Map<String, String> reqParam =null;
		try {
			bs = new BaseResult(); 
			po = quickLoginMappingDao.getDataByInputData(cusidn, idGateId, deviceId);
			if(po ==null) {
				log.warn(ESAPIUtil.vaildLog("查無資料 ...cusidn>> "+cusidn+" ,idGateId>> "+idGateId));
				log.warn(ESAPIUtil.vaildLog("deviceId>> "+deviceId));
				bs.setMessage("查無資料");
				return bs;
			}
//		註銷
			if("2".equals(action)) {
//			要先打IDGATE Server 註銷IDGATE ID
//				quickLoginMappingDao.remove(po);
				log.info("idgate_mode>>{}",idgate_mode);
				if("0".equals(idgate_mode)) {
					bs.setResult(Boolean.TRUE);
					bs.setMsgCode("0");
					bs.setMessage("ok");
					return bs;
				}
				
				reqParam = new HashMap<String,String>();
				
				reqParam.put("CUSIDN", cusidn);
				reqParam.put("IDGATEID", idGateId);
				reqParam.put("DEVICEID", deviceId);
				reqParam.put("DEVICENAME", deviceName);
				bs = IdGateCancel_REST(reqParam, null);
//				bs.setMsgCode("0");
//				bs.setResult(Boolean.TRUE);
//				bs.setMessage("ok");
				return bs;
			}
			
//			修改
			if("3".equals(action)) {
//			要先打IDGATE Server 註銷IDGATE ID
//				quickLoginMappingDao.remove(po);
				log.info("idgate_mode>>{}",idgate_mode);
				if("0".equals(idgate_mode)) {
					bs.setResult(Boolean.TRUE);
					bs.setMsgCode("0");
					bs.setMessage("ok");
					return bs;
				}
				
				reqParam = new HashMap<String,String>();
				
				reqParam.put("CUSIDN", cusidn);
				reqParam.put("IDGATEID", idGateId);
				reqParam.put("DEVICEID", deviceId);
				reqParam.put("DEVICENAME", deviceName);
				reqParam.put("TYPE", "");
				bs = IdGateUpdate_REST(reqParam, null);
//				bs.setMsgCode("0");
//				bs.setResult(Boolean.TRUE);
//				bs.setMessage("ok");
				return bs;
			}
//		啟用
			if("0".equals(action)) {
//			TODO　要先打IDGATE Server 解鎖IDGATE ID?
				po.setDEVICESTATUS("0");
			}
//		停用
			if("1".equals(action)) {
//			TODO　要先打IDGATE Server 鎖定IDGATE ID?
				po.setDEVICESTATUS("1");
			}
//			quickLoginMappingDao.update(po);
			bs.setMsgCode("0");
			bs.setResult(Boolean.TRUE);
			bs.setMessage("ok");
			return bs;
		} catch (Exception e) {
			log.error("Exception.error{}", e);
		}
		
		return bs;
		
	}
	
	
	public BaseResult IdGateN915_REST(Map<String, String> reqParam, String ms_Channel) {
        log.info("IdGateN915_REST");
        BaseResult bs = null;
        IdGateN915_REST_RQ rq = null;
        IdGateN915_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new IdGateN915_REST_RQ();
            reqParam.put("ADOPIDAML", reqParam.get("ADOPID"));
            rq = CodeUtil.objectCovert(IdGateN915_REST_RQ.class, reqParam);
            rs = restutil.send(rq, IdGateN915_REST_RS.class, rq.getAPI_Name(rq.getClass(), ms_Channel));
            log.trace(ESAPIUtil.vaildLog("IdGateN915_REST_RS>>{}"+rs));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("IdGateN915_REST is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
            
        }

        this.getMessageByMsgCode(bs);
        return bs;
    }
	public BaseResult IdGateCancel_REST(Map<String, String> reqParam, String ms_Channel) {
		log.info("IdGateCancel_REST");
		BaseResult bs = null;
		IdGateMappingDelete_REST_RQ rq = null;
		IdGateMappingDelete_REST_RS rs = null;
		try {
			bs = new BaseResult();
			rq = new IdGateMappingDelete_REST_RQ();
			reqParam.put("ADOPIDAML", reqParam.get("ADOPID"));
			rq = CodeUtil.objectCovert(IdGateMappingDelete_REST_RQ.class, reqParam);
			rs = restutil.send(rq, IdGateMappingDelete_REST_RS.class, rq.getAPI_Name(rq.getClass(), ms_Channel));
			log.trace(ESAPIUtil.vaildLog("IdGateMappingDelete_REST_RS>>{}"+rs));
			if (rs != null) {
				CodeUtil.convert2BaseResult(bs, rs);
			} else {
				log.error("IdGateCancel_REST is null");
				bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
				
			}
		} catch (Exception e) {
			log.error("", e);
			bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
			
		}
		
		this.getMessageByMsgCode(bs);
		return bs;
	}

	public BaseResult IdGateUpdate_REST(Map<String, String> reqParam, String ms_Channel) {
		log.info("IdGateCancel_REST");
		BaseResult bs = null;
		IdGateMappingUpdate_REST_RQ rq = null;
		IdGateMappingUpdate_REST_RS rs = null;
		try {
			bs = new BaseResult();
			rq = new IdGateMappingUpdate_REST_RQ();
			rq = CodeUtil.objectCovert(IdGateMappingUpdate_REST_RQ.class, reqParam);
			rs = restutil.send(rq, IdGateMappingUpdate_REST_RS.class, rq.getAPI_Name(rq.getClass(), ms_Channel));
			log.trace(ESAPIUtil.vaildLog("IdGateMappingUpdate_REST_RS>>{}"+rs));
			if (rs != null) {
				CodeUtil.convert2BaseResult(bs, rs);
			} else {
				log.error("IdGateUpdate_REST is null");
				bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
				
			}
		} catch (Exception e) {
			log.error("", e);
			bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
			
		}
		
		this.getMessageByMsgCode(bs);
		return bs;
	}
	
	/**
	 *  數位帳戶查詢 (抓第一筆)
	 * @param cusidn
	 * @param type "01" 數一、"02"  數二、"03" 數三
	 * @return
	 */
	public BaseResult getDigitalAccount(String cusidn, String type){
		BaseResult bs = new BaseResult();		
		bs.setResult(Boolean.FALSE);
		try{
			ArrayList<String> digacns = new ArrayList<String>();
			BaseResult bs_N961 = N961_REST(cusidn);
			
			if (bs_N961!=null && bs_N961.getResult()) {
				Map<String, Object> tmpData = (Map) bs_N961.getData();				
				if (tmpData!=null) {
    				List<Map<String, String>> rec = (List<Map<String,String>>) tmpData.get("REC");    				
    				if (rec!=null) {
	        			for (Map<String, String> map :rec) {
	        				if ("03".equals(map.get("DIGTYPE") )) {
	        					digacns.add(map.get("DIGACN"));
	        				}
	        			}
    				}
				}
			}
			
			if (digacns.size() > 0) {
				bs.addData("DIGACN", digacns.get(0));
				bs.setResult(Boolean.TRUE);
			}		
		}
		catch(Exception e){
			//e.printStackTrace();
			log.error("getN961Data ERROR>>",e);
		}
		return bs;
	}
	
	public BaseResult upgradeDigitalAccountClass3(String cusidn, String acn){
		BaseResult bs = new BaseResult();		
		bs.setResult(Boolean.FALSE);
		try{
			bs = N206_REST(cusidn, acn);
			
			if (bs != null && bs.getResult()) {
				bs.addData("CUSIDN", cusidn);
				bs.addData("DIGACN", acn);
			}				
		}
		catch(Exception e){
			//e.printStackTrace();
			log.error("getN961Data ERROR>>",e);
		}
		return bs;
	}
//	quickLoginMappingDao
	
}
