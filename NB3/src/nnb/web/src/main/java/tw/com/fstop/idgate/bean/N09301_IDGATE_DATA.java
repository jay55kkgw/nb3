package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N09301_IDGATE_DATA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6894775196512786815L;

	@SerializedName(value = "CUSIDN")
	private String CUSIDN;
	
	@SerializedName(value = "CUSIDN1")
	private String CUSIDN1;
	
	@SerializedName(value = "TRNTYP")
	private String TRNTYP;
	
	@SerializedName(value = "TRNCOD")
	private String TRNCOD;
	
	@SerializedName(value = "ACN")
	private String ACN;
	
	@SerializedName(value = "SVACN")
	private String SVACN;
	
	@SerializedName(value = "AMT_06")
	private String AMT_06;
	
	@SerializedName(value = "DATE_06")
	private String DATE_06;
	
	@SerializedName(value = "AMT_16")
	private String AMT_16;
	
	@SerializedName(value = "DATE_16")
	private String DATE_16;
	
	@SerializedName(value = "AMT_26")
	private String AMT_26;
	
	@SerializedName(value = "DATE_26")
	private String DATE_26;
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getCUSIDN1() {
		return CUSIDN1;
	}
	public void setCUSIDN1(String cUSIDN1) {
		CUSIDN1 = cUSIDN1;
	}
	public String getTRNTYP() {
		return TRNTYP;
	}
	public void setTRNTYP(String tRNTYP) {
		TRNTYP = tRNTYP;
	}
	public String getTRNCOD() {
		return TRNCOD;
	}
	public void setTRNCOD(String tRNCOD) {
		TRNCOD = tRNCOD;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getSVACN() {
		return SVACN;
	}
	public void setSVACN(String sVACN) {
		SVACN = sVACN;
	}
	public String getAMT_06() {
		return AMT_06;
	}
	public void setAMT_06(String aMT_06) {
		AMT_06 = aMT_06;
	}
	public String getDATE_06() {
		return DATE_06;
	}
	public void setDATE_06(String dATE_06) {
		DATE_06 = dATE_06;
	}
	public String getAMT_16() {
		return AMT_16;
	}
	public void setAMT_16(String aMT_16) {
		AMT_16 = aMT_16;
	}
	public String getDATE_16() {
		return DATE_16;
	}
	public void setDATE_16(String dATE_16) {
		DATE_16 = dATE_16;
	}
	public String getAMT_26() {
		return AMT_26;
	}
	public void setAMT_26(String aMT_26) {
		AMT_26 = aMT_26;
	}
	public String getDATE_26() {
		return DATE_26;
	}
	public void setDATE_26(String dATE_26) {
		DATE_26 = dATE_26;
	}
}
