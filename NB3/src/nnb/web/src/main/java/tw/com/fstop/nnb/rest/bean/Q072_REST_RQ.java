package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class Q072_REST_RQ extends BaseRestBean_CC implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5783752614234688293L;

    private String CARDNUM;// 銷帳編號

    private String CUSIDN;// 統一編號

    private String BankID;// 銀行代碼

    private String trin_acn;// 轉出帳號

    private String AMOUNT;// 繳款金額

    private String ipayamt;// 繳款金額

    private String CustEmail;//

    private String IP;//

    private String SessionId;//

    private String FEE;

    private String TrnsCode;//

    public String getCARDNUM() {
        return CARDNUM;
    }

    public void setCARDNUM(String cARDNUM) {
        CARDNUM = cARDNUM;
    }

    public String getCUSIDN() {
        return CUSIDN;
    }

    public void setCUSIDN(String cUSIDN) {
        CUSIDN = cUSIDN;
    }

    public String getBankID() {
        return BankID;
    }

    public void setBankID(String bankID) {
        BankID = bankID;
    }

    public String getTrin_acn() {
        return trin_acn;
    }

    public void setTrin_acn(String trin_acn) {
        this.trin_acn = trin_acn;
    }

    public String getAMOUNT() {
        return AMOUNT;
    }

    public void setAMOUNT(String aMOUNT) {
        AMOUNT = aMOUNT;
    }

    public String getIpayamt() {
        return ipayamt;
    }

    public void setIpayamt(String ipayamt) {
        this.ipayamt = ipayamt;
    }

    public String getCustEmail() {
        return CustEmail;
    }

    public void setCustEmail(String custEmail) {
        CustEmail = custEmail;
    }


    public String getIP() {
        return IP;
    }

    public void setIP(String iP) {
        IP = iP;
    }

    public String getSessionId() {
        return SessionId;
    }

    public void setSessionId(String sessionId) {
        SessionId = sessionId;
    }

    public String getFEE() {
        return FEE;
    }

    public void setFEE(String fEE) {
        FEE = fEE;
    }

    public String getTrnsCode() {
        return TrnsCode;
    }

    public void setTrnsCode(String trnsCode) {
        TrnsCode = trnsCode;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }



}
