package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class NA011_REST_RS extends BaseRestBean implements Serializable {

	private static final long serialVersionUID = -1221909546694494157L;
	private String DOCFLOW;			// 案件狀態
	private String PROJECT;			// 授信項目
	private String CMRECNUM;		// 資料筆數
	private String CMQTIME;			// 查詢時間
	private String APPLDATE;		// 核准日期(民國年)
	private String LIMITM;			// 授信期限
	private String OTHKEY;			// 案件編號
	private String ERRCODE;
	private String RATENOTE;		// 費率
	private String APPRAMT;			// 授信金額
	private String ACCOUNT;			// 空值:為未撥貸 有值:已撥貸
	private String RTNWAY;			// 撥貸及償還辦法
	private String COSTNOTE;		// 利率
	private LinkedList<NA011_REST_RSDATA> REC;
	
	public String getDOCFLOW() {
		return DOCFLOW;
	}
	public void setDOCFLOW(String dOCFLOW) {
		DOCFLOW = dOCFLOW;
	}
	public String getPROJECT() {
		return PROJECT;
	}
	public void setPROJECT(String pROJECT) {
		PROJECT = pROJECT;
	}
	public String getCMRECNUM() {
		return CMRECNUM;
	}
	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getAPPLDATE() {
		return APPLDATE;
	}
	public void setAPPLDATE(String aPPLDATE) {
		APPLDATE = aPPLDATE;
	}
	public String getLIMITM() {
		return LIMITM;
	}
	public void setLIMITM(String lIMITM) {
		LIMITM = lIMITM;
	}
	public String getOTHKEY() {
		return OTHKEY;
	}
	public void setOTHKEY(String oTHKEY) {
		OTHKEY = oTHKEY;
	}
	public String getERRCODE() {
		return ERRCODE;
	}
	public void setERRCODE(String eRRCODE) {
		ERRCODE = eRRCODE;
	}
	public String getRATENOTE() {
		return RATENOTE;
	}
	public void setRATENOTE(String rATENOTE) {
		RATENOTE = rATENOTE;
	}
	public String getAPPRAMT() {
		return APPRAMT;
	}
	public void setAPPRAMT(String aPPRAMT) {
		APPRAMT = aPPRAMT;
	}
	public String getACCOUNT() {
		return ACCOUNT;
	}
	public void setACCOUNT(String aCCOUNT) {
		ACCOUNT = aCCOUNT;
	}
	public String getRTNWAY() {
		return RTNWAY;
	}
	public void setRTNWAY(String rTNWAY) {
		RTNWAY = rTNWAY;
	}
	public String getCOSTNOTE() {
		return COSTNOTE;
	}
	public void setCOSTNOTE(String cOSTNOTE) {
		COSTNOTE = cOSTNOTE;
	}
	public LinkedList<NA011_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<NA011_REST_RSDATA> rEC) {
		REC = rEC;
	}

}
