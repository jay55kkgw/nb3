package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;


public class N900_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5370773529503299761L;
	
	@SerializedName(value = "newZIP")
	private String newZIP;
	@SerializedName(value = "newADDR1")
	private String newADDR1;
	@SerializedName(value = "newHOMEPHONE")
	private String newHOMEPHONE;
	@SerializedName(value = "newOFFICEPHONE")
	private String newOFFICEPHONE;
	@SerializedName(value = "newOFFICEEXT")
	private String newOFFICEEXT;
	
	@SerializedName(value = "newMOBILEPHONE")
	private String newMOBILEPHONE;

	
	@Override
	public Map<String, String> coverMap(){
		LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", "變更信用卡帳單地址／電話");
		result.put("交易類別", "-");
		result.put("帳單地址", this.newZIP +" "+ this.newADDR1);
		result.put("住家電話", this.newHOMEPHONE);
		result.put("公司電話 ", this.newOFFICEPHONE);
		result.put("分　　機", this.newOFFICEEXT);
		result.put("行動電話", this.newMOBILEPHONE);
		return result;
	}


}
