package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class MTPCHANGEACCOUNT_IDGATE_DATA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -762951082705400880L;

	@SerializedName(value = "cusidn")
	private String cusidn;
	@SerializedName(value = "mobilephone")
	private String mobilephone;
	@SerializedName(value = "systemflag")
	private String systemflag;
	@SerializedName(value = "txacn1")
	private String txacn1;
	@SerializedName(value = "txacn2")
	private String txacn2;
	@SerializedName(value = "binddefault")
	private String binddefault;
	public String getBinddefault() {
		return binddefault;
	}
	public void setBinddefault(String binddefault) {
		this.binddefault = binddefault;
	}
	public String getCusidn() {
		return cusidn;
	}
	public void setCusidn(String cusidn) {
		this.cusidn = cusidn;
	}
	public String getMobilephone() {
		return mobilephone;
	}
	public void setMobilephone(String mobilephone) {
		this.mobilephone = mobilephone;
	}
	public String getSystemflag() {
		return systemflag;
	}
	public void setSystemflag(String systemflag) {
		this.systemflag = systemflag;
	}
	public String getTxacn1() {
		return txacn1;
	}
	public void setTxacn1(String txacn1) {
		this.txacn1 = txacn1;
	}
	public String getTxacn2() {
		return txacn2;
	}
	public void setTxacn2(String txacn2) {
		this.txacn2 = txacn2;
	}
	
}
