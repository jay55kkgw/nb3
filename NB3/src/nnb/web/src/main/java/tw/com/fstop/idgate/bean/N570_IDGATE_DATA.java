package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N570_IDGATE_DATA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -762951082705400880L;

//	@SerializedName(value = "ACCNO")
//	private String ACCNO;
	@SerializedName(value = "BCHID")
	private String BCHID;
	@SerializedName(value = "CUSAD1C")
	private String CUSAD1C;
	@SerializedName(value = "CUSAD2C")
	private String CUSAD2C;
	@SerializedName(value = "CUSADD1")
	private String CUSADD1;
	
	@SerializedName(value = "CUSADD2")
	private String CUSADD2;
	@SerializedName(value = "CUSADD3")
	private String CUSADD3;
	@SerializedName(value = "CUSTEL1")
	private String CUSTEL1;
	@SerializedName(value = "CUSTEL2")
	private String CUSTEL2;
//	public String getACCNO() {
//		return ACCNO;
//	}
//	public void setACCNO(String aCCNO) {
//		ACCNO = aCCNO;
//	}
	public String getBCHID() {
		return BCHID;
	}
	public void setBCHID(String bCHID) {
		BCHID = bCHID;
	}
	public String getCUSAD1C() {
		return CUSAD1C;
	}
	public void setCUSAD1C(String cUSAD1C) {
		CUSAD1C = cUSAD1C;
	}
	public String getCUSAD2C() {
		return CUSAD2C;
	}
	public void setCUSAD2C(String cUSAD2C) {
		CUSAD2C = cUSAD2C;
	}
	public String getCUSADD1() {
		return CUSADD1;
	}
	public void setCUSADD1(String cUSADD1) {
		CUSADD1 = cUSADD1;
	}
	public String getCUSADD2() {
		return CUSADD2;
	}
	public void setCUSADD2(String cUSADD2) {
		CUSADD2 = cUSADD2;
	}
	public String getCUSADD3() {
		return CUSADD3;
	}
	public void setCUSADD3(String cUSADD3) {
		CUSADD3 = cUSADD3;
	}
	public String getCUSTEL1() {
		return CUSTEL1;
	}
	public void setCUSTEL1(String cUSTEL1) {
		CUSTEL1 = cUSTEL1;
	}
	public String getCUSTEL2() {
		return CUSTEL2;
	}
	public void setCUSTEL2(String cUSTEL2) {
		CUSTEL2 = cUSTEL2;
	}
	

}
