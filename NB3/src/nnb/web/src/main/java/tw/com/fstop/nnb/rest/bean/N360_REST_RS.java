package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N360_REST_RS extends BaseRestBean implements Serializable {
	
	private static final long serialVersionUID = 1972330455668047931L;
	private String SVYON = "";			// 是否有台幣存款帳號
	private String FATCA = "";			// 是否經FATCA辨識
	private String NBYON = "";			// 是否已申請一般網銀
	private String FNDYON = "";			// 是否已申請一般網銀基金下單
	private String ORDYON = "";			// 是否已申請線上開戶
	private String NBFLAG = "";			// 一般網銀申請業務類別
	private String CUSTYPE = "";		// 是否年滿二十歲

	//20210429 修正 不需要這段
//	// 電文沒回但ms_tw回應的資料
//	private String occurMsg = "";
	
	
	public String getSVYON() {
		return SVYON;
	}

	public void setSVYON(String sVYON) {
		SVYON = sVYON;
	}

	public String getFATCA() {
		return FATCA;
	}

	public void setFATCA(String fATCA) {
		FATCA = fATCA;
	}

	public String getNBYON() {
		return NBYON;
	}

	public void setNBYON(String nBYON) {
		NBYON = nBYON;
	}

	public String getFNDYON() {
		return FNDYON;
	}

	public void setFNDYON(String fNDYON) {
		FNDYON = fNDYON;
	}

	public String getORDYON() {
		return ORDYON;
	}

	public void setORDYON(String oRDYON) {
		ORDYON = oRDYON;
	}

	public String getNBFLAG() {
		return NBFLAG;
	}

	public void setNBFLAG(String nBFLAG) {
		NBFLAG = nBFLAG;
	}

	public String getCUSTYPE() {
		return CUSTYPE;
	}

	public void setCUSTYPE(String cUSTYPE) {
		CUSTYPE = cUSTYPE;
	}

//	public String getOccurMsg() {
//		return occurMsg;
//	}
//
//	public void setOccurMsg(String occurMsg) {
//		this.occurMsg = occurMsg;
//	}
	
}