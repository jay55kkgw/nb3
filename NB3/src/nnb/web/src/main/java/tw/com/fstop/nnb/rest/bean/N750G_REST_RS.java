package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N750G_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2774371349098034664L;

	private String OFFSET;		// 空白
	private String HEADER;		// HEADER
	private String SYNC;		// Sync.Check Item
	private String MSGCOD;		// 回應代碼
	private String TYPE;		// 繳款類別
	private String OUTACN;		// 轉出帳號
	private String TOTBAL;		// 帳上餘額
	private String AVLBAL;		// 可用餘額
	private String PAYDUE;		// 繳費期限
	private String BARCODE1;	// 條碼１
	private String BARCODE2;	// 條碼２
	private String BARCODE3;	// 條碼３
	private String AMOUNT;		// 繳款金額
	private String TRNDATE;		// 交易日期YYYMMDD
	private String TRNTIME;		// 交易時間HHMMSS
	private String MAC;			// MAC
	private String CMTXTIME;	// 交易時間
	
	
	public String getOFFSET() {
		return OFFSET;
	}
	public void setOFFSET(String oFFSET) {
		OFFSET = oFFSET;
	}
	public String getHEADER() {
		return HEADER;
	}
	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}
	public String getSYNC() {
		return SYNC;
	}
	public void setSYNC(String sYNC) {
		SYNC = sYNC;
	}
	public String getMSGCOD() {
		return MSGCOD;
	}
	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getOUTACN() {
		return OUTACN;
	}
	public void setOUTACN(String oUTACN) {
		OUTACN = oUTACN;
	}
	public String getTOTBAL() {
		return TOTBAL;
	}
	public void setTOTBAL(String tOTBAL) {
		TOTBAL = tOTBAL;
	}
	public String getAVLBAL() {
		return AVLBAL;
	}
	public void setAVLBAL(String aVLBAL) {
		AVLBAL = aVLBAL;
	}
	public String getPAYDUE() {
		return PAYDUE;
	}
	public void setPAYDUE(String pAYDUE) {
		PAYDUE = pAYDUE;
	}
	public String getBARCODE1() {
		return BARCODE1;
	}
	public void setBARCODE1(String bARCODE1) {
		BARCODE1 = bARCODE1;
	}
	public String getBARCODE2() {
		return BARCODE2;
	}
	public void setBARCODE2(String bARCODE2) {
		BARCODE2 = bARCODE2;
	}
	public String getBARCODE3() {
		return BARCODE3;
	}
	public void setBARCODE3(String bARCODE3) {
		BARCODE3 = bARCODE3;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	public String getTRNDATE() {
		return TRNDATE;
	}
	public void setTRNDATE(String tRNDATE) {
		TRNDATE = tRNDATE;
	}
	public String getTRNTIME() {
		return TRNTIME;
	}
	public void setTRNTIME(String tRNTIME) {
		TRNTIME = tRNTIME;
	}
	public String getMAC() {
		return MAC;
	}
	public void setMAC(String mAC) {
		MAC = mAC;
	}
	public String getCMTXTIME() {
		return CMTXTIME;
	}
	public void setCMTXTIME(String cMTXTIME) {
		CMTXTIME = cMTXTIME;
	}
	
}
