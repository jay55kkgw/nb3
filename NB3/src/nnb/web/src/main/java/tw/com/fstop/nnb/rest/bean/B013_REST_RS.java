package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class B013_REST_RS extends BaseRestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3302494855043674012L;
	private String TXNDIID;//TXN Source Institute ID
	private String CMPERIOD;
	private String SNID;//System Network Identifier
	private String RECNO;//讀取筆數
	private String NEXT;//資料起始位置
	
	private String TXNSIID;//TXN Source Institute ID
	private String TXNIDT;//TXN Initiate Date and Time
	private String SSUP;//System Supervisory
	private String FILLER_X3;
	private String SYNCCHK;//Sync. Check Item
	
	private String ACF;//Address Control Field
	private String CMRECNUM;
	private String MSGTYPE;
	private String TIME;//時間HHMMSS
	private String SYSTRACE;
	
	private String LOOPINFO;//LOOPINFO
	private String ENDDATE;//查詢迄日
	private String BEGDATE;//查詢起日
	private String CUSNAME;//姓名
	private String DATE;//日期YYYMMDD
	
	private String BITMAPCFG;//Bit Map Configuration
	private String TOTRECNO;//總筆數
	private String CMQTIME;
	private String PROCCODE;//Processing Code
	private String CUSIDN;//統一編號
	
	private String RESPCOD;//Response Code
	private String __TOPMSG;
	private String __OCCURS;
	private LinkedList<B013_REST_RSDATA> REC;
	
	public String getTXNDIID() {
		return TXNDIID;
	}
	public void setTXNDIID(String tXNDIID) {
		TXNDIID = tXNDIID;
	}
	public String getCMPERIOD() {
		return CMPERIOD;
	}
	public void setCMPERIOD(String cMPERIOD) {
		CMPERIOD = cMPERIOD;
	}
	public String getSNID() {
		return SNID;
	}
	public void setSNID(String sNID) {
		SNID = sNID;
	}
	public String getRECNO() {
		return RECNO;
	}
	public void setRECNO(String rECNO) {
		RECNO = rECNO;
	}
	public String getNEXT() {
		return NEXT;
	}
	public void setNEXT(String nEXT) {
		NEXT = nEXT;
	}
	public String getTXNSIID() {
		return TXNSIID;
	}
	public void setTXNSIID(String tXNSIID) {
		TXNSIID = tXNSIID;
	}
	public String getTXNIDT() {
		return TXNIDT;
	}
	public void setTXNIDT(String tXNIDT) {
		TXNIDT = tXNIDT;
	}
	public String getSSUP() {
		return SSUP;
	}
	public void setSSUP(String sSUP) {
		SSUP = sSUP;
	}
	public String getFILLER_X3() {
		return FILLER_X3;
	}
	public void setFILLER_X3(String fILLER_X3) {
		FILLER_X3 = fILLER_X3;
	}
	public String getSYNCCHK() {
		return SYNCCHK;
	}
	public void setSYNCCHK(String sYNCCHK) {
		SYNCCHK = sYNCCHK;
	}
	public String getACF() {
		return ACF;
	}
	public void setACF(String aCF) {
		ACF = aCF;
	}
	public String getCMRECNUM() {
		return CMRECNUM;
	}
	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}
	public String getMSGTYPE() {
		return MSGTYPE;
	}
	public void setMSGTYPE(String mSGTYPE) {
		MSGTYPE = mSGTYPE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getSYSTRACE() {
		return SYSTRACE;
	}
	public void setSYSTRACE(String sYSTRACE) {
		SYSTRACE = sYSTRACE;
	}
	public String getLOOPINFO() {
		return LOOPINFO;
	}
	public void setLOOPINFO(String lOOPINFO) {
		LOOPINFO = lOOPINFO;
	}
	public String getENDDATE() {
		return ENDDATE;
	}
	public void setENDDATE(String eNDDATE) {
		ENDDATE = eNDDATE;
	}
	public String getCUSNAME() {
		return CUSNAME;
	}
	public void setCUSNAME(String cUSNAME) {
		CUSNAME = cUSNAME;
	}
	public String getBEGDATE() {
		return BEGDATE;
	}
	public void setBEGDATE(String bEGDATE) {
		BEGDATE = bEGDATE;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getBITMAPCFG() {
		return BITMAPCFG;
	}
	public void setBITMAPCFG(String bITMAPCFG) {
		BITMAPCFG = bITMAPCFG;
	}
	public String getTOTRECNO() {
		return TOTRECNO;
	}
	public void setTOTRECNO(String tOTRECNO) {
		TOTRECNO = tOTRECNO;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getPROCCODE() {
		return PROCCODE;
	}
	public void setPROCCODE(String pROCCODE) {
		PROCCODE = pROCCODE;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getRESPCOD() {
		return RESPCOD;
	}
	public void setRESPCOD(String rESPCOD) {
		RESPCOD = rESPCOD;
	}
	public String get__TOPMSG() {
		return __TOPMSG;
	}
	public void set__TOPMSG(String __TOPMSG) {
		this.__TOPMSG = __TOPMSG;
	}
	public String get__OCCURS() {
		return __OCCURS;
	}
	public void set__OCCURS(String __OCCURS) {
		this.__OCCURS = __OCCURS;
	}
	public LinkedList<B013_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<B013_REST_RSDATA> rEC) {
		REC = rEC;
	}

}
