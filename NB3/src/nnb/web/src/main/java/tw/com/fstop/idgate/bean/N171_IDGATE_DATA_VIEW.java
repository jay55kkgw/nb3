package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.google.gson.annotations.SerializedName;

public class N171_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 635107701193196553L;
	
	@SerializedName(value = "FGTXDATE")
	private String FGTXDATE; // 交易類型

	@SerializedName(value = "transfer_date")
	private String transfer_date; // 轉帳日期
	
	@SerializedName(value = "OUTACN")
	private String OUTACN; // 轉出帳號
	
	@SerializedName(value = "TaxTypeText")
	private String TaxTypeText; // 繳稅類別
	
	@SerializedName(value = "INTSACN1")
	private String INTSACN1; // 稽徵單位代號
	
	@SerializedName(value = "INTSACN2")
	private String INTSACN2; // 身分證字號/統一編號
	
	@SerializedName(value = "INTSACN3")
	private String INTSACN3; // 銷帳編號
	
	@SerializedName(value = "PAYDUED_VIEW")
	private String PAYDUED_VIEW; // 繳納截止日
	
	@SerializedName(value = "AMOUNT_VIEW")
	private String AMOUNT_VIEW; // 繳款金額
	
	@Override
	public Map<String, String> coverMap(){
		Map<String, String> result = new LinkedHashMap<String, String>();
		
		result.put("交易名稱", "繳稅");
		
		if ("1".equals(this.FGTXDATE)) {
			result.put("交易類型", "即時");
		} else if ("2".equals(this.FGTXDATE)) {
			result.put("交易類型", "預約");
		}
		
		if (StringUtils.isNotBlank(this.transfer_date) && !"NaN".equals(this.transfer_date)) result.put("轉帳日期", this.transfer_date);
		if (StringUtils.isNotBlank(this.OUTACN) && !"NaN".equals( this.OUTACN)) result.put("轉出帳號", this.OUTACN);
		if (StringUtils.isNotBlank(this.TaxTypeText) && !"NaN".equals( this.TaxTypeText))  result.put("繳稅類別", this.TaxTypeText);
		if (StringUtils.isNotBlank(this.INTSACN1) && !"NaN".equals( this.INTSACN1)) result.put("稽徵單位代號 ", this.INTSACN1);
		if (StringUtils.isNotBlank(this.INTSACN2) && !"NaN".equals( this.INTSACN2)) result.put("身分證字號/統一編號", this.INTSACN2);
		if (StringUtils.isNotBlank(this.INTSACN3) && !"NaN".equals( this.INTSACN3)) result.put("銷帳編號", this.INTSACN3);
		if (StringUtils.isNotBlank(this.PAYDUED_VIEW) && !"NaN".equals( this.PAYDUED_VIEW)) result.put("繳納截止日", this.PAYDUED_VIEW);
		if (StringUtils.isNotBlank(this.AMOUNT_VIEW) && !"NaN".equals( this.AMOUNT_VIEW)) result.put("繳款金額", "新臺幣" + this.AMOUNT_VIEW + "元");
		return result;
	}

	public String getFGTXDATE() {
		return FGTXDATE;
	}

	public void setFGTXDATE(String fGTXDATE) {
		FGTXDATE = fGTXDATE;
	}

	public String getTransfer_date() {
		return transfer_date;
	}

	public void setTransfer_date(String transfer_date) {
		this.transfer_date = transfer_date;
	}

	public String getOUTACN() {
		return OUTACN;
	}

	public void setOUTACN(String oUTACN) {
		OUTACN = oUTACN;
	}

	public String getTaxTypeText() {
		return TaxTypeText;
	}

	public void setTaxTypeText(String taxTypeText) {
		TaxTypeText = taxTypeText;
	}

	public String getINTSACN1() {
		return INTSACN1;
	}

	public void setINTSACN1(String iNTSACN1) {
		INTSACN1 = iNTSACN1;
	}

	public String getINTSACN2() {
		return INTSACN2;
	}

	public void setINTSACN2(String iNTSACN2) {
		INTSACN2 = iNTSACN2;
	}

	public String getINTSACN3() {
		return INTSACN3;
	}

	public void setINTSACN3(String iNTSACN3) {
		INTSACN3 = iNTSACN3;
	}

	public String getPAYDUED_VIEW() {
		return PAYDUED_VIEW;
	}

	public void setPAYDUED_VIEW(String pAYDUED_VIEW) {
		PAYDUED_VIEW = pAYDUED_VIEW;
	}

	public String getAMOUNT_VIEW() {
		return AMOUNT_VIEW;
	}

	public void setAMOUNT_VIEW(String aMOUNT_VIEW) {
		AMOUNT_VIEW = aMOUNT_VIEW;
	}


	
}
