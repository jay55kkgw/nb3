package tw.com.fstop.nnb.spring.controller;

import java.net.URLEncoder;
import java.util.Base64;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.Gold_Apply_Service;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({SessionUtil.CUSIDN,SessionUtil.PRINT_DATALISTMAP_DATA,SessionUtil.RESULT_LOCALE_DATA, SessionUtil.STEP1_LOCALE_DATA,
					SessionUtil.TRANSFER_CONFIRM_TOKEN, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, SessionUtil.TRANSFER_RESULT_TOKEN,
					"KYC"})
@Controller
@RequestMapping(value = "/GOLD/APPLY")
public class Gold_Apply_Controller {
	
	Logger log = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private Gold_Apply_Service gold_apply_service;
	
	//線上申請黃金存摺帳戶(NA50)
	@RequestMapping(value = "/gold_account_apply")
	public String gold_account_apply(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		Locale currentLocale = LocaleContextHolder.getLocale();
		String locale = currentLocale.toString();
		if(!locale.equals("zh_TW")) {
			model.addAttribute("back_url", "/INDEX/index");
			model.addAttribute("rev_url", "/GOLD/APPLY/gold_account_apply");
			return "/online_apply/turn_to_zhTW_with_menu";
		}
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		String kyc = okMap.get("KYC") == null ? "":okMap.get("KYC");
		SessionUtil.addAttribute(model, "KYC" ,kyc );
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		return "/gold/gold_account_apply_p1";
	}
	//線上申請黃金存摺帳戶(NA50)
	@RequestMapping(value = "/gold_account_apply_p2")
	public String gold_account_apply_p2(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		return "/gold/gold_account_apply_p2";
	}
	//線上申請黃金存摺帳戶(NA50)
	@RequestMapping(value = "/gold_account_apply_p3")
	public String gold_account_apply_p3(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		try {
			String kyc = (String)SessionUtil.getAttribute(model, "KYC", null);
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),"utf-8");
			bs = gold_apply_service.gold_account_apply_p3(cusidn, reqParam, kyc == null? "":kyc);
			bs.addData("CUSIDN", cusidn);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_account_apply_p3 error >> {}",e);
		} finally {
			if(bs != null && bs.getResult()) {
				Map<String, Object> dataMap = (Map<String, Object>) bs.getData();
				log.debug("Gd_InvAttr >> {}", dataMap.get("Gd_InvAttr"));
				if(((String)dataMap.get("Gd_InvAttr")).equals("1")) {
					target = "/gold/gd_inv_attr";
					model.addAttribute("result_data", bs);
					model.addAttribute("TXID", (String)dataMap.get("TXID"));
					model.addAttribute("ALERTTYPE", (String)dataMap.get("ALERTTYPE"));
				}else {
					target = gold_account_apply_p4(request, response, reqParam, model);
				}
			}else {
				bs.setPrevious("/GOLD/APPLY/gold_account_apply_p2");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
//		return "/gold/gold_account_apply_p3";
		return target;
	}
	//線上申請黃金存摺帳戶(NA50)
	@RequestMapping(value = "/gold_account_apply_p4")
	public String gold_account_apply_p4(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {		
		String target = "/error";
		BaseResult bs = null;
		BaseResult bsN920 = null;
		Map<String, String> result = null;
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		// 是否是回上一頁
		if("Y".equals(okMap.get("back"))) {
			model.addAttribute("isBack","Y");
		}
		
		//手續費
		int fee = 50;
		log.trace("gold_account_apply_p4>>");
		try {

			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = gold_apply_service.getTxToken();
			log.trace("gold_account_apply.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),"utf-8");
			bsN920 = new BaseResult();
			bsN920 = gold_apply_service.N920_REST(cusidn, gold_apply_service.OUT_ACNO);
			bsN920.addData("CUSIDN", cusidn);
			bsN920.addData("fee", fee);
			bsN920.addData("TODAY", DateUtil.getTWDate("/"));
			List<Map<String,String>> callRow = (List<Map<String,String>>)((Map<String,Object>)bsN920.getData()).get("REC"); 
			if(callRow.size() <= 0) {
				bsN920.setSYSMessage("Z999");
			}
			result = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_account_apply_p4 error >> {}",e);
		} finally {
			if(bsN920 != null && bsN920.getResult()) {
				target = "/gold/gold_account_apply_p4";
				model.addAttribute("n920_data", bsN920);
				model.addAttribute("result_data", result);
			}else {
				bsN920.setPrevious("/GOLD/APPLY/gold_account_apply_p3");
				model.addAttribute(BaseResult.ERROR, bsN920);
			}
		}
		return target;
	}	
	//線上申請黃金存摺帳戶(NA50)
	@RequestMapping(value = "/gold_account_apply_p5")
	public String gold_account_apply_p5(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {		
		String target = "/error";
		Map<String, String> result = null;
		BaseResult bs = null;
		Map<String, String> okMap = null;
		//手續費
		int fee = 50;
		String jsondc = "";
		String amlmsg = "";
		log.trace("gold_account_apply_p5>>");
		try {
			//okMap = reqParam;
			okMap = ESAPIUtil.validStrMap(reqParam);
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("pay_taxes_confirm.jsondc: " + jsondc));
			
			okMap.put("jsondc", jsondc);
			log.trace(ESAPIUtil.vaildLog("pay_taxes_confirm.okMap: {}"+ okMap));
			
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),"utf-8");
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				result = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null);
			}
			if (!hasLocale) {
				bs = new BaseResult();
				// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
				String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
				log.trace("gold_account_apply.token: {}", token);
				log.trace(ESAPIUtil.vaildLog("gold_account_apply.okMap.token: {}"+ okMap.get("TOKEN")));
				if(StrUtil.isNotEmpty(okMap.get("TOKEN"))  &&  okMap.get("TOKEN").equals(token)) {
					bs.setResult(Boolean.TRUE);
				}
				log.trace("bs.getResult(): {}",bs.getResult());
				if(!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				}
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN , token);
				
				result = okMap;
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, okMap);
			}
			amlmsg = gold_apply_service.amlChecked(cusidn, reqParam);
			
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_account_apply_p5 error >> {}",e);
		} finally {
			if(amlmsg.equals("")) {
				target = "/gold/gold_account_apply_p5";
				model.addAttribute("result_data", result);
				log.debug(ESAPIUtil.vaildLog("TRANSFER_RESULT_TOKEN >> {}"+result.get("TOKEN")));
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, result.get("TOKEN"));
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, result);
			}else {
				bs = new BaseResult();
				bs.setErrorMessage("Z616", amlmsg);
				bs.setPrevious("/GOLD/APPLY/gold_account_apply_p3");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	//線上申請黃金存摺帳戶(NA50)
	@RequestMapping(value = "/gold_account_apply_result")
	public String gold_account_apply_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {		
		String target = "/error";
		
		BaseResult bs = null;
		log.trace("history_price_query_result>>");
		Map<String, String> okMap = null;
		try {
			
			bs = new BaseResult();
			okMap = ESAPIUtil.validStrMap(reqParam);
			//okMap = reqParam;
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null);
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				log.trace("gold_account_apply.validate TXTOKEN...");
				log.trace("gold_account_apply.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("gold_account_apply.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("gold_account_apply.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("gold_account_apply TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						!okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("gold_account_apply.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("gold_account_apply TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				
				bs = gold_apply_service.gold_account_apply_result(cusidn, okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, okMap);
				
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_account_apply_result error >> {}",e);
		} finally {
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TOKEN"));
			if (bs != null && bs.getResult())
			{
				target = "/gold/gold_account_apply_result";
				List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("result_data", bs);
				model.addAttribute("input_data", okMap);
			}
			else
			{
				//新增回上一頁的路徑
				bs.setPrevious("/GOLD/APPLY/gold_account_apply_p3");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	//線上申請黃金存摺網路交易(NA60)
	@RequestMapping(value = "/gold_trading_apply")
	public String gold_trading_apply(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			Locale currentLocale = LocaleContextHolder.getLocale();
			String locale = currentLocale.toString();
			if(!locale.equals("zh_TW")) {
				model.addAttribute("back_url", "/INDEX/index");
				model.addAttribute("rev_url", "/GOLD/APPLY/gold_trading_apply");
				return "/online_apply/turn_to_zhTW_with_menu";
			}
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),"utf-8");
			bs = gold_apply_service.gold_trading_apply(cusidn, reqParam);
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			log.debug("clean RESULT_LOCALE_DATA");
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_trading_apply error >> {}",e);
		} finally {
			if(bs != null && bs.getResult()) {
				if( ((Map<String, Object>) bs.getData()).get("ToGoFlag").equals("1")) {
					target = "/gold/gold_trading_apply_p1";
				}else {
					model.addAttribute("result_data", bs);
					target = "/gold/gold_reservation_query_confirm";
				}
			}else {
				bs.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	//線上申請黃金存摺網路交易(NA60)
	@RequestMapping(value = "/gold_trading_apply_p2")
	public String gold_trading_apply_p2(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		return "/gold/gold_trading_apply_p2";
	}
	//線上申請黃金存摺網路交易(NA60)
	@RequestMapping(value = "/gold_trading_apply_p3")
	public String gold_trading_apply_p3(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		BaseResult bsSvacnos = null;
		BaseResult bsAcnos = null;
		Map<String, String> result = null;
		try {
			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = gold_apply_service.getTxToken();
			log.trace("gold_account_apply.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),"utf-8");
			bsSvacnos = new BaseResult();
			bsSvacnos = gold_apply_service.gold_trading_apply_p3(cusidn, reqParam);
			bsAcnos = new BaseResult();
			bsAcnos = gold_apply_service.gold_trading_apply_p3_acnos(cusidn, reqParam);
//			result = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_trading_apply_p3 error >> {}",e);
		} finally {
			if(bsSvacnos != null && bsSvacnos.getResult()) {
				target = "/gold/gold_trading_apply_p3";
				model.addAttribute("result_data", bsSvacnos);
				model.addAttribute("result_data_acnos", bsAcnos);
//				model.addAttribute("input_data", result);
			}else {
				bsSvacnos.setPrevious("/GOLD/APPLY/gold_account_apply_p2");
				model.addAttribute(BaseResult.ERROR, bsSvacnos);
			}
		}
		
		return target;
	}
	
	//線上申請黃金存摺網路交易(NA60)
	@RequestMapping(value = "/gold_trading_apply_p4")
	public String gold_trading_apply_p4(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		Map<String, String> result = null;
		BaseResult bs = null;
		String jsondc = "";
		String amlmsg = "";
		log.trace("gold_account_apply_p5>>");
		try {
 			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			//Map<String, String> okMap = reqParam;
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("pay_taxes_confirm.jsondc: " + jsondc));
			
			okMap.put("jsondc", jsondc);
			log.trace(ESAPIUtil.vaildLog("pay_taxes_confirm.okMap: {}"+ okMap));
			
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),"utf-8");
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				result = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null);
			}
			if (!hasLocale) {
				bs = new BaseResult();
				// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
				String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
				log.trace("gold_account_apply.token: {}", token);
				log.trace(ESAPIUtil.vaildLog("gold_account_apply.okMap.token: {}"+ okMap.get("TOKEN")));
				if(StrUtil.isNotEmpty(okMap.get("TOKEN"))  &&  okMap.get("TOKEN").equals(token)) {
					bs.setResult(Boolean.TRUE);
				}
				log.trace("bs.getResult(): {}",bs.getResult());
				if(!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				}
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN , token);
				
				result = okMap;
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, okMap);
			}
//			amlmsg = gold_apply_service.amlChecked(cusidn, reqParam);
			
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_trading_apply_p4 error >> {}",e);
		} finally {
			target = "/gold/gold_trading_apply_p4";
			model.addAttribute("result_data", result);
			log.debug(ESAPIUtil.vaildLog("TRANSFER_RESULT_TOKEN >> {}"+result.get("TOKEN")));
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, result.get("TOKEN"));
			SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, result);
		}
		return target;
	}

	//線上申請黃金存摺網路交易(NA60)
	@RequestMapping(value = "/gold_trading_apply_result")
	public String gold_trading_apply_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {		
		String target = "/error";
		
		BaseResult bs = null;
		Map<String, String> okMap = null;
		log.trace("gold_trading_apply_result>>");
		try {
			
			bs = new BaseResult();
            okMap = ESAPIUtil.validStrMap(reqParam);
			//okMap = reqParam;
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				okMap = (Map<String, String>)SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null);
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				log.trace("gold_account_apply.validate TXTOKEN...");
				log.trace("gold_account_apply.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("gold_account_apply.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("gold_account_apply.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("gold_account_apply TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TOKEN")) && 
						!okMap.get("TOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("gold_account_apply.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("gold_account_apply TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				
				bs = gold_apply_service.gold_trading_apply_result(cusidn, okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, okMap);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_trading_apply_result error >> {}",e);
		} finally {
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TOKEN"));
			if (bs != null && bs.getResult())
			{
				target = "/gold/gold_trading_apply_result";
				List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("result_data", bs);
				model.addAttribute("input_data", okMap);
			}
			else
			{
				//新增回上一頁的路徑
				bs.setPrevious("/GOLD/APPLY/gold_trading_apply_p2");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
}
