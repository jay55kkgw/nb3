package tw.com.fstop.nnb.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.annotation.OptBoolean;

import fstop.orm.po.TXNCHECKACC;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.tbb.nnb.dao.SysDailySeqDao;
import tw.com.fstop.tbb.nnb.dao.TxnCheckAccDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;

@Service
public class Fisc_InterBank_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private SysDailySeqDao sysDailySeqDao;

	@Autowired
	private TxnCheckAccDao txnCheckAccDao;

	public String getATMSEQ() {
		return sysDailySeqDao.getFistATMSEQ();
	}

	public BaseResult VerfityAccount(Map<String, String> reqparam) {
		log.trace("Fisc_InterBank_Service.VerfityAccount param: {}", ESAPIUtil.vaildLog(CodeUtil.toJson(reqparam)));
		BaseResult bs = null;
		try {
			// 查詢行庫別
			String attibk = reqparam.get("ATTIBK");
			// 查詢帳號
			String attiac = reqparam.get("ATTIAC");
			// 客戶統編
			String atdeno = reqparam.get("ATDENO");
			// 手機號碼
			String atmobi = reqparam.get("ATMOBI");
			// 檢核交易序號
			String atmseq = reqparam.get("ATMSEQ");
			//客戶生日
			String birth = reqparam.get("BIRTH");

			bs = N857_REST(attibk, attiac, atdeno, atmobi, atmseq, birth);
			log.trace("Fisc_InterBank_Service.VerfityAccount bs: {}", ESAPIUtil.vaildLog( CodeUtil.toJson(bs)));

			if (bs != null && bs.getResult()) {
				log.trace("Fisc_InterBank_Service.newCheckRecord atmseq: {}",  ESAPIUtil.vaildLog(atmseq));
				TXNCHECKACC txncheckacc = txnCheckAccDao.newCheckRecord(atmseq);
				log.trace("Fisc_InterBank_Service TXNCHECKACC: {}", ESAPIUtil.vaildLog(txncheckacc.toString()));
			}

		} catch (Exception e) {
			log.error("Fisc_InterBank_Service.VerfityAccount error : " + e.getMessage(), e);
		}
		return bs;
	}

	public String getTxnCheckAccStatus(String adtxno) {
		String status = "";
		TXNCHECKACC txncheckacc = txnCheckAccDao.getTxnCheckAcc(adtxno);
		if (txncheckacc != null) {
			String rCode = txncheckacc.getRCODE();
			if ("4001".equals(rCode)) { // 4001:交易成功(需再check RTCHK,RTACN,RTOPS結果)
				status = txncheckacc.getSTATUS();
			} else {
				status = rCode;
			}

			if (status != null && status.length() > 0) {
				txncheckacc.setRPT("Y");
				txnCheckAccDao.update(txncheckacc);
			}
		}

		return status;
	}

	public void rogerThat(String adtxno, String status, String rcode) {
		txnCheckAccDao.updateReply(adtxno, status, rcode);
	}
	
	/**
	 * OTP發送簡訊驗證碼
	 * @param adopid
	 * @param cusidn
	 * @return
	 */
	public List<Map<String, String>> send_smsotp(String adopid, String cusidn, String mobtel, String msgType) {
		log.trace(ESAPIUtil.vaildLog("send_smsotp...>>> cusidn : " +cusidn + ", mobtel : " + mobtel + ", adopid : " + adopid));
		
		List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
		BaseResult bs = new BaseResult();
		try {
			Map<String, String> redata = new HashMap();
			redata.put("FuncType", "0");			
			redata.put("CUSIDN", cusidn);
			redata.put("UID", cusidn);
			redata.put("OTP", "");
			redata.put("MOBTEL", mobtel);
			//取得簡訊密碼時，不寫TXNLOG所以改SMSADOPID
			redata.put("SMSADOPID", adopid);
			redata.put("MSGTYPE", msgType);
			
			bs = Send_Otp_REST(redata);
			log.trace("Get bsdata Send_SmsOtp_REST>>>{}",bs.getData());
			Map<String, String>datamap = new HashMap();
			if(bs!=null && bs.getResult()) {
				
			}
			else {
				bs.addData("MSGCOD", bs.getMsgCode());
			}
			Map<String, String>listmap = (Map) bs.getData();
			resultList.add(listmap);
			log.trace("resultList>>>{}",resultList);
		} 
		catch (Exception e) {
			log.error("getGoldAcn.error: {}", e);
		}
		
		return resultList;
	}

	/*
	 * 4001:交易成功 0204~0206:對方行未參加本項業務，或暫停營業 0601:交易發生TIMEOUT 2999:其他類檢核錯誤 4401:失效帳戶
	 * 4405:帳戶尚未生效。 4501:問題帳戶 4507:無此帳戶 4511:無法檢核帳號 4601:發卡行檢核身分核驗超過次數 4705:拒絕交易
	 * 4806:身份証號／營利事業統一編號有誤。 其他:交易失敗
	 */
	public Map<String, String> errCodeForFisc(String status) {
		Map<String, String> msgMap = new HashMap<String, String>();
		msgMap.put("err_code", status);
		msgMap.put("RCODE", status);
		switch (status) {
		case "0204":
		case "0205":
		case "0206":
			msgMap.put("RCODE_ERR", "對方行未參加本項業務，或暫停營業。");
			msgMap.put("err_msg", "對方行未參加本項業務，或暫停營業。");
			break;
		case "0601":
			msgMap.put("RCODE_ERR", "交易發生TIMEOUT。");
			msgMap.put("err_msg", "交易發生TIMEOUT。");
			break;
		case "2999":
			msgMap.put("RCODE_ERR", "其他類檢核錯誤。");
			msgMap.put("err_msg", "其他類檢核錯誤。");
			break;
		case "4401":
			msgMap.put("RCODE_ERR", "失效帳戶。");
			msgMap.put("err_msg", "失效帳戶。");
			break;
		case "4405":
			msgMap.put("RCODE_ERR", "問題帳戶。");
			msgMap.put("err_msg", "問題帳戶。");
			break;
		case "4507":
			msgMap.put("RCODE_ERR", "無此帳戶。");
			msgMap.put("err_msg", "無此帳戶。");
			break;
		case "4511":
			msgMap.put("RCODE_ERR", "無法檢核帳號。");
			msgMap.put("err_msg", "無法檢核帳號。");
			break;
		case "4601":
			msgMap.put("RCODE_ERR", "發卡行檢核身分核驗超過次數。");
			msgMap.put("err_msg", "發卡行檢核身分核驗超過次數。");
			break;
		case "4705":
			msgMap.put("RCODE_ERR", "拒絕交易。");
			msgMap.put("err_msg", "拒絕交易。");
			break;
		case "4806":
			msgMap.put("RCODE_ERR", "身份証號／營利事業統一編號有誤。");
			msgMap.put("err_msg", "身份証號／營利事業統一編號有誤。");
			break;
		default:
			msgMap.put("RCODE_ERR", "交易失敗，請洽他行客服訊問。");
			msgMap.put("err_msg", "交易失敗，請洽他行客服訊問。");
			break;
		}
		return msgMap;
	}

	/*
	 * 000001：成功，其餘列為失敗
	 * 顯示順序 RTACN、RTCHK、RTOPS
	 */
	public Map<String, String> errCodeForVerify(String status) {
		Map<String, String> msgMap = new HashMap<String, String>();
		String rtchk = status.substring(0, 2);
		String rtacn = status.substring(2, 4);
		String rtops = status.substring(4, 6);
		msgMap.put("err_code", status);
		msgMap.put("err_msg", "請先確認您的帳號是否為有效帳號。");
		if (!rtacn.equals("00")) {
			msgMap.put("err_msg", "請先確認您的帳號是否為有效帳號。");
			msgMap.put("RTACN", rtacn);
			switch (rtacn) {
			case "01":
				msgMap.put("RTACN_ERR", "該帳號為問題帳戶(結清?靜止戶?)。");
				break;
			case "99":
				msgMap.put("RTACN_ERR", "狀態不明或未檢核帳號。");
				break;
			}
			return msgMap;
		}
		
		if (!rtchk.equals("00")) {
			msgMap.put("RTCHK", rtchk);
			switch (rtchk) {
			case "01":
				msgMap.put("RTCHK_ERR", "身分證號或統一編號有誤。");
				msgMap.put("err_msg", "身分證號或統一編號有誤。");
				break;
			case "02":
				msgMap.put("RTCHK_ERR", "驗證人之行動電話有誤。");
				msgMap.put("err_msg", "驗證人之行動電話有誤。");
				break;
			case "03":
				msgMap.put("RTCHK_ERR", "驗證人之出生年月日有誤。");
				msgMap.put("err_msg", "驗證人之出生年月日有誤。");
				break;			
			case "99":
				msgMap.put("RTCHK_ERR", "無指定之檢核項目。");
				msgMap.put("err_msg", "請與客服聯絡。");
				break;
			}
			return msgMap;
		}
		
		if (!rtops.equals("01")) {
			msgMap.put("RTOPS", rtops);
			switch (rtops) {
			case "02":
				msgMap.put("RTOPS_ERR", "數位存款帳戶，非臨櫃開立帳戶。");
				msgMap.put("err_msg", "請以臨櫃開立之存款帳戶驗證。");
				break;
			case "03":
				msgMap.put("RTOPS_ERR", "電子支付帳戶，非臨櫃開立帳戶。");
				msgMap.put("err_msg", "請以臨櫃開立之存款帳戶驗證。");
				break;
			case "04":
				msgMap.put("RTOPS_ERR", "儲值支付帳戶，非臨櫃開立帳戶。");
				msgMap.put("err_msg", "請以臨櫃開立之存款帳戶驗證。");
				break;
			case "99":
				msgMap.put("RTOPS_ERR", "無法確認開戶狀態。");
				msgMap.put("err_msg", "請先確認您的帳號是否為有效帳號。");
				break;
			}
			return msgMap;
		}
		return msgMap;
	}

	public boolean OTPVerify(String cusidn, String otp, String adopid) {
		log.trace("Fisc_InterBank_Service.OTPVerify start...");
		boolean result_bool = false;

		BaseResult bs = new BaseResult();
		Map<String, String> reqParam = null;
		try {

			// 要先驗證簡訊
			reqParam = new HashMap<String, String>();
			reqParam.put("CUSIDN", cusidn);
			reqParam.put("FuncType", "1");
			reqParam.put("OTP", otp);
			reqParam.put("ADOPID", adopid);

			bs = Send_Otp_REST(reqParam);
			if (bs.getResult()) {
				result_bool = true;
				log.info("簡訊驗證通過...");
			} else {
				result_bool = false;
				log.error("簡訊驗證失敗...");
			}
		} catch (Exception e) {
			log.error("IdGateN915 Error >>{}", e);
		}
		log.trace("Fisc_InterBank_Service.OTPVerify finish result >>{}", result_bool );
		return result_bool;
	}

}
