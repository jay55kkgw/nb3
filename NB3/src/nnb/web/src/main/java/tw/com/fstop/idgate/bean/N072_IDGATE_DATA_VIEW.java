package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;


public class N072_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -7924058732746547754L;

	@SerializedName(value = "transfer_date") //轉帳日期
	private String transfer_date;

	@SerializedName(value = "OUTACN") //轉出帳號
	private String OUTACN;
	
	@SerializedName(value = "DPAGACNO_TEXT") //轉入帳號
	private String DPAGACNO_TEXT;
	
	@SerializedName(value = "AMOUNT_SHOW") //轉帳金額
	private String AMOUNT_SHOW;
	
	@SerializedName(value = "FGTXDATE") //1即時 2預約
	private String FGTXDATE;
	
	
	@Override
	public Map<String, String> coverMap(){
		LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", "繳納本行信用卡費");
		result.put("交易類型", "1".equals(this.FGTXDATE)?"即時":"預約");
		result.put("轉帳日期", this.transfer_date);
		result.put("轉出帳號", this.OUTACN);
		result.put("轉入帳號", this.DPAGACNO_TEXT);
		result.put("轉帳金額", this.AMOUNT_SHOW);
		return result;
	}

	public String getTransfer_date() {
		return transfer_date;
	}

	public void setTransfer_date(String transfer_date) {
		this.transfer_date = transfer_date;
	}

	public String getOUTACN() {
		return OUTACN;
	}

	public void setOUTACN(String oUTACN) {
		OUTACN = oUTACN;
	}

	public String getDPAGACNO_TEXT() {
		return DPAGACNO_TEXT;
	}

	public void setDPAGACNO_TEXT(String dPAGACNO_TEXT) {
		DPAGACNO_TEXT = dPAGACNO_TEXT;
	}

	public String getAMOUNT_SHOW() {
		return AMOUNT_SHOW;
	}

	public void setAMOUNT_SHOW(String aMOUNT_SHOW) {
		AMOUNT_SHOW = aMOUNT_SHOW;
	}

	public String getFGTXDATE() {
		return FGTXDATE;
	}

	public void setFGTXDATE(String fGTXDATE) {
		FGTXDATE = fGTXDATE;
	}

}
