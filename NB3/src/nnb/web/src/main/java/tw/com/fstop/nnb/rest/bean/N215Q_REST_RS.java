package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N215Q_REST_RS extends BaseRestBean implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 775129110323679813L;
	
	private String SEQ;    
	private String COUNT;//筆數
	private String CMRECNUM;//筆數
	
	LinkedList<N215Q_REST_RSDATA> REC;

	public String getSEQ() {
		return SEQ;
	}

	public void setSEQ(String sEQ) {
		SEQ = sEQ;
	}

	public String getCOUNT() {
		return COUNT;
	}

	public void setCOUNT(String cOUNT) {
		COUNT = cOUNT;
	}

	public String getCMRECNUM() {
		return CMRECNUM;
	}

	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}

	public LinkedList<N215Q_REST_RSDATA> getREC() {
		return REC;
	}

	public void setREC(LinkedList<N215Q_REST_RSDATA> rEC) {
		REC = rEC;
	}

	
	

}
