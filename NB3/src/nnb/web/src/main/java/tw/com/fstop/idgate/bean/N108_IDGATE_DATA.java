package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N108_IDGATE_DATA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -762951082705400880L;

	@SerializedName(value = "FDPACN")
	private String FDPACN;

	public String getFDPACN() {
		return FDPACN;
	}

	public void setFDPACN(String fDPACN) {
		FDPACN = fDPACN;
	}
}
