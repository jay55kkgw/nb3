package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N2041_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -573198082752337036L;
	private String BRHNAME;
	private String UID;
	private String BRHCOD;
	private String ACNTYPE;
	private String CUSIDN;
	private String ERRCODE;
	private String NAME;
	
	public String getBRHNAME() {
		return BRHNAME;
	}
	public void setBRHNAME(String bRHNAME) {
		BRHNAME = bRHNAME;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getBRHCOD() {
		return BRHCOD;
	}
	public void setBRHCOD(String bRHCOD) {
		BRHCOD = bRHCOD;
	}
	public String getACNTYPE() {
		return ACNTYPE;
	}
	public void setACNTYPE(String aCNTYPE) {
		ACNTYPE = aCNTYPE;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getERRCODE() {
		return ERRCODE;
	}
	public void setERRCODE(String eRRCODE) {
		ERRCODE = eRRCODE;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
}
