package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class IdGateN915_REST_RQ extends BaseRestBean_IDGATE implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4554065311884526127L;
	private String	CUSIDN	;
	private String	OPTION = "1"	; // 1:啟用2:關閉 預設放啟用 
	private String	TYPE	;
	private String ISSUER;	// 晶片卡發卡行庫
	private String ACNNO;	// 晶片卡帳號
	private String ICDTTM;	// 晶片卡日期時間
	private String ICSEQ;	// 晶片卡序號
	private String ICMEMO;	// 晶片卡備註欄
	private String FGTXWAY;	// 交易機制
	private String iSeqNo;	// iSeqNo
	private	String CHIP_ACN;
	private	String TAC;
	private	String TRMID;
	private	String pkcs7Sign;
	
	
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getOPTION() {
		return OPTION;
	}
	public void setOPTION(String oPTION) {
		OPTION = oPTION;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getISSUER() {
		return ISSUER;
	}
	public void setISSUER(String iSSUER) {
		ISSUER = iSSUER;
	}
	public String getACNNO() {
		return ACNNO;
	}
	public void setACNNO(String aCNNO) {
		ACNNO = aCNNO;
	}
	public String getICDTTM() {
		return ICDTTM;
	}
	public void setICDTTM(String iCDTTM) {
		ICDTTM = iCDTTM;
	}
	public String getICSEQ() {
		return ICSEQ;
	}
	public void setICSEQ(String iCSEQ) {
		ICSEQ = iCSEQ;
	}
	public String getICMEMO() {
		return ICMEMO;
	}
	public void setICMEMO(String iCMEMO) {
		ICMEMO = iCMEMO;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getiSeqNo() {
		return iSeqNo;
	}
	public void setiSeqNo(String iSeqNo) {
		this.iSeqNo = iSeqNo;
	}
	public String getCHIP_ACN() {
		return CHIP_ACN;
	}
	public void setCHIP_ACN(String cHIP_ACN) {
		CHIP_ACN = cHIP_ACN;
	}
	public String getTAC() {
		return TAC;
	}
	public void setTAC(String tAC) {
		TAC = tAC;
	}
	public String getTRMID() {
		return TRMID;
	}
	public void setTRMID(String tRMID) {
		TRMID = tRMID;
	}
	public String getPkcs7Sign() {
		return pkcs7Sign;
	}
	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}
	
	
	
	

}
