package tw.com.fstop.web.util;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
import tw.com.fstop.util.SpringBeanFactory;

public class I18nHelper 
{
	private static Logger log = LoggerFactory.getLogger(I18nHelper.class);
	
	private static I18n i18n = SpringBeanFactory.getBean("i18n");
	
	
	/**
	 * 取得計息方式i18n
	 * 
	 * @param INTMTH
	 * @return
	 */
	public static String getINTMTH(String intmth)
	{
		String result = intmth;
		try
		{
			if("0".equals(intmth)) 
			{
				// 機動
				result = i18n.getMsg("LB.Floating");
			}
			else if("1".equals(intmth) || "2".equals(intmth))
			{
				// 固定
				result = i18n.getMsg("LB.Fixed");
			}
		}
		catch (Exception e)
		{
			log.error(ESAPIUtil.vaildLog("getINTMTH_i18n error. parameter >> {}"+ intmth+e));
		}
		return result;
	}
	
	
	/**
	 * 轉存方式i18n
	 * 
	 * @param type1
	 * @return
	 */
	public static String getTYPE1(String type1)
	{
		String result = type1;
		try
		{
			switch(type1) 
			{
			    case "1" : 
			    	// 本息
			    	result = i18n.getMsg("LB.Principal_and_interest");
			        break; 
			    case "2" :
			    	// 本金
			    	result = i18n.getMsg("LB.Principal");
			        break; 
			    default: 
			} 
		}
		catch (Exception e)
		{
			log.error("getTYPE1 error. parameter >> {}", type1, e);
		}
		return result;
	}
	
	
	/**
	 * 申請信用卡進度查詢 審核狀況 TODO i18n
	 * 
	 * @param status
	 * @return
	 */
	public String getNA021_Status(String status)
	{
		String result = status;
		Map<String, Map> argBean = (Map<String, Map>) SpringBeanFactory.getBean("na021");
		Map<String, String> definedStatus = argBean.get("STATUS");
		if(definedStatus.containsKey(status))
		{
			// TODO 待messages_zh_TW.properties新增相應的字串後打開
//			I18n i18n = SpringBeanFactory.getBean("i18n");
//			result = i18n.getMsg(status);
			
			// TODO 待messages_zh_TW.properties新增相應的字串後移除
			result = definedStatus.get(status);
		}
		return result;
	}

}
