package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N074_REST_RQ extends BaseRestBean_TW implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6618798027466799466L;

	private String ISSUER; // 晶片卡發卡行庫

	private String NeedSHA1;

	private String FLAG;// 0：非約定，1：約定

	private String TRANSEQ;// 交易序號

	private String ACNNO;// 晶片卡帳號

	private String ICDTTM;// 晶片卡日期時間

	private String ICSEQ;// 晶片卡序號

	private String ICMEMO;// 晶片卡備註欄

	private String TAC_Length;// TAC DATA Length

	private String TAC;// TAC DATA

	private String TAC_120space;// C DATA Space

	private String TRMID;// 端末設備查核碼

	private String iSeqNo; // iSeqNo

	private String pkcs7Sign;// IKEY

	private String jsondc;// IKEY

	private String PINNEW;// 網路銀行密碼（新）SSL 用

	private String FGTXWAY; // 交易機制 0:SSL ,1:ikey, 2:晶片金融卡 
	
	private String CMTRMEMO;// 交易備註

	private String CMTRMAIL;// 通訊錄

	private String CMMAILMEMO;// 摘要內容

	private String ADOPID;// 交易代號

	private String CUSIDN;// 統一編號

	private String FGSVACNO;// 約定非約定註記 1 約定帳號, 2 非約定帳號

	private String DPAGACNO;// 1 常用帳號 約轉轉入帳號 JSON ex{"BNKCOD":"050","AGREE":"1","ACN":"01062790594"}

	private String DPACNO;// 2 非約定 轉入帳號

	private String ACN;// 轉出帳號

	private String INTSACN;// 轉入帳號

	private String AMOUNT;// 轉帳金額
	
	private String FDPACC;// 存單種類
	
	private String INTMTH;// 計息方式
	 // 到期是否轉期
	private String CODE;// 到期是否轉期 是否續存（’0’不續存，’1’續存）

	private String FGSVTYPE;// 轉存方式 1: 本金轉期，利息轉入綜存活存 2:本金及利息一併轉期

	private String TXTIMES;// 轉期期數
	// 到期是否轉期-END
	
	// 存款期別或指定到期日
	private String FGTDPERIOD;// 存款期別或指定到期日 1:期別, 2:指定到期日
	
	private String TERM;// 存單期別

	private String CMDATE1;// 指定到期日
	// 存款期別或指定到期日 -END
	private String FGTXDATE;// 預約日期註記 1:即時 2:預約

	private String CMDATE;// 預約日期 

	//IDGATE 用參數---start---
	private String sessionID;
	private String idgateID;
	private String txnID;
	//IDGATE 用參數---end---
	
	public String getISSUER()
	{
		return ISSUER;
	}

	public void setISSUER(String iSSUER)
	{
		ISSUER = iSSUER;
	}

	public String getNeedSHA1()
	{
		return NeedSHA1;
	}

	public void setNeedSHA1(String needSHA1)
	{
		NeedSHA1 = needSHA1;
	}

	public String getFLAG()
	{
		return FLAG;
	}

	public void setFLAG(String fLAG)
	{
		FLAG = fLAG;
	}

	public String getTRANSEQ()
	{
		return TRANSEQ;
	}

	public void setTRANSEQ(String tRANSEQ)
	{
		TRANSEQ = tRANSEQ;
	}

	public String getACNNO()
	{
		return ACNNO;
	}

	public void setACNNO(String aCNNO)
	{
		ACNNO = aCNNO;
	}

	public String getICDTTM()
	{
		return ICDTTM;
	}

	public void setICDTTM(String iCDTTM)
	{
		ICDTTM = iCDTTM;
	}

	public String getICSEQ()
	{
		return ICSEQ;
	}

	public void setICSEQ(String iCSEQ)
	{
		ICSEQ = iCSEQ;
	}

	public String getICMEMO()
	{
		return ICMEMO;
	}

	public void setICMEMO(String iCMEMO)
	{
		ICMEMO = iCMEMO;
	}

	public String getTAC_Length()
	{
		return TAC_Length;
	}

	public void setTAC_Length(String tAC_Length)
	{
		TAC_Length = tAC_Length;
	}

	public String getTAC()
	{
		return TAC;
	}

	public void setTAC(String tAC)
	{
		TAC = tAC;
	}

	public String getTAC_120space()
	{
		return TAC_120space;
	}

	public void setTAC_120space(String tAC_120space)
	{
		TAC_120space = tAC_120space;
	}

	public String getTRMID()
	{
		return TRMID;
	}

	public void setTRMID(String tRMID)
	{
		TRMID = tRMID;
	}

	public String getFGTXWAY()
	{
		return FGTXWAY;
	}

	public void setFGTXWAY(String fGTXWAY)
	{
		FGTXWAY = fGTXWAY;
	}

	public String getiSeqNo()
	{
		return iSeqNo;
	}

	public void setiSeqNo(String iSeqNo)
	{
		this.iSeqNo = iSeqNo;
	}

	public String getPkcs7Sign()
	{
		return pkcs7Sign;
	}

	public void setPkcs7Sign(String pkcs7Sign)
	{
		this.pkcs7Sign = pkcs7Sign;
	}

	public String getJsondc()
	{
		return jsondc;
	}

	public void setJsondc(String jsondc)
	{
		this.jsondc = jsondc;
	}

	public String getPINNEW()
	{
		return PINNEW;
	}

	public void setPINNEW(String pINNEW)
	{
		PINNEW = pINNEW;
	}

	public String getCMTRMEMO()
	{
		return CMTRMEMO;
	}

	public void setCMTRMEMO(String cMTRMEMO)
	{
		CMTRMEMO = cMTRMEMO;
	}

	public String getCMTRMAIL()
	{
		return CMTRMAIL;
	}

	public void setCMTRMAIL(String cMTRMAIL)
	{
		CMTRMAIL = cMTRMAIL;
	}

	public String getCMMAILMEMO()
	{
		return CMMAILMEMO;
	}

	public void setCMMAILMEMO(String cMMAILMEMO)
	{
		CMMAILMEMO = cMMAILMEMO;
	}

	public String getADOPID()
	{
		return ADOPID;
	}

	public void setADOPID(String aDOPID)
	{
		ADOPID = aDOPID;
	}

	public String getCUSIDN()
	{
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN)
	{
		CUSIDN = cUSIDN;
	}

	public String getFGSVACNO()
	{
		return FGSVACNO;
	}

	public void setFGSVACNO(String fGSVACNO)
	{
		FGSVACNO = fGSVACNO;
	}

	public String getDPAGACNO()
	{
		return DPAGACNO;
	}

	public void setDPAGACNO(String dPAGACNO)
	{
		DPAGACNO = dPAGACNO;
	}

	public String getDPACNO()
	{
		return DPACNO;
	}

	public void setDPACNO(String dPACNO)
	{
		DPACNO = dPACNO;
	}

	public String getACN()
	{
		return ACN;
	}

	public void setACN(String aCN)
	{
		ACN = aCN;
	}

	public String getINTSACN()
	{
		return INTSACN;
	}

	public void setINTSACN(String iNTSACN)
	{
		INTSACN = iNTSACN;
	}

	public String getFDPACC()
	{
		return FDPACC;
	}

	public void setFDPACC(String fDPACC)
	{
		FDPACC = fDPACC;
	}

	public String getAMOUNT()
	{
		return AMOUNT;
	}

	public void setAMOUNT(String aMOUNT)
	{
		AMOUNT = aMOUNT;
	}

	public String getINTMTH()
	{
		return INTMTH;
	}

	public void setINTMTH(String iNTMTH)
	{
		INTMTH = iNTMTH;
	}

	public String getCODE()
	{
		return CODE;
	}

	public void setCODE(String cODE)
	{
		CODE = cODE;
	}

	public String getFGSVTYPE()
	{
		return FGSVTYPE;
	}

	public void setFGSVTYPE(String fGSVTYPE)
	{
		FGSVTYPE = fGSVTYPE;
	}

	public String getTXTIMES()
	{
		return TXTIMES;
	}

	public void setTXTIMES(String tXTIMES)
	{
		TXTIMES = tXTIMES;
	}

	public String getFGTDPERIOD()
	{
		return FGTDPERIOD;
	}

	public void setFGTDPERIOD(String fGTDPERIOD)
	{
		FGTDPERIOD = fGTDPERIOD;
	}

	public String getTERM()
	{
		return TERM;
	}

	public void setTERM(String tERM)
	{
		TERM = tERM;
	}

	public String getFGTXDATE()
	{
		return FGTXDATE;
	}

	public void setFGTXDATE(String fGTXDATE)
	{
		FGTXDATE = fGTXDATE;
	}

	public String getCMDATE1()
	{
		return CMDATE1;
	}

	public void setCMDATE1(String cMDATE1)
	{
		CMDATE1 = cMDATE1;
	}

	public String getCMDATE()
	{
		return CMDATE;
	}

	public void setCMDATE(String cMDATE)
	{
		CMDATE = cMDATE;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public String getIdgateID() {
		return idgateID;
	}

	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}

	public String getTxnID() {
		return txnID;
	}

	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}
	
	

}
