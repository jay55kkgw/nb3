package tw.com.fstop.nnb.spring.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.comm.bean.PMTInData;
import tw.com.fstop.nnb.custom.annotation.ISTXNLOG;
import tw.com.fstop.nnb.service.Financial_Trial_Service;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.web.util.WebUtil;



@SessionAttributes({
	SessionUtil.CUSIDN,
	SessionUtil.DPUSERNAME,
	SessionUtil.PRINT_DATALISTMAP_DATA,
	SessionUtil.DOWNLOAD_DATALISTMAP_DATA,
	SessionUtil.CONFIRM_LOCALE_DATA,
	SessionUtil.RESULT_LOCALE_DATA,
	SessionUtil.BACK_DATA,
	SessionUtil.TRANSFER_DATA
	})
@Controller
@RequestMapping(value = "/FINANCIAL/TRIAL")
public class Financial_Trial_Controller {

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Financial_Trial_Service financial_trial_service;

	/**
	 * 
	 * 臺幣定期存款試算
	 */
	@ISTXNLOG(value="A4010")
	@RequestMapping(value = "/ntd_tdeposit_trial")
	public String ntd_tdeposit_trial(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		return "/financial_trial/ntd_tdeposit_trial";
	}

	/**
	 * 外匯定期存款利息試算
	 */
	@ISTXNLOG(value="A4030")
	@RequestMapping(value = "/fcy_tdeposit_itrial")
	public String fcy_tdeposit_itrial(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		return "/financial_trial/fcy_tdeposit_itrial";
	}

	/**
	 * 年金計畫試算
	 */
	@ISTXNLOG(value="A4050")
	@RequestMapping(value = "/annuity_plan_trial")
	public String annuity_plan_trial(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		return "/financial_trial/annuity_plan_trial";
	}

	/**
	 * 貸款攤還試算
	 */
	@ISTXNLOG(value="A4040")
	@RequestMapping(value = "/loan_amorti_trial")
	public String loan_amorti_trial(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		return "/financial_trial/loan_amorti_trial";
	}

	/**
	 * 貸款攤還試算結果頁
	 */
	@RequestMapping(value = "/loan_amorti_trial_result")
	public String loan_amorti_trial_result(@RequestParam Map<String, String> reqParam, Model model) {
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//		判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
		BaseResult bs = new BaseResult();
		boolean hasLocale = okMap.containsKey("locale");
		if(hasLocale){
			bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
			Map<String, String> bsData = (Map) bs.getData();
			okMap.putAll(bsData);
		}
		if( ! hasLocale){
			bs.addAllData(okMap); 
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
		}

		log.trace("loan_amorti_trial_result");
		try {

			String payment = null;

			// 總費用年百分率
			double totalCostRate = 0.0;
			// 總利息
			double totalInterest = 0.0;
			// 攤還方式
			String repaymentType = okMap.get("selPayment");
			// 貸款金額(本金)
			String strLoan = okMap.get("loan");
			// 寬限期(月)
			String strPrincipalGrace = okMap.get("principalGrace");
			// 第一段利率
			String strYrate1 = okMap.get("yrate1");
			// 第一段期數(月)
			String strMperiod1 = okMap.get("mperiod1");
			// 第二段利率
			String strYrate2 = okMap.get("yrate2");
			// 第二段期數(月)
			String strMperiod2 = okMap.get("mperiod2");
			// 第三段利率
			String strYrate3 = okMap.get("yrate3");
			// 第三段期數(月)
			String strMperiod3 = okMap.get("mperiod3");
			// 其它費用
			String strOtherfee = okMap.get("otherfee");

			PMTInData inData = new PMTInData();
			int totalPeriod = 0;

			if ((repaymentType.equals("1") || repaymentType.equals("2") || repaymentType.equals("3"))
					&& !strMperiod1.equals("0") && !strLoan.equals("0")) {
				inData.setRepaymentType(Integer.parseInt(repaymentType));
				inData.setLoan(Double.parseDouble(strLoan));
				inData.setPrincipalGrace(Integer.parseInt(strPrincipalGrace));
				inData.setYRate1(Double.parseDouble(strYrate1));
				inData.setYRate2(Double.parseDouble(strYrate2));
				inData.setYRate3(Double.parseDouble(strYrate3));
				inData.setPeriod1(Integer.parseInt(strMperiod1));
				inData.setPeriod2(Integer.parseInt(strMperiod2));
				inData.setPeriod3(Integer.parseInt(strMperiod3));
				inData.setOtherFee(Double.parseDouble(strOtherfee));

				// 總期數
				totalPeriod = inData.getPeriod1() + inData.getPeriod2() + inData.getPeriod3();
				inData.setPeriods(new double[totalPeriod + 1]);
				inData.setLoanBalance(new double[totalPeriod + 1]);
				inData.setPrincipal(new double[totalPeriod + 1]);
				inData.setInterest(new double[totalPeriod + 1]);
				inData.setRate(new double[totalPeriod + 1]);
				inData.setCashFlow(new double[totalPeriod + 1]);
				log.trace(ESAPIUtil.vaildLog("repaymentType={}"+repaymentType));
				if (repaymentType.equals("1")) {
					try {
						financial_trial_service.averageAmortizationOfPrincipalAndInterest(inData);
					} catch (Exception e) {
						//Avoid Information Exposure Through an Error Message
						//e.printStackTrace();
						log.error("loan_amorti_trial_result error >> {}",e);
					}
				}
				if (repaymentType.equals("2")) {
					try {
						financial_trial_service.averageAmortizationOfPrincipal(inData);
						log.trace("into");
						
					} catch (Exception e) {
						//Avoid Information Exposure Through an Error Message
						//e.printStackTrace();
						log.error("loan_amorti_trial_result error >> {}",e);
					}
				}
				if (repaymentType.equals("3")) {
					try {
						financial_trial_service.principalRepaidAtMaturity(inData);
					} catch (Exception e) {
						//Avoid Information Exposure Through an Error Message
						//e.printStackTrace();
						log.error("loan_amorti_trial_result error >> {}",e);
					}
				}

				totalCostRate = financial_trial_service.getIRR(inData.getCashFlow());
				totalInterest = inData.getTotalInterest();

				switch (repaymentType) {
				case "1":
//					本息定額攤還
					payment = I18n.getMessage("LB.X0104");
					break;
				case "2":
//					本金定額攤還
					payment = I18n.getMessage("LB.X0105");
					break;
				case "3":
//					到期一次還本
					payment = I18n.getMessage("LB.X0106");
					break;
				}
			}

			model.addAttribute("payment", payment);
			model.addAttribute("loan", NumericUtil.fmtAmount(strLoan, 0));
			model.addAttribute("principalGrace", strPrincipalGrace);
			model.addAttribute("totalPeriod", totalPeriod);
			model.addAttribute("yrate1", strYrate1);
			model.addAttribute("mperiod1", strMperiod1);
			model.addAttribute("yrate2", strYrate2);
			model.addAttribute("mperiod2", strMperiod2);
			model.addAttribute("yrate3", strYrate3);
			model.addAttribute("mperiod3", strMperiod3);
			model.addAttribute("otherfee", NumericUtil.fmtAmount(strOtherfee, 0));
			model.addAttribute("totalInterest", NumericUtil.fmtAmount(Double.toString(Math.round(totalInterest)), 0));
			model.addAttribute("totalCostRate", totalCostRate);

			// 總期數計算(秀表格)
			model.addAttribute("forTotalPeriod",
					(Integer.parseInt(strMperiod1) + Integer.parseInt(strMperiod2) + Integer.parseInt(strMperiod3)));
			// 月份(期數)
			double[] periods = inData.getPeriods();
			model.addAttribute("periods", periods);
			// 貸款結餘
			double[] loanBalance = inData.getLoanBalance();
			//轉換成int
			int[] loanBalanceInt =  new int[loanBalance.length];
			
			for(int i=0; i<loanBalance.length; i++) {
				loanBalanceInt[i] =(int) loanBalance[i];
			}
			
			model.addAttribute("loanBalance", loanBalanceInt);
			// 償還本金
			double[] principal = inData.getPrincipal();
			model.addAttribute("principal", principal);
			// 利息
			double[] interest = inData.getInterest();
			model.addAttribute("interest", interest);
			// 每月繳款(現金流量)
			double[] cashFlow = inData.getCashFlow();
			model.addAttribute("cashFlow", cashFlow);
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("loan_amorti_trial_result error >> {}",e);
		}
		
		return "/financial_trial/loan_amorti_trial_result";
	}
	
}
