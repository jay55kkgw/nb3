package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N215M_1_REST_RQ extends BaseRestBean_OLS implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3235280286383048377L;

	private String UID;
	private String FGTXWAY;
	private String COUNT;
	private String ACNINFO;
	private String DPACCSETID1;
	private String DPACGONAME2;
	private String SelectedRecord;

	//晶片金融卡
	private	String	ACNNO;
	private	String	ICSEQ;
	private	String	iSeqNo;
	private	String	TAC;
	private	String	ISSUER;
	private	String	TRMID;
	private	String	pkcs7Sign;
	private	String	jsondc;
	private String ADOPID;
	
	public String getSelectedRecord() {
		return SelectedRecord;
	}
	public String getACNNO() {
		return ACNNO;
	}
	public String getICSEQ() {
		return ICSEQ;
	}
	public String getiSeqNo() {
		return iSeqNo;
	}
	public String getTAC() {
		return TAC;
	}
	public String getISSUER() {
		return ISSUER;
	}
	public String getTRMID() {
		return TRMID;
	}
	public String getPkcs7Sign() {
		return pkcs7Sign;
	}
	public String getJsondc() {
		return jsondc;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setSelectedRecord(String selectedRecord) {
		SelectedRecord = selectedRecord;
	}
	public void setACNNO(String aCNNO) {
		ACNNO = aCNNO;
	}
	public void setICSEQ(String iCSEQ) {
		ICSEQ = iCSEQ;
	}
	public void setiSeqNo(String iSeqNo) {
		this.iSeqNo = iSeqNo;
	}
	public void setTAC(String tAC) {
		TAC = tAC;
	}
	public void setISSUER(String iSSUER) {
		ISSUER = iSSUER;
	}
	public void setTRMID(String tRMID) {
		TRMID = tRMID;
	}
	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}
	public void setJsondc(String jsondc) {
		this.jsondc = jsondc;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getUID() {
		return UID;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public String getCOUNT() {
		return COUNT;
	}
	public String getACNINFO() {
		return ACNINFO;
	}
	public String getDPACCSETID1() {
		return DPACCSETID1;
	}
	public String getDPACGONAME2() {
		return DPACGONAME2;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public void setCOUNT(String cOUNT) {
		COUNT = cOUNT;
	}
	public void setACNINFO(String aCNINFO) {
		ACNINFO = aCNINFO;
	}
	public void setDPACCSETID1(String dPACCSETID1) {
		DPACCSETID1 = dPACCSETID1;
	}
	public void setDPACGONAME2(String dPACGONAME2) {
		DPACGONAME2 = dPACGONAME2;
	}
}
