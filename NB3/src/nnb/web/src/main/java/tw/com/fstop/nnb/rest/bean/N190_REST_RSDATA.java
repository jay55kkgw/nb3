package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N190_REST_RSDATA  extends BaseRestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3825855037205293058L;
	private String AVGCOST;//參考平均成本
    private String ABEND;//結束代碼
    private String RATIO;//參考報酬率
    private String ACN;//帳號
    private String FEEAMT2;//黃金撲滿欠繳手續費
    private String MKBAL;//參考市值(元)
    private String TSFBAL;//可用餘額(公克)
    private String ODFBAL;//質借數量
    private String FEEAMT1;//定期投資欠繳手續費
    private String USERDATA_X100;
    private String GDBAL;//帳面餘額(公克)
	
	public String getAVGCOST() {
		return AVGCOST;
	}
	public void setAVGCOST(String aVGCOST) {
		AVGCOST = aVGCOST;
	}
	public String getABEND() {
		return ABEND;
	}
	public void setABEND(String aBEND) {
		ABEND = aBEND;
	}
	public String getRATIO() {
		return RATIO;
	}
	public void setRATIO(String rATIO) {
		RATIO = rATIO;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getFEEAMT2() {
		return FEEAMT2;
	}
	public void setFEEAMT2(String fEEAMT2) {
		FEEAMT2 = fEEAMT2;
	}
	public String getMKBAL() {
		return MKBAL;
	}
	public void setMKBAL(String mKBAL) {
		MKBAL = mKBAL;
	}
	public String getTSFBAL() {
		return TSFBAL;
	}
	public void setTSFBAL(String tSFBAL) {
		TSFBAL = tSFBAL;
	}
	public String getODFBAL() {
		return ODFBAL;
	}
	public void setODFBAL(String oDFBAL) {
		ODFBAL = oDFBAL;
	}
	public String getFEEAMT1() {
		return FEEAMT1;
	}
	public void setFEEAMT1(String fEEAMT1) {
		FEEAMT1 = fEEAMT1;
	}
	public String getUSERDATA_X100() {
		return USERDATA_X100;
	}
	public void setUSERDATA_X100(String uSERDATA_X100) {
		USERDATA_X100 = uSERDATA_X100;
	}
	public String getGDBAL() {
		return GDBAL;
	}
	public void setGDBAL(String gDBAL) {
		GDBAL = gDBAL;
	}

}
