package tw.com.fstop.nnb.service;

import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.util.DateUtil;

@Service
public class Overseas_Promotion_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	//申請境外信託商品推介
	public BaseResult apply_trust_product(String cusidn)	{
		log.trace("apply_trust_product_result_service");
		BaseResult bsN922 = null;
		BaseResult bsN3231 = null;
		try {
			bsN922 = new BaseResult();
			bsN3231 = new BaseResult();
			bsN922 = N922_REST(cusidn);
			if(bsN922!=null && bsN922.getResult()) {
				bsN3231 = N3231_REST(cusidn);
				if(bsN3231!=null && bsN3231.getResult()) {
					log.debug("Put N3231 into N922 >> ");
					Map<String , Object> callRowN3231 = (Map) bsN3231.getData();
					Map<String , Object> callRowN922 = (Map) bsN922.getData();
					callRowN922.put("INTRO", callRowN3231.get("INTRO"));
					callRowN922.put("UID", cusidn);
					callRowN922.put("AGE", getAge((String)callRowN922.get("BRTHDY")));
				}else {
					bsN922.setErrorMessage(bsN3231.getMsgCode(), bsN3231.getMessage());
				}
			}
		}catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("apply_trust_product error >> {}",e);
			if(bsN922 != null){
				bsN922.setResult(Boolean.FALSE);
				bsN922.setSYSMessage("ZX99");
			}
		}	
		return  bsN922;
	}
	
	//申請境外信託商品推介
	public BaseResult apply_trust_product_result(String cusidn,Map<String, String> reqParam)	{
		log.trace("apply_trust_product_result_service");
		BaseResult bs = null ;
		try {
			bs = new BaseResult();
			reqParam.put("UID", cusidn);
			int age = Integer.valueOf(reqParam.get("AGE"));
			String mark1 = reqParam.get("MARK1");
			String mark3 = reqParam.get("MARK3");
			String risk7 = reqParam.get("RISK7");
			String intro = reqParam.get("INTRO");
			String degree = reqParam.get("DEGREE");
			String msgword = "";
			if(cusidn.length()==10 ) {
				if(risk7.equals("1") || risk7.equals("2") || risk7.equals("3") || risk7.equals("4")){
					msgword = "LB.X1536";
					bs.setResult(Boolean.TRUE);
				}else if(age > 70 || mark1.equals("Y") || Integer.parseInt(degree) == 6){
					msgword = "LB.X1537";
					bs.setResult(Boolean.TRUE);
				}else {
					bs = N323_REST(reqParam);
					msgword = "LB.X1538";
				}
			}else {
				if(risk7.equals("1") || risk7.equals("2") || risk7.equals("3") || risk7.equals("4")){
					msgword = "LB.X1536";
					bs.setResult(Boolean.TRUE);
				}else if(!mark3.equals("Y")){
					msgword = "LB.X2227";
					bs.setResult(Boolean.TRUE);
				}else {
					bs = N323_REST(reqParam);
					msgword = "LB.X1538";
				}
			}
			log.trace(msgword);
			bs.addData("MSGWORD", msgword);
		}catch(Exception e) {
//			e.printStackTrace();
			log.error("time_deposit",e);
		}
		return  bs;
	}
	
	//申請境外信託商品推介
	public BaseResult cancel_trust_product(String cusidn)	{
		log.trace("apply_trust_product_result_service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = N3231_REST(cusidn);
		}catch(Exception e) {
//			e.printStackTrace();
			log.error("time_deposit",e);
		}	
		return  bs;
	}
	
	//申請境外信託商品推介
	public BaseResult cancel_trust_product_result(String cusidn,Map<String, String> reqParam)	{
		log.trace("apply_trust_product_result_service");
		BaseResult bs = null ;
		try {
			bs = new BaseResult();
			reqParam.put("UID", cusidn);
			bs = N323_REST(reqParam);
		}catch(Exception e) {
//			e.printStackTrace();
			log.error("time_deposit",e);
		}
		return  bs;
	}
	
	
	/**
	 * 依據出生日期，算出實際年齡 
	 */	
	public int getAge(String birthDay){
		log.trace("getAge >>");
		int age = 0;
		Date date = new Date();
		String today = DateUtil.getTWDate("");
		log.debug("Today = {}",today);
		log.debug("BirthDay = {}",birthDay);
		int nowyear = Integer.parseInt(today.substring(0,3));
		int nowmonth = Integer.parseInt(today.substring(3,5));
		int nowday = Integer.parseInt(today.substring(5,7));
		int year = Integer.parseInt(birthDay.substring(0,3));
		int month = Integer.parseInt(birthDay.substring(3,5));
		int day = Integer.parseInt(birthDay.substring(5,7));
//		log.debug("now >> {} {} {}",nowyear,nowmonth,nowday);
//		log.debug("bir >> {} {} {}",year,month,day);
		//計算實際年齡
		if(nowmonth > month){
			age = nowyear - year;
		}
		else if(nowmonth == month && nowday >= day){
			age = nowyear - year;
		}
		else{
			age = nowyear - year - 1;
		}
		log.debug("age={}",age);
		
	  	return age;
	}
}
