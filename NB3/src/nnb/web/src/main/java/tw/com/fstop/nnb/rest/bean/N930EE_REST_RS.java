package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N930EE_REST_RS extends BaseRestBean implements Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 9212618584365333820L;

    String DPMYEMAIL;
    String E_BILL;
    String TXDATE;
    String EXPIREDATE;
    
	public String getDPMYEMAIL() {
		return DPMYEMAIL;
	}
	public void setDPMYEMAIL(String dPMYEMAIL) {
		DPMYEMAIL = dPMYEMAIL;
	}
	public String getE_BILL() {
		return E_BILL;
	}
	public void setE_BILL(String e_BILL) {
		E_BILL = e_BILL;
	}
	public String getTXDATE() {
		return TXDATE;
	}
	public void setTXDATE(String tXDATE) {
		TXDATE = tXDATE;
	}
	public String getEXPIREDATE() {
		return EXPIREDATE;
	}
	public void setEXPIREDATE(String eXPIREDATE) {
		EXPIREDATE = eXPIREDATE;
	}
}
