package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N8330_REST_RS  extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4865190566250816233L;

	//查詢時間
	private String CMQTIME;
    
	//rowdata用
	private LinkedList<N8330_REST_RSDATA> REC;

	public String getCMQTIME() {
		return CMQTIME;
	}

	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}

	public LinkedList<N8330_REST_RSDATA> getREC() {
		return REC;
	}

	public void setREC(LinkedList<N8330_REST_RSDATA> rEC) {
		REC = rEC;
	}
	
	

}
