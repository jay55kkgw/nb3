package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class EBPay1_1_REST_RS  extends BaseRestBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5532503972382352589L;
	private String occurMSG;
	private String occurMSG1;
	private String occurMSG1DESC;
	private String CMDATE1;
	private String WAT_NO;
	private String CHKCOD;
	private String AMOUNT;
	private String CARDNUM;
	private String EXPDTA;
	private String CHECKNO;
	private String FEE;
	private String CMQTIME;
	
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getOccurMSG() {
		return occurMSG;
	}
	public String getOccurMSG1() {
		return occurMSG1;
	}
	public String getOccurMSG1DESC() {
		return occurMSG1DESC;
	}
	public String getCMDATE1() {
		return CMDATE1;
	}
	public String getWAT_NO() {
		return WAT_NO;
	}
	public String getCHKCOD() {
		return CHKCOD;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public String getCARDNUM() {
		return CARDNUM;
	}
	public String getEXPDTA() {
		return EXPDTA;
	}
	public String getCHECKNO() {
		return CHECKNO;
	}
	public String getFEE() {
		return FEE;
	}
	public void setOccurMSG(String occurMSG) {
		this.occurMSG = occurMSG;
	}
	public void setOccurMSG1(String occurMSG1) {
		this.occurMSG1 = occurMSG1;
	}
	public void setOccurMSG1DESC(String occurMSG1DESC) {
		this.occurMSG1DESC = occurMSG1DESC;
	}
	public void setCMDATE1(String cMDATE1) {
		CMDATE1 = cMDATE1;
	}
	public void setWAT_NO(String wAT_NO) {
		WAT_NO = wAT_NO;
	}
	public void setCHKCOD(String cHKCOD) {
		CHKCOD = cHKCOD;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	public void setCARDNUM(String cARDNUM) {
		CARDNUM = cARDNUM;
	}
	public void setEXPDTA(String eXPDTA) {
		EXPDTA = eXPDTA;
	}
	public void setCHECKNO(String cHECKNO) {
		CHECKNO = cHECKNO;
	}
	public void setFEE(String fEE) {
		FEE = fEE;
	}
	
}
