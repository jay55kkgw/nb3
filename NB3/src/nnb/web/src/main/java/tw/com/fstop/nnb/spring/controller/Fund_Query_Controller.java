package tw.com.fstop.nnb.spring.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.aop.Dialog;
import tw.com.fstop.nnb.service.Fund_Query_Service;
import tw.com.fstop.nnb.service.Fund_Transfer_Service;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.web.util.WebUtil;

/**
 * 基金查詢交易的Controller
 */
@SessionAttributes({
	SessionUtil.CUSIDN,
	SessionUtil.DPUSERNAME,
	SessionUtil.PRINT_DATALISTMAP_DATA,
	SessionUtil.DOWNLOAD_DATALISTMAP_DATA,
	SessionUtil.CONFIRM_LOCALE_DATA,
	SessionUtil.RESULT_LOCALE_DATA,
	SessionUtil.BACK_DATA,
	SessionUtil.TRANSFER_DATA
	})
@Controller
@RequestMapping(value = "/FUND/QUERY")
public class Fund_Query_Controller{
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private Fund_Query_Service fund_query_service;
	@Autowired
	private Fund_Transfer_Service fund_transfer_service;
	

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/profitloss_balance")
	@Dialog(ADOPID = "C012")
	public String profitloss_balance(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		BaseResult bs = null;
		Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
		log.debug("profitloss_balance");		
		try{
			bs = new BaseResult();

			String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
			log.debug("cusidn={}",cusidn);
			String dpusername = (String)SessionUtil.getAttribute(model,SessionUtil.DPUSERNAME,null);
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");

			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale: >>"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			} else {
				//清空session
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA,"");
				//do something
				bs = fund_query_service.query_profitloss_balance_data(cusidn,dpusername);
				log.debug("FINAL_BSdata >>{}",bs.getData());
				//存入session
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("profitloss_balance error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				model.addAttribute("bs",bs);
				target = "/fund/fund_profitloss_data";
				//列印會用到，存在SESSION-----原本在service
				List<Map<String,Object>> dataListMap = new ArrayList<Map<String,Object>>();
				Map<String,Object> rowMap = new HashMap<String,Object>();
				rowMap.put("rows",((Map<String, String>) bs.getData()).get("rows"));
				rowMap.put("secondRows",((Map<String, String>) bs.getData()).get("secondRows"));
				rowMap.put("thirdRows",((Map<String, String>) bs.getData()).get("thirdRows"));
				dataListMap.add(rowMap);
				SessionUtil.addAttribute(model,SessionUtil.PRINT_DATALISTMAP_DATA,dataListMap);
			}else {
				bs.setPrevious("/INDEX/index"); 
				model.addAttribute(BaseResult.ERROR,bs);
			}

		}
		return target;
	}
	
	// 海外債券餘額及損益查詢
	@RequestMapping(value = "/oversea_balance")
	@Dialog(ADOPID = "B012")
	public String oversea_balance(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("oversea_balance >>");
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
		try {
			bs = new BaseResult();
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			bs = fund_query_service.oversea_balance_result(cusidn, okMap);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("oversea_balance error >> {}",e);
		} finally {
			if (bs != null && bs.getResult())
			{
				target = "/fund/oversea_balance";
				List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("oversea_balance_result", bs);
			}
			else
			{
				bs.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	// 海外債券資料查詢
	@RequestMapping(value = "/oversea_data")
	public String oversea_data(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs=new BaseResult();
		try {
			//TODO 從Server取得查詢時間?
			bs.addData("Today", DateUtil.getDate("/"));
			bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd hh:mm:ss"));
			if(bs.getData()!=null)bs.setResult(Boolean.TRUE);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("oversea_data error >> {}",e);
		} finally {
			if(bs!=null &&bs.getResult()) {
				model.addAttribute("bs", bs);
				target = "/acct/oversea_data";
			}else {
				bs.setPrevious("");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	// 海外債券資料查詢結果頁
	@RequestMapping(value = "/oversea_data_result")
	public String oversea_data_result(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("oversea_data_result >>");
		try {
			// 解決 Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale") || okMap.containsKey("back");
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale: >>"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null));
				log.debug("LOCALE BSDATA >> {}",CodeUtil.toJson(bs));
			} else {
				//清空session
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA,"");
				//do something
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
				okMap.put("CUSIDN", cusidn);
				bs = fund_query_service.oversea_data_result(cusidn, okMap);
				//存入session
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, bs);
			}
			log.debug("BS DATA >>{}",CodeUtil.toJson(bs));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("oversea_data_result error >> {}",e);
		} finally {
			if(bs!=null && bs.getResult()) {
				target = "/acct/oversea_data_result";
				List<Map<String, String>> dataListMap = ((List<Map<String, String>>) ((Map<String, Object>) bs.getData()).get("REC"));
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				log.debug("dataListMap={}",dataListMap);
				model.addAttribute("oversea_data_result", bs);
			}
			else
			{
				bs.setPrevious("/FUND/QUERY/oversea_data");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	//海外債券資料查詢結果頁
	@RequestMapping(value = "/trust_contract_query")
	public String trust_contract_query(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		String target = "/fund/trust_contract_query";
		log.trace("trust_contract_query >>");
		return target;
	}
	/**
	 * 海外債券資料查詢-債券配息資料查詢
	 */
	@RequestMapping(value = "/oversea_data_result_dividend")
	public String oversea_data_result_dividend(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("oversea_data_result >>");
		try {
			// 解決 Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale: >>"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				log.debug("LOCALE BSDATA >> {}",CodeUtil.toJson(bs));
			} else {
				//清空session
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA,"");
				//do something
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
				okMap.put("CUSIDN", cusidn);
				bs = fund_query_service.oversea_data_result_dividend(cusidn, okMap);
				bs.addData("PREVIOUS", okMap.get("PREVIOUS"));
				//存入session
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			log.debug("BS DATA >>{}",CodeUtil.toJson(bs));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("oversea_data_result_dividend error >> {}",e);
		} finally {
			if(bs!=null && bs.getResult()) {
				target = "/acct/oversea_data_result_dividend";
				List<Map<String, String>> dataListMap = ((List<Map<String, String>>) ((Map<String, Object>) bs.getData()).get("REC"));
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				//log.debug("dataListMap={}",dataListMap);
				model.addAttribute("oversea_data_result_dividend", bs);
			}
			else
			{
				bs.setPrevious("/FUND/QUERY/oversea_data_result");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	/**
	 * 海外債券資料查詢-債券歷史資料查詢
	 */
	@RequestMapping(value = "/oversea_data_result_history")
	public String oversea_data_result_history(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("oversea_data_result >>");
		try {
			// 解決 Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale: >>"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				log.debug("LOCALE BSDATA >> {}",CodeUtil.toJson(bs));
			} else {
				//清空session
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA,"");
				//do something
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
				okMap.put("CUSIDN", cusidn);
				bs = fund_query_service.oversea_data_result_history(cusidn, okMap);
				//存入session
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			log.debug("BS DATA >>{}",CodeUtil.toJson(bs));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("oversea_data_result_history error >> {}",e);
		} finally {
			if(bs!=null && bs.getResult()) {
				target = "/acct/oversea_data_result_history";
				List<Map<String, String>> dataListMap = ((List<Map<String, String>>) ((Map<String, Object>) bs.getData()).get("REC"));
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				log.debug("dataListMap={}",dataListMap);
				model.addAttribute("oversea_data_result_history", bs);
			}
			else
			{
				bs.setPrevious("/FUND/QUERY/oversea_data");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}	
		/**
		 * C115 定期投資約定資料查詢
		 * 
		 * @param request
		 * @param response
		 * @param reqParam
		 * @param model
		 * @return
		 */
		@RequestMapping(value = "/regular_invest")
		public String regular_invest_query(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			String target = "/error";
			BaseResult bs = null;
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			try {

				// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");

				if (hasLocale) {
					log.trace(ESAPIUtil.vaildLog("locale: >>"+ okMap.get("locale")));
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
					if (!bs.getResult()) {
						log.trace("bs.getResult() >>{}", bs.getResult());
						throw new Exception();
					}
				} else {
					// 登入時的身分證字號
					String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
					// session有無CUSIDN
					if (!"".equals(cusidn) && cusidn != null) {
						// 解密成明碼
						cusidn = new String(Base64.getDecoder().decode(cusidn));
						okMap.put("CUSIDN", cusidn);
						bs = fund_query_service.invest_query_result(okMap);
						SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
					} else {
						log.error("session no cusidn!!!");
					}
				}
			} catch (Exception e) {
				log.error("fund_invest_query error", e);
			} finally {
				if (bs != null && bs.getResult()) {
				target = "/fund/fund_regular_invest_query_result";
				log.debug("bs_get_data={}", bs.getData());
				List<Map<String, String>> dataListMap = ((List<Map<String, String>>) ((Map<String, Object>) bs.getData()).get("REC"));
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				log.debug("dataListMap={}",dataListMap);
				model.addAttribute("regular_invest_query_result", bs);
				} else {
					String previous = "/INDEX/index";
					bs.setPrevious(previous);
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}
			return target;
		}
		
		
	/**
	 * C013 基金交易資料查詢 進入點
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/transfer_data_query")
	public String transfer_data_query(HttpServletRequest request, HttpServletResponse response,
		@RequestParam Map<String, String> reqParam, Model model) 
	{
		log.debug("transfer_data_query start");
		String target = "/error";
		String previous = "/FUND/QUERY/transfer_data_query";
		BaseResult bs = new BaseResult();
		try 
		{
			cleanSession(model);
			
			String CMQTIME = DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss");
			bs.addData("CMQTIME", CMQTIME);
			bs.setResult(true);
			
			model.addAttribute("transfer_data_query", bs);
			// 預設查詢日期
			model.addAttribute("SystemDate", DateUtil.getCurentDateTime("yyyy/MM/dd"));
		} 
		catch (Exception e) 
		{
			log.error("transfer_data_query error", e);
		} 
		finally 
		{
			if (bs != null && bs.getResult()) 
			{
				target = "/fund/transfer_data_query";
			} 
			else 
			{
				bs.setPrevious(previous);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * C013 基金交易資料查詢 結果頁
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/transfer_data_query_result")
	public String transfer_data_query_result(HttpServletRequest request, HttpServletResponse response,
		@RequestParam Map<String, String> reqParam, Model model) 
	{
		log.debug("transfer_data_query_result start");
		String target = "/error";
		String previous = "/FUND/QUERY/transfer_data_query";
		BaseResult bs = new BaseResult();
		try 
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			String cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null));
			cusidn = new String(Base64.getDecoder().decode(cusidn));
			
			log.debug(ESAPIUtil.vaildLog("controller reqParam >>"+ okMap));
			
			// 判斷LOCAL KEY 若有帶值表示切換語系
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				log.debug(ESAPIUtil.vaildLog("locale >>"+ okMap.get("locale")));
				// 讀取session的資料顯示
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
			}
			else
			{
				bs = fund_query_service.transfer_data_query(cusidn, okMap.get("CMSDATE"), okMap.get("CMEDATE"));
			}
			
			model.addAttribute("transfer_data_query_result", bs);
			
			if(bs != null && bs.getResult())
			{
				Map bsData = (Map) bs.getData();
				List<Map> rec = (List<Map>) bsData.get("REC");
				// 將list存入session 讓列印頁顯示資料
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, rec);
				// 儲存切換語系要用的資料
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} 
		catch (Exception e) 
		{
			log.error("transfer_data_query_result error", e);
		} 
		finally 
		{
			if (bs != null && bs.getResult()) 
			{
				target = "/fund/transfer_data_query_result";
			} 
			else 
			{
				// 回功能首頁
				bs.setNext(previous);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * 歷史交易明細 進入點
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/transfer_data_query_history")
	public String transfer_data_query_history(HttpServletRequest request, HttpServletResponse response,
		@RequestParam Map<String, String> reqParam, Model model) 
	{
		log.debug("transfer_data_query_history start");
		String target = "/error";
		BaseResult bs = new BaseResult();
		try 
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			String cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null));
			cusidn = new String(Base64.getDecoder().decode(cusidn));
			//Avoid Privacy Violation 
			String cdNo = okMap.get("CDNO");
			
			log.debug(ESAPIUtil.vaildLog("controller reqParam >>"+ okMap));
			
			// 判斷LOCAL KEY 若有帶值表示切換語系
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				log.debug(ESAPIUtil.vaildLog("locale >>"+ okMap.get("locale")));
				// 讀取session的資料顯示
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null));
			}
			else
			{
				bs = fund_query_service.transfer_data_query_history(cusidn, cdNo);
			}
			
			model.addAttribute("transfer_data_query_history", bs);
			
			if(bs != null && bs.getResult())
			{
				Map bsData = (Map) bs.getData();
				List<Map> rec = (List<Map>) bsData.get("REC");
				// 將list如入session 讓列印頁顯示資料
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, rec);
				// 儲存下載要用的資料
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rec);
				// 儲存切換語系要用的資料
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
			}
		} 
		catch (Exception e) 
		{
			log.error("transfer_data_query_history error", e);
		} 
		finally 
		{
			if (bs != null && bs.getResult()) 
			{
				target = "/fund/transfer_data_query_history";
			} 
			else 
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * 單筆交易明細查詢 進入點
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/transfer_data_query_one")
	public String transfer_data_query_one(HttpServletRequest request, HttpServletResponse response,
		@RequestParam Map<String, String> reqParam, Model model) 
	{
		log.debug("transfer_data_query_one start");
		String target = "/error";
		String tail = "";
		// 交易類別
		int fundType = 0;
		// 交易類別Key
		String key = "FUNDTYPE";
		
		BaseResult bs = new BaseResult();
		try 
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
						
			String cusidn = String.valueOf(SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null));
			cusidn = new String(Base64.getDecoder().decode(cusidn));
			
			// 判斷LOCAL KEY 若有帶值表示切換語系
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				log.debug(ESAPIUtil.vaildLog("locale >>"+ okMap.get("locale")));
				// 讀取session的資料顯示
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null));
				// 取得交易類別
				Map<String, String> baData = (Map) bs.getData();
				fundType = Integer.valueOf(baData.get(key));
			}
			else
			{
				// 取得交易類別
				fundType = Integer.valueOf(okMap.get(key));
				// 發送電文
				bs = fund_query_service.transfer_data_query_one(cusidn, okMap);
			}
			
			if(bs != null && bs.getResult() && hasLocale == false)
			{
				// 儲存交易類別
				bs.addData(key, okMap.get(key));
				// 儲存切換語系要用的資料
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
			}
			
			model.addAttribute("transfer_data_query_one", bs);
			
			// 依交易類別判斷導向的頁面
			switch(fundType)
			{
				case 10:	// 申購
					tail = "1";
					break;
					
				case 20:	// 轉換
					tail = "2";
					break;
					
				case 30:	// 贖回
					tail = "3";
					break;
					
				case 40:	// 除息
					tail = "4";
					break;
					
				case 50:	// 除權
					tail = "5";
					break;
					
				case 60:	// 合併
					tail = "6";
					break;
					
				case 70:	// 分割
					tail = "7";
					break;
					
				default:
					log.error("找不到對應的交易類別 FUNDTYPE >> {}", fundType);
			}
			
		}
		catch (Exception e) 
		{
			log.error("transfer_data_query_one error", e);
		} 
		finally 
		{
			if (bs != null && bs.getResult()) 
			{
				target = "/fund/transfer_data_query_one_" + tail;
				log.debug("target >> {}", target);
			} 
			else 
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * N381預約交易查詢/取消(起始頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/fund_reservation_query")
	public String fund_reservation_query(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		BaseResult bs = null;
		log.trace("fund_reservation_query >>");
		try {
			log.trace(ESAPIUtil.vaildLog("fund_reservation_query reqParam>>>>>{}"+reqParam));
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.trace(" call_N922 bs.getResult>> {}", bs.getResult());
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("fund_reservation_query.cusidn >>{}", cusidn);
				bs = fund_query_service.fund_reservation_query(cusidn, okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		}
		catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_reservation_query error >> {}",e);
		}finally {
			if(bs != null && bs.getResult()) 
			{
				target = "/fund/fund_reservation_query";
				Map<String, Object> bsData = (Map) bs.getData();
				log.debug("bsData" + bsData);
				
				model.addAttribute("fund_reservation_query", bs);
			} else {
				bs.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * N382預約交易查詢/取消(申購資料頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/fund_reservation_purchase")
	public String fund_reservation_purchase(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		BaseResult bs = null;
		log.trace("fund_reservation_purchase >>");
		try {
			log.trace(ESAPIUtil.vaildLog("fund_reservation_purchase reqParam>>>>>{}"+reqParam));
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.trace(" call_N382 bs.getResult>> {}", bs.getResult());
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("fund_reservation_purchase.cusidn >>{}", cusidn);
				bs = fund_query_service.fund_reservation_purchase(cusidn, okMap);
				bs.addData("NAME", WebUtil.hideusername1Convert(reqParam.get("NAME")));
				bs.addData("ACN1", reqParam.get("ACN1"));
				bs.addData("ACN2", reqParam.get("ACN2"));
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		}
		catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_reservation_purchase error >> {}",e);
		}finally {
			if(bs != null && bs.getResult()) 
			{
				target = "/fund/fund_reservation_purchase";
				Map<String, Object> bsData = (Map) bs.getData();
//				bs.addData("NAME", WebUtil.hideUsername(reqParam.get("NAME")));
//				bs.addData("ACN1", reqParam.get("ACN1"));
//				bs.addData("ACN2", reqParam.get("ACN2"));
				log.debug("bsData" + bsData);
				List<Map<String,String>> rows = (List<Map<String,String>>) bsData.get("REC");
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, rows);
				model.addAttribute("fund_reservation_purchase", bs);
			} else {
				bs.setPrevious("/FUND/QUERY/fund_reservation_query");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * N382預約交易查詢/取消(申購確認頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/fund_reservation_purchase_c")
	public String fund_reservation_purchase_c(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		log.trace(ESAPIUtil.vaildLog("fund_reservation_purchase_c  reqParam>>>{}"+reqParam));
		BaseResult bs = new BaseResult();
		try {
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			// 清除切換語系時暫存的資料
//			cleanSession(model);
			Map<String, String> dataMap = new HashMap();
			dataMap.put("ROWDATA", reqParam.get("ROWDATA"));
			if(reqParam.get("locale") != null) {
				dataMap.put("locale", reqParam.get("locale"));
			}
			Map<String, String> okMap = ESAPIUtil.validStrMap(dataMap);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("fund_reservation_purchase_c.cusidn >>{}", cusidn);
				bs = fund_query_service.fund_reservation_purchase_c(cusidn, okMap);
				bs.addData("FDTXTYPE", reqParam.get("FDTXTYPE"));
				bs.addData("ACN1", reqParam.get("ACN1"));
				bs.addData("ACN2", reqParam.get("ACN2"));
				log.trace("GET BS>>>>>>{}",bs.getData());
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		}
		catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_reservation_purchase_c error >> {}",e);
		}
		finally {
			if(bs!=null && bs.getResult()) {
				target = "/fund/fund_reservation_purchase_c";
				model.addAttribute("fund_reservation_purchase_c", bs);
			}else {
				bs.setPrevious("/FUND/QUERY/fund_reservation_purchase");
				model.addAttribute(BaseResult.ERROR, bs);				
			}
		}
		
		return target;
	}
	
	/**
	 * N382預約交易查詢/取消(申購結果頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/fund_reservation_purchase_r")
	public String fund_reservation_purchase_r(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		log.trace(ESAPIUtil.vaildLog("Result reqParam>>>>>{}"+reqParam));
		String target = "/error";
		
		BaseResult bs = new BaseResult();
		try {
			// 清除切換語系時暫存的資料
//			cleanSession(model);
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			log.trace(ESAPIUtil.vaildLog("REQPARAM>>>>>>>>>>" + reqParam));
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("fund_reservation_purchase_r.cusidn >>{}", cusidn);
				bs = fund_query_service.fund_reservation_purchase_r(cusidn, okMap);
				bs.addData("FDTXTYPE", reqParam.get("FDTXTYPE"));
				log.trace("GET BS>>>>>>{}",bs.getData());
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		}
		catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_reservation_purchase_r error >> {}",e);
		}
		finally {
			if(bs!=null && bs.getResult()) {
				target = "/fund/fund_reservation_purchase_r";
//				Map<String, String> bsData = (Map) bs.getData();
//				List<Map<String,String>> rows = (List<Map<String,String>>) bsData.get("REC");
//				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, bsData);
				model.addAttribute("fund_reservation_purchase_r", bs);
			}else {
				bs.setPrevious("/FUND/QUERY/fund_reservation_purchase");
				model.addAttribute(BaseResult.ERROR, bs);				
			}
		}
		
		return target;
	}
	
	/**
	 * N383預約交易查詢/取消(轉換資料頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/fund_reservation_conversion")
	public String fund_reservation_conversion(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		BaseResult bs = null;
		log.trace("fund_reservation_conversion >>");
		try {
			log.trace(ESAPIUtil.vaildLog("fund_reservation_conversion reqParam>>>>>{}"+reqParam));
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.trace(" call_N383 bs.getResult>> {}", bs.getResult());
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("fund_reservation_conversion.cusidn >>{}", cusidn);
				bs = fund_query_service.fund_reservation_conversion(cusidn, okMap);
				bs.addData("NAME", WebUtil.hideusername1Convert(reqParam.get("NAME")));
				bs.addData("ACN1", reqParam.get("ACN1"));
				bs.addData("ACN2", reqParam.get("ACN2"));
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		}
		catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_reservation_conversion error >> {}",e);
		}finally {
			if(bs != null && bs.getResult()) 
			{
				target = "/fund/fund_reservation_conversion";
				Map<String, Object> bsData = (Map) bs.getData();
//				bs.addData("NAME", WebUtil.hideUsername(reqParam.get("NAME")));
//				bs.addData("ACN1", reqParam.get("ACN1"));
//				bs.addData("ACN2", reqParam.get("ACN2"));
				log.debug("bsData" + bsData);
				List<Map<String,String>> rows = (List<Map<String,String>>) bsData.get("REC");
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, rows);
				model.addAttribute("fund_reservation_conversion", bs);
			} else {
				bs.setPrevious("/FUND/QUERY/fund_reservation_query");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * N383預約交易查詢/取消(轉換確認頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/fund_reservation_conversion_c")
	public String fund_reservation_conversion_c(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		
		log.trace(ESAPIUtil.vaildLog("conversion  reqParam>>>{}"+reqParam));
		BaseResult bs = new BaseResult();
		try {
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			// 清除切換語系時暫存的資料
//			cleanSession(model);
			Map<String, String> dataMap = new HashMap();
			dataMap.put("ROWDATA", reqParam.get("ROWDATA"));
			if(reqParam.get("locale") != null) {
				dataMap.put("locale", reqParam.get("locale"));
			}
			Map<String, String> okMap = ESAPIUtil.validStrMap(dataMap);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("fund_reservation_conversion.cusidn >>{}", cusidn);
				bs = fund_query_service.fund_reservation_conversion_c(cusidn, okMap);
				bs.addData("FDTXTYPE", reqParam.get("FDTXTYPE"));
				bs.addData("ACN1", reqParam.get("ACN1"));
				bs.addData("ACN2", reqParam.get("ACN2"));
				log.trace("GET BS>>>>>>{}",bs.getData());
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		}
		catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_reservation_conversion_c error >> {}",e);
		}
		finally {
			if(bs!=null && bs.getResult()) {
				target = "/fund/fund_reservation_conversion_c";
				model.addAttribute("fund_reservation_conversion_c", bs);
			}else {
				bs.setPrevious("/FUND/QUERY/fund_reservation_conversion");
				model.addAttribute(BaseResult.ERROR, bs);				
			}
		}
		return target;
	}
	
	/**
	 * N383預約交易查詢/取消(轉換結果頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/fund_reservation_conversion_r")
	public String fund_reservation_conversion_r(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		log.trace(ESAPIUtil.vaildLog("Result reqParam>>>>>{}"+reqParam));
		String target = "/error";
		
		BaseResult bs = new BaseResult();
		try {
			// 清除切換語系時暫存的資料
//			cleanSession(model);
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			log.trace(ESAPIUtil.vaildLog("REQPARAM>>>>>>>>>>" + reqParam));
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("fund_reservation_conversion_r.cusidn >>{}", cusidn);
				bs = fund_query_service.fund_reservation_conversion_r(cusidn, okMap);
				bs.addData("FDTXTYPE", reqParam.get("FDTXTYPE"));
				log.trace("GET BS>>>>>>{}",bs.getData());
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		}
		catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_reservation_conversion_r error >> {}",e);
		}
		finally {
			if(bs!=null && bs.getResult()) {
				target = "/fund/fund_reservation_conversion_r";
//				Map<String, Object> bsData = (Map) bs.getData();
//				List<Map<String,String>> rows = (List<Map<String,String>>) bsData.get("REC");
//				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, rows);
				model.addAttribute("fund_reservation_conversion_r", bs);
			}else {
				bs.setPrevious("/FUND/QUERY/fund_reservation_conversion");
				model.addAttribute(BaseResult.ERROR, bs);				
			}
		}
		
		return target;
	}
	
	/**
	 * N384預約交易查詢/取消(贖回資料頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/fund_reservation_redemption")
	public String fund_reservation_redemption(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		BaseResult bs = null;
		log.trace("fund_reservation_redemption >>");
		try {
			log.trace(ESAPIUtil.vaildLog("fund_reservation_redemption reqParam>>>>>{}"+reqParam));
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.trace(" call_N384 bs.getResult>> {}", bs.getResult());
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("fund_reservation_redemption.cusidn >>{}", cusidn);
				bs = fund_query_service.fund_reservation_redemption(cusidn, okMap);
				bs.addData("NAME", WebUtil.hideusername1Convert(reqParam.get("NAME")));
				bs.addData("ACN1", reqParam.get("ACN1"));
				bs.addData("ACN2", reqParam.get("ACN2"));
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		}
		catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_reservation_redemption error >> {}",e);
		}finally {
			if(bs != null && bs.getResult()) 
			{
				target = "/fund/fund_reservation_redemption";
				Map<String, Object> bsData = (Map) bs.getData();
//				bs.addData("NAME", WebUtil.hideUsername(reqParam.get("NAME")));
//				bs.addData("ACN1", reqParam.get("ACN1"));
//				bs.addData("ACN2", reqParam.get("ACN2"));
				log.debug("bsData" + bsData);
				List<Map<String,String>> rows = (List<Map<String,String>>) bsData.get("REC");
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, rows);
				model.addAttribute("fund_reservation_redemption", bs);
			} else {
				bs.setPrevious("/FUND/QUERY/fund_reservation_query");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * N384預約交易查詢/取消(贖回確認頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/fund_reservation_redemption_c")
	public String fund_reservation_redemption_c(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		String target = "/error";
		
		log.trace(ESAPIUtil.vaildLog("redemption  reqParam>>>{}"+reqParam));
		BaseResult bs = new BaseResult();
		try {
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			// 清除切換語系時暫存的資料
//			cleanSession(model);
			Map<String, String> dataMap = new HashMap();
			dataMap.put("ROWDATA", reqParam.get("ROWDATA"));
			if(reqParam.get("locale") != null) {
				dataMap.put("locale", reqParam.get("locale"));
			}
			Map<String, String> okMap = ESAPIUtil.validStrMap(dataMap);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("fund_reservation_redemption_c.cusidn >>{}", cusidn);
				bs = fund_query_service.fund_reservation_redemption_c(cusidn, okMap);
				bs.addData("FDTXTYPE", reqParam.get("FDTXTYPE"));
				bs.addData("ACN1", reqParam.get("ACN1"));
				bs.addData("ACN2", reqParam.get("ACN2"));
				log.trace("GET BS>>>>>>{}",bs.getData());
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		}
		catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_reservation_redemption_c error >> {}",e);
		}
		finally {
			if(bs!=null && bs.getResult()) {
				target = "/fund/fund_reservation_redemption_c";
				model.addAttribute("fund_reservation_redemption_c", bs);
			}else {
				bs.setPrevious("/FUND/QUERY/fund_reservation_redemption");
				model.addAttribute(BaseResult.ERROR, bs);				
			}
		}
		return target;
	}
	
	/**
	 * N384預約交易查詢/取消(贖回結果頁)
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/fund_reservation_redemption_r")
	public String fund_reservation_redemption_r(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		log.trace(ESAPIUtil.vaildLog("Result reqParam>>>>>{}"+reqParam));
		String target = "/error";
		
		BaseResult bs = new BaseResult();
		try {
			// 清除切換語系時暫存的資料
//			cleanSession(model);
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			log.trace(ESAPIUtil.vaildLog("REQPARAM>>>>>>>>>>" + reqParam));
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("fund_reservation_redemption_r.cusidn >>{}", cusidn);
				bs = fund_query_service.fund_reservation_redemption_r(cusidn, okMap);
				bs.addData("FDTXTYPE", reqParam.get("FDTXTYPE"));
				log.trace("GET BS>>>>>>{}",bs.getData());
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		}
		catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_reservation_redemption_r error >> {}",e);
		}
		finally {
			if(bs!=null && bs.getResult()) {
				target = "/fund/fund_reservation_redemption_r";
//				Map<String, Object> bsData = (Map) bs.getData();
//				List<Map<String,String>> rows = (List<Map<String,String>>) bsData.get("REC");
//				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, rows);
				model.addAttribute("fund_reservation_redemption_r", bs);
			}else {
				bs.setPrevious("/FUND/QUERY/fund_reservation_redemption");
				model.addAttribute(BaseResult.ERROR, bs);				
			}
		}
		
		return target;
	}
	
	/**
	 * B023
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/oversea_order_query")
	public String oversea_order_query(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) 
	{
		log.trace(ESAPIUtil.vaildLog("oversea_order_query reqParam>>>>>{}"+reqParam));
		String target = "/error";
		
		BaseResult bsN922 = new BaseResult();
		BaseResult bs = new BaseResult();
		try {
			// 清除切換語系時暫存的資料
//			cleanSession(model);
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			String IP = WebUtil.getIpAddr(request);
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("oversea_order_query.cusidn >>{}", cusidn);
				bsN922 = fund_transfer_service.getN922Data(cusidn);
				if(bsN922 != null && bsN922.getResult()) {
					Map<String, Object> bsData = (Map) bsN922.getData();
					String BRHCOD = (String)bsData.get("APLBRH");
					log.debug("N922 BRHCOD >>> " + BRHCOD);
					okMap.put("BRHCOD", BRHCOD);
					okMap.put("IP", IP);
					
					bs = fund_query_service.oversea_order_query(cusidn, okMap);
					log.trace("GET BS>>>>>>{}",bs.getData());
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
				else {
					bs.setPrevious("/INDEX/index");
					bs = bsN922;
					model.addAttribute(BaseResult.ERROR,bsN922);
					return target;
				}
			}
		}
		catch(Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("oversea_order_query error >> {}",e);
		}
		finally {
			if(bs!=null && bs.getResult()) {
				target = "/fund/oversea_order_query";
				model.addAttribute("oversea_order_query", bs);
			}else {
				bs.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bs);				
			}
		}
		
		return target;
	}
	
	/**
	 * 清除暫存session的資料
	 * 
	 * @param model
	 */
	public void cleanSession(Model model)
	{
		// 清除切換語系
		SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null);
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null);
		// 清除回上一頁
		SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, null);
		// 清除print
		SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, null);
		// 下載
		SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, null);
		
		SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, null);
	}
	
}