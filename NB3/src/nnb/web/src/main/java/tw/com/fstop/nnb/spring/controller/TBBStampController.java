package tw.com.fstop.nnb.spring.controller;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller

public class TBBStampController{
	private Logger log = LoggerFactory.getLogger(this.getClass().getName());
	
	@RequestMapping("/tbbstamp")
	public void processRequest(HttpServletRequest request,HttpServletResponse response){
		log.debug("IN processRequest");
		
		try{
			String stampFile = "/com/images/TBBStamp.jpg"; 

			BufferedImage bufferedImage = ImageIO.read(request.getServletContext().getResourceAsStream(stampFile));
			
			Graphics graphics = bufferedImage.getGraphics();
			graphics.setColor(Color.RED);
			graphics.setFont(new Font("Arial Black",Font.PLAIN,30));

			String pattern = "yyyy/MM/dd";
			SimpleDateFormat sdf = new SimpleDateFormat(pattern);
			graphics.drawString(sdf.format(new Date()),960,180);
			graphics.dispose();

            response.setContentType("image/jpeg");
            ImageIO.write(bufferedImage,"jpeg",response.getOutputStream());
        }
        catch(Exception e){
        	//Avoid Information Exposure Through an Error Message
        	//e.printStackTrace();
        	log.error("processRequest error >> {}",e);
        }
	}
}
