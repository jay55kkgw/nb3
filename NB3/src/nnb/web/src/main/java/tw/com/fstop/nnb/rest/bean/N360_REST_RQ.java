package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N360_REST_RQ extends BaseRestBean_OLS implements Serializable {
	
	private static final long serialVersionUID = -2033006981301346693L;
	private String CUSIDN;			// 身份證號
	
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	
}