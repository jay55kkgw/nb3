package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N205_REST_RQ extends BaseRestBean_TW implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7636356474345859923L;

	
	private String CUSIDN;


	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

}
