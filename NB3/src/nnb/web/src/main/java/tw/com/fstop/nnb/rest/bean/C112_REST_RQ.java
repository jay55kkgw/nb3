package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * C112電文RQ
 */
public class C112_REST_RQ extends BaseRestBean_FUND implements Serializable
{
	private static final long serialVersionUID = 6868861910004819383L;

	private String CUSIDN;// 統一編號

	private String ALTERTYPE;//// 若有變更扣款狀態

	private String ALTERTYPE1;//// 若有變更扣款金額

	private String ALTERTYPE2;// //若有變更扣款日期
	
	private String ALTERTYPE3;//

	private String PAYTAG;// 扣款標的

	private String PAYAMT;// 扣款金額

	private String PAYDAY1;// 扣款日期1

	private String PAYDAY2;// 扣款日期2

	private String PAYDAY3;// 扣款日期3

	private String PAYDAY4;// 扣款日期4

	private String PAYDAY5;// 扣款日期5

	private String PAYDAY6;// 扣款日期6

	private String PAYDAY7;// 扣款日期7

	private String PAYDAY8;// 扣款日期8

	private String PAYDAY9;// 扣款日期9

	private String RSKATT;// 投資屬性

	private String RRSK;// 商品風險等級

	private String STOPBEGDAY;// 暫停扣款起日

	private String STOPENDDAY;// 暫停扣款迄日

	private String RESTOREDAY;// 恢復扣款日

	private String CDNO;// 信託號碼

	private String DAMT;//

	private String MIP;//

	private String TYPEFLAG;

	private String DPMYEMAIL;

	private String CMTRMAIL;

	private String PINNEW;
	
	private String FGTXWAY;

	private String IP;
	
	private String SKIP_P;
	
	public String getCUSIDN()
	{
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN)
	{
		CUSIDN = cUSIDN;
	}

	public String getALTERTYPE()
	{
		return ALTERTYPE;
	}

	public void setALTERTYPE(String aLTERTYPE)
	{
		ALTERTYPE = aLTERTYPE;
	}

	public String getALTERTYPE1()
	{
		return ALTERTYPE1;
	}

	public void setALTERTYPE1(String aLTERTYPE1)
	{
		ALTERTYPE1 = aLTERTYPE1;
	}

	public String getALTERTYPE2()
	{
		return ALTERTYPE2;
	}

	public void setALTERTYPE2(String aLTERTYPE2)
	{
		ALTERTYPE2 = aLTERTYPE2;
	}

	public String getALTERTYPE3()
	{
		return ALTERTYPE3;
	}

	public void setALTERTYPE3(String aLTERTYPE3)
	{
		ALTERTYPE3 = aLTERTYPE3;
	}

	public String getPAYTAG()
	{
		return PAYTAG;
	}

	public void setPAYTAG(String pAYTAG)
	{
		PAYTAG = pAYTAG;
	}

	public String getPAYAMT()
	{
		return PAYAMT;
	}

	public void setPAYAMT(String pAYAMT)
	{
		PAYAMT = pAYAMT;
	}

	public String getPAYDAY1()
	{
		return PAYDAY1;
	}

	public void setPAYDAY1(String pAYDAY1)
	{
		PAYDAY1 = pAYDAY1;
	}

	public String getPAYDAY2()
	{
		return PAYDAY2;
	}

	public void setPAYDAY2(String pAYDAY2)
	{
		PAYDAY2 = pAYDAY2;
	}

	public String getPAYDAY3()
	{
		return PAYDAY3;
	}

	public void setPAYDAY3(String pAYDAY3)
	{
		PAYDAY3 = pAYDAY3;
	}

	public String getPAYDAY4()
	{
		return PAYDAY4;
	}

	public void setPAYDAY4(String pAYDAY4)
	{
		PAYDAY4 = pAYDAY4;
	}

	public String getPAYDAY5()
	{
		return PAYDAY5;
	}

	public void setPAYDAY5(String pAYDAY5)
	{
		PAYDAY5 = pAYDAY5;
	}

	public String getPAYDAY6()
	{
		return PAYDAY6;
	}

	public void setPAYDAY6(String pAYDAY6)
	{
		PAYDAY6 = pAYDAY6;
	}

	public String getPAYDAY7()
	{
		return PAYDAY7;
	}

	public void setPAYDAY7(String pAYDAY7)
	{
		PAYDAY7 = pAYDAY7;
	}

	public String getPAYDAY8()
	{
		return PAYDAY8;
	}

	public void setPAYDAY8(String pAYDAY8)
	{
		PAYDAY8 = pAYDAY8;
	}

	public String getPAYDAY9()
	{
		return PAYDAY9;
	}

	public void setPAYDAY9(String pAYDAY9)
	{
		PAYDAY9 = pAYDAY9;
	}

	public String getRSKATT()
	{
		return RSKATT;
	}

	public void setRSKATT(String rSKATT)
	{
		RSKATT = rSKATT;
	}

	public String getRRSK()
	{
		return RRSK;
	}

	public void setRRSK(String rRSK)
	{
		RRSK = rRSK;
	}

	public String getSTOPBEGDAY()
	{
		return STOPBEGDAY;
	}

	public void setSTOPBEGDAY(String sTOPBEGDAY)
	{
		STOPBEGDAY = sTOPBEGDAY;
	}

	public String getSTOPENDDAY()
	{
		return STOPENDDAY;
	}

	public void setSTOPENDDAY(String sTOPENDDAY)
	{
		STOPENDDAY = sTOPENDDAY;
	}

	public String getRESTOREDAY()
	{
		return RESTOREDAY;
	}

	public void setRESTOREDAY(String rESTOREDAY)
	{
		RESTOREDAY = rESTOREDAY;
	}

	public String getDAMT()
	{
		return DAMT;
	}

	public void setDAMT(String dAMT)
	{
		DAMT = dAMT;
	}

	public String getMIP()
	{
		return MIP;
	}

	public void setMIP(String mIP)
	{
		MIP = mIP;
	}

	public String getTYPEFLAG()
	{
		return TYPEFLAG;
	}

	public void setTYPEFLAG(String tYPEFLAG)
	{
		TYPEFLAG = tYPEFLAG;
	}

	public String getDPMYEMAIL()
	{
		return DPMYEMAIL;
	}

	public void setDPMYEMAIL(String dPMYEMAIL)
	{
		DPMYEMAIL = dPMYEMAIL;
	}

	public String getCMTRMAIL()
	{
		return CMTRMAIL;
	}

	public void setCMTRMAIL(String cMTRMAIL)
	{
		CMTRMAIL = cMTRMAIL;
	}

	public String getPINNEW()
	{
		return PINNEW;
	}

	public void setPINNEW(String pINNEW)
	{
		PINNEW = pINNEW;
	}

	public String getCDNO() {
		return CDNO;
	}

	public void setCDNO(String cDNO) {
		CDNO = cDNO;
	}

	public String getFGTXWAY() {
		return FGTXWAY;
	}

	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}

	public String getIP() {
		return IP;
	}

	public void setIP(String iP) {
		IP = iP;
	}

	public String getSKIP_P() {
		return SKIP_P;
	}

	public void setSKIP_P(String sKIP_P) {
		SKIP_P = sKIP_P;
	}
	
}