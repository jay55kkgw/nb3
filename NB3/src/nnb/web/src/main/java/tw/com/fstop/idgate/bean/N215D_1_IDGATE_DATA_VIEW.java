package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.annotations.SerializedName;


public class N215D_1_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -6515274395302860301L;

	@SerializedName(value = "COUNT")
	private String COUNT;

	@SerializedName(value = "REC")
	private List<Map<String,String>> REC;
	
	@Override
	public Map<String, String> coverMap(){
		LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", "約定轉入帳號設定");
		result.put("交易類型", "註銷");
		result.put("註銷筆數", this.COUNT + "筆");
		int count=1;
		for(Map<String, String> eachRec:this.REC) {
			result.put("第 " + count +" 筆", eachRec.get("BNKCOD") +" "+ eachRec.get("ACN") +" "+ eachRec.get("DPGONAME"));
			count++;
		}
		return result;
	}

	public String getCOUNT() {
		return COUNT;
	}

	public void setCOUNT(String cOUNT) {
		COUNT = cOUNT;
	}

	public List<Map<String, String>> getREC() {
		return REC;
	}

	public void setREC(List<Map<String, String>> rEC) {
		REC = rEC;
	}
	
}
