package tw.com.fstop.nnb.spring.controller;

import java.util.Base64;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.custom.annotation.ISTXNLOG;
import tw.com.fstop.nnb.service.Acct_Service;
import tw.com.fstop.nnb.service.Fcy_Acct_Service;
import tw.com.fstop.nnb.service.Fund_Query_Service;
import tw.com.fstop.nnb.service.Gold_Passbook_Service;
import tw.com.fstop.nnb.service.Loan_Query_Service;
import tw.com.fstop.nnb.service.Overview_Service;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;

@SessionAttributes({ SessionUtil.CUSIDN, SessionUtil.PD })
@Controller
@RequestMapping(value = "/OVERVIEW")
public class Overview_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private I18n i18n;
	@Autowired
	private Overview_Service overview_service;
	@Autowired
	private Acct_Service acct_service;
	@Autowired
	private Fcy_Acct_Service fcy_acct_service;
	@Autowired
	private Loan_Query_Service loan_query_service;
	@Autowired
	private Gold_Passbook_Service gold_passbook_Service;
	@Autowired
	private Fund_Query_Service fund_query_service;
	
	// 帳戶總覽
    @ISTXNLOG(value="initview")
	@RequestMapping(value = "/initview")
	public String initview(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.debug("OVERVIEW.initview...");
		String dpoverview = "";
		
		try {
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			dpoverview = overview_service.getTxnuserDPOverview(cusidn);
			dpoverview = dpoverview.replaceAll(" ","").replaceAll("　","");
		} catch (Exception e) {
			log.error("", e);
		} finally {
			model.addAttribute("dpoverview", dpoverview);
		}
		return "/overview/allacct";
	}
	
	// 臺幣活期性存款餘額_AJAX
	@RequestMapping(value = "/allacct_n110aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult call_N110(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("call_N110...");
		BaseResult bs = null;
		String cusidn = "";
		try {
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("cusidn: " + cusidn);
				// N110
				bs = acct_service.balance_query(cusidn);
			}else {
				log.error("session no cusidn!!!");
			}
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	
	// 臺幣定期性存款明細_AJAX
	@RequestMapping(value = "/allacct_n420aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult call_N420(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("call_N420...");
		BaseResult bs = null;
		String cusidn = "";
		try {
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("cusidn: " + cusidn);
				// N420
				bs = acct_service.time_deposit_details(cusidn, "N");
			}else {
				log.error("session no cusidn!!!");
			}
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	
	// 借款明細查詢_AJAX
	@RequestMapping(value = "/allacct_n320aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult call_N320(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("call_N320...");
		BaseResult bs = null;
		try {
    		bs =new  BaseResult();
    		log.trace(ESAPIUtil.vaildLog("N320GO >>{}"+CodeUtil.toJson(reqParam.get("N320GO"))));
    		log.trace(ESAPIUtil.vaildLog("N552GO >>{}"+CodeUtil.toJson(reqParam.get("N552GO"))));
    		if(null==reqParam.get("N320GO")||"".equals(reqParam.get("N320GO"))){
    			reqParam.put("N320GO", "GO");
    		}
    		if(null==reqParam.get("N552GO")||"".equals(reqParam.get("N552GO"))) {
    			reqParam.put("N552GO", "GO");
    		}
    		String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
    		log.debug("cusidn"+cusidn);
    		reqParam.put("CUSIDN", cusidn);
    		reqParam.put("isTxnlLog", "N");
    		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			bs = loan_query_service.loan_detail(okMap);		
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	
	// 外匯活存餘額_AJAX
	@RequestMapping(value = "/allacct_n510aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult call_N510(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("call_N510...");
		BaseResult bs = null;
		String cusidn = "";
		try {
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("cusidn: " + cusidn);
				// N510
				bs = fcy_acct_service.balance_query(cusidn, null, "N");
			}else {
				log.error("session no cusidn!!!");
			}
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	
	// 外幣定存明細_AJAX
	@RequestMapping(value = "/allacct_n530aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult call_N530(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("call_N530...");
		BaseResult bs = null;
		String cusidn = "";
		try {
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("cusidn: " + cusidn);
				// N530
				bs = fcy_acct_service.f_time_deposit_details(cusidn, null, "N");
			}else {
				log.error("session no cusidn!!!");
			}
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	
	// 信用卡總覽_AJAX
	@RequestMapping(value = "/allacct_n810aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult call_N810(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("call_N810...");
		BaseResult bs = null;
		String cusidn = "";
		try {
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("cusidn: " + cusidn);
				// N810
				reqParam.put("CUSIDN", cusidn);
	    		reqParam.put("isTxnlLog", "N");
				bs = overview_service.call_N810(reqParam);
			}else {
				log.error("session no cusidn!!!");
			}
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	
	// 中央登錄債券餘額_AJAX
	@RequestMapping(value = "/allacct_n870aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult call_N870(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("call_N870...");
		BaseResult bs = null;
		String cusidn = "";
		try {
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("cusidn: " + cusidn);
				// N870
				reqParam.put("CUSIDN", cusidn);
				reqParam.put("ACN1", "ALL");
	    		reqParam.put("isTxnlLog", "N");
				bs = overview_service.call_N870(reqParam);
			}else {
				log.error("session no cusidn!!!");
			}
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	
	// 黃金存摺餘額查詢_AJAX
	@RequestMapping(value = "/allacct_n190aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult call_N190(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("call_N190...");
		BaseResult bs = null;
		String cusidn = "";
		try {
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("cusidn: " + cusidn);
				// N190
				reqParam.put("CUSIDN", cusidn);
	    		reqParam.put("isTxnlLog", "N");
				bs = gold_passbook_Service.gold_balance_query(reqParam);
				bs.addData("CUSIDN", cusidn);
			}else {
				log.error("session no cusidn!!!");
			}
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	// 基金存摺餘額查詢_AJAX
	@RequestMapping(value = "/allacct_c012aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult call_C012(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("call_c012...");
		BaseResult bs = null;
		String cusidn = "";
		String dpusername="";
		BaseResult tempbs = new BaseResult();
		try {
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				dpusername =(String)SessionUtil.getAttribute(model,SessionUtil.DPUSERNAME,null);
				log.trace("cusidn: " + cusidn);
				log.trace("dpusername: " + dpusername);
				bs = fund_query_service.query_profitloss_balance_data(cusidn, dpusername, "N");
				log.debug("BS BS DATA >>{}",bs.getData());
				if(bs!=null && bs.getResult()) {
					tempbs.addData("OverviewREC",((Map<String, String>) bs.getData()).get("OverviewREC"));
					tempbs.addData("OverviewREC2",((Map<String, String>) bs.getData()).get("OverviewREC2"));
					tempbs.addData("thirdRows",((Map<String, String>) bs.getData()).get("thirdRows"));
					tempbs.addData("txnFlag",((Map<String, String>) bs.getData()).get("txnFlag"));
					tempbs.addData("SHWD",((Map<String, String>) bs.getData()).get("SHWD"));
					Integer thirddatacount=(int)((Map<String, Object>) bs.getData()).get("thirdDataCount");
					if(thirddatacount>0)tempbs.addData("thirdDataCount",i18n.getMsg("LB.W0942"));
					else tempbs.addData("thirdDataCount","");
					if(tempbs.getData()!=null)tempbs.setResult(true);
					log.debug("AJAX BS DATA >>{}",tempbs.getData());
				}else {
					return bs;
				}
			}else {
				log.error("session no cusidn!!!");
			}
		} catch (Exception e) {
			log.error("",e);
		}
		return tempbs;
	}


	// 海外債券餘額及損益查詢_AJAX
	@RequestMapping(value = "/allacct_b012aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult call_B012(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {

		log.trace("call_B012...");
		BaseResult bs = null;
		String cusidn = "";
		try {
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
	    		reqParam.put("isTxnlLog", "N");
				bs = fund_query_service.oversea_balance_result(cusidn, reqParam);
			}else {
				log.error("session no cusidn!!!");
			}
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	
}
