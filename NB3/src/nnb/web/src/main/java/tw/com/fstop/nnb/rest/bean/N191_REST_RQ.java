package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N191_REST_RQ  extends BaseRestBean_GOLD implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7141308447890999022L;
	private String USERDATA_X100;
	private String CUSIDN;
	private String ACN;
	private String CMSDATE;
	private String CMEDATE;
	private String FGPERIOD;
	
	public String getUSERDATA_X100() {
		return USERDATA_X100;
	}
	public void setUSERDATA_X100(String uSERDATA_X100) {
		USERDATA_X100 = uSERDATA_X100;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getCMSDATE() {
		return CMSDATE;
	}
	public void setCMSDATE(String cMSDATE) {
		CMSDATE = cMSDATE;
	}
	public String getCMEDATE() {
		return CMEDATE;
	}
	public void setCMEDATE(String cMEDATE) {
		CMEDATE = cMEDATE;
	}
	public String getFGPERIOD() {
		return FGPERIOD;
	}
	public void setFGPERIOD(String fGPERIOD) {
		FGPERIOD = fGPERIOD;
	}
	
}
