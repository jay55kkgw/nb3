package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N855decrypt_REST_RQ extends BaseRestBean_TW implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3986957361012609582L;
	private String	DivData ;
	private String	CustIdnBan ;
	private String	CustBillerAcnt ;
	private String	CustBankAcnt ;
	public String getDivData() {
		return DivData;
	}
	public void setDivData(String divData) {
		this.DivData = divData;
	}
	public String getCustIdnBan() {
		return CustIdnBan;
	}
	public void setCustIdnBan(String custIdnBan) {
		CustIdnBan = custIdnBan;
	}
	public String getCustBillerAcnt() {
		return CustBillerAcnt;
	}
	public void setCustBillerAcnt(String custBillerAcnt) {
		CustBillerAcnt = custBillerAcnt;
	}
	public String getCustBankAcnt() {
		return CustBankAcnt;
	}
	public void setCustBankAcnt(String custBankAcnt) {
		CustBankAcnt = custBankAcnt;
	}
}
