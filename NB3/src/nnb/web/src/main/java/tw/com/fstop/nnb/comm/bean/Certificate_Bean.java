package tw.com.fstop.nnb.comm.bean;

public class Certificate_Bean {

	String ap = "NB3";
	String type = "logon";
	String ts ;
	ExtPrincipal extPrincipal;
	
	
	public class ExtPrincipal{
		String account;
		Ext ext;
//		姓名
		String name;
		String type = "1";
		Unit unit ;
		
		
		public String getAccount() {
			return account;
		}
		public void setAccount(String account) {
			this.account = account;
		}
		public Ext getExt() {
			return ext;
		}
		public void setExt(Ext ext) {
			this.ext = ext;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public Unit getUnit() {
			return unit;
		}
		public void setUnit(Unit unit) {
			this.unit = unit;
		}
		
		
		
	}
	
	
	
	public class Ext{
		String ip;

		public String getIp() {
			return ip;
		}

		public void setIp(String ip) {
			this.ip = ip;
		}
		
	}
	
	
	public class Unit{
//		身分證字號
		String name;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
		
	}


	public String getAp() {
		return ap;
	}


	public void setAp(String ap) {
		this.ap = ap;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getTs() {
		return ts;
	}


	public void setTs(String ts) {
		this.ts = ts;
	}


	public ExtPrincipal getExtPrincipal() {
		return extPrincipal;
	}


	public void setExtPrincipal(ExtPrincipal extPrincipal) {
		this.extPrincipal = extPrincipal;
	}


	
	
	
	
	
}
