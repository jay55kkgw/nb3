package tw.com.fstop.nnb.service;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.rest.bean.C013_REST_RQ;
import tw.com.fstop.util.DateUtil;

@Service
public class Card_Sign_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());


	/**
	 * N814 簽署信用卡來電分期專案申請書約款
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult card_sign_result(String cusidn, Map<String, String> reqParam, String name)
	{
		BaseResult bs = null;
		try
		{
			bs = new BaseResult();
			bs = N814_REST(cusidn,reqParam);
			
			if(bs != null && bs.getResult())
			{
				name = name == null ? "":name;
				if(cusidn.substring(1, 2).equals("1")) {
					bs.addData("NAME", name);
					bs.addData("SEX", "LB.D0192_1");
				}else if(cusidn.substring(1, 2).equals("2")) {
					bs.addData("NAME", name);
					bs.addData("SEX", "LB.D0192_2");
				}else {
					bs.addData("NAME", name);
					bs.addData("SEX", "");
				}
			}
		}
		catch (Exception e)
		{
			log.error("transfer_data_query Error", e);
		}
		return bs;
	}
	
	
}
