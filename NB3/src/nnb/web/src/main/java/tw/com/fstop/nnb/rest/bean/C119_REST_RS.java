package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * 
 * 功能說明 :SMART FUND自動贖回設定 回應
 *
 */
public class C119_REST_RS extends BaseRestBean implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5360357442481015229L;

	private String SSUP;// System Supervisory

	private String SNID;// System Network Identifier

	private String ACF;// Address Control Field

	private String MSGTYPE;// Message Type

	private String PROCCODE;// Processing Code

	private String SYSTRACE;// System Trace Audit

	private String TXNDIID;// TXN Destination Institute ID

	private String TXNSIID;// TXN Source Institute ID

	private String TXNIDT;// TXN Initiate Date and Time

	private String RESPCOD;// Response Code

	private String SYNCCHK;// Sync. Check Item

	private String BITMAPCFG;// Bit Map Configuration

	private String CUSIDN;// 統一編號

	private String CDNO;// 信託號碼

	private String PAYTAG;// 扣款標的

	private String FUNDAMT;// 信託金額

	private String FUNDCUR;// 贖回信託金額幣別

	private String NSTOPLOSS;// 新停損設定

	private String NSTOPPROF;// 新停利設定

	private String NOOPT;// 不設定註記

	private String AC202;// 信託業務別

	private String MIP;// 定期不定額註記
	
	private String CMQTIME;//交易時間

	public String getSSUP()
	{
		return SSUP;
	}

	public void setSSUP(String sSUP)
	{
		SSUP = sSUP;
	}

	public String getSNID()
	{
		return SNID;
	}

	public void setSNID(String sNID)
	{
		SNID = sNID;
	}

	public String getACF()
	{
		return ACF;
	}

	public void setACF(String aCF)
	{
		ACF = aCF;
	}

	public String getMSGTYPE()
	{
		return MSGTYPE;
	}

	public void setMSGTYPE(String mSGTYPE)
	{
		MSGTYPE = mSGTYPE;
	}

	public String getPROCCODE()
	{
		return PROCCODE;
	}

	public void setPROCCODE(String pROCCODE)
	{
		PROCCODE = pROCCODE;
	}

	public String getSYSTRACE()
	{
		return SYSTRACE;
	}

	public void setSYSTRACE(String sYSTRACE)
	{
		SYSTRACE = sYSTRACE;
	}

	public String getTXNDIID()
	{
		return TXNDIID;
	}

	public void setTXNDIID(String tXNDIID)
	{
		TXNDIID = tXNDIID;
	}

	public String getTXNSIID()
	{
		return TXNSIID;
	}

	public void setTXNSIID(String tXNSIID)
	{
		TXNSIID = tXNSIID;
	}

	public String getTXNIDT()
	{
		return TXNIDT;
	}

	public void setTXNIDT(String tXNIDT)
	{
		TXNIDT = tXNIDT;
	}

	public String getRESPCOD()
	{
		return RESPCOD;
	}

	public void setRESPCOD(String rESPCOD)
	{
		RESPCOD = rESPCOD;
	}

	public String getSYNCCHK()
	{
		return SYNCCHK;
	}

	public void setSYNCCHK(String sYNCCHK)
	{
		SYNCCHK = sYNCCHK;
	}

	public String getBITMAPCFG()
	{
		return BITMAPCFG;
	}

	public void setBITMAPCFG(String bITMAPCFG)
	{
		BITMAPCFG = bITMAPCFG;
	}

	public String getCUSIDN()
	{
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN)
	{
		CUSIDN = cUSIDN;
	}

	public String getPAYTAG()
	{
		return PAYTAG;
	}

	public void setPAYTAG(String pAYTAG)
	{
		PAYTAG = pAYTAG;
	}

	public String getFUNDAMT()
	{
		return FUNDAMT;
	}

	public void setFUNDAMT(String fUNDAMT)
	{
		FUNDAMT = fUNDAMT;
	}

	public String getFUNDCUR()
	{
		return FUNDCUR;
	}

	public void setFUNDCUR(String fUNDCUR)
	{
		FUNDCUR = fUNDCUR;
	}

	public String getNSTOPLOSS()
	{
		return NSTOPLOSS;
	}

	public void setNSTOPLOSS(String nSTOPLOSS)
	{
		NSTOPLOSS = nSTOPLOSS;
	}

	public String getNSTOPPROF()
	{
		return NSTOPPROF;
	}

	public void setNSTOPPROF(String nSTOPPROF)
	{
		NSTOPPROF = nSTOPPROF;
	}

	public String getNOOPT()
	{
		return NOOPT;
	}

	public void setNOOPT(String nOOPT)
	{
		NOOPT = nOOPT;
	}

	public String getAC202()
	{
		return AC202;
	}

	public void setAC202(String aC202)
	{
		AC202 = aC202;
	}

	public String getMIP()
	{
		return MIP;
	}

	public void setMIP(String mIP)
	{
		MIP = mIP;
	}

	public String getCMQTIME()
	{
		return CMQTIME;
	}

	public void setCMQTIME(String cMQTIME)
	{
		CMQTIME = cMQTIME;
	}

	public String getCDNO() {
		return CDNO;
	}

	public void setCDNO(String cDNO) {
		CDNO = cDNO;
	}



}