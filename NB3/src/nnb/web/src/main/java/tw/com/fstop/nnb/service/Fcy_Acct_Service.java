package tw.com.fstop.nnb.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtil;

@Service
public class Fcy_Acct_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private I18n i18n;


	/**
	 * 外匯活存餘額
	 * 
	 * @param sessionId
	 * @return
	 */
	public BaseResult balance_query(String cusidn, String acn) {
		return balance_query(cusidn, acn, null);
	}
	public BaseResult balance_query(String cusidn, String acn, String isTxnlLog) {

		log.trace("f_balance_query_service");
		BaseResult bs = null;
		BaseResult ck = null;
		try {
			bs = new BaseResult();
			// call ws API
			bs = N510_REST(cusidn, acn, isTxnlLog);
			ck = N920_REST(cusidn, FX_OUT_DEP_ACNO);
			if (bs != null && bs.getResult()) {
				Map<String, Object> callRow = (Map) bs.getData();
				log.trace("callRow>>{}", callRow);
				// Table1內容修改
				ArrayList<Map<String, String>> callTable1 = (ArrayList<Map<String, String>>) callRow.get("ACNInfo");
				
				Map<String, Object> ckData = (Map) ck.getData();
				ArrayList<Map<String, String>> ckrows = (ArrayList<Map<String, String>>) ckData.get("REC");
				
				String isoutACN = null;
				
				for (Map<String, String> map : callTable1) {
					// 可用餘額
					map.put("AVAILABLE", NumericUtil.fmtAmount(map.get("AVAILABLE"), 2));
					// 帳戶餘額
					map.put("BALANCE", NumericUtil.fmtAmount(map.get("BALANCE"), 2));
					String KIND = this.getBeanMapValue("fcyQuery", map.get("ACN").substring(3, 5));
					if(KIND!=null && !KIND.equals("")){
					KIND = i18n.getMsg(KIND);
					}
					map.put("KIND", KIND);	
					for (Map<String, String> ckrow : ckrows) {
//						log.trace("ACN={}",ckrow.get("ACN"));
						if(ckrow.get("ACN") != null)
							isoutACN = "Y";
						else isoutACN = "N";
					}
//					log.debug("isoutACN={}",isoutACN);
					map.put("OUTACN", isoutACN);
				}
				// Table2內容修改
				ArrayList<Map<String, String>> callTable2 = (ArrayList<Map<String, String>>) callRow.get("AVASum");
				for (Map<String, String> mapTwo : callTable2) {
					// 可用餘額小計
					mapTwo.put("AVAILABLE",
							NumericUtil.fmtAmount(new BigDecimal(mapTwo.get("AVAILABLE")).toPlainString(), 2));
				}
				// Table3內容修改
				ArrayList<Map<String, String>> callTable3 = (ArrayList<Map<String, String>>) callRow.get("BALSum");
				for (Map<String, String> mapThree : callTable3) {
					// 存款金額小計
					mapThree.put("BALANCE",
							NumericUtil.fmtAmount(new BigDecimal(mapThree.get("BALANCE")).toPlainString(), 2));
				}
			}
		} catch (Exception e) {
			log.error("Fcy_Acct_Service-balance_query ", e);
		}

		return bs;
	}

	/**
	 * 外匯活存明細輸入頁
	 * 
	 * @param sessionId
	 * @return
	 */
	public BaseResult demand_deposit_detail(String cusidn) {

		log.trace("demand_deposit_detail_service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// call ws API
			bs = N924_REST(cusidn);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("demand_deposit_detail", e);
		}
		return bs;
	}

	/**
	 * 外匯活存明細結果頁
	 * 
	 * @param sessionId
	 * @return
	 */
	public BaseResult demand_deposit_result(String cusidn, Map<String, String> reqParam) {
		log.trace("demand_deposit_result_service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			String acn = "ALL".equals(reqParam.get("ACN")) ? "" : reqParam.get("ACN");//
			reqParam = DateUtil.periodProcessing(reqParam);
			String cmsdate = reqParam.get("cmsdate");
			String cmedate = reqParam.get("cmedate");
			// 建立回傳
			LinkedList<Object> labelList = new LinkedList<>();
			// 總筆數
			int COUNT = 0;
			// call ws API
			bs = N520_REST(cusidn, acn, reqParam);
			if (bs != null && bs.getResult()) {
				Map<String, Object> callRow = (Map) bs.getData();
				ArrayList<Map<String, String>> rowListMap = (ArrayList<Map<String, String>>) callRow.get("REC");
				for (Map<String, String> map : rowListMap) {
					// 日期
					map.put("display_TXDATE", DateUtil.convertDate(2, map.get("TXDATE"), "yyyMMdd", "yyy/MM/dd"));
					// 時間處理
					map.put("display_TRNTIME", DateUtil.formatTime(map.get("TRNTIME")));
					// 金額顯示處理
					// 20200819 修正頁面顯示位數
					if(StrUtil.isEmpty(map.get("FXPAYAMT"))) {
						map.put("display_FXPAYAMT", "");
					}
					else {
						map.put("display_FXPAYAMT", NumericUtil.formatNumberString(map.get("FXPAYAMT"), 2));	
					}
					if(StrUtil.isEmpty(map.get("FXRECAMT"))) {
						map.put("display_FXRECAMT", "");
					}
					else {
						map.put("display_FXRECAMT", NumericUtil.formatNumberString(map.get("FXRECAMT"), 2));
					}
//					map.put("display_FXPAYAMT", NumericUtil.formatNumberString(StrUtil.isEmpty(map.get("FXPAYAMT")) ? "0" : map.get("FXPAYAMT"), 2));
//					map.put("display_FXRECAMT", NumericUtil.formatNumberString(StrUtil.isEmpty(map.get("FXRECAMT")) ? "0" : map.get("FXRECAMT"), 2));
					
					map.put("display_BALANCE", NumericUtil.formatNumberString(map.get("BALANCE"), 2));
					String downloadAMT = format(map.get("AMT"),1);
					String downloadBALANCE = format(map.get("BALANCE"),1);
					String downloadDATA12 = map.get("DATA12");
					
					if(downloadDATA12.indexOf(" ")!=-1) {
						downloadDATA12 = downloadDATA12.replaceAll(" ","");
					}
					map.put("download_AMT",downloadAMT);
					map.put("download_BALANCE",downloadBALANCE);
					map.put("download_DATA12",downloadDATA12);
				}
				log.debug("rowListMap : {}", rowListMap);
				COUNT += rowListMap.size();
				log.debug("COUNT : {}", COUNT);
				// 取得全部的ACN並放入List
				Set<String> acnAllList = new HashSet<String>();
				for (Map<String, String> map : rowListMap) {
					map.get("ACN");
					acnAllList.add(map.get("ACN"));
				}
				log.debug("acnAllList : {}", acnAllList);
				// 根據acnAllList重新放入listAcnMap
				for (String strAcn : acnAllList) {
					LinkedHashMap<String, Object> labelMap = new LinkedHashMap<String, Object>();
					LinkedList<Object> listAcnMap = new LinkedList<>();
					for (Map map : rowListMap) {
						if (strAcn.equals(map.get("ACN"))) {
							listAcnMap.add(map);
						}
					}
					labelMap.put("ACN", strAcn);
					labelMap.put("rowListMap", listAcnMap);
					labelList.add(labelMap);
				}
				log.trace("labelList {}", labelList);
				bs.addData("labelList", labelList);
				bs.addData("COUNT", String.valueOf(COUNT));
				bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				bs.addData("COUNT", String.valueOf(rowListMap.size()));
				bs.addData("cmqdate", cmsdate + " ～ " + cmedate);// 查詢區間
				log.debug("FX_demand_deposit_detail BaseResult getData :{}", bs.getData());
			}
		} catch (Exception e) {
			log.error("demand_deposit", e);
		}
		return bs;
	}

	// 外匯活存明細直接下載
	public BaseResult fxDepositDetaildirectDownload(String cusidn, Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			bs = demand_deposit_result(cusidn, reqParam);
			if (bs != null && bs.getResult()) {
				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap={}", dataMap);
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
				log.debug("rowListMap={}", rowListMap);
				Map<String, Object> parameterMap = new HashMap<String, Object>();

				parameterMap.put("CMQTIME", dataMap.get("CMQTIME"));
				parameterMap.put("cmqdate", dataMap.get("cmqdate"));
				parameterMap.put("COUNT", dataMap.get("COUNT"));

				parameterMap.put("downloadFileName", reqParam.get("downloadFileName"));
				parameterMap.put("downloadType", reqParam.get("downloadType"));
				parameterMap.put("templatePath", reqParam.get("templatePath"));
				parameterMap.put("hasMultiRowData", reqParam.get("hasMultiRowData"));

				String downloadType = reqParam.get("downloadType");
				log.trace(ESAPIUtil.vaildLog("downloadType={}"+ downloadType));

				// EXCEL和OLDEXCEL下載
				if ("EXCEL".equals(downloadType) || "OLDEXCEL".equals(downloadType)) {
					parameterMap.put("headerRightEnd", reqParam.get("headerRightEnd"));
					parameterMap.put("headerBottomEnd", reqParam.get("headerBottomEnd"));
					parameterMap.put("multiRowStartIndex", reqParam.get("multiRowStartIndex"));
					parameterMap.put("multiRowEndIndex", reqParam.get("multiRowEndIndex"));
					parameterMap.put("multiRowCopyStartIndex", reqParam.get("multiRowCopyStartIndex"));
					parameterMap.put("multiRowCopyEndIndex", reqParam.get("multiRowCopyEndIndex"));
					parameterMap.put("multiRowDataListMapKey", reqParam.get("multiRowDataListMapKey"));
					parameterMap.put("rowRightEnd", reqParam.get("rowRightEnd"));
				}
				// TXT下載
				else {
					parameterMap.put("txtHeaderBottomEnd", reqParam.get("txtHeaderBottomEnd"));
					parameterMap.put("txtMultiRowStartIndex", reqParam.get("txtMultiRowStartIndex"));
					parameterMap.put("txtMultiRowEndIndex", reqParam.get("txtMultiRowEndIndex"));
					parameterMap.put("txtMultiRowCopyStartIndex", reqParam.get("txtMultiRowCopyStartIndex"));
					parameterMap.put("txtMultiRowCopyEndIndex", reqParam.get("txtMultiRowCopyEndIndex"));
					parameterMap.put("txtMultiRowDataListMapKey", reqParam.get("txtMultiRowDataListMapKey"));
				}
				bs.addData("parameterMap", parameterMap);
			}

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fxDepositDetaildirectDownload error >> {}",e);
		}
		return bs;
	}

	// 信用狀查詢直接下載
	public BaseResult CreditLetterdirectDownload(String cusidn, Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			log.debug(ESAPIUtil.vaildLog("CreditLetterdirectDownload REQPARAM >> {}"+CodeUtil.toJson(reqParam)));
			bs = credit_letter_query_result(cusidn, reqParam);
			if (bs != null && bs.getResult()) {
				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap={}", dataMap);
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
				log.debug("rowListMap={}", rowListMap);
				Map<String, Object> parameterMap = new HashMap<String, Object>();

				parameterMap.put("CMQTIME", dataMap.get("CMQTIME"));
				parameterMap.put("CMPERIOD", dataMap.get("CMPERIOD"));
				parameterMap.put("CMRECNUM", dataMap.get("CMRECNUM"));
				parameterMap.put("sBAL_excel", dataMap.get("sBAL_excel"));
				parameterMap.put("sBAL_txt", dataMap.get("sBAL_txt"));
				parameterMap.put("sBAL", dataMap.get("sBAL"));
				parameterMap.put("QKIND_TXT", dataMap.get("QKIND_TXT"));
				parameterMap.put("LCNO_JSP", dataMap.get("LCNO_JSP"));
				parameterMap.put("REFNO_JSP", dataMap.get("REFNO_JSP"));

				parameterMap.put("downloadFileName", reqParam.get("downloadFileName"));
				parameterMap.put("downloadType", reqParam.get("downloadType"));
				parameterMap.put("templatePath", reqParam.get("templatePath"));
				// parameterMap.put("hasMultiRowData", reqParam.get("hasMultiRowData"));

				String downloadType = reqParam.get("downloadType");
				log.trace(ESAPIUtil.vaildLog("downloadType={}" + downloadType));

				// EXCEL和OLDEXCEL下載
				if ("EXCEL".equals(downloadType) || "OLDEXCEL".equals(downloadType)) {
					parameterMap.put("headerRightEnd", reqParam.get("headerRightEnd"));
					parameterMap.put("headerBottomEnd", reqParam.get("headerBottomEnd"));
					parameterMap.put("rowStartIndex", reqParam.get("rowStartIndex"));
					parameterMap.put("rowRightEnd", reqParam.get("rowRightEnd"));
					parameterMap.put("footerStartIndex", reqParam.get("footerStartIndex"));
					parameterMap.put("footerEndIndex", reqParam.get("footerEndIndex"));
					parameterMap.put("footerRightEnd", reqParam.get("footerRightEnd"));
				}
				// TXT下載
				else {
					parameterMap.put("txtHeaderBottomEnd", reqParam.get("txtHeaderBottomEnd"));
					parameterMap.put("txtHasRowData", reqParam.get("txtHasRowData"));
					parameterMap.put("txtHasFooter", reqParam.get("txtHasFooter"));
				}
				bs.addData("parameterMap", parameterMap);
			}

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("CreditLetterdirectDownload error >> {}",e);
		}
		return bs;
	}

	// 進口到單查詢直接下載
	public BaseResult ImportOrderdirectDownload(String cusidn, Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			bs = import_order_query_result(cusidn, reqParam);
			if (bs != null && bs.getResult()) {
				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap={}", dataMap);
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
				log.debug("rowListMap={}", rowListMap);
				Map<String, Object> parameterMap = new HashMap<String, Object>();

				parameterMap.put("CMQTIME", dataMap.get("CMQTIME"));
				parameterMap.put("CMPERIOD", dataMap.get("CMPERIOD"));
				parameterMap.put("CMRECNUM", dataMap.get("CMRECNUM"));

				parameterMap.put("downloadFileName", reqParam.get("downloadFileName"));
				parameterMap.put("downloadType", reqParam.get("downloadType"));
				parameterMap.put("templatePath", reqParam.get("templatePath"));
				// parameterMap.put("hasMultiRowData", reqParam.get("hasMultiRowData"));

				String downloadType = reqParam.get("downloadType");
				log.trace(ESAPIUtil.vaildLog("downloadType={}"+downloadType));

				// EXCEL和OLDEXCEL下載
				if ("EXCEL".equals(downloadType) || "OLDEXCEL".equals(downloadType)) {
					parameterMap.put("headerRightEnd", reqParam.get("headerRightEnd"));
					parameterMap.put("headerBottomEnd", reqParam.get("headerBottomEnd"));
					parameterMap.put("rowStartIndex", reqParam.get("rowStartIndex"));
					parameterMap.put("rowRightEnd", reqParam.get("rowRightEnd"));
				}
				// TXT下載
				else {
					parameterMap.put("txtHeaderBottomEnd", reqParam.get("txtHeaderBottomEnd"));
					parameterMap.put("txtHasRowData", reqParam.get("txtHasRowData"));
					parameterMap.put("txtHasFooter", reqParam.get("txtHasFooter"));
				}
				bs.addData("parameterMap", parameterMap);
			}

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("ImportOrderdirectDownload error >> {}",e);
		}
		return bs;
	}

	/**
	 * 外匯定存明細
	 * 
	 * @param sessionId
	 * @return
	 */
	public BaseResult f_time_deposit_details(String cusidn, String acn) {
		return f_time_deposit_details(cusidn, acn, null);
	}
	public BaseResult f_time_deposit_details(String cusidn, String acn, String isTxnlLog) {

		log.trace("time_deposit_details_service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// call ws API
			bs = N530_REST(cusidn, acn, isTxnlLog);
			if (bs != null && bs.getResult()) {
				Map<String, Object> callRow = (Map) bs.getData();
				// 總計金額處理
				ArrayList<String> fcytotmat = new ArrayList<>();
				String fcytotal = null;
				for (int i = 1; i <= 10; i++) {
					// 幣別電文處理
					String amtcuid = (String) callRow.get("AMTCUID_" + i);
					log.trace("amtcuid>>{}", amtcuid);
					// 金額電文處理
					String fxtotamt = (String) callRow.get("FXTOTAMT_" + i);
					if (amtcuid != null && fxtotamt != null) {
						String fcytotamt = new BigDecimal(fxtotamt).toPlainString();
						fcytotal = amtcuid + " " + NumericUtil.fmtAmount(fcytotamt, 2);
						log.trace("fcytotal>>{}", fcytotal);
						fcytotmat.add(fcytotal);
					}
				}
				// 總計金額
				log.trace("fcytotmat>>{}", fcytotmat);
				bs.addData("FCYTOTMAT", fcytotmat);
				
				// REC"電文處理
				ArrayList<Map<String, String>> callTable = (ArrayList<Map<String, String>>) callRow.get("REC");
				ArrayList<Map<String, String>> callRec = new ArrayList<Map<String, String>>();
				Double itr = null;
				log.trace("callTableM>>{}", callTable);
				for (Map<String, String> map : callTable) {
					if ((!"".equals(map.get("ACN"))) && map.get("ACN") != null) {
						// 計息方式
						map.put("INTMTH", getINTMTH(map.get("INTMTH")));
						// 日期
						map.put("DPISDT", DateUtil.convertDate(2, map.get("DPISDT"), "yyyMMdd", "yyy/MM/dd"));
						map.put("DUEDAT", DateUtil.convertDate(2, map.get("DUEDAT"), "yyyMMdd", "yyy/MM/dd"));
						// 金額顯示處理
						map.put("BALANCE", NumericUtil.fmtAmount(map.get("BALANCE"), 2));
						// 利率處理
						map.put("ITR", NumericUtil.fmtAmount(map.get("ITR"), 3));
						// 次數
						if ("999".equals(map.get("AUTXFTM"))) {
							map.put("AUTXFTM", i18n.getMsg("LB.Unlimited"));
						}
						
						//for選單
						if("52".equals(map.get("ACN").substring(3, 5))||"60".equals(map.get("ACN").substring(3, 5))) {
							map.put("OPTIONTYPE", "1");
						}
						//for 快速選單用
						String jsonString = CodeUtil.toJson(map);
						jsonString.replace("\"", "&acute;");
						map.put("MapValue",jsonString.replace("\"", "&quot;"));
						log.trace("test MapValue>>>>>{}",map.get("MapValue"));
						callRec.add(map);
					}
				}
				callRow.put("REC", callRec);
			}
		} catch (Exception e) {
			// e.printStackTrace();
			log.error("time_deposit", e);
		}
		return bs;
	}

	/**
	 * 取得計息方式i18n
	 * 
	 * @param INTMTH
	 * @return
	 */
	public String getINTMTH(String intmth) {
		String result = intmth;
		try {
			if ("0".equals(intmth)) {
				// 機動
				result = i18n.getMsg("LB.Floating");
			} else if ("1".equals(intmth) || "2".equals(intmth)) {
				// 固定
				result = i18n.getMsg("LB.Fixed");
			}
		} catch (Exception e) {
			log.error("getINTMTH_i18n error. parameter >> {}", intmth, e);
		}
		return result;
	}

	/**
	 * 進口到單查詢
	 * 
	 * @param cusidn
	 *            統一編號
	 * @param reqParam
	 * @return
	 */
	public BaseResult import_order_query_result(String cusidn, Map<String, String> reqParam) {
		log.trace("import_order_query_result");
		BaseResult bs = null;

		try {

			bs = new BaseResult();
			String lcno = reqParam.get("LCNO");
			lcno = StrUtil.isEmpty(lcno) ? i18n.getMsg("LB.All") : lcno;//全部
			// call ws API
			bs = N558_REST(cusidn, reqParam);
			if (bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> bsRow = (ArrayList<Map<String, String>>) bsData.get("REC");
				for(Map<String, String> bsList:bsRow) {
					if(bsList.get("RBILLAMT")!=null && !bsList.get("RBILLAMT").equals("") )
						bsList.put("RBILLAMT", NumericUtil.fmtAmount(bsList.get("RBILLAMT"), 2));
					if(bsList.get("RLCAMT")!=null && !bsList.get("RLCAMT").equals("") )
						bsList.put("RLCAMT", NumericUtil.fmtAmount(bsList.get("RLCAMT"), 2));
					bsList.put("RADVFIXR", bsList.get("RADVFIXR") + "%");
					bsList.put("RIBDATE", bsList.get("RIBDATE").replaceAll("-", "/"));
					bsList.put("RINTSTDT", bsList.get("RINTSTDT").replaceAll("-", "/"));
					bsList.put("RINTDUDT", bsList.get("RINTDUDT").replaceAll("-", "/"));
					bsList.put("RCFMDATE", bsList.get("RCFMDATE").replaceAll("-", "/"));
				}
				bs.addData("LCNO", lcno);
				bs.addData("orgLCNO", reqParam.get("LCNO"));
				bs.addData("FDATE", reqParam.get("FDATE"));
				bs.addData("TDATE", reqParam.get("TDATE"));
			}
		} catch (Exception e) {
			log.error("import_order_query_result", e);
		}
		return bs;
	}

	public BaseResult credit_letter_query_result(String cusidn, Map<String, String> reqParam) {
		log.trace("letter_of_credit_query_result");
		BaseResult bs = null;

		try {

			bs = new BaseResult();
			String lcno = reqParam.get("LCNO");
			lcno = StrUtil.isEmpty(lcno) ? i18n.getMsg("LB.All") : lcno;//全部
			String refno = reqParam.get("REFNO");
			refno = StrUtil.isEmpty(refno) ? i18n.getMsg("LB.All") : refno; //全部
			// call ws API
			bs = N550_REST(cusidn, reqParam);
			if (bs != null && bs.getResult()) {

				Map<String, Object> callRow = (Map) bs.getData();
				ArrayList<Map<String, String>> bsRow = (ArrayList<Map<String, String>>) callRow.get("REC");
				for(Map<String, String> bsList:bsRow) {
					bsList.put("RLCTXAMT", NumericUtil.fmtAmount(bsList.get("RLCTXAMT"), 2));
					bsList.put("RLCOSBAL", NumericUtil.fmtAmount(bsList.get("RLCOSBAL"), 2));
					bsList.put("RRCVDATE", bsList.get("RRCVDATE").replaceAll("-", "/"));
					bsList.put("REXPDATE", bsList.get("REXPDATE").replaceAll("-", "/"));
				}
				// 總計金額處理
				String JSPBAL = "";
				String sBAL = "";
				String sBAL_excel = "";
				String sBAL_txt = "";
				ArrayList<Map<String, String>> fcytotmat = new ArrayList<Map<String,String>>();
				String fcytotal = null;
				//FXSUBAMT_是開狀金額總計 FXSUBBAL是信用狀餘額總計
				for (int i = 1; i <= 7; i++) {
					String countString = String.valueOf(i);
					Map <String,String> totmat=new HashMap<>();
					if (callRow.get("BALRLCCCY_" + countString) != null) {
						//LB.D0249->幣別  LB.Total_amount->總計金額
						sBAL += callRow.get("BALRLCCCY_" + countString)+"     "
								+ NumericUtil.fmtAmount((String) callRow.get("FXSUBAMT_" + countString), 2)+";";
						sBAL_excel += callRow.get("BALRLCCCY_" + countString) +"     "
								+ NumericUtil.fmtAmount((String) callRow.get("FXSUBAMT_" + countString), 2)+"\n";
						sBAL_txt += callRow.get("BALRLCCCY_" + countString) +"      "
								+ NumericUtil.fmtAmount((String) callRow.get("FXSUBAMT_" + countString), 2)+"\r\n";
											
						totmat.put("currency",(String) callRow.get("BALRLCCCY_" + countString));
						totmat.put("amount",NumericUtil.fmtAmount((String) callRow.get("FXSUBAMT_" + countString), 2));
						fcytotmat.add(totmat);
					}	
				}
				bs.addData("FDATE", reqParam.get("FDATE"));
				bs.addData("TDATE", reqParam.get("TDATE"));
				bs.addData("QKIND", reqParam.get("QKIND"));
				// 總計金額
				bs.addData("fcytotmat", fcytotmat);
				bs.addData("sBAL", sBAL);
				bs.addData("sBAL_excel", sBAL_excel);
				bs.addData("sBAL_txt", sBAL_txt);
				bs.addData("LCNO", reqParam.get("LCNO"));
				bs.addData("REFNO", reqParam.get("REFNO"));
				bs.addData("LCNO_JSP", lcno);
				bs.addData("REFNO_JSP", refno);

			}
		} catch (Exception e) {
			log.error("letter_of_credit_query_result", e);
		}
		return bs;
	}

	/**
	 * 信用狀通知查詢
	 * 
	 * @param cusidn
	 *            統一編號
	 * @param reqParam
	 * @return
	 */
	public BaseResult letter_of_credit_opening_inquiry_date_result(String cusidn, Map<String, String> reqParam) {
		log.trace("letter_of_credit_opening_inquiry_date_result");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// call ws API
			bs = N551_REST(cusidn, reqParam);
			if (bs != null && bs.getResult()) {
				Map<String, Object> callRow = (Map) bs.getData();
				ArrayList<Map<String, String>> callTable = (ArrayList<Map<String, String>>) callRow.get("REC");
				for (Map<String, String> data : callTable) {

					data.put("RORIGAMT", NumericUtil.fmtAmount(data.get("RORIGAMT"), 2));
					data.put("RLCOS", NumericUtil.fmtAmount(data.get("RLCOS"), 2));
					// 清除空日期
					if (data.get("ROPENDAT").equals("0000/00/00")) {
						data.put("ROPENDAT", "");
					}
					if (data.get("REXPDATE").equals("0000/00/00")) {
						data.put("REXPDATE", "");
					}
					if (data.get("RSHPDATE").equals("0000/00/00")) {
						data.put("RSHPDATE", "");
					}
					data.put("REXPDATE", data.get("REXPDATE").equals("")? i18n.getMsg("LB.D1070_2") : data.get("REXPDATE"));//無
					data.put("RSHPDATE", data.get("RSHPDATE").equals("")? i18n.getMsg("LB.D1070_2") : data.get("RSHPDATE"));//無
				}
				String sATM = "";
				String sBAL = "";
				for (int i = 1; i <= 7; i++) {
					String countString = String.valueOf(i);
					if (callRow.get("BALRLCCCY_" + countString) != null) {
						sATM += callRow.get("BALRLCCCY_" + countString) + " "
								+ NumericUtil.fmtAmount((String) callRow.get("FXSUBAMT_" + countString), 2) + " i18n{LB.W0123} "
								+ callRow.get("FXSUBAMTRECNUM_" + countString) + " i18n{LB.Rows}" + "<br>";
					}
					if (callRow.get("BALRLCCCY_" + countString) != null) {
						sBAL += callRow.get("BALRLCCCY_" + countString) + " "
								+ NumericUtil.fmtAmount((String) callRow.get("FXSUBBAL_" + countString), 2) + " i18n{LB.W0123} "
								+ callRow.get("FXSUBBALRECNUM_" + countString) + " i18n{LB.Rows}" + "<br>";
					}
				}
				sATM = sATM.substring(0, sATM.length() - 4);
				sBAL = sBAL.substring(0, sBAL.length() - 4);
				bs.addData("SATM", sATM);
				log.debug("sATM >> {}", sATM);
				bs.addData("SBAL", sBAL);
				log.debug("sBal >> {}", sBAL);
				bs.addData("LCNO", reqParam.get("LCNO").equals("") ? i18n.getMsg("LB.All") : reqParam.get("LCNO"));
				bs.addData("putLCNO", reqParam.get("LCNO"));
				bs.addData("CMSDATE", reqParam.get("CMSDATE"));
				bs.addData("CMEDATE", reqParam.get("CMEDATE"));
			}
		} catch (Exception e) {
			log.error("letter_of_credit_query_result", e);
		}
		return bs;
	}

	/**
	 * 信用狀通知查詢直接下載
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult letter_of_credit_opening_inquiry_date_ajaxDirectDownload(String cusidn,
			Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			reqParam.put("USERDATA_X50", "");
			reqParam.put("OKOVNEXT", "TRUE");
			bs = letter_of_credit_opening_inquiry_date_result(cusidn, reqParam);
			if (bs != null && bs.getResult()) {
				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap={}", dataMap);
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
				log.debug("rowListMap={}", rowListMap);

				// QUERYNEXT自動回滾
//				String TOPMSG = (String) dataMap.get("QUERYNEXT");
//				log.debug("QUERYNEXT >> {} , is continu >> {}",TOPMSG,TOPMSG != null && !TOPMSG.equals(""));
//				int countStop = 0;
//				while (TOPMSG != null && !TOPMSG.equals("")) {
//					BaseResult bsPut = null;
//					reqParam.put("USERDATA_X50", (String) dataMap.get("USERDATA_X50"));
//					bsPut = letter_of_credit_opening_inquiry_date_result(cusidn, reqParam);
//					Map<String, Object> dataMapPut = (Map) bsPut.getData();
//					log.trace("dataMap={}", dataMapPut);
//					List<Map<String, String>> rowListMapPut = (List<Map<String, String>>) dataMapPut.get("REC");
//					log.debug("rowListMap={}", rowListMapPut);
//					rowListMap.addAll(rowListMapPut);
//					dataMap.put("CMRECNUM", String.valueOf(Integer.valueOf((String) dataMap.get("CMRECNUM"))
//							+ Integer.valueOf((String) dataMapPut.get("CMRECNUM"))));
//					// 將開狀金額小計作加總
//					for (int i = 1; i <= 7; i++) {
//						String countStringPut = String.valueOf(i);
//						if ((String) dataMapPut.get("AMTRLCCCY_" + countStringPut) == null) {
//							continue;
//						}
//						for (int j = 1; j <= 7; j++) {
//							String countString = String.valueOf(j);
//							if (((String) dataMapPut.get("AMTRLCCCY_" + countStringPut))
//									.equals((String) dataMap.get("AMTRLCCCY_" + countString))) {
//								dataMap.put("FXSUBAMTRECNUM_" + countString, String.valueOf(
//										Integer.valueOf((String) dataMap.get("FXSUBAMTRECNUM_" + countString)) + Integer
//												.valueOf((String) dataMapPut.get("FXSUBAMTRECNUM_" + countStringPut))));
//								BigDecimal oldN = new BigDecimal((String) dataMap.get("FXSUBAMT_" + countString));
//								BigDecimal putN = new BigDecimal((String) dataMapPut.get("FXSUBAMT_" + countStringPut));
//								dataMap.put("FXSUBAMT_" + countString, oldN.add(putN).toString());
//								break;
//							} else if (((String) dataMap.get("AMTRLCCCY_" + countString)) == null) {
//								dataMap.put("AMTRLCCCY_" + countString,
//										(String) dataMapPut.get("AMTRLCCCY_" + countStringPut));
//								dataMap.put("FXSUBAMTRECNUM_" + countString,
//										(String) dataMapPut.get("FXSUBAMTRECNUM_" + countStringPut));
//								dataMap.put("FXSUBAMT_" + countString,
//										(String) dataMapPut.get("FXSUBAMT_" + countStringPut));
//								break;
//							}
//						}
//					}
//					// 將信用狀餘額小計作加總
//					for (int i = 1; i <= 7; i++) {
//						String countStringPut = String.valueOf(i);
//						if ((String) dataMapPut.get("AMTRLCCCY_" + countStringPut) == null) {
//							continue;
//						}
//						for (int j = 1; j <= 7; j++) {
//							String countString = String.valueOf(j);
//							if (((String) dataMapPut.get("BALRLCCCY_" + countStringPut))
//									.equals((String) dataMap.get("BALRLCCCY_" + countString))) {
//								dataMap.put("FXSUBBALRECNUM_" + countString, String.valueOf(
//										Integer.valueOf((String) dataMap.get("FXSUBBALRECNUM_" + countString)) + Integer
//												.valueOf((String) dataMapPut.get("FXSUBBALRECNUM_" + countStringPut))));
//								BigDecimal oldN = new BigDecimal((String) dataMap.get("FXSUBBAL_" + countString));
//								BigDecimal putN = new BigDecimal((String) dataMapPut.get("FXSUBBAL_" + countStringPut));
//								dataMap.put("FXSUBBAL_" + countString, oldN.add(putN).toString());
//								break;
//							} else if (((String) dataMap.get("BALRLCCCY_" + countString)) == null) {
//								dataMap.put("BALRLCCCY_" + countString,
//										(String) dataMapPut.get("BALRLCCCY_" + countStringPut));
//								dataMap.put("FXSUBBALRECNUM_" + countString,
//										(String) dataMapPut.get("FXSUBBALRECNUM_" + countStringPut));
//								dataMap.put("FXSUBBAL_" + countString,
//										(String) dataMapPut.get("FXSUBBAL_" + countStringPut));
//								break;
//							}
//						}
//					}
//					dataMap.put("USERDATA_X50", dataMapPut.get("USERDATA_X50"));
//					dataMap.put("TOPMSG", dataMapPut.get("TOPMSG"));
//					TOPMSG = (String) dataMapPut.get("QUERYNEXT");
//					log.debug("QUERYNEXT >> {} , is continu >> {}",TOPMSG,TOPMSG != null && !TOPMSG.equals(""));
//					// TODO:防止本地端無限回圈，上傳時請註解
//					// countStop++;
//					// if(countStop >= 2) {
//					// break;
//					// }
//				}

				String sATM = "";
				String sBAL = "";
				for (int i = 1; i <= 7; i++) {
					String countString = String.valueOf(i);

					if ("EXCEL".equals(reqParam.get("downloadType")) || "OLDEXCEL".equals(reqParam.get("downloadType"))) {
						if (dataMap.get("AMTRLCCCY_" + countString) != null) {
							sATM += dataMap.get("AMTRLCCCY_" + countString) + " "
									+ NumericUtil.fmtAmount((String) dataMap.get("FXSUBAMT_" + countString), 2) + " i18n{LB.W0123} "
									+ dataMap.get("FXSUBAMTRECNUM_" + countString) + " i18n{LB.Rows}" + "\n";
						}
						if (dataMap.get("BALRLCCCY_" + countString) != null) {
							sBAL += dataMap.get("BALRLCCCY_" + countString) + " "
									+ NumericUtil.fmtAmount((String) dataMap.get("FXSUBBAL_" + countString), 2) + " i18n{LB.W0123} "
									+ dataMap.get("FXSUBBALRECNUM_" + countString) + " i18n{LB.Rows}" + "\n";
						}
					}else {
						if (dataMap.get("AMTRLCCCY_" + countString) != null) {
							sATM += dataMap.get("AMTRLCCCY_" + countString) + " "
									+ NumericUtil.fmtAmount((String) dataMap.get("FXSUBAMT_" + countString), 2) + " i18n{LB.W0123} "
									+ dataMap.get("FXSUBAMTRECNUM_" + countString) + " i18n{LB.Rows}" + "\r\n ";
						}
						if (dataMap.get("BALRLCCCY_" + countString) != null) {
							sBAL += dataMap.get("BALRLCCCY_" + countString) + " "
									+ NumericUtil.fmtAmount((String) dataMap.get("FXSUBBAL_" + countString), 2) + " i18n{LB.W0123} "
									+ dataMap.get("FXSUBBALRECNUM_" + countString) + " i18n{LB.Rows}" + "\r\n ";
						}
						
					}
				}
				Map<String, Object> parameterMap = new HashMap<String, Object>();

				parameterMap.put("downloadFileName", reqParam.get("downloadFileName"));
				parameterMap.put("downloadType", reqParam.get("downloadType"));
				parameterMap.put("templatePath", reqParam.get("templatePath"));
				parameterMap.put("hasMultiRowData", reqParam.get("hasMultiRowData"));

				parameterMap.put("CMQTIME", dataMap.get("CMQTIME"));
				parameterMap.put("CMPERIOD", dataMap.get("CMPERIOD"));
				parameterMap.put("LCNO", dataMap.get("LCNO"));
				parameterMap.put("CMRECNUM", dataMap.get("CMRECNUM"));
				parameterMap.put("SDATM", sATM.substring(0, sATM.length() - 0));
				parameterMap.put("SDBAL", sBAL.substring(0, sBAL.length() - 0));

				String downloadType = reqParam.get("downloadType");
				log.trace(ESAPIUtil.vaildLog("downloadType={}"+downloadType));

				// EXCEL和OLDEXCEL下載
				if ("EXCEL".equals(downloadType) || "OLDEXCEL".equals(downloadType)) {
					parameterMap.put("headerRightEnd", reqParam.get("headerRightEnd"));
					parameterMap.put("headerBottomEnd", reqParam.get("headerBottomEnd"));
					parameterMap.put("multiRowDataListMapKey", reqParam.get("multiRowDataListMapKey"));
					parameterMap.put("rowStartIndex", reqParam.get("rowStartIndex"));
					parameterMap.put("rowRightEnd", reqParam.get("rowRightEnd"));
					parameterMap.put("footerStartIndex", reqParam.get("footerStartIndex"));
					parameterMap.put("footerEndIndex", reqParam.get("footerEndIndex"));
					parameterMap.put("footerRightEnd", reqParam.get("footerRightEnd"));
				}
				// TXT下載
				else {
					parameterMap.put("txtHeaderBottomEnd", reqParam.get("txtHeaderBottomEnd"));
					parameterMap.put("txtHasRowData", reqParam.get("txtHasRowData"));
					parameterMap.put("txtHasFooter", reqParam.get("txtHasFooter"));
					parameterMap.put("txtMultiRowStartIndex", reqParam.get("txtMultiRowStartIndex"));
					parameterMap.put("txtMultiRowEndIndex", reqParam.get("txtMultiRowEndIndex"));
					parameterMap.put("txtMultiRowCopyStartIndex", reqParam.get("txtMultiRowCopyStartIndex"));
					parameterMap.put("txtMultiRowCopyEndIndex", reqParam.get("txtMultiRowCopyEndIndex"));
					parameterMap.put("txtMultiRowDataListMapKey", reqParam.get("txtMultiRowDataListMapKey"));
				}
				bs.addData("parameterMap", parameterMap);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("letter_of_credit_opening_inquiry_date_ajaxDirectDownload error >> {}",e);
		}
		return bs;
	}


	/**
	 * 進口到單明細
	 * 
	 * @param cusidn
	 *            統一編號
	 * @param reqParam
	 * @return
	 */
	public BaseResult letter_of_credit_opening_inquiry_import_detail(String cusidn, Map<String, String> reqParam) {
		log.trace("letter_of_credit_opening_inquiry_import_detail");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// call ws API
			bs = N555_REST(cusidn, reqParam);
			if (bs != null && bs.getResult()) {
				Map<String, Object> callRow = (Map) bs.getData();
				ArrayList<Map<String, String>> callTable = (ArrayList<Map<String, String>>) callRow.get("REC");
				for (Map<String, String> data : callTable) {
					data.put("RBILLAMT", NumericUtil.fmtAmount(data.get("RBILLAMT"), 2));
					data.put("RADVFIXR", NumericUtil.fmtAmount(data.get("RADVFIXR"), 6));
				}
				bs.addData("LCNO", reqParam.get("LCNO"));
				bs.addData("RORIGAMT", reqParam.get("RORIGAMT"));
				bs.addData("RBENNAME", reqParam.get("RBENNAME"));
			}
		} catch (Exception e) {
			log.error("letter_of_credit_query_result", e);
		}
		return bs;
	}

	/**
	 * 商業信用狀明細
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult letter_of_credit_opening_inquiry_commerce_detail(String cusidn, Map<String, String> reqParam) {
		log.trace("letter_of_credit_opening_inquiry_commerce_detail");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// call ws API
			bs = N556_REST(cusidn, reqParam);
			if (bs != null && bs.getResult()) {
				Map<String, Object> callRow = (Map) bs.getData();
				bs.addData("RAMDNO", NumericUtil.fmtAmount((String) callRow.get("RAMDNO"), 0));
				bs.addData("RORIGAMT", NumericUtil.fmtAmount((String) callRow.get("RORIGAMT"), 2));
				bs.addData("RALLOW", NumericUtil.fmtAmount((String) callRow.get("RALLOW"), 0));
				bs.addData("RMARGOS", NumericUtil.fmtAmount((String) callRow.get("RMARGOS"), 2));
				bs.addData("RMAXDRAW", NumericUtil.fmtAmount((String) callRow.get("RMAXDRAW"), 2));
				bs.addData("RLCOS", NumericUtil.fmtAmount((String) callRow.get("RLCOS"), 2));
				bs.addData("RTXTENOR", NumericUtil.fmtAmount((String) callRow.get("RTXTENOR"), 0));
				bs.addData("RTENOR", NumericUtil.fmtAmount((String) callRow.get("RTENOR"), 0));
			}
		} catch (Exception e) {
			log.error("letter_of_credit_query_result", e);
		}
		return bs;
	}

	/**
	 * 擔保信用狀/保證函明細
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult letter_of_credit_opening_inquiry_guarantee_result(String cusidn, Map<String, String> reqParam) {
		log.trace("letter_of_credit_opening_inquiry_guarantee_result");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// call ws API
			bs = N557_REST(cusidn, reqParam);
			if (bs != null && bs.getResult()) {
				Map<String, Object> callRow = (Map) bs.getData();
				ArrayList<Map<String, String>> callTable = (ArrayList<Map<String, String>>) callRow.get("REC");
				for (Map<String, String> data : callTable) {

					data.put("RLGAMT", NumericUtil.fmtAmount(data.get("RLGAMT"), 2));
					data.put("RLGOS", NumericUtil.fmtAmount(data.get("RLGOS"), 2));

					// 清除空日期
					if (data.get("ROPENDAT").equals("0000/00/00")) {
						data.put("ROPENDAT", "");
					}
					if (data.get("REXPDATE").equals("0000/00/00")) {
						data.put("REXPDATE", "");
					}
				}
				String sATM = "";
				String sBAL = "";
				for (int i = 1; i <= 7; i++) {
					String countString = String.valueOf(i);
					if (callRow.get("AMTRLGCCY_" + countString) != null) {
						sATM += callRow.get("AMTRLGCCY_" + countString) + " "
								+ NumericUtil.fmtAmount((String) callRow.get("FXSUBAMT_" + countString), 2) + " i18n{LB.W0123} "
								+ callRow.get("FXSUBAMTRECNUM_" + countString) + " i18n{LB.Rows}" + "<br>";
					}
					if (callRow.get("BALRLGCCY_" + countString) != null) {
						sBAL += callRow.get("BALRLGCCY_" + countString) + " "
								+ NumericUtil.fmtAmount((String) callRow.get("FXSUBBAL_" + countString), 2) + " i18n{LB.W0123} "
								+ callRow.get("FXSUBBALRECNUM_" + countString) + " i18n{LB.Rows}" + "<br>";
					}
				}
				sATM = sATM.substring(0, sATM.length() - 4);
				sBAL = sBAL.substring(0, sBAL.length() - 4);
				bs.addData("SATM", sATM);
				log.debug("sATM >> {}", sATM);
				bs.addData("SBAL", sBAL);
				log.debug("sBal >> {}", sBAL);
				bs.addData("LCNO", reqParam.get("LCNO").equals("") ? i18n.getMsg("LB.All") : reqParam.get("LCNO"));
				bs.addData("CMSDATE", reqParam.get("CMSDATE"));
				bs.addData("CMEDATE", reqParam.get("CMEDATE"));
			}
		} catch (Exception e) {
			log.error("letter_of_credit_query_result", e);
		}
		return bs;
	}

	/**
	 * 擔保信用狀/保證函明細 直接下載
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult letter_of_credit_opening_inquiry_guarantee_ajaxDirectDownload(String cusidn,
			Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			reqParam.put("USERDATA_X50", "");
			reqParam.put("OKOVNEXT", "TRUE");
			bs = letter_of_credit_opening_inquiry_guarantee_result(cusidn, reqParam);
			if (bs != null && bs.getResult()) {
				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap={}", dataMap);
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
				log.debug("rowListMap={}", rowListMap);

				// OKOV、OKOK自動回滾
//				String TOPMSG = (String) dataMap.get("QUERYNEXT");
//				log.debug("QUERYNEXT >> {} , is continu >> {}",TOPMSG,TOPMSG != null && !TOPMSG.equals(""));
//				int countStop = 0;
//				while (TOPMSG != null && !TOPMSG.equals("")) {
//					BaseResult bsPut = null;
//					reqParam.put("USERDATA_X50", (String) dataMap.get("USERDATA_X50"));
//					bsPut = letter_of_credit_opening_inquiry_guarantee_result(cusidn, reqParam);
//					Map<String, Object> dataMapPut = (Map) bsPut.getData();
//					log.trace("dataMap={}", dataMapPut);
//					List<Map<String, String>> rowListMapPut = (List<Map<String, String>>) dataMapPut.get("REC");
//					log.debug("rowListMap={}", rowListMapPut);
//					rowListMap.addAll(rowListMapPut);
//					dataMap.put("CMRECNUM", String.valueOf(Integer.valueOf((String) dataMap.get("CMRECNUM"))
//							+ Integer.valueOf((String) dataMapPut.get("CMRECNUM"))));
//					// 將開狀金額小計作加總
//					for (int i = 1; i <= 7; i++) {
//						String countStringPut = String.valueOf(i);
//						if ((String) dataMapPut.get("AMTRLCCCY_" + countStringPut) == null) {
//							continue;
//						}
//						for (int j = 1; j <= 7; j++) {
//							String countString = String.valueOf(j);
//							if (((String) dataMapPut.get("AMTRLGCCY_" + countStringPut))
//									.equals((String) dataMap.get("AMTRLGCCY_" + countString))) {
//								dataMap.put("FXSUBAMTRECNUM_" + countString, String.valueOf(
//										Integer.valueOf((String) dataMap.get("FXSUBAMTRECNUM_" + countString)) + Integer
//												.valueOf((String) dataMapPut.get("FXSUBAMTRECNUM_" + countStringPut))));
//								BigDecimal oldN = new BigDecimal((String) dataMap.get("FXSUBAMT_" + countString));
//								BigDecimal putN = new BigDecimal((String) dataMapPut.get("FXSUBAMT_" + countStringPut));
//								dataMap.put("FXSUBAMT_" + countString, oldN.add(putN).toString());
//								break;
//							} else if (((String) dataMap.get("AMTRLGCCY_" + countString)) == null) {
//								dataMap.put("AMTRLGCCY_" + countString,
//										(String) dataMapPut.get("AMTRLGCCY_" + countStringPut));
//								dataMap.put("FXSUBAMTRECNUM_" + countString,
//										(String) dataMapPut.get("FXSUBAMTRECNUM_" + countStringPut));
//								dataMap.put("FXSUBAMT_" + countString,
//										(String) dataMapPut.get("FXSUBAMT_" + countStringPut));
//								break;
//							}
//						}
//					}
//					// 將信用狀餘額小計作加總
//					for (int i = 1; i <= 7; i++) {
//						String countStringPut = String.valueOf(i);
//						if ((String) dataMapPut.get("AMTRLCCCY_" + countStringPut) == null) {
//							continue;
//						}
//						for (int j = 1; j <= 7; j++) {
//							String countString = String.valueOf(j);
//							if (((String) dataMapPut.get("BALRLGCCY_" + countStringPut))
//									.equals((String) dataMap.get("BALRLGCCY_" + countString))) {
//								dataMap.put("FXSUBBALRECNUM_" + countString, String.valueOf(
//										Integer.valueOf((String) dataMap.get("FXSUBBALRECNUM_" + countString)) + Integer
//												.valueOf((String) dataMapPut.get("FXSUBBALRECNUM_" + countStringPut))));
//								BigDecimal oldN = new BigDecimal((String) dataMap.get("FXSUBBAL_" + countString));
//								BigDecimal putN = new BigDecimal((String) dataMapPut.get("FXSUBBAL_" + countStringPut));
//								dataMap.put("FXSUBBAL_" + countString, oldN.add(putN).toString());
//								break;
//							} else if (((String) dataMap.get("BALRLGCCY_" + countString)) == null) {
//								dataMap.put("BALRLGCCY_" + countString,
//										(String) dataMapPut.get("BALRLGCCY_" + countStringPut));
//								dataMap.put("FXSUBBALRECNUM_" + countString,
//										(String) dataMapPut.get("FXSUBBALRECNUM_" + countStringPut));
//								dataMap.put("FXSUBBAL_" + countString,
//										(String) dataMapPut.get("FXSUBBAL_" + countStringPut));
//								break;
//							}
//						}
//					}
//					dataMap.put("USERDATA_X50", dataMapPut.get("USERDATA_X50"));
//					dataMap.put("TOPMSG", dataMapPut.get("TOPMSG"));
//					TOPMSG = (String) dataMapPut.get("QUERYNEXT");
//					log.debug("QUERYNEXT >> {} , is continu >> {}",TOPMSG,TOPMSG != null && !TOPMSG.equals(""));
//					// //TODO:防止本地端無限回圈，上傳時請註解
//					// countStop++;
//					// if(countStop >= 2) {
//					// break;
//					// }
//				}

				String sATM = "";
				String sBAL = "";
				for (int i = 1; i <= 7; i++) {
					if ("EXCEL".equals(reqParam.get("downloadType")) || "OLDEXCEL".equals(reqParam.get("downloadType"))) {
						String countString = String.valueOf(i);
						if (dataMap.get("AMTRLGCCY_" + countString) != null) {
							sATM += dataMap.get("AMTRLGCCY_" + countString) + " "
									+ NumericUtil.fmtAmount((String) dataMap.get("FXSUBAMT_" + countString), 2) + " i18n{LB.W0123} "
									+ dataMap.get("FXSUBAMTRECNUM_" + countString) + " i18n{LB.Rows} "  + "\n";
						}
						if (dataMap.get("BALRLGCCY_" + countString) != null) {
							sBAL += dataMap.get("BALRLGCCY_" + countString) + " "
									+ NumericUtil.fmtAmount((String) dataMap.get("FXSUBBAL_" + countString), 2) + " i18n{LB.W0123} "
									+ dataMap.get("FXSUBBALRECNUM_" + countString) + " i18n{LB.Rows} "  + "\n";
						}
					}else {
						String countString = String.valueOf(i);
						if (dataMap.get("AMTRLGCCY_" + countString) != null) {
							sATM += dataMap.get("AMTRLGCCY_" + countString) + " "
									+ NumericUtil.fmtAmount((String) dataMap.get("FXSUBAMT_" + countString), 2) + " i18n{LB.W0123} "
									+ dataMap.get("FXSUBAMTRECNUM_" + countString) + " i18n{LB.Rows} "  + "\r\n ";
						}
						if (dataMap.get("BALRLGCCY_" + countString) != null) {
							sBAL += dataMap.get("BALRLGCCY_" + countString) + " "
									+ NumericUtil.fmtAmount((String) dataMap.get("FXSUBBAL_" + countString), 2) + " i18n{LB.W0123} "
									+ dataMap.get("FXSUBBALRECNUM_" + countString) + " i18n{LB.Rows} "  + "\r\n ";
						}
					}
				}
				Map<String, Object> parameterMap = new HashMap<String, Object>();

				parameterMap.put("downloadFileName", reqParam.get("downloadFileName"));
				parameterMap.put("downloadType", reqParam.get("downloadType"));
				parameterMap.put("templatePath", reqParam.get("templatePath"));
				parameterMap.put("hasMultiRowData", reqParam.get("hasMultiRowData"));

				parameterMap.put("CMQTIME", dataMap.get("CMQTIME"));
				parameterMap.put("CMPERIOD", dataMap.get("CMPERIOD"));
				parameterMap.put("LCNO", dataMap.get("LCNO"));
				parameterMap.put("CMRECNUM", dataMap.get("CMRECNUM"));
				parameterMap.put("SDATM", sATM.substring(0, sATM.length() - 0));
				parameterMap.put("SDBAL", sBAL.substring(0, sBAL.length() - 0));

				String downloadType = reqParam.get("downloadType");
				log.trace(ESAPIUtil.vaildLog("downloadType={}"+ downloadType));

				// EXCEL和OLDEXCEL下載
				if ("EXCEL".equals(downloadType) || "OLDEXCEL".equals(downloadType)) {
					parameterMap.put("headerRightEnd", reqParam.get("headerRightEnd"));
					parameterMap.put("headerBottomEnd", reqParam.get("headerBottomEnd"));
					parameterMap.put("multiRowDataListMapKey", reqParam.get("multiRowDataListMapKey"));
					parameterMap.put("rowStartIndex", reqParam.get("rowStartIndex"));
					parameterMap.put("rowRightEnd", reqParam.get("rowRightEnd"));
					parameterMap.put("footerStartIndex", reqParam.get("footerStartIndex"));
					parameterMap.put("footerEndIndex", reqParam.get("footerEndIndex"));
					parameterMap.put("footerRightEnd", reqParam.get("footerRightEnd"));
				}
				// TXT下載
				else {
					parameterMap.put("txtHeaderBottomEnd", reqParam.get("txtHeaderBottomEnd"));
					parameterMap.put("txtHasRowData", reqParam.get("txtHasRowData"));
					parameterMap.put("txtHasFooter", reqParam.get("txtHasFooter"));
					parameterMap.put("txtMultiRowStartIndex", reqParam.get("txtMultiRowStartIndex"));
					parameterMap.put("txtMultiRowEndIndex", reqParam.get("txtMultiRowEndIndex"));
					parameterMap.put("txtMultiRowCopyStartIndex", reqParam.get("txtMultiRowCopyStartIndex"));
					parameterMap.put("txtMultiRowCopyEndIndex", reqParam.get("txtMultiRowCopyEndIndex"));
					parameterMap.put("txtMultiRowDataListMapKey", reqParam.get("txtMultiRowDataListMapKey"));
				}
				bs.addData("parameterMap", parameterMap);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("letter_of_credit_opening_inquiry_guarantee_ajaxDirectDownload error >> {}",e);
		}
		return bs;
	}

	/**
	 * 匯入匯款查詢
	 * 
	 * @param cusidn
	 *            統一編號
	 * @param reqParam
	 * @return
	 */
	public BaseResult inward_remittance_result(String cusidn, Map<String, String> reqParam) {
		log.trace("inward_remittance_result_service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = N565_REST(cusidn, reqParam);
			if (bs != null && bs.getResult()) {
				Map<String, Object> callRow = (Map) bs.getData();
				ArrayList<Map<String, String>> callTable = (ArrayList<Map<String, String>>) callRow.get("REC");

				// 移除空入帳帳號
				for (int i = 0; i < callTable.size(); i++) {
					log.debug("CUSIDN >> {}", callTable.get(i).get("CUSIDN"));
					if (!callTable.get(i).get("CUSIDN").equals(cusidn)) {
						callTable.remove(i);
						i--;
					}
				}
				
				// 排序
				if (reqParam.get("FXSORT").equals("FXNOTIFYDATE")) {
					// 依通知日
					log.trace("sort>>fx notify date");
					for (int i = 1; i < callTable.size(); i++) {
						log.debug("sort>> i = {}", i);
						for (int j = i; j > 0; j--) {
							log.debug("sort>> a = {}, b = {}", j, j - 1);
							log.debug("sort>>{} & {}", callTable.get(j).get("RADATE"),
									callTable.get(j - 1).get("RADATE"));
							if (callTable.get(j).get("RADATE").compareTo(callTable.get(j - 1).get("RADATE")) < 0) {
								log.trace("swap>>{} & {}", callTable.get(j).get("RADATE"),
										callTable.get(j - 1).get("RADATE"));
								Map<String, String> temp = callTable.get(j);
								callTable.set(j, callTable.get(j - 1));
								callTable.set(j - 1, temp);
							} else {
								break;
							}
						}
					}

				} else {
					// 依解款日
					log.trace("sort>>fx pay date");
					for (int i = 1; i < callTable.size(); i++) {
						for (int j = i; j > 0; j--) {
							if (callTable.get(j).get("RRTNDATE").compareTo(callTable.get(j - 1).get("RRTNDATE")) < 0) {
								Map<String, String> temp = callTable.get(j);
								callTable.set(j, callTable.get(j - 1));
								callTable.set(j - 1, temp);
							} else {
								break;
							}
						}
					}
				}

				// 移除空入帳帳號
//				for (int i = 0; i < callTable.size(); i++) {
//					log.debug("RBANC >> {}", callTable.get(i).get("RBANC"));
//					if (callTable.get(i).get("RBANC").equals("")) {
//						callTable.remove(i);
//						i--;
//					}
//				}

				Map<String, Object> fxinamtSum = new HashMap<String, Object>();
				for (Map<String, String> data : callTable) {
					// 轉換狀態
//					switch (data.get("RSTATE")) {
//					case "A":
//						// data.put("RSTATE",i18n.getMsg( ));
//						data.put("RSTATE", "通知");
//						break;
//					case "S":
//						// data.put("RSTATE",i18n.getMsg( ));
//						data.put("RSTATE", "銷帳");
//						break;
//					case "C":
//						// data.put("RSTATE",i18n.getMsg( ));
//						data.put("RSTATE", "退匯");
//						break;
//					}
					Map<String, String> argBean = SpringBeanFactory.getBean("INWARD_REMITTANCE_RSTATE");
					if(argBean.containsKey(data.get("RSTATE")))
					{
						String RSTATE = argBean.get(data.get("RSTATE"));
						data.put("RSTATE",RSTATE);
					}
					
					data.put("RADATE", DateUtil.convertDate(2, data.get("RADATE"), "yyyy-mm-dd", "yyyy/mm/dd"));
					data.put("RRTNDATE", DateUtil.convertDate(2, data.get("RRTNDATE"), "yyyy-mm-dd", "yyyy/mm/dd"));
					data.put("RVALDATE", DateUtil.convertDate(2, data.get("RVALDATE"), "yyyy-mm-dd", "yyyy/mm/dd"));
					data.put("RCNADATE", DateUtil.convertDate(2, data.get("RCNADATE"), "yyyy-mm-dd", "yyyy/mm/dd"));

					// 清除空日期
					if (data.get("RADATE").equals("0000/00/00")) {
						data.put("RADATE", "");
					}
					if (data.get("RRTNDATE").equals("0000/00/00")) {
						data.put("RRTNDATE", "");
					}
					if (data.get("RVALDATE").equals("0000/00/00")) {
						data.put("RVALDATE", "");
					}

					// 計算總金額
					if (fxinamtSum.containsKey(data.get("RREMITCY"))) {
						float[] countFxinamtSum = (float[]) fxinamtSum.get(data.get("RREMITCY"));
						countFxinamtSum[0] += Float.parseFloat(data.get("RREMITAM"));
						countFxinamtSum[1] += 1;
						fxinamtSum.put(data.get("RREMITCY"), countFxinamtSum);
					} else {
						float[] countFxinamtSum = new float[2];
						countFxinamtSum[0] = Float.parseFloat(data.get("RREMITAM"));
						countFxinamtSum[1] = 1;
						fxinamtSum.put(data.get("RREMITCY"), countFxinamtSum);
					}

					data.put("RREMITAM", NumericUtil.fmtAmount(data.get("RREMITAM"), 2));
					data.put("FEXRATE", NumericUtil.fmtAmount(data.get("FEXRATE"), 7));
					data.put("RORDBAD1_RORDBANK", data.get("RORDBAD1") + "/" + data.get("RORDBANK"));
				}
				for (Map.Entry<String, Object> entry : fxinamtSum.entrySet()) {
					float[] countFxinamtSum = (float[]) entry.getValue();
					String[] countFxinamtSumString = new String[2];
					countFxinamtSumString[0] = NumericUtil.fmtAmount(String.valueOf(countFxinamtSum[0]), 2);
					countFxinamtSumString[1] = String.valueOf((int) countFxinamtSum[1]);
					fxinamtSum.put(entry.getKey(), countFxinamtSumString);
				}
				String fxinamtSumStringMaxForPrint = "";
				for (Map.Entry<String, Object> entry : fxinamtSum.entrySet()) {
					String[] countFxinamtSum = (String[]) entry.getValue();
					fxinamtSumStringMaxForPrint += entry.getKey() + " " + countFxinamtSum[0] + " " + countFxinamtSum[1]
							+ i18n.getMsg("LB.Rows") + " <br /> ";
				}

				bs.addData("FXINAMTSUM", fxinamtSum);
				log.debug("FXINAMTSUMSTRINGFORPRINT >> {}", fxinamtSumStringMaxForPrint);
				if(fxinamtSumStringMaxForPrint.length() >= 8) {
					bs.addData("FXINAMTSUMSTRINGFORPRINT",
							fxinamtSumStringMaxForPrint.substring(0, fxinamtSumStringMaxForPrint.length() - 8));
				}else{
					bs.addData("FXINAMTSUMSTRINGFORPRINT",fxinamtSumStringMaxForPrint);
				}
				
				String fxinamtSumStringMaxForPrintMS = "";
				for(int i = 1;i <= 8;i++) {
					String AMTRREMITCY = (String)callRow.get("AMTRREMITCY_" + String.valueOf(i));
					String FXSUBAMTRECNUM = (String)callRow.get("FXSUBAMTRECNUM_" + String.valueOf(i));
					String FXSUBAMT = (String)callRow.get("FXSUBAMT_" + String.valueOf(i));
					AMTRREMITCY = AMTRREMITCY == null ? "" : AMTRREMITCY;
					if(AMTRREMITCY != null && !AMTRREMITCY.equals("")) {
						FXSUBAMT = NumericUtil.fmtAmount(String.valueOf(FXSUBAMT), 2);
						bs.addData("FXSUBAMT_" + String.valueOf(i), FXSUBAMT);
						fxinamtSumStringMaxForPrintMS += AMTRREMITCY + " " + FXSUBAMT + " i18n{LB.W0123} " + FXSUBAMTRECNUM
								+ " i18n{LB.Rows} " + " <br /> ";
					}
				}
				if(fxinamtSumStringMaxForPrintMS.length() >= 8) {
					bs.addData("FXINAMTSUMSTRINGFORPRINTMS",
							fxinamtSumStringMaxForPrintMS.substring(0, fxinamtSumStringMaxForPrintMS.length() - 8));
				}else{
					bs.addData("FXINAMTSUMSTRINGFORPRINTMS",fxinamtSumStringMaxForPrintMS);
				}
				
				log.debug("FXINAMTSUMSTRINGFORPRINT >> {}", fxinamtSumStringMaxForPrint);
				bs.addData("ACNDownload", reqParam.get("ACN"));
				bs.addData("ACN", reqParam.get("ACN"));
				callRow.put("CMRECNUM", String.valueOf(callTable.size()));
				bs.addData("CMSDATE", reqParam.get("CMSDATE"));
				bs.addData("CMEDATE", reqParam.get("CMEDATE"));
				bs.addData("FXSORT", reqParam.get("FXSORT"));
			}
		} catch (Exception e) {
			// e.printStackTrace();
			log.error("time_deposit", e);
		}
		return bs;
	}

	/**
	 * 匯入匯款查詢直接下載
	 * 
	 * @param cusidn
	 *            統一編號
	 * @param reqParam
	 * @return
	 */
	public BaseResult inward_remittance_ajaxDirectDownload(String cusidn, Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			reqParam.put("USERDATA", "");
			reqParam.put("ACN", reqParam.get("ACNDownload"));
			reqParam.put("OKOVNEXT", "TRUE");
			bs = inward_remittance_result(cusidn, reqParam);
			if (bs != null && bs.getResult()) {
				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap={}", dataMap);
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
				log.debug("rowListMap={}", rowListMap);

				// OKOV、OKOK自動回滾
//				String TOPMSG = (String) dataMap.get("QUERYNEXT");
//				log.debug("QUERYNEXT >> {} , is continu >> {}",TOPMSG,TOPMSG != null && !TOPMSG.equals(""));
//				int countStop = 0;
//				while (TOPMSG != null && !TOPMSG.equals("")) {
//					BaseResult bsPut = null;
//					reqParam.put("USERDATA", (String) dataMap.get("USERDATA"));
//					bsPut = inward_remittance_result(cusidn, reqParam);
//					Map<String, Object> dataMapPut = (Map) bsPut.getData();
//					log.trace("dataMap={}", dataMapPut);
//					List<Map<String, String>> rowListMapPut = (List<Map<String, String>>) dataMapPut.get("REC");
//					log.debug("rowListMap={}", rowListMapPut);
//					rowListMap.addAll(rowListMapPut);
//					dataMap.put("CMRECNUM", String.valueOf(Integer.valueOf((String) dataMap.get("CMRECNUM"))
//							+ Integer.valueOf((String) dataMapPut.get("CMRECNUM"))));
//					dataMap.put("USERDATA", dataMapPut.get("USERDATA"));
//					dataMap.put("TOPMSG", dataMapPut.get("TOPMSG"));
//					TOPMSG = (String) dataMapPut.get("QUERYNEXT");
//					log.debug("QUERYNEXT >> {} , is continu >> {}",TOPMSG,TOPMSG != null && !TOPMSG.equals(""));
//					// //TODO:防止本地端無限回圈，上傳時請註解
//					// countStop++;
//					// if(countStop >= 2) {
//					// break;
//					// }
//				}
//
//				// 計算總金額
//				Map<String, Object> fxinamtSum = new HashMap<String, Object>();
//				for (Map<String, String> data : rowListMap) {
//					log.debug(data.get("RREMITCY"));
//					if (fxinamtSum.containsKey(data.get("RREMITCY"))) {
//						float[] countFxinamtSum = (float[]) fxinamtSum.get(data.get("RREMITCY"));
//						countFxinamtSum[0] += new BigDecimal(data.get("RREMITAM").replaceAll(",", "")).floatValue();
//						countFxinamtSum[1] += 1;
//						fxinamtSum.put(data.get("RREMITCY"), countFxinamtSum);
//					} else {
//						float[] countFxinamtSum = new float[2];
//						countFxinamtSum[0] = new BigDecimal(data.get("RREMITAM").replaceAll(",", "")).floatValue();
//						countFxinamtSum[1] = 1;
//						fxinamtSum.put(data.get("RREMITCY"), countFxinamtSum);
//					}
//				}
//				for (Map.Entry<String, Object> entry : fxinamtSum.entrySet()) {
//					float[] countFxinamtSum = (float[]) entry.getValue();
//					String[] countFxinamtSumString = new String[2];
//					countFxinamtSumString[0] = NumericUtil.fmtAmount(String.valueOf(countFxinamtSum[0]), 2);
//					countFxinamtSumString[1] = String.valueOf((int) countFxinamtSum[1]);
//					fxinamtSum.put(entry.getKey(), countFxinamtSumString);
//				}
//				String fxinamtSumStringMax = "";
//				for (Map.Entry<String, Object> entry : fxinamtSum.entrySet()) {
//					if ("EXCEL".equals(reqParam.get("downloadType")) || "OLDEXCEL".equals(reqParam.get("downloadType"))) {
//						String[] countFxinamtSum = (String[]) entry.getValue();
//						fxinamtSumStringMax += entry.getKey() + " " + countFxinamtSum[0] + " " + countFxinamtSum[1]
//								+ i18n.getMsg("LB.Rows") + "\n";
//					}else {
//						String[] countFxinamtSum = (String[]) entry.getValue();
//						fxinamtSumStringMax += entry.getKey() + " " + countFxinamtSum[0] + " " + countFxinamtSum[1]
//								+ i18n.getMsg("LB.Rows") + "\r\n";
//					}
//				}
				

				String fxinamtSumStringMax = "";
				for(int i = 1;i <= 8;i++) {
					String AMTRREMITCY = (String)dataMap.get("AMTRREMITCY_" + String.valueOf(i));
					String FXSUBAMTRECNUM = (String)dataMap.get("FXSUBAMTRECNUM_" + String.valueOf(i));
					String FXSUBAMT = (String)dataMap.get("FXSUBAMT_" + String.valueOf(i));
					AMTRREMITCY = AMTRREMITCY == null ? "" : AMTRREMITCY;
					if(AMTRREMITCY != null && !AMTRREMITCY.equals("")) {
						FXSUBAMT = NumericUtil.fmtAmount(String.valueOf(FXSUBAMT), 2);
						bs.addData("FXSUBAMT_" + String.valueOf(i), FXSUBAMT);
						if ("EXCEL".equals(reqParam.get("downloadType")) || "OLDEXCEL".equals(reqParam.get("downloadType"))) {
							fxinamtSumStringMax += AMTRREMITCY + " " + FXSUBAMT + " i18n{LB.W0123} " + FXSUBAMTRECNUM
									+ " i18n{LB.Rows} " + "\n";
						}else {
							fxinamtSumStringMax += AMTRREMITCY + " " + FXSUBAMT + " i18n{LB.W0123} " + FXSUBAMTRECNUM
									+ " i18n{LB.Rows} " + "\r\n";
						}
					}
				}
				if(fxinamtSumStringMax.length() >= 8) {
					bs.addData("FXINAMTSUMSTRINGFORPRINTMS",
							fxinamtSumStringMax.substring(0, fxinamtSumStringMax.length() - 8));
				}else{
					bs.addData("FXINAMTSUMSTRINGFORPRINTMS",fxinamtSumStringMax);
				}
				for(Map<String, String> row :rowListMap) {
					row.put("RSTATE", i18n.getMsg(row.get("RSTATE")));
				}
				Map<String, Object> parameterMap = new HashMap<String, Object>();

				parameterMap.put("CMQTIME", dataMap.get("CMQTIME"));
				parameterMap.put("CMPERIOD", dataMap.get("CMPERIOD"));
				parameterMap.put("ACN", dataMap.get("ACN").equals("ALL")? i18n.getMsg("LB.All"):reqParam.get("ACN"));
				parameterMap.put("CMRECNUM", dataMap.get("CMRECNUM"));
				parameterMap.put("FXINAMTSUMSTRING",
						fxinamtSumStringMax.substring(0, fxinamtSumStringMax.length() - 0));

				parameterMap.put("downloadFileName", reqParam.get("downloadFileName"));
				parameterMap.put("downloadType", reqParam.get("downloadType"));
				parameterMap.put("templatePath", reqParam.get("templatePath"));
				parameterMap.put("hasMultiRowData", reqParam.get("hasMultiRowData"));

				String downloadType = reqParam.get("downloadType");
				log.trace(ESAPIUtil.vaildLog("downloadType={}"+ downloadType));

				// EXCEL和OLDEXCEL下載
				if ("EXCEL".equals(downloadType) || "OLDEXCEL".equals(downloadType)) {
					parameterMap.put("headerRightEnd", reqParam.get("headerRightEnd"));
					parameterMap.put("headerBottomEnd", reqParam.get("headerBottomEnd"));
					parameterMap.put("multiRowStartIndex", reqParam.get("multiRowStartIndex"));
					parameterMap.put("multiRowEndIndex", reqParam.get("multiRowEndIndex"));
					parameterMap.put("multiRowCopyStartIndex", reqParam.get("multiRowCopyStartIndex"));
					parameterMap.put("multiRowCopyEndIndex", reqParam.get("multiRowCopyEndIndex"));
					parameterMap.put("multiRowDataListMapKey", reqParam.get("multiRowDataListMapKey"));
					parameterMap.put("rowStartIndex", reqParam.get("rowStartIndex"));
					parameterMap.put("rowRightEnd", reqParam.get("rowRightEnd"));
					parameterMap.put("footerStartIndex", reqParam.get("footerStartIndex"));
					parameterMap.put("footerEndIndex", reqParam.get("footerEndIndex"));
					parameterMap.put("footerRightEnd", reqParam.get("footerRightEnd"));
				}
				// TXT下載
				else {
					parameterMap.put("txtHeaderBottomEnd", reqParam.get("txtHeaderBottomEnd"));
					parameterMap.put("txtHasRowData", reqParam.get("txtHasRowData"));
					parameterMap.put("txtHasFooter", reqParam.get("txtHasFooter"));
					parameterMap.put("txtMultiRowStartIndex", reqParam.get("txtMultiRowStartIndex"));
					parameterMap.put("txtMultiRowEndIndex", reqParam.get("txtMultiRowEndIndex"));
					parameterMap.put("txtMultiRowCopyStartIndex", reqParam.get("txtMultiRowCopyStartIndex"));
					parameterMap.put("txtMultiRowCopyEndIndex", reqParam.get("txtMultiRowCopyEndIndex"));
					parameterMap.put("txtMultiRowDataListMapKey", reqParam.get("txtMultiRowDataListMapKey"));
				}
				bs.addData("parameterMap", parameterMap);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("inward_remittance_ajaxDirectDownload error >> {}",e);
		}
		return bs;
	}

	/**
	 * 匯出匯款查詢
	 * 
	 * @param sessionId
	 * @return 新增的變數名: RECMON-收款人資訊(RORDCUS1收款人名稱/RBENAC收款人帳號/RAWB1AD1收款人銀行名稱)
	 *         MONEY-顯示匯款金額(RREMITCY幣別+RREMITAM金額) CURRANCY_PRINT-下載用的匯款金額小計字串
	 *         CURRANCY_AMOUN-列印即顯示用的匯款金額小計物件
	 */
	public BaseResult outward_remittances_result(String cusidn, Map<String, String> reqParam) {
		log.trace("outward_remittances_result");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			reqParam = DateUtil.periodProcessing(reqParam);
			// call N566電文
			bs = N566_REST(cusidn, reqParam);
			// 顯示資料處理區塊
			Map<String, Object> dataMap = (Map) bs.getData();

			List<Map<String, Object>> currency = null;// 計算匯款金額小計
			List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");// 滾詳細資料
			// 筆數處理
			bs.addData("COUNTS",String.valueOf(rowListMap.size()));
			int totalcurrancy_c = 0;// 計算幣別的筆數
			int checkcurrancy_c = 0;// 計算幣別迴圈是否需要新增幣別
			if(!dataMap.get("TOPMSG").equals("ENRD")) {
			for (Map<String, String> map : rowListMap) {
				// 匯款日資料處理
				map.put("RREGDATE", map.get("RREGDATE").replace("-", "/"));
				// 付款日資料處理
				map.put("RVALDATE", map.get("RVALDATE").replace("-", "/"));
				// 匯款金額顯示處理
				String temp_money = map.get("RREMITCY") +" "+ NumericUtil.fmtAmount((String) map.get("RREMITAM"), 2);
				map.put("MONEY", temp_money);
				// 收款人資訊(下載用)
				String temp_receive = map.get("RORDCUS1") + "," + map.get("RBENAC") + "," + map.get("RAWB1AD1");
				map.put("RECMON", temp_receive);
				// 匯率資料處理
				map.put("RRATE", getRrateformat(map.get("RRATE")));
				// 匯款金額小計
				if (currency != null) {
					for (Map<String, Object> cymap : currency) {
						if (cymap.get("RREMITCY").equals(map.get("RREMITCY"))) {
							BigDecimal temp = (BigDecimal) cymap.get("RREMITAM");
							BigDecimal temp2 = new BigDecimal(map.get("RREMITAM"));
							temp = temp.add(temp2);
							cymap.put("RREMITAM", temp);
							cymap.put("temp_mount", (int) cymap.get("temp_mount") + 1);

							break;
						}
						checkcurrancy_c += 1;
					}
					if (checkcurrancy_c == totalcurrancy_c) {
						// 新增幣別
						totalcurrancy_c += 1;
						iniNewCurrency(currency, map);
					}
					checkcurrancy_c = 0;// 重置
				} else {
					currency = new ArrayList<Map<String, Object>>();
					iniNewCurrency(currency, map);
					totalcurrancy_c = 1;
				}
			}
			// 製作匯款金額下載用格式
			String currency_print_excel = "";
			String currency_print_txt = "";
			for (Map<String, Object> cymap : currency) {
				//LB.W0158->共計
				currency_print_excel = currency_print_excel + cymap.get("RREMITCY") + " " + cymap.get("RREMITAM") + " i18n{LB.W0158}"
						+"   "+ cymap.get("temp_mount") + "   i18n{LB.Rows}" + "\n";
			}
			for (Map<String, Object> cymap : currency) {
				currency_print_txt = currency_print_txt + cymap.get("RREMITCY") + " " + cymap.get("RREMITAM") + " i18n{LB.W0158}"
						+"   "+ cymap.get("temp_mount") + "   i18n{LB.Rows}" + "\r\n";
			}
			bs.addData("CURRANCY_PRINT_EXCEL", currency_print_excel);// 下載用
			bs.addData("CURRANCY_PRINT_TXT", currency_print_txt);// 下載用
			bs.addData("CURRANCY_AMOUNT", currency);// 列印用
			bs.setResult(true);
			}
			else {
				bs.setResult(false);
				bs.setMessage((String)dataMap.get("msgCode"),bs.getMessage());
			}
		} catch (Exception e) {
			log.error("outward_remittances_result", e);
		}
		return bs;
	}

	public String getRrateformat(String rrate) {
		String result = rrate;
		try {
			BigDecimal b = new BigDecimal(new BigInteger(rrate), 7);
			result = NumericUtil.fmtAmount(b.toPlainString(), 7);

		} catch (Exception e) {
			log.error("getRrateformat error");
		}
		return result;
	}

	public void iniNewCurrency(List<Map<String, Object>> currency, Map<String, String> map) {
		Map<String, Object> temp_map = new HashMap<String, Object>();
		temp_map.put("RREMITCY", map.get("RREMITCY"));
		temp_map.put("RREMITAM", new BigDecimal(map.get("RREMITAM")));
		temp_map.put("temp_mount", 1);
		currency.add(temp_map);
	}

	/**
	 * 匯出匯款查詢直接下載
	 * 
	 * @param cusidn
	 *            統一編號
	 * @param reqParam
	 * @return
	 */
	public BaseResult outward_remittances_ajaxDirectDownload(String cusidn, Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			bs = outward_remittances_result(cusidn, reqParam);
			if (bs != null && bs.getResult()) {
				Map<String, Object> dataMap = (Map<String, Object>) bs.getData();
				log.trace("dataMap={}", dataMap);
				// //TODO:防止本地端無限回圈，上傳時請註解
				// countStop++;
				// if(countStop >= 2) {
				// break;
				// }
				Map<String, Object> parameterMap = new HashMap<String, Object>();
				parameterMap.put("CMQTIME", dataMap.get("CMQTIME"));
				parameterMap.put("CMPERIOD", dataMap.get("CMPERIOD"));
				parameterMap.put("COUNT", (dataMap.get("COUNTS")).toString());
				parameterMap.put("CURRANCY_PRINT_EXCEL", dataMap.get("CURRANCY_PRINT_EXCEL"));
				parameterMap.put("CURRANCY_PRINT_TXT", dataMap.get("CURRANCY_PRINT_TXT"));
				parameterMap.put("downloadFileName", reqParam.get("downloadFileName"));
				parameterMap.put("downloadType", reqParam.get("downloadType"));
				parameterMap.put("templatePath", reqParam.get("templatePath"));
				parameterMap.put("hasMultiRowData", reqParam.get("hasMultiRowData"));
				String downloadType = reqParam.get("downloadType");
				log.trace(ESAPIUtil.vaildLog("downloadType={}"+downloadType));
				// EXCEL和OLDEXCEL下載
				if ("EXCEL".equals(downloadType) || "OLDEXCEL".equals(downloadType)) {
					parameterMap.put("headerRightEnd", reqParam.get("headerRightEnd"));
					parameterMap.put("headerBottomEnd", reqParam.get("headerBottomEnd"));
					parameterMap.put("rowStartIndex", reqParam.get("rowStartIndex"));
					parameterMap.put("rowRightEnd", reqParam.get("rowRightEnd"));
					parameterMap.put("footerStartIndex", reqParam.get("footerStartIndex"));
					parameterMap.put("footerEndIndex", reqParam.get("footerEndIndex"));
					parameterMap.put("footerRightEnd", reqParam.get("footerRightEnd"));
				}
				// TXT下載
				else {
					parameterMap.put("txtHeaderBottomEnd", reqParam.get("txtHeaderBottomEnd"));
					parameterMap.put("txtHasRowData", reqParam.get("txtHasRowData"));
					parameterMap.put("txtHasFooter", reqParam.get("txtHasFooter"));
				}
				bs.addData("parameterMap", parameterMap);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("outward_remittances_ajaxDirectDownload error >> {}",e);
		}
		return bs;
	}

	// 光票託收查詢_結果頁
	// N567
	public BaseResult clean_collection_result(String cusidn, Map<String, String> reqParam) {
		log.trace("clean_collection_result_service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			reqParam = DateUtil.periodProcessing(reqParam);
			String cmsdate = reqParam.get("CMSDATE");
			String cmedate = reqParam.get("CMEDATE");
			String CMPERIOD = cmsdate + "~" + cmedate;
			String uid = cusidn;

			log.trace(ESAPIUtil.vaildLog(cmsdate));
			log.trace(ESAPIUtil.vaildLog(cmedate));
			// call N567電文
			bs = N567_REST(cusidn, reqParam);
			// 前頁傳值(查詢期間)
			bs.addData("CMPERIOD", CMPERIOD);

			if (bs != null && bs.getResult()) {
				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap={}", dataMap);
				List<Map<String, String>> ListMap = (List<Map<String, String>>) dataMap.get("REC");
				log.debug("ListMap={}", ListMap);
				//比對ID//計算筆數
				int count = 0;
				for (int i = 0;i < ListMap.size();i++) {
					if(!ListMap.get(i).get("CUSIDN").equals(uid)) {
						ListMap.remove(i);
						i--;
					}
					else {
						count ++;
					}
				}
				// 計算總金額
				Map<String, Object> CURRANCY_Sum = new LinkedHashMap<String, Object>();
				for (Map<String, String> data : ListMap) {
					log.debug(data.get("RBILLCCY"));
					if (CURRANCY_Sum.containsKey(data.get("RBILLCCY"))) {
						float[] countCURRANCY_Sum = (float[]) CURRANCY_Sum.get(data.get("RBILLCCY"));
						countCURRANCY_Sum[0] += new BigDecimal(data.get("RBILLAMT").replaceAll(",", "")).floatValue();
						countCURRANCY_Sum[1] += 1;
						CURRANCY_Sum.put(data.get("RBILLCCY"), countCURRANCY_Sum);
					} else {
						float[] countCURRANCY_Sum = new float[2];
						countCURRANCY_Sum[0] = new BigDecimal(data.get("RBILLAMT").replaceAll(",", "")).floatValue();
						countCURRANCY_Sum[1] = 1;
						CURRANCY_Sum.put(data.get("RBILLCCY"), countCURRANCY_Sum);
					}
					//調整日期格式
					if(!data.get("RCREDATE").equals("")) {
						data.put("RCREDATE", data.get("RCREDATE").replaceAll("-", "/"));
					}
					if(!data.get("RVALDATE").equals("")) {
						data.put("RVALDATE", data.get("RVALDATE").replaceAll("-", "/"));
					}
					data.put("RBILLAMT", NumericUtil.fmtAmount(data.get("RBILLAMT"), 2));
					data.put("RRETAMT", NumericUtil.fmtAmount(data.get("RRETAMT"), 2));
					//調整狀態顯示//i18n
					if(data.get("RSTATE").equals("A")) {
						//託收
						data.put("RSTATE", i18n.getMsg("LB.X0148"));
					}
					else if(data.get("RSTATE").equals("S")) {
						//入帳
						data.put("RSTATE", i18n.getMsg("LB.X0149"));
					}
					else if(data.get("RSTATE").equals("C")) {
						//退匯
						data.put("RSTATE", i18n.getMsg("LB.X0150"));
					}
				}
				for (Map.Entry<String, Object> entry : CURRANCY_Sum.entrySet()) {
					float[] countCURRANCY_Sum = (float[]) entry.getValue();
					String[] countCURRANCY_SumString = new String[2];
					countCURRANCY_SumString[0] = NumericUtil.fmtAmount(String.valueOf(countCURRANCY_Sum[0]), 2);
					countCURRANCY_SumString[1] = String.valueOf((int) countCURRANCY_Sum[1]);
					CURRANCY_Sum.put(entry.getKey(), countCURRANCY_SumString);
				}
				String countCURRANCY_SumStringMax = "";
				String countCURRANCY_Sumjsp = "";
				String countCURRANCY_Sumxlsx = "";
				String CURRANCY = "";
				for (Map.Entry<String, Object> entry : CURRANCY_Sum.entrySet()) {
					String[] countCURRANCY_Sum = (String[]) entry.getValue();
					CURRANCY += entry.getKey() + " " + countCURRANCY_Sum[0] + " ";
					countCURRANCY_SumStringMax += entry.getKey() + " " + countCURRANCY_Sum[0] + " i18n{LB.W0158} "
							+ countCURRANCY_Sum[1] + " i18n{LB.Rows}" + "\r\n";
					countCURRANCY_Sumjsp += entry.getKey() + " " + countCURRANCY_Sum[0] + " i18n{LB.W0158} "
							+ countCURRANCY_Sum[1] + " i18n{LB.Rows}" + "<br>";
					countCURRANCY_Sumxlsx += entry.getKey() + " " + countCURRANCY_Sum[0] + " i18n{LB.W0158} "
							+ countCURRANCY_Sum[1] + " i18n{LB.Rows}" + "\n";
				}
				dataMap.put("COUNT", count);
				bs.addData("CMSDATE",cmsdate);
				bs.addData("CMEDATE",cmedate);
				bs.addData("CURRANCY_PRINT", countCURRANCY_Sumjsp);// 列印用
				bs.addData("CURRANCY_txt", countCURRANCY_SumStringMax);// 下載用
				bs.addData("CURRANCY_xlsx", countCURRANCY_Sumxlsx);// 下載用
				bs.addData("CURRANCY_C", CURRANCY);// 託收金額小計用
			}
		} catch (Exception e) {
			log.error("clean_collection_result", e);
		}
		return bs;
	}

	/**
	 * 光票託收查詢直接下載
	 * 
	 * @param cusidn
	 *            統一編號
	 * @param reqParam
	 * @return
	 */
	public BaseResult f_clean_collection_directDownload(String cusidn, Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			bs = clean_collection_result(cusidn, reqParam);
			if (bs != null && bs.getResult()) {
				Map<String, Object> dataMap = (Map<String, Object>) bs.getData();
				log.trace("dataMap={}", dataMap);
				// //TODO:防止本地端無限回圈，上傳時請註解
				// countStop++;
				// if(countStop >= 2) {
				// break;
				// }
				Map<String, Object> parameterMap = new HashMap<String, Object>();
				parameterMap.put("CMQTIME", dataMap.get("CMQTIME"));
				parameterMap.put("CMPERIOD", dataMap.get("CMPERIOD"));
				parameterMap.put("COUNT", (dataMap.get("COUNT")).toString());
				parameterMap.put("CURRANCY_txt", dataMap.get("CURRANCY_txt"));
				parameterMap.put("CURRANCY_xlsx", dataMap.get("CURRANCY_xlsx"));
				parameterMap.put("downloadFileName", reqParam.get("downloadFileName"));
				parameterMap.put("downloadType", reqParam.get("downloadType"));
				parameterMap.put("templatePath", reqParam.get("templatePath"));
				parameterMap.put("hasMultiRowData", reqParam.get("hasMultiRowData"));
				String downloadType = reqParam.get("downloadType");
				log.trace(ESAPIUtil.vaildLog("downloadType={}" + downloadType));
				log.trace("CURRANCY_xlsx={}", dataMap.get("CURRANCY_xlsx"));
				// EXCEL和OLDEXCEL下載
				if ("EXCEL".equals(downloadType) || "OLDEXCEL".equals(downloadType)) {
					parameterMap.put("headerRightEnd", reqParam.get("headerRightEnd"));
					parameterMap.put("headerBottomEnd", reqParam.get("headerBottomEnd"));
					parameterMap.put("rowStartIndex", reqParam.get("rowStartIndex"));
					parameterMap.put("rowRightEnd", reqParam.get("rowRightEnd"));
					parameterMap.put("footerStartIndex", reqParam.get("footerStartIndex"));
					parameterMap.put("footerEndIndex", reqParam.get("footerEndIndex"));
					parameterMap.put("footerRightEnd", reqParam.get("footerRightEnd"));
				}
				// TXT下載
				else {
					parameterMap.put("txtHeaderBottomEnd", reqParam.get("txtHeaderBottomEnd"));
					parameterMap.put("txtHasRowData", reqParam.get("txtHasRowData"));
					parameterMap.put("txtHasFooter", reqParam.get("txtHasFooter"));
				}
				bs.addData("parameterMap", parameterMap);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("f_clean_collection_directDownload error >> {}",e);
		}
		return bs;
	}

	/**
	 * 出口押匯查詢結果頁
	 * 
	 * @param sessionId
	 * @return
	 */
	public BaseResult export_bill_query_result(String cusidn, Map<String, String> reqParam) {
		log.trace("export_bill_query_result");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// call ws API
			bs = N564_REST(cusidn, reqParam);
			List<Map<String, String>> recList = ((List<Map<String, String>>) ((Map<String, Object>) bs.getData()).get("REC"));
			for(Map row : recList) {
				if("0".equals(row.get("RUPSTAT"))) {
					row.put("RUPSTAT", "LB.W0197");//未拒付
					row.put("RUPSTAT_DOWNLOAD", i18n.getMsg("LB.W0197"));
				}
				if("1".equals(row.get("RUPSTAT"))) {
					row.put("RUPSTAT", "LB.W1739");//拒付
					row.put("RUPSTAT_DOWNLOAD", i18n.getMsg("LB.W1739"));
				}
				if("1".equals(row.get("RBILLTYP"))) {
					row.put("RBILLTYP", "SIGHT");
				}
				if("2".equals(row.get("RBILLTYP"))) {
					row.put("RBILLTYP", "USANCE");
				}
				if(StrUtil.isNotEmpty((String) row.get("RCMDATE1"))) {
					row.put("RCMDATE1",  ((String)row.get("RCMDATE1")).replaceAll("-", "/"));
				}
				if(StrUtil.isNotEmpty((String) row.get("RVALDATE"))) {
					row.put("RVALDATE",  ((String)row.get("RVALDATE")).replaceAll("-", "/"));
				}
				if(StrUtil.isNotEmpty((String) row.get("RNEGDATE"))) {
					row.put("RNEGDATE",  ((String)row.get("RNEGDATE")).replaceAll("-", "/"));
				}
			}
			
			if ("".equals(reqParam.get("LCNO")) || null == reqParam.get("LCNO")) {
				bs.addData("LCNO_SHOW", "LB.All");
				bs.addData("LCNO_DOWNLOAD", i18n.getMsg("LB.All"));
				bs.addData("LCNO", reqParam.get("LCNO"));
			} else {
				bs.addData("LCNO_SHOW", reqParam.get("LCNO"));
				bs.addData("LCNO", reqParam.get("LCNO"));
				bs.addData("LCNO_DOWNLOAD", reqParam.get("LCNO"));
			}
			
			if ("".equals(reqParam.get("REFNO")) || null == reqParam.get("REFNO")) {
				bs.addData("REFNO_SHOW", "LB.All");
				bs.addData("REFNO_DOWNLOAD", i18n.getMsg("LB.All"));
				bs.addData("REFNO", reqParam.get("REFNO"));
			} else {
				bs.addData("REFNO_SHOW", reqParam.get("REFNO"));
				bs.addData("REFNO", reqParam.get("REFNO"));
				bs.addData("REFNO_DOWNLOAD", reqParam.get("REFNO"));
			}
		} catch (Exception e) {
			log.error("export_bill_query_result", e);
		}
		return bs;
	}

	// 出口押匯查詢直接下載
	public BaseResult fxExportBillQuerydirectDownload(String cusidn, Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			bs = export_bill_query_result(cusidn, reqParam);
			if (bs != null && bs.getResult()) {
				Map<String, Object> dataMap = (Map) bs.getData();
				List<Map<String, String>> downloadMap = new ArrayList();
				String downloadStringTXT = "";
				String downloadStringEXCEL = "";
				log.trace("dataMap={}", dataMap);
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
				log.debug("rowListMap={}", rowListMap);
				downloadMap = ((List<Map<String, String>>) ((Map<String, Object>) bs
						.getData()).get("CRY"));
				for (Map crydata : downloadMap) {
					downloadStringTXT += crydata.get("AMTRBILLCCY") + " " + crydata.get("FXTOTAMT") + "  i18n{LB.W0158}  "
							+ crydata.get("FXTOTAMTRECNUM") + "  i18n{LB.Rows}  "+"\r\n";
					downloadStringEXCEL += crydata.get("AMTRBILLCCY") + " " + crydata.get("FXTOTAMT") + "  i18n{LB.W0158}  "
							+ crydata.get("FXTOTAMTRECNUM") + "  i18n{LB.Rows}  "+"\n";
				}
				bs.addData("DownloadStringTXT", downloadStringTXT.trim());
				bs.addData("DownloadStringEXCEL", downloadStringEXCEL.trim());
				Map<String, Object> parameterMap = new HashMap<String, Object>();
				
				parameterMap.put("CMQTIME", dataMap.get("CMQTIME"));
				parameterMap.put("CMPERIOD", dataMap.get("CMPERIOD"));
				parameterMap.put("CMRECNUM", dataMap.get("CMRECNUM"));
				if("".equals(reqParam.get("LCNO"))){
					dataMap.put("LCNO", "i18n{LB.All}");
				}else {
					dataMap.put("LCNO", reqParam.get("LCNO"));
				}
				if("".equals(reqParam.get("REFNO"))){
					dataMap.put("REFNO", "i18n{LB.All}");
				}else {
					dataMap.put("REFNO", reqParam.get("REFNO"));
				}
				parameterMap.put("LCNO", dataMap.get("LCNO"));
				parameterMap.put("REFNO", dataMap.get("REFNO"));
				parameterMap.put("CURRENCY_TOTAL", dataMap.get("CURRENCY_TOTAL"));
				

				parameterMap.put("downloadFileName", reqParam.get("downloadFileName"));
				parameterMap.put("downloadType", reqParam.get("downloadType"));
				parameterMap.put("templatePath", reqParam.get("templatePath")); // parameterMap.put("hasMultiRowData",
																				// reqParam.get("hasMultiRowData"));

				String downloadType = reqParam.get("downloadType");
				log.trace(ESAPIUtil.vaildLog("downloadType={}"+downloadType));
				
				// EXCEL和OLDEXCEL下載
				if ("EXCEL".equals(downloadType) || "OLDEXCEL".equals(downloadType)) {
					parameterMap.put("headerRightEnd", reqParam.get("headerRightEnd"));
					parameterMap.put("headerBottomEnd", reqParam.get("headerBottomEnd"));
					parameterMap.put("rowStartIndex", reqParam.get("rowStartIndex"));
					parameterMap.put("rowRightEnd", reqParam.get("rowRightEnd"));
					parameterMap.put("footerStartIndex", reqParam.get("footerStartIndex"));
					parameterMap.put("footerEndIndex", reqParam.get("footerEndIndex"));
					parameterMap.put("footerRightEnd", reqParam.get("footerRightEnd"));
				}
				// TXT下載
				else {
					parameterMap.put("txtHeaderBottomEnd", reqParam.get("txtHeaderBottomEnd"));
					parameterMap.put("txtHasRowData", reqParam.get("txtHasRowData"));
					parameterMap.put("txtHasFooter", reqParam.get("txtHasFooter"));
				}
				bs.addData("parameterMap", parameterMap);
			}

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fxExportBillQuerydirectDownload error >> {}",e);
		}
		return bs;
	}

	/**
	 * 
	 * @param sessionId
	 * @return
	 */
	public BaseResult f_collection_query_result(Map<String, String> reqParam) {

		log.trace("f_collection_query_result_service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			List<Map<String, String>> recList= new ArrayList();
			// call ws API
			switch (reqParam.get("FUNC")) {
			// TODO: 多語系
			case "N559":
				bs = N559_REST(reqParam);
				recList = ((List<Map<String, String>>) ((Map<String, Object>) bs.getData()).get("REC"));
				for(Map row : recList) {
					row.put("RDRFTDAY", NumericUtil.fmtAmount((String)row.get("RDRFTDAY"),0));
					if("0".equals(row.get("RDRFTDAY"))) {
						row.put("RDRFTDAY", "-");
					}
					if(StrUtil.isNotEmpty((String) row.get("RCFMDATE"))) {
						row.put("RCFMDATE",  ((String)row.get("RCFMDATE")).replaceAll("-", "/"));
					}
					if(StrUtil.isNotEmpty((String) row.get("RCADATE"))) {
						row.put("RCADATE",  ((String)row.get("RCADATE")).replaceAll("-", "/"));
					}
					if("DA".equals(row.get("RPAYTERM"))){
						row.put("RPAYTERM", "LB.W0201");//承兌交單
						row.put("RPAYTERM_DOWNLOAD", i18n.getMsg("LB.W0201"));//承兌交單
					}
					if("DP".equals(row.get("RPAYTERM"))){
						row.put("RPAYTERM", "LB.W0202");//付款交單
						row.put("RPAYTERM_DOWNLOAD", i18n.getMsg("LB.W0202"));//付款交單
					}
				}
				bs.addData("REC", recList);
				
				if ("".equals(reqParam.get("ICNO")) || null == reqParam.get("ICNO")) {
					bs.addData("ICNO_SHOW", "LB.All");
					bs.addData("ICNO_DOWNLOAD", i18n.getMsg("LB.All"));
					bs.addData("ICNO", reqParam.get("ICNO"));
				} else {
					bs.addData("ICNO_SHOW", reqParam.get("ICNO"));
					bs.addData("ICNO", reqParam.get("ICNO"));
					bs.addData("ICNO_DOWNLOAD", reqParam.get("ICNO"));
				}
				break;

			case "N560":
				bs = N560_REST(reqParam);
				recList=((List<Map<String, String>>) ((Map<String, Object>) bs.getData()).get("REC"));
				for(Map row : recList) {
					if("0".equals(row.get("RUPSTAT"))) {
						row.put("RUPSTAT", "LB.W0197");//未拒付
						row.put("RUPSTAT_DOWNLOAD", i18n.getMsg("LB.W0197"));
					}
					if("1".equals(row.get("RUPSTAT"))) {
						row.put("RUPSTAT", "LB.W1739");//拒付
						row.put("RUPSTAT_DOWNLOAD", i18n.getMsg("LB.W1739"));
					}
					if("1".equals(row.get("RBILLTYP"))) {
						row.put("RBILLTYP", "SIGHT");
					}
					if("2".equals(row.get("RBILLTYP"))) {
						row.put("RBILLTYP", "USANCE");
					}
					if(StrUtil.isNotEmpty((String) row.get("RCMDATE1"))) {
						row.put("RCMDATE1",  ((String)row.get("RCMDATE1")).replaceAll("-", "/"));
					}
					if(StrUtil.isNotEmpty((String) row.get("RVALDATE"))) {
						row.put("RVALDATE",  ((String)row.get("RVALDATE")).replaceAll("-", "/"));
					}
					if(StrUtil.isNotEmpty((String) row.get("RNREMDAT"))) {
						row.put("RNREMDAT",  ((String)row.get("RNREMDAT")).replaceAll("-", "/"));
					}
				}
				
				if ("".equals(reqParam.get("LCNO")) || null == reqParam.get("LCNO")) {
					bs.addData("LCNO_SHOW", "LB.All");
					bs.addData("LCNO_DOWNLOAD", i18n.getMsg("LB.All"));
					bs.addData("LCNO", reqParam.get("LCNO"));
				} else {
					bs.addData("LCNO_SHOW", reqParam.get("LCNO"));
					bs.addData("LCNO", reqParam.get("LCNO"));
					bs.addData("LCNO_DOWNLOAD", reqParam.get("LCNO"));
				}
				
				if ("".equals(reqParam.get("REFNO")) || null == reqParam.get("REFNO")) {
					bs.addData("REFNO_SHOW", "LB.All");
					bs.addData("REFNO_DOWNLOAD", i18n.getMsg("LB.All"));
					bs.addData("REFNO", reqParam.get("REFNO"));
				} else {
					bs.addData("REFNO_SHOW", reqParam.get("REFNO"));
					bs.addData("REFNO", reqParam.get("REFNO"));
					bs.addData("REFNO_DOWNLOAD", reqParam.get("REFNO"));
				}
				
				break;

			case "N563":
				bs = N563_REST(reqParam);
				
				recList=((List<Map<String, String>>) ((Map<String, Object>) bs.getData()).get("REC"));
				
				for(Map row : recList) {
					
//					row.put("RTENOR1", NumericUtil.fmtAmount((String)row.get("RTENOR1"),0));
//					if("0".equals(row.get("RTENOR1"))) {
//						row.put("RTENOR1", "-");
//					}
					
					if(StrUtil.isNotEmpty((String) row.get("RMATDAT1"))) {
						row.put("RMATDAT1",  ((String)row.get("RMATDAT1")).replaceAll("-", "/"));
					}
					if(StrUtil.isNotEmpty((String) row.get("RPAYDATE"))) {
						row.put("RPAYDATE",  ((String)row.get("RPAYDATE")).replaceAll("-", "/"));
					}
					if(StrUtil.isNotEmpty((String) row.get("RACCEPDT"))) {
						row.put("RACCEPDT",  ((String)row.get("RACCEPDT")).replaceAll("-", "/"));
					}
					if(StrUtil.isNotEmpty((String) row.get("RVALDATE"))) {
						row.put("RVALDATE",  ((String)row.get("RVALDATE")).replaceAll("-", "/"));
					}
				}
				break;
			}
		} catch (Exception e) {
			log.error("f_collection_query_result_Service ", e);
		}

		return bs;
	}
	
	/**
	 * 
	 * @param sessionId
	 * @return
	 */
	public BaseResult f_collection_query_directDownload(Map<String, String> reqParam) {

		log.trace("f_collection_query_directDownload_service");
		BaseResult bs = null;
		try {
			bs=f_collection_query_result(reqParam);
			log.trace("D.download bs >>{}",bs);
			List<Map<String, String>> downloadMap = new ArrayList();
			String downloadStringTXT = "";
			String downloadStringEXCEL = "";
			if (bs != null && bs.getResult()) {
				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap={}", dataMap);
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
				List<Map<String, String>> rowListMap2 = (List<Map<String, String>>) dataMap.get("CRY");
				for(Map crydata : rowListMap2) {
					downloadStringTXT += crydata.get("AMTRBILLCCY") + " " + crydata.get("FXTOTAMT") + "  i18n{LB.W0158}  "
							+ crydata.get("FXTOTAMTRECNUM") + "  i18n{LB.Rows}  "+"\r\n";
					downloadStringEXCEL += crydata.get("AMTRBILLCCY") + " " + crydata.get("FXTOTAMT") + "  i18n{LB.W0158}  "
							+ crydata.get("FXTOTAMTRECNUM") + "  i18n{LB.Rows}  "+"\n";
				}
				if("".equals(reqParam.get("ICNO"))){
					dataMap.put("ICNO", "i18n{LB.All}");
				}else {
					dataMap.put("ICNO", reqParam.get("ICNO"));
				}
				if("".equals(reqParam.get("LCNO"))){
					dataMap.put("LCNO", "i18n{LB.All}");
				}else {
					dataMap.put("LCNO", reqParam.get("LCNO"));
				}
				if("".equals(reqParam.get("REFNO"))){
					dataMap.put("REFNO", "i18n{LB.All}");
				}else {
					dataMap.put("REFNO", reqParam.get("REFNO"));
				}
				log.debug("rowListMap={}", rowListMap);
				Map<String, Object> parameterMap = new HashMap<String, Object>();
				reqParam.put("CMQTIME", (String)dataMap.get("CMQTIME"));
				reqParam.put("CMPERIOD", (String)dataMap.get("CMPERIOD"));
				reqParam.put("CMRECNUM", (String)dataMap.get("CMRECNUM"));
				reqParam.put("CURRENCY_TOTAL_TXT", downloadStringTXT.trim());
				reqParam.put("CURRENCY_TOTAL_EXCEL", downloadStringEXCEL.trim());
				reqParam.put("ICNO",  (String)dataMap.get("ICNO"));
				reqParam.put("LCNO",  (String)dataMap.get("LCNO"));
				reqParam.put("REFNO",  (String)dataMap.get("REFNO"));
				
			}
			bs.addData("parameterMap", reqParam);
			
		} catch (Exception e) {
			log.error("f_collection_query_directDownload_service ", e);
		}

		return bs;
	}
	

	/**
	 * 輸入beanId && beanMapKey Get beanMapValue
	 * 
	 * @param beanId
	 * @param beanMapKey
	 * @see spring-arg.xml
	 * @return get spring-arg.xml beanMapValue By beanId && beanMapKey, if any (or
	 *         empty String otherwise)
	 */
	public String getBeanMapValue(String beanId, String beanMapKey) {
		// 取得Bean中的Map
		Map<String, String> getTypeBean = SpringBeanFactory.getBean(beanId);
		// 取得Map中的Value
		String getTypeBeanValue = getTypeBean.get(beanMapKey);
		return getTypeBeanValue;
	}
	//下載舊txt檔，將數字負號移至後面
	//type 1為如果數字為正，後面加空白，type 2為如果數字為正，後面加加號
	public String format(String input, int type) {
		
		if(type == 1) {
			if(input.indexOf("-")!=-1) {
				input = input.replaceAll("-","");
				input+="-";
			}
			else {
				input+=" ";
			}
		}
		else if(type == 2) {
			if(input.indexOf("-")!=-1) {
				input = input.replaceAll("-","");
				input+="-";
			}
			else {
				input+="+";
			}
		}
		
		return input;
	}
	
	
	public static void main(String[] args) {
		String test = NumericUtil.formatNumberString("000000000031875", 2);
		System.out.println(test);
	}

}
