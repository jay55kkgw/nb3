package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N814_REST_RQ extends BaseRestBean_CC implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6429958096970870866L;
	private String FGTXWAY;
	private String PINNEW;
	private String FLAG;
	private String CUSIDN;
	private String TRANSPASSUPDATE;
	private String PHASE2;
	private String ADOPID;
	
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public String getFLAG() {
		return FLAG;
	}
	public void setFLAG(String fLAG) {
		FLAG = fLAG;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getTRANSPASSUPDATE() {
		return TRANSPASSUPDATE;
	}
	public void setTRANSPASSUPDATE(String tRANSPASSUPDATE) {
		TRANSPASSUPDATE = tRANSPASSUPDATE;
	}
	public String getPHASE2() {
		return PHASE2;
	}
	public void setPHASE2(String pHASE2) {
		PHASE2 = pHASE2;
	}
}
