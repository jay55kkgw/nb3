package tw.com.fstop.nnb.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import fstop.orm.po.ADMCURRENCY;
import fstop.orm.po.TXNFUNDDATA;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtils;
import tw.com.fstop.web.util.WebUtil;

/**
 * 基金贖回交易的Service
 */
@Service
public class Fund_Redeem_Service extends Base_Service{
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private Fund_Transfer_Service fund_transfer_service;
	
	@Autowired
	private I18n i18n;
	/**
	 * 基金贖回交易查詢
	 */
	@SuppressWarnings("unchecked")
	public BaseResult fund_redeem_data(String cusidn,Map<String, String>reqParam){
		BaseResult bs = null;
		try{
			bs = new BaseResult();
			bs = C024_REST(cusidn);
			if(bs != null && bs.getResult()){
				Map<String,Object> bsData = (Map<String,Object>)bs.getData();
				List<Map<String,String>> rows = (List<Map<String,String>>)bsData.get("REC");
				
				String CDNO;
				String hiddenCDNO;
				String CRY;
				ADMCURRENCY admCurrency;
				String ADCCYNAME;
				String FUNDAMT;
				String formatFUNDAMT;
				String UNIT;
				String formatUNIT;
				String FUSMON;
				
				for(int x=0;x<rows.size();x++){
					Map<String,String> rowMap = rows.get(x);
					
					CDNO = rowMap.get("CDNO");
					log.debug("CREDITNO={}",CDNO);
					hiddenCDNO = WebUtil.hideAccount(CDNO);
					log.debug("hiddenCREDITNO={}",hiddenCDNO);
					rowMap.put("hiddenCDNO",hiddenCDNO);
					
					CRY = rowMap.get("CRY");
					log.debug("CRY={}",CRY);
					admCurrency = fund_transfer_service.getCRYData(CRY);
					
					if(admCurrency != null){
						//i18n
						Locale currentLocale = LocaleContextHolder.getLocale();
			    		String locale = currentLocale.toString();
			    		
			    		switch (locale) {
						case "en":
							ADCCYNAME = admCurrency.getADCCYENGNAME();
							break;
						case "zh_CN":
							ADCCYNAME = admCurrency.getADCCYCHSNAME();
							break;
						default:
							ADCCYNAME = admCurrency.getADCCYNAME();
							break;
		                }
						log.debug("ADCCYNAME={}",ADCCYNAME);
						rowMap.put("ADCCYNAME",ADCCYNAME);
					}
					FUNDAMT = rowMap.get("FUNDAMT");
					log.debug("FUNDAMT={}",FUNDAMT);
					FUNDAMT = FUNDAMT.replaceAll("\\.","").replaceAll("\\,","");
					log.debug("FUNDAMT={}",FUNDAMT);
					
					formatFUNDAMT = fund_transfer_service.formatNumberString(FUNDAMT,2);
					log.debug("formatFUNDAMT={}",formatFUNDAMT);
					rowMap.put("formatFUNDAMT",formatFUNDAMT);
					
					UNIT = rowMap.get("UNIT");
					log.debug("UNIT={}",UNIT);
					UNIT = UNIT.replaceAll("\\.","").replaceAll("\\,","");
					log.debug("UNIT={}",UNIT);
					
					formatUNIT = fund_transfer_service.formatNumberString(UNIT,4);
					log.debug("formatUNIT={}",formatUNIT);
					rowMap.put("formatUNIT",formatUNIT);
					
					FUSMON = rowMap.get("FUSMON");
					if("000".equals(FUSMON)) {
						rowMap.put("FEE_TYPE", "B");
					}else {
						rowMap.put("FEE_TYPE", "A");
					}
					
				}
			}else {
				return bs;
			}
			//C024處理結束
			//N922開始
			BaseResult n922BS = null;
			Map<String,Object> bsData = (Map<String,Object>)bs.getData();
			List<Map<String,String>> rows = (List<Map<String,String>>)bsData.get("REC");
			
			n922BS = new BaseResult();
			n922BS = fund_transfer_service.getN922Data(cusidn);
			if(n922BS!=null && n922BS.getResult()) {
				
			}else {
				bs=n922BS;
				return bs;
			}
			
			Map<String,Object> n922BSData = (Map<String,Object>)n922BS.getData();
			String hiddenCUSIDN = WebUtil.hideID(cusidn);
			String ACN3 = (String)n922BSData.get("ACN3");
			String ACN4 = (String)n922BSData.get("ACN4");
			//信託號碼
			String CDNO = reqParam.get("CDNO");
			//基金代碼
			String TRANSCODE = reqParam.get("TRANSCODE");
//			//姓名
//			String NAME = n922BSData.get("NAME") == null ? "" : (String)n922BSData.get("NAME");
			//20210617修正  若有CUSNAME取CUSNAME 無則為NAME , 不然會是空的 //N922原住民姓名修改由NAME->CUSNAME  
			String NAME = "";
			if(StrUtils.isNotEmpty((String)n922BSData.get("CUSNAME"))) {
				NAME = (String)n922BSData.get("CUSNAME");
			}else {
				NAME = (String)n922BSData.get("NAME");
			}
			String hiddenNAME = WebUtil.hideusername1Convert(NAME);
			//投資屬性
			String FDINVTYPE = n922BSData.get("FDINVTYPE") == null ? "" : (String)n922BSData.get("FDINVTYPE");
			bs.addData("CUSIDN", cusidn);
			bs.addData("UID", cusidn);
			bs.addData("hiddenCUSIDN", hiddenCUSIDN);
			bs.addData("ACN3", ACN3);
			bs.addData("ACN4", ACN4);
			bs.addData("CDNO", CDNO);
			bs.addData("TRANSCODE", TRANSCODE);
			bs.addData("NAME", NAME);
			bs.addData("hiddenNAME", hiddenNAME);
			bs.addData("FDINVTYPE", FDINVTYPE);
			
			boolean breakFlag = false;
			
			List<Map<String,String>> fundRows = new ArrayList<Map<String,String>>();
			
			for(int x=0;x<rows.size();x++){
				Map<String,String> rowMap = rows.get(x);
				log.debug("rowMap={}",rowMap);
				
				if((CDNO != null && !CDNO.equals(rowMap.get("CDNO"))) || (TRANSCODE != null && !TRANSCODE.equals(rowMap.get("TRANSCODE")))){	
					continue;
				}
				if((CDNO != null && CDNO.equals(rowMap.get("CDNO"))) && (TRANSCODE != null && TRANSCODE.equals(rowMap.get("TRANSCODE")))){
					breakFlag = true;
				}
				
				TXNFUNDDATA txnFundData = fund_transfer_service.getFundData(rowMap.get("TRANSCODE"));
				
				if(txnFundData != null){
					rowMap.put("FUNDLNAME",txnFundData.getFUNDLNAME());
					rowMap.put("FUNDT", txnFundData.getFUNDT());
					rowMap.put("C30AMT", txnFundData.getC30AMT());
					rowMap.put("C30RAMT", txnFundData.getC30RAMT());
					rowMap.put("Y30AMT", txnFundData.getY30AMT());
					rowMap.put("Y30RAMT", txnFundData.getY30RAMT());
					rowMap.put("C302AMT", txnFundData.getC302AMT());
					rowMap.put("C302RAMT", txnFundData.getC302RAMT());
					rowMap.put("Y302AMT", txnFundData.getY302AMT());
					rowMap.put("Y302RAMT", txnFundData.getY302RAMT());
				}
				fundRows.add(rowMap);
			
				if(breakFlag){
					break;
				}
			}
			bs.addData("fundRows",fundRows);
			bs.addData("dataCount",fundRows.size());
			//bs.addData("bsData",bsData);bsData就是bs.data.REC
			//N922結束		
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_redeem_data error >> {}",e);
		}
		return bs;
	}
	/**
	 * 準備前往基金贖回選擇頁的資料
	 */
	public void prepareFundRedeemSelectData(Map<String,String> requestParam,BaseResult bs){
		String CUSIDN = requestParam.get("CUSIDN");
		String hiddenCUSIDN = WebUtil.hideID(CUSIDN);
		bs.addData("hiddenCUSIDN",hiddenCUSIDN);
		
		String CUSNAME = requestParam.get("CUSNAME");
		bs.addData("CUSNAME",CUSNAME);
		String hiddenNAME = WebUtil.hideusername1Convert(CUSNAME);
		bs.addData("hiddenNAME",hiddenNAME);
		
		String CDNO = requestParam.get("CDNO");
		bs.addData("CDNO",CDNO);
		String hiddenCDNO = WebUtil.hideAccount(CDNO);
		bs.addData("hiddenCDNO",hiddenCDNO);
		
		String TRANSCODE = requestParam.get("TRANSCODE");
		log.debug(ESAPIUtil.vaildLog("TRANSCODE={}"+TRANSCODE));
		bs.addData("TRANSCODE",TRANSCODE);
		
		String CRY = requestParam.get("CRY");
		log.debug(ESAPIUtil.vaildLog("CRY={}"+CRY));
		bs.addData("CRY",CRY);

		String TYPE10 = requestParam.get("TYPE10");
		log.debug(ESAPIUtil.vaildLog("TYPE10={}"+TYPE10));
		bs.addData("TYPE10", TYPE10);
		
		String FUNDT = requestParam.get("FUNDT");
		log.debug(ESAPIUtil.vaildLog("FUNDT={}"+FUNDT));
		bs.addData("FUNDT", FUNDT);
		
		String RAMT = "";
		String AMT = "";
		if(CRY.equals("NTD")) {
			if (TYPE10.equals("1")) {
				RAMT = requestParam.get("C30RAMT");
				AMT = requestParam.get("C30AMT");
			} else {
				RAMT = requestParam.get("C302RAMT");
				AMT = requestParam.get("C302AMT");
			}
		} else {
			if (TYPE10.equals("1")) {
				RAMT = requestParam.get("Y30RAMT");
				AMT = requestParam.get("Y30AMT");
			} else {
				RAMT = requestParam.get("Y302RAMT");
				AMT = requestParam.get("Y302AMT");
			}
		}
		bs.addData("RAMT", RAMT);
		bs.addData("AMT", AMT);
		log.debug("CRY = " + CRY + ", TYPE10 = " + TYPE10 + ", RAMT = " + RAMT + ", AMT = " + AMT);
		
		String FORMAT_RAMT = fund_transfer_service.formatNumberString(RAMT,0);
		bs.addData("FORMAT_RAMT", FORMAT_RAMT);
		String FORMAT_AMT = fund_transfer_service.formatNumberString(AMT,0);
		bs.addData("FORMAT_AMT", FORMAT_AMT);
		
		ADMCURRENCY admCurrency = fund_transfer_service.getCRYData(CRY);
		if(admCurrency != null){
			String ADCCYNAME = "";
			//i18n
			Locale currentLocale = LocaleContextHolder.getLocale();
    		String locale = currentLocale.toString();
    		
    		switch (locale) {
			case "en":
				ADCCYNAME = admCurrency.getADCCYENGNAME();
				break;
			case "zh_CN":
				ADCCYNAME = admCurrency.getADCCYCHSNAME();
				break;
			default:
				ADCCYNAME = admCurrency.getADCCYNAME();
				break;
            }
			log.debug(ESAPIUtil.vaildLog("ADCCYNAME={}"+ADCCYNAME));
			bs.addData("ADCCYNAME",ADCCYNAME);
		}
		
		String OFUNDAMT = requestParam.get("OFUNDAMT");
		log.debug(ESAPIUtil.vaildLog("OFUNDAMT={}"+OFUNDAMT));
		bs.addData("OFUNDAMT",OFUNDAMT);
		
		String OFUNDAMTDotTwo = OFUNDAMT.replaceAll("\\.","");
		log.debug("OFUNDAMTDotTwo={}",OFUNDAMTDotTwo);
		
		OFUNDAMTDotTwo = fund_transfer_service.formatNumberString(OFUNDAMTDotTwo,2);
		log.debug("OFUNDAMTDotTwo={}",OFUNDAMTDotTwo);
		bs.addData("OFUNDAMTDotTwo",OFUNDAMTDotTwo);
		
		String UNIT = requestParam.get("UNIT");
		log.debug(ESAPIUtil.vaildLog("UNIT={}"+UNIT));
		UNIT = fund_transfer_service.formatNumberString(UNIT,4);
		log.debug(ESAPIUtil.vaildLog("UNIT={}"+UNIT));
		bs.addData("UNIT",UNIT);
		
		String BILLSENDMODE = requestParam.get("BILLSENDMODE");
		log.debug(ESAPIUtil.vaildLog("BILLSENDMODE={}"+BILLSENDMODE));
		bs.addData("BILLSENDMODE",BILLSENDMODE);
		
		String SHORTTRADE = requestParam.get("SHORTTRADE");
		log.debug(ESAPIUtil.vaildLog("SHORTTRADE={}"+SHORTTRADE));
		bs.addData("SHORTTRADE",SHORTTRADE);
		
		String SHORTTUNIT = requestParam.get("SHORTTUNIT");
		log.debug(ESAPIUtil.vaildLog("SHORTTUNIT={}"+SHORTTUNIT));
		bs.addData("SHORTTUNIT",SHORTTUNIT);
		
		String FDINVTYPE = requestParam.get("FDINVTYPE");
		log.debug(ESAPIUtil.vaildLog("FDINVTYPE={}"+FDINVTYPE));
		bs.addData("FDINVTYPE",FDINVTYPE);
		
		String FUNDLNAME = requestParam.get("FUNDLNAME");
		log.debug(ESAPIUtil.vaildLog("FUNDLNAME={}"+FUNDLNAME));
		bs.addData("FUNDLNAME",FUNDLNAME);
		
		String TRADEDATE = requestParam.get("TRADEDATE");
		log.debug(ESAPIUtil.vaildLog("TRADEDATE={}"+TRADEDATE));
		bs.addData("TRADEDATE",TRADEDATE);
		
		String TRADEDATEFormat = TRADEDATE.substring(1,4) + "/" + TRADEDATE.substring(4,6) + "/" + TRADEDATE.substring(6);
		log.debug(ESAPIUtil.vaildLog("TRADEDATEFormat={}"+TRADEDATEFormat));
		bs.addData("TRADEDATEFormat",TRADEDATEFormat);
		
		String FUNDACN = requestParam.get("FUNDACN");
		log.debug(ESAPIUtil.vaildLog("FUNDACN={}"+FUNDACN));
		bs.addData("FUNDACN",FUNDACN);
		
		String ACN3 = requestParam.get("ACN3");
		log.debug(ESAPIUtil.vaildLog("ACN3={}"+ACN3));
		bs.addData("ACN3",ACN3);
		
		String ACN4 = requestParam.get("ACN4");
		log.debug(ESAPIUtil.vaildLog("ACN4={}"+ACN4));
		bs.addData("ACN4",ACN4);
		
		String BILLSENDMODEChinese;
		//全部
		if("1".equals(BILLSENDMODE)){
			BILLSENDMODEChinese = i18n.getMsg("LB.All");//全部
			bs.addData("BILLSENDMODEChinese",BILLSENDMODEChinese);
			
			if(OFUNDAMT.indexOf(".") != -1){
				OFUNDAMT = OFUNDAMT.substring(0,OFUNDAMT.indexOf("."));		
			}
			log.debug(ESAPIUtil.vaildLog("OFUNDAMT={}"+OFUNDAMT));
			OFUNDAMT = fund_transfer_service.formatNumberString(OFUNDAMT,0);
			log.debug(ESAPIUtil.vaildLog("OFUNDAMT={}"+OFUNDAMT));
			
			bs.addData("FUNDAMT",OFUNDAMT);
		}
		//部分
		else{
			BILLSENDMODEChinese = i18n.getMsg("LB.X0383");
			bs.addData("BILLSENDMODEChinese",BILLSENDMODEChinese);
		}
		
		String FEE_TYPE = requestParam.get("FEE_TYPE");
		log.debug(ESAPIUtil.vaildLog("FEE_TYPE={}"+FEE_TYPE));
		bs.addData("FEE_TYPE",FEE_TYPE);
	}
	/**
	 * 準備前往基金贖回確認頁的資料
	 */
	public BaseResult prepareFundRedeemConfirmData(Map<String,String> requestParam,String cusidn){
		BaseResult bs=new BaseResult();

		//*fund_transfer_service.getN922Data有建新BS
		bs= fund_transfer_service.getN922Data(cusidn);
		if(bs == null || !bs.getResult()){
			return bs;
		}	
		bs.addAllData(requestParam);
		
//		String TRANSCODE = requestParam.get("TRANSCODE");
//		bs.addData("TRANSCODE",TRANSCODE);
//		
//		String CREDITNO = requestParam.get("CREDITNO");
//		bs.addData("CREDITNO",CREDITNO);
//		
//		String FUNDLNAME = requestParam.get("FUNDLNAME");
//		bs.addData("FUNDLNAME",FUNDLNAME);
//		
//		String CRY = requestParam.get("CRY");
//		bs.addData("CRY",CRY);
//		
//		String OFUNDAMT = requestParam.get("OFUNDAMT");
//		bs.addData("OFUNDAMT",OFUNDAMT);
//		
//		String TRADEDATE = requestParam.get("TRADEDATE");
		bs.addData("RESTOREDAY",((Map<String, String>) bs.getData()).get("TRADEDATE"));
//		
//		String BILLSENDMODE = requestParam.get("BILLSENDMODE");
//		bs.addData("BILLSENDMODE",BILLSENDMODE);
//		
//		String UNIT = requestParam.get("UNIT");
//		bs.addData("UNIT",UNIT);
//		
//		String FUNDACN = requestParam.get("FUNDACN");
//		bs.addData("FUNDACN",FUNDACN);
//		
//		String SHORTTRADE = requestParam.get("SHORTTRADE");
//		bs.addData("SHORTTRADE",SHORTTRADE);
//		
//		String SHORTTUNIT = requestParam.get("SHORTTUNIT");
//		bs.addData("SHORTTUNIT",SHORTTUNIT);
//				
//		String FUNDAMT = requestParam.get("FUNDAMT");
//		bs.addData("FUNDAMT",FUNDAMT);
		
		String hiddenCUSIDN = WebUtil.hideID(cusidn);
		bs.addData("hiddenCUSIDN",hiddenCUSIDN);
		
//		String hiddenNAME = WebUtil.hideusername1Convert(((Map<String, String>) bs.getData()).get("NAME"));
//		bs.addData("hiddenNAME",hiddenNAME);
		
		//20210617修正  若有CUSNAME取CUSNAME 無則為NAME , 不然會是空的 //N922原住民姓名修改由NAME->CUSNAME  
		String NAME = "";
		if(StrUtils.isNotEmpty(((Map<String, String>) bs.getData()).get("CUSNAME"))) {
			NAME = ((Map<String, String>) bs.getData()).get("CUSNAME");
		}else {
			NAME = ((Map<String, String>) bs.getData()).get("NAME");
		}
		
		String hiddenNAME = WebUtil.hideusername1Convert(NAME);
		bs.addData("hiddenNAME",hiddenNAME);
				
		String hiddenCDNO = WebUtil.hideAccount(((Map<String, String>) bs.getData()).get("CDNO"));
		bs.addData("hiddenCDNO",hiddenCDNO);
		
		ADMCURRENCY admCurrency = fund_transfer_service.getCRYData(((Map<String, String>) bs.getData()).get("CRY"));
		if(admCurrency != null){
			String ADCCYNAME = "";
			//i18n
			Locale currentLocale = LocaleContextHolder.getLocale();
    		String locale = currentLocale.toString();
    		
    		switch (locale) {
			case "en":
				ADCCYNAME = admCurrency.getADCCYENGNAME();
				break;
			case "zh_CN":
				ADCCYNAME = admCurrency.getADCCYCHSNAME();
				break;
			default:
				ADCCYNAME = admCurrency.getADCCYNAME();
				break;
            }
			bs.addData("ADCCYNAME",ADCCYNAME);
		}
			
		String OFUNDAMTDotTwo = ((Map<String, String>) bs.getData()).get("OFUNDAMT").replaceAll("\\.","");		
		OFUNDAMTDotTwo = fund_transfer_service.formatNumberString(OFUNDAMTDotTwo,2);
		bs.addData("OFUNDAMTDotTwo",OFUNDAMTDotTwo);
		
		String TRADEDATE=((Map<String, String>) bs.getData()).get("TRADEDATE");
		String TRADEDATEFormat = TRADEDATE.substring(1,4) + "/" + TRADEDATE.substring(4,6) + "/" + TRADEDATE.substring(6);
		bs.addData("TRADEDATEFormat",TRADEDATEFormat);
				
		String UNITNoComma = ((Map<String, String>) bs.getData()).get("UNIT").replaceAll(",","");
		bs.addData("UNITNoComma",UNITNoComma);
				
		String BILLSENDMODEChinese;
		String BILLSENDMODE=((Map<String, String>) bs.getData()).get("BILLSENDMODE");
		String FUNDAMT=((Map<String, String>) bs.getData()).get("FUNDAMT");
		//全部or部分
		if("1".equals(BILLSENDMODE)){
			BILLSENDMODEChinese = i18n.getMsg("LB.All");//全部
			bs.addData("BILLSENDMODEChinese",BILLSENDMODEChinese);
		}else{
			BILLSENDMODEChinese = i18n.getMsg("LB.X0383");
			bs.addData("BILLSENDMODEChinese",BILLSENDMODEChinese);
		}
		//全部or部分
		if("1".equals(BILLSENDMODE)){
			bs.addData("FUNDAMTFormat",FUNDAMT);
		}else{
			String FUNDAMTFormat = fund_transfer_service.formatNumberString(FUNDAMT,0);
			bs.addData("FUNDAMTFormat",FUNDAMTFormat);
		}
		String FUNDAMTNoComma = FUNDAMT.replaceAll(",","");
		bs.addData("FUNDAMTNoComma",FUNDAMTNoComma);
		
		String FEE_TPYE = requestParam.get("FEE_TPYE");
		bs.addData("FEE_TPYE",FEE_TPYE);
				
		return bs;
	}
	/**
	 * 基金贖回交易
	 */
	@SuppressWarnings("unchecked")
	public BaseResult processFundRedeem(Map<String,String> requestParam){
		BaseResult bs = null;
		try{
			bs = new BaseResult();
			
			String UNITOriginal = requestParam.get("UNITNoComma").replaceAll("\\.","");
			log.debug("UNITOriginal={}",UNITOriginal);
			
			while(UNITOriginal.length() < 12){
				UNITOriginal = "0" + UNITOriginal;
			}
			log.debug("UNITOriginal={}",UNITOriginal);
			requestParam.put("UNITOriginal",UNITOriginal);
			
			bs = C114_REST(requestParam);
			if(bs != null && bs.getResult()){
				Map<String,Object> bsData = (Map<String,Object>)bs.getData();
				log.debug("bsData={}",bsData);
				
				String CDNO = (String)bsData.get("CDNO");
				log.debug("CDNO={}",CDNO);
				
				String hiddenCDNO = WebUtil.hideAccount(CDNO);
				log.debug("hiddenCREDITNO={}",hiddenCDNO);
				bs.addData("hiddenCDNO",hiddenCDNO);
				
				String TRANSCODE = (String)bsData.get("TRANSCODE");
				log.debug("TRANSCODE={}",TRANSCODE);
				
				String FUNDCUR = (String)bsData.get("FUNDCUR");
				log.debug("FUNDCUR={}",FUNDCUR);
				
				TXNFUNDDATA txnFundData = fund_transfer_service.getFundData(TRANSCODE);
				String FUNDLNAME = "";
				
				if(txnFundData != null){
					FUNDLNAME = txnFundData.getFUNDLNAME();
					
					ADMCURRENCY admCurrency = fund_transfer_service.getCRYData(FUNDCUR);
					
					if(admCurrency != null){
						String ADCCYNAME = "";
						//i18n
						Locale currentLocale = LocaleContextHolder.getLocale();
			    		String locale = currentLocale.toString();
			    		
			    		switch (locale) {
						case "en":
							ADCCYNAME = admCurrency.getADCCYENGNAME();
							break;
						case "zh_CN":
							ADCCYNAME = admCurrency.getADCCYCHSNAME();
							break;
						default:
							ADCCYNAME = admCurrency.getADCCYNAME();
							break;
		                }
						log.debug("ADCCYNAME={}",ADCCYNAME);
						bs.addData("ADCCYNAME",ADCCYNAME);
					}
				}
				log.debug("FUNDLNAME={}",FUNDLNAME);
				bs.addData("FUNDLNAME",FUNDLNAME);

				String BILLSENDMODE = (String)bsData.get("BILLSENDMODE");
				log.debug("BILLSENDMODE={}",BILLSENDMODE);
				
				String BILLSENDMODEChinese;
				//全部
				if("1".equals(BILLSENDMODE)){
					BILLSENDMODEChinese = i18n.getMsg("LB.All");
					bs.addData("BILLSENDMODEChinese",BILLSENDMODEChinese);
				}
				//部分
				else{
					BILLSENDMODEChinese = i18n.getMsg("LB.X0383");
					bs.addData("BILLSENDMODEChinese",BILLSENDMODEChinese);
				}
				
				String UNIT = (String)bsData.get("UNIT");
				log.debug("UNIT={}",UNIT);
				String UNITFormat = fund_transfer_service.formatNumberString(UNIT,4);
				log.debug("UNITFormat={}",UNITFormat);
				bs.addData("UNITFormat",UNITFormat);
				
				String FUNDAMT = (String)bsData.get("FUNDAMT");
				log.debug("FUNDAMT={}",FUNDAMT);
				String FUNDAMTNoDot = FUNDAMT.replaceAll("\\.","");
				log.debug("FUNDAMTNoDot={}",FUNDAMTNoDot);
				
				String FUNDAMTFormat = fund_transfer_service.formatNumberString(FUNDAMTNoDot,2);
				log.debug("FUNDAMTFormat={}",FUNDAMTFormat);
				bs.addData("FUNDAMTFormat",FUNDAMTFormat);
				
				String TRADEDATE = (String)bsData.get("RESTOREDAY");
				log.debug("TRADEDATE={}",TRADEDATE);
				
				String TRADEDATEFormat = TRADEDATE.substring(1,4) + "/" + TRADEDATE.substring(4,6) + "/" + TRADEDATE.substring(6);
				log.debug("TRADEDATEFormat={}",TRADEDATEFormat);
				bs.addData("TRADEDATEFormat",TRADEDATEFormat);
				
				BaseResult n922BS = new BaseResult();
				n922BS = fund_transfer_service.getN922Data(requestParam.get("CUSIDN"));
				
				if(n922BS != null && n922BS.getResult()){
					Map<String,Object> n922BSData = (Map<String,Object>)n922BS.getData();
					log.debug("n922BSData={}",n922BSData);
					
					//20210617修正  若有CUSNAME取CUSNAME 無則為NAME , 不然會是空的 //N922原住民姓名修改由NAME->CUSNAME  
					String NAME = "";
					if(StrUtils.isNotEmpty((String)n922BSData.get("CUSNAME"))) {
						NAME = (String)n922BSData.get("CUSNAME");
					}else {
						NAME = (String)n922BSData.get("NAME");
					}
					
					String hiddenNAME = WebUtil.hideusername1Convert(NAME);
					log.debug("hiddenNAME={}",hiddenNAME);
					bs.addData("hiddenNAME",hiddenNAME);
				}
				//bs.addData("bsData",bsData);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("processFundRedeem error >> {}",e);
		}
		return bs;
	}
	/**
	 * 基金贖回終止扣款
	 */
	@SuppressWarnings("unchecked")
	public BaseResult fundStopPay(Map<String,String> requestParam,Model model){
		BaseResult bs = null;
		try{
			bs = new BaseResult();
			
			String CDNO = requestParam.get("CDNO");
			log.debug(ESAPIUtil.vaildLog("CREDITNO={}"+CDNO));
			requestParam.put("CDNO",CDNO);
			
			String TRANSCODE = requestParam.get("TRANSCODE");
			log.debug(ESAPIUtil.vaildLog("TRANSCODE={}"+TRANSCODE));
			requestParam.put("TRANSCODE",TRANSCODE);
			
			String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
			log.debug("cusidn={}",cusidn);
			requestParam.put("CUSIDN",cusidn);
			
			requestParam.put("ALTERTYPE","B0");
			requestParam.put("PAYDAY1","");
			requestParam.put("PAYDAY2","");
			requestParam.put("PAYDAY3","");
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String ADDate = sdf.format(new Date());
			log.debug("ADDate={}",ADDate);
			int ROCYear = Integer.valueOf(ADDate.substring(0,4)) - 1911;
			log.debug("ROCYear={}",ROCYear);
			String ROCDate = ROCYear + ADDate.substring(4);
			log.debug("ROCDate={}",ROCDate);
			
			requestParam.put("STOPBEGDAY",ROCDate);
			requestParam.put("STOPENDDAY","99999999");
			requestParam.put("RESTOREDAY","0");
			requestParam.put("TYPEFLAG","     ");
			
			//寄件者信箱
			String DPMYEMAIL = (String)SessionUtil.getAttribute(model,SessionUtil.DPMYEMAIL,null);
			log.debug("DPMYEMAIL={}",DPMYEMAIL);
			requestParam.put("DPMYEMAIL",DPMYEMAIL);
			
			//收件者信箱
			requestParam.put("CMTRMAIL",DPMYEMAIL);
			
			//20210322
			String SKIP_P = requestParam.get("SKIP_P");
			requestParam.put("SKIP_P", SKIP_P);
			
			bs = C112_REST(requestParam);
			if(bs != null && bs.getResult()){
				Map<String,Object> bsData = (Map<String,Object>)bs.getData();
				log.debug("bsData={}",bsData);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fundStopPay error >> {}",e);
		}
		return bs;
	}
}
