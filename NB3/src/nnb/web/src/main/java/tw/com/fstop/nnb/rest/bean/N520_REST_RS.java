package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * 外匯活期存款交易明細查詢
 * 
 * @author Ian
 *
 */
public class N520_REST_RS extends BaseRestBean implements Serializable {

	private static final long serialVersionUID = -3140796814039597245L;
	
	private String CMQTIME;

	private String CMRECNUM;;// 筆數

	private String CMPERIOD;

	private LinkedList<N520_REST_RSDATA> REC;

	public String getCMQTIME() {
		return CMQTIME;
	}

	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}

	public String getCMRECNUM() {
		return CMRECNUM;
	}

	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}

	public String getCMPERIOD() {
		return CMPERIOD;
	}

	public void setCMPERIOD(String cMPERIOD) {
		CMPERIOD = cMPERIOD;
	}

	public LinkedList<N520_REST_RSDATA> getREC() {
		return REC;
	}

	public void setREC(LinkedList<N520_REST_RSDATA> rEC) {
		REC = rEC;
	}


}
