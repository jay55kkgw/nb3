package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class Na30_REST_RS extends BaseRestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7245151281672301339L;
	private String MSGCOD;
	private String occurMsg;

	public String getMSGCOD() {
		return MSGCOD;
	}

	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}

	public String getOccurMsg() {
		return occurMsg;
	}

	public void setOccurMsg(String occurMsg) {
		this.occurMsg = occurMsg;
	}
}
