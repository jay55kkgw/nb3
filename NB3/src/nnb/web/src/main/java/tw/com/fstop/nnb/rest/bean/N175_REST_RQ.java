package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N175_REST_RQ extends BaseRestBean_FX implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2319998362229051271L;

	private String DATE;

	private String TIME;

	private String SYNC;

	private String PPSYNC;

	private String PINKEY;

	private String CERTACN;

	private String FLAG;

	private String BNKRA;

	private String XMLCA;

	private String XMLCN;

	private String PPSYNCN;

	private String PINNEW; // 交易密碼SHA1值

	private String CUSIDN; // 身分證號

	private String ACN; // 存單帳號

	private String FDPNUM; // 存單號碼

	private String TRMTYP;

	private String TERM;

	private String CUID; // 幣別

	private String AMT; // 存單金額

	private String ITR; // 利率

	private String INTMTH; // 計息方式

	private String DPISDT; // 起存日

	private String DUEDAT; // 到期日

	private String INT; // 利息

	private String TAX; // 所得稅

	private String PAIAFTX; // 稅後本息

	private String NHITAX; // 健保費

	// 電文不需要但ms_tw需要的欄位
	private String FGTRDATE; // 即時或預約

	private String iSeqNo; // iSeqNo

	private String pkcs7Sign; // IKEY

	private String jsondc; // IKEY

	private String UID; // 同CUSIDN

	// 舊BEAN.writeLog需要的欄位
	private String ADOPID;		// 電文代號

	private String DPMYEMAIL; // 本人email

	private String CMTRMAIL; // 另通知email

	private String CMMAILMEMO; // 摘要

	private String CMTRMEMO; // 交易備註

	// 舊BEAN.booking需要的欄位
	private String CMTRDATE; // 預約日期
	
	//IDGATE
	private String sessionID;
	private String idgateID;
	private String txnID;
	
	public String getDPMYEMAIL()
	{
		return DPMYEMAIL;
	}

	public void setDPMYEMAIL(String dPMYEMAIL)
	{
		DPMYEMAIL = dPMYEMAIL;
	}

	public String getCMTRMAIL()
	{
		return CMTRMAIL;
	}

	public void setCMTRMAIL(String cMTRMAIL)
	{
		CMTRMAIL = cMTRMAIL;
	}

	public String getCMMAILMEMO()
	{
		return CMMAILMEMO;
	}

	public void setCMMAILMEMO(String cMMAILMEMO)
	{
		CMMAILMEMO = cMMAILMEMO;
	}

	public String getCMTRMEMO()
	{
		return CMTRMEMO;
	}

	public void setCMTRMEMO(String cMTRMEMO)
	{
		CMTRMEMO = cMTRMEMO;
	}

	public String getDATE()
	{
		return DATE;
	}

	public void setDATE(String dATE)
	{
		DATE = dATE;
	}

	public String getTIME()
	{
		return TIME;
	}

	public void setTIME(String tIME)
	{
		TIME = tIME;
	}

	public String getSYNC()
	{
		return SYNC;
	}

	public void setSYNC(String sYNC)
	{
		SYNC = sYNC;
	}

	public String getPPSYNC()
	{
		return PPSYNC;
	}

	public void setPPSYNC(String pPSYNC)
	{
		PPSYNC = pPSYNC;
	}

	public String getPINKEY()
	{
		return PINKEY;
	}

	public void setPINKEY(String pINKEY)
	{
		PINKEY = pINKEY;
	}

	public String getCERTACN()
	{
		return CERTACN;
	}

	public void setCERTACN(String cERTACN)
	{
		CERTACN = cERTACN;
	}

	public String getFLAG()
	{
		return FLAG;
	}

	public void setFLAG(String fLAG)
	{
		FLAG = fLAG;
	}

	public String getBNKRA()
	{
		return BNKRA;
	}

	public void setBNKRA(String bNKRA)
	{
		BNKRA = bNKRA;
	}

	public String getXMLCA()
	{
		return XMLCA;
	}

	public void setXMLCA(String xMLCA)
	{
		XMLCA = xMLCA;
	}

	public String getXMLCN()
	{
		return XMLCN;
	}

	public void setXMLCN(String xMLCN)
	{
		XMLCN = xMLCN;
	}

	public String getPPSYNCN()
	{
		return PPSYNCN;
	}

	public void setPPSYNCN(String pPSYNCN)
	{
		PPSYNCN = pPSYNCN;
	}

	public String getPINNEW()
	{
		return PINNEW;
	}

	public void setPINNEW(String pINNEW)
	{
		PINNEW = pINNEW;
	}

	public String getCUSIDN()
	{
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN)
	{
		CUSIDN = cUSIDN;
	}

	public String getACN()
	{
		return ACN;
	}

	public void setACN(String aCN)
	{
		ACN = aCN;
	}

	public String getFDPNUM()
	{
		return FDPNUM;
	}

	public void setFDPNUM(String fDPNUM)
	{
		FDPNUM = fDPNUM;
	}

	public String getTRMTYP()
	{
		return TRMTYP;
	}

	public void setTRMTYP(String tRMTYP)
	{
		TRMTYP = tRMTYP;
	}

	public String getTERM()
	{
		return TERM;
	}

	public void setTERM(String tERM)
	{
		TERM = tERM;
	}

	public String getCUID()
	{
		return CUID;
	}

	public void setCUID(String cUID)
	{
		CUID = cUID;
	}

	public String getAMT()
	{
		return AMT;
	}

	public void setAMT(String aMT)
	{
		AMT = aMT;
	}

	public String getITR()
	{
		return ITR;
	}

	public void setITR(String iTR)
	{
		ITR = iTR;
	}

	public String getINTMTH()
	{
		return INTMTH;
	}

	public void setINTMTH(String iNTMTH)
	{
		INTMTH = iNTMTH;
	}

	public String getDPISDT()
	{
		return DPISDT;
	}

	public void setDPISDT(String dPISDT)
	{
		DPISDT = dPISDT;
	}

	public String getDUEDAT()
	{
		return DUEDAT;
	}

	public void setDUEDAT(String dUEDAT)
	{
		DUEDAT = dUEDAT;
	}

	public String getINT()
	{
		return INT;
	}

	public void setINT(String iNT)
	{
		INT = iNT;
	}

	public String getTAX()
	{
		return TAX;
	}

	public void setTAX(String tAX)
	{
		TAX = tAX;
	}

	public String getPAIAFTX()
	{
		return PAIAFTX;
	}

	public void setPAIAFTX(String pAIAFTX)
	{
		PAIAFTX = pAIAFTX;
	}

	public String getNHITAX()
	{
		return NHITAX;
	}

	public void setNHITAX(String nHITAX)
	{
		NHITAX = nHITAX;
	}

	public String getUID()
	{
		return UID;
	}

	public void setUID(String uID)
	{
		UID = uID;
	}

	public String getFGTRDATE()
	{
		return FGTRDATE;
	}

	public void setFGTRDATE(String fGTRDATE)
	{
		FGTRDATE = fGTRDATE;
	}

	public String getiSeqNo()
	{
		return iSeqNo;
	}

	public void setiSeqNo(String iSeqNo)
	{
		this.iSeqNo = iSeqNo;
	}

	public String getPkcs7Sign()
	{
		return pkcs7Sign;
	}

	public void setPkcs7Sign(String pkcs7Sign)
	{
		this.pkcs7Sign = pkcs7Sign;
	}

	public String getJsondc()
	{
		return jsondc;
	}

	public void setJsondc(String jsondc)
	{
		this.jsondc = jsondc;
	}

	public String getADOPID()
	{
		return ADOPID;
	}

	public void setADOPID(String aDOPID)
	{
		ADOPID = aDOPID;
	}

	public String getCMTRDATE()
	{
		return CMTRDATE;
	}

	public void setCMTRDATE(String cMTRDATE)
	{
		CMTRDATE = cMTRDATE;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public String getIdgateID() {
		return idgateID;
	}

	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}

	public String getTxnID() {
		return txnID;
	}

	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}

}
