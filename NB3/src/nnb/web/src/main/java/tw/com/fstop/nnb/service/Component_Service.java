package tw.com.fstop.nnb.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SpringBeanFactory;

@Service
public class Component_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private I18n i18n;
	
	/**
	 * 元件_判斷使用者是否IKEY使用者
	 * 
	 * @param sessionId
	 * @return
	 */
	public BaseResult isIkeyUser(String xmlcod) {
		log.trace("isIkeyUser...");
		log.trace("xmlcod: " + xmlcod);
		
		// 處理結果
		BaseResult bs = null;
		// 使用者是否IKEY使用者
		String iSiKeyUser = "false";
		
		try {
			bs = new BaseResult();
			
			// 判斷使用者是否IKEY使用者
			if(xmlcod != null && !"".equals(xmlcod)){
				
				// N911 帶回 XMLCOD 若為 2 || 4 || 5 || 6 ，則是IKEY使用者
				iSiKeyUser = 
					( "2".equals(xmlcod) || "4".equals(xmlcod) || "5".equals(xmlcod) || "6".equals(xmlcod) ) ? "true" : "false";
				
				// 回傳使用者是否IKEY使用者
				if("true".equals(iSiKeyUser)) {
					bs.setMsgCode("0");
					bs.setMessage(i18n.getMsg("LB.X1814"));//是IKEY使用者
					bs.setResult(iSiKeyUser=="true");
				} else {
					bs.setMsgCode("0");
					bs.setMessage(i18n.getMsg("LB.X1815"));//非IKEY使用者
				}
				
			}
			log.debug("iSiKeyUser: " + iSiKeyUser);
			
		} catch (Exception e) {
			//e.printStackTrace();
			//Avoid Information Exposure Through an Error Message
			log.error("isIkeyUser error >> {}",e); 
		}
		return bs;
	}
	
	/**
	 * 元件_取得各元件的最新版本號
	 * 
	 * @return
	 */
	public BaseResult componentVersion() {
		log.trace("componentVersion...");
		// 處理結果
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			
			// 取得各元件的最新版本號-->存進BaseResult.Data回傳
			LinkedHashMap<String, String> versionMap = SpringBeanFactory.getBean("componentVersion");
			
			// versionMap有無資料
			if( versionMap != null && !versionMap.isEmpty() ) {
				// versionMap有資料
				log.debug("versionMap: " + versionMap);
				bs.setMsgCode("0");
				bs.setMessage(i18n.getMsg("LB.X1816"));//取得各元件的最新版本號
				bs.setData(versionMap);
			}else {
				log.error("can't get component latest version!!!");
				bs.setMessage(i18n.getMsg("LB.X1817"));//無法取得各元件的最新版本號
			}
			
		} catch (Exception e) {
			//e.printStackTrace();
			//Avoid Information Exposure Through an Error Message
			log.error("componentVersion error >> {}",e); 
		}
		return bs;
	}
	
	/**
	 * 元件_取得各元件的最新版本號
	 * 
	 * @return
	 */
	public BaseResult checkIdentity(String cusidn, String acn) {
		log.trace("checkIdentity...");
		
		BaseResult bs = null;
		
		try {
			bs = new BaseResult();
			bs = N953_REST(acn);	
			
			Map<String, Object> bsData = (Map) bs.getData();
			String cusidnN953 = String.valueOf( bsData.get("CUSIDN") );
			
			log.debug(ESAPIUtil.vaildLog("checkIdentity.cusidn >> " + cusidn));
			log.debug("checkIdentity.cusidnN953: {}", cusidnN953);
			
			if(!cusidn.equals(cusidnN953)) {
				bs.setMsgCode(cusidnN953);
				bs.setMessage(i18n.getMsg("LB.X1818"));//非本人帳戶之晶片金融卡
			}
			log.debug("checkIdentity.msgCode: {}", bs.getMsgCode());
			
		} catch (Exception e) {
			//e.printStackTrace();
			//Avoid Information Exposure Through an Error Message
			log.error("checkIdentity error >> {}",e); 
		}
		return bs;
	}
	
	/**
	 * 元件_驗證晶片金融卡是否為本人及是否啟用
	 * 
	 * @return
	 */
	public BaseResult checkIdentityEnable(String cusidn, String acn) {
		log.trace("checkIdentityEnable...");
		
		BaseResult bs = null;
		
		try {
			bs = new BaseResult();
			bs = N953_REST(acn);	
			
			Map<String, Object> bsData = (Map) bs.getData();
			String cusidnN953 = String.valueOf( bsData.get("CUSIDN") );
			String stscodN953 = String.valueOf( bsData.get("STSCOD") );
			
			log.debug(ESAPIUtil.vaildLog("checkIdentityEnable.cusidn >> " + cusidn));
			log.debug("checkIdentityEnable.cusidnN953: {}", cusidnN953);
			
			if(!cusidn.equals(cusidnN953)) {
				bs.setMsgCode(cusidnN953);
				bs.setMessage(i18n.getMsg("LB.X1818"));//非本人帳戶之晶片金融卡
			}
			
			if(!"A".equals(stscodN953)) {
				bs.setMsgCode("1");
				
//				bs.setMessage(i18n.getMsg("LB.Financial_debit_card") + i18n.getMsg("LB.D0322"));// 晶片金融卡未啟用
				
//				Map<String, String> n953stscod = SpringBeanFactory.getBean("N953-STSCOD");
//				String stscode = n953stscod.get(stscodN953);
//				bs.setMessage(stscode); // N953.STSCOD 對應錯誤訊息
				
				bs.setMessage("晶片金融卡狀態異常，不可使用"); // 20210520 by sox
				log.info("checkIdentityEnable.stscodN953: {}", stscodN953);
			}
			
			log.debug("checkIdentityEnable.msgCode: {}", bs.getMsgCode());
			
		} catch (Exception e) {
			//e.printStackTrace();
			//Avoid Information Exposure Through an Error Message
			log.error("checkIdentity error >> {}",e); 
		}
		return bs;
	}
	
	
	public BaseResult ikey_without_login(Map<String,String> requestParam){
		BaseResult bs = Ikeycheck_REST(requestParam);
		return bs;
	}
	
}
