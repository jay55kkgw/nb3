package tw.com.fstop.nnb.spring.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.Monitor_Service;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.SessionUtil;

@Controller
@RequestMapping(value="/MONITOR")
@SessionAttributes({SessionUtil.TOKENID})
public class Monitor_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private Monitor_Service monitor_service;
	

	@RequestMapping(value = "/monitor")
	public String monitor(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.debug("monitor...");
		String target = null;
		BaseResult bs = null;
		String turnMS = "false";
		try {
				bs = new BaseResult();
//				try {
//					bs = monitor_service.chkMobileCenter();		
//				}catch(Exception e) {
//					log.error("monitor error MS connect >> {}",e);
//				}
//				Map<String, Object> bsdata=(Map<String, Object>) bs.getData();
//				log.debug("Monitor Data: " +CodeUtil.toJson(bsdata));
//				model.addAttribute("BS",bsdata);	
//				if(((Monitor_Status)bsdata.get("TW_Status")).getSERVER_STATUS().equals("healthy") && ((Monitor_Status)bsdata.get("PS_Status")).getSERVER_STATUS().equals("healthy") &&
//				((Monitor_Status)bsdata.get("PAY_Status")).getSERVER_STATUS().equals("healthy") && ((Monitor_Status)bsdata.get("OLS_Status")).getSERVER_STATUS().equals("healthy") &&
//				((Monitor_Status)bsdata.get("OLA_Status")).getSERVER_STATUS().equals("healthy") && ((Monitor_Status)bsdata.get("LOAN_Status")).getSERVER_STATUS().equals("healthy") &&
//				((Monitor_Status)bsdata.get("FX_Status")).getSERVER_STATUS().equals("healthy") && ((Monitor_Status)bsdata.get("GOLD_Status")).getSERVER_STATUS().equals("healthy") &&
//				((Monitor_Status)bsdata.get("FUND_Status")).getSERVER_STATUS().equals("healthy") && ((Monitor_Status)bsdata.get("CC_Status")).getSERVER_STATUS().equals("healthy"))turnMS="true";
//				model.addAttribute("turnMS",turnMS);
//				model.addAttribute("turnDB",bsdata.get("turnDB"));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("monitor error >> {}",e);		
		} finally {
			model.addAttribute("SYSTIME",DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
			log.trace(CodeUtil.toJson("model>>"+model));
			target = "/monitor/monitor";							
		}		
		return target;
	}
	
	@PostMapping(value = "/service_checking_ajax", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult downAds(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs=monitor_service.SingleChecking(reqParam, bs);
		} catch (Exception e) {
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * 給F5偵測用
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/check")
	public  String check(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
//		public @ResponseBody String check(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.debug("monitor check...");
		String rt = null;
		String target = null;
		try {
			rt="ok";
			target="/monitor/f5_check";
		} catch (Exception e) {
			log.error("monitor check error >> {}",e);		
		} 		
		return target;
	}
	
	
	/**
	 * 給Client偵測是否還是原使用者
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@PostMapping(value = "/checkUser")
	public @ResponseBody BaseResult checkUser(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {

		BaseResult bs = null;
		try {
			bs = new BaseResult();
			log.debug("monitor checkUser...");

			boolean result = false;
			
			String tokenId = (String) SessionUtil.getAttribute(model, SessionUtil.TOKENID, null);
			
			String userTokenId = reqParam.get("tokenid");
			
			result = userTokenId.equals(tokenId) ? true : false;
			
			if(result) {
				bs.setResult(true);
				bs.setMsgCode("0");
				bs.setMessage("check User OK!!!");
				
			} else {
				bs.setResult(false);
				bs.setMsgCode("1");
				bs.setMessage("check User Fail!!!");
				
			}
			
		} catch (Exception e) {
			log.error("monitor checkUser error >> {}",e);		
		} 		
		return bs;
	}
	
	
	/**
	 * 偵測資料庫連線是否正常
	 * @return
	 */
	@PostMapping(value = "/checkDB")
	public @ResponseBody BaseResult checkDB(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {

		BaseResult bs = null;
		try {
			bs = new BaseResult();
			log.debug("monitor checkDB...");

			boolean result = monitor_service.testDB();
			
			if(result) {
				bs.setResult(true);
				bs.setMsgCode("0");
				bs.setMessage("check DB OK!!!");
				
			} else {
				bs.setResult(false);
				bs.setMsgCode("1");
				bs.setMessage("check DB Fail!!!");
				
			}
			
		} catch (Exception e) {
			log.error("monitor checkDB error >> {}",e);		
		} 		
		return bs;
	}
	
}
