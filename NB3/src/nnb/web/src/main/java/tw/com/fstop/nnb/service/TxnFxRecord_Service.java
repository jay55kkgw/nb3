package tw.com.fstop.nnb.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.po.TXNFXRECORD;
import fstop.orm.po.TXNFXSCHPAY;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.tbb.nnb.dao.TxnFxRecordDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.web.util.StrUtils;

@Service
public class TxnFxRecord_Service {
	private Logger log = LoggerFactory.getLogger(this.getClass().getName());

	@Autowired
	TxnFxRecordDao txnFxRecordDao;

	/**
	 * 查詢外幣轉帳結果
	 * 
	 * @param
	 * @return BaseResult
	 */
	public BaseResult query(Map<String, String> reqParam) {
		BaseResult bs = new BaseResult();
		Boolean flag = Boolean.FALSE;
		String sdate = reqParam.get("sdate");
		String edate = reqParam.get("edate");
		String cusidn = reqParam.get("cusidn");
		try {			
			// 驗證統編為空
			if (StrUtils.isNull(cusidn)) {
				bs.setMessage("cusidn不得null");
				throw new Exception();
			}
			// 驗證日期為空則改六個月內
			if (StrUtils.isNull(sdate) || StrUtils.isNull(edate)) {
				LocalDateTime now = LocalDateTime.now();
				DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE;
				formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
				reqParam.put("edate", now.format(formatter));
				reqParam.put("sdate", now.plusMonths(-6).format(formatter));
			} else {
				// 驗證日期是否正常
				DateTimeFormatter strToDateFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");
				TemporalAccessor sdate_c = LocalDate.parse(reqParam.get("sdate"), strToDateFormatter);
				TemporalAccessor edate_c = LocalDate.parse(reqParam.get("edate"), strToDateFormatter);
			}
			// 驗證ADOPID為空則把Flag改true 表查全部
			if (StrUtils.isNull(reqParam.get("adopid"))) {
				flag = true;
			}
			// 驗證起始日不得大於截止日
			if (Integer.parseInt(sdate) > Integer.parseInt(edate)) {
				bs.setMessage("期間啟日不得大於期間迄日");
				throw new Exception();
			}

			List<TXNFXRECORD> list = txnFxRecordDao.findAllByDateRange(reqParam, flag);

			bs.setData(list);
			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", "查詢成功");
		} catch (DateTimeParseException e) {
			log.error("{}", e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMsgCode("2");
			bs.setMessage("日期輸入錯誤");
		} catch (Exception e) {
			log.error("{}", e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMsgCode("1");
			if (bs.getMessage() == null) {
				bs.setMessage("查詢失敗");
			}
		}
		return bs;
	}

	/**
	 * 新增外幣轉帳結果
	 * 
	 * @param
	 * @return BaseResult
	 */
	public BaseResult add(Map<String, String> reqParam1) {
		BaseResult bs = new BaseResult();
		Map<String, String> reqParam=new HashMap<String, String>();
		Map reqmap = new HashMap();
		List<String> list = new ArrayList<String>();
		List<String> list2 = new ArrayList<String>();
		
		//排除可能給null的欄位 之後如果null給空字串 
		List<String> exlist = new ArrayList<String>();
		
		if(StrUtils.isNull(reqParam1.get("logintype"))) {
			reqParam1.put("logintype", "MB");
		}
		
		exlist.add("fxsvbh");
		exlist.add("fxtxmemo");
		exlist.add("fxtxmails");
		exlist.add("fxtxmailmemo");
		exlist.add("fxtotainfo");		
		exlist.add("fxremail");
		exlist.add("fxtelfee");
		exlist.add("fxourchg");
		exlist.add("fxexcode");		
		exlist.add("fxsvamt");
		exlist.add("fxwdamt");
		exlist.add("fxcert");
		exlist.add("fxefeeccy");
		exlist.add("fxefee");
		exlist.add("fxexrate");
		exlist.add("fxmsgseqno");
		exlist.add("fxtxcode");
		exlist.add("fxtitainfo");
		exlist.add("fxmsgcontent");		
		
		try {
			list = StrUtils.getBeanName(TXNFXRECORD.class);
			//如果帶進來的參數不在POJO內 就remove 
			for(String str: reqParam1.keySet()) {
				if(list.contains(str.toUpperCase())) {
					reqParam.put(str,reqParam1.get(str));
				}
			}
			//把JPA欄位抓出來轉小寫配合RQ
			for (String str : list) {
				list2.add(str.toLowerCase());
			}
			
			//移除可能給null的欄位 之後另外處理 
			for(String str : exlist) {
				list2.remove(str);
			}		
			//數字另外移除
			list2.remove("fxschid");
					
			// 剩下的欄位如果Null 空白字串 空字串丟出Exception
			for (String key : list2) {
				if (StrUtils.isNull(reqParam.get(key) )) {
					bs.setMessage(key + "不得為空");
					throw new Exception();
				}				
			}
			
			//如果為null則給空字串
			for(String str: exlist) {
				if (reqParam.get(str) == null) {
					reqParam.put(str, "");
				}
			}		
			
			//數字為空則強制帶0
			if(StrUtils.isNull(reqParam.get("fxschid"))) {
				reqParam.put("fxschid","0");
			}
					
			//二擇一 兩者沒有值就擋掉
			if (StrUtils.isNull(reqParam.get("fxsvamt")) && StrUtils.isNull(reqParam.get("fxwdamt")) ) {
				bs.setMessage("fxsvamt 和 fxwdamt不得同時沒有值");
				throw new Exception();				
			}			

			// 配合POJO轉大寫做新增
			for (Object obj : reqParam.keySet()) {
				reqmap.put(obj.toString().toUpperCase(), reqParam.get(obj.toString())==null?"":reqParam.get(obj.toString()));
			}
			TXNFXRECORD txnfxrecord = CodeUtil.objectCovert(TXNFXRECORD.class, reqmap);
			txnFxRecordDao.save(txnfxrecord);
			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", "新增成功");
		} catch (Exception e) {
			log.error("{}", e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMsgCode("1");
			if (bs.getMessage() == null) {
				bs.setMessage("新增失敗");
			}
		}
		return bs;
	}
}
