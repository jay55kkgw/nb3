package tw.com.fstop.nnb.aop;

import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import tw.com.fstop.nnb.rest.bean.BaseResultSerialization;
import tw.com.fstop.nnb.service.Login_out_Service;
import tw.com.fstop.nnb.service.Personalize_Service;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;

@Aspect
@Component
public class MenuAop {

	static Logger log = LoggerFactory.getLogger(MenuAop.class);
	
	@Autowired
	private HttpServletRequest req; 
	@Autowired
	private HttpServletResponse resp; 
	@Autowired
	private Login_out_Service login_out_service;
	@Autowired
	private Personalize_Service personalize_service;
	// 忽略網址的列表
	private List<String> urlList;

	@PostConstruct
    public void init() {
    	log.trace("MenuAop.init()");
    	
		// 忽略網址的列表
		urlList = Arrays.asList("/login","/login/sitemap","/login_aj","/logout_aj","/checkLogin_aj","/logoutforce_aj","/reLogin",
								"/doublepw","/doublepw_result","/expiredpw","/lateralter","/useoldpw","/pw_alter_in","/pw_alter","/ssl_pw_alter",
								"/username_reset","/username_reset_result","/lock_reset","/lock_reset_confirm","/lock_reset_result",
								"/timeout_logout","/bulletin_aj","/downAds");
    }
    
	/**
	 * 
	 */
	@After("within(tw.com.fstop.nnb.spring.controller..*)")
	public void doAfterReturning() {
//		init();
    	HttpSession session = req.getSession();
    	
    	String funcUri = "";
    	String contextPath = "";
    	String pathInfo = "";
    	
    	Map<String,String> urlKeyMap = null;
    	
    	try {
	        log.trace("Adding Menu Headers ........................");

			// request.uri, e.x: /nnb/login
			funcUri = req.getRequestURI();
			// contextPath, e.x: /nnb
			contextPath = req.getContextPath();
			// servletPath, e.x: /login
//			String pathInfo = req.getPathInfo(); // getPathInfo在較新的Server上會為null，故改用getRequestURI去掉getContextPath
			pathInfo = funcUri.indexOf(contextPath)==0 ? funcUri.replaceFirst(contextPath, "") : funcUri;
			
			// 不在白名單中
			if( !pathInfo.startsWith("/css/") && !pathInfo.startsWith("/js/") 
					&& !pathInfo.startsWith("/img/") && !pathInfo.startsWith("/fonts/")
					&& !pathInfo.startsWith("/errorCloss") && !pathInfo.startsWith("/login")
					&& !pathInfo.startsWith("/keyboard") && !pathInfo.startsWith("/CAPCODE/")
					&& !pathInfo.startsWith("/webservice") && !pathInfo.startsWith("/MB")
					&& !pathInfo.startsWith("/ebillApply/")
			) {
				if (!urlList.contains(pathInfo)) {
					
					// 依據權限取得MenuList
					String authority = String.valueOf(session.getAttribute(SessionUtil.AUTHORITY));
			        log.trace("Now auth type is : {}",authority);
					if(authority.equals("ALL")) {
						BaseResultSerialization bs = login_out_service.getMenuList("ALL");
						urlKeyMap = (Map<String,String>)((Map<String,Object>)bs.getData()).get("urlKeyMap");
						req.setAttribute("menuFilterList", bs);
					}else if(authority.equals("ATM")) {
						BaseResultSerialization bs = login_out_service.getMenuList("ATM");
						urlKeyMap = (Map<String,String>)((Map<String,Object>)bs.getData()).get("urlKeyMap");
						req.setAttribute("menuFilterList", bs);
					}else if(authority.equals("ATMT")) {
						BaseResultSerialization bs = login_out_service.getMenuList("ATMT");
						urlKeyMap = (Map<String,String>)((Map<String,Object>)bs.getData()).get("urlKeyMap");
						req.setAttribute("menuFilterList", bs);
					}else if(authority.equals("CRD")) {
						BaseResultSerialization bs = login_out_service.getMenuList("CRD");
						urlKeyMap = (Map<String,String>)((Map<String,Object>)bs.getData()).get("urlKeyMap");
						req.setAttribute("menuFilterList", bs);
					}else {
						req.setAttribute("menuFilterList", login_out_service.getMenuList("null"));
					}
				}
			}
			
    	} catch (Exception e) {
			log.error("",e);
			req.setAttribute("menuFilterList", login_out_service.getMenuList("null"));
		}
    	
    	

		// 使用者我的最愛功能: 若url與MenuList相同則加入Cookie，供前端加入我的最愛使用
    	try {
//			List<String> urlList = personalize_service.getUrlList();
//			List adopidlist = personalize_service.getAdopidList();
			
			log.trace("MenuAop.urlList: {}",ESAPIUtil.vaildLog(urlList.toString()));
			log.trace("MenuAop.pathInfo:{}",ESAPIUtil.vaildLog(pathInfo));
			log.trace(ESAPIUtil.vaildLog("MenuAop.menuStamp: " + pathInfo));
			
			if (urlKeyMap != null) {
				log.trace("MenuAop.adopidlist:{}",ESAPIUtil.vaildLog(urlKeyMap.toString()));
				if(urlKeyMap.containsKey(pathInfo)) {
					String adopid = urlKeyMap.get(pathInfo);
					session.setAttribute(SessionUtil.ADOPID, adopid);
					log.trace("MenuAop.session.adopid:{}",ESAPIUtil.vaildLog((String)session.getAttribute(SessionUtil.ADOPID)));
					// 顯示我的最愛的星星
					String cusidn = (String) req.getSession().getAttribute(SessionUtil.CUSIDN);
					cusidn = StrUtil.isNotEmpty(cusidn) ?new String( Base64.getDecoder().decode(cusidn) ,"utf-8"):cusidn;
					boolean result = personalize_service.isFavorite(cusidn, adopid);
					req.setAttribute("showStar", result);
					
					// 快速選單中的常用交易，調整成顯示該用戶最近三個月內使用次數最多的前五名交易功能
//					personalize_service.SaveUsuallyList(cusidn, adopid); // 改為批次從TXNLOG去排行並重整ADMHOTOP
				}
			}
			
    	} catch (Exception e) {
			log.error("{}", e);
		}
    	
	}
}
