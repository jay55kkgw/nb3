package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;


public class N177_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{


	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */

	
	@SerializedName(value = "ACN")
	private String ACN;
	@SerializedName(value = "FDPNO")
	private String FDPNO;
	@SerializedName(value = "CUID")
	private String CUID;
	@SerializedName(value = "display_BALANCE")
	private String display_BALANCE;
	@SerializedName(value = "DEPTYPE")	
	private String DEPTYPE;
	
	@SerializedName(value = "INTMTH_view")
	private String INTMTH_view;
	@SerializedName(value = "SHOWAUTXFTM")
	private String SHOWAUTXFTM;
	@SerializedName(value = "CODENAME")
	private String CODENAME;
	
	@Override
	public Map<String, String> coverMap(){
		LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", "外幣定存自動轉期申請/變更");
		result.put("交易類型", "-");
		result.put("存單帳號", this.ACN);
		result.put("存單號碼", this.FDPNO);
		result.put("存單金額", this.CUID+" "+this.display_BALANCE+"元");
		result.put("存單種類", this.DEPTYPE);
		result.put("計息方式", this.INTMTH_view);
		result.put("轉期次數", this.SHOWAUTXFTM);
		result.put("轉存方式", this.CODENAME);
		return result;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	public String getFDPNO() {
		return FDPNO;
	}

	public void setFDPNO(String fDPNO) {
		FDPNO = fDPNO;
	}

	public String getCUID() {
		return CUID;
	}

	public void setCUID(String cUID) {
		CUID = cUID;
	}

	public String getDisplay_BALANCE() {
		return display_BALANCE;
	}

	public void setDisplay_BALANCE(String display_BALANCE) {
		this.display_BALANCE = display_BALANCE;
	}

	public String getDEPTYPE() {
		return DEPTYPE;
	}

	public void setDEPTYPE(String dEPTYPE) {
		DEPTYPE = dEPTYPE;
	}

	public String getINTMTH_view() {
		return INTMTH_view;
	}

	public void setINTMTH_view(String iNTMTH_view) {
		INTMTH_view = iNTMTH_view;
	}

	public String getSHOWAUTXFTM() {
		return SHOWAUTXFTM;
	}

	public void setSHOWAUTXFTM(String sHOWAUTXFTM) {
		SHOWAUTXFTM = sHOWAUTXFTM;
	}

	public String getCODENAME() {
		return CODENAME;
	}

	public void setCODENAME(String cODENAME) {
		CODENAME = cODENAME;
	}


}
