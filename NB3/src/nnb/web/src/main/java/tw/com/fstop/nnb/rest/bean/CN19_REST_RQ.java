package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class CN19_REST_RQ extends BaseRestBean_OLA implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5582843138752966045L;

	private String TRANNAME;// TRANNAME
	private String HEADER;	// HEADER
	private String DATE;	// 日期YYYMMDD
	private String TIME;	// 時間HHMMSS
	private String PPSYNC;	// P.P.Key Sync.Check Item
	private String HTRANSPIN;	// 交易密碼
	private String CUSIDN;	// 統一編號
	private String PPSYNCN;	// P.P.Key Sync.Check Item 16
	private String TRANSEQ;	// 交易序號
	private String ISSUER;	// 晶片卡發卡行庫
	private String ACNNO;	// 晶片卡帳號
	private String ICDTTM;	// 晶片卡日期時間
	private String ICSEQ;	// 晶片卡序號
	private String ICMEMO;	// 晶片卡備註欄
	private String TAC_Length;// TAC DATA Length
	private String TAC;		// TAC DATA
	private String TAC_120space;// TAC DATA Space
	private String TRMID;	// 端末設備查核碼
	
	// 電文不需要但ms_tw需要的欄位
	private String FGTXWAY;	// 交易機制
	private String iSeqNo;	// iSeqNo
	private String FGTXDATE;// 即時或預約
	private String TRANSPASSUPDATE;// 即時或預約
	private String pkcs7Sign;// IKEY
	private String jsondc;// IKEY

	
	// 舊BEAN.writeLog需要的欄位
	private String UID;		// 身分證字號
	private String IP;
	private String CN19VER;// 
	public String getTRANNAME() {
		return TRANNAME;
	}
	public void setTRANNAME(String tRANNAME) {
		TRANNAME = tRANNAME;
	}
	public String getHEADER() {
		return HEADER;
	}
	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getPPSYNC() {
		return PPSYNC;
	}
	public void setPPSYNC(String pPSYNC) {
		PPSYNC = pPSYNC;
	}
	public String getHTRANSPIN() {
		return HTRANSPIN;
	}
	public void setHTRANSPIN(String hTRANSPIN) {
		HTRANSPIN = hTRANSPIN;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getPPSYNCN() {
		return PPSYNCN;
	}
	public void setPPSYNCN(String pPSYNCN) {
		PPSYNCN = pPSYNCN;
	}
	public String getTRANSEQ() {
		return TRANSEQ;
	}
	public void setTRANSEQ(String tRANSEQ) {
		TRANSEQ = tRANSEQ;
	}
	public String getISSUER() {
		return ISSUER;
	}
	public void setISSUER(String iSSUER) {
		ISSUER = iSSUER;
	}
	public String getACNNO() {
		return ACNNO;
	}
	public void setACNNO(String aCNNO) {
		ACNNO = aCNNO;
	}
	public String getICDTTM() {
		return ICDTTM;
	}
	public void setICDTTM(String iCDTTM) {
		ICDTTM = iCDTTM;
	}
	public String getICSEQ() {
		return ICSEQ;
	}
	public void setICSEQ(String iCSEQ) {
		ICSEQ = iCSEQ;
	}
	public String getICMEMO() {
		return ICMEMO;
	}
	public void setICMEMO(String iCMEMO) {
		ICMEMO = iCMEMO;
	}
	public String getTAC_Length() {
		return TAC_Length;
	}
	public void setTAC_Length(String tAC_Length) {
		TAC_Length = tAC_Length;
	}
	public String getTAC() {
		return TAC;
	}
	public void setTAC(String tAC) {
		TAC = tAC;
	}
	public String getTAC_120space() {
		return TAC_120space;
	}
	public void setTAC_120space(String tAC_120space) {
		TAC_120space = tAC_120space;
	}
	public String getTRMID() {
		return TRMID;
	}
	public void setTRMID(String tRMID) {
		TRMID = tRMID;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getiSeqNo() {
		return iSeqNo;
	}
	public void setiSeqNo(String iSeqNo) {
		this.iSeqNo = iSeqNo;
	}
	public String getFGTXDATE() {
		return FGTXDATE;
	}
	public void setFGTXDATE(String fGTXDATE) {
		FGTXDATE = fGTXDATE;
	}
	public String getTRANSPASSUPDATE() {
		return TRANSPASSUPDATE;
	}
	public void setTRANSPASSUPDATE(String tRANSPASSUPDATE) {
		TRANSPASSUPDATE = tRANSPASSUPDATE;
	}
	public String getPkcs7Sign() {
		return pkcs7Sign;
	}
	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}
	public String getJsondc() {
		return jsondc;
	}
	public void setJsondc(String jsondc) {
		this.jsondc = jsondc;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	public String getCN19VER() {
		return CN19VER;
	}
	public void setCN19VER(String cN19VER) {
		CN19VER = cN19VER;
	}
	
	
	
}
