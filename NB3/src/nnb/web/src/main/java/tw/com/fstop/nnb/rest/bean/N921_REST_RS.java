package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N921_REST_RS extends BaseRestBean implements Serializable  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -730128893542549966L;

    private String HEADER;//HEADER

    private String NAME;//姓名

    private String MMACOD;//回應代碼

    private String OFFSET;//空白
    
    private String COUNT;//筆數
    
  //電文內陣列
  	private LinkedList<N921_REST_RSDATA> REC;

    public LinkedList<N921_REST_RSDATA> getREC() {
		return REC;
	}

	public void setREC(LinkedList<N921_REST_RSDATA> rEC) {
		REC = rEC;
	}

	public String getHEADER() {
		return HEADER;
	}

	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}

	public String getNAME() {
		return NAME;
	}

	public void setNAME(String nAME) {
		NAME = nAME;
	}

	public String getMMACOD() {
		return MMACOD;
	}

	public void setMMACOD(String mMACOD) {
		MMACOD = mMACOD;
	}

	public String getOFFSET() {
		return OFFSET;
	}

	public void setOFFSET(String oFFSET) {
		OFFSET = oFFSET;
	}

	public String getCOUNT() {
		return COUNT;
	}

	public void setCOUNT(String cOUNT) {
		COUNT = cOUNT;
	}

    
	
	

}
