package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * C012電文RSDATA
 */
public class C012_REST_RSDATA implements Serializable{
	private static final long serialVersionUID = 7127169312136862734L;
	
	//資訊
	private String REFVALUE;
	private String FXRATE;
	private String FCYRAT;
	private String DIFAMT;
	private String LCYRAT;
	private String CRY;
	private String FUNDLNAME;
	private String ACUCOUNT;
	private String NET01;
	private String TOTAMT;
	private String DIFAMT2;
	private String AC225;
	private String TRANSCODE;
	private String AC202;
	private String AMT;
	private String RTNRATE;
	private String MIP;
	private String CDNO;
	private String REFUNDAMT;
	private String REFMARK;
	private String TRANSCRY;
	
	public String getREFVALUE(){
		return REFVALUE;
	}
	public void setREFVALUE(String rEFVALUE){
		REFVALUE = rEFVALUE;
	}
	public String getFXRATE(){
		return FXRATE;
	}
	public void setFXRATE(String fXRATE){
		FXRATE = fXRATE;
	}
	public String getFCYRAT(){
		return FCYRAT;
	}
	public void setFCYRAT(String fCYRAT){
		FCYRAT = fCYRAT;
	}
	public String getDIFAMT(){
		return DIFAMT;
	}
	public void setDIFAMT(String dIFAMT){
		DIFAMT = dIFAMT;
	}
	public String getLCYRAT(){
		return LCYRAT;
	}
	public void setLCYRAT(String lCYRAT){
		LCYRAT = lCYRAT;
	}
	public String getCRY(){
		return CRY;
	}
	public void setCRY(String cRY){
		CRY = cRY;
	}
	public String getFUNDLNAME(){
		return FUNDLNAME;
	}
	public void setFUNDLNAME(String fUNDLNAME){
		FUNDLNAME = fUNDLNAME;
	}
	public String getACUCOUNT(){
		return ACUCOUNT;
	}
	public void setACUCOUNT(String aCUCOUNT){
		ACUCOUNT = aCUCOUNT;
	}
	public String getNET01(){
		return NET01;
	}
	public void setNET01(String nET01){
		NET01 = nET01;
	}
	public String getTOTAMT(){
		return TOTAMT;
	}
	public void setTOTAMT(String tOTAMT){
		TOTAMT = tOTAMT;
	}
	public String getDIFAMT2(){
		return DIFAMT2;
	}
	public void setDIFAMT2(String dIFAMT2){
		DIFAMT2 = dIFAMT2;
	}
	public String getAC225(){
		return AC225;
	}
	public void setAC225(String aC225){
		AC225 = aC225;
	}
	public String getTRANSCODE(){
		return TRANSCODE;
	}
	public void setTRANSCODE(String tRANSCODE){
		TRANSCODE = tRANSCODE;
	}
	public String getAC202(){
		return AC202;
	}
	public void setAC202(String aC202){
		AC202 = aC202;
	}
	public String getAMT(){
		return AMT;
	}
	public void setAMT(String aMT){
		AMT = aMT;
	}
	public String getRTNRATE(){
		return RTNRATE;
	}
	public void setRTNRATE(String rTNRATE){
		RTNRATE = rTNRATE;
	}
	public String getMIP(){
		return MIP;
	}
	public void setMIP(String mIP){
		MIP = mIP;
	}
	public String getREFUNDAMT(){
		return REFUNDAMT;
	}
	public void setREFUNDAMT(String rEFUNDAMT){
		REFUNDAMT = rEFUNDAMT;
	}
	public String getREFMARK(){
		return REFMARK;
	}
	public void setREFMARK(String rEFMARK){
		REFMARK = rEFMARK;
	}
	public String getTRANSCRY(){
		return TRANSCRY;
	}
	public void setTRANSCRY(String tRANSCRY){
		TRANSCRY = tRANSCRY;
	}
	public String getCDNO() {
		return CDNO;
	}
	public void setCDNO(String cDNO) {
		CDNO = cDNO;
	}
}