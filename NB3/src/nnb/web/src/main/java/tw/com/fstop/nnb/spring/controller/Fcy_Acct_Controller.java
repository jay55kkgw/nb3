package tw.com.fstop.nnb.spring.controller;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.aop.Dialog;
import tw.com.fstop.nnb.service.Fcy_Acct_Service;
import tw.com.fstop.nnb.service.OldExcel_Download_Service;
import tw.com.fstop.nnb.service.Txt_Download_Service;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.DownloadUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({ SessionUtil.PD, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, SessionUtil.CONFIRM_LOCALE_DATA,
		SessionUtil.RESULT_LOCALE_DATA, SessionUtil.PRINT_DATALISTMAP_DATA, SessionUtil.CUSIDN})
@Controller
@RequestMapping(value = "/FCY/ACCT")
public class Fcy_Acct_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Fcy_Acct_Service fcy_acct_service;
	@Autowired
	private OldExcel_Download_Service oldexcel_download_service;
	@Autowired
	private Txt_Download_Service txt_download_service;
	
	@Autowired
	private I18n i18n;

	// 外匯餘額查詢
	@RequestMapping(value = "/f_balance_query")
	public String balance_query(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("fcy_balance_query");
		try {
			bs = new BaseResult();
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.debug("cusidn" + cusidn);
			bs = fcy_acct_service.balance_query(cusidn, null);

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("balance_query error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/acct_fcy/f_balance_query";
				model.addAttribute("fcy_balance_query", bs);
				List<List<Map<String, String>>> printList = new ArrayList<>();
				List<Map<String, String>> dataListMap = ((List<Map<String, String>>) ((Map<String, Object>) bs
						.getData()).get("ACNInfo"));
				List<Map<String, String>> dataListMap2 = ((List<Map<String, String>>) ((Map<String, Object>) bs
						.getData()).get("AVASum"));
				List<Map<String, String>> dataListMap3 = ((List<Map<String, String>>) ((Map<String, Object>) bs
						.getData()).get("BALSum"));
				printList.add(dataListMap);
				printList.add(dataListMap2);
				printList.add(dataListMap3);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, printList);
				// TODO: model.addAttribute("COUNT",String.valueOf(.size()));
			} else {
				String previous = "/INDEX/index";
				bs.setPrevious(previous);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 外匯活期存款明細輸入頁
	@RequestMapping(value = "/f_demand_deposit_detail")
	public String demand_deposit_detail(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		String urlId = okMap.get("ACN");
		BaseResult bs = null;
		log.trace("f_demand_deposit_detail Controller");
		// 清除切換語系時暫存的資料
		SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		try {
			bs = new BaseResult();
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.debug("cusidn" + cusidn);
			bs = fcy_acct_service.demand_deposit_detail(cusidn);
			bs.addData("TODAY", DateUtil.getCurentDateTime("yyyy/MM/dd"));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("demand_deposit_detail error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				bs.addData("urlID", urlId);
				target = "/acct_fcy/f_demand_deposit_detail";
				model.addAttribute("demand_deposit_detail", bs);
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 外匯活期存款明細結果頁
	@Dialog(ADOPID = "N520")
	@RequestMapping(value = "/f_demand_deposit_detail_result")
	public String demand_deposit_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("f_demand_deposit_detail_result");
		try {
			bs = new BaseResult();
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			// 解決 Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			
			boolean ckDate = false;
			if("CMPERIOD".equals(okMap.get("FGPERIOD"))) {
				//判斷查詢區間是否在限定範圍
				ckDate = DateUtil.checkDate(okMap.get("CMSDATE"), okMap.get("CMEDATE"), 2);
			}
			else {
				ckDate = true;
			}
			
			if(ckDate) {
				if (hasLocale) {
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
					if (!bs.getResult()) {
						log.trace("bs.getResult() >>{}", bs.getResult());
						throw new Exception();
					}
				}
				if (!hasLocale) {
					bs.reset();
					bs = fcy_acct_service.demand_deposit_result(cusidn, okMap);
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
			}
			else {
				bs.setResult(false);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("demand_deposit_result error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/acct_fcy/f_demand_deposit_detail_result";
				Map<String, Object> dataMap = (Map) bs.getData();
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("labelList");
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, rowListMap);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rowListMap);
				model.addAttribute("demand_deposit_result", bs);
			} else {
				//回上一頁 
				bs.setPrevious("/FCY/ACCT/f_demand_deposit_detail");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 外匯活期存款明細輸入頁直接下載
	@RequestMapping(value = "/f_demand_deposit_detail_directDownload")
	public String demand_deposit_detail_directDownload(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("f_demand_deposit_detail_ajaxDirectDownload");
		String target = "/error";
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.debug("cusidn {} ", cusidn);
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			bs = fcy_acct_service.fxDepositDetaildirectDownload(cusidn, okMap);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("demand_deposit_detail_directDownload error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "forward:/directDownload";
				Map<String, Object> dataMap = (Map) bs.getData();
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("labelList");
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rowListMap);
				Map<String, Object> parameterMap = (Map<String, Object>) dataMap.get("parameterMap");
				request.setAttribute("parameterMap", parameterMap);
			} else {
				//回上一頁 
				bs.setPrevious("/FCY/ACCT/f_demand_deposit_detail");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 外匯定存明細
	@RequestMapping(value = "/f_time_deposit_details")
	public String f_time_deposit_details(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("f_time_deposit_details satrt");
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.debug("f_time_deposit_details cusidn {}" ,cusidn);
			bs = fcy_acct_service.f_time_deposit_details(cusidn, null);
		} catch (Exception e) {
			log.debug("f_time_deposit_details error {}" ,e);
		} finally {
			if (bs != null && bs.getResult()) 
			{
				target = "/acct_fcy/f_time_deposit_details";
				List<Map<String, String>> dataListMap = ((List<Map<String, String>>) ((Map<String, Object>) bs
						.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("f_time_deposit_details", bs);
			} else {
				// 新增回上一頁的路徑
				bs.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/*
	 * 特別對下載檔案處理
	 */
	@RequestMapping("/f_time_deposit_details_download")
	@SuppressWarnings("unchecked")
	public ModelAndView f_time_deposit_details_download(
			@RequestParam(required = false) Map<String, Object> parameterMap, Model model) {
		log.debug("IN f_time_deposit_details_download");
		log.debug(ESAPIUtil.vaildLog("parameterMap={}" + CodeUtil.toJson(parameterMap)));

		Map<String, Object> okParameterMap = ESAPIUtil.validMap(parameterMap);
		log.debug(ESAPIUtil.vaildLog("okParameterMap={}" + CodeUtil.toJson(okParameterMap)));

		String DATATOTMAT = (String) okParameterMap.get("DATATOTMAT");
		log.debug(ESAPIUtil.vaildLog("DATATOTMAT={}" + DATATOTMAT));

		String[] DATATOTMATARRAY = DATATOTMAT.replace("[", "").replace("]", "").trim().split(", ");
		log.debug("DATATOTMATARRAY={}" + DATATOTMATARRAY);

		String downloadType = (String) okParameterMap.get("downloadType");
		log.debug(ESAPIUtil.vaildLog("downloadType={}"+ downloadType));

		List<Map<String, Object>> dataListMap = (List<Map<String, Object>>) SessionUtil.getAttribute(model,
				SessionUtil.DOWNLOAD_DATALISTMAP_DATA, null);
		log.debug("dataListMap={}", dataListMap);
		okParameterMap.put("dataListMap", dataListMap);

		// 舊版本EXCEL下載
		if ("OLDEXCEL".equals(downloadType)) {
			String fullDATATOTMAT = "";
			for (int x = 0; x < DATATOTMATARRAY.length; x++) {
				fullDATATOTMAT += DATATOTMATARRAY[x] + "\n";
			}
			log.debug("fullDATATOTMAT={}" + fullDATATOTMAT);
			okParameterMap.put("DATATOTMAT", fullDATATOTMAT);

			return new ModelAndView(oldexcel_download_service, okParameterMap);
		}
		// TXT下載
		else {
			String fullDATATOTMAT = "";
			for (int x = 0; x < DATATOTMATARRAY.length; x++) {
				if (x > 0) {
					fullDATATOTMAT += "          " + DATATOTMATARRAY[x] + "\r\n";
				} else {
					fullDATATOTMAT += DATATOTMATARRAY[x] + "\r\n";
				}
			}
			log.debug("fullDATATOTMAT={}" + fullDATATOTMAT);
			okParameterMap.put("DATATOTMAT", fullDATATOTMAT);

			return new ModelAndView(txt_download_service, okParameterMap);
		}
	}

	// 信用狀通知查詢進入頁
	@RequestMapping(value = "/f_credit_letter_query")
	public String credit_letter_query(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("f_credit_letter_query start~~~");
		try {
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			bs = new BaseResult();
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.debug("cusidn: {} ", cusidn);
			bs.addData("TODAY",DateUtil.getDate("/"));
			bs.setResult(true);
			log.debug("BSDATA : {} ", CodeUtil.toJson(bs));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("credit_letter_query error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/acct_fcy/f_credit_letter_query";
				//修改 Cleartext Submission of Sensitive Information
				model.addAttribute("cd_letter_query", bs);

			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 信用狀通知查詢結果頁
	@RequestMapping(value = "/f_credit_letter_query_result")
	public String credit_letter_query_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("f_letter_of_credit_query_result start~~~");
		try {
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			boolean hasLocale = okMap.containsKey("locale");
			boolean back = "Y".equals(okMap.get("back"));
			log.debug(ESAPIUtil.vaildLog("previous = {}" +okMap.get("previous")));
			//判斷查詢區間是否在限定範圍
			boolean ckDate = DateUtil.checkDate(okMap.get("FDATE"), okMap.get("TDATE"), 12);
			if(ckDate) {
				if (hasLocale) {
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
					log.trace("bs.getResult>> {}", bs.getResult());
				} else {
					bs = new BaseResult();
					String cusidn = new String(
							Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
					log.debug(ESAPIUtil.vaildLog("reqParam{}"+CodeUtil.toJson(reqParam)));
					log.debug("cusidn" + cusidn);
					bs = fcy_acct_service.credit_letter_query_result(cusidn, reqParam);
					
				}		
			}
			else {
				bs = new BaseResult();
				bs.setResult(false);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("credit_letter_query_result error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/acct_fcy/f_credit_letter_query_result";
				log.debug("bs_get_data={}", bs.getData());
				List<Map<String, String>> dataListMap = ((List<Map<String, String>>) ((Map<String, Object>) bs
						.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				
				//i18n資料---在這裡還有查詢頁直接下載做 才能切語系後也吃的到(也可以放到try裡面判斷語系的外面)
				Map<String,Object>i18ndata=(Map<String, Object>) bs.getData();
				if(StrUtil.isEmpty((String) i18ndata.get("LCNO")))i18ndata.put("LCNO_JSP",i18n.getMsg("LB.All"));
				if(StrUtil.isEmpty((String) i18ndata.get("REFNO")))i18ndata.put("REFNO_JSP",i18n.getMsg("LB.All"));
				//開狀日期修狀日期
				if(i18ndata.get("QKIND").equals("1")) {
					bs.addData("QKIND_JSP","LB.W0089");
					bs.addData("QKIND_TXT",i18n.getMsg("LB.W0089"));
					}
				else if(i18ndata.get("QKIND").equals("2")){
					bs.addData("QKIND_JSP","LB.X1533");
					bs.addData("QKIND_TXT",i18n.getMsg("LB.X1533"));
					}
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, bs.getData());
				model.addAttribute("credit_letter_query_result", bs);
			} else {
				bs.setPrevious("/FCY/ACCT/f_credit_letter_query");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 信用狀通知查詢輸入頁直接下載
	@RequestMapping(value = "/f_credit_letter_query_ajaxDirectDownload")
	public String credit_letter_query_directDownload(HttpServletRequest request, HttpServletResponse response,
			@RequestBody(required = false) String serializeString, Model model) {
		log.trace("f_credit_letter_query_ajaxDirectDownload");
		String target = "/error";
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.debug("cusidn {} ", cusidn);
			// 序列化傳入值
			Map<String, String> formMap = DownloadUtil.serializeStringToMap(serializeString);
			
			//修改 Potential O Reflected XSS All Clients
			Map<String, String> okMap = ESAPIUtil.validStrMap(formMap);
			
			log.debug(ESAPIUtil.vaildLog("formMap={}" + CodeUtil.toJson(okMap)));
			for (Entry<String, String> entry : okMap.entrySet()) {
				okMap.put(entry.getKey(), URLDecoder.decode(entry.getValue(), "UTF-8"));
			}
			bs = fcy_acct_service.CreditLetterdirectDownload(cusidn, okMap);

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("credit_letter_query_directDownload error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "forward:/directDownload";
				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap={}", dataMap);
				//i18n資料
				Map<String,Object>i18ndata=(Map<String, Object>) bs.getData();
				if(StrUtil.isEmpty((String) i18ndata.get("LCNO")))i18ndata.put("LCNO_JSP",i18n.getMsg("LB.All"));
				if(StrUtil.isEmpty((String) i18ndata.get("REFNO")))i18ndata.put("REFNO_JSP",i18n.getMsg("LB.All"));
				//開狀日期修狀日期
				if(i18ndata.get("QKIND").equals("1")) {
					bs.addData("QKIND_JSP","LB.W0089");
					bs.addData("QKIND_TXT",i18n.getMsg("LB.W0089"));
					}
				else if(i18ndata.get("QKIND").equals("2")){
					bs.addData("QKIND_JSP","LB.X1533");
					bs.addData("QKIND_TXT",i18n.getMsg("LB.X1533"));
					}
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
				log.trace("rowListMap={}", rowListMap);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rowListMap);
				Map<String, Object> parameterMap = (Map<String, Object>) dataMap.get("parameterMap");
				request.setAttribute("parameterMap", parameterMap);
			} else {
				bs.setPrevious("/FCY/ACCT/f_credit_letter_query");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	// 信用狀通知查詢結果頁下載
	@RequestMapping(value = "/f_credit_letter_query_SessionDownload")
	public String f_credit_letter_query_SessionDownload(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("f_credit_letter_query_SessionDownload");
		Map<String, String> okMap = null;
		String target = "/error";
		BaseResult bs = null;
		try {
			okMap = ESAPIUtil.validStrMap(reqParam);
			bs = new BaseResult();
			bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("f_credit_letter_query_SessionDownload error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "forward:/directDownload";
				Map<String, Object> dataMap = (Map) bs.getData();
				//解決 Potential O Reflected XSS All Clients
				Map<String, Object> dataOkMap = ESAPIUtil.validMap(dataMap);
				log.trace("dataMap={}", dataOkMap);
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataOkMap.get("REC");
				log.trace("rowListMap={}", rowListMap);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rowListMap);
				Map<String, Object> prMap = new HashMap<String, Object>();
				prMap.putAll(okMap);
				prMap.put("sBAL_excel", dataOkMap.get("sBAL_excel"));
				prMap.put("sBAL_txt", dataOkMap.get("sBAL_txt"));
				request.setAttribute("parameterMap", prMap);
				log.debug("rowListMap={}", rowListMap);
			} else {
				bs.setPrevious("/FCY/ACCT/f_credit_letter_query");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 信用狀開狀查詢
	@RequestMapping(value = "/f_credit_open_query")
	public String letter_of_credit_opening_inquiry(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/acct_fcy/f_credit_open_query";
		return target;
	}

	// 當日信用狀開狀查詢(N551)
	@RequestMapping(value = "/f_credit_open_query_date")
	public String letter_of_credit_opening_inquiry_date(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
        model.addAttribute("time_now", DateUtil.getDate("/") + " " + DateUtil.getTheTime(":"));
		String target = "/acct_fcy/f_credit_open_query_date";
		return target;
	}

	// 當日信用狀開狀查詢結果(N551)
	@RequestMapping(value = "/f_credit_open_query_date_result")
	public String letter_of_credit_opening_inquiry_date_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("f_credit_open_query_date_result start~~");
		try {
			bs = new BaseResult();
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+ CodeUtil.toJson(reqParam)));
			log.debug(ESAPIUtil.vaildLog("cusidn >> {}"+ cusidn));

			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			boolean back = "Y".equals(okMap.get("back"));
			log.debug(ESAPIUtil.vaildLog("previous = {}"+ okMap.get("previous")));
			//判斷查詢區間是否在限定範圍
			boolean ckDate = DateUtil.checkDate(okMap.get("CMSDATE"), okMap.get("CMEDATE"), 12);
			if(ckDate) {
				if (hasLocale || back || okMap.get("previous") != null) {
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
					bs.setResult(true);
					log.trace("bs.getResult>> {}", bs.getResult());
					// if (!bs.getResult()) {
					// log.trace("bs.getResult() >>{}", bs.getResult());
					// throw new Exception();
					// }
				} else {
					bs.reset();
					bs = fcy_acct_service.letter_of_credit_opening_inquiry_date_result(cusidn, okMap);
					SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
					log.trace("bs.getResult>> {}", bs.getResult());
				}
				log.debug("bs>>{}", bs);
				// bs =
				// fcy_acct_service.letter_of_credit_opening_inquiry_date_result(cusidn,reqParam);
			}
			else {
				bs.setResult(false);
			}

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("letter_of_credit_opening_inquiry_date_result error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/acct_fcy/f_credit_open_query_date_result";
				log.debug("bs_get_data={}", bs.getData());
				List<Map<String, String>> dataListMap = ((List<Map<String, String>>) ((Map<String, Object>) bs
						.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);

				model.addAttribute("base_result", bs);
			} else {
				// 新增回上一頁的路徑
				bs.setPrevious("/FCY/ACCT/f_credit_open_query_date");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 當日信用狀開狀查詢結果直接下載(N551)
	@RequestMapping(value = "/f_credit_open_query_date_ajaxDirectDownload")
	public String letter_of_credit_opening_inquiry_date_ajaxDirectDownload(HttpServletRequest request,
			HttpServletResponse response, @RequestBody(required = false) String serializeString, Model model) {
		log.trace("letter_of_credit_opening_inquiry_guarantee_ajaxDirectDownload start~~");
		log.trace(ESAPIUtil.vaildLog("serializeString={}"+serializeString));
		String target = "/error";
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// Get session value by session Key
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			// 序列化傳入值
			Map<String, String> formMap = DownloadUtil.serializeStringToMap(serializeString);
			//修改 Potential O Reflected XSS All Clients
			Map<String, String> okMap = ESAPIUtil.validStrMap(formMap);
			log.debug(ESAPIUtil.vaildLog("formMap={}"+ CodeUtil.toJson(okMap)));
			for (Entry<String, String> entry : okMap.entrySet()) {
				okMap.put(entry.getKey(), URLDecoder.decode(entry.getValue(), "UTF-8"));
			}
			bs = fcy_acct_service.letter_of_credit_opening_inquiry_date_ajaxDirectDownload(cusidn, okMap);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("letter_of_credit_opening_inquiry_date_ajaxDirectDownload error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "forward:/directDownload";
				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap={}", dataMap);
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
				log.debug("rowListMap={}", rowListMap);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rowListMap);
				Map<String, Object> parameterMap = (Map<String, Object>) dataMap.get("parameterMap");
				request.setAttribute("parameterMap", parameterMap);
			} else {
				bs.setPrevious("/FCY/ACCT/f_credit_open_query_date");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 當日信用狀開狀查詢結果_進口到單明細(N555)
	@RequestMapping(value = "/f_credit_open_query_import_detail")
	public String letter_of_credit_opening_inquiry_import_detail(HttpServletRequest request,
			HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+ CodeUtil.toJson(reqParam)));
			log.debug(ESAPIUtil.vaildLog("cusidn >> {}"+cusidn));
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				// bs.reset();
				bs = fcy_acct_service.letter_of_credit_opening_inquiry_import_detail(cusidn, okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("letter_of_credit_opening_inquiry_import_detail error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/acct_fcy/f_credit_open_query_import_detail_result";
				log.debug("bs_get_data={}", bs.getData());
				List<Map<String, String>> dataListMap = ((List<Map<String, String>>) ((Map<String, Object>) bs
						.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("base_result", bs);
			} else {
				// 新增回上一頁的路徑
				bs.setPrevious("/FCY/ACCT/f_credit_open_query_date_result");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 當日信用狀開狀查詢結果_明細查詢明細(N556)
	@RequestMapping(value = "/f_credit_open_query_commerce_detail")
	public String letter_of_credit_opening_inquiry_commerce_detail(HttpServletRequest request,
			HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.debug(ESAPIUtil.vaildLog("reqParam >> {}"+ CodeUtil.toJson(reqParam)));
			log.debug(ESAPIUtil.vaildLog("cusidn >> {}"+ cusidn));
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				bs.reset();
				bs = fcy_acct_service.letter_of_credit_opening_inquiry_commerce_detail(cusidn, okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			// bs
			// =fcy_acct_service.letter_of_credit_opening_inquiry_commerce_detail(cusidn,reqParam);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("letter_of_credit_opening_inquiry_commerce_detail error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/acct_fcy/f_credit_open_query_import_commerce_result";
				// log.debug("bs_get_data={}",bs.getData());
				// List<Map<String,String>> dataListMap =
				// ((List<Map<String,String>>)((Map<String,Object>)bs.getData()).get("REC"));
				// log.debug("dataListMap={}",dataListMap);
				// SessionUtil.addAttribute(model,SessionUtil.PRINT_DATALISTMAP_DATA,dataListMap);
				model.addAttribute("base_result", bs);
			} else {
				// 新增回上一頁的路徑
				bs.setPrevious("/FCY/ACCT/f_credit_open_query_date_result");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 擔保信用狀/保證函開狀查詢(N557)
	@RequestMapping(value = "/f_credit_open_query_guarantee")
	public String letter_of_credit_opening_inquiry_guarantee(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		model.addAttribute("time_now", DateUtil.getDate("/") + " " + DateUtil.getTheTime(":"));
		String target = "/acct_fcy/f_credit_open_query_guarantee";
		return target;
	}

	// 擔保信用狀/保證函開狀查詢結果(N557)
	@RequestMapping(value = "/f_credit_open_query_guarantee_result")
	public String letter_of_credit_opening_inquiry_guarantee_result(HttpServletRequest request,
			HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("letter_of_credit_opening_inquiry_guarantee_result start~~");
		try {
			bs = new BaseResult();
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.debug(ESAPIUtil.vaildLog("reqParam >> {}" + CodeUtil.toJson(reqParam)));
			log.debug(ESAPIUtil.vaildLog("cusidn >> {}" + cusidn));

			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			//判斷查詢區間是否在限定範圍
			boolean ckDate = DateUtil.checkDate(okMap.get("CMSDATE"), okMap.get("CMEDATE"), 12);
			if(ckDate) {
				if (hasLocale) {
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
					if (!bs.getResult()) {
						log.trace("bs.getResult() >>{}", bs.getResult());
						throw new Exception();
					}
				}
				if (!hasLocale) {
					bs.reset();
					bs = fcy_acct_service.letter_of_credit_opening_inquiry_guarantee_result(cusidn, okMap);
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
			}
			else {
				bs.setResult(false);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("letter_of_credit_opening_inquiry_guarantee_result error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/acct_fcy/f_credit_open_query_guarantee_result";
				log.debug("bs_get_data={}", bs.getData());
				List<Map<String, String>> dataListMap = ((List<Map<String, String>>) ((Map<String, Object>) bs
						.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);

				model.addAttribute("base_result", bs);
			} else {
				bs.setPrevious("/FCY/ACCT/f_credit_open_query_guarantee");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 擔保信用狀/保證函開狀查詢直接下載(N557)
	@RequestMapping(value = "/f_credit_open_query_guarantee_ajaxDirectDownload")
	public String letter_of_credit_opening_inquiry_guarantee_ajaxDirectDownload(HttpServletRequest request,
			HttpServletResponse response, @RequestBody(required = false) String serializeString, Model model) {
		log.trace("f_credit_open_query_guarantee_ajaxDirectDownload start~~");
		log.trace(ESAPIUtil.vaildLog("serializeString={}" + serializeString));
		String target = "/error";
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// Get session value by session Key
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			// 序列化傳入值
			Map<String, String> formMap = DownloadUtil.serializeStringToMap(serializeString);
			//修改 Potential O Reflected XSS All Clients
			Map<String, String> okMap = ESAPIUtil.validStrMap(formMap);
			log.debug(ESAPIUtil.vaildLog("formMap={}" + CodeUtil.toJson(okMap)));
			for (Entry<String, String> entry : okMap.entrySet()) {
				okMap.put(entry.getKey(), URLDecoder.decode(entry.getValue(), "UTF-8"));
			}
			bs = fcy_acct_service.letter_of_credit_opening_inquiry_guarantee_ajaxDirectDownload(cusidn, okMap);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("letter_of_credit_opening_inquiry_guarantee_ajaxDirectDownload error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "forward:/directDownload";
				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap={}", dataMap);
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
				log.debug("rowListMap={}", rowListMap);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rowListMap);
				Map<String, Object> parameterMap = (Map<String, Object>) dataMap.get("parameterMap");
				request.setAttribute("parameterMap", parameterMap);
			} else {
				bs.setPrevious("/FCY/ACCT/f_credit_open_query_guarantee");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 外匯匯入匯款查詢
	@RequestMapping(value = "/f_inward_remittance")
	public String inward_remittance(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("f_inward_remittance>>");
		String cusidn = new String(
				Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
		try {
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			bs = fcy_acct_service.N920_REST(cusidn, Fcy_Acct_Service.INWARD_REMITTANC);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("inward_remittance error >> {}",e);
		} finally {
			target = "/acct_fcy/f_inward_remittance";
			model.addAttribute("username_alter_result", bs);
		}
		model.addAttribute("time_now", DateUtil.getDate("/") + " " + DateUtil.getTheTime(":"));
		return target;
	}

	// 外匯匯入匯款查詢結果
	@RequestMapping(value = "/f_inward_remittance_result")
	public String inward_remittance_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("f_inward_remittance_result>>");
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		String cusidn = new String(
				Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
		try {
			bs = new BaseResult();
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			//判斷查詢區間是否在限定範圍
			boolean ckDate = DateUtil.checkDate(okMap.get("CMSDATE"), okMap.get("CMEDATE"), 12);
			if(ckDate) {
				if (hasLocale) {
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
					if (!bs.getResult()) {
						log.trace("bs.getResult() >>{}", bs.getResult());
						throw new Exception();
					}
				}
				if (!hasLocale) {
					bs.reset();
					bs = fcy_acct_service.inward_remittance_result(cusidn, okMap);
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
				// bs = fcy_acct_service.inward_remittance_result(cusidn,reqParam);
			}
			else {
				bs.setResult(false);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("inward_remittance_result error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/acct_fcy/f_inward_remittance_result";
				List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs
						.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("inward_remittance_result", bs);
			} else {
				// 新增回上一頁的路徑
				bs.setPrevious("/FCY/ACCT/f_inward_remittance");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 外匯匯入匯款查詢直接下載
	@RequestMapping(value = "/f_inward_remittance_ajaxDirectDownload")
	public String inward_remittance_ajaxDirectDownload(HttpServletRequest request, HttpServletResponse response,
			@RequestBody(required = false) String serializeString, Model model) {
		log.trace("f_inward_remittance_ajaxDirectDownload start~~");
		log.trace(ESAPIUtil.vaildLog("serializeString={}" + serializeString));
		String target = "/error";
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// Get session value by session Key
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			// 序列化傳入值
			Map<String, String> formMap = DownloadUtil.serializeStringToMap(serializeString);
			//修改 Potential O Reflected XSS All Clients
			Map<String, String> okMap = ESAPIUtil.validStrMap(formMap);
			log.debug(ESAPIUtil.vaildLog("formMap={}" + CodeUtil.toJson(okMap)));
			for (Entry<String, String> entry : okMap.entrySet()) {
				okMap.put(entry.getKey(), URLDecoder.decode(entry.getValue(), "UTF-8"));
			}
			bs = fcy_acct_service.inward_remittance_ajaxDirectDownload(cusidn, okMap);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("inward_remittance_ajaxDirectDownload error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "forward:/directDownload";
				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap={}", dataMap);
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
				log.debug("rowListMap={}", rowListMap);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rowListMap);
				Map<String, Object> parameterMap = (Map<String, Object>) dataMap.get("parameterMap");
				request.setAttribute("parameterMap", parameterMap);
			} else {
//				String errorCode = "";
//				String errorMessage = "";
//
//				try {
//					errorCode = URLEncoder.encode(bs.getMsgCode(), "UTF-8");
//					errorMessage = URLEncoder.encode(bs.getMessage(), "UTF-8");
//				} catch (Exception e) {
//					e.printStackTrace();
//					log.error("", e);
//				}
//				response.addHeader("errorCode", errorCode);
//				response.addHeader("errorMessage", errorMessage);
				bs.setPrevious("/FCY/ACCT/f_inward_remittance");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 匯出匯款查詢
	@RequestMapping(value = "/f_outward_remittances_q")
	public String outward_remittances(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = new BaseResult();
		log.trace("f_outward_remittances");

		try {
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			bs.addData("TODAY",DateUtil.getDate("/"));
			// Get put page value
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("outward_remittances error >> {}",e);
		} finally {
			model.addAttribute("outward_remittances", bs);
			target = "/acct_fcy/f_outward_remittances_q";
		}
		return target;
	}

	// 匯出匯款查詢結果頁
	@RequestMapping(value = "/f_outward_remittances_q_result")
	public String outward_remittances_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("f_outward_remittances_q_result");
		try {
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			//判斷查詢區間是否在限定範圍
			boolean ckDate = DateUtil.checkDate(okMap.get("CMSDATE"), okMap.get("CMEDATE"), 12);
			if(ckDate) {
				if (hasLocale) {
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				} else {
					bs = new BaseResult();
					String cusidn = new String(
							Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
					bs = fcy_acct_service.outward_remittances_result(cusidn, reqParam);
					log.trace("bs.getResult>> {}", bs.getResult());
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
			}
			else {
				bs = new BaseResult();
				bs.setResult(false);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("outward_remittances_result error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				// 列印下載功能用
				List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("REC"));
				log.trace("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, bs.getData());
				target = "/acct_fcy/f_outward_remittances_q_result";
				model.addAttribute("outward_remittances_result", bs);
			} else {
				bs.setPrevious("/FCY/ACCT/f_outward_remittances_q");
				model.addAttribute(BaseResult.ERROR, bs);

			}
		}
		return target;
	}

	// 匯出匯款查詢直接下載
	@RequestMapping(value = "/f_outward_remittances_q_ajaxDirectDownload")
	public String outward_remittances_ajaxDirectDownload(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("f_outward_remittances_q_ajaxDirectDownload start~~");
		String target = "/error";
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// Get session value by session Key
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			// 序列化傳入值
			Map<String, String> formMap = ESAPIUtil.validStrMap(reqParam);
			bs = fcy_acct_service.outward_remittances_ajaxDirectDownload(cusidn, formMap);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("outward_remittances_ajaxDirectDownload error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "forward:/directDownload";
				Map<String, Object> dataMap = (Map) bs.getData();
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rowListMap);
				Map<String, Object> parameterMap = (Map<String, Object>) dataMap.get("parameterMap");
				request.setAttribute("parameterMap", parameterMap);
			} else {
				bs.setPrevious("/FCY/ACCT/f_outward_remittances_q");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	// 匯出匯款查詢結果頁下載
		@RequestMapping(value = "/f_outward_remittances_q_SessionDownload")
		public String f_outward_remittances_q_SessionDownload(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			log.trace("f_outward_remittances_q_SessionDownload start~~");
			String target = "/error";
			BaseResult bs = null;
			try {
				bs = new BaseResult();
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				
				log.debug("BS DATA"+CodeUtil.toJson(bs));
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("f_outward_remittances_q_SessionDownload error >> {}",e);
			} finally {
				if (bs != null && bs.getResult()) {
					target = "forward:/directDownload";
					Map<String, Object> dataMap = (Map) bs.getData();
					List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
					SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rowListMap);
					Map<String, Object> parameterMap = new HashMap<>();
					//修改 Potential O Reflected XSS All Clients
					parameterMap.putAll(ESAPIUtil.validStrMap(reqParam));
					parameterMap.put("CURRANCY_PRINT_EXCEL", dataMap.get("CURRANCY_PRINT_EXCEL"));
					parameterMap.put("CURRANCY_PRINT_TXT", dataMap.get("CURRANCY_PRINT_TXT"));
					request.setAttribute("parameterMap", parameterMap);
				} else {
					bs.setPrevious("/FCY/ACCT/f_outward_remittances_q");
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}
			return target;
		}
	// 進口到單查詢
	@RequestMapping(value = "/f_import_order_query")
	public String import_order_query(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("f_import_order_query start~~~");
		try {
			bs = new BaseResult();
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.debug("cusidn: {} ", cusidn);
			bs.addData("TODAY",DateUtil.getDate("/"));
			bs.setResult(true);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("import_order_query error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/acct_fcy/f_import_order_query";
				model.addAttribute("import_order_query", bs);
				log.debug("bs: {} ", bs);
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 進口到單查詢結果
	@RequestMapping(value = "/f_import_order_query_result")
	public String import_order_query_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("f_import_order_query_result start~~~");
		try {

			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			//判斷查詢區間是否在限定範圍
			boolean ckDate = DateUtil.checkDate(okMap.get("FDATE"), okMap.get("TDATE"), 12);
			if(ckDate) {
				if (hasLocale) {
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				} else {
					bs = new BaseResult();
					String cusidn = new String(
							Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
					log.debug(ESAPIUtil.vaildLog("reqParam{}" + CodeUtil.toJson(reqParam)));
					log.debug(ESAPIUtil.vaildLog("cusidn" + cusidn));
					bs.setResult(true);
					bs = fcy_acct_service.import_order_query_result(cusidn, reqParam);
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
				//i18n資料---在這裡還有查詢頁直接下載做 才能切語系後也吃的到(也可以放到try裡面判斷語系的外面)
				//LCNO頁面顯示 orgLCNO值
				Map<String,Object>i18ndata=(Map<String, Object>) bs.getData();
				if(StrUtil.isEmpty((String) i18ndata.get("orgLCNO")))i18ndata.put("LCNO",i18n.getMsg("LB.All"));
			}
			else {
				bs = new BaseResult();
				bs.setResult(false);
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("import_order_query_result error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/acct_fcy/f_import_order_query_result";
				log.debug("bs_get_data={}", bs.getData());
				List<Map<String, String>> dataListMap = ((List<Map<String, String>>) ((Map<String, Object>) bs
						.getData()).get("REC"));
				log.debug("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				model.addAttribute("import_order_query_result", bs);
			} else {
				bs.setPrevious("/FCY/ACCT/f_import_order_query");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 進口到單查詢直接下載
	@RequestMapping(value = "/f_import_order_query_ajaxDirectDownload")
	public String import_order_query_directDownload(HttpServletRequest request, HttpServletResponse response,
			@RequestBody(required = false) String serializeString, Model model) {
		log.trace("f_import_order_query_ajaxDirectDownload");
		String target = "/error";
		BaseResult bs = null;

		try {
			bs = new BaseResult();
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.debug("cusidn {} ", cusidn);
			// 序列化傳入值
			Map<String, String> formMap = DownloadUtil.serializeStringToMap(serializeString);
			//修改 Potential O Reflected XSS All Clients
			Map<String, String> okMap = ESAPIUtil.validStrMap(formMap);
			log.debug(ESAPIUtil.vaildLog("formMap={}" + CodeUtil.toJson(okMap)));
			for (Entry<String, String> entry : okMap.entrySet()) {
				okMap.put(entry.getKey(), URLDecoder.decode(entry.getValue(), "UTF-8"));
			}
			bs = fcy_acct_service.ImportOrderdirectDownload(cusidn, okMap);
			//i18n資料---在這裡還有查詢頁直接下載做 才能切語系後也吃的到(也可以放到try裡面判斷語系的外面)
			//LCNO頁面顯示 orgLCNO值
			Map<String,Object>i18ndata=(Map<String, Object>) bs.getData();
			if(StrUtil.isEmpty((String) i18ndata.get("orgLCNO")))i18ndata.put("LCNO",i18n.getMsg("LB.All"));
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("import_order_query_directDownload error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "forward:/directDownload";
				Map<String, Object> dataMap = (Map) bs.getData();
				log.trace("dataMap={}", dataMap);
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
				log.trace("rowListMap={}", rowListMap);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rowListMap);
				Map<String, Object> parameterMap = (Map<String, Object>) dataMap.get("parameterMap");
				request.setAttribute("parameterMap", parameterMap);
			} else {
				bs.setPrevious("/FCY/ACCT/f_import_order_query");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 出口押匯查詢輸入頁
	@RequestMapping(value = "/f_export_bill_query")
	public String export_bill_query(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("f_export_bill_query");

		try {
			bs = new BaseResult();
			bs.setResult(true);
			bs.addData("TODAY",DateUtil.getCurentDateTime("yyyy/MM/dd"));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("export_bill_query error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/acct_fcy/f_export_bill_query";
				// 清除切換語系時暫存的資料
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
				model.addAttribute("export_bill_query", bs);
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 出口押匯查詢結果頁
	@RequestMapping(value = "/f_export_bill_query_result")
	public String export_bill_query_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		List<Map<String, String>> downloadMap = new ArrayList();
		String downloadStringTXT = "";
		String downloadStringEXCEL = "";
		log.trace("f_export_bill_query_result");
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			//判斷查詢區間是否在限定範圍
			boolean ckDate = DateUtil.checkDate(okMap.get("CMSDATE"), okMap.get("CMEDATE"), 12);
			if(ckDate) {
				if (hasLocale) {
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
					if (!bs.getResult()) {
						// TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
						log.trace("bs.getResult(): {}", bs.getResult());
						throw new Exception();
					} else {
						// session 資料放入 map 讓後續 model 和回上一頁取值
						okMap.putAll((Map<String, String>) bs.getData());
					}
				} else {
					bs = new BaseResult();
					String cusidn = new String(
							Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
					log.debug(ESAPIUtil.vaildLog("okMap{}" + CodeUtil.toJson(okMap)));
					log.debug(ESAPIUtil.vaildLog("cusidn" + cusidn));
					bs = fcy_acct_service.export_bill_query_result(cusidn, okMap);
					log.trace(" f_export_bill_query_result {}", bs);
					
				}
			}
			else {
				bs = new BaseResult();
				bs.setResult(false);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("export_bill_query_result error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/acct_fcy/f_export_bill_query_result";
				log.debug("bs_get_data={}", bs.getData());
				downloadMap = ((List<Map<String, String>>) ((Map<String, Object>) bs.getData()).get("CRY"));
				for (Map crydata : downloadMap) {
					downloadStringTXT += crydata.get("AMTRBILLCCY") + " " + crydata.get("FXTOTAMT") + " i18n{LB.W0158}"
							+ crydata.get("FXTOTAMTRECNUM") + "i18n{LB.Rows}"+"\r\n";
					downloadStringEXCEL += crydata.get("AMTRBILLCCY") + " " + crydata.get("FXTOTAMT") + " i18n{LB.W0158}"
							+ crydata.get("FXTOTAMTRECNUM") + "i18n{LB.Rows}"+"\n";
				}
				bs.addData("DownloadStringTXT", downloadStringTXT.trim());
				bs.addData("DownloadStringEXCEL", downloadStringEXCEL.trim());
				model.addAttribute("export_bill_query_result", bs);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				
				List<List<Map<String, String>>> dataList = new ArrayList<>();
				List<List<Map<String, String>>> dataList2 = new ArrayList<>();
				List<Map<String, String>> dataListMap = new ArrayList<>();
				List<Map<String, String>> dataListMap2 = new ArrayList<>();
				dataListMap = ((List<Map<String, String>>) ((Map<String, Object>) bs.getData()).get("REC"));
				dataListMap2 = ((List<Map<String, String>>) ((Map<String, Object>) bs.getData()).get("CRY"));

				dataList.add(dataListMap);
				dataList.add(dataListMap2);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataList);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, dataListMap);

			} else {
				bs.setPrevious("/FCY/ACCT/f_export_bill_query");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 出口押匯查詢輸入頁直接下載
	@RequestMapping(value = "/f_export_bill_query_directDownload")
	public String export_bill_query_directDownload(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("f_export_bill_query_DirectDownload");
		String target = "/error";
		String previous = "/FCY/ACCT/f_export_bill_query";
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			log.debug("cusidn {} ", cusidn);
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			for(Entry<String,String> entry : okMap.entrySet()){
				okMap.put(entry.getKey(),URLDecoder.decode(entry.getValue(),"UTF-8"));
			}
			bs = fcy_acct_service.fxExportBillQuerydirectDownload(cusidn, okMap);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("export_bill_query_directDownload error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "forward:/directDownload";
				Map<String, Object> dataMap = (Map) bs.getData();

				log.trace("dataMap={}", dataMap);
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
				log.trace("rowListMap={}", rowListMap);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rowListMap);
				Map<String, Object> parameterMap = (Map<String, Object>) dataMap.get("parameterMap");
				request.setAttribute("parameterMap", parameterMap);
			} else {
				bs.setPrevious(previous);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 光票託收查詢輸入頁
	@RequestMapping(value = "/f_clean_collection")
	public String clean_collection(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("f_clean_collection");
		try {
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			// Get put page value
			bs = new BaseResult();
			bs.addData("TODAY", DateUtil.getCurentDateTime("yyyy/MM/dd"));
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("clean_collection error >> {}",e);
		} finally {
			model.addAttribute("clean_collection", bs);
			target = "/acct_fcy/f_clean_collection";
		}
		return target;
	}

	//// 光票託收查詢結果頁
	@RequestMapping(value = "/f_clean_collection_result")
	public String clean_collection_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("clean_collection_result");
		try {
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			//判斷查詢區間是否在限定範圍
			boolean ckDate = DateUtil.checkDate(okMap.get("CMSDATE"), okMap.get("CMEDATE"), 12);
			if(ckDate) {
				if (hasLocale) {
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				} else {
					bs = new BaseResult();
					String cusidn = new String(
							Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
					bs = fcy_acct_service.clean_collection_result(cusidn, reqParam);
					log.trace("bs.getData>> {}", bs.getData());
					log.trace("bs.getResult>> {}", bs.getResult());
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
					SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				}
			}
			else {
				bs = new BaseResult();
				bs.setResult(false);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("clean_collection_result error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				// 列印下載功能用
				List<Map<String, Object>> dataListMap = ((List<Map<String, Object>>) ((Map<String, Object>) bs
						.getData()).get("REC"));
				log.trace("dataListMap={}", dataListMap);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataListMap);
				target = "/acct_fcy/f_clean_collection_result";
				model.addAttribute("clean_collection_result", bs);
				
				for(Map<String,Object>rowlist:dataListMap) {
					//下載 託收、入帳、退匯
					if(rowlist.get("RSTATE").equals("A")) {
						rowlist.put("RSTATE_D", I18n.getMessage("LB.X0148"));
					}
					else if(rowlist.get("RSTATE").equals("S")) {
						rowlist.put("RSTATE_D", I18n.getMessage("LB.X0149"));
					}
					else if(rowlist.get("RSTATE").equals("C")) {
						rowlist.put("RSTATE_D", I18n.getMessage("LB.X0150"));
					}
					else {
						rowlist.put("RSTATE_D", rowlist.get("RSTATE"));
					}
				}
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, dataListMap);
			} else {
				bs.setPrevious("/FCY/ACCT/f_clean_collection");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}


//	//// 光票託收查詢直接下載
//	@RequestMapping(value = "/f_clean_collection_ajaxDirectDownload")
//	public String clean_collection_ajaxDirectDownload(HttpServletRequest request, HttpServletResponse response,
//			@RequestBody(required = false) String serializeString, Model model) {
//		log.trace("clean_collection_ajaxDirectDownload");
//		log.trace("serializeString={}", serializeString);
//		String target = "/error";
//		BaseResult bs = null;
//		try {
//			bs = new BaseResult();
//			// Get session value by session Key
//			String cusidn = new String(
//					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
//			// 序列化傳入值
//			Map<String, String> formMap = DownloadUtil.serializeStringToMap(serializeString);
//			log.debug("formMap={}" + formMap);
//			for (Entry<String, String> entry : formMap.entrySet()) {
//				formMap.put(entry.getKey(), URLDecoder.decode(entry.getValue(), "UTF-8"));
//			}
//			bs = fcy_acct_service.clean_collection_ajaxDirectDownload(cusidn, formMap);
//		} catch (Exception e) {
//			// TODO: handle exception
//			e.printStackTrace();
//			log.error("f_clean_collection_ajaxDirectDownload", e);
//		} finally {
//			if (bs != null && bs.getResult()) {
//				target = "forward:/ajaxDirectDownload";
//				Map<String, Object> dataMap = (Map) bs.getData();
//				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
//				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rowListMap);
//				Map<String, Object> parameterMap = (Map<String, Object>) dataMap.get("parameterMap");
//				request.setAttribute("parameterMap", parameterMap);
//			} else {
//				String errorCode = "";
//				String errorMessage = "";
//				try {
//					errorCode = URLEncoder.encode(bs.getMsgCode(), "UTF-8");
//					errorMessage = URLEncoder.encode(bs.getMessage(), "UTF-8");
//				} catch (Exception e) {
//					e.printStackTrace();
//					log.error("", e);
//				}
//				response.addHeader("errorCode", errorCode);
//				response.addHeader("errorMessage", errorMessage);
//			}
//		}
//		return target;
//	}
	
	//// 光票託收查詢直接下載
	@RequestMapping(value = "/f_clean_collection_directDownload")
	public String clean_collection_ajaxDirectDownload(HttpServletRequest request, HttpServletResponse response,
			@RequestBody(required = false) String serializeString, Model model) {
		log.trace("f_clean_collection_directDownload");
		log.trace(ESAPIUtil.vaildLog("serializeString={}"+serializeString));
		String target = "/error";
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// Get session value by session Key
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			// 序列化傳入值
			Map<String, String> formMap = DownloadUtil.serializeStringToMap(serializeString);
			//修改 Potential O Reflected XSS All Clients
			Map<String, String> okMap = ESAPIUtil.validStrMap(formMap);
			log.debug(ESAPIUtil.vaildLog("formMap={}" + CodeUtil.toJson(okMap)));
			for (Entry<String, String> entry : okMap.entrySet()) {
				okMap.put(entry.getKey(), URLDecoder.decode(entry.getValue(), "UTF-8"));
			}
			bs = fcy_acct_service.f_clean_collection_directDownload(cusidn, okMap);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("clean_collection_ajaxDirectDownload error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "forward:/directDownload";
				Map<String, Object> dataMap = (Map) bs.getData();
				List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
				for(Map<String,String>rowlist:rowListMap) {
					//下載 託收、入帳、退匯
					if(rowlist.get("RSTATE").equals("A")) {
						rowlist.put("RSTATE_D", I18n.getMessage("LB.X0148"));
					}
					else if(rowlist.get("RSTATE").equals("S")) {
						rowlist.put("RSTATE_D", I18n.getMessage("LB.X0149"));
					}
					else if(rowlist.get("RSTATE").equals("C")) {
						rowlist.put("RSTATE_D", I18n.getMessage("LB.X0150"));
					}
					else {
						rowlist.put("RSTATE_D", rowlist.get("RSTATE"));
					}
				}
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rowListMap);
				Map<String, Object> parameterMap = (Map<String, Object>) dataMap.get("parameterMap");
				request.setAttribute("parameterMap", parameterMap);
			} else {
				bs.setPrevious("/FCY/ACCT/f_clean_collection");
				model.addAttribute(BaseResult.ERROR, bs);
//				String errorCode = "";
//				String errorMessage = "";
//				try {
//					errorCode = URLEncoder.encode(bs.getMsgCode(), "UTF-8");
//					errorMessage = URLEncoder.encode(bs.getMessage(), "UTF-8");
//				} catch (Exception e) {
//					e.printStackTrace();
//					log.error("", e);
//				}
//				response.addHeader("errorCode", errorCode);
//				response.addHeader("errorMessage", errorMessage);
			}
		}
		return target;
	}


	// 進出口託收查詢 選單頁
	@RequestMapping(value = "/f_collection_query")
	public String f_collection_query_menu(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("f_collection_query_menu");
		try {
			// 登入時的身分證字號
			String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if (!"".equals(cusidn) && cusidn != null) {
				// 解密成明碼
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("cusidn: " + cusidn);
			} else {
				log.error("session no cusidn!!!");
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("f_collection_query_menu error >> {}",e);
		} finally {
			target = "/acct_fcy/f_collection_query_menu";
		}
		return target;
	}

	// 進出口託收查詢 導頁
	@RequestMapping(value = "/f_collection_query_type")
	public String f_collection_query_type(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("f_collection_query_type");
		log.trace(ESAPIUtil.vaildLog("reqParam >>{}" + CodeUtil.toJson(reqParam)));
		BaseResult bs = null;
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			if (!StrUtil.isEmpty(okMap.get("FUNC"))) {
				okMap.put("FGMSGCODE", okMap.get("FUNC"));
			}
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if (!bs.getResult()) {
					// TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				} else {
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			} else {
				bs = new BaseResult();
				// 登入時的身分證字號
				String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
				// session有無CUSIDN
				if (!"".equals(cusidn) && cusidn != null) {
					// 解密成明碼
					cusidn = new String(Base64.getDecoder().decode(cusidn));
					log.trace("cusidn: " + cusidn);
				} else {
					log.error("session no cusidn!!!");
				}
				bs.addData("TODAY", DateUtil.getCurentDateTime("yyyy/MM/dd"));
				bs.addData("FGMSGCODE", okMap.get("FGMSGCODE"));
				bs.setResult(true);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
			}

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("f_collection_query_type error >> {}",e);
		} finally {
			switch (((Map<String, String>) bs.getData()).get("FGMSGCODE")) {
			case "N559":
				target = "/acct_fcy/f_collection_query_N559";
				model.addAttribute("SYSDATE", bs);
				break;
			case "N560":
				target = "/acct_fcy/f_collection_query_N560";
				model.addAttribute("SYSDATE", bs);
				break;
			case "N563":
				target = "/acct_fcy/f_collection_query_N563";
				model.addAttribute("SYSDATE", bs);
				break;
			}

		}
		return target;
	}

	// 進出口託收查詢 結果頁
	@RequestMapping(value = "/f_collection_query_result")
	public String f_collection_query_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		String previous = "/FCY/ACCT/f_collection_query_type";
		log.trace("f_collection_query_result");
		log.trace(ESAPIUtil.vaildLog("reqParam >>{}" + CodeUtil.toJson(reqParam)));
		BaseResult bs = null;
		List<Map<String, String>> downloadMap = new ArrayList();
		String downloadStringTXT = "";
		String downloadStringEXCEL = "";
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			//判斷查詢區間是否在限定範圍
			boolean ckDate = DateUtil.checkDate(okMap.get("CMSDATE"), okMap.get("CMEDATE"), 12);
			if(ckDate) {
				if (hasLocale) {
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
					if (!bs.getResult()) {
						// TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
						log.trace("bs.getResult(): {}", bs.getResult());
						
					} else {
						// session 資料放入 map 讓後續 model 和回上一頁取值
						okMap.putAll((Map<String, String>) bs.getData());
					}
				} else {
					// 登入時的身分證字號
					String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
					// session有無CUSIDN
					if (!"".equals(cusidn) && cusidn != null) {
						// 解密成明碼
						cusidn = new String(Base64.getDecoder().decode(cusidn));
						okMap.put("CUSIDN", cusidn);
					} else {
						log.error("session no cusidn!!!");
					}
					bs = new BaseResult();
					bs = fcy_acct_service.f_collection_query_result(okMap);
					bs.addData("FUNC", okMap.get("FUNC"));
					
				}
			}
			else {
				bs = new BaseResult();
				bs.setResult(false);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("f_collection_query_result error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {

				switch (((Map<String, String>) bs.getData()).get("FUNC")) {
				case "N559":
					downloadMap = ((List<Map<String, String>>) ((Map<String, Object>) bs.getData()).get("CRY"));
					for (Map crydata : downloadMap) {
						downloadStringTXT += crydata.get("AMTRBILLCCY") + " " + crydata.get("FXTOTAMT") + "  i18n{LB.W0158}  "
								+ crydata.get("FXTOTAMTRECNUM") + "  i18n{LB.Rows}  "+"\r\n";
						downloadStringEXCEL += crydata.get("AMTRBILLCCY") + " " + crydata.get("FXTOTAMT") + " i18n{LB.W0158}  "
								+ crydata.get("FXTOTAMTRECNUM") + "  i18n{LB.Rows}  "+"\n";
					}
					bs.addData("DownloadStringTXT", downloadStringTXT.trim());
					bs.addData("DownloadStringEXCEL", downloadStringEXCEL.trim());
					target = "/acct_fcy/f_collection_query_N559_result";
					model.addAttribute("N559_RESULT", bs);
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
					break;

				case "N560":
					downloadMap = ((List<Map<String, String>>) ((Map<String, Object>) bs.getData()).get("CRY"));
					for (Map crydata : downloadMap) {
						downloadStringTXT += crydata.get("AMTRBILLCCY") + " " + crydata.get("FXTOTAMT") + "  i18n{LB.W0158}  "
								+ crydata.get("FXTOTAMTRECNUM") + "  i18n{LB.Rows}  "+"\r\n";
						downloadStringEXCEL += crydata.get("AMTRBILLCCY") + " " + crydata.get("FXTOTAMT") + " i18n{LB.W0158}  "
								+ crydata.get("FXTOTAMTRECNUM") + "  i18n{LB.Rows}  "+"\n";
					}
					bs.addData("DownloadStringTXT", downloadStringTXT.trim());
					bs.addData("DownloadStringEXCEL", downloadStringEXCEL.trim());
					target = "/acct_fcy/f_collection_query_N560_result";
					model.addAttribute("N560_RESULT", bs);
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
					break;

				case "N563":
					downloadMap = ((List<Map<String, String>>) ((Map<String, Object>) bs.getData()).get("CRY"));
					for (Map crydata : downloadMap) {
						downloadStringTXT += crydata.get("AMTRBILLCCY") + " " + crydata.get("FXTOTAMT") + "  i18n{LB.W0158}  "
								+ crydata.get("FXTOTAMTRECNUM") + "  i18n{LB.Rows}  "+"\r\n";
						downloadStringEXCEL += crydata.get("AMTRBILLCCY") + " " + crydata.get("FXTOTAMT") + "  i18n{LB.W0158}  "
								+ crydata.get("FXTOTAMTRECNUM") + "  i18n{LB.Rows}  "+"\n";
					}
					bs.addData("DownloadStringTXT", downloadStringTXT.trim());
					bs.addData("DownloadStringEXCEL", downloadStringEXCEL.trim());
					target = "/acct_fcy/f_collection_query_N563_result";
					model.addAttribute("N563_RESULT", bs);
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
					break;
				}

				List<List<Map<String, String>>> dataList = new ArrayList<>();
				List<List<Map<String, String>>> dataList2 = new ArrayList<>();
				List<Map<String, String>> dataListMap = new ArrayList<>();
				List<Map<String, String>> dataListMap2 = new ArrayList<>();
				dataListMap = ((List<Map<String, String>>) ((Map<String, Object>) bs.getData()).get("REC"));
				dataListMap2 = ((List<Map<String, String>>) ((Map<String, Object>) bs.getData()).get("CRY"));

				dataList.add(dataListMap);
				dataList.add(dataListMap2);
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, dataList);
				SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, dataListMap);
			} else {
				boolean hasLocale = okMap.containsKey("locale");
				if (hasLocale) {
					log.trace("bs.getPrevious: {}", bs.getPrevious());
					bs.setPrevious(bs.getPrevious());
					model.addAttribute(BaseResult.ERROR, bs);
				}else {
					bs.setPrevious(previous+"?FUNC="+okMap.get("FUNC"));
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}
		}
		return target;
	}

	// 進出口託收查詢輸入頁直接下載
		@RequestMapping(value = "/f_collection_query_DirectDownload")
		public String f_collection_query_ajaxDirectDownload(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			log.trace("f_collection_query_DirectDownload");
			String target = "/error";
			String previous = "/FCY/ACCT/f_collection_query_type";
			BaseResult bs = null;
			Map<String, String> okMap=null;
			try {
				bs = new BaseResult();
				String cusidn = new String(
						Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
				log.debug("cusidn {} ", cusidn);
				okMap = ESAPIUtil.validStrMap(reqParam);
				for (Entry<String, String> entry : okMap.entrySet()) {
					okMap.put(entry.getKey(), URLDecoder.decode(entry.getValue(), "UTF-8"));
				}
				okMap.put("CUSIDN", cusidn);
				okMap.put("OKOVNEXT", "TRUE");

				bs = fcy_acct_service.f_collection_query_directDownload(okMap);
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("f_collection_query_ajaxDirectDownload error >> {}",e);
			} finally {
				if (bs != null && bs.getResult()) {
					target = "forward:/directDownload";
					Map<String, Object> dataMap = (Map) bs.getData();
					log.trace("dataMap={}", dataMap);
					List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("REC");
					log.trace("rowListMap={}", rowListMap);
					SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, rowListMap);
					Map<String, Object> parameterMap = (Map<String, Object>) dataMap.get("parameterMap");
					request.setAttribute("parameterMap", parameterMap);
				} else {
					bs.setPrevious(previous+"?FUNC="+okMap.get("FUNC"));
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}
			return target;
		}
}
