package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N361_REST_RS extends BaseRestBean implements Serializable {
	
	private static final long serialVersionUID = -6377810054701980497L;
	private String MSGCOD;			// 訊息代號
	
	// 電文沒回但ms_tw回應的資料
	private String occurMsg;
	private String CMQTIME;			// 交易時間
	private String BRHCOD;			// 簽約行
	
	
	public String getMSGCOD() {
		return MSGCOD;
	}
	
	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}

	public String getOccurMsg() {
		return occurMsg;
	}

	public void setOccurMsg(String occurMsg) {
		this.occurMsg = occurMsg;
	}

	public String getCMQTIME() {
		return CMQTIME;
	}

	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}

	public String getBRHCOD() {
		return BRHCOD;
	}

	public void setBRHCOD(String bRHCOD) {
		BRHCOD = bRHCOD;
	}
	
}