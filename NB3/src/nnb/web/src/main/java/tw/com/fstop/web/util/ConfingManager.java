package tw.com.fstop.web.util;

import java.io.File;

import tw.com.fstop.util.SpringBeanFactory;

/**
 * 功能說明 :管理讀取的環境變數
 * 
 */
public class ConfingManager
{

	
	// #ms_tw
	public static String MS_TW_HOST = SpringBeanFactory.getBean("ms_tw_host");

	public static String MS_TW_PORT = SpringBeanFactory.getBean("ms_tw_port");

	public static String MS_TW_PATH = SpringBeanFactory.getBean("ms_tw_path");
	
	public static String MS_TW = MS_TW_HOST +":"+MS_TW_PORT+"/"+MS_TW_PATH;
	
	// #ms_fund
	public static String MS_FUND_HOST = SpringBeanFactory.getBean("ms_fund_host");

	public static String MS_FUND_PORT = SpringBeanFactory.getBean("ms_fund_port");

	public static String MS_FUND_PATH = SpringBeanFactory.getBean("ms_fund_path");
	
	public static String MS_FUND = MS_FUND_HOST +":"+MS_FUND_PORT+"/"+MS_FUND_PATH;

	// #ms_fx
	public static String MS_FX_HOST = SpringBeanFactory.getBean("ms_fx_host");

	public static String MS_FX_PORT = SpringBeanFactory.getBean("ms_fx_port");

	public static String MS_FX_PATH = SpringBeanFactory.getBean("ms_fx_path");

	public static String MS_FX = MS_FX_HOST +":"+MS_FX_PORT+"/"+MS_FX_PATH;
	
	// #ms_gold
	public static String MS_GOLD_HOST = SpringBeanFactory.getBean("ms_gold_host");

	public static String MS_GOLD_PORT = SpringBeanFactory.getBean("ms_gold_port");

	public static String MS_GOLD_PATH = SpringBeanFactory.getBean("ms_gold_path");

	public static String MS_GOLD = MS_GOLD_HOST +":"+MS_GOLD_PORT+"/"+MS_GOLD_PATH;
	
	// #ms_cc
	public static String MS_CC_HOST = SpringBeanFactory.getBean("ms_cc_host");

	public static String MS_CC_PORT = SpringBeanFactory.getBean("ms_cc_port");

	public static String MS_CC_PATH = SpringBeanFactory.getBean("ms_cc_path");

	public static String MS_CC = MS_CC_HOST +":"+MS_CC_PORT+"/"+MS_CC_PATH;
	
	// #ms_loan
	public static String MS_LOAN_HOST = SpringBeanFactory.getBean("ms_loan_host");

	public static String MS_LOAN_PORT = SpringBeanFactory.getBean("ms_loan_port");

	public static String MS_LOAN_PATH = SpringBeanFactory.getBean("ms_loan_path");

	public static String MS_LOAN = MS_LOAN_HOST +":"+MS_LOAN_PORT+"/"+MS_LOAN_PATH;
	
	// #ms_ols
	public static String MS_OLS_HOST = SpringBeanFactory.getBean("ms_ols_host");

	public static String MS_OLS_PORT = SpringBeanFactory.getBean("ms_ols_port");

	public static String MS_OLS_PATH = SpringBeanFactory.getBean("ms_ols_path");

	public static String MS_OLS = MS_OLS_HOST +":"+MS_OLS_PORT+"/"+MS_OLS_PATH;
	
	// #ms_ola
	public static String MS_OLA_HOST = SpringBeanFactory.getBean("ms_ola_host");
	
	public static String MS_OLA_PORT = SpringBeanFactory.getBean("ms_ola_port");
	
	public static String MS_OLA_PATH = SpringBeanFactory.getBean("ms_ola_path");
	
	public static String MS_OLA = MS_OLA_HOST +":"+MS_OLA_PORT+"/"+MS_OLA_PATH;
	
	// #ms_pay
	public static String MS_PAY_HOST = SpringBeanFactory.getBean("ms_pay_host");

	public static String MS_PAY_PORT = SpringBeanFactory.getBean("ms_pay_port");

	public static String MS_PAY_PATH = SpringBeanFactory.getBean("ms_pay_path");

	public static String MS_PAY = MS_PAY_HOST +":"+MS_PAY_PORT+"/"+MS_PAY_PATH;
	
	// #ms_ps
	public static String MS_PS_HOST = SpringBeanFactory.getBean("ms_ps_host");

	public static String MS_PS_PORT = SpringBeanFactory.getBean("ms_ps_port");

	public static String MS_PS_PATH = SpringBeanFactory.getBean("ms_ps_path");

	public static String MS_PS = MS_PS_HOST +":"+MS_PS_PORT+"/"+MS_PS_PATH;
	
	// #ms_auth
	public static String MS_AUTH_HOST = SpringBeanFactory.getBean("ms_auth_host");

	public static String MS_AUTH_PORT = SpringBeanFactory.getBean("ms_auth_port");

	public static String MS_AUTH_PATH = SpringBeanFactory.getBean("ms_auth_path");
	
	public static String MS_AUTH = MS_AUTH_HOST +":"+MS_AUTH_PORT+"/"+MS_AUTH_PATH;
	
	// #IDGATE

	public static String MS_TW_IDGATE_PATH = SpringBeanFactory.getBean("ms_tw_idgate_path");
		
	public static String MS_TW_IDGATE = MS_TW_HOST +":"+MS_TW_PORT+"/"+MS_TW_IDGATE_PATH;
	
	//ms_qr
	
	public static String MS_QR_HOST = SpringBeanFactory.getBean("ms_qr_host");

	public static String MS_QR_PORT = SpringBeanFactory.getBean("ms_qr_port");

	public static String MS_QR_PATH = SpringBeanFactory.getBean("ms_qr_path");
	
	public static String MS_QR = MS_QR_HOST +":"+MS_QR_PORT+"/"+MS_QR_PATH;
	
	
	
	// #COM_IDGATE
	public static String MS_TW_COM_IDGATE = SpringBeanFactory.getBean("ms_tw_com_idgate_uri");
	public static String MS_FX_COM_IDGATE = SpringBeanFactory.getBean("ms_fx_com_idgate_uri");
	public static String MS_CC_COM_IDGATE = SpringBeanFactory.getBean("ms_cc_com_idgate_uri");
	public static String MS_PS_COM_IDGATE = SpringBeanFactory.getBean("ms_ps_com_idgate_uri");
	public static String MS_PAY_COM_IDGATE = SpringBeanFactory.getBean("ms_pay_com_idgate_uri");
	public static String MS_OLS_COM_IDGATE = SpringBeanFactory.getBean("ms_ols_com_idgate_uri");
	public static String MS_OLA_COM_IDGATE = SpringBeanFactory.getBean("ms_ola_com_idgate_uri");
	public static String MS_LOAN_COM_IDGATE = SpringBeanFactory.getBean("ms_loan_com_idgate_uri");
	public static String MS_FUND_COM_IDGATE = SpringBeanFactory.getBean("ms_fund_com_idgate_uri");
	public static String MS_GOLD_COM_IDGATE = SpringBeanFactory.getBean("ms_gold_com_idgate_uri");
	
	public static String MS_TW_COM_IDGATE_PATH = MS_TW_HOST +":"+MS_TW_PORT+"/"+MS_TW_COM_IDGATE;
	public static String MS_FX_COM_IDGATE_PATH = MS_FX_HOST +":"+MS_FX_PORT+"/"+MS_FX_COM_IDGATE;
	public static String MS_CC_COM_IDGATE_PATH = MS_CC_HOST +":"+MS_CC_PORT+"/"+MS_CC_COM_IDGATE;
	public static String MS_PS_COM_IDGATE_PATH = MS_PS_HOST +":"+MS_PS_PORT+"/"+MS_PS_COM_IDGATE;
	public static String MS_PAY_COM_IDGATE_PATH = MS_PAY_HOST +":"+MS_PAY_PORT+"/"+MS_PAY_COM_IDGATE;
	public static String MS_OLS_COM_IDGATE_PATH = MS_OLS_HOST +":"+MS_OLS_PORT+"/"+MS_OLS_COM_IDGATE;
	public static String MS_OLA_COM_IDGATE_PATH = MS_OLA_HOST +":"+MS_OLA_PORT+"/"+MS_OLA_COM_IDGATE;
	public static String MS_LOAN_COM_IDGATE_PATH = MS_LOAN_HOST +":"+MS_LOAN_PORT+"/"+MS_LOAN_COM_IDGATE;
	public static String MS_FUND_COM_IDGATE_PATH = MS_FUND_HOST +":"+MS_FUND_PORT+"/"+MS_FUND_COM_IDGATE;
	public static String MS_GOLD_COM_IDGATE_PATH = MS_GOLD_HOST +":"+MS_GOLD_PORT+"/"+MS_GOLD_COM_IDGATE;
	
}
