package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class Aioedm_REST_RS extends BaseRestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -453469269793798738L;

	private String cDate;
	private String cEmail;
	private String rtnDesc;
	private String rtnCode;
	
	public String getcDate() {
		return cDate;
	}
	public void setcDate(String cDate) {
		this.cDate = cDate;
	}
	public String getcEmail() {
		return cEmail;
	}
	public void setcEmail(String cEmail) {
		this.cEmail = cEmail;
	}
	public String getRtnDesc() {
		return rtnDesc;
	}
	public void setRtnDesc(String rtnDesc) {
		this.rtnDesc = rtnDesc;
	}
	public String getRtnCode() {
		return rtnCode;
	}
	public void setRtnCode(String rtnCode) {
		this.rtnCode = rtnCode;
	}
	
}
