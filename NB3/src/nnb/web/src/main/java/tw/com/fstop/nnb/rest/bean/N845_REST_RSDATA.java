package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N845_REST_RSDATA implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7241466661299044398L;
	
	private String ODFBAL;
	private String FEEAMT1;
	private String LOST01;
	private String LOST02;
	private String GDBAL;
	private String RATIO;
	private String FILLER;
	private String ACN;
	private String FEEAMT2;
	private String MKBAL;
	private String TSFBAL;
	
	public String getODFBAL() {
		return ODFBAL;
	}
	public void setODFBAL(String oDFBAL) {
		ODFBAL = oDFBAL;
	}
	public String getFEEAMT1() {
		return FEEAMT1;
	}
	public void setFEEAMT1(String fEEAMT1) {
		FEEAMT1 = fEEAMT1;
	}
	public String getLOST01() {
		return LOST01;
	}
	public void setLOST01(String lOST01) {
		LOST01 = lOST01;
	}
	public String getLOST02() {
		return LOST02;
	}
	public void setLOST02(String lOST02) {
		LOST02 = lOST02;
	}
	public String getGDBAL() {
		return GDBAL;
	}
	public void setGDBAL(String gDBAL) {
		GDBAL = gDBAL;
	}
	public String getRATIO() {
		return RATIO;
	}
	public void setRATIO(String rATIO) {
		RATIO = rATIO;
	}
	public String getFILLER() {
		return FILLER;
	}
	public void setFILLER(String fILLER) {
		FILLER = fILLER;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getFEEAMT2() {
		return FEEAMT2;
	}
	public void setFEEAMT2(String fEEAMT2) {
		FEEAMT2 = fEEAMT2;
	}
	public String getMKBAL() {
		return MKBAL;
	}
	public void setMKBAL(String mKBAL) {
		MKBAL = mKBAL;
	}
	public String getTSFBAL() {
		return TSFBAL;
	}
	public void setTSFBAL(String tSFBAL) {
		TSFBAL = tSFBAL;
	}
}
