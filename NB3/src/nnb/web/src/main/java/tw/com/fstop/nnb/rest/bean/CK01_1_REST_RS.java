package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class CK01_1_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -538776543972624632L;

	
	private String CUSTID;
	private String CUSNAME;
	private String HPHONE;
	private String OPHONE;
	private String MPFONE;
	private String CARDNUM;
	private String CURRBAL;
	private String POT;
	private String RATE;
	private String CRLIMIT;
	private String AMOUNT;
	private String PERIOD;
	private String APPLY_RATE;
	private String FIRST_AMOUNT;
	private String FIRST_INTEREST;
	private String PERIOD_AMOUNT;
	private String USERIP;
	
	
	public String getCUSTID() {
		return CUSTID;
	}
	public String getCUSNAME() {
		return CUSNAME;
	}
	public String getHPHONE() {
		return HPHONE;
	}
	public String getOPHONE() {
		return OPHONE;
	}
	public String getMPFONE() {
		return MPFONE;
	}
	public String getCARDNUM() {
		return CARDNUM;
	}
	public String getCURRBAL() {
		return CURRBAL;
	}
	public String getPOT() {
		return POT;
	}
	public String getRATE() {
		return RATE;
	}
	public String getCRLIMIT() {
		return CRLIMIT;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public String getPERIOD() {
		return PERIOD;
	}
	public String getAPPLY_RATE() {
		return APPLY_RATE;
	}
	public String getFIRST_AMOUNT() {
		return FIRST_AMOUNT;
	}
	public String getFIRST_INTEREST() {
		return FIRST_INTEREST;
	}
	public String getPERIOD_AMOUNT() {
		return PERIOD_AMOUNT;
	}
	public String getUSERIP() {
		return USERIP;
	}
	public void setCUSTID(String cUSTID) {
		CUSTID = cUSTID;
	}
	public void setCUSNAME(String cUSNAME) {
		CUSNAME = cUSNAME;
	}
	public void setHPHONE(String hPHONE) {
		HPHONE = hPHONE;
	}
	public void setOPHONE(String oPHONE) {
		OPHONE = oPHONE;
	}
	public void setMPFONE(String mPFONE) {
		MPFONE = mPFONE;
	}
	public void setCARDNUM(String cARDNUM) {
		CARDNUM = cARDNUM;
	}
	public void setCURRBAL(String cURRBAL) {
		CURRBAL = cURRBAL;
	}
	public void setPOT(String pOT) {
		POT = pOT;
	}
	public void setRATE(String rATE) {
		RATE = rATE;
	}
	public void setCRLIMIT(String cRLIMIT) {
		CRLIMIT = cRLIMIT;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	public void setPERIOD(String pERIOD) {
		PERIOD = pERIOD;
	}
	public void setAPPLY_RATE(String aPPLY_RATE) {
		APPLY_RATE = aPPLY_RATE;
	}
	public void setFIRST_AMOUNT(String fIRST_AMOUNT) {
		FIRST_AMOUNT = fIRST_AMOUNT;
	}
	public void setFIRST_INTEREST(String fIRST_INTEREST) {
		FIRST_INTEREST = fIRST_INTEREST;
	}
	public void setPERIOD_AMOUNT(String pERIOD_AMOUNT) {
		PERIOD_AMOUNT = pERIOD_AMOUNT;
	}
	public void setUSERIP(String uSERIP) {
		USERIP = uSERIP;
	}
	
}

