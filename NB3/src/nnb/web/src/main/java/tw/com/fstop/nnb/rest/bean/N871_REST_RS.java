package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N871_REST_RS extends BaseRestBean implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = -163291489396624102L;
	
	LinkedList<N871_REST_RSDATA> REC;
	private String CMQTIME;
	private String CMRECNUM;
	private String CMPERIOD;
	
	
	public LinkedList<N871_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<N871_REST_RSDATA> rEC) {
		REC = rEC;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getCMRECNUM() {
		return CMRECNUM;
	}
	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}
	public String getCMPERIOD() {
		return CMPERIOD;
	}
	public void setCMPERIOD(String cMPERIOD) {
		CMPERIOD = cMPERIOD;
	}
}
