package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class Ratequery_N026_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8296157067534947937L;

	private String UPDATEDATE;

	private String UPDATETIME;
	// 資料陣列
	private LinkedList<Ratequery_N026_RSDATA> REC;

	public String getUPDATEDATE() {
		return UPDATEDATE;
	}

	public String getUPDATETIME() {
		return UPDATETIME;
	}

	public LinkedList<Ratequery_N026_RSDATA> getREC() {
		return REC;
	}

	public void setUPDATEDATE(String uPDATEDATE) {
		UPDATEDATE = uPDATEDATE;
	}

	public void setUPDATETIME(String uPDATETIME) {
		UPDATETIME = uPDATETIME;
	}

	public void setREC(LinkedList<Ratequery_N026_RSDATA> rEC) {
		REC = rEC;
	}

}
