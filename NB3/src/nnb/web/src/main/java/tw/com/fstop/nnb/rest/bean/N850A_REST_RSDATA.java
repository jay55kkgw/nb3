package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N850A_REST_RSDATA implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 5995945201917667164L;
	
	private String DATE;
	private String STATUS;
	private String GDBAL;
	private String OFFSET;
	private String TOPMSG;
	private String __OCCURS;
	private String HEADER;
	private String TIME;
	private String CUSIDN;
	private String ACN;
	private String TSFBAL;
	
	
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getGDBAL() {
		return GDBAL;
	}
	public void setGDBAL(String gDBAL) {
		GDBAL = gDBAL;
	}
	public String getOFFSET() {
		return OFFSET;
	}
	public void setOFFSET(String oFFSET) {
		OFFSET = oFFSET;
	}
	public String getTOPMSG() {
		return TOPMSG;
	}
	public void setTOPMSG(String tOPMSG) {
		TOPMSG = tOPMSG;
	}
	public String get__OCCURS() {
		return __OCCURS;
	}
	public void set__OCCURS(String __OCCURS) {
		this.__OCCURS = __OCCURS;
	}
	public String getHEADER() {
		return HEADER;
	}
	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getTSFBAL() {
		return TSFBAL;
	}
	public void setTSFBAL(String tSFBAL) {
		TSFBAL = tSFBAL;
	}
}
