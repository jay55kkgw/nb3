package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;


public class N570_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5370773529503299761L;
	
	@SerializedName(value = "CUSAD1C")
	private String CUSAD1C;
	@SerializedName(value = "CUSAD2C")
	private String CUSAD2C;
	@SerializedName(value = "CUSADD1")
	private String CUSADD1;
	@SerializedName(value = "CUSADD2")
	private String CUSADD2;
	@SerializedName(value = "CUSADD3")
	private String CUSADD3;
	
	@SerializedName(value = "CUSTEL1")
	private String CUSTEL1;
	@SerializedName(value = "CUSTEL2")
	private String CUSTEL2;
	
	@Override
	public Map<String, String> coverMap(){
		LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", "變更外匯進口／出口／匯兌通訊地址／電話");
		result.put("交易型態", "-");
		result.put("變更後中文地址", this.CUSAD1C + " " + this.CUSAD2C);
		result.put("變更後英文地址", this.CUSADD1 + " " + this.CUSADD2 + " " + this.CUSADD3);
		result.put("變更後TEL", this.CUSTEL1);
		result.put("變更後FAX", this.CUSTEL2);
		return result;
	}

	public String getCUSAD1C() {
		return CUSAD1C;
	}

	public void setCUSAD1C(String cUSAD1C) {
		CUSAD1C = cUSAD1C;
	}

	public String getCUSAD2C() {
		return CUSAD2C;
	}

	public void setCUSAD2C(String cUSAD2C) {
		CUSAD2C = cUSAD2C;
	}

	public String getCUSADD1() {
		return CUSADD1;
	}

	public void setCUSADD1(String cUSADD1) {
		CUSADD1 = cUSADD1;
	}

	public String getCUSADD2() {
		return CUSADD2;
	}

	public void setCUSADD2(String cUSADD2) {
		CUSADD2 = cUSADD2;
	}

	public String getCUSADD3() {
		return CUSADD3;
	}

	public void setCUSADD3(String cUSADD3) {
		CUSADD3 = cUSADD3;
	}

	public String getCUSTEL1() {
		return CUSTEL1;
	}

	public void setCUSTEL1(String cUSTEL1) {
		CUSTEL1 = cUSTEL1;
	}

	public String getCUSTEL2() {
		return CUSTEL2;
	}

	public void setCUSTEL2(String cUSTEL2) {
		CUSTEL2 = cUSTEL2;
	}


}
