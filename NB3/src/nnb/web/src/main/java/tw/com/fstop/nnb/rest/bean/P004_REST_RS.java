package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class P004_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -646529677910131676L;
	
	private String TxnCode;//交易代碼
	private String TxnDateTime;//交易時間
	private String SerialNo;//流水號
	private String RespCode;//回應代碼
	private String RespString;//回應代碼敘述
	private String CustomerId;//客戶ID
	private String StartRecordNo;//起始筆數
	private String MaxRecordsPerTime;//本次查詢結果最多可回應的資料筆數
	private String TotalRecord;//本次查詢條件在資料庫查詢所得的總筆數
	private String WaitPayRecord;//本次查詢條件下的待扣款筆數
	private String PaySuccessRecord;//本次查詢條件下的扣款成功筆數
	private String PayFailRecord;//本次查詢條件下的扣款失敗筆數
	private String Count;//申請筆數
	private String CMQTIME;
	private String CMSEACHPERDATE;
	private LinkedList<P004_REST_RSDATA> REC;
	private String ZoneCodeZip;
	private String CarKindZip;
	private String SessionCodeZip;
	
	
	public String getZoneCodeZip() {
		return ZoneCodeZip;
	}
	public void setZoneCodeZip(String zoneCodeZip) {
		ZoneCodeZip = zoneCodeZip;
	}
	public String getCarKindZip() {
		return CarKindZip;
	}
	public void setCarKindZip(String carKindZip) {
		CarKindZip = carKindZip;
	}
	public String getSessionCodeZip() {
		return SessionCodeZip;
	}
	public void setSessionCodeZip(String sessionCodeZip) {
		SessionCodeZip = sessionCodeZip;
	}
	public LinkedList<P004_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<P004_REST_RSDATA> rEC) {
		REC = rEC;
	}
	public String getTxnCode() {
		return TxnCode;
	}
	public void setTxnCode(String txnCode) {
		TxnCode = txnCode;
	}
	public String getTxnDateTime() {
		return TxnDateTime;
	}
	public void setTxnDateTime(String txnDateTime) {
		TxnDateTime = txnDateTime;
	}
	public String getSerialNo() {
		return SerialNo;
	}
	public void setSerialNo(String serialNo) {
		SerialNo = serialNo;
	}
	public String getRespCode() {
		return RespCode;
	}
	public void setRespCode(String respCode) {
		RespCode = respCode;
	}
	public String getRespString() {
		return RespString;
	}
	public void setRespString(String respString) {
		RespString = respString;
	}
	public String getCustomerId() {
		return CustomerId;
	}
	public void setCustomerId(String customerId) {
		CustomerId = customerId;
	}
	public String getStartRecordNo() {
		return StartRecordNo;
	}
	public void setStartRecordNo(String startRecordNo) {
		StartRecordNo = startRecordNo;
	}
	public String getMaxRecordsPerTime() {
		return MaxRecordsPerTime;
	}
	public void setMaxRecordsPerTime(String maxRecordsPerTime) {
		MaxRecordsPerTime = maxRecordsPerTime;
	}
	public String getTotalRecord() {
		return TotalRecord;
	}
	public void setTotalRecord(String totalRecord) {
		TotalRecord = totalRecord;
	}
	public String getWaitPayRecord() {
		return WaitPayRecord;
	}
	public void setWaitPayRecord(String waitPayRecord) {
		WaitPayRecord = waitPayRecord;
	}
	public String getPaySuccessRecord() {
		return PaySuccessRecord;
	}
	public void setPaySuccessRecord(String paySuccessRecord) {
		PaySuccessRecord = paySuccessRecord;
	}
	public String getPayFailRecord() {
		return PayFailRecord;
	}
	public void setPayFailRecord(String payFailRecord) {
		PayFailRecord = payFailRecord;
	}
	public String getCount() {
		return Count;
	}
	public void setCount(String count) {
		Count = count;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getCMSEACHPERDATE() {
		return CMSEACHPERDATE;
	}
	public void setCMSEACHPERDATE(String cMSEACHPERDATE) {
		CMSEACHPERDATE = cMSEACHPERDATE;
	}
}
