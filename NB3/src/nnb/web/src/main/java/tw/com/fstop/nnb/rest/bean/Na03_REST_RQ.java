package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * NA03電文RQ
 */
public class Na03_REST_RQ extends BaseRestBean_CC implements Serializable{
	private static final long serialVersionUID = 6868861910004819383L;
	
	private String CPRIMID;
	private String CPRIMEMAIL;
	private String VARSTR3;
	private String CNDESC;
	private String RCVNO;
	
	public String getCPRIMID() {
		return CPRIMID;
	}
	public void setCPRIMID(String cPRIMID) {
		CPRIMID = cPRIMID;
	}
	public String getCPRIMEMAIL() {
		return CPRIMEMAIL;
	}
	public void setCPRIMEMAIL(String cPRIMEMAIL) {
		CPRIMEMAIL = cPRIMEMAIL;
	}
	public String getVARSTR3() {
		return VARSTR3;
	}
	public void setVARSTR3(String vARSTR3) {
		VARSTR3 = vARSTR3;
	}
	public String getCNDESC() {
		return CNDESC;
	}
	public void setCNDESC(String cNDESC) {
		CNDESC = cNDESC;
	}
	public String getRCVNO() {
		return RCVNO;
	}
	public void setRCVNO(String rCVNO) {
		RCVNO = rCVNO;
	}
}