package tw.com.fstop.nnb.rest.bean;
import java.io.Serializable;
import java.util.LinkedList;

public class N567_REST_RS extends BaseRestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4702084885559879641L;
	private String CMQTIME;
    private String CMRECNUM;
    private String OCCURS;
    private String CMPERIOD;
	private String LENGTH;//LENGTH
	private String ABEND;//結束代碼
	private String USERDATA;//本次未完資料KEY值
	private String REC_NO;//筆數
    private LinkedList<N567_REST_RSDATA> REC;
    
    public LinkedList<N567_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<N567_REST_RSDATA> rEC) {
		REC = rEC;
	}
	public String getLENGTH() {
		return LENGTH;
	}
	public void setLENGTH(String lENGTH) {
		LENGTH = lENGTH;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getCMRECNUM() {
		return CMRECNUM;
	}
	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}
	public String getABEND() {
		return ABEND;
	}
	public void setABEND(String aBEND) {
		ABEND = aBEND;
	}
	public String getREC_NO() {
		return REC_NO;
	}
	public void setREC_NO(String rEC_NO) {
		REC_NO = rEC_NO;
	}
	public String getOCCURS() {
		return OCCURS;
	}
	public void setOCCURS(String oCCURS) {
		OCCURS = oCCURS;
	}
	public String getUSERDATA() {
		return USERDATA;
	}
	public void setUSERDATA(String uSERDATA) {
		USERDATA = uSERDATA;
	}
	public String getCMPERIOD() {
		return CMPERIOD;
	}
	public void setCMPERIOD(String cMPERIOD) {
		CMPERIOD = cMPERIOD;
	}

}
