package tw.com.fstop.nnb.spring.controller;

import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.aop.Dialog;
import tw.com.fstop.nnb.service.Fund_Regular_Service;
import tw.com.fstop.nnb.service.Fund_Transfer_Service;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtils;
import tw.com.fstop.web.util.WebUtil;

/**
 * 基金定期（不）定額申購的Controller
 */
@SessionAttributes({SessionUtil.CUSIDN,SessionUtil.KYCDATE,SessionUtil.WEAK,SessionUtil.DPMYEMAIL,
	SessionUtil.RESULT_LOCALE_DATA,SessionUtil.CONFIRM_LOCALE_DATA,SessionUtil.STEP2_LOCALE_DATA,SessionUtil.STEP3_LOCALE_DATA})
@Controller
@RequestMapping(value = "/FUND/REGULAR")
public class Fund_Regular_Controller{
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private Fund_Transfer_Service fund_transfer_service;
	@Autowired
	private Fund_Regular_Service fund_regular_service;
	
	@Autowired
	private I18n i18n;
	
	/**
	 * 前往基金定期（不）定額選擇頁
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/fund_regular_select")
	public String fund_regular_select(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		BaseResult bs = null;
		log.debug("fund_regular_select");
		
		try{
			bs = new BaseResult();
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
			log.debug("cusidn={}",cusidn);
			String UID = cusidn;
			model.addAttribute("UID",UID);
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			model.addAttribute("CMQTIME",sdf.format(new Date()));
			
			bs = fund_transfer_service.getN922Data(cusidn);
			
			if(bs == null || !bs.getResult()){
				model.addAttribute(BaseResult.ERROR,bs);
				return target;
			}
			Map<String,Object> bsData = (Map<String,Object>)bs.getData();
			log.debug("bsData={}",bsData);
			
			String FDINVTYPE = (String)bsData.get("FDINVTYPE");
			log.debug("FDINVTYPE={}",FDINVTYPE);
			model.addAttribute("FDINVTYPE",FDINVTYPE);
			
			String FDINVTYPEChinese;
			if("1".equals(FDINVTYPE)){
				FDINVTYPEChinese = i18n.getMsg("LB.D0945");//積極型
			}
			else if("2".equals(FDINVTYPE)){
				FDINVTYPEChinese = i18n.getMsg("LB.X1766");//穩健型
			}
			else{
				FDINVTYPEChinese = i18n.getMsg("LB.X1767");//保守型
			}
			log.debug("FDINVTYPEChinese={}",FDINVTYPEChinese);
			model.addAttribute("FDINVTYPEChinese",FDINVTYPEChinese);
			
			//判斷KYC來源為行外且無錄音檔時，轉入錯誤訊息頁面 
			//是否為行外
			String EXTRCE = bsData.get("EXTRCE") == null ? "" : ((String)bsData.get("EXTRCE")).trim();
			log.debug("EXTRCE={}",EXTRCE);
			
			//錄音檔交易序號
			String RECNUM = bsData.get("RECNUM") == null ? "" : ((String)bsData.get("RECNUM")).trim();
			log.debug("RECNUM={}",RECNUM);
			
			if("Y".equals(EXTRCE) && RECNUM.length() == 0){
				bs.setMessage("Z622",i18n.getMsg("LB.X2146"));
				model.addAttribute(BaseResult.ERROR,bs);
				return target;
			}
			
//			//20220318 移除此導頁功能 , 改由頁面提示
//			//檢核Email
//			String WANAEMAIL = okMap.get("WANAEMAIL");
//			log.debug(ESAPIUtil.vaildLog("WANAEMAIL={}"+WANAEMAIL));
//			
//			if(!"NONEED".equals(WANAEMAIL)){
//				String DPMYEMAIL = (String)SessionUtil.getAttribute(model,SessionUtil.DPMYEMAIL,null);
//				log.debug("DPMYEMAIL={}",DPMYEMAIL);
//				
//				if(DPMYEMAIL == null || "".equals(DPMYEMAIL)){
//					//設定EMAIL
//					target = "forward:/FUND/TRANSFER/fundemail?ADOPID=FundEmail&TXID=C017";
//					return target;
//				}
//			}
			
			//問卷填寫日期
			String GETLTD = (String)bsData.get("GETLTD");
			log.debug("GETLTD={}",GETLTD);
			
			String KYCDAT = "";
			if(GETLTD.length() == 7 && GETLTD.equals("9999999")){
				KYCDAT = "9" + GETLTD;
			}
			else if(GETLTD.length() == 7 && !GETLTD.equals("9999999")){
				KYCDAT = "0" + GETLTD;
			}
			else{
				KYCDAT = GETLTD;
			}
			log.debug("KYCDAT={}",KYCDAT);
			model.addAttribute("KYCDAT",KYCDAT);
			SessionUtil.addAttribute(model,SessionUtil.KYCDATE,KYCDAT);
			
			//專業投資人屬性
			String RISK7 = bsData.get("RISK7") == null ? "" : ((String)bsData.get("RISK7")).trim();
			log.debug("RISK7={}",RISK7);
			model.addAttribute("RISK7",RISK7);
			//生日
			String BRTHDY = bsData.get("BRTHDY") == null ? "" : (String)bsData.get("BRTHDY");
			log.debug("BRTHDY={}",BRTHDY);
			//學歷
			String DEGREE = bsData.get("DEGREE") == null ? "" : (String)bsData.get("DEGREE");
			log.debug("DEGREE={}",DEGREE);
			//重大傷病
			String MARK1 = bsData.get("MARK1") == null ? "" : (String)bsData.get("MARK1");
			log.debug("MARK1={}",MARK1);
			//一年內信託交易>=5 筆註記
			String MARK3 = bsData.get("MARK3") == null ? "" : (String)bsData.get("MARK3");
			log.debug("MARK3={}",MARK3);
			
			int age = fund_transfer_service.getAge(BRTHDY);
		    
		    String WEAK = "";
		    if(age >= 70){
		    	WEAK = "Y";
		    }
		    else{
		    	WEAK = "N";
		    }
		    if(DEGREE.equals("6")){
		    	WEAK += "Y";
		    }
		    else{
		    	WEAK += "N";
		    }
		    if(MARK1.equals("Y")){
		    	WEAK += "Y";
		    }
		    else{
		    	WEAK += "N";
		    }
		    if(MARK3.equals("Y")){
		    	WEAK += "Y";
		    }
		    else{
		    	WEAK += "N";
		    }
		    log.debug("WEAK={}",WEAK);
		    model.addAttribute("WEAK",WEAK);
		    SessionUtil.addAttribute(model,SessionUtil.WEAK,WEAK);
			
			//新台幣扣帳帳號
			String ACN1 = (String)bsData.get("ACN1");
			log.debug("ACN1={}",ACN1);
			model.addAttribute("ACN1",ACN1);
			
			//外幣扣帳帳號
			String ACN2 = (String)bsData.get("ACN2");
			log.debug("ACN2={}",ACN2);
			model.addAttribute("ACN2",ACN2);
			
			//簽約行
			String APLBRH = (String)bsData.get("APLBRH");
			log.debug("APLBRH={}",APLBRH);
			model.addAttribute("BRHCOD",APLBRH);
			
			//客戶性質別
			String CUTTYPE = (String)bsData.get("CUTTYPE");
			log.debug("CUTTYPE={}",CUTTYPE);
			model.addAttribute("CUTTYPE",CUTTYPE);
			
			String CUTTYPEString;
			if("01".equals(CUTTYPE)){
				CUTTYPEString = "1";
			}
			else{
				CUTTYPEString = "2";
			}
			log.debug("CUTTYPEString={}",CUTTYPEString);
			model.addAttribute("CUTTYPEString",CUTTYPEString);
			
			//是否約定信用卡
			String ICCOD = (String)bsData.get("ICCOD");
			log.debug("ICCOD={}",ICCOD);
			model.addAttribute("ICCOD",ICCOD);
			
			String EMPNO = (String)bsData.get("EMPNO");
			log.debug("EMPNO={}",EMPNO);
			model.addAttribute("EMPNO",EMPNO);

			String KYCNO = (String)bsData.get("CTDNUM");
			log.debug("KYCNO={}", KYCNO);
			model.addAttribute("KYCNO", KYCNO);
			
			boolean creditCardFlag = false;
			
			if("Y".equals(ICCOD)){
				BaseResult n812BS = new BaseResult();
				n812BS = fund_regular_service.getN812Data(cusidn);
				//測試n812失效時
				//n812BS.setResult(false);
				if (null != n812BS && n812BS.getResult()) {
					
					Map<String, Object> n812BSData = (Map<String, Object>) n812BS.getData();
					log.debug("n812BSData={}", n812BSData);

					List<Map<String, String>> rows = (List<Map<String, String>>) n812BSData.get("REC");
					log.debug("rows={}", rows);

					for (int x = 0; x < rows.size(); x++) {
						Map<String, String> map = rows.get(x);
						String CARDNUM = map.get("CARDNUM");
						log.debug("CARDNUM={}", CARDNUM);

						String hiddenCARDNUM = WebUtil.hideCardNum(CARDNUM);
						log.debug("hiddenCARDNUM={}", hiddenCARDNUM);

						map.put("hiddenCARDNUM", hiddenCARDNUM);
					}

					model.addAttribute("N812REC", rows);

					creditCardFlag = true;
				}
			}
			model.addAttribute("creditCardFlag",creditCardFlag);
			
		    String time1 = "";
			String time2 = "";
			long day = 0;
			if(!GETLTD.equals("")){
				time1 = new SimpleDateFormat("yyyy/MM/dd").format(new Date());
				log.debug("time1={}",time1);
				String GETLTDString = Integer.valueOf(GETLTD.substring(0,3)) + 1911 + "";
				GETLTDString = GETLTDString + GETLTD.substring(3);
				log.debug("GETLTDString={}",GETLTDString);
				time2 = GETLTDString.substring(0,4) + "/" + GETLTDString.substring(4,6) + "/" + GETLTDString.substring(6,8);
				log.debug("time2={}",time2);
				
				//算出距離上次問卷填寫天數
				day = fund_transfer_service.getRangeDay(time1,time2);
			}
			
			//問卷版本日期
			String KYCDate = fund_transfer_service.getKYCDate();
			
			if(KYCDate == null){
				KYCDate = "";
			}
			//自然人版， 需有投資屬性及問卷填寫日期已滿一年或問卷版本日期大於問卷填寫日期；專業投資人跳過不判斷KYC
			if(((!FDINVTYPE.equals("") && !GETLTD.equals("") && day >= 365) || ((!GETLTD.equals("") && !KYCDate.equals("")) && Integer.parseInt(KYCDate) > Integer.parseInt(GETLTD))) && UID.length() == 10 && RISK7.length() == 0){
			//假的
//			if(false){
				if(day >= 365){
				//假的
//				if(true){
//				if(false){
				    model.addAttribute("TXID","C017");
				    model.addAttribute("ALERTTYPE","2");
				    
				    fund_transfer_service.prepareFundRiskAlertData(okMap,GETLTD,RISK7,model);
				    
				    target = "/fund/fund_risk_alert";
				}
				else{
					target = "forward:/FUND/TRANSFER/fund_invest_attr1?TXID=C017";
				}
				return target;
		    }
			//法人版，需有投資屬性及問卷填寫日期已滿一年或問卷版本日期大於問卷填寫日期；專業投資人跳過不判斷KYC
			else if(((!FDINVTYPE.equals("") && !GETLTD.equals("") && day >= 365) || ((!GETLTD.equals("") && !KYCDate.equals("")) && Integer.parseInt(KYCDate) > Integer.parseInt(GETLTD))) && UID.length() < 10 && RISK7.length() == 0){  	
//			else if(true){
				if(day>=365){
//				if(false){
					model.addAttribute("TXID","C017");
				    model.addAttribute("ALERTTYPE","2");
				    
				    fund_transfer_service.prepareFundRiskAlertData(okMap,GETLTD,RISK7,model);
				    
				    target = "/fund/fund_risk_alert";
				}
				else{		
					target = "forward:/FUND/TRANSFER/fund_invest_attr3?TXID=C017";
				}
				return target;
		    }
		    else if(FDINVTYPE.equals("") && RISK7.length() == 0){
		     	if(UID.length() == 10){
					target = "forward:/FUND/TRANSFER/fund_invest_attr1?TXID=C017";
		     	}
		    	else{
					target = "forward:/FUND/TRANSFER/fund_invest_attr3?TXID=C017";
		    	}
		     	return target;
		    }
			
			fund_regular_service.prepareFundRegularData(okMap,model);
			
			target = "/fund/fund_regular_select";
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_regular_select error >> {}",e);
		}
		return target;
	}
	/**
	 * 依照基金定期（不）定額選擇頁表單的資料取得薪轉戶資訊
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getSalaryAccountDataAndSubmitAjax", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult getSalaryAccountDataAndSubmitAjax(@RequestParam Map<String, String> requestParam, Model model) {
		log.debug("getSalaryAccountDataAndSubmitAjax");

		BaseResult baseResult = new BaseResult();
		baseResult.setResult(Boolean.FALSE);
		log.debug("requestParam = " + requestParam);
		
		try {
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			log.debug("getSalaryAccountDataAndSubmitAjax requestParam >> {}", okMap);
			
			String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
			okMap.put("CUSIDN",cusidn);
			
			baseResult = fund_regular_service.getSalaryAccountData(okMap);

			log.debug("baseResult.getData() = {}", baseResult.getData());
			Map<String,Object> bsData = (Map<String,Object>) baseResult.getData();

			if(bsData.get("TOPMSG").equals("0000")) {
				String FCAFEE = (String) bsData.get("FCAFEE");
				String FCAFEEFormat = fund_transfer_service.formatNumberString(FCAFEE,3);
				log.debug(ESAPIUtil.vaildLog("FCAFEEFormat={}"+FCAFEEFormat));
				bsData.put("FCAFEEFormat",FCAFEEFormat);
				
				String SAL01 = (String)bsData.get("SAL01");
				String SAL02 = (String)bsData.get("SAL02");
				String SAL01Format = fund_transfer_service.formatNumberString(SAL01,0);
				String SAL02Format = fund_transfer_service.formatNumberString(SAL02,0);
				bsData.put("SAL01Format",SAL01Format);
				bsData.put("SAL02Format",SAL02Format);
			}
			baseResult.setData(bsData);
			
			if (baseResult != null) {
				baseResult.setResult(Boolean.TRUE);
			}
			
		} catch (Exception e) {
			log.error("getSalaryAccountDataAndSubmitAjax error >> {}", e);
		}
		return baseResult;
	}
	
	/**
	 * 前往定期不定額約定條款頁
	 */
	@RequestMapping(value = "/fund_regular_confirm")
	public String fund_regular_confirm(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		
		log.debug("fund_regular_confirm");
		try{
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			fund_regular_service.prepareFundRegularConfirmData(okMap,model);
			
			target = "fund/fund_regular_confirm";
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_regular_confirm error >> {}",e);
		}
		return target;
	}
	/**
	 * 基金公開說明書同意，前往定期申購手續費頁
	 */
	@RequestMapping(value = "/fund_regular_fee_expose")
	public String fund_regular_fee_expose(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		
		log.debug("fund_regular_fee_expose");
		Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
		BaseResult bs = null;
		try{
			bs = new BaseResult();
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				if(null!=SessionUtil.STEP3_LOCALE_DATA) {
					Map<String,String> result_locale_dataMap=(Map<String,String>)SessionUtil.getAttribute(model, SessionUtil.STEP3_LOCALE_DATA, null);
					for(String key:result_locale_dataMap.keySet()) {
						model.addAttribute(key,result_locale_dataMap.get(key));
					}
					target = "fund/fund_fee_expose";
				}
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP2_LOCALE_DATA, null));
				if(bs == null || !bs.getResult()){
					model.addAttribute(BaseResult.ERROR,bs); 
					return target;
				}
			}else {
			
				String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
				okMap.put("CUSIDN",cusidn);
				
				bs = fund_regular_service.prepareFundRegularFeeExposeData(okMap,model);
				
				if(bs == null || !bs.getResult()){
					model.addAttribute(BaseResult.ERROR,bs);
					SessionUtil.addAttribute(model, SessionUtil.STEP2_LOCALE_DATA, bs);
					return target;
				}
				
				target = "fund/fund_fee_expose";
				
				SessionUtil.addAttribute(model, SessionUtil.STEP3_LOCALE_DATA, model);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_regular_fee_expose error >> {}",e);
		}
		log.debug("/fund_regular_fee_expose getData = {}", bs.getData());
		return target;
	}
	/**
	 * 前往基金定期申購確認頁
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/fund_regular_confirm_final")
	public String fund_regular_confirm_final(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
		log.debug("fund_regular_confirm_final");
		log.debug("fund_regular_confirm_final reqParam SAL01 = " + requestParam.get("SAL01"));
		try{
			
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			log.debug("fund_regular_confirm_final hasLocale = " + hasLocale);
			if (hasLocale) {
				Map<String,String> result_locale_dataMap=(Map<String,String>)SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null);
				for(String key:result_locale_dataMap.keySet()) {
					model.addAttribute(key,result_locale_dataMap.get(key));
				}
				target = "fund/fund_regular_confirm_final";
			}
			else {
				String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
				log.debug("cusidn={}",cusidn);
				String hiddenCUSIDN = WebUtil.hideID(cusidn);
				model.addAttribute("hiddenCUSIDN",hiddenCUSIDN);
				
				BaseResult bs = fund_transfer_service.getN922Data(cusidn);
				
				if(bs == null || !bs.getResult()){
					model.addAttribute(BaseResult.ERROR,bs);
					return target;
				}
				Map<String,Object> bsData = (Map<String,Object>)bs.getData();
				
//				String NAME = (String)bsData.get("NAME");
				
				//20210617修正  若有CUSNAME取CUSNAME 無則為NAME , 不然會是空的 //N922原住民姓名修改由NAME->CUSNAME  
				String NAME = "";
				if(StrUtils.isNotEmpty((String)bsData.get("CUSNAME"))) {
					NAME = (String)bsData.get("CUSNAME");
				}else {
					NAME = (String)bsData.get("NAME");
				}
				
				log.debug("NAME={}",NAME);
				model.addAttribute("NAME",NAME);
				String hiddenNAME = WebUtil.hideusername1Convert(NAME);
				model.addAttribute("hiddenNAME",hiddenNAME);
				
				fund_regular_service.prepareFundRegularConfirmFinalData(okMap,model);
				
				target = "fund/fund_regular_confirm_final";
				
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, model);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_regular_confirm_final error >> {}",e);
		}
		return target;
	}
	/**
	 * 前往基金定期申購結果頁
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/fund_regular_result")
	@Dialog(ADOPID = "C017")
	public String fund_regular_result(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		
		log.debug("fund_regular_result");
		log.debug("fund_regular_result requestParam SAL01 = " + requestParam.get("SAL01"));
		
		Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
		try{
			
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			log.debug("fund_regular_result hasLocale = " + hasLocale);
			if (hasLocale) {
				Map<String,String> result_locale_dataMap=(Map<String,String>)SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null);
				for(String key:result_locale_dataMap.keySet()) {
					model.addAttribute(key,result_locale_dataMap.get(key));
				}
				target = "fund/fund_regular_result";
			}
			else {
				String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
				log.debug("cusidn={}",cusidn);
				okMap.put("CUSIDN",cusidn);
				String hiddenCUSIDN = WebUtil.hideID(cusidn);
				model.addAttribute("hiddenCUSIDN",hiddenCUSIDN);
				
				BaseResult n922BS = fund_transfer_service.getN922Data(cusidn);
				
				if(n922BS == null || !n922BS.getResult()){
					model.addAttribute(BaseResult.ERROR,n922BS);
					return target;
				}
				Map<String,Object> bsData = (Map<String,Object>)n922BS.getData();
				
//				String NAME = (String)bsData.get("NAME");
				//20210617修正  若有CUSNAME取CUSNAME 無則為NAME , 不然會是空的 //N922原住民姓名修改由NAME->CUSNAME  
				String NAME = "";
				if(StrUtils.isNotEmpty((String)bsData.get("CUSNAME"))) {
					NAME = (String)bsData.get("CUSNAME");
				}else {
					NAME = (String)bsData.get("NAME");
				}
				
				log.debug("NAME={}",NAME);
				model.addAttribute("NAME",NAME);
				String hiddenNAME = WebUtil.hideusername1Convert(NAME);
				model.addAttribute("hiddenNAME",hiddenNAME);
				
				String KYCDATE = (String)SessionUtil.getAttribute(model,SessionUtil.KYCDATE,null);
				log.debug("KYCDATE={}",KYCDATE);
				okMap.put("KYCDATE",KYCDATE);
				
				String WEAK = (String)SessionUtil.getAttribute(model,SessionUtil.WEAK,null);
				log.debug("WEAK={}",WEAK);
				okMap.put("WEAK",WEAK);
				
				//寄件者信箱
				String DPMYEMAIL = (String)SessionUtil.getAttribute(model,SessionUtil.DPMYEMAIL,null);
				log.debug("DPMYEMAIL={}",DPMYEMAIL);
				okMap.put("DPMYEMAIL",DPMYEMAIL);
				
				//收件者信箱
				okMap.put("CMTRMAIL",DPMYEMAIL);
				
				String IP = WebUtil.getIpAddr(request);
				log.debug(ESAPIUtil.vaildLog("IP >> " + IP));
				okMap.put("IP",IP);
				
				BaseResult bs = fund_regular_service.processFundRegular(okMap,model);
				
				if(bs == null || !bs.getResult()){
					model.addAttribute(BaseResult.ERROR,bs);
					return target;
				}
				
				target = "fund/fund_regular_result";
				
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, model);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_regular_result error >> {}",e);
		}
		return target;
	}
}