package tw.com.fstop.nnb.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fstop.orm.po.ADMCURRENCY;
import fstop.orm.po.TXNFXRECORD;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.TxnFxRecordDao;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
import tw.com.fstop.tbb.nnb.util.StrUtils;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.OriginalUtil;
import tw.com.fstop.util.StrUtil;

@Service
public class Fcy_Transfer_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Fcy_Acct_Service fcy_acct_service;
	
	@Autowired
	private TxnFxRecordDao txnfxrecorddao;
	
	@Autowired
	private DaoService daoService;
	
	@Autowired
	private I18n i18n;
	
	/**
	 * 原N998判斷是否寄信通知的邏輯
	 * 
	 * String cusidn
	 * @return
	 */
	public boolean chkContains(String cusidn){
		Boolean chk_result = null;
		
		try {
			// 用cusidn查詢資料庫 TXNUSER.DPNOTIFY (e.x: 1,2,3,4,5,6,7)
			String dpnotify = daoService.get_notifyInfo(cusidn);
			if( dpnotify != null ) {
				log.debug("Pay_Expense_Service.dpnotify: " + dpnotify);
				
				// 原N998判斷是否寄信通知的邏輯
				chk_result = OriginalUtil.chkContains("1", dpnotify);
				log.debug("Pay_Expense_Service.chk_result: " + chk_result);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("chkContains error >> {}",e);
		}
		
		return chk_result;
	}
	
	/**
	 * 取得外匯表單驗證用的可交易最小時間
	 * @return
	 */
	public String getSystemDate() {
		String str_SystemDate = "";
		
		try {
			String str_CurrentTime = DateUtil.getTheTime("");
			boolean holiday = daoService.isHoliday();
			
			Calendar cal = Calendar.getInstance();  
			
			if(holiday) {
				cal.set(Calendar.DATE, cal.get(Calendar.DATE) + 1 );
				
			} else if( !holiday && str_CurrentTime.compareTo("092900") > 0 && str_CurrentTime.compareTo("235959") < 0 ) {
				cal.set(Calendar.DATE, cal.get(Calendar.DATE) + 1 );
				
			} else if( !holiday && str_CurrentTime.compareTo("000000") > 0 && str_CurrentTime.compareTo("092900") < 0 ) {
				cal.set(Calendar.DATE, cal.get(Calendar.DATE) );
			}
			
			Date date = cal.getTime();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
			
			str_SystemDate = sdf.format(date);
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		log.debug("Fcy_Transfer_Service.getSystemDate: {}", str_SystemDate);
		return str_SystemDate;
	}
	
	/**
	 * 取得外匯表單驗證用的可交易最大時間
	 * @return
	 */
	public String getSNextYearDay() {
		String sNextYearDay = "";
		
		try {
			// 可預約次日起一年內之交易
			Calendar cal = Calendar.getInstance();  
			cal.set(Calendar.DATE, cal.get(Calendar.DATE) + 1 );
			cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) + 1 );
			Date nextYearDay = cal.getTime();
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
			sNextYearDay = sdf.format(nextYearDay);
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		log.debug("Fcy_Transfer_Service.getSNextYearDay: {}", sNextYearDay);
		return sNextYearDay;
	}
	
	/**
	 * N920取得外匯資料
	 * @param cusidn 使用者統編
	 * @return
	 */
	public BaseResult getN920Data(String cusidn) {
		BaseResult bs = null;
		Map<String,Object> dataMap = new HashMap<String, Object>();
		
		// 轉出帳號
		List<String> acnoList = new ArrayList<String>();
		
		// 轉入帳號
//		List<Map<String,String>> acnRedList = new ArrayList<Map<String,String>>();
//		List<Map<String,String>> acnList = new ArrayList<Map<String,String>>();
		
		// 幣別
		List<Map<String,String>> adcurrencyList = new ArrayList<Map<String,String>>();
		
		try {
			bs = new BaseResult();
			log.trace("getN920Data.cusidn: {}", cusidn);
			
			// N920
			bs = fcy_acct_service.N920_REST(cusidn, Fcy_Acct_Service.FX_OUT_ACNO);
			
			log.info(ESAPIUtil.vaildLog("getN920Data.bsData: " + CodeUtil.toJson(bs.getData())));
			
			// 轉出帳號無任何資料時,產生 Z999 錯誤訊息
			if( bs!=null && bs.getResult() ) {
				List<Map<String,String>> recList = (List<Map<String, String>>) ((Map<String,Object>) bs.getData()).get("REC");
				
				if(recList.isEmpty()) {
					log.error("getN920Data.Z999...");
					bs.setMsgCode("Z999");
					return bs;
				}
				// 轉出帳號
				for(Map<String,String> recMap : recList) {
					String ACN = recMap.get("ACN");
					acnoList.add(ACN);
				}
			}
			
			dataMap = (Map<String,Object>) bs.getData();
			
			log.info(ESAPIUtil.vaildLog("getN920Data.acnoList: " + acnoList));
			dataMap.put("ACNOLIST", acnoList);
			
			// 外幣匯款人身份別
			String custidf = String.valueOf( dataMap.get("CUSTIDF") );
			String custype = "";   
		 	if (custidf.equals("1")) {			 		
		 		custype = "1";
		 	}
		 	else if (custidf.equals("2")) {			 		
		 		custype = "2"; 		
		 	}
		 	else if (custidf.equals("3") || custidf.equals("5")) {			 		
		 		custype = "3"; 		
		 	}
		 	else if (custidf.equals("4") || custidf.equals("6")) {			 		
		 		custype = "4"; 		
		 	}
		 	
		 	log.info(ESAPIUtil.vaildLog("getN920Data.custype: " + custype));
		 	dataMap.put("CUSTYPE", custype);
		 	
		 	
		 	// 轉入帳號
//		 	BaseResult bs2 = new BaseResult();
//		 	bs2 = getAcnoList(cusidn, FX_TRANSFER);
//			bs2 = daoService.getAgreeAcnoList(cusidn, FX_TRANSFER, false);
//			log.trace("getN920Data.bs2: {}", BeanUtils.describe(bs2));
//			if( bs2!=null && bs2.getResult() ) {
//				labelMap = new LinkedHashMap<String,String>();
//				notAgreeLabelMap = new LinkedHashMap<String,String>();
//				
//				labelMap.put("#0", i18n.getMsg("LB.Select_account"));
//				// 下拉選單-約定帳號
//				labelMap.put("#1","--"+i18n.getMsg("LB.Designated_account")+"--");
//	
//				Map<String, Object> bsData = (Map) bs2.getData();
//				log.trace("getN920Data.bs2Data: {}", bsData);
//				ArrayList<Map<String, String>> bsList = (ArrayList<Map<String, String>>) bsData.get("REC");
//				log.trace("getN920Data.bs2List: {}", bsList);
				
				// 轉入帳號選單--20210727
//				for(Map<String,String> bsMap : bsList) {
//					log.trace("getN920Data.bs2Map: {}", bsMap);
//					if ( !"050".equals(bsMap.get("BNKCOD")) && !"".equals(bsMap.get("BNKCOD")))
//						continue;
//					if( bsMap.get("TEXT").indexOf(I18n.getMessage("LB.Designated_account")) >= 0 ) {
//						acnRedList.add(bsMap);
//					}else {
//						acnList.add(bsMap);
//					}
//				}
//			}
			
//			log.info(ESAPIUtil.vaildLog("getN920Data.acnRedList: " + acnRedList));
//			log.trace(ESAPIUtil.vaildLog("getN920Data.acnList: " + acnList));
			
//			dataMap.put("ACNREDLIST", acnRedList);
//			dataMap.put("ACNLIST", acnList);
			
			// 幣別
			List<ADMCURRENCY> admCurrencyList = daoService.getCRYList();
			
			log.trace(ESAPIUtil.vaildLog("getN920Data.admCurrencyList: " + admCurrencyList));
			
			for(ADMCURRENCY po : admCurrencyList) {
				Map<String,String> currencyMap = new HashMap<String, String>();
				currencyMap.put("ADCURRENCY", po.getADCURRENCY());
				currencyMap.put("ADCCYNAME", po.getADCCYNAME());
				adcurrencyList.add(currencyMap);
			}
			
			log.info(ESAPIUtil.vaildLog("getN920Data.adcurrencyList: " + adcurrencyList));
			dataMap.put("ADCURRENCYLIST", adcurrencyList);
		 	
			// 判斷外匯是否可執行即時交易
			boolean isFxBizTime = false;
			if (StringUtils.isNotBlank(cusidn) && cusidn.length() == 10 ) {
				// 個人戶可做即時延長交易時間，0910~1900
				isFxBizTime = daoService.isFxBizFixedTime();
			} else {
				// 企業戶時間不變，同匯出匯款、匯入匯款吃資料庫參數
				isFxBizTime = daoService.isFxBizTime();
			}
			
			log.info(ESAPIUtil.vaildLog("getN920Data.isFxBizTime: " + isFxBizTime));
			dataMap.put("ISFXBIZTIME", isFxBizTime ? "Y" : "N");
			
		 	log.trace(ESAPIUtil.vaildLog("getN920Data.dataMap: " + dataMap));
		 	bs.setData(dataMap);
		 	
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getN920Data error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * 取得轉帳之轉入帳號
	 * @param cusidn
	 * @param type
	 * @return
	 */
	public BaseResult getAcnoList(String cusidn,String type) {
		log.trace("acno>>{} ",cusidn);
		BaseResult bs = null;
		LinkedHashMap<String,String> labelMap = null;
		LinkedHashMap<String,String> notAgreeLabelMap = null;
		ObjectMapper objectMapper = new ObjectMapper();
		String tmpStr = "";
		String agree = "";
		try {
			//  CALL WS api
			bs =new  BaseResult();
//			bs = daoservice.getAgreeAcnoList(cusidn,type,Boolean.TRUE);
			bs = daoService.getAgreeAcnoList(cusidn,type,Boolean.FALSE);
//			後製把table 裡面的資料轉成下拉選單所要的格式
			if(bs.getResult()) {
				labelMap = new LinkedHashMap<String,String>();
				notAgreeLabelMap = new LinkedHashMap<String,String>();
				
				// 12/06目前使用者只有SSL驗證，只能開放約定帳號，等有晶片卡、iKEY驗證後，再開放 常用非約定帳號
				// 下拉選單-請選擇約定/常用非約定帳號，暫時改為請選擇帳號
				// labelMap.put("#0", "-----" + i18n.getMsg("LB.Select_designated_or_common_non-designated_account") + "-----");
				labelMap.put("#0", i18n.getMsg("LB.Select_account"));
				// 下拉選單-約定帳號
				labelMap.put("#1","--"+i18n.getMsg("LB.Designated_account")+"--");
				
				// 下拉選單-常用非約定帳號，暫時改為不顯示
				 notAgreeLabelMap.put("#2","--"+i18n.getMsg("LB.Common_non-designated_account")+"--");
				
				Map<String,Object> data = (Map<String,Object>) bs.getData();
				List<Map<String,String>> table_list = (List<Map<String, String>>) data.get("REC");
				//取出轉入帳號
				for(Map<String,String> tmp :table_list) {
					log.trace("tmp>> {}",tmp);
					log.trace("VALUE>> {}",tmp.get("VALUE"));
					if(!"#".equals(tmp.get("VALUE"))) {
						JsonNode jsonNode = objectMapper.readTree(tmp.get("VALUE"));
						agree = jsonNode.get("AGREE").asText();
					}

					tmpStr = tmp.get("VALUE").toString();
					
//					濾掉原生API 就有放#的部分
					if(tmpStr.indexOf("#") !=-1) {
						continue;
					}
					
					if("1".equals(agree)) {	
						labelMap.put(tmpStr, tmp.get("TEXT"));
					}
					else 
					{
						notAgreeLabelMap.put(tmpStr, tmp.get("TEXT"));
					}
				}
				labelMap.putAll(notAgreeLabelMap);
				//為了查轉入帳號問題
				log.info("INACNO List >>{}",labelMap);
				data.put("REC", labelMap);
				log.trace("data >>{}",data);
				bs.setData(data);
				
			}
//			String json = "{\"_MsgCode\":\"0\",\"_MsgName\":\"\",\"REC\":[{\"_ACN\":\"00160501049\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"00160501049\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀 00160501049\"},{\"_ACN\":\"00162565656\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"00162565656\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀 00162565656\"},{\"_ACN\":\"00162778821\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"00162778821\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀 00162778821\"},{\"_ACN\":\"00162889915\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"00162889915\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀 00162889915\"},{\"_ACN\":\"01013101163\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"01013101163\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀 01013101163\"},{\"_ACN\":\"01062010505\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"01062010505\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀 01062010505\"},{\"_ACN\":\"01062010602\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"01062010602\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀 01062010602\"},{\"_ACN\":\"01062010807\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"01062010807\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀 01062010807\"},{\"_ACN\":\"01062577807\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"01062577807\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀 01062577807\"},{\"_ACN\":\"\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀\"},{\"_ACN\":\"05064600127\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"05064600127\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀 05064600127\"},{\"_ACN\":\"76062567895\",\"_BNKCOD\":\"050\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"76062567895\\\",\\\"BNKCOD\\\":\\\"050\\\"}\",\"_TEXT\":\"050-臺灣企銀 76062567895\"},{\"_ACN\":\"84201234567890\",\"_BNKCOD\":\"005\",\"_AGREE\":\"1\",\"_DPGONAME\":\"\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"AGREE\\\":\\\"1\\\",\\\"ACN\\\":\\\"84201234567890\\\",\\\"BNKCOD\\\":\\\"005\\\"}\",\"_TEXT\":\"005-土地銀行 84201234567890\"},{\"_ACN\":\"99000000000000\",\"_BNKCOD\":\"050\",\"_AGREE\":\"0\",\"_DPGONAME\":\"050-臺灣企銀\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"BNKCOD\\\":\\\"050\\\",\\\"ACN\\\":\\\"99000000000000\\\",\\\"AGREE\\\":\\\"0\\\"}\",\"_TEXT\":\"050-臺灣企銀 99000000000000 050-臺灣企銀\"},{\"_ACN\":\"05052324156\",\"_BNKCOD\":\"050\",\"_AGREE\":\"0\",\"_DPGONAME\":\"test-fx\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"BNKCOD\\\":\\\"050\\\",\\\"ACN\\\":\\\"05052324156\\\",\\\"AGREE\\\":\\\"0\\\"}\",\"_TEXT\":\"050-臺灣企銀 05052324156 test-fx\"},{\"_ACN\":\"99123123123111\",\"_BNKCOD\":\"050\",\"_AGREE\":\"0\",\"_DPGONAME\":\"050-臺灣企銀\",\"_DPPHOTOID\":\"\",\"_VALUE\":\"{\\\"BNKCOD\\\":\\\"050\\\",\\\"ACN\\\":\\\"99123123123111\\\",\\\"AGREE\\\":\\\"0\\\"}\",\"_TEXT\":\"050-臺灣企銀 99123123123111 050-臺灣企銀\"}]}";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getAcnoList error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * 外匯結購售及轉帳 確認頁 處理邏輯
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	public Map<String, String> f_transfer_t_confirm_process(Map<String, String> reqParam, String cusidn) {
		Map<String, String> resultMap = null;
		try {
			log.trace(ESAPIUtil.vaildLog("f_transfer_t_confirm_process.reqParam: {}"+ CodeUtil.toJson(reqParam)));
			resultMap = new  HashMap<String, String>();
			
			//決定是否顯示交易密碼選項
			String str_FGINACNO = reqParam.get("FGINACNO");
			String addAcnErr = "";
			String adagreef = "0";
			String inAcn = "";		
			String hiddenCMSSL = "false";
			
			// 約定/常用帳號
			if (str_FGINACNO.equals("CMDAGREE")) {
				Map<String, String> strMap = CodeUtil.fromJson(reqParam.get("INACNO1"), Map.class);
				hiddenCMSSL = strMap.get("AGREE").equals("0") ? "true" : "false";	// AGREE=0，非約定	
				adagreef = strMap.get("AGREE");		
				inAcn = strMap.get("ACN");
				
			} // 非約定
			else {
				hiddenCMSSL = "true";
				
				inAcn = reqParam.get("INACNO2");
				
				// 加入常用帳號
				if ("1".equals(reqParam.get("ADDACN"))) {
					HashMap<String, String> queryData = new HashMap<String, String>();
					queryData.put("EXECUTEFUNCTION","INSERT");
					queryData.put("DPUSERID", cusidn);
					queryData.put("DPGONAME", "050-臺企銀");
					queryData.put("DPTRACNO", "2");	//2為非約定
					queryData.put("DPTRIBANK", "050");
					queryData.put("DPTRDACNO", inAcn);
					
					try {
						N997_REST(queryData);
					} catch (Exception e) {
						log.error("f_transfer_t_confirm_process.N997_REST error >> {}",e);
					}
				}		
				
			}
					
			String str_acn = reqParam.get("CUSTACC");
			String str_amount = reqParam.get("CURAMT");	
					
			String str_PAYREMIT = reqParam.get("PAYREMIT");
			String str_CURAMT = reqParam.get("CURAMT");	
			String str_InAmt = "";
			String str_OutAmt = "";
			String str_Curr = "";
					
			if ("1".equals(str_PAYREMIT))	
			{
				str_OutAmt = str_CURAMT; 
				str_Curr = reqParam.get("display_PAYCCY");
		  	}
		  	else if ("2".equals(str_PAYREMIT))	
			{
				str_InAmt = str_CURAMT;
				str_Curr = reqParam.get("display_REMITCY");	
			}
				
			if ("".equals(reqParam.get("COMMACC"))) {
				reqParam.put("COMMACC", reqParam.get("CUSTACC"));
			}
			
			if ("".equals(reqParam.get("COMMCCY"))) {
				reqParam.put("COMMCCY", reqParam.get("PAYCCY"));
			}
			
			resultMap = reqParam;
			resultMap.put("addAcnErr", addAcnErr);
			resultMap.put("adagreef", adagreef);
			resultMap.put("inAcn", inAcn);
			resultMap.put("hiddenCMSSL", hiddenCMSSL);
			resultMap.put("str_acn", str_acn);
			resultMap.put("str_amount", str_amount);
			resultMap.put("str_PAYREMIT", str_PAYREMIT);
			resultMap.put("str_CURAMT", str_CURAMT);
			resultMap.put("str_InAmt", str_InAmt);
			resultMap.put("str_OutAmt", str_OutAmt);
			resultMap.put("str_Curr", str_Curr);
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("f_transfer_t_confirm_proces error >> {}",e);
		}
		return resultMap;
	}
	
	/**
	 * 外匯結購售及轉帳 確認頁 處理邏輯
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	public BaseResult f_transfer_t_confirm(Map<String, String> reqParam, String cusidn) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("f_transfer_t_confirm.reqParam: {}"+ CodeUtil.toJson(reqParam)));
			bs = new  BaseResult();
			
			// 日期顯示修改
			if(StrUtil.isNotEmpty(reqParam.get("FGTRDATE")) && (reqParam.get("FGTRDATE").equals("0"))) {
				reqParam.put( "transfer_date" ,DateUtil.getDate("/"));
			}
			
			// 金額顯示修改
			reqParam.put("AMOUNT", NumericUtil.fmtAmount(reqParam.get("AMOUNT"), 2) );
			
			// 放入TXTOKEN
			bs = getTxToken();
			if(bs.getResult()) {
				reqParam.put("TXTOKEN",((Map<String,String>) bs.getData()).get("TXTOKEN"));
				bs.setData(reqParam);
				bs.setResult(Boolean.TRUE);
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("f_transfer_t_confirm error >> {}",e);
		}
		return bs;
	}

	/**
	 * 外匯結購售及轉帳 即時同幣別 結果頁 處理邏輯
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	public BaseResult f_transfer_t_result(String cusidn, Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("f_transfer_t_result.reqParam: {}"+ CodeUtil.toJson(reqParam)));
			bs = new BaseResult();
			
			// 舊Bean.writeLog所需欄位
			reqParam.put("UID", cusidn);
			reqParam.put("CUSIDN", cusidn);
			reqParam.put("ADOPID", "F001");
			
			// 打ms_tw
			bs = F001T_REST(cusidn, reqParam);
			
			// 交易結果
			log.trace("f_transfer_t_result.getResult: {}", bs.getResult());
			
			// 成功的處理
			if(bs.getResult()) {
				// F003轉出帳號帳上餘額下行電文欄位跟上行 轉出/轉入金額 同名，故在ms_tw處理結果改存在TOTBAL
				bs.addData("TOTBAL", NumericUtil.fmtAmount(NumericUtil.addDot(((Map<String, String>) bs.getData()).get("TOTBAL"), 2),2)); // 轉出帳號帳上餘額
				bs.addData("AVAAMT", NumericUtil.fmtAmount(NumericUtil.addDot(((Map<String, String>) bs.getData()).get("AVAAMT"), 2),2)); // 轉出帳號帳上餘額

				// 下行電文沒有但在頁面要顯示、轉換格式
				bs.addData("CMTXTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss")); // 交易時間
				bs.addData("CUSTACC", reqParam.get("CUSTACC")); // 轉出帳號
				bs.addData("str_PAYREMIT", reqParam.get("str_PAYREMIT")); // 轉帳金額 轉出/轉入
				bs.addData("display_PAYCCY", reqParam.get("display_PAYCCY")); // 轉出金額幣別
				bs.addData("CURAMT", reqParam.get("CURAMT")); // 轉出/轉入金額
				bs.addData("BENACC", reqParam.get("BENACC")); // 轉入帳號
				bs.addData("display_REMITCY", reqParam.get("display_REMITCY")); // 轉入金額幣別
				bs.addData("SELFFLAG", reqParam.get("SELFFLAG")); // 自行轉帳才要顯示收款人附言
				bs.addData("MEMO1", reqParam.get("MEMO1")); // 收款人附言
				bs.addData("CMTRMEMO", reqParam.get("CMTRMEMO")); // 交易備註
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("f_transfer_t_result error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * 外匯結購售及轉帳 即時同幣別 結果頁 處理邏輯
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	public BaseResult f_transfer_r_step3(String cusidn, Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("f_transfer_r_step3.reqParam: {}"+ CodeUtil.toJson(reqParam)));
			bs = new BaseResult();
			
			// 舊Bean.writeLog所需欄位
			reqParam.put("UID", cusidn);
			reqParam.put("CUSIDN", cusidn);
			reqParam.put("ADOPID", "F001C");
			
			// 打ms_tw
			bs = F001R_REST(cusidn, reqParam);
			
			// 交易結果
			log.trace("f_transfer_r_step3.getResult: {}", bs.getResult());
			
			// 成功的處理
			if(bs.getResult()) {
				// 在頁面要顯示、轉換格式
				bs.addData("F007ATRAMT", NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("F007ATRAMT"),2)); // 轉入金額
				bs.addData("F007CURAMT", NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("F007CURAMT"),2)); // 轉出金額
				
				// 把SESSION資料也一併回傳
				((Map<String, String>) bs.getData()).putAll(reqParam);
				

			}
			
			// 回傳結果
			log.debug("f_transfer_r_step3.getData: {}", bs.getData());
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("f_transfer_r_step3 error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * 外匯結購售及轉帳 即時不同幣別 結果頁 處理邏輯
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	public BaseResult f_transfer_r_result(String cusidn, Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("f_transfer_r_result.reqParam: {}"+CodeUtil.toJson(reqParam)));
			bs = new BaseResult();
			
			// 舊Bean.writeLog所需欄位
			reqParam.put("UID", cusidn);
			reqParam.put("CUSIDN", cusidn);
			reqParam.put("ADOPID", "F001");
			
			// 打ms_tw
			bs = F001T_REST(cusidn, reqParam);
			
			// 交易結果
			log.trace("f_transfer_r_result.getResult: {}", bs.getResult());
			
			// 成功的處理
			if(bs.getResult()) {
				// 跟上行 轉出/轉入金額 變數同名，故改用TOTBAL
				bs.addData("TOTBAL", NumericUtil.fmtAmount(NumericUtil.addDot(((Map<String, String>) bs.getData()).get("TOTBAL"), 2),2)); // 轉出帳號帳上餘額
				bs.addData("AVAAMT", NumericUtil.fmtAmount(NumericUtil.addDot(((Map<String, String>) bs.getData()).get("AVAAMT"), 2),2)); // 轉出帳號帳上餘額
				
				// 下行電文沒有但在頁面要顯示、轉換格式
				bs.addData("CMTXTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss")); // 交易時間
				bs.addData("CUSTACC", reqParam.get("CUSTACC")); // 轉出帳號
				bs.addData("display_PAYCCY", reqParam.get("display_PAYCCY")); // 轉出金額幣別
				bs.addData("CURAMT", reqParam.get("F007CURAMT")); // 轉出金額
				bs.addData("BENACC", reqParam.get("BENACC")); // 轉入帳號
				bs.addData("display_REMITCY", reqParam.get("display_REMITCY")); // 轉入金額幣別
				bs.addData("ATRAMT", reqParam.get("F007ATRAMT")); // 轉入金額
				bs.addData("RATE", reqParam.get("RATE")); // 匯率
				bs.addData("SELFFLAG", reqParam.get("SELFFLAG")); // 自行轉帳才要顯示收款人附言
				bs.addData("MEMO1", reqParam.get("MEMO1")); // 收款人附言
				bs.addData("CMTRMEMO", reqParam.get("CMTRMEMO")); // 交易備註
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("f_transfer_r_result error >> {}",e);
		}
		return bs;
	}
	
	
	/**
	 * 外匯結購售及轉帳 預約 結果頁 處理邏輯
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	public BaseResult f_transfer_s_result(String cusidn, Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("f_transfer_s_result.reqParam: {}"+CodeUtil.toJson(reqParam)));
			bs = new BaseResult();
			
			// 舊Bean.writeLog所需欄位
			reqParam.put("UID", cusidn);
			reqParam.put("CUSIDN", cusidn);
			reqParam.put("ADOPID", "F001");
			
			// 打ms_tw
			bs = F001S_REST(cusidn, reqParam);
			
			// 交易結果
			log.trace("f_transfer_s_result.getResult: {}", bs.getResult());
			
			// 成功的處理
			if(bs.getResult()) {
				// 預約回傳結果沒有但在頁面要顯示、轉換格式
				bs.addData("CMTXTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss")); // 交易時間
				bs.addData("PAYDATE", reqParam.get("PAYDATE")); // 轉帳日期
				bs.addData("CUSTACC", reqParam.get("CUSTACC")); // 轉出帳號
				bs.addData("BENACC", reqParam.get("BENACC")); // 轉入帳號
				bs.addData("PAYREMIT", reqParam.get("PAYREMIT")); // 轉出金額 or 轉入金額
				bs.addData("display_PAYCCY", reqParam.get("display_PAYCCY")); // 轉出幣別
				bs.addData("display_REMITCY", reqParam.get("display_REMITCY")); // 轉入幣別
				bs.addData("CURAMT", reqParam.get("CURAMT")); // 轉出/轉入金額 
				bs.addData("SELFFLAG", reqParam.get("SELFFLAG")); // 是否是自行轉帳
				bs.addData("MEMO1", reqParam.get("MEMO1")); // 收款人附言
				bs.addData("CMTRMEMO", reqParam.get("CMTRMEMO")); // 交易備註
				
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("f_transfer_s_result error >> {}",e);
		}
		return bs;
	}

	/**
	 * 外匯結購售及轉帳取得外幣資料
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	public List<Map<String, String>> getCurrency(String acn) {
		log.trace("getCurrency...");
		
		List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
		
		try {
			List<ADMCURRENCY> admCurrencyList = daoService.getCRYList();
			log.trace(ESAPIUtil.vaildLog("admCurrencyList: " + admCurrencyList));
			
			if(!StrUtils.isNotEmpty(acn)) {
				return resultList;
			}
			
			String acn_4th = acn.substring(3,4);
			log.trace(ESAPIUtil.vaildLog("acn_4th: {}"+ acn_4th));
			
			String adcurrency = "";
			
			for(ADMCURRENCY po : admCurrencyList) {
				Map<String, String> resultMap = new HashMap<String, String>();
				
				adcurrency = po.getADCURRENCY();
				
				// 外幣帳號
				if (acn_4th.equals("5")) {
					if (adcurrency.equals("TWD")) {
						continue;
					} else {
						resultMap.put("ADCURRENCY", adcurrency);		
						resultMap.put("ADCCYNO", po.getADCCYNO());	
						resultMap.put("ADCCYNAME", po.getADCCYNAME());					
						
						resultList.add(resultMap);
					}
				} else {
					resultMap.put("ADCURRENCY", adcurrency);
					resultMap.put("ADCCYNO", po.getADCCYNO() );		
					resultMap.put("ADCCYNAME", po.getADCCYNAME() );	
					
					resultList.add(resultMap);
				}
			}
			
		} catch (Exception e) {
			log.error("getCurrency.error: {}", e);
		}
		
		log.debug(ESAPIUtil.vaildLog("resultList: " + resultList));
		
		return resultList;
	}
	
	/**
	 * 取得交易單據
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	public BaseResult getFxcert(String adtxno, String cusidn) {
		log.trace("getFxcert...");
		
		BaseResult bs = new BaseResult();
		try {
			Map<String, String> resultMap = new HashMap<String, String>();
			
			// FXCERT
			TXNFXRECORD po = txnfxrecorddao.findByADTXNO(adtxno);
			log.trace(ESAPIUtil.vaildLog("getFxcert.TXNFXRECORD: {}"+CodeUtil.toJson(po)));
			
			// 交易單據資料內容
			String fxcert = po.getFXCERT();
			resultMap = CodeUtil.fromJson(fxcert, Map.class);
			bs.addAllData(resultMap);
			
			// 功能代號決定target
			String adopid = po.getADOPID();
			bs.setNext(adopid);
			
		} catch (Exception e) {
			log.error("getCurrency.error: {}", e);
		}
		
		log.debug(ESAPIUtil.vaildLog("resultMap: {}"+CodeUtil.toJson(bs)));
		
		return bs;
	}
	
	
}
