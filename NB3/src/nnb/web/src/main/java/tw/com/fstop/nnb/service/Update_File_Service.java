package tw.com.fstop.nnb.service;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.commons.net.ftp.FTPClient;
import org.owasp.esapi.SafeFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.web.util.FTPUtil;

@Service
public class Update_File_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private I18n i18n;
	
	/**
	 * 圖片檢核
	 * @param file
	 * @param fileName
	 * @param fileContentType
	 * @param allowImageType
	 * @param allowImageWidth
	 * @param allowImageHeight
	 * @param allowImageSize
	 * @return
	 */
	public Map<String,String> checkImageBase64(String file, String[] allowImageType, int allowImageWidth,int allowImageHeight,int allowImageSize){
		log.info("start to check base64 image");
		Map<String,String> returnMap = new HashMap<String,String>();
		returnMap.put("result","FALSE");
		returnMap.put("summary","");
		returnMap.put("base64","");
		try{
        	log.trace("picFile >> {}", file);
        	String imageType = file.substring(0, file.indexOf(","));
        	String imageBase64 = file.substring(file.indexOf(",") + 1, file.length());
        	log.trace("type >> {}, base64 >> {}", imageType, imageBase64);
        	
        	// 檢查狀態
        	log.info("check image type");
        	Boolean flag = true;
			for(int x=0;x<allowImageType.length;x++){
				if(imageType.indexOf(allowImageType[x]) == -1){
					flag = false;
					break;
				}
			}
			if(flag){
				log.error("image not allow type");
				returnMap.put("summary",i18n.getMsg("LB.X1810"));//圖示格式不符
				return returnMap;
			}
        	log.info("check image type finish");
			
			// 重新繪圖轉檔
        	log.info("write to BufferedImage");
            byte[] imageByte= Base64.getDecoder().decode(imageBase64);
        	log.debug("source size >> {}", imageByte.length);

			if(imageByte.length > allowImageSize){
				log.error("image not allow size");
				returnMap.put("summary",i18n.getMsg("LB.X2505") + allowImageSize/1024 + "KB");//圖示格式不符
				return returnMap;
			}
        	
			ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
			BufferedImage bufferedImage = ImageIO.read(bis);
			bis.close();
            BufferedImage result = new BufferedImage(
            		bufferedImage.getWidth(),
            		bufferedImage.getHeight(),
                    BufferedImage.TYPE_INT_RGB);
            result.createGraphics().drawImage(bufferedImage, 0, 0, Color.WHITE, null);
        	log.info("write to BufferedImage finish");
        	

            //轉回Base64
        	log.info("turn back to base64");
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
			ImageIO.write(result, "jpeg", stream);
        	log.debug("cover size >> {}", stream.toByteArray().length);

			if(stream.toByteArray().length > allowImageSize){
				log.error("image not allow size");
				returnMap.put("summary",i18n.getMsg("LB.X2505") + allowImageSize/1024 + "KB");//圖示格式不符
				stream.close();
				return returnMap;
			}
        	
			String base64 = Base64.getEncoder().encodeToString(stream.toByteArray());
			stream.close();
			log.trace("base64 >> {}", base64);
			returnMap.put("base64", base64);
			returnMap.put("result", "TRUE");
			
		}
		catch(Exception e){
			if(returnMap.get("summary").equals("")) {
				returnMap.put("summary",i18n.getMsg("LB.X2131"));
			}
		}
		return returnMap;
	}
	
	public void uploadImage(String file, String updateFileName) {
		FTPUtil ftputil = new FTPUtil();
		try {
			try {
	        	log.debug("picFile >> {}", file);
//	        	String imageType = file.substring(0, file.indexOf(","));
//	        	String imageBase64 = file.substring(file.indexOf(",") + 1, file.length());
//	        	log.trace("type >> {}, base64 >> {}", imageType, imageBase64);
	            byte[] bytes= Base64.getDecoder().decode(file);
	
				String isSessionID = SpringBeanFactory.getBean("isSessionID");
				//LOCAL
				if("N".equals(isSessionID)){
					FTPClient ftpclient = ftputil.connect("localhost",21,"root","123456",30000);
					boolean result = ftputil.uploadFile(ftpclient,updateFileName,bytes);
					ftputil.finish(ftpclient);
					
				}else{
					Map<String, String> ftp = SpringBeanFactory.getBean("ftp");
					String use = ftp.get("use");
					String ip = ftp.get("ip");
					int port = Integer.parseInt(ftp.get("port"));
					String name = ftp.get("name");
					String pw = ftp.get("password");
					String path = ftp.get("path");
					String decodeString = new String(Base64.getDecoder().decode(pw.getBytes()));
					int timeout = Integer.parseInt(ftp.get("timeout"));
					if("Y".equals(use)){
			        	FTPClient ftpclient = ftputil.connect(ip,port,name,decodeString,timeout);
						ftputil.ChangeToDirectory(ftpclient,path);
						String FILE1 = updateFileName;
						log.debug(ESAPIUtil.vaildLog("FILE1={}"+FILE1));
						
						boolean result = ftputil.uploadFile(ftpclient,FILE1,bytes);
						log.info(ESAPIUtil.vaildLog("「"+FILE1+"」上傳"+ result));
						ftputil.finish(ftpclient);
		
			        }
				}
			}catch(Exception e){
				log.error("uploadImage error >> {}", e.toString());
				log.warn(ESAPIUtil.vaildLog("圖檔上傳失敗"+updateFileName));
			}
		}catch(Exception e) {
			log.error("uploadImage error >> {}", e.toString());
			log.warn(ESAPIUtil.vaildLog("圖檔上傳失敗"+updateFileName));
		}
	}

	public void movefile(String file, String movePath, String moveFileName){
		String validateMovePath = "";
		try
		{
        	log.debug("picFile >> {}", file);
//        	String imageType = file.substring(0, file.indexOf(","));
//        	String imageBase64 = file.substring(file.indexOf(",") + 1, file.length());
//        	log.trace("type >> {}, base64 >> {}", imageType, imageBase64);
            byte[] bytes= Base64.getDecoder().decode(file);

			if(!movePath.equals("") && !moveFileName.equals("")) {
				validateMovePath = ESAPIUtil.vaildPathTraversal2(movePath,moveFileName);
			}
			try {
				ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
				BufferedImage bufferedImage = ImageIO.read(bis);
				bis.close();
				File outputfile = new SafeFile(validateMovePath);
				outputfile.setWritable(true, true);
				outputfile.setReadable(true, true);
				outputfile.setExecutable(true, true);
				ImageIO.write(bufferedImage, "jpeg", outputfile);
				
				log.info(ESAPIUtil.vaildLog("複製圖檔成功:"+validateMovePath));
			}
			catch(Exception RemoveFileEx)
			{
				log.error(RemoveFileEx.getMessage());
				log.warn("搬移圖檔失敗"+validateMovePath);
			}		
		}
		catch(Exception RemoveFileEx)
		{
			log.error(RemoveFileEx.getMessage());
			log.warn("搬移圖檔失敗"+validateMovePath);
		}
	}
	
	/**
	 * 刪除圖片
	 * @param localPath
	 * @return
	 */
	public boolean removefile(String rootPath,String fileName) {
		String validatePath = "";
		try
		{
			//修改 Absolute Path Traversal
			validatePath = ESAPIUtil.GetValidPathPart(rootPath,fileName);
			try {
				Files.delete(Paths.get(validatePath));
				log.warn(ESAPIUtil.vaildLog("刪除圖檔成功:"+validatePath));
				return(true);
			}
			catch(Exception RemoveFileEx)
			{
				log.error(RemoveFileEx.getMessage());
				log.warn("刪除圖檔失敗"+validatePath);
				return(false);
			}		
		}
		catch(Exception RemoveFileEx)
		{
			log.error(RemoveFileEx.getMessage());
			log.warn("刪除圖檔失敗"+validatePath);
			return(false);
		}		
	}
	
	public String writeBinryFile(String file, String movePath, String moveFileName) {
		String validatePath = "";
		String result = "sesscue";
		try
		{
        	log.trace("picFile >> {}", file);
            byte[] bytes= Base64.getDecoder().decode(file);

			if(!movePath.equals("") && !moveFileName.equals("")) {
				validatePath = ESAPIUtil.vaildPathTraversal2(movePath,moveFileName);
			}
			try {
				Files.write(Paths.get(validatePath), bytes);
			}
			catch(Exception RemoveFileEx)
			{
				log.error(RemoveFileEx.getMessage());
				result = "fail";
			}		
		}
		catch(Exception RemoveFileEx)
		{
			log.error(RemoveFileEx.getMessage());
			result = "fail";
		}
		return result;
	}

	public String readBinryFile(String movePath, String moveFileName) {
		String validatePath = "";
		String result = "fail";
		try
		{
			if(!movePath.equals("") && !moveFileName.equals("")) {
				validatePath = ESAPIUtil.vaildPathTraversal2(movePath,moveFileName);
			}
			byte[] bytes;
            try {
                bytes = Files.readAllBytes(Paths.get(validatePath));
    			String base64 = Base64.getEncoder().encodeToString(bytes);
    			log.trace("base64 >> {}", base64);
    			log.debug("FileName >> {}, size >> {}", moveFileName, bytes.length);
    			result = base64;
			}
			catch(Exception RemoveFileEx)
			{
				log.error(RemoveFileEx.getMessage());
			}		
		}
		catch(Exception RemoveFileEx)
		{
			log.error(RemoveFileEx.getMessage());
		}
		return result;
	}
}
