package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N014_REST_RSDATA2 implements Serializable {

	private static final long serialVersionUID = -2739572910855200803L;
	private String LNCCY;// 
	private String LNREFNO;//
	private String LNCOS;//
	private String LNACCNO;//
	private String LNUPDATE;//
	private String PRNDAMT;//
	private String NXTINTD;//
	private String CUSPYDT;//
	
	public String getLNCCY() {
		return LNCCY;
	}
	public void setLNCCY(String lNCCY) {
		LNCCY = lNCCY;
	}
	public String getLNREFNO() {
		return LNREFNO;
	}
	public void setLNREFNO(String lNREFNO) {
		LNREFNO = lNREFNO;
	}
	public String getLNCOS() {
		return LNCOS;
	}
	public void setLNCOS(String lNCOS) {
		LNCOS = lNCOS;
	}
	public String getLNACCNO() {
		return LNACCNO;
	}
	public void setLNACCNO(String lNACCNO) {
		LNACCNO = lNACCNO;
	}
	public String getLNUPDATE() {
		return LNUPDATE;
	}
	public void setLNUPDATE(String lNUPDATE) {
		LNUPDATE = lNUPDATE;
	}
	public String getPRNDAMT() {
		return PRNDAMT;
	}
	public void setPRNDAMT(String pRNDAMT) {
		PRNDAMT = pRNDAMT;
	}
	public String getNXTINTD() {
		return NXTINTD;
	}
	public void setNXTINTD(String nXTINTD) {
		NXTINTD = nXTINTD;
	}
	public String getCUSPYDT() {
		return CUSPYDT;
	}
	public void setCUSPYDT(String cUSPYDT) {
		CUSPYDT = cUSPYDT;
	}
	

}
