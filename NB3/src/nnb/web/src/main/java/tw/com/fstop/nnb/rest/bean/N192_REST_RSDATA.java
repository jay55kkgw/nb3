package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N192_REST_RSDATA extends BaseRestBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1026127888834794574L;
	private String AQTAMT1;//異動後扣款金額１
	private String AQTAMT2;
	private String AQTAMT3;
	private String BQTAMT1;//異動前扣款金額１
	private String BQTAMT2;
	
	private String BQTAMT3;
	private String ASTATUS1;//異動後狀態１
	private String ASTATUS2;
	private String ASTATUS3;
	private String BSTATUS1;//異動前狀態１
	
	private String BSTATUS2;
	private String BSTATUS3;
	private String ATSFACN;//異動後扣款帳號
	private String BTSFACN;//異動前扣款帳號
	private String TIME;//異動時間
	
	private String BFEEAMT;//異動前扣款手續費
	private String AFEEAMT;//異動後扣款手續費
	private String SOURCE;//異動來源
	private String LTD;//異動日期

	
	public String getAQTAMT1() {
		return AQTAMT1;
	}
	public void setAQTAMT1(String aQTAMT1) {
		AQTAMT1 = aQTAMT1;
	}
	public String getAQTAMT2() {
		return AQTAMT2;
	}
	public void setAQTAMT2(String aQTAMT2) {
		AQTAMT2 = aQTAMT2;
	}
	public String getAQTAMT3() {
		return AQTAMT3;
	}
	public void setAQTAMT3(String aQTAMT3) {
		AQTAMT3 = aQTAMT3;
	}
	public String getBQTAMT1() {
		return BQTAMT1;
	}
	public void setBQTAMT1(String bQTAMT1) {
		BQTAMT1 = bQTAMT1;
	}
	public String getBQTAMT2() {
		return BQTAMT2;
	}
	public void setBQTAMT2(String bQTAMT2) {
		BQTAMT2 = bQTAMT2;
	}
	public String getBQTAMT3() {
		return BQTAMT3;
	}
	public void setBQTAMT3(String bQTAMT3) {
		BQTAMT3 = bQTAMT3;
	}
	public String getASTATUS1() {
		return ASTATUS1;
	}
	public void setASTATUS1(String aSTATUS1) {
		ASTATUS1 = aSTATUS1;
	}
	public String getASTATUS2() {
		return ASTATUS2;
	}
	public void setASTATUS2(String aSTATUS2) {
		ASTATUS2 = aSTATUS2;
	}
	public String getASTATUS3() {
		return ASTATUS3;
	}
	public void setASTATUS3(String aSTATUS3) {
		ASTATUS3 = aSTATUS3;
	}
	public String getBSTATUS1() {
		return BSTATUS1;
	}
	public void setBSTATUS1(String bSTATUS1) {
		BSTATUS1 = bSTATUS1;
	}
	public String getBSTATUS2() {
		return BSTATUS2;
	}
	public void setBSTATUS2(String bSTATUS2) {
		BSTATUS2 = bSTATUS2;
	}
	public String getBSTATUS3() {
		return BSTATUS3;
	}
	public void setBSTATUS3(String bSTATUS3) {
		BSTATUS3 = bSTATUS3;
	}
	public String getATSFACN() {
		return ATSFACN;
	}
	public void setATSFACN(String aTSFACN) {
		ATSFACN = aTSFACN;
	}
	public String getBTSFACN() {
		return BTSFACN;
	}
	public void setBTSFACN(String bTSFACN) {
		BTSFACN = bTSFACN;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getAFEEAMT() {
		return AFEEAMT;
	}
	public void setAFEEAMT(String aFEEAMT) {
		AFEEAMT = aFEEAMT;
	}
	public String getSOURCE() {
		return SOURCE;
	}
	public void setSOURCE(String sOURCE) {
		SOURCE = sOURCE;
	}
	public String getLTD() {
		return LTD;
	}
	public void setLTD(String lTD) {
		LTD = lTD;
	}
	public String getBFEEAMT() {
		return BFEEAMT;
	}
	public void setBFEEAMT(String bFEEAMT) {
		BFEEAMT = bFEEAMT;
	}

}
