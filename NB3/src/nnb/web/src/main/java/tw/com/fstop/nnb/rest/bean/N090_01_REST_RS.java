package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N090_01_REST_RS extends BaseRestBean implements Serializable{
	/**
	 * 黃金買進RS
	 */
	private static final long serialVersionUID = 8724524347206873879L;
	
	private String CMQTIME;
	private String TRNDATE;
	private String TRNTIME;
	private String TRNSRC;
	private String TRNTYP;
	private String TRNCOD;
	private String TRNBDT;
	private String ACN;
	private String CUSIDN;
	private String SVACN;
	private String TRNGD;
	private String PRICE;
	private String DISPRICE;
	private String PERDIS;
	private String TRNFEE;
	private String TRNAMT;
	private String DISAMT;
	
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getTRNDATE() {
		return TRNDATE;
	}
	public void setTRNDATE(String tRNDATE) {
		TRNDATE = tRNDATE;
	}
	public String getTRNTIME() {
		return TRNTIME;
	}
	public void setTRNTIME(String tRNTIME) {
		TRNTIME = tRNTIME;
	}
	public String getTRNSRC() {
		return TRNSRC;
	}
	public void setTRNSRC(String tRNSRC) {
		TRNSRC = tRNSRC;
	}
	public String getTRNTYP() {
		return TRNTYP;
	}
	public void setTRNTYP(String tRNTYP) {
		TRNTYP = tRNTYP;
	}
	public String getTRNCOD() {
		return TRNCOD;
	}
	public void setTRNCOD(String tRNCOD) {
		TRNCOD = tRNCOD;
	}
	public String getTRNBDT() {
		return TRNBDT;
	}
	public void setTRNBDT(String tRNBDT) {
		TRNBDT = tRNBDT;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getSVACN() {
		return SVACN;
	}
	public void setSVACN(String sVACN) {
		SVACN = sVACN;
	}
	public String getTRNGD() {
		return TRNGD;
	}
	public void setTRNGD(String tRNGD) {
		TRNGD = tRNGD;
	}
	public String getPRICE() {
		return PRICE;
	}
	public void setPRICE(String pRICE) {
		PRICE = pRICE;
	}
	public String getDISPRICE() {
		return DISPRICE;
	}
	public void setDISPRICE(String dISPRICE) {
		DISPRICE = dISPRICE;
	}
	public String getPERDIS() {
		return PERDIS;
	}
	public void setPERDIS(String pERDIS) {
		PERDIS = pERDIS;
	}
	public String getTRNFEE() {
		return TRNFEE;
	}
	public void setTRNFEE(String tRNFEE) {
		TRNFEE = tRNFEE;
	}
	public String getTRNAMT() {
		return TRNAMT;
	}
	public void setTRNAMT(String tRNAMT) {
		TRNAMT = tRNAMT;
	}
	public String getDISAMT() {
		return DISAMT;
	}
	public void setDISAMT(String dISAMT) {
		DISAMT = dISAMT;
	}
}
