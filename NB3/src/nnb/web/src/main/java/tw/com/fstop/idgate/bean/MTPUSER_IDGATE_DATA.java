package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class MTPUSER_IDGATE_DATA implements Serializable{

	//身分證
	@SerializedName(value = "cusidn")
	private String cusidn;
	//手機號碼
	@SerializedName(value = "mobilephone")
	private String mobilephone;
	//系統別
	@SerializedName(value = "systemflag")
	private String systemflag;
	//轉出帳號
	@SerializedName(value = "txacn")
	private String txacn;
	@SerializedName(value = "binddefault")
	private String binddefault;
	public String getBinddefault() {
		return binddefault;
	}
	public void setBinddefault(String binddefault) {
		this.binddefault = binddefault;
	}
	public String getCusidn() {
		return cusidn;
	}

	public void setCusidn(String cusidn) {
		this.cusidn = cusidn;
	}

	public String getMobilephone() {
		return mobilephone;
	}

	public String getSystemflag() {
		return systemflag;
	}

	public void setSystemflag(String systemflag) {
		this.systemflag = systemflag;
	}

	public void setMobilephone(String mobilephone) {
		this.mobilephone = mobilephone;
	}

	public String getTxacn() {
		return txacn;
	}

	public void setTxacn(String txacn) {
		this.txacn = txacn;
	}

}
