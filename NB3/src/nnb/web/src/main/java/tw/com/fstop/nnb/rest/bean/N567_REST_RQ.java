package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N567_REST_RQ extends BaseRestBean_FX implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4379100255348334145L;
	
	private String CUSIDN;
	private String CMSDATE;//起日
	private String CMEDATE;//迄日
	private String TRNSRC;
	private String USERDATA;//本次未完資料KEY值
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getCMSDATE() {
		return CMSDATE;
	}
	public void setCMSDATE(String cMSDATE) {
		CMSDATE = cMSDATE;
	}
	public String getCMEDATE() {
		return CMEDATE;
	}
	public void setCMEDATE(String cMEDATE) {
		CMEDATE = cMEDATE;
	}
	public String getTRNSRC() {
		return TRNSRC;
	}
	public void setTRNSRC(String tRNSRC) {
		TRNSRC = tRNSRC;
	}
	public String getUSERDATA() {
		return USERDATA;
	}
	public void setUSERDATA(String uSERDATA) {
		USERDATA = uSERDATA;
	}


	
	
	
}
