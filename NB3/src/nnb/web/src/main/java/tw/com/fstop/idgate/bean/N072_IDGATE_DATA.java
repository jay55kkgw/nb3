package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N072_IDGATE_DATA implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -4732001531247405356L;
	
	
	@SerializedName(value = "CUSIDN")
	private String CUSIDN;

	@SerializedName(value = "ACN")
	private String ACN;

	@SerializedName(value = "CARDNUM")
	private String CARDNUM;
	
	@SerializedName(value = "AMOUNT")
	private String AMOUNT;


	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	public String getCARDNUM() {
		return CARDNUM;
	}

	public void setCARDNUM(String cARDNUM) {
		CARDNUM = cARDNUM;
	}

	public String getAMOUNT() {
		return AMOUNT;
	}

	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	
}
