package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N092_REST_RQ extends BaseRestBean_GOLD implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8903439996008056064L;

	private String CUSIDN;
	private String ACN;
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
}
