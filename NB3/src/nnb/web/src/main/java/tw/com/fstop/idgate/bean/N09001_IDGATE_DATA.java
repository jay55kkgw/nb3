package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N09001_IDGATE_DATA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3142045771652095462L;

	@SerializedName(value = "SVACN")
	private String SVACN;

	@SerializedName(value = "ACN")
	private String ACN;

	@SerializedName(value = "TRNGD")
	private String TRNGD;
	
	@SerializedName(value = "PRICE")
	private String PRICE;
	
	@SerializedName(value = "DISPRICE")
	private String DISPRICE;
	
	@SerializedName(value = "PERDIS")
	private String PERDIS;

	@SerializedName(value = "TRNFEE")
	private String TRNFEE;

	@SerializedName(value = "TRNAMT")
	private String TRNAMT;
	
	@SerializedName(value = "DISAMT")
	private String DISAMT;

	@SerializedName(value = "TRNFEE_SIGN")
	private String TRNFEE_SIGN;

	@SerializedName(value = "TRNAMT_SIGN")
	private String TRNAMT_SIGN;
	
	@SerializedName(value = "DISAMT_SIGN")
	private String DISAMT_SIGN;
	
	@SerializedName(value = "TRNCOD")
	private String TRNCOD;

	public String getTRNFEE() {
		return TRNFEE;
	}

	public void setTRNFEE(String tRNFEE) {
		TRNFEE = tRNFEE;
	}

	public String getTRNAMT() {
		return TRNAMT;
	}

	public void setTRNAMT(String tRNAMT) {
		TRNAMT = tRNAMT;
	}

	public String getDISAMT() {
		return DISAMT;
	}

	public void setDISAMT(String dISAMT) {
		DISAMT = dISAMT;
	}

	public String getTRNFEE_SIGN() {
		return TRNFEE_SIGN;
	}

	public void setTRNFEE_SIGN(String tRNFEE_SIGN) {
		TRNFEE_SIGN = tRNFEE_SIGN;
	}

	public String getTRNAMT_SIGN() {
		return TRNAMT_SIGN;
	}

	public void setTRNAMT_SIGN(String tRNAMT_SIGN) {
		TRNAMT_SIGN = tRNAMT_SIGN;
	}

	public String getDISAMT_SIGN() {
		return DISAMT_SIGN;
	}

	public void setDISAMT_SIGN(String dISAMT_SIGN) {
		DISAMT_SIGN = dISAMT_SIGN;
	}

	public String getSVACN() {
		return SVACN;
	}

	public void setSVACN(String sVACN) {
		SVACN = sVACN;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	public String getTRNGD() {
		return TRNGD;
	}

	public void setTRNGD(String tRNGD) {
		TRNGD = tRNGD;
	}

	public String getPRICE() {
		return PRICE;
	}

	public void setPRICE(String pRICE) {
		PRICE = pRICE;
	}

	public String getDISPRICE() {
		return DISPRICE;
	}

	public void setDISPRICE(String dISPRICE) {
		DISPRICE = dISPRICE;
	}

	public String getPERDIS() {
		return PERDIS;
	}

	public void setPERDIS(String pERDIS) {
		PERDIS = pERDIS;
	}

	public String getTRNCOD() {
		return TRNCOD;
	}

	public void setTRNCOD(String tRNCOD) {
		TRNCOD = tRNCOD;
	}
}
