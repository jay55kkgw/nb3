package tw.com.fstop.nnb.spring.controller;

import java.util.Base64;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.Loss_Serving_Service;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtils;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({ SessionUtil.PD,SessionUtil.DOWNLOAD_DATALISTMAP_DATA,SessionUtil.PRINT_DATALISTMAP_DATA,SessionUtil.RESULT_LOCALE_DATA, SessionUtil.CONFIRM_LOCALE_DATA, SessionUtil.RESULT_LOCALE_DATA,SessionUtil.CUSIDN,
	SessionUtil.ADGUID})
@Controller
@RequestMapping(value = "/LOSS/SERVING")
public class Loss_Serving_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Loss_Serving_Service loss_serving_service;
	
	@Autowired
	private HttpServletRequest req;


	//掛失服務
	@RequestMapping(value = "/loss_service")
	public String virtual_account_detail(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		return "/personal_serving_loss/loss_service";
	}
	
	//新臺幣存單掛失
	@RequestMapping(value = "/reporting_loss_deposit_slip")
	public String reporting_loss_deposit_slip(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("reporting_loss_deposit_slip");
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			log.trace(" call_N841 bs.getResult>> {}", bs.getResult());
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("reporting_loss_deposit_slip.cusidn >>{}", cusidn);
				bs = loss_serving_service.reporting_loss_deposit_slip(cusidn, okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("reporting_loss_deposit_slip error >> {}",e);
		} finally {
			if(bs != null && bs.getResult()) 
			{
				target = "/personal_serving_loss/reporting_loss_deposit_slip";
				Map<String, Object> bsData = (Map) bs.getData();
				log.debug("bsData" + bsData);
				List<Map<String,String>> rows = (List<Map<String,String>>) bsData.get("REC");
				log.debug("rows" + rows);
				
				model.addAttribute("reporting_loss_deposit_slip", bs);
			} else {
				bs.setPrevious("/LOSS/SERVING/loss_service"); 
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	//新臺幣存單掛失結果頁
	@RequestMapping(value = "/reporting_loss_deposit_slip_result")
	public String reporting_loss_deposit_slip_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("reporting_loss_deposit_slip_result");
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			String adguid = (String) req.getSession().getAttribute(SessionUtil.ADGUID);
			adguid = StrUtils.isNotEmpty(adguid) ? adguid : "";
			log.trace("test adguid >>>>>"+adguid);
			reqParam.put("ADGUID", adguid);
			String aduserip = WebUtil.getIpAddr(request);
			log.trace(ESAPIUtil.vaildLog("test aduserip >>>>>"+aduserip));
			reqParam.put("ADUSERIP", aduserip);
			
			log.trace(" call_N8503 bs.getResult>> {}", bs.getResult());
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			log.trace(ESAPIUtil.vaildLog("REQPARAM>>>>>>>>>>"+CodeUtil.toJson(reqParam)));
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("reporting_loss_deposit_slip_result.cusidn >>{}", cusidn);
				bs = loss_serving_service.reporting_loss_deposit_slip_result(cusidn, okMap);
//				bs.addData("TIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("reporting_loss_deposit_slip_result error >> {}",e);
		} finally {
			if(bs != null && bs.getResult()) 
			{
				target = "/personal_serving_loss/reporting_loss_deposit_slip_result";
				Map<String, Object> bsData = (Map) bs.getData();
				log.debug("bsData" + bsData);
				List<Map<String,String>> rows = (List<Map<String,String>>) bsData.get("REC");
				log.debug("rows" + rows);
				
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, rows);
				model.addAttribute("reporting_loss_deposit_slip_result", bs);
			} else {
				bs.setPrevious("/LOSS/SERVING/loss_service"); 
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	//外幣存單掛失
	@RequestMapping(value = "/reporting_loss_deposit_slip_fcy")
	public String reporting_loss_deposit_slip_fcy(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("reporting_loss_deposit_slip_fcy");
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			log.trace(" call_N842 bs.getResult>> {}", bs.getResult());
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("reporting_loss_deposit_slip_fcy.cusidn >>{}", cusidn);
				bs = loss_serving_service.reporting_loss_deposit_slip_fcy(cusidn, okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("reporting_loss_deposit_slip_fcy error >> {}",e);
		} finally {
			if(bs != null && bs.getResult()) 
			{
				target = "/personal_serving_loss/reporting_loss_deposit_slip_fcy";
				Map<String, Object> bsData = (Map) bs.getData();
				log.debug("bsData" + bsData);
				List<Map<String,String>> rows = (List<Map<String,String>>) bsData.get("REC");
				log.debug("rows" + rows);
				
				model.addAttribute("reporting_loss_deposit_slip_fcy", bs);
			} else {
				bs.setPrevious("/LOSS/SERVING/loss_service"); 
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	//外幣存單掛失結果頁
	@RequestMapping(value = "/reporting_loss_deposit_slip_fcy_result")
	public String reporting_loss_deposit_slip_fcy_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("reporting_loss_deposit_slip_fcy_result");
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			String adguid = (String) req.getSession().getAttribute(SessionUtil.ADGUID);
			adguid = StrUtils.isNotEmpty(adguid) ? adguid : "";
			reqParam.put("ADGUID", adguid);
			String aduserip = WebUtil.getIpAddr(request);
			reqParam.put("ADUSERIP", aduserip);
			
			log.trace(" call_N8505 bs.getResult>> {}", bs.getResult());
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			log.trace(ESAPIUtil.vaildLog("REQPARAM>>>>>>>>>>"+CodeUtil.toJson(reqParam)));
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("reporting_loss_deposit_slip_fcy_result.cusidn >>{}", cusidn);
				bs = loss_serving_service.reporting_loss_deposit_slip_fcy_result(cusidn, okMap);
//				bs.addData("TIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("reporting_loss_deposit_slip_fcy_result error >> {}",e);
		} finally {
			if(bs != null && bs.getResult()) 
			{
				target = "/personal_serving_loss/reporting_loss_deposit_slip_fcy_result";
				Map<String, Object> bsData = (Map) bs.getData();
				log.debug("bsData" + bsData);
				List<Map<String,String>> rows = (List<Map<String,String>>) bsData.get("REC");
				log.debug("rows" + rows);
				
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, rows);
				model.addAttribute("reporting_loss_deposit_slip_fcy_result", bs);
			} else {
				bs.setPrevious("/LOSS/SERVING/loss_service"); 
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	//新臺幣存摺掛失
	@RequestMapping(value = "/passbook_loss")
	public String passbook_loss(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("passbook_loss");
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			log.trace(" call_N840 bs.getResult>> {}", bs.getResult());
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("passbook_loss.cusidn >>{}", cusidn);
				bs = loss_serving_service.passbook_loss(cusidn, okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("passbook_loss error >> {}",e);
		} finally {
			if(bs != null && bs.getResult()) 
			{
				target = "/personal_serving_loss/passbook_loss";
				Map<String, Object> bsData = (Map) bs.getData();
				log.debug("bsData" + bsData);
				List<Map<String,String>> rows = (List<Map<String,String>>) bsData.get("REC");
				log.debug("rows" + rows);
				
				model.addAttribute("pbook_loss", bs);
			} else {
				bs.setPrevious("/LOSS/SERVING/loss_service"); 
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	//新臺幣存摺掛失結果頁
	@RequestMapping(value = "/passbook_loss_result")
	public String passbook_loss_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("passbook_loss_result");
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			String adguid = (String) req.getSession().getAttribute(SessionUtil.ADGUID);
			adguid = StrUtils.isNotEmpty(adguid) ? adguid : "";
			reqParam.put("ADGUID", adguid);
			String aduserip = WebUtil.getIpAddr(request);
			reqParam.put("ADUSERIP", aduserip);
			
			log.trace(" call_N8501 bs.getResult>> {}", bs.getResult());
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			log.trace(ESAPIUtil.vaildLog("REQPARAM>>>>>>>>>>"+CodeUtil.toJson(reqParam)));
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("passbook_loss_result.cusidn >>{}", cusidn);
				bs = loss_serving_service.passbook_loss_result(cusidn, okMap);
//				bs.addData("TIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("passbook_loss_result error >> {}",e);
		} finally {
			if(bs != null && bs.getResult()) 
			{
				target = "/personal_serving_loss/passbook_loss_result";
				Map<String, Object> bsData = (Map) bs.getData();
				log.debug("bsData" + bsData);
				List<Map<String,String>> rows = (List<Map<String,String>>) bsData.get("REC");
				log.debug("rows" + rows);
				
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, rows);
				model.addAttribute("pbook_loss_result", bs);
			} else {
				bs.setPrevious("/LOSS/SERVING/loss_service"); 
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	//外幣存摺掛失
	@RequestMapping(value = "/fcy_passbook_loss")
	public String fcy_passbook_loss(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("fcy_passbook_loss");
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			log.trace(" call_N843 bs.getResult>> {}", bs.getResult());
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("fcy_passbook_loss.cusidn >>{}", cusidn);
				bs = loss_serving_service.fcy_passbook_loss(cusidn, okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fcy_passbook_loss error >> {}",e);
		} finally {
			if(bs != null && bs.getResult()) 
			{
				target = "/personal_serving_loss/fcy_passbook_loss";
				Map<String, Object> bsData = (Map) bs.getData();
				log.debug("bsData" + bsData);
				List<Map<String,String>> rows = (List<Map<String,String>>) bsData.get("REC");
				log.debug("rows" + rows);
				
				model.addAttribute("fcy_passbook_loss", bs);
			} else {
				bs.setPrevious("/LOSS/SERVING/loss_service"); 
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	//外幣存摺掛失結果頁
	@RequestMapping(value = "/fcy_passbook_loss_result")
	public String fcy_passbook_loss_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("fcy_passbook_loss_result");
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			String adguid = (String) req.getSession().getAttribute(SessionUtil.ADGUID);
			adguid = StrUtils.isNotEmpty(adguid) ? adguid : "";
			reqParam.put("ADGUID", adguid);
			String aduserip = WebUtil.getIpAddr(request);
			reqParam.put("ADUSERIP", aduserip);
			
			log.trace(" call_N8507 bs.getResult>> {}", bs.getResult());
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			log.trace(ESAPIUtil.vaildLog("REQPARAM>>>>>>>>>>"+CodeUtil.toJson(reqParam)));
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("fcy_passbook_loss_result.cusidn >>{}", cusidn);
				bs = loss_serving_service.fcy_passbook_loss_result(cusidn, okMap);
//				bs.addData("TIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fcy_passbook_loss_result error >> {}",e);
		} finally {
			if(bs != null && bs.getResult()) 
			{
				target = "/personal_serving_loss/fcy_passbook_loss_result";
				Map<String, Object> bsData = (Map) bs.getData();
				log.debug("bsData" + bsData);
				List<Map<String,String>> rows = (List<Map<String,String>>) bsData.get("REC");
				log.debug("rows" + rows);
				
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, rows);
				model.addAttribute("fcy_passbook_loss_result", bs);
			} else {
				bs.setPrevious("/LOSS/SERVING/loss_service"); 
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	//黃金存摺掛失
	@RequestMapping(value = "/gold_passbook_loss")
	public String gold_passbook_loss(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("gold_passbook_loss");
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			log.trace(" call_N845 bs.getResult>> {}", bs.getResult());
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("gold_passbook_loss.cusidn >>{}", cusidn);
				bs = loss_serving_service.gold_passbook_loss(cusidn, okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_passbook_loss error >> {}",e);
		} finally {
			if(bs != null && bs.getResult()) 
			{
				target = "/personal_serving_loss/gold_passbook_loss";
				Map<String, Object> bsData = (Map) bs.getData();
				log.debug("bsData" + bsData);
				List<Map<String,String>> rows = (List<Map<String,String>>) bsData.get("REC");
				log.debug("rows" + rows);		
				model.addAttribute("gold_passbook_loss", bs);
			} else {
				bs.setPrevious("/LOSS/SERVING/loss_service"); 
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	//黃金存摺掛失結果頁
	@RequestMapping(value = "/gold_passbook_loss_result")
	public String gold_passbook_loss_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("gold_passbook_loss_result");
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			String adguid = (String) req.getSession().getAttribute(SessionUtil.ADGUID);
			adguid = StrUtils.isNotEmpty(adguid) ? adguid : "";
			reqParam.put("ADGUID", adguid);
			String aduserip = WebUtil.getIpAddr(request);
			reqParam.put("ADUSERIP", aduserip);
			
			log.trace(" call_N850A bs.getResult>> {}", bs.getResult());
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			log.trace(ESAPIUtil.vaildLog("REQPARAM>>>>>>>>>>"+CodeUtil.toJson(reqParam)));
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("gold_passbook_loss_result.cusidn >>{}", cusidn);
				bs = loss_serving_service.gold_passbook_loss_result(cusidn, okMap);
//				bs.addData("TIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gold_passbook_loss_result error >> {}",e);
		} finally {
			if(bs != null && bs.getResult()) 
			{
				target = "/personal_serving_loss/gold_passbook_loss_result";
				Map<String, Object> bsData = (Map) bs.getData();
				log.debug("bsData" + bsData);
				List<Map<String,String>> rows = (List<Map<String,String>>) bsData.get("REC");
				log.debug("rows" + rows);
				
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, rows);
				model.addAttribute("gold_passbook_loss_result", bs);
			} else {
				bs.setPrevious("/LOSS/SERVING/loss_service"); 
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	//金融卡掛失
	@RequestMapping(value = "/debitcard_loss")
	public String debitcard_loss(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("debitcard_loss");
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			log.trace(" call_N844 bs.getResult>> {}", bs.getResult());
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("debitcard_loss.cusidn >>{}", cusidn);
				bs = loss_serving_service.debitcard_loss(cusidn, okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("debitcard_loss error >> {}",e);
		} finally {
			if(bs != null && bs.getResult()) 
			{
				target = "/personal_serving_loss/debitcard_loss";
				Map<String, Object> bsData = (Map) bs.getData();
				log.debug("bsData" + bsData);
				List<Map<String,String>> rows = (List<Map<String,String>>) bsData.get("REC");
				log.debug("rows" + rows);
				
				model.addAttribute("debitcard_loss", bs);
			} else {
				bs.setPrevious("/LOSS/SERVING/loss_service"); 
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	//金融卡掛失結果頁
	@RequestMapping(value = "/debitcard_loss_result")
	public String debitcard_loss_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("debitcard_loss_result");
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			String adguid = (String) req.getSession().getAttribute(SessionUtil.ADGUID);
			adguid = StrUtils.isNotEmpty(adguid) ? adguid : "";
			reqParam.put("ADGUID", adguid);
			String aduserip = WebUtil.getIpAddr(request);
			reqParam.put("ADUSERIP", aduserip);
			
			log.trace(" call_N8509 bs.getResult>> {}", bs.getResult());
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			log.trace(ESAPIUtil.vaildLog("REQPARAM>>>>>>>>>>"+CodeUtil.toJson(reqParam)));
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("debitcard_loss_result.cusidn >>{}", cusidn);
				bs = loss_serving_service.debitcard_loss_result(cusidn, okMap);
//				bs.addData("TIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("debitcard_loss_result error >> {}",e);
		} finally {
			if(bs != null && bs.getResult()) 
			{
				target = "/personal_serving_loss/debitcard_loss_result";
				Map<String, Object> bsData = (Map) bs.getData();
				log.debug("bsData" + bsData);
				List<Map<String,String>> rows = (List<Map<String,String>>) bsData.get("REC");
				log.debug("rows" + rows);
				
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, rows);
				model.addAttribute("debitcard_loss_result", bs);
			} else {
				bs.setPrevious("/LOSS/SERVING/loss_service"); 
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	//新臺幣存單印鑑掛失
	@RequestMapping(value = "/deposit_slip_seal_loss")
	public String deposit_slip_seal_loss(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("deposit_slip_seal_loss");
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			log.trace(" call_N841 bs.getResult>> {}", bs.getResult());
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("debitcard_loss.cusidn >>{}", cusidn);
				bs = loss_serving_service.deposit_slip_seal_loss(cusidn, okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("deposit_slip_seal_loss error >> {}",e);
		} finally {
			if(bs != null && bs.getResult()) 
			{
				target = "/personal_serving_loss/deposit_slip_seal_loss";
				Map<String, Object> bsData = (Map) bs.getData();
				log.debug("bsData" + bsData);
				List<Map<String,String>> rows = (List<Map<String,String>>) bsData.get("labelList");
				log.debug("rows" + rows);
				
				model.addAttribute("deposit_slip_seal_loss", bs);
			} else {
				bs.setPrevious("/LOSS/SERVING/loss_service"); 
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	//新臺幣存單印鑑掛失結果頁
	@RequestMapping(value = "/deposit_slip_seal_loss_result")
	public String deposit_slip_seal_loss_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("deposit_slip_seal_loss_result");
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			String adguid = (String) req.getSession().getAttribute(SessionUtil.ADGUID);
			adguid = StrUtils.isNotEmpty(adguid) ? adguid : "";
			reqParam.put("ADGUID", adguid);
			String aduserip = WebUtil.getIpAddr(request);
			reqParam.put("ADUSERIP", aduserip);
			
			log.trace(" call_N8504 bs.getResult>> {}", bs.getResult());
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			log.trace(ESAPIUtil.vaildLog("REQPARAM>>>>>>>>>>"+CodeUtil.toJson(reqParam)));
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("deposit_slip_seal_loss_result.cusidn >>{}", cusidn);
				bs = loss_serving_service.deposit_slip_seal_loss_result(cusidn, okMap);
//				bs.addData("TIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("deposit_slip_seal_loss_result error >> {}",e);
		} finally {
			if(bs != null && bs.getResult()) 
			{
				target = "/personal_serving_loss/deposit_slip_seal_loss_result";
				Map<String, Object> bsData = (Map) bs.getData();
				log.debug("bsData" + bsData);
				List<Map<String,String>> rows = (List<Map<String,String>>) bsData.get("REC");
				log.debug("rows" + rows);
				
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, rows);
				model.addAttribute("deposit_slip_seal_loss_result", bs);
			} else {
				bs.setPrevious("/LOSS/SERVING/loss_service"); 
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	//外幣存單掛失
	@RequestMapping(value = "/f_deposit_slip_seal_loss")
	public String f_deposit_slip_seal_loss(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("f_deposit_slip_seal_loss");
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			log.trace(" call_N842 bs.getResult>> {}", bs.getResult());
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("f_deposit_slip_seal_loss.cusidn >>{}", cusidn);
				bs = loss_serving_service.f_deposit_slip_seal_loss(cusidn, okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("f_deposit_slip_seal_loss error >> {}",e);
		} finally {
			if(bs != null && bs.getResult()) 
			{
				target = "/personal_serving_loss/f_deposit_slip_seal_loss";
				Map<String, Object> bsData = (Map) bs.getData();
				log.debug("bsData" + bsData);
				List<Map<String,String>> rows = (List<Map<String,String>>) bsData.get("REC");
				log.debug("rows" + rows);
				
				model.addAttribute("f_deposit_slip_seal_loss", bs);
			} else {
				bs.setPrevious("/LOSS/SERVING/loss_service"); 
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	//外幣存單印鑑掛失結果頁
	@RequestMapping(value = "/f_deposit_slip_seal_loss_result")
	public String f_deposit_slip_seal_loss_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("f_deposit_slip_seal_loss_result");
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			String adguid = (String) req.getSession().getAttribute(SessionUtil.ADGUID);
			adguid = StrUtils.isNotEmpty(adguid) ? adguid : "";
			reqParam.put("ADGUID", adguid);
			String aduserip = WebUtil.getIpAddr(request);
			reqParam.put("ADUSERIP", aduserip);
			
			log.trace(" call_N8506 bs.getResult>> {}", bs.getResult());
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			log.trace(ESAPIUtil.vaildLog("REQPARAM>>>>>>>>>>"+CodeUtil.toJson(reqParam)));
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("f_deposit_slip_seal_loss_result.cusidn >>{}", cusidn);
				bs = loss_serving_service.f_deposit_slip_seal_loss_result(cusidn, okMap);
//				bs.addData("TIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("f_deposit_slip_seal_loss_result error >> {}",e);
		} finally {
			if(bs != null && bs.getResult()) 
			{
				target = "/personal_serving_loss/f_deposit_slip_seal_loss_result";
				Map<String, Object> bsData = (Map) bs.getData();
				log.debug("bsData" + bsData);
				List<Map<String,String>> rows = (List<Map<String,String>>) bsData.get("REC");
				log.debug("rows" + rows);
				
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, rows);
				model.addAttribute("f_deposit_slip_seal_loss_result", bs);
			} else {
				bs.setPrevious("/LOSS/SERVING/loss_service"); 
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	//新臺幣存摺印鑑掛失
	@RequestMapping(value = "/passbook_seal_loss")
	public String passbook_seal_loss(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("passbook_seal_loss");
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			log.trace(" call_N840 bs.getResult>> {}", bs.getResult());
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("passbook_seal_loss.cusidn >>{}", cusidn);
				bs = loss_serving_service.passbook_seal_loss(cusidn, okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("passbook_seal_loss error >> {}",e);
		} finally {
			if(bs != null && bs.getResult()) 
			{
				target = "/personal_serving_loss/passbook_seal_loss";
				Map<String, Object> bsData = (Map) bs.getData();
				log.debug("bsData" + bsData);
				List<Map<String,String>> rows = (List<Map<String,String>>) bsData.get("REC");
				log.debug("rows" + rows);
				
				model.addAttribute("pbook_seal_loss", bs);
			} else {
				bs.setPrevious("/LOSS/SERVING/loss_service"); 
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	//新臺幣存款印鑑掛失結果頁
	@RequestMapping(value = "/passbook_seal_loss_result")
	public String passbook_seal_loss_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("passbook_loss_result");
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			String adguid = (String) req.getSession().getAttribute(SessionUtil.ADGUID);
			adguid = StrUtils.isNotEmpty(adguid) ? adguid : "";
			reqParam.put("ADGUID", adguid);
			String aduserip = WebUtil.getIpAddr(request);
			reqParam.put("ADUSERIP", aduserip);
			
			log.trace(" call_N8502 bs.getResult>> {}", bs.getResult());
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			log.trace(ESAPIUtil.vaildLog("REQPARAM>>>>>>>>>>"+CodeUtil.toJson(reqParam)));
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("passbook_seal_loss_result.cusidn >>{}", cusidn);
				bs = loss_serving_service.passbook_seal_loss_result(cusidn, okMap);
//				bs.addData("TIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("passbook_seal_loss_result error >> {}",e);
		} finally {
			if(bs != null && bs.getResult()) 
			{
				target = "/personal_serving_loss/passbook_seal_loss_result";
				Map<String, Object> bsData = (Map) bs.getData();
				log.debug("bsData" + bsData);
				List<Map<String,String>> rows = (List<Map<String,String>>) bsData.get("REC");
				log.debug("rows" + rows);
				
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, rows);
				model.addAttribute("pbook_seal_loss_result", bs);
			} else {
				bs.setPrevious("/LOSS/SERVING/loss_service"); 
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	//外幣存摺印鑑掛失
	@RequestMapping(value = "/f_passbook_seal_loss")
	public String f_passbook_seal_loss(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("f_passbook_seal_loss");
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			log.trace(" call_N843 bs.getResult>> {}", bs.getResult());
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				log.trace(ESAPIUtil.vaildLog("locale>>{}"+ okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("f_passbook_seal_loss.cusidn >>{}", cusidn);
				bs = loss_serving_service.f_passbook_seal_loss(cusidn, okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("f_passbook_seal_loss error >> {}",e);
		} finally {
			if(bs != null && bs.getResult()) 
			{
				target = "/personal_serving_loss/f_passbook_seal_loss";
				Map<String, Object> bsData = (Map) bs.getData();
				log.debug("bsData" + bsData);
				List<Map<String,String>> rows = (List<Map<String,String>>) bsData.get("REC");
				log.debug("rows" + rows);
				
				model.addAttribute("f_passbook_seal_loss", bs);
			} else {
				bs.setPrevious("/LOSS/SERVING/loss_service"); 
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	//外幣存摺印鑑掛失結果頁
	@RequestMapping(value = "/f_passbook_seal_loss_result")
	public String f_passbook_seal_loss_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("f_passbook_seal_loss_result");
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			String adguid = (String) req.getSession().getAttribute(SessionUtil.ADGUID);
			adguid = StrUtils.isNotEmpty(adguid) ? adguid : "";
			reqParam.put("ADGUID", adguid);
			String aduserip = WebUtil.getIpAddr(request);
			reqParam.put("ADUSERIP", aduserip);
			
			log.trace(" call_N8508 bs.getResult>> {}", bs.getResult());
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			log.trace(ESAPIUtil.vaildLog("REQPARAM>>>>>>>>>>"+CodeUtil.toJson(reqParam)));
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
//				log.trace("locale>>" + okMap.get("locale"));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("f_passbook_seal_loss_result.cusidn >>{}", cusidn);
				bs = loss_serving_service.f_passbook_seal_loss_result(cusidn, okMap);
//				bs.addData("TIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("f_passbook_seal_loss_result error >> {}",e);
		} finally {
			if(bs != null && bs.getResult()) 
			{
				target = "/personal_serving_loss/f_passbook_seal_loss_result";
				Map<String, Object> bsData = (Map) bs.getData();
				log.debug("bsData" + bsData);
				List<Map<String,String>> rows = (List<Map<String,String>>) bsData.get("REC");
				log.debug("rows" + rows);
				
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, rows);
				model.addAttribute("f_passbook_seal_loss_result", bs);
			} else {
				bs.setPrevious("/LOSS/SERVING/loss_service"); 
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	//黃金存摺印鑑掛失
	@RequestMapping(value = "/g_passbook_seal_loss")
	public String g_passbook_seal_loss(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("g_passbook_seal_loss");
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			log.trace(" call_N845 bs.getResult>> {}", bs.getResult());
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
//				log.trace("locale>>" + okMap.get("locale"));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("g_passbook_seal_loss.cusidn >>{}", cusidn);
				bs = loss_serving_service.g_passbook_seal_loss(cusidn, okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("g_passbook_seal_loss error >> {}",e);
		} finally {
			if(bs != null && bs.getResult()) 
			{
				target = "/personal_serving_loss/g_passbook_seal_loss";
				Map<String, Object> bsData = (Map) bs.getData();
				log.debug("bsData" + bsData);
				List<Map<String,String>> rows = (List<Map<String,String>>) bsData.get("REC");
				log.debug("rows" + rows);
				
				model.addAttribute("g_passbook_seal_loss", bs);
			} else {
				bs.setPrevious("/LOSS/SERVING/loss_service"); 
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	//黃金存摺印鑑掛失結果頁
	@RequestMapping(value = "/g_passbook_seal_loss_result")
	public String g_passbook_seal_loss_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("g_passbook_seal_loss_result");
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			String adguid = (String) req.getSession().getAttribute(SessionUtil.ADGUID);
			adguid = StrUtils.isNotEmpty(adguid) ? adguid : "";
			reqParam.put("ADGUID", adguid);
			String aduserip = WebUtil.getIpAddr(request);
			reqParam.put("ADUSERIP", aduserip);
			
			log.trace(" call_N850A bs.getResult>> {}", bs.getResult());
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			log.trace(ESAPIUtil.vaildLog("REQPARAM>>>>>>>>>>" + reqParam));
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
//				log.trace("locale>>" + okMap.get("locale"));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				log.debug("g_passbook_seal_loss_result.cusidn >>{}", cusidn);
				bs = loss_serving_service.g_passbook_seal_loss_result(cusidn, okMap);
//				bs.addData("TIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("g_passbook_seal_loss_result error >> {}",e);
		} finally {
			if(bs != null && bs.getResult()) 
			{
				target = "/personal_serving_loss/g_passbook_seal_loss_result";
				Map<String, Object> bsData = (Map) bs.getData();
				log.debug("bsData" + bsData);
				List<Map<String,String>> rows = (List<Map<String,String>>) bsData.get("REC");
				log.debug("rows" + rows);
				
				SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, rows);
				model.addAttribute("g_passbook_seal_loss_result", bs);
			} else {
				bs.setPrevious("/LOSS/SERVING/loss_service"); 
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
}
