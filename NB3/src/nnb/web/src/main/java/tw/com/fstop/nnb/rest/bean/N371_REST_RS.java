package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * N371電文RS
 */
public class N371_REST_RS extends BaseRestBean implements Serializable{
	private static final long serialVersionUID = 6791228050721094802L;
	
	private String TRNDATE;
	private String TRNTIME;
	private String TYPE;
	private String TRANSCODE;
	private String CDNO;
	private String TRADEDATE;
	private String INTRANSCODE;
	private String UNIT;
	private String FCA1;
	private String AMT3;
	private String FCA2;
	private String AMT5;
	private String OUTACN;
	private String INTSACN;
	private String BILLSENDMODE;
	private String SSLTXNO;
	private String FUNDAMT;
	private String CRY;
	private String B_TOTBAL;
	private String B_TOTBAL_S;
	private String B_AVLBAL;
	private String B_AVLBAL_S;
	private String AMOUNT;
	private String A_TOTBAL;
	private String A_TOTBAL_S;
	private String A_AVLBAL;
	private String A_AVLBAL_S;
	private String DATE;
	private String TIME;
	private String CUSIDN;
	private String CUSNAME;
	private String HTELPHONE;
	private String OTELPHONE;
	private String FUNCUR;
	private String SHORTTRADE;
	private String SHORTTUNIT;
	private String MAC;
	private String RSKATT;
	private String RRSK;
	private String CMQTIME;
	
	public String getTRNDATE(){
		return TRNDATE;
	}
	public void setTRNDATE(String tRNDATE){
		TRNDATE = tRNDATE;
	}
	public String getTRNTIME(){
		return TRNTIME;
	}
	public void setTRNTIME(String tRNTIME){
		TRNTIME = tRNTIME;
	}
	public String getTYPE(){
		return TYPE;
	}
	public void setTYPE(String tYPE){
		TYPE = tYPE;
	}
	public String getTRANSCODE(){
		return TRANSCODE;
	}
	public void setTRANSCODE(String tRANSCODE){
		TRANSCODE = tRANSCODE;
	}
	public String getTRADEDATE(){
		return TRADEDATE;
	}
	public void setTRADEDATE(String tRADEDATE){
		TRADEDATE = tRADEDATE;
	}
	public String getINTRANSCODE(){
		return INTRANSCODE;
	}
	public void setINTRANSCODE(String iNTRANSCODE){
		INTRANSCODE = iNTRANSCODE;
	}
	public String getUNIT(){
		return UNIT;
	}
	public void setUNIT(String uNIT){
		UNIT = uNIT;
	}
	public String getFCA1(){
		return FCA1;
	}
	public void setFCA1(String fCA1){
		FCA1 = fCA1;
	}
	public String getAMT3(){
		return AMT3;
	}
	public void setAMT3(String aMT3){
		AMT3 = aMT3;
	}
	public String getFCA2(){
		return FCA2;
	}
	public void setFCA2(String fCA2){
		FCA2 = fCA2;
	}
	public String getAMT5(){
		return AMT5;
	}
	public void setAMT5(String aMT5){
		AMT5 = aMT5;
	}
	public String getOUTACN(){
		return OUTACN;
	}
	public void setOUTACN(String oUTACN){
		OUTACN = oUTACN;
	}
	public String getINTSACN(){
		return INTSACN;
	}
	public void setINTSACN(String iNTSACN){
		INTSACN = iNTSACN;
	}
	public String getBILLSENDMODE(){
		return BILLSENDMODE;
	}
	public void setBILLSENDMODE(String bILLSENDMODE){
		BILLSENDMODE = bILLSENDMODE;
	}
	public String getSSLTXNO(){
		return SSLTXNO;
	}
	public void setSSLTXNO(String sSLTXNO){
		SSLTXNO = sSLTXNO;
	}
	public String getFUNDAMT(){
		return FUNDAMT;
	}
	public void setFUNDAMT(String fUNDAMT){
		FUNDAMT = fUNDAMT;
	}
	public String getCRY(){
		return CRY;
	}
	public void setCRY(String cRY){
		CRY = cRY;
	}
	public String getB_TOTBAL(){
		return B_TOTBAL;
	}
	public void setB_TOTBAL(String b_TOTBAL){
		B_TOTBAL = b_TOTBAL;
	}
	public String getB_TOTBAL_S(){
		return B_TOTBAL_S;
	}
	public void setB_TOTBAL_S(String b_TOTBAL_S){
		B_TOTBAL_S = b_TOTBAL_S;
	}
	public String getB_AVLBAL(){
		return B_AVLBAL;
	}
	public void setB_AVLBAL(String b_AVLBAL){
		B_AVLBAL = b_AVLBAL;
	}
	public String getB_AVLBAL_S(){
		return B_AVLBAL_S;
	}
	public void setB_AVLBAL_S(String b_AVLBAL_S){
		B_AVLBAL_S = b_AVLBAL_S;
	}
	public String getAMOUNT(){
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT){
		AMOUNT = aMOUNT;
	}
	public String getA_TOTBAL(){
		return A_TOTBAL;
	}
	public void setA_TOTBAL(String a_TOTBAL){
		A_TOTBAL = a_TOTBAL;
	}
	public String getA_TOTBAL_S(){
		return A_TOTBAL_S;
	}
	public void setA_TOTBAL_S(String a_TOTBAL_S){
		A_TOTBAL_S = a_TOTBAL_S;
	}
	public String getA_AVLBAL(){
		return A_AVLBAL;
	}
	public void setA_AVLBAL(String a_AVLBAL){
		A_AVLBAL = a_AVLBAL;
	}
	public String getA_AVLBAL_S(){
		return A_AVLBAL_S;
	}
	public void setA_AVLBAL_S(String a_AVLBAL_S){
		A_AVLBAL_S = a_AVLBAL_S;
	}
	public String getDATE(){
		return DATE;
	}
	public void setDATE(String dATE){
		DATE = dATE;
	}
	public String getTIME(){
		return TIME;
	}
	public void setTIME(String tIME){
		TIME = tIME;
	}
	public String getCUSIDN(){
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN){
		CUSIDN = cUSIDN;
	}
	public String getCUSNAME(){
		return CUSNAME;
	}
	public void setCUSNAME(String cUSNAME){
		CUSNAME = cUSNAME;
	}
	public String getHTELPHONE(){
		return HTELPHONE;
	}
	public void setHTELPHONE(String hTELPHONE){
		HTELPHONE = hTELPHONE;
	}
	public String getOTELPHONE(){
		return OTELPHONE;
	}
	public void setOTELPHONE(String oTELPHONE){
		OTELPHONE = oTELPHONE;
	}
	public String getFUNCUR(){
		return FUNCUR;
	}
	public void setFUNCUR(String fUNCUR){
		FUNCUR = fUNCUR;
	}
	public String getSHORTTRADE(){
		return SHORTTRADE;
	}
	public void setSHORTTRADE(String sHORTTRADE){
		SHORTTRADE = sHORTTRADE;
	}
	public String getSHORTTUNIT(){
		return SHORTTUNIT;
	}
	public void setSHORTTUNIT(String sHORTTUNIT){
		SHORTTUNIT = sHORTTUNIT;
	}
	public String getMAC(){
		return MAC;
	}
	public void setMAC(String mAC){
		MAC = mAC;
	}
	public String getRSKATT(){
		return RSKATT;
	}
	public void setRSKATT(String rSKATT){
		RSKATT = rSKATT;
	}
	public String getRRSK(){
		return RRSK;
	}
	public void setRRSK(String rRSK){
		RRSK = rRSK;
	}
	public String getCMQTIME(){
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME){
		CMQTIME = cMQTIME;
	}
	public String getCDNO() {
		return CDNO;
	}
	public void setCDNO(String cDNO) {
		CDNO = cDNO;
	}
}