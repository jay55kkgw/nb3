package tw.com.fstop.nnb.aop;

import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.custom.annotation.IPVERIFY;
import tw.com.fstop.nnb.service.Login_out_Service;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

@Aspect
@Component
public class IpVerifyAop {

	static Logger log = LoggerFactory.getLogger(IpVerifyAop.class);

	@Autowired
	private HttpServletRequest req;
	@Autowired
	private HttpServletResponse resp; 
	
	@Autowired
	private Login_out_Service login_out_service;
	
	
	/**
	 * NP10驗證未登入功能帶統編的請求
	 */
    @Around("@annotation(ipverify)")
    public Object verifyIp(ProceedingJoinPoint pjp, IPVERIFY ipverify) throws Throwable {
    	log.debug("IpVerifyAop.verifyIp...");
    	String cusidnKey;
    	try {
    		cusidnKey = ipverify.cusidnKey();
    		if (StrUtil.isNotEmpty(cusidnKey)) {
        		Object[] objs = pjp.getArgs();
        		for (Object obj: objs) {
        			log.debug("IpVerifyAop.verifyIp.obj.class: " + obj.getClass());
        			if (obj != null && LinkedHashMap.class == obj.getClass()) {
        				HashMap<String, Object> map = new HashMap<String, Object>();
        				map = (HashMap<String, Object>) obj;
    					String cusidn = map.get(cusidnKey) != null ? String.valueOf(map.get(cusidnKey)).toUpperCase() : "";
    					log.debug("IpVerifyAop.verifyIp.cusidn: " + cusidn);
    					
    					// 若cusidn有值就需要發NP10
    					if (StrUtil.isNotEmpty(cusidn)) {
    						String userip = ESAPIUtil.validInput(WebUtil.getIpAddr(req), "GeneralString", true) ; // 登入 IP-Address
    						// NP10
    						BaseResult bsNP10 = login_out_service.ipVerifyNP10(cusidn, userip);
    						if( !bsNP10.getResult() ) {
    							log.warn(ESAPIUtil.vaildLog("IpVerifyAop.bsNP10.fail.userip: "+ userip));
    							log.warn(ESAPIUtil.vaildLog("IpVerifyAop.bsNP10.fail.cusidn: "+ cusidn));
    							
    							// NP10未通過
    							return "/error_ipVerify";
//    							String redirectUrl = req.getContextPath() + "/ipVerify";
//    							resp.sendRedirect(redirectUrl);
//    							return resp;
    						}
    						
        				}
        			}
    		   }
    		}
    		
    	} catch (Throwable e) {
			log.error("IpVerifyAop.verifyAcn.error: " + e.toString());
		}
        
    	log.debug("IpVerifyAop.verifyAcn.Finish!!!");
    	return pjp.proceed();
    }
    
    
    
}
