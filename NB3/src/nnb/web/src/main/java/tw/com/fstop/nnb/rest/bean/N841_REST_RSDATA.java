package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N841_REST_RSDATA implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5558276628049904703L;
	
	
	private String FDPNUM;
	private String SV_LOST;
	private String AMTFDP;
	private String ITR;
	private String SING_LOST;
	private String FILLER;
	private String ACN;
	private String TSFACN;
	private String DUEDAT;
	private String DPISDT;
	
	
	public String getFDPNUM() {
		return FDPNUM;
	}
	public void setFDPNUM(String fDPNUM) {
		FDPNUM = fDPNUM;
	}
	public String getSV_LOST() {
		return SV_LOST;
	}
	public void setSV_LOST(String sV_LOST) {
		SV_LOST = sV_LOST;
	}
	public String getAMTFDP() {
		return AMTFDP;
	}
	public void setAMTFDP(String aMTFDP) {
		AMTFDP = aMTFDP;
	}
	public String getITR() {
		return ITR;
	}
	public void setITR(String iTR) {
		ITR = iTR;
	}
	public String getSING_LOST() {
		return SING_LOST;
	}
	public void setSING_LOST(String sING_LOST) {
		SING_LOST = sING_LOST;
	}
	public String getFILLER() {
		return FILLER;
	}
	public void setFILLER(String fILLER) {
		FILLER = fILLER;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getTSFACN() {
		return TSFACN;
	}
	public void setTSFACN(String tSFACN) {
		TSFACN = tSFACN;
	}
	public String getDUEDAT() {
		return DUEDAT;
	}
	public void setDUEDAT(String dUEDAT) {
		DUEDAT = dUEDAT;
	}
	public String getDPISDT() {
		return DPISDT;
	}
	public void setDPISDT(String dPISDT) {
		DPISDT = dPISDT;
	}
	public String getINTMTH() {
		return INTMTH;
	}
	public void setINTMTH(String iNTMTH) {
		INTMTH = iNTMTH;
	}
	public String getFD_LOST() {
		return FD_LOST;
	}
	public void setFD_LOST(String fD_LOST) {
		FD_LOST = fD_LOST;
	}
	String INTMTH;
	String FD_LOST;
	
	
	


}
