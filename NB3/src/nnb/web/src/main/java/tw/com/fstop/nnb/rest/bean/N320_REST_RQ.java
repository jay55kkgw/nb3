package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N320_REST_RQ extends BaseRestBean_LOAN implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -559387283010166442L;
	
	private String CUSIDN;
	private String USERDATA_X50_320;
	private String N320GO;
	private String N552GO;
	private String USERDATA_X50_552;
	private String OKOVNEXT;


	// 是否跳過TXNLOG
	private String ADOPID;
	
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getUSERDATA_X50_320() {
		return USERDATA_X50_320;
	}
	public void setUSERDATA_X50_320(String uSERDATA_X50_320) {
		USERDATA_X50_320 = uSERDATA_X50_320;
	}
	public String getN320GO() {
		return N320GO;
	}
	public void setN320GO(String n320go) {
		N320GO = n320go;
	}
	public String getN552GO() {
		return N552GO;
	}
	public void setN552GO(String n552go) {
		N552GO = n552go;
	}
	public String getUSERDATA_X50_552() {
		return USERDATA_X50_552;
	}
	public void setUSERDATA_X50_552(String uSERDATA_X50_552) {
		USERDATA_X50_552 = uSERDATA_X50_552;
	}
	public String getOKOVNEXT() {
		return OKOVNEXT;
	}
	public void setOKOVNEXT(String oKOVNEXT) {
		OKOVNEXT = oKOVNEXT;
	}	
	
}
