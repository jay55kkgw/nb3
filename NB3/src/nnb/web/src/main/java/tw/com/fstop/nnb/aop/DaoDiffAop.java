package tw.com.fstop.nnb.aop;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fstop.orm.po.TXNLOG;
import tw.com.fstop.common.ResultCode;
import tw.com.fstop.tbb.nnb.dao.SysLogDao;
import tw.com.fstop.tbb.nnb.dao.TxnLogDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;
/**
 * 紀錄Dao 時間
 * @author Hugo
 *
 */
@Aspect
@Component
public class DaoDiffAop {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private HttpServletRequest req;
	
	
	@Around("within(tw.com.fstop.tbb.nnb.dao..*) || within(tw.com.fstop.spring.dao..*) ")
//	@Around("within(tw.com.fstop.tbb.nnb.dao..*) || execution(tw.com.fstop.spring.dao..* save(..)) || execution(tw.com.fstop.spring.dao..* saveOrUpdate(..)) || execution(tw.com.fstop.spring.dao.*.* remove(..))")
	public Object doAround(ProceedingJoinPoint pjp) {
		log.info("Before doAround..");
		Object result = null;
		String pstimediff="" ,method="" ,className="";
		DateTime d1 = null;
		DateTime d2 = null;
		
		
		try {
			method = pjp.getSignature().getName();
		} catch (Exception e) {
			log.error("{}",e);
		}
		try {
			className = pjp.getTarget().getClass().getName();
		} catch (Exception e) {
			log.error("{}",e);
		}
//		要切2個try catch 原因:使前面的錯誤 不會影響pjp.proceed
		try {
			
			d1 = new DateTime();
			log.trace("d1>>{}" ,d1.toString("yyyy-MM-dd HH:mm:ss:SSS") );
			result = pjp.proceed();
			log.info("After doAround..");
			d2 = new DateTime();
			pstimediff = DateUtil.getDiffTimeMills(d1, d2);
			log.warn("method>>{} , doa_pstimediff>>{}",className + "." + method,pstimediff);
			
		} catch (Throwable e) {
			log.error(e.toString());
		}
		log.info("doAround End...");
		
		return result;
	}
	
	
}
