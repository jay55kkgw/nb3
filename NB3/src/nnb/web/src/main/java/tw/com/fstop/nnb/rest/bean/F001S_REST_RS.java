package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class F001S_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4117096049834509847L;

	private String CURAMT; // 轉出帳號帳戶餘額
	private String AVAAMT; // 轉出帳號可用餘額
	
	
	public String getCURAMT() {
		return CURAMT;
	}
	public void setCURAMT(String cURAMT) {
		CURAMT = cURAMT;
	}
	public String getAVAAMT() {
		return AVAAMT;
	}
	public void setAVAAMT(String aVAAMT) {
		AVAAMT = aVAAMT;
	}
	
	
	
}
