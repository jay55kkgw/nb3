package tw.com.fstop.nnb.spring.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.common.ResultCode;
import tw.com.fstop.nnb.service.RateQuery_Service;
import tw.com.fstop.util.ESAPIUtil;

@Controller
@RequestMapping(value = "/RATE/QUERY")
public class RateQuery_Controller
{
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	RateQuery_Service rateQuery_Service;
	
	
	@RequestMapping(value = "/N021")
	public String rate_query_n021(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("rate_query_n021...");
		
		String target = "/error";
		
		BaseResult bs = null;
		BaseResult bs1 = null;
		
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
		try {
			bs = new BaseResult();
			okMap.put("QUERYTYPE", "N021");
			bs = rateQuery_Service.query_n021(okMap);
			log.debug("bsN021 >> {} ",bs.getData());
			okMap.clear();
			
			bs1 = new BaseResult();
			okMap.put("QUERYTYPE", "N027");
			bs1 = rateQuery_Service.query_n027(okMap);
			
		} catch (Exception e) {
			log.error("Get data error >> {}",e);
			bs.setSYSMessage(ResultCode.SYS_ERROR);
		} finally {
			if (bs.getResult() && bs1.getResult()) {
				model.addAttribute("N021data", bs);
				model.addAttribute("N027data", bs1);
				target = "/rate_query/N021";
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		log.trace("target >> {}", target);
		
		return target;
		
	}
	
	@RequestMapping(value = "/N022")
	public String rate_query_n022(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("rate_query_n022...");
		
		String target = "/error";
		
		BaseResult bs = null;
		BaseResult bs1 = null;
		
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
		try {
			bs = new BaseResult();
			okMap.put("QUERYTYPE", "N022");
			bs = rateQuery_Service.query_n022(okMap);
		} catch (Exception e) {
			log.error("query_n022 error >> {}",e);
			bs.setSYSMessage(ResultCode.SYS_ERROR);
		}
		
		okMap.clear();
		
		try {
			bs1 = new BaseResult();
			okMap.put("QUERYTYPE", "N023");
			bs1 = rateQuery_Service.query_n023(okMap);
		} catch (Exception e) {
			log.error("query_n023 error >> {}",e);
			bs1.setSYSMessage(ResultCode.SYS_ERROR);
		}
		
		if (bs.getResult() && bs1.getResult()) {
			model.addAttribute("N022data", bs);
			model.addAttribute("N023data", bs1);
			target = "/rate_query/N022";
		} else {
			model.addAttribute(BaseResult.ERROR, bs);
		}
		
//		finally {
//			if (bs.getResult() && bs.getResult()) {
//				model.addAttribute("N022data", bs);
//				target = "/rate_query/n022";
//			} else {
//				model.addAttribute(BaseResult.ERROR, bs);
//			}
//		}
		
		target = "/rate_query/N022";
		log.trace("target >> {}", target);
		return target;
		
	}
	
	@RequestMapping(value = "/N024")
	public String rate_query_n024(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("rate_query_n024...");
		
		String target = "/error";
		
		BaseResult bs = null;
		
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
		try {
			bs = new BaseResult();
			okMap.put("QUERYTYPE", "N024");
			bs = rateQuery_Service.query_n024(okMap);
			
		} catch (Exception e) {
			log.error("rate_query_n024 error >> {}",e);
			bs.setSYSMessage(ResultCode.SYS_ERROR);
		} finally {
			if (bs.getResult() && bs.getResult()) {
				model.addAttribute("N024data", bs);
				target = "/rate_query/N024";
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		log.trace("target >> {}", target);
		
		return target;
		
	}
	
	@RequestMapping(value = "/N025")
	public String rate_query_n025(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("rate_query_n025...");
		
		String target = "/error";
		
		BaseResult bs = null;
		
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
		try {
			bs = new BaseResult();
			okMap.put("QUERYTYPE", "N025");
			bs = rateQuery_Service.query_n025(okMap);
			
		} catch (Exception e) {
			log.error("payment error >> {}",e);
			bs.setSYSMessage(ResultCode.SYS_ERROR);
		} finally {
			if (bs.getResult() && bs.getResult()) {
				model.addAttribute("N025data", bs);
				target = "/rate_query/N025";
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		log.trace("target >> {}", target);
		
		return target;
		
	}
	
	@RequestMapping(value = "/N026")
	public String rate_query_n026(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("rate_query_n026...");
		
		String target = "/error";
		
		BaseResult bs = null;
		
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
		try {
			bs = new BaseResult();
			okMap.put("QUERYTYPE", "N026");
			bs = rateQuery_Service.query_n026(okMap);
			
		} catch (Exception e) {
			log.error("payment error >> {}",e);
			bs.setSYSMessage(ResultCode.SYS_ERROR);
		} finally {
			if (bs.getResult() && bs.getResult()) {
				model.addAttribute("N026data", bs);
				target = "/rate_query/N026";
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		log.trace("target >> {}", target);
		
		return target;
		
	}
	
	@RequestMapping(value = "/N027")
	public String rate_query_n027(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("rate_query_n027...");
		
		String target = "/error";
		
		BaseResult bs = null;
		
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
		try {
			bs = new BaseResult();
			okMap.put("QUERYTYPE", "N027");
			bs = rateQuery_Service.query_n027(okMap);
			
		} catch (Exception e) {
			log.error("payment error >> {}",e);
			bs.setSYSMessage(ResultCode.SYS_ERROR);
		} finally {
			if (bs.getResult() && bs.getResult()) {
				model.addAttribute("N027data", bs);
				target = "/rate_query/N027";
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		log.trace("target >> {}", target);
		
		return target;
		
	}
	
	@RequestMapping(value = "/N030")
	public String rate_query_n030(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("rate_query_n030...");
		
		String target = "/error";
		
		BaseResult bs = null;
		
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
		try {
			bs = new BaseResult();
			okMap.put("QUERYTYPE", "N030");
			bs = rateQuery_Service.query_n030(okMap);
			
		} catch (Exception e) {
			log.error("payment error >> {}",e);
			bs.setSYSMessage(ResultCode.SYS_ERROR);
		} finally {
			if (bs.getResult() && bs.getResult()) {
				model.addAttribute("N030data", bs);
				target = "/rate_query/N030";
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}

		log.trace("target >> {}", target);
		
		return target;
		
	}
	
}
