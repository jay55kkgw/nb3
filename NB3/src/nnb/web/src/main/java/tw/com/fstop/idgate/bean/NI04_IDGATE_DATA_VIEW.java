package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;


public class NI04_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{
	

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3846312408765943288L;

	@SerializedName(value = "APPLYFLAG") //申請項目 : 申請/取消/變更扣繳方式
	private String APPLYFLAG;

	@SerializedName(value = "TSFACN") //扣款帳號
	private String TSFACN;
	
	@SerializedName(value = "display_PAYFLAG") //扣繳方式 : 應繳總額 /最低應繳額 
	private String display_PAYFLAG;
	
	
	@Override
	public Map<String, String> coverMap(){
		LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", "自動扣款申請/取消");
		switch (this.APPLYFLAG) {
		case "1":
			result.put("交易類型", "申請");
			result.put("扣款帳號", this.TSFACN);
			break;
		case "2":
			result.put("交易類型", "取消");
			result.put("扣款帳號", "");
			break;
		case "3":
			result.put("交易類型", "變更扣繳方式");
			result.put("扣款帳號", this.TSFACN);
			break;
		}
		
		switch (this.display_PAYFLAG) {
		case "1":
			result.put("扣繳方式", "應繳總額");
			break;
		case "2":
			result.put("扣繳方式", "最低應繳額");
			break;
		}
		return result;
	}


	public String getAPPLYFLAG() {
		return APPLYFLAG;
	}


	public void setAPPLYFLAG(String aPPLYFLAG) {
		APPLYFLAG = aPPLYFLAG;
	}


	public String getTSFACN() {
		return TSFACN;
	}


	public void setTSFACN(String tSFACN) {
		TSFACN = tSFACN;
	}


	public String getDisplay_PAYFLAG() {
		return display_PAYFLAG;
	}


	public void setDisplay_PAYFLAG(String display_PAYFLAG) {
		this.display_PAYFLAG = display_PAYFLAG;
	}
}
