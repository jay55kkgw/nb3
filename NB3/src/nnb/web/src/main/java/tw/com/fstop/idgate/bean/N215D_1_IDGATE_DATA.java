package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N215D_1_IDGATE_DATA implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1218727941369618198L;

	@SerializedName(value = "COUNT")
	private String COUNT;
	
	@SerializedName(value = "ACNINFO")
	private String ACNINFO;
	
	@SerializedName(value = "CUSIDN")
	private String CUSIDN;
	
	public String getCOUNT() {
		return COUNT;
	}

	public void setCOUNT(String cOUNT) {
		COUNT = cOUNT;
	}

	public String getACNINFO() {
		return ACNINFO;
	}

	public void setACNINFO(String aCNINFO) {
		ACNINFO = aCNINFO;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

}
