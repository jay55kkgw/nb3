package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N816L_1_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8111928052113225298L;
	
	private String FAILEDCOUNT;
	private String SUCCEEDCOUNT;
	private LinkedList<N816L_1_REST_RSDATA_SUCCEED> SUCCEED;
	private LinkedList<N816L_1_REST_RSDATA_FAILED> FAILED;
	
	
	
	public String getFAILEDCOUNT() {
		return FAILEDCOUNT;
	}
	public String getSUCCEEDCOUNT() {
		return SUCCEEDCOUNT;
	}
	public void setFAILEDCOUNT(String fAILEDCOUNT) {
		FAILEDCOUNT = fAILEDCOUNT;
	}
	public void setSUCCEEDCOUNT(String sUCCEEDCOUNT) {
		SUCCEEDCOUNT = sUCCEEDCOUNT;
	}
	public LinkedList<N816L_1_REST_RSDATA_SUCCEED> getSUCCEED() {
		return SUCCEED;
	}
	public LinkedList<N816L_1_REST_RSDATA_FAILED> getFAILED() {
		return FAILED;
	}
	public void setSUCCEED(LinkedList<N816L_1_REST_RSDATA_SUCCEED> sUCCEED) {
		SUCCEED = sUCCEED;
	}
	public void setFAILED(LinkedList<N816L_1_REST_RSDATA_FAILED> fAILED) {
		FAILED = fAILED;
	}
	
	
}