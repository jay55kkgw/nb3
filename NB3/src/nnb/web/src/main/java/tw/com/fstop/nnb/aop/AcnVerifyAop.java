package tw.com.fstop.nnb.aop;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.common.ResultCode;
import tw.com.fstop.nnb.custom.annotation.ACNVERIFY;
import tw.com.fstop.nnb.custom.annotation.ACNVERIFY0;
import tw.com.fstop.nnb.custom.annotation.ACNVERIFY1;
import tw.com.fstop.nnb.custom.annotation.ACNVERIFY2;
import tw.com.fstop.nnb.rest.bean.N920_REST_RQ;
import tw.com.fstop.nnb.rest.bean.N920_REST_RS;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.RESTUtil;

@Aspect
@Component
public class AcnVerifyAop {

	static Logger log = LoggerFactory.getLogger(AcnVerifyAop.class);

	@Autowired
	private HttpServletRequest req;
	@Autowired
	private HttpServletResponse resp; 
	
	@Autowired
	public RESTUtil restutil;
	
	
	/**
	 * 儲存轉出帳號待驗證--(e.x.臺幣轉帳: Ajax取得轉出帳號)
	 */
	@AfterReturning(
			pointcut="@annotation(tw.com.fstop.nnb.custom.annotation.ACNSET)",
	        returning="retVal")
    public void setAcn(Object retVal) {
    	log.debug("AcnVerifyAop.setAcn...");
    	
    	try {
    		if (retVal != null && retVal.getClass()==BaseResult.class ) {
    			log.debug("AcnVerifyAop.setAcn.retVal: " + retVal.toString());
    			BaseResult bs_acn = new BaseResult();
    			bs_acn = (BaseResult) retVal;
    			
    			Map<String, Object> map_acn = (Map<String, Object>) bs_acn.getData();
    			log.debug("AcnVerifyAop.setAcn.map_acn: " + map_acn.toString());
    			
    			ArrayList<String> list_acn = new ArrayList<String>();
    			map_acn.forEach((k, v) -> list_acn.add(k));
    			log.debug("AcnVerifyAop.setAcn.list_acn: " + list_acn.toString());
    			
    			req.getSession().setAttribute(SessionUtil.ACN_LIST, list_acn);
    		}
    		
    	} catch (Throwable e) {
			log.error("AcnVerifyAop.setAcn.error: " + e.toString());
		}
        
    	log.debug("AcnVerifyAop.setAcn.Finish!!!");
    }
	
	/**
	 * 儲存轉出帳號待驗證--(e.x.臺幣轉出紀錄查詢: form.submit取得轉出帳號)
	 * REC: REC=[{AMOUNT=5000, ACN=01062637011, FLAG=Y}, {AMOUNT=0, ACN=01064600441, FLAG=N}]
	 * ACN: ACN=01062637011
	 */
	@AfterReturning(
			pointcut="@annotation(tw.com.fstop.nnb.custom.annotation.ACNSET1)",
	        returning="retVal")
	public void setAcn1(Object retVal) {
    	log.debug("AcnVerifyAop.setAcn1...");
    	
    	try {
    		if (retVal != null && retVal.getClass()==BaseResult.class ) {
    			log.debug("AcnVerifyAop.setAcn1.retVal: " + retVal.toString());
    			BaseResult bs_acn = new BaseResult();
    			bs_acn = (BaseResult) retVal;
    			
    			if (bs_acn!=null && bs_acn.getResult()) {
    				Map<String, Object> tmpData = (Map) bs_acn.getData();
    				
    				if (tmpData!=null) {
	    				List<Map<String, String>> rec = (List<Map<String,String>>) tmpData.get("REC");
	    				
	    				if (rec!=null) {
		        			ArrayList<String> list_acn = new ArrayList<String>();
		        			List<String> values = rec.stream() // 取出每個Map
		        			        .map(e -> e.get("ACN")) // 找到符合key的val
		        			        .collect(Collectors.toList()); // 把val蒐集起來
		        			list_acn.addAll(values);
		        			log.debug("AcnVerifyAop.setAcn1.list_acn: " + list_acn.toString());
		        			
		        			req.getSession().setAttribute(SessionUtil.ACN_LIST, list_acn);
	    				}
    				}
    			}
    		}
    		
    	} catch (Throwable e) {
			log.error("AcnVerifyAop.setAcn1.error: " + e.toString());
		}
        
    	log.debug("AcnVerifyAop.setAcn1.Finish!!!");
    }
	
	
	/**
	 * 儲存轉出帳號待驗證(有第二個轉出帳號清單需要驗證)--(e.x.外幣轉出紀錄查詢: form.submit取得轉出帳號)
	 * REC: REC=[{AMOUNT=5000, ACN=01062637011, FLAG=Y}, {AMOUNT=0, ACN=01064600441, FLAG=N}]
	 * ACN: ACN=01062637011
	 */
	@AfterReturning(
			pointcut="@annotation(tw.com.fstop.nnb.custom.annotation.ACNSET2)",
	        returning="retVal")
	public void setAcn2(Object retVal) {
    	log.debug("AcnVerifyAop.setAcn2...");
    	
    	try {
    		if (retVal != null && retVal.getClass()==BaseResult.class ) {
    			log.debug("AcnVerifyAop.setAcn2.retVal: " + retVal.toString());
    			BaseResult bs_acn = new BaseResult();
    			bs_acn = (BaseResult) retVal;
    			
    			if (bs_acn!=null && bs_acn.getResult()) {
    				Map<String, Object> tmpData = (Map) bs_acn.getData();
    				
    				if (tmpData!=null) {
	    				List<Map<String, String>> rec = (List<Map<String,String>>) tmpData.get("REC");
	    				
	    				if (rec!=null) {
		        			ArrayList<String> list_acn = new ArrayList<String>();
		        			List<String> values = rec.stream() // 取出每個Map
		        			        .map(e -> e.get("ACN")) // 找到符合key的val
		        			        .collect(Collectors.toList()); // 把val蒐集起來
		        			list_acn.addAll(values);
		        			log.debug("AcnVerifyAop.setAcn2.list_acn: " + list_acn.toString());
		        			
		        			req.getSession().setAttribute(SessionUtil.FEE_ACN_LIST, list_acn);
	    				}
    				}
    			}
    		}
    		
    	} catch (Throwable e) {
			log.error("AcnVerifyAop.setAcn2.error: " + e.toString());
		}
        
    	log.debug("AcnVerifyAop.setAcn2.Finish!!!");
    }
    
	
	/**
	 * 驗證儲存的轉出帳號--(e.x.臺幣轉出紀錄查詢)
	 */
	@Around("@annotation(acnverify)")
    public Object verifyAcn(ProceedingJoinPoint pjp, ACNVERIFY acnverify) throws Throwable {
    	log.debug("AcnVerifyAop.verifyAcn...");
    	String[] acn_ref;
    	try {
    		String acnverifyAcn = acnverify.acn();
    		if (StrUtil.isNotEmpty(acnverifyAcn)) {
    			acn_ref = acnverifyAcn.split(",");
    			
    			ArrayList<String> list_acn = new ArrayList<String>();
        		list_acn = (ArrayList<String>) req.getSession().getAttribute(SessionUtil.ACN_LIST);
        		
        		Object[] objs = pjp.getArgs();
        		for (Object obj: objs) {
        			log.debug("AcnVerifyAop.verifyAcn.obj.class: " + obj.getClass());
        			if (obj != null && LinkedHashMap.class == obj.getClass()) {
        				HashMap<String, Object> map = new HashMap<String, Object>();
        				map = (HashMap<String, Object>) obj;
        				for(String acn_key : acn_ref) {
        					String acn = map.get(acn_key) != null ? String.valueOf(map.get(acn_key)) : "";
        					log.debug("AcnVerifyAop.verifyAcn.acn: " + acn);
        					
        					// 空、全部、包含在list_acn都視為通過
        					if (StrUtil.isNotEmpty(acn) && !"AllAcn".equals(acn) && !list_acn.contains(acn)) {
        						// 帳號並非從N920取得，可能被竄改過
        						log.info("verifyAcn fail!!! acn : " + acn);
        						
//        						String redirectUrl = req.getContextPath() + "/noPass";
//        						resp.sendRedirect(redirectUrl);
        						return "/error_noPass";
        					}
        				}
        			}
    		   }
    		}
    		
    	} catch (Throwable e) {
			log.error("AcnVerifyAop.verifyAcn.error: " + e.toString());
		}
        
    	log.debug("AcnVerifyAop.verifyAcn.Finish!!!");
    	return pjp.proceed();
    }
    
    
    /**
     * 驗證儲存的轉出帳號--(e.x.臺幣轉帳非約定轉出帳號)
     */
	@Around("@annotation(acnverify0)")
    public Object verifyAcn0(ProceedingJoinPoint pjp, ACNVERIFY0 acnverify0) throws Throwable {
    	log.debug("AcnVerifyAop.verifyAcn0...");
    	String[] acn_ref;
    	try {
    		String acnverifyAcn = acnverify0.acn();
    		if (StrUtil.isNotEmpty(acnverifyAcn)) {
    			acn_ref = acnverifyAcn.split(",");
    			
    			String cusidn = new String(Base64.getDecoder().decode((String) req.getSession().getAttribute(SessionUtil.CUSIDN)), "utf-8");
    			ArrayList<String> list_acn = new ArrayList<String>();
    			BaseResult bs_N920 = new BaseResult();
    			
    			bs_N920 = N920_REST_No_Ehcache(cusidn, "TRANSFER_RECORD");
    			if (bs_N920!=null && bs_N920.getResult()) {
    				Map<String, Object> tmpData = (Map) bs_N920.getData();
    				
    				if (tmpData!=null) {
	    				List<Map<String, String>> rec = (List<Map<String,String>>) tmpData.get("REC");
	    				
	    				if (rec!=null) {
		        			list_acn = new ArrayList<String>();
		        			List<String> values = rec.stream() // 取出每個Map
		        			        .map(e -> e.get("ACN")) // 找到符合key的val
		        			        .collect(Collectors.toList()); // 把val蒐集起來
		        			list_acn.addAll(values);
		        			log.debug("AcnVerifyAop.verifyAcn0.list_acn: " + list_acn.toString());
		        			
	    				}
    				}
    			}
    			
    			Object[] objs = pjp.getArgs();
    			for (Object obj: objs) {
    				log.debug("AcnVerifyAop.verifyAcn0.obj.class: " + obj.getClass());
    				if (obj != null && LinkedHashMap.class == obj.getClass()) {
    					HashMap<String, Object> map = new HashMap<String, Object>();
    					map = (HashMap<String, Object>) obj;
    					for(String acn_key : acn_ref) {
    						String acn = map.get(acn_key) != null ? String.valueOf(map.get(acn_key)) : "";
    						log.debug("AcnVerifyAop.verifyAcn0.acn: " + acn);
    						
    						// 空、包含在list_acn都視為通過
    						if (StrUtil.isNotEmpty(acn) && !list_acn.contains(acn)) {
    							// 帳號並非從N920取得，可能被竄改過
    							log.info("verifyAcn0 fail!!! acn : " + acn);
    							
//    							String redirectUrl = req.getContextPath() + "/noPass";
//    							resp.sendRedirect(redirectUrl);
    							return "/error_noPass";
    						}
    					}
    				}
    			}
    		}
    		
    	} catch (Throwable e) {
    		log.error("AcnVerifyAop.verifyAcn.error: " + e.toString());
    	}
    	
    	log.debug("AcnVerifyAop.verifyAcn.Finish!!!");
    	return pjp.proceed();
    }
    
    
	/**
	 * 驗證儲存的轉出帳號(有第二個轉出帳號清單需要驗證)--(e.x.外幣轉出紀錄查詢)
	 */
	@Around("@annotation(acnverify1)")
    public Object verifyAcn1(ProceedingJoinPoint pjp, ACNVERIFY1 acnverify1) throws Throwable {
    	log.debug("AcnVerifyAop.verifyAcn1...");
    	String[] acn_ref;
    	try {
    		String acnverifyAcn = acnverify1.acn();
    		if (StrUtil.isNotEmpty(acnverifyAcn)) {
    			acn_ref = acnverifyAcn.split(",");
    			
    			ArrayList<String> list_acn = new ArrayList<String>();
        		list_acn = (ArrayList<String>) req.getSession().getAttribute(SessionUtil.FEE_ACN_LIST);
        		
        		Object[] objs = pjp.getArgs();
        		for (Object obj: objs) {
        			log.debug("AcnVerifyAop.verifyAcn1.obj.class: " + obj.getClass());
        			if (obj != null && LinkedHashMap.class == obj.getClass()) {
        				HashMap<String, Object> map = new HashMap<String, Object>();
        				map = (HashMap<String, Object>) obj;
        				for(String acn_key : acn_ref) {
        					String acn = map.get(acn_key) != null ? String.valueOf(map.get(acn_key)) : "";
        					
        					// 空、全部、包含在list_acn都視為通過
        					if (StrUtil.isNotEmpty(acn) && !"AllAcn".equals(acn) && !list_acn.contains(acn)) {
        						// 帳號並非從N920取得，可能被竄改過
        						log.info("verifyAcn1 fail!!! acn : " + acn);
        						
//        						String redirectUrl = req.getContextPath() + "/noPass";
//        						resp.sendRedirect(redirectUrl);
        						return "/error_noPass";
        					}
        				}
        			}
    		   }
    		}
    		
    	} catch (Throwable e) {
			log.error("AcnVerifyAop.verifyAcn1.error: " + e.toString());
		}
        
    	log.debug("AcnVerifyAop.verifyAcn2.Finish!!!");
    	return pjp.proceed();
    }
    
    
    
    
	/**
	 * 驗證儲存的轉出帳號+金額--(e.x.國內臺幣匯入匯款通知設定)
	 * "ACNINFO": "010626370112000005000"
	 */
	@Around("@annotation(acnverify2)")
    public Object verifyAcn2(ProceedingJoinPoint pjp, ACNVERIFY2 acnverify2) throws Throwable {
    	log.debug("AcnVerifyAop.verifyAcn2...");
    	String[] acn_ref;
    	try {
    		String acnverifyAcn = acnverify2.acn();
    		if (StrUtil.isNotEmpty(acnverifyAcn)) {
    			acn_ref = acnverifyAcn.split(",");
    			
    			ArrayList<String> list_acn = new ArrayList<String>();
        		list_acn = (ArrayList<String>) req.getSession().getAttribute(SessionUtil.ACN_LIST);
        		
        		Object[] objs = pjp.getArgs();
        		for (Object obj: objs) {
        			log.debug("AcnVerifyAop.verifyAcn2.obj.class: " + obj.getClass());
        			if (obj != null && LinkedHashMap.class == obj.getClass()) {
        				HashMap<String, Object> map = new HashMap<String, Object>();
        				map = (HashMap<String, Object>) obj;
        				for(String acn_key : acn_ref) {
        					String acn = map.get(acn_key) != null ? String.valueOf(map.get(acn_key)) : "";
        					acn = acn.length() > 11 ? acn.substring(0, 11) : acn;
        					log.trace("AcnVerifyAop.verifyAcn2.acn: " + acn);
        					
        					// 包含在list_acn才視為通過
        					if (!list_acn.contains(acn)) {
        						// 帳號可能被竄改過
        						log.info("verifyAcn2 fail!!! acn : " + acn);
        						
//        						String redirectUrl = req.getContextPath() + "/noPass";
//        						resp.sendRedirect(redirectUrl);
        						return "/error_noPass";
        					}
        					
        				}
        			}
    		   }
    		}
    		
    	} catch (Throwable e) {
			log.error("AcnVerifyAop.verifyAcn2.error: " + e.toString());
		}
        
    	log.debug("AcnVerifyAop.verifyAcn2.Finish!!!");
    	return pjp.proceed();
    }
    
    
    
    
    
    
    
    /**
     * 根據傳入Type查詢帳號
     * 
     * @param cusidn
     * @param type
     * @return
     */
    public BaseResult N920_REST_No_Ehcache(String cusidn, String type) {
        log.debug("N920_REST_No_Ehcache start");
        log.trace(ESAPIUtil.vaildLog("cusidn>>" + cusidn));
        log.trace(ESAPIUtil.vaildLog("type>>" + type));
        log.debug("is in Cache");
        BaseResult bs = null;
        N920_REST_RQ rq = null;
        N920_REST_RS rs = null;
        try {
            bs = new BaseResult();
            rq = new N920_REST_RQ();
            // call REST API
            rq.setCUSIDN(cusidn);
            rq.setTYPE(type.toUpperCase());
            rs = restutil.send(rq, N920_REST_RS.class, rq.getAPI_Name(rq.getClass()));
            log.debug(ESAPIUtil.vaildLog("N920_REST_RS >> {}"+ CodeUtil.toJson(rs)));
            if (rs != null) {
                CodeUtil.convert2BaseResult(bs, rs);
            } else {
                log.error("N920_REST_RS is null");
                bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
                
            }
        } catch (Exception e) {
            log.error("", e);
            bs.setMsgCode(ResultCode.SYS_ERROR_TEL);
        }
        
        return bs;
    }
}
