package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;


public class N075_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	//交易類型
	@SerializedName(value = "FGTXDATE")
	private String FGTXDATE;
	//交易時間
	@SerializedName(value = "transfer_date")
	private String transfer_date;
	//轉出帳號
	@SerializedName(value = "OUTACN")
	private String OUTACN;
	//代收截止日
	@SerializedName(value = "IDGATE_LIMDAT")
	private String IDGATE_LIMDAT;
	//電號
	@SerializedName(value = "ELENUM")
	private String ELENUM;
	//查核碼
	@SerializedName(value = "CHKNUM")
	private String CHKNUM;
	//繳款金額
	@SerializedName(value = "IDGATE_AMOUNT")
	private String IDGATE_AMOUNT;

	
	
	@Override
	public Map<String, String> coverMap(){
		Map<String, String> result = new LinkedHashMap<String, String>();
		String fgtxdate = "";
		if("1".equals(this.FGTXDATE))
			fgtxdate = "即時繳費";
		else if("2".equals(this.FGTXDATE))
			fgtxdate = "預約繳費";
		result.put("交易名稱", "繳費-臺灣電力公司");
		result.put("交易類型",fgtxdate);
		result.put("轉帳日期", this.transfer_date);
		result.put("轉出帳號", this.OUTACN);
		result.put("代收截止日", this.IDGATE_LIMDAT);
		result.put("電號", this.ELENUM);
		result.put("查核碼", this.CHKNUM);
		result.put("繳款金額", "新臺幣"+this.IDGATE_AMOUNT+"元");
		return result;
	}



	public String getFGTXDATE() {
		return FGTXDATE;
	}



	public void setFGTXDATE(String fGTXDATE) {
		FGTXDATE = fGTXDATE;
	}



	public String getTransfer_date() {
		return transfer_date;
	}



	public void setTransfer_date(String transfer_date) {
		this.transfer_date = transfer_date;
	}



	public String getOUTACN() {
		return OUTACN;
	}



	public void setOUTACN(String oUTACN) {
		OUTACN = oUTACN;
	}


	public String getIDGATE_LIMDAT() {
		return IDGATE_LIMDAT;
	}



	public void setIDGATE_LIMDAT(String iDGATE_LIMDAT) {
		IDGATE_LIMDAT = iDGATE_LIMDAT;
	}



	public String getELENUM() {
		return ELENUM;
	}



	public void setELENUM(String eLENUM) {
		ELENUM = eLENUM;
	}



	public String getCHKNUM() {
		return CHKNUM;
	}



	public void setCHKNUM(String cHKNUM) {
		CHKNUM = cHKNUM;
	}



	public String getIDGATE_AMOUNT() {
		return IDGATE_AMOUNT;
	}



	public void setIDGATE_AMOUNT(String iDGATE_AMOUNT) {
		IDGATE_AMOUNT = iDGATE_AMOUNT;
	}



	
	


}
