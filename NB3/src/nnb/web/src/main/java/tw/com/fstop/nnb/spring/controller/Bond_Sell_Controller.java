package tw.com.fstop.nnb.spring.controller;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.Bond_Purchase_Service;
import tw.com.fstop.nnb.service.Bond_Sell_Service;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.util.StrUtils;
import tw.com.fstop.web.util.WebUtil;


/**
 * 海外債贖回 Controller
 */
@SessionAttributes({ SessionUtil.CUSIDN, SessionUtil.KYCDATE, SessionUtil.WEAK, SessionUtil.XMLCOD,
		SessionUtil.DPMYEMAIL, SessionUtil.KYC, SessionUtil.STEP1_LOCALE_DATA, SessionUtil.STEP2_LOCALE_DATA,
		SessionUtil.TRANSFER_CONFIRM_TOKEN, SessionUtil.TRANSFER_RESULT_TOKEN, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN,
		SessionUtil.STEP3_LOCALE_DATA, SessionUtil.CONFIRM_LOCALE_DATA, SessionUtil.IDGATE_TRANSDATA, SessionUtil.RESULT_LOCALE_DATA,
		SessionUtil.USERIP })
@Controller
@RequestMapping(value = "/BOND/SELL")
public class Bond_Sell_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	I18n i18n;
	
	@Autowired
	Bond_Sell_Service bond_sell_service ;
	
	@Autowired
	Bond_Purchase_Service bond_purchase_service ;
	
	/**
	 * 海外債贖回頁檢核
	 * 
	 * 1. 檢核有無超過交易時間 ( AM9:00~PM2:30 )
	 * 2. 是否有約定網銀外幣入扣帳帳號 ( N922 )
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/bond_sell_query")
	public String bond_sell_query(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		
		String target = "/error";
		log.info("<<< bond_sell_query >>>>");
		// 最後用來set 頁面值
		BaseResult bs = new BaseResult();
		// N922 電文回傳
		BaseResult bsN922 = new BaseResult();
		
		try{
			
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
			
			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = bond_sell_service.getTxToken();
			
			log.trace("bond_sell_query.TXTOKEN: " + ((Map<String, String>) bs.getData()).get("TXTOKEN"));
			if (bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN,
						((Map<String, String>) bs.getData()).get("TXTOKEN"));
			}
			bs.reset();
			//
			
			// 判斷SESSION內CUSIDN是否存在
			String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
			log.debug("cusidn={}",cusidn);
			
			if(StrUtil.isEmpty(cusidn)) {
				model.addAttribute(BaseResult.ERROR, bs);
				return target;
			}
			//
			

			//檢核1 檢核有無超過交易時間 ( AM9:00~PM2:30 )
			Boolean inTradingHour = bond_purchase_service.inTradingHour();
			
			if(!inTradingHour) {
				bs.setErrorMessage("B162", i18n.getMsg("LB.X2585"));
				bs.setPrevious("/INDEX/index");
				bs.setResult(false);
				model.addAttribute(BaseResult.ERROR,bs);
				return target;
			}
			//檢核1 結束
			
			//檢核2 是否有約定網銀外幣入扣帳帳號 ( N922 )
			bsN922 = bond_purchase_service.N922_REST(cusidn);

			// N922 失敗情況 : return
			if (bsN922 == null || !bsN922.getResult()) {
				bs = bsN922;
				bs.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bsN922);
				return target;
			}
			
			

			Map<String, Object> dataN922 = (Map<String, Object>) bsN922.getData();
			String inacn = (String) dataN922.get("ACN2");
			String outacn = (String) dataN922.get("ACN4");
			//測試用 未約定網銀外幣帳號
//			inacn="";
//			outacn="";
			//任一沒有就錯誤頁
			if(StrUtil.isEmpty(inacn) || StrUtil.isEmpty(outacn)) {
				bs.setErrorMessage("ZX99", i18n.getMsg("LB.X2586"));
				bs.setResult(false);
				bs.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR,bs);
				return target;
			}
			//檢核2 結束
			
			String NAME = "";
			if(StrUtils.isNotEmpty((String)dataN922.get("CUSNAME"))) {
				NAME = (String)dataN922.get("CUSNAME");
			}else {
				NAME = (String)dataN922.get("NAME");
			}
			
			
			okMap.put("CUSIDN", cusidn );
			okMap.put("BRHCOD", (String) dataN922.get("APLBRH"));
			
			String ip = WebUtil.getIpAddr(request);
			okMap.put("IP", ip );
			
			//打B021電文
			bs = bond_sell_service.B021_REST(okMap);
			
			//
			bs = bond_sell_service.bond_sell_query_page(bs);
			
			bs.addData("IP", ip);
			bs.addData("BRHCOD", (String) dataN922.get("APLBRH"));
			bs.addData("hiddenNAME", WebUtil.hideusername1Convert(NAME));
			
			bs.addData("ACN2", (String) dataN922.get("ACN2"));
			bs.addData("ACN4", (String) dataN922.get("ACN4"));
			
			SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, bs);
		
		}catch (Exception e) {
			log.error("bond_sell_query error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				
				model.addAttribute("bond_sell_query", bs);
				target = "/bond/bond_sell_query";
			
			}else {
				model.addAttribute(BaseResult.ERROR,bs);
			}
		}
		return target;
	}
	
	/**
	 * 海外債贖回輸入頁
	 * 
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/bond_sell_input")
	public String bond_sell_input(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		log.info("<<< bond_sell_input >>>>");
		Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
		BaseResult bs = new BaseResult();
		
		try {
			
			// 判斷SESSION內CUSIDN是否存在
			String cusidn = new String(
				Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
				"UTF-8");
			log.debug("cusidn={}", cusidn);

			if (StrUtil.isEmpty(cusidn)) {
				model.addAttribute(BaseResult.ERROR, bs);
					return target;
			}
			//
			//打B022檢核取得交易資料 
			//所選資料
			String selected_data_str = okMap.get("BONDDATA");
			log.info("selected_data_str >> {} ", selected_data_str);
			//轉MAP
			selected_data_str = selected_data_str.replace("{", "").replace("}", "");
			String[] keyValuePairs = selected_data_str.split(", ");
			
			Map<String, String> selected_data = new HashMap<>();
			
			for (String pair1 : keyValuePairs) {
				String[] entry = pair1.split("="); 
				
				String mapKey = "";
				String MapValue = "";
				
				if (entry.length == 2)
				{
					mapKey = "".equals(entry[0].trim()) ? "" : entry[0].trim();
					MapValue = "".equals(entry[1].trim()) ? "" : entry[1].trim();
				}
				else
				{
					mapKey = entry[0].trim();
				}
				selected_data.put(mapKey, MapValue);
			}
				
			log.info("selected_data >> {} ", selected_data);
			
			// b021rs , IP , BRHCOD , hiddenNAME ,ACN2 ,ACN4
			bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null);
			
			bs.addData("CUSIDN", cusidn);
			bs.addData("SELLWAY" , okMap.get("SELLWAY"));
			bs.addAllData(selected_data);
			
			bond_sell_service.bond_sell_input_page(bs);
			
			SessionUtil.addAttribute(model, SessionUtil.STEP2_LOCALE_DATA, bs);
			
		}catch (Exception e) {
			log.error("bond_sell_input error >> {}", e);
		}finally {
			if (bs != null && bs.getResult()) {
				model.addAttribute("bond_sell_input", bs);
				target = "/bond/bond_sell_input";
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		
		
		return target;
		
	}
	
	
	/**
	 * 海外債贖回確認頁
	 * 
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/bond_sell_confirm")
	public String bond_sell_confirm(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		log.info("<<< bond_sell_confirm >>>>");
		Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
		BaseResult bs = new BaseResult();
		BaseResult bsB022 = new BaseResult();
		try {
			
			// 判斷SESSION內CUSIDN是否存在
			String cusidn = new String(
				Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
				"UTF-8");
			log.debug("cusidn={}", cusidn);

			if (StrUtil.isEmpty(cusidn)) {
				model.addAttribute(BaseResult.ERROR, bs);
					return target;
			}
			
			bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.STEP2_LOCALE_DATA, null);
			
			bs.addAllData(okMap);
			
			Map<String, String> b022_1_in = new HashMap<String, String>();
			
			processB022_1_params(b022_1_in, bs);
			
			bsB022 = bond_sell_service.B022_REST(b022_1_in);
			
			bs = bond_sell_service.bond_sell_confirm_data( bs , bsB022);
			
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
			
		}catch (Exception e) {
			log.error("bond_sell_confirm error >> {}", e);
		}finally {
			if (bs != null && bs.getResult()) {
				model.addAttribute("bond_sell_confirm", bs);
				target = "/bond/bond_sell_confirm";
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		
		return target;
		
	}
	
	
	/**
	 * 海外債贖回結果頁
	 * 
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/bond_sell_result")
	public String bond_sell_result(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		log.info("<<< bond_sell_result >>>>");
		Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
		BaseResult bs = new BaseResult();
		BaseResult bsB022 = new BaseResult();
		BaseResult bsN951 = new BaseResult();
		
		try {
			
			// 判斷SESSION內CUSIDN是否存在
			String cusidn = new String(
				Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
				"UTF-8");
			log.debug("cusidn={}", cusidn);

			if (StrUtil.isEmpty(cusidn)) {
				model.addAttribute(BaseResult.ERROR, bs);
					return target;
			}
			
			bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null);
			
			okMap.putAll((Map)bs.getData());
			
			Map<String, String> b022_2_in = new HashMap<String, String>();
			
			processB022_2_params(b022_2_in, okMap);
			
			bsN951 = bond_purchase_service.N951_REST(b022_2_in.get("CUSIDN"),okMap);
			
			if(bsN951.getMsgCode().equals("0")) {
				log.debug("B022_02_REST reqParam >> {}", b022_2_in);
				
				bsB022 = bond_purchase_service.B022_REST(b022_2_in);
				
				if (bsB022 != null && bsB022.getResult()) {

					bs.addAllData((Map)bsB022.getData());

				} else {
					model.addAttribute(BaseResult.ERROR, bsB022);
					return target;
				}
			}else {
				bs.setErrorMessage(bsN951.getMsgCode(), bsN951.getMessage());
				bs.setResult(false);
				log.trace(bs.getMsgCode());
			}
			
			
			bond_sell_service.bond_sell_result_data(bs);
			
		}catch (Exception e) {
			log.error("bond_sell_result error >> {}", e);
		}finally {
			if (bs != null && bs.getResult()) {
				model.addAttribute("bond_sell_result", bs);
				target = "/bond/bond_sell_result";
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		
		return target;
		
	}
	
	
	public void processB022_1_params(Map<String, String> b022_1_in, BaseResult bs) {
		
		Map<String,Object> bsData = (Map<String,Object>)bs.getData();
		
		//B022_01 帶I1-I7 I013
		b022_1_in.put("CUSIDN", (String) bsData.get("CUSIDN"));
		b022_1_in.put("BRHCOD", (String) bsData.get("BRHCOD"));
		// 類別(1:檢核 2:確認)
		b022_1_in.put("I01", "1");
		// 信託帳號
		b022_1_in.put("I02", (String) bsData.get("O01"));
		// 商品代號
		b022_1_in.put("I03", (String) bsData.get("O02"));
		// 幣別
		b022_1_in.put("I05", (String) bsData.get("O04"));
		// 委賣面額
		b022_1_in.put("I06", (String) bsData.get("O07"));
		
		if("2".equals((String) bsData.get("SELLWAY"))) {
			b022_1_in.put("I06", (String) bsData.get("I06"));
		}
		
		// 委託賣價
		b022_1_in.put("I07", (String) bsData.get("O08"));
		
		// 贖回方式(1.全贖2.部贖) 檢核的時候不知為何只能帶1才會回來
		b022_1_in.put("I13", (String) bsData.get("SELLWAY"));
		
		b022_1_in.put("IP", (String) bsData.get("IP"));
		
		log.info("b022_1_in >> {}", b022_1_in.toString());
	}
	
	public void processB022_2_params(Map<String, String> b022_2_in, Map<String, String> confirm_data) {
		
		//B022_02 帶全帶
		b022_2_in.put("CUSIDN", confirm_data.get("CUSIDN"));
		b022_2_in.put("BRHCOD", confirm_data.get("BRHCOD"));
		// 類別(1:檢核 2:確認)
		b022_2_in.put("I01", "2");
		// 信託帳號
		b022_2_in.put("I02", confirm_data.get("O01"));
		// 商品代號
		b022_2_in.put("I03",confirm_data.get("BONDCODE"));
		// 幣別
		b022_2_in.put("I05", confirm_data.get("BONDCRY"));
		// 委賣面額
		b022_2_in.put("I06", confirm_data.get("O03"));
		// 委託賣價
		b022_2_in.put("I07", confirm_data.get("O04"));
		// 保管費
		b022_2_in.put("I08", confirm_data.get("O07"));
		// 前手息+/-
		b022_2_in.put("I09", confirm_data.get("O08"));
		// 前手息
		b022_2_in.put("I10", confirm_data.get("O09"));
		// 信託本金
		b022_2_in.put("I11", confirm_data.get("O05"));
		// 入帳帳號
		b022_2_in.put("I12", confirm_data.get("ACN4"));
		
		// 贖回方式(1.全贖2.部贖) 目前只有全贖會過 ?
		b022_2_in.put("I13", confirm_data.get("SELLWAY"));
		
		b022_2_in.put("IP", confirm_data.get("IP"));
		
		b022_2_in.put("FGTXWAY", "0");
		
		log.info("b022_2_in >> {}",b022_2_in.toString());
	}
}
