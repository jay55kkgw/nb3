package tw.com.fstop.nnb.service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.SessionAttributes;

import fstop.orm.po.ADMCURRENCY;
import fstop.orm.po.TXNFUNDDATA;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.rest.bean.C013_REST_RQ;
import tw.com.fstop.nnb.rest.bean.C013_REST_RS;
import tw.com.fstop.nnb.rest.bean.C013_REST_RSDATA;
import tw.com.fstop.nnb.rest.bean.C014_REST_RQ;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmCurrencyDao;
import tw.com.fstop.tbb.nnb.dao.TxnFundDataDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.ConfingManager;
import tw.com.fstop.web.util.WebCoreUtil;
import tw.com.fstop.web.util.WebUtil;

/**
 * 基金查詢交易的Service
 */
@SessionAttributes({ SessionUtil.DPUSERNAME, SessionUtil.C012TOTAL,SessionUtil.PRINT_DATALISTMAP_DATA})
@Service
public class Fund_Query_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private I18n i18n;
	@Autowired
	private Fund_Transfer_Service fund_transfer_service;

	@Autowired
	private AdmCurrencyDao adm_currency_dao;

	@Autowired
	private TxnFundDataDao txn_funddata_dao;
	
	@Autowired
	private AdmCurrencyDao admCurrencyDao;

	/**
	 * 基金餘額及損益查詢
	 */
	public BaseResult query_profitloss_balance_data(String cusidn,String dpusername) {
		return query_profitloss_balance_data(cusidn, dpusername, null);
	}
	@SuppressWarnings("unchecked")
	public BaseResult query_profitloss_balance_data(String cusidn,String dpusername, String isTxnlLog) {
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = C012_REST(cusidn, isTxnlLog);
			if (bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map<String, Object>) bs.getData();
				log.debug("bsData={}", bsData);
				List<Map<String, Object>> rows = (List<Map<String, Object>>) bsData.get("REC");
				log.debug("rows={}", rows);

				TXNFUNDDATA txnFundData;
				ADMCURRENCY admCurrency;
				String TRANSCODE;
				String CDNO;
				String hiddenCDNO;
				String AC225;
				String AC225Format;
				String CRY;
				String ADCCYNAME;
				String TOTAMT;
				String TOTAMTFormat;
				String AC202;
				String AC202Chinese;
				String MIP;
				String ACUCOUNT;
				String ACUCOUNTFormat = null;
				String REFUNDAMT;
				String REFUNDAMTFormat;
				String NET01;
				String NET01Format;
				String FXRATE;
				String FXRATEFormat;
				String REFVALUE;
				String REFVALUEFormat;
				String LCYRAT;
				String LCYRATFormat = null;
				double LCYRATDouble;
				String FCYRAT;
				String FCYRATFormat = null;
				double FCYRATDouble;
				String REFMARK;
				String REFMARKTransfer = "";
				String DIFAMT;
				String DIFAMTFormat;
				double DIFAMTDouble;
				String AMT;
				String AMTFormat;
				double AMTDouble;
				
				for (int x = 0; x < rows.size(); x++) {
					TRANSCODE = (String) rows.get(x).get("TRANSCODE");
					log.debug("TRANSCODE={}", TRANSCODE);

					txnFundData = fund_transfer_service.getFundData(TRANSCODE);
					String COUNTRYTYPE;

					if (txnFundData != null) {
						COUNTRYTYPE = txnFundData.getCOUNTRYTYPE();
						log.debug("COUNTRYTYPE={}", COUNTRYTYPE);

						rows.get(x).put("COUNTRYTYPE", COUNTRYTYPE);
					}
					CDNO = (String) rows.get(x).get("CDNO");
					log.debug("CDNO={}", CDNO);
					
					hiddenCDNO = WebUtil.hideAccount(CDNO);
					log.debug("hiddenCDNO={}", hiddenCDNO);
					rows.get(x).put("hiddenCDNO", hiddenCDNO);

					AC225 = (String) rows.get(x).get("AC225");
					log.debug("AC225={}", AC225);
					AC225Format = AC225.substring(1, 4) + "/" + AC225.substring(4, 6) + "/" + AC225.substring(6);
					log.debug("AC225Format={}", AC225Format);
					rows.get(x).put("AC225Format", AC225Format);

					CRY = (String) rows.get(x).get("CRY");
					log.debug("CRY={}", CRY);
					admCurrency = fund_transfer_service.getCRYData(CRY);

					if (admCurrency != null) {
						//i18n
						Locale currentLocale = LocaleContextHolder.getLocale();
			    		String locale = currentLocale.toString();
			    		
			    		switch (locale) {
						case "en":
							ADCCYNAME = admCurrency.getADCCYENGNAME();
							break;
						case "zh_CN":
							ADCCYNAME = admCurrency.getADCCYCHSNAME();
							break;
						default:
							ADCCYNAME = admCurrency.getADCCYNAME();
							break;
		                }
						
						log.debug("ADCCYNAME={}", ADCCYNAME);

						rows.get(x).put("ADCCYNAME", ADCCYNAME);
					}

					TOTAMT = (String) rows.get(x).get("TOTAMT");
					log.debug("TOTAMT={}", TOTAMT);
					TOTAMT = TOTAMT.replaceAll("\\.", "");
					log.debug("TOTAMT={}", TOTAMT);

					TOTAMTFormat = fund_transfer_service.formatNumberString(TOTAMT, 2);
					log.debug("TOTAMTFormat={}", TOTAMTFormat);

					rows.get(x).put("TOTAMTFormat", TOTAMTFormat);

					AC202 = (String) rows.get(x).get("AC202");
					log.debug("AC202={}", AC202);
					MIP = (String) rows.get(x).get("MIP");
					log.debug("MIP={}", MIP);

					if ("Y".equals(MIP)) {
						//定期不定額
						AC202Chinese = i18n.getMsg("LB.W1087");
					} else {
						if ("1".equals(AC202)) {
							//單筆
							AC202Chinese = i18n.getMsg("LB.X1847");
						} else {
							//定期定額
							AC202Chinese =  i18n.getMsg("LB.W1086");
						}
					}
					log.debug("AC202Chinese={}", AC202Chinese);

					rows.get(x).put("AC202Chinese", AC202Chinese);

					ACUCOUNT = (String) rows.get(x).get("ACUCOUNT");
					log.debug("ACUCOUNT={}", ACUCOUNT);
					ACUCOUNT = ACUCOUNT.replaceAll("\\.", "");
					log.debug("ACUCOUNT={}", ACUCOUNT);

					ACUCOUNTFormat = fund_transfer_service.formatNumberString(ACUCOUNT, 4);
					log.debug("ACUCOUNTFormat={}", ACUCOUNTFormat);

					rows.get(x).put("ACUCOUNTFormat", ACUCOUNTFormat);

					REFUNDAMT = (String) rows.get(x).get("REFUNDAMT");
					log.debug("REFUNDAMT={}", REFUNDAMT);
					REFUNDAMT = REFUNDAMT.replaceAll("\\.", "");
					log.debug("REFUNDAMT={}", REFUNDAMT);

					REFUNDAMTFormat = fund_transfer_service.formatNumberString(REFUNDAMT, 4);
					log.debug("REFUNDAMTFormat={}", REFUNDAMTFormat);

					rows.get(x).put("REFUNDAMTFormat", REFUNDAMTFormat);

					NET01 = (String) rows.get(x).get("NET01");
					log.debug("NET01={}", NET01);
					NET01Format = NET01.substring(1, 4) + "/" + NET01.substring(4, 6) + "/" + NET01.substring(6);
					log.debug("NET01Format={}", NET01Format);
					rows.get(x).put("NET01Format", NET01Format);

					FXRATE = (String) rows.get(x).get("FXRATE");
					log.debug("FXRATE={}", FXRATE);
					FXRATE = FXRATE.replaceAll("\\.", "");
					log.debug("FXRATE={}", FXRATE);

					FXRATEFormat = fund_transfer_service.formatNumberString(FXRATE, 4);
					log.debug("FXRATEFormat={}", FXRATEFormat);

					rows.get(x).put("FXRATEFormat", FXRATEFormat);

					REFVALUE = (String) rows.get(x).get("REFVALUE");
					log.debug("REFVALUE={}", REFVALUE);
					REFVALUE = REFVALUE.replaceAll("\\.", "");
					log.debug("REFVALUE={}", REFVALUE);

					REFVALUEFormat = fund_transfer_service.formatNumberString(REFVALUE, 2);
					log.debug("REFVALUEFormat={}", REFVALUEFormat);

					rows.get(x).put("REFVALUEFormat", REFVALUEFormat);

					LCYRAT = (String) rows.get(x).get("LCYRAT");
					log.debug("LCYRAT={}", LCYRAT);
					LCYRAT = LCYRAT.replaceAll("\\.", "");
					log.debug("LCYRAT={}", LCYRAT);

					LCYRATFormat = fund_transfer_service.formatNumberString(LCYRAT, 2);
					
					if(Long.valueOf(LCYRAT) < 0) {
						LCYRATFormat = LCYRATFormat.replaceAll("-", "");
						LCYRATFormat = "-" + LCYRATFormat;
					}
					
					log.debug("LCYRATFormat={}", LCYRATFormat);
					rows.get(x).put("LCYRATFormat", LCYRATFormat);

					LCYRATDouble = transferStringToDouble(LCYRATFormat);
					log.debug("LCYRATDouble={}", LCYRATDouble);
					rows.get(x).put("LCYRATDouble", LCYRATDouble);

					FCYRAT = (String) rows.get(x).get("FCYRAT");
					log.debug("FCYRAT={}", FCYRAT);
					FCYRAT = FCYRAT.replaceAll("\\.", "");
					log.debug("FCYRAT={}", FCYRAT);

					FCYRATFormat = fund_transfer_service.formatNumberString(FCYRAT, 2);
					
					if(Long.valueOf(FCYRAT) < 0) {
						FCYRATFormat = FCYRATFormat.replaceAll("-", "");
						FCYRATFormat = "-" + FCYRATFormat;
					}
					
					log.debug("FCYRATFormat={}", FCYRATFormat);
					rows.get(x).put("FCYRATFormat", FCYRATFormat);

					FCYRATDouble = transferStringToDouble(FCYRATFormat);
					log.debug("FCYRATDouble={}", FCYRATDouble);
					rows.get(x).put("FCYRATDouble", FCYRATDouble);

					DIFAMT = (String) rows.get(x).get("DIFAMT");
					log.debug("DIFAMT={}", DIFAMT);
					DIFAMT = DIFAMT.trim();
					log.debug("DIFAMT={}", DIFAMT);

					DIFAMTFormat = fund_transfer_service.formatNumberString(DIFAMT, 2);
					
					if(Long.valueOf(DIFAMT) < 0) {
						DIFAMTFormat = DIFAMTFormat.replaceAll("-", "");
						DIFAMTFormat = "-" + DIFAMTFormat;
					}
					
					log.debug("DIFAMTFormat={}", DIFAMTFormat);
					rows.get(x).put("DIFAMTFormat", DIFAMTFormat);

					DIFAMTDouble = transferStringToDouble(DIFAMTFormat);
					log.debug("DIFAMTDouble={}", DIFAMTDouble);
					rows.get(x).put("DIFAMTDouble", DIFAMTDouble);

					AMT = (String) rows.get(x).get("AMT");
					log.debug("AMT={}", AMT);
					AMT = AMT.replaceAll("\\.", "");
					log.debug("AMT={}", AMT);

					AMTFormat = fund_transfer_service.formatNumberString(AMT, 2);
					
					if(Long.valueOf(AMT) < 0) {
						AMTFormat = AMTFormat.replaceAll("-", "");
						AMTFormat = "-" + AMTFormat;
					}
					
					log.debug("AMTFormat={}", AMTFormat);
					rows.get(x).put("AMTFormat", AMTFormat);

					AMTDouble = transferStringToDouble(AMTFormat);
					log.debug("AMTDouble={}", AMTDouble);
					rows.get(x).put("AMTDouble", AMTDouble);
					
					REFMARK = (String) rows.get(x).get("REFMARK");
					log.debug("REFMARK={}", REFMARK);

					if ("N".equals(REFMARK)) {
						REFMARKTransfer = "Y";
					} else if ("Y".equals(REFMARK) || "*".equals(REFMARK) || "A".equals(REFMARK)
							|| "B".equals(REFMARK)) {
						REFMARKTransfer = "N";
					}
					log.debug("REFMARKTransfer={}", REFMARKTransfer);

					rows.get(x).put("REFMARKTransfer", REFMARKTransfer);
					
					String TRANSCRY = (String) rows.get(x).get("TRANSCRY");
					TRANSCRY = StrUtil.isNotEmpty(TRANSCRY)? TRANSCRY : "-";
					log.debug("TRANSCRY={}", TRANSCRY);
					rows.get(x).put("TRANSCRY", TRANSCRY);
				}

				List<Map<String, Object>> secondRows = (List<Map<String, Object>>) bsData.get("REC2");
				log.debug("secondRows={}", secondRows);
				
				DecimalFormat df = new DecimalFormat("#0.00");
				String SUBCRY;
				String SUBTOTAMT;
				String SUBTOTAMTFormat;
				String SUBREFVALUE;
				String SUBREFVALUEFormat;
				String SUBDIFAMT;
				String SUBDIFAMTFormat;
				String SUBLCYRAT;
				double SUBLCYRATDouble;
				String SUBLCYRATFormat;
				String SUBDIFAMT2;
				String SUBDIFAMT2Format;
				String SUBFCYRAT;
				double SUBFCYRATDouble;
				String SUBFCYRATFormat;

				for (int x = 0; x < secondRows.size(); x++) {
					SUBCRY = (String) secondRows.get(x).get("SUBCRY");
					log.debug("SUBCRY={}", SUBCRY);
					if (SUBCRY != null) {
						admCurrency = fund_transfer_service.getCRYData(SUBCRY);

						if (admCurrency != null) {
							//ADCCYNAME = admCurrency.getADCCYNAME();
							Locale currentLocale = LocaleContextHolder.getLocale();
				    		String locale = currentLocale.toString();
				    		
				    		switch (locale) {
							case "en":
								ADCCYNAME = admCurrency.getADCCYENGNAME();
								break;
							case "zh_CN":
								ADCCYNAME = admCurrency.getADCCYCHSNAME();
								break;
							default:
								ADCCYNAME = admCurrency.getADCCYNAME();
								break;
			                }
							log.debug("ADCCYNAME={}", ADCCYNAME);

							secondRows.get(x).put("ADCCYNAME", ADCCYNAME);
						}
					}
					SUBTOTAMT = (String) secondRows.get(x).get("SUBTOTAMT");
					log.debug("SUBTOTAMT={}", SUBTOTAMT);
					if (SUBTOTAMT != null) {
						SUBTOTAMT = SUBTOTAMT.replaceAll("\\.", "");
					} else {
						SUBTOTAMT = "";
					}
					log.debug("SUBTOTAMT={}", SUBTOTAMT);
					SUBTOTAMTFormat = fund_transfer_service.formatNumberString(SUBTOTAMT, 2);
					log.debug("SUBTOTAMTFormat={}", SUBTOTAMTFormat);
					secondRows.get(x).put("SUBTOTAMTFormat", SUBTOTAMTFormat);

					SUBREFVALUE = (String) secondRows.get(x).get("SUBREFVALUE");
					log.debug("SUBREFVALUE={}", SUBREFVALUE);
					if (SUBREFVALUE != null) {
						SUBREFVALUE = SUBREFVALUE.replaceAll("\\.", "");
					} else {
						SUBREFVALUE = "";
					}
					log.debug("SUBREFVALUE={}", SUBREFVALUE);
					SUBREFVALUEFormat = fund_transfer_service.formatNumberString(SUBREFVALUE, 2);
					log.debug("SUBREFVALUEFormat={}", SUBREFVALUEFormat);
					secondRows.get(x).put("SUBREFVALUEFormat", SUBREFVALUEFormat);
					
					SUBDIFAMT = (String) secondRows.get(x).get("SUBDIFAMT");
					log.debug("SUBDIFAMT={}", SUBDIFAMT);
					if (SUBDIFAMT != null) {
						SUBDIFAMT = SUBDIFAMT.replaceAll("\\.", "");
					} else {
						SUBDIFAMT = "";
					}
					log.debug("SUBDIFAMT={}", SUBDIFAMT);
					SUBDIFAMTFormat = fund_transfer_service.formatNumberString(SUBDIFAMT, 2);
					
					if(Long.valueOf(SUBDIFAMT) < 0) {
						SUBDIFAMTFormat = SUBDIFAMTFormat.replaceAll("-", "");
						SUBDIFAMTFormat = "-" + SUBDIFAMTFormat;
					}
					
					log.debug("SUBDIFAMTFormat={}", SUBDIFAMTFormat);
					secondRows.get(x).put("SUBDIFAMTFormat", SUBDIFAMTFormat);
					
					SUBLCYRAT = (String) secondRows.get(x).get("SUBLCYRAT");
					log.debug("SUBLCYRAT={}", SUBLCYRAT);
					if (SUBLCYRAT == null || "".equals(SUBLCYRAT.trim())) {
						SUBLCYRAT = "0.00";
					}
					log.debug("SUBLCYRAT={}", SUBLCYRAT);
					SUBLCYRATDouble = Double.parseDouble(SUBLCYRAT);
					log.debug("SUBLCYRATDouble={}", SUBLCYRATDouble);
					SUBLCYRATDouble = formatAmount(2, "3", SUBLCYRATDouble);
					log.debug("SUBLCYRATDouble={}", SUBLCYRATDouble);

					SUBLCYRATFormat = df.format(SUBLCYRATDouble);
					log.debug("SUBLCYRATFormat={}", SUBLCYRATFormat);

					secondRows.get(x).put("SUBLCYRATFormat", SUBLCYRATFormat);

					SUBDIFAMT2 = (String) secondRows.get(x).get("SUBDIFAMT2");
					log.debug("SUBDIFAMT2={}", SUBDIFAMT2);
					if (SUBDIFAMT2 != null) {
						SUBDIFAMT2 = SUBDIFAMT2.replaceAll("\\.", "");
					} else {
						SUBDIFAMT2 = "";
					}
					log.debug("SUBDIFAMT2={}", SUBDIFAMT2);
					SUBDIFAMT2Format = fund_transfer_service.formatNumberString(SUBDIFAMT2, 2);
					
					if(Long.valueOf(SUBDIFAMT2) < 0) {
						SUBDIFAMT2Format = SUBDIFAMT2Format.replaceAll("-", "");
						SUBDIFAMT2Format = "-" + SUBDIFAMT2Format;
					}
					
					log.debug("SUBDIFAMT2Format={}", SUBDIFAMT2Format);
					secondRows.get(x).put("SUBDIFAMT2Format", SUBDIFAMT2Format);
					
					SUBFCYRAT = (String) secondRows.get(x).get("SUBFCYRAT");
					log.debug("SUBFCYRAT={}", SUBFCYRAT);
					if (SUBFCYRAT == null || "".equals(SUBFCYRAT.trim())) {
						SUBFCYRAT = "0.00";
					}
					log.debug("SUBFCYRAT={}", SUBFCYRAT);
					SUBFCYRATDouble = Double.parseDouble(SUBFCYRAT);
					log.debug("SUBFCYRATDouble={}", SUBFCYRATDouble);
					SUBFCYRATDouble = formatAmount(2, "3", SUBFCYRATDouble);
					log.debug("SUBFCYRATDouble={}", SUBFCYRATDouble);

					SUBFCYRATFormat = df.format(SUBFCYRATDouble);
					log.debug("SUBFCYRATFormat={}", SUBFCYRATFormat);
					secondRows.get(x).put("SUBFCYRATFormat", SUBFCYRATFormat);
				}
				
				String TOTTWDAMT = (String) bsData.get("TOTTWDAMT");
				log.debug("TOTTWDAMT={}", TOTTWDAMT);
				if (TOTTWDAMT != null) {
					TOTTWDAMT = TOTTWDAMT.replaceAll("\\.", "");
				} else {
					TOTTWDAMT = "";
				}
				log.debug("TOTTWDAMT={}", TOTTWDAMT);
				String TOTTWDAMTFormat = fund_transfer_service.formatNumberString(TOTTWDAMT, 2);
				
				if(Long.valueOf(TOTTWDAMT) < 0) {
					TOTTWDAMTFormat = TOTTWDAMTFormat.replaceAll("-", "");
					TOTTWDAMTFormat = "-" + TOTTWDAMTFormat;
				}
				
				log.debug("TOTTWDAMTFormat={}", TOTTWDAMTFormat);
				bsData.put("TOTTWDAMTFormat", TOTTWDAMTFormat);
				
				String TOTREFVALUE = (String) bsData.get("TOTREFVALUE");
				log.debug("TOTREFVALUE={}", TOTREFVALUE);
				if (TOTREFVALUE != null) {
					TOTREFVALUE = TOTREFVALUE.replaceAll("\\.", "");
				} else {
					TOTREFVALUE = "";
				}
				log.debug("TOTREFVALUE={}", TOTREFVALUE);
				String TOTREFVALUEFormat = fund_transfer_service.formatNumberString(TOTREFVALUE, 2);
				
				if(Long.valueOf(TOTREFVALUE) < 0) {
					TOTREFVALUEFormat = TOTREFVALUEFormat.replaceAll("-", "");
					TOTREFVALUEFormat = "-" + TOTREFVALUEFormat;
				}
				
				log.debug("TOTREFVALUEFormat={}", TOTREFVALUEFormat);
				bsData.put("TOTREFVALUEFormat", TOTREFVALUEFormat);

				String TOTDIFAMT = (String) bsData.get("TOTDIFAMT");
				log.debug("TOTDIFAMT={}", TOTDIFAMT);
				if (TOTDIFAMT != null) {
					TOTDIFAMT = TOTDIFAMT.replaceAll("\\.", "").trim();
				} else {
					TOTDIFAMT = "";
				}
				log.debug("TOTDIFAMT={}", TOTDIFAMT);
				String TOTDIFAMTFormat = fund_transfer_service.formatNumberString(TOTDIFAMT, 2);
				
				if(Long.valueOf(TOTDIFAMT) < 0) {
					TOTDIFAMTFormat = TOTDIFAMTFormat.replaceAll("-", "");
					TOTDIFAMTFormat = "-" + TOTDIFAMTFormat;
				}
				
				log.debug("TOTDIFAMTFormat={}", TOTDIFAMTFormat);
				bsData.put("TOTDIFAMTFormat", TOTDIFAMTFormat);
				
				String TOTLCYRAT = (String) bsData.get("TOTLCYRAT");
				log.debug("TOTLCYRAT={}", TOTLCYRAT);

				String TOTLCYRATFormat;
				if (TOTLCYRAT == null || "".equals(TOTLCYRAT.trim())) {
					TOTLCYRATFormat = "0.00";
				} else {
					double TOTLCYRATDouble = Double.parseDouble(TOTLCYRAT.trim());
					log.debug("TOTLCYRATDouble={}", TOTLCYRATDouble);
					TOTLCYRATDouble = formatAmount(2, "3", TOTLCYRATDouble);
					log.debug("TOTLCYRATDouble={}", TOTLCYRATDouble);

					TOTLCYRATFormat = df.format(TOTLCYRATDouble);
				}
				log.debug("TOTLCYRATFormat={}", TOTLCYRATFormat);
				bsData.put("TOTLCYRATFormat", TOTLCYRATFormat);

				String TOTDIFAMT2 = (String) bsData.get("TOTDIFAMT2");
				log.debug("TOTDIFAMT2={}", TOTDIFAMT2);
				if (TOTDIFAMT2 != null) {
					TOTDIFAMT2 = TOTDIFAMT2.replaceAll("\\.", "").trim();
				} else {
					TOTDIFAMT2 = "";
				}
				log.debug("TOTDIFAMT2={}", TOTDIFAMT2);
				String TOTDIFAMT2Format = fund_transfer_service.formatNumberString(TOTDIFAMT2, 2);
				
				if(Long.valueOf(TOTDIFAMT2) < 0) {
					TOTDIFAMT2Format = TOTDIFAMT2Format.replaceAll("-", "");
					TOTDIFAMT2Format = "-" + TOTDIFAMT2Format;
				}
				
				log.debug("TOTDIFAMT2Format={}", TOTDIFAMT2Format);
				bsData.put("TOTDIFAMT2Format", TOTDIFAMT2Format);
				
				String TOTFCYRAT = (String) bsData.get("TOTFCYRAT");
				log.debug("TOTFCYRAT={}", TOTFCYRAT);

				String TOTFCYRATFormat;
				if (TOTFCYRAT == null || "".equals(TOTFCYRAT.trim())) {
					TOTFCYRATFormat = "0.00";
				} else {
					double TOTFCYRATDouble = Double.parseDouble(TOTFCYRAT.trim());
					log.debug("TOTFCYRATDouble={}", TOTFCYRATDouble);
					TOTFCYRATDouble = formatAmount(2, "3", TOTFCYRATDouble);
					log.debug("TOTFCYRATDouble={}", TOTFCYRATDouble);

					TOTFCYRATFormat = df.format(TOTFCYRATDouble);
				}
				log.debug("TOTFCYRATFormat={}", TOTFCYRATFormat);
				bsData.put("TOTFCYRATFormat", TOTFCYRATFormat);
				
				List<Map<String, Object>> thirdRows = (List<Map<String, Object>>) bsData.get("REC3");
				log.debug("thirdRows={}", thirdRows);
				
				String UNALLOTAMTCRY;
				String UNALLOTAMTCRYADCCYNAME;
				String UNALLOTAMT;
				String UNALLOTAMTFormat;
				
				for (int x = 0; x < thirdRows.size(); x++) {
					UNALLOTAMTCRY = (String) thirdRows.get(x).get("UNALLOTAMTCRY");
					log.debug("UNALLOTAMTCRY={}", UNALLOTAMTCRY);

					if (UNALLOTAMTCRY != null) {
						admCurrency = fund_transfer_service.getCRYData(UNALLOTAMTCRY);

						if (admCurrency != null) {
							Locale currentLocale = LocaleContextHolder.getLocale();
				    		String locale = currentLocale.toString();
				    		
				    		switch (locale) {
							case "en":
								UNALLOTAMTCRYADCCYNAME = admCurrency.getADCCYENGNAME();
								break;
							case "zh_CN":
								UNALLOTAMTCRYADCCYNAME = admCurrency.getADCCYCHSNAME();
								break;
							default:
								UNALLOTAMTCRYADCCYNAME = admCurrency.getADCCYNAME();
								break;
			                }
							//UNALLOTAMTCRYADCCYNAME = admCurrency.getADCCYNAME();
							log.debug("UNALLOTAMTCRYADCCYNAME={}", UNALLOTAMTCRYADCCYNAME);

							thirdRows.get(x).put("UNALLOTAMTCRYADCCYNAME", UNALLOTAMTCRYADCCYNAME);
						}
					}

					UNALLOTAMT = (String) thirdRows.get(x).get("UNALLOTAMT");
					log.debug("UNALLOTAMT={}", UNALLOTAMT);
					if (UNALLOTAMT != null) {
						UNALLOTAMT = UNALLOTAMT.replaceAll("\\.", "");
					} else {
						UNALLOTAMT = "";
					}
					log.debug("UNALLOTAMT={}", UNALLOTAMT);
					UNALLOTAMTFormat = fund_transfer_service.formatNumberString(UNALLOTAMT, 2);
					log.debug("UNALLOTAMTFormat={}", UNALLOTAMTFormat);
					thirdRows.get(x).put("UNALLOTAMTFormat", UNALLOTAMTFormat);
				}
				
//				model.addAttribute("rows", rows);
//				model.addAttribute("dataCount", rows.size());
//				model.addAttribute("secondRows", secondRows);
//				model.addAttribute("secondDataCount", secondRows.size());
//				model.addAttribute("thirdRows", thirdRows);
//				model.addAttribute("thirdDataCount", thirdRows.size());
//				model.addAttribute("bsData", bsData);
//				
//				//列印會用到，存在SESSION
//				List<Map<String,Object>> dataListMap = new ArrayList<Map<String,Object>>();
//				Map<String,Object> rowMap = new HashMap<String,Object>();
//				rowMap.put("rows",rows);
//				rowMap.put("secondRows",secondRows);
//				rowMap.put("thirdRows",thirdRows);
//				dataListMap.add(rowMap);
//				SessionUtil.addAttribute(model,SessionUtil.PRINT_DATALISTMAP_DATA,dataListMap);
				
				//改寫KUSO偉人作品
				bs.addData("rows",rows);
				bs.addData("dataCount", rows.size());
				bs.addData("secondRows", secondRows);
				bs.addData("secondDataCount", secondRows.size());
				bs.addData("thirdRows", thirdRows);
				bs.addData("thirdDataCount", thirdRows.size());
				bs.addData("bsData", bsData);
				
				BaseResult n922BS = new BaseResult();
				n922BS = fund_transfer_service.getN922Data(cusidn);
				String userName;
				boolean txnFlag = true;
				if(n922BS != null && n922BS.getResult()){
					Map<String,Object> n922BSData = (Map<String,Object>)n922BS.getData();
					log.debug("n922BSData={}",n922BSData);
					//N922因原住民姓名修改由NAME->CUSNAME
					userName = (String)n922BSData.get("CUSNAME");
					if(userName.length()==0) {
						userName = (String)n922BSData.get("NAME");
					}
					//BIG5轉UTF8問題-----目前先直接截掉問號
					userName=userName.replace("?","");
					log.debug("userName={}",userName);
					// 隱藏姓名 call 字霸
					String hiddenName = WebUtil.hideusername1Convert(userName);
					log.debug("hiddenName={}",hiddenName);
					bs.addData("hiddenName",hiddenName);
					bs.addData("txnFlag",txnFlag);
				}
				else{
					String messageCode = bs.getMsgCode();
					log.debug("messageCode={}",messageCode);
					if("E147".equals(messageCode)){
						userName = dpusername;
						txnFlag = false;
						String hiddenName = WebUtil.hideusername1Convert(userName);
						log.debug("hiddenName={}",hiddenName);
						bs.addData("hiddenName",hiddenName);
						bs.addData("txnFlag",txnFlag);
					}
				}
				//帳戶總覽處理
				List<Map<String,String>> OverviewREC=new ArrayList<Map<String, String>>();
				List<Map<String,String>> OverviewREC2=new ArrayList<Map<String, String>>();
				
				log.debug("CHECK1={}");
				List<Map<String,Object>> bsdata=(List<Map<String, Object>>) ((Map<String, Object>) bs.getData()).get("rows");
				for(int x = 0; x < bsdata.size(); x++) {
					log.debug("CHECK2={}");
					Map<String,String> tempdata=new HashMap<>();
					String tempstring="";
					String tempstring2="";
					tempdata.put("COL1",bsdata.get(x).get("hiddenCDNO")+"<br/>"+bsdata.get(x).get("TRANSCODE")+"/"+bsdata.get(x).get("FUNDLNAME"));
					tempdata.put("COL2",bsdata.get(x).get("AC225Format")+"<hr/>"+bsdata.get(x).get("AC202Chinese"));
					tempdata.put("COL3",bsdata.get(x).get("CRY")+"<hr/>"+bsdata.get(x).get("TRANSCRY"));
					//未分配
					if(bsdata.get(x).get("ACUCOUNTFormat").equals("0.0000")) {
						tempstring= i18n.getMsg("LB.W0921");
					}
					else {
						tempstring=(String) bsdata.get(x).get("ACUCOUNTFormat");
					}
					tempdata.put("COL4",bsdata.get(x).get("TOTAMTFormat")+"<hr/>"+bsdata.get(x).get("REFVALUEFormat"));
					tempdata.put("COL5",tempstring + "/" + bsdata.get(x).get("REFUNDAMTFormat")+"<hr/>"+bsdata.get(x).get("NET01Format"));
					tempdata.put("COL6",(String) bsdata.get(x).get("FXRATEFormat"));
					
					if((Double)bsdata.get(x).get("DIFAMTDouble")>=0) {
						tempstring="<font color='red'>"+bsdata.get(x).get("DIFAMTFormat")+"</font>";
					}
					else {
						tempstring="<font color='green'>"+bsdata.get(x).get("DIFAMTFormat")+"</font>";
					}
					if((Double)bsdata.get(x).get("AMTDouble")>=0) {
						tempstring2="<font color='red'>"+bsdata.get(x).get("AMTDouble")+"</font>";
					}
					else {
						tempstring2="<font color='green'>"+bsdata.get(x).get("AMTFormat")+"</font>";
					}
					tempdata.put("COL7",tempstring+"<hr/>"+tempstring2);
					
					if((Double)bsdata.get(x).get("LCYRATDouble")>=0) {
						tempstring="<font color='red'>"+bsdata.get(x).get("LCYRATFormat")+"％</font>";
					}
					else {
						tempstring="<font color='green'>"+bsdata.get(x).get("LCYRATFormat")+"％</font>";
					}
					if((Double)bsdata.get(x).get("FCYRATDouble")>=0) {
						tempstring2="<font color='red'>"+bsdata.get(x).get("FCYRATDouble")+"％</font>";
					}
					else {
						tempstring2="<font color='green'>"+bsdata.get(x).get("FCYRATFormat")+"％</font>";
					}
					tempdata.put("COL8",tempstring+"<hr/>"+tempstring2);
					
					
					tempdata.put("COL9",tempstring+"<br/>"+tempstring2);
					tempdata.put("TRANSCODE",(String) bsdata.get(x).get("TRANSCODE"));
					tempdata.put("CDNO",(String) bsdata.get(x).get("CDNO"));
					tempdata.put("ACUCOUNTFormat",(String) bsdata.get(x).get("ACUCOUNTFormat"));
					tempdata.put("AC202",(String) bsdata.get(x).get("AC202"));
					tempdata.put("REFMARKTransfer",(String) bsdata.get(x).get("REFMARKTransfer"));
					
				
					//請選擇
					//近30日淨值
					//近1年淨值走勢
					//短線交易規範查詢
					//基金公開說明書
					//基金通路報酬揭露
					//歷史交易明細
					//基金資料交易查詢
					String fastselect="<input type='button' class='d-sm-none d-block' id='click' value='Click' onclick=\"hd2('actionBarc012"+ x +"')\"/>"+
					"<select class='d-none d-sm-block' style='width:100px' size='1' id='typeSelect"+ x +"' onchange=\"processURL('"+bsdata.get(x).get("TRANSCODE")+"','"+bsdata.get(x).get("COUNTRYTYPE")+"','"+bsdata.get(x).get("CDNO")+"','typeSelect"+x+"','"+bsdata.get(x).get("REFMARKTransfer")+"')\">"+
                    "<option value='#'>---"+i18n.getMsg("LB.Select")+"---</option>"+
                    "<option value='1'>"+i18n.getMsg("LB.W0923")+"</option>"+
                    "<option value='2'>"+i18n.getMsg("LB.W0924")+"</option>"+
                    "<option value='7'>"+i18n.getMsg("LB.W0925")+"</option>"+
                    "<option value='8'>"+i18n.getMsg("LB.W0926")+"</option>"+
                    "<option value='9'>"+i18n.getMsg("LB.W0927")+"</option>"+
                    "<option value='5'>"+i18n.getMsg("LB.W0931")+"</option>"+
                    "<option value='10'>"+i18n.getMsg("LB.W0928")+"</option>";
					// 贖回交易 轉換交易
					if(!bsdata.get(x).get("ACUCOUNTFormat").equals("0.0000") && txnFlag==true) {
						fastselect=fastselect+"<option value='3'>"+i18n.getMsg("LB.W0929")+"</option>"+"<option value='4'>"+i18n.getMsg("LB.W0930")+"</option>";
					}
					//約定變更
					if(bsdata.get(x).get("AC202").equals("2"))fastselect=fastselect+"<option value='6'>"+i18n.getMsg("LB.W0932")+"</option>";
					fastselect=fastselect+" </select>";
					fastselect=fastselect+
					"<div id='actionBarc012"+x+"' class='fast-btn-blcok' style='visibility: hidden;'>"+
					"<span class=\"arrow_t_out\"></span>"+
					"<ul>"+
					//近30日淨值
					"<li><a href=\"javascript: processURL_small('"+bsdata.get(x).get("TRANSCODE")+"','"+bsdata.get(x).get("COUNTRYTYPE")+"','"+bsdata.get(x).get("CDNO")+"','1','"+bsdata.get(x).get("REFMARKTransfer")+"')\">"+i18n.getMsg("LB.W0923")+"</a></li>"+
					//近1年淨值走勢
					"<li><a href=\"javascript: processURL_small('"+bsdata.get(x).get("TRANSCODE")+"','"+bsdata.get(x).get("COUNTRYTYPE")+"','"+bsdata.get(x).get("CDNO")+"','2','"+bsdata.get(x).get("REFMARKTransfer")+"')\">"+i18n.getMsg("LB.W0924")+"</a></li>";
					if(!bsdata.get(x).get("ACUCOUNTFormat").equals("0.0000") && txnFlag==true) {
						fastselect=fastselect+
								//贖回交易
							"<li><a href=\"javascript: processURL_small('"+bsdata.get(x).get("TRANSCODE")+"','"+bsdata.get(x).get("COUNTRYTYPE")+"','"+bsdata.get(x).get("CDNO")+"','3','"+bsdata.get(x).get("REFMARKTransfer")+"')\">"+i18n.getMsg("LB.W0929")+"</a></li>"+
								//轉換交易
							"<li><a href=\"javascript: processURL_small('"+bsdata.get(x).get("TRANSCODE")+"','"+bsdata.get(x).get("COUNTRYTYPE")+"','"+bsdata.get(x).get("CDNO")+"','4','"+bsdata.get(x).get("REFMARKTransfer")+"')\">"+i18n.getMsg("LB.W0930")+"</a></li>";
					}
					//歷史交易明細
					fastselect=fastselect+"<li><a href=\"javascript: processURL_small('"+bsdata.get(x).get("TRANSCODE")+"','"+bsdata.get(x).get("COUNTRYTYPE")+"','"+bsdata.get(x).get("CDNO")+"','5',' "+bsdata.get(x).get("REFMARKTransfer")+"')\">"+i18n.getMsg("LB.W0931")+"</a></li>";
					//約定變更
					if(bsdata.get(x).get("AC202").equals("2"))
						fastselect=fastselect+"<li><a href=\"javascript: processURL_small('"+bsdata.get(x).get("TRANSCODE")+"',' "+bsdata.get(x).get("COUNTRYTYPE")+"','"+bsdata.get(x).get("CDNO")+"','6',' "+bsdata.get(x).get("REFMARKTransfer")+"')\">"+i18n.getMsg("LB.W0932")+"</a></li>";
					//短線交易規範查詢
					//基金公開說明書
					//基金通路報酬揭露
					//基金資料交易查詢
					fastselect=fastselect+"<li><a href=\"javascript: processURL_small('"+bsdata.get(x).get("TRANSCODE")+"','"+bsdata.get(x).get("COUNTRYTYPE")+"','"+bsdata.get(x).get("CDNO")+"','7',' "+bsdata.get(x).get("REFMARKTransfer")+"')\">"+i18n.getMsg("LB.W0925")+"</a></li>"+
						"<li><a href=\"javascript: processURL_small('"+bsdata.get(x).get("TRANSCODE")+"','"+bsdata.get(x).get("COUNTRYTYPE")+"','"+bsdata.get(x).get("CDNO")+"','8','"+bsdata.get(x).get("REFMARKTransfer")+"')\">"+i18n.getMsg("LB.W0926")+"</a></li>"+
						"<li><a href=\"javascript: processURL_small('"+bsdata.get(x).get("TRANSCODE")+"','"+bsdata.get(x).get("COUNTRYTYPE")+"','"+bsdata.get(x).get("CDNO")+"','9','"+bsdata.get(x).get("REFMARKTransfer")+"')\">"+i18n.getMsg("LB.W0927")+"</a></li>"+
						"<li><a href=\"javascript: processURL_small('"+bsdata.get(x).get("TRANSCODE")+"','"+bsdata.get(x).get("COUNTRYTYPE")+"','"+bsdata.get(x).get("CDNO")+"','10','"+bsdata.get(x).get("REFMARKTransfer")+"')\">"+i18n.getMsg("LB.W0928")+"</a></li>"+	
						"</ul></div>";
					tempdata.put("fastselect",fastselect);
					OverviewREC.add(tempdata);
				}

				Map<String,Object> overviewdata=(Map<String, Object>) bs.getData();
				OverviewREC2.addAll((List<Map<String,String>>) overviewdata.get("secondRows"));
				Map<String,String> tempdata=new HashMap<>();
				//約當臺幣總投資本金
				tempdata.put("ADCCYNAME", i18n.getMsg("LB.W0940"));
				tempdata.put("SUBTOTAMTFormat",(String) bsData.get("TOTTWDAMTFormat"));
				tempdata.put("SUBREFVALUEFormat",(String) bsData.get("TOTREFVALUEFormat"));
				tempdata.put("SUBDIFAMTFormat",(String) bsData.get("TOTDIFAMTFormat"));
				tempdata.put("SUBLCYRATFormat",(String) bsData.get("TOTLCYRATFormat"));
				tempdata.put("SUBDIFAMT2Format",(String) bsData.get("TOTDIFAMT2Format"));
				tempdata.put("SUBFCYRATFormat",(String) bsData.get("TOTFCYRATFormat"));
				OverviewREC2.add(tempdata);
				overviewdata.put("OverviewREC", OverviewREC);
				overviewdata.put("OverviewREC2", OverviewREC2);
				
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("query_profitloss_balance_data error >> {}",e);
		}
		return bs;
	}

	/**
	 * 將字串轉成DOUBLE
	 */
	public double transferStringToDouble(String string) {
		log.debug("string={}", string);
		string = string.replaceAll(",", "");
		log.debug("string={}", string);

		double result = Double.valueOf(string);
		log.debug("result={}", result);

		return result;
	}

	/**
	 * 將DOUBLE轉成金額格式
	 */
	public double formatAmount(int digital, String carryType, double sourceAmount) {
		double resultAmount = 0;

		// 四捨五入
		if (carryType.equals("1")) {
			if (digital != 0) {
				double tempD = Math.pow(10, digital);
				resultAmount = Math.round(sourceAmount * tempD) / tempD;
			} else {
				resultAmount = Math.round(sourceAmount);
			}
		}
		// 無條件進位
		else if (carryType.equals("2")) {
			if (digital != 0) {
				double tempD = Math.pow(10, digital);

				if (sourceAmount >= 0) {
					resultAmount = Math.ceil(sourceAmount * tempD) / tempD;
				} else {
					resultAmount = Math.floor(sourceAmount * tempD) / tempD;
				}
			} else {
				if (sourceAmount >= 0) {
					resultAmount = Math.ceil(sourceAmount);
				} else {
					resultAmount = Math.floor(sourceAmount);
				}
			}
		}
		// 無條件捨去
		else if (carryType.equals("3")) {
			if (digital != 0) {
				double tempD = Math.pow(10, digital);

				if (sourceAmount >= 0) {
					resultAmount = Math.floor(sourceAmount * tempD) / tempD;
				} else {
					resultAmount = Math.ceil(sourceAmount * tempD) / tempD;
				}
			} else {
				if (sourceAmount >= 0) {
					resultAmount = Math.floor(sourceAmount);
				} else {
					resultAmount = Math.ceil(sourceAmount);
				}
			}
		}
		log.debug("resultAmount={}", resultAmount);

		return resultAmount;
	}

	/**
	 * 海外債券餘額及損益查詢
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult oversea_balance_result(String cusidn, Map<String, String> reqParam) {
		log.trace("oversea_balance_result_service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = B012_REST(cusidn, reqParam, null);
			if (bs != null && bs.getResult()) {
				Map<String, Object> callRow = (Map) bs.getData();
				ArrayList<Map<String, String>> callList = (ArrayList<Map<String, String>>) callRow.get("REC");
				for (Map<String, String> data : callList) {
					data.put("HO01", WebUtil.hideAccount(data.get("O01")));
					data.put("O07", NumericUtil.fmtAmount(NumericUtil.addDot(data.get("O07"), 2), 2));
					data.put("O08OLD", data.get("O08"));
					data.put("O08", adm_currency_dao.getByPK(data.get("O08")).getADCCYNAME());
					//20210923 若O19=Y 委賣處理中 >> O09 , O12 ,O17 >> 顯示委賣處理中 ; O10 , O14 變空
					if("Y".equals(data.get("O19"))) {
						data.put("O09",  i18n.getMsg("LB.X2476"));
						data.put("O17", i18n.getMsg("LB.X2476"));
						data.put("O12",  i18n.getMsg("LB.X2476"));
						data.put("O10", "　");
						data.put("O14","　");
					}else {
						data.put("O09", NumericUtil.fmtAmount(NumericUtil.addDot(data.get("O09"), 4), 4)+"%");
						data.put("O10", DateUtil.convertDate(2, data.get("O10"), "yyyMMdd", "yyy/MM/dd"));
						data.put("O14",NumericUtil.fmtAmount(NumericUtil.addDot(data.get("O14").replaceAll(" ", ""), 2), 2) + "%");
						data.put("O17",NumericUtil.fmtAmount(NumericUtil.addDot(data.get("O17").replaceAll(" ", ""), 2), 2));
						data.put("O12", NumericUtil.fmtAmount(NumericUtil.addDot(data.get("O12"), 2), 2));
					}
					data.put("O05", NumericUtil.fmtAmount(NumericUtil.addDot(data.get("O05"), 2), 2));
					data.put("O13",NumericUtil.fmtAmount(NumericUtil.addDot(data.get("O13").replaceAll(" ", ""), 2), 2) + "%");
					data.put("O11", NumericUtil.fmtAmount(NumericUtil.addDot(data.get("O11"), 2), 2));
					data.put("O15", DateUtil.convertDate(2, data.get("O15"), "yyyMMdd", "yyy/MM/dd"));
					data.put("O16",NumericUtil.fmtAmount(NumericUtil.addDot(data.get("O16").replaceAll(" ", ""), 2), 2));
					data.put("O20",NumericUtil.fmtAmount(NumericUtil.addDot(data.get("O20").replaceAll(" ", ""), 2), 2));
				}
				
				callRow.put("DATE", DateUtil.convertDate(2, (String)callRow.get("DATE"), "yyyMMdd", "yyy/MM/dd"));
				callRow.put("TIME", DateUtil.formatTime((String)callRow.get("TIME")));
				callRow.put("TOTRECNO", NumericUtil.fmtAmount((String) callRow.get("TOTRECNO"), 0));
				callRow.put("CUSNAME", WebUtil.hideusername1Convert((String) callRow.get("CUSNAME")));
			}
		} catch (Exception e) {
			log.error("time_deposit", e);
		}
		return bs;
	}

	/**
	 * 海外債券交易紀錄查詢
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult oversea_data_result(String cusidn, Map<String, String> reqParam) {
		log.trace("oversea_balance_result_service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			reqParam.put("BEGDATE", DateUtil.convertDate(1, reqParam.get("CMSDATE"), "yyyy/MM/dd", "yyy/MM/dd"));
			reqParam.put("ENDDATE", DateUtil.convertDate(1, reqParam.get("CMEDATE"), "yyyy/MM/dd", "yyy/MM/dd"));
			bs = B013_REST(cusidn, reqParam);
			if (bs != null && bs.getResult()) {
				Map<String, Object> callRow = (Map) bs.getData();
				ArrayList<Map<String, String>> callList = (ArrayList<Map<String, String>>) callRow.get("REC");
				for (Map<String, String> data : callList) {
					data.put("O01", DateUtil.convertDate(2, data.get("O01"), "yyyMMdd", "yyy/MM/dd"));
					data.put("CREDITNO", data.get("O03"));
					data.put("HO03", WebUtil.hideAccount(data.get("O03")));
					data.put("O06", B013translater(data.get("O06")));
					data.put("O07", (new BigDecimal(data.get("O07")).divide(new BigDecimal("10000"))).toString());
					data.put("O07", NumericUtil.fmtAmount(data.get("O07"), 4));
					data.put("O08", (new BigDecimal(data.get("O08")).divide(new BigDecimal("100"))).toString());
					data.put("O08", NumericUtil.fmtAmount(data.get("O08"), 2));
					data.put("O09", (new BigDecimal(data.get("O09")).divide(new BigDecimal("10000"))).toString());
					data.put("O09", NumericUtil.fmtAmount(data.get("O09"), 4));
					data.put("O10", (new BigDecimal(data.get("O10")).divide(new BigDecimal("10000"))).toString());
					data.put("O10", NumericUtil.fmtAmount(data.get("O10"), 4));
					data.put("O11", (new BigDecimal(data.get("O11")).divide(new BigDecimal("100"))).toString());
					data.put("O11", NumericUtil.fmtAmount(data.get("O11"), 2));
					data.put("O12", (new BigDecimal(data.get("O12")).divide(new BigDecimal("10000"))).toString());
					data.put("O12", NumericUtil.fmtAmount(data.get("O12"), 4));
					data.put("O13", (new BigDecimal(data.get("O13")).divide(new BigDecimal("100"))).toString());
					data.put("O13", NumericUtil.fmtAmount(data.get("O13"), 2));
					data.put("O14", (new BigDecimal(data.get("O14")).divide(new BigDecimal("10000"))).toString());
					data.put("O14", NumericUtil.fmtAmount(data.get("O14"), 4));
					data.put("O15", (new BigDecimal(data.get("O15")).divide(new BigDecimal("100"))).toString());
					data.put("O15", NumericUtil.fmtAmount(data.get("O15"), 2));
					data.put("O16", (new BigDecimal(data.get("O16").replaceAll(" ", "")).divide(new BigDecimal("100")))
							.toString());
					data.put("O16", NumericUtil.fmtAmount(data.get("O16"), 2));
					data.put("O17", (new BigDecimal(data.get("O17")).divide(new BigDecimal("100"))).toString());
					data.put("O17", NumericUtil.fmtAmount(data.get("O17"), 2));
					data.put("O18", adm_currency_dao.getByPK(data.get("O18")).getADCCYNAME());
					data.put("O20", DateUtil.convertDate(2, data.get("O20"), "yyyMMdd", "yyy/MM/dd"));
					data.put("O21", (new BigDecimal(data.get("O21").replaceAll(" ", "")).divide(new BigDecimal("100")))
							.toString());
					data.put("O21", NumericUtil.fmtAmount(data.get("O21"), 2));
					data.put("O22", DateUtil.convertDate(2, data.get("O22"), "yyyMMdd", "yyy/MM/dd"));
					
				}

				String hiddencusidn=WebUtil.hideID((String)((Map<String, Object>) bs.getData()).get("CUSIDN"));
				String hiddenname=WebUtil.hideusername1Convert((String)((Map<String, Object>) bs.getData()).get("CUSNAME"));
				callRow.put("hiddencusidn",hiddencusidn);
				callRow.put("hiddenname",hiddenname);
			}

		} catch (Exception e) {
			log.error("time_deposit", e);
		}
		return bs;
	}
	/**
	 * 海外債券交易紀錄查詢-配息資料查詢
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult oversea_data_result_dividend(String cusidn, Map<String, String> reqParam) {
		log.trace("oversea_data_result_dividend START");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = B014_REST(cusidn, reqParam);
			//筆數
		
			if (bs != null && bs.getResult()) {
				Map<String, Object> callRow = (Map) bs.getData();
				callRow.put("CMRECNUM", NumericUtil.formatNumberString((String)callRow.get("TOTRECNO"), 0));
				String hiddencusidn=WebUtil.hideID((String)((Map<String, Object>) bs.getData()).get("CUSIDN"));
				String hiddenname=WebUtil.hideusername1Convert((String)((Map<String, Object>) bs.getData()).get("CUSNAME"));
				callRow.put("hiddencusidn",hiddencusidn);
				callRow.put("hiddenname",hiddenname);
				ArrayList<Map<String, String>> callList = (ArrayList<Map<String, String>>) callRow.get("REC");
				for (Map<String, String> data : callList) {
					data.put("O01", DateUtil.convertDate(2, data.get("O01"), "yyyMMdd", "yyy/MM/dd"));
					data.put("O03", WebUtil.hideAccount(data.get("O03")));
					data.put("O08", NumericUtil.fmtAmount(data.get("O08"), 0));
					data.put("O02", DateUtil.convertDate(2, data.get("O02"), "yyyMMdd", "yyy/MM/dd"));
					data.put("O09", (new BigDecimal(data.get("O09")).divide(new BigDecimal("1000000"))).toString());
					data.put("O09", NumericUtil.fmtAmount(data.get("O09"), 6));
					data.put("O09", data.get("O09")+"%");
					data.put("O14", (new BigDecimal(data.get("O14")).divide(new BigDecimal("10000"))).toString());
					data.put("O14", NumericUtil.fmtAmount(data.get("O14"), 4));
					data.put("O10", DateUtil.convertDate(2, data.get("O10"), "yyyMMdd", "yyy/MM/dd"));
					data.put("O11", DateUtil.convertDate(2, data.get("O11"), "yyyMMdd", "yyy/MM/dd"));
					//特例 O13可能有很多個0
					if(data.get("O13").equals("0000000")) {
						data.put("O13", "000/00/00");
					}
					else {
					data.put("O13", DateUtil.convertDate(2, data.get("O13"), "yyyMMdd", "yyy/MM/dd"));	
					}
				}

			}

		} catch (Exception e) {
			log.error("time_deposit", e);
		}
		return bs;
	}
	/**
	 * 海外債券交易紀錄查詢-歷史資料查詢
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult oversea_data_result_history(String cusidn, Map<String, String> reqParam) {
		log.trace("oversea_data_result_history START");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = B015_REST(cusidn, reqParam);
			if (bs != null && bs.getResult()) {
				Map<String, Object> callRow = (Map) bs.getData();
				callRow.put("CMRECNUM", NumericUtil.formatNumberString((String)callRow.get("TOTRECNO"), 0));
				String hiddencusidn=WebUtil.hideID(reqParam.get("CUSIDN"));
				String hiddenname=WebUtil.hideusername1Convert((String)((Map<String, Object>) bs.getData()).get("CUSNAME"));
				callRow.put("hiddencusidn",hiddencusidn);
				callRow.put("hiddenname",hiddenname);
				ArrayList<Map<String, String>> callList = (ArrayList<Map<String, String>>) callRow.get("REC");
				for (Map<String, String> data : callList) {
					data.put("O01", DateUtil.convertDate(2, data.get("O01"), "yyyMMdd", "yyy/MM/dd"));
					data.put("O03", WebUtil.hideAccount(data.get("O03")));
					if(data.get("O06").equals("B")) {
						data.put("O06", i18n.getMsg("LB.X1850"));
					}
					if(data.get("O06").equals("S")) {
						data.put("O06", i18n.getMsg("LB.X1851"));
					}
					data.put("O12", (new BigDecimal(data.get("O12")).divide(new BigDecimal("10000"))).toString());
					data.put("O12", NumericUtil.fmtAmount(data.get("O12"), 4));
					data.put("O11", (new BigDecimal(data.get("O11")).divide(new BigDecimal("100"))).toString());
					data.put("O11", NumericUtil.fmtAmount(data.get("O11"), 2));
					//正號空格濾掉
					if(data.get("O16").indexOf("-")==-1) {
						data.put("O16", data.get("O16").trim());
					}
					data.put("O16", (new BigDecimal(data.get("O16")).divide(new BigDecimal("100"))).toString());
					data.put("O16", NumericUtil.fmtAmount(data.get("O16"), 2));
					data.put("O13", (new BigDecimal(data.get("O13")).divide(new BigDecimal("100"))).toString());
					data.put("O13", NumericUtil.fmtAmount(data.get("O13"), 2));
					data.put("O14", (new BigDecimal(data.get("O14")).divide(new BigDecimal("10000"))).toString());
					data.put("O14", NumericUtil.fmtAmount(data.get("O14"), 4));
					data.put("O14", data.get("O14")+"%");
					data.put("O15", (new BigDecimal(data.get("O15")).divide(new BigDecimal("100"))).toString());
					data.put("O15", NumericUtil.fmtAmount(data.get("O15"), 2));
					data.put("O20", DateUtil.convertDate(2, data.get("O20"), "yyyMMdd", "yyy/MM/dd"));
					data.put("O21", (new BigDecimal(data.get("O21")).divide(new BigDecimal("100"))).toString());
					data.put("O21", NumericUtil.fmtAmount(data.get("O21"), 2));
					data.put("O17", (new BigDecimal(data.get("O17")).divide(new BigDecimal("100"))).toString());
					data.put("O17", NumericUtil.fmtAmount(data.get("O17"), 2));
					data.put("O22", DateUtil.convertDate(2, data.get("O22"), "yyyMMdd", "yyy/MM/dd"));
					
				}
			}

		} catch (Exception e) {
			log.error("time_deposit", e);
		}
		return bs;
	}
	/**
	 * 定期投資約定資料查詢
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult invest_query_result(Map<String, String> reqParam) {
		log.trace("invest_query_result_service");
		log.debug(ESAPIUtil.vaildLog("reqParam >>"+ reqParam));
		BaseResult bs = null;
		BaseResult bs2 = null;
//		2019/10/15 修正為多語系
//		String[] curNameEN = { "NTD", "USD", "AUD", "CAD", "HKD", "GBP", "SGD", "ZAR", "SEK", "CHF", "JPY", "THB",
//				"EUR", "NZD", "CNY" };
//		String[] curNameTW = { "新台幣", "美金", "澳幣", "加拿大幣", "港幣", "英磅", "新加坡幣", "	南非幣", "南非幣", "瑞士法郎", "日圓", "泰幣",
//				"歐洲通貨-歐元", "紐幣", "人民幣" };
		try {
			bs = new BaseResult();
			bs2 = new BaseResult();
			bs2 = N922_REST(reqParam.get("CUSIDN"));
			bs = C115_REST(reqParam);
			//N922因原住民姓名修改由NAME->CUSNAME
			if (!"".equals(((Map<String, String>) bs2.getData()).get("CUSNAME"))) {
				String cusname = ((Map<String, String>) bs2.getData()).get("CUSNAME");
				bs.addData("NAME", WebUtil.hideusername1Convert(cusname));
			}else {
				String cusname = ((Map<String, String>) bs2.getData()).get("NAME");
				bs.addData("NAME", WebUtil.hideusername1Convert(cusname));
			}
			Map<String, Object> callRow = (Map) bs.getData();
			callRow.put("TOTRECNO", NumericUtil.fmtAmount((String) callRow.get("TOTRECNO"), 0));
			ArrayList<Map<String, String>> callList = (ArrayList<Map<String, String>>) callRow.get("REC");
			for (Map<String, String> data : callList) {
				// CONDAY , FSTDAY , STPDAY , STOPBEGDAY , STOPENDDAY format "00000000"=> "" ,
				// else => ex:01070710 => 107/07/10
				data.put("CONDAY", C115_Date_Translate(data.get("CONDAY")));
				data.put("FSTDAY", C115_Date_Translate(data.get("FSTDAY")));
				data.put("STPDAY", C115_Date_Translate(data.get("STPDAY")));
				data.put("STOPBEGDAY", C115_Date_Translate(data.get("STOPBEGDAY")));
				data.put("STOPENDDAY", C115_Date_Translate(data.get("STOPENDDAY")));
				// PAYAMT format ex:6000 => 6,000
				data.put("PAYAMT", NumericUtil.fmtAmount(data.get("PAYAMT"), 2));
				// PAYACNO , CREDITNO format notice:PAYACNO has two condition
				data.put("PAYACNO", C115_ACCNO_Translate(data.get("PAYACNO")));
				data.put("CDNOCover", C115_ACCNO_Translate(data.get("CDNO")));
				// PAYTAG format => database table TXNFUNDDATA column FUNDLNAME
				data.put("PAYTAG", txn_funddata_dao.getByPK(data.get("PAYTAG")).getFUNDLNAME());
				// PAYDAY format "00"=>""
				for (int i = 1; i <= data.keySet().size(); i++) {
					if ("00".equals(data.get("PAYDAY" + i))) {
						data.put("PAYDAY" + i, "");
					}
				}
//				 TODO:以下之後要多語系 (幣別再詢問是否有好的寫法)
//				 PAYCUR 電文給英文 但是 要多語系 ... 之後處理 資料庫內目前為15種幣別
//				for (int i = 0; i < curNameEN.length; i++) {
//					if (curNameEN[i].equals(data.get("PAYCUR"))) {
//						data.put("PAYCUR", curNameTW[i]);
//						break;
//					}
//				}
				//2019/10/15 修正幣別多語系
				ADMCURRENCY admCurrency = admCurrencyDao.getByPK(data.get("PAYCUR"));
				
				if(admCurrency != null){
					Locale currentLocale = LocaleContextHolder.getLocale();
		    		String locale = currentLocale.toString();
		    		String ADCCYNAME ="";
		    		 switch (locale) {
						case "en":
							 ADCCYNAME = admCurrency.getADCCYENGNAME();
							break;
						case "zh_TW":
							 ADCCYNAME = admCurrency.getADCCYNAME();
							break;
						case "zh_CN":
							 ADCCYNAME = admCurrency.getADCCYCHSNAME();
							break;
		                }
					data.put("PAYCUR",ADCCYNAME);
				}
				// MIP format => Y = 定期不定額 ""= 定期定額
				switch (data.get("MIP")) {
				case "Y":
					data.put("MIP", "LB.W1087");
					break;
				case "":
					data.put("MIP", "LB.W1086");
					break;
				}
				// DBTTYPE format "1" => 存款 , "3" => 信用卡
				switch (data.get("DBTTYPE")) {
				case "1":
					data.put("DBTTYPE", "LB.D1393");
					break;
				case "3":
					data.put("DBTTYPE", "LB.Credit_Card");
					break;
				}
				// REMARK1 format "01" => 終止扣款 , "02" => 到期停止 , "03" => 連續扣款失敗 終止扣款
				switch (data.get("REMARK1")) {
				case "01":
					data.put("REMARK1", "LB.W1164");
					break;
				case "02":
					data.put("REMARK1", "LB.X1848");
					break;
				case "03":
					data.put("REMARK1", "LB.X1849");
					break;
				}
				// REMARK2 format "01" => "N" , "02" => "Y"
				switch (data.get("REMARK2")) {
				case "01":
					data.put("REMARK2", "N");
					break;
				case "02":
					data.put("REMARK2", "Y");
					break;
				}
			}

		} catch (Exception e) {
			log.error("invest_query_result_service_somethigWrong", e);

		}
		return bs;
	}

	public String C115_Date_Translate(String date) {
		if ("00000000".equals(date)) {
			return " ";
		} else {
			return DateUtil.addSlash2(date);
		}
	}

	public String C115_ACCNO_Translate(String ACCNO) {
		if (ACCNO.length() > 11) {
			return CodeUtil.hidecardnum(ACCNO);
		} else {
			return CodeUtil.hideaccount(ACCNO);
		}
	}

	public String B013translater(String Status) {
		if (Status.equals("B"))
			return i18n.getMsg("LB.X1850");//申購
		else if (Status.equals("S"))
			return i18n.getMsg("LB.X1851");//贖回
		return "";
	}
	
	
	/**
	 * C013 基金交易資料查詢
	 * 
	 * @param cusidn
	 * @param begData 查詢起日
	 * @param endDate 查詢迄日
	 * @return
	 */
	public BaseResult transfer_data_query(String cusidn, String begData, String endDate)
	{
		BaseResult bs = new BaseResult();
		try
		{
			// 西元轉民國
			begData = DateUtil.convertDate(1, begData, "yyyy/MM/dd", "yyyMMdd");
			endDate = DateUtil.convertDate(1, endDate, "yyyy/MM/dd", "yyyMMdd");
			
			C013_REST_RQ rq = new C013_REST_RQ();
			rq.setCUSIDN(cusidn);
			rq.setBEGDATE(begData);
			rq.setENDDATE(endDate);
			bs = C013_REST(rq);
			
			if(bs != null && bs.getResult())
			{
				bs = resultPrintData(bs);
			}
		}
		catch (Exception e)
		{
			log.error("transfer_data_query Error", e);
		}
		return bs;
	}
	
	
	/**
	 * C013_2 歷史交易明細
	 * 
	 * @param cusidn
	 * @param creditNo 信託帳號
	 * @return
	 */
	public BaseResult transfer_data_query_history(String cusidn, String cdno)
	{
		BaseResult bs = new BaseResult();
		try
		{
			C013_REST_RQ rq = new C013_REST_RQ();
			rq.setCUSIDN(cusidn);
			rq.setCDNO(cdno);
			bs = C013_REST(rq);
			
			if(bs != null && bs.getResult())
			{
				bs = historyPrintData(bs);
			}
		}
		catch (Exception e)
		{
			log.error("transfer_data_query Error", e);
		}
		return bs;
	}
	
	
	/**
	 * 單筆交易明細
	 * 
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult transfer_data_query_one(String cusidn, Map<String, String> reqParam)
	{
		BaseResult bs = new BaseResult();
		try
		{
			String json = CodeUtil.toJson(reqParam);
			C014_REST_RQ rq = CodeUtil.fromJson(json, C014_REST_RQ.class);
			rq.setCUSIDN(cusidn);
			
			bs = C014_REST(rq);
		}
		catch (Exception e)
		{
			log.error("transfer_data_query_one Error", e);
		}
		return bs;
	}
	
	
	public BaseResult resultPrintData(BaseResult bs)
	{
		BaseResult result = bs;
		Map bsData = (Map) bs.getData();
		List<Map> rec = (List<Map>) bsData.get("REC");
		LinkedList<C013_REST_RSDATA> rsRec = ((C013_REST_RS) bsData.get("RS")).getREC();
		
		Map<String, String> row = null;
		C013_REST_RSDATA rs = null;
		// 將格式化後的字串加到BaseResult物件內
		for(int i=0; i < rec.size(); i++)
		{
			row = rec.get(i);
			rs = rsRec.get(i);
			row.put("TRADEDATE_F", 				rs.getTRADEDATE_F());
			row.put("CDNO_F", 					rs.getCDNO_F());
			row.put("TRANSCODE_FUNDLNAME",		rs.getTRANSCODE_FUNDLNAME());
			row.put("TR106_F", 					rs.getTR106_F());
			row.put("FUNDTYPE_F", 				rs.getFUNDTYPE_F());
			row.put("CRY_CRYNAME", 				rs.getCRY_CRYNAME());
			row.put("FUNDAMT_F", 				rs.getFUNDAMT_F());
			row.put("UNIT_F", 					rs.getUNIT_F());
			row.put("PRICE1_F", 				rs.getPRICE1_F());
			row.put("EXRATE_F", 				rs.getEXRATE_F());
			row.put("INTRANSCODE_FUNDLNAME", 	rs.getINTRANSCODE_FUNDLNAME());
			row.put("TXUNIT_F", 				rs.getTXUNIT_F());
			row.put("INTRANSCRY_CRYNAME", 		rs.getINTRANSCRY_CRYNAME());
			row.put("PRICE2_F", 				rs.getPRICE2_F());
		}
		return result;
	}
	
	
	private BaseResult historyPrintData(BaseResult bs) 
	{
		BaseResult result = bs;
		Map bsData = (Map) bs.getData();
		List<Map> rec = (List<Map>) bsData.get("REC");
		LinkedList<C013_REST_RSDATA> rsRec = ((C013_REST_RS) bsData.get("RS")).getREC();
		
		Map<String, String> row = null;
		C013_REST_RSDATA rs = null;
		// 將格式化後的字串加到BaseResult物件內
		for(int i=0; i < rec.size(); i++)
		{
			row = rec.get(i);
			rs = rsRec.get(i);
			row.put("TRADEDATE_F", 				rs.getTRADEDATE_F());
			row.put("TRANSCODE_FUNDLNAME",		rs.getTRANSCODE_FUNDLNAME());
			row.put("TR106_F", 					rs.getTR106_F());
			row.put("FUNDTYPE_F", 				rs.getFUNDTYPE_F());
			row.put("CRY_CRYNAME", 				rs.getCRY_CRYNAME());
			row.put("FUNDAMT_F", 				rs.getFUNDAMT_F());
			row.put("UNIT_F", 					rs.getUNIT_F());
			row.put("PRICE1_F", 				rs.getPRICE1_F());
			row.put("EXRATE_F", 				rs.getEXRATE_F());
			row.put("FEECRY_CRYNAME", 			rs.getFEECRY_CRYNAME());
			row.put("FEEAMT_F", 				rs.getFEEAMT_F());
			row.put("PAYACN_F", 				rs.getPAYACN_F());
			row.put("INTRANSCODE_FUNDLNAME", 	rs.getINTRANSCODE_FUNDLNAME());
			row.put("TXUNIT_F", 				rs.getTXUNIT_F());
			row.put("INTRANSCRY_CRYNAME", 		rs.getINTRANSCRY_CRYNAME());
			row.put("PRICE2_F", 				rs.getPRICE2_F());
		}
		return result;
	}
	
	/**
	 * N381預約交易查詢/取消(起始頁)
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult fund_reservation_query(String cusidn, Map<String, String> reqParam) {
		Map<String, Object> mapN922 = null;
		log.trace("fund_query_service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = N922_REST(cusidn);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * N382預約交易查詢/取消(申購資料頁)
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult fund_reservation_purchase(String cusidn, Map<String, String> reqParam) {
		Map<String, Object> mapN382 = null;
		log.trace("fund_query_service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = N382_REST(cusidn, reqParam);
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				TXNFUNDDATA txnFundData;
				ADMCURRENCY admCurrency;
				//移除空資料
				for (int i = 0;i < rows.size();i++) {
					if(rows.get(i).get("OUTACN").equals("") || rows.get(i).get("OUTACN").equals(null)
							|| rows.get(i).get("TRANSCODE").equals("") || rows.get(i).get("TRANSCODE").equals(null)) {
						rows.remove(i);
						i--;
					}
				}
				String Count = String.valueOf(rows.size());
				bs.addData("COUNT", Count);
				for(Map<String, String> row : rows) {
					try {
						String TRANSCODE = row.get("TRANSCODE");
						String CRY = row.get("CRY");
						String locale = LocaleContextHolder.getLocale().toString();
						txnFundData = fund_transfer_service.getFundData(TRANSCODE);
						admCurrency = fund_transfer_service.getCRYData(CRY);
//						String str_FUNDSNAME = txnFundData.getFUNDLNAME();
//						String str_TRANSCRY = admCurrency.getADCCYNAME();
						if(txnFundData != null) {
							row.put("FUNDLNAME", txnFundData.getFUNDLNAME());
						}
						if(admCurrency != null) {
							if(locale.equals("zh_CN")) {
								row.put("ADCCYNAME", admCurrency.getADCCYCHSNAME());
							}
							else if(locale.equals("en")) {
								row.put("ADCCYNAME", admCurrency.getADCCYENGNAME());
							}
							else {
								row.put("ADCCYNAME", admCurrency.getADCCYNAME());
							}
						}
//						row.put("FUNDLNAME", txnFundData.getFUNDLNAME());
//						row.put("ADCCYNAME", admCurrency.getADCCYNAME());
						//帳號遮罩處理
						row.put("OUTACN_1",WebUtil.hideAccount(row.get("OUTACN")));
						//金額調整
						row.put("AMT3_1", NumericUtil.fmtAmount(row.get("AMT3"), 2));
						row.put("FCA2_1", NumericUtil.formatNumberString(row.get("FCA2"), 2));
						row.put("AMT5_1", NumericUtil.fmtAmount(row.get("AMT5"), 2));
						//手續費率
						row.put("FCAFEE_1", NumericUtil.formatNumberString(row.get("FCAFEE"), 3));
						//日期
						if(row.get("TRADEDATE").startsWith("0")) {
							String date = row.get("TRADEDATE");
							date = date.substring(1, date.length());
							row.put("TRADEDATE_1", DateUtil.convertDate(2, date, "yyyMMdd", "yyy/MM/dd"));
						}
						else {
							row.put("TRADEDATE_1", DateUtil.convertDate(2, row.get("TRADEDATE"), "yyyMMdd", "yyy/MM/dd"));							
						}
					}
					catch(Exception e) {
						log.error("do fund_reservation_purchase row>>>", e);
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * N382預約交易查詢/取消(申購確認頁)
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult fund_reservation_purchase_c(String cusidn, Map<String, String> reqParam) {
		log.trace(ESAPIUtil.vaildLog("fund_query_service>>>{}"+reqParam));
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			log.debug(ESAPIUtil.vaildLog("ROWDATA: {}"+ reqParam.get("ROWDATA")));

			String value = reqParam.get("ROWDATA");
			value = value.substring(1, value.length() - 1); // remove curly brackets
			log.trace(ESAPIUtil.vaildLog("ROWDATA.value: " + value));
			String[] keyValuePairs = value.split(","); // split the string to creat key-value pairs
			log.trace(ESAPIUtil.vaildLog("ROWDATA.keyValuePairs: " + keyValuePairs));
			Map<String, String> map = new HashMap<>();
			for (String pair : keyValuePairs) // iterate over the pairs
			{
				String[] entry = pair.split("="); // split the pairs to get key and value

				String mapKey = "";
				String MapValue = "";
				log.trace("ROWDATA.entry.length: " + entry.length);
				if (entry.length == 2) {
					mapKey = "".equals(entry[0].trim()) ? "" : entry[0].trim();
					MapValue = "".equals(entry[1].trim()) ? "" : entry[1].trim();
				} else {
					mapKey = entry[0].trim();
				}
				
				log.trace(ESAPIUtil.vaildLog("ROWDATA.mapKey:{} "+ mapKey));
				log.trace(ESAPIUtil.vaildLog("ROWDATA.MapValue:{} "+ MapValue));
				map.put(mapKey, MapValue); // add them to the hashmap and trim whitespaces
				log.debug(ESAPIUtil.vaildLog("ROWDATA:{} "+ map));
			}
			//帳號遮罩處理
			map.put("OUTACN_1",WebUtil.hideAccount(map.get("OUTACN")));
			// 金額處理
			map.put("AMT3_1", NumericUtil.fmtAmount(map.get("AMT3"), 2));
			map.put("FCA2_1", NumericUtil.formatNumberString(map.get("FCA2"), 2));
			map.put("AMT5_1", NumericUtil.fmtAmount(map.get("AMT5"), 2));
			//手續費率
			map.put("FCAFEE_1", NumericUtil.formatNumberString(map.get("FCAFEE"), 3));
			bs.addData("dataSet", map);
			bs.setResult(true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * N382預約交易查詢/取消(申購結果頁)
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult fund_reservation_purchase_r(String cusidn, Map<String, String> reqParam) {
		log.trace(ESAPIUtil.vaildLog("fund_query_service>>>{}"+reqParam));
		BaseResult bs = null;
		try {
			bs = N392_REST(cusidn, reqParam);
			if(bs != null && bs.getResult()) {
				Map<String, String> bsData = (Map) bs.getData();
				TXNFUNDDATA txnFundData;
				ADMCURRENCY admCurrency;
				String TRANSCODE = bsData.get("TRANSCODE");
				String CRY = bsData.get("CRY");
				String locale = LocaleContextHolder.getLocale().toString();
				txnFundData = fund_transfer_service.getFundData(TRANSCODE);
				admCurrency = fund_transfer_service.getCRYData(CRY);
				if(txnFundData != null) {
					bsData.put("FUNDLNAME", txnFundData.getFUNDLNAME());
				}
				if(admCurrency != null) {
					if(locale.equals("zh_CN")) {
						bsData.put("ADCCYNAME", admCurrency.getADCCYCHSNAME());
					}
					else if(locale.equals("en")) {
						bsData.put("ADCCYNAME", admCurrency.getADCCYENGNAME());
					}
					else {
						bsData.put("ADCCYNAME", admCurrency.getADCCYNAME());
					}
				}
//				bsData.put("FUNDLNAME", txnFundData.getFUNDLNAME());
//				bsData.put("ADCCYNAME", admCurrency.getADCCYNAME());
				//帳號遮罩處理
				bsData.put("OUTACN_1",WebUtil.hideAccount(bsData.get("OUTACN")));
				//金額調整
				bsData.put("AMT3_1", NumericUtil.fmtAmount(bsData.get("AMT3"), 2));
				bsData.put("FCA2_1", NumericUtil.formatNumberString(bsData.get("FCA2"), 2));
				bsData.put("AMT5_1", NumericUtil.fmtAmount(bsData.get("AMT5"), 2));
				//手續費率
				bsData.put("FCAFEE_1", NumericUtil.formatNumberString(bsData.get("FCAFEE"), 3));
				//日期
				if(bsData.get("TRADEDATE").startsWith("0")) {
					String date = bsData.get("TRADEDATE");
					date = date.substring(1, date.length());
					bsData.put("TRADEDATE_1", DateUtil.convertDate(2, date, "yyyMMdd", "yyy/MM/dd"));
				}
				else {
					bsData.put("TRADEDATE_1", DateUtil.convertDate(2, bsData.get("TRADEDATE"), "yyyMMdd", "yyy/MM/dd"));							
				}
				bs.addAllData(bsData);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * N383預約交易查詢/取消(轉換資料頁)
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult fund_reservation_conversion(String cusidn, Map<String, String> reqParam) {
		Map<String, Object> mapN383 = null;
		log.trace("fund_query_service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = N383_REST(cusidn, reqParam);
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				TXNFUNDDATA txnFundData;
				TXNFUNDDATA txnFundData_i;
				ADMCURRENCY admCurrency;
				//移除空資料
				for (int i = 0;i < rows.size();i++) {
					if(rows.get(i).get("OUTACN").equals("") || rows.get(i).get("OUTACN").equals(null)
							|| rows.get(i).get("TRANSCODE").equals("") || rows.get(i).get("TRANSCODE").equals(null)) {
						rows.remove(i);
						i--;
					}
				}
				String Count = String.valueOf(rows.size());
				bs.addData("COUNT", Count);
				for(Map<String, String> row : rows) {
					try {
						String TRANSCODE = row.get("TRANSCODE");
						String INTRANSCODE = row.get("INTRANSCODE");
						String CRY = row.get("CRY");
						String locale = LocaleContextHolder.getLocale().toString();
						txnFundData = fund_transfer_service.getFundData(TRANSCODE);
						txnFundData_i = fund_transfer_service.getFundData(INTRANSCODE);
						admCurrency = fund_transfer_service.getCRYData(CRY);
//						String str_FUNDSNAME = txnFundData.getFUNDLNAME();
//						String str_TRANSCRY = admCurrency.getADCCYNAME();
						if(txnFundData != null) {
							row.put("FUNDLNAME", txnFundData.getFUNDLNAME());
						}
						if(txnFundData_i != null) {
							row.put("I_FUNDLNAME", txnFundData_i.getFUNDLNAME());
						}
						if(admCurrency != null) {
							if(locale.equals("zh_CN")) {
								row.put("ADCCYNAME", admCurrency.getADCCYCHSNAME());
							}
							else if(locale.equals("en")) {
								row.put("ADCCYNAME", admCurrency.getADCCYENGNAME());
							}
							else {
								row.put("ADCCYNAME", admCurrency.getADCCYNAME());
							}
						}
//						row.put("FUNDLNAME", txnFundData.getFUNDLNAME());
//						row.put("I_FUNDLNAME", txnFundData_i.getFUNDLNAME());
//						row.put("ADCCYNAME", admCurrency.getADCCYNAME());
						//帳號遮罩處理
						row.put("OUTACN_1",WebUtil.hideAccount(row.get("OUTACN")));
						row.put("CDNO_1",WebUtil.hideAccount(row.get("CDNO")));
						//金額調整
						row.put("FUNDAMT_1", NumericUtil.fmtAmount(row.get("FUNDAMT"), 2));
						row.put("AMT3_1", NumericUtil.formatNumberString(row.get("AMT3"), 2));
						row.put("FCA2_1", NumericUtil.fmtAmount(row.get("FCA2"), 2));
						row.put("FCA1_1", NumericUtil.fmtAmount(row.get("FCA1"), 2));
						row.put("AMT5_1", NumericUtil.fmtAmount(row.get("AMT5"), 2));
						//單位
						row.put("UNIT_1", NumericUtil.formatNumberString(row.get("UNIT"), 4));
						//日期
						if(row.get("TRADEDATE").startsWith("0")) {
							String date = row.get("TRADEDATE");
							date = date.substring(1, date.length());
							row.put("TRADEDATE_1", DateUtil.convertDate(2, date, "yyyMMdd", "yyy/MM/dd"));
						}
						else {
							row.put("TRADEDATE_1", DateUtil.convertDate(2, row.get("TRADEDATE"), "yyyMMdd", "yyy/MM/dd"));							
						}
						//贖回方式
						if(row.get("BILLSENDMODE").equals("1")) {
							row.put("BILLSENDMODE_1", i18n.getMsg("LB.All"));
						}
						else if(row.get("BILLSENDMODE").equals("2")) {
							row.put("BILLSENDMODE_1", i18n.getMsg("LB.X1852"));
						}
					}
					catch(Exception e) {
						log.error("do fund_reservation_conversion row>>>", e);
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * N383預約交易查詢/取消(轉換確認頁)
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult fund_reservation_conversion_c(String cusidn, Map<String, String> reqParam) {
		log.trace(ESAPIUtil.vaildLog("fund_query_service>>>{}"+reqParam));
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			log.debug(ESAPIUtil.vaildLog("ROWDATA: {}"+ reqParam.get("ROWDATA")));

			String value = reqParam.get("ROWDATA");
			value = value.substring(1, value.length() - 1); // remove curly brackets
			log.trace(ESAPIUtil.vaildLog("ROWDATA.value: " + value));
			String[] keyValuePairs = value.split(","); // split the string to creat key-value pairs
			log.trace(ESAPIUtil.vaildLog("ROWDATA.keyValuePairs: " + keyValuePairs));
			Map<String, String> map = new HashMap<>();
			for (String pair : keyValuePairs) // iterate over the pairs
			{
				String[] entry = pair.split("="); // split the pairs to get key and value

				String mapKey = "";
				String MapValue = "";
				log.trace("ROWDATA.entry.length: " + entry.length);
				if (entry.length == 2) {
					mapKey = "".equals(entry[0].trim()) ? "" : entry[0].trim();
					MapValue = "".equals(entry[1].trim()) ? "" : entry[1].trim();
				} else {
					mapKey = entry[0].trim();
				}
				log.trace(ESAPIUtil.vaildLog("ROWDATA.mapKey:{} "+ mapKey));
				log.trace(ESAPIUtil.vaildLog("ROWDATA.MapValue:{} "+ MapValue));
				map.put(mapKey, MapValue); // add them to the hashmap and trim whitespaces
				log.debug(ESAPIUtil.vaildLog("ROWDATA:{} "+ map));
			}
			//帳號遮罩處理
			map.put("OUTACN_1",WebUtil.hideAccount(map.get("OUTACN")));
			map.put("CDNO_1",WebUtil.hideAccount(map.get("CDNO")));
			//金額調整
			map.put("FUNDAMT_1", NumericUtil.fmtAmount(map.get("FUNDAMT"), 2));
			map.put("AMT3_1", NumericUtil.formatNumberString(map.get("AMT3"), 2));
			map.put("FCA2_1", NumericUtil.fmtAmount(map.get("FCA2"), 2));
			map.put("FCA1_1", NumericUtil.fmtAmount(map.get("FCA1"), 2));
			map.put("AMT5_1", NumericUtil.fmtAmount(map.get("AMT5"), 2));
			//單位
			map.put("UNIT_1", NumericUtil.formatNumberString(map.get("UNIT"), 4));
			bs.addData("dataSet", map);
			bs.setResult(true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * N383預約交易查詢/取消(轉換結果頁)
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult fund_reservation_conversion_r(String cusidn, Map<String, String> reqParam) {
		log.trace(ESAPIUtil.vaildLog("fund_query_service>>>{}"+reqParam));
		BaseResult bs = null;
		try {
			bs = N393_REST(cusidn, reqParam);
			if(bs != null && bs.getResult()) {
				Map<String, String> bsData = (Map) bs.getData();
				TXNFUNDDATA txnFundData;
				TXNFUNDDATA txnFundData_i;
				ADMCURRENCY admCurrency;
				String TRANSCODE = bsData.get("TRANSCODE");
				String INTRANSCODE = bsData.get("INTRANSCODE");
				String CRY = bsData.get("CRY");
				String locale = LocaleContextHolder.getLocale().toString();
				txnFundData = fund_transfer_service.getFundData(TRANSCODE);
				txnFundData_i = fund_transfer_service.getFundData(INTRANSCODE);
				admCurrency = fund_transfer_service.getCRYData(CRY);
				if(txnFundData != null) {
					bsData.put("FUNDLNAME", txnFundData.getFUNDLNAME());
				}
				if(txnFundData_i != null) {
					bsData.put("I_FUNDLNAME", txnFundData_i.getFUNDLNAME());
				}
				if(admCurrency != null) {
					if(locale.equals("zh_CN")) {
						bsData.put("ADCCYNAME", admCurrency.getADCCYCHSNAME());
					}
					else if(locale.equals("en")) {
						bsData.put("ADCCYNAME", admCurrency.getADCCYENGNAME());
					}
					else {
						bsData.put("ADCCYNAME", admCurrency.getADCCYNAME());
					}
				}
//				bsData.put("FUNDLNAME", txnFundData.getFUNDLNAME());
//				bsData.put("ADCCYNAME", admCurrency.getADCCYNAME());
//				bsData.put("I_FUNDLNAME", txnFundData_i.getFUNDLNAME());
				//帳號遮罩處理
				bsData.put("OUTACN_1",WebUtil.hideAccount(bsData.get("OUTACN")));
				bsData.put("CDNO_1",WebUtil.hideAccount(bsData.get("CDNO")));
				//金額調整
				bsData.put("FUNDAMT_1", NumericUtil.fmtAmount(bsData.get("FUNDAMT"), 2));
				bsData.put("AMT3_1", NumericUtil.formatNumberString(bsData.get("AMT3"), 2));
				bsData.put("FCA2_1", NumericUtil.fmtAmount(bsData.get("FCA2"), 2));
				bsData.put("FCA1_1", NumericUtil.fmtAmount(bsData.get("FCA1"), 2));
				bsData.put("AMT5_1", NumericUtil.fmtAmount(bsData.get("AMT5"), 2));
				//單位
				bsData.put("UNIT_1", NumericUtil.formatNumberString(bsData.get("UNIT"), 4));
				//日期
				if(bsData.get("TRADEDATE").startsWith("0")) {
					String date = bsData.get("TRADEDATE");
					date = date.substring(1, date.length());
					bsData.put("TRADEDATE_1", DateUtil.convertDate(2, date, "yyyMMdd", "yyy/MM/dd"));
				}
				else {
					bsData.put("TRADEDATE_1", DateUtil.convertDate(2, bsData.get("TRADEDATE"), "yyyMMdd", "yyy/MM/dd"));							
				}
				//贖回方式
				if(bsData.get("BILLSENDMODE").equals("1")) {
					bsData.put("BILLSENDMODE_1", i18n.getMsg("LB.All"));
				}
				else if(bsData.get("BILLSENDMODE").equals("2")) {
					bsData.put("BILLSENDMODE_1", i18n.getMsg("LB.X1852"));
				}
				bs.addAllData(bsData);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * N384預約交易查詢/取消(贖回資料頁)
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult fund_reservation_redemption(String cusidn, Map<String, String> reqParam) {
		Map<String, Object> mapN384 = null;
		log.trace("fund_query_service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = N384_REST(cusidn, reqParam);
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				TXNFUNDDATA txnFundData;
				ADMCURRENCY admCurrency;
				//移除空資料
				for (int i = 0;i < rows.size();i++) {
					if(rows.get(i).get("FUNDACN").equals("") || rows.get(i).get("FUNDACN").equals(null)
							|| rows.get(i).get("TRANSCODE").equals("") || rows.get(i).get("TRANSCODE").equals(null)) {
						rows.remove(i);
						i--;
					}
				}
				String Count = String.valueOf(rows.size());
				bs.addData("COUNT", Count);
				for(Map<String, String> row : rows) {
					try {
						String TRANSCODE = row.get("TRANSCODE");
						String CRY = row.get("CRY");
						String locale = LocaleContextHolder.getLocale().toString();
						txnFundData = fund_transfer_service.getFundData(TRANSCODE);
						admCurrency = fund_transfer_service.getCRYData(CRY);
//						String str_FUNDSNAME = txnFundData.getFUNDLNAME();
//						String str_TRANSCRY = admCurrency.getADCCYNAME();
						if(txnFundData != null) {
							row.put("FUNDLNAME", txnFundData.getFUNDLNAME());
						}
						if(admCurrency != null) {
							if(locale.equals("zh_CN")) {
								row.put("ADCCYNAME", admCurrency.getADCCYCHSNAME());
							}
							else if(locale.equals("en")) {
								row.put("ADCCYNAME", admCurrency.getADCCYENGNAME());
							}
							else {
								row.put("ADCCYNAME", admCurrency.getADCCYNAME());
							}
						}
//						row.put("FUNDLNAME", txnFundData.getFUNDLNAME());
//						row.put("ADCCYNAME", admCurrency.getADCCYNAME());
						//帳號遮罩處理
						row.put("FUNDACN_1",WebUtil.hideAccount(row.get("FUNDACN")));
						row.put("CDNO_1",WebUtil.hideAccount(row.get("CDNO")));
						//金額調整
						row.put("FUNDAMT_1", NumericUtil.fmtAmount(row.get("FUNDAMT"), 2));
						//單位
						row.put("UNIT_1", NumericUtil.formatNumberString(row.get("UNIT"), 4));
						//日期
						if(row.get("TRADEDATE").startsWith("0")) {
							String date = row.get("TRADEDATE");
							date = date.substring(1, date.length());
							row.put("TRADEDATE_1", DateUtil.convertDate(2, date, "yyyMMdd", "yyy/MM/dd"));
						}
						else {
							row.put("TRADEDATE_1", DateUtil.convertDate(2, row.get("TRADEDATE"), "yyyMMdd", "yyy/MM/dd"));							
						}
						//贖回方式
						if(row.get("BILLSENDMODE").equals("1")) {
							row.put("BILLSENDMODE_1", i18n.getMsg("LB.All"));
						}
						else if(row.get("BILLSENDMODE").equals("2")) {
							row.put("BILLSENDMODE_1", i18n.getMsg("LB.X1852"));
						}
					}
					catch(Exception e) {
						log.error("do fund_reservation_redemption row>>>", e);
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * N384預約交易查詢/取消(贖回確認頁)
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult fund_reservation_redemption_c(String cusidn, Map<String, String> reqParam) {
		log.trace(ESAPIUtil.vaildLog("fund_query_service>>>{}"+reqParam));
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			log.debug(ESAPIUtil.vaildLog("ROWDATA: {}"+ reqParam.get("ROWDATA")));

			String value = reqParam.get("ROWDATA");
			value = value.substring(1, value.length() - 1); // remove curly brackets
			log.trace(ESAPIUtil.vaildLog("ROWDATA.value: " + value));
			String[] keyValuePairs = value.split(","); // split the string to creat key-value pairs
			log.trace(ESAPIUtil.vaildLog("ROWDATA.keyValuePairs: " + keyValuePairs));
			Map<String, String> map = new HashMap<>();
			for (String pair : keyValuePairs) // iterate over the pairs
			{
				String[] entry = pair.split("="); // split the pairs to get key and value

				String mapKey = "";
				String MapValue = "";
				log.trace("ROWDATA.entry.length: " + entry.length);
				if (entry.length == 2) {
					mapKey = "".equals(entry[0].trim()) ? "" : entry[0].trim();
					MapValue = "".equals(entry[1].trim()) ? "" : entry[1].trim();
				} else {
					mapKey = entry[0].trim();
				}
				log.trace(ESAPIUtil.vaildLog("ROWDATA.mapKey:{} "+ mapKey));
				log.trace(ESAPIUtil.vaildLog("ROWDATA.MapValue:{} "+ MapValue));
				map.put(mapKey, MapValue); // add them to the hashmap and trim whitespaces
				log.debug(ESAPIUtil.vaildLog("ROWDATA:{} "+ map));
			}
			//帳號遮罩處理
			map.put("FUNDACN_1",WebUtil.hideAccount(map.get("FUNDACN")));
			map.put("CDNO_1",WebUtil.hideAccount(map.get("CDNO")));
			//金額調整
			map.put("FUNDAMT_1", NumericUtil.fmtAmount(map.get("FUNDAMT"), 2));
			//單位
			map.put("UNIT_1", NumericUtil.formatNumberString(map.get("UNIT"), 4));
			bs.addData("dataSet", map);
			bs.setResult(true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * N384預約交易查詢/取消(贖回結果頁)
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult fund_reservation_redemption_r(String cusidn, Map<String, String> reqParam) {
		log.trace(ESAPIUtil.vaildLog("fund_query_service>>>{}"+reqParam));
		BaseResult bs = null;
		try {
			bs = N394_REST(cusidn, reqParam);
			if(bs != null && bs.getResult()) {
				Map<String, String> bsData = (Map) bs.getData();
				TXNFUNDDATA txnFundData;
				ADMCURRENCY admCurrency;
				String TRANSCODE = bsData.get("TRANSCODE");
				String CRY = bsData.get("CRY");
				String locale = LocaleContextHolder.getLocale().toString();
				txnFundData = fund_transfer_service.getFundData(TRANSCODE);
				admCurrency = fund_transfer_service.getCRYData(CRY);
				if(txnFundData != null) {
					bsData.put("FUNDLNAME", txnFundData.getFUNDLNAME());
				}
				if(admCurrency != null) {
					if(locale.equals("zh_CN")) {
						bsData.put("ADCCYNAME", admCurrency.getADCCYCHSNAME());
					}
					else if(locale.equals("en")) {
						bsData.put("ADCCYNAME", admCurrency.getADCCYENGNAME());
					}
					else {
						bsData.put("ADCCYNAME", admCurrency.getADCCYNAME());
					}
				}
//				bsData.put("FUNDLNAME", txnFundData.getFUNDLNAME());
//				bsData.put("ADCCYNAME", admCurrency.getADCCYNAME());
				//帳號遮罩處理
				bsData.put("FUNDACN_1",WebUtil.hideAccount(bsData.get("FUNDACN")));
				bsData.put("CDNO_1",WebUtil.hideAccount(bsData.get("CDNO")));
				//金額調整
				bsData.put("FUNDAMT_1", NumericUtil.fmtAmount(bsData.get("FUNDAMT"), 2));
				//單位
				bsData.put("UNIT_1", NumericUtil.formatNumberString(bsData.get("UNIT"), 4));
				//日期
				if(bsData.get("TRADEDATE").startsWith("0")) {
					String date = bsData.get("TRADEDATE");
					date = date.substring(1, date.length());
					bsData.put("TRADEDATE_1", DateUtil.convertDate(2, date, "yyyMMdd", "yyy/MM/dd"));
				}
				else {
					bsData.put("TRADEDATE_1", DateUtil.convertDate(2, bsData.get("TRADEDATE"), "yyyMMdd", "yyy/MM/dd"));							
				}
				//贖回方式
				if(bsData.get("BILLSENDMODE").equals("1")) {
					bsData.put("BILLSENDMODE_1", i18n.getMsg("LB.All"));
				}
				else if(bsData.get("BILLSENDMODE").equals("2")) {
					bsData.put("BILLSENDMODE_1", i18n.getMsg("LB.X1852"));
				}
				bs.addAllData(bsData);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * B023
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult oversea_order_query(String cusidn, Map<String, String> reqParam) {
		log.trace(ESAPIUtil.vaildLog("fund_query_service>>>{}"+reqParam));
		BaseResult bs = null;
		try {
			bs = B023_REST(cusidn, reqParam);
			if(bs != null && bs.getResult()) {
				log.debug("B023 bs data >>>"+bs.getData());
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				for (Map<String, String> row : rows) {
					row.put("O01",DateUtil.convertDate(2, row.get("O01"), "yyyMMdd", "yyy/MM/dd"));
					if(row.get("O08").equals("B")) {
						row.put("O08", i18n.getMsg("LB.X2583"));
					}
					else if(row.get("O08").equals("S")) {
						row.put("O08", i18n.getMsg("LB.X2476"));
					}
					//信託帳號遮罩
					row.put("O02", WebUtil.hideAccount(row.get("O02")));
					//入/扣款帳號遮罩
					row.put("O14", WebUtil.hideAccount(row.get("O14")));
					
					String locale = LocaleContextHolder.getLocale().toString();
					ADMCURRENCY admCurrency = adm_currency_dao.getByPK(row.get("O15"));
					if(admCurrency != null){
						String ADCCYNAME = "";
						if(locale.equals("zh_CN")) {
							ADCCYNAME = admCurrency.getADCCYCHSNAME();
						}
						else if(locale.equals("en")) {
							ADCCYNAME = admCurrency.getADCCYENGNAME();
						}
						else {
							ADCCYNAME = admCurrency.getADCCYNAME();
						}
						row.put("O15_SHOW", ADCCYNAME);
					}
				}
				String count = NumericUtil.fmtAmount((String)bsData.get("TOTRECNO"), 0);
				bs.addData("TOTRECNO", count);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}

}