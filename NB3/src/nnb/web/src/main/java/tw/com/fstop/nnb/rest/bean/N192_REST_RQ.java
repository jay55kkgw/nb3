package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N192_REST_RQ extends BaseRestBean_GOLD implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2901610331372255467L;
	private String USERDATA;
	private String ACN;
	private String UID;
	private String TYPE;
	private String CMSDATE;
	private String CMEDATE;
	private String FGPERIOD;
	
	public String getFGPERIOD() {
		return FGPERIOD;
	}
	public void setFGPERIOD(String fGPERIOD) {
		FGPERIOD = fGPERIOD;
	}
	public String getCMSDATE() {
		return CMSDATE;
	}
	public void setCMSDATE(String cMSDATE) {
		CMSDATE = cMSDATE;
	}
	public String getCMEDATE() {
		return CMEDATE;
	}
	public void setCMEDATE(String cMEDATE) {
		CMEDATE = cMEDATE;
	}
	public String getUSERDATA() {
		return USERDATA;
	}
	public void setUSERDATA(String uSERDATA) {
		USERDATA = uSERDATA;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}

}
