package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N8301_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5606027615526672293L;
	
	private String TSFACN;//轉帳帳號
	private String TYPE;//查詢類別
	private String TYPNUM;//屬性代號
	private String HLHBRH;//分局別
	private String UNTNUM1;//投保單位代號
	private String CUSIDN1;//客戶身份證統一編號
	private String UNTNUM6;//所屬投保單位統一編號
	private String CUSIDN6;//主被保險人統一編號
	private String ITMNUM;//項目代號1:申請2:註銷3:變更
	private String DATE;//日期YYYMMDD
	private String TIME;//時間HHMMSS
	private String CMQTIME;
	
	public String getTSFACN() {
		return TSFACN;
	}
	public void setTSFACN(String tSFACN) {
		TSFACN = tSFACN;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getTYPNUM() {
		return TYPNUM;
	}
	public void setTYPNUM(String tYPNUM) {
		TYPNUM = tYPNUM;
	}
	public String getHLHBRH() {
		return HLHBRH;
	}
	public void setHLHBRH(String hLHBRH) {
		HLHBRH = hLHBRH;
	}
	public String getUNTNUM1() {
		return UNTNUM1;
	}
	public void setUNTNUM1(String uNTNUM1) {
		UNTNUM1 = uNTNUM1;
	}
	public String getCUSIDN1() {
		return CUSIDN1;
	}
	public void setCUSIDN1(String cUSIDN1) {
		CUSIDN1 = cUSIDN1;
	}
	public String getUNTNUM6() {
		return UNTNUM6;
	}
	public void setUNTNUM6(String uNTNUM6) {
		UNTNUM6 = uNTNUM6;
	}
	public String getCUSIDN6() {
		return CUSIDN6;
	}
	public void setCUSIDN6(String cUSIDN6) {
		CUSIDN6 = cUSIDN6;
	}
	public String getITMNUM() {
		return ITMNUM;
	}
	public void setITMNUM(String iTMNUM) {
		ITMNUM = iTMNUM;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
}
