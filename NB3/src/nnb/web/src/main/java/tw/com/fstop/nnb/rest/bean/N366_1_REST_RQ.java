package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N366_1_REST_RQ extends BaseRestBean_OLS implements Serializable {
	
	private static final long serialVersionUID = -2341366659705300267L;
	private String CUSIDN;				// 身份證號
	private String ACN;					// 欲結清帳號
	private String DPIBAL;				// 存款餘額
	private String DPIBALS;				// 存款餘額正負號 
	private String DPIINT;				// 存款息
	private String DPIINTS;				// 存款息正負號 
	private String ODFINT;				// 透支息
	private String ODFINTS;				// 透支息正負號
	private String AMTTAX;				// 存款息稅額
	private String AMTTAXS;				// 存款息稅額正負號
	private String AMTNHI;				// 二代健保費
	private String AMTNHIS;				// 二代健保費正負號
	private String AMTCLS;				// 結清淨額
	private String AMTCLSS;				// 結清淨額正負號
	private String INACN;				// 結清款轉入本行帳號
	
	// 交易機制相關欄位
	private String FGTXWAY;				// 交易機制
	private String iSeqNo;				// iSeqNo
	private String ISSUER;
	private String ACNNO;				// 晶片卡帳號
	private String CHIP_ACN;			// 晶片卡帳號(同ACNNO)
	private String TRMID;
	private String ICSEQ;				// 晶片卡序號
	private String TAC;
	private String pkcs7Sign;			// IKEY
	private String jsondc;				// IKEY
	
	// 電文沒使用但 ms_tw 需要的資料
	private String trancode = "N366";	// MAC2.java 押碼用
	private String ADOPID;
	private String _CUSIDN;
	
	
	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	public String getDPIBAL() {
		return DPIBAL;
	}

	public void setDPIBAL(String dPIBAL) {
		DPIBAL = dPIBAL;
	}

	public String getDPIBALS() {
		return DPIBALS;
	}

	public void setDPIBALS(String dPIBALS) {
		DPIBALS = dPIBALS;
	}

	public String getDPIINT() {
		return DPIINT;
	}

	public void setDPIINT(String dPIINT) {
		DPIINT = dPIINT;
	}

	public String getDPIINTS() {
		return DPIINTS;
	}

	public void setDPIINTS(String dPIINTS) {
		DPIINTS = dPIINTS;
	}

	public String getODFINT() {
		return ODFINT;
	}

	public void setODFINT(String oDFINT) {
		ODFINT = oDFINT;
	}

	public String getODFINTS() {
		return ODFINTS;
	}

	public void setODFINTS(String oDFINTS) {
		ODFINTS = oDFINTS;
	}

	public String getAMTTAX() {
		return AMTTAX;
	}

	public void setAMTTAX(String aMTTAX) {
		AMTTAX = aMTTAX;
	}

	public String getAMTTAXS() {
		return AMTTAXS;
	}

	public void setAMTTAXS(String aMTTAXS) {
		AMTTAXS = aMTTAXS;
	}

	public String getAMTNHI() {
		return AMTNHI;
	}

	public void setAMTNHI(String aMTNHI) {
		AMTNHI = aMTNHI;
	}

	public String getAMTNHIS() {
		return AMTNHIS;
	}

	public void setAMTNHIS(String aMTNHIS) {
		AMTNHIS = aMTNHIS;
	}

	public String getAMTCLS() {
		return AMTCLS;
	}

	public void setAMTCLS(String aMTCLS) {
		AMTCLS = aMTCLS;
	}

	public String getAMTCLSS() {
		return AMTCLSS;
	}

	public void setAMTCLSS(String aMTCLSS) {
		AMTCLSS = aMTCLSS;
	}

	public String getINACN() {
		return INACN;
	}

	public void setINACN(String iNACN) {
		INACN = iNACN;
	}

	public String getFGTXWAY() {
		return FGTXWAY;
	}

	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}

	public String getiSeqNo() {
		return iSeqNo;
	}

	public void setiSeqNo(String iSeqNo) {
		this.iSeqNo = iSeqNo;
	}

	public String getISSUER() {
		return ISSUER;
	}

	public void setISSUER(String iSSUER) {
		ISSUER = iSSUER;
	}

	public String getACNNO() {
		return ACNNO;
	}

	public void setACNNO(String aCNNO) {
		ACNNO = aCNNO;
	}

	public String getCHIP_ACN() {
		return CHIP_ACN;
	}

	public void setCHIP_ACN(String cHIP_ACN) {
		CHIP_ACN = cHIP_ACN;
	}

	public String getTRMID() {
		return TRMID;
	}

	public void setTRMID(String tRMID) {
		TRMID = tRMID;
	}

	public String getICSEQ() {
		return ICSEQ;
	}

	public void setICSEQ(String iCSEQ) {
		ICSEQ = iCSEQ;
	}

	public String getTAC() {
		return TAC;
	}

	public void setTAC(String tAC) {
		TAC = tAC;
	}

	public String getPkcs7Sign() {
		return pkcs7Sign;
	}

	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}

	public String getJsondc() {
		return jsondc;
	}

	public void setJsondc(String jsondc) {
		this.jsondc = jsondc;
	}
	
	public String getTrancode() {
		return trancode;
	}

	public void setTrancode(String trancode) {
		this.trancode = trancode;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}

	public String get_CUSIDN() {
		return _CUSIDN;
	}

	public void set_CUSIDN(String _CUSIDN) {
		this._CUSIDN = _CUSIDN;
	}
	
}