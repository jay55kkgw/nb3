package tw.com.fstop.nnb.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.po.TXNFXSCHPAY;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.tbb.nnb.dao.TxnFxSchPayDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.web.util.StrUtils;;

@Service
public class TxnFxSchPay_Service extends Base_Service {
	private Logger log = LoggerFactory.getLogger(this.getClass().getName());

	@Autowired
	TxnFxSchPayDao txnFxSchPayDao;

	/**
	 * 外幣預約明細查詢
	 * 
	 * @param reqParam
	 *            must include cusidn
	 * @return BaseResult
	 */
	public BaseResult query(Map<String, String> reqParam) {
		BaseResult bs = new BaseResult();
		try {
			String cusidn = reqParam.get("cusidn");
			// 判斷統編是否為空
			if (StrUtils.isNull(cusidn)) {
				bs.setMessage("請輸入使用者統編");
				throw new Exception();
			}
			List<TXNFXSCHPAY> list = txnFxSchPayDao.getallFxtxstatus(cusidn);
			bs.setData(list);
			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", "查詢成功");
		} catch (Exception e) {
			bs.setResult(Boolean.FALSE);
			bs.setMsgCode("1");
			if (bs.getMessage() == null) {
				bs.setMessage("查詢失敗");
			}
		}
		return bs;
	}

	/**
	 * 外幣預約明細新增
	 * 
	 * @param reqParam
	 *            must include cusidn,sdate,edate
	 * @return BaseResult
	 */
	@Transactional
	public BaseResult insert(Map<String, String> reqParam1) {
		BaseResult bs = new BaseResult();
		//如果帶過來是null 空的 並且 po是有這欄位 則帶上logintype=MB
		if(StrUtils.isNull(reqParam1.get("logintype"))) {
			reqParam1.put("logintype", "MB");
		}

		Map<String, String> reqParam = new HashMap<String, String>();

		// 轉POJO用
		Map reqmap = new HashMap();
		
		// list 先拿取POJO所有欄位 移除不需要的欄位 轉小寫放進 list2
		List<String> list = new ArrayList<String>();
		// list2
		List<String> list2 = new ArrayList<String>();
		// exlist移除不判斷空字串空值Null 之後另外判斷為null給""

		List<String> exlist = new ArrayList<String>();
		exlist.add("fxsvamt");
		exlist.add("fxwdamt");
		exlist.add("fxtxmemo");
		exlist.add("fxtxmails");
		exlist.add("fxtxmailmemo");
		exlist.add("fxpermtdate");
		exlist.add("xmlca");
		exlist.add("xmlcn");

		try {
			// if(txnFxSchPayDao.fxschnoIsNull(reqParam1.get("fxschno"))) {
			// bs.setMessage("fxschno不得重複");
			// throw new Exception();
			// };
			String fxtxtype = null;
			if (!StrUtils.isNull(reqParam1.get("fxpermtdate"))) {
				Double.valueOf(reqParam1.get("fxpermtdate"));
				reqParam1.put("fxtxtype", "C");
				fxtxtype = "C";
			} else {
				reqParam1.put("fxtxtype", "S");
				reqParam1.put("fxpermtdate", "");
				fxtxtype = "S";
			}
			String fxtxstatus = reqParam1.get("fxtxstatus");
			String fxpermtdate = reqParam1.get("fxpermtdate");
			String logintype = reqParam1.get("logintype");
			String fxtxcode = reqParam1.get("fxtxcode");
			String fxfdate = reqParam1.get("fxfdate");
			String fxtdate = reqParam1.get("fxtdate");
			list = StrUtils.getBeanName(TXNFXSCHPAY.class);
			// 如果帶進來的參數不在POJO內 就remove
			for (String str : reqParam1.keySet()) {
				if (list.contains(str.toUpperCase())) {
					reqParam.put(str, reqParam1.get(str));
				}
			}
			// 如果帶POJO內不需要的參數 就remove
			reqParam.remove("fxschid");
			reqParam.remove("msaddr");

			// 在放參數的list也刪除
			list.remove("FXSCHID");
			list.remove("MSADDR");

			for (String str : list) {
				list2.add(str.toLowerCase());
			}
			// 移除可能給null的欄位 之後另外處理
			for (String str : exlist) {
				list2.remove(str);
			}

			// 判斷是否有null或空字串 空白字串
			for (String key : list2) {
				if (StrUtils.isNull(reqParam.get(key))) {
					bs.setMessage(key + "不得有空");
					throw new Exception();
				}
			}

			// 如果為null則給空字串
			for (String str : exlist) {
				if (StrUtils.isNull(reqParam.get(str))) {
					reqParam.put(str, "");
				}
			}

			// 判斷數字
			// Double.valueOf(reqParam.get("fxwdamt"));
			// Double.valueOf(reqParam.get("fxsvamt"));
			// if(StrUtils.isNull(reqParam.get("fxsvamt"))){
			// reqParam.put("fxsvamt", "0");
			// }

			// 預約型態 C:期間 S:單筆
			if (!(("0".equals(fxtxstatus)) || "3".equals(fxtxstatus))) {
				// log.error(ESAPIUtil.vaildLog("fxtxtype>>{}"+
				// CodeUtil.toJson(reqParam.get("fxtxtype"))));
				bs.setMessage("請輸入正確的交易執行狀態 0:未執行 3:取消預約");
				throw new Exception();
			}
			// 預約型態 C:期間 S:單筆
			// if (!(("C".equals(fxtxtype)) || "S".equals(fxtxtype))) {
			// log.error(ESAPIUtil.vaildLog("fxtxtype>>{}"+
			// CodeUtil.toJson(reqParam.get("fxtxtype"))));
			// bs.setMessage("請輸入正確的預約型態 C:期間 S:單筆");
			// throw new Exception();
			// }
			// 預約型態C:期間 是否有每月轉帳日
			else if ("C".equals(fxtxtype)) {
				if (fxpermtdate == null || fxpermtdate.trim().length() == 0) {
					// log.error(ESAPIUtil.vaildLog("fxpermtdate>>{}"+ fxpermtdate));
					bs.setMessage("預約型態C:期間，請輸入每月轉帳日");
					throw new Exception();
				}
			}
			// 判斷是否為交易機制 0:交易密碼 1:IKEY 2:金融卡
			// else if (!(("0".equals(fxtxcode) || "1".equals(fxtxcode) ||
			// "2".equals(fxtxcode)))) {
			// log.error(ESAPIUtil.vaildLog("fxtxcode>>{}"+
			// CodeUtil.toJson(reqParam.get("fxtxcode"))));
			// bs.setMessage("請輸入正確的交易機制碼 0:交易密碼 1:IKEY 2:金融卡");
			// throw new Exception();
			// 判斷生效日是否大於截止日
			// }
			else if (Integer.parseInt(fxfdate) > Integer.parseInt(fxtdate)) {
				// log.error(ESAPIUtil.vaildLog("fxfdate>>{}"+ fxfdate));
				// log.error(ESAPIUtil.vaildLog("fxtdate>>{}"+fxtdate));
				bs.setMessage("生效日不得大於截止日");
				throw new Exception();
			}

			// logintype為null則強制給MB
			if(reqParam.containsKey("logintype") && StrUtils.isNull(reqParam.get("logintype"))) {
				reqParam.put("logintype", "MB");
			}

			// 二擇一 兩者沒有值就擋掉
			if (StrUtils.isNull(reqParam.get("fxsvamt")) && StrUtils.isNull(reqParam.get("fxwdamt"))) {
				bs.setMessage("fxsvamt 和 fxwdamt不得同時沒有值");
				throw new Exception();
			} else if (!StrUtils.isNull(reqParam.get("fxsvamt"))) {
				Double.valueOf(reqParam.get("fxsvamt"));
			} else if (!StrUtils.isNull(reqParam.get("fxwdamt"))) {
				Double.valueOf(reqParam.get("fxwdamt"));
			}

			// 配合POJO將reqParam的Key轉大寫
			for (Object obj : reqParam.keySet()) {
				reqmap.put(obj.toString().toUpperCase(),
						reqParam.get(obj.toString()) == null ? "" : reqParam.get(obj.toString()));
				// reqmap.put(obj.toString().toUpperCase(), reqParam.get(obj.toString()));
			}

			TXNFXSCHPAY txnfxschpay = CodeUtil.objectCovert(TXNFXSCHPAY.class, reqmap);

			// 外幣固定MSADDR為ms_fx
			txnfxschpay.setMSADDR("ms_fx");
			// txnfxschpay.setFXSCHNO(txnFxSchPayDao.findfxschno());
			txnFxSchPayDao.save(txnfxschpay);

			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", "新增成功");
		} catch (NumberFormatException e) {
			log.error("{}", e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMsgCode("1");
			bs.setMessage("fxpermtdate數字欄位請勿填非數字");
		} catch (Exception e) {
			log.trace("{}", e.toString());
			log.trace(ESAPIUtil.vaildLog("{}" + CodeUtil.toJson(reqParam)));
			bs.setResult(Boolean.FALSE);
			bs.setMsgCode("1");
			if (bs.getMessage() == null) {
				bs.setMessage("新增失敗");
			}
		}
		return bs;
	}

	public BaseResult cancel(Map<String, String> reqParam) {
		BaseResult bs = new BaseResult();
		String fxuserid = reqParam.get("fxuserid");// 流水號
		String fxschid = reqParam.get("fxschid");// 使用者統編

		try {
			if (StrUtils.isNull(fxuserid) || StrUtils.isNull(fxschid)) {
				bs.setMessage("流水號或使用者統編不得有空");
				throw new Exception();
			}
			// log.trace(ESAPIUtil.vaildLog("reqParam>>{}"+CodeUtil.toJson(reqParam)));
			txnFxSchPayDao.cancelByCusidn(fxuserid, fxschid);
			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", "取消成功");
		} catch (Exception e) {
			bs.setResult(Boolean.FALSE);
			bs.setMsgCode("1");
			if (bs.getMessage() == null) {
				bs.setMessage("取消失敗");
			}
		}
		return bs;
	}

}
