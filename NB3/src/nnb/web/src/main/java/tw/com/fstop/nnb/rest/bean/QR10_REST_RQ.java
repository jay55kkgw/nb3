package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * 
 * @author Ian
 *
 */
public class QR10_REST_RQ extends BaseRestBean_CC implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9017761440065200747L;

	private String CARDNUM;// 銷帳編號

	private String CUSIDN;// 統一編號

	private String BankID;// 銀行代碼

	private String trin_acn;// 轉出帳號

	private String AMOUNT;// 繳款金額

	private String DATE;// YYYDDMM

	private String TIME;//

	public String getCARDNUM()
	{
		return CARDNUM;
	}

	public void setCARDNUM(String cARDNUM)
	{
		CARDNUM = cARDNUM;
	}

	public String getCUSIDN()
	{
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN)
	{
		CUSIDN = cUSIDN;
	}

	public String getBankID()
	{
		return BankID;
	}

	public void setBankID(String bankID)
	{
		BankID = bankID;
	}

	public String getTrin_acn()
	{
		return trin_acn;
	}

	public void setTrin_acn(String trin_acn)
	{
		this.trin_acn = trin_acn;
	}

	public String getAMOUNT()
	{
		return AMOUNT;
	}

	public void setAMOUNT(String aMOUNT)
	{
		AMOUNT = aMOUNT;
	}

	public String getDATE()
	{
		return DATE;
	}

	public void setDATE(String dATE)
	{
		DATE = dATE;
	}

	public String getTIME()
	{
		return TIME;
	}

	public void setTIME(String tIME)
	{
		TIME = tIME;
	}

	
	
	
	
}
