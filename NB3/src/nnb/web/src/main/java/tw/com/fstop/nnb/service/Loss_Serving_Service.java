package tw.com.fstop.nnb.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fstop.orm.po.ADMBANK;
import fstop.orm.po.TXNTRACCSET;
import fstop.orm.po.TXNUSER;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmBankDao;
import tw.com.fstop.tbb.nnb.dao.AdmMsgCodeDao;
import tw.com.fstop.tbb.nnb.dao.TxnTrAccSetDao;
import tw.com.fstop.tbb.nnb.dao.TxnUserDao;
import tw.com.fstop.tbb.nnb.util.StrUtils;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.ObjectConvertUtil;
import tw.com.fstop.util.StrUtil;
@Service
public class Loss_Serving_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());
	@Autowired
	AdmBankDao admBankDao;
	
	@Autowired
	TxnTrAccSetDao txnTrAccSetDao;
	
	@Autowired
	TxnUserDao txnUserDao;
	
	@Autowired
	AdmMsgCodeDao admMsgCodeDao;
	
	@Autowired
	private I18n i18n;
	/**
	 * 新臺幣存單掛失
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult reporting_loss_deposit_slip(String cusidn, Map<String, String> reqParam) {
		Map<String, Object> mapN841 = null;
		log.trace("loss_serving_service");
		BaseResult bs = null;
		try {
			String acn = "ALL".equals(reqParam.get("ACN")) ? "" : reqParam.get("ACN");
			bs = new BaseResult();
			bs = N841_REST(cusidn, acn, reqParam);
			
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				for (Map<String, String> row : rows) 
				{
					try {
						//去除金額小數點符號
						String amount = row.get("AMTFDP");
						amount = amount.replace(".", "");
						//帳號.存單金額.存單號碼.到期日.存單金額.起存日.計息方式.利率(%).利息轉入帳號
						row.put("Value", "{" + '"' + "ACN" + '"' + ":" + '"' +row.get("ACN") + '"' + "," +
								'"' + "AMTFDP" + '"' + ":" + '"' +amount + '"' + "," +
								'"' + "FDPNO" + '"' + ":" + '"' +row.get("FDPNUM") + '"' + "," +
								'"' + "DUEDAT" + '"' + ":" + '"' +row.get("DUEDAT") + '"' + "," +
								'"' + "AMTFLG" + '"' + ":" + '"' +amount + '"' + "," +
								'"' + "DPISDT" + '"' + ":" + '"' +row.get("DPISDT") + '"' + "," +
								'"' + "INTMTH" + '"' + ":" + '"' +row.get("INTMTH") + '"' + "," +
								'"' + "ITR" + '"' + ":" + '"' +row.get("ITR") + '"' + "," +
								'"' + "TSFACN" + '"' + ":" + '"' +row.get("TSFACN") + '"' +
								"}"
								);
					}
					catch(Exception e) {
						log.error("", e);
					}
					if(row.get("FD_LOST").equals("*")) {
						//已掛失
						row.put("checkStatus","已掛失");
					}
					else {
						row.put("checkStatus"," ");
					}
					if(row.get("INTMTH").equals("0")) {
						//機動
						row.put("INTMTH","LB.Floating");
					}
					else if(row.get("INTMTH").equals("1")) {
						//固定
						row.put("INTMTH","LB.Fixed");
					}
					
					// 起存日
					row.put("DPISDT", DateUtil.convertDate(2, row.get("DPISDT"), "yyyMMdd", "yyy/MM/dd"));
					// 到期日
					row.put("DUEDAT", DateUtil.convertDate(2, row.get("DUEDAT"), "yyyMMdd", "yyy/MM/dd"));
					// 存單金額
					row.put("AMTFDP", NumericUtil.fmtAmount(row.get("AMTFDP"), 2));
				}
				//移除空入帳帳號,科目簡碼第一位為1或6時不顯示可掛失之帳號
				for (int i = 0;i < rows.size();i++) {
					if(rows.get(i).get("ACN").equals("") || rows.get(i).get("ACN").substring(3, 4).equals("1")
							|| rows.get(i).get("ACN").substring(3, 4).equals("6")) {
						rows.remove(i);
						i--;
					}
				}
				if(rows.size()==0) {
					bs.setErrorMessage("",i18n.getMsg("LB.Check_no_data"));
					bs.setResult(false);				
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * 新臺幣存單掛失結果頁
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult reporting_loss_deposit_slip_result(String cusidn, Map<String, String> reqParam) {
		Map<String, Object> mapN8503 = null;
		log.trace("loss_serving_service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			reqParam.put("ADOPID", "N8503");
			bs = N8503_REST(cusidn, reqParam);
			bs.setResult(true);
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				for (Map<String, String> row : rows) 
				{
					if(row.get("INTMTH").equals("0")) {
						//機動
						row.put("INTMTH","LB.Floating");
					}
					else if(row.get("INTMTH").equals("1")) {
						//固定
						row.put("INTMTH","LB.Fixed");
					}
					
					// 起存日
					row.put("DPISDT", DateUtil.convertDate(2, row.get("DPISDT"), "yyyMMdd", "yyy/MM/dd"));
					// 到期日
					row.put("DUEDAT", DateUtil.convertDate(2, row.get("DUEDAT"), "yyyMMdd", "yyy/MM/dd"));
					// 存單金額
					if(StrUtils.isNotEmpty(row.get("AMTFDP"))) {
						row.put("AMTFDP", NumericUtil.fmtAmount(NumericUtil.addDot(row.get("AMTFDP"), 2), 2));
					}
					//錯誤訊息多語系
					if(!row.get("STATUS").equals("掛失成功")) {
						log.trace("getERROR>>>>>{}",admMsgCodeDao.getCodeMsg(row.get("TOPMSG")));
						row.put("STATUS", admMsgCodeDao.getCodeMsg(row.get("TOPMSG")));
					}
				}
				if(rows.size()==0) {
					bs.setErrorMessage("",i18n.getMsg("LB.Check_no_data"));
					bs.setResult(false);				
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * 外幣存單掛失
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult reporting_loss_deposit_slip_fcy(String cusidn, Map<String, String> reqParam) {
		Map<String, Object> mapN842 = null;
		log.trace("loss_serving_service");
		BaseResult bs = null;
		try {
			String acn = "ALL".equals(reqParam.get("ACN")) ? "" : reqParam.get("ACN");
			bs = new BaseResult();
			bs = N842_REST(cusidn, acn, reqParam);
			
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				
				for (Map<String, String> row : rows) 
				{
					try {
						//去除金額小數點符號
						String amount = row.get("BALANCE");
						amount = amount.replace(".", "");
						//帳號.幣別.存單金額.存單號碼.起存日.到期日.計息方式.利率(%).已轉期數.未轉期數.幣別
						row.put("Value", "{" + '"' + "ACN" + '"' + ":" + '"' +row.get("ACN") + '"' + "," +
								'"' + "CUID" + '"' + ":" + '"' +row.get("CUID") + '"' + "," +
								'"' + "AMTFDP" + '"' + ":" + '"' +amount + '"' + "," +
								'"' + "FDPNO" + '"' + ":" + '"' +row.get("FDPNO") + '"' + "," +
								'"' + "DPISDT" + '"' + ":" + '"' +row.get("DPISDT") + '"' + "," +
								'"' + "DUEDAT" + '"' + ":" + '"' +row.get("DUEDAT") + '"' + "," +
								'"' + "INTMTH" + '"' + ":" + '"' +row.get("INTMTH") + '"' + "," +
								'"' + "ITR" + '"' + ":" + '"' +row.get("ITR") + '"' + "," +
								'"' + "ILAZLFTM" + '"' + ":" + '"' +row.get("ILAZLFTM") + '"' + "," +
								'"' + "AUTXFTM" + '"' + ":" + '"' +row.get("AUTXFTM") + '"' + "," +
								'"' + "CRY" + '"' + ":" + '"' +row.get("CUID") + '"' +
								"}"
								);
					}
					catch(Exception e) {
						log.error("", e);
					}
					if(row.get("EVTMARK").equals("*")) {
						//已掛失
						row.put("checkStatus","已掛失");
					}
					else {
						row.put("checkStatus"," ");
					}
					if(row.get("AUTXFTM").equals("999")) {
						//無限次
						row.put("AUTXFTM","無限次");
					}
					if(row.get("INTMTH").equals("0")) {
						//機動
						row.put("INTMTH","LB.Floating");
					}
					else if(row.get("INTMTH").equals("1")) {
						//固定
						row.put("INTMTH","LB.Fixed");
					}
					
					// 起存日
					row.put("DPISDT", DateUtil.convertDate(2, row.get("DPISDT"), "yyyMMdd", "yyy/MM/dd"));
					// 到期日
					row.put("DUEDAT", DateUtil.convertDate(2, row.get("DUEDAT"), "yyyMMdd", "yyy/MM/dd"));
					// 存單金額
					row.put("BALANCE", NumericUtil.fmtAmount(row.get("BALANCE"), 2));
				}
				if(rows.size()==0) {
					bs.setErrorMessage("",i18n.getMsg("LB.Check_no_data"));
					bs.setResult(false);				
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * 外幣存單掛失結果頁
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult reporting_loss_deposit_slip_fcy_result(String cusidn, Map<String, String> reqParam) {
		Map<String, Object> mapN8505 = null;
		log.trace("loss_serving_service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			reqParam.put("ADOPID", "N8505");
			bs = N8505_REST(cusidn, reqParam);
			bs.setResult(true);
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				for (Map<String, String> row : rows) 
				{
					if(row.get("AUTXFTM").equals("999")) {
						//無限次
						row.put("AUTXFTM","無限次");
					}
					if(row.get("INTMTH").equals("0")) {
						//機動
						row.put("INTMTH","LB.Floating");
					}
					else if(row.get("INTMTH").equals("1")) {
						//固定
						row.put("INTMTH","LB.Fixed");
					}
					
					// 起存日
					row.put("DPISDT", DateUtil.convertDate(2, row.get("DPISDT"), "yyyMMdd", "yyy/MM/dd"));
					// 到期日
					row.put("DUEDAT", DateUtil.convertDate(2, row.get("DUEDAT"), "yyyMMdd", "yyy/MM/dd"));
					// 存單金額
					if(StrUtils.isNotEmpty(row.get("AMTFDP"))) {
						row.put("AMTFDP", NumericUtil.fmtAmount(NumericUtil.addDot(row.get("AMTFDP"), 2), 2));
					}
					//錯誤訊息多語系
					if(!row.get("STATUS").equals("掛失成功")) {
						log.trace("getERROR>>>>>{}",admMsgCodeDao.getCodeMsg(row.get("TOPMSG")));
						row.put("STATUS", admMsgCodeDao.getCodeMsg(row.get("TOPMSG")));
					}
				}
				if(rows.size()==0) {
					bs.setErrorMessage("",i18n.getMsg("LB.Check_no_data"));
					bs.setResult(false);				
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * 新臺幣存摺掛失
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult passbook_loss(String cusidn, Map<String, String> reqParam) {
		Map<String, Object> mapN840 = null;
		log.trace("loss_serving_service");
		BaseResult bs = null;
		try {
			String acn = "ALL".equals(reqParam.get("ACN")) ? "" : reqParam.get("ACN");
			bs = new BaseResult();
			bs = N840_REST(cusidn, acn, reqParam);
			
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				for (Map<String, String> row : rows) 
				{
					try {
						//帳號
						row.put("Value", "{" + '"' + "ACN" + '"' + ":" + '"' +row.get("ACN") + '"' +
								"}"
								);
					}
					catch(Exception e) {
						log.error("", e);
					}
					if(row.get("SV_LOST").equals("*")) {
						row.put("checkStatus","已掛失");
					}
					else {
						row.put("checkStatus"," ");
					}
				}
				//移除空入帳帳號
				for (int i = 0;i < rows.size();i++) {
					if(rows.get(i).get("ACN").equals("")) {
						rows.remove(i);
						i--;
					}
				}
				if(rows.size()==0) {
					bs.setErrorMessage("",i18n.getMsg("LB.Check_no_data"));
					bs.setResult(false);				
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}

	/**
	 * 新臺幣存摺掛失結果頁
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult passbook_loss_result(String cusidn, Map<String, String> reqParam) {
		Map<String, Object> mapN8501 = null;
		log.trace("loss_serving_service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			reqParam.put("ADOPID", "N8501");
			bs = N8501_REST(cusidn, reqParam);
			bs.setResult(true);
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				for (Map<String, String> row : rows) 
				{
					String status = row.get("STATUS");
					status = status.replace("　", "");
					status = status.replace("	", "");
					status = status.replace(" ", "");
					row.put("STATUS", status);
					// 可用餘額
					if(StrUtils.isNotEmpty(row.get("BALANCE"))) {
						row.put("BALANCE", NumericUtil.fmtAmount(NumericUtil.addDot(row.get("BALANCE"), 2), 2));
					}
					//錯誤訊息多語系
					if(!row.get("STATUS").equals("掛失成功")) {
						log.trace("getERROR>>>>>{}",admMsgCodeDao.getCodeMsg(row.get("TOPMSG")));
						row.put("STATUS", admMsgCodeDao.getCodeMsg(row.get("TOPMSG")));
					}
				}
				if(rows.size()==0) {
					bs.setErrorMessage("",i18n.getMsg("LB.Check_no_data"));
					bs.setResult(false);				
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * 外幣存褶掛失
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult fcy_passbook_loss(String cusidn, Map<String, String> reqParam) {
		Map<String, Object> mapN843 = null;
		log.trace("loss_serving_service");
		BaseResult bs = null;
		try {
			String acn = "ALL".equals(reqParam.get("ACN")) ? "" : reqParam.get("ACN");
			bs = new BaseResult();
			bs = N843_REST(cusidn, acn, reqParam);
			
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				for (Map<String, String> row : rows) 
				{
					try {
						//帳號
						row.put("Value", "{" + '"' + "ACN" + '"' + ":" + '"' +row.get("ACN") + '"' +
								"}"
								);
					}
					catch(Exception e) {
						log.error("", e);
					}
					if(row.get("EVTMARK").equals("*")) {
						row.put("checkStatus","已掛失");
					}
					else {
						row.put("checkStatus"," ");
					}
				}
				//移除空入帳帳號
				for (int i = 0;i < rows.size();i++) {
					if(rows.get(i).get("ACN").equals("")) {
						rows.remove(i);
						i--;
					}
				}
				if(rows.size()==0) {
					bs.setErrorMessage("",i18n.getMsg("LB.Check_no_data"));
					bs.setResult(false);				
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * 外幣存摺掛失結果頁
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult fcy_passbook_loss_result(String cusidn, Map<String, String> reqParam) {
		Map<String, Object> mapN8507 = null;
		log.trace("loss_serving_service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			reqParam.put("ADOPID", "N8507");
			bs = N8507_REST(cusidn, reqParam);
			bs.setResult(true);
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				for (Map<String, String> row : rows) 
				{
					//錯誤訊息多語系
					if(!row.get("STATUS").equals("掛失成功")) {
						log.trace("getERROR>>>>>{}",admMsgCodeDao.getCodeMsg(row.get("TOPMSG")));
						row.put("STATUS", admMsgCodeDao.getCodeMsg(row.get("TOPMSG")));
					}
//					if(row.get("INTMTH").equals("0")) {
//						row.put("INTMTH","機動");
//					}
//					else if(row.get("INTMTH").equals("1")) {
//						row.put("INTMTH","固定");
//					}
//					
//					// 起存日
//					row.put("DPISDT", DateUtil.convertDate(2, row.get("DPISDT"), "yyyMMdd", "yyy/MM/dd"));
//					// 到期日
//					row.put("DUEDAT", DateUtil.convertDate(2, row.get("DUEDAT"), "yyyMMdd", "yyy/MM/dd"));
//					// 存單金額
//					row.put("AMTFDP", NumericUtil.fmtAmount(row.get("AMTFDP"), 2));
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * 黃金存褶掛失
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult gold_passbook_loss(String cusidn, Map<String, String> reqParam) {
		Map<String, Object> mapN845 = null;
		log.trace("loss_serving_service");
		BaseResult bs = null;
		try {
			String acn = "ALL".equals(reqParam.get("ACN")) ? "" : reqParam.get("ACN");
			bs = new BaseResult();
			bs = N845_REST(cusidn, acn, reqParam);
			
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				for (Map<String, String> row : rows) 
				{
					try {
						//帳號.可用餘額(公克).帳面餘額(公克)
						row.put("Value", "{" + '"' + "ACN" + '"' + ":" + '"' +row.get("ACN") + '"' +
								"}"
								);
					}
					catch(Exception e) {
						log.error("", e);
					}
					if(row.get("LOST01").equals("*")) {
						row.put("checkStatus","已掛失");
					}
					else {
						row.put("checkStatus"," ");
					}
				}
				//移除空入帳帳號
				for (int i = 0;i < rows.size();i++) {
					if(rows.get(i).get("ACN").equals("")) {
						rows.remove(i);
						i--;
					}
				}
				
				//
				if(rows.size()==0) {
					bs.setErrorMessage("",i18n.getMsg("LB.Check_no_data"));
					bs.setResult(false);				
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * 黃金存摺掛失結果頁
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult gold_passbook_loss_result(String cusidn, Map<String, String> reqParam) {
		Map<String, Object> mapN850A = null;
		log.trace("loss_serving_service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			reqParam.put("ADOPID", "N850A");
			bs = N850A_REST(cusidn, reqParam);
			bs.setResult(true);
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				for (Map<String, String> row : rows) 
				{
					// 可用餘額(公克)
					if(StrUtils.isNotEmpty(row.get("TSFBAL"))) {
						row.put("TSFBAL", NumericUtil.fmtAmount(NumericUtil.addDot(row.get("TSFBAL"), 2), 2));
					}
					// 帳戶餘額(公克)
					if(StrUtils.isNotEmpty(row.get("GDBAL"))) {
						row.put("GDBAL", NumericUtil.fmtAmount(NumericUtil.addDot(row.get("GDBAL"), 2), 2));
					}
					//錯誤訊息多語系
					if(!row.get("STATUS").equals("掛失成功")) {
						log.trace("getERROR>>>>>{}",admMsgCodeDao.getCodeMsg(row.get("TOPMSG")));
						row.put("STATUS", admMsgCodeDao.getCodeMsg(row.get("TOPMSG")));
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * 金融卡掛失
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult debitcard_loss(String cusidn, Map<String, String> reqParam) {
		Map<String, Object> mapN844 = null;
		log.trace("loss_serving_service");
		BaseResult bs = null;
		try {
			String acn = "ALL".equals(reqParam.get("ACN")) ? "" : reqParam.get("ACN");
			bs = new BaseResult();
			bs = N844_REST(cusidn, acn, reqParam);
			
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				for (Map<String, String> row : rows) 
				{
					//去除金額小數點符號
					String balance = row.get("BALANCE");
					balance = balance.replace(".", "");
					String clr =row.get("CLR");
					clr = clr.replace(".", "");
					try {
						//帳號.餘額.本交.IC 圈存餘額.卡片功能
						row.put("Value", "{" + '"' + "ACN" + '"' + ":" + '"' +row.get("LOSACN") + '"' +"," +
								'"' + "BALANCE" + '"' + ":" + '"' + balance + '"' + "," +
								'"' + "CLR" + '"' + ":" + '"' + clr + '"' + "," +
								'"' + "AMTICHD" + '"' + ":" + '"' +row.get("AMTICHD") + '"' + "," +
								'"' + "CARDFUN" + '"' + ":" + '"' +row.get("CARDFUN") + '"' + "," +
								'"' + "LOSTYPE" + '"' + ":" + '"' + "031" + '"' +
								"}"
								);
					}
					catch(Exception e) {
						log.error("", e);
					}
					if(row.get("STATUS").equals("*")) {
						row.put("checkStatus","已掛失");
					}
					else if(row.get("STATUS").equals("0")){
						row.put("checkStatus","一般");
					}
					else {
						row.put("checkStatus"," ");
					}
					if(row.get("CARDFUN").equals("01")) {
						//晶片金融卡
						row.put("CARDFUN", "LB.Financial_debit_card");
					}
					else if(row.get("CARDFUN").equals("02")) {
						//磁條金融卡
						row.put("CARDFUN", "LB.X0462");
					}
					else if(row.get("CARDFUN").equals("03")) {
						//VISA 晶片金融卡
						row.put("CARDFUN", "LB.X0463");
					}
					else if(row.get("CARDFUN").equals("04")) {
						//VISA 磁條金融卡
						row.put("CARDFUN", "LB.X0464");
					}
					else if(row.get("CARDFUN").equals("05")) {
						//COMBO卡
						row.put("CARDFUN", "LB.X0465");
					}
					else if(row.get("CARDFUN").equals("06")) {
						//晶片金融卡(附屬帳號)
						row.put("CARDFUN", "LB.X0466");
					}
					else if(row.get("CARDFUN").equals("07")) {
						//金融卡
						row.put("CARDFUN", "LB.X1911");
					}
				}
				//移除空入帳帳號
				for (int i = 0;i < rows.size();i++) {
					if(rows.get(i).get("LOSACN").equals("")) {
						rows.remove(i);
						i--;
					}
				}
				if(rows.size()==0) {
					bs.setErrorMessage("",i18n.getMsg("LB.Check_no_data"));
					bs.setResult(false);				
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * 金融卡掛失結果頁
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult debitcard_loss_result(String cusidn, Map<String, String> reqParam) {
		Map<String, Object> mapN8509 = null;
		log.trace("loss_serving_service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			reqParam.put("ADOPID", "N8509");
			bs = N8509_REST(cusidn, reqParam);
			bs.setResult(true);
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				for (Map<String, String> row : rows) 
				{
					if(row.get("LOSTYPE").equals("031")) {
						//金融卡卡片
						row.put("LOSTYPE","LB.X0467");
					}
					else {
						//IC金融卡卡片
						row.put("LOSTYPE","LB.X0468");
					}
					// 帳戶餘額
					if(StrUtils.isNotEmpty(row.get("BALANCE"))) {
						row.put("BALANCE", NumericUtil.fmtAmount(NumericUtil.addDot(row.get("BALANCE"), 2) , 2));
					}
					//錯誤訊息多語系
					if(!row.get("STATUS").equals("掛失成功")) {
						try {
							log.trace("getERROR>>>>>{}",admMsgCodeDao.getCodeMsg(row.get("TOPMSG")));
							row.put("STATUS", admMsgCodeDao.getCodeMsg(row.get("TOPMSG")));
						}
						catch(Exception ex) {
							log.error("", ex);
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * 新臺幣存單印鑑掛失
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult deposit_slip_seal_loss(String cusidn, Map<String, String> reqParam) {
		Map<String, Object> mapN841 = null;
		log.trace("loss_serving_service");
		BaseResult bs = null;
		try {
			String acn = "ALL".equals(reqParam.get("ACN")) ? "" : reqParam.get("ACN");
			bs = new BaseResult();
			bs = N841_REST(cusidn, acn, reqParam);
			LinkedList<Object> labelList = new LinkedList<>();
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				ArrayList<Map<String , String>> callRec = new ArrayList<Map<String , String>>();
				
				//移除空入帳帳號,科目簡碼第一位為1或6時不顯示可掛失之帳號
				for (int i = 0;i < rows.size();i++) {
					if(rows.get(i).get("ACN").equals("") || rows.get(i).get("ACN").substring(3, 4).equals("1")
							|| rows.get(i).get("ACN").substring(3, 4).equals("6")) {
						rows.remove(i);
						i--;
					}
				}
				
				for (Map<String, String> row : rows) 
				{
					if(row.get("INTMTH").equals("0")) {
						//機動
						row.put("INTMTH","LB.Floating");
					}
					else if(row.get("INTMTH").equals("1")) {
						//固定
						row.put("INTMTH","LB.Fixed");
					}
					// 起存日
					row.put("DPISDT", DateUtil.convertDate(2, row.get("DPISDT"), "yyyMMdd", "yyy/MM/dd"));
					// 到期日
					row.put("DUEDAT", DateUtil.convertDate(2, row.get("DUEDAT"), "yyyMMdd", "yyy/MM/dd"));
					// 存單金額
					row.put("AMTFDP", NumericUtil.fmtAmount(row.get("AMTFDP"), 2));
					callRec.add(row);
				}
				rows = callRec;
				// 取得全部的ACN並放入List
				Set<String> acnAllList = new HashSet<String>();
				for (Map<String, String> map : rows) {
					map.get("ACN");
					acnAllList.add(map.get("ACN"));
				}
				log.trace("acnAllList>>{}", acnAllList);
				
				for (String strAcn : acnAllList){
					LinkedHashMap<String, Object> labelMap = new LinkedHashMap<String, Object>();
					LinkedList<Object> listAcnMap = new LinkedList<>();
					for (Map map : rows) {
						if (strAcn.equals(map.get("ACN"))) {
							listAcnMap.add(map);
							labelMap.put("SING_LOST",map.get("SING_LOST"));
							if(map.get("SING_LOST").equals("*")) {
								labelMap.put("checkStatus","已掛失");
							}
							else {
								labelMap.put("checkStatus"," ");
							}
						}
					}
					labelMap.put("ACN", strAcn);
					labelMap.put("rowListMap", listAcnMap);
					labelList.add(labelMap);
				}
				if(rows.size()==0) {
					bs.setErrorMessage("",i18n.getMsg("LB.Check_no_data"));
					bs.setResult(false);				
				}
				bs.addData("labelList", labelList);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * 新臺幣存單印鑑掛失結果頁
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult deposit_slip_seal_loss_result(String cusidn, Map<String, String> reqParam) {
		Map<String, Object> mapN8504 = null;
		log.trace("loss_serving_service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			reqParam.put("ADOPID", "N8504");
			bs = N8504_REST(cusidn, reqParam);
			bs.setResult(true);
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				for (Map<String, String> row : rows) 
				{
					if(StrUtils.isNotEmpty(row.get("INTMTH"))) {
						if(row.get("INTMTH").equals("0")) {
							//機動
							row.put("INTMTH","LB.Floating");
						}
						else if(row.get("INTMTH").equals("1")) {
							//固定
							row.put("INTMTH","LB.Fixed");
						}
					}
					
					// 起存日
					if(StrUtils.isNotEmpty(row.get("DPISDT"))) {
						row.put("DPISDT", DateUtil.convertDate(2, row.get("DPISDT"), "yyyMMdd", "yyy/MM/dd"));
					}
					// 到期日
					if(StrUtils.isNotEmpty(row.get("DUEDAT"))) {
						row.put("DUEDAT", DateUtil.convertDate(2, row.get("DUEDAT"), "yyyMMdd", "yyy/MM/dd"));
					}
					// 存單金額
					if(StrUtils.isNotEmpty(row.get("AMTFDP"))) {
						row.put("AMTFDP", NumericUtil.fmtAmount(NumericUtil.addDot(row.get("AMTFDP"), 2), 2));
					}
					//錯誤訊息多語系
					if(!row.get("STATUS").equals("掛失成功")) {
						log.trace("getERROR>>>>>{}",admMsgCodeDao.getCodeMsg(row.get("TOPMSG")));
						row.put("STATUS", admMsgCodeDao.getCodeMsg(row.get("TOPMSG")));
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * 外幣存單印鑑掛失
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult f_deposit_slip_seal_loss(String cusidn, Map<String, String> reqParam) {
		Map<String, Object> mapN842 = null;
		log.trace("loss_serving_service");
		BaseResult bs = null;
		try {
			String acn = "ALL".equals(reqParam.get("ACN")) ? "" : reqParam.get("ACN");
			bs = new BaseResult();
			bs = N842_REST(cusidn, acn, reqParam);
			LinkedList<Object> labelList = new LinkedList<>();
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				ArrayList<Map<String , String>> callRec = new ArrayList<Map<String , String>>();
				
				for (Map<String, String> row : rows) 
				{
					if(row.get("AUTXFTM").equals("999")) {
						row.put("AUTXFTM","無限次");
					}
					if(row.get("INTMTH").equals("0")) {
						//機動
						row.put("INTMTH","LB.Floating");
					}
					else if(row.get("INTMTH").equals("1")) {
						//固定
						row.put("INTMTH","LB.Fixed");
					}
					
					// 起存日
					row.put("DPISDT", DateUtil.convertDate(2, row.get("DPISDT"), "yyyMMdd", "yyy/MM/dd"));
					// 到期日
					row.put("DUEDAT", DateUtil.convertDate(2, row.get("DUEDAT"), "yyyMMdd", "yyy/MM/dd"));
					// 存單金額
					row.put("BALANCE", NumericUtil.fmtAmount(row.get("BALANCE"), 2));
					callRec.add(row);
				}
				rows = callRec;
				// 取得全部的ACN並放入List
				Set<String> acnAllList = new HashSet<String>();
				for (Map<String, String> map : rows) {
					map.get("ACN");
					acnAllList.add(map.get("ACN"));
				}
				log.trace("acnAllList>>{}", acnAllList);
				
				for (String strAcn : acnAllList){
					LinkedHashMap<String, Object> labelMap = new LinkedHashMap<String, Object>();
					LinkedList<Object> listAcnMap = new LinkedList<>();
					for (Map map : rows) {
						if (strAcn.equals(map.get("ACN"))) {
							listAcnMap.add(map);
							labelMap.put("EVTMARK",map.get("EVTMARK"));
							if(map.get("EVTMARK").equals("*")) {
								labelMap.put("checkStatus","已掛失");
							}
							else {
								labelMap.put("checkStatus"," ");
							}
						}
					}
					labelMap.put("ACN", strAcn);
					labelMap.put("rowListMap", listAcnMap);
					labelList.add(labelMap);
				}
				if(rows.size()==0) {
					bs.setErrorMessage("",i18n.getMsg("LB.Check_no_data"));
					bs.setResult(false);				
				}
				bs.addData("labelList", labelList);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * 外幣存單印鑑掛失結果頁
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult f_deposit_slip_seal_loss_result(String cusidn, Map<String, String> reqParam) {
		Map<String, Object> mapN8506 = null;
		log.trace("loss_serving_service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			reqParam.put("ADOPID", "N8506");
			bs = N8506_REST(cusidn, reqParam);
			bs.setResult(true);
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				for (Map<String, String> row : rows) 
				{
					if(StrUtils.isNotEmpty(row.get("AUTXFTM"))) {
						if(row.get("AUTXFTM").equals("999")) {
							row.put("AUTXFTM","無限次");
						}
					}
					if(StrUtils.isNotEmpty(row.get("INTMTH"))) {
						if(row.get("INTMTH").equals("0")) {
							//機動
							row.put("INTMTH","LB.Floating");
						}
						else if(row.get("INTMTH").equals("1")) {
							//固定
							row.put("INTMTH","LB.Fixed");
						}
					}
					
					// 起存日
					if(StrUtils.isNotEmpty(row.get("DPISDT"))) {
						row.put("DPISDT", DateUtil.convertDate(2, row.get("DPISDT"), "yyyMMdd", "yyy/MM/dd"));
					}
					// 到期日
					if(StrUtils.isNotEmpty(row.get("DUEDAT"))) {
						row.put("DUEDAT", DateUtil.convertDate(2, row.get("DUEDAT"), "yyyMMdd", "yyy/MM/dd"));
					}
					// 存單金額
					if(StrUtils.isNotEmpty(row.get("AMTFDP"))) {
						row.put("AMTFDP", NumericUtil.fmtAmount(NumericUtil.addDot(row.get("AMTFDP"), 2), 2));
					}
					//錯誤訊息多語系
					if(!row.get("STATUS").equals("掛失成功")) {
						log.trace("getERROR>>>>>{}",admMsgCodeDao.getCodeMsg(row.get("TOPMSG")));
						row.put("STATUS", admMsgCodeDao.getCodeMsg(row.get("TOPMSG")));
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * 新臺幣存摺印鑑掛失
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult passbook_seal_loss(String cusidn, Map<String, String> reqParam) {
		Map<String, Object> mapN840 = null;
		log.trace("loss_serving_service");
		BaseResult bs = null;
		try {
			String acn = "ALL".equals(reqParam.get("ACN")) ? "" : reqParam.get("ACN");
			bs = new BaseResult();
			bs = N840_REST(cusidn, acn, reqParam);
			
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				for (Map<String, String> row : rows) 
				{
					try {
						//帳號
						row.put("Value", "{" + '"' + "ACN" + '"' + ":" + '"' +row.get("ACN") + '"' +
								"}"
								);
					}
					catch(Exception e) {
						log.error("", e);
					}
					if(row.get("SING_LOST").equals("*")) {
						row.put("checkStatus","已掛失");
					}
					else {
						row.put("checkStatus"," ");
					}
				}
				//移除空入帳帳號
				for (int i = 0;i < rows.size();i++) {
					if(rows.get(i).get("ACN").equals("")) {
						rows.remove(i);
						i--;
					}
				}
				if(rows.size()==0) {
					bs.setErrorMessage("",i18n.getMsg("LB.Check_no_data"));
					bs.setResult(false);				
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * 新臺幣存摺印鑑掛失結果頁
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult passbook_seal_loss_result(String cusidn, Map<String, String> reqParam) {
		Map<String, Object> mapN8502 = null;
		log.trace("loss_serving_service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			reqParam.put("ADOPID", "N8502");
			bs = N8502_REST(cusidn, reqParam);
			bs.setResult(true);
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				for (Map<String, String> row : rows) 
				{
					// 餘額
					if(StrUtils.isNotEmpty(row.get("BALANCE"))) {
						row.put("BALANCE", NumericUtil.fmtAmount(NumericUtil.addDot(row.get("BALANCE"), 2), 2));
					}
					//錯誤訊息多語系
					if(!row.get("STATUS").equals("掛失成功")) {
						log.trace("getERROR>>>>>{}",admMsgCodeDao.getCodeMsg(row.get("TOPMSG")));
						row.put("STATUS", admMsgCodeDao.getCodeMsg(row.get("TOPMSG")));
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * 外幣存褶印鑑掛失
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult f_passbook_seal_loss(String cusidn, Map<String, String> reqParam) {
		Map<String, Object> mapN843 = null;
		log.trace("loss_serving_service");
		BaseResult bs = null;
		try {
			String acn = "ALL".equals(reqParam.get("ACN")) ? "" : reqParam.get("ACN");
			bs = new BaseResult();
			bs = N843_REST(cusidn, acn, reqParam);
			
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				for (Map<String, String> row : rows) 
				{
					try {
						//帳號
						row.put("Value", "{" + '"' + "ACN" + '"' + ":" + '"' +row.get("ACN") + '"' +
								"}"
								);
					}
					catch(Exception e) {
						log.error("", e);
					}
					if(row.get("EVTMARK").equals("*")) {
						row.put("checkStatus","已掛失");
					}
					else {
						row.put("checkStatus"," ");
					}
				}
				//移除空入帳帳號
				for (int i = 0;i < rows.size();i++) {
					if(rows.get(i).get("ACN").equals("")) {
						rows.remove(i);
						i--;
					}
				}
				if(rows.size()==0) {
					bs.setErrorMessage("",i18n.getMsg("LB.Check_no_data"));
					bs.setResult(false);				
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * 外幣存摺印鑑掛失結果頁
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult f_passbook_seal_loss_result(String cusidn, Map<String, String> reqParam) {
		Map<String, Object> mapN8508 = null;
		log.trace("loss_serving_service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			reqParam.put("ADOPID", "N8508");
			bs = N8508_REST(cusidn, reqParam);
			bs.setResult(true);
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				for (Map<String, String> row : rows) 
				{
					//錯誤訊息多語系
					if(!row.get("STATUS").equals("掛失成功")) {
						log.trace("getERROR>>>>>{}",admMsgCodeDao.getCodeMsg(row.get("TOPMSG")));
						row.put("STATUS", admMsgCodeDao.getCodeMsg(row.get("TOPMSG")));
					}
//					if(row.get("INTMTH").equals("0")) {
//						row.put("INTMTH","機動");
//					}
//					else if(row.get("INTMTH").equals("1")) {
//						row.put("INTMTH","固定");
//					}
//					
//					// 起存日
//					row.put("DPISDT", DateUtil.convertDate(2, row.get("DPISDT"), "yyyMMdd", "yyy/MM/dd"));
//					// 到期日
//					row.put("DUEDAT", DateUtil.convertDate(2, row.get("DUEDAT"), "yyyMMdd", "yyy/MM/dd"));
//					// 存單金額
//					row.put("AMTFDP", NumericUtil.fmtAmount(row.get("AMTFDP"), 2));
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * 黃金存褶掛失
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult g_passbook_seal_loss(String cusidn, Map<String, String> reqParam) {
		Map<String, Object> mapN845 = null;
		log.trace("loss_serving_service");
		BaseResult bs = null;
		try {
			String acn = "ALL".equals(reqParam.get("ACN")) ? "" : reqParam.get("ACN");
			bs = new BaseResult();
			bs = N845_REST(cusidn, acn, reqParam);
			
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				for (Map<String, String> row : rows) 
				{
					try {
						//帳號.可用餘額(公克).帳面餘額(公克)
						row.put("Value", "{" + '"' + "ACN" + '"' + ":" + '"' +row.get("ACN") + '"' +
								"}"
								);
					}
					catch(Exception e) {
						log.error("", e);
					}
					if(row.get("LOST02").equals("*")) {
						row.put("checkStatus","已掛失");
					}
					else {
						row.put("checkStatus"," ");
					}
				}
				//移除空入帳帳號
				for (int i = 0;i < rows.size();i++) {
					if(rows.get(i).get("ACN").equals("")) {
						rows.remove(i);
						i--;
					}
				}
				if(rows.size()==0) {
					bs.setErrorMessage("",i18n.getMsg("LB.Check_no_data"));
					bs.setResult(false);				
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
	
	/**
	 * 黃金存摺印鑑掛失結果頁
	 * @param cusidn
	 * @param reqParam
	 * @return
	 */
	public BaseResult g_passbook_seal_loss_result(String cusidn, Map<String, String> reqParam) {
		Map<String, Object> mapN850A = null;
		log.trace("loss_serving_service");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			reqParam.put("ADOPID", "N850A");
			bs = N850A_REST(cusidn, reqParam);
			bs.setResult(true);
			if(bs != null && bs.getResult()) {
				Map<String, Object> bsData = (Map) bs.getData();
				ArrayList<Map<String, String>> rows = (ArrayList<Map<String, String>>) bsData.get("REC");
				for (Map<String, String> row : rows) 
				{
					// 可用餘額(公克)
					if(StrUtils.isNotEmpty(row.get("TSFBAL"))) {
						row.put("TSFBAL", NumericUtil.fmtAmount(NumericUtil.addDot(row.get("TSFBAL"), 2), 2));
					}
					// 帳戶餘額(公克)
					if(StrUtils.isNotEmpty(row.get("GDBAL"))) {
						row.put("GDBAL", NumericUtil.fmtAmount(NumericUtil.addDot(row.get("GDBAL"), 2), 2));
					}
					//錯誤訊息多語系
					if(!row.get("STATUS").equals("掛失成功")) {
						log.trace("getERROR>>>>>{}",admMsgCodeDao.getCodeMsg(row.get("TOPMSG")));
						row.put("STATUS", admMsgCodeDao.getCodeMsg(row.get("TOPMSG")));
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			log.error("", e);
		}
		return bs;
	}
}
