package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class MTPUNBIND_IDGATE_DATA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -762951082705400880L;


	@SerializedName(value = "cusidn")
	private String cusidn;
	@SerializedName(value = "mobilephone")
	private String mobilephone;
	@SerializedName(value = "txacn")
	private String txacn;
	@SerializedName(value = "systemflag")
	private String systemflag;
	@SerializedName(value = "bankcode")
	private String bankcode;
	public String getCusidn() {
		return cusidn;
	}
	public void setCusidn(String cusidn) {
		this.cusidn = cusidn;
	}
	public String getMobilephone() {
		return mobilephone;
	}
	public void setMobilephone(String mobilephone) {
		this.mobilephone = mobilephone;
	}
	public String getTxacn() {
		return txacn;
	}
	public void setTxacn(String txacn) {
		this.txacn = txacn;
	}
	public String getSystemflag() {
		return systemflag;
	}
	public void setSystemflag(String systemflag) {
		this.systemflag = systemflag;
	}
	public String getBankcode() {
		return bankcode;
	}
	public void setBankcode(String bankcode) {
		this.bankcode = bankcode;
	}

}
