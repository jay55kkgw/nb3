package tw.com.fstop.nnb.spring.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import fstop.orm.po.TXNADDRESSBOOK;
import tw.com.fstop.nnb.service.DaoService;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;

@Controller
public class DaoController {
	private Logger log = LoggerFactory.getLogger(this.getClass().getName());

	@Autowired
	DaoService daoService;
	
	@RequestMapping("/dao")
	public void getDao(@RequestParam Map<String,String> requestParam,HttpServletResponse response,Model model)
	{
		log.debug("getDao");

		try
		{
			String dpuserid = "aa";
			List<TXNADDRESSBOOK> list = daoService.findByDPUserID(dpuserid);
			log.trace(ESAPIUtil.vaildLog("<<<<<<<<<<< "+ list));
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getDao error >> {}",e);
		}
	}
}