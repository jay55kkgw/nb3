package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N8509_REST_RSDATA implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6730799370283030038L;
	
	private String ACN;
	private String CUSIDN;
	private String DATE;
	private String TIME;
	private String BALANCE;
	private String CLR;
	private String AMTICHD;
	private String LOSTYPE;
	private String STATUS;
	private String TOPMSG;
	
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getBALANCE() {
		return BALANCE;
	}
	public void setBALANCE(String bALANCE) {
		BALANCE = bALANCE;
	}
	public String getCLR() {
		return CLR;
	}
	public void setCLR(String cLR) {
		CLR = cLR;
	}
	public String getAMTICHD() {
		return AMTICHD;
	}
	public void setAMTICHD(String aMTICHD) {
		AMTICHD = aMTICHD;
	}
	public String getLOSTYPE() {
		return LOSTYPE;
	}
	public void setLOSTYPE(String lOSTYPE) {
		LOSTYPE = lOSTYPE;
	}
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getTOPMSG() {
		return TOPMSG;
	}
	public void setTOPMSG(String tOPMSG) {
		TOPMSG = tOPMSG;
	}
}
