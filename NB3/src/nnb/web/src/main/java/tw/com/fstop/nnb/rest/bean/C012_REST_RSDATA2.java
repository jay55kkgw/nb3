package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * C012電文RSDATA2
 */
public class C012_REST_RSDATA2 implements Serializable{
	private static final long serialVersionUID = 7127169312136862734L;
	
	//各幣別的ROW資料
	private String SUBTOTAMT;
	private String SUBCRY;
	private String SUBREFVALUE;
	private String SUBDIFAMT;
	private String SUBDIFAMT2;
	private String SUBLCYRAT;
	private String SUBFCYRAT;
	
	public String getSUBTOTAMT(){
		return SUBTOTAMT;
	}
	public void setSUBTOTAMT(String sUBTOTAMT){
		SUBTOTAMT = sUBTOTAMT;
	}
	public String getSUBCRY(){
		return SUBCRY;
	}
	public void setSUBCRY(String sUBCRY){
		SUBCRY = sUBCRY;
	}
	public String getSUBREFVALUE(){
		return SUBREFVALUE;
	}
	public void setSUBREFVALUE(String sUBREFVALUE){
		SUBREFVALUE = sUBREFVALUE;
	}
	public String getSUBDIFAMT(){
		return SUBDIFAMT;
	}
	public void setSUBDIFAMT(String sUBDIFAMT){
		SUBDIFAMT = sUBDIFAMT;
	}
	public String getSUBDIFAMT2(){
		return SUBDIFAMT2;
	}
	public void setSUBDIFAMT2(String sUBDIFAMT2){
		SUBDIFAMT2 = sUBDIFAMT2;
	}
	public String getSUBLCYRAT(){
		return SUBLCYRAT;
	}
	public void setSUBLCYRAT(String sUBLCYRAT){
		SUBLCYRAT = sUBLCYRAT;
	}
	public String getSUBFCYRAT(){
		return SUBFCYRAT;
	}
	public void setSUBFCYRAT(String sUBFCYRAT){
		SUBFCYRAT = sUBFCYRAT;
	}
}