package tw.com.fstop.nnb.spring.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.custom.annotation.ISTXNLOG;
import tw.com.fstop.nnb.service.Electronic_Checksheet_Service;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({ SessionUtil.DPMYEMAIL, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, SessionUtil.BACK_DATA,
		SessionUtil.PRINT_DATALISTMAP_DATA, SessionUtil.RESULT_LOCALE_DATA, SessionUtil.CONFIRM_LOCALE_DATA,
		SessionUtil.CUSIDN, SessionUtil.AUTHORITY, SessionUtil.TRANSFER_DATA, SessionUtil.STEP1_LOCALE_DATA,
		SessionUtil.STEP2_LOCALE_DATA, SessionUtil.STEP3_LOCALE_DATA })
@Controller
@RequestMapping(value = "/ELECTRONIC/CHECKSHEET")
public class Electronic_Checksheet_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Electronic_Checksheet_Service electronic_checksheet_service;

	/**
	 * 電子帳單
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return target Sring
	 */
	@RequestMapping(value = "/apply_bill")
	public String apply_bill(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("Electronic_Checksheet_Controller_apply_bill_p1...");
		BaseResult bs = null;
		
		try {
			bs = new BaseResult();
			// Base64解碼
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			log.trace("card_overview_cusidn >>{} ", cusidn);
			reqParam.put("CUSIDN", cusidn);

			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			String IP = WebUtil.getIpAddr(request);
			log.debug(ESAPIUtil.vaildLog("IP >> " + IP));
			okMap.put("IP",IP);
			
			bs = electronic_checksheet_service.apply_bill_input(okMap);

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("apply_bill error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/electronic_checksheet/elec_bill_apply";
				model.addAttribute("elec_bill_apply", bs);
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
			log.trace("target >> {}", target);
			return target;
		}
	}

	/**
	 * 電子帳單 申請條款頁
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return target Sring
	 */
	@RequestMapping(value = "/apply_statement")
	public String apply_statement(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("Electronic_Checksheet_Controller_apply_statement...");
		BaseResult bs = null;
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			bs = new BaseResult();
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
				if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null));
				if (!bs.getResult()) {
					// TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				} else {
				// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			} else {			
				
				// Base64解碼
				String cusidn = new String(
						Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
						"utf-8");
				log.trace("apply_statement >>{} ", cusidn);
				reqParam.put("CUSIDN", cusidn);
	
				bs.setData(okMap);
				bs.setResult(true);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("apply_statement error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/electronic_checksheet/elec_bill_apply_statement";
				model.addAttribute(SessionUtil.TRANSFER_DATA, bs);
				log.debug(ESAPIUtil.vaildLog("TRANSFER DATA >> {} "+CodeUtil.toJson(bs)));
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
			log.trace("target >> {}", target);
			return target;
		}
	}

	/**
	 * 電子帳單申請確認頁
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return target Sring
	 */
	@RequestMapping(value = "/apply_confirm")
	public String apply_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("Electronic_Checksheet_Controller_apply_bill_apply_confirm ..");
		BaseResult bs = null;
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			bs = new BaseResult();

			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP2_LOCALE_DATA, null));
				if (!bs.getResult()) {
					// TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				} else {
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			} else {
				// Base64解碼
				String cusidn = new String(
						Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
						"utf-8");
				log.trace("card_overview_cusidn >>{} ", cusidn);
				okMap.put("CUSIDN", cusidn);

				bs = electronic_checksheet_service.apply_confirm(okMap);
				
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.STEP2_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("apply_confirm error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/electronic_checksheet/elec_bill_apply_confirm";
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
			log.trace("target >> {}", target);
			return target;
		}
	}

	/**
	 * 電子帳單申請結果頁
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return target Sring
	 */
	@ISTXNLOG(value="N1010_A")
	@RequestMapping(value = "/apply_result")
	public String apply_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("Electronic_Checksheet_Controller_apply_apply_result...");
		BaseResult bs = null;
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			bs = new BaseResult();
			
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP3_LOCALE_DATA, null));
				if (!bs.getResult()) {
					// TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				} else {
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			} else {
				// Base64解碼
				String cusidn = new String(
						Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
						"utf-8");
				log.trace("cancel_result_cusidn >>{} ", cusidn);
				// 取得從輸入頁輸入存在session中的輸入資料
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String, String> data = (Map<String, String>) bs.getData();
				data.putAll(okMap);
				data.put("CUSIDN", cusidn);
				data.put("Cust_id", cusidn);
				//data.put("sys_ip", CodeUtil.getIpAddress());
				// data.put("sys_ip", "172.22.11.23");

				bs.reset();
				bs = electronic_checksheet_service.apply_result(data);

				bs.addData("TYPE1", data.get("TYPE1"));
				bs.addData("TYPE2", data.get("TYPE2"));
				bs.addData("TYPE3", data.get("TYPE3"));
				bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.STEP3_LOCALE_DATA, bs);
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("apply_result error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/electronic_checksheet/elec_bill_apply_result";
				model.addAttribute("apply_result_data", bs);
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
			log.trace("target >> {}", target);
			return target;
		}
	}

	/**
	 * 電子帳單取消確認頁
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return target Sring
	 */
	@RequestMapping(value = "/cancel_confirm")
	public String cancel_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("Electronic_Checksheet_Controller_apply_bill_cancel_confirm...");
		BaseResult bs = null;
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
		try {
			bs = new BaseResult();
			
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP2_LOCALE_DATA, null));
				if (!bs.getResult()) {
					// TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				} else {
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			} else {
				// Base64解碼
				String cusidn = new String(
						Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
						"utf-8");
				log.trace("card_overview_cusidn >>{} ", cusidn);
				okMap.put("CUSIDN", cusidn);

				bs = electronic_checksheet_service.cancel_confirm(okMap);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.STEP2_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("cancel_confirm error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/electronic_checksheet/elec_bill_cancel_confirm";
				model.addAttribute(SessionUtil.TRANSFER_DATA, bs);
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
			log.trace("target >> {}", target);
			return target;
		}
	}

	/**
	 * 電子帳單取消結果頁
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return target Sring
	 */
	@ISTXNLOG(value="N1010_C")
	@RequestMapping(value = "/cancel_result")
	public String cancel_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("Electronic_Checksheet_Controller_apply_cancel_result...");
		BaseResult bs = null;
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			bs = new BaseResult();
			
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP3_LOCALE_DATA, null));
				if (!bs.getResult()) {
					// TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				} else {
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			} else {
				// Base64解碼
				String cusidn = new String(
						Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
						"utf-8");
				log.trace("cancel_result_cusidn >>{} ", cusidn);
				// 取得從輸入頁輸入存在session中的輸入資料
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String, String> data = (Map<String, String>) bs.getData();
				// data.put("TYPE", reqParam.get("TYPE"));
				// data.put("PINNEW", reqParam.get("PINNEW"));
				// data.put("BILLSENDMODE", reqParam.get("BILLSENDMODE"));
				// data.put("FGTXWAY", reqParam.get("FGTXWAY"));
				// data.put("DPMYEMAIL", reqParam.get("DPMYEMAIL"));
				okMap.putAll(data);
				okMap.put("CUSIDN", cusidn);

				bs.reset();
				bs = electronic_checksheet_service.cancel_result(okMap);

				bs.addData("TYPE4", okMap.get("TYPE4"));
				bs.addData("TYPE5", okMap.get("TYPE5"));
				bs.addData("TYPE6", okMap.get("TYPE6"));
				bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));

				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.STEP3_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("cancel_result error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/electronic_checksheet/elec_bill_cancel_result";
				model.addAttribute("cancel_result_data", bs);
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
			log.trace("target >> {}", target);
			return target;
		}
	}

	// 電子對帳單_重置密碼輸入頁
	@RequestMapping(value = "/elec_pw_reset")
	public String elec_pw_reset(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		boolean isDPUser = false;
		try {
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			
			isDPUser = electronic_checksheet_service.isDPUser(cusidn);

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("elec_pw_reset error >> {}",e);
		} finally {
			if(isDPUser) {
				target = "/electronic_checksheet/elec_pw_reset";
			}else {
				target = "/apply/apply_confirm";
			}
			return target;
		}
	}

	// 電子對帳單_重置密碼結果頁
	@RequestMapping(value = "/elec_pw_reset_result")
	public String elec_pw_reset_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("Electronic_Checksheet_Controller_elec_pw_reset_result...");
		BaseResult bs = null;
		try {
			
			
			bs = new BaseResult();
			// 查詢結果走RestService
			// Base64解碼
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			log.trace("elec_pw_reset_result_cusidn >>{} ", cusidn);
			reqParam.put("Cust_id", cusidn);
			reqParam.put("CUSIDN", cusidn);
			// data.put("sys_ip",request.getLocalAddr());
//			reqParam.put("sys_ip",CodeUtil.getIpAddress());
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					// TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				} else {
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			}else {
				log.debug(ESAPIUtil.vaildLog("elec_pw_reset_result req >>{}" + CodeUtil.toJson(okMap)));
				bs = electronic_checksheet_service.reset_result(okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("elec_pw_reset_result error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/electronic_checksheet/elec_pw_reset_result";
				model.addAttribute("reset_result_data", bs);
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
			log.trace("target >> {}", target);
			return target;
		}
	}

	// 電子對帳單_變更密碼
	@RequestMapping(value = "/elec_pw_alter")
	public String elec_pw_alter(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		boolean isDPUser = false;
		try {
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			
			isDPUser = electronic_checksheet_service.isDPUser(cusidn);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("elec_pw_alter error >> {}",e);
		} finally {
			
			if(isDPUser) {
				target = "/electronic_checksheet/elec_pw_alter";
			}else {
				target = "/apply/apply_confirm";
			}
			return target;
		}
	}

	// 電子對帳單_變更密碼結果頁
	@RequestMapping(value = "/elec_pw_alter_result")
	public String elec_pw_alter_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("Electronic_Checksheet_Controller_elec_pw_alter_result...");
		BaseResult bs = null;
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			bs = new BaseResult();
			
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			} else {
				// 查詢結果走RestService
				// Base64解碼
				String cusidn = new String(
						Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
						"utf-8");
				log.trace("elec_pw_reset_result_cusidn >>{} ", cusidn);
				okMap.put("Cust_id", cusidn);
				okMap.put("CUSIDN", cusidn);
				// data.put("sys_ip",request.getLocalAddr());
				// reqParam.put("sys_ip",CodeUtil.getIpAddress());
				log.debug(ESAPIUtil.vaildLog("elec_pw_alter_result req >>{}" + CodeUtil.toJsonCustom(okMap)));
				bs = electronic_checksheet_service.alter_result(okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("elec_pw_alter_result error >> {}",e);	
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/electronic_checksheet/elec_pw_alter_result";
				model.addAttribute("alter_result_data", bs);
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
			log.trace("target >> {}", target);
			return target;
		}
	}
	
	// 電子對帳單_補寄電子帳單輸入頁
		@RequestMapping(value = "/elec_bill_resent")
		public String elec_bill_resent(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			String target = "/error";
			try {
				// 清除切換語系時暫存的資料
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");

			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("elec_bill_resent error >> {}",e);
			} finally {
				target = "/electronic_checksheet/elec_bill_resent";
				return target;
			}
		}
		
		/**
		 *電子對帳單_補寄電子帳單結果頁
		 * 
		 * @param request
		 * @param response
		 * @param reqParam
		 * @param model
		 * @return target Sring
		 */
		@ISTXNLOG(value="N1010_R")
		@RequestMapping(value = "/elec_bill_resent_result")
		public String elec_bill_resent_result(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			String target = "/error";
			log.trace("elec_bill_resent_result...");
			BaseResult bs = null;
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			try {
				// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
				if (hasLocale) {
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
					if (!bs.getResult()) {
						log.trace("bs.getResult() >>{}", bs.getResult());
						throw new Exception();
					}
				} else {
					bs = new BaseResult();
					// 查詢結果走RestService
					// Base64解碼
					String cusidn = new String(
							Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
							"utf-8");
					log.trace("elec_bill_resent_result >>{} ", cusidn);
					okMap.put("CUSIDN", cusidn);
	
					bs = electronic_checksheet_service.elec_bill_resent_result(okMap);
	
					bs.addData("CMQTIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("elec_bill_resent_result error >> {}",e);
			} finally {
				if (bs != null && bs.getResult()) {
					target = "/electronic_checksheet/elec_bill_resent_result";
					model.addAttribute("elec_bill_resent_result", bs);
				} else {
					bs.setNext("/ELECTRONIC/CHECKSHEET/apply_bill");
					model.addAttribute(BaseResult.ERROR, bs);
				}
				log.trace("target >> {}", target);
				return target;
			}
		}
	

		// 電子對帳單_補寄電子帳單輸入頁
		@RequestMapping(value = "/aio_bill_query")
		public String aio_bill_query(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			String target = "/error";
			BaseResult bs = null;
			try {
				bs = new BaseResult();
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				bs = electronic_checksheet_service.aio_bill_query(cusidn);
				
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("elec_bill_resent error >> {}",e);
			} finally {
				if (bs != null && bs.getResult()) {
					target = "/electronic_checksheet/aio_bill_query";
					model.addAttribute("bs_result", bs);
				} else {
					bs.setNext("/ELECTRONIC/CHECKSHEET/apply_bill");
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}
			return target;
		}

		@RequestMapping("/aio_bill_resend")
		public @ResponseBody BaseResult aio_bill_resend(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
			
			BaseResult bs = null;
			try {
				bs = new BaseResult();
				Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				okMap.put("cUkey", cusidn);
				bs = electronic_checksheet_service.aio_bill_resend(okMap);
				model.addAttribute("bs",bs);
				
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("aio_bill_resend error >> {}",e);
			} 
			return bs;
		}
}