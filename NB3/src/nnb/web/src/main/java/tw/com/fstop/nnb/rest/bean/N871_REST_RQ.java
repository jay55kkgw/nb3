package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N871_REST_RQ extends BaseRestBean_TW implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4468058934969284989L;
	
	private String ACN; // 帳號
	private String CUSIDN; // 統一編號
	private String CMSDATE;
	private String CMEDATE;
	private String USERDATA_X50;
	
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getCMSDATE() {
		return CMSDATE;
	}
	public void setCMSDATE(String cMSDATE) {
		CMSDATE = cMSDATE;
	}
	public String getCMEDATE() {
		return CMEDATE;
	}
	public void setCMEDATE(String cMEDATE) {
		CMEDATE = cMEDATE;
	}
	public String getUSERDATA_X50() {
		return USERDATA_X50;
	}
	public void setUSERDATA_X50(String uSERDATA_X50) {
		USERDATA_X50 = uSERDATA_X50;
	}
}
