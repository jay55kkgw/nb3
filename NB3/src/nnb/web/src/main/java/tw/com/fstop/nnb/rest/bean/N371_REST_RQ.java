package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * N371電文RQ
 */
public class N371_REST_RQ extends BaseRestBean_FUND implements Serializable{
	private static final long serialVersionUID = 6868861910004819383L;
	private String CUSIDN;
	private String FGTXWAY;
	private String PINNEW;
	private String TYPE;
	private String TRANSCODE;
	private String CDNO;
	private String TRADEDATE;
	private String INTRANSCODE;
	private String UNIT;
	private String FCA1;
	private String AMT3;
	private String FCA2;
	private String AMT5;
	private String OUTACN;
	private String INTSACN;
	private String BILLSENDMODE;
	private String SSLTXNO;
	private String FUNDAMT;
	private String SHORTTRADE;
	private String SHORTTUNIT;
	private String PRO;
	private String KYCDATE;
	private String WEAK;
	private String RSKATT;
	private String RRSK;
	private String FDAGREEFLAG;
	private String FDNOTICETYPE;
	private String FDPUBLICTYPE;
	private String DPMYEMAIL;
	private String CMTRMAIL;
	private String IP;
	
	
	//for txnlog
	private String ADOPID = "C021";
	
	public String getCUSIDN(){
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN){
		CUSIDN = cUSIDN;
	}
	public String getFGTXWAY(){
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY){
		FGTXWAY = fGTXWAY;
	}
	public String getPINNEW(){
		return PINNEW;
	}
	public void setPINNEW(String pINNEW){
		PINNEW = pINNEW;
	}
	public String getTYPE(){
		return TYPE;
	}
	public void setTYPE(String tYPE){
		TYPE = tYPE;
	}
	public String getTRANSCODE(){
		return TRANSCODE;
	}
	public void setTRANSCODE(String tRANSCODE){
		TRANSCODE = tRANSCODE;
	}
	public String getTRADEDATE(){
		return TRADEDATE;
	}
	public void setTRADEDATE(String tRADEDATE){
		TRADEDATE = tRADEDATE;
	}
	public String getINTRANSCODE(){
		return INTRANSCODE;
	}
	public void setINTRANSCODE(String iNTRANSCODE){
		INTRANSCODE = iNTRANSCODE;
	}
	public String getUNIT(){
		return UNIT;
	}
	public void setUNIT(String uNIT){
		UNIT = uNIT;
	}
	public String getFCA1(){
		return FCA1;
	}
	public void setFCA1(String fCA1){
		FCA1 = fCA1;
	}
	public String getAMT3(){
		return AMT3;
	}
	public void setAMT3(String aMT3){
		AMT3 = aMT3;
	}
	public String getFCA2(){
		return FCA2;
	}
	public void setFCA2(String fCA2){
		FCA2 = fCA2;
	}
	public String getAMT5(){
		return AMT5;
	}
	public void setAMT5(String aMT5){
		AMT5 = aMT5;
	}
	public String getOUTACN(){
		return OUTACN;
	}
	public void setOUTACN(String oUTACN){
		OUTACN = oUTACN;
	}
	public String getINTSACN(){
		return INTSACN;
	}
	public void setINTSACN(String iNTSACN){
		INTSACN = iNTSACN;
	}
	public String getBILLSENDMODE(){
		return BILLSENDMODE;
	}
	public void setBILLSENDMODE(String bILLSENDMODE){
		BILLSENDMODE = bILLSENDMODE;
	}
	public String getSSLTXNO(){
		return SSLTXNO;
	}
	public void setSSLTXNO(String sSLTXNO){
		SSLTXNO = sSLTXNO;
	}
	public String getFUNDAMT(){
		return FUNDAMT;
	}
	public void setFUNDAMT(String fUNDAMT){
		FUNDAMT = fUNDAMT;
	}
	public String getSHORTTRADE(){
		return SHORTTRADE;
	}
	public void setSHORTTRADE(String sHORTTRADE){
		SHORTTRADE = sHORTTRADE;
	}
	public String getSHORTTUNIT(){
		return SHORTTUNIT;
	}
	public void setSHORTTUNIT(String sHORTTUNIT){
		SHORTTUNIT = sHORTTUNIT;
	}
	public String getPRO(){
		return PRO;
	}
	public void setPRO(String pRO){
		PRO = pRO;
	}
	public String getKYCDATE(){
		return KYCDATE;
	}
	public void setKYCDATE(String kYCDATE){
		KYCDATE = kYCDATE;
	}
	public String getWEAK(){
		return WEAK;
	}
	public void setWEAK(String wEAK){
		WEAK = wEAK;
	}
	public String getRSKATT(){
		return RSKATT;
	}
	public void setRSKATT(String rSKATT){
		RSKATT = rSKATT;
	}
	public String getRRSK(){
		return RRSK;
	}
	public void setRRSK(String rRSK){
		RRSK = rRSK;
	}
	public String getFDAGREEFLAG(){
		return FDAGREEFLAG;
	}
	public void setFDAGREEFLAG(String fDAGREEFLAG){
		FDAGREEFLAG = fDAGREEFLAG;
	}
	public String getFDNOTICETYPE(){
		return FDNOTICETYPE;
	}
	public void setFDNOTICETYPE(String fDNOTICETYPE){
		FDNOTICETYPE = fDNOTICETYPE;
	}
	public String getFDPUBLICTYPE(){
		return FDPUBLICTYPE;
	}
	public void setFDPUBLICTYPE(String fDPUBLICTYPE){
		FDPUBLICTYPE = fDPUBLICTYPE;
	}
	public String getDPMYEMAIL(){
		return DPMYEMAIL;
	}
	public void setDPMYEMAIL(String dPMYEMAIL){
		DPMYEMAIL = dPMYEMAIL;
	}
	public String getCMTRMAIL(){
		return CMTRMAIL;
	}
	public void setCMTRMAIL(String cMTRMAIL){
		CMTRMAIL = cMTRMAIL;
	}
	public String getCDNO() {
		return CDNO;
	}
	public void setCDNO(String cDNO) {
		CDNO = cDNO;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}

}