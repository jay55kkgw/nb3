package tw.com.fstop.nnb.service;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.po.TXNLOG;
import fstop.orm.po.TXNTWRECORD;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.tbb.nnb.dao.TxnLogDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.web.util.StrUtils;

@Service
public class TxnLog_Service {
	private Logger log = LoggerFactory.getLogger(this.getClass().getName());

	@Autowired
	TxnLogDao txnLogDao;
	/*
	 * 以顧客統編(cusidn)、電文、一段時間當作條件去查詢DB，回傳結果
	 */
	public BaseResult query(Map<String,String> reqParam) {
		BaseResult bs = new BaseResult();
		List<TXNLOG> list = null;
		//簡易時間格式檢查
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		sdf.setLenient(false);
		try {
			
			DateTime dt;
			Date d;
			//檢查統編是否為空值
			if(reqParam.get("cusidn").trim() == null || reqParam.get("cusidn").trim().length() == 0) {
				bs.setMessage("1", "統編為空值");
				bs.setResult(Boolean.FALSE);
				return bs;
				
			}
			
			int dtCon = 0;
			//判斷時間空值的狀況 0:全部有值;1:無起始日期;10:無結束日期;11:完全無日期;自動帶入前後六個月
			if(reqParam.get("sdate")== null || reqParam.get("sdate").trim().length() == 0) {
				dtCon += 1;
			}
			

			if(reqParam.get("edate")== null || reqParam.get("edate").trim().length() == 0) {
				dtCon += 10;
			}
			
			switch(dtCon) {
				case 1:
					dt = new DateTime(sdf.parse(reqParam.get("edate").replace("/", "").trim()));
					d = new Date(dt.minusMonths(6).getMillis());
					reqParam.put("sdate",sdf.format(d));
//					log.info("sdate="+reqParam.get("sdate"));
					break;
				case 10:
					dt = new DateTime(sdf.parse(reqParam.get("sdate").replace("/", "").trim()));
					d = new Date(dt.plusMonths(6).getMillis());
					reqParam.put("edate",sdf.format(d));
//					log.info("edate="+reqParam.get("edate"));
					break;
				case 11:
					dt = new DateTime();
					Date sd = new Date(dt.minusMonths(6).getMillis());
					Date ed = new Date(dt.getMillis());
					reqParam.put("sdate",sdf.format(sd));
					reqParam.put("edate",sdf.format(ed));
//					log.info("sdate="+reqParam.get("sdate"));
//					log.info("edate="+reqParam.get("edate"));
					break;
				default:
					d = sdf.parse(reqParam.get("sdate"));
					d = sdf.parse(reqParam.get("edate"));
					if(sdf.parse(reqParam.get("sdate")).getTime()>sdf.parse(reqParam.get("edate")).getTime()) {
						bs.setMessage("1", "日期錯誤，起訖日大於終止日");
						bs.setResult(Boolean.FALSE);
						return bs;
					}
					break;
			}
			
			list = txnLogDao.findByDateRange(reqParam);
			log.info("list.size= >> {}", list.size());
			bs.setData(list);
			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", "查詢成功");
			
		}catch(Exception e) {
			log.error("{}", e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", "查詢異常");
		}
		
		return bs;
	}
	/*
	 * 新增客戶交易紀錄進去DB
	 */
	public BaseResult add(Map<String,String> reqParam) {
		BaseResult bs = new BaseResult();
		TXNLOG po = null;
		//如果帶過來是null 空的 並且 po是有這欄位 則帶上logintype=MB
		if( StrUtils.isNull(reqParam.get("logintype"))) {
			reqParam.put("logintype", "MB");
		}
 		try {
 			//抓出po所有屬性
 			Field[] fs = TXNLOG.class.getDeclaredFields();
 			List<String> listName = new ArrayList<String>();
 			for(Field f : fs) {
 				if(!"serialVersionUID".equals(f.getName())) {
 					listName.add(f.getName().toLowerCase());
 				}
 			}
 			//檢查主鍵、統編是否為空值，若以外為空值則塞入空字串
			List<String> list = new ArrayList<String>();
			for(String str:listName) {
				
				if(reqParam.get(str) == null||reqParam.get(str).trim().length() == 0) {
					if("logintype".equals(str)) {
						reqParam.put("logintype","MB");
					}else {
						if("adtxno".equals(str)) {
							
							bs.setMessage("1", "流水號為空值");
							bs.setResult(Boolean.FALSE);
							return bs;
							
						}
						if("aduserid".equals(str)) {
							bs.setMessage("1", "使用者id為空值");
							bs.setResult(Boolean.FALSE);
							return bs;
						}
						reqParam.put(str,"");
					}
				}
				list.add(str);
//				log.info("str >> {}",str);
			}
			//將req的key轉成大寫
			for(String str:list) {
				reqParam.put(str.toUpperCase(),reqParam.get(str));
				reqParam.remove(str);
			}
			
//			log.info("req >> {}",reqParam);
			po = CodeUtil.objectCovert(TXNLOG.class, reqParam);
			txnLogDao.save(po);
			bs.setResult(Boolean.TRUE);
			bs.setMessage("0", "新增成功");
		}catch(Exception e) {
			log.error("{}", e.toString());
			e.getStackTrace();
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", "新增失敗");
		}
		return bs;
	}

}
