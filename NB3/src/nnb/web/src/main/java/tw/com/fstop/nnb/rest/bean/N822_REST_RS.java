package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N822_REST_RS extends BaseRestBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2497428491057957351L;
	
	private String RSPCOD;
	private String NAME;
	private String PHONE;
	private String LIMIT;
	
	
	public String getRSPCOD() {
		return RSPCOD;
	}
	public void setRSPCOD(String rSPCOD) {
		RSPCOD = rSPCOD;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getPHONE() {
		return PHONE;
	}
	public void setPHONE(String pHONE) {
		PHONE = pHONE;
	}
	public String getLIMIT() {
		return LIMIT;
	}
	public void setLIMIT(String lIMIT) {
		LIMIT = lIMIT;
	}
}
