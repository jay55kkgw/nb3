package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * 
 * 功能說明 :匯入匯款線上解款申請及註銷
 *
 */
public class F035_REST_RQ extends BaseRestBean_FX implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2337997908410851299L;

	private String pkcs7Sign;// IKEY

	private String jsondc;// IKEY

	private String PINNEW;// 網路銀行密碼（新）SSL 用

	private String CUSIDN;// 統一編號

	private String ADOPID;		// 電文代號

	private String TXTYPE;

	//IDGATE
	private String sessionID;
	private String idgateID;
	private String txnID;
	
	public String getPkcs7Sign()
	{
		return pkcs7Sign;
	}

	public void setPkcs7Sign(String pkcs7Sign)
	{
		this.pkcs7Sign = pkcs7Sign;
	}

	public String getJsondc()
	{
		return jsondc;
	}

	public void setJsondc(String jsondc)
	{
		this.jsondc = jsondc;
	}

	public String getPINNEW()
	{
		return PINNEW;
	}

	public void setPINNEW(String pINNEW)
	{
		PINNEW = pINNEW;
	}

	public String getCUSIDN()
	{
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN)
	{
		CUSIDN = cUSIDN;
	}

	public String getADOPID()
	{
		return ADOPID;
	}

	public void setADOPID(String aDOPID)
	{
		ADOPID = aDOPID;
	}

	public String getTXTYPE()
	{
		return TXTYPE;
	}

	public void setTXTYPE(String tXTYPE)
	{
		TXTYPE = tXTYPE;
	}


	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public String getIdgateID() {
		return idgateID;
	}

	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}

	public String getTxnID() {
		return txnID;
	}

	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}
}
