package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class BHWS_REST_RS extends BaseRestBean implements Serializable {

	
	private static final long serialVersionUID = -2984598977923484580L;
	/**
	 * 
	 */
	
	private String INWARDS ;
	private String INWARDSJS;
	private String IMGB64 ;
	private String EMAIL;
	
	public String getINWARDS() {
		return INWARDS;
	}
	public void setINWARDS(String iNWARDS) {
		INWARDS = iNWARDS;
	}
	public String getIMGB64() {
		return IMGB64;
	}
	public void setIMGB64(String iMGB64) {
		IMGB64 = iMGB64;
	}
	public String getINWARDSJS() {
		return INWARDSJS;
	}
	public void setINWARDSJS(String iNWARDSJS) {
		INWARDSJS = iNWARDSJS;
	}
	public String getEMAIL() {
		return EMAIL;
	}
	public void setEMAIL(String eMAIL) {
		EMAIL = eMAIL;
	}
}



	