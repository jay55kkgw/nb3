package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N841_REST_RQ extends BaseRestBean_PS implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1527863175185687181L;

	private String	ACN	;
	private String	CUSIDN ;
	private String	ADOPID ;
	private String  trancode;
	
	
	

	public String getTrancode() {
		return trancode;
	}
	public void setTrancode(String trancode) {
		this.trancode = trancode;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	

	
	
	

}
