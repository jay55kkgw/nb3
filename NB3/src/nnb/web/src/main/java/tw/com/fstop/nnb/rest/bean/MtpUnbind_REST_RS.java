package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 取消客戶綁定
 * 
 * @author Vincenthuang
 *
 */
public class MtpUnbind_REST_RS extends BaseRestBean_QR_RS implements Serializable {

}
