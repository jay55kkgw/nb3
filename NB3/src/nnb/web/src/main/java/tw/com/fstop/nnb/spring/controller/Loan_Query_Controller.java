package tw.com.fstop.nnb.spring.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.Loan_Query_Service;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({ SessionUtil.PD, SessionUtil.PRINT_DATALISTMAP_DATA, SessionUtil.CUSIDN,SessionUtil.RESULT_LOCALE_DATA})
@Controller
@RequestMapping(value="/LOAN/QUERY")
public class Loan_Query_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private Loan_Query_Service loan_query_service ;
   
	//借款明細查詢
	@RequestMapping(value = "/loan_detail")
	public String loan_detail(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model)
	{
		String target = "/error";
		BaseResult bs = null;
		log.trace("loan_detail");
    	try {
    		bs =new  BaseResult();
    		log.trace(ESAPIUtil.vaildLog("N320GO >>{}"+ reqParam.get("N320GO")));
    		log.trace(ESAPIUtil.vaildLog("N552GO >>{}"+ reqParam.get("N552GO")));
    		if(null==reqParam.get("N320GO")||"".equals(reqParam.get("N320GO"))){
    			reqParam.put("N320GO", "GO");
    		}
    		if(null==reqParam.get("N552GO")||"".equals(reqParam.get("N552GO"))) {
    			reqParam.put("N552GO", "GO");
    		}
    		String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
    		log.debug("cusidn"+cusidn);
    		reqParam.put("CUSIDN", cusidn);
    		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			bs = loan_query_service.loan_detail(okMap);		
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("loan_detail error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				target = "/loan_query/loan_detail";
				model.addAttribute("loan_detail", bs);
				log.trace("bs.getData() >> {}", bs.getData());
				//讓列印可以讀取資料
				List<List<Map<String,String>>> printList = new ArrayList<>();
				List<Map<String,String>> rowListMap = ((List<Map<String, String>>) ((Map<String, Object>) bs.getData()).get("TW"));
				List<Map<String,String>> rowListMap2 = ((List<Map<String, String>>) ((Map<String, Object>) bs.getData()).get("FX"));
				List<Map<String,String>> rowListMap3 = ((List<Map<String, String>>) ((Map<String, Object>) bs.getData()).get("CRY"));
				printList.add(rowListMap);
				printList.add(rowListMap2);
				printList.add(rowListMap3);
				log.trace("rowListMap={}",rowListMap);
				SessionUtil.addAttribute(model,SessionUtil.PRINT_DATALISTMAP_DATA,printList);
			}else {
				String previous = "/INDEX/index";
				bs.setPrevious(previous);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
    	return  target;
	}
	// 下期借款本息查詢
			@RequestMapping(value = "/next_loan")
			public String next_loan(HttpServletRequest request, HttpServletResponse response,
					@RequestParam Map<String, String> reqParam, Model model) {
				String target = "/error";
				BaseResult bs = null;
				log.trace("nextloan_query");
				try {
					bs = new BaseResult();
					String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
					reqParam.put("CUSIDN", cusidn);
					// 下期借款本息查詢結果
					bs = loan_query_service.next_loan_query(reqParam);
				} catch (Exception e) {
					//Avoid Information Exposure Through an Error Message
					//e.printStackTrace();
					log.error("next_loan error >> {}",e);
				} finally {
					if (bs != null && bs.getResult()) {
						
						target = "/loan_query/next_loan";
						model.addAttribute("nextloan_query", bs);

						Map<String, Object> dataMap = (Map) bs.getData();
						log.trace("dataMap={}", dataMap);
						List<List<Map<String,String>>> printList = new ArrayList<>();
						List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("TW");
						List<Map<String, String>> rowListMap2 = (List<Map<String, String>>) dataMap.get("FX");
						log.trace("rowListMap={}", rowListMap);
						log.trace("rowListMap2={}", rowListMap2);
						printList.add(rowListMap);
						printList.add(rowListMap2);
						SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, printList);

					} else {
						String previous = "/INDEX/index";
						bs.setPrevious(previous);
						model.addAttribute(BaseResult.ERROR, bs);

					}
				}
				return target;
			}
		// 已繳本息查詢_輸入頁
		@RequestMapping(value = "/paid_query")
		public String paid_query(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
		
			String target = "/error";
			
			String cusidn;
			try {
				cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
		
				BaseResult bs = null;
				if (cusidn != null) {
					// 清除切換語系時暫存的資料
					SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
					target = "/loan_query/paid_query";
				} else {
					model.addAttribute(BaseResult.ERROR, bs);
				}
		
			} catch (Exception e) {
				log.error("", e);
			}
			
			return target;
		}
		// 已繳本息查詢_結果頁
		@RequestMapping(value = "/paid_query_result")
		public String paid_query_result(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			String target = "/error";
			BaseResult bs = null;
			log.trace("paid_query_result");
			try {
				bs = new BaseResult();
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
				log.debug("cusidn {} ", cusidn);
				// 解決 Trust Boundary Violation
				Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
				// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
				if (hasLocale) {
					log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
					if (!bs.getResult()) {
						log.trace("bs.getResult() >>{}", bs.getResult());
						throw new Exception();
					}
				}
				else{
					bs.reset();
					String period = okMap.get("PERIOD");
					bs = loan_query_service.paid_query(cusidn,period);
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
			}catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("paid_query_result error >> {}",e);
			} finally {
				if (bs != null && bs.getResult()) {
		
					target = "/loan_query/paid_query_result";
					model.addAttribute("paid_query_result", bs);
		
					Map<String, Object> dataMap = (Map) bs.getData();
					log.trace("dataMap={}", dataMap);
					List<List<Map<String,String>>> printList = new ArrayList<>();
					List<Map<String, String>> rowListMap = (List<Map<String, String>>) dataMap.get("TW");
					List<Map<String, String>> rowListMap2 = (List<Map<String, String>>) dataMap.get("FX");
					log.trace("rowListMap={}", rowListMap);
					log.trace("rowListMap2={}", rowListMap2);
					printList.add(rowListMap);
					printList.add(rowListMap2);
					SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, printList);
		
				} else {
					model.addAttribute(BaseResult.ERROR, bs);
		
				}
			}
			
			return  target;

		}
}
