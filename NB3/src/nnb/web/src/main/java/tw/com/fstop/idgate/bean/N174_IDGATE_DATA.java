package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N174_IDGATE_DATA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -762951082705400880L;

	@SerializedName(value = "FGTRDATE")	//及時預約
	private String FGTRDATE;
	@SerializedName(value = "FGINACNO")	//約定非約定轉入
	private String FGINACNO;
	@SerializedName(value = "ADTXAMT")	//金額
	private String ADTXAMT;
//	@SerializedName(value = "AMOUNT")  //金額整數
//	private String AMOUNT;
//	@SerializedName(value = "AMOUNT_DIG")	//金額小數
//	private String AMOUNT_DIG;
	
	@SerializedName(value = "ADCURRENCY")	//幣別
	private String ADCURRENCY;
	@SerializedName(value = "CODE")
	private String CODE;
	@SerializedName(value = "AUTXFTM")	//
	private String AUTXFTM;
	public String getFGTRDATE() {
		return FGTRDATE;
	}
	public void setFGTRDATE(String fGTRDATE) {
		FGTRDATE = fGTRDATE;
	}
	public String getFGINACNO() {
		return FGINACNO;
	}
	public void setFGINACNO(String fGINACNO) {
		FGINACNO = fGINACNO;
	}
	public String getADTXAMT() {
		return ADTXAMT;
	}
	public void setADTXAMT(String aDTXAMT) {
		ADTXAMT = aDTXAMT;
	}
//	public String getAMOUNT() {
//		return AMOUNT;
//	}
//	public void setAMOUNT(String aMOUNT) {
//		AMOUNT = aMOUNT;
//	}
//	public String getAMOUNT_DIG() {
//		return AMOUNT_DIG;
//	}
//	public void setAMOUNT_DIG(String aMOUNT_DIG) {
//		AMOUNT_DIG = aMOUNT_DIG;
//	}
	public String getADCURRENCY() {
		return ADCURRENCY;
	}
	public void setADCURRENCY(String aDCURRENCY) {
		ADCURRENCY = aDCURRENCY;
	}
	public String getCODE() {
		return CODE;
	}
	public void setCODE(String cODE) {
		CODE = cODE;
	}
	public String getAUTXFTM() {
		return AUTXFTM;
	}
	public void setAUTXFTM(String aUTXFTM) {
		AUTXFTM = aUTXFTM;
	}




}
