package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 轉帳交易前代理行查詢收款戶資訊
 * 
 * @author Vincenthuang
 *
 */
public class MtpPreTransferInfoQry_REST_RS extends BaseRestBean_QR_RS implements Serializable {

	//收款戶最外層
	private AccountInfo AccountInfo;
//	private String BankCode;				//收款行金融機構代碼
//
//	public String getBankCode() {
//		return BankCode;
//	}
//	public void setBankCode(String bankCode) {
//		BankCode = bankCode;
//	}
	
	public AccountInfo getAccountInfo() {
		return AccountInfo;
	}
	public void setAccountInfo(AccountInfo accountInfo) {
		AccountInfo = accountInfo;
	}

	public class AccountInfo{
			
			/*
			 * 收款戶類型
			 * 01：個人收款戶
			 * 02：企業收款戶
			 * 
			 * */
		
			private String AccountType;
			private String AccountName;
			private String BankCode;				//收款行金融機構代碼

			public String getBankCode() {
				return BankCode;
			}
			public void setBankCode(String bankCode) {
				BankCode = bankCode;
			}
			public String getAccountType() {
				return AccountType;
			}
			public void setAccountType(String accountType) {
				AccountType = accountType;
			}
			public String getAccountName() {
				return AccountName;
			}
			public void setAccountName(String accountName) {
				AccountName = accountName;
			}
	}
}
