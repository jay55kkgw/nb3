package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class Ratequery_N022_RSDATA extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7879022290973389784L;

	private String ITR16;
	private String ITR17;
	private String ITR18;
	private String ITR19;
	private String ITR12;
	private String ITR13;
	private String ITR14;
	private String ITR15;
	private String HEADER;
	private String ITR10;
	private String CRY;
	private String ITR11;
	private String RECNO;
	private String COLOR;

	public String getITR16() {
		return ITR16;
	}

	public String getITR17() {
		return ITR17;
	}

	public String getITR18() {
		return ITR18;
	}

	public String getITR19() {
		return ITR19;
	}

	public String getITR12() {
		return ITR12;
	}

	public String getITR13() {
		return ITR13;
	}

	public String getITR14() {
		return ITR14;
	}

	public String getITR15() {
		return ITR15;
	}

	public String getHEADER() {
		return HEADER;
	}

	public String getITR10() {
		return ITR10;
	}

	public String getCRY() {
		return CRY;
	}

	public String getITR11() {
		return ITR11;
	}

	public String getRECNO() {
		return RECNO;
	}

	public String getCOLOR() {
		return COLOR;
	}

	public String getFILL() {
		return FILL;
	}

	public String getITR1() {
		return ITR1;
	}

	public String getITR2() {
		return ITR2;
	}

	public String getITR20() {
		return ITR20;
	}

	public String getTIME() {
		return TIME;
	}

	public String getCRYNAME() {
		return CRYNAME;
	}

	public String getITR5() {
		return ITR5;
	}

	public String getITR6() {
		return ITR6;
	}

	public String getITR3() {
		return ITR3;
	}

	public String getITR4() {
		return ITR4;
	}

	public String getITR9() {
		return ITR9;
	}

	public String getDATE() {
		return DATE;
	}

	public String getITR7() {
		return ITR7;
	}

	public String getITR8() {
		return ITR8;
	}

	public String getCOUNT() {
		return COUNT;
	}

	public String getSEQ() {
		return SEQ;
	}

	public void setITR16(String iTR16) {
		ITR16 = iTR16;
	}

	public void setITR17(String iTR17) {
		ITR17 = iTR17;
	}

	public void setITR18(String iTR18) {
		ITR18 = iTR18;
	}

	public void setITR19(String iTR19) {
		ITR19 = iTR19;
	}

	public void setITR12(String iTR12) {
		ITR12 = iTR12;
	}

	public void setITR13(String iTR13) {
		ITR13 = iTR13;
	}

	public void setITR14(String iTR14) {
		ITR14 = iTR14;
	}

	public void setITR15(String iTR15) {
		ITR15 = iTR15;
	}

	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}

	public void setITR10(String iTR10) {
		ITR10 = iTR10;
	}

	public void setCRY(String cRY) {
		CRY = cRY;
	}

	public void setITR11(String iTR11) {
		ITR11 = iTR11;
	}

	public void setRECNO(String rECNO) {
		RECNO = rECNO;
	}

	public void setCOLOR(String cOLOR) {
		COLOR = cOLOR;
	}

	public void setFILL(String fILL) {
		FILL = fILL;
	}

	public void setITR1(String iTR1) {
		ITR1 = iTR1;
	}

	public void setITR2(String iTR2) {
		ITR2 = iTR2;
	}

	public void setITR20(String iTR20) {
		ITR20 = iTR20;
	}

	public void setTIME(String tIME) {
		TIME = tIME;
	}

	public void setCRYNAME(String cRYNAME) {
		CRYNAME = cRYNAME;
	}

	public void setITR5(String iTR5) {
		ITR5 = iTR5;
	}

	public void setITR6(String iTR6) {
		ITR6 = iTR6;
	}

	public void setITR3(String iTR3) {
		ITR3 = iTR3;
	}

	public void setITR4(String iTR4) {
		ITR4 = iTR4;
	}

	public void setITR9(String iTR9) {
		ITR9 = iTR9;
	}

	public void setDATE(String dATE) {
		DATE = dATE;
	}

	public void setITR7(String iTR7) {
		ITR7 = iTR7;
	}

	public void setITR8(String iTR8) {
		ITR8 = iTR8;
	}

	public void setCOUNT(String cOUNT) {
		COUNT = cOUNT;
	}

	public void setSEQ(String sEQ) {
		SEQ = sEQ;
	}

	private String FILL;
	private String ITR1;
	private String ITR2;
	private String ITR20;
	private String TIME;
	private String CRYNAME;
	private String ITR5;
	private String ITR6;
	private String ITR3;
	private String ITR4;
	private String ITR9;
	private String DATE;
	private String ITR7;
	private String ITR8;
	private String COUNT;
	private String SEQ;
}
