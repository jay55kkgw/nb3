package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class GoldProduct implements  Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6719372853582042039L;

	public String currec;

	public String totrec;

	public String goodno;

	public String sell;

	public String sellt;

	public String buy;

	public String dpdiff;

	@Override
	public String toString() {
		return "GoldProduct [currec=" + currec + ", totrec=" + totrec + ", goodno=" + goodno + ", sell=" + sell
				+ ", sellt=" + sellt + ", buy=" + buy + ", dpdiff=" + dpdiff + "]";
	}

	public String getCurrec() {
		return currec;
	}

	public void setCurrec(String currec) {
		this.currec = currec;
	}

	public String getTotrec() {
		return totrec;
	}

	public void setTotrec(String totrec) {
		this.totrec = totrec;
	}

	public String getGoodno() {
		return goodno;
	}

	public void setGoodno(String goodno) {
		this.goodno = goodno;
	}

	public String getSell() {
		return sell;
	}

	public void setSell(String sell) {
		this.sell = sell;
	}

	public String getSellt() {
		return sellt;
	}

	public void setSellt(String sellt) {
		this.sellt = sellt;
	}

	public String getBuy() {
		return buy;
	}

	public void setBuy(String buy) {
		this.buy = buy;
	}

	public String getDpdiff() {
		return dpdiff;
	}

	public void setDpdiff(String dpdiff) {
		this.dpdiff = dpdiff;
	}

}
