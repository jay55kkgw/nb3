package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.List;

/**
 * GoldService電文RQ
 */
public class Ikeycheck_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4149590365191933890L;
	
	private String checkflag;

	public String getCheckflag() {
		return checkflag;
	}

	public void setCheckflag(String checkflag) {
		this.checkflag = checkflag;
	}
}