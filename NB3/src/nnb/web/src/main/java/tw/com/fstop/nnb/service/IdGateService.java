package tw.com.fstop.nnb.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fstop.orm.po.IDGATE_TXN_STATUS;
import fstop.orm.po.IDGATE_TXN_STATUS_PK;
import fstop.orm.po.QUICKLOGINMAPPING;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.idgate.bean.Base_IDGATE_DATA_VIEW;
import tw.com.fstop.nnb.rest.bean.AML_REST_RQ;
import tw.com.fstop.nnb.rest.bean.svCreate_Verify_Txn_REST_RQ;
import tw.com.fstop.tbb.nnb.dao.Idgate_Txn_StatusDao;
import tw.com.fstop.tbb.nnb.dao.QuickLoginMappingDao;
import tw.com.fstop.util.CodeUtil;

@Service
public class IdGateService extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	QuickLoginMappingDao quickLoginMappingDao;
	@Autowired
	Idgate_Txn_StatusDao idgate_txn_statusDao;
	
	/**
	 * 查詢Idgate身分
	 * 
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult checkIdentity(String cusidn) {
		BaseResult result = new BaseResult();
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// 查詢目前綁定或申請的裝置
			bs = IdGateMappingQuery_REST(cusidn);
			log.trace("IDGATE identityData>> "+CodeUtil.toJson(bs.getData()));
			Map<String, Object> bsdata = (Map<String, Object>) bs.getData();
			if (bs.getMsgCode().equals("0") && ((Map<String, Object>) bsdata.get("data")).get("REC")!=null) {
				// 取得目前預設裝置
				Map<String, String> Object = null;// 目標	
				Map<String, Object> DeviceData=null;
				DeviceData=(Map<String, Object>) bsdata.get("data");
				ArrayList<Map<String, String>> dataListMap = (ArrayList<Map<String, String>>) DeviceData.get("REC");
				if (dataListMap.size() > 0) {
					for (Map<String, String> dataMap : dataListMap) {
						if (dataMap.get("DEVICESTATUS").equals("0"))
							if (dataMap.get("ISDEFAULT").equals("Y")) {
								if (dataMap.get("QCKTRAN").equals("Y")) {
									Object = dataMap;
									break;
								}
							}
					}
					if (null != Object) {
						log.info("I am IDGATEuser");
						result.setResult(Boolean.TRUE);
						result.addAllData(Object);
					} else {
						log.info("I am not IDGATEuser");
						result.setResult(Boolean.FALSE);
					}
				} else {
					result.setResult(Boolean.FALSE);
				}
			} else {
				result.setResult(Boolean.FALSE);
				result.setMsgCode(bs.getMsgCode());
				result.setMessage(bs.getMessage());
			}
		} catch (Exception e) {
			log.error("IDGATE identity error >>" + e);
			result.setResult(Boolean.FALSE);
			if (bs.getMsgCode() != null)
				result.setMsgCode(bs.getMsgCode());
			if (bs.getMessage() != null)
				result.setMessage(bs.getMessage());
		}
		log.debug("IDGATE identity >> " + CodeUtil.toJson(result));
		return result;
	}

	/**
	 * 進行手機驗證
	 * 
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult goValidate(Map<String, Object> reqParam, Map<String,Object>IdgateData) {	
		log.debug("<<<<< IDGATE goValidate >>>>>");
		BaseResult bs=null;
		try {
			log.debug("Session data >> "+IdgateData);
			log.debug("REQ data >> "+CodeUtil.toJson(reqParam));
			/*===REQ值===
			 * adopid
			 * title
			 * ===Session值===
			 * txnData
			 * txnDataView
			 * idgateID
			 * deviceID
			 * ===固定值===
			 * resultCode=false
			 * authType=2
			 * push=1
			 * LOGINTYPE=NB
			 * */
			reqParam.put("txnData", IdgateData.get("txnData"));
			reqParam.put("txnDataView",IdgateData.get("txnDataView"));
			reqParam.put("idgateID", IdgateData.get("IDGATEID"));
			reqParam.put("deviceID", IdgateData.get("DEVICEID"));
			reqParam.put("resultCode", false);			
			//reqParam.put("authType", "2");//等手機驗證機制出來再用這個
			reqParam.put("authType", "0");
			reqParam.put("push", "1");
			//reqParam.put("LOGINTYPE", "NB");
			bs=svCreate_Verify_Txn_REST(reqParam);
		}catch(Exception e) {
			log.error("goValidate error {}",e);			
		}
		
		return bs;
	}
	/**
	 * 取消手機驗證
	 * 
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult cancelValidate(Map<String, Object> reqParam) {
		//從session取得sesssionid idgateid txnid後帶過來查
		log.info("取消手機驗證 ");
		log.debug("REQ >>"+CodeUtil.toJson(reqParam));
		BaseResult bs=null;
		try {
			bs=SvCancel_Txn_REST(reqParam);
		}
		catch(Exception e) {
			log.info("取得手機驗證結果失敗!!");
			log.error("getValidateResult >> {}",e);
		}
		return bs;
	}
	/**
	 * 取得手機驗證結果
	 * 
	 * 
	 * @param reqParam
	 * @return
	 */
	public String getValidateResult(Map<String, Object> reqParam) {
		//從session取得sesssionid idgateid txnid後帶過來查
		log.info("取得手機驗證結果 >>");
		log.debug("REQ >>"+CodeUtil.toJson(reqParam));
		IDGATE_TXN_STATUS po=new IDGATE_TXN_STATUS();
		IDGATE_TXN_STATUS_PK pk=new IDGATE_TXN_STATUS_PK();
		IDGATE_TXN_STATUS result=null;
		try {
			pk.setSESSIONID((String)reqParam.get("SESSIONID"));
			pk.setIDGATEID((String)reqParam.get("IDGATEID"));
			pk.setTXNID((String)reqParam.get("TXNID"));
			po.setPks(pk);
			po.setADOPID((String)reqParam.get("ADOPID"));
			result=idgate_txn_statusDao.findByPK(po);
			if(null!=result && null!=result.getTXNSTATUS()) {
				log.debug("REQ >>"+CodeUtil.toJson(result));
				return result.getTXNSTATUS();
			}else {
				return "false";
			}
		}
		catch(Exception e) {
			log.info("取得手機驗證結果失敗!!");
			log.error("getValidateResult >> {}",e);
		}
		return "false";
	}

	public Map<String, Object> coverTxnData(Class TD, Class TDV, Map<String, String> param){
		Map<String, Object> result = new HashMap<String, Object>();
		
		Map<String, String> txnData =  CodeUtil.objectCovert(Map.class, CodeUtil.objectCovert(TD, param));
		
		Base_IDGATE_DATA_VIEW tdv_object = (Base_IDGATE_DATA_VIEW)CodeUtil.objectCovert(TDV, param);
		Map<String, String> txnDataView =  tdv_object.coverMap();

		log.debug("txnData >> {}", CodeUtil.toJson(txnData));
		log.debug("txnDataView >> {}", CodeUtil.toJson(txnDataView));
		Map<String,Object>txndataview=new HashMap<>();
		List tmp=new ArrayList();
		for (Map.Entry<String,String> viewdata : txnDataView.entrySet()){
			Map<String,String>viewdatamap=new HashMap<>();
			viewdatamap.put(viewdata.getKey(),viewdata.getValue());
			tmp.add(viewdatamap);
		}
//		tmp.add(txnDataView);		
		txndataview.put("txndataview",tmp);
		log.debug("txnDataView >> {}", CodeUtil.toJson(txndataview));
		result.put("txnData", txnData);
//		result.put("txnDataView", CodeUtil.toJson(txnDataView));
		result.put("txnDataView", txndataview);
		result.put("IDGATEID", param.get("IDGATEID"));
		result.put("DEVICEID", param.get("DEVICEID"));
		
		return result;
	}
	
	/**
	 * 交易前驗證Idgate
	 * 
	 * 
	 * @param reqParam
	 * @return
	 */
	public Boolean transValidate(Map<String, String> reqParam) {
		return true;
	}
	
	/**
	 * NB3直接打MS IDGateCommand()
	 * 
	 * 
	 * @param reqParam
	 * @return
	 */
	public Boolean doIDGateCommand(Map reqParam,String ms_Channel) {
		//從session取得sesssionid idgateid txnid後帶過來查
		log.info("<< NB3 call MS IDGateCommand >>");
		log.debug("ms_Channel >> " +ms_Channel);
		log.debug("REQ >>" + CodeUtil.toJson(reqParam));
		boolean result = false;
		try {
			result = doIDGateCommand_REST(reqParam,ms_Channel);
		}
		catch(Exception e) {
			log.info("call MS IDGateCommand Error");
			log.error("doIDGateCommand >> {}",e.getMessage());
		}
		return result;
	}
}
