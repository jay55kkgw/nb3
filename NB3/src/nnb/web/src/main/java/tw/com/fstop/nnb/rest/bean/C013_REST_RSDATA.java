package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fstop.orm.po.ADMCURRENCY;
import fstop.orm.po.TXNFUNDDATA;
import tw.com.fstop.nnb.service.DaoService;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.TxnFundDataDao;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

public class C013_REST_RSDATA extends BaseRestBean implements Serializable
{
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3287200390404084713L;

	private String TRANSCODE; 		// 基金代碼
	private String TRANSCRY;		// 基金計價幣別
	private String PRICE1;			// 價格(淨值)
	private String UNIT;			// 單位數
	private String CRY;				// 信託金額幣別
	private String TRADEDATE;		// 生效日期(交易日期)
	
	private String PRICE2;			// 轉入價格
	private String INTRANSCODE;		// 轉入基金代碼
	private String INTRANSCRY;		// 轉入計價幣別
	private String TXUNIT;			// 轉入單位數
	private String EXRATE;			// 匯率
	private String MIP;				// 定期不定額註記
	private String TR106;			// 信託型態
	private String CDNO; 		// 信託號碼
	private String FUNDTYPE;		// 交易類別
	private String FUNDAMT;			// 信託金額
	private String PAYACN;			// 扣款帳號/卡號
	private String FEECRY;			// 手續費幣別
	private String FEEAMT;			// 手續費金額
	
	// 電文沒回應但要給頁面使用的資料
	// 中文幣別
	private String CRY_CRYNAME;				// 用 CRY 		查詢中文幣別
	private String INTRANSCRY_CRYNAME;		// 用 INTRANSCRY	查詢中文幣別
	private String FEECRY_CRYNAME;			// 用 FEECRY	查詢中文幣別
		
	// 基金名稱
	private String TRANSCODE_FUNDLNAME;		// 用 TRANSCODE	   查詢基金名稱
	private String INTRANSCODE_FUNDLNAME;	// 用 INTRANSCODE 查詢基金名稱
	
	// 暫存幣別名稱，減少重複查詢DB
	private Map<String, String> cryNamePool = new HashMap<>();
	
	// 暫存基金名稱，減少重複查詢DB
	private Map<String, String> fundLNamePool = new HashMap<>();
	
	/**
	 * 查詢中文幣別
	 * 
	 * @param cry
	 * @return
	 */
	public String getCryName(String cry)
	{
		log.debug("CRY >> {}", cry);
		String result = cry;
		try
		{
			if(cryNamePool.containsKey(cry))
			{
				result = cryNamePool.get(cry);
			}
			else
			{
				DaoService daoService = null;
				ADMCURRENCY po = null;
				// 檢查查詢條件
				if(StrUtil.isNotEmpty(cry))
				{
					// get Dao bean
					daoService = SpringBeanFactory.getBean(DaoService.class);
					log.debug("DaoService >>> {}", daoService);
				}
				
				if(daoService != null)
				{
					// query DB
					po = daoService.getCRY(cry);
					log.debug("ADMCURRENCY >>> {}", po);
				}
				
				// 檢查PK確認是否查到資料
				if(po != null && StrUtil.isNotEmpty(po.getADCURRENCY()))
				{
					result = po.getADCCYNAME();
					cryNamePool.put(cry, result);
				}
				else
				{
					log.warn("查無幣別中文名稱 CRY >> {}", cry);
				}
			}
		}
		catch (Exception e)
		{
			log.error("getCryName error. CRY >> {}", cry, e);
		}
		return result;
	}
	
	
	/**
	 * 取得基金全名
	 * 
	 * @param TRANSCODE
	 * @return
	 */
	public String getFundLName(String TRANSCODE)
	{
		log.debug("TRANSCODE >> {}", TRANSCODE);
		String result = TRANSCODE;
		try
		{
			if(fundLNamePool.containsKey(TRANSCODE))
			{
				result = fundLNamePool.get(TRANSCODE);
			}
			else
			{
				TxnFundDataDao txnFundDataDao = null;
				TXNFUNDDATA po = null;
				// 檢查查詢條件
				if(StrUtil.isNotEmpty(TRANSCODE))
				{
					// get Dao bean
					txnFundDataDao = SpringBeanFactory.getBean(TxnFundDataDao.class);
					log.debug("TxnFundDataDao >>> {}", txnFundDataDao);
				}
				
				if(txnFundDataDao != null)
				{
					// query DB
					po = txnFundDataDao.getByPK(TRANSCODE);
					log.debug("TXNFUNDDATA >>> {}", po);
				}
				
				// 檢查PK確認是否查到資料
				if(po != null && StrUtil.isNotEmpty(po.getTRANSCODE()))
				{
					result = po.getFUNDLNAME();
					fundLNamePool.put(TRANSCODE, result);
				}
			}
		}
		catch (Exception e)
		{
			log.error("getFUNDLNAME error. TRANSCODE >> {}", TRANSCODE, e);
		}
		return result;
	}
	
	public String getTRANSCODE() {
		return TRANSCODE;
	}
	
	public void setTRANSCODE(String tRANSCODE) {
		TRANSCODE = tRANSCODE;
	}
	
	public String getTRANSCRY() {
		return TRANSCRY;
	}
	
	public void setTRANSCRY(String tRANSCRY) {
		TRANSCRY = tRANSCRY;
	}
	
	public String getPRICE1() {
		return PRICE1;
	}
	
	// 0000000000 to 0.0000
	public String getPRICE1_F() 
	{
		String result = this.PRICE1;
		try
		{
			BigDecimal b = new BigDecimal(new BigInteger(result), 4);
			result = NumericUtil.fmtAmount(b.toPlainString(), 4);
		}
		catch (Exception e)
		{
			log.error("getPRICE1_F error. PRICE1 >> {}", PRICE1, e);
		}
		return result;
	}
	
	public void setPRICE1(String pRICE1) {
		PRICE1 = pRICE1;
	}
	public String getUNIT() {
		return UNIT;
	}
	
	// 000020000000 to 2,000.0000
	public String getUNIT_F() 
	{
		String result = this.UNIT;
		try
		{
			BigDecimal b = new BigDecimal(new BigInteger(result), 4);
			result = NumericUtil.fmtAmount(b.toPlainString(), 4);
		}
		catch (Exception e)
		{
			log.error("getUNIT_F error. UNIT >> {}", UNIT, e);
		}
		return result;
	}
	
	public void setUNIT(String uNIT) {
		UNIT = uNIT;
	}
	
	public String getCRY() {
		return CRY;
	}
	
	public void setCRY(String cRY) {
		CRY = cRY;
	}
	
	public String getTRADEDATE() {
		return TRADEDATE;
	}
	
	// 01080502 to 108/05/02
	public String getTRADEDATE_F() 
	{
		String result = this.TRADEDATE;
		String tradeDate = result;
		try
		{
			if(tradeDate.length() == 8 && tradeDate.startsWith("0"))
			{
				tradeDate.replaceFirst("0", "");
				result = DateUtil.addSlash2(tradeDate);
			}
		}
		catch (Exception e)
		{
			log.error("getTRADEDATE_F error. TRADEDATE >> {}", TRADEDATE, e);
		}
		return result;
	}
	
	public void setTRADEDATE(String tRADEDATE) {
		TRADEDATE = tRADEDATE;
	}
	
	public String getPRICE2() {
		return PRICE2;
	}
	
	// 0000000000 to 0.0000
	public String getPRICE2_F() 
	{
		String result = this.PRICE2;
		try
		{
			BigDecimal b = new BigDecimal(new BigInteger(result), 4);
			result = NumericUtil.fmtAmount(b.toPlainString(), 4);
		}
		catch (Exception e)
		{
			log.error("getPRICE2_F error. PRICE2 >> {}", PRICE2, e);
		}
		return result;
	}
	
	public void setPRICE2(String pRICE2) {
		PRICE2 = pRICE2;
	}
	
	public String getINTRANSCODE() {
		return INTRANSCODE;
	}
	
	public void setINTRANSCODE(String iNTRANSCODE) {
		INTRANSCODE = iNTRANSCODE;
	}
	
	public String getINTRANSCRY() {
		return INTRANSCRY;
	}
	
	public void setINTRANSCRY(String iNTRANSCRY) {
		INTRANSCRY = iNTRANSCRY;
	}
	
	public String getTXUNIT() {
		return TXUNIT;
	}
	
	// 000000000000 to 0.0000
	public String getTXUNIT_F() 
	{
		String result = this.TXUNIT;
		try
		{
			BigDecimal b = new BigDecimal(new BigInteger(result), 4);
			result = NumericUtil.fmtAmount(b.toPlainString(), 4);
		}
		catch (Exception e)
		{
			log.error("getTXUNIT_F error. TXUNIT >> {}", TXUNIT, e);
		}
		return result;
	}
	
	public void setTXUNIT(String tXUNIT) {
		TXUNIT = tXUNIT;
	}
	
	public String getEXRATE() {
		return EXRATE;
	}
	
	// 000309350 to 30.9350
	public String getEXRATE_F() 
	{
		String result = this.EXRATE;
		try
		{
			BigDecimal b = new BigDecimal(new BigInteger(result), 4);
			result = NumericUtil.fmtAmount(b.toPlainString(), 4);
		}
		catch (Exception e)
		{
			log.error("getEXRATE_F error. EXRATE >> {}", EXRATE, e);
		}
		return result;
	}
	
	public void setEXRATE(String eXRATE) {
		EXRATE = eXRATE;
	}
	
	public String getMIP() {
		return MIP;
	}
	
	public void setMIP(String mIP) {
		MIP = mIP;
	}
	
	public String getTR106() {
		return TR106;
	}
	
	// 數字轉I18N文字
	public String getTR106_F() 
	{
		String result = this.TR106;
		try
		{
			I18n i18n = SpringBeanFactory.getBean("i18n");
			if("1".equals(result))
			{
				// 單筆
				// TODO messages_zh_TW.properties
				result = i18n.getMsg("LB.X1847");
//				result = "單筆";
			}
			else if("2".equals(result))
			{
				// 定期
				if(this.MIP.equals("Y"))
				{
					// TODO messages_zh_TW.properties
					 result = i18n.getMsg("LB.W1087");
//					result = "定期不定額";
				}
				else
				{
					// TODO messages_zh_TW.properties
					result = i18n.getMsg("LB.W1086");
//					result = "定期定額";
				}
			}
		}
		catch (Exception e)
		{
			log.error("getTR106_F error. TR106 >> {}", TR106, e);
		}
		return result;
	}
	
	public void setTR106(String tR106) {
		TR106 = tR106;
	}
	
	public String getCDNO() {
		return CDNO;
	}
	
	// 01019044834 to 010190***34
	public String getCDNO_F() 
	{
		String result = this.CDNO;
		try
		{
			result = WebUtil.hideAccount(CDNO);
		}
		catch (Exception e)
		{
			log.error("getCDNO_F error. >> {}", CDNO, e);
		}
		return result;
	}
	
	public void setCDNO(String cDNO) {
		CDNO = cDNO;
	}


	public String getFUNDTYPE() {
		return FUNDTYPE;
	}
	
	/**
	 * 交易類別i18n
	 * 
	 * @return
	 */
	public String getFUNDTYPE_F() 
	{
		String result = this.FUNDTYPE;
		try
		{
			Map<String, Object> c013 = SpringBeanFactory.getBean("C013");
			Map<String, String> fundType = (Map<String, String>) c013.get("FUNDTYPE");
			I18n i18n = SpringBeanFactory.getBean("i18n");
			if(fundType.containsKey(FUNDTYPE))
			{
				result = i18n.getMsg(fundType.get(FUNDTYPE));
//				result = fundType.get(FUNDTYPE);
			}
		}
		catch (Exception e)
		{
			log.error("getFUNDTYPE_F error. FUNDTYPE >> {}", FUNDTYPE, e);
		}
		return result;
	}
	
	public void setFUNDTYPE(String fUNDTYPE) {
		FUNDTYPE = fUNDTYPE;
	}
	
	public String getFUNDAMT() {
		return FUNDAMT;
	}
	
	// 100000.00 to 100,000.00
	public String getFUNDAMT_F() 
	{
		String result = this.FUNDAMT;
		try
		{
			result = NumericUtil.fmtAmount(result, 2);
		}
		catch (Exception e)
		{
			log.error("getFUNDAMT_F error. FUNDAMT >> {}", FUNDAMT, e);
		}
		return result;
	}
	
	public void setFUNDAMT(String fUNDAMT) {
		FUNDAMT = fUNDAMT;
	}
	
	public String getPAYACN() {
		return PAYACN;
	}
	
	/**
	 * 存款帳號/信用卡卡號遮碼
	 * 
	 * @return
	 */
	public String getPAYACN_F() 
	{
		String payAcn = this.PAYACN;
		String result = payAcn;
		try
		{
			if(payAcn.length() > 11)
			{
				result = WebUtil.hideCardNum(payAcn);
			}
			else
			{
				result = WebUtil.hideAccount(payAcn);
			}
		}
		catch (Exception e)
		{
			log.error("getPAYACN_F error. PAYACN >> {}", payAcn, e);
		}
		return result;
	}
	
	public void setPAYACN(String pAYACN) {
		PAYACN = pAYACN;
	}
	
	public String getFEECRY() {
		return FEECRY;
	}
	
	public void setFEECRY(String fEECRY) {
		FEECRY = fEECRY;
	}
	
	public String getFEEAMT() {
		return FEEAMT;
	}
	
	// 0000000210000 to 2,100.00
	public String getFEEAMT_F() 
	{
		String result = this.FEEAMT;
		try
		{
			BigDecimal b = new BigDecimal(new BigInteger(result), 2);
			result = NumericUtil.fmtAmount(b.toPlainString(), 2);
		}
		catch (Exception e)
		{
			log.error("getFEEAMT_F error. FEEAMT >> {}", FEEAMT, e);
		}
		return result;
	}
	
	public void setFEEAMT(String fEEAMT) {
		FEEAMT = fEEAMT;
	}
	
	/**
	 * 用 CRY 查詢中文幣別
	 * @return
	 */
	public String getCRY_CRYNAME() 
	{
		String result = getCryName(this.CRY);
		setCRY_CRYNAME(result);
		return CRY_CRYNAME;
	}

	public void setCRY_CRYNAME(String cRY_CRYNAME) {
		CRY_CRYNAME = cRY_CRYNAME;
	}
	
	/**
	 * 用 INTRANSCRY	查詢中文幣別
	 * @return
	 */
	public String getINTRANSCRY_CRYNAME() 
	{
		String result = getCryName(this.INTRANSCRY);
		setINTRANSCRY_CRYNAME(result);
		return INTRANSCRY_CRYNAME;
	}

	public void setINTRANSCRY_CRYNAME(String iNTRANSCRY_CRYNAME) {
		INTRANSCRY_CRYNAME = iNTRANSCRY_CRYNAME;
	}
	
	/**
	 * 用 FEECRY	查詢中文幣別
	 * @return
	 */
	public String getFEECRY_CRYNAME() 
	{
		String result = getCryName(this.FEECRY);
		setFEECRY_CRYNAME(result);
		return FEECRY_CRYNAME;
	}

	public void setFEECRY_CRYNAME(String fEECRY_CRYNAME) {
		FEECRY_CRYNAME = fEECRY_CRYNAME;
	}

	/**
	 * 用 TRANSCODE 查詢基金名稱
	 * @return
	 */
	public String getTRANSCODE_FUNDLNAME() 
	{
		String result = getFundLName(this.TRANSCODE);
		setTRANSCODE_FUNDLNAME(result);
		return TRANSCODE_FUNDLNAME;
	}

	public void setTRANSCODE_FUNDLNAME(String tRANSCODE_FUNDLNAME) 
	{
		TRANSCODE_FUNDLNAME = tRANSCODE_FUNDLNAME;
	}
	
	/**
	 * 用 INTRANSCODE 查詢基金名稱
	 * @return
	 */
	public String getINTRANSCODE_FUNDLNAME() 
	{
		String result = getFundLName(this.INTRANSCODE);
		setINTRANSCODE_FUNDLNAME(result);
		return INTRANSCODE_FUNDLNAME;
	}

	public void setINTRANSCODE_FUNDLNAME(String iNTRANSCODE_FUNDLNAME) {
		INTRANSCODE_FUNDLNAME = iNTRANSCODE_FUNDLNAME;
	}
	

	public static void main(String[] args) {
//		C013_REST_RSDATA c = new C013_REST_RSDATA();
//		c.FUNDAMT = "100000.00";
//		c.TRADEDATE = "01080502";
//		c.setUNIT("000020000000");
//		c.setPRICE1("0000000000");
//		c.setEXRATE("000309350");
//		c.setTXUNIT("000000000000");
//		c.setPRICE2("0000000000");
//		c.setTR106("1");
//		System.out.println(c.getFUNDAMT_F());
//		System.out.println(c.getTRADEDATE_F());
//		System.out.println(c.getUNIT_F());
//		System.out.println(c.getPRICE1_F());
//		System.out.println(c.getEXRATE_F());
//		System.out.println(c.getTXUNIT_F());
//		System.out.println(c.getPRICE2_F());
//		System.out.println(c.getTR106_F());
	}
}
