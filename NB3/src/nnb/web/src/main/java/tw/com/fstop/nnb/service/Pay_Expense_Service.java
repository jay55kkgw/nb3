package tw.com.fstop.nnb.service;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.OriginalUtil;
import tw.com.fstop.util.StrUtil;

@Service
public class Pay_Expense_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private DaoService daoservice;
	
	@Autowired
	I18n i18n;

	/**
	 * 原N998判斷是否寄信通知的邏輯
	 * 
	 * String cusidn
	 * @return
	 */
	public boolean chkContains(String cusidn){
		Boolean chk_result = null;
		
		try {
			// 用cusidn查詢資料庫 TXNUSER.DPNOTIFY (e.x: 1,2,3,4,5,6,7)
			String dpnotify = daoservice.get_notifyInfo(cusidn);
			if( dpnotify != null ) {
				log.debug("Pay_Expense_Service.dpnotify: " + dpnotify);
				
				// 原N998判斷是否寄信通知的邏輯
				chk_result = OriginalUtil.chkContains("1", dpnotify);
				log.debug("Pay_Expense_Service.chk_result: " + chk_result);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("chkContains error >> {}",e);
		}
		
		return chk_result;
	}

	/**
	 * 前端格式轉換成電文格式前置作業
	 * @param reqParam
	 * @param newMap
	 * @param cusidn
	 */
	public void preProcessing(Map<String,String> reqParam){
		try {
			log.trace(ESAPIUtil.vaildLog("preProcessing.reqParam: {}"+CodeUtil.toJson(reqParam)));
			
			for (String key : reqParam.keySet()) {
				String parKey = key;
				String parValue = reqParam.get(key);
				log.trace(ESAPIUtil.vaildLog("Key: " + parKey + ", Value: " + parValue));
				
				/**
				 * 前端欄位形式檢核及轉換成電文格式
				 * 形式：轉換方式
				 */
				
				// 繳費類電文上行日期 西元年轉民國年、去掉"/"、10碼變7碼, e.x: "2019/03/31"-->"1080331"
				if( parValue.length()==10 && parValue.contains("/") && !parKey.equals("CMDATE") ){
					log.trace("date");
					// 轉換後重新賦值--去開頭的0轉為7碼
					parValue = DateUtil.convertDate(1, parValue, "yyyy/MM/dd", "yyyyMMdd");
					parValue = parValue.substring(parValue.length()-7, parValue.length());
				}
				
				// 金額 金額格式轉純數值, e.x: "1,870.00"-->"1870"
//				if( parValue.contains(",") && parValue.contains(".") ){
//					log.trace("currency");
//					// 轉換後重新賦值
//					NumberFormat format = NumberFormat.getInstance();
//					parValue = String.valueOf(format.parse(parValue).longValue());
//				}
				
				// AMOUNT 金額格式轉純數值, e.x: "1,870.00"-->"1870"
				if( "AMOUNT".equals(parKey) ){
					log.trace("currency");
					// 轉換後重新賦值
					NumberFormat format = NumberFormat.getInstance();
					parValue = String.valueOf(format.parse(parValue).longValue());
				}
				
				reqParam.replace(parKey, parValue);
			}
			
			log.trace(ESAPIUtil.vaildLog("preProcessing.reqParam_end: {}"+CodeUtil.toJson(reqParam)));
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("preProcessing error >> {}",e);
		}
	}
	
	
	/**
	 * 繳費繳稅_其他_繳費預約指定期間 前端格式轉換成電文格式前置作業
	 * @param reqParam
	 * @param newMap
	 * @param cusidn
	 */
	public void preProcessing_N070B(Map<String,String> reqParam){
		try {
			log.trace(ESAPIUtil.vaildLog("preProcessing.reqParam: {}"+CodeUtil.toJson(reqParam)));
			
			for (String key : reqParam.keySet()) {
				String parKey = key;
				String parValue = reqParam.get(key);
				log.trace(ESAPIUtil.vaildLog("Key: " + parKey + ", Value: " + parValue));
				
				/**
				 * 前端欄位形式檢核及轉換成電文格式
				 * 形式：轉換方式
				 */
				
				// 繳費類電文上行日期 西元年轉民國年、去掉"/"、10碼變7碼, e.x: "2019/03/31"-->"1080331"
				if( parValue.length()==10 && parValue.contains("/") && !parKey.equals("CMDATE") ){
					log.trace("date");
					// 單純去除/
					parValue = parValue.replaceAll("/", "");
					
				}
				
				if( "AMOUNT".equals(parKey) ){
					log.trace("currency");
					// 轉換後重新賦值
					NumberFormat format = NumberFormat.getInstance();
					parValue = String.valueOf(format.parse(parValue).longValue());
				}
				
				reqParam.replace(parKey, parValue);
			}
			
			log.trace(ESAPIUtil.vaildLog("preProcessing.reqParam_end: {}"+CodeUtil.toJson(reqParam)));
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("preProcessing error >> {}",e);
		}
	}
	
	/**
	 * N070A-學雜費_確認頁資料處理
	 * 
	 * @param 
	 * @return
	 */
	public BaseResult tuition_fee_confirm(Map<String, String> reqParam, String cusidn ) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("tuition_fee_confirm.reqParam: {}"+CodeUtil.toJson(reqParam)));
			bs = new  BaseResult();
			
			// 日期顯示修改
			if(StrUtil.isNotEmpty(reqParam.get("FGTXDATE")) && (reqParam.get("FGTXDATE").equals("1"))) {
				reqParam.put( "transfer_date" ,DateUtil.getDate("/"));
			}
			if(StrUtil.isNotEmpty(reqParam.get("FGTXDATE")) && (reqParam.get("FGTXDATE").equals("2"))) {
				reqParam.put( "transfer_date" ,reqParam.get("CMDATE"));
			}
			
			// 金額顯示修改
			reqParam.put("AMOUNT", NumericUtil.fmtAmount(reqParam.get("AMOUNT"), 0) );
			
			// 放入TXTOKEN
			bs = getTxToken();
			if(bs.getResult()) {
				reqParam.put("TXTOKEN",((Map<String,String>) bs.getData()).get("TXTOKEN"));
				bs.setData(reqParam);
				bs.setResult(Boolean.TRUE);
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("tuition_fee_confirm error >> {}",e);
		}
		return bs;
	}
	
	/**
	 *N070A-學雜費_結果頁資料處理
	 * 
	 * @param 
	 * @return
	 */
	public BaseResult tuition_fee_result(String cusidn, Map<String, String> reqParam ) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("tuition_fee_result.reqParam: {}"+CodeUtil.toJson(reqParam)));
			bs = new BaseResult();
			
			// 前端格式轉換成電文格式
			preProcessing(reqParam);
			
			// 舊Bean.writeLog所需欄位
			reqParam.put("UID", cusidn);
			reqParam.put("ADOPID", "N070A");
			
			// 打ms_tw
			bs = N070A_REST(reqParam, cusidn);
			
			// 交易結果
			log.trace("tuition_fee_result.getResult: {}", bs.getResult());
			
			// 成功的處理
			if(bs.getResult()) {
				// 下行電文沒有但在頁面要顯示、轉換格式
				bs.addData("FGTXDATE", reqParam.get("FGTXDATE")); // 即時/預約
				if("1".equals(reqParam.get("FGTXDATE"))) {
					// 即時
					bs.addData("CMTRMEMO", reqParam.get("CMTRMEMO")); // 交易備註
					bs.addData("CMTRMAIL", reqParam.get("CMTRMAIL")); // 轉出成功Email通知
					bs.addData("TSFACN", ((Map<String, String>) bs.getData()).get("INTSACN")); // 銷帳編號
					bs.addData("AMOUNT", NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("AMOUNT"), 0)); // 繳款金額
					bs.addData("O_TOTBAL", NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("O_TOTBAL"), 2)); // 轉出帳號帳上餘額
					bs.addData("O_AVLBAL", NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("O_AVLBAL"), 2)); // 轉出帳號可用餘額
				}else if("2".equals(reqParam.get("FGTXDATE"))) {
					// 預約
					bs.addData("CMDATE", reqParam.get("CMDATE")); // 轉帳日期
					bs.addData("OUTACN", reqParam.get("OUTACN")); // 轉出帳號
					bs.addData("TSFACN", reqParam.get("TSFACN")); // 銷帳編號
					bs.addData("AMOUNT", NumericUtil.fmtAmount(reqParam.get("AMOUNT"), 0)); // 繳款金額
					bs.addData("CMTRMEMO", reqParam.get("CMTRMEMO")); // 交易備註
					bs.addData("CMTRMAIL", reqParam.get("CMTRMAIL")); // 轉出成功Email通知
					
				}
				
				
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("tuition_fee_result error >> {}",e);
		}
		return bs;
	}
	
	
	/**
	 * N171-繳稅_確認頁資料處理
	 * 
	 * @param 
	 * @return
	 */
	public BaseResult pay_taxes_confirm( Map<String, String> reqParam, String cusidn ) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("pay_taxes_confirm.reqParam: {}"+CodeUtil.toJson(reqParam)));
			bs = new  BaseResult();
			
			// 日期顯示修改
			if(StrUtil.isNotEmpty(reqParam.get("FGTXDATE")) && (reqParam.get("FGTXDATE").equals("1"))) {
				reqParam.put( "transfer_date" ,DateUtil.getDate("/"));
			}
			if(StrUtil.isNotEmpty(reqParam.get("FGTXDATE")) && (reqParam.get("FGTXDATE").equals("2"))) {
				reqParam.put( "transfer_date" ,reqParam.get("CMDATE"));
			}
			
			// 金額顯示修改
			reqParam.put("AMOUNT", NumericUtil.fmtAmount(reqParam.get("AMOUNT"), 0) );
			
			// 放入TXTOKEN
			bs = getTxToken();
			if(bs.getResult()) {
				reqParam.put("TXTOKEN",((Map<String,String>) bs.getData()).get("TXTOKEN"));
				bs.setData(reqParam);
				bs.setResult(Boolean.TRUE);
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("pay_taxes_confirm error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * N171-繳稅_結果頁資料處理
	 * 
	 * @param 
	 * @return
	 */
	public BaseResult pay_taxes_result(String cusidn, Map<String, String> reqParam ) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("pay_taxes_result.reqParam: {}"+CodeUtil.toJson(reqParam)));
			bs = new BaseResult();
			
			// 前端格式轉換成電文格式
			preProcessing(reqParam);
			
			// 舊Bean.writeLog所需欄位
			reqParam.put("UID", cusidn);
			reqParam.put("ADOPID", "N171");
			
			// 打ms_tw
			bs = N171_REST(cusidn, reqParam);
			
			// 交易結果
			log.trace("pay_taxes_result.getResult: {}", bs.getResult());
			
			// 成功的處理
			if(bs.getResult()) {
				// 下行電文沒有但在頁面要顯示、轉換格式
				bs.addData("FGTXDATE", reqParam.get("FGTXDATE")); // 即時/預約
				if("1".equals(reqParam.get("FGTXDATE"))) {
					bs.addData("TAXTYPE1", reqParam.get("TAXTYPE1")); // 繳稅類別分類編號
					// 即時
					bs.addData("TaxTypeText", reqParam.get("TaxTypeText")); // 繳稅類別
					bs.addData("INTSACN1", reqParam.get("INTSACN1")); // 稽徵單位代號
					bs.addData("INTSACN2", reqParam.get("INTSACN2")); // 身分證字號/統一編號
					bs.addData("INTSACN3", reqParam.get("INTSACN3")); // 銷帳編號
					bs.addData("PAYDUED", reqParam.get("PAYDUED")); // 繳納截止日
					bs.addData("AMOUNT", NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("AMOUNT"), 0)); // 繳款金額
					bs.addData("CMTRMEMO", reqParam.get("CMTRMEMO")); // 交易備註
					bs.addData("CMTRMAIL", reqParam.get("CMTRMAIL")); // 轉出成功Email通知
					bs.addData("O_TOTBAL", NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("O_TOTBAL"), 2)); // 轉出帳號帳上餘額
					bs.addData("O_AVLBAL", NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("O_AVLBAL"), 2)); // 轉出帳號可用餘額
				}else if("2".equals(reqParam.get("FGTXDATE"))) {
					// 預約
					bs.addData("CMDATE", reqParam.get("CMDATE")); // 轉帳日期
					bs.addData("OUTACN", reqParam.get("OUTACN")); // 轉出帳號
					bs.addData("TaxTypeText", reqParam.get("TaxTypeText")); // 繳稅類別
					bs.addData("INTSACN1", reqParam.get("INTSACN1")); // 稽徵單位代號
					bs.addData("INTSACN2", reqParam.get("INTSACN2")); // 身分證字號/統一編號
					bs.addData("INTSACN3", reqParam.get("INTSACN3")); // 銷帳編號
					bs.addData("PAYDUED", reqParam.get("PAYDUED")); // 代收截止日
					bs.addData("AMOUNT", NumericUtil.fmtAmount(reqParam.get("AMOUNT"), 0)); // 繳稅金額
					bs.addData("CMTRMEMO", reqParam.get("CMTRMEMO")); // 交易備註
					bs.addData("TAXTYPE1", reqParam.get("TAXTYPE1")); // 交易備註
					
				}
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("pay_taxes_result error >> {}",e);
		}
		return bs;
	}
	
	
	/**
	 * N073-繳納期貨保證金(限本行)_確認頁資料處理
	 * 
	 * @param 
	 * @return
	 */
	public BaseResult futures_deposit_confirm(Map<String, String> reqParam, String cusidn ) {
		BaseResult bs = null;
		
		String fgsvacno = ""; // 約定/非約定
		String intsacn = ""; // 期貨帳號
		String agree = ""; // 期貨帳號 0:非約定 1:約定
		
		try {
			log.trace(ESAPIUtil.vaildLog("futures_deposit_confirm.reqParam: {}"+CodeUtil.toJson(reqParam)));
			bs = new  BaseResult();
			
			// 放入TXTOKEN
			bs = getTxToken();
			if(bs.getResult()) {
				reqParam.put("TXTOKEN",((Map<String,String>) bs.getData()).get("TXTOKEN"));

				// 金額顯示修改
				reqParam.put("AMOUNT", NumericUtil.fmtAmount(reqParam.get("AMOUNT"), 0) );
				
				// 約定/非約定
				fgsvacno = reqParam.get("FGSVACNO");
				log.debug(ESAPIUtil.vaildLog("futures_deposit_confirm.fgsvacno: {}"+fgsvacno));
				
				if("1".equals(fgsvacno)) {
					// 約定轉入帳號文字顯示
					String agreeAcno = reqParam.get("agreeAcno");
					log.debug(ESAPIUtil.vaildLog("futures_deposit_confirm.agreeAcno: {}"+ agreeAcno));
					reqParam.put("DPAGACNO_TEXT", agreeAcno) ;
					
					// 約定轉入帳號
					Map<String, String> map = CodeUtil.fromJson(reqParam.get("DPAGACNO"), Map.class);
					intsacn = map.get("ACN");
					agree = map.get("AGREE");
					
				}else {
					// 非約定轉入帳號
					intsacn = reqParam.get("DPACNO");
					log.debug(ESAPIUtil.vaildLog("futures_deposit_confirm.DPACNO: {}"+ intsacn));
					reqParam.put("DPAGACNO_TEXT" , "050-"+i18n.getMsg("LB.W0605") + " " + intsacn) ;
					
					// 非約定轉入帳號加入常用帳號
					if("1".equals(reqParam.get("ADDACN"))){
						bs = daoservice.add_common_acount(cusidn, "050-"+i18n.getMsg("LB.W0605"), "050", fgsvacno, intsacn);
					}
					
				}
				
				log.debug(ESAPIUtil.vaildLog("futures_deposit_confirm.transferInAccount: {}"+intsacn));
				log.debug("futures_deposit_confirm.errorMsg: {}",((Map<String,String>)bs.getData()).get("errorMsg"));
				
				// 將期貨帳號存起來
				reqParam.put("INTSACN", intsacn);
				reqParam.put("AGREE", agree);
				bs.addAllData(reqParam);
			}
			bs.setResult(Boolean.TRUE);
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("futures_deposit_confirm error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * N073-繳納期貨保證金(限本行)_結果頁資料處理
	 * 
	 * @param 
	 * @return
	 */
	public BaseResult futures_deposit_result(String cusidn, Map<String, String> reqParam ) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("futures_deposit_result.reqParam: {}"+CodeUtil.toJson(reqParam)));
			bs = new BaseResult();
			
			// 前端格式轉換成電文格式
			preProcessing(reqParam);
			
			// 舊Bean.writeLog所需欄位
			reqParam.put("UID", cusidn);
			reqParam.put("ADOPID", "N073");
			
			// 打ms_tw
			bs = N073_REST(cusidn, reqParam);
			
			// 交易結果
			log.trace("futures_deposit_result.getResult: {}", bs.getResult());
			
			// 成功的處理
			if(bs.getResult()) {
				// 下行電文沒有但在頁面要顯示、轉換格式
				bs.addData("OUTACN", reqParam.get("OUTACN")); // 轉出帳號
				bs.addData("INTSACN", reqParam.get("INTSACN")); // 期貨帳號
				bs.addData("CMTRMEMO", reqParam.get("CMTRMEMO")); // 交易備註
				bs.addData("CMTRMAIL", reqParam.get("CMTRMAIL")); // 轉出成功Email通知
				bs.addData("AMOUNT", NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("AMOUNT"), 0)); // 繳款金額
				bs.addData("O_TOTBAL", NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("O_TOTBAL"), 2)); // 轉出帳號帳上餘額
				bs.addData("O_AVLBAL", NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("O_AVLBAL"), 2)); // 轉出帳號可用餘額
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("futures_deposit_result error >> {}",e);
		}
		return bs;
	}
	

	/**
	 * N075-電費_確認頁資料處理
	 * 
	 * @param 
	 * @return
	 */
	public BaseResult electricity_fee_confirm(Map<String, String> reqParam, String cusidn ) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("electricity_fee_confirm.reqParam: {}"+CodeUtil.toJson(reqParam)));
			bs = new  BaseResult();
			
			// 日期顯示修改
			if(StrUtil.isNotEmpty(reqParam.get("FGTXDATE")) && (reqParam.get("FGTXDATE").equals("1"))) {
				reqParam.put( "transfer_date" ,DateUtil.getDate("/"));
			}
			if(StrUtil.isNotEmpty(reqParam.get("FGTXDATE")) && (reqParam.get("FGTXDATE").equals("2"))) {
				reqParam.put( "transfer_date" ,reqParam.get("CMDATE"));
			}
			
			// 金額顯示修改
			reqParam.put("AMOUNT", NumericUtil.fmtAmount(reqParam.get("AMOUNT"), 0) );
			
			// 放入TXTOKEN
			bs = getTxToken();
			if(bs.getResult()) {
				reqParam.put("TXTOKEN",((Map<String,String>) bs.getData()).get("TXTOKEN"));
				bs.setData(reqParam);
				bs.setResult(Boolean.TRUE);
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("electricity_fee_confirm error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * N075-電費_結果頁資料處理
	 * 
	 * @param 
	 * @return
	 */
	public BaseResult electricity_fee_result(String cusidn, Map<String, String> reqParam ) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("electricity_fee_result.reqParam: {}"+CodeUtil.toJson(reqParam)));
			bs = new BaseResult();
			
			// 前端格式轉換成電文格式
			preProcessing(reqParam);
			
			// 舊Bean.writeLog所需欄位
			reqParam.put("UID", cusidn);
			reqParam.put("ADOPID", "N075");
			
			// 打ms_tw
			bs = N075_REST(cusidn, reqParam);
			
			// 交易結果
			log.trace("electricity_fee_result.getResult: {}", bs.getResult());
			
			// 成功的處理
			if(bs.getResult()) {
				// 下行電文沒有但在頁面要顯示、轉換格式
				bs.addData("FGTXDATE", reqParam.get("FGTXDATE")); // 即時/預約
				if("1".equals(reqParam.get("FGTXDATE"))) {
					// 即時
					bs.addData("LIMDAT", DateUtil.convertDate(2, reqParam.get("LIMDAT"), "yyyMMdd", "yyy/MM/dd")); // 代收截止日
					bs.addData("CHKNUM", reqParam.get("CHKNUM")); // 查核碼
					bs.addData("CMTRMEMO", reqParam.get("CMTRMEMO")); // 交易備註
					bs.addData("CMTRMAIL", reqParam.get("CMTRMAIL")); // 轉出成功Email通知
					bs.addData("AMOUNT", NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("AMOUNT"), 0)); // 繳款金額
					bs.addData("O_TOTBAL", NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("O_TOTBAL"), 2)); // 轉出帳號帳上餘額
					bs.addData("O_AVLBAL", NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("O_AVLBAL"), 2)); // 轉出帳號可用餘額
				}else if("2".equals(reqParam.get("FGTXDATE"))) {
					// 預約
					bs.addData("CMDATE", reqParam.get("CMDATE")); // 轉帳日期
					bs.addData("OUTACN", reqParam.get("OUTACN")); // 轉出帳號
					bs.addData("LIMDAT", DateUtil.convertDate(2, reqParam.get("LIMDAT"), "yyyMMdd", "yyy/MM/dd")); // 代收截止日
					bs.addData("ELENUM", reqParam.get("ELENUM")); // 電號
					bs.addData("CHKNUM", reqParam.get("CHKNUM")); // 查核碼
					bs.addData("AMOUNT", NumericUtil.fmtAmount(reqParam.get("AMOUNT"), 0)); // 繳款金額
					bs.addData("CMTRMEMO", reqParam.get("CMTRMEMO")); // 交易備註
					bs.addData("CMTRMAIL", reqParam.get("CMTRMAIL")); // 轉出成功Email通知
					
				}
				
				
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("electricity_fee_result error >> {}",e);
		}
		return bs;
	}

	/**
	 * N750A-臺灣省自來水費_確認頁資料處理
	 * 
	 * @param 
	 * @return
	 */
	public BaseResult water_tw_fee_confirm(Map<String, String> reqParam, String cusidn) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("water_tw_fee_confirm.reqParam: {}"+CodeUtil.toJson(reqParam)));
			bs = new  BaseResult();
			
			// 日期顯示修改
			if(StrUtil.isNotEmpty(reqParam.get("FGTXDATE")) && (reqParam.get("FGTXDATE").equals("1"))) {
				reqParam.put( "transfer_date" ,DateUtil.getDate("/"));
			}
			if(StrUtil.isNotEmpty(reqParam.get("FGTXDATE")) && (reqParam.get("FGTXDATE").equals("2"))) {
				reqParam.put( "transfer_date" ,reqParam.get("CMDATE"));
			}
			
			// 金額顯示修改
			reqParam.put("AMOUNT", NumericUtil.fmtAmount(reqParam.get("AMOUNT"), 0) );
			
			// 放入TXTOKEN
			bs = getTxToken();
			if(bs.getResult()) {
				reqParam.put("TXTOKEN",((Map<String,String>) bs.getData()).get("TXTOKEN"));
				bs.setData(reqParam);
				bs.setResult(Boolean.TRUE);
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("water_tw_fee_confirm error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * N750A-臺灣省自來水費_結果頁資料處理
	 * 
	 * @param 
	 * @return
	 */
	public BaseResult water_tw_fee_result(String cusidn, Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("water_tw_fee_result.reqParam: {}" +CodeUtil.toJson(reqParam)));
			bs = new BaseResult();
			
			// 前端格式轉換成電文格式
			preProcessing(reqParam);
			
			// 舊Bean.writeLog所需欄位
			reqParam.put("UID", cusidn);
			reqParam.put("ADOPID", "N750A");
			
			// 打ms_tw
			bs = N750A_REST(cusidn, reqParam);
			
			// 交易結果
			log.trace("water_tw_fee_result.getResult: {}", bs.getResult());
			
			// 成功的處理
			if(bs.getResult()) {
				// 下行電文沒有但在頁面要顯示、轉換格式
				bs.addData("FGTXDATE", reqParam.get("FGTXDATE")); // 即時/預約
				
				// 即時
				if("1".equals(reqParam.get("FGTXDATE"))) {
					String totbal = (String) ((Map<String, String>) bs.getData()).get("TOTBAL");
					bs.addData("TOTBAL", NumericUtil.fmtAmount(NumericUtil.addDot(totbal, 2),2)); // 帳上餘額
					
					String avlbal = (String) ((Map<String, String>) bs.getData()).get("AVLBAL");
					bs.addData("AVLBAL", NumericUtil.fmtAmount(NumericUtil.addDot(avlbal, 2), 2)); // 可用餘額
					
					// 下行電文沒有但在頁面要顯示、轉換格式
					bs.addData("FGTXDATE", reqParam.get("FGTXDATE")); // 即時/預約
					bs.addData("PAYDUE", DateUtil.convertDate(2, reqParam.get("PAYDUE"), "yyyMMdd", "yyy/MM/dd")); // 繳費期限
					bs.addData("CMTRMEMO", reqParam.get("CMTRMEMO")); // 交易備註
					bs.addData("AMOUNT", NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("AMOUNT"), 0)); // 繳款金額
					
				} // 預約
				else if ("2".equals(reqParam.get("FGTXDATE"))) {
					bs.addData("CMDATE", reqParam.get("CMDATE")); // 轉帳日期
					bs.addData("OUTACN", reqParam.get("OUTACN")); // 轉出帳號
					bs.addData("PAYDUE", DateUtil.convertDate(2, reqParam.get("PAYDUE"), "yyyMMdd", "yyy/MM/dd")); // 代收期限
					bs.addData("WAT_NO", reqParam.get("WAT_NO")); // 銷帳編號
					bs.addData("CHKCOD", reqParam.get("CHKCOD")); // 查核碼
					bs.addData("AMOUNT", NumericUtil.fmtAmount(reqParam.get("AMOUNT"), 0)); // 繳款金額
					bs.addData("CMTRMEMO", reqParam.get("CMTRMEMO")); // 交易備註
					bs.addData("CMTRMAIL", reqParam.get("CMTRMAIL")); // 轉出成功Email通知
					
				}
				
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("water_tw_fee_result error >> {}",e);
		}
		return bs;
	}


	/**
	 * N750B-臺北市自來水費_確認頁資料處理
	 * 
	 * @param 
	 * @return
	 */
	public BaseResult water_tpe_fee_confirm(Map<String, String> reqParam, String cusidn) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("water_tpe_fee_confirm.reqParam: {}"+CodeUtil.toJson(reqParam)));
			bs = new  BaseResult();
			
			// 日期顯示修改
			if(StrUtil.isNotEmpty(reqParam.get("FGTXDATE")) && (reqParam.get("FGTXDATE").equals("1"))) {
				reqParam.put( "transfer_date" ,DateUtil.getDate("/"));
			}
			if(StrUtil.isNotEmpty(reqParam.get("FGTXDATE")) && (reqParam.get("FGTXDATE").equals("2"))) {
				reqParam.put( "transfer_date" ,reqParam.get("CMDATE"));
			}
			
			// 金額顯示修改
			reqParam.put("AMOUNT", NumericUtil.fmtAmount(reqParam.get("AMOUNT"), 0) );
			
			// 放入TXTOKEN
			bs = getTxToken();
			if(bs.getResult()) {
				reqParam.put("TXTOKEN",((Map<String,String>) bs.getData()).get("TXTOKEN"));
				bs.setData(reqParam);
				bs.setResult(Boolean.TRUE);
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("water_tpe_fee_confirm error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * N750B-臺北市自來水費_結果頁資料處理
	 * 
	 * @param 
	 * @return
	 */
	public BaseResult water_tpe_fee_result(String cusidn, Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("water_tpe_fee_result.reqParam: {}"+CodeUtil.toJson(reqParam)));
			bs = new BaseResult();
			
			// 前端格式轉換成電文格式
			preProcessing(reqParam);
			
			// 舊Bean.writeLog所需欄位
			reqParam.put("UID", cusidn);
			reqParam.put("ADOPID", "N750B");
			
			// 打ms_tw
			bs = N750B_REST(cusidn, reqParam);
			
			// 交易結果
			log.trace("water_tpe_fee_result.getResult: {}", bs.getResult());
			
			// 成功的處理
			if(bs.getResult()) {
				// 下行電文沒有但在頁面要顯示、轉換格式
				bs.addData("FGTXDATE", reqParam.get("FGTXDATE")); // 即時/預約
				
				// 即時
				if("1".equals(reqParam.get("FGTXDATE"))) {
					// 下行電文沒有但在頁面要顯示、轉換格式
					String totbal = (String) ((Map<String, String>) bs.getData()).get("TOTBAL");
					bs.addData("TOTBAL", NumericUtil.fmtAmount(NumericUtil.addDot(totbal, 2),2)); // 帳上餘額
					
					String avlbal = (String) ((Map<String, String>) bs.getData()).get("AVLBAL");
					bs.addData("AVLBAL", NumericUtil.fmtAmount(NumericUtil.addDot(avlbal, 2), 2)); // 可用餘額
					
					// 下行電文沒有但在頁面要顯示、轉換格式
					bs.addData("FGTXDATE", reqParam.get("FGTXDATE")); // 即時/預約
					bs.addData("PAYDUE", DateUtil.convertDate(2, reqParam.get("PAYDUE"), "yyyMMdd", "yyy/MM/dd")); // 繳費期限
					bs.addData("CMTRMEMO", reqParam.get("CMTRMEMO")); // 交易備註
					bs.addData("AMOUNT", NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("AMOUNT"), 0)); // 繳款金額
					
				} // 預約
				else if ("2".equals(reqParam.get("FGTXDATE"))) {
					bs.addData("CMDATE", reqParam.get("CMDATE")); // 轉帳日期
					bs.addData("OUTACN", reqParam.get("OUTACN")); // 轉出帳號
					bs.addData("BARCODE1", reqParam.get("BARCODE1")); // 條碼一
					bs.addData("BARCODE2", reqParam.get("BARCODE2")); // 條碼二
					bs.addData("BARCODE3", reqParam.get("BARCODE3")); // 條碼三
					bs.addData("WAT_NO", reqParam.get("WAT_NO")); // 水號
					bs.addData("AMOUNT", NumericUtil.fmtAmount(reqParam.get("AMOUNT"), 0)); // 繳款金額
					bs.addData("CMTRMEMO", reqParam.get("CMTRMEMO")); // 交易備註
					bs.addData("CMTRMAIL", reqParam.get("CMTRMAIL")); // 轉出成功Email通知
					
				}
				
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("water_tpe_fee_result error >> {}",e);
		}
		return bs;
	}
	
	
	/**
	 * N750C-勞保費_確認頁資料處理
	 * 
	 * @param 
	 * @return
	 */
	public BaseResult labor_insurance_confirm(Map<String, String> reqParam, String cusidn) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("labor_insurance_confirm.reqParam: {}"+CodeUtil.toJson(reqParam)));
			bs = new  BaseResult();
			
			// 日期顯示修改
			if(StrUtil.isNotEmpty(reqParam.get("FGTXDATE")) && (reqParam.get("FGTXDATE").equals("1"))) {
				reqParam.put( "transfer_date" ,DateUtil.getDate("/"));
			}
			if(StrUtil.isNotEmpty(reqParam.get("FGTXDATE")) && (reqParam.get("FGTXDATE").equals("2"))) {
				reqParam.put( "transfer_date" ,reqParam.get("CMDATE"));
			}
			
			// 金額顯示修改
			reqParam.put("AMOUNT", NumericUtil.fmtAmount(reqParam.get("AMOUNT"), 0) );
			
			// 放入TXTOKEN
			bs = getTxToken();
			if(bs.getResult()) {
				reqParam.put("TXTOKEN",((Map<String,String>) bs.getData()).get("TXTOKEN"));
				bs.setData(reqParam);
				bs.setResult(Boolean.TRUE);
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("labor_insurance_confirm error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * N750C-勞保費_結果頁資料處理
	 * 
	 * @param 
	 * @return
	 */
	public BaseResult labor_insurance_result(String cusidn, Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("labor_insurance_result.reqParam: {}"+CodeUtil.toJson(reqParam)));
			bs = new BaseResult();
			
			// 前端格式轉換成電文格式
			preProcessing(reqParam);
			
			// 舊Bean.writeLog所需欄位
			reqParam.put("UID", cusidn);
			reqParam.put("ADOPID", "N750C");
			
			// 打ms_tw
			bs = N750C_REST(cusidn, reqParam);
			
			// 交易結果
			log.trace("labor_insurance_result.getResult: {}", bs.getResult());
			
			// 成功的處理
			if(bs.getResult()) {
				// 下行電文沒有但在頁面要顯示、轉換格式
				bs.addData("FGTXDATE", reqParam.get("FGTXDATE")); // 即時/預約
				
				// 即時
				if("1".equals(reqParam.get("FGTXDATE"))) {
					// 下行電文沒有但在頁面要顯示、轉換格式
					String totbal = (String) ((Map<String, String>) bs.getData()).get("TOTBAL");
					bs.addData("TOTBAL", NumericUtil.fmtAmount(NumericUtil.addDot(totbal, 2),2)); // 帳上餘額
					
					String avlbal = (String) ((Map<String, String>) bs.getData()).get("AVLBAL");
					bs.addData("AVLBAL", NumericUtil.fmtAmount(NumericUtil.addDot(avlbal, 2), 2)); // 可用餘額
					
					// 下行電文沒有但在頁面要顯示、轉換格式
					bs.addData("FGTXDATE", reqParam.get("FGTXDATE")); // 即時/預約
					bs.addData("PAYDUE", DateUtil.convertDate(2, reqParam.get("PAYDUE"), "yyyMMdd", "yyy/MM/dd")); // 繳費期限
					bs.addData("CMTRMEMO", reqParam.get("CMTRMEMO")); // 交易備註
					bs.addData("AMOUNT", NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("AMOUNT"), 0)); // 繳款金額
					
				} // 預約
				else if ("2".equals(reqParam.get("FGTXDATE"))) {
					bs.addData("CMDATE", reqParam.get("CMDATE")); // 轉帳日期
					bs.addData("OUTACN", reqParam.get("OUTACN")); // 轉出帳號
					bs.addData("BARCODE1", reqParam.get("BARCODE1")); // 條碼一
					bs.addData("BARCODE2", reqParam.get("BARCODE2")); // 條碼二
					bs.addData("BARCODE3", reqParam.get("BARCODE3")); // 條碼三
					bs.addData("AMOUNT", NumericUtil.fmtAmount(reqParam.get("AMOUNT"), 0)); // 繳款金額
					bs.addData("CMTRMEMO", reqParam.get("CMTRMEMO")); // 交易備註
					bs.addData("CMTRMAIL", reqParam.get("CMTRMAIL")); // 轉出成功Email通知
					
				}

			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("labor_insurance_result error >> {}",e);
		}
		return bs;
	}
	
	
	/**
	 * N750E-健保費/補充保險費_確認頁資料處理
	 * 
	 * @param 
	 * @return
	 */
	public BaseResult health_insurance_confirm(Map<String, String> reqParam, String cusidn) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("health_insurance_confirm.reqParam: {}"+CodeUtil.toJson(reqParam)));
			bs = new  BaseResult();
			
			// 日期顯示修改
			if(StrUtil.isNotEmpty(reqParam.get("FGTXDATE")) && (reqParam.get("FGTXDATE").equals("1"))) {
				reqParam.put( "transfer_date" ,DateUtil.getDate("/"));
			}
			if(StrUtil.isNotEmpty(reqParam.get("FGTXDATE")) && (reqParam.get("FGTXDATE").equals("2"))) {
				reqParam.put( "transfer_date" ,reqParam.get("CMDATE"));
			}
			
			// 金額顯示修改
			reqParam.put("AMOUNT", NumericUtil.fmtAmount(reqParam.get("AMOUNT"), 0) );
			
			// 放入TXTOKEN
			bs = getTxToken();
			if(bs.getResult()) {
				reqParam.put("TXTOKEN",((Map<String,String>) bs.getData()).get("TXTOKEN"));
				bs.setData(reqParam);
				bs.setResult(Boolean.TRUE);
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("health_insurance_confirm error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * N750E-健保費/補充保險費_結果頁資料處理
	 * 
	 * @param 
	 * @return
	 */
	public BaseResult health_insurance_result(String cusidn, Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("health_insurance_result.reqParam: {}"+CodeUtil.toJson(reqParam)));
			bs = new BaseResult();
			
			// 前端格式轉換成電文格式
			preProcessing(reqParam);
			
			// 舊Bean.writeLog所需欄位
			reqParam.put("UID", cusidn);
			reqParam.put("ADOPID", "N750E");
			
			// 打ms_tw
			bs = N750E_REST(cusidn, reqParam);
			
			// 交易結果
			log.trace("health_insurance_result.getResult: {}", bs.getResult());
			
			// 成功的處理
			if(bs.getResult()) {
				// 下行電文沒有但在頁面要顯示、轉換格式
				bs.addData("FGTXDATE", reqParam.get("FGTXDATE")); // 即時/預約
				
				// 即時
				if("1".equals(reqParam.get("FGTXDATE"))) {
					// 下行電文沒有但在頁面要顯示、轉換格式
					String totbal = (String) ((Map<String, String>) bs.getData()).get("TOTBAL");
					bs.addData("TOTBAL", NumericUtil.fmtAmount(NumericUtil.addDot(totbal, 2),2)); // 帳上餘額
					
					String avlbal = (String) ((Map<String, String>) bs.getData()).get("AVLBAL");
					bs.addData("AVLBAL", NumericUtil.fmtAmount(NumericUtil.addDot(avlbal, 2), 2)); // 可用餘額
					
					// 下行電文沒有但在頁面要顯示、轉換格式
					bs.addData("FGTXDATE", reqParam.get("FGTXDATE")); // 即時/預約
					bs.addData("PAYDUE", DateUtil.convertDate(2, reqParam.get("PAYDUE"), "yyyMMdd", "yyy/MM/dd")); // 繳費期限
					bs.addData("CMTRMEMO", reqParam.get("CMTRMEMO")); // 交易備註
					bs.addData("AMOUNT", NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("AMOUNT"), 0)); // 繳款金額
					
				} // 預約
				else if ("2".equals(reqParam.get("FGTXDATE"))) {
					bs.addData("CMDATE", reqParam.get("CMDATE")); // 轉帳日期
					bs.addData("OUTACN", reqParam.get("OUTACN")); // 轉出帳號
					bs.addData("BARCODE1", reqParam.get("BARCODE1")); // 條碼一
					bs.addData("BARCODE2", reqParam.get("BARCODE2")); // 條碼二
					bs.addData("BARCODE3", reqParam.get("BARCODE3")); // 條碼三
					bs.addData("AMOUNT", NumericUtil.fmtAmount(reqParam.get("AMOUNT"), 0)); // 繳款金額
					bs.addData("CMTRMEMO", reqParam.get("CMTRMEMO")); // 交易備註
					bs.addData("CMTRMAIL", reqParam.get("CMTRMAIL")); // 轉出成功Email通知
					
				}
				
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("health_insurance_result error >> {}",e);
		}
		return bs;
	}
	
	
	/**
	 * N750F-國民年金保險費_確認頁資料處理
	 * 
	 * @param 
	 * @return
	 */
	public BaseResult national_pension_confirm(Map<String, String> reqParam, String cusidn) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("national_pension_confirm.reqParam: {}"+CodeUtil.toJson(reqParam)));
			bs = new  BaseResult();
			
			// 日期顯示修改
			if(StrUtil.isNotEmpty(reqParam.get("FGTXDATE")) && (reqParam.get("FGTXDATE").equals("1"))) {
				reqParam.put( "transfer_date" ,DateUtil.getDate("/"));
			}
			if(StrUtil.isNotEmpty(reqParam.get("FGTXDATE")) && (reqParam.get("FGTXDATE").equals("2"))) {
				reqParam.put( "transfer_date" ,reqParam.get("CMDATE"));
			}
			
			// 金額顯示修改
			reqParam.put("AMOUNT", NumericUtil.fmtAmount(reqParam.get("AMOUNT"), 0) );
			
			// 放入TXTOKEN
			bs = getTxToken();
			if(bs.getResult()) {
				reqParam.put("TXTOKEN",((Map<String,String>) bs.getData()).get("TXTOKEN"));
				bs.setData(reqParam);
				bs.setResult(Boolean.TRUE);
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("national_pension_confirm error >> {}",e);
		}
		return bs;
	}
	
	
	/**
	 * N750F-國民年金保險費_結果頁資料處理
	 * 
	 * @param 
	 * @return
	 */
	public BaseResult national_pension_result(String cusidn, Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("national_pension_result.reqParam: {}"+CodeUtil.toJson(reqParam)));
			bs = new BaseResult();
			
			// 前端格式轉換成電文格式
			preProcessing(reqParam);
			
			// 舊Bean.writeLog所需欄位
			reqParam.put("UID", cusidn);
			reqParam.put("ADOPID", "N750F");
			
			// 打ms_tw
			bs = N750F_REST(cusidn, reqParam);
			
			// 交易結果
			log.trace("national_pension_result.getResult: {}", bs.getResult());
			
			// 成功的處理
			if(bs.getResult()) {
				// 下行電文沒有但在頁面要顯示、轉換格式
				bs.addData("FGTXDATE", reqParam.get("FGTXDATE")); // 即時/預約
				
				// 即時
				if("1".equals(reqParam.get("FGTXDATE"))) {
					// 下行電文沒有但在頁面要顯示、轉換格式
					String totbal = (String) ((Map<String, String>) bs.getData()).get("TOTBAL");
					bs.addData("TOTBAL", NumericUtil.fmtAmount(NumericUtil.addDot(totbal, 2),2)); // 帳上餘額
					
					String avlbal = (String) ((Map<String, String>) bs.getData()).get("AVLBAL");
					bs.addData("AVLBAL", NumericUtil.fmtAmount(NumericUtil.addDot(avlbal, 2), 2)); // 可用餘額
					
					// 下行電文沒有但在頁面要顯示、轉換格式
					bs.addData("FGTXDATE", reqParam.get("FGTXDATE")); // 即時/預約
					bs.addData("CMTRMEMO", reqParam.get("CMTRMEMO")); // 交易備註
					bs.addData("AMOUNT", NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("AMOUNT"), 0)); // 繳款金額
					
				} // 預約
				else if ("2".equals(reqParam.get("FGTXDATE"))) {
					bs.addData("CMDATE", reqParam.get("CMDATE")); // 轉帳日期
					bs.addData("OUTACN", reqParam.get("OUTACN")); // 轉出帳號
					bs.addData("BARCODE1", reqParam.get("BARCODE1")); // 條碼一
					bs.addData("BARCODE2", reqParam.get("BARCODE2")); // 條碼二
					bs.addData("BARCODE3", reqParam.get("BARCODE3")); // 條碼三
					bs.addData("AMOUNT", NumericUtil.fmtAmount(reqParam.get("AMOUNT"), 0)); // 繳款金額
					bs.addData("CMTRMEMO", reqParam.get("CMTRMEMO")); // 交易備註
					bs.addData("CMTRMAIL", reqParam.get("CMTRMAIL")); // 轉出成功Email通知
					
				}
				
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("national_pension_result error >> {}",e);
		}
		return bs;
	}
	
	
	/**
	 * N750H-新制勞工退休金_確認頁資料處理
	 * 
	 * @param 
	 * @return
	 */
	public BaseResult labor_pension_confirm(Map<String, String> reqParam, String cusidn) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("labor_pension_confirm.reqParam: {}"+CodeUtil.toJson(reqParam)));
			bs = new  BaseResult();
			
			// 日期顯示修改
			if(StrUtil.isNotEmpty(reqParam.get("FGTXDATE")) && (reqParam.get("FGTXDATE").equals("1"))) {
				reqParam.put( "transfer_date" ,DateUtil.getDate("/"));
			}
			if(StrUtil.isNotEmpty(reqParam.get("FGTXDATE")) && (reqParam.get("FGTXDATE").equals("2"))) {
				reqParam.put( "transfer_date" ,reqParam.get("CMDATE"));
			}
			
			// 金額顯示修改
			reqParam.put("AMOUNT", NumericUtil.fmtAmount(reqParam.get("AMOUNT"), 0) );
			
			// 放入TXTOKEN
			bs = getTxToken();
			if(bs.getResult()) {
				reqParam.put("TXTOKEN",((Map<String,String>) bs.getData()).get("TXTOKEN"));
				bs.setData(reqParam);
				bs.setResult(Boolean.TRUE);
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("labor_pension_confirm error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * N750H-新制勞工退休金_結果頁資料處理
	 * 
	 * @param 
	 * @return
	 */
	public BaseResult labor_pension_result(String cusidn, Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("labor_pension_result.reqParam: {}"+CodeUtil.toJson(reqParam)));
			bs = new BaseResult();
			
			// 前端格式轉換成電文格式
			preProcessing(reqParam);
			
			// 舊Bean.writeLog所需欄位
			reqParam.put("UID", cusidn);
			reqParam.put("ADOPID", "N750H");
			
			// 打ms_tw
			bs = N750H_REST(cusidn, reqParam);
			
			// 交易結果
			log.trace("labor_pension_result.getResult: {}", bs.getResult());
			
			// 成功的處理
			if(bs.getResult()) {
				// 下行電文沒有但在頁面要顯示、轉換格式
				bs.addData("FGTXDATE", reqParam.get("FGTXDATE")); // 即時/預約
				
				// 即時
				if("1".equals(reqParam.get("FGTXDATE"))) {
					// 下行電文沒有但在頁面要顯示、轉換格式
					String totbal = (String) ((Map<String, String>) bs.getData()).get("TOTBAL");
					bs.addData("TOTBAL", NumericUtil.fmtAmount(NumericUtil.addDot(totbal, 2),2)); // 帳上餘額
					
					String avlbal = (String) ((Map<String, String>) bs.getData()).get("AVLBAL");
					bs.addData("AVLBAL", NumericUtil.fmtAmount(NumericUtil.addDot(avlbal, 2), 2)); // 可用餘額
					
					// 下行電文沒有但在頁面要顯示、轉換格式
					bs.addData("FGTXDATE", reqParam.get("FGTXDATE")); // 即時/預約
					bs.addData("CMTRMEMO", reqParam.get("CMTRMEMO")); // 交易備註
					bs.addData("AMOUNT", NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("AMOUNT"), 0)); // 繳款金額
					
				} // 預約
				else if ("2".equals(reqParam.get("FGTXDATE"))) {
					bs.addData("CMDATE", reqParam.get("CMDATE")); // 轉帳日期
					bs.addData("OUTACN", reqParam.get("OUTACN")); // 轉出帳號
					bs.addData("BARCODE1", reqParam.get("BARCODE1")); // 條碼一
					bs.addData("BARCODE2", reqParam.get("BARCODE2")); // 條碼二
					bs.addData("BARCODE3", reqParam.get("BARCODE3")); // 條碼三
					bs.addData("AMOUNT", NumericUtil.fmtAmount(reqParam.get("AMOUNT"), 0)); // 繳款金額
					bs.addData("CMTRMEMO", reqParam.get("CMTRMEMO")); // 交易備註
					bs.addData("CMTRMAIL", reqParam.get("CMTRMAIL")); // 轉出成功Email通知
					
				}
				
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("labor_pension_result error >> {}",e);
		}
		return bs;
	}
	
	
	/**
	 * N750G-欣欣瓦斯費_確認頁資料處理
	 * 
	 * @param 
	 * @return
	 */
	public BaseResult gas_fee_confirm(Map<String, String> reqParam, String cusidn) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("gas_fee_confirm.reqParam: {}"+CodeUtil.toJson(reqParam)));
			bs = new  BaseResult();
			
			// 日期顯示修改
			if(StrUtil.isNotEmpty(reqParam.get("FGTXDATE")) && (reqParam.get("FGTXDATE").equals("1"))) {
				reqParam.put( "transfer_date" ,DateUtil.getDate("/"));
			}
			if(StrUtil.isNotEmpty(reqParam.get("FGTXDATE")) && (reqParam.get("FGTXDATE").equals("2"))) {
				reqParam.put( "transfer_date" ,reqParam.get("CMDATE"));
			}
			
			// 金額顯示修改
			reqParam.put("AMOUNT", NumericUtil.fmtAmount(reqParam.get("AMOUNT"), 0) );
			
			// 放入TXTOKEN
			bs = getTxToken();
			if(bs.getResult()) {
				reqParam.put("TXTOKEN",((Map<String,String>) bs.getData()).get("TXTOKEN"));
				bs.setData(reqParam);
				bs.setResult(Boolean.TRUE);
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gas_fee_confirm error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * N750G-欣欣瓦斯費_結果頁資料處理
	 * 
	 * @param 
	 * @return
	 */
	public BaseResult gas_fee_result(String cusidn, Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("gas_fee_result.reqParam: {}"+CodeUtil.toJson(reqParam)));
			bs = new BaseResult();
			
			// 前端格式轉換成電文格式
			preProcessing(reqParam);
			
			// 舊Bean.writeLog所需欄位
			reqParam.put("UID", cusidn);
			reqParam.put("ADOPID", "N750G");
			
			// 打ms_tw
			bs = N750G_REST(cusidn, reqParam);
			
			// 交易結果
			log.trace("gas_fee_result.getResult: {}", bs.getResult());
			
			// 成功的處理
			if(bs.getResult()) {
				// 下行電文沒有但在頁面要顯示、轉換格式
				bs.addData("FGTXDATE", reqParam.get("FGTXDATE")); // 即時/預約
				
				// 即時
				if("1".equals(reqParam.get("FGTXDATE"))) {
					// 下行電文沒有但在頁面要顯示、轉換格式
					String totbal = (String) ((Map<String, String>) bs.getData()).get("TOTBAL");
					bs.addData("TOTBAL", NumericUtil.fmtAmount(NumericUtil.addDot(totbal, 2),2)); // 帳上餘額
					
					String avlbal = (String) ((Map<String, String>) bs.getData()).get("AVLBAL");
					bs.addData("AVLBAL", NumericUtil.fmtAmount(NumericUtil.addDot(avlbal, 2), 2)); // 可用餘額
					
					// 下行電文沒有但在頁面要顯示、轉換格式
					bs.addData("FGTXDATE", reqParam.get("FGTXDATE")); // 即時/預約
					bs.addData("CMTRMEMO", reqParam.get("CMTRMEMO")); // 交易備註
					bs.addData("AMOUNT", NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("AMOUNT"), 0)); // 繳款金額
					
				} // 預約
				else if ("2".equals(reqParam.get("FGTXDATE"))) {
					bs.addData("CMDATE", reqParam.get("CMDATE")); // 轉帳日期
					bs.addData("OUTACN", reqParam.get("OUTACN")); // 轉出帳號
					bs.addData("BARCODE1", reqParam.get("BARCODE1")); // 條碼一
					bs.addData("BARCODE2", reqParam.get("BARCODE2")); // 條碼二
					bs.addData("BARCODE3", reqParam.get("BARCODE3")); // 條碼三
					bs.addData("AMOUNT", NumericUtil.fmtAmount(reqParam.get("AMOUNT"), 0)); // 繳款金額
					bs.addData("CMTRMEMO", reqParam.get("CMTRMEMO")); // 交易備註
					bs.addData("CMTRMAIL", reqParam.get("CMTRMAIL")); // 轉出成功Email通知
					
				}
				
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("gas_fee_result error >> {}",e);
		}
		return bs;
	}
	
	
	/**
	 * N070B-其他費用_確認頁資料處理
	 * 
	 * @param 
	 * @return
	 */
	public BaseResult other_fee_confirm(Map<String, String> reqParam, String cusidn) {
		BaseResult bs = null;
		
		String fgsvacno = ""; // 約定/非約定
		String intsacn = ""; // 轉入帳號
		String tmp_dpbhno = "";
		String dpbhno = "";
		String hiddenCMSSL = "N"; // 決定是否顯示交易密碼選項
		
		try {
			log.trace(ESAPIUtil.vaildLog("other_fee_confirm.reqParam: {}"+CodeUtil.toJson(reqParam)));
			bs = new  BaseResult();
			
			// 放入TXTOKEN
			bs = getTxToken();
			if(bs.getResult()) {
				reqParam.put("TXTOKEN", ((Map<String,String>) bs.getData()).get("TXTOKEN"));
				
				// 日期顯示修改
				if(StrUtil.isNotEmpty(reqParam.get("FGTXDATE")) && (reqParam.get("FGTXDATE").equals("1"))) {
					reqParam.put( "transfer_date", DateUtil.getDate("/"));
				}
				if(StrUtil.isNotEmpty(reqParam.get("FGTXDATE")) && (reqParam.get("FGTXDATE").equals("2"))) {
					reqParam.put( "transfer_date", reqParam.get("CMDATE"));
				}
				
				// 金額顯示修改
				reqParam.put("AMOUNT", NumericUtil.fmtAmount(reqParam.get("AMOUNT"), 0) );
				reqParam.put("AMOUNT_FMT", NumericUtil.fmtAmount(reqParam.get("AMOUNT"), 0) );
				// 約定/非約定
				fgsvacno = reqParam.get("FGSVACNO");
				log.trace(ESAPIUtil.vaildLog("other_fee_confirm.fgsvacno: {}"+ fgsvacno));
				
				if("1".equals(fgsvacno)) {
					// 約定轉入帳號文字顯示
					String agreeAcno = reqParam.get("agreeAcno");
					log.debug(ESAPIUtil.vaildLog("futures_deposit_confirm.agreeAcno: {}"+ agreeAcno));
					reqParam.put("DPAGACNO_TEXT", agreeAcno) ;
					
					// 約定轉入帳號
					log.trace(ESAPIUtil.vaildLog("other_fee_confirm.DPAGACNO: {}"+CodeUtil.toJson(reqParam.get("DPAGACNO"))));
					Map<String, String> map = CodeUtil.fromJson(reqParam.get("DPAGACNO"), Map.class);
					intsacn = map.get("ACN");
					
					// 是否是約定帳號
					hiddenCMSSL = "0".equals(map.get("AGREE")) ? "Y" : "N"; // AGREE=0表非約定，不顯示SSL欄位
					
				} else {
					hiddenCMSSL = "Y"; // 非約定，不顯示SSL欄位
					
					tmp_dpbhno = reqParam.get("DPBHNO");
					dpbhno= reqParam.get("DPBHNO").split("-")[0];
					reqParam.put("DPBHNO", dpbhno);
					log.trace(ESAPIUtil.vaildLog("other_fee_confirm.dpbhno: {}"+CodeUtil.toJson(dpbhno)));
					
					// 非約定轉入帳號
					String DPACNO = reqParam.get("DPACNO");
					log.trace(ESAPIUtil.vaildLog("other_fee_confirm.DPACNO: {}"+CodeUtil.toJson(DPACNO)));
					intsacn = reqParam.get("DPACNO");
					reqParam.put("DPAGACNO_TEXT" , tmp_dpbhno + " " + DPACNO) ;
					// 非約定轉入帳號加入常用帳號
					if("1".equals(reqParam.get("ADDACN"))){
						bs = daoservice.add_common_acount(cusidn, tmp_dpbhno, dpbhno, fgsvacno, intsacn);
					}
					
				}
				
				log.trace(ESAPIUtil.vaildLog("other_fee_confirm.intsacn: {}"+CodeUtil.toJson(intsacn)));
				log.trace("other_fee_confirm.errorMsg: {}", ((Map<String,String>)bs.getData()).get("errorMsg"));
				
				// 將轉入帳號存起來
				reqParam.put("INTSACN", intsacn);
				
				// 決定是否顯示交易密碼選項
				reqParam.put("hiddenCMSSL", hiddenCMSSL);
				
				// 回傳處理後的資料
				bs.addAllData(reqParam);
			}
			bs.setResult(Boolean.TRUE);
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_fee_confirm error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * N070B-其他費用_結果頁資料處理
	 * 
	 * @param 
	 * @return
	 */
	public BaseResult other_fee_result(String cusidn, Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			log.trace(ESAPIUtil.vaildLog("other_fee_result.reqParam: {}"+CodeUtil.toJson(reqParam)));
			bs = new BaseResult();
			
			// 前端格式轉換成電文格式
			preProcessing_N070B(reqParam);
			
			// 舊Bean.writeLog所需欄位
			reqParam.put("UID", cusidn);
			reqParam.put("ADOPID", "N070B");
			
			// 打ms_tw
			bs = N070B_REST(cusidn, reqParam);
			
			// 交易結果
			log.trace("other_fee_result.getResult: {}", bs.getResult());
			
			// 成功的處理
			if(bs.getResult()) {
				// 下行電文沒有但在頁面要顯示、轉換格式
				bs.addData("FGTXDATE", reqParam.get("FGTXDATE")); // 即時/預約
				if("1".equals(reqParam.get("FGTXDATE"))) {
					// 即時
					bs.addData("CMTRMEMO", reqParam.get("CMTRMEMO")); // 交易備註
					bs.addData("CMTRMAIL", reqParam.get("CMTRMAIL")); // 轉出成功Email通知
					bs.addData("TSFACN", ((Map<String, String>) bs.getData()).get("INTSACN")); // 銷帳編號
					bs.addData("AMOUNT", NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("AMOUNT"), 0)); // 繳款金額
					bs.addData("O_TOTBAL", NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("O_TOTBAL"), 2)); // 轉出帳號帳上餘額
					bs.addData("O_AVLBAL", NumericUtil.fmtAmount(((Map<String, String>) bs.getData()).get("O_AVLBAL"), 2)); // 轉出帳號可用餘額
				}else if("2".equals(reqParam.get("FGTXDATE"))) {
					// 預約
					bs.addData("CMDATE", reqParam.get("CMDATE")); // 轉帳日期
					bs.addData("OUTACN", reqParam.get("OUTACN")); // 轉出帳號
					bs.addData("DPAGACNO_TEXT", reqParam.get("DPAGACNO_TEXT")); // 轉入帳號
					bs.addData("AMOUNT", NumericUtil.fmtAmount(reqParam.get("AMOUNT"), 0)); // 繳款金額
					bs.addData("CMTRMEMO", reqParam.get("CMTRMEMO")); // 交易備註
					bs.addData("CMTRMAIL", reqParam.get("CMTRMAIL")); // 轉出成功Email通知
					
				}else if("3".equals(reqParam.get("FGTXDATE"))) {
					// 預約期間
					bs.addData("CMDD", reqParam.get("CMDD")); // 每月轉帳日期
					String CMSDATE = reqParam.get("CMSDATE");
					String CMEDATE = reqParam.get("CMEDATE");
					//yyyymmdd >> yyyy/mm/dd
					CMSDATE = CMSDATE.substring(0, 4)+"/"+CMSDATE.substring(4, 6)+"/"+CMSDATE.substring(6, 8);
					CMEDATE = CMEDATE.substring(0, 4)+"/"+CMEDATE.substring(4, 6)+"/"+CMEDATE.substring(6, 8);
					bs.addData("CMSDATE", CMSDATE); // 每月轉帳起日
					bs.addData("CMEDATE", CMEDATE); // 每月轉帳迄期
					bs.addData("OUTACN", reqParam.get("OUTACN")); // 轉出帳號
					bs.addData("DPAGACNO_TEXT", reqParam.get("DPAGACNO_TEXT")); // 轉入帳號
					bs.addData("AMOUNT", NumericUtil.fmtAmount(reqParam.get("AMOUNT"), 0)); // 繳款金額
					bs.addData("CMTRMEMO", reqParam.get("CMTRMEMO")); // 交易備註
					bs.addData("CMTRMAIL", reqParam.get("CMTRMAIL")); // 轉出成功Email通知
					
				}
				
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("other_fee_result error >> {}",e);
		}
		return bs;
	}
	
	
}
