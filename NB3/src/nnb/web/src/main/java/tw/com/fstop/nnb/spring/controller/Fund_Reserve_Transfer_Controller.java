package tw.com.fstop.nnb.spring.controller;

import java.net.URLEncoder;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import fstop.orm.po.TXNFUNDDATA;
import org.springframework.beans.factory.annotation.Autowired;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.Fund_Reserve_Transfer_Service;
import tw.com.fstop.nnb.service.Fund_Transfer_Service;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.web.util.WebUtil;

/**
 * 基金預約轉換交易的Controller
 */
@SessionAttributes({SessionUtil.CUSIDN,SessionUtil.KYCDATE,SessionUtil.WEAK,SessionUtil.XMLCOD,SessionUtil.DPMYEMAIL,
					SessionUtil.STEP1_LOCALE_DATA,	SessionUtil.STEP2_LOCALE_DATA,	SessionUtil.STEP3_LOCALE_DATA,
					SessionUtil.CONFIRM_LOCALE_DATA,	SessionUtil.RESULT_LOCALE_DATA})
@Controller
@RequestMapping(value = "/FUND/RESERVE/TRANSFER")
public class Fund_Reserve_Transfer_Controller{
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private Fund_Transfer_Service fund_transfer_service;
	@Autowired
	private Fund_Reserve_Transfer_Service fund_reserve_transfer_service;
	
	/**
	 * 前往基金預約轉換選擇頁
	 */
	@RequestMapping(value = "/fund_reserve_transfer_select")
	public String fund_reserve_transfer_select(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		BaseResult bs = null;
		log.debug("fund_transfer_select");
		try{

			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				String TRANSCODE = requestParam.get("TRANSCODE");
				log.debug(ESAPIUtil.vaildLog("TRANSCODE={}"+TRANSCODE));
				model.addAttribute("TRANSCODE",TRANSCODE);
				
				
				String FEETYPE = requestParam.get("FEE_TYPE");
				log.debug(ESAPIUtil.vaildLog("FEE_TYPE={}"+FEETYPE));
				model.addAttribute("FEE_TYPE",FEETYPE);
				
				String FUSMON = requestParam.get("FUSMON");
				log.debug(ESAPIUtil.vaildLog("FUSMON={}"+FUSMON));
				model.addAttribute("FUSMON",FUSMON);
				
				String TRANSCRY = requestParam.get("CRY");
				log.debug(ESAPIUtil.vaildLog("TRANSCRY={}"+TRANSCRY));
				model.addAttribute("TRANSCRY",TRANSCRY);
				
				String COUNTRYTYPE = requestParam.get("COUNTRYTYPE");
				log.debug(ESAPIUtil.vaildLog("COUNTRYTYPE={}"+COUNTRYTYPE));
				model.addAttribute("COUNTRYTYPE",COUNTRYTYPE);
				
				List<TXNFUNDDATA> finalFundDataList = fund_transfer_service.getFundDataByGroupMark(FEETYPE,TRANSCODE,FUSMON,COUNTRYTYPE,TRANSCRY);
				if(finalFundDataList.size() == 0){
					model.addAttribute(BaseResult.ERROR,bs);
					return target;
				}
				model.addAttribute("finalFundDataList",finalFundDataList);
				
				bs = fund_reserve_transfer_service.prepareFundReserveTransferSelectData(requestParam);
				bs.addData("TRANSCODE",TRANSCODE);
				bs.addData("finalFundDataList",finalFundDataList);
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, bs);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_reserve_transfer_select error >> {}",e);
		}finally {
			if(bs != null && bs.getResult()) {
				target = "/fund/fund_reserve_transfer_select";
				model.addAttribute("result_data", bs);
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	/**
	 * 基金公開說明書同意，前往基金預約轉換手續費頁
	 */
	@RequestMapping(value = "/fund_reserve_transfer_fee_expose")
	public String fund_reserve_transfer_fee_expose(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		BaseResult bs = null;
		log.debug("fund_reserve_transfer_fee_expose");
		try{
			bs = new BaseResult();
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP3_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
				requestParam.put("CUSIDN",cusidn);
				
				String KYCDATE = (String)SessionUtil.getAttribute(model,SessionUtil.KYCDATE,null);
				log.debug("KYCDATE={}",KYCDATE);
				requestParam.put("GETLTD",KYCDATE);
				
				bs = fund_reserve_transfer_service.prepareFundReserveTransferFeeExposeData(requestParam);
				SessionUtil.addAttribute(model, SessionUtil.STEP3_LOCALE_DATA, bs);
			}
			//IKEY要使用的JSON:DC
		 	String jsondc = URLEncoder.encode(CodeUtil.toJson(requestParam),"UTF-8");
		 	log.debug(ESAPIUtil.vaildLog("jsondc={}"+jsondc));
		 	model.addAttribute("jsondc",jsondc);
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_reserve_transfer_fee_expose error >> {}",e);
		}finally {
			
			if(bs != null && bs.getResult()) {
				target = "fund/fund_fee_expose_bs";
				model.addAttribute("result_data", bs);
			}else {
				bs.setPrevious("/FUND/TRANSFER/query_fund_transfer_data");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	/**
	 * 前往基金預約轉換確認頁
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/fund_reserve_transfer_confirm")
	public String fund_reserve_transfer_confirm(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		BaseResult bs = null;
		log.debug("fund_reserve_transfer_confirm");
		try{
			bs = new BaseResult();
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
				log.debug("cusidn={}",cusidn);
				
				bs = fund_reserve_transfer_service.prepareFundReserveTransferConfirmData(requestParam, cusidn);
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_reserve_transfer_confirm error >> {}",e);
		}finally {
			if(bs != null && bs.getResult()) {
				target = "fund/fund_reserve_transfer_confirm";
				model.addAttribute("result_data", bs);
			}else {
				bs.setPrevious("/FUND/TRANSFER/query_fund_transfer_data");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	/**
	 * 前往基金預約轉換結果頁
	 */
	@RequestMapping(value = "/fund_reserve_transfer_result")
	public String fund_reserve_transfer_result(HttpServletRequest request,@RequestParam Map<String,String> requestParam,Model model){
		String target = "/error";
		BaseResult bs = null;
		
		log.debug("fund_reserve_transfer_result");
		try{
			bs = new BaseResult();
			Map<String, String> okMap = ESAPIUtil.validStrMap(requestParam);
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				String cusidn = new String(Base64.getDecoder().decode((String)SessionUtil.getAttribute(model,SessionUtil.CUSIDN,null)),"UTF-8");
				log.debug("cusidn={}",cusidn);
				requestParam.put("CUSIDN",cusidn);
				requestParam.put("UID",cusidn);
				
				
				String KYCDATE = (String)SessionUtil.getAttribute(model,SessionUtil.KYCDATE,null);
				log.debug("KYCDATE={}",KYCDATE);
				requestParam.put("KYCDATE",KYCDATE);
				
				String WEAK = (String)SessionUtil.getAttribute(model,SessionUtil.WEAK,null);
				log.debug("WEAK={}",WEAK);
				requestParam.put("WEAK",WEAK);
				
				//寄件者信箱
				String DPMYEMAIL = (String)SessionUtil.getAttribute(model,SessionUtil.DPMYEMAIL,null);
				log.debug("DPMYEMAIL={}",DPMYEMAIL);
				requestParam.put("DPMYEMAIL",DPMYEMAIL);
				
				//收件者信箱
				requestParam.put("CMTRMAIL",DPMYEMAIL);
				
				String IP = WebUtil.getIpAddr(request);
				log.debug(ESAPIUtil.vaildLog("IP >> " + IP));
				requestParam.put("IP",IP);
				bs = fund_reserve_transfer_service.processFundReserveTransfer(requestParam);
	
				String hiddenCUSIDN = WebUtil.hideID(cusidn);
				bs.addData("hiddenCUSIDN",hiddenCUSIDN);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fund_reserve_transfer_result error >> {}",e);
		}finally {
			if(bs != null && bs.getResult()) {
				target = "fund/fund_reserve_transfer_result";
				model.addAttribute("result_data", bs);
			}else {
				bs.setPrevious("/FUND/TRANSFER/query_fund_transfer_data");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
}