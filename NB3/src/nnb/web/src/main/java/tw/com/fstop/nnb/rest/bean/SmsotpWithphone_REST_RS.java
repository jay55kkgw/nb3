package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class SmsotpWithphone_REST_RS extends BaseRestBean implements Serializable {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4881687269445877296L;
	
	private String MSGCOD;
	private String MSGSTR;
	private String phone;
	
	public String getMSGCOD() {
		return MSGCOD;
	}
	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}
	public String getMSGSTR() {
		return MSGSTR;
	}
	public void setMSGSTR(String mSGSTR) {
		MSGSTR = mSGSTR;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
}
