package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N106_REST_RS extends BaseRestBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6986769197619506119L;
	
	//N1060 回傳
	private String RSPCOD;
	private String USERDATA;
	private String TYPE;
	private String CURDATE;
	private String REC_NO;
	
	private LinkedList<N106_REST_RSDATA> REC;
	//N1060 回傳 end
	
	//N1061回傳
	private String BUSIDN;
	private String CTYIDN;
	private String TAXBAID;
	private String SEQCLM;
	private String FORM;
	private String CUSIDN;
	private String TAXPYCD;
	private String COUNTRY;
	private String TAX;
	private String NAME;
	private String ADDRESS;
	private String YEARLY;
	private String LIVE183;
	private String BACKAMT;
	private String TAMPAY;
	private String N106RATE;
	private String AMTTAX;
	private String NETPAY;
	private String W_TAX;
	private String C_TAX;
	private String ACN1;
	private String ACN2;
	private String ACN3;
	private String ACN4;
	private String ACN5;
	private String ACN6;
	private String ACN7;
	private String ACN8;
	private String ACN9;
	private String ACN10;
	private String ACN11;
	private String ACN12;
	private String TBB_OWNER;
	private String CUSNAME;
	private String ENDCOD; 
	//N1061 回傳 end
	
	public String getRSPCOD() {
		return RSPCOD;
	}
	public String getUSERDATA() {
		return USERDATA;
	}
	public String getTYPE() {
		return TYPE;
	}
	public String getCURDATE() {
		return CURDATE;
	}
	public String getREC_NO() {
		return REC_NO;
	}
	public LinkedList<N106_REST_RSDATA> getREC() {
		return REC;
	}
	public String getBUSIDN() {
		return BUSIDN;
	}
	public String getCTYIDN() {
		return CTYIDN;
	}
	public String getTAXBAID() {
		return TAXBAID;
	}
	public String getSEQCLM() {
		return SEQCLM;
	}
	public String getFORM() {
		return FORM;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public String getTAXPYCD() {
		return TAXPYCD;
	}
	public String getCOUNTRY() {
		return COUNTRY;
	}
	public String getTAX() {
		return TAX;
	}
	public String getNAME() {
		return NAME;
	}
	public String getADDRESS() {
		return ADDRESS;
	}
	public String getYEARLY() {
		return YEARLY;
	}
	public String getLIVE183() {
		return LIVE183;
	}
	public String getBACKAMT() {
		return BACKAMT;
	}
	public String getTAMPAY() {
		return TAMPAY;
	}
	public String getN106RATE() {
		return N106RATE;
	}
	public String getAMTTAX() {
		return AMTTAX;
	}
	public String getNETPAY() {
		return NETPAY;
	}
	public String getW_TAX() {
		return W_TAX;
	}
	public String getC_TAX() {
		return C_TAX;
	}
	public String getACN1() {
		return ACN1;
	}
	public String getACN2() {
		return ACN2;
	}
	public String getACN3() {
		return ACN3;
	}
	public String getACN4() {
		return ACN4;
	}
	public String getACN5() {
		return ACN5;
	}
	public String getACN6() {
		return ACN6;
	}
	public String getACN7() {
		return ACN7;
	}
	public String getACN8() {
		return ACN8;
	}
	public String getACN9() {
		return ACN9;
	}
	public String getACN10() {
		return ACN10;
	}
	public String getACN11() {
		return ACN11;
	}
	public String getACN12() {
		return ACN12;
	}
	public String getTBB_OWNER() {
		return TBB_OWNER;
	}
	public String getCUSNAME() {
		return CUSNAME;
	}
	public void setRSPCOD(String rSPCOD) {
		RSPCOD = rSPCOD;
	}
	public void setUSERDATA(String uSERDATA) {
		USERDATA = uSERDATA;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public void setCURDATE(String cURDATE) {
		CURDATE = cURDATE;
	}
	public void setREC_NO(String rEC_NO) {
		REC_NO = rEC_NO;
	}
	public void setREC(LinkedList<N106_REST_RSDATA> rEC) {
		REC = rEC;
	}
	public void setBUSIDN(String bUSIDN) {
		BUSIDN = bUSIDN;
	}
	public void setCTYIDN(String cTYIDN) {
		CTYIDN = cTYIDN;
	}
	public void setTAXBAID(String tAXBAID) {
		TAXBAID = tAXBAID;
	}
	public void setSEQCLM(String sEQCLM) {
		SEQCLM = sEQCLM;
	}
	public void setFORM(String fORM) {
		FORM = fORM;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public void setTAXPYCD(String tAXPYCD) {
		TAXPYCD = tAXPYCD;
	}
	public void setCOUNTRY(String cOUNTRY) {
		COUNTRY = cOUNTRY;
	}
	public void setTAX(String tAX) {
		TAX = tAX;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public void setADDRESS(String aDDRESS) {
		ADDRESS = aDDRESS;
	}
	public void setYEARLY(String yEARLY) {
		YEARLY = yEARLY;
	}
	public void setLIVE183(String lIVE183) {
		LIVE183 = lIVE183;
	}
	public void setBACKAMT(String bACKAMT) {
		BACKAMT = bACKAMT;
	}
	public void setTAMPAY(String tAMPAY) {
		TAMPAY = tAMPAY;
	}
	public void setN106RATE(String n106rate) {
		N106RATE = n106rate;
	}
	public void setAMTTAX(String aMTTAX) {
		AMTTAX = aMTTAX;
	}
	public void setNETPAY(String nETPAY) {
		NETPAY = nETPAY;
	}
	public void setW_TAX(String w_TAX) {
		W_TAX = w_TAX;
	}
	public void setC_TAX(String c_TAX) {
		C_TAX = c_TAX;
	}
	public void setACN1(String aCN1) {
		ACN1 = aCN1;
	}
	public void setACN2(String aCN2) {
		ACN2 = aCN2;
	}
	public void setACN3(String aCN3) {
		ACN3 = aCN3;
	}
	public void setACN4(String aCN4) {
		ACN4 = aCN4;
	}
	public void setACN5(String aCN5) {
		ACN5 = aCN5;
	}
	public void setACN6(String aCN6) {
		ACN6 = aCN6;
	}
	public void setACN7(String aCN7) {
		ACN7 = aCN7;
	}
	public void setACN8(String aCN8) {
		ACN8 = aCN8;
	}
	public void setACN9(String aCN9) {
		ACN9 = aCN9;
	}
	public void setACN10(String aCN10) {
		ACN10 = aCN10;
	}
	public void setACN11(String aCN11) {
		ACN11 = aCN11;
	}
	public void setACN12(String aCN12) {
		ACN12 = aCN12;
	}
	public void setTBB_OWNER(String tBB_OWNER) {
		TBB_OWNER = tBB_OWNER;
	}
	public void setCUSNAME(String cUSNAME) {
		TBB_OWNER = cUSNAME;
	}
	public String getENDCOD() {
		return ENDCOD;
	}
	public void setENDCOD(String eNDCOD) {
		ENDCOD = eNDCOD;
	}
}
