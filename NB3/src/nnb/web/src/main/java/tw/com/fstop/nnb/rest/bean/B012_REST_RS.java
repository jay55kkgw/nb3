package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class B012_REST_RS extends BaseRestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4498237656559808395L;
	
	String TXNDIID;		//TXN Destination Institute ID
	String SNID;		//System Network Identifier
	String RECNO;		//讀取筆數
	String NEXT;		//資料起始位置
	String TXNSIID;		//TXN Source Institute ID
	String TXNIDT;		//TXN Initiate Date and Time
	String SSUP;		//System Supervisory
	String FILLER_X3;
	String SYNCCHK;		//Sync. Check Item
	String ACF;			//Address Control Field
	String CMRECNUM;	//查詢比數
	String MSGTYPE;		//Message Type
	String TIME;		//時間HHMMSS
	String SYSTRACE;	//System Trace Audit
	String LOOPINFO;
	String CUSNAME;		//姓名
	String DATE;		//日期YYYMMDD
	String BITMAPCFG;	//Bit Map Configuration
	String TOTRECNO;	//總筆數
	String CMQTIME;		//查詢時間
	String CUSIDN;		//統一編號
	String RESPCOD;		//Response Code
	String __OCCURS;
	LinkedList<B012_REST_RSDATA> REC;
	
	public LinkedList<B012_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<B012_REST_RSDATA> rEC) {
		REC = rEC;
	}
	public String getTXNDIID() {
		return TXNDIID;
	}
	public void setTXNDIID(String tXNDIID) {
		TXNDIID = tXNDIID;
	}
	public String getSNID() {
		return SNID;
	}
	public void setSNID(String sNID) {
		SNID = sNID;
	}
	public String getRECNO() {
		return RECNO;
	}
	public void setRECNO(String rECNO) {
		RECNO = rECNO;
	}
	public String getNEXT() {
		return NEXT;
	}
	public void setNEXT(String nEXT) {
		NEXT = nEXT;
	}
	public String getTXNSIID() {
		return TXNSIID;
	}
	public void setTXNSIID(String tXNSIID) {
		TXNSIID = tXNSIID;
	}
	public String getTXNIDT() {
		return TXNIDT;
	}
	public void setTXNIDT(String tXNIDT) {
		TXNIDT = tXNIDT;
	}
	public String getSSUP() {
		return SSUP;
	}
	public void setSSUP(String sSUP) {
		SSUP = sSUP;
	}
	public String getFILLER_X3() {
		return FILLER_X3;
	}
	public void setFILLER_X3(String fILLER_X3) {
		FILLER_X3 = fILLER_X3;
	}
	public String getSYNCCHK() {
		return SYNCCHK;
	}
	public void setSYNCCHK(String sYNCCHK) {
		SYNCCHK = sYNCCHK;
	}
	public String getACF() {
		return ACF;
	}
	public void setACF(String aCF) {
		ACF = aCF;
	}
	public String getCMRECNUM() {
		return CMRECNUM;
	}
	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}
	public String getMSGTYPE() {
		return MSGTYPE;
	}
	public void setMSGTYPE(String mSGTYPE) {
		MSGTYPE = mSGTYPE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getSYSTRACE() {
		return SYSTRACE;
	}
	public void setSYSTRACE(String sYSTRACE) {
		SYSTRACE = sYSTRACE;
	}
	public String getLOOPINFO() {
		return LOOPINFO;
	}
	public void setLOOPINFO(String lOOPINFO) {
		LOOPINFO = lOOPINFO;
	}
	public String getCUSNAME() {
		return CUSNAME;
	}
	public void setCUSNAME(String cUSNAME) {
		CUSNAME = cUSNAME;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getBITMAPCFG() {
		return BITMAPCFG;
	}
	public void setBITMAPCFG(String bITMAPCFG) {
		BITMAPCFG = bITMAPCFG;
	}
	public String getTOTRECNO() {
		return TOTRECNO;
	}
	public void setTOTRECNO(String tOTRECNO) {
		TOTRECNO = tOTRECNO;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getRESPCOD() {
		return RESPCOD;
	}
	public void setRESPCOD(String rESPCOD) {
		RESPCOD = rESPCOD;
	}
	public String get__OCCURS() {
		return __OCCURS;
	}
	public void set__OCCURS(String __OCCURS) {
		this.__OCCURS = __OCCURS;
	}
	
}
