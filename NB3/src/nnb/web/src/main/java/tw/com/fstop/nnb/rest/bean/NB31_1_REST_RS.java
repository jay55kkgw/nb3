package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class NB31_1_REST_RS extends BaseRestBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7681356099333359022L;
	private String CUSIDN;
	private String ACN;
	private String MSGCOD;
	private String BRHCOD;
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getMSGCOD() {
		return MSGCOD;
	}
	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}
	public String getBRHCOD() {
		return BRHCOD;
	}
	public void setBRHCOD(String bRHCOD) {
		BRHCOD = bRHCOD;
	}
}
