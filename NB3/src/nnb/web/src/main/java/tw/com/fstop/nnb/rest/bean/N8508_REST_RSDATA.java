package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N8508_REST_RSDATA implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1738169252663932594L;
	
	private String AMTFDP;
	private String FDPNO;
	private String TIME;
	private String HEADER;
	private String CRY;
	private String ACN;
	private String DATE;
	private String STATUS;
	private String DUEDAT;
	private String AMTFLG;
	private String TOPMSG;
	private String OFFSET;
	private String __OCCURS;
	private String CUSIDN;
	
	
	public String getAMTFDP() {
		return AMTFDP;
	}
	public void setAMTFDP(String aMTFDP) {
		AMTFDP = aMTFDP;
	}
	public String getFDPNO() {
		return FDPNO;
	}
	public void setFDPNO(String fDPNO) {
		FDPNO = fDPNO;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getHEADER() {
		return HEADER;
	}
	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}
	public String getCRY() {
		return CRY;
	}
	public void setCRY(String cRY) {
		CRY = cRY;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getDUEDAT() {
		return DUEDAT;
	}
	public void setDUEDAT(String dUEDAT) {
		DUEDAT = dUEDAT;
	}
	public String getAMTFLG() {
		return AMTFLG;
	}
	public void setAMTFLG(String aMTFLG) {
		AMTFLG = aMTFLG;
	}
	public String getTOPMSG() {
		return TOPMSG;
	}
	public void setTOPMSG(String tOPMSG) {
		TOPMSG = tOPMSG;
	}
	public String getOFFSET() {
		return OFFSET;
	}
	public void setOFFSET(String oFFSET) {
		OFFSET = oFFSET;
	}
	public String get__OCCURS() {
		return __OCCURS;
	}
	public void set__OCCURS(String __OCCURS) {
		this.__OCCURS = __OCCURS;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
}
