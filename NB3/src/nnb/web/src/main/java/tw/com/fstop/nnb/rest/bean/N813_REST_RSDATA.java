package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N813_REST_RSDATA extends BaseRestBean implements Serializable {


	private static final long serialVersionUID = 3588725544057128396L;
	/**
	 * 
	 */
	
	String TEXT;
	String VALUE;
	String CARDNUM;
	
	public String getTEXT() {
		return TEXT;
	}
	public void setTEXT(String tEXT) {
		TEXT = tEXT;
	}
	public String getVALUE() {
		return VALUE;
	}
	public void setVALUE(String vALUE) {
		VALUE = vALUE;
	}
	public String getCARDNUM() {
		return CARDNUM;
	}
	public void setCARDNUM(String cARDNUM) {
		CARDNUM = cARDNUM;
	}
	
}
