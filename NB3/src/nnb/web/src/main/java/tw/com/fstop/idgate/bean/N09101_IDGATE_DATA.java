package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N09101_IDGATE_DATA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -304085996413888114L;

	@SerializedName(value = "SVACN")
	private String SVACN;

	@SerializedName(value = "ACN")
	private String ACN;
	
	@SerializedName(value = "TRNGD")
	private String TRNGD;

	public String getSVACN() {
		return SVACN;
	}

	public void setSVACN(String sVACN) {
		SVACN = sVACN;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	public String getTRNGD() {
		return TRNGD;
	}

	public void setTRNGD(String tRNGD) {
		TRNGD = tRNGD;
	}
	
}
