package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class C111_REST_RSDATA implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8289261460623730439L;

	private String CDNO;// 信託號碼

	private String PAYTAG;// 扣款標的

	private String PAYAMT;// 扣款金額

	private String PAYDAY1;// 扣款日期1

	private String PAYDAY2;// 扣款日期2

	private String PAYDAY3;// 扣款日期3

	private String STOPBEGDAY;// 暫停扣款起日

	private String STOPENDDAY;// 暫停扣款迄日

	private String PAYCUR;// 扣款幣別

	private String TYPE1;// A7－定期定額扣款標的金額

	private String TYPE2;// A9－定期定額扣款日期

	private String TYPE3;// B0－定期定額終止扣款

	private String TYPE4;// B1－定期定額暫停扣款

	private String TYPE5;// B2－定期定額恢復扣款

	private String PAYDAY4;// 扣款日期4

	private String PAYDAY5;// 扣款日期5

	private String PAYDAY6;// 扣款日期6

	private String MIP;// 定期不定額註記

	private String PAYDAY7;// 扣款日期7

	private String PAYDAY8;// 扣款日期8

	private String PAYDAY9;// 扣款日期9

	private String DAMT;// 扣款方式
	
	private String SALFLG;//薪轉戶註記

	public String getPAYTAG()
	{
		return PAYTAG;
	}

	public void setPAYTAG(String pAYTAG)
	{
		PAYTAG = pAYTAG;
	}

	public String getPAYAMT()
	{
		return PAYAMT;
	}

	public void setPAYAMT(String pAYAMT)
	{
		PAYAMT = pAYAMT;
	}

	public String getPAYDAY1()
	{
		return PAYDAY1;
	}

	public void setPAYDAY1(String pAYDAY1)
	{
		PAYDAY1 = pAYDAY1;
	}

	public String getPAYDAY2()
	{
		return PAYDAY2;
	}

	public void setPAYDAY2(String pAYDAY2)
	{
		PAYDAY2 = pAYDAY2;
	}

	public String getPAYDAY3()
	{
		return PAYDAY3;
	}

	public void setPAYDAY3(String pAYDAY3)
	{
		PAYDAY3 = pAYDAY3;
	}

	public String getSTOPBEGDAY()
	{
		return STOPBEGDAY;
	}

	public void setSTOPBEGDAY(String sTOPBEGDAY)
	{
		STOPBEGDAY = sTOPBEGDAY;
	}

	public String getSTOPENDDAY()
	{
		return STOPENDDAY;
	}

	public void setSTOPENDDAY(String sTOPENDDAY)
	{
		STOPENDDAY = sTOPENDDAY;
	}

	public String getPAYCUR()
	{
		return PAYCUR;
	}

	public void setPAYCUR(String pAYCUR)
	{
		PAYCUR = pAYCUR;
	}

	public String getTYPE1()
	{
		return TYPE1;
	}

	public void setTYPE1(String tYPE1)
	{
		TYPE1 = tYPE1;
	}

	public String getTYPE2()
	{
		return TYPE2;
	}

	public void setTYPE2(String tYPE2)
	{
		TYPE2 = tYPE2;
	}

	public String getTYPE3()
	{
		return TYPE3;
	}

	public void setTYPE3(String tYPE3)
	{
		TYPE3 = tYPE3;
	}

	public String getTYPE4()
	{
		return TYPE4;
	}

	public void setTYPE4(String tYPE4)
	{
		TYPE4 = tYPE4;
	}

	public String getTYPE5()
	{
		return TYPE5;
	}

	public void setTYPE5(String tYPE5)
	{
		TYPE5 = tYPE5;
	}

	public String getPAYDAY4()
	{
		return PAYDAY4;
	}

	public void setPAYDAY4(String pAYDAY4)
	{
		PAYDAY4 = pAYDAY4;
	}

	public String getPAYDAY5()
	{
		return PAYDAY5;
	}

	public void setPAYDAY5(String pAYDAY5)
	{
		PAYDAY5 = pAYDAY5;
	}

	public String getPAYDAY6()
	{
		return PAYDAY6;
	}

	public void setPAYDAY6(String pAYDAY6)
	{
		PAYDAY6 = pAYDAY6;
	}

	public String getMIP()
	{
		return MIP;
	}

	public void setMIP(String mIP)
	{
		MIP = mIP;
	}

	public String getPAYDAY7()
	{
		return PAYDAY7;
	}

	public void setPAYDAY7(String pAYDAY7)
	{
		PAYDAY7 = pAYDAY7;
	}

	public String getPAYDAY8()
	{
		return PAYDAY8;
	}

	public void setPAYDAY8(String pAYDAY8)
	{
		PAYDAY8 = pAYDAY8;
	}

	public String getPAYDAY9()
	{
		return PAYDAY9;
	}

	public void setPAYDAY9(String pAYDAY9)
	{
		PAYDAY9 = pAYDAY9;
	}

	public String getDAMT()
	{
		return DAMT;
	}

	public void setDAMT(String dAMT)
	{
		DAMT = dAMT;
	}

	public String getCDNO() {
		return CDNO;
	}

	public void setCDNO(String cDNO) {
		CDNO = cDNO;
	}
	
	public String getSALFLG() {
		return SALFLG;
	}

	public void setSALFLG(String sALFLG) {
		SALFLG = sALFLG;
	}
	
}
