package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class NA72_REST_RQ extends BaseRestBean_TW implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1851774119923992431L;
	
	private String HLOGINPIN;//網路密碼
	private String CUSIDN;//統一編號
	private String TYPE;//類別
	private String USERID;//使用者名稱
	private String TRANSEQ;//交易序號
	private String ISSUER;//晶片卡發卡行庫
	private String ACNNO;//晶片卡帳號
	private String ICDTTM;//晶片卡日期時間
	private String ICSEQ;//晶片卡序號
	private String ICMEMO;//晶片卡備註欄
	private String TAC_Length;//TAC DATA Length
	private String TAC;//TAC DATA
	private String TAC_120space;//TAC DATA Space
	private String TRMID;//端末設備查核碼

	private String CHIP_ACN;
	private String UID;
	private String ACN;
	private String OUTACN;
	private String iSeqNo;
	private String IP;
	private String OTP;
	private String FGTXWAY;
	private String ADOPID;
	
	
	public String getHLOGINPIN() {
		return HLOGINPIN;
	}
	public void setHLOGINPIN(String hLOGINPIN) {
		HLOGINPIN = hLOGINPIN;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getUSERID() {
		return USERID;
	}
	public void setUSERID(String uSERID) {
		USERID = uSERID;
	}
	public String getTRANSEQ() {
		return TRANSEQ;
	}
	public void setTRANSEQ(String tRANSEQ) {
		TRANSEQ = tRANSEQ;
	}
	public String getISSUER() {
		return ISSUER;
	}
	public void setISSUER(String iSSUER) {
		ISSUER = iSSUER;
	}
	public String getACNNO() {
		return ACNNO;
	}
	public void setACNNO(String aCNNO) {
		ACNNO = aCNNO;
	}
	public String getICDTTM() {
		return ICDTTM;
	}
	public void setICDTTM(String iCDTTM) {
		ICDTTM = iCDTTM;
	}
	public String getICSEQ() {
		return ICSEQ;
	}
	public void setICSEQ(String iCSEQ) {
		ICSEQ = iCSEQ;
	}
	public String getICMEMO() {
		return ICMEMO;
	}
	public void setICMEMO(String iCMEMO) {
		ICMEMO = iCMEMO;
	}
	public String getTAC_Length() {
		return TAC_Length;
	}
	public void setTAC_Length(String tAC_Length) {
		TAC_Length = tAC_Length;
	}
	public String getTAC() {
		return TAC;
	}
	public void setTAC(String tAC) {
		TAC = tAC;
	}
	public String getTAC_120space() {
		return TAC_120space;
	}
	public void setTAC_120space(String tAC_120space) {
		TAC_120space = tAC_120space;
	}
	public String getTRMID() {
		return TRMID;
	}
	public void setTRMID(String tRMID) {
		TRMID = tRMID;
	}
	public String getCHIP_ACN() {
		return CHIP_ACN;
	}
	public void setCHIP_ACN(String cHIP_ACN) {
		CHIP_ACN = cHIP_ACN;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getOUTACN() {
		return OUTACN;
	}
	public void setOUTACN(String oUTACN) {
		OUTACN = oUTACN;
	}
	public String getiSeqNo() {
		return iSeqNo;
	}
	public void setiSeqNo(String iSeqNo) {
		this.iSeqNo = iSeqNo;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	public String getOTP() {
		return OTP;
	}
	public void setOTP(String oTP) {
		OTP = oTP;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
}
