package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N861_REST_RS extends BaseRestBean implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 775129110323679813L;

	private String MSGCOD;//訊息代號
	private String ACN;//帳號
	private String KIND;//存款種類
	private String ADPIBAL;//可使用餘額
	private String BDPIBAL;//帳上餘額
	private String CLR;//本交
	private String ITR;//存款利率
	private String ABAVCTX;//跨轉優惠次數
	private String ABRECTX;//跨轉優惠剩餘次數
	private String ABAVCT;//跨提優惠次數
	private String ABRECT;//跨提優惠剩餘次數
	private String MEGECT;//跨提跨轉合併優惠次數
	private String MEAVCT;//合併優惠剩餘次數
	private String CMQTIME;//CMQTIME

	public String getMSGCOD() {
		return MSGCOD;
	}

	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	public String getKIND() {
		return KIND;
	}

	public void setKIND(String kIND) {
		KIND = kIND;
	}

	public String getADPIBAL() {
		return ADPIBAL;
	}

	public void setADPIBAL(String aDPIBAL) {
		ADPIBAL = aDPIBAL;
	}

	public String getBDPIBAL() {
		return BDPIBAL;
	}

	public void setBDPIBAL(String bDPIBAL) {
		BDPIBAL = bDPIBAL;
	}

	public String getCLR() {
		return CLR;
	}

	public void setCLR(String cLR) {
		CLR = cLR;
	}

	public String getITR() {
		return ITR;
	}

	public void setITR(String iTR) {
		ITR = iTR;
	}

	public String getABAVCTX() {
		return ABAVCTX;
	}

	public void setABAVCTX(String aBAVCTX) {
		ABAVCTX = aBAVCTX;
	}

	public String getABRECTX() {
		return ABRECTX;
	}

	public void setABRECTX(String aBRECTX) {
		ABRECTX = aBRECTX;
	}

	public String getABAVCT() {
		return ABAVCT;
	}

	public void setABAVCT(String aBAVCT) {
		ABAVCT = aBAVCT;
	}

	public String getABRECT() {
		return ABRECT;
	}

	public void setABRECT(String aBRECT) {
		ABRECT = aBRECT;
	}

	public String getMEGECT() {
		return MEGECT;
	}

	public void setMEGECT(String mEGECT) {
		MEGECT = mEGECT;
	}

	public String getMEAVCT() {
		return MEAVCT;
	}

	public void setMEAVCT(String mEAVCT) {
		MEAVCT = mEAVCT;
	}

	public String getCMQTIME() {
		return CMQTIME;
	}

	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}

	
}
