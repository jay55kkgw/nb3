package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N640_REST_RSDATA extends BaseRestBean implements Serializable
{


	/**
	 * 
	 */
	private static final long serialVersionUID = -2084417650343360333L;

	private String ACN;// 帳號

	private LinkedList<N640_REST_RSDATA2> TABLE;

	public String getACN()
	{
		return ACN;
	}

	public void setACN(String aCN)
	{
		ACN = aCN;
	}

	public LinkedList<N640_REST_RSDATA2> getTABLE()
	{
		return TABLE;
	}

	public void setTABLE(LinkedList<N640_REST_RSDATA2> tABLE)
	{
		TABLE = tABLE;
	}

	

}
