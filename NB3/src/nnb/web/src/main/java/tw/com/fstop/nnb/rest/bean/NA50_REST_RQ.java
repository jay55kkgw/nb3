package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class NA50_REST_RQ extends BaseRestBean_GOLD implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7698291865307249670L;
	private String DATE;//日期YYYMMDD
	private String TIME;//時間HHMMSS
	private String PPSYNCN;//P.P.Key Sync.Check Item
	private String BNKRA;//行庫別
	private String XMLCA;//CA識別碼
	private String XMLCN;//憑證ＣＮ
	private String KIND;//交易機制 1:晶片2: 簽章
	private String UID;//統一編號
	private String CUSIDN;//統一編號
	private String TYPE;//申請作業類別
	private String ICSEQ;//晶片卡交易序號
	private String TRNSRC;//交易來源
	private String TRNTYP;//交易種類
	private String TRNCOD;//交易註記碼
	private String TRNBDT;//交易日期
	private String GDACN;//黃金存摺帳戶
	private String SVACN;//約定入扣款帳號
	private String CHIP_ACN;//晶片卡主帳號
	private String BRTHDY;//出生年月日
	private String DATECHA;//身分證補/換/新發日期
	private String TYPECHA;//補換發種類（1:初領 2:換發 3:補發）
	private String CITYCHA;//補換發縣市
	private String PIC_FLAG;//身分證有無相片
	private String ARACOD2;//連絡電話區域碼
	private String TELNUM2;//連絡電話
	private String AMT_FEE;//開戶手續費
	private String BRHNUM;//領取黃金存摺之開戶行
	private String FGTXWAY;//交易型態
	private String icSeq;//晶片金融卡
	private String accNo;//晶片金融卡
	private String CMDATE1;//出生年月日
	private String CMDATE2;//身分證補/換/新發日期
	private String pkcs7Sign;	// IKEY
	private String jsondc;		// IKEY

	private String EMPLOYER;
	private String S_MONEY;
	private String PURPOSE;
	private String RENAME;
	private String ISSUER;
	private String TRMID;
	private String TAC;
	private String CMTRANPAGE;
	private String ADOPID;
	private String braCode;
	private String chk;
	
	public String getISSUER() {
		return ISSUER;
	}
	public void setISSUER(String iSSUER) {
		ISSUER = iSSUER;
	}
	public String getTRMID() {
		return TRMID;
	}
	public void setTRMID(String tRMID) {
		TRMID = tRMID;
	}
	public String getTAC() {
		return TAC;
	}
	public void setTAC(String tAC) {
		TAC = tAC;
	}
	public String getCMTRANPAGE() {
		return CMTRANPAGE;
	}
	public void setCMTRANPAGE(String cMTRANPAGE) {
		CMTRANPAGE = cMTRANPAGE;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getBraCode() {
		return braCode;
	}
	public void setBraCode(String braCode) {
		this.braCode = braCode;
	}
	public String getChk() {
		return chk;
	}
	public void setChk(String chk) {
		this.chk = chk;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getPPSYNCN() {
		return PPSYNCN;
	}
	public void setPPSYNCN(String pPSYNCN) {
		PPSYNCN = pPSYNCN;
	}
	public String getBNKRA() {
		return BNKRA;
	}
	public void setBNKRA(String bNKRA) {
		BNKRA = bNKRA;
	}
	public String getXMLCA() {
		return XMLCA;
	}
	public void setXMLCA(String xMLCA) {
		XMLCA = xMLCA;
	}
	public String getXMLCN() {
		return XMLCN;
	}
	public void setXMLCN(String xMLCN) {
		XMLCN = xMLCN;
	}
	public String getKIND() {
		return KIND;
	}
	public void setKIND(String kIND) {
		KIND = kIND;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getICSEQ() {
		return ICSEQ;
	}
	public void setICSEQ(String iCSEQ) {
		ICSEQ = iCSEQ;
	}
	public String getTRNSRC() {
		return TRNSRC;
	}
	public void setTRNSRC(String tRNSRC) {
		TRNSRC = tRNSRC;
	}
	public String getTRNTYP() {
		return TRNTYP;
	}
	public void setTRNTYP(String tRNTYP) {
		TRNTYP = tRNTYP;
	}
	public String getTRNCOD() {
		return TRNCOD;
	}
	public void setTRNCOD(String tRNCOD) {
		TRNCOD = tRNCOD;
	}
	public String getTRNBDT() {
		return TRNBDT;
	}
	public void setTRNBDT(String tRNBDT) {
		TRNBDT = tRNBDT;
	}
	public String getGDACN() {
		return GDACN;
	}
	public void setGDACN(String gDACN) {
		GDACN = gDACN;
	}
	public String getSVACN() {
		return SVACN;
	}
	public void setSVACN(String sVACN) {
		SVACN = sVACN;
	}
	public String getCHIP_ACN() {
		return CHIP_ACN;
	}
	public void setCHIP_ACN(String cHIP_ACN) {
		CHIP_ACN = cHIP_ACN;
	}
	public String getBRTHDY() {
		return BRTHDY;
	}
	public void setBRTHDY(String bRTHDY) {
		BRTHDY = bRTHDY;
	}
	public String getDATECHA() {
		return DATECHA;
	}
	public void setDATECHA(String dATECHA) {
		DATECHA = dATECHA;
	}
	public String getTYPECHA() {
		return TYPECHA;
	}
	public void setTYPECHA(String tYPECHA) {
		TYPECHA = tYPECHA;
	}
	public String getCITYCHA() {
		return CITYCHA;
	}
	public void setCITYCHA(String cITYCHA) {
		CITYCHA = cITYCHA;
	}
	public String getPIC_FLAG() {
		return PIC_FLAG;
	}
	public void setPIC_FLAG(String pIC_FLAG) {
		PIC_FLAG = pIC_FLAG;
	}
	public String getARACOD2() {
		return ARACOD2;
	}
	public void setARACOD2(String aRACOD2) {
		ARACOD2 = aRACOD2;
	}
	public String getTELNUM2() {
		return TELNUM2;
	}
	public void setTELNUM2(String tELNUM2) {
		TELNUM2 = tELNUM2;
	}
	public String getAMT_FEE() {
		return AMT_FEE;
	}
	public void setAMT_FEE(String aMT_FEE) {
		AMT_FEE = aMT_FEE;
	}
	public String getBRHNUM() {
		return BRHNUM;
	}
	public void setBRHNUM(String bRHNUM) {
		BRHNUM = bRHNUM;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getIcSeq() {
		return icSeq;
	}
	public void setIcSeq(String icSeq) {
		this.icSeq = icSeq;
	}
	public String getAccNo() {
		return accNo;
	}
	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}
	public String getCMDATE1() {
		return CMDATE1;
	}
	public void setCMDATE1(String cMDATE1) {
		CMDATE1 = cMDATE1;
	}
	public String getCMDATE2() {
		return CMDATE2;
	}
	public void setCMDATE2(String cMDATE2) {
		CMDATE2 = cMDATE2;
	}
	public String getPkcs7Sign() {
		return pkcs7Sign;
	}
	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}
	public String getJsondc() {
		return jsondc;
	}
	public void setJsondc(String jsondc) {
		this.jsondc = jsondc;
	}
	public String getEMPLOYER() {
		return EMPLOYER;
	}
	public void setEMPLOYER(String eMPLOYER) {
		EMPLOYER = eMPLOYER;
	}
	public String getS_MONEY() {
		return S_MONEY;
	}
	public void setS_MONEY(String s_MONEY) {
		S_MONEY = s_MONEY;
	}
	public String getPURPOSE() {
		return PURPOSE;
	}
	public void setPURPOSE(String pURPOSE) {
		PURPOSE = pURPOSE;
	}
	public String getRENAME() {
		return RENAME;
	}
	public void setRENAME(String rENAME) {
		RENAME = rENAME;
	}
}
