package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N094_REST_RQ extends BaseRestBean_GOLD implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3133832046401047529L;

	private String PINNEW;		//網路銀行密碼
	private String CUSIDN;		//統一編號
	private String TRNSRC;		//交易來源
	private String TRNTYP;		//交易種類
	private String TRNCOD;		//交易註記碼
	private String TRNBDT;		//交易日期
	private String ACN;			//黃金存摺帳號
	private String CUSIDN1;		//客戶統一編號
	private String SVACN;		//台幣存款帳號
	private String KIND;		//手續費種類 01-定期定額
	private String SOURCE;		//申購/變更來源
	private String TRNAMTSIGN;	//應繳款總金額
	private String TRNAMT;		//應繳款總金額
	private String USERDATA_X50;//暫存空間區
	private String FGTXWAY;		//交易型態
	private String UID;			//統一編號
	private String ADOPID;

	private String pkcs7Sign;	// IKEY
	private String jsondc;		// IKEY
	private String ISSUER;		// IKEY
	private String ACNNO;		// IKEY
	private String TRMID;		// IKEY
	private String iSeqNo;		// IKEY
	private String ICSEQ;		// IKEY
	private String TAC;		// IKEY
	private String CMTRANPAGE;		// IKEY
	
	//IDGATE
	private String sessionID;
	private String idgateID;
	private String txnID;
	
	
	public String getPkcs7Sign() {
		return pkcs7Sign;
	}
	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}
	public String getJsondc() {
		return jsondc;
	}
	public void setJsondc(String jsondc) {
		this.jsondc = jsondc;
	}
	public String getISSUER() {
		return ISSUER;
	}
	public void setISSUER(String iSSUER) {
		ISSUER = iSSUER;
	}
	public String getACNNO() {
		return ACNNO;
	}
	public void setACNNO(String aCNNO) {
		ACNNO = aCNNO;
	}
	public String getTRMID() {
		return TRMID;
	}
	public void setTRMID(String tRMID) {
		TRMID = tRMID;
	}
	public String getiSeqNo() {
		return iSeqNo;
	}
	public void setiSeqNo(String iSeqNo) {
		this.iSeqNo = iSeqNo;
	}
	public String getICSEQ() {
		return ICSEQ;
	}
	public void setICSEQ(String iCSEQ) {
		ICSEQ = iCSEQ;
	}
	public String getTAC() {
		return TAC;
	}
	public void setTAC(String tAC) {
		TAC = tAC;
	}
	public String getCMTRANPAGE() {
		return CMTRANPAGE;
	}
	public void setCMTRANPAGE(String cMTRANPAGE) {
		CMTRANPAGE = cMTRANPAGE;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getTRNSRC() {
		return TRNSRC;
	}
	public void setTRNSRC(String tRNSRC) {
		TRNSRC = tRNSRC;
	}
	public String getTRNTYP() {
		return TRNTYP;
	}
	public void setTRNTYP(String tRNTYP) {
		TRNTYP = tRNTYP;
	}
	public String getTRNCOD() {
		return TRNCOD;
	}
	public void setTRNCOD(String tRNCOD) {
		TRNCOD = tRNCOD;
	}
	public String getTRNBDT() {
		return TRNBDT;
	}
	public void setTRNBDT(String tRNBDT) {
		TRNBDT = tRNBDT;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getCUSIDN1() {
		return CUSIDN1;
	}
	public void setCUSIDN1(String cUSIDN1) {
		CUSIDN1 = cUSIDN1;
	}
	public String getSVACN() {
		return SVACN;
	}
	public void setSVACN(String sVACN) {
		SVACN = sVACN;
	}
	public String getKIND() {
		return KIND;
	}
	public void setKIND(String kIND) {
		KIND = kIND;
	}
	public String getSOURCE() {
		return SOURCE;
	}
	public void setSOURCE(String sOURCE) {
		SOURCE = sOURCE;
	}
	public String getTRNAMTSIGN() {
		return TRNAMTSIGN;
	}
	public void setTRNAMTSIGN(String tRNAMTSIGN) {
		TRNAMTSIGN = tRNAMTSIGN;
	}
	public String getTRNAMT() {
		return TRNAMT;
	}
	public void setTRNAMT(String tRNAMT) {
		TRNAMT = tRNAMT;
	}
	public String getUSERDATA_X50() {
		return USERDATA_X50;
	}
	public void setUSERDATA_X50(String uSERDATA_X50) {
		USERDATA_X50 = uSERDATA_X50;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	public String getIdgateID() {
		return idgateID;
	}
	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}
	public String getTxnID() {
		return txnID;
	}
	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}
	
}
