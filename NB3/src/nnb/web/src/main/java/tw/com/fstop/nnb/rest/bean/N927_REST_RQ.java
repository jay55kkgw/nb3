package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N927_REST_RQ extends BaseRestBean_FUND implements Serializable{
	private static final long serialVersionUID = -1646749640736846151L;

	private String CUSIDN;

	public String getCUSIDN(){
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN){
		CUSIDN = cUSIDN;
	}
}