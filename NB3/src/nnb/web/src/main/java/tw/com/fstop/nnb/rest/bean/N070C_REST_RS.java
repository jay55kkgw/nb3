package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N070C_REST_RS extends BaseRestBean implements Serializable {	


	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8725666596842223696L;
	
	
	private	String	INTSACN;//轉入帳號
	private	String	TIME; 	//時間
	private	String	DATE; 	//日期
	private	String	AMOUNT;	//轉帳金額
	private	String	HEADER;	//銀行代號
	private	String	MAC; 
	private	String	OFFSET;	//未知
	private	String	OUTACN;	//轉出帳號
	private	String	O_AVLBAL;//轉出帳號帳戶餘額
	private	String	I_AVLBAL;//轉出帳號可用餘額
	private	String	SYNC;
	private	String	O_TOTBAL;//轉出帳號帳戶餘額
	private	String	I_TOTBAL;//轉出帳號可用餘額
	private	String	INPCST;
	private	String	TXTOKEN;
	private	String	FGTXDATE;//預約or即時
	private	String	CMMAILMEMO;//Mail備註
	private	String	FEE;//手續費
	
	public String getINTSACN() {
		return INTSACN;
	}
	public void setINTSACN(String iNTSACN) {
		INTSACN = iNTSACN;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	public String getHEADER() {
		return HEADER;
	}
	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}
	public String getMAC() {
		return MAC;
	}
	public void setMAC(String mAC) {
		MAC = mAC;
	}
	public String getOFFSET() {
		return OFFSET;
	}
	public void setOFFSET(String oFFSET) {
		OFFSET = oFFSET;
	}
	public String getOUTACN() {
		return OUTACN;
	}
	public void setOUTACN(String oUTACN) {
		OUTACN = oUTACN;
	}
	public String getO_AVLBAL() {
		return O_AVLBAL;
	}
	public void setO_AVLBAL(String o_AVLBAL) {
		O_AVLBAL = o_AVLBAL;
	}
	public String getI_AVLBAL() {
		return I_AVLBAL;
	}
	public void setI_AVLBAL(String i_AVLBAL) {
		I_AVLBAL = i_AVLBAL;
	}
	public String getSYNC() {
		return SYNC;
	}
	public void setSYNC(String sYNC) {
		SYNC = sYNC;
	}
	public String getO_TOTBAL() {
		return O_TOTBAL;
	}
	public void setO_TOTBAL(String o_TOTBAL) {
		O_TOTBAL = o_TOTBAL;
	}
	public String getI_TOTBAL() {
		return I_TOTBAL;
	}
	public void setI_TOTBAL(String i_TOTBAL) {
		I_TOTBAL = i_TOTBAL;
	}
	public String getINPCST() {
		return INPCST;
	}
	public void setINPCST(String iNPCST) {
		INPCST = iNPCST;
	}
	public String getTXTOKEN() {
		return TXTOKEN;
	}
	public void setTXTOKEN(String tXTOKEN) {
		TXTOKEN = tXTOKEN;
	}
	public String getFGTXDATE() {
		return FGTXDATE;
	}
	public void setFGTXDATE(String fGTXDATE) {
		FGTXDATE = fGTXDATE;
	}
	public String getCMMAILMEMO() {
		return CMMAILMEMO;
	}
	public void setCMMAILMEMO(String cMMAILMEMO) {
		CMMAILMEMO = cMMAILMEMO;
	}
	public String getFEE() {
		return FEE;
	}
	public void setFEE(String fEE) {
		FEE = fEE;
	}


	


}
