package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.HashMap;

public class Sendadmail_REST_RS extends BaseRestBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	private HashMap<String, Object> Result;


	public HashMap<String, Object> getResult() {
		return Result;
	}


	public void setResult(HashMap<String, Object> result) {
		Result = result;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
}
