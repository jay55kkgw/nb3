package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N1012_REST_RS extends BaseRestBean implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2527805283462428382L;

	private String ACN;// 帳號

	private String CHKTYPE;// 票據種類

	private String CHKFORM;// 票據格式

	private String CMQTIME;// 交易時間

	private String OFFSET;

	private String __OCCURS;

	private String HEADER;

	public String getACN()
	{
		return ACN;
	}

	public void setACN(String aCN)
	{
		ACN = aCN;
	}

	public String getCHKTYPE()
	{
		return CHKTYPE;
	}

	public void setCHKTYPE(String cHKTYPE)
	{
		CHKTYPE = cHKTYPE;
	}

	public String getCHKFORM()
	{
		return CHKFORM;
	}

	public void setCHKFORM(String cHKFORM)
	{
		CHKFORM = cHKFORM;
	}

	public String getCMQTIME()
	{
		return CMQTIME;
	}

	public void setCMQTIME(String cMQTIME)
	{
		CMQTIME = cMQTIME;
	}

	public String getOFFSET()
	{
		return OFFSET;
	}

	public void setOFFSET(String oFFSET)
	{
		OFFSET = oFFSET;
	}

	public String get__OCCURS()
	{
		return __OCCURS;
	}

	public void set__OCCURS(String __OCCURS)
	{
		this.__OCCURS = __OCCURS;
	}

	public String getHEADER()
	{
		return HEADER;
	}

	public void setHEADER(String hEADER)
	{
		HEADER = hEADER;
	}

}
