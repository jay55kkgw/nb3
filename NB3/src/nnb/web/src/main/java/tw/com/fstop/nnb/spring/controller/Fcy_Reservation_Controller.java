package tw.com.fstop.nnb.spring.controller;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.idgate.bean.F283_IDGATE_DATA;
import tw.com.fstop.idgate.bean.F283_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N830_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N830_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N831_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N831_IDGATE_DATA_VIEW;
import tw.com.fstop.nnb.custom.annotation.ACNVERIFY;
import tw.com.fstop.nnb.custom.annotation.ISTXNLOG;
import tw.com.fstop.nnb.service.Fcy_Reservation_Service;
import tw.com.fstop.nnb.service.IdGateService;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({ SessionUtil.CUSIDN, SessionUtil.PD, SessionUtil.PRINT_DATALISTMAP_DATA, SessionUtil.CONFIRM_LOCALE_DATA ,SessionUtil.RESULT_LOCALE_DATA,SessionUtil.DOWNLOAD_DATALISTMAP_DATA,SessionUtil.IDGATE_TRANSDATA })
@Controller
@RequestMapping(value = "/FCY/ACCT/RESERVATION")
public class Fcy_Reservation_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private Fcy_Reservation_Service fcy_reservation_service;

	@Autowired		 
	IdGateService idgateservice;
	
	
	//外幣預約結果查詢/取消 查詢頁
	@ISTXNLOG(value="F283")
	@RequestMapping(value = "/f_reservation_detail")
	public String f_reservation_detail(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("f_reservation_detail>>");
		
//		清除切換語系時暫存的資料
		SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
		SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			
			log.debug("f_reservation_detail.cusidn >>{}", cusidn);
// 			解決Trust Boundary Violation
			Map<String, String> okMap = reqParam;
			bs = fcy_reservation_service.f_reservation_detail(cusidn,okMap);
			String isProd = SpringBeanFactory.getBean("isProd");
			bs.addData("isProd",isProd);
			

            //IDGATE身分
            String idgateUserFlag="N";		 
            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
            try {		 
        	    if(IdgateData==null) {
        		    IdgateData = new HashMap<String, Object>();
        	    }
                BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
                if(tmp.getResult()) {		 
                    idgateUserFlag="Y";                  		 
                }		 
                tmp.addData("idgateUserFlag",idgateUserFlag);		 
                IdgateData.putAll((Map<String, String>) tmp.getData());
                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
            }catch(Exception e) {		 
                log.debug("idgateUserFlag error {}",e);		 
            }		 
            model.addAttribute("idgateUserFlag",idgateUserFlag);
	           
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("f_reservation_detail error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				target = "/acct_fcy/f_reservation_detail";
				
				Map<String,Object> dataMap = (Map)bs.getData();
				log.trace("dataMap={}",dataMap);
				List<Map<String,String>> rowListMap = (List<Map<String,String>>)dataMap.get("REC");
				log.trace("rowListMap={}",rowListMap);

				SessionUtil.addAttribute(model,SessionUtil.PRINT_DATALISTMAP_DATA,rowListMap);
				
				model.addAttribute("reservation_detail", bs);
			}else {
				bs.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bs);				
			}
		}
		return target;
	}
	
	//外幣預約結果查詢/取消 確認頁
	@RequestMapping(value = "/f_reservation_confirm")
	public String f_reservation_confirm(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace(ESAPIUtil.vaildLog("f_reservation_confirm>>{}"+CodeUtil.toJson(reqParam)));
		try {
			bs = new BaseResult();
			// 解決 Trust Boundary Violation
			Map<String, String> okMap = reqParam;
			// IKEY要使用的JSON:DC
			String jsondc = null;
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("f_reservation_confirm.jsondc: " + jsondc));
//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				bs = fcy_reservation_service.f_reservation_confirm(okMap);
				bs.addData("jsondc", jsondc);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);

				// IDGATE transdata            		 
	            Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);	
	    		String adopid = "F283";
	    		String title = "您有一筆外幣預約結果查詢/取消待確認";
	    		Map<String, Object> bsData = (Map<String, Object>)bs.getData();
	    		Map<String, String> result = (Map<String, String>)bsData.get("dataSet");
	        	if(IdgateData != null) {
		            model.addAttribute("idgateUserFlag",IdgateData.get("idgateUserFlag"));
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
		            	result.put("TRANTITLE", "外幣預約結果查詢/取消");
		            	result.put("TRANNAME", "取消預約");
		            	result.put("FGTXWAY", result.get("FXTXCODE"));
		            	log.trace("idgate data >> {}", result);
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(F283_IDGATE_DATA.class, F283_IDGATE_DATA_VIEW.class, result));
		                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
		            }
	        	}
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
			}
		} catch (Exception e) {			
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("f_reservation_confirm error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				target = "/acct_fcy/f_reservation_confirm";
				model.addAttribute("reservation_confirm", bs);
			}else {
				bs.setPrevious("/FCY/ACCT/RESERVATION/f_reservation_detail");
				model.addAttribute(BaseResult.ERROR, bs);				
			}
		}		
		return target;
	}
	
	//外幣預約結果查詢/取消 結果頁
	@ISTXNLOG(value="F283_1")
	@RequestMapping(value = "/f_reservation_result")
	public String f_reservation_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		BaseResult bsMsgCode = null;
		log.trace("f_reservation_result>>");
		try {
			bs = new BaseResult();
			bsMsgCode = new BaseResult();

			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			log.debug("f_reservation_result_cusidn >>{}", cusidn);
			//解決長度過長問題
			reqParam.remove("jsondc");
			reqParam.remove("pkcs7Sign");
			// 解決 Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			log.trace(ESAPIUtil.vaildLog("f_reservation_result_okMap >>{}"+ CodeUtil.toJson(okMap)));

//			判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale) 
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( ! bs.getResult())
				{
					log.trace("bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			if( ! hasLocale){
				bs.reset();
				//判斷是SSL/晶片金融卡 or i-key
				String fxtxcode = okMap.get("FXTXCODE");				
				if("1".equals(fxtxcode)) {
					bs = fcy_reservation_service.f_reservation_result(cusidn,okMap);
				}else if("2".equals(fxtxcode)){					
					bs = fcy_reservation_service.f_reservation_result(cusidn,okMap);				
				}else if("7".equals(fxtxcode)){
	                //IDgate驗證		 
	                try {
        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("F283_IDGATE");
                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));
                        Map<String, Object> idgateMap = new HashMap<String, Object>();
                        idgateMap.put("IN_DATAS", okMap);
                        Boolean checkIdGate = idgateservice.doIDGateCommand(idgateMap, "ms_fx");
                        if(checkIdGate) {
                        	bs = fcy_reservation_service.f_reservation_result(cusidn,okMap);
                        }else {
    						bs.setErrorMessage("FE0011", fcy_reservation_service.getMessageByMsgCode("FE0011"));
    						bs.setResult(false);
    						log.trace(bs.getMsgCode());
    					}
	                }catch(Exception e) {		 
	                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
	                }  
				}else{
					bsMsgCode = fcy_reservation_service.N951_REST(cusidn,okMap);										
					if(bsMsgCode.getMsgCode().equals("0")) {
						bs = fcy_reservation_service.f_reservation_result(cusidn,okMap);				
					}else {
						bs.setErrorMessage(bsMsgCode.getMsgCode(), bsMsgCode.getMessage());
						bs.setResult(false);
						log.trace(bs.getMsgCode());
					}
				}
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {			
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("f_reservation_result error >> {}",e);
		}finally {
			if(bs!=null && bs.getResult()) {
				target = "/acct_fcy/f_reservation_result";
				model.addAttribute("reservation_result", bs);
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
				bs.setPrevious("/FCY/ACCT/RESERVATION/f_reservation_detail");
			}
		}			
		return target;
	}
	
	//TODO 5/8因為拆分，外幣轉出紀錄查詢相關內容需要重新調整
	
		// F3000-轉出記錄查詢
		@RequestMapping(value = "/f_transfer_record_query")
		public String f_transfer_record_query(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			String target = "/error";
			BaseResult bs = null;
			log.trace("f_transfer_record_query start~~~");
			try {
				// 清除切換語系時暫存的資料
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
				bs = new BaseResult();
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
				log.debug("cusidn: {} ", cusidn);
				bs = fcy_reservation_service.f_transfer_record_query(cusidn);
				bs.addData("TODAY", DateUtil.getCurentDateTime("yyyy/MM/dd"));
				String isProd = SpringBeanFactory.getBean("isProd");
				bs.addData("isProd",isProd);
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("f_transfer_record_query error >> {}",e);
			} finally {
				if (bs != null && bs.getResult()) {
					target = "/acct_fcy/f_transfer_record_query";
					log.debug("f_transfer_record_query bs getData {}",bs.getData());
					model.addAttribute("f_transfer_record_query", bs);
				} else {
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}
			return target;
		}
		
		// F3000-轉出記錄查詢結果頁
		@ISTXNLOG(value="F3000")
		@ACNVERIFY(acn="ACN")
		@RequestMapping(value = "/f_transfer_record_query_result")
		public String f_transfer_record_query_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
			String target = "/error";
			BaseResult bs = null;
			log.trace("f_transfer_record_query_result start~~~");
			try {
				bs = new BaseResult();
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
				Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
				log.debug(ESAPIUtil.vaildLog("reqParam{}"+CodeUtil.toJson(reqParam)));
				log.debug(ESAPIUtil.vaildLog("cusidn" + cusidn));
				// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
				//判斷查詢區間是否在限定範圍
				boolean ckDate = DateUtil.checkDate(okMap.get("CMSDATE"), okMap.get("CMEDATE"), 6);
				if(ckDate) {
					if (hasLocale) {
						bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
						if (!bs.getResult()) {
							log.trace("bs.getResult() >>{}", bs.getResult());
							throw new Exception();
						}
					}
					if (!hasLocale) {
						bs.reset();
						bs = fcy_reservation_service.f_transfer_record_query_result(cusidn, reqParam);
						SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
					}
				}
				else {
					bs.setResult(false);
				}
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("f_transfer_record_query_result error >> {}",e);
			} finally {
				if (bs != null && bs.getResult()) {
					target = "/acct_fcy/f_transfer_record_query_result";
					log.debug("bs_get_data={}", bs.getData());
					Map<String, Object> datamap=(Map<String, Object>) bs.getData();
//					List<Map<String, String>> dataListMap = ((List<Map<String, String>>) (datamap.get("REC")));
					List<Map<String, String>> dataListMap= (List<Map<String, String>>) datamap.get("DownLoadREC");
					log.debug("dataListMap={}", dataListMap);
					SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, dataListMap);
					SessionUtil.addAttribute(model, SessionUtil.PRINT_DATALISTMAP_DATA, bs.getData());
					model.addAttribute("f_transfer_record_query_result", bs);
				} 
				else {
					//新增回上一頁的路徑
					bs.setPrevious("/FCY/ACCT/RESERVATION/f_transfer_record_query");
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}
			return target;
		}
		
		// F3000-轉出記錄查詢-mail重送
			@RequestMapping(value = "/f_transfer_record_query_result_mail")
			public @ResponseBody BaseResult transfer_record_query_result_mail(HttpServletRequest request, HttpServletResponse response,
					@RequestParam Map<String, String> reqParam, Model model) {
				BaseResult bs = null;
				Map<String,Object> datamap=null;
				log.trace("f_transfer_record_query mail start~~~");
				try {
					log.trace(ESAPIUtil.vaildLog("REQPARAM >>{}"+CodeUtil.toJson(reqParam)));
					String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
					bs = fcy_reservation_service.transfer_record_query_mail(cusidn,reqParam);
				} catch (Exception e) {
					//Avoid Information Exposure Through an Error Message
					//e.printStackTrace();
					log.error("transfer_record_query_result_mail error >> {}",e);
				} finally {
				}
				return bs;
			}
			// F3000-轉出記錄查詢-交易單據顯示
			@RequestMapping(value = "/f_transfer_record_query_FXCERT")
			public String f_transfer_record_query_FXCERT(HttpServletRequest request, HttpServletResponse response,
					@RequestParam Map<String, String> reqParam, Model model) {
				log.trace("f_transfer_record_query_FXCERT start~~~");
				String target="/error";
				BaseResult bs = null;
				String result =""; 
				try {
					 target = "/acct_fcy/f_transfer_record_query_FXCERT";
					// 清除切換語系時暫存的資料
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
					bs = new BaseResult();
					Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
					boolean hasLocale = okMap.containsKey("locale");
					if (hasLocale) {
						result = (String) SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null);
						}
					if (!hasLocale) {
						bs.reset();
						//use "ADTXNO" to get "FXCERT" from DB
						bs = fcy_reservation_service.transfer_record_query_CERT(reqParam.get("content"));
						SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
					}
				} catch (Exception e) {
					//Avoid Information Exposure Through an Error Message
					//e.printStackTrace();
					log.error("f_transfer_record_query_FXCERT error >> {}",e);
				} finally {
					model.addAttribute("transfer_record_query_FXCERT",bs);
					target="/printTemplate/f_transfer_record_fxcert_print";
				}
				return target;
			}
//			F3000-轉出記錄查詢直接下載
			@RequestMapping(value = "/f_transfer_record_query_DirectDownload")
			public String transfer_record_query_DirectDownload(HttpServletRequest request,HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model){
				String target = "/error";
				BaseResult bs = null;
				log.trace("transfer_record_query_ajaxDirectDownload");

				try{
					bs = new BaseResult();
					String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
					log.trace("cusidn={}",cusidn);
					Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
					bs = fcy_reservation_service.f_transfer_record_query_directDownload(cusidn, okMap);
				}
				catch(Exception e){
					//Avoid Information Exposure Through an Error Message
					//e.printStackTrace();
					log.error("transfer_record_query_DirectDownload error >> {}",e);
				}
				finally{
					if(bs != null && bs.getResult()){
						target = "forward:/directDownload";
						
						Map<String,Object> dataMap = (Map)bs.getData();
						log.trace("dataMap={}",dataMap);
						List<Map<String,String>> rowListMap = (List<Map<String,String>>)dataMap.get("DownLoadREC");
						log.trace("rowListMap={}",rowListMap);
						
						SessionUtil.addAttribute(model,SessionUtil.DOWNLOAD_DATALISTMAP_DATA,rowListMap);
						
						Map<String,Object> parameterMap = (Map<String,Object>)dataMap.get("parameterMap");
						request.setAttribute("parameterMap",parameterMap);
					}
					else{
						bs.setPrevious("/FCY/ACCT/RESERVATION/f_transfer_record_query");
						model.addAttribute(BaseResult.ERROR,bs);
					}
				}
				return target;
			}		
			
//			預約交易結果查詢輸入頁
			@RequestMapping(value = "/f_reservation_trans")
			public String f_reservation_trans(HttpServletRequest request, HttpServletResponse response,
					@RequestParam Map<String, String> reqParam, Model model) {
				log.trace("f_reservation_trans");
				String target = "/error";
				BaseResult bs = null;
				
//				清除切換語系時暫存的資料
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
				
				try {
					bs = new BaseResult();
					bs.addData("TODAY", DateUtil.getCurentDateTime("yyyy/MM/dd"));
					bs.setResult(Boolean.TRUE);
				} catch (Exception e) {
					//Avoid Information Exposure Through an Error Message
					//e.printStackTrace();
					log.error("f_reservation_trans error >> {}",e);
				}finally {
					if(bs!=null && bs.getResult()) {
						target = "/acct_fcy/f_reservation_trans";
						model.addAttribute("f_reservation_trans", bs);
					}else {
						model.addAttribute(BaseResult.ERROR, bs);				
					}
				}
				return target;
			}
			
			// 預約交易結果查詢結果頁
			@ISTXNLOG(value="F790")
			@RequestMapping(value = "/f_reservation_trans_result")
			public String reservation_trans_result(HttpServletRequest request, HttpServletResponse response,
					@RequestParam Map<String, String> reqParam, Model model) {
				String target = "/error";
				BaseResult bs = null;
				log.trace(ESAPIUtil.vaildLog("f_reservation_trans_result>>{}"+CodeUtil.toJson(reqParam)));
				try {
					bs = new BaseResult();
					String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
//		 			解決Trust Boundary Violation
					Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//					判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
					boolean hasLocale = okMap.containsKey("locale");
					//判斷查詢區間是否在限定範圍
					boolean ckDate = DateUtil.checkDate(okMap.get("CMSDATE"), okMap.get("CMEDATE"), 6);
					if(ckDate) {
						if(hasLocale){
							bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
							if( ! bs.getResult())
							{
								log.trace("bs.getResult() >>{}",bs.getResult());
								throw new Exception();
							}
						}
						if( ! hasLocale){
							bs.reset();			
							log.trace("cusidn>>{}", cusidn);
							bs = fcy_reservation_service.f_reservation_trans_result(cusidn, okMap);
							SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
							log.trace("bs.getData()>>{}",bs.getData());
						}
					}
					else {
						bs.setResult(false);
					}
				} catch (Exception e) {
					//Avoid Information Exposure Through an Error Message
					//e.printStackTrace();
					log.error("reservation_trans_result error >> {}",e);
				}finally {
					if(bs!=null && bs.getResult()) {
						target = "/acct_fcy/f_reservation_trans_result";
						
						Map<String,Object> dataMap = (Map)bs.getData();
						log.trace("dataMap={}",dataMap);
						List<Map<String,String>> rowListMap = (List<Map<String,String>>)dataMap.get("REC");
						log.trace("rowListMap={}",rowListMap);

						SessionUtil.addAttribute(model,SessionUtil.PRINT_DATALISTMAP_DATA,rowListMap);
						model.addAttribute("f_reservation_trans_result", bs);
						
						for(Map<String,String>rowlist:rowListMap) {
							//轉帳結果翻譯  成功、未執行、失敗、處理中
							if(rowlist.get("FXTXSTATUS").equals("LB.D1099")) {
								rowlist.put("FXTXSTATUS_D", I18n.getMessage("LB.D1099"));
							}
							else if(rowlist.get("FXTXSTATUS").equals("LB.X0048")) {
								rowlist.put("FXTXSTATUS_D", I18n.getMessage("LB.X0048"));
							}
							else if(rowlist.get("FXTXSTATUS").equals("LB.W0056")) {
								rowlist.put("FXTXSTATUS_D", I18n.getMessage("LB.W0056"));
							}
							else {
								rowlist.put("FXTXSTATUS_D", I18n.getMessage("LB.W0057"));
							}
						}
						SessionUtil.addAttribute(model,SessionUtil.DOWNLOAD_DATALISTMAP_DATA,rowListMap);
					}else {
						bs.setPrevious("/FCY/ACCT/RESERVATION/f_reservation_trans");
						model.addAttribute(BaseResult.ERROR, bs);				
					}
				}
				return target;
			}
			
			//預約交易結果直接下載
			@RequestMapping(value = "/f_reservation_trans_directDownload")
			public String f_reservation_trans_directDownload(HttpServletRequest request,HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model){
				String target = "/error";
				BaseResult bs = null;
				log.trace("f_reservation_trans_directDownload");
				//序列化傳入值
				Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
				log.debug(ESAPIUtil.vaildLog("okMap={}" + CodeUtil.toJson(okMap)));
				try{
					bs = new BaseResult();
					String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
					log.trace(ESAPIUtil.vaildLog("cusidn={}"+cusidn));
					for(Entry<String,String> entry : okMap.entrySet()){
						okMap.put(entry.getKey(),URLDecoder.decode(entry.getValue(),"UTF-8"));
					}
					bs = fcy_reservation_service.f_reservation_trans_result(cusidn,okMap);
				}
				catch(Exception e){
					//Avoid Information Exposure Through an Error Message
					//e.printStackTrace();
					log.error("f_reservation_trans_directDownload error >> {}",e);
				}
				finally{
					if(bs != null && bs.getResult()){
						target = "forward:/directDownload";
						
						Map<String,Object> dataMap = (Map)bs.getData();
						log.trace("dataMap={}",dataMap);
						List<Map<String,String>> rowListMap = (List<Map<String,String>>)dataMap.get("REC");
						log.trace("rowListMap={}",rowListMap);
						
						for(Map<String,String>rowlist:rowListMap) {
							//轉帳結果翻譯  成功、未執行、失敗、處理中
							if(rowlist.get("FXTXSTATUS").equals("LB.D1099")) {
								rowlist.put("FXTXSTATUS_D", I18n.getMessage("LB.D1099"));
							}
							else if(rowlist.get("FXTXSTATUS").equals("LB.X0048")) {
								rowlist.put("FXTXSTATUS_D", I18n.getMessage("LB.X0048"));
							}
							else if(rowlist.get("FXTXSTATUS").equals("LB.W0056")) {
								rowlist.put("FXTXSTATUS_D", I18n.getMessage("LB.W0056"));
							}
							else {
								rowlist.put("FXTXSTATUS_D", I18n.getMessage("LB.W0057"));
							}
						}
						SessionUtil.addAttribute(model,SessionUtil.DOWNLOAD_DATALISTMAP_DATA,rowListMap);
						
						Map<String,Object> parameterMap = new HashMap<String,Object>();
						
						parameterMap.put("CMQTIME", dataMap.get("CMQTIME"));
						parameterMap.put("COUNT", dataMap.get("COUNT"));
						parameterMap.put("cmedate", dataMap.get("cmedate"));
						parameterMap.put("FGTXSTATUS", dataMap.get("FGTXSTATUS_D"));
						
						parameterMap.put("downloadFileName",okMap.get("downloadFileName"));
						parameterMap.put("downloadType",okMap.get("downloadType"));
						parameterMap.put("templatePath",okMap.get("templatePath"));
						
						String downloadType = okMap.get("downloadType");
						log.trace(ESAPIUtil.vaildLog("downloadType={}"+downloadType));
						
						//EXCEL和OLDEXCEL下載
						if("EXCEL".equals(downloadType) || "OLDEXCEL".equals(downloadType)){
							parameterMap.put("headerRightEnd",okMap.get("headerRightEnd"));
							parameterMap.put("headerBottomEnd",okMap.get("headerBottomEnd"));
							parameterMap.put("rowStartIndex",okMap.get("rowStartIndex"));
							parameterMap.put("rowRightEnd",okMap.get("rowRightEnd"));
						}
						//TXT下載
						else{
							parameterMap.put("txtHeaderBottomEnd",okMap.get("txtHeaderBottomEnd"));
							parameterMap.put("txtHasRowData",okMap.get("txtHasRowData"));
						}
						request.setAttribute("parameterMap",parameterMap);
					}
					else{
						bs.setPrevious("/FCY/ACCT/RESERVATION/f_reservation_trans");
						model.addAttribute(BaseResult.ERROR, bs);
					}
				}
				return target;
			}

	}
