package tw.com.fstop.nnb.service;

import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import org.apache.commons.beanutils.BeanUtils;
import org.hsqldb.lib.StringUtil;
import org.joda.time.DateTime;
import org.joda.time.base.AbstractDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.google.gson.Gson;

import fstop.orm.po.ONLINE_APPOINTMENT_TXNLOG;
import fstop.orm.po.ONLINE_APPOINTMENT_TXNLOG_PK;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.comm.bean.IBPDParamBackData;
import tw.com.fstop.nnb.rest.bean.AML_REST_RQ;
import tw.com.fstop.tbb.nnb.dao.OnlineAppointmentTxnlogDao;
import tw.com.fstop.tbb.nnb.dao.TxnSmsLogDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtil;

@Service
public class Ebill_Apply_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private DaoService daoservice;
	@Autowired
	private Acct_Transfer_Service outaccount_Service;
	@Autowired
	private ServletContext context;
	@Autowired
	private TxnSmsLogDao txnSmsLogDao;
	@Autowired
	private OnlineAppointmentTxnlogDao onlineAppointmentTxnlogDao;
	
	private String IBPD_DEMO;
	
	public Boolean checkMac(Map<String, String> reqParam) {
		Boolean result = false;
		try {
			Gson gs = new Gson();
			Map<String, Object> param = gs.fromJson(reqParam.get("IBPD_Param"), Map.class);
			
			LinkedHashMap IBPD_Param = new LinkedHashMap();
			IBPD_Param.put("SrcID", param.get("SrcID"));
			IBPD_Param.put("KeyID", param.get("KeyID"));			
			IBPD_Param.put("TxnDatetime", param.get("TxnDatetime"));
			IBPD_Param.put("STAN", param.get("STAN"));
			IBPD_Param.put("BillerBan", param.get("BillerBan"));
			
			IBPD_Param.put("BusType", param.get("BusType"));
			IBPD_Param.put("BillerID", param.get("BillerID"));
			IBPD_Param.put("PaymentType", param.get("PaymentType"));
			IBPD_Param.put("FeeID", param.get("FeeID"));
			IBPD_Param.put("CustIdnBan", param.get("CustIdnBan"));
			
			IBPD_Param.put("CustBillerAcnt", param.get("CustBillerAcnt"));
			IBPD_Param.put("CustBankAcnt", param.get("CustBankAcnt"));

			log.debug(ESAPIUtil.vaildLog("IBPD_Param >> " + CodeUtil.toJson(IBPD_Param)));
			param.put("IBPD_Param", CodeUtil.toJson(IBPD_Param));
			log.debug(ESAPIUtil.vaildLog("checkMac_Param >> " + CodeUtil.toJson(param)));
			BaseResult bs = N855checkmac_REST(param);
			if(bs.getResult()) {
				result = true;
			}
		}catch(Exception e) {
			log.error("checkMac error >>{}", e);
		}
		return result;
	}
	
	/**
	 * 網銀線上約定作業平台 進入頁
	 * 
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult accountBinding(Map<String, String> reqParam,Model model) {
		BaseResult bs = new BaseResult();
		BaseResult tmp = new BaseResult();
		try {
			/*
			 * SrcID          參加單位代號，由本平台配發
			 * KeyID          機碼代號，左靠右補空白
			 * DivData        產製SessionKey過程中生成的隨機亂數值，以HEX格式表示
			 * ICV            產製MAC過程中生成的隨機亂數值，以HEX格式表示
			 * MAC            訊息押碼，以HEX格式表示
			 * TxnDatetime    交易日期時間(YYYYMMDDhhmmss，採西元年) ，使用對應的「事業機構線上約定請求」訊息內的交易日期時間內容。
			 * STAN           平台交易序號，由平台產生，當日交易序號不可重複
			 * BillerBan      委託單位統一編號
			 * BusType        業務類別(02為線上約定繳費)
			 * BillerID       委託單位代號
			 * PaymentType    繳費類別  
			 * FeeID          費用代號    
			 * CustIdnBan     約定連結申請人身分證號或營利事業統一編號左靠右補空白 
			 * CustBillerAcnt 約定連結申請人於事業機構開立之帳號左靠右補空白
			 * CustBankAcnt   全國性繳費交易時之扣款帳號。右靠左補零
			 * */
			Gson gs = new Gson();
			Map<String, String> param = gs.fromJson(reqParam.get("IBPD_Param"), Map.class);
			writingLog(param);
			
			/*解封包
			 * CustIdnBan_Dec 
			 * CustBillerAcnt_Dec 
			 * CustBankAcnt_Dec 
			 */
			tmp=N855decrypt_REST(param);

			if(tmp.getResult()) {
				bs.setResult(true);
				bs.addAllData(param);
				log.trace("PARAM BSDATA >> {}", CodeUtil.toJson(bs));
				bs.addAllData((Map<String, String>) tmp.getData());
				log.trace("ALLDATA BSDATA >> {}", CodeUtil.toJson(bs));				
			}else {
				bs.setResult(false);
				bs.addAllData(param);
				bs.setMsgCode(tmp.getMsgCode());
				if(StrUtil.isNotEmpty(tmp.getMessage()))bs.setMsgCode(tmp.getMessage());
			}
			//原封包
//			IBPDParamBackData backdata= CodeUtil.objectCovert(IBPDParamBackData.class, param);
//			backdata.setRCode("9998");
//			log.debug("IBPD_Param_Backdata >>"+CodeUtil.toJson(backdata));
//			bs.addData("IBPD_Param",CodeUtil.toJson(backdata));
			//返回資料
			try {
				BaseResult backdata = new BaseResult();
				Map<String,String>bsdata=(Map<String, String>) bs.getData();
				bsdata.put("RCode", "9902");
				backdata=N855canceldata_REST(bsdata);
				bs.addData("IBPD_Param",((Map<String,String>) backdata.getData()).get("IBPD_Param"));
				log.trace("backdata >>"+CodeUtil.toJson(bs.getData()));
			}catch(Exception e) {
				log.error("Backdata error!");
			}
			//平台URL
			String BackUrl="https://"+SpringBeanFactory.getBean("IBPD_HOST")+"/contract/BankToIBPD";
			bs.addData("BackUrl",BackUrl);
			//放Session
			SessionUtil.addAttribute(model, SessionUtil.EBILLAPPLY_DATA, bs);
			log.trace("accountBinding BSDATA >> {}", CodeUtil.toJson(bs));
		} catch (Exception e) {
			log.error("accountBinding  error : {}", e);
		}
		return bs;
	}
	/**
	 * 網銀線上約定作業平台 進入頁
	 * 
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult accountBinding_step2(Map<String, String> reqParam,Model model,BaseResult bs) {
		BaseResult tmp = new BaseResult();
		try {
			
			String CUSIDN=((Map<String, String>) bs.getData()).get("CustIdnBan_Dec");
			String PAYACCOUNT=((Map<String, String>) bs.getData()).get("CustBankAcnt_Dec");
			String BILLACCOUNT=((Map<String, String>) bs.getData()).get("CustBillerAcnt_Dec");
			CUSIDN = CUSIDN==null ? "": CUSIDN;
			PAYACCOUNT = PAYACCOUNT==null ? "": PAYACCOUNT;
			BILLACCOUNT = BILLACCOUNT==null ? "": BILLACCOUNT;
			CUSIDN=CUSIDN.replace(" ","");
			BILLACCOUNT=BILLACCOUNT.replace(" ","");
			if(!StringUtil.isEmpty(PAYACCOUNT))PAYACCOUNT=PAYACCOUNT.replaceFirst("^0*","");
			
			((Map<String, String>) bs.getData()).put("CUSIDN_sh",CUSIDN.substring(0,4)+"***"+CUSIDN.substring(7));
			((Map<String, String>) bs.getData()).put("CUSIDN",CUSIDN);
			((Map<String, String>) bs.getData()).put("BILLACCOUNT",BILLACCOUNT);
			((Map<String, String>) bs.getData()).put("PAYACCOUNT",PAYACCOUNT);
			bs.setResult(true);
			//放Session
			SessionUtil.addAttribute(model, SessionUtil.EBILLAPPLY_DATA, bs);
//			SessionUtil.addAttribute(model, SessionUtil.CUSIDN,
//					Base64.getEncoder().encodeToString(okMap.get("cusidn").toUpperCase().getBytes()) );			
			SessionUtil.addAttribute(model, SessionUtil.CUSIDN_EBILLAPPLY, CUSIDN);
			
			//返回資料
			try {
				BaseResult backdata = new BaseResult();
				Map<String,String>bsdata=(Map<String, String>) bs.getData();
				bsdata.put("RCode", "9902");
				backdata=N855canceldata_REST(bsdata);
				bs.addData("IBPD_Param",((Map<String,String>) backdata.getData()).get("IBPD_Param"));
				log.trace("backdata >>"+CodeUtil.toJson(bs.getData()));
			}catch(Exception e) {
				log.error("Backdata error!");
			}
			log.trace("accountBinding S2 BSDATA >> {}", CodeUtil.toJson(bs));
		} catch (Exception e) {
			log.error("accountBinding  error : {}", e);
		}
		return bs;
	}
	/**
	 * 網銀線上約定作業平台 進入頁
	 * 
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult accountBinding_step3(Map<String, String> reqParam,Model model,BaseResult bs) {
		BaseResult tmp = new BaseResult();
		Boolean mobtelFlag=true;
		Boolean mailaddrFlag=true;
		try {
			Map<String, String> dataMap=(Map<String, String>) bs.getData();
			
			//取得約定轉出帳號做比對
			tmp=getOutACNO_List(dataMap.get("CUSIDN"));
			log.trace("OUTACC BSDATA >> {}", CodeUtil.toJson(tmp));
			
			
			//TODO ErrorHandle
			((Map<String, Object>) bs.getData()).put("ACCLIST",(Object) tmp.getData());
			Map<String,String>accdata=(Map<String, String>) tmp.getData();
			//判斷下拉是否為空	
			log.trace("ACC SIZE >>"+accdata.size());
			if(accdata.size()<=0) {
				bs.setResult(false);
				bs.setMessage("您的帳戶無可約定的帳戶。");
				log.trace("ERR DATA >>"+CodeUtil.toJson(bs.getData()));
				return bs;
			}
			//判斷帳號在不在下拉資料裡面
			Map<String,String>accbsdata=(Map<String,String>)bs.getData();
			String ObjectAcc=accbsdata.get("CustBankAcnt_Dec");
			log.trace("EbillAccList >>"+CodeUtil.toJson(accdata));
			log.trace("objectAcc >>"+ObjectAcc);
			if(!StringUtil.isEmpty(ObjectAcc)) {
				if(!accdata.keySet().contains(ObjectAcc.substring(5))) {//不在下拉選單
					bs.setResult(false);
					bs.setMsgCode("2999");
					bs.setMessage("2999 : 其他類檢核錯誤。");
					log.error("EbillApplyErrorCode : "+dataMap.get("CUSIDN")+" : 事業機構約定帳號 "+ObjectAcc+" 非可約定帳號");
					log.trace("ERR DATA >>"+CodeUtil.toJson(bs.getData()));
					return bs;
				}
			}
			
			
			String MOBTEL=(String) SessionUtil.getAttribute(model, SessionUtil.MOBTEL, null);
			String MAILADDR=(String) SessionUtil.getAttribute(model, SessionUtil.DPMYEMAIL, null);
			
			//本機測試用start
			IBPD_DEMO=SpringBeanFactory.getBean("IBPD_DEMO")!=null?SpringBeanFactory.getBean("IBPD_DEMO"):"N";
			if(IBPD_DEMO.equals("Y")) {
				MOBTEL="0900777777";
				MAILADDR="TEST@gmail.com";
			}
			//開發測試用mail=H15D302B@mail.tbb.com.tw
			//本機測試用end
			
			//製作顯適用手機email
			if(!StringUtil.isEmpty(MOBTEL)) {
				dataMap.put("MOBTEL_sh",MOBTEL.substring(0,4)+"-***-"+MOBTEL.substring(7));
				dataMap.put("MOBTEL", MOBTEL);
			}else mobtelFlag=false;
			if(!StringUtil.isEmpty(MAILADDR)) {
				dataMap.put("MAILADDR_sh",MAILADDR.substring(0,4)+"****"+MAILADDR.substring(7));
				dataMap.put("MAILADDR", MAILADDR);
			}else mailaddrFlag=false;	
			SessionUtil.addAttribute(model, SessionUtil.EBILLAPPLY_DATA, bs);
			if(!mailaddrFlag||!mobtelFlag) {
				bs.setResult(false);
				if(!mailaddrFlag&&!mobtelFlag)bs.setMessage("您尚未在本行留存Email及手機號碼，因此無法繼續約定。");
				else if(!mobtelFlag)bs.setMessage("您尚未在本行留存手機號碼，因此無法繼續約定。");
				else if(!mailaddrFlag)bs.setMessage("您尚未在本行留存Email，因此無法繼續約定。");
				log.trace("ERR DATA >>"+CodeUtil.toJson(bs.getData()));
				log.error("EbillApplyErrorCode : "+dataMap.get("CUSIDN")+" : "+bs.getMessage());
				return bs;
			}
			String cusidnajax =Base64.getEncoder().encodeToString(dataMap.get("CUSIDN").toUpperCase().getBytes());
			dataMap.put("CUSIDNAJAX",cusidnajax);
			//TODO:驗證碼次數是不是達上限
				//次數查詢API 需要ADOPID
			Date d = new Date();
			Integer findcount = txnSmsLogDao.findTotalCountByInput(dataMap.get("CUSIDN"), DateUtil.format("yyyyMMdd", d),"N855");
			log.trace("FindCount >> "+findcount);
			if((3-findcount)>0) {
				dataMap.put("SMStimes",Integer.toString(3-findcount));
			}else {
				bs.setResult(false);
				bs.setMessage("您的手機號碼已達單日簡訊發送上限，因此無法繼續約定帳戶，請明日再試。");
				log.error("EbillApplyErrorCode : "+dataMap.get("CUSIDN")+" : "+bs.getMessage());
				log.trace("ERR DATA >>"+CodeUtil.toJson(bs.getData()));
				return bs;
			}
			
			//返回資料
			try {
				BaseResult backdata = new BaseResult();
				Map<String,String>bsdata=(Map<String, String>) bs.getData();
				bsdata.put("RCode", "9902");
				backdata=N855canceldata_REST(bsdata);
				bs.addData("IBPD_Param",((Map<String,String>) backdata.getData()).get("IBPD_Param"));
				log.trace("backdata >>"+CodeUtil.toJson(bs.getData()));
			}catch(Exception e) {
				log.error("Backdata error!");
			}
			bs.setResult(true);
			log.trace("accountBinding S3 BSDATA >> {}", CodeUtil.toJson(bs));
		} catch (Exception e) {
			log.error("accountBinding_step3  error : {}", e);
		}
		return bs;
	}
	/**
	 * 網銀線上約定作業平台 結果頁
	 * 
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult accountBinding_otpvf(String CUSIDN,String OTP) {
		BaseResult bs = new BaseResult();
		try {

			Map<String, String>tmpreqParam = new HashMap<String,String>();
			tmpreqParam.put("CUSIDN", CUSIDN);
			tmpreqParam.put("FuncType", "1");
			tmpreqParam.put("OTP", OTP);
			tmpreqParam.put("ADOPID", "N855");
			
			bs=SmsOtp_REST(tmpreqParam);
			
			//本機測試用start
			IBPD_DEMO=SpringBeanFactory.getBean("IBPD_DEMO")!=null?SpringBeanFactory.getBean("IBPD_DEMO"):"N";
			log.trace("IBPD_DEMO >>"+IBPD_DEMO);
			if(IBPD_DEMO.equals("Y")) {
				bs.setResult(Boolean.TRUE);
			}
			//本機測試用end
			
			log.trace("accountBinding_otpvf bs.getData()>> {}",bs);
		} catch (Exception e) {
			log.error("accountBinding_otpvf  error : {}", e);
		}
		return bs;
	}
	/**
	 * 網銀線上約定作業平台 進入頁
	 * 
	 * 
	 * @param reqParam
	 * @return
	 */
	public BaseResult accountBinding_result(Map<String, String> reqParam) {
		BaseResult bs = new BaseResult();
		try {
			bs=N855_REST(reqParam);
			List<Map<String,String>>maildata=new ArrayList();
			Map<String,String>dataMap=(Map<String, String>) bs.getData();
			//TODO 寄LOG
			Gson gs = new Gson();
			Map<String, String> param = gs.fromJson(dataMap.get("IBPD_Param"), Map.class);
			param.put("RESULTDATA",CodeUtil.toJson(param));
			writingLog(param);
			//寄MAIL
			createEbillApplyMail(param,maildata,reqParam,dataMap);
			String mailcontent = "";
			mailcontent=getMailContent(maildata,dataMap);
			String contextPath = "";
			//202202_wei_修改判斷，新增4509顯示為約定成功
			if(dataMap.get("FORCOD").equals("4001") || dataMap.get("FORCOD").equals("4509"))contextPath="臺灣企銀一般網路銀行約定成功通知";
			else contextPath="臺灣企銀一般網路銀行約定失敗通知";	
			List<String> list = new LinkedList<String>();
			list.add(reqParam.get("MAILADDR"));
			log.trace("contextPath >> {}", contextPath);
			log.trace("maillist >> {}", CodeUtil.toJson(list));
			SendAdMail_REST(contextPath, mailcontent,list,"1");
			log.trace("accountBinding_result bs.getData()>> {}", CodeUtil.toJson(bs.getData()));
		} catch (Exception e) {
			log.error("accountBinding_result  error : {}", e);
		}
		return bs;
	}
	//---------- AJAX AREA START----------
	/**
	 * 網路通行碼檢核
	 * 
	 * @param cusidn 身分證字號
	 * @param userid 使用者帳號
	 * @param pinnew 簽入密碼
	 * @return
	 */
	public BaseResult login(String cusidn, String userid, String pinnew) {
		log.debug("<<<<< Ebill_Apply service login_ajax >>>>>");
		log.trace(ESAPIUtil.vaildLog("login_cusidn: " + cusidn));
		log.trace(ESAPIUtil.vaildLog("login_userid: " + userid));

		// 處理結果
		BaseResult bs = null;
		try {
			// 網路通行碼檢核
			bs = N911_REST(cusidn, userid, pinnew);
			
			log.debug("BSDATA >> " + BeanUtils.describe(bs));
			//TODO:寫log
		} catch (Exception e) {
			log.error("{}", e);
		}
		return bs;
	}
	
	//---------- FUNCTION AREA START----------
	/**
	 * 取得轉出帳號
	 * @param cusidn
	 * @param type
	 * @return
	 */
	public BaseResult getOutACNO_List(String cusidn) {
		log.trace("cusidn>>{} ",cusidn);
		BaseResult bs = null;
		Map<String,Object> tmpData = null;
		List<String> acnoList = new ArrayList<String>();
		List<Map<String,String>> acnoList2 = null;
		LinkedHashMap<String,String> labelMap = null;
		try {
			//  CALL WS api
			bs =new  BaseResult();
			
			//N920的台幣轉帳的轉出帳號清單
			bs = N920_REST(cusidn,EBILLAPPLYACNO);
			//把REC轉出帳號轉成ajax需要的格式
			if(bs!=null && bs.getResult()) {
				tmpData = (Map) bs.getData();
				acnoList2 = (List<Map<String,String>>) tmpData.get("REC");
				labelMap = new LinkedHashMap<String,String>();

				for(Map<String,String> acMap :acnoList2) {
					String ACN = acMap.get("ACN");
					String attr = acMap.get("ATTRBUE");
					attr = outaccount_Service.chackOutacn(ACN, attr);
					labelMap.put(ACN, attr);
				}
				log.trace("labelMap data >>{}",labelMap);
				bs.setData(labelMap);

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getOutACNO_List error >> {}",e);
		}
//		return retMap;
		return bs;
	}
	public void createEbillApplyMail(Map<String,String>dataMap,List<Map<String,String>>maildata,Map<String, String>reqParam,Map<String, String>bsdata) {
		Map<String,String>tmp=new HashMap();
		tmp.put("title","交易結果");
		if(dataMap.get("RCode").equals("4001"))tmp.put("value","約定成功");
		else if(dataMap.get("RCode").equals("4509")){
			//202112_wei_新增判斷4509為約定成功
			tmp.put("value","約定成功("+dataMap.get("RCode")+")"+bsdata.get("msgName"));
		}else {
			if(StrUtil.isNotEmpty(bsdata.get("msgName")))
				tmp.put("value","約定失敗("+dataMap.get("RCode")+")"+bsdata.get("msgName"));
			else {
				tmp.put("value","約定失敗("+dataMap.get("RCode")+")");
			}
		}
		maildata.add(tmp);
		tmp=new HashMap();
		tmp.put("title","交易時間");
		tmp.put("value",DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
		maildata.add(tmp);
		tmp=new HashMap();
		tmp.put("title","事業單位");
		tmp.put("value",dataMap.get("BillerID"));
		maildata.add(tmp);
		tmp=new HashMap();
		tmp.put("title","身分證字號");
		
		//202112_wei_信件內容統編隱碼
		String cusidn = reqParam.get("CUSIDN");
		String cidn = cusidn!=null&&cusidn.length()==8 ? cusidn.substring(0, 4) + "***" : cusidn; // 企業戶
		cidn = cusidn!=null&&cusidn.length()==10 ? cusidn.substring(0, 4) + "xxx" + cusidn.substring(7, 10) : cusidn; // 一般用戶		
		tmp.put("value",cidn);
		
		maildata.add(tmp);
		tmp=new HashMap();
		tmp.put("title","授權扣款帳號");
		tmp.put("value",reqParam.get("form_account"));
		maildata.add(tmp);
		tmp=new HashMap();
		tmp.put("title","交易序號");
		tmp.put("value",dataMap.get("STAN"));
		maildata.add(tmp);
		log.debug("Maildata >> "+CodeUtil.toJson(maildata));
	}
	
	
	public String getMailContent(List<Map<String,String>> list,Map<String,String>dataMap) {
		log.debug("Maildata_list >> "+CodeUtil.toJson(list));
		log.debug("Maildata_dataMap >> "+CodeUtil.toJson(dataMap));
		StringBuffer sb = new StringBuffer();
		sb.append("<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF8\" />\n");
		if(dataMap.get("FORCOD").equals("4001"))sb.append("<title>臺灣企銀一般網路銀行約定成功通知</title>\n");
		//202201_wei_新增判斷4509為約定成功
		if(dataMap.get("FORCOD").equals("4509"))sb.append("<title>臺灣企銀一般網路銀行約定成功通知</title>\n");
		else sb.append("<title>臺灣企銀一般網路銀行約定失敗通知</title>\n");		
		sb.append("</head>\n");
		sb.append("<body>\n 親愛的客戶您好:<BR>本行依據轉出人的交易指示，發送下列通知，本通知僅供參考，不作為交易憑證，實際交易結果依本行系統為準！<BR><BR>");
		//TODO　紅字
		sb.append("<font color=\"red\">本通知為您使用臺灣企銀線上約定帳戶授權扣款之結果通知，交易資料如下:</font>");
		sb.append("<pre>\n<TABLE align=left cellSpacing=0 cellPadding=0 frame=border width=650>\n");
		sb.append("<TABLE align=left cellSpacing=0 cellPadding=0 frame=border width=600>");
		for(Map<String,String> eachdata:list) {
			sb.append("<TR><TD width='120'><DIV align=left><FONT>&nbsp;"+eachdata.get("title")+"</FONT></DIV></TD>");
			sb.append("<TD width=\"480\"><DIV align=left><FONT>&nbsp;&nbsp;"+eachdata.get("value")+"</FONT></DIV></TD></TR>");
		}
		//TODO　紅字
		sb.append("</TABLE></TABLE></pre><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>\n");
		sb.append("<font color=\"red\">(本服務透過財金資訊股份有限公司全國性繳費(稅)平台提供服務)</font><BR><font color=\"red\">若您並未執行相關動作，請立即與本行聯絡，以確保您的權益。\n</font>");
		sb.append("<BR><BR><BR>臺灣企銀 敬上<BR>\n");
		sb.append("※ 請勿直接回覆此信，如有疑問請至臺灣企銀網站");
		sb.append("<a href=\"https://www.tbb.com.tw/web/guest/-47\">客服信箱</a>留言，將盡速為您服務。\n");
		sb.append("</body>\n");
		sb.append("</html>\n");		
		log.info("sb>>{}",sb.toString());		
		return sb.toString();
	}
	
	public boolean writingLog(Map<String, String> param) {
		boolean result=false;
		ONLINE_APPOINTMENT_TXNLOG po=new ONLINE_APPOINTMENT_TXNLOG();
		ONLINE_APPOINTMENT_TXNLOG_PK pks=new ONLINE_APPOINTMENT_TXNLOG_PK();
		try {
			log.debug("IBPD Log writing start");
			Map<String,String>DBmap=new HashMap<>();
			for(String i : param.keySet()) {
				DBmap.put(i.toUpperCase(),(String)param.get(i));
			}
			log.trace("DBMap >>"+ CodeUtil.toJson(DBmap));
			//快塞
			po=CodeUtil.objectCovert(ONLINE_APPOINTMENT_TXNLOG.class, DBmap);
			log.trace("LAZY PO >>"+ CodeUtil.toJson(po));
			//補欄位
			String Txndatetime=(String)param.get("TxnDatetime");
			pks.setTXNDATE(Txndatetime.substring(0,8));
			pks.setKEYID((String)param.get("KeyID"));
			pks.setSRCID((String)param.get("SrcID"));
			pks.setSTAN((String)param.get("STAN"));
			po.setPks(pks);
			po.setTXNDATETIME(Txndatetime);
			
			DateTime d = new DateTime();
			po.setLASTDATE(d.toString("yyyyMMdd"));
			po.setLASTTIME(d.toString("HHmmss"));
			if(onlineAppointmentTxnlogDao.findByPK(po)!=null) {
				log.info("Data is exist in DB");
				onlineAppointmentTxnlogDao.update(po);
			}else {
				log.info("NEW request Data");
				onlineAppointmentTxnlogDao.save(po);
			}			
			result=true;
		}catch(Exception e) {
			log.info("IBPD Log writing Fail");
			log.debug("Log error >> {}",e);
		}
		return result;
	}
	//又靠左補0
	public static String paddingLeft(int len, String paddingString, String originalString) {
		String resultString = originalString;
		while (true) {
			if (resultString.length() < len) {
				resultString = paddingString + resultString;
			} else {
				break;
			}
		}
		return resultString;
	}
	//---------- FUNCTION AREA END----------
}
