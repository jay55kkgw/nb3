package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N070_REST_RQ extends BaseRestBean_TW implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 819877134557921735L;
	
	
	private	String	CUSIDN;	
	private	String	UID;	
	private	String	FGSVACNO;	//約定非約
	private	String	ACN;	//轉出帳號
	private	String	FGTXWAY;//繳費機制
	private	String	AMOUNT;	//繳費金額
	private	String	TYPE;	//繳費類別
	private	String	PINNEW; //壓密
	private	String	TXTOKEN;//TXTOKEN
	private	String	FGTXDATE;//即時or預約
	private	String	DPBHNO;	//非約行庫
	private	String	DPACNO;	//非約轉入帳號
	private	String	DPAGACNO;//轉入帳號
	private	String	CMTRMEMO;//備註
	private	String	CMTRMAIL;//發送Mail清單
	private	String	CMMAILMEMO;//Mail備註
	private	String	INPCST;//收款人附言
	//預約
	private String ADOPID="N070" ;//操作功能ID
	private String DPWDAC;//轉出帳號
	private String DPSVBH;//轉入行庫代碼
	private String DPSVAC;//轉入帳號/繳費代號
	private String DPTXCODE;//交易機制
	private String XMLCA;//CA 識別碼
	private String XMLCN;//憑證ＣＮ
	private String MAC;//MAC
//	private String LOGINTYPE;//NB登入或MB登入
	private String DPTXINFO;//預約時上行電文必要資訊
	private	String CMDATE;//單次預約
	private	String CMDD;//每月x日
	private	String CMSDATE;//起日
	private	String CMEDATE;//迄日
	//晶片金融卡
	private	String	ACNNO;
	private	String	iSeqNo;
	private	String	TAC;
	private	String	ISSUER;
	private	String	TRMID;
	private	String	pkcs7Sign;
	
	//IDGATE
	private String sessionID;
	private String idgateID;
	private String txnID;
	
	//手機門號TXTYPE需要
	private String isPhone;
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}	
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getFGSVACNO() {
		return FGSVACNO;
	}
	public void setFGSVACNO(String fGSVACNO) {
		FGSVACNO = fGSVACNO;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public String getTXTOKEN() {
		return TXTOKEN;
	}
	public void setTXTOKEN(String tXTOKEN) {
		TXTOKEN = tXTOKEN;
	}
	public String getFGTXDATE() {
		return FGTXDATE;
	}
	public void setFGTXDATE(String fGTXDATE) {
		FGTXDATE = fGTXDATE;
	}
	public String getDPBHNO() {
		return DPBHNO;
	}
	public void setDPBHNO(String dPBHNO) {
		DPBHNO = dPBHNO;
	}
	public String getDPACNO() {
		return DPACNO;
	}
	public void setDPACNO(String dPACNO) {
		DPACNO = dPACNO;
	}
	public String getDPAGACNO() {
		return DPAGACNO;
	}
	public void setDPAGACNO(String dPAGACNO) {
		DPAGACNO = dPAGACNO;
	}
	public String getCMTRMEMO() {
		return CMTRMEMO;
	}
	public void setCMTRMEMO(String cMTRMEMO) {
		CMTRMEMO = cMTRMEMO;
	}
	public String getCMTRMAIL() {
		return CMTRMAIL;
	}
	public void setCMTRMAIL(String cMTRMAIL) {
		CMTRMAIL = cMTRMAIL;
	}
	public String getCMMAILMEMO() {
		return CMMAILMEMO;
	}
	public void setCMMAILMEMO(String cMMAILMEMO) {
		CMMAILMEMO = cMMAILMEMO;
	}
	public String getINPCST() {
		return INPCST;
	}
	public void setINPCST(String iNPCST) {
		INPCST = iNPCST;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getDPWDAC() {
		return DPWDAC;
	}
	public void setDPWDAC(String dPWDAC) {
		DPWDAC = dPWDAC;
	}
	public String getDPSVBH() {
		return DPSVBH;
	}
	public void setDPSVBH(String dPSVBH) {
		DPSVBH = dPSVBH;
	}
	public String getDPSVAC() {
		return DPSVAC;
	}
	public void setDPSVAC(String dPSVAC) {
		DPSVAC = dPSVAC;
	}
	public String getDPTXCODE() {
		return DPTXCODE;
	}
	public void setDPTXCODE(String dPTXCODE) {
		DPTXCODE = dPTXCODE;
	}
	public String getXMLCA() {
		return XMLCA;
	}
	public void setXMLCA(String xMLCA) {
		XMLCA = xMLCA;
	}
	public String getXMLCN() {
		return XMLCN;
	}
	public void setXMLCN(String xMLCN) {
		XMLCN = xMLCN;
	}
	public String getMAC() {
		return MAC;
	}
	public void setMAC(String mAC) {
		MAC = mAC;
	}
//	public String getLOGINTYPE() {
//		return LOGINTYPE;
//	}
//	public void setLOGINTYPE(String lOGINTYPE) {
//		LOGINTYPE = lOGINTYPE;
//	}
	public String getDPTXINFO() {
		return DPTXINFO;
	}
	public void setDPTXINFO(String dPTXINFO) {
		DPTXINFO = dPTXINFO;
	}
	public String getCMDATE() {
		return CMDATE;
	}
	public void setCMDATE(String cMDATE) {
		CMDATE = cMDATE;
	}
	public String getCMDD() {
		return CMDD;
	}
	public void setCMDD(String cMDD) {
		CMDD = cMDD;
	}
	public String getCMSDATE() {
		return CMSDATE;
	}
	public void setCMSDATE(String cMSDATE) {
		CMSDATE = cMSDATE;
	}
	public String getCMEDATE() {
		return CMEDATE;
	}
	public void setCMEDATE(String cMEDATE) {
		CMEDATE = cMEDATE;
	}
	public String getACNNO() {
		return ACNNO;
	}
	public void setACNNO(String aCNNO) {
		ACNNO = aCNNO;
	}
	public String getiSeqNo() {
		return iSeqNo;
	}
	public void setiSeqNo(String iSeqNo) {
		this.iSeqNo = iSeqNo;
	}
	public String getTAC() {
		return TAC;
	}
	public void setTAC(String tAC) {
		TAC = tAC;
	}
	public String getISSUER() {
		return ISSUER;
	}
	public void setISSUER(String iSSUER) {
		ISSUER = iSSUER;
	}
	public String getTRMID() {
		return TRMID;
	}
	public void setTRMID(String tRMID) {
		TRMID = tRMID;
	}
	public String getPkcs7Sign() {
		return pkcs7Sign;
	}
	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}
	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	public String getIdgateID() {
		return idgateID;
	}
	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}
	public String getTxnID() {
		return txnID;
	}
	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}
	public String getIsPhone() {
		return isPhone;
	}
	public void setIsPhone(String isPhone) {
		this.isPhone = isPhone;
	}
	


	
	
	
	

}
