package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N392_REST_RQ extends BaseRestBean_FUND implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 880404726390179398L;
	private String DATE;
	private String TIME;
	private String PINNEW ;
	private String CUSIDN;
	private String TRANSCODE;
	private String TRADEDATE;
	private String AMT3;
	private String FCA2;
	private String OUTACN;
	private String AMT5;
	private String FCAFEE;
	private String SSLTXNO;
	private String CRY;
	
	
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getTRANSCODE() {
		return TRANSCODE;
	}
	public void setTRANSCODE(String tRANSCODE) {
		TRANSCODE = tRANSCODE;
	}
	public String getTRADEDATE() {
		return TRADEDATE;
	}
	public void setTRADEDATE(String tRADEDATE) {
		TRADEDATE = tRADEDATE;
	}
	public String getAMT3() {
		return AMT3;
	}
	public void setAMT3(String aMT3) {
		AMT3 = aMT3;
	}
	public String getFCA2() {
		return FCA2;
	}
	public void setFCA2(String fCA2) {
		FCA2 = fCA2;
	}
	public String getOUTACN() {
		return OUTACN;
	}
	public void setOUTACN(String oUTACN) {
		OUTACN = oUTACN;
	}
	public String getAMT5() {
		return AMT5;
	}
	public void setAMT5(String aMT5) {
		AMT5 = aMT5;
	}
	public String getFCAFEE() {
		return FCAFEE;
	}
	public void setFCAFEE(String fCAFEE) {
		FCAFEE = fCAFEE;
	}
	public String getSSLTXNO() {
		return SSLTXNO;
	}
	public void setSSLTXNO(String sSLTXNO) {
		SSLTXNO = sSLTXNO;
	}
	public String getCRY() {
		return CRY;
	}
	public void setCRY(String cRY) {
		CRY = cRY;
	}
}
