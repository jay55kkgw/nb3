package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N191_REST_RS  extends BaseRestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -642354852570836649L;
	
	private String CMQTIME;
	private String CMRECNUM;
	private String CMPERIOD;
	LinkedList<N191_REST_RSDATA> REC;
	
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getCMRECNUM() {
		return CMRECNUM;
	}
	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}
	public LinkedList<N191_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<N191_REST_RSDATA> rEC) {
		REC = rEC;
	}
	public String getCMPERIOD() {
		return CMPERIOD;
	}
	public void setCMPERIOD(String cMPERIOD) {
		CMPERIOD = cMPERIOD;
	}

}
