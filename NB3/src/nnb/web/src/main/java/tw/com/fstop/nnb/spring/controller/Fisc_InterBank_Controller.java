package tw.com.fstop.nnb.spring.controller;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.HtmlUtils;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.rest.bean.N194_REST_RS;
import tw.com.fstop.nnb.service.DaoService;
import tw.com.fstop.nnb.service.Fisc_InterBank_Service;
import tw.com.fstop.nnb.service.Reset_Service;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.util.StrUtils;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.web.util.JAXBUtils;
import tw.com.fstop.web.util.XSSRequestWrapper;

@Controller
@RequestMapping(value = "/FISC/InterBank")
public class Fisc_InterBank_Controller {

	Logger log = LoggerFactory.getLogger(this.getClass());

	private final String PHONE_REGEX = "^09\\d{8}$";
	
	private final String ADOPID = "FISCAT";

	@Autowired
	private Fisc_InterBank_Service fisc_interbank_service;
	
	@Autowired
	private DaoService daoservice;
	
	@Autowired
	private I18n i18n;

	// 跨行帳戶驗證
	@RequestMapping(value = "/verify_account_aj", method = {RequestMethod.POST })
	public @ResponseBody BaseResult  verify_account(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam) {
		log.trace(ESAPIUtil.vaildLog("InterBank.verify_account.reqParam: {}" + CodeUtil.toJson(reqParam)));
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

		// 查詢行庫別
		String attibk = okMap.get("ATTIBK");
		// 查詢帳號
		String attiac = okMap.get("ATTIAC");
		// 客戶統編
		String atdeno = okMap.get("CUSIDN");
		// 手機號碼
		String atmobi = okMap.get("ATMOBI");
		//客戶生日
		String birth = okMap.get("BIRTH");
		birth = birth.replaceAll("/", "");

		attiac = StrUtils.right("0000000000000000" + attiac, 16);

		String atmseq = fisc_interbank_service.getATMSEQ();

		log.info(ESAPIUtil.vaildLog("InterBank.verify_account.attibk ==> " + attibk));
		log.info(ESAPIUtil.vaildLog("InterBank.verify_account.attiac ==> " + attiac));
		log.info(ESAPIUtil.vaildLog("InterBank.verify_account.atmseq ==> " + atmseq));
		log.info(ESAPIUtil.vaildLog("InterBank.verify_account.birth ==> " + birth));

		okMap.put("ATDENO", atdeno);
		okMap.put("ATMOBI", atmobi);
		okMap.put("ATTIBK", attibk);
		okMap.put("ATTIAC", attiac);
		okMap.put("ATMSEQ", atmseq);
		okMap.put("BIRTH", birth);

		BaseResult bs = new BaseResult();
		BaseResult vebs = new BaseResult();
		try {
			vebs = fisc_interbank_service.VerfityAccount(okMap);
			
			if (vebs == null || !vebs.getResult()) {
				bs.setMessage("9999", i18n.getMsg("LB.X1761"));// 驗證失敗
				bs.setResult(false);
				return bs;
			}

			String status = "";
			long start = new Date().getTime();
			boolean keep = true;
			do {
				try {
					Thread.sleep(2800L);// 2.8秒發動一次query
				} catch (Exception e) {
				}
				
				status = fisc_interbank_service.getTxnCheckAccStatus(atmseq);
				log.info(ESAPIUtil.vaildLog("InterBank.TxnCheckAcc.status ==> " + status));
				if (status != null && status.length() > 0) {
					keep = false;
				}

				// time out處理段，60秒time out 60000L
				if ((new Date().getTime() - start) > 60000L) {
					log.info("********InterBank.verify_account time out !!!********");
					break;
				}
			} while (keep);// 狀態不為空值及狀態長度大於0

			// status 來自N194電文的 RTCHK + RTACN + RTOPS
			if (status.isEmpty()) {
				log.info("******** InterBank.verify_account FAILED!! result : false ********");
				bs.setResult(false);
				bs.setMessage("TIMEOUT", i18n.getMsg("LB.X2131"));// 驗證逾時
			} else if (status.equals("000001")) {
				log.info("******** InterBank.verify_account SUCCESS!! result : true ********");
				bs.setResult(true);
				bs.setMessage("0000", i18n.getMsg("LB.X1759"));// 驗證通過
				if (atmobi.matches(PHONE_REGEX)) {
					List<Map<String, String>> smsotpRs = fisc_interbank_service.send_smsotp(ADOPID, atdeno, atmobi, ADOPID);
					bs.setData(smsotpRs);
				}
			} else {
				log.info("******** InterBank.verify_account FAILED!! result : false ********");
				bs.setResult(false);
				
				int i = 1;
				Map<String, String> msgMap = new HashMap<String, String>();
				if (status.length() == 4) {
					msgMap = fisc_interbank_service.errCodeForFisc(status);
				} else {
					msgMap = fisc_interbank_service.errCodeForVerify(status);
				}
				//bs.setMessage("9999", i18n.getMsg("LB.X1761"));
				bs.setMessage(msgMap.get("err_code"), i18n.getMsg("LB.X1761"));
				String data = msgMap.get("err_msg") ;				
//				for (Map.Entry<String, String> entry : msgMap.entrySet()) {
//					if (entry.getKey().indexOf("_ERR") > -1) {
//						data = data + i + ".  " + entry.getValue() + " \n";
//						i ++;
//					}					
//		        }				
				bs.setData(data);
			}
		} catch (Exception e) {
			log.info("******** InterBank.verify_account FAILED!! result : false ********");
			log.error("InterBank.verify_account error >> " + e.toString(), e);
			bs.setResult(false);
			bs.setMessage("ERROR", i18n.getMsg("FE0001"));
		}

		return bs;
		
	}
		
	@RequestMapping(value = "/reply_verify_aj", method = {RequestMethod.POST })
	public @ResponseBody BaseResult reply_verify(HttpServletRequest request, HttpServletResponse response) {
		log.debug("~~~~~InterBank.reply_verify method in~~~~");
		//解決  XSS 
		XSSRequestWrapper xssRequest = new XSSRequestWrapper(request);
		BaseResult bs = new BaseResult();
		try {
			StringBuffer jb = new StringBuffer();
			String line = null;
			try {
				BufferedReader reader = xssRequest.getReader();
				while ((line = reader.readLine()) != null) {
					jb.append(ESAPIUtil.validInput(line, "GeneralString", true));
				}
				reader.close();
			} catch (Exception e) { /* report an error */ }

			String jbString = jb.toString();
			log.debug("jbString==>" + ESAPIUtil.vaildLog(jbString) + "<==");

			String sXml = HtmlUtils.htmlUnescape(jbString);
			log.info("==replace Before XML ===============\r\n" + sXml + "\r\n===============");

			sXml =sXml.replaceAll("<BlueStar.*?>", "<BlueStar>");
					//.replaceAll("</BlueStar.*?>", "");
			
			log.info("==EAI BlueStar Before XML ===============\r\n" + sXml + "\r\n===============");
			N194_REST_RS rs = JAXBUtils.unmarshallerXML(sXml, N194_REST_RS.class);

			String status = rs.getRTCHK() + rs.getRTACN() + rs.getRTOPS();
			log.info("==RTSEQ ===============\r\n" + rs.getRTSEQ() + "\r\n===============");
			//log.info("==ABEND ===============\r\n" + resp.getABEND() + "\r\n===============");
			log.info("==STATUS ===============\r\n" + status + "\r\n===============");
			log.info("==RCODE ===============\r\n" + rs.getRCODE() + "\r\n===============");
			fisc_interbank_service.rogerThat(rs.getRTSEQ(), status, rs.getRCODE());
			
		} catch (Exception e) {
			log.error("InterBank.reply_verify error >> " + e.toString(), e);
		}
		
		bs.setResult(true);
		bs.setMessage("0000", "");
		return bs;

	}
	
	@RequestMapping(value="/otpSend_aj", method=RequestMethod.POST)
	public @ResponseBody BaseResult otpSend(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqparam) {
		log.trace("InterBank.otpSend param: {}",ESAPIUtil.vaildLog( CodeUtil.toJson(reqparam)));
		
		BaseResult bs = new BaseResult();
		
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqparam);
		
		// 客戶統編
		String atdeno = okMap.get("CUSIDN");
		// 手機號碼
		String atmobi = okMap.get("ATMOBI");
		
		if (atmobi.matches(PHONE_REGEX)) {
			List<Map<String, String>> smsotpRs = fisc_interbank_service.send_smsotp(ADOPID, atdeno, atmobi, ADOPID);
			Map<String, String> rs = smsotpRs.get(0);
			if ("0000".equals(rs.get("MSGCOD"))) {
				bs.setResult(true);				
			} else {
				bs.setResult(false);
				bs.setMsgCode(rs.get("MSGCOD"));
				bs.setMessage(rs.get("MSGSTR"));
			}
			bs.setData(smsotpRs);
		} else {
			bs.setResult(false);
			bs.setMsgCode("XZ9999");
			bs.setMessage("手機電話格式錯誤");
		}
		return bs;
	}
	
	@RequestMapping(value="/otpverify_aj", method=RequestMethod.POST)
	public @ResponseBody BaseResult otpVerify(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqparam) {
		log.trace("InterBank.otpSend param: {}",ESAPIUtil.vaildLog(CodeUtil.toJson(reqparam)));
		
		BaseResult bs = new BaseResult();
		
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqparam);
		
		String cusidn = okMap.get("CUSIDN");
		String otp = okMap.get("OTP");
		
		boolean result_bool = fisc_interbank_service.OTPVerify(cusidn, otp, ADOPID);		
		bs.setResult(result_bool);

		return bs;
	}
	
	/**
	 * 取得銀行代號名稱
	 * 
	 * @param request
	 * @param response
	 * @param reqParama
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getDpBHNO_aj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult getDpBHNO(@RequestParam Map<String, String> reqParam, Model model) {
		String str = "getInAcno_aj";
		log.trace("hi {}", str);
		log.trace("reqParam>>{}", ESAPIUtil.vaildLog( CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			bs = daoservice.getDpBHNO_List("noLoing");
		} catch (Exception e) {
			log.debug("");
		}
		return bs;
	}

}
