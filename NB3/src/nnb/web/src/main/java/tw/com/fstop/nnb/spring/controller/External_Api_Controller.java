package tw.com.fstop.nnb.spring.controller;

import java.util.Base64;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.External_Api_Service;
import tw.com.fstop.util.ESAPIUtil;

@RestController
@RequestMapping(value = "/EXTERNAL/API")
public class External_Api_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private External_Api_Service external_api_service;
	
	/**
	 * 供外部系統查詢新個網使用者登入狀態
	 */
	@RequestMapping(value = "/checkLogin", method = {RequestMethod.POST }, 
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult checkLogin(HttpServletRequest request, HttpServletResponse response,
			@RequestBody Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			// 解密使用者統編
			String cusidn = new String(Base64.getDecoder().decode(okMap.get("cusidn").toString()), "utf-8").toUpperCase();
			log.debug(ESAPIUtil.vaildLog("checkLogin.cusidn: {}"+ cusidn));
			
			// 檢查登入狀態
			boolean checkLoginStatus = external_api_service.isLoginStatus(cusidn);
			log.debug("checkLogin.checkLoginStatus: {}", checkLoginStatus);
			
			// 新個網未登入或不曾登入，外部系統可做登入
			if(checkLoginStatus) {
				bs.setResult(true);
				bs.setMsgCode("0");
				bs.setMessage("NB3 is logout");
				
			} // 新個網已登入，外部系統要做登入得先呼叫新個網強制剔退API
			else {
				bs.setResult(false);
				bs.setMsgCode("1");
				bs.setMessage("NB3 is login");
				
			}
			
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	
	/**
	 * 供外部系統強制登出新個網使用者
	 */
	@RequestMapping(value = "/logoutForce", method = {RequestMethod.POST }, 
				produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult logoutForce(HttpServletRequest request, HttpServletResponse response,
			@RequestBody Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			// 解密使用者統編
			String cusidn = new String(Base64.getDecoder().decode(okMap.get("cusidn").toString()), "utf-8").toUpperCase();
			log.debug(ESAPIUtil.vaildLog("logoutForce.cusidn: {}"+ cusidn));
			
			// 檢查登入狀態
			boolean logoutForceResult = external_api_service.logoutForce(cusidn);
			log.debug("logoutForce.logoutForceResult: {}", logoutForceResult);
			
			// 強制登出新個網成功
			if(logoutForceResult) {
				bs.setResult(true);
				bs.setMsgCode("0");
				bs.setMessage("logout force success");
				
			} // 強制登出新個網失敗
			else {
				bs.setResult(false);
				bs.setMsgCode("1");
				bs.setMessage("logout force fail");
			}
			
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	
	
	/**
	 * 供外部系統查詢使用者是否曾經登入新個網
	 */
	@RequestMapping(value = "/onceLogin", method = {RequestMethod.POST }, 
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BaseResult onceLogin(HttpServletRequest request, HttpServletResponse response,
			@RequestBody Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			// 解密使用者統編
			String cusidn = new String(Base64.getDecoder().decode(okMap.get("cusidn").toString()), "utf-8").toUpperCase();
			log.debug(ESAPIUtil.vaildLog("onceLogin.cusidn: {}"+ cusidn));
			
			// 檢查登入狀態
			boolean onceLoginResult = external_api_service.onceLogin(cusidn);
			log.debug("onceLogin.onceLoginResult: {}", onceLoginResult);
			
			// 使用者曾經登入新個網
			if(onceLoginResult) {
				bs.setResult(true);
				bs.setMsgCode("0");
				bs.setMessage("user logged in");
				
			} // 使用者不曾登入新個網
			else {
				bs.setResult(false);
				bs.setMsgCode("1");
				bs.setMessage("user never logged in");
			}
			
		} catch (Exception e) {
			log.error("",e);
		}
		return bs;
	}
	
	
}
