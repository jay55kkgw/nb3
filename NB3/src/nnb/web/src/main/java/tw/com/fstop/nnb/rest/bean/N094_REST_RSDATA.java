package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N094_REST_RSDATA extends BaseRestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8597603896608879762L;

	private String SOURCE;	//申購/變更來源
	private String QTAMT;	//約定扣款金額
	private String TSFACN;	//扣款帳號
	private String FEEAMT;	//扣款失敗手續費
	private String FEELTD;	//扣款日期
	
	public String getSOURCE() {
		return SOURCE;
	}
	public void setSOURCE(String sOURCE) {
		SOURCE = sOURCE;
	}
	public String getQTAMT() {
		return QTAMT;
	}
	public void setQTAMT(String qTAMT) {
		QTAMT = qTAMT;
	}
	public String getTSFACN() {
		return TSFACN;
	}
	public void setTSFACN(String tSFACN) {
		TSFACN = tSFACN;
	}
	public String getFEEAMT() {
		return FEEAMT;
	}
	public void setFEEAMT(String fEEAMT) {
		FEEAMT = fEEAMT;
	}
	public String getFEELTD() {
		return FEELTD;
	}
	public void setFEELTD(String fEELTD) {
		FEELTD = fEELTD;
	}
}
