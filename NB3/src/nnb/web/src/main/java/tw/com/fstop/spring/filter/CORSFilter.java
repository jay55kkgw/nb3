
/*
 * Copyright (c) 2017, FSTOP, Inc. All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tw.com.fstop.spring.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.OncePerRequestFilter;


/**
 * Enable Cross-Origin Resource Sharing for RESTful API.  
 * 
 * <pre>
 * When accessing a REST API:
 * No 'Access-Control-Allow-Origin' header is present on the requested resource. 
 * Origin 'null' is therefore not allowed access.
 * 
 * </pre>
 * 
 * @since 1.0.1
 */
public class CORSFilter extends OncePerRequestFilter
{
    private final static Logger log = LoggerFactory.getLogger(CORSFilter.class);

    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse rep, FilterChain chain)
            throws ServletException, IOException
    {
//    	
//        log.info("Adding CORS Headers for RESTful API ........................");
//        //required
//        rep.setHeader("Access-Control-Allow-Origin", "*");
//        //required
//        rep.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
//        //required
//        rep.setHeader("Access-Control-Allow-Headers", "*");
//        
//        //Access-Control-Allow-Credentials (optional) true/false
//        
//        //optional
//        rep.setHeader("Access-Control-Max-Age", "3600");
//        
        chain.doFilter(req, rep);
        
    }

}
