package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N942_REST_RQ extends BaseRestBean_PS implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -925802414319513030L;

	private String CUSIDN;
	private String OLDUID;
	private String NEWUID;
	private String ADOPID;
	private String FGTXWAY;
	
	public String getFGTXWAY() {
		return FGTXWAY;
	}

	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}

	public String getCUSIDN() {
		return CUSIDN;
	}
	
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	
	public String getOLDUID() {
		return OLDUID;
	}
	
	public void setOLDUID(String oLDUID) {
		OLDUID = oLDUID;
	}
	
	public String getNEWUID() {
		return NEWUID;
	}
	
	public void setNEWUID(String nEWUID) {
		NEWUID = nEWUID;
	}
}
