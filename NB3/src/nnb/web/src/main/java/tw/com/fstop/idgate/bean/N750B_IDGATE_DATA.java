package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N750B_IDGATE_DATA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -762951082705400880L;
	
	// 轉出帳號
	@SerializedName(value = "OUTACN")
	private String OUTACN;
	// 條碼一
	@SerializedName(value = "BARCODE1")
	private String BARCODE1;
	// 條碼二
	@SerializedName(value = "BARCODE2")
	private String BARCODE2;
	// 條碼三
	@SerializedName(value = "BARCODE3")
	private String BARCODE3;
	// 水號
	@SerializedName(value = "WAT_NO")
	private String WAT_NO;
	// 繳款金額 
	@SerializedName(value = "AMOUNT")
	private String AMOUNT;

}
