package tw.com.fstop.nnb.service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.net.ftp.FTPClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tw.com.fstop.tbb.nnb.dao.AdmEmpInfoDao;
import fstop.orm.po.ADMBRANCH;
import fstop.orm.po.TXNACNAPPLY;
import fstop.orm.po.TXNLOANAPPLY;
import fstop.orm.po.TXNUSER;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmBranchDao;
import tw.com.fstop.tbb.nnb.dao.AdmZipDataDao;
import tw.com.fstop.tbb.nnb.dao.TxnAcnApplyDao;
import tw.com.fstop.tbb.nnb.dao.TxnUserDao;
import tw.com.fstop.tbb.nnb.util.StrUtils;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.NumericUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.web.util.FTPUtil;

@Service
public class Digital_Account_Service extends Base_Service{
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Resource
	private I18n i18n;

	@Autowired
	AdmZipDataDao admZipDataDao;
	
	@Autowired
	private TxnAcnApplyDao txnAcnApplyDao;

	@Autowired
	private AdmBranchDao admBranchDao;
	
	@Autowired
	private TxnUserDao txnUserDao;

	@Autowired
	private AdmEmpInfoDao admEmpInfoDao;
	/**
	 * 數位存款帳戶晶片金融卡確認領用申請及變更密碼step1
	 * @param cusidn
	 * @return 
	 */
	public BaseResult financial_card_confirm_step1(Map<String,String> reqparam)	{		
		log.trace("financial_card_confirm_step1...");
		BaseResult bs  = null ;
		try {
			bs = new BaseResult();
			
			String ACNTYPE = txnAcnApplyDao.findACNTYPEByCUSIDN(reqparam.get("CUSIDN"));
			String ACN = txnAcnApplyDao.findACNByCUSIDN(reqparam.get("CUSIDN"));
			String BRHCOD = txnAcnApplyDao.findBRHCODByCUSIDN(reqparam.get("CUSIDN"));
			
			log.trace(ESAPIUtil.vaildLog("ACNTYPE+ACN+BRHCOD>>{}"+ ACNTYPE+" "+ACN+" "+BRHCOD));
			
			Map<String,Object> datamap = new HashMap<String,Object>();
			datamap.put("ACNTYPE", ACNTYPE);
			datamap.put("CUSIDN", reqparam.get("CUSIDN"));
			datamap.put("UID", reqparam.get("CUSIDN"));
			datamap.put("ACN", ACN);
			datamap.put("BRHCOD", BRHCOD);
			datamap.put("jsondc", reqparam.get("jsondc"));
			
			if(ACNTYPE.length() != 2) {	
				reqparam.remove("CUSIDN");
				reqparam.remove("UID");
//				throw TopMessageException.create("Z617");
				bs.setResult(Boolean.FALSE);
				bs.setMsgCode("Z617");
				bs.setMessage(i18n.getMsg("LB.X1829"));//無數位帳號申請資料，不需補正交易。
				return bs;
			}
			
			bs.addData("REC", datamap);
			bs.setResult(true);
			
		} catch(Exception e) {
			reqparam.remove("CUSIDN");
			reqparam.remove("UID");		
			log.error("financial_card_confirm", e);
//			throw TopMessageException.create("Z617");
			bs.setResult(Boolean.FALSE);
			bs.setMsgCode("Z617");
			bs.setMessage(i18n.getMsg("LB.X1829"));//無數位帳號申請資料，不需補正交易。
		}
		return bs;
	}
	/**
	 * 數位存款帳戶晶片金融卡確認領用申請及變更密碼step2
	 * @param reqparam
	 * @return
	 */
	public BaseResult financial_card_confirm_step2(Map<String,String> reqparam)	{		
		log.trace("financial_card_confirm_step2...");
		BaseResult bs = null ;
		try {
			bs = NB32_1_REST(reqparam);
			bs.setResult(true);
			log.trace("financial_card_confirm_step2.bs: {}", CodeUtil.toJson(bs));
		} catch (Exception e) {
			log.error("financial_card_confirm", e);
		}	
		return  bs;
	}
	/**
	 * 數位存款帳戶晶片金融卡確認領用申請及變更密碼step2
	 * @param reqparam
	 * @return
	 */
	public BaseResult financial_card_confirm_cardEnable(Map<String,String> reqparam) {		
		log.trace("financial_card_confirm_cardEnable...");
		BaseResult bs = null ;
		try {
			bs = NB32_2_REST(reqparam);
			bs.setResult(true);
			log.debug("financial_card_confirm_cardEnable.bs: {}", CodeUtil.toJson(bs));
			if(bs != null && bs.getResult()) {
				List bh = getAdmBhContact(txnAcnApplyDao.findBRHCODByCUSIDN(reqparam.get("CUSIDN")));
				log.debug(ESAPIUtil.vaildLog("bh >> {}"+bh.get(0)));
				bs.addData("BRHNAME", ((ADMBRANCH)bh.get(0)).getADBRANCHNAME());
				bs.addData("BRHTEL", ((ADMBRANCH)bh.get(0)).getTELNUM());
				
			}
		} catch (Exception e) {
			log.error("financial_card_confirm", e);
		}	
		return  bs;
	}
	
	/**
	 * 數位存款帳戶補申請晶片金融卡step1
	 * @param reqparam
	 * @return
	 */
	public BaseResult financial_card_renew_step1(Map<String,String> reqparam)	{		
		log.trace("financial_card_confirm_Service~~~~!!");
		log.trace(ESAPIUtil.vaildLog("reqparam >> "+ CodeUtil.toJson(reqparam)));
		BaseResult bs = null ;
		try {
			bs=new BaseResult();
			bs =NB31_REST(reqparam);
			Map<String,Object>datamap=(Map<String, Object>) bs.getData();
			Map<String,String>datamapdata=new HashMap<String,String>();
			log.trace(ESAPIUtil.vaildLog("CUSIDN: >>"+reqparam.get("CUSIDN")));
			log.trace("CUSIDN: >>"+bs.getData());
			TXNACNAPPLY data =txnAcnApplyDao.getApplyDataByCUSIDN(reqparam.get("CUSIDN"));
			log.trace(ESAPIUtil.vaildLog("DATA >> "+data));
			if(data!=null) {
				datamapdata.putAll(reqparam);
				datamapdata.putAll(CodeUtil.objectCovert(Map.class,data));
				log.trace(ESAPIUtil.vaildLog("datamapdata : >> "+datamapdata));
				datamap.put("REC", datamapdata);
				bs.setResult(true);
				log.trace("BS >> "+bs.getData());
			}else {
				datamapdata.putAll(reqparam);
				bs.setResult(false);
			}
		} catch (Exception e) {
			log.error("financial_card_confirm",e);
		}	
		return  bs;
	}
	/**
	 * 數位存款帳戶補申請晶片金融卡step2
	 * @param reqparam
	 * @return
	 */
	public BaseResult financial_card_renew_step2(Map<String,String> reqparam)	{		
		log.trace("financial_card_renew_step2_Service~~~~!!");
		log.trace(ESAPIUtil.vaildLog("reqparam >> "+ CodeUtil.toJson(reqparam)));
		BaseResult bs = null ;
		try {
		bs=new BaseResult();
		CodeUtil.convert2BaseResult(bs, reqparam);
		if(bs!=null)bs.setResult(true);
		log.trace("BS >> "+bs.getData());
		} catch (Exception e) {
			log.error("financial_card_renew_step2_Service",e);
		}	
		return  bs;
	}
	/**
	 * 數位存款帳戶補申請晶片金融卡step3
	 * @param reqparam
	 * @return
	 */
	public BaseResult financial_card_renew_step3(Map<String,String> reqparam)	{		
		log.trace("financial_card_renew_step3_Service~~~~!!");
		log.trace(ESAPIUtil.vaildLog("reqparam >> "+ CodeUtil.toJson(reqparam)));
		BaseResult bs = null ;
		try { 
			bs=new BaseResult();
			List acnlist = new ArrayList(); 
			List<TXNACNAPPLY> txnacnList = txnAcnApplyDao.findByCUSIDNOrderByDateAndTimeDESC(reqparam.get("CUSIDN"));
			if (!txnacnList.isEmpty()) {
				for(int i=0;i<txnacnList.size();i++) {
					TXNACNAPPLY txnacnapply = txnacnList.get(i);
					acnlist.add(txnacnapply.getACN());
				}
			}
			TXNACNAPPLY data =txnAcnApplyDao.getApplyDataByCUSIDN(reqparam.get("CUSIDN"));
			CodeUtil.convert2BaseResult(bs, reqparam);
			Map<String,String>datamap=(Map<String, String>) bs.getData();
			bs.addData("ACNLIST", acnlist);
//			datamap.put("CRDTYP", "1");
//			datamap.put("SEND", "2");
//			datamap.put("CChargeApply", "N");
//			datamap.put("NAATAPPLY", "Y");
//			datamap.put("CROSSAPPLY", "Y");
//			datamap.put("LMT", "0");
			if(bs!=null)bs.setResult(true);
			log.trace("BS >> "+bs.getData());
			
		} catch (Exception e) {
			log.error("financial_card_renew_step3_Service",e);
		}	
		return  bs;
	}
	/**
	 * 數位存款帳戶補申請晶片金融卡step4
	 * @param reqparam
	 * @return
	 */
	public BaseResult financial_card_renew_step4(Map<String,String> reqparam)	{		
		log.trace("financial_card_renew_step4_Service~~~~!!");
		log.trace(ESAPIUtil.vaildLog("reqparam >> "+ CodeUtil.toJson(reqparam)));
		BaseResult bs = null ;
		try { 
		bs=new BaseResult();
		CodeUtil.convert2BaseResult(bs, reqparam);
		if(bs!=null)bs.setResult(true);
		log.trace("BS >> "+bs.getData());
		} catch (Exception e) {
			log.error("financial_card_renew_step4_Service",e);
		}	
		return  bs;
	}
	/**
	 * 數位存款帳戶補申請晶片金融卡step5
	 * @param reqparam
	 * @return
	 */
	public BaseResult financial_card_renew_step5(Map<String,String> reqparam)	{		
		log.trace("financial_card_confirm_Service~~~~!!");
		log.trace(ESAPIUtil.vaildLog("reqparam >> "+ CodeUtil.toJson(reqparam)));
		BaseResult bs = null ;
		try {
		bs=new BaseResult();
		bs =NB31_1_REST(reqparam);
		
		//data-post-processing
		Map<String,String>datamap=(Map<String, String>) bs.getData();
		datamap.put("SEND",reqparam.get("SEND"));
		//if(! datamap.get("SEND").equals("1")) {
			log.trace(ESAPIUtil.vaildLog("Reqparam BANK >> "+datamap.get("BRHCOD")));
			List<ADMBRANCH>bankinfo_prototype=admBranchDao.findByBhID(datamap.get("BRHCOD").toString());
			log.trace(ESAPIUtil.vaildLog("BankInfo >> "+bankinfo_prototype));
			Map<String,String>bankinfo=CodeUtil.objectCovert(Map.class,bankinfo_prototype.get(0));
			log.trace(ESAPIUtil.vaildLog("BankInfo >> "+bankinfo));
			datamap.put("BRHTEL", bankinfo.get("TELNUM"));
			datamap.put("BRHNAME",  bankinfo.get("ADBRANCHNAME"));
		//}
		if(bs.getData()!=null)bs.setResult(true);
		log.trace("BS >> "+bs.getData());
		} catch (Exception e) {
			log.error("financial_card_confirm",e);
		}	
		return  bs;
	}

	/**
	 * 線上開立數位存款帳戶(N203)
	 * @param reqparam
	 * @return
	 */
	public BaseResult apply_digital_account_p3(String FGTXWAY,String CUSIDN) {
		BaseResult bs = null;
		try {
			bs = N103_REST(FGTXWAY, CUSIDN);
			if(bs != null && bs.getResult()) {
				Map<String, Object> callData = (Map<String, Object>)bs.getData();
//				bs.addData("CUSNAME", ((String)callData.get("CUSNAME")).replaceAll("　", ""));
//				bs.addData("CTTADR", ((String)callData.get("CTTADR")).replaceAll("　", ""));
//				bs.addData("PMTADR", ((String)callData.get("PMTADR")).replaceAll("　", ""));
				
				bs.addData("ZIP1", callData.get("POSTCOD1"));
				bs.addData("ZIP2", callData.get("POSTCOD2"));
				String BIRTHDAY = (String)callData.get("BIRTHDAY");
//				bs.addData("CCBIRTHDATE",DateUtil.convertDate(1,BIRTHDAY,"yyyMMdd","yyyy/MM/dd"));
				bs.addData("CCBIRTHDATEYY", BIRTHDAY.length() >= 3 ? BIRTHDAY.substring(0, 3) : "");
				bs.addData("CCBIRTHDATEMM", BIRTHDAY.length() >= 5 ? BIRTHDAY.substring(3, 5) : "");
				bs.addData("CCBIRTHDATEDD", BIRTHDAY.length() >= 7 ? BIRTHDAY.substring(5, 7) : "");
				String PMTADR = (String)callData.get("PMTADR");
				String CTTADR = (String)callData.get("CTTADR");
				bs.addData("CITY1", PMTADR.length() >= 3 ? PMTADR.substring(0, 3) : "#");
				bs.addData("CITY2", CTTADR.length() >= 3 ? CTTADR.substring(0, 3) : "#");
				bs.addData("CITY3", ((String)callData.get("CITY2")).length() >= 3 ? (String)callData.get("CITY2") : "#");
				if(PMTADR.length() > 0) {
//					bs.addData("ADDR11",PMTADR.indexOf("村")>-1 ? PMTADR.substring(PMTADR.indexOf("村")-2,PMTADR.indexOf("村")) : "");
//					bs.addData("ADDR12",PMTADR.indexOf("里")>-1 ? PMTADR.substring(PMTADR.indexOf("里")-2,PMTADR.indexOf("里")) : "");	
//					bs.addData("ADDR13",(PMTADR.indexOf("里")>-1 && PMTADR.indexOf("鄰")>-1) ? PMTADR.substring(PMTADR.indexOf("里")+1,PMTADR.indexOf("鄰")) : (PMTADR.indexOf("鄰")>-1 ? PMTADR.substring(PMTADR.indexOf("鄰")-1,PMTADR.indexOf("鄰")) : ""));	
					if( PMTADR.substring(2, 3).equals("市")) {
						PMTADR = PMTADR.length() > 3 ? PMTADR.substring(3) : PMTADR;
						bs.addData("ADDR1", PMTADR.indexOf("區")>-1 ? PMTADR.substring(PMTADR.indexOf("區")+1) : 
											PMTADR.indexOf("東沙群島")>-1 ? PMTADR.substring(PMTADR.indexOf("島")+1) : 
											PMTADR.indexOf("南沙群島")>-1 ? PMTADR.substring(PMTADR.indexOf("島")+1) : PMTADR.substring(3));
						bs.addData("ZONE1", PMTADR.length() >= 6 ? PMTADR.substring(0, 	PMTADR.indexOf("區")>-1 ? PMTADR.indexOf("區")+1 :  
																						PMTADR.indexOf("東沙群島")>-1 ? PMTADR.indexOf("島")+1 : 
																						PMTADR.indexOf("南沙群島")>-1 ? PMTADR.indexOf("島")+1 : 3) : "#");
					}else {
						PMTADR = PMTADR.length() > 3 ? PMTADR.substring(3) : PMTADR;
						bs.addData("ADDR1", PMTADR.indexOf("市")>-1 ? PMTADR.substring(PMTADR.indexOf("市")+1) : 
											PMTADR.indexOf("鎮")>-1 ? PMTADR.substring(PMTADR.indexOf("鎮")+1) : 
											PMTADR.indexOf("鄉")>-1 ? PMTADR.substring(PMTADR.indexOf("鄉")+1) : 
											PMTADR.indexOf("釣魚臺列嶼")>-1 ? PMTADR.substring(PMTADR.indexOf("嶼")+1) : PMTADR.substring(3));
						bs.addData("ZONE1", PMTADR.length() >= 6 ? PMTADR.substring(0, 	PMTADR.indexOf("市")>-1 ? PMTADR.indexOf("市")+1 : 
																						PMTADR.indexOf("鎮")>-1 ? PMTADR.indexOf("鎮")+1 : 
																						PMTADR.indexOf("鄉")>-1 ? PMTADR.indexOf("鄉")+1 : 
																						PMTADR.indexOf("釣魚臺列嶼")>-1 ? PMTADR.indexOf("嶼")+1 : 3) : "#");
					}
				}
				if(CTTADR.length() > 0) {
//					bs.addData("ADDR21",CTTADR.indexOf("村")>-1 ? CTTADR.substring(CTTADR.indexOf("村")-2,CTTADR.indexOf("村")) : "");
//					bs.addData("ADDR22",CTTADR.indexOf("里")>-1 ? CTTADR.substring(CTTADR.indexOf("里")-2,CTTADR.indexOf("里")) : "");	
//					bs.addData("ADDR23",(CTTADR.indexOf("里")>-1 && CTTADR.indexOf("鄰")>-1) ? CTTADR.substring(CTTADR.indexOf("里")+1,CTTADR.indexOf("鄰")) : (CTTADR.indexOf("鄰")>-1 ? CTTADR.substring(CTTADR.indexOf("鄰")-1,CTTADR.indexOf("鄰")) : ""));	
					if( CTTADR.substring(2, 3).equals("市")) {
						CTTADR = CTTADR.length() > 3 ? CTTADR.substring(3) : CTTADR;
						bs.addData("ADDR2", CTTADR.indexOf("區")>-1 ? CTTADR.substring(CTTADR.indexOf("區")+1) : 
											CTTADR.indexOf("東沙群島")>-1 ? CTTADR.substring(CTTADR.indexOf("島")+1) :  
											CTTADR.indexOf("南沙群島")>-1 ? CTTADR.substring(CTTADR.indexOf("島")+1) : CTTADR.substring(3));
						bs.addData("ZONE2", CTTADR.length() >= 6 ? CTTADR.substring(0, 	CTTADR.indexOf("區")>-1 ? CTTADR.indexOf("區")+1 : 
																						CTTADR.indexOf("東沙群島")>-1 ? CTTADR.indexOf("島")+1 : 
																						CTTADR.indexOf("南沙群島")>-1 ? CTTADR.indexOf("島")+1 : 3) : "#");
					}else {
						CTTADR = CTTADR.length() > 3 ? CTTADR.substring(3) : CTTADR;
						bs.addData("ADDR2", CTTADR.indexOf("市")>-1 ? CTTADR.substring(CTTADR.indexOf("市")+1) : 
											CTTADR.indexOf("鎮")>-1 ? CTTADR.substring(CTTADR.indexOf("鎮")+1) : 
											CTTADR.indexOf("鄉")>-1 ? CTTADR.substring(CTTADR.indexOf("鄉")+1) : 
											CTTADR.indexOf("釣魚臺列嶼")>-1 ? CTTADR.substring(CTTADR.indexOf("嶼")+1) : CTTADR.substring(3));
						bs.addData("ZONE2", CTTADR.length() >= 6 ? CTTADR.substring(0, 	CTTADR.indexOf("市")>-1 ? CTTADR.indexOf("市")+1 : 
																						CTTADR.indexOf("鎮")>-1 ? CTTADR.indexOf("鎮")+1 : 
																						CTTADR.indexOf("鄉")>-1 ? CTTADR.indexOf("鄉")+1 : 
																						CTTADR.indexOf("釣魚臺列嶼")>-1 ? CTTADR.indexOf("嶼")+1 : 3) : "#");
					}
				}
				if(((String)callData.get("HOMETEL")).length() > 0) {
					bs.addData("HOMETEL",((String)callData.get("HOMETEL")).indexOf("#")>-1 ? 
							((String)callData.get("HOMETEL")).substring(0,((String)callData.get("HOMETEL")).indexOf("#")) : ((String)callData.get("HOMETEL")));
				}
				String BUSTEL = (String)callData.get("BUSTEL");
				if(BUSTEL.length() > 0) {
					bs.addData("BUSTEL11",BUSTEL.indexOf("#")>-1 ? BUSTEL.substring(0,BUSTEL.indexOf("#")) :  BUSTEL);
					bs.addData("BUSTEL12",BUSTEL.indexOf("#")>-1 ? BUSTEL.substring(BUSTEL.indexOf("#")+1) :  "");
				}
//				bs.addData("ZONE1", PMTADR.length() >= 6 ? PMTADR.substring(3, 6) : "#");
//				bs.addData("ADDR1", PMTADR.length() > 6 ? PMTADR.substring(6) : "");
//				bs.addData("ZONE2", CTTADR.length() >= 6 ? CTTADR.substring(3, 6) : "#");
//				bs.addData("ADDR2", CTTADR.length() > 6 ? CTTADR.substring(6) : "");
				bs.addData("ZONE3", ((String)callData.get("ZONE2")).length() >= 3 ? (String)callData.get("ZONE2") : "#");
				
				//因客戶輸入之單位千元改萬元，顯示須調整數值為萬元單位，從這裡除10
				int salary_in = Integer.valueOf(String.valueOf(callData.get("SALARY"))) / 10;
				String salary2_in = String.valueOf(salary_in);

				bs.addData("SALARY", NumericUtil.deleteComma(NumericUtil.fmtAmount((String)salary2_in, 0)));
				log.debug("ISCUSIDN >> {}",admEmpInfoDao.isEmpCUSID(CUSIDN));
				bs.addData("ISCUSIDN", admEmpInfoDao.isEmpCUSID(CUSIDN) ? "Y":"N");
			}
		}catch(Exception e) {
			log.error(e.toString());
		}
		return bs;
	}
	
	public BaseResult apply_deposit_account_result(Map<String, String> reqParam, String uploadPath) {
		String uploadPath_N201 = uploadPath;
		BaseResult bs = null;
		String separ = File.separator;
		try {
			bs = new BaseResult();
//			reqParam.put("CHGE_DT", DateUtil.convertDate(1,reqParam.get("CHGE_DT"),"yyyyMMdd","yyyMMdd"));
			reqParam.put("SALARY", reqParam.get("SALARY").replace(",", ""));
						
			//因客戶輸入之單位千元改萬元，後送須調整數值回千元單位，從這裡乘10
			int salary_out = Integer.valueOf(String.valueOf(reqParam.get("SALARY"))) * 10;
			String salary2_out = String.valueOf(salary_out);
			reqParam.put("SALARY", salary2_out);
			
			if(reqParam.get("ACNNO").length() > 11)
				reqParam.put("CHIP_ACN", reqParam.get("ACNNO").substring(reqParam.get("ACNNO").length()-11));
			if(reqParam.get("MAILADDR") != null)
				reqParam.put("MAILADDR", reqParam.get("MAILADDR").toLowerCase());

			List bhP = getAdmBhContact((String)reqParam.get("BANKCODE"));
			reqParam.put("BANKCODE", ((ADMBRANCH)bhP.get(0)).getBANKCODE());
			bs = N203_REST(reqParam);
			if(bs != null && bs.getResult()) {

				Map<String,Object> callData = (Map<String,Object>)bs.getData();
				List bh = getAdmBhContact((String)callData.get("BRHCOD"));
				log.debug(ESAPIUtil.vaildLog("bh >> "+bh.get(0)));
				bs.addData("BRHNAME", ((ADMBRANCH)bh.get(0)).getADBRANCHNAME());
				bs.addData("BRHTEL", ((ADMBRANCH)bh.get(0)).getTELNUM());
//				if(!reqParam.get("FILE1").equals("")) {
//					String filetype = reqParam.get("FILE1").substring(reqParam.get("FILE1").lastIndexOf(".") ,reqParam.get("FILE1").length());
//					uploadImage(uploadPath_N201, reqParam.get("FILE1"));
//					removefile(uploadPath_N201, reqParam.get("FILE1"), SpringBeanFactory.getBean("imgMovePath") + separ, reqParam.get("UID") + "_01_" + DateUtil.getDate("") + filetype);
//				}
//				if(!reqParam.get("FILE2").equals("")) {
//					String filetype = reqParam.get("FILE2").substring(reqParam.get("FILE2").lastIndexOf(".") ,reqParam.get("FILE2").length());
//					uploadImage(uploadPath_N201, reqParam.get("FILE2"));
//					removefile(uploadPath_N201, reqParam.get("FILE2"), SpringBeanFactory.getBean("imgMovePath") + separ, reqParam.get("UID") + "_02_" + DateUtil.getDate("") + filetype);
//				}
//				if(!reqParam.get("FILE3").equals("")) {
//					String filetype = reqParam.get("FILE3").substring(reqParam.get("FILE3").lastIndexOf(".") ,reqParam.get("FILE3").length());
//					uploadImage(uploadPath_N201, reqParam.get("FILE3"));
//					removefile(uploadPath_N201, reqParam.get("FILE3"), SpringBeanFactory.getBean("imgMovePath") + separ, reqParam.get("UID") + "_03_" + DateUtil.getDate("") + filetype);
//				}
//				if(!reqParam.get("FILE4").equals("")) {
//					String filetype = reqParam.get("FILE4").substring(reqParam.get("FILE4").lastIndexOf(".") ,reqParam.get("FILE4").length());
//					uploadImage(uploadPath_N201, reqParam.get("FILE4"));
//					removefile(uploadPath_N201, reqParam.get("FILE4"), SpringBeanFactory.getBean("imgMovePath") + separ, reqParam.get("UID") + "_04_" + DateUtil.getDate("") + filetype);
//				}
				
			}
		}catch(Exception e) {
			log.error(e.toString());
		}
		return bs;
	}

	public BaseResult apply_digital_account_nb_result(Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			if(reqParam.get("MAILADDR") != null)
				reqParam.put("MAILADDR", reqParam.get("MAILADDR").toLowerCase());
			bs = NB30_REST(reqParam);
		}catch(Exception e) {
			log.error(e.toString());
		}
		return bs;
	}
	/**
	 * 查詢分行回傳全部資料
	 * @param 
	 * @return
	 */
	public List getAllBh() {
		List all = null;
		try {
			all = admBranchDao.getAll();
		} catch (Exception e) {
			log.warn("無法取得分行資料.", e);
		}
		return all;
	}
	
	/**
	 * 取得縣市別
	 * @param 
	 * @return
	 */
	public List getCity() {
		List result = admZipDataDao.findCity();
		return result;
	}
	
	/**
	 * 取得區域
	 * @param 
	 * @return
	 */
	public BaseResult getArea(final String city) {
		BaseResult bs = new BaseResult();
		List result = admZipDataDao.findArea(city);
		bs.addData("Area", result);
		return bs;
	}	
	
	/**
	 * 取得郵遞區號依據縣市及區域
	 * @param 
	 * @return
	 */
	public BaseResult getZipCodeByCityArea(final String city, final String area) {
		BaseResult bs = new BaseResult();
		String result = admZipDataDao.findZipBycityarea(city, area);
		bs.addData("Zip", result);
		return bs;
	}		
	
	/**
	 * 查詢分行(郵遞區號內)
	 * @param 
	 * @return
	 */
	public BaseResult getZipBh(String ZIPCODE) {
		BaseResult bs = new BaseResult();
		log.debug("zip >>{}", ZIPCODE);
		List result = admBranchDao.findZipBh(ZIPCODE); 
		if(result.size() <= 0) {
			result = getAllBh();
		}
		bs.addData("bh", result);
		return bs;
	}
	/**
	 * 一般網銀分行聯絡方式資料檔擷取
	 * @param bhid
	 * @return
	 */	
	public List getAdmBhContact(String bhid) {

		List result = admBranchDao.findByBhID(bhid);
			
		return result;
	}
	
	public BaseResult verify_lo_account_aj(Map<String, String> reqParam){
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = N953_REST(reqParam.get("LOATTIAC"));
			if(bs.getResult()) {
				
				String cusidn = reqParam.get("CUSIDN") == null ? "" : reqParam.get("CUSIDN");
				Map<String, Object> bsData = (Map) bs.getData();
				String cusidnN953 = String.valueOf( bsData.get("CUSIDN") );
				log.debug(ESAPIUtil.vaildLog("checkIdentity.cusidn >> " + cusidn));
				log.debug("checkIdentity.cusidnN953: {}", cusidnN953);
				if(!cusidn.equals(cusidnN953)) {
					bs.reset();
					bs.setResult(false);
					bs.setMsgCode("FE0016");
					this.getMessageByMsgCode(bs);
//					bs.setMessage("請先確認您的帳號是否為本人持有");//非本人帳戶之晶片金融卡
					return bs;
				}
				String REJCOD = bsData.get("REJCOD") == null ? "": (String)bsData.get("REJCOD");
				if(REJCOD.equals("Y")) {
					bs.reset();
					bs.setResult(false);
					bs.setMsgCode("FE0017");
					this.getMessageByMsgCode(bs);
//					bs.setMessage("請先確認您的帳號是否為有效帳號");//非本人帳戶之晶片金融卡
					return bs;
				}
				
				reqParam.put("NCHECKNB", "Y");
				reqParam.put("BRHACC", reqParam.get("LOATTIAC").substring(0, 3));
				bs = N960_BRHACC_REST(reqParam);

				String mobtel = (String)((Map<String, Object>)bs.getData()).get("MOBTEL");
				if(mobtel == null || "".equals(mobtel)) {
					bs.reset();
					bs.setResult(false);
					bs.setMsgCode("FE0018");
					this.getMessageByMsgCode(bs);
//					bs.setMessage("您尚未於本行留存行動電話，請先更新您的資料。");
					return bs;
				}
				
				//寄送簡訊
				reqParam.put("FuncType", "0");			
				reqParam.put("CUSIDN", cusidn);
				reqParam.put("UID", cusidn);
				reqParam.put("OTP", "");
				reqParam.put("MOBTEL",mobtel);
				reqParam.put("SMSADOPID", "N203A4");
				reqParam.put("MSGTYPE", "N203A4");
				bs = Send_Otp_REST(reqParam);
				
				//整理回傳值
				List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
				log.trace("Get bsdata Send_SmsOtp_REST>>>{}",bs.getData());
				Map<String, String>datamap = new HashMap();
				if(bs==null || !bs.getResult()) {
					bs.addData("MSGCOD", bs.getMsgCode());
				}
				Map<String, String>listmap = (Map) bs.getData();
				resultList.add(listmap);
				log.trace("resultList>>>{}",resultList);
				bs.setData(resultList);
			}
			
		}catch(Exception e) {
			log.error("verify_lo_account_aj EXCEPTION:"+e.getMessage());
			bs.setResult(false);
		}
		return bs;
	}
	
	public BaseResult validate_sms_otp(Map<String, String> reqParam) {

		BaseResult bs = new BaseResult();
		try {

			// 要先驗證簡訊
			reqParam.put("FuncType", "1");
			reqParam.put("OTP", reqParam.get("SMSOTP"));
			reqParam.put("ADOPID", "N203A4");

			bs = Send_Otp_REST(reqParam);
			if (bs.getResult()) {
				log.info("簡訊驗證通過...");
			} else {
				log.error("簡訊驗證失敗...");
			}
		} catch (Exception e) {
			log.error("IdGateN915 Error >>{}", e);
		}
		return bs;
	}
	
	public String getUserBHID(String CUSIDN) {
		List<TXNUSER> user = null;
		String branchId = "";
		try {
			user = txnUserDao.findByUserId(CUSIDN);		
			if(user==null)
			{
				log.warn(ESAPIUtil.vaildLog("User ID not found = " + CUSIDN));
			}
			else
			{
				branchId=user.get(0).getADBRANCHID();
			}
		
		}catch (Exception e) {
			log.warn("TXNUSER EXCEPTION:"+e.getMessage());
		}
		log.debug(ESAPIUtil.vaildLog("branchId >> {}" + branchId));
		return branchId;
	}
	
	/**
	 * 上傳圖片至FTP
	 * @param filePath
	 * @param fileName
	 */
	public void uploadImage(String filePath,String fileName) {
		TXNLOANAPPLY attrib = new TXNLOANAPPLY();
		FTPUtil ftputil = new FTPUtil();
		Base64 base64 = new Base64();
		try {
			//修改 Absolute Path Traversal
		    String validFileName = ESAPIUtil.GetValidPathPart(filePath,fileName);
			try {
				byte[] bytes = Files.readAllBytes(Paths.get(validFileName));
				
				String isSessionID = SpringBeanFactory.getBean("isSessionID");
				//LOCAL
				if("N".equals(isSessionID)){
					FTPClient ftpclient = ftputil.connect("localhost",21,"root","123456",30000);
					boolean result = ftputil.uploadFile(ftpclient,fileName,bytes);
					ftputil.finish(ftpclient);
					
				}else{
					Map<String, String> ftp = SpringBeanFactory.getBean("ftp");
					String use = ftp.get("use");
					String ip = ftp.get("ip");
					int port = Integer.parseInt(ftp.get("port"));
					String name = ftp.get("name");
					String pw = ftp.get("password");
					String path = ftp.get("path");
					String decodeString = new String(base64.decode(pw.getBytes()));
					int timeout = Integer.parseInt(ftp.get("timeout"));
					if("Y".equals(use)){
			        	FTPClient ftpclient = ftputil.connect(ip,port,name,decodeString,timeout);
						ftputil.ChangeToDirectory(ftpclient,path);
						String FILE1 = fileName;
						log.debug(ESAPIUtil.vaildLog("FILE1={}"+FILE1));
						
						boolean result = ftputil.uploadFile(ftpclient,FILE1,bytes);
						log.info(ESAPIUtil.vaildLog("「"+FILE1+"」上傳"+ result));
						ftputil.finish(ftpclient);
		
			        }
				}
			}catch(Exception e){
				log.error("uploadImage error >> {}", e.toString());
				log.warn(ESAPIUtil.vaildLog("圖檔上傳失敗"+fileName));
			}
		}catch(Exception e) {
			log.error("uploadImage error >> {}", e.toString());
			log.warn(ESAPIUtil.vaildLog("圖檔上傳失敗"+fileName));
		}
	}
	/**
	 * 刪除圖片
	 * @param localPath
	 * @return
	 */
	public boolean removefile(String rootPath,String fileName, String movePath, String moveFileName) {
		String validatePath = "";
		String validateMovePath = "";
		try
		{
			//修改 Absolute Path Traversal
			validatePath = ESAPIUtil.GetValidPathPart(rootPath,fileName);
			//log.trace("movePath >> {}, moveFileName >> {}",movePath, moveFileName);
			if(!movePath.equals("") && !moveFileName.equals("")) {
				validateMovePath = ESAPIUtil.vaildPathTraversal2(movePath,moveFileName);
			}
			//log.trace("validateMovePath >> {}",validateMovePath);
			try {
				//修改 Incorrect Permission Assignment For Critical Resources
				if(!validateMovePath.equals("")) {
					// CheckMax版本提升後又被掃出Incorrect Permission Assignment For Critical Resources
//					Files.copy(Paths.get(validatePath), Paths.get(validateMovePath));
					
					// 修正Incorrect Permission Assignment For Critical Resources
					// 創建母目錄如果目錄不存在
					File file = new File(movePath);
					file.setWritable(true,true);
					file.setReadable(true,true);
					file.setExecutable(true,true);
					file.mkdirs();
					
					// 讀取source檔案
					byte[] srcFile = Files.readAllBytes(Paths.get(validatePath));
					
					// 產生目標檔案
					BufferedOutputStream fw = new BufferedOutputStream(new FileOutputStream(validateMovePath));
			        fw.write(srcFile);
			        fw.flush();
			        fw.close();
					
					log.info(ESAPIUtil.vaildLog("複製圖檔成功:"+validatePath));
				}
				Files.delete(Paths.get(validatePath));
				log.info(ESAPIUtil.vaildLog("刪除圖檔成功:"+validatePath));
				return(true);
			}
			catch(Exception RemoveFileEx)
			{
				log.warn("刪除圖檔失敗"+validatePath);
				return(false);
			}		
		}
		catch(Exception RemoveFileEx)
		{
			log.warn("刪除圖檔失敗"+validatePath);
			return(false);
		}		
	}


	public BaseResult upload_digital_identity_2041(Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = N2041_REST(reqParam);
			if(bs != null && bs.getResult()) {

			}
		}catch(Exception e) {
			log.error(e.toString());
		}
		return bs;
	}
	
	public BaseResult upload_digital_identity_2042(Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = N2042_REST(reqParam);
			if(bs != null && bs.getResult()) {

			}
		}catch(Exception e) {
			log.error(e.toString());
		}
		return bs;
	}
	

	public BaseResult upload_digital_identity_result(Map<String, String> reqParam, String uploadPath) {
		String uploadPath_N201 = uploadPath;
		String separ = File.separator;
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = N204_REST(reqParam);
			if(bs != null && bs.getResult()) {

				List bh = getAdmBhContact(reqParam.get("BRHCOD"));
				log.debug(ESAPIUtil.vaildLog("bh >> {}"+bh.get(0)));
				bs.addData("BRHNAME", ((ADMBRANCH)bh.get(0)).getADBRANCHNAME());
				bs.addData("BRHTEL", ((ADMBRANCH)bh.get(0)).getTELNUM());
//				if(!reqParam.get("FILE1").equals("")) {
//					String filetype = reqParam.get("FILE1").substring(reqParam.get("FILE1").lastIndexOf(".") ,reqParam.get("FILE1").length());
//					uploadImage(uploadPath_N201, reqParam.get("FILE1"));
//					removefile(uploadPath_N201, reqParam.get("FILE1"), SpringBeanFactory.getBean("imgMovePath") + separ, reqParam.get("CUSIDN") + "_01_" + DateUtil.getDate("") + filetype);
//				}
//				if(!reqParam.get("FILE2").equals("")) {
//					String filetype = reqParam.get("FILE2").substring(reqParam.get("FILE2").lastIndexOf(".") ,reqParam.get("FILE2").length());
//					uploadImage(uploadPath_N201, reqParam.get("FILE2"));
//					removefile(uploadPath_N201, reqParam.get("FILE2"), SpringBeanFactory.getBean("imgMovePath") + separ, reqParam.get("CUSIDN") + "_02_" + DateUtil.getDate("") + filetype);
//				}
//				if(!reqParam.get("FILE3").equals("")) {
//					String filetype = reqParam.get("FILE3").substring(reqParam.get("FILE3").lastIndexOf(".") ,reqParam.get("FILE3").length());
//					uploadImage(uploadPath_N201, reqParam.get("FILE3"));
//					removefile(uploadPath_N201, reqParam.get("FILE3"), SpringBeanFactory.getBean("imgMovePath") + separ, reqParam.get("CUSIDN") + "_03_" + DateUtil.getDate("") + filetype);
//				}
//				if(!reqParam.get("FILE4").equals("")) {
//					String filetype = reqParam.get("FILE4").substring(reqParam.get("FILE4").lastIndexOf(".") ,reqParam.get("FILE4").length());
//					uploadImage(uploadPath_N201, reqParam.get("FILE4"));
//					removefile(uploadPath_N201, reqParam.get("FILE4"), SpringBeanFactory.getBean("imgMovePath") + separ, reqParam.get("CUSIDN") + "_04_" + DateUtil.getDate("") + filetype);
//				}
				
			}
		}catch(Exception e) {
			log.error(e.toString());
		}
		return bs;
	}
	
	public BaseResult modify_digital_data_p1(Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = N104_REST(reqParam);
			if(bs != null && bs.getResult()) {

			}
		}catch(Exception e) {
			log.error(e.toString());
		}
		return bs;
	}
	
	public BaseResult modify_digital_data_p2(Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			bs = N104_1_REST(reqParam);
			log.trace("modify_digital_data_p2.bs P: {}", CodeUtil.toJson(bs));
			if(bs != null && bs.getResult()) {
				Map<String, Object> callData = (Map<String, Object>)bs.getData();
				bs.addData("ZIP1", callData.get("POSTCOD1"));
				bs.addData("ZIP2", callData.get("POSTCOD2"));
				String BIRTHDAY = (String)callData.get("BIRTHDAY");
				bs.addData("CCBIRTHDATEYY", BIRTHDAY.length() >= 3 ? BIRTHDAY.substring(0, 3) : "");
				bs.addData("CCBIRTHDATEMM", BIRTHDAY.length() >= 5 ? BIRTHDAY.substring(3, 5) : "");
				bs.addData("CCBIRTHDATEDD", BIRTHDAY.length() >= 7 ? BIRTHDAY.substring(5, 7) : "");
				String PMTADR = (String)callData.get("PMTADR");
				String CTTADR = (String)callData.get("CTTADR");
				bs.addData("CITY1", PMTADR.length() >= 3 ? PMTADR.substring(0, 3) : "#");
				bs.addData("CITY2", CTTADR.length() >= 3 ? CTTADR.substring(0, 3) : "#");
				bs.addData("CITY3", ((String)callData.get("CITY2")).length() >= 3 ? (String)callData.get("CITY2") : "#");
				if(PMTADR.length() > 0) {
					if( PMTADR.substring(2, 3).equals("市")) {
						PMTADR = PMTADR.length() > 3 ? PMTADR.substring(3) : PMTADR;
						bs.addData("ADDR1", PMTADR.indexOf("區")>-1 ? PMTADR.substring(PMTADR.indexOf("區")+1) : 
											PMTADR.indexOf("東沙群島")>-1 ? PMTADR.substring(PMTADR.indexOf("島")+1) : 
											PMTADR.indexOf("南沙群島")>-1 ? PMTADR.substring(PMTADR.indexOf("島")+1) : PMTADR.substring(3));
						bs.addData("ZONE1", PMTADR.length() >= 6 ? PMTADR.substring(0, 	PMTADR.indexOf("區")>-1 ? PMTADR.indexOf("區")+1 :  
																						PMTADR.indexOf("東沙群島")>-1 ? PMTADR.indexOf("島")+1 : 
																						PMTADR.indexOf("南沙群島")>-1 ? PMTADR.indexOf("島")+1 : 3) : "#");
					}else {
						PMTADR = PMTADR.length() > 3 ? PMTADR.substring(3) : PMTADR;
						bs.addData("ADDR1", PMTADR.indexOf("市")>-1 ? PMTADR.substring(PMTADR.indexOf("市")+1) : 
											PMTADR.indexOf("鎮")>-1 ? PMTADR.substring(PMTADR.indexOf("鎮")+1) : 
											PMTADR.indexOf("鄉")>-1 ? PMTADR.substring(PMTADR.indexOf("鄉")+1) : 
											PMTADR.indexOf("釣魚臺列嶼")>-1 ? PMTADR.substring(PMTADR.indexOf("嶼")+1) : PMTADR.substring(3));
						bs.addData("ZONE1", PMTADR.length() >= 6 ? PMTADR.substring(0, 	PMTADR.indexOf("市")>-1 ? PMTADR.indexOf("市")+1 : 
																						PMTADR.indexOf("鎮")>-1 ? PMTADR.indexOf("鎮")+1 : 
																						PMTADR.indexOf("鄉")>-1 ? PMTADR.indexOf("鄉")+1 : 
																						PMTADR.indexOf("釣魚臺列嶼")>-1 ? PMTADR.indexOf("嶼")+1 : 3) : "#");
					}
//					bs.addData("ADDR11",PMTADR.indexOf("村")>-1 ? PMTADR.substring(PMTADR.indexOf("村")-2,PMTADR.indexOf("村")) : "");
//					bs.addData("ADDR12",PMTADR.indexOf("里")>-1 ? PMTADR.substring(PMTADR.indexOf("里")-2,PMTADR.indexOf("里")) : "");	
//					bs.addData("ADDR13",(PMTADR.indexOf("里")>-1 && PMTADR.indexOf("鄰")>-1) ? PMTADR.substring(PMTADR.indexOf("里")+1,PMTADR.indexOf("鄰")) : (PMTADR.indexOf("鄰")>-1 ? PMTADR.substring(PMTADR.indexOf("鄰")-1,PMTADR.indexOf("鄰")) : ""));	
											
				}
				if(CTTADR.length() > 0) {
					if( CTTADR.substring(2, 3).equals("市")) {
						CTTADR = CTTADR.length() > 3 ? CTTADR.substring(3) : CTTADR;
						bs.addData("ADDR2", CTTADR.indexOf("區")>-1 ? CTTADR.substring(CTTADR.indexOf("區")+1) : 
											CTTADR.indexOf("東沙群島")>-1 ? CTTADR.substring(CTTADR.indexOf("島")+1) :  
											CTTADR.indexOf("南沙群島")>-1 ? CTTADR.substring(CTTADR.indexOf("島")+1) : CTTADR.substring(3));
						bs.addData("ZONE2", CTTADR.length() >= 6 ? CTTADR.substring(0, 	CTTADR.indexOf("區")>-1 ? CTTADR.indexOf("區")+1 : 
																						CTTADR.indexOf("東沙群島")>-1 ? CTTADR.indexOf("島")+1 : 
																						CTTADR.indexOf("南沙群島")>-1 ? CTTADR.indexOf("島")+1 : 3) : "#");
					}else {
						CTTADR = CTTADR.length() > 3 ? CTTADR.substring(3) : CTTADR;
						bs.addData("ADDR2", CTTADR.indexOf("市")>-1 ? CTTADR.substring(CTTADR.indexOf("市")+1) : 
											CTTADR.indexOf("鎮")>-1 ? CTTADR.substring(CTTADR.indexOf("鎮")+1) : 
											CTTADR.indexOf("鄉")>-1 ? CTTADR.substring(CTTADR.indexOf("鄉")+1) : 
											CTTADR.indexOf("釣魚臺列嶼")>-1 ? CTTADR.substring(CTTADR.indexOf("嶼")+1) : CTTADR.substring(3));
						bs.addData("ZONE2", CTTADR.length() >= 6 ? CTTADR.substring(0, 	CTTADR.indexOf("市")>-1 ? CTTADR.indexOf("市")+1 : 
																						CTTADR.indexOf("鎮")>-1 ? CTTADR.indexOf("鎮")+1 : 
																						CTTADR.indexOf("鄉")>-1 ? CTTADR.indexOf("鄉")+1 : 
																						CTTADR.indexOf("釣魚臺列嶼")>-1 ? CTTADR.indexOf("嶼")+1 : 3) : "#");
					}
//					bs.addData("ADDR21",CTTADR.indexOf("村")>-1 ? CTTADR.substring(CTTADR.indexOf("村")-2,CTTADR.indexOf("村")) : "");
//					bs.addData("ADDR22",CTTADR.indexOf("里")>-1 ? CTTADR.substring(CTTADR.indexOf("里")-2,CTTADR.indexOf("里")) : "");	
//					bs.addData("ADDR23",(CTTADR.indexOf("里")>-1 && CTTADR.indexOf("鄰")>-1) ? CTTADR.substring(CTTADR.indexOf("里")+1,CTTADR.indexOf("鄰")) : (CTTADR.indexOf("鄰")>-1 ? CTTADR.substring(CTTADR.indexOf("鄰")-1,CTTADR.indexOf("鄰")) : ""));				
				}
				if(((String)callData.get("HOMETEL")).length() > 0) {
					bs.addData("HOMETEL",((String)callData.get("HOMETEL")).indexOf("#")>-1 ? 
							((String)callData.get("HOMETEL")).substring(0,((String)callData.get("HOMETEL")).indexOf("#")) : ((String)callData.get("HOMETEL")));
				}
				String BUSTEL = (String)callData.get("BUSTEL");
				if(BUSTEL.length() > 0) {
					bs.addData("BUSTEL11",BUSTEL.indexOf("#")>-1 ? BUSTEL.substring(0,BUSTEL.indexOf("#")) :  BUSTEL);
					bs.addData("BUSTEL12",BUSTEL.indexOf("#")>-1 ? BUSTEL.substring(BUSTEL.indexOf("#")+1) :  "");
				}
//				bs.addData("ZONE1", PMTADR.length() >= 6 ? PMTADR.substring(3, 6) : "#");
//				bs.addData("ADDR1", PMTADR.length() > 6 ? PMTADR.substring(6) : "");
//				bs.addData("ZONE2", CTTADR.length() >= 6 ? CTTADR.substring(3, 6) : "#");
//				bs.addData("ADDR2", CTTADR.length() > 6 ? CTTADR.substring(6) : "");
				bs.addData("ZONE3", ((String)callData.get("ZONE2")).length() >= 3 ? (String)callData.get("ZONE2") : "#");
				
				//因客戶輸入之單位千元改萬元，顯示須調整數值為萬元單位，從這裡除10
				int salary_in = Integer.valueOf(String.valueOf(callData.get("SALARY"))) / 10;
				String salary2_in = String.valueOf(salary_in);

				bs.addData("SALARY", NumericUtil.fmtAmount((String)salary2_in, 0));
				String SMONEY = NumericUtil.fmtAmount((String)callData.get("SMONEY"), 0);
				SMONEY = SMONEY.equals("0") ? "" : SMONEY;
				bs.addData("SMONEY", SMONEY);
				log.trace("modify_digital_data_p2.bs A: {}", CodeUtil.toJson(bs));
				log.debug("ISCUSIDN >> {}",admEmpInfoDao.isEmpCUSID(reqParam.get("CUSIDN")));
				bs.addData("ISCUSIDN", admEmpInfoDao.isEmpCUSID(reqParam.get("CUSIDN")) ? "Y":"N");
			}
		}catch(Exception e) {
			log.error(e.toString());
		}
		return bs;
	}
	

	public BaseResult modify_digital_data_result(Map<String, String> reqParam) {
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			reqParam.put("SALARY", reqParam.get("SALARY").replace(",", ""));
			
			log.debug("SALARY : " + reqParam.get("SALARY"));
			//因客戶輸入之單位千元改萬元，後送須調整數值回千元單位，從這裡乘10
			int salary_out = Integer.valueOf(String.valueOf(reqParam.get("SALARY"))) * 10;
			String salary2_out = String.valueOf(salary_out);
			reqParam.put("SALARY", salary2_out);
						
			if(reqParam.get("ACNNO").length() > 11)
				reqParam.put("CHIP_ACN", reqParam.get("ACNNO").substring(reqParam.get("ACNNO").length()-11));
			if(reqParam.get("MAILADDR") != null)
				reqParam.put("MAILADDR", reqParam.get("MAILADDR").toLowerCase());
			bs = N104_2_REST(reqParam);
			String errMsg = (String)((Map<String,Object>)bs.getData()).get("errMsg");
			log.debug(errMsg);
			if (!("R000".equals(errMsg) || "OKLR".equals(errMsg) || "OKOK".equals(errMsg) || "".equals(errMsg) || "0".equals(errMsg) 
					||"0000000".equals(errMsg)|| "0000".equals(errMsg)||"OKOV".equals(errMsg) || errMsg == null)) {
				bs.setResult(false);
				bs.setMessage(errMsg, (String)((Map<String,Object>)bs.getData()).get("errMsgStr"));
			}
			if(bs != null && bs.getResult()) {

				Map<String,Object> callData = (Map<String,Object>)bs.getData();
				List bh = getAdmBhContact((String)callData.get("BRHCOD"));
				log.debug(ESAPIUtil.vaildLog("bh >> "+bh.get(0)));
				bs.addData("BRHNAME", ((ADMBRANCH)bh.get(0)).getADBRANCHNAME());
				bs.addData("BRHTEL", ((ADMBRANCH)bh.get(0)).getTELNUM());
				
			}
		}catch(Exception e) {
			log.error(e.toString());
		}
		return bs;
	}
}
