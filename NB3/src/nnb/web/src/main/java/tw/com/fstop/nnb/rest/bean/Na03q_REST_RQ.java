package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * NA03Q電文RQ
 */
public class Na03q_REST_RQ extends BaseRestBean_CC implements Serializable{
	private static final long serialVersionUID = 6868861910004819383L;
	
	private String UID;
	private String FGTXWAY;
	private String CPRIMBIRTHDAY;
	private String pkcs7Sign;
	private String jsondc;
	private String CARDNUM1;
	private String CARDNUM2;
	private String CARDNUM3;
	private String CARDNUM4;
	private String EXPIREDYEAR;
	private String EXPIREDDATEMONTHS;
	
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getCPRIMBIRTHDAY() {
		return CPRIMBIRTHDAY;
	}
	public void setCPRIMBIRTHDAY(String cPRIMBIRTHDAY) {
		CPRIMBIRTHDAY = cPRIMBIRTHDAY;
	}
	public String getPkcs7Sign() {
		return pkcs7Sign;
	}
	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}
	public String getJsondc() {
		return jsondc;
	}
	public void setJsondc(String jsondc) {
		this.jsondc = jsondc;
	}
	public String getCARDNUM1() {
		return CARDNUM1;
	}
	public void setCARDNUM1(String cARDNUM1) {
		CARDNUM1 = cARDNUM1;
	}
	public String getCARDNUM2() {
		return CARDNUM2;
	}
	public void setCARDNUM2(String cARDNUM2) {
		CARDNUM2 = cARDNUM2;
	}
	public String getCARDNUM3() {
		return CARDNUM3;
	}
	public void setCARDNUM3(String cARDNUM3) {
		CARDNUM3 = cARDNUM3;
	}
	public String getCARDNUM4() {
		return CARDNUM4;
	}
	public void setCARDNUM4(String cARDNUM4) {
		CARDNUM4 = cARDNUM4;
	}
	public String getEXPIREDYEAR() {
		return EXPIREDYEAR;
	}
	public void setEXPIREDYEAR(String eXPIREDYEAR) {
		EXPIREDYEAR = eXPIREDYEAR;
	}
	public String getEXPIREDDATEMONTHS() {
		return EXPIREDDATEMONTHS;
	}
	public void setEXPIREDDATEMONTHS(String eXPIREDDATEMONTHS) {
		EXPIREDDATEMONTHS = eXPIREDDATEMONTHS;
	}
}