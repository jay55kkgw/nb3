package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N076_REST_RS extends BaseRestBean implements Serializable {
	
	
	private static final long serialVersionUID = -2134963226245849259L;
	

	private String OFFSET;// 空白

	private String HEADER;// HEADER

	private String SYNC;// Sync.Check Item

	private String OUTACN;// 轉出帳號

	private String FDPACN;// 存單帳號

	private String FDPNUM;// 存單號碼

	private String AMOUNT;// 存單金額

	private String TERMS;// 總期數

	private String TOTAMT;// 已存入總金數

	private String RETERMS;// 剩餘期數

	private String TOTBAL;// 帳上餘額

	private String AVLBAL;// 可用餘額

	private String TRNDATE;// 交易日期YYYMMDD

	private String TRNTIME;// 交易時間HHMMSS
	
    private String CMQTIME;

    private String CMTXTIME;

	private String MAC;// MAC

	public String getOFFSET()
	{
		return OFFSET;
	}

	public void setOFFSET(String oFFSET)
	{
		OFFSET = oFFSET;
	}

	public String getHEADER()
	{
		return HEADER;
	}

	public void setHEADER(String hEADER)
	{
		HEADER = hEADER;
	}

	public String getSYNC()
	{
		return SYNC;
	}

	public void setSYNC(String sYNC)
	{
		SYNC = sYNC;
	}

	public String getOUTACN()
	{
		return OUTACN;
	}

	public void setOUTACN(String oUTACN)
	{
		OUTACN = oUTACN;
	}

	public String getFDPACN()
	{
		return FDPACN;
	}

	public void setFDPACN(String fDPACN)
	{
		FDPACN = fDPACN;
	}

	public String getFDPNUM()
	{
		return FDPNUM;
	}

	public void setFDPNUM(String fDPNUM)
	{
		FDPNUM = fDPNUM;
	}

	public String getAMOUNT()
	{
		return AMOUNT;
	}

	public void setAMOUNT(String aMOUNT)
	{
		AMOUNT = aMOUNT;
	}

	public String getTERMS()
	{
		return TERMS;
	}

	public void setTERMS(String tERMS)
	{
		TERMS = tERMS;
	}

	public String getTOTAMT()
	{
		return TOTAMT;
	}

	public void setTOTAMT(String tOTAMT)
	{
		TOTAMT = tOTAMT;
	}

	public String getRETERMS()
	{
		return RETERMS;
	}

	public void setRETERMS(String rETERMS)
	{
		RETERMS = rETERMS;
	}

	public String getTOTBAL()
	{
		return TOTBAL;
	}

	public void setTOTBAL(String tOTBAL)
	{
		TOTBAL = tOTBAL;
	}

	public String getAVLBAL()
	{
		return AVLBAL;
	}

	public void setAVLBAL(String aVLBAL)
	{
		AVLBAL = aVLBAL;
	}

	public String getTRNDATE()
	{
		return TRNDATE;
	}

	public void setTRNDATE(String tRNDATE)
	{
		TRNDATE = tRNDATE;
	}

	public String getTRNTIME()
	{
		return TRNTIME;
	}

	public void setTRNTIME(String tRNTIME)
	{
		TRNTIME = tRNTIME;
	}

	public String getCMQTIME()
	{
		return CMQTIME;
	}

	public void setCMQTIME(String cMQTIME)
	{
		CMQTIME = cMQTIME;
	}

	public String getCMTXTIME()
	{
		return CMTXTIME;
	}

	public void setCMTXTIME(String cMTXTIME)
	{
		CMTXTIME = cMTXTIME;
	}

	public String getMAC()
	{
		return MAC;
	}

	public void setMAC(String mAC)
	{
		MAC = mAC;
	}

}
