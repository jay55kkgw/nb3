package tw.com.fstop.nnb.spring.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value="/TRUST")
public class Trust_Controller {
	
	@RequestMapping(value = "/changepw")
	public String changepw(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		String target = "/trust/changepw";
		return target;
	}
	
	@RequestMapping(value = "/changepw_result")
	public String changepw_result(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		String target = "/trust/changepw_result";
		return target;
	}

	
}
