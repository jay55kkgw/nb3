package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N615_REST_RS extends BaseRestBean implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1463058301290929722L;
	String ACN	;
	String CMRECNUM ;
	String ABEND ;
	String REC_NO ;
	String CMPERIOD ;
	String USERDATA_X100 ;
	String CMQTIME ;
	String __OCCURS ;
	String FILLER_X83 ;
	LinkedList<N615_REST_RSDATA> REC;
	
	
	
	public LinkedList<N615_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<N615_REST_RSDATA> rEC) {
		REC = rEC;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getCMRECNUM() {
		return CMRECNUM;
	}
	public void setCMRECNUM(String cMRECNUM) {
		CMRECNUM = cMRECNUM;
	}
	public String getABEND() {
		return ABEND;
	}
	public void setABEND(String aBEND) {
		ABEND = aBEND;
	}
	public String getREC_NO() {
		return REC_NO;
	}
	public void setREC_NO(String rEC_NO) {
		REC_NO = rEC_NO;
	}
	public String getCMPERIOD() {
		return CMPERIOD;
	}
	public void setCMPERIOD(String cMPERIOD) {
		CMPERIOD = cMPERIOD;
	}
	public String getUSERDATA_X100() {
		return USERDATA_X100;
	}
	public void setUSERDATA_X100(String uSERDATA_X100) {
		USERDATA_X100 = uSERDATA_X100;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String get__OCCURS() {
		return __OCCURS;
	}
	public void set__OCCURS(String __oCCURS) {
		__OCCURS = __oCCURS;
	}
	public String getFILLER_X83() {
		return FILLER_X83;
	}
	public void setFILLER_X83(String fILLER_X83) {
		FILLER_X83 = fILLER_X83;
	}
	
	

}
