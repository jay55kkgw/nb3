package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;


public class N070A_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5370773529503299761L;
	
		// 轉帳日期
		@SerializedName(value = "transfer_date")
		private String transfer_date;
		
		// 轉出帳號
		@SerializedName(value = "OUTACN")
		private String OUTACN;	
		
		// 銷帳編號
		@SerializedName(value = "TSFACN")
		private String TSFACN;	
		
		// 繳款金額
		@SerializedName(value = "IDGATE_AMOUNT")
		private String IDGATE_AMOUNT;
		
		// 交易類別
		@SerializedName(value = "FGTXDATE")
		private String FGTXDATE;
		
		@Override
		public Map<String, String> coverMap(){
			Map<String, String> result = new LinkedHashMap<String, String>();
			
			String fgtxdate = "";
			if("1".equals(this.FGTXDATE)) 
				 fgtxdate = "即時繳費";
			else if("2".equals(this.FGTXDATE))
				 fgtxdate = "預約繳費";
			result.put("交易名稱", "繳費-學雜費");
			result.put("交易類別", fgtxdate);
			result.put("轉帳日期", this.transfer_date);
			result.put("轉出帳號", this.OUTACN);
			result.put("銷帳編號", this.TSFACN);
			result.put("繳款金額","新台幣 "+this.IDGATE_AMOUNT+"元");
			return result;
		}

		public String getTransfer_date() {
			return transfer_date;
		}

		public void setTransfer_date(String transfer_date) {
			this.transfer_date = transfer_date;
		}

		public String getOUTACN() {
			return OUTACN;
		}

		public void setOUTACN(String oUTACN) {
			OUTACN = oUTACN;
		}

		public String getTSFACN() {
			return TSFACN;
		}

		public void setTSFACN(String tSFACN) {
			TSFACN = tSFACN;
		}

		public String getIDGATE_AMOUNT() {
			return IDGATE_AMOUNT;
		}

		public void setIDGATE_AMOUNT(String iDGATE_AMOUNT) {
			IDGATE_AMOUNT = iDGATE_AMOUNT;
		}

		public String getFGTXDATE() {
			return FGTXDATE;
		}

		public void setFGTXDATE(String fGTXDATE) {
			FGTXDATE = fGTXDATE;
		}
		
		
}
