package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N205_REST_RSDATA implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 143635540095579971L;
	
	
	private String NAME;		// 戶名
	private String ROMENAME;	// 羅馬拼音姓名
	private String ACN;			// 帳號
	private String BRHACC;		// 分行
	private String TAXGVID;		// 分支代號
	private String CHANGE;		// 是否已轉為一般戶
	private String SDT;			// 開戶日期
	
	
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getROMENAME() {
		return ROMENAME;
	}
	public void setROMENAME(String rOMENAME) {
		ROMENAME = rOMENAME;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getBRHACC() {
		return BRHACC;
	}
	public void setBRHACC(String bRHACC) {
		BRHACC = bRHACC;
	}
	public String getTAXGVID() {
		return TAXGVID;
	}
	public void setTAXGVID(String tAXGVID) {
		TAXGVID = tAXGVID;
	}
	public String getCHANGE() {
		return CHANGE;
	}
	public void setCHANGE(String cHANGE) {
		CHANGE = cHANGE;
	}
	public String getSDT() {
		return SDT;
	}
	public void setSDT(String sDT) {
		SDT = sDT;
	}
	
}
