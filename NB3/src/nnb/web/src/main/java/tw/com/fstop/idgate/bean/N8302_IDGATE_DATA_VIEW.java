package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;


public class N8302_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5370773529503299761L;
	
	@SerializedName(value = "TSFACN")
	private String TSFACN;

	@SerializedName(value = "CUSNUM")
	private String CUSNUM;
	
	@Override
	public Map<String, String> coverMap(){
		LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
        result.put("交易名稱", "勞保費用代扣繳申請");
        result.put("交易類型", "申請");
		result.put("扣帳帳號", this.TSFACN);
		result.put("用戶編號（保險證字號）", this.CUSNUM);
		return result;
	}

	public String getTSFACN() {
		return TSFACN;
	}

	public void setTSFACN(String tSFACN) {
		TSFACN = tSFACN;
	}
}
