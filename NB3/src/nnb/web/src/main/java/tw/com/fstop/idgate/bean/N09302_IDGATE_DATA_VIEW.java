package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

import tw.com.fstop.tbb.nnb.util.StrUtils;


public class N09302_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4889085384946526052L;

	@SerializedName(value = "SVACN")
	private String SVACN;
	@SerializedName(value = "ACN")
	private String ACN;
	@SerializedName(value = "AMT_06_N1")
	private String AMT_06_N1;
	@SerializedName(value = "AMT_16_N1")
	private String AMT_16_N1;
	@SerializedName(value = "AMT_26_N1")
	private String AMT_26_N1;
	@SerializedName(value = "AMT_06_O1")
	private String AMT_06_O1;
	@SerializedName(value = "AMT_16_O1")
	private String AMT_16_O1;
	@SerializedName(value = "AMT_26_O1")
	private String AMT_26_O1;
	@SerializedName(value = "FLAG_06_O")
	private String FLAG_06_O;
	@SerializedName(value = "FLAG_16_O")
	private String FLAG_16_O;
	@SerializedName(value = "FLAG_26_O")
	private String FLAG_26_O;
	@SerializedName(value = "PAYSTATUS_06")
	private String PAYSTATUS_06;
	@SerializedName(value = "PAYSTATUS_16")
	private String PAYSTATUS_16;
	@SerializedName(value = "PAYSTATUS_26")
	private String PAYSTATUS_26;
	
	@Override
	public Map<String, String> coverMap(){
		LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", "黃金定期定額變更");
		result.put("交易類型", "-");
		result.put("約定扣款帳號", this.SVACN);
		result.put("黃金存摺帳號", this.ACN);
		result.put("原每月投資日/金額", "");
		result.put("06日新臺幣  ", this.AMT_06_O1 + " 元 " + status(this.FLAG_06_O));
		result.put("16日新臺幣  ", this.AMT_16_O1 + " 元 " + status(this.FLAG_16_O));
		result.put("26日新臺幣  ", this.AMT_26_O1 + " 元 " + status(this.FLAG_26_O));
		result.put("變更後每月投資日/金額", "");
		result.put("06日新臺幣 ", this.AMT_06_N1 + " 元 " + status(this.PAYSTATUS_06));
		result.put("16日新臺幣 ", this.AMT_16_N1 + " 元 " + status(this.PAYSTATUS_16));
		result.put("26日新臺幣 ", this.AMT_26_N1 + " 元 " + status(this.PAYSTATUS_26));
		return result;
	}

	public String getSVACN() {
		return SVACN;
	}

	public void setSVACN(String sVACN) {
		SVACN = sVACN;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	public String getAMT_06_N1() {
		return AMT_06_N1;
	}

	public void setAMT_06_N1(String aMT_06_N1) {
		AMT_06_N1 = aMT_06_N1;
	}

	public String getAMT_16_N1() {
		return AMT_16_N1;
	}

	public void setAMT_16_N1(String aMT_16_N1) {
		AMT_16_N1 = aMT_16_N1;
	}

	public String getAMT_26_N1() {
		return AMT_26_N1;
	}

	public void setAMT_26_N1(String aMT_26_N1) {
		AMT_26_N1 = aMT_26_N1;
	}
	
	public String getAMT_06_O1() {
		return AMT_06_O1;
	}

	public void setAMT_06_O1(String aMT_06_O1) {
		AMT_06_O1 = aMT_06_O1;
	}

	public String getAMT_16_O1() {
		return AMT_16_O1;
	}

	public void setAMT_16_O1(String aMT_16_O1) {
		AMT_16_O1 = aMT_16_O1;
	}

	public String getAMT_26_O1() {
		return AMT_26_O1;
	}

	public void setAMT_26_O1(String aMT_26_O1) {
		AMT_26_O1 = aMT_26_O1;
	}

	public String getFLAG_06_O() {
		return FLAG_06_O;
	}

	public void setFLAG_06_O(String fLAG_06_O) {
		FLAG_06_O = fLAG_06_O;
	}

	public String getFLAG_16_O() {
		return FLAG_16_O;
	}

	public void setFLAG_16_O(String fLAG_16_O) {
		FLAG_16_O = fLAG_16_O;
	}

	public String getFLAG_26_O() {
		return FLAG_26_O;
	}

	public void setFLAG_26_O(String fLAG_26_O) {
		FLAG_26_O = fLAG_26_O;
	}

	public String getPAYSTATUS_06() {
		return PAYSTATUS_06;
	}

	public void setPAYSTATUS_06(String pAYSTATUS_06) {
		PAYSTATUS_06 = pAYSTATUS_06;
	}

	public String getPAYSTATUS_16() {
		return PAYSTATUS_16;
	}

	public void setPAYSTATUS_16(String pAYSTATUS_16) {
		PAYSTATUS_16 = pAYSTATUS_16;
	}

	public String getPAYSTATUS_26() {
		return PAYSTATUS_26;
	}

	public void setPAYSTATUS_26(String pAYSTATUS_26) {
		PAYSTATUS_26 = pAYSTATUS_26;
	}

	public String status(String flag) {
		String chflag = "";
		if(StrUtils.isNotEmpty(flag)) {
			if(flag.equals("1")){
				chflag = "正常扣款";
			}
			else if(flag.equals("2")){
				chflag = "暫停扣款";
			}
			else if(flag.equals("3")){
				chflag = "終止扣款";
			}
			else {
				chflag = "";
			}
		}
		else {
			chflag = "";
		}
		return chflag;
	}

}
