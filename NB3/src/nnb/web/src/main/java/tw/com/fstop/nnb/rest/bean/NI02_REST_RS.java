package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class NI02_REST_RS extends BaseRestBean implements Serializable {
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = -55460393947218105L;
	
	private String MSGCOD;
	private String DATE;
	private String TIME;
	private String CMQTIME;
	public String getMSGCOD() {
		return MSGCOD;
	}
	public String getDATE() {
		return DATE;
	}
	public String getTIME() {
		return TIME;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	
	
	
}
	

