package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N997_REST_RQ extends BaseRestBean_PS implements Serializable {
	private static final long serialVersionUID = -559387283010166442L;
	
	private String EXECUTEFUNCTION;
	private String DPUSERID;
	private String DPGONAME;
	private String DPTRACNO;
	private String DPTRIBANK;
	private String DPTRDACNO;
	
	public String getEXECUTEFUNCTION(){
		return EXECUTEFUNCTION;
	}
	public void setEXECUTEFUNCTION(String eXECUTEFUNCTION){
		EXECUTEFUNCTION = eXECUTEFUNCTION;
	}
	public String getDPUSERID(){
		return DPUSERID;
	}
	public void setDPUSERID(String dPUSERID){
		DPUSERID = dPUSERID;
	}
	public String getDPGONAME(){
		return DPGONAME;
	}
	public void setDPGONAME(String dPGONAME){
		DPGONAME = dPGONAME;
	}
	public String getDPTRACNO(){
		return DPTRACNO;
	}
	public void setDPTRACNO(String dPTRACNO){
		DPTRACNO = dPTRACNO;
	}
	public String getDPTRIBANK(){
		return DPTRIBANK;
	}
	public void setDPTRIBANK(String dPTRIBANK){
		DPTRIBANK = dPTRIBANK;
	}
	public String getDPTRDACNO(){
		return DPTRDACNO;
	}
	public void setDPTRDACNO(String dPTRDACNO){
		DPTRDACNO = dPTRDACNO;
	}
}
