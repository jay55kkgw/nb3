package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class Ratequery_N030_RSDATA extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8879385650337085605L;

	private String FILL;
	private String OFCRY;
	private String HEADER;
	private String TIME;
	private String CRY;
	private String TERM;
	private String DATE;
	private String RECNO;
	private String BUYITR;
	private String SEQ;
	private String SELITR;

	public String getFILL() {
		return FILL;
	}

	public String getOFCRY() {
		return OFCRY;
	}

	public String getHEADER() {
		return HEADER;
	}

	public String getTIME() {
		return TIME;
	}

	public String getCRY() {
		return CRY;
	}

	public String getTERM() {
		return TERM;
	}

	public String getDATE() {
		return DATE;
	}

	public String getRECNO() {
		return RECNO;
	}

	public String getBUYITR() {
		return BUYITR;
	}

	public String getSEQ() {
		return SEQ;
	}

	public String getSELITR() {
		return SELITR;
	}

	public void setFILL(String fILL) {
		FILL = fILL;
	}

	public void setOFCRY(String oFCRY) {
		OFCRY = oFCRY;
	}

	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}

	public void setTIME(String tIME) {
		TIME = tIME;
	}

	public void setCRY(String cRY) {
		CRY = cRY;
	}

	public void setTERM(String tERM) {
		TERM = tERM;
	}

	public void setDATE(String dATE) {
		DATE = dATE;
	}

	public void setRECNO(String rECNO) {
		RECNO = rECNO;
	}

	public void setBUYITR(String bUYITR) {
		BUYITR = bUYITR;
	}

	public void setSEQ(String sEQ) {
		SEQ = sEQ;
	}

	public void setSELITR(String sELITR) {
		SELITR = sELITR;
	}

}
