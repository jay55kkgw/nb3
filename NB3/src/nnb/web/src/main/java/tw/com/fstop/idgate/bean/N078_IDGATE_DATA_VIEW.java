package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;


public class N078_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1786537310212125942L;

	
	@SerializedName(value = "ACN")
	private String ACN;
	
	@SerializedName(value = "FDPNUM")
	private String FDPNUM;
	
	@SerializedName(value = "AMTFDP")
	private String AMTFDP;
	
	@SerializedName(value = "TYPENAME")
	private String TYPENAME;
	
	@SerializedName(value = "SHOWDPISDT")
	private String SHOWDPISDT;
	
	@SerializedName(value = "SHOWDUEDAT")
	private String SHOWDUEDAT;
	
	@SerializedName(value = "DPINTTYPE")
	private String DPINTTYPE;
	
	@SerializedName(value = "ITR")
	private String ITR;
	
	@SerializedName(value = "FGSVNAME")
	private String FGSVNAME;
	
	@SerializedName(value = "DPSVACNO")
	private String DPSVACNO;
	 
	  
	@Override
	public Map<String, String> coverMap(){
		LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", "臺幣定存單到期續存");
		result.put("交易類型","-");
		result.put("存單帳號", this.ACN);
		result.put("存單號碼", this.FDPNUM);
		result.put("存單金額", "新台幣"+this.AMTFDP+"元");
		result.put("存單種類", this.TYPENAME);
		result.put("起存日", this.SHOWDPISDT);
		result.put("到期日", this.SHOWDUEDAT);
		result.put("計息方式", this.DPINTTYPE);
		result.put("利率", this.ITR);
		result.put("續存方式", this.FGSVNAME+this.DPSVACNO);
		return result;
	}


	public String getACN() {
		return ACN;
	}


	public void setACN(String aCN) {
		ACN = aCN;
	}


	public String getFDPNUM() {
		return FDPNUM;
	}


	public void setFDPNUM(String fDPNUM) {
		FDPNUM = fDPNUM;
	}


	public String getAMTFDP() {
		return AMTFDP;
	}


	public void setAMTFDP(String aMTFDP) {
		AMTFDP = aMTFDP;
	}


	public String getTYPENAME() {
		return TYPENAME;
	}


	public void setTYPENAME(String tYPENAME) {
		TYPENAME = tYPENAME;
	}


	public String getSHOWDPISDT() {
		return SHOWDPISDT;
	}


	public void setSHOWDPISDT(String sHOWDPISDT) {
		SHOWDPISDT = sHOWDPISDT;
	}


	public String getSHOWDUEDAT() {
		return SHOWDUEDAT;
	}


	public void setSHOWDUEDAT(String sHOWDUEDAT) {
		SHOWDUEDAT = sHOWDUEDAT;
	}


	public String getDPINTTYPE() {
		return DPINTTYPE;
	}


	public void setDPINTTYPE(String dPINTTYPE) {
		DPINTTYPE = dPINTTYPE;
	}


	public String getITR() {
		return ITR;
	}


	public void setITR(String iTR) {
		ITR = iTR;
	}


	public String getFGSVNAME() {
		return FGSVNAME;
	}


	public void setFGSVNAME(String fGSVNAME) {
		FGSVNAME = fGSVNAME;
	}


	public String getDPSVACNO() {
		return DPSVACNO;
	}


	public void setDPSVACNO(String dPSVACNO) {
		DPSVACNO = dPSVACNO;
	}

}
