package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * N174電文RQ
 */
public class N174_REST_RQ extends BaseRestBean_FX implements Serializable
{
	private static final long serialVersionUID = 6868861910004819383L;

	private String ADOPID;		// 電文代號

	private String CUSIDN;

	private String UID;

	private String PINNEW;

	private String FGINACNO;// 約定/非約定記號 1 約定帳號, 2 非約定帳號

	private String CMTRDATE;//預約單筆轉帳日期 

	private String CMSDATE;

	private String CMEDATE;

	private String CMPERIOD;

	private String INACNO1;//約轉轉入帳號JSON 

	private String INACNO2;//非約轉轉入帳號 

	private String TSFACN;

	private String FYTSFAN; //轉出帳號

	private String OUTCRY;

	private String TYPCOD;

	private String INTMTH;

	private String AMOUNT;

	private String AMOUNT_DIG;

	private String CODE;

	private String AUTXFTM;

	private String FGTRDATE;//預約註記 "0":及時;

	private String BGROENO;

	private String TRDATE;

	private String DPMYEMAIL;

	private String CMTRMAIL;

	private String CMTRMEMO;

	private String CMMAILMEMO;

	private String pkcs7Sign;
	
	//IDGATE

	private String sessionID;
	private String idgateID;
	private String txnID;

	public String getADOPID()
	{
		return ADOPID;
	}

	public void setADOPID(String aDOPID)
	{
		ADOPID = aDOPID;
	}

	public String getCUSIDN()
	{
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN)
	{
		CUSIDN = cUSIDN;
	}

	public String getPINNEW()
	{
		return PINNEW;
	}

	public void setPINNEW(String pINNEW)
	{
		PINNEW = pINNEW;
	}

	public String getFGINACNO()
	{
		return FGINACNO;
	}

	public void setFGINACNO(String fGINACNO)
	{
		FGINACNO = fGINACNO;
	}

	public String getTSFACN()
	{
		return TSFACN;
	}

	public void setTSFACN(String tSFACN)
	{
		TSFACN = tSFACN;
	}

	public String getFYTSFAN()
	{
		return FYTSFAN;
	}

	public void setFYTSFAN(String fYTSFAN)
	{
		FYTSFAN = fYTSFAN;
	}

	public String getOUTCRY()
	{
		return OUTCRY;
	}

	public void setOUTCRY(String oUTCRY)
	{
		OUTCRY = oUTCRY;
	}

	public String getTYPCOD()
	{
		return TYPCOD;
	}

	public void setTYPCOD(String tYPCOD)
	{
		TYPCOD = tYPCOD;
	}

	public String getINTMTH()
	{
		return INTMTH;
	}

	public void setINTMTH(String iNTMTH)
	{
		INTMTH = iNTMTH;
	}

	public String getAMOUNT()
	{
		return AMOUNT;
	}

	public void setAMOUNT(String aMOUNT)
	{
		AMOUNT = aMOUNT;
	}

	public String getCODE()
	{
		return CODE;
	}

	public void setCODE(String cODE)
	{
		CODE = cODE;
	}

	public String getAUTXFTM()
	{
		return AUTXFTM;
	}

	public void setAUTXFTM(String aUTXFTM)
	{
		AUTXFTM = aUTXFTM;
	}

	public String getBGROENO()
	{
		return BGROENO;
	}

	public void setBGROENO(String bGROENO)
	{
		BGROENO = bGROENO;
	}

	public String getDPMYEMAIL()
	{
		return DPMYEMAIL;
	}

	public void setDPMYEMAIL(String dPMYEMAIL)
	{
		DPMYEMAIL = dPMYEMAIL;
	}

	public String getCMTRMAIL()
	{
		return CMTRMAIL;
	}

	public void setCMTRMAIL(String cMTRMAIL)
	{
		CMTRMAIL = cMTRMAIL;
	}

	public String getUID()
	{
		return UID;
	}

	public void setUID(String uID)
	{
		UID = uID;
	}

	public String getINACNO1()
	{
		return INACNO1;
	}

	public void setINACNO1(String iNACNO1)
	{
		INACNO1 = iNACNO1;
	}

	public String getINACNO2()
	{
		return INACNO2;
	}

	public void setINACNO2(String iNACNO2)
	{
		INACNO2 = iNACNO2;
	}

	public String getAMOUNT_DIG()
	{
		return AMOUNT_DIG;
	}

	public void setAMOUNT_DIG(String aMOUNT_DIG)
	{
		AMOUNT_DIG = aMOUNT_DIG;
	}

	public String getFGTRDATE()
	{
		return FGTRDATE;
	}

	public void setFGTRDATE(String fGTRDATE)
	{
		FGTRDATE = fGTRDATE;
	}

	public String getCMMAILMEMO()
	{
		return CMMAILMEMO;
	}

	public void setCMMAILMEMO(String cMMAILMEMO)
	{
		CMMAILMEMO = cMMAILMEMO;
	}

	public String getCMTRDATE()
	{
		return CMTRDATE;
	}

	public void setCMTRDATE(String cMTRDATE)
	{
		CMTRDATE = cMTRDATE;
	}

	public String getCMSDATE()
	{
		return CMSDATE;
	}

	public void setCMSDATE(String cMSDATE)
	{
		CMSDATE = cMSDATE;
	}

	public String getCMEDATE()
	{
		return CMEDATE;
	}

	public void setCMEDATE(String cMEDATE)
	{
		CMEDATE = cMEDATE;
	}

	public String getCMPERIOD()
	{
		return CMPERIOD;
	}

	public void setCMPERIOD(String cMPERIOD)
	{
		CMPERIOD = cMPERIOD;
	}

	public String getCMTRMEMO()
	{
		return CMTRMEMO;
	}

	public void setCMTRMEMO(String cMTRMEMO)
	{
		CMTRMEMO = cMTRMEMO;
	}

	public String getPkcs7Sign()
	{
		return pkcs7Sign;
	}

	public void setPkcs7Sign(String pkcs7Sign)
	{
		this.pkcs7Sign = pkcs7Sign;
	}

	public String getTRDATE()
	{
		return TRDATE;
	}

	public void setTRDATE(String tRDATE)
	{
		TRDATE = tRDATE;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public String getIdgateID() {
		return idgateID;
	}

	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}

	public String getTxnID() {
		return txnID;
	}

	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}


}