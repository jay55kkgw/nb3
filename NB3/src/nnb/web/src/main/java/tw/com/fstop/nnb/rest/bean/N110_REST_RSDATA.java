package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N110_REST_RSDATA implements Serializable {

	private static final long serialVersionUID = -2739572910855200803L;
	private String ACN;// 帳號
	private String ADPIBAL;//可使用餘額
	private String CLR;//本交
	private String BDPIBAL;//帳上餘額
	private String FD_LOST;//存單掛失
	private String SV_LOST;//存摺掛失
	private String SIGN_LOST;//印鑑掛失
	private String FILLER;
	private String KIND;

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	public String getADPIBAL() {
		return ADPIBAL;
	}

	public void setADPIBAL(String aDPIBAL) {
		ADPIBAL = aDPIBAL;
	}

	public String getCLR() {
		return CLR;
	}

	public void setCLR(String cLR) {
		CLR = cLR;
	}

	public String getBDPIBAL() {
		return BDPIBAL;
	}

	public void setBDPIBAL(String bDPIBAL) {
		BDPIBAL = bDPIBAL;
	}

	public String getFD_LOST() {
		return FD_LOST;
	}

	public void setFD_LOST(String fD_LOST) {
		FD_LOST = fD_LOST;
	}

	public String getSV_LOST() {
		return SV_LOST;
	}

	public void setSV_LOST(String sV_LOST) {
		SV_LOST = sV_LOST;
	}

	public String getSIGN_LOST() {
		return SIGN_LOST;
	}

	public void setSIGN_LOST(String sIGN_LOST) {
		SIGN_LOST = sIGN_LOST;
	}

	public String getFILLER() {
		return FILLER;
	}

	public void setFILLER(String fILLER) {
		FILLER = fILLER;
	}

	public String getKIND() {
		return KIND;
	}

	public void setKIND(String kIND) {
		KIND = kIND;
	}

}
