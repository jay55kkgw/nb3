package tw.com.fstop.nnb.spring.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class I18nSample {

	
    @RequestMapping(value = "/i18n", method = RequestMethod.GET)
	public String i18nPage(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model)
	{
    	
    	return  "i18n";
	}
	
}
