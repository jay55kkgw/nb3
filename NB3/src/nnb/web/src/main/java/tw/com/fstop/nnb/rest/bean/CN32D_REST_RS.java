package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class CN32D_REST_RS extends BaseRestBean implements Serializable {
/**
	 * 
	 */
	private static final long serialVersionUID = 9024018239311141098L;
private String ENABLE;
private String MSGCOD;
private String occurMsg;
private String CUSIDN;
private String ACN;
private String jsondc;
private String ISSUER;
private String ACNNO;//無卡提款申請帳號
private String TRMID;
private String iSeqNo;
private String ICSEQ;
private String TAC;
private String pkcs7Sign;
private String ADOPID;
private String CHIP_ACN;
private String UID;
private String OUTACN;
public String getENABLE() {
	return ENABLE;
}
public void setENABLE(String eNABLE) {
	ENABLE = eNABLE;
}
public String getMSGCOD() {
	return MSGCOD;
}
public void setMSGCOD(String mSGCOD) {
	MSGCOD = mSGCOD;
}
public String getOccurMsg() {
	return occurMsg;
}
public void setOccurMsg(String occurMsg) {
	this.occurMsg = occurMsg;
}
public String getCUSIDN() {
	return CUSIDN;
}
public void setCUSIDN(String cUSIDN) {
	CUSIDN = cUSIDN;
}
public String getACN() {
	return ACN;
}
public void setACN(String aCN) {
	ACN = aCN;
}
public String getJsondc() {
	return jsondc;
}
public void setJsondc(String jsondc) {
	this.jsondc = jsondc;
}
public String getISSUER() {
	return ISSUER;
}
public void setISSUER(String iSSUER) {
	ISSUER = iSSUER;
}
public String getACNNO() {
	return ACNNO;
}
public void setACNNO(String aCNNO) {
	ACNNO = aCNNO;
}
public String getTRMID() {
	return TRMID;
}
public void setTRMID(String tRMID) {
	TRMID = tRMID;
}
public String getiSeqNo() {
	return iSeqNo;
}
public void setiSeqNo(String iSeqNo) {
	this.iSeqNo = iSeqNo;
}
public String getICSEQ() {
	return ICSEQ;
}
public void setICSEQ(String iCSEQ) {
	ICSEQ = iCSEQ;
}
public String getTAC() {
	return TAC;
}
public void setTAC(String tAC) {
	TAC = tAC;
}
public String getPkcs7Sign() {
	return pkcs7Sign;
}
public void setPkcs7Sign(String pkcs7Sign) {
	this.pkcs7Sign = pkcs7Sign;
}
public String getADOPID() {
	return ADOPID;
}
public void setADOPID(String aDOPID) {
	ADOPID = aDOPID;
}
public String getCHIP_ACN() {
	return CHIP_ACN;
}
public void setCHIP_ACN(String cHIP_ACN) {
	CHIP_ACN = cHIP_ACN;
}
public String getUID() {
	return UID;
}
public void setUID(String uID) {
	UID = uID;
}
public String getOUTACN() {
	return OUTACN;
}
public void setOUTACN(String oUTACN) {
	OUTACN = oUTACN;
}
}
