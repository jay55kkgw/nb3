package tw.com.fstop.nnb.spring.controller;

import java.util.Base64;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.First_Login_Service;
import tw.com.fstop.tbb.nnb.dao.Trns_nnb_CusDataDao;
import tw.com.fstop.tbb.nnb.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;

@SessionAttributes({ SessionUtil.CUSIDN, SessionUtil.FIRSTLOGIN })
@Controller
@RequestMapping(value="/FIRST/LOGIN")
public class First_Login_Controller {
	
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private First_Login_Service first_login_service;
	@Autowired
	private Trns_nnb_CusDataDao trns_nnb_cusdatadao;

	
	/**
	 * 檢查第一階段首次登入轉檔，避免重新整理
	 */
	@RequestMapping(value = "/cusdata_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult cusdata_aj(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		String cusidn = "";
		
		try {
			bs = new BaseResult();
			
			// 解密使用者統編
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
			}
			log.debug("cusdata_aj.cusidn: " + cusidn);
			
			if ( trns_nnb_cusdatadao.checkCusData(cusidn) ) {
				log.debug("cusdata_aj.checkCusData: true");
				bs.setMsgCode("0");
				bs.setMessage("success");
				bs.setResult(true);
				
			} else {
				log.debug("cusdata_aj.checkCusData: false");
				bs.setMsgCode("1");
				bs.setMessage("fail");
				bs.setResult(false);
			}
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		return bs;
	}
	
	/**
	 * ADMMAILLOG轉檔--20200316 不即時同步，改用批次
	 */
//	@RequestMapping(value = "/maillog_aj", produces = {"application/json;charset=UTF-8"})
//	public @ResponseBody BaseResult maillog_aj(HttpServletRequest request, HttpServletResponse response, 
//			@RequestParam Map<String, String> reqParam, Model model) {
//		BaseResult bs = null;
//		String cusidn = "";
//		
//		try {
//			bs = new BaseResult();
//			
//			// 解密使用者統編
//			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
//			// session有無CUSIDN
//			if( !"".equals(cusidn) && cusidn != null ) {
//				cusidn = new String(Base64.getDecoder().decode(cusidn));
//			}
//			log.debug("maillog_aj.cusidn: " + cusidn);
//			
//			boolean result = first_login_service.admmaillog_tx(cusidn);
//			log.debug("maillog_aj.result: {}", result);
//			
//			// 紀錄使用者資料轉檔結果
//			processTrnsFlow(result, cusidn, "ADMMAILLOG", bs);
//			
//		} catch (Exception e) {
//			log.error("{}", e);
//		}
//		return bs;
//	}
	
	/**
	 * TXNADDRESSBOOK轉檔
	 */
	@RequestMapping(value = "/addressbook_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult addressbook_aj(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		String cusidn = "";
		
		try {
			bs = new BaseResult();
			
			// 解密使用者統編
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
			}
			log.debug("addressbook_aj.cusidn: " + cusidn);
			
			boolean result = first_login_service.txnaddressbook_tx(cusidn);
			log.debug("addressbook_aj.result: {}", result);
			
			// 紀錄使用者資料轉檔結果
			processTrnsFlow(result, cusidn, "TXNADDRESSBOOK", bs);
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		return bs;
	}
	
	/**
	 * TXNCUSINVATTRIB轉檔
	 */
	@RequestMapping(value = "/cusinvattrib_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult cusinvattrib_aj(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		String cusidn = "";
		
		try {
			bs = new BaseResult();
			
			// 解密使用者統編
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
			}
			log.debug("cusinvattrib_aj.cusidn: " + cusidn);
			
			boolean result = first_login_service.txncusinvattrib_tx(cusidn);
			log.debug("cusinvattrib_aj.result: {}", result);
			
			// 紀錄使用者資料轉檔結果
			processTrnsFlow(result, cusidn, "TXNCUSINVATTRIB", bs);
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		return bs;
	}
	
	/**
	 * TXNCUSINVATTRIB轉檔
	 */
	@RequestMapping(value = "/cusinvattrhist_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult cusinvattrhist_aj(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		String cusidn = "";
		
		try {
			bs = new BaseResult();
			
			// 解密使用者統編
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
			}
			log.debug("cusinvattrhist_aj.cusidn: " + cusidn);
			
			boolean result = first_login_service.txncusinvattrhist_tx(cusidn);
			log.debug("cusinvattrhist_aj.result: {}", result);
			
			// 紀錄使用者資料轉檔結果
			processTrnsFlow(result, cusidn, "TXNCUSINVATTRHIST", bs);
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		return bs;
	}
	
	/**
	 * TXNCUSINVATTRIB轉檔
	 */
	@RequestMapping(value = "/phonetoken_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult phonetoken_aj(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		String cusidn = "";
		
		try {
			bs = new BaseResult();
			
			// 解密使用者統編
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
			}
			log.debug("phonetoken_aj.cusidn: " + cusidn);
			
			boolean result = first_login_service.txnphonetoken_tx(cusidn);
			log.debug("phonetoken_aj.result: {}", result);
			
			// 紀錄使用者資料轉檔結果
			processTrnsFlow(result, cusidn, "TXNPHONETOKEN", bs);
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		return bs;
	}
	
	/**
	 * NB3USER紀錄登入新網銀轉檔成功
	 */
//	@RequestMapping(value = "/nb3user_aj", produces = {"application/json;charset=UTF-8"})
//	public @ResponseBody BaseResult nb3user_aj(HttpServletRequest request, HttpServletResponse response, 
//			@RequestParam Map<String, String> reqParam, Model model) {
//		BaseResult bs = null;
//		String cusidn = "";
//		
//		try {
//			bs = new BaseResult();
//			
//			// 解密使用者統編
//			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
//			// session有無CUSIDN
//			if( !"".equals(cusidn) && cusidn != null ) {
//				cusidn = new String(Base64.getDecoder().decode(cusidn));
//			}
//			log.debug("nb3user_aj.cusidn: " + cusidn);
//			
//			boolean result = first_login_service.nb3user_tx(cusidn);
//			log.debug("nb3user_aj.result: {}", result);
//			
//			// 紀錄使用者資料轉檔結果
//			processTrnsFlow(result, cusidn, "NB3USER", bs);
//			
//		} catch (Exception e) {
//			log.error("{}", e);
//		}
//		return bs;
//	}
	
	/**
	 * TXNFXRECORD轉檔
	 */
	@RequestMapping(value = "/fxrecord_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult fxrecord_aj(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		String cusidn = "";
		
		try {
			bs = new BaseResult();
			
			// 解密使用者統編
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
			}
			log.debug("fxrecord_aj.cusidn: " + cusidn);
			
			boolean result = first_login_service.txnfxrecord_tx(cusidn);
			log.debug("fxrecord_aj.result: {}", result);
			
			// 紀錄使用者資料轉檔結果
			processTrnsFlow(result, cusidn, "TXNFXRECORD", bs);
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		return bs;
	}
	
	/**
	 * TXNFXSCHPAY轉檔
	 */
	@RequestMapping(value = "/fxschedule_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult fxschedule_aj(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		String cusidn = "";
		
		try {
			bs = new BaseResult();
			
			// 解密使用者統編
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
			}
			log.debug("fxschedule_aj.cusidn: " + cusidn);
			
			boolean result = first_login_service.txnfxschedule_tx(cusidn);
			log.debug("fxschedule_aj.result: {}", result);
			
			// 紀錄使用者資料轉檔結果
			processTrnsFlow(result, cusidn, "TXNFXSCHPAY", bs);
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		return bs;
	}
	
	/**
	 * TXNGDRECORD轉檔
	 */
	@RequestMapping(value = "/gdrecord_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult gdrecord_aj(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		String cusidn = "";
		
		try {
			bs = new BaseResult();
			
			// 解密使用者統編
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
			}
			log.debug("gdrecord_aj.cusidn: " + cusidn);
			
			boolean result = first_login_service.txngdrecord_tx(cusidn);
			log.debug("gdrecord_aj.result: {}", result);
			
			// 紀錄使用者資料轉檔結果
			processTrnsFlow(result, cusidn, "TXNGDRECORD", bs);
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		return bs;
	}
	
	/**
	 * TXNTWRECORD轉檔
	 */
	@RequestMapping(value = "/twrecord_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult twrecord_aj(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		String cusidn = "";
		
		try {
			bs = new BaseResult();
			
			// 解密使用者統編
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
			}
			log.debug("twrecord_aj.cusidn: " + cusidn);
			
			boolean result = first_login_service.txntwrecord_tx(cusidn);
			log.debug("twrecord_aj.result: {}", result);
			
			// 紀錄使用者資料轉檔結果
			processTrnsFlow(result, cusidn, "TXNTWRECORD", bs);
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		return bs;
	}
	
	/**
	 * TXNTWSCHPAY轉檔
	 */
	@RequestMapping(value = "/twschedule_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult twschedule_aj(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		String cusidn = "";
		
		try {
			bs = new BaseResult();
			
			// 解密使用者統編
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
			}
			log.debug("twschedule_aj.cusidn: " + cusidn);
			
			boolean result = first_login_service.txntwschedule_tx(cusidn);
			log.debug("twschedule_aj.result: {}", result);
			
			// 紀錄使用者資料轉檔結果
			processTrnsFlow(result, cusidn, "TXNTWSCHPAY", bs);
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		return bs;
	}
	
	/**
	 * TXNTRACCSET轉檔
	 */
	@RequestMapping(value = "/traccset_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult traccset_aj(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		String cusidn = "";
		
		try {
			bs = new BaseResult();
			
			// 解密使用者統編
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
			}
			log.debug("traccset_aj.cusidn: " + cusidn);
			
			boolean result = first_login_service.txntraccset_tx(cusidn);
			log.debug("traccset_aj.result: {}", result);
			
			// 紀錄使用者資料轉檔結果
			processTrnsFlow(result, cusidn, "TXNTRACCSET", bs);
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		return bs;
	}
	
	
	/**
	 * TXNUSER轉檔
	 */
	@RequestMapping(value = "/userdata_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult userdata_aj(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		String cusidn = "";
		
		try {
			bs = new BaseResult();
			
			// 解密使用者統編
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
			}
			log.debug("userdata_aj.cusidn: " + cusidn);
			
			boolean result = first_login_service.txnuser_tx(cusidn);
			log.debug("userdata_aj.result: {}", result);
			
			// 紀錄使用者資料轉檔結果
			processTrnsFlow(result, cusidn, "TXNUSER", bs);
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		return bs;
	}
	
	/**
	 * NB3USER轉檔
	 */
	@RequestMapping(value = "/nb3user_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult nb3user_aj(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam Map<String, String> reqParam, Model model) {
		BaseResult bs = null;
		String cusidn = "";
		
		try {
			bs = new BaseResult();
			
			// 解密使用者統編
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
			}
			log.debug("userdata_aj.cusidn: " + cusidn);
			
			// 轉檔全部成功更新舊網銀資料庫NB3USER，代表成功登入新網銀
			if ( trns_nnb_cusdatadao.checkCusNB3(cusidn) ) {
				boolean resultNB3 = first_login_service.nb3user_tx(cusidn);
				log.debug("nb3user_aj.resultNB3: {}", resultNB3);
				
				// 成功更新舊資料庫NB3USER
				if (resultNB3) {
					// 紀錄使用者資料轉檔結果
					processTrnsFlow(resultNB3, cusidn, "NB3USER", bs);
				}
			}
			
			// 成功轉檔之後進入我的首頁即不再顯示轉檔結果
			if ( bs.getResult() ) {
				SessionUtil.addAttribute(model, SessionUtil.FIRSTLOGIN, false );
			}
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		return bs;
	}
	
	/**
	 * 更新TRNS_NNB_CUSDATA轉檔進度
	 */
	public void processTrnsFlow(boolean result, String cusidn, String funcName, BaseResult bs) {
		try {
			
			log.info("processTrns.func.start: {}", funcName + ": " + DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss:SSS"));
			
			// 轉檔成功
			if(result) {
				// 紀錄使用者資料轉檔結果
				int res = trns_nnb_cusdatadao.updateByCol(cusidn, funcName, "0");
				log.info("processTrnsFlow.res: {}", res);
				
				bs.setMsgCode("0");
				bs.setMessage("success");
				bs.setResult(true);
				bs.setData(funcName);
				
			} // 轉檔失敗
			else {
				log.warn(ESAPIUtil.vaildLog("processTrnsFlow.warning: cusidn>>" + cusidn + ", funcName>>" + funcName));
				
				// 紀錄使用者資料轉檔結果
				int res = trns_nnb_cusdatadao.updateByCol(cusidn, funcName, "1");
				log.info("processTrnsFlow.res: {}", res);
				
				bs.setMsgCode("1");
				bs.setMessage("fail");
				bs.setResult(false);
				bs.setData(funcName);
			}
			
			log.info("processTrns.func.end: {}", funcName + ": " + DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss:SSS"));
			
		} catch (Exception e) {
			log.error("{}", e);
		}
	}
	
//	/**
//	 * 非同步存取資料庫避免鎖死，故在程式使用synchronized
//	 */
//	public synchronized void processTrnsFlow(boolean result, String cusidn, String funcName, BaseResult bs) {
//		try {
//			Thread.sleep(1000); // 執行緒卡住flow
//			
//			log.info("processTrns.func.start: {}", funcName + ": " + DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss:SSS"));
//			
//			// 轉檔成功
//			if(result) {
//				// 紀錄使用者資料轉檔結果
//				TRNS_NNB_CUSDATA po = first_login_service.cusdataRecord(cusidn);
//				
//				// by case update column
//				if("ADMMAILLOG".equals(funcName)) po.setADMMAILLOG("0");
//				if("TXNADDRESSBOOK".equals(funcName)) po.setTXNADDRESSBOOK("0");
//				if("TXNCUSINVATTRIB".equals(funcName)) po.setTXNCUSINVATTRIB("0");
//				if("TXNCUSINVATTRHIST".equals(funcName)) po.setTXNCUSINVATTRHIST("0");
//				if("TXNFXRECORD".equals(funcName)) po.setTXNFXRECORD("0");
//				if("TXNFXSCHPAY".equals(funcName)) po.setTXNFXSCHPAY("0");
//				if("TXNGDRECORD".equals(funcName)) po.setTXNGDRECORD("0");
//				if("TXNTWRECORD".equals(funcName)) po.setTXNTWRECORD("0");
//				if("TXNTWSCHPAY".equals(funcName)) po.setTXNTWSCHPAY("0");
//				if("TXNTRACCSET".equals(funcName)) po.setTXNTRACCSET("0");
//				if("TXNUSER".equals(funcName)) po.setTXNUSER("0");
//				if("TXNPHONETOKEN".equals(funcName)) po.setTXNPHONETOKEN("0");
//				
//				// 轉檔全部成功更新舊網銀資料庫NB3USER，代表成功登入新網銀
//				if ( po.checkCusNB3() ) {
//					boolean resultNB3 = first_login_service.nb3user_tx(cusidn);
//					log.debug("nb3user_aj.resultNB3: {}", resultNB3);
//					
//					// 成功更新舊資料庫NB3USER
//					if(resultNB3) po.setNB3USER("0");
//				}
//				trns_nnb_cusdatadao.saveOrUpdate(po);
//				
//				bs.setMsgCode("0");
//				bs.setMessage("success");
//				bs.setResult(true);
//				bs.setData(result);
//				
//			} // 轉檔失敗
//			else {
//				// 紀錄使用者資料轉檔結果
//				TRNS_NNB_CUSDATA po = first_login_service.cusdataRecord(cusidn);
//				
//				// by case update column
//				if("ADMMAILLOG".equals(funcName)) po.setADMMAILLOG("1");
//				if("TXNADDRESSBOOK".equals(funcName)) po.setTXNADDRESSBOOK("1");
//				if("TXNCUSINVATTRIB".equals(funcName)) po.setTXNCUSINVATTRIB("1");
//				if("TXNCUSINVATTRHIST".equals(funcName)) po.setTXNCUSINVATTRHIST("1");
//				if("TXNFXRECORD".equals(funcName)) po.setTXNFXRECORD("1");
//				if("TXNFXSCHPAY".equals(funcName)) po.setTXNFXSCHPAY("1");
//				if("TXNGDRECORD".equals(funcName)) po.setTXNGDRECORD("1");
//				if("TXNTWRECORD".equals(funcName)) po.setTXNTWRECORD("1");
//				if("TXNTWSCHPAY".equals(funcName)) po.setTXNTWSCHPAY("1");
//				if("TXNTRACCSET".equals(funcName)) po.setTXNTRACCSET("1");
//				if("TXNUSER".equals(funcName)) po.setTXNUSER("1");
//				if("TXNPHONETOKEN".equals(funcName)) po.setTXNPHONETOKEN("1");
//				trns_nnb_cusdatadao.saveOrUpdate(po);
//				
//				bs.setMsgCode("1");
//				bs.setMessage("fail");
//				bs.setResult(false);
//			}
//			
//			log.info("processTrns.func.end: {}", funcName + ": " + DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss:SSS"));
//			
//		} catch (Exception e) {
//			log.error("{}", e);
//		}
//	}
	
	
	/**
	 * 20201209--改成前端只發一道請求，由後端一次處理
	 */
	@RequestMapping(value = "/transfer_aj", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult transfer_aj(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam Map<String, String> reqParam, Model model, SessionStatus status) {
		BaseResult bs = null;
		String cusidn = "";
		
		try {
			// 解密使用者統編
			cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn));
			}
			log.debug("twrecord_aj.cusidn: " + cusidn);
			
			// 
			bs = new BaseResult();
			boolean result_txntwrecord = first_login_service.txntwrecord_tx(cusidn);
			log.debug("twrecord_aj.result: {}", result_txntwrecord);
			processTrnsFlow(result_txntwrecord, cusidn, "TXNTWRECORD", bs); // 紀錄使用者資料轉檔結果
			if (!bs.getResult()) { return bs; }
			
			//
			bs = new BaseResult();
			boolean result_txntwschedule = first_login_service.txntwschedule_tx(cusidn);
			log.debug("twschedule_aj.result: {}", result_txntwschedule);
			processTrnsFlow(result_txntwschedule, cusidn, "TXNTWSCHPAY", bs); // 紀錄使用者資料轉檔結果
			if (!bs.getResult()) { return bs; }
			
			//
			bs = new BaseResult();
			boolean result_txnfxrecord = first_login_service.txnfxrecord_tx(cusidn);
			log.debug("fxrecord_aj.result: {}", result_txnfxrecord);
			processTrnsFlow(result_txnfxrecord, cusidn, "TXNFXRECORD", bs); // 紀錄使用者資料轉檔結果
			if (!bs.getResult()) { return bs; }
			
			//
			bs = new BaseResult();
			boolean result_txnfxschedule = first_login_service.txnfxschedule_tx(cusidn);
			log.debug("fxschedule_aj.result: {}", result_txnfxschedule);
			processTrnsFlow(result_txnfxschedule, cusidn, "TXNFXSCHPAY", bs); // 紀錄使用者資料轉檔結果
			if (!bs.getResult()) { return bs; }
			
			//
			bs = new BaseResult();
			boolean result_txngdrecord = first_login_service.txngdrecord_tx(cusidn);
			log.debug("gdrecord_aj.result: {}", result_txngdrecord);
			processTrnsFlow(result_txngdrecord, cusidn, "TXNGDRECORD", bs); // 紀錄使用者資料轉檔結果
			if (!bs.getResult()) { return bs; }
			
			//
			bs = new BaseResult();
			boolean result_txntraccset = first_login_service.txntraccset_tx(cusidn);
			log.debug("traccset_aj.result: {}", result_txntraccset);
			processTrnsFlow(result_txntraccset, cusidn, "TXNTRACCSET", bs);	// 紀錄使用者資料轉檔結果		
			if (!bs.getResult()) { return bs; }
			
			//
			bs = new BaseResult();
			boolean result_txnaddressbook = first_login_service.txnaddressbook_tx(cusidn);
			log.debug("addressbook_aj.result: {}", result_txnaddressbook);
			processTrnsFlow(result_txnaddressbook, cusidn, "TXNADDRESSBOOK", bs); // 紀錄使用者資料轉檔結果
			if (!bs.getResult()) { return bs; }
			
			//
			bs = new BaseResult();
			boolean result_txncusinvattrib = first_login_service.txncusinvattrib_tx(cusidn);
			log.debug("cusinvattrib_aj.result: {}", result_txncusinvattrib);
			processTrnsFlow(result_txncusinvattrib, cusidn, "TXNCUSINVATTRIB", bs); // 紀錄使用者資料轉檔結果
			if (!bs.getResult()) { return bs; }
			
			//
			bs = new BaseResult();
			boolean result_txncusinvattrhist = first_login_service.txncusinvattrhist_tx(cusidn);
			log.debug("cusinvattrhist_aj.result: {}", result_txncusinvattrhist);
			processTrnsFlow(result_txncusinvattrhist, cusidn, "TXNCUSINVATTRHIST", bs); // 紀錄使用者資料轉檔結果
			if (!bs.getResult()) { return bs; }
			
			//
			bs = new BaseResult();
			boolean result_txnphonetoken = first_login_service.txnphonetoken_tx(cusidn);
			log.debug("phonetoken_aj.result: {}", result_txnphonetoken);
			processTrnsFlow(result_txnphonetoken, cusidn, "TXNPHONETOKEN", bs); // 紀錄使用者資料轉檔結果	
			if (!bs.getResult()) { return bs; }
			
			
			// 轉檔全部成功更新舊網銀資料庫NB3USER，代表成功登入新網銀
			bs = new BaseResult();
			if ( trns_nnb_cusdatadao.checkCusNB3(cusidn) ) {
				boolean resultNB3 = first_login_service.nb3user_tx(cusidn);
				log.debug("nb3user_aj.resultNB3: {}", resultNB3);
				
				// 成功更新舊資料庫NB3USER
				if (resultNB3) {
					// 紀錄使用者資料轉檔結果
					processTrnsFlow(resultNB3, cusidn, "NB3USER", bs);
					if (!bs.getResult()) { return bs; }
				}
			}
			
			// 成功轉檔之後進入我的首頁即不再顯示轉檔結果
			if ( bs.getResult() ) {
				SessionUtil.addAttribute(model, SessionUtil.FIRSTLOGIN, false );
			}			
			
		} catch (Exception e) {
			log.error("{}", e);
		}
		return bs;
	}
	
	
}
