package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * 通知手機啟動驗證
 * 
 * @author Leo
 *
 */
public class svCancel_Txn_REST_RSDATA extends BaseRestBean_IDGATE implements Serializable {
	
	private static final long serialVersionUID = -8105756062267887930L;

	private String returnCode; //
	private String returnMsg; //
	public String getReturnCode() {
		return returnCode;
	}
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}
	public String getReturnMsg() {
		return returnMsg;
	}
	public void setReturnMsg(String returnMsg) {
		this.returnMsg = returnMsg;
	}
}
