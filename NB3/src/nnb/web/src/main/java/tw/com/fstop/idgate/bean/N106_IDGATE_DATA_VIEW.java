package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

public class N106_IDGATE_DATA_VIEW  extends Base_IDGATE_DATA_VIEW implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1033882172409711942L;

	@SerializedName(value = "TRANTITLE")
	private String TRANTITLE;

	@SerializedName(value = "TRANNAME")
	private String TRANNAME;

	@Override
	public Map<String, String> coverMap(){
		Map<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", this.TRANTITLE);
		result.put("交易類型", this.TRANNAME);
		return result;
	}

	public String getTRANTITLE() {
		return TRANTITLE;
	}

	public void setTRANTITLE(String tRANTITLE) {
		TRANTITLE = tRANTITLE;
	}

	public String getTRANNAME() {
		return TRANNAME;
	}

	public void setTRANNAME(String tRANNAME) {
		TRANNAME = tRANNAME;
	}
	
}
