package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N913_REST_RQ extends BaseRestBean_OLS implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2109674885599917456L;
	
	private String CUSIDN;		// 身分證號
	private String UID;			// 同CUSIDN
	private String PINNEW;		// 交易密碼SHA1值
	
	// 電文不需要但ms_tw需要的欄位
	private String MB_Switch;	// radio選項(A啟用、D停用)
	private String OriStatus;	// 行動銀行啟用狀態
	
	public String getCUSIDN() {
		return CUSIDN;
	}
	
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	
	public String getUID() {
		return UID;
	}
	
	public void setUID(String uID) {
		UID = uID;
	}
	
	public String getPINNEW() {
		return PINNEW;
	}
	public void setPINNEW(String pINNEW) {
		PINNEW = pINNEW;
	}
	
	public String getMB_Switch() {
		return MB_Switch;
	}
	
	public void setMB_Switch(String mB_Switch) {
		MB_Switch = mB_Switch;
	}
	
	public String getOriStatus() {
		return OriStatus;
	}
	
	public void setOriStatus(String oriStatus) {
		OriStatus = oriStatus;
	}

}
