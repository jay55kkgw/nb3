package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 手機門號_更新客戶綁定
 * 
 * @author Vincenthuang
 *
 */
public class MtpChangeAccount_REST_RQ extends BaseRestBean_QR implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2523785130886310944L;
		

		//=========其餘參數===========
		private String ADOPID = "MtpChange";//紀錄TXNLOG用
		private String FGTXWAY;				//交易類別，讓ms_qr判斷是否要驗證
		
		//=========API參數===========
		private String mobilephone;			//手機門號
		private String txacn1;				//原綁定帳號
		private String txacn2;				//新綁定帳號
		private String bankcode;			//金融機構代碼
		private String binddefault;			//是否預設收款帳戶
		private String oldbind;				//歐派，喔不是，舊帳號是否預設收款帳戶
		private String usermail;				
	
		//=========IDGATE參數===========
		private String txnID;
		private String idgateID;
		private String sessionID;
		private Map IDGATEDATA;
		
		
		public String getADOPID() {
			return ADOPID;
		}
		public void setADOPID(String aDOPID) {
			ADOPID = aDOPID;
		}
		
		public String getFGTXWAY() {
			return FGTXWAY;
		}
		public void setFGTXWAY(String fGTXWAY) {
			FGTXWAY = fGTXWAY;
		}
		public Map getIDGATEDATA() {
			return IDGATEDATA;
		}
		public void setIDGATEDATA(Map iDGATEDATA) {
			IDGATEDATA = iDGATEDATA;
		}
		public String getTxnID() {
			return txnID;
		}
		public void setTxnID(String txnID) {
			this.txnID = txnID;
		}
		public String getIdgateID() {
			return idgateID;
		}
		public void setIdgateID(String idgateID) {
			this.idgateID = idgateID;
		}
		public String getSessionID() {
			return sessionID;
		}
		public void setSessionID(String sessionID) {
			this.sessionID = sessionID;
		}
		
		public String getMobilephone() {
			return mobilephone;
		}
		public void setMobilephone(String mobilephone) {
			this.mobilephone = mobilephone;
		}
		public String getTxacn1() {
			return txacn1;
		}
		public void setTxacn1(String txacn1) {
			this.txacn1 = txacn1;
		}
		public String getTxacn2() {
			return txacn2;
		}
		public void setTxacn2(String txacn2) {
			this.txacn2 = txacn2;
		}
		public String getBankcode() {
			return bankcode;
		}
		public void setBankcode(String bankcode) {
			this.bankcode = bankcode;
		}
		public String getBinddefault() {
			return binddefault;
		}
		public void setBinddefault(String binddefault) {
			this.binddefault = binddefault;
		}
		public String getOldbind() {
			return oldbind;
		}
		public void setOldbind(String oldbind) {
			this.oldbind = oldbind;
		}
		public String getUsermail() {
			return usermail;
		}
		public void setUsermail(String usermail) {
			this.usermail = usermail;
		}
		
}
