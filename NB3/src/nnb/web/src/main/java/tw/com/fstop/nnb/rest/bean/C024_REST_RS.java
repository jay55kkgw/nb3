package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * C024電文RS
 */
public class C024_REST_RS extends BaseRestBean implements Serializable{
	private static final long serialVersionUID = 6791228050721094802L;
	
	private String DATE;
	private String TIME;
	private String NEXT;
	private String RECNO;
	private String CUSIDN;
	private String CUSNAME;
	private String LOOPINFO;
	private String TRADEDATE2;
	private String FILLER_X3;
	private String TOTRECNO;
	private LinkedList<C024_REST_RSDATA> REC;
	private String CMQTIME;
	private String SHWD;//視窗註記 2020/07/02新增
	
	public String getDATE(){
		return DATE;
	}
	public void setDATE(String dATE){
		DATE = dATE;
	}
	public String getTIME(){
		return TIME;
	}
	public void setTIME(String tIME){
		TIME = tIME;
	}
	public String getNEXT(){
		return NEXT;
	}
	public void setNEXT(String nEXT){
		NEXT = nEXT;
	}
	public String getRECNO(){
		return RECNO;
	}
	public void setRECNO(String rECNO){
		RECNO = rECNO;
	}
	public String getCUSIDN(){
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN){
		CUSIDN = cUSIDN;
	}
	public String getCUSNAME(){
		return CUSNAME;
	}
	public void setCUSNAME(String cUSNAME){
		CUSNAME = cUSNAME;
	}
	public String getLOOPINFO(){
		return LOOPINFO;
	}
	public void setLOOPINFO(String lOOPINFO){
		LOOPINFO = lOOPINFO;
	}
	public String getTRADEDATE2(){
		return TRADEDATE2;
	}
	public void setTRADEDATE2(String tRADEDATE2){
		TRADEDATE2 = tRADEDATE2;
	}
	public String getFILLER_X3(){
		return FILLER_X3;
	}
	public void setFILLER_X3(String fILLER_X3){
		FILLER_X3 = fILLER_X3;
	}
	public String getTOTRECNO(){
		return TOTRECNO;
	}
	public void setTOTRECNO(String tOTRECNO){
		TOTRECNO = tOTRECNO;
	}
	public LinkedList<C024_REST_RSDATA> getREC(){
		return REC;
	}
	public void setREC(LinkedList<C024_REST_RSDATA> rEC){
		REC = rEC;
	}
	public String getCMQTIME(){
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME){
		CMQTIME = cMQTIME;
	}
	public String getSHWD() {
		return SHWD;
	}
	public void setSHWD(String sHWD) {
		SHWD = sHWD;
	}
}