package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N510_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1874898449756769229L;
	
	String ABEND;
	String __OCCURS;
	String RESERVE;
	String COUNT;
	String USERDATA_X50;
	String NTDBAL;
	String ACN;
	String AVAILABLE;
	String OCCURS_VECTOR_FOR_MULTIPLE_INDEX;
	String TOTAL_CLR;
	String CUID;
	String CHGCOD;
	String BALANCE;
	String CMQTIME;
	LinkedList<N510_REST_RSDATA> ACNInfo;
	LinkedList<N510_REST_RSDATA> AVASum;
	LinkedList<N510_REST_RSDATA> BALSum;
	
	public String getABEND() {
		return ABEND;
	}
	public void setABEND(String aBEND) {
		ABEND = aBEND;
	}
	public String get__OCCURS() {
		return __OCCURS;
	}
	public void set__OCCURS(String __OCCURS) {
		this.__OCCURS = __OCCURS;
	}
	public String getRESERVE() {
		return RESERVE;
	}
	public void setRESERVE(String rESERVE) {
		RESERVE = rESERVE;
	}
	public String getCOUNT() {
		return COUNT;
	}
	public void setCOUNT(String cOUNT) {
		COUNT = cOUNT;
	}
	public String getUSERDATA_X50() {
		return USERDATA_X50;
	}
	public void setUSERDATA_X50(String uSERDATA_X50) {
		USERDATA_X50 = uSERDATA_X50;
	}
	public String getNTDBAL() {
		return NTDBAL;
	}
	public void setNTDBAL(String nTDBAL) {
		NTDBAL = nTDBAL;
	}
	public String getACN() {
		return ACN;
	}
	public void setACN(String aCN) {
		ACN = aCN;
	}
	public String getAVAILABLE() {
		return AVAILABLE;
	}
	public void setAVAILABLE(String aVAILABLE) {
		AVAILABLE = aVAILABLE;
	}
	public String getOCCURS_VECTOR_FOR_MULTIPLE_INDEX() {
		return OCCURS_VECTOR_FOR_MULTIPLE_INDEX;
	}
	public void setOCCURS_VECTOR_FOR_MULTIPLE_INDEX(String oCCURS_VECTOR_FOR_MULTIPLE_INDEX) {
		OCCURS_VECTOR_FOR_MULTIPLE_INDEX = oCCURS_VECTOR_FOR_MULTIPLE_INDEX;
	}
	public String getTOTAL_CLR() {
		return TOTAL_CLR;
	}
	public void setTOTAL_CLR(String tOTAL_CLR) {
		TOTAL_CLR = tOTAL_CLR;
	}
	public String getCUID() {
		return CUID;
	}
	public void setCUID(String cUID) {
		CUID = cUID;
	}
	public String getCHGCOD() {
		return CHGCOD;
	}
	public void setCHGCOD(String cHGCOD) {
		CHGCOD = cHGCOD;
	}
	public String getBALANCE() {
		return BALANCE;
	}
	public void setBALANCE(String bALANCE) {
		BALANCE = bALANCE;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public LinkedList<N510_REST_RSDATA> getACNInfo() {
		return ACNInfo;
	}
	public void setACNInfo(LinkedList<N510_REST_RSDATA> aCNInfo) {
		ACNInfo = aCNInfo;
	}
	public LinkedList<N510_REST_RSDATA> getAVASum() {
		return AVASum;
	}
	public void setAVASum(LinkedList<N510_REST_RSDATA> aVASum) {
		AVASum = aVASum;
	}
	public LinkedList<N510_REST_RSDATA> getBALSum() {
		return BALSum;
	}
	public void setBALSum(LinkedList<N510_REST_RSDATA> bALSum) {
		BALSum = bALSum;
	}
	
}
