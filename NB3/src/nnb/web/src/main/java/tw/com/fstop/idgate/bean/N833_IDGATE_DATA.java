package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class N833_IDGATE_DATA implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -7811110952040045093L;

	@SerializedName(value = "CUSIDN")
	private String CUSIDN;
	
	@SerializedName(value = "TSFACN")
	private String TSFACN;
	
	@SerializedName(value = "CARDNUM")
	private String CARDNUM;

	@SerializedName(value = "ADOPID")
	private String ADOPID;
	
	@SerializedName(value = "TYPE")
	private String TYPE;
	
	@SerializedName(value = "ITMNUM")
	private String ITMNUM;
	
	@SerializedName(value = "UNTNUM")
	private String UNTNUM;

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getTSFACN() {
		return TSFACN;
	}

	public void setTSFACN(String tSFACN) {
		TSFACN = tSFACN;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}

	public String getTYPE() {
		return TYPE;
	}

	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}

	public String getITMNUM() {
		return ITMNUM;
	}

	public void setITMNUM(String iTMNUM) {
		ITMNUM = iTMNUM;
	}

	public String getUNTNUM() {
		return UNTNUM;
	}

	public void setUNTNUM(String uNTNUM) {
		UNTNUM = uNTNUM;
	}

	public String getCARDNUM() {
		return CARDNUM;
	}

	public void setCARDNUM(String cARDNUM) {
		CARDNUM = cARDNUM;
	}
}
