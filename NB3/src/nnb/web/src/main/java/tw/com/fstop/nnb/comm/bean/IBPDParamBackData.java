package tw.com.fstop.nnb.comm.bean;

import java.io.Serializable;

public class IBPDParamBackData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7336461798101794336L;
	
	private String SrcID;
	private String KeyID;
	private String DivData;
	private String ICV;
	private String MAC;
	private String TxnDatetime;
	private String STAN;
	private String RCode;
	private String BusType;
	private String TxnType="00";
	private String CustIdnBan;
	private String CustBankId="0500000";
	private String BillerBan;
	private String BillerID;
	private String PaymentType;
	private String FeeID;
	private String CustBankAcnt;
	public String getSrcID() {
		return SrcID;
	}
	public void setSrcID(String srcID) {
		SrcID = srcID;
	}
	public String getKeyID() {
		return KeyID;
	}
	public void setKeyID(String keyID) {
		KeyID = keyID;
	}
	public String getDivData() {
		return DivData;
	}
	public void setDivData(String divData) {
		DivData = divData;
	}
	public String getICV() {
		return ICV;
	}
	public void setICV(String iCV) {
		ICV = iCV;
	}
	public String getMAC() {
		return MAC;
	}
	public void setMAC(String mAC) {
		MAC = mAC;
	}
	public String getTxnDatetime() {
		return TxnDatetime;
	}
	public void setTxnDatetime(String txnDatetime) {
		TxnDatetime = txnDatetime;
	}
	public String getSTAN() {
		return STAN;
	}
	public void setSTAN(String sTAN) {
		STAN = sTAN;
	}
	public String getRCode() {
		return RCode;
	}
	public void setRCode(String rCode) {
		RCode = rCode;
	}
	public String getBusType() {
		return BusType;
	}
	public void setBusType(String busType) {
		BusType = busType;
	}
	public String getTxnType() {
		return TxnType;
	}
	public void setTxnType(String txnType) {
		TxnType = txnType;
	}
	public String getCustIdnBan() {
		return CustIdnBan;
	}
	public void setCustIdnBan(String custIdnBan) {
		CustIdnBan = custIdnBan;
	}
	public String getCustBankId() {
		return CustBankId;
	}
	public void setCustBankId(String custBankId) {
		CustBankId = custBankId;
	}
	public String getBillerBan() {
		return BillerBan;
	}
	public void setBillerBan(String billerBan) {
		BillerBan = billerBan;
	}
	public String getBillerID() {
		return BillerID;
	}
	public void setBillerID(String billerID) {
		BillerID = billerID;
	}
	public String getPaymentType() {
		return PaymentType;
	}
	public void setPaymentType(String paymentType) {
		PaymentType = paymentType;
	}
	public String getFeeID() {
		return FeeID;
	}
	public void setFeeID(String feeID) {
		FeeID = feeID;
	}
	public String getCustBankAcnt() {
		return CustBankAcnt;
	}
	public void setCustBankAcnt(String custBankAcnt) {
		CustBankAcnt = custBankAcnt;
	}

}
