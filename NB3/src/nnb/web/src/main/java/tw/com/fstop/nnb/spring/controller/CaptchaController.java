package tw.com.fstop.nnb.spring.controller;

import java.awt.image.BufferedImage;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.google.code.kaptcha.Producer;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.StrUtil;

@Controller  
@RequestMapping("/CAPCODE")
@SessionAttributes({SessionUtil.KAPTCHA_SESSION_KEY, SessionUtil.TRANS_KAPTCHA_SESSION_KEY})
public class CaptchaController {

	static Logger log = LoggerFactory.getLogger(CaptchaController.class);

	// 登入用
	@Autowired
    private Producer captchaProducer = null;
	// 交易用
	@Autowired
	private Producer captchaProducer_trans = null;
	
	@Autowired
	private I18n i18n;

	/**
	 * 取得登入用驗證碼
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/captcha_image")
    public ModelAndView getKaptchaImage(HttpServletRequest request, HttpServletResponse response,
    		@RequestParam Map<String, String> reqParam, Model model) throws Exception {

		// response設定
		response.setDateHeader("Expires", 0);
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        response.setContentType("image/jpeg");
        
        // session原驗證碼
        String code = (String) SessionUtil.getAttribute(model, SessionUtil.KAPTCHA_SESSION_KEY, null);
        log.trace("SCapCode: " + code );
        
        // 新驗證碼存session
        String capText = captchaProducer.createText();
        SessionUtil.addAttribute(model, SessionUtil.KAPTCHA_SESSION_KEY, capText);
        
        // session新驗證碼
        code = (String) SessionUtil.getAttribute(model, SessionUtil.KAPTCHA_SESSION_KEY, null);
        log.trace("final_SCapCode: " + code );
        log.trace(ESAPIUtil.vaildLog("final_SCapCode.sessionId: " + request.getSession().getId()));

        // 輸出驗證碼圖像
        BufferedImage bi = captchaProducer.createImage(capText);
        ServletOutputStream out = response.getOutputStream();
        ImageIO.write(bi, "jpg", out);
        try {
            out.flush();
        } finally {
            out.close();
        }
        return null;  
    }
	/**
	 * 驗證登入驗證碼
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/captcha_valided")
    public @ResponseBody BaseResult validedCapCode(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) throws Exception {
		log.trace("capCode.mapping...");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// 取得request的驗證碼
			String capCode = StrUtil.isEmpty((reqParam.get("capCode"))) ? "" : reqParam.get("capCode");
			log.trace(ESAPIUtil.vaildLog("capCode >> " + capCode));
			// 取得session的驗證碼
			String code = (String) SessionUtil.getAttribute(model, SessionUtil.KAPTCHA_SESSION_KEY, null);
			log.trace(ESAPIUtil.vaildLog("sessionCapCode >> " + code));
			if(code == null) {
				bs.setResult(Boolean.FALSE);
				bs.setMessage(i18n.getMsg("LB.X1758"));
				log.trace("SESSION TIMEOUT!!!");
				return bs;
			}
			
			// 通過驗證
			log.debug("captcha_valided: " + capCode.equalsIgnoreCase(code) );
			if(capCode.equalsIgnoreCase(code)) {
				bs.setResult(Boolean.TRUE);
				bs.setMessage(i18n.getMsg("LB.X1760"));
				log.trace("capCode.mapping...success!!!");
			} else {
				bs.setResult(Boolean.FALSE);
				bs.setMessage(i18n.getMsg("LB.X1082"));
				log.trace("capCode.mapping...fail!!!");
			}

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("validedCapCode error >> {}",e);
		}

		return bs;
	}
	
	/**
	 * 取得交易用驗證碼
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/captcha_image_trans")
	public ModelAndView getKaptchaImage_trans(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) throws Exception {
		
		// response設定
		response.setDateHeader("Expires", 0);
		response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");
		response.setHeader("Pragma", "no-cache");
		response.setContentType("image/jpeg");
		
		// session原驗證碼
		String code = (String) SessionUtil.getAttribute(model, SessionUtil.TRANS_KAPTCHA_SESSION_KEY, null);
		log.trace("SCapCode: " + code );
		
		// 新驗證碼存session
		String capText = captchaProducer_trans.createText();
		SessionUtil.addAttribute(model, SessionUtil.TRANS_KAPTCHA_SESSION_KEY, capText);
		
		// session新驗證碼
		code = (String) SessionUtil.getAttribute(model, SessionUtil.TRANS_KAPTCHA_SESSION_KEY, null);
		log.trace("final_SCapCode: " + code );
		
		// 輸出驗證碼圖像
		BufferedImage bi = captchaProducer_trans.createImage(capText);
		ServletOutputStream out = response.getOutputStream();
		ImageIO.write(bi, "jpg", out);
		try {
			out.flush();
		} finally {
			out.close();
		}
		return null;  
	}
	/**
	 * 驗證交易驗證碼
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/captcha_valided_trans")
	public @ResponseBody BaseResult validedCapCode_trans(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) throws Exception {
		log.trace("capCode.mapping...");
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// 取得request的驗證碼
			String capCode = StrUtil.isEmpty((reqParam.get("capCode"))) ? "" : reqParam.get("capCode");
			log.trace(ESAPIUtil.vaildLog("capCode >> " + capCode));
			// 取得session的驗證碼
			String code = (String) SessionUtil.getAttribute(model, SessionUtil.TRANS_KAPTCHA_SESSION_KEY, null);
			log.trace(ESAPIUtil.vaildLog("sessionCapCode >> " + code));
			// 通過驗證
			log.debug("captcha_valided: " + code.equalsIgnoreCase(capCode) );
			if(code.equalsIgnoreCase(capCode)) {
				bs.setResult(Boolean.TRUE);
				bs.setMessage(i18n.getMsg("LB.X1760"));
				log.trace("capCode.mapping...success!!!");
			}else {
				bs.setResult(Boolean.FALSE);
				bs.setMessage(i18n.getMsg("LB.X1761"));
				log.trace("capCode.mapping...fail!!!");
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("validedCapCode_trans error >> {}",e);
		}
		
		return bs;
	}
	
	
	
	
}
