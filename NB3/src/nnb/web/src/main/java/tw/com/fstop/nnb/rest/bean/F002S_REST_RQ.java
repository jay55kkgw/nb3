package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class F002S_REST_RQ extends BaseRestBean_FX implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1146048707430802794L;

	private String iSeqNo; // iSeqNo

	private String pkcs7Sign;// IKEY

	private String jsondc;// IKEY

	private String PINNEW;// 網路銀行密碼（新）SSL 用

	private String BENACC; // 轉入帳號

	private String BENID; // 收款人統編

	private String BENNM1; // 收款人名稱1

	private String BENTYPE; // 受款人身分別

	private String BGROENO; // 人工議價編號

	private String CMMAILMEMO;// 摘要內容

	private String CMTRMEMO;// 交易備註

	private String CMTRMAIL;// 通訊錄

	private String CMTRDATE;// 預約指定日期

	private String CMSDATE;// 預約週期起日

	private String CMEDATE;// 預約週期迄日

	private String CMPERIOD;// 預約每月日期

	private String COUNTRY; // 交易國別

	private String CUSIDN; // 付款人統一編號

	private String COMMCCY; // 手續費幣別

	private String COMMACC; // 手續費帳號

	private String CUSTACC; // 轉出帳號

	private String CUSTYPE; // 客戶身分別

	private String CURAMT; // 議價金額 9(13)V9(2)

	private String DETCHG; // 手續費分擔方式

	private String MEMO1; // 匯款附言1

	private String NAME;// 付款人名稱

	private String PAYDATE; // 付款日期

	private String PAYCCY; // 轉出幣別

	private String PAYREMIT; // 轉出轉入別

	private String REMITCY; // 轉入幣別

	private String SRCFUND; // 匯款分類編號

	private String FGTRDATE; // (0 )即時或(1) 預約或(2)預約固定每月的

	// 交易單據用
	private String display_BENTYPE;// 收款人身份別

	private String SRCFUNDDESC;// 匯款分類項目

	private String FXRMTDESC;// 匯款分類說明

	private String ADOPID;
	
	private String FXRCVBKADDR;

	//IDGATE
	private String sessionID;
	private String idgateID;
	private String txnID;

	public String getFXRCVBKADDR() {
		return FXRCVBKADDR;
	}

	public void setFXRCVBKADDR(String fXRCVBKADDR) {
		FXRCVBKADDR = fXRCVBKADDR;
	}

	public String getADOPID() {
		return ADOPID;
	}

	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	
	public String getiSeqNo()
	{
		return iSeqNo;
	}

	public void setiSeqNo(String iSeqNo)
	{
		this.iSeqNo = iSeqNo;
	}

	public String getPkcs7Sign()
	{
		return pkcs7Sign;
	}

	public void setPkcs7Sign(String pkcs7Sign)
	{
		this.pkcs7Sign = pkcs7Sign;
	}

	public String getJsondc()
	{
		return jsondc;
	}

	public void setJsondc(String jsondc)
	{
		this.jsondc = jsondc;
	}

	public String getPINNEW()
	{
		return PINNEW;
	}

	public void setPINNEW(String pINNEW)
	{
		PINNEW = pINNEW;
	}

	public String getCMTRMEMO()
	{
		return CMTRMEMO;
	}

	public void setCMTRMEMO(String cMTRMEMO)
	{
		CMTRMEMO = cMTRMEMO;
	}

	public String getCMTRMAIL()
	{
		return CMTRMAIL;
	}

	public void setCMTRMAIL(String cMTRMAIL)
	{
		CMTRMAIL = cMTRMAIL;
	}

	public String getCMMAILMEMO()
	{
		return CMMAILMEMO;
	}

	public void setCMMAILMEMO(String cMMAILMEMO)
	{
		CMMAILMEMO = cMMAILMEMO;
	}

	public String getCUSIDN()
	{
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN)
	{
		CUSIDN = cUSIDN;
	}

	public String getCUSTYPE()
	{
		return CUSTYPE;
	}

	public void setCUSTYPE(String cUSTYPE)
	{
		CUSTYPE = cUSTYPE;
	}

	public String getPAYDATE()
	{
		return PAYDATE;
	}

	public void setPAYDATE(String pAYDATE)
	{
		PAYDATE = pAYDATE;
	}

	public String getPAYCCY()
	{
		return PAYCCY;
	}

	public void setPAYCCY(String pAYCCY)
	{
		PAYCCY = pAYCCY;
	}

	public String getREMITCY()
	{
		return REMITCY;
	}

	public void setREMITCY(String rEMITCY)
	{
		REMITCY = rEMITCY;
	}

	public String getBGROENO()
	{
		return BGROENO;
	}

	public void setBGROENO(String bGROENO)
	{
		BGROENO = bGROENO;
	}

	public String getBENACC()
	{
		return BENACC;
	}

	public void setBENACC(String bENACC)
	{
		BENACC = bENACC;
	}

	public String getBENID()
	{
		return BENID;
	}

	public void setBENID(String bENID)
	{
		BENID = bENID;
	}

	public String getBENNM1()
	{
		return BENNM1;
	}

	public void setBENNM1(String bENNM1)
	{
		BENNM1 = bENNM1;
	}

	public String getBENTYPE()
	{
		return BENTYPE;
	}

	public void setBENTYPE(String bENTYPE)
	{
		BENTYPE = bENTYPE;
	}

	public String getSRCFUND()
	{
		return SRCFUND;
	}

	public void setSRCFUND(String sRCFUND)
	{
		SRCFUND = sRCFUND;
	}

	public String getCOUNTRY()
	{
		return COUNTRY;
	}

	public void setCOUNTRY(String cOUNTRY)
	{
		COUNTRY = cOUNTRY;
	}

	public String getDETCHG()
	{
		return DETCHG;
	}

	public void setDETCHG(String dETCHG)
	{
		DETCHG = dETCHG;
	}

	public String getNAME()
	{
		return NAME;
	}

	public void setNAME(String nAME)
	{
		NAME = nAME;
	}

	public String getCOMMCCY()
	{
		return COMMCCY;
	}

	public void setCOMMCCY(String cOMMCCY)
	{
		COMMCCY = cOMMCCY;
	}

	public String getCOMMACC()
	{
		return COMMACC;
	}

	public void setCOMMACC(String cOMMACC)
	{
		COMMACC = cOMMACC;
	}

	public String getCUSTACC()
	{
		return CUSTACC;
	}

	public void setCUSTACC(String cUSTACC)
	{
		CUSTACC = cUSTACC;
	}

	public String getPAYREMIT()
	{
		return PAYREMIT;
	}

	public void setPAYREMIT(String pAYREMIT)
	{
		PAYREMIT = pAYREMIT;
	}

	public String getCURAMT()
	{
		return CURAMT;
	}

	public void setCURAMT(String cURAMT)
	{
		CURAMT = cURAMT;
	}

	public String getMEMO1()
	{
		return MEMO1;
	}

	public void setMEMO1(String mEMO1)
	{
		MEMO1 = mEMO1;
	}

	public String getFGTRDATE()
	{
		return FGTRDATE;
	}

	public void setFGTRDATE(String fGTRDATE)
	{
		FGTRDATE = fGTRDATE;
	}

	public String getCMTRDATE()
	{
		return CMTRDATE;
	}

	public void setCMTRDATE(String cMTRDATE)
	{
		CMTRDATE = cMTRDATE;
	}

	public String getCMSDATE()
	{
		return CMSDATE;
	}

	public void setCMSDATE(String cMSDATE)
	{
		CMSDATE = cMSDATE;
	}

	public String getCMEDATE()
	{
		return CMEDATE;
	}

	public void setCMEDATE(String cMEDATE)
	{
		CMEDATE = cMEDATE;
	}

	public String getCMPERIOD()
	{
		return CMPERIOD;
	}

	public void setCMPERIOD(String cMPERIOD)
	{
		CMPERIOD = cMPERIOD;
	}

	public String getDisplay_BENTYPE()
	{
		return display_BENTYPE;
	}

	public void setDisplay_BENTYPE(String display_BENTYPE)
	{
		this.display_BENTYPE = display_BENTYPE;
	}

	public String getSRCFUNDDESC()
	{
		return SRCFUNDDESC;
	}

	public void setSRCFUNDDESC(String sRCFUNDDESC)
	{
		SRCFUNDDESC = sRCFUNDDESC;
	}

	public String getFXRMTDESC()
	{
		return FXRMTDESC;
	}

	public void setFXRMTDESC(String fXRMTDESC)
	{
		FXRMTDESC = fXRMTDESC;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public String getIdgateID() {
		return idgateID;
	}

	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}

	public String getTxnID() {
		return txnID;
	}

	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}

}
