package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class F001S_IDGATE_DATA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -476008617282996242L;

	
	@SerializedName(value = "CUSIDN")
	private String CUSIDN; // 使用者統編
	
	@SerializedName(value = "PAYDATE")
	private String PAYDATE; // 轉帳日期
	
	@SerializedName(value = "CUSTACC")
	private String CUSTACC; // 轉出帳號
	
	@SerializedName(value = "CURAMT")
	private String CURAMT; // 轉帳金額
	
	@SerializedName(value = "BENACC")
	private String BENACC; // 轉入帳號
	
	@SerializedName(value = "COMMCCY")
	private String COMMCCY; // 手續費幣別
	
	@SerializedName(value = "REMITCY")
	private String REMITCY; // 轉入幣別
	
	@SerializedName(value = "PAYCCY")
	private String PAYCCY; // 轉出幣別
	
	@SerializedName(value = "PAYREMIT")
	private String PAYREMIT; // 轉帳金額為轉出或轉入
	
	@SerializedName(value = "SRCFUND")
	private String SRCFUND; // 匯款分類編號
	
	@SerializedName(value = "FGINACNO")
	private String FGINACNO; // 約定/非約定

	
	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getPAYDATE() {
		return PAYDATE;
	}

	public void setPAYDATE(String pAYDATE) {
		PAYDATE = pAYDATE;
	}

	public String getCUSTACC() {
		return CUSTACC;
	}

	public void setCUSTACC(String cUSTACC) {
		CUSTACC = cUSTACC;
	}

	public String getCURAMT() {
		return CURAMT;
	}

	public void setCURAMT(String cURAMT) {
		CURAMT = cURAMT;
	}

	public String getBENACC() {
		return BENACC;
	}

	public void setBENACC(String bENACC) {
		BENACC = bENACC;
	}

	public String getCOMMCCY() {
		return COMMCCY;
	}

	public void setCOMMCCY(String cOMMCCY) {
		COMMCCY = cOMMCCY;
	}

	public String getREMITCY() {
		return REMITCY;
	}

	public void setREMITCY(String rEMITCY) {
		REMITCY = rEMITCY;
	}

	public String getPAYCCY() {
		return PAYCCY;
	}

	public void setPAYCCY(String pAYCCY) {
		PAYCCY = pAYCCY;
	}

	public String getPAYREMIT() {
		return PAYREMIT;
	}

	public void setPAYREMIT(String pAYREMIT) {
		PAYREMIT = pAYREMIT;
	}

	public String getSRCFUND() {
		return SRCFUND;
	}

	public void setSRCFUND(String sRCFUND) {
		SRCFUND = sRCFUND;
	}

	public String getFGINACNO() {
		return FGINACNO;
	}

	public void setFGINACNO(String fGINACNO) {
		FGINACNO = fGINACNO;
	}

	
}
