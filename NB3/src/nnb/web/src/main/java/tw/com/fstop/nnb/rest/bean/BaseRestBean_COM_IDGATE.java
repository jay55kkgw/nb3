package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

import tw.com.fstop.web.util.ConfingManager;

public class BaseRestBean_COM_IDGATE extends BaseRestBean implements Serializable {

	
	
	private static String ms_Channel;
	
	public static String getMs_Channel() {
		return ms_Channel;
	}

	public static void setMs_Channel(String ms_Channel) {
		switch (ms_Channel) {
			case "ms_tw":
				BaseRestBean_COM_IDGATE.ms_Channel = ConfingManager.MS_TW_COM_IDGATE_PATH;
				break;
			case "ms_fx":
				BaseRestBean_COM_IDGATE.ms_Channel = ConfingManager.MS_FX_COM_IDGATE_PATH;
				break;
			case "ms_cc":
				BaseRestBean_COM_IDGATE.ms_Channel = ConfingManager.MS_CC_COM_IDGATE_PATH;
				break;
			case "ms_loan":
				BaseRestBean_COM_IDGATE.ms_Channel = ConfingManager.MS_LOAN_COM_IDGATE_PATH;
				break;
			case "ms_pay":
				BaseRestBean_COM_IDGATE.ms_Channel = ConfingManager.MS_PAY_COM_IDGATE_PATH;
				break;
			case "ms_ps":
				BaseRestBean_COM_IDGATE.ms_Channel = ConfingManager.MS_PS_COM_IDGATE_PATH;
				break;
			case "ms_ola":
				BaseRestBean_COM_IDGATE.ms_Channel = ConfingManager.MS_OLA_COM_IDGATE_PATH;
				break;
			case "ms_ols":
				BaseRestBean_COM_IDGATE.ms_Channel = ConfingManager.MS_OLS_COM_IDGATE_PATH;
				break;
			case "ms_gold":
				BaseRestBean_COM_IDGATE.ms_Channel = ConfingManager.MS_GOLD_COM_IDGATE_PATH;
				break;
			case "ms_fund":
				BaseRestBean_COM_IDGATE.ms_Channel = ConfingManager.MS_FUND_COM_IDGATE_PATH;
				break;
		}
	}

	public static String getAPI_Name(Class<?> retClass) {
		if (ms_Channel.endsWith("/") == false) {
			ms_Channel = ms_Channel + "/";
		}
		return ms_Channel + retClass.getSimpleName().replace("_REST_RQ", "");
	}

}
