package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N323_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4114691562734911293L;

	String INTRO;
	String __OCCURS;
	String TYPE;

	public String getINTRO() {
		return INTRO;
	}

	public void setINTRO(String iNTRO) {
		INTRO = iNTRO;
	}

	public String get__OCCURS() {
		return __OCCURS;
	}

	public void set__OCCURS(String __OCCURS) {
		this.__OCCURS = __OCCURS;
	}

	public String getTYPE() {
		return TYPE;
	}

	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
}
