package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * 信用卡查詢RS
 */
public class N812_REST_RS extends BaseRestBean implements Serializable{
	private static final long serialVersionUID = 9047844481338101277L;
	
	private String RSPCOD;
	private String SEQ;
	private String CNT;
	private String CRDLIMIT;
	private String USEDLIMIT;
	private String USEDCASH;
	private String OVRPAY;
	private LinkedList<N812_REST_RSDATA> REC;
	
	public String getRSPCOD(){
		return RSPCOD;
	}
	public void setRSPCOD(String rSPCOD){
		RSPCOD = rSPCOD;
	}
	public String getSEQ(){
		return SEQ;
	}
	public void setSEQ(String sEQ){
		SEQ = sEQ;
	}
	public String getCNT(){
		return CNT;
	}
	public void setCNT(String cNT){
		CNT = cNT;
	}
	public String getCRDLIMIT(){
		return CRDLIMIT;
	}
	public void setCRDLIMIT(String cRDLIMIT){
		CRDLIMIT = cRDLIMIT;
	}
	public String getUSEDLIMIT(){
		return USEDLIMIT;
	}
	public void setUSEDLIMIT(String uSEDLIMIT){
		USEDLIMIT = uSEDLIMIT;
	}
	public String getUSEDCASH(){
		return USEDCASH;
	}
	public void setUSEDCASH(String uSEDCASH){
		USEDCASH = uSEDCASH;
	}
	public String getOVRPAY(){
		return OVRPAY;
	}
	public void setOVRPAY(String oVRPAY){
		OVRPAY = oVRPAY;
	}
	public LinkedList<N812_REST_RSDATA> getREC(){
		return REC;
	}
	public void setREC(LinkedList<N812_REST_RSDATA> rEC){
		REC = rEC;
	}	
}