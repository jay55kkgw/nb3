package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class TD01_MAIL_REST_RS extends BaseRestBean implements Serializable {

	private static final long serialVersionUID = 9018187800581094918L;
	/**
	 * 
	 */
	private String CMMAIL;
	private String CMMSG;
	private String CMQTIME;
	private String RESULT;
	
	public String getRESULT() {
		return RESULT;
	}
	public void setRESULT(String rESULT) {
		RESULT = rESULT;
	}
	public String getCMMAIL() {
		return CMMAIL;
	}
	public String getCMMSG() {
		return CMMSG;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMMAIL(String cMMAIL) {
		CMMAIL = cMMAIL;
	}
	public void setCMMSG(String cMMSG) {
		CMMSG = cMMSG;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	
	
	
}

