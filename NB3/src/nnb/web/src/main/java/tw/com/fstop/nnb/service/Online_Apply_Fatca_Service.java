package tw.com.fstop.nnb.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import fstop.orm.po.CITIES;

/**
 * FATCA及CRS個人客戶自我聲明書暨個人資料同意書Service
 */
@Service
public class Online_Apply_Fatca_Service {

	@Autowired
	Cities_Service cities_service;
	
	
	/**
	 * 準備FATCA及CRS個人客戶自我聲明書暨個人資料同意書的資料 
	 */
	@SuppressWarnings("unchecked")
	public void prepareFatcaConsentData(Map<String,String> requestParam,Model model) {
		
		List<CITIES> cityList = cities_service.getCities();
		model.addAttribute("cityList", cityList);
	}
}
