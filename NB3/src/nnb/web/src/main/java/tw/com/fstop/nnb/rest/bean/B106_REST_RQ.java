package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class B106_REST_RQ extends BaseRestBean_FUND implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5023910838555582867L;

	private String EXECUTEFUNCTION;
	private String ADLEADBYTE;
	private String LASTTIME;
	private String LASTDATE;
	private String ADBANK3MAIL;
	private String TXNTWAMTMAIL;
	
	public String getEXECUTEFUNCTION() {
		return EXECUTEFUNCTION;
	}
	public void setEXECUTEFUNCTION(String eXECUTEFUNCTION) {
		EXECUTEFUNCTION = eXECUTEFUNCTION;
	}
	public String getADLEADBYTE() {
		return ADLEADBYTE;
	}
	public void setADLEADBYTE(String aDLEADBYTE) {
		ADLEADBYTE = aDLEADBYTE;
	}
	public String getLASTTIME() {
		return LASTTIME;
	}
	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}
	public String getLASTDATE() {
		return LASTDATE;
	}
	public void setLASTDATE(String lASTDATE) {
		LASTDATE = lASTDATE;
	}
	public String getADBANK3MAIL() {
		return ADBANK3MAIL;
	}
	public void setADBANK3MAIL(String aDBANK3MAIL) {
		ADBANK3MAIL = aDBANK3MAIL;
	}
	public String getTXNTWAMTMAIL() {
		return TXNTWAMTMAIL;
	}
	public void setTXNTWAMTMAIL(String tXNTWAMTMAIL) {
		TXNTWAMTMAIL = tXNTWAMTMAIL;
	}
}
