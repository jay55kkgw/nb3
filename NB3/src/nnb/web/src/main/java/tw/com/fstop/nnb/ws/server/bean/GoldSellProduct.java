package tw.com.fstop.nnb.ws.server.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GoldSellProduct {

	public String sale_kind;
	
	public String goodno;
	
	public String unit_price;
	
	public String sale_amt;
	
	public String tax_amt;
	
	public String discount_amt;
	
	public String sale_q;
	
	public String take_q;
	
	public String unit_price_g;
	
	public String dpdiff;

	public String getSale_kind() {
		return sale_kind;
	}

	public void setSale_kind(String sale_kind) {
		this.sale_kind = sale_kind;
	}

	public String getGoodno() {
		return goodno;
	}

	public void setGoodno(String goodno) {
		this.goodno = goodno;
	}

	public String getUnit_price() {
		return unit_price;
	}

	public void setUnit_price(String unit_price) {
		this.unit_price = unit_price;
	}

	public String getSale_amt() {
		return sale_amt;
	}

	public void setSale_amt(String sale_amt) {
		this.sale_amt = sale_amt;
	}

	public String getTax_amt() {
		return tax_amt;
	}

	public void setTax_amt(String tax_amt) {
		this.tax_amt = tax_amt;
	}

	public String getDiscount_amt() {
		return discount_amt;
	}

	public void setDiscount_amt(String discount_amt) {
		this.discount_amt = discount_amt;
	}

	public String getSale_q() {
		return sale_q;
	}

	public void setSale_q(String sale_q) {
		this.sale_q = sale_q;
	}

	public String getTake_q() {
		return take_q;
	}

	public void setTake_q(String take_q) {
		this.take_q = take_q;
	}

	public String getUnit_price_g() {
		return unit_price_g;
	}

	public void setUnit_price_g(String unit_price_g) {
		this.unit_price_g = unit_price_g;
	}

	public String getDpdiff() {
		return dpdiff;
	}

	public void setDpdiff(String dpdiff) {
		this.dpdiff = dpdiff;
	}
	
	
}
