package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * C031電文RQ
 */
public class C031_Public_REST_RQ extends BaseRestBean_FUND implements Serializable{
	private static final long serialVersionUID = 6868861910004819383L;
	private String CUSIDN;
	private String TRANSCODE;
	private String COUNTRYTYPE;
	private String AMT3;
	private String BILLSENDMODE;
	private String PAYDAY1;
	private String PAYDAY2;
	private String PAYDAY3;
	private String BRHCOD;
	private String CUTTYPE;
	private String HTELPHONE;
	private String STOP;
	private String YIELD;
	private String MAC;
	private String PAYDAY4;
	private String PAYDAY5;
	private String PAYDAY6;
	private String MIP;
	private String PAYDAY7;
	private String PAYDAY8;
	private String PAYDAY9;
	private String FDAGREEFLAG;
	private String FDNOTICETYPE;
	private String FDPUBLICTYPE;
	private String ADOPID = "C031_1";
	private String FUNDLNAME;
	private String SALESNO; //推薦行員代號
	private String SLSNO; //客戶輸入推薦行員代號
	private String KYCNO; //KYC評估人員代號
	
	public String getCUSIDN(){
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN){
		CUSIDN = cUSIDN;
	}
	public String getTRANSCODE(){
		return TRANSCODE;
	}
	public void setTRANSCODE(String tRANSCODE){
		TRANSCODE = tRANSCODE;
	}
	public String getCOUNTRYTYPE(){
		return COUNTRYTYPE;
	}
	public void setCOUNTRYTYPE(String cOUNTRYTYPE){
		COUNTRYTYPE = cOUNTRYTYPE;
	}
	public String getAMT3(){
		return AMT3;
	}
	public void setAMT3(String aMT3){
		AMT3 = aMT3;
	}
	public String getBILLSENDMODE(){
		return BILLSENDMODE;
	}
	public void setBILLSENDMODE(String bILLSENDMODE){
		BILLSENDMODE = bILLSENDMODE;
	}
	public String getPAYDAY1(){
		return PAYDAY1;
	}
	public void setPAYDAY1(String pAYDAY1){
		PAYDAY1 = pAYDAY1;
	}
	public String getPAYDAY2(){
		return PAYDAY2;
	}
	public void setPAYDAY2(String pAYDAY2){
		PAYDAY2 = pAYDAY2;
	}
	public String getPAYDAY3(){
		return PAYDAY3;
	}
	public void setPAYDAY3(String pAYDAY3){
		PAYDAY3 = pAYDAY3;
	}
	public String getBRHCOD(){
		return BRHCOD;
	}
	public void setBRHCOD(String bRHCOD){
		BRHCOD = bRHCOD;
	}
	public String getCUTTYPE(){
		return CUTTYPE;
	}
	public void setCUTTYPE(String cUTTYPE){
		CUTTYPE = cUTTYPE;
	}
	public String getHTELPHONE(){
		return HTELPHONE;
	}
	public void setHTELPHONE(String hTELPHONE){
		HTELPHONE = hTELPHONE;
	}
	public String getSTOP(){
		return STOP;
	}
	public void setSTOP(String sTOP){
		STOP = sTOP;
	}
	public String getYIELD(){
		return YIELD;
	}
	public void setYIELD(String yIELD){
		YIELD = yIELD;
	}
	public String getMAC(){
		return MAC;
	}
	public void setMAC(String mAC){
		MAC = mAC;
	}
	public String getPAYDAY4(){
		return PAYDAY4;
	}
	public void setPAYDAY4(String pAYDAY4){
		PAYDAY4 = pAYDAY4;
	}
	public String getPAYDAY5(){
		return PAYDAY5;
	}
	public void setPAYDAY5(String pAYDAY5){
		PAYDAY5 = pAYDAY5;
	}
	public String getPAYDAY6(){
		return PAYDAY6;
	}
	public void setPAYDAY6(String pAYDAY6){
		PAYDAY6 = pAYDAY6;
	}
	public String getMIP(){
		return MIP;
	}
	public void setMIP(String mIP){
		MIP = mIP;
	}
	public String getPAYDAY7(){
		return PAYDAY7;
	}
	public void setPAYDAY7(String pAYDAY7){
		PAYDAY7 = pAYDAY7;
	}
	public String getPAYDAY8(){
		return PAYDAY8;
	}
	public void setPAYDAY8(String pAYDAY8){
		PAYDAY8 = pAYDAY8;
	}
	public String getPAYDAY9(){
		return PAYDAY9;
	}
	public void setPAYDAY9(String pAYDAY9){
		PAYDAY9 = pAYDAY9;
	}
	public String getFDAGREEFLAG(){
		return FDAGREEFLAG;
	}
	public void setFDAGREEFLAG(String fDAGREEFLAG){
		FDAGREEFLAG = fDAGREEFLAG;
	}
	public String getFDNOTICETYPE(){
		return FDNOTICETYPE;
	}
	public void setFDNOTICETYPE(String fDNOTICETYPE){
		FDNOTICETYPE = fDNOTICETYPE;
	}
	public String getFDPUBLICTYPE(){
		return FDPUBLICTYPE;
	}
	public void setFDPUBLICTYPE(String fDPUBLICTYPE){
		FDPUBLICTYPE = fDPUBLICTYPE;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getFUNDLNAME() {
		return FUNDLNAME;
	}
	public void setFUNDLNAME(String fUNDLNAME) {
		FUNDLNAME = fUNDLNAME;
	}
	public String getSALESNO() {
		return SALESNO;
	}
	public void setSALESNO(String sALESNO) {
		SALESNO = sALESNO;
	}
	public String getSLSNO() {
		return SLSNO;
	}
	public void setSLSNO(String sLSNO) {
		SLSNO = sLSNO;
	}
	public String getKYCNO() {
		return KYCNO;
	}
	public void setKYCNO(String kYCNO) {
		KYCNO = kYCNO;
	}
}