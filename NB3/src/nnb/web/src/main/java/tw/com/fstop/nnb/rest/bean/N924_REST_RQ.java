package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N924_REST_RQ  extends BaseRestBean_FX implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3524393005912579460L;

	private String CUSIDN;//統一編號

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
}
