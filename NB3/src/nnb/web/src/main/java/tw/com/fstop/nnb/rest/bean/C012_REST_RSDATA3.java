package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * C012電文RSDATA3
 */
public class C012_REST_RSDATA3 implements Serializable{
	private static final long serialVersionUID = 7127169312136862734L;
	
	//未分配
	private String UNALLOTAMT;
	private String UNALLOTAMTCRY;
	
	public String getUNALLOTAMT(){
		return UNALLOTAMT;
	}
	public void setUNALLOTAMT(String uNALLOTAMT){
		UNALLOTAMT = uNALLOTAMT;
	}
	public String getUNALLOTAMTCRY(){
		return UNALLOTAMTCRY;
	}
	public void setUNALLOTAMTCRY(String uNALLOTAMTCRY){
		UNALLOTAMTCRY = uNALLOTAMTCRY;
	}
}