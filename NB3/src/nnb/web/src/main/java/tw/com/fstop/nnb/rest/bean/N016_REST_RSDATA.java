package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N016_REST_RSDATA implements Serializable {

	private static final long serialVersionUID = -2739572910855200803L;
	private String ACN;// 帳號
	private String TRNINT;//
	private String TRNPAL;//
	private String CURIPD;//
	private String BAL;//
	private String DATTRN;//
	private String SEQ;//
	
	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	public String getTRNINT() {
		return TRNINT;
	}

	public void setTRNINT(String tRNINT) {
		TRNINT = tRNINT;
	}

	public String getTRNPAL() {
		return TRNPAL;
	}

	public void setTRNPAL(String tRNPAL) {
		TRNPAL = tRNPAL;
	}

	public String getCURIPD() {
		return CURIPD;
	}

	public void setCURIPD(String cURIPD) {
		CURIPD = cURIPD;
	}

	public String getBAL() {
		return BAL;
	}

	public void setBAL(String bAL) {
		BAL = bAL;
	}

	public String getDATTRN() {
		return DATTRN;
	}

	public void setDATTRN(String dATTRN) {
		DATTRN = dATTRN;
	}

	public String getSEQ() {
		return SEQ;
	}

	public void setSEQ(String sEQ) {
		SEQ = sEQ;
	}

	

}
