package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N104_2_REST_RS extends BaseRestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1110615264994371739L;
	private String errMsg;
	private String BRHCOD;
	private String occurMsg;
	private String errMsgStr;
	
	public String getErrMsgStr() {
		return errMsgStr;
	}
	public void setErrMsgStr(String errMsgStr) {
		this.errMsgStr = errMsgStr;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	public String getBRHCOD() {
		return BRHCOD;
	}
	public void setBRHCOD(String bRHCOD) {
		BRHCOD = bRHCOD;
	}
	public String getOccurMsg() {
		return occurMsg;
	}
	public void setOccurMsg(String occurMsg) {
		this.occurMsg = occurMsg;
	}
}
