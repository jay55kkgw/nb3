package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N555_REST_RSDATA implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4362962210314432503L;
	
	String RBILLAMT;	//到單金額
	String RCFMDATE;	//承兌到期日
	String RADVFIXR;	//融資利率
	String RMARK;		//備註
	String RIBNO;		//到單編號
	String RINTSTDT;	//融資起日
	String RIBDATE;		//到單日期
	String RBILLCCY;	//幣別
	String RINTDUDT;	//融資迄日
	
	public String getRBILLAMT() {
		return RBILLAMT;
	}
	public void setRBILLAMT(String rBILLAMT) {
		RBILLAMT = rBILLAMT;
	}
	public String getRCFMDATE() {
		return RCFMDATE;
	}
	public void setRCFMDATE(String rCFMDATE) {
		RCFMDATE = rCFMDATE;
	}
	public String getRADVFIXR() {
		return RADVFIXR;
	}
	public void setRADVFIXR(String rADVFIXR) {
		RADVFIXR = rADVFIXR;
	}
	public String getRMARK() {
		return RMARK;
	}
	public void setRMARK(String rMARK) {
		RMARK = rMARK;
	}
	public String getRIBNO() {
		return RIBNO;
	}
	public void setRIBNO(String rIBNO) {
		RIBNO = rIBNO;
	}
	public String getRINTSTDT() {
		return RINTSTDT;
	}
	public void setRINTSTDT(String rINTSTDT) {
		RINTSTDT = rINTSTDT;
	}
	public String getRIBDATE() {
		return RIBDATE;
	}
	public void setRIBDATE(String rIBDATE) {
		RIBDATE = rIBDATE;
	}
	public String getRBILLCCY() {
		return RBILLCCY;
	}
	public void setRBILLCCY(String rBILLCCY) {
		RBILLCCY = rBILLCCY;
	}
	public String getRINTDUDT() {
		return RINTDUDT;
	}
	public void setRINTDUDT(String rINTDUDT) {
		RINTDUDT = rINTDUDT;
	}
}
