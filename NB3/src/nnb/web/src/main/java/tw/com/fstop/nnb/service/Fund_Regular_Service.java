package tw.com.fstop.nnb.service;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;

import fstop.orm.po.ADMCURRENCY;
import fstop.orm.po.TXNFUNDCOMPANY;
import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.util.ESAPIUtil;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.web.util.WebUtil;

/**
 * 基金定期（不）定額申購的Service
 */
@Service
public class Fund_Regular_Service extends Base_Service{
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private Fund_Transfer_Service fund_transfer_service;
	@Autowired
	private Fund_Purchase_Service fund_purchase_service;
	@Autowired
	private I18n i18n;
	/**
	 * 準備基金定期（不）定額選擇頁的資料
	 */
	@SuppressWarnings("unchecked")
	public void prepareFundRegularData(Map<String,String> requestParam,Model model){
		//未確認標記
	    String ConfirmFlag = requestParam.get("ConfirmFlag");
		if(ConfirmFlag != null && "TRUE".equals(ConfirmFlag)){
			model.addAttribute("ConfirmFlag",true);
		}
		
		List<Map<String,String>> finalFundCompanyList = new ArrayList<Map<String,String>>();
		List<TXNFUNDCOMPANY> fundCompanyList = fund_purchase_service.getFundCompany("B","C");
		
		if(fundCompanyList != null){
			Map<String,String> map;
			String COUNTRYTYPE;
			for(int x=0;x<fundCompanyList.size();x++){
				map = new HashMap<String,String>();
				
				map = CodeUtil.objectCovert(map.getClass(),fundCompanyList.get(x));
				
				COUNTRYTYPE = fundCompanyList.get(x).getCOUNTRYTYPE();
				log.debug(ESAPIUtil.vaildLog("COUNTRYTYPE={}" + COUNTRYTYPE));
				
				if("C".equals(COUNTRYTYPE) || "B".equals(COUNTRYTYPE)){
					map.put("seriesName",i18n.getMsg("LB.X1505"));//"國內系列"
				}
				else{
					map.put("seriesName",i18n.getMsg("LB.X1506"));//"國外系列"
				}
				
				finalFundCompanyList.add(map);
			}
		}
		log.debug(ESAPIUtil.vaildLog("finalFundCompanyList="+finalFundCompanyList));
		model.addAttribute("finalFundCompanyList",finalFundCompanyList);
	}
	
	public BaseResult getSalaryAccountData(@RequestParam Map<String, String> okMap) {
	
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			bs = C016Public_REST(okMap);
		} catch (Exception e) {
			log.error("getSalaryAccountData error >> {}",e);
		}
		return bs;
	}
	
	/**
	 * 準備前往定期不定額約定條款頁的資料
	 */
	public void prepareFundRegularConfirmData(Map<String,String> requestParam,Model model){
		String INTRANSCODE = requestParam.get("INTRANSCODE");
		log.debug(ESAPIUtil.vaildLog("INTRANSCODE={}"+INTRANSCODE));
		if(INTRANSCODE != null){
			model.addAttribute("INTRANSCODE",INTRANSCODE);
		}
		String INFUNDSNAME = requestParam.get("INFUNDSNAME");
		log.debug(ESAPIUtil.vaildLog("INFUNDSNAME={}"+INFUNDSNAME));
		if(INFUNDSNAME != null){
			model.addAttribute("INFUNDSNAME",INFUNDSNAME);
		}
		String FUNDAMT = requestParam.get("FUNDAMT");
		log.debug(ESAPIUtil.vaildLog("FUNDAMT={}"+FUNDAMT));
		if(FUNDAMT != null){
			model.addAttribute("FUNDAMT",FUNDAMT);
		}
		String COUNTRY = requestParam.get("COUNTRY");
		log.debug(ESAPIUtil.vaildLog("COUNTRY={}"+COUNTRY));
		model.addAttribute("COUNTRY",COUNTRY);
		
		String FUS98E = requestParam.get("FUS98E");
		log.debug(ESAPIUtil.vaildLog("FUS98E={}"+FUS98E));
		model.addAttribute("FUS98E",FUS98E);
		
		String FDINVTYPE = requestParam.get("FDINVTYPE");
		log.debug(ESAPIUtil.vaildLog("FDINVTYPE={}"+FDINVTYPE));
		model.addAttribute("FDINVTYPE",FDINVTYPE);
		
		String ACN1 = requestParam.get("ACN1");
		log.debug(ESAPIUtil.vaildLog("ACN1={}"+ACN1));
		model.addAttribute("ACN1",ACN1);
		
		String ACN2 = requestParam.get("ACN2");
		log.debug(ESAPIUtil.vaildLog("ACN2={}"+ACN2));
		model.addAttribute("ACN2",ACN2);
		
		String PAYTYPE = requestParam.get("PAYTYPE");
		log.debug(ESAPIUtil.vaildLog("PAYTYPE={}"+PAYTYPE));
		model.addAttribute("PAYTYPE",PAYTYPE);
		
		String HTELPHONE = requestParam.get("HTELPHONE");
		log.debug(ESAPIUtil.vaildLog("HTELPHONE={}"+HTELPHONE));
		model.addAttribute("HTELPHONE",HTELPHONE);
		
		String FUNDACN = requestParam.get("FUNDACN");
		log.debug(ESAPIUtil.vaildLog("FUNDACN={}"+FUNDACN));
		model.addAttribute("FUNDACN",FUNDACN);
		
		String COUNTRYTYPE = requestParam.get("COUNTRYTYPE");
		log.debug(ESAPIUtil.vaildLog("COUNTRYTYPE={}"+COUNTRYTYPE));
		model.addAttribute("COUNTRYTYPE",COUNTRYTYPE);
		
		String COUNTRYTYPE1 = requestParam.get("COUNTRYTYPE1");
		log.debug(ESAPIUtil.vaildLog("COUNTRYTYPE1={}"+COUNTRYTYPE1));
		model.addAttribute("COUNTRYTYPE1",COUNTRYTYPE1);
		
		String FUNDLNAME = requestParam.get("FUNDLNAME");
		log.debug(ESAPIUtil.vaildLog("FUNDLNAME={}"+FUNDLNAME));
		model.addAttribute("FUNDLNAME",FUNDLNAME);
		
		String CUTTYPE = requestParam.get("CUTTYPE");
		log.debug(ESAPIUtil.vaildLog("CUTTYPE={}"+CUTTYPE));
		model.addAttribute("CUTTYPE",CUTTYPE);
		
		String BRHCOD = requestParam.get("BRHCOD");
		log.debug(ESAPIUtil.vaildLog("BRHCOD={}"+BRHCOD));
		model.addAttribute("BRHCOD",BRHCOD);
		
		String RISK = requestParam.get("RISK");
		log.debug(ESAPIUtil.vaildLog("RISK={}"+RISK));
		model.addAttribute("RISK",RISK);
		
		String SALESNO = requestParam.get("SALESNO");
		log.debug(ESAPIUtil.vaildLog("SALESNO={}"+SALESNO));
		model.addAttribute("SALESNO",SALESNO);
		
		String MIP = requestParam.get("MIP");
		log.debug(ESAPIUtil.vaildLog("MIP={}"+MIP));
		model.addAttribute("MIP",MIP);
		
		String PRO = requestParam.get("PRO");
		log.debug(ESAPIUtil.vaildLog("PRO={}"+PRO));
		model.addAttribute("PRO",PRO);
		
		String PAYDAY1 = requestParam.get("PAYDAY1");
		log.debug(ESAPIUtil.vaildLog("PAYDAY1={}"+PAYDAY1));
		model.addAttribute("PAYDAY1",PAYDAY1);
		
		String PAYDAY2 = requestParam.get("PAYDAY2");
		log.debug(ESAPIUtil.vaildLog("PAYDAY2={}"+PAYDAY2));
		model.addAttribute("PAYDAY2",PAYDAY2);
		
		String PAYDAY3 = requestParam.get("PAYDAY3");
		log.debug(ESAPIUtil.vaildLog("PAYDAY3={}"+PAYDAY3));
		model.addAttribute("PAYDAY3",PAYDAY3);
		
		String PAYDAY4 = requestParam.get("PAYDAY4");
		log.debug(ESAPIUtil.vaildLog("PAYDAY4={}"+PAYDAY4));
		model.addAttribute("PAYDAY4",PAYDAY4);
		
		String PAYDAY5 = requestParam.get("PAYDAY5");
		log.debug(ESAPIUtil.vaildLog("PAYDAY5={}"+PAYDAY5));
		model.addAttribute("PAYDAY5",PAYDAY5);
		
		String PAYDAY6 = requestParam.get("PAYDAY6");
		log.debug(ESAPIUtil.vaildLog("PAYDAY6={}"+PAYDAY6));
		model.addAttribute("PAYDAY6",PAYDAY6);
		
		String PAYDAY7 = requestParam.get("PAYDAY7");
		log.debug(ESAPIUtil.vaildLog("PAYDAY7={}"+PAYDAY7));
		model.addAttribute("PAYDAY7",PAYDAY7);
		
		String PAYDAY8 = requestParam.get("PAYDAY8");
		log.debug(ESAPIUtil.vaildLog("PAYDAY8={}"+PAYDAY8));
		model.addAttribute("PAYDAY8",PAYDAY8);
		
		String PAYDAY9 = requestParam.get("PAYDAY9");
		log.debug(ESAPIUtil.vaildLog("PAYDAY9={}"+PAYDAY9));
		model.addAttribute("PAYDAY9",PAYDAY9);
		
		String INVTYPE = requestParam.get("INVTYPE");
		log.debug(ESAPIUtil.vaildLog("INVTYPE={}"+INVTYPE));
		model.addAttribute("INVTYPE",INVTYPE);
		
		String TYPE = requestParam.get("TYPE");
		log.debug(ESAPIUtil.vaildLog("TYPE={}"+TYPE));
		model.addAttribute("TYPE",TYPE);
		
		String COMPANYCODE = requestParam.get("COMPANYCODE");
		log.debug(ESAPIUtil.vaildLog("COMPANYCODE={}"+COMPANYCODE));
		model.addAttribute("COMPANYCODE",COMPANYCODE);
		
		String TRANSCODE = requestParam.get("TRANSCODE");
		log.debug(ESAPIUtil.vaildLog("TRANSCODE={}"+TRANSCODE));
		model.addAttribute("TRANSCODE",TRANSCODE);
		
		String AMT3 = requestParam.get("AMT3");
		log.debug(ESAPIUtil.vaildLog("AMT3={}"+AMT3));
		model.addAttribute("AMT3",AMT3);
		
		String YIELD = requestParam.get("YIELD");
		log.debug(ESAPIUtil.vaildLog("YIELD={}"+YIELD));
		model.addAttribute("YIELD",YIELD);
		
		String STOP = requestParam.get("STOP");
		log.debug(ESAPIUtil.vaildLog("STOP={}"+STOP));
		model.addAttribute("STOP",STOP);
		
		String TXID = requestParam.get("TXID");
		log.debug(ESAPIUtil.vaildLog("TXID={}"+TXID));
		model.addAttribute("TXID",TXID);
		
		String FEE_TYPE = requestParam.get("FEE_TYPE");
		log.debug(ESAPIUtil.vaildLog("FEE_TYPE={}"+FEE_TYPE));
		model.addAttribute("FEE_TYPE",FEE_TYPE);
		
		String SAL01 = requestParam.get("SAL01");
		log.debug(ESAPIUtil.vaildLog("SAL01={}"+SAL01));
		model.addAttribute("SAL01",SAL01);
		
		String SAL03 = requestParam.get("SAL03");
		log.debug(ESAPIUtil.vaildLog("SAL03={}"+SAL03));
		model.addAttribute("SAL03",SAL03);
	}
	/**
	 * 準備前往定期申購手續費頁的資料
	 */
	@SuppressWarnings("unchecked")
	public BaseResult prepareFundRegularFeeExposeData(Map<String,String> requestParam,Model model){
		BaseResult bs = null;
		try{
			String FDINVTYPE = requestParam.get("FDINVTYPE");
			log.debug(ESAPIUtil.vaildLog("FDINVTYPE={}"+FDINVTYPE));
			model.addAttribute("FDINVTYPE",FDINVTYPE);
			model.addAttribute("RSKATT",FDINVTYPE);
			
			String FUNDLNAME = requestParam.get("FUNDLNAME");
			log.debug(ESAPIUtil.vaildLog("FUNDLNAME={}"+FUNDLNAME));
			model.addAttribute("FUNDLNAME",FUNDLNAME);
			
			String RISK = requestParam.get("RISK");
			log.debug(ESAPIUtil.vaildLog("RISK={}"+RISK));
			model.addAttribute("RISK",RISK);
			
			String SALESNO = requestParam.get("SALESNO");
			log.debug(ESAPIUtil.vaildLog("SALESNO={}"+SALESNO));
			model.addAttribute("SALESNO",SALESNO);
			
			String PRO = requestParam.get("PRO");
			log.debug(ESAPIUtil.vaildLog("PRO={}"+PRO));
			model.addAttribute("PRO",PRO);
			
			String RRSK = requestParam.get("RRSK");
			log.debug(ESAPIUtil.vaildLog("RRSK={}"+RRSK));
			model.addAttribute("RRSK",RRSK);
			
			String STOP = requestParam.get("STOP");
			log.debug(ESAPIUtil.vaildLog("STOP={}"+STOP));
			while(STOP.length() < 3){
				STOP = "0" + STOP;
			}
			log.debug(ESAPIUtil.vaildLog("STOP={}"+STOP));
			requestParam.put("STOP",STOP);
			
			String YIELD = requestParam.get("YIELD");
			log.debug(ESAPIUtil.vaildLog("YIELD={}"+YIELD));
			while(YIELD.length() < 3){
				YIELD = "0" + YIELD;
			}
			log.debug(ESAPIUtil.vaildLog("YIELD={}"+YIELD));
			requestParam.put("YIELD",YIELD);
			
			String FDAGREEFLAG = requestParam.get("FDAGREEFLAG");
			log.debug(ESAPIUtil.vaildLog("FDAGREEFLAG={}"+FDAGREEFLAG));
			model.addAttribute("FDAGREEFLAG",FDAGREEFLAG);
			
			String COUNTRYTYPE1 = requestParam.get("COUNTRYTYPE1");
			log.debug(ESAPIUtil.vaildLog("COUNTRYTYPE1={}"+COUNTRYTYPE1));
			model.addAttribute("COUNTRYTYPE1",COUNTRYTYPE1);
			
			String COMPANYCODE = requestParam.get("COMPANYCODE");
			log.debug(ESAPIUtil.vaildLog("COMPANYCODE={}"+COMPANYCODE));
			model.addAttribute("COMPANYCODE",COMPANYCODE);
			
			String PAYTYPE = requestParam.get("PAYTYPE");
			log.debug(ESAPIUtil.vaildLog("PAYTYPE={}"+PAYTYPE));
			model.addAttribute("PAYTYPE",PAYTYPE);
			
			String INVTYPE = requestParam.get("INVTYPE");
			log.debug(ESAPIUtil.vaildLog("INVTYPE={}"+INVTYPE));
			model.addAttribute("INVTYPE",INVTYPE);
			
			String FDNOTICETYPE = requestParam.get("FDNOTICETYPE");
			log.debug(ESAPIUtil.vaildLog("FDNOTICETYPE={}"+FDNOTICETYPE));
			model.addAttribute("FDNOTICETYPE",FDNOTICETYPE);
			
			String FDPUBLICTYPE = requestParam.get("FDPUBLICTYPE");
			log.debug(ESAPIUtil.vaildLog("FDPUBLICTYPE={}"+FDPUBLICTYPE));
			model.addAttribute("FDPUBLICTYPE",FDPUBLICTYPE);
			
			String SAL01 = requestParam.get("SAL01");
			log.debug(ESAPIUtil.vaildLog("SAL01={}"+SAL01));
			model.addAttribute("SAL01",SAL01);
			
			String SAL03 = requestParam.get("SAL03");
			log.debug(ESAPIUtil.vaildLog("SAL03={}"+SAL03));
			model.addAttribute("SAL03",SAL03);
			
			String SLSNO = requestParam.get("SLSNO");
			log.debug(ESAPIUtil.vaildLog("SLSNO={}"+SLSNO));
			model.addAttribute("SLSNO",SLSNO);
			
			String KYCNO = requestParam.get("KYCNO");
			log.debug(ESAPIUtil.vaildLog("KYCNO={}"+KYCNO));
			model.addAttribute("KYCNO",KYCNO);
			
			bs = new BaseResult();
			bs = C017Public_REST(requestParam);
			if(bs != null && bs.getResult()){
				Map<String,Object> bsData = (Map<String,Object>)bs.getData();
				
				String SHWD = (String)bsData.get("SHWD");
				model.addAttribute("SHWD",SHWD);
				
				String XFLAG = (String)bsData.get("XFLAG");
				model.addAttribute("XFLAG",XFLAG);
				
				String TRADEDATE = (String)bsData.get("TRADEDATE");
				log.debug("TRADEDATE={}",TRADEDATE);
				model.addAttribute("TRADEDATE",TRADEDATE);
				
				String FCA2 = (String)bsData.get("FCA2");
				log.debug("FCA2={}",FCA2);
				model.addAttribute("FCA2",FCA2);
				
				String AMT3 = (String)bsData.get("AMT3");
				log.debug("AMT3={}",AMT3);
				model.addAttribute("AMT3",AMT3);
				
				String AMT5 = (String)bsData.get("AMT5");
				log.debug("AMT5={}",AMT5);
				model.addAttribute("AMT5",AMT5);
				
				String FUNDACN = (String)bsData.get("FUNDACN");
				log.debug("FUNDACN={}",FUNDACN);
				model.addAttribute("FUNDACN",FUNDACN);
				
				String SSLTXNO = (String)bsData.get("SSLTXNO");
				log.debug("SSLTXNO={}",SSLTXNO);
				model.addAttribute("SSLTXNO",SSLTXNO);
				
				String FUDCUR = (String)bsData.get("FUDCUR");
				log.debug("FUDCUR={}",FUDCUR);
				model.addAttribute("FUNCUR",FUDCUR);
				model.addAttribute("CRY1",FUDCUR);
				
				String ACN1 = requestParam.get("ACN1");
				log.debug(ESAPIUtil.vaildLog("ACN1={}"+ACN1));
				String ACN2 = requestParam.get("ACN2");
				log.debug(ESAPIUtil.vaildLog("ACN2={}"+ACN2));
				
				String OUTACN = "";
				String TYPE = "";
				if("NTD".equals(FUDCUR)){
					OUTACN = ACN1;
					TYPE = "01";
				}
				else{
					OUTACN = ACN2;
					TYPE = "02";
				}
				log.debug(ESAPIUtil.vaildLog("OUTACN={}"+OUTACN));
				model.addAttribute("OUTACN",OUTACN);
				log.debug("TYPE={}",TYPE);
				model.addAttribute("TYPE",TYPE);
				
				String TRANSCODE = (String)bsData.get("TRANSCODE");
				log.debug("TRANSCODE={}",TRANSCODE);
				model.addAttribute("TRANSCODE",TRANSCODE);
				
				String COUNTRYTYPE = (String)bsData.get("COUNTRYTYPE");
				log.debug("COUNTRYTYPE={}",COUNTRYTYPE);
				model.addAttribute("COUNTRYTYPE",COUNTRYTYPE);
				
				String HTELPHONE = (String)bsData.get("HTELPHONE");
				log.debug("HTELPHONE={}",HTELPHONE);
				model.addAttribute("HTELPHONE",HTELPHONE);
				
				String OTELPHONE = (String)bsData.get("OTELPHONE");
				log.debug("OTELPHONE={}",OTELPHONE);
				model.addAttribute("OTELPHONE",OTELPHONE);
				model.addAttribute("INTSACN",OTELPHONE);
				
				String BILLSENDMODE = (String)bsData.get("BILLSENDMODE");
				log.debug("BILLSENDMODE={}",BILLSENDMODE);
				model.addAttribute("BILLSENDMODE",BILLSENDMODE);
				
				String FCAFEE = (String)bsData.get("FCAFEE");
				log.debug("FCAFEE={}",FCAFEE);
				model.addAttribute("FCAFEE",FCAFEE);
				
				String PAYDAY1 = (String)bsData.get("PAYDAY1");
				log.debug("PAYDAY1={}",PAYDAY1);
				model.addAttribute("PAYDAY1",PAYDAY1);
				
				String PAYDAY2 = (String)bsData.get("PAYDAY2");
				log.debug("PAYDAY2={}",PAYDAY2);
				model.addAttribute("PAYDAY2",PAYDAY2);
				
				String PAYDAY3 = (String)bsData.get("PAYDAY3");
				log.debug("PAYDAY3={}",PAYDAY3);
				model.addAttribute("PAYDAY3",PAYDAY3);
				
				String PAYDAY4 = (String)bsData.get("PAYDAY4");
				log.debug("PAYDAY4={}",PAYDAY4);
				model.addAttribute("PAYDAY4",PAYDAY4);
				
				String PAYDAY5 = (String)bsData.get("PAYDAY5");
				log.debug("PAYDAY5={}",PAYDAY5);
				model.addAttribute("PAYDAY5",PAYDAY5);
				
				String PAYDAY6 = (String)bsData.get("PAYDAY6");
				log.debug("PAYDAY6={}",PAYDAY6);
				model.addAttribute("PAYDAY6",PAYDAY6);
				
				String PAYDAY7 = (String)bsData.get("PAYDAY7");
				log.debug("PAYDAY7={}",PAYDAY7);
				model.addAttribute("PAYDAY7",PAYDAY7);
				
				String PAYDAY8 = (String)bsData.get("PAYDAY8");
				log.debug("PAYDAY8={}",PAYDAY8);
				model.addAttribute("PAYDAY8",PAYDAY8);
				
				String PAYDAY9 = (String)bsData.get("PAYDAY9");
				log.debug("PAYDAY9={}",PAYDAY9);
				model.addAttribute("PAYDAY9",PAYDAY9);
				
				String MIP = (String)bsData.get("MIP");
				log.debug("MIP={}",MIP);
				model.addAttribute("MIP",MIP);
				
				String BRHCOD = (String)bsData.get("BRHCOD");
				log.debug("BRHCOD={}",BRHCOD);
				model.addAttribute("BRHCOD",BRHCOD);
				
				String CUTTYPE = (String)bsData.get("CUTTYPE");
				log.debug("CUTTYPE={}",CUTTYPE);
				model.addAttribute("CUTTYPE",CUTTYPE);
				
				String DBDATE = (String)bsData.get("DBDATE");
				log.debug("DBDATE={}",DBDATE);
				model.addAttribute("DBDATE",DBDATE);
				
				STOP = (String)bsData.get("STOP");
				log.debug("STOP={}",STOP);
				model.addAttribute("STOP",STOP);
				
				YIELD = (String)bsData.get("YIELD");
				log.debug("YIELD={}",YIELD);
				model.addAttribute("YIELD",YIELD);
				
				Map<String,String> fundFeeExposeTextMap = fund_transfer_service.getFundFeeExposeInfo(bsData,"C017");
				model.addAttribute("fundFeeExposeTextMap",fundFeeExposeTextMap);
				
				String TXID = requestParam.get("TXID");
				log.debug(ESAPIUtil.vaildLog("TXID={}"+TXID));
				model.addAttribute("TXID",TXID);
				
				//IKEY要使用的JSON:DC
			 	String jsondc = URLEncoder.encode(CodeUtil.toJson(requestParam),"UTF-8");
			 	log.debug(ESAPIUtil.vaildLog("jsondc={}"+jsondc));
			 	model.addAttribute("jsondc",jsondc);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("prepareFundRegularFeeExposeData error >> {}",e);
		}
		return bs;
	}
	/**
	 * 準備前往基金定期申購確認頁的資料
	 */
	public void prepareFundRegularConfirmFinalData(Map<String,String> requestParam,Model model){
		String PAYTYPE = requestParam.get("PAYTYPE");
		log.debug(ESAPIUtil.vaildLog("PAYTYPE={}"+PAYTYPE));
		model.addAttribute("PAYTYPE",PAYTYPE);
		
		String MIP = requestParam.get("MIP");
		log.debug(ESAPIUtil.vaildLog("MIP={}"+MIP));
		model.addAttribute("MIP",MIP);
		
		String INVTYPEChinese = "";
	    if("Y".equals(MIP)){
	    	INVTYPEChinese = i18n.getMsg("LB.W1087"); //"定期不定額"
	    }
	    else{
	    	INVTYPEChinese = i18n.getMsg("LB.W1086");//"定期定額"
	    }
	    log.debug("INVTYPEChinese={}",INVTYPEChinese);
		model.addAttribute("INVTYPEChinese",INVTYPEChinese);
	    
		String TRANSCODE = requestParam.get("TRANSCODE");
		log.debug(ESAPIUtil.vaildLog("TRANSCODE={}"+TRANSCODE));
		model.addAttribute("TRANSCODE",TRANSCODE);
		
		String FDAGREEFLAG = requestParam.get("FDAGREEFLAG");
		log.debug(ESAPIUtil.vaildLog("FDAGREEFLAG={}"+FDAGREEFLAG));
		model.addAttribute("FDAGREEFLAG",FDAGREEFLAG);
		
		String FDNOTICETYPE = requestParam.get("FDNOTICETYPE");
		log.debug(ESAPIUtil.vaildLog("FDNOTICETYPE={}"+FDNOTICETYPE));
		model.addAttribute("FDNOTICETYPE",FDNOTICETYPE);
		
		String FDPUBLICTYPE = requestParam.get("FDPUBLICTYPE");
		log.debug(ESAPIUtil.vaildLog("FDPUBLICTYPE={}"+FDPUBLICTYPE));
		model.addAttribute("FDPUBLICTYPE",FDPUBLICTYPE);
		
		String COUNTRYTYPE1 = requestParam.get("COUNTRYTYPE1");
		log.debug(ESAPIUtil.vaildLog("COUNTRYTYPE1={}"+COUNTRYTYPE1));
		model.addAttribute("COUNTRYTYPE1",COUNTRYTYPE1);
		
		String COMPANYCODE = requestParam.get("COMPANYCODE");
		log.debug(ESAPIUtil.vaildLog("COMPANYCODE={}"+COMPANYCODE));
		model.addAttribute("COMPANYCODE",COMPANYCODE);
		
		String INVTYPE = requestParam.get("INVTYPE");
		log.debug(ESAPIUtil.vaildLog("INVTYPE={}"+INVTYPE));
		model.addAttribute("INVTYPE",INVTYPE);
		
		String OUTACN = requestParam.get("OUTACN");
		log.debug(ESAPIUtil.vaildLog("OUTACN={}"+OUTACN));
		model.addAttribute("OUTACN",OUTACN);
		
		String INTSACN = requestParam.get("INTSACN");
		log.debug(ESAPIUtil.vaildLog("INTSACN={}"+INTSACN));
		model.addAttribute("INTSACN",INTSACN);
		
		String CRY1 = requestParam.get("CRY1");
		log.debug(ESAPIUtil.vaildLog("CRY1={}"+CRY1));
		model.addAttribute("CRY1",CRY1);
		
		String TRADEDATE = requestParam.get("TRADEDATE");
		log.debug(ESAPIUtil.vaildLog("TRADEDATE={}"+TRADEDATE));
		model.addAttribute("TRADEDATE",TRADEDATE);
		
		String TRADEDATEFormat = TRADEDATE.substring(1,4) + "/" + TRADEDATE.substring(4,6) + "/" + TRADEDATE.substring(6);
		log.debug(ESAPIUtil.vaildLog("TRADEDATEFormat={}"+TRADEDATEFormat));
		model.addAttribute("TRADEDATEFormat",TRADEDATEFormat);
		
		String AMT3 = requestParam.get("AMT3");
		log.debug(ESAPIUtil.vaildLog("AMT3={}"+AMT3));
		model.addAttribute("AMT3",AMT3);
		Integer AMT3Integer = Integer.valueOf(AMT3);
		log.debug("AMT3Integer={}",AMT3Integer);
		model.addAttribute("AMT3Integer",AMT3Integer);
		String AMT3Format = fund_transfer_service.formatNumberString(String.valueOf(AMT3Integer),0);
		log.debug("AMT3Format={}",AMT3Format);
		model.addAttribute("AMT3Format",AMT3Format);
		
		String FCA2 = requestParam.get("FCA2");
		log.debug(ESAPIUtil.vaildLog("FCA2={}"+FCA2));
		model.addAttribute("FCA2",FCA2);
		
		Integer FCA2Integer = Integer.valueOf(FCA2);
		log.debug("FCA2Integer={}",FCA2Integer);
		model.addAttribute("FCA2Integer",FCA2Integer);
		String FCA2Format = fund_transfer_service.formatNumberString(String.valueOf(FCA2Integer),2);
		log.debug("FCA2Format={}",FCA2Format);
		model.addAttribute("FCA2Format",FCA2Format);
		
		String FCAFEE = requestParam.get("FCAFEE");
		log.debug(ESAPIUtil.vaildLog("FCAFEE={}"+FCAFEE));
		model.addAttribute("FCAFEE",FCAFEE);
		
		String FCAFEEFormat = fund_transfer_service.formatNumberString(FCAFEE,3);
		log.debug(ESAPIUtil.vaildLog("FCAFEEFormat={}"+FCAFEEFormat));
		model.addAttribute("FCAFEEFormat",FCAFEEFormat);
		
		String PAYDAY1 = requestParam.get("PAYDAY1");
		log.debug(ESAPIUtil.vaildLog("PAYDAY1={}"+PAYDAY1));
		model.addAttribute("PAYDAY1",PAYDAY1);
		
		String PAYDAY2 = requestParam.get("PAYDAY2");
		log.debug(ESAPIUtil.vaildLog("PAYDAY2={}"+PAYDAY2));
		model.addAttribute("PAYDAY2",PAYDAY2);
		
		String PAYDAY3 = requestParam.get("PAYDAY3");
		log.debug(ESAPIUtil.vaildLog("PAYDAY3={}"+PAYDAY3));
		model.addAttribute("PAYDAY3",PAYDAY3);
		
		String PAYDAY4 = requestParam.get("PAYDAY4");
		log.debug(ESAPIUtil.vaildLog("PAYDAY4={}"+PAYDAY4));
		model.addAttribute("PAYDAY4",PAYDAY4);
		
		String PAYDAY5 = requestParam.get("PAYDAY5");
		log.debug(ESAPIUtil.vaildLog("PAYDAY5={}"+PAYDAY5));
		model.addAttribute("PAYDAY5",PAYDAY5);
		
		String PAYDAY6 = requestParam.get("PAYDAY6");
		log.debug(ESAPIUtil.vaildLog("PAYDAY6={}"+PAYDAY6));
		model.addAttribute("PAYDAY6",PAYDAY6);
		
		String PAYDAY7 = requestParam.get("PAYDAY7");
		log.debug(ESAPIUtil.vaildLog("PAYDAY7={}"+PAYDAY7));
		model.addAttribute("PAYDAY7",PAYDAY7);
		
		String PAYDAY8 = requestParam.get("PAYDAY8");
		log.debug(ESAPIUtil.vaildLog("PAYDAY8={}"+PAYDAY8));
		model.addAttribute("PAYDAY8",PAYDAY8);
		
		String PAYDAY9 = requestParam.get("PAYDAY9");
		log.debug(ESAPIUtil.vaildLog("PAYDAY9={}"+PAYDAY9));
		model.addAttribute("PAYDAY9",PAYDAY9);
		
		String HTELPHONE = requestParam.get("HTELPHONE");
		log.debug(ESAPIUtil.vaildLog("HTELPHONE={}"+HTELPHONE));
		model.addAttribute("HTELPHONE",HTELPHONE);
		
		String BRHCOD = requestParam.get("BRHCOD");
		log.debug(ESAPIUtil.vaildLog("BRHCOD={}"+BRHCOD));
		model.addAttribute("BRHCOD",BRHCOD);

		String CUTTYPE = requestParam.get("CUTTYPE");
		log.debug(ESAPIUtil.vaildLog("CUTTYPE={}"+CUTTYPE));
		model.addAttribute("CUTTYPE",CUTTYPE);

		String DBDATE = requestParam.get("DBDATE");
		log.debug(ESAPIUtil.vaildLog("DBDATE={}"+DBDATE));
		model.addAttribute("DBDATE",DBDATE);

		String DBDATEFormat = DBDATE.substring(1,4) + "/" + DBDATE.substring(4,6) + "/" + DBDATE.substring(6);
		log.debug(ESAPIUtil.vaildLog("DBDATEFormat={}"+DBDATEFormat));
		model.addAttribute("DBDATEFormat",DBDATEFormat);
		
		String STOP = requestParam.get("STOP");
		log.debug(ESAPIUtil.vaildLog("STOP={}"+STOP));
		model.addAttribute("STOP",STOP);

		Integer STOPInteger = Integer.valueOf(STOP);
		log.debug("STOPInteger={}",STOPInteger);
		model.addAttribute("STOPInteger",STOPInteger);
		
		String YIELD = requestParam.get("YIELD");
		log.debug(ESAPIUtil.vaildLog("YIELD={}"+YIELD));
		model.addAttribute("YIELD",YIELD);

		Integer YIELDInteger = Integer.valueOf(YIELD);
		log.debug("YIELDInteger={}",YIELDInteger);
		model.addAttribute("YIELDInteger",YIELDInteger);
		
		String FDINVTYPE = requestParam.get("FDINVTYPE");
		log.debug(ESAPIUtil.vaildLog("FDINVTYPE={}"+FDINVTYPE));
		model.addAttribute("FDINVTYPE",FDINVTYPE);
		model.addAttribute("RSKATT",FDINVTYPE);
		
		String FDINVTYPEChinese;
		if("1".equals(FDINVTYPE)){
			FDINVTYPEChinese = i18n.getMsg("LB.D0945");//積極型
		}
		else if("2".equals(FDINVTYPE)){
			FDINVTYPEChinese = i18n.getMsg("LB.X1766");//穩健型
		}
		else{
			FDINVTYPEChinese = i18n.getMsg("LB.X1767");//保守型
		}
		log.debug("FDINVTYPEChinese={}",FDINVTYPEChinese);
		model.addAttribute("FDINVTYPEChinese",FDINVTYPEChinese);
		
		String FUNDLNAME = requestParam.get("FUNDLNAME");
		log.debug(ESAPIUtil.vaildLog("FUNDLNAME={}"+FUNDLNAME));
		model.addAttribute("FUNDLNAME",FUNDLNAME);

		String RISK = requestParam.get("RISK");
		log.debug(ESAPIUtil.vaildLog("RISK={}"+RISK));
		model.addAttribute("RISK",RISK);
		model.addAttribute("RRSK",RISK);
		
		String SALESNO = requestParam.get("SALESNO");
		log.debug(ESAPIUtil.vaildLog("SALESNO={}"+SALESNO));
		model.addAttribute("SALESNO",SALESNO);

		String PRO = requestParam.get("PRO");
		log.debug(ESAPIUtil.vaildLog("PRO={}"+PRO));
		model.addAttribute("PRO",PRO);

		String COUNTRYTYPE = requestParam.get("COUNTRYTYPE");
		log.debug(ESAPIUtil.vaildLog("COUNTRYTYPE={}"+COUNTRYTYPE));
		model.addAttribute("COUNTRYTYPE",COUNTRYTYPE);
		
		String OTELPHONE = requestParam.get("OTELPHONE");
		log.debug(ESAPIUtil.vaildLog("OTELPHONE={}"+OTELPHONE));
		model.addAttribute("OTELPHONE",OTELPHONE);
		
		String AMT5 = requestParam.get("AMT5");
		log.debug(ESAPIUtil.vaildLog("AMT5={}"+AMT5));
		model.addAttribute("AMT5",AMT5);
		
		String AMT5NoDot = AMT5.replaceAll("\\.","");
		log.debug("AMT5NoDot={}",AMT5NoDot);
		Integer AMT5Integer = Integer.valueOf(AMT5NoDot);
		log.debug("AMT5Integer={}",AMT5Integer);
		model.addAttribute("AMT5Integer",AMT5Integer);
		String AMT5Format = fund_transfer_service.formatNumberString(String.valueOf(AMT5Integer),2);
		log.debug("AMT5Format={}",AMT5Format);
		model.addAttribute("AMT5Format",AMT5Format);
		
		String BILLSENDMODE = requestParam.get("BILLSENDMODE");
		log.debug(ESAPIUtil.vaildLog("BILLSENDMODE={}"+BILLSENDMODE));
		model.addAttribute("BILLSENDMODE",BILLSENDMODE);
		
		String SSLTXNO = requestParam.get("SSLTXNO");
		log.debug(ESAPIUtil.vaildLog("SSLTXNO={}"+SSLTXNO));
		model.addAttribute("SSLTXNO",SSLTXNO);
		
		String FUNDACN = requestParam.get("FUNDACN");
		log.debug(ESAPIUtil.vaildLog("FUNDACN={}"+FUNDACN));
		model.addAttribute("FUNDACN",FUNDACN);
		
		String hiddenFUNDACN = WebUtil.hideCardNum(FUNDACN);
		log.debug(ESAPIUtil.vaildLog("hiddenFUNDACN={}"+hiddenFUNDACN));
		model.addAttribute("hiddenFUNDACN",hiddenFUNDACN);
		
		String FUDCUR = requestParam.get("FUNCUR");
		log.debug(ESAPIUtil.vaildLog("FUDCUR={}"+FUDCUR));
		model.addAttribute("FUNCUR",FUDCUR);
		model.addAttribute("FUDCUR",FUDCUR);
		
		ADMCURRENCY admCurrency = fund_transfer_service.getCRYData(FUDCUR);
		
		if(admCurrency != null){
			//新增幣別多語系
			Locale currentLocale = LocaleContextHolder.getLocale();
    		String locale = currentLocale.toString();
    		String ADCCYNAME ="";
    		 switch (locale) {
				case "en":
					ADCCYNAME = admCurrency.getADCCYENGNAME();
					break;
				case "zh_TW":
					ADCCYNAME = admCurrency.getADCCYNAME();
					break;
				case "zh_CN":
					ADCCYNAME = admCurrency.getADCCYCHSNAME();
					break;
             }
			
			log.debug(ESAPIUtil.vaildLog("ADCCYNAME={}"+ADCCYNAME));
			model.addAttribute("ADCCYNAME",ADCCYNAME);
		}
		
		String TYPE = "";
		String TYPEChinese = "";
		
		if(!"3".equals(PAYTYPE)){
		    if("NTD".equals(FUDCUR)){
		    	TYPE = "01";
		    	TYPEChinese = i18n.getMsg("LB.W1089");//臺幣
		    }
		    else{
		    	TYPE = "02";
		    	TYPEChinese = i18n.getMsg("LB.W1090") ;//"外幣"
		    }
		    log.debug("TYPE={}",TYPE);
			model.addAttribute("TYPE",TYPE);
		    
		    model.addAttribute("displayTYPE",TYPE);
		}
		else{
			TYPE = "01";
			TYPEChinese = i18n.getMsg("LB.Credit_Card");//"信用卡"
		    model.addAttribute("TYPE",TYPE);
		    model.addAttribute("displayTYPE","03");
		}
		log.debug("TYPEChinese={}",TYPEChinese);
		model.addAttribute("TYPEChinese",TYPEChinese);
		
		String SHWD = requestParam.get("SHWD");
		log.debug(ESAPIUtil.vaildLog("SHWD= >>" + SHWD));
		model.addAttribute("SHWD",SHWD);
		
		String XFLAG = requestParam.get("XFLAG");
		log.debug(ESAPIUtil.vaildLog("XFLAG= >>" + XFLAG));
		model.addAttribute("XFLAG",XFLAG);

		String NUM = requestParam.get("NUM");
		log.debug(ESAPIUtil.vaildLog("NUM= >>" + NUM));
		model.addAttribute("NUM",NUM);
		
		String SAL01 = requestParam.get("SAL01");
		log.debug(ESAPIUtil.vaildLog("SAL01= >>" + SAL01));
		model.addAttribute("SAL01",SAL01);
		
		String SAL01Format = fund_transfer_service.formatNumberString(String.valueOf(SAL01),0);
		log.debug(ESAPIUtil.vaildLog("SAL01Format= >>" + SAL01Format));
		model.addAttribute("SAL01Format",SAL01Format);
		
		String SAL03 = requestParam.get("SAL03");
		log.debug(ESAPIUtil.vaildLog("SAL03= >>" + SAL03));
		model.addAttribute("SAL03",SAL03);
		
		String SLSNO = requestParam.get("SLSNO");
		log.debug(ESAPIUtil.vaildLog("SLSNO= >>" + SLSNO));
		model.addAttribute("SLSNO",SLSNO);
		
		String KYCNO = requestParam.get("KYCNO");
		log.debug(ESAPIUtil.vaildLog("KYCNO= >>" + KYCNO));
		model.addAttribute("KYCNO",KYCNO);
		
	}
	/**
	 * 基金定期申購交易
	 */
	@SuppressWarnings("unchecked")
	public BaseResult processFundRegular(Map<String,String> requestParam,Model model){
		BaseResult bs = null;
		log.debug("C020 requestParam = {}", requestParam);
		try{
			bs = new BaseResult();
			bs = C020_REST(requestParam);
			if(bs != null && bs.getResult()){
				Map<String,Object> bsData = (Map<String,Object>)bs.getData();
				log.debug("bsData={}",bsData);
				
				String MIP = (String)bsData.get("MIP");
				log.debug("MIP={}",MIP);
				
				String INVTYPEChinese = "";
				if("Y".equals(MIP)){
					INVTYPEChinese = i18n.getMsg("LB.W1087"); //"定期不定額"
				}
				else{
					INVTYPEChinese = i18n.getMsg("LB.W1086");//"定期定額"
				}
				log.debug("INVTYPEChinese={}",INVTYPEChinese);
				model.addAttribute("INVTYPEChinese",INVTYPEChinese);
				
				model.addAttribute("FDINVTYPEChinese",requestParam.get("FDINVTYPEChinese"));
				
				String displayTYPE = requestParam.get("displayTYPE");
				log.debug(ESAPIUtil.vaildLog("displayTYPE={}"+displayTYPE));
				
				String TYPEChinese = "";
				if("01".equals(displayTYPE)){
					TYPEChinese = i18n.getMsg("LB.W1089");//臺幣
				}
				else if("02".equals(displayTYPE)){
					TYPEChinese = i18n.getMsg("LB.W1090") ;//"外幣"
				}
				else{
					TYPEChinese = i18n.getMsg("LB.Credit_Card");//"信用卡"
				}
				log.debug("TYPEChinese={}",TYPEChinese);
				
				model.addAttribute("TYPEChinese",TYPEChinese);
				model.addAttribute("TRANSCODE",requestParam.get("TRANSCODE"));
				model.addAttribute("FUNDLNAME",requestParam.get("FUNDLNAME"));
				model.addAttribute("RISK",requestParam.get("RISK"));
				
				String FUDCUR = requestParam.get("FUNCUR");
				log.debug(ESAPIUtil.vaildLog("FUDCUR={}"+FUDCUR));
				model.addAttribute("FUNCUR",FUDCUR);
				model.addAttribute("FUDCUR",FUDCUR);
				
				String SAL01 = requestParam.get("SAL01");
				log.debug("SAL01 = " + SAL01);
				model.addAttribute("SAL01", SAL01);
				
				String SAL01Format = fund_transfer_service.formatNumberString(SAL01, 0);
				log.debug("SAL01Format = " + SAL01Format);
				model.addAttribute("SAL01Format", SAL01Format);
				
				String SAL03 = requestParam.get("SAL03");
				log.debug("SAL03 = " + SAL03);
				model.addAttribute("SAL03", SAL03);
				
				ADMCURRENCY admCurrency = fund_transfer_service.getCRYData(FUDCUR);
				
				
				
				if(admCurrency != null){
					//新增幣別多語系
					Locale currentLocale = LocaleContextHolder.getLocale();
		    		String locale = currentLocale.toString();
		    		String ADCCYNAME ="";
		    		 switch (locale) {
						case "en":
							ADCCYNAME = admCurrency.getADCCYENGNAME();
							break;
						case "zh_TW":
							ADCCYNAME = admCurrency.getADCCYNAME();
							break;
						case "zh_CN":
							ADCCYNAME = admCurrency.getADCCYCHSNAME();
							break;
		             }
					
					log.debug(ESAPIUtil.vaildLog("ADCCYNAME={}"+ADCCYNAME));
					model.addAttribute("ADCCYNAME",ADCCYNAME);
				}
				
				String AMT3 = (String)bsData.get("AMT3");
				log.debug("AMT3={}",AMT3);
				model.addAttribute("AMT3",AMT3);
				Integer AMT3Integer = Integer.valueOf(AMT3);
				log.debug("AMT3Integer={}",AMT3Integer);
				model.addAttribute("AMT3Integer",AMT3Integer);
				String AMT3Format = fund_transfer_service.formatNumberString(String.valueOf(AMT3Integer),0);
				log.debug("AMT3Format={}",AMT3Format);
				model.addAttribute("AMT3Format",AMT3Format);
				
				String FCAFEE = (String)bsData.get("FCAFEE");
				log.debug("FCAFEE={}",FCAFEE);
				model.addAttribute("FCAFEE",FCAFEE);
				Integer FCAFEEInteger = Integer.valueOf(FCAFEE);
				log.debug("FCAFEEInteger={}",FCAFEEInteger);
				model.addAttribute("FCAFEEInteger",FCAFEEInteger);
				String FCAFEEFormat = fund_transfer_service.formatNumberString(String.valueOf(FCAFEEInteger),3);
				log.debug("FCAFEEFormat={}",FCAFEEFormat);
				model.addAttribute("FCAFEEFormat",FCAFEEFormat);
				
				String FCA2 = (String)bsData.get("FCA2");
				log.debug("FCA2={}",FCA2);
				model.addAttribute("FCA2",FCA2);
				Integer FCA2Integer = Integer.valueOf(FCA2);
				log.debug("FCA2Integer={}",FCA2Integer);
				model.addAttribute("FCA2Integer",FCA2Integer);
				String FCA2Format = fund_transfer_service.formatNumberString(String.valueOf(FCA2Integer),2);
				log.debug("FCA2Format={}",FCA2Format);
				model.addAttribute("FCA2Format",FCA2Format);
				
				String AMT5 = (String)bsData.get("AMT5");
				log.debug("AMT5={}",AMT5);
				model.addAttribute("AMT5",AMT5);
				
				String AMT5NoDot = AMT5.replaceAll("\\.","");
				log.debug("AMT5NoDot={}",AMT5NoDot);
				Integer AMT5Integer = Integer.valueOf(AMT5NoDot);
				log.debug("AMT5Integer={}",AMT5Integer);
				model.addAttribute("AMT5Integer",AMT5Integer);
				String AMT5Format = fund_transfer_service.formatNumberString(String.valueOf(AMT5Integer),2);
				log.debug("AMT5Format={}",AMT5Format);
				model.addAttribute("AMT5Format",AMT5Format);
				
				String FUNDACN = (String)bsData.get("FUNDACN");
				log.debug("FUNDACN={}",FUNDACN);
				
				if(FUNDACN.length() > 11){
					model.addAttribute("FUNDACN",WebUtil.hideCardNum(FUNDACN));
				}
				else{
					model.addAttribute("FUNDACN",FUNDACN);
				}
				
				String TRADEDATE = (String)bsData.get("TRADEDATE");
				log.debug("TRADEDATE={}",TRADEDATE);
				model.addAttribute("TRADEDATE",TRADEDATE);
				
				String TRADEDATEFormat = TRADEDATE.substring(1,4) + "/" + TRADEDATE.substring(4,6) + "/" + TRADEDATE.substring(6);
				log.debug("TRADEDATEFormat={}",TRADEDATEFormat);
				model.addAttribute("TRADEDATEFormat",TRADEDATEFormat);
				
				model.addAttribute("PAYTYPE",requestParam.get("PAYTYPE"));
				
				String PAYDAY1 = (String)bsData.get("PAYDAY1");
				log.debug("PAYDAY1={}",PAYDAY1);
				model.addAttribute("PAYDAY1",PAYDAY1);
				String PAYDAY2 = (String)bsData.get("PAYDAY2");
				log.debug("PAYDAY2={}",PAYDAY2);
				model.addAttribute("PAYDAY2",PAYDAY2);
				String PAYDAY3 = (String)bsData.get("PAYDAY3");
				log.debug("PAYDAY3={}",PAYDAY3);
				model.addAttribute("PAYDAY3",PAYDAY3);
				String PAYDAY4 = (String)bsData.get("PAYDAY4");
				log.debug("PAYDAY4={}",PAYDAY4);
				model.addAttribute("PAYDAY4",PAYDAY4);
				String PAYDAY5 = (String)bsData.get("PAYDAY5");
				log.debug("PAYDAY5={}",PAYDAY5);
				model.addAttribute("PAYDAY5",PAYDAY5);
				String PAYDAY6 = (String)bsData.get("PAYDAY6");
				log.debug("PAYDAY6={}",PAYDAY6);
				model.addAttribute("PAYDAY6",PAYDAY6);
				String PAYDAY7 = (String)bsData.get("PAYDAY7");
				log.debug("PAYDAY7={}",PAYDAY7);
				model.addAttribute("PAYDAY7",PAYDAY7);
				String PAYDAY8 = (String)bsData.get("PAYDAY8");
				log.debug("PAYDAY8={}",PAYDAY8);
				model.addAttribute("PAYDAY8",PAYDAY8);
				String PAYDAY9 = (String)bsData.get("PAYDAY9");
				log.debug("PAYDAY9={}",PAYDAY9);
				model.addAttribute("PAYDAY9",PAYDAY9);
				
				String DBDATE = (String)bsData.get("DBDATE");
				log.debug("DBDATE={}",DBDATE);
				model.addAttribute("DBDATE",DBDATE);

				String DBDATEFormat = DBDATE.substring(1,4) + "/" + DBDATE.substring(4,6) + "/" + DBDATE.substring(6);
				log.debug("DBDATEFormat={}",DBDATEFormat);
				model.addAttribute("DBDATEFormat",DBDATEFormat);
				
				String STOP = (String)bsData.get("STOP");
				log.debug("STOP={}",STOP);
				model.addAttribute("STOP",STOP);

				Integer STOPInteger = Integer.valueOf(STOP);
				log.debug("STOPInteger={}",STOPInteger);
				model.addAttribute("STOPInteger",STOPInteger);
				
				String YIELD = (String)bsData.get("YIELD");
				log.debug("YIELD={}",YIELD);
				model.addAttribute("YIELD",YIELD);

				Integer YIELDInteger = Integer.valueOf(YIELD);
				log.debug("YIELDInteger={}",YIELDInteger);
				model.addAttribute("YIELDInteger",YIELDInteger);
				
				model.addAttribute("bsData",bsData);
			}
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("processFundRegular error >> {}",e);
		}
		return bs;
	}
	/**
	 * 信用卡查詢
	 */
	public BaseResult getN812Data(String UID){
		BaseResult bs = null;
		try{
			bs = new BaseResult();
			bs = N812_REST(UID);
		}
		catch(Exception e){
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getN812Data error >> {}",e);
		}
		return bs;
	}
	
}