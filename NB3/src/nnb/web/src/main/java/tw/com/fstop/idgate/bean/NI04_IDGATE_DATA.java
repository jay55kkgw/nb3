package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class NI04_IDGATE_DATA implements Serializable{



	/**
	 * 
	 */
	private static final long serialVersionUID = -5590837622193211968L;

	@SerializedName(value = "APPLYFLAG")
	private String APPLYFLAG;

	@SerializedName(value = "TSFACN")
	private String TSFACN;
	
	@SerializedName(value = "PAYFLAG")
	private String PAYFLAG;

	public String getAPPLYFLAG() {
		return APPLYFLAG;
	}

	public void setAPPLYFLAG(String aPPLYFLAG) {
		APPLYFLAG = aPPLYFLAG;
	}

	public String getTSFACN() {
		return TSFACN;
	}

	public void setTSFACN(String tSFACN) {
		TSFACN = tSFACN;
	}

	public String getPAYFLAG() {
		return PAYFLAG;
	}

	public void setPAYFLAG(String pAYFLAG) {
		PAYFLAG = pAYFLAG;
	}

}
