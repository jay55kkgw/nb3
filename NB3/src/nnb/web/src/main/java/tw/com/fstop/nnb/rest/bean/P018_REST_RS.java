package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class P018_REST_RS extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7823231950354170648L;
	private String CMQTIME;
	private String Count;
	private String TxnCode;
	private String RespString;
	private LinkedList<P018_REST_RSDATA> REC;
	
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getCount() {
		return Count;
	}
	public void setCount(String count) {
		Count = count;
	}
	public String getTxnCode() {
		return TxnCode;
	}
	public void setTxnCode(String txnCode) {
		TxnCode = txnCode;
	}
	public String getRespString() {
		return RespString;
	}
	public void setRespString(String respString) {
		RespString = respString;
	}
	public LinkedList<P018_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<P018_REST_RSDATA> rEC) {
		REC = rEC;
	}
}
