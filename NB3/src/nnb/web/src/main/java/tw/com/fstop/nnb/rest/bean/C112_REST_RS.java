package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * C112電文RS
 */
public class C112_REST_RS extends BaseRestBean implements Serializable{
	private static final long serialVersionUID = 6791228050721094802L;
	
	private String CUSIDN;
	private String CUSNAME;
	private String TYPE1;
	private String TYPE2;
	private String TYPE3;
	private String TYPE4;
	private String TYPE5;
	private String PAYTAG;
	private String PAYAMT;
	private String PAYDAY1;
	private String PAYDAY2;
	private String PAYDAY3;
	private String STOPBEGDAY;
	private String STOPENDDAY;
	private String CDNO;
	private String RESTOREDAY;
	private String MAC;
	private String PAYDAY4;
	private String PAYDAY5;
	private String PAYDAY6;
	private String RSKATT;
	private String PAYDAY7;
	private String PAYDAY8;
	private String PAYDAY9;
	private String RRSK;
	private String RESPCOD;
	private String CMQTIME;
	
	public String getCUSIDN(){
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN){
		CUSIDN = cUSIDN;
	}
	public String getCUSNAME(){
		return CUSNAME;
	}
	public void setCUSNAME(String cUSNAME){
		CUSNAME = cUSNAME;
	}
	public String getTYPE1(){
		return TYPE1;
	}
	public void setTYPE1(String tYPE1){
		TYPE1 = tYPE1;
	}
	public String getTYPE2(){
		return TYPE2;
	}
	public void setTYPE2(String tYPE2){
		TYPE2 = tYPE2;
	}
	public String getTYPE3(){
		return TYPE3;
	}
	public void setTYPE3(String tYPE3){
		TYPE3 = tYPE3;
	}
	public String getTYPE4(){
		return TYPE4;
	}
	public void setTYPE4(String tYPE4){
		TYPE4 = tYPE4;
	}
	public String getTYPE5(){
		return TYPE5;
	}
	public void setTYPE5(String tYPE5){
		TYPE5 = tYPE5;
	}
	public String getPAYTAG(){
		return PAYTAG;
	}
	public void setPAYTAG(String pAYTAG){
		PAYTAG = pAYTAG;
	}
	public String getPAYAMT(){
		return PAYAMT;
	}
	public void setPAYAMT(String pAYAMT){
		PAYAMT = pAYAMT;
	}
	public String getPAYDAY1(){
		return PAYDAY1;
	}
	public void setPAYDAY1(String pAYDAY1){
		PAYDAY1 = pAYDAY1;
	}
	public String getPAYDAY2(){
		return PAYDAY2;
	}
	public void setPAYDAY2(String pAYDAY2){
		PAYDAY2 = pAYDAY2;
	}
	public String getPAYDAY3(){
		return PAYDAY3;
	}
	public void setPAYDAY3(String pAYDAY3){
		PAYDAY3 = pAYDAY3;
	}
	public String getSTOPBEGDAY(){
		return STOPBEGDAY;
	}
	public void setSTOPBEGDAY(String sTOPBEGDAY){
		STOPBEGDAY = sTOPBEGDAY;
	}
	public String getSTOPENDDAY(){
		return STOPENDDAY;
	}
	public void setSTOPENDDAY(String sTOPENDDAY){
		STOPENDDAY = sTOPENDDAY;
	}
	public String getRESTOREDAY(){
		return RESTOREDAY;
	}
	public void setRESTOREDAY(String rESTOREDAY){
		RESTOREDAY = rESTOREDAY;
	}
	public String getMAC(){
		return MAC;
	}
	public void setMAC(String mAC){
		MAC = mAC;
	}
	public String getPAYDAY4(){
		return PAYDAY4;
	}
	public void setPAYDAY4(String pAYDAY4){
		PAYDAY4 = pAYDAY4;
	}
	public String getPAYDAY5(){
		return PAYDAY5;
	}
	public void setPAYDAY5(String pAYDAY5){
		PAYDAY5 = pAYDAY5;
	}
	public String getPAYDAY6(){
		return PAYDAY6;
	}
	public void setPAYDAY6(String pAYDAY6){
		PAYDAY6 = pAYDAY6;
	}
	public String getRSKATT(){
		return RSKATT;
	}
	public void setRSKATT(String rSKATT){
		RSKATT = rSKATT;
	}
	public String getPAYDAY7(){
		return PAYDAY7;
	}
	public void setPAYDAY7(String pAYDAY7){
		PAYDAY7 = pAYDAY7;
	}
	public String getPAYDAY8(){
		return PAYDAY8;
	}
	public void setPAYDAY8(String pAYDAY8){
		PAYDAY8 = pAYDAY8;
	}
	public String getPAYDAY9(){
		return PAYDAY9;
	}
	public void setPAYDAY9(String pAYDAY9){
		PAYDAY9 = pAYDAY9;
	}
	public String getRRSK(){
		return RRSK;
	}
	public void setRRSK(String rRSK){
		RRSK = rRSK;
	}
	public String getRESPCOD(){
		return RESPCOD;
	}
	public void setRESPCOD(String rESPCOD){
		RESPCOD = rESPCOD;
	}
	public String getCMQTIME(){
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME){
		CMQTIME = cMQTIME;
	}
	public String getCDNO() {
		return CDNO;
	}
	public void setCDNO(String cDNO) {
		CDNO = cDNO;
	}
}