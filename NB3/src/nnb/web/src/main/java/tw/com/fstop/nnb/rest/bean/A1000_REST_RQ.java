package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class A1000_REST_RQ extends BaseRestBean_TW implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4296985136348706587L;

	private String Cust_ID;

	public String getCust_ID() {
		return Cust_ID;
	}

	public void setCust_ID(String cust_ID) {
		Cust_ID = cust_ID;
	}
	

}
