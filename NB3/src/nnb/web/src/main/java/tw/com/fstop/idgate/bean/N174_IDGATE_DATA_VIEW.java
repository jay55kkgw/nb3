package tw.com.fstop.idgate.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

import tw.com.fstop.util.NumericUtil;


public class N174_IDGATE_DATA_VIEW extends Base_IDGATE_DATA_VIEW implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	
	@SerializedName(value = "FGTRDATE")
	private String FGTRDATE;
	@SerializedName(value = "transfer_date")
	private String transfer_date;
	@SerializedName(value = "FYTSFAN")
	private String FYTSFAN;
	@SerializedName(value = "FGINACNO")
	private String FGINACNO;
	@SerializedName(value = "INACN")
	private String INACN;
	
	@SerializedName(value = "INACNO2")
	private String INACNO2;
	@SerializedName(value = "OUTCRY_TEXT")
	private String OUTCRY_TEXT;
	@SerializedName(value = "AMOUNT")
	private String AMOUNT;
	@SerializedName(value = "AMOUNT_DIG")
	private String AMOUNT_DIG;
	@SerializedName(value = "TYPCOD_TEXT")
	private String TYPCOD_TEXT;
	
	@SerializedName(value = "INTMTHNAME")
	private String INTMTHNAME;
	@SerializedName(value = "AUTXFTMNAME")
	private String AUTXFTMNAME;
	@SerializedName(value = "CODENAME")
	private String CODENAME;
	@SerializedName(value = "BGROENO")
	private String BGROENO;
	
	@Override
	public Map<String, String> coverMap(){
		LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
		result.put("交易名稱", "轉入 外幣綜存定存");
		if(this.FGTRDATE.equals("0")) {
			result.put("交易型態","即時");
		}else {
			result.put("交易型態","預約");
		}
		result.put("轉帳日期", this.transfer_date);
		result.put("轉出帳號", this.FYTSFAN);
		if(this.FGINACNO.equals("CMDAGREE")) {
			result.put("轉入帳號", this.INACN);
		}else if(this.FGINACNO.equals("CMDISAGREE")) {
			result.put("轉入帳號", this.INACNO2);
		}
		String amount=NumericUtil.fmtAmount(this.AMOUNT,0);
		result.put("轉帳金額",this.OUTCRY_TEXT+amount+"."+this.AMOUNT_DIG );
		
		result.put("存款期別", this.TYPCOD_TEXT);
		result.put("計息方式", this.INTMTHNAME);
		result.put("轉存方式", this.AUTXFTMNAME);
		result.put("到期轉期", this.CODENAME);
		result.put("議價編號", this.BGROENO);
		
		return result;
	}

	public String getFGTRDATE() {
		return FGTRDATE;
	}

	public void setFGTRDATE(String fGTRDATE) {
		FGTRDATE = fGTRDATE;
	}

	public String getTransfer_date() {
		return transfer_date;
	}

	public void setTransfer_date(String transfer_date) {
		this.transfer_date = transfer_date;
	}

	public String getFYTSFAN() {
		return FYTSFAN;
	}

	public void setFYTSFAN(String fYTSFAN) {
		FYTSFAN = fYTSFAN;
	}

	public String getFGINACNO() {
		return FGINACNO;
	}

	public void setFGINACNO(String fGINACNO) {
		FGINACNO = fGINACNO;
	}

	public String getINACN() {
		return INACN;
	}

	public void setINACN(String iNACN) {
		INACN = iNACN;
	}

	public String getINACNO2() {
		return INACNO2;
	}

	public void setINACNO2(String iNACNO2) {
		INACNO2 = iNACNO2;
	}

	public String getOUTCRY_TEXT() {
		return OUTCRY_TEXT;
	}

	public void setOUTCRY_TEXT(String oUTCRY_TEXT) {
		OUTCRY_TEXT = oUTCRY_TEXT;
	}

	public String getAMOUNT() {
		return AMOUNT;
	}

	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}

	public String getAMOUNT_DIG() {
		return AMOUNT_DIG;
	}

	public void setAMOUNT_DIG(String aMOUNT_DIG) {
		AMOUNT_DIG = aMOUNT_DIG;
	}

	public String getTYPCOD_TEXT() {
		return TYPCOD_TEXT;
	}

	public void setTYPCOD_TEXT(String tYPCOD_TEXT) {
		TYPCOD_TEXT = tYPCOD_TEXT;
	}

	public String getINTMTHNAME() {
		return INTMTHNAME;
	}

	public void setINTMTHNAME(String iNTMTHNAME) {
		INTMTHNAME = iNTMTHNAME;
	}

	public String getAUTXFTMNAME() {
		return AUTXFTMNAME;
	}

	public void setAUTXFTMNAME(String aUTXFTMNAME) {
		AUTXFTMNAME = aUTXFTMNAME;
	}

	public String getCODENAME() {
		return CODENAME;
	}

	public void setCODENAME(String cODENAME) {
		CODENAME = cODENAME;
	}

	public String getBGROENO() {
		return BGROENO;
	}

	public void setBGROENO(String bGROENO) {
		BGROENO = bGROENO;
	}

}

