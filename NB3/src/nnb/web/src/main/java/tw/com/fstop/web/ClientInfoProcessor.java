
/*
 * Copyright (c) 2017, FSTOP, Inc. All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tw.com.fstop.web;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tw.com.fstop.util.DateTimeUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.web.ctx.WebAppRequestContext;
import tw.com.fstop.web.util.WebUtil;

/**
 * Get client informations and put into {@link WebAppRequestContext}.
 * 
 *
 * @since 1.0.1
 */
public class ClientInfoProcessor implements FilterProcessor
{

    private final static Logger log = LoggerFactory.getLogger(ClientInfoProcessor.class);
            
    @Override
    public void process(ServletRequest request, ServletResponse response) throws ProcessorException
    {
        HttpServletRequest req = (HttpServletRequest) request;
        
        String uri = req.getRequestURI();
        String ip = WebUtil.getIpAddr(req);
        String agent = WebUtil.getUserAgent(req);
        String startTime = String.valueOf(DateTimeUtil.getCurrentEpochSecond());
        String acceptLanguage = WebUtil.getAcceptLanguage(req);
        String preferredLocale = WebUtil.getPreferredLocale(req);
        
        WebAppRequestContext.setValue(WebAppRequestContext.Key.IP_ADDRESS, ip);
        WebAppRequestContext.setValue(WebAppRequestContext.Key.USER_AGENT, agent);
        WebAppRequestContext.setValue(WebAppRequestContext.Key.REQ_RESOURCE, uri);
        WebAppRequestContext.setValue(WebAppRequestContext.Key.START_TIME, startTime);
        WebAppRequestContext.setValue(WebAppRequestContext.Key.ACCEPT_LANGUAGE, acceptLanguage);
        WebAppRequestContext.setValue(WebAppRequestContext.Key.PREFERRED_LOCALE, preferredLocale);
        
        log.debug(ESAPIUtil.vaildLog(">>>>> URL= " + uri)); 
        log.debug(ESAPIUtil.vaildLog(">>>>> IP= " + ip)); 
        log.debug(ESAPIUtil.vaildLog(">>>>> AG= " + agent));
        log.debug(">>>>> TM= {} ", startTime);    
        log.debug(ESAPIUtil.vaildLog(">>>>> AL= " + acceptLanguage));
        log.debug(">>>>> PL= {} ", preferredLocale);  

    }

}
