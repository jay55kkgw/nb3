package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class NA80_REST_RS extends BaseRestBean implements Serializable 
{
	private static final long serialVersionUID = -1605893000376680726L;
	
	private String MSGCOD;		// 回應代碼
	
	// 電文沒回但ms_tw回應的欄位
	private String CMQTIME;		// 執行時間

	
	public String getMSGCOD() {
		return MSGCOD;
	}

	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}

	public String getCMQTIME() {
		return CMQTIME;
	}

	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	
}
