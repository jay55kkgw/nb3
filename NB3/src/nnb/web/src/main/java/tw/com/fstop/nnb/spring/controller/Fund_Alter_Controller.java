package tw.com.fstop.nnb.spring.controller;

import java.util.Base64;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.Fund_Alter_Service;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.web.util.WebUtil;

/**
 * 
 * 功能說明 :變更服務
 *
 */
@SessionAttributes({ SessionUtil.CUSIDN, SessionUtil.DPMYEMAIL, SessionUtil.STEP1_LOCALE_DATA, SessionUtil.STEP2_LOCALE_DATA,
		SessionUtil.CONFIRM_LOCALE_DATA, SessionUtil.RESULT_LOCALE_DATA, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN,
		SessionUtil.TRANSFER_DATA ,SessionUtil.USERIP })
@Controller
@RequestMapping(value = "/FUND/ALTER")
public class Fund_Alter_Controller
{

	Logger log = LoggerFactory.getLogger(this.getClass());

	// 錯誤頁路徑
	private final String ERRORJSP = "/error";

	@Autowired
	private Fund_Alter_Service fund_Alter_Service;

	/**
	 * C111定期投資約定變更進入頁!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/regular_investment")
	public String regular_investment(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("regular_investment Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		try
		{
			// 清除暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.STEP2_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			// Get session value by session Key
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			bs = fund_Alter_Service.regular_investment(cusidn, reqParam);
			log.trace("bs.getResult>> {}", bs.getResult());
			log.debug("bs.getData() = {}", bs.getData());
		}
		catch (Exception e)
		{
			log.error("regular_investment Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				model.addAttribute("regular_investment", bs);
				target = "fund/regular_investment";
			}
			else
			{
				// 新增回上一頁的路徑
				bs.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * C111定期投資約定變更第一頁!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/regular_investment_step1")
	public String regular_investment_step1(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("regular_investment Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			boolean back = "Y".equals(okMap.get("back"));
			if (hasLocale || back)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null));
			}
			else
			{
				bs = fund_Alter_Service.regular_investment_step1(reqParam);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, bs);
				log.trace("bs.getResult>> {}", bs.getResult());
				log.debug("bs.getData() = {}", bs.getData());
			}
		}
		catch (Exception e)
		{
			log.error("regular_investment_step1 Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				// TODO 這邊還要調整 不能存物件，要存成JSNO字串
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				model.addAttribute("regular_investment_step1", bs);
				target = "fund/regular_investment_step1";
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * C111定期投資約定變更確認頁!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/regular_investment_confirm")
	public String regular_investment_confirm(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("regular_investment_confirm Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
			}
			else
			{
				// Get session value by session Key
				BaseResult bsdata = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String, String> transfer_data = (Map<String, String>) bsdata.getData();
				log.debug("stop_loss_notice_confirm transfer_data >>> {}", transfer_data);
				bs = fund_Alter_Service.regular_investment_confirm(transfer_data, reqParam);
				// 防止重送代碼TxToken
				bs.addData("TXTOKEN", ((Map<String, String>) fund_Alter_Service.getTxToken().getData()).get("TXTOKEN"));
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
			}
		}
		catch (Exception e)
		{
			log.error("regular_investment_confirm Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				// TODO 這邊還要調整 不能存物件，要存成JSNO字串
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				model.addAttribute("regular_investment_confirm", bs);
				target = "fund/regular_investment_confirm";
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * C111定期投資約定變更結果頁!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/regular_investment_result")
	public String regular_investment_result(HttpServletRequest request,@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("regular_investment_result Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		String pageToken = "";
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
			}
			else
			{
				// Check Session Token
				String SessionFinshToken = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null);
				pageToken = reqParam.get("TXTOKEN");
				BaseResult checkToken = fund_Alter_Service.validateToken(pageToken, SessionFinshToken);
				if (!checkToken.getResult())
				{
					bs = checkToken;
				}
				else
				{
					// Get session value by session Key
					BaseResult bsdata = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
					Map<String, String> transfer_data = (Map<String, String>) bsdata.getData();
					
					String IP = WebUtil.getIpAddr(request);
					log.debug(ESAPIUtil.vaildLog("IP >> " + IP));
					reqParam.put("IP",IP);
					
					bs = fund_Alter_Service.regular_investment_result(transfer_data, reqParam);
					// 將查過的資料放入session，待切換 locale 時讀取
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);

				}

			}
		}
		catch (Exception e)
		{
			log.error("regular_investment_result Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				// session add TxToken
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, pageToken);
				model.addAttribute("regular_investment_result", bs);
				target = "fund/regular_investment_result";
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * C116停損/停利通知設定 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/stop_loss_notice")
	public String stop_loss_notice(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("stop_loss_notice Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		try
		{
			// 清除暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.STEP2_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			// Get session value by session Key
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			bs = fund_Alter_Service.stop_loss_notice(cusidn, reqParam);
		}
		catch (Exception e)
		{
			log.error("stop_loss_notice Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				model.addAttribute("stop_loss_notice", bs);
				target = "fund/stop_loss_notice";
			}
			else
			{
				// 新增回上一頁的路徑
				bs.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * C116停損/停利通知設定輸入頁 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/stop_loss_notice_step1")
	public String stop_loss_notice_step1(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("stop_loss_notice_step1 Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			boolean back = "Y".equals(okMap.get("back"));
			if (hasLocale || back)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null));
			}
			else
			{
				// Get session value by session Key
				bs = fund_Alter_Service.stop_loss_notice_step1(reqParam);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, bs);
			}

		}
		catch (Exception e)
		{
			log.error("stop_loss_notice_step1 Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				// TODO 這邊還要調整 不能存物件，要存成JSNO字串
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				model.addAttribute("stop_loss_notice_step1", bs);
				target = "fund/stop_loss_notice_step1";
			}
			else
			{

				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * C116停損/停利通知設定確認頁 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/stop_loss_notice_confirm")
	public String stop_loss_notice_confirm(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("stop_loss_notice_confirm Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		try
		{

			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
			}
			else
			{
				// Get session value by session Key
				BaseResult bsdata = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String, String> transfer_data = (Map<String, String>) bsdata.getData();
				log.debug("stop_loss_notice_confirm transfer_data >>> {}", transfer_data);
				bs = fund_Alter_Service.stop_loss_notice_confirm(transfer_data, okMap);
				// 防止重送代碼TxToken
				bs.addData("TXTOKEN", ((Map<String, String>) fund_Alter_Service.getTxToken().getData()).get("TXTOKEN"));
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
			}

		}
		catch (Exception e)
		{
			log.error("stop_loss_notice_confirm Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				// TODO 這邊還要調整 不能存物件，要存成JSNO字串
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				model.addAttribute("stop_loss_notice_confirm", bs);
				target = "fund/stop_loss_notice_confirm";
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * C116停損/停利通知設定結果頁 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/stop_loss_notice_result")
	public String stop_loss_notice_result(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("stop_loss_notice_result Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		String pageToken = "";
		try
		{

			// 解決Trust Boundary Violation
		    Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			//Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
			}
			else
			{

				// Check Session Token
				String SessionFinshToken = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null);
				pageToken = reqParam.get("TXTOKEN");
				BaseResult checkToken = fund_Alter_Service.validateToken(pageToken, SessionFinshToken);
				if (!checkToken.getResult())
				{
					bs = checkToken;
				}
				else
				{
					BaseResult bsdata = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
					Map<String, String> transfer_data = (Map<String, String>) bsdata.getData();
					log.debug("stop_loss_notice_result transfer_data >>> {}", transfer_data);
					bs = fund_Alter_Service.stop_loss_notice_result(transfer_data, reqParam);
					// 將查過的資料放入session，待切換 locale 時讀取
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);

				}
			}

		}
		catch (Exception e)
		{
			log.error("stop_loss_notice Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				// session add TxToken
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, pageToken);
				model.addAttribute("stop_loss_notice_result", bs);
				target = "fund/stop_loss_notice_result";
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * C118 SMART FUND自動贖回設定 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/smart_fund")
	public String smart_fund(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("smart_fund Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		try
		{
			// 清除暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.STEP2_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			bs.setResult(true);
		}
		catch (Exception e)
		{
			log.error("smart_fund Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				model.addAttribute("smart_fund", bs);
				target = "fund/smart_fund";
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * C118 SMART FUND自動贖回設定 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/smart_fund_step1")
	public String smart_fund_step1(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("smart_fund_step1 Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			

			String sI_P = (String) SessionUtil.getAttribute(model, SessionUtil.USERIP, null);
			log.debug("sI_P >>>>>>>>>>>>>>>> {}", sI_P );
			String USERp = sI_P.substring(0,sI_P.indexOf("."));
			boolean checknbinhouse = USERp.equals("10")? true : false;		
			if(!checknbinhouse)
			{
				bs.setErrorMessage("Z607", fund_Alter_Service.getMessageByMsgCode("Z607"));
				bs.setResult(false);
				throw new Exception("IP is not in bank");
			}
			
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			boolean back = "Y".equals(okMap.get("back"));
			if (hasLocale || back)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP1_LOCALE_DATA, null));
			}
			else
			{
				// Get session value by session Key
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
				bs = fund_Alter_Service.smart_fund_step1(cusidn, reqParam);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.STEP1_LOCALE_DATA, bs);
			}

		}
		catch (Exception e)
		{
			log.error("smart_fund_step1 Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				model.addAttribute("smart_fund_step1", bs);
				target = "fund/smart_fund_step1";
			}
			else
			{
				// 新增回上一頁的路徑
				bs.setPrevious("/INDEX/index");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * SMART FUND自動贖回設定 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/smart_fund_step2")
	public String smart_fund_step2(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("smart_fund_step2 Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			boolean back = "Y".equals(okMap.get("back"));
			if (hasLocale || back)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.STEP2_LOCALE_DATA, null));
				log.trace("smart_fund_step2 locale bs.getData>> {}", bs.getData());
			}
			else
			{
				bs = fund_Alter_Service.smart_fund_step2(reqParam);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.STEP2_LOCALE_DATA, bs);
				log.trace("bs.getResult>> {}", bs.getResult());
			}

		}
		catch (Exception e)
		{
			log.error("smart_fund_step2 Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				// TODO 這邊還要調整 不能存物件，要存成JSNO字串
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				model.addAttribute("smart_fund_step2", bs);
				target = "fund/smart_fund_step2";
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * SMART FUND自動贖回設定 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/smart_fund_confirm")
	public String smart_fund_confirm(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("smart_fund_confirm Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
			}
			else
			{
				// Get session value by session Key
				BaseResult bsdata = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String, String> transfer_data = (Map<String, String>) bsdata.getData();
				log.debug("stop_loss_notice_confirm transfer_data >>> {}", transfer_data);
				bs = fund_Alter_Service.smart_fund_confirm(transfer_data, reqParam);
				// 防止重送代碼TxToken
				bs.addData("TXTOKEN", ((Map<String, String>) fund_Alter_Service.getTxToken().getData()).get("TXTOKEN"));
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
			}

		}
		catch (Exception e)
		{
			log.error("smart_fund_confirm Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				// TODO 這邊還要調整 不能存物件，要存成JSNO字串
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				model.addAttribute("smart_fund_confirm", bs);
				target = "fund/smart_fund_confirm";
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	/**
	 * SMART FUND自動贖回設定 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/smart_fund_result")
	public String smart_fund_result(@RequestParam Map<String, String> reqParam, Model model)
	{
		log.trace("smart_fund_result Start ~~~~~~~~~~~~~~~~");
		String target = ERRORJSP;
		BaseResult bs = new BaseResult();
		String pageToken = "";
		try
		{
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale)
			{
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
			}
			else
			{
				// Check Session Token
				String SessionFinshToken = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null);
				pageToken = reqParam.get("TXTOKEN");
				BaseResult checkToken = fund_Alter_Service.validateToken(pageToken, SessionFinshToken);
				if (!checkToken.getResult())
				{
					bs = checkToken;
				}
				else
				{
					// Get session value by session Key
					BaseResult bsdata = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
					String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
					String dpmyemail = (String) SessionUtil.getAttribute(model, SessionUtil.DPMYEMAIL, null);
					Map<String, String> transfer_data = (Map<String, String>) bsdata.getData();
					log.debug("stop_loss_notice_result transfer_data >>> {}", transfer_data);
					bs = fund_Alter_Service.smart_fund_result(cusidn, dpmyemail, transfer_data, reqParam);
					// 將查過的資料放入session，待切換 locale 時讀取
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
			}
		}
		catch (Exception e)
		{
			log.error("smart_fund_result Error ", e);
		}
		finally
		{
			if (bs != null && bs.getResult())
			{
				// session add TxToken
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, pageToken);
				model.addAttribute("smart_fund_result", bs);
				target = "fund/smart_fund_result";
			}
			else
			{
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

}
