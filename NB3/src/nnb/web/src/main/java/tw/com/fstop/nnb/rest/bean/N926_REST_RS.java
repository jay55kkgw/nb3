package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N926_REST_RS  extends BaseRestBean implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3895014146518018882L;

	private LinkedList<N926_REST_RSDATA> REC;

	public LinkedList<N926_REST_RSDATA> getREC() {
		return REC;
	}

	public void setREC(LinkedList<N926_REST_RSDATA> rEC) {
		REC = rEC;
	}
	
}
