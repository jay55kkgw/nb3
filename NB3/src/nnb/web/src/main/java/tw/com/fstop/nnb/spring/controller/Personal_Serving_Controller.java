package tw.com.fstop.nnb.spring.controller;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.idgate.bean.N106_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N106_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N830_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N830_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.N831_IDGATE_DATA;
import tw.com.fstop.idgate.bean.N831_IDGATE_DATA_VIEW;
import tw.com.fstop.nnb.custom.annotation.ACNVERIFY2;
import tw.com.fstop.nnb.custom.annotation.ISTXNLOG;
import tw.com.fstop.nnb.service.DaoService;
import tw.com.fstop.nnb.service.Electronic_Checksheet_Service;
import tw.com.fstop.nnb.service.IdGateService;
import tw.com.fstop.nnb.service.Personal_Serving_Service;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({ SessionUtil.CUSIDN, SessionUtil.USERID, SessionUtil.CONFIRM_LOCALE_DATA,SessionUtil.IDGATE_TRANSDATA,
		SessionUtil.RESULT_LOCALE_DATA,SessionUtil.DPMYEMAIL,SessionUtil.TRANSFER_RESULT_TOKEN })
@Controller
@RequestMapping(value = "/PERSONAL/SERVING")
public class Personal_Serving_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Personal_Serving_Service personal_serving_service;
	@Autowired
	private Electronic_Checksheet_Service electronic_checksheet_service;
	
	@Autowired
	private DaoService daoService;
	

	@Autowired		 
	IdGateService idgateservice;
	
	// 使用者名稱變更
	@RequestMapping(value = "/username_alter")
	public String username_alter(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("username_alter>>");

		try {
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");

			String username = (String) SessionUtil.getAttribute(model, SessionUtil.USERID, null);
			String userId = new String(Base64.getDecoder().decode(username), "utf-8");
			model.addAttribute("userId", userId);
		} catch (UnsupportedEncodingException e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("username_alter error >> {}",e);
		}

		target = "/personal_serving/username_alter";
		return target;
	}

	// 使用者名稱變更結果頁
	@RequestMapping(value = "/username_alter_result")
	public String username_alter_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("username_alter_result>>");
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			bs = new BaseResult();
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			// 解決 Trust Boundary Violation

			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>{}" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				bs = personal_serving_service.username_alter_result(cusidn, okMap);
				bs.addData("TIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				bs.addData("NEWUID", reqParam.get("NEWUID"));
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				String userId;
				if(bs != null && bs.getResult()) {
					try {
						userId = new String(Base64.getEncoder().encodeToString(okMap.get("NEWUID").getBytes("UTF-8")));
						SessionUtil.addAttribute(model, SessionUtil.USERID, userId);
					} catch (UnsupportedEncodingException e) {
						//Avoid Information Exposure Through an Error Message
						//e.printStackTrace();
						log.error("username_alter_result error >> {}",e);
					}
				}
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("username_alter_result error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/personal_serving/username_alter_result";
				model.addAttribute("username_alter_result", bs);
			} else {
				bs.setPrevious("/PERSONAL/SERVING/username_alter");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 密碼變更
	@RequestMapping(value = "/pw_alter")
	public String pw_alter(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("username_alter>>");
		target = "/personal_serving/pw_alter";
		return target;
	}

	// 密碼變更更改頁
	@RequestMapping(value = "/pw_alter_step")
	public String pw_alter_step(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("pw_alter_step>>");
		BaseResult bs = new BaseResult();
		try {
			// 解決Trust Boundary Violation 
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam); 
			
			String ACRADIO = okMap.get("ACRADIO");

			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				ACRADIO = (String) SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null);
			}
			
			// 判斷使用者要變更的密碼
			log.trace(ESAPIUtil.vaildLog("ACRADIO: {}"+ ACRADIO));
			if (ACRADIO.equals("pw")) {
				// 簽入密碼
				target = "/personal_serving/pw_alter_step";
				
			} else if (ACRADIO.equals("ssl")) {
				// 交易密碼
				target = "/personal_serving/pw_alter_ssl_step";
				
			} else if (ACRADIO.equals("elec")) {
				// 電子對帳單密碼
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				// 檢查是否有申請電子帳單
				boolean isDPUser = electronic_checksheet_service.isDPUser(cusidn);
				if(isDPUser) {
					target = "/electronic_checksheet/elec_pw_alter";
				}else {
					target = "/apply/apply_confirm";
				}
			}
			
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, ACRADIO);
			
			// 新密碼不可與身分證/營利事業統一編號相同
			String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				cusidn = new String(Base64.getDecoder().decode(cusidn), "utf-8");
				log.trace("cusidn: " + cusidn);
				model.addAttribute("UID", cusidn);
			}
			
			// 新密碼不可與使用者名稱相同
			String userId = (String) SessionUtil.getAttribute(model, SessionUtil.USERID, null);
			// session有無USERID
			if( !"".equals(userId) && userId != null ) {
				userId = new String(Base64.getDecoder().decode(userId), "utf-8");
				log.trace("userId: " + userId);
				model.addAttribute("USERNAME", userId);
			}
			
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("pw_alter_step error >> {}",e);
		}
		return target;
	}

	// pw變更結果頁
//	@ISTXNLOG(value="N940_S1")
	@RequestMapping(value = "/pw_alter_result")
	public String pw_alter_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace(ESAPIUtil.vaildLog("pw_alter_result>>" +CodeUtil.toJson(reqParam)));
		try {
			bs = new BaseResult();

			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				bs.reset();
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				bs = personal_serving_service.pw_alter_result(cusidn, reqParam);
				bs.addData("TIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("pw_alter_result error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/personal_serving/pw_alter_result";
				model.addAttribute("pw_alter_result", bs);
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// ssl變更結果頁
//	@ISTXNLOG(value="N940_T1")
	@RequestMapping(value = "/pw_alter_ssl_result")
	public String pw_alter_ssl_result(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace(ESAPIUtil.vaildLog("pw_alter_ssl_result>>" +CodeUtil.toJson(reqParam)));
		try {
			bs = new BaseResult();

			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}
			if (!hasLocale) {
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				bs = personal_serving_service.pw_alter_ssl_result(cusidn, reqParam);
				bs.addData("TIME", DateUtil.getCurentDateTime("yyyy/MM/dd HH:mm:ss"));
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);

			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("pw_alter_ssl_result error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/personal_serving/pw_alter_ssl_result";
				model.addAttribute("pw_alter_ssl_result", bs);
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 取得N997輸入頁下拉選單之銀行代碼列表
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getBankList_aj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody List getInACNO(HttpServletRequest request, HttpServletResponse response, Model model) {
		String str = "getBankList_aj";
		log.trace("getBankList_aj >> {}", str);
		List bankList = null;
		try {
			bankList = daoService.getBankList();
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getInACNO error >> {}",e);
		}
		return bankList;
	}

	/**
	 * 取得N997輸入頁下拉選單之約定轉入帳號
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getN997AgInco_aj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult getInACNO(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String str = "getN997AgInco_aj";
		log.trace("getN997AgInco_aj >> {}", str);
		BaseResult bs = new BaseResult();
		try {
			log.trace(ESAPIUtil.vaildLog("reqParam>>" +CodeUtil.toJson(reqParam)));
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			reqParam.put("CUSIDN", cusidn);
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

			bs = personal_serving_service.getAgInAcno(okMap);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("getInACNO error >> {}",e);
		}
		return bs;
	}

	// 常用帳號設定(N997)-輸入頁
	@RequestMapping(value = "/common_acct_setting")
	public String common_acct_setting(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = null;
		log.trace("common_acct_setting");
		try {
			bs = new BaseResult();
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
			String action = "QUERY";
			log.debug("N997cusidn>>{}", cusidn);
			log.debug("N997action>>{}", action);
			bs = personal_serving_service.common_acct_setting(cusidn, action);

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("common_acct_setting error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/personal_serving/common_acct_setting";
				model.addAttribute("common_acct_setting", bs);
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}

	// 常用帳號設定(N997)-結果頁
	@ISTXNLOG(value="N997")
	@RequestMapping(value = "/common_acct_setting_result")
	public String common_acct_setting_step(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("common_acct_setting_action");
		log.trace(ESAPIUtil.vaildLog("reqParam >> {}"+CodeUtil.toJson(reqParam)));
		String target = "/error";
		BaseResult bs = null;
		String cusidn = new String(
				Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
		reqParam.put("DPUSERID", cusidn);
		// 解決 Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
		try {
			// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("bs.getResult() >>{}", bs.getResult());
					throw new Exception();
				}
			}else {
				bs = new BaseResult();
				// pretendXSS
				//log.trace("BEFORE　XSSFilter >>" + CodeUtil.toJson(okMap));
				okMap.put("DPGONAME", ESAPIUtil.pretendNameColumnFromXSS(okMap.get("DPGONAME")));
				//log.trace("AFTER　XSSFilter >>" + CodeUtil.toJson(okMap));
				log.debug("cusidn" + cusidn);
				bs = personal_serving_service.common_acct_setting_action(okMap);
				
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error(ESAPIUtil.vaildLog("common_acct_setting_step error >> {}"+e));
		} finally {
			if (bs != null && bs.getResult()) {
				target = "/personal_serving/common_acct_setting_result";
				model.addAttribute("common_acct_setting_result", bs);
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
		
	/**
	 * 通知設定輸入頁
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/notification_service")
	public String notifySetting(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		log.trace(ESAPIUtil.vaildLog("notifySetting reqParam >> {}"+CodeUtil.toJson(reqParam)));
		String target = "/error";
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		BaseResult bs = new BaseResult();
		boolean hasLocale = okMap.containsKey("locale");
		try {
			if (hasLocale) {
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				} else {
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			}
			if (!hasLocale) {
				// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
				bs = personal_serving_service.getNotifyInfo(cusidn);
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				log.trace("notifyDate >> {}",bs.getData().toString());
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("notifySetting error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				log.trace("bs >>{}", bs);
				String authority = (String) SessionUtil.getAttribute(model, SessionUtil.AUTHORITY, null);
				String email = (String) SessionUtil.getAttribute(model, SessionUtil.DPMYEMAIL, null);
				bs.addData("authority", authority);
				bs.addData("DPMYEMAIL", email);
				model.addAttribute("notifyInfo", bs);
				target = "/personal_serving/notification_service";
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 通知設定結果頁
	 * 
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@ISTXNLOG(value="N998")
	@RequestMapping(value = "/notification_service_result")
	public String notifyResult(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		BaseResult bs = new BaseResult();
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
		boolean hasLocale = okMap.containsKey("locale");
		try {
			if (hasLocale) {
				
				log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if (!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				} else {
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			} else {
				// 解決 Trust Boundary Violation
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
				okMap.put("CUSIDN", cusidn);
				
				bs = new BaseResult();
				
				// log.debug("sessionId >>{}", sessionId);
				
				log.trace(ESAPIUtil.vaildLog("ok.NOTIFYDATA >>{}"+okMap.get("NOTIFYDATA")));
				bs = personal_serving_service.setNotify(okMap);
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("notifyResult error >> {}",e);
		} finally {
			if (bs != null && bs.getResult()) {
				log.trace("bs >>{}", bs);
				String email = (String) SessionUtil.getAttribute(model, SessionUtil.DPMYEMAIL, null);
				bs.addData("DPMYEMAIL", email);
				model.addAttribute("notifyResult", bs);
				target = "/personal_serving/notification_service_result";
			} else {
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;

	}

		/**
		 * N935_國內臺幣匯入匯款通知設定輸入頁
		 * 
		 * @param request
		 * @param response
		 * @param reqParam
		 * @param model
		 * @return
		 */
		@RequestMapping(value = "/inward_notice_setting")
		public String inward_notice_setting(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			String target = "/error";
			BaseResult bs = new BaseResult();
			try {
				Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
				// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
				if (hasLocale) {
					log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
					if (!bs.getResult()) {
						log.trace("throw new Exception()");
						throw new Exception();
					} else {
						// session 資料放入 map 讓後續 model 和回上一頁取值
						okMap.putAll((Map<String, String>) bs.getData());
					}
				} else {
					// 解決 Trust Boundary Violation
					String cusidn = new String(
							Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
					okMap.put("CUSIDN", cusidn);
					bs = personal_serving_service.inward_notice_setting(okMap);
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("inward_notice_setting error >> {}",e);
			} finally {
				if (bs != null && bs.getResult()) {
					log.trace("bs >>{}", bs.getData());
					model.addAttribute("inward_notice_setting", bs);
					target = "/personal_serving/inward_notice_setting";
				} else {
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}
			return target;
		}

		/**
		 * 
		 * N935_國內臺幣匯入匯款通知設定結果頁
		 * 
		 * @param request
		 * @param response
		 * @param reqParam
		 * @param model
		 * @return
		 */
		@ACNVERIFY2(acn="ACNINFO")
		@RequestMapping(value = "/inward_notice_setting_result")
		public String inward_notice_setting_result(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			String target = "/error";
			BaseResult bs = new BaseResult();
			log.debug(ESAPIUtil.vaildLog("inward_notice_setting_result {} "+CodeUtil.toJson(reqParam)));
			try {
				Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
				// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
				if (hasLocale) {
					log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
					if (!bs.getResult()) {
						log.trace("throw new Exception()");
						throw new Exception();
					} else {
						// session 資料放入 map 讓後續 model 和回上一頁取值
						okMap.putAll((Map<String, String>) bs.getData());
					}
				} else {
					// 解決 Trust Boundary Violation
					String cusidn = new String(
							Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
					okMap.put("CUSIDN", cusidn);
					bs = personal_serving_service.inward_notice_setting_result(okMap);
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("inward_notice_setting_result error >> {}",e);
			} finally {
				if (bs != null && bs.getResult()) {
					log.trace("bs >>{}", bs.getData());
					model.addAttribute("inward_notice_setting_result", bs);
					target = "/personal_serving/inward_notice_setting_result";
				} else {
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}
			return target;
		}
		
	
		/**
		 * N999 帳戶總覽設定(選擇頁)
		 * @param reqParam
		 * @param model
		 * @return
		 */
		@RequestMapping(value = "/account_settings")
		public String account_settings(@RequestParam Map<String, String> reqParam, Model model) 
		{
			String target = "/error";
			BaseResult bs = new BaseResult();
			try {
				Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
				// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
				if (hasLocale) {
					log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
					if (!bs.getResult()) {
						log.trace("throw new Exception()");
						throw new Exception();
					} else {
						// session 資料放入 map 讓後續 model 和回上一頁取值
						okMap.putAll((Map<String, String>) bs.getData());
					}
				} else {
					// 解決 Trust Boundary Violation
					String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
					bs = personal_serving_service.account_settings(cusidn,okMap);
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("account_settings error >> {}",e);
			} finally {
				if (bs != null && bs.getResult()) {
					log.trace("bs >>{}", bs.getData());
					model.addAttribute("inward_notice_setting", bs);
					target = "/personal_serving/account_settings";
				} else {
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}
			return target;
		}

		/**
		 * N999 帳戶總覽設定(結果頁)
		 * @param reqParam
		 * @param model
		 * @return
		 */
		@ISTXNLOG(value="N999")
		@RequestMapping(value = "/account_settings_result")
		public String account_settings_result(@RequestParam Map<String, String> reqParam, Model model) 
		{
			String target = "/error";
			BaseResult bs = new BaseResult();
			log.debug(ESAPIUtil.vaildLog("inward_notice_setting_result {} "+CodeUtil.toJson(reqParam)));
			try {
				Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
				// 判斷LOCAL KEY 是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
				if (hasLocale) {
					log.trace(ESAPIUtil.vaildLog("locale>>" + okMap.get("locale")));
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
					if (!bs.getResult()) {
						log.trace("throw new Exception()");
						throw new Exception();
					} else {
						// session 資料放入 map 讓後續 model 和回上一頁取值
						okMap.putAll((Map<String, String>) bs.getData());
					}
				} else {
					// 解決 Trust Boundary Violation
					String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)));
					okMap.put("CUSIDN", cusidn);
					bs = personal_serving_service.account_settings_result(cusidn, okMap);
					SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
				}
			} catch (Exception e) {
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("account_settings_result error >> {}",e);
			} finally {
				if (bs != null && bs.getResult()) {
					log.trace("bs >>{}", bs.getData());
					model.addAttribute("inward_notice_setting_result", bs);
					target = "/personal_serving/account_settings_result";
				} else {
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}
			return target;
		}

		
		/**
		 *  N106
		 * @param request
		 * @param response
		 * @param reqParam
		 * @param model
		 * @return
		 */
		@RequestMapping(value = "/vouchers_print")
		public String vouchers_print(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			log.trace("vouchers_print...");
			
			String target = "/error";
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
			
			// 登入時的身分證字號
			String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if (!"".equals(cusidn) && cusidn != null) {
				// 解密成明碼
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("cusidn: " + cusidn);

			} else {
				log.error("session no cusidn!!!");
			}
			try {
				Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
				//IDGATE身分
		        String idgateUserFlag="N";		 
		        try {		 
		        	if(IdgateData==null) {
		        		IdgateData = new HashMap<String, Object>();
		        	}
		        	BaseResult tmp=idgateservice.checkIdentity(cusidn);		 
		        	if(tmp.getResult()) {		 
		        		idgateUserFlag="Y";                  		 
		        	}		 
		        	tmp.addData("idgateUserFlag",idgateUserFlag);		 
		        	IdgateData.putAll((Map<String, String>) tmp.getData());
		        	SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);
		        }catch(Exception e) {		 
		        	log.debug("idgateUserFlag error {}",e);		 
		        }		 
		        model.addAttribute("idgateUserFlag",idgateUserFlag);
				// IDGATE transdata
	    		String adopid = "N106";
	    		String title = "您有一筆各類所得扣繳憑單列印待確認";
	    		Map<String, String> result = new HashMap<String, String>();
	        	if(IdgateData != null) {
		            if(IdgateData.get("idgateUserFlag").equals("Y")) {
		            	result.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
		            	result.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
		            	result.put("TRANTITLE", "各類所得扣繳憑單列印");
		            	result.put("TRANNAME", "-");
		            	result.put("CUSIDN", cusidn);
						IdgateData.put(adopid + "_IDGATE", idgateservice.coverTxnData(N106_IDGATE_DATA.class, N106_IDGATE_DATA_VIEW.class, result));

		                SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
		            }
	        	}
	            model.addAttribute("idgateAdopid", adopid);
	            model.addAttribute("idgateTitle", title);
	            
			}catch(Exception e) {
				log.error("{}", e);
			}
			model.addAttribute("CUSIDN", cusidn);

			target = "/personal_serving/vouchers_print";
			return target;
		}
		
		/**
		 * N106 列表頁
		 *  
		 * @param request
		 * @param response
		 * @param reqParam
		 * @param model
		 * @return
		 */
		@RequestMapping(value = "/vouchers_print_list")
		public String vouchers_print_list(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			log.trace("vouchers_print_list...");
			String target = "/error";

			log.trace(ESAPIUtil.vaildLog("vouchers_print_list_reqParam>>{}"+CodeUtil.toJson(reqParam)));

			BaseResult bs = new BaseResult();

			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			
			try {
				// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
				if (hasLocale) {
					log.trace(ESAPIUtil.vaildLog("locale: {}"+ okMap.get("locale")));
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
					if (!bs.getResult()) {
						log.trace("bs.getResult(): {}", bs.getResult());
						throw new Exception();
					} else {
						// session 資料放入 map 讓後續 model 和回上一頁取值
						okMap.putAll((Map<String, String>) bs.getData());
					}
				} else {
					
					// 登入時的身分證字號
					String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
					// session有無CUSIDN
					if (!"".equals(cusidn) && cusidn != null) {
						// 解密成明碼
						cusidn = new String(Base64.getDecoder().decode(cusidn));
						log.trace("cusidn: " + cusidn);

						// Todo 轉出成功是否Email通知
						// boolean chk_result = pay_expense_service.chkContains(cusidn);
						// model.addAttribute("sendMe", chk_result);
						okMap.put("CUSIDN", cusidn);
					} else {
						log.error("session no cusidn!!!");
					}

	                //IDgate驗證		 
	                try {		 
	                    if(okMap.get("FGTXWAY").equals("7")) {		 

	        				Map<String,Object>IdgateDataSession = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);				
	        				Map<String,Object>IdgateData = (Map<String, Object>) IdgateDataSession.get("N106_IDGATE");
	                        okMap.put("sessionID", (String)IdgateData.get("sessionid"));		 
	                        okMap.put("txnID", (String)IdgateData.get("txnID"));		 
	                        okMap.put("idgateID", (String)IdgateData.get("IDGATEID"));		 
	                    }		 
	                }catch(Exception e) {		 
	                    log.error("IDGATE ValidateFail>>{}",bs.getResult());		 
	                }  
					
					bs = personal_serving_service.vouchers_print_list(okMap);
					bs.addData("FGTXWAY", reqParam.get("FGTXWAY"));
					
					// 將查過的資料放入session，待切換 locale 時讀取
					SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				}
				String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
				//IDGATE身分
		        String idgateUserFlag="N";		 
		        Map<String,Object>IdgateData=(Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
		        idgateUserFlag = (String) IdgateData.get("idgateUserFlag");
		        model.addAttribute("idgateUserFlag",idgateUserFlag);
			} catch (Exception e) { 
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("vouchers_print_list error >> {}",e);
			} finally {
				if (bs.getResult()) {
					target = "/personal_serving/vouchers_print_list";
					model.addAttribute("vouchers_print_list", bs);
				} else {
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}
			log.trace("target {}", target);
			return target;
		}
		
		/**
		 * N106 明細頁
		 * 
		 * @param request
		 * @param response
		 * @param reqParam
		 * @param model
		 * @return
		 */
		@RequestMapping(value = "/vouchers_print_detail")
		public String vouchers_print_detal(HttpServletRequest request, HttpServletResponse response,
				@RequestParam Map<String, String> reqParam, Model model) {
			log.trace("vouchers_print_detail...");
			String target = "/error";

			log.trace(ESAPIUtil.vaildLog("vouchers_print_detail_reqParam >>{}"+CodeUtil.toJson(reqParam)));

			BaseResult bs = new BaseResult();

			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);

			try {

				// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
				boolean hasLocale = okMap.containsKey("locale");
				if (hasLocale) {
					log.trace(ESAPIUtil.vaildLog("locale: {}"+ okMap.get("locale")));
					bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
					if (!bs.getResult()) {
						// TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
						log.trace("bs.getResult(): {}", bs.getResult());
						throw new Exception();
					} else {
						// session 資料放入 map 讓後續 model 和回上一頁取值
						okMap.putAll((Map<String, String>) bs.getData());
					}
				} else {
					
					bs = personal_serving_service.vouchers_print_detail(okMap);
					
					// 將查過的資料放入session，待切換 locale 時讀取
					SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
				}
			} catch (Exception e) { 
				//Avoid Information Exposure Through an Error Message
				//e.printStackTrace();
				log.error("vouchers_print_detal error >> {}",e);
			} finally {
				if (bs.getResult()) {
					target = "/personal_serving/vouchers_print_detail";
					// 這邊還要調整 不能存物件，要存成JSNO字串
					model.addAttribute("vouchers_print_detail", bs);
				} else {
					model.addAttribute(BaseResult.ERROR, bs);
				}
			}
			log.trace("target {}", target);
			return target;
		}
		
}
