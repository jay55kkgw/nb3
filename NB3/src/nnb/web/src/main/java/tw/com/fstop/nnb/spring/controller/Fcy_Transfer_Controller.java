package tw.com.fstop.nnb.spring.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.idgate.bean.F001R_IDGATE_DATA;
import tw.com.fstop.idgate.bean.F001R_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.F001S_IDGATE_DATA;
import tw.com.fstop.idgate.bean.F001S_IDGATE_DATA_VIEW;
import tw.com.fstop.idgate.bean.F001T_IDGATE_DATA;
import tw.com.fstop.idgate.bean.F001T_IDGATE_DATA_VIEW;
import tw.com.fstop.nnb.aop.Dialog;
import tw.com.fstop.nnb.service.Acct_Transfer_Service;
import tw.com.fstop.nnb.service.Fcy_Transfer_Service;
import tw.com.fstop.nnb.service.IdGateService;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.SessionUtil;
import tw.com.fstop.util.SpringBeanFactory;
import tw.com.fstop.util.StrUtil;
import tw.com.fstop.web.util.WebUtil;

@SessionAttributes({ SessionUtil.CUSIDN, SessionUtil.PD, SessionUtil.TRANSFER_CONFIRM_TOKEN, 
						SessionUtil.CONFIRM_LOCALE_DATA, SessionUtil.RESULT_LOCALE_DATA, SessionUtil.BACK_DATA,
						SessionUtil.TRANSFER_CONFIRM_TOKEN, SessionUtil.TRANSFER_DATA, SessionUtil.TRANSFER_RESULT_TOKEN,
						SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, SessionUtil.FXAGREEFLAG, SessionUtil.IDGATE_TRANSDATA })
@Controller
@RequestMapping(value = "/FCY/TRANSFER")
public class Fcy_Transfer_Controller {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private Acct_Transfer_Service acct_transfer_service ;
	
	@Autowired
	private Fcy_Transfer_Service fcy_transfer_service;
	
	@Autowired
	private IdGateService idgateservice;

	
	/**
	 * 取得轉帳之轉入帳號
	 * 
	 * @param request
	 * @param response
	 * @param reqParama
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getInAcno_aj", produces = { "application/json;charset=UTF-8" })
	public @ResponseBody BaseResult getInACNO(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String str = "getInAcno_aj";
		log.trace(ESAPIUtil.vaildLog("hi >> " + str));
		log.trace(ESAPIUtil.vaildLog("reqParam >> " + CodeUtil.toJson(reqParam)));
		BaseResult bs = new BaseResult();
		try {
			String cusidn = new String(
					Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)),
					"utf-8");
			String type = reqParam.get("type");
			bs = fcy_transfer_service.getAcnoList(cusidn, type);
			
			// 過濾非050的帳戶
			Map<String,Object> data = (Map<String,Object>) bs.getData();
			log.debug("Before process data = {}", data.get("REC"));
			LinkedHashMap<String,String> labelMap = new LinkedHashMap<String,String>();
			
			for(String key: ((Map<String,String>)data.get("REC")).keySet()) {
				if((key == "#0" || key == "#1" || key == "#2") || key.contains("\"BNKCOD\":\"050\"")) {
					log.debug("Add key: " + key + " value: " + ((Map<String,String>)data.get("REC")).get(key));
					labelMap.put(key, ((Map<String,String>)data.get("REC")).get(key));
				}
			}
			data.replace("REC", labelMap);
			log.debug("After process data = {}", data.get("REC"));
			bs.setData(data);
			
		} catch (UnsupportedEncodingException e) {
			// Avoid Information Exposure Through an Error Message
			// e.printStackTrace();
			log.error("getInACNO error >> {}", e);
		}

		return bs;

	}
	
	/**
	 * 交易單據
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/fxcertpreview")
	public String fxcertpreview(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("fxcertpreview...");
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		try {
			BaseResult bs = new BaseResult();
			
			String adtxno = okMap.get("ADTXNO");
			
			// 登入時的身分證字號
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			
			// 幣別資料
			bs = fcy_transfer_service.getFxcert(adtxno, cusidn);
			model.addAttribute("fxcertpreview", bs);
			
			target = "/acct_fcy/F001CertPreview";
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("fxcertpreview error >> {}",e);
		}
		
		return target;
	}
	
	
	// 幣別Ajax
	@RequestMapping(value = "/currency_ajax", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody List<Map<String, String>> currency_ajax(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> reqParam, Model model) {
		log.trace("currency_ajax...");
		List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
		try {
			String acn = reqParam.get("ACN");
			// 幣別資料
			resultList = fcy_transfer_service.getCurrency(acn);
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("currency_ajax error >> {}",e);
		}
		return resultList;
	}
	
	/**
	 * 外匯結購售及轉帳 匯款分類編號查詢
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/fxremitquery_fix")
	public String fxremitquery_fix(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/acct_fcy/FxRemitQuery_Fix";
		return target;
	}
	
	/**
	 * 外匯結購售及轉帳 通知頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_transfer")
	public String f_transfer(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/acct_fcy/f_transfer";
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		String urlId = okMap.get("ACN");
		log.trace(ESAPIUtil.vaildLog("urlId: " + urlId));
		BaseResult bs = null;
		try {
			String fxagreeflag = (String) SessionUtil.getAttribute(model, SessionUtil.FXAGREEFLAG, null);
			log.debug("f_transfer.fxagreeflag: {}", fxagreeflag);
			bs = new BaseResult();
			bs.addData("urlID", urlId);

			if (StrUtil.isEmpty(fxagreeflag)) {
				target = "/acct_fcy/f_transfer";
				log.trace("bs: {}", bs);
				model.addAttribute("f_transfer", bs);
			} else {
				target = "forward:/FCY/TRANSFER/f_transfer_step1";
				log.trace("bs: {}", bs);
				model.addAttribute("f_transfer_step1_r", bs);
			}
		} catch (Exception e){
			log.error("", e);
		}
		return target;
	}
	
	/**
	 * 外匯結購售及轉帳 輸入頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_transfer_step1")
	public String f_transfer_step1(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/error";
		log.trace("f_transfer_step1...");
		BaseResult bs = null;
		
		// 表單驗證用之可交易時間
		String str_SystemDate = "";
		String sNextYearDay = "";
		String urlId = "";
		boolean uidCheck = false;
		
		try {
			// 閱讀過同意書頁後，外匯部分功能不必再顯示同意書
			String fxagreeflag = reqParam.get("FXAGREEFLAG");
			if( !"".equals(fxagreeflag) && fxagreeflag != null ) {
				SessionUtil.addAttribute(model, SessionUtil.FXAGREEFLAG, fxagreeflag);
			}
			
			// 清除切換語系時暫存的資料
			SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, "");
			SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, "");
		
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			if(okMap.containsKey("URLID")) {
				urlId = okMap.get("URLID");
			}
			

			// 是否是回上一頁
			if("Y".equals(okMap.get("back"))) {
				model.addAttribute("isBack","Y");
			}
			
			// 設定頁面預約日期為明天，如果當前時間太晚則預約後天
			model.addAttribute("tmrDate", DateUtil.getTomorrowOrAfterTomorrowDate());
			
			// 防止F5重送request & 防止使用者不經輸入頁從確認頁發request
			bs = acct_transfer_service.getTxToken();
			log.trace("f_transfer_step1.TXTOKEN: " + ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			if(bs.getResult()) {
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN , ((Map<String,String>) bs.getData()).get("TXTOKEN"));
			}
			
			// 登入時的身分證字號
			String cusidn = (String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null);
			// session有無CUSIDN
			if( !"".equals(cusidn) && cusidn != null ) {
				// 解密成明碼
				cusidn = new String(Base64.getDecoder().decode(cusidn));
				log.trace("cusidn: " + cusidn);
				
				// N920回傳資料處理後回傳
				bs = fcy_transfer_service.getN920Data(cusidn);
				
				// 表單驗證用
				str_SystemDate = fcy_transfer_service.getSystemDate(); // 最小時間
				sNextYearDay = fcy_transfer_service.getSNextYearDay(); // 最大時間
				uidCheck = cusidn.length()==10; // CUSIDN是否為十位數
				
				
				// 轉出成功是否Email通知
				boolean chk_result = fcy_transfer_service.chkContains(cusidn);
				model.addAttribute("sendMe", chk_result);
				
			}else {
				log.error("session no cusidn!!!");
			}
			
			// IDGATE身分
			String idgateUserFlag = "N";
			Map<String, Object> IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			try {
				if (IdgateData == null) {
					IdgateData = new HashMap<String, Object>();
				}
				BaseResult tmp = idgateservice.checkIdentity(cusidn);
				if (tmp.getResult()) {
					idgateUserFlag = "Y";
				}
				tmp.addData("idgateUserFlag", idgateUserFlag);
				IdgateData.putAll((Map<String, String>) tmp.getData());
				SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);
			} catch (Exception e) {
				log.debug("idgateUserFlag error {}", e);
			}
			model.addAttribute("idgateUserFlag", idgateUserFlag);

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("f_transfer_step1 error >> {}",e);
		} finally {
			if(bs!=null && bs.getResult()) {
				target = "/acct_fcy/f_transfer_step1";
				bs.addData("urlID", urlId);
				//第三階段說明
				String isProd = SpringBeanFactory.getBean("isProd");
				bs.addData("isProd",isProd);
				
				model.addAttribute("f_transfer_step1", bs);
				
				model.addAttribute("str_CurrentSystemDate", DateUtil.getDate("/"));
				model.addAttribute("str_SystemDate", str_SystemDate);
				model.addAttribute("sNextYearDay", sNextYearDay);
				model.addAttribute("uidCheck", uidCheck);
				
			}else {
				model.addAttribute(BaseResult.ERROR, bs);
				
			}
		}
		return target;
	}
	
	/**
	 * 外匯結購售及轉帳 即時同幣別 確認頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_transfer_t_confirm")
	public String f_transfer_t_confirm(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("f_transfer_t_confirm...");
		
		String target = "/error";
		String jsondc = "";
		BaseResult bs = new BaseResult();
		String cusidn = "";
		
		log.trace(ESAPIUtil.vaildLog("f_transfer_t_confirm.reqParam: {}"+CodeUtil.toJson(reqParam)));
		
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
		try {
			// 回上一頁重新賦值
			if( okMap.containsKey("previous") ){
				Map<String, String> backMap = (Map<String, String>) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				okMap = backMap;
			}
			
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if( !bs.getResult())
				{
//					TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				}else{
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			}else {
				// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
				String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
				log.trace("f_transfer_t_confirm.token: {}", token);
				if(StrUtil.isNotEmpty(okMap.get("TOKEN"))  &&  okMap.get("TOKEN").equals(token)) {
					bs.setResult(Boolean.TRUE);
				}
				log.trace("bs.getResult(): {}",bs.getResult());
				if(!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				}
				
				// TOKEN驗證成功，資料處理後進入確認頁
				bs.reset();
				
				// 外匯結購售及轉帳 確認頁 處理邏輯
				cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				okMap = fcy_transfer_service.f_transfer_t_confirm_process(okMap, cusidn);
				
				// IKEY要使用的JSON:DC
				jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
				log.trace(ESAPIUtil.vaildLog("f_transfer_t_confirm.jsondc: " + jsondc));
				okMap.put("jsondc", jsondc);
				log.trace(ESAPIUtil.vaildLog("f_transfer_t_confirm.okMap: {}"+ CodeUtil.toJson(okMap)));
				
				bs = fcy_transfer_service.f_transfer_t_confirm(okMap, cusidn);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
			}
			
			// IDGATE transdata
			Map<String, Object> IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			String adopid = "F001T";
    		String title = "您有一筆買賣外幣/約定轉帳 交易待確認，"
    				+ " 幣別"
    				+ ((Map<String, String>) bs.getData()).get("str_Curr")
    				+ "，金額"
    				+ ((Map<String, String>) bs.getData()).get("CURAMT");
			if (IdgateData != null) {
            	model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
	            if(IdgateData.get("idgateUserFlag").equals("Y")) {		 
	            	
	            	Map<String, String> idgateMap = new HashMap<String, String>();
	            	idgateMap.putAll(okMap);
	            	idgateMap.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
	            	idgateMap.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
	            	idgateMap.put("CUSIDN", cusidn);
	            	// 避免被格式化
	            	idgateMap.put("PAYDATE_val", okMap.get("PAYDATE")); // 日期
	            	idgateMap.put("CURAMT_val", okMap.get("CURAMT")); // 金額
	            	IdgateData.put("F001T_IDGATE", idgateservice.coverTxnData(F001T_IDGATE_DATA.class, F001T_IDGATE_DATA_VIEW.class, idgateMap));
	            	
	            	SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	            }
            }
            
            model.addAttribute("idgateAdopid", adopid);
            model.addAttribute("idgateTitle", title);

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("f_transfer_t_confirm error >> {}",e);
		}finally {
			if(bs.getResult()) {
				target = "/acct_fcy/f_transfer_t_confirm";
//			TODO 這邊還要調整 不能存物件，要存成JSNO字串
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, ((Map<String,Object>)bs.getData()).get("TXTOKEN"));
				
				// 設定回上一頁的url,配合confirm頁面的js
				model.addAttribute("previous","/FCY/TRANSFER/f_transfer_step1");
				// 回填使用DATA
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
			}else {
				// 到錯誤頁，錯誤頁的確認按鈕導頁到輸入頁
				bs.setNext("/FCY/TRANSFER/f_transfer_step1");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 外匯結購售及轉帳 即時同幣別 結果頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_transfer_t_result")
	@Dialog(ADOPID = "F001")
	public String f_transfer_t_result(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("f_transfer_t_result...");
		log.trace(ESAPIUtil.vaildLog("reqParam {}" + CodeUtil.toJson(reqParam)));

		String target = "/error";
		String next = "/FCY/TRANSFER/f_transfer";
//		String previous = "/FCY/TRANSFER/f_transfer_t_confirm";

		BaseResult bs = null;
		try {
			// 初始化BaseResult
			bs = new BaseResult();
			
			// 取得CUSIDN明碼
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 判斷LOCAL KEY是否有帶值，有帶表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			log.trace("f_transfer_t_result.locale: " + hasLocale);
			if(hasLocale){
				log.trace("f_transfer_t_result.hasLocale...");
				// 判斷從session取出的資料物件是否為BaseResult物件
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( !bs.getResult() ){
					log.trace("f_transfer_t_result.bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			// 判斷LOCAL KEY沒有帶值則驗證TXTOKEN，驗證通過才可做交易
			if(!hasLocale){
				log.trace("f_transfer_t_result.validate TXTOKEN...");
				log.trace("f_transfer_t_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("f_transfer_t_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("f_transfer_t_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("f_transfer_t_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						!okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("f_transfer_t_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("f_transfer_t_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				
				
				// 取得從輸入頁輸入存在session中的輸入資料
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String,String> transfer_data = (Map<String, String>) bs.getData();
				
				// 取得SESSION中的輸入資料，加入到確認頁的交易機制資料
				okMap.putAll(transfer_data);
				
				// 重置bs，進行交易
				bs.reset();
				
				// IDgate驗證
				try {
					if (okMap.get("FGTXWAY").equals("7")) {
						Map<String, Object> IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
						IdgateData = (Map<String, Object>) IdgateData.get("F001T_IDGATE");
						okMap.put("sessionID", (String) IdgateData.get("sessionid"));
						okMap.put("txnID", (String) IdgateData.get("txnID"));
						okMap.put("idgateID", (String) IdgateData.get("IDGATEID"));
						
						// 避免被格式化
						okMap.put("PAYDATE_val", okMap.get("PAYDATE")); // 日期
						okMap.put("CURAMT_val", okMap.get("CURAMT")); // 金額
					}
				} catch (Exception e) {
					log.error("IDGATE ValidateFail>>{}", bs.getResult());
				}

				Map<String,Object>IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
				model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
				
				bs = fcy_transfer_service.f_transfer_t_result(cusidn, okMap);
				
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			model.addAttribute("transfer_result_data", bs);
			
			// E092 是輸入資料有誤  要讓使用者可以回上一步
//			if( "E092".equals(bs.getMsgCode()) ) {
//				bs.setPrevious(previous);
//				model.addAttribute(BaseResult.ERROR, bs);
//				
//				// 回填使用DATA
//				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
//			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("f_transfer_t_result error >> {}",e);
		} finally {
//			TODO 要清除必要的SESSION
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
	//		Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 交易成功要存之前的token 在Session 以利下次比對是否重複交易
			List<Map<String, String>> dataListMap = new ArrayList<Map<String, String>>();
			dataListMap.add((Map<String, String>) bs.getData());
			log.trace("f_transfer_t_result.dataListMap: {}", dataListMap);
			SessionUtil.addAttribute(model, SessionUtil.DOWNLOAD_DATALISTMAP_DATA, dataListMap);
			
			// 交易成功要存之前的token 在Session 以利下次比對是否重複交易
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TXTOKEN"));
			if(bs.getResult()) {
				target = "/acct_fcy/f_transfer_t_result";
			}else {
				bs.setNext(next);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * 外匯結購售及轉帳 即時不同幣別 step2
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_transfer_r_step2")
	public String f_transfer_r_step2(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("f_transfer_r_step2...");
		
		String target = "/error";
		BaseResult bs = new BaseResult();
		
		log.trace(ESAPIUtil.vaildLog("f_transfer_r_step2.reqParam: {}"+CodeUtil.toJson( reqParam)));
		
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
		try {
			// 回上一頁重新賦值
			if( okMap.containsKey("previous") ){
				Map<String, String> backMap = (Map<String, String>) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				okMap = backMap;
			}
			
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if( !bs.getResult())
				{
//					TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				}else{
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			}else {
				// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
				String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
				log.trace("f_transfer_t_step2.token: {}", token);
				if(StrUtil.isNotEmpty(okMap.get("TOKEN"))  &&  okMap.get("TOKEN").equals(token)) {
					bs.setResult(Boolean.TRUE);
				}
				log.trace("bs.getResult(): {}",bs.getResult());
				if(!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				}
				
				// TOKEN驗證成功，資料處理後進入確認頁
				bs.reset();
				
				String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
				// TODO 外匯結購售及轉帳 確認頁 處理邏輯
				okMap = fcy_transfer_service.f_transfer_t_confirm_process(okMap, cusidn);
				
				okMap.put("LOGINTYPE", "NB"); // 系統別
				log.trace(ESAPIUtil.vaildLog("f_transfer_r_step2.okMap: {}"+ CodeUtil.toJson(okMap)));
				
				bs = fcy_transfer_service.f_transfer_t_confirm(okMap, cusidn);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("f_transfer_r_step2 error >> {}",e);
		}finally {
			if(bs.getResult()) {
				target = "/acct_fcy/f_transfer_r_step2";
//			TODO 這邊還要調整 不能存物件，要存成JSNO字串
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, ((Map<String,Object>)bs.getData()).get("TXTOKEN"));
				
				// 設定回上一頁的url,配合step2頁面的js
				model.addAttribute("previous","/FCY/TRANSFER/f_transfer_step1");
				// 回填使用DATA
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
			}else {
				// 到錯誤頁，錯誤頁的確認按鈕導頁到輸入頁
				bs.setNext("/FCY/TRANSFER/f_transfer_step1");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 外匯結購售及轉帳 即時不同幣別 step3
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_transfer_r_step3")
	public String f_transfer_r_step3(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("f_transfer_r_step3...");
		log.trace(ESAPIUtil.vaildLog("reqParam: {}" +CodeUtil.toJson(reqParam)));

		String target = "/error";
		BaseResult bs = null;
		
		Map<String, String> okMap = null;
		try {
			// 初始化BaseResult
			bs = new BaseResult();
			okMap = new HashMap<String, String>();
			
			// 取得CUSIDN明碼
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			
			// 解決Trust Boundary Violation
			okMap = ESAPIUtil.validStrMap(reqParam);
			//okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 判斷LOCAL KEY是否有帶值，有帶表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			log.trace("f_transfer_r_step3.locale: " + hasLocale);
			if(hasLocale){
				log.trace("f_transfer_r_step3.hasLocale...");
				// 判斷從session取出的資料物件是否為BaseResult物件
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( !bs.getResult() ){
					log.trace("f_transfer_r_step3.bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			// 判斷LOCAL KEY沒有帶值則驗證TXTOKEN，驗證通過才可做交易
			if(!hasLocale){
				log.trace("f_transfer_r_step3.validate TXTOKEN...");
				log.trace("f_transfer_r_step3.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("f_transfer_r_step3.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("f_transfer_r_step3.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("f_transfer_r_step3 TXTOKEN 不正確，TXTOKEN>>{}"+okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						!okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("f_transfer_r_step3.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("f_transfer_r_step3 TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				
				
				// 取得從輸入頁輸入存在session中的輸入資料
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String,String> transfer_data = (Map<String, String>) bs.getData();
				
				// 取得SESSION中的輸入資料，加入到確認頁的交易機制資料
				okMap.putAll(transfer_data);
				
				// 重置bs，進行交易
				bs.reset();
				bs = fcy_transfer_service.f_transfer_r_step3(cusidn, okMap);
				
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("f_transfer_r_step3 error >> {}",e);
		} finally {
			if(bs.getResult()) {
				target = "/acct_fcy/f_transfer_r_step3";
//			TODO 這邊還要調整 不能存物件，要存成JSNO字串
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, ((Map<String,Object>)bs.getData()).get("TXTOKEN"));
				
				// 設定回上一頁的url,配合step2頁面的js
//				model.addAttribute("previous","/FCY/TRANSFER/f_transfer_step1");
				// 回填使用DATA
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
			}else {
				// 到錯誤頁，錯誤頁的確認按鈕導頁到輸入頁
				bs.setNext("/FCY/TRANSFER/f_transfer_step1");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 外匯結購售及轉帳 即時不同幣別 確認頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_transfer_r_confirm")
	public String f_transfer_r_confirm(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("f_transfer_r_confirm...");
		
		String target = "/error";
		String jsondc = "";
		BaseResult sessionBS = new BaseResult();
		BaseResult bs = new BaseResult();
		
		Map<String, String> okMap = new HashMap<String, String>();
		
		try {
			// 取得從輸入頁輸入存在session中的輸入資料
			sessionBS = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
			Map<String,String> transfer_data = (Map<String, String>) sessionBS.getData();
			
			// 取得SESSION中的輸入資料，加入到確認頁的交易機制資料
			okMap.putAll(transfer_data);
			log.trace("f_transfer_r_confirm.sessionMap: {}", okMap);
			
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			// TODO 外匯結購售及轉帳 確認頁 處理邏輯
			okMap = fcy_transfer_service.f_transfer_t_confirm_process(okMap, cusidn);
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("f_transfer_r_confirm.jsondc: " + jsondc));
			
			okMap.put("jsondc", jsondc);
			okMap.put("LOGINTYPE", "NB"); // 系統別
			log.trace(ESAPIUtil.vaildLog("f_transfer_r_confirm.okMap: {}"+ CodeUtil.toJson(okMap)));
			
			
			// 回上一頁重新賦值
			if( okMap.containsKey("previous") ){
				Map<String, String> backMap = (Map<String, String>) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				okMap = backMap;
			}
			okMap = ESAPIUtil.validStrMap(okMap); 
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if( !bs.getResult())
				{
//					TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				}else{
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			}else {
				// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
				String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
				log.trace("f_transfer_t_confirm.token: {}", token);
				if(StrUtil.isNotEmpty(okMap.get("TOKEN"))  &&  okMap.get("TOKEN").equals(token)) {
					bs.setResult(Boolean.TRUE);
				}
				log.trace("bs.getResult(): {}",bs.getResult());
				if(!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				}
				
				// TOKEN驗證成功，資料處理後進入確認頁
				bs.reset();
				bs = fcy_transfer_service.f_transfer_t_confirm(okMap, cusidn);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
			}
			
			// IDGATE transdata
			Map<String, Object> IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			String adopid = "F001R";
//    		String title = "您有一筆買賣外幣/約定轉帳 交易待確認，金額"+((Map<String, String>) bs.getData()).get("CURAMT");
    		String title = "您有一筆買賣外幣/約定轉帳 交易待確認，"
    				+ " 幣別"
    				+ ((Map<String, String>) bs.getData()).get("str_Curr")
    				+ "，金額"
    				+ ((Map<String, String>) bs.getData()).get("CURAMT");
			if (IdgateData != null) {
            	model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
	            if(IdgateData.get("idgateUserFlag").equals("Y")) {		 
	            	
	            	Map<String, String> idgateMap = new HashMap<String, String>();
	            	idgateMap.putAll(okMap);
	            	idgateMap.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
	            	idgateMap.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
	            	idgateMap.put("CUSIDN", cusidn);
	            	// 避免被格式化
	            	idgateMap.put("PAYDATE_val", okMap.get("PAYDATE")); // 日期
	            	idgateMap.put("CURAMT_val", okMap.get("CURAMT")); // 金額
	            	IdgateData.put("F001R_IDGATE", idgateservice.coverTxnData(F001R_IDGATE_DATA.class, F001R_IDGATE_DATA_VIEW.class, idgateMap));
	            	
	            	SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	            }
            }
            
            model.addAttribute("idgateAdopid", adopid);
            model.addAttribute("idgateTitle", title);
            
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("f_transfer_r_confirm error >> {}",e);
		}finally {
			if(bs.getResult()) {
				target = "/acct_fcy/f_transfer_r_confirm";
//			TODO 這邊還要調整 不能存物件，要存成JSNO字串
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, ((Map<String,Object>)bs.getData()).get("TXTOKEN"));
				
				// 設定回上一頁的url,配合confirm頁面的js
//				model.addAttribute("previous","/FCY/TRANSFER/f_transfer_step1");
				// 回填使用DATA
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
			}else {
				// 到錯誤頁，錯誤頁的確認按鈕導頁到輸入頁
				bs.setNext("/FCY/TRANSFER/f_transfer_step1");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 外匯結購售及轉帳 即時不同幣別 結果頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_transfer_r_result")
	@Dialog(ADOPID = "F001")
	public String f_transfer_r_result(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("f_transfer_r_result...");
		log.trace(ESAPIUtil.vaildLog("reqParam {}" +CodeUtil.toJson(reqParam)));

		String target = "/error";
		String next = "/FCY/TRANSFER/f_transfer";
//		String previous = "/FCY/TRANSFER/f_transfer_r_confirm";

		BaseResult bs = null;
		try {
			// 初始化BaseResult
			bs = new BaseResult();
			
			// 取得CUSIDN明碼
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 判斷LOCAL KEY是否有帶值，有帶表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			log.trace("f_transfer_r_result.locale: " + hasLocale);
			if(hasLocale){
				log.trace("f_transfer_r_result.hasLocale...");
				// 判斷從session取出的資料物件是否為BaseResult物件
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( !bs.getResult() ){
					log.trace("f_transfer_r_result.bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			// 判斷LOCAL KEY沒有帶值則驗證TXTOKEN，驗證通過才可做交易
			if(!hasLocale){
				log.trace("f_transfer_r_result.validate TXTOKEN...");
				log.trace("f_transfer_r_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("f_transfer_r_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("f_transfer_r_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("f_transfer_r_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						!okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("f_transfer_r_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("f_transfer_r_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				
				
				// 取得從輸入頁輸入存在session中的輸入資料
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String,String> transfer_data = (Map<String, String>) bs.getData();
				
				// 取得SESSION中的輸入資料，加入到確認頁的交易機制資料
				okMap.putAll(transfer_data);
				
				// 重置bs，進行交易
				bs.reset();
				
				// IDgate驗證
				try {
					if (okMap.get("FGTXWAY").equals("7")) {
						Map<String, Object> IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
						IdgateData = (Map<String, Object>) IdgateData.get("F001R_IDGATE");
						okMap.put("sessionID", (String) IdgateData.get("sessionid"));
						okMap.put("txnID", (String) IdgateData.get("txnID"));
						okMap.put("idgateID", (String) IdgateData.get("IDGATEID"));
						
						// 避免被格式化
						okMap.put("PAYDATE_val", okMap.get("PAYDATE")); // 日期
						okMap.put("CURAMT_val", okMap.get("CURAMT")); // 金額
					}
				} catch (Exception e) {
					log.error("IDGATE ValidateFail>>{}", bs.getResult());
				}
				Map<String,Object>IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
				model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
				
				bs = fcy_transfer_service.f_transfer_r_result(cusidn, okMap);
				
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			model.addAttribute("transfer_result_data", bs);
			
			// E092 是輸入資料有誤  要讓使用者可以回上一步
//			if( "E092".equals(bs.getMsgCode()) ) {
//				bs.setPrevious(previous);
//				model.addAttribute(BaseResult.ERROR, bs);
//				
//				// 回填使用DATA
//				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
//			}
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("f_transfer_r_result error >> {}",e);
		} finally {
//			TODO 要清除必要的SESSION
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			//Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 交易成功要存之前的token 在Session 以利下次比對是否重複交易
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TXTOKEN"));
			if(bs.getResult()) {
				target = "/acct_fcy/f_transfer_r_result";
			}else {
				bs.setNext(next);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	
	/**
	 * 外匯結購售及轉帳 預約 確認頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_transfer_s_confirm")
	public String f_transfer_s_confirm(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("f_transfer_s_confirm...");
		
		String target = "/error";
		String jsondc = "";
		BaseResult bs = new BaseResult();
		
		log.trace(ESAPIUtil.vaildLog("f_transfer_s_confirm.reqParam: {}"+ CodeUtil.toJson(reqParam)));
		
		// 解決Trust Boundary Violation
		Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
		
		try {
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			// TODO 外匯結購售及轉帳 確認頁 處理邏輯
			okMap = fcy_transfer_service.f_transfer_t_confirm_process(okMap, cusidn);
			
			// IKEY要使用的JSON:DC
			jsondc = URLEncoder.encode(CodeUtil.toJson(reqParam),"UTF-8");
			log.trace(ESAPIUtil.vaildLog("f_transfer_s_confirm.jsondc: " + jsondc));
			
			okMap.put("jsondc", jsondc);
			okMap.put("LOGINTYPE", "NB"); // 系統別
			log.trace(ESAPIUtil.vaildLog("f_transfer_s_confirm.okMap: {}"+ CodeUtil.toJson(okMap)));
			
			
			// 回上一頁重新賦值
			if( okMap.containsKey("previous") ){
				Map<String, String> backMap = (Map<String, String>) SessionUtil.getAttribute(model, SessionUtil.BACK_DATA, null);
				okMap = backMap;
			}
			
			// 判斷LOCAL KEY是否有帶值表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			if(hasLocale){
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, null));
				if( !bs.getResult())
				{
//					TODO 交易結束後，CONFIRM_LOCALE_DATA 會被清空，造成在URL輸入確認頁會導向錯誤頁，原因待查
					log.trace("bs.getResult(): {}", bs.getResult());
					throw new Exception();
				}else{
					// session 資料放入 map 讓後續 model 和回上一頁取值
					okMap.putAll((Map<String, String>) bs.getData());
				}
			}else {
				// 驗證TOKEN 沒TOKEN會到錯誤頁，錯誤頁確認後返回輸入頁
				String token = (String) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_CONFIRM_TOKEN, null);
				log.trace("f_transfer_t_confirm.token: {}", token);
				if(StrUtil.isNotEmpty(okMap.get("TOKEN"))  &&  okMap.get("TOKEN").equals(token)) {
					bs.setResult(Boolean.TRUE);
				}
				log.trace("bs.getResult(): {}",bs.getResult());
				if(!bs.getResult()) {
					log.trace("throw new Exception()");
					throw new Exception();
				}
				
				// TOKEN驗證成功，資料處理後進入確認頁
				bs.reset();
				bs = fcy_transfer_service.f_transfer_t_confirm(okMap, cusidn);
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.CONFIRM_LOCALE_DATA, bs);
			}
			
			// IDGATE transdata
			Map<String, Object> IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
			String adopid = "F001S";
//    		String title = "您有一筆買賣外幣/約定轉帳 交易待確認，金額"+((Map<String, String>) bs.getData()).get("CURAMT");
    		String title = "您有一筆買賣外幣/約定轉帳 交易待確認，"
    				+ " 幣別"
    				+ ((Map<String, String>) bs.getData()).get("str_Curr")
    				+ "，金額"
    				+ ((Map<String, String>) bs.getData()).get("CURAMT");
			if (IdgateData != null) {
            	model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
	            if(IdgateData.get("idgateUserFlag").equals("Y")) {		 
	            	
	            	Map<String, String> idgateMap = new HashMap<String, String>();
	            	idgateMap.putAll(okMap);
	            	idgateMap.put("IDGATEID", (String)IdgateData.get("IDGATEID"));
	            	idgateMap.put("DEVICEID", (String)IdgateData.get("DEVICEID"));
	            	idgateMap.put("CUSIDN", cusidn);
	            	IdgateData.put("F001S_IDGATE", idgateservice.coverTxnData(F001S_IDGATE_DATA.class, F001S_IDGATE_DATA_VIEW.class, idgateMap));
	            	
	            	SessionUtil.addAttribute(model, SessionUtil.IDGATE_TRANSDATA, IdgateData);		 
	            }
            }
            
            model.addAttribute("idgateAdopid", adopid);
            model.addAttribute("idgateTitle", title);

		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("f_transfer_s_confirm error >> {}",e);
		}finally {
			if(bs.getResult()) {
				target = "/acct_fcy/f_transfer_s_confirm";
//			TODO 這邊還要調整 不能存物件，要存成JSNO字串
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_DATA, bs);
				SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, ((Map<String,Object>)bs.getData()).get("TXTOKEN"));
				
				// 設定回上一頁的url,配合confirm頁面的js
				model.addAttribute("previous","/FCY/TRANSFER/f_transfer_step1");
				// 回填使用DATA
				SessionUtil.addAttribute(model, SessionUtil.BACK_DATA, okMap);
			}else {
				// 到錯誤頁，錯誤頁的確認按鈕導頁到輸入頁
				bs.setNext("/FCY/TRANSFER/f_transfer_step1");
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
	/**
	 * 外匯結購售及轉帳 預約 結果頁
	 * @param request
	 * @param response
	 * @param reqParam
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/f_transfer_s_result")
	public String f_transfer_s_result(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, String> reqParam, Model model) {
		log.trace("f_transfer_s_result...");
		log.trace(ESAPIUtil.vaildLog("reqParam {}" + CodeUtil.toJson(reqParam)));

		String target = "/error";
		String next = "/FCY/TRANSFER/f_transfer";

		BaseResult bs = null;
		try {
			// 初始化BaseResult
			bs = new BaseResult();
			
			// 取得CUSIDN明碼
			String cusidn = new String(Base64.getDecoder().decode((String) SessionUtil.getAttribute(model, SessionUtil.CUSIDN, null)), "utf-8");
			
			// 解決Trust Boundary Violation
			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
//			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 判斷LOCAL KEY是否有帶值，有帶表示按下I18N，抓取之前的資料顯示
			boolean hasLocale = okMap.containsKey("locale");
			log.trace("f_transfer_s_result.locale: " + hasLocale);
			if(hasLocale){
				log.trace("f_transfer_s_result.hasLocale...");
				// 判斷從session取出的資料物件是否為BaseResult物件
				bs = WebUtil.toBs(SessionUtil.getAttribute(model, SessionUtil.RESULT_LOCALE_DATA, null));
				if( !bs.getResult() ){
					log.trace("f_transfer_s_result.bs.getResult() >>{}",bs.getResult());
					throw new Exception();
				}
			}
			// 判斷LOCAL KEY沒有帶值則驗證TXTOKEN，驗證通過才可做交易
			if(!hasLocale){
				log.trace("f_transfer_s_result.validate TXTOKEN...");
				log.trace("f_transfer_s_result.TRANSFER_RESULT_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null));
				log.trace("f_transfer_s_result.TRANSFER_RESULT_FINSH_TOKEN: " + SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null));
				
				// 1. 驗證token是否與確認頁的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_TOKEN, null)) ) {
					// TXTOKEN第一階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("f_transfer_s_result.bs.step1 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("f_transfer_s_result TXTOKEN 不正確，TXTOKEN>>{}"+ okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，繼續往下驗證
				bs.reset();
				
				// 2. 驗證token是否與結果頁完成交易後的TOKEN重複，result token 如果存在且與SESSION中的相同表示F5 或i18n
				if(StrUtil.isNotEmpty(okMap.get("TXTOKEN")) && 
						!okMap.get("TXTOKEN").equals(SessionUtil.getAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, null)) ) {
					// TXTOKEN第二階段驗證成功
					bs.setResult(Boolean.TRUE);
					log.trace("f_transfer_s_result.bs.step2 is successful...");
				}
				if(!bs.getResult()) {
					log.error(ESAPIUtil.vaildLog("f_transfer_s_result TXTOKEN 不正確，或重複交易，TXTOKEN>>{}"+okMap.get("TXTOKEN")));
					throw new Exception();
				}
				// 重置bs，準備進行交易
				bs.reset();
				
				
				// 取得從輸入頁輸入存在session中的輸入資料
				bs = (BaseResult) SessionUtil.getAttribute(model, SessionUtil.TRANSFER_DATA, null);
				Map<String,String> transfer_data = (Map<String, String>) bs.getData();
				
				// 取得SESSION中的輸入資料，加入到確認頁的交易機制資料
				okMap.putAll(transfer_data);
				
				// 重置bs，進行交易
				bs.reset();
				
				// IDgate驗證
				try {
					if (okMap.get("FGTXWAY").equals("7")) {
						Map<String, Object> IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
						IdgateData = (Map<String, Object>) IdgateData.get("F001S_IDGATE");
						okMap.put("sessionID", (String) IdgateData.get("sessionid"));
						okMap.put("txnID", (String) IdgateData.get("txnID"));
						okMap.put("idgateID", (String) IdgateData.get("IDGATEID"));
					}
				} catch (Exception e) {
					log.error("IDGATE ValidateFail>>{}", bs.getResult());
				}

				Map<String,Object>IdgateData = (Map<String, Object>) SessionUtil.getAttribute(model, SessionUtil.IDGATE_TRANSDATA, null);
				model.addAttribute("idgateUserFlag", IdgateData.get("idgateUserFlag"));
				
				bs = fcy_transfer_service.f_transfer_s_result(cusidn, okMap);
				
				// 將查過的資料放入session，待切換 locale 時讀取
				SessionUtil.addAttribute(model, SessionUtil.RESULT_LOCALE_DATA, bs);
			}
			model.addAttribute("transfer_result_data", bs);
			
		} catch (Exception e) {
			//Avoid Information Exposure Through an Error Message
			//e.printStackTrace();
			log.error("f_transfer_s_result error >> {}",e);
		} finally {
//			TODO 要清除必要的SESSION
			
			// 解決Trust Boundary Violation
//			Map<String, String> okMap = ESAPIUtil.validStrMap(reqParam);
			Map<String, String> okMap = reqParam;// jsondc會被ESAPI拿掉，故先不做ESAPI
			
			// 交易成功要存之前的token 在Session 以利下次比對是否重複交易
			SessionUtil.addAttribute(model, SessionUtil.TRANSFER_RESULT_FINSH_TOKEN, okMap.get("TXTOKEN"));
			if(bs.getResult()) {
				target = "/acct_fcy/f_transfer_s_result";
			}else {
				bs.setNext(next);
				model.addAttribute(BaseResult.ERROR, bs);
			}
		}
		return target;
	}
	
}
