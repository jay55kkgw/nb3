package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class GD011_REST_RS extends BaseRestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7431159079294072780L;

	String PERIODFROM;
	String PERIODTO;
	String S_MAX;
	String B_MAX;
	String S_MIN;
	String B_MIN;
    LinkedList<GD011_REST_RSDATA> REC;
    
	public String getPERIODFROM() {
		return PERIODFROM;
	}
	public void setPERIODFROM(String pERIODFROM) {
		PERIODFROM = pERIODFROM;
	}
	public String getPERIODTO() {
		return PERIODTO;
	}
	public void setPERIODTO(String pERIODTO) {
		PERIODTO = pERIODTO;
	}
	public String getS_MAX() {
		return S_MAX;
	}
	public void setS_MAX(String s_MAX) {
		S_MAX = s_MAX;
	}
	public String getB_MAX() {
		return B_MAX;
	}
	public void setB_MAX(String b_MAX) {
		B_MAX = b_MAX;
	}
	public String getS_MIN() {
		return S_MIN;
	}
	public void setS_MIN(String s_MIN) {
		S_MIN = s_MIN;
	}
	public String getB_MIN() {
		return B_MIN;
	}
	public void setB_MIN(String b_MIN) {
		B_MIN = b_MIN;
	}
	public LinkedList<GD011_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<GD011_REST_RSDATA> rEC) {
		REC = rEC;
	}
}
