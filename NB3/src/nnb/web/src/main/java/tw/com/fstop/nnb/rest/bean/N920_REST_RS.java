package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N920_REST_RS extends BaseRestBean implements Serializable  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -730128893542549966L;

	private String VIPNUMF;		// 外幣匯款人VIP註記

    private String SECCOD;		// 交易安按模式

    private String HEADER;		// HEADER

    private String MSGCOD;		// 回應代碼

    private String NAME;		// 姓名

    private String MMACOD;		// 回應代碼

    private String OFFSET;		// 空白

    private String __OCCURS;

    private String COUNT;		// 筆數

    private String CUSTIDF;		// 外幣匯款人身份別

    private String SEQ;			// 序號 (01-99)
    
    private String CUSNAME;		// 原住民姓名
    
	//電文內陣列
	private LinkedList<N920_REST_RSDATA> REC;

	public String getVIPNUMF() {
		return VIPNUMF;
	}

	public void setVIPNUMF(String vIPNUMF) {
		VIPNUMF = vIPNUMF;
	}

	public String getSECCOD() {
		return SECCOD;
	}

	public void setSECCOD(String sECCOD) {
		SECCOD = sECCOD;
	}

	public String getHEADER() {
		return HEADER;
	}

	public void setHEADER(String hEADER) {
		HEADER = hEADER;
	}

	public String getMSGCOD() {
		return MSGCOD;
	}

	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}

	public String getNAME() {
		return NAME;
	}

	public void setNAME(String nAME) {
		NAME = nAME;
	}

	public String getMMACOD() {
		return MMACOD;
	}

	public void setMMACOD(String mMACOD) {
		MMACOD = mMACOD;
	}

	public String getOFFSET() {
		return OFFSET;
	}

	public void setOFFSET(String oFFSET) {
		OFFSET = oFFSET;
	}

	public String get__OCCURS() {
		return __OCCURS;
	}

	public void set__OCCURS(String __OCCURS) {
		this.__OCCURS = __OCCURS;
	}

	public String getCOUNT() {
		return COUNT;
	}

	public void setCOUNT(String cOUNT) {
		COUNT = cOUNT;
	}

	public String getCUSTIDF() {
		return CUSTIDF;
	}

	public void setCUSTIDF(String cUSTIDF) {
		CUSTIDF = cUSTIDF;
	}

	public String getSEQ() {
		return SEQ;
	}

	public void setSEQ(String sEQ) {
		SEQ = sEQ;
	}

	public LinkedList<N920_REST_RSDATA> getREC() {
		return REC;
	}

	public void setREC(LinkedList<N920_REST_RSDATA> rEC) {
		REC = rEC;
	}

	public String getCUSNAME() {
		return CUSNAME;
	}

	public void setCUSNAME(String cUSNAME) {
		CUSNAME = cUSNAME;
	}
	
	

}
