package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N942_REST_RS  extends BaseRestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4865190566250816233L;

	//查詢時間
    String CMQTIME;
    String MSGCOD;
    
	//rowdata用
	LinkedList<N942_REST_RSDATA> REC;

	public String getMSGCOD() {
		return MSGCOD;
	}


	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}
	
	public LinkedList<N942_REST_RSDATA> getREC() {
		return REC;
	}


	public void setREC(LinkedList<N942_REST_RSDATA> rEC) {
		REC = rEC;
	}
}
