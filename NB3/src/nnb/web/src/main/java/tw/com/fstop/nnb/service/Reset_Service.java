package tw.com.fstop.nnb.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.spring.helper.I18n;
import tw.com.fstop.tbb.nnb.dao.AdmMsgCodeDao;
import tw.com.fstop.tbb.nnb.dao.TxnSmsLogDao;
import tw.com.fstop.util.CodeUtil;
import tw.com.fstop.util.DateUtil;
import tw.com.fstop.util.ESAPIUtil;
import tw.com.fstop.util.EncUtil;

@Service
public class Reset_Service extends Base_Service {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Personal_Serving_Service personal_serving_service;
	@Autowired
	AdmMsgCodeDao admMsgCodeDao;
	
	@Autowired
	I18n i18n;
	
	@Autowired
	private TxnSmsLogDao txnSmsLogDao;
	/**
	 * 重設簽入、交易密碼
	 * @param sessionId
	 * @return
	 */
	public BaseResult Reset(String cusidn, Map<String, String> reqParam) {
		log.trace("Reset_service");
		BaseResult bs = null;
		BaseResult bsPin = null ; // 簽入密碼
		BaseResult bsSSL = null ; // 交易密碼
		
		try {
			bs = new BaseResult(); // 回傳結果
			bsPin = new BaseResult(); // 簽入密碼
			bsSSL = new BaseResult(); // 交易密碼
			
			try {
				
				Map<String, String> pwMap = new HashMap<String, String>(); // 簽入密碼
				Map<String, String> sslMap = new HashMap<String, String>(); // 交易密碼
				
				pwMap.put("PINOLD", reqParam.get("PINOLD")); // 舊簽入密碼
				pwMap.put("PINNEW", reqParam.get("PINNEW")); // 新簽入密碼
				sslMap.put("PINOLD", reqParam.get("SSLOLD")); // 舊交易密碼
				sslMap.put("PINNEW", reqParam.get("SSLNEW")); // 新交易密碼
				
				// 同時變更簽入、交易密碼
				bsPin = personal_serving_service.pw_alter_result(cusidn, pwMap);
				bsSSL = personal_serving_service.pw_alter_ssl_result(cusidn, sslMap);
				
				log.trace("Reset.bsPin: {}", CodeUtil.toJson(bsPin));
				log.trace("Reset.bsSSL: {}", CodeUtil.toJson(bsSSL));
				
				// 若都成功才算成功
				if( bsPin.getResult() && bsSSL.getResult() ) {
					bs.setResult(Boolean.TRUE);
					bs.setMsgCode("0");
					bs.setMessage(i18n.getMsg("LB.X1894"));//變更簽入密碼及交易密碼成功
					
				} // 簽入密碼已變更成功，需變更交易密碼
				else if ( bsPin.getResult() && !bsSSL.getResult() ){
					bs.setResult(Boolean.TRUE);
					bs.setMsgCode("E225");
					bs.setMessage(i18n.getMsg("LB.Alert135"));//簽入密碼已變更成功，需變更交易密碼
					
				} // 需變更簽入密碼
				else if ( !bsPin.getResult() && bsSSL.getResult() ){
					bs.setResult(Boolean.TRUE);
					bs.setMsgCode("E178");
					bs.setMessage(i18n.getMsg("LB.X1895"));//需變更簽入密碼
				} // 變更簽入密碼及交易密碼皆失敗
				else {
					bs.setResult(Boolean.FALSE);
					// 兩個都錯就統一回應99320
					String msgCode = "99320";
					bs.setMsgCode(msgCode);
					String message = admMsgCodeDao.errorMsg(msgCode);
					bs.setMessage(message);
				}
				
				
			} catch (Exception e) {
				log.error("Reset_Service.Reset: {}", e);
			}
			
		
		} catch (Exception e) {
			log.error("Reset_service: {}", e);
		}
	
		return  bs;
	}
	
	/**
	 * N948--客戶沿用舊密碼
	 * @return
	 */
	public BaseResult keepPW(String cusidn)	{		
		log.trace("Reset.keepPW.cusidn: " + cusidn);
		BaseResult bs = null;
		try {
			bs = new BaseResult();
			// n948客戶沿用舊密碼
			bs = N948_REST(cusidn);
		} catch (Exception e) {
			log.error("Reset_service.keepPW: {}", e);
		}
	
		return  bs;
	}	
		
	public BaseResult ssl_reset_result(Map<String, String> reqParam) {
		BaseResult bs = null ;
		try {
			bs = new BaseResult();
			bs = NA721_REST(reqParam);
		} catch (Exception e) {
			log.error("Reset_service.keepPW: {}", e);
		}
		return  bs;
	}
	
	/**
	 * 變更使用名稱/簽入密碼  結果頁
	 * @param reqParam
	 * @return
	 */
	public BaseResult user_reset_result(Map<String, String> reqParam) {
		BaseResult bs = null ;
		try {
			bs = new BaseResult();
			bs = NA72_REST(reqParam);
		} catch (Exception e) {
			log.error("Reset_service.keepPW: {}", e);
		}
		return  bs;
	}
    
	/**
	 * OTP發送簡訊驗證碼
	 * @param adopid
	 * @param cusidn
	 * @return
	 */
	public List<Map<String, String>> smsotp_ajax(String adopid, String cusidn, String msgType) {
		log.trace(ESAPIUtil.vaildLog("smsotp_ajax...>>>" + CodeUtil.toJson(cusidn)));
		
		List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
		BaseResult bs = new BaseResult();
		try {
			Map<String, String> redata = new HashMap();
			redata.put("FuncType", "0");			
			redata.put("CUSIDN", cusidn);
			redata.put("UID", cusidn);
			redata.put("OTP", "");
			//取得簡訊密碼時，不寫TXNLOG所以改SMSADOPID
			redata.put("SMSADOPID", adopid);
			redata.put("MSGTYPE", msgType);
			
			bs = SmsOtp_REST(redata);
			log.trace("Get bsdata SmsOtp_REST>>>{}",bs.getData());
			Map<String, String>datamap = new HashMap();
			if(bs!=null && bs.getResult()) {
				try {
					//取得當日已傳送次數
					Date d = new Date();
					Integer findcount = txnSmsLogDao.findTotalCountByInput(cusidn, DateUtil.format("yyyyMMdd", d),adopid);
					bs.addData("SMStimes", findcount.toString());
				}catch(Exception e) {
					log.error("Get SMSLog Failed >> {}", e);
				}
			}
			else {
				bs.addData("MSGCOD", bs.getMsgCode());
			}
			Map<String, String>listmap = (Map) bs.getData();
			resultList.add(listmap);
			log.trace("resultList>>>{}",resultList);
		} 
		catch (Exception e) {
			log.error("getGoldAcn.error: {}", e);
		}
		
		return resultList;
	}
	
	/**
	 * OTP發送簡訊驗證碼
	 * @param adopid
	 * @param cusidn
	 * @param phone
	 * @return
	 */
	public List<Map<String, String>> smsotpwithphone_ajax(String adopid, String cusidn, String phone, String msgType) {
		log.trace(ESAPIUtil.vaildLog("smsotpwithphone_ajax...>>>" + CodeUtil.toJson(cusidn)));
		log.trace(ESAPIUtil.vaildLog("smsotpwithphone_ajax phone...>>>" + CodeUtil.toJson(phone)));
		
		List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
		BaseResult bs = new BaseResult();
		try {
			Map<String, String> redata = new HashMap();
			redata.put("FuncType", "0");			
			redata.put("CUSIDN", cusidn);
			redata.put("PHONE", phone);
			redata.put("UID", cusidn);
			redata.put("OTP", "");
			//取得簡訊密碼時，不寫TXNLOG所以改SMSADOPID
			redata.put("SMSADOPID", adopid);
			redata.put("MSGTYPE", msgType);
			
			bs = SmsOtpWithphone_REST(redata);
			log.trace("Get bsdata SmsOtpWithphone_REST>>>{}",bs.getData());
			Map<String, String>datamap = new HashMap();
			if(bs!=null && bs.getResult()) {
				
			}
			else {
				bs.addData("MSGCOD", bs.getMsgCode());
			}
			Map<String, String>listmap = (Map) bs.getData();
			resultList.add(listmap);
			log.trace("resultList>>>{}",resultList);
		} 
		catch (Exception e) {
			log.error("getGoldAcn.error: {}", e);
		}
		
		return resultList;
	}		
}
