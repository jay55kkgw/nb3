package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.HashMap;

public class Sendmail_REST_RS extends BaseRestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2690948036979470472L;
	
	private String sedMailResult;

	public String getSedMailResult() {
		return sedMailResult;
	}

	public void setSedMailResult(String sedMailResult) {
		this.sedMailResult = sedMailResult;
	}
}
