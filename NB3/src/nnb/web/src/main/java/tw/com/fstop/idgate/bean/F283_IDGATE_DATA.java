package tw.com.fstop.idgate.bean;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class F283_IDGATE_DATA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2344473999658373035L;
	
	@SerializedName(value = "FXSCHID")
	private String FXSCHID;
	@SerializedName(value = "FXSCHNO")
	private String FXSCHNO;
	@SerializedName(value = "FXPERMTDATE")
	private String FXPERMTDATE;
	@SerializedName(value = "FXFDATE")
	private String FXFDATE;
	@SerializedName(value = "FXTDATE")
	private String FXTDATE;
	@SerializedName(value = "FXNEXTDATE")
	private String FXNEXTDATE;
	@SerializedName(value = "FXWDAC")
	private String FXWDAC;
	@SerializedName(value = "FXWDCURR")
	private String FXWDCURR;
	@SerializedName(value = "FXWDAMT")
	private String FXWDAMT;
	@SerializedName(value = "FXSVBH")
	private String FXSVBH;
	@SerializedName(value = "FXSVAC")
	private String FXSVAC;
	@SerializedName(value = "FXSVCURR")
	private String FXSVCURR;
	@SerializedName(value = "FXSVAMT")
	private String FXSVAMT;
	@SerializedName(value = "TXTYPE")
	private String TXTYPE;
	@SerializedName(value = "FXTXMEMO")
	private String FXTXMEMO;
	@SerializedName(value = "FXTXCODE")
	private String FXTXCODE;
	@SerializedName(value = "FGTXWAY")
	private String FGTXWAY;
	@SerializedName(value = "FXTXCODES")
	private String FXTXCODES;
	
	public String getFXSCHID() {
		return FXSCHID;
	}
	public void setFXSCHID(String fXSCHID) {
		FXSCHID = fXSCHID;
	}
	public String getFXSCHNO() {
		return FXSCHNO;
	}
	public void setFXSCHNO(String fXSCHNO) {
		FXSCHNO = fXSCHNO;
	}
	public String getFXPERMTDATE() {
		return FXPERMTDATE;
	}
	public void setFXPERMTDATE(String fXPERMTDATE) {
		FXPERMTDATE = fXPERMTDATE;
	}
	public String getFXFDATE() {
		return FXFDATE;
	}
	public void setFXFDATE(String fXFDATE) {
		FXFDATE = fXFDATE;
	}
	public String getFXTDATE() {
		return FXTDATE;
	}
	public void setFXTDATE(String fXTDATE) {
		FXTDATE = fXTDATE;
	}
	public String getFXNEXTDATE() {
		return FXNEXTDATE;
	}
	public void setFXNEXTDATE(String fXNEXTDATE) {
		FXNEXTDATE = fXNEXTDATE;
	}
	public String getFXWDAC() {
		return FXWDAC;
	}
	public void setFXWDAC(String fXWDAC) {
		FXWDAC = fXWDAC;
	}
	public String getFXWDCURR() {
		return FXWDCURR;
	}
	public void setFXWDCURR(String fXWDCURR) {
		FXWDCURR = fXWDCURR;
	}
	public String getFXWDAMT() {
		return FXWDAMT;
	}
	public void setFXWDAMT(String fXWDAMT) {
		FXWDAMT = fXWDAMT;
	}
	public String getFXSVBH() {
		return FXSVBH;
	}
	public void setFXSVBH(String fXSVBH) {
		FXSVBH = fXSVBH;
	}
	public String getFXSVAC() {
		return FXSVAC;
	}
	public void setFXSVAC(String fXSVAC) {
		FXSVAC = fXSVAC;
	}
	public String getFXSVCURR() {
		return FXSVCURR;
	}
	public void setFXSVCURR(String fXSVCURR) {
		FXSVCURR = fXSVCURR;
	}
	public String getFXSVAMT() {
		return FXSVAMT;
	}
	public void setFXSVAMT(String fXSVAMT) {
		FXSVAMT = fXSVAMT;
	}
	public String getTXTYPE() {
		return TXTYPE;
	}
	public void setTXTYPE(String tXTYPE) {
		TXTYPE = tXTYPE;
	}
	public String getFXTXMEMO() {
		return FXTXMEMO;
	}
	public void setFXTXMEMO(String fXTXMEMO) {
		FXTXMEMO = fXTXMEMO;
	}
	public String getFXTXCODE() {
		return FXTXCODE;
	}
	public void setFXTXCODE(String fXTXCODE) {
		FXTXCODE = fXTXCODE;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getFXTXCODES() {
		return FXTXCODES;
	}
	public void setFXTXCODES(String fXTXCODES) {
		FXTXCODES = fXTXCODES;
	}
}
