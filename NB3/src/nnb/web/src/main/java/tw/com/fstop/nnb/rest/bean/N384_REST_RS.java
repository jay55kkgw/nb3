package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N384_REST_RS extends BaseRestBean implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1983603912473214189L;
	
	private String SEQ;
	private String CMQTIME;
	private String USERDATA;
	private String REC_NO;
	private LinkedList<N384_REST_RSDATA> REC;
	
	
	public String getSEQ() {
		return SEQ;
	}
	public void setSEQ(String sEQ) {
		SEQ = sEQ;
	}
	public String getCMQTIME() {
		return CMQTIME;
	}
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	public String getUSERDATA() {
		return USERDATA;
	}
	public void setUSERDATA(String uSERDATA) {
		USERDATA = uSERDATA;
	}
	public String getREC_NO() {
		return REC_NO;
	}
	public void setREC_NO(String rEC_NO) {
		REC_NO = rEC_NO;
	}
	public LinkedList<N384_REST_RSDATA> getREC() {
		return REC;
	}
	public void setREC(LinkedList<N384_REST_RSDATA> rEC) {
		REC = rEC;
	}
}
