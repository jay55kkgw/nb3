package tw.com.fstop.nnb.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import fstop.orm.po.CITIES;
import tw.com.fstop.tbb.nnb.dao.CityDataDao;

@Service
public class Cities_Service extends Base_Service{
	Logger log = LoggerFactory.getLogger(this.getClass());
	

	@Autowired
	private CityDataDao cityDataDao;

	/**
	 * 取得身分證縣市
	 */
	public List<CITIES> getCities(){
		List<CITIES> cityData = cityDataDao.getCities();
		
		return cityData;
	}
}
