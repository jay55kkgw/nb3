package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class N900_REST_RQ extends BaseRestBean_OLS implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2636105955478275130L;
	
	
	private String DATE;//日期YYYMMDD
	private String TIME;//時間HHMMSS
	private String CUSIDN;//統一編號
	private String SMSA;//帳單遞送地址
	private String POSTCOD;//郵遞區號
	private String ADDRESS;//地址
	private String HOME_TEL;//住家電話
	private String OFFICE_TEL;//公司電話
	private String MOBILE_TEL;//手機號碼
	private String KIND;//交易機制
	private String BNKRA;//行庫別
	private String XMLCA;//CA 識別碼
	private String XMLCN;//憑證ＣＮ
	private String CHIP_ACN;//晶片卡主帳號
	private String ICSEQ;//晶片卡交易序號
	private String PERID;
	private String UID;
	private String ADOPID;


	//晶片金融卡
	private	String	ACNNO;
	private	String	FGTXWAY;
	private	String	iSeqNo;
	private	String	TAC;
	private	String	ISSUER;
	private	String	TRMID;
	private	String	pkcs7Sign;
	
	private	String newZIP;
	private	String newADDR;
	private	String newHOMEPHONE;
	private	String newOFFICEPHONE;
	private	String newMOBILEPHONE;
	
	private String MSGNAME;
	private String PRIACN;
	private String STAN;
	private String DATE4;
	private String CCMMDD;
	private String TERMID;
	private String NAME;
	private String DATEBRI;
	private String IDNUM;
	private String OFFPHONE;
	private String HOUPHONE;
	private String MOTMAINAME;
	private String REASON;
	private String NEWPIN;
	
	//IDGATE
	private String sessionID;
	private String idgateID;
	private String txnID;
	
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getSMSA() {
		return SMSA;
	}
	public void setSMSA(String sMSA) {
		SMSA = sMSA;
	}
	public String getPOSTCOD() {
		return POSTCOD;
	}
	public void setPOSTCOD(String pOSTCOD) {
		POSTCOD = pOSTCOD;
	}
	public String getADDRESS() {
		return ADDRESS;
	}
	public void setADDRESS(String aDDRESS) {
		ADDRESS = aDDRESS;
	}
	public String getHOME_TEL() {
		return HOME_TEL;
	}
	public void setHOME_TEL(String hOME_TEL) {
		HOME_TEL = hOME_TEL;
	}
	public String getOFFICE_TEL() {
		return OFFICE_TEL;
	}
	public void setOFFICE_TEL(String oFFICE_TEL) {
		OFFICE_TEL = oFFICE_TEL;
	}
	public String getMOBILE_TEL() {
		return MOBILE_TEL;
	}
	public void setMOBILE_TEL(String mOBILE_TEL) {
		MOBILE_TEL = mOBILE_TEL;
	}
	public String getKIND() {
		return KIND;
	}
	public void setKIND(String kIND) {
		KIND = kIND;
	}
	public String getBNKRA() {
		return BNKRA;
	}
	public void setBNKRA(String bNKRA) {
		BNKRA = bNKRA;
	}
	public String getXMLCA() {
		return XMLCA;
	}
	public void setXMLCA(String xMLCA) {
		XMLCA = xMLCA;
	}
	public String getXMLCN() {
		return XMLCN;
	}
	public void setXMLCN(String xMLCN) {
		XMLCN = xMLCN;
	}
	public String getCHIP_ACN() {
		return CHIP_ACN;
	}
	public void setCHIP_ACN(String cHIP_ACN) {
		CHIP_ACN = cHIP_ACN;
	}
	public String getICSEQ() {
		return ICSEQ;
	}
	public void setICSEQ(String iCSEQ) {
		ICSEQ = iCSEQ;
	}
	public String getPERID() {
		return PERID;
	}
	public void setPERID(String pERID) {
		PERID = pERID;
	}
	public String getACNNO() {
		return ACNNO;
	}
	public void setACNNO(String aCNNO) {
		ACNNO = aCNNO;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getiSeqNo() {
		return iSeqNo;
	}
	public void setiSeqNo(String iSeqNo) {
		this.iSeqNo = iSeqNo;
	}
	public String getTAC() {
		return TAC;
	}
	public void setTAC(String tAC) {
		TAC = tAC;
	}
	public String getISSUER() {
		return ISSUER;
	}
	public void setISSUER(String iSSUER) {
		ISSUER = iSSUER;
	}
	public String getTRMID() {
		return TRMID;
	}
	public void setTRMID(String tRMID) {
		TRMID = tRMID;
	}
	public String getPkcs7Sign() {
		return pkcs7Sign;
	}
	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}
	public String getNewZIP() {
		return newZIP;
	}
	public void setNewZIP(String newZIP) {
		this.newZIP = newZIP;
	}
	public String getNewADDR() {
		return newADDR;
	}
	public void setNewADDR(String newADDR) {
		this.newADDR = newADDR;
	}
	public String getNewHOMEPHONE() {
		return newHOMEPHONE;
	}
	public void setNewHOMEPHONE(String newHOMEPHONE) {
		this.newHOMEPHONE = newHOMEPHONE;
	}
	public String getNewOFFICEPHONE() {
		return newOFFICEPHONE;
	}
	public void setNewOFFICEPHONE(String newOFFICEPHONE) {
		this.newOFFICEPHONE = newOFFICEPHONE;
	}
	public String getNewMOBILEPHONE() {
		return newMOBILEPHONE;
	}
	public void setNewMOBILEPHONE(String newMOBILEPHONE) {
		this.newMOBILEPHONE = newMOBILEPHONE;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getMSGNAME() {
		return MSGNAME;
	}
	public void setMSGNAME(String mSGNAME) {
		MSGNAME = mSGNAME;
	}
	public String getPRIACN() {
		return PRIACN;
	}
	public void setPRIACN(String pRIACN) {
		PRIACN = pRIACN;
	}
	public String getSTAN() {
		return STAN;
	}
	public void setSTAN(String sTAN) {
		STAN = sTAN;
	}
	public String getDATE4() {
		return DATE4;
	}
	public void setDATE4(String dATE4) {
		DATE4 = dATE4;
	}
	public String getCCMMDD() {
		return CCMMDD;
	}
	public void setCCMMDD(String cCMMDD) {
		CCMMDD = cCMMDD;
	}
	public String getTERMID() {
		return TERMID;
	}
	public void setTERMID(String tERMID) {
		TERMID = tERMID;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getDATEBRI() {
		return DATEBRI;
	}
	public void setDATEBRI(String dATEBRI) {
		DATEBRI = dATEBRI;
	}
	public String getIDNUM() {
		return IDNUM;
	}
	public void setIDNUM(String iDNUM) {
		IDNUM = iDNUM;
	}
	public String getOFFPHONE() {
		return OFFPHONE;
	}
	public void setOFFPHONE(String oFFPHONE) {
		OFFPHONE = oFFPHONE;
	}
	public String getHOUPHONE() {
		return HOUPHONE;
	}
	public void setHOUPHONE(String hOUPHONE) {
		HOUPHONE = hOUPHONE;
	}
	public String getMOTMAINAME() {
		return MOTMAINAME;
	}
	public void setMOTMAINAME(String mOTMAINAME) {
		MOTMAINAME = mOTMAINAME;
	}
	public String getREASON() {
		return REASON;
	}
	public void setREASON(String rEASON) {
		REASON = rEASON;
	}
	public String getNEWPIN() {
		return NEWPIN;
	}
	public void setNEWPIN(String nEWPIN) {
		NEWPIN = nEWPIN;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getSessionID() {
		return sessionID;
	}
	public String getIdgateID() {
		return idgateID;
	}
	public String getTxnID() {
		return txnID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}
	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}
}
