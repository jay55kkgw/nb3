package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class AML_REST_RQ extends BaseRestBean_GOLD implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7084868616232876565L;
	private String NAME;
	private String BIRTHDAY;
	private String BRHCOD;
	private String ADOPIDAML;
	private String UID;
	//20210429 修正AML缺少欄位
	private String BUSINESSUNIT;
	
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getADOPIDAML() {
		return ADOPIDAML;
	}
	public void setADOPIDAML(String aDOPIDAML) {
		ADOPIDAML = aDOPIDAML;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getBRHCOD() {
		return BRHCOD;
	}
	public void setBRHCOD(String bRHCOD) {
		BRHCOD = bRHCOD;
	}
	public String getBIRTHDAY() {
		return BIRTHDAY;
	}
	public void setBIRTHDAY(String bIRTHDAY) {
		BIRTHDAY = bIRTHDAY;
	}
	public String getBUSINESSUNIT() {
		return BUSINESSUNIT;
	}
	public void setBUSINESSUNIT(String bUSINESSUNIT) {
		BUSINESSUNIT = bUSINESSUNIT;
	}
}
