package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

/**
 * InvAttribS電文RQ
 */
public class InvAttrib_S_REST_RQ extends BaseRestBean_FUND implements Serializable{
	private static final long serialVersionUID = 6868861910004819383L;
	//共用
	private String CUSIDN;
	private String DATA;
	private String FGTXWAY;
	private String PINNEW;
	private String RTC;
	private String ADOPID;

	//N322電文會用到
	private String TYPE;
	private String UPD_FLG;//異動註記
	private String DEGREE;//學歷
	private String CAREER;//職業
	private String SALARY;//年收入
	private String EDITON;//測驗問卷版次
	private String ANSWER;//答案
	private String FDMARK1;
	private String KIND;
	private String BNKRA;//行庫別
	private String XMLCA;//CA識別碼
	private String XMLCN;//憑證CN
	private String FDQ1;
	private String FDQ2;
	private String FDQ3;
	private String FDQ4;
	private String FDQ5;
	private String FDQ6;
	private String FDQ7;
	private String FDQ8;
	private String FDQ9;
	private String FDQ10;
	private String FDQ11;
	private String FDQ12;
	private String FDQ13;
	private String FDQ14;
	private String FDQ15;
	private String FDSCORE;
	private String FDINVTYPE;
	
	private String iSeqNo;
	private String ISSUER;
	private String ACNNO;
	private String TRMID;
	private String ICSEQ;
	private String TAC;
	private String pkcs7Sign;
	private String jsondc;
	
	
	private String AGREE;
	private String ADUSERIP;
	private String PCMAC;
	private String DEVNAME;
	private String FDHISTID;
	
	//IDGATE
	private String sessionID;
	private String idgateID;
	private String txnID;
	
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getCUSIDN(){
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN){
		CUSIDN = cUSIDN;
	}
	public String getDATA(){
		return DATA;
	}
	public void setDATA(String dATA){
		DATA = dATA;
	}
	public String getFGTXWAY(){
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY){
		FGTXWAY = fGTXWAY;
	}
	public String getPINNEW(){
		return PINNEW;
	}
	public void setPINNEW(String pINNEW){
		PINNEW = pINNEW;
	}
	public String getRTC(){
		return RTC;
	}
	public void setRTC(String rTC){
		RTC = rTC;
	}
	public String getTYPE(){
		return TYPE;
	}
	public void setTYPE(String tYPE){
		TYPE = tYPE;
	}
	public String getUPD_FLG(){
		return UPD_FLG;
	}
	public void setUPD_FLG(String uPD_FLG){
		UPD_FLG = uPD_FLG;
	}
	public String getDEGREE(){
		return DEGREE;
	}
	public void setDEGREE(String dEGREE){
		DEGREE = dEGREE;
	}
	public String getCAREER(){
		return CAREER;
	}
	public void setCAREER(String cAREER){
		CAREER = cAREER;
	}
	public String getSALARY(){
		return SALARY;
	}
	public void setSALARY(String sALARY){
		SALARY = sALARY;
	}
	public String getEDITON(){
		return EDITON;
	}
	public void setEDITON(String eDITON){
		EDITON = eDITON;
	}
	public String getANSWER(){
		return ANSWER;
	}
	public void setANSWER(String aNSWER){
		ANSWER = aNSWER;
	}
	public String getFDMARK1(){
		return FDMARK1;
	}
	public void setFDMARK1(String fDMARK1){
		FDMARK1 = fDMARK1;
	}
	public String getKIND(){
		return KIND;
	}
	public void setKIND(String kIND){
		KIND = kIND;
	}
	public String getBNKRA(){
		return BNKRA;
	}
	public void setBNKRA(String bNKRA){
		BNKRA = bNKRA;
	}
	public String getXMLCA(){
		return XMLCA;
	}
	public void setXMLCA(String xMLCA){
		XMLCA = xMLCA;
	}
	public String getXMLCN(){
		return XMLCN;
	}
	public void setXMLCN(String xMLCN){
		XMLCN = xMLCN;
	}
	public String getACNNO(){
		return ACNNO;
	}
	public void setACNNO(String aCNNO){
		ACNNO = aCNNO;
	}
	public String getICSEQ(){
		return ICSEQ;
	}
	public void setICSEQ(String iCSEQ){
		ICSEQ = iCSEQ;
	}
	public String getFDQ1(){
		return FDQ1;
	}
	public void setFDQ1(String fDQ1){
		FDQ1 = fDQ1;
	}
	public String getFDQ2(){
		return FDQ2;
	}
	public void setFDQ2(String fDQ2){
		FDQ2 = fDQ2;
	}
	public String getFDQ3(){
		return FDQ3;
	}
	public void setFDQ3(String fDQ3){
		FDQ3 = fDQ3;
	}
	public String getFDQ4(){
		return FDQ4;
	}
	public void setFDQ4(String fDQ4){
		FDQ4 = fDQ4;
	}
	public String getFDQ5(){
		return FDQ5;
	}
	public void setFDQ5(String fDQ5){
		FDQ5 = fDQ5;
	}
	public String getFDQ6(){
		return FDQ6;
	}
	public void setFDQ6(String fDQ6){
		FDQ6 = fDQ6;
	}
	public String getFDQ7(){
		return FDQ7;
	}
	public void setFDQ7(String fDQ7){
		FDQ7 = fDQ7;
	}
	public String getFDQ8(){
		return FDQ8;
	}
	public void setFDQ8(String fDQ8){
		FDQ8 = fDQ8;
	}
	public String getFDQ9(){
		return FDQ9;
	}
	public void setFDQ9(String fDQ9){
		FDQ9 = fDQ9;
	}
	public String getFDQ10(){
		return FDQ10;
	}
	public void setFDQ10(String fDQ10){
		FDQ10 = fDQ10;
	}
	public String getFDQ11(){
		return FDQ11;
	}
	public void setFDQ11(String fDQ11){
		FDQ11 = fDQ11;
	}
	public String getFDQ12(){
		return FDQ12;
	}
	public void setFDQ12(String fDQ12){
		FDQ12 = fDQ12;
	}
	public String getFDQ13(){
		return FDQ13;
	}
	public void setFDQ13(String fDQ13){
		FDQ13 = fDQ13;
	}
	public String getFDQ14(){
		return FDQ14;
	}
	public void setFDQ14(String fDQ14){
		FDQ14 = fDQ14;
	}
	public String getFDQ15(){
		return FDQ15;
	}
	public void setFDQ15(String fDQ15){
		FDQ15 = fDQ15;
	}
	public String getFDSCORE(){
		return FDSCORE;
	}
	public void setFDSCORE(String fDSCORE){
		FDSCORE = fDSCORE;
	}
	public String getFDINVTYPE(){
		return FDINVTYPE;
	}
	public void setFDINVTYPE(String fDINVTYPE){
		FDINVTYPE = fDINVTYPE;
	}
	public String getiSeqNo(){
		return iSeqNo;
	}
	public void setiSeqNo(String iSeqNo){
		this.iSeqNo = iSeqNo;
	}
	public String getISSUER(){
		return ISSUER;
	}
	public void setISSUER(String iSSUER){
		ISSUER = iSSUER;
	}
	public String getTRMID(){
		return TRMID;
	}
	public void setTRMID(String tRMID){
		TRMID = tRMID;
	}
	public String getTAC(){
		return TAC;
	}
	public void setTAC(String tAC){
		TAC = tAC;
	}
	public String getPkcs7Sign(){
		return pkcs7Sign;
	}
	public void setPkcs7Sign(String pkcs7Sign){
		this.pkcs7Sign = pkcs7Sign;
	}
	public String getJsondc(){
		return jsondc;
	}
	public void setJsondc(String jsondc){
		this.jsondc = jsondc;
	}
	public String getAGREE() {
		return AGREE;
	}
	public void setAGREE(String aGREE) {
		AGREE = aGREE;
	}
	public String getADUSERIP() {
		return ADUSERIP;
	}
	public void setADUSERIP(String aDUSERIP) {
		ADUSERIP = aDUSERIP;
	}
	public String getPCMAC() {
		return PCMAC;
	}
	public void setPCMAC(String pCMAC) {
		PCMAC = pCMAC;
	}
	public String getDEVNAME() {
		return DEVNAME;
	}
	public void setDEVNAME(String dEVNAME) {
		DEVNAME = dEVNAME;
	}
	public String getFDHISTID() {
		return FDHISTID;
	}
	public void setFDHISTID(String fDHISTID) {
		FDHISTID = fDHISTID;
	}
	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public String getIdgateID() {
		return idgateID;
	}

	public void setIdgateID(String idgateID) {
		this.idgateID = idgateID;
	}

	public String getTxnID() {
		return txnID;
	}

	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}

}