package tw.com.fstop.nnb.spring.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import tw.com.fstop.common.BaseResult;
import tw.com.fstop.nnb.service.TxnCsssLogService;
import tw.com.fstop.util.ESAPIUtil;

@RestController
@RequestMapping(value = "/Dialog")
public class TxnCsssLog_Controller {

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	TxnCsssLogService txnCsssLogService;

	/*
	 * 客戶滿意度調查
	 */
	@PostMapping(value = "/add", produces = {"application/json;charset=UTF-8"})
	public @ResponseBody BaseResult add(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {		
	    Map<String,String> okMap = ESAPIUtil.validStrMap(reqParam);
		log.trace(ESAPIUtil.vaildLog("reqParam>>{}"+ okMap));
		BaseResult bs = new BaseResult();
		try {
			bs = txnCsssLogService.add(okMap,model);
			log.trace(ESAPIUtil.vaildLog("bs.getData>>{}" + bs.getData()));
		} catch (Throwable e) {
			log.error("Throwable>>{}", e.toString());
			bs.setResult(Boolean.FALSE);
			bs.setMessage("1", "查詢失敗");
		}
		return bs;
	}
}
