package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class N813_REST_RS extends BaseRestBean implements Serializable {

	
	private static final long serialVersionUID = -2984598977923484580L;
	/**
	 * 
	 */
	
	String CUSIDN;
	String RECNUM ;
	String CARDLIST ;
	
	//電文內陣列
	LinkedList<N813_REST_RSDATA> REC;

	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getRECNUM() {
		return RECNUM;
	}

	public void setRECNUM(String rECNUM) {
		RECNUM = rECNUM;
	}

	public String getCARDLIST() {
		return CARDLIST;
	}

	public void setCARDLIST(String cARDLIST) {
		CARDLIST = cARDLIST;
	}

	public LinkedList<N813_REST_RSDATA> getREC() {
		return REC;
	}

	public void setREC(LinkedList<N813_REST_RSDATA> rEC) {
		REC = rEC;
	}
	
}



	//空白
	//String OFFSET;
	//N810
	//String HEADER;
