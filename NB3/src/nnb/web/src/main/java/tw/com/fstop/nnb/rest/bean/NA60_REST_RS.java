package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;
import java.util.LinkedList;

public class NA60_REST_RS  extends BaseRestBean_OLS implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4729348849625413831L;

	private String MSGCOD;
	private String CMQTIME;
	private LinkedList<NA60_REST_RSDATA> REC;

	public String getCMQTIME() {
		return CMQTIME;
	}
	
	public void setCMQTIME(String cMQTIME) {
		CMQTIME = cMQTIME;
	}
	
	public LinkedList<NA60_REST_RSDATA> getREC() {
		return REC;
	}
	
	public void setREC(LinkedList<NA60_REST_RSDATA> rEC) {
		REC = rEC;
	}
	
	public String getMSGCOD() {
		return MSGCOD;
	}

	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}
}
