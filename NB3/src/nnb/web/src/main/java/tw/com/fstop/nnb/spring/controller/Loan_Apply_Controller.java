package tw.com.fstop.nnb.spring.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import tw.com.fstop.nnb.service.Loan_Apply_Service;
import tw.com.fstop.util.SessionUtil;

@SessionAttributes({  SessionUtil.PD, SessionUtil.PRINT_DATALISTMAP_DATA })
@Controller
@RequestMapping(value = "/LOAN/APPLY")
public class Loan_Apply_Controller {

	@Autowired
	private Loan_Apply_Service loan_apply_service;

	@RequestMapping(value = "/loan_apply")
	public String loan_apply(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, String> reqParam, Model model) {
		String target = "/loan_apply/loan_apply";
		return target;

	}

}
