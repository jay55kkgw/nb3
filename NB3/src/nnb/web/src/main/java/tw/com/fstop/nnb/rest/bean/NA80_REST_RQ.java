package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class NA80_REST_RQ extends BaseRestBean_OLS implements Serializable 
{

	private static final long serialVersionUID = 7664274433117764332L;
	
	private String CUSIDN;		// 身份證號
	private String TYPE;		// 類別
	private String ACN;			// 晶片卡主帳號
	private String ICSEQ;		// 晶片卡交易序號
	//For TXNLOG
	private String FGTXWAY = "2";
	// 電文不需要但ms_tw需要的欄位
	private String UID;
	
	
	public String getCUSIDN() {
		return CUSIDN;
	}

	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}

	public String getTYPE() {
		return TYPE;
	}

	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}

	public String getACN() {
		return ACN;
	}

	public void setACN(String aCN) {
		ACN = aCN;
	}

	public String getICSEQ() {
		return ICSEQ;
	}

	public void setICSEQ(String iCSEQ) {
		ICSEQ = iCSEQ;
	}

	public String getUID() {
		return UID;
	}

	public void setUID(String uID) {
		UID = uID;
	}

	public String getFGTXWAY() {
		return FGTXWAY;
	}

	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	
}
