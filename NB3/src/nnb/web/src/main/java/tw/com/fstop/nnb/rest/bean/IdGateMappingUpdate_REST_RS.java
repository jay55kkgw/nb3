package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class IdGateMappingUpdate_REST_RS extends BaseRestBean implements Serializable {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6818575726699594741L;
	private String MSGCOD;
	private String OPNCODE;


	public String getMSGCOD() {
		return MSGCOD;
	}


	public void setMSGCOD(String mSGCOD) {
		MSGCOD = mSGCOD;
	}


	public String getOPNCODE() {
		return OPNCODE;
	}


	public void setOPNCODE(String oPNCODE) {
		OPNCODE = oPNCODE;
	}
	
	
	

}
