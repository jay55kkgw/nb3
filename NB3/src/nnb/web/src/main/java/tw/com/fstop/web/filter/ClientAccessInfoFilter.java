
/*
 * Copyright (c) 2017, FSTOP, Inc. All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tw.com.fstop.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tw.com.fstop.web.ClientInfoProcessor;
import tw.com.fstop.web.FilterProcessor;
import tw.com.fstop.web.ProcessorException;

/**
 * Filter to get client access informations.
 * 
 *
 * @since 1.0.1
 */
public class ClientAccessInfoFilter  implements Filter
{
    private final static Logger log = LoggerFactory.getLogger(ClientAccessInfoFilter.class);
    

    @Override
    public void destroy()
    {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException
    {

        FilterProcessor processor = new ClientInfoProcessor();        
        try
        {
            processor.process(request, response);
        }
        catch (ProcessorException e)
        {
            log.error("ClientAccessInfoFilter error=", e);
        }
        
        chain.doFilter(request, response);
    }
    
    @Override
    public void init(FilterConfig arg0) throws ServletException
    {
    }
    
    
}
