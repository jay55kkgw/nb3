package tw.com.fstop.nnb.rest.bean;

import java.io.Serializable;

public class NA70_REST_RQ extends BaseRestBean_TW implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7808324726872366419L;
	
	private String DATE;		// 日期YYYMMDD
	private String TIME;		// 時間HHMMSS
	private String CUSIDN;		// 統一編號
	private String TYPE;		// 類別
	private String KIND;		// 交易機制
	private String BNKRA;		// 行庫別
	private String XMLCA;		// CA識別碼
	private String XMLCN;		// 憑證CN
	private String CHIP_ACN;	// 晶片卡主帳號
	private String ICSEQ;		// 晶片卡交易序號
	
	// 電文不需要但ms_tw需要的欄位
	private String FGTXWAY;		// 交易機制
	private String CMDATE;		// 預約日期
	private String iSeqNo;		// iSeqNo
	private String FGTXDATE;	// 即時或預約
	private String TRANSPASSUPDATE;// 即時或預約
	private String pkcs7Sign;	// IKEY
	private String jsondc;		// IKEY

	
	// 舊BEAN.writeLog需要的欄位
	private String UID;			// 身分證字號
	private String ADOPID;		// 電文代號
	private String CMTRMEMO;	// 交易備註
	private String DPMYEMAIL;	// 通訊錄
	private String CMTRMAIL;	// 通訊錄
	private String CMMAILMEMO;	// 摘要內容
	
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getCUSIDN() {
		return CUSIDN;
	}
	public void setCUSIDN(String cUSIDN) {
		CUSIDN = cUSIDN;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getKIND() {
		return KIND;
	}
	public void setKIND(String kIND) {
		KIND = kIND;
	}
	public String getBNKRA() {
		return BNKRA;
	}
	public void setBNKRA(String bNKRA) {
		BNKRA = bNKRA;
	}
	public String getXMLCA() {
		return XMLCA;
	}
	public void setXMLCA(String xMLCA) {
		XMLCA = xMLCA;
	}
	public String getXMLCN() {
		return XMLCN;
	}
	public void setXMLCN(String xMLCN) {
		XMLCN = xMLCN;
	}
	public String getCHIP_ACN() {
		return CHIP_ACN;
	}
	public void setCHIP_ACN(String cHIP_ACN) {
		CHIP_ACN = cHIP_ACN;
	}
	public String getICSEQ() {
		return ICSEQ;
	}
	public void setICSEQ(String iCSEQ) {
		ICSEQ = iCSEQ;
	}
	public String getFGTXWAY() {
		return FGTXWAY;
	}
	public void setFGTXWAY(String fGTXWAY) {
		FGTXWAY = fGTXWAY;
	}
	public String getCMDATE() {
		return CMDATE;
	}
	public void setCMDATE(String cMDATE) {
		CMDATE = cMDATE;
	}
	public String getiSeqNo() {
		return iSeqNo;
	}
	public void setiSeqNo(String iSeqNo) {
		this.iSeqNo = iSeqNo;
	}
	public String getFGTXDATE() {
		return FGTXDATE;
	}
	public void setFGTXDATE(String fGTXDATE) {
		FGTXDATE = fGTXDATE;
	}
	public String getTRANSPASSUPDATE() {
		return TRANSPASSUPDATE;
	}
	public void setTRANSPASSUPDATE(String tRANSPASSUPDATE) {
		TRANSPASSUPDATE = tRANSPASSUPDATE;
	}
	public String getPkcs7Sign() {
		return pkcs7Sign;
	}
	public void setPkcs7Sign(String pkcs7Sign) {
		this.pkcs7Sign = pkcs7Sign;
	}
	public String getJsondc() {
		return jsondc;
	}
	public void setJsondc(String jsondc) {
		this.jsondc = jsondc;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getADOPID() {
		return ADOPID;
	}
	public void setADOPID(String aDOPID) {
		ADOPID = aDOPID;
	}
	public String getCMTRMEMO() {
		return CMTRMEMO;
	}
	public void setCMTRMEMO(String cMTRMEMO) {
		CMTRMEMO = cMTRMEMO;
	}
	public String getDPMYEMAIL() {
		return DPMYEMAIL;
	}
	public void setDPMYEMAIL(String dPMYEMAIL) {
		DPMYEMAIL = dPMYEMAIL;
	}
	public String getCMTRMAIL() {
		return CMTRMAIL;
	}
	public void setCMTRMAIL(String cMTRMAIL) {
		CMTRMAIL = cMTRMAIL;
	}
	public String getCMMAILMEMO() {
		return CMMAILMEMO;
	}
	public void setCMMAILMEMO(String cMMAILMEMO) {
		CMMAILMEMO = cMMAILMEMO;
	}

	
}
