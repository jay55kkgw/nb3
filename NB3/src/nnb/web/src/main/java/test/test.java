package test;

import java.util.UUID;

public class test {

	public static void main(String[] args) {
		test();
		
	}
	
	public static String test() {
		/**
		 * 在JAVA中生成UUID字符串的有效方法（UUID.randomUUID（）。的toString（），不帶破折號）
		 * @return
		 */
			 String uuid = UUID.randomUUID().toString();
			 System.out.print(uuid);
			 return uuid;
		
	}

}
