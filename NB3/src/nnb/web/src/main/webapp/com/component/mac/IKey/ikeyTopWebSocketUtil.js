var ikeyTopWebSocketUtil = ikeyTopWebSocketUtil || function()
{
// private:	
    var ServerNotAvailableError = "E_Send_11_OnError_1006";
    var m_BrowserIsFullScreen = false;
	var m_RpcWebSocket = null;
	var m_RpcRequestTicketId = 0;
	var m_RpcTicketIdJsonAttr = 'invokeTicketId';
	var m_RpcJsonRequestQueue = {};
	var m_RpcJsonResponseQueue = {};
	var m_RpcResponseCallback = {};
	var m_WssUrl = "";
	
// private:	
    //--------------------------------------------------------------------------
	/// Name: ValidateLegalURL_Callback_Mac
	function ValidateLegalURL_Callback_Mac(rpcStatus, rpcReturn)
	{
	    try {
            //------------------------------------------------------------------
//	        topWebSocketUtil.tryRpcStatus(rpcStatus);
	        //------------------------------------------------------------------

//			alert("!!!! ValidateLegalURL_Callback_Mac = ");
	    } catch (exception) {
	        alert("exception: " + exception);
	    }
	}

	/// Name: validateLegalURL
    function validateLegalURL()
	{
//		alert("!!! validateLegalURL !!!");
					
		var rpcParams = new Array(1);
					
		rpcParams[0] = window.location.href;
					
	    sendRpcRequest(m_RpcWebSocket,
	    				ValidateLegalURL_Callback_Mac,
					   "ValidateLegalURL", 
					   rpcParams);
	}
	
	/// Name: pauseBrowser
	function pauseBrowser(millis)
	{
		var date = Date.now();
		var curDate = null;
		do {
			curDate = Date.now();
		} while (curDate-date < millis);
	}
	
	//
	/// Name: newWebSocketClient
	//
	function newWebSocketClient()
	{
		if ( m_WssUrl == "" )
		{
			return false;
		}

		//----------------------------------------------------------------------
		if ( !("WebSocket" in window ) )
		{
			alert("This browser does not support WebSockets !!!");
			return false;
		}
						
		if ( m_RpcWebSocket == null )
		{
			m_RpcWebSocket = new WebSocket(m_WssUrl);
										 			
			m_RpcWebSocket.onerror = function(evt)
			{ 
			    // alert("請注意：元件已關閉或尚未安裝 !!! " + evt.data);
				console.log("===> m_RpcWebSocket.onerror: " + evt.code);
			};
			
			m_RpcWebSocket.onmessage = onRpcResponse;

			m_RpcWebSocket.onclose = function(evt)
			{ 
				m_RpcWebSocket = null;
				console.log("===> [" + new Date() + "] m_RpcWebSocket.onclose : " + evt.code);
				if (evt.code == 1006)
				{
//					alert("請注意：元件已關閉或尚未安裝 !!! onClose=" + evt.code + "  [" + new Date() + "]");
				}
				setTimeout(newWebSocketClient, 30*1000);
			};

			m_RpcWebSocket.onopen = function(evt)
			{
				validateLegalURL();													 
			};					
		}
	}
	
	//
	/// Name: sendRpcRequest
	//
	function sendRpcRequest(aWebSocket, 
							aCallback,
							rpcName,
							params)
	{
		//--------------------------------------------------------------------------
		++m_RpcRequestTicketId;
		
		if (0 == m_RpcRequestTicketId)
		{
			m_RpcRequestTicketId = 1;
		}
		
		//--------------------------------------------------------------------------
		var requestQueueIdKey = m_RpcTicketIdJsonAttr 
							  + "_" 
							  + m_RpcRequestTicketId;
		var jsonRpcRequest;
		var jsonRequestStr;
		var jsonTmpRequestStr;
		
		jsonRpcRequest = JSON.stringify({ 'invokeTicketId': m_RpcRequestTicketId, 
										  'function': rpcName,
										  'params': params
										} );
				
		jsonRequestStr = jsonRpcRequest.replace('invokeTicketId', m_RpcTicketIdJsonAttr);
		
		// jsonRequestStr = jsonRequestStr.replace(/",/g, '\\",\\"');
		
		console.log("===> Origin jsonRequestStr: " + jsonRequestStr);
		
		// 修正不合法的 JSON 格式
		if ( params.length == 0 )
		{
			// ("[]") to ([])
			jsonRequestStr = jsonRequestStr.replace('"[]"', '[]');
			console.log("===> trace-0 jsonRequestStr: " + jsonRequestStr);
		}
//		else
		{	
		    if ( jsonRequestStr.indexOf('\"\"') !== -1)
			{
	            // (\"\") to ("")
	            jsonRequestStr = jsonRequestStr.replace(/\"\"/g, '""');
				console.log("===> trace-1 jsonRequestStr: " + jsonRequestStr);
			}
			
			if ( jsonRequestStr.indexOf('"[\\"') !== -1)
			{
				// ("[\") to ([")
				jsonRequestStr = jsonRequestStr.replace('"[\\"', '["');
				console.log("===> trace-2 jsonRequestStr: " + jsonRequestStr);
			}
			
			if ( jsonRequestStr.indexOf('\\"]"') !== -1)
			{
				// (\"]") to ("])
				jsonRequestStr = jsonRequestStr.replace('\\"]"', '"]');
				console.log("===> trace-3 jsonRequestStr: " + jsonRequestStr);
			}
			
		    if ( jsonRequestStr.indexOf(':"[') !== -1)
			{
	            // (:"[) to (:[)
	            jsonRequestStr = jsonRequestStr.replace(':"[', ':[');
				console.log("===> trace-4 jsonRequestStr: " + jsonRequestStr);
			}

		    if ( jsonRequestStr.indexOf(']"}') !== -1)
			{
	            // (]"}) to (]})
	            jsonRequestStr = jsonRequestStr.replace(']"}', ']}');
				console.log("===> trace-5 jsonRequestStr: " + jsonRequestStr);
			}

			if ( jsonRequestStr.indexOf('\\",\\"') !== -1)
			{
				// (\",\") to (",")
				// jsonRequestStr = jsonRequestStr.replace('\\",\\"', '","');
				jsonRequestStr = jsonRequestStr.replace(/\\",\\"/g, '","');
				console.log("===> trace-6 jsonRequestStr: " + jsonRequestStr);
			}

			if ( jsonRequestStr.indexOf('\\", \\"') !== -1)
			{
				// (\", \") to (", ")
				// jsonRequestStr = jsonRequestStr.replace('\\", \\"', '", "');
				jsonRequestStr = jsonRequestStr.replace(/\\", \\"/g, '", "');
				console.log("===> trace-7 jsonRequestStr: " + jsonRequestStr);
			}

			if ( jsonRequestStr.indexOf('\\",') !== -1)
			{
				// (\",) to	(",)
				// jsonRequestStr = jsonRequestStr.replace('\\",', '",');
				jsonRequestStr = jsonRequestStr.replace(/\\",/g, '",');
				console.log("===> trace-8 jsonRequestStr: " + jsonRequestStr);
			}

			if ( jsonRequestStr.indexOf('\\", ') !== -1)
			{
				// (\", ) 	(", )
				// jsonRequestStr = jsonRequestStr.replace('\\", ', '", ');
				jsonRequestStr = jsonRequestStr.replace(/\\", /g, '", ');
				console.log("===> trace-9 jsonRequestStr: " + jsonRequestStr);
			}

			if ( jsonRequestStr.indexOf(',\\"') !== -1)
			{
				// (,\") to	(,")
				// jsonRequestStr = jsonRequestStr.replace(',\\"', ',"');
				jsonRequestStr = jsonRequestStr.replace(/,\\"/g, ',"');
				console.log("===> trace-10 jsonRequestStr: " + jsonRequestStr);
			}

			if ( jsonRequestStr.indexOf(', \\"') !== -1)
			{
				// (, \") to (, ")
				// jsonRequestStr = jsonRequestStr.replace(', \\"', ', "');
				jsonRequestStr = jsonRequestStr.replace(/, \\"/g, ', "');
				console.log("===> trace-11 jsonRequestStr: " + jsonRequestStr);
			}

		    if ( jsonRequestStr.indexOf('\\"') !== -1)
			{
	            // (\") to (")
	            jsonRequestStr = jsonRequestStr.replace(/\\"/g, '"');
				console.log("===> trace-12 jsonRequestStr: " + jsonRequestStr);
			}
		}
		
//		alert("in jsonRequestStr: " + jsonRequestStr);
		console.log("===> Last jsonRequestStr: " + jsonRequestStr);
		
		m_RpcJsonRequestQueue[requestQueueIdKey] = jsonRequestStr;
		m_RpcResponseCallback[requestQueueIdKey] = aCallback;

		try
		{
			aWebSocket.send(jsonRequestStr);					
			console.log( "[" + new Date() + "] Sent");
			return requestQueueIdKey;
		}
		catch(e)	
		{
			var rpcStatus = 0;
			var rpcReturn = ServerNotAvailableError;
			
			console.log('===> sendRpcRequest failed !!! sendRpcRequest=' + e.code);
			console.log('===> sendRpcRequest failed !!! rpcReturn=' + ServerNotAvailableError);
			
			if ( aCallback != null )
			{
				aCallback(rpcStatus, rpcReturn);
			}

			return "";
		}
	}

	//
	/// Name: onRpcResponse
	//
	function onRpcResponse(e) 
	{
		var jsonRpcResponse;
		
		try
		{
			jsonRpcResponse = JSON.parse(e.data);
		}
		catch(e)
		{
			console.log('socket parse error: ' + e.data);
		}
		
		//--------------------------------------------------------------------------
		var rpcResponseTickIdKey = m_RpcTicketIdJsonAttr;
		var rpcResponseTicketId = jsonRpcResponse[rpcResponseTickIdKey];
		var requestQueueIdKey = m_RpcTicketIdJsonAttr 
							  + "_"
							  + rpcResponseTicketId;
		var jsonRpcRequest = m_RpcJsonRequestQueue[requestQueueIdKey];
		
		if ( typeof(rpcResponseTicketId) != undefined                                                                                        
		   && ( typeof(jsonRpcRequest) == 'string'
			   || typeof(jsonRpcRequest) == 'object') )
		{		
			m_RpcJsonResponseQueue[requestQueueIdKey] = true;
	
	        var rpcCallback = m_RpcResponseCallback[requestQueueIdKey];
			
			if ( rpcCallback != null )
			{
				rpcCallback(jsonRpcResponse["status"], jsonRpcResponse["return"]);
			}

			return;
		}
		else
		{
			console.log("onRpcResponse: other");
	//        socketRecieveData(e.data);
		}
	}

// public:	
    return {	
		//
		/// Name: isRpcDispatcherAvailable
		//
		isRpcDispatcherAvailable: function()
		{
			return m_RpcWebSocket != null ? true
			                              : false;
										  
			if ( m_WssUrl == "" )
			{
				m_WssUrl = aWssUrl;
				
				newWebSocketClient();
			}
		},
		
		//
		/// Name: setWssUrl
		//
		setWssUrl: function(aWssUrl)
		{
			if ( m_WssUrl == "" )
			{
				m_WssUrl = aWssUrl;
				
				newWebSocketClient();
			}
		},

		//
		/// Name: checkinBrowserIsFullScreen
		//
		checkinBrowserIsFullScreen: function(aIsFullScreen)
		{
			m_BrowserIsFullScreen = aIsFullScreen;
		},
		
		//
		/// Name: checkoutBrowserIsFullScreen
		//
		checkoutBrowserIsFullScreen: function()
		{
			return m_BrowserIsFullScreen;
		},

		//
		/// Name: tryRpcStatus
		//
		tryRpcStatus: function(aRpcStatus)
		{
			if ( aRpcStatus != 0 )
			{
				throw new Error("RpcStatus: " + aRpcStatus);
			}
			
			return aRpcStatus;
		},

		//
		/// Name: invokeRpcDispatcher
		//
		invokeRpcDispatcher: function (aCallback, aRpcName, aRpcParams)
		{
			var rpcParamsFromArgumentsIndex = 2;
			var rpcParams = new Array(arguments.length
									- rpcParamsFromArgumentsIndex);
				
			for ( var i = 0 ; i < rpcParams.length; ++i)
			{
				rpcParams[i] = arguments[i
										 + rpcParamsFromArgumentsIndex];
			}
				
			//------------------------------------------------------------------
			var requestQueueIdKey = "";
			
			try
			{
				//--------------------------------------------------------------
				// 底下如此做是防止 WebSocket Server 重開啟
				newWebSocketClient();
				
				m_RpcWebSocket.onopen = function()
				{			
                    validateLegalURL();
													 
				    requestQueueIdKey = sendRpcRequest(m_RpcWebSocket, 
													   aCallback,
													   aRpcName,
													   rpcParams);

				};					

				//--------------------------------------------------------------
				if ( null != m_RpcWebSocket )
				{
					requestQueueIdKey = sendRpcRequest(m_RpcWebSocket, 
													   aCallback,
													   aRpcName,
													   rpcParams);
					return requestQueueIdKey;
				}
										
				return requestQueueIdKey;
			}
			catch(exception)
			{
				console.log("invokeRpcDispatcher::exception: " + exception);
			}
			finally
			{

			}
		}		
    }
}();