var topWebSocketUtil = topWebSocketUtil || function()
{
// private:	
    var m_VersionText = "2018/04/19 Ver.5.0";
    var ServerNotAvailableError = "E_Send_11_OnError_1006";
    var m_BrowserIsFullScreen = false;
	var m_RpcWebSocket = null;
	var m_IsWebsocketAvailable = false;
	var m_AlreadyCallValidateLegalURL = false;
	var m_RpcRequestTicketId = 0;
	var m_RpcTicketIdJsonAttr = 'invokeTicketId';
	var m_RpcJsonRequestQueue = {};
	var m_RpcJsonResponseQueue = {};
	var m_RpcResponseCallback = {};
	var m_WssUrl = "";
	
// private:	
    //--------------------------------------------------------------------------
	/// Name: ValidateLegalURL_Callback_internal
	function ValidateLegalURL_Callback_internal(rpcStatus, rpcReturn)
	{
	    try {
            //------------------------------------------------------------------
	        topWebSocketUtil.tryRpcStatus(rpcStatus);
	        //------------------------------------------------------------------

//			alert("!!!! ValidateLegalURL_Callback_internal = ");
	    } catch (exception) {
	        alert("exception: " + exception);
	    }
	}

	/// Name: validateLegalURL
    function validateLegalURL()
	{
		// 為了 Client 端有狀況，故 'ValidateLegalURL' 不用 call back。
		var ValidateLegalURL_Callback_Win = null;						
		var rpcParams = new Array(1);
					
		rpcParams[0] = window.location.href;
					
	    sendRpcRequest(m_RpcWebSocket,
	    				ValidateLegalURL_Callback_Win,
					   "ValidateLegalURL", 
					   rpcParams);
	}
	
	/// Name: validateLegalURL
    function validateLegalURL(aCallback)
	{
		// 由 AP 提供 call back 
		var ValidateLegalURL_Callback_Win = aCallback;						
		var rpcParams = new Array(1);
					
		rpcParams[0] = window.location.href;
					
	    sendRpcRequest(m_RpcWebSocket,
	    				ValidateLegalURL_Callback_Win,
					   "ValidateLegalURL", 
					   rpcParams);
	}

	//
	/// Name: newWebSocketClient
	//
	function newWebSocketClient(aCallback)
	{
		if ( m_WssUrl == "" )
		{
			return false;
		}

		//----------------------------------------------------------------------
		if ( !("WebSocket" in window ) )
		{
			alert("This browser does not support WebSockets !!!");
			return false;
		}
								
		if ( m_RpcWebSocket == null )
		{
			//------------------------------------------------------------------			
			console.log("===> [" + new Date() + "] topWebSocketUtil.js Version: " + m_VersionText);
			
			m_RpcWebSocket = new WebSocket(m_WssUrl);
										 			
			m_RpcWebSocket.onmessage = onRpcResponse;

			// set onerror callback
			m_RpcWebSocket.onerror = function(evt)
			{ 
			    // alert("請注意：元件已關閉或尚未安裝 !!! " + evt.data);
				console.log("===> m_RpcWebSocket.onerror: " + evt.code);
				console.log("===> m_IsWebsocketAvailable: " + m_IsWebsocketAvailable);
			};
			
			// set onclose callback
			m_RpcWebSocket.onclose = function(evt)
			{ 
				//--------------------------------------------------------------
				console.log("===> [" + new Date() + "] m_RpcWebSocket.onclose : " + evt.code);
				
				if (evt.code == 1006)
				{
//					alert("請注意：元件已關閉或尚未安裝 !!! onClose=" + evt.code + "  [" + new Date() + "]");
				}
				
				console.log("===> [" + new Date() + "] m_IsWebsocketAvailable : " + m_IsWebsocketAvailable);
				
//				if ( m_IsWebsocketAvailable )
				if ( m_AlreadyCallValidateLegalURL )
				{
					setTimeout(newWebSocketClient, 30*1000);
					m_IsWebsocketAvailable = false;
					console.log("===> [" + new Date() + "] onclose :: setTimeout(newWebSocketClient, 30*1000)");
				}
				
				//--------------------------------------------------------------				
				if ( !m_AlreadyCallValidateLegalURL )
				{
				    console.log("===> [" + new Date() + "] m_AlreadyCallValidateLegalURL : " + m_AlreadyCallValidateLegalURL);
					
					if ( aCallback != null )
					{
						aCallback("0", ServerNotAvailableError);
					}
				}

				//--------------------------------------------------------------		
				m_RpcWebSocket = null;
				m_AlreadyCallValidateLegalURL = false;				
			};

			// set onopen callback
			m_RpcWebSocket.onopen = function(evt)
			{			
				m_IsWebsocketAvailable = true;				
				m_AlreadyCallValidateLegalURL = true;
				console.log("===> [" + new Date() + "] After call validateLegalURL, m_IsWebsocketAvailable : " + m_IsWebsocketAvailable);
				console.log("===> [" + new Date() + "] After call validateLegalURL, m_AlreadyCallValidateLegalURL : " + m_AlreadyCallValidateLegalURL);
				validateLegalURL(aCallback);	
			};					
		}
	}
	
	//
	/// Name: sendRpcRequest
	//
	function sendRpcRequest(aWebSocket, 
							aCallback,
							rpcName,
							params)
	{
		//--------------------------------------------------------------------------
		++m_RpcRequestTicketId;
		
		if (0 == m_RpcRequestTicketId)
		{
			m_RpcRequestTicketId = 1;
		}
		
		//--------------------------------------------------------------------------
		var requestQueueIdKey = m_RpcTicketIdJsonAttr 
							  + "_" 
							  + m_RpcRequestTicketId;
		var jsonRpcRequest;
		var jsonRequestStr;
		var jsonTmpRequestStr;
		
		jsonRpcRequest = JSON.stringify({ 'invokeTicketId': m_RpcRequestTicketId, 
										  'function': rpcName,
										  'params': params
										} );
				
		jsonRequestStr = jsonRpcRequest.replace('invokeTicketId', m_RpcTicketIdJsonAttr);
		
		// jsonRequestStr = jsonRequestStr.replace(/",/g, '\\",\\"');
		
		console.log("===> Origin jsonRequestStr: " + jsonRequestStr);
		
		// 修正不合法的 JSON 格式
		if ( params.length == 0 )
		{
			// ("[]") to ([])
			jsonRequestStr = jsonRequestStr.replace('"[]"', '[]');
			console.log("===> trace-0 jsonRequestStr: " + jsonRequestStr);
		}
//		else
		{	
		    if ( jsonRequestStr.indexOf('\"\"') !== -1)
			{
	            // (\"\") to ("")
	            jsonRequestStr = jsonRequestStr.replace(/\"\"/g, '""');
				console.log("===> trace-1 jsonRequestStr: " + jsonRequestStr);
			}
			
			if ( jsonRequestStr.indexOf('"[\\"') !== -1)
			{
				// ("[\") to ([")
				jsonRequestStr = jsonRequestStr.replace('"[\\"', '["');
				console.log("===> trace-2 jsonRequestStr: " + jsonRequestStr);
			}
			
			if ( jsonRequestStr.indexOf('\\"]"') !== -1)
			{
				// (\"]") to ("])
				jsonRequestStr = jsonRequestStr.replace('\\"]"', '"]');
				console.log("===> trace-3 jsonRequestStr: " + jsonRequestStr);
			}
			
		    if ( jsonRequestStr.indexOf(':"[') !== -1)
			{
	            // (:"[) to (:[)
	            jsonRequestStr = jsonRequestStr.replace(':"[', ':[');
				console.log("===> trace-4 jsonRequestStr: " + jsonRequestStr);
			}

		    if ( jsonRequestStr.indexOf(']"}') !== -1)
			{
	            // (]"}) to (]})
	            jsonRequestStr = jsonRequestStr.replace(']"}', ']}');
				console.log("===> trace-5 jsonRequestStr: " + jsonRequestStr);
			}

			if ( jsonRequestStr.indexOf('\\",\\"') !== -1)
			{
				// (\",\") to (",")
				// jsonRequestStr = jsonRequestStr.replace('\\",\\"', '","');
				jsonRequestStr = jsonRequestStr.replace(/\\",\\"/g, '","');
				console.log("===> trace-6 jsonRequestStr: " + jsonRequestStr);
			}

			if ( jsonRequestStr.indexOf('\\", \\"') !== -1)
			{
				// (\", \") to (", ")
				// jsonRequestStr = jsonRequestStr.replace('\\", \\"', '", "');
				jsonRequestStr = jsonRequestStr.replace(/\\", \\"/g, '", "');
				console.log("===> trace-7 jsonRequestStr: " + jsonRequestStr);
			}

			if ( jsonRequestStr.indexOf('\\",') !== -1)
			{
				// (\",) to	(",)
				// jsonRequestStr = jsonRequestStr.replace('\\",', '",');
				jsonRequestStr = jsonRequestStr.replace(/\\",/g, '",');
				console.log("===> trace-8 jsonRequestStr: " + jsonRequestStr);
			}

			if ( jsonRequestStr.indexOf('\\", ') !== -1)
			{
				// (\", ) 	(", )
				// jsonRequestStr = jsonRequestStr.replace('\\", ', '", ');
				jsonRequestStr = jsonRequestStr.replace(/\\", /g, '", ');
				console.log("===> trace-9 jsonRequestStr: " + jsonRequestStr);
			}

			if ( jsonRequestStr.indexOf(',\\"') !== -1)
			{
				// (,\") to	(,")
				// jsonRequestStr = jsonRequestStr.replace(',\\"', ',"');
				jsonRequestStr = jsonRequestStr.replace(/,\\"/g, ',"');
				console.log("===> trace-10 jsonRequestStr: " + jsonRequestStr);
			}

			if ( jsonRequestStr.indexOf(', \\"') !== -1)
			{
				// (, \") to (, ")
				// jsonRequestStr = jsonRequestStr.replace(', \\"', ', "');
				jsonRequestStr = jsonRequestStr.replace(/, \\"/g, ', "');
				console.log("===> trace-11 jsonRequestStr: " + jsonRequestStr);
			}

		    if ( jsonRequestStr.indexOf('\\"') !== -1)
			{
	            // (\") to (")
	            jsonRequestStr = jsonRequestStr.replace(/\\"/g, '"');
				console.log("===> trace-12 jsonRequestStr: " + jsonRequestStr);
			}
		}
		
//		alert("in jsonRequestStr: " + jsonRequestStr);
		console.log("===> Last jsonRequestStr: " + jsonRequestStr);
		
		m_RpcJsonRequestQueue[requestQueueIdKey] = jsonRequestStr;
		m_RpcResponseCallback[requestQueueIdKey] = aCallback;

		try
		{
			aWebSocket.send(jsonRequestStr);					
			console.log( "[" + new Date() + "] Sent...");
			return requestQueueIdKey;
		}
		catch(e)	
		{
			var rpcStatus = 0;
			var rpcReturn = ServerNotAvailableError;
			
			console.log('===> sendRpcRequest failed !!! sendRpcRequest=' + e.code);
			console.log('===> sendRpcRequest failed !!! rpcReturn=' + ServerNotAvailableError);
			
			if ( aCallback != null )
			{
				aCallback(rpcStatus, rpcReturn);
			}

			return "";
		}
	}

	//
	/// Name: onRpcResponse
	//
	function onRpcResponse(e) 
	{
		var jsonRpcResponse;
		
		try
		{
			jsonRpcResponse = JSON.parse(e.data);
		}
		catch(e)
		{
			console.log('socket parse error: ' + e.data);
		}
		
		//--------------------------------------------------------------------------
		var rpcResponseTickIdKey = m_RpcTicketIdJsonAttr;
		var rpcResponseTicketId = jsonRpcResponse[rpcResponseTickIdKey];
		var requestQueueIdKey = m_RpcTicketIdJsonAttr 
							  + "_"
							  + rpcResponseTicketId;
		var jsonRpcRequest = m_RpcJsonRequestQueue[requestQueueIdKey];
		
		if ( typeof(rpcResponseTicketId) != undefined                                                                                        
		   && ( typeof(jsonRpcRequest) == 'string'
			   || typeof(jsonRpcRequest) == 'object') )
		{		
			m_RpcJsonResponseQueue[requestQueueIdKey] = true;
	
	        var rpcCallback = m_RpcResponseCallback[requestQueueIdKey];
			
			if ( rpcCallback != null )
			{
				rpcCallback(jsonRpcResponse["status"], jsonRpcResponse["return"]);
			}

			return;
		}
		else
		{
			console.log("onRpcResponse: other");
	//        socketRecieveData(e.data);
		}
	}

// public:	
    return {	
		//
		/// Name: isRpcDispatcherAvailable
		//
		isRpcDispatcherAvailable: function()
		{
			return m_IsWebsocketAvailable;
										  
			if ( m_WssUrl == "" )
			{
				m_WssUrl = aWssUrl;
				
				newWebSocketClient();
			}
		},
		
		//
		/// Name: setWssUrl (Don't need call back function)
		//
		setWssUrl: function(aWssUrl)	
		{
			if ( m_WssUrl == "" )
			{
				m_WssUrl = aWssUrl;
				
				newWebSocketClient(null);
			}
		},

		//
		/// Name: setWssUrl (Need client's call back function)
		//
		setWssUrl: function(aWssUrl, aCallback)
		{
			if ( m_WssUrl == "" )
			{
				m_WssUrl = aWssUrl;
				
				newWebSocketClient(aCallback);
			}
		},

		//
		/// Name: tryRpcStatus
		//
		tryRpcStatus: function(aRpcStatus)
		{
			if ( aRpcStatus != 0 )
			{
				throw new Error("RpcStatus: " + aRpcStatus);
			}
			
			return aRpcStatus;
		},

		//
		/// Name: invokeRpcDispatcher
		//
		invokeRpcDispatcher: function (aCallback, aRpcName, aRpcParams)
		{
			var rpcParamsFromArgumentsIndex = 2;
			var rpcParams = new Array(arguments.length
									- rpcParamsFromArgumentsIndex);
				
			for ( var i = 0 ; i < rpcParams.length; ++i)
			{
				rpcParams[i] = arguments[i
										 + rpcParamsFromArgumentsIndex];
			}
				
			//------------------------------------------------------------------
			var requestQueueIdKey = "";
			
			try
			{
				//--------------------------------------------------------------
				// 底下如此做是防止 WebSocket Server 重開啟
				var isNewWebSocketClientOK = false;
								
				isNewWebSocketClientOK = newWebSocketClient();
								
				m_RpcWebSocket.onopen = function()
				{				
					console.log("===> [" + new Date() + "] !!! invokeRpcDispatcher:: reopen, Before validateLegalURL");
					
                    validateLegalURL();
													 
				    requestQueueIdKey = sendRpcRequest(m_RpcWebSocket, 
													   aCallback,
													   aRpcName,
													   rpcParams);
													   
					console.log("===> [" + new Date() + "] !!! invokeRpcDispatcher::reopen");
				};					

				//--------------------------------------------------------------
				if ( null != m_RpcWebSocket )
				{
					var requestQueueIdKey = false;
					
					console.log("===> [" + new Date() + "] !!! invokeRpcDispatcher::Before sendRpcRequest");
					
					requestQueueIdKey = sendRpcRequest(m_RpcWebSocket, 
													   aCallback,
													   aRpcName,
													   rpcParams);
					return requestQueueIdKey;
				}
										
				return requestQueueIdKey;
			}
			catch(exception)
			{
				console.log("invokeRpcDispatcher::exception: " + exception);
			}
			finally
			{

			}
		}		
    }
}();