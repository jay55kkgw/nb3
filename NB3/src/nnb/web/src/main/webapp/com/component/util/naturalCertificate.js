// 使用讀卡機驗證自然人憑證、晶片金融卡
function docheck(capUri){
	console.log("docheck...");

	// 驗證碼驗證
	var capData = $("#formId").serializeArray();
	fstop.getServerDataEx(capUri, capData, false, bodycallback);
}
// ajax回傳檢核結果，後續處理。
function bodycallback(response) {
	var fgtxway = $('input[name="FGTXWAY"]:checked').val();
	console.log("bodycallback.fgtxway: " + fgtxway);
	if (response.result) {
		// 交易機制選項
		switch(fgtxway) {
			case '4':
				// 自然人憑證
				processQuery2();

				break;
			case '2':
				// 晶片金融卡
				processQuery1();
		    	break;
			case '5':
				//跨行金融帳戶驗證
				vaInterBankAcc();
				break;
			case '6':
				//跨行金融帳戶驗證
				vaLocalBankAcc();
				break;
			default:
				unBlockUI(initBlockId);
				alert("請選擇交易機制");
		}
		changeCode();
	} else {
		unBlockUI(initBlockId);
		alert("驗證碼輸入錯誤請重新輸入!");
		changeCode();
		return false;
	}
}

//-----------------------------------------------------------------------------------------------

// 晶片金融卡
function processQuery1() {
	var main = document.getElementById("formId");
	var oCMCARD = document.getElementById("CMCARD");
	var CardReInsert = true;
	var GoQueryPass = true;
	//取得讀卡機
	listReaders();
}

//取得讀卡機
function listReaders(){
	ConnectCard("listReadersFinish");
}

//取得讀卡機結束
function listReadersFinish(result){
	//成功
	if(result != "false" && result != "E_Send_11_OnError_1006"){
		//找出有插卡的讀卡機
		findOKReader();
	}
}

//找出有插卡的讀卡機
function findOKReader(){
	FindOKReader("findOKReaderFinish");
}


var OKReaderName = "";

//找出有插卡的讀卡機結束(有找到才會到此FUNCTION)
function findOKReaderFinish(okReaderName){
	console.log("findOKReaderFinish.okReaderName: " + okReaderName);
	//ASSIGN到全域變數
	OKReaderName = okReaderName;

	//拔插卡改到輸入密碼之後
//	removeThenInsertCard();

	// 不拔插先驗證密碼，GenerateTAC前會做拔插
	getCardBankID();
}

//取得卡片銀行代碼
function getCardBankID(){
	console.log("getCardBankID...");
	GetUnitCode(OKReaderName,"getCardBankIDFinish");
}
//取得卡片銀行代碼結束
function getCardBankIDFinish(result){
	console.log("getCardBankIDFinish.result: " + result);
	//成功
	if(result != "false"){
		//還要另外判斷是否為本行卡
		//是
		if(result == "05000000"){
			var main = document.getElementById("formId");
			main.ISSUER.value = result;

			showDialog("verifyPin",OKReaderName,"verifyPinFinish");
		}
		//不是
		else{
			unBlockUI(initBlockId);
			alert(GetErrorMessage("E005"));
		}
	}
}

var pazzword = "";

//驗證卡片密碼
function verifyPin(readerName,password,outerCallBackFunction){
	//ASSIGN到全域變數
	pazzword = password;

	VerifyPin(readerName,password,outerCallBackFunction);
}

//驗證卡片密碼結束
function verifyPinFinish(result){
	//成功
	if(result == "true"){
		// 原先先拔插卡驗證完密碼要遮罩，改不遮罩拔插卡
//		CheckIdProcess();

		// 不遮罩改拔插卡
		getMainAccount();
	} else {
		unBlockUI(initBlockId);
	}
}

//CheckIdProcess = function() {
//	showTempMessage(500,"晶片金融卡身份查驗","<br><p>晶片金融卡身份查驗中，請稍候...</p><br><p>查驗期間請勿移除您的晶片金融卡，以免造成驗證錯誤。</p><br><br>","MaskArea",true);
//	//取得卡片主帳號
//	getMainAccount();
//}

//取得卡片主帳號
function getMainAccount(){
	GetMainAccount(OKReaderName,"getMainAccountFinish");
}

//取得卡片主帳號結束
function getMainAccountFinish(result){
	//成功
	if(result != "false"){
		var main = document.getElementById("formId");
		main.ACNNO.value = result;

		var cardACN = result;
		if(cardACN.length > 11){
			cardACN = cardACN.substr(cardACN.length - 11);
		}
		
		var uri = urihost+"/COMPONENT/component_enable_aj";
		var rdata = { ACN: cardACN };
		fstop.getServerDataEx(uri, rdata, true, CheckIdResult);

//		var cusidn = main.CUSIDN.value;
//
//		var x = { method: 'get', parameters: '', onComplete: CheckIdResult };
//		var myAjax = new Ajax.Request( '<%= RootPath %>/simpleform?trancode=N953&TXID=N953&ACN='+cardACN+'&UID='+cusidn, x);
	}
	//失敗
	else {
		unBlockUI(initBlockId);
		showTempMessage(500,"晶片金融卡身份查驗","","MaskArea",false);
	}
}

function CheckIdResult(data) {
	unBlockUI(initBlockId);
	console.log("data: " + data);
	if (data) {
		// login_aj回傳資料
		console.log("data.json: " + JSON.stringify(data));
		// 成功
		if("0" == data.msgCode) {
//			SubmitForm();
			removeThenInsertCard();
			
		} else if ("1" == data.msgCode) {
			unBlockUI(initBlockId);
			showTempMessage(500,"晶片金融卡身份查驗","","MaskArea",false);
			errorBlock(
					null, 
					null,
					[data.message], 
					'確定', 
					null
				);
			
		} else {
			unBlockUI(initBlockId);
			showTempMessage(500,"晶片金融卡身份查驗","","MaskArea",false);
			alert("非本人帳戶之晶片金融卡，無法進行線上開戶");
		}
	} else {
		unBlockUI(initBlockId);
		alert(data);
	}
}
//CheckIdResult = function(response) {
//	var ResultStr = response.responseText;
//	//CRC.showTempMessage(500, "晶片金融卡身份查驗", "", false);
//	showTempMessage(500,"晶片金融卡身份查驗","","MaskArea",false);
//	if(ResultStr.indexOf("GRANTED") == -1) {
//		alert("非本人帳戶之晶片金融卡，無法進行線上開戶");
//	} else {
//		SubmitForm();
//	}
//}

//拔插卡
function removeThenInsertCard(){
	console.log("removeThenInsertCard.OKReaderName: " + OKReaderName);
	RemoveThenInsertCard("MaskArea",60,OKReaderName,"removeThenInsertCardFinish");
}

//拔插卡結束(成功才會到此FUNCTION)
function removeThenInsertCardFinish(){
	initBlockUI();
	console.log("removeThenInsertCardFinish...");
	//取得卡片銀行代碼
	SubmitForm();
}

// 準備送出表單
function SubmitForm() {
	var main = document.getElementById("formId");
	FinalSendout("MaskArea",true);
	var acntype = $("#ACNTYPE").val();
	console.log("SubmitForm.acntype: " + acntype);
	if(acntype=="A2") {
		if(document.getElementById("CMCARD").checked == true) {
			//卡片押碼
			generateTAC();
		} else {
			var ACN_Str1 = main.ACNNO.value;
			if(ACN_Str1.length > 11) ACN_Str1 = ACN_Str1.substr(ACN_Str1.length - 11);

			main.CHIP_ACN.value = ACN_Str1;
			main.OUTACN.value = ACN_Str1;
			main.ACNNO.value = ACN_Str1;

//			main.setAttribute("action", '<%= RootPath %>/simpleform?trancode=NB32_1&ADOPID=NB32&JSPNAME=NB32_1.jsp');
			main.submit();
		}
	} else {
		document.getElementById("alldata").style.display = "none";
		main.CMSUBMIT.disabled=true;
//		main.setAttribute("action", '<%= RootPath %>/simpleform?trancode=NB32_1&ADOPID=NB32&JSPNAME=NB32_1.jsp');
		main.submit();
	}
}

//卡片押碼
function generateTAC(){
	var TRMID = MakeTRMID();
	var main = document.getElementById("formId");
	main.TRMID.value = TRMID;
	var transData = "2500" + TRMID + main.ACNNO.value;
	GenerateTAC(OKReaderName,transData,pazzword,"generateTACFinish");
}

//卡片押碼結束
function generateTACFinish(result){
	if(window.console){console.log("generateTACFinish.result: " + result);}
	//成功
	if(result != "false"){
		// e.x. E000,00000551,6BF84A4B9319B145A64F3866506D3313594B10D08CDEA863BFA8F9D6
		var TACData = result.split(",");

		var main = document.getElementById("formId");
		main.iSeqNo.value = TACData[1];
		main.ICSEQ.value = TACData[1];
		main.TAC.value = TACData[2];

		var ACN_Str1 = main.ACNNO.value;
		if(ACN_Str1.length > 11){
			ACN_Str1 = ACN_Str1.substr(ACN_Str1.length - 11);
		}

		main.CHIP_ACN.value = ACN_Str1;
		main.OUTACN.value = ACN_Str1;
		main.ACNNO.value = ACN_Str1;

//		main.setAttribute("action", '<%= RootPath %>/simpleform?trancode=NB32_1&ADOPID=NB32&JSPNAME=NB32_1.jsp');
		main.submit();
	}
	//失敗
	else {
		unBlockUI(initBlockId);
		FinalSendout("MaskArea",false);
	}
}


//-----------------------------------------------------------------------------------------------

// 自然人憑證
function processQuery2() {
	//Sox Add 自然人憑證元件語法 2017.06.13========================================================= Start
	var main = document.getElementById("formId");
	var npcpin = $("#NPCPIN").val();
	if(npcpin.length<6 || npcpin.length>8) {
		unBlockUI(initBlockId);
		alert("自然人憑證PIN碼長度錯誤");
		return false;
	}
	console.log("processQuery2.pass...");
	showTempMessage(500,"自然人憑證身份查驗","<br><p>自然人憑證身份查驗中，請稍候...</p><br><p>查驗期間請勿移除您的自然人憑證，以免造成驗證錯誤。</p><br><br>","validateMaskNatural",true);

	$("#AUTHCODE").val(getServerAuth());
	doquery1();
}
function doquery1() {
	var uri = urihost + "/COMPONENT/vaCheckAjax";
	var rdata = {AUTHCODE:$("#AUTHCODE").val()};
	console.log("doquery1.uri: " + uri);
	fstop.getServerDataEx(uri, rdata, true, vaCheckAjaxFinish);
}
function vaCheckAjaxFinish(data){
	console.log("data.json: " + JSON.stringify(data));
	if(data.result == true){
		var vaResult = data.data;
		$("#AUTHCODE1").val(vaResult);

		var checkauthcodertn = -1;
		setTimeout(function(){
        	checkauthcodertn = myobj.VerifyServerResponse($("#AUTHCODE1").val());

        	if(checkauthcodertn != 0){
        		unBlockUI(initBlockId);
				showTempMessage(500, "自然人憑證身份查驗", "", "validateMaskNatural", false);
				alert("雙向認證失敗");
				return false;
			}
        	console.log("vaCheckAjaxFinish.checkauthcodertn: " + checkauthcodertn);

        	var checkpinrtn = CHECKPIN($("#NPCPIN").val());
        	console.log("vaCheckAjaxFinish.checkpinrtn: " + checkpinrtn);

			if(checkpinrtn == 0){
				var getcertdatartn = SelectCert();

				if(getcertdatartn == 0){
					var CUSIDN4 = $("#CUSIDN").val().substring(6,10);//先取身分證末四碼
					var validdate = $("#CertNotAfter").val().substring(0,8);//有效日檢核到日
					var today = yyyymmdd(new Date());
					if(CUSIDN4 != $("#CUSIDN4").val()){//CUSIDN4 value為自然人憑證末四碼
						unBlockUI(initBlockId);
						showTempMessage(500, "自然人憑證身份查驗", "", "validateMaskNatural", false);
						alert("自然人憑證非此身分證字號，請確認");
						return false;
					}
					if(validdate < today){//檢查自然人憑證是否過期
						unBlockUI(initBlockId);
						showTempMessage(500, "自然人憑證身份查驗", "", "validateMaskNatural", false);
						alert("自然人憑證已過期");
						return false;
					}
					console.log("vaCheckAjaxFinish.jsondc: " + $("#jsondc").val());
					var result = myobj.SignPKCS7($("#jsondc").val());

					if(result == 0){
						$("#pkcs7Sign").val(myobj.GetPKCS7Data());

						$("#formId").submit();
					}
					else{
						unBlockUI(initBlockId);
						alert("錯誤代碼：" + myobj.getErrorCode() + "，錯誤訊息：" + myobj.getErrorMsg());
						showTempMessage(500, "自然人憑證身份查驗", "", "validateMaskNatural", false);
						alert("簽章錯誤");
						return false;
					}
				}
				else{
					unBlockUI(initBlockId);
					showTempMessage(500, "自然人憑證身份查驗", "", "validateMaskNatural", false);
					alert("取得憑證資訊錯誤");
					return false;
				}
			}
			else{
				unBlockUI(initBlockId);
				showTempMessage(500, "自然人憑證身份查驗", "", "validateMaskNatural", false);
				alert("自然人憑證登入失敗");
				return false;
			}
        	myobj.Logout();//載具登出
    	},
    		2000
    	);
	} else {
		unBlockUI(initBlockId);
		showTempMessage(500, "自然人憑證身份查驗", "", "validateMaskNatural", false);
		alert("無法驗證自然人憑證");
	}
}
function yyyymmdd(dateIn) {
	var yyyy = dateIn.getFullYear();
	var mm = dateIn.getMonth()+1; // getMonth() is zero-based
	var dd  = dateIn.getDate();
	return String(10000*yyyy + 100*mm + dd); // Leading zeros for mm and dd
}

