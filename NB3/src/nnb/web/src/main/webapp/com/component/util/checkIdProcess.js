// 交易機制使用IKEY
function useIKey(){
	console.log("IKey...");
	
	var jsondc = $("#jsondc").val();
	console.log("jsondc: " + jsondc);
	
	// IKEY驗證流程
	uiSignForPKCS7(jsondc);
}

// 交易機制使用讀卡機
function useCardReader(){
	console.log("CardReader...");
	
	var capUri = urihost + "/CAPCODE/captcha_valided_trans";
	
	// 驗證碼驗證
	var capData = $("#formId").serializeArray();
	var capResult = fstop.getServerDataEx(capUri, capData, false);
	console.log("chaCode_valid: " + JSON.stringify(capResult) );
	
	// 驗證結果
	if (capResult.result) {

		// 開始讀卡機驗證流程
		listReaders();
		// 讀卡機驗證流程失敗重新產生驗證碼
		//2019/09/23 因驗證完晶片金融卡後就會form submit 不須再度換驗證碼 故註解
		//changeCode();
		
	} else {
		// 失敗重新產生驗證碼
		alert("驗證碼有誤");
		changeCode();
	}
}

//IKEY簽章
function uiSignForPKCS7(data){
	var formId = document.getElementById("formId");
	SLBForSeconds("MaskArea",60000);
	UISignForPKCS7(data,formId.pkcs7Sign,"uiSignForPKCS7Finish");
}
//IKEY簽章結束
function uiSignForPKCS7Finish(result){
	var main = document.getElementById("formId");
	
	//成功
	if(result != "false"){
		SubmitForm();
	}
	//失敗
	else{
		alert("IKEY簽章失敗");
		ShowLoadingBoard("MaskArea",false);
		return false;
	}
}

//取得讀卡機
function listReaders(){
	var CardReInsert = true;
	var GoQueryPass = true;
	ConnectCard("listReadersFinish");
}
//取得讀卡機結束
function listReadersFinish(result){
	//成功
	if(result != "false" && result != "E_Send_11_OnError_1006"){
		//找出有插卡的讀卡機
		findOKReader();
	}
}
//找出有插卡的讀卡機
function findOKReader(){
	FindOKReader("findOKReaderFinish");
}
var OKReaderName = "";
//找出有插卡的讀卡機結束(有找到才會到此FUNCTION)
function findOKReaderFinish(okReaderName){
	//ASSIGN到全域變數
	OKReaderName = okReaderName;
	
	//拔插卡
//	removeThenInsertCard();
	
	// 不拔插先驗證密碼，GenerateTAC前會做拔插
	getCardBankID();
}
//取得卡片銀行代碼
function getCardBankID(){
	GetUnitCode(OKReaderName,"getCardBankIDFinish");
}
//取得卡片銀行代碼結束
function getCardBankIDFinish(result){
	//成功
	if(result != "false"){
		//還要另外判斷是否為本行卡
		//是
		if(result == "05000000"){
			var formId = document.getElementById("formId");
			formId.ISSUER.value = result;
			
			showDialog("verifyPin",OKReaderName,"verifyPinFinish");
		}
		//不是
		else{
			alert(GetErrorMessage("E005"));
		}
	}
}
var pazzword = "";
//驗證卡片密碼
function verifyPin(readerName,password,outerCallBackFunction){
	//ASSIGN到全域變數
	pazzword = password;
	
	VerifyPin(readerName,password,outerCallBackFunction);
}
//驗證卡片密碼結束
function verifyPinFinish(result){
	//成功
	if(result == "true"){
		// 遮罩
		checkIdBlockId = initBlockUI();
		
		//繼續做
		SubmitForm();
	}
}
function SubmitForm()
{
	var main = document.getElementById("formId");
	//CRC.finalSendout(document.getElementById('MaskArea'), true);
	FinalSendout("MaskArea",true);
	
	if(document.getElementById("CMCARD").checked == true)
	{
		/*CRC.getCardInfo1();
		
		main.ISSUER.value = CRC.ISSUER;
		main.TRMID.value = CRC.TRMID;
		main.ACNNO.value = CRC.Accounts[0];
		main.iSeqNo.value = CRC.ICSEQ;
		main.ICSEQ.value = CRC.ICSEQ;
		main.ICSEQ1.value = CRC.ICSEQ1;
		main.TAC.value = CRC.TAC;

		if (main.ICSEQ.value.length > 0 && main.TAC.value.length > 0)
		{
		}
		else
		{
			alert("產生晶片金融卡交易驗證碼失敗!!");
			CRC.finalSendout(document.getElementById('MaskArea'), false);
			
			return false;
		}*/
		
		//卡片押碼
		getMainAccount();
	}
	else{
		var ACN_Str1 = main.ACNNO.value;
		if(ACN_Str1.length > 11){
			ACN_Str1 = ACN_Str1.substr(ACN_Str1.length - 11);
		}
	
		main.CHIP_ACN.value = ACN_Str1;
		main.OUTACN.value = ACN_Str1;
		main.ACNNO.value = ACN_Str1;
		
		main.submit();	
	}
}
//取得卡片主帳號
function getMainAccount(){
	GetMainAccount(OKReaderName,"getMainAccountFinish");
}
//取得卡片主帳號結束
function getMainAccountFinish(result){
	//成功
	if(result != "false"){
		var main = document.getElementById("formId");
		main.ACNNO.value = result;
		var cardACN = result;
		if(cardACN.length > 11){
			cardACN = cardACN.substr(cardACN.length - 11);
		}
		
//		var x = { method: 'get', parameters: '', onComplete: CheckIdResult };	
//		var myAjax = new Ajax.Request( "simpleform?trancode=N953&TXID=N953&ACN=" + cardACN, x);
		
		var uri = urihost+"/COMPONENT/component_acct_aj";
		var rdata = { ACN: cardACN };
		
		fstop.getServerDataEx(uri, rdata, true, CheckIdResult);
	}
	//失敗
	else{
		showTempMessage(500,"晶片金融卡身份查驗","","MaskArea",false);
	}
}
function CheckIdResult(data) {
	console.log("data: " + data);
	unBlockUI(checkIdBlockId);
	if (data) {
		// login_aj回傳資料
		console.log("data.json: " + JSON.stringify(data));
		// 成功
		if("0" == data.msgCode) {
			removeThenInsertCard();
		} else {
			showTempMessage(500,"晶片金融卡身份查驗","","MaskArea",false);
			alert("非本人帳戶之晶片金融卡，無法進行此功能");
		}
	}
}
//拔插卡
function removeThenInsertCard(){
	RemoveThenInsertCard("MaskArea",60,OKReaderName,"removeThenInsertCardFinish");
}
//拔插卡結束(成功才會到此FUNCTION)
function removeThenInsertCardFinish(){
	//取得卡片銀行代碼
	preGenerateTAC();
}
CheckIdProcess = function()
{
	/*var strCheckMessage = "<br><p>晶片金融卡身份查驗中，請稍候...</p><br>";
	strCheckMessage += "<p>查驗期間請勿移除您的晶片金融卡，以免造成驗證錯誤。</p><br><br>";
	CRC.showTempMessage(500, "晶片金融卡身份查驗", strCheckMessage, true);
	CRC.getCardInfo();

	var cardACN = CRC.Accounts[0];
	if(cardACN.length > 11)cardACN = cardACN.substr(cardACN.length - 11);
	
	var x = { method: 'get', parameters: '', onComplete: CheckIdResult };	
	var myAjax = new Ajax.Request( "simpleform?trancode=N953&TXID=N953&ACN=" + cardACN, x);*/
	
	showTempMessage(500,"晶片金融卡身份查驗","<br><p>晶片金融卡身份查驗中，請稍候...</p><br><p>查驗期間請勿移除您的晶片金融卡，以免造成驗證錯誤。</p><br><br>","MaskArea",true);
	
	//取得卡片主帳號
	getMainAccount();
}
//CheckIdResult = function(response)
//{
//	var ResultStr = response.responseText;
//	//CRC.showTempMessage(500, "晶片金融卡身份查驗", "", false);
//	showTempMessage(500,"晶片金融卡身份查驗","","MaskArea",false);
//	
//	if(ResultStr.indexOf("GRANTED") == -1)
//	{
//		alert("非本人帳戶之晶片金融卡，無法進行此功能");
//	}
//	else
//	{
//		SubmitForm();
//	}
//}

//準備押碼所需資料
function preGenerateTAC(){
	if(window.console){console.log("preGenerateTAC...");}
	
	var TRMID = MakeTRMID();
	
	var formId = document.getElementById("formId");
	formId.TRMID.value = TRMID;
	
	var transData = "2500" + TRMID + formId.ACNNO.value;
	
	// 小鍵盤輸入密碼
	if(window.console){console.log("generateTAC...");}
	GenerateTAC(OKReaderName,transData,pazzword,"generateTACFinish");
	
	// 改成新機制 (二代機由鍵盤輸入密碼、generateTAC驗密碼、元件遮罩)
//	var readerType = getReaderType(cardReadersName,"","readerType");
//	if(readerType=="1"){
//		// 一代機
//		generateTAC(cardReadersName, pazzword, transData);
//	} else {
//		// 二代機
//		alert("請於讀卡機鍵盤輸入密碼");
//		generateTAC(cardReadersName, "******", transData);
//	}
	
}
//卡片押碼結束
function generateTACFinish(result){
	//成功
	if(result != "false"){
		var TACData = result.split(",");
		
		var main = document.getElementById("formId");
		main.iSeqNo.value = TACData[1];
		main.ICSEQ.value = TACData[1];
		main.TAC.value = TACData[2];
		
		//再押一次
		generateTAC2();
	}
	//失敗
	else{
		FinalSendout("MaskArea",false);
	}
}
//卡片押碼2
function generateTAC2(){
	var TRMID = MakeTRMID();
	
	var main = document.getElementById("formId");
	main.TRMID.value = TRMID;
	
	var transData = "2500" + TRMID + main.ACNNO.value;
	
	GenerateTAC(OKReaderName,transData,pazzword,"generateTAC2Finish");
}
//卡片押碼2結束
function generateTAC2Finish(result){
	//成功
	if(result != "false"){
		var TACData = result.split(",");
		
		var main = document.getElementById("formId");
		main.ICSEQ1.value = TACData[1];
		main.TAC.value = TACData[2];
		
		var ACN_Str1 = main.ACNNO.value;
		if(ACN_Str1.length > 11){
			ACN_Str1 = ACN_Str1.substr(ACN_Str1.length - 11);
		}
	
		main.CHIP_ACN.value = ACN_Str1;
		main.OUTACN.value = ACN_Str1;
		main.ACNNO.value = ACN_Str1;
		
		main.submit();	
	}
	//失敗
	else{
		FinalSendout("MaskArea",false);
	}
}



