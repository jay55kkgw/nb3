
var readerNamesArray = ""; // ConnectCard方法撈出來的讀卡機名稱陣列
var currentIndexAtReaderNamesArray = -1; // 目前讀卡機名稱陣列的位置
var result = ""; // Callback裡面塞要回傳的結果
var retryTime = 0; // 重試次數計數
var retryTimeLimit = 0; // 重試次數上限

// 讀卡機初始化
function Initialize(outerCallBackFunction) {
	if(window.console){console.log("Initialize...");}
	
	// 還原成預設值
	readerNamesArray = "";
	currentIndexAtReaderNamesArray = -1;
	result = "";
	
	var rpcName = "Initialize";

	topWebSocketUtil.invokeRpcDispatcher(Initialize_Callback,
										 rpcName,
										 window.location.href);
	
    // Initialize_Callback
	function Initialize_Callback(rpcStatus, rpcReturn) {
		if(window.console){console.log("Initialize_Callback...");}
	    try {
	        topWebSocketUtil.tryRpcStatus(rpcStatus);

	        if(window.console){console.log("rpcReturn: " + rpcReturn);}
	        var readerData = rpcReturn.split('|');
	        if(window.console){console.log("readerData: " + readerData);}

	        // 失敗
	        if (readerData[0] != "E000") {
//	            alert("Initialize Error = " + readerData[0]);
	        	// e.x. E002,8010002E
	        	var readerData = rpcReturn.split(',');
	        	if(window.console){console.log(GetErrorMessage(readerData[0]));}
	        	
	        } // 成功
	        else {
	        	// e.x. E000,Generic Smart Card Reader Interface 0
	        	if(window.console){console.log("readerData0: " + readerData[0]);}
	        	if(window.console){console.log("readerData1: " + readerData[1]);}
	        	
	        	result = readerData[0];
	        	
	        	var readerName = readerData[1];
	        	
	        	// 二代讀卡機要另外做判斷
	        	if( getReaderType(readerName) == '2' ) {
	        		var targetArray = ["CASTLES EZpad","Todos eCode Connectable","Todos eCode Connectable II","ACS ACR83U","ACS APG8201"];
	        		readerName.split('~').forEach(function(itemA, indexA, arrayA){
	        			targetArray.forEach(function(itemB, indexB, arrayB) {
//	        				if( itemA.includes(itemB) && !itemA.includes('SAM') ){ // IE不支援
        					if( itemA.indexOf(itemB)>=0 && !itemA.indexOf('SAM')>=0 ){
	        					// 目標二代讀卡機名稱
	        					readerName = itemA;
	        				}
	        			});
	        		});
	        		
	        	} else {
	        		// 只有一代讀卡機但可能是複數台
	        		readerName.split('~').forEach(function(itemA, indexA, arrayA){
    					// 目標讀卡機名稱
    					readerName = itemA;
	        		});
	        	}
	        	
	        	document.getElementById('CARDNAME').value = readerName;
			}
	    } catch (exception) {
	    	unBlockUI(initBlockId);
//	        alert("exception: " + exception);
            errorBlock(
    				null,
    				null,
    				[exception], 
    				'確定', 
    				null
    			);
	        result = false;
	    }
	    
	    var fullCallBack = outerCallBackFunction + "('" + result + "')";
		eval(fullCallBack);
	}
}

// 偵測讀卡機
function ConnectCard(outerCallBackFunction){
	if(window.console){console.log("ConnectCard...");}
	
	// 還原成預設值
	readerNamesArray = "";
	currentIndexAtReaderNamesArray = -1;
	result = "";
	
	var rpcName = "ConnectCard";
	
	// 偵測讀卡機
	topWebSocketUtil.invokeRpcDispatcher(ConnectCard_Callback,
										 rpcName,
										 window.location.href);
	// 偵測讀卡機_Callback
	function ConnectCard_Callback(rpcStatus,rpcReturn){
		if(window.console){console.log("ConnectCard_Callback...");}
		try{
			topWebSocketUtil.tryRpcStatus(rpcStatus);
			
			if(window.console){console.log("rpcReturn: " + rpcReturn);}
			var splitedReturn = rpcReturn.split('|');
			if(window.console){console.log("splitedReturn: " + splitedReturn);}
			
			// 失敗
			if (splitedReturn[0] != "E000"){
				//重試
	        	if(retryTime < retryTimeLimit){
	        		retryTime += 1;
	        		
	        		setTimeout(function(){ConnectCard(outerCallBackFunction);},1000);
	        		
	        		return;
	        	} //不重試了
	        	else{
	        		splitedReturn = rpcReturn.split(',');
	        		unBlockUI(initBlockId);
	        		
//					alert(GetErrorMessage(splitedReturn[0]));
			   		errorBlock(
						null,
						null,
						[GetErrorMessage(splitedReturn[0])], 
						'確定', 
						null
					);

		        	//這裡要分沒安裝元件和有安裝元件但有E系列的錯誤訊息
		        	//沒安裝元件
		        	if(splitedReturn[0] == "E_Send_11_OnError_1006"){
		        		result = "E_Send_11_OnError_1006";
		        	}
		        	//有安裝元件但有E系列的錯誤訊息
		        	else{
		        		result = false;
		        	}
	        	}
			} // 成功
			else {
				//在某些情況下，未插讀卡機時，元件會回傳0，所以要另外判斷splitedReturn[1]是否有值
				var getSeperate = rpcReturn.indexOf("|");
				var splitedReturnOne = rpcReturn.substring(getSeperate+1);
				//var splitedReturnOne = splitedReturn[1].trim();
				if(window.console){console.log("splitedReturnOne=" + splitedReturnOne);}
				
				//有值
				if(splitedReturnOne != ""){
					result = splitedReturnOne;
		        	
		        	//將值ASSIGN給readerNamesArray
					readerNamesArray= splitedReturnOne.split(",");
				} //沒值等同失敗
				else{
					//重試
		        	if(retryTime < retryTimeLimit){
		        		retryTime += 1;
		        		
		        		setTimeout(function(){ConnectCard(outerCallBackFunction);},1000);
		        		
		        		return;
		        	} //不重試了
		        	else{
		        		//0且splitedReturnOne沒值，代碼改成E001
		        		if(window.console){console.log("CHANGE MESSAGECODE TO E001");}
		        		unBlockUI(initBlockId);
		        		
//		        		alert(GetErrorMessage("E001"));
				   		errorBlock(
							null,
							null,
							[GetErrorMessage("E001")], 
							'確定', 
							null
						);
			        	
		        		result = false;
		        	}
				}
			}
		} catch (exception){
			unBlockUI(initBlockId);
//			alert(exception);
            errorBlock(
    				null,
    				null,
    				[exception], 
    				'確定', 
    				null
    			);
			result = false;
		}
		
		//歸零
		retryTime = 0;
		
		var fullCallBack = outerCallBackFunction + "('" + result + "')";

		eval(fullCallBack);
	}
}

//和ConnectCard的差別在於不ALERT MESSAGE，用於判斷用戶是否需要下載元件
function ConnectCardLite(outerCallBackFunction){
	if(window.console){console.log("ConnectCardLite...");}
	
	// 還原成預設值
	readerNamesArray = "";
	currentIndexAtReaderNamesArray = -1;
	result = "";
	
	var rpcName = "ConnectCard";

	// 偵測讀卡機
	topWebSocketUtil.invokeRpcDispatcher(ConnectCard_Callback,
										 rpcName,
										 window.location.href);
	// 偵測讀卡機_Callback
	function ConnectCard_Callback(rpcStatus,rpcReturn){
		if(window.console){console.log("ConnectCard_Callback...");}
		try{
			topWebSocketUtil.tryRpcStatus(rpcStatus);
			
			if(window.console){console.log("rpcReturn: " + rpcReturn);}
			var splitedReturn = rpcReturn.split('|');
			if(window.console){console.log("splitedReturn: " + splitedReturn);}
			
			// 失敗
			if (splitedReturn[0] != "E000"){
				// 重試
	        	if(retryTime < retryTimeLimit){
	        		retryTime += 1;
	        		
	        		setTimeout(function(){ConnectCardLite(outerCallBackFunction);},1000);
	        		
	        		return;
	        		
	        	} // 不重試了，這裡要分沒安裝元件和有安裝元件但有E系列的錯誤訊息
	        	else{
		        	// 沒安裝元件
		        	if(splitedReturn[0] == "E_Send_11_OnError_1006"){
		        		result = "E_Send_11_OnError_1006";
		        	} // 有安裝元件但有E系列的錯誤訊息
		        	else{
		        		result = false;
		        	}
		        	
	        	}
	        	
			} // 成功
	        else{
        		result = splitedReturn[1];
	        	
	        	// 將值ASSIGN給readerNamesArray
				readerNamesArray= splitedReturn[1].split(",");
	        	
	        }
			
		} catch (exception){
			unBlockUI(initBlockId);
//			alert(exception);
            errorBlock(
    				null,
    				null,
    				[exception], 
    				'確定', 
    				null
    			);
			result = false;
		}
		
		//歸零
		retryTime = 0;
		
		var fullCallBack = outerCallBackFunction + "('" + result + "')";

		eval(fullCallBack);
	}
}

// 取得卡片編號
function DetectCard(readerName,outerCallBackFunction){
	if(window.console){console.log("DetectCard...");}
	
	// 還原成預設值
	result = "";
	
	var rpcName = "DetectCard";
	
	topWebSocketUtil.invokeRpcDispatcher(DetectCard_Callback,
										 rpcName,
										 readerName);
	// DetectCard__Callback
	function DetectCard_Callback(rpcStatus,rpcReturn){
		if(window.console){console.log("DetectCard_Callback...");}
		try{
			topWebSocketUtil.tryRpcStatus(rpcStatus);
			
			if(window.console){console.log("rpcReturn: " + rpcReturn);}
			var DetectCardData = rpcReturn.split('|');
			if(window.console){console.log("DetectCardData: " + DetectCardData);}
			
			// 失敗
			if (DetectCardData[0] != "E000") {
	            DetectCardData = rpcReturn.split(',');
	            unBlockUI(initBlockId);
	            
//	            alert(GetErrorMessage(DetectCardData[0]));
		   		errorBlock(
					null,
					null,
					[GetErrorMessage(DetectCardData[0])], 
					'確定', 
					null
				);
		   		
	        	result = false;
	        } // 成功
	        else {
	            DetectCardData = rpcReturn.split('|');
	            result = DetectCardData[1];
	        }
			
		} catch (exception){
			if(window.console){console.log("exception="+exception);}
			unBlockUI(initBlockId);
//			alert("無法取得卡片編號");
            errorBlock(
    				null,
    				null,
    				["無法取得卡片編號"], 
    				'確定', 
    				null
    			);
			result = false;
		}
		
		var fullCallBack = outerCallBackFunction + "('" + result + "')";
		
		eval(fullCallBack);
	}
}

// 找出有插卡的讀卡機
function FindOKReader(outerCallBackFunction){
	if(window.console){console.log("FindOKReader...");}
	
	// 準備
	++currentIndexAtReaderNamesArray;
	// 目前的讀卡機名稱
	var thisReaderName = readerNamesArray[currentIndexAtReaderNamesArray];
	
	var rpcName = "DetectCard";
	
	topWebSocketUtil.invokeRpcDispatcher(FindOKReader_Callback,
										 rpcName,
										 thisReaderName);
	// FindOKReader_Callback
	function FindOKReader_Callback(rpcStatus,rpcReturn){
		if(window.console){console.log("FindOKReader_Callback...");}
		try{
			topWebSocketUtil.tryRpcStatus(rpcStatus);
			
			if(window.console){console.log("rpcReturn: " + rpcReturn);}
			var DetectCardData = rpcReturn.split('|');
			if(window.console){console.log("DetectCardData: " + DetectCardData);}
			
			// 失敗
			if (DetectCardData[0] != "E000") {
				// 結束
				if(currentIndexAtReaderNamesArray == readerNamesArray.length - 1){
					// 還原成預設值
					readerNamesArray = "";
					currentIndexAtReaderNamesArray = -1;
					
					DetectCardData = rpcReturn.split(',');
					unBlockUI(initBlockId);
//		            alert(GetErrorMessage(DetectCardData[0]));
		            errorBlock(
						null,
						null,
						[GetErrorMessage(DetectCardData[0])], 
						'確定', 
						null
					);
				}
				// 繼續
				else{
					FindOKReader(outerCallBackFunction);
				}
				
	        } // 成功
	        else {
	        	console.log("readerNamesArray: " + readerNamesArray);
				var fullCallBack = outerCallBackFunction + "('" + readerNamesArray + "')";
//	        	console.log("readerNamesArray[currentIndexAtReaderNamesArray]: " + readerNamesArray[currentIndexAtReaderNamesArray]);
//				var fullCallBack = outerCallBackFunction + "('" + readerNamesArray[currentIndexAtReaderNamesArray] + "')";
				
				// 還原成預設值
				readerNamesArray = "";
				currentIndexAtReaderNamesArray = -1;
				
				eval(fullCallBack);
	        }
			
		} catch (exception){
			if(window.console){console.log("exception="+exception);}
			unBlockUI(initBlockId);
//			alert(exception);
            errorBlock(
    				null,
    				null,
    				[exception], 
    				'確定', 
    				null
    			);
		}
	}
}

// 取得卡片發卡行資訊
function GetUnitCode(readerName,outerCallBackFunction){
	if(window.console){console.log("GetUnitCode...");}
	
	// 還原成預設值
	result = "";
	
    var rpcName = "GetUnitCode";
    
    topWebSocketUtil.invokeRpcDispatcher(GetUnitCode_Callback,
										 rpcName,
										 readerName);
	// GetUnitCode_Callback
    function GetUnitCode_Callback(rpcStatus,rpcReturn){
    	if(window.console){console.log("GetUnitCode_Callback...");}
    	try{
    		topWebSocketUtil.tryRpcStatus(rpcStatus);
    		
    		if(window.console){console.log("rpcReturn: " + rpcReturn);}
            var UnitCodeData = rpcReturn.split('|');
            if(window.console){console.log("UnitCodeData: " + UnitCodeData);}
            
        	// 失敗
            if (UnitCodeData[0] != "E000") {
            	UnitCodeData = rpcReturn.split(',');
            	unBlockUI(initBlockId);
//            	alert(GetErrorMessage(UnitCodeData[0]));
	            errorBlock(
					null,
					null,
					[GetErrorMessage(UnitCodeData[0])], 
					'確定', 
					null
				);
	        	result = false;
	        	
            } // 成功
        	else {
        		result = UnitCodeData[1];
            }

    	} catch (exception){
    		if(window.console){console.log("exception="+exception);}
    		unBlockUI(initBlockId);
//    		alert("無法取得卡片銀行代號");
            errorBlock(
    				null,
    				null,
    				["無法取得卡片銀行代號"], 
    				'確定', 
    				null
    			);
    		result = false;
        }
		var fullCallBack = outerCallBackFunction + "('" + result + "')";
		
		eval(fullCallBack);
    }
}

// 取得卡片主帳號
function GetMainAccount(readerName,outerCallBackFunction){
	if(window.console){console.log("GetMainAccount...");}
	
	// 還原成預設值
	result = "";
	
	var rpcName = "GetMainAccount";

	topWebSocketUtil.invokeRpcDispatcher(GetMainAccount_Callback,
										 rpcName,
										 readerName);

	// GetMainAccount_Callback
	function GetMainAccount_Callback(rpcStatus,rpcReturn){
		if(window.console){console.log("GetMainAccount_Callback...");}
		try{
			topWebSocketUtil.tryRpcStatus(rpcStatus);
			
			if(window.console){console.log("rpcReturn: " + rpcReturn);}
			var AccountData = rpcReturn.split('|');
			if(window.console){console.log("AccountData="+AccountData);}
			
			// 失敗
	        if (AccountData[0] != "E000") {
	            AccountData = rpcReturn.split(',');
	            unBlockUI(initBlockId);
//	            alert(GetErrorMessage(AccountData[0]));
	            errorBlock(
					null,
					null,
					[GetErrorMessage(AccountData[0])], 
					'確定', 
					null
				);
	        	result = false;
            } // 成功
	        else {
	        	result = AccountData[1].split(',')[0];
	        }
			
		} catch (exception){
			if(window.console){console.log("exception="+exception);}
			unBlockUI(initBlockId);
//			alert("無法取得卡片帳號");
            errorBlock(
    				null,
    				null,
    				["無法取得卡片帳號"], 
    				'確定', 
    				null
    			);
			result = false;
		}
		
		var fullCallBack = outerCallBackFunction + "('" + result + "')";

		eval(fullCallBack);
	}
}

// 卡片押碼
function GenerateTAC(readerName, transData, pinCode, outerCallBackFunction){
	if(window.console){console.log("GenerateTAC...");}
	
	// 還原成預設值
	result = "";
	
	var rpcName = "SimpleGenerateTAC";
	
	topWebSocketUtil.invokeRpcDispatcher(GenerateTAC_Callback,
										 rpcName,
										 readerName,
										 transData,
										 pinCode);
	
	// GenerateTAC_Callback
	function GenerateTAC_Callback(rpcStatus,rpcReturn){
		if(window.console){console.log("GenerateTAC_Callback...");}
		try{
			topWebSocketUtil.tryRpcStatus(rpcStatus);
			
			if(window.console){console.log("rpcReturn: " + rpcReturn);}
			var GenerateTACData = rpcReturn.split('|');
			if(window.console){console.log("GenerateTACData="+GenerateTACData);}
			
			// 成功
			if (GenerateTACData[0] == "0") {
				var tsnData = GenerateTACData[1];
				var tacData = GenerateTACData[2];
				
				result = GenerateTACData;
						
	        } // 失敗
	        else {
				GenerateTACData = rpcReturn.split(',');
				unBlockUI(initBlockId);
//				alert(GetErrorMessage(GenerateTACData[0]));
	            errorBlock(
					null,
					null,
					[GetErrorMessage(GenerateTACData[0])], 
					'確定', 
					null
				);
				result = false;
			}
			
		} catch (exception){
			if(window.console){console.log("exception="+exception);}
			unBlockUI(initBlockId);
//			alert("無法取得卡片TAC");
            errorBlock(
    				null,
    				null,
    				["無法取得卡片TAC"], 
    				'確定', 
    				null
    			);
			result = false;
		}
		
		var fullCallBack = outerCallBackFunction + "('" + result + "')";
		
		eval(fullCallBack);
	}
}

// 驗證卡片密碼
function VerifyPin(readerName,pinCode,outerCallBackFunction){
	if(window.console){console.log("VerifyPin...");}
	
	// 還原成預設值
	result = "";

	var rpcName = "VerifyPin";
	
	topWebSocketUtil.invokeRpcDispatcher(VerifyPin_Callback,
										 rpcName,
										 readerName,
										 pinCode);
	// VerifyPin_Callback
	function VerifyPin_Callback(rpcStatus,rpcReturn){
		if(window.console){console.log("VerifyPin_Callback...");}
		try{
			topWebSocketUtil.tryRpcStatus(rpcStatus);
			
			if(window.console){console.log("rpcReturn: " + rpcReturn);}
			var VerifyPinData = rpcReturn.split('|');
			if(window.console){console.log("VerifyPinData: " + VerifyPinData);}

			// 失敗
			if(VerifyPinData[0] != "E000"){
				unBlockUI(initBlockId);
//				alert(GetErrorMessage(VerifyPinData[0]));
	            errorBlock(
					null,
					null,
					[GetErrorMessage(VerifyPinData[0])], 
					'確定', 
					null
				);
				result = false;
			} // 成功
			else{
				result = true;
			}
		}
		catch(exception){
			unBlockUI(initBlockId);
//			alert("無法驗證卡片密碼");
            errorBlock(
    				null,
    				null,
    				["無法驗證卡片密碼"], 
    				'確定', 
    				null
    			);
			result = false;
	    }
		
		var fullCallBack = outerCallBackFunction + "('" + result + "')";
		
		eval(fullCallBack);
	}
}

// 偵測拔插卡動作
function RemoveThenInsertCard(maskID, maxCountDown, readerName, outerCallBackFunction){
	if(window.console){console.log("RemoveThenInsertCard...");}
	
	var rpcName = "TestRemoveThenInsertCard";
	
	// 元件開始偵測拔插卡和倒數計時
	topWebSocketUtil.invokeRpcDispatcher(TestRemoveThenInsertCard_Callback,
										 rpcName,
										 readerName,
										 maxCountDown);
	
	// 顯示拔插卡的畫面，元件不顯示畫面了
	showRemoveInsertPad(maskID,maxCountDown);
	
	RelocateCountdown(maskID,parseInt(document.getElementById("CountdownNum").value),readerName,outerCallBackFunction);
	
	// TestRemoveThenInsertCard_Callback
	function TestRemoveThenInsertCard_Callback(rpcStatus,rpcReturn){
		if(window.console){console.log("TestRemoveThenInsertCard_Callback...");}
		try{
			RelocatedFinish(maskID);
			
			topWebSocketUtil.tryRpcStatus(rpcStatus);
			
			// "OK":正常, "CANCEL":按"取消"按鈕, "TIMEOUT":操作逾時, OTHER:異常
			if(window.console){console.log("rpcReturn: " + rpcReturn);}
	        
			// 成功
			if(rpcReturn == "OK"){
				var fullCallBack = outerCallBackFunction + "()";
				eval(fullCallBack);
				
			} // 失敗
			else{
				if(rpcReturn == "TIMEOUT"){
					unBlockUI(initBlockId);
//					alert("拔插卡操作逾時，請重新再試");
		            errorBlock(
						null,
						null,
						["拔插卡操作逾時，請重新再試"], 
						'確定', 
						null
					);
				}
				else if(rpcReturn == "CANCEL"){
					unBlockUI(initBlockId);
//					alert("使用者取消拔插卡操作，請重新再試");
		            errorBlock(
						null,
						null,
						["使用者取消拔插卡操作，請重新再試"], 
						'確定', 
						null
					);
				}
				else{
					unBlockUI(initBlockId);
//					alert("拔插卡操作錯誤，請重新再試");
		            errorBlock(
						null,
						null,
						["拔插卡操作錯誤，請重新再試"], 
						'確定', 
						null
					);
				}
			}
			
		} catch (exception){
			unBlockUI(initBlockId);
//			alert(exception);
            errorBlock(
    				null,
    				null,
    				[exception], 
    				'確定', 
    				null
    			);
		}
	}
}

// TIMEOUT物件
var setTimeoutObject;

// 拔插卡倒數計時
function RelocateCountdown(maskID,countDown,readerName,outerCallBackFunction){
	if(window.console){console.log("countDown="+countDown);}
	
	if(countDown > 0){
		document.getElementById("CountdownNum").innerHTML = countDown - 1;
		
//		if(countDown < 10){
//			document.getElementById("CountdownNum").style.color = "red";
//		}

		setTimeoutObject = setTimeout(function(){
			RelocateCountdown(maskID,parseInt(document.getElementById("CountdownNum").innerHTML),readerName,outerCallBackFunction);
		},1000);
	}
	else{
		RelocatedFinish(maskID);
	}
}

// 拔插卡倒數計時結束
function RelocatedFinish(maskID){
	clearTimeout(setTimeoutObject);
	
//	document.getElementById(maskID).innerHTML = "";
	$("#removeInsertMask").hide();
	blockId = blockUI();
}

// 目前沒用到
function GetAccounts(readerName,outerCallBackFunction){
	var rpcName = "GetMainAccount";
	var result;
	
	topWebSocketUtil.invokeRpcDispatcher(GetMainAccount_Callback,rpcName,readerName);

	function GetMainAccount_Callback(rpcStatus,rpcReturn){
		try{
			topWebSocketUtil.tryRpcStatus(rpcStatus);
			var AccountData = rpcReturn.split('|');
			
			if(AccountData[0] != "E000"){
				AccountData = rpcReturn.split(',');
				unBlockUI(initBlockId);
//				alert(GetErrorMessage(AccountData[0]));
	            errorBlock(
					null,
					null,
					[GetErrorMessage(AccountData[0])], 
					'確定', 
					null
				);
				result = false;
			}
			else{
				result = AccountData[1];
			}
		}
		catch(exception){
			unBlockUI(initBlockId);
//			alert("無法取得卡片帳號");
            errorBlock(
    				null,
    				null,
    				["無法取得卡片帳號"], 
    				'確定', 
    				null
    			);
			result = false;
		}
		var fullCallBack = outerCallBackFunction + "('" + result + "')";

		eval(fullCallBack);
	}
}

// 目前沒用到
function GetNoteData(readerName,outerCallBackFunction){
	var rpcName = "GetNoteData";
	var result;
	
	topWebSocketUtil.invokeRpcDispatcher(GetNoteData_Callback,rpcName,readerName);

	function GetNoteData_Callback(rpcStatus,rpcReturn){
		try{
            topWebSocketUtil.tryRpcStatus(rpcStatus);
            var NoteData = rpcReturn.split('|');

            if(NoteData[0] != "E000"){
            	NoteData = rpcReturn.split(',');
            	unBlockUI(initBlockId);
//            	alert(GetErrorMessage(NoteData[0]));
	            errorBlock(
					null,
					null,
					[GetErrorMessage(NoteData[0])], 
					'確定', 
					null
				);
            	result = false;
            }
            else{
            	result = NoteData[1];
            }
		}
		catch(exception){
			unBlockUI(initBlockId);
//			alert("無法取得卡片備註");
            errorBlock(
    				null,
    				null,
    				["無法取得卡片備註"], 
    				'確定', 
    				null
    			);
			result = false;
		}
		var fullCallBack = outerCallBackFunction + "('" + result + "')";
		
		eval(fullCallBack);
    }
}

// 目前沒用到
function GetTranAccount(readerName,outerCallBackFunction){
	var rpcName = "GetTranAccount";
	var result;
	
	topWebSocketUtil.invokeRpcDispatcher(GetTranAccount_Callback,rpcName,readerName);

	function GetTranAccount_Callback(rpcStatus,rpcReturn){
		try{
			topWebSocketUtil.tryRpcStatus(rpcStatus);
			var TranAccountData = rpcReturn.split('|');

			if(TranAccountData[0] != "E000"){
				TranAccountData = rpcReturn.split(',');
				unBlockUI(initBlockId);
//				alert(GetErrorMessage(TranAccountData[0]));
	            errorBlock(
					null,
					null,
					[GetErrorMessage(TranAccountData[0])], 
					'確定', 
					null
				);
				result = false;
			}
			else{
				result = TranAccountData[1];
			}
		}
		catch(exception){
			unBlockUI(initBlockId);
//			alert("無法取得卡片交易帳號");
            errorBlock(
    				null,
    				null,
    				["無法取得卡片交易帳號"], 
    				'確定', 
    				null
    			);
			result = false;
		}
		var fullCallBack = outerCallBackFunction + "('" + result + "')";
		
		eval(fullCallBack);
	}
}

// 目前沒用到
function ChangePin(readerName,oldPassword,newPassword,outerCallBackFunction){
    var rpcName = "ChangePin";
    var result;
    
    topWebSocketUtil.invokeRpcDispatcher(ChangePin_Callback,rpcName,readerName,oldPassword,newPassword);
    
	function ChangePin_Callback(rpcStatus,rpcReturn){
    	try{
    		topWebSocketUtil.tryRpcStatus(rpcStatus);
    		var ChangePinData = rpcReturn.split('|');

			if(ChangePinData[0] != "E000"){
				ChangePinData = rpcReturn.split(',');
				unBlockUI(initBlockId);
//				alert(GetErrorMessage(ChangePinData[0]));
	            errorBlock(
					null,
					null,
					[GetErrorMessage(ChangePinData[0])], 
					'確定', 
					null
				);
				result = false;
			}
			else{
				result = true;
			}
    	}
    	catch(exception){
    		unBlockUI(initBlockId);
//    		alert("無法變更卡片密碼");
            errorBlock(
    				null,
    				null,
    				["無法變更卡片密碼"], 
    				'確定', 
    				null
    			);
    		result = false;
    	}
    	var fullCallBack = outerCallBackFunction + "('" + result + "')";
		
		eval(fullCallBack);
    }
}

// 目前沒用到
function Finalize(readerName){
	var rpcName = "Finalize";

	topWebSocketUtil.invokeRpcDispatcher(Finalize_Callback,rpcName,readerName);

	function Finalize_Callback(rpcStatus,rpcReturn){
		topWebSocketUtil.tryRpcStatus(rpcStatus);
	}
}

// 檢查元件版本
function GetVersion(newVersion,outerCallBackFunction){
	if(window.console){console.log("GetVersion...");}
	
	// 還原成預設值
	result = "";

	var rpcName = "GetVersion";
	
	topWebSocketUtil.invokeRpcDispatcher(GetVersion_Callback,rpcName);

	// GetVersion_Callback
	function GetVersion_Callback(rpcStatus,rpcReturn){
		if(window.console){console.log("GetVersion_Callback...");}
		try{
			topWebSocketUtil.tryRpcStatus(rpcStatus);
			
			var currentVersion = rpcReturn;
			if(window.console){console.log("currentVersion: " + currentVersion);}
			if(window.console){console.log("newVersion: " + newVersion);}
			
			var currentVersionNO = parseInt(currentVersion.replace(/\./g,""));
			var newVersionNO = parseInt(newVersion.replace(/\./g,""));
			
			if(window.console){console.log("currentVersionNO="+currentVersionNO);}
			if(window.console){console.log("newVersionNO="+newVersionNO);}
			
			// 不用更新
			if(currentVersionNO >= newVersionNO){
				result = true;
			}
			// 要更新
			else{
	        	result = false;
	        }
			
		} catch (exception){
			if(window.console){console.log("exception="+exception);}
			unBlockUI(initBlockId);
//			alert("無法取得元件版本");
            errorBlock(
				null,
				null,
				["無法取得元件版本"], 
				'確定', 
				null
			);
			result = false;
		}
		
		var fullCallBack = outerCallBackFunction + "('" + result + "')";
		
		eval(fullCallBack);
	}
	
}

