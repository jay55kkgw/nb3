//取得讀卡機
function listReaders(){
	ConnectCard("listReadersFinish");
}
//取得讀卡機結束
function listReadersFinish(result){
	//成功
	if(result != "false" && result != "E_Send_11_OnError_1006"){
		//找出有插卡的讀卡機
		findOKReader();
	}
}
//找出有插卡的讀卡機
function findOKReader(){
	FindOKReader("findOKReaderFinish");
}
var OKReaderName = "";
//找出有插卡的讀卡機結束(有找到才會到此FUNCTION)
function findOKReaderFinish(okReaderName){
	//ASSIGN到全域變數
	OKReaderName = okReaderName;
	
	//取得卡片銀行代碼
	getCardBankID();
}
//取得卡片銀行代碼
function getCardBankID(){
	GetUnitCode(OKReaderName,"getCardBankIDFinish");
}
//取得卡片銀行代碼結束
function getCardBankIDFinish(result){
	//成功
	if(result != "false"){
		//還要另外判斷是否為本行卡
		//是
		if(result == "05000000"){
			showDialog("verifyPin",OKReaderName,"verifyPinFinish");
		}
		//不是
		else{
			alert(GetErrorMessage("E005"));
		}
	}
}
//驗證卡片密碼
function verifyPin(readerName,password,outerCallBackFunction){
	VerifyPin(readerName,password,outerCallBackFunction);
}
//驗證卡片密碼結束
function verifyPinFinish(result){
	//成功
	if(result == "true"){
		ShowLoadingBoard("MaskArea",true);
		
		//取得卡片主帳號
		getMainAccount();
	}
}
//取得卡片主帳號
function getMainAccount(){
	GetMainAccount(OKReaderName,"getMainAccountFinish");
}
//取得卡片主帳號結束
function getMainAccountFinish(result){
	//成功
	if(result != "false"){
		var uri = "/nb3"+"/COMPONENT/component_acct_aj";
		var data=result.substring(5);
		var rdata = { ACN:data   };
		fstop.getServerDataEx(uri, rdata, true, CheckIdResult,null,result);

	}
	ShowLoadingBoard("MaskArea",false);
}
function CheckIdResult(data,result){
	showTempMessage(500,'晶片金融卡身份查驗',"","MaskArea",false);
	console.log(data);
	if("0" == data.msgCode) {
		console.log("GOODPEOPLE");
		document.getElementById("OUTACN").value = result;
		document.getElementById("OUTACN_NPD").value = result.substr(5);
	}else{
		//unBlockUI(initBlockId);
		try{
			if(data.data.msgCode == "0"){
		        errorBlock(
					null,
					null,
					["非本人帳戶之晶片金融卡，無法進行此功能"], 
					'確定', 
					null
				);
			}else{
		        errorBlock(
						null,
						null,
						[ data.data.msgCode + " " + data.data.msgName ], 
						'確定', 
						null
					);
			}
		}catch (e) {
	        errorBlock(
				null,
				null,
				["非本人帳戶之晶片金融卡，無法進行此功能"], 
				'確定', 
				null
			);
		}
	}
}