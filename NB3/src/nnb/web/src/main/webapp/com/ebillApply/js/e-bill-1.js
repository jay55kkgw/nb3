$( document ).ready( function () {

    $( "#validationForm" ).validate( {
    	ignore: ".ignore",
    	rules: {
            form_check_agreement: "required"
        },
        messages: {
            form_check_agreement: {
                required: "請勾選確認"
            }
        },
        errorElement: "label",
        errorPlacement: function ( error, element ) {
            error.addClass( "invalid-feedback" );

            if ( element.prop( "type" ) === "checkbox" ) {
                error.insertAfter( element.next( "label" ) );
            } else {
                error.insertAfter( element );
            }
        },
        highlight: function ( element, errorClass, validClass ) {
            $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
        },
        unhighlight: function (element, errorClass, validClass) {
            $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
        },
        submitHandler: function(form) {
            form.submit();
        }
    } );

} );