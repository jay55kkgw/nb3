﻿//網頁一進來就會直接抓indexOf去判斷是哪一種瀏覽器
ckBrowser1 = function()
{
	var sAgent = navigator.userAgent.toLowerCase();
	//IE為預設值，若是其他瀏覽器為true會把上述給override掉，所以不會有判定問題
	this.isIE = (sAgent.indexOf("msie")!=-1 || sAgent.indexOf("trident")!=-1); //IE6.0-7
	
}
/*  
loadva = function(path)
{
	//alert('path==>'+path);
	var oBrow1 = new ckBrowser1();
	
	if(oBrow1.isIE){	
			var strObject = "";
    		// 使用 Script 判斷 win64 或 win32, 執行時間有點久
			if (navigator.platform.toLowerCase() == "win64"){
				//"Windows 64 位元";
				strObject = "<OBJECT ID='myobj' codebase='TBBankPkcs11ATLx64.cab' CLASSID='CLSID:32FDE779-F9DC-4D39-97FB-434102000101' style='display:none'></OBJECT>";
			} else {
				// "Windows 32 位元 或 模擬 64 位元(WOW64)";
				strObject = "<OBJECT ID='myobj' codebase='TBBankPkcs11ATL.cab' CLASSID='CLSID:32FDE779-F9DC-4D39-97FB-434102000101' style='display:none'></OBJECT>";
			}
			document.write(strObject);			
	}
			
}
*/
//元件產生認證碼
function getServerAuth() {
	var ret=myobj.GetServerAuth();
	return ret;
}
//回應認證碼
function verifyServerResponse(authcode) {
		var ret=myobj.VerifyServerResponse(authcode); 
		return ret;
} 
//初始化登入PIN
function CHECKPIN(PIN) {
	myobj.SetTokenType(2);
	var ret=myobj.GetCardStatus();
	console.log("CHECKPIN.ret: " + ret);
	var ret1=-1;
	if(ret!=0)
		alert("請確認讀卡機狀態");
	else {
		if(PIN.length<6 || PIN.length>8)
			alert("密碼長度錯誤");
		else
		{
			ret1=myobj.Login(PIN, "");
			if(ret1!=0)
				alert("錯誤代碼:"+myobj.getErrorCode()+" "+myobj.getErrorMsg());
		}
	}
	return ret1;
}
//載具登出
function LOGOUT() {
	var ret=myobj.Logout();
	return ret;
}
//選擇憑證及取得憑證相關資訊
function SelectCert() {
	var result;
	var ret=myobj.SelectCert(1);
	var x = document.getElementsByName("CMSUBMIT")[0];//取得jsp頁面上的submit button name;
	//alert("SelectCert:"+ret);
	if(ret==0)
	{
		try{
			$("#CertFinger").val(myobj.GetCertFinger());
			$("#CertB64").val(myobj.GetSelectedCertB64());
			$("#CertSerial").val(myobj.GetSelectedCertSerial());
			$("#CertSubject").val(myobj.GetSelectedCertSubject());
			$("#CertIssuer").val(myobj.GetSelectedCertIssuer());
			$("#CertNotBefore").val(myobj.GetSelectedCertNotBefore());
			$("#CertNotAfter").val(myobj.GetSelectedCertNotAfter());
			$("#HiCertType").val(myobj.GetHiCertType());
			
			if( $("#HiCertType").val() == "2.16.886.1.100.3.1.1")
			{
				$("#CUSIDN4").val(myobj.GetHiCertTailCitizenID());
			}
			else if( $("#HiCertType").val() == "2.16.886.1.100.3.2.2.1.1")
			{
				$("#CUSIDN4").val(myobj.GetHiCertUniformOrganizationID());
			}
			//alert("CUSIDN4:"+main.CUSIDN4.value);
		}catch(e){
			alert(e);
		}
	}
	else
	{
		alert("取得憑證資訊失敗");
		var xParent = x.parentNode;//取得innerHTML按鈕的父節點
		  	//將jsp有disabled的按鈕全部將之取消
		while(xParent.innerHTML.indexOf("disabled") > -1)
		{
		  	xParent.innerHTML = xParent.innerHTML.replace("disabled", "");
		}					
	}
	return ret;
}
//取得簽章元件值
function getPkcs7Sign1(Data)
{
	//alert("Data ==>\n"+Data);
	//判斷瀏覽器之後決定適用哪種元件，IE為ActiveX，其他家瀏覽器皆為Plug-in
	var result;//宣告變數
	var x = document.getElementsByName("CMSUBMIT")[0];//取得jsp頁面上的submit button name;
	//alert("Sign Before value:"+Data);
	result  = myobj.SignPKCS7(Data);
	if(result==0)
	{
		$("#pkcs7Sign").val(myobj.GetPKCS7Data()); //31
		//alert("Sign After value:"+main.pkcs7Sign.value);
		SubmitForm();
	}	
	else 
	{
		alert("錯誤代碼："+myobj.getErrorCode()+" 錯誤描述："+myobj.getErrorMsg());
		var xParent = x.parentNode;//取得innerHTML按鈕的父節點
		//將jsp有disabled的按鈕全部將之取消
		while(xParent.innerHTML.indexOf("disabled") > -1)
		{
			xParent.innerHTML = xParent.innerHTML.replace("disabled", "");
		}								
	}			
	return $("#pkcs7Sign").val();
}