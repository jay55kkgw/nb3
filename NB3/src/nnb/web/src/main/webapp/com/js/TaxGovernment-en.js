var Tax = new Array();
var Tax00 = new Array(); //基隆市
var Tax01 = new Array(); //台北市
var Tax02 = new Array(); //新北市
var Tax03 = new Array(); //桃園市
var Tax04 = new Array(); //新竹市
var Tax05 = new Array(); //新竹縣
var Tax06 = new Array(); //苗栗縣
var Tax07 = new Array(); //台中市
var Tax08 = new Array(); //彰化縣
var Tax09 = new Array(); //南投縣
var Tax10 = new Array(); //雲林縣
var Tax11 = new Array(); //嘉義市
var Tax12 = new Array(); //嘉義縣
var Tax13 = new Array(); //台南市
var Tax14 = new Array(); //高雄市
var Tax15 = new Array(); //屏東縣
var Tax16 = new Array(); //宜蘭縣
var Tax17 = new Array(); //花蓮縣
var Tax18 = new Array(); //台東縣
var Tax19 = new Array(); //金門縣	
var Tax20 = new Array(); //澎湖縣
var Tax21 = new Array(); //連江縣


Tax = [Tax00, Tax01, Tax02, Tax03, Tax04, Tax05, Tax06, Tax07, Tax08, Tax09,
       Tax10, Tax11, Tax12, Tax13, Tax14, Tax15, Tax16, Tax17, Tax18, Tax19,
       Tax20, Tax21];
     
//稅徵機關資料
//基隆市 Tax00=[0鄉鎮區,1代號]
Tax00[0]=['Keelung City ',''];
Tax00[1]=['QiduDistrict','114'];
Tax00[2]=['Zhongshan District','111'];
Tax00[3]=['ZhongzhengDistrict','116'];
Tax00[4]=['Ren’ai District','111'];
Tax00[5]=['Anle District','111'];
Tax00[6]=['XinyiDistrict','116'];
Tax00[7]=['Nuannuan District','114'];

//台北市 Tax01=[0鄉鎮區,1代號]
Tax01[0]=['Taipei City',''];
Tax01[1]=['ShilinDistrict','013'];
Tax01[2]=['Datong District ','004'];
Tax01[3]=['Da’an District','011'];
Tax01[4]=['中北區','005'];
Tax01[5]=['ZhongzhengDistrict','007'];
Tax01[6]=['中南區','012'];
Tax01[7]=['Neihu District ','014'];
Tax01[8]=['Wenshan District ','010'];
Tax01[9]=['Beitou District','002'];
Tax01[10]=['Songshan District','008'];
Tax01[11]=['XinyiDistrict','001'];
Tax01[12]=['Nangang District ','009'];
Tax01[13]=['Wanhua District','006'];

//新北市 Tax02=[0鄉鎮區,1代號]
Tax02[0]=['New TaipeiCity ',''];
Tax02[1]=['BaliDistrict','258'];
Tax02[2]=['SanzhiDistrict','258'];
Tax02[3]=['SanchongDistrict','256'];
Tax02[4]=['SanxiaDistrict','251'];
Tax02[5]=['TuchengDistrict','251'];
Tax02[6]=['ZhongheDistrict','274'];
Tax02[7]=['WuguDistrict','276'];
Tax02[8]=['PingxiDistrict','269'];
Tax02[9]=['YongheDistrict','274'];
Tax02[10]=['ShimenDistrict','258'];
Tax02[11]=['ShidingDistrict','264'];
Tax02[12]=['XizhiDistrict','281'];
Tax02[13]=['PinglinDistrict','264'];
Tax02[14]=['LinkouDistrict','276'];
Tax02[15]=['BanqiaoDistrict','251'];
Tax02[16]=['JinshanDistrict','258'];
Tax02[17]=['TaishanDistrict','276'];
Tax02[18]=['WulaiDistrict','264'];
Tax02[19]=['GongliaoDistrict','269'];
Tax02[20]=['TamsuiDistrict ','258'];
Tax02[21]=['ShenkengDistrict','264'];
Tax02[22]=['XindianDistrict','264'];
Tax02[23]=['XinzhuangDistrict','276'];
Tax02[24]=['RuifangDistrict','269'];
Tax02[25]=['WanliDistrict','258'];
Tax02[26]=['ShulinDistrict','251'];
Tax02[27]=['ShuangxiDistrict','269'];
Tax02[28]=['LuzhouDistrict','256'];
Tax02[29]=['YinggeDistrict','251'];

//桃園市 Tax03=[0鄉鎮區,1代號]
Tax03[0]=['Taoyuan City',''];
Tax03[1]=['BadeDistrict','354'];
Tax03[2]=['DayuanDistrict','354'];
Tax03[3]=['DaxiDistrict','362'];
Tax03[4]=['ZhongliDistrict','359'];
Tax03[5]=['PingzhenDistrict','359'];
Tax03[6]=['TaoyuanDistrict','354'];
Tax03[7]=['FuxingDistrict','362'];
Tax03[8]=['XinwuDistrict','352'];
Tax03[9]=['YangmeiDistrict','352'];
Tax03[10]=['LongtanDistrict','362'];
Tax03[11]=['GuishanDistrict','354'];
Tax03[12]=['LuzhuDistrict','354'];
Tax03[13]=['GuanyinDistrict','359'];
Tax03[14]=['LuzhuDistrict','370'];
Tax03[15]=['DayuanDistrict','371'];

//新竹市 Tax04=[0鄉鎮區,1代號]
Tax04[0]=['Hsinchu City ',''];
Tax04[1]=['North District','604'];
Tax04[2]=['East District ','604'];
Tax04[3]=['XiangshanDistrict','604'];

//新竹縣 Tax05=[0鄉鎮區,1代號]
Tax05[0]=['Hsinchu County',''];
Tax05[1]=['WufengTownship','396'];
Tax05[2]=['Beipu Township ','396'];
Tax05[3]=['JianshiTownship','396'];
Tax05[4]=['ZhubeiCity','391'];
Tax05[5]=['ZhudongTownship','396'];
Tax05[6]=['QionglinTownship','396'];
Tax05[7]=['Emei Township ','396'];
Tax05[8]=['HukouTownship','391'];
Tax05[9]=['XinpuTownship','391'];
Tax05[10]=['XinfengTownship','391'];
Tax05[11]=['HengshanTownship','396'];
Tax05[12]=['GuanxiTownship','391'];
Tax05[13]=['Baoshan Township ','396'];

//苗栗縣 Tax06=[0鄉鎮區,1代號]
Tax06[0]=['Miaoli County',''];
Tax06[1]=['Sanyi Township','426'];
Tax06[2]=['Sanwan Township','437'];
Tax06[3]=['Dahu Township','430'];
Tax06[4]=['Gongguan Township','424'];
Tax06[5]=['ZhunanTownship','434'];
Tax06[6]=['XihuTownship','427'];
Tax06[7]=['ZhuolanTownship','431'];
Tax06[8]=['NanzhuangTownship','438'];
Tax06[9]=['Houlong Township ','429'];
Tax06[10]=['Miaoli City','421'];
Tax06[11]=['Yuanli Township ','422'];
Tax06[12]=['Tai’an Township','433'];
Tax06[13]=['TongxiaoTownship','423'];
Tax06[14]=['ZaoqiaoTownship','436'];
Tax06[15]=['ShitanTownship','432'];
Tax06[16]=['Tongluo Township','425'];
Tax06[17]=['Toufen Township ','435'];
Tax06[18]=['Touwu Township ','428'];

//台中市 Tax07=[0鄉鎮區,1代號]
Tax07[0]=['Taichung City',''];
Tax07[1]=['Central District ','083'];
Tax07[2]=['Beitun District ','082'];
Tax07[3]=['North District','085'];
Tax07[4]=['XitunDistrict','086'];
Tax07[5]=['West District ','084'];
Tax07[6]=['East District','088'];
Tax07[7]=['Nantun District ','087'];
Tax07[8]=['South District ','089'];
Tax07[9]=['Dajia District','491'];
Tax07[10]=['Da’an District','496'];
Tax07[11]=['Dadu District','497'];
Tax07[12]=['Dali District','502'];
Tax07[13]=['DayaDistrict','489'];
Tax07[14]=['Taiping District','501'];
Tax07[15]=['Waipu District','495'];
Tax07[16]=['ShigangDistrict','505'];
Tax07[17]=['HouliDistrict','488'];
Tax07[18]=['ShaluDistrict','493'];
Tax07[19]=['Heping District','506'];
Tax07[20]=['DongshiDistrict','503'];
Tax07[21]=['WuriDistrict','499'];
Tax07[22]=['ShengangDistrict','487'];
Tax07[23]=['WuqiDistrict','494'];
Tax07[24]=['QingshuiDistrict','492'];
Tax07[25]=['XinsheDistrict','504'];
Tax07[26]=['TanziDistrict','490'];
Tax07[27]=['Longjing District','498'];
Tax07[28]=['FengyuanDistrict','486'];
Tax07[29]=['WufengDistrict','500'];


//彰化縣 Tax08=[0鄉鎮區,1代號]
Tax08[0]=['ZhanghuaCounty',''];
Tax08[1]=['Ershui Township','558'];
Tax08[2]=['Erlin Township','560'];
Tax08[3]=['Dacun Township','553'];
Tax08[4]=['Dacheng Township','564'];
Tax08[5]=['Beidou Township','559'];
Tax08[6]=['Yongjing Township','556'];
Tax08[7]=['Tianzhong Township','552'];
Tax08[8]=['Tianwei Township','561'];
Tax08[9]=['Zhutang Township','565'];
Tax08[10]=['Shengang Township','545'];
Tax08[11]=['Xiushui Township','547'];
Tax08[12]=['Hemei Township','543'];
Tax08[13]=['Shetou Township','557'];
Tax08[14]=['Fangyuan Township','563'];
Tax08[15]=['Huatan Township','548'];
Tax08[16]=['Fenyuan Township','549'];
Tax08[17]=['Yuanlin Township','550'];
Tax08[18]=['Puxin Township','555'];
Tax08[19]=['Puyan Township','554'];
Tax08[20]=['Pitou Township','562'];
Tax08[21]=['Lugang Township','542'];
Tax08[22]=['Xizhou Township','566'];
Tax08[23]=['Xihu Township','551'];
Tax08[24]=['Zhanghua City','541'];
Tax08[25]=['Fuxing Township','546'];
Tax08[26]=['Xianxi Township','544'];

//NantouCounty Tax09=[0鄉鎮區,1代號]
Tax09[0]=['NantouCounty',''];
Tax09[1]=['Zhongliao Township','515'];
Tax09[2]=['Ren’aiTownship','521'];
Tax09[3]=['ShuiliTownship','516'];
Tax09[4]=['MingjianTownship','514'];
Tax09[5]=['ZhushanTownship','522'];
Tax09[6]=['XinyiTownship','517'];
Tax09[7]=['NantouCity','511'];
Tax09[8]=['PuliTownship','518'];
Tax09[9]=['CaotunTownship','512'];
Tax09[10]=['GuoxingTownship','520'];
Tax09[11]=['YuchiTownship','519'];
Tax09[12]=['LuguTownship','523'];
Tax09[13]=['JijiTownship','513'];

//YunlinCounty Tax10=[0鄉鎮區,1代號]
Tax10[0]=['YunlinCounty',''];
Tax10[1]=['ErlunTownship','625'];
Tax10[2]=['KouhuTownship','621'];
Tax10[3]=['Tuku Township','624'];
Tax10[4]=['DapiTownship','615'];
Tax10[5]=['YuanchangTownship','619'];
Tax10[6]=['DouliuCity','611'];
Tax10[7]=['DounanTownship','612'];
Tax10[8]=['ShuilinTownship','622'];
Tax10[9]=['BeigangTownship','618'];
Tax10[10]=['GukengTownship','614'];
Tax10[11]=['SihuTownship','620'];
Tax10[12]=['XiluoTownship','613'];
Tax10[13]=['DongshiTownship','628'];
Tax10[14]=['LinneiTownship','617'];
Tax10[15]=['HuweiTownship','623'];
Tax10[16]=['LunbeiTownship','626'];
Tax10[17]=['MailiaoTownship','627'];
Tax10[18]=['CitongTownship','616'];
Tax10[19]=['TaixiTownship','630'];
Tax10[20]=['BaozhongTownship','629'];

//JiayiCity Tax11=[0鄉鎮區,1代號]
Tax11[0]=['JiayiCity',''];
Tax11[1]=['WestDistrict','382'];
Tax11[2]=['EastDistrict','381'];

//JiayiCounty Tax12=[0鄉鎮區,1代號]
Tax12[0]=['JiayiCounty',''];
Tax12[1]=['DalinTownship','664'];
Tax12[2]=['DapuTownship','662'];
Tax12[3]=['ZhongpuTownship','660'];
Tax12[4]=['LiujiaoTownship','654'];
Tax12[5]=['Taibao City','658'];
Tax12[6]=['Shuishang Township','659'];
Tax12[7]=['Budai Township','652'];
Tax12[8]=['Minxiong Township','665'];
Tax12[9]=['Puzi City','651'];
Tax12[10]=['Zhuqi Township','667'];
Tax12[11]=['Dongshi Township','655'];
Tax12[12]=['Alishan Township','663'];
Tax12[13]=['Meishan Township','668'];
Tax12[14]=['Lucao Township','657'];
Tax12[15]=['Fanlu Township','661'];
Tax12[16]=['Xingang Township','653'];
Tax12[17]=['Xikou Township','666'];
Tax12[18]=['Yizhu Township','656'];

//台南市 Tax13=[0鄉鎮區,1代號]
Tax13[0]=['Tainan City',''];
Tax13[1]=['East District','142'];
Tax13[2]=['SouthDistrict','143'];
Tax13[3]=['NorthDistrict','145'];
Tax13[4]=['Anping District ','147'];
Tax13[5]=['Annan District ','148'];
Tax13[6]=['West Central District','152'];
Tax13[7]=['QiguDistrict','717'];
Tax13[8]=['XiayingDistrict','697'];
Tax13[9]=['DaneiDistrict','700'];
Tax13[10]=['ShanshangDistrict','704'];
Tax13[11]=['RendeDistrict','709'];
Tax13[12]=['LiujiaDistrict','698'];
Tax13[13]=['BeimenDistrict','719'];
Tax13[14]=['ZuozhenDistrict','708'];
Tax13[15]=['YongkangDistrict','713'];
Tax13[16]=['YujingDistrict','705'];
Tax13[17]=['BaiheDistrict','693'];
Tax13[18]=['AndingDistrict','721'];
Tax13[19]=['XigangDistrict','716'];
Tax13[20]=['JialiDistrict','715'];
Tax13[21]=['GuantianDistrict','699'];
Tax13[22]=['DongshanDistrict','696'];
Tax13[23]=['NanhuaDistrict','707'];
Tax13[24]=['HoubiDistrict','695'];
Tax13[25]=['LiuyingDistrict','694'];
Tax13[26]=['JiangjunDistrict','718'];
Tax13[27]=['MadouDistrict','714'];
Tax13[28]=['ShanhuaDistrict','701'];
Tax13[29]=['XinhuaDistrict','702'];
Tax13[30]=['XinshiDistrict','703'];
Tax13[31]=['XinyingDistrict','691'];
Tax13[32]=['NanxiDistrict','706'];
Tax13[33]=['XuejiaDistrict','720'];
Tax13[34]=['LongqiDistrict','712'];
Tax13[35]=['GuirenDistrict','710'];
Tax13[36]=['GuanmiaoDistrict','711'];
Tax13[37]=['YanshuiDistrict','692'];
Tax13[38]=['EastDistrict','153'];
Tax13[39]=['SouthDistrict','154'];
Tax13[40]=['NorthDistrict','155'];
Tax13[41]=['Anping District ','156'];
Tax13[42]=['West Central District','157'];

//GaoxiongCity Tax14=[0鄉鎮區,1代號]
Tax14[0]=['GaoxiongCity',''];
Tax14[1]=['SanminDistrict','180'];
Tax14[2]=['XiaogangDistrict','181'];
Tax14[3]=['ZuoyingDistrict','182'];
Tax14[4]=['QianjinDistrict','173'];
Tax14[5]=['QianzhenDistrict','178'];
Tax14[6]=['LingyaDistrict','174'];
Tax14[7]=['XinxingDistrict','172'];
Tax14[8]=['NanziDistrict','177'];
Tax14[9]=['GushanDistrict','176'];
Tax14[10]=['QijinDistrict','176'];
Tax14[11]=['YanchengDistrict','173'];
Tax14[12]=['SanminDistrict','180'];
Tax14[13]=['DasheDistrict','794'];
Tax14[14]=['DaliaoDistrict','796'];
Tax14[15]=['DashuDistrict','795'];
Tax14[16]=['RenwuDistrict','793'];
Tax14[17]=['NeimenDistrict','800'];
Tax14[18]=['LiuguiDistrict','799'];
Tax14[19]=['Yong’anDistrict','809'];
Tax14[20]=['TianliaoDistrict','812'];
Tax14[21]=['JiaxianDistrict','803'];
Tax14[22]=['ShanlinDistrict','804'];
Tax14[23]=['GangshanDistrict','807'];
Tax14[24]=['LinyuanDistrict','797'];
Tax14[25]=['AlianDistrict','813'];
Tax14[26]=['MeinongDistrict','801'];
Tax14[27]=['QiedingDistrict','808'];
Tax14[28]=['MaolinDistrict','804'];
Tax14[29]=['TaoyuanDistrict','805'];
Tax14[30]=['ZiguanDistrict','811'];
Tax14[31]=['Niaosong District','792'];
Tax14[32]=['HuneiDistrict','817'];
Tax14[33]=['LuzhuDistrict','814'];
Tax14[34]=['QishanDistrict','798'];
Tax14[35]=['FengshanDistrict','791'];
Tax14[36]=['QiaotouDistrict','810'];
Tax14[37]=['YanchaoDistrict','815'];
Tax14[38]=['MituoDistrict','816'];
Tax14[39]=['LingyaDistrict','222'];
Tax14[40]=['GushanDistrict','223'];
Tax14[41]=['QijinDistrict','224'];
Tax14[42]=['Niaosong District','858'];
Tax14[43]=['RenwuDistrict','859'];
Tax14[44]=['DasheDistrict','860'];
Tax14[45]=['DashuDistrict','861'];
Tax14[46]=['DaliaoDistrict','862'];
Tax14[47]=['LinyuanDistrict','863'];

//PingdongCounty Tax15=[0鄉鎮區,1代號]
Tax15[0]=['PingdongCounty',''];
Tax15[1]=['JiuruTownship','825'];
Tax15[2]=['SandimenTownship','829'];
Tax15[3]=['NeipuTownship','841'];
Tax15[4]=['ZhutianTownship','842'];
Tax15[5]=['MudanTownship','853'];
Tax15[6]=['ChechengTownship','849'];
Tax15[7]=['LigangTownship','826'];
Tax15[8]=['JiadongTownship','837'];
Tax15[9]=['LaiyiTownship','846'];
Tax15[10]=['FangshanTownship','851'];
Tax15[11]=['FangliaoTownship','844'];
Tax15[12]=['DonggangTownship','832'];
Tax15[13]=['LinbianTownship','835'];
Tax15[14]=['ChangzhiTownship','823'];
Tax15[15]=['NanzhouTownship','836'];
Tax15[16]=['PingdongCity','821'];
Tax15[17]=['HengchunTownship','848'];
Tax15[18]=['ChunriTownship','847'];
Tax15[19]=['KandingTownship','834'];
Tax15[20]=['TaiwuTownship','845'];
Tax15[21]=['LiuqiuTownship','838'];
Tax15[22]=['GaoshuTownship','828'];
Tax15[23]=['XinpiTownship','843'];
Tax15[24]=['XinyuanTownship','833'];
Tax15[25]=['ShiziTownship','852'];
Tax15[26]=['WandanTownship','822'];
Tax15[27]=['WanluanTownship','840'];
Tax15[28]=['ManzhouTownship','850'];
Tax15[29]=['MajiaTownship','831'];
Tax15[30]=['ChaozhouTownship','839'];
Tax15[31]=['Wutai Township','830'];
Tax15[32]=['LinluoTownship','824'];
Tax15[33]=['YanpuTownship','827'];

//YilanCounty Tax16=[0鄉鎮區,1代號]
Tax16[0]=['YilanCounty',''];
Tax16[1]=['SanxingTownship','326'];
Tax16[2]=['DatongTownship','326'];
Tax16[3]=['WujieTownship','326'];
Tax16[4]=['DongshanTownship','326'];
Tax16[5]=['ZhuangweiTownship','321'];
Tax16[6]=['YilanCity','321'];
Tax16[7]=['Nan’aoTownship','326'];
Tax16[8]=['YuanshanTownship','321'];
Tax16[9]=['TouchengTownship','321'];
Tax16[10]=['JiaoxiTownship','321'];
Tax16[11]=['LuodongTownship','326'];
Tax16[12]=['Su’aoTownship','326'];

//HualianCounty Tax17=[0鄉鎮區,1代號]
Tax17[0]=['HualianCounty',''];
Tax17[1]=['YuliTownship','900'];
Tax17[2]=['GuangfuTownship','891'];
Tax17[3]=['Ji’anTownship','891'];
Tax17[4]=['XiulinTownship','891'];
Tax17[5]=['ZhuoxiTownship','900'];
Tax17[6]=['HualianCity','891'];
Tax17[7]=['FuliTownship','900'];
Tax17[8]=['XinchengTownship','891'];
Tax17[9]=['RuisuiTownship','900'];
Tax17[10]=['WanrongTownship','891'];
Tax17[11]=['ShoufengTownship','891'];
Tax17[12]=['FenglinTownship','891'];
Tax17[13]=['FengbinTownship','891'];

//台東縣 Tax18=[0鄉鎮區,1代號]
Tax18[0]=['Taitung County',''];
Tax18[1]=['DawuTownship','925'];
Tax18[2]=['TaimaliTownship','926'];
Tax18[3]=['Taitung City','921'];
Tax18[4]=['ChenggongTownship','922'];
Tax18[5]=['ChishangTownship','930'];
Tax18[6]=['BeinanTownship','924'];
Tax18[7]=['Yanping Township','932'];
Tax18[8]=['DongheTownship','927'];
Tax18[9]=['JinfengTownship','935'];
Tax18[10]=['ChangbinTownship','928'];
Tax18[11]=['HaiduanTownship','933'];
Tax18[12]=['LuyeTownship','929'];
Tax18[13]=['DarenTownship','934'];
Tax18[14]=['LüdaoTownship','931'];
Tax18[15]=['GuanshanTownship','923'];
Tax18[16]=['LanyuTownship','936'];

//JinmenCounty Tax19=[0鄉鎮區,1代號]
Tax19[0]=['JinmenCounty',''];
Tax19[1]=['JinshaTownship','951'];
Tax19[2]=['JinchengTownship','951'];
Tax19[3]=['JinhuTownship','951'];
Tax19[4]=['JinningTownship','951'];
Tax19[5]=['LieyuTownship','951'];
Tax19[6]=['WuqiuTownship','951'];

//PenghuCounty Tax20=[0鄉鎮區,1代號]
Tax20[0]=['PenghuCounty',''];
Tax20[1]=['QimeiTownship','976'];
Tax20[2]=['BaishaTownship','973'];
Tax20[3]=['XiyuTownship','974'];
Tax20[4]=['MagongCity','971'];
Tax20[5]=['Wang’anTownship','975'];
Tax20[6]=['HuxiTownship','972'];

//LianjiangCounty Tax21=[0鄉鎮區,1代號]
Tax21[0]=['LianjiangCounty',''];
Tax21[1]=['Beigan Township','981'];
Tax21[2]=['Dongyin Township','981'];
Tax21[3]=['Nangan Township','981'];
Tax21[4]=['JuguangTownship','981'];

//顯示區域的內容
function ShowTaxDataTitle(){
	if (Tax.length == 0)
		return;
	
	var colCount = 9;	
		
	for (var i=0;i<Tax.length;i++)
	{
		var j = (i + 1) % colCount;
		document.write("<a href=javascript:ShowTable(" + i + ")>" + Tax[i][0][0] + "</a>" + "　");
		if (j == 0) document.write("<br />");
	}
}

function CreateAreaTables()
{
	for (var i=0;i<Tax.length;i++)
		CreateAreaTable(i);
}

function CreateAreaTable(cityIndex){
	var colCount = 5;
	var areaCount = Tax[cityIndex].length;

	document.write("<div id=Tax" + cityIndex + "><table width=100% class=DataBorder>");
	document.write("<tr><th>Tax Identification Country</th><td colspan=" + (colCount - 1) + " class=DataCell>" + Tax[cityIndex][0][0] + "</td></tr>");
	document.write("<tr>");
	for (var i=0;i<colCount;i++) document.write("<th>Unit Code</th>");
	document.write("</tr>");
	for (var i=0;i<colCount;i++) document.write("<col align=center />");	
	
	for (var i=0;i<Tax[cityIndex].length;i+=colCount)
	{
		var j = i;
		document.write("<tr class=Row" + i%2 + ">");
		for (var k=0;k<colCount;k++)
		{
			j++;
			if (j < areaCount) document.write("<td><a href=javascript:fillData('" + Tax[cityIndex][j][1] + "')>" + Tax[cityIndex][j][0] + "_" + Tax[cityIndex][j][1] + "</a></td>"); else	document.write("<td></td>");
		}
		document.write("</tr>");
	}
	
	document.write("</table></div>");
}

function ShowTaxDataContent(id){
document.write("<b><font size=-1>" + Tax[id][0][0] + "Tax Identification Unit Code</font></b>");
document.write("<Table border=0>");
if (Tax[id].length > 10){
  document.write("<TR bgcolor=orange><TD align=center><font size=-1>Township area</font></TD><TD align=center><font size=-1>Code</font></TD><TD align=center><font size=-1>Township area</font></TD><TD align=center><font size=-1>Code</font></TD></TR>");	
	for (i=1;i<Tax[id].length;i=i+2){
		document.write("<TR>");
		document.write("<TD align=center><font size=-1>" + Tax[id][i][0] + "</font></TD>");
		document.write("<TD align=center><font size=-1>" + Tax[id][i][1] + "</font></TD>");
		if (Tax[id].length > (i+1)){
		document.write("<TD align=center><font size=-1>" + Tax[id][i+1][0] + "</font></TD>");
		document.write("<TD align=center><font size=-1>" + Tax[id][i+1][1] + "</font></TD>");
	  }
		document.write("</TR>");
	}
} else {
  document.write("<TR bgcolor=orange><TD align=center><font size=-1>Township area</font></TD><TD align=center><font size=-1>Code</font></TD></TR>");	
	for (i=1;i<Tax[id].length;i++){
		document.write("<TR>");
		document.write("<TD align=center><font size=-1>" + Tax[id][i][0] + "</font></TD>");
		document.write("<TD align=center><font size=-1>" + Tax[id][i][1] + "</font></TD>");
		document.write("</TR>");
	}
}
document.write("</Table>");
}

function ShowTable(cityIndex){
	hideTables();
	document.getElementById("Tax" + cityIndex).style.display = "";
}

function hideTables()
{
	for (var i=0;i<Tax.length;i++)
		document.getElementById("Tax" + i).style.display = "none";
}

Gold =['Taipei City','Taipei City','New TaipeiCity','Keelung City','Yilan County','Taoyuan City','Taoyuan Country','Hsinchu City','Hsinchu County','Miaoli County','Taichung City','Taichung Country','Changhua County','Nantou County ','Yunlin County','Chiayi City','Chiayi County','Tainan City ','Tainan Country','Kaohsiung City','Kaohsiung Country','Pingtung County','Taitung County','Hualien County','Penghu County','Lienchiang County','Kinmen County'];
Goldid =['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0'];
GoldTW =['台北市','台北縣','新北市','基隆市','宜蘭縣','桃園市','桃園縣','新竹市','新竹縣','苗栗縣','台中市','台中縣','彰化縣','南投縣','雲林縣','嘉義市','嘉義縣','台南市','台南縣','高雄市','高雄縣','屏東縣','台東縣','花蓮縣','澎湖縣','連江縣','金門縣'];
function AddCityOptions(oSelect)
{
	for (i=0;i<Gold.length;i++)
	{
		var city = Gold[i];
		oSelect.options.add(new Option(city, city));
	}
}
function AddCityOptions1(oSelect)
{
	for (i=0;i<Gold.length;i++)
	{
		
		var city = Gold[i];
		var cityvalue = Goldid[i];
		oSelect.options.add(new Option(city, cityvalue));
	}
}
function AddCityOptionsTW(oSelect)
{
	for (i=0;i<Gold.length;i++)
	{
		
		var city = Gold[i];
		var cityvalue = GoldTW[i];
		oSelect.options.add(new Option(city, cityvalue));
	}
}
