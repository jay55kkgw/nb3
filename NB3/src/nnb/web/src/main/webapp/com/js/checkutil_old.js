function CheckRadio(name, radioName) {
	if ($("input[name=" + radioName + "]:checked").val() == null) {
		//alert("請點選" + name);
		errorBlock(
				null, 
				null,
				["請點選" + name], 
				'離開', 
				null
			);
		return false;
	}
	return true;
}
function CheckNumber(ID, name, canEmpty) {
	var value = $("#" + ID).val();
	if (window.console) {
		console.log("value=" + value);
	}

	if (canEmpty == false && value == "") {
		//alert("請輸入" + name);
		errorBlock(
				null, 
				null,
				["請輸入" + name], 
				'離開', 
				null
			);
		return false;
	}
	var regex = /[^0-9]/;
	if (regex.test(value)) {
		//alert(name + "欄位只能輸入數字（0～9）");
		errorBlock(
				null, 
				null,
				[name + "欄位只能輸入數字（0～9）"], 
				'離開', 
				null
			);

		return false;
	}
	return true;
}

function CheckAmount(ID, name, MIN, MAX) {
	if (MIN != null && MAX != null && MIN > MAX) {
//		alert("CheckAmount的參數MIN不能大於MAX");
		errorBlock(
				null, 
				null,
				["CheckAmount的參數MIN不能大於MAX"], 
				'離開', 
				null
			);
		return false;
	}
	var amount = $("#" + ID).val();

	if (amount == "") {
		//alert("請輸入" + name);
		errorBlock(
				null, 
				null,
				["請輸入" + name], 
				'離開', 
				null
			);
		return false;
	}
	var regex = /[^0-9]/;
	if (regex.test(amount)) {
		//alert(name + "欄位請輸入正確的金額格式");
		errorBlock(
				null, 
				null,
				[name + "欄位請輸入正確的金額格式"], 
				'離開', 
				null
			);
		return false;
	}
	if (amount.charAt(0) == "0" && amount.length > 1) {
		//alert(name + "欄位請勿以零為開頭");
		errorBlock(
				null, 
				null,
				[name + "欄位請勿以零為開頭"], 
				'離開', 
				null
			);
		return false;
	}
	if (MIN != null && amount < MIN) {
		//alert(name + "不能小於" + MIN);
		errorBlock(
				null, 
				null,
				[name + "不能小於" + MIN], 
				'離開', 
				null
			);
		return false;
	}
	if (MAX != null && amount > MAX) {
		//alert(name + "不能大於" + MAX);
		errorBlock(
				null, 
				null,
				[name + "不能大於" + MAX], 
				'離開', 
				null
			);
		return false;
	}
	return true;
}

function CheckPuzzle(ID) {
	var value = $("#" + ID).val();
	 var reNumber = /[^a-z^A-Z^0-9]/g;

	if (value.length < 6 || value.length > 8 || reNumber.test(value)) {
		//alert("交易密碼欄位請輸入6-8位英數字");
		errorBlock(
				null, 
				null,
				["交易密碼欄位請輸入6-8位英數字"], 
				'離開', 
				null
			);
		return false;
	}

	return true;
}

function CheckSelect(ID, name, notSelectValue) {
	var selectValue = $("#" + ID + " option:selected").val();

	if (selectValue == notSelectValue) {
		//alert("請選擇" + name);
		errorBlock(
				null, 
				null,
				["請選擇" + name], 
				'離開', 
				null
			);
		return false;
	}
	return true;
}

function checkEmail(ID, name) {
	var rexpress = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

	var value = $("#" + ID).val();

	if (!rexpress.test(value)) {
		//alert(name + "格式有誤");
		errorBlock(
				null, 
				null,
				[name + "格式有誤"], 
				'離開', 
				null
			);
		return false;
	} else {
		return true;
	}
}

function isChinese(value) {
	if(value.length == 0){
		return false;
	}

	for(i=0;i<value.length;i++) {
		if(value.charCodeAt(i) > 128){
			return true;
		}
	}
	return   false;
}
function checkCUSIDN(CUSIDN){
	if(CUSIDN != ""){
		if(CUSIDN.length == 8 || CUSIDN.length == 10){
			if(CUSIDN.length == 8){
				if(!TaxID(CUSIDN)){
					//alert("身分證或統一編號輸入有錯，請重新輸入。");
					errorBlock(
							null, 
							null,
							["身分證或統一編號輸入有錯，請重新輸入。"], 
							'離開', 
							null
						);

		         	 return false;
				}
				else{
					return true;
				}
			}
			if(CUSIDN.length==10){
				var reType1 = /[A-Z]{1}\d{9}/;
				var reType2 = /[A-Z]{2}\d{8}/;
				var reType3 = /\d{8}[A-Z]{2}/;      	
		      	
				if(reType1.test(CUSIDN)){
					if(!checkID(CUSIDN)){
						//alert("身分證或統一編號輸入有錯，請重新輸入。");
						errorBlock(
								null, 
								null,
								["身分證或統一編號輸入有錯，請重新輸入。"], 
								'離開', 
								null
							);

						return false;
					}
					else{
						return true;
					}
				}        
				else if(reType2.test(CUSIDN)){
					if(!checkID2(CUSIDN)){
						//alert("身分證或統一編號輸入有錯，請重新輸入。");
						errorBlock(
								null, 
								null,
								["身分證或統一編號輸入有錯，請重新輸入。"], 
								'離開', 
								null
							);

						return false;
					}
					else{
						return true;
					}					 					         		
				}       
				else if(reType3.test(CUSIDN)){
					return true;					 					
				}  	         		        
				else{
					//alert("身分證或統一編號輸入有錯，請重新輸入。");
					errorBlock(
							null, 
							null,
							["身分證或統一編號輸入有錯，請重新輸入。"], 
							'離開', 
							null
						);

					return false;
				}
			}
		}
		else{
			//alert("身分證或統一編號輸入有錯，請重新輸入。");
			errorBlock(
					null, 
					null,
					["身分證或統一編號輸入有錯，請重新輸入。"], 
					'離開', 
					null
				);

			return false;
		}
	}
}
//統一編號檢查
function TaxID(tax){
	var i,a1,a2,a3,a4,a5;
	var b1,b2,b3,b4,b5;
	var c1,c2,c3,c4;
	var d1,d2,d3,d4,d5,d6,d7,cd8;
	if(tax.length != 8){
		return false;
	}
	var c; 
	for(i=0;i<8;i++){
		c = tax.charAt(i);
		if("0123456789".indexOf(c) == -1){
			return false;
		}
	} 
	d1 = parseInt(tax.charAt(0));
	d2 = parseInt(tax.charAt(1));
	d3 = parseInt(tax.charAt(2));
	d4 = parseInt(tax.charAt(3));
	d5 = parseInt(tax.charAt(4));
	d6 = parseInt(tax.charAt(5));
	d7 = parseInt(tax.charAt(6));
	cd8 = parseInt(tax.charAt(7));
	c1 = d1; 
	c2 = d3; 
	c3 = d5; 
	c4 = cd8; 
	a1 = parseInt((d2 * 2) / 10); 
	b1 = (d2 * 2) % 10; 
	a2 = parseInt((d4 * 2) / 10); 
	b2 = (d4 * 2) % 10; 
	a3 = parseInt((d6 * 2) / 10); 
	b3 = (d6 * 2) % 10; 
	a4 = parseInt((d7 * 4) / 10); 
	b4 = (d7 * 4) % 10; 
	a5 = parseInt((a4 + b4) / 10); 
	b5 = (a4 + b4) % 10; 
	if((a1 + b1 + c1 + a2 + b2 + c2 + a3 + b3 + c3 + a4 + b4 + c4) % 5 == 0){
		return true;
	}
	if(d7 = 7){
		if((a1 + b1 + c1 + a2 + b2 + c2 + a3 + b3 + c3 + a5 + c4) % 5 == 0){
			return true;
		}
	}
 return false;
}
//身份證字號檢查
function checkID(ID){
	//英文字母轉大寫
	ID = ID.toUpperCase();
	var tab = "ABCDEFGHJKLMNPQRSTUVXYWZIO";
	var A1 = new Array(1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3);
	var A2 = new Array(0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5);
	var Mx = new Array(9,8,7,6,5,4,3,2,1,1);

	if(ID.length != 10 ){
		return false;
	}
	var i = tab.indexOf(ID.charAt(0));
	if(i == -1){
		return false;
	}
	var sum = A1[i] + A2[i] * 9;

	for(i=1;i<10;i++){
		var v = parseInt(ID.charAt(i));
		if(isNaN(v)){
			return false;
		}
		sum = sum + v * Mx[i];
	}
	if(sum % 10 != 0){
		return false;
	}
   return true;
}
//外僑居留證字號檢查
function checkID2(ID){
	//英文字母轉大寫
	ID = ID.toUpperCase();
	var tab = "ABCDEFGHJKLMNPQRSTUVWXYZIO";
	var A1 = new Array(1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3);
	var A2 = new Array(0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5);
	var Mx = new Array(9,8,7,6,5,4,3,2,1,1);

	var i1 = tab.indexOf(ID.charAt(0));
	var i2 = tab.indexOf(ID.charAt(1));
      
	var front_1 = A1[i1] * 1 + A2[i1] * 9;
	var front_2 = A2[i2] * 8;
	var sum = front_1 + front_2;

	for(i=2;i<10;i++){
		var v = parseInt(ID.charAt(i));
		if(isNaN(v)){
			return false;
		}
		sum = sum + v * Mx[i];
	}
	if(sum % 10 != 0){
		return false;
	}
	return true;
}