/*
 * 检核指定名稱的Radio物件是否被點選
 * PARAM sFieldName:欄位中文描述
 * PARAM sRadioName:Radio物件名稱
 * return 
 * validate[funcCall[validate_Radio['sFieldName',sRadioName]]]
 */
function validate_Radio(field, rules, i, options) {
	
	console.log("_one_checkRadio");
	
	var sFieldName = rules[i + 2];
	var sRadioName = rules[i + 3];

	if (typeof (sFieldName) != "string") {
		return "* CheckRadio含有无效的参数sFieldName";
	}

	if (typeof (sRadioName) != "string") {
		return "* CheckRadio含有无效的参数sRadioName";
	}

	try {
		var aRadios = document.getElementsByName(sRadioName);

		if (aRadios.length == 0) {
			return "没有可点选的" + sFieldName;
		}

		var bHasChecked = false;
		for (var i = 0; i < aRadios.length; i++) {
			if (aRadios[i].checked) {
				bHasChecked = true;
				break;
			}
		}

		if (!bHasChecked) {
			return "* 请点选" + sFieldName;
		}
	} catch (exception) {
		//alert("检核" + sFieldName + "时发生错误:" + exception);
		errorBlock(
				null, 
				null,
				["检核" + sFieldName + "时发生错误:" + exception], 
				'离开', 
				null
			);
	}
}

/*
 * 檢查數字輸入框內容(不含加號、減號、小數點)
 * PARAM sFieldName:欄位中文描述
 * PARAM oField:數字輸入框
 * return 
 * validate[required,funcCall[validate_CheckNumber['sFieldName',oField]]]
 */
function validate_CheckNumber(field, rules, i, options) {
	
	console.log("_CheckNumber");
	
	var sFieldName = rules[i + 2];
	var oField = rules[i + 3];
	
	if (typeof (sFieldName) != "string") {
		return "* CheckNumber含有无效的参数sFieldName";
	}
    /*if (window.console) {
		console.log("_CheckNumber");
	}*/
	try {
		var Numbervalue = $("#" + oField).val();
		var regex = /[^0-9]/;
		var valid = regex.test(Numbervalue);
		if (valid) {
			return options.allrules.onlyNumberSp.alertText;
		}
	} catch (exception) {
		//alert("检核" + sFieldName + "时发生错误:" + exception);
		errorBlock(
				null, 
				null,
				["检核" + sFieldName + "时发生错误:" + exception], 
				'离开', 
				null
			);
	}
}

/*
 * 檢查金額輸入框內容
 * PARAM sFieldName:欄位中文描述
 * PARAM bIsInteger:是否整數
 * PARAM fMin:最小值，不限制時給null
 * PARAM fMin:最大值，不限制時給null
 * return 
 * 檢查金額輸入框內容 PARAM ID:金額輸入框 PARAM Min:最小值 PARAM Max:最大值 return
 * validate[required,min[fMin],max[fMin],funcCall[validate_CheckAmount[ 'sFieldName' , oField]]]
 */
function boolIsInteger(i)
{
	return i%1 == 0
}
function validate_CheckAmount(field, rules, i, options) {
	
	console.log("_CheckAmount");
	
	var sFieldName = rules[i + 2];
	var oField = rules[i + 3];
	
	if (typeof (sFieldName) != "string") {
		return "* CheckAmount含有无效的参数sFieldName";
	}
			
	try {
		
		var sAmount = $("#" + oField).val();
		bIsInteger = boolIsInteger(sAmount);
		
		
		if ( isNaN(sAmount) && sAmount != "") {//數字
			//return "*　"+ sFieldName + "欄位請輸入正確的金額格式";
			return "请输入正确的金额格式";
		}
		
		//先TRIM掉，不要用startsWith與endsWith，在舊版瀏覽器會出錯
		//TODO:疑似用法錯誤，先改成下面方式
//		sAmount = sAmount.trim();
		sAmount = $.trim(sAmount);
		/*if (sAmount == "") {
			return "*　"+ sFieldName + "栏位请输入正确的金额格式";
		}*/
		
		if (sFieldName.indexOf("+") == 0) {
			return "* "+ sFieldName + "栏位请勿输入加号";
		}
		
		if (sAmount.charAt(0) == "0" && sAmount.length > 1) {//check不以零為開頭
			return "* "+ sFieldName + "栏位请勿以零为开头";
		}

		if (bIsInteger == true && sAmount.indexOf(".") != -1) {
			return "* "+ sFieldName + "栏位请输入整数";
		}

		if (bIsInteger == false && sAmount.indexOf(".") == sAmount.length - 1) {
			return "* "+ sFieldName + "栏位请输入正确的含小数点金额格式";
		}

	} catch (exception) {
		//alert("检核" + sFieldName + "时发生错误:" + exception);
		errorBlock(
				null, 
				null,
				["检核" + sFieldName + "时发生错误:" + exception], 
				'离开', 
				null
			);
	}

}
/*
 * 檢查交易密碼
 * PARAM oField:交易密碼輸入框
 * return  
 * validate[required,funcCall[validate_CheckTxnPassword[oField]]]
 */
//交易密碼舊的沒有驗英數字，但現在交易密碼有英數字
//function validate_CheckTxnPassword(field, rules, i, options) {
//	
//	console.log("_CheckTxnPassword");
//
//	var oField = rules[i + 2];
//	
//	try {
//		var sValue =  $("#" + oField).val();
//		var regex = /[^0-9]/;
//
//		if (sValue.length < 6 || sValue.length > 8 || regex.test(sValue)) {
//			return "* 交易密码栏位请输入6-8位数字";
//
//		}
//	} 
//	catch (exception) {
//		alert("检核" + sFieldName + "时发生错误:" + exception);
//	}
//
//}

/*
 * 檢查交易密碼
 * PARAM oField:交易密碼輸入框
 * return  
 * validate[required,funcCall[validate_CheckTxnNewPassword[oField]]]
 */

function validate_CheckTxnNewPassword(field, rules, i, options) {
	
	console.log("_CheckTxnNewPassword");

	var oField = rules[i + 2];
	
	try {
		var sValue =  $("#" + oField).val();
		var regex = /[^a-z^A-Z^0-9]/g;

		if (sValue.length < 6 || sValue.length > 8 || regex.test(sValue)) {
			return "* 交易密码栏位请输入6-8位英数字";

		}
	} 
	catch (exception) {
		//alert("检核" + sFieldName + "时发生错误:" + exception);
		errorBlock(
				null, 
				null,
				["检核" + sFieldName + "时发生错误:" + exception], 
				'离开', 
				null
			);
	}

}

/*
 * 检核Select物件
 * PARAM sFieldName:欄位中文描述
 * PARAM aSelect:Select物件
 * PARAM sDoNotSelect:不能為選取值的字串，無限制時給null
 * return
 * validate[funcCall[validate_CheckSelect['sFieldName',aSelect,sDoNotSelect]]]
 */
function validate_CheckSelect(field, rules, i, options) {
	
	console.log("_CheckSelect");
	
	var sFieldName = rules[i + 2];
	var aSelect = rules[i + 3];
	var sDoNotSelect = rules[i + 4];
	
	if (typeof (sFieldName) != "string") {
		return "* CheckSelect含有无效的参数sFieldName";
	}
	
	try {
		
		var aSelected = document.getElementById(aSelect);
		var selectValue = $("#" + aSelect + " option:selected").val();
		
		if (aSelected.length == 0 || (aSelected.length == 1 && aSelected[0].value == sDoNotSelect)) {
			return "* 没有可选择的" + sFieldName;
		}
		
		if (typeof (sDoNotSelect) == "string") {
			var bHasSelected = false;

			for (var i = 0; i < aSelected.length; i++) {
				var o = aSelected[i];
				if (o.selected) {
					bHasSelected = true;

					if (o.value == sDoNotSelect) {
						return "* 请选择" + sFieldName;
					}
				}
			}

			// 當所有的項目皆未選取時，比較第一個項目
			if (bHasSelected == false && aSelected.length > 0) {
				if (aSelected[0].value == sDoNotSelect) {
					return "* 请选择" + sFieldName;
				}
			}
		}
	} catch (exception) {
		//alert("检核" + sFieldName + "时发生错误:" + exception)
		errorBlock(
				null, 
				null,
				["检核" + sFieldName + "时发生错误:" + exception], 
				'离开', 
				null
			);
	}
}

/*
 * 繳稅功能 : 隱藏欄位驗證之錯誤訊息不適合於表單顯示故用alert
 * PARAM sFieldName:欄位中文描述
 * PARAM aSelect:Select物件
 * PARAM sDoNotSelect:不能為選取值的字串，無限制時給null
 * return
 */
function CheckSelect(sFieldName, aSelect, sDoNotSelect)
{   
 
	if  (typeof(sFieldName) != "string")
	{
		//alert("CheckSelect含有无效的参数sFieldName");
		errorBlock(
				null, 
				null,
				["CheckSelect含有无效的参数sFieldName"], 
				'离开', 
				null
			);
		return false;
	}
	
	try
	{	
		if (aSelect.length == 0 ||
			(aSelect.length == 1 && aSelect[0].value == sDoNotSelect))
		{
			//alert("没有可选择的" + sFieldName);
			errorBlock(
					null, 
					null,
					["没有可选择的" + sFieldName], 
					'离开', 
					null
				);
			return false;
		}
		
		if (typeof(sDoNotSelect) == "string")
		{	
			var bHasSelected = false;
		
			for (var i = 0; i < aSelect.length; i++)
			{
				var o = aSelect[i];
				if (o.selected)
				{
					bHasSelected = true;
					
					if (o.value == sDoNotSelect)
					{
						//alert("请选择" + sFieldName);
						errorBlock(
								null, 
								null,
								["请选择" + sFieldName], 
								'离开', 
								null
							);
						return false;
					}
				}
			}
			
			//當所有的項目皆未選取時，比較第一個項目
			if (bHasSelected == false && aSelect.length > 0)
			{
				if (aSelect[0].value == sDoNotSelect)
				{
					//alert("请选择" + sFieldName);
					errorBlock(
							null, 
							null,
							["请选择" + sFieldName], 
							'离开', 
							null
						);
					return false;
				}					
			}
		}
	}
	catch (exception)
	{
		//alert("检核" + sFieldName + "时发生错误:" + exception)
		errorBlock(
				null, 
				null,
				["检核" + sFieldName + "时发生错误:" + exception], 
				'离开', 
				null
			);
		return false;
	}
	return true;
}

function IsValidDate(sText)
{
///<summary>檢查指定文字是否符合1900~2099年間的"4碼年/2碼月/2碼日"日期格式</summary>
///<param name="sText">待檢查的字串</param>
///<returns>true:检核成功 false:检核失敗</returns>

	//先檢查字串是否符合格式
	var reDate = /(19\d{2}|20\d{2}|21\d{2})\/(0[1-9]|1[0-2])\/(0[1-9]|[12][0-9]|3[01])/;
	var result = reDate.test(sText);
	if (result == false)
		return result;
		
	//再檢查是否有符合格式卻不符合真正日期的錯誤，如2008/02/31
	var sYear = RegExp.$1;
	var sMonth = RegExp.$2;
	var sDate = RegExp.$3;
	
	var dDate = new Date(sText); //如字串"2008/02/31"轉成Date物件，月份會變3月，日期會變2號

	var iYear = dDate.getFullYear();
	var iMonth = dDate.getMonth() + 1;
	var iDate = dDate.getDate();
	
	if (sYear != iYear || sMonth != iMonth || sDate != iDate)
		return  false;
		
	return true;
}

function CyearToEyear(sCyear)
{
///<summary>轉換民國年(097)至西元年(2008)</summary>
///<param name="sCyear">前三碼為民國年之字串</param>
///<returns>前四碼為西元年之字串</returns>
	//sCyear="097/01/31";
	//傳入字串應先通過驗證符合三碼民國年格式
	var year = parseInt(sCyear.substring(0, 3), 10) + 1911;
	return year + sCyear.substring(3);
}
/*
 * 檢查日期輸入框內容
 * PARAM sFieldName:欄位中文描述
 * PARAM oDate:要检核的日期輸入框物件
 * PARAM sMinDate:最小日期字串，無最小日限制時給null
 * PARAM sMaxDate:最大日期字串，無最大日限制時給null
 * return
 * validate[funcCall[validate_CheckDate['sFieldName',oDate,sMinDate,sMaxDate]]]
 */
function validate_CheckDate(field, rules, i, options) {

	console.log("_CheckDate");
	
	var sFieldName = rules[i + 2];
	var oDate=rules[i + 3];
	var sMinDate=rules[i + 4];
	var sMaxDate=rules[i + 5];
	/**
	 * 因為用 validate 參數傳入為字串,必須轉型正確的型別
	 */
	var minDate = "null" === sMinDate ? null: sMinDate;
	var maxDate = "null" === sMaxDate ? null: sMaxDate;

	if (typeof (sFieldName) != "string") {
		return "* CheckDate含有无效的参数sFieldName";
	}

	// 檢查最小日參數
	var dMinDate = null;
	if (minDate != null) {
		if (IsValidDate(minDate)) {
			dMinDate = new Date(minDate);
		} else {
			return "* CheckDate含有无效的参数sMinDate";
		}
	}

	// 檢查最大日參數
	var dMaxDate = null;
	if (maxDate != null) {
		if (IsValidDate(maxDate)) {
			dMaxDate = new Date(maxDate);
		} else {
			return "* CheckDate含有无效的参数sMaxDate";
		}
	}

	if (dMinDate != null && dMaxDate != null && dMinDate > dMaxDate) {
		return "* CheckDate的参数sMinDate不能大于sMaxDate";
	}
	
	var ovalue = $("#" + oDate).val();
	
	try {
		if (!IsValidDate(ovalue)) {
			return "* 请输入正确的" + sFieldName + "格式，例如 2009/01/31";
		}
		var dDate = new Date(ovalue);

		if (dMaxDate != null && dDate > dMaxDate) {
			return "* "+ sFieldName + "不能大于" + maxDate;
		}

		if (dMinDate != null && dDate < dMinDate) {
			return "* "+sFieldName + "不能小于 " + minDate;
		}
	} catch (exception) {
		return "* 检核" + sFieldName + "时发生错误:" + exception;
	}
}


/*
 * 檢查民國日期輸入框內容
 * PARAM sFieldName:欄位中文描述
 * PARAM oDate:要检核的日期輸入框物件
 * PARAM sMinDate:最小日期字串，無最小日限制時給null
 * PARAM sMaxDate:最大日期字串，無最大日限制時給null
 * return 
 * validate[funcCall[validate_CheckCDate['sFieldName',oDate,sMinDate,sMaxDate]]]
 */
function validate_CheckCDate(field, rules, i, options) {

	console.log("_CheckCDate");
	
	var sFieldName = rules[i + 2];
	var oDate=rules[i + 3];
	var sMinDate=rules[i + 4];
	var sMaxDate=rules[i + 5];
	
	// 檢查是否為字串
	if (typeof (sFieldName) != "string") {
		return "* CheckCDate含有无效的参数sFieldName";
	}

	// 檢查最小日參數
	var dMinDate = null;
	if (sMinDate != null) {
		var min = CyearToEyear(sMinDate);
		
		if (IsValidDate(min)) {
			dMinDate = new Date(min);
		} else {
			return "* CheckCDate含有无效的参数sMinDate";
		}
	}

	// 檢查最大日參數
	var dMaxDate = null;
	if (sMaxDate != null) {
		var max = CyearToEyear(sMaxDate);
		if (IsValidDate(max)) {
			dMaxDate = new Date(max);
		} else {
			return "* CheckCDate含有无效的参数sMaxDate";
		}
	}
 
	if (dMinDate != null && dMaxDate != null && dMinDate > dMaxDate) {
		return "* CheckCDate的参数sMinDate不能大于sMaxDate";
	}
	
	var ovalue = $("#" + oDate).val();
	
	try {
		var sDate = CyearToEyear(ovalue);

		if (!IsValidDate(sDate)) {
			return "* 请输入正确的" + sFieldName + "格式，例如 097/01/31";
		}
		var dDate = new Date(sDate);

		if (dMaxDate != null && dDate > dMaxDate) {
			return "* " +sFieldName + "不能大于" + sMaxDate;
		}

		if (dMinDate != null && dDate < dMinDate) {
			return "* " +sFieldName + "不能小于 " + sMinDate;
		}
	} catch (exception) {
		return "*检核" + sFieldName + "时发生错误:" + exception;
	}

}


/*
 * 檢查數字輸入框內容(不含加號、減號、小數點),並依指定長度作检核
 * PARAM sFieldName:欄位中文描述
 * PARAM oField:數字輸入框
 * PARAM len:最小值，長度限制
 * return 
 * validate[required,funcCall[validate_CheckNumWithDigit['sFieldName',oField,len]]]
 */
function validate_CheckNumWithDigit(field, rules, i, options) {

	console.log("_CheckNumWithDigit");
	
	var sFieldName = rules[i + 2];
	var oField=rules[i + 3];
	var len=rules[i + 4];
	
	if (typeof (sFieldName) != "string") {
		return "* CheckNumWithDigit含有无效的参数sFieldName";
	}

	try {
		var sNumber = $("#" + oField).val();;

		var reNumber = /[^0-9]/;
		if (reNumber.test(sNumber)) {//僅能輸入數字
			return "* " + sFieldName + "栏位只能输入数字(0~9)";
		}

		if (sNumber.length != len) {
			return "* " + sFieldName + "栏位应为" + len + "位数字";
		}
	} catch (exception) {
		return "* 检核" + sFieldName + "时发生错误:" + exception;
	}

}
/*
 * 檢查數字輸入框內容(不含加號、減號、小數點)可否為空字串
 * PARAM sFieldName:欄位中文描述
 * PARAM oField:數字輸入框
 * PARAM bCanEmpty:可否為空
 * return 
 * validate[funcCallRequired[validate_CheckNumWithDigit2['sFieldName',oField,bCanEmpty]]]
 */
function validate_CheckNumWithDigit2(field, rules, i, options) {
	console.log("_CheckNumWithDigit2");
	var sFieldName = rules[i + 2];
	var oField=rules[i + 3];
	var bCanEmpty=rules[i + 4];
	if (typeof (sFieldName) != "string") {
		return "* CheckNumWithDigit含有无效的参数sFieldName";
	}
	try {
		var sNumber = $("#" + oField).val();;
    	if (bCanEmpty == false && sNumber == ""){
        	return "*请输入" + sFieldName;
		} 
		var reNumber = /[^0-9]/;
		if (reNumber.test(sNumber)) {//僅能輸入數字
			return "* " + sFieldName + "栏位只能输入数字(0~9)";
		}

	} catch (exception) {
		return "* 检核" + sFieldName + "时发生错误:" + exception;
	}

}

/*
 * 檢查日期輸入框內容
 * PARAM oField:eMAIL輸入欄位
 * return 
 * validate[required,funcCall[validate_EmailCheck[oField]]]
 */
function validate_EmailCheck(field, rules, i, options) {
	
	console.log("_EmailCheck");
	
//	var oField = rules[i + 2];
//	var rexpress = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
//	var value = $("#" + oField).val();
//
//	if (!rexpress.test(value)) {
//		return options.allrules.email.alertText;
//	}
	
	// 多組email會以分號分隔，原本的驗證機制會驗不過
	var oField = rules[i + 2];
	var rexpress = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$/;
	var value = $("#" + oField).val();
	var emails = value.split(';');

	var valid = true;
	
    for(var i in emails) {
        value = $.trim(emails[i]);
    	if (!rexpress.test(value)) {
			return options.allrules.email.alertText;
		}
    }
	
}
/*
 * 检核日期範圍
 * PARAM sFieldName:欄位中文描述
 * PARAM oBaseDate:基準日期，可為輸入框物件或日期物件或日期字串
 * PARAM oStartDate:起始日輸入框物件
 * PARAM oEndDate:終止日輸入框物件
 * PARAM bCanOverBaseDate:起始日或終止日可否大於基準日
 * PARAM iMonthAgo:以基準日往前推算之月份，無限制時給null
 * PARAM iMonthScope:起始日與終止日之相距月份限制，無月份限制時給null
 * return 
 * validate[required,funcCall[validate_CheckDateScope['sFieldName', oBaseDate, oStartDate, oEndDate, bCanOverBaseDate, iMonthAgo, iMonthScope]]]
 */
function validate_CheckDateScope(field, rules, i, options)
{	
	console.log("_CheckDateScope");
	
	var sFieldName = rules[i + 2];
	var oBaseDate = rules[i + 3];
	var oStartDate = rules[i + 4];
	var oEndDate = rules[i + 5];
	/**
	 * 因為用 validate 參數傳入為字串,必須轉型正確的型別
	 */
	// 判斷是不是true
	var bCanOverBaseDate = 'true'===rules[i + 6];
	//傳入為"null"字串 則回傳 null 不是則轉型 Number 
	var iMonthAgoStr = rules[i + 7];
	var	iMonthAgo = "null" === iMonthAgoStr ? null: Number(iMonthAgoStr);
	//傳入為"null"字串 則回傳 null 不是則轉型 Number 
	var iMonthScopeStr = rules[i + 8];
	var	iMonthScope = "null" === iMonthScopeStr ? null: Number(iMonthScopeStr);
	
	if  (typeof(sFieldName) != "string")
	{
		return "* CheckDateScope含有无效的参数sFieldName";
	}
	
	try
	{
		oBaseDatevalue = $("#" + oBaseDate).val();
		//基準日
		var dBaseDate;
		if (oBaseDate instanceof Date)
		{//日期物件
			dBaseDate = oBaseDatevalue;
		}
		else if (typeof(oBaseDatevalue) == "string")
		{//日期字串，如2008/08/01
			if (!IsValidDate(oBaseDatevalue))
			{
				return "* 请使用正确的基准日参数，例如 2009/01/31";
			}
			dBaseDate = new Date(oBaseDatevalue);
		}
		else
		{//輸入框物件
			if (!IsValidDate(oBaseDatevalue))
			{
				return "* 请输入正确的基准日，例如 2009/01/31";
			}
			dBaseDate = new Date(oBaseDatevalue);
		}
		
		var oBaseYear = dBaseDate.getFullYear().toString();
		var oBaseMonth = dBaseDate.getMonth() + 1;
		oBaseMonth = oBaseMonth < 10 ? "0" + oBaseMonth : oBaseMonth.toString();
		var oBaseDay = dBaseDate.getDate();
		oBaseDay = oBaseDay < 10 ? "0" + oBaseDay : oBaseDay.toString();
		sBaseDate = oBaseYear + "/" + oBaseMonth + "/" + oBaseDay;
		
		//最小日
		var dMinDate = null;
		var sMinDate = null;
		if (iMonthAgo != null)
		{
		    var oMinMonth = null;
		    if (oBaseMonth > iMonthAgo)
		        oMinMonth = oBaseMonth - iMonthAgo - 1;
		    else
		        oMinMonth = -1 - (iMonthAgo - oBaseMonth);
		        
		    dMinDate = new Date(sBaseDate);
		    dMinDate.setMonth(oMinMonth);
		    dMinDate.setDate(1);
		    
		    oMinMonth = dMinDate.getMonth() + 1;
		    oMinMonth = oMinMonth < 10 ? "0" + oMinMonth : oMinMonth.toString();
		
			sMinDate = dMinDate.getFullYear() + "/" + oMinMonth + "/01";
		}
			
		oStartDatevalue = $("#" + oStartDate).val();
		//起始日
		if (!IsValidDate(oStartDatevalue))
		{
			return "* 请输入正确的起始日格式，例如 2009/01/31";
		}
		var dStartDate = new Date(oStartDatevalue);
		var oStartYear = dStartDate.getFullYear().toString();
		var oStartMonth = dStartDate.getMonth() + 1;
		oStartMonth = oStartMonth < 10 ? "0" + oStartMonth : oStartMonth.toString();
		var oStartDay = dStartDate.getDate();
		var iStartDay = oStartDay;
		oStartDay = oStartDay < 10 ? "0" + oStartDay : oStartDay.toString();

		if (!bCanOverBaseDate && dStartDate > dBaseDate)
		{
			return "* 起始日不能大于 " + sBaseDate;
		}
		
		if (dMinDate != null && dStartDate < dMinDate)
		{
			return "* 起始日不能小于 " + sMinDate;
		}
		oEndDatevalue = $("#" + oEndDate).val();
		//終止日
		if (!IsValidDate(oEndDatevalue))
		{
			return "* 请输入正确的终止日格式，例如 2009/01/31";
		}
		var dEndDate = new Date(oEndDatevalue);
		var oEndYear = dEndDate.getFullYear().toString();
		var oEndMonth = dEndDate.getMonth() + 1;
		oEndMonth = oEndMonth < 10 ? "0" + oEndMonth : oEndMonth.toString();
		var oEndDay = dEndDate.getDate();
		oEndDay = oEndDay < 10 ? "0" + oEndDay : oEndDay.toString();
		
		//最大日
		var dMaxDate = null;
		var sMaxDate = null;
		if (iMonthScope != null)
		{
			if (iMonthScope == iMonthAgo)
			{
				dMaxDate = dBaseDate;
				sMaxDate = sBaseDate;
			}
			else
			{
				oMaxMonth = dStartDate.getMonth() + 1 + iMonthScope;
				if (oMaxMonth > 12)
				{
					var i = parseInt(oMaxMonth / 12);
					oMaxMonth = oMaxMonth - 12 * i;
					if (oMaxMonth == 0)
					{
						oMaxMonth = 12;
						i = i - 1;
					}
					oMaxMonth = oMaxMonth < 10 ? "0" + oMaxMonth : oMaxMonth.toString();
					sMaxDate = (parseInt(oStartYear) + i) + "/" + oMaxMonth + "/"  //尚未加入day
				}else
				{
					oMaxMonth = oMaxMonth < 10 ? "0" + oMaxMonth : oMaxMonth.toString();
					sMaxDate = oStartYear + "/" + oMaxMonth + "/"    //尚未加入day
				}
				
				for (var i = parseInt(iStartDay); i > 0; i--)
				{//防止出現如2008/2/31之日期
					var oDay = i < 10 ? "0" + i.toString() : i.toString();
					var sDate = sMaxDate + oDay;
					if (IsValidDate(sDate))
					{
						sMaxDate = sDate;
						dMaxDate = new Date(sMaxDate);
						break;
					}
				}
			}
		}
		
		if (!bCanOverBaseDate && (dMaxDate == null || dMaxDate > dBaseDate))
		{//最大日不能超過基準日
			sMaxDate = sBaseDate;
			dMaxDate = dBaseDate;
		}

		if (dMaxDate != null && dEndDate > dMaxDate)
		{
			return "* 终止日不能大于 " + sMaxDate;
		}
		
		if (dEndDate < dStartDate)
		{
			return "* 终止日不能小于起始日";
		}
	}
	catch (exception)
	{
		//alert("检核" + sFieldName + "时发生错误:" + exception);
		errorBlock(
				null, 
				null,
				["检核" + sFieldName + "时发生错误:" + exception], 
				'离开', 
				null
			);
	}
}

/*
* 檢核日期範圍(alert)
* PARAM sFieldName:欄位中文描述
* PARAM oBaseDate:基準日期，可為輸入框物件或日期物件或日期字串
* PARAM oStartDate:起始日輸入框物件
* PARAM oEndDate:終止日輸入框物件
* PARAM bCanOverBaseDate:起始日或終止日可否大於基準日
* PARAM iMonthAgo:以基準日往前推算之月份，無限制時給null
* PARAM iMonthScope:起始日與終止日之相距月份限制，無月份限制時給null
* return validate_CheckDateScopeAlert
* 
*/
function validate_CheckDateScopeAlert(sFieldName, oBaseDatevalue, oStartDate, oEndDate,bCanOverBaseDate,iMonthAgo,iMonthScope)
{	
	console.log("validate_CheckDateScopeAlert");

	try
	{
		
		//基準日
		var dBaseDate;
		if (oBaseDatevalue instanceof Date)
		{//日期物件
			dBaseDate = oBaseDatevalue;
		}
		else if (typeof(oBaseDatevalue) == "string")
		{//日期字串，如2008/08/01
			if (!IsValidDate(oBaseDatevalue))
			{
				return "*  请输入正确的基准日，例如 2009/01/31";
			}
			dBaseDate = new Date(oBaseDatevalue);
		}
		else
		{//輸入框物件
			if (!IsValidDate(oBaseDatevalue))
			{
				return "*  请输入正确的基准日，例如 2009/01/31";
				
			}
			dBaseDate = new Date(oBaseDatevalue);
		}
		
		var oBaseYear = dBaseDate.getFullYear().toString();
		var oBaseMonth = dBaseDate.getMonth() + 1;
		oBaseMonth = oBaseMonth < 10 ? "0" + oBaseMonth : oBaseMonth.toString();
		var oBaseDay = dBaseDate.getDate();
		oBaseDay = oBaseDay < 10 ? "0" + oBaseDay : oBaseDay.toString();
		sBaseDate = oBaseYear + "/" + oBaseMonth + "/" + oBaseDay;
		
		//最小日
		var dMinDate = null;
		var sMinDate = null;
		if (iMonthAgo != null)
		{
		    var oMinMonth = null;
		    if (oBaseMonth > iMonthAgo)
		        oMinMonth = oBaseMonth - iMonthAgo - 1;
		    else
		        oMinMonth = -1 - (iMonthAgo - oBaseMonth);
		        
		    dMinDate = new Date(sBaseDate);
		    dMinDate.setMonth(oMinMonth);
		    dMinDate.setDate(1);
		    
		    oMinMonth = dMinDate.getMonth() + 1;
		    oMinMonth = oMinMonth < 10 ? "0" + oMinMonth : oMinMonth.toString();
		
			sMinDate = dMinDate.getFullYear() + "/" + oMinMonth + "/01";
		}
			
		oStartDatevalue = $("#" + oStartDate).val();
		
		var dStartDate = new Date(oStartDatevalue);
		var oStartYear = dStartDate.getFullYear().toString();
		var oStartMonth = dStartDate.getMonth() + 1;
		oStartMonth = oStartMonth < 10 ? "0" + oStartMonth : oStartMonth.toString();
		var oStartDay = dStartDate.getDate();
		var iStartDay = oStartDay;
		oStartDay = oStartDay < 10 ? "0" + oStartDay : oStartDay.toString();

		if (!bCanOverBaseDate && dStartDate > dBaseDate)
		{
			return "* 起始日不能大于 " + sBaseDate;
		}
		
		if (dMinDate != null && dStartDate < dMinDate)
		{
			return "* 起始日不能小于 " + sMinDate;
			
		}
		oEndDatevalue = $("#" + oEndDate).val();

		var dEndDate = new Date(oEndDatevalue);
		var oEndYear = dEndDate.getFullYear().toString();
		var oEndMonth = dEndDate.getMonth() + 1;
		oEndMonth = oEndMonth < 10 ? "0" + oEndMonth : oEndMonth.toString();
		var oEndDay = dEndDate.getDate();
		oEndDay = oEndDay < 10 ? "0" + oEndDay : oEndDay.toString();
		
		//最大日
		var dMaxDate = null;
		var sMaxDate = null;
		if (iMonthScope != null)
		{
			if (iMonthScope == iMonthAgo)
			{
				dMaxDate = dBaseDate;
				sMaxDate = sBaseDate;
			}
			else
			{
				oMaxMonth = dStartDate.getMonth() + 1 + iMonthScope;
				if (oMaxMonth > 12)
				{
					var i = parseInt(oMaxMonth / 12);
					oMaxMonth = oMaxMonth - 12 * i;
					if (oMaxMonth == 0)
					{
						oMaxMonth = 12;
						i = i - 1;
					}
					oMaxMonth = oMaxMonth < 10 ? "0" + oMaxMonth : oMaxMonth.toString();
					sMaxDate = (parseInt(oStartYear) + i) + "/" + oMaxMonth + "/"  //尚未加入day
				}else
				{
					oMaxMonth = oMaxMonth < 10 ? "0" + oMaxMonth : oMaxMonth.toString();
					sMaxDate = oStartYear + "/" + oMaxMonth + "/"    //尚未加入day
				}
				
				for (var i = parseInt(iStartDay); i > 0; i--)
				{//防止出現如2008/2/31之日期
					var oDay = i < 10 ? "0" + i.toString() : i.toString();
					var sDate = sMaxDate + oDay;
					if (IsValidDate(sDate))
					{
						sMaxDate = sDate;
						dMaxDate = new Date(sMaxDate);
						break;
					}
				}
			}
		}
		
		if (!bCanOverBaseDate && (dMaxDate == null || dMaxDate > dBaseDate))
		{//最大日不能超過基準日
			sMaxDate = sBaseDate;
			dMaxDate = dBaseDate;
		}

		if (dMaxDate != null && dEndDate > dMaxDate)
		{
			return "* 终止日不能大于 " + sMaxDate;
		}
		
		if (dEndDate < dStartDate)
		{
			return "*  终止日不能小于起始日";
		}
	}
	catch (exception)
	{
		//alert("检核" + sFieldName + "时发生错误:" + exception);
		errorBlock(
				null, 
				null,
				["检核" + sFieldName + "时发生错误:" + exception], 
				'离开', 
				null
			);
	}
	
	return true;
}

/*
 * 检核每月指定日是否在指定的日期範圍中
 * PARAM sFieldName:欄位中文描述
 * PARAM oPerMonthDate:每月指定日下拉框物件
 * PARAM oStartDate:起始日輸入框物件
 * PARAM oEndDate:終止日輸入框物件
 * return 
 * validate[required,funcCall[CheckPerMonthDay['sFieldName', oPerMonthDate, oStartDate, oEndDate]]]
 */
function validate_CheckPerMonthDay(field, rules, i, options)
{
	console.log("_CheckPerMonthDay");
	
	var sFieldName = rules[i + 2];
	var oPerMonthDate = rules[i + 3];
	var oStartDate = rules[i + 4];
	var oEndDate = rules[i + 5];
	
	if  (typeof(sFieldName) != "string")
	{
		return "* CheckPerMonthDay含有无效的参数sFieldName";
	}
	
	try
	{
		//每月特定日(1~31)
		var sPerMonthDate = $("#" + oPerMonthDate).val();
		
		var bMatch = false;
		for (var i = 1; i <= 31; i++)
		{
			if (i.toString() == sPerMonthDate)
			{
				bMatch = true;
				break;
			}
		}
		if (!bMatch)
		{
			return "选择的" + sFieldName + "为不正确的值";
		}
		
		//起始日
		if (!IsValidDate($("#" + oStartDate).val()))
		{
			return "请输入正确的起始日格式，例如 2009/01/31";
		}
		var dStartDate = new Date($("#" + oStartDate).val());
		var oStartYear = dStartDate.getFullYear().toString();
		var oStartMonth = dStartDate.getMonth() + 1;
		var oNextMonth = dStartDate.getMonth() + 2;
		var oNextYear = null;
		if (oNextMonth == 13)
		{
			oNextYear = dStartDate.getFullYear() + 1;
			oNextYear = oNextYear.toString();
			oNextMonth = "01";
		} else
		{
			oNextYear = dStartDate.getFullYear().toString();
			oNextMonth = oNextMonth < 10 ? "0" + oNextMonth : oNextMonth.toString();
		}
		oStartMonth = oStartMonth < 10 ? "0" + oStartMonth : oStartMonth.toString();
		
		//終止日
		if (!IsValidDate($("#" + oEndDate).val()))
		{
			return "请输入正确的终止日格式，例如 2009/01/31";
		}
		var dEndDate = new Date($("#" + oEndDate).val());
		
		var dNextDate = null;
		for (var i = parseInt(sPerMonthDate); i > 0; i--)
		{//防止出現如2008/2/31之日期
			var oDay = i < 10 ? "0" + i.toString() : i.toString();
			var sDate = oStartYear + "/" + oStartMonth + "/" + oDay;
			if (IsValidDate(sDate))
			{
				dNextDate = new Date(sDate);
				break;
			}
		}
		
		if (dNextDate < dStartDate)
		{
			for (var i = parseInt(sPerMonthDate); i > 0; i--)
			{//防止出現如2008/2/31之日期
				var oDay = i < 10 ? "0" + i.toString() : i.toString();
				var sDate = oNextYear + "/" + oNextMonth + "/" + oDay;
				if (IsValidDate(sDate))
				{
					dNextDate = new Date(sDate);
					break;
				}
			}
		}
		
		if (dNextDate > dEndDate)
		{
			return "选择的" + sFieldName + "不在日期范围内";
		}
		
	}
	catch (exception)
	{
		//alert("检核" + sFieldName + "时发生错误:" + exception);
		errorBlock(
				null, 
				null,
				["检核" + sFieldName + "时发生错误:" + exception], 
				'离开', 
				null
			);
	}
	
}

function EyearToCyear(sEyear)
{
///<summary>轉換西元年(2008)至民國年(097)</summary>
///<param name="sEyear">前四碼為西元年之字串</param>
///<returns>前三碼為民國年之字串</returns>

	//傳入字串É先通過驗證符合四碼西元年格式
	var zeros = "000";
	var year = (parseInt(sEyear.substring(0, 4), 10) - 1911).toString();
	year = zeros.substring(0, 3 - year.length) + year;
	return year + sEyear.substring(4);
}

/*
 * 检核民國日期範圍
 * PARAM sFieldName:欄位中文描述
 * PARAM oBaseDate:基準日期，可為輸入框物件或日期物件或日期字串
 * PARAM oStartDate:起始日輸入框物件
 * PARAM oEndDate:終止日輸入框物件
 * PARAM bCanOverBaseDate:起始日或終止日可否大於基準日
 * PARAM iMonthAgo:以基準日往前推算之月份，無限制時給null
 * PARAM iMonthScope:起始日與終止日之相距月份限制，無月份限制時給null
 * return
 * validate[required,funcCall[validate_CheckCDateScope['sFieldName', oBaseDate, oStartDate, oEndDate, bCanOverBaseDate, iMonthAgo, iMonthScope]]]
 */
function validate_CheckCDateScope(field, rules, i, options)
{
	console.log("_CheckCDateScope");
	
	var sFieldName = rules[i + 2];
	var oBaseDate = rules[i + 3];
	var oStartDate = rules[i + 4];
	var oEndDate = rules[i + 5];
	var bCanOverBaseDate = rules[i + 6];
	var iMonthAgo = rules[i + 7];
	var iMonthScope = rules[i + 8];
	
	if  (typeof(sFieldName) != "string")
	{
		return "* CheckDateScope含有无效的参数sFieldName";
	}
	
	try
	{
		oBaseDatevalue = $("#" + oBaseDate).val();
		//基準日
		var dBaseDate;
		if (oBaseDate instanceof Date)
		{//日期物件
			dBaseDate = oBaseDatevalue;
		}
		else if (typeof(oBaseDatevalue) == "string")
		{//日期字串，如097/08/01
			var base = CyearToEyear(oBaseDatevalue);
			if (!IsValidDate(base))
			{
				return "* 请使用正确的基准日参数，例如 097/01/31";
			}
			dBaseDate = new Date(base);
		}
		else
		{//輸入框物件
			var base = CyearToEyear(oBaseDatevalue);
			if (!IsValidDate(base))
			{
				return "* 请输入正确的基准日，例如 097/01/31";
			}
			dBaseDate = new Date(base);
		}
		
		var oBaseYear = EyearToCyear(dBaseDate.getFullYear().toString());
		var oBaseMonth = dBaseDate.getMonth() + 1;
		oBaseMonth = oBaseMonth < 10 ? "0" + oBaseMonth : oBaseMonth.toString();
		var oBaseDay = dBaseDate.getDate();
		oBaseDay = oBaseDay < 10 ? "0" + oBaseDay : oBaseDay.toString();
	    sBaseDate = oBaseYear + "/" + oBaseMonth + "/" + oBaseDay;
		
		//最小日
		var dMinDate = null;
		var sMinDate = null;
		if (iMonthAgo != null)
		{
		    var oMinMonth = null;
		    if (oBaseMonth > iMonthAgo)
		        oMinMonth = oBaseMonth - iMonthAgo - 1;
		    else
		        oMinMonth = -1 - (iMonthAgo - oBaseMonth);
		        
		    dMinDate = new Date(CyearToEyear(sBaseDate));
		    dMinDate.setMonth(oMinMonth);
		    dMinDate.setDate(1);
		    
		    oMinMonth = dMinDate.getMonth() + 1;
		    oMinMonth = oMinMonth < 10 ? "0" + oMinMonth : oMinMonth.toString();
		
			oMinYear = EyearToCyear(dMinDate.getFullYear().toString());
			sMinDate = oMinYear + "/" + oMinMonth + "/01";
		}
		oStartDatevalue = $("#" + oStartDate).val();
		//起始日
		var start = CyearToEyear(oStartDatevalue);
		if (!IsValidDate(start))
		{
			return "* 请输入正确的起始日格式，例如 097/01/31";
		}
		var dStartDate = new Date(start);
		var oStartYear = EyearToCyear(dStartDate.getFullYear().toString());
		var oStartMonth = dStartDate.getMonth() + 1;
		oStartMonth = oStartMonth < 10 ? "0" + oStartMonth : oStartMonth.toString();
		var oStartDay = dStartDate.getDate();
		var iStartDay = oStartDay;
		oStartDay = oStartDay < 10 ? "0" + oStartDay : oStartDay.toString();
		
		if (!bCanOverBaseDate && dStartDate > dBaseDate)
		{
			return "* 起始日不能大于 " + sBaseDate;
		}
		
		if (dMinDate != null && dStartDate < dMinDate)
		{
			return "* 起始日不能小于 " + sMinDate;
		}
		oEndDatevalue = $("#" + oEndDate).val();
		//終止日
		var end = CyearToEyear(oEndDatevalue);
		if (!IsValidDate(end))
		{
			return "* 请输入正确的终止日格式，例如 097/01/31";
		}
		var dEndDate = new Date(end);
		var oEndYear = EyearToCyear(dEndDate.getFullYear().toString());
		var oEndMonth = dEndDate.getMonth() + 1;
		oEndMonth = oEndMonth < 10 ? "0" + oEndMonth : oEndMonth.toString();
		var oEndDay = dEndDate.getDate();
		oEndDay = oEndDay < 10 ? "0" + oEndDay : oEndDay.toString();
		
		//最大日
		var dMaxDate = null;
		var sMaxDate = null;
		if (iMonthScope != null)
		{
			if (iMonthScope == iMonthAgo)
			{
				dMaxDate = dBaseDate;
				sMaxDate = sBaseDate;
			}
			else
			{
				oMaxMonth = dStartDate.getMonth() + 1 + iMonthScope;
				if (oMaxMonth > 12)
				{
					var i = parseInt(oMaxMonth / 12);
					oMaxMonth = oMaxMonth - 12 * i;
					if (oMaxMonth == 0)
					{
						oMaxMonth = 12;
						i = i - 1;
					}
					oMaxMonth = oMaxMonth < 10 ? "0" + oMaxMonth : oMaxMonth.toString();
					var zeros = "000";
					var max = (parseInt(oStartYear, 10) + i).toString();
					max = zeros.substring(0, 3 - max.length) + max;
					sMaxDate = max + "/" + oMaxMonth + "/"  //尚未加入day
				}else
				{
					oMaxMonth = oMaxMonth < 10 ? "0" + oMaxMonth : oMaxMonth.toString();
					sMaxDate = oStartYear + "/" + oMaxMonth + "/"    //尚未加入day
				}
				
				for (var i = parseInt(iStartDay); i > 0; i--)
				{//防止出現如097/2/31之日期
					var oDay = i < 10 ? "0" + i.toString() : i.toString();
					var sDate = sMaxDate + oDay;
					var d = CyearToEyear(sDate);
					if (IsValidDate(d))
					{
						sMaxDate = sDate;
						dMaxDate = new Date(d);
						break;
					}
				}
			}
		}
		
		if (!bCanOverBaseDate && (dMaxDate == null || dMaxDate > dBaseDate))
		{//最大日不能超過基準日
			sMaxDate = sBaseDate;
			dMaxDate = dBaseDate;
		}
		
		if (dMaxDate != null && dEndDate > dMaxDate)
		{
			return "* 终止日不能大于 " + sMaxDate;
		}
		
		if (dEndDate < dStartDate)
		{
			return "*　终止日不能小于起始日";
		}
	}
	catch (exception)
	{
		//alert("检核" + sFieldName + "时发生错误:" + exception);
		errorBlock(
				null, 
				null,
				["检核" + sFieldName + "时发生错误:" + exception], 
				'离开', 
				null
			);
	}

}
//身份證字號檢查
function checkID( idname ) {
	 var id = idname.value;
	 if (id == undefined)	id = idname;
	 id=id.toUpperCase();//英文字母轉大寫
	 tab = "ABCDEFGHJKLMNPQRSTUVXYWZIO"
   A1 = new Array (1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3 );
   A2 = new Array (0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5 );
   Mx = new Array (9,8,7,6,5,4,3,2,1,1);

   if ( id.length != 10 ) return false;
   i = tab.indexOf( id.charAt(0) );
   if ( i == -1 ) return false;
   
   
   sum = A1[i] + A2[i]*9;

   for ( i=1; i<10; i++ ) {
      v = parseInt( id.charAt(i) );
      if ( isNaN(v) ) return false;
      sum = sum + v * Mx[i];
   }
   if ( sum % 10 != 0 ) return false;
   return true;
}

//外僑居留證字號檢查
function checkID2( idname ) {
	 var id = idname.value;
	 if (id == undefined)	id = idname;
	 id=id.toUpperCase();//英文字母轉大寫
	 tab = "ABCDEFGHJKLMNPQRSTUVWXYZIO"
   A1 = new Array (1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3 );
   A2 = new Array (0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5 );
   Mx = new Array (9,8,7,6,5,4,3,2,1,1);

   i1 = tab.indexOf( id.charAt(0) );
   i2 = tab.indexOf( id.charAt(1) );
      
   front_1 = A1[i1]*1 + A2[i1]*9;
	 front_2 = A2[i2]*8;
	 sum = front_1 + front_2;

   for ( i=2; i<10; i++ ) {
      v = parseInt( id.charAt(i) );
      if ( isNaN(v) ) return false;
      sum = sum + v * Mx[i];
   }
   if ( sum % 10 != 0 ) return false;
   return true;
}

// 統一編號檢查
function TaxID(sTaxID) {
	try {
//		var sTaxID = sTax.value;
		var i, a1, a2, a3, a4, a5;
		var b1, b2, b3, b4, b5;
		var c1, c2, c3, c4;
		var d1, d2, d3, d4, d5, d6, d7, cd8;
		if (sTaxID.length != 8)
			return false;
		var c;
		for (i = 0; i < 8; i++) {
			c = sTaxID.charAt(i);
			if ("0123456789".indexOf(c) == -1)
				return false;
		}
		d1 = parseInt(sTaxID.charAt(0));
		d2 = parseInt(sTaxID.charAt(1));
		d3 = parseInt(sTaxID.charAt(2));
		d4 = parseInt(sTaxID.charAt(3));
		d5 = parseInt(sTaxID.charAt(4));
		d6 = parseInt(sTaxID.charAt(5));
		d7 = parseInt(sTaxID.charAt(6));
		cd8 = parseInt(sTaxID.charAt(7));
		c1 = d1;
		c2 = d3;
		c3 = d5;
		c4 = cd8;
		a1 = parseInt((d2 * 2) / 10);
		b1 = (d2 * 2) % 10;
		a2 = parseInt((d4 * 2) / 10);
		b2 = (d4 * 2) % 10;
		a3 = parseInt((d6 * 2) / 10);
		b3 = (d6 * 2) % 10;
		a4 = parseInt((d7 * 4) / 10);
		b4 = (d7 * 4) % 10;
		a5 = parseInt((a4 + b4) / 10);
		b5 = (a4 + b4) % 10;
		if ((a1 + b1 + c1 + a2 + b2 + c2 + a3 + b3 + c3 + a4 + b4 + c4) % 5 == 0)
			return true;
		if (d7 = 7) {
			if ((a1 + b1 + c1 + a2 + b2 + c2 + a3 + b3 + c3 + a5 + c4) % 5 == 0)
				return true;
		}
		return false;
	} catch (e) {
		return false;
	}
}
/*
 * 檢查身份證字號及統一編號
 * PARAM obj:身份證字號輸入欄位
 * return 
 * validate[required,funcCall[validate_checkSYS_IDNO[obj]]]
 */
function validate_checkSYS_IDNO(field, rules, i, options){
	
	console.log("_checkSYS_IDNO");
	
	var obj = rules[i + 2];
	var objvalue = $("#"+obj).val().toUpperCase();

	if(objvalue!=""){
	  if(objvalue.length==8 || objvalue.length==10){
		  if(objvalue.length==8){
			  if(!TaxID(objvalue))
			  {
				  return "* 身分证或统一编号输入有错，请重新输入。";
			  }
		  }
	    
		  if(objvalue.length==10){
	    	
			  var reType1 = /[A-Z]{1}\d{9}/;
			  var reType2 = /[A-Z]{2}\d{8}/;
			  var reType3 = /\d{8}[A-Z]{2}/;      	
	    	
			  if (reType1.test(objvalue)) {      	
				  if(!checkID(objvalue))
				  {
					  return "* 身分证或统一编号输入有错，请重新输入。";
				  }				 					 
			  }        
			  else if (reType2.test(objvalue)) {
				  if(!checkID2(objvalue))
				  {
					  return "* 身分证或统一编号输入有错，请重新输入。";
				  }					 					         		
			  }       
			  else if (!reType3.test(objvalue)) {       
				      return "* 身分证或统一编号输入有错，请重新输入。";				 					
			  }  	         		        
			  else {
				   return "* 身分证或统一编号输入有错，请重新输入。";
			  }
					
		  }
	  }
	  else{
	    return "* 身分证或统一编号输入有错，请重新输入。";
	  }
	}
}
/*
 * 檢查輸入框內容字數是否符合指定字數
 * PARAM sFieldName:欄位中文描述
 * PARAM oField:輸入框
 * PARAM bCanEmpty:可否為空字串
 * PARAM iEqualLen:必須符合的字串長度
 * return 
 * validate[required,funcCall[validate_CheckLenEqual['sFieldName',oField,bCanEmpty,iEqualLen]]]
 */
function validate_CheckLenEqual(field, rules, i, options)
{
	var sFieldName = rules[i + 2];
	var oField = rules[i + 3];
	var bCanEmpty = rules[i + 4];
	var iEqualLen = rules[i + 5];
	
    if  (typeof(sFieldName) != "string")
	{
		return "* CheckLenEqual含有无效的参数sFieldName";
	}
	
	try
	{
	    var sNumber = $('#'+oField).val();
	    
	    if (bCanEmpty == false && sNumber == "")
	    {
	        return "* 请输入" + sFieldName;
	    }
	    
	    var iLen = sNumber.length;
		
		if (iLen != iEqualLen)
		{
	        return "* "+sFieldName + "的内容长度应该等于" + iEqualLen;		
		}
	}
	catch (exception)
	{
	    //alert("检核" + sFieldName + "时发生错误:" + exception);
		errorBlock(
				null, 
				null,
				["检核" + sFieldName + "时发生错误:" + exception], 
				'离开', 
				null
			);
	}
}
/*
 * 檢查輸入框內容字數是否符合指定字數
 * PARAM sFieldName:欄位中文描述
 * PARAM oField:輸入框
 * PARAM bCanEmpty:可否為空字串
 * PARAM LenMin:必須符合的字串最小長度
 * PARAM LenMax:必須符合的字串最大長度
 * return 
 * validate[required,funcCall[validate_CheckLenEqual['sFieldName',oField,bCanEmpty,LenMin,LenMax]]]
 */
function validate_CheckLenEqual2(field, rules, i, options)
{
	var sFieldName = rules[i + 2];
	var oField = rules[i + 3];
	var bCanEmpty = rules[i + 4];
	var LenMin = rules[i + 5];
	var LenMax = rules[i + 6];
    if  (typeof(sFieldName) != "string"){
		return "* CheckLenEqual含有无效的参数sFieldName";
	}
	try{
	    var sNumber = $('#'+oField).val();
	    
	    if (bCanEmpty == 'false' && sNumber == ""){
	        return "* 请输入" + sFieldName;
	    }
	    
	    var iLen = sNumber.length;
		
		if (iLen <LenMin || iLen > LenMax){
	        return "请输入"+LenMin+ "-" +LenMax+"位"+sFieldName;		
		}
	}
	catch (exception)
	{
	    //alert("检核" + sFieldName + "时发生错误:" + exception);
		errorBlock(
				null, 
				null,
				["检核" + sFieldName + "时发生错误:" + exception], 
				'离开', 
				null
			);
	}
}
/*
 * 檢查輸入框內容字數是否只有英文數字
 * PARAM sFieldName:欄位中文描述
 * PARAM oField:輸入框
 * return 
 * validate[required,funcCall[validate_chkChrNum['sFieldName',oField]]]
 */
function validate_chkChrNum(field, rules, i, options){
	
	var sFieldName = rules[i + 2];
	var oField = rules[i + 3];
	
	var str=$('#'+oField).val();
	
	var slength = str.length;
	for(i=0;i<slength;i++){
		var chkchar = str.charAt(i);
		if(chkchar<'0' || chkchar>'z'){
			return "* "+sFieldName+"仅能输入英文数字";
		}
		else{
			if(chkchar>'9' && chkchar<'A'){
				return "* "+sFieldName+"仅能输入英文数字";
			}
			if(chkchar>'Z' && chkchar<'a'){
				return "* "+sFieldName+"仅能输入英文数字";
			}

		}
	}
}

/*
 * 居住地電話、公司電話、行動電話擇一填寫
 * PARAM sFieldName:欄位中文描述
 * PARAM oFieldHome:輸入框
 * PARAM oFieldCom:輸入框
 * PARAM oFieldCell:輸入框
 * return 
 * validate[funcCallRequired[validate_chkPhoneOne['sFieldName',oFieldHome,oFieldCom,oFieldCell]]]
 */
function validate_chkPhoneOne(field, rules, i, options){

	console.log("validate_chkPhoneOne");
	var sFieldName = rules[i + 2];
	var oFieldHome = rules[i + 3];
	var oFieldCom = rules[i + 4];
	var oFieldCell = rules[i + 5];

	if($("#"+oFieldHome).val().length==0 && $("#"+oFieldCom).val().length==0 && $("#"+oFieldCell).val().length==0){
		console.log("validate_chkPhoneOne");
		return sFieldName;
	}
//	if( $("#"+oFieldCell).val().length > 0 &&  $("#"+oFieldCell).val().length < 10){
//		return "* 行动电话长度有误";
//	}
}
/*
 * 行動電話檢驗
 * PARAM sFieldName:欄位中文描述
 * PARAM oFieldHome:輸入框
 * PARAM oFieldCom:輸入框
 * PARAM oFieldCell:輸入框
 * return 
 * validate[funcCallRequired[validate_chkPhoneOne['sFieldName',oFieldCell]]]
 */
function validate_chkCelPhome(field, rules, i, options){

	console.log("validate_chkPhoneOne");
	var sFieldName = rules[i + 2];
	var oFieldCell = rules[i + 3];

	if( $("#"+oFieldCell).val().length > 0 &&  $("#"+oFieldCell).val().length < 10){
		return "* 行动电话长度有误";
	}
}
/*
 * 限額大小檢查
 * PARAM sFieldName:欄位中文描述
 * PARAM oFieldHome:輸入框
 * PARAM oFieldCom:輸入框
 * PARAM oFieldCell:輸入框
 * return 
 * validate[required,funcCall[validate_chkCountMaxMin['sFieldName',oField,Max,Min]]]
 */
function validate_chkCountMaxMin(field, rules, i, options){

	console.log("validate_chkCountMaxMin");
	var sFieldName = rules[i + 2];
	var oField = rules[i + 3];
	var Max = rules[i + 4];
	var Min = rules[i + 5];

	var lmt = $("#"+oField).val();
	if(parseInt(lmt)>Max)
	{
		return "申请" + sFieldName + "限额不得超过" + Max + "万元";
	}
	if(parseInt(lmt)<=Min)
	{
		return "申请" + sFieldName + "限额不得小于" + Min + "万元";
	}
}

/*
 * 車號检核
 * PARAM sFieldName:欄位中文描述
 * PARAM oFieldCarKind:輸入框
 * PARAM oFieldCarId1:輸入框
 * PARAM oFieldCarId2:輸入框
 * return 
 * validate[funcCallRequired[validate_chkCarId['sFieldName',oFieldCarKind,oFieldCarId1,oFieldCarId2]]]
 */
function validate_chkCarId(field, rules, i, options){

	var sFieldName = rules[i + 2];
	var oFieldCarKind = rules[i + 3];
	var oFieldCarId1 = rules[i + 4];
	var oFieldCarId2 = rules[i + 5];
	
    if($('#'+oFieldCarKind).val()=="C")
    {
		if(!(	($('#' + oFieldCarId1).val().length==2 && $('#' + oFieldCarId2).val().length==4) || 
				($('#' + oFieldCarId1).val().length==4 && $('#' + oFieldCarId2).val().length==2) || 
				($('#' + oFieldCarId1).val().length==3 && $('#' + oFieldCarId2).val().length==2) || 
				($('#' + oFieldCarId1).val().length==2 && $('#' + oFieldCarId2).val().length==3) || 
				($('#' + oFieldCarId1).val().length==2 && $('#' + oFieldCarId2).val().length==2) || 
				($('#' + oFieldCarId1).val().length==3 && $('#' + oFieldCarId2).val().length==3) || 
				($('#' + oFieldCarId1).val().length==3 && $('#' + oFieldCarId2).val().length==4))) 
		{
		 return "汽车车号检核错误";
		} 
	  } 
    else if($('#'+oFieldCarKind).val()=="M")
	  {
		if(!(	($('#' + oFieldCarId1).val().length==2 && $('#' + oFieldCarId2).val().length==2) || 
				($('#' + oFieldCarId1).val().length==3 && $('#' + oFieldCarId2).val().length==3) || 
				($('#' + oFieldCarId1).val().length==3 && $('#' + oFieldCarId2).val().length==4) || 
				($('#' + oFieldCarId1).val().length==2 && $('#' + oFieldCarId2).val().length==3))) 
		{
		 return "机车车号检核错误";
		} 	   
	  }
}

/*
 * 检核下列是否有選擇
 * PARAM sFieldName:欄位中文描述
 * PARAM oFieldKindNum:輸入框
 * return 
 * validate[funcCallRequired[validate_chkClickboxKind['sFieldName',oFieldKindNum,.....]]]
 */
function validate_chkClickboxKind(field, rules, i, options){

	var sFieldName = rules[i + 2];
	var oFieldKindNum = parseInt(rules[i + 3]);
	console.log(oFieldKindNum);
	var flag = true;
	for(k = 4;k < 4+oFieldKindNum;k++){
		var oFieldName = rules[i + k];
		console.log(oFieldName);
		console.log($('#'+oFieldName).prop("checked"));
		if($('#'+oFieldName).prop("checked")){
			flag = false;
		}
	}
	if(flag){
		return "* 请点选" + sFieldName;
	}
}
/*
 * 連號數字检核
 * PARAM sFieldName:欄位中文描述
 * PARAM oFieldId1:輸入框
 * return 
 * 欄位可空白:validate[funcCallRequired[validate_ChkSerialNum['sFieldName',oFieldCarId1]]]
 * 欄位不空白:validate[required,funcCall[validate_ChkSerialNum['sFieldName',oFieldCarId1]]]
 */
function validate_ChkSerialNum(field, rules, i, options){
	console.log("validate_ChkSerialNum~~");
	var sFieldName = rules[i + 2];
	var oFieldCarId1 = rules[i + 3];
	console.log("oFieldCarId1="+oFieldCarId1);
    if(!chkSerialNum($('#'+oFieldCarId1).val()))
    {
    	$('#'+oFieldCarId1).val("");
		 return sFieldName+"不能为连号数字";
	} 
}
/*
 * 全部相同之數字
 * PARAM sFieldName:欄位中文描述
 * PARAM oFieldId1:輸入框
 * return 
 * 欄位可空白:validate[funcCallRequired[validate_ChkSameEngOrNum['sFieldName',oFieldCarId1]]]
 * 欄位不空白:validate[required,funcCall[validate_ChkSameEngOrNum['sFieldName',oFieldCarId1]]]
 */
function validate_ChkSameEngOrNum(field, rules, i, options){
	var sFieldName = rules[i + 2];
	var oFieldCarId1 = rules[i + 3];
	console.log("oFieldCarId1="+oFieldCarId1);
    if(!chkSameEngOrNum($('#'+oFieldCarId1).val()))
    {
		 return sFieldName+"不能为全部相同之数字";
	} 
}

/*
 * 使用者名稱Supervisor
 * 	1.	6-16碼之英數字
 *	2.	至少兩位英文字不限位子
 *	3.	不可為特殊符號
 *	4.	數字部份不得全部與身分證統一編號、出生年月日(公司設立日期)、電話號碼、簽入密碼及交易密碼完全相同(不检核)
 *	5.	數字不得連號或完全重號(僅检核英文字重號)
 *	6.	連續字串(僅限英文)
 *	7.	大小寫區分	
 * PARAM sFieldName:欄位中文描述
 * PARAM oFieldId1:輸入框
 * return 
 * 欄位不空白:validate[required,funcCall[validate_CheckUserName['sFieldName',oFieldCarId1]]]
 */
function validate_CheckUserName(field, rules, i, options){
	
	var sFieldName = rules[i + 2];
	var oFieldCheckName = rules[i + 3];
	var username=$('#'+oFieldCheckName).val();
	var min = 6;
	var max = 16;
	console.log("field="+field);
	console.log("oFieldCheckName="+oFieldCheckName);
	console.log("username="+username);
	if(!checkLength(username,min,max)){
		return '请输入'+min+'-'+max+'码之使用者名称';
	}
	if(!check2EnChar(username)){;
		return '请输入至少两位英文字之使用者名称';
	}
	if(checkIllegalChar(username)){
		return '使用者名称不可含有特殊符号';
	}
	if(!chkSameEngOrNum(username)){
		return '使用者名称不可完全重号';
	}
	if(!chkSerialNum(username)){
		return '使用者名称不能为连号数字';
	}
	if(checkContinuousENChar(username)){
		return '使用者名称不可为连续字串';
	}
}

/*
 * 簽入密碼、交易密碼
 * 	1.	6-8码之数字
 *	2.	不可為特殊符號
 *	3.	數字部份不得全部與身分證統一編號、出生年月日(公司設立日期)、電話號碼、簽入密碼及交易密碼完全相同(不检核)
 *	4.	數字不得連號或完全重號
 * PARAM sFieldName:欄位中文描述
 * PARAM oFieldPwd1:輸入框
 * return 
 * 欄位不空白:validate[required,funcCall[validate_CheckPwd['sFieldName',oFieldPwd1]]]
 */
function validate_CheckPwd(field, rules, i, options){
	
	var sFieldName = rules[i + 2];
	var oFieldPwd1 = rules[i + 3];
	var pw=$('#'+oFieldPwd1).val();
	var min = 6;
	var max = 8;
	console.log("field="+field);
	console.log("oFieldPwd1="+oFieldPwd1);
	console.log("pw="+pw);
	if  (typeof(sFieldName) != "string")
	{
		return "* 含有无效的参数sFieldName";
	}
	if(!checkLength(pw,min,max)){
		return '* 请输入'+min+'-'+max+'码之英数字';
	}
	var regex = /[^a-z^A-Z^0-9]/g;
	var valid = regex.test(pw);
	if (valid) {
		return options.allrules.onlyLetterNumber.alertText;
	}
	if(!chkSerialNum(pw)){
		return '* ' + sFieldName + '不能为连号数字';
	}
	if(!chkSameEngOrNum(pw)){
		return '* ' + sFieldName + '不可完全重号';
	}
	if(checkContinuousENChar(pw)){
		return '* ' + sFieldName + '不能为连续字串';
	}
}

/*
 * 重複輸入項目检核
 * PARAM sFieldName:欄位中文描述
 * PARAM oFieldId1:輸入框
 * PARAM oFieldId2:輸入框
 * return 
 * 欄位可空白:validate[funcCallRequired[validate_DoubleCheck['sFieldName',oFieldCarId1,oFieldCarId2]]]
 * 欄位不空白:validate[required,funcCall[validate_DoubleCheck['sFieldName',oFieldCarId1,oFieldCarId2]]]
 */
function validate_DoubleCheck(field, rules, i, options){

	var sFieldName = rules[i + 2];
	var oFieldId1 = rules[i + 3];
	var oFieldId2 = rules[i + 4];
	
    if($('#'+oFieldId1).val()!=$('#'+oFieldId2).val())
    {
    	$('#'+oFieldId1).val("");
    	$('#'+oFieldId2).val("");
		 return "您所输入的"+sFieldName+"有误，请重新检查!";
	} 
}


/* *******************
 * *****以下為tool******
 * *****又名吐鷗 ********
 * ******************* */

/* 6-16碼之英數字
 * true :介於6-16碼
 * false:不介於6-16碼	*/
function checkLength(iText,min,max){
	var result = true;
	if(iText.length < parseInt(min,10))
		result = false;			
	if(iText.length > parseInt(max,10))
		result = false;
	return result;	
}
/* 至少兩位英文字不限位子
 * false:沒有至少兩位英文字
 * true :有至少兩位英文字*/
function check2EnChar(iText){
	var result = false;
	var pattern = /.*[a-zA-Z]{1}.*[a-zA-Z]{1}/;
	result = pattern.test(iText);
	return result;
}
/* 檢查不合法字元
 * false:沒有不合法字眼
 * true :有不合法字眼	*/
function checkIllegalChar(iText){
	var result = false;
	var legal = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890_#';
	var i=0
	while(i < iText.length) {
		var c = iText.substr(i, 1);
		if(legal.indexOf(c)<0) {
			result = true;
			break;
		}
		i++;
	}
	return result;
}

/* 檢查英數字重號
 * false:不重號
 * true :重號	*/
function checkDuplicateChar(iText){
	var result = true;
	var firstchar = iText.substr(0, 1);
	var i=0
	while(i < iText.length) {
		var c = iText.substr(i, 1);
		if(firstchar != c) {
			result = false;
			break;
		}
		i++;
	}
	return result;
}
/* 連續字串(僅限英文)
 * false:非連續字串
 * true :連續字串	*/
function checkContinuousENChar(iText){
	var result = true;
	var result1 = true;
	var firstchar = iText.substr(0, 1);
	var CChar = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
	var CCharArray = new Array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
	var i=1
	while(i < iText.length) {
		var c = iText.substr(i, 1);
		if(CCharArray[CChar.indexOf(firstchar)+i] != c) {
			result = false;
			break;
		}
		i++;
	}
	var CChar1 = 'zyxwvutsrqponmlkjihgfedcbaZYXWVUTSRQPONMLKJIHGFEDCBA';
	var CCharArray1 = new Array('z','y','x','w','v','u','t','s','r','q','p','o','n','m','l','k','j','i','h','g','f','e','d','c','b','a','Z','Y','X','W','V','U','T','S','R','Q','P','O','N','M','L','K','J','I','H','G','F','E','D','C','B','A');
	var x=1
	while(x < iText.length) {
		var c = iText.substr(x, 1);
		if(CCharArray1[CChar1.indexOf(firstchar)+x] != c) {
			result1 = false;
			break;
		}
		x++;
	}
	if(result == true || result1 == true){
		return true;
	}
	else{
		return false;
	}
}
/* 是否為連續數字
 * false:非連續數字
 * true :連續數字	*/
function chkSerialNum(obj) {
	var str = obj.toString();
	console.log("str="+str);
	if(str != '') {
		var iCode1 = str.charCodeAt(0); 
		if(iCode1 >=48 && iCode1 <= 57) {
			var iCode2 = 0;
			var bPlus = true;
			var bMinus = true;
			for(var i=1;i<str.length;i++) {
				iCode2 = str.charCodeAt(i);//now
				if(iCode2 != (iCode1+1)) {
					if(!(iCode2 == 48 && iCode1 == 57)) {
						bPlus = false;
					}
				}
				if(iCode2 != (iCode1-1)) {
					if(!(iCode2 == 57 && iCode1 == 48)) {
						bMinus = false;	
					}
				}
				iCode1 = iCode2;//before
			}
			if(bPlus || bMinus) return false;
		}
	}
	return true;	
}
/* 是否全為相同之文數字
 * false:非全同文數字
 * true :全同文數字	*/
function chkSameEngOrNum(obj) {
	var str = obj.toString();
	if(str != '') {
		var iCode1 = str.charCodeAt(0);
		if((iCode1 >=48 && iCode1 <= 57) || (iCode1 >= 65 && iCode1 <= 90) || (iCode1 >= 97 && iCode1 <= 122)) {
			var iCode2 = 0;
			var bSame  = true;
			for(var i=0;i<str.length;i++) {
				iCode2 = str.charCodeAt(i);
				if(iCode1 != iCode2) {
					bSame = false;
				}
			}
		}
		if(bSame) {
			return false;
		}
	}
	return true;
}


/*
 * 中文值檢測
 * PARAM sFieldName:欄位中文描述
 * PARAM oField:輸入框
 * return 
 * validate[funcCallRequired[validate_isChinese['sFieldName',oFieldCarKind]]]
 */
function validate_isChinese(field, rules, i, options)
{  
	console.log("validate_isChinese~~");
	var sFieldName = rules[i + 2];
	var oField = rules[i + 3];
	var name = $("#" + oField).val();
	console.log(name);
	for(i=0;i<name.length;i++)   
	{  
		console.log(name.charCodeAt(i));
		if((name.charCodeAt(i) < 0x4E00 || name.charCodeAt(i) > 0x9FA5)){
			return  sFieldName;
		}
	}
}   

/*
* 檢查台電電號
* PARAM sFieldName:欄位中文描述
* PARAM C1:輸入框
* PARAM C2:輸入框
* PARAM C3:輸入框
* PARAM C4:輸入框
* PARAM C5:輸入框
* return 
* validate[funcCallRequired[validate_chkEleNum[C1,C2,C3,C4,C5]]]
*/
function validate_chkEleNum(field, rules, i, options)
{
	var C1 = rules[i + 2];
	var C2 = rules[i + 3];
	var C3 = rules[i + 4];
	var C4 = rules[i + 5];
	var C5 = rules[i + 6];
	var elenum = $("#"+C1).val() + $("#"+C2).val() + $("#"+C3).val() + $("#"+C4).val();
	var chknum = $("#"+C5).val();
	if (elenum.length != 10){
		return "电号有误，请检查！";
	}
	var A=0,B=0;
	var tot=0;
	var chk;
	var ch = new Array(elenum.length);
	for(var index = 0; index < ch.length; index++)
	{//奇數位*2
		if ((index % 2) == 0)
		{
			ch[index] = elenum.charAt(index) * 2;
			if(isNaN(ch[index])){
				return "电号有误，请重新输入";
			}
			if(ch[index] > 9)
			{
				A = ch[index].toString().charAt(0);
				B = ch[index].toString().charAt(1);
				ch[index] = eval(A) + eval(B);
			}
		}
		else
			ch[index] = elenum.charAt(index);
		tot = tot + eval(ch[index]);
	}
	chk = tot.toString().charAt(1);
	if (chk!=chknum)
	{
		return "电号有误，请检查！";
	}
}

/*
* 檢查台電電號
* PARAM sFieldName:欄位中文描述
* PARAM tel1:輸入框
* PARAM tel2:輸入框
* return 
* validate[funcCallRequired[validate_chkTelFull[tel1,tel2]]]
*/
function validate_chkTelFull(field, rules, i, options)
{
	var tel1 = rules[i + 2];
	var tel2 = rules[i + 3];
	var UNTTEL1 = $("#" + tel1).val();
	var UNTTEL2 = $("#" + tel2).val();
   	if((UNTTEL1=="" && UNTTEL2!="") || (UNTTEL1!="" && UNTTEL2=="")){
   		return "电话区域码及电话号码输入不完全";
   	}
}


/*
* 檢查台電電號
* PARAM sFieldName:欄位中文描述
* PARAM time:輸入框
* PARAM today:輸入框
* PARAM limate:輸入框
* PARAM up:輸入框
* return 
* validate[funcCallRequired[validate_age['sFieldName',time,today,limate,up]]]
*/
function validate_age(field, rules, i, options)
{
	var sFieldName = rules[i + 2];
	var time = rules[i + 3];
	var today = rules[i + 4];
	var age = 0;
	var time1 = $("#" + time).val();
	var today1 = $("#" + today).val();
	var nowyear=Number(today1.substring(0,3));
	var nowmonth=Number(today1.substring(3,5));
	var nowday=Number(today1.substring(5,7));
	var year=Number(time1.substring(0,3));
	var month=Number(time1.substring(3,5));
	var day=Number(time1.substring(5,7));
	//計算實際年齡
	if( nowmonth > month )
		age = nowyear - year ;
	else if( (nowmonth == month)&&(nowday>=day) )
		age = nowyear - year;
	else
		age = nowyear - year - 1 ;
	

	var limate = rules[i + 5];
	var up = rules[i + 6];
	
	if(up){
		if(limate > age){
			return sFieldName + "必须大于" + limate;
		}
	}else{
		if(limate < age){
			return sFieldName + "必须小于" + limate;
		}
	}
}

/*
 * 檢查金額輸入框內容金額上下限是否為某值倍數
 * PARAM sFieldName:欄位中文描述
 * PARAM bIsInteger:是否整數
 * PARAM fMin:最小值，不限制時給null
 * PARAM fMax:最大值，不限制時給null
 * multiple:金額倍數  ex:輸入值須為1000的倍數 multiple=1000
 * return 
 * 檢查金額輸入框內容 PARAM ID:金額輸入框 PARAM Min:最小值 PARAM Max:最大值 return
 * validate[funcCallRequired[validate_CheckAmount[ 'sFieldName' , oField, fMin, fMax, multiple]]]
 */
function validate_Check_Amount(field, rules, i, options) {
	
	console.log("_CheckAmount");
	
	var sFieldName = rules[i + 2];
	var oField = rules[i + 3];
	var fMin = rules[i + 4];
	var fMax = rules[i + 5];
	var multiple = rules[i + 6];
	/**
	 * 因為用 validate 參數傳入為字串,必須轉型正確的型別
	 */
	var _fMin = "null" === fMin ? null: Number(fMin);
	var _fMax = "null" === fMax ? null: Number(fMax);
	var _multiple = "null" === multiple ? null: Number(multiple);
	
	if (typeof (sFieldName) != "string") {
		return "* CheckAmount含有无效的参数sFieldName";
	}
			
	try {
		
		var sAmount = $("#" + oField).val();
		bIsInteger = boolIsInteger(sAmount);
		
		if ( isNaN(sAmount)) {//數字
			return "* "+ sFieldName + "栏位请输入正确的金额格式";
		}
		
		//先TRIM掉，不要用startsWith與endsWith，在舊版瀏覽器會出錯
		//TODO:疑似用法錯誤，先改成下面方式
//		sAmount = sAmount.trim();
		sAmount = $.trim(sAmount);
		if (sAmount == "") {
			return "* "+ sFieldName + "栏位请输入正确的金额格式";
		}

		if (sFieldName.indexOf("+") == 0) {
			return "* "+ sFieldName + "栏位请勿输入加号";
		}

		if (sAmount.charAt(0) == "0" && sAmount.length > 1) {//check不以零为开头
			return "* "+ sFieldName + "栏位请勿以零为开头";
		}

		if (bIsInteger == true && sAmount.indexOf(".") != -1) {
			return "* "+ sFieldName + "栏位请输入整数";
		}

		if (bIsInteger == false && sAmount.indexOf(".") == sAmount.length - 1) {
			return "* "+ sFieldName + "栏位请输入正确的含小数点金额格式";
		}

		if(_fMin != null){
			if(parseInt(sAmount) < parseInt(fMin)){
				return "* "+ sFieldName + "栏位输入金额至少为" + fMin + "元";
			}
		}

		if(_fMax != null){
			if(parseInt(sAmount) > parseInt(fMax)){
				return "* "+ sFieldName + "栏位输入金额最多为" + fMax + "元";
			}
		}

		if(_multiple != null){
			if(parseInt(sAmount % multiple) > parseInt(0)){
				return "* "+ sFieldName + "栏位输入金额得为" + multiple + "元之整倍数增加";
			}
		}

	} catch (exception) {
		//alert("检核" + sFieldName + "时发生错误:" + exception);
		errorBlock(
				null, 
				null,
				["检核" + sFieldName + "时发生错误:" + exception], 
				'离开', 
				null
			);
	}

}

/*
 * 擇一填寫
 * PARAM sFieldName:欄位中文描述
 * PARAM oField1st:輸入框
 * PARAM oField2nd:輸入框
 * PARAM oField3rd:輸入框
 * return 
 * validate[funcCallRequired[validate_chkOne['sFieldName',oField1st,oField2nd,oField3rd]]]
 */
function validate_chkOne(field, rules, i, options){
	
	console.log("validate_chkOne");
	var sFieldName = rules[i + 2];
	var oField1st = rules[i + 3];
	var oField2nd = rules[i + 4];
	var oField3rd = rules[i + 5];
	
	if($("#"+oField1st).val().length==0 && $("#"+oField2nd).val().length==0 && $("#"+oField3rd).val().length==0){
		console.log("validate_chkOne");
		return "* 请输入一笔"+sFieldName;
	}
}

/*
 * 檢查狀態
 * PARAM sFieldName:欄位中文描述
 * PARAM oField:輸入框
 * PARAM oFieldStatus:輸入框
 * PARAM check1:輸入框
 * PARAM check2:輸入框
 * return 
 * validate[funcCallRequired[validate_chkStatus['sFieldName',oField,oFieldStatus,check1,check2]]]
 */
function validate_chkStatus(field, rules, i, options){
	
	console.log("validate_chkStatus");
	var sFieldName = rules[i + 2];
	var oField = rules[i + 3];
	var oFieldStatus = rules[i + 4];
	var check1 = rules[i + 5];
	var check2 = rules[i + 6];
	var Status = document.getElementsByName(oFieldStatus);
	console.log($("#"+oField).val());
	console.log(Status[check1].checked);
	console.log(Status[check2].checked);
	if($("#"+oField).val() != null){
		if($("#"+oField).val() == '' || $("#"+oField).val() == '0'){
			if(Status[check1].checked || Status[check2].checked){
				return "* "+sFieldName+"尚未约定定期定额,不可执行暂停(终止)扣款";
			}
		}
	}
}


/*
 * 檢查狀態
 * PARAM sFieldName:欄位中文描述
 * PARAM oField:輸入框
 * PARAM bCanEmpty:是否可以為空
 * return 
 * validate[funcCallRequired[validate_CheckMail['sFieldName',oField,bCanEmpty]]]
 */
function validate_CheckMail(field, rules, i, options){
	var sFieldName = rules[i + 2];
	var oField = rules[i + 3];
	var bCanEmpty = rules[i + 4];
	if (typeof (sFieldName) != "string") {
		return "CheckMail含有无效的参数sFieldName";
	}

	try {
		var mail = $("#"+oField).val();

        if (bCanEmpty == "false" && mail == "") {
			return "请输入" + sFieldName;
		}

		var str = mail + ";";
		var str1 = "";
		var str2 = "";

		if (mail.indexOf("<") != -1 || mail.indexOf(">") != -1) {
			return "输入之电子邮件帐号格式有误";
		}

		var i = str.indexOf(";");

		if (i == -1 && mail != ""
				&& (mail.indexOf("@") == -1 || mail.indexOf(".") == -1)) {
			return "输入之电子邮件帐号格式有误";
		}

		while (i != -1) {
			str1 = str.substring(0, i);
			str2 = str.substring(0, i + 1);
			str = str.replace(str2, "");

			if (str1 != ""
					&& (str1.indexOf("@") == -1 || str1.indexOf(".") == -1)) {
				return "输入之电子邮件帐号格式有误";
			}
			i = str.indexOf(";");
		}
	} catch (exception) {
		return "检核" + sFieldName + "时发生错误:" + exception;
	}
}

/*
 * 檢查信用卡號碼
 * PARAM sFieldName:欄位中文描述
 * PARAM carenum1:輸入框
 * PARAM carenum2:輸入框
 * PARAM carenum3:輸入框
 * PARAM carenum4:輸入框
 * return 
 * validate[required,funcCall[validate_checkMajorCard['sFieldName',carenum1,cardnum2,cardnum3,cardnum4]]]
 */
function validate_checkMajorCard(field, rules, i, options){
	/*
	 * 有新增卡片，請將範圍值填寫於此。
	 */
	var ccr = new Array(
		{name:"VISA白金卡", 		lowerbound: 405760000, upperbound: 405760999},
		{name:"故宮白金卡", 		lowerbound: 405760500, upperbound: 405760599},
		{name:"VISA 金卡", 		lowerbound: 490723000, upperbound: 490723879},
		{name:"嘉義認同金卡",		lowerbound: 490723880, upperbound: 490723882},
		{name:"VISA 金卡", 		lowerbound: 490723883, upperbound: 490723886},
		{name:"英康聯名金卡", 		lowerbound: 490723887, upperbound: 490723889},
		{name:"國際百貨金卡", 		lowerbound: 490723890, upperbound: 490723899},
		{name:"故宮認同金卡", 		lowerbound: 490723900, upperbound: 490723906},
		{name:"故宮認同金卡", 		lowerbound: 490723900, upperbound: 490723902},
		{name:"VISA金卡", 		lowerbound: 490723903, upperbound: 490723906},
		{name:"遠來休閒金卡", 		lowerbound: 490723907, upperbound: 490723909},
		{name:"VISA金卡", 		lowerbound: 490723925, upperbound: 490723999},
		{name:"VISA 普卡", 		lowerbound: 493825000, upperbound: 493825879},
		{name:"嘉義認同普卡", 		lowerbound: 493825880, upperbound: 493825882},
		{name:"VISA 普卡", 		lowerbound: 493825883, upperbound: 493825886},
		{name:"英康聯名卡", 		lowerbound: 493825887, upperbound: 493825889},
		{name:"國際百貨卡", 		lowerbound: 493825890, upperbound: 493825899},
		{name:"故宮認同卡", 		lowerbound: 493825900, upperbound: 493825906},
		{name:"故宮認同卡", 		lowerbound: 493825900, upperbound: 493825902},
		{name:"VISA 普卡", 		lowerbound: 493825903, upperbound: 493825906},
		{name:"花蓮海洋公園卡", 	lowerbound: 493825907, upperbound: 493825909},
		{name:"帶就富卡", 		lowerbound: 493825910, upperbound: 493825912},
		{name:"VISA 普卡", 		lowerbound: 493825925, upperbound: 493825999},
		{name:"COMBO普卡", 		lowerbound: 512296000, upperbound: 512296999},
		{name:"COMBO金卡", 		lowerbound: 512400000, upperbound: 512400999},
		{name:"COMBO白金卡", 		lowerbound: 514952000, upperbound: 514952999},
		{name:"MASTER 金卡", 	lowerbound: 540974000, upperbound: 540974879},
		{name:"MASTER 金卡", 	lowerbound: 540974883, upperbound: 540974886},
		{name:"森林卡金卡", 		lowerbound: 540974890, upperbound: 540974899},
		{name:"MASTER 金卡", 	lowerbound: 540974903, upperbound: 540974906},
		{name:"MASTER 金卡", 	lowerbound: 540974910, upperbound: 540974999},
		{name:"MASTER 普卡", 	lowerbound: 543770000, upperbound: 543770879},
		{name:"屏東師院卡", 		lowerbound: 543770880, upperbound: 543770880},
		{name:"MASTER 普卡", 	lowerbound: 543770884, upperbound: 543770886},
		{name:"森林卡普卡", 		lowerbound: 543770890, upperbound: 543770899},
		{name:"MASTER 普卡", 	lowerbound: 543770903, upperbound: 543770906},
		{name:"MASTER 普卡", 	lowerbound: 543770910, upperbound: 543770999},
		{name:"MASTER 鈦金商旅卡",lowerbound: 558866000, upperbound: 558866999},
		{name:"MASTER 悠遊鈦金卡",lowerbound: 524263000, upperbound: 524263009},
		{name:"MASTER 悠遊普通卡",lowerbound: 542367000, upperbound: 542367009},
		{name:"MASTER 悠遊金融卡",lowerbound: 53336000, upperbound: 53336019},
		{name:"VISA 悠遊白金卡",	lowerbound: 405760600, upperbound: 405760602},
		{name:"故宮之友VISA 悠遊金卡",lowerbound: 490723910, upperbound: 490723912},
		{name:"故宮之友VISA 悠遊普通卡",lowerbound: 4938259160, upperbound: 4938259168},
		{name:"故宮之友VISA御璽卡手機",lowerbound: 4712353000, upperbound: 4712353199},
		{name:"故宮之友VISA御璽悠遊卡",lowerbound: 4712350000, upperbound: 4712352999},
		{name:"宜蘭大學認同卡普卡",lowerbound: 5423673990, upperbound: 5423674089},
		{name:"宜蘭大學認同卡鈦金卡",lowerbound: 5242634990, upperbound: 5242635089},
		{name:"青溪之友認同卡普卡",lowerbound: 5423676990, upperbound: 5423677089},
		{name:"青溪之友認同卡鈦金卡",lowerbound: 5242636990, upperbound: 5242637089},
		{name:"永續生活一卡通MASTER普卡",lowerbound: 5423677090, upperbound: 5423677299},
		{name:"永續生活一卡通VISA普卡",lowerbound: 4938259190, upperbound: 4938259399},
		{name:"永續生活一卡通MASTER鈦金卡",lowerbound: 5242637090, upperbound: 5242637399},
		{name:"永續生活一卡通VISA白金卡",lowerbound: 4057606030, upperbound: 4057606299},
		{name:"永續生活一卡通VISA御璽卡",lowerbound: 4712353200, upperbound: 4712353499},
		{name:"VISA DEBIT 即時卡",lowerbound: 53336020, upperbound: 53336079},
		{name:"北港朝天宮媽祖認同卡普卡(一卡通)",lowerbound: 54236773, upperbound: 54236774},
		{name:"北港朝天宮媽祖認同卡鈦金卡(一卡通)",lowerbound: 52426375, upperbound: 52426376},
		{name:"北港朝天宮媽祖認同卡普卡(悠遊卡)",lowerbound: 54236775, upperbound: 54236776},
		{name:"北港朝天宮媽祖認同卡鈦金卡(悠遊卡)",lowerbound: 52426377, upperbound: 52426378},
		{name:"銀色之愛信用卡鈦金商旅卡(一卡通)",lowerbound: 558866500, upperbound: 558866509},
		{name:"銀色之愛信用卡鈦金商旅卡(悠遊卡)",lowerbound: 558866510, upperbound: 558866519},
		{name:"信保基金111關懷認同卡",lowerbound: 558866520, upperbound: 558866520},
		{name:"銀色之愛商務御璽卡(一卡通)",lowerbound: 490852000, upperbound: 490852009},
		{name:"銀色之愛商務御璽卡(悠遊卡)",lowerbound: 490852010, upperbound: 490852019},
		{name:"多產品商務御璽卡(一卡通)",lowerbound: 490852020, upperbound: 490852029},
		{name:"多產品商務御璽卡(悠遊卡)",lowerbound: 490852030, upperbound: 490852039},
		{name:"福獅公益卡(一卡通)",lowerbound: 558866530, upperbound: 558866534},
		{name:"樂獅公益卡(一卡通)",lowerbound: 558866535, upperbound: 558866536},
		{name:"利得Combo鈦金卡",lowerbound: 524263880, upperbound: 524263889},
		{name:"利得Combo鈦金卡",lowerbound: 533360800, upperbound: 533360802},
		{name:"藝FUN悠遊御璽卡",lowerbound: 471235360, upperbound: 471235369},
		{name:"藝FUN御璽卡",lowerbound: 471235370, upperbound: 471235372}
	);
	
	console.log("validate_chkStatus");
	var sFieldName = rules[i + 2];
	var carenum1 = rules[i + 3];
	var carenum2 = rules[i + 4];
	var carenum3 = rules[i + 5];
	var carenum4 = rules[i + 6];
	var carenum = $("#" + carenum1).val() + $("#" + carenum2).val() + $("#" + carenum3).val() + $("#" + carenum4).val();
	console.log(carenum);
	
	var regex = /[^0-9]/;
	var valid = regex.test(carenum);
	if (valid) {
		return options.allrules.onlyNumberSp.alertText;
	}
	if(carenum.length != 16){
		return "* 请输入正确信用卡号码";
	}
	if(carenum.substr(carenum.length - 3, 1) != '1'){
		return "* 限本行信用卡正卡客户申请(商务卡、采购卡、附卡除外)!";
	}
	var flg = false;

	for (var i = 0; i < ccr.length; i++) {
		var cardrange = ccr[i].lowerbound;
		var cardlow = cardrange.toString();
		cardrange = ccr[i].upperbound;
		var cardtop = cardrange.toString();
		var tmp = parseInt(carenum.substr(0, cardlow.length), 10);
		if (parseInt(cardlow, 10) <= tmp && tmp <= parseInt(cardtop, 10)) {
			flg = true;
			break;
		}
	}
	if(!flg){
		return "* 请使用一般信用卡正卡申请(不含商务卡、采购卡、附卡及VISA金融卡)!";
	}
}

/*
 * 檢查信用卡有效期限-年月	
 * PARAM sFieldName:欄位中文描述
 * PARAM YY:輸入框
 * PARAM MM:輸入框
 * return 
 * validate[funcCallRequired[validate_chkperiod['sFieldName',YY,MM]]]
 */
function validate_chkperiod(field, rules, i, options){
	
	console.log("validate_chkperiod");
	var sFieldName = rules[i + 2];
	var YY = rules[i + 3];
	var MM = rules[i + 4];
	
	var d = new Date();
	var y = d.getFullYear();
	var m = d.getMonth() + 1;
	
	if(parseInt(m,10) < 10){
		m = '0' + m;
	}
	
	if($("#"+YY).val().length != 2 && $("#"+MM).val().length != 2){
		return "* 请输入两码卡片有效期限(年、月)";
	}
	if($("#"+YY).val().length != 2){
		return "* 请输入两码卡片有效期限(年)";
	}
	if($("#"+MM).val().length != 2){
		return "* 请输入两码卡片有效期限(年)";
	}
	if(parseInt($("#"+MM).val(),10) > 12){
		return "* 请输入正确卡片有效期限(月)";
	}
	if(parseInt('20'+$("#"+YY).val()+$("#"+MM).val(),10) < parseInt(y+m,10)){
		return "* 卡片有效期限已过期!";
	}
}

/*
 * 檢查全形、半形
 * PARAM sFieldName:欄位中文描述
 * PARAM oField:輸入框
 * PARAM oType:輸入框(1為全形,0為半形)
 * return 
 * validate[required,funcCall[validate_CheckAddressFormat['sFieldName',oField,oType]]]
 */
function validate_CheckAddressFormat(field, rules, i, options){
	
	var sFieldName = rules[i + 2];
	var oField = rules[i + 3];
	var oType = rules[i + 4];
	if(oType == '1'){
		if($("#"+oField).val().match(/[\u0000-\u00ff]/g) != null){
			return sFieldName;
		}
	}
	if(oType == '0'){
		if($("#"+oField).val().match(/[\uff00-\uffff]/g) != null || $("#"+oField).val().match(/[\u4e00-\u9fa5]/g) != null){
			return sFieldName;
		}
	}
	if(oType == '3'){
		var regex = /[^0-9\-]/;
		var valid = regex.test($("#"+oField).val());
		if(valid){
			return "* " + sFieldName + "只能输入半形数字或 - ";
		}
	}
	if(oType == '4'){
		var regex1 = /[^0-9\-\#]/;
		var valid1 = regex1.test($("#"+oField).val());
		if(valid1){
			return "* " + sFieldName + "只能输入半形数字、# 或 - ";
		}
	}
	if(oType == '5'){
		var regex1 = /[^0-9]/;
		var valid1 = regex1.test($("#"+oField).val());
		if(valid1){
			return "* " + sFieldName + "只能输入半形数字";
		}
	}
}

/*
 * 檢查外匯交易之金額輸入框內容
 * PARAM sFieldName:欄位中文描述
 * PARAM oField:金額輸入框
 * PARAM bIsInteger:是否整數
 * PARAM fMin:最小值，不限制時給null
 * PARAM fMax:最大值，不限制時給null
 * PARAM amtType:轉帳金額性質
 * return 
 */
function validate_CheckFxAmount(field, rules, i, options) {
	var sFieldName = rules[i + 2];
	var oField = rules[i + 3];
	var bIsInteger = rules[i + 4];
//	var fMin = rules[i + 5];
//	var fMax = rules[i + 6];
	var ccy = rules[i + 5];
	var amtType = rules[i + 6];
	
    if  (typeof(sFieldName) != "string") {
		//alert("CheckAmount含有无效的参数sFieldName");
    	errorBlock(
				null, 
				null,
				["CheckAmount含有无效的参数sFieldName"], 
				'离开', 
				null
			);
		return false;
	}
//	if (fMin != null && fMax != null && fMin > fMax) {
//		alert("CheckAmount的參數fMin不能大於fMax");
//		return false;
//	}
	try {
	    var sAmount = $("#"+oField).val();
	    if (sAmount == "") {
	    	return "* 请输入" + sFieldName;
	    }
	    if (isNaN(sAmount) || sAmount.startsWith(' ') || sAmount.endsWith(' ')) {
	    	return "* " + sFieldName + "栏位请输入正确的金额格式";
	    }
		if (sAmount.indexOf("+") == 0) {
			return "* " + sFieldName + "栏位请勿输入加号";
	    }
		if (sAmount.charAt(0) == "0" && sAmount.length > 1) {
			return "* " + sFieldName + "栏位请勿以零为开头";
	    }
	    if (bIsInteger == true && sAmount.indexOf(".") != -1) {
	    	return "* " + sFieldName + "栏位请输入整数";
	    }
	    if (bIsInteger == false && sAmount.indexOf(".") != -1) {
	    	if ($("#"+ccy).val() == 'TWD' || $("#"+ccy).val() == 'JPY') {	
	    		var sAmtType = $("#" + amtType + " option:selected").val();
	    		return "* " + sAmtType.substring(0,2) + "币别为新台币或日圆时," + sFieldName + "栏位请勿包含小数位";	        	
	      	}  
	    }
//	    if (fMin != null && sAmount < fMin) {
//	    	return "* " + sFieldName + "不能小于" + fMin;
//	    }
//	    if (fMax != null && sAmount > fMax) {
//	    	return "* " + sFieldName + "不能大於" + fMax;
//	    }
	    
	} catch (exception) {
	    //alert("检核" + sFieldName + "时发生错误:" + exception);
		errorBlock(
				null, 
				null,
				["检核" + sFieldName + "时发生错误:" + exception], 
				'离开', 
				null
			);
		return false;
	}
	return true;
}

/*
 * TODO TypeError: Cannot read property 'test' of undefined
 * 檢查輸入框內容的位元組數(英文1，中文2)是否超過指定的最大值
 * PARAM sFieldName:欄位中文描述
 * PARAM oField:輸入框
 * PARAM iByteLength:最大位元組數
 * return 
 */
function validate_CheckMaxBytes(field, rules, i, options) {
	var sFieldName = rules[i + 2];
	var oField = rules[i + 3];
	var iByteLength = rules[i + 4];
	
    if  (typeof(sFieldName) != "string") {
		//alert("CheckMaxBytes含有无效的参数sFieldName");
    	errorBlock(
				null, 
				null,
				["CheckMaxBytes含有无效的参数sFieldName"], 
				'离开', 
				null
			);
		return false;
	}
	
	try {
	    var sValue = $("#"+oField).val();
	    
    	var maxbytes = new RegExp(/[^\x00-\xff]/ig);
	    var oArr = sValue.test(maxbytes); 
		var valueLength = oArr == null ? sValue.length : sValue.length + oArr.length; 
	    	    
	    if (valueLength > iByteLength) {
	    	return "* " + sFieldName + "超过长度限制，请重新输入";
	    }
	} catch (exception) {
	    //alert("检核" + sFieldName + "时发生错误:" + exception);
		errorBlock(
				null, 
				null,
				["检核" + sFieldName + "时发生错误:" + exception], 
				'离开', 
				null
			);
		return false;
	}
	
	return true;
}

/*
 * 检核輸入值是否為連續數字
 * PARAM sFieldName:欄位中文描述
 * PARAM oField:輸入框
 * return 
 */
function validate_chkSerialNum(field, rules, i, options) {
	var sFieldName = rules[i + 2];
	var oField = rules[i + 3];
	
	var str = $("#"+oField).val();
	if(str != '') {
		var iCode1 = str.charCodeAt(0); 
		if(iCode1 >=48 && iCode1 <= 57) {
			var iCode2 = 0;
			var bPlus = true;
			var bMinus = true;
			for(var i=1;i<str.length;i++) {
				iCode2 = str.charCodeAt(i);//now
				if(iCode2 != (iCode1+1)) {
					if(!(iCode2 == 48 && iCode1 == 57)) {
						bPlus = false;
					}
				}
				if(iCode2 != (iCode1-1)) {
					if(!(iCode2 == 57 && iCode1 == 48)) {
						bMinus = false;	
					}
				}
				iCode1 = iCode2;//before
			}
			if(bPlus || bMinus) return "* " + sFieldName + "不能为连号数字";
		}
	}
	return true;	
}

/*
 * 检核輸入值是否為相同之文數字
 * PARAM sFieldName:欄位中文描述
 * PARAM oField:輸入框
 * return 
 */
function validate_chkSameEngOrNum(field, rules, i, options) {
	var sFieldName = rules[i + 2];
	var oField = rules[i + 3];
	
	var str = $("#"+oField).val();
	if(str != '') {
		var iCode1 = str.charCodeAt(0);
		
		if((iCode1 >=48 && iCode1 <= 57) || (iCode1 >= 65 && iCode1 <= 90) || (iCode1 >= 97 && iCode1 <= 122)) {
			var iCode2 = 0;
			var bSame  = true;
			for(var i=0;i<str.length;i++) {
				iCode2 = str.charCodeAt(i);
				if(iCode1 != iCode2) {
					bSame = false;
				}
			}
		}
		if(bSame) {
			return "* " + sFieldName + "不得为全部相同之文数字";
		}
	}
	return true;
}

/*
 * 檢核輸入(自訂說明與欄位)
 * PARAM sFieldName:欄位中文描述
 * PARAM oField:輸入框
 * return 
 */
function validate_requiredWith(field, rules, i, options) {
	var sFieldName = rules[i + 2];
	var oField = rules[i + 3];
	
	var str = $("#"+oField).val();
	if(str == '') {
		return "* " + sFieldName;
	}
}
/*
 * 單行手機檢核
 * PARAM sFieldName:欄位中文描述
 * PARAM oField:輸入框
 * validate[funcCallRequired[validate_OneInputPhone['sFieldName',oField]]]
 * return 
 */
function validate_OneInputPhone(field, rules, i, options) {
	var sFieldName = rules[i + 2];
	var oField = rules[i + 3];
	
	var data = $("#"+oField).val();
	var re = new RegExp(/^\d{1,3}-\d{1,10}$/);
	if(!re.test(data)){
		return sFieldName;
	}
}


/*
 * 檢查非行員之欄位
 * PARAM sFieldName:欄位中文描述
 * PARAM oField:輸入框
 * validate[funcCallRequired[validate_TmailNotEmp[oField]]]
 * return 
 */
function validate_TmailNotEmp(field, rules, i, options) {
	var oField = rules[i + 2];
	
	var EMAIL = $("#"+oField).val();
	if("mail.tbb.com.tw"==(EMAIL.substring(EMAIL.indexOf("@")+1)).toLowerCase()){
		return "* 您留存之電子郵件信箱係為本行公務信箱，請確實留存您個人之電子郵件信箱。";
	}
}

/*
 * 檢核多個Select物件
 * PARAM sFieldName:欄位中文描述
 * PARAM aSelect:Select物件
 * PARAM aSelect:Select物件
 * PARAM aSelect:Select物件
 * PARAM sDoNotSelect:不能為選取值的字串，無限制時給null
 * return
 * validate[funcCall[validate_CheckSelect['sFieldName',aSelect,bSelect,cSelect,sDoNotSelect]]]
 */
function validate_CheckSelects(field, rules, i, options) {
	
	console.log("_CheckSelect");
	
	var sFieldName = rules[i + 2];
	var aSelect = rules[i + 3];
	var bSelect = rules[i + 4];
	var cSelect = rules[i + 5];
	var sDoNotSelect = rules[i + 6];
	
	if (typeof (sFieldName) != "string") {
		return "* CheckSelect含有无效的参数sFieldName";
	}
	
	try {
		
		var aSelected = document.getElementById(aSelect);
		var bSelected = document.getElementById(bSelect);
		var cSelected = document.getElementById(cSelect);
		var selectValuea = $("#" + aSelect + " option:selected").val();
		var selectValueb = $("#" + bSelect + " option:selected").val();
		var selectValuec = $("#" + cSelect + " option:selected").val();
		
		if (aSelected.length == 0 || (aSelected.length == 1 && aSelected[0].value == sDoNotSelect)
				|| bSelected.length == 0 || (bSelected.length == 1 && bSelected[0].value == sDoNotSelect)
				|| cSelected.length == 0 || (cSelected.length == 1 && cSelected[0].value == sDoNotSelect)) {
			return "* 没有可选择的" + sFieldName;
		}
		
		if (typeof (sDoNotSelect) == "string") {
			var bHasSelected = false;

			for (var i = 0; i < aSelected.length; i++) {
				var o = aSelected[i];
				if (o.selected) {
					bHasSelected = true;

					if (o.value == sDoNotSelect) {
						return "* 请选择" + sFieldName;
					}
				}
			}
			for (var i = 0; i < bSelected.length; i++) {
				var o = bSelected[i];
				if (o.selected) {
					bHasSelected = true;
					
					if (o.value == sDoNotSelect) {
						return "* 请选择" + sFieldName;
					}
				}
			}
			for (var i = 0; i < cSelected.length; i++) {
				var o = cSelected[i];
				if (o.selected) {
					bHasSelected = true;
					
					if (o.value == sDoNotSelect) {
						return "* 请选择" + sFieldName;
					}
				}
			}

			// 當所有的項目皆未選取時，比較第一個項目
			if (bHasSelected == false && (aSelected.length > 0 || bSelected.length > 0 || cSelected.length > 0)) {
				if (aSelected[0].value == sDoNotSelect) {
					return "* 请选择" + sFieldName;
				}
				if (bSelected[0].value == sDoNotSelect) {
					return "* 请选择" + sFieldName;
				}
				if (cSelected[0].value == sDoNotSelect) {
					return "* 请选择" + sFieldName;
				}
			}
		}
	} catch (exception) {
		//alert("检核" + sFieldName + "时发生错误:" + exception)
		errorBlock(
				null, 
				null,
				["检核" + sFieldName + "时发生错误:" + exception], 
				'离开', 
				null
			);
	}
}

/*
 * 檢查數字輸入框內容(不含加號、減號、小數點) for RWD的alert用
 * PARAM sFieldName:欄位中文描述
 * PARAM oField:數字輸入框
 * PARAM bCanEmpty:可否為空字串
 * return
 * validate[funcCallRequired[validate_CheckNumber1['sFieldName',oField,bCanEmpty]]]
 */
function validate_CheckNumber1(field, rules, i, options)
{
	console.log("validate_CheckNumber1");
	
	var sFieldName = rules[i + 2];
	var oField = rules[i + 3];
	var bCanEmpty = (rules[i + 4] == 'true');

    if  (typeof(sFieldName) != "string")
	{
//		console.log("CheckNumber含有無效的參數sFieldName");
		return sFieldName;
	}
	try
	{
	    var sNumber = $("#"+oField).val();
	    if (bCanEmpty == false && sNumber == "")
	    {
//	    	console.log("請輸入" + sFieldName);
	        return sFieldName;
	    }
	    var reNumber = /[^0-9]/;
	    if (reNumber.test(sNumber))
	    {
//	    	console.log(sFieldName + "欄位只能輸入數字(0~9)");
	        return sFieldName;
	    }
	}
	catch (exception)
	{
//		console.log("檢核" + sFieldName + "時發生錯誤:" + exception);
		return sFieldName;
	}
}
/*
 * 檢查數字輸入框內容(不含加號、減號、小數點),並依指定長度作檢核 for RWD的alert用
 * PARAM sFieldName:欄位中文描述
 * PARAM oField:數字輸入框
 * PARAM bCanEmpty:可否為空字串
 * PARAM len:長度限制
 * return
 * validate[funcCallRequired[validate_CheckNumWithDigit1['sFieldName',oField,bCanEmpty,len]]]
 */
function validate_CheckNumWithDigit1(field, rules, i, options)
{
	console.log("validate_CheckNumber1");
	
	var sFieldName = rules[i + 2];
	var oField = rules[i + 3];
	var bCanEmpty = (rules[i + 4] == 'true');
	var len = rules[i + 5];


   	if  (typeof(sFieldName) != "string")
	{
//   		console.log("CheckNumWithDigit含有無效的參數sFieldName");
		return sFieldName;
	}
	try
	{
	    var sNumber = $("#"+oField).val();
	    if (bCanEmpty == false && sNumber == "")
	    {
//	    	console.log("請輸入" + sFieldName);
	        return sFieldName;
	    }
	    
	    var reNumber = /[^0-9]/;
	    if (reNumber.test(sNumber))
	    {
//	    	console.log(sFieldName + "欄位只能輸入數字(0~9)");
	        return sFieldName;
	    }
	    
	    if (sNumber.length != len)
	    {
//	    	console.log(sFieldName + "欄位應為" + len + "位數字");
	        return sFieldName;	    
	    }
	}
	catch (exception)
	{
//		console.log("檢核" + sFieldName + "時發生錯誤:" + exception);
		return sFieldName;
	}
}

/*
 * 檢查金額輸入框內容 for RWD的alert用
 * PARAM sFieldName:欄位中文描述
 * PARAM oField:數字輸入框
 * PARAM bIsInteger:是否整數
 * PARAM fMin:最小值，不限制時給null
 * PARAM fMax:最大值，不限制時給null
 * return
 * validate[funcCallRequired[validate_CheckAmount1['sFieldName',oField,bIsInteger,fMin,fMax]]]
 */
function validate_CheckAmount1(field, rules, i, options)
{
	console.log("validate_CheckAmount1");
	
	var sFieldName = rules[i + 2];
	var oField = rules[i + 3];
	var bIsInteger = rules[i + 4];
	var fMin = rules[i + 5];
	var fMax = rules[i + 6];


    if  (typeof(sFieldName) != "string")
	{
//    	console.log("CheckAmount含有無效的參數sFieldName");
		return sFieldName;
	}
	
	if (fMin != null && fMax != null && fMin > fMax)
	{
//		console.log("CheckAmount的參數fMin不能大於fMax");
		return sFieldName;
	}
	
	try
	{
	    var sAmount = $("#"+oField).val();
	    
	    if (sAmount == "")
	    {
//	    	console.log("請輸入" + sFieldName);
	        return sFieldName;
	    }
	    
	    if (isNaN(sAmount) || sAmount.startsWith(' ') || sAmount.endsWith(' '))
	    {
//	    	console.log(sFieldName + "欄位請輸入正確的金額格式");
	        return sFieldName;
	    }
		
		if (sAmount.indexOf("+") == 0)
	    {
//			console.log(sFieldName + "欄位請勿輸入加號");
	        return sFieldName;  
	    }
		
		if (sAmount.charAt(0) == "0" && sAmount.length > 1)
	    {
//			console.log(sFieldName + "欄位請勿以零為開頭");
	        return sFieldName;  
	    }
	    
	    if (bIsInteger == true && sAmount.indexOf(".") != -1)
	    {
//	    	console.log(sFieldName + "欄位請輸入整數");
	        return sFieldName;  
	    }
	    
	    if (bIsInteger == false && sAmount.indexOf(".") == sAmount.length - 1)
	    {
//	    	console.log(sFieldName + "欄位請輸入正確的含小數點金額格式");
	        return sFieldName;  
	    }
	    
	    if (fMin != null && sAmount < fMin)
	    {
//	    	console.log(sFieldName + "不能小於" + fMin);
	        return sFieldName;
	    }
	    
	    if (fMax != null && sAmount > fMax)
	    {
//	    	console.log(sFieldName + "不能大於" + fMax);
	        return sFieldName;
	    } 
	}
	catch (exception)
	{
//		console.log("檢核" + sFieldName + "時發生錯誤:" + exception);
		return sFieldName;
	}
}
/*
 * 檢查台灣省水費 for RWD的alert用
 * PARAM oDate:代收期限
 * PARAM oWatno:銷帳編號
 * PARAM oCheckno:查核碼
 * PARAM oAmount:應繳總金額
 * return
 * validate[funcCallRequired[validate_CheckTaiwanWater['sFieldName',oDate,oWatno,oCheckno,oAmount]]]
 */
function validate_CheckTaiwanWater(field, rules, i, options)
{

	console.log("validate_CheckTaiwanWater");
	
	var sFieldName = rules[i + 2];
	var oDate = rules[i + 3];
	var oWatno = rules[i + 4];
	var oCheckno = rules[i + 5];
	var oAmount = rules[i + 6];
	
	try
	{
	    var sDate = $("#"+oDate).val();
	    var sWatno = $("#"+oWatno).val();
	    var sCheckno = $("#"+oCheckno).val();
	    var sAmount = $("#"+oAmount).val();

        var sYear = sDate.substring(1, 3);
		var sYear1 = sDate.substring(0, 3);
        var sMonth = sDate.substring(4, 6);
        var sDay = sDate.substring(7);
        
        var sAmount = "00000000" + sAmount;
        var sAmount = sAmount.substring(sAmount.length - 8);
        
		//檢查碼一檢核
		if(parseInt(sYear1)<100) {
        var sCheck = sYear.toString() + sMonth.toString() + sDay.toString() + sWatno.toString() + sAmount.toString();
	    //舊格式-代收期限六位
		var iBase = sCheck.charAt(0)*2 + sCheck.charAt(1)*3 + sCheck.charAt(2)*5 + sCheck.charAt(3)*7 + 
	                 sCheck.charAt(4)*2 + sCheck.charAt(5)*3 + sCheck.charAt(6)*5 + sCheck.charAt(7)*7 + 
	                 sCheck.charAt(8)*2 + sCheck.charAt(9)*3 + sCheck.charAt(10)*5 + sCheck.charAt(11)*7 + 
	                 sCheck.charAt(12)*2 + sCheck.charAt(13)*3 + sCheck.charAt(14)*5 + sCheck.charAt(15)*7 + 
	                 sCheck.charAt(16)*2 + sCheck.charAt(17)*3 + sCheck.charAt(18)*5 + sCheck.charAt(19)*7 +
	                 sCheck.charAt(20)*2 + sCheck.charAt(21)*3 + sCheck.charAt(22)*5 + sCheck.charAt(23)*7 + 
	                 sCheck.charAt(24)*2 + sCheck.charAt(25)*3 + sCheck.charAt(26)*5 + sCheck.charAt(27)*7;                 
	    
	    var iRemainder = iBase % 10;        
		}
		else {
		var sCheck = sYear1.toString() + sMonth.toString() + sDay.toString() + sWatno.toString() + sAmount.toString();
		//新格式-代收期限七位
	    var iBase =  sCheck.charAt(0)*2 + sCheck.charAt(1)*3 + sCheck.charAt(2)*5 + sCheck.charAt(3)*7 + 
	                 sCheck.charAt(4)*2 + sCheck.charAt(5)*3 + sCheck.charAt(6)*5 + sCheck.charAt(7)*7 + 
	                 sCheck.charAt(8)*2 + sCheck.charAt(9)*3 + sCheck.charAt(10)*5 + sCheck.charAt(11)*7 + 
	                 sCheck.charAt(12)*2 + sCheck.charAt(13)*3 + sCheck.charAt(14)*5 + sCheck.charAt(15)*7 + 
	                 sCheck.charAt(16)*2 + sCheck.charAt(17)*3 + sCheck.charAt(18)*5 + sCheck.charAt(19)*7 +
	                 sCheck.charAt(20)*2 + sCheck.charAt(21)*3 + sCheck.charAt(22)*5 + sCheck.charAt(23)*7 + 
	                 sCheck.charAt(24)*2 + sCheck.charAt(25)*3 + sCheck.charAt(26)*5 + sCheck.charAt(27)*7 + sCheck.charAt(28)*2;                 
	    
	    var iRemainder = iBase % 10;
        }	    
	    if (iRemainder.toString() != sCheckno)
	    {
//	    	console.log("輸入資料不符合台灣省水費檢查公式，請重新輸入");
	        return sFieldName;
	    }
	}
	catch (exception)
	{
//		console.log("檢核台灣省水費時發生錯誤:" + exception);
		return sFieldName;
	}
}
/*
 * 檢查是否為台企銀所發之信用卡
 * PARAM sFieldName:欄位中文描述
 * PARAM cardno:信用卡卡號
 * return
 * validate[funcCallRequired[validate_checkTBBCard1[sFieldName,cardno]]]
 */
function validate_checkTBBCard1(field, rules, i, options){

	var sFieldName = rules[i + 2];
	var oDate = rules[i + 3];
	var cardno = $("#" + oDate).val();
	var flg = false;

	for(var i = 0; i < ccr.length; i++) {
	    var cardrange= ccr[i].lowerbound;
	    var cardlow=cardrange.toString();
		cardrange = ccr[i].upperbound;
		var cardtop=cardrange.toString();
	    var tmp = parseInt(cardno.substr(0,cardlow.length), 10);
		if( parseInt(cardlow,10) <= tmp && tmp <= parseInt(cardtop,10)) {
			flg = true;
			break;
		}
	}
	if(!flg){
		return sFieldName;
	}
}

/*
 * 檢核確認欄位2是否與欄位1相同
 * PARAM sFieldName:欄位中文描述
 * PARAM oFieldId1:輸入框
 * PARAM oFieldId2:輸入框
 * return 
 * 欄位可空白:validate[funcCallRequired[validate_CheckSameInput['sFieldName',oFieldCarId1,oFieldCarId2]]]
 * 欄位不空白:validate[required,funcCall[validate_CheckSameInput['sFieldName',oFieldCarId1,oFieldCarId2]]]
 */
function validate_CheckSameInput(field, rules, i, options){

	var sFieldName = rules[i + 2];
	var oFieldId1 = rules[i + 3];
	var oFieldId2 = rules[i + 4];
	
    if($('#'+oFieldId2).val()!=$('#'+oFieldId1).val())
    {
    	$('#'+oFieldId2).val("");
		 return "您所输入的"+sFieldName+"有误!";
	} 
}

/*
 * 檢核指定名稱的CheckBox物件是否被點選
 * PARAM sFieldName:欄位中文描述
 * PARAM sRadioName:CheckBox物件名稱
 * return 
 * validate[funcCallRequired[validate_CheckBox['sFieldName',sCheckBoxName]]]
 */
function validate_CheckBox(field, rules, i, options) {
	
	console.log("_one_CheckBox");
	
	var sFieldName = rules[i + 2];
	var sCheckBoxName = rules[i + 3];

	if (typeof (sFieldName) != "string") {
		return "* CheckBox含有无效的参数sFieldName";
	}

	if (typeof (sCheckBoxName) != "string") {
		return "* CheckBox含有无效的参数sCheckBoxName";
	}

	try {
		var aCheckBox = document.getElementsByName(sCheckBoxName);
		if (aCheckBox.length == 0) {
			return "没有可勾选的" + sFieldName;
		}

		var bHasChecked = false;
		
		for (var i = 0; i < aCheckBox.length; i++) {
			if (aCheckBox[i].checked) {
				bHasChecked = true;
				break;
			}
		}

		if (!bHasChecked) {
			return "* 请勾选" + sFieldName;
		}
	} catch (exception) {
		//alert("檢核" + sFieldName + "時發生錯誤:" + exception);
		errorBlock(
				null, 
				null,
				["检核" + sFieldName + "时发生错误:" + exception], 
				'离开', 
				null
			);
	}
}

/*
 * 居留證檢核
 * PARAM sFieldName:欄位中文描述
 * PARAM sName:物件名稱
 * return 
 * validate[funcCallRequired[validate_CheckFxId['sFieldName',sName]]]
 */
function validate_CheckFxId(field, rules, i, options) {
	var oFieldId1 = rules[i + 3];
	var id=$('#'+oFieldId1).val().toUpperCase();//英文字母轉大寫
	var regex = /^[A-Z]/;
	tab = "ABCDEFGHJKLMNPQRSTUVWXYZIO"
	A1 = new Array (1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3 );
	A2 = new Array (0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5 );
	Mx = new Array (9,8,7,6,5,4,3,2,1,1);

	i1 = tab.indexOf( id.charAt(0) );
	if(regex.test(id.charAt(1))){
		i2 = tab.indexOf( id.charAt(1) );
	}
	else if(id.charAt(1) == '8' || id.charAt(1) == '9'){
		i2 = id.charAt(1);
	}
	else{
		return "* 居留证号格式错误";
	}
	      
	front_1 = A1[i1]*1 + A2[i1]*9;
	front_2 = A2[i2]*8;
	sum = front_1 + front_2;

	for ( i=2; i<10; i++ ) {
		v = parseInt( id.charAt(i) );
		if ( isNaN(v) ) return "* 居留证号格式错误";
		sum = sum + v * Mx[i];
	}
	if ( sum % 10 != 0 ) return "* 居留证号格式错误";
	return true;
}

/*
 * 檢查金額輸入框內容金額上下限是否為某值倍數
 * PARAM sFieldName:欄位中文描述
 * PARAM bIsInteger:是否整數
 * PARAM fMin:最小值，不限制時給null
 * PARAM fMax:最大值，不限制時給null
 * multiple:金額倍數  ex:輸入值須為1000的倍數 multiple=1000
 * return 
 * 檢查金額輸入框內容 PARAM ID:金額輸入框 PARAM Min:最小值 PARAM Max:最大值 return
 * validate[funcCallRequired[validate_CheckAmount[ 'sFieldName' , oField, fMin, fMax, multiple]]]
 */
function validate_Check_Amount_int(field, rules, i, options) {
	
	console.log("_CheckAmount");
	
	var sFieldName = rules[i + 2];
	var oField = rules[i + 3];
	var fMin = rules[i + 4];
	var fMax = rules[i + 5];
	var multiple = rules[i + 6];
	/**
	 * 因為用 validate 參數傳入為字串,必須轉型正確的型別
	 */
	var _fMin = "null" === fMin ? null: Number(fMin);
	var _fMax = "null" === fMax ? null: Number(fMax);
	var _multiple = "null" === multiple ? null: Number(multiple);
	
	if (typeof (sFieldName) != "string") {
		return "* CheckAmount含有无效的参数sFieldName";
	}
			
	try {
		
		var sAmount = $("#" + oField).val();
		bIsInteger = boolIsInteger(sAmount);
		
		if ( isNaN(sAmount)) {//數字
			return "* "+ sFieldName + "栏位请输入正确的金额格式";
		}
		
		//先TRIM掉，不要用startsWith與endsWith，在舊版瀏覽器會出錯
		//TODO:疑似用法錯誤，先改成下面方式
//		sAmount = sAmount.trim();
		sAmount = $.trim(sAmount);
		if (sAmount == "") {
			return "* "+ sFieldName + "栏位请输入正确的金额格式";
		}

		if (sFieldName.indexOf("+") == 0) {
			return "* "+ sFieldName + "栏位请勿输入加号";
		}

		if (sAmount.charAt(0) == "0" && sAmount.length > 1) {//check不以零为开头
			return "* "+ sFieldName + "栏位请勿以零为开头";
		}

		if (sAmount.indexOf(".") != -1) {
			return "* "+ sFieldName + "栏位请输入整数";
		}

		if(_fMin != null){
			if(parseInt(sAmount) <= parseInt(fMin)){
				return "* "+ sFieldName + "栏位输入金额需大于" + fMin + "元";
			}
		}

		if(_fMax != null){
			if(parseInt(sAmount) > parseInt(fMax)){
				return "* "+ sFieldName + "栏位输入金额最多为" + fMax + "元";
			}
		}

		if(_multiple != null){
			if(parseInt(sAmount % multiple) > parseInt(0)){
				return "* "+ sFieldName + "栏位输入金额得为" + multiple + "元之整倍数增加";
			}
		}

	} catch (exception) {
		//alert("检核" + sFieldName + "时发生错误:" + exception);
		errorBlock(
				null, 
				null,
				["检核" + sFieldName + "时发生错误:" + exception], 
				'离开', 
				null
			);
	}
}

/*
 * 手機檢核 09XXXXXXXX
 * PARAM field:欄位中文描述
 * PARAM rules:輸入框
 * validate[funcCallRequired[validate_OneInputPhone['field',rules]]]
 * return 
 */
function validate_cellPhone(field, rules, i, options) {
	var sFieldName = rules[i + 2];
	var oField = rules[i + 3];
	
	var data = $("#"+oField).val();
	var re = new RegExp( /^(09)[0-9]{8}$/);
	if(!re.test(data)){
		return sFieldName;
	}
}

/**
 * 輸入內容檢核，不能含有TAB
 * @param field
 * @param rules
 * @param i
 * @param options
 * @returns
 */
function validate_CheckSpecialCharacters(field, rules, i, options){
	
	var sFieldName = rules[i + 2];
	var oField = rules[i + 3];
	var regex = /\t/;
	
	var valid = regex.test($("#"+oField).val());
	if(valid){
		return "* " + sFieldName + "不能输入含有Tab的内容";
	}
}
