/**
 * @file FSTOP Javascript for select2 handling.
 * 
 * Mixin for select2.
 * 
 * 
 * 
 * <pre>
 * 文件產生:
 * 安裝 node.js 後，將 js 複製到另外的目錄以 jsdoc 產生文件
 * 指令： jsdoc fstop.web-x.x.x.js
 * </pre>
 * 
 * @copyright Fstop 2017
 * @version 1.0.1
 */

/**
 * Global namespace fstop
 * @namespace fstop
 */
var fstop = fstop || {};


fstop.web = (function(parent)
{
	
	var web = parent.web = parent.web || {};
	
	
    //============================ [select2 functions] ===========================
    
    /**
     * 將表單中 select2 元件的值清空.
     * 
     * @param id id containing select2
     */
    web.clearSelect2Value = function (id)
    {
  	    $("#"+id).select2("val", "").trigger('change');
  	    $("#"+id).select2('close');
    };
    
    /**
     * Add and set select2 value.
     * 
     * @param id select2 id
     * @param name select item name
     * @param value select item value
     * 
     */
    web.addAndSetSelect2Value = function (id, name, value)
    {
  		//$("<option value='" + value + "'>"+ name +"</option>").appendTo("#" + id);
        $("#" + id).append($("<option></option>").attr("value", value).text(name));
		$("#"+id).select2('val', value).trigger('change');
  		$("#"+id).select2('close');    	  		
    };
    
    /**
     * Set setect option .
     * 設定選項中被選取的項目.
     * 
     * @param id select2 id
     * @param name select item name (unused)
     * @param value select item value
     * 
     */
    web.setSelect2Value = function (id, name, value)
    {
  		$("#"+id).select2('val', value).trigger('change');
  		$("#"+id).select2('close');    	    	
    };
    
    
    /**
     * Clear and Set select2 value.
     * 會清掉原有的選項.
     * 
     * @param id select2 id
     * @param name select item name
     * @param value select item value 
     *  
     */
    web.clearAndSetSelect2Value = function (id, name, value)
    {
		$("#" + id).empty();
		//加入值 
        $("#" + id).append($("<option></option>").attr("value", value).text(name));
		$("#"+id).select2('val', value).trigger('change');
  		$("#"+id).select2('close');    	
    };

    /**
     * Get select2 selected text.
     * 
     * @param id select2 id
     */
    web.getSelect2Text = function (id)
    {
    	if ($('#' + id).select2('data'))
    	{
        	return $('#' + id).select2('data').text;    		
    	}
    	return "";
    };
	
	
	// Remember return web object!!
	return web;
	
})(fstop || {});