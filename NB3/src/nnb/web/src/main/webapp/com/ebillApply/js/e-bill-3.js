$( document ).ready( function () {

    $( "#validationForm" ).validate( {
    	ignore: ".ignore",
    	rules: {
            form_account: {
                required: true,
            },
            form_email: {
                required: true,
                email: true
            }
        },
        messages: {
            form_email: {
                email: "請輸入正確的Email"
            }
        },
        onfocusout: function(element) {
            if ($(element).html().match('filter-option') ) {
        
                $(element).closest('.bootstrap-select').on('changed.bs.select', function() {
                    $(this).find('select').valid();
                });

                $(element).closest('.bootstrap-select').on('hidden.bs.select', function() {
                    $(this).find('select').valid();
                });
                
            }else{
                this.element(element);
             }
        },
        errorElement: "label",
        errorPlacement: function ( error, element ) {
            error.addClass( "invalid-feedback" );

            if ( element.prop( "type" ) === "radio" || element.prop( "type" ) === "checkbox" ) {
                error.appendTo( element.closest('div div.form-check').parent() );
            }else{
                if ( element.prop( "type" ) !== "select-one") {
                    error.insertAfter( element );
                }
            }
        },
        highlight: function ( element, errorClass, validClass ) {
            $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
        },
        unhighlight: function (element, errorClass, validClass) {
            $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
        },
//        submitHandler: function(form) {
//            form.submit();
//        }
    } );
} );

var interval = null;
var counter = 121;
        function trigger_smscountdown() {
        	
            interval = setInterval(function() {
                counter--;
                $('#sms_countdown').html(counter + "秒");
                if (counter <= 0) {
                    clearInterval(interval);
                    $("#submitbtn").addClass("disabled");
                    $("#form_smscode").attr("disabled", true);
                    //本機測試用
                    $("#form_smscode").attr("disabled", false);
                    //本機測試用
                    return;
                }
            }, 1000);
        }

        $( document ).ready( function () {
            $('.submit').on('click', function(){
                clearInterval(interval);
                counter = 121;
                getsmsotp();
                trigger_smscountdown();
            });

            $('#sms-btn-resend').on('click', function(){
                clearInterval(interval);
                counter = 121;
                getsmsotp();
                trigger_smscountdown();
                $('#sms_countdown').html('發送中...');
            });

            $( "#SMSvalidationForm" ).validate( {
                rules: {
                    form_smscode: {
                        required: true,
                        rangelength: [8,8],
                        number: true
                    }
                },
                messages: {
                    form_smscode: {
                        rangelength: "請輸入8碼簡訊驗證碼",
                        number: "請輸入8碼簡訊驗證碼",
                    }
                },
                onfocusout: function(element) {
                    this.element(element);
                },
                onkeyup: function(element){
                    //this.element(element);
                    if ($("#SMSvalidationForm").validate().checkForm()) {
//                        $('form[id=SMSvalidationForm]').submit();
                    }
                },
                errorElement: "label",
                errorPlacement: function ( error, element ) {
                    error.addClass( "invalid-feedback" );
                    error.insertAfter( element );
                },
                highlight: function ( element, errorClass, validClass ) {
                    $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
                },
                unhighlight: function (element, errorClass, validClass) {
                    $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
                },
                submitHandler: function(form) { // <- pass 'form' argument in
                    form.submit(); // <- use 'form' argument here.
                }
            } );
        } );