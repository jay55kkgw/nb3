/**
 * @file FSTOP Javascript for web idle handling.
 * 
 * Mixin for web idle check.
 * Idle timeout default value is 60 seconds.
 * <pre>
 * Usage:
 *   fstop.web.gIdleTimeout = 10;						//setup idle timeout seconds
 *   fstop.web.customIdleAction = idleAction;			//custom idle timeout action
 *   fstop.web.customIdleProgress = customProgress;		//custom progress function 
 *   fstop.web.idleCheckStart();						//start idle check
 * Note:
 *   custom progress function :
 *     function(idle_expire_remain_seconds)   
 * 
 * </pre>
 * <pre>
 * 文件產生:
 * 安裝 node.js 後，將 js 複製到另外的目錄以 jsdoc 產生文件
 * 指令： jsdoc fstop.web-x.x.x.js
 * </pre>
 * 
 * @copyright Fstop 2017
 * @version 1.0.1
 */


/**
 * Global namespace fstop
 * @namespace fstop
 */
var fstop = fstop || {};


fstop.web = (function(parent)
{
	
	var web = parent.web = parent.web || {};
	
	var _localStorageKey = 'fstop_global_countdown_last_reset_timestamp';
	var _idleSecondsTimer = null;
	var _lastResetTimeStamp = (new Date()).getTime();
	var _localStorage = null;
	
	//== custom global and functions
	web.gIdleTimeout = 60; //seconds
	web.gCountDownID = "idle_expire_remain_seconds";

	web.customIdleAction = null;
	web.customIdleProgress = null;
	//--
	
	web.attachEvent = function (element, eventName, eventHandler) 
	{
	    if (element.addEventListener) 
	    {
	        element.addEventListener(eventName, eventHandler, false);
	        return true;
	    } 
	    else if (element.attachEvent) 
	    {
	        element.attachEvent('on' + eventName, eventHandler);
	        return true;
	    } 
	    else 
	    {
	        //nothing to do, browser too old or non standard anyway
	        return false;
	    }
	};
	
	web.resetIdleTime = function () 
	{
	    web.setLastResetTimeStamp((new Date()).getTime());
	};
	
	web.setLastResetTimeStamp = function (timeStamp) 
	{
	    if (_localStorage) 
	    {
	        _localStorage[_localStorageKey] = timeStamp;
	    } 
        _lastResetTimeStamp = timeStamp;
	};
	
	web.getLastResetTimeStamp = function () 
	{
	    var lastResetTimeStamp = 0;
	    if (_localStorage) 
	    {
	        lastResetTimeStamp = parseInt(_localStorage[_localStorageKey], 10);
	        if (isNaN(lastResetTimeStamp) || lastResetTimeStamp < 0)
	            lastResetTimeStamp = (new Date()).getTime();
	        
	    } 
	    else 
	    {
	        lastResetTimeStamp = _lastResetTimeStamp;
	    }

	    return lastResetTimeStamp;
	};
	
	web.writeIdleProgress = function (msg) 
	{
	    var oPanel = document.getElementById(web.gCountDownID);
	    if (oPanel)
	         oPanel.innerHTML = msg;
	    //else if (console)
	    //    console.log(msg);
	};	
	
	
	web.checkIdleTime = function () 
	{
	    var currentTimeStamp = (new Date()).getTime();
	    var lastResetTimeStamp = web.getLastResetTimeStamp();	    
	    var secondsDiff = Math.floor((currentTimeStamp - lastResetTimeStamp) / 1000);
	    var msg = (web.gIdleTimeout - secondsDiff) + "";
	    if (secondsDiff <= 0) 
	    {
	        web.resetIdleTime();
	        secondsDiff = 0;
	    }
	    
	    if (web.customIdleProgress)
	    {
	    	web.customIdleProgress(msg)
	    }
	    else
	    {
		    web.writeIdleProgress(msg);	    	
	    }
	    
	    if (secondsDiff >= web.gIdleTimeout) 
	    {
	        window.clearInterval(_idleSecondsTimer);
	        web.resetIdleTime();
	        if (web.customIdleAction)
	        {
	        	web.customIdleAction();
	        }
	        //alert("Time expired!");
	        //document.location.href = "logout.html";
	    }
	};
	
	web.idleCheckStart = function ()
	{
		try 
		{
		    _localStorage = window.localStorage;
		}
		catch (ex) 
		{
		}

		if (_idleSecondsTimer)
		{
			window.clearInterval(_idleSecondsTimer);
		}

		web.resetIdleTime();
		
		web.attachEvent(document, 'click', web.resetIdleTime);
		web.attachEvent(document, 'mousemove', web.resetIdleTime);
		web.attachEvent(document, 'keypress', web.resetIdleTime);
		web.attachEvent(window, 'load', web.resetIdleTime);

		_idleSecondsTimer = window.setInterval(web.checkIdleTime, 1000); //check every second

	};
	
	// Remember return web object!!
	return web;
	
})(fstop || {});