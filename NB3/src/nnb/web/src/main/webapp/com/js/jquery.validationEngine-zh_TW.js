(function($){
    $.fn.validationEngineLanguage = function(){
    };
    $.validationEngineLanguage = {
        newLang: function(){
            $.validationEngineLanguage.allRules = {
                "required": { // Add your regex rules here, you can take telephone as an example
                    "regex": "none",
                    "alertText": "* 此欄位不可空白",
                    "alertTextCheckboxMultiple": "* 請選擇一個項目",
                    "alertTextCheckboxe": "* 您必需勾選此欄位",
                    "alertTextDateRange": "* 日期範圍欄位都不可空白",
                    "alertRadio": "* 請點選"
                },
                "requiredInFunction": { 
                    "func": function(field, rules, i, options){
                        return (field.val() == "test") ? true : false;
                    },
                    "alertText": "* Field must equal test"
                },
                "dateRange": {
                    "regex": "none",
                    "alertText": "* 無效的 ",
                    "alertText2": " 日期範圍"
                },
                "dateTimeRange": {
                    "regex": "none",
                    "alertText": "* 無效的 ",
                    "alertText2": " 時間範圍"
                },
                "pwcolumn": {
                    "regex": "none",
                    "alertText": "* 與原密碼不能相同",
                    "alertText2": "* 與新密碼不相同",
                    "alertText3": "* 簽入密碼與交易密碼不能相同",
                    "alertText4": "* 不得與使用者名稱相同"
                },
                "acctcolumn": {
                    "regex": "none",
                    "alertText": "* 與原使用者名稱不能相同",
                    "alertText2": "* 與新使用者名稱不相同",                    	
                },
                "minSize": {
                    "regex": "none",
                    "alertText": "* 最少 ",
                    "alertText2": " 個字元"
                },
                "maxSize": {
                    "regex": "none",
                    "alertText": "* 最多 ",
                    "alertText2": " 個字元"
                },
				"groupRequired": {
                    "regex": "none",
                    "alertText": "* 你必需選填其中一個欄位"
                },
                "min": {
                    "regex": "none",
                    "alertText": "* 最小值為 "
                },
                "max": {
                    "regex": "none",
                    "alertText": "* 最大值為 "
                },
                "past": {
                    "regex": "none",
                    "alertText": "* 日期必須早於 "
                },
                "future": {
                    "regex": "none",
                    "alertText": "* 日期必須晚於 "
                },
                "verification_date": {
                    "regex":"none",
                    "alertText": "* 日期或格式錯誤，格式必需為 YYYY/MM/DD"
                },
                "maxCheckbox": {
                    "regex": "none",
                    "alertText": "* 最多選取 ",
                    "alertText2": " 個項目"
                },
                "minCheckbox": {
                    "regex": "none",
                    "alertText": "* 請選取 ",
                    "alertText2": " 個項目"
                },
                "equals": {
                    "regex": "none",
                    "alertText": "* 欄位內容不相符",
                    "CN19": "* 您所輸入的下載密碼與確認下載密碼不同，請重新檢查!",
                    "N1010_CP": "*您兩次輸入的新密碼不一致!"
                },
                "notEquals": {
                    "regex": "none",
                    "N1010_CP": "*新密碼與原密碼不能相同"
                },
                "creditCard": {
                    "regex": "none",
                    "alertText": "* 無效的信用卡號碼"
                },
                "phone": {
                    // credit: jquery.h5validate.js / orefalo
                    "regex": /^([\+][0-9]{1,3}([ \.\-])?)?([\(][0-9]{1,6}[\)])?([0-9 \.\-]{1,32})(([A-Za-z \:]{1,11})?[0-9]{1,4}?)$/,
                    "alertText": "* 無效的電話號碼"
                },
                "email": {
                    // Shamelessly lifted from Scott Gonzalez via the Bassistance Validation plugin http://projects.scottsplayground.com/email_address_validation/
                    "regex": /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i,
                    "alertText": "* 無效的電子郵件"
                },
                "integer": {
                    "regex": /^[\-\+]?\d+$/,
                    "alertText": "* 請輸入0-9數字"
                },
                "number": {
                    // Number, including positive, negative, and floating decimal. credit: orefalo
                    "regex": /^[\-\+]?((([0-9]{1,3})([,][0-9]{3})*)|([0-9]+))?([\.]([0-9]+))?$/,
                    "alertText": "* 無效的數字"
                },
                "date": {
                    "regex": /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/,
                    "alertText": "* 無效的日期，格式必需為 YYYY-MM-DD"
                },
//                "twdate": {
//                	"regex": /^[\0]\/\d{3}/,
//                	"alertText": "* 無效的日期，格式必需為 YYYY-MM-DD"
//                },
                "ipv4": {
                    "regex": /^((([01]?[0-9]{1,2})|(2[0-4][0-9])|(25[0-5]))[.]){3}(([0-1]?[0-9]{1,2})|(2[0-4][0-9])|(25[0-5]))$/,
                    "alertText": "* 無效的 IP 位址"
                },
                "url": {
                    "regex": /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i,
                    "alertText": "* Invalid URL"
                },
                "onlyNumberSp": {
                    "regex": /^[0-9\ ]+$/,
                    "alertText": "* 只能填數字"
                },
                "onlyLetterSp": {
                    "regex": /^[a-zA-Z\ \']+$/,
                    "alertText": "* 只接受英文字母大小寫"
                },
				"onlyLetterAccentSp":{
                    "regex": /^[a-z\u00C0-\u017F\ \']+$/i,
                    "alertText": "* 只接受英文字母大小寫"
                },
                "onlyLetterNumber": {
                    "regex": /^[0-9a-zA-Z]+$/,
                    "alertText": "* 不接受特殊字元"
                },
                // --- CUSTOM RULES -- Those are specific to the demos, they can be removed or changed to your likings
                "ajaxUserCall": {
                    "url": "ajaxValidateFieldUser",
                    // you may want to pass extra data on the ajax call
                    "extraData": "name=eric",
                    "alertText": "* 此名稱已經被其他人使用",
                    "alertTextLoad": "* 正在確認名稱是否有其他人使用，請稍等。"
                },
				"ajaxUserCallPhp": {
                    "url": "phpajax/ajaxValidateFieldUser.php",
                    // you may want to pass extra data on the ajax call
                    "extraData": "name=eric",
                    // if you provide an "alertTextOk", it will show as a green prompt when the field validates
                    "alertTextOk": "* 此帳號名稱可以使用",
                    "alertText": "* 此帳號名稱已經被其他人使用",
                    "alertTextLoad": "* 正在確認帳號名稱是否有其他人使用，請稍等。"
                },
                "ajaxNameCall": {
                    // remote json service location
                    "url": "ajaxValidateFieldName",
                    // error
                    "alertText": "* 此名稱可以使用",
                    // if you provide an "alertTextOk", it will show as a green prompt when the field validates
                    "alertTextOk": "* 此名稱已經被其他人使用",
                    // speaks by itself
                    "alertTextLoad": "* 正在確認名稱是否有其他人使用，請稍等。"
                },
				 "ajaxNameCallPhp": {
	                    // remote json service location
	                    "url": "phpajax/ajaxValidateFieldName.php",
	                    // error
	                    "alertText": "* 此名稱已經被其他人使用",
	                    // speaks by itself
	                    "alertTextLoad": "* 正在確認名稱是否有其他人使用，請稍等。"
	                },
                "validate2fields": {
                    "alertText": "* 請輸入 HELLO"
                },
	            //tls warning:homegrown not fielded 
                "dateFormat":{
                    "regex": /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$|^(?:(?:(?:0?[13578]|1[02])(\/|-)31)|(?:(?:0?[1,3-9]|1[0-2])(\/|-)(?:29|30)))(\/|-)(?:[1-9]\d\d\d|\d[1-9]\d\d|\d\d[1-9]\d|\d\d\d[1-9])$|^(?:(?:0?[1-9]|1[0-2])(\/|-)(?:0?[1-9]|1\d|2[0-8]))(\/|-)(?:[1-9]\d\d\d|\d[1-9]\d\d|\d\d[1-9]\d|\d\d\d[1-9])$|^(0?2(\/|-)29)(\/|-)(?:(?:0[48]00|[13579][26]00|[2468][048]00)|(?:\d\d)?(?:0[48]|[2468][048]|[13579][26]))$/,
                    "alertText": "* 無效的日期格式"
                },
                //tls warning:homegrown not fielded 
				"dateTimeFormat": {
	                "regex": /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])\s+(1[012]|0?[1-9]){1}:(0?[1-5]|[0-6][0-9]){1}:(0?[0-6]|[0-6][0-9]){1}\s+(am|pm|AM|PM){1}$|^(?:(?:(?:0?[13578]|1[02])(\/|-)31)|(?:(?:0?[1,3-9]|1[0-2])(\/|-)(?:29|30)))(\/|-)(?:[1-9]\d\d\d|\d[1-9]\d\d|\d\d[1-9]\d|\d\d\d[1-9])$|^((1[012]|0?[1-9]){1}\/(0?[1-9]|[12][0-9]|3[01]){1}\/\d{2,4}\s+(1[012]|0?[1-9]){1}:(0?[1-5]|[0-6][0-9]){1}:(0?[0-6]|[0-6][0-9]){1}\s+(am|pm|AM|PM){1})$/,
                    "alertText": "* 無效的日期或時間格式",
                    "alertText2": "可接受的格式： ",
                    "alertText3": "mm/dd/yyyy hh:mm:ss AM|PM 或 ", 
                    "alertText4": "yyyy-mm-dd hh:mm:ss AM|PM"
	            },
	            "branchId": {
                    "regex": "none",
                    "alertText": "* 無此分行"
                },
	            "twDate": {
                    "regex": "none",
                    "alertText": "* 請確認是否輸入民國年。<br>e.g. 民國103年請輸入 '0103'",
                    "alertText2": "* 日期格式錯誤",
                    "alertText3": "* 日期格式錯誤。e.g. 01030101"
                },
                "twYear": {
                    "regex": "none",
                    "alertText": "* 請確認是否輸入民國年。<br>e.g. 民國103年請輸入 '0103'",
                    "alertText2": "* 日期格式錯誤",
                    "alertText3": "* 日期格式錯誤。e.g. 01030101"
                },
	            "decimal": {
                    "regex": "none",
                    "alertText": "* 錯誤的格式，請輸入"
                },
	            "groupCheckbox": {
                    "regex": "none",
                    "alertText": "* 至少選擇其中一項"
                },
                "userId": {
                	"regex": "none",
                	"alertText": ""
                },
                "notChinese": {
                	"regex": "none",
                    "alertText": "* 無效的中文字符"
                },
                "twIDEasy": {
                	"regex": /^[A-Z]{1}(2|1){1}[0-9]{8}$/,
                	"regex":"none",
                	"alertText": "* 身分證字號格式錯誤",
                	"alertText1": "* 請輸入正卡持卡人身分證字號/統一編號",
                	"alertText2": "* 資料有誤，請重新輸入!"
                },
                "installmentSavingAmount": {
                	"regex": "none",
                	"alertText": "* 轉帳金額必須是零存整付金額的倍數。"
                },
                "notContinueOrSame":{
                	"regex": "none",
                	"alertText1": "* 密碼不得為3個以上連續英文或連號數字",
                	"alertText2": "* 密碼不得為2個以上相同英文或數字",
                	"alertText3": "* 密碼不得為相同英文或數字"
                },
                "notSPCha":{
                	"regex": /^[A-Za-z0-9_.@\u2E80-\u9FFF]+$/,
                	"alertText": "* 不接受特殊字元"
                },
                "CapitalAndNum":{
                	"regex": /^[A-Z0-9]+$/,
                	"alertText": "* 轉入帳號欄位只能輸入合法字元"
                },
                "timeScopeMsg":{
                	"regex": "none",
                	"alertText1": "* 起始日不能大於 ",
                	"alertText2": "* 起始日不能小於 ",
                	"alertText3": "* 終止日不能大於 ",
                	"alertText4": "* 終止日不能小於起始日"
                },
                "sameMailMsg":{
                	"alertText": "*  新舊電子信箱不可相同"
                },
                
            };
        }
    };
    $.validationEngineLanguage.newLang();
})(jQuery);
