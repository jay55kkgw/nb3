// Avoid `console` errors in browsers that lack a console.
(function () {
	var method;
	var noop = function () { };
	var methods = [
		'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
		'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
		'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
		'timeStamp', 'trace', 'warn'
	];
	var length = methods.length;
	var console = (window.console = window.console || {});

	while (length--) {
		method = methods[length];

		// Only stub undefined methods.
		if (!console[method]) {
			console[method] = noop;
		}
	}
}());

// Place any jQuery/helper plugins in here.

; (function ($) {
	var settings = {

	},
	methods = {
		init: function (defaults) {
			var options = $.extend({}, settings, defaults);

			return this.each(function (i, el) {
				var $el = $(el);
				$el.shift = false;
				$el.capslock = false;

				var symbolArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
				symbolArray.sort(function () { return Math.random() > 0.5 ? -1 : 1; });
				$('[data-role="symbol"]').each(function (i, el) {
					$(el).attr('data-key', symbolArray[i]);
					$(el).attr('data-key-caps', symbolArray[i]);
					$(el).text(symbolArray[i]);
				});

				var letterArray = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
				letterArray.sort(function () { return Math.random() > 0.5 ? -1 : 1; });
				$('[data-role="letter"]').each(function (i, el) {
					$(el).attr('data-key', letterArray[i]);
					$(el).attr('data-key-caps', String(letterArray[i]).toUpperCase());
					$(el).text(letterArray[i]);
				});

				$('[data-role]', el).on('click', function (e) {
					e.stopImmediatePropagation();
					e.preventDefault();
				});

				$('[data-role]', el).mousedown(function (e) {
					e.stopImmediatePropagation();
					e.preventDefault();

					var $this = $(this),
						character = $this.html();

					var $write = $('textarea:focus, :input:focus:not(:checkbox, :radio, :button, :submit, :reset)');

					// Confirm
					if ($this.data('role') == 'confirm') {
						methods.focus($write);
						return false;
					}

					// Caps lock
					if ($this.data('role') == 'capslock') {
						$('[data-role="letter"]').toggleClass('uppercase');
						$el.capslock = true;
						methods.focus($write);
						return false;
					}

					if ($write.length == 0) return false;

					// Delete
					if ($this.data('role') == 'delete') {
						var html = $write.val();
						$write.val(html.substr(0, html.length - 1));
						methods.focus($write);
						return false;
					}

					// reset
					if ($this.data('role') == 'reset') {
						var html = $write.val();
						$write.val(html.substr(0, html.length - html.length));
						methods.focus($write);
						return false;
					}

					// Uppercase letter
					if ($this.hasClass('uppercase')) character = character.toUpperCase();

					// Add the character
					if ($write.is(':input')) {
						$write.val($write.val() + character);
					} else if ($write.is('textarea')) {
						$write.html($write.html() + character);
					}
					methods.focus($write);
				});
			});
		},
		focus: function (el) {
			if (!window.getSelection) {
				setTimeout(function () {
					el.focus();
					sel = document.selection.createRange();
					var value = el.val();
					el.val('');
					sel.text = value;
					el.focus();
				}, 10);
			}
		}
	};

	$.fn.keyboard = function (methodOrOptions) {
		if (methods[methodOrOptions]) {
			return methods[methodOrOptions].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + methodOrOptions + ' does not exist on jQuery.magicLine');
		}
	};
})(jQuery);
