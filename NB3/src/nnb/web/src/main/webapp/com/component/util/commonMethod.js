var commonMethod_uri;
// 讀卡機名稱 (用來判斷機型)
var cardReadersName;

// 交易機制使用IKEY
function useIKey(){
	console.log("IKey...");
	
	var jsondc = $("#jsondc").val();
	console.log("jsondc: " + jsondc);
	
	// IKEY驗證流程
	uiSignForPKCS7(jsondc);
}

// 交易機制使用讀卡機
function useCardReader(capUri){
	console.log("CardReader...");
	tmp_uri=capUri.split("/");
	console.log(tmp_uri);
	commonMethod_uri="/"+tmp_uri[1];
	console.log(commonMethod_uri);
	// 驗證碼驗證
	var capData = $("#formId").serializeArray();
	var capResult = fstop.getServerDataEx(capUri, capData, false);
	console.log("chaCode_valid: " + JSON.stringify(capResult) );
	
	// 驗證結果
	if (capResult.result) {

		// 開始讀卡機驗證流程
		listReaders();
		// 讀卡機驗證流程失敗重新產生驗證碼
		//2019/09/23 因驗證完晶片金融卡後就會form submit 不須再度換驗證碼 故註解
		//changeCode();
		
	} else {
		// 失敗重新產生驗證碼
		alert("驗證碼有誤");
		changeCode();
	}
}

//IKEY簽章
function uiSignForPKCS7(data){
	if(window.console){console.log("uiSignForPKCS7...");}
	var formId = document.getElementById("formId");
	SLBForSeconds("MaskArea",60000);
	UISignForPKCS7(data,formId.pkcs7Sign,"uiSignForPKCS7Finish");
}
//IKEY簽章結束
function uiSignForPKCS7Finish(result){
	if(window.console){console.log("uiSignForPKCS7Finish...");}
	//成功
	if(result != "false"){
		//繼續做
		SubmitForm();
	}
	//失敗
	else{
		unBlockUI(initBlockId);
		alert("IKEY簽章失敗");
		ShowLoadingBoard("MaskArea",false);
	}
}

//取得讀卡機
function listReaders(){
	if(window.console){console.log("listReaders...");}
	var CardReInsert = true;
	var GoQueryPass = true;
	ConnectCard("listReadersFinish");
}
//取得讀卡機結束
function listReadersFinish(result){
	if(window.console){console.log("listReadersFinish...");}
	//成功
	if(result != "false" && result != "E_Send_11_OnError_1006"){
		cardReadersName = result;
		//找出有插卡的讀卡機
		findOKReader();
	}
}
//找出有插卡的讀卡機
function findOKReader(){
	if(window.console){console.log("findOKReader...");}
	FindOKReader("findOKReaderFinish");
}

var OKReaderName = "";

//找出有插卡的讀卡機結束(有找到才會到此FUNCTION)
function findOKReaderFinish(okReaderName){
	if(window.console){console.log("findOKReaderFinish...");}
	//ASSIGN到全域變數
	OKReaderName = okReaderName;
	
	//拔插卡改到輸入密碼之後
//	removeThenInsertCard();
	
	// 不拔插先驗證密碼，GenerateTAC前會做拔插
	getCardBankID();
}

//取得卡片銀行代碼
function getCardBankID(){
	if(window.console){console.log("getCardBankID...");}
	GetUnitCode(OKReaderName,"getCardBankIDFinish");
}
//取得卡片銀行代碼結束
function getCardBankIDFinish(result){
	if(window.console){console.log("getCardBankIDFinish...");}
	if(window.console){console.log("result: " + result);}
	
	//成功
	if(result != "false"){
		//還要另外判斷是否為本行卡
		//是
		if(result == "05000000"){
			var formId = document.getElementById("formId");
			formId.ISSUER.value = result;
			
			// 驗證卡片密碼
			showDialog("verifyPin",OKReaderName,"verifyPinFinish");
			
			// 改成新機制 (二代機由鍵盤輸入密碼)
//			var readerType = getReaderType(cardReadersName,"","readerType");
//			if(readerType=="1"){
//				showDialog("verifyPin",OKReaderName,"verifyPinFinish");
//			}else{
//				SubmitForm();
//			}
			
		}
		//不是
		else{
			alert(GetErrorMessage("E005"));
		}
	}
}

var pazzword = "";

//驗證卡片密碼
function verifyPin(readerName,password,outerCallBackFunction){
	//ASSIGN到全域變數
	pazzword = password;
	
	VerifyPin(readerName,password,outerCallBackFunction);
}
//驗證卡片密碼結束
function verifyPinFinish(result){
	//成功
	if(result == "true"){
		// 繼續做
		SubmitForm();
	}
}

// 送交前押碼
function SubmitForm(){
	if(window.console){console.log("SubmitForm...");}
	
	//alert(" === SubmitForm === ");
	var oCMCARD = document.getElementById("CMCARD");
	//CRC.finalSendout(document.getElementById('MaskArea'), true);
	
	FinalSendout("MaskArea",true);
	
	if (oCMCARD != null && oCMCARD.checked){
		//取得卡片主帳號
		getMainAccount();
	}
	else{
		// 遮罩
		initBlockUI();

		var formId = document.getElementById("formId");
		formId.submit();
	}
	//var ACN_Str1 = formId.ACN.value;
	//if(ACN_Str1.length > 11)ACN_Str1 = ACN_Str1.substr(ACN_Str1.length - 11);

	//formId.ACN.value = ACN_Str1;
	//formId.OUTACN.value = ACN_Str1;
}
//取得卡片主帳號
function getMainAccount(){
	if(window.console){console.log("getMainAccount...");}
	GetMainAccount(OKReaderName,"getMainAccount_beforeFinish");
}
//驗卡片身分
function getMainAccount_beforeFinish(result){
	var uri = "";
	if(result != "false"){
	if (null != commonMethod_uri && commonMethod_uri!="")uri=commonMethod_uri+"/COMPONENT/component_acct_aj";
	else uri="/nb3/COMPONENT/component_acct_aj"
	var data=result.substring(5);
	var rdata = { ACN:data   };
	fstop.getServerDataEx(uri, rdata, true, CheckIdResult,null,result);
	}
}
function CheckIdResult(data,result){
	showTempMessage(500,'晶片金融卡身份查驗',"","MaskArea",false);
	console.log(data);
	if("0" == data.msgCode) {
		console.log("GOODPEOPLE");
		getMainAccountFinish(result);
	}else{
		//unBlockUI(initBlockId);
		try{
			if(data.data.msgCode == "0"){
		        errorBlock(
					null,
					null,
					["非本人帳戶之晶片金融卡，無法進行此功能"], 
					'確定', 
					null
				);
			}else{
		        errorBlock(
						null,
						null,
						[ data.data.msgCode + " " + data.data.msgName ], 
						'確定', 
						null
					);
			}
		}catch (e) {
	        errorBlock(
				null,
				null,
				["非本人帳戶之晶片金融卡，無法進行此功能"], 
				'確定', 
				null
			);
		}
		changeCode();
	}
}
//取得卡片主帳號結束
function getMainAccountFinish(result){
	if(window.console){console.log("getMainAccountFinish...");}
	unBlockUI(initBlockId);
	//成功
	if(result != "false"){
		var formId = document.getElementById("formId");

		formId.ACNNO.value = result;


		//拔插卡
		removeThenInsertCard();

	}
	//失敗
	else{
//		unBlockUI(initBlockId);
		FinalSendout("MaskArea",false);
	}
}

//拔插卡
function removeThenInsertCard(){
	console.log("removeThenInsertCard.OKReaderName: " + OKReaderName);
	RemoveThenInsertCard("MaskArea", 60, OKReaderName, "removeThenInsertCardFinish");
}
//拔插卡結束(成功才會到此FUNCTION)
function removeThenInsertCardFinish(){
	//拔插後繼續
	preGenerateTAC();
}

// 準備押碼所需資料
function preGenerateTAC(){
	if(window.console){console.log("preGenerateTAC...");}
	
	var TRMID = MakeTRMID();
	
	var formId = document.getElementById("formId");
	formId.TRMID.value = TRMID;
	
	var transData = "2500" + TRMID + formId.ACNNO.value;
	
	// 小鍵盤輸入密碼
	generateTAC(cardReadersName, pazzword, transData);
	
	// 改成新機制 (二代機由鍵盤輸入密碼、generateTAC驗密碼、元件遮罩)
//	var readerType = getReaderType(cardReadersName,"","readerType");
//	if(readerType=="1"){
//		// 一代機
//		generateTAC(cardReadersName, pazzword, transData);
//	} else {
//		// 二代機
//		alert("請於讀卡機鍵盤輸入密碼");
//		generateTAC(cardReadersName, "******", transData);
//	}
	
}

//卡片押碼
function generateTAC(OKReaderName, pazzword, transData){
	if(window.console){console.log("generateTAC...");}
	GenerateTAC(OKReaderName, transData, pazzword, "generateTACFinish");
}

//卡片押碼結束
function generateTACFinish(result){
	console.log(result)
	if(window.console){console.log("generateTACFinish...");}
	//成功
	if(result != "false"){
		// e.x. E000,00000551,6BF84A4B9319B145A64F3866506D3313594B10D08CDEA863BFA8F9D6
		var TACData = result.split(",");
		
		var formId = document.getElementById("formId");
		formId.iSeqNo.value = TACData[1];
		formId.ICSEQ.value = TACData[1];
		formId.TAC.value = TACData[2];
		
		// 遮罩
		initBlockUI();
		
		formId.submit();
	}
	//失敗
	else{
		unBlockUI(initBlockId);
		FinalSendout("MaskArea",false);
	}
}

// 從讀卡機名稱判斷是幾代機
function getReaderType(readerName,readerNameElementID,readerTypeElementID){
	if(window.console){console.log("getReaderType...");}
    var readerType = "1";
    
	if(readerName.indexOf("CASTLES EZpad") >= 0){
        readerType = "2";
    }
	else if(readerName.indexOf("Todos eCode Connectable") >= 0){
        readerType = "2";
    }
	else if(readerName.indexOf("Todos eCode Connectable II") >= 0){
        readerType = "2";
    }
	else if(readerName.indexOf("ACS ACR83U") >= 0){
        readerType = "2";
    }
	else if(readerName.indexOf("ACS APG8201") >= 0){
        readerType = "2";
    }
	if(readerNameElementID != ""){
		document.getElementById(readerNameElementID).value = readerName;
	}
	if(readerTypeElementID != ""){
		document.getElementById(readerTypeElementID).value = readerType;
	}
	
	if(window.console){console.log("readerType: " + readerType);}
    return readerType;
}



