// 交易機制使用IKEY
function useIKey(){
	console.log("IKey...");
	
	var jsondc = $("#jsondc").val();
	console.log("jsondc: " + jsondc);
	
	// IKEY驗證流程
	uiSignForPKCS7(jsondc);
}

// 交易機制使用讀卡機
function useCardReader(){
	console.log("CardReader...");
	
	var capUri = urihost + "/CAPCODE/captcha_valided_trans";
	
	// 驗證碼驗證
	var capData = $("#formId").serializeArray();
	var capResult = fstop.getServerDataEx(capUri, capData, false);
	console.log("chaCode_valid: " + JSON.stringify(capResult) );
	
	// 驗證結果
	if (capResult.result) {

		// 開始讀卡機驗證流程
		listReaders();
		// 讀卡機驗證流程失敗重新產生驗證碼
		//2019/09/23 因驗證完晶片金融卡後就會form submit 不須再度換驗證碼 故註解
		//changeCode();
		
	} else {
		// 失敗重新產生驗證碼
//		alert("驗證碼有誤");
        errorBlock(
				null,
				null,
				["驗證碼有誤"], 
				'確定', 
				null
			);
		changeCode();
	}
}

// 自然人憑證
function useNatural(){
	console.log("NaturalCertificate...");
	
	var capUri = urihost + "/CAPCODE/captcha_valided_trans";
	
	// 驗證碼驗證
	var capData = $("#formId").serializeArray();
	var capResult = fstop.getServerDataEx(capUri, capData, false);
	console.log("chaCode_valid: " + JSON.stringify(capResult) );
	
	// 驗證結果
	if (capResult.result) {

		// 開始讀卡機驗證流程
		processQuery2();
		// 讀卡機驗證流程失敗重新產生驗證碼
		//2019/09/23 因驗證完晶片金融卡後就會form submit 不須再度換驗證碼 故註解
		//changeCode();
		
	} else {
		// 失敗重新產生驗證碼
//		alert("驗證碼有誤");
        errorBlock(
				null,
				null,
				["驗證碼有誤"], 
				'確定', 
				null
			);
		changeCode();
	}
}


//--------------------------------------------------------------------------------------------------------------------------------------

//IKEY簽章
function uiSignForPKCS7(data){
	var formId = document.getElementById("formId");
	SLBForSeconds("MaskArea",60000);
	UISignForPKCS7(data,formId.pkcs7Sign,"uiSignForPKCS7Finish");
}
//IKEY簽章結束
function uiSignForPKCS7Finish(result){
	//成功
	if(result != "false"){
		//繼續做
		SubmitForm();
	}
	//失敗
	else{
//		alert("IKEY簽章失敗");
        errorBlock(
				null,
				null,
				["IKEY簽章失敗"], 
				'確定', 
				null
			);
		ShowLoadingBoard("MaskArea",false);
	}
}

//--------------------------------------------------------------------------------------------------------------------------------------

//取得讀卡機
function listReaders(){
	ConnectCard("listReadersFinish");
}
//取得讀卡機結束
function listReadersFinish(result){
	//成功
	if(result != "false" && result != "E_Send_11_OnError_1006"){
		//找出有插卡的讀卡機
		findOKReader();
	}
}
//找出有插卡的讀卡機
function findOKReader(){
	FindOKReader("findOKReaderFinish");
}
var OKReaderName = "";
//找出有插卡的讀卡機結束(有找到才會到此FUNCTION)
function findOKReaderFinish(okReaderName){
	//ASSIGN到全域變數
	OKReaderName = okReaderName;
	
	//拔插卡
	removeThenInsertCard();
}
//拔插卡
function removeThenInsertCard(){
	RemoveThenInsertCard("MaskArea",60,OKReaderName,"removeThenInsertCardFinish");
}
//拔插卡結束(成功才會到此FUNCTION)
function removeThenInsertCardFinish(){
	//取得卡片銀行代碼
	getCardBankID();
}
//取得卡片銀行代碼
function getCardBankID(){
	GetUnitCode(OKReaderName,"getCardBankIDFinish");
}
//取得卡片銀行代碼結束
function getCardBankIDFinish(result){
	//成功
	if(result != "false"){
		//還要另外判斷是否為本行卡
		//是
		if(result == "05000000"){
			showDialog("verifyPin",OKReaderName,"verifyPinFinish");
		}
		//不是
		else{
//			alert(GetErrorMessage("E005"));
	        errorBlock(
					null,
					null,
					[GetErrorMessage("E005")], 
					'確定', 
					null
				);
		}
	}
}
var pazzword = "";
//驗證卡片密碼
function verifyPin(readerName,password,outerCallBackFunction){
	//ASSIGN到全域變數
	pazzword = password;
	
	VerifyPin(readerName,password,outerCallBackFunction);
}
//驗證卡片密碼結束
function verifyPinFinish(result){
	//成功
	if(result == "true"){
		//繼續做
		CheckIdProcess();
	}
}
CheckIdProcess = function()
{
	/*
	var strCheckMessage = "<br><p>晶片金融卡身份查驗中，請稍候...</p><br>";
	strCheckMessage += "<p>查驗期間請勿移除您的晶片金融卡，以免造成驗證錯誤。</p><br><br>";
	CRC.showTempMessage(500, "晶片金融卡身份查驗", strCheckMessage, true);
	CRC.getCardInfo();

	var cardACN = CRC.Accounts[0];
	if(cardACN.length > 11)cardACN = cardACN.substr(cardACN.length - 11);
	
	var x = { method: 'get', parameters: '', onComplete: CheckIdResult };	
	var myAjax = new Ajax.Request( "simpleform?trancode=N953&TXID=N953&ACN=" + cardACN, x);
	*/
	
	showTempMessage(500,"晶片金融卡身份查驗","<br><p>晶片金融卡身份查驗中，請稍候...</p><br><p>查驗期間請勿移除您的晶片金融卡，以免造成驗證錯誤。</p><br><br>","MaskArea",true);
	
	//取得卡片主帳號
	getMainAccount();
}
//取得卡片主帳號
function getMainAccount(){
	GetMainAccount(OKReaderName,"getMainAccountFinish");
}
//取得卡片主帳號結束
function getMainAccountFinish(result){
	if(window.console){console.log("getMainAccountFinish: " + result);}
	//成功
	if(result != "false"){
		var cardACN = result;
		if(cardACN.length > 11){
			cardACN = cardACN.substr(cardACN.length - 11);
		}
		
//		var x = { method: 'get', parameters: '', onComplete: CheckIdResult };	
//		var myAjax = new Ajax.Request( "simpleform?trancode=N953&TXID=N953&ACN=" + cardACN, x);
		
		var uri = urihost+"/COMPONENT/component_acct_aj";
		var rdata = { ACN: cardACN };
		fstop.getServerDataEx(uri, rdata, true, CheckIdResult);
	}
	//失敗
	else{
		showTempMessage(500,"晶片金融卡身份查驗","","MaskArea",false);
	}
}
//CheckIdResult = function(response)
//{
//	var ResultStr = response.responseText;
//	
//	
//	if(ResultStr.indexOf("GRANTED") == -1)
//	{
//		//CRC.showTempMessage(500, "晶片金融卡身份查驗", "", false);
//		showTempMessage(500,"晶片金融卡身份查驗","","MaskArea",false);
//		
//		alert("非本人帳戶之晶片金融卡，無法進行此功能");
//	}
//	else
//	{
//		SubmitForm();
//	}
//}

function CheckIdResult(data) {
	console.log("data: " + data);
	if (data) {
		// login_aj回傳資料
		console.log("data.json: " + JSON.stringify(data));
		// 成功
		if("0" == data.msgCode) {
			SubmitForm();
		} else {
			showTempMessage(500,"晶片金融卡身份查驗","","MaskArea",false);
//			alert("非本人帳戶之晶片金融卡，無法進行此功能");
			try{
				if(data.data.msgCode == "0"){
			        errorBlock(
						null,
						null,
						["非本人帳戶之晶片金融卡，無法進行此功能"], 
						'確定', 
						null
					);
				}else{
			        errorBlock(
							null,
							null,
							[ data.data.msgCode + " " + data.data.msgName ], 
							'確定', 
							null
						);
				}
			}catch (e) {
		        errorBlock(
					null,
					null,
					["非本人帳戶之晶片金融卡，無法進行此功能"], 
					'確定', 
					null
				);
			}
		}
	}
}

function SubmitForm() {
	var formId = document.getElementById("formId");
	formId.submit();
	return false;	 				
}

//--------------------------------------------------------------------------------------------------------------------------------------

//自然人憑證
function processQuery2() {
	//Sox Add 自然人憑證元件語法 2017.06.13========================================================= Start
	var main = document.getElementById("formId");
	var npcpin = $("#NPCPIN").val();
	if(npcpin.length<6 || npcpin.length>8) {
//		alert("自然人憑證PIN碼長度錯誤");
        errorBlock(
				null,
				null,
				["自然人憑證PIN碼長度錯誤"], 
				'確定', 
				null
			);
		return false;
	}
	console.log("processQuery2.pass...");
	showTempMessage(500,"自然人憑證身份查驗","<br><p>自然人憑證身份查驗中，請稍候...</p><br><p>查驗期間請勿移除您的自然人憑證，以免造成驗證錯誤。</p><br><br>","validateMaskNatural",true);
	
	$("#AUTHCODE").val(getServerAuth());
	doquery1();
}
function doquery1() {
	var uri = urihost + "/COMPONENT/vaCheckAjax";
	var rdata = {AUTHCODE:$("#AUTHCODE").val()};
	console.log("doquery1.uri: " + uri);
	fstop.getServerDataEx(uri, rdata, false, vaCheckAjaxFinish);
}
function vaCheckAjaxFinish(data){
	console.log("data.json: " + JSON.stringify(data));
	if(data.result == true){
		var vaResult = data.data;
		$("#AUTHCODE1").val(vaResult);
		
		var checkauthcodertn = -1;
		setTimeout(function(){ 
        	checkauthcodertn = myobj.VerifyServerResponse($("#AUTHCODE1").val());
			
        	if(checkauthcodertn != 0){
				showTempMessage(500, "自然人憑證身份查驗", "", "validateMaskNatural", false);
//				alert("雙向認證失敗");
		        errorBlock(
						null,
						null,
						["雙向認證失敗"], 
						'確定', 
						null
					);
				return false;
			}
        	console.log("vaCheckAjaxFinish.checkauthcodertn: " + checkauthcodertn);
        	
        	var checkpinrtn = CHECKPIN($("#NPCPIN").val());
        	console.log("vaCheckAjaxFinish.checkpinrtn: " + checkpinrtn);
        	
			if(checkpinrtn == 0){
				var getcertdatartn = SelectCert();
				
				if(getcertdatartn == 0){
					var CUSIDN4 = $("#CUSIDN").val().substring(6,10);//先取身分證末四碼
					var validdate = $("#CertNotAfter").val().substring(0,8);//有效日檢核到日
					var today = yyyymmdd(new Date());
					if(CUSIDN4 != $("#CUSIDN4").val()){//CUSIDN4 value為自然人憑證末四碼
						showTempMessage(500, "自然人憑證身份查驗", "", "validateMaskNatural", false);
//						alert("自然人憑證非此身分證字號，請確認");
				        errorBlock(
								null,
								null,
								["自然人憑證非此身分證字號，請確認"], 
								'確定', 
								null
							);
						return false;
					}	
					if(validdate < today){//檢查自然人憑證是否過期
						showTempMessage(500, "自然人憑證身份查驗", "", "validateMaskNatural", false);
//						alert("自然人憑證已過期");
				        errorBlock(
								null,
								null,
								["自然人憑證已過期"], 
								'確定', 
								null
							);
						return false;
					}
					console.log("vaCheckAjaxFinish.jsondc: " + $("#jsondc").val());
					var result = myobj.SignPKCS7($("#jsondc").val());
					
					if(result == 0){
						$("#pkcs7Sign").val(myobj.GetPKCS7Data());
						
						$("#formId").submit();
					}
					else{
//						alert("錯誤代碼：" + myobj.getErrorCode() + "，錯誤訊息：" + myobj.getErrorMsg());
				        errorBlock(
								null,
								null,
								["錯誤代碼：" + myobj.getErrorCode() + "，錯誤訊息：" + myobj.getErrorMsg()], 
								'確定', 
								null
							);
						
						showTempMessage(500, "自然人憑證身份查驗", "", "validateMaskNatural", false);
//						alert("簽章錯誤");
				        errorBlock(
								null,
								null,
								["簽章錯誤"], 
								'確定', 
								null
							);
						return false;
					}
				}
				else{
					showTempMessage(500, "自然人憑證身份查驗", "", "validateMaskNatural", false);
//					alert("取得憑證資訊錯誤");
			        errorBlock(
							null,
							null,
							["取得憑證資訊錯誤"], 
							'確定', 
							null
						);
					return false;
				}	
			}
			else{
				showTempMessage(500, "自然人憑證身份查驗", "", "validateMaskNatural", false);
//				alert("自然人憑證登入失敗");
		        errorBlock(
						null,
						null,
						["自然人憑證登入失敗"], 
						'確定', 
						null
					);
				return false;
			}
        	myobj.Logout();//載具登出
    	},
    		2000
    	);
	} else {
		showTempMessage(500, "自然人憑證身份查驗", "", "validateMaskNatural", false);
//		alert("無法驗證自然人憑證");
		errorBlock(
				null,
				null,
				["無法驗證自然人憑證"], 
				'確定', 
				null
			);
	}
}
function yyyymmdd(dateIn) {
	var yyyy = dateIn.getFullYear();
	var mm = dateIn.getMonth()+1; // getMonth() is zero-based
	var dd  = dateIn.getDate();
	return String(10000*yyyy + 100*mm + dd); // Leading zeros for mm and dd
}


