
/**
 * @file AP_js_deom for javascript deom.
 * @copyright Fstop 2017
 * @version 1.0.1
 */

var fstop = fstop || {};

fstop.ap = (function(parent){
	
	var ap = parent.ap = parent.ap || {};
	
	ap.onConfirm = function()
	{
		alert("Confirm");
	};

	ap.onExit = function()
	{
		alert("Exit");
	};
	
	return ap;
	
})(fstop || {});


//======================================
    var gPageName = "Javascript_Deom";
	fstop.web.gContext = "/fstop-util-javascript";

	
	function getData(msg)
	{
		console.log("jsonDataByGet="+msg.data);
	}
	function postData(msg)
	{
		console.log("jsonDataByPost="+msg.data);
	}
	
	function seqData(msg)
	{
		console.log("sequentialAjax="+msg.data);
	}
	
	function frmSuccess(msg)
	{
		console.log("frmSuccess="+msg.rc);
	}
	function frmError(url, frmData)
	{
		console.log("frmError="+url);
	}
	function getListError(url, rData, node)
	{
		console.log("getListError="+url + " node=" + node);
	}
	
	function idleAction()
	{
		alert("custom idle timeout!");
	}
	function customProgress(msg)
	{
		console.log("custom idle="+msg);
		$("#idle_expire_remain_seconds").text("custom idle=" + msg);
	}
	
  	//共用按鈕列的浮動處理
    function btnBarScroll()
    {
        $("#btnBar")[0].style.top = $(window).height() - $("#btnBar").height()-10 + $(window).scrollTop() + 'px';
    }
	
	//document ready
    $(document).ready(function(){
    	
    	
    	btnBarScroll();
        
        $(window).scroll(function (evt) {
        	btnBarScroll();
        });
      	    	
        
        //initialize select2 select options
        $(".select2able").select2();
        
    	//Test web page url: http://localhost:8080/fstop-util-javascript/js_demo.html?f=01
    	
    	var url = "/jsdemo?f=01";
    	var data = "";
    	var key = "";
    	var async = false;
    	var isOK;
    	var msg;
    	var frmData;
    	var json;
    	var frmID;
    	var optID;
    	var itemID;
    	var itemName;
    	
    	// ajax request functions
    	fstop.web.jsonDataByPost(url, data, async, postData, null);
    	
    	fstop.web.jsonDataByGet(url, data, async, getData, null);

    	msg = fstop.web.returnJsonData(url, data, async);
    	console.log("returnJsonData=" + msg.data);
    	
    	msg = fstop.web.getQueryValue("f");
    	console.log("getQueryValue=" + msg);

    	msg = fstop.web.getQueryValueByName("f");
    	console.log("getQueryValueByName=" + msg);
    	
    	var requests = [
			{ url: fstop.web.makeUrl(url), seq: { always: null, done: seqData, fail:null} },
			{ url: fstop.web.makeUrl(url), seq: { always: null, done: seqData, fail:null} }
		];
    	fstop.web.sequentialAjax(1, requests, 'GET');
    	
    	frmID = "edit-form";
    	//ajax form submit 
    	url = fstop.web.makeUrl("/jsdemo?f=02");
    	frmData = fstop.web.getFormData(frmID);    	
    	fstop.web.ajaxFormSubmit(url, frmData, frmSuccess, frmError);
    	
    	//form submit
    	url = fstop.web.makeUrl("/jsdemo?f=02");
    	frmData = fstop.web.getFormData(frmID);
    	fstop.web.formSubmit(url, frmData, frmSuccess, frmError);
    	
    	//object to json
    	json = fstop.web.objToJson(frmData);
    	console.log("objToJson=" + json);
    	
    	json = fstop.web.getFormFieldValue(frmID);
    	console.log("getFormFieldValue=" + json);
    	
    	//pick list
    	url = fstop.web.makeUrl("/jsdemo?f=03");
    	optID = "customer_id_r";
    	fstop.web.getPickListData(optID, url, "{}", null, false, "GET",  getListError);

    	url = fstop.web.makeUrl("/jsdemo?f=03");
    	optID = "customer_id";
    	fstop.web.getPickListData(optID, url, "{}", null, false, "GET",  getListError);
    	
    	fstop.web.removeSelectOption(optID);
    	
    	itemID = "is_virtual";
    	itemName = "is_virtual";
    	fstop.web.setCheckBox(itemID, itemName, "y");
    	    	
    	frmID = "edit-form";
    	fstop.web.setRequiredLabel(frmID, true, "*");
    	
    	fstop.web.disableFieldEdit(frmID);

    	fstop.web.enableFieldEdit(frmID);
    	
    	fstop.web.disableKeyEdit(frmID);
    	
    	// i18n functions
    	msg = fstop.web.getUserLang();
    	console.log("getUserLang=" + msg);
    	
    	data = fstop.web.getI18nKeys();
    	console.log("getI18nKeys=" + data);
    	
    	// check & validation functions
    	isOK = fstop.web.isValidData("aaaa;bbbb");
    	console.log("isValidData=" + isOK);
    	
    	isOK = fstop.web.isNumeric("123");
    	console.log("isNumeric=" + isOK);

    	isOK = fstop.web.isNumeric("abc");
    	console.log("isNumeric=" + isOK);
    	
    	isOK = fstop.web.isEmptyString("abc");
    	console.log("isEmptyString=" + isOK);
    	
    	isOK = fstop.web.isEmptyString("");
    	console.log("isEmptyString=" + isOK);
    	
    	// location functions
    	msg = fstop.web.getTimezoneOffset();
    	console.log("getTimezoneOffset=" + msg);
    	
    	// html5 storage functions
    	isOK = fstop.web.isSupportSessionStorage();
    	console.log("isSupportSessionStorage=" + isOK);
    	
    	key = "session storage key";
    	data = "session storage data";
    	fstop.web.setSessionStorageItem(key, data);
    	
    	msg = fstop.web.getSessionStorageItem(key);
    	console.log("getSessionStorageItem=" + msg);
    	
    	fstop.web.removeSessionStorageItem(key);
    	
    	msg = fstop.web.getSessionStorageItem(key);
    	console.log("After remove getSessionStorageItem=" + msg);
    	

    	isOK = fstop.web.isSupportLocalStorage();
    	console.log("isSupportLocalStorage=" + isOK);
    	
    	key = "local storage key";
    	data = "local storage data";
    	fstop.web.setLocalStorageItem(key, data);
    	
    	msg = fstop.web.getLocalStorageItem(key);
    	console.log("getLocalStorageItem=" + msg);
    	
    	fstop.web.removeLocalStorageItem(key);
    	
    	msg = fstop.web.getLocalStorageItem(key);
    	console.log("After remove getLocalStorageItem=" + msg);
    	
    	// data convert functions
    	msg = "1234567890abcdef";
    	data = fstop.web.strToArrayBuffer(msg);
    	console.log("strToArrayBuffer=" + data);
    	
    	msg = fstop.web.arrayBufferToStr(data);
    	console.log("arrayBufferToStr=" + msg);
    	
    	msg = fstop.web.numberRound(123.45678, 2);
    	console.log("numberRound=" + msg);
    	
    	msg = fstop.web.getIsoDateTime(new Date());
    	console.log("getIsoDateTime=" + msg);
    	
    	//----------------- Idle check mixin tests
    	fstop.web.gIdleTimeout = 10;
    	fstop.web.customIdleAction = idleAction;
    	fstop.web.customIdleProgress = customProgress;
    	fstop.web.idleCheckStart();
    	
    	//----------------- select2 mixin tests
    	fstop.web.addAndSetSelect2Value("machine_id", "machine1", "value1");
    	fstop.web.addAndSetSelect2Value("machine_id", "machine2", "value2");
    	
    	fstop.web.addAndSetSelect2Value("area_id", "area1", "value1");
    	fstop.web.addAndSetSelect2Value("area_id", "area2", "value2");
    	fstop.web.setSelect2Value("area_id", "area1", "value1");

    	fstop.web.addAndSetSelect2Value("case_id", "case1", "value1");
    	fstop.web.addAndSetSelect2Value("case_id", "case2", "value2");
    	
    	msg = fstop.web.getSelect2Text("area_id");    	
    	console.log("getSelect2Text=" + msg);
    	
    	fstop.web.clearAndSetSelect2Value("case_id", "case1", "value1");
    	fstop.web.setSelect2Value("case_id", "case1", "value1");
    	fstop.web.clearSelect2Value("case_id");
    	
    });
