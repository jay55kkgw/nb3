var CapiATL = "TBBANKPKCS11ATL.dll";
var sessionToken;
if (window.sessionStorage) {
    sessionToken = window.sessionStorage.getItem("sessionToken");
}
function initCapiAgentForChrome() {
	var newPk11ATL = "PKCS11";
    var objPkcs11 = null;
	var detectVersion = "4.3.0.0"; // 偵測是否為此版號
	var isNewAPI = false;
    var beginPort = 45500;
 //   if (/MSIE/.test(navigator.userAgent) || /Trident/.test(navigator.userAgent)) {
 //   } else {
        objPkcs11 = {
            port: (window.sessionStorage && window.sessionStorage.getItem("sessionTokenPort")) || beginPort,
            send: function (str, cb) {
                var xhr = new XMLHttpRequest();
                var resp;
                var URL;
                var j = 100;
                if (navigator.userAgent.indexOf("Safari") > 1) {
                    if (sessionToken != null) {
                        URL = "https://localhost:" + objPkcs11.port + "/TWCA?" + str;
                        try {
                            if (cb != null) {
                                xhr.onload = function () {
                                    cb(xhr.responseText);
                                };
                            }
                            xhr.open("POST", URL, cb != null);
                            xhr.send(null);
                            resp = xhr.responseText;
                        } catch (err) {
                            while (j > 0) {
                                try {
                                    xhr.open("POST", "https://localhost:" + objPkcs11.port + "/TWCA?" + objPkcs11.ExecMethod("retry"), false);
                                    xhr.send(null);
                                    resp = xhr.responseText;
                                    break;
                                } catch (err) {
                                    j = j - 1;
                                    if (j == 0)
                                        alert("operation timeout");
                                }
                            }
                        }
                    } else {
                        for (i = 0; i < 3; i++) {
                            thisPort = beginPort + i;
                            try {
                                var URL = "https://localhost:" + thisPort + "/TWCA?" + str;
                                xhr.open("POST", URL, false);
                                xhr.send(null);
                                resp = xhr.responseText;
                                if (resp.substring(1, 4) == "|0|") {
                                    objPkcs11.port = thisPort;
                                    if (window.sessionStorage) {
                                        window.sessionStorage.setItem("sessionTokenPort", thisPort);
                                    }
                                    break;
                                }
                            } catch (err) {
                            }
                        }
                    }
                    return resp;
                } else {
                    if (sessionToken != null) {
                        URL = "https://localhost:" + objPkcs11.port + "/TWCA?" + str;
                        if (cb != null) {
                            xhr.onload = function () {
                                cb(xhr.responseText);
                            };
                        }
                        xhr.open("POST", URL, cb != null);
                        xhr.send(null);
                        resp = xhr.responseText;
                    } else {
                        var thisPort;
                        for (i = 0; i < 3; i++) {
                            thisPort = beginPort + i;
                            try {
                                URL = "https://localhost:" + thisPort + "/TWCA?" + str;
                                xhr.open("POST", URL, false);
                                xhr.send(null);
                                resp = xhr.responseText;
                                if (resp.substring(1, 4) == "|0|") {
                                    objPkcs11.port = thisPort;
                                    if (window.sessionStorage) {
                                        window.sessionStorage.setItem("sessionTokenPort", thisPort);
                                    }
                                    break;
                                }
                            } catch (err) {
                            }
                        }
                    }
                    return resp;
                }
            },
            initChrome: function () {
				try { // 先試 新版功能
					var str = objPkcs11.send("InitDLL=" + newPk11ATL);
					sessionToken = objPkcs11.ParseReturnNoAlert(str); // 不顯示警告訊息, 再判斷舊版中介
					if (window.sessionStorage && sessionToken != null && sessionToken != "") {
						window.sessionStorage.setItem("sessionToken", sessionToken);
                        isNewAPI = true;
                    } else {
						var str = objPkcs11.send("InitDLL=" + CapiATL);
						sessionToken = objPkcs11.ParseReturn(str);
						if (window.sessionStorage && sessionToken != null && sessionToken != "") {
							window.sessionStorage.setItem("sessionToken", sessionToken);
						}
					}
				} catch (err) { // 舊版功能
					var str = objPkcs11.send("InitDLL=" + CapiATL);
					sessionToken = objPkcs11.ParseReturn(str);
					if (window.sessionStorage && sessionToken != null && sessionToken != "") {
						window.sessionStorage.setItem("sessionToken", sessionToken);
					}
				}
            },
            ExecMethod: function (apiName) {
                return "DLLToken=" + sessionToken + "&ExecMethod=" + apiName; // 全部內容注意大小寫
            },
            ExecParam: function () {
                var ret = "";
                for (var i = 0; i < arguments.length; i++) {
                    ret += "&Param" + (i + 1) + "=" + encodeURIComponent(arguments[i]);
                }
                return ret; // 全部參數注意大小寫
            },
            ParseReturnNoAlert: function (result) { // 不顯示警告訊息
                if (result == null) {
                    return null;
                }
                var res = result.split("||");
				if (parseInt(res[1]) == 0) {
                    return res[2];
                } else {
                    //alert("執行失敗, 錯誤訊息 = " + res[2]);
                    return null;
                }
            },
            ParseReturn: function (result) {
                if (result == null) {
                    return null;
                }
                var res = result.split("||");
				if (parseInt(res[1]) == 0) {
                    return res[2];
                } else {
                    alert("執行失敗, 錯誤訊息 = " + res[2]);
                    return null;
                }
            },
			ParseInt:function(result) {
				var res = result.split("||");
				if (parseInt(res[1]) == 0) {
					return parseInt(res[2]);
				} else {
					alert("執行失敗, 錯誤訊息 = "+res[2]);
					return null;
				}
			},
			GetVersion :function(){ // 中介版號
				if(objPkcs11.isMacOS()) {
					var str=objPkcs11.ExecMethod("GetDllVersion"); // DLL 版號
					if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str)); 
				}else {
					if (isNewAPI == true) {
						var str=objPkcs11.ExecMethod("GetVersion");
					} else { // 舊版取得版號 API
						var str=objPkcs11.ExecMethod("GetDLLVersion");
					}
					if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
				}
			},
			// 基本功能(版本/錯誤碼/返回值)
			GetDllVersion :function(){ // 
				var str=objPkcs11.ExecMethod("GetDllVersion"); // DLL 版號
				if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str)); 
			},
            GetDLLVersion: function () { //
                var str = objPkcs11.ExecMethod("GetDLLVersion"); // DLL 版號
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            SetTokenType: function (type) { // 
                var str = objPkcs11.ExecMethod("SetTokenType") + objPkcs11.ExecParam(type);
                if (sessionToken != null) return objPkcs11.ParseInt(objPkcs11.send(str));
            },
            getErrorCode: function () { // 舊 API
				if (isNewAPI == true) {
					var str = objPkcs11.ExecMethod("GetErrorCode"); // 
				} else {
					var str = objPkcs11.ExecMethod("getErrorCode"); // 
				}
                if (sessionToken != null) return objPkcs11.ParseInt(objPkcs11.send(str));
            },
            getErrorMsg: function () { // 舊 API
				if (isNewAPI == true) {
					var str = objPkcs11.ExecMethod("GetErrorMsg");
				} else {
					var str = objPkcs11.ExecMethod("getErrorMsg");
				}
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
			GetErrorCode :function(){ // 
				var str=objPkcs11.ExecMethod("GetErrorCode"); 
				if (sessionToken != null) return objPkcs11.ParseInt(objPkcs11.send(str));
			},
			GetErrorMsg :function(){ // 
				var str=objPkcs11.ExecMethod("GetErrorMsg");
				if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
			},
            GetCardType: function () { //
                var str = objPkcs11.ExecMethod("GetCardType");
                if (sessionToken != null) return objPkcs11.ParseInt(objPkcs11.send(str));
            },
            GetCardStatus: function () { //
                var str = objPkcs11.ExecMethod("GetCardStatus");
                if (sessionToken != null) return objPkcs11.ParseInt(objPkcs11.send(str));
            },
            Login: function (PINCode, PfxPath) { //
                var str = objPkcs11.ExecMethod("Login") + objPkcs11.ExecParam(PINCode, PfxPath);
                if (sessionToken != null) return objPkcs11.ParseInt(objPkcs11.send(str));
            },
            Logout: function () { // 
                var str = objPkcs11.ExecMethod("Logout");
                if (sessionToken != null) return objPkcs11.ParseInt(objPkcs11.send(str));
            },
            SelectCert: function (CertType) { //
                var str = objPkcs11.ExecMethod("SelectCert") + objPkcs11.ExecParam(CertType);
                if (sessionToken != null) return objPkcs11.ParseInt(objPkcs11.send(str));
            },
            GetHiCertType: function () { // 
                var str = objPkcs11.ExecMethod("GetHiCertType");
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            GetHiCertTailCitizenID: function () { // 
                var str = objPkcs11.ExecMethod("GetHiCertTailCitizenID");
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            GetHiCertUniformOrganizationID: function () { // 
                var str = objPkcs11.ExecMethod("GetHiCertUniformOrganizationID");
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            GetCardPriSec: function () { // 
                var str = objPkcs11.ExecMethod("GetCardPriSec");
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            GetGPKICardNo: function () { // 
                var str = objPkcs11.ExecMethod("GetGPKICardNo");
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            readGPKIUserData: function (l_uchptrUserPIN, l_uchptrSNO) { // 無 API
            },
            getGPKIUserData: function () { // 無 API
            },
            Hash: function (Hashdata, type) { // 
                var str = objPkcs11.ExecMethod("Hash") + objPkcs11.ExecParam(Hashdata, type);
                if (sessionToken != null) return objPkcs11.ParseInt(objPkcs11.send(str));
            },
            HashFile: function (toFilePath, type) { // 
                var str = objPkcs11.ExecMethod("HashFile") + objPkcs11.ExecParam(toFilePath, type);
                if (sessionToken != null) return objPkcs11.ParseInt(objPkcs11.send(str));
            },
            GetHashedData: function () { // 
                var str = objPkcs11.ExecMethod("GetHashedData");
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            SignPKCS7: function (toSigndata) { // 
                var str = objPkcs11.ExecMethod("SignPKCS7") + objPkcs11.ExecParam(toSigndata);
                if (sessionToken != null) return objPkcs11.ParseInt(objPkcs11.send(str));
            },
            SignPKCS7ByFile: function (toSignFilePath) { // 
                var str = objPkcs11.ExecMethod("SignPKCS7ByFile") + objPkcs11.ExecParam(toSignFilePath);
                if (sessionToken != null) return objPkcs11.ParseInt(objPkcs11.send(str));
            },
            SignPKCS7ByBigFile: function (toSignFilePath) { // 無 API
            },
            GetPKCS7Data: function () { // 
                var str = objPkcs11.ExecMethod("GetPKCS7Data");
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            SignPKCS1: function (toSigndata) { // 
                var str = objPkcs11.ExecMethod("SignPKCS1") + objPkcs11.ExecParam(toSigndata);
                if (sessionToken != null) return objPkcs11.ParseInt(objPkcs11.send(str));
            },
            GetPKCS1SignData: function () { // 
                var str = objPkcs11.ExecMethod("GetPKCS1SignData");
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            VerifyPKCS7: function (toVerifyData) { // 無 API
                var str = objPkcs11.ExecMethod("VerifyPKCS7") + objPkcs11.ExecParam(toVerifyData);
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            GetVerifiedContent: function () { // 無 API
                var str = objPkcs11.ExecMethod("GetVerifiedContent");
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            GetVerifiedCertB64: function () { // 無 API
                var str = objPkcs11.ExecMethod("GetVerifiedCertB64");
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            GetVerifiedCertSerial: function () { // 無 API
                var str = objPkcs11.ExecMethod("GetVerifiedCertSerial");
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            GetVerifiedCertSubject: function () { // 無 API
                var str = objPkcs11.ExecMethod("GetVerifiedCertSubject");
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            GetVerifiedCertIssuer: function () { // 無 API
                var str = objPkcs11.ExecMethod("GetVerifiedCertIssuer");
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            GetVerifiedCertNotBefore: function () { // 無 API
                var str = objPkcs11.ExecMethod("GetVerifiedCertNotBefore");
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            GetVerifiedCertNotAfter: function () { // 無 API
                var str = objPkcs11.ExecMethod("GetVerifiedCertNotAfter");
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            GetCertFinger: function () { // 
                var str = objPkcs11.ExecMethod("GetCertFinger");
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            GetSelectedCertB64: function () { // 
                var str = objPkcs11.ExecMethod("GetSelectedCertB64");
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            GetSelectedCertSerial: function () { // 
                var str = objPkcs11.ExecMethod("GetSelectedCertSerial");
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            GetSelectedCertSubject: function () { // 
                var str = objPkcs11.ExecMethod("GetSelectedCertSubject");
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            GetSelectedCertIssuer: function () { // 
                var str = objPkcs11.ExecMethod("GetSelectedCertIssuer");
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            GetSelectedCertNotBefore: function () { // 
                var str = objPkcs11.ExecMethod("GetSelectedCertNotBefore");
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            GetSelectedCertNotAfter: function () { // 
                var str = objPkcs11.ExecMethod("GetSelectedCertNotAfter");
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            CertEncrypt: function (Base64Cert, EncryptData) { // 
                var str = objPkcs11.ExecMethod("CertEncrypt") + objPkcs11.ExecParam(Base64Cert, EncryptData);
                if (sessionToken != null) return objPkcs11.ParseInt(objPkcs11.send(str));
            },
            GetCertEncryptedData: function () { // 
                var str = objPkcs11.ExecMethod("GetCertEncryptedData");
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            SymEncrypt: function (Key, Data, iv) { // 
                var str = objPkcs11.ExecMethod("SymEncrypt") + objPkcs11.ExecParam(Key, Data, iv);
                if (sessionToken != null) return objPkcs11.ParseInt(objPkcs11.send(str));
            },
            SymDecrypt: function (Key, Base64Data, iv) { // 
                var str = objPkcs11.ExecMethod("SymDecrypt") + objPkcs11.ExecParam(Key, Base64Data, iv);
                if (sessionToken != null) return objPkcs11.ParseInt(objPkcs11.send(str));
            },
            GetSymEncryptedData: function () { // 
                var str = objPkcs11.ExecMethod("GetSymEncryptedData");
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            GetSymDecryptedData: function () { // 
                var str = objPkcs11.ExecMethod("GetSymDecryptedData");
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            GetServerAuth: function () { // 
                var str = objPkcs11.ExecMethod("GetServerAuth");
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            VerifyServerResponse: function (strServerResponse) { // 
                var str = objPkcs11.ExecMethod("VerifyServerResponse") + objPkcs11.ExecParam(strServerResponse);
                if (sessionToken != null) return objPkcs11.ParseInt(objPkcs11.send(str));
            },
            //
            CreatePKCS10: function (commonName, keyBits, genKeyFlags, cb) { 
                var str = objPkcs11.ExecMethod("CreatePKCS10") + objPkcs11.ExecParam(commonName, keyBits, genKeyFlags);
                if (sessionToken != null)
                    objPkcs11.send(str, function (txt) {
                        cb(objPkcs11.ParseReturn(txt));
                    });
            },
            GetPKCS10: function () { // 
                var str = objPkcs11.ExecMethod("GetPKCS10") + objPkcs11.ExecParam();
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            ICCardInsertCheck: function (szdwTimeOut) { // 
                var str = objPkcs11.ExecMethod("ICCardInsertCheck") + objPkcs11.ExecParam(szdwTimeOut);
                if (sessionToken != null) return objPkcs11.ParseInt(objPkcs11.send(str));
            },
            AcceptPKCS7: function (strPkcs7, dwFlag) { // 
                var str = objPkcs11.ExecMethod("AcceptPKCS7") + objPkcs11.ExecParam(strPkcs7, dwFlag);
                if (sessionToken != null) return objPkcs11.ParseInt(objPkcs11.send(str));
            },
            ChangeCardPasswd: function (strOldPass, strNewPass, dwFlag) { // 
                var str = objPkcs11.ExecMethod("ChangeCardPasswd") + objPkcs11.ExecParam(strOldPass, strNewPass, dwFlag);
                if (sessionToken != null) return objPkcs11.ParseInt(objPkcs11.send(str));
            },
            GetCardSerial: function () { // 
                var str = objPkcs11.ExecMethod("GetCardSerial") + objPkcs11.ExecParam();
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            ClearExpiredCert: function () { // 
                var str = objPkcs11.ExecMethod("ClearExpiredCert") + objPkcs11.ExecParam();
                if (sessionToken != null) return objPkcs11.ParseInt(objPkcs11.send(str));
            },
            IFXSign: function (sXMLData, sID, sSignedIDs, iFlags) { // 
                var str = objPkcs11.ExecMethod("IFXSign") + objPkcs11.ExecParam(sXMLData, sID, sSignedIDs, iFlags);
                if (sessionToken != null) return objPkcs11.ParseInt(objPkcs11.send(str));
            },
            GetIFXSignData: function () { // 
                var str = objPkcs11.ExecMethod("GetIFXSignData") + objPkcs11.ExecParam();
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            GetAuthenState: function () { // 
                var str = objPkcs11.ExecMethod("GetAuthenState") + objPkcs11.ExecParam();
                if (sessionToken != null) return objPkcs11.ParseInt(objPkcs11.send(str));
            },
            GetBasicData: function () { // 
                var str = objPkcs11.ExecMethod("GetBasicData") + objPkcs11.ExecParam();
                if (sessionToken != null) return objPkcs11.ParseReturn(objPkcs11.send(str));
            },
            SignPKCS1ByFile: function (path) { // 
                var str = objPkcs11.ExecMethod("SignPKCS1ByFile") + objPkcs11.ExecParam(path);
                if (sessionToken != null) return objPkcs11.ParseInt(objPkcs11.send(str));
            },
            LoginUI: function (cb) { // 
                var str = objPkcs11.ExecMethod("LoginUI") + objPkcs11.ExecParam();
                if (sessionToken != null)
                    objPkcs11.send(str, function (txt) {
                        cb(objPkcs11.ParseReturn(txt));
                    });
            },
            SelectCertUI: function (CertType) { // 
                var str = objPkcs11.ExecMethod("SelectCertUI") + objPkcs11.ExecParam(CertType);
                if (sessionToken != null) return objPkcs11.ParseInt(objPkcs11.send(str));
            },
			SelectCertID:function(CertType, strId){ // 
				var str=objPkcs11.ExecMethod("SelectCertID")+objPkcs11.ExecParam(CertType, strId);
				if (sessionToken != null) return objPkcs11.ParseInt(objPkcs11.send(str));
			},
			//P7 及 P1簽章 (40~49)
			SetEncoding:function(iFlag){ // 
				var str=objPkcs11.ExecMethod("SetEncoding")+objPkcs11.ExecParam(iFlag);
				if (sessionToken != null) return objPkcs11.ParseInt(objPkcs11.send(str));
			},
			//
			finalizeAgent:function() { // 終止 ATLAgent
				if (sessionToken != null) objPkcs11.send(objPkcs11.ExecMethod("Logout"));
				var str=objPkcs11.ExecMethod("finalizeAgent");
				if (sessionToken != null) objPkcs11.send(str);
				sessionToken = null;
			},
			isMacOS:function() {    // 檢測目前作業系統是否為 Apple MacOS
				return navigator.platform.indexOf('Mac') > -1;
			}
        };
    //}
    return objPkcs11;
}
