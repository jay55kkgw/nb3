$( document ).ready( function () {

    $('#capCode').on('input', function(e){
        this.value = this.value.toUpperCase().replace(/[^A-Z0-9]+/i, '');
    });

    $( "#validationForm" ).validate( {
    	ignore: ".ignore",
    	rules: {
            form_username: {
                required: true,
                rangelength: [6,16]
            },
            form_password: {
                required: true,
                rangelength: [6,8]
            },
            capCode: {
                required: true,
                rangelength: [4,4]
            }
        },
        messages: {
            form_username: {
                rangelength: "請輸入6~16碼的使用者名稱"
            },
            form_password: {
                rangelength: "請輸入正確格式"
            },
            capCode: {
                rangelength: "請輸入4碼驗證碼"
            }
        },
        onfocusout: function(element) {
            this.element(element);
        },
        errorElement: "label",
        errorPlacement: function ( error, element ) {
            error.addClass( "invalid-feedback" );

            if ( element.prop( "type" ) === "checkbox" ) {
                error.insertAfter( element.next( "label" ) );
            } else {
                error.insertAfter( element );
            }
        },
        highlight: function ( element, errorClass, validClass ) {
            $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
        },
        unhighlight: function (element, errorClass, validClass) {
            $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
        },
//        submitHandler: function(form) {
//            form.submit();
//        }
    } );
} );