/**
 * @file FSTOP Javascript for web.
 * 
 * <pre>
 * 安裝 node.js 後，將 js 複製到另外的目錄以 jsdoc 產生文件
 * 指令： jsdoc fstop.web-x.x.x.js
 * </pre>
 * 
 * @copyright Fstop 2017
 * @version 1.0.1
 */


/**
 * Global namespace fstop
 * @namespace fstop
 */
var fstop = fstop || {};

/**
 * Global namespace fstop.web.
 * Provide basic javascript functions for web development.
 *  
 * Require jQuery! 
 * 
 * @namespace fstop.web
 */
fstop.web = (function(parent, $){
	
	var web = parent.web = parent.web || {};
	

    //======================================
	
    //設定 ajax 不要 cache
    $.ajaxSetup({cache: false});
	
    web.gDomainObject = {};  	//存放  form 欄位
    web.gEditType = "";  		//add=新增, modify=修改, delete=刪除, info=明細
    web.gLang = "zh-TW";		//語系 會被改為 小寫
    web.gLang2 = "zh-TW";		//語系 保留有大小寫的格式，例如 zh-TW
    web.gContext = "";			//web site context
    
    web.makeUrl = function(uri)
    {
		uri = web.gContext + uri;
		uri = encodeURI(uri);
    	return uri;
    };
    
    
    //========================== [ajax request functions] ==========================
    /**
     * Get server json data by ajax.
     * If request successful then successCallback is called, otherwise errorCallback is called.
     * 
     * @param ajaxType POST/GET
     * @param uri url to request
     * @param rdata request form data
     * @param isAsync is async request
     * @param successCallback callback function when request success
     *        function successCallback(msg)
     * @param errorCallback callback function when request error
     *        function errorCallback(uri, rdata)
     */    
    web.jsonDataByAjax = function (ajaxType, uri, rdata, isAsync, successCallback, errorCallback) 
    {
		uri = web.makeUrl(uri);
		
		$.ajax({
			type : ajaxType,
			async : isAsync,
			url : uri,
			data : encodeURIComponent(rdata),
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			success : function(msg) {
				if (successCallback)
					successCallback(msg);
			},
			error : function() {
				if (errorCallback)
					errorCallback(uri, rdata);
			}
		});    	
    };
    
    /**
     * Get server json data by http post.
     * If request successful then successCallback is called, otherwise errorCallback is called.
     * 
     * @param uri url to request
     * @param rdata request form data
     * @param isAsync is async request
     * @param successCallback callback function when request success
     *        function successCallback(msg)
     * @param errorCallback callback function when request error
     *        function errorCallback(uri, rdata)
     */
	web.jsonDataByPost = function (uri, rdata, isAsync, successCallback, errorCallback) 
	{		
		web.jsonDataByAjax("POST", uri, rdata, isAsync, successCallback, errorCallback);		
	};

    /**
     * Get server json data by http get.
     * If request successful then successCallback is called, otherwise errorCallback is called.
     * 
     * @param uri url to request
     * @param rdata request form data
     * @param isAsync is async request
     * @param successCallback callback function when request success
     *        function successCallback(msg)
     * @param errorCallback callback function when request error
     *        function errorCallback(uri, rdata)
     */
	web.jsonDataByGet = function (uri, rdata, isAsync, successCallback, errorCallback) 
	{
		web.jsonDataByAjax("GET", uri, rdata, isAsync, successCallback, errorCallback);		
	};
	
	/**
	 * Get server json data by http post.
	 * 
     * @param uri url to request
     * @param rdata request form data
     * @param isAsync is async request
     * @return data in json format or null
	 */
	web.returnJsonData = function (uri, rdata, isAsync) 
	{
		var rawData = null;
		uri = web.makeUrl(uri);
		$.ajax({
			type : "POST",
			async : isAsync,
			url : uri,
			data : rdata,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			success : function(msg) {
				rawData = msg;
			},
			error : function() {
			}
		});
		return rawData;
	};

	
	/**
	 * 循序執行 ajax 及 相關的callback
	 * Reference : "https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce"
	 *             "https://api.jquery.com/deferred.pipe/"
	 * "seqArr = [{ url: '/aaa/bbb', seq: { always: callback1, done: callback2, fail:callback3} },
	 *             { url: '/ccc/ddd', seq: { always: callback1, done: [seqDone1, seqDone2]} }
	 *            ...]"
	 * @param seqMode
	 * 1.Sequential in series using pre-defined data;
	 * 2.Sequential in series, feeding each request using the response from previous request;
     * 3.Parallel request using pre-defined data, where responses are sequenced.
	 * @param seqArr
	 * @param ajaxType	GET, POST
	 */
	web.sequentialAjax = function (seqMode, seqArr, ajaxType)
	{
		if (seqMode == 1)
		{
			seqArr.reduce(function (prev, req) {
				req.type = ajaxType;
				req.data = null;
			    return $.ajax(req).always(function () {
			        var a = arguments;
			        $.when(prev).always(function () { a[1] === 'success'
			            ? $.Callbacks().add(req.seq.done).fireWith(req, a)
			            : $.Callbacks().add(req.seq.fail).fireWith(req, a);
			            $.Callbacks().add(req.seq.always).fireWith(req, a); 
			        });
			    });
			}, true);			
		}
		else if (seqMode == 2)
		{
			//deferred.resolve() : Resolve a Deferred object , here is ajax request
			//deferred.pipe( [doneFilter ] [, failFilter ] ) : Utility method to filter and/or chain Deferreds.
			seqArr.reduce(function (prev, req, idx, reqs) {
				req.type = ajaxType;
				req.data = null;
			    return prev.pipe(function () {
			        return $.ajax(req);
			    }, function () {
			        return $.ajax(req);
			    });
			}, $.Deferred().resolve() );
		}
		else if (seqMode == 3)
		{
			seqArr.reduce(function (prev, req) {
				req.type = ajaxType;
				req.data = null;
			    return prev.pipe(function () {
			        return $.ajax(req);
			    }, function () {
			        return $.ajax(req);
			    });
			}, $.Deferred().resolve() );
		}
	};
	
	
	/**
	 * Post form data to server and get result.
	 * If request successful then successCallback is called, otherwise errorCallback is called.
	 * 
     * @param uri url to request
     * @param frmData request form data
     * @param successCallback callback function when request success
     *        function successCallback(msg)
     * @param errorCallback callback function when request error
     *        function errorCallback(uri, rdata)
	 */	
	web.ajaxFormSubmit = function (uri, frmData, successCallback, errorCallback)
	{	
		var furi = encodeURI(uri);
		var fdata = frmData; //encodeURIComponent(frmData);
		
		$.ajax({
			url : furi,
			type : 'POST',
			dataType : 'json',
			data : fdata,
			contentType: "application/x-www-form-urlencoded; charset=utf-8",
			async : false,  //alwasy sync
			success: function(msg) 
            {
				if (successCallback)
				{
	            	successCallback(msg);					
				}
            },
            error: function() 
            {
            	if(errorCallback)
            	{
            		errorCallback(uri, rdata);
            	}
            }
        });

	}; //ajaxFormSubmit
	
	
	
	//=============================== [form process functions] ===========================
	/**
	 * Post form data to server and get result.
	 * If request successful then successCallback is called, otherwise errorCallback is called.
	 * 
     * @param uri url to request
     * @param frmData request form data
     * @param successCallback callback function when request success
     *        function successCallback(msg)
     * @param errorCallback callback function when request error
     *        function errorCallback(uri, rdata)
	 */	
  	web.formSubmit = function (uri, frmData, successCallback, errorCallback)
  	{
  		
		var furi = encodeURI(uri);
		var fdata = frmData; //encodeURIComponent(frmData);
		
		$.post(furi, fdata, function(data, status) 
		{
			if (status == "success") 
			{
				if (successCallback)
				{
	            	successCallback(data);					
				}				
			} 
			else 
			{
            	if(errorCallback)
            	{
            		errorCallback(uri, frmData);
            	}
			}
		});
  	};
	
	
  	/**
  	 * 取得指定的 form 項下所有欄位名稱與欄位值，並轉換為 json字串，以 id 取值
  	 * @param id form id
  	 * @return json string
  	 */
    web.getFormFieldValue = function (id)
    {
    	$("#"+id).each(function(){
    	    $(this).find(':input').each(function(){
    	    	if (web.notEmptyString(this.id))
    	    	{
            	    web.gDomainObject[this.id] = $("#"+this.id).val();
    	    	}
    	    });    	   
    	});
    	
    	var ret = web.objToJson(web.gDomainObject);
    	return ret;
    };
  	
    
    /**
     * 取得指定的 form 項下所有欄位名稱與欄位值，以 name 取值
     * @param id form id
     * @return form data object
     */
	web.getFormData = function (id)
	{
		var frmData = {};
		var k = '#' + id;
		$(k + " input[type=text]," + 
		  k + " select," + 
		  k + " textarea," + 
		  k + " input[type=hidden]," + 
		  k + " input[type=checkbox]," + 
		  k + " input[type=password]," + 
		  k + " input[type='radio']:checked").each(function(i)
		{
			var name = this.name;
			var value = this.value;

			//console.log(name + " " + value);
			if (name != '')
			{
				frmData[name] = value;				
			}
		});
		
		return frmData;
	}
    
    /**
     * Get pick list data from server.
     * 在回復原始表單資料時，若使用非同步方式，會造成問題
     * 
     * @param node pick list id / 要套用的 id
     * @param uri url to request / 取得資料的 url
     * @param rdata request data / 額外的資料
     * @param defVal default selected value / 預設選取的值
     * @param isAsyncPickList is async request / 是否使用非同步方式
     * @param reqType POST/GET / 使用 POST 或是 GET
     * @param errorCallback callback function when request error
     *        function errorCallback(uri, rdata, node)
     * @return selected value
     */
    web.getPickListData = function (node, uri, rdata, defVal, isAsyncPickList, reqType, errorCallback)
    {
    	var ret = null;
    	var hasEmptyValue = false;
    	
		var furi = encodeURI(uri);
		var fdata = encodeURIComponent(rdata);
		
		//append id selector 
		node = "#" + node;
		
        $.ajax({
            type: reqType,
            async: isAsyncPickList,
            url: furi,
            data: fdata,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(msg) {
            	//若是 select tag 已有空值的選項或傳下來有空值的選項則不再新增
            	$(node).each(function()
                {
                	var v = $(this).val();
                	//沒有任何選項時會是 null，因為 isEmptyString 將 null 視為空字串，所以要先過濾掉
                	if (v != null && web.isEmptyString(v))
                	{
                		hasEmptyValue = true;
                	}
                });
                if (hasEmptyValue == false)
                {
                	$(node).get(0).options.length = 0;
                	//$(node).get(0).options[0] = new Option("select", "");
                	//判斷是否有預設空值的選項
                	$.each(msg, function(index, item) {
                		if (web.isEmptyString(item.value))
                		{
                			hasEmptyValue = true;
                		}                	
                	});
                	if (hasEmptyValue == false)
                	{
                		$(node).get(0).options[0] = new Option("Select", "");
                	}            		
                }
                //msg 以 arraylist 傳入
                $.each(msg, function(index, item) {
                	//console.log(item.name);
                    $(node).get(0).options[$(node).get(0).options.length] = new Option(item.name, item.value);
                    if (defVal != null){
						if (item.name == defVal || item.value == defVal){
                    		$(node).val( item.value ).attr('selected', 'selected');
                    		ret = item.value;
                    	}
              		}
                });
            },
            error: function() 
            {
            	if(errorCallback)
            	{
            		errorCallback(uri, rdata, node);
            	}
            }
        });
        return ret;
    };
    
    
    /**
     * Remove select options.
     * 清除下拉選項
     * @param id id of select option     * 
     */
    web.removeSelectOption = function (id)
    {
    	$('#'+id)
        .find('option')
        .remove();
    };

    /**
     * Set check box value and checked status.
     * 
     * @param id check box id
     * @param name check box name
     * @param value literal data value of check box
     *        data value can be "y" or "yes" or "true"
     */
    web.setCheckBox = function (id, name, value)
    {
  		$("#" + id).val(value);  		
  		var ck = value.toLowerCase();
  		if (ck == 'y' || ck == 'yes' || ck == 'true')
  		{
  	  		$('input:checkbox[name='+ name +']').prop('checked',true);  			
  		}
  		else
  		{
  			$('input:checkbox[name='+ name +']').prop('checked',false);  			  			
  		}    	
    };
    
    /**
     * 將有設定 required 屬性的欄位 label 加上  mark 記號
     * 必需在 select2 之前使用，否則對 select2 的 label 無效
     * @param formId form id
     * @param isOn is enable mark
     * @param mark string to mark required field
     */
    web.setRequiredLabel = function (formId, isOn, mark)
    {
    	$( "#" + formId + " [required] " ).each(function(index, element) {
        	var currentId = $(this).attr('id');        	
        	var t = $('label[for=' + currentId + ']').text();
        	if (isOn)
        	{        		
        		if(t.substring(0,1) != mark)
        		{
        			var m = "<strong style='color:red'>" + mark + "</strong>" + t;
       		    	$('label[for=' + currentId + ']').html(m);
       		    }
        	}
        	else
        	{
        		if(t.substring(0,1) == mark)
        		{
       		 		t = t.replace(mark, '');  //將開頭第一個 "*" 移除
       		 	}
       		    $('label[for=' + currentId + ']').text(t);
        	}
    	});		    
    };    

    
    /**
     * 將特定元素下的輸入項 enable, 該元素通常是 form 或 div
     * @param id form id
     */
    web.enableFieldEdit = function (id)
    {
    	//將特定 id 項下元素整批處理
    	$( "#" + id + " input,#"+id+" textarea,#"+id+" select" ).each(function(index, element) {
        	$(this).removeAttr('disabled');  
        	$(this).prop('disabled', false);
    	});	    
		$("#" + id +  " select").select2("enable",true);
		$("#" + id +  " select").select2("readonly", false);
    };
    
    
    /**
     * 將特定元素下的輸入項 disable, 該元素通常是 form 或 div
     * @param id form id
     */
    web.disableFieldEdit = function (id)
    {    	
    	//將特定 id 項下元素整批處理
    	$( "#" + id + " input,#"+id+" textarea,#"+id+" select" ).each(function(index, element) {
        	$(this).attr('disabled', 'disabled');        	
    	});
		$("#"+id + " select").select2("enable",false);
		$("#"+id + " select").select2("readonly", true);
    }
    
    
    /**
     * 不允許編輯的欄位加上  data-key 屬性，自動保護
     * @param id form id 
     */
    web.disableKeyEdit = function (id)
  	{
  		var tagName;
    	$("#" + id + " [data-key]" ).each(function(index, element) {
        	$(this).attr('disabled', 'disabled');     
        	tagName = $(this).prop("tagName");
			if(tagName != undefined && tagName.toLowerCase() == "select"){
				$(this).select2("enable",false);
				$(this).select2("readonly", true);
			}
    	});	    	  		
  	}
    
    /**
     * 當 json 物件的屬性與欄位的 id 相同時，自動將 json 物件的值設定到欄位上.
     * select2 與 checkbox 元件必需另外處理!
     * 
     * @param data json object
     */
  	web.setValueFromJson = function (data)
  	{
  		//console.log("setValueFromJson=" + JSON.stringify(data));

  		for (var key in data) 
  		{
  			//console.log("setValueFromJson=" + key + " " + data[key]);
  		   $("#"+key).val(data[key]);
  		}
  	}
    

  	//============================ [json functions] =========================
  	/**
  	 * Convert object to  json string.
  	 * @param obj obj to convert
  	 * @return result json string
  	 */
  	web.objToJson = function (obj)
  	{
  		return JSON.stringify(obj);
  	}
  	
  	//============================ [data convert functions] =========================

  	/**
  	 * Convert array buffer to string.
  	 * @param buf array buffer to convert
  	 * @return result string
  	 */
    web.arrayBufferToStr = function(buf) 
    {
    	  return String.fromCharCode.apply(null, new Uint16Array(buf));
    }
    
    /**
     * Convert string to array buffer.
     * @param str string to convert
     * @return result array buffer
     */
    web.strToArrayBuffer = function (str) 
    {
    	  var buf = new ArrayBuffer(str.length*2); // 2 bytes for each char
    	  var bufView = new Uint16Array(buf);
    	  for (var i=0, strLen=str.length; i < strLen; i++) {
    	    bufView[i] = str.charCodeAt(i);
    	  }
    	  return buf;
    }

    /**
     * 輸入數字，依指定的小數位數進位，四捨五入.
     * 
     * @param number number to convert
     * @param fractionDigits fraction digits
     * @return result number
     */
	web.numberRound = function (number, fractionDigits)
	{
		return round(number*Math.pow(10,fractionDigits))/Math.pow(10,fractionDigits);
	}
  	
	/**
	 * Input date object and return iso date time format string.
	 * 回傳日期格式  "YYYY-MM-DD"T"HH:MM:ss".
	 * 
	 * @param dt date object to convert
	 * @return iso date string
	 */
	web.getIsoDateTime = function (dt) 
	{
		var yy = dt.getFullYear().toString();
		var mm = (dt.getMonth() + 1).toString(); // getMonth() is zero-based
		var dd = dt.getDate().toString();
		var hh = dt.getHours().toString();
		var mi = dt.getMinutes().toString();
		var ss = dt.getSeconds().toString();
		return yy + "-" + (mm.length === 2 ? mm : "0" + mm[0]) + "-"
				+ (dd.length === 2 ? dd : "0" + dd[0]) + "T"
				+ (hh.length === 2 ? hh : "0" + hh[0]) + ":"
				+ (mi.length === 2 ? mi : "0" + mi[0]) + ":"
				+ (ss.length === 2 ? mi : "0" + ss[0]) 
				;
	}
	
	
  	//============================ [url process functions] ========================
  	
    /**
     * Get query string value by name.
     * 由 URL 中取得特定參數值
     * @param name name of query string
     * @return value of query string. If name dose not exist then returns empty string.
     */  	
    web.getQueryValueByName = function (name) 
    {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };
    
    /**
     * Get query string value by name.
     * 由 URL 中取得特定參數值
     * @param name name of query string
     * @return value of query string. If name dose not exist then returns empty string.
     */
    web.getQueryValue = function (name)
    {
        // 如果 url 中沒有參數，或是沒有我們要的參數，則返回
        if(location.href.indexOf("?")==-1 || location.href.indexOf(name+'=')==-1)
        {
            return '';
        }
     
        // 取得參數
        var queryString = location.href.substring(location.href.indexOf("?")+1);
     
        // 分離參數對 ?key=value&key2=value2
        var parameters = queryString.split("&");
     
        var pos, paraName, paraValue;
        for(var i=0; i<parameters.length; i++)
        {
            // 取得等號位置
            pos = parameters[i].indexOf('=');
            if(pos == -1) { continue; }
     
            // 取得name 和 value
            paraName = parameters[i].substring(0, pos);
            paraValue = parameters[i].substring(pos + 1);
     
            // 如果查詢的name 等於目前的 name，就返回目前的值，同時，將 url 中的 + 號還原成空格
            if(paraName == name)
            {
                return unescape(paraValue.replace(/\+/g, " "));
            }
        }
        return '';
    };

  	
  	//============================ [check & validation functions] =========================
	/**
	 * Check invalid chars in the given data.
	 * Invalid chars: "!@#$%^&*()+=[]\\\';,/{}.-_|\":<>?"
	 * 
	 * @param data
	 *            data to check
	 * @return valid returns true otherwise returns false.
	 */
    web.isValidData = function (data)
    {
    	var iChars = "!@#$%^&*()+=[]\\\';,/{}.-_|\":<>?";
    	for (var i = 0; i < data.length; i++) 
		{
		  	if (iChars.indexOf(data.charAt(i)) != -1) 
		  	{
		  		return false;
		  	}
		}
    	
    	return true;
    };

    /**
     * Check if input data is not empty string.
     * @param str string to check
     * @return true/flase
     */
    web.notEmptyString = function (str)
    {
    	return web.isEmptyString(str) == false;
    }
    
	/**
	 * Check if input data is empty string.
     * @param str string to check
     * @return true/flase 
	 */
    web.isEmptyString = function (str)
    {
    	if (typeof(str) === 'undefined' || str == null || str.length == 0)
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }
    
    /**
     * Check if input data is numeric.
     * @param n string to check
     * @return true/flase 
     * 
     */
    web.isNumeric = function (n) 
    {
    	 return !isNaN(parseFloat(n)) && isFinite(n);
    }
    
    /**
     * Check if string is starts with prefix.
     * @param str string to check
     * @param prefix prefix string to search
     * @return true/false
     */
    web.startWith = function(str, prefix)
    {
    	return str.lastIndexOf(prefix, 0) === 0;
    }
    
	//============================= [i18n functions] =========================
    
    /**
     * Get user browser language setting.
     * @return user language setting
     */
    web.getUserLang = function ()
    {
    	var lang = "";
    	/*
		console.log("window.navigator.language=" + window.navigator.language);
		console.log("window.navigator.browserLanguage=" + window.navigator.browserLanguage);
		console.log("window.navigator.userLanguage=" + window.navigator.userLanguage);
		console.log("window.navigator.systemLanguage=" + window.navigator.systemLanguage);
		*/
		lang = (window.navigator.language==null)?
			   (window.navigator.browserLanguage==null)?
			   (window.navigator.userLanguage==null)?
			   (window.navigator.systemLanguage==null)?'':window.navigator.systemLanguage:window.navigator.userLanguage:window.navigator.browserLanguage:window.navigator.language;
			
		return lang;  //zh-TW, zh-CN ...
    }

    /**
     * Get all data-i18n item from page.
     * 取得當前頁面的所有 i18n 標籤
     * @return item key array
     */
  	web.getI18nKeys = function ()
  	{
  		var keys = [];
    	$( "[data-i18n]" ).each(function(index, element) 
    	{
        	//console.log("i18n==>" + $(this).attr('data-i18n'));
        	keys.push($(this).attr('data-i18n'));
    	});
    	return keys;
  	}
    
    //============================= [location functions] ==========================
    
    /**
     * Get user timezone offset.
     * @return timezone offset 
     *         Ex: "+8:00"
     */
    web.getTimezoneOffset = function ()
    {
    	var offset = new Date().getTimezoneOffset();//-480
    	var hour = offset / 60 * (-1);
    	var minutes = Math.abs(offset % 60);
		var timezone = (hour>=0?"+":"-") + Math.abs(hour) + ":" + (minutes<10?"0"+minutes:minutes);
    	return timezone;
    };
    
    //============================ [html5 local storage functions] ===========================
    
    /**
     * Check if client supports session storage.
     * @return true/false
     */
    web.isSupportSessionStorage = function()
    {
    	if (window.sessionStorage)
    	{
    		return true;
    	}
    	return false;
    };
        
    /**
     * Remove item from session storage.
     * @param key item key to remove
     */
    web.removeSessionStorageItem = function(key)
    {
    	sessionStorage.removeItem(key);
    };

    /**
     * Store data to session storage.
     * @param key item key to store
     * @param value item value to store
     */
    web.setSessionStorageItem = function(key, value)
    {
    	sessionStorage.setItem(key, value);    
    };
    
    /**
     * Get data from session storage.
     * @param key item key for data
     * @return storage data or null
     */
    web.getSessionStorageItem = function(key)
    {
    	return sessionStorage.getItem(key);
    }
    
    /**
     * Check if client supports local storage.
     * @return true/false
     */
    web.isSupportLocalStorage = function()
    {
    	if (window.localStorage)
    	{
    		return true;
    	}
    	return false;
    };

    /**
     * Remove item from local storage.
     * @param key item key to remove
     */
    web.removeLocalStorageItem = function(key)
    {
    	localStorage.removeItem(key);
    };

    /**
     * Store data to local storage.
     * @param key item key to store
     * @param value item value to store
     */
    web.setLocalStorageItem = function(key, value)
    {
    	localStorage.setItem(key, value);    
    };
    
    /**
     * Get data from session storage.
     * @param key item key for data
     * @return storage data or null
     */
    web.getLocalStorageItem = function(key)
    {
    	return localStorage.getItem(key);
    }
    
    //============================ [Dynamic page element] ==============================
    web.addJs = function (jsPath)
    {
    	var headID = document.getElementsByTagName("head")[0]; 
    	var newJs = document.createElement('script');
    	newJs .type = 'text/javascript';
    	newJs .src= jsPath;
    	headID.appendChild(newJs);
    }
    
    web.addCss = function (cssPath)
    {
    	var headID = document.getElementsByTagName("head")[0]; 
    	var newCss = document.createElement('link');
    	newCss.type = 'text/css';
    	newCss.rel = "stylesheet";
    	newCss.href = cssPath;
    	headID.appendChild(newCss);    	
    }
    
    
	
	return web;
	
})(fstop || {}, jQuery);
