topWebSocketUtil.setWssUrl("wss://localhost:9009/",ValidateLegalURL_Callback_Mac);

//ConnectCard方法撈出來的讀卡機名稱陣列
var readerNamesArray = "";
//目前讀卡機名稱陣列的位置
var currentIndexAtReaderNamesArray = -1;
//Callback裡面塞要回傳的結果
var result = "";
//重試次數
var retryTime = 0;

function ConnectCard(outerCallBackFunction){
	//還原成預設值
	readerNamesArray = "";
	currentIndexAtReaderNamesArray = -1;
	
	var rpcName = "ConnectCard";
	result = "";
	
	var nowDate = new Date();
	if(window.console){console.log("ConnectCard Time is " + nowDate.getHours() + "時" + nowDate.getMinutes() + "分" + nowDate.getSeconds() + "秒" + nowDate.getMilliseconds() + "毫秒");}
	if(window.console){console.log("retryTime=" + retryTime);}
	
	topWebSocketUtil.invokeRpcDispatcher(ConnectCard_Callback,rpcName);
	
	function ConnectCard_Callback(rpcStatus,rpcReturn){
		try{
			topWebSocketUtil.tryRpcStatus(rpcStatus);
			var splitedReturn = rpcReturn.split('|');
			if(window.console){console.log("splitedReturn="+splitedReturn);}
			
			//成功
	        if(splitedReturn[0] == "0"){
	        	//在某些情況下，未插讀卡機時，元件會回傳0，所以要另外判斷splitedReturn[1]是否有值
				var splitedReturnOne = splitedReturn[1].trim();
				if(window.console){console.log("splitedReturnOne=" + splitedReturnOne);}
				
				//有值
				if(splitedReturnOne != ""){
					result = splitedReturnOne;
		        	
		        	//將值ASSIGN給readerNamesArray
					readerNamesArray= splitedReturnOne.split(",");
				}
				//沒值等同失敗
				else{
					//重試
		        	if(retryTime < 3){
		        		retryTime += 1;
		        		
		        		setTimeout(function(){ConnectCard(outerCallBackFunction);},1000);
		        		
		        		return;
		        	}
		        	//不重試了
		        	else{
		        		//0且splitedReturnOne沒值，代碼改成E001
		        		if(window.console){console.log("CHANGE MESSAGECODE TO E001");}
		        		alert(GetErrorMessage("E001"));
			        	
		        		result = false;
		        	}
				}
			}
	        //失敗
	        else{
	        	//重試
	        	if(retryTime < 3){
	        		retryTime += 1;
	        		
	        		setTimeout(function(){ConnectCard(outerCallBackFunction);},1000);
	        		
	        		return;
	        	}
	        	//不重試了
	        	else{
	        		alert(GetErrorMessage(splitedReturn[0]));
		        	
		        	//這裡要分沒安裝元件和有安裝元件但有E系列的錯誤訊息
		        	//沒安裝元件
		        	if(splitedReturn[0] == "E_Send_11_OnError_1006"){
		        		result = "E_Send_11_OnError_1006";
		        	}
		        	//有安裝元件但有E系列的錯誤訊息
		        	else{
		        		result = false;
		        	}
	        	}
	        }
		}
		catch(exception){
			alert(exception);
			result = false;
		}
		//歸零
		retryTime = 0;
		
		var fullCallBack = outerCallBackFunction + "('" + result + "')";

		eval(fullCallBack);
	}
}
//和ConnectCard的差別在於不ALERT MESSAGE
function ConnectCardLite(outerCallBackFunction){
	//還原成預設值
	readerNamesArray = "";
	currentIndexAtReaderNamesArray = -1;
	
	var rpcName = "ConnectCard";
	result = "";
	
	var nowDate = new Date();
	if(window.console){console.log("ConnectCardLite Time is " + nowDate.getHours() + "時" + nowDate.getMinutes() + "分" + nowDate.getSeconds() + "秒" + nowDate.getMilliseconds() + "毫秒");}
	if(window.console){console.log("retryTime=" + retryTime);}
	
	topWebSocketUtil.invokeRpcDispatcher(ConnectCard_Callback,rpcName);
	
	function ConnectCard_Callback(rpcStatus,rpcReturn){
		try{
			topWebSocketUtil.tryRpcStatus(rpcStatus);
			var splitedReturn = rpcReturn.split('|');
			if(window.console){console.log("splitedReturn="+splitedReturn);}
			
			//成功
	        if(splitedReturn[0] == "0"){
	        	result = splitedReturn[1];
	        	
	        	//將值ASSIGN給readerNamesArray
				readerNamesArray= splitedReturn[1].split(",");
			}
	        //失敗
	        else{
	        	//重試
	        	if(retryTime < 3){
	        		retryTime += 1;
	        		
	        		setTimeout(function(){ConnectCardLite(outerCallBackFunction);},1000);
	        		
	        		return;
	        	}
	        	//不重試了
	        	else{
		        	//這裡要分沒安裝元件和有安裝元件但有E系列的錯誤訊息
		        	//沒安裝元件
		        	if(splitedReturn[0] == "E_Send_11_OnError_1006"){
		        		result = "E_Send_11_OnError_1006";
		        	}
		        	//有安裝元件但有E系列的錯誤訊息
		        	else{
		        		result = false;
		        	}
	        	}
	        }
		}
		catch(exception){
			alert(exception);
			result = false;
		}
		//歸零
		retryTime = 0;
		
		var fullCallBack = outerCallBackFunction + "('" + result + "')";

		eval(fullCallBack);
	}
}
function DetectCard(readerName,outerCallBackFunction){
	var rpcName = "DetectCard";
	result = "";
	
	topWebSocketUtil.invokeRpcDispatcher(DetectCard_Callback,rpcName,readerName);

	function DetectCard_Callback(rpcStatus,rpcReturn){
		try{
			topWebSocketUtil.tryRpcStatus(rpcStatus);
			var DetectCardData = rpcReturn.split('|');
			if(window.console){console.log("DetectCardData="+DetectCardData);}
			
			//成功
			if(DetectCardData[0] == "0"){
				result = DetectCardData[1];
			}
			//失敗
			else{
				alert(GetErrorMessage(DetectCardData[0]));
	        	result = false;
	        }
		}
		catch(exception){
			alert(exception);
			result = false;
		}
		var fullCallBack = outerCallBackFunction + "('" + result + "')";
		
		eval(fullCallBack);
	}
}
function FindOKReader(outerCallBackFunction){
	//準備
	++currentIndexAtReaderNamesArray;
	//目前的讀卡機名稱
	var thisReaderName = readerNamesArray[currentIndexAtReaderNamesArray];
	
	var rpcName = "DetectCard";
	
	topWebSocketUtil.invokeRpcDispatcher(DetectCard_Callback,rpcName,thisReaderName);

	function DetectCard_Callback(rpcStatus,rpcReturn){
		try{
			topWebSocketUtil.tryRpcStatus(rpcStatus);
			var DetectCardData = rpcReturn.split('|');
			if(window.console){console.log("DetectCardData="+DetectCardData);}
			
			//成功
			if(DetectCardData[0] == "0"){
				var fullCallBack = outerCallBackFunction + "('" + readerNamesArray[currentIndexAtReaderNamesArray] + "')";
				
				//還原成預設值
				readerNamesArray = "";
				currentIndexAtReaderNamesArray = -1;
				
				eval(fullCallBack);
			}
			//失敗
			else{
				//結束
				if(currentIndexAtReaderNamesArray == readerNamesArray.length - 1){
					//還原成預設值
					readerNamesArray = "";
					currentIndexAtReaderNamesArray = -1;
					alert(GetErrorMessage(DetectCardData[0]));
				}
				//繼續
				else{
					FindOKReader(outerCallBackFunction);
				}
	        }
		}
		catch(exception){
			if(window.console){console.log("exception="+exception);}
			alert(exception);
		}
	}
}
function GetUnitCode(readerName,outerCallBackFunction){
    var rpcName = "GetUnitCode";
    result = "";
    
	topWebSocketUtil.invokeRpcDispatcher(GetUnitCode_Callback,rpcName,readerName);
	
    function GetUnitCode_Callback(rpcStatus,rpcReturn){
    	try{
    		topWebSocketUtil.tryRpcStatus(rpcStatus);
            var UnitCodeData = rpcReturn.split('|');
            if(window.console){console.log("UnitCodeData="+UnitCodeData);}
            
            //成功
			if(UnitCodeData[0] == "0"){
				result = UnitCodeData[1];
			}
			//失敗
			else{
				alert(GetErrorMessage(UnitCodeData[0]));
	        	result = false;
	        }
    	}
    	catch(exception){
    		alert(exception);
    		result = false;
        }
		var fullCallBack = outerCallBackFunction + "('" + result + "')";
		
		eval(fullCallBack);
    }
}
function GetMainAccount(readerName,outerCallBackFunction){
	var rpcName = "GetMainAccount";
	result = "";
	
	topWebSocketUtil.invokeRpcDispatcher(GetMainAccount_Callback,rpcName,readerName);

	function GetMainAccount_Callback(rpcStatus,rpcReturn){
		try{
			topWebSocketUtil.tryRpcStatus(rpcStatus);
			var AccountData = rpcReturn.split('|');
			if(window.console){console.log("AccountData="+AccountData);}
			
			//成功
			if(AccountData[0] == "0"){
				result = AccountData[1].split(',')[0];
			}
			//失敗
			else{
				alert(GetErrorMessage(AccountData[0]));
	        	result = false;
	        }
		}
		catch(exception){
			alert(exception);
			result = false;
		}
		var fullCallBack = outerCallBackFunction + "('" + result + "')";

		eval(fullCallBack);
	}
}
function GenerateTAC(readerName,transData,password,outerCallBackFunction){
	var rpcName = "GenerateTAC";
	result = "";
	
	topWebSocketUtil.invokeRpcDispatcher(GenerateTAC_Callback,rpcName,readerName,transData,password);

	function GenerateTAC_Callback(rpcStatus,rpcReturn){
		try{
			topWebSocketUtil.tryRpcStatus(rpcStatus);
			var GenerateTACData = rpcReturn.split('|');
			if(window.console){console.log("GenerateTACData="+GenerateTACData);}
			
			//成功
			if(GenerateTACData[0] == "0"){
				result = rpcReturn;
			}
			//失敗
			else{
				alert(GetErrorMessage(GenerateTACData[0]));
	        	result = false;
	        }
		}
		catch(exception){
			alert(exception);
			result = false;
		}
		var fullCallBack = outerCallBackFunction + "('" + result + "')";
		
		eval(fullCallBack);
	}
}
function VerifyPin(readerName,password,outerCallBackFunction){
	var rpcName = "VerifyPin";
	result = "";
	
	topWebSocketUtil.invokeRpcDispatcher(VerifyPin_Callback,rpcName,readerName,password);

	function VerifyPin_Callback(rpcStatus,rpcReturn){
		try{
			topWebSocketUtil.tryRpcStatus(rpcStatus);
			var VerifyPinData = rpcReturn.split('|');
			if(window.console){console.log("VerifyPinData="+VerifyPinData);}
			
			//成功
			if(VerifyPinData[0] == "0"){
				result = true;
			}
			//失敗
			else{
				alert(GetErrorMessage(VerifyPinData[0]));
	        	result = false;
	        }
		}
		catch(exception){
			alert(exception);
			result = false;
	    }
		var fullCallBack = outerCallBackFunction + "('" + result + "')";
		
		eval(fullCallBack);
	}
}
function RemoveThenInsertCard(maskID,maxCountDown,readerName,outerCallBackFunction){
	var rpcName = "TestRemoveThenInsertCard";
	//元件開始偵測拔插卡和倒數計時
	topWebSocketUtil.invokeRpcDispatcher(TestRemoveThenInsertCard_Callback,rpcName,readerName,maxCountDown);
	
	//顯示拔插卡的畫面，元件不顯示畫面了
	showRemoveInsertPad(maskID,maxCountDown);
	
	RelocateCountdown(maskID,parseInt(document.getElementById("CountdownNum").value),readerName,outerCallBackFunction);
	
	function TestRemoveThenInsertCard_Callback(rpcStatus,rpcReturn){
		try{
			RelocatedFinish(maskID);
			
			topWebSocketUtil.tryRpcStatus(rpcStatus);
			//"OK":正常
			//"CANCEL":按"取消"按鈕
			//"TIMEOUT":操作逾時
			//其他值:異常
			if(window.console){console.log("rpcReturn="+rpcReturn);}
	        
			//成功
			if(rpcReturn == "OK"){
				var fullCallBack = outerCallBackFunction + "()";
				
				eval(fullCallBack);
			}
			//失敗
			else{
				if(rpcReturn == "TIMEOUT"){
					alert("拔插卡操作逾時，請重新再試");
				}
				else if(rpcReturn == "CANCEL"){
					alert("使用者取消拔插卡操作，請重新再試");
				}
				else{
					alert("拔插卡操作錯誤，請重新再試");
				}
			}
		}
		catch(exception){
			alert(exception);
		}
	}
}
//TIMEOUT物件
var setTimeoutObject;
function RelocateCountdown(maskID,countDown,readerName,outerCallBackFunction){
	if(window.console){console.log("countDown="+countDown);}
	
	if(countDown > 0){
		document.getElementById("CountdownNum").innerHTML = countDown - 1;
		
//		if(countDown < 10){
//			document.getElementById("CountdownNum").style.color = "red";
//		}

		setTimeoutObject = setTimeout(function(){
			RelocateCountdown(maskID,parseInt(document.getElementById("CountdownNum").innerHTML),readerName,outerCallBackFunction);
		},1000);
	}
	else{
		RelocatedFinish(maskID);
	}
}
function RelocatedFinish(maskID){
	clearTimeout(setTimeoutObject);
	
//	document.getElementById(maskID).innerHTML = "";
	$("#removeInsertMask").hide();
	blockId = blockUI();
}
function GetAccounts(readerName,outerCallBackFunction){
	var rpcName = "GetMainAccount";
	var result;
	
	topWebSocketUtil.invokeRpcDispatcher(GetMainAccount_Callback,rpcName,readerName);

	function GetMainAccount_Callback(rpcStatus,rpcReturn){
		try{
			topWebSocketUtil.tryRpcStatus(rpcStatus);
			var AccountData = rpcReturn.split('|');
			
			if(AccountData[0] != "E000"){
				AccountData = rpcReturn.split(',');
				alert(formatCardMsg(AccountData[0]));
				result = false;
			}
			else{
				result = AccountData[1];
			}
		}
		catch(exception){
			alert("無法取得卡片帳號");
			result = false;
		}
		var fullCallBack = outerCallBackFunction + "('" + result + "')";

		eval(fullCallBack);
	}
}
function GetNoteData(readerName,outerCallBackFunction){
	var rpcName = "GetNoteData";
	var result;
	
	topWebSocketUtil.invokeRpcDispatcher(GetNoteData_Callback,rpcName,readerName);

	function GetNoteData_Callback(rpcStatus,rpcReturn){
		try{
            topWebSocketUtil.tryRpcStatus(rpcStatus);
            var NoteData = rpcReturn.split('|');

            if(NoteData[0] != "E000"){
            	NoteData = rpcReturn.split(',');
            	alert(formatCardMsg(NoteData[0]));
            	result = false;
            }
            else{
            	result = NoteData[1];
            }
		}
		catch(exception){
			alert("無法取得卡片備註");
			result = false;
		}
		var fullCallBack = outerCallBackFunction + "('" + result + "')";
		
		eval(fullCallBack);
    }
}
function GetTranAccount(readerName,outerCallBackFunction){
	var rpcName = "GetTranAccount";
	var result;
	
	topWebSocketUtil.invokeRpcDispatcher(GetTranAccount_Callback,rpcName,readerName);

	function GetTranAccount_Callback(rpcStatus,rpcReturn){
		try{
			topWebSocketUtil.tryRpcStatus(rpcStatus);
			var TranAccountData = rpcReturn.split('|');

			if(TranAccountData[0] != "E000"){
				TranAccountData = rpcReturn.split(',');
				alert(formatCardMsg(TranAccountData[0]));
				result = false;
			}
			else{
				result = TranAccountData[1];
			}
		}
		catch(exception){
			alert("無法取得卡片交易帳號");
			result = false;
		}
		var fullCallBack = outerCallBackFunction + "('" + result + "')";
		
		eval(fullCallBack);
	}
}
function ChangePin(readerName,oldPassword,newPassword,outerCallBackFunction){
    var rpcName = "ChangePin";
    var result;
    
    topWebSocketUtil.invokeRpcDispatcher(ChangePin_Callback,rpcName,readerName,oldPassword,newPassword);
    
	function ChangePin_Callback(rpcStatus,rpcReturn){
    	try{
    		topWebSocketUtil.tryRpcStatus(rpcStatus);
    		var ChangePinData = rpcReturn.split('|');

			if(ChangePinData[0] != "E000"){
				ChangePinData = rpcReturn.split(',');
				alert(formatCardMsg(ChangePinData[0]));
				result = false;
			}
			else{
				result = true;
			}
    	}
    	catch(exception){
    		alert("無法變更卡片密碼");
    		result = false;
    	}
    	var fullCallBack = outerCallBackFunction + "('" + result + "')";
		
		eval(fullCallBack);
    }
}
function Finalize(readerName){
	var rpcName = "Finalize";

	topWebSocketUtil.invokeRpcDispatcher(Finalize_Callback,rpcName,readerName);

	function Finalize_Callback(rpcStatus,rpcReturn){
		topWebSocketUtil.tryRpcStatus(rpcStatus);
	}
}
function GetVersion(newVersion,outerCallBackFunction){
	var rpcName = "GetVersion";
	result = "";
	
	topWebSocketUtil.invokeRpcDispatcher(GetVersion_Callback,rpcName);

	function GetVersion_Callback(rpcStatus,rpcReturn){
		try{
			topWebSocketUtil.tryRpcStatus(rpcStatus);
			var currentVersion = rpcReturn;
			if(window.console){console.log("currentVersion="+currentVersion);}
			if(window.console){console.log("newVersion="+newVersion);}
			
			var currentVersionNO = parseInt(currentVersion.replace(/\./g,""));
			var newVersionNO = parseInt(newVersion.replace(/\./g,""));
			
			if(window.console){console.log("currentVersionNO="+currentVersionNO);}
			if(window.console){console.log("newVersionNO="+newVersionNO);}
			
			//不用更新
			if(currentVersionNO >= newVersionNO){
				result = true;
			}
			//要更新
			else{
	        	result = false;
	        }
		}
		catch(exception){
			alert(exception);
			result = false;
		}
		var fullCallBack = outerCallBackFunction + "('" + result + "')";
		
		eval(fullCallBack);
	}
}