/*
 * 檢核指定名稱的Radio物件是否被點選
 * PARAM sFieldName:欄位中文描述
 * PARAM sRadioName:Radio物件名稱
 * return 
 */
function CheckRadio(sFieldName, sRadioName) {

	if (typeof (sFieldName) != "string") {
		alert("CheckRadio含有無效的參數sFieldName");
		return false;
	}

	if (typeof (sRadioName) != "string") {
		alert("CheckRadio含有無效的參數sRadioName");
		return false;
	}

	try {
		var aRadios = document.getElementsByName(sRadioName);

		if (aRadios.length == 0) {
			alert("沒有可點選的" + sFieldName);
			return false;
		}

		var bHasChecked = false;
		for (var i = 0; i < aRadios.length; i++) {
			if (aRadios[i].checked) {
				bHasChecked = true;
				break;
			}
		}

		if (!bHasChecked) {
			alert("請點選" + sFieldName);
			aRadios[0].focus();
			return false;
		}
	} catch (exception) {
		alert("檢核" + sFieldName + "時發生錯誤:" + exception)
		return false;
	}

	return true;
}

/*
 * 檢查數字輸入框內容(不含加號、減號、小數點) PARAM sFieldName:欄位中文描述 PARAM oField:數字輸入框 PARAM
 * bCanEmpty:可否為空字串 return
 */
function CheckNumber(sFieldName, oField, bCanEmpty) {

	if (typeof (sFieldName) != "string") {
		alert("CheckNumber含有無效的參數sFieldName");
		return false;
	}

	try {
		var sNumber = oField.value;

		if (bCanEmpty == false && sNumber == "") {
			alert("請輸入" + sFieldName);
			oField.focus();
			return false;
		}

		var reNumber = /[^0-9]/;
		if (reNumber.test(sNumber)) {
			alert(sFieldName + "欄位只能輸入數字(0~9)");
			oField.focus();
			return false;
		}
	} catch (exception) {
		alert("檢核" + sFieldName + "時發生錯誤:" + exception);
		return false;
	}

	return true;
}

/*
 * 檢查金額輸入框內容 PARAM sFieldName:欄位中文描述 PARAM oField:金額輸入框 PARAM bIsInteger:是否整數
 * PARAM fMin:最小值，不限制時給null PARAM fMax:最大值，不限制時給null PARAM bCanEmpty:可否為空字串
 * return
 */
function CheckAmount(sFieldName, oField, bIsInteger, fMin, fMax) {

	if (typeof (sFieldName) != "string") {
		alert("CheckAmount含有無效的參數sFieldName");
		return false;
	}

	if (fMin != null && fMax != null && fMin > fMax) {
		alert("CheckAmount的參數fMin不能大於fMax");
		return false;
	}

	try {
		var sAmount = oField.value;

		if (sAmount == "") {
			alert("請輸入" + sFieldName);
			oField.focus();
			return false;
		}

		if (isNaN(sAmount) || sAmount.startsWith(' ') || sAmount.endsWith(' ')) {
			alert(sFieldName + "欄位請輸入正確的金額格式");
			oField.focus();
			return false;
		}

		if (sAmount.indexOf("+") == 0) {
			alert(sFieldName + "欄位請勿輸入加號");
			oField.focus();
			return false;
		}

		if (sAmount.charAt(0) == "0" && sAmount.length > 1) {
			alert(sFieldName + "欄位請勿以零為開頭");
			oField.focus();
			return false;
		}

		if (bIsInteger == true && sAmount.indexOf(".") != -1) {
			alert(sFieldName + "欄位請輸入整數");
			oField.focus();
			return false;
		}

		if (bIsInteger == false && sAmount.indexOf(".") == sAmount.length - 1) {
			alert(sFieldName + "欄位請輸入正確的含小數點金額格式");
			oField.focus();
			return false;
		}

		if (fMin != null && sAmount < fMin) {
			alert(sFieldName + "不能小於" + fMin);
			oField.focus();
			return false;
		}

		if (fMax != null && sAmount > fMax) {
			alert(sFieldName + "不能大於" + fMax);
			oField.focus();
			return false;
		}
	} catch (exception) {
		alert("檢核" + sFieldName + "時發生錯誤:" + exception);
		return false;
	}

	return true;
}

/*
 * 檢查交易密碼 PARAM oField:交易密碼輸入框 return
 */
function CheckTxnPassword(oField) {

	try {
		var sValue = oField.value;
		var reNumber = /[^0-9]/;

		if (sValue.length < 6 || sValue.length > 8 || reNumber.test(sValue)) {
			alert("交易密碼欄位請輸入6-8位數字");

			try {
				oField.focus();
			} catch (exception) {
			}

			return false;
		}
	} catch (exception) {
		alert("檢核" + sFieldName + "時發生錯誤:" + exception);
		return false;
	}

	return true;
}

/*
 * 檢核Select物件 PARAM sFieldName:欄位中文描述 PARAM aSelect:Select物件 PARAM
 * sDoNotSelect:不能為選取值的字串，無限制時給null return
 */
function CheckSelect(sFieldName, aSelect, sDoNotSelect) {

	if (typeof (sFieldName) != "string") {
		alert("CheckSelect含有無效的參數sFieldName");
		return false;
	}

	try {
		if (aSelect.length == 0
				|| (aSelect.length == 1 && aSelect[0].value == sDoNotSelect)) {
			alert("沒有可選擇的" + sFieldName);
			aSelect.focus();
			return false;
		}

		if (typeof (sDoNotSelect) == "string") {
			var bHasSelected = false;

			for (var i = 0; i < aSelect.length; i++) {
				var o = aSelect[i];
				if (o.selected) {
					bHasSelected = true;

					if (o.value == sDoNotSelect) {
						alert("請選擇" + sFieldName);
						aSelect.focus();
						return false;
					}
				}
			}

			// 當所有的項目皆未選取時，比較第一個項目
			if (bHasSelected == false && aSelect.length > 0) {
				if (aSelect[0].value == sDoNotSelect) {
					alert("請選擇" + sFieldName);
					aSelect.focus();
					return false;
				}
			}
		}
	} catch (exception) {
		alert("檢核" + sFieldName + "時發生錯誤:" + exception)
		return false;
	}

	return true;
}

/*
 * 檢查日期輸入框內容 PARAM oField:eMAIL輸入欄位 return
 */
function EmailCheck(oField) {
	var rexpress = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	var val = oField.value;
	if (!rexpress.test(val)) {
		alert("電子郵件帳號格式有誤");
		oField.focus();
		return false;
	} else
		return true;
}

/*
 * 檢查日期輸入框內容 PARAM sFieldName:欄位中文描述 PARAM oDate:要檢核的日期輸入框物件 PARAM
 * sMinDate:最小日期字串，無最小日限制時給null PARAM sMaxDate:最大日期字串，無最大日限制時給null return
 */
function CheckDate(sFieldName, oDate, sMinDate, sMaxDate) {

	if (typeof (sFieldName) != "string") {
		alert("CheckDate含有無效的參數sFieldName");
		return false;
	}

	// 檢查最小日參數
	var dMinDate = null;
	if (sMinDate != null) {
		if (IsValidDate(sMinDate)) {
			dMinDate = new Date(sMinDate);
		} else {
			alert("CheckDate含有無效的參數sMinDate");
			return false;
		}
	}

	// 檢查最大日參數
	var dMaxDate = null;
	if (sMaxDate != null) {
		if (IsValidDate(sMaxDate)) {
			dMaxDate = new Date(sMaxDate);
		} else {
			alert("CheckDate含有無效的參數sMaxDate");
			return false;
		}
	}

	if (dMinDate != null && dMaxDate != null && dMinDate > dMaxDate) {
		alert("CheckDate的參數sMinDate不能大於sMaxDate");
		return false;
	}

	try {
		if (!IsValidDate(oDate.value)) {
			alert("請輸入正確的" + sFieldName + "格式，例如 2009/01/31");
			oDate.focus();
			return false;
		}
		var dDate = new Date(oDate.value);

		if (dMaxDate != null && dDate > dMaxDate) {
			alert(sFieldName + "不能大於 " + sMaxDate);
			oDate.focus();
			return false;
		}

		if (dMinDate != null && dDate < dMinDate) {
			alert(sFieldName + "不能小於 " + sMinDate);
			oDate.focus();
			return false;
		}
	} catch (exception) {
		alert("檢核" + sFieldName + "時發生錯誤:" + exception);
		return false;
	}

	return true;
}

/*
 * 檢查民國日期輸入框內容 PARAM sFieldName:欄位中文描述 PARAM oDate:要檢核的日期輸入框物件 PARAM
 * sMinDate:最小日期字串，無最小日限制時給null PARAM sMaxDate:最大日期字串，無最大日限制時給null return
 */
function CheckCDate(sFieldName, oDate, sMinDate, sMaxDate) {

	// 檢查是否為字串
	if (typeof (sFieldName) != "string") {
		alert("CheckCDate含有無效的參數sFieldName");
		return false;
	}

	// 檢查最小日參數
	var dMinDate = null;
	if (sMinDate != null) {
		var min = CyearToEyear(sMinDate);
		if (IsValidDate(min)) {
			dMinDate = new Date(min);
		} else {
			alert("CheckCDate含有無效的參數sMinDate");
			return false;
		}
	}

	// 檢查最大日參數
	var dMaxDate = null;
	if (sMaxDate != null) {
		var max = CyearToEyear(sMaxDate);
		if (IsValidDate(max)) {
			dMaxDate = new Date(max);
		} else {
			alert("CheckCDate含有無效的參數sMaxDate");
			return false;
		}
	}

	if (dMinDate != null && dMaxDate != null && dMinDate > dMaxDate) {
		alert("CheckCDate的參數sMinDate不能大於sMaxDate");
		return false;
	}

	try {
		var sDate = CyearToEyear(oDate.value);

		if (!IsValidDate(sDate)) {
			alert("請輸入正確的" + sFieldName + "格式，例如 097/01/31");
			TryFocus(oDate);
			return false;
		}
		var dDate = new Date(sDate);

		if (dMaxDate != null && dDate > dMaxDate) {
			alert(sFieldName + "不能大於 " + sMaxDate);
			TryFocus(oDate);
			return false;
		}

		if (dMinDate != null && dDate < dMinDate) {
			alert(sFieldName + "不能小於 " + sMinDate);
			TryFocus(oDate);
			return false;
		}
	} catch (exception) {
		alert("檢核" + sFieldName + "時發生錯誤:" + exception);
		return false;
	}

	return true;
}

/*
 * 檢查數字輸入框內容(不含加號、減號、小數點),並依指定長度作檢核 PARAM sFieldName:欄位中文描述 PARAM oField:數字輸入框
 * PARAM bCanEmpty:可否為空字串 PARAM len:最小值，長度限制 return
 */
function CheckNumWithDigit(sFieldName, oField, bCanEmpty, len) {

	if (typeof (sFieldName) != "string") {
		alert("CheckNumWithDigit含有無效的參數sFieldName");
		return false;
	}

	try {
		var sNumber = oField.value;

		if (bCanEmpty == false && sNumber == "") {
			alert("請輸入" + sFieldName);
			oField.focus();
			return false;
		}

		var reNumber = /[^0-9]/;
		if (reNumber.test(sNumber)) {
			alert(sFieldName + "欄位只能輸入數字(0~9)");
			oField.focus();
			return false;
		}

		if (sNumber.length != len) {
			alert(sFieldName + "欄位應為" + len + "位數字");
			oField.focus();
			return false;
		}
	} catch (exception) {
		alert("檢核" + sFieldName + "時發生錯誤:" + exception);
		return false;
	}

	return true;
}

/*
 * 檢查是否為有效之Email帳號 PARAM sFieldName:欄位中文描述 PARAM oField:文字輸入框 PARAM
 * bCanEmpty:可否為空字串 return
 */
function CheckMail(sFieldName, oField, bCanEmpty) {

	if (typeof (sFieldName) != "string") {
		alert("CheckMail含有無效的參數sFieldName");
		return false;
	}

	try {
		var mail = oField.value;

		if (bCanEmpty == false && mail == "") {
			alert("請輸入" + sFieldName);
			oField.focus();
			return false;
		}

		var str = strReplaceAll(mail, ";", ",") + ",";
		var str1 = "";
		var str2 = "";

		if (mail.indexOf("<") != -1 || mail.indexOf(">") != -1) {
			alert("輸入之電子郵件帳號格式有誤");
			oField.focus();
			return false;
		}

		var i = str.indexOf(",");

		if (i == -1 && mail != ""
				&& (mail.indexOf("@") == -1 || mail.indexOf(".") == -1)) {
			alert("輸入之電子郵件帳號格式有誤");
			oField.focus();
			return false;
		}

		while (i != -1) {
			str1 = str.substring(0, i);
			str2 = str.substring(0, i + 1);
			str = str.replace(str2, "");

			if (str1 != ""
					&& (str1.indexOf("@") == -1 || str1.indexOf(".") == -1)) {
				alert("輸入之電子郵件帳號格式有誤");
				oField.focus();
				return false;
			}
			i = str.indexOf(",");
		}
	} catch (exception) {
		alert("檢核" + sFieldName + "時發生錯誤:" + exception);
		return false;
	}
	return true;
}

function strReplaceAll(str,strFind,strReplace) {
    var returnStr = str;
    var start = returnStr.indexOf(strFind);
    while (start>=0)
    {
        returnStr = returnStr.substring(0,start) + strReplace + returnStr.substring(start+strFind.length,returnStr.length);
        start = returnStr.indexOf(strFind,start+strReplace.length);
    }
    return returnStr;
}

function CheckDateScope(sFieldName, oBaseDate, oStartDate, oEndDate,
		bCanOverBaseDate, iMonthAgo, iMonthScope) {
	// /<summary>檢核日期範圍</summary>
	// /<param name="sFieldName">欄位中文描述</param>
	// /<param name="oBaseDate">基準日期，可為輸入框物件或日期物件或日期字串</param>
	// /<param name="oStartDate">起始日輸入框物件</param>
	// /<param name="oEndDate">終止日輸入框物件</param>
	// /<param name="bCanOverBaseDate">起始日或終止日可否大於基準日</param>
	// /<param name="iMonthAgo">以基準日往前推算之月份，無限制時給null</param>
	// /<param name="iMonthScope">起始日與終止日之相距月份限制，無月份限制時給null</param>
	// /<returns>true:檢核成功 false:檢核失敗</returns>

	if (typeof (sFieldName) != "string") {
		alert("CheckDateScope含有無效的參數sFieldName");
		return false;
	}

	try {
		// 基準日
		var dBaseDate;
		if (oBaseDate instanceof Date) {// 日期物件
			dBaseDate = oBaseDate;
		} else if (typeof (oBaseDate) == "string") {// 日期字串，如2008/08/01
			if (!IsValidDate(oBaseDate)) {
				alert("請使用正確的基準日參數，例如 2009/01/31");
				return false;
			}
			dBaseDate = new Date(oBaseDate);
		} else {// 輸入框物件
			if (!IsValidDate(oBaseDate.value)) {
				alert("請輸入正確的基準日，例如 2009/01/31");
				oBaseDate.focus();
				return false;
			}
			dBaseDate = new Date(oBaseDate.value);
		}
		var oBaseYear = dBaseDate.getFullYear().toString();
		var oBaseMonth = dBaseDate.getMonth() + 1;
		oBaseMonth = oBaseMonth < 10 ? "0" + oBaseMonth : oBaseMonth.toString();
		var oBaseDay = dBaseDate.getDate();
		oBaseDay = oBaseDay < 10 ? "0" + oBaseDay : oBaseDay.toString();
		sBaseDate = oBaseYear + "/" + oBaseMonth + "/" + oBaseDay;

		// 最小日
		var dMinDate = null;
		var sMinDate = null;
		if (iMonthAgo != null) {
			var oMinMonth = null;
			if (oBaseMonth > iMonthAgo)
				oMinMonth = oBaseMonth - iMonthAgo - 1;
			else
				oMinMonth = -1 - (iMonthAgo - oBaseMonth);

			dMinDate = new Date(sBaseDate);
			dMinDate.setMonth(oMinMonth);
			dMinDate.setDate(1);

			oMinMonth = dMinDate.getMonth() + 1;
			oMinMonth = oMinMonth < 10 ? "0" + oMinMonth : oMinMonth.toString();

			sMinDate = dMinDate.getFullYear() + "/" + oMinMonth + "/01";
		}

		// 起始日
		if (!IsValidDate(oStartDate.value)) {
			alert("請輸入正確的起始日格式，例如 2009/01/31");
			oStartDate.focus();
			return false;
		}
		var dStartDate = new Date(oStartDate.value);
		var oStartYear = dStartDate.getFullYear().toString();
		var oStartMonth = dStartDate.getMonth() + 1;
		oStartMonth = oStartMonth < 10 ? "0" + oStartMonth : oStartMonth
				.toString();
		var oStartDay = dStartDate.getDate();
		var iStartDay = oStartDay;
		oStartDay = oStartDay < 10 ? "0" + oStartDay : oStartDay.toString();

		if (!bCanOverBaseDate && dStartDate > dBaseDate) {
			alert("起始日不能大於 " + sBaseDate);
			oStartDate.focus();
			return false;
		}

		if (dMinDate != null && dStartDate < dMinDate) {
			alert("起始日不能小於 " + sMinDate);
			oStartDate.focus();
			return false;
		}

		// 終止日
		if (!IsValidDate(oEndDate.value)) {
			alert("請輸入正確的終止日格式，例如 2009/01/31");
			oEndDate.focus();
			return false;
		}
		var dEndDate = new Date(oEndDate.value);
		var oEndYear = dEndDate.getFullYear().toString();
		var oEndMonth = dEndDate.getMonth() + 1;
		oEndMonth = oEndMonth < 10 ? "0" + oEndMonth : oEndMonth.toString();
		var oEndDay = dEndDate.getDate();
		oEndDay = oEndDay < 10 ? "0" + oEndDay : oEndDay.toString();

		// 最大日
		var dMaxDate = null;
		var sMaxDate = null;
		if (iMonthScope != null) {
			if (iMonthScope == iMonthAgo) {
				dMaxDate = dBaseDate;
				sMaxDate = sBaseDate;
			} else {
				oMaxMonth = dStartDate.getMonth() + 1 + iMonthScope;
				if (oMaxMonth > 12) {
					var i = parseInt(oMaxMonth / 12);
					oMaxMonth = oMaxMonth - 12 * i;
					if (oMaxMonth == 0) {
						oMaxMonth = 12;
						i = i - 1;
					}
					oMaxMonth = oMaxMonth < 10 ? "0" + oMaxMonth : oMaxMonth
							.toString();
					sMaxDate = (parseInt(oStartYear) + i) + "/" + oMaxMonth
							+ "/" // 尚未加入day
				} else {
					oMaxMonth = oMaxMonth < 10 ? "0" + oMaxMonth : oMaxMonth
							.toString();
					sMaxDate = oStartYear + "/" + oMaxMonth + "/" // 尚未加入day
				}

				for (var i = parseInt(iStartDay); i > 0; i--) {// 防止出現如2008/2/31之日期
					var oDay = i < 10 ? "0" + i.toString() : i.toString();
					var sDate = sMaxDate + oDay;
					if (IsValidDate(sDate)) {
						sMaxDate = sDate;
						dMaxDate = new Date(sMaxDate);
						break;
					}
				}
			}
		}

		if (!bCanOverBaseDate && (dMaxDate == null || dMaxDate > dBaseDate)) {// 最大日不能超過基準日
			sMaxDate = sBaseDate;
			dMaxDate = dBaseDate;
		}

		if (dMaxDate != null && dEndDate > dMaxDate) {
			alert("終止日不能大於 " + sMaxDate);
			oEndDate.focus();
			return false;
		}

		if (dEndDate < dStartDate) {
			alert("終止日不能小於起始日");
			oEndDate.focus();
			return false;
		}

		return true;
	} catch (exception) {
		alert("檢核" + sFieldName + "時發生錯誤:" + exception);
		return false;
	}

	return true;
}

function CheckCDateScope(sFieldName, oBaseDate, oStartDate, oEndDate,
		bCanOverBaseDate, iMonthAgo, iMonthScope) {
	// /<summary>檢核民國日期範圍</summary>
	// /<param name="sFieldName">欄位中文描述</param>
	// /<param name="oBaseDate">基準日期，可為輸入框物件或日期物件或日期字串</param>
	// /<param name="oStartDate">起始日輸入框物件</param>
	// /<param name="oEndDate">終止日輸入框物件</param>
	// /<param name="bCanOverBaseDate">起始日或終止日可否大於基準日</param>
	// /<param name="iMonthAgo">以基準日往前推算之月份，無限制時給null</param>
	// /<param name="iMonthScope">起始日與終止日之相距月份限制，無月份限制時給null</param>
	// /<returns>true:檢核成功 false:檢核失敗</returns>

	if (typeof (sFieldName) != "string") {
		alert("CheckDateScope含有無效的參數sFieldName");
		return false;
	}

	try {
		// 基準日
		var dBaseDate;
		if (oBaseDate instanceof Date) {// 日期物件
			dBaseDate = oBaseDate;
		} else if (typeof (oBaseDate) == "string") {// 日期字串，如097/08/01
			var base = CyearToEyear(oBaseDate);
			if (!IsValidDate(base)) {
				alert("請使用正確的基準日參數，例如 097/01/31");
				return false;
			}
			dBaseDate = new Date(base);
		} else {// 輸入框物件
			var base = CyearToEyear(oBaseDate.value);
			if (!IsValidDate(base)) {
				alert("請輸入正確的基準日，例如 097/01/31");
				oBaseDate.focus();
				return false;
			}
			dBaseDate = new Date(base);
		}

		var oBaseYear = EyearToCyear(dBaseDate.getFullYear().toString());
		var oBaseMonth = dBaseDate.getMonth() + 1;
		oBaseMonth = oBaseMonth < 10 ? "0" + oBaseMonth : oBaseMonth.toString();
		var oBaseDay = dBaseDate.getDate();
		oBaseDay = oBaseDay < 10 ? "0" + oBaseDay : oBaseDay.toString();
		sBaseDate = oBaseYear + "/" + oBaseMonth + "/" + oBaseDay;

		// 最小日
		var dMinDate = null;
		var sMinDate = null;
		if (iMonthAgo != null) {
			var oMinMonth = null;
			if (oBaseMonth > iMonthAgo)
				oMinMonth = oBaseMonth - iMonthAgo - 1;
			else
				oMinMonth = -1 - (iMonthAgo - oBaseMonth);

			dMinDate = new Date(CyearToEyear(sBaseDate));
			dMinDate.setMonth(oMinMonth);
			dMinDate.setDate(1);

			oMinMonth = dMinDate.getMonth() + 1;
			oMinMonth = oMinMonth < 10 ? "0" + oMinMonth : oMinMonth.toString();

			oMinYear = EyearToCyear(dMinDate.getFullYear().toString());
			sMinDate = oMinYear + "/" + oMinMonth + "/01";
		}

		// 起始日
		var start = CyearToEyear(oStartDate.value);
		if (!IsValidDate(start)) {
			alert("請輸入正確的起始日格式，例如 097/01/31");
			oStartDate.focus();
			return false;
		}
		var dStartDate = new Date(start);
		var oStartYear = EyearToCyear(dStartDate.getFullYear().toString());
		var oStartMonth = dStartDate.getMonth() + 1;
		oStartMonth = oStartMonth < 10 ? "0" + oStartMonth : oStartMonth
				.toString();
		var oStartDay = dStartDate.getDate();
		var iStartDay = oStartDay;
		oStartDay = oStartDay < 10 ? "0" + oStartDay : oStartDay.toString();

		if (!bCanOverBaseDate && dStartDate > dBaseDate) {
			alert("起始日不能大於 " + sBaseDate);
			oStartDate.focus();
			return false;
		}

		if (dMinDate != null && dStartDate < dMinDate) {
			alert("起始日不能小於 " + sMinDate);
			oStartDate.focus();
			return false;
		}

		// 終止日
		var end = CyearToEyear(oEndDate.value);
		if (!IsValidDate(end)) {
			alert("請輸入正確的終止日格式，例如 097/01/31");
			oEndDate.focus();
			return false;
		}
		var dEndDate = new Date(end);
		var oEndYear = EyearToCyear(dEndDate.getFullYear().toString());
		var oEndMonth = dEndDate.getMonth() + 1;
		oEndMonth = oEndMonth < 10 ? "0" + oEndMonth : oEndMonth.toString();
		var oEndDay = dEndDate.getDate();
		oEndDay = oEndDay < 10 ? "0" + oEndDay : oEndDay.toString();

		// 最大日
		var dMaxDate = null;
		var sMaxDate = null;
		if (iMonthScope != null) {
			if (iMonthScope == iMonthAgo) {
				dMaxDate = dBaseDate;
				sMaxDate = sBaseDate;
			} else {
				oMaxMonth = dStartDate.getMonth() + 1 + iMonthScope;
				if (oMaxMonth > 12) {
					var i = parseInt(oMaxMonth / 12);
					oMaxMonth = oMaxMonth - 12 * i;
					if (oMaxMonth == 0) {
						oMaxMonth = 12;
						i = i - 1;
					}
					oMaxMonth = oMaxMonth < 10 ? "0" + oMaxMonth : oMaxMonth
							.toString();
					var zeros = "000";
					var max = (parseInt(oStartYear, 10) + i).toString();
					max = zeros.substring(0, 3 - max.length) + max;
					sMaxDate = max + "/" + oMaxMonth + "/" // 尚未加入day
				} else {
					oMaxMonth = oMaxMonth < 10 ? "0" + oMaxMonth : oMaxMonth
							.toString();
					sMaxDate = oStartYear + "/" + oMaxMonth + "/" // 尚未加入day
				}

				for (var i = parseInt(iStartDay); i > 0; i--) {// 防止出現如097/2/31之日期
					var oDay = i < 10 ? "0" + i.toString() : i.toString();
					var sDate = sMaxDate + oDay;
					var d = CyearToEyear(sDate);
					if (IsValidDate(d)) {
						sMaxDate = sDate;
						dMaxDate = new Date(d);
						break;
					}
				}
			}
		}

		if (!bCanOverBaseDate && (dMaxDate == null || dMaxDate > dBaseDate)) {// 最大日不能超過基準日
			sMaxDate = sBaseDate;
			dMaxDate = dBaseDate;
		}

		if (dMaxDate != null && dEndDate > dMaxDate) {
			alert("終止日不能大於 " + sMaxDate);
			oEndDate.focus();
			return false;
		}

		if (dEndDate < dStartDate) {
			alert("終止日不能小於起始日");
			oEndDate.focus();
			return false;
		}

		return true;
	} catch (exception) {
		alert("檢核" + sFieldName + "時發生錯誤:" + exception);
		return false;
	}

	return true;
}

// 檢查身份證字號及統一編號
function checkSYS_IDNO(obj) {
	if (obj.value != "") {
		if (obj.value.length == 8 || obj.value.length == 10) {
			if (obj.value.length == 8) {
				if (!TaxID(obj)) {
					alert("身分證或統一編號輸入有錯，請重新輸入。");
					obj.select();
					obj.focus();
					return false;
				} else
					return true;
			}// if(obj.value.length==8)

			if (obj.value.length == 10) {

				var reType1 = /[A-Z]{1}\d{9}/;
				var reType2 = /[A-Z]{2}\d{8}/;
				var reType3 = /\d{8}[A-Z]{2}/;

				if (reType1.test(obj.value)) {
					if (!checkID(obj)) {
						alert("身分證或統一編號輸入有錯，請重新輸入。");
						obj.select();
						obj.focus();
						return false;
					} else
						return true;
				} else if (reType2.test(obj.value)) {
					if (!checkID2(obj)) {
						alert("身分證或統一編號輸入有錯，請重新輸入。");
						obj.select();
						obj.focus();
						return false;
					} else
						return true;
				} else if (reType3.test(obj.value)) {
					return true;
				} else {
					alert("身分證或統一編號輸入有錯，請重新輸入。");
					obj.select();
					obj.focus();
					return false;
				}

			}// if(obj.value.length==10)
		} else {
			alert("身分證或統一編號輸入有錯，請重新輸入。");
			obj.select();
			obj.focus();
			return false;
		}
	}
}

function CheckLenEqual(sFieldName, oField, bCanEmpty, iEqualLen) {
	// /<summary>檢查輸入框內容字數是否符合指定字數</summary>
	// /<param name="sFieldName">欄位中文描述</param>
	// /<param name="oField">輸入框</param>
	// /<param name="bCanEmpty">可否為空字串</param>
	// /<param name="iEqualLen">必須符合的字串長度</param>
	// /<returns>true:檢核成功 false:檢核失敗</returns>

	if (typeof (sFieldName) != "string") {
		alert("CheckLenEqual含有無效的參數sFieldName");
		return false;
	}

	try {
		var sNumber = oField.value;

		if (bCanEmpty == false && sNumber == "") {
			alert("請輸入" + sFieldName);
			oField.focus();
			return false;
		}

		var iLen = sNumber.length;

		if (iLen != iEqualLen) {
			alert(sFieldName + "的內容長度應該等於" + iEqualLen);
			oField.focus();
			return false;
		}
	} catch (exception) {
		alert("檢核" + sFieldName + "時發生錯誤:" + exception);
		return false;
	}

	return true;
}

function chkChrNum(str, fieldname) {
	var slength = str.length;
	for (i = 0; i < slength; i++) {
		var chkchar = str.charAt(i);
		if (chkchar < '0' || chkchar > 'z') {
			alert(fieldname + "僅能輸入文數字");
			return false;
		} else {
			if (chkchar > '9' && chkchar < 'A') {
				alert(fieldname + "僅能輸入文數字");
				return false;
			}
			if (chkchar > 'Z' && chkchar < 'a') {
				alert(fieldname + "僅能輸入文數字");
				return false;
			}

		}
	}
	return true;
}

function CheckNotEmpty(sFieldName, oField) {
	// /<summary>檢查不可為空字串的欄位</summary>
	// /<param name="sFieldName">欄位中文描述</param>
	// /<param name="oField">輸入框</param>
	// /<returns>true:檢核成功 false:檢核失敗</returns>

	if (typeof (sFieldName) != "string") {
		alert("CheckNotEmpty含有無效的參數sFieldName");
		return false;
	}

	try {
		if (oField.value == "") {
			alert("請輸入" + sFieldName);
			oField.focus();
			return false;
		}
	} catch (exception) {
		alert("檢核" + sFieldName + "時發生錯誤:" + exception);
		return false;
	}

	return true;
}

function CheckCardNum(sFieldName, oField) {
	// /<summary>檢核信用卡號應為16位且皆為數字</summary>
	// /<param name="sFieldName">欄位中文描述</param>
	// /<param name="oField">輸入框</param>
	// /<returns>true:檢核成功 false:檢核失敗</returns>

	try {
		var sNumber = oField.value;

		var reNumber = /[^0-9]/;
		if (reNumber.test(sNumber)) {
			alert(sFieldName + "欄位只能輸入數字(0~9)");
			oField.focus();
			return false;
		}

		if (sNumber.length != 16) {
			alert(sFieldName + "欄位應為16位數字");
			oField.focus();
			return false;
		}
	} catch (exception) {
		alert("檢核" + sFieldName + "時發生錯誤:" + exception);
		return false;
	}

	return true;

}

function CheckCarId(CarKind, CarId1, CarId2) {

	if (CarKind == "C") {
		if (!((CarId1.length == 2 && CarId2.length == 4)
				|| (CarId1.length == 4 && CarId2.length == 2)
				|| (CarId1.length == 3 && CarId2.length == 2)
				|| (CarId1.length == 2 && CarId2.length == 3)
				|| (CarId1.length == 2 && CarId2.length == 2)
				|| (CarId1.length == 3 && CarId2.length == 3) || (CarId1.length == 3 && CarId2.length == 4))) {
			alert("汽車車號檢核錯誤");
			return false;
		}
	} else if (CarKind == "M") {
		if (!((CarId1.length == 2 && CarId2.length == 2)
				|| (CarId1.length == 3 && CarId2.length == 3)
				|| (CarId1.length == 3 && CarId2.length == 4) || (CarId1.length == 2 && CarId2.length == 3))) {
			alert("機車車號檢核錯誤");
			return false;
		}
	}
	return true;
}

// 中文值檢測
function isChinese(name) {
	if (name.length == 0)
		return false;
	for (i = 0; i < name.length; i++) {
		if (!(name.charCodeAt(i) < 0x4E00 || name.charCodeAt(i) > 0x9FA5)) {
			return true;
		}

	}

	return false;
}

/*
 * 有新增卡片，請將範圍值填寫於此。
 */
var ccr = new Array({
	name : "VISA白金卡",
	lowerbound : 405760000,
	upperbound : 405760999
}, {
	name : "故宮白金卡",
	lowerbound : 405760500,
	upperbound : 405760599
}, {
	name : "VISA 金卡",
	lowerbound : 490723000,
	upperbound : 490723879
}, {
	name : "嘉義認同金卡",
	lowerbound : 490723880,
	upperbound : 490723882
}, {
	name : "VISA 金卡",
	lowerbound : 490723883,
	upperbound : 490723886
}, {
	name : "英康聯名金卡",
	lowerbound : 490723887,
	upperbound : 490723889
}, {
	name : "國際百貨金卡",
	lowerbound : 490723890,
	upperbound : 490723899
}, {
	name : "故宮認同金卡",
	lowerbound : 490723900,
	upperbound : 490723906
}, {
	name : "故宮認同金卡",
	lowerbound : 490723900,
	upperbound : 490723902
}, {
	name : "VISA金卡",
	lowerbound : 490723903,
	upperbound : 490723906
}, {
	name : "遠來休閒金卡",
	lowerbound : 490723907,
	upperbound : 490723909
}, {
	name : "VISA金卡",
	lowerbound : 490723925,
	upperbound : 490723999
}, {
	name : "VISA 普卡",
	lowerbound : 493825000,
	upperbound : 493825879
}, {
	name : "嘉義認同普卡",
	lowerbound : 493825880,
	upperbound : 493825882
}, {
	name : "VISA 普卡",
	lowerbound : 493825883,
	upperbound : 493825886
}, {
	name : "英康聯名卡",
	lowerbound : 493825887,
	upperbound : 493825889
}, {
	name : "國際百貨卡",
	lowerbound : 493825890,
	upperbound : 493825899
}, {
	name : "故宮認同卡",
	lowerbound : 493825900,
	upperbound : 493825906
}, {
	name : "故宮認同卡",
	lowerbound : 493825900,
	upperbound : 493825902
}, {
	name : "VISA 普卡",
	lowerbound : 493825903,
	upperbound : 493825906
}, {
	name : "花蓮海洋公園卡",
	lowerbound : 493825907,
	upperbound : 493825909
}, {
	name : "帶就富卡",
	lowerbound : 493825910,
	upperbound : 493825912
}, {
	name : "VISA 普卡",
	lowerbound : 493825925,
	upperbound : 493825999
}, {
	name : "COMBO普卡",
	lowerbound : 512296000,
	upperbound : 512296999
}, {
	name : "COMBO金卡",
	lowerbound : 512400000,
	upperbound : 512400999
}, {
	name : "COMBO白金卡",
	lowerbound : 514952000,
	upperbound : 514952999
}, {
	name : "MASTER 金卡",
	lowerbound : 540974000,
	upperbound : 540974879
}, {
	name : "MASTER 金卡",
	lowerbound : 540974883,
	upperbound : 540974886
}, {
	name : "森林卡金卡",
	lowerbound : 540974890,
	upperbound : 540974899
}, {
	name : "MASTER 金卡",
	lowerbound : 540974903,
	upperbound : 540974906
}, {
	name : "MASTER 金卡",
	lowerbound : 540974910,
	upperbound : 540974999
}, {
	name : "MASTER 普卡",
	lowerbound : 543770000,
	upperbound : 543770879
}, {
	name : "屏東師院卡",
	lowerbound : 543770880,
	upperbound : 543770880
}, {
	name : "MASTER 普卡",
	lowerbound : 543770884,
	upperbound : 543770886
}, {
	name : "森林卡普卡",
	lowerbound : 543770890,
	upperbound : 543770899
}, {
	name : "MASTER 普卡",
	lowerbound : 543770903,
	upperbound : 543770906
}, {
	name : "MASTER 普卡",
	lowerbound : 543770910,
	upperbound : 543770999
}, {
	name : "MASTER 鈦金商旅卡",
	lowerbound : 558866000,
	upperbound : 558866999
}, {
	name : "MASTER 悠遊鈦金卡",
	lowerbound : 524263000,
	upperbound : 524263009
}, {
	name : "MASTER 悠遊普通卡",
	lowerbound : 542367000,
	upperbound : 542367009
}, {
	name : "MASTER 悠遊金融卡",
	lowerbound : 53336000,
	upperbound : 53336019
}, {
	name : "VISA 悠遊白金卡",
	lowerbound : 405760600,
	upperbound : 405760602
}, {
	name : "故宮之友VISA 悠遊金卡",
	lowerbound : 490723910,
	upperbound : 490723912
}, {
	name : "故宮之友VISA 悠遊普通卡",
	lowerbound : 4938259160,
	upperbound : 4938259168
}, {
	name : "故宮之友VISA御璽卡手機",
	lowerbound : 4712353000,
	upperbound : 4712353199
}, {
	name : "故宮之友VISA御璽悠遊卡",
	lowerbound : 4712350000,
	upperbound : 4712352999
}, {
	name : "宜蘭大學認同卡普卡",
	lowerbound : 5423673990,
	upperbound : 5423674089
}, {
	name : "宜蘭大學認同卡鈦金卡",
	lowerbound : 5242634990,
	upperbound : 5242635089
}, {
	name : "青溪之友認同卡普卡",
	lowerbound : 5423676990,
	upperbound : 5423677089
}, {
	name : "青溪之友認同卡鈦金卡",
	lowerbound : 5242636990,
	upperbound : 5242637089
}, {
	name : "永續生活一卡通MASTER普卡",
	lowerbound : 5423677090,
	upperbound : 5423677299
}, {
	name : "永續生活一卡通VISA普卡",
	lowerbound : 4938259190,
	upperbound : 4938259399
}, {
	name : "永續生活一卡通MASTER鈦金卡",
	lowerbound : 5242637090,
	upperbound : 5242637399
}, {
	name : "永續生活一卡通VISA白金卡",
	lowerbound : 4057606030,
	upperbound : 4057606299
}, {
	name : "永續生活一卡通VISA御璽卡",
	lowerbound : 4712353200,
	upperbound : 4712353499
}, {
	name : "VISA DEBIT 即時卡",
	lowerbound : 53336020,
	upperbound : 53336079
}, {
	name : "北港朝天宮媽祖認同卡普卡(一卡通)",
	lowerbound : 54236773,
	upperbound : 54236774
}, {
	name : "北港朝天宮媽祖認同卡鈦金卡(一卡通)",
	lowerbound : 52426375,
	upperbound : 52426376
}, {
	name : "北港朝天宮媽祖認同卡普卡(悠遊卡)",
	lowerbound : 54236775,
	upperbound : 54236776
}, {
	name : "北港朝天宮媽祖認同卡鈦金卡(悠遊卡)",
	lowerbound : 52426377,
	upperbound : 52426378
}, {
	name : "銀色之愛信用卡鈦金商旅卡(一卡通)",
	lowerbound : 558866500,
	upperbound : 558866509
}, {
	name : "銀色之愛信用卡鈦金商旅卡(悠遊卡)",
	lowerbound : 558866510,
	upperbound : 558866519
});

// 判斷信用卡是否為正卡
function checkMajorCard(cardno) {
	if (cardno.substr(cardno.length - 3, 1) != '1') {
		alert("限本行信用卡正卡客戶申請(商務卡、採購卡、附卡除外)!");
		return false;
	}
	return true;
}

// 檢查是否為台企銀所發之信用卡
function checkTBBCard(cardno) {

	// var tmp = parseInt(cardno.substr(0,9), 10);
	var flg = false;

	for (var i = 0; i < ccr.length; i++) {
		var cardrange = ccr[i].lowerbound;
		var cardlow = cardrange.toString();
		cardrange = ccr[i].upperbound;
		var cardtop = cardrange.toString();
		var tmp = parseInt(cardno.substr(0, cardlow.length), 10);
		if (parseInt(cardlow, 10) <= tmp && tmp <= parseInt(cardtop, 10)) {
			flg = true;
			break;
		}
	}
	if (!flg) {
		alert("請使用一般信用卡正卡申請(不含商務卡、採購卡、附卡及VISA金融卡)!");
	}
	return flg;
}

/*
 * 使用者名稱Supervisor 1. 6-16碼之英數字 2. 至少兩位英文字不限位子 3. 不可為特殊符號 4.
 * 數字部份不得全部與身分證統一編號、出生年月日(公司設立日期)、電話號碼、簽入密碼及交易密碼完全相同(不檢核) 5.
 * 數字不得連號或完全重號(僅檢核英文字重號) 6. 連續字串(僅限英文) 7. 大小寫區分
 */

function checkUserName(iText) {
	var min = 6;
	var max = 16;
	if (!checkLength(iText, min, max)) {
		alert('請輸入' + min + '-' + max + '碼之使用者名稱');
		return false;
	}
	if (!check2EnChar(iText)) {
		alert('請輸入至少兩位英文字之使用者名稱');
		return false;
	}
	if (checkIllegalChar(iText)) {
		alert('使用者名稱不可含有特殊符號');
		return false;
	}
	if (checkDuplicateChar(iText)) {
		alert('使用者名稱不可完全重號');
		return false;
	}
	if (checkContinuousENChar(iText)) {
		alert('使用者名稱不可為連續字串');
		return false;
	}
	return true;
}

/*
 * 6-16碼之英數字 true :介於6-16碼 false:不介於6-16碼
 */
function checkLength(iText, min, max) {
	var result = true;
	if (iText.length < parseInt(min, 10))
		result = false;
	if (iText.length > parseInt(max, 10))
		result = false;
	return result;

}

/*
 * 至少兩位英文字不限位子 false:沒有至少兩位英文字 true :有至少兩位英文字
 */
function check2EnChar(iText) {
	var result = false;
	var pattern = /.*[a-zA-Z]{1}.*[a-zA-Z]{1}/;
	result = pattern.test(iText);
	return result;
}

/*
 * 檢查不合法字元 false:沒有不合法字眼 true :有不合法字眼
 */
function checkIllegalChar(iText) {
	var result = false;
	var legal = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890_#';
	var i = 0
	while (i < iText.length) {
		var c = iText.substr(i, 1);
		if (legal.indexOf(c) < 0) {
			result = true;
			break;
		}
		i++;
	}
	return result;
}
/* 檢查英數字重號
 * false:不重號
 * true :重號	*/
function checkDuplicateChar(iText){
	var result = true;
	var firstchar = iText.substr(0, 1);
	var i=0
	while(i < iText.length) {
		var c = iText.substr(i, 1);
		if(firstchar != c) {
			result = false;
			break;
		}
		i++;
	}
	return result;
}
/*
 * 連續字串(僅限英文) false:非連續字串 true :連續字串
 */
function checkContinuousENChar(iText) {
	var result = true;
	var firstchar = iText.substr(0, 1);
	var CChar = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
	var CCharArray = new Array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
			'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
			'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
			'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
			'w', 'x', 'y', 'z');
	var i = 1
	while (i < iText.length) {
		var c = iText.substr(i, 1);
		if (CCharArray[CChar.indexOf(firstchar) + i] != c) {
			result = false;
			break;
		}
		i++;
	}
	return result;
}

function chkSameEngOrNum(obj, field_name) {
	// <summary>檢核輸入值是否為相同之文數字</summary>
	// <param name="obj">畫面上要檢核的欄位(目前只用在密碼檢核)</param>
	// <param name="field_name">檢核不過要show的欄位及訊息</param>
	var str = obj.value;
	if (str != '') {
		var iCode1 = str.charCodeAt(0);

		if ((iCode1 >= 48 && iCode1 <= 57) || (iCode1 >= 65 && iCode1 <= 90)
				|| (iCode1 >= 97 && iCode1 <= 122)) {
			var iCode2 = 0;
			var bSame = true;
			for (var i = 0; i < str.length; i++) {
				iCode2 = str.charCodeAt(i);
				if (iCode1 != iCode2) {
					bSame = false;
				}
			}
		}
		if (bSame) {
			return (doAlert(obj, field_name));
		}
	}
	return true;
}

function chkSerialNum(obj, field_name) {
	// <summary>檢核輸入值是否為連續數字</summary>
	// <param name="obj">畫面上要檢核的欄位(目前只用在密碼檢核)</param>
	// <param name="field_name">檢核不過要show的欄位及訊息</param>
	var str = obj.value;
	if (str != '') {
		var iCode1 = str.charCodeAt(0);
		if (iCode1 >= 48 && iCode1 <= 57) {
			var iCode2 = 0;
			var bPlus = true;
			var bMinus = true;
			for (var i = 1; i < str.length; i++) {
				iCode2 = str.charCodeAt(i);// now
				if (iCode2 != (iCode1 + 1)) {
					if (!(iCode2 == 48 && iCode1 == 57)) {
						bPlus = false;
					}
				}
				if (iCode2 != (iCode1 - 1)) {
					if (!(iCode2 == 57 && iCode1 == 48)) {
						bMinus = false;
					}
				}
				iCode1 = iCode2;// before
			}
			if (bPlus || bMinus)
				return (doAlert(obj, field_name));
		}
	}
	return true;
}