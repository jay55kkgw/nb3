var bannerCssTextL = ""; // banner大廣告css動態呈現
var bannerCssTextM = ""; // banner中廣告css動態呈現
var bannerCssTextS = ""; // banner小廣告css動態呈現

/**
 * 登入頁Banner廣告顯示JS
 */
function showBanner(bannerImgs, ctx, adsudt) {
	console.log("login.bannerImgs: " + bannerImgs);

	if(bannerImgs>0){
		for (var i = 1; i <= bannerImgs; i++) {
			textBanner(i, ctx, adsudt);
			$("#bg-for-banner").text(
				/* 大圖 */
				"@media screen and (min-width: 992px) {" + bannerCssTextL + "}" +
				/* 中圖 */
				"@media screen and (min-width: 767px) and (max-width: 992px) {" + bannerCssTextM + "}" +
				/* 小圖 */
				"@media screen and (max-width: 767px) {" + bannerCssTextS + "}"
			);
		}
		
	} else {
		textBanner(0, ctx, adsudt);
		$("#bg-for-banner").text(
			/* 大圖 */
			"@media screen and (min-width: 992px) {" + bannerCssTextL + "}" +
			/* 中圖 */
			"@media screen and (min-width: 767px) and (max-width: 992px) {" + bannerCssTextM + "}" +
			/* 小圖 */
			"@media screen and (max-width: 767px) {" + bannerCssTextS + "}"
		);
	}
	
	console.log("login.showBanner.Finish!");
}

/**
 * 登入頁Banner廣告顯示css動態組成
 */
function textBanner(index, ctx, adsudt) {
	// 預設圖檔跟落地圖檔不同目錄
	var directory = "banner/bs";
	var i = index;
	
	if(index==0){
		directory = "default"
		i = index+1;
	}
	
	bannerCssTextL = bannerCssTextL +
		".carousel-inner .carousel-item:nth-child(" + i + ") {" +
			"background: url('" + ctx + "/login/" + directory + "/banner-lg-" + index + ".png?bdt=" + adsudt + "');" +
			"background-repeat: no-repeat;" +
			"background-size: cover;" +
		"}";
		
	bannerCssTextM = bannerCssTextM +
		".carousel-inner .carousel-item:nth-child(" + i + ") { " +
			"background: url('" + ctx + "/login/" + directory + "/banner-md-" + index + ".png');" + 
			"background-repeat: no-repeat;" +
			"background-size: cover;" +
		"}";
		
	 bannerCssTextS = bannerCssTextS + 	
		".carousel-inner .carousel-item:nth-child(" + i + ") {" +
			"background: url('" + ctx + "/login/" + directory + "/banner-sm-" + index + ".png');" +
			"background-repeat: no-repeat;" +
			"background-size: cover;" +
		"}";
		
}

/**
 * 判斷裝置
 */
function checkBrowser() {
	var checkBrowserResult = false;
	if (navigator.userAgent.match(/Android/i)
			|| navigator.userAgent.match(/webOS/i)
			|| navigator.userAgent.match(/iPhone/i)
			|| navigator.userAgent.match(/iPad/i)
			|| navigator.userAgent.match(/iPod/i)
			|| navigator.userAgent.match(/BlackBerry/i)
			|| navigator.userAgent.match(/Windows Phone/i)) {
		// 行動裝置
		console.log("component checkBrowser not pass!!!");
	} else {
		// 可使用元件之裝置
		checkBrowserResult = true;
		console.log("component checkBrowser pass...");
	}

	console.log("checkBrowser: " + checkBrowserResult);

	return checkBrowserResult;
}


//-------------------------------------------------------------------------------------

/**
 * 動態鍵盤
 */
function dynamicKeyboard() {
	// 驗證裝置
	if(checkBrowser()){
		// 電腦
		$('.keyboard').keyboard();

//		$("#clearButton").click(function(){
//			$("#webpw").val("");
//			$("#webpw").focus();
//		});
//		$(".keyboardButtons li").mouseover(function(){
//			$("#webpw").focus();
//		});
		
		$("#openKeyboardBtn").click(function() {
			$('#keyboardBlock').show();
		});
		$("#closeKeyboardBtn").click(function(){
			$('#keyboardBlock').hide();
		});
	} else {
		// 隱藏動態鍵盤按鈕
		$('#openKeyboardBtn').hide();
		$('#keyboardBlock').hide();
	}
	
}

// 紀錄上次focus的欄位讓動態鍵盤不指定浮標也可以輸入
var k_focus = null;
function KeyBoardF() {
	$( "#cusidn" ).focus(function() {
		k_focus = $( "#cusidn" );
		});
	$( "#userName" ).focus(function() {
		k_focus = $( "#userName" );
		});
	$( "#webpw" ).focus(function() {
		k_focus = $( "#webpw" );
		});
	$( "#capCode" ).focus(function() {
		k_focus = $( "#capCode" );
		});
}

// 初始化BlockUI
function initBlockUI() {
	initBlockId = blockUI();
}


//-------------------------------------------------------------------------------------

function getB(i){
	var getBuri = uriCtx + "/banner_aj";
	console.log("banner_uri: " + getBuri);
	rdata = {
		num : i
	};
	fstop.getServerDataEx(getBuri, rdata, true, getBanner);
}

// 取得公告訊息
function getBulletin(){
	var getBulletinUri = uriCtx + "/bulletin_aj";
	console.log("bulletin_uri: " + getBulletinUri);
	fstop.getServerDataEx(getBulletinUri, null, true, showBulletin);
}

// 顯示公告訊息
function showBulletin(data){
	if (data && data.result) {
		var implength = 0;
		var genlength = 0;
		for (i = 0; i < data.data.length; i++) {
			var id = data.data[i].id; // 訊息類型
			var type = data.data[i].type; // 訊息類型
			var title = data.data[i].title; // 訊息標題
			var contenttype = data.data[i].contenttype; // 訊息內容類型
			
			var urlLink = data.data[i].url; // 訊息內容類型--超連結
			var content = data.data[i].content; // 訊息內容類型--文字
			var srccontent = data.data[i].srccontent; // 訊息內容類型--pdf
			
			// 訊息類型--1:重要、0:一般、其他:已讀
			console.log("data.data["+i+"].type: " + data.data[i].type);
			//變更換行符號
			if(content != null){
				content=content.replace(/\n\r/g,"<br/>");
				content=content.replace(/\r\n/g,"<br/>");
				content=content.replace(/\n/g,"<br/>");
				content=content.replace(/\r/g,"<br/>");
			}
			// 訊息類型--1:重要
			if (type == '1') {
				implength++;
				// 訊息公告內容為超連結
				if (contenttype == '1') {
					$('[id^=IMP]').append(
						'<li>'
						+ '<a href="' + urlLink + '" target="_blank">' + title + '</a>'
						+ '</li>'
					);
					
				} // 訊息公告內容為pdf
				else if (contenttype == '2') {
					var uri = uriCtx + "/getAnn/"+id;
					
					$('[id^=IMP]').append(
						'<li>'
						+ '<a href="' + uri + '" target="_blank">' + title + '</a>'
						+ title
						+ '</li>'
					);
					
				} // 訊息公告內容為文字訊息
				else if (contenttype == '3') {
					$('[id^=IMP]').append(
						'<li onclick="' + "$('#text-info').html(" + "'" + content + "'" + '); ' + "$('#text-block').show(); " + '">'
						+ title
						+ '</li>'
					);
				}
				
			} // 訊息類型--0:一般
			else if(type == '0'){
				genlength++;
				// 訊息公告內容為超連結
				if (contenttype == '1') {
					$('[id^=GEN]').append(
						'<li>'
						+ '<a href="' + urlLink + '" target="_blank">' + title + '</a>'
						+ '</li>'
					);
					
				} // 訊息公告內容為pdf
				else if (contenttype == '2') {
					var uri = uriCtx + "/getAnn/"+id;
					
					$('[id^=GEN]').append(
						'<li>'
						+ '<a href="' + uri + '" target="_blank">' + title + '</a>'
						+ title
						+ '</li>'
					);
					
				} // 訊息公告內容為文字訊息
				else if (contenttype == '3') {
					$('[id^=GEN]').append(
						'<li onclick="' + "$('#text-info').html(" + "'" + content + "'" + '); ' + "$('#text-block').show(); " + '">'
						+ title
						+ '</li>'
					);
				}
			}
		}
		if(implength <= 1){
			$("div[name='impl']").addClass("one-ticker");
		}
		else{
			$("div[name='impl']").removeClass("one-ticker");
		}
		if(genlength <= 1){
			$("div[name='genl']").addClass("one-ticker");
		}
		else{
			$("div[name='genl']").removeClass("one-ticker");
		}
		console.log("showBulletin...Finish!!!");
		
	} else {
		console.log("Oops");
	}
}

function getBanner(data){
	console.log("GET DATA>>>>>"+JSON.stringify(data));
	if (data && data.result) {
		if(data.data.targettype == "1"){
			console.log(">>>>>>1");
			if(data.data.url == "" || data.data.url == " "){
				return false;
			}
			else{
				$("#tt").attr("href", data.data.url);
				document.querySelector('#tt').click();
			}
		}
		else if(data.data.targettype == "2"){
			console.log(">>>>>>2");
			var bid = data.data.id;
			$("#tt").attr("href", uriCtx + '/getAds/'+bid);
			document.querySelector('#tt').click();
		}
		else if(data.data.targettype == "3"){
			console.log(">>>>>>3");
			$('#text-info').html(data.data.showText);
			$('#text-block').show();
		}
	}
}

function getTxt(){
	uri = uriCtx+"/txt_aj";
	console.log("banner_uri: " + uri);
	rdata = {
		num : bannerImgs
	};
	fstop.getServerDataEx(uri, rdata, true, getTxtB);
}

function getTxtB(data){
	var v = 0;
	if(data.data != null){
		for(var i = 1; i <= bannerImgs; i++){
			var tit = data.data.ADST[v].TITLE;
			console.log("get title>>>"+tit);
			$("#adsT"+i).prop('title', tit);
			v++;
		}
	}
}