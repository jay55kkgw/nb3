ikeyTopWebSocketUtil.setWssUrl("wss://localhost:9005/");

function DoUpdate(newVersion,outerCallBackFunction){
	var nowDate = new Date();
	if(window.console){console.log("DoUpdate Time is " + nowDate.getHours() + "時" + nowDate.getMinutes() + "分" + nowDate.getSeconds() + "秒" + nowDate.getMilliseconds() + "毫秒");}
	var rpcName = "get_Version";
	var result = "";
	if(window.console){console.log("rpcName="+rpcName);}
	ikeyTopWebSocketUtil.invokeRpcDispatcher(get_Version_Callback,rpcName);

	function get_Version_Callback(rpcStatus,rpcReturn){
		try{
			ikeyTopWebSocketUtil.tryRpcStatus(rpcStatus);
			
			var currentVersion = rpcReturn;
			if(window.console){console.log("currentVersion="+currentVersion);}
			
			//未安裝IKEY元件
			if(currentVersion == "E_Send_11_OnError_1006"){
				result = "系統使用的【IKey元件】尚未安裝";
			}
			//已安裝
			else{
				if(window.console){console.log("newVersion="+newVersion);}
				
				var hasUpdates = CheckUpdates(currentVersion,newVersion);
				if(window.console){console.log("hasUpdates="+hasUpdates);}
				
				if(hasUpdates == true){
					result = "系統使用的【IKey元件】有最新版本";
				}
			}
		}
		catch(exception){
			result = exception;
		}
		var fullCallBack = outerCallBackFunction + "('" + result + "')";
		
		eval(fullCallBack);
    }
}
function UISignForPKCS7(data,signElement,outerCallBackFunction){
	//簽章內Hash訊息使用的演算法，值 0:MD5,1:SHA-1,2:SHA-256,其他值:SHA-1
	var hashAlgOption = 1;
	var result = "";
	
	try{
		var rpcName = "UISignForPKCS7";
		
		ikeyTopWebSocketUtil.invokeRpcDispatcher(UISignForPKCS7_Callback,rpcName,data,hashAlgOption);
	}
	catch(exception){
		if(window.console){console.log("exception="+exception);}
		result = false;
		
		var fullCallBack = outerCallBackFunction + "('" + result + "')";
		
		eval(fullCallBack);
	}
	
	function UISignForPKCS7_Callback(rpcStatus,rpcReturn){
		try{
			ikeyTopWebSocketUtil.tryRpcStatus(rpcStatus);
			
			var pkcs7Sign = rpcReturn;
			if(window.console){console.log("pkcs7Sign="+pkcs7Sign);}
			
			//不為空值
			if(pkcs7Sign != ""){
				//沒有安裝IKEY元件
				if(pkcs7Sign == "E_Send_11_OnError_1006"){
					alert("尚未安裝IKEY元件");
					
					result = false;
					
					var fullCallBack = outerCallBackFunction + "('" + result + "')";
					
					eval(fullCallBack);
				}
				//成功，其值就是PKCS7簽章
				else{
					signElement.value = pkcs7Sign;
					
					result = true;
					
					var fullCallBack = outerCallBackFunction + "('" + result + "')";
					
					eval(fullCallBack);
				}
			}
			//pkcs7Sign若為空字串，使用ErrorDescription取得錯誤訊息
			else{
				var rpcName = "get_ErrorDescription";
				
				ikeyTopWebSocketUtil.invokeRpcDispatcher(get_ErrorDescription_Callback,rpcName);
			}
		}
		catch(exception){
			if(window.console){console.log("exception="+exception);}
			result = false;

			var fullCallBack = outerCallBackFunction + "('" + result + "')";
			
			eval(fullCallBack);
		}
	}
	function get_ErrorDescription_Callback(rpcStatus,rpcReturn){
		result = false;
		
		try{
			ikeyTopWebSocketUtil.tryRpcStatus(rpcStatus);
			
			var errorDescription = rpcReturn;
			if(window.console){console.log("errorDescription="+errorDescription);}
			
			alert(errorDescription);
		}
		catch(exception){
			if(window.console){console.log("exception="+exception);}
		}
		var fullCallBack = outerCallBackFunction + "('" + result + "')";
		
		eval(fullCallBack);
	}	
}
function CheckUpdates(currentVersion,newVersion){
	var currentVersionWeighting = parseInt(currentVersion.replace(/\./g,""));
	if(window.console){console.log("currentVersionWeighting="+currentVersionWeighting);}
	
	var newVersionWeighting = parseInt(newVersion.replace(/\./g,""));
	if(window.console){console.log("newVersionWeighting="+newVersionWeighting);}
	
	//已安裝的版本較舊
	if(currentVersionWeighting < newVersionWeighting){
		return true;
	}
	//已安裝的版本比較新或一樣，不必更新
	else{
		return false;
	}
}