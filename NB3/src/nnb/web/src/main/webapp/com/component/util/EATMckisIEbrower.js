﻿//網頁一進來就會直接抓indexOf去判斷是哪一種瀏覽器
ckBrowser2 = function()
{
	var sAgent = navigator.userAgent.toLowerCase();
	//IE為預設值，若是其他瀏覽器為true會把上述給override掉，所以不會有判定問題
	this.isIE = (sAgent.indexOf("msie")!=-1 || sAgent.indexOf("trident")!=-1); //IE6.0-7
	
}
/*
function loadeatm()
{
	var oBrow2 = new ckBrowser2();
	
	if(oBrow2.isIE){
			var strObject = "";
    		// 使用 Script 判斷 win64 或 win32, 執行時間有點久
			if (navigator.platform.toLowerCase() == "win64"){
				//"Windows 64 位元";
				//strObject = "<OBJECT id='myobj' classid='CLSID:100C2AC9-3810-4EB8-9272-11D509309254' CODEBASE='TopICCardATL.cab#version=-1,-1,-1,-1' style='display:none'></OBJECT>";
				strObject = "<object id='myobj' classid='CLSID:83652FA4-A9BA-479B-8955-3A07C337AB55' codebase='TbbWebAtmApiX64.cab#version=-1,-1,-1,-1' style='display:none'></object>";
				
			} else {
				// "Windows 32 位元 或 模擬 64 位元(WOW64)";
				strObject = "<OBJECT id='myobj' classid='CLSID:100C2AC9-3810-4EB8-9272-11D509309254' CODEBASE='TopICCardATL.cab#version=-1,-1,-1,-1' style='display:none'></OBJECT>";
			}
			document.write(strObject);			
	}
			
}
*/
function Initialize(outerCallBackFunction)
{
	var oBrow2 = new ckBrowser2();
	var cardname="";
	var cardname1="";
	var status="";
	var status1="";
	if(!oBrow2.isIE){
		if((navigator.userAgent.indexOf("Chrome") > -1) || (navigator.userAgent.indexOf("Firefox") > -1) || (navigator.userAgent.indexOf("Safari") > -1)) {
			var rpcName = "Initialize";
			var rpcParam1 = window.location.href;

			topWebSocketUtil.invokeRpcDispatcher(Initialize_Callback,rpcName,window.location.href);
	
			//--------------------------------------------------------------------------
			// Initialize_Callback
			function Initialize_Callback(rpcStatus, rpcReturn)
			{
				try {
					//------------------------------------------------------------------
					topWebSocketUtil.tryRpcStatus(rpcStatus);
					//------------------------------------------------------------------
					var readerData = rpcReturn.split('|');
			
					status=readerData[0];
					//alert("cardname:"+readerData[1]);
					if(status != "E000")
					{
						//cardname="FAIL"
						//alert("Initialize Error = " + status);
					}
					else
					{
						document.getElementById('CARDNAME').value = readerData[1];
						//alert("initialize status:"+status+" cardname:"+readerData[1]);						
					}
				} catch (exception) {
					alert("exception: " + exception);
				}
				var fullCallBack = outerCallBackFunction + "('" + status + "')";
				eval(fullCallBack);				
			}		
		}
	}
	else 
	{	
		var ff;
		ff = myobj.Initialize("4df092b87bd35e6f0cd940164479b6a63b896810");
		var readerData = ff.split('|');
		//document.getElementById('CARDNAME').value = readerData[1];
		status=readerData[0];
		if(status=="E000")
		{
			//alert(readerData[1]);
			cardname=(readerData[1]).split('~');
			for(var cid = 0; cid < cardname.length; cid++)
			{
				//alert("cardname:"+cardname[cid]);
				status1=DetectCard("",cardname[cid]);
				if(status1=="E000")
				{
					cardname1=cardname[cid];
					document.getElementById('CARDNAME').value = cardname1;
					//alert(cardname1); 
					break;	
				}
			}			
		}
		if(status!="E000")
		{	
			//cardname="FAIL";
			//alert("Initialize Error = " +status);
			
		}
		else
		{
			if(status1=="E000" && cardname1.length>0)
			{
				document.getElementById('CARDNAME').value = cardname1;
			}
			else
			{
				status=status1;  //偵測不到正確的讀卡機	
			}
			//alert("initialize status:"+status+" cardname:"+readerData[1]);
    	}
		return status;
    }
}

function VerifyPin(outerCallBackFunction)
{
	console.log("EATM.VerifyPin...");
	var oBrow2 = new ckBrowser2();
	var status="";
	var	readerName = document.getElementById('CARDNAME').value;
	var	pinCode = document.getElementById('OLDPIN').value;
	
	console.log("readerName: " + readerName);
	console.log("pinCode: " + pinCode);
	
	if(!oBrow2.isIE){
		console.log("is not IE");
		if((navigator.userAgent.indexOf("Chrome") > -1) || (navigator.userAgent.indexOf("Firefox") > -1) || (navigator.userAgent.indexOf("Safari") > -1)) {

			//--------------------------------------------------------------------------
			// call VerifyPin
			var rpcName = "VerifyPin";
			var rpcParam1 = readerName.slice(0, readerName.indexOf('~'));
			var rpcParam2 = pinCode;
			
			console.log("rpcParam1: " + rpcParam1);
			console.log("rpcParam2: " + rpcParam2);

			topWebSocketUtil.invokeRpcDispatcher(VerifyPin_Callback,rpcName,readerName,pinCode);

			//--------------------------------------------------------------------------
			// Name: VerifyPin_Callback
			function VerifyPin_Callback(rpcStatus, rpcReturn)
			{
				try {
					//------------------------------------------------------------------
					topWebSocketUtil.tryRpcStatus(rpcStatus);
					//------------------------------------------------------------------
					status=rpcReturn;
				}catch (exception) {
					alert("exception: " + exception);
				}
				var fullCallBack = outerCallBackFunction + "('" + status + "')";
				eval(fullCallBack);								
			}		
		}
	}
	else
	{
		console.log("is IE");
		status = myobj.VerifyPin(readerName, pinCode);
		//alert("PIN verify:"+status);
		return status;
	}
	console.log("VerifyPin.status: " + status);
}

function ChangePin(outerCallBackFunction)
{
	var oBrow2 = new ckBrowser2();
	var status="";
	var readerName = document.getElementById('CARDNAME').value;
	var pinCode = document.getElementById('OLDPIN').value;
	var newPinCode = document.getElementById('NEWPIN').value;
	
	if(!oBrow2.isIE){
		if((navigator.userAgent.indexOf("Chrome") > -1) || (navigator.userAgent.indexOf("Firefox") > -1) || (navigator.userAgent.indexOf("Safari") > -1)) {

			//--------------------------------------------------------------------------
			// call ChangePin
			var rpcName = "ChangePin";
			var rpcParam1 = readerName;
			var rpcParam2 = pinCode;
			var rpcParam3 = newPinCode;

			topWebSocketUtil.invokeRpcDispatcher(ChangePin_Callback,rpcName,readerName,pinCode,newPinCode);

			//--------------------------------------------------------------------------
			// Name: ChangePin_Callback
			function ChangePin_Callback(rpcStatus, rpcReturn)
			{
				try {
					//------------------------------------------------------------------
					topWebSocketUtil.tryRpcStatus(rpcStatus);
					//------------------------------------------------------------------
					status=rpcReturn;
				} catch (exception) {
					alert("exception: " + exception);
				}
				var fullCallBack = outerCallBackFunction + "('" + status + "')";
				eval(fullCallBack);								
			}		
		}
	}
	else 
	{
		status = myobj.ChangePin(readerName, pinCode, newPinCode);
		//alert("ChangePin status:"+status);
		return status;
	}
}

function GetMainAccount(outerCallBackFunction)
{
	var oBrow2 = new ckBrowser2();
	var status="";
	var readerName = document.getElementById('CARDNAME').value;
	if(!oBrow2.isIE){
		if((navigator.userAgent.indexOf("Chrome") > -1) || (navigator.userAgent.indexOf("Firefox") > -1) || (navigator.userAgent.indexOf("Safari") > -1)) {

			//--------------------------------------------------------------------------
			// call 'GetMainAccount'
			var rpcName = "GetMainAccount";
			var rpcParam1 = readerName;

			topWebSocketUtil.invokeRpcDispatcher(GetMainAccount_Callback,rpcName,readerName);

			//--------------------------------------------------------------------------
			// Name: GetMainAccount_Callback
			function GetMainAccount_Callback(rpcStatus, rpcReturn)
			{
				try {
					//------------------------------------------------------------------
					topWebSocketUtil.tryRpcStatus(rpcStatus);
					//------------------------------------------------------------------
					var AccountData = rpcReturn.split('|');
					status=AccountData[0];
					if (status!= "E000")
					{
						//alert("GetMainAccount Error = " + AccountData[0]);
					}
					else
					{
						// alert("Account = " + AccountData[1]);
						document.getElementById('ACNNO').value = AccountData[1];
					}	        
				} catch (exception) {
					alert("exception: " + exception);
				}
				var fullCallBack = outerCallBackFunction + "('" + status + "')";
				eval(fullCallBack);								
			}		
		}
	}
	else 
	{
		var ff;	
		ff = myobj.GetMainAccount(readerName);
		var AccountData = ff.split('|');
		status=AccountData[0];
		if(status!= "E000")
		{	
			//alert("GetMainAccount Error = " + AccountData[0]);
		}
		else
		{
			document.getElementById('ACNNO').value = AccountData[1];
			//alert("GetMainAccount:"+AccountData[1]);
		}
		//alert("GetMainAccount status:"+status);
		return status;
	}
}
function GetUnitCode(outerCallBackFunction)
{
	var oBrow2 = new ckBrowser2();
	var status="";
	var readerName=document.getElementById('CARDNAME').value;
	if(!oBrow2.isIE){
		if((navigator.userAgent.indexOf("Chrome") > -1) || (navigator.userAgent.indexOf("Firefox") > -1) || (navigator.userAgent.indexOf("Safari") > -1)) {
			//--------------------------------------------------------------------------
			// call 'GetUnitCode'
			var rpcName = "GetUnitCode";
			var rpcParam1 = readerName;

			topWebSocketUtil.invokeRpcDispatcher(GetUnitCode_Callback,rpcName,readerName);

			//--------------------------------------------------------------------------
			// Name: GetUnitCode_Callback
			function GetUnitCode_Callback(rpcStatus, rpcReturn)
			{
				try {
					//------------------------------------------------------------------
					topWebSocketUtil.tryRpcStatus(rpcStatus);
					//------------------------------------------------------------------
					var AccountData = rpcReturn.split('|');
					status=AccountData[0];
					if (status!= "E000")
					{
						//alert("GetUnitCode Error = " + AccountData[0]);
					}
					else
					{
						document.getElementById('BANKID').value = AccountData[1].substring(0,3);
						// alert("UnitCode = " + AccountData[1]);
					}
				} catch (exception) {
					alert("exception: " + exception);
				}
				var fullCallBack = outerCallBackFunction + "('" + status + "')";
				eval(fullCallBack);								
			}		
		}
	}
	else
	{
		var ff;
		ff = myobj.GetUnitCode(readerName);
		var AccountData = ff.split('|');
		status=AccountData[0];
		if (status!= "E000")
		{	
			//alert("GetUnitCode Error = " + AccountData[0]);
		}
		else
		{    	
			document.getElementById('BANKID').value = AccountData[1].substring(0,3);
			//alert("GetUnitCode:"+document.getElementById('BANKID').value);
		}
		//alert("GetUnitCode status:"+status);
		return status
	}	
}
function Finalize()
{
	var oBrow2 = new ckBrowser2();
	var status="";
	var readerName=document.getElementById('CARDNAME').value;
	if(!oBrow2.isIE){
		if((navigator.userAgent.indexOf("Chrome") > -1) || (navigator.userAgent.indexOf("Firefox") > -1) || (navigator.userAgent.indexOf("Safari") > -1)) {
			//--------------------------------------------------------------------------
			// call 'Finalize'
			var rpcName = "Finalize";
			var rpcParam1 = readerName;

			topWebSocketUtil.invokeRpcDispatcher(Finalize_Callback,rpcName,readerName);

			//--------------------------------------------------------------------------
			// Name: Finalize_Callback
			function Finalize_Callback(rpcStatus, rpcReturn)
			{
				try {
					//------------------------------------------------------------------
					topWebSocketUtil.tryRpcStatus(rpcStatus);
					//------------------------------------------------------------------
					status=rpcReturn;
					//alert("Finalize: " + rpcReturn);
				} catch (exception) {
					alert("exception: " + exception);
				}
			}
		}
	}
	else
	{
		status = myobj.Finalize(readerName);
		//alert("Finalize status:"+status);
		return status;
	}
}
function DetectCard(outerCallBackFunction,readername1)
{
	var oBrow2 = new ckBrowser2();
	var status="";
	var ff,readerName;
	readerName = readername1;
	if(!oBrow2.isIE){
		if((navigator.userAgent.indexOf("Chrome") > -1) || (navigator.userAgent.indexOf("Firefox") > -1) || (navigator.userAgent.indexOf("Safari") > -1)) {
			var rpcName = "DetectCard";
			var rpcParam1 = readerName;

			topWebSocketUtil.invokeRpcDispatcher(DetectCard_Callback,rpcName,readerName);

 		   //--------------------------------------------------------------------------
    		/// Name: DetectCard_Callback
			function DetectCard_Callback(rpcStatus, rpcReturn)
			{
	    		try {
	        		//------------------------------------------------------------------
	        		topWebSocketUtil.tryRpcStatus(rpcStatus);
	        		//------------------------------------------------------------------
	        		var DetectCardData = rpcReturn.split('|');
	    			status= DetectCardData[0];
 					//alert("DetectCard readerName = " + readerName);
	        		if (DetectCardData[0] != "E000")
	        		{
	            		//alert("DetectCard Error = " + DetectCardData[0]);
	        		}
	        		else
	        		{
						document.getElementById('CARDNAME').value = readerName;
	            		//alert("Card SN = " + DetectCardData[1]);
	        		}
	    		} catch (exception) {
	        		alert("exception: " + exception);
	    		}
				var fullCallBack = outerCallBackFunction + "('" + status + "')";
				eval(fullCallBack);									    		
			}		
		}
	}
	else {
			ff = myobj.DetectCard(readerName);
			var DetectCardData = ff.split('|');
 			status = DetectCardData[0];
 			//alert("DetectCard readerName = " + readerName);
    		if (DetectCardData[0] != "E000")
    		{
    			//alert("DetectCard Error = " + DetectCardData[0]);
    		}
    		else
    		{
    			//alert("Card SN = " + DetectCardData[1]);
    			document.getElementById('CARDNAME').value = readerName;
			}
			return status;
	}
}