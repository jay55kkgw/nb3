<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 50);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 400);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
		
		jQuery(function($) {
			$('.dtable').DataTable({
				scrollX: true,
				sScrollX: "99%",
				scrollY: true,
				bPaginate: false,
				bFilter: false,
				bDestroy: true,
				bSort: false,
				info: false,
			});
		});
	});
	
	// 畫面初始化
	function init() {
		// 確認鍵 click
		submit();
	}
	
	// 確認鍵 Click
	function submit() {
		$("#CMSUBMIT").click( function(e) {
			console.log("submit~~");
			// 遮罩
         	initBlockUI();
            $("#formId").attr("action","");
	 	  	$("#formId").submit();
		});
	}

</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 通訊資料變更     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0359" /></li>
    <!-- 信用卡帳單地址/電話變更     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0404" /></li>
		</ol>
	</nav>


	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.D0404" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="">
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-message">
							<span><spring:message code="LB.X0360"/></span>
						</div>
						<table class="stripe table table-striped ttb-table dtable m-0" data-show-toggle="first">
							<thead>
								<tr>
									<th data-title=""></th>
									<th data-title=""></th>
									<th data-title="<spring:message code="LB.D0409" />"><spring:message code="LB.D0409" /></th>
									<th data-title="<spring:message code="LB.Telephone" />"><spring:message code="LB.Telephone" /></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><spring:message code="LB.D0406" /></td>

									<td>
										<spring:message code="LB.D0149" />：<br>(<spring:message code="${cd_result.data.strBILLTITLE}" />)
									</td>
									<td style="text-align:left"><c:out value='${fn:escapeXml(cd_result.data.oldZIP)}' /><br><c:out value='${fn:escapeXml(cd_result.data.oldADDR)}' /></td>
									<td style="text-align:left">
										<spring:message code="LB.D0206" />：<c:out value='${fn:escapeXml(cd_result.data.oldHOMEPHONE)}' /><br>
										<spring:message code="LB.D0094" />：<c:out value='${fn:escapeXml(cd_result.data.oldOFFICEPHONE)}' /><br>
										<spring:message code="LB.D0413" />：<c:out value='${fn:escapeXml(cd_result.data.oldOFFICEEXT)}' /><br>
										<spring:message code="LB.D0069" />：<c:out value='${fn:escapeXml(cd_result.data.oldMOBILEPHONE)}' />
									</td>

								</tr>
								<tr>
									<td style="text-align:center"><spring:message code="LB.D0415" /></td>

									<td>
										<spring:message code="LB.D0149" />：
									</td>
									<td style="text-align:left"><c:out value='${fn:escapeXml(cd_result.data.newZIP)}' /><br><c:out value='${fn:escapeXml(cd_result.data.newADDR)}' /></td>
									<td style="text-align:left">
										<spring:message code="LB.D0206" />：<c:out value='${fn:escapeXml(cd_result.data.newHOMEPHONE)}' /><br>
										<spring:message code="LB.D0094" />：<c:out value='${fn:escapeXml(cd_result.data.strOfficeTel)}' /><br>
										<spring:message code="LB.D0413" />：<c:out value='${fn:escapeXml(cd_result.data.strOfficeExt)}' /><br>
										<spring:message code="LB.D0069" />：<c:out value='${fn:escapeXml(cd_result.data.newMOBILEPHONE)}' />
									</td>
								</tr>
							</tbody>
						</table>
						<p><font color="red">※&nbsp;<spring:message code="LB.X0362" /></font></p>
					</div>
				</div>
				</form>
				<p><spring:message code="LB.Description_of_page" /></p>
				<ol class="list-decimal">
					<li><spring:message code="LB.Credit_Data_P4_D1" /></li>
					<li><spring:message code="LB.Credit_Data_P4_D2" /></li>
				</ol>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>