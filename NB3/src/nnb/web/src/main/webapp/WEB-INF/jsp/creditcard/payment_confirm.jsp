<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>

	<style>
		.noZhCn{
			-ms-ime-mode: disabled;
		}
	</style>

<script type="text/javascript">
		var transferType = '${transfer_data.data.TransferType }'
		var fgsvacno = '${transfer_data.data.FLAG }'
		var isikeyuser = '${sessionScope.isikeyuser}';
		$(document).ready(function() {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 初始化驗證碼
			setTimeout("initKapImg()", 200);
			// 生成驗證碼
			setTimeout("newKapImg()", 300);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		
		
		function init(){
			// 表單驗證初始化
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
			
			// 顯示預約或即時
			nowOrBooking();
			//一開始先FOCUS到BHO
			focusBHO();
			//BHO跳格
			BHOJump();
			// 確認鍵 click
			goOn();
			// 上一頁按鈕 click
			goBack();
			// 判斷顯不顯示驗證碼
			chaBlock();
			// 交易機制 click
			fgtxwayClick();
			// 交易機制 表單驗證
			fgtxwayValidateEvent();
			
			$($('input[name="FGTXWAY"]')[0]).click();
			
		};
		
		function checkAmountByFGTXWAY(fgtxway){
			var retStr = "";
			var amount = '${transfer_data.data.AMOUNT }'
				amount = fstop.unFormatAmtToInt(amount);  
				console.log("amount: " + amount);
				console.log("transferType: " + transferType);
				console.log("fgsvacno: " + fgsvacno);
			if('0' == fgtxway && amount > 5000000 ){
				retStr = "<spring:message code= "LB.X1413" />\n\n <spring:message code= "LB.X1414" />";
				//alert(retStr);
				errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1413' />", "\n", "\n", "<spring:message code= 'LB.X1414' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.X1413' />", "\n", "\n", "<spring:message code= 'LB.X1414' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
				return false;
			}
			if('1' == fgtxway  ){
				//約>約
				if(transferType =='PD' && fgsvacno=='1'){
					if(amount > 15000000){
						retStr = "<spring:message code= "LB.X1413" />\n\n <spring:message code= "LB.X1415" />";
						//alert(retStr);
						
						return false;
					}
				//約>非約
				}else{
					if(amount > 3000000){
						retStr = "<spring:message code= "LB.X1413" />\n\n <spring:message code= "LB.X1416" />";
						//alert(retStr);
						errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1413' />", "\n", "\n", "<spring:message code= 'LB.X1416' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
						return false;
					}
				}
			}
			
			if('2' == fgtxway){
				
// 				if( amount > 5000000){
// 					retStr = "交易機制選擇錯誤\n\n 預約/即時轉帳金額大於500萬時,交易機制不可以選【晶片金融卡】";
// 					alert(retStr);
// 					return false;
// 				}
				if(transferType =='NPD' || (fgsvacno =='2' || fgsvacno =='0')){
					if( amount > 100000){
					retStr = "<spring:message code= "LB.X1413" />\n\n <spring:message code= "LB.X1417" />";
					//alert(retStr);
					errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.X1413' />", "\n", "\n", "<spring:message code= 'LB.X1417' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
					return false;
					}
				}
			}
			
			return true;
			
		}
		
		function nowOrBooking() {
			if ("${transfer_data.data.FGTXDATE}" == "1") {
				$("#FGTXDATE").html('<spring:message code="LB.Immediately" />');
			} else if ("${transfer_data.data.FGTXDATE}" == "2") {
				$("#FGTXDATE").html('<spring:message code="LB.Booking" />');
			}
		}
		
		//一開始先FOCUS到BHO
		function focusBHO(){
    		$("#creditcardpayInAccountText1").focus();
		}
		
		function BHOJump(){
			$("#creditcardpayInAccountText1").keyup(
				function(event) {
					if (event.which != 8 && event.which != 46 && event.which != 9 && event.which != 16 && event.which != 20) {
						$("#creditcardpayInAccountText2").focus();
					}
				}
			);
			$("#creditcardpayInAccountText2").keyup(function(event) {
					if (event.which != 8 && event.which != 46 && event.which != 9 && event.which != 16 && event.which != 20) {
						$("#creditcardpayInAccountText3").focus();
					}
			});
		}
		
		
		// 確認鍵 Click
		function goOn() {
			$("#CMSUBMIT").click( function(e) {
				// 送出進表單驗證前將span顯示
				$("#hideblock").show();
				
				console.log("submit~~");
				
				$("#BHO").val($('#creditcardpayInAccountText1').val()+$('#creditcardpayInAccountText2').val()+$('#creditcardpayInAccountText3').val());
				// 表單驗證
				if ( !$('#formId').validationEngine('validate') ) {
					e.preventDefault();
				} else {
					var uri = "${__ctx}/checkBHO"
						rdata = {
							keyInBHO : $("#BHO").val(),
							functionName : "creditcardpay"
						};
						fstop.getServerDataEx(uri,rdata, true, function(result) {
							
							if(result.result){
								console.log("BHO PASS");
								// 解除表單驗證
								$("#formId").validationEngine('detach');
								// 通過表單驗證
								processQuery();
							}else{
								//alert(result.message);
								errorBlock(
										null, 
										null,
										[result.message], 
										'<spring:message code= "LB.Quit" />', 
										null
									);
							}
							
						})
				}
			});
			
		}
		
		
		// 通過表單驗證準備送出
		function processQuery(){
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
			if(!checkAmountByFGTXWAY(fgtxway)){
				return false;
			}
			// 交易機制選項
			switch(fgtxway) {
				case '0':
					$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
					initBlockUI();
					$("form").submit();
					break;
				case '1':
					// IKEY
					var jsondc = $("#jsondc").val();
					uiSignForPKCS7(jsondc);
					break;
				case '2':
					// 晶片金融卡
					var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
					useCardReader(capUri);
			    	break;
				case '7'://IDGATE認證		 
		            idgatesubmit= $("#formId");		 
		            showIdgateBlock();		 
		            break;
				default:
					//請選擇交易機制
					//alert("<spring:message code= "LB.Alert001" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert001' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			}
		}
		
		
		function goBack(){
			$("#previous").click(function() {
				var action = '${pageContext.request.contextPath}'+ '${previous}'
				$('#back').val("Y");
				$("form").attr("action", action);
				//jsondc不須要回傳
				$('#jsondc').attr("disabled",true);
				initBlockUI();
				$("#formId").validationEngine('detach');
				$("form").submit();
			});
		}
		
		 // 判斷交易機制決定顯不顯示驗證碼區塊
		function chaBlock() {
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
	 		console.log("fgtxway: " + fgtxway);
			// 交易機制選項
			if(fgtxway == '2'){
				// 若為晶片金融卡才顯示驗證碼欄位
				$("#chaBlock").show();
			}else{
				// 若非晶片金融卡則隱藏驗證碼欄位
				$("#chaBlock").hide();
			}
		 }
		 
		// 使用者選擇晶片金融卡要顯示驗證碼區塊
		 function fgtxwayClick() {
		 	$('input[name="FGTXWAY"]').change(function(event) {
		 		// 判斷交易機制決定顯不顯示驗證碼區塊
		 		chaBlock();
			});
		
		}
		

		// 驗證碼刷新
		function changeCode() {
			$('input[name="capCode"]').val('');
			// 大小版驗證碼用同一個
			console.log("changeCode...");
			$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

			// 登入失敗解遮罩
			unBlockUI(initBlockId);
		}
			
		// 初始化驗證碼
		function initKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
		}
			
		// 生成驗證碼
		function newKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').click(function() {
				$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
			});
		}
		
		function fgtxwayValidateEvent(){
			$('input[name="FGTXWAY"]').change(function(event) {
				if($('input[name="FGTXWAY"]:checked').val()=='0'){
					$('#CMPASSWORD').addClass("validate[required,custom[onlyLetterNumber]]");
				}
				if($('input[name="FGTXWAY"]:checked').val()=='1'){
					$('#CMPASSWORD').removeClass("validate[required,custom[onlyLetterNumber]]");
				}
				if($('input[name="FGTXWAY"]:checked').val()=='2'){
					$('#CMPASSWORD').removeClass("validate[required,custom[onlyLetterNumber]]");
				}
				if($('input[name="FGTXWAY"]:checked').val()=='7'){
					$('#CMPASSWORD').removeClass("validate[required,custom[onlyLetterNumber]]");
				}
			});
			
		}
		
		
		


</script>

</head>
<body>

	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	    <!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %> 
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 信用卡     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Credit_Card" /></li>
    <!-- 繳本行信用卡費     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Payment_The_Banks_Credit_Card" /></li>
		</ol>
	</nav>


	<!-- 左邊menu及登入資訊 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>


		<main class="col-12"> <!-- 主頁內容  -->
		<section id="main-content" class="container">
			<h2>
				<spring:message code="LB.Payment_The_Banks_Credit_Card" />
				<!-- 繳納本行信用卡 -->
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<!-- 下拉式選單-->
				<div class="print-block no-l-display-btn">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message
								code="LB.Execution_option" /></option>
						<!-- 重新輸入-->
						<option value="reEnter" >
							<spring:message code="LB.Re_enter" />
						</option>
					</select>
				</div>	
			<form id="formId" method="post"
				action="${__ctx}/CREDIT/PAY/payment_result">
				<input type="hidden" name="TXTOKEN" value="${transfer_data.data.TXTOKEN}"> 
				<input type="hidden" name="FGTXDATE" value="${transfer_data.data.FGTXDATE}"> 
				<input type="hidden" name="CMTRMEMO" value="${transfer_data.data.CMTRMEMO}">
				<input type="hidden" name="CMMAILMEMO" value="${transfer_data.data.CMMAILMEMO}">
				<input type="hidden" name="CMTRMAIL" value="${transfer_data.data.CMTRMAIL}">
				<input type="hidden" id="PINNEW" name="PINNEW" value="">
				<input type="hidden" id="back" name="back" value="">
				<!-- 交易機制所需欄位 -->
				<input type="hidden" id="jsondc" name="jsondc" value="${transfer_data.data.jsondc}">
				<input type="hidden" id="ISSUER" name="ISSUER" value="">
				<input type="hidden" id="ACNNO" name="ACNNO" value="">
				<input type="hidden" id="TRMID" name="TRMID" value="">
				<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
				<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
				<input type="hidden" id="TAC" name="TAC" value="">
				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
				<div id="step-bar">
					<ul>
						<li class="finished"><spring:message code="LB.Enter_data" />
							<!-- 輸入資料 --></li>
						<li class="active"><spring:message code="LB.Confirm_data" />
							<!-- 確認資料 --></li>
						<li class=""><spring:message code="LB.Transaction_complete" />
							<!-- 交易完成 --></li>
					</ul>
				</div>
				<div class="main-content-block row">
					<div class="col-12">
						<div class="ttb-input-block">
							<div class="ttb-message">
								<p id="FGTXDATE"></p>
								<span> <spring:message code="LB.Confirm_payment_data" />
									<!-- 請確認繳費資料 -->
								</span>
							</div>
							<!-- 轉帳日期 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
										<h4>
											<spring:message code="LB.Transfer_date" />
											<!-- 轉帳日期 -->
										</h4>
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<p>
				      						${transfer_data.data.transfer_date}
										</p>
									</div>
								</span>
							</div>
							<!-- 轉出帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
										<h4>
											<spring:message code="LB.Payers_account_no" />
											<!-- 轉出帳號 -->
										</h4>
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<p>
											${transfer_data.data.OUTACN }${transfer_data.data.OUTACN_NPD }
										</p>
									</div>
								</span>
							</div>
							<!-- 轉入帳戶 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
										<h4>
											<spring:message code="LB.Payees_account_no" />
											<!-- 轉入信用卡帳號（轉入帳號） -->
										</h4>
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<p>
	                                       ${transfer_data.data.DPAGACNO_TEXT}
										</p>
									</div>
								</span>
							</div>
							<!-- 轉入帳號確認 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label><h4>
											<spring:message code="LB.Confirm_payers_account_no" />
										</h4></label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<img
											src="${__ctx}/getBHO?transferInAccount=${transferInAccount}&functionName=creditcardpay" />
										<br />
										<br />
									</div>
									<div class="BHOInput">
										<input type="text" id="creditcardpayInAccountText1" maxlength="1" class="text-input noZhCn" />
									  - <input type="text" id="creditcardpayInAccountText2" maxlength="1" class="text-input noZhCn" />
									  - <input type="text" id="creditcardpayInAccountText3" maxlength="1" class="text-input noZhCn" />
										<span class="input-unit"><spring:message code="LB.Anti-blocking_BHO_attack" /></span>
										<span id="hideblock" >
										<!-- 驗證用的input -->
										<input id="BHO" name="BHO" type="text" maxlength="3"  class="text-input validate[required,custom[onlyLetterNumber]]" 
											style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
									</span>
									</div>
								</span>
							</div>
							<!-- 轉帳金額 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
										<h4>
											<spring:message code="LB.D0021" />
											<!-- 轉帳金額 -->
										</h4>
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<p class="high-light">
											<spring:message code="LB.NTD" />
											<!-- 新台幣 -->
											&nbsp;
											${transfer_data.data.AMOUNT_SHOW}
											&nbsp;
											<spring:message code="LB.Dollar" />
										</p>
									</div>
								</span>
							</div>
							<!-- 交易機制 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
										<h4>
											<spring:message code="LB.Transaction_security_mechanism" />
											<!-- 交易機制 -->
										</h4>
								</label>
								</span> 
								<span class="input-block">
								<c:if test="${SSLFLAG eq 'Y' || transfer_data.data.FLAG ne '0'}">
									<div class="ttb-input">
										<label class="radio-block"> <spring:message
												code="LB.SSL_password" />
											<!-- 交易密碼(SSL) --> <input type="radio" name="FGTXWAY"
											value="0" > <!-- 	                                        <input type="radio" name="FGTXWAY" checked="checked" value="0"> -->
											<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input" name="pw_group">
										<input type="password" id="CMPASSWORD" name="CMPASSWORD" type="password"
											maxlength="8" value=""
											class="text-input validate[required,custom[onlyLetterNumber]]"
											placeholder="<spring:message code="LB.Please_enter_password" /> ">
										<!-- 請輸入交易密碼 -->
									</div>
								</c:if>									
									<!-- 使用者是否可以使用IKEY -->
										<c:if test = "${sessionScope.isikeyuser}">
											<!-- 即使使用者可以用IKEY，若轉出帳號為晶片金融卡帶出的非約定帳號，也不給用 (By 景森)-->
											<c:if test = "${transfer_data.data.TransferType != 'NPD'}">
												<div class="ttb-input" name="ikey_group">
													<label class="radio-block"> <spring:message code="LB.Electronic_signature" />
												<input type="radio" name="FGTXWAY" value="1"> <span class="ttb-radio"></span></label> <!-- 電子簽章(請載入載具i-key) --> 
												</div>	
											</c:if>
										</c:if>
										<div class="ttb-input" name="idgate_group" style="display:none">		 
			                                <label class="radio-block">裝置推播認證(請確認您的行動裝置網路連線是否正常，及推播功能是否已開啟)
				                                <input type="radio" id="IDGATE" name="FGTXWAY" value="7"> 
				                                <span class="ttb-radio"></span>
			                                </label>		 
                                		</div>
									<!-- 晶片金融卡 -->
										<div class="ttb-input" name="card_group">
											<label class="radio-block">
												<spring:message code="LB.Financial_debit_card" />
												
												<!-- 只能選晶片金融卡時，預設為非勾選讓使用者點擊後顯示驗證碼-->
												<c:choose>
													<c:when test="${transfer_data.data.TransferType == 'NPD'}">
														<input type="radio" name="FGTXWAY" id="CMCARD" value="2">
													</c:when>
													<c:when test="${!sessionScope.isikeyuser}">
														<input type="radio" name="FGTXWAY" id="CMCARD" value="2">
													</c:when>
													<c:otherwise>
													 	<input type="radio" name="FGTXWAY" id="CMCARD" value="2">
													</c:otherwise>
												</c:choose>
												
												<span class="ttb-radio"></span>
											</label>
										</div>
							</span>
						</div>
						
						<!-- 如果是非約定則不能使用IKEY，只能選晶片金融卡，使用者選擇晶片金融卡後才顯示驗證碼-->
								<div class="ttb-input-item row" id="chaBlock" style="display:none">
									<!-- 驗證碼 -->
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Captcha" /></h4>
										</label>
									</span>
									<span class="input-block">
										<spring:message code="LB.Captcha" var="labelCapCode" />
										<input id="capCode" name="capCode" type="text"
											class="text-input" maxlength="6" autocomplete="off">
										<img name="kaptchaImage" class = "verification-img" src="" />
										<input type="button" name="reshow" class="ttb-sm-btn btn-flat-orange"
											onclick="changeCode()" value="<spring:message code="LB.Regeneration" />" />
									</span>
								</div>
					</div>

						<input type="button" id="previous" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />">
						<!-- 回上頁 -->
						<input type="button" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />">
						<!-- 確定 -->

					</div>
					<!-- col-12 tab-content END -->
				</div>
				<!-- main-content-block END -->
			</form>
		</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>