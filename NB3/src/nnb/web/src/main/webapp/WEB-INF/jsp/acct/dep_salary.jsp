<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>

<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>
<style>
     .dep_comm{
            background-color: white;
            text-align: center;
        }
        .dep_comm button{
            font-size:x-large;
        }
  
        .dep_title{
            background-color: white;
            text-align: center;
            width: 280px;
            height: 24px;
            margin-bottom: 0.2em;
            padding-top: 1em;
        }

		.dep_div_img{
			width: 100%;
			margin-left: 3px;
		}
		
		.dep_row{
			margin-left: 3px;
		}

        @media screen and (min-width: 260px) and (max-width: 285px) {
            .dep_title{
                width: 100%;
            }
            .dep_body{
                width: 100%!important;
                margin-bottom: 3em;
            } 
            .dep_div1 div{
                font-size: 0.85em;
            }
            
            .dep_description{
                width: 100%;
            }
			.dep_div_img{
				width: 280px;
			}
        }

        @media screen and (min-width: 310px) and (max-width: 321px) {
            .dep_title{
                width: 100%;
            }
            .dep_body{
                width: 100%!important;
                margin-bottom: 3em;
            } 
            .dep_div1 div{
                font-size: 0.85em;
            }

            .dep_description{
                width: 100%;
            }
			.dep_div_img{
				width: 320px;
			}
        }

        @media screen and (min-width: 355px) and (max-width: 361px) {
            .dep_title{
                width: 100%;
            }
            .dep_body{
                width: 100%!important;
                margin-bottom: 4em;
            }
  
            .dep_description{
                width: 100%;
            }
			.dep_div_img{
				width: 360px;
			}
        }


        @media screen and (min-width: 370px) and (max-width: 376px) {
            .dep_title{
                width: 100%;
            }
            .dep_body{
                width: 100%!important;
                margin-bottom: 3em;
            }
            .dep_img{
                width: 100%;
            }
            .dep_description{
                width: 100%;
            }
			.dep_div_img{
				width: 375px;
			}
        }

        @media screen and (min-width: 377px) and (max-width: 450px) {
            .dep_title{
                width: 100%;
            }
            .dep_body{
                width: 100%!important;
                margin-bottom: 3em;
            }
           
            .dep_description{
                width: 100%;
            }
			.dep_div_img{
				width: 411px;
			}
		}

        @media screen and (min-width: 530px) and (max-width: 550px) {
            .dep_title{
                width:100%;
            }
            .dep_body{
                width:100%!important;
                margin-bottom: 3em;
            }
            .ttb-button{
                width: 100%;
            }
            .dep_div2 div div{
                padding-left: 2em!important;
            }
       
            .dep_description{
                width: 100%;
            }

			.dep_div_img{
				width: 540px;
			}
        }

        @media screen and (min-width: 750px) and (max-width: 770px) {
            .dep_title{
                width: 100%;
            }

            .ttb-button{
                width: 100%;
            }
            .dep_body{
                width: 100%!important;
                margin-bottom: 3em;
            }
			.dep_div_img{
				width: 768px;
			} 
        }

        .dep_body{
            background-color: white;
            text-align: center;
            width: 280px;
            height: 250px;
        }
        .dep_div1{
            padding: 0;
        }

        .dep_div1 .dep_body{
            font-size: 1.5em;
            padding-bottom: 1.5em;
        }

        .dep_div1 .dep_title{
            padding-bottom: 2.3em;
        }
        
        .dep_div1 div div{
            font-size: 0.6em;
            text-align: center;
        }
        
        .dep_div1 .ttb-button{
           bottom:0;
        }
        
        .dep_div1 .dep_message font{
        	font-size:1.2em;
        }
        
        .dep_div2{
            margin-left: auto;	
            padding: 0;
        }

        .dep_div2 .dep_body{
            font-size: 1.5em;
        }

        .dep_div2 .dep_title{
            font-size: 0.85em;
            padding-bottom: 3em;
        }
        
        .dep_div2 .dep_message{
            padding-top: 1em;
            padding-bottom: 1em;
        }


        .dep_div2 div{
            font-size: 0.85em;
        }

        .dep_div2 div div{
            font-size: 1.2em;
            text-align: center;
        }
        
        .dep_div2 div .beat{
            padding-bottom: 1em;
        }
	
		.dep_div2 .ttb-button{
           bottom:-90;
        }
        .dep_row1{
            margin-bottom:0.2em;
		}


		.dep_part{
            background-color: transparent;
            box-shadow: none;
        }

        .dep_core{
            background-color: white;
            border-radius: 8px;
            padding-bottom: 20px;
        }

</style>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>




	<!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i
					class="fa fa-home"></i></a></li>
			<!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message
					code="LB.NTD_Services" /></li>
			<!-- 帳戶查詢 -->
			<li class="ttb-breadcrumb-item">薪轉戶專區</li>
			<!-- 帳戶餘額查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X2481" /></li>
		</ol>
	</nav>



	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>

		<!-- 	快速選單及主頁內容 -->
		<main class="col-12">
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.X2481" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>

				<div class="main-content-block row">
					<div class="col-12">
						<ul class="ttb-result-list">
							<!-- 查詢時間 -->
							<li>
								<h3>
									<spring:message code="LB.Inquiry_time" />
									：
								</h3>
								<p>${dep_salary.data.CMQTIME }</p>
								<p><spring:message code="LB.X2484" />:
									<c:if test="${not empty dep_salary.data.ITR}">
										${dep_salary.data.ITR }%
									</c:if>
								</p>
							</li>
						</ul>

						<table class="stripe table-striped ttb-table dtable" id="example"
							data-show-toggle="first">
							<thead>
								<tr>
									<th class="text-center"><spring:message code="LB.Account" /></th>
									<th class="text-center"><spring:message
											code="LB.Deposit_type" /></th>
									<th class="text-center"><spring:message
											code="LB.Available_balance" /></th>
									<th class="text-center"><spring:message
											code="LB.Account_balance" /></th>
									<th class="text-center"><spring:message
											code="LB.Exchange_ticket_today" /></th>
									<!-- 快速選單功能 -->
									<th class="text-center"><spring:message
											code="LB.Quick_Menu" /></th>
									<!-- 快速選單測試 -->
								</tr>
							</thead>
							<c:if test="${empty dep_salary.data.ACN}">
								<tbody>
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
								</tbody>
							</c:if>
							<c:if test="${not empty dep_salary.data.ACN}">
								<tbody>
									<tr>
										<td class="text-center">${dep_salary.data.ACN }</td>
										<td class="text-center">${dep_salary.data.KIND }</td>
										<td class="text-right">${dep_salary.data.ADPIBAL }</td>
										<td class="text-right">${dep_salary.data.BDPIBAL }</td>
										<td class="text-center">${dep_salary.data.CLR }</td>
										<td class="text-center"><c:if
												test="${dep_salary.data.SHOWSELECT == 'Y'}">
												<!-- 下拉式選單-->
												<input type="button" class="d-lg-none" id="click"
													onclick="hd2('actionBar2${data.index}')" value="..." />
												<%--  <button class="d-lg-none" id="click" onclick="hd2('actionBar2${data.index}')">...</button> --%>
												<%--  <input type="button" class="d-sm-none d-block" id="click" value="..." onclick="hd2('actionBar2${data.index}')"/><!-- 快速選單測試 -->				 --%>
												<select
													class="custom-select d-none d-lg-inline-block fast-select"
													id="actionBar"
													onchange="formReset(this.value,'${dep_salary.data.ACN }');">
													<option value="">
														<spring:message code="LB.Select" />
													</option>
													<!-- 活期性存款明細 -->
													<option value="demand_deposit_detail">
														<spring:message code="LB.NTD_Demand_Deposit_Detail" />
													</option>
													<c:if test="${dep_salary.data.ACNGROUP == '01'}">
														<!-- 當日待補票據明細 -->
														<option value="checking_insufficient">
															<spring:message code="LB.X1571" />
														</option>
														<!-- 已兌現票據明細 -->
														<option value="checking_cashed">
															<spring:message code="LB.Cashed_Note_Details" />
														</option>
													</c:if>
													<c:if test="${dep_salary.data.OUTACN == 'Y'}">
														<!-- 轉帳 -->
														<option value="transfer">
															<spring:message code="LB.NTD_Transfer" />
														</option>
														<!-- 轉入台幣綜存定存 -->
														<option value="deposit_transfer">
															<spring:message
																code="LB.Open_Taiwan_Currency_Time_Deposit" />
														</option>
														<!-- 繳本行信用卡款 -->
														<option value="payment">
															<spring:message code="LB.Payment_The_Banks_Credit_Card" />
														</option>
														<!-- 停車費代扣繳申請 -->
														<option value="n614">
															<spring:message code="LB.X0276" />
														</option>
													</c:if>
												</select>
												<!-- 快速選單測試新增div -->
												<div id="actionBar2${data.index}" class="fast-div-grey"
													style="visibility: hidden">
													<div class="fast-div">
														<img src="${__ctx}/img/icon-close-2.svg"
															class="top-close-btn"
															onclick="hd2('actionBar2${data.index}')">
														<p>
															<spring:message code="LB.X1592" />
														</p>
														<ul>
															<a
																href="${__ctx}/NT/ACCT/demand_deposit_detail?Acn=${dep_salary.data.ACN }">
																<li><spring:message
																		code="LB.NTD_Demand_Deposit_Detail" /><img
																	src="${__ctx}/img/icon-10.svg" align="right"></li>
															</a>
															<c:if test="${dep_salary.data.ACNGROUP == '01'}">
																<a
																	href="${__ctx}/NT/ACCT/checking_insufficient?Acn=${dep_salary.data.ACN }">
																	<li><spring:message code="LB.X1571" /><img
																		src="${__ctx}/img/icon-10.svg" align="right"></li>
																</a>
																<a
																	href="${__ctx}/NT/ACCT/checking_cashed?Acn=${dep_salary.data.ACN }">
																	<li><spring:message code="LB.Cashed_Note_Details" />
																		<img src="${__ctx}/img/icon-10.svg" align="right"></li>
																</a>
															</c:if>
															<c:if test="${dep_salary.data.OUTACN == 'Y'}">
																<a
																	href="${__ctx}/NT/ACCT/TRANSFER/transfer?Acn=${dep_salary.data.ACN }">
																	<li><spring:message code="LB.NTD_Transfer" /><img
																		src="${__ctx}/img/icon-10.svg" align="right"></li>
																</a>
																<a
																	href="${__ctx}/NT/ACCT/TDEPOSIT/deposit_transfer?Acn=${dep_salary.data.ACN }">
																	<li><spring:message
																			code="LB.Open_Taiwan_Currency_Time_Deposit" /> <img
																		src="${__ctx}/img/icon-10.svg" align="right"></li>
																</a>
																<a
																	href="${__ctx}/CREDIT/PAY/payment?Acn=${dep_salary.data.ACN }">
																	<li><spring:message
																			code="LB.Payment_The_Banks_Credit_Card" /> <img
																		src="${__ctx}/img/icon-10.svg" align="right"></li>
																</a>
																<a
																	href="${__ctx}/PARKING/FEE/parking_withholding?Acn=${dep_salary.data.ACN }">
																	<li><spring:message code="LB.X0276" /><img
																		src="${__ctx}/img/icon-10.svg" align="right"></li>
																</a>
															</c:if>
														</ul>
													</div>
												</div>
											</c:if></td>
									</tr>
								</tbody>
							</c:if>
						</table>
					</div>
				</div>
				
				<h2>
					<!-- 每月薪轉優惠 -->
					<spring:message code="LB.X2482" />
				</h2>

				<BR>
				<div class="row dep_row">
					<div class="col-sm-12 col-md-12 col-lg-3 col-xs-12 dep_div1">
						<div class="dep_title">
							<h4>
								<b>跨行提款/轉帳優惠</b>
							</h4>
						</div>
						<div class="dep_body">
							<div class="dep_message">
								<br>
									<b>免費跨行提款優惠剩餘</b>
									<br>
									<b><font style="color: red;">${dep_salary.data.ABRECT}</font>次(本月優惠<font style="color: red;">${dep_salary.data.ABAVCT}</font>次)</b> 
									<br>
									<br>
									<b>免費跨行轉帳優惠剩餘</b>
									<br>
									<b><font style="color: red;">${dep_salary.data.ABRECTX}</font>次(本月優惠<font style="color: red;">${dep_salary.data.ABAVCTX}</font>次)</b> 
									<br>
									<br>
									<b><font style="color: blue;">專案優惠</font></b>
									<br>
									<b>免費跨行轉帳/提款優惠剩餘</b>
									<br>
									<b><font style="color: red;">${dep_salary.data.MEAVCT}</font>次(本月合併優惠<font style="color: red;">${dep_salary.data.MEGECT}</font>次)</b> 
							</div>
							<input type="button" class="ttb-button btn-flat-orange"
								id="goToTransfer" value="我要轉帳"
								onclick="location.href='${__ctx}/NT/ACCT/TRANSFER/transfer?Acn=${dep_salary.data.ACN }'" />
						</div>
					</div>
					<div class="col-sm-12 col-md-12 col-lg-3  col-xs-12 dep_div2">
						<div class="dep_title">
							<h4>
								<b>基金優惠</b>
							</h4>
						</div>
						<div class="dep_body">
							<div class="dep_message">
								基金手續費 <br> <font style="color: #ffa500;"> 最高優惠3.8折 </font>
							</div>
							<input type="button" class="ttb-button btn-flat-orange"
								id="goToPurchase" value="我要下單"
								onclick="location.href='${__ctx}/FUND/PURCHASE/fund_purchase_select'" />
						</div>
					</div>
					<div class="col-sm-12 col-md-12 col-lg-3 col-xs-12 dep_div2">
						<div class="dep_title">
							<h4>
								<b>換匯優惠</b>
							</h4>
						</div>
						<div class="dep_body">
							<div class="dep_message">
								美元換匯 <br> <font style="color: #ffa500;"> 最高減兩分 </font>
							</div>
							<input type="button" class="ttb-button btn-flat-orange"
								id="goToF_transfer" value="我要換匯"
								onclick="location.href='${__ctx}/FCY/TRANSFER/f_transfer'" />
						</div>
					</div>
				</div>
				<br>
				<br>
				<div class="row dep_div_img">
					<img style="width: 100%;height: 100%" src="${__ctx}/img/depnew.jpg">
				</div>
				<br>
			</section>
		</main>
	</div>
	<!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
	<!-- 快速選單測試CSS -->

	<script type="text/javascript">
		$(document).ready(function() {

			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);

			setTimeout("initDataTable()", 100);
			// $('.dtablet').DataTable({
			// 	scrollX: true,
			// 	sScrollX: "99%",
			// 	scrollY: true,
			// 	bPaginate: false,
			// 	bFilter: false,
			// 	bDestroy: true,
			// 	bSort: false,
			// 	info: false,
			// });
			
			var row_width = $(".main-content-block").width();
			$(".dep_row").width(row_width);
		});

		function init() {

			// initFootable();
		}

		//快速選單
		function formReset(value, acn) {
			switch (value) {
			case "demand_deposit_detail":
				fstop.getPage('${pageContext.request.contextPath}'
						+ '/NT/ACCT/demand_deposit_detail?Acn=' + acn + '&',
						'', '');
				break;
			case "checking_insufficient":
				fstop.getPage('${pageContext.request.contextPath}'
						+ '/NT/ACCT/checking_insufficient?Acn=' + acn + '&',
						'', '');
				break;
			case "checking_cashed":
				fstop.getPage('${pageContext.request.contextPath}'
						+ '/NT/ACCT/checking_cashed?Acn=' + acn + '&', '', '');
				break;
			case "transfer":
				fstop
						.getPage(
								'${pageContext.request.contextPath}'
										+ '/NT/ACCT/TRANSFER/transfer?Acn='
										+ acn + '&', '', '');
				break;
			case "deposit_transfer":
				fstop.getPage(
						'${pageContext.request.contextPath}'
								+ '/NT/ACCT/TDEPOSIT/deposit_transfer?Acn='
								+ acn + '&', '', '');
				break;
			case "payment":
				fstop.getPage('${pageContext.request.contextPath}'
						+ '/CREDIT/PAY/payment?Acn=' + acn + '&', '', '');
				break;
			case "n614":
				fstop.getPage('${pageContext.request.contextPath}'
						+ '/PARKING/FEE/parking_withholding?Acn=' + acn + '&',
						'', '');
				break;
			}
		}

		//快速選單測試新增
		//將actionbar select 展開閉合
		$(function() {
			$("#click").on('click', function() {
				var s = $("#actionBar2").attr('size') == 1 ? 8 : 1
				$("#actionBar2").attr('size', s);
			});
			$("#actionBar2 option").on({
				click : function() {
					$("#actionBar2").attr('size', 1);
				},
			});
		});

		function hd2(T) {
			var t = document.getElementById(T);
			if (t.style.visibility === 'visible') {
				t.style.visibility = 'hidden';
			} else {
				$("div[id^='actionBar2']").each(function() {
					var d = document.getElementById($(this).attr('id'));
					d.style.visibility = 'hidden';
				});
				t.style.visibility = 'visible';
			}
		}
	</script>
</body>

</html>