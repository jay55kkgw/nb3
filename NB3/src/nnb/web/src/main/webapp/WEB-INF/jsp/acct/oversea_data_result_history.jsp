<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {
		// 將.table變更為footable
		//initFootable();
		setTimeout("initDataTable()",100);
		init();
	});
	function init() {
		//列印
		$("#printbtn").click(function() {
			var params = {
				"jspTemplateName" : "oversea_data_history_print",
				"jspTitle" : "<spring:message code= "LB.X1496" />",
				"CMQTIME" : "${oversea_data_result_history.data.CMQTIME}",
				"COUNT" : "${oversea_data_result_history.data.CMRECNUM}",
				"CUSIDN":"${oversea_data_result_history.data.CUSIDN}",
				"CUSNAME":"${oversea_data_result_history.data.CUSNAME}"
				};
			openWindowWithPost(
				"${__ctx}/print",
				"height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",
				params);
		});
		$('#CMBACK').click(function(){
			initBlockUI();
			$('#back').val("Y");
			var action = '${__ctx}/FUND/QUERY/oversea_data_result';
			$("#formId").attr("action", action);
			$("#formId").submit();		
		});
	}
</script>
</head>
<body>
<form id="formId" action="" method="post">
<input type="hidden" id="back" name="back" value="">
</form>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

	<!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><a href="#"><spring:message code= "LB.X0368" /></a></li>
			<li class="ttb-breadcrumb-item"><a href="#"><spring:message code= "LB.Inquiry_Service" /></a></li>
			<li class="ttb-breadcrumb-item active" aria-current="page">
				<spring:message code= "LB.W1021" />
			</li>
		</ol>
	</nav>
	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code= "LB.W1021" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>

				<div class="main-content-block row">
				<div class="col-12">
					<ul class="ttb-result-list">
						<li>
							<!-- 查詢時間 -->
							<h3>
								<spring:message code="LB.Inquiry_time" />：
							</h3>
							<p>
								 ${oversea_data_result_history.data.CMQTIME}
							</p> 
						</li>
						<li>
							 <!-- 資料總數 -->
							<h3>
								<spring:message code="LB.Total_records" />：
							</h3>
							<p>
								 ${oversea_data_result_history.data.CMRECNUM}
								<spring:message code="LB.Rows" />
							</p>
						</li>
						<li>
						<!-- 身分證字號/統一編號 -->
						<h3>
						<spring:message code="LB.Id_no" />
						</h3>
						<p>
						${oversea_data_result_history.data.hiddencusidn}
						</p>
						</li>
						<li>
						<h3><spring:message code="LB.Name" /></h3>
						<p>${oversea_data_result_history.data.hiddenname}</p>
						<!-- 姓名 -->
						</li>
					</ul>
						<table class="stripe table-striped ttb-table dtable"  data-toggle-column="first">
							<thead>
								<tr>
									<th><spring:message code="LB.W1022" /><br><spring:message code="LB.W0023" /></th>
      								<th><spring:message code="LB.W1011" /><br><spring:message code="LB.W1012" /></th>
      								<th><spring:message code="LB.Transaction_type" /></th>
      								<th><spring:message code="LB.W1032" /><br><spring:message code="LB.W1014" /></th>
      								<th><spring:message code="LB.W1018" /><br><spring:message code="LB.W1033" /></th>
      								<th><spring:message code="LB.W1034" /><br><spring:message code="LB.D0507" /></th>
      								<th><spring:message code="LB.X0369" /><br><spring:message code="LB.X0370" /></th>
      								<th><spring:message code="LB.W1037" /><br><spring:message code="LB.W0909" /></th>
      								<th><spring:message code="LB.W0977" /><br><spring:message code="LB.W1038" /></th>       
						    	</tr>
							</thead>
							<tbody>
								<c:forEach var="dataList" items="${oversea_data_result_history.data.REC}">
									<tr>
										<td class="text-center">${dataList.O01}<br>${dataList.O03}</td>
										<td class="text-center">${dataList.O04}<br>${dataList.O05}</td>
										<td class="text-center">${dataList.O06}</td>
										<td class="text-center">${dataList.O12}<br>${dataList.O11}</td>
										<td class="text-center">${dataList.O16}<br>${dataList.O13}</td>
										<td class="text-center">${dataList.O14}<br>${dataList.O15}</td>
										<td class="text-center">${dataList.O20}<br>${dataList.O21}</td>
										<td class="text-center">${dataList.O17}<br>${dataList.O18}</td>
										<td class="text-center">${dataList.O22}<br>${dataList.O19}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
		
					<!-- 回上頁-->
					<input type="button" class="ttb-button btn-flat-gray" name="CMBACK" id="CMBACK" value='<spring:message code="LB.Back_to_previous_page"/>' />
					<!-- 列印鈕-->
					<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />" />
					
				</div>
			</div>
		</section>
		</main>	
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>