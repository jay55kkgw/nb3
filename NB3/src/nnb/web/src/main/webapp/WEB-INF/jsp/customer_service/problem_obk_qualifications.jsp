<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="page1" class="card-detail-block">
	<div class="card-center-block">
		<!-- Q1 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-11" aria-expanded="true"
				aria-controls="popup1-11">
				<div class="row">
					<span class="col-1">Q1</span>
					<div class="col-11">
						<span>網路銀行的申請條件？如何申請？</span>
					</div>
				</div>
			</a>
			<div id="popup1-11" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div class="ttb-result-list terms">
								<ol>
									<li>一、臨櫃申請
										<ol>
											<li>　如果您已是本行存款客戶，請本人親攜身分證件、存款印鑑及存摺，赴開戶行填寫申請書，
												並取得網路銀行密碼單即可登入本行網站(https://ebank.tbb.com.tw)進行交易。如為全行收付個人戶（即存摺首頁蓋有存款印鑑），
												亦可攜身份證件、存款印鑑及存摺至國內各分行辦理。</li>
										</ol>
									</li>
									<li>二、線上申請
										<ol>
											<li>1、 本行晶片金融卡存戶：限個人存戶首次申請。 未曾臨櫃申請之存戶，可線上申請查詢服務，請於網路銀行網站使用『
												本行晶片金融卡 + 晶片讀卡機』線上申請網路銀行，如該晶片金融卡已具備非約定轉帳者，
												得線上申請「金融卡線上申請/取消交易服務功能」，執行新臺幣非約定轉帳交易、繳費稅交易、線上更改通訊地址/電話及使用者名稱/簽入密碼/交易密碼線上解鎖等功能。</li>
											<li>2、 本行信用卡正卡客戶（不含商務卡、採購卡、附卡、VISA金融卡）：
												您可於網路銀行網站以信用卡線上申請網路銀行，輸入信用卡個人驗證資料，使用網路銀行信用卡相關服務。
												註：線上申請者無法執行「轉帳」、「申購基金」等功能，如欲增加前述功能，請親洽往來分行辦理升級為臨櫃申請用戶，即可使用完整的理財服務。</li>
										</ol>
									</li>
								</ol>
							</div>
						</li>
						<li>
							<p>三、如果您尚未在本行開戶，請本人攜帶身份證件及印章至本行國內各分行辦理開戶，同時申請網路銀行。</p>
						</li>
						<li>
							<p>四、服務功能：</p>
							<div style="overflow: auto">
								<table class="question-table" style="width: 100%">
									<thead>
										<tr>
											<th rowspan="2">功能/申請方式</th>
											<th rowspan="2">臨櫃申請</th>
											<th colspan="3">線上申請</th>
										</tr>
										<tr>
											<th>晶片金融卡</th>
											<th>晶片金融卡+線上交易功能<br>(需開通非約轉交易)</th>
											<th>信用卡</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th colspan="5" style="text-align: left;font-size: 16px;">臺幣服務</th>
										</tr>
										<tr>
											<th style="text-align: left;">約定/非約定轉帳 (註1)</th>
											<td>O</td>
											<td>X</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<th style="text-align: left;">定存</th>
											<td>O</td>
											<td>X</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<th style="text-align: left;">匯入匯款通知設定</th>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<th colspan="5" style="text-align: left;font-size: 16px;">外幣服務</th>
										</tr>
										<tr>
											<th style="text-align: left;">存款/放款/帳務查詢</th>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<th colspan="5" style="text-align: left;font-size: 16px;">基金服務</th>
										</tr>
										<tr>
											<th style="text-align: left;">基金查詢</th>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<th style="text-align: left;">基金申購及變更</th>
											<td>O</td>
											<td>X</td>
											<td>X</td>
											<td>X</td>
										</tr>
										<tr>
											<th style="text-align: left;">理財試算</th>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>O</td>
										</tr>
										<tr>
											<th colspan="5" style="text-align: left;font-size: 16px;">信用卡服務</th>
										</tr>
										<tr>
											<th style="text-align: left;">信用卡帳務查詢</th>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>O</td>
										</tr>
										<tr>
											<th style="text-align: left;">電子帳單/補寄帳單/Email繳款通知</th>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>O</td>
										</tr>
										<tr>
											<th style="text-align: left;">登錄道路救援</th>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>O</td>
										</tr>
										<tr>
											<th colspan="5" style="text-align: left;font-size: 16px;">繳費服務</th>
										</tr>
										<tr>
											<th style="text-align: left;">繳費稅</th>
											<td>O</td>
											<td>X</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<th style="text-align: left;">申請代扣繳費用</th>
											<td>O</td>
											<td>X</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<th colspan="5" style="text-align: left;font-size: 16px;">個人化服務</th>
										</tr>
										<tr>
											<th style="text-align: left;">變更通訊資料 (註2)</th>
											<td>O</td>
											<td>X</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<th style="text-align: left;">線上解鎖使用者名稱/簽入/交易密碼 (註3)</th>
											<td>O</td>
											<td>X</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<th style="text-align: left;">掛失服務</th>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>X</td>
										</tr>
									</tbody>
								</table>
							</div>
							<p>備註</p>
							<ol class="list-decimal">
								<li>倘已線上申請交易功能者，且該晶片金融卡已具備非約定轉帳功能者，並得以本人帳戶之晶片金融卡之主帳號為轉出帳戶，執行新臺幣非約定轉帳交易、繳費稅交易。</li>
								<li>交易機制為「電子簽章」或「晶片金融卡」，即可執行線上更改通訊地址/電話。</li>
								<li>交易機制為「電子簽章」或「晶片金融卡」，即可執行線上簽入/交易密碼線上解鎖。</li>
								<li>O：表示可使用功能，X：表示不可使用功能。</li>
							</ol>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q2 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-12" aria-expanded="true"
				aria-controls="popup1-12">
				<div class="row">
					<span class="col-1">Q2</span>
					<div class="col-11">
						<span>如果我在高雄分行及台北分行各有一個帳戶，是否可以任選一個開戶分行讓兩個帳戶都申請轉出功能，而不要跑兩家分行辦理?</span>
					</div>
				</div>
			</a>
			<div id="popup1-12" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>可以，您如果在台北分行申請網路銀行服務，只需在台北分行填妥約定書，
								於轉出帳號欄位填入台北分行及高雄分行帳戶即可，但請您要帶高雄分行帳戶的存摺（限全行收付戶，存摺首頁蓋有存款印鑑）及印鑑，減少您南北奔波的麻煩。
								如為全行收付個人戶，亦可攜身份證件、存款印鑑及存摺至國內各分行辦理。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q3 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-13" aria-expanded="true"
				aria-controls="popup1-13">
				<div class="row">
					<span class="col-1">Q3</span>
					<div class="col-11">
						<span>申請網路銀行可不可以不要轉帳功能，如果以後再恢復可以嗎？</span>
					</div>
				</div>
			</a>
			<div id="popup1-13" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>可以，轉帳功能可隨您需要做選擇，請您在臨櫃申請時「轉出帳號」欄位不要填寫往來帳號即可，
								日後如果要申請轉帳功能請至原申請分行辦理；如為全行收付個人戶（即存摺首頁蓋有存款印鑑），
								亦可攜身份證件、存款印鑑及存摺至國內各分行辦理。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q4 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-14" aria-expanded="true"
				aria-controls="popup1-14">
				<div class="row">
					<span class="col-1">Q4</span>
					<div class="col-11">
						<span>網路銀行有哪些交易機制？</span>
					</div>
				</div>
			</a>
			<div id="popup1-14" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>網路銀行交易機制區分電子簽章、交易密碼(SSL) 、晶片金融卡及裝置推播認證四種</p>
						</li>
						<li>
							<ol>
								<li>
									<p>1. 電子簽章(採金融XML憑證)</p>
									<p>臨櫃申辦時，請填寫「同意」申請金融XML憑證，取得「憑證識別資料（CN）」及憑證載具(i-Key)，完成申請手續後，請登入網路銀行的「憑證註冊中心」申請及下載憑證。憑證載具每支NT$600元；電子憑證年費採線上扣款，個人戶NT$150元，法人戶NT$900元，有效期間一年。</p>
								</li>
								<li>
									<p>2. 交易密碼(SSL)</p>
									<p>未申請金融XML憑證者，僅可使用交易密碼交易（如查詢、已約定帳戶轉帳…等）。</p>
								</li>
								<li>
									<p>3. 晶片金融卡(本行晶片金融卡+讀卡機)</p>
									<p>該晶片金融卡已具備非約定轉帳功能者，得執行新臺幣非約定交易服務功能。</p>
								</li>
								<li>
									<p>4. 裝置推播認證</p>
									<p>藉由透過綁定用戶的行動裝置來雙重確認用戶之低風險交易，以提升網路銀行交易安控之安全性及便利性，完成綁定後方能使用行動銀行非約定轉帳服務、繳費服務、Email變更及地址與電話變更之功能項目。</p>
								</li>
							</ol>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q5 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-15" aria-expanded="true"
				aria-controls="popup1-15">
				<div class="row">
					<span class="col-1">Q5</span>
					<div class="col-11">
						<span>電子簽章、交易密碼、晶片金融卡及裝置推播認證交易有何不同？</span>
					</div>
				</div>
			</a>
			<div id="popup1-15" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>網路銀行所有交易皆採用SSL 256bit加密保障傳輸安全，交易密碼交易可使用在已約定轉帳，而未約定轉帳需使用安全性更高的電子簽章、晶片金融卡及裝置推播認證。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>