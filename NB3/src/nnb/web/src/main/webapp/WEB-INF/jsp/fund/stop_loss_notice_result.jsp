<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript">
		// 初始化
		$(document).ready(function () {
			init();
		});

		function init() {
			//表單驗證
			$("#formId").validationEngine({
				binded: false,
				promptPosition: "inline"
			});
		}
		//列印
		function print() {
			var params = {
				"jspTemplateName": 'stop_loss_notice_result_print',
				"jspTitle": '<spring:message code= "LB.X0414" />',
				"CMQTIME": '${stop_loss_notice_result.data.CMQTIME}',
				"CUSIDN": '${stop_loss_notice_result.data.CUSIDN}',
				"CDNO": '${stop_loss_notice_result.data.CDNO}',
				"hideid_CUSIDN": '${stop_loss_notice_result.data.hideid_CUSIDN}',
				"hideid_CDNO": '${stop_loss_notice_result.data.hideid_CDNO}',
				"FUNDLNAME": '${stop_loss_notice_result.data.FUNDLNAME}',
				"str_AC202": '${stop_loss_notice_result.data.str_AC202}',
				"FUNDAMT": '${stop_loss_notice_result.data.FUNDAMT}',
				"display_FUNDCUR": '${stop_loss_notice_result.data.display_FUNDCUR}',
				"display_FUNDAMT": '${stop_loss_notice_result.data.display_FUNDAMT}',
				"PAYAMT": '${stop_loss_notice_result.data.PAYAMT}',
				"display_PAYCUR": '${stop_loss_notice_result.data.display_PAYCUR}',
				"show_PAYAMT": '${stop_loss_notice_result.data.show_PAYAMT}',
				"display_PAYAMT": '${stop_loss_notice_result.data.display_PAYAMT}',
				"PAYAMT1": '${stop_loss_notice_result.data.PAYAMT1}',
				"NSTOPPROF": '${stop_loss_notice_result.data.NSTOPPROF}',
				"NSTOPLOSS": '${stop_loss_notice_result.data.NSTOPLOSS}',
			};
			openWindowWithPost("${__ctx}/print",
				"height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",
				params);
		}
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金    -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0901" /></li>
    <!-- 基金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1064" /></li>
    <!-- 停損停利通知設定     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0414" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!--停損停利通知設定 -->
				<h2>
					<spring:message code="LB.X0414" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form autocomplete="off" method="post" id="formId" action="${__ctx}/">
					<c:set var="BaseResultData" value="${stop_loss_notice_result.data}"></c:set>
					<!-- 表單顯示區 -->
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<div class="ttb-message">
									<span>
										<spring:message code="LB.D1583" />
									</span>
								</div>
								<!-- 交易時間-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.Trading_time" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span> ${BaseResultData.CMQTIME}</span>
										</div>
									</span>
								</div>
								<!-- 身分證字號/統一編號-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.Id_no" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${BaseResultData.hideid_CUSIDN}</span>
										</div>
									</span>
								</div>
								<!--信託帳號-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.W0944" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${BaseResultData.hideid_CDNO }</span>
										</div>
									</span>
								</div>
								<!--基金名稱-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.W0025" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${BaseResultData.FUNDLNAME}</span>
										</div>
									</span>
								</div>

								<!--類別-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.D0973" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${BaseResultData.str_AC202}</span>
										</div>
									</span>
								</div>
								<!--信託金額-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.W0026" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												<c:if test="${BaseResultData.FUNDAMT != '0.00'}">
													${BaseResultData.display_FUNDCUR }
													${BaseResultData.display_FUNDAMT }
												</c:if>

											</span>
										</div>
									</span>
								</div>
								<!-- 每次定額申購金額＋<br>不定額基準申購金額  -->

								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												${BaseResultData.show_PAYAMT}
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												<c:if test="${BaseResultData.PAYAMT != '0'}">
													${BaseResultData.display_PAYCUR }
													${BaseResultData.PAYAMT1 }
												</c:if>

											</span>
										</div>
									</span>
								</div>
								<!--停利點-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.W1174" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.NSTOPPROF}%
											</span>
										</div>
									</span>
								</div>
								<!--停損點-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.W1175" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
											-${BaseResultData.NSTOPLOSS}%
											</span>
										</div>
									</span>
								</div>
							</div>
							<!-- button -->
								<!-- 列印 -->
								<input class="ttb-button btn-flat-orange" TYPE="button" id="printbtn" onclick="print()"
									value="<spring:message code="LB.Print" />" />
							<!-- button -->
						</div>
					</div>
					<div class="text-left">
						<!-- 						說明： -->
						<!-- <spring:message code="LB.Description_of_page"/>: -->
					</div>
				</form>
			</section>
			<!-- main-content END -->
		</main>
	</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>