<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
	$(document).ready(function() {
		window.print();
	});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br />
	<br />
	<div style="text-align: center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br />
	<br />
	<br />
	<div style="text-align: center">
		<font style="font-weight: bold; font-size: 1.2em">${jspTitle}</font>
	</div>
	<br />
	<br />
	<br />
	<label><spring:message code="LB.Inquiry_time" /> <!-- 查詢時間 -->：</label>
	<label>${CMQTIME}</label>
	<br />
	<label><spring:message code="LB.Inquiry_period_1" /> <!-- 查詢期間 -->：</label>
	<label>${CMPERIOD}</label>
	<br />
	<label><spring:message code="LB.Total_records" /> <!-- 資料總數 -->：</label>
	<label>${COUNT}　<spring:message code="LB.Rows" /></label>
	<br />
	<table class="print">
		<tr>
			<th><spring:message code= "LB.W0147" /></th>
			<th data-breakpoints="xs sm"><spring:message code= "LB.W0148" /></th>
			<th data-breakpoints="xs sm"><spring:message code= "LB.Transaction_Number" /></th>
			<th data-breakpoints="xs sm"><spring:message code= "LB.W0150" /></th>
			<th data-breakpoints="xs sm"><spring:message code= "LB.W0151" />/<spring:message code= "LB.W0152" />/<spring:message code= "LB.W0153" /></th>
			<th><spring:message code= "LB.W0155" /></th>
			<th data-breakpoints="xs sm"><spring:message code= "LB.W0156" /></th>
			<th data-breakpoints="xs sm"><spring:message code="LB.Exchange_rate" /></th>						
		</tr>
		<c:forEach var="dataList" items="${print_datalistmap_data.REC}">
		<tr>
			<td style="text-align:center">${dataList.RREGDATE}</td>
			<td style="text-align:center">${dataList.RVALDATE}</td>
	        <td style="text-align:center">${dataList.RREFNO}</td>
			<td style="text-align:center">${dataList.MONEY}</td>
			<td style="text-align:center">${dataList.RORDCUS1}<br>${dataList.RBENAC}<br>${dataList.RAWB1AD1}</td>
			<td style="text-align:center">${dataList.DETCHG}</td>
			<td style="text-align:center">${dataList.MEMO1}</td>
			<td style="text-align:right">${dataList.RRATE}</td>				               
		</tr>
		</c:forEach>
	</table>
	<br />
	<br />
	<!-- 匯款金額小計 -->
	<p>
		<!--TODO is8n -->
		<spring:message code= "LB.W0157" />:
	</p>
	<c:forEach var="dataList"
		items="${ print_datalistmap_data.CURRANCY_AMOUNT}">
		<p>
			<!--LB.W0158->共計 -->
			${dataList.RREMITCY} ${dataList.RREMITAM} <spring:message code="LB.W0158" /> ${dataList.temp_mount}
			<!--LB.Rows->筆 -->
			<spring:message code="LB.Rows" />
		</p>
	</c:forEach>
	<p>
		<br /> <br />
</body>
</html>