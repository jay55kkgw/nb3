<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
<script type="text/javascript" src="${__ctx}/js/dataTables.rowsGroup.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	// HTML載入完成後開始遮罩
	setTimeout("initBlockUI()", 50);
	// 開始跑下拉選單並完成畫面
	setTimeout("init()", 400);
	// 解遮罩
	setTimeout("unBlockUI(initBlockId)", 500);
	
	jQuery(function($) {
		$('.dtable').DataTable({
			scrollX: true,
			sScrollX: "99%",
			scrollY: true,
			bPaginate: false,
			bFilter: false,
			bDestroy: true,
			bSort: false,
			info: false,
			rowsGroup: [0, 1, 2],
		});
	});
});

function init(){
	
// 	$("#printbtn").click(function(){
// 		var params = {
// 			"jspTemplateName":"predesignated_account_cancellation_print",
// 			"jspTitle":"變更外匯進口／出口／匯兌通訊地址／電話",
// 			"CMQTIME":"${dataSet.CMQTIME}",
// 			"MSG":"${dataSet.MSG}",
// 			"CCY":"${dataSet.CCY}",
// 			"CUSIDN":"${dataSet.CUSIDN}",
// 			"BNKCOD":"${dataSet.BNKCOD}",
// 			"ACN":"${dataSet.ACN}"
// 		};
// 		openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
// 	});
	
}

</script>

</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

	<!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 通訊資料變更     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0359" /></li>
    <!-- 外匯進口/出口/匯兌通訊地址/電話變更     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0385" /></li>
		</ol>
	</nav>
		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 --> 
 		<div class="content row">
 		   <%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
 		
 		<main class="col-12">	

		<!-- 		主頁內容  -->

			<section id="main-content" class="container">
		
				<h2><spring:message code="LB.D0385" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<div class="main-content-block row">
					<div class="col-12 tab-content printClass"> 
						<div class="ttb-input-block">
                        	<div class="ttb-message">
								<!-- 變更成功 -->
                                <span><spring:message code= "LB.D0399" /></span>
                            </div>
                        </div>
						<!-- 線上約定轉入帳號註銷表 -->
						<table class="stripe table ttb-table dtable m-0" data-show-toggle="first">
							<thead>
								<tr>
									<th rowspan="2"><spring:message code="LB.D0375" /></th>
									<th colspan="2"><spring:message code="LB.D0376" /></th>
									<th rowspan="2"><spring:message code="LB.Telephone" /></th>
								</tr>
								<tr style="display:none">
									<th></th>
									<th></th>
								</tr>
							</thead>
								<c:set var="dataList" value="${fcy_data.data.sessionRec}" />
	                        <tbody>
                                <tr>
                                    <td>${dataList.BCHNAME}(${dataList.BCHID})</td>
                                    <td><spring:message code="LB.D0392" /></td>
                                    <td><span class="sec-title"><spring:message code="LB.D0394" />:</span><br>
                                    	${dataList.CUSAD1C}
                                    	${dataList.CUSAD2C}
                                    </td>
                                    <td><span class="sec-title">TEL:</span><br>
                                    	${dataList.CUSTEL1}
                                    </td>
                                </tr>
                                <tr>
                                	<td>${dataList.BCHNAME}(${dataList.BCHID})</td>
                                    <td><spring:message code="LB.D0392" /></td>
                                    <td><span class="sec-title"><spring:message code="LB.D0396" />:</span><br>
                                    	${dataList.CUSADD1}
                                    	${dataList.CUSADD2}
                                    	${dataList.CUSADD3}
                                    </td>
                                    <td><span class="sec-title">FAX</span><br>
                                    	${dataList.CUSTEL2}
                                    </td>
                                </tr>
	                         <c:set var="dataSet" value="${fcy_data.data.dataSet}" />
                                <tr>
                                    <td>${dataSet.BCHNAME}(${dataSet.BCHID})</td>
                                    <td><spring:message code="LB.D0393" /></td>
                                    <td><span class="sec-title"><spring:message code="LB.D0394" />:</span><br>
                                    	${dataSet.CUSAD1C}
                                    	${dataSet.CUSAD2C}
                                    </td>
                                    <td><span class="sec-title">TEL:</span><br>
                                    	${dataSet.CUSTEL1}
                                    </td>
                                </tr>
                                <tr>
                                	<td>${dataSet.BCHNAME}(${dataSet.BCHID})</td>
                                    <td><spring:message code="LB.D0393" /></td>
                                    <td><span class="sec-title"><spring:message code="LB.D0396" />:</span><br>
                                    	${dataSet.CUSADD1}
                                    	${dataSet.CUSADD2}
                                    	${dataSet.CUSADD3}
                                    </td>
                                    <td><span class="sec-title">FAX</span><br>
                                    	${dataSet.CUSTEL2}
                                    </td>
                                </tr>
	                         </tbody>
						</table>
						<br>
<%-- 					<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/> --%>
					</div>
				</div>
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Note" /></p> 
					<li><span><spring:message code="LB.Fcy_Data_P4_D1" /></span></li>
					<li><span><spring:message code="LB.Fcy_Data_P4_D2" /></span></li>
				</ol>
			</section>			
			<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>

</body>
</html>