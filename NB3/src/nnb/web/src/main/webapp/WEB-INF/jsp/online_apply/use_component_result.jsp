<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript"src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript"src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript"src="${__ctx}/js/jquery.datetimepicker.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		init();
		initKapImg();
	});
	function init() {
		// 確認鍵 click
		$("#CMSUBMIT").click( function(e) {
			window.open('${__ctx}/login');
		});
	}
</script>
</head>

<body>
<%@ include file="../component/trading_component.jsp"%>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	<!-- 交易機制所需畫面 -->
	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Register" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<!-- 晶片金融卡申請網路銀行 -->
			<h2><spring:message code= "LB.X1077" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="${__ctx}/ONLINE/APPLY/use_component_confirm">
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
							
							<!--交易時間-->
							<div class="ttb-input-item row">
								<span class="input-title">
                                    <label>
										<h4> <spring:message code= "LB.D0019" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
										<span>${use_component_result.data.CCTXTIME}</span>
									</div>               
                                </span>
                            </div> 
                            
							 <!--存款帳號-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D0988" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
										<span>${use_component_result.data._accNo}</span>
									</div>               
                                </span>
                            </div> 
                                
							 <!--電子郵箱-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D0346" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
										<span>${use_component_result.data.MAIL}</span>
									</div>               
                                </span>
                            </div> 
							<c:if test="${use_component_result.data.NOTIFY_ACTIVE == 'Y'}">
							<!--通知項目-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D1016" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
										<label for="NOTIFY_ACTIVE" >
											<span class="ttb-unit"> <spring:message code= "LB.D0997" /></span>
										</label>
                                    </div>               
                                </span>
                            </div> 
							</c:if>

							
							 <!--服務分行-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code= "LB.D0998" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
										<span>${use_component_result.data.BRHNAME}</span>
									</div>               
                                </span>
                            </div> 
							
							 <!--密碼-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D1019" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
                                    <!-- 請以原設定之使用者名稱及密碼登入網路銀行使用 -->
										<span><spring:message code= "LB.D1020" /> </span>	
                                    </div>               
                                </span>
                            </div> 
							
                        </div>
                       <!-- 立即登入 -->
                        <input class="ttb-button btn-flat-orange" name="CMSUBMIT" type="button" value="<spring:message code= "LB.X1083" />" onclick="window.open('${__ctx}/login')"/>
                        
                    </div>
                </div>
                <div class="text-left">
						<ol class="description-list list-decimal">
							<p><spring:message code="LB.Description_of_page" /></p>						
							<li> <spring:message code= "LB.Use_Component_P7_D1" /></li>
						</ol>
				</div>
			</form>
		</section>
		</main>
	<!-- 		main-content END -->
	<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>

</html>