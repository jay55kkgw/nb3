<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			// 表單驗證
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
			// 初始化
			init();
		});
		
		function init() {
			$("#CMSUBMIT").click( function(e) {
				console.log("submit~~");

				var main = document.getElementById("formId");
				$('#CUSIDN').val($('#CUSIDN').val().toUpperCase());
				$('#UID').val($('#CUSIDN').val());

				// 表單驗證
				if ( !$('#formId').validationEngine('validate') ) {
					e = e || window.event; // for IE
					e.preventDefault();
				} else {
					initBlockUI(); //遮罩
					$("#formId").attr("action", "${__ctx}/DIGITAL/ACCOUNT/financial_card_confirm_step1");
					$("#formId").submit();
				}
				
			});
			
			$("#CUSIDN").focus();
		}

	</script>
</head>

<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Register" /></li>
    <!-- 數位存款帳戶晶片金融卡確認領用申請及變更密碼     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1535" /></li>
		</ol>
	</nav>

	
	<!-- menu、登出窗格 -->
	<div class="content row">
	        <main class="col-12">
            <section id="main-content" class="container">
                <h2><spring:message code="LB.D1535"/></h2>
                <div id="step-bar">
                    <ul>
                        <li class="active">身分驗證</li>
                        <li class="">變更密碼</li>
                        <li class="">完成申請</li>
                    </ul>
                </div>
                
				<form id="formId" method="post" action="">
					<input type="hidden" id="UID" name="UID" value="">
					
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<div class="ttb-message">
									<p>身份驗證</p>
								</div>
								<p class="form-description">請您輸入您的身分證字號與開戶認證的方式。</p>
					
								<!-- 身分證字號 -->
					    	<div class="ttb-input-item row">
					        <span class="input-title">
					            <label>
					                <h4>身分證字號</h4>
					            </label>
					        </span>
					        <span class="input-block">
					            <div class="ttb-input">
					                <input id="CUSIDN" name="CUSIDN" type="text" size="10" maxlength="10" value="" placeholder="請輸入身分證字號" 
					                	class="text-input validate[required, funcCall[validate_checkSYS_IDNO[CUSIDN]]]"
					                	/>
					            </div>
					        </span>
					    </div>
					</div>
					<input type="button" class="ttb-button btn-flat-gray" value="返回上一頁" name="" onclick="window.close();">
					        <input type="button" id="CMSUBMIT" name="CMSUBMIT" class="ttb-button btn-flat-orange" value="下一步" name="">
					
					    </div>
					</div>
				</form>
				
                <ol class="list-decimal description-list"></ol>

            </section>
        </main>
	</div>
	
	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>