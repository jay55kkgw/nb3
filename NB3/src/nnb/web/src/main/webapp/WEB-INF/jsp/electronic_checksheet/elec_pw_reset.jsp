<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 電子對帳單     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1396" /></li>
    <!-- 密碼重置     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0296" /></li>
		</ol>
	</nav>

	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">

		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
	

		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2>
				<spring:message code="LB.D0296" /><!-- 密碼重置 -->
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" action="${__ctx}/ELECTRONIC/CHECKSHEET/elec_pw_reset_result" method="post">
			<input type="hidden" id="PINNEW" name="PINNEW"  value="">
			<input type="hidden" name="Cust_id" value="">
   			<input type="hidden" name="Status" value="IR">
   			<input type="hidden" name="Email" value="${sessionScope.dpmyemail}">
   			<input type="hidden" name="sys_ip" value="">
				<div class="main-content-block row">
					<div class="col-12 tab-content" id="nav-tabContent">
						<!-- 主頁內容  -->
						<div class="ttb-input-block">
							<div class="ttb-input-item row">
								<span class="input-title"> <label><h4><spring:message code="LB.D0169" /><!-- 申請項目 -->：</h4></label></span> <!-- 系統時間 -->
								<span class="input-block">
									<div class="ttb-input">
										<spring:message code="LB.D0296" /><!-- 密碼重置 -->
									</div>
								</span>
							</div>
							
							<div class="ttb-input-item row">
								<span class="input-title"> <spring:message code="LB.Transaction_security_mechanism" /> </span> <!--交易機制-->
								<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block">
										<input type="radio" name="FGTXWAY" value="0" checked>
										<span><spring:message code="LB.SSL_password"/></span>
										<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<input type="password" name="CMPASSWORD" id="CMPASSWORD" maxlength="8" value="" class="text-input validate[required,custom[onlyLetterNumber]]"
											placeholder="<spring:message code="LB.Please_enter_password" />">
									</div>
								</span>
							</div>
						</div>
						<input type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />"/>
								<!-- 重新輸入 -->
						<input type="submit" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
								<!-- 確定 -->	
					</div>
				</div>
			</form>
		</section>
		</main>
	</div>
				
	<%@ include file="../index/footer.jsp"%>
	 <script type="text/javascript">
	    $(document).ready(function() {
			init();
		});
	    function init(){
			$("#formId").validationEngine({binded:false,promptPosition: "inline" });
			$("form").submit(function(e){
				console.log("submit~~");
				if('${sessionScope.dpmyemail}'==''){
					//alert("<spring:message code= "LB.EmailAlert" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.EmailAlert' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
					e.preventDefault();
				}else if(!$('#formId').validationEngine('validate')){
		        	e.preventDefault();
	 			}else{
	 				$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
	 				initBlockUI();
	 				$("#formId").validationEngine('detach');
	 			}
		    })
	    }
	    
	 </script>
</body>
</html>