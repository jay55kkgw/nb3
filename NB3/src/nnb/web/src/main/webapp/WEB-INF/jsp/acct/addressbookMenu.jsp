<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>
<c:set var="rec" value="${Addressbook.data.REC}"></c:set>
<script type="text/javascript">
	//Client Cross Frame Scripting Attack
	if (top != self) top.location=encodeURI(self.location)

	$(document).ready(function() {
		init();
		setTimeout("initDataTable()",100);
		setTimeout("dataShowAll()",100);
	});
	
	function dataShowAll() {
        jQuery(function($) {
    		$('.dtablet').DataTable({
    			scrollX: true,
    			sScrollX: "99%",
    			scrollY: true,
    			bPaginate: false,
    			bFilter: false,
    			bDestroy: true,
    			bSort: false,
    			info: false,
    		});
    	});
	}
	
	function init()
	{
		//initFootable();
		var n = ${fn:length(rec)};
		if (typeof(n) === 'undefined' || n == null || n == 0)
		{
			// 查無資料
			//alert('<spring:message code="LB.Check_no_data" />');
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Check_no_data' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);

		}
	}
	
	function fillData() 
	{
		var checkBoxList = document.getElementsByName('CHECKBOX');
		var sAddressList = '';

		for (var i = 0; i < checkBoxList.length; i++)
		{
			console.log("checkBoxList[i].checked>>"+checkBoxList[i].checked)
			if (checkBoxList[i].checked)
			{
				sAddressList += (checkBoxList[i].getAttribute('text')+";");
			}
		}
		var last = sAddressList.lastIndexOf(";");
// 		console.log("last>>"+last)
// 		console.log("sAddressList.length>>"+sAddressList.length)
		if(last == (sAddressList.length -1)){
			sAddressList = sAddressList.substr(0,last);
		}
		
// 		console.log("sAddressList>>"+sAddressList);
		if (sAddressList.length > 500)
		{
			// 所勾選的Mail地址超過限制範圍，請減少勾選項目			
			//alert('<spring:message code="LB.Check_Mail_data" />');
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Check_Mail_data' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
			return;
		}
		
		var oMail = self.opener.document.getElementById('CMTRMAIL');
		if (oMail != undefined)
		{
			oMail.value = sAddressList;
		}

		self.close();
	}
	
	function selectAll()
	{	
		var obj = document.getElementsByName('CHECKBOX');

		for(var i = 0; i < obj.length; i++)
		{
		    if(obj[i].type == 'checkbox')
		    {
				obj[i].checked = true;
		    }
		}
	} 
</script>
<title><spring:message code="LB.Title" /></title>
</head>
<body>
<div class="content row">
	<main class="col-12"> 
		<!-- 主頁內容  -->
		<section id="main-content" class="container">
			<form method="post" id="formId" action="">
				<!-- 我的通訊錄 -->
				<h2><spring:message code="LB.My_Address_Book" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<ul class="ttb-result-list"></ul>
						<table class="stripe table-striped ttb-table dtablet"  data-show-toggle="first">
							<thead>
							<tr>
								<!-- 勾選 -->
								<th data-title='<spring:message code="LB.Check"/>'><spring:message code="LB.Check" /></th>
								<!-- 好記名稱 -->
								<th data-title='<spring:message code="LB.Favorite_name"/>'><spring:message code="LB.Favorite_name" /></th>
								<!-- 電子郵箱 -->
								<th data-title='<spring:message code="LB.Mail_address"/>'><spring:message code="LB.Mail_address" /></th>
							</tr>
							</thead>
							<c:if test="${empty Addressbook.data.REC}">
								<tbody>
									<tr>
										<td></td><td></td><td></td>
									</tr>
								</tbody>
							</c:if>
							<c:if test="${not empty Addressbook.data.REC}">
								<tbody>
								<c:forEach var="row" items="${Addressbook.data.REC}" >
									<tr>
										<td class="text-center">
											<label class="check-block">&nbsp;
												<input type="checkbox" name="CHECKBOX" text="${row.DPABMAIL}"/>
												<span class="ttb-check"></span>
											</label> 
										</td>
										<td class="text-center">${row.DPGONAME}</td>
										<td class="text-center">${row.DPABMAIL}</td>
									</tr>
								</c:forEach>
								</tbody>
							</c:if>
						</table>
						<!-- 全選 -->
						<input type="button" value="<spring:message code="LB.All_select" />" onClick="selectAll()" class="ttb-button btn-flat-orange"/>  
						<!-- 確定 -->
						<input type="button" name="CMSUBMIT" id="CMSUBMIT" value="<spring:message code="LB.Confirm" />" onClick="fillData()" class="ttb-button btn-flat-orange"/>
					</div>
				</div>
				</form>	
			</section>
		</main> <!-- main-content END -->
	</div>
</body>
</html>