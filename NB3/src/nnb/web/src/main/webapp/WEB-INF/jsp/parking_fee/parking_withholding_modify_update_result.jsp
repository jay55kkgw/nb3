<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
	
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>	
</head>
 <body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>

	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 自動扣繳服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2360" /></li>
    <!-- 停車費     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.D0330" /></li>
    <!-- 資料維護     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0285" /></li>
		</ol>
	</nav>

 	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
	<main class="col-12"> 
	<!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<h2><spring:message code="LB.X0285"/></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>						
			<p style="text-align: center;color: red;"><spring:message code="LB.X0293"/></p>
			<form method="post" id="formId">
				<input type="hidden" id="back" name="back" value="">
				<input type="hidden" id="TOKEN" name="TOKEN" value="${input_data.TOKEN}" /><!-- 防止重複交易 -->
                
				
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
						
							<!-- 交易種類 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.X0279"/>
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p><spring:message code="LB.X0294"/></p>
									</div>
								</span>
							</div>
							
							<!-- 交易種類 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.Trading_time"/>
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>${ result_data.data.CMQTIME }</p>
									</div>
								</span>
							</div>
							<!-- 交易種類 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.W0702"/>
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>${ input_data.Account }</p>
									</div>
								</span>
							</div>
							<!-- 交易種類 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.D0059"/>
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>${ input_data.ZoneCode_str }</p>
									</div>
								</span>
							</div>
							<!-- 台幣轉出帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0342"/>
										</h4>
									</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<p>${ input_data.CarId }</p>
									</div>
								</span>
							</div>
							
							<!-- 確認新使用者名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0343"/>
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										${ input_data.CarKind_str }
									</div>
								</span>
							</div>
							
							<!-- 確認新使用者名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0020"/>
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p><spring:message code="LB.X0278"/></p>
									</div>
								</span>
							</div>
							
							
							<!-- 確認新使用者名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.Mail_address"/>
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p> ${ input_data.Email }</p>
									</div>
								</span>
							</div>
							
							
							<!-- 確認新使用者名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0069"/>
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p> ${ input_data.Phone }</p>
									</div>
								</span>
							</div>
							
							
							<!-- 交易種類 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.X0281"/>
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>${ result_data.data.RespCode }-${ result_data.data.RespString }</p>
									</div>
								</span>
							</div>
						</div>
                        <!-- 列印  -->
                        <spring:message code="LB.Print" var="printbtn"></spring:message>
                        <input type="button" id="printbtn" value="${printbtn}" class="ttb-button btn-flat-orange" />		
						<!--回上頁 -->
<!--                         <input type="button" name="CMBACK" id="CMBACK" value="繼續申請" class="ttb-button btn-flat-gray">					 -->
					</div>
				</div>
			</form>
		</section>
		<!-- 		main-content END -->
	</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
	<!--   Js function -->
    <script type="text/JavaScript">
		$(document).ready(function() {
			init();
		});
		
	    function init(){
	    	//列印 資料維護
        	$("#printbtn").click(function(){
				var params = {
					"jspTemplateName":"parking_withholding_modify_update_result_print",
					"jspTitle":'<spring:message code= "LB.X0285" />',
					"CMQTIME":"${result_data.data.CMQTIME}",
					"Account":"${input_data.Account}",
					"ZoneCode":"${input_data.ZoneCode_str}",
					"CarId":"${input_data.CarId}",
					"CarKind":"${input_data.CarKind_str}",
					"Email":"${input_data.Email}",
					"Phone":"${input_data.Phone}",
					"RespString":"${result_data.data.RespString}",
					"RespCode":"${result_data.data.RespCode}"
				};
				openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
    		//上一頁按鈕
    		$("#CMBACK").click(function() {
    			var action = '${__ctx}/PARKING/FEE/parking_withholding_apply';
    			$('#back').val("Y");
    			$('#formId').attr("action", action);
    			$('#formId').submit();
    		});
	    }	
	    

 	</script>
</body>
</html>
 
