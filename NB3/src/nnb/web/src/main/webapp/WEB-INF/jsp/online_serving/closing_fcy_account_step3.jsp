<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
		//表單驗證
		$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
		// 確認鍵 click
		 submit();
		
		var tempAMTDHID ='${closing_fcy_account_step3.data.tempAMTDHID}';
		var FYACN = '${closing_fcy_account_step3.data.FYACN}';
		//轉出金額 OR 稅後本息
		if(tempAMTDHID==0 || FYACN.substring(0,3) == "893"){
			$('#rd1').attr("disabled",true);
		}
		if(tempAMTDHID==0 ){
			$('#rd2').attr("disabled",true);
			$('#FXQREMITNO').attr("disabled",true);
		}
		
	}
	
	// 確認鍵 Click
	function submit() {
		$("#CMSUBMIT").click(function (e) {
			if (!$('#formId').validationEngine('validate')) {
				e = e || window.event; // for IE
				e.preventDefault();
			} else {
				$("#formId").validationEngine('detach');
				processQuery();
			}
		});
	}
	
       
	//
	function processQuery()
	{
		var main = document.getElementById("formId");
		var FYACN = '${closing_fcy_account_step3.data.FYACN}';
	   	var AMTDHID ='${closing_fcy_account_step3.data.tempAMTDHID}';
	   	if(AMTDHID>0)
	   	{
	   		if(main.rd1[0].checked)
	   		{   	
// 	   			if (!CheckSelect("台幣帳號", main.FYACN1, "#")) 
// 	   				return false;
	   			main.TSFAN.value=main.FYACN1.options[main.FYACN1.selectedIndex].value;
	   			main.IN_CRY.value="TWD";
	   			main.CURCODE.value="TWD";	
		   	 	var action = '${__ctx}/ACCOUNT/CLOSING/closing_fcy_account_step4_r';
				$("#formId").attr("action", action);
	   		}
	   		if(main.rd1[1].checked)
	   		{   	
// 	   			if (!CheckSelect("外幣帳號", main.FYACN2, "#")) 
// 	   				return false;
	   			main.TSFAN.value=main.FYACN2.options[main.FYACN2.selectedIndex].value;
	   			if(FYACN==main.TSFAN.value)
	   			{
	   				//alert("<spring:message code="LB.Alert144"/>");
	   				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert144' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
	   				return false;
	   			}
	   			main.IN_CRY.value=main.CRY.value;
	   			main.CURCODE.value=main.CRY.value;
	   			main.ATRAMT.value='${closing_fcy_account_step3.data.AMTDHID}';	
	   			var action = '${__ctx}/ACCOUNT/CLOSING/closing_fcy_account_confirm';
				$("#formId").attr("action", action);
	   			
	   		}
	   		if(!main.rd1[0].checked && !main.rd1[1].checked)
	   		{
	   			//alert("<spring:message code="LB.Alert145"/>");
	   			errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert145' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
	   			return false;
	   		}
	   		if (main.SRCFUNDDESC.value == "" || main.SRCFUNDDESC.value == "<spring:message code="LB.Please_select_code"/>") {
				//alert("<spring:message code="LB.Alert146"/>");   		
	   	    	errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert146' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
				return false;   				
			}
		}
		else
		{
	   			main.TSFAN.value="00000000000";
	   			main.IN_CRY.value=main.CRY.value;
	   			main.CURCODE.value=main.CRY.value;	
	  			main.ATRAMT.value='${closing_fcy_account_step3.data.AMTDHID}';
				var action = '${__ctx}/ACCOUNT/CLOSING/closing_fcy_account_confirm';
				$("#formId").attr("action", action);
		}
		var inacc=main.TSFAN.value;
		if((FYACN.substring(0 ,3)=="893" && inacc.substring(0 ,3)!="893") || (FYACN.substring(0 ,3)!="893" && inacc.substring(0 ,3)=="893"))	   		
	   	{	
	   		//alert("<spring:message code="LB.Alert147"/>");
	   		errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert147' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
	   		return false;
	   	}
	   	main.SRCFUNDDESC.disabled = false;
	 	$("#formId").submit();
		return false;	
	}
	
	//
	function change()
	{	
		var main = document.getElementById("formId");
		if(main.rd1[0].checked)
		{
			main.FYACN1.disabled=false;
			main.FYACN2.value="#";
			main.FYACN2.disabled=true;
			
			document.getElementById("SRCFUNDDESC").value = "<spring:message code="LB.Please_select_code"/>";
			main.SRCFUND.value="";
	 	}	
		if(main.rd1[1].checked)
		{
			main.FYACN2.disabled=false;
			main.FYACN1.value="#";
			main.FYACN1.disabled=true;
	 		document.getElementById("SRCFUNDDESC").value = "<spring:message code="LB.Please_select_code"/>";
			main.SRCFUND.value="";
		}	
	}      
	//
	  function openMenu() {
		  var main = document.getElementById("formId");
		       
	    var payccy = main.CRY.value;
	   	if(!main.rd1[0].checked && !main.rd1[1].checked)
	   	{
	   		//alert("<spring:message code="LB.Alert145"/>");
	   		errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert145' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
	   		return false;
	   	}	    
	    var remitccy = "";	  
	    var outacc = '${closing_fcy_account_step3.data.FYACN}';
	    var inacc = "";	  
	    var type;
	    if(main.rd1[0].checked)
	    {
   	
// 	    	if (!CheckSelect("台幣帳號", main.FYACN1, "#")) 
// 	   			return false; 
	   			
	   		inacc=main.FYACN1.options[main.FYACN1.selectedIndex].value;	 
	   		main.IN_CRY.value="TWD";
	   		main.CURCODE.value="TWD";
	   		remitccy=main.IN_CRY.value;	
	   		  	
	    }
	    if(main.rd1[1].checked)
	    {

// 	    	if (!CheckSelect("外幣帳號", main.FYACN2, "#")) 
// 	   			return false;    	
	    	
	    	inacc=main.FYACN2.options[main.FYACN2.selectedIndex].value;	
	   		main.IN_CRY.value=main.CRY.value;
	   		main.CURCODE.value=main.CRY.value;	
	    	remitccy=main.IN_CRY.value;	    	   	
	    }
		// {D-D}
	   	if ((outacc.substring(0 ,3) != '893') && (inacc.substring(0 ,3) != '893')) {
	   		if (((payccy != '#') && (payccy != 'TWD')) && (remitccy == 'TWD')) 
				type = '2';    			
	   		else
				type = '1'; 
	   	}
	   	// {O-D}
	   	else if ((outacc.substring(0 ,3) == '893') && (inacc.substring(0 ,3) != '893')) 
	   		type = '2';  
	   	// {D-O,O-O}	
	   	else 
	   		type = '1';  
	   	if ( ((outacc.substring(0 ,3) != '893') && (inacc.substring(0 ,3) != '893')) || 
	   		 ((outacc.substring(0 ,3) == '893') && (inacc.substring(0 ,3) == '893')) ) {   	
			main.SRCFUND.value = '';
			document.getElementById("SRCFUNDDESC").value = '';		
			main.FIXEDFLAG.value = 'N';	
			main.NULFLAG.value = 'N';
			if (payccy == remitccy) { //f-f
			 	//SPC
			   	main.FIXEDFLAG.value = 'Y';
	   		}
			else { 		
			   if ((payccy != 'TWD') && (remitccy != 'TWD')) {
			   
			   	   if (inacc == outacc) {	
					   $("#SRCFUND").val('694');
					   $("#SRCFUNDDESC").val('694');
				   }	
				   else {
			 	   	   //SPC
				   	   main.FIXEDFLAG.value = 'Y';
				   }
			   }	
			   else {
			       //NUL
			       main.NULFLAG.value = 'Y';
			   }
	   		} 						     			
		}
	    else {	
			main.NULFLAG.value = 'N';	
			main.FIXEDFLAG.value = 'N';	
		}	
	  	if (main.FIXEDFLAG.value == 'Y')
	  		window.open('${__ctx}/FCY/TRANSFER/fxremitquery_fix');
		else
			window.open('${__ctx}/FCY/COMMON/FxRemitQuery?' + 'NULFLAG=' + main.NULFLAG.value
				+ '&ADRMTTYPE=' + type + '&CUTTYPE=${closing_fcy_account_step3.data.str_CUSTYPE}');
	  }  

</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 外匯存款帳戶結清銷戶申請    -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0299" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.X0299" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="">
				 <c:set var="BaseResultData" value="${closing_fcy_account_step3.data}"></c:set>
				<input type="hidden" name="ADOPID" value="F017">
				<input type="hidden" name="previousPageJson" value='${BaseResultData.previousPageJson}'>
				<input type="hidden" name="FYACN" id="FYACN" value='${BaseResultData.FYACN}'>
				<input type="hidden" name="OUT_CRY" id="OUT_CRY" value='${BaseResultData.OUT_CRY}'>
				<input type="hidden" name="PAIAFTX" id="PAIAFTX" value='${BaseResultData.AMTDHID}'>
				<input type="hidden" name="TSFAN" id="TSFAN" value="">
				<input type="hidden" name="IN_CRY" id="IN_CRY" value="">
				<input type="hidden" name="CRY" id="CRY" value='${BaseResultData.OUT_CRY}'>
				<input type="hidden" name="AMTDHID" id="AMTDHID" value='${BaseResultData.AMTDHID}'>
				<input type="hidden" name="CURCODE" id="CURCODE" value="">
				<input type="hidden" name="ROE" id="ROE" value='${BaseResultData.ROE}'>
				<input type="hidden" name="SFXGLSTNO" id="SFXGLSTNO" value='${BaseResultData.SFXGLSTNO}'>
				<input type="hidden" name="INT" id="INT" value='${BaseResultData.INT}'>
				<input type="hidden" name="FYTAX" id="FYTAX" value='${BaseResultData.FYTAX}'>
				<input type="hidden" name="FYNHITAX" id="FYNHITAX" value='${BaseResultData.FYNHITAX}'>
				<input type="hidden" name="ATRAMT" id="ATRAMT" value="">
				<input type="hidden" name="FIXEDFLAG" id="FIXEDFLAG" value="N">
				<input type="hidden" name="SRCFUND" id="SRCFUND" value="">
				<input type="hidden" name="NULFLAG" id="NULFLAG" value="N">
				<input type="hidden" name="NAME" id="NAME" value='${BaseResultData.NAME}'>
				<input type="hidden" name="CUSTYPE" id="CUSTYPE" value='${BaseResultData.str_CUSTYPE}'>
				<!-- 表單顯示 -->
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
                             <div class="ttb-message">
                                <span>
                                
                                </span>
                            </div>
                            <!--轉存入本行帳號-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.D0458"/></h4>
                                    </label></span>
                                <span class="input-block">
                                    <div class="ttb-input">
										 <label class="radio-block"><spring:message code="LB.D0459"/>
                                            <input type="radio" name="rd1" id="rd1" onclick="change()">
                                            <span class="ttb-radio"></span>
                                        </label>
									</div>
									 <div class="ttb-input">
									 <select class="custom-select select-input half-input validate[required]" name="FYACN1" id="FYACN1" disabled>
												<!--  請選擇帳號 -->
											<option value="">---<spring:message code="LB.Select_account" />---</option>
											<c:forEach var="dataList" items="${BaseResultData.agreeAcnosList}">
												<option value='${dataList.ACN}'> ${dataList.ACN}</option>
											</c:forEach>
										</select>
									</div>
									  <div class="ttb-input">
										 <label class="radio-block"><spring:message code="LB.D0460"/>
                                            <input type="radio" name="rd1" id="rd2" onclick="change()" >
                                            <span class="ttb-radio"></span>
                                        </label>
									</div>
									 <div class="ttb-input">
										<select class="custom-select select-input half-inputselect-input validate[required]" name="FYACN2" id="FYACN2"  disabled>
											<!--  請選擇帳號 -->
											<option value="">---<spring:message code="LB.Select_account" />---</option>
											<c:forEach var="dataList" items="${BaseResultData.acnos1}">
												<option value='${dataList.ACN}'> ${dataList.ACN}</option>
											</c:forEach>
										</select>
									</div>
                                </span>		
                            </div>
							 <!-- 匯款分類編號 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                	<label>
                                        <h4><spring:message code="LB.Remittance_Classification"/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <font id="SHOW_SRCFUNDDESC"><spring:message code="LB.Please_select_code" /></font>
										<input class="text-input " type="text" name="SRCFUNDDESC" id="SRCFUNDDESC"  style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;">
                                        <button class="btn-flat-orange" type="button" value="<spring:message code= "LB.Menu" />" name="FXQREMITNO" id="FXQREMITNO" onclick="openMenu()"><spring:message code="LB.Menu" /></button>
                                    </div>
                                </span>
                            </div>
						</div>
                        <input class="ttb-button btn-flat-orange" type="button"  name="CMSUBMIT" id="CMSUBMIT" value="<spring:message code="LB.X0080"/>" />
                    </div>
                </div>
				</form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>