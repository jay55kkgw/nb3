<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="../__import_head_tag.jsp" %>
    <%@ include file="../__import_js.jsp" %>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.twzipcode-1.6.1.js"></script>
    <script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
    <!--舊版驗證-->
    <script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
    <!-- DIALOG會用到 -->
    <script type="text/javascript" src="${__ctx}/js/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery-ui.css">
    <style type="text/css">
        #countryTable {
            border: solid 2px;
        }

        #countryTable tr {
            border: solid 2px;
        }

        #countryTable td {
            border: solid 2px;
        }
    </style>
    <script type="text/JavaScript">

        var isTimeout = "${not empty sessionScope.timeout}";
        var countdownObjheader = isTimeout == 'true' ? "${sessionScope.timeout}" : 600;
        var countdownSecHeader = isTimeout == 'true' ? "${sessionScope.timeout}" : 600;

        $(document).ready(function () {
            //HTML載入完成後開始遮罩
            setTimeout("initBlockUI()", 10);
            // 開始查詢資料並完成畫面
            setTimeout("init()", 20);
            //解遮罩
            setTimeout("unBlockUI(initBlockId)", 500);
            //time out
            timeLogout();
        });

        function init() {
            $("#formId").validationEngine({
                binded: false,
                promptPosition: "inline"
            });

            //生日
            <!-- Avoid Reflected XSS All Clients -->
// 		$("#CPRIMBIRTHDAY").val("${result_data.data.requestParam.BIRTHY}/${result_data.data.requestParam.BIRTHM}/${result_data.data.requestParam.BIRTHD}");
            $("#CPRIMBIRTHDAY").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.BIRTHY)}' />/<c:out value='${fn:escapeXml(result_data.data.requestParam.BIRTHM)}' />/<c:out value='${fn:escapeXml(result_data.data.requestParam.BIRTHD)}' />");
            //戶籍地址
            <!-- Avoid Reflected XSS All Clients -->
// 		var PMTADR = "${result_data.data.requestParam.PMTADR}";
            var PMTADR = "<c:out value='${fn:escapeXml(result_data.data.requestParam.PMTADR)}' />";
            if (PMTADR.length >= 3) {
                var PMTADRstring = PMTADR.substring(0, 3);
                $("#CITY1set").val(PMTADRstring);
            }
            if (PMTADR.length >= 6) {
                PMTADRstring = PMTADR.substring(3, 6);
                $("#ZONE1set").val(PMTADRstring);
            }
            //END
            //居住地址
            <!-- Avoid Reflected XSS All Clients -->
// 		var CTTADR = "${result_data.data.requestParam.CTTADR}";
            var CTTADR = "<c:out value='${fn:escapeXml(result_data.data.requestParam.CTTADR)}' />";
            if (CTTADR.length >= 3) {
                var CTTADRstring = CTTADR.substring(0, 3);
                $("#CITY2set").val(CTTADRstring);
            }
            if (CTTADR.length >= 6) {
                CTTADRstring = CTTADR.substring(3, 6);
                $("#ZONE2set").val(CTTADRstring);
            }
            //END
            //年資選單
            //年
            var WORKYEARSHTML = "<option value='#'><spring:message code= 'LB.D0089_2' /></option>";
            for (var x = 0; x < 100; x++) {
                WORKYEARSHTML += "<option value='" + x + "'>" + x + "<spring:message code= 'LB.D0089_2' />" + "</option>";
            }
            $("#WORKYEARS").html(WORKYEARSHTML);
            //月
            var WORKMONTHSHTML = "<option value='#'><spring:message code= 'LB.D0089_3' /></option>";
            for (var x = 0; x < 12; x++) {
                WORKMONTHSHTML += "<option value='" + x + "'>" + x + "<spring:message code= 'LB.D0089_3' />" + "</option>";
            }
            $("#WORKMONTHS").html(WORKMONTHSHTML);
            //END

            dateprocess();
            AddCityOptions1(document.getElementById("CITYCHA"));
            setCITY();
            setCITY1();
            setCITY2();
            countrydata();
            check();
            GO();
            goBack();
            putData();
        }

        function putData() {
            var getback = '${result_data.data.back}';
            if (getback == 'Y') {
                //資料回填
                var oldcardownerB = '${result_data.data.oldcardowner}';
                $("#CPRIMCHNAME").val('${result_data.data.CPRIMCHNAME}');
                $("#CPRIMENGNAME").val('${result_data.data.CPRIMENGNAME}');
// 			$("#BD").text('<spring:message code="LB.D0583" /> ${result_data.data.BIRTHY} <spring:message code="LB.Year" /> ${result_data.data.BIRTHM} <spring:message code="LB.Month" /> ${result_data.data.BIRTHD} <spring:message code="LB.D0586" />');
                $("#BD").text('${result_data.data.CPRIMBIRTHDAYshow}');
                $("#IDnum").text('${result_data.data.CUSIDN}');
                $("#CPRIMID").val('${result_data.data.CUSIDN}');
                $("#CTRYDESC1").val('${result_data.data.CTRYDESC1}');
                $("#MPRIMEDUCATION").val('${result_data.data.MPRIMEDUCATION}');
                $("#MPRIMADDR1COND").val('${result_data.data.MPRIMADDR1COND}');
                $("#CPRIMSALARY").val('${result_data.data.CPRIMSALARY}');
                $("#CPRIMEMAIL").val('${result_data.data.CPRIMEMAIL}');
                $("#CPRIMCELLULANO1").val("${result_data.data.CPRIMCELLULANO1}");
                $("#CPRIMHOMETELNO").val("${result_data.data.CPRIMHOMETELNO}");
                $("#CPRIMHOMETELNO2").val("${result_data.data.CPRIMHOMETELNO2}");
                //居住地址
                $("#ADDR2").val("${result_data.data.ADDR2}");
// 			//戶籍地址
                $("#ADDR1").val("${result_data.data.ADDR2}");
// 			//公司地址
                $("#ADDR4").val("${result_data.data.ADDR4}");

                $("#CPRIMJOBTYPE").val("${result_data.data.CPRIMJOBTYPE}");
                $("#CPRIMJOBTITLE").val("${result_data.data.CPRIMJOBTITLE}");
                $("#CPRIMCOMPANY").val("${result_data.data.CPRIMCOMPANY}");
                $("#CPRIMOFFICETELNO1").val("${result_data.data.CPRIMOFFICETELNO1}");
                $("#WORKYEARS").val("${result_data.data.WORKYEARS}");
                $("#WORKMONTHS").val("${result_data.data.WORKMONTHS}");
                $("#CUSNAME").val("${result_data.data.CUSNAME}");
                $("#ROMANAME").val("${result_data.data.ROMANAME}");
                $("#PAYMONEY").val("${result_data.data.PAYMONEY}");


                var chename = '${result_data.data.CHENAME}';
                if (chename == '1') {
                    $("#CHENAMEN").prop("checked", false);
                    $("#CHENAMEY").prop("checked", true);
                } else {
                    $("#CHENAMEY").prop("checked", false);
                    $("#CHENAMEN").prop("checked", true);
                }
                var MPRIM = '${result_data.data.MPRIMMARRIAGE}';
                if (MPRIM == '1') {
                    $("#MPRIMMARRIAGE1").prop("checked", true);
                } else if (MPRIM == '2') {
                    $("#MPRIMMARRIAGE2").prop("checked", true);
                } else if (MPRIM == '3') {
                    $("#MPRIMMARRIAGE3").prop("checked", true);
                }
                $("#CUSIDN").val("${result_data.data.CUSIDN}");
                $("#CFU2").val("${result_data.data.CFU2}");
                $("#CN").val("${result_data.data.CN}");
                $("#CARDNAME").val("${result_data.data.CARDNAME}");
                $("#CARDMEMO").val("${result_data.data.CARDMEMO}");
                $("#FGTXWAY").val("${result_data.data.FGTXWAY}");
                $("#OLAGREEN1").val("${result_data.data.OLAGREEN1}");
                $("#OLAGREEN2").val("${result_data.data.OLAGREEN2}");
                $("#OLAGREEN3").val("${result_data.data.OLAGREEN3}");
                $("#OLAGREEN4").val("${result_data.data.OLAGREEN4}");
                $("#OLAGREEN5").val("${result_data.data.OLAGREEN5}");
                $("#oldcardowner").val("${result_data.data.oldcardowner}");
                $("#CPRIMBIRTHDAY").val("${result_data.data.CPRIMBIRTHDAY}");
                $("#CPRIMBIRTHDAYshow").val("${result_data.data.CPRIMBIRTHDAYshow}");
                $("#BIRTHY").val("${result_data.data.BIRTHY}");
                $("#BIRTHM").val("${result_data.data.BIRTHM}");
                $("#BIRTHD").val("${result_data.data.BIRTHD}");
                $("#CTTADR").val("${result_data.data.CTTADR}");
                <!-- Avoid Reflected XSS All Clients -->
                //$("#PMTADR").val("${result_data.data.PMTADR}");
                $("#PMTADR").val("<c:out value='${fn:escapeXml(result_data.data.PMTADR)}' />");
                $("#VARSTR2").val("${result_data.data.VARSTR2}");
                $("#QRCODE").val("${result_data.data.QRCODE}");
                var Lcard = "${result_data.data.QRCODE}";
                //獅友會公益卡
                if (Lcard == "L") {
                    $("#partition").val("${result_data.data.partition}");
                    $("#partition").change();
                    $("#branch").val("${result_data.data.branch}");
                    if ('${result_data.data.CARDNAME}' == '85') {
                        $("#memberId").val("${result_data.data.memberId}");
                    }
                }
                //身分證補換發
                $("#CHDATEY").val("${result_data.data.CHDATEY}");
                $("#CHDATEM").val("${result_data.data.CHDATEM}");
                $("#CHDATED").val("${result_data.data.CHDATED}");
                $("#CITYCHA").val("${result_data.data.CITYCHA}");
                $("#CHANTYPE").val("${result_data.data.CHANTYPE}");
                var changetype = ${result_data.data.CHANTYPE}-1;
                $("input[name*='CHTYPE']")[changetype].checked = true;
            } else {
                <!-- Avoid Reflected XSS All Clients -->
// 			var oldcardowner = '${result_data.data.requestParam.oldcardowner}';
                var oldcardowner = "<c:out value='${fn:escapeXml(result_data.data.requestParam.oldcardowner)}' />";
                if (oldcardowner == "Y") {
                    <!-- Avoid Reflected XSS All Clients -->
// 				$("#CPRIMCHNAME").val('${result_data.data.requestParam.USERNAME}');
                    $("#CPRIMCHNAME").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.USERNAME)}' />");
// 				$("#CPRIMEMAIL").val("${result_data.data.requestParam.DPMYEMAIL}");
                    $("#CPRIMEMAIL").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.DPMYEMAIL)}' />");
// 				$("#CPRIMCELLULANO1").val("${result_data.data.requestParam.MOBTEL}");
                    $("#CPRIMCELLULANO1").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.MOBTEL)}' />");
// 				$("#CPRIMHOMETELNO").val("${result_data.data.requestParam.TEL11}${result_data.data.requestParam.TEL12}");
                    $("#CPRIMHOMETELNO").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.TEL11)}' />-<c:out value='${fn:escapeXml(result_data.data.requestParam.TEL12)}' />");
                }
// 			$("#BD").text('${result_data.data.requestParam.CPRIMBIRTHDAYshow}');
                $("#BD").text("<c:out value='${fn:escapeXml(result_data.data.requestParam.CPRIMBIRTHDAYshow)}' />");
// 			$("#IDnum").text('${result_data.data.requestParam.CUSIDN}');
                $("#IDnum").text("<c:out value='${fn:escapeXml(result_data.data.requestParam.CUSIDN)}' />");
// 			$("#CPRIMID").val('${result_data.data.requestParam.CUSIDN}');
                $("#CPRIMID").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.CUSIDN)}' />");
//			$("#CUSIDN").val("${result_data.data.requestParam.CUSIDN}");
                $("#CUSIDN").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.CUSIDN)}' />");
//			$("#CFU2").val("${result_data.data.requestParam.CFU2}");
                $("#CFU2").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.CFU2)}' />");
//			$("#CN").val("${result_data.data.requestParam.CN}");
                $("#CN").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.CN)}' />");
//			$("#CARDNAME").val("${result_data.data.requestParam.CARDNAME}");
                $("#CARDNAME").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.CARDNAME)}' />");
// 			$("#CARDMEMO").val("${result_data.data.requestParam.CARDMEMO}");
                $("#CARDMEMO").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.CARDMEMO)}' />");
// 			$("#FGTXWAY").val("${result_data.data.requestParam.FGTXWAY}");
                $("#FGTXWAY").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.FGTXWAY)}' />");
// 			$("#OLAGREEN1").val("${result_data.data.requestParam.OLAGREEN1}");
                $("#OLAGREEN1").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.OLAGREEN1)}' />");
// 			$("#OLAGREEN2").val("${result_data.data.requestParam.OLAGREEN2}");
                $("#OLAGREEN2").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.OLAGREEN2)}' />");
// 			$("#OLAGREEN3").val("${result_data.data.requestParam.OLAGREEN3}");
                $("#OLAGREEN3").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.OLAGREEN3)}' />");
// 			$("#OLAGREEN4").val("${result_data.data.requestParam.OLAGREEN4}");
                $("#OLAGREEN4").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.OLAGREEN4)}' />");
// 			$("#OLAGREEN5").val("${result_data.data.requestParam.OLAGREEN5}");
                $("#OLAGREEN5").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.OLAGREEN5)}' />");
// 			$("#oldcardowner").val("${result_data.data.requestParam.oldcardowner}");
                $("#oldcardowner").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.oldcardowner)}' />");
// 			$("#CPRIMBIRTHDAY").val("${result_data.data.requestParam.CPRIMBIRTHDAY}");
                $("#CPRIMBIRTHDAY").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.CPRIMBIRTHDAY)}' />");
// 			$("#BIRTHY").val("${result_data.data.requestParam.BIRTHY}");
                $("#BIRTHY").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.BIRTHY)}' />");
// 			$("#BIRTHM").val("${result_data.data.requestParam.BIRTHM}");
                $("#BIRTHM").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.BIRTHM)}' />");
// 			$("#BIRTHD").val("${result_data.data.requestParam.BIRTHD}");
                $("#BIRTHD").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.BIRTHD)}' />");
// 			$("#CTTADR").val("${result_data.data.requestParam.CTTADR}");
                $("#CTTADR").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.CTTADR)}' />");
// 			$("#ADDR2").val("${result_data.data.requestParam.CTTADR1}");
                $("#ADDR2").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.CTTADR1)}' />");
// 			$("#PMTADR").val("${result_data.data.requestParam.PMTADR}");
                $("#PMTADR").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.PMTADR)}' />");
// 			$("#ADDR1").val("${result_data.data.requestParam.PMTADR1}");
                $("#ADDR1").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.PMTADR1)}' />");
// 			$("#VARSTR2").val("${result_data.data.requestParam.VARSTR2}");
                $("#VARSTR2").val("<c:out value='${fn:escapeXml(result_data.data.requestParam.VARSTR2)}' />");
            }
        }

        function check() {
            <!-- Avoid Reflected XSS All Clients -->
            //var oldcardowner = "${result_data.data.requestParam.oldcardowner}";
            var oldcardowner = "<c:out value='${fn:escapeXml(result_data.data.requestParam.oldcardowner)}' />";
            var getback = '${result_data.data.back}';
            if (getback == 'Y') {
                oldcardowner = '${result_data.data.oldcardowner}';
            }
            if (oldcardowner == "N") {
                //英文姓名
                $("#CPRIMENGNAME").addClass("validate[required]");
                //婚姻狀況
// 			$("#MPRIMMARRIAGECHK").addClass("validate[funcCallRequired[validate_Radio[<spring:message code='LB.D0057' />,MPRIMMARRIAGE]]]");
                //教育程度
// 			$("#MPRIMEDUCATION").addClass("validate[funcCall[validate_CheckSelect[<spring:message code='LB.D0055' />,MPRIMEDUCATION,#]]]");
                //現居房屋
// 			$("#MPRIMADDR1COND").addClass("validate[funcCall[validate_CheckSelect[<spring:message code='LB.D0067' />,MPRIMADDR1COND,#]]]");
                //戶籍地址
                $("#ADDR1").addClass("validate[required,funcCall[validate_CheckSelect[<spring:message code='LB.D0058' />,CITY1,#]]]");
                //居住地址
                $("#ADDR2").addClass("validate[required,funcCall[validate_CheckSelect[<spring:message code='LB.D0063' />,CITY2,#]]]]");
                //公司地址
// 			$("#ADDR4").addClass("validate[required,funcCall[validate_CheckSelect[<spring:message code='LB.D0090' />,CITY4,#]]]]");
                //行動電話
                //$("#CPRIMCELLULANO1").addClass("validate[required,funcCall[validate_CheckNumber[<spring:message code='LB.D0069' />,CPRIMCELLULANO1]]]");
                $("#CPRIMCELLULANO1").addClass("validate[required,funcCall[validate_cellPhone[手機號碼格式錯誤,CPRIMCELLULANO1]]]");
                //居住電話
                $("#CPRIMHOMETELNO").addClass("validate[required,funcCall[validate_CheckNumber[<spring:message code='LB.X2040' />,CPRIMHOMETELNO]]]");
                //戶籍電話
                $("#CPRIMHOMETELNO2").addClass("validate[required,funcCall[validate_CheckNumber[<spring:message code='LB.X2041' />,CPRIMHOMETELNO2]]]");
                //年收入
                $("#CPRIMSALARYchk").addClass("validate[required,funcCall[validate_CheckNumber[<spring:message code='LB.D0625_1' />,CPRIMSALARY]]]");
                //公司電話
// 			$("#CPRIMOFFICETELNO1chk").addClass("validate[required,funcCall[validate_OneInputPhone[<spring:message code='LB.X2042' />,CPRIMOFFICETELNO1]]]");
                //年資(年)
                $("#WORKYEARS").addClass("validate[funcCall[validate_CheckSelect[<spring:message code='LB.X1645' />,WORKYEARS,#]]]");
                //年資(月)
                $("#WORKMONTHS").addClass("validate[funcCall[validate_CheckSelect[<spring:message code='LB.X1646' />,WORKMONTHS,#]]]");
                //往來預期金額
                $("#PAYMONEY").addClass("validate[required,funcCall[validate_CheckNumber[<spring:message code='LB.D0625_1' />,PAYMONEY]]]");
                //E-Mail
// 			$("#CPRIMEMAIL").addClass("validate[funcCallRequired[validate_CheckMail[E-mail,CPRIMEMAIL,false]]]");
                //身分證補換發日期
                $("#checkDD").addClass("validate[funcCallRequired[validate_CheckSelects[<spring:message code='LB.X2449'/>,CHDATEY,CHDATEM,CHDATED,#]]]");
                //身分證補換發縣市
                $("#CITYCHA").addClass("validate[funcCall[validate_CheckSelect[<spring:message code='LB.D1071' />,CITYCHA,#]]]");
                //身分證補換發資訊
                $("#checkTYPE").addClass("validate[funcCallRequired[validate_CheckBox[<spring:message code='LB.X2450'/>,CHTYPE]]]");
            } else {
                //行動電話
// 			$("#CPRIMCELLULANO1").addClass("validate[funcCall[validate_CheckNumber[<spring:message code='LB.D0069' />,CPRIMCELLULANO1]]]");
                $("#CPRIMCELLULANO1").addClass("validate[funcCall[validate_cellPhone[手機號碼格式錯誤,CPRIMCELLULANO1]]]");
                //居住電話
                $("#CPRIMHOMETELNO").addClass("validate[funcCall[validate_CheckNumber[<spring:message code='LB.X2040' />,CPRIMHOMETELNO]]]");
                //戶籍電話
                $("#CPRIMHOMETELNO2").addClass("validate[funcCall[validate_CheckNumber[<spring:message code='LB.X2041' />,CPRIMHOMETELNO2]]]");
                //公司電話
// 			$("#CPRIMOFFICETELNO1chk").addClass("validate[funcCall[validate_OneInputPhone[<spring:message code='LB.X2042' />,CPRIMOFFICETELNO1]]]");
                //年收入
                $("#CPRIMSALARYchk").addClass("validate[funcCallRequired[validate_CheckNumber[<spring:message code='LB.D0625_1' />,CPRIMSALARY]]]");
                //往來預期金額
                $("#PAYMONEY").addClass("validate[funcCallRequired[validate_CheckNumber[<spring:message code='LB.D0625_1' />,PAYMONEY]]]");
                //E-Mail
// 			$("#CPRIMEMAIL").addClass("validate[funcCallRequired[validate_CheckMail[E-mail,CPRIMEMAIL,true]]]");
            }
            //E-Mail
            $("#CPRIMEMAIL").addClass("validate[funcCallRequired[validate_CheckMail[E-mail,CPRIMEMAIL,true]]]");
            //公司電話
            $("#CPRIMOFFICETELNO1chk").addClass("validate[funcCall[validate_OneInputPhone[<spring:message code='LB.X2042' />,CPRIMOFFICETELNO1]]]");
            //戶籍電話
            $("#CPRIMHOMETELNO2").addClass("validate[funcCall[validate_CheckNumber[<spring:message code='LB.X2041' />,CPRIMHOMETELNO2]]]");
            //中文姓名
            $("#CPRIMCHNAME").addClass("validate[required,funcCall[validate_isChinese[<spring:message code= 'LB.X1056' />,CPRIMCHNAME]]]");
            //是否更換過中文姓名
            $("#CHENAMECHK").addClass("validate[funcCallRequired[validate_Radio[<spring:message code='LB.D0052' />,CHENAME]]]");
            //原住民姓名
// 		$("#CUSNAME").addClass("validate[funcCallRequired[validate_isChinese['原住民姓名',CUSNAME]]]");
            //職位名稱
            $("#CPRIMJOBTITLE").addClass("validate[funcCall[validate_CheckSelect[<spring:message code='LB.X2438' />,CPRIMJOBTITLE,#]]]");
            //公司名稱
            $("#CPRIMCOMPANY").addClass("validate[required]");
            //職業類別
            $("#CPRIMJOBTYPE").addClass("validate[funcCall[validate_CheckSelect[<spring:message code='LB.X2437' />,CPRIMJOBTYPE,#]]]");
        }

        function goBack() {
            $("#CMBACK").click(function (e) {
                $("#formId").validationEngine('detach');
                console.log("submit~~");
                $("#formId").attr("action", "${__ctx}/CREDIT/APPLY/apply_creditcard_terms");
                $("#back").val('Y');

                initBlockUI();
                $("#formId").submit();
            });
        }

        function startsWith(str, word) {
            return str.lastIndexOf(word, 0) === 0;
        }

        function GO() {
            $("#CMSUBMIT").click(function (e) {
                $("#CPRIMSALARYchk").val($("#CPRIMSALARY").val());
                $("#CPRIMOFFICETELNO1chk").val($("#CPRIMOFFICETELNO1").val());
                //切割居住電話
                var inputData = $("#CPRIMHOMETELNO").val();
                $('#CPRIMHOMETELNO1B').val(inputData);
// 			var HOMEP = matchPhone(inputData);
// 			if(HOMEP != null){
//     			$('#CPRIMHOMETELNO1A').val(HOMEP[0]);
//     			$('#CPRIMHOMETELNO1B').val(HOMEP[1]);
// 			}
// 			//若有值加入驗證
// 			if($('#CPRIMHOMETELNO1A').val().length > 0){
// 				$('#CPRIMHOMETELNO1A').addClass("validate[required,funcCall[validate_chkChrNum['<spring:message code= "LB.X1217" />',CPRIMHOMETELNO1A]]");
// 				$('#CPRIMHOMETELNO1A').addClass("validate[funcCallRequired[validate_CheckLenEqual2['<spring:message code= "LB.X1217" />',CPRIMHOMETELNO1A,true,0,3]]");
// 			}
// 			else{
// 				$('#CPRIMHOMETELNO1A').removeClass("validate[required,funcCall[validate_chkChrNum['<spring:message code= "LB.X1217" />',CPRIMHOMETELNO1A]]");
// 				$('#CPRIMHOMETELNO1A').removeClass("validate[funcCallRequired[validate_CheckLenEqual2['<spring:message code= "LB.X1217" />',CPRIMHOMETELNO1A,true,0,3]]");
// 			}
                if ($('#CPRIMHOMETELNO1B').val().length > 0) {
                    $('#CPRIMHOMETELNO1B').addClass("validate[required,funcCall[validate_CheckNumber['<spring:message code= "LB.D1127" />',CPRIMHOMETELNO1B]]]");
                } else {
                    $('#CPRIMHOMETELNO1B').removeClass("validate[required,funcCall[validate_CheckNumber['<spring:message code= "LB.D1127" />',CPRIMHOMETELNO1B]]]");
                }

                //切割戶籍電話
                var inputData2 = $("#CPRIMHOMETELNO2").val();
                $('#CPRIMHOMETELNO2B').val(inputData2);
// 			var HOMEP2 = matchPhone(inputData2);
// 			if(HOMEP2 != null){
//     			$('#CPRIMHOMETELNO2A').val(HOMEP2[0]);
//     			$('#CPRIMHOMETELNO2B').val(HOMEP2[1]);
// 			}
// 			//若有值加入驗證
// 			if($('#CPRIMHOMETELNO2A').val().length > 0){
// 				$('#CPRIMHOMETELNO2A').addClass("validate[required,funcCall[validate_chkChrNum['<spring:message code= "LB.X1644" />',CPRIMHOMETELNO2A]]");
// 				$('#CPRIMHOMETELNO2A').addClass("validate[funcCallRequired[validate_CheckLenEqual2['<spring:message code= "LB.X1644" />',CPRIMHOMETELNO2A,true,0,3]]");
// 			}
// 			else{
// 				$('#CPRIMHOMETELNO2A').removeClass("validate[required,funcCall[validate_chkChrNum['<spring:message code= "LB.X1644" />',CPRIMHOMETELNO2A]]");
// 				$('#CPRIMHOMETELNO2A').removeClass("validate[funcCallRequired[validate_CheckLenEqual2['<spring:message code= "LB.X1644" />',CPRIMHOMETELNO2A,true,0,3]]");
// 			}
                if ($('#CPRIMHOMETELNO2B').val().length > 0) {
                    $('#CPRIMHOMETELNO2B').addClass("validate[required,funcCall[validate_CheckNumber['<spring:message code= "LB.D0144" />',CPRIMHOMETELNO2B]]]");
                } else {
                    $('#CPRIMHOMETELNO2B').removeClass("validate[funcCallRequired[validate_CheckNumber['<spring:message code= "LB.D0144" />',CPRIMHOMETELNO2B]]]");
                }

                //切割公司/學校電話
                var inputData3 = $("#CPRIMOFFICETELNO1").val();
                var HOMEP3 = matchPhone(inputData3);
                if (HOMEP3 != null) {
                    $('#CPRIMOFFICETELNO1A').val(HOMEP3[0]);
                    $('#CPRIMOFFICETELNO1B').val(HOMEP3[1]);
                }
                //若有值加入驗證
                if ($('#CPRIMOFFICETELNO1A').val().length > 0) {
                    $('#CPRIMOFFICETELNO1A').addClass("validate[required,funcCall[validate_chkChrNum[<spring:message code='LB.X2043' />,CPRIMOFFICETELNO1A]]");
                    $('#CPRIMOFFICETELNO1A').addClass("validate[funcCallRequired[validate_CheckLenEqual2[<spring:message code='LB.X2043' />,CPRIMOFFICETELNO1A,true,0,3]]");
                } else {
                    $('#CPRIMOFFICETELNO1A').removeClass("validate[required,funcCall[validate_chkChrNum[<spring:message code='LB.X2043' />,CPRIMOFFICETELNO1A]]");
                    $('#CPRIMOFFICETELNO1A').removeClass("validate[funcCallRequired[validate_CheckLenEqual2[<spring:message code='LB.X2043' />,CPRIMOFFICETELNO1A,true,0,3]]");
                }
                if ($('#CPRIMOFFICETELNO1B').val().length > 0) {
                    $('#CPRIMOFFICETELNO1B').addClass("validate[required,funcCall[validate_chkChrNum[<spring:message code='LB.X1981' />,CPRIMOFFICETELNO1B]]");
                    $('#CPRIMOFFICETELNO1B').addClass("validate[funcCallRequired[validate_CheckLenEqual2[<spring:message code='LB.X1981' />,CPRIMOFFICETELNO1B,true,0,10]]");
                } else {
                    $('#CPRIMOFFICETELNO1B').removeClass("validate[required,funcCall[validate_chkChrNum[<spring:message code='LB.X1981' />,CPRIMOFFICETELNO1B]]");
                    $('#CPRIMOFFICETELNO1B').removeClass("validate[funcCallRequired[validate_CheckLenEqual2[<spring:message code='LB.X1981' />,CPRIMOFFICETELNO1B,true,0,10]]");
                }

                var job = $("#CPRIMJOBTYPE").val();
                var getback = '${result_data.data.back}';
                var oldcardowner = "${result_data.data.requestParam.oldcardowner}";
                if (getback == 'Y') {
                    oldcardowner = '${result_data.data.oldcardowner}';
                }
                if (job != "#") {
                    if (oldcardowner == "N") {
                        //公司電話
// 					$("#CPRIMOFFICETELNO1chk").removeClass("validate[required,funcCall[validate_OneInputPhone[<spring:message code='LB.X2042' />,CPRIMOFFICETELNO1]]]");
                        //年收入
// 					$("#CPRIMSALARYchk").removeClass("validate[required,funcCall[validate_CheckNumber[<spring:message code='LB.D0625_1' />,CPRIMSALARY]]]");
                    } else {
                        //公司電話
// 					$("#CPRIMOFFICETELNO1chk").removeClass("validate[funcCall[validate_OneInputPhone[<spring:message code='LB.X2042' />,CPRIMOFFICETELNO1]]]");
                    }
                    //若有選擇職業，不檢核公司名稱、職稱、年收入、年資
// 				$('#CPRIMJOBTITLE').removeClass("validate[funcCall[validate_CheckSelect[<spring:message code='LB.X2438' />,CPRIMJOBTITLE,#]]]");
                    //年資(年)
                    $("#WORKYEARS").removeClass("validate[funcCall[validate_CheckSelect[<spring:message code='LB.X1645' />,WORKYEARS,#]]]");
                    //年資(月)
                    $("#WORKMONTHS").removeClass("validate[funcCall[validate_CheckSelect[<spring:message code='LB.X1646' />,WORKMONTHS,#]]]");
                    //公司地址
// 				$("#ADDR4").removeClass("validate[required,funcCall[validate_CheckSelect[<spring:message code='LB.D0090' />,CITY4,#]]]]");
                    //公司名稱
                    $("#CPRIMCOMPANY").removeClass("validate[required]");
                } else {
                    //公司名稱
                    $("#CPRIMCOMPANY").addClass("validate[required]");

// 				$("#CPRIMJOBTITLE").addClass("validate[funcCall[validate_CheckSelect[<spring:message code='LB.X2438' />,CPRIMJOBTITLE,#]]]");
                    if (oldcardowner == "N") {
                        //年收入
                        $("#CPRIMSALARYchk").addClass("validate[required,funcCall[validate_CheckNumber[<spring:message code='LB.D0625_1' />,CPRIMSALARY]]]");
                        //公司電話
// 					$("#CPRIMOFFICETELNO1chk").addClass("validate[required,funcCall[validate_OneInputPhone[<spring:message code='LB.X2042' />,CPRIMOFFICETELNO1]]]");
                        //年資(年)
                        $("#WORKYEARS").addClass("validate[funcCall[validate_CheckSelect[<spring:message code='LB.X1645' />,WORKYEARS,#]]]");
                        //年資(月)
                        $("#WORKMONTHS").addClass("validate[funcCall[validate_CheckSelect[<spring:message code='LB.X1646' />,WORKMONTHS,#]]]");
                        //公司地址
// 					$("#ADDR4").addClass("validate[required,funcCall[validate_CheckSelect[<spring:message code='LB.D0090' />,CITY4,#]]]]");
                    } else {
                        //公司電話
// 					$("#CPRIMOFFICETELNO1chk").addClass("validate[funcCall[validate_OneInputPhone[<spring:message code='LB.X2042' />,CPRIMOFFICETELNO1]]]");
                    }
                }

                e = e || window.event;
                if (!$('#formId').validationEngine('validate')) {
                    e.preventDefault();
                } else {
// 				var oldcardowner = "${result_data.data.requestParam.oldcardowner}";
                    var CPRIMCHNAME = $("#CPRIMCHNAME").val();
                    var CPRIMENGNAME = $("#CPRIMENGNAME").val();

                    var ZIP1 = $("#ZIP1").val();
                    var CITY1 = $("#CITY1").val();
                    var ZONE1 = $("#ZONE1").val();
                    var ADDR1 = $("#ADDR1").val();
                    if (CITY1 == "#") {
                        CITY1 = "";
                    }
                    if (ZONE1 == "#") {
                        ZONE1 = "";
                    }
                    if ('${transfer}' == 'en') {
                        var CPRIMADDR2 = ZIP1 + " " + CITY1 + " " + ZONE1 + " " + ADDR1;
                    } else {
                        var CPRIMADDR2 = ZIP1 + CITY1 + ZONE1 + ADDR1;
                    }

                    var ZIP2 = $("#ZIP2").val();
                    var CITY2 = $("#CITY2").val();
                    var ZONE2 = $("#ZONE2").val();
                    var ADDR2 = $("#ADDR2").val();
                    if (CITY2 == "#") {
                        CITY2 = "";
                    }
                    if (ZONE2 == "#") {
                        ZONE2 = "";
                    }
                    if ('${transfer}' == 'en') {
                        var CPRIMADDR = ZIP2 + " " + CITY2 + " " + ZONE2 + " " + ADDR2;
                    } else {
                        var CPRIMADDR = ZIP2 + CITY2 + ZONE2 + ADDR2;
                    }

                    var ZIP4 = $("#ZIP4").val();
                    var CITY4 = $("#CITY4").val();
                    var ZONE4 = $("#ZONE4").val();
                    var ADDR4 = $("#ADDR4").val();
                    if (CITY4 == "#") {
                        CITY4 = "";
                    }
                    if (ZONE4 == "#") {
                        ZONE4 = "";
                    }
                    if ('${transfer}' == 'en') {
                        var CPRIMADDR3 = ZIP4 + " " + CITY4 + " " + ZONE4 + " " + ADDR4;
                    } else {
                        var CPRIMADDR3 = ZIP4 + CITY4 + ZONE4 + ADDR4;
                    }

                    var hometel = $("#CPRIMHOMETELNO").val();
                    hometel = hometel.replace("-", "");
                    var housetel = $("#CPRIMHOMETELNO2").val();
                    housetel = housetel.replace("-", "");
                    var comtel = $("#CPRIMOFFICETELNO1").val();
                    comtel = comtel.replace("-", "");
                    comtel = comtel.replace("#", "");

                    var CPRIMCOMPANY = $("#CPRIMCOMPANY").val();

                    $("#CPRIMADDR2").val(CPRIMADDR2);
                    $("#CPRIMADDR").val(CPRIMADDR);
                    $("#CPRIMADDR3").val(CPRIMADDR3);

                    //<!-- Avoid Reflected XSS All Clients -->
                    //var CARDMEMO = "${result_data.data.requestParam.CARDMEMO}";
                    var CARDMEMO = "<c:out value='${fn:escapeXml(result_data.data.requestParam.CARDMEMO)}' />";

                    if (CARDMEMO == "1" || CARDMEMO == "2") {
                        $("input[type=radio][name=CNOTE1][value=1]").prop("checked", true);
                    }

                    var CTRYDESC = $("#CTRYDESC1").val();
                    if (CTRYDESC.length == 0) {
                        errorBlock(null, null, ["<spring:message code='LB.Alert045' />"],
                            '<spring:message code= "LB.Quit" />', null);
// 					alert("<spring:message code= "LB.Alert045" />");
                        return false;
                    }
                    $("#CTRYDESC").val($("#CTRYDESC1").find(":selected").text());

                    //身分證補換發
                    if ($("#CITYCHA").val() != "#") {
                        $("#CHANCITY").val($("#CITYCHA").find(":selected").text());
                    }

                    $("#formId").attr("action", "${__ctx}/CREDIT/APPLY/apply_creditcard_p4_2");
                    $("#formId").submit();
                }
            });
        }

        //居住地址:縣市
        function doquery() {
            if ($("#CITY2").val() != "#") {
                var URI = "${__ctx}/APPLY/CREDIT/getAreaListAjax";
                var rqData = {CITY: $("#CITY2").val()};
                fstop.getServerDataEx(URI, rqData, false, doqueryFinish);
            }
        }

        function doqueryFinish(data) {
            if (data.result == true) {
                var areaList = $.parseJSON(data.data);

                $("#ZONE2").html("");
// 		var ZONE2HTML = "<option value='#'>---<spring:message code= "LB.X0546" />---</option>";
                var ZONE2HTML = "<option value='#'><spring:message code='LB.D0060' /></option>";
                for (var x = 0; x < areaList.length; x++) {
                    ZONE2HTML += "<option value='" + areaList[x].AREA + "'>" + areaList[x].AREA + "</option>";
                }
                $("#ZONE2").html(ZONE2HTML);
            } else {
                //alert("<spring:message code= "LB.X1069" />");
                errorBlock(
                    null,
                    null,
                    ["<spring:message code= "LB.X1069" />"],
                    '<spring:message code= "LB.Quit" />',
                    null
                );
            }
        }

        //居住地址:市／區鄉鎮
        function doquery1() {
            if ($("#ZONE2").val() != "#") {
                var URI = "${__ctx}/APPLY/CREDIT/getZipCodeByCityAreaAjax";
                var rqData = {CITY: $("#CITY2").val(), AREA: $("#ZONE2").val()};
                fstop.getServerDataEx(URI, rqData, false, doquery1Finish);
            }
        }

        function doquery1Finish(data) {
            if (data.result == true) {
                var zipCode = data.data;

                $("#ZIP2").val(zipCode);
            } else {
                //alert("<spring:message code= "LB.X1070" />");
                errorBlock(
                    null,
                    null,
                    ["<spring:message code= "LB.X1070" />"],
                    '<spring:message code= "LB.Quit" />',
                    null
                );
            }
        }

        //戶籍地址:縣市
        function doquery2() {
            if ($("#CITY1").val() != "#") {
                var URI = "${__ctx}/APPLY/CREDIT/getAreaListAjax";
                var rqData = {CITY: $("#CITY1").val()};
                fstop.getServerDataEx(URI, rqData, false, doquery2Finish);
            }
        }

        function doquery2Finish(data) {
            if (data.result == true) {
                var areaList = $.parseJSON(data.data);

                $("#ZONE1").html("");
// 		var ZONE1HTML = "<option value='#'>---<spring:message code= "LB.X0546" />---</option>";
                var ZONE1HTML = "<option value='#'><spring:message code='LB.D0060' /></option>";
                for (var x = 0; x < areaList.length; x++) {
                    ZONE1HTML += "<option value='" + areaList[x].AREA + "'>" + areaList[x].AREA + "</option>";
                }
                $("#ZONE1").html(ZONE1HTML);
            } else {
                //alert("<spring:message code= "LB.X1069" />");
                errorBlock(
                    null,
                    null,
                    ["<spring:message code= "LB.X1069" />"],
                    '<spring:message code= "LB.Quit" />',
                    null
                );
            }
        }

        //戶籍地址:市／區鄉鎮
        function doquery3() {
            if ($("#ZONE1").val() != "#") {
                var URI = "${__ctx}/APPLY/CREDIT/getZipCodeByCityAreaAjax";
                var rqData = {CITY: $("#CITY1").val(), AREA: $("#ZONE1").val()};
                fstop.getServerDataEx(URI, rqData, false, doquery3Finish);
            }
        }

        function doquery3Finish(data) {
            if (data.result == true) {
                var zipCode = data.data;

                $("#ZIP1").val(zipCode);
            } else {
                //alert("<spring:message code= "LB.X1070" />");
                errorBlock(
                    null,
                    null,
                    ["<spring:message code= "LB.X1070" />"],
                    '<spring:message code= "LB.Quit" />',
                    null
                );
            }
        }

        //公司地址:縣市
        function doquery4() {
            if ($("#CITY4").val() != "#") {
                var URI = "${__ctx}/APPLY/CREDIT/getAreaListAjax";
                var rqData = {CITY: $("#CITY4").val()};
                fstop.getServerDataEx(URI, rqData, false, doquery4Finish);
            }
        }

        function doquery4Finish(data) {
            if (data.result == true) {
                var areaList = $.parseJSON(data.data);

                $("#ZONE4").html("");
// 		var ZONE4HTML = "<option value='#'>---<spring:message code= "LB.X0546" />---</option>";
                var ZONE4HTML = "<option value='#'><spring:message code='LB.D0060' /></option>";
                for (var x = 0; x < areaList.length; x++) {
                    ZONE4HTML += "<option value='" + areaList[x].AREA + "'>" + areaList[x].AREA + "</option>";
                }
                $("#ZONE4").html(ZONE4HTML);
            } else {
                //alert("<spring:message code= "LB.X1069" />");
                errorBlock(
                    null,
                    null,
                    ["<spring:message code= "LB.X1069" />"],
                    '<spring:message code= "LB.Quit" />',
                    null
                );
            }
        }

        //公司地址:市／區鄉鎮
        function doquery5() {
            if ($("#ZONE4").val() != "#") {
                var URI = "${__ctx}/APPLY/CREDIT/getZipCodeByCityAreaAjax";
                var rqData = {CITY: $("#CITY4").val(), AREA: $("#ZONE4").val()};
                fstop.getServerDataEx(URI, rqData, false, doquery5Finish);
            }
        }

        function doquery5Finish(data) {
            if (data.result == true) {
                var zipCode = data.data;

                $("#ZIP4").val(zipCode);
            } else {
                //alert("<spring:message code= "LB.X1070" />");
                errorBlock(
                    null,
                    null,
                    ["<spring:message code= "LB.X1070" />"],
                    '<spring:message code= "LB.Quit" />',
                    null
                );
            }
        }

        //國籍選擇
        function fillData(id, desc) {
            $("#CTRYDESC").val(desc);
            $("#countryDialog").dialog("close");
        }

        //英文姓名檢核
        function checkinput(name) {
            var CPRIMENGNAME = $("#" + name).val();
            $("#" + name).val(CPRIMENGNAME.toUpperCase());

            if (checkENGNAMEChar($("#" + name).val())) {
                $("#" + name).val("");
            }
        }

        function checkENGNAMEChar(iText) {
            var result = false;
            var legal = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ ';
            var i = 0
            while (i < iText.length) {
                var c = iText.substr(i, 1);
                if (legal.indexOf(c) < 0) {
                    result = true;
                    break;
                }
                i++;
            }
            return result;
        }

        //勾選同行動電話
        function mobtohome() {
            if ($("#samemobtel").prop("checked") == true) {
                $("#samemobtel").attr("value", "Y");
                //居住電話=行動電話
                $("#CPRIMHOMETELNO").val($("#CPRIMCELLULANO1").val());
            } else {
                $("#samemobtel").attr("value", "N");
                $("#CPRIMHOMETELNO").val("");
            }
        }

        //勾選同居住電話
        function samehome() {
            if ($("#samehometel").prop("checked") == true) {
                $("#samehometel").attr("value", "Y");
                //戶籍電話=居住電話
                $("#CPRIMHOMETELNO2").val($("#CPRIMHOMETELNO").val());
            } else {
                $("#samehometel").attr("value", "N");
                $("#CPRIMHOMETELNO2").val("");
            }
        }

        //勾選同居住地址
        function samehomeADDR() {
            if ($("#sameaddr").prop("checked") == true) {
// 		$("#sameaddr").attr("value","Y");
// 		$("#ADDR1").val($("#ADDR2").val());
// 		$("#ZIP1").val($("#ZIP2").val());
// 		$("#CITY1").val($("#CITY2").val());
// 		$("#ZONE1").val($("#ZONE2").val());
// 		doaddrAjax();
                doquery91();
            } else {
                $("#sameaddr").attr("value", "N");
                $("#CITY1").val("#");
                $("#ZONE1").val("#");
                $("#ADDR1").val("");
                $("#ZIP1").val("");
            }
        }

        function doaddrAjax() {
            if ($("#sameaddr").val() == "Y") {
                $("#CITY1").val($("#CITY2").val());
                var URI = "${__ctx}/APPLY/CREDIT/getAreaListAjax";
                var rqData = {CITY: $("#CITY1").val()};
                fstop.getServerDataEx(URI, rqData, false, doaddrAjaxFinish);
            }
        }

        function doaddrAjaxFinish(data) {
            if (data.result == true) {
                var areaList = $.parseJSON(data.data);

                $("#ZONE1").html("");
// 		var ZONE1HTML = "<option value='#'>---<spring:message code= "LB.X0546" />---</option>";
                var ZONE1HTML = "<option value='#'><spring:message code='LB.D0060' /></option>";
                for (var x = 0; x < areaList.length; x++) {
                    ZONE1HTML += "<option value='" + areaList[x].AREA + "'>" + areaList[x].AREA + "</option>";
                }
                $("#ZONE1").html(ZONE1HTML);

                $("#ZONE1").val($("#ZONE2").val());
            } else {
                //alert("<spring:message code= "LB.X1069" />");
                errorBlock(
                    null,
                    null,
                    ["<spring:message code= "LB.X1069" />"],
                    '<spring:message code= "LB.Quit" />',
                    null
                );
            }
        }

        //國籍欄位帶初始值
        function setCTRYDESC() {
            <!-- Avoid Reflected XSS All Clients -->
            //var uid = "${result_data.data.requestParam.CUSIDN}";
            var uid = "<c:out value='${fn:escapeXml(result_data.data.requestParam.CUSIDN)}' />";
            var intVal = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
            for (var i = 0; i < intVal.length; i++) {
                //判斷身分證第二位是否為數字，不為數字為外國人
                if (uid.substring(1, 2) == intVal[i]) {
                    $("#CTRYDESC").val("<spring:message code= "LB.X0828" />");
                    break;
                }
            }

            <!-- Avoid Reflected XSS All Clients -->
            //var CARDMEMO = "${result_data.data.requestParam.CARDMEMO}";
            var CARDMEMO = "<c:out value='${fn:escapeXml(result_data.data.requestParam.CARDMEMO)}' />";

            if (CARDMEMO == "1" || CARDMEMO == "2") {
                $("input[type=radio][name=CNOTE1][value=1]").prop("checked", true);
            }
        }

        function countrydata() {
            uri = '${__ctx}' + "/CREDIT/APPLY/country_ajax";

            console.log("creatOutAcn.uri: " + uri);

            data = fstop.getServerDataEx(uri, null, false,
                backdata);
        }

        function backdata(data) {
            console.log("data: " + data);
            if (data) {
                //ajax回傳資料型態: List<Map<String, String>>
                console.log("data.json: " + JSON.stringify(data));

                //先清空原有之"OPTION"內容
                $("#CTRYDESC1").empty();
// 		$("#CTRYDESC").append( $("<option></option>").attr("value", "#").text("---<spring:message code="LB.Select_account"/>---"));

                //迴圈帶出"國籍"之下拉式選單
                if ('${transfer}' == 'en') {
                    data.forEach(function (map) {
                        console.log(map);
                        $("#CTRYDESC1").append($("<option></option>").attr("value", map.COUNTRYABB).text(map.COUNTRYEN));
                    });
                } else if ('${transfer}' == 'zh') {
                    data.forEach(function (map) {
                        console.log(map);
                        $("#CTRYDESC1").append($("<option></option>").attr("value", map.COUNTRYABB).text(map.COUNTRYCN));
                    });
                } else {
                    data.forEach(function (map) {
                        console.log(map);
                        $("#CTRYDESC1").append($("<option></option>").attr("value", map.COUNTRYABB).text(map.COUNTRY));
                    });
                }
                $("#CTRYDESC1").val('TW');
// 		data.forEach(function(map) {
// 			console.log(map);
// 			$("#CTRYDESC").append( $("<option></option>").attr("value", map.ACN).text(map.ACN));
// 		});
            }
        }

        function matchPhone(input) {
            var phoneList = [];
            var re = new RegExp(/^\d{0,3}-\d{0,10}$/)
            if (re.test(input)) {
                var vList1 = input.split("-");
                phoneList.push(vList1[0]);
                phoneList.push(vList1[1]);
                return phoneList;
            }
            return null;
        }

        function changeMatchPhone(input) {
            var inputData = $("#" + input).val();
            var phoneList = matchPhone(inputData);
            if (phoneList != null)
                $("#" + input).val(phoneList[0] + '-' + phoneList[1]);
        }

        //居住地縣市鄉鎮市區
        function setCITY() {
            llo = "${__ctx}/js/";
            if ('${result_data.data.back}' == 'Y') {
                $('#twzipcode').twzipcode({
                    countyName: 'CITY2',
                    districtName: 'ZONE2',
                    zipcodeName: 'ZIP2',
                    countySel: '${result_data.data.CITY2}',
                    districtSel: '${result_data.data.ZONE2}',
                    language: 'zip_${pageContext.response.locale}',
                    onCountySelect: null
                });
            } else {
                var c2s = $("#CITY2set").val();
                var z2s = $("#ZONE2set").val();
                $('#twzipcode').twzipcode({
                    countyName: 'CITY2',
                    districtName: 'ZONE2',
                    zipcodeName: 'ZIP2',
                    countySel: c2s,
                    districtSel: z2s,
                    language: 'zip_${pageContext.response.locale}',
                    onCountySelect: null
                });
            }
        }

        //戶籍地縣市鄉鎮市區
        function setCITY1() {
            llo = "${__ctx}/js/";
            if ('${result_data.data.back}' == 'Y') {
                $('#twzipcode1').twzipcode({
                    countyName: 'CITY1',
                    districtName: 'ZONE1',
                    zipcodeName: 'ZIP1',
                    countySel: '${result_data.data.CITY1}',
                    districtSel: '${result_data.data.ZONE1}',
                    language: 'zip_${pageContext.response.locale}',
                    onCountySelect: null
                });
            } else {
                var c1s = $("#CITY1set").val();
                var z1s = $("#ZONE1set").val();
                $('#twzipcode1').twzipcode({
                    countyName: 'CITY1',
                    districtName: 'ZONE1',
                    zipcodeName: 'ZIP1',
                    countySel: c1s,
                    districtSel: z1s,
                    language: 'zip_${pageContext.response.locale}',
                    onCountySelect: null
                });
            }
        }

        //公司縣市鄉鎮市區
        function setCITY2() {
            llo = "${__ctx}/js/";
            $('#twzipcode2').twzipcode({
                countyName: 'CITY4',
                districtName: 'ZONE4',
                zipcodeName: 'ZIP4',
                countySel: '${result_data.data.CITY4}',
                districtSel: '${result_data.data.ZONE4}',
                language: 'zip_${pageContext.response.locale}',
                onCountySelect: null
            });
        }

        function doquery91() {
            console.log($("#sameaddr").prop("checked"));
            if ($("#sameaddr").prop("checked")) {
                $("#sameaddr").val('Y');
                $("#CITY1").children().each(function () {
                    console.log($(this).val());
                    if ($(this).val() == $("#CITY2").val()) {
                        $(this).attr("selected", true);
                    }
                });
                $('#ZONE1').html("<option value='#'><spring:message code= "LB.X0546" /></option> ");

                $("#ZONE2").children().each(function () {
                    console.log($(this).val());
                    if ($(this).val() != $("#ZONE2").val()) {
                        $("#ZONE1").append("<option>" + $(this).val() + "</option>");
                    } else {
                        $("#ZONE1").append("<option selected>" + $(this).val() + "</option>");
                    }
                });

                $("#ADDR1").val($("#ADDR2").val());
                $("#ZIP1").val($("#ZIP2").val());
            } else {
                $("#sameaddr").val('N');
            }
        }

        function dateprocess() {
            var Today = new Date();
            var y = Today.getFullYear();
            var m = (Today.getMonth() + 1 < 10 ? '0' : '') + (Today.getMonth() + 1);
            var d = (Today.getDate() < 10 ? '0' : '') + Today.getDate();
            y = y - 1911;
            var min_y = y;
            var max_y = y - 100;

            for (var i = min_y; i >= max_y; i--) {
                var YYtext = i;
                if (i.toString().length < 2) {
                    YYtext = "00" + YYtext;
                } else if (i.toString().length < 3) {
                    YYtext = "0" + YYtext;
                }
                //民國年
                $("#CHDATEY").append($("<option></option>").attr("value", YYtext).text("<spring:message code='LB.D0583' />" + i + "<spring:message code='LB.Year' />"));
            }
            orderby();
            for (var i = 1; i <= 12; i++) {
                var j = i;
                if (j < 10) {
                    j = "0" + j;
                }
                //月
                if ('${transfer}' == 'en') {
                    switch (j) {
                        case '01':
                            $("#CHDATEM").append($("<option></option>").attr("value", j).text("January"));
                            break;
                        case '02':
                            $("#CHDATEM").append($("<option></option>").attr("value", j).text("February"));
                            break;
                        case '03':
                            $("#CHDATEM").append($("<option></option>").attr("value", j).text("March"));
                            break;
                        case '04':
                            $("#CHDATEM").append($("<option></option>").attr("value", j).text("April"));
                            break;
                        case '05':
                            $("#CHDATEM").append($("<option></option>").attr("value", j).text("May"));
                            break;
                        case '06':
                            $("#CHDATEM").append($("<option></option>").attr("value", j).text("June"));
                            break;
                        case '07':
                            $("#CHDATEM").append($("<option></option>").attr("value", j).text("July"));
                            break;
                        case '08':
                            $("#CHDATEM").append($("<option></option>").attr("value", j).text("August"));
                            break;
                        case '09':
                            $("#CHDATEM").append($("<option></option>").attr("value", j).text("September"));
                            break;
                        case 10:
                            $("#CHDATEM").append($("<option></option>").attr("value", j).text("October"));
                            break;
                        case 11:
                            $("#CHDATEM").append($("<option></option>").attr("value", j).text("November"));
                            break;
                        case 12:
                            $("#CHDATEM").append($("<option></option>").attr("value", j).text("December"));
                            break;
                        default:
                            x = "沒有符合的條件";
                    }
                } else {
                    $("#CHDATEM").append($("<option></option>").attr("value", j).text(j + "<spring:message code='LB.Month' />"));
                }
            }
            for (var i = 1; i <= 31; i++) {
                var j = i;
                if (j < 10) {
                    j = "0" + j;
                }
                //日
                if ('${transfer}' == 'en') {
                    $("#CHDATED").append($("<option></option>").attr("value", j).text(j));
                } else {
                    $("#CHDATED").append($("<option></option>").attr("value", j).text(j + "<spring:message code='LB.D0586' />"));
                }
            }
        }

        function orderby() {
            $('#CHDATEY>option').sort(function (a, b) {
                //按option中的值排序
                var aText = $(a).val() * 1;
                var bText = $(b).val() * 1;
                if (aText > bText) return -1;
                if (aText < bText) return 1;
                return 0;
            }).appendTo('#CHDATEY');
            $('#CHDATEY>option').eq(0).attr("selected", "selected");
        }

        //補換發勾選
        function CHATYPE(data) {
            if (data.checked) {
                for (var i = 0; i < $("input[name*='CHTYPE']").length; i++) {
                    $("input[name*='CHTYPE']")[i].checked = false;
                }
                data.checked = true;
                $("#CHANTYPE").val(data.value);
            } else {
                for (var i = 0; i < $("input[name*='CHTYPE']").length; i++) {
                    $("input[name*='CHTYPE']")[i].checked = false;
                }
                $("#CHANTYPE").val("");
            }
            console.log($("input[name*='CHTYPE']").val());
            console.log(data.value);
            return true;
        }

        function timeLogout() {
            // 刷新session
            var uri = '${__ctx}/login_refresh';
            console.log('refresh.uri: ' + uri);
            var result = fstop.getServerDataEx(uri, null, false, null);
            console.log('refresh.result: ' + JSON.stringify(result));
            // 初始化登出時間
            $("#countdownheader").html(parseInt(countdownSecHeader) + 1);
            $("#countdownMin").html("");
            $("#mobile-countdownMin").html("");
            $("#countdownSec").html("");
            $("#mobile-countdownSec").html("");
            // 倒數
            startIntervalHeader(1, refreshCountdownHeader, []);
        }

        function refreshCountdownHeader() {
            // timeout剩餘時間
            var nextSec = parseInt($("#countdownheader").html()) - 1;
            $("#countdownheader").html(nextSec);

            // 提示訊息--即將登出，是否繼續使用
            if (nextSec == 120) {
                initLogoutBlockUI();
            }
            // timeout
            if (nextSec == 0) {
                // logout
                fstop.logout('${__ctx}' + '/logout_aj', '${__ctx}' + '/timeout_logout');
            }
            if (nextSec >= 0) {
                // 倒數時間以分秒顯示
                var minutes = Math.floor(nextSec / 60);
                $("#countdownMin").html(('0' + minutes).slice(-2));
                $("#mobile-countdownMin").html(('0' + minutes).slice(-2));

                var seconds = nextSec - minutes * 60;
                $("#countdownSec").html(('0' + seconds).slice(-2));
                $("#mobile-countdownSec").html(('0' + seconds).slice(-2));
            }
        }

        function startIntervalHeader(interval, func, values) {
            clearInterval(countdownObjheader);
            countdownObjheader = setRepeater(func, values, interval);
        }

        function setRepeater(func, values, interval) {
            return setInterval(function () {
                func.apply(this, values);
            }, interval * 1000);
        }

        /**
         * 初始化logoutBlockUI
         */
        function initLogoutBlockUI() {
            logoutblockUI();
        }

        /**
         * 畫面BLOCK
         */
        function logoutblockUI(timeout) {
            $("#logout-block").show();

            // 遮罩後不給捲動
            document.body.style.overflow = "hidden";

            var defaultTimeout = 60000;
            if (timeout) {
                defaultTimeout = timeout;
            }
        }

        /**
         * 畫面UNBLOCK
         */
        function unLogoutBlockUI(timeoutID) {
            if (timeoutID) {
                clearTimeout(timeoutID);
            }
            $("#logout-block").hide();

            // 解遮罩後給捲動
            document.body.style.overflow = 'auto';
        }

        /**
         *繼續使用
         */
        function keepLogin() {
            unLogoutBlockUI(); // 解遮罩
            timeLogout(); // 刷新倒數計時
        }
    </script>
</head>
<body>
<!-- header -->
<header>
    <%@ include file="../index/header_logout.jsp" %>
</header>

<!-- 麵包屑     -->
<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
    <ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
        <li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
        <!-- 申請信用卡     -->
        <li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0666"/></li>
    </ol>
</nav>

<!--左邊menu及登入資訊-->
<div class="content row">
    <%-- 	<%@ include file="../index/menu.jsp"%> --%>
    <!--快速選單及主頁內容-->
    <main class="col-12">
        <!--主頁內容-->
        <section id="main-content" class="container">
            <!--線上申請信用卡 -->
            <h2><spring:message code="LB.D0022"/></h2>
            <div id="step-bar">
                <ul>
                    <li class="finished">信用卡選擇</li><!-- 信用卡 -->
                    <li class="finished">身份驗證與權益</li><!-- 身份驗證與權益 -->
                    <li class="active"><spring:message code="LB.X1967"/></li><!-- 申請資料 -->
                    <li class=""><spring:message code="LB.Confirm_data"/></li><!-- 確認資料 -->
                    <li class=""><spring:message code="LB.X1968"/></li><!-- 完成申請 -->

                </ul>
            </div>
            <form method="post" id="formId">
                <!-- Avoid Reflected XSS All Clients -->
                <input type="hidden" name="ADOPID" value="NA03"/>
                <input type="hidden" name="back" id="back"/>
                <%-- 			<input type="hidden" name="CUSIDN" id="CUSIDN" value="${result_data.data.requestParam.CUSIDN}"/> --%>
                <input type="hidden" name="CUSIDN" id="CUSIDN"
                       value="<c:out value='${fn:escapeXml(result_data.data.requestParam.CUSIDN)}' />"/>
                <%-- 			<input type="hidden" name="CFU2" id="CFU2" value="${result_data.data.requestParam.CFU2}"/> --%>
                <input type="hidden" name="CFU2" id="CFU2"
                       value="<c:out value='${fn:escapeXml(result_data.data.requestParam.CFU2)}' />"/>
                <%-- 			<input type="hidden" name="CN" id="CN" value="${result_data.data.requestParam.CN}"/> --%>
                <input type="hidden" name="CN" id="CN"
                       value="<c:out value='${fn:escapeXml(result_data.data.requestParam.CN)}' />"/>
                <%-- 			<input type="hidden" name="CARDNAME" id="CARDNAME" value="${result_data.data.requestParam.CARDNAME}"/> --%>
                <input type="hidden" name="CARDNAME" id="CARDNAME"
                       value="<c:out value='${fn:escapeXml(result_data.data.requestParam.CARDNAME)}' />"/>
                <input type="hidden" name="CARDMEMO" id="CARDMEMO" value="${result_data.data.requestParam.CARDMEMO}"/>
                <input type="hidden" name="FGTXWAY" id="FGTXWAY" value="${result_data.data.requestParam.FGTXWAY}"/>
                <input type="hidden" name="OLAGREEN1" id="OLAGREEN1"
                       value="${result_data.data.requestParam.OLAGREEN1}"/>
                <input type="hidden" name="OLAGREEN2" id="OLAGREEN2"
                       value="${result_data.data.requestParam.OLAGREEN2}"/>
                <input type="hidden" name="OLAGREEN3" id="OLAGREEN3"
                       value="${result_data.data.requestParam.OLAGREEN3}"/>
                <input type="hidden" name="OLAGREEN4" id="OLAGREEN4"
                       value="${result_data.data.requestParam.OLAGREEN4}"/>
                <input type="hidden" name="OLAGREEN5" id="OLAGREEN5"
                       value="${result_data.data.requestParam.OLAGREEN5}"/>
                <input type="hidden" id="RCVNO" name="RCVNO"/>
                <input type="hidden" id="CPRIMADDR" name="CPRIMADDR"/>
                <input type="hidden" id="CPRIMADDR2" name="CPRIMADDR2"/>
                <input type="hidden" id="CPRIMADDR3" name="CPRIMADDR3"/>
                <input type="hidden" name="STATUS" value="0"/>
                <input type="hidden" id="VARSTR2" name="VARSTR2" value="${result_data.data.requestParam.VARSTR2}"/>
                <!-- 			  	<input type="hidden" id="BRANCHNAME" name="BRANCHNAME"/> -->
                <input type="hidden" name="oldcardowner" id="oldcardowner"
                       value="${result_data.data.requestParam.oldcardowner}"/>
                <input type="hidden" name="CPRIMBIRTHDAY" id="CPRIMBIRTHDAY"
                       value="${result_data.data.requestParam.CPRIMBIRTHDAY}"/>
                <input type="hidden" name="CPRIMBIRTHDAYshow" id="CPRIMBIRTHDAYshow"
                       value="${result_data.data.requestParam.CPRIMBIRTHDAYshow}"/>
                <input type="hidden" name="BIRTHY" id="BIRTHY" value="${result_data.data.requestParam.BIRTHY}"/>
                <input type="hidden" name="BIRTHM" id="BIRTHM" value="${result_data.data.requestParam.BIRTHM}"/>
                <input type="hidden" name="BIRTHD" id="BIRTHD" value="${result_data.data.requestParam.BIRTHD}"/>
                <input type="hidden" name="CTTADR" id="CTTADR" value="${result_data.data.requestParam.CTTADR}"/>
                <input type="hidden" name="PMTADR" id="PMTADR" value="${result_data.data.requestParam.PMTADR}"/>
                <input type="hidden" id="CITY1set"/>
                <input type="hidden" id="ZONE1set"/>
                <input type="hidden" id="CITY2set"/>
                <input type="hidden" id="ZONE2set"/>

                <input type="hidden" name="QRCODE" id="QRCODE" value="${result_data.data.requestParam.QRCODE}"/>
                <input type="hidden" name="BRANCH" id="BRANCH" value="${result_data.data.requestParam.BRANCH}"/>
                <input type="hidden" name="branch" id="branch"
                       value="<c:out value='${fn:escapeXml(result_data.data.requestParam.branch)}' />"/>
                <input type="hidden" name="memberId" id="memberId"
                       value="<c:out value='${fn:escapeXml(result_data.data.requestParam.memberId)}' />"/>
                <input type="hidden" name="partition" id="partition"
                       value="<c:out value='${fn:escapeXml(result_data.data.requestParam.partition)}' />"/>

                <input type="hidden" name="CHANTYPE" id="CHANTYPE" value=""/>
                <input type="hidden" name="CHANCITY" id="CHANCITY" value=""/>

                <!-- timeout -->
                <section id="id-and-fast">
						<span class="id-name">
							<spring:message code="LB.Welcome_1"/>&nbsp;
							<c:if test="${empty result_data.data.CUSNAME}">
                                <span id="username" name="username_show"><spring:message code="LB.X2190"/></span>
                            </c:if>
							<c:if test="${not empty result_data.data.CUSNAME}">
                                <span id="username" name="username_show">${result_data.data.CUSNAME}</span>
                            </c:if>
							&nbsp;<spring:message code="LB.Welcome_2"/>
						</span>
                    <div id="id-block">
                        <div>
							<span class="id-time">
								<fmt:setLocale value="${pageContext.response.locale}"/>
								<c:if test="${!empty sessionScope.logindt && !empty sessionScope.logintm}">
                                    <fmt:parseDate var="parseDate"
                                                   value="${sessionScope.logindt} ${sessionScope.logintm}"
                                                   pattern="yyyy-MM-dd HH:mm:ss"/>
                                    <fmt:formatDate value="${parseDate}" dateStyle="full"
                                                    type="both"/>&nbsp;<spring:message code="LB.X2250"/>
                                    <br/>
                                </c:if>

								<fmt:formatDate value="${sessionScope.login_time}" type="both" dateStyle="full"/>
								&nbsp;<spring:message code="LB.Login_successful"/>
							</span>
                            <!-- 自動登出剩餘時間 -->
                            <span class="id-time"><spring:message code="LB.X1912"/>
								<span class="high-light ml-1">
									<!-- 分 -->
									<font id="countdownMin"></font>
									:
                                    <!-- 秒 -->
									<font id="countdownSec"></font>
								</span>
							</span>
                        </div>
                        <button type="button" class="btn-flat-orange" onclick="timeLogout()"><spring:message
                                code="LB.X1913"/></button>
                        <button type="button" class="btn-flat-darkgray"
                                onClick="fstop.logout( '${__ctx}'+'/logout_aj' , '${__ctx}'+'/login')"><spring:message
                                code="LB.Logout"/></button>
                    </div>
                </section>
                <!-- 顯示區  -->
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
                            <div class="ttb-message">
                                <p><spring:message code="LB.X1967"/> (1/3)</p>
                            </div>
                            <div class="classification-block">
                                <!-- 基本資料 -->
                                <p><spring:message code='LB.D0109'/></p>
                                <p id="showp">
                                    <!-- 									新卡戶請填寫完整基本資料，已持有本行信用卡正卡僅需填寫「<span class="high-light">*</span>」欄位 -->
                                    <spring:message code='LB.X2409'/>「<span class="high-light"><spring:message
                                        code='LB.X2410'/></span>」<spring:message code='LB.X2411'/>
                                </p>
                            </div>
                            <!-- 中文姓名 -->
                            <div id="nameN" class="ttb-input-item row">
	                        	<span class="input-title">
	                        		<label>
	                        			<h4><spring:message code='LB.D0049'/><span class="high-light">*</span></h4>
	                        		</label>
	                        	</span>
                                <span class="input-block">
	                        		<div class="ttb-input">
	                        			<input type="text" class="text-input" name="CPRIMCHNAME" id="CPRIMCHNAME"
                                               placeholder="<spring:message code='LB.X2044' />" size="6" maxlength="5"/>
	                        			<span class="input-remarks"><spring:message code='LB.X1982'/></span>
                                        <!-- 請輸入與您身分證姓名相同的中文姓名，以利審核的進行。 -->
	                        		</div>
	                        	</span>
                            </div>
                            <!-- 英文姓名 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code='LB.D0050'/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                    	<input type="text" class="text-input" name="CPRIMENGNAME" id="CPRIMENGNAME"
                                               placeholder="<spring:message code='LB.X2045' />" size="25" maxlength="24"
                                               onkeyup="checkinput(this.id)"/>
										<span class="input-remarks"><spring:message code='LB.X1983'/></span>
                                        <!-- 請輸入與您護照相同的英文名字 -->
                                    </div>
                                </span>
                            </div>
                            <!-- 更換過中文姓名 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                    	<!-- 更換過中文姓名 -->
                                        <h4><spring:message code='LB.X1984'/><span class="high-light">*</span></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <label class="radio-block"
                                               for="CHENAMEN"><spring:message code='LB.D0034_3' /><!-- 否 -->
                                            <input type="radio" name="CHENAME" id="CHENAMEN" value="2"/>
                                            <span class="ttb-radio"></span>
                                        </label>
                                        <label class="radio-block"
                                               for="CHENAMEY"><spring:message code='LB.D0034_2' /><!-- 是 -->
                                            <input type="radio" name="CHENAME" id="CHENAMEY" value="1"/>
                                            <span class="ttb-radio"></span>
                                        </label>
                                        <input type="text" id="CHENAMECHK" class="text-input"
                                               style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;"/>
                                    </div>
                                </span>
                            </div>
                            <!-- 原住民姓名 -->
                            <div id="nameAbo" class="ttb-input-item row">
	                        	<span class="input-title">
	                        		<label>
	                        			<h4><spring:message code='LB.X2412'/></h4>
	                        		</label>
	                        	</span>
                                <span class="input-block">
	                        		<div class="ttb-input">
	                        			<input type="text" class="text-input" name="CUSNAME" id="CUSNAME"
                                               placeholder="<spring:message code='LB.X2413' />" maxlength="50"/>
	                        		</div>
	                        	</span>
                            </div>
                            <!-- 羅馬拼音姓名 -->
                            <div id="nameRP" class="ttb-input-item row">
	                        	<span class="input-title">
	                        		<label>
	                        			<h4><spring:message code='LB.X2414'/></h4>
	                        		</label>
	                        	</span>
                                <span class="input-block">
	                        		<div class="ttb-input">
	                        			<input type="text" class="text-input" name="ROMANAME" id="ROMANAME"
                                               placeholder="<spring:message code='LB.X2415' />" maxlength="50"
                                               onkeyup="checkinput(this.id)"/>
	                        		</div>
	                        	</span>
                            </div>
                            <!-- 出生日期 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code='LB.D0582'/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                    	<span>
                                    		<font id="BD"></font>
                                    	</span>
                                    </div>
                                </span>
                            </div>
                            <!--身分證字號-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code='LB.D0581'/><span class="high-light">*</span></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                       <span><font id="IDnum"></font></span>
                                       <input type="hidden" name="CPRIMID" id="CPRIMID"/>
                                    </div>
                                </span>
                            </div>
                            <!--身分證補換發資料-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code='LB.X2435'/><span class="high-light">*</span></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
										<!-- 年 -->
										<select name="CHDATEY" id="CHDATEY"
                                                class="custom-select select-input input-width-100">
											<option value="#">---</option>
										</select>
                                        <!-- 月 -->
										<select name="CHDATEM" id="CHDATEM"
                                                class="custom-select select-input input-width-60">
											<option value="#">---</option>
										</select>
                                        <!-- 日 -->
										<select name="CHDATED" id="CHDATED"
                                                class="custom-select select-input input-width-60">
											<option value="#">---</option>
										</select>
										<input type="text" id="checkDD"
                                               style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;"/>
                                    </div>
									<div class="ttb-input">
										<select name="CITYCHA" id="CITYCHA" class="custom-select select-input">
							          		<option value="#"><spring:message code="LB.W1670"/></option>
										</select>
									</div>
									<div class="ttb-input">
										<div class="ttb-input">
	                                        <label class="check-block"><spring:message code='LB.D1113'/>
	                                            <input type="checkbox" name="CHTYPE" value="1" onclick="CHATYPE(this)">
	                                            <span class="ttb-check"></span>
	                                        </label>
	                                        <label class="check-block"><spring:message code='LB.W1665'/>
	                                            <input type="checkbox" name="CHTYPE" value="2" onclick="CHATYPE(this)">
	                                            <span class="ttb-check"></span>
	                                        </label>
	                                        <label class="check-block"><spring:message code='LB.W1664'/>
	                                            <input type="checkbox" name="CHTYPE" value="3" onclick="CHATYPE(this)">
	                                            <span class="ttb-check"></span>
	                                        </label>
	                                    </div>
	                                        <input type="text" id="checkTYPE"
                                                   style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;"/>
                               			<span class="input-remarks"><spring:message code='LB.X2436'/></span>
									</div>
                                </span>
                            </div>
                            <!--國籍-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code='LB.X0205'/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                    	<select class="custom-select select-input" name="CTRYDESC1" id="CTRYDESC1">
                                    	</select>
                                    	<input type="hidden" id="CTRYDESC" name="CTRYDESC">
<%--                                     	<input id="CTRYDESC" name="CTRYDESC" type="text"  class="text-input" size="15" maxlength="15" value="（<spring:message code="LB.X1647" />）"/> --%>
<%-- 										<input type="button" value="<spring:message code="LB.Menu" />" id="COUNTRYMENU"  class="ttb-sm-btn btn-flat-orange" /> --%>
                                    </div>
                                </span>
                            </div>
                            <!-- 教育程度 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D0055"/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <select class="custom-select select-input" name="MPRIMEDUCATION"
                                                id="MPRIMEDUCATION">
<%--                                             <option value="#">---<spring:message code="LB.X0543" />---</option><!-- 請選擇教育程度 --> --%>
                                            <option value="#"><spring:message code="LB.Select"/></option>
                                            <!-- 請選擇教育程度 -->
								 			<option value="1"><spring:message code="LB.D0588"/></option><!-- 博士 -->
								 			<option value="2"><spring:message code="LB.D0589"/></option><!-- 碩士 -->
								 			<option value="3"><spring:message code="LB.D0590"/></option><!-- 大學 -->
								 			<option value="4"><spring:message code="LB.D0591"/></option><!-- 專科 -->
								 			<option value="5"><spring:message code="LB.D0592"/></option><!-- 高中職 -->
								 			<option value="6"><spring:message code="LB.D0572"/></option><!-- 其他 -->
                                        </select>
                                    </div>
                                </span>
                            </div>
                            <!-- 婚姻狀況 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D0057"/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <label class="radio-block" for="MPRIMMARRIAGE1">
                                            <input type="radio" name="MPRIMMARRIAGE" id="MPRIMMARRIAGE1" value="1"/>
                                            <span class="ttb-radio"></span><spring:message code="LB.D0595" /><!-- 已婚 -->
                                        </label>
                                        <label class="radio-block" for="MPRIMMARRIAGE2">
                                            <input type="radio" name="MPRIMMARRIAGE" id="MPRIMMARRIAGE2" value="2"/>
                                            <span class="ttb-radio"></span><spring:message code="LB.D0596" /><!-- 未婚 -->
                                        </label>
                                        <label class="radio-block" for="MPRIMMARRIAGE3">
                                            <input type="radio" name="MPRIMMARRIAGE" id="MPRIMMARRIAGE3" value="3"/>
                                            <span class="ttb-radio"></span><spring:message code="LB.D0572" /><!-- 其他 -->
                                        </label>
                                        <input type="text" id="MPRIMMARRIAGECHK" class="text-input"
                                               style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;"/>
                                    </div>
                                </span>
                            </div>
                            <!--現居房屋 *-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code='LB.D0067'/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <select class="custom-select select-input" name="MPRIMADDR1COND"
                                                id="MPRIMADDR1COND">
<%--                                             <option value="#">---<spring:message code="LB.X0547" />---</option><!-- 請選擇居住房子狀況 --> --%>
                                            <option value="#"><spring:message code="LB.Select"/></option>
                                            <!-- 請選擇居住房子狀況 -->
								 			<option value="1"><spring:message code="LB.X0548"/></option><!-- 本人或配偶持有 -->
								 			<option value="2"><spring:message code="LB.X0549"/></option><!-- 直系親屬持有 -->
								 			<option value="3"><spring:message code="LB.X0550"/></option><!-- 租賃或宿舍 -->
                                        </select>
                                    </div>
                                </span>
                            </div>
                            <!--年收入 *-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code='LB.D0625_1'/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                    <!-- 請輸入年收入 (新臺幣) -->
                                    	<input type="text" class="text-input" name="CPRIMSALARY" id="CPRIMSALARY"
                                               placeholder="<spring:message code='LB.X1985' />" size="7" maxlength="6"/>
										<span class="input-unit m-0"><spring:message code='LB.D0088_2'/></span>
										<input type="text" id="CPRIMSALARYchk"
                                               style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;"/>
                                    </div>
                                </span>
                            </div>
                            <div class="classification-block">
                                <!-- 聯絡資訊 -->
                                <p><spring:message code='LB.X1986'/></p>
                                <%--                                 <p>( <span class="high-light">*</span> <spring:message code='LB.D0557' />)</p><!-- 為必填 --> --%>
                            </div>
                            <!-- E-mail -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4>E-mail </h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
<%--                                     	<c:if test="${result_data.data.requestParam.oldcardowner == 'Y'}"> --%>
<%--                                         	<input type="text" class="text-input" name="CPRIMEMAIL" id="CPRIMEMAIL" value="${result_data.data.requestParam.DPMYEMAIL}" size="31" maxlength="30"/> --%>
<%--                                         </c:if> --%>
<%--                                         <c:if test="${result_data.data.requestParam.oldcardowner == 'N'}"> --%>
	                                        <input type="text" class="text-input" name="CPRIMEMAIL" id="CPRIMEMAIL"
                                                   value="" placeholder="<spring:message code='LB.X2038' />" size="31"
                                                   maxlength="30"/>
<%-- 	                                   	</c:if> --%>
                                    </div>
                                </span>
                            </div>
                            <!-- 往來產品之預期金額 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code='LB.X2416'/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <input type="text" class="text-input" name="PAYMONEY" id="PAYMONEY" value=""
                                               placeholder="<spring:message code='LB.X2417' />" size="11"
                                               maxlength="3"/>
                                        <span class="input-unit m-0"><spring:message code='LB.D0088_2'/></span>
                                    </div>
                                </span>
                            </div>
                            <!-- 行動電話 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code='LB.D0069'/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
<%--                                     	<c:if test="${result_data.data.requestParam.oldcardowner == 'Y'}"> --%>
<%-- 	                                        <input type="text" class="text-input" name="CPRIMCELLULANO1" id="CPRIMCELLULANO1" value="${result_data.data.requestParam.MOBTEL}" size="11" maxlength="10"/> --%>
<%--                                     	</c:if> --%>
<%--                                     	<c:if test="${result_data.data.requestParam.oldcardowner == 'N'}"> --%>
	                                        <input type="text" class="text-input" name="CPRIMCELLULANO1"
                                                   id="CPRIMCELLULANO1" value=""
                                                   placeholder="<spring:message code='LB.X1989' />：0912345678" size="11"
                                                   maxlength="10"/>
<%--                                     	</c:if> --%>
                                    </div>
                                </span>
                            </div>
                            <!-- 居住地電話 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code='LB.D1127'/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <label class="check-block" for="samemobtel">同行動電話
                                            <input type="checkbox" name="samemobtel" id="samemobtel"
                                                   onclick="mobtohome()">
                                            <span class="ttb-check"></span>
                                        </label>
                                    </div>
                                    <div class="ttb-input">
<%--                                     	<c:if test="${result_data.data.requestParam.oldcardowner == 'Y'}"> --%>
<%--                                         	<input type="text" class="text-input" name="CPRIMHOMETELNO" id="CPRIMHOMETELNO" value="${result_data.data.requestParam.TEL11}${result_data.data.requestParam.TEL12}" maxlength="11"/> --%>
<%--                                     	</c:if> --%>
<%--                                     	<c:if test="${result_data.data.requestParam.oldcardowner == 'N'}"> --%>
                                        	<input type="text" class="text-input" name="CPRIMHOMETELNO"
                                                   id="CPRIMHOMETELNO" value="" maxlength="10"/>
                                        	<input type="hidden" name="CPRIMHOMETELNO1A" id="CPRIMHOMETELNO1A" value=""
                                                   maxlength="3"/>
                                        	<input type="hidden" name="CPRIMHOMETELNO1B" id="CPRIMHOMETELNO1B" value=""
                                                   maxlength="10"/>
<%--                                         </c:if> --%>
                                    </div>
                                </span>
                            </div>
                            <!--戶籍電話 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code='LB.D0062'/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <label class="check-block"
                                               for="samehometel"><spring:message code='LB.X1988' /><!-- 同居住地電話 -->
                                            <input type="checkbox" name="samehometel" id="samehometel"
                                                   onclick="samehome()">
                                            <span class="ttb-check"></span>
                                        </label>
                                    </div>
                                    <div class="ttb-input">
                                        <input type="text" class="text-input" name="CPRIMHOMETELNO2"
                                               id="CPRIMHOMETELNO2" value="" maxlength="10">
                                        <input type="hidden" name="CPRIMHOMETELNO2A" id="CPRIMHOMETELNO2A" value=""
                                               maxlength="3">
                                        <input type="hidden" name="CPRIMHOMETELNO2B" id="CPRIMHOMETELNO2B" value=""
                                               maxlength="10">
                                    </div>
                                </span>
                            </div>
                            <!-- 居住地址 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D0063"/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input" id="twzipcode">
										<span data-role="county"
                                              data-style="custom-select select-input input-width-125"></span>
										<span data-role="district"
                                              data-style="custom-select select-input input-width-100"></span>
										<span data-role="zipcode" data-name="ZIP2" data-style="zipcode"
                                              style="display: none;"></span>
									</div>
                                    <div class="ttb-input">
                                    	<!-- 請輸入居住地址 -->
                                        <input type="text" class="text-input" name="ADDR2" id="ADDR2" maxlength="100"
                                               value="" placeholder="<spring:message code='LB.X1990' />">
                                    </div>
                                </span>
                            </div>
                            <!-- 戶籍地址 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D0058"/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <label class="check-block"
                                               for="sameaddr"><spring:message code="LB.X0552" /><!-- 同居住地址 -->
                                            <input type="checkbox" name="sameaddr" id="sameaddr" onclick="doquery91()">
                                            <span class="ttb-check"></span>
                                        </label>
                                    </div>
                                    <div class="ttb-input" id="twzipcode1">
										<span data-role="county"
                                              data-style="custom-select select-input input-width-125"></span>
										<span data-role="district"
                                              data-style="custom-select select-input input-width-100"></span>
										<span data-role="zipcode" data-name="ZIP1" data-style="zipcode"
                                              style="display: none;"></span>
									</div>
                                    <div class="ttb-input">
                                    	<!-- 請輸入戶籍地址 -->
                                        <input type="text" class="text-input" name="ADDR1" id="ADDR1" maxlength="100"
                                               value="" placeholder="<spring:message code='LB.X1991' />"/>
                                    </div>
                                </span>
                            </div>
                            <div class="classification-block">
                                <!-- 職業資訊 -->
                                <p><spring:message code='LB.X1992'/></p>
                                <%--                                 <p>( <span class="high-light">*</span> <spring:message code='LB.D0557' />)</p><!-- 為必填 --> --%>
                            </div>
                            <!-- 職業類別 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code='LB.X2437'/><span class="high-light">*</span></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <select class="custom-select select-input" name="CPRIMJOBTYPE"
                                                id="CPRIMJOBTYPE">
                                            <option value="#"><spring:message code='LB.X2447'/></option><!-- 請選擇職業類別 -->
                                            <option value="061100"><spring:message code='LB.X2439'/></option>
                                            <!-- 軍官、軍人 -->
                                            <option value="061200"><spring:message code='LB.X2440'/></option>
                                            <!-- 警官、員警 -->
                                            <option value="061300"><spring:message code='LB.D0873'/></option>
                                            <!-- 其它公共行政業 -->
                                            <option value="061400"><spring:message code='LB.D0874'/></option>
                                            <!-- 教育業 -->
                                            <option value="061410"><spring:message code='LB.D0081'/></option><!-- 學生 -->
                                            <option value="061500"><spring:message code='LB.D0876'/></option>
                                            <!-- 工、商及服務業-->
                                            <option value="0615A0"><spring:message code='LB.D0877'/></option>
                                            <!-- 農林漁牧業 -->
                                            <option value="0615B0"><spring:message code='LB.D0878'/></option>
                                            <!-- 礦石及土石採取業 -->
                                            <option value="0615C0"><spring:message code='LB.D0879'/></option>
                                            <!-- 製造業 -->
                                            <option value="0615D0"><spring:message code='LB.D0880'/></option>
                                            <!-- 水電燃氣業 -->
                                            <option value="0615E0"><spring:message code='LB.D0881'/></option>
                                            <!-- 營造業 -->
                                            <option value="0615F0"><spring:message code='LB.D0882'/></option>
                                            <!-- 批發及零售業 -->
                                            <option value="0615G0"><spring:message code='LB.D0883'/></option>
                                            <!-- 住宿及餐飲業 -->
                                            <option value="0615H0"><spring:message code='LB.D0884'/></option>
                                            <!-- 運輸、倉儲及通信業 -->
                                            <option value="0615I0"><spring:message code='LB.D0885'/></option>
                                            <!-- 金融及保險業 -->
                                            <option value="0615J0"><spring:message code='LB.D0886'/></option>
                                            <!-- 不動產及租賃業 -->
                                            <option value="061610"><spring:message code='LB.X2441'/></option>
                                            <!-- 其他專業服務業 (建築、電腦資訊、設計、顧問、研發、醫護、社服等服務業) -->
                                            <option value="061620"><spring:message code='LB.X2442'/></option>
                                            <!-- 技術服務業 (出版、廣告、影視、休閒、保全、環保、維修、宗教、團體、美髮、殯葬、停車場…等) -->
                                            <option value="069999"><spring:message code='LB.D0899'/></option>
                                            <!-- 非法人組織授信戶負責人 -->
                                            <option value="061630"><spring:message code='LB.D0889'/></option>
                                            <!-- 特定專業服務業(律師、會計師、地政士、公證人、記帳士、記帳及報稅代理人) -->
                                            <option value="061640"><spring:message code='LB.D0890'/></option>
                                            <!-- 特定專業服務業(受聘於專業服務業之行政事務職員) -->
                                            <option value="061650"><spring:message code='LB.D0891'/></option>
                                            <!-- 銀樓業 (包含珠寶、鐘錶及貴金屬之製造、批發及零售) -->
                                            <option value="061660"><spring:message code='LB.D0892'/></option>
                                            <!-- 虛擬貨幣交易服務業 -->
                                            <option value="061670"><spring:message code='LB.D0893'/></option>
                                            <!-- 博弈業 -->
                                            <option value="061680"><spring:message code='LB.D0894'/></option>
                                            <!-- 國防武器或戰爭設備相關行業(軍火) -->
                                            <option value="061690"><spring:message code='LB.D0082'/></option><!-- 家管 -->
                                            <option value="061691"><spring:message code='LB.D0083'/></option>
                                            <!-- 自由業 -->
                                            <option value="061692"><spring:message code='LB.X0571'/></option><!-- 無業 -->
                                            <option value="061700"><spring:message code='LB.D0572'/></option><!-- 其他 -->
                                        </select>
                                    </div>
                                </span>
                            </div>
                            <!-- 職位名稱 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code='LB.X2438'/><span class="high-light">*</span></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <select class="custom-select select-input" name="CPRIMJOBTITLE"
                                                id="CPRIMJOBTITLE">
                                            <option value="#"><spring:message code="LB.X2448"/></option><!-- 請選擇職位名稱 -->
											<option value="01"><spring:message code='LB.X2443'/></option>
                                            <!-- 董事長/負責人 -->
											<option value="02"><spring:message code='LB.X2444'/></option> <!--總經理 -->
											<option value="03"><spring:message code="LB.X0561"/></option><!-- 主管 -->
											<option value="04"><spring:message code="LB.X0588"/></option><!-- 專業人員 -->
											<option value="05"><spring:message code='LB.X2445'/></option><!-- 職員 -->
											<option value="06"><spring:message code='LB.X2446'/></option>
                                            <!-- 業務/服務人員 -->
											<option value="07"><spring:message code='LB.D0572'/></option><!-- 其他 -->
                                        </select>
                                    </div>
                                </span>
                            </div>
                            <!--公司/學校名稱 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code='LB.X1993'/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                    	<!-- 請輸入公司/學校名稱 -->
                                        <input type="text" class="text-input" name="CPRIMCOMPANY" id="CPRIMCOMPANY"
                                               value="" placeholder="<spring:message code='LB.X1994' />" size="16"
                                               maxlength="16"/>
                                    </div>
                                </span>
                            </div>

                            <!--公司/學校電話 *-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code='LB.X1981'/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <input type="text" class="text-input" name="CPRIMOFFICETELNO1"
                                               id="CPRIMOFFICETELNO1" value=""
                                               placeholder="<spring:message code='LB.X1989' />：02-12345678"
                                               maxlength="11" onchange="changeMatchPhone('CPRIMOFFICETELNO1')"/>
                                        <spring:message code="LB.D0095"/>
                                        <input type="text" id="CPRIMOFFICETELNO1C" name="CPRIMOFFICETELNO1C"
                                               class="text-input card-input" value="" maxLength="4"/>
                                        <input type="text" id="CPRIMOFFICETELNO1chk"
                                               style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;"/>
                                        <input type="hidden" name="CPRIMOFFICETELNO1A" id="CPRIMOFFICETELNO1A" value=""
                                               maxlength="3"/>
                                        <input type="hidden" name="CPRIMOFFICETELNO1B" id="CPRIMOFFICETELNO1B" value=""
                                               maxlength="8"/>
                                    </div>
                                </span>
                            </div>
                            <!--公司地址 *-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D0090"/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input" id="twzipcode2">
										<span data-role="county"
                                              data-style="custom-select select-input input-width-125"></span>
										<span data-role="district"
                                              data-style="custom-select select-input input-width-100"></span>
										<span data-role="zipcode" data-name="ZIP4" data-style="zipcode"
                                              style="display: none;"></span>
									</div>
                                    <div class="ttb-input">
                                    	<!-- 請輸入公司地址 -->
                                        <input type="text" class="text-input" name="ADDR4" id="ADDR4" maxlength="100"
                                               value="" placeholder="<spring:message code='LB.X1995' />">
                                    </div>
                                </span>
                            </div>
                            <!--年資 *-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D0089_1"/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <select class="custom-select select-input input-width-125" name="WORKYEARS"
                                                id="WORKYEARS">
                                            <option value="#"><spring:message code="LB.D0089_2"/></option><!-- 年 -->
                                        </select>
                                        <select class="custom-select select-input input-width-100" name="WORKMONTHS"
                                                id="WORKMONTHS">
                                            <option value="#"><spring:message code="LB.D0089_3"/></option><!-- 月 -->
                                        </select>
                                    </div>
                                </span>
                            </div>
                        </div>
                        <input type="button" id="CMBACK" value="<spring:message code="LB.X0318" />"
                               class="ttb-button btn-flat-gray"/><!-- 上一步 -->
                        <input type="button" id="CMSUBMIT" value="<spring:message code="LB.X0080" />"
                               class="ttb-button btn-flat-orange"/><!-- 下一步 -->
                    </div>
                </div>
            </form>
        </section>
    </main>
</div>
<%@ include file="../index/footer.jsp" %>
</body>
</html>