<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<!-- 交易機制所需JS -->
<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
<!--舊版驗證-->
<script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
<script type="text/JavaScript">
$(document).ready(function() {
	// 開始查詢資料並完成畫面
	setTimeout("init()", 20);
});
function init(){
	$("#CMSUBMIT").click(function(){
		$("#formId").attr("action","${__ctx}/CREDIT/APPLY/upload_creditcard_identity_result");
		initBlockUI();
		$("#formId").submit();
	});
	$("#backButton").click(function(){
		$("#back").val('Y');
		$("#formId").attr("action","${__ctx}/CREDIT/APPLY/upload_creditcard_identity_p2");
		$("#formId").submit();
	});
}

</script>
</head>
<body>
<!-- header -->
<header>
	<%@ include file="../index/header_logout.jsp"%>
</header>	
 
	<!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<!--信用卡 -->
			<li class="ttb-breadcrumb-item"><spring:message code= "LB.Credit_Card" /></a></li>
			<!--信用卡補上傳資料 -->
			<li class="ttb-breadcrumb-item"><a href="#"><spring:message code= "LB.onlineapply_creditcard_progress_TITLE" /></a></li>
		</ol>
	</nav>

	<div class="content row">
		<!-- 主頁內容  -->
	<main class="col-12">
		<!--主頁內容-->
		<section id="main-content" class="container">
			<h2><spring:message code= "LB.onlineapply_creditcard_progress_TITLE" /></h2>
			<div id="step-bar">
				<ul>
					<li class="finished"><spring:message code="LB.D0851" /></li><!-- 身份驗證 -->
					<li class="finished"><spring:message code="LB.onlineapply_creditcard_progress_OTP" /></li><!-- OTP驗證 -->
					<li class="finished"><spring:message code="LB.D0359" /></li><!-- 查詢結果 -->
					<li class="finished"><spring:message code="LB.onlineapply_creditcard_progress_UploadDocuments" /></li><!-- 上傳證件 -->
					<li class="active"><spring:message code="LB.Confirm_data" /></li><!-- 確認資料 -->
					<li class=""><spring:message code="LB.onlineapply_creditcard_progress_COMPLETE" /></li><!-- 完成補件 -->					
				</ul>
			</div>
			<form method="post" id="formId" enctype="multipart/form-data">
				<input type="hidden" name="ADOPID" value="NA03"/>
				<input type="hidden" name="back" id="back" />
			    <input type="hidden" id="ECERT" name="ECERT"/>
			    <input type="hidden" name="STATUS" value="0"/>
			    <input type="hidden" name="VERSION" value="10808"/>
				<input type="hidden" name="ISSUER" value="">
				<input type="hidden" name="ACNNO" value="">
				<input type="hidden" name="iSeqNo" value="">
				<input type="hidden" name="ICSEQ" value="">
				<input type="hidden" name="TAC" value="">
				<input type="hidden" name="TRMID" value="">
				<!--信用卡PDF會用到-->
				<input type="hidden" id="templatePath" name="templatePath" value=""/>
				<div class="main-content-block row">
					<div class="col-12">
						<div class="ttb-input-block">
							<div class="ttb-message">
								<!-- 確認資料 -->
                                <p><spring:message code="LB.Confirm_data" /></p>
                            </div>
                            <!-- 請確認以下申請資料 -->
							<p class="form-description"><spring:message code="LB.X2026" /></p>
                            <c:if test = "${sessionScope.pflg == '1'||sessionScope.pflg == '3'}">
	                            <div class="classification-block">
		                        	<!-- 身分證資料 -->
		                        	<p><spring:message code='LB.X2021' /></p>
		                       	</div>
		                       	<div class="photo-block">
		                        	<div>
										<c:if test="${ !result_data.data.FILE1.equals('') }">
											<img id="msgPic1" src="${__ctx}/upload/${result_data.data.FILE1}">
										</c:if>
										<c:if test="${ result_data.data.FILE1.equals('') }">
											<img src="${__ctx}/img/upload_empty.svg" />
										</c:if>
										<input type="hidden" name="FILE1" value="${result_data.data.FILE1}">
		                                <p><spring:message code="LB.D0111"/></p>
		                       		</div>
		                            <div>
										<c:if test="${ !result_data.data.FILE2.equals('') }">
											<img id="msgPic2" src="${__ctx}/upload/${result_data.data.FILE2}">
										</c:if>
										<c:if test="${ result_data.data.FILE2.equals('') }">
											<img src="${__ctx}/img/upload_empty.svg" />
										</c:if>
										<input type="hidden" name="FILE2" value="${result_data.data.FILE2}">
		                                <p><spring:message code="LB.D0118"/></p>
		                            </div>
		                      	</div>
	                       	</c:if>
	                       	<c:if test = "${sessionScope.pflg == '2'||sessionScope.pflg == '3'}">
		                       	<div class="classification-block">
		                        	<!-- 財力證明文件 -->
		                        	<p><spring:message code="LB.X2024"/></p>
		                        </div>
		                        <div class="photo-block" style="justify-content: flex-start; margin-left: 1rem;">
		                        	<ol style="list-style-type: decimal;">
		                        		<li>
		                        			<c:if test="${ !result_data.data.FILE3.equals('') }">
												<img id="msgPic3" src="${__ctx}/upload/${result_data.data.FILE3}">
											</c:if>
											<c:if test="${ result_data.data.FILE3.equals('') }">
												<img src="${__ctx}/img/upload_empty.svg" />
											</c:if>
											<input type="hidden" name="FILE3" value="${result_data.data.FILE3}">
			                                <p></p>
		                        		</li>
		                        		<li>
		                        			<c:if test="${ !result_data.data.FILE4.equals('') }">
												<img id="msgPic4" src="${__ctx}/upload/${result_data.data.FILE4}">
											</c:if>
											<c:if test="${ result_data.data.FILE4.equals('') }">
												<img src="${__ctx}/img/upload_empty.svg" />
											</c:if>
											<input type="hidden" name="FILE4" value="${result_data.data.FILE4}">
			                                <p></p>
		                        		</li>
		                        		<li>
		                        			<c:if test="${ !result_data.data.FILE5.equals('') }">
												<img id="msgPic5" src="${__ctx}/upload/${result_data.data.FILE5}">
											</c:if>
											<c:if test="${ result_data.data.FILE5.equals('') }">
												<img src="${__ctx}/img/upload_empty.svg" />
											</c:if>
											<input type="hidden" name="FILE5" value="${result_data.data.FILE5}">
			                                <p></p>
		                        		</li>
		                        	</ol>	                       		
		                       	</div>
							</c:if>
                       	</div>
						<input type="button" id="backButton" value="<spring:message code="LB.X0318" />" class="ttb-button btn-flat-gray"/><!-- 上一步 -->
						<input type="button" id="CMSUBMIT" value="<spring:message code="LB.X0302" />" class="ttb-button btn-flat-orange"/><!-- 送出申請 -->
					</div>
				</div>
			</form>
		</section>
	</main>
</div>
<%@ include file="../index/footer.jsp"%>
</body>
</html>