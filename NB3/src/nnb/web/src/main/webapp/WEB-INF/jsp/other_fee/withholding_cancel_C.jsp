<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">

	<script type="text/javascript">
	$(document).ready(function() {
		setTimeout("initDataTable()",100);
		// 開始跑下拉選單並完成畫面
		setTimeout("init()", 400);

	});
	function init(){
		$("#formId").validationEngine({binded: false,promptPosition: "inline"});
		
		$("#CMSUBMIT").click(function(e){
			//打開驗證欄位
			$("#hideblock").show();
			
			e = e || window.event;
		
			if(!$('#formId').validationEngine('validate')){
	        	e.preventDefault();
 			}
			else{
				
				rowdata = $("input[name='ROWDATA']:radio:checked").val().split(",");
				$("#CARDNUM").val(rowdata[0]);
				$("#TYPE").val(rowdata[1]);
				$("#CUSNUM").val(rowdata[2]);
				$("#TYPE_str").val(rowdata[3]);
 				$("#formId").validationEngine('detach');
 				$("#formId").attr("action","${__ctx}/OTHER/FEE/withholding_cancel_C_confirm");
 	  			$("#formId").submit(); 
 			}		
  		});
	}
	</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 自動扣繳服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2360" /></li>
    <!-- 自動扣繳查詢/取消     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0768" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2><spring:message code="LB.W0768" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				
				<form id="formId" method="post" action="">
				<input type="hidden" name="ADOPID" id="ADOPID" value="N815">
				<input type="hidden" name="ITMNUM" id="ITMNUM" value="2">
				<input type="hidden" name="CARDNUM" id="CARDNUM" value="">
				<input type="hidden" name="TYPE" id="TYPE" value="">
				<input type="hidden" name="TYPE_str" id="TYPE_str" value="">
				<input type="hidden" name="CUSNUM" id="CUSNUM" value="">
				<input type="hidden" name="action" id="ADOPID" value="forward">

					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-input-block">
							
	                            <ul class="ttb-result-list">
	                                <li><!-- 查詢時間 -->
		                            	<h3><spring:message code="LB.Inquiry_time" /></h3>
		                            	<p>${withholding_cancel.data.CMQTIME}</p>
										<input type="hidden" name="CMQTIME" id="CMQTIME" value="${withholding_cancel.data.CMQTIME}">
		                            </li>
		                            <li><!-- 資料總數 : -->
		                            	<h3><spring:message code="LB.Total_records" /></h3>
		                            	<p>${withholding_cancel.data.RECNUM} <spring:message code="LB.Rows" /><!--筆 --></p>
		                            </li>
	                            </ul>
								<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
									<thead>
										<tr>
											<!-- 留白 -->
											<th></th> 
											<!-- 卡號-->
											<th><spring:message code="LB.Card_number" /></th>
											<!-- 查詢代繳類別-->
											<th><spring:message code="LB.X2265" /></th>
											<!-- 用戶編號-->
											<th><spring:message code="LB.W0691" /></th>
										</tr>
									</thead>
									<tbody>
									<c:forEach var="dataList" items="${withholding_cancel.data.REC}" varStatus="data">
										<tr>									
											<td class="text-center">
												<label class="radio-block">&nbsp;
													<input type="radio" name="ROWDATA" value="${dataList.CARDNUM},${dataList.TYPE},${dataList.CUSNUM},${dataList.TYPE_str}">
													<span class="ttb-radio"></span>
												</label>
											</td>								
											<td class="text-center">${dataList.CARDNUM}</td>
											<td class="text-center">
												<c:if test="${!dataList.TYPE_str.equals('')}">
													<spring:message code="${dataList.TYPE_str}" />
												</c:if>
											</td>
											<td class="text-center">${dataList.CUSNUM}</td>
										</tr>
									</c:forEach>
									</tbody>
								</table>
								
								<!-- 不在畫面上顯示的span -->
								<span class="hideblock">
									<input id="validate_ROWDATA" name="ROWDATA" type="radio"  
										class="validate[funcCall[validate_Radio['<spring:message code='LB.Alert196' />',ROWDATA]]]" 
										style="border:white; margin:0px; padding:0%; height: 0px; width: 0px; visibility: hidden;"/>
								</span>
								
							</div>
							<!-- 確定-->
							<input type="button" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
						</div>
					</div>
				</form>
			</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>