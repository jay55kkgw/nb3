<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js_u2.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<!-- <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script> -->
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function () {
			window.print();
		});
	</script>
</head>

<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br /><br />
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif" /></div>
	<br /><br />
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<br />
	<div>
		<!-- 查詢時間 -->
		<p>
			<spring:message code="LB.Inquiry_time" />：${CMQTIME}
		</p>
		<!-- 帳號 -->
		<p>
			<spring:message code="LB.Account"/>：${ACN}
		</p>
		<!-- 託收總張數 -->
		<p>
			<spring:message code= "LB.Total_number_of_Collection" /> ：${REMCNT}					
		</p>
		<!-- 託收總金額 -->
		<p>
			<spring:message code= "LB.Total_amount_of_Collection" /> ：${REMAMT}						
		</p>
		<!-- 已入帳總張數 -->
		<p>
			<spring:message code= "LB.Total_number_of_already_accounted" /> ：${VALCNT}						
		</p>
		<!-- 已入帳總金額 -->
		<p>
			<spring:message code= "LB.Total_amount_of_already_accounted" /> ：${VALAMT}						
		</p>
		<!-- 查詢項目 -->
		<p>
			<spring:message code= "LB.Query_item" />：${TYPE}
		</p>
		<!-- 查詢期間 -->
		<p>
			<spring:message code="LB.Inquiry_period_1" />：${CMPERIOD}
		</p>
			<table class="print">
				<thead>
					<tr>
						<th><spring:message code="LB.Account" /></th>
						<th>${collection_bills_result.data.ACN}</th>
					</tr>
					<tr>
<!-- 託收行 -->						<th data-breakpoints="xs sm"><spring:message code= "LB.Collection_bank" /></th>
<!-- 託收日 -->						<th data-breakpoints="xs sm"><spring:message code= "LB.Collection_date" /></th>
<!-- 票據號碼 -->					<th data-breakpoints="xs sm"><spring:message code= "LB.Checking_account" /></th>
<!-- 付款行代號 -->					<th data-breakpoints="xs sm"><spring:message code= "LB.Issuers_bank_code" /></th>
<!-- 付款人帳號 -->					<th data-breakpoints="xs sm"><spring:message code= "LB.Issuers_account_no" /></th>
<!-- 到期日 -->						<th data-breakpoints="xs sm"><spring:message code= "LB.Expired_date" /></th>
<!-- 票據金額 -->					<th data-breakpoints="xs sm"><spring:message code= "LB.Banknote_amount" /></th>
<!-- 入帳日 -->						<th data-breakpoints="xs sm"><spring:message code= "LB.Accounted_date" /></th>
<!-- 未入帳原因 -->					<th data-breakpoints="xs sm"><spring:message code= "LB.Unlogged_reason" /></th>	
<!-- 補充資料內容 -->					<th data-breakpoints="xs sm"><spring:message code= "LB.Supplementary_content" /></th>								
					</tr>
					</thead>
					<tbody>
					<c:forEach var="dataList" items="${dataListMap}">
					<tr>
					    <td class="text-center">${dataList.COLLBRH }</td>
					    <td class="text-center">${dataList.REMDATE }</td>
					    <td class="text-center">${dataList.CHKNUM }</td>
					    <td class="text-center">${dataList.BNKNUM }</td>
					    <td class="text-center">${dataList.PAYACN }</td>
					    <td class="text-center">${dataList.DUEDATE }</td>
					    <td class="text-right">${dataList.AMOUNT }</td>
					    <td class="text-center">${dataList.VALDATE }</td>
					    <td class="text-center">${dataList.FAILRSN }</td>
					     <td class="text-center">${dataList.DATA }</td>					               
					</tr>
					</c:forEach>
					</tbody>
			</table>
			<br /><br />		
	</div>
</body>

</html>