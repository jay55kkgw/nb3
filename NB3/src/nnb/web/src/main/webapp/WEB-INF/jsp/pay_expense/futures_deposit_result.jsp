<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	
	<script type="text/javascript">
		$(document).ready(function(){
			// 列印
			$("#printbtn").click(function(){
				var params = {
					"jspTemplateName":"futures_deposit_print",
					"jspTitle":"<spring:message code= "LB.W0599" />",
					"CMTXTIME":"${transfer_result_data.data.CMTXTIME }",
					"OUTACN":"${transfer_result_data.data.OUTACN }",
					"INTSACN":"${transfer_result_data.data.INTSACN }",
					"AMOUNT":"${transfer_result_data.data.AMOUNT }",
					"CMTRMEMO":"${transfer_result_data.data.CMTRMEMO }",
					"O_TOTBAL":"${transfer_result_data.data.O_TOTBAL }",
					"O_AVLBAL":"${transfer_result_data.data.O_AVLBAL }",
					"RESULT":"${transfer_result_data.result}"
				};
				openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
				
			});

		});
	</script>
</head>
<body>
    <!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	
	<c:if test="${transfer_result_data.result == true}">
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %> 
	</c:if>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 期貨保證金     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0599" /></li>
		</ol>
	</nav>

	
	<!-- 功能清單及登入資訊 -->
	<div class="content row">

		<!-- 功能清單 -->
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
			
				<!-- 功能名稱 -->
	            <h2><spring:message code="LB.W0599" /></h2>
	            <i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
	            
				<!-- 交易流程階段 -->
	            <div id="step-bar">
	                <ul>
	                    <li class="finished"><spring:message code="LB.Enter_data" /></li>
	                    <li class="finished"><spring:message code="LB.Confirm_data" /></li>
	                    <li class="active"><spring:message code="LB.Transaction_complete" /></li>
	                </ul>
	            </div>
	            
	            <!-- 功能內容 -->
				<div class="main-content-block row">
					<div class="col-12">
					<c:if test="${transfer_result_data.result == true}">
				        <h4 style="margin-top:10px;font-weight:bold;color:red">
							<spring:message code="LB.Payment_successful" />
						</h4>
						<div class="ttb-input-block">
							<!--交易時間區塊 -->
							<div class="ttb-input-item row">
							<!--交易時間  -->
							<span class="input-title">
								<label>
									<spring:message code="LB.Trading_time" /><!-- 交易時間 -->
								</label>
							</span>
							<span class="input-block">
								${transfer_result_data.data.CMTXTIME }
							</span>
							</div>
							<!--交易時間區塊END -->
							<!--轉出帳號區塊 -->
							<div class="ttb-input-item row">
							<!--轉出帳號  -->
							<span class="input-title">
								<label>
									<spring:message code="LB.Payers_account_no" />
								</label>
							</span>
							<span class="input-block">
								${transfer_result_data.data.OUTACN }
							</span>
							</div>
							<!--轉出帳號區塊END -->
							<!--期貨帳號區塊 -->
							<div class="ttb-input-item row">
								<!--期貨帳號  -->
								<span class="input-title"> 
									<label> <spring:message code="LB.W0602" /> </label>
								</span> 
								<span class="input-block">
									${transfer_result_data.data.INTSACN }
								</span>
							</div>
							<!--期貨帳號區塊END -->
							<!--繳款金額區塊 -->
							<div class="ttb-input-item row">
								<!--繳款金額  -->
								<span class="input-title"> 
									<label> <spring:message code="LB.Payment_amount" /> </label>
								</span> 
								<span class="input-block">
									<spring:message code="LB.NTD" />
		                            ${transfer_result_data.data.AMOUNT }
		                            <spring:message code="LB.Dollar" />
								</span>
							</div>
							<!--繳款金額區塊END -->
							<!--交易備註區塊 -->
							<div class="ttb-input-item row">
								<!--交易備註  -->
								<span class="input-title"> 
									<label> <spring:message code="LB.Transfer_note" /> </label>
								</span> 
								<span class="input-block">
									${transfer_result_data.data.CMTRMEMO }
								</span>
							</div>
							<!--交易備註區塊END -->
							<!--轉出帳號帳上餘額區塊 -->
							<div class="ttb-input-item row">
								<!--轉出帳號帳上餘額  -->
								<span class="input-title"> 
									<label> <spring:message code="LB.W0282" /> </label>
								</span> 
								<span class="input-block">
									<spring:message code="LB.NTD" />
		                            ${transfer_result_data.data.O_TOTBAL }
		                            <spring:message code="LB.Dollar" />
								</span>
							</div>
							<!--轉出帳號帳上餘額區塊END -->
							<!--轉出帳號可用餘額區塊 -->
							<div class="ttb-input-item row">
								<!--轉出帳號可用餘額 -->
								<span class="input-title"> 
									<label> <spring:message code="LB.Payers_available_balance" /> </label>
								</span> 
								<span class="input-block">
									<spring:message code="LB.NTD" />
		                           ${transfer_result_data.data.O_AVLBAL }
		                            <spring:message code="LB.Dollar" />
								</span>
							</div>
							<!--轉出帳號帳上餘額區塊END -->
						</div>
					</c:if>
					<!-- 列印 -->
					<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
				    </div>
				</div>
				
				<!-- 說明 -->
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>
					<li><spring:message code="LB.Futures_Deposit_P1_D2" /></li>
					<li><spring:message code="LB.Futures_Deposit_P3_D2" /></li>
					<li><spring:message code="LB.Futures_Deposit_P3_D3" /></li>
				</ol>
				
	        </section>    
		</main>
	</div>

	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>