<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="page2" class="card-detail-block">
	<div class="card-center-block">
		<!-- Q1 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-21" aria-expanded="true"
				aria-controls="popup1-21">
				<div class="row">
					<span class="col-1">Q1</span>
					<div class="col-11">
						<span>What features does online banking offer?</span>
					</div>
				</div>
			</a>
			<div id="popup1-21" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>Online banking offers ten types of functions, as described below:</p>
							<div style="overflow: auto">
								<table class="question-table">
									<thead>
										<tr>
											<th>Project</th>
											<th>Content</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th>Services Overview</th>
											<td style="text-align: left;">Account Overview</td>
										</tr>
										<tr>
											<th>NTD Service</th>
											<td class="white-spacing text-left">
												<ol class="list-decimal">
													<li data-num="1.">Account Inquiry: Demand Deposit Balance, Demand Deposit Detail, Collect of Virtual Account Detail</li>
													<li data-num="2.">Transfer: Transfer</li>
													<li data-num="3.">Transfer Record: Transfer Record Inquiry</li>
													<li data-num="4.">Scheduled Transfer: Query/Cancel Scheduled Transfer, Query Scheduled Transfer Result</li>
													<li data-num="5.">Time Deposit Service: Current Deposit to Time Deposit, Time Deposit Detail, Apply/Change Time Deposit Automatic Rollover, Renewal at Maturity of Time Deposit, Time Deposit Termination, Periodical Deposits and Lumpsum Payment (Deposit Monthly)</li>
													<li data-num="6.">Easy Management Account: Easy Management Account Detail, Easy Management Securities Delivery Detail, Easy Management Automatic Subscription Fund Detail</li>
													<li data-num="7.">Bill Inquiry: Bill for Collection (B/C), Insufficient Balance of Check Deposits Detail, Settlement of Check Detail</li>
													<li data-num="8.">Central Government Bond: Bond Balance, Bond Detail</li>
													<li data-num="9.">Pledged Time Deposit Function: Cancel Pledged Time Deposit Function </li>
												</ol>
											</td>
										</tr>
										<tr>
											<th>Foreign Currency Service</th>
											<td class="white-spacing text-left">
												<ol class="list-decimal">
													<li data-num="1.">Account Inquiry: Demand Deposit Balance, Demand Deposit Detail</li>
													<li data-num="2.">Foreign Exchange/Transfer: Foreign Exchange/Pre-designated Transfer </li>
													<li data-num="3.">Foreign Currency Transfer Record: Foreign Currency Transfer Record Inquiry </li>
													<li data-num="4.">Foreign Exchange Outward Remittance: Foreign Exchange Outward Remittance , Outward Remittance Query</li>
													<li data-num="5.">Foreign Exchange Inward Remittance: Inward Remittance Query, Foreign Exchange Inward Remittance Online Settle, Apply/Cancel Foreign Exchange Inward Remittance Online Settle</li>
													<li data-num="6.">Scheduled Transactions: Query/Cancel Scheduled Transfer, Scheduled Transfer Result Inquiry</li>
													<li data-num="7.">Time Deposit Service: Foreign Currency Current Deposit To Time Deposit, Time Deposit Detail, Time Deposit Termination, Apply/Change Time Deposit Automatic Rollover, Renewal at Maturity of Time Deposit</li>
													<li data-num="8.">Export/Import Service: Import to Order Inquiry, Export Bill Inquiry</li>
													<li data-num="9.">Letter of Credit Service: Letter of Credit Notification Inquiry, Open Letter of Credit Inquiry</li>
													<li data-num="10.">Collection Inquiry: Import/Export Collection Inquiry, Light Ticket Collection Inquiry</li>
												</ol>
											</td>
										</tr>
										<tr>
											<th>Payment and Pay Taxes</th>
											<td class="white-spacing text-left">
												<ol class="list-decimal">
													<li data-num="1.">Pay Taxes: Pay Taxes</li>
													<li data-num="2.">Public Business Fee: Taipower Bill, Taiwan Water Corporation Bill, Taipei Water Department Bill, Shin Shin Natural Gas Corporation Bill</li>
													<li data-num="3.">National Insuance Fee: Health Insurance/Supplementary Insurance Fee, Labor Insurance Fee, National Pension Insurance Fee, The 2nd-tier New Labor Pension Fee </li>
													<li data-num="4.">Tuition Fee: Tuition Fee</li>
													<li data-num="5.">Other Fees: Pay Futures Margine, Payment</li>
													<li data-num="6.">Application For Authorized Payment Of Othe Fee: Query/Cancel Authorized Automatic Payment, Taipower Bill, Taiwan Water Corporation Bill, Taipei Water Department Bill, Chunghwa Telecom Phone Bill, Health Insurance Fee, Labor Insurance Fee, The 2nd-tier New Labor Pension Fee, Labor Pension Fee, Parking Bill</li>
												</ol>
											</td>
										</tr>
										<tr>
											<th>Loan Service</th>
											<td class="white-spacing text-left">
												<ol class="list-decimal">
													<li data-num="1.">Microloan Application: Apply for Microloan, Microloan Application Inquiry</li>
													<li data-num="2.">Loan Inquiry: Loan Detail, Loan Detail (Next Period), Paid Loan Detail</li>
													<li data-num="3.">Mortgage Loan Interest Payment List: Mortgage Loan Interest Payment List</li>
												</ol>
											</td>
										</tr>
										<tr>
											<th>Funds</th>
											<td class="white-spacing text-left">
												<ol class="list-decimal">
													<li data-num="1.">Fund Inquiry: Fund Balance/Profit and Loss Inquiry, Fund Transaction Detail, Systematic Investment Agreement Data Inquiry, Domestic Fund Net Value Inquiry, Overseas Fund Net Value Inquiry, Trust Contrast Inquiry, Query/Cancel Scheduled Transaction</li>
													<li data-num="2.">Overseas Bond Inquiry: Overseas Bond Balance/Profit and Loss Inquiry, Overseas Bond Transaction Details</li>
													<li data-num="3.">Fund Transaction: Single Purchase, Systematic Investment, Conversion Transaction, Redemption Transaction, Modify Systematic Investment Convention, Setting Stop Loss/Take Profit Notice, Setting Automatic Redemption for SMART FUND</li>
													<li data-num="4.">Investment Survey (KYC): Investment Survey</li>
													<li data-num="5.">Statement of Funds Email: Apply for Statement of Funds Email, Reset Password, Change Password</li>
													<li data-num="6.">Overseas Trust Commodity Promotion: Apply for Product Promotion, Cancel Product Promotion</li>
												</ol>
											</td>
										</tr>
										<tr>
											<th>Gold Passbook</th>
											<td class="white-spacing text-left">
												<ol class="list-decimal">
													<li data-num="1.">Inquiry Service: Gold Passbook Balance, Gold Passbook Detail, Present Price Inquiry, Historical Price Inquiry, Query/Cancel Scheduled Transaction</li>
													<li data-num="2.">Gold Transaction: Purchase Gold, Resale Gold, Pay Regular Deduction Failure Fee</li>
													<li data-num="3.">Systematic Investment: Systematic Investment, Change Systematic Investment Plan, Systematic Investment Plan Inquiry</li>
													<li data-num="4.">Gold Passbook Account:  Apply for Gold Passbook Account,  Apply for Gold Passbook Online Transaction</li>
													<li data-num="5.">Investment Survey (KYC): Investment Survey</li>
												</ol>
											</td>
										</tr>
										<tr>
											<th>Credit Card</th>
											<td class="white-spacing text-left">
												<ol class="list-decimal">
													<li data-num="1.">Credit Card Application: Apply for Credit Card, Credit Card Application Progress Inquiry</li>
													<li data-num="2.">Credit Card Inquiry: Credit Card Overview, Un-billed Credit Card Transactions, Historical Statement Detail, Payment Record Inquiry</li>
													<li data-num="3.">Pay Credit Card Fees: Pay TBB Credit Card Fees, Apply/Cancel Auto-deductible</li>
													<li data-num="4.">E-Reconciliation Statement: Apply for E-Reconciliation Statement, Reset Password, Change Password</li>
													<li data-num="5.">Reconciliation Statement: Resend Reconciliation Statement</li>
													<li data-num="6.">Credit Card Service: Activate Credit Card, Deactivate Credit Card</li>
													<li data-num="7.">Advance Cash: Resend Password Letter</li>
													<li data-num="8.">Credit Card Installments: Sign the Credit Card Installment Agreement, Apply for Installment on Long-term Use of Recolving Credit</li>
												</ol>
											</td>
										</tr>
										<tr>
											<th>Online Services</th>
											<td class="white-spacing text-left">
												<ol class="list-decimal">
													<li data-num="1.">Mobile Banking Service: Enable Mobile Banking Service</li>
													<li data-num="2.">Pre-designated Receiving Account Service: Setting Pre-designated Receiving Account, Cancel Pre-designated Receiving Account</li>
													<li data-num="3.">E-Reconciliation Statement: Apply for E-Reconciliation Statement, Reset Password, Change Password</li>
													<li data-num="4.">Transfer Service: Apply/Cancel Transfer Service</li>
													<li data-num="5.">Deposit Balance Certificates: Apply for Deposit Balance Certificates</li>
													<li data-num="6.">Bill Service: Apply for Check Deposit Account, Blank Bill</li>
													<li data-num="7.">Credit Card Service: Apply for Credit Card, Credit Card Application Progress Inquiry, Sign the Credit Card Installment Agreement, Apply for Installment on Long-term Use of Recolving Credit</li>
													<li data-num="8.">Microloan Application: Apply for Microloan, Microloan Application Inquiry</li>
													<li data-num="9.">Fund Services: Apply for Fund Account</li>
													<li data-num="10.">Investment Survey (KYC): Investment Survey</li>
													<li data-num="11.">Overseas Trust Commodities: Apply for Product Promotion, Cancel Product Promotion</li>
													<li data-num="12.">Guardian Aegis: Apply for Guardian Aegis</li>
													<li data-num="13.">Financial Management Service: NTD Fixed Deposit Saving Deposit Trial Balance, NTD Fixed Deposit Saving Deposit Trial Balance, Foreign Exchange Time-Deposit Trial Balance, Loan Amortization Trial Balance, Annuity Plan Trial Balance, Systematic Investment Plan Trial Balance</li>
												</ol>
											</td>
										</tr>
										<tr>
											<th>Personal Services</th>
											<td class="white-spacing text-left">
												<ol class="list-decimal">
													<li data-num="1.">Account Overview Setting: Account Overview Setting</li>
													<li data-num="2.">Account Settings: Common Account Setting, Change User Name, Change Password, Unlock Online Banking Transaction Password, Setting Email</li>
													<li data-num="3.">Contact Information Settings: Change Current Account and Trust Business Address/Telephone, Change Import/Export/Exchange Contact Address/Phone, Change Credit Card Billing Address/Telephone</li>
													<li data-num="4.">Notification Service: Setting Notification Service, Setting Domestic NTD Remittance Notification</li>
													<li data-num="5.">Deactivate Credit Card/Account Service: Deactivate Credit Card Service, Apply for NTD Deposit Account Settlement, Apply for Foreign Currency Account Settlement</li>
													<li data-num="6.">Withholding Statements: Print Income Tax Withholding Statements</li>
												</ol>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q2 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-22" aria-expanded="true"
				aria-controls="popup1-22">
				<div class="row">
					<span class="col-1">Q2</span>
					<div class="col-11">
						<span>The maximum amount of online banking transactions?</span>
					</div>
				</div>
			</a>
			<div id="popup1-22" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>First, According to the current security mechanism of the Bank, the daily transaction amount for the designated and non-predesignated transfer accounts are as follows:</p>
							<div style="overflow: auto">
								<table class="question-table">
									<thead>
										<tr>
											<th rowspan="2">Security mechanism</th>
											<th colspan="2">Designated transfer</th>
											<th colspan="3">Non-predesignated transfer</th>
											<th colspan="3">Online designated transfer to account</th>
										</tr>
										<tr>
											<th>Self bank</th>
											<th>Interbank transfer</th>
											<th>Self bank</th>
											<th>Interbank transfer</th>
											<th>Merge</th>
											<th>Self bank</th>
											<th>Interbank transfer</th>
											<th>Merge</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th>Transaction password</th>
											<td rowspan="5">5 million</td>
											<td rowspan="5">2 million</td>
											<td>-</td>
											<td>-</td>
											<td>-</td>
											<td rowspan="5" colspan="3">Single amount 50,000<br>100,000 daily<br>200,000 yuan per month.</td>
										</tr>
										<tr>
											<th>Chip financial card</th>
											<td rowspan="4" colspan="3">Single amount 50,000<br>100,000 daily<br>200,000 yuan per month.</td>
										</tr>
										<tr>
											<th>Dynamic password card</th>
										</tr>
										<tr>
											<th>Aegis with care</th>
										</tr>
										<tr>
											<th>Taiwan Pay<br>(Fast Trading)</th>
										</tr>
										<tr>
											<th>Electronic Signature</th>
											<td>15 million</td>
											<td class="white-spacing">5 million(2 million yuan each)</td>
											<td>3 million</td>
											<td class="white-spacing">2 million(Excluding foreign exchange outward remittance)</td>
											<td>-</td>
											<td>3 million</td>
											<td>2 million</td>
											<td>-</td>
										</tr>
										<tr>
											<th>Remarks</th>
											<td colspan="8" class="white-spacing text-left">
												<ol class="list-decimal">
													<li data-num="1.">The above password / electronic signature / chip financial card / dynamic password card / Aegis shield daily transfer limit calculation combined with 20 million yuan as the upper limit.</li>
													<li data-num="2.">The foreign currency transaction limits are combined.</li>
													<li data-num="3.">The mobile banking transaction limit is calculated in conjunction with the general online banking.</li>
												</ol>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
									
							<div class="ttb-result-list terms">
								<p>Second, other transaction limits and related regulations:</p>
								<ol>
									<li>(A) Digital deposit account transfer limit: The total non-contracted transfer amount of the same customer, the first and second types of digital deposit accounts are limited to NT$50,000 per transaction, NT$100,000 per day, NT$100,000 per month RMB 200,000 yuan; after the customer visits the service branch to apply for the "appointed transfer" function, the daily transaction limit will be determined by different security machines based on the transaction limit of the bank's general deposit account transfer. For the third type of digital deposit account, the agreed, non-approved transfer and inter-bank transfer limit for my account are limited to NT$10,000 per transaction, NT$30,000 per day, and NT$30,000 per month. NT$50,000; the non-agreed transfer limit for transfer into my bank account is limited to NT$50,000 per transaction, NT$100,000 per day, and NT$200,000 per month; The transaction limits for transfers to my bank’s account and on-line agreed transfers shall be the same as the bank’s general deposit account.</li>
									<li>(B) Transferring each of the contracted regular deposits and non-contracted accounts of the comprehensive deposits into each account, and the cumulative limit for the month is 1 million. (Applicable from September 1, 104)</li>
									<li>(C) "Payment/Tax" category:
										<ol>
											<li data-num="1.">The  Inter-bank transfer transaction with the "Financial Card", "Dynamic Password Card" and "Aegis with care" method and the "Password Method (SSL Mechanism)" to calculate the limit of inter bank contracted transfer amount.</li>
											<li data-num="2.">The "electronic signature" method combines the calculation of the inter-bank transfer transaction with the electronic signature and non-contracted transfer.</li>
										</ol>
									</li>
									<li>(D) Fund purchase: The amount of funds transferred by the electronic transaction method (including Internet/voice) per person per business day, the total amount should not exceed the equivalent of NT$5 million.</li>
									<li>(E) Gold purchase: The minimum transaction volume is 1 gram per transaction, and the maximum number of transactions per day is 50,000 grams per account.</li>
									<li>(F) Foreign exchange transactions:
										<ol>
											<li data-num="1.">Foreign exchange transfer / foreign exchange outward remittance:
												<ol>
													<li data-num="(1)">The foreign currency and Taiwanese currency of the same household name transfer, the foreign currency purchase or settlement shall be accumulated separately. The daily cumulative maximum limit shall be less than NT$500,000, and the calculation shall be made in conjunction with the bank telephone transfer and the financial card.</li>
													<li data-num="(2)">Natural persons under the age of 20, the same foreign exchange deposit account, the transfer of accounts between different currencies, and the transfer of accounts between different accounts (including outward remittances) are calculated separately. The daily cumulative maximum limit should be less than NT$500,000.</li>
													<li data-num="(3)">Individuals who purchase or close the RMB should accumulate separately, and the amount of each person's daily purchase through the account must not exceed CNY 20,000.</li>
													<li data-num="(4)">Individuals with a National Identity Card of the Republic of China can apply for RMB remittances to the mainland, and the daily limit for each person is CNY 80,000.</li>
												</ol>
												* If (3)(4) above refers to RMB traders, individual households should still be subject to a limit of CNY 20,000.
											</li>
											<li data-num="2.">Foreign exchange inward remittance online payment:
												<ol>
													<li data-num="(1)">The deposit into the Taiwanese currency account should be less than NT$500,000.</li>
													<li data-num="(2)">There is no limit on the amount of deposits into external accounts, but natural persons under the age of 20 should be lower than the equivalent of NT$500,000 per day.</li>
												</ol>
											</li>
										</ol>
									</li>
									<li>(G) Transferring the account without transaction amount limit items:
										<ol>
											<li data-num="1.">Transfer transactions for different currencies in the same foreign exchange deposit account.</li>
											<li data-num="2.">Transfer transactions between demand deposits and time deposits in the same foreign exchange comprehensive deposit account.</li>											
										</ol>
									</li>
								</ol>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q3 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-23" aria-expanded="true"
				aria-controls="popup1-23">
				<div class="row">
					<span class="col-1">Q3</span>
					<div class="col-11">
						<span>Online banking service hours</span>
					</div>
				</div>
			</a>
			<div id="popup1-23" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>The foreign exchange service is available from 9:10 am to 3:30 pm on the bank business day from Monday to Friday. Other projects are for 24-hour service, but the transfer time for inter-bank transfers is subject to the transfer operations.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q4 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-24" aria-expanded="true"
				aria-controls="popup1-24">
				<div class="row">
					<span class="col-1">Q4</span>
					<div class="col-11">
						<span>Can I find all the accounts of own (the company) if I apply for online banking?</span>
					</div>
				</div>
			</a>
			<div id="popup1-24" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>Yes, you can find out all the account transaction details of your own (the company) opened in the Bank.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q5 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-25" aria-expanded="true"
				aria-controls="popup1-25">
				<div class="row">
					<span class="col-1">Q5</span>
					<div class="col-11">
						<span>Does the online bank provide non-contracted transfer function?</span>
					</div>
				</div>
			</a>
			<div id="popup1-25" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div class="ttb-result-list terms">
								<p>Yes, customers can use the "Electronic Signature" or "Bank's Chip Financial Card" + "Card Reader" to execute non-contracted transfer transactions.</p>
								<ol>
									<li data-num="1.">Electronic signature: You need to apply for financial XML certificate, please follow one: [Application eligibility] A04 to apply for and download the certificate.</li>
									<li data-num="2.">The Bank's financial chip card:Bank financial support chip card+Card reader and the chip card with the financial function by non-consensual transfer:
										<ol>
											<li data-num="(1)">Cabinet application for general online banking customers, the main account of the chip financial card to implement NTD non-contracted transfer transactions and other transaction services.</li>
											<li data-num="(2)">Customers who apply online for the "Bank's Chip Financial Card" only have the inquiry function. If you have a non-contracted transfer function for the "Bank's Chip Financial Card", you can log in to the general online banking to apply for the trading service function. That is, the main account of the chip financial card can execute the NTD non-contracted transfer transaction and other transaction services.</li>											
										</ol>
									</li>											
								</ol>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q6 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-26" aria-expanded="true"
				aria-controls="popup1-26">
				<div class="row">
					<span class="col-1">Q6</span>
					<div class="col-11">
						<span>What foreign exchange functions does online banking provide?</span>
					</div>
				</div>
			</a>
			<div id="popup1-26" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>Yes. The Bank's online banking provides functions such as foreign exchange deposit accounting inquiry, foreign exchange comprehensive deposit, foreign exchange comprehensive deposit and withdrawal, foreign exchange transfer and foreign exchange outward remittance.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q7 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-27" aria-expanded="true"
				aria-controls="popup1-27">
				<div class="row">
					<span class="col-1">Q7</span>
					<div class="col-11">
						<span>Does the online bank foreign exchange transfer have a minimum transaction amount and the number of times in addition to the maximum transaction amount limit?</span>
					</div>
				</div>
			</a>
			<div id="popup1-27" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>Yes. The total number of transactions (including general online banking, corporate online banking, mobile banking and telephone banking) on the same day is the total number of transactions for the single-customer foreign exchange deposits and settlement transactions, which are equal to NT$1,000, total is limited to 20 times.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q8 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-28" aria-expanded="true"
				aria-controls="popup1-28">
				<div class="row">
					<span class="col-1">Q8</span>
					<div class="col-11">
						<span>Can I use the "General Internet Banking" to deduct parking fees? how to apply? Need to pay a handling fee?</span>
					</div>
				</div>
			</a>
			<div id="popup1-28" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>Yes, but only for Taiwan Business Bank depositors. Please log in to the "General Internet Banking" account to withhold the parking fee of the person or others. Currently, you can pay the parking fee for the roadside in Taipei City, New Taipei City and Kaohsiung City. The operation process is as follows:</p>
							<p>※If you have not applied for online banking, please apply for “General Internet Banking” first, and designated  transfer function.</p>
							<p>※After the application effective, your parking ticket will be automatically charged by the system, and E-MAIL will notify you of the payment result. You can also log in to “General Internet Banking” for enquiry.</p>
							<p>※ Withholding parking fees are free of charge, and you are welcome to use them more.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q9 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-29" aria-expanded="true"
				aria-controls="popup1-29">
				<div class="row">
					<span class="col-1">Q9</span>
					<div class="col-11">
						<span>When is the application for road side parking fee deductible service deducted? Suppose the billing date is April 2, 2010, when will the bank deduct the payment?</span>
					</div>
				</div>
			</a>
			<div id="popup1-29" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div class="ttb-result-list terms">
								<ol>
									<li data-num="1.">The application will take effect the next day, for example: 
										<ol>
											<li data-num="(1)">On April 2, the customer applied for the road-side parking fee on behalf of the online banking. The customer's billing date will be after April 3 (inclusive), and the Bank will deduct the debited from the contracted account.</li>
										</ol>
									</li>
									<li data-num="2.">According to the different deduction dates contracted by the city parking management offices and the Bank, the deduction of parking fees on the roadsides of Taipei City, New Taipei City and Kaohsiung City is as follows:
										<ol>
											<li data-num="(1)">Taipei City: The 5th day of the billing date is deducted, regardless of the holiday, the April 2th (Friday) billing date as example, the deduction date is April 6th, if the billing date is April 7th, then the deduction date is April 11th (Sunday).</li>
											<li data-num="(2)">New Taipei City: The deduction of the 6th day from the billing date, regardless of the holiday, the April 2th (Friday) billing date, for example, the deduction date is April 7th, if the billing date is On April 7th, the deduction date is April 12 (Monday).</li>											
											<li data-num="(3)">Kaohsiung City: On the 5th day from the billing date, the company will stop the transfer at the end of the day, and the 6th day will be deducted. However, if the transfer date is at the holiday then the transfer will stop, and the next day will be deducted. Take April 2 (Friday) billing as example, the deduction date is April 7th. If the billing date is April 7th, the deduction date is April 13th (Tuesday, due to April 11th is the holiday, it will be postponed until April 12th) (the buckle is received on Monday and debited every other day).</li>											
										</ol>
									</li>											
								</ol>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q10 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-210" aria-expanded="true"
				aria-controls="popup1-210">
				<div class="row">
					<span class="col-1">Q10</span>
					<div class="col-11">
						<span>How to deal with repeated payments? The online banking roadside parking fee withholding service has been cancelled, and the repeated payment before the cancellation date, does the bank automatically refund the fee?</span>
					</div>
				</div>
			</a>
			<div id="popup1-210" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div class="ttb-result-list terms">
								<ol>
									<li data-num="1.">The client has applied for the Bank's online banking roadside parking fee withholding service, and has paid the bill at the convenience store. The situation is as follows:
										<ol>
											<li data-num="(1)">For example, the application for withholding the parking fee of the roadside in Taipei City and New Taipei City: After receiving the refund fee 00 from the parking management office of the Bank, the company will retreat to the customer and the Bank's contracted debit account.</li>
											<li data-num="(2)">If you apply for deduction of Kaohsiung City Roadside Parking Fee: Before June 30, 99 (including), please ask the customer to request a refund from the Kaohsiung City Government Transportation Bureau. From July 1, 99, the Bank will be responsible for the refund. Upon receipt of the refund notice from the parking management office, after 24:00 on the same day, the company will retreat to the customer and the Bank's contracted debit account.</li>
										</ol>
									</li>
									<li data-num="2.">If the parking fee refund information is received before the cancellation date, the system will refund the contracted account on the next day. However, if the parking fee refund information is received after the cancellation date (including the day), please go to the city government parking management of the parking lot to apply for a refund.</li>
								</ol>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q11 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-211" aria-expanded="true"
				aria-controls="popup1-211">
				<div class="row">
					<span class="col-1">Q11</span>
					<div class="col-11">
						<span>Suppose the customer has cancelled the online banking roadside parking fee withholding service on April 2, 2010. What is the date of the bank's deduction to the billing date?</span>
					</div>
				</div>
			</a>
			<div id="popup1-211" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div class="ttb-result-list terms">
								<ol>
									<li data-num="1.">Taipei city was cancelled on April 2, the Bank only processed the parking receipts received  order information received the previous day. The parking order information received the previous day was reduced by three days from the date of receipt. Therefore, only deal with the billing date until March 30th.</li>
									<li data-num="2.">New Taipei City was cancelled on April 2nd, the Bank only processed the parking order information received the previous day. The parking order information received the previous day was reduced by five days from the date of receipt. Therefore, only deal with the billing date until March 28th.</li>
									<li data-num="3.">Kaohsiung City was cancelled on April 2, the Bank only processed the parking order information received the previous day. The parking order information received the previous day was reduced by five days from the date of receipt. Therefore, only deal with the billing date until March 28th.</li>
								</ol>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q12 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-212" aria-expanded="true"
				aria-controls="popup1-212">
				<div class="row">
					<span class="col-1">Q12</span>
					<div class="col-11">
						<span>How do I apply for an online banking electronic bill?</span>
					</div>
				</div>
			</a>
			<div id="popup1-212" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>Online banking provides electronic transaction statements, fund electronic statements and credit card electronic bills. Please log in to the online banking "online service area", "funds" or "credit card" option to apply for electronic bills; such as if the email address is changed, please log in and change it to send the electronic bill.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q13 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-213" aria-expanded="true"
				aria-controls="popup1-213">
				<div class="row">
					<span class="col-1">Q13</span>
					<div class="col-11">
						<span>How do I open an electronic transaction statement?</span>
					</div>
				</div>
			</a>
			<div id="popup1-213" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div class="ttb-result-list terms">
								<ol>
									<li data-num="1.">Before the first time using, please install the browser according to the electronic statement email instructions (the same computer install once).</li>
									<li data-num="2.">Click on the attached file of the email and enter the password: according to your tax ID number / identity card number, if you are 10 yards, please enter the last 8 yards; if less than 10 yards, please enter the full number.</li>
								</ol>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q14 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-214" aria-expanded="true"
				aria-controls="popup1-214">
				<div class="row">
					<span class="col-1">Q14</span>
					<div class="col-11">
						<span>How to use the notification service function?</span>
					</div>
				</div>
			</a>
			<div id="popup1-214" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>Please log in to "My Email" in "Personal Services" - "Email Settings" of Internet Banking. After the first setting is completed, the Email Notification Service function will be enabled, the transfer will be successful, the reservation will be debited, the designated transaction result, the deposit will be notice of expiration,  fund stop loss/take profit point and related services and offers, such multiple notification services. Remind you, in order to inform you, please confirm the correctness of your email address, thank you!</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q15 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-215" aria-expanded="true"
				aria-controls="popup1-215">
				<div class="row">
					<span class="col-1">Q15</span>
					<div class="col-11">
						<span>How to apply for the financial card transaction service function online?</span>
					</div>
				</div>
			</a>
			<div id="popup1-215" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>To apply for a general online banking customer with the "Bank's Chip Financial Card" online, log in to the general online banking "Online Service Area" and click on the "Transfer Function Request/Cancel" to apply.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q16 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-216" aria-expanded="true"
				aria-controls="popup1-216">
				<div class="row">
					<span class="col-1">Q16</span>
					<div class="col-11">
						<span>If someone forgets the online banking password or the online banking password is locked out?</span>
					</div>
				</div>
			</a>
			<div id="popup1-216" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>In order to control the security of online transactions, the online banking transaction password is locked, and you have to go to the counter to handle the password reset. If you are in a foreign country, you can entrust your family to handle it, but you need to contact the foreign unit to apply for authorization and send the certificate to Taiwan for your agent. After receiving the certificate of authorization, asks the agent to hold the certificate, the account opening seal of the authorized person, the identity certificate and the seal and identity document to reset the password during the business hours to the branch.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>