<% response.setHeader("X-Frame-Options", "DENY"); %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %> 
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<!-- 	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous"> -->
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
	<script type="text/javascript">
		var isTimeout = "${not empty sessionScope.timeout}";
		var countdownObjheader = isTimeout == 'true' ? "${sessionScope.timeout}" : 600;
		var countdownSecHeader = isTimeout == 'true' ? "${sessionScope.timeout}" : 600;
        $(document).ready(function () {
            initFootable(); // 將.table變更為footable 
            init();
            putDataInit();
			timeLogout();
        });

        function init() {
			// $("#formId").validationEngine({binded: false,promptPosition: "inline"});
			$("#formId").validationEngine({
			validationEventTriggers:'keyup blur', 
			binded:true,
			promptPosition: "inline",
			addFailureCssClassToField:"isValid",
			scroll: true
			});
	    	$("#CMSUBMIT").click(function(e){
				<c:if test="${ input_data.data.checknb.equals('N') }">
		    		if(!username_check() || !username2_check() || !transpin_check() || !transpin2_check() || !loginpin_check() || !loginpin2_check()){
		    			//alert("請確認 網路銀行帳號密碼設定 資料是否正確");
		    			errorBlock(
							null, 
							null,
							["請確認 網路銀行帳號密碼設定 資料是否正確"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
		    			return false;
		    		}
					var LOGINPIN = $('#LOGINPIN').val();
					var TRANSPIN = $('#TRANSPIN').val();
		    		$('#HLOGINPIN').val(pin_encrypt(LOGINPIN));
		    		$('#HTRANSPIN').val(pin_encrypt(TRANSPIN));
				</c:if>
	    		if($("#CRDTYP13").prop("checked")){
					$("#CARDAPPLY").val('N');
				}else{
					$("#CARDAPPLY").val('Y');
				}
    			if($('input[name="CChargeApply"]:checked').val() == 'Y' && $('#CARDAPPLY').val() == 'Y'){
    				$("#validate_LMT").val($('#LMT').val());
    				var lmt = $('#LMT').val();
    			}
	    		if($("#LMTTYN1").prop("checked")){
					$("#LMTTYN").val('Y');
				}else{
					$("#LMTTYN").val('N');
				}
				e = e || window.event;
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {  
	        		// if($('#LMT').val().length==1)
	        		// {
	        		// 	$('#LMT').val('0' + $('#LMT').val());
	        		// }
					$("#formId").validationEngine('detach');
		        	var action = '${__ctx}/DIGITAL/ACCOUNT/apply_digital_account_p4';
		        	$("#formId").attr("action", action);
		        	$("#formId").submit();
				}
			});
			$("#CMBACK").click(function(e){
				$("#formId").validationEngine('detach');
	        	var action = '${__ctx}/DIGITAL/ACCOUNT/apply_digital_account_p3_2';
				$("#back").val('Y');
				$("#formId").attr("action", action);
				$("#formId").submit();
			});
        }

		function ValidateValue(textbox) {
			var IllegalString = "+-_＿︿%@[`~!#$^&*()=|{}':;',\\[\\].<>/?~！#￥……&*（）——|{}【】‘；：”“'。，、？]‘'";
			var textboxvalue = textbox.value;
			var index = textboxvalue.length - 1;
			var s = textbox.value.charAt(index);
			if (IllegalString.indexOf(s) >= 0) {
				s = textboxvalue.substring(0, index);
				textbox.value = s;
			}
		}
		function chooseOne2(flag){

			console.log(flag);
			switch(flag){
				// 感應式金融卡
			
				case 'inductiveCard':
				    $(".validate_card").css('display', '');
					$(".noCard-block").show();
					$('#LMT_Block').show();
					$('#LMT').val('');
					$('#CRDTYP12-mark').hide();
				    $("#NAATAPPLY1").prop('checked', true);
				    $("#NAATAPPLY2").prop('checked', false);
				    $("#CROSSAPPLY1").prop('checked', true);
				    $("#CROSSAPPLY2").prop('checked', false);
					if($("input[name=CChargeApply]").prop('checked')){
						$('#form-validation-field-0').hide();
					}
					if($("input[name=NAATAPPLY]").prop('checked')){
						$('#form-validation-field-2').hide();
					}
					if($("input[name=SNDFLG]").prop('checked')){
						$('#form-validation-field-4').hide();
					}
					if($("input[name=EBILLFLAG]").prop('checked')){
						$('#form-validation-field-5').hide();
					}
					break;
				//晶片金融卡
				case 'chipCard':
				    $(".validate_card").css('display', '');
					$(".noCard-block").show();
					$('#LMT_Block').show();
					$('#LMT').val('');
					$('#CRDTYP12-mark').show();
					$("#NAATAPPLY1").prop('checked', true);
				    $("#NAATAPPLY2").prop('checked', false);
				    $("#CROSSAPPLY1").prop('checked', true);
				    $("#CROSSAPPLY2").prop('checked', false);
					if($("input[name=CChargeApply]").prop('checked')){
						$('#form-validation-field-0').hide();
					}
					if($("input[name=NAATAPPLY]").prop('checked')){
						$('#form-validation-field-2').hide();
					}
					if($("input[name=SNDFLG]").prop('checked')){
						$('#form-validation-field-4').hide();
					}
					if($("input[name=EBILLFLAG]").prop('checked')){
						$('#form-validation-field-5').hide();
					}
					break;
					// 否
				case 'noCard':
					$("#CRDTYP11").prop('checked', false);
					$("#CRDTYP12").prop('checked', false);
					$("#SNDFLG11").prop('checked', false);
					$("#SNDFLG12").prop('checked', false);$("#SNDFLG13").prop('checked', false);
					$("#CChargeApply1").prop('checked', false);
					$("#CChargeApply2").prop('checked', false);
					$("#NAATAPPLY1").prop('checked', false);
					$("#NAATAPPLY2").prop('checked', false);
					$("#CROSSAPPLY1").prop('checked', false);
					$("#CROSSAPPLY2").prop('checked', false);
					$(".validate_card").css('display', 'block');
					$('#LMT_Block').hide();
					$('#LMT').val("");
					$('#CRDTYP12-mark').hide();
					$(".validate_card").css('display', 'none');
					$(".noCard-block").hide();
					break;
			}
		}	
	
		//初始化資料自填
		function putDataInit(){
			if('${input_data.data.CARDAPPLY}' == 'N'){
				$("#CRDTYP13").prop('checked',true);
				chooseOne2('noCard');
			}else if('${input_data.data.CARDAPPLY}' == 'Y'){
				
				if('${input_data.data.CRDTYP}' == '1'){
					$("#CRDTYP12").prop('checked',true);
					chooseOne2('chipCard');
				}else if('${input_data.data.CRDTYP}' == '2'){
					$("#CRDTYP11").prop('checked',true);
					chooseOne2('inductiveCard');
				}
				if('${input_data.data.CChargeApply}' == 'N'){
					$("#CChargeApply2").prop('checked',true);
					$("#LMT_Block").hide();
					$('#LMT').val("");
				}else if('${input_data.data.CChargeApply}' == 'Y'){
					$("#CChargeApply1").prop('checked',true);
					$("#LMT_Block").show();
				}
				$("#LMT").children().each(function(){
	    		    if ($(this).val()=='${input_data.data.LMT}'){
	    		        $(this).prop("selected", true);
	    		    }
	    		});
				if('${input_data.data.NAATAPPLY}' == 'N'){
					$("#NAATAPPLY2").prop('checked',true);
				}else if('${input_data.data.NAATAPPLY}' == 'Y'){
					$("#NAATAPPLY1").prop('checked',true);
				}
				if('${input_data.data.CROSSAPPLY}' == 'N'){
					$("#CROSSAPPLY2").prop('checked',true);
				}else if('${input_data.data.CROSSAPPLY}' == 'Y'){
					$("#CROSSAPPLY1").prop('checked',true);
				}
				if('${input_data.data.SNDFLG}' == '1'){
					$("#SNDFLG11").prop('checked',true);
				}else if('${input_data.data.SNDFLG}' == '2'){
					$("#SNDFLG12").prop('checked',true);
				}
			}
			if('${input_data.data.EBILLFLAG}' == 'N'){
				$("#EBILLFLAG2").prop('checked',true);
			}else if('${input_data.data.EBILLFLAG}' == 'Y'){
				$("#EBILLFLAG1").prop('checked',true);
			}
		}
		
		/***********************************/
		/*****			動態檢驗		   *****/
		/***********************************/
		function checkUserName(userID){
			var username=$('#'+userID).val();
			var min = 6;
			var max = 16;
			if(!checkLength(username,min,max)){
				return false;
			}
			if(!check2EnChar(username)){;
				return false;
			}
			if(checkIllegalChar(username)){
				return false;
			}
			if(checkDuplicateChar(username)){
				return false;
			}
			if(checkContinuousENChar(username)){
				return false;
			}
			return true;
		}
		function NewCheckpwd(oField, bCanEmpty)
		{
			try
			{
			    var sNumber = $('#'+oField).val();
			    if (bCanEmpty == false && sNumber == "")
			    {
			        return false;
			    }
			    var reNumber = new RegExp(/^[A-Za-z0-9]+$/);
			    if (!reNumber.test(sNumber))
			    {
			        return false;
			    }
			}
			catch (exception)
			{
				return false;
			}
			return true;
		}
		function CheckNotEmpty(oField)
		{
			try
			{
			    if ($('#'+oField).val() == "")
			        return false;
			}
			catch (exception)
			{
				return false;
			}
			return true;
		}
		//不能為連號數字
		function chkSerialNum(obj) {
			var str = $('#'+obj).val();
			if(str != '') {
				var iCode1 = str.charCodeAt(0); 
				if(iCode1 >=48 && iCode1 <= 57) {
					var iCode2 = 0;
					var bPlus = true;
					var bMinus = true;
					for(var i=1;i<str.length;i++) {
						iCode2 = str.charCodeAt(i);//now
						if(iCode2 != (iCode1+1)) {
							if(!(iCode2 == 48 && iCode1 == 57)) {
								bPlus = false;
							}
						}
						if(iCode2 != (iCode1-1)) {
							if(!(iCode2 == 57 && iCode1 == 48)) {
								bMinus = false;	
							}
						}
						iCode1 = iCode2;//before
					}
					if(bPlus || bMinus) 
						return false;
				}
			}
			return true;	
		}

		//檢核輸入值是否為相同之文數字
		function chkSameEngOrNum(obj) {
			var str = $('#'+obj).val();
			if(str != '') {
				var iCode1 = str.charCodeAt(0);
				
				if((iCode1 >=48 && iCode1 <= 57) || (iCode1 >= 65 && iCode1 <= 90) || (iCode1 >= 97 && iCode1 <= 122)) {
					var iCode2 = 0;
					var bSame  = true;
					for(var i=0;i<str.length;i++) {
						iCode2 = str.charCodeAt(i);
						if(iCode1 != iCode2) {
							bSame = false;
						}
					}
				}
				if(bSame) {
					return false;
				}
			}
			return true;
		}
		
		
		function username_check(){
			var ckeckflag = true;
			if($("#USERNAME").val() != $("#LOGINPIN").val() && $("#USERNAME").val() != $("#TRANSPIN").val() && $("#LOGINPIN").val() != $("#TRANSPIN").val()){
				$("#USERNAME_S1").removeClass("input-error-remarks");
				$("#USERNAME_S1").addClass("input-correct-remarks");
				$("#USERNAME_I1").removeClass("fa-times-circle");
				$("#USERNAME_I1").addClass("fa-check-circle");
			}else{
				$("#USERNAME_S1").addClass("input-error-remarks");
				$("#USERNAME_S1").removeClass("input-correct-remarks");
				$("#USERNAME_I1").addClass("fa-times-circle");
				$("#USERNAME_I1").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			if(checkUserName("USERNAME") && $("#USERNAME").val().length >= 6 && $("#USERNAME").val().length <= 16){
				$("#USERNAME_S2").removeClass("input-error-remarks");
				$("#USERNAME_S2").addClass("input-correct-remarks");
				$("#USERNAME_I2").removeClass("fa-times-circle");
				$("#USERNAME_I2").addClass("fa-check-circle");
			}else{
				$("#USERNAME_S2").addClass("input-error-remarks");
				$("#USERNAME_S2").removeClass("input-correct-remarks");
				$("#USERNAME_I2").addClass("fa-times-circle");
				$("#USERNAME_I2").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			if($("#USERNAME").val().indexOf("${ input_data.data.BIRTHDAY }") == -1){
				$("#USERNAME_S3").removeClass("input-error-remarks");
				$("#USERNAME_S3").addClass("input-correct-remarks");
				$("#USERNAME_I3").removeClass("fa-times-circle");
				$("#USERNAME_I3").addClass("fa-check-circle");
			}else{
				$("#USERNAME_S3").addClass("input-error-remarks");
				$("#USERNAME_S3").removeClass("input-correct-remarks");
				$("#USERNAME_I3").addClass("fa-times-circle");
				$("#USERNAME_I3").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			if(chkSerialNum("USERNAME") && chkSameEngOrNum("USERNAME")){
				$("#USERNAME_S4").removeClass("input-error-remarks");
				$("#USERNAME_S4").addClass("input-correct-remarks");
				$("#USERNAME_I4").removeClass("fa-times-circle");
				$("#USERNAME_I4").addClass("fa-check-circle");
			}else{
				$("#USERNAME_S4").addClass("input-error-remarks");
				$("#USERNAME_S4").removeClass("input-correct-remarks");
				$("#USERNAME_I4").addClass("fa-times-circle");
				$("#USERNAME_I4").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			return ckeckflag;
		}
		function username2_check(){
			var ckeckflag = true;
			if($("#USERNAME").val() == $("#USERNAME2").val()){
				$("#USERNAME2_S1").removeClass("input-error-remarks");
				$("#USERNAME2_S1").addClass("input-correct-remarks");
				$("#USERNAME2_I1").removeClass("fa-times-circle");
				$("#USERNAME2_I1").addClass("fa-check-circle");
			}else{
				$("#USERNAME2_S1").addClass("input-error-remarks");
				$("#USERNAME2_S1").removeClass("input-correct-remarks");
				$("#USERNAME2_I1").addClass("fa-times-circle");
				$("#USERNAME2_I1").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			return ckeckflag;
		}
		
		function loginpin_check(){
			var ckeckflag = true;
			if($("#USERNAME").val() != $("#LOGINPIN").val() && $("#USERNAME").val() != $("#TRANSPIN").val() && $("#LOGINPIN").val() != $("#TRANSPIN").val()){
				$("#LOGINPIN_S1").removeClass("input-error-remarks");
				$("#LOGINPIN_S1").addClass("input-correct-remarks");
				$("#LOGINPIN_I1").removeClass("fa-times-circle");
				$("#LOGINPIN_I1").addClass("fa-check-circle");
			}else{
				$("#LOGINPIN_S1").addClass("input-error-remarks");
				$("#LOGINPIN_S1").removeClass("input-correct-remarks");
				$("#LOGINPIN_I1").addClass("fa-times-circle");
				$("#LOGINPIN_I1").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			if(NewCheckpwd("LOGINPIN") && $("#LOGINPIN").val().length >= 6 && $("#LOGINPIN").val().length <= 8){
				$("#LOGINPIN_S2").removeClass("input-error-remarks");
				$("#LOGINPIN_S2").addClass("input-correct-remarks");
				$("#LOGINPIN_I2").removeClass("fa-times-circle");
				$("#LOGINPIN_I2").addClass("fa-check-circle");
			}else{
				$("#LOGINPIN_S2").addClass("input-error-remarks");
				$("#LOGINPIN_S2").removeClass("input-correct-remarks");
				$("#LOGINPIN_I2").addClass("fa-times-circle");
				$("#LOGINPIN_I2").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			if($("#LOGINPIN").val().indexOf("${ input_data.data.BIRTHDAY }") == -1){
				$("#LOGINPIN_S3").removeClass("input-error-remarks");
				$("#LOGINPIN_S3").addClass("input-correct-remarks");
				$("#LOGINPIN_I3").removeClass("fa-times-circle");
				$("#LOGINPIN_I3").addClass("fa-check-circle");
			}else{
				$("#LOGINPIN_S3").addClass("input-error-remarks");
				$("#LOGINPIN_S3").removeClass("input-correct-remarks");
				$("#LOGINPIN_I3").addClass("fa-times-circle");
				$("#LOGINPIN_I3").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			if(chkSerialNum("LOGINPIN") && chkSameEngOrNum("LOGINPIN")){
				$("#LOGINPIN_S4").removeClass("input-error-remarks");
				$("#LOGINPIN_S4").addClass("input-correct-remarks");
				$("#LOGINPIN_I4").removeClass("fa-times-circle");
				$("#LOGINPIN_I4").addClass("fa-check-circle");
			}else{
				$("#LOGINPIN_S4").addClass("input-error-remarks");
				$("#LOGINPIN_S4").removeClass("input-correct-remarks");
				$("#LOGINPIN_I4").addClass("fa-times-circle");
				$("#LOGINPIN_I4").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			return ckeckflag;
		}
		function loginpin2_check(){
			var ckeckflag = true;
			if($("#LOGINPIN").val() == $("#LOGINPIN2").val()){
				$("#LOGINPIN2_S1").removeClass("input-error-remarks");
				$("#LOGINPIN2_S1").addClass("input-correct-remarks");
				$("#LOGINPIN2_I1").removeClass("fa-times-circle");
				$("#LOGINPIN2_I1").addClass("fa-check-circle");
			}else{
				$("#LOGINPIN2_S1").addClass("input-error-remarks");
				$("#LOGINPIN2_S1").removeClass("input-correct-remarks");
				$("#LOGINPIN2_I1").addClass("fa-times-circle");
				$("#LOGINPIN2_I1").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			return ckeckflag;
		}
		
		function transpin_check(){
			var ckeckflag = true;
			if($("#USERNAME").val() != $("#LOGINPIN").val() && $("#USERNAME").val() != $("#TRANSPIN").val() && $("#LOGINPIN").val() != $("#TRANSPIN").val()){
				$("#TRANSPIN_S1").removeClass("input-error-remarks");
				$("#TRANSPIN_S1").addClass("input-correct-remarks");
				$("#TRANSPIN_I1").removeClass("fa-times-circle");
				$("#TRANSPIN_I1").addClass("fa-check-circle");
			}else{
				$("#TRANSPIN_S1").addClass("input-error-remarks");
				$("#TRANSPIN_S1").removeClass("input-correct-remarks");
				$("#TRANSPIN_I1").addClass("fa-times-circle");
				$("#TRANSPIN_I1").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			if(NewCheckpwd("TRANSPIN") && $("#TRANSPIN").val().length >= 6 && $("#TRANSPIN").val().length <= 8){
				$("#TRANSPIN_S2").removeClass("input-error-remarks");
				$("#TRANSPIN_S2").addClass("input-correct-remarks");
				$("#TRANSPIN_I2").removeClass("fa-times-circle");
				$("#TRANSPIN_I2").addClass("fa-check-circle");
			}else{
				$("#TRANSPIN_S2").addClass("input-error-remarks");
				$("#TRANSPIN_S2").removeClass("input-correct-remarks");
				$("#TRANSPIN_I2").addClass("fa-times-circle");
				$("#TRANSPIN_I2").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			if($("#TRANSPIN").val().indexOf("${ input_data.data.BIRTHDAY }") == -1){
				$("#TRANSPIN_S3").removeClass("input-error-remarks");
				$("#TRANSPIN_S3").addClass("input-correct-remarks");
				$("#TRANSPIN_I3").removeClass("fa-times-circle");
				$("#TRANSPIN_I3").addClass("fa-check-circle");
			}else{
				$("#TRANSPIN_S3").addClass("input-error-remarks");
				$("#TRANSPIN_S3").removeClass("input-correct-remarks");
				$("#TRANSPIN_I3").addClass("fa-times-circle");
				$("#TRANSPIN_I3").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			if(chkSerialNum("TRANSPIN") && chkSameEngOrNum("TRANSPIN")){
				$("#TRANSPIN_S4").removeClass("input-error-remarks");
				$("#TRANSPIN_S4").addClass("input-correct-remarks");
				$("#TRANSPIN_I4").removeClass("fa-times-circle");
				$("#TRANSPIN_I4").addClass("fa-check-circle");
			}else{
				$("#TRANSPIN_S4").addClass("input-error-remarks");
				$("#TRANSPIN_S4").removeClass("input-correct-remarks");
				$("#TRANSPIN_I4").addClass("fa-times-circle");
				$("#TRANSPIN_I4").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			return ckeckflag;
		}
		function transpin2_check(){
			var ckeckflag = true;
			if($("#TRANSPIN").val() == $("#TRANSPIN2").val()){
				$("#TRANSPIN2_S1").removeClass("input-error-remarks");
				$("#TRANSPIN2_S1").addClass("input-correct-remarks");
				$("#TRANSPIN2_I1").removeClass("fa-times-circle");
				$("#TRANSPIN2_I1").addClass("fa-check-circle");
			}else{
				$("#TRANSPIN2_S1").addClass("input-error-remarks");
				$("#TRANSPIN2_S1").removeClass("input-correct-remarks");
				$("#TRANSPIN2_I1").addClass("fa-times-circle");
				$("#TRANSPIN2_I1").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			return ckeckflag;
		}
		function all_check(){
			username_check();
			username2_check();
			transpin_check();
			transpin2_check();
			loginpin_check();
			loginpin2_check();
		}
		function errorMsg_hide(type){
			console.log(type);  
			switch(type){
				case 'CChargeApply1': //申請消費扣款(Smart Pay)
				$('#form-validation-field-0').hide();
				$('#LMT_Block').show();
				break;
				case 'CChargeApply2': //申請消費扣款(Smart Pay)
				$('#form-validation-field-0').hide();
				$('#LMT_Block').hide();
				$('#LMT').val('');
				break;
				case 'NAATAPPLY': //申請非約定帳號轉帳
				$('#form-validation-field-2').hide();
				break;
				case 'CROSSAPPLY': //申請跨國交易
				$('#form-validation-field-3').hide();
				break;
				case 'SNDFLG': //領取方式
				$('#form-validation-field-4').hide();
				break;
				case 'EBILLFLAG': //申請電子帳單
				$('#form-validation-field-5').hide();
				break;
		}
	}

		function timeLogout(){
			// 刷新session
			var uri = '${__ctx}/login_refresh';
			console.log('refresh.uri: '+uri);
			var result = fstop.getServerDataEx( uri, null, false, null);
			console.log('refresh.result: '+JSON.stringify(result));
			// 初始化登出時間
			$("#countdownheader").html(parseInt(countdownSecHeader)+1);
			$("#countdownMin").html("");
			$("#mobile-countdownMin").html("");
			$("#countdownSec").html("");
			$("#mobile-countdownSec").html("");
			// 倒數
			startIntervalHeader(1, refreshCountdownHeader, []);
		}

		function refreshCountdownHeader(){
			// timeout剩餘時間
			var nextSec = parseInt($("#countdownheader").html()) - 1;
			$("#countdownheader").html(nextSec);

			// 提示訊息--即將登出，是否繼續使用
			if(nextSec == 120){
				initLogoutBlockUI();
			}
			// timeout
			if(nextSec == 0){
				// logout
				fstop.logout( '${__ctx}'+'/logout_aj' , '${__ctx}'+'/timeout_logout');
			}
			if (nextSec >= 0){
				// 倒數時間以分秒顯示
				var minutes = Math.floor(nextSec / 60);
				$("#countdownMin").html(('0' + minutes).slice(-2));
				$("#mobile-countdownMin").html(('0' + minutes).slice(-2));

				var seconds = nextSec - minutes * 60;
				$("#countdownSec").html(('0' + seconds).slice(-2));
				$("#mobile-countdownSec").html(('0' + seconds).slice(-2));
			}
		}
		function startIntervalHeader(interval, func, values){
			clearInterval(countdownObjheader);
			countdownObjheader = setRepeater(func, values, interval);
		}
		function setRepeater(func, values, interval){
			return setInterval(function(){
				func.apply(this, values);
			}, interval * 1000);
		}
		/**
		 * 初始化logoutBlockUI
		 */
		function initLogoutBlockUI() {
			logoutblockUI();
		}

		/**
		 * 畫面BLOCK
		 */
		function logoutblockUI(timeout){
			$("#logout-block").show();

			// 遮罩後不給捲動
			document.body.style.overflow = "hidden";

			var defaultTimeout = 60000;
			if(timeout){
				defaultTimeout = timeout;
			}
		}

		/**
		 * 畫面UNBLOCK
		 */
		function unLogoutBlockUI(timeoutID){
			if(timeoutID){
				clearTimeout(timeoutID);
			}
			$("#logout-block").hide();

			// 解遮罩後給捲動
			document.body.style.overflow = 'auto';
		}
		/**
		 *繼續使用
		 */
		function keepLogin(){
			unLogoutBlockUI(); // 解遮罩
			timeLogout(); // 刷新倒數計時
		}
    </script>
	<style>
		@media screen and (max-width:767px) {
			.ttb-button {
					width: 38%;
					left: 36px;
			}
			#CMBACK.ttb-button{
					border-color: transparent;
					box-shadow: none;
					color: #333;
					background-color: #fff;
			}
 

		}
	</style>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上開立數位存款帳戶     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1361" /></li>
		</ol>
	</nav>

	<div class="content row">
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
<!-- 					線上開立數位存款帳戶 -->
					<spring:message code="LB.D1361"/>
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <div id="step-bar">
                    <ul>
                        <li class="finished">注意事項與權益</li>
                        <li class="finished">開戶認證</li>
                        <li class="active">開戶資料</li>
                        <li class="">確認資料</li>
                        <li class="">完成申請</li>
                    </ul>
                </div>
                <form id="formId" method="post">
					<input type="hidden" name="ADOPID" value="N203">
				  	<input type="hidden" name="HLOGINPIN" id="HLOGINPIN" value="">
				  	<input type="hidden" name="HTRANSPIN" id="HTRANSPIN" value="">
				  	<input type="hidden" name="CARDAPPLY" id="CARDAPPLY" value="">
					<input type="hidden" name="LMTTYN" id="LMTTYN" value="">	
					<input type="hidden" name="back" id="back" value=>
					<!-- timeout -->
					<section id="id-and-fast">
						<span class="id-name">
							<spring:message code="LB.Welcome_1"/>&nbsp;
							<c:if test="${empty input_data.data.CUSNAME}">
								<span id="username" name="username_show"><spring:message code="LB.X2190"/></span>
							</c:if>
							<c:if test="${not empty input_data.data.CUSNAME}">
								<span id="username" name="username_show">${input_data.data.CUSNAME}</span>
							</c:if>
							&nbsp;<spring:message code="LB.Welcome_2"/>
						</span>
						<div id="id-block">
							<div>
							<span class="id-time">
								<fmt:setLocale value="${pageContext.response.locale}"/>
								<c:if test="${!empty sessionScope.logindt && !empty sessionScope.logintm}">
									<fmt:parseDate var="parseDate"
												   value="${sessionScope.logindt} ${sessionScope.logintm}"
												   pattern="yyyy-MM-dd HH:mm:ss"/>
									<fmt:formatDate value="${parseDate}" dateStyle="full"
													type="both"/>&nbsp;<spring:message code="LB.X2250"/>
									<br/>
								</c:if>

								<fmt:formatDate value="${sessionScope.login_time}" type="both" dateStyle="full"/>
								&nbsp;<spring:message code="LB.Login_successful"/>
							</span>
								<!-- 自動登出剩餘時間 -->
								<span class="id-time"><spring:message code="LB.X1912"/>
								<span class="high-light ml-1">
									<!-- 分 -->
									<font id="countdownMin"></font>
									:
									<!-- 秒 -->
									<font id="countdownSec"></font>
								</span>
							</span>
							</div>
							<button type="button" class="btn-flat-orange" onclick="timeLogout()"><spring:message
									code="LB.X1913"/></button>
							<button type="button" class="btn-flat-darkgray"
									onClick="fstop.logout( '${__ctx}'+'/logout_aj' , '${__ctx}'+'/login')"><spring:message
									code="LB.Logout"/></button>
						</div>
					</section>
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
                        
							<div class="ttb-input-block">
	                            <div class="ttb-message">
	                                <p>開戶資料 (3/4)</p>
	                            </div>
	                            <div class="classification-block">
									<!-- 晶片金融卡 -->
	                                <p><spring:message code="LB.Financial_debit_card"/></p>
	                            </div>
								
<!-- 								<div class="ttb-input-item row"> -->
<!-- 									<span class="input-title">  -->
<!-- 										<label> -->
<!-- 											<h4> -->
<!-- 												申請晶片金融卡 -->
<%-- 												 <spring:message code="LB.D1431_3"/> --%>
<!-- 											</h4> -->
<!-- 										</label> -->
<!-- 									</span>  -->
<!-- 									<span class="input-block"> -->
<!-- 										<div class="ttb-input"> -->
<!-- 											<label class="radio-block"> -->
<!--  												是 -->
<%-- 												<spring:message code="LB.D0034_2"/> --%>
<!-- 												<input type="radio" name="CARDAPPLY" id="CARDAPPLY1" value="Y" onClick="chooseOne2(false)" /> -->
<!-- 												<span class="ttb-radio"></span> -->
<!-- 											</label>&nbsp; -->
<!-- 											<label class="radio-block"> -->
<!-- 												否 -->
<%-- 												<spring:message code="LB.D0034_3"/> --%>
<!-- 												<input type="radio" name="CARDAPPLY" id="CARDAPPLY2" value="N" onClick="chooseOne2(true)" /> -->
<!-- 												<span class="ttb-radio"></span> -->
<!-- 											</label> -->
<!-- 										</div> -->
<!-- 									</span> -->
<!-- 								</div> -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 												卡片種類 -->
											<spring:message code="LB.Card_kind"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
<!-- 											<label class="radio-block"> -->
<!-- 												感應式金融卡 -->
<%-- 												<spring:message code="LB.D1432"/><br> --%>
<%-- 												<img src="${__ctx}/img/CHIPCARD_TOUCH.jpg" alt="<spring:message code= "LB.D1432" />" style="vertical-align: top;"/> --%>
<!-- 												<input type="radio" name="CRDTYP" id="CRDTYP11" value="2"  onclick="chooseOne2('inductiveCard')"/> -->
<!-- 												<span class="ttb-radio"></span> -->
<!-- 											</label>&nbsp; -->
											<label class="radio-block">
<!-- 												晶片金融卡 -->
												<spring:message code="LB.Financial_debit_card"/><br>
<%-- 												<img src="${__ctx}/img/CHIPCARD.jpg" alt="<spring:message code= "LB.Financial_debit_card" />"  style="vertical-align: top;"/> --%>
												<input type="radio" name="CRDTYP" id="CRDTYP12" value="1"  onclick="chooseOne2('chipCard')"/>
												<span class="ttb-radio"></span>
											</label>&nbsp;
											<label class="radio-block">
<!-- 												否 -->
												<spring:message code="LB.D0034_3"/><br>
<%-- 												<img src="${__ctx}/img/CHIPCARD.jpg" alt="<spring:message code= "LB.Financial_debit_card" />"  style="vertical-align: top;"/> --%>
												<input type="radio" name="CRDTYP" id="CRDTYP13" value="" onclick="chooseOne2('noCard')"/>
												<span class="ttb-radio"></span>
											</label>
											<span class="input-remarks" id="CRDTYP12-mark">您申請的晶片金融卡使用範圍包含：餘額查詢、存/提款、非約定轉帳、繳費稅、消費扣款及跨國交易等功能。</span>
										</div>
									</span>
								</div>
								<div class="ttb-input-item  row noCard-block">
									<span class="input-title"> 
									<label>
										<h4>
											<!-- 申請消費扣款(Smart Pay) -->
											<spring:message code="LB.X0214"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
<!-- 												是 -->
												<spring:message code="LB.D0034_2"/>
												<input class="card-input" type="radio" name="CChargeApply" id="CChargeApply1" value="Y" onclick="errorMsg_hide('CChargeApply1')"/>
												<span class="ttb-radio"></span>
											</label>&nbsp;
											<label class="radio-block">
<!-- 												否 -->
												<spring:message code="LB.D0034_3"/>
												<input class="card-input" type="radio" name="CChargeApply" id="CChargeApply2" value="N" onclick="errorMsg_hide('CChargeApply2')"/>
												<span class="ttb-radio"></span>
											</label>
											<span class="hideblock validate_card">
												<input id="validate_CChargeApply" name="CChargeApply" type="radio"  
													class="card-input validate[funcCall[validate_Radio['<spring:message code= "LB.X1587" />',CChargeApply]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
											</span>
										</div>
									</span>
								</div>
								<div class="ttb-input-item row" id="LMT_Block">
									<span class="input-title"> 
									<label>
										<h4>每日交易限額 <span class="high-light">*</span></h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<select name="LMT" id="LMT" class="custom-select validate[required]" value="${input_data.data.LMT }">
												<option value="">請選擇每日交易限額</option>
												<option value="01">1萬元</option>
												<option value="02">2萬元</option>
												<option value="03">3萬元</option>
												<option value="04">4萬元</option>
												<option value="05">5萬元</option>
												<option value="06">6萬元</option>
												<option value="07">7萬元</option>
												<option value="08">8萬元</option>	
												<option value="09">9萬元</option>	
												<option value="10">10萬元</option>
											</select>
											<span class="input-remarks">(<spring:message code="LB.D1436"/>)</span>
										</div>
									</span>
								</div>
								<div class="ttb-input-item row noCard-block">
									<span class="input-title"> 
									<label>
<!-- 												申請非約定帳號轉帳(每日限額三萬元整) -->
										<h4><spring:message code="LB.D1437"/></h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
<!-- 												是 -->
												<spring:message code="LB.D0034_2"/>
												<input class="card-input" type="radio" name="NAATAPPLY" id="NAATAPPLY1" value="Y"  onclick="errorMsg_hide('NAATAPPLY')" />
												<span class="ttb-radio"></span>
											</label>&nbsp;
											<label class="radio-block">
<!-- 												否 -->
												<spring:message code="LB.D0034_3"/>
												<input class="card-input" type="radio" name="NAATAPPLY" id="NAATAPPLY2" value="N"  onclick="errorMsg_hide('NAATAPPLY')"/>
												<span class="ttb-radio"></span>
											</label> 
											<span class="hideblock validate_card">
												<input id="validate_NAATAPPLY" name="NAATAPPLY" type="radio"  
													class="card-input validate[funcCall[validate_Radio['<spring:message code= "LB.D1567" />',NAATAPPLY]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
											</span>
										</div>
									</span>
								</div>
								<div class="ttb-input-item row noCard-block">
									<span class="input-title"> 
									<label>
<!-- 												申請跨國交易 -->
										<h4> <spring:message code="LB.D1438"/></h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
<!-- 												是 -->
												<spring:message code="LB.D0034_2"/>
												<input class="card-input" type="radio" name="CROSSAPPLY" id="CROSSAPPLY1" value="Y"  onclick="errorMsg_hide('CROSSAPPLY')"/>
												<span class="ttb-radio"></span>
											</label>&nbsp;
											<label class="radio-block">
<!-- 												否 -->
												<spring:message code="LB.D0034_3"/>
												<input class="card-input" type="radio" name="CROSSAPPLY" id="CROSSAPPLY2" value="N" onclick="errorMsg_hide('CROSSAPPLY')"/>
												<span class="ttb-radio"></span>
											</label>
											<span class="hideblock validate_card">
												<input id="validate_CROSSAPPLY" name="CROSSAPPLY" type="radio"  
													class="card-input validate[funcCall[validate_Radio['<spring:message code= "LB.X1588" />',CROSSAPPLY]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
											</span>
										</div>
									</span>
								</div>
								<div class="ttb-input-item row noCard-block">
									<span class="input-title"> 
										<label>
											<h4><spring:message code="LB.D0503"/></h4>
										</label>
									</span> 
									<span class="input-block">
										<!-- 郵寄至通訊地址 -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.B0007"/>
												<input class="card-input" type="radio" name="SNDFLG" id="SNDFLG11" value="1"  onclick="errorMsg_hide('SNDFLG')" />
												<span class="ttb-radio"></span>
											</label>
											<div class="text-box" style="font-size: 0.875rem; color: #6c757d; text-align: left;">
												${input_data.data.POSTCOD2} ${input_data.data.CTTADR}
											</div>
										</div>
										<!-- 郵寄至戶籍地址 -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.B0006"/>
												<input class="card-input" type="radio" name="SNDFLG" id="SNDFLG13" value="3"   onclick="errorMsg_hide('SNDFLG')"/>
												<span class="ttb-radio"></span>
											</label>
											<div class="text-box" style="font-size: 0.875rem; color: #6c757d; text-align: left;">
												${input_data.data.POSTCOD1} ${input_data.data.PMTADR}
											</div>
										</div>
										<!-- 至指定服務分行親領 -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.B0008"/>
												<input class="card-input" type="radio" name="SNDFLG" id="SNDFLG12" value="2"  onclick="errorMsg_hide('SNDFLG')"/>
												<span class="ttb-radio"></span>
											</label>
											<div class="text-box" style="font-size: 0.875rem; color: #6c757d; text-align: left;">
												${input_data.data.BRHCOD} ${input_data.data.BRHNAME} <br> ${input_data.data.BRHADDRESS}
											</div>
										</div>
										<span class="hideblock validate_card">
											<input id="validate_SNDFLG" name="SNDFLG" type="radio"  
													class="card-input validate[funcCall[validate_Radio['<spring:message code= "LB.X1586" />',SNDFLG]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
										</span>
									</span>
								</div>
	                            <div class="classification-block">
                                 	<p>網路銀行服務申請</p>
                                 	<ol class="description-list list-decimal" style="color:red">
                                 	<span>提醒您:</span>
          								<li style="color:red">本次開戶設定之網路銀行簽入密碼及交易密碼為「預設啟用」密碼，請先妥善保管。</li>
          								<li style="color:red">數位存款帳戶開立後，為保障您帳戶安全，請於30天內盡速登入網路銀行或行動銀行APP進行密碼變更。</li>
          								<li style="color:red">首次登入網路銀行或行動銀行APP需先輸入預設啟用密碼，系統會要求重新設定簽入密碼及交易密碼，爾後登入則請使用新設定之簽入密碼及交易密碼。</li>
         							</ol>
                                    <input type="hidden" name="TRFLAG" id="TRFLAG" value="Y">
                             	</div>
	                            <!-- 網路銀行帳號密碼設定 -->
								<c:if test="${ input_data.data.checknb.equals('Y') }">
		                            <div class="ttb-input-item row">
		                                <span class="input-title">
		                                    <label>
		                                        <h4>網路銀行帳號密碼設定</h4>
		                                    </label>
		                                </span>
		                                <span class="input-block">
		                                    <div class="ttb-input">
		                                        <span>您已經開通網路銀行服務，您可透過現有的使用者名稱、簽入密碼、交易密碼登入使用。</span>
		                                    </div>
		                                </span>
		                            </div>
	                            </c:if>
								<c:if test="${ input_data.data.checknb.equals('N') }">
									<div class="ttb-input-item row">
										<span class="input-title"> 
										<label>
											<h4>
												網路銀行帳號密碼設定
											</h4>
										</label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
                                        		<span class="input-subtitle subtitle-color"><spring:message code="LB.Loginpage_User_name"/></span>
												<input type="text" id="USERNAME" name="USERNAME" placeholder="請輸入<spring:message code="LB.Loginpage_User_name"/>" value="${ input_data.data.USERNAME }" class="text-input validate[required]" onkeyup="all_check()" maxLength="16">
											</div>
											<div class="ttb-input">
												<input type="text" id="USERNAME2" name="USERNAME2" placeholder="<spring:message code="LB.D1005"/>" value="${ input_data.data.USERNAME2 }" class="text-input validate[required]" onkeyup="all_check()" maxLength="16">
		                                        <span class="input-error-remarks" id="USERNAME2_S1"><i class="fa fa-times-circle mr-1" id="USERNAME2_I1"></i>使用者名稱需相同</span>
	                                        	<span class="input-error-remarks" id="USERNAME_S1"><i class="fa fa-times-circle mr-1" id="USERNAME_I1"></i>帳號、簽入密碼、交易密碼不得相同</span>
		                                        <span class="input-error-remarks" id="USERNAME_S2"><i class="fa fa-times-circle mr-1" id="USERNAME_I2"></i><spring:message code="LB.D1003"/></span>
		                                        <span class="input-error-remarks" id="USERNAME_S3"><i class="fa fa-times-circle mr-1" id="USERNAME_I3"></i>不能含有生日</span>
		                                        <span class="input-error-remarks" id="USERNAME_S4"><i class="fa fa-times-circle mr-1" id="USERNAME_I4"></i>不得設定為相同或升降冪的英文或數字，例如：11111、AAAAA、123456、654321、ABCDED、FEDCBA</span>	
											</div>
											<div class="ttb-input">
                                        		<span class="input-subtitle subtitle-color"><spring:message code="LB.X0210"/></span>
												<input type="password" class="text-input validate[required]"placeholder="請輸入簽入密碼"  id="LOGINPIN" name="LOGINPIN" value="" onkeyup="all_check()" maxLength="8">
											</div>
											<div class="ttb-input">
												<input type="password" id="LOGINPIN2" name="LOGINPIN2" placeholder="<spring:message code="LB.D1009"/>" value="" class="text-input validate[required]" onkeyup="all_check()" maxLength="8">
		                                        <span class="input-error-remarks" id="LOGINPIN2_S1"><i class="fa fa-times-circle mr-1" id="LOGINPIN2_I1"></i>簽入密碼需相同</span>
	                                        	<span class="input-error-remarks" id="LOGINPIN_S1"><i class="fa fa-times-circle mr-1" id="LOGINPIN_I1"></i>帳號、簽入密碼、交易密碼不得相同</span>
		                                        <span class="input-error-remarks" id="LOGINPIN_S2"><i class="fa fa-times-circle mr-1" id="LOGINPIN_I2"></i><spring:message code="LB.X0212"/></span>
		                                        <span class="input-error-remarks" id="LOGINPIN_S3"><i class="fa fa-times-circle mr-1" id="LOGINPIN_I3"></i>不能含有生日</span>
		                                        <span class="input-error-remarks" id="LOGINPIN_S4"><i class="fa fa-times-circle mr-1" id="LOGINPIN_I4"></i>不得設定為相同或升降冪的英文或數字，例如：11111、AAAAA、123456、654321、ABCDED、FEDCBA</span>
		                                        
											</div>
											<div class="ttb-input">
                                        		<span class="input-subtitle subtitle-color"><spring:message code="LB.X0211"/></span>
												<input type="password" id="TRANSPIN" name="TRANSPIN" value="" placeholder="請輸入交易密碼" class="text-input validate[required]" onkeyup="all_check()" maxLength="8">
											</div>
											<div class="ttb-input">
												<input type="password" id="TRANSPIN2" name="TRANSPIN2" value="" placeholder="<spring:message code="LB.D1012"/>" class="text-input validate[required]" onkeyup="all_check()" maxLength="8">
		                                        <span class="input-error-remarks" id="TRANSPIN2_S1"><i class="fa fa-times-circle mr-1" id="TRANSPIN2_I1"></i>交易密碼需相同</span>
	                                        	<span class="input-error-remarks" id="TRANSPIN_S1"><i class="fa fa-times-circle mr-1" id="TRANSPIN_I1"></i>帳號、簽入密碼、交易密碼不得相同</span>
		                                        <span class="input-error-remarks" id="TRANSPIN_S2"><i class="fa fa-times-circle mr-1" id="TRANSPIN_I2"></i><spring:message code="LB.X0212"/></span>
		                                        <span class="input-error-remarks" id="TRANSPIN_S3"><i class="fa fa-times-circle mr-1" id="TRANSPIN_I3"></i>不能含有生日</span>
		                                        <span class="input-error-remarks" id="TRANSPIN_S4"><i class="fa fa-times-circle mr-1" id="TRANSPIN_I4"></i>不得設定為相同或升降冪的英文或數字，例如：11111、AAAAA、123456、654321、ABCDED、FEDCBA</span>
											</div>
										</span>
									</div>
								</c:if>
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.D1425"/>
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
			                                <div class="ttb-input">
			                                    <label class="check-block" for="TRFLAG1">
													<spring:message code="LB.D0034_2"/>
			                                        <input type="checkbox" name="TRFLAG1" id="TRFLAG1" value="Y" checked disabled>
			                                        <span class="ttb-check"></span>
			                                    </label>
			                                </div>
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.D0279"/>
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
<!-- 											是 -->
												<spring:message code="LB.D0034_2"/>
												<input type="radio" name="EBILLFLAG" id="EBILLFLAG1" value="Y" onclick="errorMsg_hide('EBILLFLAG')"/>
												<span class="ttb-radio"></span>
											</label>&nbsp;
											<label class="radio-block">
<!-- 											否 -->
												<spring:message code="LB.D0034_3"/>
												<input type="radio" name="EBILLFLAG" id="EBILLFLAG2" value="N"  onclick="errorMsg_hide('EBILLFLAG')"/>
												<span class="ttb-radio"></span>
<!-- 												申請電子帳單 -->
											</label>
											<span class="hideblock validate_card">
												<input id="validate_EBILLFLAG" name="EBILLFLAG" type="radio"  
													class="validate[funcCall[validate_Radio['<spring:message code= "LB.D0279" />',EBILLFLAG]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
											</span>
										</div>
									</span>
								</div>
                           	</div>
							
	                        <!--button 區域 -->
							<!-- 重新輸入 -->
							<input type="button" name="CMBACK" id="CMBACK" value="<spring:message code="LB.X0318"/>" class="ttb-button btn-flat-gray">
                            <input id="CMSUBMIT" name="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.X0080"/>" />
                        <!--                     button 區域 -->
                        </div>  
                    </div>
                </form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>
