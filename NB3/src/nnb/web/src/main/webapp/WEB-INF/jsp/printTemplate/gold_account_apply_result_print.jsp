<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function(){
			window.print();
		});
	</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br/><br/>
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
	<br/><br/><br/>
	<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
	<br/><br/><br/>	
    <div id="step-bar">
        <ul>
            <li class="finished">注意事項與權益</li>
            <li class="finished">開戶資料</li>
            <li class="finished">確認資料</li>
            <li class="active">申請結果</li>
        </ul>
    </div>
	
	<div> 
		<!-- 資料 -->
		<!-- 表格區塊 -->
		<div class="ttb-input-block">
			<!-- 申請完成 -->					
			<div class="ttb-message">
				<p><spring:message code= "LB.X0932" /></p>
				<i class="fa fa-check-circle fa-5x success-color"></i>
			<span>申請結果</span>
            </div>
			
			<table class="print">
				<tr>
					<td style="text-align: center">申請時間</td>
					<td>
						${CMTIME}
					</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.X0933" /></td>
					<td>
						${GDACN}
					</td>
				</tr>
				<tr>
<!-- 指定網路銀行交易 -->
<!-- 買進／回售新臺幣帳戶 -->
					<td style="text-align: center"><spring:message code= "LB.W1066" /></td>
					<td>
						${SVACN}
					</td>
				</tr>
				<tr>
<!-- 領取黃金存摺之開戶行 -->
					<td style="text-align: center"><spring:message code= "LB.D1088" /></td>
					<td>
						${ BRHNUM }
					</td>
				</tr>
				<tr>
<!-- 開戶手續費 -->
					<td style="text-align: center"><spring:message code= "LB.D1082" /></td>
					<td>
<!-- 新臺幣 -->
<!-- 元 -->
						<spring:message code= "LB.NTD" /> ${ AMT_FEE }<spring:message code= "LB.Dollar" />
					</td>
				</tr>
			</table>
		</div>
		
		<br>
		<br>
		<div class="text-left">
			<spring:message code="LB.Description_of_page" />
			<ol class="list-decimal text-left">
<!--   您所提供的身分證資料，經本行查詢後，如有疑義，本行得暫停您的黃金存摺網路銀行功能。-->
			        <li><spring:message code= "LB.Gold_Account_Apply_P6_D1" /></li>
<!--  臨櫃領摺須知： -->
			         <li><spring:message code= "LB.Gold_Account_Apply_P6_D2" /><BR>
			         	<table>
			         		<tr><td>（1）</td><td><spring:message code= "LB.Gold_Account_Apply_P6_D22" /><font color="red"><u><b><spring:message code= "LB.Gold_Account_Apply_P6_D22-1" /></b></u></font><spring:message code= "LB.Gold_Account_Apply_P6_D22-2" /><font color="red"><u><b><spring:message code= "LB.Gold_Account_Apply_P6_D22-3" /></b></u></font></td></tr>
			         		<tr><td>（2）</td><td><spring:message code= "LB.Gold_Account_Apply_P6_D23" /><font color="red"><u><b><spring:message code= "LB.Gold_Account_Apply_P6_D23-1" /></b></u></font><spring:message code= "LB.Gold_Account_Apply_P6_D23-2" /><font color="red"><u><b><spring:message code= "LB.Gold_Account_Apply_P6_D23-3" /></b></u></font><spring:message code= "LB.Gold_Account_Apply_P6_D23-4" /><font color="red"><u><b><spring:message code= "LB.Gold_Account_Apply_P6_D23-5" /></b></u></font><spring:message code= "LB.Gold_Account_Apply_P6_D23-6" /></td></tr>
			         		<!--請親攜身分證件、新台幣存款存摺及原留存款印鑑至黃金存摺開戶行辦理領取黃金存摺手續。  -->
			         		<tr><td>（3）</td><td><spring:message code= "LB.Gold_Account_Apply_P6_D24" /></td></tr>
		         	        <tr><td>（4）</td><td>為保護您的安全，交易結束或離開電腦時，請務必將晶片金融卡抽離讀卡機或是將電子簽章 (載具i-key)拔除，並登出系統。</td></tr>
			         	</table>
			         </li>
<!-- 查詢分行位址 -->
			         <li><spring:message code= "LB.Gold_Account_Apply_P6_D3" /><a href="https://www.tbb.com.tw/5_3_2.html?" target="_blank">https://www.tbb.com.tw/5_3_2.html?</a></li>
			</ol>
		</div>
	</div>
	
</body>
</html>