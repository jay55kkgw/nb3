<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
	$(document).ready(function() {
		window.print();
	});
</script>
</head>
	<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
		<br/><br/>
		<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
		<br/><br/>
		<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
		<br/><br/>
		<ul class="ttb-result-list">
			<!-- 分行名稱 -->
			<li>
				<h3><spring:message code="LB.W0883" />：</h3>
                <p>${BRHNUM}</p>
			</li>
			  <!-- 地　　址 -->
             <li>
                <h3><spring:message code="LB.W0884" />：</h3>
                <p>${BRHADR}</p>
             </li>
             <!-- 電　　話 -->
             <li>
                <h3><spring:message code="LB.W0885" />：</h3>
                <p>${TELNUM1}</p>
             </li>
		<br/>
		<!-- 房屋擔保借款繳息清單  表格 -->
		<c:forEach items="${dataListMap}" var="map" >
		<table class="print" data-toggle-column="first">
			<tr>
			<!-- 借戶姓名 -->
				<td><spring:message code= "LB.W0886" /></td>
				<td class="font1" style="text-align: left; -ms-word-break: break-all;">${NAME}</td>
				<!-- 統一編號 -->
				<td rowspan="2"><spring:message code= "LB.D0621" /></td>
				<td rowspan="2">${CUSIDN}</td>
				<!-- 房屋座落 -->
				<td rowspan="2"><spring:message code= "LB.W0888" /></td>
				<td rowspan="2" colspan="2">${map.ADDR}</td>
			</tr>
			<tr>
			<!-- 房屋所有權人姓名 -->
				<td><spring:message code= "LB.W0889" /></td>
				<td>${map.OWNERNM}</td>
			</tr>
			<tr>
			<!-- 房屋所有權取得日 -->
				<td><spring:message code= "LB.W0890" /></td>
				<!-- 貸款帳號 -->
				<td>
				<!-- 貸款帳號 -->
					<spring:message code= "LB.W0891" /><br>
					<!-- 序號 -->
					<spring:message code= "LB.Serial_number" />
				</td>
				<!-- 最初貸款金額(元) -->
				<td colspan="2"><spring:message code= "LB.W0893" /></td>
				<!-- 貸款起日 -->
				<td><spring:message code= "LB.W0880" /></td>
				<!-- 貸款迄日 -->
				<td><spring:message code= "LB.W0881" /></td>
				<td>
				<!-- 本期末未償還 -->
					<spring:message code= "LB.W0896" /><br>
					<!-- 本金餘額(元) -->
					<spring:message code= "LB.W0897" />
				</td>
			</tr>
				<tr>
				<!-- 房屋所有權取得日 -->
					<td>${map.OWNDATE}</td>
					<td>
						<!-- 貸款帳號 -->
						${map.ACN}<br>
						<!-- 序號 -->
						${map.SEQ}
					</td>
					<!-- 最初貸款金額(元) -->
					<td colspan="2">${map.AMTORLN}</td>
					<!-- 貸款起日 -->
					<td>${map.DATFSLN}</td>
					<!-- 貸款迄日 -->
					<td>${map.DDT}</td>
					<!-- 本期末未償還 --> <!-- 本金餘額(元) -->
					<td>${map.BAL_E}</td>
				</tr>
			<tr>
				<!-- 繳息所屬年月 -->
				<td><spring:message code= "LB.W0898" /></td>
				<td colspan="3">
					${map.SINQ}
					~
					${map.DINQ}									
				</td>
				<!-- 繳息金額 -->
				<td><spring:message code= "LB.W0899" /></td>
				<td colspan="2">${map.TOTAMT_E}</td>
			</tr>
		</table>
		</c:forEach>
	</body>
</html>