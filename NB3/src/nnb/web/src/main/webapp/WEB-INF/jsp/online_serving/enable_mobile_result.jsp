<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {
		// 將.table變更為footable
		//initFootable();
		setTimeout("initDataTable()",100);
	});
	
	//下拉式選單
 	function formReset() {
 		if ($('#actionBar').val()=="reEnter"){
	 		document.getElementById("formId").reset();
	 		$('#actionBar').val("");
 		}
	}
</script>
</head>
	<body>
		<!-- header     -->
		<header>
			<%@ include file="../index/header.jsp"%>
		</header>
	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 行動銀行服務     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0318" /></li>
		</ol>
	</nav>

		<!-- menu、登出窗格 -->
		<div class="content row">
			<%@ include file="../index/menu.jsp"%>
			<!-- 	快速選單內容 -->
			<!-- content row END -->
			<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2><spring:message code="LB.D0318"/></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="">
					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-message">
								<p><spring:message code="LB.Change_successful"/></p>
							</div>
							<!-- 線上啟用行動銀行服務 -->
							<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
								<thead>
									<tr>
										<!-- 目前設定狀態 -->
										<th data-title="<spring:message code="LB.D0319"/>"><spring:message code="LB.D0319"/></th>
										<!-- 啟用/停用日期及時間 -->
										<th data-title="<spring:message code="LB.D0320"/>"><spring:message code="LB.D0320"/></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<c:choose>
											<c:when test="${enable_mobile_result.currentStatus eq 'A' }">
												<!-- 已啟用 -->
												<td><font style="color:green; font-weight:bold;"><spring:message code="LB.D0329"/></font></td>
											</c:when>
											<c:when test="${enable_mobile_result.currentStatus eq 'D' }">
												<!-- 已停用 -->
												<td><font style="color:red; font-weight:bold;"><spring:message code="LB.D0328"/></font></td>
											</c:when>
											<c:otherwise>
												<!-- 未啟用 -->
												<td><font style="color:gray; font-weight:bold;"><spring:message code="LB.D0322"/></font></td>
											</c:otherwise>
										</c:choose>
										<td style="text-align:center;">
											<c:if test="${transfer == 'en'}">
												${enable_mobile_result.modifyMonth }/${enable_mobile_result.modifyDay }/${enable_mobile_result.modifyYearEn }
												--
												${enable_mobile_result.modifyHH }:${enable_mobile_result.modifyMM }:${enable_mobile_result.modifySS }
											</c:if>
											<c:if test="${transfer != 'en'}">
												<spring:message code="LB.D0583"/>
												${enable_mobile_result.modifyYear }
												<spring:message code="LB.Year"/>
												${enable_mobile_result.modifyMonth }
												<spring:message code="LB.Month"/>
												${enable_mobile_result.modifyDay }
												<spring:message code="LB.Day"/>
												--
												${enable_mobile_result.modifyHH }
												<spring:message code= "LB.X1723" /><!-- 時 -->
												${enable_mobile_result.modifyMM }
												<spring:message code="LB.Minute"/>
												${enable_mobile_result.modifySS }
												<spring:message code="LB.Second"/>
											</c:if>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</form>
				</section>
			</main>
			<!-- 		main-content END -->
			<!-- 	content row END -->
		</div>
		<%@ include file="../index/footer.jsp"%>
	</body>
</html>