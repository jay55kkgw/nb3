
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<body>
	<div class="ttb-terms">
		<div class="ttb-result-list terms ">
				<p class="terms-text-bold terms-text-center">手機門號轉帳服務約定條款</p>
					<p><p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Calibri,sans-serif"><span style="font-family:標楷體">歡迎您使用臺灣中小企業銀行(下稱本行)「手機門號轉帳服務」(下稱本服務)，本服務之提供係透過財金資訊股份有限公司「門號轉帳平台」進行，為保障您的使用權益，在您註冊及使用本服務前，請務必詳讀以下約定條款並完成註冊程序，一旦您完成註冊程序或開始使用本服務，即視為您已充分瞭解並同意接受以下各項條款之約定：</span></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify">&nbsp;</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-family:標楷體">一、註冊程序</span></strong></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Calibri,sans-serif"><span style="font-family:標楷體">(</span><span style="font-family:標楷體">一)您得透過本行網路銀行/行動銀行辦理註冊及身分認證程序，以開戶時所登記留存之本人手機門號綁定一個本行存款帳號，作為轉帳交易之「預設收款帳號」，並得查詢該手機門號註冊狀態。</span></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Calibri,sans-serif"><span style="font-family:標楷體">(</span><span style="font-family:標楷體">二)本服務一次僅能於財金資訊股份有限公司「門號轉帳平台」綁定一個本行存款帳號，若需綁定本行其他帳號應重新完成註冊及身分認證程序。倘您至他行就同一手機門號進行註冊綁定他行存款帳戶，並作為「預設收款帳號」，則您於本行就本服務於財金資訊股份有限公司「門號轉帳平台」所註冊之資料將自動取消，相關「預設收款帳號」取消註冊之資訊，本行將另以電子郵件/簡訊方式通知您；除非您完全註銷本服務，否則本行系統將保留您的手機門號，俾於跨行間仍可透過本行「金融機構代號+手機門號」使用本服務。</span></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-family:標楷體">二、手機門號更新</span></strong></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Calibri,sans-serif"><span style="font-family:標楷體">(</span><span style="font-family:標楷體">一)您的手機門號發生變更、停用或被註銷時，應透過臨櫃/網路銀行/客服中心主動通知本行辦理手機門號資料更新，並於完成身分認證後以新手機門號綁定原本行存款帳號。</span></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Calibri,sans-serif"><span style="font-family:標楷體">(</span><span style="font-family:標楷體">二)<strong>若因您未主動依前項約定更新手機門號，致生交易錯帳之情事，而您因此所受之損失，本行不負賠償責任。</strong></span></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-family:標楷體">三、資料暨交易正確性聲明</span></strong></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Calibri,sans-serif"><span style="font-family:標楷體">(</span><span style="font-family:標楷體">一)<strong>您應聲明並擔保向本行提供之手機門號係本人所有，且完全真實、正確、完整，以確保交易安全及正確性</strong>，若因您提供之手機門號有任何錯誤、遺漏或不完整，導致無法使用本服務或交易錯誤所生之任何損失，本行不負賠償責任。</span></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Calibri,sans-serif"><span style="font-family:標楷體">(</span><span style="font-family:標楷體">二)<strong>您於使用本服務執行轉帳交易前，應確實檢查、核實所有交易資訊之正確性</strong>，當您向本行發出轉帳交易指示後即不可撤回、撤銷或否認，對您具有完全約束力。</span></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-family:標楷體">四、安全使用約定事項</span></strong></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Calibri,sans-serif"><span style="font-family:標楷體">(</span><span style="font-family:標楷體">一)</span><strong><span style="font-family:標楷體">您於使用本服務時，請自行採取防護措施(如防毒軟體)，並應妥善保管金融卡、存款帳號、網路/行動銀行登入帳號密碼等註冊相關資訊，不得以任何方式交付或轉借給第三人</span></strong><span style="font-family:標楷體">。任何經由您所註冊之手機門號及綁定本行存款帳號所進行之所有行為，除屬於不可歸責於您的事由外，應由您自負責任。</span></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Calibri,sans-serif"><span style="font-family:標楷體">(</span><span style="font-family:標楷體">二)</span><strong><span style="font-family:標楷體">您應合法、有效使用本服務，且保證未侵害任何第三方權益，包括但不限於侵害他人隱私權、專利權、智慧財產權及其他權利、冒用他人名義使用本服務或其他違法行為。</span></strong><span style="font-family:標楷體">倘因可歸責於您之事由違反本約定條款或開戶總約定書(含網路銀行暨行動銀行約定條款)或有其他違反法令規定之情事者，本行除得暫停或終止您使用本服務外，若因此造成本行或第三人受有損失，應由您自負賠償及相關法律責任。</span></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Calibri,sans-serif"><span style="font-family:標楷體">(</span><span style="font-family:標楷體">三)</span><span style="font-family:標楷體">您不得以木馬、病毒、惡意程式、後門程式、駭客入侵、阻斷式攻擊、上載資料或其它任何方式，修改、變更、增加或刪除本行系統設備之全部或一部，及阻礙、延滯、干擾、損壞或盜用本行或相關第三人任何硬體及軟體之使用、功能及運作。</span></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-family:標楷體">五、個人資料運用法定告知事項</span></strong></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Calibri,sans-serif"><span style="font-family:標楷體">(</span><span style="font-family:標楷體">一)<strong>您因使用本服務所提供之個人資料，包含手機門號、經遮罩之戶名及其他您為使用本服務所自主提供之必要資訊，本行得於提供本服務之特定目的範圍內，以自動化或其他非自動化之機器、應用程式或行動裝置蒐集、處理及利用您的個人資料，並於必要範圍內，提供予財金資訊股份有限公司、依法律規定或經法律授權之非公務機關，以及依法具有調查權之機關或金融監理機關。</strong></span></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Calibri,sans-serif"><span style="font-family:標楷體">(</span><span style="font-family:標楷體">二)依據個資法第三條規定，您就本行保有您之個人資料得行使下列權利：</span></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="background-color:white"><span style="font-family:Calibri,sans-serif"><span style="font-family:標楷體"><span style="color:black">1.</span></span><span style="font-family:標楷體"><span style="color:black">除有個資法第十條所規定之例外情形外，得向本行查詢、請求閱覽或請求製給複製本，惟本行依個資法第十四條規定得酌收必要成本費用。</span></span></span></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="background-color:white"><span style="font-family:Calibri,sans-serif"><span style="font-family:標楷體"><span style="color:black">2.</span></span><span style="font-family:標楷體"><span style="color:black">得向本行請求補充或更正，惟依個資法施行細則第十九條規定，您應適當釋明其原因及事實。</span></span></span></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="background-color:white"><span style="font-family:Calibri,sans-serif"><span style="font-family:標楷體"><span style="color:black">3.</span></span><span style="font-family:標楷體"><span style="color:black">本行如有違反個資法規定蒐集、處理或利用您之個人資料，依個資法第十一條第四項規定，您得向本行請求停止蒐集、處理及利用。</span></span></span></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="background-color:white"><span style="font-family:Calibri,sans-serif"><span style="font-family:標楷體"><span style="color:black">4.</span></span><span style="font-family:標楷體"><span style="color:black">依個資法第十一條第二項規定，個人資料正確性有爭議者，得向本行請求停止處理或利用您的個人資料。惟依該項但書規定，本行因執行業務所必須，或經您書面同意，並經註明其爭議者，不在此限。</span></span></span></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Calibri,sans-serif"><span style="font-family:標楷體">5.</span><span style="font-family:標楷體">依個資法第十一條第三項規定，個人資料蒐集之特定目的消失或期限屆滿時，得向本行請求刪除、停止處理或利用您之個人資料。惟依該項但書規定，本行因執行業務所必須或經您書面同意者，不在此限。</span></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Calibri,sans-serif"><span style="font-family:標楷體">(</span><span style="font-family:標楷體">三)</span><strong><span style="font-family:標楷體">您得自由選擇是否提供相關個人資料，惟您若拒絕提供相關個人資料，本行將無法執行必要之處理作業，致無法提供您本服務。</span></strong></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Calibri,sans-serif"><span style="font-family:標楷體">(</span><span style="font-family:標楷體">四)</span><strong><span style="font-family:標楷體">提醒您註冊或使用本服務前，應詳閱上開個人資料運用法定告知事項及</span></strong><strong><span style="font-family:標楷體">本行開戶總約定書</span></strong><strong><span style="font-family:標楷體">，當您開始註冊或使用本服務時，視為已充分瞭解相關權利義務並同意提供您的個人資料。</span></strong></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-family:標楷體">六、約定條款修訂效力</span></strong></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-family:標楷體">本約定條款如有修改或增刪時，本行應於變更前六十日揭露於本行網站，並以行動銀行推播或電子文件傳送等方式通知您，若您於任何修改或變更後繼續使用本服務，則視為您已閱讀、瞭解並同意接受該等修改或變更。</span></strong></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-family:標楷體">七、損害賠償</span></strong></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-family:標楷體">您已明確瞭解並同意，因非可歸責於本行之事由致您使用或無法使用本行網路銀行或行動銀行APP全部或一部所造成之直接或間接之損害，本行不負任何損害賠償責任。</span></strong></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-family:標楷體">八、電子文件之效力</span></strong></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Calibri,sans-serif"><span style="font-family:標楷體">您同意以電子文件作為表示方法，依本約定條款交換之電子文件，其效力與書面文件相同。但法令另有排除適用者，不在此限。</span></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-family:標楷體">九、</span></strong><strong><span style="font-family:標楷體">準據法及管轄法院</span></strong></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-family:標楷體">本約定條款之解釋與適用，以中華民國法律為準據法。因本服務所生之任何爭議及糾紛，雙方應先尋求以協商方式解決，如未能解決者，雙方同意以臺灣臺北地方法院為第一審管轄法院。但不得排除消費者保護法四十七條或民事訴訟法第四百三十六條之九規定小額訴訟管轄法院之適用。</span></strong></span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-family:標楷體">十、其他約定事項</span></strong></span></span></p>

<p><span style="font-size:12.0pt"><span style="font-family:標楷體">本約定服務條款未盡事宜，悉依中華民國相關法令、本行開戶總約定書及個人網路銀行暨行動銀行約定條款辦理。</span></span></p>

					</p>
		</div>
	</div>
</body>
<style>
	.ttb-result-list.terms p {
		line-height: 2rem;
	}

</style>

</html>