<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript">
		// 初始化
		$(document).ready(function () {
			init();
		});

		function init() {
			//表單驗證
			$("#formId").validationEngine({binded: false,promptPosition: "inline"});
			// 確認鍵 click
			goOn();
			//判斷是否需預設選取TW
			retccySelect();
			

		}
		//開啟匯款分類
		function openFxRemitQuery() {
			//國別
			var cnty = $('#COUNTRY').val();
			//收款人身份別 
			var remtype = $('#REMTYPE').val();
			//FROM_FCY_REMITTANCES=Y，表示是從外幣的匯入解款頁面開啟此頁面(110.12.22更新，國際部需求041_110_289)
			var fxremitqueryUrl = '${__ctx}/FCY/COMMON/FxRemitQuery?ADRMTTYPE=2&CUTTYPE=${f_remittances_payments_step2.data.CUSTYPE}' + '&CNTY=' + cnty + '&REMTYPE=' + remtype + "&FROM_FCY_REMITTANCES=Y";
			console.log('fxremitqueryUrl >>>>>> '+ fxremitqueryUrl)
			window.open(fxremitqueryUrl);
		}
		//開啟常用匯款分類
		function openMyRemitMenu() {
			window.open('${__ctx}/FCY/COMMON/MyRemitMenu');
		}

		// 確認鍵 click
		function goOn() {
			$("#CMSUBMIT").click(function (e) {
				if (!$('#formId').validationEngine('validate')) {
					e = e || window.event; // for IE
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					initBlockUI(); //遮罩
					processTran();
					$("#formId").submit();
				}
			});
		}
		//判斷是否需預設選取TW
		function retccySelect() {
			var str_Flag = '${f_remittances_payments_step2.data.str_Flag}';
			if('T' ===  str_Flag){
			 $('#RETCCY option[value=TWD]').attr('selected', 'selected');
			}
		}
		//
		function processTran() {
			var TXCCY = '${ f_remittances_payments_step2.data.TXCCY}';
			var RETCCY = $("#RETCCY").find(":selected").val(); //解款幣別
			//同幣別解款          
			$("#formId").removeAttr("target");
			if (TXCCY == RETCCY) {
				$('#SameCurrency').val('Y');
				$("#formId").attr("action", "${__ctx}/FCY/ACCT/ONLINE/f_remittances_payments_confirm");
			}
			//不同幣別解款
			else {
				$("#formId").attr("action", "${__ctx}/FCY/ACCT/ONLINE/f_remittances_payments_step3");
			}
		}
		//檢查輸入匯款分類編號
		function validate_Srcfunddesc(field, rules, i, options){
			var Srcfunddesc = $('#SRCFUNDDESC').val(); 
			console.log("Srcfunddesc >>" + Srcfunddesc);
			 if (Srcfunddesc === "") {
				 // <!-- 請輸入匯款分類編號 -->
		   	     return '<spring:message code= "LB.Alert146" />';   				
			  }	
		}
		//檢查繳款方式
		function validate_checkbox(field, rules, i, options){	
			// 勾選
			if($("#PAYAGREE").prop('checked')){
				
			}
			else{
				//您尚未同意費用繳款方式,請勾選
				 return '<spring:message code= "LB.X1491" />';   
			}
		}
		
		 function setremtype(obj) {
				var main = document.getElementById("main");	           
			    var curr = obj.options[obj.selectedIndex].value;
				if (curr == '#')   	   	        
					return false;
				$('#REMTYPE').val(curr);
				$('#FXRMTDESC').val("");
				$('#SHOW_SRCFUNDDESC').text('<spring:message code="LB.Please_select_code" />');
			  }	 
		 
		
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
	<!-- 匯入匯款     -->
    		<li class="ttb-breadcrumb-item"><spring:message code="LB.W0125" /></li>
    <!-- 線上解款     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0317" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!--外匯匯出匯款 -->
				<h2>
					<spring:message code="LB.W0317" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form autocomplete="off" method="post" id="formId"
					action="${__ctx}/FCY/ACCT/ONLINE/f_remittances_payments_step3">
					<input type="hidden" name="ADOPID" value="F003">
					<input type="hidden" id="SameCurrency" name="SameCurrency" value="N">
					<input type="hidden" id="SRCFUND" name="SRCFUND" value="">
					<input type="hidden" id="CMTRANPAGE" name="CMTRANPAGE" value="1">
					<input type="hidden" id="REMTYPE" name="REMTYPE"   value="">	
					<input type="hidden" id="CUSTYPE" name="CUSTYPE" value="${f_remittances_payments_step2.data.CUSTYPE}">
					<input type="hidden" id="COUNTRY" name="COUNTRY" value="${f_remittances_payments_step2.data.CNTRY}">
					<!-- 表單顯示區 -->
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<div class="ttb-message">
									<span>
										<!-- 請確認解款資料 -->
										<spring:message code="LB.X0054" />
									</span>
								</div>
								<!-- 匯入匯款編號-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.X0055" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${f_remittances_payments_step2.data.REFNO}</span>
										</div>
									</span>
								</div>
								<!--解款人-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.X0056" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color"><spring:message code="LB.X0059" />/&nbsp;<spring:message code="LB.X0060" /></span>
											<span>
												${f_remittances_payments_step2.data.BENNAME}
												<br>
												${f_remittances_payments_step2.data.BENAD1}
												<br>
												${f_remittances_payments_step2.data.BENAD2}
												<br>
												${f_remittances_payments_step2.data.BENAD3}
											</span>
										</div>
									</span>
								</div>
								<!--匯款人-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0136" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color"><spring:message code="LB.X0059" />/&nbsp;<spring:message code="LB.X0060" /></span>
											<span>
												${f_remittances_payments_step2.data.ORDNAME}
												<br>
												${f_remittances_payments_step2.data.ORDAD1}
												<br>
												${f_remittances_payments_step2.data.ORDAD2}
												<br>
												${f_remittances_payments_step2.data.ORDAD3}
											</span>
										</div>
									</span>
								</div>
								<!--匯款銀行-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.X0057" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color"><spring:message code="LB.X0059" />/&nbsp;<spring:message code="LB.X0060" /></span>
											<span>
												${f_remittances_payments_step2.data.RCVBANK}
												<br>
												${f_remittances_payments_step2.data.RCVAD1}
												<br>
												${f_remittances_payments_step2.data.RCVAD2}
												<br>
												${f_remittances_payments_step2.data.RCVAD3}
											</span>
										</div>
									</span>
								</div>
								<!-- 匯款人身份別 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.X0058" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
										<!-- 請選擇匯款人身份別 -->
											<select class="custom-select select-input half-input validate[funcCall[validate_CheckSelect['<spring:message code= "LB.X1492" />',BENTYPE,#]]]"  id="BENTYPE" name="BENTYPE" onchange="setremtype(this)">
												<option value="#">---<spring:message code="LB.Select" />---</option>
												<option value="1"><spring:message code="LB.Government" /></option>
												<option value="2"><spring:message code="LB.Public_enterprise" /></option>
												<option value="3"><spring:message code="LB.Private_enterprise" /></option>
												<option value="4"><spring:message code="LB.Other_Account" /></option>
												<option value="5"><spring:message code="LB.My_Account" /></option>
											</select>
										</div>
									</span>
								</div>
								<!--匯款地區國家-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.X0061" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${f_remittances_payments_step2.data.CNTRY}</span>
										</div>
									</span>
								</div>
								<!--匯款分類項目-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Select" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<font id="SHOW_SRCFUNDDESC"><spring:message code="LB.Please_select_code" /></font>
											<input class="text-input validate[funcCallRequired[validate_Srcfunddesc[SRCFUNDDESC]]]" type="text" name="SRCFUNDDESC" id="SRCFUNDDESC"  style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;">
										</div>
										<div class="ttb-input">
											<button class="btn-flat-orange" type="button" value="<spring:message code="LB.Menu" />" name="FXQREMITNO"
												onclick="openFxRemitQuery()"><spring:message code="LB.Menu" /></button>
											<button class="btn-flat-orange" type="button" value="<spring:message code="LB.Common_menu" />" name="FXQREMITNO2"
												onclick="openMyRemitMenu()"><spring:message code="LB.Common_menu" /></button>
											<br />
											<span class="input-unit"><spring:message code="LB.Remittance_Classification_Note" /></span>
											<br />
											<spring:message code="LB.Consulting_line" />：<font id="ADCONTACTTEL">
											${f_remittances_payments_step2.data.ADCONTACTTEL}</font>
										</div>
									</span>
								</div>
								<!-- 匯款分類說明 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0299" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<font id="SHOW_FXRMTDESC"><spring:message code="LB.Please_select_code" /></font>
											<input class="text-input" name="FXRMTDESC" id="FXRMTDESC" type="hidden" value="" placeholder="<spring:message code="LB.Please_select_code" />" readonly>
										</div>
									</span>
								</div>
								<!--匯入匯款金額-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.X0062" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${f_remittances_payments_step2.data.TXCCY}
												${f_remittances_payments_step2.data.showTXAMT}
											</span>
										</div>
									</span>
								</div>
								<!--解款帳號-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0329" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${f_remittances_payments_step2.data.BENACC}</span>
										</div>
									</span>
								</div>
								<!--解款幣別-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.X0063" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<select class="custom-select select-input half-input" name="RETCCY" id="RETCCY">
												<c:forEach var="dataList" items="${ f_remittances_payments_step2.data.optionList}">
													<option value='${dataList.TXCCY}'> ${dataList.TXCCY}</option>
												</c:forEach>
											</select>
										</div>
									</span>
								</div>
								<!--費用繳款方式-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.X0064" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span><spring:message code="LB.X0065" /></span>
										</div>
										<div class="ttb-input">
											<label class="check-block" for="PAYAGREE">
											<input class="validate[funcCall[validate_checkbox[PAYAGREE]]]" type="checkbox" name="PAYAGREE" id="PAYAGREE" value="Y">
												<spring:message code="LB.X0066" />
												<spring:message code="LB.X0067" />
											<span class="ttb-check"></span>
											</label>
										</div>
									</span>
								</div>
							</div>
							<input class="ttb-button btn-flat-gray" type="reset" value="<spring:message code="LB.Re_enter" />" />
							<input class="ttb-button btn-flat-orange" id="CMSUBMIT" type="button" value="<spring:message code= "LB.X1148" />" />
						</div>
					</div>
					<font color="#FF0000"><spring:message code="LB.Confirm_exchange_rate" />
					</font>
					<div class="text-left">
						<!-- 						說明： -->
						<!-- <spring:message code="LB.Description_of_page"/>: -->
					</div>
				</form>
			</section>
			<!-- main-content END -->
		</main>
	</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>