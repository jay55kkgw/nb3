<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>
<script type="text/javascript" src="${__ctx}/js/jquery.maskedinput.js"></script>
<!-- DIALOG會用到 -->
<script type="text/javascript" src="${__ctx}/js/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery-ui.css">

<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		// HTML載入完成後開始遮罩
	// 	setTimeout("initBlockUI()", 10);
		initBlockUI();
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
		
	function init(){
		$("#formId").validationEngine({
			binded: false,
			promptPosition: "inline"
		});
		
		$("#CMSUBMIT").click(function(e){
			
			e = e || window.event;
			if(!$('#formId').validationEngine('validate')){
	    		e.preventDefault();
	    	}
			else{
				$("#formId").attr("action","${__ctx}/ONLINE/APPLY/creditcard_limit_increase_p3");
				$("#formId").submit();
			}
		});
		
		$("#CMBACK").click(function(e){
			$("#formId").validationEngine('detach');
			if ($("#FROM_NB3").val() == 'Y') {
				top.location = '${pageContext.request.contextPath}'+'/ONLINE/APPLY/online_apply_menu';
			} else {
				window.open("https://www.tbb.com.tw/web/guest/-2", "_self");	
			}
		});
	}
	
	var sec = 120;
	var the_Timeout;

	function countDown()
	{
		var main = document.getElementById("formId");      
      	var counter = document.getElementById("CountDown");                      
       	counter.innerHTML = sec;
       	sec--;
       	
       	if (sec == -1) 
       	{              
        	main.getotp.disabled = false;
			$('#getotp').removeClass('btn-flat-gray');
        	$('#getotp').addClass('btn-flat-orange');
			sec =120;
			$("#OTP").prop("disabled", true);
			$("#CMSUBMIT").prop("disabled", true);
			$('#CMSUBMIT').removeClass('btn-flat-orange');
        	$('#CMSUBMIT').addClass('btn-flat-gray');
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert199' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
           	return;                
       	}                                     
       
       	//網頁倒數計時(120秒)              
     	the_Timeout = setTimeout("countDown()", 1000);    
	} 
	
	//取得otp並發送簡訊
	function getsmsotp() {
		uri = '${__ctx}' + "/RESET/smsotp_ajax";
		$("#CUSIDN").val($("#CUSIDN").val().toUpperCase());
		$("#OTP").prop("disabled", false);
		$("#CMSUBMIT").prop("disabled", false);
		$('#CMSUBMIT').removeClass('btn-flat-gray');
    	$('#CMSUBMIT').addClass('btn-flat-orange');
		if ($("#CUSIDN").val() != "") {
			rdata = {
				ADOPID : $("#ADOPID").val(),
				CUSIDN : $("#CUSIDN").val(),
				MSGTYPE : "N822${limit_increase.data.PHONE }"
			};
			
			console.log("creatSmsotp.uri: " + uri);
			console.log("creatSmsotp.rdata: " + rdata);
			
			//關閉按鈕
			$("#getotp").attr("disabled",true);
			$('#getotp').removeClass('btn-flat-orange');
			$('#getotp').addClass('btn-flat-gray');
			//觸發倒數計時
// 			countDown();
			//簡訊發送異常先註解
			data = fstop.getServerDataEx(uri, rdata, false,
					callbackgetsmsotp);
	    }
		else{
			var main = document.getElementById("formId");
			//alert("<spring:message code= "LB.D0025" />");
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.D0025' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
// 	    	main.CUSIDN.focus();
	    	return false;
		}
	}
	
	function callbackgetsmsotp(data) {
		console.log("data: " + data);
	    var main = document.getElementById("formId");
		var msgstr = data[0].MSGSTR;
		var msgcod = data[0].MSGCOD;		
		var phone = data[0].phone;		
		if(msgcod=="0000")
		{		
			countDown();
			phone = phone = phone.substring(0,4)+"-XXX-X"+phone.substring(8);
		    errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.X2280' />"+phone+"<br/><spring:message code= 'LB.X2281' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
		    main.OTP.focus();			
		}
		else if(msgcod=="XQ03")
		{
// 			sec =120;
// 	  		var counter = document.getElementById("CountDown");                      
// 	   		counter.innerHTML = sec;			
			main.getotp.disabled = false;
			$('#getotp').removeClass('btn-flat-gray');
	    	$('#getotp').addClass('btn-flat-orange');
	    	$("#OTP").prop("disabled", true);
			$("#CMSUBMIT").prop("disabled", true);
			$('#CMSUBMIT').removeClass('btn-flat-orange');
        	$('#CMSUBMIT').addClass('btn-flat-gray');
	    	errorBlock(
					null, 
					null,
					[msgcod+":"+msgstr], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
	    	$("#errorBtn1").click(function() {
	    		$("#formId").attr("action","${__ctx}/ONLINE/APPLY/creditcard_limit_increase");
				$("#formId").submit();
			});
		}				
		else
		{
// 			sec =120;
// 	  		var counter = document.getElementById("CountDown");                      
// 	   		counter.innerHTML = sec;			
			main.getotp.disabled = false;
			$('#getotp').removeClass('btn-flat-gray');
	    	$('#getotp').addClass('btn-flat-orange');
	    	$("#OTP").prop("disabled", true);
			$("#CMSUBMIT").prop("disabled", true);
			$('#CMSUBMIT').removeClass('btn-flat-orange');
        	$('#CMSUBMIT').addClass('btn-flat-gray');
	    	errorBlock(
					null, 
					null,
					[msgcod+":"+msgstr], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
		}				
	}
</script>
</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Credit_Card_Limit_Increase" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2><spring:message code="LB.Credit_Card_Limit_Increase" /></h2>
				<div id="step-bar">
                    <ul>
                        <li class="finished"><spring:message code="LB.D0851" /></li><!-- 身份驗證 -->
                        <li class="active"><spring:message code="LB.SMS_Verification" /></li>
                        <li class=""><spring:message code="LB.X1967" /></li><!-- 申請資料 -->
                        <li class=""><spring:message code="LB.Confirm_data" /></li><!-- 確認資料 -->
                        <li class=""><spring:message code="LB.X1968" /></li><!-- 完成申請 -->
                    </ul>
                </div>
				<form method="post" id="formId" action="">
					<input type="hidden" name="ADOPID" id="ADOPID" value="N822"/>
					<input type="hidden" name="CUSIDN" id="CUSIDN" value="${limit_increase.data.CUSIDN }"/>
					<input type="hidden" name="back" id="back" value=""/>
					<input type="hidden" id="FROM_NB3" name="FROM_NB3" value="${FROM_NB3}"/>
					<!-- 表單顯示區  -->
					<div class="main-content-block pl-0 pr-0 row">
						<div class="col-12">
							<div class="ttb-input-block">
								<div class="ttb-message">
									<p><spring:message code="LB.SMS_Verification" /></p>
								</div>
							
								<div>
									<div class="ttb-input-item row">
										<spring:message code="LB.D1595" />（${limit_increase.data.PHONEshow }）<br><!-- 0912-XXX-X00 -->
										<spring:message code="LB.D1596" />
									</div>
									
									<div class="ttb-input-item row">
										<input type="button" id="getotp" value="<spring:message code="LB.D1600" />" class="ttb-button btn-flat-orange" style="position: initial; bottom: 0px;" onclick="javascript:getsmsotp()"/>
									</div>
									
									<div class="ttb-input-item row">
										<spring:message code="LB.D1597" /><br><!-- 0912-XXX-X00 -->
										<spring:message code="LB.D1598" /><br>
										<spring:message code="LB.D1599" />
									</div>
								</div>
									
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.SMS_Verification" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input id="OTP" type="text" class="ttb-input text-input validate[required,funcCall[validate_CheckLenEqual[<spring:message code="LB.Captcha"/>,OTP,false,8,8]],funcCall[validate_CheckNumber['LB.Captcha',OTP]]]" name="OTP" placeholder="<spring:message code='LB.X1702' />" maxlength="8" autocomplete="off" disabled="disabled">
										</div>
										<span class="ttb-unit"><spring:message code= "LB.X1575" />：<font id="CountDown" color="red"></font><spring:message code= "LB.Second" /></span>
									</span>
								</div>
							</div>
							<!-- 取消 -->
							<input type="button" name="CMBACK" id="CMBACK" value="<spring:message code="LB.Cancel" />" class="ttb-button btn-flat-gray"/>
							<input type="button" id="CMSUBMIT" value="<spring:message code="LB.X0080" />" class="ttb-button btn-flat-gray" disabled="disabled"/><!-- 下一步 -->
						</div>
					</div>
				</form>
			</section>
		</main>
	</div>
	
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>