<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp" %>

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
		initFootable();
		$('#but_Agree').prop('checked', false);
		// 確認鍵 click
		submit();
	}
	
	// 確認鍵 Click
	function submit() {
		$("#CMSUBMIT").click( function(e) {
			console.log("submit~~");
			// 遮罩
         	initBlockUI();
            $("#formId").attr("action","${__ctx}/ONLINE/APPLY/use_creditcard_step1");
	 	  	$("#formId").submit();
		});
		$("#CMPRINT").click(function(){
			var params = {
					"jspTemplateName":"use_creditcard_print",
					//臺灣企銀信用卡申請網路銀行
					"jspTitle":"<spring:message code= "LB.X1085" />"
			};
			openWindowWithPost("${__ctx}/ONLINE/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
		});	
	}

</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Register" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
			<!-- 信用卡申請網路銀行 -->
				<h2><spring:message code= "LB.X1086" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="">
                <div class="main-content-block row">
                    <div class="col-12">
                        <div class="ttb-message">
                        <!-- 電子銀行業務總契約書 -->
                        <!-- 為確保您的權益，請詳細閱讀本約定條款所載事項，您如接受本約定條款請按 我同意約定條款 鍵 鍵，以完成申請作業，您如不同意條款內容，則請按 取消 鍵，本行將不受理您的申請。-->
                            <p><spring:message code= "LB.X1078" /></p>
    								<p><font color="blue" size="3"><b><spring:message code= "LB.X1208" /><span style="border: 1px solid #000000;background-color: #FF0000;color: #FFFFFF;"><spring:message code= "LB.W1554" /></span><spring:message code= "LB.X1209" /><span style="border: 1px solid #000000;background-color: #FF0000;color: #FFFFFF;"><spring:message code= "LB.Cancel" /></span><spring:message code= "LB.X1210" /></b></font></p>
								
						</div>
						
						<div align="left">
									<h4>壹、線上申請網路銀行約定事項</h4>
						</div>
                        
                        <ul class="ttb-result-list">
                            <li class="full-list"><span><b>第一條　</b></span><span class="input-subtitle subtitle-color">服務範圍</span><br>使用信用卡申請網路銀行：係指立約人經由連結網際網路之電腦，以身分證字號、出生年月日、信用卡卡號等真實資料，向貴行線上申請網路銀行服務後，即可使用網路銀行查詢立約人的信用卡帳務、申請信用卡電子帳單、補寄信用卡帳單、信用卡繳款電子郵件通知等服務。
							</li>
							
                            <li class="full-list"><span><b>第二條　</b></span><span class="input-subtitle subtitle-color">密碼管理</span><br>
							使用信用卡申請網路銀行：立約人首次登入網路銀行應進行密碼變更。因遺忘使用者名稱、簽入密碼、交易密碼或連續輸入錯誤達五次遭系統停止使用時，立約人應親自電洽信用卡客服專線0800-01-7171確認後，次日起再使用信用卡線上申請網路銀行服務。<br>
        					若使用者名稱或簽入密碼或交易密碼有遭他人得知、洩漏或竊用之虞者，立約人應自行登入網路銀行變更密碼，並即依上開手續辦理。
							</li>
						</ul>
							<div align="left">
								<h4>貳、共通約定事項</h4>
							</div>
						<ul class="ttb-result-list">
                            <li class="full-list"><span><b>第一條　</b></span><span class="input-subtitle subtitle-color">貴行資訊</span>
								<ul>
										<li>
											一、貴行名稱： 臺灣中小企業銀行 
										</li>
										<li>
											二、申訴及客服專線：0800-00-7171#5(申訴)，#1(客服)  
										</li>
										<li>
											三、網址：https://www.tbb.com.tw
										</li>
										<li>
											四、地址： 台北市塔城街30號 
										</li>
										<li>
											五、傳真號碼：02-2550-8338
										</li>
										<li>
											六、貴行電子信箱：臺灣企銀網站 https://www.tbb.com.tw 客服信箱 （tbb@mail.tbb.com.tw） 
										</li>
									
								</ul>
							</li>
							
                            <li class="full-list"><span><b>第二條　</b></span><span class="input-subtitle subtitle-color">適用範圍</span><br>本契約係網路銀行業務服務之一般性共同約定，除個別契約另有約定外，悉依本契約之約定。個別契約不得牴觸本契約。但個別契約對立約人之保護更有利者，從其約定，所指之保護，不含貴行基於業務考量所為之各項作業推廣措施。	  
							</li>
							
							 <li class="full-list"><span><b>第三條　</b></span><span class="input-subtitle subtitle-color">名詞定義</span>
								 <ul>
								 	<li>
								 		一、「網路銀行業務」：指立約人端電腦經由網際網路或行動網路與貴行電腦連線，無須親赴貴行櫃台，即可直接取得貴行所提供之各項金融服務。
								 	</li>
									<li>
										二、「電子文件」：指貴行或立約人經由網路連線傳遞之文字、聲音、圖片、影像、符號或其他資料，以電子或其他以人之知覺無法直接認識之方式，所製成足以表示其用意之紀錄，而供電子處理之用者。 
									</li>
									<li>
										三、「數位簽章」：指將電子文件以數學演算法或其他方式運算為一定長度之數位資料，以簽署人之私密金鑰對其加密，形成電子簽章，並得以公開金鑰加以驗證者。
									</li>
									<li>
								 		四、「憑證」：指載有簽章驗證資料，用以確認簽署人身分、資格之電子形式證明。 
									</li>
									<li>
										五、「私密金鑰」：係指具有配對關係之數位資料中，由簽署人保有，用以製作數位簽章者。
									</li>
									<li>
										六、「公開金鑰」：係指具有配對關係之數位資料中，對外公開，用以驗證數位簽章者。
									</li>
									<li>
									 	七、「服務時間」：除了另行公告服務時間之項目外，提供二十四小時服務。
									</li>
								 </ul>
							</li>
							
							
							 <li class="full-list"><span><b>第四條　</b></span><span class="input-subtitle subtitle-color">網頁之確認</span><br>立約人使用網路銀行前，請先確認網路銀行正確之網址，才使用網路銀行服務；如有疑問，請電客服電話詢問。貴行應以一般民眾得認知之方式，告知立約人網路銀行應用環境之風險。貴行應盡善良管理人之注意義務，隨時維護網站的正確性與安全性，並隨時注意有無偽造之網頁，以避免立約人之權益受損。 
							</li>
							
							 <li class="full-list"><span><b>第五條　</b></span><span class="input-subtitle subtitle-color">服務項目</span><br>貴行應於本契約載明提供之服務項目，如於網路銀行網站呈現相關訊息者，並應確保該訊息之正確性，其對立約人所負之義務不得低於網站之內容。 
							</li>
							
							 <li class="full-list"><span><b>第六條　</b></span><span class="input-subtitle subtitle-color">連線所使用之網路</span><br>貴行及立約人同意使用網路進行電子文件傳送及接收。貴行及立約人應分別就各項權利義務關係與各該網路業者簽訂網路服務契約，並各自負擔網路使用之費用。
							</li>
							
							<li class="full-list"><span><b>第七條　</b></span><span class="input-subtitle subtitle-color">電子文件之接收與回應</span><br>貴行接收含數位簽章或經貴行及立約人同意用以辨識身分之電子文件後，除查詢之事項外，貴行應提供該交易電子文件中重要資訊之網頁供立約人再次確認後，即時進行檢核及處理，並將檢核及處理結果，以電子文件之方式通知立約人。貴行或立約人接收來自對方任何電子文件，若無法辨識其身分或內容時，視為自始未傳送。但貴行可確定立約人身分時，應立即將內容無法辨識之事實，以電子文件之方式通知立約人。
							</li>
							
								<li class="full-list"><span><b>第八條　</b></span><span class="input-subtitle subtitle-color">電子文件之不執行</span><br>
									如有下列情形之一，貴行得不執行任何接收之電子文件：
									<ul>
										<li>
											一、有具體理由懷疑電子文件之真實性或所指定事項之正確性者。
										</li>
										<li>
											二、貴行依據電子文件處理，將違反相關法令之規定者。
										</li>
										<li>
											三、貴行因立約人之原因而無法於帳戶扣取立約人所應支付之費用者。貴行不執行前項電子文件者，應同時將不執行之理由及情形，以電子文件之方式通知立約人，立約人受通知後得以電話或電子郵件方式向貴行確認。
										</li>
										
									</ul>
							</li>
							
							<li class="full-list"><span><b>第九條　</b></span><span class="input-subtitle subtitle-color">電子文件交換作業時限</span><br>電子文件係由貴行電腦自動處理，立約人發出電子文件，經立約人依第七條第一項貴行提供之再確認機制確定其內容正確性後，傳送至貴行後即不得撤回。但未到期之預約交易在貴行規定之期限內，得撤回、修改。若電子文件經由網路傳送至貴行後，於貴行電腦自動處理中已逾貴行營業時間時（營業時間：為星期一至星期五（例假日除外）及政府公告補上班日，上午九時至下午三時三十分），貴行應即以電子文件通知立約人，該筆交易將改於次一營業日處理或依其他約定方式處理。
							</li>
							<li class="full-list"><span><b>第十條　</b></span><span class="input-subtitle subtitle-color">系統故障之權宜處理</span><br>立約人同意當連線設備或貴行系統或第三人網路服務業系統發生故障時，貴行得採行必要之權宜措施處理。
							</li>
							<li class="full-list"><span><b>第十一條　</b></span><span class="input-subtitle subtitle-color">立約人軟硬體安裝與風險</span><br>立約人申請使用本契約之服務項目，應自行安裝所需之電腦軟體、硬體，以及其他與安全相關之設備。安裝所需之費用及風險，由立約人自行負擔。第一項軟硬體設備及相關文件如係由貴行所提供，貴行僅同意立約人於約定服務範圍內使用，不得將之轉讓、轉借或以任何方式交付第三人。貴行並應於網站及所提供軟硬體之包裝上載明進行本服務之最低軟硬體需求，且負擔所提供軟硬體之風險。立約人於契約終止時，如貴行要求返還前項之相關設備，應以契約特別約定者為限。
							</li>
							
							<li class="full-list"><span><b>第十二條　</b></span><span class="input-subtitle subtitle-color">立約人連線與責任</span><br>	立約人申請使用本契約之服務項目，應自行安裝所需之電腦軟體、硬體，以及其他與安全相關之設備。安裝所需之費用及風險，由立約人自行負擔。貴行與立約人有特別約定者，必須為必要之測試後，始得連線。立約人對貴行所提供之使用者代號、密碼、憑證及其它足以識別身分之工具，應負保管之責。立約人輸入前項密碼連續錯誤達五次時，貴行電腦即自動停止立約人使用本契約之服務。立約人如擬恢復使用，應依約定辦理相關手續。惟網路銀行未屆轉帳日之各項預約轉帳仍屬有效，將於轉帳日依預約指示轉帳。
							</li>
							
								<li class="full-list"><span><b>第十三條　</b></span><span class="input-subtitle subtitle-color">交易核對</span><br>	
									貴行於每筆交易指示處理完畢後，以電子文件、電話或書面之方式通知立約人，立約人應核對其結果有無錯誤。如有不符，應於使用完成之日起四十五日內，以電話或電子郵件方式通知貴行查明。
									<br>
									貴行應於每月對立約人以書面或電子文件方式寄送上月之交易對帳單（該月無交易時不寄）。立約人核對後如認為交易對帳單所載事項有錯誤時，應於收受之日起四十五日內，以電話或電子郵件方式通知貴行查明。貴行對於立約人之通知，應即進行調查，並於通知到達貴行之日起三十日內，將調查之情形或結果以書面方式覆知立約人。
							</li>
							<li class="full-list"><span><b>第十四條　</b></span><span class="input-subtitle subtitle-color">電子文件錯誤之處理</span><br>	
										立約人利用本契約之服務，其電子文件如因不可歸責於立約人之事由而發生錯誤時，貴行應協助立約人更正，並提供其他必要之協助。前項服務因可歸責於貴行之事由而發生錯誤時，貴行應於知悉時，立即更正，並同時以電子文件或雙方約定之方式通知立約人。立約人利用本契約之服務，其電子文件因可歸責於立約人之事由而發生錯誤時，倘屬立約人申請或操作轉入之金融機構代號、存款帳號或金額錯誤，致轉入他人帳戶或誤轉金額時，一經立約人通知貴行，貴行應即辦理以下事項：
								<ul>
									<li>
										一、依據相關法令提供該筆交易之明細及相關資料。
									</li>
									<li>
										二、通知轉入行協助處理。
									</li>
									<li>
										三、回報處理情形。
									</li>
								</ul>
							</li>
							
							<li class="full-list"><span><b>第十五條　</b></span><span class="input-subtitle subtitle-color">電子文件之合法授權與責任</span><br>	
									貴行及立約人應確保所傳送至對方之電子文件均經合法授權。<br>
									貴行或立約人於發現有第三人冒用或盜用使用者代號、密碼、憑證、私密金鑰，或其他任何未經合法授權之情形，應立即以電話、電子文件或書面方式通知他方停止使用該服務並採取防範之措施。
								
									貴行接受前項通知前，對第三人使用該服務已發生之效力，由貴行負責。但有下列任一情形者，不在此限：
								<ul>
									<li>
										一、貴行能證明立約人有故意或過失。
									</li>
									<li>
										二、貴行依電子文件、電話或書面方式通知交易核對資料或帳單後超過四十五日。惟立約人有特殊事由致無法通知者，以該特殊事由結束日起算四十五日，但貴行有故意或過失者，不在此限。
									</li>
									
        							針對第二項冒用、盜用事實調查所生之鑑識費用由貴行負擔。
								</ul>
							</li>
							
							
							<li class="full-list"><span><b>第十六條　</b></span><span class="input-subtitle subtitle-color">資料系統安全</span><br>	貴行及立約人應各自確保所使用資訊系統之安全，防止非法入侵、取得、竄改、毀損業務紀錄或立約人個人資料。第三人破解貴行資訊系統之保護措施或利用資訊系統之漏洞爭議，由貴行就該事實不存在負舉證責任。第三人入侵貴行資訊系統對立約人所造成之損害，由貴行負擔。
        			
							</li>
								<li class="full-list"><span><b>第十七條　</b></span><span class="input-subtitle subtitle-color">保密義務</span><br>除其他法律規定外，貴行應確保所交換之電子文件因使用或執行本契約服務而取得立約人之資料，不洩漏予第三人，亦不可使用於與本契約無關之目的，且於經立約人同意告知第三人時，應使第三人負本條之保密義務。前項第三人如不遵守此保密義務者，視為本人義務之違反。
        			
							</li>
							
							<li class="full-list"><span><b>第十八條　</b></span><span class="input-subtitle subtitle-color">資料完整</span><br>立約人使用貴行所提供之各項服務，如需以書面方式為之者，立約人仍須補足書面資料後方屬完成手續，惟在尚未補足書面資料前，仍應對於完成訊息傳送之交易負責。		
							</li>
							
							<li class="full-list"><span><b>第十九條　</b></span><span class="input-subtitle subtitle-color">損害賠償責任</span><br>	貴行及立約人同意依本契約傳送或接收電子文件，因可歸責於當事人一方之事由，致有遲延、遺漏或錯誤之情事，而致他方當事人受有損害時，該當事人應就他方所生之損害負賠償責任。		
							</li>
							
							<li class="full-list"><span><b>第二十條　</b></span><span class="input-subtitle subtitle-color">紀錄保存</span><br>貴行及立約人應保存所有交易指示類電子文件紀錄，並應確保其真實性及完整性。貴行對前項紀錄之保存，應盡善良管理人之注意義務。保存期限為五年以上，但其他法令有較長規定者，依其規定。	
							</li>
							
							<li class="full-list"><span><b>第二十一條　</b></span><span class="input-subtitle subtitle-color">電子訊息之效力</span><br>貴行及立約人同意以電子文件作為表示方法，依本契約交換之電子文件，其效力與書面文件相同。但法令另有排除適用者，不在此限。
							</li>
							<li class="full-list"><span><b>第二十二條　</b></span><span class="input-subtitle subtitle-color">立約人終止契約及變更本服務之內容</span><br>	立約人得隨時終止本契約及變更本服務之內容，但應親自、書面或雙方約定方式辦理。
							</li>
							<li class="full-list"><span><b>第二十三條　</b></span><span class="input-subtitle subtitle-color">銀行終止契約</span><br>	立約人得隨時終止本貴行終止本契約時，須於終止日三十日前以書面通知立約人。立約人如有下列情事之一者，貴行得隨時以書面或雙方約定方式通知立約人終止本契約：
								<ul>
									<li>
        								一、立約人未經貴行同意，擅自將契約之權利或義務轉讓第三人者。
									</li>
									<li>
										二、立約人依破產法聲請宣告破產或消費者債務清理條例聲請更生、清算程序者。
									</li>
									<li>
										三、立約人違反本「共通約定事項」（十五）至（十七）之規定者。
									</li>
									<li>
										四、立約人違反本契約之其他約定，經催告改善或限期請求履行未果者。
									</li>
									
									本網路銀行約定事項之終止，以貴行辦妥註銷登錄始為生效，對於終止前發送訊息所需完成或履行之義務不生任何影響，惟網路銀行未屆轉帳日之各項預約轉帳自終止生效起即停止轉帳作業。
								</ul>
							</li>
							<li class="full-list"><span><b>第二十四條　</b></span><span class="input-subtitle subtitle-color">個人資料之利用</span><br>立約人同意貴行及與貴行有業務往來之機構，於符合其營業登記項目或章程所定業務之需要，得蒐集、處理或國際傳輸及利用立約人之個人資料。
							</li>
							<li class="full-list"><span><b>第二十五條　</b></span><span class="input-subtitle subtitle-color">委外作業</span><br>立約人同意貴行得於主管機關核定或核准得委外之作業事項範圍內，將涉及本約定書有關之資訊作業得委託適當之第三人處理。
							</li>
							<li class="full-list"><span><b>第二十六條　</b></span><span class="input-subtitle subtitle-color">異常提領規定</span><br>貴行因電腦故障或線路中斷，以致不能提供本網路銀行服務時，立約人仍能親赴貴行櫃台提領帳戶款項；但因貴行無法得知電腦故障或線路中斷前立約人已發送之付款指示，因此立約人同意貴行得視立約人往來狀況彈性酌予提領，如有溢領情事應即時返還。
							</li>
							<li class="full-list"><span><b>第二十七條　</b></span><span class="input-subtitle subtitle-color">契約修訂</span><br>本契約條款如有修改或增刪時，貴行以書面或雙方約定方式通知立約人後，立約人於七日內不為異議者，視同承認該修改或增刪約款。但下列事項如有變更，應於變更前六十日以書面或雙方約定方式通知立約人，並於該書面或雙方約定方式以顯著明確文字載明其變更事項、新舊約款內容，暨告知立約人得於變更事項生效前表示異議，及立約人未於該期間內異議者，視同承認該修改或增刪約款；並告知立約人如有異議，應於前項得異議時間內通知貴行終止契約：
								<ul>
									<li>
        								一、第三人冒用或盜用使用者代號、密碼、憑證、私密金鑰，或其他任何未經合法授權之情形，貴行或立約人通知他方之方式。
									</li>
									<li>
										二、其他經主管機關規定之事項。
									</li>
								</ul>
							</li>
							<li class="full-list"><span><b>第二十八條　</b></span><span class="input-subtitle subtitle-color">文書送達</span><br>立約人同意以契約中載明之地址為相關文書之送達處所，倘立約人之地址變更，應即以書面或其他約定方式通知貴行，並同意改依變更後之地址為送達處所；如立約人未以書面或依約定方式通知變更地址時，貴行仍以契約中立約人載明之地址或最後通知貴行之地址為送達處所。
							</li>
							<li class="full-list"><span><b>第二十九條　</b></span><span class="input-subtitle subtitle-color">法令適用</span><br>本契約準據法，依中華民國法律。
							</li>
							<li class="full-list"><span><b>第三十條　</b></span><span class="input-subtitle subtitle-color">法院管轄</span><br>因本契約而涉訟者，貴行及立約人同意以臺灣臺北地方法院為第一審管轄法院。但不得排除消費者保護法第四十七條或民事訴訟法第四百三十六條之九小額訴訟管轄法院之適用。
							</li>
							<li class="full-list"><span><b>第三十一條　</b></span><span class="input-subtitle subtitle-color">標題</span><br>本契約各條標題，僅為查閱方便而設，不影響契約有關條款之解釋、說明及瞭解。
							</li>
							<li class="full-list"><span><b>第三十二條　</b></span><span class="input-subtitle subtitle-color">申訴管道</span><br>立約人如對本契約有爭議，申訴管道如下：
								<ul>
									<li>
        								1.免付費服務電話：0800-00-7171#5。
									</li>
									<li>
										2.電子信箱(e-mail)：臺灣企銀網站 http://www.tbb.com.tw 客服信箱
									</li>
								</ul>
							</li>
						</ul>
						<!-- 取消 -->
						<input class="ttb-button btn-flat-gray" type="button" name="CMBACK" id="CMBACK" value="<spring:message code="LB.Cancel" />" onclick="window.close();"/>
						<!-- 列印 -->
						<input type="button" class="ttb-button btn-flat-gray" name="CMPRINT" id="CMPRINT" value="<spring:message code="LB.Print" />" />
						<!-- 我同意約定條款 -->
                        <input class="ttb-button btn-flat-orange" type="button" name="CMSUBMIT" id="CMSUBMIT" value="<spring:message code= "LB.W1554" />" />
                		<input type="hidden" id="ADOPID" name="ADOPID" value="NA40">
                    </div>
                </div>
                </form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>