<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",100);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",200);
	
	$("#CMSUBMIT").click(function(){
		initBlockUI();
		$("#formID").attr("action","${__ctx}/BOND/DEAL/bond_sign");
		initBlockUI();//遮罩
		$("#formID").submit();
	});

	$("#CMCANCEL").click(function(){
		initBlockUI();
		$("#formID").attr("action","${__ctx}/INDEX/index");
		initBlockUI();//遮罩
		$("#formID").submit();
	});
	
});

</script>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 海外債券     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2391" /></li>
    <!-- 海外債券交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2515" /></li>
    <!-- 海外債券特別約定事項暨同意書     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X2538" /></li>
		</ol>
	</nav>

	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
<!-- 			海外債券特別約定事項暨同意書 -->
				<h2><spring:message code="LB.X2538" /></h2>
				<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
					<form id="formID" method="post">
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<div class="ttb-message">
<!-- 								海外債券交易 -->
	                                <h4><spring:message code="LB.X2515" /></h4>
	                            </div>
								<div class="ttb-message" align="left">
<!-- 	                                 您尚未簽屬「海外債券特別約定事項暨同意書」，簽屬後才可進行申購，是否需要線上簽屬「海外債券特別約定事項暨同意書」。 -->
	                           		<spring:message code="LB.X2539" />
	                            </div>
                            </div>
							<input type="button" id="CMCANCEL" value="<spring:message code="LB.Cancel"/>" class="ttb-button btn-flat-gray"/>	
							<input type="button" id="CMSUBMIT" value="<spring:message code="LB.Confirm"/>" class="ttb-button btn-flat-orange"/>
						</div>
					</div>	
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>