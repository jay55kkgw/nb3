<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function(){
			window.print();
		});
	</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br/><br/>
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
	<br/><br/><br/>
	<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
	<br/><br/><br/>
	
	<div> 
		<div class="ttb-input-block">
			<!-- 查詢時間 -->
			<p>
				<spring:message code="LB.Inquiry_type"/>：
	            <c:if test="${ QueryTerms.equals('0') }">
	            	<spring:message code="LB.D0336"/>
	            </c:if>
	            <c:if test="${ QueryTerms.equals('1') }">
	            	<spring:message code="LB.D0337"/>  － ${ CarId }
	            </c:if>
	            <c:if test="${ QueryTerms.equals('2') }">
	            	<spring:message code="LB.D0338"/> － ${ Account }
	            </c:if>
			</p>
			<!-- 查詢期間 -->
			<p>
				<spring:message code="LB.Inquiry_period" /> : ${CMSEACHPERDATE}
			</p>
			<!-- 查詢帳號 : -->
	        <p>
				<spring:message code="LB.D0359" />:
	        	<spring:message code="LB.D0360_1"/> ${TotalRecord } <spring:message code="LB.Rows" />
		                            <spring:message code="LB.D0360_3"/><br>(<spring:message code="LB.D0361"/> ${WaitPayRecord } <spring:message code="LB.Rows" />
		                            、<spring:message code="LB.D0362"/> ${PaySuccessRecord } <spring:message code="LB.Rows" />
		                            、<spring:message code="LB.D0363"/> ${PayFailRecord } <spring:message code="LB.Rows" />)
			</p>
			<!-- 資料總數 : -->
			<p>
				<spring:message code="LB.Inquiry_time" /> : ${CMQTIME}
			</p>
			<!-- 匯入金額總金額 : -->
			
			
			<!-- 資料Row -->
			
			<table class="print">
				<tr>
					<th><spring:message code="LB.D0059"/></th>
					<th><spring:message code="LB.D0342"/></th>
					<th><spring:message code="LB.D0343"/></th>
					<th><spring:message code="LB.D0364"/></th>
					<th><spring:message code="LB.D0365"/></th>
					<th><spring:message code="LB.D0366"/></th>
					<th><spring:message code="LB.D0367"/></th>
					<th><spring:message code="LB.D0168"/></th>
					<th><spring:message code="LB.D0368"/></th>
					<th><spring:message code="LB.D0369"/></th>
				</tr>
				<c:forEach var="dataList" items="${print_datalistmap_data}">
					<tr>
		                <td>${dataList.ZoneCode }</td>
		                <td>${dataList.CarId }</td>
		                <td>${dataList.CarKind }</td>
		                <td>${dataList.SlipNo }</td>
		                <td>${dataList.OpenDate }</td>
		                <td>${dataList.LastDate}</td>
		                <td>${dataList.OpenAmt }</td>
		                <td>${dataList.PayAccount }</td>
		                <td>${dataList.PayDate}</td>
		                <td>${dataList.SessionCode }</td>
					</tr>
				</c:forEach>
			</table>
		</div>
		
		<br>
		<br>
		<div class="text-left">
			<!-- 		說明： -->
			<spring:message code="LB.Description_of_page"/>
			<ol class="list-decimal text-left">
	                       	     <li><spring:message code="LB.parking_withholding_query_result_P3_D1"/></li>
		
				            	 <li><spring:message code="LB.parking_withholding_query_result_P3_D2"/><a href="#" onClick="window.open('http://www.tcgpmo.nat.gov.tw/index.php?act=query')" ><spring:message code="LB.parking_withholding_query_result_P3_D2-1"/></a><spring:message code="LB.parking_withholding_query_result_P3_D2-2"/>。</li>
			
			            		 <li><spring:message code="LB.parking_withholding_query_result_P3_D3"/><a href="#" onClick="window.open('http://info1.tpc.gov.tw/querycar/parking.htm')" ><spring:message code="LB.parking_withholding_query_result_P3_D3-1"/></a><spring:message code="LB.parking_withholding_query_result_P3_D3-2"/>。</li>
			
			           		 	 <li><spring:message code="LB.parking_withholding_query_result_P3_D4"/><a href="#" onClick="window.open('http://kpp.tbkc.gov.tw/')" ><spring:message code="LB.parking_withholding_query_result_P3_D4-1"/></a><spring:message code="LB.parking_withholding_query_result_P3_D4-2"/>。</li>
				
				            	 <li><spring:message code="LB.parking_withholding_query_result_P3_D5"/></li>       
	
			</ol>
		</div>
	</div>
</body>
</html>