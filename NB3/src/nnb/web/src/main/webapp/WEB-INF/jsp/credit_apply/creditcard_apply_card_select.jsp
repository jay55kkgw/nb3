<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>
<!--舊版驗證-->
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
<script type="text/javascript">
	var cardgroup = "";
	var cardgroup2 = "";
	var urlBRANCH = '${result_data.data.requestParam.BRANCH}';
	//卡片按鈕陣列
	//var cardgroups = ['04','07','10','06','11'];
	var cardgroups2 = ['04','07','10','11'];
	//var cardgroups2 = ['05','01','03','08','09'];
	var cardgroups = ['06','05','01','03','08'];
	if('040' == urlBRANCH){
		//20220117業務單位又改
		cardgroups = ['05']
	}
$(document).ready(function(){
	$("#formId").validationEngine({
		binded: false,
		promptPosition: "inline"
	});
	$("#card-tab-2").hide();
	selectCard();
	cardchk();
	changeCard("card_"+cardgroup);
	tabEvent();
	getQR();
	
	if('040' == urlBRANCH){
		$('#nav-trans-now').click();
		$('#nav-trans-future').hide();
		
	}
	
// 	if(${result_data.data.requestParam.oldcardownerBoolean == "true"}){
// 		$("input[type=checkbox][value=2]").prop("checked",true);
// 		$("input[type=checkbox][value=1]").prop("disabled",true);
// 		$("#helpMemo").hide();
// 	}
// 	else{
// 		$("input[type=checkbox][value=2]").prop("disabled",true);
// 		$("input[type=checkbox][value=1]").prop("checked",true);
// 		$("#helpMemo").show();
// 	}
	$("#CMSUBMIT").click(function(e){
		var CARDNAME = $("#CARDNAME").val();//第一張卡值
		
		var CN = $("#CARDNAME option:selected").text();//第一張卡片名稱
		$("#CN").val(CN);
		
		//確認是否重複申請卡片
		var applingCardListStr ='${result_data.data.ApplingCardListStr}';
		var cardArray = applingCardListStr.split(',');
		for (var i=0; i<cardArray.length; i++){
			  if (CARDNAME == cardArray[i]) {
				  errorBlock(null, null, ["<spring:message code='LB.X2434' />"],
							'<spring:message code= "LB.Confirm" />', null);
					return false;
	  		}
		}
		

		if(CARDNAME == "#"){
			errorBlock(null, null, ["<spring:message code='LB.Alert032' />"],
					'<spring:message code= "LB.Quit" />', null);
			return false;
		};
		e = e || window.event;
		if(!$('#formId').validationEngine('validate')){
    		e.preventDefault();
    	}
		else{
			initBlockUI();
			$("#formId").submit();
		}
	});
});
var smartcard1 = ['26','27','37','38','41','45','46','47','63','66','69','72','80','82'];//定義悠遊卡 
var smartcard2 = ['38','48','61','65','68','81','85','86'];//定義一卡通代碼
var displaycardimg = ['01','03','11','13','26','27','31','32','38','41','43','45','46','48','51','52','61','63','65','66','68','69','80','81','82','85','86','58'];//要顯示圖片的卡片
var cardimgnum = ['11-050-4-0001.png','11-050-4-0002.png','11-050-5-0006.png','11-050-5-0005.png','11-050-5-0004.png',
	             '11-050-5-0010.png','11-050-4-0003.png','11-050-4-0006.png','11-050-5-0008.png','11-050-5-0007.png',
	             '11-050-5-0016.png','11-050-5-0011.png','11-050-4-0007.png','11-050-5-0009.png','11-050-4-0004.png',
	             '11-050-4-0005.png','11-050-5-0014.png','11-050-5-0012.png','11-050-5-0015.png','11-050-5-0013.png',
	             '11-050-5-0017.png','11-050-5-0018.png','11-050-5-0019.png','11-050-5-0022.png','11-050-5-0022.png',
	             '11-050-5-0020.png','11-050-5-0021.png','11-050-5-0023.png'];//卡片檔名
var cardmsg = ['最高0.5%現金回饋','最高0.5%現金回饋','最高0.5%現金回饋','最高0.5%現金回饋','最高0.65%現金回饋 無上限','最高0.65%現金回饋 無上限',
    			'最高0.75%現金回饋','免費參觀故宮及國外10大博物館','綠色交通最高200元回饋','綠色通路最高10%回饋','國外刷卡享2%現金回饋無上限．享機場貴賓室及市區停車優惠。',
    			'最高0.65%現金回饋 無上限','免費參觀故宮及國外10大博物館，國外刷卡最高享2%現金回饋','綠色交通最高450元回饋','免費參觀故宮及國外10大博物館','免費參觀故宮及國外10大博物館',
    			'在信用卡內置入萬年爐香灰，加上別具創意的卡面設計，緊密串聯媽祖信仰和持卡人的連結。更提供最高1.5%海外現金回饋無上限。','在信用卡內置入萬年爐香灰，加上別具創意的卡面設計，緊密串聯媽祖信仰和持卡人的連結。更提供最高1.5%海外現金回饋無上限。',
    			'在信用卡內置入萬年爐香灰，加上別具創意的卡面設計，緊密串聯媽祖信仰和持卡人的連結。更提供最高1.5%海外現金回饋無上限。','在信用卡內置入萬年爐香灰，加上別具創意的卡面設計，緊密串聯媽祖信仰和持卡人的連結。更提供最高1.5%海外現金回饋無上限。',
    			'當您刷卡消費，臺灣企銀就會提撥消費金額0.3%專款專用幫助銀髮長輩共食共學','當您刷卡消費，臺灣企銀就會提撥消費金額0.3%專款專用幫助銀髮長輩共食共學','本行秉持扶助銀髮族享受健康樂活及提升中小企業競爭力之理念，與信保基金合作推出「信保基金認同卡」，由本行提撥一般刷卡消費金額0.1%專款專用於幫助銀髮長輩共食共學；0.1%回饋信保基金；0.1%回饋經本行送信保基金案件之保證手續費',
    			'老人議題為全球關懷的焦點，本行秉持「老吾老以及人之老」之精神，及落實扶助銀髮族享受健康樂活之理念，推出「銀色之愛信用卡」，由本行提撥一般刷卡消費金額千分之三，除幫助銀髮族群，同時將愛心散撥至全台各角落，扶助弱勢族群、身心障礙者、落實社區照顧及社會救助等。',
    			'老人議題為全球關懷的焦點，本行秉持「老吾老以及人之老」之精神，及落實扶助銀髮族享受健康樂活之理念，推出「銀色之愛信用卡」，由本行提撥一般刷卡消費金額千分之三，除幫助銀髮族群，同時將愛心散撥至全台各角落，扶助弱勢族群、身心障礙者、落實社區照顧及社會救助等。',
    			'首次申辦本認同卡核卡後90天內一般刷卡消費3筆，且每筆金額達NT$666元(含)以上，每卡提撥NT$500予獅子會作為公益活動使用','首次申辦本認同卡核卡後90天內一般刷卡消費3筆，且每筆金額達NT$666元(含)以上，每卡提撥NT$500予獅子會作為公益活動使用',
    			'免費參觀國內外百大指定博物館，國外刷卡最高享2%現金回饋無上限'];	             

var cardmsgen = ['Maximum 0.5% cash back','Maximum 0.5% cash back','Maximum 0.5% cash back','Maximum 0.5% cash back','Maximum 0.65% cash back without limit','Maximum 0.65% cash back without limit','Maximum 0.75% cash back','Free visit to the Forbidden City and 10 major foreign museums',
	'Up to $200 feedback for green energy transportation','Up to 10% feedback on green channels','Foreign credit card enjoy 2% cash feedback without limit. Enjoy airport lounge and city parking discounts. ','Maximum 0.65% cash back without limit','Free visit to the Forbidden City and 10 major foreign museums, 2% cash credit for foreign card swipe',
	'Up to $450 feedback for green energy transportation','Free visit to the Forbidden City and 10 major foreign museums','Free visit to the Forbidden City and 10 major foreign museums','In the credit card built into the millennium incense, plus a creative card design, close to the link between Mazu belief and cardholders. There is no upper limit for up to 1.5% of overseas cash feedback. ',
	'In the credit card built into the millennium incense, plus a creative card design, close to the link between Mazu belief and cardholders. There is no upper limit for up to 1.5% of overseas cash feedback. ',
	'In the credit card built into the millennium incense, plus a creative card design, close to the link between Mazu belief and cardholders. There is no upper limit for up to 1.5% of overseas cash feedback. ',
	'In the credit card built into the millennium incense, plus a creative card design, close to the link between Mazu belief and cardholders. There is no upper limit for up to 1.5% of overseas cash feedback. ',
	'When you swipe your card, Taiwan’s corporate bank will set aside 0.3% of the special amount of money to help the silver-haired elders eat together.',
	'When you swipe your card, Taiwan’s corporate bank will set aside 0.3% of the special amount of money to help the silver-haired elders eat together.',
	'The Bank adheres to the philosophy of helping the silver-haired people to enjoy health and enhance the competitiveness of SMEs. It has cooperated with the Sino-Singapore Fund to launch the "Sincere Insurance Fund Approval Card". The Bank will allocate 0.1% of the general credit card spending amount to help the silver-haired elders to eat together. A total of 0.1% of the credit insurance fund; 0.1% of the guarantee fee for the case of the Bank\'s delivery of the insurance fund',
	'The issue of the elderly is the focus of global care. The Bank adheres to the spirit of "Old and old and the old" and implements the concept of helping the silver-haired people enjoy healthy and happy lives. The Bank launched the "Silver Love Credit Card". Three-thousandths of the amount, in addition to helping the silver-haired ethnic group, also distributes love to all corners of Taiwan, helping the disadvantaged groups, the physically and mentally disabled, implementing community care and social assistance, etc.',
	'The issue of the elderly is the focus of global care. The Bank adheres to the spirit of "Old and old and the old" and implements the concept of helping the silver-haired people enjoy healthy and happy lives. The Bank launched the "Silver Love Credit Card". Three-thousandths of the amount, in addition to helping the silver-haired ethnic group, also distributes love to all corners of Taiwan, helping the disadvantaged groups, the physically and mentally disabled, implementing community care and social assistance, etc.',
	'In the first 90 days after the first application for this approval card, the card will generally be used for 3 transactions, and the amount of each transaction is more than NT$666 (inclusive), and each card withdraws NT$500 to the Lions club as a charity event.','First application After the approval of the card, the card is generally used for 3 transactions within 90 days, and the amount of each transaction is more than NT$666 (inclusive), and each card withdraws NT$500 to the Lions club for public welfare activities.',
	'Free access to the top 100 designated museums at home and abroad, foreign credit card up to 2% cashback unlimited']	             
	             
var cardmsgch = ['最高0.5%现金回馈','最高0.5%现金回馈','最高0.5%现金回馈','最高0.5%现金回馈','最高0.65%现金回馈 无上限','最高0.65%现金回馈 无上限','最高0.75%现金回馈','免费参观故宫及国外10大博物馆','绿色交通最高200元回馈','绿色通路最高10%回馈','国外刷卡享2%现金回馈无上限．享机场贵宾室及市区停车优惠。 ',
	'最高0.65%现金回馈 无上限','免费参观故宫及国外10大博物馆，国外刷卡最高享2%现金回馈','绿色交通最高450元回馈','免费参观故宫及国外10大博物馆','免费参观故宫及国外10大博物馆','在信用卡内置入万年炉香灰，加上别具创意的卡面设计，紧密串联妈祖信仰和持卡人的连结。更提供最高1.5%海外现金回馈无上限。 ','在信用卡内置入万年炉香灰，加上别具创意的卡面设计，紧密串联妈祖信仰和持卡人的连结。更提供最高1.5%海外现金回馈无上限。 ',
	'在信用卡内置入万年炉香灰，加上别具创意的卡面设计，紧密串联妈祖信仰和持卡人的连结。更提供最高1.5%海外现金回馈无上限。 ','在信用卡内置入万年炉香灰，加上别具创意的卡面设计，紧密串联妈祖信仰和持卡人的连结。更提供最高1.5%海外现金回馈无上限。 ','当您刷卡消费，台湾企银就会提拨消费金额0.3%专款专用帮助银发长辈共食共学','当您刷卡消费，台湾企银就会提拨消费金额0.3%专款专用帮助银发长辈共食共学',
	'本行秉持扶助银发族享受健康乐活及提升中小企业竞争力之理念，与信保基金合作推出「信保基金认同卡」，由本行提拨一般刷卡消费金额0.1%专款专用于帮助银发长辈共食共学；0.1%回馈信保基金；0.1%回馈经本行送信保基金案件之保证手续费',
	'老人议题为全球关怀的焦点，本行秉持「老吾老以及人之老」之精神，及落实扶助银发族享受健康乐活之理念，推出「银色之爱信用卡」，由本行提拨一般刷卡消费金额千分之三，除帮助银发族群，同时将爱心散拨至全台各角落，扶助弱势族群、身心障碍者、落实小区照顾及社会救助等。',
	'老人议题为全球关怀的焦点，本行秉持「老吾老以及人之老」之精神，及落实扶助银发族享受健康乐活之理念，推出「银色之爱信用卡」，由本行提拨一般刷卡消费金额千分之三，除帮助银发族群，同时将爱心散拨至全台各角落，扶助弱势族群、身心障碍者、落实小区照顾及社会救助等。',
	'首次申办本认同卡核卡后90天内一般刷卡消费3笔，且每笔金额达NT$666元(含)以上，每卡提拨NT$500予狮子会作为公益活动使用','首次申办本认同卡核卡后90天内一般刷卡消费3笔，且每笔金额达NT$666元(含)以上，每卡提拨NT$500予狮子会作为公益活动使用',
	'免费参观国内外百大指定博物馆，国外刷卡最高享2%现金回馈无上限']	  

var cardtype = ['普卡','金卡','白金卡','鈦金','御璽'];
// var cardtype = ['御璽','鈦金','白金卡','金卡','普卡'];

var imgUrl =['https://www.tbb.com.tw/web/guest/visa-','https://www.tbb.com.tw/web/guest/visa-','https://www.tbb.com.tw/web/guest/master-','https://www.tbb.com.tw/web/guest/master-',
		'https://www.tbb.com.tw/web/guest/-283','https://www.tbb.com.tw/web/guest/-285','https://www.tbb.com.tw/web/guest/visa-','https://www.tbb.com.tw/web/guest/-282',
		'https://www.tbb.com.tw/web/guest/-284','https://www.tbb.com.tw/web/guest/-283','https://www.tbb.com.tw/web/guest/-278','https://www.tbb.com.tw/web/guest/-285',
		'https://www.tbb.com.tw/web/guest/-282','https://www.tbb.com.tw/web/guest/-284','https://www.tbb.com.tw/web/guest/-282','https://www.tbb.com.tw/web/guest/-282',
		'https://www.tbb.com.tw/web/guest/-288','https://www.tbb.com.tw/web/guest/-288','https://www.tbb.com.tw/web/guest/-288','https://www.tbb.com.tw/web/guest/-288',
    	'https://www.tbb.com.tw/web/guest/-487','https://www.tbb.com.tw/web/guest/-487','https://www.tbb.com.tw/web/guest/-111-1','https://www.tbb.com.tw/web/guest/-487',
    	'https://www.tbb.com.tw/web/guest/-487','https://www.tbb.com.tw/web/guest/-630','https://www.tbb.com.tw/web/guest/-630','https://www.tbb.com.tw/web/guest/-fun-'];

//選擇卡片變更資料
function change(){
	var CARDNAME = $("#CARDNAME").val();
	$("#card_name").empty();
	$("#displaymsg").empty();
	$("#more").attr("onclick","");
	$("#addValue").hide();
	
	if(CARDNAME != "#"){
		$("#card_name").text($("#CARDNAME").find(':selected').text());
		$("#checkcard").prop("checked",true);
		$("#smartcard").val("");
		
		for(var i=0;i<smartcard2.length;i++){
			var card2 = smartcard2[i];
			
			if(CARDNAME == card2){
				$("#smartcard").val("Y");
				
				$("#addValue").show();
				break;
			}
		}
	}
	else{
		$("#checkcard").prop("checked",false);
		getpic();
	}
}
//變更卡片產品說明
function discardimg(){
	var CARDNAME = $("#CARDNAME").val();
	
	var isImg = false;
	for(var i=0;i<displaycardimg.length;i++){
		if(CARDNAME == displaycardimg[i]){//當符合所選擇的卡時
			$("#creditPIC").attr("src","${__ctx}/img/creditcard/" + cardimgnum[i]);
			$("#creditPIC").css("width","220px");
			$("#creditPIC").css("height","120px");
			$("#creditPIC").css("cursor","pointer");
			$("#creditPIC").attr("onclick","javascript:window.open('" + imgUrl[i] + "')");
			$("#more").attr("onclick","javascript:window.open('" + imgUrl[i] + "')");
			isImg = true;
			if('${transfer}' == 'en'){
				$("#displaymsg").html(cardmsgen[i]);//顯示產品說明文字
			}
			else if('${transfer}' == 'zh'){
				$("#displaymsg").html(cardmsgch[i]);//顯示產品說明文字
			}
			else{
				$("#displaymsg").html(cardmsg[i]);//顯示產品說明文字
			}
			
			break;
		}		
	}
	
	if(isImg == false){
// 		$("#creditPIC").attr("src","${__ctx}/img/MissingPic.gif");
		$("#creditPIC").attr("onclick","");
// 		$("#creditPIC").css("width","220px");
// 		$("#creditPIC").css("height","120px");
	}
	
	//判斷獅友卡是否顯示編號輸入欄位
	var QR = '${result_data.data.requestParam.QRCODE}';
// 	if(QR == "L"){
		if(CARDNAME == "85"){
			$("#MemberID").show();
		}
		else{
			$("#MemberID").hide();
		}
// 	}
}
//變更卡片圖示
function getpic(){
	var CARDNAME = $("#CARDNAME option:last").val();
	var defaultVal = ['銀色之愛信用卡', 'Master鈦金商旅卡', '藝FUN悠遊御璽卡', '北港朝天宮認同卡', '<spring:message code= "LB.B0030" />', '<spring:message code= "LB.B0029" />', '宜蘭悠遊認同卡', 
		'VISA信用卡', 'MasterCard信用卡', '信保基金111關懷認同卡','獅子會公益卡'];

	$("#card_name").text(defaultVal[cardgroup-1]);
	
	for(var i=0;i<displaycardimg.length;i++){
		if(CARDNAME == displaycardimg[i]){//當符合所選擇的卡時
			$("#creditPIC").attr("src","${__ctx}/img/creditcard/" + cardimgnum[i]);
			$("#creditPIC").css("width","220px");
			$("#creditPIC").css("height","120px");
			$("#creditPIC").css("cursor","pointer");
			$("#displaymsg").empty();
			break;
		}		
	}
}
//取得卡片下拉選單資料
function selectCard(){
	console.log("selectCard...");
	//修改 Reflected XSS All Clients
	var QRCODE = "<c:out value='${fn:escapeXml(result_data.data.requestParam.QRCODE)}' />";
	
	if(QRCODE == 'Y' || QRCODE == 'L' || '040' == urlBRANCH){
		var data = '${result_data.data.normalSilverCardList}';
// 		data = JSON.stringify(data);
		var dataobj = JSON.parse(data);
		dataobj.forEach(function(dataMap){
			if(cardgroup == dataMap.GPODER){
				$("#CARDNAME").append( $("<option></option>").attr("value", dataMap.CARDNO).text(dataMap.CARDNAME));
			}
		});
		getpic();
	}else if(QRCODE == 'Y' || QRCODE == 'L' && '040' != urlBRANCH){
		var data = '${result_data.data.normalSilverCardList}';
// 		data = JSON.stringify(data);
		var dataobj = JSON.parse(data);
		dataobj.forEach(function(dataMap){
			if(cardgroup == dataMap.GPODER){
				$("#CARDNAME").append( $("<option></option>").attr("value", dataMap.CARDNO).text(dataMap.CARDNAME));
			}
		});
		getpic();
	}else if (QRCODE != 'Y' && QRCODE != 'L' && '040' != urlBRANCH){
		var data = '${result_data.data.otherCardList}';
// 		data = JSON.stringify(data);
		var dataobj = JSON.parse(data);
		dataobj.forEach(function(dataMap){
			if(dataMap.Ncard!=null){
				var test = dataMap.Ncard;
				var arr = new Array();
				arr = test.split(",");
				for(var i = 0;i<arr.length;i++){
					$('#card_'+arr[i]).attr("style","display:none");
				}
			}
			if(cardgroup == dataMap.GPODER){
				$("#CARDNAME").append( $("<option></option>").attr("value", dataMap.CARDNO).text(dataMap.CARDNAME));
			}
		});
		getpic();
	}
	
}
//按鈕點選時變更資料
function changeCard(id){
	$("input[name^='card_']").removeClass('active');
// 	$("input[name^='card_']").addClass('btn-flat-gray');
// 	$('#'+id).removeClass('btn-flat-gray');
	$('#'+id).addClass('active');
	cardgroup = id.replace("card_","");
	$("#CARDNAME").empty();
	$("#displaymsg").empty();
	$("#card_name").empty();
	$("#more").attr("onclick","");
	$("#addValue").hide();
// 	$("#creditPIC").attr("src","${__ctx}/img/MissingPic.gif");
	$("#creditPIC").attr("onclick","");
	$("#CARDNAME").append( $("<option></option>").attr("value", "#").text("<spring:message code='LB.X1977' />"));
	selectCard();
	if(cardgroup == '11'){
		$("#liondata").show();
	}
	else{
		$("#liondata").hide();
	}
}
//切換卡別
function tabEvent(){
	$("#nav-trans-now").click(function(){
		cardchk();
		changeCard("card_"+cardgroup);
		$("#card-tab-1").show();
		$("#card-tab-2").hide();
	});
	$("#nav-trans-future").click(function(){
		cardchk2();
		changeCard("card_"+cardgroup2);
		$("#card-tab-2").show();
		$("#card-tab-1").hide();
	});
}
//判斷是否由QRCODE進入
function getQR(){
	var QR = '${result_data.data.requestParam.QRCODE}';
	if(QR == 'Y'){
		$("#nav-trans-now").click();
		changeCard("card_01");
		$("#card-tab-1").show();
		$("#card-tab-2").hide();
		$("#nav-trans-future").attr("style","display:none");
		$("#card_03").attr("style","display:none");
		$("#card_05").attr("style","display:none");
		$("#card_06").attr("style","display:none");
		$("#card_08").attr("style","display:none");
		
	}
	else if(QR == 'L'){
		$("#nav-trans-future").click();
		changeCard("card_11");
		$("#card-tab-2").show();
		$("#card-tab-1").hide();
		$("#nav-trans-now").attr("style","display:none");
		$("#card_02").attr("style","display:none");
		$("#card_04").attr("style","display:none");
		$("#card_07").attr("style","display:none");
		$("#card_09").attr("style","display:none");
		$("#card_10").attr("style","display:none");
		
		$("#liondata").show();
	}
}
//變更一般卡預設按鈕
function cardchk(){
	//若按鈕不等於none將第一個按鈕設為預設
	for(var i=0;i<cardgroups.length;i++){
		if($("#card_"+cardgroups[i]).css("display")!='none'){
			cardgroup = cardgroups[i];
			break;
		}
	}
}
//變更聯名卡預設按鈕
function cardchk2(){
	//若按鈕不等於none將第一個按鈕設為預設
	for(var i=0;i<cardgroups2.length;i++){
		if($("#card_"+cardgroups2[i]).css("display")!='none'){
			cardgroup2 = cardgroups2[i];
			break;
		}
	}
}

function changeLion(data){
	if(data == '  '){
		//20201130 新增分區選取'其他'，分會資料放'無'
		$("#branch").empty();
		$("#branch").append( $("<option></option>").attr("value", "#").text('<spring:message code="LB.Select" />'));
// 		$("#branch").append( $("<option></option>").attr("value", '無').text('<spring:message code="LB.D1070_2" />'));
		$("#branch").append( $("<option></option>").attr("value", '愛的協會').text('愛的協會'));
	}
	else{
		var URI = "${__ctx}/CREDIT/APPLY/getLionBranchAjax";
		var rqData = {ZONE:data};
		fstop.getServerDataEx(URI,rqData,false,changeLionFinish);
	}
}

function changeLionFinish(data){
	if(data){
		console.log("data.json: " + JSON.stringify(data));
		$("#branch").empty();

		
		$("#branch").append( $("<option></option>").attr("value", "#").text('<spring:message code="LB.Select" />'));
		data.forEach(function(map) {
			console.log(map);
			$("#branch").append( $("<option></option>").attr("value", map.TEAM).text(map.TEAM));
		});
	}
	else{
		errorBlock(
				null, 
				null,
				["<spring:message code='LB.X2368' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
			);
	}
}
</script>
</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 申請信用卡     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0666" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
<%-- 		<%@ include file="../index/menu.jsp"%> --%>
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!--線上申請信用卡 -->
				<h2><spring:message code="LB.D0022" /></h2>
				<div id="step-bar">
                    <ul>
                    	
                        <li class="active">信用卡選擇</li><!-- 信用卡選擇 -->
                        <c:if test="${result_data.data.passAuth !='Y'}">
                        	<li class="">身份驗證與權益</li><!-- 身份驗證與權益 -->
                        </c:if>
                        <li class=""><spring:message code="LB.X1967" /></li><!-- 申請資料 -->
                        <li class=""><spring:message code="LB.Confirm_data" /></li><!-- 確認資料 -->
                        <li class=""><spring:message code="LB.X1968" /></li><!-- 完成申請 -->
                    </ul>
                </div>
                <c:if test="${result_data.data.passAuth =='Y'}">
                	<form method="post" id="formId" action="${__ctx}/CREDIT/APPLY/apply_creditcard_terms">
                </c:if>
                <c:if test="${result_data.data.passAuth !='Y'}">
                	<form method="post" id="formId" action="${__ctx}/CREDIT/APPLY/creditcard_apply_authentication">
                </c:if>
                	<input type="hidden" id="smartcard" name="smartcard"/>
					<input type="hidden" name="VARSTR2" value="1"/>
					<input type="hidden" id="CN" name="CN"/>
					<input type="hidden" name="BRANCH" value="${result_data.data.requestParam.BRANCH}"/>
					<input type="hidden" name="QRCODE" value="${result_data.data.requestParam.QRCODE}"/>
					<input type="hidden" id="TOKEN" name="TOKEN" value="${sessionScope.transfer_confirm_token}"/>
					<input type="hidden" id="passAuth" name="passAuth" value="${result_data.data.passAuth}"/>
					<!-- 表單顯示區  -->
                    <div class="main-content-block row pb-0">
                        <div class="col-12 terms-block">
	                        <div class="ttb-message">
	                            <p>信用卡選擇</p><!-- 信用卡選擇 -->
	                        </div>
	                        <c:if test="${result_data.data.oldcardowner == 'Y'}">
	                        	<p class="form-description"><spring:message code="LB.X1973" />：<spring:message code="LB.X1974" /></p>
	                        </c:if>
	                        <c:if test="${ result_data.data.hasThisCard == 'Y' && result_data.data.doubleApply !='Y'}">
	                        	<p class="form-description"> 您選擇的卡片您已持有，請重新選擇您欲申請的卡片，謝謝。 </p>
	                        </c:if>
	                        
	                        <c:if test="${result_data.data.doubleApply =='Y'}">
	                        	<p class="form-description"> 您選擇的卡片已申請中，請重新選擇您欲申請的卡片，謝謝。</p>
	                        </c:if>
	                        
                        	<p class="form-description"><spring:message code="LB.X1976" /></p><!-- 請選擇您欲申請的信用卡 -->
                        	
                        	<div class="card-block shadow-box">
	                            <nav class="w-100">
	                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
	                                    <a class="nav-item nav-link active" id="nav-trans-now" data-toggle="tab" href="#nav-trans-now" role="tab" aria-selected="true"><spring:message code="LB.X2063" /></a><!-- 一般銀行卡 -->
	                                    <a class="nav-item nav-link" id="nav-trans-future" data-toggle="tab" href="#nav-trans-future" role="tab" aria-selected="false"><spring:message code="LB.X2062" /></a><!-- 聯名認同卡 -->
	                                </div>
	                            </nav>
                        		<div class="col-12 tab-content" id="nav-tabContent">
                                	<div class="ttb-input-block tab-pane fade" id="nav-trans-now-tab" role="tabpanel" aria-labelledby="nav-profile-tab">
                                	</div>
                                	<div class="ttb-input-block tab-pane fade show active" id="nav-trans-future-tab" role="tabpanel" aria-labelledby="nav-home-tab">
	                                    <div class="card-select-block" id="card-tab-1">
	                                    <c:choose>
	                                    <c:when test="${result_data.data.requestParam.BRANCH != '040'}">
	                                    	 
	                                    	
	                                    	 <!-- 永續生活一卡通 -->
	                                    	<input type="button" class="ttb-sm-btn" name="card_06" id="card_06" value="<spring:message code= 'LB.B0029' />" onclick="changeCard(this.id)">
	                                    	<input type="button" class="ttb-sm-btn" name="card_05" id="card_05" value="<spring:message code= 'LB.B0030' />" onclick="changeCard(this.id)">
	                                    	<input type="button" class="ttb-sm-btn" name="card_01" id="card_01" value="銀色之愛信用卡" onclick="changeCard(this.id)">
	                                    	<input type="button" class="ttb-sm-btn" name="card_03" id="card_03" value="藝FUN悠遊御璽卡" onclick="changeCard(this.id)">
	                                        <input type="button" class="ttb-sm-btn" name="card_08" id="card_08" value="VISA信用卡" onclick="changeCard(this.id)">
	                                    </c:when>
	                                    <c:when test="${'040' == result_data.data.requestParam.BRANCH}">
	                                    	<input type="button" class="ttb-sm-btn" name="card_05" id="card_05" value="<spring:message code= 'LB.B0030' />" onclick="changeCard(this.id)">
	                                    </c:when>
	                                    </c:choose>
	                                      
	                                    </div>
	                                    <div class="card-select-block" id="card-tab-2">
	                                        <input type="button" class="ttb-sm-btn active" name="card_04" id="card_04" value="北港朝天宮認同卡" onclick="changeCard(this.id)">
	                                    	<input type="button" class="ttb-sm-btn" name="card_07" id="card_07" value="宜蘭悠遊認同卡" onclick="changeCard(this.id)">
	                                        <input type="button" class="ttb-sm-btn" name="card_10" id="card_10" value="信保基金111關懷認同卡" onclick="changeCard(this.id)">
	                                        <input type="button" class="ttb-sm-btn" name="card_11" id="card_11" value="獅子會公益卡" onclick="changeCard(this.id)">
	                                    </div>
	                                	<img id="creditPIC"  width="300px" height="225px" style="width: 220px; height: 120px; min-width: 220px; min-height: 120px; cursor: pointer;">
		                                <div class="card-detail-block d-flex">
		                                    <div class="d-inline-block ml-4">
		                                    	<p class="card-detail-title"><font id="card_name"></font></p>
		                                    	<span id="displaymsg"></span>
		                                       	<div id="addValue"  style="display:none">
													<font size="2" color="red"><spring:message code="LB.B0028" /></font><!-- 永續生活一卡通已預設自動加值功能開啟 -->
												</div>
		                                        <div class="ttb-input-item">
		                                        	<select class="custom-select select-input" name="CARDNAME" id="CARDNAME" onchange="change();discardimg()">
		                                            	<option value="#"><spring:message code="LB.X1977" /></option><!-- 請選擇卡別 -->
		                                           	</select>
		                                            <input type="button" class="ttb-sm-btn btn-flat-gray ml-4" name="more" id="more" value="<spring:message code='LB.X1978' />" onclick="">
		                                      	</div>
		                                 	</div>
		                            	</div>
			                           	<div id="liondata" style="display:none">
					                        <div class="ttb-input-item row">
				                        		<div class="ttb-input" style="margin-right: 1%;">
													<!-- 分區 -->
				                        			<span><spring:message code='LB.X2364' /></span>
				                        			<select class="custom-select select-input validate[funcCall[validate_CheckSelect[<spring:message code='LB.X2364' />,partition,#]]]" name="partition" id="partition" onchange="changeLion(this.value)">
				                        				<option value="#"><spring:message code="LB.Select" /></option>
				                        				<c:forEach var="row" items="${result_data.data.lionList}" >
				                        					<option value="${row.ZONE}">${row.ZONE}</option>
				                        				</c:forEach>
				                        				<!-- 20201130新增'其他'選項 ，value帶兩個半型空白-->
				                        				<option value="  "><spring:message code="LB.D0572" /></option>
			                                    	</select>
			                                    </div>
			                                   	<div class="ttb-input" style="margin-right: 1%;">
			                                    	<!-- 分會 -->
			                                    	<span><spring:message code='LB.X2365' /></span>
			                                    	<select class="custom-select select-input validate[funcCall[validate_CheckSelect[<spring:message code='LB.X2365' />,branch,#]]]" name="branch" id="branch">
				                        				<option value="#"><spring:message code="LB.Select" /></option>
			                                    	</select>
			                                    </div>
			                                    <div class="ttb-input">
			                                    	<!-- 會員編號 -->
			                                    	<span id="MemberID" style="display:none">
				                                    	<span><spring:message code='LB.X2366' /></span>
				                                    	<input type="text" class="text-input validate[funcCall[validate_CheckNumber[<spring:message code='LB.X2366' />,memberId]]]" name="memberId" id="memberId" placeholder="<spring:message code='LB.X2369' />" maxlength="8"/>
					                        			<span class="input-remarks"><spring:message code='LB.X2367' /></span><!-- 未填寫會員編號者卡面上將無法印製。 -->
					                        		</span>
				                        		</div>
					                        </div>
										</div>
                                	</div>
                                	<!-- 取消 -->
                                	<input type="button" name="CMBACK" id="CMBACK" value="<spring:message code="LB.X0194" />" class="ttb-button btn-flat-gray" onclick="window.close();"/>
	                            	<!-- 下一步 -->
		                            <input type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.X0080" />" id="CMSUBMIT" name="CMSUBMIT">
                            	</div>
                        	</div>
                        </div>
                 	</div>
					<ol class="description-list list-decimal">
						<p>【<spring:message code="LB.X1900" />】 ：</p>
						<li>
<!-- 							申請資格 -->
							<spring:message code="LB.X1901" />
							<ul class="text-left">
								<li>● <spring:message code="LB.X1902" /></li>
							</ul>
						</li>
						<li>
							<spring:message code="LB.X1903" />
							<ul class="text-left">
								<li>● <spring:message code="LB.X1904" /></li>
								<li>
									● <spring:message code="LB.X1905" />
									<ul class="text-left">
										<li>．<spring:message code="LB.X1906" /></li>
										<li>．<spring:message code="LB.X1907" /></li>
										<li>．<spring:message code="LB.X1908" /></li>
										<li>．<spring:message code="LB.X1909" /></li>
									</ul>
								</li>
							</ul>
						</li>
						<li type="none"><spring:message code="LB.X1910" /></li>
					</ol>
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>