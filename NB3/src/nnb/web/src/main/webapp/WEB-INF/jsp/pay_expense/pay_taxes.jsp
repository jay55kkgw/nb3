<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<!-- 讀卡機所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/cardReaderMethod.js"></script>
	
	<script type="text/JavaScript">
		// 遮罩用
		var blockId;
		
	    $(document).ready(function() {
	    	// HTML載入完成後開始遮罩
			initBlockUI();
			// 開始查詢資料並完成畫面
			setTimeout("init()", 100);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});

	 	// 初始化
	    function init(){
	    	// 即時、預約標籤 click
	    	tabEvent();
	    	// 建立約定轉出帳號下拉選單
	    	creatOutAcn();
	    	// 轉出帳號 change
	    	acnoEvent();
	    	// 預約自動輸入明天
	    	getTmrDate();
	    	// 預約日期 change
	    	fgtxdateEvent();
	    	// 西元年日期輸入欄位 click
	    	datetimepickerEvent();
	    	// 通訊錄 click
			addressbookClickEvent();
	    	
			// 回上一頁填入資料
			refillData();

			// 初始化後隱藏span( 表單驗證提示訊息用的 )
			$(".hideblock").hide();
			// 表單驗證
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
			// submit觸發事件
			finish();
	    }
	 	
	 	// 確認按鈕點擊觸發submit
		function finish(){
			$("#CMSUBMIT").click(function (e) {
				// 去掉舊的錯誤提示訊息
				$(".formError").remove();
				// 顯示驗證隱藏欄位( 為了讓提示訊息對齊 )
				$(".hideblock").show();
				
				console.log("submit~~");
				
				// 塞值進隱藏span內的input
				$("#AMOUNT").val("");
				$("#CMDATE").val($("#CMDATE_TMP").val());
				$("#PAYDUED").val($("#PAYDUED_TMP").val());
				$("#AMOUNT").val($("#AMOUNT_TMP").val());
				// 塞值進隱藏span內的input--約定/非約定 之轉出帳號
				if( $('input[name=TransferType]:checked').val() == 'PD'){
					// 轉出帳號為約定
					$("#OUTACN").val($("#OUTACN_PD").val());
				}else if( $('input[name=TransferType]:checked').val() == 'NPD'){
					// 轉出帳號為非約定
					$("#OUTACN").val($("#OUTACN_NPD").val());
				}

			   	// 表單驗證--即時/預約
			   	var fgtxdate = $('input[name="FGTXDATE"]:checked').val();
			   	console.log('fgtxdate: ' + fgtxdate);
			   	
				var sMinDate = '${str_SystemDate}';
				var sMaxDate = '${sNextYearDay}';
				console.log("processQuery.sMinDate: " + sMinDate);
				console.log("processQuery.sMaxDate: " + sMaxDate);
				
				if (fgtxdate == '2'){
					// 驗證是否是可預約單日之日期
					$("#CMDATE").addClass("validate[required ,funcCall[validate_CheckDate[<spring:message code= "LB.X0377" />,CMDATE,"+sMinDate+","+sMaxDate+"]]]");
			   	} else {
			   		// 取消驗證是否是可預約單日之日期
			   		$("#CMDATE").removeClass("validate[required ,funcCall[validate_CheckDate[<spring:message code= "LB.X0377" />,CMDATE,"+sMinDate+","+sMaxDate+"]]]");
				}

				// 表單驗證--轉出帳號
				var transferType = $('input[name="TransferType"]:checked').val();
				console.log('transferType: ' + transferType);
				if(transferType == 'PD') {
					$("#OUTACN_PD").addClass("validate[required, funcCall[validate_CheckSelect['<spring:message code= "LB.Payers_account_no" />',OUTACN_PD,'']]]");
					$("#OUTACN_NPD").removeClass("validate[required, funcCall[validate_CheckSelect['<spring:message code= "LB.Payers_account_no" />',OUTACN_NPD,'']]]");
				} else if(transferType == 'NPD') {
					$("#OUTACN_PD").removeClass("validate[required, funcCall[validate_CheckSelect['<spring:message code= "LB.Payers_account_no" />',OUTACN_PD,'']]]");
					$("#OUTACN_NPD").addClass("validate[required, funcCall[validate_CheckSelect['<spring:message code= "LB.Payers_account_no" />',OUTACN_NPD,'']]]");
				}

				// 繳稅類別判斷式
				taxtypeCheck();

				// 表單驗證--轉出帳號
				var sType2 = $("#TAXTYPE2").val().substring(0, 2);
				var str_Today = EyearToCyear('${str_Today}');
				console.log("processQuery.str_Today: " + str_Today);
				$("#PAYDUED").val(EyearToCyear($("#PAYDUED").val()));
				console.log("processQuery.PAYDUED: " + $("#PAYDUED").val());

				switch(sType2){
					case "11":
						$("#INTSACN3").addClass("validate[required]");
						$("#PAYDUED").addClass("validate[required, funcCall[validate_CheckCDate['<spring:message code= "LB.W0409" />',PAYDUED," + str_Today + "]]]");
						$("#INTSACN1").removeClass("validate[required, funcCall[validate_CheckNumWithDigit['<spring:message code= "LB.W0410" />',INTSACN1,3]]]");
						$("#INTSACN2").removeClass("validate[required]");
						$("#INTSACN2").removeClass("validate[required, funcCall[validate_checkSYS_IDNO[INTSACN2]]]");
						break;
						
					case "15":
						$("#INTSACN1").addClass("validate[required, funcCall[validate_CheckNumWithDigit['<spring:message code= "LB.W0410" />',INTSACN1,3]]]");
						$("#INTSACN2").addClass("validate[required]");
						$("#INTSACN2").addClass("validate[required, funcCall[validate_checkSYS_IDNO[INTSACN2]]]");
						$("#INTSACN3").removeClass("validate[required]");
						$("#PAYDUED").removeClass("validate[required, funcCall[validate_CheckCDate['<spring:message code= "LB.W0409" />',PAYDUED," + str_Today + "]]]");
						break;
						
					default :
						$("#INTSACN3").removeClass("validate[required]");
						$("#PAYDUED").removeClass("validate[required, funcCall[validate_CheckCDate['<spring:message code= "LB.W0409" />',PAYDUED," + str_Today + "]]]");
						$("#INTSACN1").removeClass("validate[required, funcCall[validate_CheckNumWithDigit['<spring:message code= "LB.W0410" />',INTSACN1,3]]]");
						$("#INTSACN2").removeClass("validate[required]");
						$("#INTSACN2").removeClass("validate[required, funcCall[validate_checkSYS_IDNO[INTSACN2]]]");
				}
				
				// 表單驗證
				if ( !$('#formId').validationEngine('validate') ) {
					e = e || window.event; // for IE
					e.preventDefault();
					
				} else {
					// 資料檢核，過了則送出表單
					processQuery();
				}
			});
	 	}

		// 即時、預約標籤切換事件
		function tabEvent(){
			// 即時
			$("#nav-trans-now").click(function() {
				$("#transfer-date").hide();
				fstop.setRadioChecked("FGTXDATE" , "1" ,true);
			})
			// 預約
			$("#nav-trans-future").click(function() {
				$("#transfer-date").show();
				fstop.setRadioChecked("FGTXDATE" , "2" ,true);
			})
		}

		// 建立約定轉出帳號下拉選單
		function creatOutAcn(){
			var options = { keyisval:false ,selectID:'#OUTACN_PD'};
			
			uri = '${__ctx}'+"/PAY/EXPENSE/getOutAcn_aj"
			rdata = {type: 'acno' };
			console.log("creatOutAcn.uri: " + uri);
			console.log("creatOutAcn.rdata: " + rdata);
			
			data = fstop.getServerDataEx(uri,rdata,false);

			console.log("creatOutAcn.data: " + JSON.stringify(data));
			
			if(data !=null && data.result == true ){
				fstop.creatSelect( data.data, options);
			}
		}
		
		// 轉出帳號change事件，要秀出選擇的轉出帳號可用餘額
		function acnoEvent(){
			$('input[type=radio][name=TransferType]').change(function() {
				if (this.value == 'PD') { //約定
					$("#OUTACN_NPD").val("");
				} else if (this.value == 'NPD') { //非約
					$("#OUTACN_PD").val("");
					$("#acnoIsShow").hide();
				}
			});
			$("#OUTACN_PD").change(function() {
				var acno = $('#OUTACN_PD :selected').val();
				console.log("acnoEvent.acno: " + acno);
				getACNO_Data(acno);
				$('#TransferType_01').click();
			});
		}
		// 取得轉出帳號餘額資料
		function getACNO_Data(acno){
			// 變更轉出帳號就隱藏重置再重新顯示
			$("#acnoIsShow").hide();
			$("#showText").html('');
			
			var options = { keyisval:true ,selectID:'#OUTACN'};
			
			uri = '${__ctx}'+"/NT/ACCT/TRANSFER/getACNO_Data_aj"
			rdata = {acno: acno};
			console.log("getACNO_Data.uri: " + uri);
			console.log("getACNO_Data.rdata: " + rdata);
			
			fstop.getServerDataEx(uri,rdata,false,isShowACNO_Data);
		}
		// 顯示轉出帳號餘額
		function isShowACNO_Data(data){
			var i18n = new Object();
			i18n['available_balance'] = '<spring:message code="LB.Available_balance" />: ';
			
			console.log("isShowACNO_Data.data: " + JSON.stringify(data));
			
			if(data && data.result){
				// 可用餘額
				console.log("data.data.accno_data: " + data.data.accno_data.ADPIBAL);
				// 格式化金額欄位
				i18n['available_balance'] += fstop.formatAmt(data.data.accno_data.ADPIBAL);
				$("#showText").html(i18n['available_balance']);
				$("#acnoIsShow").show();
			}else{
				$("#acnoIsShow").hide();
			}
		}

		// 預約自動輸入明天
		function getTmrDate() {
			// 預約日期欄位顯示明天
			$('#CMDATE_TMP').val("${tmrDate}");
		}
		// 預約日期change事件 ，檢核日期邏輯
		function fgtxdateEvent(){
			 $('input[type=radio][name=FGTXDATE]').change(function() {
		        if (this.value == '2') { // 預約
		            console.log("tomorrow");
		            $("#CMDATE").addClass("validate[required]");
		        }else if (this.value == '1') { // 即時
		            $("#CMDATE").removeClass("validate[required]");
		        }
		    });
		}

		// 西元小日曆click
		function datetimepickerEvent(){
		    $(".CMDATE").click(function(event) {
				$('#CMDATE_TMP').datetimepicker('show');
			});
		    $(".PAYDUED").click(function(event) {
				$('#PAYDUED_TMP').datetimepicker('show');
			});
			jQuery('.datetimepicker').datetimepicker({
				timepicker:false,
				closeOnDateSelect : true,
				scrollMonth : false,
				scrollInput : false,
			 	format:'Y/m/d',
			 	lang: '${transfer}'
			});
		}

		// 通訊錄click
		function addressbookClickEvent() {
			// 通訊錄btn
			$('#getAddressbook').click(function() {
				openAddressbook();
			});
			// 同交易備註btn
			$('#CMMAILMEMO_btn').click(function() {
				$('#CMMAILMEMO').val( $('#CMTRMEMO').val() );
			});
		}
		// 開啟通訊錄email選單
		function openAddressbook() {
			window.open('${__ctx}/NT/ACCT/TRANSFER/showAddressbook');
		}

		// 繳稅類別判斷式
		function taxtypeCheck() {
			var main = document.getElementById("formId");
			
			if(document.getElementById("TAXTYPE211").value!='#'){
				main.TAXTYPE2.value=document.getElementById("TAXTYPE211").value;
				oTAXTYPE2 = document.getElementById("TAXTYPE211");
				main.TaxTypeText.value = oTAXTYPE2.options[oTAXTYPE2.selectedIndex].text;

			}else if(document.getElementById("TAXTYPE212").value !='#'){
				main.TAXTYPE2.value=document.getElementById("TAXTYPE212").value;
				oTAXTYPE2 = document.getElementById("TAXTYPE212");
				main.TaxTypeText.value = oTAXTYPE2.options[oTAXTYPE2.selectedIndex].text;

			}else if(document.getElementById("TAXTYPE213").value!='#'){
				main.TAXTYPE2.value=document.getElementById("TAXTYPE213").value;
				oTAXTYPE2 = document.getElementById("TAXTYPE213");
				main.TaxTypeText.value = oTAXTYPE2.options[oTAXTYPE2.selectedIndex].text;

			}else if(document.getElementById("TAXTYPE214").value!='#'){
				main.TAXTYPE2.value=document.getElementById("TAXTYPE214").value;
				oTAXTYPE2 = document.getElementById("TAXTYPE214");
				main.TaxTypeText.value = oTAXTYPE2.options[oTAXTYPE2.selectedIndex].text;

			}else if(document.getElementById("TAXTYPE215").value!='#'){
				main.TAXTYPE2.value=document.getElementById("TAXTYPE215").value;
				oTAXTYPE2 = document.getElementById("TAXTYPE215");
				main.TaxTypeText.value = oTAXTYPE2.options[oTAXTYPE2.selectedIndex].text;

			}else if(document.getElementById("TAXTYPE216").value!='#'){
				main.TAXTYPE2.value=document.getElementById("TAXTYPE216").value;
				oTAXTYPE2 = document.getElementById("TAXTYPE216");
				main.TaxTypeText.value = oTAXTYPE2.options[oTAXTYPE2.selectedIndex].text;	
			}
		}
		

		function processQuery(){
			var main = document.getElementById("formId");

			// 隱藏欄位驗證之錯誤訊息不適合於表單顯示故用alert
			if (!CheckSelect("<spring:message code= "LB.X1423" />", main.TAXTYPE1, "#")) return false;
			if (!CheckSelect("<spring:message code= "LB.X0469" />", main.TAXTYPE2.value, "#")) return false;

			// 繳稅類別文字
			oTAXTYPE2 = main.TAXTYPE2.value;
			main.TAXTYPE1.value = oTAXTYPE2.substring(0, 2);
			
			// 隱藏表單按鈕
			$("#CMTRMAIL").hide();
			$("#CMSUBMIT").hide();
			$("#CMRESET").hide();
			$("#CMMAILMEMO_btn").hide();

			// 通過資料檢核解除表單驗證
			$("#formId").validationEngine('detach');
			
			// 畫面遮罩
			initBlockUI();
			// 送交
			$("#formId").submit();
			
		}
		
		// 變更細稅名稱選單
		function ChangeTaxType(oSelect){
			var sSelectedType = oSelect.options[oSelect.selectedIndex].value;
			console.log("ChangeTaxType.sSelectedType2_before: " + sSelectedType);
			
			if (sSelectedType.length > 2){
				sSelectedType = sSelectedType.substring(0, 2);
			}
			console.log("ChangeTaxType.sSelectedType_after: " + sSelectedType);
			
			switch (sSelectedType){
				case "#":
					$("#TYPE11_1").hide();
					$("#TYPE11_2").hide();
					$("#TYPE15_1").hide();
					$("#TYPE15_2").hide();
					break;
				case "11":
					$("#TYPE11_1").show();
					$("#TYPE11_2").show();
					$("#TYPE15_1").hide();
					$("#TYPE15_2").hide();
					break;
				case "15":
					$("#TYPE11_1").hide();
					$("#TYPE11_2").hide();
					$("#TYPE15_1").show();
					$("#TYPE15_2").show();
					break;
			}
		}

		// 變更稅別選單
		function ChangeTaxType2(oSelect){
			var sSelectedType2 = oSelect.options[oSelect.selectedIndex].value;
			console.log("ChangeTaxType2.sSelectedType2_before: " + sSelectedType2);
			
			//document.getElementById("TAXTYPE211").value='#';
			$("#TAXTYPE212").val('#');
			$("#TAXTYPE213").val('#');
			$("#TAXTYPE214").val('#');
			$("#TAXTYPE215").val('#');
			$("#TAXTYPE216").val('#');
			
			if (sSelectedType2.length > 2){
				sSelectedType2 = sSelectedType2.substring(0, 1);
			}
			console.log("ChangeTaxType2.sSelectedType2_after: " + sSelectedType2);
			
			switch (sSelectedType2){
				case "#":
					$("#TTYPE111").hide();
					$("#TTYPE112").hide();
					$("#TTYPE113").hide();
					$("#TTYPE114").hide();
					$("#TTYPE115").hide();
					$("#TTYPE116").hide();
					break;
				case "A":
					$("#TTYPE111").show();
					$("#TTYPE112").hide();
					$("#TTYPE113").hide();
					$("#TTYPE114").hide();
					$("#TTYPE115").hide();
					$("#TTYPE116").hide();
					break;
				case "B":
					$("#TTYPE111").hide();
					$("#TTYPE112").show();
					$("#TTYPE113").hide();
					$("#TTYPE114").hide();
					$("#TTYPE115").hide();
					$("#TTYPE116").hide();		
					break;
				case "C":
					$("#TTYPE111").hide();
					$("#TTYPE112").hide();
					$("#TTYPE113").show();
					$("#TTYPE114").hide();
					$("#TTYPE115").hide();
					$("#TTYPE116").hide();		
					break;
				case "D":
					$("#TTYPE111").hide();
					$("#TTYPE112").hide();
					$("#TTYPE113").hide();
					$("#TTYPE114").show();
					$("#TTYPE115").hide();
					$("#TTYPE116").hide();	
					break;	
				case "E":
					$("#TTYPE111").hide();
					$("#TTYPE112").hide();
					$("#TTYPE113").hide();
					$("#TTYPE114").hide();
					$("#TTYPE115").show();
					$("#TTYPE116").hide();		
					break;	
				case "F":
					$("#TTYPE111").hide();
					$("#TTYPE112").hide();
					$("#TTYPE113").hide();
					$("#TTYPE114").hide();
					$("#TTYPE115").hide();
					$("#TTYPE116").show();	
					break;	
				case "G":
					$("#TTYPE111").hide();
					$("#TTYPE112").hide();
					$("#TTYPE113").hide();
					$("#TTYPE114").hide();
					$("#TTYPE115").hide();
					$("#TTYPE116").hide();		
					break;				
			}
		}
		
		// 表單重置
		function formReset() {
			$("#acnoIsShow").hide();
			$(".formError").remove();
			$('form').find('*').filter(':input:visible:first').focus();
			$('#formId').trigger("reset");
		}
		
		// 回上一頁填入資料
		function refillData() {
			var isBack = '${isBack}';
			console.log('isBack: ' + isBack);
			var jsondata = '${previousdata}';
			console.log('jsondata: ' + jsondata);
			
			if ('Y' == isBack && jsondata != null) {
				JSON.parse(jsondata, function(key, value) {
					if(key) {
						var obj = $("input[name='"+key+"']");
						var type = obj.attr('type');
						
						console.log('--------------------------------');
						console.log('type: ' + type);
						console.log('key: ' + key);
						console.log('value: ' + value);
						console.log('--------------------------------');
						
						if(type == 'text'){
							obj.val(value);
						}
						
						if(type == 'hidden'){
							obj.val(value);
						}

						if(type == 'radio'){
							obj.filter('[value='+value+']').prop('checked', true);
						}
						
						if(type == 'checkbox'){
							obj.prop('checked', true);
						}
						
						// SELECT例外處理--約定轉出帳號
						if(key == 'OUTACN_PD'){
							$("#OUTACN_PD option").filter(function() {
								return $(this).val() == value;
							}).attr('selected', true);
						}
						
						// 即時或預約標籤切換
						if(key == 'FGTXDATE' && value == '2'){
							$("#nav-trans-future").click();
						}
					}
				});
				
				// TOKEN值要換一個，不可用上一次的
				$("#TOKEN").val('${sessionScope.transfer_confirm_token}');
			}
		}
	</script>
</head>
<body>
	<!-- 交易機制元件所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	
	
    <!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 繳稅     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0367" /></li>
		</ol>
	</nav>

	
	<!-- 功能清單及登入資訊 -->
	<div class="content row">
	
		<!-- 功能清單 -->
		<%@ include file="../index/menu.jsp"%>

		<!-- 快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
				<!-- 功能內容 -->
				<form method="post" id="formId" action="${__ctx}/PAY/EXPENSE/pay_taxes_confirm">
					<input type="hidden" id="TOKEN" name="TOKEN" value="${sessionScope.transfer_confirm_token}" /><!-- 防止重複交易 -->
					<input type="hidden" name="FLAG" value="1" /><!-- 約定/非約定 -->
					
					<input type="hidden" id="TAXTYPE1" name="TAXTYPE1" value="" /><!-- 分類 -->
					<input type="hidden" id="TaxTypeText" name="TaxTypeText" value="" /><!-- 繳稅類別文字 -->
					<input type="hidden" id="TAXTYPE2" name="TAXTYPE2" value="" /><!-- 繳稅類別 -->
					
					<!-- 功能名稱 -->
					<h2><spring:message code="LB.W0367" /></h2>
					<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
					
					<!-- 交易流程階段 -->
					<div id="step-bar">
						<ul>
							<li class="active"><spring:message code="LB.Enter_data" /></li>
							<li class=""><spring:message code="LB.Confirm_data" /></li>
							<li class=""><spring:message code="LB.Transaction_complete" /></li>
						</ul>
					</div>
					
					<!-- 功能內容 -->
					<div class="main-content-block row radius-50">
						<!-- 即時、預約導引標籤 -->
						<nav style="width: 100%;">
							<div class="nav nav-tabs" id="nav-tab" role="tablist">
								<a class="nav-item nav-link active" id="nav-trans-now" data-toggle="tab" href="#nav-trans-now" 
									role="tab" aria-controls="nav-home" aria-selected="false">
									<spring:message code="LB.Immediately" />
								</a> 
								<a class="nav-item nav-link" id="nav-trans-future" data-toggle="tab" href="#nav-trans-future" 
									role="tab" aria-controls="nav-profile" aria-selected="true" >
									<spring:message code="LB.Booking" />
								</a>
							</div>
						</nav>
						
						<!-- 交易流程階段 -->
						<div class="col-12 tab-content" id="nav-tabContent">
							<div class="tab-pane fade " id="nav-trans-future" role="tabpanel"
								aria-labelledby="nav-profile-tab"></div>

							<!-- 預約才顯示轉帳日期 -->
							<div class="ttb-input-block tab-pane fade show active"
								id="nav-trans-now" role="tabpanel" aria-labelledby="nav-home-tab">
								<div class="ttb-message">
									<span></span>
								</div>
								
								<!-- 即時的屬性(不須顯示預設隱藏) -->
								<div class="ttb-input" style="display: none;">
									<label class="radio-block">
										<spring:message code="LB.Immediately" />
										<input type="radio" name="FGTXDATE" value="1" checked /> 
										<span class="ttb-radio"></span>
									</label>
								</div>
								
								<!-- 轉帳日期 -->
								<div id="transfer-date" class="ttb-input-item row" style="display: none;">
									<span class="input-title">
										<label><h4><spring:message code="LB.Transfer_date" /></h4></label>
									</span>
									<span class="input-block">
										<!-- 預約的屬性(必須顯示) -->
										<div class="ttb-input ">
											<label class="radio-block">
												<spring:message code="LB.Booking" /> 
												<input type="radio" name="FGTXDATE" value="2" /> 
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!-- 預約日期 -->
										<div class="ttb-input">
											<input type="text" id="CMDATE_TMP" name="CMDATE_TMP" value="" size="10"
												maxlength="10" class="text-input datetimepicker" />
											<span class="input-unit CMDATE">
												<img src="${__ctx}/img/icon-7.svg" />
											</span>
											<!-- 為了讓提示訊息對齊用，會在初始化時先隱藏 -->
											<span class="hideblock">
												<!-- 驗證用的input -->
												<input id="CMDATE" name="CMDATE" type="text" class="text-input" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
									</span>
								</div>
								
								<!-- 轉出帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label><h4><spring:message code="LB.Payers_account_no" /></h4></label>
									</span>
									<span class="input-block">
										<!-- 約定 轉出帳號 -->
										<div class="ttb-input">
											<label class="radio-block"> 
												<input type="radio" id="TransferType_01" name="TransferType" value="PD" checked>
													<spring:message code="LB.Designated_account" />
												<span class="ttb-radio"></span>
											</label>
										</div> 
										<div class="ttb-input ">
											<select name="OUTACN_PD" id="OUTACN_PD" class="custom-select select-input half-input ">
												<option value="">----<spring:message code="LB.Select_account" />-----</option>
											</select>
											<!-- 約定 轉出帳號 餘額 -->
											<div id="acnoIsShow">
												<span id="showText" class="input-unit"></span>
											</div>
										</div>
										
										<!-- 非約定 轉出帳號 -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Out_nondesignated_account" /> 
												<input type="radio" id="TransferType_02" name="TransferType" value="NPD" onclick="listReaders();" />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<input type="text" name="OUTACN_NPD" id="OUTACN_NPD" class="text-input" readonly="readonly" value="" />
											<span class="input-subtitle subtitle-color">
												<spring:message code="LB.X2361" />
											</span>
											<!-- 為了讓提示訊息對齊用，會在初始化時先隱藏；只會顯示提示訊息不會顯示欄位 -->
											<span class="hideblock">
												<!-- 轉出帳號 -->
												<input id="OUTACN" name="OUTACN" type="text" class="text-input" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
										
									</span>
								</div>
								
								<!-- 稅別 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label><h4><spring:message code="LB.W0376" /></h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<select name="TAXTYPE22" id="TAXTYPE22" class="custom-select select-input half-input " onChange="ChangeTaxType2(this)">
								        		<option value="#" selected>---<spring:message code="LB.W0377" />---</option>
												<option value="A"><spring:message code="LB.W0380" /></option>
								        		<option value="B"><spring:message code="LB.W0392" /></option>
												<option value="C"><spring:message code="LB.W0393" /></option>
												<option value="D"><spring:message code="LB.W0395" /></option>
												<option value="E"><spring:message code="LB.W0397" /></option>
								        		<option value="F"><spring:message code="LB.W0401" /></option>
								        	</select>
										</div>
									</span>
								</div>

								<!-- 細稅名稱211 -->
								<!-- 依據稅別決定是否顯示 -->
								<div class="ttb-input-item row" id="TTYPE111" style="display:none">
									<!-- 細稅名稱 -->
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0378" /></h4>
										</label>
									</span>
									<span class="input-block">
										<select name="TAXTYPE211" id="TAXTYPE211" class="custom-select select-input half-input " onChange="ChangeTaxType(this)">
							        		<option value="#" selected>---<spring:message code="LB.W0379" />---</option>
							        		<option value="11221">201 <spring:message code="LB.W0381" /></option>
							        		<option value="11222">202 <spring:message code="LB.W0382" /></option>
							        		<option value="11223">203 <spring:message code="LB.W0383" /></option>
							        		<option value="11224">204 <spring:message code="LB.W0384" /></option>
							        		<option value="11226">206 <spring:message code="LB.W0385" /></option>
							        		<option value="11227">207 <spring:message code="LB.W0388" /></option>
											<option value="11228">208 <spring:message code="LB.W0386" /></option>
							        		<option value="11229">209 <spring:message code="LB.W0387" /></option>
							        		<option value="11230">20E <spring:message code="LB.W0385" /></option>
							        		<option value="11232">20K <spring:message code="LB.W0389" /></option>
							        		<option value="11235">20N <spring:message code="LB.W0390" /></option>
							        		<option value="11236">20P <spring:message code="LB.W0391" /></option>
							        	</select>
									</span>
								</div>

								<!-- 細稅名稱212 -->
								<!-- 依據稅別決定是否顯示 -->
								<div class="ttb-input-item row" id="TTYPE112" style="display:none">
									<!-- 細稅名稱 -->
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0378" /></h4>
										</label>
									</span>
									<span class="input-block">
										<select name="TAXTYPE212" id="TAXTYPE212" class="custom-select select-input half-input " onChange="ChangeTaxType(this)">
							        		<option value="#" selected>---<spring:message code="LB.W0379" />---</option>
							        		<option value="11201">101 <spring:message code="LB.W0394" /></option>
							        	</select>
									</span>
								</div>

								<!-- 細稅名稱213 -->
								<!-- 依據稅別決定是否顯示 -->
								<div class="ttb-input-item row" id="TTYPE113" style="display:none">
									<!-- 細稅名稱 -->
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0378" /></h4>
										</label>
									</span>
									<span class="input-block">
										<select name="TAXTYPE213" id="TAXTYPE213" class="custom-select select-input half-input " onChange="ChangeTaxType(this)">
							        		<option value="#" selected>---<spring:message code="LB.W0379" />---</option>
							        		<option value="11331">551 <spring:message code="LB.W0394" /></option>
							        	</select>
									</span>
								</div>
								
								<!-- 細稅名稱214 -->
								<!-- 依據稅別決定是否顯示 -->
								<div class="ttb-input-item row" id="TTYPE114" style="display:none">
									<!-- 細稅名稱 -->
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0378" /></h4>
										</label>
									</span>
									<span class="input-block">
										<select name="TAXTYPE214" id="TAXTYPE214" class="custom-select select-input half-input " onChange="ChangeTaxType(this)">
							        		<option value="#" selected>---<spring:message code="LB.W0379" />---</option>
							        		<option value="11251">405 <spring:message code="LB.W0396" /></option>
							        	</select>
									</span>
								</div>
								
								<!-- 細稅名稱215 -->
								<!-- 依據稅別決定是否顯示 -->
								<div class="ttb-input-item row" id="TTYPE115" style="display:none">
									<!-- 細稅名稱 -->
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0378" /></h4>
										</label>
									</span>
									<span class="input-block">
										<select name="TAXTYPE215" id="TAXTYPE215" class="custom-select select-input half-input " onChange="ChangeTaxType(this)">
							        		<option value="#" selected>---<spring:message code="LB.W0379" />---</option>
							        		<option value="15001">15G <spring:message code="LB.W0398" /></option>				
							        		<option value="11002">15N <spring:message code="LB.W0399" /></option>
							        		<option value="11003">15S <spring:message code="LB.W0400" /></option>
							        	</select>
									</span>
								</div>
								
								<!-- 細稅名稱216 -->
								<!-- 依據稅別決定是否顯示 -->
								<div class="ttb-input-item row" id="TTYPE116" style="display:none">
									<!-- 細稅名稱 -->
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0378" /></h4>
										</label>
									</span>
									<span class="input-block">
										<select name="TAXTYPE216" id="TAXTYPE216" class="custom-select select-input half-input " onChange="ChangeTaxType(this)">
							        		<option value="#" selected>---<spring:message code="LB.W0379" />---</option>
							        		<option value="15031">355 <spring:message code="LB.W0402" /></option>
											<option value="15032">357 <spring:message code="LB.W0403" /></option>
											<option value="11033">35H <spring:message code="LB.W0404" /></option>
							        		<option value="11035">35K <spring:message code="LB.W0405" /></option>
							        		<option value="15056">35F <spring:message code="LB.W0406" /></option>
							        	</select>
									</span>
								</div>
								
								<!-- 依據稅別決定是否顯示 -->
								<div class="ttb-input-item row" id="TYPE15_1" style="display:none">
									<!-- 稽徵單位代號 -->
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0410" /></h4>
										</label>
									</span>
									<span class="input-block">
										<input type="text" name="INTSACN1" id="INTSACN1" class="text-input"
											 value="" size="3" maxlength="3" />
										(<spring:message code="LB.W0411" />
											<a href='#' onclick="window.open('${pageContext.request.contextPath}'+'/PAY/EXPENSE/unit_code_list','', '')">
				                   			※<spring:message code="LB.W0412" /></a>)
									</span>
								</div>
								
								<!-- 依據稅別決定是否顯示 -->
								<div class="ttb-input-item row" id="TYPE15_2" style="display:none">
									<!-- 身分證字號/統一編號 -->
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Id_no" /></h4>
										</label>
									</span>
									<span class="input-block">
										<input type="text" name="INTSACN2" id="INTSACN2" class="text-input"
											 value="" size="10" maxlength="10" /><spring:message code="LB.W0414" />
									</span>
								</div>
								
								<!-- 依據稅別決定是否顯示 -->
								<div class="ttb-input-item row" id="TYPE11_1" style="display:none">
									<!-- 銷帳編號 -->
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0407" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" name="INTSACN3" id="INTSACN3" class="text-input"
											 value="" size="16" maxlength="16" />
											<span class="input-subtitle subtitle-color">
													<spring:message code="LB.W0408" />
												</span>
										</div>
										
									</span>
								</div>
								
								<!-- 依據稅別決定是否顯示 -->
								<div class="ttb-input-item row" id="TYPE11_2" style="display:none">
									<!-- 繳納截止日 -->
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0409" /></h4>
										</label>
									</span>
									<span class="input-block">
										<input type="text" id="PAYDUED_TMP" name="PAYDUED_TMP" value="" size="10"
												maxlength="10" class="text-input datetimepicker" />
										<span class="input-unit PAYDUED">
											<img src="${__ctx}/img/icon-7.svg" />
										</span>
										<!-- 為了讓提示訊息對齊用，會在初始化時先隱藏 -->
										<span class="hideblock">
											<!-- 驗證用的input -->
											<input id="PAYDUED" name="PAYDUED" type="text" class="text-input" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</span>
									</span>
								</div>
								
								<!-- 繳稅金額 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label><h4><spring:message code="LB.W0415" /></h4></label></span> 
									<span class="input-block">
										<div class="ttb-input">
											<!-- 新台幣  -->
											<span class="input-unit"><spring:message code="LB.NTD" /></span>
											<input type="text" id="AMOUNT_TMP" name="AMOUNT_TMP" class="text-input" size="10" maxlength="10" value="" /> 
											<span class="input-unit"><spring:message code="LB.Dollar" /></span>
										</div>
										<!-- 為了讓提示訊息對齊用，會在初始化時先隱藏；只會顯示提示訊息不會顯示欄位 -->
										<span class="hideblock">
											<!-- 繳稅金額 -->
											<input id="AMOUNT" name="AMOUNT" type="text" readonly="readonly" 
												class="text-input validate[required,min[1],max[9999999999],funcCall[validate_CheckAmount[<spring:message code= "LB.The_total_amount_payable" />,AMOUNT]]]"
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</span>
									</span>
								</div>
								
								<!-- 交易備註 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label><h4><spring:message code="LB.Transfer_note" /></h4></label></span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="CMTRMEMO" name="CMTRMEMO" class="text-input" size="56" maxlength="20" value="" />
											<span class="input-remarks"><spring:message code="LB.Transfer_note_note" /></span>
<%-- 											<span class="input-unit"><img src="${__ctx}/img/icon-13.svg"></span> --%>
										</div>
									</span>
								</div>
	
								<!-- 轉出成功Email通知 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4><spring:message code="LB.X0479" /></h4>
										</label>
									</span>
									<span class="input-block">
									
								<!-- 寄信通知 -->
								<c:choose>
								    <c:when test="${sendMe} && !${sessionScope.dpmyemail}.equals('')">
										    
								        <!-- 通知本人 -->
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color"><spring:message code="LB.Notify_me" />：</span>
											<span class="input-subtitle subtitle-color">${sessionScope.dpmyemail}</span>
										</div>
										
										<!-- 另通知 -->
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color"><spring:message code="LB.Another_notice" />：</span>
										</div>
												
								    </c:when>
						    		<c:otherwise>
										    
								        <!-- 通訊錄 -->
										<div class="ttb-input">
											<input type="text" id="CMTRMAIL" name="CMTRMAIL" placeholder="<spring:message code="LB.Email" />"
												class="text-input validate[funcCall[validate_EmailCheck[CMTRMAIL]]]" />
											<span class="input-unit"></span>
											<!-- window open 去查 TxnAddressBook 參數帶入身分證字號 -->
											<button type="button" class="btn-flat-orange" id="getAddressbook"><spring:message code="LB.Address_book" /></button>
										</div>
												
								    </c:otherwise>
								</c:choose>
										
										<!-- 摘要內容 -->
										<div class="ttb-input">
											<input type="text" id="CMMAILMEMO" name="CMMAILMEMO" class="text-input"
												 placeholder="<spring:message code="LB.Summary" />" value="" />
											<span class="input-unit"></span>
											<button type="button" class="btn-flat-orange" id="CMMAILMEMO_btn"><spring:message code="LB.As_transfer_note" /></button>
										</div>
									</span>
								</div>
							</div>
							
							<!-- 重新輸入、確認按鈕 -->
							<input id="CMRESET" type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" onclick="formReset();" />
							<input id="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
						
						</div>
					</div>
					
					<!-- 說明 -->
					<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /></p>
						<li><spring:message code="LB.Pay_Taxes_P1_D1-1" /><a href="${__ctx}/CUSTOMER/SERVICE/rate_table" target="_blank"><spring:message code="LB.Pay_Taxes_P1_D1-2" /></a> </li>
						<li><spring:message code="LB.Pay_Taxes_P1_D2" /></li>
						<li><spring:message code="LB.Pay_Taxes_P1_D3" /></li>
						<li><spring:message code="LB.Pay_Taxes_P1_D4" /> </li>
						<li><spring:message code="LB.Pay_Taxes_P1_D5" /></li>
						<li><B><spring:message code="LB.Pay_Taxes_P1_D6-1" /><a href="https://paytax.nat.gov.tw" target=_blank><spring:message code="LB.Pay_Taxes_P1_D6-2" /></a><spring:message code="LB.Pay_Taxes_P1_D6-3" /></B></li>
						<li><font style="color:red"><spring:message code="LB.Pay_Taxes_P1_D7" /></font></li>
			        </ol>
					
				</form>
			</section>
		</main>
	</div>

	<%@ include file="../index/footer.jsp"%>

</body>
</html>
