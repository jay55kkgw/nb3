<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {
		// 將.table變更為footable
		initFootable();
	});
	
	//下拉式選單
 	function formReset() {
 		if ($('#actionBar').val()=="reEnter"){
	 		document.getElementById("formId").reset();
	 		$('#actionBar').val("");
 		}
	}
</script>
</head>
	<body>
		<!-- header     -->
		<header>
			<%@ include file="../index/header.jsp"%>
		</header>
	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 貸款服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Loan_Service" /></li>
    <!-- 借款查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Loan_Inquiry" /></li>
		</ol>
	</nav>

		<!-- menu、登出窗格 -->
		<div class="content row">
			<%@ include file="../index/menu.jsp"%>
			<!-- 	快速選單內容 -->
			<!-- content row END -->
			<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2><spring:message code= "LB.W0012" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 						下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
<!-- 						下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
					</select>
				</div>
				<form id="formId" method="post" action="">
					<div class="main-content-block row">
						<div class="col-12">
							<!-- 輕鬆理財證券交割明細 -->
							<table class="table" data-toggle-column="first">
								<tbody>
									<tr>
										<!-- 帳號 -->
										<td><spring:message code="LB.Inquiry_time"/></td>
										<!-- 帳號 的下拉式選單-->
										<td class="text-left">
											<form>
												<select name="dptrdacno">
													<option value="00">---<spring:message code= "LB.X1556" />---</option>
													<option value="01"><spring:message code= "LB.Account" />1</option>
													<option value="02"><spring:message code= "LB.Account" />2</option>
												</select>
											</form>
										</td>
									</tr>
									<tr>
										<!-- 查詢期間 -->
										<td><spring:message code="LB.Inquiry_period_1"/></td>
										<!-- 輸入 查詢期間-->
										<td class="text-left">
											<!--  指定日期區塊 -->
											<div class="ttb-input">
												<!--期間起日 -->
												<input type="text" id="CMSDATE" name="CMSDATE" class="text-input datetimepicker" value="" />
												<span class="input-unit CMSDATE">
													<img src="${__ctx}/img/icon-7.svg" />
												</span>
												<!-- 驗證用的span預設隱藏 -->
												<span id="hideblock_CMSDATE" >
												<!-- 驗證用的input -->
												<input id="validate_CMSDATE" name="validate_CMSDATE" type="text" class="text-input validate[required, verification_date[validate_CMSDATE]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												</span>~&nbsp;
												<!--期間迄日 -->
												<input type="text" id="CMEDATE" name="CMEDATE" class="text-input datetimepicker" value="" />
												<span class="input-unit CMEDATE">
													<img src="${__ctx}/img/icon-7.svg" />
												</span>
												<!-- 驗證用的span預設隱藏 -->
												<span id="hideblock_CMEDATE" >
												<!-- 驗證用的input -->
												<input id="validate_CMEDATE" name="validate_CMEDATE" type="text" class="text-input validate[required, verification_date[validate_CMEDATE]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												</span>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
							<!-- 網頁顯示-->
							<input type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Display_as_web_page"/>"/>
							<!-- 下載Excel檔-->
							<input type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Download_excel_file"/>"/>
							<!-- 下載.txt檔-->
							<input type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Download_txt_file"/>"/>
							<!-- 重新輸入-->
	                        <input type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Re_enter"/>"/>
						</div>
					</div>
					<div class="text-left">
						<!-- 說明 -->		
						
						<ol class="list-decimal text-left description-list">
						<p><spring:message code="LB.Description_of_page" /></p>
							<li style="margin-left:20px;">
								<spring:message code= "LB.Passbook_Securities_Details_P1_D1" />
							</li>
						</ol>
					</div>
				</form>
				</section>
			</main>
			<!-- 		main-content END -->
			<!-- 	content row END -->
		</div>
		<%@ include file="../index/footer.jsp"%>
	</body>
</html>