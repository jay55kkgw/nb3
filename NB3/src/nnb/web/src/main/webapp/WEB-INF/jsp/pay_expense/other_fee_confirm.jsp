<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
	
	<script type="text/JavaScript">
		$(document).ready(function() {
			// HTML載入完成後開始遮罩
			initBlockUI();
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 初始化驗證碼
			setTimeout("initKapImg()", 200);
			// 生成驗證碼
			setTimeout("newKapImg()", 300);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});

		// 畫面初始化
		function init() {
			// 表單驗證初始化
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });

			// 確認鍵 click
			goOn();
			// 上一頁按鈕 click
			goBack();
			// 判斷顯不顯示驗證碼
			chaBlock();
			// 交易機制 click
			fgtxwayClick();

		}

		// 確認鍵 Click
		function goOn() {
			$("#CMSUBMIT").click( function(e) {
				// 送出進表單驗證前將span顯示
				$("#hideblock").show();
				
				console.log("submit~~");
	
				// 表單驗證
				if ( !$('#formId').validationEngine('validate') ) {
					e.preventDefault();
				} else {
					// 解除表單驗證
					$("#formId").validationEngine('detach');
					
					// 通過表單驗證
					processQuery();
				}
			});
		}

		// 通過表單驗證準備送出
		function processQuery(){
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
			
			// 根據金額驗證交易機制
			if(!checkAmountByFGTXWAY(fgtxway)){
				return false;
			}
			
			// 交易機制選項
			switch(fgtxway) {
				case '0':
					$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
					initBlockUI();
					$('#formId').submit();
					break;
				case '1':
					// IKEY
					useIKey();
					
					break;
				case '2':
					// 晶片金融卡
					var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
					useCardReader(capUri);
					
			    	break;
				case '7'://IDGATE認證		 
		            idgatesubmit= $("#formId");		 
		            showIdgateBlock();		 
		            break;
				default:
					//alert("<spring:message code= "LB.Alert001" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert001' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			}
		}
		
		// 根據金額驗證交易機制
		function checkAmountByFGTXWAY(fgtxway){
			var retStr = "";
			var amount = '${transfer_data.data.AMOUNT }';
			amount = parseInt(fstop.unFormatAmtToInt(amount));
			console.log("checkAmountByFGTXWAY.amount: " + amount);
			
			var transferType = '${transfer_data.data.TransferType }';
			console.log("checkAmountByFGTXWAY.transferType: " + transferType);
			
			var fgsvacno = '${transfer_data.data.FGSVACNO }';
			console.log("checkAmountByFGTXWAY.fgsvacno: " + fgsvacno);
			
			// 預約/即時轉帳金額大於500萬時,交易機制不可以選SSL
			if('0' == fgtxway && amount > 5000000 ){
				retStr = "<spring:message code= "LB.X1413" />\n\n <spring:message code= "LB.X1414" />";
				//alert(retStr);
				errorBlock(
								null, 
								null,
								["<spring:message code= "LB.X1413" />", "\n", "\n", "<spring:message code= "LB.X1414" />"], 
								'<spring:message code= "LB.Quit" />', 
								null
							);
				return false;
				
			} // IKEY
			else if('1' == fgtxway){
				// 約 -> 約
				if(transferType=='PD' && fgsvacno=='1'){
					// 預約/即時轉帳金額大於1500萬時,交易機制不可以選i-key
					if(amount > 15000000){
						retStr = "<spring:message code= "LB.X1413" />\n\n <spring:message code= "LB.X1415" />";
						//alert(retStr);
						errorBlock(
								null, 
								null,
								["<spring:message code= "LB.X1413" />", "\n", "\n", "<spring:message code= "LB.X1415" />"], 
								'<spring:message code= "LB.Quit" />', 
								null
							);
						return false;
					}
				} // 約 -> 非約
				else {
					// 預約/即時非約定轉帳金額大於300萬時,交易機制不可以選IKEY
					if(amount > 3000000) {
						retStr = "<spring:message code= "LB.X1413" />\n\n <spring:message code= "LB.X1416" />";
						//alert(retStr);
						errorBlock(
								null, 
								null,
								["<spring:message code= "LB.X1413" />", "\n", "\n", "<spring:message code= "LB.X1416" />"], 
								'<spring:message code= "LB.Quit" />', 
								null
							);
						return false;
					}						
				}
				
			} // 晶片金融卡
			else if('2' == fgtxway){
				// 預約/即時轉帳金額大於10萬時,交易機制不可以選晶片金融卡
				if(transferType == "NPD" || fgsvacno == "2") {
					if(amount > 100000) {
						retStr = "<spring:message code= "LB.X1413" />\n\n <spring:message code= "LB.X1417" />";
						//alert(retStr);
						errorBlock(
									null, 
									null,
									["<spring:message code= "LB.X1413" />", "\n", "\n", "<spring:message code= "LB.X1417" />"], 
									'<spring:message code= "LB.Quit" />', 
									null
								);
						return false;
					}
				}
			}
			
			return true;
			
		}
		
		// 上一頁按鈕 click
		function goBack() {
			// 上一頁按鈕
			$("#pageback").click(function() {
				// 回上一頁到輸入頁ESAPI驗證jsondc會壞掉故不送jsondc
				$("#jsondc").prop("disabled",true);
				
				// 遮罩
				initBlockUI();
				
				// 解除表單驗證
				$("#formId").validationEngine('detach');
				
				// 讓Controller知道是回上一頁
				$('#back').val("Y");
				// 回上一頁
				var action = '${pageContext.request.contextPath}' + '${previous}';
				$("#formId").attr("action", action);
				$("#formId").submit();
			});
		}

		// 執行選項
	 	function formReset() {
	 		if ($('#actionBar').val()=="reEnter"){
		 		document.getElementById("formId").reset();
		 		$('#actionBar').val("");
	 		}
		}

		// 使用者選擇晶片金融卡要顯示驗證碼區塊
	 	function fgtxwayClick() {
	 		$('input[name="FGTXWAY"]').change(function(event) {
	 			// 判斷交易機制決定顯不顯示驗證碼區塊
	 			chaBlock();
			});
		}
	 	// 判斷交易機制決定顯不顯示驗證碼區塊
		function chaBlock() {
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
 			console.log("fgtxway: " + fgtxway);

 			// 交易機制選項
 			switch(fgtxway) {
 			// SSL
			case '0':
				$('#CMPASSWORD').addClass("validate[required]")
				$("#chaBlock").hide();
				break;
			// ICCARD
			case '2':
				$('#CMPASSWORD').removeClass("validate[required]")
				// 若為晶片金融卡才顯示驗證碼欄位
				$("#chaBlock").show();
		    	break;
		    // IKEY
			default:
				$('#CMPASSWORD').removeClass("validate[required]")
				$("#chaBlock").hide();
 			}
			
	 	}

		// 驗證碼刷新
		function changeCode() {
			$('input[name="capCode"]').val('');
			// 大小版驗證碼用同一個
			console.log("changeCode...");
			$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

			// 登入失敗解遮罩
			unBlockUI(initBlockId);
		}
		
		// 初始化驗證碼
		function initKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
		}
		
		// 生成驗證碼
		function newKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').click(function() {
				$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
			});
		}
		
	</script>
</head>
<body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	<!--   IDGATE -->
    <%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %>	
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 繳費     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0616" /></li>
		</ol>
	</nav>


	<!-- 功能清單及登入資訊 -->
	<div class="content row">
	
		<!-- 功能清單 -->
		<%@ include file="../index/menu.jsp"%>

		<!-- 快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
				<form method="post" id="formId" name="formId" action="${__ctx}/PAY/EXPENSE/other_fee_result">
					<input type="hidden" name="TXTOKEN" value="${transfer_data.data.TXTOKEN}"><!-- 防止不正常交易 -->
					<input type="hidden" id="back" name="back" value=""><!-- 回上一頁資料 -->
					<input type="hidden" name="CMTRMAIL" value="${transfer_data.data.CMTRMAIL}"><!--轉出成功Email通知-->
					<input type="hidden" id="PINNEW" name="PINNEW"  value="">
					<!-- 交易機制所需欄位 -->
					<input type="hidden" id="jsondc" name="jsondc" value='${transfer_data.data.jsondc}'>
					<input type="hidden" id="ISSUER" name="ISSUER" value="">
					<input type="hidden" id="ACNNO" name="ACNNO" value="">
					<input type="hidden" id="TRMID" name="TRMID" value="">
					<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
					<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
					<input type="hidden" id="TAC" name="TAC" value="">
					<input type="hidden" id="CMDD" name="CMDD" value='${transfer_data.data.CMDD}'>
					<input type="hidden" id="CMSDATE" name="CMSDATE" value='${transfer_data.data.CMSDATE}'>
					<input type="hidden" id="CMEDATE" name="CMEDATE" value='${transfer_data.data.CMEDATE}'>
					<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
					
					<!-- 功能名稱 -->
					<h2><spring:message code="LB.W0616" /></h2>
					<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
					
					<!-- 下拉式選單-->
					<div class="print-block no-l-display-btn">
						<select class="minimal" id="actionBar" onchange="formReset()">
							<!-- 執行選項-->
							<option>
								<spring:message code="LB.Execution_option" />
							</option>
							<!-- 重新輸入-->
							<option value="reEnter">
								<spring:message code="LB.Re_enter" />
							</option>
						</select>
					</div>
					
					<!-- 交易流程階段 -->
					<div id="step-bar">
						<ul>
							<li class="finished"><spring:message code="LB.Enter_data" /></li>
							<li class="active"><spring:message code="LB.Confirm_data" /></li>
							<li class=""><spring:message code="LB.Transaction_complete" /></li>
						</ul>
					</div>
					
					<!-- 功能內容 -->
					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-input-block">
							<div class="ttb-message">
									<p id="FGTXDATE"></p>
									<!-- 即時繳費-->
									<c:if test = "${transfer_data.data.FGTXDATE == 1}">
										<span><spring:message code="LB.Confirm_transfer_data" /></span>
									</c:if>
									<!-- 預約繳費-->
									<c:if test = "${transfer_data.data.FGTXDATE == 2}">
										<span><spring:message code="LB.Confirm_the_Scheduled_Transaction_data" /></span>
									</c:if>
								</div>
								
								<!-- 轉帳日期 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Transfer_date" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<c:choose>
												<c:when test="${transfer_data.data.FGTXDATE == 3}">
													<spring:message code="LB.Monthly" />${transfer_data.data.CMDD}
													<spring:message code="LB.Day" />，
					      							<spring:message code="LB.Period_start_date" />${transfer_data.data.CMSDATE}，
					      							<spring:message code="LB.Period_end_date" />${transfer_data.data.CMEDATE}
												</c:when>
												<c:otherwise>
													<p>${transfer_data.data.transfer_date }</p>
												</c:otherwise>
											</c:choose>
										</div>
									</span>
								</div>
								
								<!-- 轉出帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Payers_account_no" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>${transfer_data.data.OUTACN }</p>
										</div>
									</span>
								</div>
								
								<!-- 轉入帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Payees_account_no" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>${transfer_data.data.DPAGACNO_TEXT}</p>
											<c:if test="${not empty transfer_data.data.errorMsg}">
												<p style="color:red;">${transfer_data.data.errorMsg}</p>
											</c:if>
										</div>
									</span>
								</div>
	
								<!-- 轉帳金額 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Amount" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p class="high-light">
												<!-- 新臺幣 -->
												<span class="input-unit">
													<spring:message code="LB.NTD" />
												</span>
												${transfer_data.data.AMOUNT_FMT }
												<!-- 元 -->
												<span class="input-unit">
													<spring:message code="LB.Dollar" />
												</span>
											</p>
										</div>
									</span>
								</div>
	
								<!-- 交易機制 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Transaction_security_mechanism" /></h4>
										</label>
									</span>
									<!-- 交易機制選項 -->
									<span class="input-block">
									
									<!-- 使用者是否可以使用SSL -->
									<c:if test = "${transfer_data.data.hiddenCMSSL != 'Y'}">
									
										<!-- 交易密碼SSL -->
										<div class="ttb-input">
											<label class="radio-block"> 
												<spring:message	code="LB.SSL_password" /> 
												<input type="radio" name="FGTXWAY" checked="checked" value="0"> 
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<input id="CMPASSWORD" name="CMPASSWORD" type="password" class="text-input" size="8" maxlength="8"
												placeholder="<spring:message code="LB.Please_enter_password"/>">
										</div>
										
									</c:if>


								<!-- 使用者是否可以使用IKEY -->
								<c:if test = "${sessionScope.isikeyuser}">
									<!-- 即使使用者可以用IKEY，若轉出帳號為晶片金融卡帶出的非約定帳號，也不給用 (By 景森)-->
									<c:if test = "${transfer_data.data.TransferType != 'NPD'}">
							
										<!-- IKEY -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Electronic_signature" />
												<input type="radio" name="FGTXWAY" id="CMIKEY" value="1">
												<span class="ttb-radio"></span>
											</label>
										</div>
										
									</c:if>
								</c:if>
									<div class="ttb-input" name="idgate_group" style="display:none">
                                        <label class="radio-block">
                                        	裝置推播認證(請確認您的行動裝置網路連線是否正常，及推播功能是否已開啟)
                                        	<input type="radio" id="IDGATE" name="FGTXWAY" value="7"/>
                                            <span class="ttb-radio"></span>
                                        </label>
                                    </div>	
									
									<!-- 晶片金融卡 -->
									<div class="ttb-input">
										<label class="radio-block">
											<spring:message code="LB.Financial_debit_card" />
												<!-- 只能選晶片金融卡時，預設為非勾選讓使用者點擊後顯示驗證碼-->
												<c:choose>
													<c:when test="${transfer_data.data.TransferType == 'NPD'}">
														<input type="radio" name="FGTXWAY" id="CMCARD" value="2" checked="checked">
													</c:when>
													<c:when test="${!sessionScope.isikeyuser}">
														<input type="radio" name="FGTXWAY" id="CMCARD" value="2" checked="checked">
													</c:when>
													<c:otherwise>
													 	<input type="radio" name="FGTXWAY" id="CMCARD" value="2">
													</c:otherwise>
												</c:choose>
												
												<span class="ttb-radio"></span>
											</label>
										</div>
										
									</span>
								</div>
								
								<!-- 如果是非約定則不能使用IKEY，只能選晶片金融卡，使用者選擇晶片金融卡後才顯示驗證碼-->
								<div class="ttb-input-item row" id="chaBlock" style="display:none">
									<!-- 驗證碼 -->
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Captcha" /></h4>
										</label>
									</span>
									<span class="input-block">
	                                    <div class="ttb-input">
	                                        <img name="kaptchaImage" class="verification-img" src="" align="top" id="random" />
	                                        <button type="button" class="btn-flat-gray" name="reshow" onclick="changeCode()">
												<spring:message code="LB.Regeneration" /></button>
	                                    </div>
	                                    <div class="ttb-input">
	                                        <input id="capCode" name="capCode" type="text"
												class="text-input" maxlength="6" autocomplete="off">
	                                        <br>
	                                        <span class="input-unit"><spring:message code="LB.Captcha_refence" /></span>
	                                    </div>
	                                </span>
								</div>
								
							</div><!-- ttb-input-block end -->
							
							
							<input type="button" id="pageback" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />" />
							<input type="reset"  class="ttb-button btn-flat-gray no-l-disappear-btn" value="<spring:message code="LB.Re_enter" />" />
							<input type="button" id="CMSUBMIT" name="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
						</div>
					</div>
				</form>
			</section>
		</main>
	</div>

	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>