<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<script type="text/javascript" src="${__ctx}/js/jquery.maskedinput.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.twzipcode-1.6.1.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
	<script type="text/javascript" src="${__ctx}/js/CCardRange.js"></script>
	<style>
		.zipcode {
			display: none;
		}
	</style>
	    <script type="text/javascript">
        $(document).ready(function () {
        	init();
    		// 初始化驗證碼
    		setTimeout("initKapImg()", 200);
    		// 生成驗證碼
    		setTimeout("newKapImg()", 300);
        });
        function init() {
			$("#formId").validationEngine({binded: false,promptPosition: "inline", scroll: false});
	    	$("#CMSUBMIT").click(function(e){
				var craddate = $("#CCEXDATE").val().replace(new RegExp("/", "g"),'');
				$("#CCEXDATEYY").val(craddate.substring(2));
				$("#CCEXDATEMM").val(craddate.substring(0,2));
				$("#EXPDTA").val($("#CCEXDATEYY").val() + $("#CCEXDATEMM").val());
				$("#CARDNUM").val($("#CARDNUM1").val().replace(new RegExp("/", "g"),''));
	    		//驗證碼驗證
	    		var capURI = "${__ctx}/CAPCODE/captcha_valided_trans";
	    		var capData = $("#formId").serializeArray();
	    		var capResult = fstop.getServerDataEx(capURI,capData,false);
	    		
	    		//驗證結果
	    		if(capResult.result == true){
	    		
					e = e || window.event;
	    			//進行 validation 並送出
					if (!$('#formId').validationEngine('validate')) {
						e.preventDefault();
					} else {
						$("#formId").validationEngine('detach');
			        	var action = '${__ctx}/EBILL/PAY/taiwaterpayment_result';
						$("form").attr("action", action);
						$("#CMBACK").hide();
						$("#CMSUBMIT").hide();
						$("#waitnotice").show();
		    			$("form").submit();
					}
	    		}else{
	    			//失敗重新產生驗證碼
	    			errorBlock(null, null, ["<spring:message code='LB.X1082' />"],
	    				'<spring:message code= "LB.Quit" />', null);
//	    				alert("<spring:message code= "LB.X1082" />");
	    			changeCode();
	    		}
			});
	    	$("#CMBACK").click(function(){
	    		$("#formId").validationEngine('detach');
	    		$("#formId").attr("action","${__ctx}/EBILL/PAY/taiwaterpayment");
	    		$("#formId").submit();
	    	});
        }

		jQuery(function($) {//,{autoclear: false}
			$("#CARDNUM").mask("9999/9999/9999/9999",{autoclear: false});
			$("#EXPIREDYM").mask("99/99",{autoclear: false});
		});
		
		
		// 刷新驗證碼
		function refreshCapCode() {
			console.log("refreshCapCode...");

			// 驗證碼
			$('input[name="capCode"]').val('');
			
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

			// 登入失敗解遮罩
			unBlockUI(initBlockId);
		}

		// 刷新輸入欄位
		function changeCode() {
			console.log("changeCode...");
			
			// 清空輸入欄位
			$('input[name="capCode"]').val('');
			
			// 刷新驗證碼
			refreshCapCode();
		}

		// 初始化驗證碼
		function initKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
		}

		// 生成驗證碼
		function newKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').click(function() {
				refreshCapCode();
			});
		}
    </script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
    <!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item">信用卡</li>
			<li class="ttb-breadcrumb-item">繳費區</li>
			<li class="ttb-breadcrumb-item">臺灣自來水水費</li>
		</ol>
	</nav>
	<div class="content row">
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					臺灣企銀信用卡繳臺灣自來水水費
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form id="formId" method="post">
					<input type="hidden" name="back" id="back" value=>	
					<input type="hidden" name="SessionId" value="${result_data.data.SessionId}" />
					<input type="hidden" name="EXPDTA" id="EXPDTA" value="" />
					<input type="hidden" name="IP" value="${result_data.data.IP}">
					<input type="hidden" name="TXTOKEN" value="${result_data.data.TXTOKEN}">
					<input type="hidden" name="TrnsCode" value="${result_data.data.TrnsCode}">
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
							
							<div class="ttb-input-block">
	                            <div class="ttb-message">
                                	<p>輸入信用卡資料</p>
	                            </div>
								<!-- 個人/家庭年收入 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											代收期限
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<spring:message code="LB.D0583"/>&nbsp;${input_data.CMDATE1.substring(0,3)}<spring:message code="LB.Year"/>&nbsp;${input_data.CMDATE1.substring(3,5)}<spring:message code="LB.Month"/>&nbsp;${input_data.CMDATE1.substring(5,7)}<spring:message code="LB.D0586"/>&nbsp;
											<input type="hidden" name="CMDATE1" value="${input_data.CMDATE1}" />
											<input type="hidden" name="CMDATE2" value="${result_data.data.CMDATE2}" />
										</div>
									</span>
								</div>
								
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											銷帳編號
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.WAT_NO}
											<input type="hidden" name="WAT_NO" value="${input_data.WAT_NO}" />
										</div>
									</span>
								</div>
								
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											查核碼
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.CHKCOD}
											<input type="hidden" name="CHKCOD" value="${input_data.CHKCOD}" />
										</div>
									</span>
								</div>
								
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											應繳款金額
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.AMOUNT}
											<input type="hidden" name="AMOUNT" value="${input_data.AMOUNT}" />
										</div>
									</span>
								</div>
								
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											信用卡卡號
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="CARDNUM1" name="CARDNUM1"  placeholder="____ / ____ / ____ / ____" class="text-input validate[required]" maxLength="16" size="16"/>
											
											<input id="CARDNUM" name="CARDNUM" type="text" class="
												validate[funcCallRequired[validate_CheckNumber1[信用卡號碼資料有誤，請確認,CARDNUM,false]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											<input id="validate_CARDNUM2" name="validate_CARDNUM2" type="text" class="
												validate[funcCallRequired[validate_checkTBBCard1[請使用本行一般信用卡繳費,CARDNUM]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											卡片效期
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
                                            <input type="text" class="text-input validate[required,funcCall[validate_chkperiod['sFieldName',CCEXDATEYY,CCEXDATEMM]]]" name="CCEXDATE" id="CCEXDATE" value="" placeholder="MM/YY" style="text-align:center"/>
                                            <input type="hidden" name="CCEXDATEMM" id="CCEXDATEMM" value="">
                                            <input type="hidden" name="CCEXDATEYY" id="CCEXDATEYY" value="">
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											信用卡背面末三碼
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
                                            <input type="text" class="text-input validate[required]" name="CHECKNO" id="CHECKNO" value="" placeholder="CVV" style="text-align:center" maxLength="3"/>
											<input id="validate_CHECKNO" name="validate_CHECKNO" type="text" class="
												validate[funcCallRequired[validate_CheckNumber1[信用卡背面末三碼資料有誤，請確認,CHECKNO,false]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</div>
									</span>
								</div>
								
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											手續費
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${result_data.data.FEE}
											<input type="hidden" name="FEE" value="${result_data.data.FEE}" />
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
<!-- 												驗證碼 -->
												<spring:message code="LB.Captcha" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input id="capCode" type="text" class="ttb-input text-input input-width-125"
												name="capCode" placeholder="<spring:message code='LB.X1702' />" maxlength="6" autocomplete="off">
											<img name="kaptchaImage" src="" class="verification-img"/>
											<!-- 重新產生驗證碼 -->
											<input type="button" name="reshow" class="ttb-sm-btn btn-flat-gray" onclick="refreshCapCode()" value="<spring:message code='LB.Regeneration' />" /><!-- 重新產生驗證碼 -->
											<span class="input-remarks"><spring:message code="LB.X1972" /></span><!-- 請注意：英文不分大小寫，限半形字元 -->
										</div>
									</span>
								</div>
							</div>
                            <input id="CMBACK" name="CMBACK" type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.X0318"/>" />
                            <input id="CMSUBMIT" name="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.X0080"/>" />
                            <div id='waitnotice' style="display:none"><center><br><font color="red">處理中，請稍候。<br>請勿再次執行，以免重複繳款！</font></center></div>
                        </div>  
                    </div>
                </form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>