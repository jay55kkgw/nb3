<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
	    <script type="text/javascript">
	    $(document).ready(function () {
            initFootable(); // 將.table變更為footable 
            init();
        });

        function init() {
	    	$("#CMSUBMIT").click(function(e){			
	        	initBlockUI();

				var action = "${__ctx}/OTHER/FEE/other_health_insurance_confirm";
				$("form").attr("action", action);
    			$("form").submit();
			});
	    	$("#CMBACK").click(function(e){			
	        	initBlockUI();
				var action = '${__ctx}/OTHER/FEE/other_health_insurance';
				$("#back").val("Y");
				$("form").attr("action", action);
    			$("form").submit();
			});
        }
    </script>
    <style>
    	.DataCell{
    	    text-align: left;
    	}
    </style>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 自動扣繳服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2360" /></li>
    <!-- 委託轉帳代繳全民健康保險費約定條款     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0084" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!-- 預約黃金交易查詢/取消 -->
					<!-- <spring:message code="LB.NTD_Demand_Deposit_Detail" /> -->
					<spring:message code="LB.X0084" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>						
				
                <form id="formId" method="post">
					<input type="hidden" id="back" name="back" value="">
					<input type="hidden" name="action" value="forward">
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
							<div class="ttb-message">
								<p><spring:message code="LB.X0084" /></p>
								<span><font color="royalblue" size="3"><b>下列為申請委託轉帳代繳全民健康保險保險費應遵守之約定條款內容，您如接受本約定條款則請按<font color=red>我同意約定條款</font>鍵，以完成申請作業，您如不同意條款內容，則請按<font color=red>回上頁</font>鍵，本行將不受理您的代扣繳申請。</b></font></P></span>
							</div>
							<ul class="ttb-result-list terms">
                            	<li data-num="一、">
                            		<p>本人【公司】(以下簡稱立約人)委託臺灣中小企業銀行(以下簡稱貴行)自指定之存款帳戶(即指前頁之扣帳帳號，以下簡稱轉帳代繳帳戶)轉帳代繳全民健康保險保險費(以下簡稱保險費)，並自行依據最近月份保險費繳款單或收據內容輸入代扣繳資料，如因代扣繳申請書內容輸入不全、錯誤或其他原因，致貴行無法辦理轉帳，則本約定書不生效力。</p>
                            	</li>
                            	<li data-num="二、">
                            		<p>立約人申請轉帳代繳本人【公司】或指定第三人保險費，同意自貴行接受委託，並洽妥中央健康保險局(以下簡稱健保局)轄區分局完成建檔之月份(以申請之次月為原則)起開始轉帳。在未完成建檔前各月份之保險費，仍由保險費繳款人(以下簡稱繳款人)自行繳納。</p>
                            	</li>
                            	<li data-num="三、">
                            		<p>貴行代繳義務，以立約人轉帳代繳帳戶可用餘額足敷當月份委託代繳之保險費為限(即每月月底前一日轉帳代繳帳戶須保持足夠之餘額以供備付)。轉帳代繳帳戶餘額不敷繳付時，貴行應於次月十五日零時(如遇假日為其次一營業日)再行轉帳乙次(即十四日轉帳代繳帳戶須足夠餘額以供備付)，倘仍存款不足，則由繳款人自行持保險費繳款單至指定之金融機構繳納。如繳款人因此而須負擔滯納金，立約人同意貴行自轉帳代繳帳戶中扣繳或由立約人自行負責。</p>
                            	</li>
                            	<li data-num="四、">
                            		<p>立約人委託代繳保險費，如轉帳代繳帳戶因遭法院強制執行或其他事故致無法代繳時，貴行得終止代繳之約定，其因此而致繳款人須負擔滯納金，概由立約人負責。</p>
                            	</li>
                            	<li data-num="五、">
                            		<p>立約人擬在貴行另行指定轉帳代繳帳戶時，應註銷原委託約定再重新辦理代扣繳申請；並同意自貴行受理變更，及洽妥健保局轄區分局完成更檔之月份(以申請之次月為原則)起，由新帳戶代繳保險費。</p>
                            	</li>
                            	<li data-num="六、">
                            		<p>立約人委託代繳保險費，在未終止委託前，不得藉故拒絕繳納保險費，如因此而致繳款人須負擔滯納金時，概由立約人負責。</p>
                            	</li>
                            	<li data-num="七、">
                            		<p>立約人委託代繳保險費，在未終止委託前，自行結清轉帳代繳帳戶時，視同自動解除代繳之約定，其因此而致繳款人須負擔滯納金時，概由立約人負責。</p>
                            	</li>
                            	<li data-num="八、">
                            		<p>貴行或立約人皆得隨時以書面通知對方終止代繳契約。立約人終止代繳時應填具「註銷委託轉帳代繳全民健康保險費約定書」，並自貴行接受註銷委託，且洽妥健保局轄區分局完成更檔之月份(以申請之次月為原則)起，終止以該轉帳代繳帳戶轉帳代繳保險費。其因註銷委託而致繳款人須負擔滯納金時，概由立約人負責。</p>
                            	</li>
                            	<li data-num="九、">
                            		<p>立約人委託代繳保險費之繳款人代號，倘貴行接獲健保局轄區分局改號通知時，立約人同意貴行以異動後之繳款人代號，繼續委託代繳。</p>
                            	</li>
                            	<li data-num="十、">
                            		<p>立約人指定之轉帳代繳帳戶為支票存款帳戶者，倘因扣繳保險費而致存款不足，發生退票情事，概由立約人自行負責。</p>
                            	</li>
                            	<li data-num="十一、">
                            		<p>倘貴行之電腦系統發生故障或電信中斷等因素致無法執行轉帳代繳時，貴行得順延至系統恢復正常，始予扣款，其因上開事由所致之損失及責任，由立約人自行負擔。</p>
                            	</li>
                            	<li data-num="十二、">
                            		<p>貴行於同一日需自轉帳代繳帳戶執行多筆轉帳扣繳作業而立約人存款不足時，立約人同意由貴行自行選定扣款順序。</p>
                            	</li>
                            	<li data-num="十三、">
                            		<p>立約人委託代繳保險費之收據由健保局寄發。</p>
                            	</li>
                            	<li data-num="十四、">
                            		<p>立約人同意貴行得將立約人個人根據特定目的填列之相關基本資料提供貴行電腦處理及利用。</p>
                            	</li>
                            </ul>
						        	
	                        <!--button 區域 -->
	                            <input id="CMBACK" name="CMBACK" type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />" />
	                            <input id="CMSUBMIT" name="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.W1554" />"/>
                        <!--                     button 區域 -->
                        </div>  
                    </div>
                </form>
<!-- 				<div class="text-left"> -->
<!-- 				    		說明： -->
<%-- 					<spring:message code="LB.Description_of_page"/>: --%>
<!-- 				    <ol class="list-decimal text-left"> -->
<!-- 				        <li>您所提供的身分證資料，經本行查詢後，如有疑義，本行得暫停您的黃金存摺網路銀行功能。 </li> -->
<!--           				<li>.線上申請黃金存摺帳戶手續費優惠為新台幣50元。 </li> -->
<!-- 				    </ol> -->
<!-- 				</div> -->

	
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>