<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<!-- 讀卡機所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/cardReaderMethod.js"></script>

	<script type="text/javascript">
		// 初始化
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		// 初始化
		function init() {
			// 初始化時隱藏span
			$("#hideblock").hide();
			//建立下拉選單的Json
			TableJson = ${installment_saving.data.TableJson}
// 			fdpnumAmountTable = {};
			$("#formId").validationEngine({binded: false,promptPosition: "inline"});
			//建立約定轉出帳號下拉選單
			creatOutACNO();
			// 轉出帳號change事件，要秀出可用餘額
			acnoEvent();
			//轉出帳號切換事件
			transferTypeChange();
			autoFillbyN420();
			// 確認按鈕點擊觸發submit
			finish();
		}
		
		//若從台幣定存明細而來 將存單帳號及號碼帶入
		function autoFillbyN420(){
			if('${installment_saving.data.ACN}'!=''){
				console.log("FROM N420");
				$('#FDPACN').val('${installment_saving.data.ACN}');
				changeFdpAcn('${installment_saving.data.ACN}');
				$('#FDPNUM').val('${installment_saving.data.FDPNUM}');
				var fdpacnText = $("#FDPACN").find(":selected").text();
				$("#FDPACN_TEXT").val(fdpacnText);
				
			}else{
				console.log("NOT FROM N420");
			}
		}
		
		// 確認按鈕點擊觸發submit
		function finish(){
			$("#CMSUBMIT").click(function (e) {
				console.log("submit~~");
				//打開驗證隱藏欄位
				$("input[name='hideblock']").show();
				//塞值進span內的input
				$("#H_AMOUNT").val($("#AMOUNT").val());
				$("#H_CMTRMAIL").val($("#CMTRMAIL").val());
				// 塞值進隱藏span內的input--約定/非約定 之轉出帳號
				if( $('input[name=TransferType]:checked').val() == 'PD'){
					// 轉出帳號為約定
					$("#OUTACN").val($("#ACNO").val());
				}else if( $('input[name=TransferType]:checked').val() == 'NPD'){
					// 轉出帳號為非約定
					$("#OUTACN").val($("#OUTACN_NPD").val());
				}
				
				//驗證
				if (!$('#formId').validationEngine('validate')) {
					e = e || window.event; // for IE
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					initBlockUI();//遮罩
					$("#formId").submit();
				}
			});
		}
		
		// 把下拉選單選中的文字塞到隱藏欄位 
		function changeDisplay() {
			var fdpacnVal = $("#FDPACN").find(":selected").val();
			var fdpacnText = $("#FDPACN").find(":selected").text();
			console.log("FDPACNval" + fdpacnVal)
			console.log("FDPACNval2" + fdpacnText)
			changeFdpAcn(fdpacnVal);
			$("#FDPACN_TEXT").val(fdpacnText);
		}
		
		function changeFdpAcn(FDPACN){
				//宣告變數，資料類型為物件 
				var dataTerm = {}
				var options = {keyisval: false,selectID: '#FDPNUM'}
				// 先清空原有之"OPTION"內容,並加上一個 "---請選擇存單號碼---" 欄位
				$('#FDPNUM').empty();
				$("#FDPNUM").append($("<option></option>").attr("value", "#").text("<spring:message code="LB.Select_certificate_no" />"));
				//建立選單資料
				for (var key in TableJson) {
					if(FDPACN==TableJson[key].ACN)
					{
					dataTerm[TableJson[key].FDPNUM] = TableJson[key].FDPNUM;
					// fdpnumAmountTable[TableJson[key].FDPNUM] = TableJson[key].AMTFDP;
					}
				}
				//建立下拉選單 
				fstop.creatSelect(dataTerm, options);
				// 存單號碼
				$("#FDPNUM").addClass("validate[funcCall[validate_CheckSelect[<spring:message code= "LB.Certificate_no" />,FDPNUM,#]]]");
		}
	
		//轉出帳號change事件，要秀出可用餘額
		function acnoEvent() {
			$("#ACNO").change(function () {
				var acno = $('#ACNO :selected').val();
				console.log("acno>>" + acno);
				if(acno!='')getACNO_Data(acno);
				else $("#acnoIsShow").hide();
				$('#TransferType_01').trigger('click');
			});
		}
		//取得轉出帳號餘額資料
		function getACNO_Data(acno) {
			var options = {keyisval: true,selectID: '#ACNO'};
			uri = '${__ctx}' + "/NT/ACCT/TRANSFER/getACNO_Data_aj"
			console.log("getACNO_Data>>" + uri);
			rdata = {acno: acno};
			console.log("rdata>>" + rdata);
			fstop.getServerDataEx(uri, rdata, false, isShowACNO_Data);
		}
		//顯示轉出帳號餘額
		function isShowACNO_Data(data) {
			if (data != null && data.data.accno_data != null) {
				$("#acnoIsShow").show();
				var str = fstop.formatAmt(data.data.accno_data.BDPIBAL);//格式化金額
				console.log("str :" + str);
				$("#showText").html(str);
			} else {
				$("#acnoIsShow").hide();
			}
		}
		//轉出帳號切換事件
		function transferTypeChange(){
			$('input[type=radio][name=TransferType]').change(function () {
				console.log(this.value);
				if (this.value == 'PD') {
					console.log('TransferType01');
					$("#ACNO").change();
					$("#ACNO").addClass("validate[required]")
					$("#OUTACN_NPD").removeClass("validate[required]");
				}
				else if (this.value == 'NPD') {
					console.log('TransferType02');
					$("#ACNO").val('');
					$("#acnoIsShow").hide();
					$("#OUTACN_NPD").addClass("validate[required]");
					$("#ACNO").removeClass("validate[required]")
				}
			});
		} 
		//	建立約定轉出帳號下拉選單
		function creatOutACNO() {
			var options = {keyisval: false,selectID: '#ACNO'};
			uri = '${__ctx}' + "/NT/ACCT/TRANSFER/getOutAcno_aj"
			console.log("getSelectData>>" + uri);
			rdata = {type: 'acno'};
			console.log("rdata>>" + rdata);
			data = fstop.getServerDataEx(uri, rdata, false);
			if (data != null && data.result == true) {
				fstop.creatSelect(data.data, options);
			}
		}
		
   		//copyTN
		function copyTN(){
			$("#CMMAILMEMO").val($("#CMTRMEMO").val());
		}
		//開啟通訊錄email選單
		function openAddressbook(){
			window.open('${__ctx}/NT/ACCT/TRANSFER/showAddressbook');
		}
		//轉帳金額必須是零存整付金額的倍數。>>> 設為10000的倍數 
// 		function validate_Amount(field, rules, i, options){
// 			var fdpnum = $('#FDPNUM :selected').val();存單號碼
// 			var fdpnumAmount  = fdpnumAmountTable[fdpnum];//存單總金額
// 			console.log("fdpnum>>" + fdpnum);
// 			console.log("fdpnumAmount>>" + fdpnumAmount);
// 			var amount = $('#AMOUNT').val(); 
// 			console.log("amount>>" + amount);
// 			if(amount>1500000){
// 				return options.allrules.max.alertText + 1500000;
// 			}
// 			if(amount%10000!=0){
// 				return options.allrules.installmentSavingAmount.alertText;
// 			}
// 		}

	</script>
</head>

<body>
<!-- 交易機制元件所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	<!--   IDGATE -->
	<%@ include file="../idgate_tran/idgateAlert.jsp" %>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 定存服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Current_Deposit_To_Time_Deposit" /></li>
    <!-- 零存整付按月繳存     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Periodical_Deposits_And_Lumpsum_Payment_monthly" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
	</div>
	<!-- content row END -->
	<!-- 		主頁內容  -->
	<main class="col-12">
		<section id="main-content" class="container">
				<!--  臺幣零存整付按月繳存 -->
			<h2>
				<spring:message code="LB.Periodical_Deposits_And_Lumpsum_Payment_monthly" />
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form method="post" id="formId" action="${__ctx}/NT/ACCT/TDEPOSIT/installment_saving_confirm">
				<input type="hidden" id="FDPACN_TEXT" name="FDPACN_TEXT" value="">
				<!-- 轉出帳號 -->
				<input type="hidden" id="OUTACN" name="OUTACN" value="">
				 <!--交易步驟 -->
				<div id="step-bar">
					<ul>
				<!--輸入資料 -->
						<li class="active"><spring:message code="LB.Enter_data"/></li>
				<!-- 確認資料 -->
						<li class=""><spring:message code="LB.Confirm_data"/></li>
				<!-- 交易完成 -->
						<li class=""><spring:message code="LB.Transaction_complete"/></li>
					</ul>
        		</div>
				<!-- 			表單顯示區 -->
				<div class="main-content-block row">
					<div class="col-12 tab-content" id="nav-tabContent">
						<div class="ttb-input-block tab-pane fade show active" id="nav-trans-now" role="tabpanel" aria-labelledby="nav-home-tab">
							<div class="ttb-message">
								<span>
								</span>
							</div>
							<!-- 轉出帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Payers_account_no" /></h4>
									</label>
								</span>
								<span class="input-block">
									<!-- 約定 轉出帳號 -->
									<div class="ttb-input">
										<label class="radio-block">
											<input type="radio" id="TransferType_01" name="TransferType" value="PD" checked>
											<spring:message code="LB.Designated_account" />
											<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input ">
										<select name="ACNO" id="ACNO" class="custom-select select-input half-input validate[required]">
											<!-- 請選擇約定帳號 -->
											<option value="">----<spring:message code="LB.Select_designated_account" />-----</option>
										</select>
										<!--可用餘額 -->
										<div id="acnoIsShow"style="display: none">
										<span class="input-unit">
											<spring:message code="LB.Available_balance"/>
											<span id="showText" class="input-unit "></span>
										</span>
										</div>
									</div>
									<!-- 非約定 -->
									<div class="ttb-input">
										<label class="radio-block">
											<spring:message code="LB.Out_nondesignated_account" />
											<input type="radio" id="TransferType_02" name="TransferType" value="NPD" onclick="listReaders();" />
											<span class="ttb-radio"></span>
										</label>
									</div>
									<!-- (晶片金融卡主帳號) -->
									<div class="ttb-input">
										<input type="text" name="OUTACN_NPD" id="OUTACN_NPD" class="text-input" readonly="readonly" value="" />
										<span class="input-subtitle subtitle-color">
											<spring:message code="LB.X2361" />
										</span>
									</div>
								</span>
							</div>
							<!-- 存單帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Account_no" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<select id="FDPACN" name="FDPACN" class="custom-select select-input half-input validate[funcCall[validate_CheckSelect[<spring:message code= "LB.Account_no" />,FDPACN,#]]]" onchange="changeDisplay()">
												<!-- 請選擇存單帳號-->
											<option value="#">---<spring:message code="LB.Select_certificate_account" />---</option>
											<c:forEach var="dataList" items="${ installment_saving.data.fdpAcnSet}">
												<option value='${dataList.ACN}'> ${dataList.TEXT}</option>
											</c:forEach>
										</select>
									</div>
								</span>
							</div>
							<!-- 存單號碼 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Certificate_no" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<select name="FDPNUM" id="FDPNUM" class="custom-select select-input half-input">
										</select>
									</div>
								</span>
							</div>
							<!-- 轉帳金額 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Amount" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<input type="text" id="AMOUNT" name="AMOUNT" class="text-input" size="8" maxlength="8" value="">
										<!--元 -->
										<span class="input-unit">
										<spring:message code="LB.Dollar"/>
										</span>
										<!-- 不在畫面上顯示的span -->
										<span name="hideblock" >
											<!-- 驗證用的input -->
											<input id="H_AMOUNT" name="H_AMOUNT" type="text" class=" text-input validate[required]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</span>
									</div>
								</span>
							</div>
							<!-- 交易備註 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Transfer_note" /></h4>
									</label>
								</span> 
								<!--(可輸入20字的用途摘要，您可於「轉出紀錄查詢」功能中查詢) -->
								<span class="input-block">
									<div class="ttb-input">
										<input type="text" id="CMTRMEMO" name="CMTRMEMO" class="text-input" maxlength="20"  value="">
										<span class="input-unit"></span>
										<span class="input-remarks"><spring:message code="LB.Transfer_note_note" /></span>
									</div>
								</span>
							</div>
							<!-- 轉出成功 Email通知 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.X0479" />
										：</h4>
									</label>
								</span>
								<span class="input-block">
									<!--通知本人 -->
									<div class="ttb-input">
										<span class="input-subtitle subtitle-color">
											<spring:message code="LB.Notify_me"/>
											：
										</span>
										<span class="input-subtitle subtitle-color">
											${sessionScope.dpmyemail}
										</span>
									</div>
									<!--另通知 -->
									<div class="ttb-input">
										<span class="input-subtitle subtitle-color">
											<spring:message code="LB.Another_notice"/>
											：
										</span>
									</div>
											<!-- 通訊錄 -->
									<div class="ttb-input">
										<!--非必填 -->
										<spring:message code="LB.Not_required" var="notrequired"></spring:message>
										<input class="text-input" type="text" id="CMTRMAIL" name="CMTRMAIL" maxlength="500" placeholder="${notrequired}">
										<span class="input-unit"></span>
										<button class="btn-flat-orange" type="button" onclick="openAddressbook()">
											<spring:message code="LB.Address_book" />
										</button>
										<!-- 不在畫面上顯示的span -->
											<span name="hideblock" >
												<!-- 驗證用的input -->
												<input id="H_CMTRMAIL" name="H_CMTRMAIL" type="text" class="text-input validate[funcCall[validate_EmailCheck[CMTRMAIL]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
									</div>
									<!--摘要內容-->
									<div class="ttb-input">
										<spring:message code="LB.Summary" var="summary"></spring:message>
										<input type="text" id="CMMAILMEMO" name="CMMAILMEMO"  class="text-input"  maxlength="20" placeholder="${summary}">
										<span class="input-unit">
										</span>
										 <!-- 同交易備註 -->
        								<button class="btn-flat-orange" type="button" onclick="copyTN()">
                							<spring:message code="LB.As_transfer_note" />
            							</button>
									</div>
								</span>
							</div>
						</div>
						<!--button 區域 -->
							<!--重新輸入 -->
							<spring:message code="LB.Re_enter" var="cmRest"></spring:message>
							<input type="reset" name="CMRESET" id="CMRESET" value="${cmRest}" class="ttb-button btn-flat-gray">
							<!-- 確定 -->
							<spring:message code="LB.Confirm" var="cmSubmit"></spring:message>
							<input type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" class="ttb-button btn-flat-orange">
						<!--button 區域 -->
					</div>
				</div>
				<div class="text-left">
					<!-- 		說明： -->
					<ol class="description-list list-decimal">
					    <p><spring:message code="LB.Description_of_page"/></p>
						<li><span><spring:message code="LB.Installment_saving_P1_D1"/></span></li>
						<li><span><spring:message code="LB.Installment_saving_P1_D2"/></span></li>
					</ol>
				</div>
			</form>
		</section>
		<!-- 		main-content END -->
	</main>
	<%@ include file="../index/footer.jsp"%>
</body>

</html>