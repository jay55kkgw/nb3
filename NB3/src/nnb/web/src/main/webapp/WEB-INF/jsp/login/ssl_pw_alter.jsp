<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>	
</head>
<script type="text/JavaScript">
	$(document).ready(function() {
		init();
	});
	  
	function init(){
		// 如果是從同時變更簽入、交易密碼一成功一失敗來先跳提示訊息
		var acradio = '${ACRADIO}';
		if("pw" == acradio){
			//alert("<spring:message code= "LB.Alert134" />"); // 交易密碼已變更成功，需變更簽入密碼
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert134' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
		}  else if ("ssl" == acradio) {
			//alert("<spring:message code= "LB.Alert135" />"); // 簽入密碼已變更成功，需變更交易密碼
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert135' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
		}
		
		$("#formId").validationEngine({binded:false,promptPosition: "inline" });
	
		$("#pageshow").click(function () {
			console.log("submit~~");
			if (!$('#formId').validationEngine('validate')) {
				e.preventDefault();
			} else {
				$("#formId").validationEngine('detach');

				// 送出前先把變更的密碼做加密塞入
				var OLDTRANPIN = $('#OLDTRANPIN').val();
				var NEWTRANPIN = $('#NEWTRANPIN').val();
				$('#PINOLD').val(pin_encrypt(OLDTRANPIN)); // 舊交易密碼加密
				$('#PINNEW').val(pin_encrypt(NEWTRANPIN)); // 新交易密碼加密

				initBlockUI();//遮罩
				$("#formId").submit();
			}
		});
	}
</script>
<body>
	<div class="content row">
		<main class="col-12">	
			<section id="main-content" class="container">
				<form method="post" id="formId" action="${__ctx}/ssl_pw_alter">
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<div style="text-align: center">
									<h2 style="color:#ed6d00;"><spring:message code="LB.Modify_trans_pwd_note" /></h2>
								</div>
								
								<br/><br/><br/>
								
					        	<!-- 舊交易密碼 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4><spring:message code="LB.SSL_password_1" /></h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="hidden" id="PINOLD" name="PINOLD" />
											<input type="password" name="OLDTRANPIN" id="OLDTRANPIN" class="text-input validate[required, custom[onlyLetterNumber]] " size="10"  minlength="6" maxlength="8" value="">
										</div>
									</span>
								</div>
								
								<!--新交易密碼 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4><spring:message code="LB.New_SSL_Password"/></h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="hidden" id="PINNEW" name="PINNEW" />
											<input type="password" name = "NEWTRANPIN" id = "NEWTRANPIN" class="text-input  validate[required , recolumn[CK,OLDTRANPIN, NEWTRANPIN, NEWPIN], custom[onlyLetterNumber]] " size="10"  minlength="6" maxlength="8" value="">
											<p><spring:message code="LB.User_Change_password_note" /></p>
										</div>
									</span>
								</div>
								
								<!-- 確認新交易密碼 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4><spring:message code="LB.Confirm_SSL_password" /></h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="password" name = "RENEWTRANPIN" id = "RENEWTRANPIN" class="text-input  validate[required ,recolumn[PW, OLDTRANPIN, NEWTRANPIN, RENEWTRANPIN], custom[onlyLetterNumber]] " size="10"  minlength="6" maxlength="8" value="">
											<p><spring:message code="LB.Please_enter_new_SSL_password_again" /></p>
										</div>
									</span>
								</div>							
							</div>

							<input id="logout" name="logout" type="button" class="ttb-button btn-flat-orange" 
								onClick="fstop.logout( '${__ctx}'+'/logout_aj' , '${__ctx}'+'/login')" value="<spring:message code="LB.Logout" />" />
							<input id="reset" name="reset" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" />	
							<input id="pageshow" name="pageshow" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />						
						
						</div>
					</div>
				</form>

				<div class="text-left">
					
					<ol class="list-decimal text-left description-list">
					<p><spring:message code="LB.Description_of_page" /> </p>
						<li><spring:message code="LB.Ssl_pw_alter_P1_D1" /></li>
					</ol>
				</div>
				
			</section>
		</main>
	</div>
</body>
</html>
