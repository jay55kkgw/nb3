<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>

<script type="text/javascript">
$(document).ready(function(){

	//`initFootable();
	setTimeout("initBlockUI()",10);
	setTimeout("initDataTable()",100);
	setTimeout("unBlockUI(initBlockId)",500);
	
	$("#printbtn").click(function(){
		var i18n = new Object();
		i18n['jspTitle']='D/A、D/P<spring:message code= "LB.W0180" />'
		var params = {
				"jspTemplateName":"f_collection_query_N563_print",
				"jspTitle":i18n['jspTitle'],
				"CMQTIME":"${N563_RESULT.data.CMQTIME}",
				"CMPERIOD":"${N563_RESULT.data.CMPERIOD}",
				"CMRECNUM":"${N563_RESULT.data.CMRECNUM}",
			
		}
		openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);

	});
	
	$('#CMBACK').click(function() {
		var action = '${__ctx}/FCY/ACCT/f_collection_query_type' 
		$('#formId').attr("action",action);
		$('#formId').submit();
	})
	
});
function selectBlockUI() {
	//change後遮罩啟動
	setTimeout("initBlockUI()",10);
	// 開始執行動作
	setTimeout("selectAction()",20);
	// 解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
		
}
function selectAction(){
	if($('#actionBar').val()=="excel"){
		$("#downloadType").val("OLDEXCEL");
		$("#templatePath").val("/downloadTemplate/f_collection_query_N563.xls");
	}else if ($('#actionBar').val()=="txt"){
		$("#downloadType").val("TXT");
		$("#templatePath").val("/downloadTemplate/f_collection_query_N563.txt");
	}
	//ajaxDownload("${__ctx}/ajaxDownload","DownloadformId","finishAjaxDownload()");
	$("#DownloadformId").attr("target", "");
	$("#DownloadformId").submit();
	$('#actionBar').val("");
}
function finishAjaxDownload(){
	$("#actionBar").val("");
	unBlockUI();
}

</script>
</head>
<body>
   	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 帳戶查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Account_Inquiry" /></li>
    <!-- 進口/出口託收查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0176" /></li>
    <!-- D/A、D/P出口託收     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0030" /></li>
		</ol>
	</nav>



	
	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
	

	<main class="col-12">
		<section id="main-content" class="container"><!-- 主頁內容  -->
			<!--進口託收查詢-->
			<h2><spring:message code="LB.X0030" /></h2>
			
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="${__ctx}/FCY/ACCT/f_collection_query_result">
			<input type="hidden" name="FUNC" value="N563">
			<input type="hidden" name="USERDATA" value="${N563_RESULT.data.USERDATA}">
			<div class="print-block">
					<select class="minimal" id="actionBar" onchange="selectBlockUI()">
						<!-- 下載-->
						<option value=""><spring:message
								code="LB.Downloads" /></option>
						<!-- 下載Excel檔-->
						<option value="excel"><spring:message
								code="LB.Download_excel_file" /></option>
						<!-- 下載為txt檔-->
						<option value="txt"><spring:message
								code="LB.Download_txt_file" /></option>
					</select>
				</div>
			<br/>
			<br/>
				<div class="main-content-block row">
					<div class="col-12 tab-content">
							<ul class="ttb-result-list">
								<li>
									<!-- 查詢時間  -->
									<h3>
										<spring:message code="LB.Inquiry_time" />
									</h3>
									<p>
										${N563_RESULT.data.CMQTIME}
									</p>
								</li>
								<li>
									<!--查詢期間 -->
									<h3>
										<spring:message code="LB.Inquiry_period_1" />
									</h3>
									<p>
										${N563_RESULT.data.CMPERIOD}
									</p>
								</li>
								<li>
									<h3>
										<!-- 資料總數  -->
										<spring:message code="LB.Total_records" />
									</h3>
									<p>
										${N563_RESULT.data.CMRECNUM}
										<spring:message code="LB.Rows" />
									</p>
								</li>
							</ul>
							<table  class="stripe table-striped ttb-table dtable" data-toggle-column="first">
								<thead>
					            <tr>
					                <th data-title='<spring:message code="LB.W0186"/>'>
										<!-- 託收日期  -->
					               		<spring:message code="LB.W0186" />
					                </th>
					                <th data-title='<spring:message code="LB.W0183"/>'>
					                	<!-- 託收編號  -->
					               		<spring:message code="LB.W0183" />
					                </th>
					                <th data-title='<spring:message code="LB.W0237"/>' data-breakpoints="xs sm">
										<!-- 託收幣別  -->
					               		<spring:message code="LB.W0237" />
					                </th>
					                <th data-title='<spring:message code="LB.W0189"/>' data-breakpoints="xs sm">
										<!-- 託收金額  -->
					               		<spring:message code="LB.W0189" />
					                </th>
					                <th data-title='<spring:message code="LB.W0190"/>' data-breakpoints="xs sm">
										<!--天期 -->
					               		<spring:message code="LB.W0190" />
					                </th>
					                <th data-title='<spring:message code="LB.W0191"/>' data-breakpoints="xs sm">
										<!-- 進口商  -->
					               		<spring:message code="LB.W0191" />
					                </th>
					                <th data-title='<spring:message code="LB.W0192"/>' data-breakpoints="xs sm">
										<!-- 國外承兌日  -->
					               		<spring:message code="LB.W0192" />
					                </th>
					                <th data-title='<spring:message code="LB.W0193"/>' data-breakpoints="xs sm">
										<!-- 國外預定到期日  -->
					               		 <spring:message code="LB.W0193" />
					                </th>
					                <th data-title="<spring:message code="LB.W0194"/>" data-breakpoints="xs sm">
										<!-- 本行入帳日 -->
					               		<spring:message code="LB.W0194" />
					                </th>
					            </tr>
					            </thead>
					            <tbody>
					            <c:forEach var="dataTable" items="${N563_RESULT.data.REC}">
					            <tr>
					                 <td class="text-center">
										<!-- 託收日期  -->
					               		${dataTable.RVALDATE}
					                </td>
					                <td class="text-center">
					                	<!-- 託收編號  -->
					               		${dataTable.RREFNO}
					                </td>
					                <td class="text-center">
										<!-- 託收幣別  -->
					               		${dataTable.RBILLCCY}
					                </td>
					                <td  class="text-right">
										<!-- 託收金額  -->
					               		${dataTable.RBILLAMT}
					                </td>
					                <td class="text-center">
					                <!--天期 -->
<%-- 					                <c:if test="${dataTable.RTENOR1 eq '-'}"> --%>
					                	${dataTable.RTENOR1}
<%-- 					                </c:if> --%>
<%-- 					                <c:if test="${dataTable.RTENOR1 != '-'}"> --%>
<%-- 					                	${dataTable.RTENOR1}<spring:message code="LB.W1121" /> --%>
<%-- 					                </c:if> --%>
					                </td>
					                <td class="text-center">
										<!-- 進口商  -->
					               		${dataTable.RDRAWEE}
					                </td>
					                <td class="text-center">
										<!-- 國外承兌日  -->
					               		${dataTable.RACCEPDT}
					                </td>
					                <td class="text-center">
										<!-- 國外預定到期日  -->
					               		${dataTable.RMATDAT1}
					                </td>
					                <td class="text-center">
										<!-- 本行入帳日 -->
					               		${dataTable.RPAYDATE}
					                </td>
					            </tr>
					            </c:forEach>
					            </tbody>
							</table>
							<br>
						<div class="text-left">
							<p><spring:message code="LB.W0209" />：</p>
							<c:forEach var="dataTable2" items="${N563_RESULT.data.CRY}">
							 	<p>&nbsp;${dataTable2.AMTRBILLCCY}&nbsp;${dataTable2.FXTOTAMT}&nbsp;<spring:message code="LB.W0158" />&nbsp;${dataTable2.FXTOTAMTRECNUM}&nbsp;<spring:message code="LB.Rows" /></p>
							</c:forEach>
							<c:if test="${N563_RESULT.data.TOPMSG eq 'OKOV'}">
							<div>
								<spring:message code="LB.F_Collection_Query_N563_P2_D1" />
								<input type="button" class="ttb-sm-btn btn-flat-orange"  id="Query" value="<spring:message code="LB.X0151" />" onclick="QueryNext();"/>
							</div>
							</c:if>
							
						</div>
						<br>
						   <!--回上頁 -->
                         <spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>
                         <input type="button" name="CMBACK" id="CMBACK" value="${cmback}" class="ttb-button btn-flat-gray">
						<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
					</form>
					<form id="DownloadformId" action="${__ctx}/download" method="post">
<!-- 						下載用 -->
						<input type="hidden" name="downloadFileName" value="<spring:message code="LB.X0030" />"/>
						<input type="hidden" name="CMQTIME" value="${N563_RESULT.data.CMQTIME}"/>
						<input type="hidden" name="CMRECNUM" value="${N563_RESULT.data.CMRECNUM}"/>
						<input type="hidden" name="CMPERIOD" value="${N563_RESULT.data.CMPERIOD}"/>
						<input type="hidden" name="CURRENCY_TOTAL_TXT" value="${N563_RESULT.data.DownloadStringTXT}"/>
						<input type="hidden" name="CURRENCY_TOTAL_EXCEL" value="${N563_RESULT.data.DownloadStringEXCEL}"/>
						<input type="hidden" name="downloadType" id="downloadType"/> 					
						<input type="hidden" name="templatePath" id="templatePath"/>
						<input type="hidden" name="hasMultiRowData" value="false"/> 
						<input type="hidden" name="hasMultiRowData" value="false"/> 	
<!-- 						EXCEL下載用 -->
						<!-- 	EXCEL下載用 -->
						<input type="hidden" name="headerRightEnd" value="9"/>
						<input type="hidden" name="headerBottomEnd" value="6"/>
						<input type="hidden" name="rowStartIndex" value="7"/>
						<input type="hidden" name="rowRightEnd" value="9"/>
						<input type="hidden" name="footerStartIndex" value="9" />
						<input type="hidden" name="footerEndIndex" value="11" />
						<input type="hidden" name="footerRightEnd" value="10" />
						
<!-- 						TXT下載用 -->
						<input type="hidden" name="txtHeaderBottomEnd" value="7"/>
						<input type="hidden" name="txtHasRowData" value="true"/>
						<input type="hidden" name="txtHasFooter" value="true"/>
					</form>
				</div>
		</section>
	</main><!-- main-content END --> 
	</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>
