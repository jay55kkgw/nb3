<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {
		// 將.table變更為footable
		setTimeout("initDataTable()",100);
		//initFootable();
		init();
	});
	
	function init() {
        //列印
    	$("#printbtn").click(function(){
			var params = {
				"jspTemplateName":"oversea_balance_print",
				"jspTitle":'<spring:message code= "LB.W1007" />',
				"DATE":"${oversea_balance_result.data.DATE}",
				"TOTRECNO":"${oversea_balance_result.data.TOTRECNO}",
				"TIME":"${oversea_balance_result.data.TIME}",
				"CUSNAME":"${oversea_balance_result.data.CUSNAME}"
			};
			openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
		});
		//上一頁按鈕
		$("#CMBACK").click(function() {
			var action = '${__ctx}/NT/ACCT/BOND/bond_balance';
			$('#back').val("Y");
			$("#formId").attr("action", action);
			initBlockUI();
			$("#formId").submit();
		});
		
		//繼續查詢
    	$("#CMCONTINU").click(function (e) {
			e = e || window.event;
			console.log("submit~~");

			$("#formId").validationEngine('detach');
			initBlockUI(); //遮罩
			$("#formId").attr("action", "${__ctx}/NT/ACCT/BOND/bond_balance_result");
			$("#formId").submit();
		});
    }
	
	//下拉式選單
 	function formReset() {
 		if ($('#actionBar').val()=="reEnter"){
	 		document.getElementById("formId").reset();
	 		$('#actionBar').val("");
 		}
	}
	
 	function processQuery(CDNO) {
		//配息查詢
		   var sCDNO = CDNO;
		   if(!(sCDNO.length==11)){
			   sCDNO = "0"+sCDNO;
			}		
		  	$("#formId").attr("action", "${__ctx}/FUND/QUERY/oversea_data_result_dividend");
  			$("#CDNO").val(sCDNO);
			$("#formId").submit();	
		return false; 		
 	} 	

</script>
</head>
	<body>
		<!-- header     -->
		<header>
			<%@ include file="../index/header.jsp"%>
		</header>
	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金/海外債券     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0368" /></li>
    <!-- 帳務查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Inquiry_Service" /></li>
    <!-- 海外債券餘額/損益查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1007" /></li>
		</ol>
	</nav>

		<!-- menu、登出窗格 -->
		<div class="content row">
			<%@ include file="../index/menu.jsp"%>
			<!-- 	快速選單內容 -->
			<!-- content row END -->
			<main class="col-12">
				<section id="main-content" class="container">
					<!-- 主頁內容  -->
					<h2><spring:message code="LB.W1007" /></h2>
					<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				   <form id="formId" method="post" action="">
						<input type="hidden" id="CDNO" name="CDNO" value="" /> 
						<input type="hidden" id="PREVIOUS" name="PREVIOUS" value="oversea_balance" /> 												
					</form>
						<div class="main-content-block row">
							<div class="col-12  tab-content">
								<ul class="ttb-result-list">
									<li><!-- 查詢時間 -->
										<h3><spring:message code="LB.Inquiry_time" /></h3>
	                                	<p>${oversea_balance_result.data.DATE} ${oversea_balance_result.data.TIME}</p>
									</li>
									<li><!-- 資料總數 -->
										<h3><spring:message code="LB.Total_records" /></h3>
	                                	<p>${oversea_balance_result.data.TOTRECNO}&nbsp;<spring:message code="LB.Rows"/></p>
									</li>
								</ul>
								<!-- 海外債券餘額及損益查詢 -->
								
		                        <ul class="ttb-result-list">
		                            <li>
		                                <h3><spring:message code="LB.Name" /></h3>
		                                <p>${oversea_balance_result.data.CUSNAME}</p>
		                            </li>
		                        </ul>
								<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
									<thead>
<%-- 										<tr> --%>
<!-- 											姓名   -->
<%-- 											<th><spring:message code="LB.Name" /></th> --%>
<%-- 											<th class="text-left" colspan="6">${oversea_balance_result.data.CUSNAME}</th> --%>
<%-- 										</tr> --%>
										<tr>
											<!--申購日期/信託帳號  -->
											<th><spring:message code="LB.W1009" /><BR><spring:message code="LB.W0944" /></th>
											<!--債券代碼/債券名稱  -->
									      	<th><spring:message code="LB.W1011" /><BR><spring:message code="LB.W1012" /></th>
									      	<!--信託金額/面額  -->
									      	<th data-breakpoints="xs"><spring:message code="LB.W0026" /><BR><spring:message code="LB.W1014" /></th>
									      	<!--參考贖回報價/報價日期  -->
									      	<th data-breakpoints="xs"><spring:message code="LB.X2577" /><br><spring:message code="LB.W1016" /></th>      	
									      	<!--參考現值/計價幣別  -->
									      	<th data-breakpoints="xs sm"><spring:message code="LB.W0915" /><br><spring:message code="LB.W0909" /></th>
									      	<!--累計配息金額/前手息  -->
									      	<th data-breakpoints="xs sm"><spring:message code="LB.W0918" /><br><spring:message code="LB.W1018" /></th>
									      	<!--參考投資損益/參考含息報酬率  -->
									      	<th><spring:message code="LB.W1019" /><BR><spring:message code="LB.W0919" /></th>
									      	<!--詳細配息/資料明細  -->
									      	<th><spring:message code="LB.X2578" /><br><spring:message code="LB.X2579" /></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="dataList" items="${oversea_balance_result.data.REC}">
											<c:if test="${ dataList.O18.equals('1')  }">
												<tr>
													<td class="text-center">${dataList.O15}<br>${dataList.HO01}</td>
													<td class="text-center">${dataList.O02}<br>${dataList.O03}</td>
													<td class="text-right">${dataList.O07}<br>${dataList.O05}</td>
													<td class="text-center">${dataList.O09}<br>${dataList.O10}</td>
													<td class="text-center">${dataList.O12}<br>${dataList.O08}</td>
													<td class="text-right">${dataList.O11}<br>${dataList.O16}</td>
													<td class="text-right">${dataList.O17}<br>${dataList.O14}</td>
												    <td class="text-center">
      									          	<input type="button" class="ttb-sm-btn btn-flat-orange" name="AAA"value="<spring:message code="LB.X2219" />" onclick="processQuery('${dataList.O01}')"><br>     					
      								            	</td>
												</tr>
											</c:if>
										</c:forEach>
									</tbody>
								</table>
								
		                        <ul class="ttb-result-list">
		                            <li>
		                                <p><spring:message code="LB.X0393" /></p>
		                            </li>
		                        </ul>
								<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
									<thead>
<%-- 										<tr> --%>
<!-- 											姓名   -->
<%-- 											<th class="text-left" colspan="7"><spring:message code="LB.X0393" /></th> --%>
<%-- 										</tr> --%>
										<tr>
											<!--投資幣別  -->
											<th ><spring:message code="LB.W0908" /></th>
									      	<!--總信託金額  -->
									      	<th ><spring:message code="LB.W0933" /></th>
									      	<!--總參考現值  -->
									      	<th ><spring:message code="LB.W0934" /></th>
									      	<!--前手息  -->
									      	<th data-breakpoints="xs"><spring:message code="LB.W1018" /></th>
									      	<!--(不含息參考)投資損益報酬率  -->
									      	<th data-breakpoints="xs sm">(<spring:message code="LB.X2580" />)<br><spring:message code="LB.X2581" /><br><spring:message code="LB.W1196" /></th>
									      	<!--累計配息金額  -->
									      	<th data-breakpoints="xs"><spring:message code="LB.W0918" /></th>   
									      	<!--(含息參考)投資損益報酬率 -->
									      	<th >(<spring:message code="LB.X2582" />)<br><spring:message code="LB.X2581" /><br><spring:message code="LB.W1196" /></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="dataList" items="${oversea_balance_result.data.REC}">
											<c:if test="${ dataList.O18.equals('2')  }">
												<tr>
													<td class="text-center">${dataList.O08}</td>
													<td class="text-right">${dataList.O07}</td>
													<td class="text-right">${dataList.O12}</td>
													<td class="text-right">${dataList.O16}</td>
													<td class="text-right">${dataList.O20}<br>${dataList.O13}</td>
													<td class="text-right">${dataList.O11}</td>
													<td class="text-right">${dataList.O17}<br>${dataList.O14}</td>
												</tr>
											</c:if>
										</c:forEach>
									</tbody>
								</table>
								
								<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
		                        
							</div>
						</div>
						<!-- 說明： -->
				        <ol class="description-list list-decimal">
				        	<p><spring:message code="LB.Description_of_page"/></p>
				            <li><span>
								<spring:message code="LB.oversea_balance_P1_D1" /><br>
	                            	(1)<spring:message code="LB.oversea_balance_P1_D11" /><br>
	                            	(2)<spring:message code="LB.oversea_balance_P1_D12" />
							</span></li>
							<li><span><spring:message code="LB.oversea_balance_P1_D2" /></span></li>
				        </ol>
					
				</section>
			</main>
			<!-- 		main-content END -->
			<!-- 	content row END -->
		</div>
		<%@ include file="../index/footer.jsp"%>
	</body>
<c:if test="${showDialog == 'true'}">
		<%@ include file="../index/txncssslog.jsp"%>
		</c:if>
</html>