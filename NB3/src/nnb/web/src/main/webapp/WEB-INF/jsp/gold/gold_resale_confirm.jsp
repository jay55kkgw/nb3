<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<!--舊版驗證-->
<script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
<!--交易機制所需JS-->
<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	
	countDown();
	
	$("#CMSUBMIT").click(function(){
		var FGTXWAY = $("input[name=FGTXWAY]:checked").val();
		
		//交易密碼
		if(FGTXWAY == "0"){
			if(!CheckPuzzle("CMPWD")){
				return false;
			}
			$("#CMPASSWORD").val($("#CMPWD").val());
			
			var PINNEW = pin_encrypt($("#CMPASSWORD").val());
			$("#PINNEW").val(PINNEW);
			initBlockUI();
			
			$("#formId").submit();
		}
		//IKEY
		else if(FGTXWAY == "1"){
			var jsondc = $("#jsondc").val();
			
			//IKEY驗證流程
			uiSignForPKCS7(jsondc);
		}
		else if(FGTXWAY == "7"){
			//IDGATE認證		 
	        idgatesubmit= $("#formId");		 
	        showIdgateBlock();		 
		}
	});
	$("#resetButton").click(function(){
		$("#CMPWD").val("");
	});
	$("#backButton").click(function(){
		location.href = "${__ctx}/GOLD/TRANSACTION/gold_resale";
	});
});
	
var sec = 180;
function countDown(){
	$("#CountDown").html(sec);		   		

	sec--;

	if(sec <= -1){
		location.href = "${__ctx}/GOLD/TRANSACTION/gold_resale";
	}	   	  	 	
	//倒數計時
	else{
		setTimeout("countDown()",1000);		   		
	}	
}
</script>
</head>
<body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
    <!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %>  
	<!--header-->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 黃金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2272" /></li>
    <!-- 黃金回售     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1512" /></li>
		</ol>
	</nav>

	<!--左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!--快速選單及主頁內容-->
		<main class="col-12"> 
			<!--主頁內容-->
			<section id="main-content" class="container">
<!-- 黃金回售 -->
				<h2><spring:message code= "LB.W1512" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form id="formId" method="post" action="${__ctx}/GOLD/TRANSACTION/gold_resale_result">
                	<input type="hidden" name="ADOPID" value="N09002"/>
               		<input type="hidden" name="CMTRANPAGE" value="1"/>
               		<input type="hidden" name="TRNCOD" value="02"/>
               		<input type="hidden" name="PRICE" value="${bsData.PRICE}"/>
               		<input type="hidden" name="TRNAMT" value="${bsData.TRNAMT}"/>
               		<input type="hidden" name="FEEAMT1" value="${bsData.FEEAMT1}"/>
               		<input type="hidden" name="FEEAMT2" value="${bsData.FEEAMT2}"/>
               		<input type="hidden" name="TRNAMT2" value="${bsData.TRNAMT2}"/>
               		<input type="hidden" name="SVACN" value="${bsData.SVACN}"/>
					<input type="hidden" name="ACN" value="${bsData.ACN}"/>
					<input type="hidden" name="TRNGD" value="${bsData.TRNGD}"/>
					<input type="hidden" name="SELLFLAG" value="${bsData.SELLFLAG}"/>
					<!--交易機制所需欄位-->
					<input type="hidden" id="jsondc" name="jsondc" value="${jsondc}"/>
					<input type="hidden" id="ISSUER" name="ISSUER"/>
					<input type="hidden" id="ACNNO" name="ACNNO"/>
					<input type="hidden" id="TRMID" name="TRMID"/>
					<input type="hidden" id="iSeqNo" name="iSeqNo"/>
					<input type="hidden" id="ICSEQ" name="ICSEQ"/>
					<input type="hidden" id="TAC" name="TAC"/>
					<input type="hidden" id="pkcs7Sign" name="pkcs7Sign"/>
					<input type="hidden" id="PINNEW" name="PINNEW"/>
					<input type="hidden" id="CMPASSWORD" name="CMPASSWORD"/>
	                <div class="main-content-block row">
	                    <div class="col-12 tab-content">
	                        <div class="ttb-input-block">
	                        	<div class="ttb-message">
<!-- 請確認回售資料，並於三分鐘內執行確定本筆交易 － 時間： XX 秒 -->
	                        		<span><spring:message code= "LB.W1523" /> － <spring:message code= "LB.W1488" />：<font id="CountDown" color="red"></font> <spring:message code= "LB.D0467_1" /></span>
	                        	</div>
								<!--交易日期-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
										<label>
<!-- 交易日期 -->
	                                        <h4><spring:message code= "LB.D0450" /></h4>
	                                    </label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
	                                       	<span>${bsData.CMQTIME}</span>
										</div>
									</span>
								</div>
								<!--黃金轉出帳號-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                	<label>
<!-- 黃金轉出帳號 -->
	                                        <h4><spring:message code= "LB.W1515" /></h4>
	                                    </label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
	                                       	<span>${bsData.ACN}</span>
										</div>
									</span>
								</div>
								<!--台幣轉入帳號-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
		                                <label>
											<h4><spring:message code= "LB.W1518" /></h4>
										</label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
	                                       	<span>${bsData.SVACN}</span>
										</div>
									</span>
								</div>
								<!--賣出公克數-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
										<label>
<!-- 賣出公克數 -->
	                                        <h4><spring:message code= "LB.W1519" /></h4>
	                                    </label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
	                                       	<span>${bsData.TRNGDFormat}</span>
<!-- 公克 -->
											<span class="ttb-unit"><spring:message code= "LB.W1435" /></span>
										</div>
									</span>
								</div>
								<!--牌告單價-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
										<label>
<!-- 牌告單價 -->
	                                        <h4><spring:message code= "LB.W1524" /></h4>
	                                    </label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
										<!-- 新台幣 -->
	                                       	<span class="ttb-unit"><spring:message code= "LB.NTD" /></span>
											<span>${bsData.PRICEFormat}</span>
<!-- 元 -->
<!-- 公克 -->
											<span class="ttb-unit"><spring:message code= "LB.W1511" /></span>
										</div>
									</span>
								</div>
								<!--回售金額-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                	<label>
<!-- 回售金額 -->
											<h4><spring:message code= "LB.W1527" /></h4>
	                                    </label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
										<!-- 新台幣 -->
	                                       	<span class="ttb-unit"><spring:message code= "LB.NTD" /></span>
											<span>${bsData.TRNAMTFormat}</span>
<!-- 元 -->
											<span class="ttb-unit"><spring:message code= "LB.Dollar" /></span>
										</div>
									</span>
								</div>
								<c:if test="${bsData.FEEAMT1Format != '0'}">
		                            <div class="ttb-input-item row">
		                                <span class="input-title">
		                                	<label>
<!-- 定期投資扣款失敗 -->
<!-- 累計手續費 -->
												<h4><spring:message code= "LB.W1529" /><br/><spring:message code= "LB.W1530" /></h4>
		                                    </label>
										</span>
		                                <span class="input-block">
											<div class="ttb-input">
											<!-- 新台幣 -->
		                                       	<span class="ttb-unit"><spring:message code= "LB.NTD" /></span>
												<span>${bsData.FEEAMT1Format}</span>
<!-- 元 -->
												<span class="ttb-unit"><spring:message code= "LB.D0509" /></span>
											</div>
										</span>
									</div>
								</c:if>
								<c:if test="${bsData.FEEAMT2Format != '0'}">
									<div class="ttb-input-item row">
		                                <span class="input-title">
		                                	<label>
<!-- 黃金撲滿扣款失敗 -->
<!-- 累計手續費 -->
												<h4><spring:message code= "LB.W1531" /><br/><spring:message code= "LB.W1530" /></h4>
		                                    </label>
										</span>
		                                <span class="input-block">
											<div class="ttb-input">
		                                       	<span class="ttb-unit"><spring:message code= "LB.NTD" /></span>
												<span>${bsData.FEEAMT2Format}</span>
<!-- 元 -->
												<span class="ttb-unit"><spring:message code= "LB.Dollar" /></span>
											</div>
										</span>
									</div>
								</c:if>
								<div class="ttb-input-item row">
	                                <span class="input-title">
	                                	<label>
	                                	<!-- 存入台幣金額 -->
											<h4><spring:message code= "LB.W1532" /></h4>
	                                    </label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
										<!-- 新台幣 -->
	                                       	<span class="ttb-unit"><spring:message code= "LB.NTD" /></span>
											<span>${bsData.TRNAMT2Format}</span>
<!-- 元 -->
											<span class="ttb-unit"><spring:message code= "LB.Dollar" /></span>
										</div>
									</span>
								</div>
								<!--交易機制-->
	                            <div class="ttb-input-item row">
									<span class="input-title">
										<label>
	                                        <h4><spring:message code="LB.Transaction_security_mechanism"/></h4>
	                                    </label>
	                                </span>
	                                <span class="input-block" >
	                                    <div class="ttb-input">
											<label class="radio-block"><spring:message code="LB.SSL_password"/> 
	                                            <input type="radio" name="FGTXWAY" id="CMSSL" value="0" checked/>
	                                            <span class="ttb-radio"></span>
	                                        </label>
	                                    </div>
										<div class="ttb-input">
											<input type="password" class="text-input" name="CMPWD" id="CMPWD" size="8" maxlength="8"/>
										</div>
										<!--使用者是否可以使用IKEY-->
										<c:if test = "${XMLCOD == '2' || XMLCOD == '4' || XMLCOD == '5' || XMLCOD == '6'}">
	                                    	<div class="ttb-input">
	                                       		<label class="radio-block"><spring:message code="LB.Electronic_signature"/>
	                                            	<input type="radio" name="FGTXWAY" id="CMIKEY" value="1"/>
	                                            	<span class="ttb-radio"></span>
	                                        	</label>
	                                    	</div>
	                                    </c:if>
										<div class="ttb-input" name="idgate_group" style="display:none" onclick="hideCapCode('Y')">		 
	                                       <label class="radio-block">裝置推播認證(請確認您的行動裝置網路連線是否正常，及推播功能是否已開啟)
		                                       <input type="radio" id="IDGATE" 	name="FGTXWAY" value="7"> 
		                                       <span class="ttb-radio"></span>
	                                       </label>		 
	                                   </div>
	                                </span>
	                            </div>
	                        </div>
	                       
<!-- 回上頁 -->
	                  			<input type="button" id="backButton" value="<spring:message code= "LB.Back_to_previous_page" />" class="ttb-button btn-flat-gray"/>
<!-- 確定 -->
								<input type="button" id="CMSUBMIT" value="<spring:message code= "LB.Confirm" />" class="ttb-button btn-flat-orange"/>
	                		
	                    </div>
	                </div>
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>