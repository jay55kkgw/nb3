<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
   <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">    
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    
</head>
<script type="text/javascript">
	$(document).ready(function () {
		
		//表單驗證
		$("#formId").validationEngine({
			binded : false,
			promptPosition : "inline"
		});
		$("#CMSUBMIT").click(function(e){
			console.log("submit~~");
			//打開驗證隱藏欄位
			$("#hideblock_CheckDateScope").show();
			if (!$('#formId').validationEngine('validate')) {
				e.preventDefault();
			} else {
				initBlockUI();
				$("#formId").validationEngine('detach');
				$("form").submit();
			}
		});
		datetimepickerEvent();
		disableFunction();
		autoclickRAD2();
	});
	
	//日曆欄位參數設定
	function datetimepickerEvent(){
			$(".CMSDATE").click(function(event) {
				$('#CMSDATE').datetimepicker('show');
			});
			$(".CMEDATE").click(function(event) {
				$('#CMEDATE').datetimepicker('show');
			});
				
			jQuery('.datetimepicker').datetimepicker({
				timepicker:false,
				closeOnDateSelect : true,
				scrollMonth : false,
				scrollInput : false,
				format:'Y/m/d',
				lang: '${transfer}'
			});
	}
	function showVISACardNo(id){
		if (id=="RAD1"){
			$("#hid").hide();
		}
		if (id=="RAD2"){
			$("#hid").show();
		}
	}
	function autoclickRAD2(){
		if($('#RAD2').is('checked')){
			$('#RAD2').trigger("click");
		}
	}
	
	function disableFunction(){
		//有一般卡但有錯誤訊息
		if('${card_paid_history.data.n810MsgCode}'!='E091' && '${card_paid_history.data.n810MsgCode}'!=''){
			$('#RAD1').removeAttr("checked");
			$('#RAD1').attr("disabled",true);
			//只有一般卡的情況,把確認鈕鎖起來
			if($('#RAD2').length==0){
				$('#CMSUBMIT').removeClass("ttb-button btn-flat-orange");
				$('#CMSUBMIT').addClass("ttb-button btn-flat-gray");
				$('#CMSUBMIT').attr("disabled",true);
			}
		}
		//有VISA卡但有錯誤訊息
		if('${card_paid_history.data.n813MsgCode}'!='E091' && '${card_paid_history.data.n813MsgCode}'!=''){
			$('#RAD2').removeAttr("checked");
			$('#RAD2').attr("disabled",true);
			//只有VISA的情況,把確認鈕鎖起來
			if($('#RAD1').length==0){
				$('#CMSUBMIT').removeClass("ttb-button btn-flat-orange");
				$('#CMSUBMIT').addClass("ttb-button btn-flat-gray");
				$('#CMSUBMIT').attr("disabled",true);
			}
		}
		//有一般卡及VISA卡且兩個都有錯誤訊息  把確認鈕鎖起來
		if(('${card_paid_history.data.n810MsgCode}'!='E091' && '${card_paid_history.data.n810MsgCode}'!='') 
				&& ('${card_paid_history.data.n813MsgCode}'!='E091' && '${card_paid_history.data.n813MsgCode}'!='')){
			$('#RAD1').removeAttr("checked");
			$('#RAD2').removeAttr("checked");
			$('#CMSUBMIT').removeClass("ttb-button btn-flat-orange");
			$('#CMSUBMIT').addClass("ttb-button btn-flat-gray");
			$('#CMSUBMIT').attr("disabled",true);
		}
		
	}
	
	function changeRadio(){
		$('#CMPERIOD').prop("checked",true);
	}
	
	// 驗證帳號下拉選單
	function validate_CARDNUM(field, rules, i, options) {
		var inputAttr = rules[i + 2];
		console.log("inputAttr>>" + inputAttr);
		var dpagacno_val = $("#" + inputAttr).find(":selected").val()
		console.log("funccall test");
		console.log("funccall test>>" + options.allrules.required.alertText);
		if (fstop.isEmptyString(dpagacno_val) || dpagacno_val.indexOf('#') > -1) {
			return options.allrules.required.alertText
		}
	}
	
</script>
<body>
   	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 信用卡     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Credit_Card" /></li>
    <!-- 帳務查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Inquiry_Service" /></li>
    <!-- 繳款記錄查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0003" /></li>
		</ol>
	</nav>

	
	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
	
	
	<main class="col-12">
		<section id="main-content" class="container"><!-- 主頁內容  -->
			<!-- 繳款紀錄查詢 -->
			<h2><spring:message code="LB.D0003" /></h2>
			
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="${__ctx}/CREDIT/INQUIRY/card_paid_history_result">
			<input type="hidden" name="CARDNUM_ALL" value="${card_paid_history.data.CARDLIST}">
				<div class="main-content-block row">
					<!-- 主頁內容  -->
					<div class="col-12">
						<div class="ttb-input-block">
							<div class="ttb-message">
							<p><spring:message code="LB.D0003" /></p><!-- 繳款紀錄查詢 -->
							
						</div>
						<div class="ttb-input-item row">
							<span class="input-title"> <label><h4><spring:message code="LB.Inquiry_time" /></h4></label><!-- 查詢時間 -->
							</span>
							<span class="input-block">
								<div class="ttb-input">
									<p>${card_paid_history.data.CMQTIME}</p>
								</div>
							</span>
						</div>
						<div class="ttb-input-item row">
							<span class="input-title"> <label><h4><spring:message code="LB.Card_type" /></h4></label><!-- 卡別 -->
							</span> 
							<c:if test="${card_paid_history.data.n810MsgCode != 'E091'}">
							<span class="input-block">
								<div class="ttb-input">
								<!-- 顯示一般卡  -->
								
										<label class="radio-block"><spring:message code="LB.Bank_card" />
											<input type="radio" name="CARDTYPE" value="0" onclick="showVISACardNo(this.id)" id="RAD1" checked/>
											<span class="ttb-radio"></span>
										</label>
										<c:if test="${card_paid_history.data.n810MsgCode != 'E091' && card_paid_history.data.n810MsgCode !=''}">
											<span class="input-unit">
											<font color=red>
											(<spring:message code="LB.ErrMsg" />：&nbsp;${card_paid_history.data.n810MsgCode}&nbsp;${card_paid_history.data.N810Message})
											</font>
											</span>
										</c:if>
									
							</span>
							</c:if>
							<c:if test="${card_paid_history.data.n813MsgCode != 'E091'}">
							<br><br>
							<span class="input-block">
								<div class="ttb-input">
								<!-- 顯示VISA金融卡  -->
										<c:if test="${card_paid_history.data.n810MsgCode != 'E091'}">
											<label class="radio-block"><spring:message code="LB.VISA_debit_card" /><!-- VISA金融卡 -->
												<input type="radio" name="CARDTYPE" value="1" onclick="showVISACardNo(this.id)" id="RAD2" 
												<c:if test="${(card_paid_history.data.n810MsgCode=='E091'||card_paid_history.data.n810MsgCode !='')&&card_paid_history.data.n813MsgCode ==''}">checked</c:if> />
												<span class="ttb-radio"></span>
											</label>
											</c:if>
										<c:if test="${card_paid_history.data.n813MsgCode != 'E091' && card_paid_history.data.n813MsgCode != ''}">
											<span class="input-unit">
											<font color="red">
											(<spring:message code="LB.ErrMsg" />：&nbsp;${card_paid_history.data.n813MsgCode}&nbsp;${card_paid_history.data.N813Message})
											</font>
											</span>
										</c:if>
									<br>
									<span id="hid" class="ttb-input" style="display:none">
									<br>
									<spring:message code="LB.Card_number" />
									<br>
								    <select name="CARDNUM" id="CARDNUM" class="select-input validate[required,funcCall[validate_CARDNUM[CARDNUM]]">
					                		<!-- 預設值  -->
						    				<option value="#">------<spring:message code="LB.Select_account" />------</option> <!-- ------ 請選擇(信用卡)帳號 ------ -->
						    			
						    				<!-- 卡號_List迴圈  -->
						    				<c:set var="count" value="0" scope="page" />
					                		<c:forEach var="cardnumList" items="${card_paid_history.data.N813REC}">
					                				<option value="${cardnumList.VALUE}"> ${cardnumList.CARDNUM} </option>
					                				<c:set var="count" value="${count + 1}" scope="page"/>
					                		</c:forEach>
					                		<!-- 若卡號不只一個則新增 [全部]選項  -->
					                		<c:if test="${count > 1}">
					                			<option value="ALL">------ <spring:message code="LB.All" /> ------</option><!-- 全部＊ -->
					                		</c:if>
						    			</select>
									</span>
							</span>
							</c:if>
						</div>
						</div>
						
						<c:if test="${card_paid_history.data.n810MsgCode != 'E091' && card_paid_history.data.n813MsgCode != 'E091'}">
						</div>
						<br>
						</c:if>
						
						<div class="ttb-input-item row">
							<span class="input-title"> <label><h4><spring:message code="LB.Inquiry_period_1" /><!-- 查詢期間 --></h4></label></span>
								<span class="input-block">
									<div class="ttb-input">
										<!-- 當月 -->
										<span>
										<label class="radio-block"><spring:message code="LB.D0009" /><!-- 當月 -->
											<input type="radio" name="FGPERIOD" value="CMCURMON" checked> <span 
											class="ttb-radio"></span>
										</label>
										&nbsp;&nbsp;
										<label class="radio-block"><spring:message code="LB.D0010" /><!-- 上月 -->
											<input type="radio" name="FGPERIOD" value="CMLASTMON"> <span 
											class="ttb-radio"></span>
										</label>
										&nbsp;&nbsp;
										<label class="radio-block"><spring:message code="LB.D0011" /><!-- 上上月 -->
											<input type="radio" name="FGPERIOD" value="CMLAST2MON"> <span 
											class="ttb-radio"></span>
										</label>
										</span>
									</div>
									<div class="ttb-input">
											<span>
											<label class="radio-block"><spring:message code="LB.Specified_period" />
											<input type="radio" name="FGPERIOD" value="CMPERIOD" id="CMPERIOD"> 
											<span class="ttb-radio"></span><!-- 指定日期 -->	
											</label>
									</div>
									<div class="ttb-input">
										<span class="input-subtitle subtitle-color"> <spring:message code="LB.Period_start_date" /><!-- 起日 --> </span> 
											<input type="text" id="CMSDATE" name="CMSDATE" class="text-input datetimepicker"  maxlength="10" value="${card_paid_history.data.TODAY}" onchange="changeRadio()"/> 
												<span class="input-unit CMSDATE">
													<img src="${__ctx}/img/icon-7.svg" />
												</span>
									</div>
									<div class="ttb-input">
											<span class="input-subtitle subtitle-color"> <spring:message code="LB.Period_end_date" /><!-- 迄日 --> </span> 
											<input type="text" id="CMEDATE" name="CMEDATE" class="text-input datetimepicker" maxlength="10" value="${card_paid_history.data.TODAY}" onchange="changeRadio()"/> 
												<span class="input-unit CMEDATE">
													<img src="${__ctx}/img/icon-7.svg" />
												</span>
									</div>
										<span id="hideblock_CheckDateScope">
											<!--驗證用的input -->
										<input id="odate" name="odate" type="text" value="${card_paid_history.data.TODAY}" class="text-input validate[required,funcCall[validate_CheckDateScope['<spring:message code= "LB.X1485" />',odate , CMSDATE ,CMEDATE, false,6,null]]]" style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</span>
								</div>
						</div>
						<input id="CMRESET" name="CMRESET" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" /> 
						<input id="CMSUBMIT" name="CMSUBMIT" type="submit" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
					</div>
				</div>
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p><!-- 說明 -->
						<li><spring:message code="LB.Card_Paid_History_P1_D1" /></li>
						<li><spring:message code="LB.Card_Paid_History_P1_D2" /></li>
						<li><spring:message code="LB.Card_Paid_History_P1_D3" /></li>
						<li><spring:message code="LB.Card_Paid_History_P1_D4" /></li>
				</ol>
			</div>
			</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>
