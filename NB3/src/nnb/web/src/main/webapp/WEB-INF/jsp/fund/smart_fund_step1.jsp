<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="RS" value="${transfer_data_query_history.data.RS}"></c:set>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript">
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});

		function init() {
			initDataTable();
			//表單驗證
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
		}
		
		// 點按鈕click
		function relieve(index) {
			// 塞資料
			$("#helperjson").val($("#" + index).val());
			//
			if (!$('#formId').validationEngine('validate')) {
				e = e || window.event; // for IE
				e.preventDefault();
			} else {
				$("#formId").validationEngine('detach');
				initBlockUI();//遮罩
				$("#formId").submit();
			}
		}



	</script>
</head>

<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金    -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0901" /></li>
    <!-- 基金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1064" /></li>
    <!-- SMART FUND自動贖回設定     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1184" /></li>
		</ol>
	</nav>

	<!-- 左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>

		<!-- 快速選單及主頁內容 -->
		<main class="col-12">
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.W1184" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form autocomplete="off" method="post" id="formId" action="${__ctx}/FUND/ALTER/smart_fund_step2">
				<c:set var="BaseResultData" value="${smart_fund_step1.data}"></c:set>
				<input type="hidden" id="helperjson" name="helperjson" value=''>
				<input type="hidden" id="CUSIDN" name="CUSIDN" value='${BaseResultData.CUSIDN}'>
				<input type="hidden" id="hideid_CUSIDN" name="hideid_CUSIDN" value='${BaseResultData.hideid_CUSIDN}'>
				<input type="hidden" id="hideid_NAME" name="hideid_NAME" value='${BaseResultData.hideid_NAME}'>
				<div class="main-content-block row">
					<div class="col-12">
						<ul class="ttb-result-list">
						<!-- 資料總數 -->
								<li>
									<h3>
										<spring:message code="LB.Total_records" />：
									</h3>
									<p>
										${BaseResultData.COUNT}
										<spring:message code="LB.Rows" />
									</p>
								</li>
								<!-- 變更種類 -->
								<li>
									<h3>
										<spring:message code="LB.W1167" />
									</h3>
									<p>
										<spring:message code="LB.W1186" />
									</p>
								</li>
								<!-- 身份證字號/統一編號 -->
								<li>
									<h3>
										<spring:message code="LB.Id_no" />
									</h3>
									<p>
										${BaseResultData.hideid_CUSIDN}
									</p>
								</li>
								<!--姓名 -->
								<li>
									<h3>
										<spring:message code="LB.Name" />
									</h3>
									<p>
										${BaseResultData.hideid_NAME}
									</p>
								</li>
						</ul>
						<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
							<thead>
								<!-- 變更種類-->
<%-- 								<tr> --%>
<%-- 									<th> --%>
<%-- 										<spring:message code="LB.W1167" /> --%>
<%-- 									</th> --%>
<%-- 									<th class="text-left" colspan="12"> --%>
<%-- 										<spring:message code="LB.W1186" /> --%>
<%-- 									</th> --%>
<%-- 								<tr> --%>
<!-- 								身份證字號/統一編號 -->
<%-- 								<tr> --%>
<%-- 									<th> --%>
<%-- 										<spring:message code="LB.Id_no" /> --%>
<%-- 									</th> --%>
<%-- 									<th class="text-left" colspan="12"> --%>
<%-- 										${BaseResultData.hideid_CUSIDN} --%>
<%-- 									</th> --%>
<%-- 								<tr> --%>
<!-- 									姓名 -->
<%-- 								<tr> --%>
<%-- 									<th> --%>
<%-- 										<spring:message code="LB.Name" /> --%>
<%-- 									</th> --%>
<%-- 									<th class="text-left" colspan="12"> --%>
<%-- 										${BaseResultData.hideid_NAME} --%>
<%-- 									</th> --%>
<%-- 								<tr> --%>
								<tr>
									<!--信託帳號 -->
									<th nowrap><spring:message code="LB.W0944" /></th>
									<!-- 扣款標的 -->
									<th nowrap><spring:message code="LB.W1041" /></th>
									<!-- 類別 -->
									<th nowrap>&nbsp;<spring:message code="LB.D0973" />&nbsp;</th>
									<!-- 信託金額-->
									<th nowrap><spring:message code="LB.W0026" /></th>
									<!-- 單位數 -->
									<th nowrap><spring:message code="LB.W0027" /></th>
									<!-- 未分配<br>金額-->
									<th nowrap><spring:message code="LB.W0921" /><br><spring:message code="LB.Deposit_amount_1" /></th>
									<!-- 參考現值-->
									<th nowrap><spring:message code="LB.W0915" /></th>
									<!-- 基準<br>報酬率 -->
									<th nowrap><spring:message code="LB.W1195" /><br><spring:message code="LB.W1196" /></th>
									<!-- 參考匯率<br>參考淨值 -->
									<th nowrap><spring:message code="LB.W0030" /><br><spring:message code="LB.W1198" /></th>
									<!-- 原自動贖回設定點 -->
									<th nowrap><spring:message code="LB.W1199" /></th>
									<!-- 自動贖回<BR>設定/變更< -->
									<th>
										<spring:message code="LB.X1916"/>
										<br>
										<spring:message code="LB.X1917"/>
									</th>
								</tr>
							</thead>
							<tbody>
							<c:forEach varStatus="loop" var="dataList" items="${ BaseResultData.REC }">
									<tr>
									<input type="hidden" id="helperjson_${loop.index}" value='${dataList.helperjson}'/>
										<td class="text-center">${dataList.hideid_CDNO }</td>
										<td class="text-center">${dataList.FUNDLNAME }</td>
										<td class="text-center">${dataList.str_AC202 }</td>
										<td class="text-center">${dataList.ADCCYNAME }<br>${dataList.display_FUNDAMT }</td>
										<td class="text-right">${dataList.display_ACUCOUNT }</td>
										<td class="text-right">${dataList.display_NAMT }</td>
										<td class="text-right">${dataList.display_AMT }</td>
										<td class="text-center">${dataList.RTNC }${dataList.display_RTNRATE }</td>
										<td class="text-right">
										${dataList.display_FXRATE }
										<br>
										${dataList.display_REFVALUE1 }
										</td>
										<td class="text-center">
										${dataList.display_STOPPROF }
										<br>
										${dataList.display_STOPLOSS }
										</td>
										<td  align="center">							
											<!--執行選項 -->
											<input class="ttb-sm-btn btn-flat-orange" type="button" value="<spring:message code="LB.Confirm" />" name="CMSUBMIT" onclick="relieve('helperjson_${loop.index}')"/>
										</td>	
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				</form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>

</body>

</html>