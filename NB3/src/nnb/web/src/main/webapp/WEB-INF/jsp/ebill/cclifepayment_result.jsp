<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/component/util/cardReaderMethod.js"></script>
	<script type="text/javascript">

	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	<!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<!-- 活存帳戶繳信用卡費 -->
			<li class="ttb-breadcrumb-item">
				<a href="#">
					<spring:message code="LB.X2427" />
				</a>
			</li>
			<!-- 活存帳戶繳信用卡費 -->
			<li class="ttb-breadcrumb-item active" aria-current="page">
				<spring:message code="LB.X2429" />
			</li>
		</ol>
	</nav>
	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
<%-- 		<%@ include file="../index/menu.jsp"%> --%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!--活存帳戶繳信用卡費 -->
				<h2>
					<spring:message code="LB.X2429" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form autocomplete="off" method="post" id="formId" action="">
					<!-- 表單顯示區 -->
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<div class="ttb-message">
									<span><spring:message code="LB.Transaction_successful" /></span>
								</div>
								<!--  交易時間: -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.Trading_time" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												${cclifepayment_result.data.DATETIME }
											</p>
										</div>
									</span>
								</div>
								<!--  銷帳編號: -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.W0407" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												${cclifepayment_result.data.CARDNUM }
											</p>
										</div>
									</span>
								</div>
								<!-- 本期應繳金額 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.This_period" /><spring:message code="LB.Repayment_of_every_month" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												NTD ${cclifepayment_result.data.PAYAMT } <spring:message code="LB.Dollar" />
											</p>
										</div>
									</span>
								</div>
								<!-- 最低應繳金額:-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.X2432" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												NTD ${cclifepayment_result.data.LPAYAMT } <spring:message code="LB.Dollar" />
											</p>
										</div>
									</span>
								</div>
								<!-- 身分證字號/統編: -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.Id_no" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												${cclifepayment_result.data.CUSIDN }
											</p>
										</div>
									</span>
								</div>
								<!-- 轉出銀行 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.X2431" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												${cclifepayment_result.data.BANKNAME }
											</p>
										</div>
									</span>
								</div>
								<!-- 轉出帳號-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.Payers_account_no" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												${cclifepayment_result.data.trin_acn }
											</p>
										</div>
									</span>
								</div>
								<!-- 繳款金額:-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.Payment_amount" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												NTD ${cclifepayment_result.data.ipayamt } <spring:message code="LB.Dollar" />
											</p>
										</div>
									</span>
								</div>
								<!-- 手續費:-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.D0507" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												NTD ${cclifepayment_result.data.FEE } <spring:message code="LB.Dollar" />
											</p>
										</div>
									</span>
								</div>
								<!-- 交易成功，已發送通知至您的信箱:-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.X2430" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												${cclifepayment_result.data.CustEmail }
											</p>
										</div>
									</span>
								</div>
							</div>
							<!-- button -->
							<!--buttonEND -->
						</div>
					</div>
				</form>
			</section>
			<!-- main-content END -->
		</main>
	</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>