<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>

<script type="text/javascript">
$(document).ready(function(){
	// HTML載入完成後開始遮罩
	setTimeout("initBlockUI()", 10);
	// 開始查詢資料並完成畫面
	setTimeout("init()", 20);
	// 解遮罩
	setTimeout("unBlockUI(initBlockId)", 500);

	setTimeout("initDataTable()",100);
	setTimeout("dodatataable()",100);
	jQuery(function($) {
		$('.dtablet').DataTable({
			scrollX: true,
			sScrollX: "99%",
			scrollY: true,
			bPaginate: false,
			bFilter: false,
			bDestroy: true,
			bSort: false,
			info: false,
		});
	});
	
});

function init() {
	$("#printbtn").click(function(){
		var i18n = new Object();
		i18n['jspTitle']='<spring:message code="LB.Credit_Card_Un-billed_Transactions" />'
		var params = {
				"jspTemplateName":"unbilled_result_print",
				"jspTitle":i18n['jspTitle'],
				"HOLDERNAME":'${card_unbilled_result.data.CUSNAME}',
				"CYCLEDATE":'${card_unbilled_result.data.CYCLE}',
				"CMQTIME":"${card_unbilled_result.data.CMQTIME}",
				"COUNT":"${card_unbilled_result.data.TOTCNT}"
		}
		openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);

	});
}

function selectBlockUI() {
	//change後遮罩啟動
	setTimeout("initBlockUI()",10);
	// 開始執行動作
	setTimeout("selectAction()",20);
	// 解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
		
}
function selectAction(){
	if($('#actionBar').val()=="excel"){
		$("#downloadType").val("OLDEXCEL");
		$("#templatePath").val("/downloadTemplate/unbilled.xls");
	}else if ($('#actionBar').val()=="txt"){
		$("#downloadType").val("TXT");
		$("#templatePath").val("/downloadTemplate/unbilled.txt");
	}
	//ajaxDownload("${__ctx}/ajaxDownload","formId","finishAjaxDownload()");
	$("#DownloadformId").attr("target", "");
	$("#DownloadformId").submit();
	$('#actionBar').val("");
}
function finishAjaxDownload(){
	$("#actionBar").val("");
	unBlockUI(initBlockId);
}

function dodatataable(){
	jQuery(function($) {
		$('.dtable_1').DataTable({
			scrollX: true,
			sScrollX: "99%",
			lengthChange: false,
			searching: false,
			bPaginate: false,
			bFilter: false,
			bDestroy: true,
			bSort: false,
			info: false,
		});
	});
}


</script>
</head>
<body>
   	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

	<!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 信用卡     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Credit_Card" /></li>
    <!-- 帳務查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Inquiry_Service" /></li>
    <!-- 未出帳單明細查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Credit_Card_Un-billed_Transactions" /></li>
		</ol>
	</nav>
	
	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
	

	<main class="col-12">
		<section id="main-content" class="container"><!-- 主頁內容  -->
			<!-- 信用卡未出帳單明細查詢 -->
			<h2><spring:message code="LB.Credit_Card_Un-billed_Transactions" /></h2>
			
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			
			<div class="print-block">
					<select class="minimal" id="actionBar" onchange="selectBlockUI()">
						<!-- 執行選項-->
						<option value=""><spring:message
								code="LB.Downloads" /></option>
						<!-- 下載Excel檔-->
						<option value="excel"><spring:message
								code="LB.Download_excel_file" /></option>
						<!-- 下載為txt檔-->
						<option value="txt"><spring:message
								code="LB.Download_txt_file" /></option>
					</select>
				</div>
			<br/>
			<br/>
				<div class="main-content-block row">
					<div class="col-12">
						<div>
							<ul class="ttb-result-list">
								<li>
									<!-- 查詢時間  -->
									<h3>
										<spring:message code="LB.Inquiry_time" />
									</h3>
									<p>
										${card_unbilled_result.data.CMQTIME}
									</p>
								</li>
								<li>
										<!-- 資料總數  -->
									<h3>
										<spring:message code="LB.Total_records" />
									</h3>
									<p>
										${card_unbilled_result.data.TOTCNT} <spring:message code="LB.Rows" />
									</p>
								</li>
							</ul>
							
							
							<table class="stripe table-striped ttb-table dtable_1" data-toggle-column="first">
								<thead>
					            <tr>
					                <th data-title='<spring:message code="LB.Name"/>'>
										<!-- 姓名  -->
					               		<spring:message code="LB.Name" />
					                </th>
					                <th data-title='<spring:message code="LB.Statement_date_TD01"/>'>
					                	<!-- 每月結帳日  -->
					               		<spring:message code="LB.Statement_date_TD01" />
					                </th>
					            </tr>
					            </thead>
					            <tbody>
					            <tr>
					                <td class="text-center">
										<!-- 姓名  -->
					               		${card_unbilled_result.data.CUSNAME}
					                </td>
					                <td class="text-center">
					                	<!-- 每月結帳日  -->
					               		${card_unbilled_result.data.CYCLE}
					                </td>
					            </tr>
					            </tbody>
							</table>
							
							<c:forEach var="dataTable" items="${card_unbilled_result.data.REC}">
							
							<table class="stripe table-striped ttb-table dtablet" data-toggle-column="first">
								<thead>
					            <tr>
					                <th data-title='<spring:message code="LB.Card_kind"/>'>
										<!-- 卡片種類  -->
					               		<spring:message code="LB.Card_kind" />
					                </th>
					                <th data-title='<spring:message code="LB.Consumption_date"/>' data-breakpoints="xs">
					                	<!-- 消費日期  -->
					               		<spring:message code="LB.Consumption_date" />
					                </th>
					                <th data-title='<spring:message code="LB.The_account_credited_date"/>' data-breakpoints="xs">
					                	<!-- 入帳日期  -->
					               		<spring:message code="LB.The_account_credited_date" />
					                </th>
					                <th data-title='<spring:message code="LB.Description_of_transaction"/>' data-breakpoints="xs">
					                	<!-- 交易說明  -->
					               		<spring:message code="LB.Description_of_transaction" />
					                </th>
					                <th data-title='<spring:message code="LB.Card_holder"/>' data-breakpoints="xs">
					                	<!-- 持卡人 -->
					               		<spring:message code="LB.Card_holder" />
					                </th>
					                <th data-title='<spring:message code="LB.Currency"/>' data-breakpoints="xs sm">
					                	<!-- 幣別  -->
					               		<spring:message code="LB.Currency" />
					                </th >
					                <th data-title='<spring:message code="LB.Foreign_currency_amount"/>' data-breakpoints="xs sm">
					                	<!-- 外幣金額  -->
					               		<spring:message code="LB.Foreign_currency_amount" />
					                </th>
					                <th data-title='<spring:message code="LB.NTD_amount"/>'>
					                	<!-- 台幣金額  -->
					               		<spring:message code="LB.NTD_amount" />
					                </th>
					            </tr>
					            </thead>
					            <tbody>
							<c:forEach var="dataList" items="${dataTable.TABLE}">
					            <tr>
					                <td class="text-center">
<!-- 										卡片種類  -->
					               		${dataList.CARDTYPE}
					                </td>
					                <td class="text-center">
<!-- 					                	消費日期  -->
					               		${dataList.PURDATE}
					                </td>
					                <td class="text-center">
<!-- 					                	入帳日期  -->
					               		${dataList.POSTDATE}
					                </td>
					                <td class="text-center">
<!-- 					                	交易說明  -->
					               		${dataList.DESCTXT}
					                </td>
					                <td class="text-center">
<!-- 					                	持卡人 -->
					               		${dataList.CRDNAME}
					                </td>
					                <td class="text-center">
<!-- 					                	幣別  -->
					               		${dataList.CURRENCY}
					                </td>
					                <td class="text-right">
<!-- 					                	外幣金額  -->
					               		${dataList.SRCAMNT}
					                </td>
					                <td class="text-right">
<!-- 					                	台幣金額  -->
					               		${dataList.CURAMNT}
					                </td>
					            </tr>
							</c:forEach>
							</tbody>
					        </table>
					       </c:forEach>
						</div>
						
						<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
					</div>
					<form id="DownloadformId" action="${__ctx}/download" method="post">
<!-- 						下載用 -->
						<input type="hidden" name="downloadFileName" value="LB.Credit_Card_Un-billed_Transactions"/>
						<input type="hidden" name="CMQTIME" value="${card_unbilled_result.data.CMQTIME}"/>
						<input type="hidden" name="COUNT" value="${card_unbilled_result.data.TOTCNT}"/>
						<input type="hidden" name="CUSNAME" value="${card_unbilled_result.data.CUSNAME}"/>
						<input type="hidden" name="CYCLE" value="${card_unbilled_result.data.CYCLE}"/>
						<input type="hidden" name="downloadType" id="downloadType"/> 					
						<input type="hidden" name="templatePath" id="templatePath"/> 
						<input type="hidden" name="hasMultiRowData" value="true"/> 	
<!-- 						EXCEL下載用 -->
						<input type="hidden" name="headerRightEnd" value="1" />
			            <input type="hidden" name="headerBottomEnd" value="5" />
			            <input type="hidden" name="multiRowStartIndex" value="8" />
			            <input type="hidden" name="multiRowEndIndex" value="8" />
			            <input type="hidden" name="multiRowCopyStartIndex" value="7" />
			            <input type="hidden" name="multiRowCopyEndIndex" value="9" />
			            <input type="hidden" name="multiRowDataListMapKey" value="TABLE" />
			            <input type="hidden" name="rowRightEnd" value="10" />
<!-- 						TXT下載用 -->
						<input type="hidden" name="txtHeaderBottomEnd" value="8"/>
						<input type="hidden" name="txtMultiRowStartIndex" value="11"/>
						<input type="hidden" name="txtMultiRowEndIndex" value="11"/>
						<input type="hidden" name="txtMultiRowCopyStartIndex" value="9"/>
						<input type="hidden" name="txtMultiRowCopyEndIndex" value="12"/>
						<input type="hidden" name="txtMultiRowDataListMapKey" value="TABLE"/>
					</form>
				</div>
		</section>
	</main><!-- main-content END --> 
	</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>
