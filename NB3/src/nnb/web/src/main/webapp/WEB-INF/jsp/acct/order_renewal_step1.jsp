<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		
		$('#DPSVACNO option[value=${order_renewal_step1.data.fgselectMap.TSFACN}]').attr('selected', 'selected');//預設選取
		
		//表單驗證
		$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
	    // 確定按鈕事件
	    $("#CMSUBMIT").click(function() {
	            if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					initBlockUI();//遮罩
					$("#formId").submit();
				}
	    });
	    
		// 續存方式change事件 
			$('input[type=radio][name=FGSVTYPE]').change(function () {
				if (this.value == '1') {
					$("#DPSVACNO").addClass("validate[required]")
				}
				else if (this.value == '2') {
					$("#DPSVACNO").removeClass("validate[required]");
				}
			});
		//上一頁按鈕
		$("#CMBACK").click(function() {
			var action = '${__ctx}/NT/ACCT/TDEPOSIT/order_renewal';
			$('#back').val("Y");
			$("form").attr("action", action);
			initBlockUI();
			$("#formId").validationEngine('detach');
			$("form").submit();
		});
	});
	//重新輸入
 	function formReset() {
 		if ($('#actionBar').val()=="reEnter"){
 			$('#actionBar').val("");
	 		document.getElementById("formId").reset();
 		}
	}
	</script>
</head>

<body>
	<!--   IDGATE -->
	<%@ include file="../idgate_tran/idgateAlert.jsp" %>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 定存服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Current_Deposit_To_Time_Deposit" /></li>
    <!-- 定存單到期續存     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Renewal_At_Maturity_Of_NTD_Time_Deposit" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
	<!-- 		主頁內容  -->
	<main class="col-12">
		<section id="main-content" class="container">
			<h2>
				<!-- 臺幣定存單到期續存 -->
				<spring:message code="LB.Renewal_At_Maturity_Of_NTD_Time_Deposit" />
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<!-- 下拉式選單-->
			<div class="print-block no-l-display-btn">
				<select class="minimal" id="actionBar" onchange="formReset()">
					<option value=""><spring:message code="LB.Execution_option" /></option>
					<!-- 重新輸入-->
					<option value="reEnter"><spring:message code="LB.Re_enter"/></option>
				</select>
			</div>
			<form id="formId" method="post" action="${__ctx}/NT/ACCT/TDEPOSIT/order_renewal_confirm">
			<input type="hidden" id="back" name="back" value="">
				<c:set var="BaseResultData" value="${order_renewal_step1.data }"></c:set>
				<input type="hidden" name="REQPARAMJSON" value='${order_renewal_step1.data.fgselctJson}' />
				<!--交易步驟 -->
				<div id="step-bar">
					<ul>
				<!--輸入資料 -->
						<li class="active"><spring:message code="LB.Enter_data"/></li>
				<!-- 確認資料 -->
						<li class=""><spring:message code="LB.Confirm_data"/></li>
				<!-- 交易完成 -->
						<li class=""><spring:message code="LB.Transaction_complete"/></li>
					</ul>
        		</div>
				<!-- 表單顯示區  -->
				<div class="main-content-block row">
					<div class="col-12">
						<div class="ttb-input-block">
							<!-- 存單帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Account_no" /></h4>
									</label>
								</span>
								<span class="input-block">
									${BaseResultData.fgselectMap.ACN }
								</span>
							</div>
							<!-- 存單號碼 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Certificate_no" /></h4>
									</label>
								</span>
								<span class="input-block">
									${BaseResultData.fgselectMap.FDPNUM }
								</span>
							</div>
							<!-- 計息方式 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Interest_calculation" /></h4>
									</label>
								</span>
								<span class="input-block">
									<!-- 									機動 -->
									<label class="radio-block">
										<spring:message code="LB.Floating" />
										<input type="radio" name="INTMTH" id="DPINTTYPE1" value="0" checked="checked">
										<span class="ttb-radio"></span>
									</label>
									<!-- 										固定 -->
									<label class="radio-block">
										<spring:message code="LB.Fixed" />
										<input type="radio" name="INTMTH" id="DPINTTYPE2" value="1">
										<span class="ttb-radio"></span>
									</label>
								</span>
							</div>
							<!--續存方式 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Renew_method" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<!--本金轉期，利息轉入帳號 -->
										<label class="radio-block">
											<spring:message code="LB.Principal_rollover_interest_to_account" />
											<input type="radio" name="FGSVTYPE" id="DPSVTYPE1" value="1" checked="checked" />
											<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<!-- 請選擇帳號 -->
											<select name="DPSVACNO" id="DPSVACNO" class="custom-select select-input half-input validate[required]">
												<option value="">---<spring:message code="LB.Select_account" />---</option>
												<c:forEach var="dataList" items="${ BaseResultData.REC }">
													<option value="${dataList.ACN}">${dataList.ACN}</option>
												</c:forEach>
											</select>
									</div>
									<div class="ttb-input">
										<!-- 本金及利息一併轉期 -->
										<label class="radio-block">
											<spring:message code="LB.Renew_principal_and_interest" />
											<input type="radio" name="FGSVTYPE" id="DPSVTYPE2" value="2" />
											<span class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>
						</div>
						<!-- button -->
							<!--回上頁 -->
							<spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>
							<input type="button" name="CMBACK" id="CMBACK" value="${cmback}" class="ttb-button btn-flat-gray">
							<!--重新輸入 -->
							<spring:message code="LB.Re_enter" var="cmRest"></spring:message>
							<input type="reset" name="CMRESET" id="CMRESET" value="${cmRest}" class="ttb-button btn-flat-gray no-l-disappear-btn">
							<!-- 確定 -->
							<spring:message code="LB.Confirm" var="cmSubmit"></spring:message>
							<input type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" class="ttb-button btn-flat-orange">
						<!--button 區域 -->
					</div>
				</div>
<!-- 				說明 -->
				<div class="text-left">
						<!-- 		說明： -->
					<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page"/></p>
<!-- 						<li>限本人<span class="style1">已到期</span>的定期存款、存本取息、整存整付儲蓄存款實體定存單，辦理到期續存。 </li> -->
						<li><span><spring:message code="LB.Order_renewaly_P2_D1"/></span></li>
<!-- 						<li>利息轉入帳號限本行本人新台幣活期性存款帳號。</li> -->
						<li><span><spring:message code="LB.Order_renewaly_P2_D2"/></span></li>
<!-- 						<li>建議請以PC桌上型電腦操作使用，作業系統Windows98、IE6.0以上。</li> -->
						<li><span><spring:message code="LB.Order_renewaly_P2_D3"/></span></li>
					</ol>
				</div>
			</form>
		</section>
		<!-- main-content END -->
	</main>
		</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>