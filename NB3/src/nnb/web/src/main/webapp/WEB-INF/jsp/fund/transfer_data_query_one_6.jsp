<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="RS" value="${transfer_data_query_one.data.RS}"></c:set>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<script type="text/javascript">
		$(document).ready(function () {
			//列印
			$("#printbtn").click(function () {
				var params = {
						jspTemplateName:		"transfer_data_query_one_6_print",
						jspTitle:				"<spring:message code= "LB.W1000" />",
						TRADEDATE_F: 			"${RS.TRADEDATE_F }",
						CDNO_F: 			"${RS.CDNO_F }",
						TRANSCODE_FUNDLNAME: 	"${RS.TRANSCODE_FUNDLNAME }",
						CRY_CRYNAME: 			"${RS.CRY_CRYNAME }",
						AMT1_F: 				"${RS.AMT1_F }",
						UNIT_F: 				"${RS.UNIT_F }",
						PRICE1_F: 				"${RS.PRICE1_F }",
						INTRANSCODE_FUNDLNAME:	"${RS.INTRANSCODE_FUNDLNAME }",
						NEWCRY:					"${RS.NEWCRY }",
						EXAMT_F:				"${RS.EXAMT_F }",
						TXUNIT_F:				"${RS.TXUNIT_F }",
						INTRANSCRY_CRYNAME:		"${RS.INTRANSCRY_CRYNAME }",
						PRICE2_F: 				"${RS.PRICE2_F }",
						EXRATE_F: 				"${RS.EXRATE_F }"
					};
				openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
		});
	</script>
</head>

<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金    -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0901" /></li>
    <!-- 基金查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0902" /></li>
    <!-- 基金交易資料查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0943" /></li>
    <!-- 基金單位數合併交易資料查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1000" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
	</div>
	<!-- 快速選單及主頁內容 -->
	<main class="col-12">
		<!-- 主頁內容  -->
		<section id="main-content" class="container">
			<h2><spring:message code="LB.W1000" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form method="post" action="" >
			
				<!-- 表單顯示區  -->
				<div class="main-content-block row">
					<div class="col-12">
						<div class="ttb-input-block">
							<!-- 合併日期 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W1001" />
									</label>
								</span>
								<span class="input-block">
									${RS.TRADEDATE_F }
								</span>
							</div>
							<!-- 信託編號 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W0904" />
									</label>
								</span>
								<span class="input-block">
									${RS.CDNO_F }
								</span>
							</div>
							<!-- 轉出基金名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W0963" />
									</label>
								</span>
								<span class="input-block">
									${RS.TRANSCODE_FUNDLNAME }
								</span>
							</div>
							<!-- 基準日 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W0028" />
									</label>
								</span>
								<span class="input-block">
									${RS.TRADEDATE_F }
								</span>
							</div>
							<!-- 轉出信託金額 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W1116" />
									</label>
								</span>
								<span class="input-block">
									${RS.CRY_CRYNAME } ${RS.AMT1_F }
								</span>
							</div>
							<!-- 轉出單位數 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W1122" />
									</label>
								</span>
								<span class="input-block">
									${RS.UNIT_F }
								</span>
							</div>
							<!-- 轉出單位淨值 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.X0384" />
									</label>
								</span>
								<span class="input-block">
									${RS.CRY_CRYNAME } ${RS.PRICE1_F }
								</span>
							</div>
							<!-- 併入基金名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W1002" />
									</label>
								</span>
								<span class="input-block">
									${RS.INTRANSCODE_FUNDLNAME }
								</span>
							</div>
							<!-- 併入後信託金額 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W1003" />
									</label>
								</span>
								<span class="input-block">
									${RS.NEWCRY } ${RS.EXAMT_F }
								</span>
							</div>
							<!-- 併入後信託單位數 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W1004" />
									</label>
								</span>
								<span class="input-block">
									${RS.TXUNIT_F }
								</span>
							</div>
							<!-- 併入單位淨值 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W1005" />
									</label>
								</span>
								<span class="input-block">
									${RS.INTRANSCRY_CRYNAME } ${RS.PRICE2_F }
								</span>
							</div>
							<!-- 轉換匯率 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W1006" />
									</label>
								</span>
								<span class="input-block">
									${RS.EXRATE_F }
								</span>
							</div>
						</div>
						
						
						<%-- <ul class="ttb-result-list result-shift">
							<li>
								<!-- 合併日期-->
								<h3><spring:message code="LB.W1001" /></h3>
								<p>${RS.TRADEDATE_F }</p>
							</li>
							<li>
								<!-- 信託編號-->
								<h3><spring:message code="LB.W0904" /></h3>
								<p>${RS.CREDITNO_F }</p>
							</li>
							<li>
								<!-- 轉出基金名稱 -->
								<h3><spring:message code="LB.W0963" /></h3>
								<p>${RS.TRANSCODE_FUNDLNAME }</p>
							</li>
							<li>
								<!-- 基準日 -->
								<h3><spring:message code="LB.W0028" /></h3>
								<p>${RS.TRADEDATE_F }</p>
							</li>
							<li>
								<!-- 轉出信託金額 -->
								<h3><spring:message code="LB.W1116" /></h3>
								<p>${RS.CRY_CRYNAME } ${RS.AMT1_F }</p>
							</li>
							<li>
								<!-- 轉出單位數 -->
								<h3><spring:message code="LB.W1122" /></h3>
								<p>${RS.UNIT_F }</p>
							</li>
							<li>
								<!-- 轉出單位淨值 -->
								<h3><spring:message code="LB.X0384" /></h3>
								<p>${RS.CRY_CRYNAME } ${RS.PRICE1_F }</p>
							</li>
							<li>
								<!-- 併入基金名稱 -->
								<h3><spring:message code="LB.W1002" /></h3>
								<p>${RS.INTRANSCODE_FUNDLNAME }</p>
							</li>
							<li>
								<!-- 併入後信託金額 -->
								<h3><spring:message code="LB.W1003" /></h3>
								<p>${RS.NEWCRY } ${RS.EXAMT_F }</p>
							</li>
							<li>
								<!-- 併入後信託單位數 -->
								<h3><spring:message code="LB.W1004" /></h3>
								<p>${RS.TXUNIT_F } </p>
							</li>
							<li>
								<!-- 併入單位淨值 -->
								<h3><spring:message code="LB.W1005" /></h3>
								<p>${RS.INTRANSCRY_CRYNAME } ${RS.PRICE2_F } </p>
							</li>
							<li>
								<!-- 轉換匯率 -->
								<h3><spring:message code="LB.W1006" /></h3>
								<p>${RS.EXRATE_F } </p>
							</li>
						</ul> --%>
						<div>
							<!-- 列印  -->
							<spring:message code="LB.Print" var="printbtn"></spring:message>
							<input type="button" id="printbtn" value="${printbtn}" class="ttb-button btn-flat-orange" />
						</div>
					</div>
				</div>
			</form>
		</section>
		<!-- main-content END -->
	</main>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>