<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script type="text/javascript">

	// column's title i18n

	
	// n510-外匯活存餘額
	i18n['ACN'] = '<spring:message code="LB.Account" />'; // 帳號
	i18n['KIND'] = '<spring:message code="LB.Deposit_type" />'; // 存款種類
	i18n['CUID'] = '<spring:message code="LB.Currency" />'; // 幣別
	i18n['AVAILABLE'] = '<spring:message code="LB.Available_balance" />'; // 可用餘額
	i18n['BALANCE'] = '<spring:message code="LB.Account_balance" />'; // 帳戶餘額
	i18n['QM'] = '<spring:message code="LB.Quick_Menu" />';//快速選單
	//可用餘額小計
	i18n['CURRENCY']='<spring:message code="LB.Currency" />';//貨幣
	i18n['SOAVB']='<spring:message code="LB.Subtotal_of_available_balance" />';//可用餘額小計
	i18n['TOTALRECORDS']='<spring:message code="LB.Total_records" />';//資料總數
	//帳戶餘額小計
	i18n['SOACB']='<spring:message code="LB.Subtotal_of_account_balance" />';//帳戶餘額小計
	i18n['ROWS']='<spring:message code="LB.Rows"/>';//筆
	
	
    // DataTable Ajax tables
	var n510_columns = [
		{ "data":"ACN", "title":i18n['ACN'] },
		{ "data":"KIND", "title":i18n['KIND'] },
		{ "data":"CUID", "title":i18n['CUID'] },
		{ "data":"AVAILABLE", "title":i18n['AVAILABLE'] },
		{ "data":"BALANCE", "title":i18n['BALANCE'] },
		{ "data":"QM","title":i18n['QM'] }
	];
	var n510_align = [0,0,0,1,1,0];
	var n510_Sum_align = [0,1,0];
	//可用餘額小計
	var n510_columns_AVASum = [
		{"data":"CUID","title":i18n['CURRENCY']},
		{"data":"AVAILABLE","title":i18n['SOAVB']},
		{"data":"COUNT","title":i18n['TOTALRECORDS']},
	];
	//帳戶餘額小計
	var n510_columns_BALSum = [
		{"data":"CUID","title":i18n['CURRENCY']},
		{"data":"BALANCE","title":i18n['SOACB']},
		{"data":"COUNT","title":i18n['TOTALRECORDS']},
	];
	
	// Ajax_n510-外匯活存餘額
	function getMyAssets510() {
		$("#n510").show();
		$("#n510_title").show();
		createLoadingBox("n510_BOX",'');
	
		uri = '${__ctx}' + "/OVERVIEW/allacct_n510aj";
		console.log("allacct_n510aj_uri: " + uri);
		fstop.getServerDataEx(uri, null, true, countAjax_n510, null, "n510");
	}

	// Ajax_callback
	function countAjax_n510(data){
		var error = false;
		console.log(data);
		// 資料處理後呈現畫面
		if (data.result) {
			//
			n510_rows = data.data.ACNInfo;
			n510_rows.forEach(function(d,i) {
				var options = [];
				//<!-- 外匯活存明細 -->
				options.push(new Option("<spring:message code='LB.FX_Demand_Deposit_Detail' />", "f_demand_deposit_detail"));
				if(d.OUTACN == "Y")
					{
					//	<!-- 外匯轉帳 -->
					options.push(new Option("<spring:message code='LB.FX_Transfer' />", "f_transfer"));
					//<!-- 外匯匯出匯款 -->
					options.push(new Option("<spring:message code='LB.W0286' />", "f_outward_remittances"));
					//	<!-- 轉入台幣綜存定存 -->
					options.push(new Option("<spring:message code='LB.Open_Foreign_Currency_Time_Deposit' />", "f_deposit_transfer"));
					}
				var qm = $("#actionBar").clone();
				qm.children()[0].id = "n510";
				qm.children()[0].name=i;
				qm.show();
				options.forEach(function(x){
					qm[0].children[0].options.add(x);
				});
				
				d["QM"]=qm.html();
			});
			//可用餘額小計
			n510_rows_AVASum = data.data.AVASum;
			n510_rows_BALSum = data.data.BALSum;
			n510_rows_AVASum.forEach(function(d,i) {
				d.COUNT+=i18n['ROWS'];
			});
			n510_rows_BALSum.forEach(function(d,i) {
				d.COUNT+=i18n['ROWS'];
			});
		} else {
			error= true;
			// 錯誤訊息
			n510_columns = [ {
					"data" : "MSGCODE",
					"title" : '<spring:message code="LB.Transaction_code" />'
				}, {
					"data" : "MESSAGE",
					"title" : '<spring:message code="LB.Transaction_message" />'
				}
			];
			n510_rows = [ {
				"MSGCODE" : data.msgCode,
				"MESSAGE" : data.message
			} ];
		}
		createTable("n510_BOX",n510_rows,n510_columns,n510_align);
		if(!error)
			{
			createLoadingBox("n510_InnerBOX1",'<spring:message code="LB.Subtotal_of_available_balance" />');
			createLoadingBox("n510_InnerBOX2",'<spring:message code="LB.Subtotal_of_account_balance" />');	
			createTable("n510_InnerBOX1",n510_rows_AVASum,n510_columns_AVASum,n510_Sum_align);
			createTable("n510_InnerBOX2",n510_rows_BALSum,n510_columns_BALSum,n510_Sum_align);
			}
	}

</script>

<p id="n510_title" class="home-title" style="display: none"><spring:message code="LB.X2357" /></p>
<div id="n510" class="main-content-block" style="display:none"> 
<div id="n510_BOX" style="display:none"></div>
<div id="n510_InnerBOX1" style="display:none"></div>
<div id="n510_InnerBOX2" style="display:none"></div>
</div>

