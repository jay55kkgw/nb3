<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<script type="text/javascript">
	$(document).ready(function () {
  	  init();
	});

	function init() {
		//上一頁按鈕
		$("#CMBACK").click(function() {
			initBlockUI();
			fstop.getPage('${pageContext.request.contextPath}'+'/INDEX/index','', '');
		});
	}
</script>
</head>
	<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金    -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0901" /></li>
    <!-- 基金查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0902" /></li>
    <!-- 信託契約書查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0373" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<main class="col-12">
	    	<section id="main-content" class="container">
	        	<h2><spring:message code="LB.X0374" /></h2>
	        	<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
	        	<form id="formID" action="" method="post">
	                <div class="main-content-block row">
	                    <div class="col-12 tab-content">
	                    	<img class="w-100" src="${__ctx}/img/trust_contract_0.JPG">
	                    	<hr />
	                    	<img class="w-100" src="${__ctx}/img/trust_contract_1.JPG">
	                    	<hr />
	                    	<img class="w-100" src="${__ctx}/img/trust_contract_2.JPG">
	                    	<hr />
	                    	<img class="w-100" src="${__ctx}/img/trust_contract_3.JPG">
	                    	<hr />
	                    	<img class="w-100" src="${__ctx}/img/trust_contract_4.JPG">
	                    	<hr />
	                    	<img class="w-100" src="${__ctx}/img/trust_contract_5.JPG">
	                    	<hr />
	                    	<img class="w-100" src="${__ctx}/img/trust_contract_6.JPG">
	                    	<hr />
	                    	<img class="w-100" src="${__ctx}/img/trust_contract_7.JPG">
	                    	<hr />
	                    	<img class="w-100" src="${__ctx}/img/trust_contract_8.JPG">
	                    	<hr />
	                    	<img class="w-100" src="${__ctx}/img/trust_contract_9.JPG">
	                    	<hr />
	                    	<img class="w-100" src="${__ctx}/img/trust_contract_10.JPG">
	                    	<hr />
	                    	<img class="w-100" src="${__ctx}/img/trust_contract_11.JPG">
	                    	<hr />
	                    	<img class="w-100" src="${__ctx}/img/trust_contract_12.JPG">
	                    	<hr />
	                    	<img class="w-100" src="${__ctx}/img/trust_contract_13.JPG">
	                    	<hr />
	                    	<img class="w-100" src="${__ctx}/img/trust_contract_14.JPG">
	                    	<hr />
	                    	<img class="w-100" src="${__ctx}/img/trust_contract_15.JPG">
	                    	
	                        <!--回上頁 -->
		   	 				<spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>
		    				<input type="button" name="CMBACK" id="CMBACK" value="${cmback}" class="ttb-button btn-flat-orange">
	                    </div>
	                </div>
	                </form>
	            </section>
	        </main>
	      </div>
	      <%@ include file="../index/footer.jsp"%>
	</body>
</html>