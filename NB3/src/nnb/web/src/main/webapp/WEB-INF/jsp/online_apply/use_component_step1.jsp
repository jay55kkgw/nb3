<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<script type="text/javascript"src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript"src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript"src="${__ctx}/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript">
var mylocale = "${pageContext.response.locale}";
	$(document).ready(function() {
		init();
		initKapImg();
	});
	function init() {	
		var main = document.getElementById("formId");
	  	//表單驗證
		$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
		$("#CMSUBMIT").click(function(e){		
			//Email 欄位若有輸入則須檢核格式
			if($('#MAIL').val() == ''){
				$('#MAIL').removeClass();
				$('#MAIL').addClass("text-input");
			}
			else{
				$('#MAIL').removeClass();
				$('#MAIL').addClass("text-input");
				$('#MAIL').addClass("validate[required,funcCall[validate_EmailCheck[MAIL]]]");

			}
			checktheform();
  		});
	  	datetimepickerEvent();		
		getTmr();	
		$("#BHID").val($("#accNo").val().substr(0,3));//服務分行根據存款帳號前三碼代替
	}
	function checktheform(e){
		$('#_CUSIDN').val($('#_CUSIDN').val().toUpperCase());
		// 送出進表單驗證前將span顯示
		$("#hideblock").show();
		e = e || window.event;
		if(!$('#formId').validationEngine('validate')){
        	e.preventDefault();
		}
		else{
				$("#formId").validationEngine('detach');				
				processQuery(); 
		}	
	}
	//確認鈕實作流程
	function processQuery(){

		var main = document.getElementById("formId");

		// 送出進表單驗證前將span顯示
		$("#hideblock").show();
		
		console.log("submit~~");

		/*------------------驗證清單----------------------
		*CheckSYS_IDNO	
		*CheckSelect("出生年", main.CCBIRTHDATEYY, "#")
		*檢查出生年月日
		*Email 欄位若有輸入則須檢核格式
		*啟用欄位
		*驗證碼
		----------------------------------------------*/
//		驗證閏月日期
		if(!checkDate1( main.CMTRDATE.value) ) {
			//您所輸入的出生年月日有誤，請重新檢查
			//alert("<spring:message code= "LB.X1079" />");
			errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.X1079' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
			);
	       	//EnableButtons();
	       	//DisconnectCard();					
	       	main.CMTRDATE.focus();
	       	return false;
		}
		//啟用不啟用
		if(main.ATMTRAN1[0].checked==false && main.ATMTRAN1[1].checked==false)	
		{
			main.ATMTRAN1[0].focus();
			//金融卡線上申請轉帳功能請勾選啟用或不啟用
			//alert("<spring:message code= "LB.X1080" />");
			errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.X1080' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
			);
			return false;
		}
		if(main.NOTIFY_ACTIVE.checked==true){
			if($('#MAIL').val() == ''){
				main.MAIL.focus();
				//申請活動通知請填選EMAIL
				//alert("<spring:message code= "LB.X1081" />");
				errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.X1081' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
			);
				return false;
			}
		}
		// 驗證碼驗證
		var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
		var capData = $("#formId").serializeArray();
		var capResult = fstop.getServerDataEx(capUri, capData, false);
		console.log("chaCode_valid: " + JSON.stringify(capResult) );
		// 驗證結果
		if (capResult.result) {
			
			main.CUSIDN.value=$("#_CUSIDN").val();
			//服務分行取值放入送出表單
			main.BRHCOD.value=$("#BHID").find(":selected").val();
			main.BRHNAME.value=$("#BHID").find(":selected").text();	
			//本行活動通知取值放入送出表單
			if(main.NOTIFY_ACTIVE.checked) {
				main.NOTIFY_ACTIVE.value="Y";
			}
			//啟用取值放入送出表單
			if(main.ATMTRAN1[0].checked)main.ATMTRAN.value="Y"
			if(main.ATMTRAN1[1].checked)main.ATMTRAN.value="N"
			
			getAuth();
			changeCode();
		} else {
			// 失敗重新產生驗證碼
			//驗證碼有誤
			//alert("<spring:message code= "LB.X1082" />");
			errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.X1082' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
			);
			changeCode();
		}

	  	return false;

	}
	//取得是否為信用卡申請網銀客戶
	function getAuth(){
		var main = document.getElementById("main");
		var timenow = new Date().getTime();
		var CUSIDN = formId._CUSIDN.value;
		//打啟用的電文
		$.ajax({
			 	type: "post",
			 	data:{
			 		CUSIDN:CUSIDN,
			 	},
			 	dataType:"text",
  				url:"${__ctx}/ONLINE/APPLY/use_component_step1_AUTH",
  				success:function(data){ 
  					console.log("AJAXDATA >>"+data);
  					callback_final(data);
  				}, 
		});
	}

	function callback_final(response){
		initBlockUI();
		var main = document.getElementById("formId");
		var rst = response.responseText;
			main.cflg.value = "N";		
			main.setAttribute("action", "${__ctx}/ONLINE/APPLY/use_component_confirm");
			main.submit();
			return false;
	}

	function mainQuery() {
		var main = document.getElementById("main");
		
		if(main.chk.value == "Y"){
			main.setAttribute("action", 'simpleform?trancode=NA30_3');
			main.submit();
		} else {
			//EnableButtons();
		}
	  	return false;
	}
	//判斷日期是否正確 for NA30 NA40
	function checkDate1(y){
		var datearray=y.split("/");
		var oyyy=Number(datearray[0]);
		var m=Number(datearray[1]);
		var d=Number(datearray[2])
		console.log("BY >>"+oyyy);
		console.log("BM >>"+m);
		console.log("BD >>"+d);
		//alert("checkdate1 year:"+oyyy);
		if(m==2||m==4||m==6||m==9||m==11){
				if(m==2&&d>29){
						//alert("checkDate:false");
						return	false;
				}		
				if(d>30){
						//alert("checkDate:false");
						return	false;
				}		
		}
		if(m==2&&d==29){
			if(((oyyy%4)==0 && !(oyyy%100)==0) || (oyyy%400==0)){
				//alert("checkDate:true");
				return true;
			}
			else {
				//alert("checkDate:false");
				return	false;
			}		
		}
		return	true;
	}
	//啟用與不啟用
	function chooseOne(cb){
		//先取得同name的chekcBox的集合物件
		var obj = document.getElementsByName("ATMTRAN1");
		if(obj[0]==cb && obj[0].checked==true)
			 window.open('${__ctx}/ONLINE/APPLY/use_component_ARTICLE');
		for (i=0; i<obj.length; i++){
			//判斷obj集合中的i元素是否為cb，若否則表示未被點選
			if (obj[i]!=cb)	obj[i].checked = false;
			//若是 但原先未被勾選 則變成勾選；反之 則變為未勾選
			//else	obj[i].checked = cb.checked;
			//若要至少勾選一個的話，則把上面那行else拿掉，換用下面那行
			else obj[i].checked = true;
		}
	}
	//產生預設日期
	function getTmr() {
		var today = new Date();
		today.setDate(today.getDate());
		var y = today.getFullYear();
		var m = today.getMonth() + 1;
		var d = today.getDate();
		if(m<10){
			m = "0"+m
		}
		if(d<10){
			d="0"+d
		}
		var tmr = y + "/" + m + "/" + d
		//$('#CMTRDATE').val(tmr);
	}
    
	// 日曆欄位參數設定
	function datetimepickerEvent() {
		$(".CMTRDATE").click(function (event) {
			$('#CMTRDATE').datetimepicker('show');
		});
		jQuery('.datetimepicker').datetimepicker({
			timepicker: false,
			closeOnDateSelect: true,
			scrollMonth: false,
			scrollInput: false,
			format: 'Y/m/d',
			lang: '${transfer}',
			viewDate:""
		});
	}	
	// 驗證碼刷新
	function changeCode() {
		$('input[name="capCode"]').val('');
		// 大小版驗證碼用同一個
		console.log("changeCode...");
		$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

		// 登入失敗解遮罩
		unBlockUI(initBlockId);
	}
	
	// 初始化驗證碼
	function initKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
	}
	
	// 生成驗證碼
	function newKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').click(function() {
			$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
		});
	}
	//check N960 BRHCOD
	function docheckN960() {
		var formId = document.getElementById("formId");
		var CUSIDN = formId._CUSIDN.value;
		//打啟用的電文
		$.ajax({
			 	type: "post",
			 	data:{
			 		CUSIDN:CUSIDN,
			 	},
			 	dataType:"text",
  				url:"${__ctx}/ONLINE/APPLY/use_component_step1_idcheck",
  				success:function(data){ 
  					console.log("AJAXDATA >>"+data);
  					callbackn960(data);
  				}, 
		});
		
	}
	//ajax回傳檢核結果，後續處理。
	function callbackn960(response) {
			console.log(response);
			//alert(response.responseText);
			var result = JSON.parse(response);
			console.log(typeof(response));
			console.log(typeof(result));
			console.log(result.data.MSGCOD);
			console.log("BANK >> "+result.data.Bank);
			//先清空原有之"OPTION"內容,並加上一個 "--- 請選擇 ---" 欄位
			if(result!=null && result.result==true)
			{
				console.log("SELECT >> "+$('#BHID').length);
				//清空下拉
				$("#BHID").find("option:selected").text("");
				$("#BHID").empty();
				$("#BHID").length = 0;
				//放預設欄位
				//請選擇分行
				$("#BHID").append($("<option></option>").attr({
					value : "#",
					selected : "selected"
				}).text("---  <spring:message code= "LB.X1003" /> ---"));
				// 帶出"分行 "之下拉式選單
				for(var i=0;i<result.data.Bank.length;i++){
					if(mylocale=="zh_CN"){
					 $("#BHID").append($("<option></option>").attr({
							value : result.data.Bank[i].ADBRANCHID
						}).text(result.data.Bank[i].ADBRANCHSNAME));
					}
					else if(mylocale=="en")
						{
						 $("#BHID").append($("<option></option>").attr({
								value : result.data.Bank[i].ADBRANCHID
							}).text(result.data.Bank[i].ADBRANENGNAME));
						}		
					else
						{
						 $("#BHID").append($("<option></option>").attr({
								value : result.data.Bank[i].ADBRANCHID
							}).text(result.data.Bank[i].ADBRANCHNAME));
						}									
				}
			} else {
				//需改寫:表單新增一個欄位，把全部分行放進session之後可以取來直接顯示
			var formId = document.getElementById("formId");
			var chipbrh = formId.accNo.toString().substring(0, 3);
			$('BHID').options.length = 0;
			(Sys.firefox) ? $('BHID').appendChild(
					document.createElement("option")) : $('BHID').add(
					document.createElement("option"));
			$('BHID').options[0].text = "---  <spring:message code= "LB.X1003" /> ---";
			$('BHID').options[0].value = "#";
			var result = $.map($('BHID').options, function(item, index) {
				return item.value
			}).indexOf(chipbrh);
			if (result > 0)
				$('BHID').options[index].selected = true;
			}
	}
</script>
</head>

<body>
<%@ include file="../component/trading_component.jsp"%>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	<!-- 交易機制所需畫面 -->
	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Register" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<!-- 晶片金融卡申請網路銀行 -->
			<h2><spring:message code= "LB.X1077" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="">
				<input type="hidden" id="isBack" name="isBack" value="">
				<!-- 交易機制所需欄位 -->
				<input type="hidden" id="jsondc" name="jsondc" value='${use_component_step1.data.jsondc}'>
  				<input type="hidden" name="ADOPID" value="NA30">
  				<input type="hidden" name="_braCode" value="${use_component_step1.data.bracode}"> 
  				<input type="hidden" name="_accNo" value="${use_component_step1.data.ACNNO}"> 
  				<input type="hidden" name="CARDNUM" value="${use_component_step1.data.ACNNO}">
  				<input type="hidden" name="readername" value=""> 
  				<input type="hidden" name="braCode" value=""> 
  				<input type="hidden" name="accNo" value="${use_component_step1.data.ACNNO}">
  				<input type="hidden" name="icSeq" value="${use_component_step1.data.iSeqNo}">
  				<input type="hidden" name="chk" value="">
  				<input type="hidden" name="cflg" value="">
  				<input type="hidden" name="BRHCOD" value="">
  				<input type="hidden" name="BRHNAME" value="">
  				
				<input type="hidden" id="ISSUER" name="ISSUER" value="">
				<input type="hidden" id="ACNNO" name="ACNNO" value="${use_component_step1.data.ACNNO}">
				<input type="hidden" id="TRMID" name="TRMID" value="">
				<input type="hidden" id="iSeqNo" name="iSeqNo" value="${use_component_step1.data.iSeqNo}">
				<input type="hidden" id="ICSEQ" name="ICSEQ" value="${use_component_step1.data.iSeqNo}">
				<input type="hidden" id="TAC" name="TAC" value="${use_component_step1.data.TAC}">
				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
				<!--開發用欄位 -->
<!-- 				<input type="hidden" id="ISSUER" name="ISSUER" value="05000000"> -->
<!-- 				<input type="hidden" id="ACNNO" name="ACNNO" value="0000005062433347"> -->
<!-- 				<input type="hidden" id="TRMID" name="TRMID" value="00034808"> -->
<!-- 				<input type="hidden" id="iSeqNo" name="iSeqNo" value="00002532"> -->
<!-- 				<input type="hidden" id="ICSEQ" name="ICSEQ" value="00002532"> -->
<!-- 				<input type="hidden" id="TAC" name="TAC" value="1E506BBFA9ACC4D8"> -->
<!-- 				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value=""> -->
				<!-- 下一步驟需要的 -->
				<input type="hidden" name="UID" value="${use_component_step1.data.CUSIDN}">	
				<input type="hidden" name="CUSIDN" value="${use_component_step1.data.CUSIDN}">
				<div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
                            <div class="ttb-message">
                            <!-- 限個人存戶首次申請 -->
                                <span><spring:message code= "LB.D0987" /></span>
                            </div>
                            <!--存款帳號-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D0988" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
                                      	<input type="text" class="text-input" name="accNo" id="accNo" value="${use_component_step1.data.ACNNO}" disabled>
                                    </div>
								</span>
							</div>
							
							<!--身分證字號-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D0581" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
                                      	<input type="text" class="text-input validate[required, funcCall[validate_checkSYS_IDNO[_CUSIDN]]]"  name="_CUSIDN" id="_CUSIDN" value="" onchange="docheckN960();">
                                    </div>
								</span>
							</div>
							
                                <!--出生年月日-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D0054" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <input class="text-input datetimepicker  validate[validate_CheckSelect['<spring:message code= "LB.D0054" />', CMTRDATE,'#']]" name="CMTRDATE" id="CMTRDATE" type="text" value="" size="10" />
                                        <span class="input-unit CMTRDATE"><img src="${__ctx}/img/icon-7.svg" /></span>
                                    </div>
                                </span>
                            </div>
							
                            <!--聯絡電話-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D0539" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
                                      	<input type="text" class="text-input" name="PHONE_H" id="PHONE_H" value="">
                                    </div>
								</span>
							</div>
                                
							<!--電子郵箱-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D0346" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
                                      	<input type="text" class="text-input validate[required,funcCall[validate_EmailCheck[MAIL]]]" name="MAIL" id="MAIL" value="">
                                    </div>
								</span>
							</div>
							
							<!--金融卡線上申請轉帳功能-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D0993" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
                                        <label for="ATMTRAN">
										  <input type="checkbox" name="ATMTRAN1" id="ATMTRAN" value="Y" onClick="chooseOne(this);"/>
										  <span class="ttb-unit"><spring:message code= "LB.D0324" /></span>
										</label>
                                    </div>
									<div class="ttb-input">
										<label for="ATMTRAN1">
											<input type="checkbox" name="ATMTRAN1" id="ATMTRAN1" value="N" onClick="chooseOne(this);"/>
											<span class="ttb-unit"> <spring:message code= "LB.D0995" /></span>
										</label>
                                    </div>
								</span>
							</div>
							
							    
							<!--Email通知服務-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D0996" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
                                      	<label for="NOTIFY_ACTIVE">
											<input type="checkbox" name="NOTIFY_ACTIVE" id="NOTIFY_ACTIVE" value="">
											<!-- 網路銀行相關服務及優惠訊息 -->
											<span class="ttb-unit"> <spring:message code= "LB.D0997" /></span>
										</label>
                                    </div>
								</span>
							</div>
							
							<!--服務分行-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D0998" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
                                      	<select class="custom-select select-input half-input validate[validate_CheckSelect['<spring:message code= "LB.X1003" />',BHID, '#']]" name="BHID" id="BHID">
											<!-- 請選擇分行 -->
											<option value="#">--- <spring:message code= "LB.X1003" /> ---</option>
											<c:forEach var="dataList" items="${use_component_step1.data.REC}">
											<c:choose>
											<c:when test="${pageContext.response.locale=='zh_CN'}">
											<option value="${dataList.ADBRANCHID}">${dataList.ADBRANCHSNAME}</option>
											</c:when>
											<c:when test="${pageContext.response.locale=='en'}">
											<option value="${dataList.ADBRANCHID}">${dataList.ADBRANENGNAME}</option>									
											</c:when>
											<c:otherwise>
											<option value="${dataList.ADBRANCHID}">${dataList.ADBRANCHNAME}</option>
											</c:otherwise>
											</c:choose>
											
											
											</c:forEach>
										</select>
                                    </div>
								</span>
							</div>
							
							 <!-- 驗證碼 -->
                            <div class="ttb-input-item row" id="chaBlock">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D0032" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
  									<spring:message code="LB.Captcha" var="labelCapCode" />
									<input id="capCode" name="capCode" type="text" class="text-input" maxlength="6" autocomplete="off">
									<img name="kaptchaImage" class="verification-img" src="" />
									<!-- 重新產生驗證碼 -->
									<input type="button" name="reshow" class="ttb-sm-btn btn-flat-orange" onclick="changeCode()" value="<spring:message code= "LB.Regeneration_1" />" />
                                    <!-- 英文不分大小寫，限半型字 -->
                                       <br><span class="" style="color:red;">※ <spring:message code= "LB.D0033" /></span>
                                </span>
                            </div>   
                        </div>
                      <!-- 重新輸入 -->
                        <input class="ttb-button btn-flat-gray" name="CMRESET" type="reset" value=" <spring:message code= "LB.Re_enter" />" />
                      <!-- 確定 -->
                        <input class="ttb-button btn-flat-orange" name="CMSUBMIT" id="CMSUBMIT" type="button" value=" <spring:message code= "LB.Confirm" />" />
                        
                    </div>
                </div>
                <!-- 說明 -->
                <ol class="list-decimal description-list">
   				<p> <spring:message code= "LB.Description_of_page" /></p>             
                <!-- 完成線上申請即可立刻使用網路銀行查詢服務(含存款、放款、基金、外匯、信用卡)、掛失及申請信用卡的電子帳單、補寄帳單、Email繳款通知等相關服務。 -->
                 <li><spring:message code= "LB.use_component_P1_D1" /><font color="#FF0000"><spring:message code= "LB.use_component_P1_D1-1" /></font><spring:message code= "LB.use_component_P1_D1-2" /><font color="#FF0000"><spring:message code= "LB.use_component_P1_D1-3" /></font><spring:message code= "LB.use_component_P1_D1-4" /><font color="#FF0000"><spring:message code= "LB.use_component_P1_D1-5" /></font><spring:message code= "LB.use_component_P1_D1-6" /></li>
		         <!-- 線上申請者無法執行「轉帳」、「申購基金」等功能，如欲增加前述功能，需啟用「金融卡線上申請轉帳功能」或親洽往來分行辦理升級為臨櫃申請用戶，即可使用完整的理財服務。 -->
		          <li> <spring:message code= "LB.Use_Component_P3_D2" /></li>
		          <!-- 本服務限「首次」申請，如網路銀行密碼忘記或失效時，請至往來分行辦理重置密碼。 -->
		          <li><spring:message code= "LB.use_component_P1_D3-1" /><font color="#FF0000"><b><spring:message code= "LB.use_component_P1_D3-2" /></b></font><spring:message code= "LB.use_component_P1_D3-3" /></li>
		          <!-- 申請啟用金融卡線上申請轉帳功能需具備非約定轉帳功能。 -->
		          <li><spring:message code= "LB.Use_Component_P3_D4" /></li>
		 		  <!-- 查詢「網路銀行交易限額」。 -->
		 		  <li> <spring:message code= "LB.Use_Component_P3_D5-1" /><a href="${__ctx}/CUSTOMER/SERVICE/rate_table" target="_blank"><spring:message code= "LB.Use_Component_P3_D5-2" /></a></li>
                </ol>
				
			</form>
		</section>
		</main>
	<!-- 		main-content END -->
	<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>

</html>