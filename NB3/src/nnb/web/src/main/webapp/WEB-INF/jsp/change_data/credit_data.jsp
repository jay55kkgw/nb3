<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/checkIdentity.js"></script>

<script type="text/javascript">

$(document).ready(function() {
	// HTML載入完成後開始遮罩
	setTimeout("initBlockUI()", 50);
	// 開始查詢資料並完成畫面
	setTimeout("init()", 400);
	// 解遮罩
	setTimeout("unBlockUI(initBlockId)", 500);
});

var urihost = "${__ctx}";


function init(){
	// 表單驗證初始化
	$("#formId").validationEngine({ binded: false, promptPosition: "inline" });

	// 確認鍵 click
	goOn();
	//上一頁
	goBack();
	
	fgtxwayValidateEvent();
};

// 確認鍵 Click
function goOn() {
	$("#CMSUBMIT").click( function(e) {
		$("#formId").validationEngine({
			binded : false,
			promptPosition : "inline"
		});
		
		e = e || window.event;
		// 送出進表單驗證前將span顯示
		$("#hideblock").show();
		console.log("submit~~");
		// 表單驗證
		if ( !$('#formId').validationEngine('validate') ) {
			e.preventDefault();
		} 
		else {
			// 解除表單驗證
			$("#formId").validationEngine('detach');
			// 通過表單驗證
			processQuery();
		}
	});
}

//上一頁按鈕 click
function goBack() {
	// 上一頁按鈕
	$("#CMBACK").click(function () {
		// 遮罩
		initBlockUI();
		// 解除表單驗證
		$("#formId").validationEngine('detach');
		// 讓Controller知道是回上一頁
		$('#back').val("Y");
		// 回上一頁
		var action = '${__ctx}/INDEX/index';
		$("#formId").attr("action", action);
		$("#formId").submit();
	});
}

// 通過表單驗證準備送出
function processQuery(){
	var fgtxway = $('input[name="FGTXWAY"]:checked').val();
	console.log("fgtxway: " + fgtxway);
	// 交易機制選項
	switch(fgtxway) {
		case '1':
			// IKEY
			useIKey();
		
			break;
		case '2':
			// 晶片金融卡
			console.log("urihost>>>"+urihost);
			listReaders();
		
    		break;
		default:
		//alert("<spring:message code= "LB.Alert001" />");
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert001' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
	}
}

//交易機制元件--複寫後蓋前，不用拔插卡
// 找出有插卡的讀卡機結束(有找到才會到此FUNCTION)
function findOKReaderFinish(okReaderName){
	// ASSIGN到全域變數
	OKReaderName = okReaderName;
	
	// 此功能不拔插卡直接輸入密碼故註解
// 	removeThenInsertCard();

	// 取得卡片銀行代碼
	getCardBankID();
}
// 交易機制元件--複寫後蓋前，顯示客製訊息
// 驗證卡片密碼結束
// 驗證卡片密碼結束
//function verifyPinFinish(result){
//	// 成功
//	if(result == "true"){
//		// 繼續做
//		CheckIdProcess();
//	}
//}

//CheckIdProcess = function(){
//	showTempMessage(500,"晶片金融卡身份查驗","<br><p>晶片金融卡身份查驗中，請稍候...</p><br><p>查驗期間請勿移除您的晶片金融卡，以免造成驗證錯誤。</p><br><br>","MaskArea",true);
//	// 取得卡片主帳號
//	getMainAccount();
//}

////複寫取得卡片主帳號結束拔插卡動作取消
//function getMainAccountFinish(result){
//	if(window.console){console.log("getMainAccountFinish...");}
//	//成功
//	if(result != "false"){
//		var formId = document.getElementById("formId");
//		formId.ACNNO.value = result;
//		//卡片壓碼結束
//		generateTACFinish(result);
//	}
//	//失敗
//	else{
//		FinalSendout("MaskArea",false);
//	}
//}

function fgtxwayValidateEvent(){
	$('input[name="FGTXWAY"]').change(function(event) {
		if($('input[name="FGTXWAY"]:checked').val()=='1'){
			
		}
		if($('input[name="FGTXWAY"]:checked').val()=='2'){
			
		}
	});
}
</script>
</head>
<body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 通訊資料變更     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0359" /></li>
    <!-- 信用卡帳單地址/電話變更     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0404" /></li>
		</ol>
	</nav>


	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.D0404" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="${__ctx}/CHANGE/DATA/credit_data_detail">
				<input type="hidden" id="ADOPID" name="ADOPID" value="N900">
				<input type="hidden" id="TOKEN" name="TOKEN" value="<c:out value='${fn:escapeXml(sessionScope.transfer_confirm_token)}' />" /><!-- 防止重複交易 -->
				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
				<input type="hidden" id="jsondc" name="jsondc" value="<c:out value='${fn:escapeXml(cd.data.jsondc)}' />">
				<input type="hidden" id="CMTRANPAGE" name="CMTRANPAGE" value="1">
				<input type="hidden" id="ISSUER" name="ISSUER" value="">
				<input type="hidden" id="ACNNO" name="ACNNO" value="">
				<input type="hidden" id="TRMID" name="TRMID" value="">
				<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
				<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
				<input type="hidden" id="TAC" name="TAC" value="">
				<input type="hidden" id="CardPassPassed" name="CardPassPassed" value="">				
				
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">

							<!-- 交易機制 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
										<h4>
											<spring:message code="LB.Transaction_security_mechanism" />
											<!-- 交易機制 -->
										</h4>
								</label>
								</span> 
								<span class="input-block">
									<!-- 使用者是否可以使用IKEY -->
									<c:if test = "${sessionScope.isikeyuser}">
										<div class="ttb-input">
											<label class="radio-block"> <spring:message code="LB.Electronic_signature" />
											<input type="radio" name="FGTXWAY" value="1"> <span class="ttb-radio"></span></label> <!-- 電子簽章(請載入載具i-key) --> 
										</div>
									</c:if>
									<!-- 晶片金融卡 -->
									<div class="ttb-input">
										<label class="radio-block">
											<spring:message code="LB.Financial_debit_card" />
												
											<!-- 只能選晶片金融卡時，預設為非勾選讓使用者點擊後顯示驗證碼-->
											<c:choose>
												<c:when test="${!sessionScope.isikeyuser}">
													<input type="radio" name="FGTXWAY" id="CMCARD" value="2" checked>
												</c:when>
												<c:otherwise>
												 	<input type="radio" name="FGTXWAY" id="CMCARD" value="2" checked>
												</c:otherwise>
											</c:choose>
												
											<span class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>
						</div>
						<input class="ttb-button btn-flat-gray" type="button" name="CMBACK" id="CMBACK" value="<spring:message code="LB.Back_to_previous_page" />" />
						<input type="button" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />

					</div>
				</div>
			</form>
		</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>