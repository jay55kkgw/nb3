<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:block>
<jsp:body>

<c:set var="__resPath" value="${__ctx}/site/admin/main"/>
<c:set var="__baseUrl" value="${__ctx}/site/admin"/>

<%-- <c:set var="__pageTitle" value="Form Template"/> --%>
<%-- <c:set var="__siteName" value="Site Name"/> --%>
<%-- <c:set var="__userDisplayInfo" value="User Display Information"/> --%>
<%-- <c:set var="__appName" value="Applicatin Name"/> --%>
<%-- <c:set var="__appDesc" value="Applicatin Description"/> --%>
<%-- <c:set var="__sidebarMenu" value="sidebar-menu"/> --%>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" CONTENT="-1">		
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="description" content="Form Template" />
    <meta name="keywords" content="login, form, input, submit, button, html5, placeholder" />	
    <title>${__pageTitle}</title>
	<link rel="shortcut icon" href="${__resPath}/dist/img/favicon.png" />
    <!-- Bootstrap 3.3.2 -->
    <link type="text/css" rel="stylesheet" href="${__resPath}/bootstrap/css/bootstrap.min.css"/>
    <!-- Font Awesome Icons -->
    <link type="text/css" rel="stylesheet" href="${__resPath}/plugins/font-awesome/font-awesome.min.css"/>
    <!-- Ionicons -->
    <link type="text/css" rel="stylesheet" href="${__resPath}/plugins/ionicons/ionicons.min.css"/>

	<!-- DATA TABLES -->
    <link type="text/css" rel="stylesheet" href="${__resPath}/plugins/datatables/dataTables.css" />    
    <link type="text/css" rel="stylesheet" href="${__resPath}/plugins/datatables/dataTables.bootstrap.css" />    
    <link type="text/css" rel="stylesheet" href="${__resPath}/plugins/datatables/dataTables.tableTools.css" />    
	<link type="text/css" rel="stylesheet" href="${__resPath}/plugins/datatables/dataTables.responsive.css" /> 

    <!-- Theme style -->
    <link type="text/css" rel="stylesheet" href="${__resPath}/dist/css/AdminLTE.min.css"/>    
    <link type="text/css" rel="stylesheet" href="${__resPath}/dist/css/skins/skin-blue.min.css"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="${__resPath}/plugins/html5shiv/html5shiv.js"></script>
        <script src="${__resPath}/plugins/respond/respond.min.js"></script>
    <![endif]-->

	<link type="text/css" rel="stylesheet" href="${__resPath}/dist/css/site-admin.css" /> 

  </head>
  <body class="skin-blue">
    <div class="wrapper">
      
      <header class="main-header">
        <a href="#" class="logo">${__siteName}</a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
			
              <!-- User Account -->
              <li class="user">
                <a href="#">
                  <span class="hidden-xs">${__userDisplayInfo}</span>
                </a>
              </li>			  
              <li>
                <a href="${__baseUrl}/logout.do">
                  <span class="hidden-xs">Logout</span>
                </a>
              </li>
			  
            </ul>
			
          </div>
        </nav>
      </header>
	  
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar -->
        <section class="sidebar">
          
          
		  <!-- sidebar menu:  -->
          <ul class="sidebar-menu">
			${__sidebarMenu}
		  </ul>
		   
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Right side column. Contains the navbar and content of the page -->
      <div class="content-wrapper">
	  
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            ${__appName}
            <small>${__appDesc}</small>
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
        	${__content}
        </section><!-- /.content -->
		
      </div><!-- /.content-wrapper -->
	  
      <footer class="main-footer">        
        ${__footer}
      </footer>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.3 -->
    <script type="text/javascript" src="${__resPath}/plugins/jQuery/jQuery-2.1.3.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script type="text/javascript" src="${__resPath}/bootstrap/js/bootstrap.min.js"></script>
    
    <script type="text/javascript" src="${__resPath}/plugins/datatables/jquery.dataTables.js"> </script>
    <script type="text/javascript" src="${__resPath}/plugins/datatables/dataTables.bootstrap.js" ></script>
	<script type="text/javascript" src="${__resPath}/plugins/datatables/dataTables.tableTools.js"></script>
	<script type="text/javascript" src="${__resPath}/plugins/datatables/dataTables.responsive.js"></script>
        
    <script type="text/javascript" src="${__resPath}/plugins/jQuery-blockUI/jquery.blockUI.js" ></script>
    
    <script type="text/javascript" src="${__resPath}/dist/js/form-template.js"></script>
    
	<!-- AP -->
	<script type="text/javascript" src="${__resPath}/dist/js/fstop.web-1.0.0.js"></script>
	<script type="text/javascript">
		fstop.web.gContext = "${__baseUrl}";  //"/fstop-util-web";
		fstop.web.gLang2 = "${__userLocale ? __userLocale : 'zh-TW'}";  //"zh-TW";
    </script>
	
    <script type="text/javascript" src="${__resPath}/dist/js/site-admin-common.js"></script>

    <!-- 此處放置共用的 js -->
	<script type="text/javascript">
    </script>
	${__js}	
	
  </body>
</html>

</jsp:body>
</t:block>

