<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<!--舊版驗證-->
<script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	
	$("#CMSUBMIT").click(function(){
		//臺幣轉出帳號
		if(!CheckSelect("SVACN","<spring:message code= "LB.W1496" />","#")){ 
	   		return false;
		}
		//黃金轉入帳號
	   	if(!CheckSelect("ACN","<spring:message code= "LB.W1497" />","#")){ 
	   		return false;
	   	}
	   	var TRNGD = $("#TRNGD").val();
	   	//請輸入整數的買進公克數
		if(TRNGD.indexOf(".") != -1){
			//alert("<spring:message code= "LB.X0944" />");
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.X0944' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
			);
			return false;
		}
	   	//買進公克數
	   	if (!CheckNumber("TRNGD","<spring:message code= "LB.W1498" />",false)){
	   	    return false;   	 	
	   	}
		if(parseInt(TRNGD) < 1 || parseInt(TRNGD) > 50000){
			//每筆最低交易數量為 1 公克，最高交易數量為 50,000 公克。
			//alert("<spring:message code= "LB.X0945" />");
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.X0945' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
			);
			return false;
		}
		
		$('#formID').attr("action","${__ctx}/GOLD/TRANSACTION/gold_buy_confirm");
		$('#formID').removeAttr("target");
		$("#formID").submit();
	});
	$("#resetButton").click(function(){
		$("#SVACN").val("#");
		$("#ACN").val("#");
		$("#TRNGD").val("");
	});
	
});
function displayAcn(){
	if($("#SVACN").val() != "#"){
		var URI = "${__ctx}/GOLD/TRANSACTION/getGoldTradeTWAccountListAjax";
		var rqData = {SVACN:$("#SVACN").val()};
		fstop.getServerDataEx(URI,rqData,false,getGoldTradeTWAccountListAjaxFinish);
	}
}
function getGoldTradeTWAccountListAjaxFinish(data){
	if(data.result == true){
		var goldTradeTWAccountList = $.parseJSON(data.data);
		
		$("#ACN").html("");
		//請選擇帳號
		var ACNHTML = "<option value='#'>---<spring:message code="LB.Select_account"/>---</option>";
		for(var x=0;x<goldTradeTWAccountList.length;x++){
			ACNHTML += "<option value='" + goldTradeTWAccountList[x].ACN + "'>" + goldTradeTWAccountList[x].ACN + "</option>";
		}
		$("#ACN").html(ACNHTML);
	}
	else{
		//無法取得黃金轉入帳號資料
		//alert("<spring:message code= "LB.X0946" />");
		errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.X0946' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
		);
	}
}

function priceQuery(){
	$('#formID').attr("action","${__ctx}/GOLD/PASSBOOK/history_price_query_result");
	$('#formID').attr("target","_blank");
	$('#formID').submit();
	
}


</script>
</head>
<body>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %> 
	<!--header-->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 黃金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2272" /></li>
    <!-- 黃金申購     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1493" /></li>
		</ol>
	</nav>

	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!--快速選單及主頁內容-->
		<main class="col-12"> 
			<!--主頁內容-->
			<section id="main-content" class="container">
			<!-- 黃金買進 -->
				<h2><spring:message code="LB.W1493"/></h2><i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00"></i>
                <form id="formID" action="${__ctx}/GOLD/TRANSACTION/gold_buy_confirm" method="post">
                	<input type="hidden" name="ADOPID" value="N09001_C"/>
					<input type="hidden" name="TRNCOD" value="01"/>	
					<input type="hidden" name="FGTXWAY" value="0"/>
					<input type="hidden" name="QUERYTYPE" value="LASTMON">
	                <div class="main-content-block row">
	                    <div class="col-12 tab-content">
	                        <div class="ttb-input-block">
								<!--交易日期-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
										<label>
	                                        <h4><spring:message code="LB.Transaction_date"/></h4>
	                                    </label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
	                                       	<span>
	                                       		${nowDate}<br/>
	                                       		<!-- (營業日09:00分起至15:30分) -->
	                                       		<font color="red">(<spring:message code="LB.X0943"/>)</font>
	                                       	</span>
										</div>
									</span>
								</div>
								<!--臺幣轉出帳號-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
		                                <label>
											<h4><spring:message code="LB.W1496"/></h4>
										</label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
	                                       	<select name="SVACN" id="SVACN" onchange="displayAcn()" class="custom-select select-input half-input">
												<!-- 請選擇帳號 -->
												<option value="#">---<spring:message code="LB.Select_account"/>---</option>
												<c:forEach var="dataMap" items="${goldTradeAccountList}">
													<option value="${dataMap.SVACN}">${dataMap.SVACN}</option>
												</c:forEach>
											</select>
										</div>
									</span>
								</div>
								 <!--黃金轉入帳號-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
										<label>
											<h4><spring:message code="LB.W1497"/></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<select name="ACN" id="ACN" class="custom-select select-input half-input">
												<!-- 請選擇帳號 -->
												<option value="#">---<spring:message code="LB.Select_account"/>---</option>
											</select>
										</div>
									</span>
								</div>
								<!--買進公克數-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
										<label>
											<h4><spring:message code="LB.W1498"/></h4>
										</label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
										<!-- 公克 -->
	                                       	<input type="text" id="TRNGD" name="TRNGD" maxLength="5" style="width: 100px" class="text-input" >&nbsp;&nbsp;<spring:message code="LB.W1435"/> <br>
										</div>
									</span>
								</div>
								 <div class="ttb-input-item row">
									  <span class="input-title">
										</span>
										<span class="input-block">
										<div class="ttb-input">
								 	<!-- 黃金重量換算表 -->
											<a href="https://www.tbb.com.tw/web/guest/-260" target="_blank"><spring:message code="LB.W1500"/></a>&nbsp;&nbsp;
											<!-- 走勢 -->
											<a href="javascript:void(0)" onclick="priceQuery()"><spring:message code="LB.W1501"/></a>
								 		</div>
									</span>
								 </div>
							</div>
							<!-- 重新輸入 -->
	                				<input type="button" id="resetButton" value="<spring:message code="LB.Re_enter"/>" class="ttb-button btn-flat-gray"/>	
	                			<!-- 確定 -->
	                				<input type="button" id="CMSUBMIT" value="<spring:message code="LB.Confirm"/>" class="ttb-button btn-flat-orange"/>
	                    </div>
	                </div>
					<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /></p><!-- 說明 -->
						<!-- 每筆最低交易數量為1公克，每日累計買進最高交易數量為50,000公克。 -->
							<li><spring:message code= "LB.Gold_Buy_P1_D1" /></li>
							<!-- 申購黃金數量達3,000公克（含）以上時，特予以優惠折讓，查看折讓率請按 折讓表  -->
							<li><spring:message code= "LB.Gold_Buy_P1_D2-1" /><a href="https://www.tbb.com.tw/web/guest/-264" target="_blank"><spring:message code= "LB.Gold_Buy_P1_D2-2" /></a></li>
							<!-- 黃金存摺不支付利息，黃金價格有漲有跌，投資時可能產生收益或損失，敬請慎選買賣時機，並承擔風險。 -->
							<li><spring:message code="LB.Gold_Buy_P1_D3"/></li>
							<!-- 「黃金買進」免手續費。 -->
							<li><spring:message code="LB.Gold_Buy_P1_D4"/></li>
						</ol>
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>