<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp" %>

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
	}
	
</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Register" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
			<!-- 信用卡額度調升 -->
				<h2><spring:message code="LB.Credit_Card_Limit_Increase" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="">
                <div class="main-content-block row">
	                <div class="col-12 terms-block">
		                <div class="ttb-terms">
		                    <div class="ttb-result-list terms">
		                    	<div style="padding-top:30px">
			                    	<p class="terms-version terms-text-bold"> (版號：10912) </p>
			                        <h4><p class="terms-text-center terms-text-bold">臺灣中小企業銀行履行個人資料保護法告知事項</p></h4>
		                    	</div>
		                        <p>由於個人資料之蒐集，涉及申請人(含附卡申請人)的隱私權益，臺灣中小企業銀行股份有限公司(以下稱本行)向申請人(含附卡申請人)蒐集個人資料時，依據個人資料保護法(以下稱個資法)第八條第一項規定，應明確告知申請人(含附卡申請人)下列事項，請申請人(含附卡申請人)詳閱:</p>
								
		                        <ul class="ttb-result-list terms">
		                            <li data-num="一、"><span>有關本行蒐集申請人(含附卡申請人)個人資料之目的、個人資料類別及個人資料利用之期間、地區、對象及方式等內容如下:</span>
			                            <ul>
				                            <li data-num="（一）"><span>蒐集之目的：</span>
				                            	<ul>
				                            		<li data-num="1.">業務特定目的及代號:022外匯業務、067信用卡、現金卡、轉帳卡或電子票證業務、082借款戶與存款戶存借作業綜合管理、088核貸與授信業務、106授信業務、154徵信、181其他經營合於營業登記項目或組織章程所定之業務（例如：共同行銷或合作推廣業務等。）</li>
				                            		<li data-num="2.">共通特定目的及代號:040行銷、059金融服務業依法令規定及金融監理需要，所為之蒐集處理及利用、060金融爭議處理、063非公務機關依法定義務所進行個人資料之蒐集處理及利用、069契約、類似契約或其他法律關係管理之事務、090消費者、客戶管理與服務、091消費者保護、098商業與技術資訊、104帳務管理及債權交易業務、136資(通)訊與資料庫管理、137資通安全與管理、157調查、統計與研究分析、182其他諮詢與顧問服務。</li>
				                            	</ul>
											</li>
											<li data-num="（二）">個人資料類別：姓名、身分證統一編號、性別、出生年月日、通訊方式及其他詳如相關業務申請書或契約書之內容，並以本行與客戶往來之相關業務、帳戶或服務及自客戶或第三人處（例如：財團法人金融聯合徵信中心）所實際蒐集之個人資料為準。</li>
											<li data-num="（三）">利用之期間：特定目的存續期間／依相關法令所定(例如商業會計法等)或因執行業務所必須之保存期間或依個別契約就資料之保存所定之保存年限。(以期限最長者為準)</li>
											<li data-num="（四）">利用之地區：下列對象之國內及國外所在地。</li>
											<li data-num="（五）">利用之對象：
												<ul>
													<li data-num="1、">本行(含受本行委託處事務之委外機構)。</li>
													<li data-num="2、">依法令規定利用之機構(例如：本行母公司或所屬金融控股公司等)。</li>
													<li data-num="3、">其他業務相關之機構（例如：通匯行、財團法人金融聯合徵信中心、財團法人聯合信用卡處理中心、台灣票據交換所、財金資訊股份有限公司、信用保證機構、信用卡國際組織、收單機構暨特約商店等)。</li>
													<li data-num="4、">依法有權機關或金融監理機關。</li>
													<li data-num="5、">客戶所同意之對象（例如本行共同行銷或交互運用客戶資料之公司、與本行合作推廣業務之公司等）。</li>
												</ul>
											</li>
											<li data-num="（六）">利用之方式：符合個人資料保護相關法令以自動化機器或其他非自動化之利用方式。</li>
										</ul>
									</li>
		                            <li data-num="二、"><span>依據個資法第三條規定，申請人(含附卡申請人)就本行保有申請人(含附卡申請人)之個人資料得行使下列權利：</span>
		                            	<ul>
		                            		<li data-num="（一）">除有個資法第十條所規定之例外情形外，得向本行查詢、請求閱覽或請求製給複製本，惟本行依個資法第十四條規定得酌收必要成本費用。</li>
											<li data-num="（二）">得向本行請求補充或更正，惟依個資法施行細則第十九條規定，申請人(含附卡申請人)應適當釋明其原因及事實。</li>
											<li data-num="（三）">本行如有違反個資法規定蒐集、處理或利用申請人(含附卡申請人)之個人資料依個資法第十一條第四項規定，申請人(含附卡申請人)得向本行請求停止蒐集。</li>
											<li data-num="（四）">依個資法第十一條第二項規定，個人資料正確性有爭議者，得向本行請求停止處理或利用 申請人(含附卡申請人)之個人資料。惟依該項但書規定，本行因執行業務所必須或經申請人(含附卡申請人)書面同意，並經註明其爭議者，不在此限。</li>
											<li data-num="（五）">依個資法第十一條第三項規定，個人資料蒐集之特定目的消失或期限屆滿時，得向本行請求刪除、停止處理或利用申請人(含附卡申請人)之個人資料。惟依該項但書規定，本行因執行業務所必須或經申請人(含附卡申請人)書面同意者，不在此限。</li>
		                            	</ul>
		                            </li>
		                            <li data-num="三、"><span>申請人(含附卡申請人)如欲行使上述個資法第三條規定之各項權利，有關如何行使之方式，得向本行客服(0800-00-7171）詢問或於本行網站（網址：http://www.tbb.com.tw）查詢。</span></li>
		                            <li data-num="四、"><span>申請人(含附卡申請人)得自由選擇是否提供相關個人資料及類別，惟申請人(含附卡申請人)所拒絕提供之個人資料及類別，如果是辦理業務審核或作業所需之資料，本行可能無法進行必要之業務審核或作業而無法提供申請人(含附卡申請人)相關服務或無法提供較佳之服務，敬請見諒。</span></li>
									
								</ul>
								<!-- 取消 -->
		                    </div>
		                </div>
						<input class="ttb-button btn-flat-gray" type="button" name="CMBACK" id="CMBACK" value="<spring:message code="LB.X1572" />" onclick="window.close();"/>
	                </div>
                </div>
                </form>
			</section>
		</main>

	<%@ include file="../index/footer.jsp"%>
</body>
</html>