<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<!--舊版驗證-->
<script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//開始查詢資料並完成畫面
	setTimeout("init()",20);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	
	//控制後收提醒彈窗
// 	afterCheck=0;
	var mail = '${sessionScope.dpmyemail}';
// 	mail='';
	if(${ConfirmFlag == true}){
		//<!-- 未填寫基金風險確認書，故交易未完成 -->
		errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.Alert110' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
		);
		$("#errorBtn1").click(function(e) {
			$('#error-block').hide();
			if( ''== mail){
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.X2637' />"], 
						'<spring:message code= "LB.Confirm" />', 
						null
					);
				$("#errorBtn1").click(function(e) {
					$('#error-block').hide();
				});
			}
		});
	}else{
		if( ''== mail){
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.X2637' />"], 
					'<spring:message code= "LB.Confirm" />', 
					null
				);
			$("#errorBtn1").click(function(e) {
				$('#error-block').hide();
			});
		}
	}
	
	if($("#SLSNO").val() == '' && "${EMPNO}" != '') {
		$("#SLSNO").val("${EMPNO}");
	}
	
	$("#CMSUBMIT").click(function(){
		var RISK7 = "${RISK7}";
		
		if(!CheckSelect("COMPANYCODE","<spring:message code= "LB.W1069" />","#")){
			return false;
		} 
		if(!CheckSelect("TRANSCODE","<spring:message code= "LB.W0025" />","#")){ 
			return false;
		}
		
		if( $('#SLSNO').val() != '') {
			if(!CheckNumberAndLen("SLSNO", "<spring:message code= "LB.X2589" />", true, 6)) {
				return false;
			}
		}
		
		var feeType = $('input[name=FEE_TYPE]:checked').val();
		var min = $('#MIN').val();
		var plus = $('#PLUS').val();
		var min_fmt = $('#MIN_FMT').val();
		var plus_fmt = $('#PLUS_FMT').val();

		if(!CheckAmountForAfterFund("AMT3","<spring:message code= "LB.W1074" />",min,plus,min_fmt,plus_fmt)){
			return false;
		}	
		if(!CheckNumber("YIELD","<spring:message code= "LB.X1502" />",true)){
			return false;
		}
		if(!CheckNumber("STOP","<spring:message code= "LB.X1503" />",true)){
	   		 return false;
		}
		if(parseInt($("#STOP").val()) > 100){
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert111' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
			);
			return false;      	  
		}

		if($("input[name=TYPE]:checked").val() == "C" && parseInt($("#AMT3").val()) > 5000000){
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert112' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
			);
			return false;
		} 
		if(RISK7 == "4" && $("input[name=TYPE]:checked").val() == "C" && parseInt($("#AMT3").val()) <= 3000000){
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert113' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
			);
			return false;		
		}  	     		    		            

		//台幣扣款
		if($("input[name=TYPE]:checked").val() == "C"){
			if($("#COUNTRYTYPE").val() == "" || $("#COUNTRYTYPE").val() == " "){
				$("#COUNTRYTYPE").val("N");
				$("#COUNTRYTYPE1").val("N");
			}	
			else if($("#COUNTRYTYPE").val() == "B"){
				$("#COUNTRYTYPE").val("N");
				$("#COUNTRYTYPE1").val("B");
			}
			else{
				$("#COUNTRYTYPE").val("C");
				$("#COUNTRYTYPE1").val("C");
			}
		}
		//外幣扣款
		else{
			if($("#COUNTRYTYPE").val() == "B"){
				$("#COUNTRYTYPE").val("Y");
				$("#COUNTRYTYPE1").val("B");
			}	
			else{
				$("#COUNTRYTYPE").val("Y");
				$("#COUNTRYTYPE1").val("Y");
			}	        			
		}
		var hasRisk = checkRisk($("#RISK").val(),$("#FDINVTYPE").val());
		//基金風險警告，投資屬性不合
		if(hasRisk == true && RISK7.length == 0){
			$("#formID").attr("action","${__ctx}/FUND/TRANSFER/fund_risk_alert?TXID=C016&ALERTTYPE=1");
		//基金風險確認書
		}else{
			$("#COUNTRY").val($("#COUNTRYTYPE1").val());
			$("#formID").attr("action","${__ctx}/FUND/TRANSFER/fund_risk_confirm?TXID=C016");
		}
		$("#formID").submit();
	});
	$("#resetButton").click(function(){
		$("#AMT3").val("");
		$("#YIELD").val("");
		$("#STOP").val("");
		$("#COMPANYCODE").val("#");
		$("#TRANSCODE").val("#");
	});
});
function init(){
	initFootable();
}
function queryFundCompany(){
	$("#COMPANYCODE").val("#");
	$("#TRANSCODE").val("#");
	$("#TRANSCRY").html("");
	
	$("#alertMsg2").html("");
	$("#alertMsg3").html("");

	var feeType = $('input[name=FEE_TYPE]:checked').val();
	var countryType = $('input[name=TYPE]:checked').val();
	alertMsgHTML = '<strong><font color="red"><spring:message code= "LB.X2486" /></font></strong>';
	if(feeType=="A"){
		$("#alertMsg").html(alertMsgHTML);
	}else{
		$("#alertMsg").html('');
	}
	
	if(countryType == "C"){
		$("#HTELPHONE").val("${ACN1}");
	}
	else{ 
		$("#HTELPHONE").val("${ACN2}");
	}

	$("#ACNTD").html($("#HTELPHONE").val());
	
	var URI = "${__ctx}/FUND/PURCHASE/getFundCompanyDataAjax";
	var rqData = {FEETYPE:feeType,COUNTRYTYPE:countryType};
	fstop.getServerDataEx(URI,rqData,false,queryFundCompanyFinish);
}
function queryFundCompanyFinish(data){
	if(data.result == true){
		var fundCompanyData = $.parseJSON(data.data);
		
		var COMPANYCODEHTML = "<option value='#'>---<spring:message code= "LB.Select" />---</option>";
		
		var seriesName;
		var typeName;
		
		var feeType = $('input[name=FEE_TYPE]:checked').val();
		var countryType = $('input[name=TYPE]:checked').val();
		
		
		for(var x=0;x<fundCompanyData.length;x++){
			if(fundCompanyData[x].COUNTRYTYPE == "C" || fundCompanyData[x].COUNTRYTYPE == "B"){
				seriesName = "<spring:message code= "LB.X1505" />";
				if(countryType=="C" && feeType=="A"){
					typeName = " <spring:message code= "LB.X2487" />";
				}
				if(countryType=="Y" && feeType=="A"){
					typeName = " <spring:message code= "LB.X2487" />";
				}
			}
			else{
				seriesName = "<spring:message code= "LB.X1506" />";
				typeName = " <spring:message code= "LB.X2487" />";
			}
			
			if(feeType=="A"){
				COMPANYCODEHTML += "<option value='" + fundCompanyData[x].COMPANYCODE + "'>" + fundCompanyData[x].COMPANYCODE +" "+ seriesName + " "+fundCompanyData[x].COMPANYSNAME + typeName + "</option>";
			}else{
				COMPANYCODEHTML += "<option value='" + fundCompanyData[x].COMPANYCODE + "'>" + fundCompanyData[x].COMPANYCODE +" " + seriesName + " "+fundCompanyData[x].COMPANYSNAME + "</option>";
			}
		}
		$("#COMPANYCODE").html(COMPANYCODEHTML);
	}
	else{
		errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.Alert114' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
		);
	}
}
function getFundDataByCompany(){
	
	$("#alertMsg2").html("");
	$("#alertMsg3").html("");
	if($("#COMPANYCODE").val() != "#"){
		
		var feeType = $('input[name=FEE_TYPE]:checked').val();
		
		var URI = "${__ctx}/FUND/PURCHASE/getFundDataByCompanyAjax";
		var rqData = { FEETYPE:feeType , COMPANYCODE:$("#COMPANYCODE").val(), REGULAR:"N"};
		fstop.getServerDataEx(URI,rqData,false,getFundDataByCompanyFinish);
	}else{
		
	}
}
function getFundDataByCompanyFinish(data){
	if(data.result == true){
		var fundDatas = $.parseJSON(data.data);
		
		var TRANSCODEHTML = "<option value='#'>---<spring:message code= "LB.Select" />---</option>";
		
		for(var x=0;x<fundDatas.length;x++){
			
			if(fundDatas[x].FUNDMARK == "Y" || fundDatas[x].FUNDMARK == "A"){
				TRANSCODEHTML += "<option value='" + fundDatas[x].TRANSCODE + "'>（" + fundDatas[x].TRANSCODE + "）" + fundDatas[x].FUNDLNAME + "</option>";
			}
		}
		$("#TRANSCODE").html(TRANSCODEHTML);
	}
	else{
		errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.Alert115' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
		);
	}
}
function getFundData(){
	$("#alertMsg2").html("");
	$("#alertMsg3").html("");
	
	if($("#TRANSCODE").val() != "#"){
		var URI = "${__ctx}/FUND/TRANSFER/getFundDataAjax";
		var rqData = {INTRANSCODE:$("#TRANSCODE").val()};
		fstop.getServerDataEx(URI,rqData,false,getFundDataFinish);
	}
}
function getFundDataFinish(data){
	if(data.result == true){
		var fundData = $.parseJSON(data.data);
		
		$("#RISKSHOW").html(fundData.RISK);
		
		var checkType = $("input[name=TYPE]:checked").val();
		
		if(checkType == "C"){
			$("#TRANSCRY").html("<spring:message code= "LB.NTD" />");
		}
		else{
			$("#TRANSCRY").html(fundData.ADCCYNAME);
		}
		
		$("#COUNTRYTYPE").val(fundData.COUNTRYTYPE);
		$("#FUNDLNAME").val(fundData.FUNDLNAME);
		$("#RISK").val(fundData.RISK);
		$("#FUS98E").val(fundData.FUS98E);
		$("#FUNDT").val(fundData.FUNDT);
		
		var feeType = $('input[name=FEE_TYPE]:checked').val();
		var countryType = $('input[name=TYPE]:checked').val();
		
		if(countryType =='C'){
			$("#MIN").val(fundData.CBAMT);
			$("#MIN_FMT").val(fundData.CBAMT_FMT);
			$("#PLUS").val(fundData.CBTAMT);
			$("#PLUS_FMT").val(fundData.CBTAMT_FMT);	
			alertMsg2HTML = '<strong><font color="red"><spring:message code= "LB.X2488" /> : '+ '<font color="red"><spring:message code= "LB.X2328" /> '+ $("#MIN_FMT").val() +'&nbsp&nbsp <spring:message code= "LB.X2489" /> : '+ '<font color="red"><spring:message code= "LB.X2328" /> ' + $("#PLUS_FMT").val() +'</font></strong>'
			$("#alertMsg2").html(alertMsg2HTML);
		}
		if(countryType =='Y'){
			$("#MIN").val(fundData.YBAMT);
			$("#MIN_FMT").val(fundData.YBAMT_FMT);
			$("#PLUS").val(fundData.YBTAMT);
			$("#PLUS_FMT").val(fundData.YBTAMT_FMT);
			alertMsg2HTML = '<strong><font color="red"><spring:message code= "LB.X2488" /> : ' + fundData.ADCCYNAME + ' ' + $("#MIN_FMT").val() +'&nbsp&nbsp <spring:message code= "LB.X2489" /> : '+ fundData.ADCCYNAME + ' ' + $("#PLUS_FMT").val() +'</font></strong>'
			$("#alertMsg2").html(alertMsg2HTML);
		}

		if(fundData.FUNDT == '1') {
			alertMsg3HTML = '<br><strong><font color="red"><spring:message code= "LB.X2610" /></font></strong>'
			$("#alertMsg3").html(alertMsg3HTML);
		} else {
			$("#alertMsg3").html("");
		}
	}
	else{
		errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.Alert115' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
		);
	}
}

//營業時間
function workTime(){
	errorBlock(
			["<spring:message code= 'LB.X2371' />"], 
			["1.<spring:message code= 'LB.X2372' />","2.<spring:message code= 'LB.X2373' />"],
			null,
			'<spring:message code= "LB.Quit" />', 
			null
	);
}
</script>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Funds" /></li>
    <!-- 基金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1064" /></li>
    <!-- 單筆申購     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1065" /></li>
		</ol>
	</nav>

	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.W1065" /></h2>
				<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
					<form id="formID" method="post">
						<input type="hidden" name="ADOPID" value="C016"/>
						<input type="hidden" id="FDINVTYPE" name="FDINVTYPE" value="${FDINVTYPE}"/>
						<input type="hidden" name="ACN1" value="${ACN1}"/>
						<input type="hidden" name="ACN2" value="${ACN2}"/>
						<input type="hidden" id="HTELPHONE" name="HTELPHONE" value="${ACN1}"/>
						<input type="hidden" id="COUNTRYTYPE" name="COUNTRYTYPE"/>
						<input type="hidden" id="COUNTRYTYPE1" name="COUNTRYTYPE1"/>
						<input type="hidden" id="COUNTRY" name="COUNTRY"/>
						<input type="hidden" name="BILLSENDMODE" value="1"/>
						<input type="hidden" id="FUNDLNAME" name="FUNDLNAME"/>		
						<input type="hidden" name="CUTTYPE" value="${CUTTYPEString}"/>		
						<input type="hidden" name="BRHCOD" value="${BRHCOD}"/>	
						<input type="hidden" id="RISK" name="RISK"/>	
						<input type="hidden" name="SALESNO" value="${EMPNO}"/>								
						<input type="hidden" id="FUS98E" name="FUS98E"/>
						<input type="hidden" name="PRO" value="${RISK7}"/>
						<input type="hidden" name="KYCDATE" value="${KYCDAT}"/>
						<input type="hidden" name="WEAK" value="${WEAK}"/>
						<input type="hidden" id="MIN" name="MIN" value=""/>
						<input type="hidden" id="PLUS" name="PLUS" value=""/>
						<input type="hidden" id="MIN_FMT" name="MIN_FMT" value=""/>
						<input type="hidden" id="PLUS_FMT" name="PLUS_FMT" value=""/>
						<input type="hidden" id="Step" name="Step" value="1"/>
						<input type="hidden" id="KYCNO" name="KYCNO" value="${KYCNO}" />
						<input type="hidden" id="FUNDT" name="FUNDT" />
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<!-- 客戶姓名 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1066" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${hiddenNAME}</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 客戶投資屬性 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1067" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${FDINVTYPEChinese}</span>
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 手續費 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code= "LB.X2492" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<label class="radio-block"><spring:message code= "LB.X2491" />
                                            	<input type="radio" name="FEE_TYPE" value='B' onclick="queryFundCompany()" checked>
												<span class="ttb-radio"></span>
                                        	</label>
                                        	<label class="radio-block"><spring:message code= "LB.X2490" />
                                            	<input type="radio" name="FEE_TYPE" value='A' onclick="queryFundCompany()">
												<span class="ttb-radio"></span>
                                        	</label>
                                        	<div id="alertMsg" name="alertMsg"></div>
                                		</div>
                               		</span>
                            	</div>
								<!-- 扣款方式 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.D0173" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<label class="radio-block"><spring:message code="LB.W1089" />
                                            	<input type="radio" name="TYPE" value='C' onclick="queryFundCompany()" checked>
												<span class="ttb-radio"></span>
                                        	</label>
                                        	<label class="radio-block"><spring:message code="LB.W1090" />
                                            	<input type="radio" name="TYPE" value='Y' onclick="queryFundCompany()">
												<span class="ttb-radio"></span>
                                        	</label>
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 基金公司名稱 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1069" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<select class="custom-select select-input half-input " style="width:auto" name="COMPANYCODE" id="COMPANYCODE" onchange="getFundDataByCompany()">
                                            	<option value="#">---<spring:message code= "LB.Select" />---</option>
                                        			<c:forEach var="fundCompany" items="${finalFundCompanyList}">
														<option value="${fundCompany.COMPANYCODE}">${fundCompany.seriesName} ${fundCompany.COMPANYSNAME}</option>
													</c:forEach>
                                        	</select>
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 扣帳帳號 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W0702" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span id="ACNTD">${ACN1}</span>
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 基金名稱 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W0025" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<select class="custom-select select-input half-input " style="width:auto" name="TRANSCODE" id="TRANSCODE" onchange="getFundData()">
                                            	<option value="#">---<spring:message code= "LB.Select" />---</option>
                                        	</select>
                                		</div>
                                		<div class="ttb-input">
                                			<span class="input-unit"><spring:message code="LB.W1073" />：<font id="RISKSHOW"></font></span>
                                		</div>
                               			<div id="alertMsg3" name="alertMsg3"></div>
                               		</span>
                            	</div>
                            	<!-- 轉介人員(6碼) -->
								<div class="ttb-input-item row">
                                	<span class="input-title">
	                                	<label>
	                                        <h4><spring:message code="LB.X2589" /></h4>
	                                    </label>
                                    </span>
                                	<span class="input-block">
                                		<div class="ttb-input">
											<input type="text" class="text-input" size="6" maxlength="6" id="SLSNO" name="SLSNO"/>
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 申購金額 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1074" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
											<input type="text" class="text-input" maxLength="9" size="15" id="AMT3" name="AMT3"/>
                                		</div>
                                		<div id="alertMsg2" name="alertMsg2"></div>
                               		</span>
                            	</div>
                            	<!-- 停利通知設定 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1075" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<input type="text" class="text-input" maxLength="3" size="4" id="YIELD" name="YIELD"/>
                                        	<span class="input-unit">％</span>
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 停損通知設定 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1076" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<input type="text" class="text-input" maxLength="3" size="4" id="STOP" name="STOP"/>
                                        	<span class="input-unit">％</span>
                                		</div>
                               		</span>
                            	</div>
                            </div>
							<input type="button" id="resetButton" value="<spring:message code="LB.Re_enter"/>" class="ttb-button btn-flat-gray"/>	
							<input type="button" id="CMSUBMIT" value="<spring:message code="LB.Confirm"/>" class="ttb-button btn-flat-orange"/>
						</div>
					</div>	
				</form>
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>
					<li><span><spring:message code="LB.fund_purchase_P1_D1" /></span></li>
					<li><span><a href="javascript:void(0)" onclick="workTime()"><spring:message code="LB.fund_purchase_P1_D2" /></a></span></li>
					<li><span><spring:message code="LB.fund_purchase_P1_D3" /></span></li>
					<li><span><spring:message code="LB.fund_purchase_P1_D4" /></span></li>
					<li><span><spring:message code="LB.fund_purchase_P1_D5-1" /><a href="${__ctx}/CUSTOMER/SERVICE/purchase_table" target="_blank"><spring:message code="LB.fund_purchase_P1_D5-2" /></a></span></li>
				</ol>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>
