<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/fullcalendar.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/fullcalendar.print.min.css" rel="stylesheet" media="print">
<script type="text/javascript" src="${__ctx}/js/moment.min.js"></script>
<script type="text/javascript" src="${__ctx}/js/fullcalendar.min.js"></script>
<script type="text/javascript" src="${__ctx}/js/fullcalendar.zh-cn.js"></script>
<script type="text/javascript" src="${__ctx}/js/fullcalendar.zh-tw.js"></script>


<style type="text/css">
	.fast-btn-blcok {
		position: absolute;
		top: auto;
		bottom: auto;
		left: auto;
		right: 20%;
		min-width: 40%;
		width:55%;
		margin: 0 auto;
		background-color: #fbf8e9;
		z-index: 999;
	}
	/* 箭頭上-邊框 */
	.fast-btn-blcok .arrow_t_out {
		width: 0px;
		height: 0px;
		border-width: 15px;
		border-style: solid;
		border-color: transparent transparent #fddd9a transparent;
		position: absolute;
		top: -29px;
		right: 20px;
	}

	.fast-btn-blcok ul {
		text-align: left;
		margin: 0 auto;
		padding: 6%;
		background-color: #fddd9a;
		border-radius: 5px;
	}

	.fast-btn-blcok ul li a {
		color: #374140;
	}	
</style>
<script type="text/javascript">

	// column's title i18n
	var i18n = new Object();
	// n110-臺幣活期性存款餘額
	i18n['ACN'] = '<spring:message code="LB.Account" />'; // 帳號
	i18n['KIND'] = '<spring:message code="LB.Deposit_type" />'; // 存款種類
	i18n['ADPIBAL'] = '<spring:message code="LB.Available_balance" />'; // 可用餘額
	i18n['BDPIBAL'] = '<spring:message code="LB.Account_balance" />'; // 帳戶餘額
	i18n['CLR'] = '<spring:message code="LB.Exchange_ticket_today" />'; // 本日交換票
	// n420-臺幣定期性存款明細
	i18n['ACN'] = '<spring:message code="LB.Account" />'; // 帳號
	i18n['TYPE'] = '<spring:message code="LB.Deposit_type" />'; // 存單種類
	i18n['FDPNUM'] = '<spring:message code="LB.Certificate_no" />'; // 存單號碼
	i18n['AMTFDP'] = '<spring:message code="LB.Certificate_amount" />'; // 存單金額
	i18n['ITR'] = '<spring:message code="LB.Interest_rate1" />'; // 利率(%)
	i18n['INTMTH'] = '<spring:message code="LB.Interest_calculation" />'; // 計息方式
	i18n['DPISDT'] = '<spring:message code="LB.Start_date" />'; // 起存日
	i18n['DUEDAT'] = '<spring:message code="LB.Maturity_date" />'; // 到期日
	i18n['DUEDAT'] = '<spring:message code="LB.Interest_transfer_to_account" />'; // 利息轉入帳號
	i18n['ILAZLFTM'] = '<spring:message code="LB.Automatic_number_of_rotations" />'; // 自動轉期已轉次數
	i18n['AUTXFTM'] = '<spring:message code="LB.Automatic_number_of_unrotated" />'; // 自動轉期未轉次數
	i18n['TYPE1'] = '<spring:message code="LB.Rollover_method" />'; // 轉存方式
	i18n['TYPE2'] = '<spring:message code="LB.Non_Automatic_unrotated_application" />'; // 申請不轉期
	i18n['REMIND'] = '<spring:message code="LB.Note" />'; // 備註
	// n510-外匯活存餘額
	i18n['ACN'] = '<spring:message code="LB.Account" />'; // 帳號
	i18n['KIND'] = '<spring:message code="LB.Deposit_type" />'; // 存款種類
	i18n['CUID'] = '<spring:message code="LB.Currency" />'; // 幣別
	i18n['AVAILABLE'] = '<spring:message code="LB.Available_balance" />'; // 可用餘額
	i18n['BALANCE'] = '<spring:message code="LB.Account_balance" />'; // 帳戶餘額
	// n530-外幣定存明細
	i18n['ACN'] = '<spring:message code="LB.Account" />'; // 帳號
	i18n['FDPNO'] = '<spring:message code="LB.Certificate_no" />'; // 存單號碼
	i18n['CUID'] = '<spring:message code="LB.Currency" />'; // 幣別
	i18n['BALANCE'] = '<spring:message code="LB.Certificate_amount" />'; // 存單金額
	i18n['ITR'] = '<spring:message code="LB.Interest_rate1" />'; // 利率(%)
	i18n['INTMTH'] = '<spring:message code="LB.Interest_calculation" />'; // 計息方式
	i18n['DPISDT'] = '<spring:message code="LB.Start_date" />'; // 起存日
	i18n['DUEDAT'] = '<spring:message code="LB.Maturity_date" />'; // 到期日
	i18n['TSFACN'] = '<spring:message code="LB.Interest_transfer_to_account" />'; // 利息轉入帳號
	i18n['ILAZLFTM'] = '<spring:message code="LB.Automatic_number_of_rotations" />'; // 自動轉期已轉次數
	i18n['AUTXFTM'] = '<spring:message code="LB.Automatic_number_of_unrotated" />'; // 	自動轉期未轉次數
	// n320-借款明細查詢
	i18n['ACNCOVER'] = '<spring:message code="LB.Loan_account" />'; // 帳號
	i18n['ACNCOVER'] = '<spring:message code="LB.Seq_of_account" />'; // 分號
	i18n['AMTORLN'] = '<spring:message code="LB.Original_loan_amount" />'; // 原貸金額
	i18n['BAL'] = '<spring:message code="LB.Loan_Outstanding" />'; // 貸款餘額
	i18n['ITR'] = '<spring:message code="LB.Interest_rate1" />'; // 利率(%)
	i18n['DATFSLN'] = '<spring:message code="LB.Loan_drawdown_date" />'+'<spring:message code="LB.Loan_drawdown_date2" />'; // 初貸日（額度起日）
	i18n['DDT'] = '<spring:message code="LB.Expired_date" />'+'<spring:message code="LB.Expired_date2" />'; // 到期日（額度迄日）
	i18n['AWT'] = '<spring:message code="LB.Months_of_deferred_principal" />'; // 開始償還本金月份
	i18n['DATITPY'] = '<spring:message code="LB.End_date_of_repay_interest" />'; // 繳息迄日
	// c012-基金餘額及損益查詢
	i18n['CRLIMITFMT'] = '<spring:message code="LB.Credit_line" />'; // 信用額度
	i18n['CSHLIMITFMT'] = '<spring:message code="LB.Cash_advanced_line" />'; // 預借現金額度
	i18n['ACCBALFMT'] = '<spring:message code="LB.Current_balance" />'; // 目前已使用額度
	i18n['CSHBALFMT'] = '<spring:message code="LB.Cash_advanceUsed" />'; // 已動用預借現金
	i18n['INT_RATEFMT'] = '<spring:message code="LB.Current_revolving_interest" />'; // 本期循環信用利率
	i18n['PAYMT_ACCT_COVER'] = '<spring:message code="LB.Auto-pay_account_number" />'; // 自動扣繳帳號
	i18n['DBCODE'] = '<spring:message code="LB.Payment_method" />'; // 扣繳方式
	// n810-信用卡總覽
	i18n['CRLIMITFMT'] = '<spring:message code="LB.Credit_line" />'; // 信用額度
	i18n['CSHLIMITFMT'] = '<spring:message code="LB.Cash_advanced_line" />'; // 預借現金額度
	i18n['ACCBALFMT'] = '<spring:message code="LB.Current_balance" />'; // 目前已使用額度
	i18n['CSHBALFMT'] = '<spring:message code="LB.Cash_advanceUsed" />'; // 已動用預借現金
	i18n['INT_RATEFMT'] = '<spring:message code="LB.Current_revolving_interest" />'; // 本期循環信用利率
	i18n['PAYMT_ACCT_COVER'] = '<spring:message code="LB.Auto-pay_account_number" />'; // 自動扣繳帳號
	i18n['DBCODE'] = '<spring:message code="LB.Payment_method" />'; // 扣繳方式
	// n870-中央登錄債券餘額
	i18n['BONCOD'] = '<spring:message code="LB.W0036" />'; // 債券代號
	i18n['DPIBAL'] = '<spring:message code="LB.W0017" />'; // 餘額
	i18n['TSFBAL_N870'] = '<spring:message code="LB.W0038" />'; // 可動支餘額
	i18n['LMTSFO'] = '<spring:message code="LB.W0039" />'; // 限制性轉出餘額
	i18n['LMTSFI'] = '<spring:message code="LB.W0040" />'; // 限制性轉入餘額
	i18n['RPBAL'] = '<spring:message code="LB.W0041" />'; // 附條件簽發餘額
	// n190-黃金存摺餘額查詢
	i18n['ACN'] = '<spring:message code="LB.Loan_account" />'; // 帳號
	i18n['TSFBAL'] = '<spring:message code="LB.W1433" />(<spring:message code="LB.W1435" />)'; // 可用餘額(公克)
	i18n['GDBAL'] = '<spring:message code="LB.Account_balance" />(<spring:message code="LB.W1435" />)'; // 帳戶餘額(公克)
	i18n['MKBAL'] = '<spring:message code="LB.W1436" />(<spring:message code="LB.Dollar_1" />)'; // 參考市值(元)
	i18n['AVGCOST'] = '<spring:message code="LB.W1437" />(<spring:message code="LB.Dollar_1" />)'; // 每公克原始投入+參考平均成本(元)
	// n320-借款明細查詢
	//臺幣
	i18n['ACNCOVER'] = '<spring:message code="LB.Loan_account" />'; // 帳號
	i18n['SEQ'] = '<spring:message code="LB.Seq_of_account" />'; // 分號
	i18n['AMTORLN'] = '<spring:message code="LB.Original_loan_amount" />'; // 原貸金額
	i18n['BAL'] = '<spring:message code="LB.Loan_Outstanding" />'; // 貸款餘額
	i18n['ITR'] = '<spring:message code="LB.Interest_rate1" />'; // 利率(%)
	i18n['DATFSLN'] = '<spring:message code="LB.Loan_drawdown_date" /><br><spring:message code="LB.Loan_drawdown_date2" />'; // 初貸日（額度起日）
	i18n['DDT'] = '<spring:message code="LB.Expired_date" /><br><spring:message code="LB.Expired_date2" />'; // 到期日（額度迄日）
	i18n['AWT'] = '<spring:message code="LB.Months_of_deferred_principal" />'; // 開始償還本金月份
	i18n['DATITPY'] = '<spring:message code="LB.End_date_of_repay_interest" />'; // 繳息迄日
	i18n['PAYTYPE'] = '<spring:message code="LB.Quick_Menu" />'; // 快速選項
	//外幣
	i18n['ACNCOVER'] = '<spring:message code="LB.Loan_account" />'; // 帳號
	i18n['LNREFNO'] = ' <spring:message code= "LB.W0134" />'; // 交易編號
	i18n['LNCCY'] = '<spring:message code="LB.Currency"/>'; // 幣別
	i18n['LNCOS'] = ' <spring:message code= "LB.Loan_Balance" />'; // 放款餘額
	i18n['LNINTST'] = ' <spring:message code= "LB.X0965" />'; // 動撥日
	i18n['LNDUEDT'] = '<spring:message code="LB.Expired_date"/>'; // 到期日
	i18n['NXTINTD'] = ' <spring:message code= "LB.X0966" />'; // 下次繳息日
	i18n['LNFIXR'] = ' <spring:message code= "LB.X0967" />'; // 適用利率
	i18n['LNUPDATE'] = '<spring:message code="LB.Data_date"/>'; // 資料日期
	i18n['TYPEA'] = '<spring:message code="LB.Note"/>'; // 備註
	var i18nc = new Object();
	i18nc["LB.X1757"]='<spring:message code="LB.X1757" />';
	i18nc["LB.FX_Current_Deposit_To_Time_Deposit"]='<spring:message code="LB.FX_Current_Deposit_To_Time_Deposit" />';
	i18nc["LB.X1738"]='<spring:message code="LB.X1738" />';
	i18nc["LB.X1739"]='<spring:message code="LB.X1739" />';
	i18nc["LB.X1740"]='<spring:message code="LB.X1740" />';
	i18nc["LB.X1741"]='<spring:message code="LB.X1741" />';
	i18nc["LB.X1742"]='<spring:message code="LB.X1742" />';
	// FooTable Ajax tables
	var n110_columns = [
		{ "name":"ACN", "title":i18n['ACN'] },
		{ "name":"KIND", "title":i18n['KIND'] },
		{ "name":"ADPIBAL", "title":i18n['ADPIBAL'], "breakpoints":"xs sm", "style":{"text-align":"right"} },
		{ "name":"BDPIBAL", "title":i18n['BDPIBAL'], "style":{"text-align":"right"} },
		{ "name":"CLR", "title":i18n['CLR'], "breakpoints":"xs sm", "style":{"text-align":"right"} }
	];
	var n420_columns = [
		{ "name":"ACN", "title":i18n['ACN'] },
		{ "name":"TYPENAME", "title":i18n['TYPE'], "breakpoints":"xs sm" },
		{ "name":"FDPNUM", "title":i18n['FDPNUM'], "breakpoints":"xs sm" },
		{ "name":"AMTFDP", "title":i18n['AMTFDP'], "breakpoints":"xs sm", "style":{"text-align":"right"} },
		{ "name":"ITR", "title":i18n['ITR'], "breakpoints":"xs sm", "style":{"text-align":"right"} },
		{ "name":"INTMTH", "title":i18n['INTMTH'], "breakpoints":"xs sm" },
		{ "name":"DPISDT", "title":i18n['DPISDT'], "breakpoints":"xs sm" },
		{ "name":"DUEDAT", "title":i18n['DUEDAT'], "breakpoints":"xs sm" },
		{ "name":"TSFACN", "title":i18n['TSFACN'] },
		{ "name":"ILAZLFTM", "title":i18n['ILAZLFTM'], "breakpoints":"xs sm" },
		{ "name":"AUTXFTM", "title":i18n['AUTXFTM'], "breakpoints":"xs sm" },
		{ "name":"TYPE1", "title":i18n['TYPE1'], "breakpoints":"xs sm" },
		{ "name":"TYPE2", "title":i18n['TYPE2'], "breakpoints":"xs sm" },
		{ "name":"REMIND", "title":i18n['REMIND'], "breakpoints":"xs sm" }
	];
	var n510_columns = [
		{ "name":"ACN", "title":i18n['ACN'] },
		{ "name":"KIND", "title":i18n['KIND'] },
		{ "name":"CUID", "title":i18n['CUID'] },
		{ "name":"AVAILABLE", "title":i18n['AVAILABLE'], "breakpoints":"xs sm", "style":{"text-align":"right"} },
		{ "name":"BALANCE", "title":i18n['BALANCE'], "style":{"text-align":"right"} },
	];
	var n530_columns = [
		{ "name":"ACN", "title":i18n['ACN'] },
		{ "name":"FDPNO", "title":i18n['FDPNO'] },
		{ "name":"CUID", "title":i18n['CUID'], "breakpoints":"xs sm" },
		{ "name":"BALANCE", "title":i18n['BALANCE'], "breakpoints":"xs sm", "style":{"text-align":"right"} },
		{ "name":"ITR", "title":i18n['ITR'], "breakpoints":"xs sm", "style":{"text-align":"right"} },
		{ "name":"INTMTH", "title":i18n['INTMTH'], "breakpoints":"xs sm" },
		{ "name":"DPISDT", "title":i18n['DPISDT'], "breakpoints":"xs sm" },
		{ "name":"DUEDAT", "title":i18n['DUEDAT'], "breakpoints":"xs sm" },
		{ "name":"TSFACN", "title":i18n['TSFACN'], "breakpoints":"xs sm" },
		{ "name":"ILAZLFTM", "title":i18n['ILAZLFTM'], "breakpoints":"xs sm" },
		{ "name":"AUTXFTM", "title":i18n['AUTXFTM'], "breakpoints":"xs sm" }
	];
	var n810_columns = [
		{"name":"CRLIMITFMT", "title":i18n['CRLIMITFMT'], "style":{"text-align":"right"} },
		{"name":"CSHLIMITFMT", "title":i18n['CSHLIMITFMT'], "breakpoints":"xs", "style":{"text-align":"right"} },
		{"name":"ACCBALFMT", "title":i18n['ACCBALFMT'], "style":{"text-align":"right"} },
		{"name":"CSHBALFMT", "title":i18n['CSHBALFMT'], "breakpoints":"xs", "style":{"text-align":"right"} },
		{"name":"INT_RATEFMT", "title":i18n['INT_RATEFMT'], "breakpoints":"xs sm", "style":{"text-align":"right"} },
		{"name":"PAYMT_ACCT_COVER", "title":i18n['PAYMT_ACCT_COVER'], "breakpoints":"xs sm" },
		{"name":"DBCODE", "title":i18n['DBCODE'], "breakpoints":"xs" },
	];
	var n870_columns = [
		{"name":"BONCOD", "title":i18n['BONCOD'] },
		{"name":"DPIBAL", "title":i18n['DPIBAL'], "style":{"text-align":"right"} },
		{"name":"TSFBAL_N870", "title":i18n['TSFBAL_N870'], "style":{"text-align":"right"} },
		{"name":"LMTSFO", "title":i18n['LMTSFO'], "style":{"text-align":"right"} },
		{"name":"LMTSFI", "title":i18n['LMTSFI'], "style":{"text-align":"right"} },
		{"name":"RPBAL", "title":i18n['RPBAL'], "style":{"text-align":"right"} },
	];
	var n190_columns = [
		{ "name":"ACN", "title":i18n['ACN'] },
		{ "name":"TSFBAL", "title":i18n['TSFBAL'], "style":{"text-align":"right"} },
		{ "name":"GDBAL", "title":i18n['GDBAL'], "style":{"text-align":"right"} },
		{ "name":"MKBAL", "title":i18n['MKBAL'], "style":{"text-align":"right"} },
		{ "name":"AVGCOST", "title":i18n['AVGCOST'], "style":{"text-align":"right"} }
	];
	var c012_columns = [
		//信託編號 代號/基金名稱
		{ "name":"COL1", "title":'<spring:message code= "LB.W0904" /><br><spring:message code= "LB.W0905" />'},
		//首次申購日 單筆/定期定額
		{ "name":"COL2", "title":'<spring:message code= "LB.W0906" /><br><spring:message code= "LB.W0907" />', "style":{"text-align":"right"}, "breakpoints":"xs sm" },
		//投資幣別 計價幣別
		{ "name":"COL3", "title":'<spring:message code= "LB.W0908" /><br><spring:message code= "LB.W0909" /></th>', "breakpoints":"xs sm" },
		//信託金額 單位數(A)
		{ "name":"COL4", "title":'<spring:message code= "LB.W0026" /><br><spring:message code= "LB.W0027" />（A）', "style":{"text-align":"right"}, "breakpoints":"xs sm" },
		//淨值日期 參考贖回淨值(B)
		{ "name":"COL5", "title":'<spring:message code= "LB.W0912" /><br><spring:message code= "LB.W0913" />（B）', "breakpoints":"xs sm" },
		//參考匯率(C)
		{ "name":"COL6", "title":'<spring:message code= "LB.W0030" />（C）', "style":{"text-align":"right"},"breakpoints":"xs sm" },
		//投資幣別 參考現值  (A)*(B)*(C)
		{ "name":"COL7", "title":'<spring:message code= "LB.W0908" /><br/><spring:message code= "LB.W0915" />（A）＊（B）＊（C）', "breakpoints":"xs sm"},
		//參考損益 參考損益報酬率
		{ "name":"COL8", "title":'<spring:message code= "LB.W0916" /><br> <spring:message code= "LB.W0917" />', "style":{"text-align":"right"} ,"breakpoints":"xs sm"},
		//累計配息金額 參考含息報酬率
		{ "name":"COL9", "title":'<spring:message code= "LB.W0918" /><br><spring:message code= "LB.W0919" />', "style":{"text-align":"right"} ,"breakpoints":"xs sm"},
		//快速選項
		{ "name":"fastselect", "title":'<spring:message code= "LB.W0920" />'}
	];
	var c012_sum_columns = [
		// 投資幣別
				{ "name":"ADCCYNAME", "title":'<spring:message code= "LB.W0908" />'},
		// 總信託金額
				{ "name":"SUBTOTAMTFormat", "title":'<spring:message code= "LB.W0933" />', "style":{"text-align":"right"} },
		// 總參考現值
				{ "name":"SUBREFVALUEFormat", "title":'<spring:message code= "LB.W0934" />', "breakpoints":"xs sm" },
		// 不含息總損益
				{ "name":"SUBDIFAMTFormat", "title":'<spring:message code= "LB.W0935" />', "style":{"text-align":"right"} },
		// 損益報酬率
				{ "name":"SUBLCYRATFormat", "title":'<spring:message code= "LB.W0936" />', "breakpoints":"xs sm" },
		// 含息總損益
				{ "name":"SUBDIFAMT2Format", "title":'<spring:message code= "LB.W0937" />', "style":{"text-align":"right"},"breakpoints":"xs sm" },
		// 含息報酬率
				{ "name":"SUBFCYRATFormat", "title":'<spring:message code= "LB.W0938" />', "breakpoints":"xs sm"}
			];
	var c012_sum2_columns = [
		{ "name":"UNALLOTAMTCRYADCCYNAME", "title":''},
		{ "name":"UNALLOTAMTFormat", "title":'', "style":{"text-align":"right"} },
		{ "name":"", "title":'', "breakpoints":"xs sm" }
	];
	var n320tw_columns = [
		{ "name":"ACNCOVER", "title":i18n['ACNCOVER'] },
		{ "name":"SEQ", "title":i18n['SEQ'] ,"breakpoints":"xs sm" },
		{ "name":"AMTORLN", "title":i18n['AMTORLN'], "style":{"text-align":"right"},"breakpoints":"xs sm" },
		{ "name":"BAL", "title":i18n['BAL'], "style":{"text-align":"right"} },
		{ "name":"ITR", "title":i18n['ITR'], "style":{"text-align":"right"} ,"breakpoints":"xs sm" },
		{ "name":"DATFSLN", "title":i18n['DATFSLN'],"breakpoints":"xs sm" },
		{ "name":"DDT", "title":i18n['DDT'] ,"breakpoints":"xs sm" },
		{ "name":"AWT", "title":i18n['AWT'] ,"breakpoints":"xs sm"},
		{ "name":"DATITPY", "title":i18n['DATITPY'] ,"breakpoints":"xs sm"},
		{ "name":"PAYTYPE", "title":i18n['PAYTYPE'] }
	];
	
	var n320fx_columns = [
		{ "name":"ACNCOVER", "title":i18n['ACNCOVER'] },
		{ "name":"LNREFNO", "title":i18n['LNREFNO'] },
		{ "name":"LNCCY", "title":i18n['LNCCY'] ,"breakpoints":"xs sm"},
		{ "name":"LNCOS", "title":i18n['LNCOS'], "style":{"text-align":"right"},"breakpoints":"xs sm" },
		{ "name":"LNINTST", "title":i18n['LNINTST'] ,"breakpoints":"xs sm" },
		{ "name":"LNDUEDT", "title":i18n['LNDUEDT'] ,"breakpoints":"xs sm"},
		{ "name":"NXTINTD", "title":i18n['NXTINTD'] ,"breakpoints":"xs sm"},
		{ "name":"LNFIXR", "title":i18n['LNFIXR'], "style":{"text-align":"right"} ,"breakpoints":"xs sm"},
		{ "name":"LNUPDATE", "title":i18n['LNUPDATE'],"breakpoints":"xs sm" },
		{ "name":"TYPEA", "title":i18n['TYPEA'] ,"breakpoints":"xs sm"}
	];
	// 成功查詢資料
	var n110_rows, n420_rows, n510_rows, n530_rows, n810_rows, n870_rows, n190_rows, c012_rows,c012_sum_rows,c012_sum2_rows,n320tw_rows, n320fx_rows;
	allacct_forN320 = function(type,dataList,index,ctx){
		var selectString = "";
		var buttonString = "";
		if(type == "A"){
			selectString += '<input type="button" class="d-sm-none d-block" id="click" value="Click" onclick="hd2(\'actionBar2'+index+'\')" />';
			selectString += '<select class="d-none d-sm-block" id="actionBar" style="width:100px" onchange="formResetN320(this.value,'+dataList.ACN+','+dataList.SEQ+','+dataList.AMTAPY+'); ">';
			//請選擇
			//提前償還本金
			//貸款結清
			//房屋擔保借款繳息清單
			//已繳款本息查詢
			selectString += '<option value="">-<spring:message code= "LB.W0259" />-</option>';
			selectString += '<option value="repay_advance_p1"> <spring:message code= "LB.X0972" /></option>'
						+'<option value="settlement_advance_p1"> <spring:message code= "LB.X0973" /></option>'
						+'<option value="interest_list"><spring:message code= "LB.W0876" /></option>'
						+'<option value="paid_query"><spring:message code= "LB.W0856" /></option>'
						+'</select>';
			buttonString += '<div id="actionBar2'+index+'" class="fast-btn-blcok" style="visibility: hidden">'
							+'<span class="arrow_t_out"></span>'
							+'<ul><li><a href="javascript:void(0)" onclick="formResetN320(\'repay_advance_p1\','+dataList.ACN+','+dataList.SEQ+','+dataList.AMTAPY+');"> <spring:message code= "LB.X0972" /></a></li>'
							+'<li><a href="javascript:void(0)" onclick="formResetN320(\'settlement_advance_p1\','+dataList.ACN+','+dataList.SEQ+','+dataList.AMTAPY+');"> <spring:message code= "LB.X0973" /></a></li>'
							+'<li><a href="'+ctx+'/HOUSE/GUARANTEE/interest_list"><spring:message code= "LB.W0876" /></a></li>'
							+'<li><a href="'+ctx+'/LOAN/QUERY/paid_query"> <spring:message code= "LB.W0856" /></a></li></ul></div>'
		}else if (type == "Y"){
			selectString += '<input type="button" class="d-sm-none d-block" id="click" value="Click" onclick="hd2(\'actionBar2'+index+'\')" />';
			selectString += '<select class="d-none d-sm-block" id="actionBar" style="width:100px" onchange="formResetN320(this.value,'+dataList.ACN+','+dataList.SEQ+','+dataList.AMTAPY+'); ">';
			selectString += '<option value="">-<spring:message code= "LB.W0259" />-</option>';
			//提前償還本金
			selectString += '<option value="repay_advance_p1"><spring:message code= "LB.X0972" /></option>'
						+'<option value="interest_list"><spring:message code= "LB.W0876" /></option>'
						+'<option value="paid_query"> <spring:message code= "LB.W0856" /></option>'
						+'</select>';
			buttonString += '<div id="actionBar2'+index+'" class="fast-btn-blcok" style="visibility: hidden">'
							+'<span class="arrow_t_out"></span>'
							//提前償還本金
							+'<ul><li><a href="javascript:void(0)" onclick="formResetN320(\'repay_advance_p1\','+dataList.ACN+','+dataList.SEQ+','+dataList.AMTAPY+');"><spring:message code= "LB.X0972" /></a></li>'
							+'<li><a href="'+ctx+'/HOUSE/GUARANTEE/interest_list"><spring:message code= "LB.W0876" /></a></li>'
							+'<li><a href="'+ctx+'/LOAN/QUERY/paid_query"><spring:message code= "LB.W0856" /></a></li></ul></div>'
		}
		return selectString+buttonString ;
	}
	
	// 畫面呈現
	$(document).ready(function () {
		// 開始查詢資料並完成畫面
		init();
	});

	// 帳戶總覽查詢
	function init() {
		var dpoverview = '${dpoverview}';
		console.log("dpoverview: " + dpoverview);
		
		// 使用者定義的查詢功能
		if(dpoverview == null || dpoverview == '') {
			//若沒定義則全部查詢
			dpoverview();
			
		} else {
			// 依據使用者定義的查詢功能發Ajax
			var dpArray = dpoverview.split(',');
			for(let o in dpArray){
				console.log("dpoverview.dpArray[o]: " + dpArray[o]);
				switch (dpArray[o]) {
					case "1":
						getMyAssets110(); // n110-臺幣活期性存款餘額
						getMyAssets420(); // n420-臺幣定期性存款明細
						break;
					case "2":
						getMyAssets510(); // n510-外匯活存餘額
						getMyAssets530(); // n530-外幣定存明細
						break;
					case "3":
						getMyAssets320(); // n320-借款明細查詢
						break;
					case "4":
						getMyAssets012(); // c012-基金餘額及損益查詢
						break;
					case "5":
						getMyAssets810(); // n810-信用卡總覽
						break;
					case "6":
						getMyAssets870(); // n870-中央登錄債券餘額
						break;
					case "7":
						getMyAssets190(); // n190-黃金存摺餘額查詢
						break;
					default:
						console.log("dpoverview.default !");
				}
			}
		}
	}

	// 全部查詢
	function dpoverview(){
		// n110-臺幣活期性存款餘額
		getMyAssets110();
		// n420-臺幣定期性存款明細
		getMyAssets420();
		// n510-外匯活存餘額
		getMyAssets510();
		// n530-外幣定存明細
		getMyAssets530();
		// n320-借款明細查詢
 		getMyAssets320();
		// c012-基金餘額及損益查詢
 		getMyAssets012();
		// n810-信用卡總覽
		getMyAssets810();
		// n870-中央登錄債券餘額
		getMyAssets870();
		// n190-黃金存摺餘額查詢
		getMyAssets190();
	}
	
	// Ajax_n110-臺幣活期性存款餘額
	function getMyAssets110() {
		$("#N110").show();
		$("#n110_table_loadingBox").show();
		uri = '${__ctx}' + "/OVERVIEW/allacct_n110aj";
		console.log("allacct_n110aj_uri: " + uri);
		fstop.getServerDataEx(uri, null, true, countAjax, null, "n110");
	}
	// Ajax_n420-臺幣定期性存款明細
	function getMyAssets420() {
		$("#N420").show();
		$("#n420_table_loadingBox").show();
		uri = '${__ctx}' + "/OVERVIEW/allacct_n420aj";
		console.log("allacct_n420aj_uri: " + uri);
		fstop.getServerDataEx(uri, null, true, countAjax, null, "n420");
	}
	// Ajax_n510-外匯活存餘額
	function getMyAssets510() {
		$("#N510").show();
		$("#n510_table_loadingBox").show();
		uri = '${__ctx}' + "/OVERVIEW/allacct_n510aj";
		console.log("allacct_n510aj_uri: " + uri);
		fstop.getServerDataEx(uri, null, true, countAjax, null, "n510");
	}
	// Ajax_n530-外幣定存明細
	function getMyAssets530() {
		$("#N530").show();
		$("#n530_table_loadingBox").show();
		uri = '${__ctx}' + "/OVERVIEW/allacct_n530aj";
		console.log("allacct_n530aj_uri: " + uri);
		fstop.getServerDataEx(uri, null, true, countAjax, null, "n530");
	}
	// Ajax_n320-借款明細查詢
	function getMyAssets320() {
		$("#N320").show();
		$("#n320_table_loadingBox").show();
		uri = '${__ctx}' + "/OVERVIEW/allacct_n320aj";
		console.log("allacct_n320aj_uri: " + uri);
		var Data = $("#formN320").serializeArray();
		fstop.getServerDataEx(uri, Data, true, countAjax, null, "n320");
	}
	// Ajax_c012-基金餘額及損益查詢
	function getMyAssets012() {
		$("#C012").show();
		$("#c012_table_loadingBox").show();
		uri = '${__ctx}' + "/OVERVIEW/allacct_c012aj";
		console.log("allacct_c012aj_uri: " + uri);
		fstop.getServerDataEx(uri, null, true, countAjax, null, "c012");
	}
	// Ajax_n810-信用卡總覽
	function getMyAssets810() {
		$("#N810").show();
		$("#n810_table_loadingBox").show();
		uri = '${__ctx}' + "/OVERVIEW/allacct_n810aj";
		console.log("allacct_n810aj_uri: " + uri);
		fstop.getServerDataEx(uri, null, true, countAjax, null, "n810");
	}
	// Ajax_n870-中央登錄債券餘額
	function getMyAssets870() {
		$("#N870").show();
		$("#n870_table_loadingBox").show();
		uri = '${__ctx}' + "/OVERVIEW/allacct_n870aj";
		console.log("allacct_n870aj_uri: " + uri);
		fstop.getServerDataEx(uri, null, true, countAjax, null, "n870");
	}
	// Ajax_n190-黃金存摺餘額查詢
	function getMyAssets190() {
		$("#N190").show();
		$("#n190_table_loadingBox").show();
		uri = '${__ctx}' + "/OVERVIEW/allacct_n190aj";
		console.log("allacct_n190aj_uri: " + uri);
		fstop.getServerDataEx(uri, null, true, countAjax, null, "n190");
	}


	// Ajax_callback
	function countAjax(data, type){
		console.log("type: " + type);
		if (data) {
			// login_aj回傳資料
			console.log("data.json: " + JSON.stringify(data));

			switch (type) {
				// n110-臺幣活期性存款餘額
				case "n110":
					if (data.result) {
						n110_rows = data.data.REC;
					} else {
						// 錯誤訊息
						n110_columns = [ {
								"name" : "MSGCODE",
								"title" : '<spring:message code="LB.Transaction_code" />'
							}, {
								"name" : "MESSAGE",
								"title" : '<spring:message code="LB.Transaction_message" />'
							}
						];
						n110_rows = [ {
							"MSGCODE" : data.msgCode,
							"MESSAGE" : data.message
						} ];
					}
					// 解遮罩
					$("#n110_table_loadingBox").hide();
					
					jQuery(function($) {
						// footable呈現N110，callback變更css置中table的表頭
						$('#n110_table').footable({
							"columns" : n110_columns,
							"rows" : n110_rows
						}, function() {
							$('#n110_table thead tr th').css({
								'text-align' : 'center'
							});
						});
					});
					break;

				// n420-臺幣定期性存款明細
				case "n420":
					if (data.result) {
						n420_rows = data.data.REC;
					} else {
						// 錯誤訊息
						n420_columns = [ {
								"name" : "MSGCODE",
								"title" : '<spring:message code="LB.Transaction_code" />'
							}, {
								"name" : "MESSAGE",
								"title" : '<spring:message code="LB.Transaction_message" />'
							}
						];
						n420_rows = [ {
							"MSGCODE" : data.msgCode,
							"MESSAGE" : data.message
						} ];
					}
					// 解遮罩
					$("#n420_table_loadingBox").hide();
					
					jQuery(function($) {
						// footable呈現N420，callback變更css置中table的表頭
						$('#n420_table').footable({
							"columns" : n420_columns,
							"rows" : n420_rows
						}, function() {
							$('#n420_table thead tr th').css({
								'text-align' : 'center'
							});
						});
					});
					break;
					
				// 外幣餘額查詢
				case "n510":
					if (data.result) {
						n510_rows = data.data.ACNInfo;
						n510_rows.forEach(function(k,v)
								{
							k["KIND"]=i18nc[k["KIND"]];
								});
					} else {
						// 錯誤訊息
						n510_columns = [ {
								"name" : "MSGCODE",
								"title" : '<spring:message code="LB.Transaction_code" />'
							}, {
								"name" : "MESSAGE",
								"title" : '<spring:message code="LB.Transaction_message" />'
						} ];
						n510_rows = [ {
							"MSGCODE" : data.msgCode,
							"MESSAGE" : data.message
						} ];
					}
					// 解遮罩
					$("#n510_table_loadingBox").hide();
					
					jQuery(function($) {
						// footable呈現N510，callback變更css置中table的表頭
						$('#n510_table').footable({
							"columns" : n510_columns,
							"rows" : n510_rows
						}, function() {
							$('#n510_table thead tr th').css({
								'text-align' : 'center'
							});
							console.log("n510_rows.length_1: " + n510_rows.length);
							// n510_rows若為空，不顯示NO RESULT
							if(n510_rows.length==0){
								$('#n510_table tbody').css({
									'display' : 'none'
								});
							}
						});
					});
					break;

				// n530-外幣定存明細
				case "n530":
					if (data.result) {
						n530_rows = data.data.REC;
					} else {
						// 錯誤訊息
						n530_columns = [ {
								"name" : "MSGCODE",
								"title" : '<spring:message code="LB.Transaction_code" />'
							}, {
								"name" : "MESSAGE",
								"title" : '<spring:message code="LB.Transaction_message" />'
						} ];
						n530_rows = [ {
							"MSGCODE" : data.msgCode,
							"MESSAGE" : data.message
						} ];
					}
					// 解遮罩
					$("#n530_table_loadingBox").hide();
					
					jQuery(function($) {
						// footable呈現n530，callback變更css置中table的表頭
						$('#n530_table').footable({
							"columns" : n530_columns,
							"rows" : n530_rows
						}, function() {
							$('#n530_table thead tr th').css({
								'text-align' : 'center'
							});
						});
					});
					break;

				// n320-借款明細查詢
				case "n320":
					$('#n320tw_sum').html("");
					$('#n320fx_sum').html("");
					$('#searchAgain').html("");
					$('#n320tw_table').html("");
					$('#n320fx_table').html("");
					$('#N320GO').val(data.data.N320GO)
					$('#N552GO').val(data.data.N552GO)
					$('#USERDATA_X50_552').val(data.data.USERDATA_X50_552)
					$('#USERDATA_X50_320').val(data.data.USERDATA_X50_320)
					
					if (data.data.TOPMSG_320=='OKLR'||data.data.TOPMSG_320=='OKOV') {
						n320tw_rows = data.data.TW;
				
						n320tw_rows.forEach(function(n320tw_rows,index){
							PAYTYPE = n320tw_rows.PAYTYPE
							n320tw_rows.PAYTYPE = allacct_forN320(PAYTYPE,n320tw_rows,index,'${__ctx}');
						});
						$('#n320tw_sum').html(
							'<p><spring:message code="LB.Total_records"/>&nbsp;'+data.data.TWNUM+'&nbsp; <spring:message code="LB.Rows"/></p>'+
							'<p><spring:message code="LB.NTD"/> &nbsp;'+data.data.TOTALBAL_320+'&nbsp;<spring:message code="LB.Dollar"/></p>'+
							'<spring:message code="LB.Loan_detail_P1_NTD_note"/>'
						);
					}else if(data.data.MSGFLG_320=='01'){
						n320tw_rows = [ {
							"ACNCOVER" : 'MSGFLG=01',
							//請洽櫃檯
							"SEQ" : '<spring:message code="LB.Loan_contact_note"/>'
						} ];
					}else if (data.data.TOPMSG_320 =='NoCall'){
						n320tw_rows = "";
					}else{
						n320tw_rows = [ {
							"ACNCOVER" : data.data.TOPMSG_320,
							"SEQ": data.data.errorMsg320
						} ];
					}
					
					if(data.data.TOPMSG_552 =='OKLR'||data.data.TOPMSG_552 == 'OKOV' ){
						n320fx_rows = data.data.FX;
						var n320cry = data.data.CRY;
						var FxSumString = "";
						n320cry.forEach(function(n320cry){
							FxSumString += '<p>'+ n320cry.AMTLNCCY +' &nbsp;' + n320cry.FXTOTAMT + '&nbsp;<spring:message code="LB.Dollar"/></p>'
						});
						
						$('#n320fx_sum').html(
							'<p><spring:message code="LB.Total_records"/>&nbsp;'+data.data.FXNUM+'&nbsp; <spring:message code="LB.Rows"/></p>'+FxSumString
						);
					}else{
						n320fx_rows = [ {
							"ACNCOVER" : data.data.TOPMSG_552,
							"LNREFNO" : data.data.errorMsg552
						} ];
					}
					
					//查詢結果超出每頁可顯示最大筆數，如欲顯示後續資料，請按 
					//繼續查詢
					if(data.data.TOPMSG_552 =='OKOV'||data.data.TOPMSG_320 == 'OKOV' ){
						$('#searchAgain').html(
							'<font color="red"> <spring:message code= "LB.X0076" /></font>'
							+'<input type="button" class="ttb-sm-btn btn-flat-orange"  id="Query" value=" <spring:message code= "LB.X0151" />" onclick="getMyAssets320();"/>'
						);
					}
					
					// 解遮罩
					$("#n320_table_loadingBox").hide();
					
					jQuery(function($) {
						// footable呈現N110，callback變更css置中table的表頭
						$('#n320tw_table').footable({
							"columns" : n320tw_columns,
							"rows" : n320tw_rows
						}, function() {
							$('#n320tw_table thead tr th').css({
								'text-align' : 'center'
							});
							$('#n320tw_table thead').prepend(function() {
								return '<tr><th style="border: 1px solid #DDD;"><spring:message code="LB.Report_name" /></th>'
									+'<th class="text-left" colspan="9"><spring:message code="LB.NTD_loan_detail"/></th></tr>';
							});
							if( n320tw_rows ==""){
								$('#n320tw_table tbody').css({
									'display' : 'none'
								})	
							};
						});
						
						$('#n320fx_table').footable({
							"columns" : n320fx_columns,
							"rows" : n320fx_rows
						}, function() {
							$('#n320fx_table thead tr th').css({
								'text-align' : 'center'
							});
							$('#n320fx_table thead').prepend(function() {
								return '<tr><th style="border: 1px solid #DDD;"><spring:message code="LB.Report_name" /></th>'
									+'<th class="text-left" colspan="9"><spring:message code="LB.FX_loan_detail"/></th></tr>';
							});
						});
					});
					
					break;
					
				// c012-基金餘額及損益查詢
				case "c012":
					if (data.result) {
						c012_rows = data.data.OverviewREC;		
						c012_sum_rows=data.data.OverviewREC2;
						c012_sum2_rows=data.data.thirdRows;
					} else {
						// 錯誤訊息
						c012_columns = [ {
								"name" : "MSGCODE",
								"title" : '<spring:message code="LB.Transaction_code" />'
							}, {
								"name" : "MESSAGE",
								"title" : '<spring:message code="LB.Transaction_message" />'
						} ];
						c012_rows = [ {
							"MSGCODE" : data.msgCode,
							"MESSAGE" : data.message
						} ];
						// 錯誤訊息
						c012_sum_columns = [ {
								"name" : "MSGCODE",
								"title" : '<spring:message code="LB.Transaction_code" />'
							}, {
								"name" : "MESSAGE",
								"title" : '<spring:message code="LB.Transaction_message" />'
						} ];
						c012_sum_rows = [ {
							"MSGCODE" : data.msgCode,
							"MESSAGE" : data.message
						} ];
					}
					
					// 解遮罩
					$("#c012_table_loadingBox").hide();
					
					jQuery(function($) {
						// footable呈現c012，callback變更css置中table的表頭
						$('#c012_table').footable({
							"columns" : c012_columns,
							"rows" : c012_rows
						}, function() {
							$('#c012_table thead tr th').css({
								'text-align' : 'center'
							});
						});
						$('#c012_table_sum').footable({
							"columns" : c012_sum_columns,
							"rows" : c012_sum_rows
						}, function() {
							$('#c012_sum_table thead tr th').css({
								'text-align' : 'center'
							});
						});
						$('#c012_note').html(data.data.thirdDataCount);
						c012_sum2_rows.forEach(function(element){
							//未分配
							$('#c012_table_sum2').html($('#c012_table_sum2').html()+
									"<tr><td>"+element.UNALLOTAMTCRYADCCYNAME+"</td><td>"+element.UNALLOTAMTFormat+"</td><td><spring:message code= "LB.W0921" /></td></tr>")
						});
// 						$('#c012_table_sum2').footable({
// 							"columns" : c012_sum2_columns,
// 							"rows" : c012_sum2_rows
// 						}, function() {
// 							$('#c012_sum2_table thead tr th').css({
// 								'text-align' : 'center'
// 							});
// 						});
// 						$('#c012_note').val(data.data.thirdDataCount);
					});
					break;
						
				// 信用卡總覽
				case "n810":
					if (data.result) {
						n810_rows = data.data;
					} else {
						// 錯誤訊息
						n810_columns = [ {
								"name" : "MSGCODE",
								"title" : '<spring:message code="LB.Transaction_code" />'
							}, {
								"name" : "MESSAGE",
								"title" : '<spring:message code="LB.Transaction_message" />'
						} ];
						n810_rows = [ {
							"MSGCODE" : data.msgCode,
							"MESSAGE" : data.message
						} ];
					}
					// 解遮罩
					$("#n810_table_loadingBox").hide();
					
					jQuery(function($) {
						// footable呈現N810，callback變更css置中table的表頭
						$('#n810_table').footable({
							"columns" : n810_columns,
							"rows" : n810_rows
						}, function() {
							$('#n810_table thead tr th').css({
								'text-align' : 'center'
							});
						});
					});
					break;

				// n870-中央登錄債券餘額
				case "n870":
					// 解遮罩
					$("#n870_table_loadingBox").hide();
					
					if (data.result) {
						var rec = data.data.REC;
						console.log("n870.rec: " + JSON.stringify(rec));
						
						for(let o in rec) {
							if(o==0){
								console.log("n870.first: " +o);
								n870_rows = rec[o]['Table'];
								
								jQuery(function($) {
									$('#n870_table').footable({
										"columns" : n870_columns,
										"rows" : n870_rows
									}, function() {
										$('#n870_table thead tr th').css({
											'text-align' : 'center'
										});
										$('#n870_table thead').prepend(function() {
											return '<tr><th style="border: 1px solid #DDD;"><spring:message code="LB.Account"/></th>'
												+'<th class="text-left" colspan="5" style="background-color: #ffffff;">'
												+rec[o]['ACN']
												+'</th></tr>';
										});
									});
								});
							} else {
								console.log("n870.other: " + o);
								$('#bondbalance').append(function() {
									return '<table id="n870_table_' + o + '" data-toggle-column="first"></table>';
								});

								n870_rows = rec[o]['Table'];
								
								jQuery(function($) {
									$("#n870_table_"+o).footable({
										"columns" : n870_columns,
										"rows" : n870_rows
									}, function() {
										console.log("n870.back: " + o);
										$('#n870_table_'+o+' thead tr th').css({
											'text-align' : 'center'
										});
										$('#n870_table_'+o+' thead').prepend(function() {
											return '<tr><th style="border: 1px solid #DDD;"><spring:message code="LB.Account"/></th>'
												+'<th class="text-left" colspan="5" style="background-color: #ffffff;">'
												+rec[o]['ACN']
												+'</th></tr>';
										});
									});
								});
							}
						}
					} else {
						// 錯誤訊息
						n870_columns = [ {
								"name" : "MSGCODE",
								"title" : '<spring:message code="LB.Transaction_code" />'
							}, {
								"name" : "MESSAGE",
								"title" : '<spring:message code="LB.Transaction_message" />'
						} ];
						n870_rows = [ {
							"MSGCODE" : data.msgCode,
							"MESSAGE" : data.message
						} ];
						
						jQuery(function($) {
							// footable呈現n190，callback變更css置中table的表頭
							$('#n870_table').footable({
								"columns" : n870_columns,
								"rows" : n870_rows
							}, function() {
								$('#n870_table thead tr th').css({
									'text-align' : 'center'
								});
							});
						});
					}
					break;
					
				// n190-黃金存摺餘額查詢
				case "n190":
					if (data.result) {
						n190_rows = data.data.REC;
					} else {
						// 錯誤訊息
						n190_columns = [ {
								"name" : "MSGCODE",
								"title" : '<spring:message code="LB.Transaction_code" />'
							}, {
								"name" : "MESSAGE",
								"title" : '<spring:message code="LB.Transaction_message" />'
						} ];
						n190_rows = [ {
							"MSGCODE" : data.msgCode,
							"MESSAGE" : data.message
						} ];
					}
					
					// 解遮罩
					$("#n190_table_loadingBox").hide();
					
					jQuery(function($) {
						// footable呈現n190，callback變更css置中table的表頭
						$('#n190_table').footable({
							"columns" : n190_columns,
							"rows" : n190_rows
						}, function() {
							$('#n190_table thead tr th').css({
								'text-align' : 'center'
							});
						});
					});
					break;
					
				default:
					console.log("countAjax.default !");
			}
		}
	}
	//c012快速選單
	function processURL(TRANSCODE,COUNTRYTYPE,CREDITNO,selectID,REFMARKTransfer){
		var selectValue = $("#" + selectID).val();

		if(selectValue == "#"){
			return false;
		}

		if(selectValue == "1"){
			window.open("https://tbb.moneydj.com/w/CustFundIDMap.djhtm?A=" + TRANSCODE + "&B=02");
		}
		else if(selectValue == "2"){
			window.open("https://tbb.moneydj.com/w/CustFundIDMap.djhtm?A=" + TRANSCODE + "&B=02");
		}
		else if(selectValue == "3"){
			if(REFMARKTransfer != "N"){
				//網路銀行對實體憑證不提供交易類服務，請洽往來營業單位辦理。
				//alert('<spring:message code= "LB.X0970" />');
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.X0970' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
				return false;
	        }
			$("#c012formID").attr("action","${__ctx}/FUND/REDEEM/fund_redeem_data?CREDITNO=" + CREDITNO + "&TRANSCODE=" + TRANSCODE);
			$("#c012formID").submit();
		}
		else if(selectValue == "4"){
			if(REFMARKTransfer != "N"){
				//網路銀行對實體憑證不提供交易類服務，請洽往來營業單位辦理。
				//alert('<spring:message code= "LB.X0970" />');
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.X0970' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
				return false;
	        }
			$("#c012formID").attr("action","${__ctx}/FUND/TRANSFER/query_fund_transfer_data?CREDITNO=" + CREDITNO + "&TRANSCODE=" + TRANSCODE);
			$("#c012formID").submit();
		}
		else if(selectValue == "5"){
			//基金歷史交易明細資料查詢
			$("#c012formID").attr("action","${__ctx}/FUND/QUERY/transfer_data_query_history?CREDITNO=" + CREDITNO);
			$("#c012formID").submit();
		}
		else if(selectValue == "6"){
			//定期投資約定變更
			$("#c012formID").attr("action","${__ctx}/FUND/ALTER/regular_investment");
			$("#c012formID").submit();
		}
		else if(selectValue == "7"){
			window.open("https://tbb.moneydj.com/w/CustFundIDMap.djhtm?A=" + TRANSCODE + "&DownFile=99");
		}
		else if(selectValue == "8"){
			window.open("https://tbb.moneydj.com/w/CustFundIDMap.djhtm?A=" + TRANSCODE + "&DownFile=1");
		}
		else if(selectValue == "9"){
			window.open("https://tbb.moneydj.com/w/CustFundIDMap.djhtm?A=" + TRANSCODE + "&B=08");
		}
		else if(selectValue == "10"){
			//基金交易資料查詢
			$("#c012formID").attr("action","${__ctx}/FUND/QUERY/transfer_data_query");
			$("#c012formID").submit();
		}
	}
	function processURL_small(TRANSCODE,COUNTRYTYPE,CREDITNO,selectval,REFMARKTransfer){
		console.log("small_TEST")
		var selectValue = selectval;

		if(selectValue == "#"){
			return false;
		}

		if(selectValue == "1"){
			window.open("https://tbb.moneydj.com/w/CustFundIDMap.djhtm?A=" + TRANSCODE + "&B=02");
		}
		else if(selectValue == "2"){
			window.open("https://tbb.moneydj.com/w/CustFundIDMap.djhtm?A=" + TRANSCODE + "&B=02");
		}
		else if(selectValue == "3"){
			if(REFMARKTransfer != "N"){
				//網路銀行對實體憑證不提供交易類服務，請洽往來營業單位辦理。
				//alert('<spring:message code= "LB.X0970" />');
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.X0970' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
				return false;
	        }
			$("#c012formID").attr("action","${__ctx}/FUND/REDEEM/fund_redeem_data?CREDITNO=" + CREDITNO + "&TRANSCODE=" + TRANSCODE);
			$("#c012formID").submit();
		}
		else if(selectValue == "4"){
			if(REFMARKTransfer != "N"){
				//網路銀行對實體憑證不提供交易類服務，請洽往來營業單位辦理。
				//alert('<spring:message code= "LB.X0970" />');
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.X0970' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
				return false;
	        }
			$("#c012formID").attr("action","${__ctx}/FUND/TRANSFER/query_fund_transfer_data?CREDITNO=" + CREDITNO + "&TRANSCODE=" + TRANSCODE);
			$("#c012formID").submit();
		}
		else if(selectValue == "5"){
			//基金歷史交易明細資料查詢
			$("#c012formID").attr("action","${__ctx}/FUND/QUERY/transfer_data_query_history?CREDITNO=" + CREDITNO);
			$("#c012formID").submit();
		}
		else if(selectValue == "6"){
			//定期投資約定變更
			$("#c012formID").attr("action","${__ctx}/FUND/ALTER/regular_investment");
			$("#c012formID").submit();
		}
		else if(selectValue == "7"){
			window.open("https://tbb.moneydj.com/w/CustFundIDMap.djhtm?A=" + TRANSCODE + "&DownFile=99");
		}
		else if(selectValue == "8"){
			window.open("https://tbb.moneydj.com/w/CustFundIDMap.djhtm?A=" + TRANSCODE + "&DownFile=1");
		}
		else if(selectValue == "9"){
			window.open("https://tbb.moneydj.com/w/CustFundIDMap.djhtm?A=" + TRANSCODE + "&B=08");
		}
		else if(selectValue == "10"){
			//基金交易資料查詢
			$("#c012formID").attr("action","${__ctx}/FUND/QUERY/transfer_data_query");
			$("#c012formID").submit();
		}
	}
	//快速選單N320
	function formResetN320(value,loanACN,loanSEQ,loanAMT){
		switch(value){
			case "repay_advance_p1":
				$("#loanACN").val(loanACN);
				$("#loanSEQ").val(loanSEQ);
				$("#loanAMT").val(loanAMT);
				$("#formN320").attr("action","${__ctx}/LOAN/ADVANCE/repay_advance_p1");
	  			$("#formN320").submit(); 
				break;
			case "settlement_advance_p1":
				$("#loanACN").val(loanACN);
				$("#loanSEQ").val(loanSEQ);
				$("#loanAMT").val(loanAMT);
				$("#formN320").attr("action","${__ctx}/LOAN/ADVANCE/settlement_advance_p1");
	  			$("#formN320").submit(); 
				break;
			case "interest_list":
				fstop.getPage('${pageContext.request.contextPath}'+'/HOUSE/GUARANTEE/interest_list','', '');
				break;
			case "paid_query":
				fstop.getPage('${pageContext.request.contextPath}'+'/LOAN/QUERY/paid_query','', '');
				break;
		}
	}
	//快速選單測試新增
	//將actionbar select 展開閉合
	$(function(){
		$("#click").on('click', function(){
	   	var s = $("#actionBar2").attr('size')==1?8:1
	   	$("#actionBar2").attr('size', s);
		});
		$("#actionBar2 option").on({
	   		click: function() {
	       	$("#actionBar2").attr('size', 1);
	   		},
		});
	});
	
	function hd2(T){
		var t = document.getElementById(T);
		if(t.style.visibility === 'visible'){
			t.style.visibility = 'hidden';
		}
		else{
			$("div[id^='"+T+"']").each(function() {
				var d = document.getElementById($(this).attr('id'));
				d.style.visibility = 'hidden';
		    });
			t.style.visibility = 'visible';
		}
	}
</script>

</head>
<body>
   	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 帳戶總覽     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Account_Overview" /></li>
		</ol>
	</nav>

	
	<!-- 主畫面 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->

		<main class="col-12">
			<section id="main-content" class="container">
				<!-- 帳戶總覽 -->
	           	<h2>
	           		<spring:message code="LB.Account_Overview"/>
	           	</h2>
	           	<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
	           	
				<!-- 查詢時間 -->
	           	<jsp:useBean id="now" class="java.util.Date" />
				<fmt:formatDate value="${now}" type="both" dateStyle="long" pattern="yyyy/MM/dd HH:mm:ss" var="today"/>
	           	<p><spring:message code="LB.Inquiry_time"/>：${today}</p>
	           	
				<!-- 臺幣活期性存款餘額 -->
				<div class="main-content-block row" id="N110" style="display:none">
					<div class="col-12 block-title">
						<h3><spring:message code="LB.NTD_Demand_Deposit_Balance"/></h3>
					</div>
					<div id="NTD_Demand" class="col-12">
						<!-- LOADING遮罩區塊 -->
						<div id="n110_table_loadingBox" class="table_loadingBox">
						    <div class="sk-fading-circle">
						      <div class="sk-circle1 sk-circle"></div>
						      <div class="sk-circle2 sk-circle"></div>
						      <div class="sk-circle3 sk-circle"></div>
						      <div class="sk-circle4 sk-circle"></div>
						      <div class="sk-circle5 sk-circle"></div>
						      <div class="sk-circle6 sk-circle"></div>
						      <div class="sk-circle7 sk-circle"></div>
						      <div class="sk-circle8 sk-circle"></div>
						      <div class="sk-circle9 sk-circle"></div>
						      <div class="sk-circle10 sk-circle"></div>
						      <div class="sk-circle11 sk-circle"></div>
						      <div class="sk-circle12 sk-circle"></div>
						    </div>
						</div>
						<table id="n110_table" class="table table-striped" data-show-toggle="first"></table>
					</div>
				</div>
				
				<!-- 臺幣定期性存款明細 -->
				<div class="main-content-block row" id="N420" style="display:none">
					<div class="col-12 block-title">
						<h3><spring:message code= "LB.NTD_Time_Deposit_Detail" /></h3>
					</div>
					<div id="NTD_Demand" class="col-12">
						<!-- LOADING遮罩區塊 -->
						<div id="n420_table_loadingBox" class="table_loadingBox">
						    <div class="sk-fading-circle">
						      <div class="sk-circle1 sk-circle"></div>
						      <div class="sk-circle2 sk-circle"></div>
						      <div class="sk-circle3 sk-circle"></div>
						      <div class="sk-circle4 sk-circle"></div>
						      <div class="sk-circle5 sk-circle"></div>
						      <div class="sk-circle6 sk-circle"></div>
						      <div class="sk-circle7 sk-circle"></div>
						      <div class="sk-circle8 sk-circle"></div>
						      <div class="sk-circle9 sk-circle"></div>
						      <div class="sk-circle10 sk-circle"></div>
						      <div class="sk-circle11 sk-circle"></div>
						      <div class="sk-circle12 sk-circle"></div>
						    </div>
						</div>
						<table id="n420_table" class="table table-striped" data-show-toggle="first"></table>
					</div>
				</div>
				
				<!-- 外匯活存餘額 -->
				<div class="main-content-block row" id="N510" style="display:none">
					<div class="col-12 block-title">
						<h3><spring:message code="LB.FX_Demand_Deposit_Balance"/></h3>
					</div>
					<div id="FX_Demand" class="col-12">
						<!-- LOADING遮罩區塊 -->
						<div id="n510_table_loadingBox" class="table_loadingBox">
						    <div class="sk-fading-circle">
						      <div class="sk-circle1 sk-circle"></div>
						      <div class="sk-circle2 sk-circle"></div>
						      <div class="sk-circle3 sk-circle"></div>
						      <div class="sk-circle4 sk-circle"></div>
						      <div class="sk-circle5 sk-circle"></div>
						      <div class="sk-circle6 sk-circle"></div>
						      <div class="sk-circle7 sk-circle"></div>
						      <div class="sk-circle8 sk-circle"></div>
						      <div class="sk-circle9 sk-circle"></div>
						      <div class="sk-circle10 sk-circle"></div>
						      <div class="sk-circle11 sk-circle"></div>
						      <div class="sk-circle12 sk-circle"></div>
						    </div>
						</div>
						<table id="n510_table" class="table table-striped" data-show-toggle="first"></table>
					</div>
				</div>				
				
				<!-- 外匯定期存款明細 -->
				<div class="main-content-block row" id="N530" style="display:none">
					<div class="col-12 block-title">
						<h3> <spring:message code= "LB.X0964" /></h3>
					</div>
					<div id="FX_Demand" class="col-12">
						<!-- LOADING遮罩區塊 -->
						<div id="n530_table_loadingBox" class="table_loadingBox">
						    <div class="sk-fading-circle">
						      <div class="sk-circle1 sk-circle"></div>
						      <div class="sk-circle2 sk-circle"></div>
						      <div class="sk-circle3 sk-circle"></div>
						      <div class="sk-circle4 sk-circle"></div>
						      <div class="sk-circle5 sk-circle"></div>
						      <div class="sk-circle6 sk-circle"></div>
						      <div class="sk-circle7 sk-circle"></div>
						      <div class="sk-circle8 sk-circle"></div>
						      <div class="sk-circle9 sk-circle"></div>
						      <div class="sk-circle10 sk-circle"></div>
						      <div class="sk-circle11 sk-circle"></div>
						      <div class="sk-circle12 sk-circle"></div>
						    </div>
						</div>
						<table id="n530_table" class="table table-striped" data-show-toggle="first"></table>
					</div>
				</div>
				
				<!-- 借款明細查詢 -->
				<div class="main-content-block row" id="N320" style="display:none">
					<form id="formN320" method="post" action="">
						<input type="hidden" id="N320GO" name="N320GO" value=""/>
						<input type="hidden" id="N552GO" name="N552GO" value=""/>
						<input type="hidden" id="USERDATA_X50_552" name="USERDATA_X50_552" value=''/>
						<input type="hidden" id="USERDATA_X50_320" name="USERDATA_X50_320" value=''/>
						<input type="hidden" id="loanACN" name="loanACN" />
						<input type="hidden" id="loanSEQ" name="loanSEQ" />
						<input type="hidden" id="loanAMT" name="loanAMT" />
					</form>
					<div class="col-12 block-title">
					<!-- 借款明細查詢 -->
						<h3> <spring:message code= "LB.Loan_Detail_Inquiry" /></h3>
					</div>
					<div class="col-12">
						<!-- LOADING遮罩區塊 -->
						<div id="n320_table_loadingBox" class="table_loadingBox">
						    <div class="sk-fading-circle">
						      <div class="sk-circle1 sk-circle"></div>
						      <div class="sk-circle2 sk-circle"></div>
						      <div class="sk-circle3 sk-circle"></div>
						      <div class="sk-circle4 sk-circle"></div>
						      <div class="sk-circle5 sk-circle"></div>
						      <div class="sk-circle6 sk-circle"></div>
						      <div class="sk-circle7 sk-circle"></div>
						      <div class="sk-circle8 sk-circle"></div>
						      <div class="sk-circle9 sk-circle"></div>
						      <div class="sk-circle10 sk-circle"></div>
						      <div class="sk-circle11 sk-circle"></div>
						      <div class="sk-circle12 sk-circle"></div>
						    </div>
						</div>
						<table id="n320tw_table" class="table table-striped" data-show-toggle="first"></table>
						<div id="n320tw_sum" class="text-left">
						</div>
					</div>
					<div id="searchAgain" style="margin: auto;"></div>				 
					<div class="col-12">
						<table id="n320fx_table" class="table table-striped" data-show-toggle="first"></table>
						<div id="n320fx_sum" class="text-left">
						</div>
					</div>
				</div>
				
				<!-- 基金餘額及損益查詢 -->
				<div class="main-content-block row" id="C012" style="display:none">
					<form id="c012formID" method="post"></form>
					<div class="col-12 block-title">
						<h3><spring:message code= "LB.W0903" /></h3>
					</div>
					<div id="FX_Demand" class="col-12">
						<!-- LOADING遮罩區塊 -->
						<div id="c012_table_loadingBox" class="table_loadingBox">
						    <div class="sk-fading-circle">
						      <div class="sk-circle1 sk-circle"></div>
						      <div class="sk-circle2 sk-circle"></div>
						      <div class="sk-circle3 sk-circle"></div>
						      <div class="sk-circle4 sk-circle"></div>
						      <div class="sk-circle5 sk-circle"></div>
						      <div class="sk-circle6 sk-circle"></div>
						      <div class="sk-circle7 sk-circle"></div>
						      <div class="sk-circle8 sk-circle"></div>
						      <div class="sk-circle9 sk-circle"></div>
						      <div class="sk-circle10 sk-circle"></div>
						      <div class="sk-circle11 sk-circle"></div>
						      <div class="sk-circle12 sk-circle"></div>
						    </div>
						</div>
						<table id="c012_table" class="table table-striped" data-show-toggle="first"></table>
						<table id="c012_table_sum" class="table table-striped" data-show-toggle="first"></table>
						<!-- 備註：在途未分配金額不列入上述計算金額。  -->
						<p><spring:message code= "LB.W0941" /></p>
						<p id="c012_note" class="text-left"></p>
						<table id="c012_table_sum2" data-toggle-column="first"></table>
						<br><br>
					</div>
				</div>
				
				<!-- 信用卡持卡總覽 -->
				<div class="main-content-block row" id="N810" style="display:none">
					<div class="col-12 block-title">
						<h3><spring:message code="LB.Credit_Card_Holder_Overview"/></h3>
					</div>
					<div id="cardOverview" class="col-12">
						<!-- LOADING遮罩區塊 -->
						<div id="n810_table_loadingBox" class="table_loadingBox">
						    <div class="sk-fading-circle">
						      <div class="sk-circle1 sk-circle"></div>
						      <div class="sk-circle2 sk-circle"></div>
						      <div class="sk-circle3 sk-circle"></div>
						      <div class="sk-circle4 sk-circle"></div>
						      <div class="sk-circle5 sk-circle"></div>
						      <div class="sk-circle6 sk-circle"></div>
						      <div class="sk-circle7 sk-circle"></div>
						      <div class="sk-circle8 sk-circle"></div>
						      <div class="sk-circle9 sk-circle"></div>
						      <div class="sk-circle10 sk-circle"></div>
						      <div class="sk-circle11 sk-circle"></div>
						      <div class="sk-circle12 sk-circle"></div>
						    </div>
						</div>
						<table id="n810_table" class="table table-striped" data-show-toggle="first"></table>
					</div>
				</div>
				
				<!-- 中央登錄債券餘額 -->
				<div class="main-content-block row" id="N870" style="display:none">
					<div class="col-12 block-title">
						<h3><spring:message code= "LB.W0034" /></h3>
					</div>
					<div id="bondbalance" class="col-12">
						<!-- LOADING遮罩區塊 -->
						<div id="n870_table_loadingBox" class="table_loadingBox">
						    <div class="sk-fading-circle">
						      <div class="sk-circle1 sk-circle"></div>
						      <div class="sk-circle2 sk-circle"></div>
						      <div class="sk-circle3 sk-circle"></div>
						      <div class="sk-circle4 sk-circle"></div>
						      <div class="sk-circle5 sk-circle"></div>
						      <div class="sk-circle6 sk-circle"></div>
						      <div class="sk-circle7 sk-circle"></div>
						      <div class="sk-circle8 sk-circle"></div>
						      <div class="sk-circle9 sk-circle"></div>
						      <div class="sk-circle10 sk-circle"></div>
						      <div class="sk-circle11 sk-circle"></div>
						      <div class="sk-circle12 sk-circle"></div>
						    </div>
						</div>
						<table id="n870_table" data-toggle-column="first"></table>
					</div>
				</div>
				
				<!-- 黃金存摺餘額 -->
				<div class="main-content-block row" id="N190" style="display:none">
					<div class="col-12 block-title">
						<h3> <spring:message code= "LB.X0971" /></h3>
					</div>
					<div id="cardOverview" class="col-12">
						<!-- LOADING遮罩區塊 -->
						<div id="n190_table_loadingBox" class="table_loadingBox">
						    <div class="sk-fading-circle">
						      <div class="sk-circle1 sk-circle"></div>
						      <div class="sk-circle2 sk-circle"></div>
						      <div class="sk-circle3 sk-circle"></div>
						      <div class="sk-circle4 sk-circle"></div>
						      <div class="sk-circle5 sk-circle"></div>
						      <div class="sk-circle6 sk-circle"></div>
						      <div class="sk-circle7 sk-circle"></div>
						      <div class="sk-circle8 sk-circle"></div>
						      <div class="sk-circle9 sk-circle"></div>
						      <div class="sk-circle10 sk-circle"></div>
						      <div class="sk-circle11 sk-circle"></div>
						      <div class="sk-circle12 sk-circle"></div>
						    </div>
						</div>
						<table id="n190_table" class="table table-striped" data-show-toggle="first"></table>
					</div>
				</div>
				
				
				<!-- 說明 -->
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>
					<!-- 本功能自動提供台幣存款歸戶查詢，欲增加查詢項目，請按 -->
					<li><spring:message code= "LB.Allact_P1_D1-1" />
						<!-- 帳戶總覽設定 -->
						<input type="button" name="DPSET" id="DPSET" value="<spring:message code= "LB.Allact_P1_D1-2" />" onClick="fstop.getPage('${pageContext.request.contextPath}'+'/PERSONAL/SERVING/account_settings','', '')">。
					</li>
					<!-- 帳戶總覽設定項目愈多，查詢時等待系統回應時間愈長。 -->
					<li><spring:message code= "LB.Allact_P1_D2" /></li>
		        </ol>
					
			</section>
		</main>
	</div><!-- content row END -->
	
	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>