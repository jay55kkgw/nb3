<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/checkIdProcess.js"></script>

	<script type="text/javascript">
	
		<!-- 不驗證是否是IKey使用者 -->
		var notCheckIKeyUser = true;
		var myobj = null; // 自然人憑證用
		var urihost = "${__ctx}";
		
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 初始化驗證碼
			setTimeout("initKapImg()", 200);
			// 生成驗證碼
			setTimeout("newKapImg()", 300);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		
		// 畫面初始化
		function init() {
			// 表單驗證初始化
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
			// 確認鍵 click
			goOn();
			// 判斷顯不顯示驗證碼
			chaBlock();
			// 交易機制 click
			fgtxwayClick();
			
			$("#CUSIDN").focus();
		}
		
		
		// 確認鍵 Click
		function goOn() {
			$("#CMSUBMIT").click( function(e) {
				// 送出進表單驗證前將span顯示
				$("#hideblock").show();
				
				console.log("submit~~");
	
				// 表單驗證
				if ( !$('#formId').validationEngine('validate') ) {
					e.preventDefault();
				} else {
					// 解除表單驗證
					$("#formId").validationEngine('detach');
					
					// 通過表單驗證準備送出
					processQuery();
				}
			});
		}
		
		// 通過表單驗證準備送出
		function processQuery() {
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
		    // 表單驗證--不可為企業戶
		   	if ($("#CUSIDN").val().length != 10) {
		   		errorBlock(
					null, 
					null,
					["本功能僅開放年滿二十歲之本國自然人。"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
		   		
		   		// 表單驗證初始化
				$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
		   		
		   	    return false;      		   	       		   	
		   	}
		    
		    
		 // 交易機制選項
			switch(fgtxway) {
				case '1':
					// IKEY
					useIKey();
					
					break;
				case '2':
					// 晶片金融卡
					console.log("urihost: " + urihost);
					useCardReader();
					
					break;
				default:
					errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert001' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			}
// 			// 遮罩
//          	initBlockUI();
		    
//             $("#formId").submit();
		}
		
		//IKEY簽章結束
		function uiSignForPKCS7Finish(result){
			var main = document.getElementById("main");
			
			//成功
			if(result != "false"){
				//SubmitForm();
				checkxmlcn();
			}
			//失敗
			else{
				alert("IKEY簽章失敗");
				ShowLoadingBoard("MaskArea",false);
			}
		}
		
		function checkxmlcn(){
			var cusidn = $('#CUSIDN').val();
			var jsondc = $('#jsondc').val();
			var pkcs7Sign = $('#pkcs7Sign').val();
			var uri = urihost+"/COMPONENT/ikey_without_login_aj";
			var rdata = { UID:cusidn, jsondc: jsondc, pkcs7Sign:pkcs7Sign };
			
			fstop.getServerDataEx(uri, rdata, true, IkeyCheckcallback);
		}
		
		function IkeyCheckcallback(response) {
			var checkflag = response.data.checkflag;
			if(checkflag.indexOf("SUCCESSFUL")>-1){
				initBlockUI();
	            $("#formId").submit();
			}else{
				alert("IKEY簽章驗證失敗，您使用的是非本人持有之簽章憑證");
				ShowLoadingBoard("MaskArea",false);
			}
		}
		
		// 使用者選擇晶片金融卡要顯示驗證碼區塊
		function fgtxwayClick() {
			$('input[name="FGTXWAY"]').change(function(event) {
				// 判斷交易機制決定顯不顯示驗證碼區塊
				chaBlock();
			});
		}
		// 判斷交易機制決定顯不顯示驗證碼區塊
		function chaBlock() {
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
			// 交易機制選項
			if(fgtxway == '2'){
				// 若為晶片金融卡才顯示驗證碼欄位
				$("#chaBlock").show();
			}else{
				// 若非晶片金融卡則隱藏驗證碼欄位
				$("#chaBlock").hide();
			}
		}
		
		// 驗證碼刷新
		function changeCode() {
			$('input[name="capCode"]').val('');
			// 大小版驗證碼用同一個
			console.log("changeCode...");
			$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

			// 登入失敗解遮罩
			unBlockUI(initBlockId);
		}
		
		// 初始化驗證碼
		function initKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
		}
		
		// 生成驗證碼
		function newKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').click(function() {
				$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
			});
		}
		
		//取得卡片主帳號結束
		function getMainAccountFinish(result){
			//成功
			if(result != "false"){
				var main = document.getElementById("formId");
				main.ACNNO.value = result;
				var cardACN = result;
				if(cardACN.length > 11){
					cardACN = cardACN.substr(cardACN.length - 11);
				}
				
//				var x = { method: 'get', parameters: '', onComplete: CheckIdResult };	
//				var myAjax = new Ajax.Request( "simpleform?trancode=N953&TXID=N953&ACN=" + cardACN, x);
				var cusidn = $('#CUSIDN').val();
				var uri = urihost+"/COMPONENT/component_without_id_aj";
				var rdata = { UID: cusidn, ACN: cardACN };
				
				fstop.getServerDataEx(uri, rdata, true, CheckIdResult);
			}
			//失敗
			else{
				showTempMessage(500,"晶片金融卡身份查驗","","MaskArea",false);
			}
		}
		
	</script>
</head>

<body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0847" /></li>
		</ol>
	</nav>
	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 主頁內容  -->
        <main class="col-12">
            <section id="main-content" class="container">
                <h2>預約開立基金戶</h2>
                <div id="step-bar">
                    <ul>
						<li class="active">注意事項與權益</li>
						<li class="">投資屬性調查</li>
						<li class="">開戶資料</li>
						<li class="">確認資料</li>
						<li class="">完成申請</li>
					</ul>
                </div>
                
				<form method="post" id="formId" name="formId" action="${__ctx}${next}">
	                <!-- 交易機制所需欄位 -->
					<input type="hidden" id="jsondc" 	name="jsondc"  value="N361">
					<input type="hidden" id="ISSUER" 	name="ISSUER" />
					<input type="hidden" id="ACNNO" 	name="ACNNO" />
					<input type="hidden" id="TRMID" 	name="TRMID" />
					<input type="hidden" id="iSeqNo" 	name="iSeqNo" />
					<input type="hidden" id="ICSEQ" 	name="ICSEQ" />
					<input type="hidden" id="ICSEQ1" 	name="ICSEQ1" />
					<input type="hidden" id="TAC" 		name="TAC" />
					<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" />
					<input type="hidden" id="CHIP_ACN"  name="CHIP_ACN" />
					<input type="hidden" id="OUTACN"	name="OUTACN">
	                
	                <div class="main-content-block row">
	                    <div class="col-12 terms-block">
	                        <div class="ttb-message">
							<p>注意事項</p>
						</div>
						<div class="text-left">
							<p>親愛的客戶您好：</p>
							<p>歡迎您使用臺灣企銀線上預約開立基金戶服務，請您詳閱下列告知事項並填寫開戶資料完成交易程序：</p>
							<ul>
								<li data-num="">
									<span>1.年滿20歲之本國自然人 (非美國公民或稅務居民身份)，且未受監護或輔助宣告。</span>
								</li>
								<li data-num="">
									<span>2.已申請使用本行網路銀行且已約定新臺幣活期性存款（不含支票存款）帳戶。</span>
								</li>
								<li data-num="">
									<span>3.已申請使用本行晶片金融卡或電子簽章(憑證载具)。</span>
								</li>
								<li data-num="">
									<span>4.線上預約開戶完成後，審核約需1個工作天。本行會以電子郵件通知開立基金戶之審核結果，審核通過後即可通過本行網路銀行辦理基金交易服務。</span>
								</li>
								<li data-num="">
									<span>5.對於您線上申請預約開立基金戶服務，本行保留准駁之權利。</span>
								</li>
							</ul>
						</div>
						<p class="form-description">請輸入您的身分證字號</p>
						<div class="ttb-input-block">
                            <!-- 身分證字號 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4>身分證字號</h4>
	                                    </label>
	                                </span>
	                                <span class="input-block">
	                                    <div class="ttb-input">
	                                        <input type="text" id="CUSIDN" name="CUSIDN" value="" placeholder="請輸入身分證字號" maxlength="10"
	                                        	class="text-input validate[required, funcCall[validate_checkSYS_IDNO[CUSIDN]]]" >
	                                    </div>
	                                </span>
	                            </div>
	                            <div class="classification-block">
									<p>身份驗證</p>
								</div>
								
								<!-- 交易機制 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>驗證機制</h4>
										</label>
									</span>

									<!-- 交易機制選項 -->
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
												<!-- IKEY -->
												<spring:message code="LB.Electronic_signature" />
												<input type="radio" name="FGTXWAY" id="CMIKEY" value="1">
												<span class="ttb-radio"></span>
											</label>
										</div>

										<div class="ttb-input">
											<label class="radio-block">
												<!-- 晶片金融卡 -->
												<spring:message code="LB.Financial_debit_card" />
												<input type="radio" name="FGTXWAY" id="CMCARD" value="2" checked="checked">
												<span class="ttb-radio"></span>
											</label>
										</div>
									</span>
								</div>
	                            <!-- 驗證碼 -->
	                            <div class="ttb-input-item row" id="chaBlock" style="display:none">
	                                <span class="input-title">
	                                    <label>
	                                        <h4>驗證碼</h4>
	                                    </label>
	                                </span>
	                                <span class="input-block">
										<div class="ttb-input">
											<input id="capCode" type="text" class="text-input input-width-125"
												name="capCode" placeholder="請輸入驗證碼" maxlength="6" autocomplete="off">
											<img name="kaptchaImage" class="verification-img"/>
											<input type="button" name="reshow" class="ttb-sm-btn btn-flat-gray validate[required]" onclick="changeCode()" value="重新產生" />
											<span class="input-remarks">請注意：英文不分大小寫，限半形字元</span>
										</div>
									</span>
	                            </div>
	                        </div>
	                        
	                        <input type="button" id="CLOSEPAGE" value="取消" class="ttb-button btn-flat-gray" onclick="window.close();" />
	                        <input type="BUTTON" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="下一步" name="">
	                        
	                    </div>
	                </div>
				</form>
				
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>