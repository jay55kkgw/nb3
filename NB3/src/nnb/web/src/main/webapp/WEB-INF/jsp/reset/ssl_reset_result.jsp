<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
</head>
<body>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	<!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
	<!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 重設交易密碼    -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X1579" /></li>
		</ol>
	</nav>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	<!-- 自然人憑證元件載點 -->
	<a href="${__ctx}/component/windows/natural/TBBankPkcs11ATL.exe" id="naturalComponent" target="_blank" style="display:none"></a>
	
	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<spring:message code= "LB.X1580" />
				</h2>
				<form method="post" id="formId">
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-3">
						</div>
						<div class="col-6">
							<div class="text-left">
								<br>
								<B><spring:message code= "LB.X0152" />:</B>
								<br>
								<spring:message code= "LB.X1581" /><BR>
							           <spring:message code= "LB.X1582" />
							    <br>
							    <br>
						 	</div>
						</div>
						<div class="col-3">
						</div>
					</div>
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>