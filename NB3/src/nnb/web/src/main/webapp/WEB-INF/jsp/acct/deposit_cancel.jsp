<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
    <%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    <script type="text/javascript">
    $(document).ready(function() {
    	initDataTable();	// 將.table變更為DataTable 
        // 確定按鈕事件
        $("#CMSUBMIT").click(function() {
            var radioflag = $('input[type=radio][name=FGSELECT]').is(':checked');
            console.log('radioflag :' + radioflag);
            if (!radioflag) {
            	//錯誤!!尚未選取資料
                //alert('<spring:message code="LB.Field_check_note"/>');
                errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Field_check_note' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
            } else {
                $("#formId").submit();
            }
        });
    });

    //copyTN
    function copyTN() {
        $("#CMMAILMEMO").val($("#CMTRMEMO").val());
    }
   
	//開啟通訊錄email選單
	function openAddressbook()
	{
		window.open('${__ctx}/NT/ACCT/TRANSFER/showAddressbook');
	}
    </script>
</head>

<body>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %> 
    <!-- header     -->
    <header>
        <%@ include file="../index/header.jsp"%>
    </header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 定存服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Current_Deposit_To_Time_Deposit" /></li>
    <!-- 綜存定存解約     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.NTD_Time_Deposit_Termination" /></li>
		</ol>
	</nav>



    <!-- menu、登出窗格 -->
    <div class="content row">
        <!-- 功能選單內容 -->
        <%@ include file="../index/menu.jsp"%>
    <!-- content row END -->
    <!-- 		主頁內容  -->
    <main class="col-12">
        <section id="main-content" class="container">
            <!--  臺幣綜存定存解約 -->
            <h2>
                <spring:message code="LB.NTD_Time_Deposit_Termination" />
            </h2>
            <i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
            <form autocomplete="off" id="formId" method="post" action="${__ctx}/NT/ACCT/TDEPOSIT/deposit_cancel_confirm" >
                <input type="hidden" name="FLAG" value="1">
				<input type="hidden" name="FGTXWAY" value="0">
				<input type="hidden" name="ADOPID" value="N079">
				<input type="hidden" name="TXNTYPE" value="01">
                <c:set var="BaseResultData" value="${deposit_cancel.data}"></c:set>
                <!-- 表單顯示區  -->
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <ul class="ttb-result-list">
                            <!-- 查詢時間 -->
                            <li>
                                <h3><spring:message code="LB.Inquiry_time" />：</h3> 
                                <p>${BaseResultData.CMQTIME }</p>
                            </li>
                            <!-- 資料總數 : -->
                            <li>
                                <h3><spring:message code="LB.Total_records" />：</h3>
                                <p>${BaseResultData.COUNT }
                                    <!-- 筆 -->
                                    <spring:message code="LB.Rows" />
                                </p>
                            </li>
                        </ul>
                       	<!-- 表格區塊 -->
                        <table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
                            <thead>
                            <tr>
                                <!--解約  -->
                                <th>
                                    <spring:message code="LB.Termination" />
                                </th>
                                <!--帳號-->
                                <th>
                                    <spring:message code="LB.Account" />
                                </th>
                                <!--存款種類  -->
                                <th>
                                    <spring:message code="LB.Deposit_type" />
                                </th>
                                <!--存單號碼  -->
                                <th>
                                    <spring:message code="LB.Certificate_no" />
                                </th>
                                <!--存單金額 -->
                                <th>
                                    <spring:message code="LB.Certificate_amount" />
                                </th>
                                <!--利率(%)-->
                                <th>
                                    <spring:message code="LB.Interest_rate1" />
                                </th>
                                <!--計息 方式-->
                                <th>
                                    <spring:message code="LB.Interest_calculation" />
                                </th>
                                <!--起存日-->
                                <th>
                                    <spring:message code="LB.Start_date" />
                                </th>
                                <!--到期日-->
                                <th>
                                    <spring:message code="LB.Maturity_date" />
                                </th>
                        		<!--  自動轉期已轉次數/自動轉期未轉次數 -->
								<th>
									<spring:message code="LB.Automatic_number_of_rotations"/><hr/><spring:message code="LB.Automatic_number_of_unrotated" />
								</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach var="dataList" items="${ BaseResultData.REC }">
                                <tr>
                                    <td class="text-center"> 
                    					<label class="radio-block">&nbsp; 
                                               <input type="radio" name="FGSELECT" value='${dataList.JSON}' <c:if test="${deposit_cancel.data.defaultCheck eq 'TRUE'}">checked</c:if> />
                                            <span class="ttb-radio"></span> 
                                        </label> 
                                    </td> 
                                    <!-- 帳號 -->
                                    <td class="text-center"> ${dataList.ACN }</td>
                                    <!-- 存款種類 -->
                                    <td class="text-center"> ${dataList.TYPENAME }</td>
                                    <!-- 存單號碼-->
                                    <td class="text-center"> ${dataList.FDPNUM }</td>
                                    <!-- 存單金額 -->
                                    <td class="text-right">
                                        <fmt:formatNumber type="number" minFractionDigits="2" value='${dataList.AMTFDP }' />
                                    </td>
                                    <!-- 利率(%) -->
                                    <td class="text-right"> ${dataList.ITR }</td>
                                    <!-- 計息方式 -->
                                    <td class="text-center"> ${dataList.INTMTHNAME}</td>
                                    <!-- 起存日 -->
                                    <td class="text-center"> ${dataList.SHOWDPISDT }</td>
                                    <!-- 到期日-->
                                    <td class="text-center"> ${dataList.SHOWDUEDAT }</td>
                                    <!-- 自動轉期已轉次數/自動轉期未轉次數 -->
                                    <td class="text-center"> ${dataList.ILAZLFTM }<hr/>${dataList.SHOWAUTXFTM }</td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                        <div class="ttb-input-block">
                            <!-- 交易備註 -->
                          <div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Transfer_note" /></h4>
									</label>
								</span> 
								<!--(可輸入20字的用途摘要，您可於「轉出紀錄查詢」功能中查詢) -->
								<span class="input-block">
									<div class="ttb-input">
										<input type="text" id="CMTRMEMO" name="CMTRMEMO" class="text-input" maxlength="20"  value="">
										<span class="input-unit"></span>
										<span class="input-remarks"><spring:message code="LB.Transfer_note_note" /></span>
									</div>
								</span>
							</div>
                            <!-- 轉出成功 Email通知 -->
							<div class="ttb-input-item row">
								<!-- 轉出成功Email 通知(可不填) -->
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.X0479" />
										：</h4>
									</label>
								</span>
								<span class="input-block">
									<!-- 通知本人 -->
									<div class="ttb-input">
										<span class="input-subtitle subtitle-color">
											<spring:message code="LB.Notify_me"/>：
										</span>
										<span class="input-subtitle subtitle-color">
											${sessionScope.dpmyemail}
										</span>
									</div>
									<!--另通知 -->
									<div class="ttb-input">
										<span class="input-subtitle subtitle-color">
											<spring:message code="LB.Another_notice"/>
											：
										</span>
									</div>
									<!-- 通訊錄 -->
									<div class="ttb-input">
										<!--非必填 -->
										<spring:message code="LB.Not_required" var="notrequired"></spring:message>
										<input type="text" id="CMTRMAIL" name="CMTRMAIL" class="text-input" maxlength="500" placeholder="${notrequired}">
										<span class="input-unit"></span>
										<!--window open 去查 TxnAddressBook  參數帶入身分證字號 -->
										<button class="btn-flat-orange" type="button" id="AddressbookBtn" onclick="openAddressbook()"><spring:message code="LB.Address_book" /></button>
									</div>
									<div class="ttb-input">
									<!--摘要內容-->
										<spring:message code="LB.Summary" var="summary"></spring:message>
										<input type="text" id="CMMAILMEMO" name="CMMAILMEMO"  class="text-input"  maxlength="20" placeholder="${summary}">
										<span class="input-unit">
										</span>
											<!-- 同交易備註 -->
										<button class="btn-flat-orange" type="button" onclick="copyTN()">
											<spring:message code="LB.As_transfer_note" />
										</button>
									</div>
								</span>
							</div>
                        </div>
                        <!--button 區域 -->
                            <!--重新輸入 -->
                            <spring:message code="LB.Re_enter" var="cmRest"></spring:message>
                            <input type="reset" name="CMRESET" id="CMRESET" value="${cmRest}" class="ttb-button btn-flat-gray">
                        	 <!-- 確定 -->
                            <spring:message code="LB.Confirm" var="cmSubmit"></spring:message>
                            <input type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" class="ttb-button btn-flat-orange">
                        <!--button 區域 -->
                    </div>
                </div>
                <div class="text-left">
                    	<!-- 		說明： -->
                    <ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page"/></p>
<!--                         <li>定存解約之稅後本金及利息轉入該綜存活期性存款帳戶。</li> -->
						<li><span><spring:message code="LB.Deposit_cancel_P1_D1"/></span></li>
<!--                         <li>『狀態』為『＊』或『質押』或『質借』者，係指不得於一般網銀執行中途解約，請洽原開戶分行。</li> -->
                        <li><span><spring:message code="LB.Deposit_cancel_P1_D2"/></span></li>
                        <li><span><spring:message code="LB.Deposit_cancel_P1_D3"/><br>
                            	<spring:message code="LB.Deposit_cancel_P1_D3_1"/><br>
                            	<spring:message code="LB.Deposit_cancel_P1_D3_2-1"/><br>
                            <a href="${__ctx}/public/transpay.htm" target="_blank">
                            	<spring:message code="LB.Deposit_cancel_P1_D3_2-2"/>
                            </a>
                            </span>
                         </li>
                        <li><span><spring:message code="LB.Deposit_cancel_P1_D4"/></span></li>
                    </ol>
                </div>
            </form>
        </section>
        <!-- 		main-content END -->
    </main>
        </div>
    <!-- 	content row END -->
    <%@ include file="../index/footer.jsp"%>
</body>

</html>