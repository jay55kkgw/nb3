<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js_u2.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <!-- <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script> -->
  <title>${jspTitle}</title>
  <script type="text/javascript">
    $(document).ready(function () {
      window.print();
    });
  </script>
</head>
<body class="watermark" style="-webkit-print-color-adjust: exact">
	<br/><br/>
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
	<br/><br/><br/>
	<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
	<br/><br/><br/>
	
	<div> 
		<div class="ttb-input-block">
			<!-- 查詢時間 -->
			<p>
				<spring:message code="LB.Inquiry_time" /> : ${CMQTIME}
			</p>
			<!-- 資料總數 : -->
			<p>
				<spring:message code="LB.Total_records" /> : ${CMRECNUM}
				<!--筆 -->
				<spring:message code="LB.Rows" />
			</p>
			
			<!-- 資料Row -->
			<c:forEach var="tableList" items="${print_datalistmap_data}">
				<table class="print">
				
					<thead>
						<tr>
							<!-- 帳號 -->
							<th><spring:message code="LB.Account"/></th>
							<th class="text-left" colspan="9">${tableList.ACN}</th>
						</tr>
						<tr>
							<!-- 憑證號碼 -->
<!-- 憑證號碼 -->						<th><spring:message code= "LB.W0023" /></th>
							<!-- 基金代號-->
<!-- 基金代號 -->						<th><spring:message code= "LB.W0024" /></th>
							<!-- 基金名稱 -->
<!-- 基金名稱 -->						<th><spring:message code= "LB.W0025" /></th>
							<!-- 基金名稱 -->
<!-- 交易金額 -->						<th><spring:message code= "LB.W0016" /></th>
							<!-- 單位數 -->
<!-- 單位數 -->							<th><spring:message code= "LB.W0027" /></th>
							<!-- 基準日-->
<!-- 基準日 -->							<th><spring:message code= "LB.W0028" /></th>
							<!-- 買入價格 -->
<!-- 買入價格 -->						<th><spring:message code= "LB.W0029" /></th>
							<!-- 參考匯率 -->
<!-- 參考匯率 -->						<th><spring:message code= "LB.W0030" /></th>
							<!-- 台幣現值 -->
<!-- 臺幣現值 -->						<th><spring:message code= "LB.W0031" /></th>
							<!-- 損益-->
<!-- 損益 -->						<th><spring:message code= "LB.W0032" /></th>
						</tr>
					</thead>
					<tbody>
						<c:if test="${tableList.Table.size() > 0}">
							<c:forEach var="dataList" items="${tableList.Table}">
								<tr>
									<td>${dataList.FUNDNUM}</td>
									<td>${dataList.FUNDID}</td>
									<td class="text-left">${dataList.FDNAME}</td>
									<td class="text-right">${dataList.FUNDAMT }</td>
									<td class="text-right">${dataList.FUNDPNT }</td>
									<td class="text-right">${dataList.BASEDAY }</td>
									<td class="text-right">${dataList.BYPRICE }</td>
									<td class="text-right">${dataList.ITR }</td>
									<td class="text-right">${dataList.REFAVL }</td>
									<td class="text-right">${dataList.BAL }</td>
								</tr>
							</c:forEach>
						</c:if>
						<c:if test="${tableList.Table.size() <= 0}">
							<tr>
								<td>${tableList.msgCode}</td>
								<td>${tableList.msgName}</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</c:if>
					</tbody>
				</table>
				<br>
			</c:forEach>
		</div>
	</div>
	
	<br>
	<br>
</body>
</html>