<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
    <meta content="" name="description">
    <meta content="臺灣中小企業銀行" name="author">
    <title>臺灣中小企業銀行 - 全國性繳費平台線上約定作業</title>
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+TC&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <link href="${__ctx}/ebillApply/css/bootstrap.min.css" rel="stylesheet">
    <link href="${__ctx}/ebillApply/css/full-width-pics.css" rel="stylesheet">
    <link href="${__ctx}/ebillApply/css/e-style.css" rel="stylesheet">
    <link href="${__ctx}/ebillApply/css/all.css" rel="stylesheet">
    <link href="${__ctx}/ebillApply/css/bootstrap-select.min.css" rel="stylesheet">
</head>

<body class="page">
    <header>
        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg fixed-top">
            <div class="container">
                <div class="col-12 text-center">
                    <a class="navbar-brand" href=""><img src="${__ctx}/ebillApply/img/logo_tbb.svg"></a>
                </div>
            </div>
        </nav>
        <!-- Header section -->
    </header>
    <main class="px-3 px-sm-0 py-2">
        <form id="validationForm" method="post">
            <input type="hidden" name="form_smscode" id="form_smscode_out" value="">
            <div class="container mt-3 mt-sm-3 px-0">
                <div class="row">
                    <div class="col-md-12 py-sm-4">
                        <div class="progressbar text-center">
                            <ul class="list-inline">
                                <li class="list-inline-item done"><span>01</span></li>
                                <li class="list-inline-item current"><span>02</span>
                                    <step>網銀認證</step>
                                </li>
                                <li class="list-inline-item"><span>03</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <p class="main-content-desc text-left">請確認您的約定資訊</p>
            </div>
            <div class="container form-table mt-3 mt-sm-3">
                <div class="row">
                    <div class="col-md-12 main-content pt-3 py-sm-4 px-sm-5">
                        <div class="form-group">
                            <p class="form-id form-title"><span class="form-required"> * </span>約定的帳戶號碼</p>
                            <div class="container table form-table col-md-12">
                                <div class="row pt-sm-0">
                                    <div class="col-12 col-sm-6 px-0 py-sm-3" id="form-account">
                                        <div class="form-check form-check-inline mb-md-0 w-100">
                                            <select class="selectpicker form-control user-number w-100"
                                                id="form_account" name="form_account" data-width="230px" required>
                                                <option hidden disabled selected>請選擇欲綁定的帳戶</option>
                                                <c:forEach var="dataList" items="${ accountBinding_step3.data.ACCLIST }">
                                                <option value="${dataList.key}">${dataList.key}</option>
                                                </c:forEach>
<%--                                                 <option value="${accountBinding_step3.data.PAYACCOUNT}">${accountBinding_step3.data.PAYACCOUNT}</option> --%>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <p class="form-id form-title py-0"><span class="form-required"> * </span>您的E-mail</p>
                            <p class="user-information" style="margin-top:-10px;">${accountBinding_step3.data.MAILADDR_sh}</p>
                            <p class="content-note">本行將透過您留存於本行的Email，發送約定的結果。</p>
                        </div>
                        <div class="form-group">
                            <p class="form-id form-title py-0"><span class="form-required"> * </span>您的手機號碼 (留存於本行的號碼)
                            </p>
                            <p class="user-information" style="margin-top:-10px;">${accountBinding_step3.data.MOBTEL_sh}</p>
                        </div>
                        <p class="form-branch_name-data form-normal">下一步，我們將發送簡訊認證碼給您，驗證您的手機號碼。</p>
                    </div>
                </div>
            </div>
            <br><br>
            <div class="container">
                <div class="row action-container">
                    <div class="col-sm-12 px-0">
                        <div class="float-right">
                            <button id="goback" class="back">取消</button>
                            <a id="getSMSCodebtn" class="ml-2 btn btn-primary check-phone" data-toggle="modal" data-target="#mv-getSMSCode"
                                onclick="javascript:getsmsotp()">驗證手機號碼 <i class="fas fa-chevron-right float-right mt-1"
                                    style="color: #fff;padding: 1px 4px 0;"></i></a>
                        </div>
                        <br>
                        <div class="notice-txt mt-5">
                            <p style="color: #aaa;font-size: 16px;">注意事項</p>
                            <li>1. 如上揭您的Email或手機號碼與留存本行的資料不一致，請臨櫃或至網路銀行進行變更 (隔日生效)。</li>
                            <li>2. 本交易點選確認送出後將無法取消，請您再次確認資料。</li>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </main>
    <footer class="py-sm-0 d-flex align-items-center align-middle">
        <div class="container">
            <div class="row d-flex align-items-center align-middle">
                <div class="col-12 mt-3 mt-sm-0 pr-sm-0 text-center order-last order-sm-first">
                    <h6 class="copyright mb-0">©臺灣中小企業銀行</h6>
                    <span class="small"><a href="https://www.tbb.com.tw/web/guest/-173" target="_blank">隱私權聲明</a> | <a
                            href="https://www.tbb.com.tw/web/guest/-551" target="_blank">安全政策</a></span>
                </div>
                <div class="col-12 mt-3 mt-sm-0 p-0 text-center d-sm-none">
                    <p class="mt-sm-4">24H服務專線 <a href="tel:0800-01-7171">0800-01-7171</a>(限市話), <a
                            href="tel:02-2357-7171">02-2357-7171</a></p>
                </div>
                <div class="col-12 pt-0 footer-sns-link">
                    <ul class="list-inline ml-auto mb-sm-0 text-center">
                        <li class="list-inline-item d-none d-sm-inline-block mr-2">
                            <p>24H服務專線 <a href="tel:0800-01-7171">0800-01-7171</a>(限市話), <a
                                    href="tel:02-2357-7171">02-2357-7171</a></p>
                        </li>
                        <li class="list-inline-item mr-2">
                            <a href="https://www.facebook.com/tbbdreamplus/" target="_blank"><i
                                    class="fab fa-facebook"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://www.youtube.com/channel/UCyRmUHjcJV3ROmXrF7q3xOA" target="_blank"><i
                                    class="fab fa-youtube"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!-- Bootstrap core JavaScript -->
    <script src="${__ctx}/ebillApply/js/jquery.min.js"></script>
    <script src="${__ctx}/ebillApply/js/bootstrap.bundle.min.js"></script>
    <script src="${__ctx}/ebillApply/js/jquery.validate.min.js"></script>
    <script src="${__ctx}/ebillApply/js/localization/messages_zh_TW.js"></script>
    <script src="${__ctx}/ebillApply/js/bootstrap-select.min.js"></script>
    <script src="${__ctx}/ebillApply/js/e-bill-3.js"></script>

    <!-- SMS Modal -->
    <div class="modal fade" id="mv-getSMSCode" tabindex="-1" role="dialog" aria-labelledby="mv-getSMSCodeTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title mx-auto mt-3" id="mv-getSMSCodeLongTitle">請輸入簡訊驗證碼</h4>
                </div>
                <!--connect error start-->
                <div id="otpErrorBlock" class="error-block mb-1 ml-3 mr-3" style='display:none;'>
                    <div class="row">
                        <div class="col-3">
                            <div class="error-group">
                                <img src="${__ctx}/ebillApply/img/apply-failed-white.svg" alt=""
                                    class="pl-1 img-fluid error-icon float-right">
                            </div>
                        </div>
                        <div class="col-9">
                            <div class="error-txt">
                                <p class="error-lg">抱歉，連線有點問題</p>
                                <p class="error-xs">您與本行連線逾時，請重新登入</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!--connect error over-->
                <div class="modal-body form-table">
                    <div class="message-group">
                        <div class="text-center">
                            <p class="modal-txt mt-2">確認簡訊已經傳送到 <span
                                    class="form-highlight user-phone">${accountBinding_step3.data.MOBTEL_sh}</span></p>
                            <p class="modal-txt" style="margin-top: -15px;">有效時間剩餘：<span id="sms_countdown"
                                    class="form-highlight">120秒</span></p>
                            <p class="modal-txt" style="margin-top: -15px;">簡訊驗證碼每日可發送次數:<span id="sms_lefttimes"
                                    class="form-highlight"></span>次</p>
                            <form id="SMSvalidationForm" class="form-table" action="${__ctx}/EBILL/APPLY/accountBinding_result">
                                <div class="row d-flex justify-content-center mt-4 list-inline m-1">
                                    <input type="number" pattern="[0-9]*" inputmode="numeric"
                                        class="form-control form-sms-code list-inline-item" id="form_smscode"
                                        name="form_smscode" aria-label="form-sms" maxlength="8"
                                        placeholder="請輸入8位數簡訊驗證碼"
                                        oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                        required>
                                    <button type="button" id="sms-btn-resend"
                                        class="btn-default ml-2 mt-2 px-3 list-inline-item"
                                        >重新發送</button>
                                </div>
                                <br>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer mb-2">
                    <div class="float-right">
                        <button class="back" data-dismiss="modal">取消</button>
                        <a href="#" class="btn btn-primary btn-active ml-1" data-toggle="modal"
                            data-target="#mv-getSMSCode" id="submitbtn">送出</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">    
    	// 載入後初始化JS
    	$(document).ready(function () {
        	//註冊按鈕
        	buttonRegistry();
        	chooseAcn();
        	$("#otpErrorBlock").hide();

    	});
    	/**
		* ----- Registry Area -----
		*/
		 //註冊按鈕
        function buttonRegistry() {
            $("#submitbtn").click(function () {
            	$("#form_account").prop("disabled",false);
            	$("#form_smscode_out").val($("#form_smscode").val());
          		$("#validationForm").attr("action", "${__ctx}/EBILL/APPLY/accountBinding_result");
          		$("#validationForm").submit();
            });
            $("#goback").click(function () {
            	$("#validationForm").attr("action", "${accountBinding_step3.data.BackUrl}");
                $("#validationForm").submit();
            });
    	}
      	//註冊enter
      	$(window).keydown(function(e){
			var curKey = window.event ? e.keyCode : e.which;
			if(curKey == 13){//enter键位:13
				e.preventDefault();//攔截form表單btn送出
				if($("#mv-getSMSCode").is(":hidden")){				
					$("#getSMSCodebtn").click();
				}else{
					$("#submitbtn").click();
				}
				
			}
		})
    	/**
		* ----- Logical Area -----
		*/
		//帶入獄設帳號
		function chooseAcn(){
      		var acn="${accountBinding_step3.data.CustBankAcnt_Dec}";
			$("#form_account option[value= '"+ acn.substring(5,acn.length) +"' ]").prop("selected", true);
			$("#form_account").change();
			//若有指定帳號，鎖定下拉選單
			if($("#form_account option[value= '"+ acn.substring(5,acn.length) +"' ]").val()!= undefined)$("#form_account").prop("disabled",true);
      	}
    	//取得otp並發送簡訊
        function getsmsotp() {
            //TODO: session 取值 解密cusidn
            var cusidn = '${accountBinding_step3.data.CUSIDNAJAX}';
            console.log("cusidn>>" + cusidn);
            cusidn = atob(cusidn);
            $("#otpErrorBlock").hide();
            ResetTimerFlag=false;
            //表單驗證
            if (!$("#validationForm").valid())
                return false;
            var main = document.getElementById("validationForm");
            counter = 120;

            $.ajax({
                type: "POST",
                url: "${__ctx}/RESET/smsotp_ajax",
                data: {
                    CUSIDN: cusidn,
                    ADOPID: 'N855',
                    MSGTYPE: 'N855'
                },
                async: false,
                dataType: "json",
                success: function (msg) {
                    callbackgetsmsotp(msg)
                },
                error: function () {
                    //404 500 connnection error
                    $("#error-lg").val();
                    $("#error-xs").val();
                    $("#otpErrorBlock").show();
                }
            })
        }

      	//取得otp的Callback
        function callbackgetsmsotp(response) {
            //alert(response.responseText);
            var main = document.getElementById("validationForm");
            //eval("var result = " + response.responseText);
            console.log(response);
            var msgCode = response[0].MSGCOD;
            var msgName = response[0].MSGSTR;
            var phone = response[0].phone;
            if (msgCode == "0000") {
                phone = phone.substring(0, 6) + "***" + phone.substring(9);
                //TODO　傳送次數更新　SMStimes
                if((3-response[0].SMStimes) >0)$("#sms_lefttimes").text(3-response[0].SMStimes);
                else $("#sms_lefttimes").text("0");
                $("#submitbtn").removeClass("disabled");
                $("#form_smscode").attr("disabled", false);
                $("#form_smscode").focus();
                trigger_smscountdown();
            } else {
            	counter = 1;
            	$('#sms_countdown').html(counter-1 + "秒");
                //systemerror
                $(".error-xs").text(msgCode + " - 請點選[重新發送]");
                $("#form_smscode").attr("disabled", true);
                $("#submitbtn").addClass("disabled");
                //本機測試用
                var demoMode="${accountBinding_step3.data.IBPD_DEMO}";
                if(demoMode=='Y'){
    				$("#form_smscode").attr("disabled", false);
    				$("#submitbtn").removeClass("disabled");
                }
                //本機測試用
                $("#otpErrorBlock").show();

            }
        }
		
      	//NextPage
//       	function gogogo() {
//       		$("#validationForm").attr("action", "${__ctx}/EBILL/APPLY/accountBinding_result");
//       		$("#validationForm").submit();
//       	}
      	
        /**
 		* ----- Funcional Area -----
 		*/

    </script>
</body>

</html>