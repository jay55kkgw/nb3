<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
    <%@ include file="../__import_head_tag.jsp"%>
    <%@ include file="../__import_js_u2.jsp" %>
    <script type="text/javascript">
        $(document).ready(function () {
            initDataTable();	// 將.table變更為DataTable 
            // 確定按鈕事件
            $("#CMSUBMIT").click(function () {
                var radioflag = $('input[type=radio][name=FGSELECT]').is(':checked');
                console.log('radioflag :' + radioflag);
                if (!radioflag) {
                    //錯誤!!尚未選取資料
                    //alert('<spring:message code="LB.Field_check_note"/>');
                    errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Field_check_note' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
                } else {
                    initBlockUI(); //遮罩
                    $("#formId").submit();
                }
            });
        });
    </script>
</head>
<body>
    <!-- header     -->
    <header>
        <%@ include file="../index/header.jsp"%>
    </header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 定存服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Current_Deposit_To_Time_Deposit" /></li>
    <!-- 定存單到期續存     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Renewal_At_Maturity_Of_NTD_Time_Deposit" /></li>
		</ol>
	</nav>



    <!-- menu、登出窗格 -->
    <div class="content row">
        <!-- 功能選單內容 -->
        <%@ include file="../index/menu.jsp"%>
    </div>
    <!-- content row END -->
    <!-- 		主頁內容  -->
    <main class="col-12">
        <section id="main-content" class="container">
            <h2>
                <!-- 臺幣定存單到期續存 -->
                <spring:message code="LB.Renewal_At_Maturity_Of_NTD_Time_Deposit" />
            </h2>
            <i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
            <form id="formId" method="post" action="${__ctx}/NT/ACCT/TDEPOSIT/order_renewal_step1">
                <c:set var="BaseResultData" value="${order_renewal.data}"></c:set>
                <!-- 表單顯示區  -->
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <ul class="ttb-result-list">
                            <!-- 查詢時間 -->
                            <li>
                                <h3><spring:message code="LB.Inquiry_time" /> :</h3>
                                <p>${BaseResultData.CMQTIME }</p>
                            </li>
                                <!-- 資料總數 : -->
                            <li>    
                                <h3><spring:message code="LB.Total_records" /> :</h3>
                                <p>${BaseResultData.CMRECNUM }
                                    <!-- 筆 -->
                                    <spring:message code="LB.Rows" />
                                </p>
                            </li>
                        </ul>
                        <!-- 表格區塊 -->
                        <table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
                            <thead>
                                <tr>
                                    <!--點選  -->
                                    <th>
                                        <spring:message code="LB.Option" />
                                    </th>
                                    <!--帳號-->
                                    <th>
                                        <spring:message code="LB.Account" />
                                    </th>
                                    <!--存款種類  -->
                                    <th>
                                        <spring:message code="LB.Deposit_type" />
                                    </th>
                                    <!--存單號碼  -->
                                    <th>
                                        <spring:message code="LB.Certificate_no" />
                                    </th>
                                    <!--存單金額 -->
                                    <th>
                                        <spring:message code="LB.Certificate_amount" />
                                    </th>
                                    <!--利率(%)-->
                                    <th>
                                        <spring:message code="LB.Interest_rate1" />
                                    </th>
                                    <!--計息 方式-->
                                    <th>
                                        <spring:message code="LB.Interest_calculation" />
                                    </th>
                                    <!--起存日-->
                                    <th>
                                        <spring:message code="LB.Start_date" />
                                    </th>
                                    <!--到期日-->
                                    <th>
                                        <spring:message code="LB.Maturity_date" />
                                    </th>
                                    <!--利息轉入帳號-->
                                    <th>
                                        <spring:message code="LB.Interest_transfer_to_account" />
                                    </th>
                                    <!--自動轉期已轉次數 -->
                                    <th>
                                        <spring:message code="LB.Automatic_number_of_rotations" />
                                    </th>
                                    <!--自動轉期未轉次數 -->
                                    <th>
                                        <spring:message code="LB.Automatic_number_of_unrotated" />
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:if test="${BaseResultData.REC.size() > 0}">
                                    <c:forEach var="dataList" items="${ BaseResultData.REC }">
                                        <tr>
                                             <td class="text-center"> 
		                    					<label class="radio-block">
		                    						&nbsp; 
		                                          	<input type="radio" name="FGSELECT" value='${dataList.JSON}' />
		                                            <span class="ttb-radio"></span> 
		                                        </label> 
                                   			 </td> 
                                            <!-- 帳號 -->
                                            <td class="text-center"> ${dataList.ACN }</td>
                                            <!-- 存款種類 -->
                                            <td class="text-center"> ${dataList.TYPENAME }</td>
                                            <!-- 存單號碼-->
                                            <td class="text-center"> ${dataList.FDPNUM }</td>
                                            <!-- 存單金額 -->
                                            <td class="text-right">
                                                <fmt:formatNumber type="number" minFractionDigits="2"
                                                    value='${dataList.AMTFDP }' />
                                            </td>
                                            <!-- 利率(%) -->
                                            <td class="text-right"> ${dataList.ITR }</td>
                                            <!-- 計息 -->
                                            <td class="text-center"> ${dataList.DPINTTYPE }</td>
                                            <!-- 起存日 -->
                                            <td class="text-center"> ${dataList.SHOWDPISDT }</td>
                                            <!-- 到期日-->
                                            <td class="text-center"> ${dataList.SHOWDUEDAT }</td>
                                            <!-- 利息轉入帳號 -->
                                            <td class="text-center"> ${dataList.TSFACN }</td>
                                            <!-- 自動轉期已轉次數< -->
                                            <td class="text-center"> ${dataList.ILAZLFTM }</td>
                                            <!-- 自動轉期未轉次數 -->
                                            <td class="text-center"> ${dataList.AUTXFTM }</td>
                                        </tr>
                                    </c:forEach>
                                </c:if>
                                <c:if test="${BaseResultData.REC.size() <= 0}">
                                    <tr style="display:none;">
                                    </tr>
                                </c:if>
                            </tbody>
                        </table>
                        <!--   button -->
                            <!--重新輸入 -->
                            <spring:message code="LB.Re_enter" var="cmRest"></spring:message>
                            <input type="reset" name="CMRESET" id="CMRESET" value="${cmRest}" class="ttb-button btn-flat-gray">
                            <!-- 確定 -->
                            <spring:message code="LB.Confirm" var="cmSubmit"></spring:message>
                            <input type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" class="ttb-button btn-flat-orange">
                        <!--button 區域 -->
                    </div>
                </div>
                <div class="text-left">
                    <!-- 		說明： -->
                    <ol class="description-list list-decimal">
                        <p><spring:message code="LB.Description_of_page" /></p>
                        <!--   <li>提供已到期的定期存款、存本取息、整存整付儲蓄存款實體定存單，辦理到期續存；存款種類『其他』者，請至往來分行辦理。</li> -->
                        <li>
                            <span><spring:message code="LB.Order_renewaly_P1_D1" /></span>
                        </li>
                        <!--                         <li>建議請以PC桌上型電腦操作使用，作業系統Windows7、IE11以上。</li> -->
                        <li>
                            <span><spring:message code="LB.Order_renewaly_P1_D2" /></span>
                        </li>
                    </ol>
                </div>
            </form>
        </section>
        <!-- 		main-content END -->
    </main>
    <!-- 	content row END -->
    <%@ include file="../index/footer.jsp"%>
</body>

</html>