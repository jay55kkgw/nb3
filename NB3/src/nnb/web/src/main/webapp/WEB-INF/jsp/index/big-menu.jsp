<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<script type="text/javascript">
	$(document).ready(function(){
		// 初始化
		initMenu();
	});

	// 初始化
	function initMenu(){
		// contorl nav-menu left/right-btn
		$('#nav-left-btn').click(function() {
		    $('#nav-menu').animate({
		        scrollLeft: "-=151"
		    }, "fast", "swing");
		});
		$('#nav-right-btn').click(function() {
		    $('#nav-menu').animate({
		        scrollLeft: "+=151"
		    }, "fast", "swing");
		});
		
		//因語系不同 子選單排列位置計算也不同
		if("${transfer}" == 'en'){
			$('.sec-menu').on('shown.bs.collapse', function() {
	            var maxHeight = -1;
	            var minHeight = 100;
	            var newHeight = 0;
	            var secMenuCount = $(this).find('.sec-menu-item').length;

	            $("#tbb-sec-menu-bg").css({
	                "display": "flex"
	            });
	            
	            $(this).find('.sec-menu-item').each(function() {
	                if ($(this).height() > 550) {
	                    $(this).width(490);
	                    $(this).find('li').addClass('tbb-menu-list-extend');
	                }
	                maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
	                minHeight = minHeight < $(this).height() ? minHeight : $(this).height();
	            });

	            if (secMenuCount < 7) {
	                newHeight = maxHeight;
	                if (secMenuCount == 6 && maxHeight < 500) {
	                    newHeight = maxHeight + minHeight * 1;
	                }
	            } else {
	                newHeight = maxHeight + minHeight + 5;
	                if (secMenuCount == 8) {
	                    newHeight = maxHeight + minHeight + 100;
	                }
	                if (secMenuCount == 10) {
	                    if (maxHeight < 400) {
	                        newHeight = maxHeight + 200;
	                    } else {
	                        newHeight = maxHeight + minHeight * 1.5;
	                    }
	                }
	                if (secMenuCount == 12) {
	                    newHeight = maxHeight + minHeight + 100;
	                }
	            }

	            $(this).find('.sec-menu-list').height(newHeight);

	            $('.sec-menu-list').isotope({
	                layoutMode: 'masonry',
	                itemSelector: '.sec-menu-item',
	                transitionDuration: 0
	            });
	            $('.sec-menu').on('hide.bs.collapse', function() {
	                $("#tbb-sec-menu-bg").css({
	                    "display": "none"
	                });
	            })
	        })
		}
		else{
			$('.sec-menu').on('shown.bs.collapse', function() {
				var maxHeight = -1;
				var minHeight = 100;
				var newHeight = 0;
				var secMenuCount = $(this).find('.sec-menu-item').length;

				$("#tbb-sec-menu-bg").css({
					"display": "flex"
				});

				$(this).find('.sec-menu-item').each(function() {
	                 if ($(this).height() > 550) {
	                    $(this).width(490);
	                    $(this).find('li').addClass('tbb-menu-list-extend');
	                }
					maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
					minHeight = minHeight < $(this).height() ? minHeight : $(this).height();
				});

				if (secMenuCount < 7) {
					newHeight = maxHeight;
					if (secMenuCount == 6 && maxHeight < 500) {
						newHeight = maxHeight + minHeight + 5;
					}
				} else {
					newHeight = maxHeight + minHeight + 5;
					if (secMenuCount == 10) {
						newHeight = maxHeight + minHeight * 0.7;
						if (maxHeight < 270) {
							newHeight = maxHeight + minHeight + 45;
						}
					}
					if (secMenuCount == 12) {
						newHeight = maxHeight + minHeight + 60;
					}
				}

				$(this).find('.sec-menu-list').height(newHeight);

				$('.sec-menu-list').isotope({
					layoutMode: 'masonry',
					itemSelector: '.sec-menu-item',
					transitionDuration: 0
				});
			})

			$('.sec-menu').on('hide.bs.collapse', function() {
				$("#tbb-sec-menu-bg").css({
					"display": "none"
				});
			})
	
		}
		// When the user clicks on the button, scroll to the top of the document
		function topFunction() {
			document.body.scrollTop = 0; // For Safari
			document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
		}
		
		//contorl click outside can close menu
		$(document).click(function(e) {
			if (!$(e.target).is('.sec-menu')) {
				$('.sec-menu.collapse').collapse('hide');
			}
		});
	}
	
	function meun_close(){
		$("#tbb-sec-menu-bg").hide();
		$("[id^=main-fuct-]").removeClass("show");
		$("[id^=CM_]").addClass("collapsed");
	}
	
</script>

<!-- 登出前的遮罩，放在header.jsp -->


<!-- for mobile menu -->
<%@ include file="../index/mobile_menu.jsp"%>
<!-- for mobile menu End-->


<!-- 功能選單 -->
<nav id="ttb-menu">
<!--     <button id="nav-left-btn" class="triangle-left"></button> -->
    <ul id="nav-menu" class="menu-list d-inline-block">
        <li>
        	<!-- 我的首頁 -->
            <a href="#main-fuct-1" class="collapsed" onclick="fstop.getPage('${pageContext.request.contextPath}'+'/INDEX/index','', '')"
					data-toggle="collapse" role="button" aria-expanded="true" aria-controls="main-fuct-01">
                <div class="menu-img-block"><img src="${__ctx}/img/menu-icon-01.svg?a=${jscssDate}"></div>
                <p class="menu-title"><spring:message code="LB.My_home"/></p>
            </a>
        </li>
        <li>
        	<!-- 帳戶總覽 -->
            <a href="#main-fuct-2" class="collapsed" onclick="fstop.getPage('${pageContext.request.contextPath}'+'/OVERVIEW/initview','', '')" 
            		data-toggle="collapse" role="button" aria-expanded="true" aria-controls="main-fuct-02" onmouseenter="meun_close()">
                <div class="menu-img-block"><img src="${__ctx}/img/menu-icon-02.svg?a=${jscssDate}"></div>
                <p class="menu-title"><spring:message code="LB.Account_Overview"/></p>
            </a>
        </li>
        	<!-- 臺幣服務 -->
		<c:forEach var="dataListLayer1" items="${ menuFilterList.data.sort_menu[0].SEED_LAYER }">
			<c:if test="${ dataListLayer1.ADOPID.equals('NTDService') && dataListLayer1.SEED_LAYER.size() > 0 }">
		        <li>
		            <a id="CM_main-fuct-NTDService" href="#main-fuct-NTDService" class="collapsed" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="main-fuct-NTDService" onmouseenter="this.click()">
		                <div class="menu-img-block"><img src="${__ctx}/img/menu-icon-03.svg?a=${jscssDate}"></div>
		                <p class="menu-title" id="main-fuct-NTDService-title"><spring:message code="LB.NTD_Services"/></p>
		            </a>
		        </li>
		    </c:if>
		</c:forEach>
       	<!-- 外幣服務 -->
		<c:forEach var="dataListLayer1" items="${ menuFilterList.data.sort_menu[0].SEED_LAYER }">
			<c:if test="${ dataListLayer1.ADOPID.equals('FXCurrency') && dataListLayer1.SEED_LAYER.size() > 0 }">
		        <li>
		            <a id="CM_main-fuct-FXCurrency" href="#main-fuct-FXCurrency" class="collapsed" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="main-fuct-FXCurrency" onmouseenter="this.click()">
		                <div class="menu-img-block"><img src="${__ctx}/img/menu-icon-04.svg?a=${jscssDate}"></div>
		                <p class="menu-title" id="main-fuct-FXCurrency-title"><spring:message code="LB.FX_Service"/></p>
		            </a>
		        </li>
		    </c:if>
		</c:forEach>
            <!-- 繳費繳稅 -->
		<c:forEach var="dataListLayer1" items="${ menuFilterList.data.sort_menu[0].SEED_LAYER }">
			<c:if test="${ dataListLayer1.ADOPID.equals('fee_tax') && dataListLayer1.SEED_LAYER.size() > 0 }">
		        <li>
		            <a id="CM_main-fuct-fee_tax" href="#main-fuct-fee_tax" class="collapsed" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="main-fuct-fee_tax" onmouseenter="this.click()">
		                <div class="menu-img-block"><img src="${__ctx}/img/menu-icon-05.svg?a=${jscssDate}"></div>
		                <p class="menu-title" id="main-fuct-fee_tax-title"><spring:message code="LB.W0366"/></p>
		            </a>
		        </li>
		    </c:if>
		</c:forEach>
        	<!-- 貸款專區 -->
		<c:forEach var="dataListLayer1" items="${ menuFilterList.data.sort_menu[0].SEED_LAYER }">
			<c:if test="${ dataListLayer1.ADOPID.equals('loanservice') && dataListLayer1.SEED_LAYER.size() > 0 }">
		        <li>
		            <a id="CM_main-fuct-loanservice" href="#main-fuct-loanservice" class="collapsed" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="main-fuct-loanservice" onmouseenter="this.click()">
		                <div class="menu-img-block"><img src="${__ctx}/img/menu-icon-06.svg?a=${jscssDate}"></div>
		                <p class="menu-title" id="main-fuct-loanservice-title"><spring:message code="LB.Loan_Service"/></p>
		            </a>
		        </li>
		    </c:if>
		</c:forEach>
        	<!-- 基金 -->
		<c:forEach var="dataListLayer1" items="${ menuFilterList.data.sort_menu[0].SEED_LAYER }">
			<c:if test="${ dataListLayer1.ADOPID.equals('fundservice') && dataListLayer1.SEED_LAYER.size() > 0 }">
		        <li>
		            <a id="CM_main-fuct-fundservice" href="#main-fuct-fundservice" class="collapsed" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="main-fuct-fundservice" onmouseenter="this.click()">
		                <div class="menu-img-block"><img src="${__ctx}/img/menu-icon-07.svg?a=${jscssDate}"></div>
		                <p class="menu-title" id="main-fuct-fundservice-title"><spring:message code="LB.Funds"/></p>
		            </a>
		        </li>
		    </c:if>
		</c:forEach>
          <!-- 黃金存摺 -->
		<c:forEach var="dataListLayer1" items="${ menuFilterList.data.sort_menu[0].SEED_LAYER }">
			<c:if test="${ dataListLayer1.ADOPID.equals('goldqryservice') && dataListLayer1.SEED_LAYER.size() > 0 }">
		        <li>
		            <a id="CM_main-fuct-goldqryservice" href="#main-fuct-goldqryservice" class="collapsed" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="main-fuct-goldqryservice" onmouseenter="this.click()">
		                <div class="menu-img-block"><img src="${__ctx}/img/menu-icon-08.svg?a=${jscssDate}"></div>
		                <p class="menu-title" id="main-fuct-goldqryservice-title"><spring:message code="LB.W1428"/></p>
		            </a>
		        </li>
		    </c:if>
		</c:forEach>
          <!-- 信用卡 -->
		<c:forEach var="dataListLayer1" items="${ menuFilterList.data.sort_menu[0].SEED_LAYER }">
			<c:if test="${ dataListLayer1.ADOPID.equals('creditcard') && dataListLayer1.SEED_LAYER.size() > 0 }">
		        <li>
		            <a id="CM_main-fuct-creditcard" href="#main-fuct-creditcard" class="collapsed" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="main-fuct-creditcard" onmouseenter="this.click()">
		                <div class="menu-img-block"><img src="${__ctx}/img/menu-icon-09.svg?a=${jscssDate}"></div>
		                <p class="menu-title" id="main-fuct-creditcard-title"><spring:message code="LB.Credit_Card"/></p>
		            </a>
		        </li>
		    </c:if>
		</c:forEach>
        	<!-- 線上服務專區 -->
		<c:forEach var="dataListLayer1" items="${ menuFilterList.data.sort_menu[0].SEED_LAYER }">
			<c:if test="${ dataListLayer1.ADOPID.equals('bank30service') && dataListLayer1.SEED_LAYER.size() > 0 }">
		        <li>
		            <a id="CM_main-fuct-bank30service" href="#main-fuct-bank30service" class="collapsed" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="main-fuct-bank30service" onmouseenter="this.click()">
		                <div class="menu-img-block"><img src="${__ctx}/img/menu-icon-10.svg?a=${jscssDate}"></div>
		                <p class="menu-title" id="main-fuct-bank30service-title"><spring:message code="LB.X0262"/></p>
		            </a>
		        </li>
		    </c:if>
		</c:forEach>
        	<!-- 個人服務 -->
		<c:forEach var="dataListLayer1" items="${ menuFilterList.data.sort_menu[0].SEED_LAYER }">
			<c:if test="${ dataListLayer1.ADOPID.equals('personalservice') && dataListLayer1.SEED_LAYER.size() > 0 }">
		        <li>
		            <a id="CM_main-fuct-personalservice" href="#main-fuct-personalservice" class="collapsed" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="main-fuct-personalservice" onmouseenter="this.click()">
		                <div class="menu-img-block"><img src="${__ctx}/img/menu-icon-11.svg?a=${jscssDate}"></div>
		                <p class="menu-title" id="main-fuct-personalservice-title"><spring:message code="LB.Personal_Service"/></p>
		            </a>
		        </li>
		    </c:if>
		</c:forEach>
    </ul>
<!--     <button id="nav-right-btn" class="triangle-right"></button> -->
</nav>

<!-- TODO menu之後會改成由DB來產生-->
<!-- 第二個區塊 -->
<fmt:setLocale value="${__i18n_locale}"/>
<div id="tbb-sec-menu-bg">
	<nav id="ttb-sec-menu" onmouseleave="meun_close()">
	    <c:forEach var="dataListLayer1" items="${ menuFilterList.data.sort_menu[0].SEED_LAYER }">
		    <div class="sec-menu collapse" id="main-fuct-${dataListLayer1.ADOPID}" aria-labelledby="main-fuct-${dataListLayer1.ADOPID}" data-parent="#ttb-sec-menu">
		        <div class="sec-menu-list">
	    			<c:forEach var="dataListLayer2" items="${ dataListLayer1.SEED_LAYER }">
						<c:if test="${ dataListLayer2.ADOPALIVE.equals('1') && dataListLayer2.ISANONYMOUS.equals('0') }" >
				            <div class="sec-menu-item">
				                <p class="sec-menu-title">
				                	<c:if test="${__i18n_locale eq 'en' }">
		                   				${dataListLayer2.ADOPENGNAME}
		                    		</c:if>
		                    		<c:if test="${__i18n_locale eq 'zh_TW' }">
		                   				${dataListLayer2.ADOPNAME}
		                    		</c:if>
		                    		<c:if test="${__i18n_locale eq 'zh_CN' }">
		                   				${dataListLayer2.ADOPCHSNAME}
		                    		</c:if>
				                </p>
			                	<ul class="third-menu-list">
			    					<c:forEach var="dataListLayer3" items="${ dataListLayer2.SEED_LAYER }">
			    						<c:if test="${ dataListLayer3.ADOPALIVE.equals('1') && dataListLayer3.ISANONYMOUS.equals('0') }" >
						                    <li id="menu_${ dataListLayer3.ADOPID }">
						                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('0') }">
							                    	<a href='#' onclick="fstop.getPage('${pageContext.request.contextPath}'+'${dataListLayer3.URL}','', '')">
							                    		<c:if test="${__i18n_locale eq 'en' }">
							                   				${dataListLayer3.ADOPENGNAME}
							                    		</c:if>
							                    		<c:if test="${__i18n_locale eq 'zh_TW' }">
							                   				${dataListLayer3.ADOPNAME}
							                    		</c:if>
							                    		<c:if test="${__i18n_locale eq 'zh_CN' }">
							                   				${dataListLayer3.ADOPCHSNAME}
							                    		</c:if>
							                   		</a>
						                   		</c:if>
						                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('1') && !dataListLayer3.ADOPID.equals('A4000') }">
							                    	<a target="_blank" href="${pageContext.request.contextPath}${dataListLayer3.URL}">
														<c:if test="${__i18n_locale eq 'en' }">
							                   				${dataListLayer3.ADOPENGNAME}
							                    		</c:if>
							                    		<c:if test="${__i18n_locale eq 'zh_TW' }">
							                   				${dataListLayer3.ADOPNAME}
							                    		</c:if>
							                    		<c:if test="${__i18n_locale eq 'zh_CN' }">
							                   				${dataListLayer3.ADOPCHSNAME}
							                    		</c:if>
													</a>
						                    	</c:if>
						                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('1') && dataListLayer3.ADOPID.equals('A4000') }">
							                    	<a target="_blank" href="${dataListLayer3.URL}${sessionScope.cusidn}">
														<c:if test="${__i18n_locale eq 'en' }">
							                   				${dataListLayer3.ADOPENGNAME}
							                    		</c:if>
							                    		<c:if test="${__i18n_locale eq 'zh_TW' }">
							                   				${dataListLayer3.ADOPNAME}
							                    		</c:if>
							                    		<c:if test="${__i18n_locale eq 'zh_CN' }">
							                   				${dataListLayer3.ADOPCHSNAME}
							                    		</c:if>
													</a>
						                    	</c:if>
						                   	</li>
					                   	</c:if>
			    					</c:forEach>
			                	</ul>
				            </div>
			            </c:if>
		            </c:forEach>
		        </div>
		         <c:if test="${ dataListLayer1.ADOPID.equals('fundservice') && dataListLayer1.SEED_LAYER.size() > 0 }">
		        	<p class="text-box" style="padding: 0 0 30px; margin: 0;">
		        		<font color="red">
		        			<spring:message code="LB.B0031"/>
		        		</font>
		        	</p>
		        </c:if>
		    </div>
	    </c:forEach>
	</nav>
</div>
<script type="text/javascript">

</script>