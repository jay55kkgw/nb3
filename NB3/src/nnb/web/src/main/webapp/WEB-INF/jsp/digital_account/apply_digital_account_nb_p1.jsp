<% response.setHeader("X-Frame-Options", "DENY"); %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %> 
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<!-- 	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous"> -->
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
	<script type="text/javascript">
        $(document).ready(function () {
            initFootable(); // 將.table變更為footable 
            init();
            putDataInit();
        });

        function init() {
			// $("#formId").validationEngine({binded: false,promptPosition: "inline"});
			$("#formId").validationEngine({
			validationEventTriggers:'keyup blur', 
			binded:true,
			promptPosition: "inline",
			addFailureCssClassToField:"isValid",
			scroll: true
			});
	    	$("#CMSUBMIT").click(function(e){
	    		if(!username_check() || !username2_check() || !transpin_check() || !transpin2_check() || !loginpin_check() || !loginpin2_check()){
	    			//alert("請確認 網路銀行帳號密碼設定 資料是否正確");
	    			errorBlock(
						null, 
						null,
						["請確認 網路銀行帳號密碼設定 資料是否正確"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
	    			return false;
	    		}
				var LOGINPIN = $('#LOGINPIN').val();
				var TRANSPIN = $('#TRANSPIN').val();
	    		$('#HLOGINPIN').val(pin_encrypt(LOGINPIN));
	    		$('#HTRANSPIN').val(pin_encrypt(TRANSPIN));
				e = e || window.event;
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else { 
					$("#formId").validationEngine('detach');
		        	var action = '${__ctx}/DIGITAL/ACCOUNT/apply_digital_account_nb_p2';
		        	$("#formId").attr("action", action);
		        	$("#formId").submit();
				}
			});
			$("#CMBACK").click(function(e){
				$("#formId").validationEngine('detach');
	        	var action = '${__ctx}/DIGITAL/ACCOUNT/apply_digital_account_p2';
				$("#back").val('Y');
				$("#formId").attr("action", action);
				$("#formId").submit();
			});
        }

		function ValidateValue(textbox) {
			var IllegalString = "+-_＿︿%@[`~!#$^&*()=|{}':;',\\[\\].<>/?~！#￥……&*（）——|{}【】‘；：”“'。，、？]‘'";
			var textboxvalue = textbox.value;
			var index = textboxvalue.length - 1;
			var s = textbox.value.charAt(index);
			if (IllegalString.indexOf(s) >= 0) {
				s = textboxvalue.substring(0, index);
				textbox.value = s;
			}
		}	
		
		/***********************************/
		/*****			動態檢驗		   *****/
		/***********************************/
		function checkUserName(userID){
			var username=$('#'+userID).val();
			var min = 6;
			var max = 16;
			if(!checkLength(username,min,max)){
				return false;
			}
			if(!check2EnChar(username)){;
				return false;
			}
			if(checkIllegalChar(username)){
				return false;
			}
			if(checkDuplicateChar(username)){
				return false;
			}
			if(checkContinuousENChar(username)){
				return false;
			}
			return true;
		}
		function NewCheckpwd(oField, bCanEmpty)
		{
			try
			{
			    var sNumber = $('#'+oField).val();
			    if (bCanEmpty == false && sNumber == "")
			    {
			        return false;
			    }
			    var reNumber = new RegExp(/^[A-Za-z0-9]+$/);
			    if (!reNumber.test(sNumber))
			    {
			        return false;
			    }
			}
			catch (exception)
			{
				return false;
			}
			return true;
		}
		function CheckNotEmpty(oField)
		{
			try
			{
			    if ($('#'+oField).val() == "")
			        return false;
			}
			catch (exception)
			{
				return false;
			}
			return true;
		}
		//不能為連號數字
		function chkSerialNum(obj) {
			var str = $('#'+obj).val();
			if(str != '') {
				var iCode1 = str.charCodeAt(0); 
				if(iCode1 >=48 && iCode1 <= 57) {
					var iCode2 = 0;
					var bPlus = true;
					var bMinus = true;
					for(var i=1;i<str.length;i++) {
						iCode2 = str.charCodeAt(i);//now
						if(iCode2 != (iCode1+1)) {
							if(!(iCode2 == 48 && iCode1 == 57)) {
								bPlus = false;
							}
						}
						if(iCode2 != (iCode1-1)) {
							if(!(iCode2 == 57 && iCode1 == 48)) {
								bMinus = false;	
							}
						}
						iCode1 = iCode2;//before
					}
					if(bPlus || bMinus) 
						return false;
				}
			}
			return true;	
		}

		//檢核輸入值是否為相同之文數字
		function chkSameEngOrNum(obj) {
			var str = $('#'+obj).val();
			if(str != '') {
				var iCode1 = str.charCodeAt(0);
				
				if((iCode1 >=48 && iCode1 <= 57) || (iCode1 >= 65 && iCode1 <= 90) || (iCode1 >= 97 && iCode1 <= 122)) {
					var iCode2 = 0;
					var bSame  = true;
					for(var i=0;i<str.length;i++) {
						iCode2 = str.charCodeAt(i);
						if(iCode1 != iCode2) {
							bSame = false;
						}
					}
				}
				if(bSame) {
					return false;
				}
			}
			return true;
		}
		
		
		function username_check(){
			var ckeckflag = true;
			if($("#USERNAME").val() != $("#LOGINPIN").val() && $("#USERNAME").val() != $("#TRANSPIN").val() && $("#LOGINPIN").val() != $("#TRANSPIN").val()){
				$("#USERNAME_S1").removeClass("input-error-remarks");
				$("#USERNAME_S1").addClass("input-correct-remarks");
				$("#USERNAME_I1").removeClass("fa-times-circle");
				$("#USERNAME_I1").addClass("fa-check-circle");
			}else{
				$("#USERNAME_S1").addClass("input-error-remarks");
				$("#USERNAME_S1").removeClass("input-correct-remarks");
				$("#USERNAME_I1").addClass("fa-times-circle");
				$("#USERNAME_I1").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			if(checkUserName("USERNAME") && $("#USERNAME").val().length >= 6 && $("#USERNAME").val().length <= 16){
				$("#USERNAME_S2").removeClass("input-error-remarks");
				$("#USERNAME_S2").addClass("input-correct-remarks");
				$("#USERNAME_I2").removeClass("fa-times-circle");
				$("#USERNAME_I2").addClass("fa-check-circle");
			}else{
				$("#USERNAME_S2").addClass("input-error-remarks");
				$("#USERNAME_S2").removeClass("input-correct-remarks");
				$("#USERNAME_I2").addClass("fa-times-circle");
				$("#USERNAME_I2").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			if($("#USERNAME").val().indexOf("${ result_data.data.BIRTHDAY }") == -1){
// 			if($("#USERNAME").val().indexOf("800101") == -1){
				$("#USERNAME_S3").removeClass("input-error-remarks");
				$("#USERNAME_S3").addClass("input-correct-remarks");
				$("#USERNAME_I3").removeClass("fa-times-circle");
				$("#USERNAME_I3").addClass("fa-check-circle");
			}else{
				$("#USERNAME_S3").addClass("input-error-remarks");
				$("#USERNAME_S3").removeClass("input-correct-remarks");
				$("#USERNAME_I3").addClass("fa-times-circle");
				$("#USERNAME_I3").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			if(chkSerialNum("USERNAME") && chkSameEngOrNum("USERNAME")){
				$("#USERNAME_S4").removeClass("input-error-remarks");
				$("#USERNAME_S4").addClass("input-correct-remarks");
				$("#USERNAME_I4").removeClass("fa-times-circle");
				$("#USERNAME_I4").addClass("fa-check-circle");
			}else{
				$("#USERNAME_S4").addClass("input-error-remarks");
				$("#USERNAME_S4").removeClass("input-correct-remarks");
				$("#USERNAME_I4").addClass("fa-times-circle");
				$("#USERNAME_I4").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			return ckeckflag;
		}
		function username2_check(){
			var ckeckflag = true;
			if($("#USERNAME").val() == $("#USERNAME2").val()){
				$("#USERNAME2_S1").removeClass("input-error-remarks");
				$("#USERNAME2_S1").addClass("input-correct-remarks");
				$("#USERNAME2_I1").removeClass("fa-times-circle");
				$("#USERNAME2_I1").addClass("fa-check-circle");
			}else{
				$("#USERNAME2_S1").addClass("input-error-remarks");
				$("#USERNAME2_S1").removeClass("input-correct-remarks");
				$("#USERNAME2_I1").addClass("fa-times-circle");
				$("#USERNAME2_I1").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			return ckeckflag;
		}
		
		function loginpin_check(){
			var ckeckflag = true;
			if($("#USERNAME").val() != $("#LOGINPIN").val() && $("#USERNAME").val() != $("#TRANSPIN").val() && $("#LOGINPIN").val() != $("#TRANSPIN").val()){
				$("#LOGINPIN_S1").removeClass("input-error-remarks");
				$("#LOGINPIN_S1").addClass("input-correct-remarks");
				$("#LOGINPIN_I1").removeClass("fa-times-circle");
				$("#LOGINPIN_I1").addClass("fa-check-circle");
			}else{
				$("#LOGINPIN_S1").addClass("input-error-remarks");
				$("#LOGINPIN_S1").removeClass("input-correct-remarks");
				$("#LOGINPIN_I1").addClass("fa-times-circle");
				$("#LOGINPIN_I1").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			if(NewCheckpwd("LOGINPIN") && $("#LOGINPIN").val().length >= 6 && $("#LOGINPIN").val().length <= 8){
				$("#LOGINPIN_S2").removeClass("input-error-remarks");
				$("#LOGINPIN_S2").addClass("input-correct-remarks");
				$("#LOGINPIN_I2").removeClass("fa-times-circle");
				$("#LOGINPIN_I2").addClass("fa-check-circle");
			}else{
				$("#LOGINPIN_S2").addClass("input-error-remarks");
				$("#LOGINPIN_S2").removeClass("input-correct-remarks");
				$("#LOGINPIN_I2").addClass("fa-times-circle");
				$("#LOGINPIN_I2").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			if($("#LOGINPIN").val().indexOf("${ result_data.data.BIRTHDAY }") == -1){
// 			if($("#LOGINPIN").val().indexOf("800101") == -1){
				$("#LOGINPIN_S3").removeClass("input-error-remarks");
				$("#LOGINPIN_S3").addClass("input-correct-remarks");
				$("#LOGINPIN_I3").removeClass("fa-times-circle");
				$("#LOGINPIN_I3").addClass("fa-check-circle");
			}else{
				$("#LOGINPIN_S3").addClass("input-error-remarks");
				$("#LOGINPIN_S3").removeClass("input-correct-remarks");
				$("#LOGINPIN_I3").addClass("fa-times-circle");
				$("#LOGINPIN_I3").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			if(chkSerialNum("LOGINPIN") && chkSameEngOrNum("LOGINPIN")){
				$("#LOGINPIN_S4").removeClass("input-error-remarks");
				$("#LOGINPIN_S4").addClass("input-correct-remarks");
				$("#LOGINPIN_I4").removeClass("fa-times-circle");
				$("#LOGINPIN_I4").addClass("fa-check-circle");
			}else{
				$("#LOGINPIN_S4").addClass("input-error-remarks");
				$("#LOGINPIN_S4").removeClass("input-correct-remarks");
				$("#LOGINPIN_I4").addClass("fa-times-circle");
				$("#LOGINPIN_I4").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			return ckeckflag;
		}
		function loginpin2_check(){
			var ckeckflag = true;
			if($("#LOGINPIN").val() == $("#LOGINPIN2").val()){
				$("#LOGINPIN2_S1").removeClass("input-error-remarks");
				$("#LOGINPIN2_S1").addClass("input-correct-remarks");
				$("#LOGINPIN2_I1").removeClass("fa-times-circle");
				$("#LOGINPIN2_I1").addClass("fa-check-circle");
			}else{
				$("#LOGINPIN2_S1").addClass("input-error-remarks");
				$("#LOGINPIN2_S1").removeClass("input-correct-remarks");
				$("#LOGINPIN2_I1").addClass("fa-times-circle");
				$("#LOGINPIN2_I1").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			return ckeckflag;
		}
		
		function transpin_check(){
			var ckeckflag = true;
			if($("#USERNAME").val() != $("#LOGINPIN").val() && $("#USERNAME").val() != $("#TRANSPIN").val() && $("#LOGINPIN").val() != $("#TRANSPIN").val()){
				$("#TRANSPIN_S1").removeClass("input-error-remarks");
				$("#TRANSPIN_S1").addClass("input-correct-remarks");
				$("#TRANSPIN_I1").removeClass("fa-times-circle");
				$("#TRANSPIN_I1").addClass("fa-check-circle");
			}else{
				$("#TRANSPIN_S1").addClass("input-error-remarks");
				$("#TRANSPIN_S1").removeClass("input-correct-remarks");
				$("#TRANSPIN_I1").addClass("fa-times-circle");
				$("#TRANSPIN_I1").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			if(NewCheckpwd("TRANSPIN") && $("#TRANSPIN").val().length >= 6 && $("#TRANSPIN").val().length <= 8){
				$("#TRANSPIN_S2").removeClass("input-error-remarks");
				$("#TRANSPIN_S2").addClass("input-correct-remarks");
				$("#TRANSPIN_I2").removeClass("fa-times-circle");
				$("#TRANSPIN_I2").addClass("fa-check-circle");
			}else{
				$("#TRANSPIN_S2").addClass("input-error-remarks");
				$("#TRANSPIN_S2").removeClass("input-correct-remarks");
				$("#TRANSPIN_I2").addClass("fa-times-circle");
				$("#TRANSPIN_I2").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			if($("#TRANSPIN").val().indexOf("${ result_data.data.BIRTHDAY }") == -1){
// 			if($("#TRANSPIN").val().indexOf("800101") == -1){
				$("#TRANSPIN_S3").removeClass("input-error-remarks");
				$("#TRANSPIN_S3").addClass("input-correct-remarks");
				$("#TRANSPIN_I3").removeClass("fa-times-circle");
				$("#TRANSPIN_I3").addClass("fa-check-circle");
			}else{
				$("#TRANSPIN_S3").addClass("input-error-remarks");
				$("#TRANSPIN_S3").removeClass("input-correct-remarks");
				$("#TRANSPIN_I3").addClass("fa-times-circle");
				$("#TRANSPIN_I3").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			if(chkSerialNum("TRANSPIN") && chkSameEngOrNum("TRANSPIN")){
				$("#TRANSPIN_S4").removeClass("input-error-remarks");
				$("#TRANSPIN_S4").addClass("input-correct-remarks");
				$("#TRANSPIN_I4").removeClass("fa-times-circle");
				$("#TRANSPIN_I4").addClass("fa-check-circle");
			}else{
				$("#TRANSPIN_S4").addClass("input-error-remarks");
				$("#TRANSPIN_S4").removeClass("input-correct-remarks");
				$("#TRANSPIN_I4").addClass("fa-times-circle");
				$("#TRANSPIN_I4").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			return ckeckflag;
		}
		function transpin2_check(){
			var ckeckflag = true;
			if($("#TRANSPIN").val() == $("#TRANSPIN2").val()){
				$("#TRANSPIN2_S1").removeClass("input-error-remarks");
				$("#TRANSPIN2_S1").addClass("input-correct-remarks");
				$("#TRANSPIN2_I1").removeClass("fa-times-circle");
				$("#TRANSPIN2_I1").addClass("fa-check-circle");
			}else{
				$("#TRANSPIN2_S1").addClass("input-error-remarks");
				$("#TRANSPIN2_S1").removeClass("input-correct-remarks");
				$("#TRANSPIN2_I1").addClass("fa-times-circle");
				$("#TRANSPIN2_I1").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			return ckeckflag;
		}
		function all_check(){
			username_check();
			username2_check();
			transpin_check();
			transpin2_check();
			loginpin_check();
			loginpin2_check();
		}
    </script>
	<style>
		@media screen and (max-width:767px) {
			.ttb-button {
					width: 38%;
					left: 36px;
			}
			#CMBACK.ttb-button{
					border-color: transparent;
					box-shadow: none;
					color: #333;
					background-color: #fff;
			}
 

		}
	</style>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上開立數位存款帳戶     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1361" /></li>
		</ol>
	</nav>

	<div class="content row">
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
<!-- 					線上開立數位存款帳戶 -->
					<spring:message code="LB.D1361"/>
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <div id="step-bar">
                    <ul>
                        <li class="finished">注意事項與權益</li>
                        <li class="finished">開戶認證</li>
                        <li class="active">開戶資料</li>
                        <li class="">確認資料</li>
                        <li class="">完成申請</li>
                    </ul>
                </div>
                <form id="formId" method="post">
					<input type="hidden" name="ADOPID" value="N203">
				  	<input type="hidden" name="HLOGINPIN" id="HLOGINPIN" value="">
				  	<input type="hidden" name="HTRANSPIN" id="HTRANSPIN" value="">
				  	<input type="hidden" name="CARDAPPLY" id="CARDAPPLY" value="">
					<input type="hidden" name="LMTTYN" id="LMTTYN" value="">	
					<input type="hidden" name="back" id="back" value=>	
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
                        
							<div class="ttb-input-block">
	                            <div class="ttb-message">
	                                <p>開戶資料</p>
	                            </div>
	                            <div class="classification-block">
                                 	<p>網路銀行服務申請</p>
                                 	<ol class="description-list list-decimal" style="color:red">
                                 	<span>提醒您:</span>
          								<li style="color:red">本次開戶設定之網路銀行簽入密碼及交易密碼為「預設啟用」密碼，請先妥善保管。</li>
          								<li style="color:red">數位存款帳戶開立後，為保障您帳戶安全，請於30天內盡速登入網路銀行或行動銀行APP進行密碼變更。</li>
          								<li style="color:red">首次登入網路銀行或行動銀行APP需先輸入預設啟用密碼，系統會要求重新設定簽入密碼及交易密碼，爾後登入則請使用新設定之簽入密碼及交易密碼。</li>
         							</ol>
                                    <input type="hidden" name="TRFLAG" id="TRFLAG" value="Y">
                             	</div>
	                            <!-- 網路銀行帳號密碼設定 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											網路銀行帳號密碼設定
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
                                       		<span class="input-subtitle subtitle-color"><spring:message code="LB.Loginpage_User_name"/></span>
											<input type="text" id="USERNAME" name="USERNAME" placeholder="請輸入<spring:message code="LB.Loginpage_User_name"/>" value="${ result_data.data.USERNAME }" class="text-input validate[required]" onkeyup="all_check()">
										</div>
										<div class="ttb-input">
											<input type="text" id="USERNAME2" name="USERNAME2" placeholder="<spring:message code="LB.D1005"/>" value="${ result_data.data.USERNAME2 }" class="text-input validate[required]" onkeyup="all_check()">
	                                        <span class="input-error-remarks" id="USERNAME2_S1"><i class="fa fa-times-circle mr-1" id="USERNAME2_I1"></i>使用者名稱需相同</span>
                                        	<span class="input-error-remarks" id="USERNAME_S1"><i class="fa fa-times-circle mr-1" id="USERNAME_I1"></i>帳號、簽入密碼、交易密碼不得相同</span>
	                                        <span class="input-error-remarks" id="USERNAME_S2"><i class="fa fa-times-circle mr-1" id="USERNAME_I2"></i><spring:message code="LB.D1003"/></span>
	                                        <span class="input-error-remarks" id="USERNAME_S3"><i class="fa fa-times-circle mr-1" id="USERNAME_I3"></i>不能含有生日</span>
	                                        <span class="input-error-remarks" id="USERNAME_S4"><i class="fa fa-times-circle mr-1" id="USERNAME_I4"></i>不得設定為相同或升降冪的英文或數字，例如：11111、AAAAA、123456、654321、ABCDED、FEDCBA</span>	
										</div>
										<div class="ttb-input">
                                       		<span class="input-subtitle subtitle-color"><spring:message code="LB.X0210"/></span>
											<input type="password" class="text-input validate[required]"placeholder="請輸入簽入密碼"  id="LOGINPIN" name="LOGINPIN" value="" onkeyup="all_check()">
										</div>
										<div class="ttb-input">
											<input type="password" id="LOGINPIN2" name="LOGINPIN2" placeholder="<spring:message code="LB.D1009"/>" value="" class="text-input validate[required]" onkeyup="all_check()">
	                                        <span class="input-error-remarks" id="LOGINPIN2_S1"><i class="fa fa-times-circle mr-1" id="LOGINPIN2_I1"></i>簽入密碼需相同</span>
                                        	<span class="input-error-remarks" id="LOGINPIN_S1"><i class="fa fa-times-circle mr-1" id="LOGINPIN_I1"></i>帳號、簽入密碼、交易密碼不得相同</span>
	                                        <span class="input-error-remarks" id="LOGINPIN_S2"><i class="fa fa-times-circle mr-1" id="LOGINPIN_I2"></i><spring:message code="LB.X0212"/></span>
	                                        <span class="input-error-remarks" id="LOGINPIN_S3"><i class="fa fa-times-circle mr-1" id="LOGINPIN_I3"></i>不能含有生日</span>
	                                        <span class="input-error-remarks" id="LOGINPIN_S4"><i class="fa fa-times-circle mr-1" id="LOGINPIN_I4"></i>不得設定為相同或升降冪的英文或數字，例如：11111、AAAAA、123456、654321、ABCDED、FEDCBA</span>
	                                        
										</div>
										<div class="ttb-input">
                                       		<span class="input-subtitle subtitle-color"><spring:message code="LB.X0211"/></span>
											<input type="password" id="TRANSPIN" name="TRANSPIN" value="" placeholder="請輸入交易密碼" class="text-input validate[required]" onkeyup="all_check()">
										</div>
										<div class="ttb-input">
											<input type="password" id="TRANSPIN2" name="TRANSPIN2" value="" placeholder="<spring:message code="LB.D1012"/>" class="text-input validate[required]" onkeyup="all_check()">
	                                        <span class="input-error-remarks" id="TRANSPIN2_S1"><i class="fa fa-times-circle mr-1" id="TRANSPIN2_I1"></i>交易密碼需相同</span>
                                        	<span class="input-error-remarks" id="TRANSPIN_S1"><i class="fa fa-times-circle mr-1" id="TRANSPIN_I1"></i>帳號、簽入密碼、交易密碼不得相同</span>
	                                        <span class="input-error-remarks" id="TRANSPIN_S2"><i class="fa fa-times-circle mr-1" id="TRANSPIN_I2"></i><spring:message code="LB.X0212"/></span>
	                                        <span class="input-error-remarks" id="TRANSPIN_S3"><i class="fa fa-times-circle mr-1" id="TRANSPIN_I3"></i>不能含有生日</span>
	                                        <span class="input-error-remarks" id="TRANSPIN_S4"><i class="fa fa-times-circle mr-1" id="TRANSPIN_I4"></i>不得設定為相同或升降冪的英文或數字，例如：11111、AAAAA、123456、654321、ABCDED、FEDCBA</span>
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.D1425"/>
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
			                                <div class="ttb-input">
			                                    <label class="check-block" for="TRFLAG1">
													<spring:message code="LB.D0034_2"/>
			                                        <input type="checkbox" name="TRFLAG1" id="TRFLAG1" value="Y" checked disabled>
			                                        <span class="ttb-check"></span>
			                                    </label>
			                                </div>
										</div>
									</span>
								</div>
                           	</div>
							
	                        <!--button 區域 -->
							<!-- 重新輸入 -->
							<input type="button" name="CMBACK" id="CMBACK" value="<spring:message code="LB.X0318"/>" class="ttb-button btn-flat-gray">
                            <input id="CMSUBMIT" name="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.X0080"/>" />
                        <!--                     button 區域 -->
                        </div>  
                    </div>
                </form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>