<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 信用卡     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Credit_Card" /></li>
    <!-- 實體對帳單補寄    -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1638" /></li>
		</ol>
	</nav>

	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">

		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
		
		

		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2>
				<spring:message code="LB.W1638" /><!-- 實體對帳單補寄 -->
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" action="${__ctx}/CREDIT/APPLY/entity_bill_resend_confirm" method="post">
				<input type="hidden" id="TOKEN" name="TOKEN" value="${sessionScope.transfer_confirm_token}">
				<div class="main-content-block row">
					<div class="col-12 tab-content" id="nav-tabContent">
						<!-- 主頁內容  -->
						<div class="ttb-input-block">
							<div class="ttb-input-item row">
								<span class="input-title"> <label><h4><spring:message code="LB.W1639" /><!-- 消費期間 --></h4></label></span> 
								<span class="input-block">
									<div class="ttb-input">
											<select name="PRTYM" id="PRTYM" class="custom-select" style="width: 90px;">
												<c:forEach var="dataList" items="${entity_bill_resend.data.selectData}" >
													<option value="${dataList.value}">${dataList.text}</option>
												</c:forEach>
											</select>&nbsp;&nbsp;<spring:message code="LB.Month" /><!-- 月 -->
									</div>
								</span>
							</div>
					</div>
					<input type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />"/>
								<!-- 重新輸入 -->
					<input type="submit" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
								<!-- 確定 -->
				</div>
				</div>
					<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>
						<li><spring:message code="LB.Entity_Bill_Resend_P1_D1" /></li>
						<li><spring:message code="LB.Entity_Bill_Resend_P1_D2" /></li>
					</ol>
			</form>
		</section>
		</main>
	</div>	
	<%@ include file="../index/footer.jsp"%>
</body>
</html>