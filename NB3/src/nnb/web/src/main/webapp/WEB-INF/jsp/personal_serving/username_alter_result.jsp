<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
</head>
<body>
    <!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 使用者名稱變更     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Change_User_Name" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
	
		<%@ include file="../index/menu.jsp"%>
		<!-- 	主頁內容 -->
		<main class="col-12"> 
		
		<!-- 		主頁內容  -->

			<section id="main-content" class="container">
		
				<h2><spring:message code="LB.Change_User_Name" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>

				<div class="main-content-block row">
					<div class="col-12">
					
					
						<div class="ttb-input-block">			
                            <div class="ttb-message">
                                <span><spring:message code="LB.Change_successful" /></span>
                            </div>				
							<!-- 系統時間 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.System_time" />
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										${ username_alter_result.data.TIME }
									</div>
								</span>
							</div>
							<!-- 系統時間 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
								<h4><spring:message code="LB.New_user_name" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										${ username_alter_result.data.NEWUID }
									</div>
								</span>
							</div>
					</div>
					</div>
				</div>
<!-- 				</form> -->
			</section>
			<!-- 		main-content END -->
		</main>
	</div>	

	<%@ include file="../index/footer.jsp"%>
</body>
</html>