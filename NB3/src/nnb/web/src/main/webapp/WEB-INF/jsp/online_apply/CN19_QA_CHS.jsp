<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

	<script type="text/javascript">
		$(function(){
			// 幫 #qaContent 的 ul 子元素加上 .accordionPart
			// 接著再找出 li 中的第一個 div 子元素加上 .qa_title
			// 並幫其加上 hover 及 click 事件
			// 同時把兄弟元素加上 .qa_content 並隱藏起來
			$('#qaContent ul').addClass('accordionPart').find('li div:nth-child(1)').addClass('qa_title').hover(function(){
				$(this).addClass('qa_title_on');
			}, function(){
				$(this).removeClass('qa_title_on');
			}).click(function(){
				// 當點到標題時，若答案是隱藏時則顯示它，同時隱藏其它已經展開的項目
				// 反之則隱藏
				var $qa_content = $(this).next('div.qa_content');
				if(!$qa_content.is(':visible')){
					$('#qaContent ul li div.qa_content:visible').slideUp();
				}
				$qa_content.slideToggle();
			}).siblings().addClass('qa_content').hide();
		});
	</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Register" /></li>
    <!-- 隨護神盾     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0343" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<!-- 申請隨護神盾 -->
			<h2><spring:message code="LB.D1573"/></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="">
				<div class="main-content-block row">
					<div class="col-12">
<!-- 						<div class="CN19-1-header"> -->
<!-- 							<div class="logo"> -->
<%-- 								<img src="${__ctx}/img/logo-1.svg"> --%>
<!-- 							</div> -->
<!-- 							常用網址超連結 -->
<!-- 							<div class="text-right hyperlink"> -->
<!-- 							臺灣企銀首頁 -->
<%-- 								<a href="#" onClick="window.open('https://www.tbb.com.tw');" style="text-decoration: none"> <spring:message code="LB.TAIWAN_BUSINESS_BANK"/> </a> <strong><font color="#e65827">|</font></strong>  --%>
<!-- 								網路ATM -->
<%-- 								<a href="#" onClick="window.open('https://eatm.tbb.com.tw/');" style="text-decoration: none"> <spring:message code="LB.Web_ATM"/> </a><strong><font color="#e65827">|</font></strong> 	 --%>
<!-- 								意見信箱  -->
<%-- 								<a href="#" onClick="window.open('https://www.tbb.com.tw/q3.html?');" style="text-decoration: none"> <spring:message code="LB.Customer_service"/> </a> --%>
<!-- 							</div> -->
<!-- 						</div> -->
						<!-- 數位存款帳戶補申請晶片金融卡 -->
						<br><br>
						<div id="qaContent">
							<ul>
								<li>
									<div>Q1. 什么是随护神盾？</div>
									<div>随护神盾是结合智能型装置进行身分认证，客户透过输入密码确认交易，随时随地带着行动装置都能轻松完成交易。
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q2. 随护神盾有哪些功能？</div>
									<div>
										提供客户于行动银行使用下列服务：<br> 
										一、非约定转账<br> 
										二、台湾Pay行动支付<br>
										三、无卡提款功能<br> 
										四、缴纳学杂费及企业代收费用
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q3. 下载密码是做什么用的？</div>
									<div>绑定行动装置时，用于行动银行下载凭证时使用。</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q4. 随护密码是做什么用的？</div>
									<div>下载凭证时所设定的密码，用于行动银行透过随护神盾进行交易验证时使用。</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q5. 申请资格？</div>
									<div>已申请本行一般网络银行并开通行动银行，且有设定转出账号之客户。</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q6. 如何申请？</div>
									<div>
										一、 本行在线申请专区：持本人本行芯片金融卡至申请页面申请，并于48小时内至行动银行进行启用。(限自然人)<BR>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(一) 选择申请「随护神盾」<BR> 
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(二) 自行设定6~8位数「下载密码」<BR> 
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(三) 插入本行芯片金融卡并输入金融卡密码进行验证<BR>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(四)登入行动银行选择「随护神盾」、输入「下载密码」并设定「随护密码」，下载凭证成功后即完成启用程序<BR> 
										二、一般网络银行：登入一般网络银行，选择「申请/挂失服务」，点选「申请随护神盾」，搭配本人本行芯片金融卡验证申请，并于48小时内至行动银行进行启用。(限自然人)<BR>
										三、 临柜：亲携身分证明文件与原留印鉴，洽原网银开户行办理(注)，并于30天内至行动银行进行启用。
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q7. 如何终止随护神盾服务？</div>
									<div>
										一、  临柜：亲携身分证明文件与原留印鉴洽原网银开户行办理(注)。<BR> 
										二、客服人员：拨打客服专线0800-01-7171、02-2357-7171按3(手机请拨付费电话)，洽客服人员终止(限自然人)。
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q8. 更换或遗失手机时，应如何处理？</div>
									<div>
										更换手机时，透过旧手机设定更换设备下载密码，再以新的手机依程序下载即可。<BR>
										若手机遗失欲终止随护神盾服务时，可于原网银开户行(注)或洽客服人员(限自然人)进行终止。
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q9. 随护神盾连续错误之限制？</div>
									<div>
										为防止有心人士随意测试，「下载密码」连续错误5次时密码将失效，需重新申请随护神盾。「随护密码」连续错误5次时将自动终止服务。
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q10. 随护神盾下载密码超过时效后该如何处理？</div>
									<div>重新于本行在线申请专区(限自然人)、一般网络银行(限自然人)或原网银开户行办理(注)申请即可。</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q11. 终止随护神盾后是否可以重新申请？</div>
									<div>
										终止随护神盾服务后，可于本行在线申请专区(限自然人)、一般网络银行(限自然人)或原网银开户行重新申请(注)。
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q12. 忘记下载密码、随护密码或密码连续错误5次时该怎么办？</div>
									<div>
										一、 忘记下载密码时:可直接于本行线上申请专区(限自然人)、一般网路银行(限自然人)或原网银开户行重新申请(注)。 <BR>
										二、 忘记随护密码或密码连续错误5次时:<BR>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(一) 如凭证效期尚未逾有效期限，需终止随护神盾服务后再重新申请(详见Q7.如何终止随护神盾服务? )。 <BR>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(二) 如凭证已超过有效期限，可直接于本行线上申请专区(限自然人)、一般网路银行(限自然人)或网银开户行重新申请(注)<BR>
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q13. 每个人可以申请几个随护神盾？</div>
									<div>随护神盾以身分证字号或统一编号归户，每个人只能绑定一个行动装置。</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q14. 申请/更新随护神盾需要费用吗？</div>
									<div>									
										免收凭证年费，欢迎多加利用。
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q15. 随护神盾进行非约定帐户转账之交易有金额限制吗？</div>
									<div>
										一、 预设限额：每笔新台币5万元、每日新台币10万元、每月新台币20万元。<BR> 
										二、此限额与芯片金融卡及动态密码卡之转账交易合并计算。
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q16. 随护神盾有使用时间的限制吗？</div>
									<div>
										申请随护神盾，使用期限自下载之日起算一年。随护神盾到期前一个月内，系统自动以eMail提示展期讯息。客户须于期限内办理更新，逾有效期限导致更新不成功时，请重新申请随护神盾服务。
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q17. 我收到随护神盾凭证即将到期通知信，要如何更新随护神盾？</div>
									<div>
										请于凭证到期前一个月内登入本行行动银行，点选「随护神盾」并依指示办理凭证更新。
									</div>
								</li>
							</ul>
							注：个人户已申请全行收付者可至台湾企银任一分行办理，法人户限于开户行办理。
						</div>
					</div>
				</div>
			</form>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>