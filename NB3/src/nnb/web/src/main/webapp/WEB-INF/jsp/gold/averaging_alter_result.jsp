<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>	
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
		initFootable();
		if('${averaging_alter_result.data.DATE_06}' != ("")){
			$("#DATE_06").prop('checked',true);
		}
		if('${averaging_alter_result.data.DATE_16}' != ""){
			$("#DATE_16").attr('checked',true);
		}
		if('${averaging_alter_result.data.DATE_26}' != ""){
			$("#DATE_26").attr('checked',true);
		}
		
		$("#CMPRINT").click(function(){
			var params = {
					"jspTemplateName":"averaging_alter_result_print",
                   // 黃金定期定額變更 
					"jspTitle":"<spring:message code= "LB.W1565" />",
					"SVACN":"${averaging_alter_result.data.SVACN}",
					"ACN":"${averaging_alter_result.data.ACN}",
					"AMT_06_O":"${averaging_alter_result.data.AMT_06_O}",
					"AMT_16_O":"${averaging_alter_result.data.AMT_16_O}",
					"AMT_26_O":"${averaging_alter_result.data.AMT_26_O}",
					"FLAG_06_O":"${averaging_alter_result.data.FLAG_06_O}",
					"FLAG_16_O":"${averaging_alter_result.data.FLAG_16_O}",
					"FLAG_26_O":"${averaging_alter_result.data.FLAG_26_O}",
					"AMT_06_N":"${averaging_alter_result.data.AMT_06_N}",
					"AMT_16_N":"${averaging_alter_result.data.AMT_16_N}",
					"AMT_26_N":"${averaging_alter_result.data.AMT_26_N}",
					"PAYSTATUS_06":"${averaging_alter_result.data.PAYSTATUS_06}",
					"PAYSTATUS_16":"${averaging_alter_result.data.PAYSTATUS_16}",
					"PAYSTATUS_26":"${averaging_alter_result.data.PAYSTATUS_26}",
					"DATE_06":"${averaging_alter_result.data.DATE_06}",
					"DATE_16":"${averaging_alter_result.data.DATE_16}",
					"DATE_26":"${averaging_alter_result.data.DATE_26}"
			};
			openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
		});	
	}
	

</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %>

	<!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i
					class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 定期定額交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1552" /></li>
    <!-- 定期定額變更     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1564" /></li>
		</ol>

	</nav>
	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<!-- 黃金定期定額變更 -->
				<h2><spring:message code= "LB.W1565" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form id="formId" method="post" action="">
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
                             <div class="ttb-message">
								<!-- 變更成功 -->
                                <span><spring:message code= "LB.D0182" /></span>
                            </div>
							<c:set value="${averaging_alter_result.data}" var="dataList"></c:set>
                             <!--約定扣款帳號-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
									<!-- 約定扣款帳號 -->
                                        <h4><spring:message code= "LB.W1539" /></h4>
                                    </label></span>
                                <span class="input-block">
									<div class="ttb-input">
                                       <span>${dataList.SVACN}</span>
                                    </div>
                                </span>
                            </div>
							
                             <!--黃金存摺帳號-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
									<!-- 黃金存摺帳號 -->
                                        <h4><spring:message code= "LB.D1090" /></h4>
                                    </label></span>
                                <span class="input-block">
                                    <div class="ttb-input">
										<span>${dataList.ACN}</span>
                                    </div>
                                </span>
                            </div>
                              
							 <!--原每月投資日/金額-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                           <h4><spring:message code= "LB.W1581" /></h4>
                                    </label></span>
                                <span class="input-block">
									<div class="ttb-input">
										<!-- 日、新臺幣、元、扣款狀態 -->
										<span>
											06<spring:message code= "LB.Day" /> 
											<spring:message code="LB.NTD" />&nbsp;${dataList.AMT_06_O}&nbsp;<spring:message code= "LB.Dollar" /> &nbsp;
											<c:if test="${!dataList.FLAG_06_O.equals('')}">
												<spring:message code="${dataList.FLAG_06_O}" />
											</c:if>
										</span>
                                    </div>
									<div class="ttb-input">
										<!-- 日、新臺幣、元、扣款狀態 -->
										<span>
											16<spring:message code= "LB.Day" /> 
											<spring:message code="LB.NTD" />&nbsp;${dataList.AMT_16_O}&nbsp;<spring:message code= "LB.Dollar" /> &nbsp;
											<c:if test="${!dataList.FLAG_16_O.equals('')}">
												<spring:message code="${dataList.FLAG_16_O}" />
											</c:if>
										</span>
                                    </div>
									<div class="ttb-input">
										<!-- 日、新臺幣、元、扣款狀態 -->
										<span>
											26<spring:message code= "LB.Day" /> 
											<spring:message code="LB.NTD" />&nbsp;${dataList.AMT_26_O}&nbsp;<spring:message code= "LB.Dollar" /> &nbsp;
											<c:if test="${!dataList.FLAG_26_O.equals('')}">
												<spring:message code="${dataList.FLAG_26_O}" />
											</c:if>
										</span>
                                    </div>
                                </span>
                            </div>
							
							 <!--變更後每月投資日/金額-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code= "LB.W1582" /></h4>
                                    </label></span>
                                <span class="input-block">
									<div class="ttb-input">
										<label class="check-block" for="DATE_06"><input value="1" name="DATE_06" id="DATE_06" type="checkbox" disabled>
											<span class="ttb-check"></span>
											<!-- 日、新臺幣 -->
											<span>06<spring:message code= "LB.Day" /> <spring:message code="LB.NTD" />&nbsp;
												<!-- 元、扣款狀態 -->
												<c:if test = "${empty dataList.DATE_06}">${dataList.AMT_06_O}&nbsp;<spring:message code= "LB.Dollar" /> 
													<c:if test="${!dataList.FLAG_06_O.equals('')}">
														&nbsp;<spring:message code="${dataList.FLAG_06_O}" />
													</c:if>
												</c:if>
												<!-- 元、扣款狀態 -->
												<c:if test = "${not empty dataList.DATE_06}">${dataList.AMT_06_N}&nbsp;<spring:message code= "LB.Dollar" /> 
													<c:if test="${!dataList.PAYSTATUS_06.equals('')}">
													&nbsp;<spring:message code="${dataList.PAYSTATUS_06}" />
													</c:if>
												</c:if>
											</span>
										</label>
									</div>
									
									<div class="ttb-input">
										<label class="check-block" for="DATE_16"><input value="1" name="DATE_16" id="DATE_16" type="checkbox" disabled>
											<span class="ttb-check"></span>
											<!-- 日 -->
											<span>16<spring:message code= "LB.Day" /> <spring:message code="LB.NTD" />&nbsp;
												<!-- 元、扣款狀態 -->
												<c:if test = "${empty dataList.DATE_16}">${dataList.AMT_16_O}&nbsp;<spring:message code= "LB.Dollar" /> 
													<c:if test="${!dataList.FLAG_16_O.equals('')}">
														&nbsp;<spring:message code="${dataList.FLAG_16_O}" />
													</c:if>
												</c:if>
												<!-- 元、扣款狀態 -->
												<c:if test = "${not empty dataList.DATE_16}">${dataList.AMT_16_N}&nbsp;<spring:message code= "LB.Dollar" /> 
													<c:if test="${!dataList.PAYSTATUS_16.equals('')}">
														&nbsp;<spring:message code="${dataList.PAYSTATUS_16}" />
													</c:if>
												</c:if>
											</span>
										</label>
									</div>
									
									<div class="ttb-input">
										<label class="check-block" for="DATE_26"><input value="1" name="DATE_26" id="DATE_26" type="checkbox" disabled>
											<span class="ttb-check"></span>
											<!-- 日 -->
											<span>26<spring:message code= "LB.Day" /> <spring:message code="LB.NTD" />&nbsp;
												<!-- 元、扣款狀態 -->
												<c:if test = "${empty dataList.DATE_26}">${dataList.AMT_26_O}&nbsp;<spring:message code= "LB.Dollar" /> 
													<c:if test="${!dataList.FLAG_26_O.equals('')}">
														&nbsp;<spring:message code="${dataList.FLAG_26_O}" />
													</c:if>
												</c:if>
												<!-- 元、扣款狀態 -->
												<c:if test = "${not empty dataList.DATE_26}">${dataList.AMT_26_N}&nbsp;<spring:message code= "LB.Dollar" /> 
													<c:if test="${!dataList.PAYSTATUS_26.equals('')}">
														&nbsp;<spring:message code="${dataList.PAYSTATUS_26}" />
													</c:if>
												</c:if>
											</span>
										</label>
									</div>
                                </span>
                            </div>
                        </div>
                        <input class="ttb-button btn-flat-orange" type="button" name="CMPRINT" id="CMPRINT" value="<spring:message code="LB.Print" />"/>
                    </div>
                </div>
				</form>
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>
					<!-- 投資日上午凌晨0〜10點(遇假日則自原投資日起至次一營業日上午凌晨0〜10點)，因系統進行批次扣款作業，暫時停止變更服務。 -->
					<li><span><spring:message code="LB.Averaging_Alter_P3_D1" /></span></li>
	                <!-- 每次投資金額至少為新臺幣3,000元，且得以新臺幣1,000元之整倍數增加。  -->
					<li><span><spring:message code="LB.Averaging_Alter_P3_D2" /></span></li>
	                <!-- 網路銀行定期定額每次扣款優惠手續費為新臺幣50元，連同投資金額一併扣繳。 -->
					<li><span><spring:message code="LB.Averaging_Alter_P3_D3" /></span></li>
					<!-- 黃金存摺不支付利息，黃金價格有漲有跌，投資時可能產生收益或損失，請慎選買賣時機，並承擔風險。 -->
					<li><span><spring:message code="LB.Averaging_Alter_P3_D4" /></span></li>
					<!-- 電子簽章:為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章(載具i-key)拔除並登出系統。 -->
					<li><span><spring:message code="LB.Averaging_Alter_P3_D5" /></span></li>		 
	            </ol>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>