<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ include file="../__import_js.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
	$(document).ready(function() {
		window.print();
	});
</script>
</head>
<body class="bodymargin watermark"
	style="-webkit-print-color-adjust: exact">
	<br />
	<br />
	<div style="text-align: center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br />
	<br />
	<br />
	<div style="text-align: center">
		<font style="font-weight: bold; font-size: 1.2em">${jspTitle}</font>
	</div>
	<br />
	<label><spring:message code="LB.System_time" />：${SYSTIME}</label>
	<br />
	<br />
	<c:if test="${SUCCEEDCOUNT !='0'}">
		<table class="print">
			<tr>
				<td style="text-align: center"><spring:message code="LB.Status" />
				</td>
				<td style="text-align: center"><spring:message
						code="LB.Card_kind" /></td>
				<td style="text-align: center"><spring:message
						code="LB.Card_number" /></td>
			</tr>
			<c:forEach var="suList" items="${dataListMap[0]}">
			<tr>
				<td style="text-align: center; font-weight: bold">
					<c:if test="${suList.STATUS =='掛失成功'}">
						<spring:message code="LB.Report_successful" />
					</c:if>
				</td>
				<td style="text-align: center">${suList.TYPENAME}</td>
				<td style="text-align: center">${suList.CARDNUM_COVER}</td>
			</tr>
			</c:forEach>
		</table>
		<br/>
		<div class="text-left">
			<spring:message code="LB.Description_of_page" />
			：
			<!-- 說明 -->
			<ol class="list-decimal text-left">
				<li>
					<spring:message code="LB.X1636" /></li>
			</ol>
		</div>
	</c:if>
	<c:if test="${FAILEDCOUNT !='0'}">
		<table class="print">
			<tr>
				<td style="text-align: center"><spring:message code="LB.Status" />
				</td>
				<td style="text-align: center"><spring:message
						code="LB.Card_kind" /></td>
				<td style="text-align: center"><spring:message
						code="LB.Card_number" /></td>
			</tr>
			<c:forEach var="flList" items="${dataListMap[1]}">
			<tr>
				<td style="text-align: center; font-weight: bold">
					<c:if test="${flList.STATUS =='掛失失敗'}">
						<spring:message code="LB.Report_loss_fail" /><!-- 掛失失敗 -->
					</c:if>
				</td>
				<td style="text-align: center">${flList.TYPENAME}</td>
				<td style="text-align: center">${flList.CARDNUM_COVER}</td>
			</tr>
			</c:forEach>
		</table>
		<br/>
		<div class="text-left">
			<spring:message code="LB.Description_of_page" />
			：
			<!-- 說明 -->
			<ol class="list-decimal text-left">
				<li><spring:message code="LB.Loss_P3_D2" /> <!-- 系統忙碌中請稍後再試，或洽本行客服02-2357-7171、0800-01-7171。 --></li>
			</ol>
		</div>
	</c:if>
	<br />
</body>
</html>