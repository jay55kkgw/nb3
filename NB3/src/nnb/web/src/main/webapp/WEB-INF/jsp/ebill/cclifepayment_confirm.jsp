<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/component/util/cardReaderMethod.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後0.1秒開始遮罩
		setTimeout("initBlockUI()", 100);
		// 初始化驗證碼
		setTimeout("initKapImg()", 200);
		// 生成驗證碼
		setTimeout("newKapImg()", 300);
		// 過0.5秒解遮罩
		setTimeout("unBlockUI(initBlockId)", 800);
		goOn()
	});
	// 確認鍵 click
	function goOn() {
		$("#CMSUBMIT").click(function (e) {
			if(checkcapCode()){
				if (!$('#formId').validationEngine('validate')) {
					e = e || window.event; // for IE
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					initBlockUI(); //遮罩
					$("#formId").submit();
				}
			}
		});
	}
	/**************************************/
	/*          	驗證碼驗證   		          */
	/**************************************/
	// 刷新驗證碼
	function refreshCapCode() {
		console.log("refreshCapCode...");

		// 驗證碼
		$('input[name="capCode"]').val('');
		
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
	}
	
	// 刷新輸入欄位
	function changeCode() {
		console.log("changeCode...");
		// 清空輸入欄位
		$('input[name="capCode"]').val('');
		// 刷新驗證碼
		refreshCapCode();
	}
	
	// 初始化驗證碼
	function initKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
	}
	
	// 生成驗證碼
	function newKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').click(function() {
			refreshCapCode();
		});
	}
	
	
	function checkcapCode(){
		console.log("CardReader...");
		var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
		// 驗證碼驗證
		var capData = $("#formId").serializeArray();
		var capResult = fstop.getServerDataEx(capUri, capData, false);
		console.log("chaCode_valid: " + JSON.stringify(capResult) );
		// 驗證結果
		if (capResult.result) {
			return true;
// 			alert("驗證成功");
		} else {
			// 失敗重新產生驗證碼
	   		errorBlock(
				null,
				null,
				['<spring:message code= "LB.X1082" />'], 
				'<spring:message code= "LB.Confirm" />', 
				null
			);
			changeCode();
			return false;
		}
	}
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	<!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<!-- 活存帳戶繳信用卡費 -->
			<li class="ttb-breadcrumb-item">
				<a href="#">
					<spring:message code="LB.X2427" />
				</a>
			</li>
			<!-- 活存帳戶繳信用卡費 -->
			<li class="ttb-breadcrumb-item active" aria-current="page">
				<spring:message code="LB.X2427" />
			</li>
		</ol>
	</nav>
	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!--活存帳戶繳信用卡費 -->
				<h2>
<!-- 					活存帳戶繳信用卡費 -->
					<spring:message code="LB.X2428" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form autocomplete="off" method="post" id="formId" action="${__ctx}/EBILL/PAY/cclifepayment_result">
					 <input type='hidden' id="accountid" name="accountid" value=' ${cclifepayment_confirm.data.accountid }'> 
					 <input type="hidden" id="BankID" name="BankID" value=' ${cclifepayment_confirm.data.BankID }' />
					 <input type='hidden' id="BANKNAME" name="BANKNAME" value=' ${cclifepayment_confirm.data.BANKNAME }'>
					 <input type='hidden' id="CARDNUM" name="CARDNUM" value=' ${cclifepayment_confirm.data.CARDNUM }'>
					 <input type='hidden' id="trin_acn" name="trin_acn" value=' ${cclifepayment_confirm.data.trin_acn }'>
					 <input type="hidden" id="CustEmail" name="CustEmail" value=' ${cclifepayment_confirm.data.CustEmail }' />
					 <input type="hidden" id="FEE" name="FEE" value=' ${cclifepayment_confirm.data.FEE }' />
					 <input type="hidden" id="ipayamt" name="ipayamt" value=' ${cclifepayment_confirm.data.ipayamt }' />
					 <input type="hidden" id="AMOUNT" name="AMOUNT" value=' ${cclifepayment_confirm.data.ipayamt }' />
					 <input type='hidden' id="LPAYAMT" name="LPAYAMT" value=' ${cclifepayment_confirm.data.LPAYAMT }'>
					 <input type='hidden' id="PAYAMT" name="PAYAMT" value=' ${cclifepayment_confirm.data.PAYAMT }'>
					 <input type="hidden" name="SessionId" value="${cclifepayment_confirm.data.SessionId}" />
					 <input type="hidden" name="CUSIDN" value="${cclifepayment_confirm.data.CUSIDN}" />
					 <input type="hidden" name="TrnsCode" value="${cclifepayment_confirm.data.TrnsCode}" />
					 <input type="hidden" name="TXTOKEN" value="${cclifepayment_confirm.data.TXTOKEN}" />
					<!-- 表單顯示區 -->
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
									<!--  銷帳編號: -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.W0407" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
                                            <p>
                                               ${cclifepayment_confirm.data.CARDNUM }
                                            </p>
                                        </div>
									</span>
								</div>
									<!-- 本期應繳金額 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.This_period" /><spring:message code="LB.Repayment_of_every_month" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
                                            <p>
                                             NTD ${cclifepayment_confirm.data.PAYAMT } <spring:message code="LB.Dollar" />
                                            </p>
                                        </div>
									</span>
								</div>
								<!-- 最低應繳金額:-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.X2432" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
                                            <p>
                                             NTD ${cclifepayment_confirm.data.LPAYAMT} <spring:message code="LB.Dollar" />
                                            </p>
                                        </div>
									</span>
								</div>
								<!-- 身分證字號/統編: -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label><spring:message code="LB.Id_no" /></label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
										 	${cclifepayment_confirm.data.CUSIDN }
										</p>
									</div>
								</span>
							</div>
								<!-- 轉出銀行:-->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label><spring:message code="LB.X2431" /> </label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
										 	${cclifepayment_confirm.data.BANKNAME }
										</p>
									</div>
								</span>
							</div>
								<!-- 轉出帳號: -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label><spring:message code="LB.Payers_account_no" /></label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
										 	${cclifepayment_confirm.data.trin_acn }
										</p>
									</div>
								</span>
							</div>
							<!-- 繳款金額:: -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label><spring:message code="LB.Payment_amount" /></label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
										 	${cclifepayment_confirm.data.ipayamt }
										</p>
									</div>
								</span>
							</div>
							<!-- 手續費: -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label><spring:message code="LB.D0507" /></label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
										 	${cclifepayment_confirm.data.FEE }
										</p>
									</div>
								</span>
							</div>
							<!-- 當轉帳交易成功，發送通知至您的信箱 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label><spring:message code="LB.X2433" /></label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
										 	${cclifepayment_confirm.data.CustEmail }
										</p>
									</div>
								</span>
							</div>
							<!--圖形驗證碼-->							
							<div class="ttb-input-item row" id="chaBlock">
								<!-- 驗證碼 -->
								<span class="input-title">
									<label>
										<h4><spring:message code= "LB.D1581" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
									<spring:message code="LB.Captcha" var="labelCapCode" />
									<input id="capCode" name="capCode" type="text"
										class="ttb-input text-input input-width-125" maxlength="6" autocomplete="off">
									<img name="kaptchaImage" class="verification-img" src="" />
									<input type="button" name="reshow" class="ttb-sm-btn btn-flat-orange"
										onclick="changeCode()" value="<spring:message code="LB.Regeneration" />" />
									</div>
								</span>
							</div>
							</div>
							<!-- button -->
							<!--重新輸入 -->
							<spring:message code="LB.Re_enter" var="cmRest" />
							<input type="button" name="CMRESET" id="CMRESET" value="${cmRest}" class="ttb-button btn-flat-gray">
							<!-- 確定 -->
							<spring:message code="LB.Confirm" var="cmSubmit" />
							<input type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" class="ttb-button btn-flat-orange">
							<!--buttonEND -->
						</div>
					</div>
					<ol class="description-list">
						<p><spring:message code="LB.Description_of_page" /></p><!-- 說明 -->
						<li><spring:message code="LB.X2483"/></li>
					</ol>
				</form>
			</section>
			<!-- main-content END -->
		</main>
	</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>