<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
  <title>${jspTitle}</title>
  <script type="text/javascript">
    $(document).ready(function () {
      window.print();
    });
  </script>
</head>

<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
  <br /><br />
  <div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif" /></div>
  <br /><br />
  <div style="text-align:center">
    <font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
  </div>
  <br />
  <div style="text-align:center">
    ${MsgName}
  </div>
  <br />
  <table class="print">
      <tr>
        <td style="width:10em"><spring:message code="LB.Trading_time" /></td>
        <td >${CMQTIME}</td>
      </tr>
      <tr>
        <td ><spring:message code="LB.Id_no" /></td>
        <td >${hideid_CUSIDN}</td>
      </tr>
      <tr>
        <td ><spring:message code="LB.W0944" /></td>
        <td >${hideid_CDNO}</td>
      </tr>
      <tr>
        <td><spring:message code="LB.W0025" /></td>
        <td >${FUNDLNAME}</td>
      </tr>
      <tr>
        <td><spring:message code="LB.D0973" /></td>
        <td >${str_AC202}</td>
      </tr>      
      <tr>
        <td><spring:message code="LB.W0026" /></td>
        <td >
        		<!-- 幣別 -->
        ${ADCCYNAME }
        <!--金額 -->
        ${FUNDAMT }
        </td>      
      </tr> 
      <tr>
        <td><spring:message code="LB.W0027" /></td>
        <td >
         ${display_ACUCOUNT }
        </td>
      </tr>  
      <tr>
        <td><spring:message code="LB.W0921" /><spring:message code="LB.Deposit_amount_1" /></td>
        <td >
          ${display_NAMT }
       	</td>
      </tr>       
      <tr>
        <td><spring:message code="LB.W0915" /></td>
        <td >
          ${display_AMT }
        </td>
      </tr>       
      <tr>
        <td><spring:message code="LB.W1195" /><spring:message code="LB.W1196" /></td>
        <td >
        ${RTNC }
        ${display_RTNRATE }%
        </td>
      </tr>
      <tr>
        <td><spring:message code="LB.W1198" /></td>
        <td > ${display_REFVALUE1 }</td>
      </tr>       
      <tr>
        <td><spring:message code="LB.W0030" /></td>
        <td >
        ${display_FXRATE }
       </td>
      </tr> 
      <tr>
        <td><spring:message code="LB.W1206" /></td>
        <td >
               <c:if test="${NOOPT == ''}">
         	<spring:message code="LB.D0324" />
        </c:if>
            <c:if test="${NOOPT != ''}">
         	 <spring:message code="LB.D0995" />
        </c:if>
        </td>
      </tr> 
   <c:if test="${NOOPT == ''}">
                             
      <tr>
        <td ><spring:message code="LB.W1210" />(<spring:message code="LB.W1211" />)</td>
        <td >+${NSTOPPROF } %</td>
      </tr>
      <tr>
        <td ><spring:message code="LB.W1210" />(<spring:message code="LB.X0419" />)</td>
        <td >-${NSTOPLOSS }%</td>
      </tr>
             </c:if>    
      
    </table>    

  <br>
  <br>
  <!--   說明 -->
  <div class="text-left">
  </div>
</body>

</html>