<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
<script type="text/javascript">
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});

		// 畫面初始化
		function init() {
			$("#CMSUBMIT").click(function (e) {
				initBlockUI();
				$("#formId").submit();
			});
		}
		// 勾選條款
		function checkReadFlag() {
			if ($("#ReadFlag").prop('checked')) {
				$("#CMSUBMIT").prop('disabled', false);
				$("#CMSUBMIT").addClass("btn-flat-orange");
			} else {
				$("#CMSUBMIT").prop('disabled', true);
				$("#CMSUBMIT").removeClass("btn-flat-orange");
			}
		}
		function openMenu() {
			var main = document.getElementById("main");
			window.open('${__ctx}/public/OpenAccount.pdf');
		}
	</script>
<style>
.ttb-result-list {
	margin: 0;
}

.ttb-result-list ul {
	margin-bottom: 30px;
}

.ttb-result-list ul li {
	margin: 0;
}

.ttb-result-list ul li span {
	display: inline-block;
	line-height: 24px;
	vertical-align: top;
}

.ttb-result-list ul li span:nth-child(1) {
	width: 30px;
}

.ttb-result-list ul li span:nth-child(2) {
	width: calc(100% - 30px);
}

.ttb-button:disabled {
	background: #ddd;
}

.radio-block, .check-block {
	display: inline-block !important;
}

@media screen and (max-width:767px) {
	.ttb-button {
		width: 38%;
		left: 36px;
	}
	#CMBACK.ttb-button {
		border-color: transparent;
		box-shadow: none;
		color: #333;
		background-color: #fff;
	}
}
</style>
</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	<!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<!-- 數位存款帳戶第三類升級為第一類 -->
			<li class="ttb-breadcrumb-item active" aria-current="page">數位存款帳戶第三類升級為第一類</li>
		</ol>
	</nav>
	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 主頁內容  -->
		<main class="col-12">
		<section id="main-content" class="container">
			<h2>
				<!-- 數位存款帳戶第三類升級為第一類 -->
				數位存款帳戶第三類升級為第一類
			</h2>
			<div id="step-bar">
				<ul>
					<li class="active">注意事項</li>
					<li class="">交易驗證</li>
					<li class="">完成申請</li>
				</ul>
			</div>
			<form method="post" id="formId" name="formId" action="${__ctx}/ONLINE/SERVING/upgrade_digital_acc_p2">
				<input type="hidden" id="DIGACN" name="DIGACN" value="${digital_acc.DIGACN}"> 
				<input type="hidden" id="CUSIDN" name="CUSIDN" value="${digital_acc.CUSIDN}">
				<!-- 表單顯示區  -->
				<div class="main-content-block row">
					<div class="col-12 terms-block">
						<div class="ttb-input-block">
							<div class="ttb-message">
								<p>數位存款帳戶升級注意事項</p>
							</div>
							<div class="text-left">
								<ul>
									<li><%@ include file="../term/N206.jsp"%></li>
								</ul>
								<span class="input-block text-center">
									<div class="ttb-input">
										<label class="check-block" for="ReadFlag"> 
											<input type="checkbox" name="ReadFlag" id="ReadFlag" onclick="checkReadFlag()"> 
											<b>本人於事前詳閱<font class="high-light">全部條款，充分瞭解且同意其內容。</font></b> <span class="ttb-check"></span>
										</label>
									</div>
								</span>
							</div>
						</div>
						<!-- 按鈕 -->
						<input type="button" onclick="window.close();" value="<spring:message code="LB.D0041" />" class="ttb-button btn-flat-gray" /> 
						<input type="button" id="CMSUBMIT" value="<spring:message code="LB.D0097" />" class="ttb-button" disabled />
					</div>
				</div>
			</form>
		</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>