<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>   
		<script type="text/javascript">
			$(document).ready(function(){
				// HTML載入完成後開始遮罩
				setTimeout("initBlockUI()",10);
				// 開始查詢資料並完成畫面
				setTimeout("init()",20);
				// 解遮罩
				setTimeout("unBlockUI(initBlockId)",500);	
			});
			function init(){
				// DataTable
				initDataTable();				
				//上一頁按鈕
				$("#previous").click(function() {
					initBlockUI();
					fstop.getPage('${pageContext.request.contextPath}'+'/NT/ACCT/checking_insufficient','', '');
				});
				
				$("#printbtn").click(function(){
					var params = {
							"jspTemplateName":"checking_insufficient_print",
							"jspTitle":"<spring:message code='LB.Insufficient_Balance_Of_Check_Deposits_Detail' />",
							"CMQTIME":"${checking_insufficient.data.CMQTIME}",
							"ACN":"${checking_insufficient.data.ACN}",
							"SNTCNT":"${checking_insufficient.data.SNTCNT}",
							"SNTAMT":"${checking_insufficient.data.SNTAMT}",
							"TOTBAL":"${checking_insufficient.data.TOTBAL}",
							"AVLBAL":"${checking_insufficient.data.AVLBAL}"
						};
						openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
					});							
			}
			
				//選項
			 	function formReset() {
// 			 		initBlockUI();
	 				if ($('#actionBar').val()=="excel"){
						$("#downloadType").val("OLDEXCEL");
						$("#templatePath").val("/downloadTemplate/checking_insufficient.xls");
			 		}else if ($('#actionBar').val()=="txt"){
						$("#downloadType").val("TXT");
						$("#templatePath").val("/downloadTemplate/checking_insufficient.txt");
			 		}else if ($('#actionBar').val()=="oldtxt"){
						$("#downloadType").val("OLDTXT");
						$("#templatePath").val("/downloadTemplate/checking_insufficientOLD.txt");
			 		}
// 	 	    		ajaxDownload("${__ctx}/ajaxDownload","formId","finishAjaxDownload()");
	 				$("#formId").attr("target", "");
		            $("#formId").submit();
		            $('#actionBar').val("");
				}
// 				function finishAjaxDownload(){
// 					$("#actionBar").val("");
// 					unBlockUI(initBlockId);
// 				}
			 	function processQuery(){
	 				$("#reRec").attr("action","${__ctx}/NT/ACCT/checking_insufficient_details_result");
	 	  			$("#reRec").submit();
				}
				
		</script>
	
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 支存當日不足扣票據明細     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Insufficient_Balance_Of_Check_Deposits_Detail" /></li>
		</ol>
	</nav>



		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 --> 
 		<div class="content row">
 		
 		   <%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->

 		
 		<main class="col-12">	

		<!-- 		主頁內容  -->

			<section id="main-content" class="container">
		
				<h2><spring:message code="LB.Insufficient_Balance_Of_Check_Deposits_Detail" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 						下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
<!-- 						下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
<!-- 						下載為舊txt檔 -->
						<option value="oldtxt"><spring:message code="LB.Download_oldversion_txt_file" /></option>
					</select>
				</div>
				<!-- 				繼續查詢 -->
				<c:if test="${checking_insufficient.data.TOPMSG == 'OKOV'}">
					 <div class="MessageBar"><spring:message code= "LB.X0076" />
	       			 <input id="CMCONTINUEQ" type="button"class="ttb-sm-btn btn-flat-orange" value="<spring:message code= "LB.X0151" />" name="CMCONTINUEQ" onClick="processQuery()">
	       			 </div>
       			</c:if>
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<ul class="ttb-result-list">
								<!-- 查詢時間 -->
							<li>
								<h3>
									<spring:message code="LB.Inquiry_time" /> ：	
								</h3>
								<p>
									${checking_insufficient.data.CMQTIME}
								</p>
							</li>
								<!-- 支票存款帳號 -->
							<li>
								<h3>
									<spring:message code="LB.Check_deposit_account" /> ：
								</h3>
								<p>
									${checking_insufficient.data.ACN}
								</p>
							</li>
							<!--不足扣票據總張數 -->
							<li>
								<h3>
									<spring:message code="LB.Total_number_of_insufficient_funds_items" /> ：
								</h3>
								<p>
									${checking_insufficient.data.SNTCNT}							
								</p>
							</li>
								<!--不足扣票據總金額 -->
							<li>
								<h3>
									<spring:message code="LB.Tota_dollar_amount_of_insufficient_funds" /> ：
								</h3>
								<p>
									${checking_insufficient.data.SNTAMT}							
								</p>
							</li>
							<!-- 帳上餘額 -->
							<li>
								<h3>
									<spring:message code="LB.Account_balance" /> ：
								</h3>
								<p>
									${checking_insufficient.data.TOTBAL}
								</p>
							</li>
							
							<!-- 可用餘額 -->
							<li>
								<h3>
									<spring:message code="LB.Available_balance" /> ：
								</h3>
								<p>
									${checking_insufficient.data.AVLBAL}
								</p>
							</li>
						</ul>
						<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
							<thead>
								<tr>
									<th><spring:message code="LB.Date_of_banknote_exchange" /></th>
									<th><spring:message code="LB.Checking_account" /></th>
									<th><spring:message code="LB.Checking_amount" /></th>
								</tr>
							</thead>
							
							<c:if test="${empty checking_insufficient.data.REC}"> 
							<tbody>
							<tr>
							<td></td>
							<td></td>
							<td></td>
							</tr>
							</tbody>
							</c:if>
								<c:if test="${not empty checking_insufficient.data.REC}"> 
								<tbody>
								<c:forEach var="dataList" items="${ checking_insufficient.data.REC }">
									<tr data-expanded="true">
						                <td class="text-center">${dataList.CHKDATE }</td>
						                <td class="text-center">${dataList.CHKNUM }</td>
						                <td class="text-right">${dataList.AMTACC }</td>
						            </tr>
								</c:forEach>
								</tbody>
								</c:if>
							
						</table>
						<input id="previous" name="previous" type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />" />	
						<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
					</div>
				</div>
				<div class="text-left">
					<ol class="list-decimal text-left">
						<p><spring:message code="LB.Description_of_page" /></p>
						<li><spring:message code="LB.Checking_insufficient_P2_D1" /></li>
					</ol>
				</div>
				<form method="post" id="reRec">
					<input type="hidden" id="ACN" name="ACN" value="${checking_insufficient.data.ACN}">
					<input type="hidden" id="USERDATA_X50" name="USERDATA_X50" value="${checking_insufficient.data.USERDATA_X50}">
				</form>
					
				<form method="post" id="formId" action="${__ctx}/download">
					<!-- 下載用 -->
					<input type="hidden" name="downloadFileName" value="LB.Insufficient_Balance_Of_Check_Deposits_Detail"/>
					<input type="hidden" name="downloadType" id="downloadType"/> 					
					<input type="hidden" name="templatePath" id="templatePath"/>
					<input type="hidden" name="CMQTIME" value="${checking_insufficient.data.CMQTIME}"/>
					<input type="hidden" name="ACN" value="${checking_insufficient.data.ACN}"/>
					<input type="hidden" name="SNTCNT" value="${checking_insufficient.data.SNTCNT}"/>
					<input type="hidden" name="SNTAMT" value="${checking_insufficient.data.SNTAMT}"/>
					<input type="hidden" name="TOTBAL" value="${checking_insufficient.data.TOTBAL}"/>
					<input type="hidden" name="AVLBAL" value="${checking_insufficient.data.AVLBAL}"/>
					<!-- EXCEL下載用 -->
					<input type="hidden" name="headerRightEnd" value="4"/>
					<input type="hidden" name="headerBottomEnd" value="9"/>
					<input type="hidden" name="rowStartIndex" value="10"/>
					<input type="hidden" name="rowRightEnd" value="2"/>
					<!-- 					上到下長度 -->
					<input type="hidden" name="txtHeaderBottomEnd" value="12"/> 					
					<input type="hidden" name="txtHasRowData" value="true"/>
					<input type="hidden" name="txtHasFooter" value="false"/>
				</form>
				
			</section>
			<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->


	<%@ include file="../index/footer.jsp"%>
</body>
</html>