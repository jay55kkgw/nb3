<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>	
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
		initFootable();
		
		$("#CMPRINT").click(function(){
			var params = {
					"jspTemplateName":"averaging_purchase_result_print",
					//黃金定期定額申購
					"jspTitle":"<spring:message code= "LB.X0930" />",
					"SVACN":"${averaging_purchase_result.data.SVACN}",
					"ACN":"${averaging_purchase_result.data.ACN}",
					"AMT_06_1":"${averaging_purchase_result.data.AMT_06_1}",
					"AMT_16_1":"${averaging_purchase_result.data.AMT_16_1}",
					"AMT_26_1":"${averaging_purchase_result.data.AMT_26_1}"
			};
			openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
		});	
	}

</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %> 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 定期定額交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1552" /></li>
    <!-- 定期定額申購     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1553" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
			<!-- TODO i18n  -->
			<!-- 黃金定期定額申購 -->
				<h2><spring:message code= "LB.X0930" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form id="formId" method="post" action="">
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
                            <div class="ttb-message">
                            <!-- 定期定額申購成功 -->
                                <span><spring:message code= "LB.X0931" /></span>
                            </div>
                            <c:set value="${averaging_purchase_result.data}" var="dataList"></c:set>
                             <!--約定扣款帳號 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.W1539" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                       <span>${dataList.SVACN}</span>
                                    </div>
                                </span>
                            </div>    
                                
                            <!--黃金存摺帳號 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D1090" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                     	<span>${dataList.ACN}</span>
                                    </div>
                                </span>
                            </div>

                            <!--每月投資日/金額-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.W1557" /></h4>
                                    </label></span>
                                <span class="input-block">
									<div class="ttb-input">
                                    	<span>06<spring:message code="LB.Day" />&nbsp;<spring:message code="LB.NTD" />&nbsp;${dataList.AMT_06_1}&nbsp;<spring:message code="LB.Dollar" /></span>
                                    </div>
									<div class="ttb-input">
										<span>16<spring:message code="LB.Day" />&nbsp;<spring:message code="LB.NTD" />&nbsp;${dataList.AMT_16_1}&nbsp;<spring:message code="LB.Dollar" /></span>
									</div>
									<div class="ttb-input">
										<span>26<spring:message code="LB.Day" />&nbsp;<spring:message code="LB.NTD" />&nbsp;${dataList.AMT_26_1}&nbsp;<spring:message code="LB.Dollar" /></span>
									</div>
                                </span>
                            </div>
                                
                        </div>
                        <input class="ttb-button btn-flat-orange" id="CMPRINT" name="CMPRINT" type="button" value="<spring:message code="LB.Print" />"/>
                    </div>
                </div>
				</form>
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>
					<!-- 若於投資日或投資日以後始申購定期定額投資者，自下次投資日起開始扣款。 -->
					<li><span><spring:message code="LB.Averaging_Purchase_P3_D1" /></span></li>
					<!--網路銀行定期定額每次扣款優惠手續費為新臺幣50元，連同投資金額一併扣繳。  -->
					<li><span><spring:message code="LB.Averaging_Purchase_P3_D2" /></span></li>
					<!-- 電子簽章:為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章(載具i-key)拔除並登出系統。 -->
					<li><span><spring:message code="LB.Averaging_Purchase_P3_D3" /></span></li>
	            </ol>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>