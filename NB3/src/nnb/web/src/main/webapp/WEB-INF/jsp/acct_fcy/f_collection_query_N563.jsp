<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
   <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">    
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    
</head>
<script type="text/javascript">
	$(document).ready(function () {
		
		// 初始化時隱藏驗證span
		$("#hideblock_CheckDateScope").hide();
// 		$("#hideblock_CheckDateFormat1").hide();
// 		$("#hideblock_CheckDateFormat2").hide();
		
		//表單驗證
		$("#formId").validationEngine({
			binded : false,
			promptPosition : "inline"
		});
		$("#CMSUBMIT").click(function(e){
			console.log("submit~~");
			
			//打開驗證隱藏欄位
			$("#hideblock_CheckDateScope").show();
// 			$("#hideblock_CheckDateFormat1").show();
// 			$("#hideblock_CheckDateFormat2").show();
		
			//塞值進隱藏span內的input
// 			$("#Monthly_DateA").val($("#CMSDATE").val());
// 			$("#Monthly_DateB").val($("#CMEDATE").val());
			
			if (!$('#formId').validationEngine('validate')) {
				e.preventDefault();
			} else {
				initBlockUI();
				$("#formId").validationEngine('detach');
				$("form").submit();
			}
		});
		
		$("#PREVIOUS").click(function(e){
			$('#formId').attr('action','${__ctx}'+'/FCY/ACCT/f_collection_query');
			initBlockUI();
			$("form").submit();
		});
		datetimepickerEvent();
	});
	
	//日曆欄位參數設定
	function datetimepickerEvent(){
		$(".CMSDATE").click(function(event) {
			$('#CMSDATE').datetimepicker('show');
		});
		$(".CMEDATE").click(function(event) {
			$('#CMEDATE').datetimepicker('show');
		});
			
		jQuery('.datetimepicker').datetimepicker({
			timepicker:false,
			closeOnDateSelect : true,
			scrollMonth : false,
			scrollInput : false,
			maxDate:'${SYSDATE.data.TODAY}',
			format:'Y/m/d',
			lang: '${transfer}'	
		});
	}
	function selectAction(){
		if($('#actionBar').val()=="excel"){
			$("#downloadType").val("OLDEXCEL");
			$("#templatePath").val("/downloadTemplate/f_collection_query_N563.xls");
		}else if ($('#actionBar').val()=="txt"){
			$("#downloadType").val("TXT");
			$("#templatePath").val("/downloadTemplate/f_collection_query_N563.txt");
		}
		$("#formId").attr("action", "${__ctx}/FCY/ACCT/f_collection_query_DirectDownload");
		$("#formId").submit();
		$('#actionBar').val("");
	}
</script>
<body>
   	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 帳戶查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Account_Inquiry" /></li>
    <!-- 進口/出口託收查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0176" /></li>
    <!-- D/A、D/P出口託收     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0030" /></li>
		</ol>
	</nav>



	
	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
	
	
	<main class="col-12">
		<section id="main-content" class="container"><!-- 主頁內容  -->
			<h2><spring:message code="LB.X0030" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<div class="print-block">
					<select class="minimal" id="actionBar" onchange="selectAction()">
						<!-- 下載-->
						<option value=""><spring:message
								code="LB.Downloads" /></option>
						<!-- 下載Excel檔-->
						<option value="excel"><spring:message
								code="LB.Download_excel_file" /></option>
						<!-- 下載為txt檔-->
						<option value="txt"><spring:message
								code="LB.Download_txt_file" /></option>
					</select>
				</div>
			<br/>
			<br/>
			<form id="formId" method="post" action="${__ctx}/FCY/ACCT/f_collection_query_result">
				<input type="hidden" name="FUNC" value="N563">
				<!-- 						下載用 -->
						<input type="hidden" name="downloadFileName" value="<spring:message code="LB.X0030" />"/>
						<input type="hidden" name="downloadType" id="downloadType"/> 					
						<input type="hidden" name="templatePath" id="templatePath"/>
						<input type="hidden" name="hasMultiRowData" value="false"/> 
						<input type="hidden" name="hasMultiRowData" value="false"/> 	
<!-- 						EXCEL下載用 -->
						<!-- 	EXCEL下載用 -->
						<input type="hidden" name="headerRightEnd" value="9"/>
						<input type="hidden" name="headerBottomEnd" value="6"/>
						<input type="hidden" name="rowStartIndex" value="7"/>
						<input type="hidden" name="rowRightEnd" value="9"/>
						<input type="hidden" name="footerStartIndex" value="9" />
						<input type="hidden" name="footerEndIndex" value="11" />
						<input type="hidden" name="footerRightEnd" value="10" />
						
<!-- 						TXT下載用 -->
						<input type="hidden" name="txtHeaderBottomEnd" value="7"/>
						<input type="hidden" name="txtHasRowData" value="true"/>
						<input type="hidden" name="txtHasFooter" value="true"/>
				<div class="main-content-block row">
					<!-- 主頁內容  -->
					<div class="col-12">
						<div class="ttb-input-block">
							<div class="ttb-message">
							<p><spring:message code="LB.X0030" /></p>
							</div>
						<div class="ttb-input-item row">
							<span class="input-title"> <label><h4><spring:message code="LB.Inquiry_period_1" />：</h4></label></span>
								<span class="input-block">
									<div class="ttb-input">
											<span class="input-subtitle subtitle-color"> <spring:message code="LB.D0013" /> </span> 
											<input type="text" id="CMSDATE" name="CMSDATE"
												class="text-input datetimepicker" maxlength="10" value="${SYSDATE.data.TODAY}" />
											<span class="input-unit CMSDATE"> 
											<img src="${__ctx}/img/icon-7.svg" />
											</span>
<!-- 											不在畫面上顯示的span -->
<!-- 											<span id="hideblock_CheckDateFormat1"> 驗證用的input  -->
<!-- 											<input id="Monthly_DateA" type="text" -->
<!-- 												class="text-input validate[required, verification_date[Monthly_DateA]]" -->
<!-- 												style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;" /> -->
<!-- 											</span> -->
									</div>
									<div class="ttb-input">
											<span class="input-subtitle subtitle-color"> <spring:message code="LB.D0014" /> </span> 
												<input type="text" id="CMEDATE" name="CMEDATE"
													class="text-input datetimepicker" maxlength="10" value="${SYSDATE.data.TODAY}" />
											<span class="input-unit CMEDATE"> 
												<img src="${__ctx}/img/icon-7.svg" />
											</span>
<!-- 											不在畫面上顯示的span -->
<!-- 											<span id="hideblock_CheckDateFormat2"> 驗證用的input  -->
<!-- 											<input id="Monthly_DateB" type="text" -->
<!-- 												class="text-input validate[required, verification_date[Monthly_DateB]]" -->
<!-- 												style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;" /> -->
<!-- 											</span> -->
										</div>
									<span id="hideblock_CheckDateScope">
									<!--驗證用的input -->
									<input id="odate" name="odate" type="text" value="${SYSDATE.data.TODAY}" class="text-input validate[required,funcCall[validate_CheckDateScope[ '<spring:message code="LB.X1485" />',odate , CMSDATE ,CMEDATE, false,12,null]]]" style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
									</span>
						</div>
						</div>
						<input id="PREVIOUS" type="button" class="ttb-button btn-flat-gray"  value="<spring:message code="LB.Back_to_previous_page" />"> 
						<input id="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
					</div>
				</div>
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p><!-- 說明 -->
						<li><spring:message code="LB.F_Collection_Query_N563_P1_D1" /></li>
						<li><spring:message code="LB.F_Collection_Query_N563_P1_D2" /></li>
					</ol>
			</div>
			</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>
