<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);

	$("#printButton").click(function(){
		var params = {
			"jspTemplateName":"fund_regular_result_print",
			"jspTitle":"<spring:message code= "LB.W1083" />",
			"CMQTIME":"${bsData.CMQTIME}",
			"FDINVTYPEChinese":"${FDINVTYPEChinese}",
			"INVTYPEChinese":"${INVTYPEChinese}",
			"TYPEChinese":"${TYPEChinese}",
			"hiddenCUSIDN":"${hiddenCUSIDN}",
			"hiddenNAME":"${hiddenNAME}",
			"TRANSCODE":"${TRANSCODE}",
			"FUNDLNAME":"${FUNDLNAME}",
			"RISK":"${RISK}",
			"ADCCYNAME":"${ADCCYNAME}",
			"AMT3Format":"${AMT3Format}",
			"FCAFEEFormat":"${FCAFEEFormat}",
			"FCA2Format":"${FCA2Format}",
			"AMT5Format":"${AMT5Format}",
			"FUNDACN":"${bsData.FUNDACN}",
			"TRADEDATEFormat":"${TRADEDATEFormat}",
			"PAYTYPE":"${PAYTYPE}",
			"PAYDAY1":"${PAYDAY1}",
			"PAYDAY2":"${PAYDAY2}",
			"PAYDAY3":"${PAYDAY3}",
			"PAYDAY4":"${PAYDAY4}",
			"PAYDAY5":"${PAYDAY5}",
			"PAYDAY6":"${PAYDAY6}",
			"PAYDAY7":"${PAYDAY7}",
			"PAYDAY8":"${PAYDAY8}",
			"PAYDAY9":"${PAYDAY9}",
			"DBDATEFormat":"${DBDATEFormat}",
			"YIELDInteger":"${YIELDInteger}",
			"STOPInteger":"${STOPInteger}"
		};
		openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
	});
});
</script>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	<!--麵包屑-->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Funds" /></li>
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1064" /></li>
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1083" /></li>
		</ol>
	</nav>
	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.W1083" /></h2>
				<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
<!-- 				<h4>定期投資申購成功</h4> -->
					<form id="formID" method="post">
						<input type="hidden" name="ADOPID" value="C017"/>
						<div class="main-content-block row">
							<div class="col-12 tab-content">
							<br>
							<h5 style="color:red"><Strong><spring:message code="LB.W1102" /></Strong></h5>
								<div class="ttb-input-block">
									<!-- 客戶投資屬性 -->
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label><h4><spring:message code="LB.W1067" />：</h4></label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												<span>${FDINVTYPEChinese}</span>
											</div>
										</span>
									</div>
									<!-- 投資方式 -->
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label><h4><spring:message code="LB.W0946" />：</h4></label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												<span>${INVTYPEChinese}</span>
											</div>
										</span>
									</div>
									<!-- 扣款方式 -->
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label><h4><spring:message code="LB.D0173" />：</h4></label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												<span>${TYPEChinese}</span>
											</div>
										</span>
									</div>
									<!-- Id_no -->
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label><h4><spring:message code="LB.Id_no"/>：</h4></label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												<span>${hiddenCUSIDN}</span>
											</div>
										</span>
									</div>
									<!-- Name -->
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label><h4><spring:message code="LB.Name"/>：</h4></label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												<span>${hiddenNAME}</span>
											</div>
										</span>
									</div>
									<!-- 基金名稱 -->
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label><h4><spring:message code="LB.W0025" />：</h4></label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												<span>（${TRANSCODE}）${FUNDLNAME}</span>
											</div>
										</span>
									</div>
									<!-- 商品風險等級-->
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label><h4><spring:message code="LB.W1073" />：</h4></label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												<span>${RISK}</span>
											</div>
										</span>
									</div>
									<!-- 申購金額-->
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label><h4><spring:message code="LB.W1074" />：</h4></label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												<span>${ADCCYNAME}&nbsp;${AMT3Format}</span>
											</div>
										</span>
									</div>
									<!-- 手續費率-->
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label><h4><spring:message code="LB.W1034" />：</h4></label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												<span>${FCAFEEFormat}％
												<c:if test="${SAL01 > 0 && SAL03 == 'N'}">
													<font color="red">（<spring:message code="LB.X2606_1" />${SAL01Format}<spring:message code="LB.X2606_2" />）</font>
												</c:if></span>
											</div>
										</span>
									</div>
									<!-- 手續費-->
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label><h4><spring:message code="LB.D0507" />：</h4></label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												<span>${ADCCYNAME}&nbsp;${FCA2Format}</span>
												<font color="red">（<spring:message code="LB.X0405" />）</font>
											</div>
										</span>
									</div>
									<!-- 每次扣款金額-->
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label><h4><spring:message code="LB.X0406" />：</h4></label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												<span>${ADCCYNAME}&nbsp;${AMT5Format}</span>
											</div>
										</span>
									</div>
									<!-- 扣款帳號／卡號-->
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label><h4><spring:message code="LB.W1046" />：</h4></label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												${FUNDACN}
												<c:if test="${SAL01 > 0}">
													<font color="red">（<spring:message code="LB.X2607" />）</font>
												</c:if>
											</div>
										</span>
									</div>
									<!-- 申請日期-->
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label><h4><spring:message code="LB.D0127" />：</h4></label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												<span>${TRADEDATEFormat}</span>
											</div>
										</span>
									</div>
									<!-- 每月扣款日-->
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label><h4><spring:message code="LB.W1592" />：</h4></label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												<span>
												<c:if test="${PAYTYPE == '3'}">
													<c:if test="${PAYDAY4 != '00'}">
														${PAYDAY4}<spring:message code="LB.Day" />&nbsp;
													</c:if>
													<c:if test="${PAYDAY1 != '00'}">
														${PAYDAY1}<spring:message code="LB.Day" />&nbsp;
													</c:if>
													<c:if test="${PAYDAY5 != '00'}">
														${PAYDAY5}<spring:message code="LB.Day" />&nbsp;
													</c:if>
													<c:if test="${PAYDAY2 != '00'}">
														${PAYDAY2}<spring:message code="LB.Day" />&nbsp;
													</c:if>
													<c:if test="${PAYDAY6 != '00'}">
														${PAYDAY6}<spring:message code="LB.Day" />&nbsp;
													</c:if>
													<c:if test="${PAYDAY3 != '00'}">
														${PAYDAY3}<spring:message code="LB.Day" />&nbsp;
													</c:if>
												</c:if>
												<c:if test="${PAYTYPE == '1' || PAYTYPE == '2'}">
													<c:if test="${PAYDAY1 != '00'}">
														${PAYDAY1}<spring:message code="LB.Day" />&nbsp;
													</c:if>
													<c:if test="${PAYDAY2 != '00'}">
														${PAYDAY2}<spring:message code="LB.Day" />&nbsp;
													</c:if>
													<c:if test="${PAYDAY3 != '00'}">
														${PAYDAY3}<spring:message code="LB.Day" />&nbsp;
													</c:if>
													<c:if test="${PAYDAY4 != '00'}">
														${PAYDAY4}<spring:message code="LB.Day" />&nbsp;
													</c:if>
													<c:if test="${PAYDAY5 != '00'}">
														${PAYDAY5}<spring:message code="LB.Day" />&nbsp;
													</c:if>
													<c:if test="${PAYDAY6 != '00'}">
														${PAYDAY6}<spring:message code="LB.Day" />&nbsp;
													</c:if>
													<c:if test="${PAYDAY7 != '00'}">
														${PAYDAY7}<spring:message code="LB.Day" />&nbsp;
													</c:if>
													<c:if test="${PAYDAY8 != '00'}">
														${PAYDAY8}<spring:message code="LB.Day" />&nbsp;
													</c:if>
													<c:if test="${PAYDAY9 != '00'}">
														${PAYDAY9}<spring:message code="LB.Day" />&nbsp;
													</c:if>
												</c:if>
												</span>
											</div>
										</span>
									</div>
									<!-- 首次扣款日-->
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label><h4><spring:message code="LB.W1108" />：</h4></label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												<span>${DBDATEFormat}</span>
											</div>
										</span>
									</div>
									<!--停利通知設定-->
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label><h4><spring:message code="LB.W1075" />：</h4></label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												<span>${YIELDInteger}％</span>
											</div>
										</span>
									</div>
									<!--停損通知設定-->
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label><h4><spring:message code="LB.W1076" />：</h4></label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												<span>－${STOPInteger}％</span>
											</div>
										</span>
									</div>
								</div>
								<!--本筆符合薪轉戶優惠，交易成立後，不可變更扣款日期及金額。-->
								<c:if test="${SAL01 > 0 && SAL03 == 'N'}">
									<div class="row" style="justify-content: space-around">
										<span> 
											<font color="red"><spring:message code="LB.X2603" /></font>
										</span> 
									</div>
								</c:if>
								<input type="button" id="printButton" value="<spring:message code="LB.Print"/>" class="ttb-button btn-flat-orange"/>
							</div>
						</div>
					</form>
					<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /></p>
						<li><spring:message code="LB.X1234" />：</li>
						<ul class="description-list">
							<li>(1)&nbsp;<span><spring:message code="LB.Fund_Redeem_Data_P3_D1" /></span></li>
							<li>(2)&nbsp;<span><spring:message code="LB.Regular_Investment_PPP2_D1" /></span></li>
						</ul>
<!-- 					</ol> -->
<!-- 					<ol class="description-list list-decimal"> -->
						<li><spring:message code="LB.X0400" />：</li>
						<ul class="description-list">
							<li><span>(1)&nbsp;<spring:message code="LB.D1424" /></li>
							<ul class="description-list">	
								<li>&nbsp;&nbsp;「<spring:message code="LB.Funds_NonMenu" />」<spring:message code="LB.X0401" />→「<spring:message code="LB.Inquiry_Service" />」→「<spring:message code="LB.W0927" />」</span></li>
							</ul>
							<li><span>(2)&nbsp;<spring:message code="LB.X0402" />（https://www.tbb.com.tw/）</li>
							<ul class="description-list">	
								<li>&nbsp;&nbsp;「<spring:message code="LB.X0403" />」→「<spring:message code="LB.X0404" />」→「<spring:message code="LB.W0927" />」</span></li>
							</ul>
						</ul>
					</ol>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
<c:if test="${showDialog == 'true'}">
	<%@ include file="../index/txncssslog.jsp"%>
	</c:if>
</html>