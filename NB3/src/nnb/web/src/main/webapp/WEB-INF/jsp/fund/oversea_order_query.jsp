<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {
		// 將.table變更為footable
		setTimeout("initDataTable()",100);
		//initFootable();
		init();
	});
	
	function init() {
        //列印
    	$("#printbtn").click(function(){
			var params = {
				"jspTemplateName":"oversea_balance_print",
				"jspTitle":'<spring:message code= "LB.W1007" />',
				"DATE":"${oversea_order_query.data.DATE}",
				"TOTRECNO":"${oversea_order_query.data.TOTRECNO}",
				"TIME":"${oversea_order_query.data.TIME}",
				"CUSNAME":"${oversea_order_query.data.CUSNAME}"
			};
			openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
		});
		//上一頁按鈕
		$("#CMBACK").click(function() {
			var action = '${__ctx}/NT/ACCT/BOND/bond_balance';
			$('#back').val("Y");
			$("#formId").attr("action", action);
			initBlockUI();
			$("#formId").submit();
		});
		
		//繼續查詢
    	$("#CMCONTINU").click(function (e) {
			e = e || window.event;
			console.log("submit~~");

			$("#formId").validationEngine('detach');
			initBlockUI(); //遮罩
			$("#formId").attr("action", "${__ctx}/NT/ACCT/BOND/bond_balance_result");
			$("#formId").submit();
		});
    }
	
	//下拉式選單
 	function formReset() {
 		if ($('#actionBar').val()=="reEnter"){
	 		document.getElementById("formId").reset();
	 		$('#actionBar').val("");
 		}
	}
	
 	function processQuery(CDNO) {
		//配息查詢
		   var sCDNO = CDNO;
		   if(!(sCDNO.length==11)){
			   sCDNO = "0"+sCDNO;
			}		
		  	$("#formId").attr("action", "${__ctx}/FUND/QUERY/oversea_data_result_dividend");
  			$("#CDNO").val(sCDNO);
			$("#formId").submit();	
		return false; 		
 	} 	

</script>
</head>
	<body>
		<!-- header     -->
		<header>
			<%@ include file="../index/header.jsp"%>
		</header>
	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金/海外債券     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0368" /></li>
    <!-- 帳務查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Inquiry_Service" /></li>
    <!-- 海外債券委託單查詢    -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X2571" /></li>
		</ol>
	</nav>

		<!-- menu、登出窗格 -->
		<div class="content row">
			<%@ include file="../index/menu.jsp"%>
			<!-- 	快速選單內容 -->
			<!-- content row END -->
			<main class="col-12">
				<section id="main-content" class="container">
					<!-- 主頁內容  -->
<!-- 					海外債券委託單查詢 -->
					<h2><spring:message code="LB.X2571" /></h2>
					<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				   <form id="formId" method="post" action="">
						<input type="hidden" id="CDNO" name="CDNO" value="" /> 
						<input type="hidden" id="PREVIOUS" name="PREVIOUS" value="oversea_balance" /> 												
					</form>
						<div class="main-content-block row">
							<div class="col-12  tab-content">
								<ul class="ttb-result-list">
									<li><!-- 查詢時間 -->
										<h3><spring:message code="LB.Inquiry_time" /></h3>
	                                	<p>${oversea_order_query.data.CMQTIME}</p>
									</li>
									<li><!-- 資料總數 -->
										<h3><spring:message code="LB.Total_records" /></h3>
	                                	<p>${oversea_order_query.data.TOTRECNO}&nbsp;<spring:message code="LB.Rows"/></p>
									</li>
								</ul>
								<!-- 海外債券餘額及損益查詢 -->
								<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
									<thead>
										<tr>
<%-- 											<th>委託日期<br>信託帳號</th> --%>
											<th><spring:message code="LB.W1022"/><br><spring:message code="LB.W0944"/></th>
<%-- 									      	<th>債券代碼<br>債券名稱</th> --%>
									      	<th><spring:message code="LB.W1011"/><br><spring:message code="LB.W1012"/></th>
<%-- 									      	<th>計價幣別</th> --%>
									      	<th><spring:message code="LB.W0909"/></th>
<%-- 									      	<th>信託金額<br>面額</th>      	 --%>
									      	<th><spring:message code="LB.W0026"/><br><spring:message code="LB.W1014"/></th>      	
<%-- 									      	<th>委託買/賣價(%)<br>交易類別</th> --%>
									      	<th><spring:message code="LB.X2572"/>(%)<br><spring:message code="LB.W0076"/></th>
<%-- 									      	<th>累計配息金額<br>前手息</th> --%>
									      	<th><spring:message code="LB.W0918"/><br><spring:message code="LB.W1018"/></th>
<%-- 									      	<th>申購手續費<br>贖回保管費</th> --%>
									      	<th><spring:message code="LB.X2573"/><br><spring:message code="LB.X2574"/></th>
<%-- 									      	<th>入/扣款帳號<br>預估收/付金額</th> --%>
									      	<th><spring:message code="LB.X2575"/><br><spring:message code="LB.X2576"/></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="dataList" items="${oversea_order_query.data.REC}">
											<tr>
												<td class="text-center">${dataList.O01}<br>${dataList.O02}</td>
												<td class="text-left">${dataList.O03}<br>${dataList.O04}</td>
												<td class="text-center">${dataList.O15_SHOW}</td>
												<td class="text-right">${dataList.O05}<br>${dataList.O06}</td>
												<td class="text-center">${dataList.O07}%<br>${dataList.O08}</td>
												<td class="text-right">${dataList.O09}<br>${dataList.O10}${dataList.O11}</td>
												<td class="text-right">${dataList.O13}<br>${dataList.O12}</td>
											    <td class="text-center">${dataList.O14}<br>${dataList.O16}</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
								
							</div>
						</div>
					
				</section>
			</main>
			<!-- 		main-content END -->
			<!-- 	content row END -->
		</div>
		<%@ include file="../index/footer.jsp"%>
	</body>
<c:if test="${showDialog == 'true'}">
		<%@ include file="../index/txncssslog.jsp"%>
		</c:if>
</html>