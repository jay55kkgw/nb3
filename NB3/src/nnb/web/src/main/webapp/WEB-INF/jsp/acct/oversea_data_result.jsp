<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {
		
		setTimeout("initDataTable()",100);

		// 將.table變更為footable
		//initFootable();
		init();
	});
	function init() {
		//列印
		$("#printbtn").click(function() {
			var params = {
				"jspTemplateName" : "oversea_data_result_print",
				"jspTitle" : "<spring:message code= "LB.W1021" />",
				"CMQTIME" : "${oversea_data_result.data.CMQTIME}",
				"CMPERIOD" : "${oversea_data_result.data.CMPERIOD}",
				"COUNT" : "${oversea_data_result.data.CMRECNUM}",
				"hiddenname":"${oversea_data_result.data.hiddenname}",
				"hiddencusidn":"${oversea_data_result.data.hiddencusidn}"
				};
			openWindowWithPost(
				"${__ctx}/print",
				"height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",
				params);
		});
		$('#CMBACK').click(function(){
			initBlockUI();
			fstop.getPage('${pageContext.request.contextPath}'+'/FUND/QUERY/oversea_data','', '');
		});
	}
 	function processQuery(CDNO) {
		//配息查詢
		   var sCDNO = CDNO;
		   if(!(sCDNO.length==11)){
			   sCDNO = "0"+sCDNO;
			}		
		  	$("#formId").attr("action", "${__ctx}/FUND/QUERY/oversea_data_result_dividend");
  			$("#CDNO").val(sCDNO);
			$("#formId").submit();	
		return false; 		
 	} 	
  	function processQueryHistory(CDNO) {
  		//歷史資料查詢
  		  var sCDNO = CDNO;
		   if(!(sCDNO.length==11)){
			   sCDNO = "0"+sCDNO;
			}			
  			$("#formId").attr("action", "${__ctx}/FUND/QUERY/oversea_data_result_history");
  			$("#CDNO").val(sCDNO);
			$("#formId").submit();	
 		return false;
 	}
</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金/海外債券     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0368" /></li>
    <!-- 帳務查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Inquiry_Service" /></li>
    <!-- 海外債券交易明細查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1021" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code="LB.W1021" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>

			<div class="main-content-block row">
				<div class="col-12">
					<ul class="ttb-result-list">
						<li>
							<!-- 查詢時間 -->
							<h3>
								<spring:message code="LB.Inquiry_time" />：
							</h3>
							<p>
								 ${oversea_data_result.data.CMQTIME}
							</p> 
						</li>
						<li>
							<!-- 查詢期間 -->
							<h3>
								<spring:message code="LB.Inquiry_period_1" />：
							</h3>
							<p>
							 ${oversea_data_result.data.CMPERIOD}
							</p>
						</li>
						<li>
							 <!-- 資料總數 -->
							<h3>
								<spring:message code="LB.Total_records" />：
							</h3>
							<p>
								 ${oversea_data_result.data.CMRECNUM}
								<spring:message code="LB.Rows" />
							</p>
						</li>
						<li>
						<!-- 身分證字號/統一編號 -->
						<h3>
						<spring:message code="LB.Id_no" />
						</h3>
						<p>
						${oversea_data_result.data.hiddencusidn}
						</p>
						</li>
						<li>
						<h3><spring:message code="LB.Name" /></h3>
						<p>${oversea_data_result.data.hiddenname}</p>
						<!-- 姓名 -->
						</li>
					</ul>
					<!-- 輕鬆理財證券交割明細 -->
						<table class="stripe table-striped ttb-table dtable"  data-toggle-column="first">
							<thead>
							<!-- 
								<tr>			
									<th><spring:message code="LB.Id_no" /></th>
									<th class="text-left" colspan="7">${oversea_data_result.data.hiddencusidn}</th>
								</tr>
								<tr>
									
									<th><spring:message code="LB.Name" /></th>
									<th class="text-left" colspan="7">${oversea_data_result.data.hiddenname}</th>
								</tr>
						    -->
								<tr>
									<th><spring:message code="LB.W1022" /><br><spring:message code="LB.W1023" /></th>
      								<th><spring:message code="LB.W1011" /><br><spring:message code="LB.W1012" /></th>
									<th><spring:message code="LB.Transaction_type" /></th>
      								<th><spring:message code="LB.W1032" /><br><spring:message code="LB.W1014" /></th>
      								<th><spring:message code="LB.W1018" /><br><spring:message code="LB.W1033" /></th>   
									<th><spring:message code="LB.W1034" /><br><spring:message code="LB.D0507" /></th>
									<th><spring:message code="LB.X0369" /><br><spring:message code="LB.X0370" /></th>
									<th><spring:message code="LB.W1037" /><br><spring:message code="LB.W0909" /></th>
									<th><spring:message code="LB.The_account_credited_date" /><br><spring:message code="LB.W1038" /></th>
									<th><spring:message code="LB.W0953" /></th>
							</tr>
							</thead>
							<tbody>
								<c:forEach var="dataList" items="${oversea_data_result.data.REC}">
									<tr>
										<td class="text-center">${dataList.O01}<br>${dataList.HO03}</td>
										<td class="text-center">${dataList.O04}<br>${dataList.O05}</td>
										<td class="text-center">${dataList.O06}</td>
										<td class="text-right">${dataList.O12}<br>${dataList.O11}</td>
										<td class="text-right">${dataList.O16}<br>${dataList.O13}</td>
										<td class="text-right">${dataList.O14}<br>${dataList.O15}</td>
										<td class="text-right">${dataList.O20}<br>${dataList.O21}</td>
										<td class="text-center">${dataList.O17}<br>${dataList.O18}</td>
										<td class="text-center">${dataList.O22}<br>${dataList.O19}</td>
										<td class="text-center">
      										<input type="button" class="ttb-sm-btn btn-flat-orange" name="AAA" value="<spring:message code="LB.X2219" />" onclick="processQuery('${dataList.O03}')"><br>
      										<input type="button" style="margin-top: 5px;" class="ttb-sm-btn btn-flat-orange" name="BBB"value="<spring:message code="LB.W0931" />" onclick="processQueryHistory('${dataList.O03}')">
      									</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<form id="formId" method="post" action="">
						<input type="hidden" id="CDNO" name="CDNO" value="" /> 
						<input type="hidden" id="PREVIOUS" name="PREVIOUS" value="oversea_data_result" /> 												
						</form>
					<!-- 回上頁-->
					<input type="button" class="ttb-button btn-flat-gray" name="CMBACK" id="CMBACK" onclick="${__ctx}/FUND/QUERY/oversea_data" value='<spring:message code="LB.Back_to_previous_page"/>' />
					<!-- 列印鈕-->
					<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />" />
					
				</div>
			</div>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>