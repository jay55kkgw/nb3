<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
	<style>
		.Line-break{
			white-space:normal;
			word-break:break-all;
			width:100px;
			word-wrap:break-word;
		}
	</style>
</head>
<body>
    <!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 預約交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X1477" /></li>
    <!-- 預約交易結果查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0053" /></li>
		</ol>
	</nav>



		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 --> 
 		<div class="content row">
 		   <%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->

 		<main class="col-12">	
		<!-- 		主頁內容  -->

			<section id="main-content" class="container">
		
				<h2><spring:message code="LB.W0053" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 						下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
<!-- 						下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
					</select>
				</div>
				<div class="main-content-block row">
					<div class="col-12">
						<ul class="ttb-result-list">
							<li>
								<!-- 查詢時間 -->
								<h3><spring:message code="LB.Inquiry_time" /> </h3>								
								<p>
								${reservation_trans_result.data.CMQTIME}
								</p>
							</li>
							<li>
								<!-- 查詢期間 -->
								<h3><spring:message code="LB.Inquiry_period_1" /> </h3>								
								<p>
								${reservation_trans_result.data.cmedate}
								</p>
							</li>
							<li>
								<!-- 交易狀態-->
								<h3><spring:message code="LB.W0054" /> </h3>
								<p>
								${reservation_trans_result.data.FGTXSTATUS}
								</p>
							</li>
							<li>
								<!-- 資料總數 -->
								<h3><spring:message code="LB.Total_records" /> </h3>								
								<p>
								${reservation_trans_result.data.COUNT} <spring:message code="LB.Rows" />
								</p>
							</li>							
							<li>
								<!-- 表名 -->
								<h3><spring:message code="LB.Report_name" /> </h3>								
								<p>
									<spring:message code="LB.W0059" />
								</p>
							</li>							
						</ul>
						
						<table class="stripe table-striped ttb-table dtable" data-show-toggle="first">
						<thead>
							<tr>
								<!-- 預約編號 -->
								<th>
									<spring:message code="LB.Booking_number" />
								</th>
								<!-- 轉帳日期 -->								
								<th>
									<spring:message code="LB.Transfer_date" />
								</th>
								<!-- 轉出帳號 -->
								<th>
									<spring:message code="LB.Payers_account_no" />
								</th>
								<!-- 轉入帳號/繳費稅代號 -->
								<th>
									<spring:message code="LB.W0060" />
								</th>
								<!-- 轉帳金額 -->
								<th>
									<spring:message code="LB.Amount" />
								</th>
								<!-- 手續費-->
								<th>
									<spring:message code="LB.D0507" />
								</th>
								<!-- 跨行序號 --><!-- 交易類別 -->
								<th>
									<spring:message code="LB.W0062" /><hr/><spring:message code="LB.Transaction_type" />
								</th>
								<!-- 備註 -->
								<th>
									<spring:message code="LB.Note" />
								</th>
								<!-- 轉帳結果 -->
								<th>
									<spring:message code="LB.W0063" />
								</th>
							</tr>
						</thead>
						<c:if test="${reservation_trans_result.data.COUNT == '0'}">
							<tbody>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							</tbody>
						</c:if>	
						<!--          列表區 -->
						<c:if test="${reservation_trans_result.data.COUNT > '0'}">
						<tbody>
         					<c:forEach var="dataList" items="${reservation_trans_result.data.REC}">
							<tr>            	
								<!-- 預約編號 -->
								<td class="text-center">${dataList.DPSCHNO}</td>
								<!-- 轉帳日期 -->
								<td class="text-center">${dataList.DPSCHTXDATE}</td>                
								<!-- 轉出帳號 -->
								<td class="text-center">${dataList.DPWDAC}</td>				          	               
								<!-- 轉入帳號/繳費稅代號 -->
								<td class="text-center">
									<c:if test="${not empty dataList.DPSVBH}">
										${dataList.DPSVBH}-
										<br>
									</c:if>
									${dataList.DPSVAC}
								</td>								                
								<!-- 轉帳金額 -->
								<td class="text-right">${dataList.DPTXAMT}</td>                
								<!-- 手續費-->
								<td class="text-right">${dataList.DPEFEE}</td>
								<!-- 跨行序號 --><!-- 交易類別 -->
								<td class="text-center">
									<c:if test="${empty dataList.DPSTANNO}">
										-
									</c:if>
									<c:if test="${not empty dataList.DPSTANNO}">
										${dataList.DPSTANNO}
									</c:if>
									<hr/>${dataList.DPSEQH}
								</td>
								<!-- 備註 -->
								<td class="text-center Line-break">${dataList.DPTXMEMO}</td>
								<!-- 轉帳結果 -->
								<td class="text-center">
									${dataList.DPTXSTATUS}
									<br>
									<a href="javascript:return false;" style="color:blue;" title="${dataList.CODEMSG}">${dataList.DPEXCODE}</a>																		
								</td>
							</tr>	
							</c:forEach>						
							</tbody>
						</c:if>
						</table>
						<input id="previous" name="previous" type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />" />	
						<input type="button" class="ttb-button btn-flat-orange" id = "printbtn" value="<spring:message code="LB.Print" />"/>
					</div>
				</div>
					<!-- 說明 -->
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>
					<li style="font-weight:bold; color:#FF0000;"><span><spring:message code="LB.Reservation_Trans_P2_D1" /></span></li>
					<li><span><spring:message code="LB.Reservation_Trans_P2_D2" /></span></li>
					<li><span><spring:message code="LB.Reservation_Trans_P2_D3" /></span></li>
				</ol>
				<form method="post" id="formId" action="${__ctx}/download">
					<!-- 下載用 -->
					<input type="hidden" name="downloadFileName" value="<spring:message code="LB.W0053" />"/>
					<input type="hidden" name="downloadType" id="downloadType"/> 					
					<input type="hidden" name="templatePath" id="templatePath"/>
					<input type="hidden" name="CMQTIME" value="${reservation_trans_result.data.CMQTIME}"/>
					<input type="hidden" name="cmdate" value="${reservation_trans_result.data.cmedate}"/>
					<input type="hidden" name="COUNT" value="${reservation_trans_result.data.COUNT}"/>
					<input type="hidden" name="FGTXSTATUS" value="${reservation_trans_result.data.FGTXSTATUS}"/>
					<!-- EXCEL下載用 -->
					<input type="hidden" name="headerRightEnd" value="10"/>
					<input type="hidden" name="headerBottomEnd" value="8"/>
					<input type="hidden" name="rowStartIndex" value="9"/>
					<input type="hidden" name="rowRightEnd" value="10"/>
					<!-- 					上到下長度 -->
					<input type="hidden" name="txtHeaderBottomEnd" value="10"/> 					
					<input type="hidden" name="txtHasRowData" value="true"/>
					<input type="hidden" name="txtHasFooter" value="false"/>
				</form>
			</section>
			<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->


	<%@ include file="../index/footer.jsp"%>
		<script type="text/javascript">
		$(document).ready(function(){
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()",10);
			// 開始查詢資料並完成畫面
			setTimeout("init()",20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)",500);
			setTimeout("initDataTable()",100);
		});
		function init(){
			//initFootable();

			
			//上一頁按鈕
			$("#previous").click(function() {
				initBlockUI();
				fstop.getPage('${pageContext.request.contextPath}'+'/NT/ACCT/RESERVATION/reservation_trans','', '');
			});
			
			$("#printbtn").click(function(){
				var params = {
						"jspTemplateName":"reservation_trans_result_print",
						"jspTitle":"<spring:message code= "LB.W0053" />",
						"CMQTIME":"${reservation_trans_result.data.CMQTIME}",
						"cmdate":"${reservation_trans_result.data.cmedate}",
						"COUNT":"${reservation_trans_result.data.COUNT}",
						"FGTXSTATUS":"${reservation_trans_result.data.FGTXSTATUS}"
					};
					openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});				
		}
		//選項
	 	function formReset() {
// 	 		initBlockUI();
			if ($('#actionBar').val()=="excel"){
				$("#downloadType").val("OLDEXCEL");
				$("#templatePath").val("/downloadTemplate/reservation_trans.xls");
	 		}else if ($('#actionBar').val()=="txt"){
				$("#downloadType").val("TXT");
				$("#templatePath").val("/downloadTemplate/reservation_trans.txt");
	 		}
			$("#formId").attr("target", "");
            $("#formId").submit();
            $('#actionBar').val("");
        }
		</script>
</body>
</html>