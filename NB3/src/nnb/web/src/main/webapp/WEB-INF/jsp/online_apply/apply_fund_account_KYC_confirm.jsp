<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<script type="text/javascript">

$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()", 10);
	// 開始查詢資料並完成畫面
	setTimeout("init()", 100);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)", 500);
});

//初始化
function init(){
	
	// 學歷
	if(${UPD_FLG1 == 'Y'}){
		$("#firstTR").show();
	}
	// 職業
	if(${UPD_FLG2 == 'Y'}){
		$("#secondTR").show();
	}
	// 年收入
	if(${UPD_FLG3 == 'Y'}){
		$("#thirdTR").show();
	}
	
	// 領有全民健康保險重大傷病證明--變更前
	if(${OMARK1 == 'Y'}){
		$("#OMARK1TD").html("<spring:message code= "LB.D0034_2" />");
	} else {
		$("#OMARK1TD").html("<spring:message code= "LB.D0034_3" />");
	}
	// 領有全民健康保險重大傷病證明--變更後
	if(${MARK1 == 'Y'}){
		$("#MARK1TD").html("<spring:message code= "LB.D0034_2" />");
	} else {
		$("#MARK1TD").html("<spring:message code= "LB.D0034_3" />");
	}
	
	// 確認送出	
	$("#CMSUBMIT").click(function(){
		$("#formId").attr("action", "${__ctx}" + "${next}");
		$("#formId").submit();
	});
	// 重新填寫
	$("#previous").click(function(){
		$("#formId").append('<input type="hidden" name="back" value="Y" />');	
		$("#formId").attr("action", "${__ctx}" + "${previous}");
		$("#formId").submit();
	});
	
}

</script>
</head>
<body>
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0847" /></li>
		</ol>
	</nav>
	<div class="content row">
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>預約開立基金戶</h2>
				<div id="step-bar">
					<ul>
						<li class="finished">注意事項與權益</li>
						<c:if test="${FATCA_CONSENT == 'Y'}">
							<li class="finished">FATCA個人客戶身份識別聲明</li>
						</c:if>
						<li class="active">投資屬性調查</li>
						<li class="">開戶資料</li>
						<li class="">確認資料</li>
						<li class="">完成申請</li>
					</ul>
				</div>
				
				<form id="formId" action="${__ctx}/FUND/TRANSFER/fund_invest_attr_s" method="post">
					<input type="hidden" name="FDQ1" value="${Q1}"/>
					<input type="hidden" name="FDQ2" value="${Q2String}"/>
					<input type="hidden" name="FDQ3" value="${Q3}"/>
					<input type="hidden" name="FDQ4" value="${Q4}"/>
					<input type="hidden" name="FDQ5" value="${Q5}"/>
					<input type="hidden" name="FDQ6" value="${Q6}"/>
					<input type="hidden" name="FDQ7" value="${Q7}"/>
					<input type="hidden" name="FDQ8" value="${Q8}"/>
					<input type="hidden" name="FDQ9" value="${Q9}"/>
					<input type="hidden" name="FDQ10" value="${Q10}"/>
					<input type="hidden" name="FDQ11" value="${Q11}"/>  
					<input type="hidden" name="FDQ12" value="${Q12}"/>   
					<input type="hidden" name="FDQ13" value="${Q13}"/>
					<input type="hidden" name="FDQ14" value="${Q14}"/>  
					<input type="hidden" name="FDQ15" value="${Q15}"/>
					<input type="hidden" name="FDMARK1" value="${MARK1}"/>        
					<input type="hidden" name="FDINVTYPE" value="${FDINVTYPE}"/>
					<input type="hidden" name="FDSCORE" value="${iScore}"/>
					<input type="hidden" name="TYPE" value="01"/>
					<input type="hidden" name="UPD_FLG" value="${UPD_FLG}"/>
					<input type="hidden" name="DEGREE" value="${DEGREE}"/>
					<input type="hidden" name="CAREER" value="${SRCFUND}"/>
					<input type="hidden" name="SALARY" value="${SALARY}"/>
					<input type="hidden" name="EDITON" value="11005"/>
					<input type="hidden" name="ANSWER" value="${ANSWER}"/>
					<input type="hidden" id="CMPASSWORD" name="CMPASSWORD"/>
					<input type="hidden" id="PINNEW" name="PINNEW"/>	 
					<input type="hidden" name="RTC" value="${RTC}"/>
					<input type="hidden" name="TXID" value="${RTC}"/>
					<input type="hidden" id="KIND" name="KIND"/>
					<!--交易機制所需欄位-->
					<input type="hidden" id="jsondc" name="jsondc" value="${jsondc}"/>
					<input type="hidden" id="ISSUER" name="ISSUER"/>
					<input type="hidden" id="ACNNO" name="ACNNO"/>
					<input type="hidden" id="TRMID" name="TRMID"/>
					<input type="hidden" id="iSeqNo" name="iSeqNo"/>
					<input type="hidden" id="ICSEQ" name="ICSEQ"/>
					<input type="hidden" id="TAC" name="TAC"/>
					<input type="hidden" id="pkcs7Sign" name="pkcs7Sign"/>
    				<!--FATCA所需欄位-->
    				<input type="hidden" name="FATCA_CONSENT" value="${FATCA_CONSENT}"/>
    				<input type="hidden" name="CITY" value="${CITY}"/>
					
					<div class="main-content-block row">
						<div class="col-12 terms-block questionnaire-block">
							<div class="ttb-message">
								<p>線上客戶投資屬性問卷調查表</p>
							</div>
							<div class="text-left">
								<p class="form-description">請您再次確認您填答的投資屬性問卷調查</p>
								<ol class="description-list list-decimal">
				                    <p>說明</p>
				                    <li>
				                    	<span>提醒您，申購/轉換基金之風險等級須符合您的風險承受度，才可以進行交易。若超出您的風險承受度，系統將不允許您進行交易。</span>
				                    </li>
				                    <li>
				                        <span>如同意下方客戶投資屬性問卷調查內容，請點選確定送出，以進行風險屬性評估。如不同意，可以點選重新填寫按鈕重新進行問卷。</span>
				                    </li>
				                    <li>
										<span>線上客戶投資屬性調查表每天最多僅能填寫3次。</span>
									</li>
				                </ol>
	
								<div class="classification-block">
									<p>個人資料異動項目</p>
								</div>
	
								<table class="stripe table table-striped ttb-table">
									<thead>
										<tr>
											<th data-title="異動項目">異動項目</th>
											<th data-title="變更前">變更前</th>
											<th data-title="變更後">變更後</th>
										</tr>
									</thead>
									<tbody>
										<tr id="firstTR" style="display:none">
											<td>學歷</td>
											<td>${DEGREE_DESC}</td>
											<td>${NEWDEGREE_DESC}</td>
										</tr>
										<tr id="secondTR" style="display:none">
											<td>職業</td>
											<td>${CAREERCHINESE}</td>
											<td>${SRCFUNDCHINESE}</td>
										</tr>
										<tr id="thirdTR" style="display:none">
											<td>年收入</td>
											<td>${oldsalary}萬元</td>
											<td>${newsalary}萬元</td>
										</tr>
										<tr>
											<td>領有全民健康保險重大傷病證明</td>
											<td id="OMARK1TD"></td>
											<td id="MARK1TD"></td>
										</tr>
									<tbody>
								</table>
	
								<div class="classification-block">
									<p>投資屬性問卷調查</p>
								</div>
								
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>1. 您的年齡</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>${age}歲</p>
											<input type="hidden" value="${Q1}" id="Q1" name="Q1" >
										</div>
									</li>
									<li>
										<div class="questionnaire-title">
											<p>2. 您的教育程度為何?</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>${NEWDEGREE_DESC}</p>
											<input type="hidden" value="${Q2}" id="Q2" name="Q2" >
										</div>
									</li>
									<li>
										<div class="questionnaire-title">
											<p>3. 您的職業為何?</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>${SRCFUNDCHINESE}</p>
											<input type="hidden" value="${Q3}" id="Q3" name="Q3" >
										</div>
									</li>
									<li>
										<div class="questionnaire-title">
											<p>4. 您個人/家庭年收入為 (新臺幣)?</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>${newsalary}萬元</p>
											<input type="hidden" value="${Q4}" id="Q4" name="Q4" >
										</div>
									</li>
									<li>
										<div class="questionnaire-title">
											<p>5. 您個人所得與資金來源?</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												<c:if test="${Q5=='A'}">
													A﹒ 薪資、租金、資本利得等
												</c:if>
												<c:if test="${Q5=='B'}">
													B﹒ 儲蓄所得
												</c:if>
												<c:if test="${Q5=='C'}">
													C﹒ 退休金
												</c:if>
												<input type="hidden" value="${Q5}" id="Q5" name="Q5" >
											</p>
										</div>
									</li>
									<li>
										<div class="questionnaire-title">
											<p>6. 您目前的有價證券與存款合計為 (新臺幣)?</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												<c:if test="${Q6=='A'}">
													A﹒ 未達50萬
												</c:if>
												<c:if test="${Q6=='B'}">
													B﹒ 50萬以上 ~ 未達100萬
												</c:if>
												<c:if test="${Q6=='C'}">
													C﹒ 100萬以上 ~ 未達200萬
												</c:if>
												<c:if test="${Q6=='D'}">
													D﹒ 200萬以上
												</c:if>
												<input type="hidden" value="${Q6}" id="Q6" name="Q6" >
											</p>
										</div>
									</li>
									<li>
										<div class="questionnaire-title">
											<p>7. 您投資的主要目的與需求為何?</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												<c:if test="${Q7=='A'}">
													A﹒ 投資理財
												</c:if>
												<c:if test="${Q7=='B'}">
													B﹒ 退休計畫
												</c:if>
												<c:if test="${Q7=='C'}">
													C﹒ 教育基金
												</c:if>
												<c:if test="${Q7=='D'}">
													D﹒ 清償債務
												</c:if>
												<input type="hidden" value="${Q7}" id="Q7" name="Q7" >
											</p>
										</div>
									</li>
									<li>
										<div class="questionnaire-title">
											<p>8. 您計畫何時開始提領您投資的部分金額?</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												<c:if test="${Q8=='A'}">
													A﹒ 未達1年
												</c:if>
												<c:if test="${Q8=='B'}">
													B﹒ 1年以上 ~ 未達2年
												</c:if>
												<c:if test="${Q8=='C'}">
													C﹒ 2年以上 ~ 未達5年
												</c:if>
												<c:if test="${Q8=='D'}">
													D﹒ 5年以上
												</c:if>
												<input type="hidden" value="${Q8}" id="Q8" name="Q8" >
											</p>
										</div>
									</li>
									<li>
										<div class="questionnaire-title">
											<p>9. 您對投資之期望報酬率為?</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												<c:if test="${Q9=='A'}">
													A﹒ 0% ~ 5%
												</c:if>
												<c:if test="${Q9=='B'}">
													B﹒ 6% ~ 10%
												</c:if>
												<c:if test="${Q9=='C'}">
													C﹒ 11% ~ 20%
												</c:if>
												<c:if test="${Q9=='D'}">
													D﹒ 大於20%
												</c:if>
												<input type="hidden" value="${Q9}" id="Q9" name="Q9" >
											</p>
										</div>
									</li>
									<li>
										<div class="questionnaire-title">
											<p>10. 您投資金融商品預計投資期限多長?</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												<c:if test="${Q10=='A'}">
													A﹒ 未達1年
												</c:if>
												<c:if test="${Q10=='B'}">
													B﹒ 1年以上 ~ 未達2年
												</c:if>
												<c:if test="${Q10=='C'}">
													C﹒ 2年以上
												</c:if>
												<input type="hidden" value="${Q10}" id="Q10" name="Q10" >
											</p>
										</div>
									</li>
									<li>
										<div class="questionnaire-title">
											<p>11. 您過去的投資經驗?</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												<c:if test="${Q111 != null}">
													<p>A﹒ 曾投資期貨、連動債、衍生性金融商品</p>
													<input type="hidden" value="A" id="Q111" name="Q111" >
												</c:if>
												<c:if test="${Q112 != null}">
													<p>B﹒ 曾投資國內外共同基金、債券、股票、投資型保單</p>
													<input type="hidden" value="B" id="Q112" name="Q112" >
												</c:if>
												<c:if test="${Q113 != null}">
													<p>C﹒ 對於投資有心得，偏好自行決定投資策略</p>
													<input type="hidden" value="C" id="Q113" name="Q113" >
												</c:if>
												<c:if test="${Q114 != null}">
													<p>D﹒ 無經驗</p>
													<input type="hidden" value="D" id="Q114" name="Q114" >
												</c:if>
											</p>
										</div>
									</li>
									<li>
										<div class="questionnaire-title">
											<p>12. 您從事投資理財的時間?</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												<c:if test="${Q12=='A'}">
													A﹒ 未達1年
												</c:if>
												<c:if test="${Q12=='B'}">
													B﹒ 1年以上 ~ 未達2年
												</c:if>
												<c:if test="${Q12=='C'}">
													C﹒ 2年以上 ~ 未達5年
												</c:if>
												<c:if test="${Q12=='D'}">
													D﹒ 5年以上
												</c:if>
												<input type="hidden" value="${Q12}" id="Q12" name="Q12" >
											</p>
										</div>
									</li>
									<li>
										<div class="questionnaire-title">
											<p>13. 您對金融商品的認識?</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<c:if test="${Q131 != null}">
												<p>A﹒ 對金融商品（如連動債等衍生性商品）了解</p>
												<input type="hidden" value="A" id="Q131" name="Q131" >
											</c:if>
											<c:if test="${Q132 != null}">
												<p>B﹒ 對金融商品（如股票）了解</p>
												<input type="hidden" value="B" id="Q132" name="Q132" >
											</c:if>
											<c:if test="${Q133 != null}">
												<p>C﹒ 對金融商品（如國內外共同基金）了解</p>
												<input type="hidden" value="C" id="Q133" name="Q133" >
											</c:if>
											<c:if test="${Q134 != null}">
												<p>D﹒ 對金融商品不了解</p>
												<input type="hidden" value="D" id="Q134" name="Q134" >
											</c:if>
										</div>
									</li>
									<li>
										<div class="questionnaire-title">
											<p>14. 下您可承受的投資損失風險波動範圍?</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												<c:if test="${Q14=='A'}">
													A﹒ 無法忍受投資虧損
												</c:if>
												<c:if test="${Q14=='B'}">
													B﹒ 5％以下
												</c:if>
												<c:if test="${Q14=='C'}">
													C﹒ 6％～10％
												</c:if>
												<c:if test="${Q14=='D'}">
													D﹒ 11％～20％
												</c:if>
												<c:if test="${Q14=='E'}">
													E﹒ 大於20％
												</c:if>
												<input type="hidden" value="${Q14}" id="Q14" name="Q14" >
											</p>
										</div>
									</li>
									<li>
										<div class="questionnaire-title">
											<p>15. 您偏好以下列何種商品做為您投資理財配置?</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												<c:if test="${Q15=='A'}">
													A﹒ 期貨或衍生性金融商品
												</c:if>
												<c:if test="${Q15=='B'}">
													B﹒ 國內外基金、股票或投資型保單
												</c:if>
												<c:if test="${Q15=='C'}">
													C﹒ 存款、定存
												</c:if>
												<input type="hidden" value="${Q15}" id="Q15" name="Q15" >
											</p>
										</div>
									</li>
									<li>
										<div class="questionnaire-title">
											<p>16. 是否領有全民健康保險重大傷病證明?</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												<c:if test="${MARK1=='Y'}">
													是
												</c:if>
												<c:if test="${MARK1=='N'}">
													否
												</c:if>
											</p>
										</div>
									</li>
								</ul>
							</div>
							
							<input type="BUTTON" class="ttb-button btn-flat-gray" value="重新填寫" id="previous" name="previous">
							<input type="BUTTON" class="ttb-button btn-flat-orange" value="確認送出" id="CMSUBMIT" name="CMSUBMIT">
						</div>
					</div>
				</form>
				
			</section>
		</main>
	</div>
	
	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>