<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="zh">

<head>
    <meta charset="UTF-8">
    <title>臺灣中小企業銀行 新世代網路銀行</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <meta itemprop="image" content="">
    <%@ include file="../__import_js.jsp"%>
<!--     <link rel="stylesheet" href="../css/reset.css"> -->
<!--     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
</head>
<style type="text/css">
.test-img {
    width: 50%;
}
</style>

<body onresize="fixArea()">
    <main>
        <section id="lion-card">
	        <form method="post" id="formId" action="${__ctx}/CREDIT/APPLY/apply_creditcard">
				<input type="hidden" name="ADOPID" value="NA03"/>
				<input type="hidden" id="QRCODE" name="QRCODE" value="L"/>
	            <img class="" src="${__ctx}/img/creditcard/lion_cardBN.jpg" border="0" usemap="#lionCardMap" alt="lionCard" style="width:100%" />
	            <map name="lionCardMap" id="lionCardMap">
	                <area target="_blank" alt="Official website" title="Official website" href="" coords="233,970,681,1017" shape="rect">
	                <!-- <area target="_blank" alt="Applicationnow" title="Applicationnow" href="" coords="1292,904,1785,1038" shape="rect"> -->
	            </map>
	    	</form>
        </section>
    </main>
	<script>
		$(document).ready(function() {
			fixArea();
		});
		
		function fixArea() {
			var screenW = $(window).width();
			var ratio = screenW/1920;
			var leftUpX = Math.round(ratio*233);
			var leftUpY = Math.round(ratio*970);
			var rightDownX = Math.round(ratio*681);
			var rightDownY = Math.round(ratio*1017);
			var ss = leftUpX+","+leftUpY+","+rightDownX+","+rightDownY;
			
			var $area1 = $("<area target='_blank' alt='Official website' href='https://www.tbb.com.tw/web/guest/-630' title='Official website' shape='rect' style='cursor: hand'>");
			$area1.attr("coords", ss);
                    
			$("#lionCardMap").empty();
			$("#lionCardMap").append($area1);
			
			leftUpX = Math.round(ratio*1292);
			leftUpY = Math.round(ratio*904);
			rightDownX = Math.round(ratio*1785);
			rightDownY = Math.round(ratio*1038);
			ss = leftUpX+","+leftUpY+","+rightDownX+","+rightDownY;
			
			var $area1 = $("<area target='_self' alt='Applicationnow' title='Applicationnow' shape='rect' style='cursor: hand' onclick='formsubmit()'>");
			$area1.attr("coords", ss);
			$("#lionCardMap").append($area1);
		}
		
		function formsubmit(){
			$("#formId").submit();
		}
	</script>
</body>

</html>