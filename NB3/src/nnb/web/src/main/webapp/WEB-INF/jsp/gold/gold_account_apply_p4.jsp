<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment-${pageContext.response.locale}.js"></script>
	    <script type="text/javascript">
        $(document).ready(function () {
            initFootable(); // 將.table變更為footable 
            init();
// 			datetimepickerEvent();
// 			getTmr();
			genDateList();
			// 初始化時隱藏span
			$("#hideblock_CMSDATE").hide();
			$("#hideblock_CMEDATE").hide();
        });

        function init() {
			$("#formId").validationEngine({binded: false,promptPosition: "inline", scroll: false});
	    	AddCityOptionsTW(document.getElementById("CITYCHA"));
	    	
	    	//進行初始化
    		//網路交易指定新台幣帳戶 (預設設定)
    		$("#SVACN").children().each(function(){
    		    if ($(this).val()=='${result_data.SVACN}'){
    		        $(this).attr("selected", true);
    		    }
    		});
    		//本次開戶目的 (預設設定)
   		    if ('${result_data.PURPOSE}' == '1'){
   		        $("#PURPOSE1").attr("checked", true);
   		    }
   		    if ('${result_data.PURPOSE}' == '2'){
   		        $("#PURPOSE2").attr("checked", true);
   		    }
   		    if ('${result_data.PURPOSE}' == '3'){
   		        $("#PURPOSE3").attr("checked", true);
   		    }
   		    if ('${result_data.PURPOSE}' == '4'){
   		        $("#PURPOSE4").attr("checked", true);
   		    }
   		    if ('${result_data.PURPOSE}' == '5'){
   		        $("#PURPOSE5").attr("checked", true);
   		    }
    		//是否更換過姓名 (預設設定)
   		    if ('${result_data.RENAME}' == 'Y'){
   		        $("#RENAME1").attr("checked", true);
   		    }
   		    if ('${result_data.RENAME}' == 'N'){
   		        $("#RENAME2").attr("checked", true);
   		    }
    		//補換發縣市 (預設設定)
    		$("#CITYCHA").children().each(function(){
    		    if ($(this).val()=='${result_data.CITYCHA}'){
    		        $(this).attr("selected", true);
    		    }
    		});

	    	
	    	$("#CMSUBMIT").click(function(e){			
	    		$("#CMDATE1").val((parseInt($("#CMDATE1YY").val()) + 1911).toString() + "/" + $("#CMDATE1MM").val() + "/" + $("#CMDATE1DD").val());
	    		$("#CMDATE2").val((parseInt($("#CMDATE2YY").val()) + 1911).toString() + "/" + $("#CMDATE2MM").val() + "/" + $("#CMDATE2DD").val());
				//打開驗證隱藏欄位
				$("#hideblock_CMDATE1").show();
				$("#hideblock_CMDATE2").show();
				//塞值進span內的input
				$("#validate_CMDATE1").val($("#CMDATE1YY").val() + "/" + $("#CMDATE1MM").val() + "/" + $("#CMDATE1DD").val());
				$("#validate_CMDATE2").val($("#CMDATE2YY").val() + "/" + $("#CMDATE2MM").val() + "/" + $("#CMDATE2DD").val());
				$("#CITYCHA_str").val($("#CITYCHA").find(":selected").text());
    			//切割 HOMEP 至 HOMEZIP 與 HOMETEL
				var inputData = $("#TELNUMP").val();
				var HOMEP = matchPhone(inputData);
				
				//將S_MONEY值塞入validate_S_MONEY驗證
				$("#validate_S_MONEY").val($("#S_MONEY").val());
				
				if(HOMEP != null){
	    			$('#ARACOD2').val(HOMEP[0]);
	    			$('#TELNUM2').val(HOMEP[1]);
    				$('#ARACOD2').addClass("validate[required,funcCall[validate_CheckNumber['<spring:message code= "LB.X1207" />',ARACOD2]]]");
    				$('#TELNUM2').addClass("validate[required]");
				}else{
    				$('#ARACOD2').removeClass("validate[required,funcCall[validate_CheckNumber['<spring:message code= "LB.X1207" />',ARACOD2]]]");
    				$('#TELNUM2').removeClass("validate[required]");
				}
				e = e || window.event;
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					$("#CUSIDN2").prop("disabled",false);
		        	initBlockUI();
					var action = '${__ctx}/GOLD/APPLY/gold_account_apply_p5';
					$("form").attr("action", action);
	    			$("form").submit();
				}
			});
			$("#CMRESET").click(function(e){
				$("#formId")[0].reset();
				getTmr();
			});

    		//上一頁按鈕
    		$("#CMBACK").click(function() {
				$("#formId").validationEngine('detach');
    			var action = '${__ctx}/GOLD/APPLY/gold_account_apply_p2';
    			$('#back').val("Y");
    			$("form").attr("action", action);
    			$("form").submit();
    		});
        }

		// 日曆欄位參數設定
		function datetimepickerEvent() {
			$(".CMDATE1").click(function (event) {
				$('#CMDATE1').datetimepicker('show');
			});
			$(".CMDATE2").click(function (event) {
				$('#CMDATE2').datetimepicker('show');
			});
			jQuery('.datetimepicker').datetimepicker({
				timepicker: false,
				closeOnDateSelect: true,
				scrollMonth: false,
				scrollInput: false,
				format: 'Y/m/d',
				lang: '${transfer}'
			});
		}
		//預約自動輸入今天
		function getTmr() {
			var today = new Date();
			today.setDate(today.getDate());
			var y = today.getFullYear();
			var m = today.getMonth() + 1;
			var d = today.getDate();
			if(m<10){
				m = "0"+m
			}
			if(d<10){
				d="0"+d
			}
			var tmr = y + "/" + m + "/" + d
			$('#CMDATE1').val(tmr);
			$('#CMDATE2').val(tmr);
		}
		function ValidateValue(textbox) {
			var IllegalString = "+-_＿︿%@[`~!#$^&*()=|{}':;',\\[\\].<>/?~！#￥……&*（）——|{}【】‘；：”“'。，、？]‘'";
			var textboxvalue = textbox.value;
			var index = textboxvalue.length - 1;
			var s = textbox.value.charAt(index);
			if (IllegalString.indexOf(s) >= 0) {
				s = textboxvalue.substring(0, index);
				textbox.value = s;
			}
		}

		function genDateList(){
			var thisYear = new Date().getFullYear();
			var thisMonth = new Date().getMonth() + 1;
			var thisDay = new Date().getDate();
			var CMDATE1YY = "${ result_data.CMDATE1YY }";
			var CMDATE1MM = "${ result_data.CMDATE1MM }";
			var CMDATE1DD = "${ result_data.CMDATE1DD }";
			var CMDATE2YY = "${ result_data.CMDATE2YY }";
			var CMDATE2MM = "${ result_data.CMDATE2MM }";
			var CMDATE2DD = "${ result_data.CMDATE2DD }";
			for(var i = thisYear - 1911;i > 0 ;i--){
				j = i.toString();
				for(k = 0;k <= 2 - i.toString().length;k++)j='0' + j;
				if(j == CMDATE1YY){
					$("#CMDATE1YY").append("<option value='" + j + "' selected><spring:message code="LB.D0583"/>&nbsp;" + i + "&nbsp;<spring:message code="LB.Year"/></option>");
				}else{
					$("#CMDATE1YY").append("<option value='" + j + "' ><spring:message code="LB.D0583"/>&nbsp;" + i + "&nbsp;<spring:message code="LB.Year"/></option>");
				}
			}
			for(var i = 1;i <= 12;i++){
				j = i.toString();
				for(k = 0;k <= 1 - i.toString().length;k++)j='0' + j;
				if(j == CMDATE1MM){
					$("#CMDATE1MM").append("<option value='" + j + "' selected>" + i + "&nbsp;<spring:message code="LB.Month"/></option>");
				}else{
					$("#CMDATE1MM").append("<option value='" + j + "' >" + i + "&nbsp;<spring:message code="LB.Month"/></option>");
				}
			}
			for(var i = 1;i <= 31;i++){
				j = i.toString();
				for(k = 0;k <= 1 - i.toString().length;k++)j='0' + j;
				if(j == CMDATE1DD){
					$("#CMDATE1DD").append("<option value='" + j + "' selected>" + i + "&nbsp;<spring:message code="LB.D0586"/></option>");
				}else{
					$("#CMDATE1DD").append("<option value='" + j + "' >" + i + "&nbsp;<spring:message code="LB.D0586"/></option>");
				}
			}
			for(var i = thisYear - 1911;i > 0 ;i--){
				j = i.toString();
				for(k = 0;k <= 2 - i.toString().length;k++)j='0' + j;
				if(j == CMDATE2YY){
					$("#CMDATE2YY").append("<option value='" + j + "' selected><spring:message code="LB.D0583"/>&nbsp;" + i + "&nbsp;<spring:message code="LB.Year"/></option>");
				}else{
					$("#CMDATE2YY").append("<option value='" + j + "' ><spring:message code="LB.D0583"/>&nbsp;" + i + "&nbsp;<spring:message code="LB.Year"/></option>");
				}
			}
			for(var i = 1;i <= 12;i++){
				j = i.toString();
				for(k = 0;k <= 1 - i.toString().length;k++)j='0' + j;
				if(j == CMDATE2MM){
					$("#CMDATE2MM").append("<option value='" + j + "' selected>" + i + "&nbsp;<spring:message code="LB.Month"/></option>");
				}else{
					$("#CMDATE2MM").append("<option value='" + j + "' >" + i + "&nbsp;<spring:message code="LB.Month"/></option>");
				}
			}
			for(var i = 1;i <= 31;i++){
				j = i.toString();
				for(k = 0;k <= 1 - i.toString().length;k++)j='0' + j;
				if(j == CMDATE2DD){
					$("#CMDATE2DD").append("<option value='" + j + "' selected>" + i + "&nbsp;<spring:message code="LB.D0586"/> </option>");
				}else{
					$("#CMDATE2DD").append("<option value='" + j + "' >" + i + "&nbsp;<spring:message code="LB.D0586"/> </option>");
				}
			}
		}
		
		function matchPhone(input){
			var phoneList = [];
			var re = new RegExp(/^\d{1,3}-\d{1,10}$/)
			if(re.test(input)){
				var vList1 = input.split("-");
				phoneList.push(vList1[0]);
				phoneList.push(vList1[1]);
				return phoneList;
			}
			return null;
		}
		
		function changeMatchPhone(input){
			var inputData = $("#"+input).val();
			var phoneList = matchPhone(inputData);
			if(phoneList != null)
				$("#"+input).val(phoneList[0] + '-' + phoneList[1]);
		}
    </script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 黃金存摺帳戶     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2273" /></li>
    <!-- 黃金存摺帳戶申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1655" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!-- 線上申請黃金存摺帳戶 -->
					<spring:message code="LB.W1655"/>
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <div id="step-bar">
                    <ul>
                        <li class="finished">注意事項與權益</li>
                        <li class="active">開戶資料</li>
                        <li class="">確認資料</li>
                        <li class="">申請結果</li>
                    </ul>
                </div>
                <form id="formId" method="post">
					<input type="hidden" name="AMT_FEE" value="${n920_data.data.fee}">
					<input type="hidden" name="action" value="forward">
					<input type="hidden" id="TOKEN" name="TOKEN" value="${sessionScope.transfer_confirm_token}" /><!-- 防止重複交易 -->
					<input type="hidden" id="CITYCHA_str" name="CITYCHA_str" />
					<input type="hidden" name="PIC_FLAG" value="Y">
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
							<div class="ttb-input-block">
								<!-- 臺灣中小企業銀行履行個人資料保護法告知義務告知書 -->
	                            <div class="ttb-message">
	                                <p>開戶資料</p>
	                            </div>
	                            <!-- ************************************************** -->
	                            <div class="classification-block">
	                                <p>開戶資料</p>
	                                <p>( <span class="high-light">*</span> 為必填)</p>
	                            </div>
	                            <!-- ************************************************** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
										<!-- 指定網路銀行交易 -->
										<!-- 買進／回售新臺幣帳戶 -->
											網路交易指定新台幣帳戶 <span class="high-light">*</span>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<select name="SVACN" id="SVACN" class="custom-select select-input half-input validate[funcCallRequired[validate_CheckSelect[網路交易指定新台幣帳戶,SVACN,#]]]">
												<option value="#">
												<!-- 請選擇帳號 -->
													<spring:message code="LB.Select_account"/>
												</option>
												<c:forEach var="dataList" items="${n920_data.data.REC}">
													<option> ${dataList.ACN}</option>
												</c:forEach>
											</select>
										</div>
									</span>
								</div>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
									<!-- 本次開戶目的 -->
										<h4><spring:message code="LB.D1075"/> <span class="high-light">*</span></h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
											<!-- 儲蓄 -->
												<spring:message code="LB.D1076"/>
												<input type="radio" name="PURPOSE" id="PURPOSE1" value="1" />
												<span class="ttb-radio"></span>
											</label>
											<label class="radio-block">
												<!--薪轉-->
												<spring:message code="LB.D1077"/>
												<input type="radio" name="PURPOSE" id="PURPOSE2" value="2" />
												<span class="ttb-radio"></span>
											</label>
											<label class="radio-block">
												<!--資金調撥-->
												<spring:message code="LB.D1078"/>
												<input type="radio" name="PURPOSE" id="PURPOSE3" value="3" />
												<span class="ttb-radio"></span>
											</label>
											<label class="radio-block">
											<!-- 投資 -->
												<spring:message code="LB.D1079"/>
												<input type="radio" name="PURPOSE" id="PURPOSE4" value="4" />
												<span class="ttb-radio"></span>
											</label>
											<label class="radio-block">
											<!-- 其他 -->
												<spring:message code="LB.D0572"/>
												<input type="radio" name="PURPOSE" id="PURPOSE5" value="5" />
												<span class="ttb-radio"></span>
											</label>
										
											<span class="hideblock">
												<input id="validate_PURPOSE" name="PURPOSE" type="radio"  
													class="validate[funcCall[validate_Radio['<spring:message code= "LB.D1075" />',PURPOSE]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
											</span>
										</div>
									</span>
								</div>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<!--  預期往來金額(近一年) -->
											<spring:message code="LB.D1081_1"/> <span class="high-light">*</span>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
										<!-- 萬元 -->
											<input type="text" id="S_MONEY" name="S_MONEY" class="text-input" value="${ result_data.S_MONEY }" size="8" maxlength="7" onkeyup="this.value=this.value.replace(/[^\d\.]/g, &#39;&#39;)"/> <spring:message code="LB.D0088_2"/>
											
											<!-- 不在畫面上顯示的span -->
											<span id="hideblocka" >
											<!-- 驗證用的input -->
											<input id="validate_S_MONEY" name="validate_S_MONEY" type="text" class="validate[required]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
									</span>
								</div>
	                            <!-- ************************************************** -->
							  	<div class="classification-block">
	                                <p>基本資料</p>
	                                <p>( <span class="high-light">*</span> 為必填)</p>
	                            </div>
	                            <!-- ************************************************** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
									<!-- 身分證統一編號 -->
										<h4>
											<spring:message code="LB.D0519"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="CUSIDN2" name="CUSIDN2" class="text-input" value="${n920_data.data.CUSIDN}" size="10" maxlength="10" disabled/>
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
									<!-- 身分證統一編號 -->
										<h4>
											是否<spring:message code="LB.D0052_3"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
											<!-- 有 -->
												<spring:message code="LB.D1070_1"/>
												<input type="radio" name="RENAME" id="RENAME1" value="Y" />
												<span class="ttb-radio"></span>
											</label>
											<label class="radio-block">
												<!-- 無 -->
												<spring:message code="LB.D1070_2"/>
												<input type="radio" name="RENAME" id="RENAME2" value="N" />
												<span class="ttb-radio"></span>
												<!-- 更換過姓名 -->
											</label>
											<span class="hideblock">
												<input id="validate_RENAME" name="RENAME" type="radio"  
													class="validate[funcCall[validate_Radio[是否<spring:message code="LB.D0052_3"/>,RENAME]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
											</span>
										</div>
									</span>
								</div>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
									<!-- 出生年月日 -->
										<h4><spring:message code="LB.D0054"/> <span class="high-light">*</span></h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<div class="ttb-input">	
												<select name="CMDATE1YY" id="CMDATE1YY" class="custom-select select-input input-width-100">
													<option value="">
														---
													</option>
												</select>
												<select name="CMDATE1MM" id="CMDATE1MM" class="custom-select select-input input-width-60">
													<option value="">
														---
													</option>
												</select>
												<select name="CMDATE1DD" id="CMDATE1DD" class="custom-select select-input input-width-60">
													<option value="">
														---
													</option>
												</select>
												<input type="hidden" id="CMDATE1" name="CMDATE1" maxlength="10" value="" /> 
<!-- 												<span class="input-unit CMDATE1"> -->
<%-- 													<img src="${__ctx}/img/icon-7.svg" /> --%>
<!-- 												</span> -->
												<!-- 不在畫面上顯示的span -->
												<span id="hideblocka" >
												<!-- 驗證用的input -->
												<input id="validate_CMDATE1" name="validate_CMDATE1" type="text" class="text-input
													validate[funcCall[validate_CheckCDate['<spring:message code= "LB.D0054" />',validate_CMDATE1,001/01/01,${n920_data.data.TODAY}]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												</span>
											</div>
										</div>
									</span>
								</div>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
									<!-- 身分證為換發/補發/新發日期 -->
										<h4>身分證發證資訊 <span class="high-light">*</span></h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<select name="CMDATE2YY" id="CMDATE2YY" class="custom-select select-input input-width-100">
												<option value="">
													---
												</option>
											</select>
											<select name="CMDATE2MM" id="CMDATE2MM" class="custom-select select-input input-width-60">
												<option value="">
													---
												</option>
											</select>
											<select name="CMDATE2DD" id="CMDATE2DD" class="custom-select select-input input-width-60">
												<option value="">
													---
												</option>
											</select>
											<input type="hidden" id="CMDATE2" name="CMDATE2" maxlength="10" value="" /> 
<!-- 											<input type="text" id="CMDATE2" name="CMDATE2" class="text-input datetimepicker"  maxlength="10" value="" />  -->
<!-- 											<span class="input-unit CMDATE2"> -->
<%-- 												<img src="${__ctx}/img/icon-7.svg" /> --%>
<!-- 											</span> -->
											<!-- 不在畫面上顯示的span -->
											<span id="hideblocka" >
											<!-- 驗證用的input -->
											<input id="validate_CMDATE2" name="validate_CMDATE2" type="text" class="text-input
												validate[funcCall[validate_CheckCDate['身分證發證日期', validate_CMDATE2, 001/01/01, ${n920_data.data.TODAY}]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px ;" />
											</span>
										</div>
										<div class="ttb-input">
											<select name="CITYCHA" id="CITYCHA" class="custom-select select-input half-input input-width-125">
          									<!-- 請選擇縣市 -->
          										<option value="#" selected><spring:message code="LB.W1670"/></option>
											</select>
											<select name="TYPECHA" id="TYPECHA" class="custom-select select-input half-input input-width-100">
											<!-- 初領 -->
								          		<option value="1" ><spring:message code="LB.D1113"/></option>
								          		<!-- 換發 -->
								          		<option value="2" selected><spring:message code="LB.W1664"/></option>
								          		<!-- 補發 -->
								          		<option value="3"><spring:message code="LB.W1665"/></option>
											</select>
											<input id="validate_CITYCHA" name="validate_CITYCHA" type="text" class="
												validate[funcCallRequired[validate_CheckSelect[<spring:message code= "LB.D1071" />,CITYCHA,#]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</div>
									</span>
								</div>
								<!-- ********** -->
<!-- 								<div class="ttb-input-item row"> -->
<!-- 									<span class="input-title">  -->
<!-- 									<label> -->
									<!-- 身份證有無相片 -->
<%-- 										<h4><spring:message code="LB.D1072"/></h4> --%>
<!-- 									</label> -->
<!-- 									</span>  -->
<!-- 									<span class="input-block"> -->
<!-- 										<div class="ttb-input"> -->
<!-- 											<label class="radio-block"> -->
											<!-- 有 -->
<%-- 												<spring:message code="LB.D1070_1"/> --%>
<!-- 												<input type="radio" name="PIC_FLAG" id="PIC_FLAG1" value="Y" checked /> -->
<!-- 												<span class="ttb-radio"></span> -->
<!-- 											</label> -->
<!-- 											<label class="radio-block"> -->
											<!-- 無 -->
<%-- 												<spring:message code="LB.D1070_2"/> --%>
<!-- 												<input type="radio" name="PIC_FLAG" id="PIC_FLAG2" value="N" /> -->
<!-- 												<span class="ttb-radio"></span> -->
<!-- 											</label> -->
<!-- 										</div> -->
<!-- 									</span> -->
<!-- 								</div> -->
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
										<!-- 聯絡連話 -->
											<spring:message code="LB.D0205"/> <span class="high-light">*</span>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="TELNUMP" name="TELNUMP" value="${ result_data.TELNUMP }" class="text-input validate[funcCallRequired[validate_OneInputPhone[聯絡連話格式錯誤,TELNUMP]]]" 
												placeholder="例如：02-7818218"  maxLength="14" size="11" onchange="changeMatchPhone('TELNUMP')"/>
										<!-- 連絡電話區域碼 -->
											<input type="text" id="ARACOD2" name="ARACOD2" class="validate[required,funcCall[validate_CheckNumber['<spring:message code= "LB.X1207" />',ARACOD2]]]" value="" size="3" maxlength="3"
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											<input type="text" id="TELNUM2" name="TELNUM2" class="validate[required]" value="" size="17" maxlength="17"
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</div>
									</span>
								</div>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
										<!-- 任職機構 -->
										<spring:message code="LB.D1074"/> <span class="high-light">*</span>
											
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="EMPLOYER" name="EMPLOYER" value="${ result_data.EMPLOYER }" class="text-input validate[required]" value="" size="30" maxlength="30" onkeyup="ValidateValue(this)" 
													onchange="value=value.replace(/[^\a-\z\A-\Z0-9\u4E00-\u9FA5]/g,&#39;&#39;)" 
													onpaste="value=value.replace(/[^\a-\z\A-\Z0-9\u4E00-\u9FA5]/g,&#39;&#39;)"/>
										</div>
									</span>
								</div>
	                            <!-- ************************************************** -->
								<div class="classification-block">
	                                <p>開戶費用</p>
	                                <p>( <span class="high-light">*</span> 為必填)</p>
	                            </div>
	                            <!-- ************************************************** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<!-- 開戶手續費 -->
											<spring:message code="LB.D1082"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
										<!-- 新臺幣 XX 元 -->
											<span class="high-light"><spring:message code="LB.NTD"/> ${n920_data.data.fee} <spring:message code="LB.Dollar"/></span>
										</div>
									</span>
								</div>
							</div>
                           
	                        <!--button 區域 -->
							<!-- 重新輸入 -->
                       		<spring:message code="LB.X0318" var="cmback"></spring:message>
                       		<input type="button" name="CMBACK" id="CMBACK" value="${cmback}" class="ttb-button btn-flat-gray">		
<%-- 								<spring:message code="LB.Re_enter" var="cmRest"></spring:message> --%>
<%-- 								<input type="button" name="CMRESET" id="CMRESET" value="${cmRest}" class="ttb-button btn-flat-gray"> --%>
                            <input id="CMSUBMIT" name="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.X0080"/>" />
                        <!--                     button 區域 -->
                        </div>  
                    </div>
                </form>
				<div class="text-left">
				    <!-- 		說明： -->
				    <ol class="list-decimal description-list">
				    	<p><spring:message code="LB.Description_of_page"/></p>
				    <!-- 您所提供的身分證資料，經本行查詢後，如有疑義，本行得暫停您的黃金存摺網路銀行功能。 -->
				        <li><spring:message code="LB.Gold_Account_Apply_P4_D1"/> </li>
          				<!-- 線上申請黃金存摺帳戶手續費優惠為新台幣50元。 -->
          				<li><spring:message code="LB.Gold_Account_Apply_P4_D2"/> </li>
				    </ol>
				</div>

	
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>