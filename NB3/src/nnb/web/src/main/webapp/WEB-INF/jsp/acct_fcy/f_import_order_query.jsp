<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<meta http-equiv="Content-Language" content="zh-tw">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
</head>
 <body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 帳戶查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Account_Inquiry" /></li>
    <!-- 進口到單查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0161" /></li>
		</ol>
	</nav>



		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 --> 
 		<div class="content row">
 		   <%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->

 		
		
		<!-- 		主頁內容  -->

			<section id="main-content" class="container">
				<h2><spring:message code="LB.W0161" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				
				<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 						下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
<!-- 						下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
					</select>
				</div>
				
					<div class="main-content-block row">
						<div class="col-12"> 						
				<form id="formId" method="post" action="${__ctx}/FCY/ACCT/f_import_order_query_result">
					<!-- 下載用 -->
 					<!-- 	TODO downloadFileName	要改為信用狀名稱 -->
					<input type="hidden" name="downloadFileName" value="<spring:message code="LB.W0161" />"/>
					<input type="hidden" name="CMQTIME" value=${import_order_query_result.data.CMQTIME}/>
					<input type="hidden" name="CMPERIOD" value=${import_order_query_result.data.CMPERIOD} />
					<input type="hidden" name="CMRECNUM" value=${import_order_query_result.data.CMRECNUM} />
					<input type="hidden" name="downloadType" id="downloadType"/> 					
					<input type="hidden" name="templatePath" id="templatePath"/> 
                    <input type="hidden" id="USERDATA" name="USERDATA" value="" />
					<!-- EXCEL下載用 -->
					<input type="hidden" name="headerRightEnd" value="11"/>
					<input type="hidden" name="headerBottomEnd" value="12"/>
					<input type="hidden" name="rowStartIndex" value="13"/>
					<input type="hidden" name="rowRightEnd" value="11"/>
					
					<!-- TXT下載用 -->
					<input type="hidden" name="txtHeaderBottomEnd" value="9"/>
					<input type="hidden" name="txtHasRowData" value="true"/>
					<input type="hidden" name="txtHasFooter" value="false"/>
						<div class="ttb-input-block">

							<!-- 查詢區間區塊 -->
							<div class="ttb-input-item row">
								<!--查詢區間  -->
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Inquiry_period" /></h4>
									</label>
								</span>
								<span class="input-block">

									<!--  指定日期區塊 -->
									<div class="ttb-input">								

										<!--期間起日 -->
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color">
												<spring:message code="LB.D0013" />
											</span>
											<input type="text" id="FDATE" name="FDATE" class="text-input datetimepicker" maxlength="10" value="" /> 
											<span class="input-unit FDATE">
												<img src="${__ctx}/img/icon-7.svg" />
											</span>
											<!-- 不在畫面上顯示的span -->
											<span id="hideblocka" >
											<!-- 驗證用的input -->
											<input id="Monthly_DateA" name="Monthly_DateA" type="text" class="text-input validate[required, verification_date[Monthly_DateA]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color">
												<spring:message code="LB.Period_end_date" />
											</span>
											<input type="text" id="TDATE" name="TDATE" class="text-input datetimepicker" maxlength="10" value="" /> 
											<span class="input-unit TDATE">
												<img src="${__ctx}/img/icon-7.svg" />
											</span>
											<!-- 不在畫面上顯示的span -->
											<span id="hideblockb" >
											<!-- 驗證用的input -->
											<input id="Monthly_DateB" name="Monthly_DateB" type="text" class="text-input validate[required, verification_date[Monthly_DateB]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
											<!-- 驗證用的span預設隱藏 -->
											<span id="hideblock_CheckDateScope" >
												<!-- 驗證用的input -->
												<input id="odate" name="odate" type="text" class="text-input validate[required,funcCall[validate_CheckDateScope['<spring:message code= "LB.Inquiry_period_1" />', odate, FDATE, TDATE, false, 12, null]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
										
									</div>
								</span>
							</div>
							<!-- 信用狀號碼區塊 -->
							<div class="ttb-input-item row">
								<!--信用狀號碼  -->
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.W0085" /></h4>
									</label>								

								</span>
								<input type="text" id="LCNO" name="LCNO" class="text-input" maxlength="20" value="" />
									&nbsp;&nbsp;&nbsp;&nbsp;
									<label>
										<spring:message code="LB.Blank_For_ALL" />
									</label>
							</div>
						</div>
					</form>
<%-- 					<spring:message code="LB.Re_enter" var="cmRest"></spring:message> --%>
<%-- 						<input type="button" name="CMRESET" id="CMRESET" value="${cmRest}" class="ttb-button btn-flat-gray"> --%>
						<spring:message code="LB.Confirm" var="cmConfirm"></spring:message>
						<input type="button" class="ttb-button btn-flat-orange" id="CMSUBMIT" value="${cmConfirm}"/>
					</div>
				</div>
					<div class="text-left">					
						<ol class="description-list list-decimal text-left">
						<p><spring:message code="LB.Description_of_page" /></p>
							<li><span><spring:message code="LB.F_Import_Order_Query_P1_D1" /></span></li>
							<li><span><spring:message code="LB.F_Import_Order_Query_P1_D2" /></span></li>
						</ol>
					</div>
<!-- 				</form> -->
			</section>
			<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->
    <%@ include file="../index/footer.jsp"%>
<!--   Js function -->
    <script type="text/JavaScript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 50);
		// 開始跑下拉選單並完成畫面
		setTimeout("init()", 400);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	function init(){
		// 初始化時隱藏span
		$("#hideblocka").hide();
		$("#hideblockb").hide();
		
		datetimepickerEvent();
		
		getTmr();
		
		initFootable();
		
		$("#formId").validationEngine({
			binded: false,
			promptPosition: "inline"
		});	
		$("#CMSUBMIT").click(function(e){
	 		//打開驗證隱藏欄位
			$("#hideblock_CheckDateScope").show();
			//打開驗證隱藏欄位
			$("#hideblocka").show();
			$("#hideblockb").show();
			//塞值進span內的input
			$("#Monthly_DateA").val($("#FDATE").val());
			$("#Monthly_DateB").val($("#TDATE").val());
			
			e = e || window.event;
			if( $('#CMPERIOD').prop('checked') )
			{
				if(checkTimeRange() == false )
				{
					return false;
				}
			}
			
			if(!$('#formId').validationEngine('validate')){
	        	e.preventDefault();
 			}else{
 				console.log("main submit");
 				$("#formId").validationEngine('detach');
 				initBlockUI();
 				$("#formId").removeAttr("target");
 				$("#formId").attr("action","${__ctx}/FCY/ACCT/f_import_order_query_result");
 	  			$("#formId").submit(); 
 			}		
  		});
		$("#CMRESET").click(function(e){
			console.log("CMRESET submit");
			$("#formId")[0].reset();
			getTmr();
  		});

	}
	
	function getTmr() {
		var today = "${import_order_query.data.TODAY}";
		$('#FDATE').val(today);
		$('#TDATE').val(today);
		$('#odate').val(today);
	}
    
	// 日曆欄位參數設定
	function datetimepickerEvent() {
		$(".FDATE").click(function (event) {
			$('#FDATE').datetimepicker('show');
		});
		$(".TDATE").click(function (event) {
			$('#TDATE').datetimepicker('show');
		});
		jQuery('.datetimepicker').datetimepicker({
			timepicker: false,
			closeOnDateSelect: true,
			scrollMonth: false,
			scrollInput: false,
			format: 'Y/m/d',
			lang: '${transfer}'
		});
	}	
	
	function checkTimeRange()
	{
		var now = Date.now();
		var twoYm = 63115200000;
		var twoMm = 5259600000;
		
		var startT = new Date( $('#FDATE').val() );
		var endT = new Date( $('#TDATE').val() );
		var distance = now - startT;
		var range = endT - startT;
		
		var limitS = new Date(now - twoYm);
		var limitE = new Date(startT.getTime() + twoMm); 
		if(distance > twoYm)
		{
			var m = limitS.getMonth() + 1;
			var time = limitS.getFullYear() + '/' + m + '/' + limitS.getDate();
			// 起始日不能小於
			var msg = '<spring:message code="LB.Start_date_check_note_1" />' + time;
			//alert(msg);
			return false;
		}
		else
		{
			if(range > twoMm)
			{
				var m = limitE.getMonth() + 1;
				var time = limitE.getFullYear() + '/' + m + '/' + limitE.getDate();
				// 終止日不能大於
				var msg = '<spring:message code="LB.End_date_check_note_1" />' + time;
				//alert(msg);
				return false;
			}
		}
		return true;
	}
	//選項，之後來做
	 	function formReset() {
	 		//打開驗證隱藏欄位
			$("#hideblocka").show();
			$("#hideblockb").show();
			//塞值進span內的input
			$("#Monthly_DateA").val($("#FDATE").val());
			$("#Monthly_DateB").val($("#TDATE").val());
			
			if( $('#CMPERIOD').prop('checked') )
			{
				if(checkTimeRange() == false )
				{
					return false;
				}
			}
			if(!$("#formId").validationEngine("validate")){
				e = e || window.event;//forIE
				e.preventDefault();
			}
	 		else{
		 		//initBlockUI();
				if ($('#actionBar').val()=="excel"){
					$("#downloadType").val("OLDEXCEL");
					$("#templatePath").val("/downloadTemplate/f_import_order.xls");
		 		}else if ($('#actionBar').val()=="txt"){
		 			$("#downloadType").val("TXT");
		 			$("#templatePath").val("/downloadTemplate/f_import_order.txt");
		 		}
				//ajaxDownload("${__ctx}/FCY/ACCT/f_import_order_query_ajaxDirectDownload","formId","finishAjaxDownload()");
				$("#formId").attr("target", "");
				$("#formId").attr("action", "${__ctx}/FCY/ACCT/f_import_order_query_ajaxDirectDownload");
				$("#formId").submit();
				$('#actionBar').val("");
	 		}
		}
		function finishAjaxDownload(){
			$("#actionBar").val("");
			unBlockUI(initBlockId);
		}
		

		
 	</script>
</body>
</html>
