<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/checkIdProcess.js"></script>
	
<script type="text/javascript">
	$(document).ready(function() {
		// 將.table變更為footable
		initFootable();
		//初始化驗證碼
		initKapImg();
		
		$("#formId").validationEngine({
			binded : false,
			promptPosition : "inline"
		});
		$("#CMSUBMIT").click(function(e) {
			$("#validate").attr("type","text");
			$("#validate").val($('#PW').val());
			
			var cus = $('#CUSIDN').val();
			var cus1;
			var cus2;
			cus1 = cus.substring(0,1);
			cus2 = cus.substring(1,2);
			var regex =  /^[a-zA-Z]/;
			var re =  /^[8-9]/;
			if(regex.test(cus1) && re.test(cus2) || regex.test(cus1) && regex.test(cus2)){
				$('#CUSIDN').removeClass("validate[required,twIdCheck[CUSIDN]]");
				$('#CUSIDN').addClass("validate[required,funcCall[validate_CheckFxId['',CUSIDN]]]");
			}
			else{
				$('#CUSIDN').removeClass("validate[required,funcCall[validate_CheckFxId['',CUSIDN]]]");
				$('#CUSIDN').addClass("validate[required,twIdCheck[CUSIDN]]");
			}
			
			console.log("submit~~");
			$("#hideblock").show();
			if (!$('#formId').validationEngine('validate')) {
				e.preventDefault();
				$("#validate").attr("type","hidden");
			} else {
				$('#HTRANSPIN').val(pin_encrypt_CN19($('#validate').val()));
				$('#CUSIDN').val(($('#CUSIDN').val()).toUpperCase());
				$("#formId").validationEngine('detach');
				processQuery();
			}
		});
		$("#PREVIOUS").click(function(e) {
			action = '${__ctx}/ONLINE/APPLY/shield_apply_p2'
			$("form").attr("action", action);
			$("#formId").validationEngine('detach');
			$('#CUSIDN').removeClass("validate[required,twIdCheck[CUSIDN]]");
			$('#validate').removeClass("validate[chkpwd[validate]]");
			$('#validate2').removeClass("validate[equals2[validate]]");
			$("form").submit();
		});
		
		var cusidn = '${shield_apply.data.CUSIDN}';
		if(cusidn != null && cusidn != ''){
			$("#CUSIDN").val(cusidn);
		}
	});
	
	// 初始化驗證碼
	function initKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
	}
	
	// 生成驗證碼
	function newKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').click(function() {
			$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
		});
	}
	
	// 驗證碼刷新
	function changeCode() {
		$('input[name="capCode"]').val('');
		// 大小版驗證碼用同一個
		console.log("changeCode...");
		$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

		// 登入失敗解遮罩
		unBlockUI(initBlockId);
	}
	var urihost = "${__ctx}";
	// 通過表單驗證準備送出
	function processQuery(){
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
		console.log("fgtxway: " + fgtxway);
		useCardReader();
	}
	
	/**************************************/
	/*          複寫checkIdProcess         */
	/**************************************/
	//取得卡片主帳號結束
	function getMainAccountFinish(result){
		//成功
		if(result != "false"){
			var cardACN = result;
			$("#ACNNO").val(cardACN);
			if(cardACN.length > 11){
				cardACN = cardACN.substr(cardACN.length - 11);
			}
			var UID = $("#CUSIDN").val();
			
			var uri = urihost+"/COMPONENT/component_without_id_aj";
			var rdata = { ACN: cardACN ,UID: UID };
			fstop.getServerDataEx(uri, rdata, true, CheckIdResult);
		}
		//失敗
		else{
			showTempMessage(500,"<spring:message code= "LB.X1250" />","","MaskArea",false);
		}
	}
	//卡片押碼結束
	function generateTACFinish(result){
		//成功
		if(result != "false"){
			var TACData = result.split(",");
			
			var main = document.getElementById("formId");
			main.iSeqNo.value = TACData[1];
			main.ICSEQ.value = TACData[1];
			main.TAC.value = TACData[2];

			var ACN_Str1 = main.ACNNO.value;
			main.CHIP_ACN.value = ACN_Str1;
			main.OUTACN.value = ACN_Str1;
			main.ACNNO.value = ACN_Str1;
			main.submit();
		}
		//失敗
		else{
			FinalSendout("MaskArea",false);
		}
	}
	

</script>
</head>
<body>
<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Register" /></li>
    <!-- 隨護神盾申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1573" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
	
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			
			<h2><spring:message code="LB.D1573"/></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<div id="step-bar">
				<ul>
				    <li class="finished"><spring:message code="LB.D0176"/></li>
				    <li class="active"><spring:message code="LB.X2066"/></li>
				    <li class=""><spring:message code="LB.X2067"/></li>
			    </ul>
			</div>
			<form id="formId" name="formId" method="post" action="${__ctx}/ONLINE/APPLY/shield_apply_result">
			<!-- 交易機制所需欄位 -->
				<input type="hidden" id="ACN" name="ACN" value="">
				<input type="hidden" id="ISSUER" name="ISSUER" value="">
				<input type="hidden" id="ACNNO" name="ACNNO" value="">
				<input type="hidden" id="TRMID" name="TRMID" value="">
				<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
				<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
				<input type="hidden" id="TAC" name="TAC" value="">
				<input type="hidden" id="HTRANSPIN" name="HTRANSPIN" value="">
			<!-- 舊網銀額外的欄位? -->
				<input type="hidden" name="ACN" value="">
				<input type="hidden" name="UID" value="">
				<input type="hidden" name="OUTACN" value="">
				<input type="hidden" name="CHIP_ACN" value="">
				<input type="hidden" id="CN19VER" name="CN19VER" value="10609"/> <!-- 寫DB用 -->
				<input type="hidden" id="IP" name="IP" value=""/> <!-- 寫DB用 -->
				<div class="main-content-block row">
					<div class="col-12 tab-content">
<!-- 						<div class="CN19-1-header"> -->
<!-- 							<div class="logo"> -->
<%-- 								<img src="${__ctx}/img/logo-1.svg"> --%>
<!-- 							</div> -->
<!-- 							常用網址超連結 -->
<!-- 							<div class="text-right hyperlink"> -->
<!-- 							臺灣企銀首頁 -->
<%-- 								<a href="#" onClick="window.open('https://www.tbb.com.tw');" style="text-decoration: none"> <spring:message code="LB.TAIWAN_BUSINESS_BANK"/> </a> <strong><font color="#e65827">|</font></strong>  --%>
<!-- 								網路ATM -->
<%-- 								<a href="#" onClick="window.open('https://eatm.tbb.com.tw/');" style="text-decoration: none"> <spring:message code="LB.Web_ATM"/> </a><strong><font color="#e65827">|</font></strong> 	 --%>
<!-- 								意見信箱  -->
<%-- 								<a href="#" onClick="window.open('https://www.tbb.com.tw/q3.html?');" style="text-decoration: none"> <spring:message code="LB.Customer_service"/> </a> --%>
<!-- 							</div> -->
<!-- 						</div> -->
						
			            <div class="ttb-input-block">
                            <div class="ttb-message">
                                <p><spring:message code="LB.X2066"/></p>
                            </div>
			            
			            <!-- 身分證字號 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D0581"/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <input type="text" id="CUSIDN" name="CUSIDN" value="" placeholder="<spring:message code="LB.D0025"/>" maxLength="10" class="text-input">
                                    </div>
                                </span>
                            </div>
                        <!-- 請自行設定下載密碼 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D1576"/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                    	<input id="PW" type="password" name="TRANSPIN" value=""  maxLength="8" size="10" class="text-input" style="width:230px">
                                        <input id="validate" type="hidden" size="8" maxlength="10"  class="text-input validate[chkpwd[validate]]" 
											style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										 <span class="input-remarks">※<spring:message code="LB.D1577"/></span>
                                    </div>
                                </span>
                            </div>
			            <!-- 請再次設定下載密碼 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D1578"/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <input type="password" maxLength="8" id="validate2" size="10" name="TRANSPIN2" value="" class="text-input validate[equals2[validate]]" style="width:230px">
                                    </div>
                                </span>
                            </div>
                            
                          <!-- 安控機制 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D1579"/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">

                                        <label class="radio-block" for="ATMTRAN1"><spring:message code="LB.Financial_debit_card"/>
                                            <input type="radio" name="FGTXWAY" id="CMCARD" value="2" checked />
                                            <span class="ttb-radio"></span>
                                        </label>
                                    </div>
                                </span>
                            </div>
                            
                         
                             <!-- 驗證碼 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.Captcha" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                   	 	<input id="capCode" name="capCode" type="text"
											class="text-input input-width-125" maxlength="6" autocomplete="off" >
											
                                        <img name="kaptchaImage" src="" class="verification-img"/>
										
                                        <input type="button" name="reshow" class="ttb-sm-btn btn-flat-gray"
											onclick="changeCode()" value="<spring:message code="LB.Regeneration_1"/>" />
                                        <span class="input-remarks"><spring:message code="LB.X1972"/></span>
                                    </div>
                                </span>
                            </div>
                            
						</div>
						 <!-- 回上一頁 -->
						<input type="button" id="PREVIOUS" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />" />
						<!-- 確定 -->
						<input type="button" name="CMSUBMIT" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm"/>" />
                            
<%-- 						<table class="table" data-toggle-column="first"> --%>
<%-- 							<tbody> --%>
<%-- 								<tr> --%>
<!-- 									請輸入身分證字號 -->
<%-- 							        <td><spring:message code="LB.D0025"/></td> --%>
<%-- 							        <td class="text-left"> --%>
<!-- 										<input type="text" id="CUSIDN" name="CUSIDN" value="" maxlength="10" size="11" class="validate[required,twIdCheck[CUSIDN]]"> -->
<%-- 							        </td> --%>
<%-- 								</tr>     --%>
<%-- 						      	<tr> --%>
<!-- 							      	請自行設定下載密碼 -->
<%-- 							      	<td><spring:message code="LB.D1576"/></td> --%>
<%-- 							      	<td class="text-left"> --%>
<!-- 							      		綁定行動裝置時，用於行動銀行下載憑證時一次性使用。 -->
<%-- 							      		<input id="PW" type="password" maxLength="8" size="10" name="TRANSPIN" value="" >&nbsp;<font color="red">※<spring:message code="LB.D1577"/></font> --%>
<!-- 							      		<span id="hideblock" > -->
<!-- 										驗證用的input -->
<!-- 										<input id="validate" type="hidden" size="8" maxlength="10"  class="text-input validate[chkpwd[validate]]"  -->
<!-- 											style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
<!-- 										</span> -->
<%-- 							      	</td> --%>
<%-- 							     </tr> --%>
<%-- 							      <tr> --%>
<!-- 							      	請再次輸入下載密碼確認  -->
<%-- 							      	<td><spring:message code="LB.D1578"/></td> --%>
<%-- 							      	<td class="text-left"> --%>
<!-- 							      		<input type="password" maxLength="8" id="validate2" size="10" name="TRANSPIN2" value="" class="validate[equals2[validate]]"> -->
<%-- 							      	</td> --%>
<%-- 							      </tr>  --%>
<%-- 						      <tr> --%>
<!-- 						      	安控機制 -->
<%-- 						        <td><spring:message code="LB.D1579"/></td> --%>
<%-- 						        <td class="text-left"> --%>
<!-- 										晶片金融卡 -->
<%-- 										<input type="radio" name="FGTXWAY" id="CMCARD" value="2" /><spring:message code="LB.Financial_debit_card"/> --%>
									
<%-- 						        </td> --%>
<%-- 						      </tr>          --%>
<%-- 						      <tr> --%>
<!-- 						      	驗證碼那一塊的程式 -->
<!-- 						      	圖形驗證碼 -->
<%-- 						      	<td><spring:message code="LB.D1581"/></td> --%>
<%-- 						      	<td class="text-left"> --%>
<!-- 									<span class="input-block"> -->
<%-- 										<spring:message code="LB.Captcha" var="labelCapCode" /> --%>
<!-- 										<img name="kaptchaImage" src="" /> -->
<!-- 										<input id="capCode" name="capCode" type="text" -->
<!-- 											class="text-input" maxlength="6" autocomplete="off" > -->
<!-- 										※英文不分大小寫，限半型字 -->
<%-- 										 <font color="red"><spring:message code="LB.Captcha_refence"/></font> --%>
<!-- 										 重新產生驗證碼 -->
<!-- 										<input type="button" name="reshow" class="ttb-sm-btn btn-flat-orange" -->
<%-- 											onclick="changeCode()" value="<spring:message code="LB.Regeneration_1"/>" /> --%>
<!-- 									</span> -->
<%-- 						      	</td> --%>
<%-- 						      </tr> --%>
<%-- 							</tbody> --%>
<%-- 						</table> --%>
						
					</div>
				</div>
				<div class="text-left">
					
					<ol class="list-decimal text-left description-list">
					<p><spring:message code="LB.Description_of_page" /></p>
            			<li><spring:message code="LB.shield_apply_P4_D2"/></li>
            			<li>
            				<spring:message code="LB.shield_apply_P4_D3"/><br>
            					&nbsp;&nbsp;&nbsp;&nbsp;(1) <spring:message code="LB.shield_apply_P4_D31"/><br>
								&nbsp;&nbsp;&nbsp;&nbsp;(2)<spring:message code="LB.shield_apply_P4_D32"/><br>
								&nbsp;&nbsp;&nbsp;&nbsp;(3)<spring:message code="LB.shield_apply_P4_D33"/>
						</li>
					</ol>
 				</div>
			</form>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>