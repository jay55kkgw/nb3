<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>

	<script type="text/javascript">
	var notCheckIKeyUser = true; // 不驗證是否是IKey使用者
	var myobj = null; // 自然人憑證用
	var urihost = "${__ctx}";
	$(document).ready(function() {
		// HTML載入完成後0.1秒開始遮罩
		setTimeout("initBlockUI()", 100);

		// 將.table變更為footable ??
		initFootable(); 
		
		init();
		
		// 建立轉入帳號下拉選單
			creatOutACNO();

		// 過0.5秒解遮罩
		setTimeout("unBlockUI(initBlockId)", 800);
	});
	
	/**
	 * 初始化BlockUI
	 */
	function initBlockUI() {
		initBlockId = blockUI();
	}

//		把下拉選單選中的文字塞到隱藏欄位
	function changeDisplay(){
		console.log("val"+$("#DPAGACNO").find(":selected").val())
		console.log("val1>>"+$("#DPAGACNO").val())
		$("#DPAGACNO_TEXT").val($("#DPAGACNO").find(":selected").text());
	}
	
	
	
    function init(){

    	$('input[type=radio][name=binddefault]').change(function() {
            	$("#validate_binddefault").val(this.value);
        });
    	
    	//上一頁按鈕
		$("#pageback").click(function() {

			var action = '${pageContext.request.contextPath}' + '${previous}'
			$('#back').val("Y");
			$('#jsondc').attr('disabled', true);
			$("#formId").attr("action", action);
			initBlockUI();
			$("#formId").validationEngine('detach');
			$("#formId").submit();

		});
        
		$("#formId").validationEngine({binded: false,promptPosition: "inline"});
    	$("#CMSUBMIT").click(function(e){			
			console.log("submit~~");
			
				if (!$('#formId').validationEngine('validate')){
		        	e.preventDefault();
	 			}else{
	 				console.log("submit~~");
	 				$("#formId").validationEngine('detach');
	 				initBlockUI();
	   				var action = '${__ctx}/PNONE/CONFIG/update_change_confirm';
	   				$('#formId').attr("action", action);
	    			unBlockUI(initBlockId);
	    			$('#LOGINPIN').val("");
	    			$('#CKLOGINPIN').val("");
	    			$("#formId").validationEngine({binded: false,promptPosition: "inline"});
	    			$('#formId').submit();
	 			}
			
		});
    }	

//	建立約定轉出帳號下拉選單
	function creatOutACNO(){
		var options = { keyisval:false ,selectID:'#DPAGACNO'};
		uri = '${__ctx}'+"/NT/ACCT/TRANSFER/getOutAcno_aj"
		console.log("getSelectData>>" + uri);
		rdata = {type: 'acno' };
		console.log("rdata>>" + rdata);
		data = fstop.getServerDataEx(uri,rdata,false);
		console.log("data>>", data);
		if(data !=null && data.result == true ){
			fstop.creatSelect(data.data,options);
		}else{
			errorBlock(
					null, 
					null,
					['<spring:message code= 'LB.Alert004' />'], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
		}
	}
    
	</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	<!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
	<!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 手機門號收款帳號設定    -->
			<li class="ttb-breadcrumb-item active" aria-current="page">手機門號收款帳號設定</li>
		</ol>
	</nav>
	
	<!-- menu、登出窗格 -->
	<div class="content row">
		
		<c:if test="${sessionScope.cusidn != null}">
			<%@ include file="../index/menu.jsp"%>
		</c:if>
		
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					變更轉入帳號
				</h2>
				
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
<!-- 				<div id="step-bar"> -->
<!-- 					<ul> -->
<%-- 						<li class="active"><spring:message code="LB.Enter_data" /></li> --%>
<%-- 						<li class=""><spring:message code="LB.Confirm_data" /></li> --%>
<%-- 						<li class="">  <spring:message	code="LB.Transaction_complete" /></li> --%>
<!-- 					</ul> -->
<!-- 				</div> -->
				
				<form method="post" id="formId">
					<input type="hidden" id="DPAGACNO_TEXT" name = "DPAGACNO_TEXT" value="${requestScope.back_data.DPAGACNO_TEXT }">
					<input type="hidden" name="HLOGINPIN" id="HLOGINPIN" value="">
					<input type="hidden" name="CHIP_ACN" id="CHIP_ACN" value="">
					<input type="hidden" name="UID" id="UID" value="">
					<input type="hidden" name="ACN" id="ACN" value="">
					<input type="hidden" name="ISSUER" id="ISSUER" value="">
					<input type="hidden" name="ACNNO" id="ACNNO" value="">
					<input type="hidden" name="OUTACN" id="OUTACN" value="">
					<input type="hidden" name="iSeqNo" id="iSeqNo" value="">
					<input type="hidden" name="ICSEQ" id="ICSEQ" value="">
					<input type="hidden" name="TAC" id="TAC" value="">
					<input type="hidden" name="TRMID" id="TRMID" value="">
					<input type="hidden" name="TRANSEQ" id="TRANSEQ" value="2500">
					<input type="hidden" name="oldtxacn" id="oldtxacn" value="${result_data.data.oldtxacn}">
					<input type="hidden" name="mobilephone" id="mobilephone" value="${result_data.data.mobilephone}">
					<input type="hidden" name="oldbind" id="oldbind" value="${result_data.data.oldbind}">
					<input type="hidden" id="TOKEN" name="TOKEN" value="${sessionScope.transfer_confirm_token}" /><!-- 防止重複交易 -->
					
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
							<!-- 手機號碼 -->
							<div class="ttb-input-block">
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
<%-- 											手機號碼 --%>
手機號碼
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${result_data.data.mobilephone}
										</div>
									</span>
								</div>
							</div>
							
	                       <div class="ttb-input-block">
		                    	<div class="ttb-input-item row">
		                             <span class="input-title">
		                                 <label>
		                                    綁定收款帳號
		                                 </label>
		                              </span>
		                              <span class="input-block">
		                                <div class="ttb-input">
										<select id="DPAGACNO" name="DPAGACNO" class="custom-select select-input half-input" onchange="changeDisplay()" value="">
										
										</select>
		                                </div>
		                              </span>
			                	</div>
							</div>
							
							
						 <div class="ttb-input-block">
		                    	<div class="ttb-input-item row">
		                             <span class="input-title">
		                                 <label>
		                                 同意作為預設收款帳號
		                                 </label>
		                              </span>
		                              <span class="input-block">
		                                <div class="ttb-input">
											<input type="radio" id="AGREE" name="binddefault" value="Y" checked>
												同意預設<br> <font color='red'>您的朋友只要輸入您的手機門號，無需輸入收款行，即可轉帳至您綁定之帳號。</font>
											<br>
											<br>
											<input type="radio" id="DISAGREE" name="binddefault" value="N" >
		                                		暫不預設<br><font color='red'>您的朋友需同時 輸入您的手機門號及收款行(050)，方可轉帳至您綁定之帳號。</font>
											<span id="hideblocka" name="hideblock">
											<input id="validate_binddefault" name="validate_binddefault" type="text" class="text-input validate[required]" 
											style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" autocomplete="off" value = "Y"/>
		                           		 	 </span>
		                                </div>
		                              </span>
			                	</div>
							</div>
							<input type="button" id="pageback" class="ttb-button btn-flat-gray" value="回上頁"/>
						<!-- 確定 -->
						<input type="button" id="CMSUBMIT" value="確認" class="ttb-button btn-flat-orange"/>
						</div>
					</div>
												<!-- 說明 -->
					<ol class="description-list list-decimal">
						<li>若您變更基本資料之手機門號，原已設定之綁定帳號將同時註銷，需重新申請。</li>
						<li>如欲變更手機門號，請依下列方式辦理變更基本資料之手機號碼：<br>
							(1)臨櫃：攜帶雙證件及印鑑親赴任一營業單位辦理。<br>
							(2)網路銀行：登入網路銀行後於個人服務->通訊資料變更->異動全行往來業務之基本資料項下使用晶片金融卡+讀卡機進行手機門號變更。<br>
							(3)行動銀行：登入行動銀行後於更多->設定->通訊地址與電話變更項下使用安控機制進行手機門號變更。
						</li>
			        </ol>
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>