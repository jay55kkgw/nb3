<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 通訊資料變更     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0359" /></li>
    <!-- 往來帳戶及信託業務通訊地址/電話變更     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0370" /></li>
		</ol>
	</nav>


		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 --> 
 		<div class="content row">
 		   <%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
 		
 		<main class="col-12">	

		<!-- 		主頁內容  -->

			<section id="main-content" class="container">
		
				<h2><spring:message code="LB.D0370"/></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<div class="main-content-block row">
					<div class="col-12 tab-content printClass">
					<div class="ttb-message">
					<span><spring:message code="LB.D0384"/></span></div>
					
						<c:set var="dataSet" value="${communication_data_plural.data}" />							
						<div class="ttb-input-block">
							<!-- 通訊地址 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.D0380"/></h4>
										</label>
									</span>
									<span class="input-block">
										<!-- 郵遞區號 -->
										<div class="ttb-input">
											<spring:message code="LB.D0061"/>:
											${dataSet.POSTCOD2}
											${dataSet.CTTADR}											
										</div>
									</span>
								</div>

								<!-- 通訊電話 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.D0383"/></h4>
										</label>
									</span>
									<span class="input-block">
											<!--公司電話 -->
										<div class="ttb-input">
											<spring:message code="LB.D0094"/>:
											${dataSet.ARACOD2}
											-
											${dataSet.TELNUM2}
										<c:if test="${not empty dataSet.TELEXT}">
											<spring:message code="LB.D0095"/>：
											${dataSet.TELEXT}
										</c:if>
										</div>
										<!-- 居住電話 -->
										<div class="ttb-input">
											<spring:message code="LB.D0065"/>:
											${dataSet.ARACOD}
											-
											${dataSet.TELNUM}
										</div>
										<!-- 行動電話 -->
										<div class="ttb-input">
											<spring:message code="LB.D0069"/>:
											${dataSet.MOBTEL}
										</div>
									</span>
								</div>
								
								<!-- 變更信用卡帳單地址（公司地址） -->
								<c:if test="${communication_data900.data.SMSA == '0003'}">
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4><spring:message code="LB.X2507"/></h4>
											</label>
										</span>
										<span class="input-block">
											<!-- 郵遞區號 -->
											<div class="ttb-input">
												<spring:message code="LB.D0061"/>:
												${communication_data900.data.POSTCOD}
												${communication_data900.data.ADDRESS}
											</div>
										</span>
									</div>
								</c:if>
<%-- 					<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/> --%>
					</div>
				</div>
			</div>
				<div class="text-left">
					<ol class="description-list list-decimal">
						<p><spring:message code="LB.Note" /> :</p>					
						<li><spring:message code="LB.Communication_Data_P5_D1"/></li>
						<li><spring:message code="LB.Communication_Data_P5_D2"/></li>
					</ol>
				</div>	
			</section>			
			<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
	
	<script type="text/javascript">
			$(document).ready(function(){
				// HTML載入完成後開始遮罩
				setTimeout("initBlockUI()", 50);
				// 開始跑下拉選單並完成畫面
				setTimeout("init()", 400);
				// 解遮罩
				setTimeout("unBlockUI(initBlockId)", 500);
				showAlert();
			});
			
			function init(){
// 				$("#printbtn").click(function(){
// 					var params = {
// 						"jspTemplateName":"predesignated_account_cancellation_print",
// 						"jspTitle":"取消約定轉入帳號",
// 						"CMQTIME":"${dataSet.CMQTIME}",
// 						"MSG":"${dataSet.MSG}",
// 						"CCY":"${dataSet.CCY}",
// 						"CUSIDN":"${dataSet.CUSIDN}",
// 						"BNKCOD":"${dataSet.BNKCOD}",
// 						"ACN":"${dataSet.ACN}"
// 					};
// 					openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
// 				});
			}
			
			function showAlert(){
				var err = "${ERROR_900.data.err900}";
				if(err != null && err == "true"){
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X2510' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				}
			}
	</script>
</body>
</html>