<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<!-- 交易機制所需JS -->
<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>

<script type="text/javascript">
	var idgatesubmit= $("#formId");
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
		initFootable();
		$('#but_Agree').prop('checked', false);
		if('${averaging_alter_confirm.data.DATE_06}' != ("")){
			$("#DATE_06").prop('checked',true);
		}
		if('${averaging_alter_confirm.data.DATE_16}' != ""){
			$("#DATE_16").attr('checked',true);
		}
		if('${averaging_alter_confirm.data.DATE_26}' != ""){
			$("#DATE_26").attr('checked',true);
		}
		$("#formId").validationEngine({
			binded: false,
			promptPosition: "inline"
		});
		chaBlock();
		// 確認鍵 click
// 		submit();
		$("#CMSUBMIT").click( function(e) {
			console.log("submit~~");
			e = e || window.event;
			
         	if(!$('#formId').validationEngine('validate') && $('input[name="FGTXWAY"]:checked').val() == 0){
	        	e.preventDefault();
 			}else{
// 				$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
 				$("#formId").validationEngine('detach');
    			processQuery();
 			}
		});
		//上一頁按鈕
		$("#CMBACK").click(function() {
			initBlockUI();
			fstop.getPage('${pageContext.request.contextPath}'+'/GOLD/AVERAGING/averaging_alter','', '');
		});	
	}
	
	// 確認鍵 Click
	function submit() {
	}
	
	// 通過表單驗證準備送出
	function processQuery(){
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
		console.log("fgtxway: " + fgtxway);
		// 交易機制選項
		switch(fgtxway) {
			case '0':
				// 遮罩
	         	initBlockUI();
	         	$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
				$("form").submit();
				break;
			case '1':
				// IKEY
				useIKey();
			
				break;
			case '7'://IDGATE認證		 
               idgatesubmit= $("#formId");		 
               showIdgateBlock();		 
               break;
			default:
				//alert("nothing...")
				errorBlock(
						null, 
						null,
						["nothing..."], 
						'<spring:message code= "LB.Quit" />', 
						null
				);
		}
	}
	
	// 判斷交易機制決定顯不顯示驗證碼區塊
	function chaBlock() {
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
			
			
			switch(fgtxway) {
			case '0':
				$('#CMSSL').attr('checked',true);
				$('#CMPASSWORD').addClass("validate[required]");
				break;
			case '1':
				$('#CMSSL').removeAttr('checked');
				$('#CMPASSWORD').removeClass("validate[required]");
		    	break;
			case '7':
				$('#CMSSL').removeAttr('checked');
				$('#CMPASSWORD').removeClass("validate[required]");
		    	break;
		    	
			default:
				$('#CMSSL').removeAttr('checked');
				$('#CMPASSWORD').removeClass("validate[required]");
		}
	}

</script>
</head>
<body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 定期定額交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1552" /></li>
    <!-- 定期定額變更     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1564" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 黃金定期定額變更 -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.W1565"/></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form id="formId" method="post" action="${__ctx}/GOLD/AVERAGING/averaging_alter_result">
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
                             <div class="ttb-message">
								<!-- 請確定變更資料 -->
                                <span><spring:message code= "LB.W1585" /></span>
                            </div>
							
							<c:set value="${averaging_alter_confirm.data}" var="dataList"></c:set>
                             <!--約定扣款帳號-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                	<label>
										<!-- 約定扣款帳號 -->
                                        <h4><spring:message code= "LB.W1539" /></h4>
                                    </label></span>
                                <span class="input-block">
									<div class="ttb-input">
                                       <span>${dataList.SVACN}</span>
                                     </div>
                                </span>
                            </div>					
                             <!--黃金存摺帳號-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                	<label>
										<!-- 黃金存摺帳號 -->
                                        <h4><spring:message code= "LB.D1090" /></h4>
                                    </label></span>
                                <span class="input-block">
                                	<div class="ttb-input">
                                  		<span>${dataList.ACN}</span>
                                    </div>
                                </span>
                            </div>

                              
							 <!--原每月投資日/金額-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>

                                        <h4><spring:message code= "LB.W1581" /></h4>
                                    </label></span>
                                <span class="input-block">
									<div class="ttb-input">
										<!-- 日、新臺幣、元、扣款狀態 -->
										<span>
											06<spring:message code= "LB.Day" /> 
											<spring:message code="LB.NTD" />&nbsp;${dataList.AMT_06_O1}&nbsp;<spring:message code= "LB.Dollar" /> &nbsp;
											<c:if test="${!dataList.FLAG_06_O1.equals('')}">
												<spring:message code= "${dataList.FLAG_06_O1}" />
											</c:if>
										</span>
                                     </div>
									<div class="ttb-input">
										<!-- 日、新臺幣、元、扣款狀態 -->
										<span>
											16<spring:message code= "LB.Day" /> 
											<spring:message code="LB.NTD" />&nbsp;${dataList.AMT_16_O1}&nbsp;<spring:message code= "LB.Dollar" /> &nbsp;
											<c:if test="${!dataList.FLAG_16_O1.equals('')}">
												<spring:message code= "${dataList.FLAG_16_O1}" />
											</c:if>
										</span>
                                     </div>
									<div class="ttb-input">
										<!-- 日、新臺幣、元、扣款狀態 -->
										<span>
											26<spring:message code= "LB.Day" /> 
											<spring:message code="LB.NTD" />&nbsp;${dataList.AMT_26_O1}&nbsp;<spring:message code= "LB.Dollar" /> &nbsp;
											<c:if test="${!dataList.FLAG_26_O1.equals('')}">
												<spring:message code= "${dataList.FLAG_26_O1}" />
											</c:if>
										</span>
                                     </div>
                                </span>
                            </div>
							
							 <!--變更後每月投資日/金額-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code= "LB.W1582" /></h4>
                                    </label></span>
                                <span class="input-block">
									<div class="ttb-input">
										<label class="check-block" for="DATE_06"><input value="1" name="DATE_06" id="DATE_06" type="checkbox" disabled>
											<span class="ttb-check"></span>
											<!-- 日、新臺幣 -->
											<span>06<spring:message code= "LB.Day" /> <spring:message code="LB.NTD" />&nbsp;
												<!-- 元、扣款狀態 -->
												<c:if test = "${empty dataList.DATE_06}">
													${dataList.AMT_06_O1}&nbsp;<spring:message code= "LB.Dollar" /> 
													<c:if test="${!dataList.FLAG_06_O1.equals('')}">
														&nbsp;<spring:message code= "${dataList.FLAG_06_O1}" />
													</c:if>
												</c:if>
												<!-- 元、扣款狀態 -->
												<c:if test = "${not empty dataList.DATE_06}">
													${dataList.AMT_06_N1}&nbsp;<spring:message code= "LB.Dollar" /> 
													<c:if test="${!dataList.PAYSTATUS_061.equals('')}">
													&nbsp;<spring:message code= "${dataList.PAYSTATUS_061}" />
													</c:if>
												</c:if>
											</span>
										</label>
									</div>
									
									<div class="ttb-input">
										<label class="check-block" for="DATE_16"><input value="1" name="DATE_16" id="DATE_16" type="checkbox" disabled>
											<span class="ttb-check"></span>
											<!-- 日、新臺幣 -->
											<span>16<spring:message code= "LB.Day" /> <spring:message code="LB.NTD" />&nbsp;
												<!-- 元、扣款狀態 -->
												<c:if test = "${empty dataList.DATE_16}">${dataList.AMT_16_O1}&nbsp;<spring:message code= "LB.Dollar" /> 
													<c:if test="${!dataList.FLAG_16_O1.equals('')}">
													&nbsp;<spring:message code= "${dataList.FLAG_16_O1}" />
													</c:if>
												</c:if>
												<!-- 元、扣款狀態 -->
												<c:if test = "${not empty dataList.DATE_16}">${dataList.AMT_16_N1}&nbsp;<spring:message code= "LB.Dollar" /> 
													<c:if test="${!dataList.PAYSTATUS_161.equals('')}">
													&nbsp;<spring:message code= "${dataList.PAYSTATUS_161}" />
													</c:if>
												</c:if>
											</span>
										</label>
									</div>
									
									<div class="ttb-input">
										<label class="check-block" for="DATE_26"><input value="1" name="DATE_26" id="DATE_26" type="checkbox" disabled>
											<span class="ttb-check"></span>
											<!-- 日、新臺幣 -->
											<span>26<spring:message code= "LB.Day" /> <spring:message code="LB.NTD" />&nbsp;
												<!-- 元、扣款狀態 -->
												<c:if test = "${empty dataList.DATE_26}">${dataList.AMT_26_O1}&nbsp;<spring:message code= "LB.Dollar" /> 
													<c:if test="${!dataList.FLAG_26_O1.equals('')}">
														&nbsp;<spring:message code= "${dataList.FLAG_26_O1}" />
													</c:if>
												</c:if>
												<!-- 元、扣款狀態 -->
												<c:if test = "${not empty dataList.DATE_26}">${dataList.AMT_26_N1}&nbsp;<spring:message code= "LB.Dollar" /> 
													<c:if test="${!dataList.PAYSTATUS_261.equals('')}">
														&nbsp;<spring:message code= "${dataList.PAYSTATUS_261}" />
													</c:if>
												</c:if>
											</span>
										</label>
									</div>
                                </span>
                            </div>
							
							<!--交易機制-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.Transaction_security_mechanism" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
                                       <label class="radio-block"><spring:message	code="LB.SSL_password" /> 
                                            <input type="radio" name="FGTXWAY" id="CMSSL" value="0" checked onclick="chaBlock()">
                                            <span class="ttb-radio"></span>
                                        </label>
                                    </div>
									<div class="ttb-input">
										<input type="password" class="text-input" name="CMPASSWORD" id="CMPASSWORD" autocomplete="off" size="8" maxlength="8" value="">
									</div>
									<!-- 使用者是否可以使用IKEY -->
									<c:if test = "${sessionScope.isikeyuser}">
                                    	<div class="ttb-input">
                                       		<label class="radio-block"><spring:message code="LB.Electronic_signature" />
                                           		<input type="radio" name="FGTXWAY" id="CMIKEY" value="1" onclick="chaBlock()">
                                           		<span class="ttb-radio"></span>
                                        	</label>
                                    	</div>
                                    </c:if> 
                                    <div class="ttb-input" name="idgate_group" style="display:none" onclick="chaBlock()">		 
                                       <label class="radio-block">裝置推播認證(請確認您的行動裝置網路連線是否正常，及推播功能是否已開啟)
	                                       <input type="radio" id="IDGATE" 	name="FGTXWAY" value="7"> 
	                                       <span class="ttb-radio"></span>
                                       </label>		 
                                   </div>            
                                </span>
                            </div>
							
                        </div>
                        <input class="ttb-button btn-flat-gray" type="button" name="CMBACK" id="CMBACK" value="<spring:message code="LB.Back_to_previous_page" />" /> 
                        <input class="ttb-button btn-flat-gray" name="CMRESET" type="reset" value="<spring:message code="LB.Re_enter" />" />
                        <input class="ttb-button btn-flat-orange" id="CMSUBMIT" name="CMSUBMIT" type="button" value="<spring:message code="LB.Confirm" />" />
                         <!-- ikey -->
						<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
						<input type="hidden" id="jsondc" name="jsondc" value='${dataList.jsondc }'>
						<input type="hidden" id="CMTRANPAGE" name="CMTRANPAGE" value="1">
						<input type="hidden" id="ISSUER" name="ISSUER" value="">
						<input type="hidden" id="ACNNO" name="ACNNO" value="">
						<input type="hidden" id="TRMID" name="TRMID" value="">
						<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
						<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
						<input type="hidden" id="TAC" name="TAC" value="">
                        <input type="hidden" id="PINNEW" name="PINNEW" value="">
                        
                        <input type="hidden" id="ADOPID" name="ADOPID" value="N09302">
                        <input type="hidden" id="TRNCOD" name="TRNCOD" value="02">
                        <input type="hidden" id="SVACN" name="SVACN" value="${dataList.SVACN}">
                        <input type="hidden" id="ACN" name="ACN" value="${dataList.ACN}">
                        <input type="hidden" id="DPMYEMAIL" name="DPMYEMAIL" value="${sessionScope.dpmyemail}">
                        <input type="hidden" id="AMT_06_N" name="AMT_06_N" value="${dataList.AMT_06_N}">
                        <input type="hidden" id="AMT_16_N" name="AMT_16_N" value="${dataList.AMT_16_N}">
                        <input type="hidden" id="AMT_26_N" name="AMT_26_N" value="${dataList.AMT_26_N}">
                        <input type="hidden" id="PAYSTATUS_06" name="PAYSTATUS_06" value="${dataList.PAYSTATUS_06}">
                        <input type="hidden" id="PAYSTATUS_16" name="PAYSTATUS_16" value="${dataList.PAYSTATUS_16}">
                        <input type="hidden" id="PAYSTATUS_26" name="PAYSTATUS_26" value="${dataList.PAYSTATUS_26}">
                        <c:if test="${not empty dataList.DATE_06}">
                        	<input type="hidden" id="DATE_06" name="DATE_06" value="${dataList.DATE_06}">
                        </c:if>
                        <c:if test="${not empty dataList.DATE_16}">
                        	<input type="hidden" id="DATE_16" name="DATE_16" value="${dataList.DATE_16}">
                        </c:if>
                        <c:if test="${not empty dataList.DATE_26}">
                        	<input type="hidden" id="DATE_26" name="DATE_26" value="${dataList.DATE_26}">
                        </c:if>
                        <input type="hidden" id="AMT_06_O" name="AMT_06_O" value="${dataList.AMT_06_O1}">
                        <input type="hidden" id="AMT_16_O" name="AMT_16_O" value="${dataList.AMT_16_O1}">
                        <input type="hidden" id="AMT_26_O" name="AMT_26_O" value="${dataList.AMT_26_O1}">
                        <input type="hidden" id="FLAG_06_O" name="FLAG_06_O" value="${dataList.FLAG_06_O1}">
                        <input type="hidden" id="FLAG_16_O" name="FLAG_16_O" value="${dataList.FLAG_16_O1}">
                        <input type="hidden" id="FLAG_26_O" name="FLAG_26_O" value="${dataList.FLAG_26_O1}">
                    </div>
                </div>
				</form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>