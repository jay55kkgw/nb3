<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	    <script type="text/javascript">
        $(document).ready(function () {
            //initFootable(); // 將.table變更為footable 
            init();
            setTimeout("initDataTable()",100);
        });

        function init() {
        	//若前頁有帶acn，預設為此acn
			var getacn = '${result_data.data.getAcn}';
			if(getacn != null && getacn != ''){
				$("#getAcn").val(getacn);
			}
        }
        
	 	//到單查詢按鈕
	 	function delete_submit(Account,CarId,CarKind,ZoneCode,RegistDate,CarKind_str,ZoneCode_str){
			$("#Account").val(Account);
			$("#CarId").val(CarId);
			$("#CarKind").val(CarKind);
			$("#ZoneCode").val(ZoneCode);
			$("#RegistDate").val(RegistDate);
			$("#CarKind_str").val(CarKind_str);
			$("#ZoneCode_str").val(ZoneCode_str);
			var action = '${__ctx}/PARKING/FEE/parking_withholding_modify_delete';
			$("#formId").attr("action", action);
			initBlockUI();
			$("#formId").submit();
	 	}
	 	//信用狀明細查詢按鈕
	 	function update_submit(Account,CarId,CarKind,ZoneCode,RegistDate,CarKind_str,ZoneCode_str){
	 		$("#Account").val(Account);
			$("#CarId").val(CarId);
			$("#CarKind").val(CarKind);
			$("#ZoneCode").val(ZoneCode);
			$("#RegistDate").val(RegistDate);
			$("#CarKind_str").val(CarKind_str);
			$("#ZoneCode_str").val(ZoneCode_str);
			var action = '${__ctx}/PARKING/FEE/parking_withholding_modify_update';
			$("#formId").attr("action", action);
			initBlockUI();
			$("#formId").submit();
	 	}
    </script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 自動扣繳服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2360" /></li>
    <!-- 停車費     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.D0330" /></li>
    <!-- 資料維護     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0285" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!-- 商業信用狀明細(未輸入) -->
					<!-- <spring:message code="LB.NTD_Demand_Deposit_Detail" /> -->
					<spring:message code="LB.X0285"/>
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <!-- 下拉式選單-->
                <form id="formId" method="post">
                	<input type="hidden" id="back" name="back" value="">
				    <input type="hidden" name="Account" id="Account" >
				    <input  type="hidden" name="CarId" id="CarId" >
				    <input  type="hidden" name="CarKind" id="CarKind" >
				    <input  type="hidden" name="ZoneCode" id="ZoneCode" >
				    <input  type="hidden" name="RegistDate" id="RegistDate" >
				    <input  type="hidden" name="Phone" id="Phone" value="${result_data.data.Phone }">
				    <input  type="hidden" name="Email" id="Email" value="${result_data.data.Email }">
				    <input  type="hidden" name="CarKind_str" id="CarKind_str" >
				    <input  type="hidden" name="ZoneCode_str" id="ZoneCode_str" >
				    <input  type="hidden" name="getAcn" id="getAcn" value="">
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
                            <ul class="ttb-result-list">
                                <li>
                                    <!-- 查詢時間 -->
                                    <p>
                                       	<spring:message code="LB.X0279"/>：
										<spring:message code="LB.X0286"/>
                                    </p>
                                    <!-- 查詢期間 -->
                                    <p>
										<spring:message code="LB.Data_time"/>：
                                        ${result_data.data.CMQTIME }
                                    </p>
                                    <!-- 資料總數 : -->
                                    <p>
										 <spring:message code="LB.D0020"/>：
										 <spring:message code="LB.X0278"/>
                                    </p>
                                </li>
                            </ul>
                            <!-- 表格區塊 -->
                        
                            <table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
                                <thead>
                                    <tr>
                                    	<th><spring:message code="LB.D0342"/></th>
									      <th><spring:message code="LB.D0343"/></th>
									      <th><spring:message code="LB.X0287"/></th>
									      <th><spring:message code="LB.X0288"/></th>
									      <th><spring:message code="LB.D0168"/></th>
									      <th><spring:message code="LB.D1098"/></th>
									      <th><spring:message code="LB.X0289"/></th>
									      <th><spring:message code="LB.D0415"/></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="dataList" items="${ result_data.data.REC }">
                                        <tr>
                                            <td>${dataList.CarId }</td>
                                            <td align="center">${dataList.CarKind_str }</td>
                                            <td align="center">${dataList.ZoneCode_str }</td>
                                            <td align="center">${dataList.RegistDate }</td>
                                            <td align="center">${dataList.MaskAccount }</td>
                                            <td align="center">${dataList.Status_str }</td>
                                            <td align="center">
					                            <input type="button" class="ttb-sm-btn btn-flat-orange" value="<spring:message code="LB.X0290"/>" 
			                            				onclick="delete_submit('${dataList.Account}','${dataList.CarId }','${dataList.CarKind }',
                         														'${dataList.ZoneCode}','${dataList.RegistDate }','${dataList.CarKind_str }','${dataList.ZoneCode_str }')" />
					                        </td>
                                            <td align="center">
					                            <input type="button" class="ttb-sm-btn btn-flat-orange" value="<spring:message code="LB.X0290"/>" 
					                            		onclick="update_submit('${dataList.Account}','${dataList.CarId }','${dataList.CarKind }',
                          														'${dataList.ZoneCode}','${dataList.RegistDate }','${dataList.CarKind_str }','${dataList.ZoneCode_str }')"  />
											</td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                            
	                        <!--button 區域 -->
	                        <div>
	                            <!-- 列印  -->
<%-- 	                            <spring:message code="LB.Print" var="printbtn"></spring:message> --%>
<%-- 	                            <input type="button" id="printbtn" value="${printbtn}" class="ttb-button btn-flat-orange" /> --%>
	                            <!-- 繼續查詢 -->
<%-- 	                            <c:if test="${ base_result.data.TOPMSG != 'OKLR' }"> --%>
<!-- 		                            <spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message> -->
<!-- 		                            <input type="button" name="CMCONTINU" id="CMCONTINU" value="繼續查詢" class="ttb-button btn-flat-orange"> -->
<%-- 	                            </c:if> --%>
	                            <!--回上頁 -->
<%-- 	                            <spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message> --%>
<%-- 	                            <input type="button" name="CMBACK" id="CMBACK" value="${cmback}" class="ttb-button btn-flat-gray"> --%>
	                        </div>
                        <!--                     button 區域 -->
                        </div>  
                    </div>
<!--                     <div class="text-left"> -->
<!--                         		說明： -->
<%-- 						<spring:message code="LB.Description_of_page"/>: --%>
<!--                         <ol class="list-decimal text-left"> -->
<!--                             <li> -->
<!--                             	<spring:message code="LB.Demand_deposit_detail_P2_D1"/> -->
<!--                             	查詢結果超出每頁可顯示最大筆數(100筆)，如欲顯示其餘筆數資料，請按 "繼續查詢"。 -->
<!--                             </li> -->
<!--                         </ol> -->
<!--                     </div> -->
                </form>

	
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>