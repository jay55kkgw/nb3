<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	//alert('${transfer_data.data}')
	init();
});
function init(){
	$("#formId").validationEngine({binded:false,promptPosition: "inline" });
}

	
	function check(intvar) {
		console.log("submit~~");

		if (intvar == 1) {
			//導向email設定頁
			$('#type').val("DPSETUPE");
			$('#formId').attr("action",
					"${__ctx}/PERSONAL/SERVING/mail_setting_choose");
			$('#CMPASSWORD').removeClass(
					"validate[required,custom[onlyLetterNumber]]");
			$("form").submit();
			initBlockUI();
		} else {
			if ($('#DPMYEMAIL').val() == '') {
				//alert('<spring:message code= "LB.Alert072" />');
				errorBlock(null, null,
						[ "<spring:message code= 'LB.Alert072' />" ],
						'<spring:message code= "LB.Quit" />', null);

				$('#CMSUBMIT2').focus();
				return false;
			} else {
				if (!$('#formId').validationEngine('validate')) {
					
				} else {
					$('#CMPASSWORD').addClass("validate[required,custom[onlyLetterNumber]]");
					$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
					$('#formId').attr("action","${__ctx}/ELECTRONIC/CHECKSHEET/elec_bill_resent_result");
					$("#formId").validationEngine('detach');
					$("form").submit();
					initBlockUI();
				}
			}
			
		}
	}
</script>

</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 電子對帳單     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1396" /></li>
    <!-- 電子對帳單申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0270" /></li>
		</ol>
	</nav>

	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">

		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
	

		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2>
				<spring:message code="LB.D0285" /><!-- 補發電子帳單 -->
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" action="" method="post">
			<input type="hidden" name="TYPE" value="02">
			<input type="hidden" id="PINNEW" name="PINNEW"  value="">
			<input type="hidden" id="type" name="type" value="">
			<input type="hidden" id="DPMYEMAIL" name="DPMYEMAIL" value="${sessionScope.dpmyemail}">
				<div class="main-content-block row">
					<div class="col-12 tab-content" id="nav-tabContent">
						<!-- 主頁內容  -->
						<div class="ttb-input-block">
							<div class="ttb-input-item row">
								<span class="input-title"> <label><h4><spring:message code="LB.D0271" /><!-- 項目 -->：</h4></label></span> <!-- 系統時間 -->
								<span class="input-block">
									<div class="ttb-input">
										<spring:message code="LB.D0280" /><!-- 電子交易對帳單 -->
									</div>
								</span>
							</div>
							
							<div class="ttb-input-item row">
								<span class="input-title"> <label><h4><spring:message code="LB.W1623" /><!-- 電子帳單傳送位址(我的Email) -->：</h4></label></span> <!-- 系統時間 -->
								<span class="input-block">
									<div class="ttb-input">
											${sessionScope.dpmyemail} &nbsp;&nbsp;
											<input onclick="check(1);" type="button" class="ttb-sm-btn btn-flat-orange" value="<spring:message code= "LB.X1539" />" name="CMSUBMIT2" id="CMSUBMIT2">
									</div>
								</span>
							</div>
							
							<div class="ttb-input-item row">
								<span class="input-title"> <spring:message code="LB.Transaction_security_mechanism" /> </span> <!--交易機制-->
								<label class="radio-block">
										<input type="radio" name="FGTXWAY" value="0" checked>
										<span><spring:message code="LB.SSL_password"/>：</span>
										<span class="ttb-radio" style="top: 12px"></span>
										<span class="ttb-input">
										<input type="password" name="CMPASSWORD" id="CMPASSWORD" maxlength="8" value="" class="text-input validate[required,custom[onlyLetterNumber]]"
											placeholder="<spring:message code="LB.Please_enter_password" />">
										</span>
								</label>
							</div>
							
					</div>
					<input type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />"/>
								<!-- 重新輸入 -->
					<input type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" onclick="check(0)"/>
								<!-- 確定 -->	
				</div>
				</div>
			</form>
		</section>
		</main>
	</div>
				
	<%@ include file="../index/footer.jsp"%>
</body>
</html>