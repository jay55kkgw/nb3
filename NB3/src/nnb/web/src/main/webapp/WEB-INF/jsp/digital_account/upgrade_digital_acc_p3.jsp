<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %> 
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<style>
		@media screen and (max-width:767px) {
			.ttb-button {
				width: 32%;
				left: 0px;
			}
		}
    </style>
	<script type="text/javascript">
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		function openMenu() {
			var main = document.getElementById("main");
			window.open('${__ctx}/public/OpenAccount1.pdf');		   
		}
	</script>
</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    		<!-- 數位存款帳戶第三類升級為第一類  -->
			<li class="ttb-breadcrumb-item active" aria-current="page">數位存款帳戶第三類升級為第一類</li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 主頁內容 -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!-- 數位存款帳戶第三類升級為第一類 -->
					數位存款帳戶第三類升級為第一類
				</h2>
                <div id="step-bar">
                    <ul>
                        <li class="finished">注意事項</li>
                        <li class="finished">交易驗證</li>
                        <li class="active">完成申請</li>
                    </ul>
                </div>
				<form method="post" id="formId" name="formId" action="">
					<input type="hidden" name="TYPE" value="5">
					<!-- 表單顯示區 -->
					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-input-block">
		                        <div class="ttb-message">
		                            <p>完成申請</p>
		                        </div>	
								<div class="text-center">
									<p>您已於 <b class="high-light">${upgrade_date}</b> 完成數位存款帳戶升級申請</p>
								</div>
							</div>
							
							<!-- 按鈕 -->
							<input type="button" id="CLOSE" value="<spring:message code="LB.X0956" />" class="ttb-button btn-flat-orange"  onclick="fstop.getPage('${pageContext.request.contextPath}'+'/INDEX/index','', '')"/>
						</div>
					</div>		
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>