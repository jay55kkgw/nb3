<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	
	//定期定額申購
	if(${reqParam.MSGTYPE == "N093"}){
		$("#okButton").val('<spring:message code="LB.W1553"/>');
	}
	else{
		//線上立即申請
		$("#okButton").val('<spring:message code= "LB.X0954" />');
	}
	
	$("#okButton").click(function(){
		<!-- Avoid Reflected XSS All Clients -->
		$("#formID").attr("action","${__ctx}/<c:out value='${fn:escapeXml(reqParam.trancode)}' />");
		$("#formID").submit();
	});
	$("#cancelButton").click(function(){
		$("#formID").attr("action","${__ctx}/INDEX/index");
		$("#formID").submit();
	});
});
</script>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	<!--麵包屑-->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><a href="#"></a></li>
			<li class="ttb-breadcrumb-item active" aria-current="page"></li>
		</ol>
	</nav>
	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
				<c:if test = "${reqParam.title == 'gold_buy'}">
							<h2><spring:message code= "LB.W1493" /></h2>
				</c:if>
				<c:if test = "${reqParam.title == 'gold_resale'}">
							<h2><spring:message code= "LB.W1512" /></h2>
				</c:if>
				<c:if test = "${reqParam.title == 'averaging_purchase'}">
							<h2><spring:message code= "LB.W1553" /></h2>
				</c:if>
				<c:if test = "${reqParam.title == 'averaging_alter'}">
							<h2><spring:message code= "LB.W1564" /></h2>
				</c:if>
				<br/><br/><br/>
					<form id="formID" method="post">
						<!-- Avoid Reflected XSS All Clients -->
					<input type="hidden" name="ADOPID" value="<c:out value='${fn:escapeXml(reqParam.txType)}' />"/>
					<div class="main-content-block row">
						<div class="col-12">
						<h4 style="text-align:center;margin: 50px 0 50px 0 ;"><b><font color="red"><c:out value='${fn:escapeXml(reqParam.msgDesc)}' /></font></b></h4>
							
	                  			<input type="button" id="okButton" class="ttb-button btn-flat-orange"/>
	                  			<!-- 取消 -->
	                  			<input type="button" id="cancelButton" value="<spring:message code= "LB.Cancel" />" class="ttb-button btn-flat-gray"/>
	                		
						</div>
					</form>
				</div>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>