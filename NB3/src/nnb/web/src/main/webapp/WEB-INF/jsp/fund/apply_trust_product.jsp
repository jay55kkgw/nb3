<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">

<script type="text/javascript">
	$(document).ready(function() {
		// 將.table變更為footable
		initFootable();
		init();
	});

	function init(){
		$("#formId").validationEngine({binded: false,promptPosition: "inline"});
		$("#CMSUBMIT").click(function(e){
			if(!$('#formId').validationEngine('validate')){
	        	e.preventDefault();
	        }
	        else{
				$("#formId").validationEngine('detach');
				e = e || window.event;
				initBlockUI();
				$("#MARK1").val("Y");
				$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
				$(".Q11:checked").each(function(){
					$("#DEGREE").val($(this).val());
				});
				$("#Q2:checked").each(function(){
					$("#MARK1").val("N");
				});
				$("#formId").attr("action","${__ctx}/OVERSEAS/PROMOTION/apply_trust_product_result");
	  			$("#formId").submit(); 	
	        }
  		});

		//上一頁按鈕
		$("#CMBACK").click(function() {
			$("#formId").validationEngine('detach');
			var action = '${__ctx}/INDEX/index';
			$('#back').val("Y");
			$("form").attr("action", action);
			$("form").submit();
		});
	}
	
	//下拉式選單
	function formReset() {
		if ($('#actionBar').val() == "reEnter") {
			document.getElementById("formId").reset();
			$('#actionBar').val("");
		}
	}

	//列印
	$("#printbtn").click(function() {
		var params = {
			"jspTemplateName" : "demand_deposit_detail_result_print",
			"jspTitle" : '<spring:message code="LB.NTD_Demand_Deposit_Detail"/>',
			"CMQTIME" : "${demand_deposit_detail_result.data.CMQTIME}",
			"CMPERIOD" : "${demand_deposit_detail_result.data.CMPERIOD}",
			"COUNT" : "${demand_deposit_detail_result.data.COUNT}"
		};
		openWindowWithPost(
			"${__ctx}/print",
			"height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",
			params
		);
	});
</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 境外信託商品推介     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X1167" /></li>
    <!-- 申請境外信託商品推介     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0947" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code="LB.D0947" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="">
				<input type="hidden" name="TYPE" value="01">
				<input type="hidden" name="AGE" value="${result_data.data.AGE}">
				<input type="hidden" id="DEGREE" name="DEGREE" value="${result_data.data.DEGREE}">
				<input type="hidden" id="MARK1" name="MARK1" value="${result_data.data.MARK1}">
				<input type="hidden" name="MARK3" value="${result_data.data.MARK3}">
				<input type="hidden" name="RISK7" value="${result_data.data.RISK7}">
				<input type="hidden" name="INTRO" value="${result_data.data.INTRO}">
				<input type="hidden" name="PINNEW" id="PINNEW" value="">
				<input type="hidden" name="FGTXWAY" id="FGTXWAY" value="0">
				<div class="main-content-block row">
					<div class="col-12 terms-block">
						<div class="ttb-input-block">
							<!-- 境外信託商品推介同意書  -->
                      		<ul class="ttb-result-list terms">
                            	<li>
									<c:if test="${result_data.data.INTRO.equals('N')}">
										<c:if test="${result_data.data.UID.length()==10}">
											<spring:message code="LB.W1382" /><br>
											<spring:message code="LB.D0949" />：<br>
											【
											<label class="radio-block" for="Q115">
												<input type="radio" class="Q11" value="5" name="Q11" id="Q115"><spring:message code="LB.D0950" />
	                                            <span class="ttb-radio"></span>
	                                        </label>
											<label class="radio-block" for="Q114">
												<input type="radio" class="Q11" value="4" name="Q11" id="Q114"><spring:message code="LB.D0591" />
	                                            <span class="ttb-radio"></span>
	                                        </label>
											<label class="radio-block" for="Q113">
												<input type="radio" class="Q11" value="3" name="Q11" id="Q113"><spring:message code="LB.D0590" />
	                                            <span class="ttb-radio"></span>
	                                        </label>
											<label class="radio-block" for="Q112">
												<input type="radio" class="Q11" value="2" name="Q11" id="Q112"><spring:message code="LB.D0589" />
	                                            <span class="ttb-radio"></span>
	                                        </label>
											<label class="radio-block" for="Q111">
												<input type="radio" class="Q11" value="1" name="Q11" id="Q111"><spring:message code="LB.D0588" />
	                                            <span class="ttb-radio"></span>
	                                        </label>】
											<span class="hideblock">
												<input id="validate_Q11" name="Q11" type="radio"  
													class="validate[funcCall[validate_Radio[<spring:message code="LB.D1136" />,Q11]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
											</span><br>
											<label class="check-block" for="Q2">
												<input type="checkbox" id="Q2" value="N" name="Q2"><spring:message code="LB.D0955"/>
	                                            <span class="ttb-check"></span>
	                                        </label>
											<br><br>
											<spring:message code="LB.Note"/>：<br>
											<spring:message code="LB.W1390"/>
										</c:if>
										<c:if test="${result_data.data.UID.length()!=10}">
												<spring:message code="LB.W1382"/>
										</c:if>
									</c:if>
									<c:if test="${!result_data.data.INTRO.equals('N')}">
											<spring:message code="LB.W1391"/>
									</c:if>
	 							</li>
	 						</ul>
							<c:if test="${result_data.data.INTRO.equals('N')}">
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                    <label>
	                                        <h4><spring:message code="LB.Transaction_security_mechanism"/></h4>
	                                    </label>
	                                </span>
	                                <span class="input-block">
	                                    <div class="ttb-input">
											<label class="radio-block" for="SSL">
												<input type="radio" id="SSL"  checked="checked"> <spring:message code="LB.SSL_password"/>&nbsp; 
		                                        <span class="ttb-radio"></span>
		                                    </label>
	                                    </div>
	                                    <div class="ttb-input">
											<input type="password" name="CMPASSWORD" id="CMPASSWORD" class="text-input validate[required]" size="8" maxlength="8">
	                                    </div>
	                                </span>
	                            </div>
							</c:if>
						</div>
						<c:if test="${result_data.data.INTRO.equals('N')}">
							<!-- 取消 -->
							<input id="CMBACK" name="CMBACK" type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Cancel"/>"/>
							<!-- 確定 -->
							<input type="button" name="CMSUBMIT" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm"/>"/>
						</c:if>
					</div>
				</div>
			</form>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>