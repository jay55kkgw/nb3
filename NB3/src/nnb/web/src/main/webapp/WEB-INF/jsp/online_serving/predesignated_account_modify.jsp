<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	// HTML載入完成後開始遮罩
	setTimeout("initBlockUI()", 50);
	// 開始跑下拉選單並完成畫面
	setTimeout("init()", 400);
	// 解遮罩
	setTimeout("unBlockUI(initBlockId)", 500);
	// 將.table變更為footable
	//initFootable();
	setTimeout("initDataTable()",100);
	// 初始化時隱藏span
	$("#hideblock").hide();
});

function init(){
	
	$("#formId").validationEngine({
		binded: false,
		promptPosition: "inline"
	});
	
	//確定 
	$("#CMSUBMIT").click(function(e){
		//打開驗證欄位
		$("#hideblock").show();
		var datalist = $("#ROWDATA").val();
		$("#ErrorMsg").val(datalist);
		
		e = e || window.event;
	
		if(!$('#formId').validationEngine('validate')){
        	e.preventDefault();
		}
		else{
				$("#formId").validationEngine('detach');
				initBlockUI();
				$("#formId").attr("action","${__ctx}/ONLINE/SERVING/predesignated_account_modify_s");
	  			$("#formId").submit(); 
			}		
		});
	
}

 function addData(dataCount){
 	// 將頁面資料塞進hidden欄位
 	console.log("dataCount: " + dataCount);
 	var data = $("#"+dataCount).val();
 	console.log("data: " + data);
 	$("#ROWDATA").val(data);	
 };

// function addData(value){
// // 	alert(value);
// 	$("#ErrorMsg").val(value);
// }
	
//重新選擇清除
function clearData(){    	    	
	$("#ROWDATA").val("");
	};
	
</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 修改好記名稱     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0226" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code="LB.D0226"/></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="">
				<input id="ROWDATA" name="ROWDATA" type="hidden" value="" />
				<input id="COUNT" name="COUNT" type="hidden" value="${predesignated_account_modify.data.COUNT}" />
				<div class="main-content-block row">
					<div class="col-12 tab-content">
                        <ul class="ttb-result-list">
                            <li>
                                <h3><spring:message code="LB.Total_records" /></h3>
                                <p>${predesignated_account_modify.data.COUNT} <spring:message code="LB.Rows"/></p>
                            </li>
                        </ul>
						<!-- 修改好記名稱-->
						<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
							<thead>
								<tr>
									<!-- 銀行名稱 -->
									<th>
										<spring:message code="LB.Bank_name"/>
									</th>
									<!-- 約定轉入帳號 -->
									<th>
										<spring:message code="LB.D0227" />
									</td>
									<!-- 好記名稱 -->
									<th>
										<spring:message code="LB.Favorite_name"/>
									</th>
									<!-- 約定註記 -->
									<th>
										<spring:message code="LB.D0239"/>
									</th>
								</tr>
							</thead>							
							<c:if test="${predesignated_account_modify.data.COUNT == '0'}">
								<tbody style="display: none;">
								<tr></tr>
								</tbody>
							</c:if>																	
							<c:if test="${predesignated_account_modify.data.COUNT > '0'}">
							<tbody>
							<c:forEach var="dataList" items="${predesignated_account_modify.data.REC}" varStatus="data" >
								<tr>            	
					                <td class="text-center">
					                	<label class="radio-block">
					                		<input type="radio" name="dataRadio" value="${data.count}" onclick="addData(this.value)"/>${dataList.BNKCOD_C}&nbsp;
                                            <span class="ttb-radio"></span>       
											<input id="${data.count}" name="${data.count}" type="hidden" value="<c:out value='${dataList}' />" />
                                        </label>
					                </td>
									<td class="text-center">${dataList.ACN}</td>
									<td class="text-center"><c:out value='${dataList.DPGONAME}' /></td>
									<td class="text-center">${dataList.RAFLAG}</td>
					           	</tr> 
							</c:forEach>
							</tbody>
							</c:if>
						</table>
						<!-- 不在畫面上顯示的span -->
						<span id="hideblock" >
						<!-- 驗證用的input -->
						<input id="ErrorMsg" name="ErrorMsg" type="text" class="text-input validate[groupRequired[dataRadio]]" 
							style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" value="" />
						</span>
						<!-- 重新輸入 -->
						<input type="reset" class="ttb-button btn-flat-gray" onclick="clearData()" value="<spring:message code="LB.Re_enter"/>"/>
						<!-- 確定 -->
                        <input type="button" name="CMSUBMIT" id="CMSUBMIT" value="<spring:message code="LB.Confirm"/>" class="ttb-button btn-flat-orange">
					</div>
				</div>
			</form>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>