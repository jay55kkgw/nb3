<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>

<!-- 交易機制所需JS -->
<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
		initFootable();
		$('#but_Agree').prop('checked', false);
	}
	
	// 確認鍵 Click
	function processQuery() {
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
		console.log("fgtxway: " + fgtxway);
		// 交易機制選項
		switch(fgtxway) {
			case '0':
				// SSL
				$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
				$('#CMPASSWORD').val("");
	         	initBlockUI();//遮罩
	            $("#formId").submit();
				break;
			case '1':
				// IKEY
				//useIKey()
				var jsondc = $("#jsondc").val();
				uiSignForPKCS7(jsondc);
				break;
			case '2':
				// 晶片金融卡
				var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
				useCardReader(capUri);
				initBlockUI();
		    	break;
			default:
				//alert("<spring:message code="LB.Alert001" />");
				errorBlock(
						null, 
						null,
						["<spring:message code='LB.Alert001' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
		}
	}

</script>
</head>
<body>

	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 信用卡     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Credit_Card" /></li>
    <!-- 密碼函補寄     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1647" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.W1647" /><!-- 申請補寄預借現金密碼 --></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form id="formId" method="post" action="${__ctx}/CREDIT/APPLY/cash_pw_resend_result">
                <input type="hidden" name="TXTOKEN" value="${cash_pw_resend_confirm.data.TXTOKEN}"> 
				<!-- 交易機制所需欄位 -->
				<input type="hidden" id="jsondc" name="jsondc" value="${cash_pw_resend_confirm.data.jsondc}">
				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
				
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">

                            <!-- 卡號-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.Card_number" /><!-- 卡號 --></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                      <span>${cash_pw_resend_confirm.data.hideCardNum}</span>
                                    </div>
                                </span>
                            </div> 
                            
                            <!-- 交易機制-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.Transaction_security_mechanism" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
										<label class="radio-block"><spring:message code="LB.Electronic_signature" /><!-- 電子簽章(載具i-key) -->
                                            <input type="radio" name="FGTXWAY" id="CMIKEY" value="1" checked>
                                           <span class="ttb-radio"></span>
                                        </label>
                                    </div>
                                </span>
                            </div>   
                        </div>
                        <input class="ttb-button btn-flat-orange" id="CMSUBMIT" name="CMSUBMIT" type="button" value="<spring:message code="LB.Confirm" />" onclick="processQuery()"/>
                    </div>
                </div>
				</form>
				<ol class="description-list list-decimal">
               	 <p><spring:message code="LB.Remind_you" /></p>
                    <li><spring:message code="LB.Cash_Pw_Resend_P2_D1" /></li>
                </ol>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>