<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>

<script type="text/javascript">
$(document).ready(function(){
	setTimeout("initBlockUI()",10);
	setTimeout("initDataTable()",100);
	setTimeout("unBlockUI(initBlockId)",500);
	
	$("#printbtn").click(function(){
		var i18n = new Object();
		i18n['jspTitle']='<spring:message code= "LB.D0003" />'
		var params = {
				"jspTemplateName":"card_paid_history_print",
				"jspTitle":i18n['jspTitle'],
				"CMQTIME":"${card_paid_history_result.data.CMQTIME}",
				"CMPERIOD":"${card_paid_history_result.data.CMPERIOD}",
				"TOTCNT":"${card_paid_history_result.data.TOTCNT}",
			
		}
		openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);

	});
	
});
function selectBlockUI() {
	//change後遮罩啟動
	setTimeout("initBlockUI()",10);
	// 開始執行動作
	setTimeout("selectAction()",20);
	// 解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
		
}
function selectAction(){
	if($('#actionBar').val()=="excel"){
		$("#downloadType").val("OLDEXCEL");
		$("#templatePath").val("/downloadTemplate/card_paid_history.xls");
	}else if ($('#actionBar').val()=="txt"){
		$("#downloadType").val("TXT");
		$("#templatePath").val("/downloadTemplate/card_paid_history.txt");
	}
	//ajaxDownload("${__ctx}/ajaxDownload","formId","finishAjaxDownload()");
	$("#formId").attr("target", "");
	$("#formId").submit();
	$('#actionBar').val("");
}
function finishAjaxDownload(){
	$("#actionBar").val("");
	unBlockUI(initBlockId);
}


</script>
</head>
<body>
   	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 信用卡     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Credit_Card" /></li>
    <!-- 帳務查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Inquiry_Service" /></li>
    <!-- 繳款記錄查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0003" /></li>
		</ol>
	</nav>

	
	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
	

	<main class="col-12">
		<section id="main-content" class="container"><!-- 主頁內容  -->
			<!--繳款紀錄查詢-->
			<h2><spring:message code="LB.D0003" /></h2>
			
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			
			<div class="print-block">
					<select class="minimal" id="actionBar" onchange="selectBlockUI()">
						<!-- 下載-->
						<option value=""><spring:message
								code="LB.Downloads" /></option>
						<!-- 下載Excel檔-->
						<option value="excel"><spring:message
								code="LB.Download_excel_file" /></option>
						<!-- 下載為txt檔-->
						<option value="txt"><spring:message
								code="LB.Download_txt_file" /></option>
					</select>
				</div>
			<br/>
			<br/>
				<div class="main-content-block row">
					<div class="col-12">
						<div>
							<ul class="ttb-result-list">
								<li>
									<!-- 查詢時間  -->
									<h3><spring:message code="LB.Inquiry_time" /></h3>
									<p>
										${card_paid_history_result.data.CMQTIME}
									</p>
								</li>
									<!--查詢期間 -->
								<li>
									<h3>
										<spring:message code="LB.Inquiry_period_1" />
									</h3>
									<p>	
										${card_paid_history_result.data.CMPERIOD}
									</p>
								</li>
								<li>
										<!-- 資料總數  -->
									<h3>
									<spring:message code="LB.Total_records" />
									</h3>
									<p>	
										${card_paid_history_result.data.TOTCNT} <spring:message code="LB.Rows" />
									</p>
								</li>
							</ul>
							<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
								<thead>
					            <tr>
					                <th data-title='<spring:message code="LB.Trading_time"/>'>
										<!-- 交易時間  -->
					               		<spring:message code="LB.Trading_time" />
					                </th>
					                <th data-title='<spring:message code="LB.D0020"/>'>
					                	<!-- 繳款方式  -->
					               		<spring:message code="LB.D0020" />
					                </th>
					                <th data-title='<spring:message code="LB.Payment_amount"/>'>
										<!-- 繳款金額  -->
					               		<spring:message code="LB.Payment_amount" />
					                </th>
					            </tr>
					            </thead>
					            <tbody>
					            <c:forEach var="dataTable" items="${card_paid_history_result.data.REC}">
					            <tr>
					                <td class="text-center">
										<!-- 交易時間  -->
										${dataTable.TRNDATE}&nbsp;${dataTable.TRNTIME}
					                </td>
					                <td class="text-center">
					                	<!-- 繳款方式  -->
					                	${dataTable.PAYMTH}
					                </td>
					                <td  class="text-right">
                                         <!-- 繳款金額 -->
                                         ${dataTable.TRNAMT}
					                </td>
					            </tr>
					            </c:forEach>
					            </tbody>
							</table>
						</div>
						
						<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
					</div>
					<form id="formId" action="${__ctx}/download" method="post">
<!-- 						下載用 -->
						<input type="hidden" name="downloadFileName" value="<spring:message code= "LB.D0003" />"/>
						<input type="hidden" name="CMQTIME" value="${card_paid_history_result.data.CMQTIME}"/>
						<input type="hidden" name="TOTCNT" value="${card_paid_history_result.data.TOTCNT}"/>
						<input type="hidden" name="CMPERIOD" value="${card_paid_history_result.data.CMPERIOD}"/>
						<input type="hidden" name="downloadType" id="downloadType"/> 					
						<input type="hidden" name="templatePath" id="templatePath"/> 	
<!-- 						EXCEL下載用 -->
						<input type="hidden" name="headerRightEnd" value="3"/>
						<input type="hidden" name="headerBottomEnd" value="6"/>
						<input type="hidden" name="rowStartIndex" value="7"/>
						<input type="hidden" name="rowRightEnd" value="8"/>
						
<!-- 						TXT下載用 -->
						<input type="hidden" name="txtHeaderBottomEnd" value="6"/>
						<input type="hidden" name="txtHasRowData" value="true"/>
						<input type="hidden" name="txtHasFooter" value="false"/>
					</form>
				</div>
		</section>
	</main><!-- main-content END --> 
	</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>
