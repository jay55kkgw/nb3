<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
<script type="text/javascript" src="${__ctx}/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">    
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript" src="${__ctx}/js/checkutil.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 初始化驗證碼
		setTimeout("initKapImg()", 200);
		// 生成驗證碼
		setTimeout("newKapImg()", 300);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
		initFootable();
		$("#formId").validationEngine({
			binded: false,
			promptPosition: "inline"
		});
		$("#CMRESET").click( function(e) {
			document.getElementById("formId").reset();
// 			getTmr();
		});
		
		datetimepickerEvent();
		//預設日期
// 		getTmr();
		// 確認鍵 click
		submit();
	}
	
	// 確認鍵 Click
	function submit() {
		$("#CMSUBMIT").click( function(e) {
			console.log("submit~~");
			var cardnum = $("#CARDNUMA").val();
			$("#CARDNUM_1").val(cardnum.substring(0,4));
			$("#CARDNUM_2").val(cardnum.substring(5,9));
			$("#CARDNUM_3").val(cardnum.substring(10,14));
			$("#CARDNUM_4").val(cardnum.substring(15));
			console.log($("#CARDNUM_1").val()+""+$("#CARDNUM_2").val()+""+$("#CARDNUM_3").val()+""+$("#CARDNUM_4").val());
			var MM = $("#CCEXDATEMM").val();
			var YY = $("#CCEXDATEYY").val();
			$("#checkMM").val(MM);
			$("#checkYY").val(YY);
			$("#_CUSIDN").val($("#_CUSIDN").val().toUpperCase());
			e = e || window.event;
			if(!$('#formId').validationEngine('validate')){
        		e.preventDefault();
        	}
			else{
				if(!processQuery()){
					return false;
				}
				// 遮罩
         		initBlockUI();
            	$("#formId").attr("action","${__ctx}/ONLINE/APPLY/use_creditcard_step2");
	 	  		$("#formId").submit();
			}
		});
	}
	// 驗證碼刷新
	function changeCode() {
		$('input[name="capCode"]').val('');
		// 大小版驗證碼用同一個
		console.log("changeCode...");
		$('img[name="kaptchaImage"]').hide().attr(
			'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

		// 登入失敗解遮罩
		unBlockUI(initBlockId);
	}
	
	// 初始化驗證碼
	function initKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
	}
	
	// 生成驗證碼
	function newKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').click(function() {
			$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
		});
	}
	//日曆
	function datetimepickerEvent(){

	    $(".CCBIRTHDATE").click(function(event) {
			$('#CCBIRTHDATE').datetimepicker('show');
		});
		
		jQuery('.datetimepicker').datetimepicker({
			timepicker:false,
			closeOnDateSelect : true,
			scrollMonth : false,
			scrollInput : false,
		 	format:'Y/m/d',
		 	lang: '${transfer}'
		});
	}
	
function processQuery(){
		
	  	var main = document.getElementById("formId");
	  	var reg = new RegExp("\/","g");
		var chk = $("#CARDNUMA").val();
		chk = chk.replace(reg,"");
		main.CARDNUM.value = chk;
// 		main.CARDNUM.value = main.CARDNUM_1.value + main.CARDNUM_2.value + main.CARDNUM_3.value + main.CARDNUM_4.value ;

		main.EXPDTA.value = main.CCEXDATEMM.value + main.CCEXDATEYY.value;
		
		if(!checkDate1( main.CCBIRTHDATE.value) ) { 
        	return false;
		}
		
		// 驗證碼驗證
		var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
		var capData = $("#formId").serializeArray();
		var capResult = fstop.getServerDataEx(capUri, capData, false);
		console.log("chaCode_valid: " + JSON.stringify(capResult) );
		// 驗證結果
		if (capResult.result) {
			main._CUSIDN.value=$("#_CUSIDN").val();
			return true;			
		} else {
			// 失敗重新產生驗證碼
			//驗證碼有誤
			//alert("<spring:message code= "LB.X1082" />");
			errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.X1082' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
			);
			changeCode();
		}
	  	return false;
	}
	
function checkDate1(date){
	var newdate = date.split("/");
	var yyyy = Number(newdate[0]);
	var oyyy=Number(yyyy - 1911); 
	var m = newdate[1];
	var d = newdate[2];
// 	alert("" + yyyy + "" + oyyy + ""+m+""+d);
	//alert("checkdate1 year:"+oyyy);
	if(m==2||m==4||m==6||m==9||m==11){
			if(m==2&&d>29){
					//alert("checkDate:false");
					return	false;
			}		
			if(d>30){
					//alert("checkDate:false");
					return	false;
			}		
	}
	if(m==2&&d==29){
		if(((oyyy%4)==0 && !(oyyy%100)==0) || (oyyy%400==0)){
			//alert("checkDate:true");
			$("#CCBIRTHDATEYY").val(yyyy);
			$("#CCBIRTHDATEMM").val(m);
			$("#CCBIRTHDATEDD").val(d);
			return true;
		}
		else {
			//alert("checkDate:false");
			return	false;
		}		
	}
	$("#CCBIRTHDATEYY").val(yyyy);
	$("#CCBIRTHDATEMM").val(m);
	$("#CCBIRTHDATEDD").val(d);
	return	true;
}

//預約自動輸入今天
function getTmr() {
	var today = new Date();
	today.setDate(today.getDate());
	var y = today.getFullYear();
	var m = today.getMonth() + 1;
	var d = today.getDate();
	if(m<10){
		m = "0"+m
	}
	if(d<10){
		d="0"+d
	}
	var tmr = y + "/" + m + "/" + d
	$('#CCBIRTHDATE').val(tmr);
}

function setBlur(obj,target2)
{
   var target =document.getElementById(target2);
     if( obj.value.length ==obj.getAttribute('maxlength'))
         {
             target.focus();
         }
     return;
} 
jQuery(function($) {//,{autoclear: false}
	$("#CARDNUMA").mask("9999/9999/9999/9999",{autoclear: false});
});

</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Register" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<!-- 信用卡申請網路銀行 -->
				<h2><spring:message code= "LB.X1086" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="">
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
                            
                            <!--身份證字號-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D0204" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <input type="text" maxLength="10" size="10" class="text-input validate[required,funcCall[validate_checkSYS_IDNO[_CUSIDN]]]"id="_CUSIDN" name="_CUSIDN" value="" >
                                    </div>
                                </span>
                            </div>

							<!--信用卡號碼(十六碼)-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code= "LB.D1023" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                    	<input type="text" id="CARDNUMA" name="CARDNUMA" class="text-input validate[required,funcCall[validate_checkMajorCard['<spring:message code= "LB.D1033" />',CARDNUM_1,CARDNUM_2,CARDNUM_3,CARDNUM_4]]]" placeholder="____ / ____ / ____ / ____"/>
                                      	<input type="hidden" id="CARDNUM_1" />
                                        <input type="hidden" id="CARDNUM_2" />
                                        <input type="hidden" id="CARDNUM_3" />
      	                                <input type="hidden" id="CARDNUM_4" />
<%--       	                                <input class="card-input validate[required,funcCall[validate_checkMajorCard['<spring:message code= "LB.D1033" />',CARDNUM_1,CARDNUM_2,CARDNUM_3,CARDNUM_4]]]" maxLength="4" size="4" id="CARDNUM_4" name="CARDNUM_4" value="" /> --%>
                                    </div>
                                </span>
                            </div>
							
							<!--卡片有效期限-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D1024" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <input type="text" maxLength="2" size="2" class="text-input" id="CCEXDATEMM" name="CCEXDATEMM" value="">
      									<!-- 月 -->
      									<span class="ttb-unit"> <spring:message code= "LB.D0089_3" /></span>
      									<span id="hideblock_CMM"> 
											<!-- 驗證用的input --> 
											<!-- 卡片有效期限(月) -->
											<input id="checkMM" name="checkMM" type="text"
												class="text-input validate[required,funcCall[validate_CheckNumber[<spring:message code= "LB.D1024" />(<spring:message code= "LB.Month" />),CCEXDATEMM]]]"
												style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;" />
										</span>
									</div>
									<!-- 年 -->
									<div class="ttb-input">
      									<input type="text" maxLength="2" size="2" class="text-input" id="CCEXDATEYY" name="CCEXDATEYY" value="">
										<span class="ttb-unit"> <spring:message code= "LB.D0089_2" /></span>
										<span id="hideblock_CYY"> 
											<!-- 驗證用的input --> 
											<input id="checkYY" name="checkYY" type="text"
												class="text-input validate[required,funcCall[validate_CheckNumber[<spring:message code= "LB.D1024" />(<spring:message code= "LB.Year" />),CCEXDATEYY]],funcCallRequired[validate_chkperiod['<spring:message code= "LB.D1024" />(<spring:message code= "LB.Year" />、<spring:message code= "LB.Month" />)',CCEXDATEYY,CCEXDATEMM]]]"
												style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;" />
										</span>
										<br>
										<!-- 請輸入卡片上的西元有效月年(MM/YY) -->
										<span class="ttb-unit"><spring:message code= "LB.D1027" /></span>
                                    </div>
                                </span>
                            </div>
							
							<!--卡片簽名欄(三碼)-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D1028" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                      <input type="text" maxLength="3" size="4" class="text-input validate[required,funcCall[validate_CheckNumWithDigit[<spring:message code= "LB.D1028" />,CHECKNO,3]]]" id="CHECKNO" name="CHECKNO" value=""/>
                                    </div>
                                </span>
                            </div>

							<!--出生年月日-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code= "LB.D0054" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
										  <input type="text" class="text-input datetimepicker validate[funcCall[validate_CheckDate[<spring:message code= "LB.D0054" />,CCBIRTHDATE,null,null]]]" id="CCBIRTHDATE" name="CCBIRTHDATE" value="" >
                                            <span class="input-unit CCBIRTHDATE"><img src="${__ctx}/img/icon-7.svg"/></span>
                                    </div>
                                </span>
                            </div>
							
							<!--住家電話(末四碼)-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D1030" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                      <input type="text" maxLength="4" size="5" class="text-input validate[required,funcCall[validate_CheckNumWithDigit[<spring:message code= "LB.D1030" />,PHONE_H,4]]]" id="PHONE_H" name="PHONE_H" value=""/>
                                    </div>
                                </span>
                            </div>
							
							<!--行動電話(末四碼)-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D1031" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                      <input type="text" maxLength="4" size="5" class="text-input validate[required,funcCall[validate_CheckNumWithDigit[<spring:message code= "LB.D1031" />,MOBILE,4]]]" id="MOBILE" name="MOBILE" value="">
                                    </div>
                                </span>
                            </div>
							
							<div class="ttb-input-item row" id="chaBlock">
								<!-- 驗證碼 -->
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Captcha" /></h4>
									</label>
								</span>
								<span class="input-block">
									<spring:message code="LB.Captcha" var="labelCapCode" />
									<input id="capCode" name="capCode" type="text"
										class="text-input" maxlength="6" autocomplete="off">
									<img name="kaptchaImage" src="" class="verification-img" />
									<input type="button" name="reshow" class="ttb-sm-btn btn-flat-orange"
										onclick="changeCode()" value="<spring:message code="LB.Regeneration" />" />
								</span>
							</div>
							
                        </div>
                        <input class="ttb-button btn-flat-gray" id="CMRESET" name="CMRESET" type="button" value="<spring:message code="LB.Re_enter" />" />
                        <input class="ttb-button btn-flat-orange" id="CMSUBMIT" name="CMSUBMIT" type="button" value="<spring:message code="LB.Confirm" />" />
                        <input type="hidden" id="ADOPID" name="ADOPID" value="NA40">
  						<input type="hidden" id="CARDNUM" name="CARDNUM" value="">
  						<input type="hidden" id="EXPDTA" name="EXPDTA" value="">
  						<input type="hidden" id="CCBIRTHDATEYY" name="CCBIRTHDATEYY" value="">
  						<input type="hidden" id="CCBIRTHDATEMM" name="CCBIRTHDATEMM" value="">
  						<input type="hidden" id="CCBIRTHDATEDD" name="CCBIRTHDATEDD" value="">
                    </div>
                </div>
                </form>
				
                <ol class="list-decimal description-list">
                <p><spring:message code="LB.Description_of_page" /></p>
                <!-- 請使用一般信用卡正卡申請(不含商務卡、採購卡、附卡及手機信用卡)，完成申請即可立刻使用網路銀行查詢信用卡、電子帳單、Email繳款通知等相關服務。 -->
                   <li><spring:message code= "LB.Creditcard_Apply_Controller_P1_D1" /><font color="#FF0000"><spring:message code= "LB.Creditcard_Apply_Controller_P1_D1-1" /></font><spring:message code= "LB.Creditcard_Apply_Controller_P1_D1-2" /><font color="#FF0000"><spring:message code= "LB.Creditcard_Apply_Controller_P1_D1-3" /></font><spring:message code= "LB.Creditcard_Apply_Controller_P1_D1-4" /></li>
		         <!-- VISA金融卡、悠遊Debit卡請改以「使用臺灣企銀晶片金融卡 + 讀卡機」方式申請。 -->
		          <li><spring:message code= "LB.Creditcard_Apply_Controller_P1_D2" /><a href='${__ctx}/ONLINE/APPLY/use_component'><spring:message code= "LB.Creditcard_Apply_Controller_P1_D2-1" /></a><spring:message code= "LB.Creditcard_Apply_Controller_P1_D2-2" /></li>
		         <!-- 信用卡線上申請者無法執行「存/放款查詢」、「轉帳」、「申購基金」等功能，如欲增加前述功能，請親洽往來分行辦理升級為臨櫃申請用戶，即可使用完整的理財服務。 -->
		          <li> <spring:message code= "LB.Use_Creditcard_P3_D3" /></li>
		          <!-- 申請如有疑問，請電洽客服專線02-2357-7171、0800-01-7171。 -->
		          <li><spring:message code= "LB.Use_Creditcard_P3_D4" /></li>
                </ol>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>