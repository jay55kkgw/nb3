<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	//setTimeout("initDataTable()",100);
	$("#backButton").click(function(){
		$("#formID").attr("action","${__ctx}/FUND/QUERY/profitloss_balance");
		$("#formID").submit();
	});
	$("#printButton").click(function(){
		var params = {
				"jspTemplateName":"fund_transfer_result_print",
				"jspTitle":"<spring:message code= "LB.W0930" />",
				"CMQTIME":"${result_data.data.CMQTIME}",
				"hiddenCUSIDN":"${result_data.data.hiddenCUSIDN}",
				"hiddenNAME":"${result_data.data.hiddenNAME}",
				"hiddenCDNO":"${result_data.data.hiddenCDNO}",
				"TRANSCODE":"${result_data.data.TRANSCODE}",
				"fundName":"${result_data.data.fundName}",
				"INTRANSCODE":"${result_data.data.INTRANSCODE}",
				"inFundName":"${result_data.data.inFundName}",
				"BILLSENDMODEChinese":"${result_data.data.BILLSENDMODEChinese}",
				"UNITFormat":"${result_data.data.UNITFormat}",
				"ADCCYNAME":"${result_data.data.ADCCYNAME}",
				"FUNDAMTFormat":"${result_data.data.FUNDAMTFormat}",
				"AMT3Integer":"${result_data.data.AMT3Integer}",
				"FCA2Format":"${result_data.data.FCA2Format}",
				"FCA1Format":"${result_data.data.FCA1Format}",
				"OUTACN":"${result_data.data.OUTACN}",
				"AMT5Format":"${result_data.data.AMT5Format}",
				"TRADEDATEFormat":"${result_data.data.TRADEDATEFormat}"
			};
			openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
	});
});
</script>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金    -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0901" /></li>
    <!-- 基金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1064" /></li>
    <!-- 轉換交易     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0930" /></li>
		</ol>
	</nav>

	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.W0930"/></h2>
				<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
				
<%-- 					<h4><spring:message code="LB.X0410" /></h4> --%>
					<form id="formID" method="post">
						<input type="hidden" name="ADOPID" value="C021"/>
						<div class="main-content-block row">
							<div class="col-12">
								<div class="ttb-input-block">
									<div class="ttb-message">
		                                <span><spring:message code="LB.X0410" /></span>
		                            </div>
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label>
												<h4>
													<spring:message code="LB.Trading_time"/>
												</h4>
											</label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												${result_data.data.CMQTIME}
											</div>
										</span>
									</div>
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label>
												<h4>
													<spring:message code="LB.Id_no"/>
												</h4>
											</label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												${result_data.data.hiddenCUSIDN}
											</div>
										</span>
									</div>
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label>
												<h4>
													<spring:message code="LB.Name"/>
												</h4>
											</label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												${result_data.data.hiddenNAME}
											</div>
										</span>
									</div>
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label>
												<h4>
													<spring:message code="LB.W1111"/>
												</h4>
											</label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												${result_data.data.hiddenCDNO}
											</div>
										</span>
									</div>
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label>
												<h4>
													<spring:message code="LB.W0025"/>
												</h4>
											</label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												（${result_data.data.TRANSCODE}）${result_data.data.fundName}
											</div>
										</span>
									</div>
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label>
												<h4>
													<spring:message code="LB.W0950"/>
												</h4>
											</label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												（${result_data.data.INTRANSCODE}）${result_data.data.inFundName}
											</div>
										</span>
									</div>
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label>
												<h4>
													<spring:message code="LB.W1115"/>
												</h4>
											</label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												${result_data.data.BILLSENDMODEChinese}
											</div>
										</span>
									</div>
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label>
												<h4>
													<spring:message code="LB.W1122"/>
												</h4>
											</label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												${result_data.data.UNITFormat}
											</div>
										</span>
									</div>
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label>
												<h4>
													<spring:message code="LB.W1116"/>
												</h4>
											</label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												${result_data.data.ADCCYNAME}${result_data.data.FUNDAMTFormat}
											</div>
										</span>
									</div>
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label>
												<h4>
													<spring:message code="LB.W1123"/>
												</h4>
											</label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												<spring:message code="LB.NTD" /> ${result_data.data.AMT3Integer}
											</div>
										</span>
									</div>
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label>
												<h4>
													<spring:message code="LB.W1125"/>
												</h4>
											</label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												<spring:message code="LB.NTD" /> ${result_data.data.FCA2Format}
											</div>
										</span>
									</div>
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label>
												<h4>
													<spring:message code="LB.W0974"/>
												</h4>
											</label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												<spring:message code="LB.NTD" /> ${result_data.data.FCA1Format}
											</div>
										</span>
									</div>
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label>
												<h4>
													<spring:message code="LB.D0168"/>
												</h4>
											</label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												${result_data.data.OUTACN}
											</div>
										</span>
									</div>
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label>
												<h4>
													<spring:message code="LB.W1079"/>
												</h4>
											</label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												<spring:message code="LB.NTD" /> ${result_data.data.AMT5Format}
											</div>
										</span>
									</div>
									<div class="ttb-input-item row">
										<span class="input-title"> 
											<label>
												<h4>
													<spring:message code="LB.W1080"/>
												</h4>
											</label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												${result_data.data.TRADEDATEFormat}
											</div>
										</span>
									</div>
								</div>
<!-- 								<div> -->
<!-- 									<p style="text-align:left;"> -->
<%-- 										<b><spring:message code="LB.X1234" />：</b><br/> --%>
<%-- 										<spring:message code="LB.Fund_Redeem_Data_P3_D1" /><br/> --%>
<%-- 										<spring:message code="LB.fund_redeem_data_P4_D2" /><br/> --%>
<%-- 										<spring:message code="LB.fund_redeem_data_P4_D4" /> --%>
<!-- 									</p> -->
<!-- 								</div> -->
								
									<input type="button" id="backButton" value="<spring:message code="LB.X0413" />" class="ttb-button btn-flat-gray"/>
									<input type="button" id="printButton" value="<spring:message code="LB.Print"/>" class="ttb-button btn-flat-orange"/>
							
							</div>
						</div>
					</form>
					<div class="text-left">
					
						<ol class="list-decimal text-left description-list">
							<p><spring:message code="LB.Description_of_page"/></p>
							<li><span><spring:message code="LB.X1234" />：<br>
								<ol>
									<li style="text-indent:-43px;">（1）<span><font color="red"><spring:message code="LB.Fund_Redeem_Data_P3_D1" /></font></span></li>
									<li style="text-indent:-43px;">（2）<span><font color="red"><spring:message code="LB.fund_redeem_data_P4_D2" /></font></span></li>
									<li style="text-indent:-43px;">（3）<span><font color="red"><spring:message code="LB.fund_redeem_data_P4_D4" /></font></span></li>
								</ol>
								</span></li>
								
						</ol>
					</div>
				</div>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
<c:if test="${showDialog == 'true'}">
	<%@ include file="../index/txncssslog.jsp"%>
	</c:if>
</html>