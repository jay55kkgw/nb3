<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="page7" class="card-detail-block">
	<div class="card-center-block">
		<!-- Q1 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-71" aria-expanded="true"
				aria-controls="popup1-71">
				<div class="row">
					<span class="col-1">Q1</span>
					<div class="col-11">
						<span>为什么申请电子凭证要支付凭证费，凭证的有效期限为多久？</span>
					</div>
				</div>
			</a>
			<div id="popup1-71" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p> 网络银行电子签章交易使用台湾网络认证公司金融XML凭证，需支付该公司凭证费用，目前收取之凭证年费为个人户NT$150元，法人户NT$900元，有效期间一年。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q2 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-72" aria-expanded="true"
				aria-controls="popup1-72">
				<div class="row">
					<span class="col-1">Q2</span>
					<div class="col-11">
						<span>除了使用电子签章交易应支付凭证年费外，还有什么与凭证相关之费用？</span>
					</div>
				</div>
			</a>
			<div id="popup1-72" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>依照金融XML凭证作业规定，使用金融XML凭证，必需使用凭证载具储存凭证及公私钥，「凭证载具」每支NT$600元，「凭证载具」若未有任何毁损，可以无限期使用。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q3 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-73" aria-expanded="true"
				aria-controls="popup1-73">
				<div class="row">
					<span class="col-1">Q3</span>
					<div class="col-11">
						<span>网络银行转账手续费及相关优惠？</span>
					</div>
				</div>
			</a>
			<div id="popup1-73" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div style="overflow: auto">
								<table class="question-table">
									<thead>
										<tr>
											<th colspan="4">服务项目</th>
											<th>收费标准</th>
											<th>备注</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th rowspan="4">台币</th>
											<th colspan="3">自行转账</th>
											<td>免手续费</td>
											<td></td>
										</tr>
										<tr>
											<th rowspan="3">跨行转账</th>
											<th colspan="2">NTD 500以下 (含)</th>
											<td>NTD 10/每笔<br>(每账户每日第1笔免手续费)</td>
											<td>当日未使用之优惠次数不累计至隔日使用。</td>
										</tr>
										<tr>
											<th colspan="2">NTD 501 ~ 1,000(含)</th>
											<td>NTD 10/每笔</td>
											<td></td>
										</tr>
										<tr>
											<th colspan="2">NTD 1,000以上</th>
											<td>NTD 15/每笔</td>
											<td></td>
										</tr>
										<tr>
											<th rowspan="3">缴费税</th>
											<th colspan="3">缴税</th>
											<td>免手续费</td>
											<td></td>
										</tr>
										<tr>
											<th rowspan="2">缴费</th>
											<th colspan="2">本行代收者</th>
											<td>免手续费</td>
											<td rowspan="2" class="white-spacing text-left">缴费项目包含：电费、台北市水费、台湾省水费、劳保费、健保费、国民年金保险费、新制劳工退休准备金、本行信用卡费、欣欣瓦斯费、学杂费、其他费用。</td>
										</tr>
										<tr>
											<th colspan="2">跨行缴费</th>
											<td>NTD 15/每笔</td>
										</tr>
										<tr>
											<th rowspan="28">外汇</th>
											<th rowspan="7">买卖外币<br>(结构结售)</th>
											<th colspan="2">美金</th>
											<td>挂牌汇率优惠0.015</td>
											<td rowspan="7" class="white-spacing text-left">以上汇率之优惠不得优于本行营业单位之成本汇率。</td>
										</tr>
										<tr>
											<th colspan="2">澳币</th>
											<td>挂牌汇率优惠0.015</td>
										</tr>
										<tr>
											<th colspan="2">欧元</th>
											<td>挂牌汇率优惠0.015</td>
										</tr>
										<tr>
											<th colspan="2">纽币</th>
											<td>挂牌汇率优惠0.01</td>
										</tr>
										<tr>
											<th colspan="2">南非币</th>
											<td>挂牌汇率优惠0.006</td>
										</tr>
										<tr>
											<th colspan="2">港币</th>
											<td>挂牌汇率优惠0.002</td>
										</tr>
										<tr>
											<th colspan="2">日币</th>
											<td>挂牌汇率优惠0.0002</td>
										</tr>
										<tr>
											<th colspan="3">综存定存</th>
											<td>各币别一律按本行牌告利率加计0.05%</td>
											<td class="white-spacing text-left">
												<ol class="list-decimal">
													<li>包含外汇综存定存及其自动转期案件</li>
													<li>以上利率之优惠不得优于本行营业单位之成本利率。</li>
												</ol>
											</td>
										</tr>
										<tr>
											<th rowspan="2">自行转账</th>
											<th colspan="2">手续费</th>
											<td>免手续费</td>
											<td class="white-spacing text-left">国内分行(DBU)与国际金融业务分行(OBU)账户互转视同汇出汇款，费率详如外汇汇出汇款。</td>
										</tr>
										<tr>
											<th colspan="2">转让费</th>
											<td>免手续费<br>(不同户名之外币间转账)</td>
											<td></td>
										</tr>
										<tr>
											<th rowspan="8">汇出汇款<br>(DBU/国内分行)</th>
											<th colspan="2">手续费</th>
											<td>依汇款金额<br>0.05%计收</td>
											<td>最低收费等值NTD 100，最高收费等值NTD 800。</td>
										</tr>
										<tr>
											<th colspan="2">邮电费</th>
											<td>等值NTD 300/每笔</td>
											<td>另依实际发电笔数加收电报费。</td>
										</tr>
										<tr>
											<th rowspan="6" class="white-spacing text-left">国外费用</th>
											<th colspan="3">如手续费负担别选择『OUR』，每笔依「费用扣款币别」加收等值的国外费用：</th>
										</tr>
										<tr>
											<th>欧元汇款</th>
											<td>依汇款金额<br>0.1%计收</td>
											<td>最低收费EUR 30</td>
										</tr>
										<tr>
											<th>日币汇款</th>
											<td>依汇款金额<br>0.05%计收</td>
											<td>最低收费JPY 5,000</td>
										</tr>
										<tr>
											<th>英镑汇款</th>
											<td>依汇款金额<br>0.1%计收</td>
											<td>最低收费GBP 25</td>
										</tr>
										<tr>
											<th>港币汇款</th>
											<td>HKD 300/每笔</td>
											<td></td>
										</tr>
										<tr>
											<th>其他币别汇款</th>
											<td>等值USD 35/每笔</td>
											<td></td>
										</tr>
										<tr>
											<th rowspan="8">汇出汇款<br>(OBU/国际金融业务分行)</th>
											<th colspan="2">手续费</th>
											<td>依汇款金额<br>0.05%计收</td>
											<td>最低收费等值USD 10，最高收费等值USD 40。</td>
										</tr>
										<tr>
											<th colspan="2">邮电费</th>
											<td>等值USD 15/每笔</td>
											<td>另依实际发电笔数加收电报费。</td>
										</tr>
										<tr>
											<th rowspan="6">国外费用</th>
											<th colspan="3">如手续费负担别选择『OUR』，每笔依「费用扣款币别」加收等值的国外费用：</th>
										</tr>
										<tr>
											<th>欧元汇款</th>
											<td>依汇款金额<br>0.1%计收</td>
											<td>最低收费EUR 40</td>
										</tr>
										<tr>
											<th>日币汇款</th>
											<td>依汇款金额<br>0.05%计收</td>
											<td>最低收费JPY 5,000</td>
										</tr>
										<tr>
											<th>英镑汇款</th>
											<td>依汇款金额<br>0.1%计收</td>
											<td>最低收费GBP 25</td>
										</tr>
										<tr>
											<th>港币汇款</th>
											<td>HKD 300/每笔</td>
											<td></td>
										</tr>
										<tr>
											<th>其他币别汇款</th>
											<td>等值USD 35/每笔</td>
											<td></td>
										</tr>
										<tr>
											<th>汇入汇款/在线解款<br>(DBU/国内分行)</th>
											<th colspan="2">手续费</th>
											<td>依汇入款金额<br>0.05%计收</td>
											<td>最低收费等值NTD 200、最高收费等值NTD 800。</td>
										</tr>
										<tr>
											<th>汇入汇款/在线解款<br>(OBU/国际金融业务分行)</th>
											<th colspan="2">手续费</th>
											<td>依汇入款金额<br>0.05%计收</td>
											<td>最低收费等值USD 10、最高收费等值USD 40。</td>
										</tr>
										<tr>
											<th rowspan="2">基金</th>
											<th colspan="3">国内</th>
											<td>4.5折起</td>
											<td></td>
										</tr>
										<tr>
											<th colspan="3">国外</th>
											<td>5折起</td>
											<td></td>
										</tr>
									</tbody>
								</table>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>