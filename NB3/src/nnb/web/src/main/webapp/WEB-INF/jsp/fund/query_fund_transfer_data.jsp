<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//開始查詢資料並完成畫面
	setTimeout("init()",20);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	setTimeout("initDataTable()",100);
	var mail = '${sessionScope.dpmyemail}';
// 	mail='';
	if(${ConfirmFlag == true}){
		//alert("<spring:message code= "LB.Alert110" />");
		errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.Alert110' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
		);
		$("#errorBtn1").click(function(e) {
			$('#error-block').hide();
			if( ''== mail){
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.X2637' />"], 
						'<spring:message code= "LB.Confirm" />', 
						null
					);
				$("#errorBtn1").click(function(e) {
					$('#error-block').hide();
				});
			}
		});
	}else{
		if( ''== mail){
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.X2637' />"], 
					'<spring:message code= "LB.Confirm" />', 
					null
				);
			$("#errorBtn1").click(function(e) {
				$('#error-block').hide();
			});
		}
	}
});
function init(){
	//initFootable();
}
function checkShortTrade(CDNO,TRANSCODE,CRY,FUNDAMT,UNIT,INDEX,SHORTTRADE,SHORTTUNIT,FUNDLNAME,COUNTRYTYPE,FEE_TYPE,FUSMON){
	$("#CDNO").val(CDNO);      
	$("#TRANSCODE").val(TRANSCODE);
	$("#CRY").val(CRY);      
  	$("#OFUNDAMT").val(FUNDAMT);
	$("#UNIT").val(UNIT);
  	$("#SHORTTRADE").val(SHORTTRADE);
  	$("#SHORTTUNIT").val(SHORTTUNIT);
	$("#FUNDLNAME").val(FUNDLNAME);
	$("#COUNTRYTYPE").val(COUNTRYTYPE);
	$("#FEE_TYPE").val(FEE_TYPE);
	$("#FUSMON").val(FUSMON);
	
// 	if(FEE_TYPE == "A"){
// 		errorBlock(
// 				null, 
// 				null,
// 				["手續費後收型基金，申購時無須支付申購手續費，惟贖回時須支付條件式遞延銷售手續費，其費率依其持有之期間而不同"], 
// 				'已了解', 
// 				null
// 		);
// 		return false ;
// 	}
	
	var BILLSENDMODE = $("input[name=BILLSENDMODE_"+ INDEX +"]:checked").val();
	
	if(BILLSENDMODE == null){
		//alert("<spring:message code= "LB.Alert124" />");
		errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.Alert124' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
		);
		return;
	}
	$("#BILLSENDMODE").val($("input[name=BILLSENDMODE_"+ INDEX +"]:checked").val());
	$("#formID").submit();
} 

//營業時間
function workTime(){
	errorBlock(
			["<spring:message code= 'LB.X2371' />"], 
			["1.<spring:message code= 'LB.X2372' />","2.<spring:message code= 'LB.X2373' />"],
			null,
			'<spring:message code= "LB.Quit" />', 
			null
		);
}
</script>
</head>
<body>
	<fmt:setLocale value="${__i18n_locale}"/>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金/海外債券     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Funds" /></li>
    <!-- 基金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1064" /></li>
    <!-- 轉換交易     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0930" /></li>
		</ol>
	</nav>

	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
				<h2>
				<spring:message code="LB.W0930"/>
				</h2>
				<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
			
					<form id="formID" action="${__ctx}/FUND/TRANSFER/fund_transfer_select" method="post">
						<input type="hidden" name="ADOPID" value="C021"/>
						<input type="hidden" name="CUSIDN" value="${CUSIDN}"/>
						<input type="hidden" name="NAME" value="${NAME}"/>    
						<input type="hidden" id="CDNO" name="CDNO"/>
						<input type="hidden" id="TRANSCODE" name="TRANSCODE"/>
						<input type="hidden" id="CRY" name="CRY"/>
						<input type="hidden" id="OFUNDAMT" name="OFUNDAMT"/>
						<input type="hidden" id="UNIT" name="UNIT"/>
						<input type="hidden" id="BILLSENDMODE" name="BILLSENDMODE"/>
						<input type="hidden" id="SHORTTRADE" name="SHORTTRADE"/>
						<input type="hidden" id="SHORTTUNIT" name="SHORTTUNIT"/>
						<input type="hidden" id="FUNDLNAME" name="FUNDLNAME"/>
						<input type="hidden" id="COUNTRYTYPE" name="COUNTRYTYPE"/>
						<input type="hidden" id="FEE_TYPE" name="FEE_TYPE"/>
						<input type="hidden" id="FUSMON" name="FUSMON"/>
						<input type="hidden" name="FDINVTYPE" value="${FDINVTYPE}"/>
						<input type="hidden" name="GETLTD7" value="${GETLTD7}"/>
						<input type="hidden" name="BRHCOD" value="${BRHCOD}"/>
						<input type="hidden" name="PRO" value="${RISK7}"/>
						<input type="hidden" name="RISK7" value="${RISK7}"/>
						<input type="hidden" name="KYCDATE" value="${KYCDAT}"/>
						<input type="hidden" name="WEAK" value="${WEAK}"/>
				<div class="main-content-block row">
						<div class="col-12">
							<ul class="ttb-result-list">
								<li>
									<!--資料總數-->
									<h3><spring:message code="LB.Total_records"/>：</h3>
									<p>
										${dataCount}
										<spring:message code="LB.Rows"/>
									</p>
								</li>
								<li>
									<!--資料總數-->
									<h3><spring:message code="LB.Id_no"/></h3>
									<p>
										${hiddenCUSIDN}
									</p>
								</li>
								<li>
									<!--資料總數-->
									<h3><spring:message code="LB.Name"/></h3>
									<p>
										${hiddenNAME}
									</p>
								</li>
							</ul>
							<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
								<thead>
<!-- 									<tr> -->
<%-- 										<th><spring:message code="LB.Id_no"/></th> --%>
<%-- 								 		<th colspan="5" style="text-align:left">${hiddenCUSIDN}</th> --%>
<!-- 									</tr> -->
<!-- 									<tr> -->
<%-- 										<th><spring:message code="LB.Name"/></th> --%>
<%-- 								 		<th colspan="5" style="text-align:left">${hiddenNAME}</th> --%>
<!-- 									</tr> -->
									<tr>
										<th><spring:message code="LB.W0944"/></th>
								 		<th><spring:message code="LB.W0025"/></th>
										<th><spring:message code="LB.W0026"/></th>
										<th><spring:message code="LB.W0027"/></th>
										<th><spring:message code="LB.W1115"/></th>
										<th><spring:message code="LB.X0407" /></th>
									</tr>
								</thead>
								<tbody>
									<c:if test="${fundRows.size() > 0}">
									<c:forEach var="dataMap" items="${fundRows}" varStatus="status">
										<tr>
											<td style="text-align:center">${dataMap.hiddenCDNO}</td>
											<td style="text-align:center">（${dataMap.TRANSCODE}）${dataMap.FUNDLNAME}
											<c:if test="${dataMap.FUSMON != '000'}">
												<br><strong><font color="red"><spring:message code= "LB.X2497_1" /></font></strong>
											</c:if>
											<c:if test="${dataMap.FUNDT == '1'}">
												<br><strong><font color="red"><spring:message code= "LB.X2497_2" /></font></strong>
											</c:if>
											</td>
											<td style="text-align:center">
							                	<c:if test="${__i18n_locale eq 'en' }">
					                   				${dataMap.ADCCYENGNAME}<br/>${dataMap.formatFUNDAMT}
					                    		</c:if>
					                    		<c:if test="${__i18n_locale eq 'zh_TW' }">
					                   				${dataMap.ADCCYNAME}<br/>${dataMap.formatFUNDAMT}
					                    		</c:if>
					                    		<c:if test="${__i18n_locale eq 'zh_CN' }">
					                   				${dataMap.ADCCYCHSNAME}<br/>${dataMap.formatFUNDAMT}
					                    		</c:if>
											</td>
											<td style="text-align:right">${dataMap.formatUNIT}</td>
											
											<td style="text-align:center">
											<c:if test="${dataMap.FEE_TYPE_CHK == '1' && dataMap.FUNDT != '1'}">
												<label class="radio-block">
													<input type="radio" value="1" name="BILLSENDMODE_${status.index}"><spring:message code="LB.All"/>
		                                            <span class="ttb-radio"></span>
		                                        </label>
												<br/>
												<label class="radio-block">
													<input type="radio" value="2" name="BILLSENDMODE_${status.index}" ><spring:message code="LB.X0383" />
		                                            <span class="ttb-radio"></span>
		                                        </label>
											</td>
											<td style="text-align:center;vertical-align:middle;"><input type="button" value="<spring:message code="LB.X1941"/>"
											
											onclick="checkShortTrade('${dataMap.CDNO}',
                                      		'${dataMap.TRANSCODE}',
                                      		'${dataMap.CRY}',
                                      		'${dataMap.FUNDAMT}',
                                      		'${dataMap.UNIT}',
                                      		'${status.index}',
                                      		'00000000',
                                      		'${dataMap.SHORTTUNIT}',
                                      		'${dataMap.FUNDLNAME}',
                                      		'${dataMap.COUNTRYTYPE}',
                                      		'${dataMap.FEE_TYPE}',
                                      		'${dataMap.FUSMON}')" class="ttb-sm-btn btn-flat-orange">  <!--  class="ttb-button btn-flat-orange"-->
                                      		</td>
											</c:if>
											<c:if test="${dataMap.FEE_TYPE_CHK == '2' && dataMap.FUNDT != '1'}">
												<label class="radio-block">
													<input type="radio" value="1" name="BILLSENDMODE_${status.index}"><spring:message code="LB.All"/>
		                                            <span class="ttb-radio"></span>
		                                        </label>
		                                    </td>
		                                    <td style="text-align:center;vertical-align:middle;"><input type="button" value="<spring:message code="LB.X1941"/>"
											
											onclick="checkShortTrade('${dataMap.CDNO}',
                                      		'${dataMap.TRANSCODE}',
                                      		'${dataMap.CRY}',
                                      		'${dataMap.FUNDAMT}',
                                      		'${dataMap.UNIT}',
                                      		'${status.index}',
                                      		'00000000',
                                      		'${dataMap.SHORTTUNIT}',
                                      		'${dataMap.FUNDLNAME}',
                                      		'${dataMap.COUNTRYTYPE}',
                                      		'${dataMap.FEE_TYPE}',
                                      		'${dataMap.FUSMON}')" class="ttb-sm-btn btn-flat-orange">  <!--  class="ttb-button btn-flat-orange"-->
                                      		</td>
											</c:if>
											<c:if test="${dataMap.FUNDT == '1'}">	<!-- 補格線，勿刪  -->
		                                        <td style="text-align:center;vertical-align:middle;"></td>
											</c:if>
											<c:if test="${dataMap.FEE_TYPE_CHK == '3'}">
												<font color="red">${dataMap.noticeNsg}<font>
		                                        </td>
		                                        <td></td>
											</c:if>
										</tr>
									</c:forEach>
									</c:if>
									<c:if test="${fundRows.size() <= 0}">
										<tr style="display:none;">
										</tr>
									</c:if>
								</tbody>
							</table>
						</div>
						<strong style="margin: auto"><font color="red"><spring:message code="LB.X2613"/></font></strong>
					</form>
				</div>
				<div class="text-left">
					<ol class="list-decimal description-list">
						<p><spring:message code="LB.Description_of_page"/>:</p>
						<li><spring:message code="LB.Query_Fund_Transfer_Data_P1_D1"/></li>
						<li><span><spring:message code="LB.Query_Fund_Transfer_Data_P1_D2-1"/><a href="javascript:void(0)" onclick="workTime()"><spring:message code="LB.Query_Fund_Transfer_Data_P1_D2-2"/></a></span></li>
						<li><spring:message code="LB.Query_Fund_Transfer_Data_P1_D3"/></li>
						<ul class="description-list">
							<li>(1)<spring:message code="LB.Query_Fund_Transfer_Data_P1_D4"/></li>
							<li>(2)<spring:message code="LB.Query_Fund_Transfer_Data_P1_D5"/></li>
							<li>(3)<spring:message code="LB.Query_Fund_Transfer_Data_P1_D6"/></li>
							<li>(4)<spring:message code="LB.Query_Fund_Transfer_Data_P1_D7"/></li>
						</ul>
						<li><spring:message code="LB.Query_Fund_Transfer_Data_P1_D8"/></li>
					</ol>
				</div>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>