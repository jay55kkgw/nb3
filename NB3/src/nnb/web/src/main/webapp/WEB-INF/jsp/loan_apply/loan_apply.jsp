<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<title>Insert title here</title>

<script type="text/javascript">
		var loanflag=0;
		
		//第	0層 最一開始select選單
		function view0(){
			$('#functionTitle').show();
			$('#loanType').val("");
			$('#Step1').show();
			$("#Step2").hide();
			$("#button1").hide();
		}
		//第一層 簡介及注意事項
		function view1(){
			//判斷上一頁是選擇哪種貸款
			if(loanflag==1){
				$("#step2-1").show();
				$("#step2-2").hide();
			}else{
				$("#step2-2").show();
				$("#step2-1").hide();
			}
			$('#step2-3').hide();
			$('#button2').hide();
			$('#button1').show();
		}
		//第二層 個人資料保護法告知義務告知書
		function view2(){
			$("#button1").hide();
			$("#step2-1").hide();
			$("#step2-2").hide();
			$('#step2-3').show();
			$('#button2').show();
		}
		//第三層 申請人聲明事項
		function view3(){
			$("#button2").hide();
			$("#button3").show();
			$('#step2-3').hide();
			$('#step2-4').show();
		}
		
		
	function selectAction() {
		$('#functionTitle').hide();
		$('#Step1').hide();
		$("#Step2").show();
		if ($('#loanType').val() == "B") {
			loanflag = 1;
			$("#step2-1").show();
			$("#step2-2").hide();
		} else if ($('#loanType').val() == "E") {
			loanflag = 2;
			$("#step2-2").show();
			$("#step2-1").hide();
		}
		//將頁面跳至指定ID
		$('#Step2')[0].scrollIntoView(true);
		
		$("#button1").show();
	}
	//從簡介及注意事項回最一開始select選單
	function backBtn1(){
		view0();
	}
	//從簡介及注意事項>>個人資料保護法告知義務告知書
	function goBtn1(){
		view2();
	}
	//從個人資料保護法告知義務告知書回簡介及注意事項
	function backBtn2(){
		if(	$('input[name=agreecheck1]:checked').val()=="N"||$('input[name=agreecheck1]:checked').val()=="Y"){
			$('input[name=agreecheck1]').prop("checked",false);
			view1();
		}else{
			//alert("<spring:message code= "LB.Alert133" />");
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert133' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
		}
	}
	function goBtn2(){
		if(	$('input[name=agreecheck1]:checked').val()=="N"||$('input[name=agreecheck1]:checked').val()=="Y"){
			$('input[name=agreecheck1]').prop("checked",false);
			view3();
		}else{
			//alert("<spring:message code= "LB.Alert133" />");
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert133' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
		}
	}
	
</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 貸款服務     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Loan_Service" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!-- 		主頁內容  -->
				<h2>
				<spring:message code= "LB.D0554" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<div class="main-content-block row">
				<div class="col-12 tab-content" id="nav-tabContent">
					<div class="ttb-input-block" id="Step1">
						<div class="ttb-input-item row">
							<span class="input-title" id="functionTitle"> <label>
									<h4><spring:message code= "LB.D0544" /></h4>
							</label>
							</span> 
							<span class="input-block">
								<div class="ttb-input" >
									<select id="loanType" onchange="selectAction()">
										<option value="">--<spring:message code= "LB.D0545" />--</option>
										<option value="B"><spring:message code= "LB.D0546" /></option>
										<option value="E"><spring:message code= "LB.D0547" /></option>
									</select>
								</div>
								
							</span>
						</div>
					</div>
					<div id="Step2" style="padding-top: 5%">
							<div  id="step2-1" style="display: none">
							</div>
							<div  id="step2-2" style="display: none">
							</div>
							<div  id="step2-3" style="display: none">
								<p>
								<input type=radio name="agreecheck1" value="Y">
									<spring:message code= "LB.D0548" />
								</p>
								<p>
								<input type=radio name="agreecheck1" value="N">
									<spring:message code= "LB.D0549" />
								</p>
							</div>
							<div  id="step2-4" style="display: none">
								<p>
								<input type=radio name="agreecheck2" value="Y">
									<spring:message code= "LB.D0550" />
								</p>
							</div>
					</div>
					
					<div id="button1" style="display:none">
						<input type="button" class="ttb-button btn-flat-orange"  value="<spring:message code= "LB.X0318" />" onclick="backBtn1()"/>
						<input type="button" class="ttb-button btn-flat-orange"  value="<spring:message code= "LB.X0080" />" onclick="goBtn1()"/>
					</div>
					<div id="button2" style="display:none">
						<input type="button" class="ttb-button btn-flat-orange"  value="<spring:message code= "LB.X0318" />" onclick="backBtn2()"/>
						<input type="button" class="ttb-button btn-flat-orange"  value="<spring:message code= "LB.X0080" />" onclick="goBtn2()"/>
					</div>
					<div id="button3" style="display:none">
						<input type="button" class="ttb-button btn-flat-orange"  value="<spring:message code= "LB.X0318" />"/>
						<input type="button" class="ttb-button btn-flat-orange"  value="<spring:message code= "LB.X0080" />"/>
					</div>
				</div>
			</div>
		</section>
		</main>
	</div>
</body>
</html>