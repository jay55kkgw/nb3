<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js_u2.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
<br/><br/>
<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
<br/><br/><br/>
<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
<br/><br/><br/>
<label><spring:message code="LB.Inquiry_time" />：</label><label>${CMQTIME}</label>
<br/><br/>
<label><spring:message code="LB.Total_records" />：</label><label>${COUNT} <spring:message code="LB.Rows" /></label>
<br/><br/>
<table class="print">
	<tr>
<!-- 							預約編號 -->
		<td style="text-align:center"><spring:message code="LB.Booking_number" /></td>
<!-- 							週期 -->
		<td style="text-align:center"><spring:message code="LB.Period" /></td>
<!-- 							生效日/截止日 -->
		<td style="text-align:center">
			<spring:message code="LB.Effective_date" />
			</br>
			<spring:message code="LB.Deadline" />
		</td>
<!-- 							下次轉帳日 -->
		<td style="text-align:center"><spring:message code="LB.Next_transfer_date" /></td>
<!-- 							轉出帳號 -->
		<td style="text-align:center"><spring:message code="LB.Payers_account_no" /></td>
<!-- 							轉入帳號/繳費稅代號 -->
		<td style="text-align:center">
			<spring:message code="LB.Payees_account_no" />/
			</br>
			<spring:message code="LB.Pay_taxes_fee_code" />
		</td>
<!-- 							轉帳金額 -->
		<td style="text-align:center"><spring:message code="LB.Amount" /></td>
<!-- 							交易類別 -->
		<td style="text-align:center"><spring:message code="LB.Transaction_type" /></td>
<!-- 							備註 -->
		<td style="text-align:center"><spring:message code="LB.Note" /></td>
<!-- 							交易機制 -->
		<td style="text-align:center"><spring:message code="LB.Transaction_security_mechanism" /></td>
<!-- 							狀態 -->
		<td style="text-align:center"><spring:message code="LB.Status" /></td>
<%-- 		<td style="text-align:center"><spring:message code="LB.Option" /></td> --%>
	</tr>
	<c:forEach items="${dataListMap}" var="map">
	<tr>
		<td class="text-center">${map.DPSCHNO}</td>
		<td class="text-center">${map.DPPERMTDATE}</td>
		<td class="text-center">
			${map.DPFDATE}
            <c:if test="${map.DPTDATE != ''}">
            <br>
            ${map.DPTDATE}
            </c:if>
		</td>
		<td class="text-center">${map.DPNEXTDATE}</td>
		<td class="text-center">${map.DPWDAC}</td>
		<td class="text-center">
			${map.DPSVBH}
			<br>
			${map.DPSVAC}		
		</td>
		<td style="text-align: right">${map.DPTXAMTS}</td>
		<td class="text-center">${map.TXTYPE}</td>
		<td class="text-center">${map.DPTXMEMO}</td>
		<td class="text-center">${map.DPTXCODES}</td>
		<td class="text-center">${map.DPTXSTATUS}</td>
<%-- 		<td><input type="button" value = '<spring:message code="LB.Cancel" />'/></td> --%>
	</tr>
	</c:forEach>
</table>
<br/><br/>
	<div class="text-left">
		<spring:message code="LB.Description_of_page" />
		<ol class="list-decimal text-left">
			<li><spring:message code="LB.Reservation_P1_D1" /></li>
			<li><spring:message code="LB.Reservation_P1_D3" /></li>
			<li><spring:message code="LB.Reservation_P1_D4" /></li>
		</ol>
	</div>
</body>
</html>