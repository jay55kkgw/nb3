<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function(){
// 			if(${!CJT.equals("")}){
// 				var CJT = "${CJT}";
// 				if(CJT == "1"){
// 					$("input[type=checkbox][name=CJT][value=1]").prop("checked",true);
// 				}
// 				else if(CJT == "2"){
// 					$("input[type=checkbox][name=CJT][value=2]").prop("checked",true);
// 				}
// 				else if(CJT == "3"){
// 					$("input[type=checkbox][name=CJT][value=3]").prop("checked",true);
// 				}
// 				else if(CJT == "4"){
// 					$("input[type=checkbox][name=CJT][value=4]").prop("checked",true);
// 				}
// 			}
			
			var C2 = "${C2}";
			if(C2 == "1"){
				$("input[type=checkbox][name=C2][value=1]").prop("checked",true);
			}
			else{
				$("input[type=checkbox][name=C2][value=2]").prop("checked",true);
			}
			
			var C3 = "${C3}";
			if(C3 == "1"){
				$("input[type=checkbox][name=C3][value=1]").prop("checked",true);
			}
			else{
				$("input[type=checkbox][name=C3][value=2]").prop("checked",true);
			}

			var C4 = "${C4}";
			if(C4 == "1"){
				$("input[type=checkbox][name=C4][value=1]").prop("checked",true);
			}
			else{
				$("input[type=checkbox][name=C4][value=2]").prop("checked",true);
			}
			window.print();
		});
	</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br/><br/>
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
	<br/><br/><br/>
	<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
	<br/>
	
	<div> 
		<!-- 資料 -->
		<!-- 表格區塊 -->
		<div class="ttb-input-block">
				
			<p style="text-align: center;"><spring:message code= "LB.D0120" /></p>

			<table class="print">
				<tr>
					<td style="text-align: center;width:25%;"><spring:message code= "LB.System_time" /></td>
					<td colspan="5">${DATETIME}</td>
				</tr>
				<tr>
					<td style="text-align: center" colspan="6"><spring:message code= "LB.D0105" /></td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.D0106" /></td>
					<td colspan="2">
						<c:if test="${CFU2TD.equals('2')}"><spring:message code= "LB.D0107" /></c:if>
						<c:if test="${!CFU2TD.equals('2')}"><spring:message code= "LB.X1640" /></c:if>
					</td>
					<td style="text-align: center"><spring:message code= "LB.D0108" /></td>
					<td colspan="2">${CNTD}</td>
				</tr>
				<tr>
					<td style="text-align: center" colspan="6"><spring:message code= "LB.D0109" /></td>
				</tr>
				<tr>
					<td style="text-align: center"><font color="red">＊</font><spring:message code= "LB.D0049" /></td>
					<td colspan="2">${CPRIMCHNAME}</td>
					<td style="text-align: center"><spring:message code= "LB.D0050" />（<spring:message code= "LB.D0138" />）</td>
					<td colspan="2">${CPRIMENGNAME}</td>
				</tr>
				<tr>
					<td style="text-align: center"><font color="red">＊</font><spring:message code= "LB.D0052" /></td>
					<td colspan="5">${CHENAMEChinese}</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.X2412" /></td>
					<td colspan="5">${CUSNAME}</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.X2414" /></td>
					<td colspan="5">${ROMANAME}</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.X0205" /></td>
					<td colspan="5">${CTRYDESC}</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.D0054" /></td>
					<td colspan="2">${CPRIMBIRTHDAY}</td>
					<td style="text-align: center"><spring:message code= "LB.D0055" /></td>
					<td colspan="2">${MPRIMEDUCATIONChinese}</td>
				</tr>
				<tr>
					<td style="text-align: center"><font color="red">＊</font><spring:message code= "LB.D0056" /></td>
					<td colspan="2">${CUSIDN}</td>
					<td style="text-align: center"><spring:message code= "LB.D0057" /></td>
					<td colspan="2">${MPRIMMARRIAGEChinese}</td>
				</tr>
				<!-- 身分證補換發資料 -->
				<tr>
					<td style="text-align: center; width: 16%"><font color="red">＊</font><spring:message code= "LB.X2451" /></td>
					<td style="width: 16%">${CHDATEY}<spring:message code= "LB.Year" />${CHDATEM}<spring:message code= "LB.Month" />${CHDATED}<spring:message code= "LB.D0586" /></td>
					<td style="text-align: center; width: 16%"><spring:message code= "LB.X2452" /></td>
					<td style="width: 16%">${CHANCITY}</td>
					<td style="text-align: center; width: 16%"><spring:message code= "LB.X2453" /></td>
					<td style="width: 16%">${CHANTYPEChinese}</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.D0143" /></td>
					<td colspan="5">${CPRIMADDR2}</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.D0144" /></td>
					<td colspan="5">${CPRIMHOMETELNO2A}－${CPRIMHOMETELNO2B}</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.D0063" /></td>
					<td colspan="5">${CPRIMADDR}</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.D0065" /></td>
					<td colspan="5">${CPRIMHOMETELNO1A}－${CPRIMHOMETELNO1B}</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.D0067" /></td>
					<td colspan="5">${MPRIMADDR1CONDChinese}</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.D0069" /></td>
					<td colspan="5">${CPRIMCELLULANO1}</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.D0070" /></td>
					<td colspan="5">${MBILLTOChinese}</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.D0071" /></td>
					<td colspan="5">${MCARDTOChinese}</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.Email" /></td>
					<td colspan="5">${CPRIMEMAIL}</td>
				</tr>
				<tr>
					<td style="text-align: center" colspan="6">
						<c:if test="${V3.equals('11')}"><input type="checkbox" id="V3" checked disabled/></c:if>
						<c:if test="${!V3.equals('11')}"><input type="checkbox" id="V3" disabled/></c:if>
						<spring:message code= "LB.X0555" />
						<a href="${__ctx}/term/NA03_6.pdf" target="_blank"><spring:message code= "LB.X0556" /></a>。<br/>（<spring:message code= "LB.D0153" />）
					</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.X2416" /></td>
					<td colspan="5">${PAYMONEY}<spring:message code= "LB.D0088_2" /></td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.D0076" /></td>
					<td colspan="2">${FDRSGSTAFF}</td>
					<td style="text-align: center"><spring:message code= "LB.D0077" /></td>
					<td colspan="2">${BANKERNO}</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.Note" /></td>
					<td colspan="5">${MEMO}</td>
				</tr>
				<tr>
					<td style="text-align: center" colspan="6"><b><spring:message code= "LB.W0831" /></b></td>
				</tr>
				<tr>
<%-- 					<td style="text-align: center"><spring:message code= "LB.D0155" /></td> --%>
<%-- 					<td colspan="3"> --%>
<%-- 						<input type="checkbox" name="CJT" value="1" disabled/><spring:message code= "LB.D0081" /> --%>
<%-- 						<input type="checkbox" name="CJT" value="2" disabled/><spring:message code= "LB.D0082" /> --%>
<%-- 						<input type="checkbox" name="CJT" value="3" disabled/><spring:message code= "LB.D0083" /> --%>
<%-- 						<input type="checkbox" name="CJT" value="4" disabled/><spring:message code= "LB.D0572" /> --%>
<%-- 					</td> --%>
					<td style="text-align: center"><font color="red">＊</font><spring:message code= "LB.X2437" /></td>
					<td colspan="5">
						${CPRIMJOBTYPEChinese}
					</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.D0086" /></td>
					<td colspan="2">${CPRIMCOMPANY}</td>
					<td style="text-align: center"><font color="red">＊</font><spring:message code= "LB.X2438" /></td>
					<td colspan="2">${CPRIMJOBTITLEChinese}</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.D0088_1" /></td>
					<td colspan="2">${CPRIMSALARY}<spring:message code= "LB.D0088_2" /></td>
					<td style="text-align: center"><spring:message code= "LB.D0089_1" /></td>
					<td colspan="2">
						<c:if test="${!WORKYEARS.equals('')}">
							${WORKYEARS}<spring:message code= "LB.Year" />
						</c:if>
						<c:if test="${!WORKMONTHS.equals('')}">
							${WORKMONTHS}<spring:message code= "LB.Month" />
						</c:if>
					</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.D0090" /></td>
					<td colspan="5">${CPRIMADDR3}</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code= "LB.D0094" /></td>
					<td colspan="5">${CPRIMOFFICETELNO1A}－${CPRIMOFFICETELNO1B}<spring:message code= "LB.D0095" />：${CPRIMOFFICETELNO1C}</td>
				</tr>
				<tr>
					<td style="text-align: center" colspan="6">
						1﹒<spring:message code= "LB.D0096" />
						<input type="checkbox" name="C2" value="1" disabled/><spring:message code= "LB.D0097" />
						<input type="checkbox" name="C2" value="2" disabled/><spring:message code= "LB.X0204" />　<spring:message code= "LB.X0590" /><font color="red">（<spring:message code= "LB.X0591" />）</font>。
					</td>
				</tr>
				<tr>
					<td style="text-align: center" colspan="6">
						2﹒<spring:message code= "LB.X1238" /><font color="red">（<spring:message code= "LB.X0593" />）</font>。
						<c:if test="${C5.equals('2')}"><input type="checkbox" id="C5" checked disabled/></c:if>
						<c:if test="${!C5.equals('2')}"><input type="checkbox" id="C5" disabled/></c:if><spring:message code= "LB.D0745" />。
					</td>
				</tr>
				<tr>
					<td style="text-align: center" colspan="6">
						3﹒<spring:message code= "LB.D0096" />
						<input type="checkbox" name="C3" value="1" disabled/><spring:message code= "LB.D0097" />
						<input type="checkbox" name="C3" value="2" disabled/><spring:message code= "LB.X0204" /> <spring:message code= "LB.D0102_1" /><font color="red">（未勾選將無法核發申辦之聯名/認同卡）</font>
					</td>
				</tr>
				<tr>
					<td style="text-align: center" colspan="6">
						4﹒<spring:message code= "LB.D0096" />
						<input type="checkbox" name="C4" value="1" disabled/><spring:message code= "LB.D0097" />
						<input type="checkbox" name="C4" value="2" disabled/><spring:message code= "LB.X0204" /> <spring:message code= "LB.D0103" />
					</td>
				</tr>
			</table>
		</div>
		
		<br>
		<br>
	</div>
	
</body>
</html>