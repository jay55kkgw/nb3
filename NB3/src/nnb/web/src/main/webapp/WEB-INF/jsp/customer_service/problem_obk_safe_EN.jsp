<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="page5" class="card-detail-block">
	<div class="card-center-block">
		<!-- Q1 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-51" aria-expanded="true"
				aria-controls="popup1-51">
				<div class="row">
					<span class="col-1">Q1</span>
					<div class="col-11">
						<span>How to ensure the security of online banking transactions?</span>
					</div>
				</div>
			</a>
			<div id="popup1-51" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<ol class="ttb-result-list terms">
								<li>1.Do not tell others about the various passwords of online banking.</li>
								<li>2.Please avoid setting passwords with information such as the ID number, date of birth, company or home phone, etc, that are easily known to others.</li>
								<li>3.The personal computer avoids the Browser setting the memory user ID number and password.</li>
								<li>4.Please avoid using online computers (such as Internet cafes) to perform online banking transactions, so as to prevent unscrupulous people from using the keyboard to enter the side-recording program to record the data entered by the user and steal private information such as the customer's personal account number and password.</li>
								<li>5.Please change your password periodically.</li>
								<li>6.When you leave your seat, remember to log out of the online banking system and close your browser.</li>
							</ol>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q2 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-52" aria-expanded="true"
				aria-controls="popup1-52">
				<div class="row">
					<span class="col-1">Q2</span>
					<div class="col-11">
						<span>Why do I enter the online banking page for login action, it will display the last input data, whether there is a problem with the system settings, is it caused the computer to remember the ID and password, what should I do?</span>
					</div>
				</div>
			</a>
			<div id="popup1-52" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>This is the client IE function that allows you to automatically remember without enter each time. It is not the scope of the Bank's online banking system can control. To prevent your password outflow or other reasons, please turn off this function as follows:</p>
							<p>Click “Tools” → “Internet Options” → “Contents” in the toolbar of IE. There is an “AutoComplete” button in “Personal Information”, and then click the option “User Name and Password on Form” to cancel and "clear the form", "clear the password", and then press 『OK』.</p>
							<p><font color="red">It is not recommended to use the automatic memory function to prevent the leakage of ID and password.</font></p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>