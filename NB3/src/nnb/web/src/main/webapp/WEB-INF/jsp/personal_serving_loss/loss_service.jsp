<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
</head>
 <body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 掛失服務     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0441" /></li>
		</ol>
	</nav>



	
	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
	<main class="col-12"> 
	<!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<h2><spring:message code="LB.X0441" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
					
			<form method="post" id="formId" action="" onSubmit="return processQuery()">
  				<div class="main-content-block row">
					<div class="col-12">
						<div class="ttb-input-block">
							<div class="ttb-message">
								<span><spring:message code="LB.X0442" /></span>
							</div>
    						<div class="ttb-input-item row">
								<span class="input-title" > <label><spring:message code="LB.X0443" /></label></span> 
								<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block"> <spring:message code="LB.X0444" />&nbsp;
											<input type="radio" name="reportingLoss" value="0" checked/> 
											<span class="ttb-radio"></span>
										</label>
										<label class="radio-block"> <spring:message code="LB.X0445" />&nbsp;
											<input type="radio" name="reportingLoss" value="1"/> 
											<span class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title"> <label><spring:message code="LB.X0446" /></label></span> 
								<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block"> <spring:message code="LB.X0447" />&nbsp;
											<input type="radio" name="reportingLoss" value="2" /> 
											<span class="ttb-radio"></span>
										</label>
										<label class="radio-block"> <spring:message code="LB.X0448" />&nbsp;
											<input type="radio" name="reportingLoss" value="3"/> 
											<span class="ttb-radio"></span>
										</label>
										<label class="radio-block"> <spring:message code="LB.X0449" />&nbsp;
											<input type="radio" name="reportingLoss" value="4"/> 
											<span class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>
    						<div class="ttb-input-item row">
								<span class="input-title"> <label><spring:message code="LB.X0450" /></label></span> 
								<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block"> <spring:message code="LB.X0450" />&nbsp;
											<input type="radio" name="reportingLoss" value="5" /> 
											<span class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title"> <label><spring:message code="LB.X0451" /></label></span> 
								<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block"> <spring:message code="LB.X0452" />&nbsp;
											<input type="radio" name="reportingLoss" value="6" /> 
											<span class="ttb-radio"></span>
										</label>
										<label class="radio-block"> <spring:message code="LB.X1235" />&nbsp;
											<input type="radio" name="reportingLoss" value="7" /> 
											<span class="ttb-radio"></span>
										</label>
										<label class="radio-block"> <spring:message code="LB.X0453" />&nbsp;
											<input type="radio" name="reportingLoss" value="8" /> 
											<span class="ttb-radio"></span>
										</label>
										<label class="radio-block"> <spring:message code="LB.X0454" />&nbsp;
											<input type="radio" name="reportingLoss" value="9" /> 
											<span class="ttb-radio"></span>
										</label>
										<label class="radio-block"> <spring:message code="LB.X0455" />&nbsp;
											<input type="radio" name="reportingLoss" value="10" /> 
											<span class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title"> <label><spring:message code="LB.Card_Report_Loss" /></label></span> 
								<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block"> <spring:message code="LB.Card_Report_Loss" />&nbsp;
											<input type="radio" name="reportingLoss" value="11" /> 
											<span class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>
  						</div>
  						<div style="text-align:center;">
  							<input type="hidden" name="ACN" value=" " >
  							<input type="hidden" name="TXID" value=" " >  	
  							<input type="hidden" name="CUSIDN" value=" " >
  							<input type="hidden" name="ADOPID" value="">
  							<input type="hidden" name="trancode" value=" ">
  						</div>
  						<input type="submit" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" name="CMSUBMIT" >
  					</div>
  				</div>
			</form>	

<!-- 				</form> -->
			</section>
		<!-- 		main-content END -->
		</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
<!--   Js function -->
	<script type="text/JavaScript">
	function processQuery()
	{
		var main = document.getElementById("formId");
		var fm=document.forms[0];
	
		if (fm.reportingLoss[0].checked==true ){
	 		main.TXID.value = "N841";
	 		main.trancode.value = "N8411";
	 		main.action = "${__ctx}/LOSS/SERVING/reporting_loss_deposit_slip";
		}else if (fm.reportingLoss[1].checked==true ){
	 		main.TXID.value = "N842";
	 		main.trancode.value = "N8421";
	 		main.action = "${__ctx}/LOSS/SERVING/reporting_loss_deposit_slip_fcy";
		}else if (fm.reportingLoss[2].checked==true ){
	 		main.TXID.value = "N840";
	 		main.trancode.value = "N8401";
	 		main.action = "${__ctx}/LOSS/SERVING/passbook_loss";
		}else if (fm.reportingLoss[3].checked==true ){
	 		main.TXID.value = "N843";
	 		main.trancode.value = "N8431";
			main.action = "${__ctx}/LOSS/SERVING/fcy_passbook_loss";
		}else if (fm.reportingLoss[4].checked==true ){
	 		main.TXID.value = "N845";
			main.trancode.value = "N8451";
			main.action = "${__ctx}/LOSS/SERVING/gold_passbook_loss";
        }else if (fm.reportingLoss[5].checked==true ){
	 		main.TXID.value = "N844";
			main.trancode.value = "N844";
			main.action = "${__ctx}/LOSS/SERVING/debitcard_loss";
		}else if (fm.reportingLoss[6].checked==true ){
	 		main.TXID.value = "N841";
	 		main.trancode.value = "N8412";
	 		main.action = "${__ctx}/LOSS/SERVING/deposit_slip_seal_loss";
		}else if (fm.reportingLoss[7].checked==true ){
	 		main.TXID.value = "N840";
	 		main.trancode.value = "N8402";
	 		main.action = "${__ctx}/LOSS/SERVING/passbook_seal_loss";
		}else if (fm.reportingLoss[8].checked==true ){
	 		main.TXID.value = "N842";
	 		main.trancode.value = "N8422";
			main.action = "${__ctx}/LOSS/SERVING/f_deposit_slip_seal_loss";
		}else if (fm.reportingLoss[9].checked==true ){
	 		main.TXID.value = "N843";
	 		main.trancode.value = "N8432";
	 		main.action = "${__ctx}/LOSS/SERVING/f_passbook_seal_loss";
		}else if (fm.reportingLoss[10].checked==true ){
	 		main.TXID.value = "N845";
	 		main.trancode.value = "N8452";
			main.action = "${__ctx}/LOSS/SERVING/g_passbook_seal_loss";
		}else if (fm.reportingLoss[11].checked==true ){
	 		main.TXID.value = "N816L";
	 		main.trancode.value = "N816L";
			main.action = "${__ctx}/CREDIT/APPLY/card_loss";
		}
		main.CMSUBMIT.disabled = true;
// 	  	alert("main.TXID.value"+main.TXID.value);
// 	  	alert("main.action"+main.action);
	  	main.submit();
	  	return false;
	}
	</script>
</body>
</html>
