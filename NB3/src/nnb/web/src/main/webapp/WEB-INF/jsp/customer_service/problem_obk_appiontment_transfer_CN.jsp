<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="page3" class="card-detail-block">
	<div class="card-center-block">
		<!-- Q1 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-31" aria-expanded="true"
				aria-controls="popup1-31">
				<div class="row">
					<span class="col-1">Q1</span>
					<div class="col-11">
						<span>网络银行预约转账，是否需另外临柜申请?</span>
					</div>
				</div>
			</a>
			<div id="popup1-31" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>如已约定转出账号，即具有实时转账及预约转账功能，不需另外临柜提出预约转账之申请。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q2 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-32" aria-expanded="true"
				aria-controls="popup1-32">
				<div class="row">
					<span class="col-1">Q2</span>
					<div class="col-11">
						<span>网络银行预约转账交易限额有何限制?</span>
					</div>
				</div>
			</a>
			<div id="popup1-32" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>网络银行预约转账交易限额，于转账日与实时转账合并计算当日累计转账额度。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q3 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-33" aria-expanded="true"
				aria-controls="popup1-33">
				<div class="row">
					<span class="col-1">Q3</span>
					<div class="col-11">
						<span>网络银行预约转账之预约期限?</span>
					</div>
				</div>
			</a>
			<div id="popup1-33" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>预约转账期限最长一年，至迟须于转账日之前一日完成预约；
							如预约之转账日为历法所无之日期，以该月之末日为转账日。新台币之预约转账，
							如遇例假日仍照常执行交易；外币之预约转账，如遇例假日则顺延至次一营业日执行交易。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q4 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-34" aria-expanded="true"
				aria-controls="popup1-34">
				<div class="row">
					<span class="col-1">Q4</span>
					<div class="col-11">
						<span>外币预约转账之汇率?</span>
					</div>
				</div>
			</a>
			<div id="popup1-34" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>转账汇率采实际转账日上午9：10之挂牌汇率，并比照二：【服务项目】A10提供汇率优惠。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q5 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-35" aria-expanded="true"
				aria-controls="popup1-35">
				<div class="row">
					<span class="col-1">Q5</span>
					<div class="col-11">
						<span>如何得知预约转账之转账结果?</span>
					</div>
				</div>
			</a>
			<div id="popup1-35" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>请利用网络银行「帐务查询」－「预约交易查询/取消」的「预约交易结果查询」，
							可查询最近六个月的预约交易转账结果，如查询转账日当日转账结果，请于当日上午10:30后执行，
							才为当日转账之最终结果（不成功交易将于当日上午10:00再次执行转账作业）。对于已登录「我的Email」者，
							将传送转账结果至贵户的电子邮箱，故务请登录电子邮箱，如有变更邮箱地址，亦请更新以利传送讯息。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q6 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-36" aria-expanded="true"
				aria-controls="popup1-36">
				<div class="row">
					<span class="col-1">Q6</span>
					<div class="col-11">
						<span>网络银行签入密码、交易密码变更或密码重置，已登录之预约转账是否仍属有效?</span>
					</div>
				</div>
			</a>
			<div id="popup1-36" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>密码变更及重置，已登录之预约转账仍属有效，将于转账日依预约指示执行转账作业。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q7 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-37" aria-expanded="true"
				aria-controls="popup1-37">
				<div class="row">
					<span class="col-1">Q7</span>
					<div class="col-11">
						<span>网络银行用户名称、签入密码或交易密码输入错误超过次数致遭系统停用，已登录之预约转账是否仍属有效?</span>
					</div>
				</div>
			</a>
			<div id="popup1-37" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>上列数据输入错误超过次数致遭系统停用时，原已登录之预约转账仍属有效，将于转账日依预约指示执行转账作业。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q8 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-38" aria-expanded="true"
				aria-controls="popup1-38">
				<div class="row">
					<span class="col-1">Q8</span>
					<div class="col-11">
						<span>电子凭证更新不成功、注销或到期，已登录之电子签章预约转账交易是否仍属有效?</span>
					</div>
				</div>
			</a>
			<div id="popup1-38" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>电子凭证更新不成功、注销或到期，已登录之电子签章预约转账交易仍属有效，将于转账日依预约指示执行转账作业。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q9 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-39" aria-expanded="true"
				aria-controls="popup1-39">
				<div class="row">
					<span class="col-1">Q9</span>
					<div class="col-11">
						<span>已成功登录网络银行预约转账交易，如何办理取消预约转账?</span>
					</div>
				</div>
			</a>
			<div id="popup1-39" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>客户如拟取消预约转账，至迟应于预约之转账日前一日使用网络银行完成取消预约转账程序。
							以交易密码交易(SSL)做的预约转账，须以交易密码交易取消；以电子签章交易(i-Key)做的预约转账，
							须以电子签章交易取消。若客户用户名称、密码输入错误超过次数遭系统停用或凭证更新不成功、注销、到期致凭证无法使用，
							原已登录之预约转账仍属有效，如拟取消预约转账而因前述原因无法进入网络银行取消时，应亲赴网络银行开户行申办取消未届转账日之全部预约转账。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q10 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-310" aria-expanded="true"
				aria-controls="popup1-310">
				<div class="row">
					<span class="col-1">Q10</span>
					<div class="col-11">
						<span>申办注销已约定转出账号或转入账号，已登录之预约转账是否仍属有效?</span>
					</div>
				</div>
			</a>
			<div id="popup1-310" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>客户临柜申办注销网络银行转出账号，该注销账号下未届转账日之预约转账均停止转账作业；注销转入账号，未届转账日之预约转入已约定该账号即停止转账作业。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>