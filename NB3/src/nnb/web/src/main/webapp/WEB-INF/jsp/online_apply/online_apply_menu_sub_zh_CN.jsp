<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp"%>
	<link rel="stylesheet" type="text/css"
		href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript"
		src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript"
		src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript"
		src="${__ctx}/js/jquery.datetimepicker.js"></script>
		<script type="text/JavaScript">
			$(document).ready(function () {
				
				$('#applyTable').DataTable({
					"bPaginate" : false,
					"bLengthChange": false,
					"scrollX": true,
					"sScrollX": "99%",
					"scrollY": "80vh",
			        "bFilter": false,
			        "bDestroy": true,
			        "bSort": false,
			        "info": false,
			        "scrollCollapse": true,
			    });
				
				$(window).bind('resize', function (){
					$('#applyTable').DataTable({
						"bPaginate" : false,
						"bLengthChange": false,
						"scrollX": true,
						"sScrollX": "99%",
						"scrollY": "80vh",
				        "bFilter": false,
				        "bDestroy": true,
				        "bSort": false,
				        "info": false,
				        "scrollCollapse": true,
				    });
				});

				
			});
			
		</script>
	</head>
	<body >
		<!-- 功能清单及登入资讯 -->
		<div class="content row">
			<!-- 快速选单及主页内容 -->
			<main class="col-12">
				<!-- 主页内容  -->
				<section id="main-content" class="container" >
					<!-- 功能名称 -->
					<h2>线上申请</h2>
					<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
					<!-- 功能内容 -->
					<div class="main-content-block row">
						<div class="col-12">
							<div class="CN19-1-header">
								<div class="logo">
									<img src="${__ctx}/img/logo-1.svg">
								</div>
							</div>
						</div>
						<!-- 如何申请 页面 -->
						<div class="col-12 tab-content" id="CN19-how-apply">
						<div id="MESSAGE" style="margin: auto">
							<font color="red" size="5">
					      	<B>申请方式</B><br>
				      		</font>
							</div>
							<div class="ttb-input-block">
								<div class="CN19-caption1">
									壹、网路银行申请方式及服务功能
								</div>
								<div class="CN19-caption2">
									<span style="FONT-FAMILY: Wingdings;">u</span>
									<span>申请方式</span>
								</div>
								<div class="CN19-caption3">
									<ul>
										<li>线上申请：限个人户</li>
									</ul>
									<div>
										本行芯片金融卡存户：<br>
 										未曾临柜申请之存户，可线上申请<font color="#FF6600">查询</font>服务，请使用<b>本行芯片金融卡 + 芯片读卡机</b> ，直接线上申请网路银行，如该芯片金融卡已具备非约定转帐功能者，得线上申请「金融卡线上申请/取消交易服务功能」，执行新台币非约定转帐交易、缴费稅交易、线上更改通讯地址/电话及使用者名称/簽入密码/交易密码线上解锁等功能。<br>
										<br>
										本行信用卡正卡客户（不含商务卡、采购卡、附卡及VISA金融卡）：<br>
 										请您直接线上申请网路银行，只要输入个人验证资料，即可立刻使用网路银行信用卡相关服务。<br>
 										<br>
									</div>
									<ul>
										<li>临柜申请：</li>
									</ul>
									<div>
										法人存户：<br>
 										请负责人携带<b>公司登记证件、身分证、存款印鉴及存摺</b>至开户行申请网路银行。<br>
										<br>
										个人存户：<br>
 										请本人携带<b>身分证、存款印鉴及存摺</b>至开户行申请网路银行，全行收付户（即通储户），亦可至各营业单位申请。<br>
 										<br>
									</div>
								</div>
								<div class="CN19-caption2">
									<span style="FONT-FAMILY: Wingdings;">u</span>
									<span>服务功能</span>
								</div>
								<div>
									<table id="applyTable" class="stripe table-striped ttb-table dtable" data-toggle-column="first">
										<thead>				
											<tr>
												<!-- 功能比较-->
												<th>功能比较</th>
												<!-- 临柜申请-->
	 											<th>临柜申请</th>
												<!-- 芯片金融卡线上申请-->
												<th>芯片金融卡线上申请</th>
												<!-- 芯片金融卡线上申请且线上申请交易服务功能 -->
												<th>芯片金融卡线上申请且线上申请交易服务功能</th>
												<!-- 信用卡线上申请 -->
												<th>信用卡线上申请</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>
													信用卡帐务查询
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
											</tr>
											<tr>
												<td>
													信用卡电子帐单
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
											</tr>
											<tr>
												<td>
													补寄信用卡帐单
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
											</tr>
											<tr>
												<td>
													信用卡缴款Email通知
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
											</tr>
											<tr>
												<td>
													存、放款、外汇帐务查询
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													基金查询
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													黄金存折查询
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													黄金申购/回售及变更
												</td>
												<td>O(注1)</td>
												<td>X</td>
												<td>X</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													转帐、缴费稅、定存
												</td>
												<td>O</td>
												<td>X</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													新台币非约定转帐(注2)
												</td>
												<td>O</td>
												<td>X</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													基金申购及变更
												</td>
												<td>O</td>
												<td>X</td>
												<td>X</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													申请代扣缴费用
												</td>
												<td>O</td>
												<td>X</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													掛失服务
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													变更通讯资料(注3)
												</td>
												<td>O</td>
												<td>X</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													使用者名称/簽入/交易密码线上解锁(注4)
												</td>
												<td>O</td>
												<td>X</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													理财试算
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
											</tr>
											<tr>
												<td>
													国内台币汇入汇款通知设定
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>X</td>
											</tr>
										</tbody>
									</table>
									</div>
									<div>
										<b>备注：</b><br>
										<ol class="list-decimal">
											<li>临柜申请黄金存折帐户客户，需於临柜或迳自本行网路银行申请黄金存折网路交易功能，始得执行黄金申购/回售及变更等功能。</li>
											<li>倘已线上申请交易功能者，且该芯片金融卡已具备非约定转帐功能者，并得以本人帐户之芯片金融卡之主帐号为转出帐户，执行新台币非约定转帐交易、缴费稅交易。</li>
											<li>交易机制为「电子簽章」或「芯片金融卡」，即可执行线上更改通讯地址/电话。</li>
											<li>交易机制为「电子簽章」或「芯片金融卡」，即可执行线上簽入/交易密码线上解锁。</li>
											<li>O：表示可使用功能，X：表示不可使用功能。(原注2)</li>
										</ol>
									</div>
								<div class="CN19-caption1">
									贰、黄金存折线上申请资格、服务功能及费用收取
								</div>
								<div class="CN19-caption2">
									<span style="FONT-FAMILY: Wingdings;">u</span>
									<span>线上申请黄金存折帐户</span>
								</div>
								<div class="CN19-caption3">
									<ul>
										<li>申请资格</li>
									</ul>
									<div>
										年满20岁之本国人。<br>
 										已申请使用本行网路银行且已约定新台币活期性存款（不含支票存款）帐户为转出帐号者。<br>
										已申请使用本行芯片金融卡＋芯片读卡机或电子簽章（凭证载具）。<br>
									</div>
								</div>
								<div class="CN19-caption2">
									<span style="FONT-FAMILY: Wingdings;">u</span>
									<span>线上申请黄金存折网路交易功能</span>
								</div>
								<div class="CN19-caption3">
									<ul>
										<li>申请资格</li>
									</ul>
									<div>
										年满20岁之本国人。<br>
 										已申请使用本行网路银行且已约定新台币活期性存款（不含支票存款）帐户为转出帐号者。<br>
										已申请使用本行芯片金融卡＋芯片读卡机或电子簽章（凭证载具）。<br>
									</div>
								</div>
								<div class="CN19-caption3">
									<ul>
										<li>服务功能</li>
									</ul>
									<div>
										查询服务：黄金存折余额、明细、当日、历史价格查询功能。<br> 
 										交易服务：黄金买进、回售、缴纳定期扣款失败手续费功能。<br>
										预约服务：预约买进、回售、取消、查询功能。<br>
									 	定期定额服务：定期定额申购、定期定额变更、定期定额查询功能。<br>
									 	线上申请：线上申请黄金存折帐户、黄金存折网路交易功能。<br>
									</div>
								</div>
								<div class="CN19-caption3">
									<ul>
										<li>费用收取</li>
									</ul>
									<div>
										线上「申请黄金存折帐户」及「定期定额投资」扣帐成功，收取新台币50元手续费，其余交易免收。<br>
									</div>
								</div>
							</div>
						</div>
					</div>
					</form>
					</div>
				</section>
			</main>
		</div>
	</body>

</html>