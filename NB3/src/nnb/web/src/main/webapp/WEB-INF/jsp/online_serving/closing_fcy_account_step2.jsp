<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
		//表單驗證
		$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
		// 確認鍵 click
		submit();
	}
	
		// 確認鍵 click
	function submit()
	{
			$("#CMSUBMIT").click(function (e) {
				if (!$('#formId').validationEngine('validate')) {
					e = e || window.event; // for IE
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					initBlockUI(); //遮罩
					$("#formId").submit();
				}
			});
	}
</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 外匯存款帳戶結清銷戶申請    -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0299" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.X0299" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
					<form autocomplete="off" method="post" id="formId" action="${__ctx}/ACCOUNT/CLOSING/closing_fcy_account_step3">
				<c:set var="BaseResultData" value="${closing_fcy_account_step2.data}"></c:set>
                <input type="hidden" name="previousPageJson" id="previousPageJson" value='${BaseResultData.previousPageJson}'>
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
                             <div class="ttb-message">
                                <span><spring:message code="LB.D0449"/></span>
                            </div>
							<!--交易日期-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.Transaction_date"/></h4>
                                    </label></span>
                                <span class="input-block">
                                    <div class="ttb-input">
										<span>${BaseResultData.CMQTIME}</span>
									</div>
                                </span>		
							</div>
							<!--外匯活存帳號-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.D0451"/></h4>
                                    </label></span>
                                <span class="input-block">
                                    <div class="ttb-input">
										<span>${BaseResultData.FYACN}</span>
									</div>
                                </span>		
							</div>
							<!--幣別-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.Currency"/></h4>
                                    </label></span>
                                <span class="input-block">
                                    <div class="ttb-input">
										<span>${BaseResultData.OUT_CRY}</span>
									</div>
                                </span>		
							</div>
							
							<!--金額-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.Deposit_amount_1"/></h4>
                                    </label></span>
                                <span class="input-block">
                                    <div class="ttb-input">
										<span>${BaseResultData.display_AMTDHID}</span>
									</div>
                                </span>		
							</div>
							<!--利息-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.Interest"/></h4>
                                    </label></span>
                                <span class="input-block">
                                    <div class="ttb-input">
										<span>${BaseResultData.display_INT}</span>
									</div>
                                </span>		
							</div>
							<!--稅款-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.D0455"/></h4>
                                    </label></span>
                                <span class="input-block">
                                    <div class="ttb-input">
										<span>${BaseResultData.display_FYTAX}</span>
									</div>
                                </span>		
							</div>
							<!--補充保險費-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.D0456"/></h4>
                                    </label></span>
                                <span class="input-block">
                                    <div class="ttb-input">
										<span>${BaseResultData.display_FYNHITAX}</span>
									</div>
                                </span>		
							</div>
							<!--稅後本息-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.After-tax_principal_and_interest"/></h4>
                                    </label></span>
                                <span class="input-block">
                                    <div class="ttb-input">
										<span>${BaseResultData.display_AMTDHID}</span>
									</div>
                                </span>		
							</div>		
						</div>
                        <input class="ttb-button btn-flat-orange" id="CMSUBMIT" name="CMSUBMIT" type="button" value="<spring:message code="LB.X0080"/>"/>
                    </div>
                </div>
				</form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>