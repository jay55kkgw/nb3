<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
	
	<script type="text/JavaScript">
		var idgatesubmit= $("#formId");		
		$(document).ready(function() {
			// HTML載入完成後開始遮罩
			initBlockUI();
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 初始化驗證碼
			setTimeout("initKapImg()", 200);
			// 生成驗證碼
			setTimeout("newKapImg()", 300);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});

		// 畫面初始化
		function init() {
			// 表單驗證初始化
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });

			// 顯示預約或即時
			nowOrBooking();
			// 確認鍵 click
			goOn();
			// 上一頁按鈕 click
			goBack();
			// 判斷顯不顯示驗證碼
			chaBlock();
			// 交易機制 click
			fgtxwayClick();

		}

		// 顯示預約或即時
		function nowOrBooking() {
			var i18n = new Object();
			if ("${transfer_data.data.FGTXDATE}" == "1") {
				i18n['Immediately'] = '<spring:message code="LB.Immediately" />';
				$("#FGTXDATE").html(i18n['Immediately']);
			} else {
				i18n['Booking'] = '<spring:message code="LB.Booking" />';
				$("#FGTXDATE").html(i18n['Booking']);
			}
		}

		// 確認鍵 Click
		function goOn() {
			$("#CMSUBMIT").click( function(e) {
				// 送出進表單驗證前將span顯示
				$("#hideblock").show();
				
				console.log("submit~~");
	
				// 表單驗證
				if ( !$('#formId').validationEngine('validate') ) {
					e.preventDefault();
				} else {
					// 解除表單驗證
					$("#formId").validationEngine('detach');
					
					// 通過表單驗證
					processQuery();
				}
			});
		}

		// 通過表單驗證準備送出
		function processQuery(){
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
			// 交易機制選項
			switch(fgtxway) {
				case '1':
					// IKEY
					useIKey();
					
					break;
				case '2':
					// 晶片金融卡
					var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
					useCardReader(capUri);
					
			    	break;
		       case '7'://IDGATE認證		 
		               idgatesubmit= $("#formId");		 
		               showIdgateBlock();		 
		               break;
				default:
					//請選擇交易機制
					//alert("<spring:message code= "LB.Alert001" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert001' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			}
		}
		
		// 上一頁按鈕 click
		function goBack() {
			// 上一頁按鈕
			$("#pageback").click(function() {
				// 回上一頁到輸入頁ESAPI驗證jsondc會壞掉故不送jsondc
				$("#jsondc").prop("disabled",true);
				
				// 遮罩
				initBlockUI();
				
				// 解除表單驗證
				$("#formId").validationEngine('detach');
				
				// 讓Controller知道是回上一頁
				$('#back').val("Y");
				// 回上一頁
				var action = '${pageContext.request.contextPath}' + '${previous}';
				$("#formId").attr("action", action);
				$("#formId").submit();
			});
		}

		// 執行選項
	 	function formReset() {
	 		if ($('#actionBar').val()=="reEnter"){
		 		document.getElementById("formId").reset();
		 		$('#actionBar').val("");
	 		}
		}

		// 使用者選擇晶片金融卡要顯示驗證碼區塊
	 	function fgtxwayClick() {
	 		$('input[name="FGTXWAY"]').change(function(event) {
	 			// 判斷交易機制決定顯不顯示驗證碼區塊
	 			chaBlock();
			});
		}
	 	// 判斷交易機制決定顯不顯示驗證碼區塊
		function chaBlock() {
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
 			console.log("fgtxway: " + fgtxway);
			// 交易機制選項
			if(fgtxway == '2'){
				// 若為晶片金融卡才顯示驗證碼欄位
				$("#chaBlock").show();
			}else{
				// 若非晶片金融卡則隱藏驗證碼欄位
				$("#chaBlock").hide();
			}
	 	}

		// 驗證碼刷新
		function changeCode() {
			$('input[name="capCode"]').val('');
			// 大小版驗證碼用同一個
			console.log("changeCode...");
			$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

			// 登入失敗解遮罩
			unBlockUI(initBlockId);
		}
		
		// 初始化驗證碼
		function initKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
		}
		
		// 生成驗證碼
		function newKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').click(function() {
				$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
			});
		}
		
	</script>
</head>
<body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 臺北自來水事業處     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0468" /></li>
		</ol>
	</nav>


	<!-- 功能清單及登入資訊 -->
	<div class="content row">
	
		<!-- 功能清單 -->
		<%@ include file="../index/menu.jsp"%>

		<!-- 快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
				<form method="post" id="formId" name="formId" action="${__ctx}/PAY/EXPENSE/water_tpe_fee_result">
					<input type="hidden" name="TXTOKEN" value="${transfer_data.data.TXTOKEN}"><!-- 防止不正常交易 -->
					<input type="hidden" name="FGTXDATE" value="${transfer_data.data.FGTXDATE}"><!-- 即時或預約 -->
					<input type="hidden" id="back" name="back" value=""><!-- 回上一頁資料 -->
					<!-- 交易機制所需欄位 -->
					<input type="hidden" id="jsondc" name="jsondc" value='${transfer_data.data.jsondc}'>
					<input type="hidden" id="ISSUER" name="ISSUER" value="">
					<input type="hidden" id="ACNNO" name="ACNNO" value="">
					<input type="hidden" id="TRMID" name="TRMID" value="">
					<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
					<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
					<input type="hidden" id="TAC" name="TAC" value="">
					<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
					
					<!-- 功能名稱 -->
					<h2><spring:message code="LB.W0468" /></h2>
					<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
					
					<!-- 下拉式選單-->
					<div class="print-block no-l-display-btn">
						<select class="minimal" id="actionBar" onchange="formReset()">
							<!-- 執行選項-->
							<option>
								<spring:message code="LB.Execution_option" />
							</option>
							<!-- 重新輸入-->
							<option value="reEnter">
								<spring:message code="LB.Re_enter" />
							</option>
						</select>
					</div>
					
					<!-- 交易流程階段 -->
					<div id="step-bar">
						<ul>
							<li class="finished"><spring:message code="LB.Enter_data" /></li>
							<li class="active"><spring:message code="LB.Confirm_data" /></li>
							<li class=""><spring:message code="LB.Transaction_complete" /></li>
						</ul>
					</div>
					
					<!-- 功能內容 -->
					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-input-block">
								<div class="ttb-message">
									<p id="FGTXDATE"></p>
									<!-- 即時繳費-->
									<c:if test = "${transfer_data.data.FGTXDATE == 1}">
										<span><spring:message code="LB.Confirm_transfer_data" /></span>
									</c:if>
									<!-- 預約繳費-->
									<c:if test = "${transfer_data.data.FGTXDATE == 2}">
										<span><spring:message code="LB.Confirm_the_Scheduled_Transaction_data" /></span>
									</c:if>
								</div>
								
								<!-- 轉帳日期 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Transfer_date" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>${transfer_data.data.transfer_date }</p>
										</div>
									</span>
								</div>
								
								<!-- 轉出帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Payers_account_no" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>${transfer_data.data.OUTACN }</p>
										</div>
									</span>
								</div>
								
								<!-- 條碼一 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0470" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>${transfer_data.data.BARCODE1 }</p>
										</div>
									</span>
								</div>
								
								<!-- 條碼二 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0472" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>${transfer_data.data.BARCODE2 }</p>
										</div>
									</span>
								</div>
								
								<!-- 條碼三 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0474" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>${transfer_data.data.BARCODE3 }</p>
										</div>
									</span>
								</div>
								
								<!-- 水號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0476" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>${transfer_data.data.WAT_NO }</p>
										</div>
									</span>
								</div>
								
								<!-- 繳款金額 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Payment_amount" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p class="high-light">
												<!-- 新臺幣 -->
												<span class="input-unit">
													<spring:message code="LB.NTD" />
												</span>
												${transfer_data.data.AMOUNT }
												<!-- 元 -->
												<span class="input-unit">
													<spring:message code="LB.Dollar" />
												</span>
											</p>
										</div>
									</span>
								</div>

								<!-- 交易機制 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Transaction_security_mechanism" /></h4>
										</label>
									</span>
									<!-- 交易機制選項 -->
									<span class="input-block">

								<!-- 使用者是否可以使用IKEY -->
								<c:if test = "${sessionScope.isikeyuser}">
									<!-- 即使使用者可以用IKEY，若轉出帳號為晶片金融卡帶出的非約定帳號，也不給用 (By 景森)-->
									<c:if test = "${transfer_data.data.TransferType != 'NPD'}">
									
										<!-- IKEY -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Electronic_signature" />
												<input type="radio" name="FGTXWAY" id="CMIKEY" value="1">
												<span class="ttb-radio"></span>
											</label>
										</div>
										
									</c:if>
								</c:if>
								   <div class="ttb-input" name="idgate_group" style="display:none" onclick="hideCapCode('Y')">		 
                                       <label class="radio-block">裝置推播認證(請確認您的行動裝置網路連線是否正常，及推播功能是否已開啟)
	                                       <input type="radio" id="IDGATE" 	name="FGTXWAY" value="7"> 
	                                       <span class="ttb-radio"></span>
                                       </label>		 
                                   </div>
										<!-- 晶片金融卡 -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Financial_debit_card" />
												
												<!-- 只能選晶片金融卡時，預設為非勾選讓使用者點擊後顯示驗證碼-->
												<c:choose>
													<c:when test="${transfer_data.data.TransferType == 'NPD'}">
														<input type="radio" name="FGTXWAY" id="CMCARD" value="2" checked="checked">
													</c:when>
													<c:when test="${!sessionScope.isikeyuser}">
														<input type="radio" name="FGTXWAY" id="CMCARD" value="2" checked="checked">
													</c:when>
													<c:otherwise>
													 	<input type="radio" name="FGTXWAY" id="CMCARD" value="2">
													</c:otherwise>
												</c:choose>
												
												<span class="ttb-radio"></span>
											</label>
										</div>
										
									</span>
								</div>
								
								<!-- 如果是非約定則不能使用IKEY，只能選晶片金融卡，使用者選擇晶片金融卡後才顯示驗證碼-->
								<div class="ttb-input-item row" id="chaBlock" style="display:none">
									<!-- 驗證碼 -->
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Captcha" /></h4>
										</label>
									</span>
									<span class="input-block">
	                                    <div class="ttb-input">
	                                        <img name="kaptchaImage" class="verification-img" src="" align="top" id="random" />
	                                        <button type="button" class="btn-flat-gray" name="reshow" onclick="changeCode()">
												<spring:message code="LB.Regeneration" /></button>
	                                    </div>
	                                    <div class="ttb-input">
	                                        <input id="capCode" name="capCode" type="text"
												class="text-input" maxlength="6" autocomplete="off">
	                                        <br>
	                                        <span class="input-unit"><spring:message code="LB.Captcha_refence" /></span>
	                                    </div>
	                                </span>
								</div>
								
							</div><!-- ttb-input-block end -->
							
							
							<input type="button" id="pageback" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />" />
							<input type="reset"  class="ttb-button btn-flat-gray no-l-disappear-btn" value="<spring:message code="LB.Re_enter" />" />
							<input type="button" id="CMSUBMIT" name="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
						</div>
					</div>
				</form>
			</section>
		</main>
	</div>

	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>