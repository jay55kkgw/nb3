<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 投資屬性評估調查表－（法人版）/投資屬性評估調查表－（自然人版）     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0855" />－${type}</li>
		</ol>
	</nav>
	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.D0855" />－${type}</h2>
				<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
				<br/><br/><br/>
				<h4 style="text-align:center"><font color="red"><b><spring:message code="LB.D0944" />：${FDINVTYPEChinese}</b></font></h4>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>