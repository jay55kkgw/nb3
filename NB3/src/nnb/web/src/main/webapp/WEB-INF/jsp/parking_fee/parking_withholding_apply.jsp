<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>

<script type="text/javascript">
$(document).ready(function () {
    initFootable(); // 將.table變更為footable 
    init();
});

function init() {
	$("#formId").validationEngine({binded: false,promptPosition: "inline"});
	$("#pageshow").click(function(e){

// 	      if($('#CarKind').val()=="C")
// 	      {
// 			if(!(	($('#CarId1').val().length==2 && $('#CarId2').val().length==4) || 
// 					($('#CarId1').val().length==4 && $('#CarId2').val().length==2) || 
// 					($('#CarId1').val().length==3 && $('#CarId2').val().length==2) || 
// 					($('#CarId1').val().length==2 && $('#CarId2').val().length==3) || 
// 					($('#CarId1').val().length==2 && $('#CarId2').val().length==2) || 
// 					($('#CarId1').val().length==3 && $('#CarId2').val().length==3) || 
// 					($('#CarId1').val().length==3 && $('#CarId2').val().length==4))) 
// 			{
// 			 alert("汽車車號檢核錯誤");
// 			 return false;
// 			} 
// 		  } 
// 	      else if($('#CarKind').val()=="M")
// 		  {
// 			if(!(	($('#CarId1').val().length==2 && $('#CarId2').val().length==2) || 
// 					($('#CarId1').val().length==3 && $('#CarId2').val().length==3) || 
// 					($('#CarId1').val().length==3 && $('#CarId2').val().length==4) || 
// 					($('#CarId1').val().length==2 && $('#CarId2').val().length==3))) 
// 			{
// 			 alert("機車車號檢核錯誤");
// 			 return false;
// 			} 	   
// 		  }		  
		if (!$('#formId').validationEngine('validate')) {
			e.preventDefault();
		} else {
			$("#ZoneName").val($("#ZoneCode").find(":selected").text());
			$("#formId").validationEngine('detach');
        	initBlockUI();
        	var action = '${__ctx}/PARKING/FEE/parking_withholding_apply_detile';
			$("form").attr("action", action);
			$("form").submit();
		}
	});
	$("#CMRESET").click(function(e){
		$("#formId")[0].reset();
	});
	
	//若前頁有帶acn，轉出預設為此acn
	var getacn = '${result_data.data.Acn}';
	if(getacn != null && getacn != ''){
		$("#Account option[value= '"+ getacn +"' ]").prop("selected", true);
	}
}

function ValidateValue(textbox) {
	var IllegalString = "+-_＿︿%@[`~!#$^&*()=|{}':;',\\[\\].<>/?~！#￥……&*（）——|{}【】‘；：”“'。，、？]‘'";
	var textboxvalue = textbox.value;
	var index = textboxvalue.length - 1;
	var s = textbox.value.charAt(index);
	if (IllegalString.indexOf(s) >= 0) {
		s = textboxvalue.substring(0, index);
		textbox.value = s;
	}
}
</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 自動扣繳服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2360" /></li>
    <!-- 停車費     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.D0330" /></li>
    <!-- 停車費代扣繳申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0276" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.X0276"/></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form id="formId" method="post" action="">
					<input type="hidden" name="action" value="forward">
				    <input type="hidden" name="ADOPID" value="N614_1">
				    <input type="hidden" name="CMQTIME" value="${result_data_p018.data.CMQTIME}">
				    <input type="hidden" name="ZoneName" id="ZoneName">
	                <div class="main-content-block row">
	                    <div class="col-12 tab-content">
	                        <div class="ttb-input-block">
	                            
	                             <!--扣款帳號-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title"><label>
	                                        <h4><spring:message code="LB.Data_time"/></h4>
	                                    </label></span>
	                                <span class="input-block">
	                                        <div class="ttb-input">
	                                        	<lable>${result_data_p018.data.CMQTIME}</lable>
	                                       </div>
									</span>
								</div>
	                            
	                             <!--扣款帳號-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title"><label>
	                                        <h4><spring:message code="LB.D0168"/></h4>
	                                    </label></span>
	                                <span class="input-block">
	                                        <div class="ttb-input">
	                                        <select class="custom-select select-input half-input  validate[required,funcCall[validate_CheckSelect[<spring:message code= 'LB.D0168' />,Account,#]]]" name="Account" id="Account">
												<option value="#" selected> <spring:message code= 'LB.Select_account' /></option>
												<c:forEach var="dataList" items="${result_data.data.REC}">
													<option value="${dataList.ACN}"> ${dataList.ACN}</option>
												</c:forEach>
	                                        </select>
	                                       </div>
									</span>
								</div>
	                                   
	   							
	                             <!--縣市別-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title"><label>
	                                        <h4><spring:message code="LB.D0059"/></h4>
	                                    </label></span>
	                                <span class="input-block">
	                                        <div class="ttb-input">
											<select name="ZoneCode" id="ZoneCode" class="custom-select select-input half-input  validate[required,funcCall[validate_CheckSelect[<spring:message code= 'LB.D0059' />,ZoneCode,#]]]">
												<option value="#" selected><spring:message code= 'LB.X0545' /></option>
												<c:forEach var="dataList" items="${result_data_p018.data.REC_sort}">
													<option value="${dataList.ZoneCode}"> ${dataList.ZoneName}</option>
												</c:forEach>
											</select>
	                                       </div>
									</span>
								</div>
								
	   							 <!-- 車號 -->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                    <label>
	                                        <h4><spring:message code="LB.D0342"/></h4>
	                                    </label>
	                                </span>
	                                <span class="input-block" >
	                                    <div class="ttb-input">
				                            <input class="text-input card-input" type="text" name="CarId1" id="CarId1" maxLength="4" size="4">－
				                            <input class="text-input card-input" type="text" name="CarId2" id="CarId2" maxLength="4" size="4">
	                                    </div> 
										<!-- 驗證用的span預設隱藏 -->
										<span id="hideblock_CarId" >
											<input id="validate_CarId" name="validate_CarId" type="text" class="text-input
												validate[funcCallRequired[validate_chkCarId['sFieldName', CarKind, CarId1, CarId2]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</span>
	                                </span>
	                            </div> 
	                            
								<!-- 車種 -->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                    <label>
	                                        <h4><spring:message code="LB.D0343"/></h4>
	                                    </label>
	                                </span>
	                                <span class="input-block" >
	                                   <div class="ttb-input">
	                                        <select class="custom-select select-input half-input validate[required,funcCall[validate_CheckSelect[<spring:message code= 'LB.D0343' />,CarKind,#]]]" name="CarKind" id="CarKind">
												<option value="#" selected><spring:message code="LB.X2383" /></option>
												<option value="C"><spring:message code="LB.D0344"/></option>
        										<option value="M"><spring:message code="LB.D0345"/></option>
	                                        </select>
	                                    </div>
	                                </span>
	                            </div> 
	                         
								<!--投保單位電話區域碼 -->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                    <label>
	                                        <h4><spring:message code="LB.Mail_address"/> </br><spring:message code="LB.W1412"/></h4>
	                                    </label>
	                                </span>
	                                <span class="input-block" >
	                                    <div class="ttb-input">
				                           <input class="text-input  validate[required,funcCall[validate_EmailCheck[Email]]" type="text" name="Email" id="Email" value="${result_data.data.EMail}">  
	                                    </div>
	                                </span>
	                            </div> 
								
	                        </div>
	                        <!--button 區域 -->
	                        
								<!-- 重新輸入 -->
								<spring:message code="LB.Re_enter" var="cmRest"></spring:message>
								<input type="button" name="CMRESET" id="CMRESET" value="${cmRest}" class="ttb-button btn-flat-gray">
	                            <input id="pageshow" name="pageshow" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm_1"/>" />
	                        
	                       	<!-- button 區域 -->
	                    </div>
	                </div>
				</form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>