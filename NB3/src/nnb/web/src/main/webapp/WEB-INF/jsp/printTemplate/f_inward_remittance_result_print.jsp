<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
  <title>${jspTitle}</title>
  <script type="text/javascript">
    $(document).ready(function () {
      window.print();
    });
  </script>
</head>
<body calss="watermark" style="-webkit-print-color-adjust: exact">
	<br /><br />
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif" /></div>
	<br /><br />
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<br />
	
	<div> 
		<!-- 查詢時間 -->
		<p>
			<spring:message code="LB.Inquiry_time" /> : ${CMQTIME}
		</p>
		<!-- 查詢期間 -->
		<p>
			<spring:message code="LB.Inquiry_period" /> : ${CMPERIOD}
		</p>
		<!-- 查詢帳號 : -->
        <p>
			<spring:message code= "LB.W0132" /> :
        	${ACN}
		</p>
		<!-- 資料總數 : -->
		<p>
			<spring:message code="LB.Total_records" /> : ${CMRECNUM}
			<!--筆 -->
			<spring:message code="LB.Rows" />
		</p>
		<!-- 匯入金額總金額 : -->
		
		
		<!-- 資料Row -->
		
		<table class="print">
			<tr>
				<!--交易編號-->
				<th>
					<!--<spring:message code="LB.Change_date" />-->
					<spring:message code= "LB.Transaction_Number" />
				</th>
				<!--入帳帳號-->
				<th>
					<!--<spring:message code="LB.Change_date" />-->
					<spring:message code= "LB.W0135" />
                </th>
				<!-- 匯款人 -->
				<!-- 匯款銀行名稱 / SWIFT CODE -->
				<th>
					<!--  <spring:message code="LB.Summary_1" /> -->
					<spring:message code= "LB.W0136" /><br><spring:message code= "LB.W0137" /> / SWIFT CODE
                </th>
				<!-- 匯入金額 -->
				<th>
					<!-- <spring:message code="LB.Withdrawal_amount" /> -->
					<spring:message code= "LB.W0138" />
                </th>
				<!--通知日 -->
				<!--解款日 -->
				<th>
					<!-- <spring:message code="LB.Deposit_amount" /> -->
					<spring:message code= "LB.W0139" /><br><spring:message code= "LB.W0140" />
                </th>
				<!-- 狀態 -->
				<th>
					<!-- <spring:message code="LB.Account_balance_2" /> -->
					<spring:message code= "LB.Status" />
                </th>
				<!-- 匯款銀行  參考號碼 -->
				<th>
					<!--<spring:message code="LB.Checking_account" /> -->
					<spring:message code= "LB.X0057" /><br><spring:message code= "LB.X1474" />
                </th>
				<!-- 匯款客戶名稱 -->
				<th>
					<!--<spring:message code="LB.Data_content" />-->
					<spring:message code= "LB.X0024" />
                </th>
				<!-- 生效日 -->
				<!-- 退匯日期 -->
				<!-- 匯率 -->
				<th>
					<!--<spring:message code="LB.Receiving_Bank" />-->
					<spring:message code= "LB.Effective_date" /><br><spring:message code= "LB.X0025" /><br><spring:message code= "LB.Exchange_rate" />
                </th>
				<!-- 附言 -->
				<th>
					<!--<spring:message code="LB.Trading_time" />-->
					<spring:message code= "LB.W0156" />
                </th>
			</tr>
			<c:forEach var="dataList" items="${print_datalistmap_data}">
				<tr>
				<!--交易編號-->
                <td style="text-align:center">${dataList.RREFNO }</td>
                <!--入帳帳號-->
                <td style="text-align:center">${dataList.RBANC }</td>
	            <!-- 匯款人 -->
	            <!-- 匯款銀行名稱 / SWIFT CODE -->
                <td style="text-align:center">${dataList.RORDNAME }<br> ${dataList.RORDBAD1 }/${dataList.RORDBANK } </td>
            	<!-- 匯入金額 -->
                <td style="text-align:center">${dataList.RREMITCY }<br>${dataList.RREMITAM }</td>
	            <!--通知日 -->
	            <!--解款日 -->
                <td style="text-align:center">${dataList.RADATE }<br>${dataList.RRTNDATE }</td>
            	<!-- 狀態 -->
                <td style="text-align:center">
                	<!--<c:choose>
                 		<c:when test="${dataList.RSTATE == 'A' }">
                 			通知
                 		</c:when>
                 		<c:when test="${dataList.RSTATE == 'S' }">
                 			銷帳
                 		</c:when>
                 		<c:when test="${dataList.RSTATE == 'C' }">
                 			退匯
                 		</c:when>
                 	</c:choose>-->
					<c:if test="${!dataList.RSTATE.equals('')}">
						<spring:message code="${dataList.RSTATE}"/>
					</c:if>
<%--                  	${dataList.RSTATE} --%>
                </td>
             	<!-- 匯款銀行 參考號碼 -->
                <td style="text-align:center">${dataList.RREMBREF }</td>
            	<!-- 匯款客戶名稱 -->
                <td style="text-align:center">${dataList.PORDCUS1 }</td>
	            <!-- 生效日 -->
	            <!-- 退匯日期 -->
	            <!-- 匯率 -->
                <td style="text-align:center">${dataList.RVALDATE }<br>${dataList.RCNADATE }<br>${dataList.FEXRATE }</td>
            	<!-- 附言 -->
                <td style="text-align:center">${dataList.RMEM01 }<br>${dataList.RMEM02 }<br>${dataList.RMEM03 }<br>${dataList.RMEM04 }</td>
				</tr>
			</c:forEach>
		</table>
   		<c:set var="total_i18n">
   			<spring:message code='LB.W0123' />
   		</c:set>
   		<c:set var="row_i18n">
   			<spring:message code='LB.Rows' />
   		</c:set>
		<p>
			<spring:message code= "LB.W0142" /> : <br>
			
       		<c:set var="FXINAMTSUMSTRINGFORPRINT_replace" value="${ fn:replace( FXINAMTSUMSTRINGFORPRINTMS, 'i18n{LB.W0123}', total_i18n) }" />
       		<c:set var="FXINAMTSUMSTRINGFORPRINT_replace2" value="${ fn:replace( FXINAMTSUMSTRINGFORPRINT_replace, 'i18n{LB.Rows}', row_i18n) }" />
  			${FXINAMTSUMSTRINGFORPRINT_replace2}
		</p>
	</div>
	
	<br>
	<br>
	<div class="text-left">
		<!-- 		說明： -->
		<spring:message code="LB.Description_of_page"/>
		<ol class="list-decimal text-left">
			<li>
				<!-- <spring:message code="LB.Demand_deposit_detail_P2_D1"/> -->
                            <spring:message code="LB.F_Inward_Remittance_P2_D1"/>
			</li>
		</ol>
	</div>
</body>
</html>