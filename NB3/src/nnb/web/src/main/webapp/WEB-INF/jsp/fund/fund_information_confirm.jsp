<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	$("#okButton").click(function(){
		var allCheck = true;
		
		$(".riskConfirmCheckBox").each(function(){
			if($(this).prop("checked") == false){
				allCheck = false;
			}
		});
		if(allCheck == true){
			$("#formID").attr("action","${__ctx}/FUND/TRANSFER/fund_risk_confirmY");
			$("#formID").submit();
		}
		else{
			//alert('<spring:message code="LB.Alert123"/>');
			errorBlock(
					null, 
					null,
					['請詳閱內容，並勾選「已充分評估並詳閱」之方塊。'], 
					'<spring:message code="LB.Quit" />', 
					null
			);
		}
	});
	
	$("#cancelButton").click(function(){
		var TXID = "${result_data.data.TXID}";
		
		if(TXID == "C021" || TXID == "C032"){
			$("#formID").attr("action","${__ctx}/FUND/TRANSFER/query_fund_transfer_data");
		}
		else if(TXID == "C016"){
			$("#formID").attr("action","${__ctx}/FUND/PURCHASE/fund_purchase_select");
		}
		else if(TXID == "C017"){
			$("#formID").attr("action","${__ctx}/FUND/REGULAR/fund_regular_select");
		}
		else if(TXID == "C031"){
			$("#formID").attr("action","${__ctx}/FUND/RESERVE/PURCHASE/fund_reserve_purchase_select");
		}
		$("#formID").submit();
	});
});

function clickCheck(){
	if($("#allCheckBox").prop("checked") == true){
		$('#okButton').removeAttr("disabled");
		$('#okButton').removeClass('btn-flat-gray');
		$('#okButton').addClass('btn-flat-orange');
	}
	else{
		$('#okButton').attr("disabled",true);
		$('#okButton').removeClass('btn-flat-orange');
		$('#okButton').addClass('btn-flat-gray');
	}
}

</script>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	<!--麵包屑-->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<!-- 基金    -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0901" /></li>
			<!-- 基金交易 -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1064" /></li>
			<!-- 近五年度之費用率及報酬率資訊 -->
			<li class="ttb-breadcrumb-item active" aria-current="page">投信基金、境外基金各級別近五年度之費用率及報酬率資訊</li>
		</ol>
	</nav>
	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
				<form id="formID" method="post">
					<input type="hidden" name="TXID" value="${result_data.data.TXID}"/>
					<input type="hidden" name="TRANSCODE" value="${result_data.data.TRANSCODE}"/>
					<input type="hidden" name="CDNO" value="${result_data.data.CDNO}"/>
					<input type="hidden" name="UNIT" value="${result_data.data.UNIT}"/>
					<input type="hidden" name="BILLSENDMODE" value="${result_data.data.BILLSENDMODE}"/>
					<input type="hidden" name="SHORTTRADE" value="${result_data.data.SHORTTRADE}"/>
					<input type="hidden" name="SHORTTUNIT" value="${result_data.data.SHORTTUNIT}"/>
					<input type="hidden" name="INTRANSCODE" value="${result_data.data.INTRANSCODE}"/>
					<input type="hidden" name="INFUNDSNAME" value="${result_data.data.INFUNDSNAME}"/>
					<input type="hidden" name="FUNDAMT" value="${result_data.data.FUNDAMT}"/>
					<input type="hidden" name="RRSK" value="${result_data.data.RRSK}"/>
					<input type="hidden" name="RSKATT" value="${result_data.data.RSKATT}"/>
					<input type="hidden" name="GETLTD" value="${result_data.data.GETLTD}"/>
					<input type="hidden" name="RISK7" value="${result_data.data.RISK7}"/>
					<input type="hidden" name="GETLTD7" value="${result_data.data.GETLTD7}"/>
					<input type="hidden" name="FDINVTYPE" value="${result_data.data.FDINVTYPE}"/>
					<input type="hidden" name="FDAGREEFLAG" value="${result_data.data.FDAGREEFLAG}"/>
					<input type="hidden" name="FDNOTICETYPE" value="5"/>
					<input type="hidden" name="ACN1" value="${result_data.data.ACN1}"/>
					<input type="hidden" name="ACN2" value="${result_data.data.ACN2}"/>
					<input type="hidden" name="HTELPHONE" value="${result_data.data.HTELPHONE}"/>
					<input type="hidden" name="COUNTRYTYPE" value="${result_data.data.COUNTRYTYPE}"/>
					<input type="hidden" name="COUNTRYTYPE1" value="${result_data.data.COUNTRYTYPE1}"/>
					<input type="hidden" name="CUTTYPE" value="${result_data.data.CUTTYPE}"/>
					<input type="hidden" name="BRHCOD" value="${result_data.data.BRHCOD}"/>
					<input type="hidden" name="RISK" value="${result_data.data.RISK}"/>
					<input type="hidden" name="SALESNO" value="${result_data.data.SALESNO}"/>
					<input type="hidden" name="PRO" value="${result_data.data.PRO}"/>
					<input type="hidden" name="TYPE" value="${result_data.data.TYPE}"/>
					<input type="hidden" name="COMPANYCODE" value="${result_data.data.COMPANYCODE}"/>
					<input type="hidden" name="AMT3" value="${result_data.data.AMT3}"/>
					<input type="hidden" name="YIELD" value="${result_data.data.YIELD}"/>
					<input type="hidden" name="STOP" value="${result_data.data.STOP}"/>
					<input type="hidden" name="FUNDLNAME" value="${result_data.data.FUNDLNAME}"/>
					<input type="hidden" name="PAYTYPE" value="${result_data.data.PAYTYPE}"/>
					<input type="hidden" name="FUNDACN" value="${result_data.data.FUNDACN}"/>
					<input type="hidden" name="MIP" value="${result_data.data.MIP}"/>
					<input type="hidden" name="PAYDAY1" value="${result_data.data.PAYDAY1}"/>
					<input type="hidden" name="PAYDAY2" value="${result_data.data.PAYDAY2}"/>
					<input type="hidden" name="PAYDAY3" value="${result_data.data.PAYDAY3}"/>
					<input type="hidden" name="PAYDAY4" value="${result_data.data.PAYDAY4}"/>
					<input type="hidden" name="PAYDAY5" value="${result_data.data.PAYDAY5}"/>
					<input type="hidden" name="PAYDAY6" value="${result_data.data.PAYDAY6}"/>
					<input type="hidden" name="PAYDAY7" value="${result_data.data.PAYDAY7}"/>
					<input type="hidden" name="PAYDAY8" value="${result_data.data.PAYDAY8}"/>
					<input type="hidden" name="PAYDAY9" value="${result_data.data.PAYDAY9}"/>
					<input type="hidden" name="INVTYPE" value="${result_data.data.INVTYPE}"/>
					<input type="hidden" name="FEE_TYPE" value="${result_data.data.FEE_TYPE}"/>
					<div class="main-content-block row">
						<div class="col-12">
							<!--內容-->
							<div class="ttb-message">
								<p>投信基金、境外基金各級別近五年度之費用率及報酬率資訊</p>
                          	</div>
                            <ul class="ttb-result-list" style="list-style:none;">
								<li class="full-list">
									<strong style="font-size:15px">
										台端所申購之基金有不同級別，台端應於申購前充分瞭解該檔基金各級別之不同（如不同計價幣別、配息或不配息、手續費為前收或後收等），以投資合適之級別。
										不同級別之費用率與報酬率或有差異，請詳閱「<a href="https://tbb.moneydj.com/w/CustFundIDMap.djhtm?A=${result_data.data.TRANSCODE}&B=21" target="_blank" style="color:#007bff;">基金理財網站</a>」之各級別近五年度之費用率與報酬率資訊。
									</strong>
                               	</li>
                            </ul>
	                        <label class="check-block">
								<input type="checkbox" class="riskConfirmCheckBox" id="allCheckBox" onclick="clickCheck()"/>
								本人已充分評估並詳閱本次申購貴行上架之基金各級別近五年度之費用率及報酬率資訊，且確認本次申購之基金級別符合本人投資需求，並同意貴行留存此評估結果。
								<span class="ttb-check"></span>
							</label>
                			<input type="button" id="cancelButton" value="<spring:message code="LB.Cancel"/>" class="ttb-button btn-flat-gray"/>
                			<input type="button" id="okButton" value="<spring:message code="LB.Confirm_1"/>" class="ttb-button btn-flat-gray" disabled/>	
                		</div>
					</div>
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>