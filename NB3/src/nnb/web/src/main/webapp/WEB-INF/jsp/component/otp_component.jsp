<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script type="text/javascript" src="${__ctx}/keyboard/js/plugins.js"></script>

<script type="text/javascript">
	var verifyCallBack = "";
	var refreshCall = "";
	var sec = 120;
	var the_Timeout;
	var messageMaskId = "";

	// 顯示OTP
	function showOTPDialog() {
		$("#windowMask").show(); // 背景遮罩
		$("#inputOTPDiv").show(); // OTP 
		$('#otp_error').hide();
		$("#OTP").focus();
	}

	// OTP取消
	function closeOTPDialog() {
		$('#inputOTPDiv').hide();
		$('#windowMask').hide();
		$('#OTP').val('');
		disableCountDown();
	}

	// OTP取消
	function verifyOTP() {
		var inputVal = $("#OTP").val();
		// 輸入密碼後callbackFunction
		eval(verifyCallBack + "('" + inputVal + "')");
	}

	function callInterBankVerifyAcc(uri,rdate, callBack, refresh_call, maskId) {
		messageMaskId = maskId;
		showTempMessage(500,"<spring:message code= "LB.X1250" />","",messageMaskId,true);	

		verifyCallBack = callBack;
		refreshCall = refresh_call;
		fstop.getServerDataEx(uri, rdate, true, CheckFiscResult);		
	}

	function CheckFiscResult(data){
		console.log(data);
		showTempMessage(500,"<spring:message code= "LB.X1250" />","",messageMaskId,false);
		if (data.result) {
			var sdata = data.data[0];
			if (sdata.MSGCOD == '0000') {
				sec = 120;
				countDown();
				showOTPDialog();
			} else {
				eval(refreshCall+ "()" );
				var sb = [sdata.MSGSTR + "("+sdata.MSGCOD+")"];
				errorBlock(null, null, sb, '確定', null);				
			}
		} else {
			eval(refreshCall+ "()" );
			var datamsg = data.data ? data.data : "";
			var b = [data.message + "("+data.msgCode+")", datamsg];
			errorBlock(null, null, b, '確定', null);
		}		
	}

	function countDown(){
		var main = document.getElementById("inputOTPDiv");      
      	var counter = document.getElementById("CountDown");                      
       	counter.innerHTML = sec;
       	sec--;
       	
       	if (sec == -1) {              
			sec =120;
           	return;                
       	}                                     
       
       	//網頁倒數計時(120秒)              
     	the_Timeout = setTimeout("countDown()", 1000);    
	}

	function disableCountDown() {
		clearTimeout(the_Timeout);
		sec =120;
		document.getElementById("CountDown").innerHTML = sec;
	}
</script>


<!--輸入簡訊驗證碼-->
<section id="inputOTPDiv" class="error-block" style="display: none">
	<div class="error-for-code">
		<p class="error-title"><spring:message code="LB.X1574" /></p>

		<input type="text" maxLength="8" size="10" class="ttb-input text-input input-width-125 validate[required,funcCall[validate_CheckLenEqual[<spring:message code="LB.Captcha"/>,OTP,false,8,8]],funcCall[validate_CheckNumber['LB.Captcha',OTP]]]" id="OTP" name="OTP" value=""/> 
		<button id="otpCancel" class="btn-flat-gray ttb-pup-btn" onclick="resendOTP();"><spring:message code="LB.Re_send" /></button>
		<!-- 驗證碼錯誤 -->
        <p class="error-content" id="otp_error" style="display: none"><font color="red"><spring:message code="LB.X1082" /></font></p>
        
		<p class="error-content"><spring:message code="LB.X1575" />：<font id="CountDown" color="red"></font><spring:message code="LB.Second" /></p>		
		
		<button id="otpCancel" class="btn-flat-gray ttb-pup-btn" onclick="closeOTPDialog();"><spring:message code="LB.Cancel" /></button>
		<button id="otpCommit" class="btn-flat-orange ttb-pup-btn" onclick="verifyOTP();"><spring:message code="LB.X0290" /></button>
	</div>
</section>

		
