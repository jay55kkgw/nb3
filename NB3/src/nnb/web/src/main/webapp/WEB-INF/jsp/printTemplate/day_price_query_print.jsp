<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function () {
			window.print();
		});
	</script>
</head>

<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br /><br />
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif" /></div>
	<br /><br />
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<br />
	<div>
		<!-- 系統時間 -->
		<p>
			<spring:message code="LB.System_time" /> : ${SYSTIME}

		</p>
		<!-- 掛牌時間 -->
		<p>
			<spring:message code="LB.W1465"/> : ${QDATE} ${QTIME}
		</p>
		<div class="ttb-message">
		<!-- 請注意：本表資料僅供參考，實際交易價格以交易確認時顯示之價格為準。 -->
			<span><spring:message code="LB.Day_Price_Query_P2_D1"/></span>
		</div>
					<div ALIGN="right">
							<spring:message code="LB.W1466"/>：<spring:message code="LB.NTD"/><spring:message code="LB.Dollar"/>
					</div>	
	
					<table class="print">
							<tr>
								<td class="text-center" colspan="2"><spring:message code="LB.W1467"/></td>
								<!-- 1,100,250,500公克 1公斤 -->
								<td>1<spring:message code="LB.W1435"/></td>
								<td>100<spring:message code="LB.W1435"/></td>
								<td>250<spring:message code="LB.W1435"/></td>
								<td>500<spring:message code="LB.W1435"/></td>
								<td>1<spring:message code="LB.W1469"/></td>
					
							</tr>
								<tr>
									<td rowspan="2" style="vertical-align:middle;text-align:center;"><spring:message code="LB.W1428"/></td>
					                <!-- 賣出價格 -->
					                <td><spring:message code="LB.W1470"/></td>
					                <td>${print_datalistmap_data[0].BPRICE}</td>
					                <td>-</td>
					                <td>-</td>
					                <td>-</td>
					                <td>-</td>
					                
					             
								</tr>
								
								<tr>
									<!-- 買進價格 -->
					                <td><spring:message code="LB.W1471"/></td>
					                <td>${print_datalistmap_data[0].SPRICE}</td>
					                <td>-</td>
					                <td>-</td>
					                <td>-</td>
					                <td>-</td>
					                
					               
								</tr>
								
								<tr>
								<!-- 轉換黃金條塊應補繳款 -->
									<td class="text-center" colspan="2"><spring:message code="LB.W1472"/></td>
					                <td>-</td>
					                <td>${print_datalistmap_data[0].PRICE1}</td>
					                <td>${print_datalistmap_data[0].PRICE2}</td>
					                <td>${print_datalistmap_data[0].PRICE3}</td>
					                <td>${print_datalistmap_data[0].PRICE4}</td>
					     			
								</tr>
						
				</table>
			<br /><br />
	</div>
</body>

</html>