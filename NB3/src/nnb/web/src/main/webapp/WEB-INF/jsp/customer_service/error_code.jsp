<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript">

	
	
</script>
<div class="card-detail-block" id="err1">
	<div class="card-center-block">
<!-- 		<h2>中心主機</h2> -->
		<table class="stripe table table-striped ttb-table dtable1" data-toggle-column="first">
			<thead>
				<tr>
					<th><spring:message code="LB.error_code" /></th>
					<th><spring:message code="LB.ErrMsg" /></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="dataList" items="${error_code}" >
					<tr>
						<td>${dataList.ADMCODE}</td>
						<td>${dataList.ADMSGIN}</td>
					</tr>

				</c:forEach>
			</tbody>
		</table>
	</div>
</div>
<div class="card-detail-block" id="err2">
	<div class="card-center-block">
<!-- 		<h2>瀏覽器元件</h2> -->
		<table class="stripe table table-striped ttb-table dtable2" data-toggle-column="first">
			<thead>
				<tr>
					<th><spring:message code="LB.error_code" /></th>
					<th><spring:message code="LB.ErrMsg" /></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						E001
					</td>
					<td>
						讀卡機偵測失敗。
					</td>
				</tr>
				
				<tr>
					<td>
						E002
					</td>
					<td>
						無法連接讀卡機驅動程式。
					</td>
				</tr>
				
				<tr>
					<td>
						E003
					</td>
					<td>
						卡片偵測失敗。
					</td>
				</tr>
				
				<tr>
					<td>
						E004
					</td>
					<td>
						系統異常，請將IC卡片拔出後重新插入，再試。
					</td>
				</tr>
				
				<tr>
					<td>
						E005
					</td>
					<td>
						非本行晶片金融卡/密碼輸入錯誤，或驗證密碼失敗。
					</td>
				</tr>
				
				<tr>
					<td>
						E005,69F0, 0
					</td>
					<td>
						確認密碼操作錯誤
					</td>
				</tr>
					
				<tr>
					<td>
						E006
					</td>
					<td>
						無法取得發卡行資訊。
					</td>
				</tr>
				
				<tr>
					<td>
						E007
					</td>
					<td>
						卡片密碼不可為空白。
					</td>
				</tr>
				
				<tr>
					<td>
						E007|69F0
					</td>
					<td>
						密碼輸入操作錯誤
					</td>
				</tr>
				
				<tr>
					<td>
						E007|69F0,0
					</td>
					<td>
						確認密碼操作錯誤
					</td>
				</tr>
				
				<tr>
					<td>
						E007|6610
					</td>
					<td>
						密碼輸入錯誤
					</td>
				</tr>
				
				<tr>
					<td>
						E008
					</td>
					<td>
						卡片密碼錯誤。
					</td>
				</tr>
				
				<tr>
					<td>
						E009
					</td>
					<td>
						卡片已遭鎖定。
					</td>
				</tr>
				
				<tr>
					<td>
						E010
					</td>
					<td>
						取得發卡行代碼失敗。
					</td>
				</tr>
				
				<tr>
					<td>
						E011
					</td>
					<td>
						取得帳號資料失敗。
					</td>
				</tr>
					
				<tr>
					<td>
						E012
					</td>
					<td>
						請確認是否正確插入晶片金融卡。
					</td>
				</tr>
				<tr>
					<td>
						E013
					</td>
					<td>
						取得交易序號與TAC碼失敗。
					</td>
				</tr>
				
				<tr>
					<td>
						E014
					</td>
					<td>
						您於抽插拔晶片金融卡時更換不同IC卡片，該交易取消，若要重試，請重新按確定鍵。
					</td>
				</tr>
				
				<tr>
					<td>
						E015
					</td>
					<td>
						抽插拔晶片金融卡逾時，該交易取消，若要重試，請重新按確定鍵。
					</td>
				</tr>
				
				<tr>
					<td>
						E016
					</td>
					<td>
						您於抽插拔晶片金融卡時點了[取消]鍵，該交易取消，若要重試，請重新按確定鍵。
					</td>
				</tr>
				
				<tr>
					<td>
						E017
					</td>
					<td>
						系統異常，請關閉瀏覽器後重做。
					</td>
				</tr>
					
				<tr>
					<td>
						E018
					</td>
					<td>
						系統異常，請關閉瀏覽器後重做。
					</td>
				</tr>
				<tr>
					<td>
						E019
					</td>
					<td>
						系統異常，請關閉瀏覽器後重做。
					</td>
				</tr>
					
				<tr>
					<td>
						E020
					</td>
					<td>
						系統異常，請關閉瀏覽器後重做。
					</td>
				</tr>
				<tr>
					<td>
						E021
					</td>
					<td>
						轉入帳號驗證錯誤，請重新輸入後再試。
					</td>
				</tr>
					
				<tr>
					<td>
						E022
					</td>
					<td>
						系統異常，請關閉瀏覽器後重做。
					</td>
				</tr>
				<tr>
					<td>
						E023
					</td>
					<td>
						系統異常，請關閉瀏覽器後重做。
					</td>
				</tr>
					
				<tr>
					<td>
						E024
					</td>
					<td>
						密碼變更失敗。
					</td>
				</tr>
				<tr>
					<td>
						E025
					</td>
					<td>
						原密碼輸入錯誤。
					</td>
				</tr>
				<tr>
					<td>
						E999
					</td>
					<td>
						讀卡機控制物件載入失敗。
					</td>
				</tr>
				<tr>
					<td>
						E_Send_11_OnError_1006
					</td>
					<td>
						尚未安裝元件。
					</td>
				</tr>
				<tr>
					<td>
						0x8010001B
					</td>
					<td>
						The reader driver did not produce a unique reader name.讀卡機驅動程式未產生唯一的讀卡機名稱。
					</td>
				</tr>
				
				<tr>
					<td>
						0x80100003
					</td>
					<td>
						The supplied handle was not valid.讀卡機裝置控制失效。
					</td>
				</tr>
				
				<tr>
					<td>
						0x80100027
					</td>
					<td>
						Access is denied to the file.讀取IC卡片檔錯誤。
					</td>
				</tr>
				
				<tr>
					<td>
						0x80100006
					</td>
					<td>
						Not enough memory available to complete this command.記憶空間不足。
					</td>
				</tr>
				
				<tr>
					<td>
						0x8010002E
					</td>
					<td>
						No smart card reader is available.未偵測到讀卡機。
					</td>
				</tr>
					
				<tr>
					<td>
						0x8010000C
					</td>
					<td>
						The operation requires a smart card, but no smart card is currently in the device.讀卡機裝置已離線。
					</td>
				</tr>
				
				<tr>
					<td>
						0x8010000B
					</td>
					<td>
						The smart card cannot be accessed because of other outstanding connections.讀卡機正被其他程式獨佔中。
					</td>
				</tr>
					
				<tr>
					<td>
						0x80100066
					</td>
					<td>
						The smart card is not responding to a reset.IC卡片重置無回應。
					</td>
				</tr>
				
				<tr>
					<td>
						0x80100068
					</td>
					<td>
						The smart card was reset.IC卡片重置。
					</td>
				</tr>
				
				<tr>
					<td>
						0x80100069
					</td>
					<td>
						The smart card has been removed, so further communication is not possible.IC卡片移出讀卡機，無法連接。
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<div class="card-detail-block" id="err3">
	<div class="card-center-block">
<!-- 		<h2>電子簽章</h2> -->
		<table class="stripe table table-striped ttb-table dtable3" data-toggle-column="first">
			<thead>
				<tr>
					<th><spring:message code="LB.error_code" /></th>
					<th><spring:message code="LB.ErrMsg" /></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>3202</td>
					<td>找不到可用憑證</td>
				</tr>
				<tr>
					<td>3206</td>
					<td>取得憑證資訊失敗</td>
				</tr>
				<tr>
					<td>3209</td>
					<td>憑證已過期</td>
				</tr>
				<tr>
					<td>3210</td>
					<td>憑證尚未到達憑證啟用時間</td>
				</tr>
				<tr>
					<td>3211</td>
					<td>無可用憑證！憑證不是未達啟用時間就是已過期</td>
				</tr>
				<tr>
					<td>5001</td>
					<td>一般錯誤</td>
				</tr>
				<tr>
					<td>5002</td>
					<td>配置記憶體發生錯誤</td>
				</tr>
				<tr>
					<td>5003</td>
					<td>記憶體緩衝區太小</td>
				</tr>
				<tr>
					<td>5004</td>
					<td>未支援函式</td>
				</tr>
				<tr>
					<td>5005</td>
					<td>錯誤的參數</td>
				</tr>
				<tr>
					<td>5006</td>
					<td>無效的handle</td>
				</tr>
				<tr>
					<td>5007</td>
					<td>試用版期限已過</td>
				</tr>
				<tr>
					<td>5008</td>
					<td>Base64編碼錯誤</td>
				</tr>
				<tr>
					<td>5010</td>
					<td>無法在MS CryptoAPI Database中找到指定憑證</td>
				</tr>
				<tr>
					<td>5011</td>
					<td>憑證已過期</td>
				</tr>
				<tr>
					<td>5012</td>
					<td>憑證尚未合法，無法使用</td>
				</tr>
				<tr>
					<td>5013</td>
					<td>憑證可能過期或無法使用</td>
				</tr>
				<tr>
					<td>5014</td>
					<td>憑證主旨錯誤</td>
				</tr>
				<tr>
					<td>5015</td>
					<td>無法找到憑證發行者</td>
				</tr>
				<tr>
					<td>5016</td>
					<td>不合法的憑證簽章</td>
				</tr>
				<tr>
					<td>5017</td>
					<td>憑證用途（加解密、簽驗章）不合適</td>
				</tr>
				<tr>
					<td>5020</td>
					<td>憑證已撤銷</td>
				</tr>
				<tr>
					<td>5021</td>
					<td>憑證已撤銷（金鑰洩露）</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>


