<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div>
	<div class="ttb-message">
		<p>Customers should pay attention to online banking foreign
			exchange structure and transfer, remittance remittance and remittance
			remittance</p>
	</div>
	<ul class="ttb-result-list terms">
		<li data-num="">
			<ul class="">
				<li><strong style="font-size: 15px"> 1.Online banking <font
						color="red"> Does not provide foreign exchange services
							that require approval of approval letters or transaction
							documents </font> If the reporting obligor has an approval letter or
						wants to execute the foreign exchange service subject to the
						transaction documents, please go to the counter.
				</strong></li>
				<li><strong style="font-size: 15px"> 2.Online banking <font
						color="red"> Foreign exchange business involving NT </font> Only
						available <font color="red"> Transfers, remittances and
							remittances that are not equivalent to NT$500,000 </font> . If you
						exceed the transaction limit, please go to the counter.
				</strong></li>
				<li><strong style="font-size: 15px"> 3.<font color="red">Individuals
							involving RMB purchases (including currency transfers) and
							settlements (including currency transfers) should also be
							accumulatively not more than RMB 20,000. </font>
				</strong></li>
				<li><strong style="font-size: 15px"> 4.<font color="red">If
							the single transaction or the accumulated equivalent of
							NT$500,000, the transaction will be cancelled. </font>
				</strong></li>
				<li><strong style="font-size: 15px"> 5.Online banking <font
						color="red">Forex business not involving NTD</font> , each of its
						daily or daily transaction limits <font color="red">According
							to the Bank's online banking system operating procedures
							regarding transaction limits</font> . (See the important announcement
						content of our website)
				</strong></li>
				<li><strong style="font-size: 15px"> 6.If the foreign
						exchange receipts and payments or transactions of the reporting
						obligor are not handled by NTD, the other transaction vouchers
						issued by the Bank shall be deemed to be the same. </strong></li>
				<li><strong style="font-size: 15px"> 7.The reporting
						obligor uses the Internet to handle the NTT settlement report. If
						the applicant has found a false report, he/she will handle the NTD
						settlement in the future. </strong></li>
				<li><strong style="font-size: 15px"> 8.In the case of
						remittance remittance transactions provided by this online bank,
						if there is any mispayment or any dispute arising from your bank,
						I (the company) is willing to be responsible for the return of the
						remittance and possible interest. </strong></li>
			</ul>
		</li>
	</ul>
</div>