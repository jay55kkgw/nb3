<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<!--舊版驗證-->
<script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",100);
	//開始查詢資料並完成畫面
	setTimeout("init()",200);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
});
function init(){
	$("#formID").validationEngine({
		binded: false,
		promptPosition: "inline"
	});
	
	$("#CMSUBMIT").click(function(e){
		e = e || window.event;
		if(!$('#formID').validationEngine('validate')){
    		e.preventDefault();
    	}
		else{
			$("#formID").attr("action","${__ctx}/BOND/SELL/bond_sell_result");
			$('#PINNEW').val(pin_encrypt($('#CMPW').val()));
			initBlockUI();
			initBlockUI();//遮罩
			$("#formID").submit();	
		}
	
	});
	
	$("#CMCANCEL").click(function(){
		$("#formID").validationEngine('detach');
		initBlockUI();
		$("#formID").attr("action","${__ctx}/BOND/SELL/bond_sell_query");
		$("#formID").submit();
	});
	
}

</script>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 海外債券     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2391" /></li>
    <!-- 海外債券交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2515" /></li>
    <!-- 海外債券贖回/部分贖回     -->
    		<c:if test="${bond_sell_confirm.data.SELLWAY == '1'}">
<!--     			海外債券贖回確認 -->
    			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X2542" /></li>
    		</c:if>
    		<c:if test="${bond_sell_confirm.data.SELLWAY == '2'}">
<!--     		海外債券部分贖回確認 -->
    			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X2543" /></li>
    		</c:if>
		</ol>
	</nav>

	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
				<c:if test="${bond_sell_confirm.data.SELLWAY == '1'}">
<!-- 				海外債券贖回交易確認 -->
    				<h2><spring:message code="LB.X2564" /></h2>
    			</c:if>
    			<c:if test="${bond_sell_confirm.data.SELLWAY == '2'}">
<!--     			海外債券部分贖回交易確認 -->
    				<h2><spring:message code="LB.X2565" /></h2>
    			</c:if>
				<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
					<form id="formID" method="post">
					<input type="hidden" id="TOKEN" name = "TOKEN" value="${sessionScope.transfer_confirm_token}">
					<input type="hidden" id="FGTXWAY" name="FGTXWAY" value="0">	
					<input type="hidden" id="PINNEW" name="PINNEW" value="">				
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<!-- 身分證字號/統一編號 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.Id_no" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${bond_sell_confirm.data.CUSIDN}</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 客戶姓名 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1066" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${bond_sell_confirm.data.hiddenNAME}</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 信託帳號 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W0944" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${bond_sell_confirm.data.O01} </span>
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 債券名稱 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1012" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${bond_sell_confirm.data.BONDNAME}</span>
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 投資幣別 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W0908" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${bond_sell_confirm.data.BONDCRY_SHOW}</span>
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 委託賣價 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.X2531" />(A)：</h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                			<span>${bond_sell_confirm.data.O04_FMT}%</span>
                                		</div>
                               		</span>
                            	</div>
                            	
                            	<!-- 委賣面額 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.X2532" />(B)：</h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                			<span>
                                				${bond_sell_confirm.data.O03_FMT} <spring:message code="LB.Dollar" />
                                			</span> 
                                		</div>
                               		</span>
                            	</div>
                            	
                            	<!-- 前手利息 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.X2533" />(C)：</h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                			<span>
                                			<c:if test="${bond_sell_confirm.data.O08 == '-'}">
                                			- 
                                			</c:if>
                                			${bond_sell_confirm.data.O09_FMT} <spring:message code="LB.Dollar" />
                                			</span> 
                                		</div>
                               		</span>
                            	</div>
                            	
                            	<!-- 保管費 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.X2534" />(D)：</h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                			<span>${bond_sell_confirm.data.O07_FMT} <spring:message code="LB.Dollar" /></span> 
                                		</div>
                               		</span>
                            	</div>
                            	
                            	<!-- 贖回方式 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1140" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                            				<c:if test="${bond_sell_confirm.data.SELLWAY == '1'}">
                            					<spring:message code="LB.All" />
                            				</c:if>
                            				<c:if test="${bond_sell_confirm.data.SELLWAY == '2'}">
                            					<spring:message code="LB.X2525" />
                            				</c:if>
                                		</div>
                               		</span>
                            	</div>
                            	
                            	<!-- 信託本金 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.X2535" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                			<span>
                                				${bond_sell_confirm.data.O05_FMT} <spring:message code="LB.Dollar" />
                                			</span> 
                                		</div>
                               		</span>
                            	</div>
                            	
                            	<!-- 預估收款金額 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.X2536" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                			<span>${bond_sell_confirm.data.BONDCRY_SHOW} ${bond_sell_confirm.data.O10_FMT} <spring:message code="LB.Dollar" /><br>(A*B+C-D)</span> 
                                		</div>
                               		</span>
                            	</div>
                            	
                            	<!-- 入帳帳號 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W0135" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                			<span>${bond_sell_confirm.data.ACN4}</span> 
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 交易密碼 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message	code="LB.SSL_password" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                			<input type="password" class="text-input validate[required,funcCall[validate_CheckTxnNewPassword[CMPW]]]" name="CMPW" id="CMPW" autocomplete="off" size="8" maxlength="8" value="">
                                			<span class="input-remarks" style="color:red"><spring:message	code="LB.X2544" /></span> 
<!--                                 			請注意海外債券交易如經完成，即無法取消交易 -->
                                		</div>
                               		</span>
                            	</div>
                            </div>
							<input type="button" id="CMCANCEL" value="<spring:message code="LB.Cancel"/>" class="ttb-button btn-flat-gray"/>	
							<input type="button" id="CMSUBMIT" value="<spring:message code="LB.Confirm"/>" class="ttb-button btn-flat-orange"/>
						</div>
					</div>	
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>