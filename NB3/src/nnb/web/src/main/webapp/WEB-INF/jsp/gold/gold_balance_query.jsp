<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
</head>
<body>
    <!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 查詢服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.use_component_P1_D1-1" /></li>
    <!-- 黃金存摺餘額查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1430" /></li>
		</ol>
	</nav>

		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 --> 
 		<div class="content row">
 		   <%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->

 		<main class="col-12">	
		<!-- 		主頁內容  -->

			<section id="main-content" class="container">
<!-- 				黃金存摺餘額查詢 -->
				<h2><spring:message code="LB.W1430" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<ul class="ttb-result-list">
							<li>
								<!-- 查詢時間 -->
								<h3>
								<spring:message code="LB.Inquiry_time" /> ：
								</h3>
								<p>						
								${gold_balance_query.data.CMQTIME }
								</p>
							</li>
							<li>
								<!-- 資料總數 -->
								<h3>
								<spring:message code="LB.Total_records" /> ：								
								</h3>
								<p>
								${gold_balance_query.data.CMRECNUM }
								<spring:message code="LB.Rows" />
								</p>
							</li>							
						</ul>		
						
						<table id="table1" class="stripe table-striped ttb-table dtable" data-show-toggle="first">
						<thead>
							<tr>
								<!--帳號 -->
								<th class="text-center"><spring:message code="LB.Loan_account" /></th>
								<!--可用餘額(公克) -->							
								<th class="text-center"><spring:message code="LB.W1433" />(<spring:message code="LB.W1435" />)</th>
								<!--帳戶餘額(公克) -->
								<th class="text-center"><spring:message code="LB.W1434" />(<spring:message code="LB.W1435" />)</th>
								<!--參考市值-->
								<th class="text-center"><spring:message code="LB.W1436" /></th>
								<!--每公克原始投入參考平均成本-->
								<th class="text-center"><spring:message code="LB.W1437" /></th>
								<!--快速選項 -->
								<th class="text-center"><spring:message code="LB.Quick_option" /></th>
							</tr>
						</thead>
					<!--          列表區 -->
						<tbody>
						<c:forEach var="dataList" items="${gold_balance_query.data.REC}" varStatus="loop">		
							<tr>            	
								<td>${dataList.ACN}</td>
								<td class="text-right">${dataList.TSFBAL}</td>
								<td class="text-right">${dataList.GDBAL}</td>
								<td class="text-right">${dataList.MKBAL}</td>
								<td class="text-right">${dataList.AVGCOST}</td>
								<td>	
<%-- 								<input type="button" class="d-lg-none" id="click" onclick="hd2('actionBar2${data.index}')" value="..." /> --%>
									<select class="custom-select fast-select" name="next-action" id="actionBar" onchange="formReset(this,'${dataList.ACN}')">
		      							<!-- 請選擇 -->
		      							<option value="">-<spring:message code="LB.Select" />-</option>
		      							<!-- 明細查詢 -->
		      							<option value="N191"><spring:message code="LB.W1442" /></option>
		      							<!-- 單筆買進 -->
		       							<option value="N09001"><spring:message code= "LB.X0921" /></option>
		       							<!-- 單筆回售 -->
		       							<option value="N09002"><spring:message code= "LB.X0922" /></option>
		       							<!-- 定期定額申購 -->
		      							<option value="N09301"><spring:message code="LB.W1553" /></option>
		      							<!-- 定期定額變更 -->
		       							<option value="N09302"><spring:message code="LB.W1564" /></option>
									</select>
									<!-- 快速選單測試新增div -->
<%--  					        		<div id="actionBar2${data.index}" class="fast-div-grey" style="visibility: hidden"> --%>
<!-- 										<div class="fast-div"> -->
<%-- 											<img src="${__ctx}/img/icon-close-2.svg" class="top-close-btn" onclick="hd2('actionBar2${data.index}')"> --%>
<%--                               				<p><spring:message code= "LB.X1592" /></p> --%>
<!-- 											<ul> -->
<%-- 												<li onclick="fomrReset2('N191','${dataList.ACN}')"><spring:message code="LB.W1442" /><img src="${__ctx}/img/icon-10.svg" align="right"> </li> --%>
<%-- 											    <li onclick="fomrReset2('N09001','${dataList.ACN}')"><spring:message code="LB.X0921" /><img src="${__ctx}/img/icon-10.svg" align="right"></li> --%>
<%-- 											    <li onclick="fomrReset2('N09002','${dataList.ACN}')"><spring:message code="LB.X0922" /><img src="${__ctx}/img/icon-10.svg" align="right"></li> --%>
<%-- 											    <li onclick="fomrReset2('N09301','${dataList.ACN}')"><spring:message code="LB.W1553" /><img src="${__ctx}/img/icon-10.svg" align="right"> </li> --%>
<%-- 											    <li onclick="fomrReset2('N09302','${dataList.ACN}')"><spring:message code="LB.W1564" /><img src="${__ctx}/img/icon-10.svg" align="right"> </li>					 --%>
<!-- 											</ul> -->
<%-- 											<input type="button" class="bottom-close-btn" onclick="hd2('actionBar2${data.index}')" value="<spring:message code= "LB.X1572" />"/> --%>
<!-- 										</div> -->
<!-- 									</div> -->
								</td>            
							</tr>
						</c:forEach>
							<tr>
<!-- 							合計 -->
								<td class="text-center"><spring:message code= "LB.X0923" /></td>
								<td class="text-right">${gold_balance_query.data.TOTAL_TSFBAL}</td>
								<td class="text-right">${gold_balance_query.data.TOTAL_GDBAL}</td>
								<td class="text-right">${gold_balance_query.data.TOTAL_MKBAL}</td>
								<td></td>
								<td></td>
						</tr>

						</tbody>
						</table>
						<ol class="description-list text-left">
						   
							<!-- 備註:每公克原始投入參考平均成本計算如說明，且不含投資手續費成本，本項目僅供投資人參考。 -->
							<li><span><spring:message code="LB.W1440"/><a href="${__ctx}/GOLD/PASSBOOK/gold_balance_query_explan" style="color:#007bff;" target=_blank><spring:message code="LB.W1441"/></a>
                            </span></li>
                         </ol>
							
							<form id="formId" method="post">
								<input type="hidden" name="ACN" id="ACN"/> 
								<input type="hidden" name="CUSIDN" value="${gold_balance_query.data.CUSIDN}"/> 
								<input type="hidden" name="FASTMENU" id="FASTMENU" value=""/> 
							</form>
						<input type="button" class="ttb-button btn-flat-orange" id = "printbtn" value="<spring:message code="LB.Print" />"/>
					</div>
				</div>

			</section>
			<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->


	<%@ include file="../index/footer.jsp"%>
	
		<script type="text/javascript">
			$(document).ready(function(){
				// HTML載入完成後開始遮罩
				setTimeout("initBlockUI()",10);
				// 開始查詢資料並完成畫面
				setTimeout("init()",20);
				// 解遮罩
				setTimeout("unBlockUI(initBlockId)",500);
				//setTimeout("initDataTable()",100);
				setTimeout("initDataTable()",100);
			});
			function init(){
				 // $('.table').footable();
				//fixtable();
				
				$("#printbtn").click(function(){
					var params = {
							"jspTemplateName":"gold_balance_query_print",
							//黃金存摺餘額查詢
							"jspTitle":'<spring:message code="LB.W1430" />',
							"CMQTIME":"${gold_balance_query.data.CMQTIME }",
							"COUNT":"${gold_balance_query.data.CMRECNUM }",
							"TOTAL_TSFBAL":"${gold_balance_query.data.TOTAL_TSFBAL }",
							"TOTAL_GDBAL":"${gold_balance_query.data.TOTAL_GDBAL }",
							"TOTAL_MKBAL":"${gold_balance_query.data.TOTAL_MKBAL }"
						};
						openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
				});	
			}
			//下拉式選單
			
			function formReset(d,ACN) {
				var v = d.options[d.options.selectedIndex].value;
				var target="";
				if (v == "N09001") {
					$("#ACN").val(ACN);
					target="${__ctx}/GOLD/TRANSACTION/gold_buy";
				}
				else if (v == "N09002") {
					$("#ACN").val(ACN);
					target="${__ctx}/GOLD/TRANSACTION/gold_resale";
				}
				else if (v == "N09301") {
					$("#ACN").val(ACN);
					target="${__ctx}/GOLD/AVERAGING/averaging_purchase";
				}
				else if (v == "N09302") {
					$("#ACN").val(ACN);
					target="${__ctx}/GOLD/AVERAGING/averaging_alter";
				}
				else if (v == "N191") {
					$("#ACN").val(ACN);
					$("#FASTMENU").val(ACN);
					target="${__ctx}/GOLD/PASSBOOK/gold_detail_query";
				}
				$("#formId").attr("action", target);
				$("#formId").submit();
			}
			function fomrReset2(code,ACN){
				$('#actionBar').val(code);
				formReset(ACN);
			}
			function fixtable(){
				$(".table").find("td .same").hide();
				$(".table").find("th .same").hide();
			}
			function hd2(T){
				var t = document.getElementById(T);
				if(t.style.visibility === 'visible'){
					t.style.visibility = 'hidden';
				}
				else{
					$("div[id^='actionBar2']").each(function() {
						var d = document.getElementById($(this).attr('id'));
						d.style.visibility = 'hidden';
				    });
					t.style.visibility = 'visible';
				}
			}
		</script>
</body>
</html>