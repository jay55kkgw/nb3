<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>	
</head>
 <body>

	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 自動扣繳服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2360" /></li>
    <!-- 停車費     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.D0330" /></li>
    <!-- 停車費代扣繳申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0276" /></li>
		</ol>
	</nav>

 	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
	<main class="col-12"> 
	<!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<h2><spring:message code="LB.X0276"/></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>						
<!-- 			<p style="text-align: center;color: red;">請確認申請資料</p> -->
			<form method="post" id="formId">
				<input type="hidden" id="back" name="back" value="">
				<input type="hidden" id="TOKEN" name="TOKEN" value="${result_data.TOKEN}" /><!-- 防止重複交易 -->
                
                <!-- ikey -->
				<input type="hidden" id="jsondc" name="jsondc" value='${ result_data.jsondc }'>
				<input type="hidden" id="ISSUER" name="ISSUER" value="">
				<input type="hidden" id="ACNNO" name="ACNNO" value="">
				<input type="hidden" id="TRMID" name="TRMID" value="">
				<input type="hidden" id="iSeqNo" name="icSeq" value="">
				<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
				<input type="hidden" id="TAC" name="TAC" value="">
				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
				<input type="hidden" id="CMTRANPAGE" name="CMTRANPAGE" value="1">
				<input type="hidden" id="NeedSHA1" name="NeedSHA1" value="">
				<input type="hidden" id="PINNEW" name="PINNEW" value="">
				<input type="hidden" id="ACN" name="ACN" value="">
				<input type="hidden" id="OUTACN" name="OUTACN" value="">
				<input type="hidden" id="ADSVBH" name="ADSVBH" value="">
				<input type="hidden" id="ADAGREEF" name="ADAGREEF" value="">
    			<input type="hidden" id="ADOPID" name="ADOPID" value="${ result_data.ADOPID }">
				<input type="hidden" id="ICDTTM" name="ICDTTM" value="">
				<input type="hidden" id="ICMEMO" name="ICMEMO" value="">
				<input type="hidden" id="TAC_Length" name="TAC_Length" value="">
				<input type="hidden" id="TAC_120space" name="TAC_120space" value="">
			    <input type="hidden" name="CardType" value="B">
			    <input type="hidden" name="NeedConfirm" value="0">
			    <input type="hidden" name="MailFlag" value="1">
			    <input type="hidden" name="SMSFlag" value="0">
			    <input type="hidden" name="Source" value="NB   ">
			    <input type="hidden" name="CardValidDate" value=" ">
			    <input type="hidden" name="CardCVC2" value=" ">
				
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
						
							<!-- 確認新使用者名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.Data_time"/>
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
		 								
										<p>${ result_data.CMQTIME }</p>
									</div>
								</span>
							</div>
							
							<!-- 確認新使用者名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.W0702"/>
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p> ${ result_data.Account }</p>
									</div>
								</span>
								<input type="hidden" id="Account" name="Account" value="${ result_data.Account }">
							</div>
							
							<!-- 確認新使用者名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0059"/>
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p> ${ result_data.ZoneName }</p>
									</div>
								</span>
								<input type="hidden" id="ZoneCode" name="ZoneCode" value="${ result_data.ZoneCode }">
								<input type="hidden" id="ZoneName" name="ZoneName" value="${ result_data.ZoneName }">
							</div>
							
							<!-- 確認新使用者名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0342"/>
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p> ${ result_data.CarId1 }-${ result_data.CarId2 }</p>
									</div>
								</span>
								<input type="hidden" id="CarId" name="CarId" value="${ result_data.CarId1 }-${ result_data.CarId2 }">
								<input type="hidden" id="CarId1" name="CarId1" value="${ result_data.CarId1 }">
								<input type="hidden" id="CarId2" name="CarId2" value="${ result_data.CarId2 }">
							</div>
							
							<!-- 確認新使用者名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0343"/>
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<c:if test="${ result_data.CarKind.equals('C') }">
											<p><spring:message code="LB.D0344"/></p>
										</c:if>
										<c:if test="${ result_data.CarKind.equals('M') }">
											<p><spring:message code="LB.D0345"/></p>
										</c:if>
									</div>
								</span>
								<input type="hidden" id="CarKind" name="CarKind" value="${ result_data.CarKind }">
							</div>
							
							<!-- 確認新使用者名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0020"/>
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p><spring:message code="LB.X0278"/></p>
									</div>
								</span>
							</div>
							
							<!-- 確認新使用者名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.Mail_address"/>
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p> ${ result_data.Email }</p>
									</div>
								</span>
								<input type="hidden" id="Email" name="Email" value="${ result_data.Email }">
							</div>
							
							<!-- 確認新使用者名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0069"/>
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
									<!-- 行動電話 -->
										<input class="text-input  validate[required,funcCall[validate_CheckNumber[<spring:message code= "LB.D0069" />,Phone]]]" maxLength="10" size="10" type="text" name="Phone" id="Phone" placeholder="<spring:message code= "LB.D1200" />">
									</div>
								</span>
							</div>
							
							
							
							
							<!--交易機制  SSL 密碼 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.Transaction_security_mechanism" />
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block" for="CMSSL">
								       		<input type="radio" name="FGTXWAY" id="CMSSL" value="0" checked /><spring:message code="LB.SSL_password" />
                                            <span class="ttb-radio"></span>
								       	</label>
								    </div>
									<div class="ttb-input">
										<input type="password" name = "CMPASSWORD" id="CMPASSWORD"  maxlength="8" class="text-input validate[required, pwvd[PW, CMPASSWORD], custom[onlyLetterNumber]]">
										<input type="hidden" name = "PINNEW" id="PINNEW" value="">
									</div>
<!-- 									使用者是否可以使用IKEY -->
									<!-- 使用者是否可以使用IKEY -->
<%-- 									<c:if test = "${sessionScope.isikeyuser}"> --%>
<!-- 										即使使用者可以用IKEY，若轉出帳號為晶片金融卡帶出的非約定帳號，也不給用 (By 景森) -->
<%-- 										<c:if test = "${!result_data.TransferType.equals('NPD')}"> --%>
								
<!-- 											IKEY -->
<!-- 											<div class="ttb-input"> -->
<!-- 												<label class="radio-block"> -->
<%-- 													<spring:message code="LB.Electronic_signature" /> --%>
<!-- 													<input type="radio" name="FGTXWAY" id="CMIKEY" value="1"> -->
<!-- 													<span class="ttb-radio"></span> -->
<!-- 												</label> -->
<!-- 											</div> -->
											
<%-- 										</c:if> --%>
<%-- 									</c:if> --%>
									<!-- 晶片金融卡 -->
<!-- 									<div class="ttb-input"> -->
<!-- 										<label class="radio-block"> -->
<%-- 											<spring:message code="LB.Financial_debit_card" /> --%>
											
<!-- 											只能選晶片金融卡時，預設為非勾選讓使用者點擊後顯示驗證碼 -->
<%-- 											<c:choose> --%>
<%-- 												<c:when test="${result_data.TransferType.equals('NPD')}"> --%>
<!-- 													<input type="radio" name="FGTXWAY" id="CMCARD" value="2"> -->
<%-- 												</c:when> --%>
<%-- 												<c:when test="${!sessionScope.isikeyuser}"> --%>
<!-- 													<input type="radio" name="FGTXWAY" id="CMCARD" value="2"> -->
<%-- 												</c:when> --%>
<%-- 												<c:otherwise> --%>
<!-- 												 	<input type="radio" name="FGTXWAY" id="CMCARD" value="2" checked="checked"> -->
<%-- 												</c:otherwise> --%>
<%-- 											</c:choose> --%>
											
<!-- 											<span class="ttb-radio"></span> -->
<!-- 										</label> -->
<!-- 									</div> -->
									
								</span>
							</div>
							
<!-- 							確認新使用者名稱 -->
<!-- 							<div class="ttb-input-item row"> -->
<!-- 								<span class="input-title">  -->
<!-- 									<label> -->
<!-- 										<h4> -->
<!-- 											驗證碼 -->
<!-- 										</h4> -->
<!-- 									</label> -->
<!-- 								</span>  -->
<!-- 								<span class="input-block"> -->
<!-- 									<div class="ttb-input"> -->
										
<%-- 										<spring:message code="LB.Captcha" var="labelCapCode" /> --%>
<!-- 										<input id="capCode" type="text" class="ttb-input" -->
<%-- 											name="capCode" placeholder="${labelCapCode}" maxlength="6" autocomplete="off"> --%>
<!-- 										<img name="kaptchaImage" src="" /> -->
<!-- 										&nbsp;<font color="#FF0000">※英文不分大小寫，限半型字 </font> -->
<!-- 										<div class="login-input-block"> -->
<!-- 											<input type="button" name="reshow" class="ttb-sm-btn btn-flat-orange" onclick="refreshCapCode()" value="重新產生驗證碼" /> -->
<!-- 										</div> -->
<!-- 									</div> -->
<!-- 								</span> -->
<!-- 							</div> -->
							
						</div>
						<input id="reset" name="reset" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" />	
						<input id="pageshow" name="pageshow" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
						<!--回上頁 -->
<%--                         <spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message> --%>
<%--                         <input type="button" name="CMBACK" id="CMBACK" value="${cmback}" class="ttb-button btn-flat-gray">						 --%>
					</div>
				</div>
			</form>
			<div class="text-left">
			    <ol class="list-decimal description-list">
			    	<p><spring:message code="LB.Description_of_page"/></p>
			        <li><spring:message code="LB.parking_withholding_P4_D1"/></li>
	            	<li><spring:message code="LB.parking_withholding_P4_D2"/></li>
		           	<li><spring:message code="LB.parking_withholding_P4_D3"/></li>
		           	<li><spring:message code="LB.parking_withholding_P4_D4"/><a href="#" onClick="window.open('http://www.tcgpmo.nat.gov.tw/index.php?act=query')" ><spring:message code="LB.parking_withholding_P4_D4-1"/></a><spring:message code="LB.parking_withholding_P4_D4-2"/>。</li>
		           	<li><spring:message code="LB.parking_withholding_P4_D5"/><a href="#" onClick="window.open('http://info1.tpc.gov.tw/querycar/parking.htm')" ><spring:message code="LB.parking_withholding_P4_D5-1"/></a><spring:message code="LB.parking_withholding_P4_D5-2"/>。</li>
		           	<li><spring:message code="LB.parking_withholding_P4_D6"/><a href="#" onClick="window.open('http://kpp.tbkc.gov.tw/')" ><spring:message code="LB.parking_withholding_P4_D6-1"/></a><spring:message code="LB.parking_withholding_P4_D6-2"/>。</li>
			    </ol>
			</div>
		</section>
		<!-- 		main-content END -->
	</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
	<!--   Js function -->
    <script type="text/JavaScript">
		$(document).ready(function() {
			// HTML載入完成後0.1秒開始遮罩
			setTimeout("initBlockUI()", 100);

			// 初始化驗證碼
// 			setTimeout("initKapImg()", 200);
			// 生成驗證碼
// 			setTimeout("newKapImg()", 300);
			
			init();
			
			// 過0.5秒解遮罩
			setTimeout("unBlockUI(initBlockId)", 800);
		});
		
		/**
		 * 初始化BlockUI
		 */
		function initBlockUI() {
			initBlockId = blockUI();
		}

	    function init(){
	    	$("#formId").validationEngine({binded:false,promptPosition: "inline" });
	    	$("#pageshow").click(function(e){			
				console.log("submit~~");
// 				var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
// 				var capData = $("#formId").serializeArray();
// 				var capResult = fstop.getServerDataEx(capUri, capData, false);
// 				if (capResult.result) {
				if(!$('#formId').validationEngine('validate') && $('input[name="FGTXWAY"]:checked').val() == 0){
		        	e.preventDefault();
	 			}else{
					$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
	 				$("#formId").validationEngine('detach');
	 				initBlockUI();
    				var action = '${__ctx}/PARKING/FEE/parking_withholding_apply_result';
	    			$("form").attr("action", action);
	    			unBlockUI(initBlockId);
	    			$("form").submit();
	 			}
// 				} else {
// 					alert("驗證碼有誤");
// 					changeCode();
// 				}
			});
    		//上一頁按鈕
    		$("#CMBACK").click(function() {
    			var action = '${__ctx}/PARKING/FEE/parking_withholding_apply';
    			$('#back').val("Y");
    			$("form").attr("action", action);
    			$("form").submit();
    		});
	    }	


 	</script>
</body>
</html>
 