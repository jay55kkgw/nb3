<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript"
	src="${__ctx}/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css"
	href="${__ctx}/css/validationEngine.jquery.css">
<link rel="stylesheet" type="text/css"
	href="${__ctx}/css/jquery.datetimepicker.css">

<script type="text/javascript">
	$(document).ready(function() {
		init();
	});
	function init() {
		$("#CMSUBMIT").click(
				function(e) {
					//TODO 驗證
					console.log("submit~~");
					initBlockUI(); //遮罩
					$("#formId").attr("action",
							"${__ctx}/DIGITAL/ACCOUNT/financial_card_renew_step1");
					$("#formId").submit();
				});
	}
</script>
</head>

<body>
<!-- 	 header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Register" /></li>
    <!-- 無卡提款     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0330" /></li>
    <!-- 線上註銷無卡提款功能     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1584" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code="LB.X0331"/></h2>
               <div id="step-bar">
                    <ul>
                        <li class="finished"><spring:message code="LB.D1204"/></li>
                        <li class="finished"><spring:message code="LB.D1585"/></li>
                        <li class="active"><spring:message code="LB.X2060"/></li>
                    </ul>
                </div>
			<form id="formId" method="post" action="">
				<div class="main-content-block row">
					
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
						 <div class="ttb-message">
                                <p><spring:message code="LB.X2060"/></p>
                         </div>
						
						<div class="ttb-input-item row">
							<span class="input-title"> <label>
									<h4><spring:message code="LB.D1587"/></h4>
							</label>
							</span> 
							<span class="input-block">
								<div class="ttb-input">
									<input type="text" name="NAATAPPLY" value="<spring:message code="LB.X0292"/>" hidden disabled>
									<span><spring:message code="LB.X0292"/></span>
								</div>
							</span>
						</div>
						<div class="ttb-input-item row">
							<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D1585"/></h4>
								</label>
							</span> 
							<span class="input-block">
								<div class="ttb-input">
								<input type="text" name="NAATAPPLY" value="${logout_without_card_step2.data.ACN}" hidden disabled>
								<span>${logout_without_card_step2.data.ACN}</span>
								</div>
							</span>
						</div>
						<div class="ttb-input-item row">
							<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D1588"/></h4>
								</label>
							</span> 
							<span class="input-block">
								<div class="ttb-input">
									<input type="text" name="NAATAPPLY" value="${logout_without_card_step2.data.DATETIME}" hidden disabled>
									<span>${logout_without_card_step2.data.DATETIME}</span>
								</div>
							</span>
						</div>
						</div>
					</div>
					<div class="col-12 text-center align-center" style="color:red;" >~<spring:message code="LB.X0333"/>~</div>
				</div>
			</form>
			<ol class="list-decimal description-list">
			
            </ol>
		</section>
		</main>
	<!-- 		main-content END -->
	<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>

</html>