<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %> 
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
	    <script type="text/javascript">
        $(document).ready(function () {
            initFootable(); // 將.table變更為footable 
            init();
   			genDateList();
   			setCITY();
   			setFlag();
			selectDropdown();
        });

        function init() {
			// $("#formId").validationEngine({binded: false,promptPosition: "inline", scroll: false});
			$("#formId").validationEngine({
				validationEventTriggers:'keyup blur', 
				binded:true,
				scroll:true,
				addFailureCssClassToField:"isValid",
				promptPosition: "inline" });
	    	$("#CMSUBMIT").click(function(e){
// 	    		$("#BRHCOD").val($("#BHID").val());
//     			$("#BRHNAME").val($("#BHID").find(":selected").text());
				if($("#S_MONEY").val() != ""){
	    			console.log($("#S_MONEY").val());
	    			$("#S_MONEY").val(parseInt($("#S_MONEY").val()));
	    			console.log($("#S_MONEY").val());
	    			$("#validate_SMONEY").val($("#S_MONEY").val());
    			}
//     			if($("#SALARY").val().length<=0)
//     				$("#SALARY").val('0');
    			
    			//SALARY值篩入validate_SALARY驗證
    			$("#validate_SALARY").val($("#SALARY").val());
    			
    			$("#BDEALING").val('');
    			$("#BDEALING_str").val('');
    			<!-- 存款 -->
    			if($('#BD1').prop('checked')){
        			$("#BDEALING").val($("#BDEALING").val() + $('#BD1').val());
        			$("#BDEALING_str").val($("#BDEALING_str").val() + ' <spring:message code= "LB.D1393" /> ');
    			}
    			<!-- 授信 -->
    			if($('#BD2').prop('checked')){
        			$("#BDEALING").val($("#BDEALING").val() + $('#BD2').val());
        			$("#BDEALING_str").val($("#BDEALING_str").val() + ' <spring:message code= "LB.D1394" /> ');
    			}
    			<!-- 現金卡 -->
    			if($('#BD3').prop('checked')){
        			$("#BDEALING").val($("#BDEALING").val() + $('#BD3').val());
        			$("#BDEALING_str").val($("#BDEALING_str").val() + '<spring:message code= "LB.D1395" /> ');
    			}
    			<!-- 壽險 -->
    			if($('#BD4').prop('checked')){
        			$("#BDEALING").val($("#BDEALING").val() + $('#BD4').val());
        			$("#BDEALING_str").val($("#BDEALING_str").val() + ' <spring:message code= "LB.D1396" /> ');
    			}
    			if($('#BD5').prop('checked')){
        			$("#BDEALING").val($("#BDEALING").val() + $('#BD5').val());
        			$("#BDEALING_str").val($("#BDEALING_str").val() + $('#BREASON').val());
    				$('#BREASON').addClass("validate[required]");
    			}else{
    				$('#BREASON').removeClass("validate[required]");
    			}
    			<!--無-->
    			if($('#BD6').prop('checked')){
        			$("#BDEALING").val($("#BDEALING").val() + $('#BD6').val());
        			$("#BDEALING_str").val($("#BDEALING_str").val() + ' <spring:message code= "LB.D1070_2" /> ');
    			}
//     			$("#CUSIDN").val($("#CUSIDN").val().toUpperCase());
    			
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
// 		        	initBlockUI();
		        	var action = '${__ctx}/DIGITAL/ACCOUNT/modify_digital_data_p2_2';
					$("form").attr("action", action);
	    			$("form").submit();
				}
			});
			$("#CMRESET").click(function(e){
				$("#formId")[0].reset();
				getTmr();
			});
			$("#CMBACK").click( function(e) {
				$("#formId").validationEngine('detach');
				console.log("submit~~");
    			$("#formId").attr("action", "${__ctx}/DIGITAL/ACCOUNT/modify_digital_data_p1");
	        	$("#back").val('Y');
	
// 	         	initBlockUI();
	            $("#formId").submit();
			});
        }

		function ValidateValue(textbox) {
			var IllegalString = "+-_＿︿%@[`~!#$^&*()=|{}':;',\\[\\].<>/?~！#￥……&*（）——|{}【】‘；：”“'。，、？]‘'";
			var textboxvalue = textbox.value;
			var index = textboxvalue.length - 1;
			var s = textbox.value.charAt(index);
			if (IllegalString.indexOf(s) >= 0) {
				s = textboxvalue.substring(0, index);
				textbox.value = s;
			}
		}

		function genDateList(){
			var thisYear = new Date().getFullYear();
			for(var i = 1;i <= thisYear - 1911;i++){
				j = i.toString();
				for(k = 0;k <= 2 - i.toString().length;k++)j='0' + j;
				if('${result_data.data.CCBIRTHDATEYY}' == j){
					$("#CCBIRTHDATEYY").append("<option selected>" + j + "</option>");
				}else{
					$("#CCBIRTHDATEYY").append("<option>" + j + "</option>");
				}
			}
			for(var i = 1;i <= 12;i++){
				j = i.toString();
				for(k = 0;k <= 1 - i.toString().length;k++)j='0' + j;
				if('${result_data.data.CCBIRTHDATEMM}' == j){
					$("#CCBIRTHDATEMM").append("<option selected>" + j + "</option>");
				}else{
					$("#CCBIRTHDATEMM").append("<option>" + j + "</option>");
				}
			}
			for(var i = 1;i <= 31;i++){
				j = i.toString();
				for(k = 0;k <= 1 - i.toString().length;k++)j='0' + j;
				if('${result_data.data.CCBIRTHDATEDD}' == j){
					$("#CCBIRTHDATEDD").append("<option selected>" + j + "</option>");
				}else{
					$("#CCBIRTHDATEDD").append("<option>" + j + "</option>");
				}
			}
		}
		
		// 日曆欄位參數設定
		function datetimepickerEvent() {
			$(".CMDATE2").click(function (event) {
				$('#CMDATE2').datetimepicker('show');
			});
// 			$(".ENDSERACHDATE").click(function (event) {
// 				$('#ENDSERACHDATE').datetimepicker('show');
// 			});
			jQuery('.datetimepicker').datetimepicker({
				timepicker: false,
				closeOnDateSelect: true,
				scrollMonth: false,
				scrollInput: false,
				format: 'Y/m/d',
				lang: '${transfer}'
			});
		}
		//預約自動輸入今天
		function getTmr() {
			var today = new Date();
			today.setDate(today.getDate());
			var y = today.getFullYear();
			var m = today.getMonth() + 1;
			var d = today.getDate();
			if(m<10){
				m = "0"+m
			}
			if(d<10){
				d="0"+d
			}
			var tmr = y + "/" + m + "/" + d
			$('#CMDATE2').val(tmr);
		}

		function doquery2(cityFilter,areaFilter){
			if($("#SAMEADDRCHK").val() == 'Y'){
				$("#CITY2").val($("#CITY1").val());
				$("#CITY3").val($("#CITY1").val());
			}
			$.ajax({
		        type :"POST",
		        url  : "${__ctx}/ONLINE/APPLY/apply_deposit_account_get_area",
		        data : { 
		        	CITY : $(cityFilter).val()
		        },
		        async: false ,
		        dataType: "json",
		        success : function(msg) {
		        	//請選擇市/區鄉鎮
		        	$(areaFilter).html("<option value='#'>--- <spring:message code= "LB.X0546" /> ---</option> ");
		        	msg.data.Area.forEach(function(area){
		        		console.log(area.AREA);
		        		$(areaFilter).append("<option>" + area.AREA + "</option>");
		        	});
					if($("#SAMEADDRCHK").val() == 'Y'){
						//請選擇市/區鄉鎮
			        	$('#ZONE2').html("<option value='#'>--- <spring:message code= "LB.X0546" /> ---</option> ");
			        	msg.data.Area.forEach(function(area){
			        		console.log(area.AREA);
			        		$("#ZONE2").append("<option>" + area.AREA + "</option>");
			        	});
					}
		        }
		    })
		}
		

// 		function doquery3(cityFilter,areaFilter,zipFilter1,zipFilter2){
// 			if($("#SAMEADDRCHK").val() == 'Y'){
// 				console.log('SAMEADDRCHK~');
// 				$("#ZONE2").val($("#ZONE1").val());
// 				$("#ZONE3").val($("#ZONE1").val());
// 			}
// 			$.ajax({
// 		        type :"POST",
// 		        url  : "${__ctx}/ONLINE/APPLY/apply_deposit_account_get_zip_code",
// 		        data : { 
// 		        	CITY : $(cityFilter).val(),
// 		        	AREA : $(areaFilter).val()
// 		        },
// 		        async: false ,
// 		        dataType: "json",
// 		        success : function(msg) {
// 	        		console.log(msg.data);
// 	        		$(zipFilter1).html(msg.data.Zip);
// 	        		$(zipFilter2).val(msg.data.Zip);
// 	        		if($("#SAMEADDRCHK").val() == 'Y'){
// 		        		$("#ZIP21").html(msg.data.Zip);
// 		        		$("#ZIP2").val(msg.data.Zip);
// 	        		}
// 		        }
// 		    })
// 		}
		
		function doquery4(cityFilter,areaFilter,zipFilter1){
			if($("#SAMEADDRCHK").val() == 'Y'){
				console.log('SAMEADDRCHK~');
				$("#ZONE2").val($("#ZONE1").val());
			}
			$.ajax({
		        type :"POST",
		        url  : "${__ctx}/ONLINE/APPLY/apply_deposit_account_get_zip_code",
		        data : { 
		        	CITY : $(cityFilter).val(),
		        	AREA : $(areaFilter).val()
		        },
		        async: false ,
		        dataType: "json",
		        success : function(msg) {
	        		console.log(msg);
	        		$(zipFilter1).val(msg.data.Zip);
	        		if($("#SAMEADDRCHK").val() == 'Y'){
	        			doquery8();
	        		}
		        }
		    })
		}
		
		
		function doquery8(){
			$.ajax({
				type :"POST",
		        url  : "${__ctx}/ONLINE/APPLY/apply_deposit_account_get_zip_bh",
		        data : { 
		        	zip : $("#ZIP3").val()
		        },
		        async: false ,
		        dataType: "json",
		        success : function(msg) {
		        	//請選擇分行
		        	$('#BHID').html("<option value='#'>--- <spring:message code= "LB.X1003" /> ---</option>  ");
		        	msg.data.bh.forEach(function(bh){
		        		console.log(bh);
		        		$("#BHID").append("<option value='" + bh.adbranchid +"'> " + bh.adbranchname + "</option>");
		        	});
		        	$("#BHID option:eq(1)").prop("selected",true);
		        }
			})
		}
		function setCITY(){
    		$("#CITY1").children().each(function(){
    		    if ($(this).text()=='${result_data.data.CITY1}'){
    		        //jQuery給法
    		        console.log("TRUE");
    		        $(this).prop("selected", true); //或是給"selected"也可
    		    }
    		});
    		
    		doquery2('#CITY1','#ZONE1');

    		$("#CITY2").children().each(function(){
    		    if ($(this).text()=='${result_data.data.CITY2}'){
    		        //jQuery給法
    		        console.log("TRUE");
    		        $(this).prop("selected", true); //或是給"selected"也可
    		    }
    		});
    		doquery2('#CITY2','#ZONE2');
    		$("#ZONE2").children().each(function(){
    		    if ($(this).text()=='${result_data.data.ZONE2}'){
    		        //jQuery給法
    		        console.log("TRUE");
    		        $(this).prop("selected", true); //或是給"selected"也可
    		    }
    		});
    		$("#ZONE1").children().each(function(){
    		    if ($(this).text()=='${result_data.data.ZONE1}'){
    		        //jQuery給法
    		        console.log("TRUE");
    		        $(this).prop("selected", true); //或是給"selected"也可
    		    }
    		});
    		$("#BHID").children().each(function(){
    		    if ($(this).val()=='${result_data.data.BHID}'){
    		        //jQuery給法
    		        console.log("TRUE");
    		        $(this).prop("selected", true); //或是給"selected"也可
    		    }
    		});

		}
		function setFlag(){
			//本次開戶目的 (預設設定)
			$("#PURPOSE").children().each(function(){
    		    if ($(this).val()=='${result_data.data.PURPOSE}'){
    		        $(this).prop("selected", true); //或是給"selected"也可
    		    }
    		});

			if ($("#P5").prop("selected")) {
				$("[toggle-input='P5']").show();
			}  
			
			//主要資金/財產來源 (預設設定)
			$("#MAINFUND").children().each(function(){
    		    if ($(this).val()=='${result_data.data.MAINFUND}'){
    		        $(this).prop("selected", true); //或是給"selected"也可
    		    }
    		});

			if ($("#M8").prop("selected")) {
				$("[toggle-input='M8']").show();
			} 
			
			//與本行已往來業務(預設設定)
			if('${result_data.data.BDEALING}'.indexOf('A') != -1){
				$("#BD1").attr("checked",true);
			}
			if('${result_data.data.BDEALING}'.indexOf('B') != -1){
				$("#BD2").attr("checked",true);
			}
			if('${result_data.data.BDEALING}'.indexOf('C') != -1){
				$("#BD3").attr("checked",true);
			}
			if('${result_data.data.BDEALING}'.indexOf('D') != -1){
				$("#BD4").attr("checked",true);
			}
			if('${result_data.data.BDEALING}'.indexOf('E') != -1){
				$("#BD5").attr("checked",true);
			}
			if('${result_data.data.BDEALING}'.indexOf('F') != -1){
				$("#BD6").attr("checked",true);
			}
			//與本行往來金額
			if('${result_data.data.SMONEY}'){
				$('#S_MONEY').val('${result_data.data.SMONEY}');
			}
		}
		
		function changebedealing(num)
		{	
			console.log(num);
			if(num=="6")
		    {
				$("#BD1").prop("checked",false);
				$("#BD2").prop("checked",false);
				$("#BD3").prop("checked",false);
				$("#BD4").prop("checked",false);
				$("#BD5").prop("checked",false);
				$("#BREASON").val('');
		    }
			else if(num=="5" || num=="4" || num=="3" || num=="2" || num=="1")
		    {
				$("#BD6").prop("checked",false);
		    }
		}

		function selectDropdown() {
			//選擇'其他'出現文字輸入框
			$( ".custom-select" ).on( "change", function() {
				if($(this).children("option:selected").attr("id") == "P5" || $(this).children("option:selected").attr("id") == "M8") {
					$(this).parent().siblings(".tbb-toggle-input").show();
				} else {    
					$(this).parent().siblings(".tbb-toggle-input").hide();
				}
			});
		}

    </script>
	<style>
			@media screen and (max-width:767px) {
			.ttb-button {
					width: 38%;
					left: 36px;
			}
			#CMBACK.ttb-button{
					border-color: transparent;
					box-shadow: none;
					color: #333;
					background-color: #fff;
			}
 

		}
	</style>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 修改數位存款帳戶資料     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1474" /></li>
		</ol>
	</nav>

	<div class="content row">
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
			<!-- 修改數位存款帳戶資料 -->
				<h2>
					 <spring:message code= "LB.D1474" />
				</h2>
				<div id="step-bar">
                    <ul>
						<li class="finished">身分驗證</li>
                        <li class="active">開戶資料</li>
                        <li class="">確認資料</li>
						<li class="">完成修改</li>
                    </ul>
                </div>
                <form id="formId" method="post">
					<input type="hidden" name="ADOPID" id="ADOPID" value="N104">
					<input type="hidden" name="FGTXWAY" id="FGTXWAY" value="${result_data.data.FGTXWAY}">	
					<input type="hidden" name="BRHCOD" id="BRHCOD" value="${result_data.data.BRHCOD}">
					<input type="hidden" name="BRHNAME" id="BRHNAME" value="">	
					<input type="hidden" name="BIRTHDAY" id="BIRTHDAY" value="">	
					<input type="hidden" name="POSTCOD1" id="POSTCOD1" value="">	
					<input type="hidden" name="PMTADR" id="PMTADR" value="">	
					<input type="hidden" name="POSTCOD2" id="POSTCOD2" value="">	
					<input type="hidden" name="CTTADR" id="CTTADR" value="">	
					<input type="hidden" name="BUSTEL" id="BUSTEL" value="">	
					<input type="hidden" name="ID_CHGE" id="ID_CHGE" value="">	
					<input type="hidden" name="CHGE_DT" id="CHGE_DT" value="">	
					<input type="hidden" name="CHGE_CY" id="CHGE_CY" value="">
					<input type="hidden" name="CHGE_CYNAME" id="CHGE_CYNAME" value="">	
					<input type="hidden" name="ISBRANCH" id="ISBRANCH" value="">	
					<input type="hidden" name="CAREER1_str" id="CAREER1_str" value="">
					<input type="hidden" name="BDEALING_str" id="BDEALING_str" value="">
					<input type="hidden" name="TODAY" id="TODAY" value="${ today }">
					<input type="hidden" id="back" name="back" value="">
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12 tab-content">
                        
							<div class="ttb-input-block">
							 <div class="ttb-message">
                                <p>修改開戶資料 (1/2)</p>
                            </div>
                            <div class="classification-block">
                                <p>開戶資訊</p>
                                <p>( <span class="high-light">*</span> 為必填)</p>
                            </div>
								<!-- ********** -->
								<!--★本次開戶目的-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4>
												<spring:message code= "LB.D1075" /><span class="high-light">*</span>
											</h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input ">
											<select name="PURPOSE" id="PURPOSE" class="custom-select validate[required]">
												<!-- 儲蓄 -->
												<option id="P1" value="A">
													<spring:message code="LB.D1076"/>
												</option>
												<!-- 薪轉 -->
												<option id="P2" value="B">
													<spring:message code="LB.D1077"/>
												</option>
												<!-- 資金調撥 -->
												<option id="P3" value="C">
													<spring:message code="LB.D1078"/>
												</option>
												<!-- 投資 -->
												<option id="P4" value="D">
													<spring:message code="LB.D1079"/>
												</option>
												<!-- 其他 -->
												<option id="P5" value="E">
													<spring:message code="LB.D0572"/>
												</option>
											</select>
										</div>
										<div class="ttb-input tbb-toggle-input" style="display: none;" toggle-input="P5">
											<input type="text" class="text-input validate[condRequired[P5]]" placeholder="請填入" name="PREASON" id="PREASON" size="11" maxlength="10" value="${result_data.data.PREASON}">
										</div>
									</span>
								</div>
								<!-- ********** -->
								<!--★與本行已往來業務-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
<!-- 與本行已往來業務 -->
											<h4><spring:message code= "LB.D1391" /><span class="high-light">*</span></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
										
											<label class="check-block" for="BD6">
												
												<input type="checkbox" name="BDEALING6" id="BD6" value="F" onclick="changebedealing('6');">
<!-- 無 -->
												<spring:message code= "LB.D1070_2" />
												<span class="ttb-check"></span>
											</label>
	
											<label class="check-block" for="BD1">
												<input type="checkbox" name="BDEALING1" id="BD1" value="A"  onclick="changebedealing('1');"/>
<!-- 存款 -->
												<spring:message code= "LB.D1393" />
												<span class="ttb-check"></span>
											</label>
											<label class="check-block" for="BD2">
												<input type="checkbox" name="BDEALING2" id="BD2" value="B" onclick="changebedealing('2');"/>
<!-- 授信 -->
												<spring:message code= "LB.D1394" />
												<span class="ttb-check"></span>
											</label>
											<label class="check-block" for="BD3">
												<input type="checkbox" name="BDEALING3" id="BD3" value="C" onclick="changebedealing('3');"/>
<!-- 現金卡 -->
												<spring:message code= "LB.D1395" />
												<span class="ttb-check"></span>
											</label>
											<label class="check-block" for="BD4">
												<input type="checkbox" name="BDEALING4" id="BD4" value="D" onclick="changebedealing('4');"/>
<!-- 壽險 -->
												<spring:message code= "LB.D1396" />
												<span class="ttb-check"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="check-block" for="BD5">
												<input type="checkbox" name="BDEALING5" id="BD5" value="E" onclick="changebedealing('5');"/>
<!-- 其他 -->
												<spring:message code= "LB.D0572" />
												<span class="ttb-check"></span>
											</label>
											<input type="text" class="text-input" placeholder="請填入" value="${result_data.data.BREASON}" name="BREASON" id="BREASON"  size="11" maxlength="10">
										</div>
										<span class="hideblock">
											<input type="text" class="validate[required]" name="BDEALING" id="BDEALING" value=""
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</span>
										
									</span>
								</div>
								<!-- ********** -->
								<!--★主要資金/財產來源-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
<!-- 主要資金/財產來源 -->
											<h4><spring:message code= "LB.D1397" /><span class="high-light">*</span></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<select name="MAINFUND" id="MAINFUND" class="custom-select validate[required]">
												<!-- 薪資 -->
												<option id="M1" value="A">
													<spring:message code="LB.D1398"/>
												</option>
												<!-- 儲蓄 -->
												<option id="M2" value="B">
													<spring:message code="LB.D1076"/>
												</option>
												<!-- 營運 -->
												<option id="M3" value="C">
													<spring:message code="LB.D1399"/>
												</option>
												<!-- 投資 -->
												<option id="M4" value="D">
													<spring:message code="LB.D1079"/>
												</option>
												<!-- 銷售 -->
												<optionid="M5" value="E">
													<spring:message code="LB.D1400"/>
												</option>
												<!-- 家業繼承 -->
												<option id="M6" value="F">
													<spring:message code="LB.D1401"/>
												</option>
												<!-- 退休金 -->
												<option id="M7" value="G">
													<spring:message code="LB.D0910"/>
												</option>
												<!-- 其他 -->
												<option id="M8" value="H">
													<spring:message code="LB.D0572"/>
												</option>
											</select>
										</div>
										<div class="ttb-input tbb-toggle-input" style="display: none;" toggle-input="M8">
											<!-- 其他 -->
											<input type="text" class="text-input validate[condRequired[M8]]" placeholder="請填入" name="MREASON" id="MREASON" value="${result_data.data.MREASON}">
										</div>
									</span>
								</div>
								<!-- ********** -->
								
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 個人/家庭年收入 -->
											<spring:message code= "LB.D1143" /><span class="high-light">*</span>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="SALARY" name="SALARY" class="text-input" placeholder="請輸入個人/家庭年收入(新台幣)" value="${result_data.data.SALARY.length() == 0 ? '0':result_data.data.SALARY}" maxLength="6" size="8"/>
											<spring:message code= "LB.D1144" />
											<span class="hideblock">
												<input id="validate_SALARY" name="validate_SALARY" type="text"  
													class="validate[required]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
											</span>
										</div>
									</span>
								</div>
								<!-- ********** -->
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
 								<!-- 預期往來金額(近一年) -->
											<spring:message code= "LB.D1081_1" /><span class="high-light">*</span>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
								<!-- 萬元 -->
											<input type="text" id="S_MONEY" name="SMONEY" placeholder="請輸入預期往來金額(近一年)" class="text-input " value="${result_data.data.SMONEY}" maxLength="7" size="8"/><spring:message code= "LB.D0088_2" />
											
											<span class="hideblock">
												<input id="validate_SMONEY" name="validate_SMONEY" type="text" class="validate[required]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
											</span>
										</div>
									</span>
								</div>
								<!-- 推薦碼 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											推薦碼
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="EMPNO" name="EMPNO" placeholder="請填入6位數字" class="text-input validate[funcCall[validate_CheckLenEqual[推薦碼,EMPNO,true,6]],
											funcCall[validate_CheckNumWithDigit2['推薦碼',EMPNO,true]]]" value="${result_data.data.EMPNO}" maxLength="6" size="8"/>
										</div>
									</span>
								</div>
<!-- 								<div class="ttb-input-item row"> -->
<!-- 									<span class="input-title">  -->
<!-- 									<label> -->
<!-- 										<h4> -->
<!-- <!-- 										★指定服務分行 --> 
<%-- 											<spring:message code="LB.D1472"/><span class="high-light">*</span> --%>
<!-- 										</h4> -->
<!-- 									</label> -->
<!-- 									</span>  -->
<!-- 									<span class="input-block"> -->
<!-- 										<div class="ttb-input"> -->
<!-- 											<input type="hidden" name="ZIP2" id="ZIP2" value="" size=4 maxlength=3> -->
<!-- 											<select name="CITY2" id="CITY2" class="custom-select select-input input-width-125 validate[required]" onchange="doquery2('#CITY2','#ZONE2')"> -->
<%-- 								          		<option value="#">--- <spring:message code="LB.Select"/><spring:message code="LB.D0059"/> ---</option>   --%>
<%-- 								          		<c:forEach var="dataList" items="${city_data}"> --%>
<%-- 													<option>${dataList.CITY}</option> --%>
<%-- 												</c:forEach> --%>
<!-- 											</select> -->
<%-- <%-- 											<spring:message code="LB.D1118"/> --%> 
<!-- 											<select name="ZONE2" id="ZONE2" class="custom-select select-input input-width-100 validate[required]"  onchange="doquery4('#CITY2','#ZONE2','#ZIP2')"> -->
<%-- 								          		<option value="#">--- <spring:message code="LB.Select"/><spring:message code="LB.D0060"/> ---</option> --%>
<!-- 											</select> -->
<%-- <%-- 											<spring:message code="LB.D1490"/> --%> 
<!-- 										</div> -->
<!-- 										<div class="ttb-input"> -->
<%-- 											<select name="BHID" id="BHID" class="custom-select select-input validate[required]" value="${result_data.data.BHID}"> --%>
<%-- 								          		<option value="#">--- <spring:message code="LB.Select"/><spring:message code="LB.X0169"/> ---</option>   --%>
<%-- 								          		<c:forEach var="dataList" items="${bh_data}"> --%>
<%-- 													<option value="${dataList.ADBRANCHID}"> ${dataList.ADBRANCHNAME}</option> --%>
<%-- 												</c:forEach> --%>
<!-- 											</select> -->
<!-- 										</div> -->
<!-- 									</span> -->
<!-- 								</div> -->
                           	</div>
	                        <!--button 區域 -->
                            <!-- 下一步 -->
                            <input id="CMSUBMIT" name="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code= "LB.X0080" />" />
                        <!--                     button 區域 -->
                        </div>  
                    </div>
                </form>
				<!-- 				20211028 Han 要求新增  -->
				<div class="text-left">
				    <!-- 		說明： -->
				    <ol class="list-decimal text-left description-list">
				    <p><spring:message code="LB.Description_of_page"/></p>
        				<li><spring:message code="LB.Apply_Deposit_Account_P3_D2"/></li>
				    </ol>
				</div>

			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>