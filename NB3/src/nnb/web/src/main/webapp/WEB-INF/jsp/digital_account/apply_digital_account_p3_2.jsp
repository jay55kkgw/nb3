<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %> 
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.twzipcode-1.6.1.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
	<style>
		.zipcode {
			display: none;
		}
	
	@media screen and (max-width:767px) {
		.ttb-button {
				width: 38%;
				left: 36px;
		}
		#CMBACK.ttb-button{
				border-color: transparent;
				box-shadow: none;
				color: #333;
				background-color: #fff;
		}
 

	}

	</style>
	    <script type="text/javascript">
	    oldBHID = '${result_data.data.BHID}';
	    firstTimeBHID = true;
		var isTimeout = "${not empty sessionScope.timeout}";
		var countdownObjheader = isTimeout == 'true' ? "${sessionScope.timeout}" : 600;
		var countdownSecHeader = isTimeout == 'true' ? "${sessionScope.timeout}" : 600;
        $(document).ready(function () {
            initFootable(); // 將.table變更為footable 
            init();
   			setCITY();
   			setFlag();
   			selectDropdown();
			timeLogout();
        });
		
        function init() {
			$("#formId").validationEngine({
			validationEventTriggers:'keyup blur', 
			binded:true,
			scroll:true,
			addFailureCssClassToField:"isValid",
			promptPosition: "inline" });
// 			$("input[name='ZONE3']").change(doquery8());
	    	$("#CMSUBMIT").click(function(e){
    			
	    		//指定分行 ，放入 BRHCOD 隱藏欄位 與 BRHNAME 隱藏欄位
    			$("#BRHCOD").val($("#BHID").val());
    			$("#BRHNAME").val($("#BHID").find(":selected").text());

    			//個人/家庭年收入 預設為0
//     			if($("#SALARY").val().length<=0)
//     				$("#SALARY").val('0');
    			
    			//與本行已往來業務，串接放至 BDEALING 與 BDEALING_str 隱藏欄位
    			$("#BDEALING").val('');
    			$("#BDEALING_str").val('');
    			if($('#BD1').prop('checked')){
        			$("#BDEALING").val($("#BDEALING").val() + $('#BD1').val());
        			$("#BDEALING_str").val($("#BDEALING_str").val() + '<spring:message code= "LB.D1393" />');
    			}
    			if($('#BD2').prop('checked')){
        			$("#BDEALING").val($("#BDEALING").val() + $('#BD2').val());
        			$("#BDEALING_str").val($("#BDEALING_str").val() + '<spring:message code= "LB.D1394" />');
    			}
    			if($('#BD3').prop('checked')){
        			$("#BDEALING").val($("#BDEALING").val() + $('#BD3').val());
        			$("#BDEALING_str").val($("#BDEALING_str").val() + '<spring:message code= "LB.D1395" />');
    			}
    			if($('#BD4').prop('checked')){
        			$("#BDEALING").val($("#BDEALING").val() + $('#BD4').val());
        			$("#BDEALING_str").val($("#BDEALING_str").val() + '<spring:message code= "LB.D1396" />');
    			}
    			if($('#BD5').prop('checked')){
        			$("#BDEALING").val($("#BDEALING").val() + $('#BD5').val());
        			$("#BDEALING_str").val($("#BDEALING_str").val() + $('#BREASON').val());
    				$('#BREASON').addClass("validate[required]");
    			}else{
    				$('#BREASON').removeClass("validate[required]");
    			}
    			if($('#BD6').prop('checked')){
        			$("#BDEALING").val($("#BDEALING").val() + $('#BD6').val());
        			$("#BDEALING_str").val($("#BDEALING_str").val() + '<spring:message code= "LB.D1070_2" />');
    			}
				
    			//SALARY值篩入validate_SALARY驗證
    			$("#validate_SALARY").val($("#SALARY").val());
				//將S_MONEY值塞入validate_S_MONEY驗證
				$("#validate_S_MONEY").val($("#S_MONEY").val());

				e = e || window.event;
    			//進行 validation 並送出
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
					var b = ["<spring:message code= "LB.Confirm021" />"];
					errorBlock(null, null, b, '取消', '確定');
				}
			});
			
			$("#CMRESET").click(function(e){
				$("#formId")[0].reset();
			});
			$("#CMBACK").click( function(e) {
				$("#formId").validationEngine('detach');
				console.log("submit~~");
    			$("#formId").attr("action", "${__ctx}/DIGITAL/ACCOUNT/apply_digital_account_p3");
				$("#back").val('Y');
	            $("#formId").submit();
			});
			$("#errorBtn2").click(function(e) {
				$("#formId").validationEngine('detach');
	        	var action = '${__ctx}/DIGITAL/ACCOUNT/apply_digital_account_p5';
	        	$("#formId").attr("action", action);
	        	$("#formId").submit();
			});
			$("#errorBtn1").click(function(e) {
				$('#error-block').hide();
			});
			
			$("#BHID").on('change', function() {
				if(this.value == "#"){
					$("#address").empty();
					$("#mapicon").removeAttr("href");
					$("#addressmap").hide();
				}
				else{
					$("#addressmap").show();
					$.ajax({
						type :"POST",
				        url  : "${__ctx}/DIGITAL/ACCOUNT/apply_deposit_branch",
				        data : { 
				        	zip : $("#BHID").val()
				        },
				        async: false ,
				        dataType: "json",
				        success : function(msg) {
				        	msg.data.AdmBh.forEach(function(bh){
				        		console.log(bh);
					        	$("#address").html(bh.address);
					        	$("#BRHADDRESS").val(bh.address);
					        	$("#mapicon").attr("href","https://www.google.com/maps/search/"+bh.address+"臺灣企銀"+bh.adbranchname);
					        	
				        	});
				        }
					})
				}
			});
        }
		
		//顯示該Zip下之分行
		function doquery8(){
			$.ajax({
				type :"POST",
		        url  : "${__ctx}/ONLINE/APPLY/apply_deposit_account_get_city_bh",
		        data : { 
		        	zip : $("#CITY3").val()
		        },
		        async: false ,
		        dataType: "json",
		        success : function(msg) {
		        	if($("#BHID").val() != "#")
		        		oldBHID = $("#BHID").val();
		        	$('#BHID').html("<option value='#'><spring:message code= "LB.X1003" /></option>  ");
		        	msg.data.bh.forEach(function(bh){
		        		console.log(bh);
		        		$("#BHID").append("<option value='" + bh.adbranchid +"'> " + bh.adbranchname + "</option>");
		        	});
		        	$("#BHID option:eq(1)").prop("selected",true);
		    		$("#BHID").children().each(function(){
		    		    if ($(this).val()==oldBHID){
		    		        $(this).prop("selected", true); //或是給"selected"也可
		    		    }
		    		});
		    		$("#BHID").change();
		        }
			})
		}
		
		//初始化 指定服務分行
		function setCITY(){
   			llo = "${__ctx}/js/";
			$('#twzipcode').twzipcode({
                countyName: 'CITY3',
                districtName: 'ZONE3',
                zipcodeName: 'ZIP3',
                countySel: '${result_data.data.CITY2}' == ''? '${result_data.data.CITY2}' :'${result_data.data.CITY3}',
                districtSel: '${result_data.data.ZONE2}' == ''?'${result_data.data.ZONE2}' : '${result_data.data.ZONE3}',
                onCountySelect: function(){
                	if($("#CITY3").val() != "#"){
	                	doquery8();
	                	if(firstTimeBHID){
	                		$("#BHID").children().each(function(){
	                		    if ($(this).val()=='${result_data.data.BHID}'){
	                		        $(this).prop("selected", true); //或是給"selected"也可
	                		        firstTimeBHID = !firstTimeBHID;
	                		        $("#BHID").change();
	                		    }
	                		});
	                	}
                	}
                	else{
                		$("#BHID").val("#");
                		$("#BHID").change();
                	}
                },
                language:'zip_${pageContext.response.locale}',
                onDistrictSelect: null
			});
//     		$("#BHID").children().each(function(){
//     		    if ($(this).val()=='${result_data.data.BHID}'){
//     		        //jQuery給法
//     		        console.log("TRUE");
//     		        $(this).attr("selected", true); //或是給"selected"也可
//     		    }
//     		});
		}
		
		function setFlag(){
			//本次開戶目的 (預設設定)
			$("#PURPOSE").children().each(function(){
    		    if ($(this).val()=='${result_data.data.PURPOSE}'){
    		        $(this).prop("selected", true); //或是給"selected"也可
    		    }
    		});

			if ($("#P5").prop("selected")) {
				$("[toggle-input='P5']").show();
			} 
			
			//主要資金/財產來源 (預設設定)
			$("#MAINFUND").children().each(function(){
    		    if ($(this).val()=='${result_data.data.MAINFUND}'){
    		        $(this).prop("selected", true); //或是給"selected"也可
    		    }
    		});
			if ($("#M8").prop("selected")) {
				$("[toggle-input='M8']").show();
			}
			
			//與本行已往來業務(預設設定)
			if('${result_data.data.BDEALING}'.indexOf('A') != -1){
				$("#BD1").attr("checked",true);
			}
			if('${result_data.data.BDEALING}'.indexOf('B') != -1){
				$("#BD2").attr("checked",true);
			}
			if('${result_data.data.BDEALING}'.indexOf('C') != -1){
				$("#BD3").attr("checked",true);
			}
			if('${result_data.data.BDEALING}'.indexOf('D') != -1){
				$("#BD4").attr("checked",true);
			}
			if('${result_data.data.BDEALING}'.indexOf('E') != -1){
				$("#BD5").attr("checked",true);
			}
			if('${result_data.data.BDEALING}'.indexOf('F') != -1){
				$("#BD6").attr("checked",true);
			}
		}
		
		function changebedealing(num)
		{	
			console.log(num);
			if(num=="6")
		    {
				$("#BD1").prop("checked",false);
				$("#BD2").prop("checked",false);
				$("#BD3").prop("checked",false);
				$("#BD4").prop("checked",false);
				$("#BD5").prop("checked",false);
				$("#BREASON").val('');
		    }
			if(num=="5" || num=="4" || num=="3" || num=="2" || num=="1")
		    {
				$("#BD6").prop("checked",false);
		    }
		}

		function selectDropdown() {
			//選擇'其他'出現文字輸入框
			$( ".custom-select" ).on( "change", function() {
				if($(this).children("option:selected").attr("id") == "P5" || $(this).children("option:selected").attr("id") == "M8") {
					$(this).parent().siblings(".tbb-toggle-input").show();
				} else {    
					$(this).parent().siblings(".tbb-toggle-input").hide();
				}
			});
		}

		function timeLogout(){
			// 刷新session
			var uri = '${__ctx}/login_refresh';
			console.log('refresh.uri: '+uri);
			var result = fstop.getServerDataEx( uri, null, false, null);
			console.log('refresh.result: '+JSON.stringify(result));
			// 初始化登出時間
			$("#countdownheader").html(parseInt(countdownSecHeader)+1);
			$("#countdownMin").html("");
			$("#mobile-countdownMin").html("");
			$("#countdownSec").html("");
			$("#mobile-countdownSec").html("");
			// 倒數
			startIntervalHeader(1, refreshCountdownHeader, []);
		}

		function refreshCountdownHeader(){
			// timeout剩餘時間
			var nextSec = parseInt($("#countdownheader").html()) - 1;
			$("#countdownheader").html(nextSec);

			// 提示訊息--即將登出，是否繼續使用
			if(nextSec == 120){
				initLogoutBlockUI();
			}
			// timeout
			if(nextSec == 0){
				// logout
				fstop.logout( '${__ctx}'+'/logout_aj' , '${__ctx}'+'/timeout_logout');
			}
			if (nextSec >= 0){
				// 倒數時間以分秒顯示
				var minutes = Math.floor(nextSec / 60);
				$("#countdownMin").html(('0' + minutes).slice(-2));
				$("#mobile-countdownMin").html(('0' + minutes).slice(-2));

				var seconds = nextSec - minutes * 60;
				$("#countdownSec").html(('0' + seconds).slice(-2));
				$("#mobile-countdownSec").html(('0' + seconds).slice(-2));
			}
		}
		function startIntervalHeader(interval, func, values){
			clearInterval(countdownObjheader);
			countdownObjheader = setRepeater(func, values, interval);
		}
		function setRepeater(func, values, interval){
			return setInterval(function(){
				func.apply(this, values);
			}, interval * 1000);
		}
		/**
		 * 初始化logoutBlockUI
		 */
		function initLogoutBlockUI() {
			logoutblockUI();
		}

		/**
		 * 畫面BLOCK
		 */
		function logoutblockUI(timeout){
			$("#logout-block").show();

			// 遮罩後不給捲動
			document.body.style.overflow = "hidden";

			var defaultTimeout = 60000;
			if(timeout){
				defaultTimeout = timeout;
			}
		}

		/**
		 * 畫面UNBLOCK
		 */
		function unLogoutBlockUI(timeoutID){
			if(timeoutID){
				clearTimeout(timeoutID);
			}
			$("#logout-block").hide();

			// 解遮罩後給捲動
			document.body.style.overflow = 'auto';
		}
		/**
		 *繼續使用
		 */
		function keepLogin(){
			unLogoutBlockUI(); // 解遮罩
			timeLogout(); // 刷新倒數計時
		}
    </script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上開立數位存款帳戶     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1361" /></li>
		</ol>
	</nav>

	<div class="content row">
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
<!-- 					線上開立數位存款帳戶 -->
					<spring:message code="LB.D1361"/>
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <div id="step-bar">
                    <ul>
                        <li class="finished">注意事項與權益</li>
                        <li class="finished">開戶認證</li>
                        <li class="active">開戶資料</li>
                        <li class="">確認資料</li>
                        <li class="">完成申請</li>
                    </ul>
                </div>
                <form id="formId" method="post">
					<input type="hidden" name="ADOPID" id="ADOPID" value="N203">
					<input type="hidden" name="FGTXWAY" id="FGTXWAY" value="${result_data.data.FGTXWAY}">	
					<input type="hidden" name="BRHCOD" id="BRHCOD" value="">
					<input type="hidden" name="BRHNAME" id="BRHNAME" value="">
					<input type="hidden" name="ISBRANCH" id="ISBRANCH" value="">	
					<input type="hidden" name="BRHADDRESS" id="BRHADDRESS" value="">	
					<input type="hidden" name="ISCUSIDN" id="ISCUSIDN" value="">
					<input type="hidden" name="BDEALING_str" id="BDEALING_str" value="">
					<input type="hidden" name="TODAY" id="TODAY" value="${ today }">
					<input type="hidden" name="CUSIDN" value="${result_data.data.CUSIDN}">
					<input type="hidden" name="back" id="back" value=>
					<!-- timeout -->
					<section id="id-and-fast">
						<span class="id-name">
							<spring:message code="LB.Welcome_1"/>&nbsp;
							<c:if test="${empty result_data.data.CUSNAME}">
								<span id="username" name="username_show"><spring:message code="LB.X2190"/></span>
							</c:if>
							<c:if test="${not empty result_data.data.CUSNAME}">
								<span id="username" name="username_show">${result_data.data.CUSNAME}</span>
							</c:if>
							&nbsp;<spring:message code="LB.Welcome_2"/>
						</span>
						<div id="id-block">
							<div>
							<span class="id-time">
								<fmt:setLocale value="${pageContext.response.locale}"/>
								<c:if test="${!empty sessionScope.logindt && !empty sessionScope.logintm}">
									<fmt:parseDate var="parseDate"
												   value="${sessionScope.logindt} ${sessionScope.logintm}"
												   pattern="yyyy-MM-dd HH:mm:ss"/>
									<fmt:formatDate value="${parseDate}" dateStyle="full"
													type="both"/>&nbsp;<spring:message code="LB.X2250"/>
									<br/>
								</c:if>

								<fmt:formatDate value="${sessionScope.login_time}" type="both" dateStyle="full"/>
								&nbsp;<spring:message code="LB.Login_successful"/>
							</span>
								<!-- 自動登出剩餘時間 -->
								<span class="id-time"><spring:message code="LB.X1912"/>
								<span class="high-light ml-1">
									<!-- 分 -->
									<font id="countdownMin"></font>
									:
									<!-- 秒 -->
									<font id="countdownSec"></font>
								</span>
							</span>
							</div>
							<button type="button" class="btn-flat-orange" onclick="timeLogout()"><spring:message
									code="LB.X1913"/></button>
							<button type="button" class="btn-flat-darkgray"
									onClick="fstop.logout( '${__ctx}'+'/logout_aj' , '${__ctx}'+'/login')"><spring:message
									code="LB.Logout"/></button>
						</div>
					</section>
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
                        
							<div class="ttb-input-block">
                            	<div class="ttb-message">
								    <p>開戶資料 (2/4)</p>
								</div>
								
	                            <div class="classification-block">
	                                <p>開戶資訊</p>
	                                <p>(  <span class="high-light">*</span> 為必填)</p>
	                            </div>
	                            
								<!--★本次開戶目的-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
<!-- 									本次開戶目的 -->
											<h4>
												<spring:message code="LB.D1075"/> <span class="high-light">*</span>
											</h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<select name="PURPOSE" id="PURPOSE" class="custom-select validate[required]">
												<!-- 儲蓄 -->
												<option id="P1" value="A">
													<spring:message code="LB.D1076"/>
												</option>
												<!-- 薪轉 -->
												<option id="P2" value="B">
													<spring:message code="LB.D1077"/>
												</option>
												<!-- 資金調撥 -->
												<option id="P3" value="C">
													<spring:message code="LB.D1078"/>
												</option>
												<!-- 投資 -->
												<option id="P4" value="D">
													<spring:message code="LB.D1079"/>
												</option>
												<!-- 其他 -->
												<option id="P5" value="E">
													<spring:message code="LB.D0572"/>
												</option>
											</select>
										</div>
										<div class="ttb-input tbb-toggle-input" style="display: none;" toggle-input="P5">
											<input type="text" class="text-input validate[condRequired[P5]]" placeholder="請填入" name="PREASON" id="PREASON" size="11" maxlength="10" value="${result_data.data.PREASON}">
										</div>
									</span>
								</div>
								<!-- ********** -->
								<!--★與本行已往來業務-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
<!-- 									與本行已往來業務 -->
											<h4>
												<spring:message code="LB.D1391"/> <span class="high-light">*</span>
											</h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<label class="check-block" for="BD6">
												<input type="checkbox" name="BDEALING6" id="BD6" value="F" onclick="changebedealing('6');">
<!-- 												無 -->
												<spring:message code="LB.D1070_2"/>
                                            	<span class="ttb-check"></span>
											</label>
	
											<label class="check-block" for="BD1">
												<input type="checkbox" name="BDEALING1" id="BD1" value="A"  onclick="changebedealing('1');"/>
<!-- 												存款 -->
												<spring:message code="LB.D1393"/>
                                            	<span class="ttb-check"></span>
											</label>
											<label class="check-block" for="BD2">
												<input type="checkbox" name="BDEALING2" id="BD2" value="B" onclick="changebedealing('2');"/>
<!-- 												授信 -->
												<spring:message code="LB.D1394"/>
                                            	<span class="ttb-check"></span>
											</label>
											<label class="check-block" for="BD3">
												<input type="checkbox" name="BDEALING3" id="BD3" value="C" onclick="changebedealing('3');"/>
<!-- 												現金卡 -->
												<spring:message code="LB.D1395"/>
                                            	<span class="ttb-check"></span>
											</label>
											<label class="check-block" for="BD4">
												<input type="checkbox" name="BDEALING4" id="BD4" value="D" onclick="changebedealing('4');"/>
<!-- 												壽險 -->
												<spring:message code="LB.D1396"/>
                                         		<span class="ttb-check"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="check-block" for="BD5">
												<input type="checkbox" name="BDEALING5" id="BD5" value="E" onclick="changebedealing('5');"/>
<!-- 												其他 -->
												<spring:message code="LB.D0572"/>
                                            	<span class="ttb-check"></span>
											</label>
											<input type="text" class="text-input" placeholder="請填入" name="BREASON" id="BREASON" size="11" maxlength="10" value="${result_data.data.BREASON}">
										</div>
										<span class="hideblock">
											<input type="text" class="validate[required]" name="BDEALING" id="BDEALING" value=""
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</span>
									</span>
								</div>
								<!-- ********** -->
								<!--★主要資金/財產來源-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
<!-- 									主要資金/財產來源 -->
											<h4><spring:message code="LB.D1397"/> <span class="high-light">*</span></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<select name="MAINFUND" id="MAINFUND" class="custom-select validate[required]">
												<!-- 薪資 -->
												<option id="M1" value="A">
													<spring:message code="LB.D1398"/>
												</option>
												<!-- 儲蓄 -->
												<option id="M2" value="B">
													<spring:message code="LB.D1076"/>
												</option>
												<!-- 營運 -->
												<option id="M3" value="C">
													<spring:message code="LB.D1399"/>
												</option>
												<!-- 投資 -->
												<option id="M4" value="D">
													<spring:message code="LB.D1079"/>
												</option>
												<!-- 銷售 -->
												<option id="M5" value="E">
													<spring:message code="LB.D1400"/>
												</option>
												<!-- 家業繼承 -->
												<option id="M6" value="F">
													<spring:message code="LB.D1401"/>
												</option>
												<!-- 退休金 -->
												<option id="M7" value="G">
													<spring:message code="LB.D0910"/>
												</option>
												<!-- 其他 -->
												<option id="M8" value="H">
													<spring:message code="LB.D0572"/>
												</option>
											</select>
										</div>
										<div class="ttb-input tbb-toggle-input" style="display: none;" toggle-input="M8">
											<!-- 其他 -->
											<input type="text" class="text-input validate[condRequired[M8]]" placeholder="請填入" name="MREASON" id="MREASON" value="${result_data.data.MREASON}">
										</div>
									</span>
								</div>
								<!-- ********** -->
								<!-- 個人/家庭年收入 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.X0170"/><spring:message code="LB.D0625_1"/> <span class="high-light">*</span>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="tel" inputmode=“numeric id="SALARY" name="SALARY" placeholder="請輸入個人/家庭年收入" class="text-input" value="${result_data.data.SALARY.length() == 0 ? '0':result_data.data.SALARY}" maxLength="6" size="8"/>
											<span class="input-unit"><spring:message code="LB.D1144"/></span>
											<span class="hideblock">
												<input id="validate_SALARY" name="validate_SALARY" type="text"  
													class="validate[required,funcCall[validate_CheckNumWithDigit2[ '個人/家庭年收入' , validate_SALARY, false]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
											</span>
										</div>
									</span>
								</div>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 										預期往來金額(近一年) -->
											<spring:message code="LB.D1081_1"/> <span class="high-light">*</span>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="S_MONEY" name="S_MONEY" placeholder="請輸入預期往來金額(近一年)" class="text-input" value="${result_data.data.S_MONEY}" maxLength="7" size="8"/>
											<span class="input-unit"><spring:message code="LB.D0088_2"/></span>
												<input id="validate_S_MONEY" name="validate_S_MONEY" type="text"  
													class="validate[required,funcCall[validate_CheckNumWithDigit2[ '個人/家庭年收入' , validate_S_MONEY, false]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
										</div>
									</span>
								</div>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 										★指定服務分行 -->
											<spring:message code="LB.D1472"/> <span class="high-light">*</span>
										</h4>
									</label>
									</span> 
									<span class="input-block">
<!-- 										<div class="ttb-input"> -->
<!-- 											<select name="CITY3" id="CITY3" class="custom-select select-input input-width-125 validate[required]" onchange="doquery5('#CITY3','#ZONE3')"> -->
<%-- 								          		<option value="#">--- <spring:message code="LB.Select"/><spring:message code="LB.D0059"/> ---</option>   --%>
<%-- 								          		<c:forEach var="dataList" items="${city_data}"> --%>
<%-- 													<option>${dataList.CITY}</option> --%>
<%-- 												</c:forEach> --%>
<!-- 											</select> -->
<%-- 											<spring:message code="LB.D1118"/> --%>
<!-- 											<select name="ZONE3" id="ZONE3" class="custom-select select-input input-width-100 validate[required]"  onchange="doquery6('#CITY3','#ZONE3','#ZIP3')"> -->
<%-- 								          		<option value="#">--- <spring:message code="LB.Select"/><spring:message code="LB.D0060"/> ---</option>  --%>
<!-- 											</select> -->
<!-- 											<input type="text" name="ZIP3" id="ZIP3" value="" class="zipcode" size=4 maxlength=3> -->
<%-- 											<spring:message code="LB.D1490"/> --%>
<!-- 										</div> -->
										<div class="ttb-input" id="twzipcode">
											<span data-role="county" data-style="custom-select select-input input-width-100 validate[required]"></span>
											<span data-role="district" data-style="custom-select select-input input-width-100 validate[required]" style="display:none"></span>
											<span data-role="zipcode" data-name="ZIP3" data-style="zipcode"></span>
<!-- 										</div> -->
<!-- 										<div class="ttb-input"> -->
											<select name="BHID" id="BHID" class="custom-select input-width-125 select-input validate[funcCall[validate_CheckSelect[<spring:message code='LB.D1472'/>,BHID,#]]]" value="${result_data.data.BHID}">
								          		<option value="#"><spring:message code="LB.Select"/><spring:message code="LB.X0169"/></option>  
								          		<c:forEach var="dataList" items="${bh_data}">
													<option value="${dataList.ADBRANCHID}"> ${dataList.ADBRANCHNAME}</option>
												</c:forEach>
											</select>
										</div>
										<div id="addressmap" style="display: none; margin-top: 0.625rem;">
											<span id="address"></span>
											<a id="mapicon" href="javascript:void(0);" target="_blank"><img src="${__ctx}/img/map.svg" style="width: 16px;height: 14px;margin-left: 5px;" /></a>
										</div>
									</span>
								</div>
								<!-- ********** -->
								<!-- 推薦碼 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											推薦碼
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="EMPNO" name="EMPNO" placeholder="請填入6位數字" class="text-input validate[funcCall[validate_CheckLenEqual[推薦碼,EMPNO,true,6]],
											funcCall[validate_CheckNumWithDigit2['推薦碼',EMPNO,true]]]" value="${result_data.data.EMPNO}" maxLength="6" size="8"/>
										</div>
									</span>
								</div>
	                            
	                            
	                            
							</div>
	                        <!--button 區域 -->
							<!-- 重新輸入 -->
							<input type="button" name="CMBACK" id="CMBACK" value="<spring:message code="LB.X0318"/>" class="ttb-button btn-flat-gray">
                            <input id="CMSUBMIT" name="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.X0080"/>" />
                        <!--                     button 區域 -->
                        </div>  
                    </div>
                </form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>
