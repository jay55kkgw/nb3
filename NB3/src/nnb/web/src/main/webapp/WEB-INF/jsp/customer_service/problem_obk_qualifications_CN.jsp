<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="page1" class="card-detail-block">
	<div class="card-center-block">
		<!-- Q1 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block" data-toggle="collapse" href="#popup1-11" aria-expanded="true" aria-controls="popup1-11">
				<div class="row">
					<span class="col-1">Q1</span>
					<div class="col-11">
						<span>网络银行的申请条件？如何申请？</span>
					</div>
				</div>
			</a>
			<div id="popup1-11" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div class="ttb-result-list terms">
								<ol>
								
									<li>一、临柜申请：
										<ol>
											<li>　如果您已是本行存款客户，请本人亲携身分证件、存款印鉴及存折，赴开户行填写申请书，并取得网络银行密码单即可登入本行网站(https://ebank.tbb.com.tw)进行交易。如为全行收付个人户（即存折首页盖有存款印鉴），亦可携身份证件、存款印鉴及存折至国内各分行办理。</li>
										</ol>
									</li>
									<li>二、在线申请：
										<ol>
											<li>1、 本行芯片金融卡存户：限个人存户首次申请。
												未曾临柜申请之存户，可在线申请查询服务，请于网络银行网站使用『 本行芯片金融卡 + 芯片卡片阅读机』在线申请网络银行，，如该芯片金融卡已具备非约定转账者，得在线申请「金融卡在线申请/取消交易服务功能」，执行新台币非约定转账交易、缴费税交易、在线更改通讯地址/电话及用户名称/签入密码/交易密码在线解锁等功能。
											</li>
											<li>2、 本行信用卡正卡客户（不含商务卡、采购卡、附卡、VISA金融卡）：
												您可于网络银行网站以信用卡在线申请网络银行，输入信用卡个人验证数据，使用网络银行信用卡相关服务。
												注：在线申请者无法执行「转账」、「申购基金」等功能，如欲增加前述功能，请亲洽往来分行办理升级为临柜申请用户，即可使用完整的理财服务。
											</li>
										</ol>
									</li>
								</ol>
							</div>
						</li>
						<li>
							<p>三、如果您尚未在本行开户，请本人携带身份证件及印章至本行国内各分行办理开户，同时申请网络银行。</p>
						</li>
						<li>
							<p>四、服务功能：</p>
							<div style="overflow: auto">
								<table class="question-table" style="width: 100%">
									<thead>
										<tr>
											<th rowspan="2">功能/申请方式</th>
											<th rowspan="2">临柜申请</th>
											<th colspan="3">在线申请</th>
										</tr>
										<tr>
											<th>芯片金融卡</th>
											<th>芯片金融卡+在线交易功能<br>(需开通非约转交易)</th>
											<th>信用卡</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th colspan="5" style="text-align: left;font-size: 16px;">台币服务</th>
										</tr>
										<tr>
											<th style="text-align: left;">约定/非约定转账 (注1)</th>
											<td>O</td>
											<td>X</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<th style="text-align: left;">定存</th>
											<td>O</td>
											<td>X</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<th style="text-align: left;">汇入汇款通知设定</th>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<th colspan="5" style="text-align: left;font-size: 16px;">外币服务</th>
										</tr>
										<tr>
											<th style="text-align: left;">存款/放款/帐务查询</th>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<th colspan="5" style="text-align: left;font-size: 16px;">基金服务</th>
										</tr>
										<tr>
											<th style="text-align: left;">基金查询</th>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<th style="text-align: left;">基金申购及变更</th>
											<td>O</td>
											<td>X</td>
											<td>X</td>
											<td>X</td>
										</tr>
										<tr>
											<th style="text-align: left;">理财试算</th>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>O</td>
										</tr>
										<tr>
											<th colspan="5" style="text-align: left;font-size: 16px;">信用卡服务</th>
										</tr>
										<tr>
											<th style="text-align: left;">信用卡帐务查询</th>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>O</td>
										</tr>
										<tr>
											<th style="text-align: left;">电子账单/补寄账单/Email缴款通知</th>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>O</td>
										</tr>
										<tr>
											<th style="text-align: left;">登录道路救援</th>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>O</td>
										</tr>
										<tr>
											<th colspan="5" style="text-align: left;font-size: 16px;">缴费服务</th>
										</tr>
										<tr>
											<th style="text-align: left;">缴费税</th>
											<td>O</td>
											<td>X</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<th style="text-align: left;">申请代扣缴费用</th>
											<td>O</td>
											<td>X</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<th colspan="5" style="text-align: left;font-size: 16px;">个人化服务</th>
										</tr>
										<tr>
											<th style="text-align: left;">变更通讯数据 (注2)</th>
											<td>O</td>
											<td>X</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<th style="text-align: left;">在线解锁使用者名称/签入/交易密码 (注3)</th>
											<td>O</td>
											<td>X</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<th style="text-align: left;">挂失服务</th>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>X</td>
										</tr>
									</tbody>
								</table>
							</div>
							<p>备注</p>
							<ol class="list-decimal">
								<li>倘已在线申请交易功能者，且该芯片金融卡已具备非约定转账功能者，并得以本人帐户之芯片金融卡之主账号为转出账户，执行新台币非约定转账交易、缴费税交易。</li>
								<li>交易机制为「电子签章」或「芯片金融卡」，即可执行在线更改通讯地址/电话。</li>
								<li>交易机制为「电子签章」或「芯片金融卡」，即可执行在线签入/交易密码在线解锁。</li>
								<li>O：表示可使用功能，X：表示不可使用功能。</li>
							</ol>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q2 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block" data-toggle="collapse" href="#popup1-12" aria-expanded="true" aria-controls="popup1-12">
				<div class="row">
					<span class="col-1">Q2</span>
					<div class="col-11">
						<span>如果我在高雄分行及台北分行各有一个账户，是否可以任选一个开户分行让两个帐户都申请转出功能，而不要跑两家分行办理?</span>
					</div>
				</div>
			</a>
			<div id="popup1-12" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>可以，您如果在台北分行申请网络银行服务，只需在台北分行填妥约定书，于转出账号字段填入台北分行及高雄分行账户即可，但请您要带高雄分行账户的存折（限全行收付户，存折首页盖有存款印鉴）及印鉴，减少您南北奔波的麻烦。如为全行收付个人户，亦可携身份证件、存款印鉴及存折至国内各分行办理。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q3 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block" data-toggle="collapse" href="#popup1-13" aria-expanded="true" aria-controls="popup1-13">
				<div class="row">
					<span class="col-1">Q3</span>
					<div class="col-11">
						<span>申请网络银行可不可以不要转账功能，如果以后再恢复可以吗？</span>
					</div>
				</div>
			</a>
			<div id="popup1-13" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>可以，转账功能可随您需要做选择，请您在临柜申请时「转出账号」字段不要填写往来账号即可，日后如果要申请转账功能请至原申请分行办理；如为全行收付个人户（即存折首页盖有存款印鉴），亦可携身份证件、存款印鉴及存折至国内各分行办理。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q4 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block" data-toggle="collapse" href="#popup1-14" aria-expanded="true" aria-controls="popup1-14">
				<div class="row">
					<span class="col-1">Q4</span>
					<div class="col-11">
						<span>网络银行有哪些交易机制？</span>
					</div>
				</div>
			</a>
			<div id="popup1-14" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>网络银行交易机制区分电子签章、交易密码(SSL)、芯片金融卡及装置推播认证四种</p>
						</li>
						<li>
							<ol>
								<li>
									<p>1. 电子签章(采金融XML凭证)</p>
									<p>临柜申办时，请填写「同意」申请金融XML凭证，取得「凭证识别资料（CN）」及凭证载具(i-Key)，完成申请手续后，请登入网路银行的「凭证注册中心」申请及下载凭证。凭证载具每支NT$600元；电子凭证年费采线上扣款，个人户NT$150元，法人户NT$900元，有效期间一年。</p>
								</li>
								<li>
									<p>2. 交易密码(SSL)</p>
									<p>未申请金融XML凭证者，仅可使用交易密码交易（如查询、已约定帐户转帐…等）。</p>
								</li>
								<li>
									<p>3. 芯片金融卡(本行芯片金融卡+卡片阅读机)</p>
									<p>该芯片金融卡已具备非约定转账功能者，得执行新台币非约定交易服务功能。</p>
								</li>
								<li>
									<p>4. 装置推播认证</p>
									<p>藉由透过绑定用户的行动装置来双重确认用户之低风险交易，以提升网路银行交易安控之安全性及便利性，完成绑定后方能使用行动银行非约定转帐服务、缴费服务、Email变更及地址与电话变更之功能项目。</p>
								</li>
							</ol>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q5 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block" data-toggle="collapse" href="#popup1-15" aria-expanded="true" aria-controls="popup1-15">
				<div class="row">
					<span class="col-1">Q5</span>
					<div class="col-11">
						<span>电子签章、交易密码、芯片金融卡及装置推播认证交易有何不同？</span>
					</div>
				</div>
			</a>
			<div id="popup1-15" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>网络银行所有交易皆采用SSL 256bit加密保障传输安全，交易密码交易可使用在已约定转账，而未约定转账需使用安全性更高的电子签章、芯片金融卡及装置推播认证。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
