<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ include file="../__import_js.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br/>
	<br/>
	<div style="text-align: center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/>
	</div>
	<br/>
	<br/>
	<div style="text-align: center">
		<font style="font-weight: bold;font-size: 1.2em">${jspTitle}</font>
	</div>
	<br/>
	<table class="print">
		<tr>
<!-- 系統時間 -->
			<th><b><spring:message code= "LB.D0121" /> : ${CMQTIME}</b></th>
		</tr>
		<tr>
<!-- 貸款資料 -->
			<td style="text-align: center"><b><spring:message code= "LB.D0558" /></b></td>
		</tr>
		<tr>
<!-- 貸款種類 -->
			<td style="text-align: left"><spring:message code= "LB.D0544" />：<spring:message code= "${APPKINDNAME}" /></td>
		</tr>
		<tr>
<!-- 申請金額：新臺幣 -->
<!-- 萬元整 -->
			<td style="text-align: left"><spring:message code= "LB.W0803" />>&nbsp;${bs.data.APPAMT}&nbsp;<spring:message code= "LB.D0563" /></td>
		</tr>
		<tr>
<!-- 資金用途 -->
			<td style="text-align: left"><spring:message code= "LB.D0564" />：<spring:message code= "${PURPOSENAME}" /></td>
		</tr>
		<tr>
<!-- 申請還款期數 -->
<!-- 個月 -->
			<td style="text-align: left"><spring:message code= "LB.D0573" />：${PERIODS}<spring:message code= "LB.D0574" /></td>
		</tr>
		<tr>
<!-- 指定對保分行 -->
			<td style="text-align: left"><spring:message code= "LB.D0575" />：${BANKNA}</td>
		</tr>
		<tr>
<!-- 個人資料 -->
			<td style="text-align: center"><b><spring:message code= "LB.D0576" /></b></td>
		</tr>
		<tr>
<!-- 姓名 -->
			<td style="text-align: left"><spring:message code= "LB.D0203" />：${APPNAME}</td>
		</tr>
		<tr>
<!-- 性別 -->
			<td style="text-align: left"><spring:message code= "LB.D0578" />：<spring:message code= "${SEXNAME}" /></td>
		</tr>
		<tr>
<!-- 身分證字號 -->
			<td style="text-align: left"><spring:message code= "LB.D0581" />：${CUSIDN}</td>
		</tr>
		<tr>
<!-- 出生日期 -->
			<td style="text-align: left"><spring:message code= "LB.D0582" />：${BRTHDT}</td>
		</tr>
		<tr>
<!-- 教育程度 -->
			<td style="text-align: left"><spring:message code= "LB.D0055" />：<spring:message code= "${EDUSNAME}" /></td>
		</tr>
		<tr>
<!-- 婚姻狀況 -->
			<td style="text-align: left"><spring:message code= "LB.D0057" />：<spring:message code= "${MARITALNA}" /></td>
		</tr>
		<tr>
<!-- 子女數 -->
<!-- 人 -->
			<td style="text-align: left"><spring:message code= "LB.D0602_1" />：${CHILDNO}&nbsp;&nbsp;<spring:message code= "LB.D0602_2" /></td>
		</tr>
		<tr>
<!-- 行動電話 -->
			<td style="text-align: left"><spring:message code= "LB.D0069" />：${CPHONE}</td>
		</tr>
		<tr>
<!-- 通訊電話 -->
<!-- 分機 -->
			<td style="text-align: left"><spring:message code= "LB.D0599" />：（${PHONE_01}）${PHONE_02}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<spring:message code= "LB.D0095" />：${PHONE_03}</td>
		</tr>
		<tr>
<!-- 戶籍電話 -->
			<td style="text-align: left"><spring:message code= "LB.D0144" />：（${PHONE_11}）${PHONE_12}</td>
		</tr>
		<tr>
			<td style="text-align: left">
<!-- 住宅狀況 -->
				<spring:message code= "LB.D0603" />：<spring:message code= "${HOUSENAME}" /><br/>
				<c:if test="${MONEY11 != ''}">
<!-- 房貸月繳 -->
<!-- 元 -->
					<spring:message code= "LB.D0609_1" />：${MONEY11}<spring:message code= "LB.Dollar" />
				</c:if>
				<c:if test="${MONEY12 != ''}">
<!-- 房租月繳 -->
<!-- 仟元 -->
					<spring:message code= "LB.D0610_1" />：${MONEY12}<spring:message code= "LB.D0610_2" />
				</c:if>
			</td>
		</tr>
		<tr>
			<td style="text-align: left">
<!-- 現住地址 -->
				<spring:message code= "LB.D0611" />：${ZIP2}${CITY2}${ADDR2}
			</td>
		</tr>
		<tr>
			<td style="text-align: left">
<!-- 戶籍地址 -->
				<spring:message code= "LB.D0143" />：${ZIP1}${CITY1}${ADDR1}
			</td>
		</tr>
		<tr>
			<td  style="text-align: left">E-MAIL：${EMAIL}</td>
		</tr>
		<tr>
		<!-- ※配偶及二親等以內血親：為遵循銀行法第33條之3所定授信限額之規定而取得「同一關係人」之基本資料包括祖（外祖）父母、父母、兄弟姊妹、子女、孫（外孫）子女 -->
			<td style="text-align: left"><b>※<spring:message code= "LB.D0616" />：<spring:message code= "LB.D0617" /></b>
			</td>
		</tr>
		<tr>
<!-- 稱謂 -->
<!-- 姓名 -->
			<td style="text-align: left"><spring:message code= "LB.D0618" />：${RELTYPE1} <spring:message code= "LB.D0203" />：${RELNAME1}</td>
		</tr>
		<tr>
<!-- 身分證字號 -->
			<td style="text-align: left"><spring:message code= "LB.D0581" />：${RELID1}</td>
		</tr>
		<tr>
<!-- 稱謂 -->
<!-- 姓名 -->
			<td style="text-align: left"><spring:message code= "LB.D0618" />：${RELTYPE2} <spring:message code= "LB.D0203" />：${RELNAME2}</td>
		</tr>
		<tr>
<!-- 身分證字號 -->
			<td style="text-align: left"><spring:message code= "LB.D0581" />：${RELID2}</td>
		</tr>
		<tr>
<!-- 稱謂 -->
<!-- 姓名 -->
			<td style="text-align: left"><spring:message code= "LB.D0618" />：${RELTYPE3} <spring:message code= "LB.D0203" />：${RELNAME3}</td>
		</tr>
		<tr>
<!-- 身分證字號 -->
			<td style="text-align: left"><spring:message code= "LB.D0581" />：${RELID3}</td>
		</tr>
		<tr>
<!-- 稱謂 -->
<!-- 姓名 -->
			<td style="text-align: left"><spring:message code= "LB.D0618" />：${RELTYPE4} <spring:message code= "LB.D0203" />：${RELNAME4}</td>
		</tr>
		<tr>
<!-- 身分證字號 -->
			<td style="text-align: left"><spring:message code= "LB.D0581" />：${RELID4}</td>
		</tr>
		<tr>
<!-- 職 業 資 料 -->
			<td style="text-align: center"><b><spring:message code= "LB.W0831" /></b></td>
		</tr>
		<tr>
<!-- 本人擔任負責人之其他企業名稱 -->
			<td style="text-align: left"><spring:message code= "LB.D0620" />：${OTHCONM1}</td>
		</tr>
		<tr>
<!-- 統一編號 -->
			<td style="text-align: left"><spring:message code= "LB.D0621" />：${OTHCOID1}</td>
		</tr>
		<tr>
<!-- 配偶擔任負責人之企業名稱 -->
			<td style="text-align: left"><spring:message code= "LB.D0622" />：${MOTHCONM1}</td>
		</tr>
		<tr>
<!-- 統一編號 -->
			<td style="text-align: left"><spring:message code= "LB.D0621" />：${MOTHCOID1}</td>
		</tr>
		<tr>
<!-- 本人服務單位 -->
			<td style="text-align: center"><b><spring:message code= "LB.D0623" /></b></td>
		</tr>
		<tr>
<!-- 公司名稱 -->
			<td style="text-align: left"><spring:message code= "LB.D0086" />：${CAREERNAME}</td>
		</tr>
		<tr>
<!-- 統一編號 -->
			<td style="text-align: left"><spring:message code= "LB.D0621" />：${CAREERIDNO}</td>
		</tr>
		<tr>
<!-- 年收入 -->
<!-- 萬元 -->
			<td style="text-align: left"><spring:message code= "LB.D0625_1" />：${INCOME}<spring:message code= "LB.D0088_2" /></td>
		</tr>
		<tr>
<!-- 公司地址 -->
			<td style="text-align: left"><spring:message code= "LB.D0090" />：${ZIP4}${CITY4}${ADDR4}</td>
		</tr>
		<tr>
<!-- 公司電話 -->
<!-- 分機 -->
<!-- 職稱 -->
			<td style="text-align: left"><spring:message code= "LB.D0094" />：（${PHONE_41}）${PHONE_42}&nbsp;&nbsp;&nbsp;<spring:message code= "LB.D0095" />：${PHONE_43}</td>
		</tr>
		<tr>
			<td><spring:message code= "LB.D0087" />：${PROFNONAME}</td>
		</tr>
		<tr>
<!-- 到職日：民國 -->
<!-- 年 -->
<!-- 月 -->
			<td style="text-align: left"><spring:message code= "LB.W0842" />&nbsp;${OFFICEY}&nbsp;<spring:message code= "LB.D0089_2" />&nbsp;${OFFICEM}&nbsp;<spring:message code= "LB.D0089_3" /></td>
		</tr>
		<tr>
<!-- 前職公司名稱 -->
			<td style="text-align: left"><spring:message code= "LB.D0630" />：${PRECARNAME}</td>
		</tr>
		<tr>
<!-- 前職工作年資 -->
			<td><spring:message code= "LB.D0631" />： <c:if test="${PRETKY != ''}">
<!-- 年 -->
				${PRETKY}<spring:message code= "LB.D0089_2" />
			</c:if> <c:if test="${PRETKM != ''}">
<!-- 個月 -->
				${PRETKM}<spring:message code= "LB.D0574" />
			</c:if>
			</td>
		</tr>
	</table>
	<br/>
	<br/>
</body>
</html>