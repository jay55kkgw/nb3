<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	
	<script type="text/javascript">
		$(document).ready(function(){
			// 列印
			$("#printbtn").click(function(){
				if('${transfer_result_data.data.FGTXDATE}' == '1'){
					if('${transfer_result_data.data.TAXTYPE1}' == '15'){
						var params = {
							"jspTemplateName":"pay_taxes_15_print",
							"jspTitle":"<spring:message code= "LB.W0367" />",
							"CMTXTIME":"${transfer_result_data.data.CMTXTIME }",
							"OUTACN":"${transfer_result_data.data.OUTACN }",
							"TaxTypeText":"${transfer_result_data.data.TaxTypeText }",
							"INTSACN1":"${transfer_result_data.data.INTSACN1 }",
							"INTSACN2":"${transfer_result_data.data.INTSACN2 }",
							"AMOUNT":"${transfer_result_data.data.AMOUNT }",
							"CMTRMEMO":"${transfer_result_data.data.CMTRMEMO }",
							"O_TOTBAL":"${transfer_result_data.data.O_TOTBAL }",
							"O_AVLBAL":"${transfer_result_data.data.O_AVLBAL }",
							"RESULT":"${transfer_result_data.result }"
						};
						openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);

					} else if('${transfer_result_data.data.TAXTYPE1}' == '11'){
						var params = {
							"jspTemplateName":"pay_taxes_11_print",
							"jspTitle":"<spring:message code= "LB.W0367" />",
							"CMTXTIME":"${transfer_result_data.data.CMTXTIME }",
							"OUTACN":"${transfer_result_data.data.OUTACN }",
							"TaxTypeText":"${transfer_result_data.data.TaxTypeText }",
							"INTSACN3":"${transfer_result_data.data.INTSACN3 }",
							"PAYDUED":"${transfer_result_data.data.PAYDUED }",
							"AMOUNT":"${transfer_result_data.data.AMOUNT }",
							"CMTRMEMO":"${transfer_result_data.data.CMTRMEMO }",
							"O_TOTBAL":"${transfer_result_data.data.O_TOTBAL }",
							"O_AVLBAL":"${transfer_result_data.data.O_AVLBAL }",
							"RESULT":"${transfer_result_data.result }"
						};
						openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);

					}
						
				} else if('${transfer_result_data.data.FGTXDATE}' == '2'){
					if('${transfer_result_data.data.TAXTYPE1}' == '15'){
						var params = {
							"jspTemplateName":"pay_taxes_s_15_print",
							"jspTitle":"<spring:message code= "LB.W0367" />",
							"CMTXTIME":"${transfer_result_data.data.CMTXTIME }",
							"CMDATE":"${transfer_result_data.data.CMDATE }",
							"OUTACN":"${transfer_result_data.data.OUTACN }",
							"TaxTypeText":"${transfer_result_data.data.TaxTypeText }",
							"INTSACN1":"${transfer_result_data.data.INTSACN1 }",
							"INTSACN2":"${transfer_result_data.data.INTSACN2 }",
							"AMOUNT":"${transfer_result_data.data.AMOUNT }",
							"CMTRMEMO":"${transfer_result_data.data.CMTRMEMO }",
							"RESULT":"${transfer_result_data.result }"
						};
						openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);

					} else if('${transfer_result_data.data.TAXTYPE1}' == '11'){
						var params = {
							"jspTemplateName":"pay_taxes_s_11_print",
							"jspTitle":"<spring:message code= "LB.W0367" />",
							"CMTXTIME":"${transfer_result_data.data.CMTXTIME }",
							"CMDATE":"${transfer_result_data.data.CMDATE }",
							"OUTACN":"${transfer_result_data.data.OUTACN }",
							"TaxTypeText":"${transfer_result_data.data.TaxTypeText }",
							"INTSACN3":"${transfer_result_data.data.INTSACN3 }",
							"PAYDUED":"${transfer_result_data.data.PAYDUED }",
							"AMOUNT":"${transfer_result_data.data.AMOUNT }",
							"CMTRMEMO":"${transfer_result_data.data.CMTRMEMO }",
							"RESULT":"${transfer_result_data.result }"
						};
						openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);

					}
					
				}
			});

			// 繳納證明
			$("#payproof").click(function(){
				var CMTXTIME = "${transfer_result_data.data.CMTXTIME}";
				var yyymmdd = CMTXTIME.split(" ")[0];
				var yyy = yyymmdd.split("/")[0];
				var yyyy = parseInt(yyy) + 1911;
				var yyyymmdd = yyyy + "/" + yyymmdd.split("/")[1] + "/" + yyymmdd.split("/")[2];

				var TaxTypeText = "${transfer_result_data.data.TaxTypeText }";
				var str_TaxTypeText = trimNumber(TaxTypeText);
				
				if('${transfer_result_data.data.TAXTYPE1}' == '15'){
					var params = {
						"jspTemplateName":"pay_taxes_15_proof",
						"jspTitle":"<spring:message code= "LB.X1424" />",
						"yyyymmdd":yyyymmdd,
						"CMTXTIME":yyyymmdd,
						"OUTACN":"${transfer_result_data.data.OUTACN }",
						"TaxTypeText":str_TaxTypeText,
						"INTSACN1":"${transfer_result_data.data.INTSACN1 }",
						"INTSACN2":"${transfer_result_data.data.INTSACN2 }",
						"AMOUNT":"${transfer_result_data.data.AMOUNT }",
						"CMTRMEMO":"${transfer_result_data.data.CMTRMEMO}",
						"CMTRMAIL":"${transfer_result_data.data.CMTRMAIL}"
					};
					openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);

				} else if('${transfer_result_data.data.TAXTYPE1}' == '11'){
					var params = {
						"jspTemplateName":"pay_taxes_11_proof",
						"jspTitle":"<spring:message code= "LB.X1424" />",
						"yyyymmdd":yyyymmdd,
						"CMTXTIME":yyyymmdd,
						"OUTACN":"${transfer_result_data.data.OUTACN }",
						"TaxTypeText":str_TaxTypeText,
						"INTSACN3":"${transfer_result_data.data.INTSACN3 }",
						"PAYDUED":"${transfer_result_data.data.PAYDUED }",
						"AMOUNT":"${transfer_result_data.data.AMOUNT }",
						"CMTRMEMO":"${transfer_result_data.data.CMTRMEMO}",
						"CMTRMAIL":"${transfer_result_data.data.CMTRMAIL}"
					};
					openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);

				}
				
			});

		});

		// 去掉字串中數字
		function trimNumber(str){
			return str.replace(/\d+/g,'');
		}
	</script>
</head>
<body>
    <!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	
	<c:if test="${transfer_result_data.result == true}">
	<!--   IDGATE --> 		 
	<%@ include file="../idgate_tran/idgateAlert.jsp" %> 
	</c:if>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 繳稅     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0367" /></li>
		</ol>
	</nav>

	
	<!-- 功能清單及登入資訊 -->
	<div class="content row">

		<!-- 功能清單 -->
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
			
				<!-- 功能名稱 -->
	            <h2><spring:message code="LB.W0367" /></h2>
	            <i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
	            
				<!-- 交易流程階段 -->
	            <div id="step-bar">
	                <ul>
	                    <li class="finished"><spring:message code="LB.Enter_data" /></li>
	                    <li class="finished"><spring:message code="LB.Confirm_data" /></li>
	                    <li class="active"><spring:message code="LB.Transaction_complete" /></li>
	                </ul>
	            </div>
	            
	            <!-- 即時交易結果 -->
	            <c:if test="${transfer_result_data.data.FGTXDATE == '1'}">
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
						<c:if test="${transfer_result_data.result == true}">
							<h4 style="margin-top:10px;font-weight:bold;color:red">
								<spring:message code="LB.X2220" />
							</h4>
		                    	<div class="ttb-input-block">
								<!--交易時間區塊 -->
								<div class="ttb-input-item row">
								<!--交易時間  -->
								<span class="input-title">
									<label>
										<spring:message code="LB.Trading_time" /><!-- 交易時間 -->
									</label>
								</span>
								<span class="input-block">
									${transfer_result_data.data.CMTXTIME }
								</span>
								</div>
								<!--交易時間區塊END -->
		                       <!--轉出帳號區塊 -->
								<div class="ttb-input-item row">
								<!--轉出帳號  -->
								<span class="input-title">
									<label>
										<spring:message code="LB.Payers_account_no" />
									</label>
								</span>
								<span class="input-block">
									${transfer_result_data.data.OUTACN }
								</span>
								</div>
								<!--轉出帳號區塊END -->
								<!--繳稅類別區塊 -->
								<div class="ttb-input-item row">
								<!--繳稅類別  -->
								<span class="input-title">
									<label>
										<spring:message code="LB.X0469" />
									</label>
								</span>
								<span class="input-block">
									${transfer_result_data.data.TaxTypeText }
								</span>
								</div>
								<!--繳稅類別區塊END -->
		                 <c:choose>
	                        <c:when test="${transfer_result_data.data.TAXTYPE1 == '15'}">
		                        <!--稽徵單位代號區塊 -->
								<div class="ttb-input-item row">
								<!--稽徵單位代號  -->
								<span class="input-title">
									<label>
										<spring:message code="LB.W0410" />
									</label>
								</span>
								<span class="input-block">
									${transfer_result_data.data.INTSACN1 }
								</span>
								</div>
								<!--稽徵單位代號區塊END -->
								<!--身分證字號/統一編號區塊 -->
								<div class="ttb-input-item row">
								<!--身分證字號/統一編號  -->
								<span class="input-title">
									<label>
										<spring:message code="LB.Id_no" />
									</label>
								</span>
								<span class="input-block">
									${transfer_result_data.data.INTSACN2 }
								</span>
								</div>
								<!--身分證字號/統一編號區塊END -->
		                    </c:when>
		                    <c:when test="${transfer_result_data.data.TAXTYPE1 == '11'}">
		                        
		                        <!--銷帳編號區塊 -->
								<div class="ttb-input-item row">
								<!--銷帳編號  -->
								<span class="input-title">
									<label>
										<spring:message code="LB.W0407" />
									</label>
								</span>
								<span class="input-block">
									${transfer_result_data.data.INTSACN3 }
								</span>
								</div>
								<!--銷帳編號區塊END -->
								 <!--繳納截止日區塊 -->
								<div class="ttb-input-item row">
								<!--繳納截止日  -->
								<span class="input-title">
									<label>
										<spring:message code="LB.W0409" />
									</label>
								</span>
								<span class="input-block">
									${transfer_result_data.data.PAYDUED }
								</span>
								</div>
								<!--繳納截止日區塊END -->
								
		                    </c:when>
		                 </c:choose>
		                        
		                         <!--繳稅金額 區塊 -->
								<div class="ttb-input-item row">
								<!--繳稅金額   -->
								<span class="input-title">
									<label>
										<spring:message code="LB.W0415" /> 
									</label>
								</span>
								<span class="input-block">
									<spring:message code="LB.NTD" />
		                            	${transfer_result_data.data.AMOUNT }
		                            	<spring:message code="LB.Dollar" />
								</span>
								</div>
								<!--繳稅金額 區塊END -->
		                        <!--交易備註區塊 -->
								<div class="ttb-input-item row">
								<!--交易備註  -->
								<span class="input-title">
									<label>
										<spring:message code="LB.Transfer_note" /><!-- 交易備註 -->
									</label>
								</span>
								<span class="input-block">
									${transfer_result_data.data.CMTRMEMO }
								</span>
								</div>
								<!--交易備註區塊END -->
		                        <!-- 轉出帳號帳上餘額 -->
		                        <!--轉出帳號帳上餘額區塊 -->
								<div class="ttb-input-item row">
								<!--轉出帳號帳上餘額  -->
								<span class="input-title">
									<label>
										<spring:message code="LB.W0282" />
									</label>
								</span>
								<span class="input-block">
									<spring:message code="LB.NTD" />
		                            ${transfer_result_data.data.O_TOTBAL }
		                            <spring:message code="LB.Dollar" />
								</span>
								</div>
								<!--轉出帳號帳上餘額區塊END -->
								<!--轉出帳號可用餘額區塊 -->
								<div class="ttb-input-item row">
								<!--轉出帳號可用餘額  -->
								<span class="input-title">
									<label>
										<spring:message code="LB.W0283" />
									</label>
								</span>
								<span class="input-block">
									<spring:message code="LB.NTD" />
		                            ${transfer_result_data.data.O_AVLBAL }
		                            <spring:message code="LB.Dollar" />
								</span>
								</div>
								<!--轉出帳號可用餘額區塊END -->
							</div>
						</c:if>
						<!-- 繳納證明 -->
						<input type="button" class="ttb-button btn-flat-orange" id="payproof" value="<spring:message code="LB.X0477" />"/>
						<!-- 列印 ( 業務單位說只保留繳納證明 ) -->
<%-- 						<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/> --%>
						</div>
					</div>
					
					<!-- 說明 -->
					<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /></p>
						<li><spring:message code="LB.Pay_Taxes_P3_D4" /></li>
						<li><spring:message code="LB.Pay_Taxes_P3_D5" /></li>
					</ol>
					
		    	</c:if>
		    	
		         <!-- 預約交易結果 -->
	            <c:if test="${transfer_result_data.data.FGTXDATE == '2'}">
	            	<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
						<c:if test="${transfer_result_data.result == true}">
							<h4 style="margin-top:10px;font-weight:bold;color:red">
								<spring:message code="LB.W0575" />
							</h4>
							<div class="ttb-input-block">
								<!--資料時間區塊 -->
								<div class="ttb-input-item row">
								<!--資料時間  -->
								<span class="input-title">
									<label>
										<spring:message code="LB.Data_time" />
									</label>
								</span>
								<span class="input-block">
									${transfer_result_data.data.CMTXTIME }
								</span>
								</div>
								<!--資料時間區塊END -->
								<!--轉帳日期區塊 -->
								<div class="ttb-input-item row">
								<!--轉帳日期  -->
								<span class="input-title">
									<label>
										<spring:message code="LB.Transfer_date" />
									</label>
								</span>
								<span class="input-block">
									${transfer_result_data.data.CMDATE }
								</span>
								</div>
								<!--轉帳日期區塊END -->
								<!--轉出帳號區塊 -->
								<div class="ttb-input-item row">
								<!--轉出帳號  -->
								<span class="input-title">
									<label>
										<spring:message code="LB.Payers_account_no" />
									</label>
								</span>
								<span class="input-block">
									${transfer_result_data.data.OUTACN }
								</span>
								</div>
								<!--轉出帳號區塊END -->
								<!--轉出帳號區塊 -->
								<div class="ttb-input-item row">
								<!--繳稅類別  -->
								<span class="input-title">
									<label>
										<spring:message code="LB.X0469" />
									</label>
								</span>
								<span class="input-block">
									${transfer_result_data.data.TaxTypeText }
								</span>
								</div>
								<!--繳稅類別區塊END -->
						<c:choose>
	                        <c:when test="${transfer_result_data.data.TAXTYPE1 == '15'}">
		                    	<!--稽徵單位代號區塊 -->
								<div class="ttb-input-item row">
								<!--稽徵單位代號  -->
								<span class="input-title">
									<label>
										<spring:message code="LB.W0410" />
									</label>
								</span>
								<span class="input-block">
									${transfer_result_data.data.INTSACN1 }
								</span>
								</div>
								<!--稽徵單位代號區塊END -->
								<!--身分證字號/統一編號區塊 -->
								<div class="ttb-input-item row">
								<!--身分證字號/統一編號  -->
								<span class="input-title">
									<label>
										<spring:message code="LB.Id_no" />
									</label>
								</span>
								<span class="input-block">
									${transfer_result_data.data.INTSACN2 }
								</span>
								</div>
								<!--身分證字號/統一編號區塊END -->
							</c:when>
							<c:when test="${transfer_result_data.data.TAXTYPE1 == '11'}">
								<!--銷帳編號區塊 -->
								<div class="ttb-input-item row">
								<!--銷帳編號 -->
								<span class="input-title">
									<label>
										<spring:message code="LB.W0407" />
									</label>
								</span>
								<span class="input-block">
									${transfer_result_data.data.INTSACN3 }
								</span>
								</div>
								<!--銷帳編號區塊END -->
								<!--繳納截止日區塊 -->
								<div class="ttb-input-item row">
								<!--繳納截止日 -->
								<span class="input-title">
									<label>
										<spring:message code="LB.W0409" />
									</label>
								</span>
								<span class="input-block">
									${transfer_result_data.data.PAYDUED }
								</span>
								</div>
								<!--繳納截止日區塊END -->
							</c:when>
						</c:choose>
								<!--繳款金額區塊 -->
								<div class="ttb-input-item row">
								<!--繳稅金額  -->
								<span class="input-title">
									<label>
										<spring:message code="LB.W0415" />
									</label>
								</span>
								<span class="input-block">
									<spring:message code="LB.NTD" />
		                            ${transfer_result_data.data.AMOUNT }
		                            <spring:message code="LB.Dollar" />
								</span>
								</div>
								<!--繳稅金額區塊END -->
								<!--交易備註區塊 -->
								<div class="ttb-input-item row">
								<!--交易備註  -->
								<span class="input-title">
									<label>
										<spring:message code="LB.Transfer_note" /><!-- 交易備註 -->
									</label>
								</span>
								<span class="input-block">
									${transfer_result_data.data.CMTRMEMO }
								</span>
								</div>
								<!--交易備註區塊END -->
								</div>
						</c:if>
						<!-- 列印 -->
						<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
					</div>
				</div>
				
				<!-- 說明 -->
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>
					<li><spring:message code="LB.Pay_Taxes_P3_D1" /></li>
					<li><spring:message code="LB.Pay_Taxes_P3_D2" /></li>
					<li><spring:message code="LB.Pay_Taxes_P3_D3" /></li>
					<li><spring:message code="LB.Pay_Taxes_P3_D4" /></li>
					<li><spring:message code="LB.Pay_Taxes_P3_D5" /></li> 
				</ol>

		    </c:if>
	        </section>    
		</main>
	</div>

	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>