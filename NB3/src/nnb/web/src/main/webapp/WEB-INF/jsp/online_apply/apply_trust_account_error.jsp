<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
	<c:set var="bs" value="${error.data}"></c:set>
	<script type="text/javascript">
		var errObj = 
		{
				occurMsg:	"${error.msgCode}",
				URL:		"${__ctx }${bs.URL }",
				TYPE:		"${TYPE}"
		}
	</script>
<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<script type="text/javascript">
	var action = '${__ctx}';
	$(document).ready(function() {
		init();
	});
	
	function init() {
		
		$("#next").click(function() {
			console.log("next action>>" + action);
			action = action + '${error.next}'
			submitForm(action);
		});
		
		// 重新輸入
		previous();
		
		// 發生錯誤導頁
		forward();
	}
	
	
	// 重新輸入
	function previous() {
		$("#PREVIOUS").click(function() {
			initBlockUI();
			console.log("previous action>>" + action);
			action = action + '${error.previous}'
			$("form").attr("action", action);
			
			$("form").append('<input type="hidden" name="TYPE" value="${PREVIOUS_TYPE }" />');
			$("form").submit();
		});
	}
	
	
	// 發生錯誤導頁
	function forward()
	{
		$("#forward").click(function() {
			if(errObj.occurMsg === 'Z612' || errObj.occurMsg === 'Z613')
			{
				openApplyWindow(errObj.URL, errObj.TYPE);
			}
			else
			{
				submitForm(errObj.URL, errObj.TYPE);
			}
		});
	}
	
	
	function submitForm(action, type) 
	{
		initBlockUI();
		$("form").attr("action", action);
		if(type !== undefiend)
		{
			// 讓 online_apply 判斷導向哪支 Controller
			$("form").append('<input type="hidden" name="TYPE" value=' + type + ' />');
		}
		$("form").submit();
	}
	
	
	// 開新分頁
	function openApplyWindow(url, type) {
		// 讓 online_apply 判斷導向哪支 Controller
		var params = {
			"TYPE": type
		};
		openWindowWithPost(url, "", params);
	}
	
</script>
</head>
	<body>
		<!-- header -->
		<header>
			<%@ include file="../index/header_logout.jsp"%>
		</header>
		<!-- 麵包屑 -->
		<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
			<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
				<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			</ol>
		</nav>
		<!-- menu、登出窗格 -->
		<div class="content row">
			<%@ include file="../index/menu.jsp"%>
		</div>
		
		<main class="col-12">
		<!-- 主頁內容  --> 
			<section id="main-content" class="container">
				<h2><spring:message code="LB.Transaction_result"/></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="">
					
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<ul class="ttb-result-list">
								<li>
									<h3><spring:message code="LB.Transaction_code" /></h3>
									<p>${error.msgCode}</p>
								</li>
								<li>
									<h3><spring:message code="LB.Transaction_message" /></h3>
									<p>${error.message}
									<br />
									
									<c:choose>
										<c:when test="${error.msgCode eq 'Z611'}">
											<spring:message code="LB.X1165" />
											<font color=red><a id="forward" href="#">【<spring:message code="LB.X0327" />】</a></font>
										</c:when>
										<c:when test="${error.msgCode eq 'Z612'}">
											<spring:message code="LB.X0329" />
											<!--  待連結的功能實作後在Controller加入URL -->
											<font color=red><a id="forward" href="#">【<spring:message code="LB.X0305" />】</a></font>
										</c:when>
										<c:when test="${error.msgCode eq 'Z613'}">
											<spring:message code="LB.X1166" />
											<font color=red><a id="forward" href="#">【<spring:message code="LB.D1100" />】</a></font>
										</c:when>
									</c:choose>
									</p>
								</li>
							</ul>
							
							<!-- 重新輸入 -->
							<button type="button" id="PREVIOUS" class="ttb-button btn-flat-orange">
								<spring:message code="LB.Re_enter" />
							</button>
						</div>
					</div>
				</form>
			</section>
		</main>
		<!-- main-content END --> 
		<%@ include file="../index/footer.jsp"%>
	</body>
</html>