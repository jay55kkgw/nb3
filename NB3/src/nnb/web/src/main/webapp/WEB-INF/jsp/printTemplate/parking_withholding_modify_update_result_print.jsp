<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function(){
			window.print();
		});
	</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br/><br/>
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
	<br/><br/><br/>
	<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
	<br/><br/><br/>
	
	<div> 
		<!-- 資料 -->
		<!-- 表格區塊 -->
		<div class="ttb-input-block">
			<table class="print">
				<tr>
					<td style="text-align: center"><spring:message code="LB.X0279"/></td>
					<td><spring:message code="LB.X0294"/></td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code="LB.Trading_time"/></td>
					<td>${CMQTIME}</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code="LB.W0702"/></td>
					<td>${Account}</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code="LB.D0059"/></td>
					<td>${ZoneCode}</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code="LB.D0342"/></td>
					<td>${ CarId }</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code="LB.D0343"/></td>
					<td>
						${CarKind}
					</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code="LB.D0020"/></td>
					<td><spring:message code="LB.X0278"/></td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code="LB.Mail_address"/></td>
					<td>${Email}</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code="LB.D0069"/></td>
					<td>${Phone}</td>
				</tr>
				<tr>
					<td style="text-align: center"><spring:message code="LB.X0281"/></td>
					<td>${RespCode}-${RespString}</td>
				</tr>
			</table>
		</div>
		
		<br>
		<br>
	</div>
	
</body>
</html>