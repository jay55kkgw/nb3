<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>

<script type="text/javascript">
    var main;
	$(document).ready(function() {
		initFootable();
		main = document.getElementById("formId");
	});

$(document).keydown(function(event){
		
		if(!$('#errorBtn2').is(":hidden") && !wait && event.keyCode == 27){
		
			console.log("27",confirmType);
			if(confirmType==1){
	        	main.CMSUBMIT.disabled = true;  
				main.action = '${__ctx}/DIGITAL/ACCOUNT/financial_card_confirm';
				setTimeout("main.submit();", 250);
			}
	        if(confirmType==2){
	        	main.CMSUBMIT.disabled = true;  
				main.action = '${__ctx}/DIGITAL/ACCOUNT/financial_card_confirm';	    
				setTimeout("main.submit();", 250);
			}
			confirmType=0;
		}
	});
	
	function processQuery2() {
		console.log("processQuery2...");
		main.UID.value=main.CUSIDN.value;
		
		// Ajax打啟用的電文
		var uri = '${__ctx}' + "/DIGITAL/ACCOUNT/financial_card_confirm_step2_activate_aj";
		
		rdata = {
			ACN:"${financial_card_confirm_step2.data.ACN}",
	 		CUSIDN:"${financial_card_confirm_step2.data.CUSIDN}",
		};

		console.log("creatOutAcn.uri: " + uri);
		console.log("creatOutAcn.rdata: " + rdata);

		data = fstop.getServerDataEx(uri, rdata, false, CheckIdResult);
				
		return false;	 				
	}

	var confirmType=0;
	
	$("#errorBtn1").click(function(){
		if(confirmType==1){
			main.CMSUBMIT.disabled = true;  
			main.action = '${__ctx}/DIGITAL/ACCOUNT/financial_card_confirm_step3';	    
			main.submit();
		}
        if(confirmType==2){
        	main.CMSUBMIT.disabled = true;  
			main.action = '${__ctx}/DIGITAL/ACCOUNT/financial_card_confirm_step3';	    
			main.submit();
		}
		confirmType=0;
	});
	
	
	
	$("#errorBtn2").click(function(){
        if(confirmType==1){
        	main.CMSUBMIT.disabled = true;  
			main.action = '${__ctx}/DIGITAL/ACCOUNT/financial_card_confirm';	    
			main.submit();
		}
        if(confirmType==2){
        	main.CMSUBMIT.disabled = true;  
			main.action = '${__ctx}/DIGITAL/ACCOUNT/financial_card_confirm';	    
			main.submit();
		}
		confirmType=0;
	});
	function CheckIdResult(data) {
		var main = document.getElementById("formId");
		console.log("CheckIdResult.data: " + JSON.stringify(data));
		var ResultStr = data.msgCode;
		//CRC.showTempMessage(500, "晶片金融卡身份查驗", "", false);
		//alert("ResultStr:"+ResultStr+" len:"+ResultStr.length);
		//CRC.finalSendout(document.getElementById('MaskArea'), true);
		
		if(ResultStr=="E300") {
			confirmType = 1;
			errorBlock(
						null, 
						null,
						['<spring:message code="LB.Confirm022"/>'], 
						'<spring:message code= "LB.Confirm" />', 
						'<spring:message code= "LB.Cancel" />'
					);
// 			var r1 = confirm('<spring:message code="LB.Confirm022"/>');
// 			if (r1 == true) {
// 	 			main.CMSUBMIT.disabled = true;  
// 				main.action = '${__ctx}/DIGITAL/ACCOUNT/financial_card_confirm_step3';	    
// 				main.submit();
// 			} else {
// 	 			main.CMSUBMIT.disabled = true;  
// 				main.action = '${__ctx}/DIGITAL/ACCOUNT/financial_card_confirm';	    
// 				main.submit();
// 			}
		} else {
			if(ResultStr=="" || ResultStr=="0000") {
				confirmType = 2;
				errorBlock(
						null, 
						null,
						['<spring:message code="LB.Confirm023"/>'], 
						'<spring:message code= "LB.Confirm" />', 
						'<spring:message code= "LB.Cancel" />'
					);
// 				var r2 = confirm('<spring:message code="LB.Confirm023"/>');
// 				if (r2 == true) {
// 	 				main.CMSUBMIT.disabled = true;  
// 					main.action = '${__ctx}/DIGITAL/ACCOUNT/financial_card_confirm_step3';	    
// 					main.submit();
// 				} else {
// 	 				main.CMSUBMIT.disabled = true;  
// 					main.action = '${__ctx}/DIGITAL/ACCOUNT/financial_card_confirm';	    
// 					main.submit();
// 				}				
			} else {
				//alert('<spring:message code="LB.Alert054"/>'+ResultStr);
				errorBlock(
							null, 
							null,
							['<spring:message code="LB.Alert054"/>'+ResultStr], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				return false;
			}
		}
	} 
</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	<!-- 交易機制所需畫面 -->
<%-- 	<%@ include file="../component/trading_component.jsp"%> --%>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Register" /></li>
    <!-- 數位存款帳戶晶片金融卡確認領用申請及變更密碼     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1535" /></li>
		</ol>
	</nav>

	
	<div class="content row">
		<main class="col-12">
			<section id="main-content" class="container">
				<!-- 數位存款帳戶晶片金融卡確認領用申請及變更密碼 -->
				<h2><spring:message code="LB.D1535"/></h2>
				<div id="step-bar">
                    <ul>
                        <li class="active">身分驗證</li>
                        <li class="">變更密碼</li>
                        <li class="">完成申請</li>
                    </ul>
                </div>
				<form id="formId" method="post" action="" >
					<input type="hidden" value="NB32" name="ADOPID"> 
					<input type="hidden" value="${financial_card_confirm_step2.data.ACN}" name="ACN">
					<input type="hidden" value="${financial_card_confirm_step2.data.FGTXWAY}" name="FGTXWAY"> 
					<input type="hidden" value="${financial_card_confirm_step2.data.CUSIDN}" name="CUSIDN"> 
					<input type="hidden" value="${financial_card_confirm_step2.data.UID}" name="UID"> 
					<input type="hidden" value="02" name="TYPE">
	
					<div class="main-content-block row">
						<div class="col-12 tab-content"">
							<div class="ttb-input-block">
	                            <div class="ttb-message">
	                            	<!-- 確認收到晶片金融卡 -->
	                                <p><spring:message code="LB.D1542"/></p>
	                            </div>
	                            
	                            <!-- 帳號 -->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                    <label>
	                                        <h4><spring:message code="LB.Account"/></h4>
	                                    </label>
	                                </span>
	                                <span class="input-block">
	                                    <div class="ttb-input">
	                                        ${financial_card_confirm_step2.data.ACN}
	                                    </div>
	                                </span>
	                            </div>
	                            <!-- 服務分行 -->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                    <label>
	                                        <h4><spring:message code="LB.D0998"/></h4>
	                                    </label>
	                                </span>
	                                <span class="input-block">
	                                    <div class="ttb-input">
	                                    	${financial_card_confirm_step2.data.BRHNAME}
	                                    </div>
	                                </span>
	                            </div>
                            
							</div>		
							
							<input class="ttb-button btn-flat-orange" type="button" name="CMSUBMIT" id="CMSUBMIT" value="<spring:message code="LB.X0345"/>" onclick="processQuery2()"/>
												
						</div>
					</div>
					
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>

</html>