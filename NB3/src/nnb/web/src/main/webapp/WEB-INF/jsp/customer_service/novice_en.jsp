<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
</head>
<script type="text/javascript">
	$(document).ready(function(){
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	function init(){
		$("#CMBACK").click(function(e){
			$("#formId").attr("action","${__ctx}/login");
			$("#formId").submit();
		});
	}
	
	function pdfdownload(data){
		window.open("${__ctx}/CUSTOMER/SERVICE/pdf_download?pdf="+data,"_black");
	}
</script>
<body>
   	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	<!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><a href="#">Instructions</a></li>
		</ol>
	</nav>
	
	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">
		<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2>
					Instructions
				</h2>
				<form id="formId" method="post" action="">
					<div class="card-block shadow-box terms-pup-blcok">
	                    <nav class="nav card-select-block text-center d-block" style="padding-top: 30px;" id="nav-tab" role="tablist">
	                        <input type="button" class="nav-item ttb-sm-btn active" id="nav-trans-1-tab" data-toggle="tab" href="#nav-trans-1" role="tab" aria-selected="false" value="First Login" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-2-tab" data-toggle="tab" href="#nav-trans-2" role="tab" aria-selected="true" value="Transfer" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-3-tab" data-toggle="tab" href="#nav-trans-3" role="tab" aria-selected="true" value="Demand Deposit Balance" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-4-tab" data-toggle="tab" href="#nav-trans-4" role="tab" aria-selected="true" value="Reset username/check-in password" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-5-tab" data-toggle="tab" href="#nav-trans-5" role="tab" aria-selected="true" value="Current Deposit to Time Deposit" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-6-tab" data-toggle="tab" href="#nav-trans-6" role="tab" aria-selected="true" value="Foreign Exchange/Pre-designated Transfer" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-7-tab" data-toggle="tab" href="#nav-trans-7" role="tab" aria-selected="true" value="Pay Taxes" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-8-tab" data-toggle="tab" href="#nav-trans-8" role="tab" aria-selected="true" value="Modify Systematic Investment Convention" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-9-tab" data-toggle="tab" href="#nav-trans-9" role="tab" aria-selected="true" value="Systematic Investment" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-10-tab" data-toggle="tab" href="#nav-trans-10" role="tab" aria-selected="true" value="Credit Card Overview" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-11-tab" data-toggle="tab" href="#nav-trans-11" role="tab" aria-selected="true" value="Pay the TBB Credit Card Fees" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-12-tab" data-toggle="tab" href="#nav-trans-12" role="tab" aria-selected="true" value="My Email Setting" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-13-tab" data-toggle="tab" href="#nav-trans-13" role="tab" aria-selected="true" value="Common Account Settings" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-14-tab" data-toggle="tab" href="#nav-trans-14" role="tab" aria-selected="true" value="Apply for E-Reconciliation Statement" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-15-tab" data-toggle="tab" href="#nav-trans-15" role="tab" aria-selected="true" value="Setting Domestic NTD Remittance Notification" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-16-tab" data-toggle="tab" href="#nav-trans-16" role="tab" aria-selected="true" value="Operation manual download" />
	                    </nav>
	                    <div class="col-12 tab-content" id="nav-tabContent">
	                    	<!-- 首次登入 -->
	                    	<div class="ttb-input-block tab-pane fade show active" id="nav-trans-1" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">First Login</h3>
	                                    <p>Function introduction</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>Provide you to change your login password and SSL password after you log in for the first time.</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/firsttime_en.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/firsttime-2_en.png" alt="Second slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicatorWord" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 1</h5>
	                                                <p>1.Please enter the old login password and the new login password.</p>
	                                                <p>2.Please enter the old SSL password and the new SSL password.</p>
	                                                <p>3.After inputting, press "Confirm".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 2</h5>
	                                                <p>The result after the change is displayed.</p>
	                                                <p>After pressing "Confirm", return to the login page and log in with the changing password.</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- Transfer -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-2" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">Transfer</h3>
	                                    <p>Function introduction</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>Provide you with instant or scheduled NTD transfer.</p>
	                                                </li>
	                                                <li>
	                                                    <p>It has been designated to transfer to account number, you can transfer by "transaction password (SSL)"、"electronic signature (vehicle i-Key)" 、 "chip financial card" 、 "Device push authentication"; if you have not designated to transfer to account, please use the "electronic signature(vehicle i-Key)" 、 "chipFinancial Card" 、 "Device push authentication".</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator2" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/N070-1_en.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/N070-2_en.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/N070-5_en.png" alt="Third slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator2word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 1</h5>
	                                                <p>After entering the necessary fields such as accounts for outwards transfer,  accounts for inwards transfer, and transferring amount, press "Confirm".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator2" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator2" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 2</h5>
	                                                <p>Enter the account number marked in yellow and select the transaction mechanism. If it is a chip financial card (you need to enter the verification code), press "Confirm".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator2" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator2" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 3</h5>
	                                                <p>Display the trading results.</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator2" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator2" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 存款帳戶查詢 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-3" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">Demand Deposit Balance</h3>
	                                    <p>Function introduction</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>Provide you to inquiry with the balance of all NTD current accounts.</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator3" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/balance_query_1_en.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/balance_query_2_en.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/balance_query_3_en.png" alt="Second slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator3word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 1</h5>
	                                                <p>Click on the "Demand Deposit Balance" under the "NTD Service" project.</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator3" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator3" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 2</h5>
	                                                <p>Show all NTD current account balances.</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator3" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator3" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 3</h5>
	                                                <p>Click on the quick menu to direct to other NTD service transactions.</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator3" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator3" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 重設使用者名稱/簽入密碼 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-4" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">Reset username/check-in password</h3>
	                                    <p>Function introduction</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>Provides you  to use when you forgot your user name or login password.</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator4" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/username_alert_1_en.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/username_alert_2_en.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/username_alert_3_en.png" alt="Third slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/username_alert_4_en.png" alt="Four slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator4word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 1</h5>
	                                                <p>Click "Forgot User name/Password".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator4" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator4" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 2</h5>
	                                                <p>Click "Reset username/check-in password".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator4" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator4" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 3</h5>
	                                                <p>1.Please enter the ID number, user name and login password.</p>
	                                                <p>2.Click "Get SMS verification code" and wait for the mobile phone to receive the text message and enter the SMS verification code.</p>
	                                                <p>3.Please enter the verification code and confirm the insertion of the bank's chip financial card, click "Confirm".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator4" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator4" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 4</h5>
	                                                <p>The result of the reset is displayed.</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator4" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator4" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 轉入臺幣綜存定存 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-5" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">Current Deposit to Time Deposit</h3>
	                                    <p>Function introduction</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>Provide you with instant or scheduled transfer to the Taiwan dollar comprehensive deposit.</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator5" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/deposit_transfer_1_EN.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/deposit_transfer_2_EN.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/deposit_transfer_3_EN.png" alt="Third slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/deposit_transfer_4_EN.png" alt="Four slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator5word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 1</h5>
	                                                <p>Click "Current Deposit to Time Deposit".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator5" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator5" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 2</h5>
	                                                <p>Enter the necessary fields such as transfer account number, transfer account number, and transfer amount, and then click "Confirm".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator5" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator5" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 3</h5>
	                                                <p>Enter the transfer account number marked in yellow and select the trading mechanism. If it is a chip financial card (you need to enter the verification code), click "Confirm".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator5" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator5" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 4</h5>
	                                                <p>Display transaction results.</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator5" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator5" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 換匯/轉帳交易 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-6" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">Foreign Exchange/Pre-designated Transfer</h3>
	                                    <p>Function introduction</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>Provide you with instant or scheduled exchange and transfer transactions.</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator6" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/f_transfer_1_EN.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/f_transfer_2_EN.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/f_transfer_3_EN.png" alt="Third slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/f_transfer_4_EN.png" alt="Four slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/f_transfer_5_EN.png" alt="Five slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/f_transfer_6_EN.png" alt="Six slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/f_transfer_7_EN.png" alt="Seven slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator6word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 1</h5>
	                                                <p>Click "Foreign Exchange/Pre-designated Transfer".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator6" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator6" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 2</h5>
	                                                <p>After reviewing the precautions, press "Confirm".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator6" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator6" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 3</h5>
	                                                <p>Enter the necessary fields such as transfer account number, transfer currency type, transfer account number, transfer currency type, and transfer amount, and then click "Confirm".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator6" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator6" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 4</h5>
	                                                <p>After confirming the transfer information, press "Next".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator6" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator6" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 5</h5>
	                                                <p>After confirming the exchange rate information, click "Confirm".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator6" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator6" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 6</h5>
	                                                <p>Enter the transfer account number marked in yellow, select the transaction mechanism, and click "Confirm".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator6" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator6" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 7</h5>
	                                                <p>Display transaction results.</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator6" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator6" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 繳稅 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-7" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">Pay Taxes</h3>
	                                    <p>Function introduction</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>Provide you with instant or scheduled tax payment.</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator7" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/pay_taxes_1_EN.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/pay_taxes_2_EN.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/pay_taxes_3_EN.png" alt="Third slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/pay_taxes_4_EN.png" alt="Four slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator7word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 1</h5>
	                                                <p>Click "Pay Taxes".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator7" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator7" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 2</h5>
	                                                <p>Enter the necessary fields such as account number, tax type, and tax payment amount, and then click "Confirm".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator7" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator7" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 3</h5>
	                                                <p>Choose a trading mechanism. If it is a chip financial card (you need to enter the verification code), click "Confirm".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator7" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator7" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 4</h5>
	                                                <p>Display transaction results.</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator7" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator7" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 基金定期投資約定變更 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-8" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">Modify Systematic Investment Convention</h3>
	                                    <p>Function introduction</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>Provide you with changes to the fund's regular investment agreement.</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator8" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/regular_investment_1_EN.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/regular_investment_2_EN.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/regular_investment_3_EN.png" alt="Third slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/regular_investment_4_EN.png" alt="Four slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/regular_investment_5_EN.png" alt="Five slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator8word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 1</h5>
	                                                <p>Click "Modify Systematic Investment Convention".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator8" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator8" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 2</h5>
	                                                <p>Select the information you want to change and press "Confirm".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator8" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator8" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 3</h5>
	                                                <p>Select the change item, enter the necessary fields, and press "Confirm".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator8" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator8" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 4</h5>
	                                                <p>Confirm the information, enter the transaction password, and click "Confirm".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator8" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator8" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 5</h5>
	                                                <p>The result of the change is displayed.</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator8" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator8" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 基金定期定額申購 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-9" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">Systematic Investment</h3>
	                                    <p>Function introduction</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>Provide you with regular fixed-rate subscription of funds.</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator9" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/fund_regular_1_EN.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/fund_regular_2_EN.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/fund_regular_3_EN.png" alt="Third slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/fund_regular_4_EN.png" alt="Four slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/fund_regular_5_EN.png" alt="Five slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/fund_regular_6_EN.png" alt="Six slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/fund_regular_7_EN.png" alt="Seven slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator9word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 1</h5>
	                                                <p>Click "Systematic Investment".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator9" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator9" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 2</h5>
	                                                <p>Enter the necessary fields such as the name of the fund company, the name of the fund, and the purchase amount, and then click "Confirm".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator9" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator9" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 3</h5>
	                                                <p>Read the fund risk, select the read fund risk, and click "Confirm".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator9" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator9" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 4</h5>
	                                                <p>After choosing the method of obtaining the public prospectus / investor instructions, press "Confirm".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator9" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator9" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 5</h5>
	                                                <p>Read the remuneration description, check the box and press "Confirm".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator9" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator9" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 6</h5>
	                                                <p>Confirm the information and enter the transaction password, and click "Confirm".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator9" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator9" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 7</h5>
	                                                <p>Display transaction results.</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator9" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator9" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 信用卡持卡總覽 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-10" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">Credit Card Overview</h3>
	                                    <p>Function introduction</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>Allows you to view the credit card you have.</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator10" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/card_overview_1_en.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/card_overview_2_en.png" alt="Second slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator10word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 1</h5>
	                                                <p>Click "Credit Card Overview".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator10" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator10" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 2</h5>
	                                                <p>Display the results of the query.</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator10" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator10" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 繳納本行信用卡款 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-11" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">Pay the TBB Credit Card Fees</h3>
	                                    <p>Function introduction</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>Provide you with the bill to pay your credit card.</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator11" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/card_payment_1_en.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/card_payment_2_en.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/card_payment_3_en.png" alt="Third slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/card_payment_4_en.png" alt="Four slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator11word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 1</h5>
	                                                <p>Click "Pay the TBB Credit Card Fees".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator11" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator11" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 2</h5>
	                                                <p>1.Select or enter the necessary fields such as "Transfer Account Number", "Transfer Credit Card Account", and "Payment Amount".</p>
	                                                <p>2.After inputting, click "Confirm".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator11" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator11" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 3</h5>
	                                                <p>Enter the transfer account number marked in yellow and select the trading mechanism. If it is a chip financial card (you need to enter the verification code), click "Confirm".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator11" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator11" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 4</h5>
	                                                <p>Display the result of payment.</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator11" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator11" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 我的Email設定 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-12" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">My Email Setting</h3>
	                                    <p>Function introduction</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>Provide you to set your personal email.</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator12" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/email_setting_1_en.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/email_setting_2_en.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/email_setting_3_en.png" alt="Third slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/email_setting_4_en.png" alt="Four slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator12word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 1</h5>
	                                                <p>Click "Setting Email".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator12" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator12" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 2</h5>
	                                                <p>Select "My Email" and click "Confirm".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator12" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator12" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 3</h5>
	                                                <p>Enter your e-mail address, select the transaction mechanism, and click "Confirm".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator12" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator12" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 4</h5>
	                                                <p>The result of the setting is displayed.</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator12" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator12" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 常用帳號設定 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-13" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">Common Account Settings</h3>
	                                    <p>Function introduction</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>Provide you to set up your frequently used account.</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator13" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/account_setting_1_en.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/account_setting_2_en.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/account_setting_3_en.png" alt="Third slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator13word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 1</h5>
	                                                <p>Click "Common Account Settings".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator13" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator13" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 2</h5>
	                                                <p>Enter "transfer account" and "friendly name", and click "Confirm".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator13" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator13" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 3</h5>
	                                                <p>The result of the setting is displayed.</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator13" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator13" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 申請電子帳單 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-14" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">Apply for E-Reconciliation Statement</h3>
	                                    <p>Function introduction</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>Provide you to apply for an electronic bill.</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator14" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/electronic_reconciliation_1_en.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/electronic_reconciliation_2_en.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/electronic_reconciliation_3_en.png" alt="Third slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/electronic_reconciliation_4_en.png" alt="Four slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/electronic_reconciliation_5_en.png" alt="Five slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator14word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 1</h5>
	                                                <p>Click "Apply for E-Reconciliation Statement".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator14" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator14" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 2</h5>
	                                                <p>Select the project you want to apply, and click "Confirm".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator14" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator14" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 3</h5>
	                                                <p>Read the agreed terms carefully. If you agree, click "Accept & Next".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator14" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator14" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 4</h5>
	                                                <p>Enter the transaction password and click "Confirm".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator14" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator14" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 5</h5>
	                                                <p>The result of the application is displayed.</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator14" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator14" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 國內臺幣匯入匯款通知設定 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-15" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">Setting Domestic NTD Remittance Notification</h3>
	                                    <p>Function introduction</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>Allows you to set up inbound remittance notifications.</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator15" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/N935_en_1.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/N935_en_2.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/N935_en_3.png" alt="Third slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator15word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 1</h5>
	                                                <p>Click "Setting Domestic NTD Remittance Notification".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator15" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator15" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 2</h5>
	                                                <p>Check the "Account" to be set and fill in the "Amount".</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator15" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator15" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>Operation steps</p>
	                                                <h5>Step 3</h5>
	                                                <p>The result of the setting is displayed.</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator15" role="button" data-slide="prev" value="Previous" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator15" role="button" data-slide="next" value="Next step" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 操作手冊下載 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-16" role="tabpanel" aria-labelledby="nav-home-tab">
	                        	<h3 class="guide-function-title">Operation manual download</h3>
								<div style="display: flex">
									<div class="element-area" style="background-color: #ffffff">
										<!-- 登入 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('login')">
												<img src="${__ctx}/img/icon-ob_login.svg?a=${jscssDate}" style="border-radius:0%;">
												<span><spring:message code="LB.Login" /></span>
											</a>
										</div>
										<!-- 我的首頁 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('Myhome')">
												<img src="${__ctx}/img/menu-icon-01.svg?a=${jscssDate}">
												<span><spring:message code="LB.My_home" /></span>
											</a>
										</div>
										<!-- 帳戶總覽 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('AccountOverview')">
												<img src="${__ctx}/img/menu-icon-02.svg?a=${jscssDate}">
												<span><spring:message code="LB.Account_Overview" /></span>
											</a>
										</div>
										<!-- 臺幣服務 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('NTDService')">
												<img src="${__ctx}/img/menu-icon-03.svg?a=${jscssDate}">
												<span><spring:message code="LB.NTD_Services" /></span>
											</a>
										</div>
										<!-- 外幣服務 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('ForeignCurrency')">
												<img src="${__ctx}/img/menu-icon-04.svg?a=${jscssDate}">
												<span><spring:message code="LB.FX_Service" /></span>
											</a>
										</div>
										<!-- 繳費繳稅 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('PaymentofTaxes')">
												<img src="${__ctx}/img/menu-icon-05.svg?a=${jscssDate}">
												<span><spring:message code="LB.W0366" /></span>
											</a>
										</div>
										<!-- 貸款專區 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('LoanService')">
												<img src="${__ctx}/img/menu-icon-06.svg?a=${jscssDate}">
												<span><spring:message code="LB.Loan_Service" /></span>
											</a>
										</div>
									</div>
								</div>
								<div style="display: flex">
									<div class="element-area" style="background-color: #ffffff">
										<!-- 基金 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('FundsBond')">
												<img src="${__ctx}/img/menu-icon-07.svg?a=${jscssDate}">
												<span><spring:message code="LB.Funds" /></span>
											</a>
										</div>
										<!-- 黃金存摺 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('GoldPassbook')">
												<img src="${__ctx}/img/menu-icon-08.svg?a=${jscssDate}">
												<span><spring:message code="LB.W1428" /></span>
											</a>
										</div>
										<!-- 信用卡 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('CreditCard')">
												<img src="${__ctx}/img/menu-icon-09.svg?a=${jscssDate}">
												<span><spring:message code="LB.Credit_Card" /></span>
											</a>
										</div>
										<!-- 線上服務專區 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('OnlineServices')">
												<img src="${__ctx}/img/menu-icon-10.svg?a=${jscssDate}" style="border-radius:0%;">
												<span><spring:message code="LB.X0262" /></span>
											</a>
										</div>
										<!-- 個人服務 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('PersonalServices')">
												<img src="${__ctx}/img/menu-icon-11.svg?a=${jscssDate}">
												<span><spring:message code="LB.Personal_Service" /></span>
											</a>
										</div>
										<!-- 線上申請 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('Register')">
												<img src="${__ctx}/img/icon-03.svg?a=${jscssDate}">
												<span><spring:message code="LB.Register" /></span>
											</a>
										</div>
										<!-- 附件 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('Attachment')">
												<img src="${__ctx}/img/icon-attach.svg?a=${jscssDate}">
												<span>Attachment</span>
											</a>
										</div>
									</div>
								</div>
	                        </div>
	                        <input type="BUTTON" class="ttb-button btn-flat-gray" value="Back" name="CMBACK" id="CMBACK">
	                    </div>
	                </div>
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
	<script type="text/javascript">
		//步驟說明變換
		var carousel1 = $('#carouselGuideIndicator').carousel();
		var carousel2= $('#carouselGuideIndicatorWord').carousel();
		carousel1.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel2.carousel(to);
		});
	
		var carousel3 = $('#carouselGuideIndicator2').carousel();
		var carousel4= $('#carouselGuideIndicator2word').carousel();
		carousel3.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel4.carousel(to);
		});
	
		var carousel5 = $('#carouselGuideIndicator3').carousel();
		var carousel6= $('#carouselGuideIndicator3word').carousel();
		carousel5.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel6.carousel(to);
		});
	
		var carousel7 = $('#carouselGuideIndicator4').carousel();
		var carousel8= $('#carouselGuideIndicator4word').carousel();
		carousel7.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel8.carousel(to);
		});
		
		var carousel9 = $('#carouselGuideIndicator5').carousel();
		var carousel10= $('#carouselGuideIndicator5word').carousel();
		carousel9.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel10.carousel(to);
		});
	
		var carousel11 = $('#carouselGuideIndicator6').carousel();
		var carousel12= $('#carouselGuideIndicator6word').carousel();
		carousel11.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel12.carousel(to);
		});
	
		var carousel13 = $('#carouselGuideIndicator7').carousel();
		var carousel14= $('#carouselGuideIndicator7word').carousel();
		carousel13.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel14.carousel(to);
		});
	
		var carousel15 = $('#carouselGuideIndicator8').carousel();
		var carousel16= $('#carouselGuideIndicator8word').carousel();
		carousel15.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel16.carousel(to);
		});
	
		var carousel17 = $('#carouselGuideIndicator9').carousel();
		var carousel18= $('#carouselGuideIndicator9word').carousel();
		carousel17.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel18.carousel(to);
		});
		
		var carousel19 = $('#carouselGuideIndicator10').carousel();
		var carousel20= $('#carouselGuideIndicator10word').carousel();
		carousel19.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel20.carousel(to);
		});
		
		var carousel21 = $('#carouselGuideIndicator11').carousel();
		var carousel22= $('#carouselGuideIndicator11word').carousel();
		carousel21.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel22.carousel(to);
		});
		
		var carousel23 = $('#carouselGuideIndicator12').carousel();
		var carousel24= $('#carouselGuideIndicator12word').carousel();
		carousel23.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel24.carousel(to);
		});
		
		var carousel25 = $('#carouselGuideIndicator13').carousel();
		var carousel26= $('#carouselGuideIndicator13word').carousel();
		carousel25.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel26.carousel(to);
		});
		
		var carousel27 = $('#carouselGuideIndicator14').carousel();
		var carousel28= $('#carouselGuideIndicator14word').carousel();
		carousel27.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel28.carousel(to);
		});
		
		var carousel29 = $('#carouselGuideIndicator15').carousel();
		var carousel30= $('#carouselGuideIndicator15word').carousel();
		carousel29.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel30.carousel(to);
		});
	</script>
</body>
</html>
