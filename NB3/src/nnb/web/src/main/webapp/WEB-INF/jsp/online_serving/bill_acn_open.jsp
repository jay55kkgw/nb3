<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
   <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">    
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
</head>
 <body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 支票存款開戶申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0514" /></li>
		</ol>
	</nav>

	
	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
	<main class="col-12"> 
	<!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<h2><spring:message code="LB.D0514" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>					
			<form method="post" id="formId">
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-message">
							<p><spring:message code="LB.D0486" /></p>
						</div>
						<div class="ttb-input-block">							
							<!-- 申請人 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
								<h4><spring:message code="LB.D0516" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text" maxLength="50" id="CUSNAME" name="CUSNAME" class="select-input validate[required]">
									</div>
								</span>
							</div>
							
							<!-- 地址 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
								<h4><spring:message code="LB.D0517" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text" maxLength="66" id="ADDRESS" name="ADDRESS" class="select-input validate[required]">
									</div>
								</span>
							</div>								
							
							<!-- 營業種類或職(事)業 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0518" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text" maxLength="40" id="BUSINESS" name="BUSINESS" class="select-input validate[required]">
									</div>
								</span>
							</div>	
							 
							<!-- 身分證統一編號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0519" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${bill_acn_open.data.CUSIDN}</span>							
									</div>
								</span>
							</div>
							 
							<div class="ttb-input-item row">
								<!--票信查詢費指定扣帳帳號  -->
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.D0520" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">										
										<select name="ACN" id="ACN" class="custom-select select-input half-input validate[required]">
											<c:forEach var="dataList" items="${bill_acn_open.data.REC}">
												<option>${dataList.ACN}</option>
											</c:forEach>
										</select>
									</div>
								</span>
							</div>
							
							<!-- 生日 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0521" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<!--生日 -->
										<div class="ttb-input">	
											<input type="text" id="BRHDAY" name="BRHDAY" class="text-input datetimepicker" maxlength="10" value="" /> 
											<span class="input-unit BRHDAY">
												<img src="${__ctx}/img/icon-7.svg" />
											</span>
											<!-- 不在畫面上顯示的span -->
											<span id="hideblocka" >
											<!-- 驗證用的input -->
											<input id="Monthly_DateA" name="Monthly_DateA" type="text" class="text-input validate[required, verification_date[Monthly_DateA]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
									</div>
								</span>
							</div>
							
							<!-- 國別 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0522" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">	
										<c:choose>
											<c:when test="${transfer == 'en'}">
												<select name="COUNTRY" id="COUNTRY" class="custom-select select-input half-input validate[required]">
												<c:forEach var="countryList" items="${bill_acn_open.data.COUNTRYNAME}">
													<option value="${countryList.COUNTRYABB}">${countryList.COUNTRYEN}</option>
												</c:forEach>
											</select>
											</c:when>
											<c:when test="${transfer == 'zh'}">
												<select name="COUNTRY" id="COUNTRY" class="custom-select select-input half-input validate[required]">
													<c:forEach var="countryList" items="${bill_acn_open.data.COUNTRYNAME}">
														<option value="${countryList.COUNTRYABB}">${countryList.COUNTRYCN}(${countryList.COUNTRYABB})</option>
													</c:forEach>
												</select>
											</c:when>
											<c:otherwise>
												<select name="COUNTRY" id="COUNTRY" class="custom-select select-input half-input validate[required]">
													<c:forEach var="countryList" items="${bill_acn_open.data.COUNTRYNAME}">
														<option value="${countryList.COUNTRYABB}">${countryList.COUNTRY}(${countryList.COUNTRYABB})</option>
													</c:forEach>
												</select>
											</c:otherwise>										
										</c:choose>									
<!-- 										<select name="COUNTRY" id="COUNTRY" class="custom-select select-input half-input validate[required]"> -->
<%-- 											<c:forEach var="countryList" items="${bill_acn_open.data.COUNTRYNAME}"> --%>
<%-- 												<option value="${countryList.COUNTRYABB}">${countryList.COUNTRY}(${countryList.COUNTRYABB})</option> --%>
<%-- 											</c:forEach> --%>
<!-- 										</select> -->
									</div>
								</span>
<!-- 								<span class="input-block"> -->
<!-- 									<div class="ttb-input"> -->
<%-- 										<input type="text" maxLength="30" value='<spring:message code="LB.X0828" />' id="COUNTRY" name="COUNTRY" class="select-input validate[required]"> --%>
<!-- 									</div> -->
<!-- 								</span> -->
							</div>
							
							<!-- 出生地 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0523" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text" maxLength="12" id="BIRTHPLA" name="BIRTHPLA" class="select-input validate[required]">
									</div>
								</span>
							</div>
							
							<!-- 電話號碼(公) -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0524" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text" maxLength="18" id="TEL_O" name="TEL_O" class="select-input validate[required]">											
									</div>
								</span>
							</div>
							
							<!-- 電話號碼(宅) -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0525" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text" maxLength="18" id="TEL_H" name="TEL_H" class="select-input validate[required]">
									</div>
								</span>
							</div>
							
							<!--交易機制 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
									<h4><spring:message code="LB.Transaction_security_mechanism" /></h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block"> 
										<spring:message code="LB.SSL_password" /> 
										<input type="radio" name="FGTXWAY" checked="checked" value="0"> 
										<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<input id="CMPASSWORD" name="CMPASSWORD" type="password"
											class="text-input validate[required]"  
											maxlength="8" 
											placeholder="<spring:message code="LB.Please_enter_password"/>">
										<input type="hidden" id="PINNEW" name="PINNEW" value="">
										<input type="hidden" id="TYPE" name="TYPE" value="01">										
									</div>
								</span>
							</div>
							
						</div>
						<input id="CMRESET" name="CMRESET" type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" onclick="resetdata()"/>	
						<input type="button" class="ttb-button btn-flat-orange" id="CMSUBMIT" value="<spring:message code="LB.Confirm" />"/>
					</div>
				</div>
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>					
					<li><span><spring:message code="LB.Bill_Acn_Open_P1_D1" /></span></li>
					<li><span><font color="red"><spring:message code="LB.Bill_Acn_Open_P1_D2" /></font></span></li>
					<li><span><spring:message code="LB.Bill_Acn_Open_P1_D3" />
						<br><spring:message code="LB.Bill_Acn_Open_P1_D31" />
            			<br><spring:message code="LB.Bill_Acn_Open_P1_D32" />
            			<br><spring:message code="LB.Bill_Acn_Open_P1_D33" />
					</span></li>						
				</ol>
				
				</form>

<!-- 				</form> -->
			</section>
		<!-- 		main-content END -->
		</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
<!--   Js function -->
    <script type="text/JavaScript">
		$(document).ready(function() {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 50);
			// 開始跑下拉選單並完成畫面
			setTimeout("init()", 400);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
			
		});
		
		function init(){
			
			getTmr();
			datetimepickerEvent();
			//國別下拉選單預設台灣
			$('#COUNTRY').val('TW');
			// 初始化時隱藏span
			$("#hideblocka").hide();
		
			$("#formId").validationEngine({
				binded: false,
				promptPosition: "inline"
			});			
			$("#CMSUBMIT").click(function(e){
				//打開驗證隱藏欄位
				$("#hideblocka").show();
				//塞值進span內的input
				$("#Monthly_DateA").val($("#BRHDAY").val());
				
				e = e || window.event;
			
				if(!$('#formId').validationEngine('validate')){
		        	e.preventDefault();
	 			}
				else{
	 				$("#formId").validationEngine('detach');
	 				initBlockUI();
	 				$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
	 				$('#CMPASSWORD').val("");
	 				$("#formId").attr("action","${__ctx}/ONLINE/SERVING/bill_acn_open_result");
	 	  			$("#formId").submit(); 
	 			}		
	  		});
		}
		
		function getTmr() {
			var today = new Date();
			today.setDate(today.getDate());
			var y = today.getFullYear();
			var m = today.getMonth() + 1;
			var d = today.getDate();
			if(m<10){
				m = "0"+m
			}
			if(d<10){
				d="0"+d
			}
			var tmr = y + "/" + m + "/" + d
			$('#BRHDAY').val(tmr);
		}
		
// 		日曆欄位參數設定
		function datetimepickerEvent(){

		    $(".BRHDAY").click(function(event) {
				$('#BRHDAY').datetimepicker('show');
			});
			
			jQuery('.datetimepicker').datetimepicker({
				timepicker:false,
				closeOnDateSelect : true,
				scrollMonth : false,
				scrollInput : false,
			 	format:'Y/m/d',
			 	lang: '${transfer}'
			 	
			});
		}	
		
		function resetdata(){
			$("#formId")[0].reset(); 
			getTmr();
			$('#COUNTRY').val('TW');
			$('#ACN')[0].selectedIndex = 0;
		}
		
 	</script>
</body>
</html>
