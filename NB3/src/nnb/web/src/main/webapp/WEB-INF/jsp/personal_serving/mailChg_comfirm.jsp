<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>

<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<!-- 元件驗證身分JS -->
	<script type="text/javascript" src="${__ctx}/component/util/checkIdProcess.js"></script>

<script type="text/javascript">
	var urihost = "${__ctx}";
	$(document).ready(function() {
		
		//HTML載入完成後開始遮罩
		setTimeout("initBlockUI()",10);
		//解遮罩
		setTimeout("unBlockUI(initBlockId)",500);
		
		initKapImg();
		// 判斷顯不顯示驗證碼
		chaBlock();
		// 交易機制 click
		fgtxwayClick();
		
		$("#CMSUBMIT").click(function(e) {
			console.log("submit~~");
			$("#formId").validationEngine({
				binded : false,
				promptPosition : "inline"
			});
			e = e || window.event;
			if (!$('#formId').validationEngine('validate')) {
				e.preventDefault();
			} else {
				$("#formId").validationEngine('detach');
				processQuery();
			}
		});
		
		$("#cancelButton").click(function(){
			$("#formId").validationEngine('detach');
			$("#isBack").val("Y");
			$("#formId").attr("action","${__ctx}"+'${mailChg_comfirm.PREVIOUS}');
			$("#formId").submit();
		});
	});

	// 通過表單驗證準備送出
	function processQuery() {
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
		// 交易機制選項
		switch (fgtxway) {
		case '1':
			// IKEY
			useIKey();
			break;
		case '2':
			// 晶片金融卡
			useCardReader();
			break;
		case '7'://IDGATE認證		 
			idgatesubmit = $("#formId");
			showIdgateBlock();
			break;
		default:
			//請選擇交易機制
			//alert("<spring:message code= "LB.Alert001" />");
			errorBlock(null, null,
					[ "<spring:message code= 'LB.Alert001' />" ],
					'<spring:message code= "LB.Quit" />', null);
			unBlockUI(initBlockId);
		}
	}

	//IKEY簽章結束
	function uiSignForPKCS7Finish(result){
		var main = document.getElementById("main");
		
		//成功
		if(result != "false"){
			//SubmitForm();
			checkxmlcn();
		}
		//失敗
		else{
			alert("IKEY簽章失敗");
			ShowLoadingBoard("MaskArea",false);
		}
	}
	
	function checkxmlcn(){
		var cusidn = '${sessionScope.cusidn}';
		var jsondc = $('#jsondc').val();
		var pkcs7Sign = $('#pkcs7Sign').val();
		var bs64 = $('#bs64').val();
		var uri = "${__ctx}"+"/COMPONENT/ikey_without_login_aj";
		var rdata = { UID:cusidn, jsondc: jsondc, pkcs7Sign:pkcs7Sign ,bs64:bs64 };
		
		fstop.getServerDataEx(uri, rdata, true, IkeyCheckcallback);
	}
	
function IkeyCheckcallback(response) {
		
		var checkflag = response.data.checkflag;
		if(!checkflag){
			alert(response.data.msgCode +" : "+ response.data.msgName);
			ShowLoadingBoard("MaskArea",false);
			return ; 
		}
		if(checkflag.indexOf("SUCCESSFUL")>-1){
			initBlockUI();
            $("#formId").submit();
		}else{
			alert("<spring:message code= 'LB.X2287' />");
			ShowLoadingBoard("MaskArea",false);
		}
	}
	
//取得卡片主帳號結束
function getMainAccountFinish(result){
	//成功
	if(result != "false"){
		var main = document.getElementById("formId");
		main.ACNNO.value = result;
		var cardACN = result;
		if(cardACN.length > 11){
			cardACN = cardACN.substr(cardACN.length - 11);
		}
		
//		var x = { method: 'get', parameters: '', onComplete: CheckIdResult };	
//		var myAjax = new Ajax.Request( "simpleform?trancode=N953&TXID=N953&ACN=" + cardACN, x);
		var cusidn = '${sessionScope.cusidn}';
		var uri =  "${__ctx}"+"/COMPONENT/component_checkid_aj";
		var rdata = { CUSIDN: cusidn, ACN: cardACN };
		
		fstop.getServerDataEx(uri, rdata, true, CheckIdResult);
	}
	//失敗
	else{
		showTempMessage(500,"晶片金融卡身份查驗","","MaskArea",false);
	}
}
	
	function goNotificationService() {
		console.log("go Notification Service")
		$('#formId').attr("action",
				"${__ctx}" + "/PERSONAL/SERVING/notification_service");
		$('#formId').submit();
	}
	
	// 驗證碼刷新
	function changeCode() {
		$('input[name="capCode"]').val('');
		// 大小版驗證碼用同一個
		console.log("changeCode...");
		$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

		// 登入失敗解遮罩
		unBlockUI(initBlockId);
	}
	
	// 初始化驗證碼
	function initKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
	}
	
	// 生成驗證碼
	function newKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').click(function() {
			$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
		});
	}
	
	 // 判斷交易機制決定顯不顯示驗證碼區塊
	function chaBlock() {
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
 		console.log("fgtxway: " + fgtxway);
		// 交易機制選項
		if(fgtxway == '2'){
			// 若為晶片金融卡才顯示驗證碼欄位
			$("#chaBlock").show();
		}else{
			// 若非晶片金融卡則隱藏驗證碼欄位
			$("#chaBlock").hide();
		}
	 }
	 
	// 使用者選擇晶片金融卡要顯示驗證碼區塊
	 function fgtxwayClick() {
	 	$('input[name="FGTXWAY"]').change(function(event) {
	 		// 判斷交易機制決定顯不顯示驗證碼區塊
	 		chaBlock();
		});
	
	}
</script>
</head>
<body>
	
	<!-- 	晶片金融卡 -->
	<%@ include file="../component/trading_component.jsp"%>
	 <!--   IDGATE --> 		 
	<%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %>  
	
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- Email設定     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Email_Setting" /></li>
    <!-- 我的Email設定     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.My_Email_setting" /></li>
		</ol>
	</nav>

	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">

		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
	

		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2>
				<spring:message code="LB.My_Email_setting" /><!-- 我的 Email 設定 -->
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" action="${__ctx}/PERSONAL/SERVING/mailChg" method="post" autocomplete="off">
				<input type="hidden" id="isBack" name="isBack" value=""/>
				<!-- 交易機制所需欄位 -->
				<input type="hidden" id="jsondc" name="jsondc" value="${mailChg_comfirm.jsondc}">
				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
				<input type="hidden" id="ISSUER" 	name="ISSUER" />
				<input type="hidden" id="ACNNO" 	name="ACNNO" />
				<input type="hidden" id="TRMID" 	name="TRMID" />
				<input type="hidden" id="iSeqNo" 	name="iSeqNo" />
				<input type="hidden" id="ICSEQ" 	name="ICSEQ" />
				<input type="hidden" id="ICSEQ1" 	name="ICSEQ1" />
				<input type="hidden" id="TAC" 		name="TAC" />
				<input type="hidden" id="CHIP_ACN"  name="CHIP_ACN" />
				<input type="hidden" id="OUTACN"	name="OUTACN">
				<input type="hidden" id="bs64"	name="bs64" value="Y">
				<input type="hidden" name="CARDAP" value="Y">
				<div class="main-content-block row">
					<div class="col-12 tab-content" id="nav-tabContent">
						<!-- 主頁內容  -->
						<div class="ttb-input-block">
							<div class="ttb-message">
								<b><spring:message code="LB.X2634" /></b>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title"> <label><h4><spring:message code="LB.System_time" />：</h4></label></span> <!-- 系統時間 -->
								<span class="input-block">
									<div class="ttb-input">
										<p>${mailChg_comfirm.CMQTIME}</p>
									</div>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title"> <label><h4><spring:message code="LB.Current_Email" />：</h4></label></span> <!-- 目前電子郵箱 -->
								<span class="input-block">
									<div class="ttb-input">
										<p>${mailChg_comfirm.DPMYEMAIL}</p>
									</div>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title"> <label><h4><spring:message code="LB.New_Email" />：</h4></label></span> <!-- 新的電子郵箱 -->
								<span class="input-block">
									<div class="ttb-input">
										<p>${mailChg_comfirm.NEW_EMAIL}</p>
									</div>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<h4><spring:message code="LB.Transaction_security_mechanism" />：</h4>
								</span>
								<span class="input-block">
								<!-- 使用者是否可以使用IKEY -->
								<c:if test = "${sessionScope.isikeyuser}">
								<div class="ttb-input">
									<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block"> <spring:message code="LB.Electronic_signature" />
										<input type="radio" id="FGTXWAY" name="FGTXWAY" value="1"> 
										<span class="ttb-radio"></span>
										</label> <!-- 電子簽章(請載入載具i-key) --> 
									</div>
									</span>
								</div>
								</c:if>
								<div class="ttb-input">
								<span class="input-block">
								<div class="ttb-input" name="idgate_group" style="display:none">		 
									<label class="radio-block"><spring:message code= "LB.Device_push_authentication" />
										<input type="radio" id="IDGATE" 	name="FGTXWAY" value="7"> 
										<span class="ttb-radio"></span>
									</label>		 
								</div>
								</span>	
								</div>
								<div class="ttb-input">
									<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block">
								        <input type="radio" name="FGTXWAY" id="CMCARD" value="2" checked>
								        <spring:message code="LB.Financial_debit_card" />
										<span class="ttb-radio"></span>
										</label>
									</div>						           	
									</span>
								</div>
								</span>
								<div class="ttb-input-item row" id="chaBlock" style="display:none">
								<!-- 驗證碼 -->
								<span class="input-title">
									<label>
										<h4><spring:message code= "LB.Captcha" /></h4>
									</label>
								</span>
								<span class="input-block">
									<spring:message code="LB.Captcha" var="labelCapCode" />
									<input id="capCode" name="capCode" type="text"
										class="text-input" maxlength="6" autocomplete="off">
									<img name="kaptchaImage" class = "verification-img" src="" />
									<input type="button" name="reshow" class="ttb-sm-btn btn-flat-orange"
										onclick="changeCode()" value="<spring:message code= "LB.Regeneration" />" />
								</span>
								</div>
							</div>
					</div>
					<input type="button" id="cancelButton" value="<spring:message code="LB.X0420" />" class="ttb-button btn-flat-gray"/>	
								<!-- 重新輸入 -->
					<input type="button" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
								<!-- 確定 -->
				</div>
				</div>
    				<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /></p>
						<li><span><spring:message code="LB.Mail_set_P2_D1" /></span><u><input onClick="goNotificationService();" class="ttb-sm-btn btn-flat-orange" type="button" value='<spring:message code="LB.Notification_Service" />' name="CMSUBMIT1"></u></li>
						<li><span><spring:message code="LB.Mail_set_P2_D2" /></span></li>
						<li><span><spring:message code="LB.Mail_set_P2_D3" /></span></li>
						<li><span><spring:message code="LB.Mail_set_P2_D4" /></span></li>
						<li><span><font color="red"><spring:message code="LB.Mail_set_P2_D5" /></font></span></li>
					</ol>
  			</div>
			</form>
		</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>