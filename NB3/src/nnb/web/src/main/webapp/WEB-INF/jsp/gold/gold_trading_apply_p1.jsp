<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	    <script type="text/javascript">
        $(document).ready(function () {
            initFootable(); // 將.table變更為footable 
            init();
        });

        function init() {
	    	$("#pageshow").click(function(e){			
// 	        	initBlockUI();
				var action = '${__ctx}/GOLD/APPLY/gold_trading_apply_p2';
				$("form").attr("action", action);
    			$("form").submit();
			});
	    	$("#CMBACK").click(function(e){			
// 	        	initBlockUI();
				var action = '${__ctx}/INDEX/index';
				$("form").attr("action", action);
    			$("form").submit();
			});
        }
        
    </script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 黃金存摺帳戶     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2273" /></li>
    <!-- 黃金存摺網路交易申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1687" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
				<!-- 	線上申請黃金存摺網路交易 -->
					<spring:message code="LB.W1687"/>
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <div id="step-bar">
                    <ul>
                        <li class="active">注意事項與權益</li>
                        <li class="">設定帳戶</li>
                        <li class="">確認資料</li>
                        <li class="">申請結果</li>
                    </ul>
                </div>
                <form id="formId" method="post">
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12 terms-block">
	                        <div class="ttb-message">
	                            <p>
									<!--  注意事項 -->
									<spring:message code="LB.D1101"/>
								</p>
	                        </div>
	                        <div class="text-left">
	                            <p>親愛的客戶您好：</p>
	                            
                            	<span>本服務須具備下列申請要件：</span>
	
	                            <ul class="list-decimal">
	                                <li data-num="1.">
	                                    <span>年滿20歲之本國人。</span>
	                                </li>
	                                <li data-num="2.">
	                                    <span>已申請使用本行網路銀行且已約定新臺幣活期性存款（不含支票存款）帳戶為轉出帳號者。</span>
	                                </li>
	                                <li data-num="3.">
	                                    <span>已臨櫃開立黃金存摺帳戶，但尚未申請黃金存摺網路交易。</span>
	                                </li>
	                                <li data-num="4.">
	                                    <span>已申請使用本行晶片金融卡或電子簽章(憑證载具)。</span>
	                                </li>
	                            </ul>
	                            <p>符合上述要件，請備妥您的「臺灣企銀晶片金融卡＋讀卡機」或「電子簽章（憑證載具）」，並點擊下一步繼續。</p>
	
	                        </div>
                           
	                        <!--button 區域 -->
                            <input id="CMBACK" name="CMBACK" type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Cancel" />" />
                             <!-- 確定 -->
                            <input id="pageshow" name="pageshow" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.X0080" />" />
                        <!--                     button 區域 -->
                        </div>  
                    </div>
                </form>

	
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>