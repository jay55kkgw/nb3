<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">

<style>
	.Line-break{
		text-align-last: justify;
		text-justify:distribute-all-lines;
		width: 100px;
	}
</style>

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});

	// 畫面初始化
	function init() {
		$("#downpdf").click(function(e){
			$("#templatePath").val("/pdfTemplate/digital_deposit.html");
			$("#formId").attr("action","${__ctx}/NT/ACCT/downloadDigitalDepositPDF");
			$("#formId").submit();
		})
	}

</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
	<!-- 帳戶查詢 -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Account_Inquiry" /></li>
    <!-- 數位存款帳戶存摺封面下載     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X2455" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
<%-- 				<h2><spring:message code="LB.Virtual_Account_Detail" /></h2> --%>
				<h2><spring:message code="LB.X2455" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form method="post" id="formId" action="">
					<input type="hidden" id="SDTformat" name="SDTformat" value="${digital_deposit_result.data.SDTformat}"/>
					<input type="hidden" id="NAME" name="NAME" value="${digital_deposit_result.data.NAME}"/>
					<input type="hidden" id="TAXGVID" name="TAXGVID" value="${digital_deposit_result.data.TAXGVID}"/>
					<input type="hidden" id="BRANCH" name="BRANCH" value="${digital_deposit_result.data.BRANCH}"/>
					<input type="hidden" id="ACNO" name="ACNO" value="${digital_deposit_result.data.ACNO}"/>
					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-input-block" style="overflow: auto;">
								<div class="ttb-input-item row" style="background-image: url('${__ctx}/img/digital_deposit_demo.png'); width:931px; height:514px;">
									<table style="margin-top: 22%; margin-left: 11%;">
										<thead>
										
										</thead>
										<tbody style="color: white">
											<tr>
												<td class="Line-break">開戶日期</td>
												<td>：${digital_deposit_result.data.SDTformat}</td>
											</tr>
											<tr>
												<td class="Line-break">戶名</td>
												<td style="text-indent: -16px; padding-left: 16px; width: 425;">：${digital_deposit_result.data.NAME}</td>
											</tr>
											<tr>
												<td>　</td>
												<td>　</td>
											</tr>
											<tr>
												<td class="Line-break">銀行代號</td>
												<td>：050</td>
											</tr>
											<tr>
												<td class="Line-break">分支機構代號</td>
												<td>：${digital_deposit_result.data.TAXGVID}（${digital_deposit_result.data.BRANCH}）</td>
											</tr>
											<tr>
												<td class="Line-break">帳號</td>
												<td>：${digital_deposit_result.data.ACNO}</td>
											</tr>
										</tbody>
									</table>
								</div>
  							</div>
<%--   							<input id="CMRESET" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" />  --%>
							<input id="downpdf" type="button" class="ttb-button btn-flat-orange" value="<spring:message code='LB.X2037' />" />
  						</div>
  					</div>
  				</form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>