<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
</head>
 <body>
	
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 質借功能取消     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Pledged_Time_Deposit_Function_Application_Cancellation" /></li>
		</ol>
	</nav>



		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 --> 
 		<div class="content row">
 		   <%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
 
 		
 		<main class="col-12">	
		
		<!-- 		主頁內容  -->

			<section id="main-content" class="container">
	
			<h2><spring:message code="LB.Pledged_Time_Deposit_Function_Application_Cancellation" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<div id="step-bar">
					<ul>
						<!-- 輸入資料 -->
						<li class="active"><spring:message code="LB.Enter_data" /></li>
						<!-- 確認資料 -->
						<li class=""><spring:message code="LB.Confirm_data" /></li>
						<!-- 交易完成 -->
						<li class=""><spring:message code="LB.Transaction_complete" /></li>
					</ul>
				</div>
			<div class="main-content-block row">
				<div class="col-12 tab-content">
				<form method="post" id="formId" action="${__ctx}/NT/ACCT/collateral_confirm">
					<input type="hidden" id="TOKEN" name="TOKEN" value="${sessionScope.transfer_confirm_token}" /><!-- 防止重複交易 -->
					<div class="ttb-input-block">
						<!-- 帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
								<h4><spring:message code="LB.Account" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
						           		<select name="FDPACN" id="FDPACN" class="select-input validate[required]">
												<option value=""><spring:message code="LB.Select_account" /></option>
												
										</select>
									</div>
								</span>
							</div>
					</div>
											
					</form>
<%-- 					<input id="reset" name="reset" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" /> --%>
						<input id="pageshow" name="pageshow" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />	
					</div>
				</div>
						<div class="text-left">
							<spring:message code="LB.Description_of_page" />
							<ol class="list-decimal text-left">
								<li><spring:message code="LB.collateral_P1_D1" /></li>
								<li><spring:message code="LB.collateral_P1_D2" /></li>
							</ol>
						</div>
<!-- 				</form> -->
			</section>
			<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->
    <%@ include file="../index/footer.jsp"%>
	<!--   Js function -->
    <script type="text/JavaScript">

    $(document).ready(function() {
    		// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 50);
			// 開始跑下拉選單並完成畫面
			setTimeout("init()", 400);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
    
    function init(){
    	$("#formId").validationEngine({binded:false,promptPosition: "inline" });
    	$("#pageshow").click(function(e){			
				console.log("submit~~");
				if(!$('#formId').validationEngine('validate')){
		        	e.preventDefault();
	 			}else{
	 				$("#formId").validationEngine('detach');
	 				initBlockUI();
	 				$("form").submit();
	 			}		
		});
    	creatAcn();
    }
    
    function creatAcn(){
		var options = { keyisval:true ,selectID:'#FDPACN'};
		
		uri = '${__ctx}'+"/NT/ACCT/getCollateralAcn_aj"
		rdata = {type: 'collater' };
		
		console.log("creatAcn.uri: " + uri);
		console.log("creatAcn.rdata: " + rdata);
		
		data = fstop.getServerDataEx(uri,rdata,false);

		console.log("creatAcn.data: " + JSON.stringify(data));
		
		if(data !=null && data.result == true ){
			fstop.creatSelect( data.data, options);
		}
	}
    
	
 	</script>

</body>
</html>
