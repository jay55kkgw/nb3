<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust:exact">
	<br/><br/>
	<div style="text-align:center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/>
	</div>
	<br/><br/><br/>
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<table class="print">
		<tr>
			<td><spring:message code="LB.Trading_time"/></td>
		 	<td>${CMQTIME}</td>
		</tr>
		<tr>
			<td><spring:message code="LB.Id_no"/></td>
		 	<td>${hiddenCUSIDN}</td>
		</tr>
		<tr>
			<td><spring:message code="LB.Name"/></td>
		 	<td>${hiddenNAME}</td>
		</tr>
		<tr>
  			<td><spring:message code= "LB.W1111" /></td>
			<td>${hiddenCDNO}</td>
		</tr>
		<tr>
  			<td><spring:message code= "LB.W0025" /></td>
			<td>（${TRANSCODE}）${FUNDLNAME}</td>            
		</tr>
		<tr>
			<td><spring:message code= "LB.W1140" /></td>
			<td>${BILLSENDMODEChinese}</td>
		</tr>
		<tr>
			<td><spring:message code= "LB.W0979" /></td>
			<td>${UNITFormat}</td>
		</tr>
		<tr>
			<td><spring:message code= "LB.W0135" /></td>
			<td>${FUNDACN}</td>
		</tr>
		<tr>
			<td><spring:message code= "LB.W1143" /></td>
			<td>${TRADEDATEFormat}</td>
		</tr>
		<tr> 
			<td><spring:message code= "LB.W0978" /></td>
			<td>
				${ADCCYNAME}${FUNDAMTFormat}
			</td>
		</tr>
	</table>
	<div>
		<p style="text-align:left">
			<spring:message code= "LB.X1508" /><br/>
			<spring:message code= "LB.X1509" />
		</p>
	</div>
</body>
</html>