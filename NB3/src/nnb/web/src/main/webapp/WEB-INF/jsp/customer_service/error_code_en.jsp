<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript">

	
	
</script>
<div class="card-detail-block" id="err1">
	<div class="card-center-block">
<!-- 		<h2>中心主機</h2> -->
		<table class="stripe table table-striped ttb-table dtable1" data-toggle-column="first">
			<thead>
				<tr>
					<th><spring:message code="LB.error_code" /></th>
					<th><spring:message code="LB.ErrMsg" /></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="dataList" items="${error_code}" >
					<tr>
						<td>${dataList.ADMCODE}</td>
						<td>${dataList.ADMSGIN}</td>
					</tr>

				</c:forEach>
			</tbody>
		</table>
	</div>
</div>
<div class="card-detail-block" id="err2">
	<div class="card-center-block">
<!-- 		<h2>瀏覽器元件</h2> -->
		<table class="stripe table table-striped ttb-table dtable2" data-toggle-column="first">
			<thead>
				<tr>
					<th><spring:message code="LB.error_code" /></th>
					<th><spring:message code="LB.ErrMsg" /></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						E001
					</td>
					<td>
						Card Reader Failed to Detect.
					</td>
				</tr>
				
				<tr>
					<td>
						E002
					</td>
					<td>
						Unable to Connect to The Card Reader Driver.
					</td>
				</tr>
				
				<tr>
					<td>
						E003
					</td>
					<td>
						Card Detection Failed.
					</td>
				</tr>
				
				<tr>
					<td>
						E004
					</td>
					<td>
						The system is abnormal, please remove the IC card and reinsert it and try again.
					</td>
				</tr>
				
				<tr>
					<td>
						E005
					</td>
					<td>
						Not TBB IC Card/password was entered incorrectly, or the password verification failed.
					</td>
				</tr>
				
				<tr>
					<td>
						E005,69F0, 0
					</td>
					<td>
						Confirm password operation error
					</td>
				</tr>
					
				<tr>
					<td>
						E006
					</td>
					<td>
						Unable to get card issuer information.
					</td>
				</tr>
				
				<tr>
					<td>
						E007
					</td>
					<td>
						Card Password Cannot be Blank.
					</td>
				</tr>
				
				<tr>
					<td>
						E007|69F0
					</td>
					<td>
						Password input operation error
					</td>
				</tr>
				
				<tr>
					<td>
						E007|69F0,0
					</td>
					<td>
						Confirm password operation error
					</td>
				</tr>
				
				<tr>
					<td>
						E007|6610
					</td>
					<td>
						Password input error
					</td>
				</tr>
				
				<tr>
					<td>
						E008
					</td>
					<td>
						Card Password Error.
					</td>
				</tr>
				
				<tr>
					<td>
						E009
					</td>
					<td>
						Card has been Locked.
					</td>
				</tr>
				
				<tr>
					<td>
						E010
					</td>
					<td>
						Failed to get the issuer code.
					</td>
				</tr>
				
				<tr>
					<td>
						E011
					</td>
					<td>
						Failed to Get Account Data.
					</td>
				</tr>
					
				<tr>
					<td>
						E012
					</td>
					<td>
						Please confirm whether IC ATM Card  is inserted correctly.
					</td>
				</tr>
				<tr>
					<td>
						E013
					</td>
					<td>
						Failed to Get Transaction Serial Number and TAC Code.
					</td>
				</tr>
				
				<tr>
					<td>
						E014
					</td>
					<td>
						You change a different IC card when you pull out the chip financial card. The transaction is cancelled. If you want to try again, please press the OK button again.
					</td>
				</tr>
				
				<tr>
					<td>
						E015
					</td>
					<td>
						The transaction will be cancelled if the chip financial card is pulled out and inserted after the timeout expires. If you want to try again, please press the OK button again.
					</td>
				</tr>
				
				<tr>
					<td>
						E016
					</td>
					<td>
						When you clicked the [Cancel] button while pulling out the chip financial card, the transaction was cancelled. If you want to try again, please press the OK button again.
					</td>
				</tr>
				
				<tr>
					<td>
						E017
					</td>
					<td>
						The system is abnormal, please close the browser and redo.
					</td>
				</tr>
					
				<tr>
					<td>
						E018
					</td>
					<td>
						The system is abnormal, please close the browser and redo.
					</td>
				</tr>
				<tr>
					<td>
						E019
					</td>
					<td>
						The system is abnormal, please close the browser and redo.
					</td>
				</tr>
					
				<tr>
					<td>
						E020
					</td>
					<td>
						The system is abnormal, please close the browser and redo.
					</td>
				</tr>
				<tr>
					<td>
						E021
					</td>
					<td>
						The transfer account verification error, please re-enter and try again.
					</td>
				</tr>
					
				<tr>
					<td>
						E022
					</td>
					<td>
						The system is abnormal, please close the browser and redo.
					</td>
				</tr>
				<tr>
					<td>
						E023
					</td>
					<td>
						The system is abnormal, please close the browser and redo.
					</td>
				</tr>
					
				<tr>
					<td>
						E024
					</td>
					<td>
						Password change failed.
					</td>
				</tr>
				<tr>
					<td>
						E025
					</td>
					<td>
						The original password was entered incorrectly.
					</td>
				</tr>
				<tr>
					<td>
						E999
					</td>
					<td>
						The Card Reader Control Object Loading Failed.
					</td>
				</tr>
				<tr>
					<td>
						E_Send_11_OnError_1006
					</td>
					<td>
						Component not Yet Installed.
					</td>
				</tr>
				<tr>
					<td>
						0x8010001B
					</td>
					<td>
						The reader driver did not produce a unique reader name.讀卡機驅動程式未產生唯一的讀卡機名稱。
					</td>
				</tr>
				
				<tr>
					<td>
						0x80100003
					</td>
					<td>
						The supplied handle was not valid.讀卡機裝置控制失效。
					</td>
				</tr>
				
				<tr>
					<td>
						0x80100027
					</td>
					<td>
						Access is denied to the file.讀取IC卡片檔錯誤。
					</td>
				</tr>
				
				<tr>
					<td>
						0x80100006
					</td>
					<td>
						Not enough memory available to complete this command.記憶空間不足。
					</td>
				</tr>
				
				<tr>
					<td>
						0x8010002E
					</td>
					<td>
						No smart card reader is available.未偵測到讀卡機。
					</td>
				</tr>
					
				<tr>
					<td>
						0x8010000C
					</td>
					<td>
						The operation requires a smart card, but no smart card is currently in the device.讀卡機裝置已離線。
					</td>
				</tr>
				
				<tr>
					<td>
						0x8010000B
					</td>
					<td>
						The smart card cannot be accessed because of other outstanding connections.讀卡機正被其他程式獨佔中。
					</td>
				</tr>
					
				<tr>
					<td>
						0x80100066
					</td>
					<td>
						The smart card is not responding to a reset.IC卡片重置無回應。
					</td>
				</tr>
				
				<tr>
					<td>
						0x80100068
					</td>
					<td>
						The smart card was reset.IC卡片重置。
					</td>
				</tr>
				
				<tr>
					<td>
						0x80100069
					</td>
					<td>
						The smart card has been removed, so further communication is not possible.IC卡片移出讀卡機，無法連接。
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<div class="card-detail-block" id="err3">
	<div class="card-center-block">
<!-- 		<h2>電子簽章</h2> -->
		<table class="stripe table table-striped ttb-table dtable3" data-toggle-column="first">
			<thead>
				<tr>
					<th><spring:message code="LB.error_code" /></th>
					<th><spring:message code="LB.ErrMsg" /></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>3202</td>
					<td>Certificate not found</td>
				</tr>
				<tr>
					<td>3206</td>
					<td>Get certificate fail</td>
				</tr>
				<tr>
					<td>3209</td>
					<td>Certificate is expired</td>
				</tr>
				<tr>
					<td>3210</td>
					<td>The certificate is not yet valid</td>
				</tr>
				<tr>
					<td>3211</td>
					<td>No valid certificate ! The certificate is either expired or not yet valid</td>
				</tr>
				<tr>
					<td>5001</td>
					<td>General error</td>
				</tr>
				<tr>
					<td>5002</td>
					<td>Memory Allocation Error</td>
				</tr>
				<tr>
					<td>5003</td>
					<td>Buffer too small</td>
				</tr>
				<tr>
					<td>5004</td>
					<td>Function not support</td>
				</tr>
				<tr>
					<td>5005</td>
					<td>Invalid parameter</td>
				</tr>
				<tr>
					<td>5006</td>
					<td>Invalid handle</td>
				</tr>
				<tr>
					<td>5007</td>
					<td>TrialVersion Library is expired</td>
				</tr>
				<tr>
					<td>5008</td>
					<td>Base64 Encoding/Decoding Error</td>
				</tr>
				<tr>
					<td>5010</td>
					<td>Certificate not found in MS CryptoAPI Database</td>
				</tr>
				<tr>
					<td>5011</td>
					<td>Certificate is expired</td>
				</tr>
				<tr>
					<td>5012</td>
					<td>Certificate can not used now</td>
				</tr>
				<tr>
					<td>5013</td>
					<td>Some certificates are expired, some can not be used now</td>
				</tr>
				<tr>
					<td>5014</td>
					<td>Certificate subject not match</td>
				</tr>
				<tr>
					<td>5015</td>
					<td>Unable to find certificate issuer</td>
				</tr>
				<tr>
					<td>5016</td>
					<td>Certificate signature is invlaid</td>
				</tr>
				<tr>
					<td>5017</td>
					<td>Invalid ertificate keyusage</td>
				</tr>
				<tr>
					<td>5020</td>
					<td>Certificate is revoked</td>
				</tr>
				<tr>
					<td>5021</td>
					<td>Certificate is revoke (key compromised)</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>


