<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<!--舊版驗證-->
<script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	
	$("#okButton").click(function(){
		if(!CheckRadio("基金公開說明書交付方式","FDPUBLICTYPE")){
			return false;
		}
		var XFLAGV = $('input[name=FDPUBLICTYPE]:checked').val();
		$("#XFLAG").val(XFLAGV);
		
		var TXID = "${result_data.data.TXID}";
		
		if(TXID == "C021"){
			$("#formID").attr("action","${__ctx}/FUND/TRANSFER/fund_fee_expose");
		}
		else if(TXID == "C016"){
			$("#formID").attr("action","${__ctx}/FUND/PURCHASE/fund_purchase_fee_expose");
		}
		else if(TXID == "C017"){
			$("#formID").attr("action","${__ctx}/FUND/REGULAR/fund_regular_fee_expose");
		}
		else if(TXID == "C031"){
			$("#formID").attr("action","${__ctx}/FUND/RESERVE/PURCHASE/fund_reserve_purchase_fee_expose");
		}
		else if(TXID == "C032"){
			$("#formID").attr("action","${__ctx}/FUND/RESERVE/TRANSFER/fund_reserve_transfer_fee_expose");
		}
		$("#formID").submit();
	});
	$("#cancelButton").click(function(){
		var TXID = "${result_data.data.TXID}";
		
		if(TXID == "C021" || TXID == "C032"){
			$("#formID").attr("action","${__ctx}/FUND/TRANSFER/query_fund_transfer_data");
		}
		else if(TXID == "C016"){
			$("#formID").attr("action","${__ctx}/FUND/PURCHASE/fund_purchase_select");
		}
		else if(TXID == "C017"){
			$("#formID").attr("action","${__ctx}/FUND/REGULAR/fund_regular_select");
		}
		else if(TXID == "C031"){
			$("#formID").attr("action","${__ctx}/FUND/RESERVE/PURCHASE/fund_reserve_purchase_select");
		}
		$("#formID").submit();
	});
});
</script>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	<!--麵包屑-->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><a href="#"><spring:message code= "LB.Funds" /></a></li>
			<li class="ttb-breadcrumb-item"><a href="#"><spring:message code= "LB.W1064" /></a></li>
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code= "LB.X1785" /></li>
		</ol>
	</nav>
	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
				<h2><spring:message code= "LB.X1785" /></h2>
					<form id="formID" method="post">
						<input type="hidden" name="TXID" value="${result_data.data.TXID}"/>
						<input type="hidden" name="TRANSCODE" value="${result_data.data.TRANSCODE}"/>
						<input type="hidden" name="CDNO" value="${result_data.data.CDNO}"/>
						<input type="hidden" name="UNIT" value="${result_data.data.UNIT}"/>
						<input type="hidden" name="BILLSENDMODE" value="${result_data.data.BILLSENDMODE}"/>
						<input type="hidden" name="SHORTTRADE" value="${result_data.data.SHORTTRADE}"/>
						<input type="hidden" name="SHORTTUNIT" value="${result_data.data.SHORTTUNIT}"/>
						<input type="hidden" name="INTRANSCODE" value="${result_data.data.INTRANSCODE}"/>
						<input type="hidden" name="INFUNDSNAME" value="${result_data.data.INFUNDSNAME}"/>
						<input type="hidden" name="FUNDAMT" value="${result_data.data.FUNDAMT}"/>
						<input type="hidden" name="RRSK" value="${result_data.data.RRSK}"/>
						<input type="hidden" name="RSKATT" value="${result_data.data.RSKATT}"/>
						<input type="hidden" name="GETLTD" value="${result_data.data.GETLTD}"/>
						<input type="hidden" name="RISK7" value="${result_data.data.RISK7}"/>
						<input type="hidden" name="GETLTD7" value="${result_data.data.GETLTD7}"/>
						<input type="hidden" name="FDINVTYPE" value="${result_data.data.FDINVTYPE}"/>
						<input type="hidden" name="FDAGREEFLAG" value="${result_data.data.FDAGREEFLAG}"/>
						<input type="hidden" name="FDNOTICETYPE" value="4"/>
						<input type="hidden" id="XFLAG" name="XFLAG" value="">
						<input type="hidden" name="ACN1" value="${result_data.data.ACN1}"/>
						<input type="hidden" name="ACN2" value="${result_data.data.ACN2}"/>
						<input type="hidden" name="HTELPHONE" value="${result_data.data.HTELPHONE}"/>
						<input type="hidden" name="COUNTRYTYPE" value="${result_data.data.COUNTRYTYPE}"/>
						<input type="hidden" name="COUNTRYTYPE1" value="${result_data.data.COUNTRYTYPE1}"/>
						<input type="hidden" name="CUTTYPE" value="${result_data.data.CUTTYPE}"/>
						<input type="hidden" name="BRHCOD" value="${result_data.data.BRHCOD}"/>
						<input type="hidden" name="RISK" value="${result_data.data.RISK}"/>
						<input type="hidden" name="SALESNO" value="${result_data.data.SALESNO}"/>
						<input type="hidden" name="PRO" value="${result_data.data.PRO}"/>
						<input type="hidden" name="TYPE" value="${result_data.data.TYPE}"/>
						<input type="hidden" name="COMPANYCODE" value="${result_data.data.COMPANYCODE}"/>
						<input type="hidden" name="AMT3" value="${result_data.data.AMT3}"/>
						<input type="hidden" name="YIELD" value="${result_data.data.YIELD}"/>
						<input type="hidden" name="STOP" value="${result_data.data.STOP}"/>
						<input type="hidden" name="FUNDLNAME" value="${result_data.data.FUNDLNAME}"/>
						<input type="hidden" name="PAYTYPE" value="${result_data.data.PAYTYPE}"/>
						<input type="hidden" name="FUNDACN" value="${result_data.data.FUNDACN}"/>
						<input type="hidden" name="MIP" value="${result_data.data.MIP}"/>
						<input type="hidden" name="PAYDAY1" value="${result_data.data.PAYDAY1}"/>
						<input type="hidden" name="PAYDAY2" value="${result_data.data.PAYDAY2}"/>
						<input type="hidden" name="PAYDAY3" value="${result_data.data.PAYDAY3}"/>
						<input type="hidden" name="PAYDAY4" value="${result_data.data.PAYDAY4}"/>
						<input type="hidden" name="PAYDAY5" value="${result_data.data.PAYDAY5}"/>
						<input type="hidden" name="PAYDAY6" value="${result_data.data.PAYDAY6}"/>
						<input type="hidden" name="PAYDAY7" value="${result_data.data.PAYDAY7}"/>
						<input type="hidden" name="PAYDAY8" value="${result_data.data.PAYDAY8}"/>
						<input type="hidden" name="PAYDAY9" value="${result_data.data.PAYDAY9}"/>
						<input type="hidden" name="INVTYPE" value="${result_data.data.INVTYPE}"/>
						<input type="hidden" name="FEE_TYPE" value="${result_data.data.FEE_TYPE}"/>
						<input type="hidden" name="SLSNO" value="${result_data.data.SLSNO}"/>
						<input type="hidden" name="KYCNO" value="${result_data.data.KYCNO}"/>
						<input type="hidden" name="SAL01" value="${result_data.data.SAL01}"/>
						<input type="hidden" name="SAL03" value="${result_data.data.SAL03}"/>
						<input type="hidden" name="FUNDT" value="${result_data.data.FUNDT}"/>
						
						<div class="main-content-block row">
							<div class="col-12">
								<div class="ttb-message">
									<p>公開說明書／投資人須知　交付確認</p>
								</div>
								 <ul class="ttb-result-list" style="list-style: decimal; list-style-position: inside;">
								 	<strong style="font-size:15px;color:red;">為維護您的權益，每次投資基金時請先詳閱下列文件：</strong>
								 	<li class="full-list"><strong style="font-size:15px;color:red;">國內基金：基金公開說明書。</strong></li>
								 	<li class="full-list"><strong style="font-size:15px;color:red;">國外基金：基金公開說明書、投資人須知。並瞭解該基金之投資內容、風險、各項收費標準及其他相關規定（如：洗錢防制、短線交易等）</strong></li>
								 	<strong style="font-size:15px;color:red;">您可透過本行「<a style="color: blue;" target="_blank" href="https://tbb.moneydj.com/w/CustFundIDMap.djhtm?A=${result_data.data.TRANSCODE}&B=01">基金理財網站</a>」，下載取得各基金財務報告書、公開說明書、投資人須知或短線交易規定。</strong>
								 </ul>
								 <br>
									<p><strong style="font-size:15px;">本基金公開說明書／投資人須知取得方式（請投資人勾選確認下列一種）：</strong></p>
									<div class="ttb-input-block">
									<div class="ttb-input-item row">
		                                <span style="flex: 0 0 25%;">
		                                </span>
		                                <span class="input-block">
		                                    <div class="ttb-input">
		
		                                        <label class="radio-block">
													<input type="radio" name="FDPUBLICTYPE" value="1"/><strong style="font-size:15px">已取得，本次無須交付。</strong>
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" name="FDPUBLICTYPE" value="2"/><strong style="font-size:15px">已由經理公司交付或銷售機構取得。</strong>
													<span class="ttb-radio"></span>
												</label>
		                                    </div>
		                                </span>
		                            </div>
		                            </div>
									
		                			<input type="button" id="cancelButton" value="<spring:message code="LB.Cancel"/>" class="ttb-button btn-flat-gray"/>	
		                			<input type="button" id="okButton" value="<spring:message code="LB.Confirm"/>" class="ttb-button btn-flat-orange"/>
							</div>
					</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>