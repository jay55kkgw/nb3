<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %> 
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>

	<script type="text/javascript">
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		
		// 畫面初始化
		function init() {

	    	//列印
        	$("#PRINT").click(function(){
				var params = {
					"jspTemplateName":"modify_digital_data_result_print",
					//修改數位存款帳戶資料
					"jspTitle":' <spring:message code= "LB.D1474" />',
					"errMsgStr":"${result_data.data.errMsgStr}",
					"BRHNAME":"${result_data.data.BRHNAME}",
					"BRHTEL":"${result_data.data.BRHTEL}"
				};
				openWindowWithPost("${__ctx}/ONLINE/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
			$("#GOBAKE").click(function(e){
	        	var action = '${__ctx}/login';
				$("form").attr("action", action);
    			$("form").submit();
			});
	    	
		}
		
	</script>
	<style>
	
			@media screen and (max-width:767px) {
				.ttb-button {
						width: 32%;
						left: 0px;
				}
				#CMBACK.ttb-button{
						border-color: transparent;
						box-shadow: none;
						color: #333;
						background-color: #fff;
				}
			}
	</style>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 修改數位存款帳戶資料     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1474" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
			<!-- 修改數位存款帳戶資料 -->
				<h2>
					 修改開戶資料
				</h2>
				<div id="step-bar">
                    <ul>
                        <li class="finished">身分驗證</li>
                        <li class="finished">開戶資料</li>
                        <li class="finished">確認資料</li>
						<li class="active">完成修改</li>
                    </ul>
                </div>
				<form method="post" id="formId" name="formId">
					<input type="hidden" name="TYPE" value="5">
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12 terms-block">
							<div class="ttb-message">
                            <p>完成修改</p>
                       		 </div>
							
							<div class="text-left">
								<!-- 親愛的客戶您好: -->
									<p><spring:message code= "LB.X0152" />:</p>
									<!-- 謝謝您完成修改數位存款帳戶資料交易， -->
									<p>謝謝您完成修改數位存款帳戶資料交易。<br> 
									<!-- 若有任何問題，您可撥電話至您所指定服務分行： -->
									<!-- (聯絡電話： XXX ) 詢問我們將竭誠為您服務 -->
									<spring:message code= "LB.X0221" /><B>${result_data.data.BRHNAME}(<spring:message code= "LB.X0222" />${result_data.data.BRHTEL})</B><spring:message code= "LB.X0223" /></p> 
									<!-- 祝您事業順心，萬事如意! -->
									<p><spring:message code= "LB.X0224" /></p>
							</div>
							<!-- 臺灣企銀   敬上 -->
							<div class="text-right">
								 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<spring:message code= "LB.W0605" />&nbsp;&nbsp;&nbsp;<spring:message code= "LB.X0193" /></font>
							</div>    
							
<!-- 							<div class="text-left"> -->
<%-- 								${result_data.data.errMsgStr} --%>
<!-- 							</div> -->
							<input type="button" id="PRINT" value="<spring:message code="LB.Print" />" class="ttb-button btn-flat-gray" />
							<!-- 確定 -->
<!-- 列印 -->					<input type="button" id="GOBAKE" value="回首頁" class="ttb-button btn-flat-orange"/>
							<input type="button" id="CLOSE" value="繼續其他申請" class="ttb-button btn-flat-orange"  onClick="window.close();"/>
						</div>
					</div>
						
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>