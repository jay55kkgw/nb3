<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<!-- DIALOG會用到 -->
	<script type="text/javascript" src="${__ctx}/js/jquery-ui.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery-ui.css">
	<!-- N361交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/checkIdProcess.js"></script>
	<style>
	 .ui-dialog-titlebar-close {
	    display: none;
	 }
	</style>
	<script type="text/javascript">
		<!-- 不驗證是否是IKey使用者 -->
		var notCheckIKeyUser = true;
		var myobj = null; // 自然人憑證用
		var urihost = "${__ctx}";
	
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 初始化驗證碼
			setTimeout("initKapImg()", 200);
			// 生成驗證碼
			setTimeout("newKapImg()", 300);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		
		// 畫面初始化
		function init() {
			// 表單驗證初始化
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
			// 確認鍵 click
			goOn();
			// 回上一頁
			goBack();
			// 關閉視窗
			closePage();
			// 判斷顯不顯示驗證碼
			chaBlock();
			// 交易機制 click
			fgtxwayClick();
			// KYC彈出視窗click
			dialogClick();
			
		}
		
		// 確認鍵 Click
		function goOn() {
			$("#CMSUBMIT").click( function(e) {
				// 送出進表單驗證前將span顯示
				$("#hideblock").show();
				
				console.log("submit~~");
	
				// 表單驗證
				if ( !$('#formId').validationEngine('validate') ) {
					e.preventDefault();
				} else {
					// 解除表單驗證
					$("#formId").validationEngine('detach');
					
					// 通過表單驗證準備送出
					processQuery();
				}
			});
		}
		
		function dialogClick() {
			var w = $(window).width() < 850 ? $(window).width() : 850 ;
			$("#agreeDialog").dialog({
				autoOpen:false,
				modal:true,
				closeOnEscape:false,
				width:w,
				height:250,
				position:{
					my:"center",at:"top",of:window
				}
			});
			//此處是同意
			$("#CMSUBMIT_Y").click(function() {
				$("#AGREE").val("Y");
				$("#DEVNAME").val(getOSAndBrowser());
				$("#formId").submit();
				$("#agreeDialog").dialog("close");
				initBlockUI();
			});
			//此處是不同意
			$("#CMSUBMIT_N").click(function() {
				$("#AGREE").val("N");
				$("#DEVNAME").val(getOSAndBrowser());
				$("#formId").submit();
				$("#agreeDialog").dialog("close");
				initBlockUI();
			});
		}
		
		
		// 通過表單驗證準備送出
		function processQuery() {
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
			// 交易機制選項
			switch(fgtxway) {
				case '1':
					// IKEY
					useIKey();
					
					break;
				case '2':
					// 晶片金融卡
					console.log("urihost: " + urihost);
					useCardReader();
					
					break;
				default:
					//alert("<spring:message code= "LB.Alert001" />");//<!-- 請選擇交易機制 -->
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert001' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			}
		}
		
		// 回上一頁
		function goBack() {
			$("#CMBACK").click( function(e) {
				// 解除表單驗證
				$("#formId").validationEngine('detach');
				
				var url = "${__ctx}" + "${previous}";
				$("#formId").attr("action", url);
				// 讓Controller知道是回上一頁
				$("#formId").append('<input type="hidden" name="back" value="Y" />');
				$("#formId").submit();
			});
		}
		
		// 關閉視窗
		function closePage() {
			$("#CLOSEPAGE").click( function(e) {
				window.close();
			});			
		}
		
		// 使用者選擇晶片金融卡要顯示驗證碼區塊
		function fgtxwayClick() {
			$('input[name="FGTXWAY"]').change(function(event) {
				// 判斷交易機制決定顯不顯示驗證碼區塊
				chaBlock();
			});
		}
		// 判斷交易機制決定顯不顯示驗證碼區塊
		function chaBlock() {
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
			// 交易機制選項
			if(fgtxway == '2'){
				// 若為晶片金融卡才顯示驗證碼欄位
				$("#chaBlock").show();
			}else{
				// 若非晶片金融卡則隱藏驗證碼欄位
				$("#chaBlock").hide();
			}
		}

		// 驗證碼刷新
		function changeCode() {
			$('input[name="capCode"]').val('');
			// 大小版驗證碼用同一個
			console.log("changeCode...");
			$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

			// 登入失敗解遮罩
			unBlockUI(initBlockId);
		}
		
		// 初始化驗證碼
		function initKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
		}
		
		// 生成驗證碼
		function newKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').click(function() {
				$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
			});
		}
		
		//取得卡片主帳號結束
		function getMainAccountFinish(result){
			//成功
			if(result != "false"){
				var main = document.getElementById("formId");
				main.ACNNO.value = result;
				var cardACN = result;
				if(cardACN.length > 11){
					cardACN = cardACN.substr(cardACN.length - 11);
				}
				
//				var x = { method: 'get', parameters: '', onComplete: CheckIdResult };	
//				var myAjax = new Ajax.Request( "simpleform?trancode=N953&TXID=N953&ACN=" + cardACN, x);
				
				var cusidn = '${sessionScope.cusidn_n361}';
				var uri = urihost+"/COMPONENT/component_checkid_aj";
				var rdata = { CUSIDN: cusidn, ACN: cardACN };
				
				fstop.getServerDataEx(uri, rdata, true, CheckIdResult);
			}
			//失敗
			else{
				showTempMessage(500,"晶片金融卡身份查驗","","MaskArea",false);
			}
		}
		//複寫checkIdProcess.js
		function generateTAC2Finish(result){
			//成功
			if(result != "false"){
				var TACData = result.split(",");
				
				var main = document.getElementById("formId");
				main.ICSEQ1.value = TACData[1];
				main.TAC.value = TACData[2];
				
				var ACN_Str1 = main.ACNNO.value;
				if(ACN_Str1.length > 11){
					ACN_Str1 = ACN_Str1.substr(ACN_Str1.length - 11);
				}
			
				main.CHIP_ACN.value = ACN_Str1;
				main.OUTACN.value = ACN_Str1;
				main.ACNNO.value = ACN_Str1;
				
				checkIdBlockId = initBlockUI();
				var uri = "${__ctx}/ONLINE/APPLY/online_apply_insert_hist";
				var rdata2 = $("#formId").serializeArray();
				data2 = fstop.getServerDataEx(uri,rdata2,false);
				if(data2 !=null && data2.result == true ){
					$("#FDHISTID").val(data2.data.FDHISTID);
					$("#FDSCORE").val(data2.data.iScore);
					$("#FDINVTYPE").val(data2.data.FDINVTYPE);
					console.log("Insert HIS result ok , FDHISTID :"+$("#FDHISTID").val());
					if('1'==data2.data.FDINVTYPE){
						//積極型
						$("#FDINVTYPEShow").html("<spring:message code='LB.D0944' />"+":"+"<spring:message code='LB.D0945' />");
					}else if ("2"==data2.data.FDINVTYPE){
						//穩健型
						$("#FDINVTYPEShow").html("<spring:message code='LB.D0944' />"+":"+"<spring:message code='LB.X1766' />");
					}else{
						//保守型
						$("#FDINVTYPEShow").html("<spring:message code='LB.D0944' />"+":"+"<spring:message code='LB.X1767' />");
					}
					$("#kyc_insert_uuid").val(data2.data.kyc_insert_uuid);
					$("#agreeDialog").dialog("open");
					unBlockUI(checkIdBlockId);
				}else{
					if(data2.msgCode != null && data2.msgCode == "FE0019"){
						var message = data2.message;
						alert(message);
						unBlockUI(checkIdBlockId);
						// 複寫errorBtn1 事件
						/* $("#errorBtn1").click(function(e) {
							$("#formId").validationEngine('detach');
					    	var action = '${__ctx}/INDEX/index';
							$("#formId").attr("action", action);
							$("#formId").submit();
						}); */
					}else{
						alert(data2.msgCode + "：" + data2.message);
					}
					return false;
					//insert DB wrong
				}
			}
			//失敗
			else{
				FinalSendout("MaskArea",false);
			}
		}
		
		//IKEY簽章結束
		function uiSignForPKCS7Finish(result){
			var main = document.getElementById("main");
			
			//成功
			if(result != "false"){
				//SubmitForm();
				checkxmlcn();
			}
			//失敗
			else{
				alert("IKEY簽章失敗");
				ShowLoadingBoard("MaskArea",false);
			}
		}
		
		function checkxmlcn(){
			var cusidn = '${sessionScope.cusidn_n361}';
			var jsondc = $('#jsondc').val();
			var pkcs7Sign = $('#pkcs7Sign').val();
			var bs64 = 'Y';
			var uri = urihost+"/COMPONENT/ikey_without_login_aj";
			var rdata = { UID:cusidn, jsondc: jsondc, pkcs7Sign:pkcs7Sign ,bs64:bs64 };
			
			fstop.getServerDataEx(uri, rdata, true, IkeyCheckcallback);
		}
		
		function IkeyCheckcallback(response) {
			var checkflag = response.data.checkflag;
			if(checkflag.indexOf("SUCCESSFUL")>-1){
				SubmitForm();
			}else{
				alert("IKEY簽章驗證失敗，您使用的是非本人持有之簽章憑證");
				ShowLoadingBoard("MaskArea",false);
			}
		}
		
	////複寫checkIdProcess.js
		function SubmitForm()
		{
			FinalSendout("MaskArea",true);
			
			if(document.getElementById("CMCARD").checked == true)
			{
				//卡片押碼
				getMainAccount();
			}
			else{
				// 遮罩
//		 		initBlockUI();
				var formId = document.getElementById("formId");
				var uri = "${__ctx}/ONLINE/APPLY/online_apply_insert_hist";
				var rdata2 = $("#formId").serializeArray();
				data2 = fstop.getServerDataEx(uri,rdata2,false);
				if(data2 !=null && data2.result == true ){
					$("#FDHISTID").val(data2.data.FDHISTID);
					$("#FDSCORE").val(data2.data.iScore);
					$("#FDINVTYPE").val(data2.data.FDINVTYPE);
					console.log("Insert HIS result ok , FDHISTID :"+$("#FDHISTID").val());
					if('1'==data2.data.FDINVTYPE){
						//積極型
						$("#FDINVTYPEShow").html("<spring:message code='LB.D0944' />"+":"+"<spring:message code='LB.D0945' />");
					}else if ("2"==data2.data.FDINVTYPE){
						//穩健型
						$("#FDINVTYPEShow").html("<spring:message code='LB.D0944' />"+":"+"<spring:message code='LB.X1766' />");
					}else{
						//保守型
						$("#FDINVTYPEShow").html("<spring:message code='LB.D0944' />"+":"+"<spring:message code='LB.X1767' />");
					}
					$("#kyc_insert_uuid").val(data2.data.kyc_insert_uuid);
					$("#agreeDialog").dialog("open");
				}else{
					if(data2.msgCode != null && data2.msgCode == "FE0019"){
						var message = data2.message;
						alert(message);
						// 複寫errorBtn1 事件
						/* $("#errorBtn1").click(function(e) {
							$("#formId").validationEngine('detach');
					    	var action = '${__ctx}/INDEX/index';
							$("#formId").attr("action", action);
							$("#formId").submit();
						}); */
					}else{
						alert(data2.msgCode + "：" + data2.message);
					}
					return false;
					//insert DB wrong
				}
//		 		formId.submit();
			 }
		}
		
		function getOSAndBrowser() {  
		    var os = navigator.platform;  
		    var userAgent = navigator.userAgent;  
		    var info = "";  
		    var tempArray = "";  
		    //判斷作業系統  
		    if (os.indexOf("Win") > -1) {  
		        if (userAgent.indexOf("Windows NT 5.0") > -1) {  
		            info += "Win2000";  
		        } else if (userAgent.indexOf("Windows NT 5.1") > -1) {  
		            info += "WinXP";  
		        } else if (userAgent.indexOf("Windows NT 5.2") > -1) {  
		            info += "Win2003";  
		        } else if (userAgent.indexOf("Windows NT 6.0") > -1) {  
		            info += "WindowsVista";  
		        } else if (userAgent.indexOf("Windows NT 6.1") > -1 || userAgent.indexOf("Windows 7") > -1) {  
		            info += "Win7";  
		        } else if (userAgent.indexOf("Windows NT 6.2") > -1 || userAgent.indexOf("Windows 8") > -1) {  
		            info += "Win8";  
		        } else if (userAgent.indexOf("Windows NT 6.3") > -1 || userAgent.indexOf("Windows 8.1") > -1) {  
		            info += "Win8.1";  
		        } else if (userAgent.indexOf("Windows NT 10.0") > -1 || userAgent.indexOf("Windows 10") > -1) {  
		            info += "Win10";  
		        }  
		        else {  
		            info += "Other";  
		        }  
		    } else if (os.indexOf("Mac") > -1) {  
		        info += "Mac";  
		    } else if (os.indexOf("X11") > -1) {  
		        info += "Unix";  
		    } else if (os.indexOf("Linux") > -1) {  
		        info += "Linux";  
		    } else {  
		        info += "Other";  
		    }  
		    info += " ";  


		    //判斷瀏覽器版本  
		    var isOpera = userAgent.indexOf("Opera") > -1; //判斷是否Opera瀏覽器  
		    var isIE = userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera; //判斷是否IE瀏覽器  
		    var isEdge = userAgent.toLowerCase().indexOf("edge") > -1 && !isIE; //判斷是否IE的Edge瀏覽器  
		    var isIE11 = (userAgent.toLowerCase().indexOf("trident") > -1 && userAgent.indexOf("rv") > -1);  

		    if (/[Ff]irefox(\/\d+\.\d+)/.test(userAgent)) {  
		        tempArray = /([Ff]irefox)\/(\d+\.\d+)/.exec(userAgent);  
		        info += tempArray[1] + tempArray[2];  
		    } else if (isIE) {  

		        var version = "";  
		        var reIE = new RegExp("MSIE (\\d+\\.\\d+);");  
		        reIE.test(userAgent);  
		        var fIEVersion = parseFloat(RegExp["$1"]);  
		        if (fIEVersion == 7)  
		        { version = "IE7"; }  
		        else if (fIEVersion == 8)  
		        { version = "IE8"; }  
		        else if (fIEVersion == 9)  
		        { version = "IE9"; }  
		        else if (fIEVersion == 10)  
		        { version = "IE10"; }  
		        else  
		        { version = "0" }  

		        info += version;  

		    } else if (isEdge) {  
		        info += "Edge";  
		    } else if (isIE11) {  
		        info += "IE11";  
		    } else if (/[Cc]hrome\/\d+/.test(userAgent)) {  
		        tempArray = /([Cc]hrome)\/(\d+)/.exec(userAgent);  
		        info += tempArray[1] + tempArray[2];  
		    } else if (/[Vv]ersion\/\d+\.\d+\.\d+(\.\d)* *[Ss]afari/.test(userAgent)) {  
		        tempArray = /[Vv]ersion\/(\d+\.\d+\.\d+)(\.\d)* *([Ss]afari)/.exec(userAgent);  
		        info += tempArray[3] + tempArray[1];  
		    } else if (/[Oo]pera.+[Vv]ersion\/\d+\.\d+/.test(userAgent)) {  
		        tempArray = /([Oo]pera).+[Vv]ersion\/(\d+)\.\d+/.exec(userAgent);  
		        info += tempArray[1] + tempArray[2];  
		    } else {  
		        info += "unknown";  
		    }  
		    return info;  
		};
	</script>
</head>

<body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0847" /></li>
		</ol>
	</nav>
	<div class="content row">
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>預約開立基金戶</h2>
				<div id="step-bar">
					<ul>
						<li class="finished">身份驗證</li>
						<c:if test="${FATCA_CONSENT == 'Y'}">
							<li class="finished">FATCA個人客戶身份識別聲明</li>
						</c:if>
						<li class="finished">顧客權益</li>
						<li class="finished">申請資料</li>
						<li class="active">確認資料</li>
						<li class="">完成申請</li>
					</ul>
				</div>
				
				<form method="post" id="formId" action="${__ctx}${next}">
					<!-- 交易機制所需欄位 -->
					<input type="hidden" id="jsondc" 	name="jsondc" value='${sessionScope.transfer_data}'>
					<input type="hidden" id="ISSUER" 	name="ISSUER" />
					<input type="hidden" id="ACNNO" 	name="ACNNO" />
					<input type="hidden" id="TRMID" 	name="TRMID" />
					<input type="hidden" id="iSeqNo" 	name="iSeqNo" />
					<input type="hidden" id="ICSEQ" 	name="ICSEQ" />
					<input type="hidden" id="ICSEQ1" 	name="ICSEQ1" />
					<input type="hidden" id="TAC" 		name="TAC" />
					<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" />
					<input type="hidden" id="CHIP_ACN"  name="CHIP_ACN" />
					<input type="hidden" id="OUTACN"	name="OUTACN">
					<input type="hidden" id="AGREE" name="AGREE"/>
					<input type="hidden" id="DEVNAME" name="DEVNAME"/>
					<input type="hidden" id="FDHISTID" name="FDHISTID"/>
					<input type="hidden" id="LoginYN" name="LoginYN"/ value="N">
					<input type="hidden" id="FDINVTYPE" name="FDINVTYPE" />
					<input type="hidden" id="FDSCORE" name="FDSCORE"/>
					<input type="hidden" id="kyc_insert_uuid" name="kyc_insert_uuid"/>
    				<!--FATCA所需欄位-->
    				<input type="hidden" name="FATCA_CONSENT" value="${FATCA_CONSENT}"/>
    				<input type="hidden" name="CITY" value="${CITY}"/>
					
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<div class="ttb-message">
									<p>確認資料</p>
								</div>
								
								<div class="classification-block">
									<p>帳戶設定</p>
								</div>
	
								<!--新臺幣活期性存款帳戶-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>新臺幣活期性存款帳戶</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span class="input-title">
												<label>
													<h4>${sessionScope.n361_4.ACN_SSV }</h4>
												</label>
											</span>
										</div>
									</span>
								</div>
	
								<!--外幣活期性存款帳戶-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>外幣活期性存款帳戶</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span class="input-title">
												<label>
													<h4>${sessionScope.n361_4.ACN_FUD }</h4>
												</label>
											</span>
										</div>
									</span>
								</div>
	
	
								<div class="classification-block">
									<p>通知與報告書</p>
								</div>
								<p class="form-description">各項通知及報告書之收取方式</p>
	
								<!--E-mail-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>E-mail</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span class="input-title">
												<label>
													<h4>${sessionScope.n361_4.DPMYEMAIL }</h4>
												</label>
											</span>
										</div>
									</span>
								</div>
	
								<div class="ttb-input">
									<label class="check-block">本人同意上述各項通知及報告書於發送E-mail後視為交付。若有資料需修改，除E-mail信箱可利用一般網路銀行變更外，其餘事項於臨櫃辦理。
										<input type="checkbox" id="ACNMTHSHOW" name="ACNMTHSHOW" disabled checked/>
										<input type="hidden" id="ACNMTH" name="ACNMTH" value="Y" />
										<span class="ttb-check"></span>
									</label>
								</div>
								
								<!--推介人員行員編號-->
								<div class="classification-block">
									<p>推介人員</p>
								</div>
								
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>推介人員行員編號</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span class="input-title">
												<label>
													<h4>${sessionScope.n361_4.CTDNUM }</h4>
												</label>
											</span>
										</div>
									</span>
								</div>
								
								<div class="classification-block">
									<p>身份驗證</p>
								</div>
								
								<!-- 交易機制 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>驗證機制</h4>
										</label>
									</span>

									<!-- 交易機制選項 -->
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
												<!-- IKEY -->
												<spring:message code="LB.Electronic_signature" />
												<input type="radio" name="FGTXWAY" id="CMIKEY" value="1">
												<span class="ttb-radio"></span>
											</label>
										</div>

										<div class="ttb-input">
											<label class="radio-block">
												<!-- 晶片金融卡 -->
												<spring:message code="LB.Financial_debit_card" />
												<input type="radio" name="FGTXWAY" id="CMCARD" value="2" checked="checked">
												<span class="ttb-radio"></span>
											</label>
										</div>
									</span>
								</div>
								
								<!-- 驗證碼 -->
	                            <div class="ttb-input-item row" id="chaBlock" style="display:none">
	                                <span class="input-title">
	                                    <label>
	                                        <h4>驗證碼</h4>
	                                    </label>
	                                </span>
	                                <span class="input-block">
										<div class="ttb-input">
											<input id="capCode" type="text" class="text-input input-width-125"
												name="capCode" placeholder="請輸入驗證碼" maxlength="6" autocomplete="off">
											<img name="kaptchaImage" class="verification-img"/>
											<input type="button" name="reshow" class="ttb-sm-btn btn-flat-gray validate[required]" onclick="changeCode()" value="重新產生" />
											<span class="input-remarks">請注意：英文不分大小寫，限半形字元</span>
										</div>
									</span>
	                            </div>

							</div>
							
							<input type="button" id="CMBACK" value="上一步" class="ttb-button btn-flat-gray" />
							<input type="button" id="CMSUBMIT" value="下一步" class="ttb-button btn-flat-orange" />
	
						</div>
					</div>
				</form>

				<ol class="list-decimal description-list">

				</ol>

			</section>
		</main>
		<div id="agreeDialog" title="<spring:message code='LB.D0855' />–( <spring:message code='LB.D0856' /> ) ">
		<div style="text-align:center">
			<table id="careerTable" class="stripe table-striped ttb-table dtable" data-toggle-column="first">
					<tr>
						<p id="FDINVTYPEShow"></p>
<%-- 							<c:if test ="${ FDINVTYPE == '1'}"> --%>
<%-- 								<spring:message code='LB.D0944' />:<spring:message code='LB.D0945' /> --%>
<%-- 							</c:if> --%>
<%-- 							<c:if test ="${ FDINVTYPE == '2'}"> --%>
<%-- 								<spring:message code='LB.D0944' />:<spring:message code='LB.X1766' /> --%>
<%-- 							</c:if> --%>
<%-- 							<c:if test ="${ FDINVTYPE == '3'}"> --%>
<%-- 								<spring:message code='LB.D0944' />:<spring:message code='LB.X1767' /> --%>
<%-- 							</c:if> --%>
					</tr>
			</table>
			<input type="button" id="CMSUBMIT_Y" value="<spring:message code="LB.X0203"/>" class="ttb-button btn-flat-orange"/>
			<input type="button" id="CMSUBMIT_N" value="<spring:message code="LB.X0204"/>" class="ttb-button btn-flat-orange"/>
		</div>
	</div>
	</div>
	<%@ include file="../index/footer.jsp"%>

</body>

</html>