<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {
		// 將.table變更為footable
		initFootable();
	});
	
	//下拉式選單
 	function formReset() {
 		if ($('#actionBar').val()=="reEnter"){
	 		document.getElementById("formId").reset();
	 		$('#actionBar').val("");
 		}
	}
 
	//列印
	$("#printbtn").click(function(){
		var params = {
				"jspTemplateName":"demand_deposit_detail_result_print",
				"jspTitle":'<spring:message code="LB.NTD_Demand_Deposit_Detail"/>',
				"CMQTIME":"${demand_deposit_detail_result.data.CMQTIME}",
				"CMPERIOD":"${demand_deposit_detail_result.data.CMPERIOD}",
				"COUNT":"${demand_deposit_detail_result.data.COUNT}"
			};
			openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
	});
	//上一頁按鈕
	$("#CMBACK").click(function() {
		var action = '${__ctx}/NT/ACCT/demand_deposit_detail';
		$('#back').val("Y");
		$("form").attr("action", action);
		initBlockUI();
		$("form").submit();
	});
</script>
</head>
	<body>
		<!-- header     -->
		<header>
			<%@ include file="../index/header.jsp"%>
		</header>
	
		<!-- 麵包屑     -->
		<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
			<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
				<li class="ttb-breadcrumb-item"><a href="#"><i
						class="fa fa-home"></i></a></li>
				<li class="ttb-breadcrumb-item"><a href="#"><spring:message code= "LB.NTD_Services" /></a></li><!-- 臺幣服務 -->
				<li class="ttb-breadcrumb-item"><a href="#"><spring:message code= "LB.W0004" /></a></li><!-- 輕鬆理財戶 -->
				<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code= "LB.W0018" /></li><!-- 輕鬆理財自動申購基金明細 -->
			</ol>
		</nav>
		<!-- menu、登出窗格 -->
		<div class="content row">
			<%@ include file="../index/menu.jsp"%>
			<!-- 	快速選單內容 -->
			<!-- content row END -->
			<main class="col-12">
				<section id="main-content" class="container">
					<!-- 主頁內容  -->
					<h2><spring:message code= "LB.W0018" /></h2><!-- 輕鬆理財自動申購基金明細 -->
					<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
					<form id="formId" method="post" action="">
						<div class="main-content-block row">
							<div class="col-12">
								<ul class="ttb-result-list">
									<li>
										<!-- 查詢時間 -->
										<p>
											<spring:message code="LB.Inquiry_time" />：
											${loan_detail.data.CMQTIME}
										</p>
										<!-- 資料總數 -->
										<p>
											<spring:message code="LB.Total_records" />：
											${loan_detail.data.CMRECNUM}&nbsp;<spring:message code="LB.Rows"/>
										</p>
									</li>
								</ul>
								<!-- 輕鬆理財自動申購基金明細 -->
								<table class="table" data-toggle-column="first">
									<tr>
										<!-- 帳號 -->
										<td><spring:message code="LB.Account"/></td>
										<td class="text-left" colspan="9">${loan_detail.data.CMRECNUM}</td>
									</tr>
									<thead>
										<tr>
											<!-- 憑證號碼 -->
											<th><spring:message code= "LB.W0023" /></th>
											<!-- 基金代號-->
											<th><spring:message code= "LB.W0024" /></th>
											<!-- 基金名稱 -->
											<th><spring:message code= "LB.W0025" /></th>
											<!-- 基金名稱 -->
											<th><spring:message code= "LB.W0016" /></th>
											<!-- 單位數 -->
											<th><spring:message code= "LB.W0027" /></th>
											<!-- 基準日-->
											<th><spring:message code= "LB.W0028" /></th>
											<!-- 買入價格 -->
											<th><spring:message code= "LB.W0029" /></th>
											<!-- 參考匯率 -->
											<th><spring:message code= "LB.W0030" /></th>
											<!-- 台幣現值 -->
											<th><spring:message code= "LB.W0031" /></th>
											<!-- 損益-->
											<th><spring:message code= "LB.W0032" /></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="dataList" items="${loan_detail.data.TW}">
											<tr>
												<td>${dataList.ACNCOVER}</td>
												<td>${dataList.SEQ}</td>
												<td class="text-left">${dataList.SEQ}</td>
												<td class="text-right">
													<fmt:formatNumber type="number" minFractionDigits="3" value='${dataList.AMTFDP }'/>
												</td>
												<td class="text-right">
													<fmt:formatNumber type="number" minFractionDigits="4" value='${dataList.AMTFDP }'/>
												</td>
												<td class="text-right">${dataList.AMTFDP }</td>
												<td class="text-right">
													<fmt:formatNumber type="number" minFractionDigits="4" value='${dataList.AMTFDP }'/>
												</td>
												<td class="text-right">
													<fmt:formatNumber type="number" minFractionDigits="4" value='${dataList.AMTFDP }'/>
												</td>
												<td class="text-right">
													<fmt:formatNumber type="number" minFractionDigits="4" value='${dataList.AMTFDP }'/>
												</td>
												<td class="text-right">
													<fmt:formatNumber type="number" minFractionDigits="3" value='${dataList.AMTFDP }'/>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
								<!-- 列印鈕-->					
								<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>	
								<!-- 回上頁-->
	                            <input type="button" name="CMBACK" id="CMBACK" value="<spring:message code="LB.Back_to_previous_page"/>"/>
							</div>
						</div>
					</form>
				</section>
			</main>
			<!-- 		main-content END -->
			<!-- 	content row END -->
		</div>
		<%@ include file="../index/footer.jsp"%>
	</body>
</html>