<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
</head>

<body>
						<c:choose>
							<c:when test="${pageContext.response.locale=='zh_CN'}">
									<%@ include file="online_apply_menu_sub_zh_CN.jsp"%>
							</c:when>
							<c:when test="${pageContext.response.locale=='en'}">
									<%@ include file="online_apply_menu_sub_en.jsp"%>									
							</c:when>
							<c:otherwise>
									<%@ include file="online_apply_menu_sub_zh_TW.jsp"%>		
							</c:otherwise>
						</c:choose>	

	
</body>
</html>
