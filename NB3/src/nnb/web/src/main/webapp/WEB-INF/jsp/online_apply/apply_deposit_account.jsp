<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>

	<script type="text/javascript">
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 初始化驗證碼
			setTimeout("initKapImg()", 200);
			// 生成驗證碼
			setTimeout("newKapImg()", 300);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
			initKapImg();
		});
		
		// 畫面初始化
// 		function init() {
// 			// 表單驗證初始化
// 			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
// 			// 確認鍵 click
// 			goOn();
// 		}
		function init() {
			$("#CMSUBMIT").click(function(e) {
				var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
				var capData = $("#formId").serializeArray();
				var capResult = fstop.getServerDataEx(capUri, capData, false);
				if (capResult.result) {
					console.log("submit~~");
					initBlockUI(); //遮罩
					$("#formId").attr("action",
							"${__ctx}/ONLINE/APPLY/apply_deposit_account_p1");
					$("#formId").submit();
	 			}
				else {
					var b = ["<spring:message code= 'LB.X1082' />"];
					errorBlock(null, null, b, "<spring:message code= 'LB.Confirm' />", null);
					changeCode()
				}
			});
		}
		
		
		// 確認鍵 Click
// 		function goOn() {
// 			$("#CMSUBMIT").click( function(e) {
// 				// 送出進表單驗證前將span顯示
// 				$("#hideblock").show();
				
// 				console.log("submit~~");
	
// 				// 表單驗證
// 				if ( !$('#formId').validationEngine('validate') ) {
// 					e.preventDefault();
// 				} else {
// 					// 解除表單驗證
// 					$("#formId").validationEngine('detach');
					
// 					// 通過表單驗證準備送出
// 					processQuery();
// 				}
// 			});
// 		}
		
		// 通過表單驗證準備送出
		function processQuery() {
			// 遮罩
         	initBlockUI();
			
            $("#formId").submit();
		}
		function refreshCapCode() {
			console.log("refreshCapCode...");

			// 驗證碼
			$('input[name="capCode"]').val('');
			
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

			// 登入失敗解遮罩
			unBlockUI(initBlockId);
		}

		// 刷新輸入欄位
		function changeCode() {
			console.log("changeCode...");
			
			// 清空輸入欄位
			$('input[name="capCode"]').val('');
			
			// 刷新驗證碼
			refreshCapCode();
		}

		// 初始化驗證碼
		function initKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
		}

		// 生成驗證碼
		function newKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').click(function() {
				refreshCapCode();
			});
		}
		
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
    <!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><a href="#">開戶申請</a></li>
			<li class="ttb-breadcrumb-item"><a href="#">預約開立存款戶</a></li>
		</ol>
	</nav>
	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!--基金線上預約開戶作業 -->
					預約開立存款戶
				</h2>
				<div id="step-bar">
                    <ul>
                        <li class="active">注意事項與權益</li>
                        <li class="">開戶資料</li>
                        <li class="">確認資料</li>
                        <li class="">完成預約</li>
                        
                    </ul>
                </div>
				<form method="post" id="formId" name="formId" action="${__ctx}/ONLINE/APPLY/apply_deposit_account_p1">
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12 terms-block">
                            <div class="ttb-message">
                                <p><spring:message code="LB.D1101"/></p>
								
                            </div><br>
						
							<div class="text-left">  
								<spring:message code="LB.X0152"/>：<br><br>
								<spring:message code="LB.X0153"/>。<br>
								<ul class="list-decimal">
									<li data-num="1.">
										<span><spring:message code="LB.X0154"/></span>
									</li>            
									<li data-num="2.">
										<span><spring:message code="LB.X0155"/><font color=#FF8000><spring:message code="LB.X0156"/></font><span>
									</li>            
					                <li data-num="3.">
					                	<span><spring:message code="LB.X0157"/><span>
					                </li>
					                <li data-num="4.">
					                	<span><spring:message code="LB.X0158"/><font color=#FF8000><spring:message code="LB.X0159"/></font><spring:message code="LB.X0160"/></span>
					                </li>
					                <li data-num="5.">
					                	<span><spring:message code="LB.X0161"/></span>
					                </li>
									<li data-num="6.">
										<span><spring:message code="LB.X2454"/></span>
									</li>
								</ul>
							</div>
							<div class="ttb-input-block">
								<div class="ttb-input-item row">
	                                <span class="input-title">
	                                    <label>
	                                        <h4><spring:message code="LB.Captcha"/></h4>
	                                    </label>
	                                </span>
	                                <span class="input-block">
	                                    <div class="ttb-input">
	                                        <spring:message code="LB.Captcha" var="labelCapCode" />
											<input id="capCode" type="text" class="text-input input-width-125"
												name="capCode" placeholder="${labelCapCode}" maxlength="6" autocomplete="off">
											<img name="kaptchaImage" src=""  class="verification-img"/>
	                                        <input type="button" name="reshow" class="ttb-sm-btn btn-flat-gray" onclick="refreshCapCode()" value="<spring:message code="LB.Regeneration"/>" />
	                                        <span class="input-remarks">請注意：英文不分大小寫，限半形字元</span>
	                                    </div>
	                                </span>
	                            </div>
                            </div>
								
							<input type="button"  value="<spring:message code="LB.Cancel"/>" class="ttb-button btn-flat-gray" onclick="window.close();" />
							<!-- 確定 -->
							<input type="button" id="CMSUBMIT" value="<spring:message code="LB.X0080"/>" class="ttb-button btn-flat-orange" />
						</div>
					</div>
						
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>