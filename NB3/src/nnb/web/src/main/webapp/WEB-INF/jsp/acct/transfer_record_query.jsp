<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">

	<script type="text/javascript">
	$(document).ready(function() {
// 		getTmr();
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 50);
		// 開始跑下拉選單並完成畫面
		setTimeout("init()", 400);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
		
		datetimepickerEvent();
//			fgtxdateEvent();
		$("#formId").validationEngine({
					binded: false,
					promptPosition: "inline"
				});
		$("#CMSUBMIT").click(function(e){
			e = e || window.event;
			
			
			if(!$('#formId').validationEngine('validate')){
	        	e.preventDefault();
 			}else{
 				$("#formId").validationEngine('detach');
 				initBlockUI();
 				$("#formId").attr("action","${__ctx}/NT/ACCT/RESERVATION/transfer_record_query_result");
 	  			$("#formId").submit(); 
 			}		
  		});
	});

		//預約自動輸入今天
// 		function getTmr() {
// 			var today = new Date();
// 			today.setDate(today.getDate());
// 			var y = today.getFullYear();
// 			var m = today.getMonth() + 1;
// 			var d = today.getDate();
// 			if (m < 10) {
// 				m = "0" + m
// 			}
// 			if (d < 10) {
// 				d = "0" + d
// 			}
// 			var tmr = y + "/" + m + "/" + d
// 			$('#CMSDATE').val(tmr);
// 			$('#CMEDATE').val(tmr);
// 		}
		//	日曆欄位參數設定
		function datetimepickerEvent() {

			$(".CMSDATE").click(function (event) {
				$('#CMSDATE').datetimepicker('show');
			});
			$(".CMEDATE").click(function (event) {
				$('#CMEDATE').datetimepicker('show');
			});

			jQuery('.datetimepicker').datetimepicker({
				timepicker: false,
				closeOnDateSelect: true,
				scrollMonth: false,
				scrollInput: false,
				format: 'Y/m/d',
				lang: '${transfer}'
			});
		}
		//日期驗證
		/*
		function checkTimeRange() {
			console.log($('#CMSDATE').val());
			console.log($('#CMEDATE').val());
			var now = new Date(Date.now());
			var sixMm = 15778800000;
			var startT = new Date($('#CMSDATE').val());
			var endT = new Date($('#CMEDATE').val());
			var distance = now - startT;
			var range = endT - startT;
			var distanceE = now - endT;

			var limitS = new Date(now - sixMm);
			if (distance > sixMm) {
				var m = limitS.getMonth() + 1;
				var time = limitS.getFullYear() + '/' + m + '/' + limitS.getDate();
				// 起始日不能小於
				var msg = '<spring:message code="LB.Start_date_check_note_1" />' +
					time;
				alert(msg);
				return false;
			} else if (distance < 0) {
				var m = (now.getMonth()) + 1;
				var time = now.getFullYear() + '/' + m + '/' + now.getDate();
				// 起始日不能大於
				var msg = '起始日不能大於' + time;
				alert(msg);
				return false;
			} else {
				if (range < 0) {
					var msg = '終止日不能小於起始日';
					alert(msg);
					return false;
				} else if (distanceE < 0) {
					var m = now.getMonth() + 1;
					var time = now.getFullYear() + '/' + m + '/' + now.getDate();
					// 終止日不能大於
					var msg = '<spring:message code="LB.End_date_check_note_1" />' +
						time;
					alert(msg);
					return false;
				}
			}
			return true;
		}*/
		//下拉式選單
		 	function formReset() {
		 		//打開驗證隱藏欄位
				$("#hideblocka").show();
				$("#hideblockb").show();
				//塞值進span內的input
				$("#Monthly_DateA").val($("#CMSDATE").val());
				$("#odate").val($("#CMEDATE").val());
				
				if($('#CMPERIOD').prop('checked')){
					return false;
					
				}
				if(!$("#formId").validationEngine("validate")){
					e = e || window.event;//forIE
					e.preventDefault();
				}
		 		else{
					if ($('#actionBar').val()=="excel"){
						$("#downloadType").val("OLDEXCEL");
						$("#templatePath").val("/downloadTemplate/transfer_record_query.xls");
			 		}else if ($('#actionBar').val()=="txt"){
						$("#downloadType").val("TXT");
						$("#templatePath").val("/downloadTemplate/transfer_record_query.txt");
			 		}
					$("#formId").attr("target", "");
	                $("#formId").attr("action", "${__ctx}/NT/ACCT/RESERVATION/transfer_record_query_directDownload");
		            $("#formId").submit();
		            $('#actionBar').val("");		 		
		        }
			}

	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 轉出記錄查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0002" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2><spring:message code="LB.X0002" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value="">
							<spring:message code="LB.Downloads" />
						</option>
						<!-- 						下載Excel檔 -->
						<option value="excel">
							<spring:message code="LB.Download_excel_file" />
						</option>
						<!-- 						下載為txt檔 -->
						<option value="txt">
							<spring:message code="LB.Download_txt_file" />
						</option>
					</select>
				</div>
				
				<form id="formId" method="post" action="">
					<!-- 下載用 -->
					<!--台幣轉出紀錄查詢 -->
					<input type="hidden" name="downloadFileName" value="<spring:message code="LB.X0002" />" />
					<input type="hidden" name="downloadType" id="downloadType"/> 					
					<input type="hidden" name="templatePath" id="templatePath"/> 	

					<!-- EXCEL下載用 -->
					<!-- headerRightEnd  資料列以前的右方界線
						 headerBottomEnd 資料列到第幾列 從0開始
						 rowStartIndex 資料列第一列的位置
						 rowRightEnd 資料列用方的界線
					 -->
					<input type="hidden" name="headerRightEnd" value="8"/>
					<input type="hidden" name="headerBottomEnd" value="6"/>
					<input type="hidden" name="rowStartIndex" value="7"/>
					<input type="hidden" name="rowRightEnd" value="8"/>
					<!-- TXT下載用
						txtHeaderBottomEnd需為資料第一列(從0開始)-->
					<input type="hidden" name="txtHeaderBottomEnd" value="11"/>
					<input type="hidden" name="txtHasRowData" value="true"/>
					<input type="hidden" name="txtHasFooter" value="false"/>
					<input type="hidden" name="LOGINTYPE" value="NB"/>

					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-input-block">
								<!-- 帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4><spring:message code="LB.Payers_account_no" /></h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<!--帳號 -->
											<select class="custom-select select-input half-input" name="ACN" id="ACN">
												<c:forEach var="dataList" items="${transfer_record_query.data.REC}">
													<option>${dataList.ACN}</option>
												</c:forEach>
												<option value="AllAcn">
													<spring:message code="LB.All" />
												</option>
											</select>
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<!--查詢區間  -->
									<span class="input-title"> 
										<label>
											<h4><spring:message code="LB.Inquiry_period_1" /></h4>
										</label>
									</span> 
									<span class="input-block">
										<!--期間起日 -->
										<div class="ttb-input">	
											<span class="input-subtitle subtitle-color">
												<spring:message code="LB.D0013" />
											</span>
											<input type="text" id="CMSDATE" name="CMSDATE" class="text-input datetimepicker"  maxlength="10" value="${transfer_record_query.data.TODAY}" /> 
											<span class="input-unit CMSDATE">
												<img src="${__ctx}/img/icon-7.svg" />
											</span>
											<!-- 不在畫面上顯示的span -->
										</div>
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color">
												<spring:message code="LB.Period_end_date" />
											</span>
											<input type="text" id="CMEDATE" name="CMEDATE" class="text-input datetimepicker" maxlength="10" value="${transfer_record_query.data.TODAY}" /> 
											<span class="input-unit CMEDATE">
												<img src="${__ctx}/img/icon-7.svg" />
											</span>
											<!-- 不在畫面上顯示的span -->
											<span id="hideblockc" >
												<!-- 驗證用的input -->
													<input id="odate" name="odate" value="${transfer_record_query.data.TODAY}"  type="text" class="text-input validate[required,funcCall[validate_CheckDateScope['<spring:message code= "LB.X1445" />', odate, CMSDATE, CMEDATE, false, 6, null]]]" 
														style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												</span>
										</div>
									</span>
								</div>
							</div>
							<!-- 重新輸入 -->
							<input id="reset" name="reset" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" />
							<!-- 網頁顯示-->
							<input type="button" class="ttb-button btn-flat-orange" id="CMSUBMIT" value="<spring:message code="LB.Display_as_web_page" />"/>
						</div>
					</div>
					<!-- 說明 -->
					<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /></p>
						<li><span><spring:message code="LB.Transfer_Record_Query_P1_D1" /></span></li>
						<li><span><spring:message code="LB.Transfer_Record_Query_P1_D2" /></span></li>
						<li><span><spring:message code="LB.Transfer_Record_Query_P1_D3" /></span></li>
						<li><span><spring:message code="LB.Transfer_Record_Query_P1_D4" /></span></li>
						<c:if test="${transfer_record_query.data.isProd eq 3}"><li><span><font color="red"><spring:message code="LB.Reservation_P1_D5" /></font></span></li></c:if>
					</ol>
				</form>
			</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>

</html>