<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {
		setTimeout("initDataTable()",100);
		init();
		var mail = '${sessionScope.dpmyemail}';
// 		 	mail='';
		
		if( ''== mail){
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.X2637' />"], 
					'<spring:message code= "LB.Confirm" />', 
					null
				);
			$("#errorBtn1").click(function(e) {
				$('#error-block').hide();
			});
		}
		
	});
	
	function init() {
    }
	
	function sell(data,way){
		$('#BONDDATA').val(data);
		$('#SELLWAY').val(way);
		$("#formId").attr("action","${__ctx}/BOND/SELL/bond_sell_input");
		$('#formId').submit();
	}
</script>
</head>
	<body>
		<!-- header     -->
		<header>
			<%@ include file="../index/header.jsp"%>
		</header>
	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 海外債券     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2391" /></li>
    <!-- 海外債券交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2515" /></li>
    <!-- 海外債券贖回    -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X2520" /></li>
		</ol>
	</nav>

		<!-- menu、登出窗格 -->
		<div class="content row">
			<%@ include file="../index/menu.jsp"%>
			<!-- 	快速選單內容 -->
			<!-- content row END -->
			<main class="col-12">
				<section id="main-content" class="container">
					<!-- 主頁內容  -->
					<h2><spring:message code="LB.X2520" /></h2>
					<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				   <form id="formId" method="post" action="">
				   		<input type="hidden" id="BONDDATA" name="BONDDATA"/>
				   		<input type="hidden" id="SELLWAY" name="SELLWAY"/>
					</form>
						<div class="main-content-block row">
							<div class="col-12  tab-content">
								<ul class="ttb-result-list">
									<li><!-- 查詢時間 -->
										<h3><spring:message code="LB.Inquiry_time" /></h3>
	                                	<p>${bond_sell_query.data.CMQTIME}</p>
									</li>
									<li><!-- 資料總數 -->
										<h3><spring:message code="LB.Total_records" /></h3>
	                                	<p>${bond_sell_query.data.CMRECNUM}&nbsp;<spring:message code="LB.Rows"/></p>
									</li>
								</ul>
								<!-- 海外債券餘額及損益查詢 -->
								<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
									<thead>
										<tr>
<!-- 											信託帳號 -->
											<th class="text-left"><spring:message code="LB.W0944" /></th>
<!-- 											債券名稱 -->
									      	<th class="text-left"><spring:message code="LB.W1012" /></th>
<!-- 									      	投資幣別 -->
									      	<th class="text-center"><spring:message code="LB.W0908" /></th>
<!-- 									      	信託金額 -->
									      	<th class="text-right"><spring:message code="LB.W0026" /></th>
<!-- 									      	持有面額      	 -->
									      	<th class="text-right"><spring:message code="LB.X2521" /></th>
<!-- 									      	參考贖回報價 -->
									      	<th class="text-right"><spring:message code="LB.X2522" /></th>
									      	<th></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="dataList" items="${bond_sell_query.data.REC}">
											<tr>
												<td class="text-left">${dataList.O01}</td>
												<td class="text-left">${dataList.O02} &nbsp; ${dataList.O03}
												<c:if test="${dataList.KIND eq 3}">
<!-- 												不可部分贖回 -->
													<br> <font color=red>(<spring:message code="LB.X2523" />)</font>
												</c:if>
												</td>
												<td class="text-center">${dataList.O04_FMT}</td>
												<td class="text-right">${dataList.O06_FMT}</td>
												<td class="text-right">${dataList.O07_FMT}</td>
												<td class="text-right">${dataList.O08_FMT}%</td>
												<td class="text-center">
												<!-- KIND=1 >> 尚未報價 -->
												<c:if test="${dataList.KIND eq 1}">
<!-- 													尚未報價 -->
													<spring:message code="LB.X2524" />
												</c:if>
												<!-- KIND=2 >> 全部贖回/部分贖回 -->
												<c:if test="${dataList.KIND eq 2}">
													<input type="button" value='<spring:message code="LB.X1851" />' class="ttb-sm-btn btn-flat-orange" 
													onclick="sell('${dataList}',1)" />
													<hr style="margin: 2px 2px 2px 2px">
													<input type="button" value='<spring:message code="LB.X2525" />' class="ttb-sm-btn btn-flat-orange"
													onclick="sell('${dataList}',2)" />
												</c:if>
												<!-- KIND=3 >> 全部贖回 -->
												<c:if test="${dataList.KIND eq 3}">
													<input type="button" value='<spring:message code="LB.X1851" />' class="ttb-sm-btn btn-flat-orange" 
													onclick="sell('${dataList}',1)" />
												</c:if>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
				</section>
			</main>
			<!-- 		main-content END -->
			<!-- 	content row END -->
		</div>
		<%@ include file="../index/footer.jsp"%>
	</body>
<c:if test="${showDialog == 'true'}">
		<%@ include file="../index/txncssslog.jsp"%>
		</c:if>
</html>