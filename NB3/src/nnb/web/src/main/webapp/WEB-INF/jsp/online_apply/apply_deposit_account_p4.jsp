<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.twzipcode-1.6.1.js"></script>
		<style>
			.zipcode {
				display: none;
			}
		</style>
	    <script type="text/javascript">
	    oldBHID = '${input_data.BHID}';
	    firstTimeBHID = true;
        $(document).ready(function () {
            initFootable(); // 將.table變更為footable 
            init();
            setCITY();
        });

        function init() {
			$("#formId").validationEngine({binded: false,promptPosition: "inline"});
			document.getElementById("IBHPWD_BLOCK").style.display="none";
			
			//臺幣綜合存款質借功能(預設設定)
   		    if ('${input_data.ODFTYN}' == 'Y'){
   		        $("#ODFTYN2").attr("checked", true);
   		    }
   		    if ('${input_data.ODFTYN}' == 'N'){
   		        $("#ODFTYN1").attr("checked", true);
   		    }
			//非約定帳號轉帳功能(預設設定)
   		    if ('${input_data.LMTTYN}' == 'Y'){
   		        $("#LMTTYN1").attr("checked", true);
   		    }
   		    if ('${input_data.LMTTYN}' == 'N'){
   		        $("#LMTTYN2").attr("checked", true);
   		    }
			//全行收付功能(預設設定)
   		    if ('${input_data.IBHPWD_SHOW}' == '0'){
   		        $("#IBHPWD_SHOW0").attr("checked", true);
   		    }
   		    if ('${input_data.IBHPWD_SHOW}' == '1'){
   		        $("#IBHPWD_SHOW1").attr("checked", true);
   		    }
            //起家金帳號(預設設定)
            if ('${input_data.STAEDYN}' == 'Y'){
                $("#STAEDYN1").attr("checked", true);
				$("#ODFTYN1").attr("checked", true);

				//若選擇為起家金帳戶 則不提供調整質借功能
				$("#ODFTYN2").attr("disabled",true);

				$("#STAEDSQ_BLOCK").show();
			}
            if ('${input_data.STAEDYN}' == 'N'){
                $("#STAEDYN2").attr("checked", true);
                $("#STAEDSQ_BLOCK").hide();
            }
   		 	show_IBHPWD();
   		 	IBHPWD_check();
   		 	COMFIRMIBHPWD_check();
			<c:if test="${ input_data.checknb.equals('N') }">
			username_check();
			username2_check();
			</c:if>
    		
   		 	
			//$("#IBHPWD_BLOCK")
	    	$("#CMSUBMIT").click(function(e){
// 	        	if($("#IBHPWD").val()=="0000" || $("#IBHPWD").val() == ${ input_data.CCBIRTHDATEMM + input_data.CCBIRTHDATEDD }) 
// 	        	{
// 	        		alert("<spring:message code= "LB.Alert139" />");
// 	        		return false;
// 	       		}


				<c:if test="${ input_data.checknb.equals('N') }">
		    		if(!username_check() || !username2_check()){
						var b = ["請確認 網路銀行使用者名稱設定 資料是否正確"];
						errorBlock(null, null, b, '確定', null);
		    			return false;
		    		}
				</c:if>
	    		if(document.getElementsByName("IBHPWD_SHOW")[0].checked && (!IBHPWD_check() || !COMFIRMIBHPWD_check())){
					var b = ["請確認 全行收付功能密碼設定 資料是否正確"];
					errorBlock(null, null, b, '確定', null);
	    			return false;
	    		}

                if($("input:radio[name='STAEDYN']")[0].checked && (!STAEDSQ_check() )){
                    var b = ["請確認 起家金帳戶活動憑證號碼 資料是否正確"];
                    errorBlock(null, null, b, '確定', null);
                    return false;
                }

    			$("#BRHCOD").val($("#BHID").val());
    			$("#BRHNAME").val($("#BHID").find(":selected").text());
// 	    		if($("#LMTTYN1").prop("checked")){
// 					$("#LMTTYN").val('Y');
// 				}else{
// 					$("#LMTTYN").val('N');
// 				}
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
// 		        	initBlockUI();
		        	var action = '${__ctx}/ONLINE/APPLY/apply_deposit_account_p5';
					$("form").attr("action", action);
	    			$("form").submit();
				}
			});

			$("#CMBACK").click( function(e) {
				$("#formId").validationEngine('detach');
				console.log("submit~~");
    			$("#formId").attr("action", "${__ctx}/ONLINE/APPLY/apply_deposit_account_p3");
				$("#back").val('Y');
// 	         	initBlockUI();
	            $("#formId").submit();
			});
			$("#CMRESET").click(function(e){
				$("#formId")[0].reset();
			});
        }

		function ValidateValue(textbox) {
			var IllegalString = "+-_＿︿%@[`~!#$^&*()=|{}':;',\\[\\].<>/?~！#￥……&*（）——|{}【】‘；：”“'。，、？]‘'";
			var textboxvalue = textbox.value;
			var index = textboxvalue.length - 1;
			var s = textbox.value.charAt(index);
			if (IllegalString.indexOf(s) >= 0) {
				s = textboxvalue.substring(0, index);
				textbox.value = s;
			}
		}
		
		function show_IBHPWD(){
			//var IBHPWD = document.getElementById("IBHPWD_BLOCK");
			//var child = document.getElementById("childHiddenId");
			if(document.getElementsByName("IBHPWD_SHOW")[0].checked){
				document.getElementById("IBHPWD_BLOCK").style.display='';
				
			}else if(document.getElementsByName("IBHPWD_SHOW")[1].checked){
				document.getElementById("IBHPWD_BLOCK").style.display='none';
			}
		}
        function show_STAEDSQ(){
            if($("input:radio[name='STAEDYN']")[0].checked){
                $("#STAEDSQ_BLOCK").show();
                $("#ODFTYN1").attr("checked", true);
            }else if($("input:radio[name='STAEDYN']")[1].checked){
                $("#STAEDSQ_BLOCK").hide();
            }
        }
		
		//顯示該Zip下之分行
		function doquery8(){
			$.ajax({
				type :"POST",
		        url  : "${__ctx}/ONLINE/APPLY/apply_deposit_account_get_zip_bh",
		        data : { 
		        	zip : $("input[name='ZIP3']").val()
		        },
		        async: false ,
		        dataType: "json",
		        success : function(msg) {
		        	if($("#BHID").val() != "#")
		        		oldBHID = $("#BHID").val();
		        	$('#BHID').html("<option value='#'>--- <spring:message code= "LB.X1003" /> ---</option>  ");
		        	msg.data.bh.forEach(function(bh){
		        		console.log(bh);
		        		$("#BHID").append("<option value='" + bh.adbranchid +"'> " + bh.adbranchname + "</option>");
		        	});
		        	$("#BHID option:eq(1)").prop("selected",true);
		    		$("#BHID").children().each(function(){
		    		    if ($(this).val()==oldBHID){
		    		        $(this).prop("selected", true); //或是給"selected"也可
		    		    }
		    		});
		        }
			})
		}
		
		//初始化 指定服務分行
		function setCITY(){
   			llo = "${__ctx}/js/";
			$('#twzipcode').twzipcode({
                countyName: 'CITY3',
                districtName: 'ZONE3',
                zipcodeName: 'ZIP3',
                language:'zip_${pageContext.response.locale}',
                countySel: '${input_data.CITY3}' == ''?'${input_data.CITY2}' :'${input_data.CITY3}',
                districtSel: '${input_data.ZONE3}' == ''?'${input_data.ZONE2}' : '${input_data.ZONE3}',
                onCountySelect: null,
                onDistrictSelect: function(){
                	doquery8();
                	if(firstTimeBHID){
                		$("#BHID").children().each(function(){
                		    if ($(this).val()=='${input_data.BHID}'){
                		        $(this).prop("selected", true); //或是給"selected"也可
                		        firstTimeBHID = !firstTimeBHID;
                		    }
                		});
                	}
                }
			});
		}
		

		/***********************************/
		/*****			動態檢驗		   *****/
		/***********************************/
		function checkUserName(userID){
			var username=$('#'+userID).val();
			var min = 6;
			var max = 16;
			if(!checkLength(username,min,max)){
				return false;
			}
			if(!check2EnChar(username)){;
				return false;
			}
			if(checkIllegalChar(username)){
				return false;
			}
			if(checkDuplicateChar(username)){
				return false;
			}
			if(checkContinuousENChar(username)){
				return false;
			}
			return true;
		}
		function NewCheckpwd(oField, bCanEmpty)
		{
			try
			{
			    var sNumber = $('#'+oField).val();
			    if (bCanEmpty == false && sNumber == "")
			    {
			        return false;
			    }
			    var reNumber = new RegExp(/^[A-Za-z0-9]+$/);
			    if (!reNumber.test(sNumber))
			    {
			        return false;
			    }
			}
			catch (exception)
			{
				return false;
			}
			return true;
		}
		function CheckNotEmpty(oField)
		{
			try
			{
			    if ($('#'+oField).val() == "")
			        return false;
			}
			catch (exception)
			{
				return false;
			}
			return true;
		}

		//檢核輸入值是否為相同之文數字
		function chkSameEngOrNum(obj) {
			var str = $('#'+obj).val();
			if(str != '') {
				var iCode1 = str.charCodeAt(0);
				
				if((iCode1 >=48 && iCode1 <= 57) || (iCode1 >= 65 && iCode1 <= 90) || (iCode1 >= 97 && iCode1 <= 122)) {
					var iCode2 = 0;
					var bSame  = true;
					for(var i=0;i<str.length;i++) {
						iCode2 = str.charCodeAt(i);
						if(iCode1 != iCode2) {
							bSame = false;
						}
					}
				}
				if(bSame) {
					return false;
				}
			}
			return true;
		}
		function check4pwd(oField, bCanEmpty)
		{
			try
			{
			    var sNumber = $('#'+oField).val();
			    if (bCanEmpty == false && sNumber == "")
			    {
			        return false;
			    }
			    var reNumber = new RegExp(/^[0-9]+$/);
			    if (!reNumber.test(sNumber))
			    {
			        return false;
			    }
			    if(sNumber.length != 4){
			        return false;
			    }
			}
			catch (exception)
			{
				return false;
			}
			return true;
		}
		
		
		function username_check(){
			var ckeckflag = true;
			if($("#USERID").val() == $("#COMFIRMUSERID").val()){
				$("#USERNAME2_S1").removeClass("input-error-remarks");
				$("#USERNAME2_S1").addClass("input-correct-remarks");
				$("#USERNAME2_I1").removeClass("fa-times-circle");
				$("#USERNAME2_I1").addClass("fa-check-circle");
			}else{
				$("#USERNAME2_S1").addClass("input-error-remarks");
				$("#USERNAME2_S1").removeClass("input-correct-remarks");
				$("#USERNAME2_I1").addClass("fa-times-circle");
				$("#USERNAME2_I1").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			if(checkUserName("USERID") && $("#USERID").val().length >= 6 && $("#USERID").val().length <= 16){
				$("#USERNAME_S2").removeClass("input-error-remarks");
				$("#USERNAME_S2").addClass("input-correct-remarks");
				$("#USERNAME_I2").removeClass("fa-times-circle");
				$("#USERNAME_I2").addClass("fa-check-circle");
			}else{
				$("#USERNAME_S2").addClass("input-error-remarks");
				$("#USERNAME_S2").removeClass("input-correct-remarks");
				$("#USERNAME_I2").addClass("fa-times-circle");
				$("#USERNAME_I2").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			if($("#USERID").val().indexOf("${ input_data.BIRTHDAY }") == -1){
				$("#USERNAME_S3").removeClass("input-error-remarks");
				$("#USERNAME_S3").addClass("input-correct-remarks");
				$("#USERNAME_I3").removeClass("fa-times-circle");
				$("#USERNAME_I3").addClass("fa-check-circle");
			}else{
				$("#USERNAME_S3").addClass("input-error-remarks");
				$("#USERNAME_S3").removeClass("input-correct-remarks");
				$("#USERNAME_I3").addClass("fa-times-circle");
				$("#USERNAME_I3").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			if(chkSerialNum("USERID") && chkSameEngOrNum("USERID")){
				$("#USERNAME_S4").removeClass("input-error-remarks");
				$("#USERNAME_S4").addClass("input-correct-remarks");
				$("#USERNAME_I4").removeClass("fa-times-circle");
				$("#USERNAME_I4").addClass("fa-check-circle");
			}else{
				$("#USERNAME_S4").addClass("input-error-remarks");
				$("#USERNAME_S4").removeClass("input-correct-remarks");
				$("#USERNAME_I4").addClass("fa-times-circle");
				$("#USERNAME_I4").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			return ckeckflag;
		}
		function username2_check(){
			var ckeckflag = true;
			if($("#USERID").val() == $("#COMFIRMUSERID").val()){
				$("#USERNAME2_S1").removeClass("input-error-remarks");
				$("#USERNAME2_S1").addClass("input-correct-remarks");
				$("#USERNAME2_I1").removeClass("fa-times-circle");
				$("#USERNAME2_I1").addClass("fa-check-circle");
			}else{
				$("#USERNAME2_S1").addClass("input-error-remarks");
				$("#USERNAME2_S1").removeClass("input-correct-remarks");
				$("#USERNAME2_I1").addClass("fa-times-circle");
				$("#USERNAME2_I1").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			return ckeckflag;
		}

		function IBHPWD_check(){
			var ckeckflag = true;
			if($("#IBHPWD").val() == $("#COMFIRMIBHPWD").val()){
				$("#COMFIRMIBHPWD_S1").removeClass("input-error-remarks");
				$("#COMFIRMIBHPWD_S1").addClass("input-correct-remarks");
				$("#COMFIRMIBHPWD_I1").removeClass("fa-times-circle");
				$("#COMFIRMIBHPWD_I1").addClass("fa-check-circle");
			}else{
				$("#COMFIRMIBHPWD_S1").addClass("input-error-remarks");
				$("#COMFIRMIBHPWD_S1").removeClass("input-correct-remarks");
				$("#COMFIRMIBHPWD_I1").addClass("fa-times-circle");
				$("#COMFIRMIBHPWD_I1").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			if(check4pwd("IBHPWD")){
				$("#IBHPWD_S1").removeClass("input-error-remarks");
				$("#IBHPWD_S1").addClass("input-correct-remarks");
				$("#IBHPWD_I1").removeClass("fa-times-circle");
				$("#IBHPWD_I1").addClass("fa-check-circle");
			}else{
				$("#IBHPWD_S1").addClass("input-error-remarks");
				$("#IBHPWD_S1").removeClass("input-correct-remarks");
				$("#IBHPWD_I1").addClass("fa-times-circle");
				$("#IBHPWD_I1").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			if("${ input_data.BIRTHDAY }".indexOf($("#IBHPWD").val()) == -1){
				$("#IBHPWD_S2").removeClass("input-error-remarks");
				$("#IBHPWD_S2").addClass("input-correct-remarks");
				$("#IBHPWD_I2").removeClass("fa-times-circle");
				$("#IBHPWD_I2").addClass("fa-check-circle");
			}else{
				$("#IBHPWD_S2").addClass("input-error-remarks");
				$("#IBHPWD_S2").removeClass("input-correct-remarks");
				$("#IBHPWD_I2").addClass("fa-times-circle");
				$("#IBHPWD_I2").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			if(chkSameEngOrNum("IBHPWD")){
				$("#IBHPWD_S3").removeClass("input-error-remarks");
				$("#IBHPWD_S3").addClass("input-correct-remarks");
				$("#IBHPWD_I3").removeClass("fa-times-circle");
				$("#IBHPWD_I3").addClass("fa-check-circle");
			}else{
				$("#IBHPWD_S3").addClass("input-error-remarks");
				$("#IBHPWD_S3").removeClass("input-correct-remarks");
				$("#IBHPWD_I3").addClass("fa-times-circle");
				$("#IBHPWD_I3").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			return ckeckflag;
		}
		function COMFIRMIBHPWD_check(){
			var ckeckflag = true;
			if($("#IBHPWD").val() == $("#COMFIRMIBHPWD").val()){
				$("#COMFIRMIBHPWD_S1").removeClass("input-error-remarks");
				$("#COMFIRMIBHPWD_S1").addClass("input-correct-remarks");
				$("#COMFIRMIBHPWD_I1").removeClass("fa-times-circle");
				$("#COMFIRMIBHPWD_I1").addClass("fa-check-circle");
			}else{
				$("#COMFIRMIBHPWD_S1").addClass("input-error-remarks");
				$("#COMFIRMIBHPWD_S1").removeClass("input-correct-remarks");
				$("#COMFIRMIBHPWD_I1").addClass("fa-times-circle");
				$("#COMFIRMIBHPWD_I1").removeClass("fa-check-circle");
				ckeckflag = false;
			}
			return ckeckflag;
		}
        function STAEDSQ_check(){
            var ckeckflag = true;
            if(check5STAEDSQ("STAEDSQD")){
                $("#COMFIRMSTAEDSQ_S3").removeClass("input-error-remarks");
                $("#COMFIRMSTAEDSQ_S3").addClass("input-correct-remarks");
                $("#COMFIRMSTAEDSQ_I3").removeClass("fa-times-circle");
                $("#COMFIRMSTAEDSQ_I3").addClass("fa-check-circle");
            }else{
                $("#COMFIRMSTAEDSQ_S3").addClass("input-error-remarks");
                $("#COMFIRMSTAEDSQ_S3").removeClass("input-correct-remarks");
                $("#COMFIRMSTAEDSQ_I3").addClass("fa-times-circle");
                $("#COMFIRMSTAEDSQ_I3").removeClass("fa-check-circle");
                ckeckflag = false;
            }
            return ckeckflag;
        }
        function check5STAEDSQ(oField, bCanEmpty)
        {
            try
            {
                var sNumber = $('#'+oField).val();
                if (bCanEmpty == false && sNumber == "")
                {
                    return false;
                }
				var reNumber = new RegExp(/0\d{3}[0-9]{1}|[0-2]\d{4}|30000/g);
                if (!reNumber.test(sNumber))
                {
                    return false;
                }
                if(sNumber.length != 5){
                    return false;
                }
            }
            catch (exception)
            {
                return false;
            }
            return true;
        }

    </script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
    <!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><a href="#">開戶申請</a></li>
			<li class="ttb-breadcrumb-item"><a href="#">預約開立存款戶</a></li>
		</ol>
	</nav>
	<div class="content row">
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>預約開立存款戶</h2>
                <div id="step-bar">
                    <ul>
                      	<li class="finished">注意事項與權益</li>
						<li class="active">開戶資料</li>
						<li class="">確認資料</li>
						<li class="">完成預約</li>
                    </ul>
                </div>
                <form id="formId" method="post">
					<input type="hidden" name="back" id="back" value=>	
					<input type="hidden" name="BRHCOD" id="BRHCOD" value="">
					<input type="hidden" name="BRHNAME" id="BRHNAME" value="">	
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12 tab-content">
							<div class="ttb-input-block">
								<div class="ttb-message">
	                                <p>開戶資料 (3/3)</p>
	                            </div>
	                            <div class="classification-block">
	                                <p>帳戶約定事項</p>
									<p>( <span class="high-light">*</span> 為必填)</p>
	                            </div>
	                            <!-- **臺幣綜合存款質借功能*** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>臺幣綜合存款質借功能 <span class="high-light">*</span></h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
												同意申請
												<input type="radio" name="ODFTYN" id="ODFTYN2" value="Y" />
												<span class="ttb-radio"></span>
											</label>
											<label class="radio-block">
												不同意
												<input type="radio" name="ODFTYN" id="ODFTYN1" value="N" />
												<span class="ttb-radio"></span>
											</label>
											<span class="input-remarks">若您同意，本帳戶將能用於質借定期性存款。</span>
										</div>
										<span class="hideblock">
											<input id="validate_ODFTYN" name="ODFTYN" type="radio"  
												class="validate[funcCall[validate_Radio[是否同意臺幣綜合存款質借功能, ODFTYN]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px; visibility: hidden;"/>
										</span>
									</span>
								</div>
								<!-- **非約定帳號轉帳功能*** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>非約定帳號轉帳功能 <span class="high-light">*</span></h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
	                                        <label class="radio-block">同意申請
	                                            <input type="radio" name="LMTTYN" id="LMTTYN1" value="Y" />
	                                            <span class="ttb-radio"></span>
	                                        </label>
	                                        <label class="radio-block">不同意
	                                            <input type="radio" name="LMTTYN" id="LMTTYN2" value="N" />
	                                            <span class="ttb-radio"></span>
	                                        </label>
											<span class="input-remarks">若您同意，您的晶片金融卡將支援非約定帳號轉帳功能 (每日轉帳限額：3萬元)。</span>
	                                    </div>
	                                    
	                                    <span class="hideblock">
											<input id="validate_LMTTYN1" name="LMTTYN" type="radio"  
												class="validate[funcCall[validate_Radio[是否同意非約定帳號轉帳功能 , LMTTYN]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px; visibility: hidden;"/>
										</span>
										
<%-- 										<input type="checkbox" name="LMTTYN1" id="LMTTYN1"><spring:message code="LB.D1173"/><br>(<spring:message code="LB.D1174"/>)<br> --%>
										
									</span>
								</div>
								<!-- 全行收付功能 -->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                    <label>
	                                        <h4>全行收付功能 <span class="high-light">*</span></h4>
	                                    </label>
	                                </span>
	                                <span class="input-block">
	                                    <div class="ttb-input">
	                                        <label class="radio-block" >同意申請
	                                            <input type="radio" name="IBHPWD_SHOW" id="IBHPWD_SHOW1" value="1" onclick="show_IBHPWD()"/>
	                                            <span class="ttb-radio"></span>
	                                        </label>
	                                        <label class="radio-block" >不同意
	                                            <input type="radio" name="IBHPWD_SHOW" id="IBHPWD_SHOW0" value="0" onclick="show_IBHPWD()"/>
	                                            <span class="ttb-radio"></span>
	                                        </label>
	                                    </div>
	                                    
	                                    <span class="hideblock">
											<input id="validate_IBHPWD_SHOW" name="IBHPWD_SHOW" type="radio"  
												class="validate[funcCall[validate_Radio[是否同意全行收付功能 , IBHPWD_SHOW]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px; visibility: hidden;"/>
										</span>
	                                </span>
	                            </div>
								<!-- **全行收付功能密碼設定***** -->
								<div class="ttb-input-item row" id="IBHPWD_BLOCK">
									<span class="input-title"> 
									<label>
										<h4>全行收付功能密碼設定 <span class="high-light">*</span></h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>全行收付功能密碼</span>
										</div>
										<div class="ttb-input">
										<!-- 20191206 by hugo from Cleartext Submission of Sensitive Information -->
	                                        <input type="password" id="IBHPWD" name="IBHPW" class="text-input validate[required,funcCall[validate_CheckNumber['<spring:message code= "LB.D1175" />',IBHPWD]]]" value="${input_data.IBHPW}" placeholder="請輸入全行收付功能密碼" onkeyup="IBHPWD_check()">
	                                    </div>
										 <div class="ttb-input">
										<!-- 20191206 by hugo from Cleartext Submission of Sensitive Information -->
	                                        <input type="password" class="text-input" name="COMFIRMIBHPW" id="COMFIRMIBHPWD" value="${input_data.COMFIRMIBHPW}"  placeholder="請再次輸入全行收付功能密碼" onkeyup="COMFIRMIBHPWD_check()">
	                                    </div>
										<div class="ttb-input">
	                                        <span class="input-error-remarks" id="COMFIRMIBHPWD_S1"><i class="fa fa-times-circle mr-1" id="COMFIRMIBHPWD_I1"></i>全行收付功能密碼需相同</span>
	                                        <span class="input-error-remarks" id="IBHPWD_S1"><i class="fa fa-times-circle mr-1" id="IBHPWD_I1"></i>4位數字</span>
	                                        <span class="input-error-remarks" id="IBHPWD_S2"><i class="fa fa-times-circle mr-1" id="IBHPWD_I2"></i>不能含有生日</span>
	                                        <span class="input-error-remarks" id="IBHPWD_S3"><i class="fa fa-times-circle mr-1" id="IBHPWD_I3"></i>不得設定為相同的數字，例如：1111、0000</span>
										</div>
										
									</span>
								</div>
                                <!-- 起家金帳戶 -->
                                <div class="ttb-input-item row">
	                                <span class="input-title">
	                                    <label>
	                                        <h4>是否申請為起家金帳戶 <span class="high-light">*</span></h4>
	                                    </label>
	                                </span>
                                    <span class="input-block">
	                                    <div class="ttb-input">
	                                        <label class="radio-block" >同意申請
	                                            <input type="radio" name="STAEDYN" id="STAEDYN1" value="Y" disabled/>
	                                            <span class="ttb-radio"></span>
	                                        </label>
	                                        <label class="radio-block" >不同意
	                                            <input type="radio" name="STAEDYN" id="STAEDYN2" value="N" disabled/>
	                                            <span class="ttb-radio"></span>
	                                        </label>
	                                    </div>

	                                    <span class="hideblock">
											<input id="validate_STAEDSQ_SHOW" name="STAEDSQ_SHOW" type="radio"
                                                   class="validate[funcCall[validate_Radio[是否申請為起家金帳戶 , STAEDYN]]]"
                                                   style="border:white; margin:0px; padding:0%; height: 0px; width: 0px; visibility: hidden;"/>
										</span>
	                                </span>
                                </div>
                                <!-- **起家金帳戶***** -->
                                <div class="ttb-input-item row" id="STAEDSQ_BLOCK">
									<span class="input-title">
										<label>
											<h4>起家金帳戶<span class="high-light">*</span></h4>
										</label>
									</span>
                                    <span class="input-block">
										<div class="ttb-input">
											<span>北港朝天宮起家金帳戶活動憑證號碼5位數字</span>
										</div>
										<div class="ttb-input">
										<!-- 20191206 by hugo from Cleartext Submission of Sensitive Information -->
<%--	                                        <input type="text" id="STAEDSQD" name="STAEDSQ" class="text-input validate[required,min[1],max[20000],funcCall[validate_CheckAmount1['您所輸入的活動憑證號碼格式有誤，請重新檢查',STAEDSQ]]]" value="${input_data.STAEDSQ}" placeholder="請輸入起家金帳戶序號" onkeyup="STAEDSQ_check()">--%>
											<input type="text" id="STAEDSQD" name="STAEDSQ" class="text-input validate[required]" value="${input_data.STAEDSQ}" placeholder="請輸入起家金帳戶活動憑證號碼" onkeyup="STAEDSQ_check()">
	                                    </div>
										<div class="ttb-input">
	                                        <span class="input-remarks" id="COMFIRMSTAEDSQ_S1">起家金帳戶不得質借</span>
											<span class="input-remarks" id="COMFIRMSTAEDSQ_S2">申請起家金帳戶不得同時申請晶片金融卡</span>
                                            <span class="input-error-remarks" id="COMFIRMSTAEDSQ_S3"><i class="fa fa-times-circle mr-1" id="COMFIRMSTAEDSQ_I3"></i>活動憑證號碼需為五位數字格式，例如：00001、00002、00003、00200</span>
										</div>

									</span>
                                </div>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>網路銀行使用者名稱設定 <span class="high-light">*</span></h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
										<span>使用者名稱</span>
										</div>
										<div class="ttb-input">
											<c:if test="${ input_data.checknb.equals('N') }">
												<input type="text" id="USERID" name="USERID" class="text-input validate[required]" value="${ input_data.USERID }" maxLength="16" placeholder="請輸入使用者名稱" onkeyup="username_check()"/>
											</c:if>
											<c:if test="${ input_data.checknb.equals('Y') }">
												<input type="text" id="USERID" name="USERID" class="text-input" value="${ input_data.USERID }" maxLength="16" disabled/>
											</c:if>
										</div>
										<div class="ttb-input">
											<c:if test="${ input_data.checknb.equals('N') }">
												<input type="text" id="COMFIRMUSERID" name="COMFIRMUSERID" class="text-input validate[required]" value="${ input_data.COMFIRMUSERID }" maxLength="16" placeholder="請再次輸入使用者名稱" onkeyup="username2_check()"/>
											</c:if>
											<c:if test="${ input_data.checknb.equals('Y') }">
												<input type="text" id="COMFIRMUSERID" name="COMFIRMUSERID" class="text-input" value="${ input_data.COMFIRMUSERID }" maxLength="16" disabled/>
											</c:if>
										</div>
										<c:if test="${ input_data.checknb.equals('N') }">
											<div class="ttb-input">
		                                        <span class="input-error-remarks" id="USERNAME2_S1"><i class="fa fa-times-circle mr-1" id="USERNAME2_I1"></i>使用者名稱需相同</span>
		                                        <span class="input-error-remarks" id="USERNAME_S2"><i class="fa fa-times-circle mr-1" id="USERNAME_I2"></i><spring:message code="LB.D1003"/></span>
		                                        <span class="input-error-remarks" id="USERNAME_S3"><i class="fa fa-times-circle mr-1" id="USERNAME_I3"></i>不能含有生日</span>
		                                        <span class="input-error-remarks" id="USERNAME_S4"><i class="fa fa-times-circle mr-1" id="USERNAME_I4"></i>不得設定為相同或升降冪的英文或數字，例如：11111、AAAAA、123456、654321、ABCDED、FEDCBA</span>
											</div>
										</c:if>
									</span>
								</div>
								<!-- **服務分*** -->								
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 										★指定服務分行 -->
											<spring:message code="LB.D1472"/><span class="high-light">*</span>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										
										<div class="ttb-input" id="twzipcode">
											<span data-role="county" data-style="custom-select select-input input-width-125 validate[required]"></span>
											<span data-role="district" data-style="custom-select select-input input-width-100 validate[required]"></span>
											<span data-role="zipcode" data-name="ZIP3" data-style="zipcode"></span>
										</div>
										<div class="ttb-input">
											<select name="BHID" id="BHID" class="custom-select select-input validate[required]">
								          		<option value="#">--- <spring:message code="LB.Select"/><spring:message code="LB.X0169"/> ---</option>  
								          		<c:forEach var="dataList" items="${bh_data}">
													<option value="${dataList.ADBRANCHID}"> ${dataList.ADBRANCHNAME}</option>
												</c:forEach>
											</select>
										
											<span id="hideblock_ADDR2" >
												<input id="validate_CITY3" name="validate_CITY3" type="text" value="#" class="text-input 
													validate[funcCall[validate_CheckSelect['<spring:message code= "LB.D1118" />',CITY3,#]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												<input id="validate_ZONE3" name="validate_ZONE3" type="text" value="#" class="text-input 
													validate[funcCall[validate_CheckSelect['<spring:message code= "LB.D1490" />',ZONE3,#]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												<input id="validate_BHID" name="validate_BHID" type="text" value="#" class="text-input 
													validate[funcCall[validate_CheckSelect['<spring:message code= "LB.D1472" />',BHID,#]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
									</span>
								</div>
								
                           	</div>
	                        <!--button 區域 -->
							<!-- 重新輸入 -->
							<input type="button" name="CMBACK" id="CMBACK" value="<spring:message code="LB.X0318"/>" class="ttb-button btn-flat-gray">
                            <input id="CMSUBMIT" name="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.X0080"/>" />
                        <!--                     button 區域 -->
                        </div>  
                    </div>
                </form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>