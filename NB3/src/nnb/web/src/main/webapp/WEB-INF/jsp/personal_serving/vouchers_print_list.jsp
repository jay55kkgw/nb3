<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	
	<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()",10);
		
		setTimeout("initDataTable()",100);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)",500);
		
		$('#CMSUBMIT').click(function(e){
			 if(!checkSelected()) 
		   	  {	
		   	  	//alert("<spring:message code= "LB.Alert196" />");
		   	  	errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert196' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
		   	  	return false;
		   	  }else{
		   		  $("#formId").submit();
		   	  }
			 
		});
	});
	function setVal(SEQCLM,FORM){
		$('#SEQCLM').val(SEQCLM);
		$('#FORM').val(FORM);
	}
	
	 function checkSelected()
	   {
	   	  var isSelected=false;
	   	  var form = document.getElementById("formId");
	   	  for(var i=0 ; i< form.elements.length ; i++)
	   	  	{
	   	  		if (form.elements[i].type=="radio"){
					if (form.elements[i].checked){
						isSelected=true;
					}}
	   	  	}
	   	  return isSelected;
	   }
	</script>
	
</head>
<body>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %> 
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 各類所得扣繳憑單列印     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0141" /></li>
		</ol>
	</nav>

		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
            <section id="main-content" class="container">
                <h2><spring:message code="LB.X0141" /></h2>
                <i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="${__ctx}/PERSONAL/SERVING/vouchers_print_detail">
				<input type="hidden" id="FGTXWAY" name="FGTXWAY" value="${vouchers_print_list.data.FGTXWAY}" >
				<input type="hidden" id="ADOPID" name="ADOPID" value="N106_Detile" >
				<input type="hidden" id="SEQCLM" name="SEQCLM" value="" >
				<input type="hidden" id="FORM" name="FORM" value="" >
                <div class="main-content-block row">
                    <div class="col-12">
                    	<br><br>
                        <table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
                            <thead>
                                <tr>
                                    <th><spring:message code="LB.W0879" /></th>
                                    <th data-title="<spring:message code="LB.X0142" />" data-breakpoints="xs "><spring:message code="LB.X0142" /></th>
                                    <th data-title="<spring:message code="LB.W0883" />" data-breakpoints="xs "><spring:message code="LB.W0883" /></th>
                                    <th data-title="<spring:message code="LB.X0143" />"><spring:message code="LB.X0143" /></th>
                                    <th data-title="<spring:message code="LB.X0144" />" data-breakpoints="xs "><spring:message code="LB.X0144" /></th>
                                    <th data-title="<spring:message code="LB.X0145" />" data-breakpoints="xs "><spring:message code="LB.X0145" /></th>
                                    <th data-title="<spring:message code="LB.X0146" />" data-breakpoints="xs "><spring:message code="LB.X0146" /></th>
                                    <th data-title="<spring:message code="LB.X0147" />"data-breakpoints="xs "><spring:message code="LB.X0147" /></th>
									<th data-title="<spring:message code="LB.W0985" />"data-breakpoints="xs "><spring:message code="LB.W0985" /></th>
								   
                                </tr>
                            </thead>
                            <tbody>
                            <c:forEach var="dataList" items="${vouchers_print_list.data.REC}">
                                <tr>
                                    <td class="text-center">
										<label class="radio-block">&nbsp;
                                           	<input type="radio" name="DATA" onclick="setVal('${dataList.SEQCLM}','${dataList.FORM}')">
                                            <span class="ttb-radio"></span>
                                        </label>
                                    </td>
                                    <td class="text-center">${dataList.BRHCOD}</td>
                                    <td class="text-center">${dataList.BRHNAME_DB}</td>
                                    <td class="text-center">${dataList.BUSIDN}</td>
                                    <td class="text-center">${dataList.SEQCLM}</td>
                                    <td class="text-center">${dataList.FORM}</td>
                                    <td class="text-right">${dataList.TAMPAY_SHOW}</td>
									<td class="text-right">${dataList.AMTTAX_SHOW}</td>
                                    <td class="text-right">${dataList.NETPAY_SHOW}</td>
                                </tr>
								</c:forEach>
                            </tbody>
                        </table>
						<input class="ttb-button btn-flat-gray" type="reset" value="<spring:message code="LB.Re_enter" />" name="CMRESET">
                        <input class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" id="CMSUBMIT" name="CMSUBMIT"  type="button">
                    </div>
                </div>
              </form>
            </section>
        </main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>