<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<!--舊版驗證-->
<script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
<style type="text/css">
.table-1 {
    width: 100%;
    border-spacing: 0;
    border-collapse: collapse;
    margin: 0 auto;
    border:
}

.table-1>thead>tr>th,
.table-1>thead>tr>td {
    background-color: #EEEEEE;
    border-top: none;
    text-shadow: 0 1px 0 rgba(255, 255, 255, .5);
    font-weight: bold;
}

.table-1>tbody>tr>td,
.table-1>thead>tr>th {
    border: 1px solid #DDD;
    padding: 8px;
    text-align: left;
}

.table-1 tbody tr:first-child td:first-child {
    text-align: center;
}

.table-1 td:hover {
    background-color: #fbf8e9;
}
</style>
<script type="text/JavaScript">
function fillDefaultData(){
	$("#CHILDNO").val("0");
	$("#APPKIND").val("${bs.data.APPKIND}");
	$("#font1").html($("#APPNAME").val());
	$("#APPNAME").val("${bs.data.hiddenUSERNAME}");
	$("#BIRTHY").val("${bs.data.BIRTHY}");
	$("#BIRTHM").val("${bs.data.BIRTHM}");
	$("#CPHONE").val("${bs.data.MOBTEL}");
	
	$("#BIRTHD").val("${bs.data.BIRTHD}");
	$("#PHONE_01").val("${bs.data.TEL11}");
	$("#PHONE_02").val("${bs.data.TEL12}");
	$("#PHONE_03").val("${bs.data.TEL13}");
	$("#ZIP2").val("${bs.data.POSTCOD2}");
	
	$("#ADDR2").val("${bs.data.CTTADR1}");
	$("#ADDR1").val("${bs.data.PMTADR1}");
	$("#EMAIL").val("${bs.data.DPMYEMAIL}");
	changeamt();
	var UID = "${bs.data.cusidn}";
	var sex = UID.substring(1,2);
	$("input[name=SEX][value=" + sex + "]").prop("checked",true);
	
	var CTTADR = "${bs.data.CTTADR}";
	
	var CTTADRstring = CTTADR.substring(0,3);
	$("#CITY2").val(CTTADRstring);
	doquery();
	if(CTTADR.length >= 6){
		CTTADRstring = CTTADR.substring(3,6);
		$("#ZONE2").val(CTTADRstring);
	}
	doquery1();
	var PMTADR = "${bs.data.PMTADR}";
	var PMTADRstring = PMTADR.substring(0,3);
	$("#CITY1").val(PMTADRstring);
	doquery2();
	if(PMTADR.length >= 6){
		PMTADRstring = PMTADR.substring(3,6);
		$("#ZONE1").val(PMTADRstring);
	}
	doquery3();
	
}
$(document).ready(function() {
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	initFootable();
	fillDefaultData();
	$("#okButton").click(function(){
		if($("#CHILDNO").val()==""){
			$("#CHILDNO").val("0");
		}
		var APPKIND = $("#APPKIND").val();
		var APPAMT = $("#APPAMT").val();      	
		var APPNAME = $("#APPNAME").val();
		var BIRTHY = $("#BIRTHY").val();
		var BIRTHM = $("#BIRTHM").val();
		var BIRTHD = $("#BIRTHD").val();      
		var RELNAME1 = $("#RELNAME1").val();
		var RELNAME2 = $("#RELNAME2").val();
		var RELNAME3 = $("#RELNAME3").val();
		var RELNAME4 = $("#RELNAME4").val();
		var RELID1 = $("#RELID1").val();
		var RELID2 = $("#RELID2").val();
		var RELID3 = $("#RELID3").val();
		var RELID4 = $("#RELID4").val();
		var OTHCONM1 = $("#OTHCONM1").val();
		var OTHCOID1 = $("#OTHCOID1").val();
		var MOTHCONM1 = $("#MOTHCONM1").val();
		var MOTHCOID1 = $("#MOTHCOID1").val();
		var CAREERNAME = $("#CAREERNAME").val();
		var CAREERIDNO = $("#CAREERIDNO").val();
		var INCOME = $("#INCOME").val();
		var ADDR1 = $("#ADDR1").val();
		var ADDR2 = $("#ADDR2").val();
		var ADDR4 = $("#ADDR4").val();
		var PHONE0 = $("#PHONE_01").val() + $("#PHONE_02").val() + $("#PHONE_03").val();
		var PHONE1 = $("#PHONE_11").val() + $("#PHONE_12").val();
		var PHONE4 = $("#PHONE_41").val() + $("#PHONE_42").val() + $("#PHONE_43").val();      
		var OFFICEY = $("#OFFICEY").val();
		var OFFICEM = $("#OFFICEM").val();
		var PRECARNAME = $("#PRECARNAME").val();   
		var PAYSOURCE2 = $("#PAYSOURCE2").val(); 
		
		//申請金額
		if(!CheckNumber("APPAMT","<spring:message code= 'LB.D0560' />",false)){
			$("#APPAMT").focus();
			return false;
		}
		if((APPKIND == "B" && parseInt(APPAMT) * 10000 > 1000000) || (APPKIND == "F" && parseInt(APPAMT) * 10000 > 1000000)){
			//貸款金額不得超過100萬
			
			//alert("<spring:message code= 'LB.X1048' />");
			errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.X1048' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
			);
			$("#APPAMT").focus();
	      	return false;
		} 
		if(APPKIND == "E" && parseInt(APPAMT) * 10000 > 100000){
			//貸款金額不得超過10萬
			//alert("<spring:message code= 'LB.X1049' />");
			errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1049' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			$("#APPAMT").focus();
			return false;
		}      	   
		//資金用途
		if(!CheckRadio("<spring:message code= 'LB.D0564' />","PURPOSE")){
			$("input[name=PURPOSE]").focus();
			return false;
		}
		//申請還款期數
		if(!CheckRadio("<spring:message code= 'LB.D0573' />","PERIODS")){
			$("input[name=PERIODS]:checked").focus();
			return false;
		}
		if(!checkperiods()){
			return false;
		}
		//指定對保分行
		if (!CheckSelect("BHID","<spring:message code= 'LB.D0575' />","#")){
			$("#BHID").focus();
			return false;
		}
		if(!isChinese(APPNAME)){
			$("#APPNAME").focus();
			//中文姓名不合法!
			//alert("<spring:message code= 'LB.X1056' />");
			errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1056' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			return  false;
		}
		//出生日期
		if(!CheckNumber("BIRTHY","<spring:message code= 'LB.D0582' />",false)){
			$("#BIRTHY").focus();
			return false;
		}
		if(!CheckNumber("BIRTHM","<spring:message code= 'LB.D0582' />",false)){
			$("#BIRTHM").focus();
			return false;
		}
		if(parseInt(BIRTHM) > 12){
			//出生日期月份輸入有誤
			//alert("<spring:message code= 'LB.X1050' />");
			errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1050' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			$("#BIRTHM").focus();
			return false;
		}
		if(!CheckNumber("BIRTHD","<spring:message code= 'LB.D0582' />",false)){
			$("#BIRTHD").focus();
			return false;
		}
		if(parseInt(BIRTHD) > 31){
			//出生日期日期輸入有誤
			//alert("<spring:message code= 'LB.X1051' />");
			errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1051' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			$("#BIRTHD").focus();
			return false;
		}
		if(BIRTHY.length < 3 && BIRTHY.length > 0){	
			BIRTHY = "0" + BIRTHY;
	      	$("#BIRTHY").val(BIRTHY);
		}      
		if(BIRTHM.length < 2 && BIRTHM.length > 0){
			BIRTHM = "0" + BIRTHM;
			$("#BIRTHM").val(BIRTHM);
		}
		if(BIRTHD.length < 2 && BIRTHD.length > 0){
			BIRTHD = "0" + BIRTHD;
			$("#BIRTHD").val(BIRTHD);
		}	   	    	  
		if(BIRTHY.length == 3 && BIRTHM.length == 2 && BIRTHD.length == 2){
			$("#BRTHDT").val(BIRTHY + "/" + BIRTHM + "/" + BIRTHD);
		}
		else{
			//出生日期長度錯誤
			//alert("<spring:message code= 'LB.X1052' />");
			errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1052' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			$("#BIRTHD").focus();
			return false;
		}
		//教育程度
		if(!CheckRadio("<spring:message code= 'LB.D0055' />","EDUS")){
			$("input[name=EDUS]").focus();
			return false;
		}
		//婚姻狀況
		if(!CheckRadio("<spring:message code= 'LB.D0057' />","MARITAL")){
			$("input[name=MARITAL]").focus();
			return false;
		}
// 		//子女數
// 		if(!CheckNumber("CHILDNO","<spring:message code= 'LB.D0602_1' />",false)){
// 			$("#CHILDNO").focus();
// 			return false;
// 		}
		//行動電話
		if(!CheckNumber("CPHONE","<spring:message code= 'LB.D0069' />",false)){
			$("#CPHONE").focus();
			return false;
		}
		//通訊電話
		if(!CheckNumber("PHONE_01","<spring:message code= 'LB.D0599' />",false)){
			$("#PHONE_01").focus();
			return false;
		}
		//通訊電話
		if(!CheckNumber("PHONE_02","<spring:message code= 'LB.D0599' />",false)){
			$("#PHONE_02").focus();
			return false;
		}
		//通訊電話分機
		if(!CheckNumber("PHONE_03","<spring:message code= 'LB.D0599' /><spring:message code= 'LB.D0095' />",true)){
			$("#PHONE_03").focus();
			return false;
		}
		if(PHONE0.length == 0){
			//通訊電話不得為空白
			//alert("<spring:message code= 'LB.X1053' />");
			errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1053' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			$("#PHONE_01").focus();
			return  false;	  	
		}
		//戶籍電話
		if(!CheckNumber("PHONE_11","<spring:message code= 'LB.D0144' />",true)){
			$("#PHONE_11").focus();
			return false;
		}
		//戶籍電話
		if(!CheckNumber("PHONE_12","<spring:message code= 'LB.D0144' />",true)){
			$("#PHONE_12").focus();
			return false;
		}
		//住宅狀況
		if(!CheckRadio("<spring:message code= 'LB.D0603' />","HOUSE")){
			$("input[name=HOUSE]").focus();
			return false;
		}
		if(!checkhouse()){
			return false;
		}
		//房貸月繳
		if(!CheckNumber("MONEY11","<spring:message code= 'LB.D0609_1' />",true)){
			$("#MONEY11").focus();
			return false;
		}
		//房租月繳
		if(!CheckNumber("MONEY12","<spring:message code= 'LB.D0610_1' />",true)){
			$("#MONEY12").focus();
			return false;
		}
		//現住地址郵遞區號
		if(!CheckNumber("ZIP2","<spring:message code= 'LB.D0611' /><spring:message code= 'LB.D0061' />",false)){
			$("#ZIP2").focus();
			return false;
		}
		//現住地址縣市別
		if(!CheckSelect("CITY2","<spring:message code= 'LB.D0611' /><spring:message code= 'LB.D0059' />","#")){
			$("#CITY2").focus();
			return false;
		}
		//現住地址市/區鄉鎮
		if(!CheckSelect("ZONE2","<spring:message code= 'LB.D0611' /><spring:message code= 'LB.D0060' />","#")){
			$("#ZONE2").focus();
			return false;
		}
		if(ADDR2.length == 0){
			//現住地址不得為空白
			//alert("<spring:message code= 'LB.X1054' />");
			errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1054' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			$("#ADDR2").focus();
			return false;
		}
		//戶籍地址郵遞區號
		if(!CheckNumber("ZIP1","<spring:message code= 'LB.D0058' /><spring:message code= 'LB.D0061' />",false)){
			$("#ZIP1").focus();
			return false;
		}
		//戶籍地址縣市別
		if(!CheckSelect("CITY1","<spring:message code= 'LB.D0058' /><spring:message code= 'LB.D0059' />","#")){
			$("#CITY1").focus();
			return false;
		}
		//戶籍地址市/區鄉鎮
		if(!CheckSelect("ZONE1","<spring:message code= 'LB.D0058' /><spring:message code= 'LB.D0060' />","#")){
			$("#ZONE1").focus();
			return false;
		}
		if(ADDR1.length == 0){
			//戶籍地址不得為空白
			//alert("<spring:message code= 'LB.X1055' />");
			errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1055' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			$("#ADDR1").focus();
			return false;
		}   	   	  
		//電子郵件
		if(!checkEmail("EMAIL"," <spring:message code= 'LB.e-mail' />")){
			$("#EMAIL").focus();
			return false;
		}
		if(!CheckSelect("RELTYPE1","<spring:message code= 'LB.X1185' />","#")){
			$("#RELTYPE1").focus();
			return false;
		}
		if(RELNAME1.length > 0){
			if(!isChinese(RELNAME1)){
				//中文姓名不合法！
				//alert("<spring:message code= 'LB.X1056' />");
				errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1056' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				$("#RELNAME1").focus();
				return  false;
			}  	  
		}
		if(RELNAME2.length > 0){
			if(!isChinese(RELNAME2)){  
				//中文姓名不合法！
				//alert("<spring:message code= 'LB.X1056' />");
				errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1056' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				$("#RELNAME2").focus();
				return  false;
			}  	  
		}
		if(RELNAME3.length > 0){
			if(!isChinese(RELNAME3)){
				//中文姓名不合法！
				//alert("<spring:message code= 'LB.X1056' />");
				errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1056' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				$("#RELNAME3").focus();
				return  false;
			}  	  
		}
		if(RELNAME4.length > 0){
			if(!isChinese(RELNAME4)){ 
				//中文姓名不合法！
				//alert("<spring:message code= 'LB.X1056'/>");
				errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1056' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				$("#RELNAME4").focus();
				return  false;
			}  	  
		}
		if(RELID1.length > 0 && RELID1.length != 10){
			//身分證字號長度錯誤
			//alert("<spring:message code= 'LB.X1057' />");
			errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1057' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			$("#RELID1").focus();
			return  false;  	  	
		}
		if(RELID2.length > 0 && RELID2.length != 10){
			//身分證字號長度錯誤
			//alert("<spring:message code= 'LB.X1057' />");
			errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1057' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			$("#RELID2").focus();
			return  false;  	  	
		}
		if(RELID3.length > 0 && RELID3.length != 10){
			//身分證字號長度錯誤
			//alert("<spring:message code= 'LB.X1057' />");
			errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1057' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			$("#RELID3").focus();
			return  false;  	  	
		}
		if(RELID4.length > 0 && RELID4.length != 10){
			//身分證字號長度錯誤
			//alert("<spring:message code= 'LB.X1057' />");
			errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1057' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			$("#RELID4").focus();
			return  false;  	  	
		}
		if(RELNAME1.length > 0 && RELID1.length > 0){
			$("#CKS1").val("1");
			
		}
		else{
			$("#CKS1").val("");
		}
		if(RELNAME2.length > 0 && RELID2.length > 0){
			$("#CKS2").val("1");
		}
		else{
			$("#CKS2").val("");
		}
		if(RELNAME3.length > 0 && RELID3.length > 0){
			$("#CKS3").val("1");
		}
		else{
			$("#CKS3").val("");
		}
		if(RELNAME4.length > 0 && RELID4.length > 0){
			$("#CKS4").val("1");
		}
		else{
			$("#CKS4").val("");
		}
		if($("#CKS1").val() != "1" && $("#CKS2").val() != "1" && $("#CKS3").val() != "1" && $("#CKS4").val() != "1"){
			//同一關係人資料至少要填一個
			//alert("<spring:message code= 'LB.X1058' />");
			errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1058' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			$("#RELNAME1").focus();
			return false;
		}
		if(OTHCONM1.length > 0){
			if(!isChinese(OTHCONM1)){
				//中文名稱不合法
				//alert("<spring:message code= "LB.X1186" />");
				errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1186' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				$("#OTHCONM1").focus();
				return false;
			}
		}
		if(OTHCOID1.length > 0 && OTHCOID1.length != 8){
			//統一編號長度錯誤
			//alert("<spring:message code= 'LB.X1059' />");
			errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1059' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			$("#OTHCOID1").focus();
			return false;  	  	
		}	
		if(MOTHCONM1.length > 0){
			if(!isChinese(MOTHCONM1)){
				//中文名稱不合法
				//alert("<spring:message code= "LB.X1186" />");
				errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1186' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				$("#MOTHCONM1").focus();
				return  false;
			}  	  
		} 
		if(MOTHCOID1.length > 0 && MOTHCOID1.length != 8){
			//統一編號長度錯誤
			//alert("<spring:message code= 'LB.X1059' />");
			errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1059' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			$("#MOTHCOID1").focus();
			return false;  	  	
		}	
		if(OTHCONM1.length > 0 && OTHCOID1.length > 0){
			$("#CKP1").val("1");
		}
		else{
			$("#CKP1").val("");
		}
		if(MOTHCONM1.length > 0 && MOTHCOID1.length > 0){
			$("#CKM1").val("1");
		}
		else{
			$("#CKM1").val("");
		}
		if(CAREERNAME.length > 0){
			if(!isChinese(CAREERNAME)){
				//公司中文名稱不合法
				//alert("<spring:message code= "LB.X1187" />");
				errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1187' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				$("#CAREERNAME").focus();
				return false;
			}  	  
		}
		else{
			//公司名稱長度不得為零
			//alert("<spring:message code= 'LB.X1060' />");
			errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1060' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			$("#CAREERNAME").focus();
			return false;  	  	
		} 
		if(CAREERIDNO.length > 0 && CAREERIDNO.length != 8){	
			//公司統一編號長度錯誤
			//alert("<spring:message code= 'LB.X1061' />");
			errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1061' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			$("#CAREERIDNO").focus();
			return false;  	  	
		}	 
		//年收入
		if(!CheckNumber("INCOME"," <spring:message code= "LB.D0625_1" />",false)){
			$("#INCOME").focus();
			return false;
		}
		//公司地址郵遞區號
		if(!CheckNumber("ZIP4","<spring:message code= 'LB.D0090' /><spring:message code= 'LB.D0061' />",false)){
			$("#ZIP4").focus();
			return false;
		}
		//公司地址縣市別
		if(!CheckSelect("CITY4","<spring:message code= 'LB.D0090' /><spring:message code= 'LB.D0059' />","#")){
			$("#CITY4").focus();
			return false;
		}
		//公司地址市區鄉鎮
		if(!CheckSelect("ZONE4","<spring:message code= 'LB.D0090' /><spring:message code= 'LB.D0060' />","#")){
			$("#ZONE4").focus();
			return false;
		}
		if(ADDR4.length == 0){
			//公司地址不得為空白
			//alert("<spring:message code= 'LB.X1062' />");
			errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1062' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			$("#ADDR4").focus();
			return false;
		}
		//公司電話
		if(!CheckNumber("PHONE_41","<spring:message code= 'LB.D0094' />",false)){
			$("#PHONE_41").focus();
			return false;
		}
		//公司電話
		if(!CheckNumber("PHONE_42","<spring:message code= 'LB.D0094' />",false)){
			$("#PHONE_42").focus();
			return false;
		}
		//公司電話分機
		if(!CheckNumber("PHONE_43","<spring:message code= 'LB.D0094' /><spring:message code= "LB.D0095" />",true)){
			$("#PHONE_43").focus();
			return false;
		}
		if(PHONE4.length == 0){
			//公司電話不得為空白
			//alert(" <spring:message code= 'LB.X1063' />");
			errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1063' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			$("#PHONE4").focus();
			return false;
		}   	      		 	  
		if(!CheckSelect("PROFNO","<spring:message code= 'LB.X1188' />","#")){
			$("#PROFNO").focus();
			return false;
		}
		//到職日
		if(!CheckNumber("OFFICEY","<spring:message code= 'LB.D0629' />",false)){
			$("#OFFICEY").focus();
			return false;
		}
		if(!CheckNumber("OFFICEM","<spring:message code= 'LB.D0629' />",false)){
			$("#OFFICEM").focus();
			return false;
		}
		if(parseInt(OFFICEM) > 12){
			//到職日月份輸入有誤
			//alert(" <spring:message code= 'LB.X1064' />");
			errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1064' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			$("#OFFICEM").focus();
			return false;
		}
		if(PRECARNAME.length > 0){
			if(!isChinese(PRECARNAME)){
				//前職公司名稱中文不合法
				//alert("<spring:message code= 'LB.X1065' />");
				errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1065' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				$("#PRECARNAME").focus();
				return false;
			}
		}
		if(!CheckNumber("PRETKY","<spring:message code= 'LB.X1189' />",true)){
			$("#PRETKY").focus();
			return false;
		}
		if(parseInt($("#PRETKY").val()) > 100){
			//前職工作年資輸入有誤
			//alert("<spring:message code= 'LB.X1066' />");
			errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1066' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			$("#PRETKY").focus();
			return false;
		}
		if(!CheckNumber("PRETKM","<spring:message code= "LB.X1190" />",true)){
			$("#PRETKY").focus();
			return false;
		}
		if(parseInt($("#PRETKM").val()) > 12){
			//前職工作月份輸入有誤
			//alert(" <spring:message code= 'LB.X1067' />");
			errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1067' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			$("#PRETKY").focus();
			return false;
		}
		if(OFFICEY.length < 3 && OFFICEY.length > 0){
			OFFICEY = "0" + OFFICEY;
			$("#OFFICEY").val(OFFICEY);
		}
		if(OFFICEM.length < 2 && OFFICEM.length > 0){
			OFFICEM = "0" + OFFICEM;
			$("#OFFICEM").val(OFFICEM);
		}      
		if(!CheckSelect("PAYSOURCE1","<spring:message code= 'LB.X1191' />","#")){
			$("#PAYSOURCE1").focus();
			return false;
		}
		//其他
		if($("#PAYSOURCE1").val() == "其他" && PAYSOURCE2.length == 0){
			//還款來源選其他，需填入其他的內容
			//alert("<spring:message code= 'LB.X1068' />");
			errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1068' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			$("#PAYSOURCE1").focus();
			return false;      		  		
		}						
		//其他
		if($("#PAYSOURCE1").val() == "其他" && PAYSOURCE2.length > 0){
			$("#PAYSOURCE").val(PAYSOURCE2);
			
		}
		//薪資收入
		if($("#PAYSOURCE1").val() == "薪資收入"){
			$("#PAYSOURCE").val($("#PAYSOURCE1").val());
		}
		if(APPNAME == "${bs.data.hiddenUSERNAME}"){
			$("#APPNAME").val("${bs.data.USERNAME}");
		}
		$("#RELID1").val(RELID1.toUpperCase());
		$("#RELID2").val(RELID2.toUpperCase());
		$("#RELID3").val(RELID3.toUpperCase());
		$("#RELID4").val(RELID4.toUpperCase());
		
		$("#formID").attr("action","${__ctx}/APPLY/CREDIT/apply_creditloan_p7");
		$("#formID").submit();
	});
	$("#resetButton").click(function(){
		
		$("input").not(':input[type=button]').not(':input[type=radio]').val("");
		
// 		$(":radio").prop("checked",false); // Client JQuery Deprecated Symbols
		$('input[type=radio]').prop("checked",false);
		
		$("#BHID").val("#");
		$("#APPKIND").val("#");
		$("#CITY2").val("#");
		$("#ZONE2").val("#");
		$("#CITY1").val("#");
		$("#ZONE1").val("#");
		$("#CITY4").val("#");
		$("#ZONE4").val("#");
		$("#PROFNO").val("#");
		openinput('1');
		fillDefaultData();
	});
});
function setBRH(){
	var BHIDValue = $("#BHID").val();
	
	if(BHIDValue != "#"){
		var BHIDText = $("#BHID option:selected").text();
		
		$("#BANKID").val(BHIDValue);
		$("#BANKNA").val(BHIDText);
	}
}
function changeamt(){
	var APPKINDvalue = $("#APPKIND").val();

	if(APPKINDvalue == "B" || APPKINDvalue == "F"){
	//最高限額100萬元
		$("#AMT1").html("（ <spring:message code= 'LB.D0563_1' />）");
		$("#Repayment48").show();
		$("#Repayment60").show();
		$("#Repayment72").show();
		$("#Repayment84").show();
	}
	if(APPKINDvalue == "E"){
		//最高限額10萬元
		$("#AMT1").html("（ <spring:message code= 'LB.D0563' />）");
		$("#Repayment48").hide();
		$("#Repayment60").hide();
		$("#Repayment72").hide();
		$("#Repayment84").hide();
	}
}
function changefont(type){
	if(type == "1"){
		$("#font1").html($("#APPNAME").val());
	}
  	if(type == "2"){
  		$("#font2").html($("#APPAMT").val());
  	}
  	if(type == "3"){
		$("#font3").html((parseInt($("#INCOME").val()) / 12).toFixed(2));
		$("#font4").html($("#INCOME").val());
	}
}
function openinput(type){
	if(type == "1"){	
		$("#MONEY11").val("");
		$("#MONEY11").prop("disabled",true);//X
		$("#MONEY12").val("");
		$("#MONEY12").prop("disabled",true);//X
	}
	if(type == "2"){	
		$("#MONEY11").prop("disabled",false);//O
		$("#MONEY12").val("");
		$("#MONEY12").prop("disabled",true);//X
    }
	if(type == "3"){
		$("#MONEY11").val("");
		$("#MONEY11").prop("disabled",true);//X
		$("#MONEY12").val("");
		$("#MONEY12").prop("disabled",true);//X
    }
	if(type == "4"){
		$("#MONEY11").prop("disabled",true);//X
		$("#MONEY11").val("");
		$("#MONEY12").prop("disabled",false);//O
    }
	if(type == "5"){
		$("#MONEY11").prop("disabled",false);//O
		$("#MONEY12").prop("disabled",false);//O
    }
}
function doquery(){
	if($("#CITY2").val() != "#"){
		var URI = "${__ctx}/APPLY/CREDIT/getAreaListAjax";
		var rqData = {CITY:$("#CITY2").val()};
		fstop.getServerDataEx(URI,rqData,false,getAreaListFinish);
	}
}
function getAreaListFinish(data){
	if(data.result == true){
		var areaList = $.parseJSON(data.data);
		
		$("#ZONE2").html("");
		// 請選擇市／區鄉鎮
		var ZONE2HTML = "<option value='#'>---<spring:message code= 'LB.X0546' />---</option>";
		for(var x=0;x<areaList.length;x++){
			ZONE2HTML += "<option value='" + areaList[x].AREA + "'>" + areaList[x].AREA + "</option>";
		}
		$("#ZONE2").html(ZONE2HTML);
	}
	else{
		//無法取得市區鄉鎮資料
		//alert("<spring:message code= 'LB.X1069' />");
		errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.X1069' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
			);
	}
}
function doquery1(){
	if($("#ZONE2").val() != "#"){
		var URI = "${__ctx}/APPLY/CREDIT/getZipCodeByCityAreaAjax";
		var rqData = {CITY:$("#CITY2").val(),AREA:$("#ZONE2").val()};
		fstop.getServerDataEx(URI,rqData,false,getZipCodeByCityAreaFinish);
	}
}
function getZipCodeByCityAreaFinish(data){
	if(data.result == true){
		var zipCode = data.data;
		
		$("#ZIP2").val(zipCode);
	}
	else{
		//無法取得郵遞區號資料
		//alert("<spring:message code= "LB.X1070" />");
		errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.X1070' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
			);
	}
}
function doquery2(){
	if($("#CITY1").val() != "#"){
		var URI = "${__ctx}/APPLY/CREDIT/getAreaListAjax";
		var rqData = {CITY:$("#CITY1").val()};
		fstop.getServerDataEx(URI,rqData,false,getAreaListFinish2);
	}
}
function getAreaListFinish2(data){
	if(data.result == true){
		var areaList = $.parseJSON(data.data);
		
		$("#ZONE1").html("");
		//請選擇市／區鄉鎮 
		var ZONE1HTML = "<option value='#'>---<spring:message code= 'LB.X0546' />---</option>";
		for(var x=0;x<areaList.length;x++){
			ZONE1HTML += "<option value='" + areaList[x].AREA + "'>" + areaList[x].AREA + "</option>";
		}
		$("#ZONE1").html(ZONE1HTML);
	}
	else{
		//無法取得市區鄉鎮資料
		//alert("<spring:message code= "LB.X1069" />");
		errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.X1069' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
			);
	}
}
function doquery3(){
	if($("#ZONE1").val() != "#"){
		var URI = "${__ctx}/APPLY/CREDIT/getZipCodeByCityAreaAjax";
		var rqData = {CITY:$("#CITY1").val(),AREA:$("#ZONE1").val()};
		fstop.getServerDataEx(URI,rqData,false,getZipCodeByCityAreaFinish2);
	}
}
function getZipCodeByCityAreaFinish2(data){
	if(data.result == true){
		var zipCode = data.data;
		
		$("#ZIP1").val(zipCode);
	}
	else{
		//無法取得郵遞區號資料
		//	("<spring:message code= "LB.X1070" />");
		errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.X1070' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
			);
	}
}
function doquery4(){
	if($("#CITY4").val() != "#"){
		var URI = "${__ctx}/APPLY/CREDIT/getAreaListAjax";
		var rqData = {CITY:$("#CITY4").val()};
		fstop.getServerDataEx(URI,rqData,false,getAreaListFinish3);
	}
}
function getAreaListFinish3(data){
	if(data.result == true){
		var areaList = $.parseJSON(data.data);
		
		$("#ZONE4").html("");
		//請選擇市／區鄉鎮
		var ZONE4HTML = "<option value='#'>---<spring:message code= 'LB.X0546' />---</option>";
		for(var x=0;x<areaList.length;x++){
			ZONE4HTML += "<option value='" + areaList[x].AREA + "'>" + areaList[x].AREA + "</option>";
		}
		$("#ZONE4").html(ZONE4HTML);
	}
	else{
		//<!-- 無法取得市區鄉鎮資料 -->
		//alert("<spring:message code= 'LB.X1069' />");
		errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.X1069' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
			);
	}
}
function doquery5(){
	if($("#ZONE4").val() != "#"){
		var URI = "${__ctx}/APPLY/CREDIT/getZipCodeByCityAreaAjax";
		var rqData = {CITY:$("#CITY4").val(),AREA:$("#ZONE4").val()};
		fstop.getServerDataEx(URI,rqData,false,getZipCodeByCityAreaFinish3);
	}
}
function getZipCodeByCityAreaFinish3(data){
	if(data.result == true){
		var zipCode = data.data;
		
		$("#ZIP4").val(zipCode);
	}
	else{
		//無法取得郵遞區號資料
		//alert("<spring:message code= 'LB.X1070' />");
		errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.X1070' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
			);
	}
}
function changecks(type){
	var RELNAME1 = $("#RELNAME1").val();
    var RELNAME2 = $("#RELNAME2").val();
    var RELNAME3 = $("#RELNAME3").val();
    var RELNAME4 = $("#RELNAME4").val();
  	var RELID1 = $("#RELID1").val();
  	var RELID2 = $("#RELID2").val();
  	var RELID3 = $("#RELID3").val();
  	var RELID4 = $("#RELID4").val();
  	var OTHCONM1 = $("#OTHCONM1").val();
  	var OTHCOID1 = $("#OTHCOID1").val();
  	var MOTHCONM1 = $("#MOTHCONM1").val();
  	var MOTHCOID1 = $("#MOTHCOID1").val();
  	
	if(type == "1" && RELNAME1.length > 0 && RELID1.length > 0){
		$("#CKS1").val("1");
	}
	if(type == "2" && RELNAME2.length > 0 && RELID2.length > 0){
		$("#CKS2").val("1");
	}
	if(type == "3" && RELNAME3.length > 0 && RELID3.length > 0){
		$("#CKS3").val("1");
	}
	if(type == "4" && RELNAME4.length > 0 && RELID4.length > 0){
		$("#CKS4").val("1");
	}
	if(type == "5" && OTHCONM1.length > 0 && OTHCOID1.length > 0){
		$("#CKP1").val("1");
	}
	if(type == "6" && MOTHCONM1.length > 0 && MOTHCOID1.length > 0){
		$("#CKM1").val("1");
	}
	//其他
	if(type == "7" && $("#PAYSOURCE1").val() == "其他"){
		$("#PAYSOURCE2").prop("disabled",false);
	}
	//薪資收入
	if(type == "7" && $("#PAYSOURCE1").val() == "薪資收入"){
		$("#PAYSOURCE2").prop("disabled",true);
	}
}
function checkperiods(){
	var PERIODSvalue = parseInt($("input[name=PERIODS]:checked").val());
	
	var APPKINDvalue = $("#APPKIND").val();
	
	if(PERIODSvalue > 36 && APPKINDvalue == "E"){
		//貸款期數不得超過36期
		//alert("<spring:message code= "LB.X1071" />");
		errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1071' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
		$("input[name=PERIODS]:checked").focus();
		return false;
	}
	return true;
} 	
function checkhouse(){
	var HOUSEvalue = parseInt($("input[name=HOUSE]:checked").val());
	
	if(HOUSEvalue == "2"){
		if($("#MONEY11").val() == ""){
			//房貸月繳未輸入
			//alert("<spring:message code= 'LB.X1072' />");
			errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1072' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			$("input[name=HOUSE]:checked").focus();
			return false;
		}
		//房貸月繳
		if(!CheckNumber("MONEY11"," <spring:message code= 'LB.D0609_1' />",false)){
			$("input[name=HOUSE]:checked").focus();
			return false;
		}
	}
	if(HOUSEvalue == "4"){
		if($("#MONEY12").val() == ""){
			//房租月繳未輸入
			//alert("<spring:message code= 'LB.X1073' />");
			errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1073' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			$("input[name=HOUSE]:checked").focus();
			return false;
		}
		//房租月繳
		if(!CheckNumber("MONEY12","<spring:message code= 'LB.D0610_1' />",false)){
			$("input[name=HOUSE]:checked").focus();
			return false;
		}
	}
	return true;
}
</script>
</head>
<body>
<!-- header -->
<header>
	<%@ include file="../index/header.jsp"%>
</header>	
<!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<!--申請小額信貸 -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0543" /></a></li>
		</ol>
	</nav>
<!--左邊menu及登入資訊-->
<div class="content row">
	<%@ include file="../index/menu.jsp"%>
<!--快速選單及主頁內容-->
	<main class="col-12">
		<!--主頁內容-->
		<section id="main-content" class="container">
			<h2><!--申請小額信貸 -->
					<spring:message code= "LB.D0543" />
					</h2>
			<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
			<form method="post" id="formID">
				<input type="hidden" name="ADOPID" value="NA01"/>
				<input type="hidden" name="APPIDNO" value="${bs.data.cusidn}"/>
				<input type="hidden" name="APPString" value="${bs.data.APPString}"/>
				<input type="hidden" name="USERNAME" value="${bs.data.USERNAME}"/>
				<input type="hidden" name="APPNAME_OLD" value="${bs.data.USERNAME}"/>
				<input type="hidden" name="BRTHDY" value="${bs.data.BRTHDY}"/>
				<input type="hidden" name="POSTCOD1" value="${bs.data.POSTCOD1}"/>
				<input type="hidden" name="PMTADR" value="${bs.data.PMTADR}"/>
				<input type="hidden" name="PMTADR1" value="${bs.data.PMTADR1}"/>
				<input type="hidden" name="POSTCOD2" value="${bs.data.POSTCOD2}"/>
				<input type="hidden" name="CTTADR" value="${bs.data.CTTADR}"/>
				<input type="hidden" name="CTTADR1" value="${bs.data.CTTADR1}"/>
				<input type="hidden" name="TEL13" value="${bs.data.TEL13}"/>
				<input type="hidden" name="TEL12" value="${bs.data.TEL12}"/>
				<input type="hidden" name="TEL11" value="${bs.data.TEL11}"/>
				<input type="hidden" name="MOBTEL" value="${bs.data.MOBTEL}"/>
				<input type="hidden" id="BANKID" name="BANKID"/>
				<input type="hidden" id="BANKNA" name="BANKNA"/>
				<input type="hidden" id="BRTHDT" name="BRTHDT"/>
				<input type="hidden" id="CKS1" name="CKS1"/>
				<input type="hidden" id="CKS2" name="CKS2"/>
				<input type="hidden" id="CKS3" name="CKS3"/>
				<input type="hidden" id="CKS4" name="CKS4"/>
				<input type="hidden" id="CKP1" name="CKP1"/>
				<input type="hidden" id="CKM1" name="CKM1"/>
				<input type="hidden" id="PAYSOURCE" name="PAYSOURCE"/>
  				<input type="hidden" id="RCVNO" name="RCVNO"/>
  				<input type="hidden" id="ZIP3" name="ZIP3"/>
  				<input type="hidden" id="CITY3" name="CITY3"/>
  				<input type="hidden" id="ZONE3" name="ZONE3"/>
  				<input type="hidden" id="ADDR3" name="ADDR3"/>
  				<input type="hidden" name="STATUS" value="0"/>
				<div class="main-content-block row">
				<div class="col-12">
					<div class="ttb-message">
					<!-- 臺灣中小企業銀行小額信用貸款申請書（集中徵審專用） -->
						<p><spring:message code= "LB.D0555" />（<spring:message code= "LB.D0556" />）</p>
					</div>
					<!-- 為必填 -->
					<div class="text-right"><font color="red"><b style="padding-right: 30px;">※<spring:message code= "LB.D0557" /></b></font></div>
					<table class="table-1 four-col" data-toggle-column="first" id="smalltable">
						<thead>
							<tr>
							<!-- 貸款資料 -->
								<th data-title="<spring:message code= "LB.D0558" />" style="text-align: center"><b><spring:message code= "LB.D0558" /></b></th>
							</tr>
						</thead>
						<tbody>
							<tr>
							<!-- 貸款種類 -->
								<td  style="text-align: left"><font color="red"><b>※</b></font><spring:message code= "LB.D0544" />：
									<select name="APPKIND" class = "custom-select select-input half-input" id="APPKIND" onchange="changeamt()">
										<!-- 卓越圓夢 -->
										<option value="B"><spring:message code= "LB.D0546" /></option>
										<!-- e網輕鬆貸 -->
										<option value="E"><spring:message code= "LB.D0547" /></option>
									</select>
								</td>
							</tr>
							<tr>
								<td  style="text-align:left">
								<!-- 申請金額 -->
									<font color="red"><b>※</b></font><spring:message code= "LB.D0560" />：
									<!-- 新臺幣 XX 萬元整-->
									<spring:message code= "LB.NTD" /><input type="text" class="text-input" id="APPAMT" name="APPAMT" size="4" maxlength="3" onblur="changefont('2')"> 
									<span class="input-unit"><spring:message code= "LB.D0562" /></span>
									
									<font id="AMT1" color="red">${bs.data.APPString}</font>
								</td>
							</tr>
							<tr>
								<td  style="text-align:left">
									<!-- 資金用途 -->
									<font color="red"><b>※</b></font><spring:message code= "LB.D0564" />：
									<!-- 房屋修繕 -->
									<label class="radio-block"> 
											<spring:message code="LB.D0565" /> <input type="radio" name="PURPOSE" value="B" /> <span class="ttb-radio"></span>
									</label> 
									<!-- 旅遊 -->
									<label class="radio-block"> 
											<spring:message code="LB.D0566" /> <input type="radio" name="PURPOSE" value="D" /> <span class="ttb-radio"></span>
									</label> 
									<!-- 購置耐久消費財 -->
									<label class="radio-block"> 
											<spring:message code="LB.D0567" /> <input type="radio" name="PURPOSE" value="E" /> <span class="ttb-radio"></span>
									</label>
									<!-- 購買汽車 -->
									<label class="radio-block"> 
											<spring:message code="LB.D0568" /> <input type="radio" name="PURPOSE" value="G" /> <span class="ttb-radio"></span>
									</label> 
									<!-- 進修 -->
									<label class="radio-block"> 
											<spring:message code="LB.D0569" /> <input type="radio" name="PURPOSE" value="I" /> <span class="ttb-radio"></span>
									</label> 
									<!-- 結婚 -->
									<label class="radio-block"> 
											<spring:message code="LB.D0570" /> <input type="radio" name="PURPOSE" value="J" /> <span class="ttb-radio"></span>
									</label>
									<!-- 子女教育基金 --> 
									<label class="radio-block"> 
											<spring:message code="LB.D0571" /> <input type="radio" name="PURPOSE" value="K" /> <span class="ttb-radio"></span>
									</label> 
									<!-- 其他 -->
									<label class="radio-block"> 
											<spring:message code="LB.D0572" /> <input type="radio" name="PURPOSE" value="Z" /> <span class="ttb-radio"></span>
									</label> 
								</td>
							</tr>
							<tr>
								<td  style="text-align:left">
									<!-- 申請還款期數 -->
									<font color="red"><b>※</b></font><spring:message code= "LB.D0573" />： 
									<!-- 個月 -->
									<label class="radio-block"> 
											12<spring:message code="LB.D0574" /><input type="radio" name="PERIODS" value="12" /> <span class="ttb-radio"></span>
									</label>
									<!-- 個月 -->
									<label class="radio-block"> 
											24<spring:message code="LB.D0574" /><input type="radio" name="PERIODS" value="24" /> <span class="ttb-radio"></span>
									</label> 
									<!-- 個月 -->
									<label class="radio-block"> 
											36<spring:message code="LB.D0574" /><input type="radio" name="PERIODS" value="36" /> <span class="ttb-radio"></span>
									</label> 
									<!-- 個月 -->
									<label id="Repayment48" class="radio-block"> 
											48<spring:message code="LB.D0574" /><input type="radio" name="PERIODS" value="48" /> <span class="ttb-radio"></span>
									</label> 
									<!-- 個月 -->
									<label id="Repayment60" class="radio-block"> 
											60<spring:message code="LB.D0574" /><input type="radio" name="PERIODS" value="60" /> <span class="ttb-radio"></span>
									</label> 
									<!-- 個月 -->
									<label id="Repayment72" class="radio-block"> 
											72<spring:message code="LB.D0574" /><input type="radio" name="PERIODS" value="72" /> <span class="ttb-radio"></span>
									</label> 
									<!-- 個月 -->
									<label id="Repayment84" class="radio-block"> 
											84<spring:message code="LB.D0574" /><input type="radio" name="PERIODS" value="84" /> <span class="ttb-radio"></span>
									</label>  
								</td>
							</tr>
							<tr>
								<td  style="text-align:left">
<!-- 指定對保分行 -->
									<font color="red"><b>※</b></font><spring:message code= "LB.D0575" />：
       								<select name="BHID" id="BHID" class="custom-select select-input half-input" onchange="setBRH()"> 
       								<!-- 請選擇分行 -->
										<option value="#">---<spring:message code= "LB.X1003" />---</option>
										<c:forEach var="ADMBHCONTACT" items="${bs.data.ADMBHCONTACTList}">
											<c:if test="${ADMBHCONTACT.ADBRANCHID != '893'}">
												<option value="${ADMBHCONTACT.ADBRANCHID}">${ADMBHCONTACT.ADBRANCHNAME}</option>
											</c:if>
										</c:forEach>
									</select>
								</td>
							</tr>
						</tbody>
						<thead>
							<tr>
								<th data-title="<spring:message code= "LB.D0576" />"  style="text-align:center">
									<!-- 個人資料 -->
									<b><spring:message code= "LB.D0576" /></b>
								</th>
							</tr>	
						</thead>
						<tbody>
							<tr>
								<!-- 姓名 -->
								<td  style="text-align: left"><font color="red"><b>※</b></font><spring:message code= "LB.D0203" />：<input
										type="text" class="text-input" id="APPNAME" name="APPNAME"
										value="" size="10" maxlength="50"
										onblur="changefont('1')" /></td>
							</tr>
							<tr>
								<!-- 性別 -->
								<td  style="text-align: left"><font color="red"><b>※</b></font><spring:message code= "LB.D0578" />：
									<!-- 男 女 -->	
									<label class="radio-block"> 
											<spring:message code="LB.D0579" /><input type="radio" name="SEX" value="1" /> <span class="ttb-radio"></span>
									</label>  
									<label class="radio-block"> 
											<spring:message code="LB.D0580" /><input type="radio" name="SEX" value="2" /> <span class="ttb-radio"></span>
									</label>  
								</td>
							</tr>
							<tr>
								<!-- 身分證字號 -->
								<td  style="text-align: left"><font color="red"><b>※</b></font><spring:message code= "LB.D0581" />：${bs.data.hiddenCUSIDN}
								</td>
							</tr>
							<tr>
								<td  style="text-align:left">
<!-- 出生日期 -->
									<font color="red"><b>※</b></font><spring:message code= "LB.D0582" />：
<!-- 民國 -->
<!-- 年 -->
									<spring:message code= "LB.D0583" /><input type="text"  class="text-input"id="BIRTHY" name="BIRTHY" value="${bs.data.BIRTHY}" size="4" maxlength="3"/><spring:message code= "LB.D0089_2" />
<!-- 月 -->
									<input type="text"  class="text-input" id="BIRTHM" name="BIRTHM" value="${bs.data.BIRTHM}" size="3" maxlength="2"/><spring:message code= "LB.D0089_3" />
<!-- 日 -->
									<input type="text" class="text-input" id="BIRTHD" name="BIRTHD" value="${bs.data.BIRTHD}" size="3" maxlength="2"/><spring:message code= "LB.Day" />
								</td>
							</tr>		
							<tr>
									<td  style="text-align:left">
<!-- 教育程度 -->
									<font color="red"><b>※</b></font><spring:message code= "LB.D0055" />：
									<!-- 博士 -->
									<label class="radio-block"> 
											<spring:message code="LB.D0588" /><input type="radio" name="EDUS" value="1" /> <span class="ttb-radio"></span>
									</label>
									<!-- 碩士 -->
									<label class="radio-block"> 
											<spring:message code="LB.D0589" /><input type="radio" name="EDUS" value="2" /> <span class="ttb-radio"></span>
									</label>
									<!-- 大學 -->
									<label class="radio-block"> 
											<spring:message code="LB.D0590" /><input type="radio" name="EDUS" value="3" /> <span class="ttb-radio"></span>
									</label>
									<!-- 專科 -->
									<label class="radio-block"> 
											<spring:message code="LB.D0591" /><input type="radio" name="EDUS" value="4" /> <span class="ttb-radio"></span>
									</label>
									<!-- 高中職  -->
									<label class="radio-block"> 
											<spring:message code="LB.D0592" /><input type="radio" name="EDUS" value="5" /> <span class="ttb-radio"></span>
									</label>
									<!-- 其他  -->
									<label class="radio-block"> 
											<spring:message code="LB.D0572" /><input type="radio" name="EDUS" value="9" /> <span class="ttb-radio"></span>
									</label>
								</td>
							</tr>		
							<tr>
								<td  style="text-align:left">
									<!-- 婚姻狀況 -->
									<font color="red"><b>※</b></font><spring:message code= "LB.D0057" />：
									<!-- 已婚 -->
									<label class="radio-block"> 
											<spring:message code="LB.D0595" /><input type="radio" name="MARITAL" value="1" /> <span class="ttb-radio"></span>
									</label> 
									<!-- 未婚 -->
									<label class="radio-block"> 
											<spring:message code="LB.D0596" /><input type="radio" name="MARITAL" value="2" /> <span class="ttb-radio"></span>
									</label>
									<!-- 其他 -->
									<label class="radio-block"> 
											<spring:message code="LB.D0572" /><input type="radio" name="MARITAL" value="3" /> <span class="ttb-radio"></span>
									</label>
								</td>
							</tr>
							<tr>
								<td  style="text-align:left">
<!-- 子女數 -->
<!-- 人 -->
									<font color="red"><b></b></font><spring:message code= "LB.D0602_1" />：<input type="text" class="text-input" id="CHILDNO" name="CHILDNO" size="2" maxlength="2"/><spring:message code= "LB.D0602_2" />
								</td>
							</tr>
							<tr>
								<td  style="text-align:left">
<!-- 行動電話 -->
									<font color="red"><b>※</b></font><spring:message code= "LB.D0069" />：
									<input type="text" class="text-input" id="CPHONE" name="CPHONE" value="${bs.data.MOBTEL}" size="11" maxlength="10"/>
								</td></tr><tr>
								<td  style="text-align:left">
<!-- 通訊電話 -->
									<font color="red"><b>※</b></font><spring:message code= "LB.D0599" />：
									<input type="text" class="text-input" id="PHONE_01" name="PHONE_01" value="${bs.data.TEL11}" size="4" maxlength="3"/>
									－
									<input type="text" class="text-input" id="PHONE_02" name="PHONE_02" value="${bs.data.TEL12}" size="11" maxlength="10"/>
<!-- 分機 -->
									<spring:message code= "LB.D0095" />：
									<input type="text" class="text-input" id="PHONE_03" name="PHONE_03" value="${bs.data.TEL13}" size="6" maxlength="5"/>
								</td></tr><tr>
								<td  style="text-align:left">
<!-- 戶籍電話 -->
									<spring:message code= "LB.D0144" />：
									<input type="text" class="text-input" id="PHONE_11" name="PHONE_11" size="4" maxlength="3"/>
									－
									<input type="text" class="text-input" id="PHONE_12" name="PHONE_12" size="11" maxlength="10"/>
								</td>
							</tr>		
							<tr>
								<td  style="text-align:left">
									<!-- 住宅狀況 -->
									<font color="red"><b>※</b></font><spring:message code= "LB.D0603" />：
<!-- 自有或配偶持有無設定抵押權或設押本行 -->
<label class="radio-block"> 
<spring:message code= "LB.D0604" /><input type="radio" name="HOUSE" value="1" onclick="openinput('1')"/> <span class="ttb-radio"></span>
</label>
<!-- 自有或配偶持有已設定他行抵押權 -->
<label class="radio-block"> 
<spring:message code= "LB.D0605" /><input type="radio" name="HOUSE" value="2" onclick="openinput('2')"/> <span class="ttb-radio"></span>
</label>
<!-- 直系親屬持有 -->
<label class="radio-block"> 
<spring:message code= "LB.D0606" /><input type="radio" name="HOUSE" value="3" onclick="openinput('3')"/> <span class="ttb-radio"></span>
</label>
<!-- 租賃或宿舍 -->
<label class="radio-block"> 
<spring:message code= "LB.D0068" /><input type="radio" name="HOUSE" value="4" onclick="openinput('4')"/> <span class="ttb-radio"></span>
</label>
<!-- 其他 -->
<label class="radio-block"> 
<spring:message code= "LB.D0572" /><input type="radio" name="HOUSE" value="5" onclick="openinput('5')"/> <span class="ttb-radio"></span>
</label>
									<br/>
<!-- 房貸月繳 -->
									<spring:message code= "LB.D0609_1" />：
<!-- 元 -->
									<input type="text" id="MONEY11" class="text-input" name="MONEY11" size="9" maxlength="8" disabled/><spring:message code= "LB.Dollar" />
									&nbsp;&nbsp;&nbsp;&nbsp;
<!-- 房租月繳 -->
									<spring:message code= "LB.D0610_1" />：
<!-- 仟元 -->
									<input type="text" id="MONEY12" class="text-input" name="MONEY12" size="4" maxlength="3" disabled><spring:message code= "LB.D0610_2" />
								</td>
							</tr>
							</tr>
							<tr>
									<td  style="text-align:left">
<!-- 現住地址 -->
									<font color="red"><b>※</b></font><spring:message code= "LB.D0611" />：
<!-- 郵遞區號 -->
									<spring:message code= "LB.D0061" />：<input type="text" class="text-input" id="ZIP2" name="ZIP2" value="${bs.data.POSTCOD2}" size="4" maxlength="3"/>
<!-- 縣市別 -->
									<spring:message code= "LB.D0059" />：
									<select class="custom-select select-input half-input" name="CITY2" id="CITY2" onchange="doquery()">
									<!-- 請選擇縣市別 -->
									<option value="#">---<spring:message code= "LB.X0545" />---</option>
									<c:forEach var="city" items="${bs.data.cityList}">
										<option value="${city.CITY}">${city.CITY}</option>
									</c:forEach>
									</select>
<!-- 市/區鄉鎮 -->
									<spring:message code= "LB.D0060" />：
									<select class="custom-select select-input half-input" name="ZONE2" id="ZONE2" onchange="doquery1()"> 
									<!-- 請選擇市／區鄉鎮 -->
										<option value="#">---<spring:message code= "LB.X0546" />---</option>
									<c:forEach var="CTTADR" items="${bs.data.CTTADRList}">
										<option value="${CTTADR.AREA}">${CTTADR.AREA}</option>
									</c:forEach>
									</select>
									<input type="text" class="text-input" id="ADDR2" name="ADDR2" value="${bs.data.CTTADR1}" size="50" maxlength="25"/>
								</td>
							</tr>
							<tr>
									<td  style="text-align:left">
<!-- 戶籍地址 -->
									<font color="red"><b>※</b></font><spring:message code= "LB.D0143" />：
<!-- 郵遞區號 -->
									<spring:message code= "LB.D0061" />：<input type="text" class="text-input" id="ZIP1" name="ZIP1" value="${bs.data.POSTCOD1}" size="4" maxlength="3"/>
<!-- 縣市別 -->
									<spring:message code= "LB.D0059" />：
									<select name="CITY1" class="custom-select select-input half-input" id="CITY1" onchange="doquery2()"> 
									<!-- 請選擇縣市別 -->
										<option value="#">---<spring:message code= "LB.X0545" />---</option> 
										<c:forEach var="city" items="${bs.data.cityList}">
											<option value="${city.CITY}">${city.CITY}</option>
										</c:forEach>
									</select>
<!-- 市/區鄉鎮 -->
									<spring:message code= "LB.D0060" />：
									<select name="ZONE1" class="custom-select select-input half-input" id="ZONE1" onchange="doquery3()"> 
									<!-- 請選擇市／區鄉鎮 -->
										<option value="#">---<spring:message code= "LB.X0546" />---</option>
										<c:forEach var="PMTADR" items="${bs.data.PMTADRList}">
											<option value="${PMTADR.AREA}">${PMTADR.AREA}</option>
										</c:forEach>
									</select>
									<input type="text" class="text-input" id="ADDR1" name="ADDR1" value="${bs.data.PMTADR1}" size="50" maxlength="25"/>
								</td>
							</tr>
							<tr>
								<td  style="text-align:left">
									<font color="red"><b>※</b></font>E-MAIL：
									<input type="text" class="text-input" id="EMAIL" name="EMAIL" value="${bs.data.DPMYEMAIL}" size="31" maxlength="30"/>
								</td> 
							</tr>
							</tbody>
							<thead>
								<tr>
									<th  data-title="<spring:message code= "LB.D0616" />：<spring:message code= "LB.D0617" />" style="text-align:left">
									<!-- 配偶及二親等以內血親 ：為遵循銀行法第33條之3所定授信限額之規定而取得「同一關係人」之基本資料包括祖（外祖）父母、父母、兄弟姊妹、子女、孫（外孫）子女-->
									<b><font color="red"><b>※</b></font><spring:message code= "LB.D0616" />：<spring:message code= "LB.D0617" /></b>
									</th> 
								</tr>
							</thead>
							<tbody>
								<tr>
								<!-- 稱謂 -->
									<td  style="text-align: left"><spring:message code= "LB.D0618" />： <select name="RELTYPE1"
										class="custom-select select-input half-input" id="RELTYPE1" onchange="changecks('1')">
										<!-- 請選擇稱謂 -->
											<option value="#">---<spring:message code= "LB.X1006" />---</option>
											<!-- 祖父母 -->
											<option value="祖父母"><spring:message code= "LB.X1007" /></option>
											<!-- 外祖父母 -->
											<option value="外祖父母"><spring:message code= "LB.X1008" /></option>
											<!-- 父母 -->
											<option value="父母"><spring:message code= "LB.X1009" /></option>
											<!-- 配偶 -->
											<option value="配偶"><spring:message code= "LB.X1010" /></option>
											<!-- 兄弟姊妹 -->
											<option value="兄弟姊妹"><spring:message code= "LB.X1011" /></option>
											<!-- 子女 -->
											<option value="子女"><spring:message code= "LB.X1012" /></option>
											<!-- 孫子女 -->
											<option value="孫子女"><spring:message code= "LB.X1013" /></option>
											<!-- 外孫子女 -->
											<option value="外孫子女"><spring:message code= "LB.X1014" /></option>
									<!-- 姓名 -->
									</select> <spring:message code= "LB.D0203" />： <input type="text" class="text-input" id="RELNAME1" name="RELNAME1" size="10"
										maxlength="5" onblur="changecks('1')" />
									</td>
								</tr>
								<tr>
									<!-- 身分證字號 -->
									<td  style="text-align: left"><spring:message code= "LB.D0581" />： <input type="text" class="text-input"
										id="RELID1" name="RELID1" size="11" maxlength="10"
										onblur="changecks('1')" />
									</td>
								</tr>
								<tr>
									<!-- 稱謂 -->
									<td  style="text-align: left"><spring:message code= "LB.D0618" />： <select name="RELTYPE2"
										class="custom-select select-input half-input" id="RELTYPE2" onchange="changecks('2')">
											<!-- 請選擇稱謂 -->
											<option value="#">---<spring:message code= "LB.X1006" />---</option>
											<!-- 祖父母 -->
											<option value="祖父母"><spring:message code= "LB.X1007" /></option>
											<!-- 外祖父母 -->
											<option value="外祖父母"><spring:message code= "LB.X1008" /></option>
											<!-- 父母 -->
											<option value="父母"><spring:message code= "LB.X1009" /></option>
											<!-- 配偶 -->
											<option value="配偶"><spring:message code= "LB.X1010" /></option>
											<!-- 兄弟姊妹 -->
											<option value="兄弟姊妹"><spring:message code= "LB.X1011" /></option>
											<!-- 子女 -->
											<option value="子女"><spring:message code= "LB.X1012" /></option>
											<!-- 孫子女 -->
											<option value="孫子女"><spring:message code= "LB.X1013" /></option>
											<!-- 外孫子女 -->
											<option value="外孫子女"><spring:message code= "LB.X1014" /></option>
									<!-- 姓名 -->
									</select> <spring:message code= "LB.D0203" />： <input type="text" id="RELNAME2" name="RELNAME2" size="10" class="text-input"
										maxlength="5" onblur="changecks('2')" />
									</td>
								</tr>
								<tr>
									<!-- 身分證字號 -->
									<td  style="text-align: left"><spring:message code= "LB.D0581" />： <input type="text" class="text-input"
										id="RELID2" name="RELID2" size="11" maxlength="10"
										onblur="changecks('2')" />
									</td>
								</tr>
								<tr>
								<!-- 稱謂 -->
									<td  style="text-align: left"><spring:message code= "LB.D0618" />： <select name="RELTYPE3" class="custom-select select-input half-input"
										id="RELTYPE3" onchange="changecks('3')">
										<!-- 請選擇稱謂 -->
											<option value="#">---<spring:message code= "LB.X1006" />---</option>
											<!-- 祖父母 -->
											<option value="祖父母"><spring:message code= "LB.X1007" /></option>
											<!-- 外祖父母 -->
											<option value="外祖父母"><spring:message code= "LB.X1008" /></option>
											<!-- 父母 -->
											<option value="父母"><spring:message code= "LB.X1009" /></option>
											<!-- 配偶 -->
											<option value="配偶"><spring:message code= "LB.X1010" /></option>
											<!-- 兄弟姊妹 -->
											<option value="兄弟姊妹"><spring:message code= "LB.X1011" /></option>
											<!-- 子女 -->
											<option value="子女"><spring:message code= "LB.X1012" /></option>
											<!-- 孫子女 -->
											<option value="孫子女"><spring:message code= "LB.X1013" /></option>
											<!-- 外孫子女 -->
											<option value="外孫子女"><spring:message code= "LB.X1014" /></option>
								<!-- 姓名 -->
									</select> <spring:message code= "LB.D0203" />： <input type="text" id="RELNAME3" name="RELNAME3" size="10" class="text-input"
										maxlength="5" onblur="changecks('3')" />
									</td>
								</tr>
								<tr>
									<!-- 身分證字號 -->
									<td  style="text-align: left"><spring:message code= "LB.D0581" />： <input type="text" class="text-input"
										id="RELID3" name="RELID3" size="11" maxlength="10"
										onblur="changecks('3')" />
									</td>
								</tr>
								<tr>
									<!-- 稱謂 -->
									<td  style="text-align: left"><spring:message code= "LB.D0618" />： <select name="RELTYPE4" class="custom-select select-input half-input"
										id="RELTYPE4" onchange="changecks('4')">
											<!-- 請選擇稱謂 -->
											<option value="#">---<spring:message code= "LB.X1006" />---</option>
											<!-- 祖父母 -->
											<option value="祖父母"><spring:message code= "LB.X1007" /></option>
											<!-- 外祖父母 -->
											<option value="外祖父母"><spring:message code= "LB.X1008" /></option>
											<!-- 父母 -->
											<option value="父母"><spring:message code= "LB.X1009" /></option>
											<!-- 配偶 -->
											<option value="配偶"><spring:message code= "LB.X1010" /></option>
											<!-- 兄弟姊妹 -->
											<option value="兄弟姊妹"><spring:message code= "LB.X1011" /></option>
											<!-- 子女 -->
											<option value="子女"><spring:message code= "LB.X1012" /></option>
											<!-- 孫子女 -->
											<option value="孫子女"><spring:message code= "LB.X1013" /></option>
											<!-- 外孫子女 -->
											<option value="外孫子女"><spring:message code= "LB.X1014" /></option>
									<!-- 姓名 -->
									</select> <spring:message code= "LB.D0203" />： <input type="text" id="RELNAME4" name="RELNAME4" size="10" class="text-input"
										maxlength="5" onblur="changecks('4')" />
									</td>
								</tr>
								<tr>
									<!-- 身分證字號 -->
									<td  style="text-align: left"><spring:message code= "LB.D0581" />： <input type="text" class="text-input"
										id="RELID4" name="RELID4" size="11" maxlength="10"
										onblur="changecks('4')" />
									</td>
								</tr>
							</tbody>
							<thead>
								<tr>
								<!-- 職 業 資 料 -->
									<th data-title="<spring:message code= "LB.W0831" />" style="text-align: center"><b><spring:message code= "LB.W0831" /></b></th>
								</tr>
							</thead>
							<tbody>
								<tr>
<!-- 本人擔任負責人之其他企業名稱 -->
									<td  style="text-align: left"><spring:message code= "LB.D0620" />： <input
										type="text" class="text-input" id="OTHCONM1" name="OTHCONM1" size="32"
										maxlength="16" onblur="changecks('5')" />
									</td></tr><tr>
									<!-- 統一編號 -->
									<td  style="text-align: left"><spring:message code= "LB.D0621" />： <input type="text"
										id="OTHCOID1" class="text-input" name="OTHCOID1" size="11" maxlength="10"
										onblur="changecks('5')" />
									</td>
								</tr>
								<tr>
									<!-- 配偶擔任負責人之企業名稱 -->
									<td  style="text-align: left"><spring:message code= "LB.D0622" />： 
									<input type="text" class="text-input" id="MOTHCONM1" name="MOTHCONM1" size="32"
										maxlength="16" onblur="changecks('6')" />
									</td></tr><tr>
								<!-- 統一編號 -->
									<td  style="text-align: left"><spring:message code= "LB.D0621" />： <input type="text"
										id="MOTHCOID1" class="text-input" name="MOTHCOID1" size="11" maxlength="10"
										onblur="changecks('6')" />
									</td>
								</tr>
							</tbody>
							<thead>
								<tr>
<!-- 本人服務單位 -->
									<th data-title="<spring:message code= "LB.D0623" />" style="text-align: center"><b><spring:message code= "LB.D0623" /></b></th>
								</tr>
							</thead>
							<tbody>
								<tr>
							<!-- 公司名稱 -->
									<td  style="text-align: left"><font color="red"><b>※</b></font><spring:message code= "LB.D0086" />：<input
										type="text" class="text-input" id="CAREERNAME" name="CAREERNAME" size="32"
										maxlength="16" />
									</td></tr><tr>
<!-- 統一編號 -->
									<td  style="text-align: left"><spring:message code= "LB.D0621" />： <input type="text" class="text-input"
										id="CAREERIDNO" name="CAREERIDNO" size="11" maxlength="10" />
									</td>
								</tr>
								<tr>
<!-- 年收入 -->
<!-- 萬元 -->
									<td  style="text-align: left"><font color="red"><b>※</b></font><spring:message code= "LB.D0625_1" />：
										<input type="text" id="INCOME" name="INCOME" size="7" class="text-input"
										maxlength="6" onblur="changefont('3')" />
										<span class="input-unit"><spring:message code= "LB.D0088_2" /></span>
										</td>
								</tr>
								<tr>
<!-- 公司地址 -->
									<td  style="text-align: left"><font color="red"><b>※</b></font><spring:message code= "LB.D0090" />：
<!-- 郵遞區號 -->
<!-- 縣市別 -->
										<spring:message code= "LB.D0061" />：<input type="text" id="ZIP4" class="text-input" name="ZIP4" size="4"
										maxlength="3" /> <spring:message code= "LB.D0059" />： <select name="CITY4" id="CITY4" class="custom-select select-input half-input"
										onchange="doquery4()">
										<!-- 請選擇縣市別 -->
											<option value="#">---<spring:message code= "LB.X0545" />---</option>
											<c:forEach var="city" items="${bs.data.cityList}">
												<option value="${city.CITY}">${city.CITY}</option>
											</c:forEach>
											<!-- 市/區鄉鎮 -->
									</select> <spring:message code= "LB.D0060" />： <select name="ZONE4" id="ZONE4"  class="custom-select select-input half-input" onchange="doquery5()">
											<!-- 請選擇市／區鄉鎮 -->
											<option value="#">---<spring:message code= "LB.X0546" />---</option>
									</select> <input type="text" id="ADDR4" name="ADDR4" size="50" class="text-input"
										maxlength="25" /></td>
								</tr>
								<tr>
									<!-- 公司電話 -->
									<!-- 分機 -->
<!-- 職稱 -->
									<td  style="text-align: left"><font color="red"><b>※</b></font><spring:message code= "LB.D0094" />：
										<input type="text" id="PHONE_41" name="PHONE_41" size="4" class="text-input"
										maxlength="3" /> － <input type="text" id="PHONE_42" class="text-input"
										name="PHONE_42" size="11" maxlength="10" /> <spring:message code= "LB.D0095" />：<input
										type="text" id="PHONE_43" name="PHONE_43" size="6" class="text-input"
										maxlength="5" /> <font color="red"><b>※</b></font><spring:message code= "LB.D0087" />： 
										<select	name="PROFNO" id="PROFNO" class="custom-select select-input half-input">
<!-- 請選擇職稱 -->
											<option value="#">---<spring:message code= "LB.X0558" />---</option>
<!-- 民意代表 -->
											<option value="01"><spring:message code= "LB.X0559" /></option>
<!-- 公司負責人／董事／總經理／股東 -->	
											<option value="02"><spring:message code= "LB.X0560" /></option>
<!-- 主管 -->
											<option value="03"><spring:message code= "LB.X0561" /></option>
<!-- 一般職員 -->
											<option value="04"><spring:message code= "LB.X0562" /></option>
<!-- 業務人員 -->
											<option value="05"><spring:message code= "LB.X0563" /></option>
<!-- 公營事業職工 -->
											<option value="06"><spring:message code= "LB.X0564" /></option>
<!-- 文化藝術工作者 -->
											<option value="07"><spring:message code= "LB.X0565" /></option>
<!-- 家庭主婦 -->
											<option value="08"><spring:message code= "LB.X0566" /></option>
<!-- 廚師 -->
											<option value="09"><spring:message code= "LB.X0567" /></option>
<!-- 臨時人員 -->
											<option value="10"><spring:message code= "LB.X0568" /></option>
<!-- 退休人員 -->
											<option value="11"><spring:message code= "LB.X0569" /></option>
<!-- 學生 -->
											<option value="12"><spring:message code= "LB.D0081" /></option>
<!-- 無業 -->
											<option value="13"><spring:message code= "LB.D0897" /></option>
<!-- 自營登記之司機 -->
											<option value="14"><spring:message code= "LB.X0572" /></option>
<!-- 有營登或稅籍之自營業主 -->
											<option value="15"><spring:message code= "LB.X0573" /></option>
<!-- 無營登或稅籍之自營業主 -->
											<option value="16"><spring:message code= "LB.X0574" /></option>
<!-- 作業（技術或操作）員 -->
											<option value="17"><spring:message code= "LB.X0575" /></option>
<!-- 工程師 -->
											<option value="18"><spring:message code= "LB.X0576" /></option>
<!-- 財務人員 -->
											<option value="19"><spring:message code= "LB.X0577" /></option>
<!-- 駕駛（司機） -->
											<option value="20"><spring:message code= "LB.X0578" /></option>
<!-- 警衛 -->
											<option value="21"><spring:message code= "LB.X0579" /></option>
<!-- 工友 -->
											<option value="22"><spring:message code= "LB.X0580" /></option>
<!-- 技工 -->
											<option value="23"><spring:message code= "LB.X0581" /></option>
<!-- 約聘人員 -->
											<option value="24"><spring:message code= "LB.X0582" /></option>
<!-- 教師 -->
											<option value="25"><spring:message code= "LB.X0583" /></option>
<!-- 律師 -->
											<option value="26"><spring:message code= "LB.X0584" /></option>
<!-- 醫師 -->
											<option value="27"><spring:message code= "LB.X0585" /></option>
<!-- 會計師 -->
											<option value="28"><spring:message code= "LB.X0586" /></option>
<!-- 護理人員 -->
											<option value="29"><spring:message code= "LB.X0587" /></option>
<!-- 專業人員 -->
											<option value="30"><spring:message code= "LB.X0588" /></option>
<!-- 保全人員 -->
											<option value="31"><spring:message code= "LB.X0589" /></option>
									</select></td>
								</tr>
								<tr>
								<!-- 到職日 -->
									<td  style="text-align: left"><font color="red"><b>※</b></font> <spring:message code= "LB.D0629" />：
									<!-- 民國 年 月-->
										<spring:message code= "LB.D0583" /><input type="text" id="OFFICEY" name="OFFICEY" size="4" class="text-input"
										maxlength="3" /><spring:message code= "LB.D0089_2" /> <input type="text" id="OFFICEM" class="text-input"
										name="OFFICEM" size="3" maxlength="2" /> <spring:message code= "LB.D0089_3" /></td>
								</tr>
																<tr>
<!-- 前職公司名稱 -->
									<td  style="text-align: left"><spring:message code= "LB.D0630" />： <input type="text" class="text-input"
										id="PRECARNAME" name="PRECARNAME" size="32" maxlength="16" />
										</td>
								</tr>
								<tr>
									<td>
									<!-- 前職工作年資 -->
									<!-- 年 個月 -->
										 <spring:message code= "LB.D0631" />： <input type="text" id="PRETKY" name="PRETKY" size="3" class="text-input" 
										maxlength="2" /> <spring:message code= "LB.D0089_2" /> <input type="text" id="PRETKM" class="text-input"
										name="PRETKM" size="3" maxlength="2" /> <spring:message code= "LB.D0574" />
										</td>
								</tr>
								<tr>
								<!-- 以上所填資料均按實填列，如有不實或漏報願負一切法律責任。 -->
									<td  style="text-align: center"><font
										color="red"><b><spring:message code= "LB.D0633" /></b> </font></td>
								</tr>
							</tbody>
							<thead>
								<tr>
									<!-- 收入聲明書 -->
									<th data-title="<spring:message code= "LB.D0634" />" style="text-align: center"><b><spring:message code= "LB.D0634" /></b></th>
								</tr>
							</thead>
							<tbody>
								<tr>
								<!-- 立書人 -->
								<!-- 本次擬向臺灣中心企業銀行申請消費者信用貸款 -->
								<!-- 萬元，特此聲明本人月收入新臺幣 -->
								<!-- 萬元，年收入新臺幣 -->
								<!-- 萬元，還款來源為 -->
									<td class="font1" style="text-align: left; -ms-word-break: break-all;"><spring:message code= "LB.X1046" /><font id="font1"		
										color="red"></font> <spring:message code= "LB.X1047" /><font id="font2"
										color="red"></font><spring:message code= "LB.D0635_2" /> <font id="font3"
										color="red"></font><spring:message code= "LB.D0635_3" /> <font id="font4" color="red"></font><spring:message code= "LB.D0635_4" />
										<select name="PAYSOURCE1" id="PAYSOURCE1" class="custom-select select-input half-input"
										onchange="changecks('7')">
											<!-- 請選擇還款來源 -->
											<option value="#">---<spring:message code= "LB.D0636" />---</option>
											<!-- 薪資收入 -->
											<option value="薪資收入"><spring:message code= "LB.D0637" /></option>
											<!-- 其他（客戶自行填載）  -->
											<option value="其他"><spring:message code= "LB.D0638" />）</option>
									</select> 
									<!-- 並保證前項聲明事項均無不實。 -->
									<input type="text" class="text-input" name="PAYSOURCE2" id="PAYSOURCE2" size="32"
										maxlength="16">，<spring:message code= "LB.D0639" />
									</td>
								</tr>
							</tbody>
						</table>

						<input type="button" id="resetButton" value="<spring:message code="LB.Re_enter"/>" class="ttb-button btn-flat-gray"/>
						<input type="button" id="okButton" value="<spring:message code="LB.Confirm"/>" class="ttb-button btn-flat-orange"/>	

				</div>
				</div>
			</form>
		</section>
	</main>
</div>
<%@ include file="../index/footer.jsp"%>
</body>
</html>