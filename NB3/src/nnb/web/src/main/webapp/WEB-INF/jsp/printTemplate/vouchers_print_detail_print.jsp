<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ include file="../__import_js.jsp"%> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin">
	<br>
	<br>
	<br>
	<br>
                <c:choose>
                	<c:when test="${FORM != '5C' && FORM != '60' && FORM != '61' && FORM != '90' && FORM != '96' }">
                	   <table border="0" width="100%" id="table3" style="border-collapse: collapse" cellpadding="0">
					<tr>
						<td rowspan="2" width="480">
						<p align="right"><b><font face="標楷體" style="font-size: 16pt">
						各類所得扣繳暨免扣繳憑單</font><font face="標楷體" size="4">&nbsp;&nbsp;&nbsp; <br>
						</font>
						</b><font face="標楷體">Withholding &amp; Non-Withholding&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br>
						Tax Statement(金融機構電子申報專用)&nbsp;&nbsp;&nbsp; </font></td>
						<td  width="487">　</td>
					</tr>
					<tr>
						<td height="57">
						<table border="0" width="100%" id="table11" style="border-collapse: collapse"                       cellspacing="0" cellpadding="0">
							<tr>
								<td style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 3px; padding: 0" width="487" align="left"><font face="標楷體" style="font-size: 9pt">租賃房屋之房屋稅籍編號:</font></td>
							</tr>
							<tr>
								<td height="52" style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" width="487" align="left"><font face="標楷體" style="font-size: 9pt">租賃房屋地址:</font></td>
							</tr>
						</table>
						</td>
					</tr>
					</table>
				<table border="0" width="100%" id="table2" style="border-collapse: collapse"                       cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<table border="0" width="100%" id="table4" style="border-collapse: collapse"                       cellspacing="0" cellpadding="0">
							<tr>
								<td align="center" width="255" style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 3px; padding: 0">
								<font face="標楷體" style="font-size: 9pt">扣繳單位統一編號</font></td>
								<td align="center"  width="74" style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 3px; padding: 0"><font face="標楷體" style="font-size: 9pt">稽徵機關</font></td>
								<td align="center" width="295"  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 3px; padding: 0">
								<font face="標楷體" style="font-size: 9pt">製 單 編 號</font></td>
								<td align="center" width="341"  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 3px; padding: 0">
								<font face="標楷體" style="font-size: 9pt">格式代號及所得類別Category of Income</font></td>
							</tr>
							<tr>
								<td align="center" width="255" style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0">${BUSIDN }</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="74">${CTYIDN }${TAXBAID }</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="295">${BUSIDN }${SEQCLM }</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="341">${FORM }</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table border="0" width="100%" id="table5" style="border-collapse: collapse"                       cellspacing="0" cellpadding="0">
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="253">
								<font face="標楷體" style="font-size: 9pt">所得人統一編(證)號<br>
								Taxpayer's ID No.</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="199">
								<font face="標楷體" style="font-size: 9pt">國內有無住所<br>
								<u>Residency</u></font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="320">
								<font face="標楷體" style="font-size: 9pt">所得人、執業別代號、其他所得給付項目或外僑護照號碼&nbsp;&nbsp;&nbsp; Passport No.</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="80" colspan="2">
								<font face="標楷體" style="font-size: 9pt"><u>國家代碼<br>
								Country<br>
								Code</u></font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="111" colspan="2">
								<font face="標楷體" style="font-size: 9pt"><u>適用租稅協定<br>
								Tax<br>
								Agreement</u></font></td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="253">${CUSIDN }</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="199">
								<font face="標楷體" style="font-size: 9pt">有( ｖ )&nbsp; 
								無(&nbsp; )</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="320">${TAXPYCD }</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="37">${COUNTRY }</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="34"></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="53">${TAX }</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="55"></td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table border="0" width="100%" id="table6" style="border-collapse: collapse"                       cellspacing="0" cellpadding="0">
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="16%"><font face="標楷體">
								<span style="font-size: 9pt">所得人姓名<br>
								</span>
								<font style="font-size: 9pt">Name of Taxpayer</font></font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="left" width="37%">${NAME }</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="47%">
								<p align="left"><font face="標楷體" size="1"><u>本給付年度內按所得人護照入出境章戳日期累計在華是否已滿183天? </u></font><input type="checkbox" name="C1" <c:if test="${LIVE183 =='Y' }">checked</c:if> disabled>是<input type="checkbox" name="C2" <c:if test="${LIVE183 =='N' }">checked</c:if> disabled>否</td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="16%"><font face="標楷體">
								<span style="font-size: 9pt">所得人地址<br>
												Present Address</span></font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="left" colspan="2">${ADDRESS}</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table border="0" width="100%" id="table7" style="border-collapse: collapse"                       cellspacing="0" cellpadding="0">
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="429"><font face="標楷體" style="font-size: 9pt">所得所屬年月 Period of Income</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="167"><font face="標楷體">
								<span style="font-size: 9pt">所得給付年度<br>
								</span>
								<font style="font-size: 9pt">Year of Payment</font></font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="371">
								<font face="標楷體" style="font-size: 9pt">
								依勞退條例自願提繳之退休金額(E)</font></td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="429"><font face="標楷體" style="font-size: 9pt">自${YEARLY } 
								年&nbsp;&nbsp;&nbsp;&nbsp;01&nbsp;&nbsp; 月至${YEARLY } 年&nbsp;&nbsp;&nbsp;12&nbsp;&nbsp;&nbsp;月<br>
								From&nbsp; &nbsp; Year&nbsp; &nbsp;&nbsp; 
								Month To&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Year&nbsp;&nbsp;&nbsp;&nbsp; 
								Month</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="167">${YEARLY }</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="371">${BACKAMT }</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table border="0" width="100%" id="table8" height="134" style="border-collapse: collapse"                       cellspacing="0" cellpadding="0">
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="279" height="64"><font face="標楷體">
								<font style="font-size: 9pt">給付總額(A)Total Amount Paid</font><br>
								<font size="1">(結算申報時應按本欄數額填報)<br>
								The above amount should be reported in the Individual Income Tax Return</font></font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="150" height="64">
								<font face="標楷體" style="font-size: 9pt">扣 繳 
								率(<u>B</u>)<br>
								Withholding Rate</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="290" height="64" colspan="2">
								<font face="標楷體" style="font-size: 9pt">
								扣繳稅額(<u>C=C1-C2</u>)<u><br>
								</u>Net Withholding Tax</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" height="64" width="248">
								<font face="標楷體" style="font-size: 9pt">給付淨額<u>(D=A-C)<br>
								</u>Net Payment</font></td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="279" rowspan="3">${TAMPAY }</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="150" rowspan="3">
								<p align="center"><font face="標楷體">${N106RATE } %</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="290" colspan="2" height="27">${AMTTAX }</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" rowspan="3" width="248">${NETPAY }</td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="132" height="33">
								<font face="標楷體" style="text-decoration: underline" size="1">
								應扣繳稅額(C1=AXB)Withholding Tax</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="157" height="33">
								<font face="標楷體" style="text-decoration: underline" size="1">
								股利或盈餘抵繳稅額(C2)Creditable Surtax</font></td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="132">${W_TAX }</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="157">${C_TAX }</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table border="0" width="100%" id="table9" style="border-collapse: collapse"                       cellspacing="0" cellpadding="0">
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="44" rowspan="3">
								<font face="標楷體" style="font-size: 9pt">
								存號款明帳細</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="230"><c:choose><c:when test="${ACN1 != '' }">${ACN1}</c:when><c:otherwise>&emsp;</c:otherwise></c:choose></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="230"><c:choose><c:when test="${ACN2 != '' }">${ACN2}</c:when><c:otherwise>&emsp;</c:otherwise></c:choose></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="230"><c:choose><c:when test="${ACN3 != '' }">${ACN3}</c:when><c:otherwise>&emsp;</c:otherwise></c:choose></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="230"><c:choose><c:when test="${ACN4 != '' }">${ACN4}</c:when><c:otherwise>&emsp;</c:otherwise></c:choose></td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="230"><c:choose><c:when test="${ACN5 != '' }">${ACN5}</c:when><c:otherwise>&emsp;</c:otherwise></c:choose></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="230"><c:choose><c:when test="${ACN6 != '' }">${ACN6}</c:when><c:otherwise>&emsp;</c:otherwise></c:choose></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="230"><c:choose><c:when test="${ACN7 != '' }">${ACN7}</c:when><c:otherwise>&emsp;</c:otherwise></c:choose></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="230"><c:choose><c:when test="${ACN8 != '' }">${ACN8}</c:when><c:otherwise>&emsp;</c:otherwise></c:choose></td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="230"><c:choose><c:when test="${ACN9 != '' }">${ACN9}</c:when><c:otherwise>&emsp;</c:otherwise></c:choose></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="230"><c:choose><c:when test="${ACN10 != '' }">${ACN10}</c:when><c:otherwise>&emsp;</c:otherwise></c:choose></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="230"><c:choose><c:when test="${ACN11 != '' }">${ACN11}</c:when><c:otherwise>&emsp;</c:otherwise></c:choose></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="230"><c:choose><c:when test="${ACN12 != '' }">${ACN12}</c:when><c:otherwise>&emsp;</c:otherwise></c:choose></td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table border="0" width="100%" id="table10" style="border-collapse: collapse"                       cellspacing="0" cellpadding="0">
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" colspan="2"  width="523">
								<font face="標楷體" style="font-size: 9pt">扣&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
								款&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 單&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
								位</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="440">
								<font face="標楷體" style="font-size: 9pt">格式代號說明 Category 
								of Income</font></td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="145">
								<font face="標楷體" style="font-size: 9pt">名&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
								稱</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="378">
								<font face="標楷體" style="font-size: 9pt">臺 灣 
								中 小 企 業 銀 行</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; border-bottom-style: solid; border-bottom-width: 3px; padding: 0" align="left" width="440" rowspan="3">
								<font face="標楷體" size="1">
								${FORM}:${TRANS_FORM}&nbsp;&nbsp;&nbsp;&nbsp;<br><br><br><br>
								</font></td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="145">
								<font face="標楷體" style="font-size: 9pt">地&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
								址</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="378">
								<font face="標楷體" style="font-size: 9pt">台 北 
								市 塔 城 街 30 號</font></td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; border-bottom-style: solid; border-bottom-width: 3px; padding: 0" align="center" width="145">
								<font face="標楷體" style="font-size: 9pt">扣繳義務人</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; border-bottom-style: solid; border-bottom-width: 3px; padding: 0" align="center" width="378">${TBB_OWNER}</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				
				<div style="font-family: 標楷體" align="left"><font style="font-size: 8.5pt">※<u>依勞工退休金條例規定自願提繳之退休金或年金保險費，合計在每月工資6%範圍內，免計入薪資給付總額，其金額應另行填寫於(E)欄。</u><br>
				第２聯：備查聯　交所得人保存備查<br>
				Copy II For the taxpayer's reference.</font></div>
                	</c:when>
                	<c:otherwise>
                		<table border="0" width="100%" id="table3" style="border-collapse: collapse" cellpadding="0">
					<tr>
						<td width="887">
						<p align="center"><b><font face="標楷體" style="font-size: 16pt">
						各類所得扣繳暨免扣繳憑單</font><font face="標楷體" size="4">&nbsp;&nbsp;&nbsp; <br>
						</font>
						</b><font face="標楷體">Withholding &amp; Non-Withholding Tax Statement&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br>
						(For the Income of Individuals to be taxed separately and of Profit - Seeking Enterprise)<br>
						(個人分離課稅所得及營利事業同類所得電子申報專用)</font></td>
					</tr>
					</table>
				<table border="0" width="100%" id="table2" style="border-collapse: collapse"                       cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<table border="0" width="100%" id="table4" style="border-collapse: collapse"                       cellspacing="0" cellpadding="0">
							<tr>
								<td align="center" width="253" style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 3px; padding: 0">
								<font face="標楷體" style="font-size: 9pt">扣繳單位統一編號</font></td>
								<td align="center"  width="74" style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 3px; padding: 0"><font face="標楷體" style="font-size: 9pt">稽徵機關</font></td>
								<td align="center" width="295"  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 3px; padding: 0">
								<font face="標楷體" style="font-size: 9pt">製 單 編 號</font></td>
								<td align="center" width="341"  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 3px; padding: 0">
								<font face="標楷體" style="font-size: 9pt">格式代號及所得類別<br>Category of Income</font></td>
							</tr>
							<tr>
								<td align="center" width="253" style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0">${BUSIDN}</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="74">${CTYIDN}${TAXBAID}</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="295">${BUSIDN}${SEQCLM}</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="341">${FORM}</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table border="0" width="100%" id="table5" style="border-collapse: collapse"                       cellspacing="0" cellpadding="0">
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="253">
								<font face="標楷體" style="font-size: 9pt">所得人統一編(證)號<br>
								Taxpayer's ID No.</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="199">
								<font face="標楷體" style="font-size: 9pt">國內有無住所<br>
								<u>Residency</u></font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="300">
								<font face="標楷體" style="font-size: 9pt">所得人代號或外僑護照號碼<br>Passport No.</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="90" colspan="2">
								<font face="標楷體" style="font-size: 9pt"><u>國家代碼<br>
								Country	Code</u></font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="121" colspan="2">
								<font face="標楷體" style="font-size: 9pt"><u>適用租稅協定<br>
								Tax	Agreement</u></font></td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="253">${CUSIDN}</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="199">
								<font face="標楷體" style="font-size: 9pt">有( ｖ )&nbsp; 
								無(&nbsp; )</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="320">${TAXPYCD}</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="37">${COUNTRY}</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="34"></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="53">${TAX}</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="55"></td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table border="0" width="100%" id="table6" style="border-collapse: collapse"                       cellspacing="0" cellpadding="0">
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="26%"><font face="標楷體">
								<span style="font-size: 9pt">所得人姓名 Name of Taxpayer </span></font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="left" width="27%">${NAME}</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="47%">
								<p align="left"><font face="標楷體" size="1"><u>本給付年度內按所得人護照入出境章戳日期累計在華是否已滿183天? </u></font><input type="checkbox" name="C1" <c:if test="${LIVE183 =='Y' }">checked</c:if> disabled>是<input type="checkbox" name="C2" <c:if test="${LIVE183 =='N' }">checked</c:if> disabled>否</td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="16%"><font face="標楷體">
								<span style="font-size: 9pt">所得人地址 Present Address</span></font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="left" colspan="2">${ADDRESS}</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table border="0" width="100%" id="table7" style="border-collapse: collapse" cellspacing="0" cellpadding="0">
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="479"><font face="標楷體" style="font-size: 9pt">所得所屬年月 Period of Income</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="217"><font face="標楷體">
								<span style="font-size: 9pt">所得給付年度<br>
								</span>
								<font style="font-size: 9pt">Year of Payment</font></font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="271">
								<font face="標楷體" style="font-size: 9pt">
								扣 繳 率 <br>Withholding Rate</font></td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="479"><font face="標楷體" style="font-size: 9pt">自${YEARLY} 
								年&nbsp;&nbsp;&nbsp;&nbsp;01&nbsp;&nbsp; 月至${YEARLY} 年&nbsp;&nbsp;&nbsp;12&nbsp;&nbsp;&nbsp;月<br>
								From&nbsp; &nbsp; Year&nbsp; &nbsp;&nbsp; 
								Month To&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Year&nbsp;&nbsp;&nbsp;&nbsp; 
								Month</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="217">${YEARLY} </td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="271">${N106RATE} %</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table border="0" width="100%" id="table8" height="134" style="border-collapse: collapse" cellspacing="0" cellpadding="0">
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="329" height="64"><font face="標楷體">
								<font style="font-size: 9pt">給付總額(A)<br>Total Amount Paid</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="340" height="64">
								<font face="標楷體" style="font-size: 9pt">扣繳稅額(B)<br>Net Withholding Tax</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" height="64" width="298">
								<font face="標楷體" style="font-size: 9pt">給付淨額(A)-(B)<br>	Net Payment</font></td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="329">${TAMPAY}</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="340" height="27">${AMTTAX}</td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="298">${NETPAY}</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table border="0" width="100%" id="table9" style="border-collapse: collapse"                       cellspacing="0" cellpadding="0">
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="5%">
								<font face="標楷體" style="font-size: 9pt">說<br>明</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="left" width="95%">
								本憑單之各項所得，所得人為個人時，給付總額經依規定扣繳稅款後，免併計綜合所得總額課稅，其已扣繳稅款亦不得抵繳應納稅額；所得人非屬個人時，除發票日在98年12月31日以前之短期票券利息及依法免辦理結算申報者外，仍應併入營利事業所得額課稅，其已扣繳稅款得抵繳應納稅額。
								</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table border="0" width="100%" id="table10" style="border-collapse: collapse"                       cellspacing="0" cellpadding="0">
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" colspan="2"  width="523">
								<font face="標楷體" style="font-size: 9pt">扣&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
								繳&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 單&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
								位</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="440">
								<font face="標楷體" style="font-size: 9pt">格式代號說明 Category 
								of Income</font></td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="145">
								<font face="標楷體" style="font-size: 9pt">名&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
								稱</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="378">
								<font face="標楷體" style="font-size: 9pt">臺 灣 
								中 小 企 業 銀 行</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 3px; border-top-style: solid; border-top-width: 1px; border-bottom-style: solid; border-bottom-width: 3px; padding: 0" align="left" width="440" rowspan="3">
								<font face="標楷體" size="1">
								5C：公債、公司債或金融債券利息<br>
								60：資產基礎證券分配之利息<br>
								61：附條件交易之利息<br>
								90：告發或檢舉獎金<br>
								96：結構型商品交易之所得
								</font></td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="145">
								<font face="標楷體" style="font-size: 9pt">地&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
								址</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; padding: 0" align="center" width="378">
								<font face="標楷體" style="font-size: 9pt">台 北 
								市 塔 城 街 30 號</font></td>
							</tr>
							<tr>
								<td  style="border-left-style: solid; border-left-width: 3px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; border-bottom-style: solid; border-bottom-width: 3px; padding: 0" align="center" width="145">
								<font face="標楷體" style="font-size: 9pt">扣繳義務人</font></td>
								<td  style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; border-bottom-style: solid; border-bottom-width: 3px; padding: 0" align="center" width="378">${TBB_OWNER}</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				
				<div style="font-family: 標楷體" align="left"><font style="font-size: 8.5pt">※本扣繳憑單適用於99年1月1日起給付之所得。<br>
				第２聯：備查聯　交所得人保存備查。Copy II For the taxpayer's reference.</font></div>
                	</c:otherwise>
                </c:choose>
				 </div> 
</body>
</html>