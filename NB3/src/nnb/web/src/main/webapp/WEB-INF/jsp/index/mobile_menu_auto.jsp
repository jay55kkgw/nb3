<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
	<script type="text/javascript">
		$(document).ready(function(){
// 			var cusid = $.base64.decode('${sessionScope.cusidn}');
// 			$("#trading_ticket").attr("href","https://10.16.22.32/icb/webbank/login_tbb.jsp?name=" + cusid);
		});
	</script>
        <div class="d-lg-none header-container">
            <div class="header-content collapse" id="header-content">
				<!-- 歡迎標頭 -->
                <div class="mobile-login">
                    <span class="id-name">
                    <spring:message code="LB.Welcome_1"/> ${sessionScope.dpusername} <spring:message code="LB.Welcome_2"/>！
                    </span>
                    <span class="id-time">
                    <fmt:setLocale value="${__i18n_locale}"/>
					<fmt:formatDate value="${sessionScope.login_time}" type="both" dateStyle="full"/>
					&nbsp;<spring:message code="LB.Login_successful"/>
                    
<!--                     2017/12/26(二)16:23:36 登入成功 -->
                    </span>
                    <!-- 自動登出剩餘時間 -->
					<span class="id-time">
						<spring:message code="LB.Remaining"/>
						<!-- 分 -->
						<font color="red" id="mobile-countdownMin"></font>
						:
						<!-- 秒 -->
						<font color="red" id="mobile-countdownSec"></font>
						
						<img src="${__ctx}/img/reset-timeout-${__i18n_locale}.svg" onclick="timeLogout()">
					</span>
                    
                    <button class="ttb-button btn-flat-darkgray" onClick="fstop.logout( '${__ctx}'+'/logout_aj' , '${__ctx}'+'/login')" type="button" name="button"><spring:message code="LB.Logout"/></button>
                </div>

                <div class="sub-nav">
                    <ul class="sub-nav-ul">


						<!--第一層 -->
                        <li class="sub-nav-ul-li sub-nav-ul-li-1">
							<!--我的首頁-->
                            <a href="#" class="sub-nav-link" onclick="fstop.getPage('${pageContext.request.contextPath}'+'/INDEX/index','', '')">
                                <img src="${__ctx}/img/menu-icon-01.svg?a=${jscssDate}" alt='<spring:message code="LB.My_home"/>'>
                                <spring:message code="LB.My_home"/>
                            </a>
                        </li>    	
                        
						<!--第一層 -->
<!--                         <li class="sub-nav-ul-li sub-nav-ul-li-1"> -->
<!-- 							帳戶總覽 -->
<%--                             <a href="#" class="sub-nav-link" onclick="fstop.getPage('${pageContext.request.contextPath}'+'/OVERVIEW/initview','', '')"> --%>
<%--                                 <img src="${__ctx}/img/menu-icon-02.svg?a=${jscssDate}" alt='<spring:message code="LB.Account_Overview"/>'> --%>
<%--                                 <spring:message code="LB.Account_Overview"/> --%>
<!--                             </a> -->
<!--                         </li>    	 -->

						<!--第一層 -->
                        <li class="sub-nav-ul-li sub-nav-ul-li-1">
							<!--臺幣服務 -->
                            <a class="sub-nav-link" data-toggle="collapse" href="#ul-li-3" role="button" aria-expanded="false" aria-controls="ul-li-3" >
                                <img src="${__ctx}/img/menu-icon-03.svg?a=${jscssDate}" alt='<spring:message code="LB.NTD_Services"/>'>
                                <spring:message code="LB.NTD_Services"/>
                            </a>
							<!--第二層 --><!--臺幣帳務查詢 -->
                            <div id="ul-li-3" class="collapse">
                                <ul class="sub-nav-ul">
    								<c:forEach var="dataListLayer1" items="${ sessionScope.menu.data.sort_menu[0].SEED_LAYER }">
    									<c:if test="${ dataListLayer1.ADOPID.equals('NTDService') }">
    										<c:forEach var="dataListLayer2" items="${ dataListLayer1.SEED_LAYER }" varStatus="idx2">
			                                    <li class="sub-nav-ul-li sub-nav-ul-li-2">
			                                        <a data-toggle="collapse" href="#ul-li-3-${idx2.index}" role="button" aria-expanded="false" aria-controls="ul-li-3-${idx2.index}">
			                                       		${dataListLayer2.ADOPNAME}
			                                        </a>
			                                        <!--第三層 -->
			                                        <div id="ul-li-3-${idx2.index}" class="collapse">
			                                            <ul class="sub-nav-ul">
    														<c:forEach var="dataListLayer3" items="${ dataListLayer2.SEED_LAYER }" varStatus="idx3">
				                                                <li class="sub-nav-ul-li sub-nav-ul-li-3">
											                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('0') }">
												                    	<a href='#' onclick="fstop.getPage('${pageContext.request.contextPath}'+'${dataListLayer3.URL}','', '')">
												                   			${dataListLayer3.ADOPNAME}
												                   		</a>
											                   		</c:if>
											                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('1') }">
												                    	<a target="_blank" href="${pageContext.request.contextPath}${dataListLayer3.URL}">
																			${dataListLayer3.ADOPNAME}
																		</a>
											                    	</c:if>
											                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('2') }">
												                    	<a target="_blank" href="${dataListLayer3.URL}${sessionScope.cusidn}">
																			${dataListLayer3.ADOPNAME}
																		</a>
											                    	</c:if>
				                                                </li>
    														</c:forEach>
			                                            </ul>
			                                        </div>
			                                    </li>
    										</c:forEach>
    									</c:if>
    								</c:forEach>
								</ul>
                       		</div><!--第二層 --><!--臺幣帳務查詢  end -->
                      	</li>
						<!--第一層 -->
                        <li class="sub-nav-ul-li sub-nav-ul-li-1">
							<!--外幣服務 -->
                            <a class="sub-nav-link" data-toggle="collapse" href="#ul-li-4" role="button" aria-expanded="false" aria-controls="ul-li-4" >
                                <img src="${__ctx}/img/menu-icon-04.svg?a=${jscssDate}" alt='<spring:message code="LB.FX_Service"/>'>
                                <spring:message code="LB.FX_Service"/>
                            </a>
							<!--第二層 --><!--外幣帳務查詢 -->
                            <div id="ul-li-4" class="collapse">
                                <ul class="sub-nav-ul">
    								<c:forEach var="dataListLayer1" items="${ sessionScope.menu.data.sort_menu[0].SEED_LAYER }">
    									<c:if test="${ dataListLayer1.ADOPID.equals('FXCurrency') }">
    										<c:forEach var="dataListLayer2" items="${ dataListLayer1.SEED_LAYER }" varStatus="idx2">
			                                    <li class="sub-nav-ul-li sub-nav-ul-li-2">
			                                        <a data-toggle="collapse" href="#ul-li-4-${idx2.index}" role="button" aria-expanded="false" aria-controls="ul-li-4-${idx2.index}">
			                                       		${dataListLayer2.ADOPNAME}
			                                        </a>
			                                        <!--第三層 -->
			                                        <div id="ul-li-4-${idx2.index}" class="collapse">
			                                            <ul class="sub-nav-ul">
    														<c:forEach var="dataListLayer3" items="${ dataListLayer2.SEED_LAYER }" varStatus="idx3">
				                                                <li class="sub-nav-ul-li sub-nav-ul-li-3">
											                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('0') }">
												                    	<a href='#' onclick="fstop.getPage('${pageContext.request.contextPath}'+'${dataListLayer3.URL}','', '')">
												                   			${dataListLayer3.ADOPNAME}
												                   		</a>
											                   		</c:if>
											                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('1') }">
												                    	<a target="_blank" href="${pageContext.request.contextPath}${dataListLayer3.URL}">
																			${dataListLayer3.ADOPNAME}
																		</a>
											                    	</c:if>
											                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('2') }">
												                    	<a target="_blank" href="${dataListLayer3.URL}${sessionScope.cusidn}">
																			${dataListLayer3.ADOPNAME}
																		</a>
											                    	</c:if>
				                                                </li>
    														</c:forEach>
			                                            </ul>
			                                        </div>
			                                    </li>
    										</c:forEach>
    									</c:if>
    								</c:forEach>
								</ul><!-- 外幣sub-nav-ul end-->
                            </div><!--第二層 --><!--外幣帳務查詢  end -->
						</li>
                            
						<!--第一層 -->
						<li class="sub-nav-ul-li sub-nav-ul-li-1">
							<!-- 繳費繳稅 -->
							<a class="sub-nav-link" data-toggle="collapse" href="#ul-li-5" role="button" aria-expanded="false" aria-controls="ul-li-5" >
							    <img src="${__ctx}/img/menu-icon-05.svg?a=${jscssDate}" alt='<spring:message code="LB.Loan_Inquiry"/>'>
							    	<spring:message code= "LB.W0366" />
							</a>
							<!--第二層 -->
							<div id="ul-li-5" class="collapse">
								<ul class="sub-nav-ul">
    								<c:forEach var="dataListLayer1" items="${ sessionScope.menu.data.sort_menu[0].SEED_LAYER }">
    									<c:if test="${ dataListLayer1.ADOPID.equals('fee_tax') }">
    										<c:forEach var="dataListLayer2" items="${ dataListLayer1.SEED_LAYER }" varStatus="idx2">
			                                    <li class="sub-nav-ul-li sub-nav-ul-li-2">
			                                        <a data-toggle="collapse" href="#ul-li-5-${idx2.index}" role="button" aria-expanded="false" aria-controls="ul-li-5-${idx2.index}">
			                                       		${dataListLayer2.ADOPNAME}
			                                        </a>
			                                        <!--第三層 -->
			                                        <div id="ul-li-5-${idx2.index}" class="collapse">
			                                            <ul class="sub-nav-ul">
    														<c:forEach var="dataListLayer3" items="${ dataListLayer2.SEED_LAYER }" varStatus="idx3">
				                                                <li class="sub-nav-ul-li sub-nav-ul-li-3">
											                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('0') }">
												                    	<a href='#' onclick="fstop.getPage('${pageContext.request.contextPath}'+'${dataListLayer3.URL}','', '')">
												                   			${dataListLayer3.ADOPNAME}
												                   		</a>
											                   		</c:if>
											                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('1') }">
												                    	<a target="_blank" href="${pageContext.request.contextPath}${dataListLayer3.URL}">
																			${dataListLayer3.ADOPNAME}
																		</a>
											                    	</c:if>
											                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('2') }">
												                    	<a target="_blank" href="${dataListLayer3.URL}${sessionScope.cusidn}">
																			${dataListLayer3.ADOPNAME}
																		</a>
											                    	</c:if>
				                                                </li>
    														</c:forEach>
			                                            </ul>
			                                        </div>
			                                    </li>
    										</c:forEach>
    									</c:if>
    								</c:forEach>
								</ul>
							</div>
						</li>
						<!-- 繳費繳稅  end-->
                            
                        <!--第一層 -->
                  		<li class="sub-nav-ul-li sub-nav-ul-li-1">
							<!-- 貸款專區 -->
                            <a class="sub-nav-link" data-toggle="collapse" href="#ul-li-6" role="button" aria-expanded="false" aria-controls="ul-li-6" >
                                <img src="${__ctx}/img/menu-icon-06.svg?a=${jscssDate}" alt='<spring:message code="LB.Loan_Inquiry"/>'>
                                <spring:message code="LB.Loan_Service"/>
                            </a>
							<!--第二層 -->
                            <div id="ul-li-6" class="collapse">
                                <ul class="sub-nav-ul">
    								<c:forEach var="dataListLayer1" items="${ sessionScope.menu.data.sort_menu[0].SEED_LAYER }">
    									<c:if test="${ dataListLayer1.ADOPID.equals('loanservice') }">
    										<c:forEach var="dataListLayer2" items="${ dataListLayer1.SEED_LAYER }" varStatus="idx2">
			                                    <li class="sub-nav-ul-li sub-nav-ul-li-2">
			                                        <a data-toggle="collapse" href="#ul-li-6-${idx2.index}" role="button" aria-expanded="false" aria-controls="ul-li-6-${idx2.index}">
			                                       		${dataListLayer2.ADOPNAME}
			                                        </a>
			                                        <!--第三層 -->
			                                        <div id="ul-li-6-${idx2.index}" class="collapse">
			                                            <ul class="sub-nav-ul">
    														<c:forEach var="dataListLayer3" items="${ dataListLayer2.SEED_LAYER }" varStatus="idx3">
				                                                <li class="sub-nav-ul-li sub-nav-ul-li-3">
											                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('0') }">
												                    	<a href='#' onclick="fstop.getPage('${pageContext.request.contextPath}'+'${dataListLayer3.URL}','', '')">
												                   			${dataListLayer3.ADOPNAME}
												                   		</a>
											                   		</c:if>
											                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('1') }">
												                    	<a target="_blank" href="${pageContext.request.contextPath}${dataListLayer3.URL}">
																			${dataListLayer3.ADOPNAME}
																		</a>
											                    	</c:if>
											                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('2') }">
												                    	<a target="_blank" href="${dataListLayer3.URL}${sessionScope.cusidn}">
																			${dataListLayer3.ADOPNAME}
																		</a>
											                    	</c:if>
				                                                </li>
    														</c:forEach>
			                                            </ul>
			                                        </div>
			                                    </li>
    										</c:forEach>
    									</c:if>
    								</c:forEach>
                                	
								</ul>
                            </div>
                         </li>
                         <!--第一層 --><!-- 貸款專區  end-->
                         
                         <!--第一層 -->
						 <li class="sub-nav-ul-li sub-nav-ul-li-1">
							 <!-- 基金 -->
                             <a class="sub-nav-link" data-toggle="collapse" href="#ul-li-7" role="button" aria-expanded="false" aria-controls="ul-li-7" >
                                 <img src="${__ctx}/img/menu-icon-07.svg?a=${jscssDate}" alt='<spring:message code="LB.Loan_Inquiry"/>'>
                                 <spring:message code="LB.Funds"/>
                             </a>
							 <!--第二層 -->
                             <div id="ul-li-7" class="collapse">
                                <ul class="sub-nav-ul">
    								<c:forEach var="dataListLayer1" items="${ sessionScope.menu.data.sort_menu[0].SEED_LAYER }">
    									<c:if test="${ dataListLayer1.ADOPID.equals('fundservice') }">
    										<c:forEach var="dataListLayer2" items="${ dataListLayer1.SEED_LAYER }" varStatus="idx2">
			                                    <li class="sub-nav-ul-li sub-nav-ul-li-2">
			                                        <a data-toggle="collapse" href="#ul-li-7-${idx2.index}" role="button" aria-expanded="false" aria-controls="ul-li-7-${idx2.index}">
			                                       		${dataListLayer2.ADOPNAME}
			                                        </a>
			                                        <!--第三層 -->
			                                        <div id="ul-li-7-${idx2.index}" class="collapse">
			                                            <ul class="sub-nav-ul">
    														<c:forEach var="dataListLayer3" items="${ dataListLayer2.SEED_LAYER }" varStatus="idx3">
				                                                <li class="sub-nav-ul-li sub-nav-ul-li-3">
											                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('0') }">
												                    	<a href='#' onclick="fstop.getPage('${pageContext.request.contextPath}'+'${dataListLayer3.URL}','', '')">
												                   			${dataListLayer3.ADOPNAME}
												                   		</a>
											                   		</c:if>
											                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('1') }">
												                    	<a target="_blank" href="${pageContext.request.contextPath}${dataListLayer3.URL}">
																			${dataListLayer3.ADOPNAME}
																		</a>
											                    	</c:if>
											                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('2') }">
												                    	<a target="_blank" href="${dataListLayer3.URL}${sessionScope.cusidn}">
																			${dataListLayer3.ADOPNAME}
																		</a>
											                    	</c:if>
				                                                </li>
    														</c:forEach>
			                                            </ul>
			                                        </div>
			                                    </li>
    										</c:forEach>
    									</c:if>
    								</c:forEach>
                                </ul>
                        	 </div>
                       	 </li>
                         <!--第一層 --><!-- 基金   end-->
	                        
                         <!--第一層 -->
                         <li class="sub-nav-ul-li sub-nav-ul-li-1">
							 <!-- 黃金存摺 -->
                             <a class="sub-nav-link" data-toggle="collapse" href="#ul-li-8" role="button" aria-expanded="false" aria-controls="ul-li-8" >
                                <img src="${__ctx}/img/menu-icon-08.svg?a=${jscssDate}" alt='<spring:message code="LB.Loan_Inquiry"/>'>
                                 	<spring:message code= "LB.W1428" /> 
                             </a>
							 <!--第二層 -->
                             <div id="ul-li-8" class="collapse">
                                <ul class="sub-nav-ul">
    								<c:forEach var="dataListLayer1" items="${ sessionScope.menu.data.sort_menu[0].SEED_LAYER }">
    									<c:if test="${ dataListLayer1.ADOPID.equals('goldqryservice') }">
    										<c:forEach var="dataListLayer2" items="${ dataListLayer1.SEED_LAYER }" varStatus="idx2">
			                                    <li class="sub-nav-ul-li sub-nav-ul-li-2">
			                                        <a data-toggle="collapse" href="#ul-li-8-${idx2.index}" role="button" aria-expanded="false" aria-controls="ul-li-8-${idx2.index}">
			                                       		${dataListLayer2.ADOPNAME}
			                                        </a>
			                                        <!--第三層 -->
			                                        <div id="ul-li-8-${idx2.index}" class="collapse">
			                                            <ul class="sub-nav-ul">
    														<c:forEach var="dataListLayer3" items="${ dataListLayer2.SEED_LAYER }" varStatus="idx3">
				                                                <li class="sub-nav-ul-li sub-nav-ul-li-3">
											                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('0') }">
												                    	<a href='#' onclick="fstop.getPage('${pageContext.request.contextPath}'+'${dataListLayer3.URL}','', '')">
												                   			${dataListLayer3.ADOPNAME}
												                   		</a>
											                   		</c:if>
											                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('1') }">
												                    	<a target="_blank" href="${pageContext.request.contextPath}${dataListLayer3.URL}">
																			${dataListLayer3.ADOPNAME}
																		</a>
											                    	</c:if>
											                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('2') }">
												                    	<a target="_blank" href="${dataListLayer3.URL}${sessionScope.cusidn}">
																			${dataListLayer3.ADOPNAME}
																		</a>
											                    	</c:if>
				                                                </li>
    														</c:forEach>
			                                            </ul>
			                                        </div>
			                                    </li>
    										</c:forEach>
    									</c:if>
    								</c:forEach>
                                </ul>
                        	 </div>
                        </li>
                        <!--第一層 --><!-- 黃金存摺  end-->
	                        
                        <!--第一層 -->
                        <li class="sub-nav-ul-li sub-nav-ul-li-1">
							<!-- 信用卡 -->
                            <a class="sub-nav-link" data-toggle="collapse" href="#ul-li-9" role="button" aria-expanded="false" aria-controls="ul-li-9" >
                                <img src="${__ctx}/img/menu-icon-09.svg?a=${jscssDate}" alt='<spring:message code="LB.Credit_Card"/>'>
                                <spring:message code="LB.Credit_Card"/>
                            </a>
							<!--第二層 -->
                            <div id="ul-li-9" class="collapse">
                                <ul class="sub-nav-ul">
    								<c:forEach var="dataListLayer1" items="${ sessionScope.menu.data.sort_menu[0].SEED_LAYER }">
    									<c:if test="${ dataListLayer1.ADOPID.equals('creditcard') }">
    										<c:forEach var="dataListLayer2" items="${ dataListLayer1.SEED_LAYER }" varStatus="idx2">
			                                    <li class="sub-nav-ul-li sub-nav-ul-li-2">
			                                        <a data-toggle="collapse" href="#ul-li-9-${idx2.index}" role="button" aria-expanded="false" aria-controls="ul-li-9-${idx2.index}">
			                                       		${dataListLayer2.ADOPNAME}
			                                        </a>
			                                        <!--第三層 -->
			                                        <div id="ul-li-9-${idx2.index}" class="collapse">
			                                            <ul class="sub-nav-ul">
    														<c:forEach var="dataListLayer3" items="${ dataListLayer2.SEED_LAYER }" varStatus="idx3">
				                                                <li class="sub-nav-ul-li sub-nav-ul-li-3">
											                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('0') }">
												                    	<a href='#' onclick="fstop.getPage('${pageContext.request.contextPath}'+'${dataListLayer3.URL}','', '')">
												                   			${dataListLayer3.ADOPNAME}
												                   		</a>
											                   		</c:if>
											                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('1') }">
												                    	<a target="_blank" href="${pageContext.request.contextPath}${dataListLayer3.URL}">
																			${dataListLayer3.ADOPNAME}
																		</a>
											                    	</c:if>
											                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('2') }">
												                    	<a target="_blank" href="${dataListLayer3.URL}${sessionScope.cusidn}">
																			${dataListLayer3.ADOPNAME}
																		</a>
											                    	</c:if>
				                                                </li>
    														</c:forEach>
			                                            </ul>
			                                        </div>
			                                    </li>
    										</c:forEach>
    									</c:if>
    								</c:forEach>
								</ul>
                            </div>
                         </li><!--第一層 --><!-- 信用卡  end-->
	                        
                         <!--第一層 -->
                         <li class="sub-nav-ul-li sub-nav-ul-li-1">
							<!-- 線上服務專區 -->
                            <a class="sub-nav-link" data-toggle="collapse" href="#ul-li-10" role="button" aria-expanded="false" aria-controls="ul-li-10" >
                                <img src="${__ctx}/img/menu-icon-10.svg?a=${jscssDate}" alt='<spring:message code="LB.Personal_Service"/>'>
                                	<spring:message code= "LB.X0262" />
                            </a>
							<!--第二層 -->
                            <div id="ul-li-10" class="collapse">
                                <ul class="sub-nav-ul">
    								<c:forEach var="dataListLayer1" items="${ sessionScope.menu.data.sort_menu[0].SEED_LAYER }">
    									<c:if test="${ dataListLayer1.ADOPID.equals('bank30service') }">
    										<c:forEach var="dataListLayer2" items="${ dataListLayer1.SEED_LAYER }" varStatus="idx2">
			                                    <li class="sub-nav-ul-li sub-nav-ul-li-2">
			                                        <a data-toggle="collapse" href="#ul-li-10-${idx2.index}" role="button" aria-expanded="false" aria-controls="ul-li-10-${idx2.index}">
			                                       		${dataListLayer2.ADOPNAME}
			                                        </a>
			                                        <!--第三層 -->
			                                        <div id="ul-li-10-${idx2.index}" class="collapse">
			                                            <ul class="sub-nav-ul">
    														<c:forEach var="dataListLayer3" items="${ dataListLayer2.SEED_LAYER }" varStatus="idx3">
				                                                <li class="sub-nav-ul-li sub-nav-ul-li-3">
											                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('0') }">
												                    	<a href='#' onclick="fstop.getPage('${pageContext.request.contextPath}'+'${dataListLayer3.URL}','', '')">
												                   			${dataListLayer3.ADOPNAME}
												                   		</a>
											                   		</c:if>
											                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('1') }">
												                    	<a target="_blank" href="${pageContext.request.contextPath}${dataListLayer3.URL}">
																			${dataListLayer3.ADOPNAME}
																		</a>
											                    	</c:if>
											                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('2') }">
												                    	<a target="_blank" href="${dataListLayer3.URL}${sessionScope.cusidn}">
																			${dataListLayer3.ADOPNAME}
																		</a>
											                    	</c:if>
				                                                </li>
    														</c:forEach>
			                                            </ul>
			                                        </div>
			                                    </li>
    										</c:forEach>
    									</c:if>
    								</c:forEach>
                                </ul>
                            </div>
                         </li><!--第一層 --><!-- 線上服務專區   end-->
	                         
                         <!--第一層 -->
                         <li class="sub-nav-ul-li sub-nav-ul-li-1">
							 <!-- 個人服務 -->
                             <a class="sub-nav-link" data-toggle="collapse" href="#ul-li-11" role="button" aria-expanded="false" aria-controls="ul-li-11" >
                                <img src="${__ctx}/img/menu-icon-11.svg?a=${jscssDate}" alt='<spring:message code="LB.Personal_Service"/>'>
                                <spring:message code="LB.Personal_Service"/>
                             </a>
							 <!--第二層 -->
                             <div id="ul-li-11" class="collapse">
                                <ul class="sub-nav-ul">
    								<c:forEach var="dataListLayer1" items="${ sessionScope.menu.data.sort_menu[0].SEED_LAYER }">
    									<c:if test="${ dataListLayer1.ADOPID.equals('personalservice') }">
    										<c:forEach var="dataListLayer2" items="${ dataListLayer1.SEED_LAYER }" varStatus="idx2">
			                                    <li class="sub-nav-ul-li sub-nav-ul-li-2">
			                                        <a data-toggle="collapse" href="#ul-li-11-${idx2.index}" role="button" aria-expanded="false" aria-controls="ul-li-11-${idx2.index}">
			                                       		${dataListLayer2.ADOPNAME}
			                                        </a>
			                                        <!--第三層 -->
			                                        <div id="ul-li-11-${idx2.index}" class="collapse">
			                                            <ul class="sub-nav-ul">
    														<c:forEach var="dataListLayer3" items="${ dataListLayer2.SEED_LAYER }" varStatus="idx3">
				                                                <li class="sub-nav-ul-li sub-nav-ul-li-3">
											                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('0') }">
												                    	<a href='#' onclick="fstop.getPage('${pageContext.request.contextPath}'+'${dataListLayer3.URL}','', '')">
												                   			${dataListLayer3.ADOPNAME}
												                   		</a>
											                   		</c:if>
											                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('1') }">
												                    	<a target="_blank" href="${pageContext.request.contextPath}${dataListLayer3.URL}">
																			${dataListLayer3.ADOPNAME}
																		</a>
											                    	</c:if>
											                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('2') }">
												                    	<a target="_blank" href="${dataListLayer3.URL}${sessionScope.cusidn}">
																			${dataListLayer3.ADOPNAME}
																		</a>
											                    	</c:if>
				                                                </li>
    														</c:forEach>
			                                            </ul>
			                                        </div>
			                                    </li>
    										</c:forEach>
    									</c:if>
    								</c:forEach>
								</ul>
                             </div>
                         </li><!--第一層 --><!-- 個人服務   end-->
	                         
                     </ul><!--sub-nav-ul end-->
                </div>
                
                
                
                <div class="mobile-footer">
                    <ul>
                        <!-- 臺灣企銀首頁 -->
						<li>
			            	<c:choose>
								<c:when test="${__i18n_locale eq 'en' }">
				 					<a href="https://www.tbb.com.tw/web/guest/english2" target="_blank" role="button">
				                   		<spring:message code="LB.TAIWAN_BUSINESS_BANK"/>
				                   	</a>
								</c:when>
								<c:when test="${__i18n_locale eq 'zh_TW'}">
						  			<a href="https://www.tbb.com.tw/" target="_blank" role="button">
				                    	<spring:message code="LB.TAIWAN_BUSINESS_BANK"/>
				                    </a>
								</c:when>
								<c:otherwise>
							   		<a href="https://www.tbb.com.tw/" target="_blank" role="button">
				                    	<spring:message code="LB.TAIWAN_BUSINESS_BANK"/>
				                    </a>
								</c:otherwise>
							</c:choose>
			            </li>
                        <li>
<!--                             <a href="#">網路銀行</a> -->
							<a href="https://nnb.tbb.com.tw/tbbportal/CustomerLogin.jsp" target="_blank">
								<spring:message code="LB.Internet_banking"/>
							</a>			
                        </li>
                        <li>
<!--                             <a href="#">網路ATM</a> -->
                            <a href="https://eatm.tbb.com.tw/" target="_blank">
								<spring:message code="LB.Web_ATM"/>
							</a>
                        </li>
                        <li>
<!--                             <a href="#">證券下單</a> -->

							<a href="https://www.tbbstock.com.tw/" target="_blank">
								<spring:message code="LB.Securities_order"/>
							</a>
                        </li>
                        <li>
<!--                             <a href="#">網站導覽</a> -->
                            <a href="https://nnb.tbb.com.tw/tbbportal/CustomerLogin.jsp" target="_blank">
								<spring:message code="LB.Service_overview"/>
							</a>
                        </li>
                        <li>
				           	<!-- 意見信箱 -->
				           	<c:choose>
								<c:when test="${__i18n_locale eq 'en' }">
					    			<a href="https://www.tbb.com.tw/web/guest/contact-us" target="_blank" role="button">
				                      	<spring:message code="LB.Customer_service"/>
			                      	</a>
								</c:when>
								<c:when test="${__i18n_locale eq 'zh_TW'}">
					    			<a href="https://www.tbb.com.tw/-47" target="_blank" role="button">
				                      	<spring:message code="LB.Customer_service"/>
			                      	</a>
								</c:when>
								<c:otherwise>
						    		<a href="https://www.tbb.com.tw/-47" target="_blank" role="button">
				                      	<spring:message code="LB.Customer_service"/>
			                      	</a>
								</c:otherwise>
							</c:choose>
			       		</li>
                    </ul>
                </div>

            </div>
        </div>


