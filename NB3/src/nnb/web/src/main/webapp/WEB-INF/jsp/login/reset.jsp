<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>	
</head>
<script type="text/JavaScript">
	$(document).ready(function() {
		init();
	});
	  
	function init(){
		$("#formId").validationEngine({binded:false,promptPosition: "inline" });
	
		$("#pageshow").click(function () {
			console.log("submit~~");
			if (!$('#formId').validationEngine('validate')) {
				e.preventDefault();
			} else {
				$("#formId").validationEngine('detach');

				// 送出前先把變更的密碼做加密塞入
				var OLDPIN = $('#OLDPIN').val();
				var NEWPIN = $('#NEWPIN').val();
				var OLDTRANPIN = $('#OLDTRANPIN').val();
				var NEWTRANPIN = $('#NEWTRANPIN').val();
				$('#PINOLD').val(pin_encrypt(OLDPIN)); // 舊簽入密碼加密
				$('#PINNEW').val(pin_encrypt(NEWPIN)); // 新簽入密碼加密
				$('#SSLOLD').val(pin_encrypt(OLDTRANPIN)); // 舊簽入密碼加密
				$('#SSLNEW').val(pin_encrypt(NEWTRANPIN)); // 新簽入密碼加密
				
				initBlockUI();//遮罩
				$("#formId").submit();
			}
		});
		
		$(document).keydown(function(event){
	    	if(event.keyCode == 27) {
	    		if(!$('#errorBtn2').is(":hidden") && !wait1){
	    			wait1 = true;
	    			$("#errorBtn2").click();
	    			$('#error-block').hide();
	    		}
	    		if($('#errorBtn2').is(":hidden")){
	    			$('#error-block').hide();
	    		}
	       	}
	    	
		});
	}
	
	function chkp(data){
		if(data.length >= 8){
			errorBlock(
					null, 
					null,
					['<spring:message code= "LB.X2362" />'], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
		}
	}
</script>
<body>
	<div class="content row">
		<main class="col-12">	
			<section id="main-content" class="container">
				<form method="post" id="formId" action="${__ctx}/doublepw_result">
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
							
								<div style="text-align: center">
									<h2 style="color:#ed6d00;"><spring:message code="LB.First_login_success" /></h2>
								</div>
								
								<br/><br/><br/>
								
								<!-- 舊簽入密碼 -->
								<div style="text-align: center">
									<h2><spring:message code="LB.Modify_login_pwd_note" /></h2>
					        	</div>						
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4><spring:message code="LB.Original_login_pwd_note" /></h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="hidden" id="PINOLD" name="PINOLD" />
											<input type="password" id="OLDPIN" name="OLDPIN" size="10" minlength="6" maxlength="8" value=""
												class="text-input validate[required, custom[onlyLetterNumber]]" onkeypress="chkp(this.value)"/>
										</div>
									</span>
								</div>
								
								<!-- 新簽入密碼 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4><spring:message code="LB.New_login_password" /></h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="hidden" id="PINNEW" name="PINNEW" />
											<input type="password" id="NEWPIN" name="NEWPIN" size="10" minlength="6" maxlength="8" value=""
												class="text-input validate[required, recolumn[CK,OLDPIN,NEWPIN,NEWTRANPIN], custom[onlyLetterNumber]] " onkeypress="chkp(this.value)"/>
											<p><spring:message code="LB.User_Change_password_note" /></p>
										</div>
									</span>
								</div>
								
								<!-- 確認新簽入密碼 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4><spring:message code="LB.Confirm_Login_Password" /></h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="password" id="RENEWPIN" name="RENEWPIN" size="10" minlength="6" maxlength="8" value=""
												class="text-input validate[required ,recolumn[PW,OLDPIN,NEWPIN,RENEWPIN],custom[onlyLetterNumber]]" onkeypress="chkp(this.value)"/>
											<p><spring:message code="LB.Please_Enter_New_Login_Password_again" /></p>
										</div>
									</span>
								</div>
								
								<!-- 交易密碼修改 -->
								<div style="text-align: center">
									<h2><spring:message code="LB.Modify_trans_pwd_note" /></h2>
					        	</div>
					        	
					        	<br>
					        	
					        	<!-- 舊交易密碼 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4><spring:message code="LB.Original_SSL_pwd_note" /></h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="hidden" id="SSLOLD" name="SSLOLD" />
											<input type="password" name="OLDTRANPIN" id="OLDTRANPIN" size="10" minlength="6" maxlength="8" value=""
												class="text-input validate[required, custom[onlyLetterNumber]]" onkeypress="chkp(this.value)"/>
										</div>
									</span>
								</div>
								
								<!--新交易密碼 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4><spring:message code="LB.New_SSL_Password"/></h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="hidden" id="SSLNEW" name="SSLNEW" />
											<input type="password" name="NEWTRANPIN" id="NEWTRANPIN" size="10" minlength="6" maxlength="8" value=""
												class="text-input validate[required ,recolumn[CK,OLDTRANPIN,NEWTRANPIN,NEWPIN], custom[onlyLetterNumber]]" onkeypress="chkp(this.value)"/>
											<p><spring:message code="LB.User_Change_password_note" /></p>
										</div>
									</span>
								</div>
								
								<!-- 確認新交易密碼 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4><spring:message code="LB.Confirm_SSL_password" /></h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="password" name="RENEWTRANPIN" id="RENEWTRANPIN" size="10" minlength="6" maxlength="8" value=""
												class="text-input validate[required ,recolumn[PW,OLDTRANPIN,NEWTRANPIN,RENEWTRANPIN], custom[onlyLetterNumber]]" onkeypress="chkp(this.value)"/>
											<p><spring:message code="LB.Please_enter_new_SSL_password_again" /></p>
										</div>
									</span>
								</div>							
							</div>
							
							<input id="reset" name="reset" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" />	
							<input id="pageshow" name="pageshow" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />						
						</div>
					</div>
				</form>
						
				<div class="text-left">
					<spring:message code="LB.Description_of_page" /> ：
					<ol class="list-decimal text-left">
						<li><spring:message code="LB.Pw_alter_step_P2_D1" /></li>
						<li><spring:message code="LB.DoublePW_P1_D3" /></li>
					</ol>
				</div>
				
			</section>
		</main>
	</div>

</body>
</html>
