<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
   <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">    
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
</head>
 <body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 已兌現票據明細     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Cashed_Note_Details" /></li>
		</ol>
	</nav>



	
	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
	<main class="col-12"> 
	<!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<h2><spring:message code="LB.Cashed_Note_Details" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 						下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
<!-- 						下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
<!-- 						下載為舊txt檔 -->
						<option value="oldtxt"><spring:message code="LB.Download_oldversion_txt_file" /></option>
					</select>
				</div>						
			<form method="post" id="formId">
				<input type="hidden" name="All_Acn" value='${checking_cashed.data.JSON}'>
				<!-- 下載用 -->
				<input type="hidden" name="downloadFileName" value="LB.Cashed_Note_Details"/>
				<input type="hidden" name="downloadType" id="downloadType"/> 					
				<input type="hidden" name="templatePath" id="templatePath"/> 	
				<input type="hidden" name="hasMultiRowData" value="true"/>
				<!-- EXCEL下載用 -->
				<input type="hidden" name="headerRightEnd" value="1" />
				<input type="hidden" name="headerBottomEnd" value="4" />
				<input type="hidden" name="multiRowStartIndex" value="8" />
                <input type="hidden" name="multiRowEndIndex" value="8" />
                <input type="hidden" name="multiRowCopyStartIndex" value="5" />
                <input type="hidden" name="multiRowCopyEndIndex" value="10" />
                <input type="hidden" name="multiRowDataListMapKey" value="rowListMap" />
                <input type="hidden" name="rowRightEnd" value="8" />
				<!-- TXT下載用 -->
				<input type="hidden" name="txtHeaderBottomEnd" value="4"/>
				<input type="hidden" name="txtMultiRowStartIndex" value="11"/>
				<input type="hidden" name="txtMultiRowEndIndex" value="11"/>
				<input type="hidden" name="txtMultiRowCopyStartIndex" value="6"/>
				<input type="hidden" name="txtMultiRowCopyEndIndex" value="12"/>
				<input type="hidden" name="txtMultiRowDataListMapKey" value="rowListMap"/>
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">							
							<!-- 帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
								<h4><spring:message code="LB.Account" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<select name="ACN" id="ACN" class="custom-select select-input half-input">
											<option value=""><spring:message code="LB.Select_account" /></option>
											<c:forEach var="dataList" items="${checking_cashed.data.REC }">
												<option value="${dataList.ACN}">${dataList.ACN}</option>
											</c:forEach>
											<option value="AllAcn"><spring:message code="LB.All" /></option>
										</select>
									</div>
								</span>
							</div>				
							 
							<div class="ttb-input-item row">
								<!--查詢區間  -->
								<span class="input-title">
									<label>
										<spring:message code="LB.Inquiry_period" />
									</label>
								</span>
								<span class="input-block">
									<!--  當日交易 -->
									<div class="ttb-input">
										<label class="radio-block" for="CMTODAY">
											<input type="radio" name="FGPERIOD" id="CMTODAY" value="CMTODAY" checked/>
											<spring:message code="LB.Todays_transaction" />
											<span class="ttb-radio"></span>
										</label>
									</div>
									<!--  當月交易 -->
									<div class="ttb-input">
										<label class="radio-block" for="CMCURMON">
											<input type="radio" name="FGPERIOD" id="CMCURMON" value="CMCURMON" />
											<spring:message code="LB.This_months_transaction" />
											<span class="ttb-radio"></span>
										</label>
									</div>
									<!-- 上月交易 -->
									<div class="ttb-input">
										<label class="radio-block" for="CMLASTMON">
											<input type="radio" name="FGPERIOD" id="CMLASTMON" value="CMLASTMON" />
											<spring:message code="LB.Last_months_trading" />
											<span class="ttb-radio"></span>
										</label>
									</div>
									<!--  指定日期區塊 -->
									<div class="ttb-input">
										<label class="radio-block" for="CMPERIOD">
											<!--  指定日期 -->
											<spring:message code="LB.Specified_period" />
											<input type="radio" name="FGPERIOD" id="CMPERIOD" value="CMPERIOD" /> 
											<span class="ttb-radio"></span>
										</label>
										<!-- 起迄日區塊預設隱藏 -->
										<div id = "hideblock_FGPERIOD" style="display: none;">
										<br>
											<!--期間起日 -->
											<div class="ttb-input">
												<span class="input-subtitle subtitle-color">
													<spring:message code="LB.Period_start_date" />
												</span>
												<input type="text" id="CMSDATE" name="CMSDATE" class="text-input datetimepicker" value="${checking_cashed.data.TODAY}" />
												<span class="input-unit CMSDATE">
													<img src="${__ctx}/img/icon-7.svg" />
												</span>
											</div>
											<!--期間迄日 -->
											<div class="ttb-input">
												<span class="input-subtitle subtitle-color">
													<spring:message code="LB.Period_end_date" />
												</span>
												<input type="text" id="CMEDATE" name="CMEDATE" class="text-input datetimepicker" value="${checking_cashed.data.TODAY}" />
												<span class="input-unit CMEDATE">
													<img src="${__ctx}/img/icon-7.svg" />
												</span>
											</div>
												<!-- 驗證用的span預設隱藏 -->
												<span id="hideblock_CheckDateScope" >
												<!-- 驗證用的input -->
												<input id="odate" name="odate" type="text" value="${checking_cashed.data.TODAY}" class="text-input validate[required,funcCall[validate_CheckDateScope['<spring:message code= "LB.Inquiry_period_1" />', odate, CMSDATE, CMEDATE, false, 24, null]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												</span>
										</div>
										<!-- 起迄日區塊 -END-->
									</div>
								</span>
							</div>
						</div>
						<input id="reset" name="reset" type="reset" class="ttb-button btn-flat-gray" onclick="reFGPERIOD()" value="<spring:message code="LB.Re_enter" />" />	
						<input type="button" class="ttb-button btn-flat-orange" id="pageshow" value="<spring:message code="LB.Display_as_web_page" />"/>
					</div>
				</div>
					<div class="text-left">
						<ol class="description-list list-decimal">
						<p>
							<spring:message code="LB.Description_of_page" />
						</p>
							<li>
								<spring:message code= "LB.Checking_cashed_P1_D1-1" />
								<a target="_blank" href='${pageContext.request.contextPath}/public/N625_TXT.htm'>
									<spring:message code= "LB.Checking_cashed_P1_D1-2" />
								</a> 
								<spring:message code= "LB.Checking_cashed_P1_D1-3" />
							</li>
							<li>
								<spring:message code= "LB.Checking_cashed_P1_D2-1" />
								<a target="_blank" href='${pageContext.request.contextPath}/public/N625_OLDTXT.htm'>
									<spring:message code= "LB.Checking_cashed_P1_D2-2" />
								</a>
								
							</li>
						</ol>
					</div>								
				</form>
			</section>
		<!-- 		main-content END -->
		</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
<!--   Js function -->
    <script type="text/JavaScript">
		$(document).ready(function() {
			init();
			//日期
			datetimepickerEvent();
			//預約自動輸入今天
// 			getTmr();
			//日期區間切換按鈕,如果點選指定日期則顯示起迄日
			fgperiodChageEvent();
			// 初始化時隱藏span
			$("#hideblock_CheckDateScope").hide();
		});
		
		function init(){
			$("#formId").validationEngine({
				binded: false,
				promptPosition: "inline"
			});
			
			//acn
			var getacn = '${checking_cashed.data.Acn}';
			if(getacn != null && getacn != ''){
				$("#ACN option[value= '"+ getacn +"' ]").prop("selected", true);
			}
			
			$("#pageshow").click(function(e){
				//打開驗證隱藏欄位
				$("#hideblock_CheckDateScope").show();
				//塞值進span內的input
// 				$("#Monthly_DateA").val($("#CMSDATE").val());
// 				$("#Monthly_DateB").val($("#CMEDATE").val());
				
				e = e || window.event;
// 				if( $('#CMPERIOD').prop('checked') )
// 				{
// 					if(checkTimeRange() == false )
// 					{
// 						return false;
// 					}
// 				}
				
				if(!$('#formId').validationEngine('validate')){
		        	e.preventDefault();
	 			}
// 				else if ($('#ACN').val() == ""){
// 	 				e.preventDefault();
// 	 			}
				else{
	 				$("#formId").validationEngine('detach');
	 				initBlockUI();
	 				$("#formId").attr("action","${__ctx}/NT/ACCT/checking_cashed_result");
	 	  			$("#formId").submit(); 
	 			}		
	  		});
			
			//日期驗證機制切換
// 			changeFgtxway();
		}
		
// 		function getTmr() {
// 			var today = new Date();
// 			today.setDate(today.getDate());
// 			var y = today.getFullYear();
// 			var m = today.getMonth() + 1;
// 			var d = today.getDate();
// 			if(m<10){
// 				m = "0"+m
// 			}
// 			if(d<10){
// 				d="0"+d
// 			}
// 			var tmr = y + "/" + m + "/" + d
// 			$('#CMSDATE').val(tmr);
// 			$('#CMEDATE').val(tmr);
// 			$('#odate').val(${checking_cashed.data.TODAY});
// 		}
		
		//日曆欄位參數設定
		function datetimepickerEvent(){

		    $(".CMSDATE").click(function(event) {
				$('#CMSDATE').datetimepicker('show');
			});
		    $(".CMEDATE").click(function(event) {
				$('#CMEDATE').datetimepicker('show');
			});
			
			jQuery('.datetimepicker').datetimepicker({
				timepicker:false,
				closeOnDateSelect : true,
				scrollMonth : false,
				scrollInput : false,
			 	format:'Y/m/d',
			 	lang: '${transfer}'
			});
		}
		
		//日期區間切換按鈕,如果點選指定日期則顯示起迄日
		function fgperiodChageEvent() {
			$('input[type=radio][name=FGPERIOD]').change(function () {
				if (this.value == 'CMPERIOD') {
					$("#hideblock_FGPERIOD").show();
				} else {
					$("#hideblock_FGPERIOD").hide();
					//預約自動輸入今天
					getTmr();
				}
			});
		}
		
// 		function checkTimeRange()
// 		{
// 			var now = Date.now();
// 			var twoYm = 63115200000;
// 			var twoMm = 5259600000;
			
// 			var startT = new Date( $('#CMSDATE').val() );
// 			var endT = new Date( $('#CMEDATE').val() );
// 			var distance = now - startT;
// 			var range = endT - startT;
			
// 			var limitS = new Date(now - twoYm);
// 			var limitE = new Date(startT.getTime() + twoMm); 
// 			if(distance > twoYm)
// 			{
// 				var m = limitS.getMonth() + 1;
// 				var time = limitS.getFullYear() + '/' + m + '/' + limitS.getDate();
// 				// 起始日不能小於
// 				var msg = '<spring:message code="LB.Start_date_check_note_1" />' + time;
// 				alert(msg);
// 				return false;
// 			}
// 			else
// 			{
// 				if(range > twoMm)
// 				{
// 					var m = limitE.getMonth() + 1;
// 					var time = limitE.getFullYear() + '/' + m + '/' + limitE.getDate();
// 					// 終止日不能大於
// 					var msg = '<spring:message code="LB.End_date_check_note_1" />' + time;
// 					alert(msg);
// 					return false;
// 				}
// 			}
// 			return true;
// 		}
		
		//選項
		 	function formReset() {
		 		//打開驗證隱藏欄位
				$("#hideblock_CheckDateScope").show();
				//塞值進span內的input
// 				$("#Monthly_DateA").val($("#CMSDATE").val());
// 				$("#Monthly_DateB").val($("#CMEDATE").val());
				
// 				if($('#CMPERIOD').prop('checked')){
// 					if(checkTimeRange() == false){
// 						return false;
// 					}
// 				}
				if(!$("#formId").validationEngine("validate")){
					e = e || window.event;//forIE
					e.preventDefault();
				}
		 		else{
// 			 		initBlockUI();
					if ($('#actionBar').val()=="excel"){
						$("#downloadType").val("OLDEXCEL");
						$("#templatePath").val("/downloadTemplate/checking_cashed.xls");
			 		}else if ($('#actionBar').val()=="txt"){
						$("#downloadType").val("TXT");
						$("#templatePath").val("/downloadTemplate/checking_cashed.txt");
			 		}else if ($('#actionBar').val()=="oldtxt"){
						$("#downloadType").val("OLDTXT");
						$("#templatePath").val("/downloadTemplate/checking_cashedOLD.txt");
			 		}
// 					ajaxDownload("${__ctx}/NT/ACCT/checking_cashed_ajaxDirectDownload","formId","finishAjaxDownload()");
					$("#formId").attr("target", "");
	                $("#formId").attr("action", "${__ctx}/NT/ACCT/checking_cashed_directDownload");
		            $("#formId").submit();
		            $('#actionBar').val("");
		 		}
			}
		
		//交易類別change 事件
// 		function changeFgtxway() {
// 			$('input[type=radio][name=FGPERIOD]').change(function () {
// 				console.log(this.value);
// 				if (this.value == 'CMPERIOD') {
// 					$("#Monthly_DateA").addClass("validate[required, verification_date[Monthly_DateA]]")
// 					$("#Monthly_DateB").addClass("validate[required, verification_date[Monthly_DateB]]")
// 				} else if (this.value != 'CMPERIOD') {
// 					$("#Monthly_DateA").removeClass("validate[required, verification_date[Monthly_DateA]]");
// 					$("#Monthly_DateB").removeClass("validate[required, verification_date[Monthly_DateB]]");
// 				}
// 			});
// 		}
		
		//交易類別change 事件
// 		function reFGPERIOD() {
// 				console.log(this.value);
// 				$("#Monthly_DateA").removeClass("validate[required, verification_date[Monthly_DateA]]");
// 				$("#Monthly_DateB").removeClass("validate[required, verification_date[Monthly_DateB]]");				
// 		}

 	</script>
</body>
</html>
