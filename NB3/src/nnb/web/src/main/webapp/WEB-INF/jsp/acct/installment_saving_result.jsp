<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
  <%@ include file="../__import_head_tag.jsp"%>
  <%@ include file="../__import_js_u2.jsp" %>
  <script type="text/javascript">
    $(document).ready(function () {
      //列印
      $("#printbtn").click(function () {
        var params = {
          "jspTemplateName": "installment_saving_result_print",
          "jspTitle": '<spring:message code="LB.Periodical_Deposits_And_Lumpsum_Payment_monthly"/>',
          "MsgName": '${installment_saving_result.data.MsgName}',
          "CMTXTIME": '${installment_saving_result.data.CMTXTIME}',
          "OUTACN": '${installment_saving_result.data.OUTACN}',
          "FDPACN": '${installment_saving_result.data.FDPACN}',
          "FDPNUM": '${installment_saving_result.data.FDPNUM}',
          "AMOUNT": '${installment_saving_result.data.AMOUNT}',
          "TERMS": '${installment_saving_result.data.TERMS}',
          "TOTAMT": '${installment_saving_result.data.TOTAMT}',
          "RETERMS": '${installment_saving_result.data.RETERMS}',
          "TOTBAL": '${installment_saving_result.data.TOTBAL}',
          "AVLBAL": '${installment_saving_result.data.AVLBAL}',
        };

        openWindowWithPost("${__ctx}/print",
          "height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",
          params);
      });
    });
  </script>
</head>

<body>

  <!-- header     -->
  <header>
    <%@ include file="../index/header.jsp"%>
  </header>

	<!--   IDGATE -->
	<%@ include file="../idgate_tran/idgateAlert.jsp" %> 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 定存服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Current_Deposit_To_Time_Deposit" /></li>
    <!-- 零存整付按月繳存     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Periodical_Deposits_And_Lumpsum_Payment_monthly" /></li>
		</ol>
	</nav>



  <!-- menu、登出窗格 -->
  <div class="content row">
    <!-- 功能選單內容 -->
    <%@ include file="../index/menu.jsp"%>
  </div>
  <!-- content row END -->
  <!-- 		主頁內容  -->
  <main class="col-12">
    <section id="main-content" class="container">
      <h2>
        <!--  臺幣零存整付按月繳存 -->
        <spring:message code="LB.Periodical_Deposits_And_Lumpsum_Payment_monthly" />
      </h2>
      <i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
      <form id="formId" method="post" action="">
        <c:set var="BaseResultData" value="${installment_saving_result.data}"></c:set>
        <!-- 交易步驟 -->
        <div id="step-bar">
          <ul>
            <!-- 輸入資料 -->
            <li class="finished">
              <spring:message code="LB.Enter_data" />
            </li>
            <!-- 確認資料 -->
            <li class="finished">
              <spring:message code="LB.Confirm_data" />
            </li>
            <!-- 交易完成 -->
            <li class="active">
              <spring:message code="LB.Transaction_complete" />
            </li>
          </ul>
        </div>

        <!-- 表單顯示區 -->
        <div class="main-content-block row">
          <div class="col-12 tab-content">
            <div class="ttb-input-block">
              <div class="ttb-message">
                <span>
                <!-- 轉帳成功 -->
                  <spring:message code= "LB.Transfer_successful" />
                </span>
              </div>
              <!-- 交易時間 -->
              <div class="ttb-input-item row">
                <span class="input-title">
                  <label>
                    <h4>
                      <spring:message code="LB.Trading_time" />
                    </h4>
                  </label>
                </span>
                <span class="input-block">
                  <div class="ttb-input">
                    <span>${BaseResultData.CMTXTIME}</span>
                  </div>
                </span>
              </div>
              <!--轉出帳號-->
              <div class="ttb-input-item row">
                <span class="input-title">
                  <label>
                    <h4>
                      <spring:message code="LB.Payers_account_no" />
                    </h4>
                  </label>
                </span>
                <span class="input-block">
                  <div class="ttb-input">
                    <span>${BaseResultData.OUTACN}</span>
                  </div>
                </span>
              </div>
              <!--存單帳號 -->
              <div class="ttb-input-item row">
                <span class="input-title">
                  <label>
                    <h4>
                      <spring:message code="LB.Account_no" />
                    </h4>
                  </label>
                </span>
                <span class="input-block">
                  <div class="ttb-input">
                    <span>${BaseResultData.FDPACN}</span>
                  </div>
                </span>
              </div>
              <!--存單號碼 -->
              <div class="ttb-input-item row">
                <span class="input-title">
                  <label>
                    <h4>
                      <spring:message code="LB.Certificate_no" />
                    </h4>
                  </label>
                </span>
                <span class="input-block">
                  <div class="ttb-input">
                    <span>${BaseResultData.FDPNUM}</span>
                  </div>
                </span>
              </div>
              <!--轉帳金額-->
              <div class="ttb-input-item row">
                <span class="input-title">
                  <label>
                    <h4>
                      <spring:message code="LB.Amount" />
                    </h4>
                  </label>
                </span>
                <span class="input-block">
                  <div class="ttb-input">
                    <span>
                      <!-- 新台幣 -->
                      <spring:message code="LB.NTD" />
                      &nbsp;
                      ${BaseResultData.AMOUNT }
                      &nbsp;
                      <!--                   元 -->
                      <spring:message code="LB.Dollar" />
                    </span>
                  </div>
                </span>
              </div>
              <!--已存入總期數 -->
              <div class="ttb-input-item row">
                <span class="input-title">
                  <label>
                    <h4>
                      <spring:message code="LB.Total_number_of_periods_deposited" />
                    </h4>
                  </label>
                </span>
                <span class="input-block">
                  <div class="ttb-input">
                    <span>
                      ${BaseResultData.TERMS}
                    </span>
                  </div>
                </span>
              </div>
              <!--已存入總金額 -->
              <div class="ttb-input-item row">
                <span class="input-title">
                  <label>
                    <h4>
                      <spring:message code="LB.Total_amount_deposited" />
                    </h4>
                  </label>
                </span>
                <span class="input-block">
                  <div class="ttb-input">
                    <span>
                      <!-- 新台幣 -->
                      <spring:message code="LB.NTD" />
                      &nbsp;
                      ${BaseResultData.TOTAMT}
                      &nbsp;
                      <!--元 -->
                      <spring:message code="LB.Dollar" />
                    </span>
                  </div>
                </span>
              </div>
              <!--欠繳期數 -->
              <div class="ttb-input-item row">
                <span class="input-title">
                  <label>
                    <h4>
                      <spring:message code="LB.Number_of_outstanding_periods" />
                    </h4>
                  </label>
                </span>
                <span class="input-block">
                  <div class="ttb-input">
                    <span>
                      ${BaseResultData.RETERMS}
                    </span>
                  </div>
                </span>
              </div>
              <!--轉出帳號帳戶餘額 -->
              <div class="ttb-input-item row">
                <span class="input-title">
                  <label>
                    <h4>
                      <spring:message code="LB.Payers_account_balance" />
                    </h4>
                  </label>
                </span>
                <span class="input-block">
                  <div class="ttb-input">
                    <span>
                      <!-- 新台幣 -->
                      &nbsp;
                      <spring:message code="LB.NTD" />
                      ${BaseResultData.TOTBAL}
                      &nbsp;
                      <!--元 -->
                      <spring:message code="LB.Dollar" />
                    </span>
                  </div>
                </span>
              </div>
              <!--轉出帳號可用餘額 -->
              <div class="ttb-input-item row">
                <span class="input-title">
                  <label>
                    <h4>
                      <spring:message code="LB.Payers_available_balance" />
                    </h4>
                  </label>
                </span>
                <span class="input-block">
                  <div class="ttb-input">
                    <span>
                      <!-- 新台幣 -->
                      <spring:message code="LB.NTD" />
                      &nbsp;
                      ${BaseResultData.AVLBAL}
                      &nbsp;
                      <!--元 -->
                      <spring:message code="LB.Dollar" />
                    </span>
                  </div>
                </span>
              </div>
            </div>
            <!--button 區域 -->
              <!-- 列印  -->
              <spring:message code="LB.Print" var="printbtn"></spring:message>
              <input type="button" id="printbtn" value="${printbtn}" class="ttb-button btn-flat-orange" />
            <!--button 區域 -->
          </div>
        </div>
        <div class="text-left">
          <!--               	說明： -->
          <!--               <ol class="list-decimal text-left"> -->
          <!--                 <li>晶片金融卡:為保護您的交易安全，結束交易或離開電腦時，請務必將晶片金融卡抽離讀卡機並登出系統。</li> -->
          <!--                 <li>電子簽章:為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章(載具i-key)拔除並登出系統。</li> -->
          <!--               </ol> -->
        </div>
      </form>
    </section>
    <!-- 		main-content END -->
  </main>
  <%@ include file="../index/footer.jsp"%>
</body>

</html>