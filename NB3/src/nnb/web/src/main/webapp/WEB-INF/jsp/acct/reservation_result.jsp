<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
	<!-- 預約交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X1477" /></li>
    <!-- 預約交易查詢/取消     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Query_Cancel_Scheduled_Transactions" /></li>
		</ol>
	</nav>



		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 --> 
 		<div class="content row">
 		   <%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
 		
 		<main class="col-12">	

		<!-- 		主頁內容  -->

			<section id="main-content" class="container">
		
				<h2><spring:message code="LB.Query_Cancel_Scheduled_Transactions" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
<!-- 				<div id="step-bar"> -->
<!-- 					<ul> -->
<!-- 						<li class="finished">輸入資料</li> -->
<!-- 						<li class="finished">確認資料</li> -->
<!-- 						<li class="active">交易完成</li> -->
<!-- 					</ul> -->
<!-- 				</div> -->
				<div class="main-content-block row">
					<div class="col-12 tab-content"> 
						<div class="ttb-input-block">
							<div class="ttb-message">
								<span><spring:message code="LB.Cancel_booking_successful" /></span>
							</div>
						<c:set var="dataSet" value="${ reservation_result.data.dataSet }" />
<!-- 								資料時間 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Data_time" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>${reservation_result.data.CMQTIME }</span>		
									</div>
								</span>
							</div>
<!--								  預約編號     -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Booking_number" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>${dataSet.DPSCHNO}</span>
									</div>
								</span>
							</div>
<!-- 					           	 週期    -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Period" /></h4>	
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>${dataSet.DPPERMTDATE}</span>
									</div>
								</span>
							</div>
<!-- 					                生效日截止日	-->								
								<div class="ttb-input-item row">
								<span class="input-title">
									<label>
									<h4>
										<spring:message code="LB.Effective_date" />
										-
										<spring:message code="LB.Deadline" />
									</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
									<span>
										${dataSet.DPFDATE}
										<c:if test="${not empty dataList.DPTDATE}">
							            -
							            ${dataList.DPTDATE}
							            </c:if>
							        </span>
									</div>
								</span>
								</div>
<!-- 					            下次轉帳日     -->
								<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Next_transfer_date" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>${dataSet.DPNEXTDATE}</span>
									</div>
								</span>
								</div>
<!-- 					            轉出帳號     -->
								<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Payers_account_no" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>${dataSet.DPWDAC}</span>
									</div>
								</span>
								</div>
<!-- 					            轉入帳號/繳費稅代號    -->
								<div class="ttb-input-item row">
								<span class="input-title">
									<label>
									<h4>
										<spring:message code="LB.Payees_account_no" />
										/
										<spring:message code="LB.Pay_taxes_fee_code" />
									</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
									<span>
										${dataSet.DPSVBH}
										${dataSet.DPSVAC}
									</span>
									</div>
								</span>
								</div>
<!-- 					            轉帳金額     -->								
								<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Amount" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>${dataSet.DPTXAMT}</span>
									</div>
								</span>
									</div>
<!-- 					            交易類別                    -->
								<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Transaction_type" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>${dataSet.TXTYPE}</span>
									</div>
								</span>
									</div>
<!-- 					            備註                    -->
								<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Note" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>${dataSet.DPTXMEMO}</span>
									</div>
								</span>
								</div>
						</div>
					<input id="previous" name="previous" type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_function_home_page" />" />
					<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
					</div>
				</div>
				</form>
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>
					<li><span><spring:message code="LB.Reservation_P2_D1" /></span></li>
					<li><span><spring:message code="LB.Reservation_P2_D2" /></span></li>
				</ol>
<!-- 					<div class="text-left"> -->
<!-- 						<spring:message code="LB.Description_of_page" /> : -->
<!-- 						<ol class="list-decimal text-left"> -->
<!-- 							<li>晶片金融卡:為保護您的交易安全，結束交易或離開電腦時，請務必將晶片金融卡抽離讀卡機並登出系統。</li> -->
<!-- 							<li>電子簽章:為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章(載具i-key)拔除並登出系統。</li> -->
<!-- 						</ol> -->
<!-- 					</div> -->
			</section>
			<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
	
	<script type="text/javascript">
			$(document).ready(function(){
				// HTML載入完成後開始遮罩
				setTimeout("initBlockUI()", 50);
				// 開始跑下拉選單並完成畫面
				setTimeout("init()", 400);
				// 解遮罩
				setTimeout("unBlockUI(initBlockId)", 500);
			});
			
			function init(){
				$("#printbtn").click(function(){
					var params = {
						"jspTemplateName":"reservation_result_print",
						"jspTitle":"<spring:message code='LB.Query_Cancel_Scheduled_Transactions' />",
						"MSG":"<spring:message code='LB.Cancel_booking_successful' />",
						"CMQTIME":"${reservation_result.data.CMQTIME}",
						"DPSCHNO":"${dataSet.DPSCHNO}",
						"DPPERMTDATE":"${dataSet.DPPERMTDATE}",
						"DPFDATE":"${dataSet.DPFDATE}",
						"DPTDATE":"${dataSet.DPTDATE}",
						"DPNEXTDATE":"${dataSet.DPNEXTDATE}",
						"DPWDAC":"${dataSet.DPWDAC}",
						"DPSVBH":"${dataSet.DPSVBH}",
						"DPSVAC":"${dataSet.DPSVAC}",
						"DPTXAMT":"${dataSet.DPTXAMT}",
						"TXTYPE":"${dataSet.TXTYPE}",
						"DPTXMEMO":"${dataSet.DPTXMEMO}"
					};
					openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
				});
			
				//上一頁按鈕
				$("#previous").click(function() {
					initBlockUI();
					fstop.getPage('${pageContext.request.contextPath}'+'/NT/ACCT/RESERVATION/reservation_detail','', '');
				});

			}
	</script>
</body>
</html>