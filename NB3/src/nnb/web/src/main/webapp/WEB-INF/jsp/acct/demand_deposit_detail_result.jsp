<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
    <%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
    <script type="text/javascript">
        $(document).ready(function () {
            initDataTable();// 將.table變更為DataTable 
            init();
            jQuery(function($) {
        		$('.dtablet').DataTable({
        			scrollX: true,
        			sScrollX: "99%",
        			scrollY: true,
        			bPaginate: false,
        			bFilter: false,
        			bDestroy: true,
        			bSort: false,
        			info: false,
        		});
        	});
        });

        function init() {
//             //Excel
//             $("#excelbtn").click(function () {
//                 $("#downloadType").val("OLDEXCEL");
//                 $("#templatePath").val("/downloadTemplate/acct_demand_deposit_detail.xls");
//                 $("#formId").submit();
//             });
//             //txt
//             $("#txtbtn").click(function () {
//                 $("#downloadType").val("TXT");
//                 $("#templatePath").val("/downloadTemplate/acct_demand_deposit_detail.txt");
//                 $("#formId").submit();
//             });
//             //oldtxt
//             $("#oldTxtbtn").click(function () {
//                 $("#downloadType").val("OLDTXT");
//                 $("#templatePath").val("/downloadTemplate/acct_demand_deposit_detailOLD.txt");
//                 $("#formId").submit();
//             });
            //列印
        	$("#printbtn").click(function(){
				var params = {
						"jspTemplateName":"demand_deposit_detail_result_print",
						"jspTitle":'<spring:message code="LB.NTD_Demand_Deposit_Detail"/>',
						"CMQTIME":"${demand_deposit_detail_result.data.CMQTIME}",
						"CMPERIOD":"${demand_deposit_detail_result.data.CMPERIOD}",
						"COUNT":"${demand_deposit_detail_result.data.COUNT}"
					};
					openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
    		//上一頁按鈕
    		$("#CMBACK").click(function() {
    			var action = '${__ctx}/NT/ACCT/demand_deposit_detail';
    			$('#back').val("Y");
    			$("form").attr("action", action);
    			initBlockUI();
    			$("form").submit();
    		});
        }
        
    	//選項
	 	function formReset() {
    		if ($('#actionBar').val()=="excel"){
				$("#downloadType").val("OLDEXCEL");
				$("#templatePath").val("/downloadTemplate/acct_demand_deposit_detail.xls");
	 		}else if ($('#actionBar').val()=="txt"){
				$("#downloadType").val("TXT");
			    $("#templatePath").val("/downloadTemplate/acct_demand_deposit_detail.txt");
	 		}else if ($('#actionBar').val()=="oldtxt"){
				$("#downloadType").val("OLDTXT");
				$("#templatePath").val("/downloadTemplate/acct_demand_deposit_detailOLD.txt");
	 		}else if($('#actionBar').val()=="pdf"){
	 			$("#downloadType").val("PDF");
	 			$("#templatePath").val("");
	 			
	 			$("#formId").attr("target", "");
				$("#formId").attr("action", "${__ctx}/NT/ACCT/downloadDemandDepositDetailPDF");
				$("#formId").submit();
				return;
	 		}
    		
			// 下載EXCEL,TXT submit
    		$("#formId").attr("target", "");
			$("#formId").attr("action", "${__ctx}/download");
			$("#formId").submit();
			$('#actionBar').val("");
		}
    	console.log("showDialog = "+"${showDialog}");
    </script>
</head>

<body>
    <!-- header     -->
    <header>
        <%@ include file="../index/header.jsp"%>
    </header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
	<!-- 帳戶查詢 -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Account_Inquiry" /></li>
    <!-- 帳戶明細查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.NTD_Demand_Deposit_Detail" /></li>
		</ol>
	</nav>



    <!-- menu、登出窗格 -->
    <div class="content row">
        <!-- 功能選單內容 -->
        <%@ include file="../index/menu.jsp"%>
        <!-- 	快速選單及主頁內容 -->
        <main class="col-12">
            <!-- 		主頁內容  -->
            <section id="main-content" class="container">
                <h2>
                    <!--臺幣活期性存款明細 -->
                    <spring:message code="LB.NTD_Demand_Deposit_Detail" />
                </h2>
                <i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                							<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 						下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
<!-- 						下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
<!-- 						下載為舊txt檔 -->
						<option value="oldtxt"><spring:message code="LB.Download_oldversion_txt_file" /></option>
						<!-- 下載PDF -->
						<option value="pdf"><spring:message code="LB.X2037" /> PDF</option>
					</select>
				</div>	
                <form id="formId" action="" method="post">
                <input type="hidden" id="back" name="back" value="">
                    <!-- 下載用 -->
                    <input type="hidden" name="downloadFileName" value="LB.NTD_Demand_Deposit_Detail" />
                    <input type="hidden" name="CMQTIME" value="${demand_deposit_detail_result.data.CMQTIME}" />
                    <input type="hidden" name="CMPERIOD" value="${demand_deposit_detail_result.data.CMPERIOD}" />
                    <input type="hidden" name="COUNT" value="${demand_deposit_detail_result.data.COUNT}" />
                    <input type="hidden" name="SUMALLIN" value="${demand_deposit_detail_result.data.SUMALLIN}" />
                    <input type="hidden" name="SUMALLOUT" value="${demand_deposit_detail_result.data.SUMALLOUT}" />
                    <input type="hidden" name="downloadType" id="downloadType" />
                    <input type="hidden" name="templatePath" id="templatePath" />
                   <input type="hidden" name="hasMultiRowData" value="true"/>
                    <!-- EXCEL下載用 -->
                    <input type="hidden" name="headerRightEnd" value="1" />
                    <input type="hidden" name="headerBottomEnd" value="6" />
                    <input type="hidden" name="multiRowStartIndex" value="10" />
                    <input type="hidden" name="multiRowEndIndex" value="10" />
                    <input type="hidden" name="multiRowCopyStartIndex" value="8" />
                    <input type="hidden" name="multiRowCopyEndIndex" value="12" />
                    <input type="hidden" name="multiRowDataListMapKey" value="rowListMap" />  
                    <input type="hidden" name="rowRightEnd" value="8" />
                    <!-- TXT下載用 -->
                    <input type="hidden" name="txtHeaderBottomEnd" value="6"/>
					<input type="hidden" name="txtMultiRowStartIndex" value="10"/>
					<input type="hidden" name="txtMultiRowEndIndex" value="10"/>
					<input type="hidden" name="txtMultiRowCopyStartIndex" value="7"/>
					<input type="hidden" name="txtMultiRowCopyEndIndex" value="11"/>
					<input type="hidden" name="txtMultiRowDataListMapKey" value="rowListMap"/>
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
                            <ul class="ttb-result-list">
                                     <!-- 查詢時間 -->
		                            <li>
		                                <h3> 
		                                	<spring:message code="LB.Inquiry_time" /> :
		                                </h3>
		                                <p>
		                                	${demand_deposit_detail_result.data.CMQTIME }
		                                </p>
		                            </li>
                               
                                    <!-- 查詢期間 -->
                                    <li>
                                    	<h3>
                                    	 	<spring:message code="LB.Inquiry_period" /> :
                                    	</h3>
                                    	<p>
                                    		${demand_deposit_detail_result.data.CMPERIOD }
                                    	</p>
                                    </li>
                                    <!-- 資料總數 : -->
                                    <li>
                                    	<h3>
                                    		<spring:message code="LB.Total_records" /> :
                                    	</h3>
                                    	<p>
	                                    	  ${demand_deposit_detail_result.data.COUNT}&nbsp;<spring:message code="LB.Rows" />
	                                        <!--筆 -->                         
                                    	</p>
                                    
                                    </li>
                            </ul>
                            <!-- 表格區塊 -->
                            <c:forEach var="labelList" items="${ demand_deposit_detail_result.data.labelList }">
								<ul class="ttb-result-list">
                                    <!-- 帳號 : -->
                                    <li>
                                    	<h3>
                                    	   <spring:message code="LB.Account" />
                                    	</h3>
                                    	<p>
                                    		${labelList.ACN }
                                    	</p>
                                    </li>
                           	 	</ul> 
                           	 	<table class="stripe table-striped ttb-table dtablet" data-toggle-column="first">
                                    <thead>
										<!--帳號 -->
<%--                                         <tr> --%>
<%--                                             <th> --%>
<%--                                                 <spring:message code="LB.Account" /> --%>
<%--                                             </th> --%>
<%--                                             <th colspan="8">${labelList.ACN }</th> --%>
<%--                                         </tr> --%>
                                        <tr>
                                            <!--異動日-->
                                            <th>
                                                <spring:message code="LB.Change_date" />
                                            </th>
                                            <!-- 摘要 -->
                                             <th data-breakpoints="xs sm">
                                                <spring:message code="LB.Summary_1" />
                                            </th>
                                            <!--支出金額-->
                                            <th>
                                                <spring:message code="LB.Withdrawal_amount" />
                                            </th>
                                            <!--存入金額 -->
                                            <th>
                                                <spring:message code="LB.Deposit_amount" />
                                            </th>
                                            <!-- 餘額 -->
                                            <th>
                                                <spring:message code="LB.Account_balance_2" />
                                            </th>
                                            <!-- 票據號碼 -->
                                            <th>
                                                <spring:message code="LB.Checking_account" />
                                            </th>
                                            <!-- 資料內容 -->
                                            <th>
                                                <spring:message code="LB.Data_content" />
                                            </th>
                                            <!-- 收付行 -->
                                            <th>
                                                <spring:message code="LB.Receiving_Bank" />
                                            </th>
                                            <!-- 交易時間 -->
                                            <th>
                                                <spring:message code="LB.Trading_time" />
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="dataList" items="${labelList.rowListMap }">
                                            <tr>
                                                <!-- 異動日期-->
                                                <td class="text-center">${dataList.SHOWLSTLTD }</td>
                                                <!-- 摘要 -->
                                                <td class="text-center">${dataList.MEMO_C }</td>
                                                <!-- 支出-->
                                                <td class="text-right">${dataList.DPWDAMTfmt }</td>
                                                <!-- 存入 -->
                                                <td class="text-right">${dataList.DPSVAMTfmt }</td>
                                                <!-- 餘額 -->
                                                <td class="text-right">${dataList.BALfmt }</td>
                                                <!-- 票據號碼 -->
                                                <td class="text-center">${dataList.CHKNUM }</td>
                                                <!-- 資料內容 -->
                                                <td class="text-center">${dataList.DATA16 }</td>
                                                <!-- 收付行 -->
                                                <td class="text-center">${dataList.TRNBRH }</td>
                                                <!--交易時間 -->
                                                <td class="text-center">${dataList.SHOWTRNTIME }</td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </c:forEach>
                            <!--button 區域 -->
                         	<!--回上頁 -->
                            <spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>
                            <input type="button" name="CMBACK" id="CMBACK" value="${cmback}" class="ttb-button btn-flat-gray">
                            <!-- 列印  -->
                            <spring:message code="LB.Print" var="printbtn"></spring:message>
                            <input type="button" id="printbtn" value="${printbtn}" class="ttb-button btn-flat-orange" />
                        	<!--button 區域 -->
                        </div>  
                    </div>
                    <div class="text-left">
                    	<ol class="description-list list-decimal">
	                        <!-- 		說明： -->
	                        <p>
								<spring:message code="LB.Description_of_page"/>
	                        </p>
	                            <li><spring:message code="LB.Demand_deposit_detail_P2_D1"/></li>
                        </ol>
                    </div>
                </form>
            </section>
            <!-- 		main-content END -->
        </main>
    </div>
    <!-- content row END -->
    <%@ include file="../index/footer.jsp"%>
</body>
<c:if test="${showDialog == 'true'}">
 	<%@ include file="../index/txncssslog.jsp"%>
 	</c:if>
</html>
