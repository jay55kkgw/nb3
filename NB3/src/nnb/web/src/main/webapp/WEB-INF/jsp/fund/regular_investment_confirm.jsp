<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript">
		// 初始化
		$(document).ready(function () {
			init();
		});


		function init() {
			//表單驗證
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
			// 確認鍵 click
			goOn();
			//上一頁按鈕
			back();
			//重新整理
			cmrest();
		}
		//上一頁按鈕
		function back() {
			$("#CMBACK").click(function () {
				var action = '${__ctx}/FUND/ALTER/regular_investment';
				initBlockUI();
				$('#back').val("Y");
				$("form").attr("action", action);
				$("#formId").validationEngine('detach');
				$("form").submit();
			});
		}
		//重新整理
		function cmrest() {
			$("#CMRESET").click(function () {
				$("#formId")[0].reset();
			});
		}
		// 確認鍵 click
		function goOn() {
			$("#CMSUBMIT").click(function (e) {
				if (!$('#formId').validationEngine('validate')) {
					e = e || window.event; // for IE
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					processQuery();
				}
			});
		}
		// 交易機制選項
		function processQuery() {
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
			switch (fgtxway) {
				case '0':
					$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
					$('#CMPASSWORD').val("");
					initBlockUI();//遮罩
					$("#formId").submit();
					break;
				default:
					//<!-- 請選擇交易機制 -->
					//alert("<spring:message code= "LB.Alert001" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert001' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
					);
			}
		}
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金    -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0901" /></li>
    <!-- 基金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1064" /></li>
    <!-- 定期投資約定變更     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1057" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!--定期投資約定變更 -->
				<h2>
					<spring:message code="LB.W1057" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form autocomplete="off" method="post" id="formId"
					action="${__ctx}/FUND/ALTER/regular_investment_result">
					<c:set var="BaseResultData" value="${regular_investment_confirm.data}"></c:set>
					<input type="hidden" id="back" name="back" value="">
					<%-- 驗證相關 --%>
					<input type="hidden" id="PINNEW" name="PINNEW" value="">
					<%--  TXTOKEN  防止重送代碼--%>
					<input type="hidden" name="TXTOKEN" id="TXTOKEN" value="${BaseResultData.TXTOKEN}" />
					<input type="hidden" name="ADOPID" value="C111">
					<input type="hidden" name="STOPBEGDAY" value="${BaseResultData.str_STOPBEGDAY}">
					<input type="hidden" name="STOPENDDAY" value="${BaseResultData.str_STOPENDDAY}">
					<input type="hidden" name="RESTOREDAY" value="${BaseResultData.str_RESTOREDAY}">
					<input type="hidden" id="PINNEW" name="PINNEW" value="">
					<input type="hidden" name="RSKATT" value="${BaseResultData.RSKATT}">
					<input type="hidden" name="RRSK" value="${BaseResultData.RRSK}">
					<!-- 表單顯示區 -->
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<!-- 信託帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.W0944" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.hide_CDNO}
											</span>
										</div>
									</span>
								</div>
								<!--扣款標的-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4>
												<spring:message code="LB.W1041" />
											</h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.FUNDLNAME}
											</span>
										</div>
									</span>
								</div>
								<!--類別 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4>
												<spring:message code="LB.D0973" />
											</h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span><spring:message code="${BaseResultData.str_MIP}" /></span>
										</div>
									</span>
								</div>
								<!--扣款方式 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4>
												<spring:message code="LB.D0173" />
											</h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												<spring:message code="${BaseResultData.str_DAMT}" />
											</span>
										</div>
									</span>
								</div>
								<!--變更項目  -->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4>
												<spring:message code="LB.W1153" />
											</h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												<c:if test="${BaseResultData.str_ALTERTYPE1 != ''}">
													<spring:message code="LB.Deposit_amount_1" />
												</c:if>
												<c:if test="${BaseResultData.str_ALTERTYPE2 != ''}">
													<spring:message code="LB.W1155" />
												</c:if>
												<c:if test="${BaseResultData.str_ALTERTYPE3 != ''}">
													<spring:message code="LB.W1156" />
												</c:if>
											</span>
										</div>
									</span>
								</div>
								<c:if test="${BaseResultData.str_ALTERTYPE1 != ''}">
									<!--原每次定期申購金額 -->
									<div class="ttb-input-item row">
										<span class="input-title"><label>
												<h4><spring:message code="${BaseResultData.str_OPAYAMT}" /> </h4>
											</label></span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.ADCCYNAME}
													${BaseResultData.display_OPAYAMT}
												</span>
											</div>
										</span>
									</div>
									<!--變更後每次定期申購金額 -->
									<div class="ttb-input-item row">
										<span class="input-title"><label>
												<h4><spring:message code="${BaseResultData.str_NPAYAMT}" /> </h4>
											</label></span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.ADCCYNAME}
													${BaseResultData.display_PAYAMT}
												</span>
											</div>
										</span>
									</div>
								</c:if>
								<c:if test="${BaseResultData.str_ALTERTYPE2 != ''}">
									<c:if test="${BaseResultData.DAMT == '2'}">
										<!--原日期 -->
										<div class="ttb-input-item row">
											<span class="input-title">
												<label>
													<h4>
														<spring:message code="LB.W1158" />
													</h4>
												</label>
											</span>
											<span class="input-block">
												<div class="ttb-input">
													<span>
														${BaseResultData.OPAYDAY4}
														${BaseResultData.OPAYDAY5}
														${BaseResultData.OPAYDAY6}
														${BaseResultData.OPAYDAY1}
														${BaseResultData.OPAYDAY2}
														${BaseResultData.OPAYDAY3}
													</span>
												</div>
											</span>
										</div>
										<!--變更後日期 -->
										<div class="ttb-input-item row">
											<span class="input-title">
												<label>
													<h4>
														<spring:message code="LB.W1159" />
													</h4>
												</label>
											</span>
											<span class="input-block">
												<div class="ttb-input">
													<span>
														${BaseResultData.PAYDAY4}
														${BaseResultData.PAYDAY5}
														${BaseResultData.PAYDAY6}
														${BaseResultData.PAYDAY1}
														${BaseResultData.PAYDAY2}
														${BaseResultData.PAYDAY3}
													</span>
												</div>
											</span>
										</div>
									</c:if>
									<c:if test="${BaseResultData.DAMT != '2'}">
										<!--原日期 -->
										<div class="ttb-input-item row">
											<span class="input-title">
												<label>
													<h4>
														<spring:message code="LB.W1158" />
													</h4>
												</label>
											</span>
											<span class="input-block">
												<div class="ttb-input">
													<span>
														${BaseResultData.OPAYDAY1}
														${BaseResultData.OPAYDAY2}
														${BaseResultData.OPAYDAY3}
														${BaseResultData.OPAYDAY4}
														${BaseResultData.OPAYDAY5}
														${BaseResultData.OPAYDAY6}
														${BaseResultData.OPAYDAY7}
														${BaseResultData.OPAYDAY8}
														${BaseResultData.OPAYDAY9}
													</span>
												</div>
											</span>
										</div>
										<!--變更後日期 -->
										<div class="ttb-input-item row">
											<span class="input-title"><label>
													<h4>
														<spring:message code="LB.W1159" />
													</h4>
												</label></span>
											<span class="input-block">
												<div class="ttb-input">
													<span>
														${BaseResultData.PAYDAY1}
														${BaseResultData.PAYDAY2}
														${BaseResultData.PAYDAY3}
														${BaseResultData.PAYDAY4}
														${BaseResultData.PAYDAY5}
														${BaseResultData.PAYDAY6}
														${BaseResultData.PAYDAY7}
														${BaseResultData.PAYDAY8}
														${BaseResultData.PAYDAY9}
													</span>
												</div>
											</span>
										</div>
									</c:if>
								</c:if>
								<c:if test="${BaseResultData.str_ALTERTYPE3 != ''}">
									<!--原扣款狀態 -->
									<div class="ttb-input-item row">
										<span class="input-title"><label>
												<h4>
													<spring:message code="LB.X0371" />
												</h4>
											</label></span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													<spring:message code="${BaseResultData.str_Before}" />
												</span>
											</div>
										</span>
									</div>
									<!--變更後扣款狀態-->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.W1161" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<spring:message code="${BaseResultData.str_After}" />
										</span>
									</div>
								</c:if>
								<!-- 交易機制 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.Transaction_security_mechanism" />
										</label>
									</span>
									<span class="input-block">
										<!-- 交易密碼SSL -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.SSL_password" />
												<input type="radio" name="FGTXWAY" checked="checked" value="0">
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!--請輸入密碼 -->
										<div class="ttb-input">
											<spring:message code="LB.Please_enter_password" var="pleaseEnterPin" />
											<input type="password" id="CMPASSWORD" name="CMPASSWORD" class="text-input validate[required]" maxlength="8" placeholder="${plassEntpin}">
										</div>
									</span>
								</div>
							</div>
							<!-- button -->
							<input class="ttb-button btn-flat-gray" name="CMBACK" id="CMBACK" type="button" value="<spring:message code="LB.Back_to_function_home_page" />" />
							<input class="ttb-button btn-flat-gray" name="CMRESET" id="CMRESET" type="button" value="<spring:message code="LB.Re_enter" />" />
							<input class="ttb-button btn-flat-orange" name="CMSUBMIT" id="CMSUBMIT" type="button" value="<spring:message code="LB.Confirm" />" />
							<!-- button -->
						</div>
					</div>
					<div class="text-left">
						<!-- 						說明： -->
						<!-- <spring:message code="LB.Description_of_page"/>: -->
					</div>
				</form>
			</section>
			<!-- main-content END -->
		</main>
	</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>