<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {
		// 將.table變更為footable
		initFootable();
	});
	
	//下拉式選單
 	function formReset() {
 		if ($('#actionBar').val()=="reEnter"){
	 		document.getElementById("formId").reset();
	 		$('#actionBar').val("");
 		}
	}
 
	//列印
	$("#printbtn").click(function(){
		var params = {
				"jspTemplateName":"demand_deposit_detail_result_print",
				"jspTitle":'<spring:message code="LB.NTD_Demand_Deposit_Detail"/>',
				"CMQTIME":"${demand_deposit_detail_result.data.CMQTIME}",
				"CMPERIOD":"${demand_deposit_detail_result.data.CMPERIOD}",
				"COUNT":"${demand_deposit_detail_result.data.COUNT}"
			};
			openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
	});
	//上一頁按鈕
	$("#CMBACK").click(function() {
		var action = '${__ctx}/NT/ACCT/demand_deposit_detail';
		$('#back').val("Y");
		$("form").attr("action", action);
		initBlockUI();
		$("form").submit();
	});
</script>
</head>
	<body>
		<!-- header     -->
		<header>
			<%@ include file="../index/header.jsp"%>
		</header>
	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 貸款服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Loan_Service" /></li>
    <!-- 借款查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Loan_Inquiry" /></li>
		</ol>
	</nav>

		<!-- menu、登出窗格 -->
		<div class="content row">
			<%@ include file="../index/menu.jsp"%>
			<!-- 	快速選單內容 -->
			<!-- content row END -->
			<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2><spring:message code= "LB.W0012" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 						下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
<!-- 						下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
					</select>
				</div>
				<form id="formId" method="post" action="">
					<div class="main-content-block row">
						<div class="col-12">
							<ul class="ttb-result-list">
								<li>
									<!-- 查詢時間 -->
									<p>
									<spring:message code="LB.Inquiry_time" />：
									${loan_detail.data.CMQTIME}
									</p>
									<!-- 查詢期間 -->
									<p>
									<spring:message code="LB.Inquiry_period_1"/>：
									${loan_detail.data.CMRECNUM}&nbsp;~&nbsp;${loan_detail.data.CMRECNUM}
									</p>	
										<!-- 資料總數 -->
										<p>
										<spring:message code="LB.Total_records" />：
										${loan_detail.data.CMRECNUM}&nbsp;<spring:message code="LB.Rows"/>
									</p>
								</li>
							</ul>
							<!-- 輕鬆理財證券交割明細 -->
							<table class="table" data-toggle-column="first">
								<thead>
									<tr>
										<!-- 帳號 -->
										<th><spring:message code="LB.Account"/></th>
										<th class="text-left" colspan="7">${loan_detail.data.CMRECNUM}</th>
									</tr>
									<tr>
										<!-- 異動日 -->
										<th><spring:message code="LB.Change_date"/></th>
										<!-- 摘要-->
										<th><spring:message code="LB.Summary_1"/></th>
										<!-- 借貸別 -->
										<th><spring:message code= "LB.W0015" /></th>
										<!-- 交易金額-->
										<th><spring:message code= "LB.W0016" /></th>
										<!-- 資料內容 -->
										<th><spring:message code="LB.Data_content"/></th>
										<!-- 收付行-->
										<th><spring:message code="LB.Receiving_Bank"/></th>
										<!-- 備註 -->
										<th><spring:message code="LB.Change_date"/></th>
										<!-- 摘要-->
										<th><spring:message code="LB.Note"/></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="dataList" items="${loan_detail.data.TW}">
										<tr>
											<td>${dataList.ACNCOVER}</td>
											<td class="text-left">${dataList.SEQ}</td>
											<td>${dataList.AMTORLN}</td>
											<td class="text-right">
												<fmt:formatNumber type="number" minFractionDigits="2" value="${dataList.BAL}"/>
											</td>
											<td class="text-right">
												<fmt:formatNumber type="number" minFractionDigits="2" value="${dataList.BAL}"/>
											</td>
											<td class="text-left">${dataList.SEQ}</td>
											<td>${dataList.ACNCOVER}</td>
											<td>${dataList.ACNCOVER}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
							<!-- 列印鈕-->					
							<input type="button" class="ttb-button btn-flat-gray" id="printbtn" value="<spring:message code="LB.Print" />"/>	
							<!-- 下載Excel檔-->
							<input type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Download_excel_file"/>"/>
							<!-- 下載.txt檔-->
							<input type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Download_txt_file"/>"/>
							<!-- 回上頁-->
                            <input type="button" class="ttb-button btn-flat-gray" name="CMBACK" id="CMBACK" value="<spring:message code="LB.Back_to_previous_page"/>"/>
						</div>
					</div>
				</form>
				</section>
			</main>
			<!-- 		main-content END -->
			<!-- 	content row END -->
		</div>
		<%@ include file="../index/footer.jsp"%>
	</body>
</html>