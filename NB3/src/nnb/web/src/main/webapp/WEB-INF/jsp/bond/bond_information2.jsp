<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",100);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",200);
	
	$("#CMSUBMIT").click(function(){
		if($('#ckFlag').prop('checked')){
			initBlockUI();
			$("#formID").attr("action","${__ctx}/BOND/PURCHASE/bond_information3");
			initBlockUI();//遮罩
			$("#formID").submit();
		}
		else{
			errorBlock(null, null, ['<spring:message code= "LB.X2555" />'],
					'<spring:message code= "LB.Confirm" />', null);
		}
	});

	$("#CMCANCEL").click(function(){
		initBlockUI();
		$("#formID").attr("action","${__ctx}/BOND/PURCHASE/bond_purchase_input");
		initBlockUI();//遮罩
		$("#formID").submit();
	});
	
});

</script>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 海外債券     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2391" /></li>
    <!-- 海外債券交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2515" /></li>
    <!-- 主動購買聲明書     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X2556" /></li>
		</ol>
	</nav>

	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
<!-- 			主動購買聲明書 -->
				<h2><spring:message code="LB.X2556" /></h2>
				<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
				<form id="formID" method="post">
					<input type="hidden" id="TOKEN" name = "TOKEN" value="${sessionScope.transfer_confirm_token}">
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
	                            <div class="ttb-message"">
<!-- 	                            主動購買聲明書 -->
                            		<span><b><spring:message code="LB.X2556" /></b></span>
								</div>
								<div class="ttb-message" style="text-align: left; width: 50%; margin-left: 25%; margin-left: 25%; margin-right: 25%;">
<!--                                 	本人確認本次申購/轉換之境外信託商品,係基於個人資產規劃之安排,經本人審慎考慮後所為之決定,且係本人主動要求貴行辦理,而非由貴行推介。 -->
                                	<span><b><spring:message code="LB.X2557" /></b></span>
                            	</div>
                            	<div class="ttb-input" style="text-align: center;">
									<label class="check-block" for="ckFlag">
										<input type="checkbox" name="ckFlag" id="ckFlag">
<!-- 										<b>本人已閱讀主動購買確認說明</b> -->
		                                	<b><spring:message code="LB.X2558" /></b>
		                                <span class="ttb-check"></span>
									</label>
                                </div>
                            </div>
							<input type="button" id="CMCANCEL" value="<spring:message code="LB.Cancel"/>" class="ttb-button btn-flat-gray"/>	
							<input type="button" id="CMSUBMIT" value="<spring:message code="LB.Confirm"/>" class="ttb-button btn-flat-orange"/>
						</div>
					</div>	
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>