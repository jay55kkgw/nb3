<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="page5" class="card-detail-block">
	<div class="card-center-block">
		<!-- Q1 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-51" aria-expanded="true"
				aria-controls="popup1-51">
				<div class="row">
					<span class="col-1">Q1</span>
					<div class="col-11">
						<span>如何确保网络银行交易之安全?</span>
					</div>
				</div>
			</a>
			<div id="popup1-51" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<ol class="ttb-result-list terms">
								<li>1、网络银行各类密码请勿告知他人。</li>
								<li>2、请避免以身分证统一编号、出生日期、公司或住家电话等易遭他人知悉的数据设定密码。</li>
								<li>3、个人计算机避免Browser设定记忆用户身分证统一编号及密码。</li>
								<li>4、请避免利用公共场所之计算机（如网咖）执行网络银行交易，以免遭不法人士利用键盘输入侧录程序记录用户所输入数据，窃取客户个人账号及密码等私密数据。</li>
								<li>5、请定期变更密码。</li>
								<li>6、离开座位时，请记得注销网络银行系统，并关闭浏览器。</li>
							</ol>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q2 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-52" aria-expanded="true"
				aria-controls="popup1-52">
				<div class="row">
					<span class="col-1">Q2</span>
					<div class="col-11">
						<span>为什么每次进入网络银行网页作登入动作，会显示上次输入数据，是否系统设定有问题，这样是不是造成计算机记忆ID和密码，该如何处理？</span>
					</div>
				</div>
			</a>
			<div id="popup1-52" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>
								此为客户端IE为方便您不用每次输入，而自动记忆的功能，并非本行网络银行系统所能控制范围，为防范您的密码外流或其他原因，请关闭此功能如下：<br>
								点选IE的工具栏中的『工具』→『因特网选项』→『内容』，在「个人信息」中有一个『自动完成』的按钮，后将『窗体上用户名称和密码』的选项'取消'并按『清除窗体』、『清除密码』，再按『确定』即可。<font color="red">建议不要使用自动记忆功能，以防ID及密码外泄。</font>。
							</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>