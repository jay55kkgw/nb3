<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
<br/><br/>
<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
<br/><br/><br/>
<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
<br/><br/><br/>
	<table class="print">
		<tbody>
			<tr>
				<td style="text-align:center"><spring:message code="LB.System_time" /></td>
				<td>${CMQTIME}</td>
			</tr>
			<tr>
				<td style="text-align:center"><spring:message code= "LB.W0781" /></td>
				<td><spring:message code="${MEMO_C}" /></td>
			</tr>
			<tr>			
				<td style="text-align:center"><spring:message code= "LB.W0702" /></td>
				<td>${TSFACN}</td>
			</tr>
			<tr>			
				<td style="text-align:center"><spring:message code= "LB.W0691" /></td>
				<td>${CUSNUM}</td>
			</tr>
			<c:if test="${MEMO_C == 'TLL'}">
			<tr>			
				<td style="text-align:center"><spring:message code= "LB.W0692" /></td>
				<td>${CUSNUM}</td>
			</tr>
			</c:if>
		</tbody>
	</table>
<br/><br/>
		<div class="text-left">
			<spring:message code="LB.Description_of_page" /> 
			<ol class="list-decimal text-left">
				<li><spring:message code= "LB.Withhold_Cancel_P4_D1" /></li>
				<li><spring:message code= "LB.Withhold_Cancel_P4_D2" /></li>
			</ol>
		</div>
</body>
</html>