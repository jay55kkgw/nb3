<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %> 
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/checkIdentity.js"></script>
	
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
	<!-- 讀卡機所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/VAckisIEbrower.js"></script>
	<script type="text/javascript" src="${__ctx}/component/util/Pkcs11ATLAgent.js"></script>
	    <script type="text/javascript">
		var notCheckIKeyUser = true; // 不驗證是否是IKey使用者
		//var myobj = null; // 自然人憑證用
		var urihost = "${__ctx}";
		
		var width = $(window).width();
		$(window).resize(function() {
			width = $(this).width();
			changeDisplay();
		});

        $(document).ready(function () {
			// HTML載入完成後0.1秒開始遮罩
			setTimeout("initBlockUI()", 100);

			// 初始化驗證碼
			setTimeout("initKapImg()", 200);
			// 生成驗證碼
			setTimeout("newKapImg()", 300);
			

			setTimeout("init()", 20);
			
			// 過0.5秒解遮罩
			setTimeout("unBlockUI(initBlockId)", 800);
        });
//         var isconfirm = false;
//     	$("#errorBtn1").click(function(){
//     		if(!isconfirm)
//     			return false;
//     		isconfirm = false;
//     		$("#naturalComponent")[0].click();
//     	});
//     	$("#errorBtn2").click(function(){
//     		isconfirm = false;
//     		$('#error-block').hide();
//     	});

    	// 初始化自然人元件
//     	function initNatural() {
//     		var Brow1 = new ckBrowser1();
//     		if(!Brow1.isIE){	
//     			if((navigator.userAgent.indexOf("Chrome") > -1) || (navigator.userAgent.indexOf("Firefox") > -1) || (navigator.userAgent.indexOf("Safari") > -1)) {
//     				myobj = initCapiAgentForChrome();
//     				try {
//     					if(sessionToken == null){
//     						myobj.initChrome();
//     						console.log("myobj.initChrome...");
//     					}
//     				} catch (e) {
// //     					if(confirm("<spring:message code= "LB.X1216" />")){
// //     						$("#naturalComponent")[0].click();
// //     					}
//     					isconfirm = true;
// 						errorBlock(
// 								null, 
// 								null,
// 								['<spring:message code= "LB.X1216" />'], 
// 								'<spring:message code= "LB.Confirm" />', 
// 								'<spring:message code= "LB.Cancel" />'
// 							);
//     				}			
//     			}
//     		} else {
//     			var strObject = "";
//     	    	// 使用 Script 判斷 win64 或 win32, 執行時間有點久
//     	 		if (navigator.platform.toLowerCase() == "win64"){
//     				// "Windows 64 位元";
//     	 			document.getElementById("obj").outerHTML = '<object id="myobj" classid="CLSID:32FDE779-F9DC-4D39-97FB-434102000101" codebase="${__ctx}/component/windows/natural/TBBankPkcs11ATLx64.cab" width="0px" height="0px"></object>';
//     			} else {
//     				// "Windows 32 位元 或 模擬 64 位元(WOW64)";
//     				document.getElementById("obj").outerHTML = '<object id="myobj" classid="CLSID:32FDE779-F9DC-4D39-97FB-434102000101" codebase="${__ctx}/component/windows/natural/TBBankPkcs11ATL.cab" width="0px" height="0px"></object>';
//     			}
//     		    myobj = document.myobj;
//     		}
//     		console.log("initNatural.finish...");
//     	}
		
		/**
		 * 初始化BlockUI
		 */
		function initBlockUI() {
			initBlockId = blockUI();
		}

		// 交易機制選項
		function processQuery(){
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
		
			switch(fgtxway) {
				case '0':
//						alert("交易密碼(SSL)...");
	    			$("form").submit();
					break;
					
				case '1':
//						alert("IKey...");
					var jsondc = $("#jsondc").val();
					
					// 遮罩後不給捲動
//						document.body.style.overflow = "hidden";

					// 呼叫IKEY元件
//						uiSignForPKCS7(jsondc);
					
					// 解遮罩後給捲動
//						document.body.style.overflow = 'auto';
					
					useIKey();
					break;
					
				case '2':
//						alert("晶片金融卡");

					// 遮罩後不給捲動
//						document.body.style.overflow = "hidden";
					
					// 呼叫讀卡機元件
					useCardReader();

					// 解遮罩後給捲動
//						document.body.style.overflow = 'auto';
					
			    	break;

				case '4':
//					自然人憑證
					useNatural();
			    	break;
				case '5':
					//跨行帳戶驗證
					initBlockUI();
					$("#formId").submit();
					break;			    	
				case '6':
					//跨行帳戶驗證
					initBlockUI();
					$("#formId").submit();
					break;
				default:
					alert("nothing...");
			}
			
		}
		
        function init() {
	    	$("#CMSUBMIT").click(function(e){
				var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
				var capData = $("#formId").serializeArray();
				var capResult = fstop.getServerDataEx(capUri, capData, false);
				if (capResult.result) {
					if (!$('#formId').validationEngine('validate')) {
						e.preventDefault();
					} else {
						$("#formId").validationEngine('detach');
	// 		        	initBlockUI();
			        	var action = '${__ctx}/DIGITAL/ACCOUNT/upload_digital_identity_result';
						$("form").attr("action", action);
		    			unBlockUI(initBlockId);
		    			processQuery();
					}
				} else {
					//alert("<spring:message code= "LB.X1082" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= "LB.X1082" />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
					changeCode();
				}
			});
			$("#CMBACK").click(function(e){
	        	var action = '${__ctx}/DIGITAL/ACCOUNT/upload_digital_identity_p2';
	        	$("#back").val('Y');
				$("form").attr("action", action);
    			$("form").submit();
			});

    		// 初始化自然人元件
    		initNatural();
			//img值初始化
			if("${input_data.data.FILE1}" != ""){
            	initImage("${input_data.data.FILE1}", '#img1');
			}
			if("${input_data.data.FILE2}" != ""){
            	initImage("${input_data.data.FILE2}", '#img2');
			}
			if("${input_data.data.FILE3}" != ""){
            	initImage("${input_data.data.FILE3}", '#img3');
			}
			if("${input_data.data.FILE4}" != ""){
            	initImage("${input_data.data.FILE4}", '#img4');
			}

			changeDisplay();
        }
        
        function changeDisplay(){
			<%-- 避免小版跑版 --%>
			if(width < 900){
				$("#bigBlock1").css("display","inline-block");
				$("#bigBlock2").css("display","inline-block");
// 				$("#hideBlock").hide();
			}
			else{
				$("#bigBlock1").css("display","flex");
				$("#bigBlock2").css("display","flex");
// 				$("#hideBlock").show();
			}
		}

		function ValidateValue(textbox) {
			var IllegalString = "+-_＿︿%@[`~!#$^&*()=|{}':;',\\[\\].<>/?~！#￥……&*（）——|{}【】‘；：”“'。，、？]‘'";
			var textboxvalue = textbox.value;
			var index = textboxvalue.length - 1;
			var s = textbox.value.charAt(index);
			if (IllegalString.indexOf(s) >= 0) {
				s = textboxvalue.substring(0, index);
				textbox.value = s;
			}
		}
		
		// 刷新驗證碼
		function refreshCapCode() {
			console.log("refreshCapCode...");

			// 驗證碼
			$('input[name="capCode"]').val('');
			
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

			// 登入失敗解遮罩
			unBlockUI(initBlockId);
		}
		
		// 刷新輸入欄位
		function changeCode() {
			console.log("changeCode...");
			
			// 清空輸入欄位
			$('input[name="capCode"]').val('');
			
			// 刷新驗證碼
			refreshCapCode();
		}

		// 初始化驗證碼
		function initKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
		}

		// 生成驗證碼
		function newKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').click(function() {
				refreshCapCode();
			});
		}

		//取得卡片主帳號結束
		function getMainAccountFinish(result){
			if(window.console){console.log("getMainAccountFinish: " + result);}
			//成功
			if(result != "false"){
				var cardACN = result;
				if(cardACN.length > 11){
					cardACN = cardACN.substr(cardACN.length - 11);
				}

				var UID = $("#CUSIDN").val();
//				var x = { method: 'get', parameters: '', onComplete: CheckIdResult };	
//				var myAjax = new Ajax.Request( "simpleform?trancode=N953&TXID=N953&ACN=" + cardACN, x);
				
				var uri = urihost+"/COMPONENT/component_without_id_aj";
				var rdata = { ACN: cardACN, UID: UID };
				fstop.getServerDataEx(uri, rdata, true, CheckIdResult);
			}
			//失敗
			else{
				showTempMessage(500,"晶片金融卡身份查驗","","MaskArea",false);
			}
		}
        function initImage(fileName, imgFilter){
            $.ajax({
                url: "${__ctx}/ONLINE/APPLY/apply_deposit_account_get_image",
                type: "POST",
                data: {
                	"filename": fileName
                },
		        dataType: "json",
                success: function (res) {
                	console.log(res);
                 if(res.data.validated){
 					 $(imgFilter).attr('src',res.data.picFile);
                     $(imgFilter+"_close_btn").show();
                 } else {
					$(imgFilter).after("<p class='photo-error-message' id='" + type + "_error'>" + res.data.summary + "</p>");
                     $(imgFilter+"_close_btn").hide();
                 }
                }
            });
        	
        }

    </script>
	<style>
	
		@media screen and (max-width:767px) {
		.ttb-button {
				width: 38%;
				left: 0px;
		}
		#CMBACK.ttb-button{
				border-color: transparent;
				box-shadow: none;
				color: #333;
				background-color: #fff;
		}
 

	}
</style>
</head>
<body>
	<div id="obj"></div>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	<!-- 自然人憑證元件載點 -->
	<a href="${__ctx}/component/windows/natural/TBBankPkcs11ATL.exe" id="naturalComponent" target="_blank" style="display:none"></a>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	<!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<!--數位存款帳戶 -->
			<li class="ttb-breadcrumb-item"><spring:message code= "LB.X0348" /></a></li>
			<!--修改數位存款帳戶資料 -->
			<li class="ttb-breadcrumb-item"><a href="#">補上傳身分證件</a></li>
		</ol>
	</nav>

	<!-- 自然人憑證元件載點 -->
	<a href="${__ctx}/component/windows/natural/TBBankPkcs11ATL.exe" id="naturalComponent" target="_blank" style="display:none"></a>
	<div class="content row">
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
				補上傳身分證件
				</h2>
				<div id="step-bar">
                    <ul>
                        <li class="finished">身分驗證</li>
                        <li class="finished">上傳證件</li>
                        <li class="active">確認資料</li>
                        <li class="">完成補件</li>
                    </ul>
                </div>
                <form id="formId" method="post">
                    <!-- ikey -->
					<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
					<input type="hidden" id="jsondc" name="jsondc" value='${input_data.data.jsondc }'>
					<input type="hidden" id="CUSIDN" name="CUSIDN" value="${input_data.data.CUSIDN}">
					<input type="hidden" id="IP" name="IP" value="${input_data.data.IP}">
					<input type="hidden" id="ISSUER" name="ISSUER" value="">
					<input type="hidden" id="ACNNO" name="ACNNO" value="">
					<input type="hidden" id="TRMID" name="TRMID" value="">
					<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
					<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
					<input type="hidden" id="TAC" name="TAC" value="">
					<input type="hidden" id="OUTACN" name="OUTACN" value="">
					<input type="hidden" id="AUTHCODE" name="AUTHCODE" value="">
					<input type="hidden" id="AUTHCODE1" name="AUTHCODE1" value="">
					
					<input type="hidden" id="ACN" name="ACN" value="">
					<input type="hidden" id="NOTIEINSTALL" name="NOTIEINSTALL" value="">
					<input type="hidden" id="CertFinger" name="CertFinger" value="">
					<input type="hidden" id="CertB64" name="CertB64" value="">
					<input type="hidden" id="CertSerial" name="CertSerial" value="">
					<input type="hidden" id="CertSubject" name="CertSubject" value="">
					<input type="hidden" id="CertIssuer" name="CertIssuer" value="">
					<input type="hidden" id="CertNotBefore" name="CertNotBefore" value="">
					<input type="hidden" id="CertNotAfter" name="CertNotAfter" value="">
					<input type="hidden" id="HiCertType" name="HiCertType" value="">
					<input type="hidden" id="CUSIDN4" name="CUSIDN4" value="">
					<input type="hidden" id="CHIP_ACN" name="CHIP_ACN" value="">
					
					<input type="hidden" id="back" name="back" value="">
						
					<input type="hidden" id="TOKEN" name="TOKEN" value="${input_data.data.TOKEN}" /><!-- 防止重複交易 -->
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
                        
							<div class="ttb-input-block">
								<div class="ttb-message">
		                            <p>確認資料</p>
		                        </div>
							
	                            <div class="classification-block">
	                                <p>分行資訊</p>
	                            </div>
							
<!-- 								<div class="ttb-input-item row"> -->
<!-- 									<span class="input-title">  -->
<!-- 									<label> -->
<!-- 										<h4> -->
<%-- 											<spring:message code="LB.D1105" /> --%>
<!-- 										</h4> -->
<!-- 									</label> -->
<!-- 									</span>  -->
<!-- 									<span class="input-block"> -->
<!-- 										<div class="ttb-input"> -->
<%-- 											${input_data.data.NAME} --%>
<%-- 											<input type="hidden" name="NAME" value="${input_data.data.NAME}"> --%>
<!-- 										</div> -->
<!-- 									</span> -->
<!-- 								</div> -->
<!-- 								********** -->
<!-- 								<div class="ttb-input-item row"> -->
<!-- 									<span class="input-title">  -->
<!-- 									<label> -->
<!-- 										<h4> -->
<%-- 											<spring:message code="LB.D0519" /> --%>
<!-- 										</h4> -->
<!-- 									</label> -->
<!-- 									</span>  -->
<!-- 									<span class="input-block"> -->
<!-- 										<div class="ttb-input"> -->
<%-- 											${input_data.data.CUSIDN} --%>
<%-- 											<input type="hidden" id="UID" name="UID" value="${input_data.data.CUSIDN}"> --%>
<!-- 										</div> -->
<!-- 									</span> -->
<!-- 								</div> -->
<!-- 								********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D1472" />
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.BRHCOD} ${input_data.data.BRHNAME}
											<input type="hidden" name="BRHCOD" value="${input_data.data.BRHCOD}">
											<input type="hidden" name="BRHNAME" value="${input_data.data.BRHNAME}">
										</div>
									</span>
								</div>
								
								
	                            <div class="classification-block">
	                                <p>身分證資料</p>
	                            </div>
								<!-- ***************** image ****************** -->
								<div class="photo-block" id="bigBlock1">
	                                <div>
										<img id="img1" src="${__ctx}/img/upload_empty.svg" />
										<input type="hidden" name="FILE1" value="${input_data.data.FILE1}">
	                                    <p><spring:message code="LB.D0111"/></p>
	                                </div>
	                                <div>
										<img id="img2" src="${__ctx}/img/upload_empty.svg" />
										<input type="hidden" name="FILE2" value="${input_data.data.FILE2}">
	                                    <p>身分證背面圖片</p>
	                                </div>
	                            </div>
								<!-- ***************************************** -->
	                            <div class="classification-block">
	                                <p>第二證件資料</p>
<!-- 	                                <a href="#" class="classification-edit-btn">編輯</a> -->
	                            </div>
								<!-- ***************************************** -->
								<!-- ********** -->
								 <div class="photo-block" id="bigBlock2">
	                                <div>
										<img id="img3" src="${__ctx}/img/upload_empty.svg" />
										<input type="hidden" name="FILE3" value="${input_data.data.FILE3}">
	                                    <p><spring:message code="LB.D1159"/></p>
	                                </div>
									<div>
										<img id="img4" src="${__ctx}/img/upload_empty.svg" />
										<input type="hidden" name="FILE4" value="${input_data.data.FILE4}">
	                                    <p>第二證件背面圖片</p>

	                                </div>
	                            </div>
								<!-- *********************************** -->
	                            <div class="classification-block">
	                                <p>身分驗證</p>
	                            </div>
								<!-- *********************************** -->
								
							<!--交易機制  SSL 密碼 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.Transaction_security_mechanism" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
	<!-- 									<div class="ttb-input"> -->
	<!-- 										<label> -->
	<%-- 								       		<input type="radio" name="FGTXWAY" id="CMSSL" value="0" checked /><spring:message code="LB.SSL_password" /> --%>
	<!-- 								       	</label> -->
	<!-- 										<input type="password" name = "CMPASSWORD" id="CMPASSWORD"  maxlength="8" class="text-input validate[required, pwvd[PW, CMPASSWORD], custom[onlyLetterNumber]]"> -->
	<!-- 										<input type="hidden" name = "PINNEW" id="PINNEW" value=""> -->
	<!-- 									</div> -->
	<!-- 									使用者是否可以使用IKEY -->
										<!-- 使用者是否可以使用IKEY -->
<%-- 										<c:if test = "${sessionScope.isikeyuser}"> --%>
											<!-- 即使使用者可以用IKEY，若轉出帳號為晶片金融卡帶出的非約定帳號，也不給用 (By 景森)-->
<%-- 											<c:if test = "${!result_data.TransferType.equals('NPD')}"> --%>
									
												<!-- IKEY -->
<!-- 												<div class="ttb-input"> -->
<!-- 													<label class="radio-block"> -->
<%-- 														<spring:message code="LB.Electronic_signature" /> --%>
<!-- 														<input type="radio" name="FGTXWAY" id="CMIKEY" value="1"> -->
<!-- 														<span class="ttb-radio"></span> -->
<!-- 													</label> -->
<!-- 												</div> -->
												
<%-- 											</c:if> --%>
<%-- 										</c:if> --%>
										
										<c:if test="${input_data.data.FGTXWAY.equals('4')}">
											<!-- 自然人憑證 -->
											<div class="ttb-input">
												<label class="radio-block">
													<spring:message code="LB.D0437" />
													
												 	<input type="radio" name="FGTXWAY" id="CMNPC" value="4" checked="checked">
														
													<span class="ttb-radio"></span>
													<input type="hidden" name="ACNTYPE" value="A1">
												</label>
											</div>
										</c:if>
										<c:if test="${input_data.data.FGTXWAY.equals('2')}">
											<!-- 晶片金融卡 -->
											<div class="ttb-input">
												<label class="radio-block">
													<spring:message code="LB.Financial_debit_card" />
													
												 	<input type="radio" name="FGTXWAY" id="CMCARD" value="2" checked="checked">
														
													<span class="ttb-radio"></span>
													<input type="hidden" name="ACNTYPE" value="A2">
												</label>
											</div>
										</c:if>
										<c:if test="${input_data.data.FGTXWAY.equals('5')}">
											<!-- 跨行金融帳戶認證 -->
											<div class="ttb-input">
												<label class="radio-block" > 
													<spring:message code="LB.Fisc_inter-bank_acc" /> 
													<input type="radio" name="FGTXWAY" id="CMFISCACC" value="5" checked="checked">
													<span class="ttb-radio"></span>
													<input type="hidden" name="ACNTYPE" value="A3">
												</label>
											</div	>											
										</c:if>	
										<c:if test="${input_data.data.FGTXWAY.equals('6')}">
											<!-- 跨行金融帳戶認證 -->
											<div class="ttb-input">
												<label class="radio-block">
													自行帳戶認證 
												 	<input type="radio" name="FGTXWAY" id="LOISCACC" value="6" checked="checked">														
													<span class="ttb-radio"></span>
													<input type="hidden" name="ACNTYPE" value="A4">
												</label>
											</div>
										</c:if>										
										
									</span>
								</div>
								<!-- 確認新使用者名稱 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.Captcha" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											
											<input type="text" class="text-input input-width-125" id="capCode" name="capCode" placeholder="請輸入驗證碼">
											<img name="kaptchaImage" src=""  class="verification-img"/>
											<input type="button" name="reshow" class="ttb-sm-btn btn-flat-gray" onclick="refreshCapCode()" value=" <spring:message code= "LB.Regeneration_1" />" />
											<!-- 英文不分大小寫，限半型字 -->
											&nbsp;<span class="input-remarks">請注意： <spring:message code= "LB.D0033" /> </span>
										</div>
									</span>
								</div>
								
								<c:if test="${input_data.data.FGTXWAY.equals('4')}">
									<!-- 自然人憑證PIN碼 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4><spring:message code="LB.D1540" /></h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<input type="PASSWORD" class="text-input" maxLength="8" size="10" id="NPCPIN" name="NPCPIN" value=""/>
											</div>
										</span>
									</div>
								</c:if>
                           	</div>
	                        <!--button 區域 -->
							<!-- 重新輸入 -->
							<input type="button" name="CMBACK" id="CMBACK" value="<spring:message code="LB.X0318" />" class="ttb-button btn-flat-gray">
                            <input id="CMSUBMIT" name="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="確認送出" />
                        <!--                     button 區域 -->
                        </div>  
                    </div>
                </form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>