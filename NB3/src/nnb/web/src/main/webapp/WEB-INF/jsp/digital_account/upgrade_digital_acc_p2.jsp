<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>
<script type="text/javascript" src="${__ctx}/js/jquery.maskedinput.js"></script>
<!-- 交易機制所需JS -->
<script type="text/javascript" src="${__ctx}/component/util/checkIdentity.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
<!-- 讀卡機所需JS -->
<script type="text/javascript" src="${__ctx}/component/util/VAckisIEbrower.js"></script>
<script type="text/javascript" src="${__ctx}/component/util/Pkcs11ATLAgent.js"></script>
<script type="text/javascript">
		var notCheckIKeyUser = true; // 不驗證是否是IKey使用者
		var urihost = "${__ctx}";
		$(document).ready(function () {
			// HTML載入完成後0.1秒開始遮罩
			setTimeout("initBlockUI()", 100);
			// 自然人憑證
			setTimeout("initNatural_EXCUTE()", 100);
			// 初始化驗證碼
			setTimeout("initKapImg()", 200);
			// 生成驗證碼
			setTimeout("newKapImg()", 300);

			setTimeout("init()", 20);
			// 過3秒解遮罩
			setTimeout("unBlockUI(initBlockId)", 3000);
		});

		// 初始化BlockUI
		function initBlockUI() {
			initBlockId = blockUI();
		}

		// 交易機制選項
		function processQuery() {
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();

			switch (fgtxway) {
				case '4':
					// 自然人憑證
					$('#UID').val($('#CUSIDN').val());
					//useNatural();
					try{
						var ret=myobj.GetServerAuth();
						if(ret){
							useNatural();
						}
						else{
							errorBlock(null, null, ["<spring:message code='LB.X1668' />"],
									'<spring:message code= "LB.Confirm" />', null);
						}
					} catch(e){
						errorBlock(null, null, ["<spring:message code='LB.X1668' />"],
								'<spring:message code= "LB.Confirm" />', null);
					}
					break;

				default:
					alert("nothing...");
			}
		}

		function init() {
			$("#formId").validationEngine({
				validationEventTriggers: 'keyup blur',
				binded: true,
				scroll: true,
				addFailureCssClassToField: "isValid",
				promptPosition: "inline"
			});
			$("#CMSUBMIT").click(function (e) {

				var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
				var capData = $("#formId").serializeArray();
				var capResult = fstop.getServerDataEx(capUri, capData, false);
				$("#CUSIDN").val($("#CUSIDN").val().toUpperCase());

				if (capResult != null && capResult.result) {
					if (!$('#formId').validationEngine('validate')) {
						e.preventDefault();
					} else {
						$("#formId").validationEngine('detach');
						initBlockUI();
						var action = '${__ctx}/ONLINE/SERVING/upgrade_digital_acc_p3';
						$("#formId").attr("action", action);
						unBlockUI(initBlockId);
						processQuery();
					}
				} else {
					var b = ["<spring:message code= "LB.X1082" />"];
					errorBlock(null, null, b, '確定', null);
					changeCode();
				}
			});
			$("#CMBACK").click(function (e) {

				$("#formId").attr("action", "${__ctx}/ONLINE/SERVING/upgrade_digital_acc_p1");
				$("#back").val('Y');

				initBlockUI();
				$("#formId").submit();
			});

			//showNature();
		}

		// 刷新驗證碼
		function refreshCapCode() {
			console.log("refreshCapCode...");

			// 驗證碼
			$('input[name="capCode"]').val('');

			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

			// 登入失敗解遮罩
			unBlockUI(initBlockId);
		}

		// 刷新輸入欄位
		function changeCode() {

			// 清空輸入欄位
			$('input[name="capCode"]').val('');

			// 刷新驗證碼
			refreshCapCode();
		}

		// 初始化驗證碼
		function initKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
		}

		// 生成驗證碼
		function newKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').click(function () {
				refreshCapCode();
			});
		}

		// 關閉彈出視窗
		function closeDialog() {
			$('#remind-block').hide();
			$('#windowMask').hide();
		}

		function setVAID(idn) {
			$("#CUSIDN").val(idn.toUpperCase());
		}

	</script>
<style>
button:focus {
	outline: none !important;
}
input.tbb-disabled {
	background-color: #d8d8d8 !important;
	border-color: #bebec0 !important;
	color: #000 !important;
	pointer-events: none !important;
	cursor: no-drop !important;
}
@media screen and (max-width:767px) {
	.ttb-button {
		width: 38%;
		left: 36px;
	}
	#CMBACK.ttb-button {
		border-color: transparent;
		box-shadow: none;
		color: #333;
		background-color: #fff;
	}
	.text-input[type="password"] {
		width: calc(100% - 40px) !important;
	}
}
</style>
</head>
<body>
	<!-- 自然人憑證登入失敗 -->
	<c:if test="false">
		<section id="remind-block" class="error-block">
			<div class="error-for-message">
				<p class="error-title"></p>
				<p class="error-info">自然人憑證登入失敗</p>
				<button id="textBtn" class="btn-flat-orange ttb-pup-btn" onclick="closeDialog();">
					<spring:message code="LB.X1572" />
				</button>
			</div>
		</section>
	</c:if>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component_u2.jsp"%>
	<%@ include file="../component/otp_component.jsp"%>
	<!-- 自然人憑證元件載點 -->
	<a href="${__ctx}/component/windows/natural/TBBankPkcs11ATL.exe" id="naturalComponent" target="_blank" style="display: none"></a>
	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	<!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<!-- 數位存款帳戶第三類升級為第一類 -->
			<li class="ttb-breadcrumb-item active" aria-current="page">數位存款帳戶第三類升級為第一類</li>
		</ol>
	</nav>
	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 主頁內容  -->
		<main class="col-12">
		<section id="main-content" class="container">
			<h2>
				<!-- 數位存款帳戶第三類升級為第一類 -->
				數位存款帳戶第三類升級為第一類
			</h2>
			<div id="step-bar">
				<ul>
					<li class="finished">注意事項</li>
					<li class="active">交易驗證</li>
					<li class="">完成申請</li>
				</ul>
			</div>
			<form method="post" id="formId" name="formId">
				<input type="hidden" id="AUTHCODE" name="AUTHCODE"/>
				<input type="hidden" id="AUTHCODE1" name="AUTHCODE1"/>
				<input type="hidden" id="CertFinger" name="CertFinger"/>
				<input type="hidden" id="CertB64" name="CertB64"/>
				<input type="hidden" id="CertSerial" name="CertSerial"/>
				<input type="hidden" id="CertSubject" name="CertSubject"/>
				<input type="hidden" id="CertIssuer" name="CertIssuer"/>
				<input type="hidden" id="CertNotBefore" name="CertNotBefore"/>
				<input type="hidden" id="CertNotAfter" name="CertNotAfter"/>
				<input type="hidden" id="HiCertType" name="HiCertType"/>
				<input type="hidden" id="CUSIDN4" name="CUSIDN4"/>
				<input type="hidden" id="UID" name="UID"  value="">			
				<input type="hidden" id="CUSIDN" name="CUSIDN" value="${digital_acc.CUSIDN}">
				<!-- 表單顯示區  -->
				<div class="main-content-block row">
					<div class="col-12">
						<div class="ttb-input-block">
							<div class="ttb-message">
								<p>交易驗證</p>
							</div>
							<!-- 存款帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.B0016" />
											<span class="high-light">*</span>
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input id="DIGACN" name="DIGACN" class="text-input tbb-disabled" value="${digital_acc.DIGACN}" readonly>
									</div>
								</span>
							</div>
							<!--交易機制  SSL 密碼 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>安控機制</h4>
									</label>
								</span> 
								<span class="input-block"> <!-- 自然人憑證 -->
									<div class="ttb-input">
										<label class="radio-block " for="CMNPC" onclick=""> 
											<spring:message code="LB.D0437" /> <span class="d-inline-block d-lg-none">(限桌機)</span> 
											<input type="radio" name="FGTXWAY" id="CMNPC" value="4" checked> 
											<span class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>
							<div id="NPCDIV">
								<!-- 自然人憑證PIN碼 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.D1540" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="PASSWORD" class="text-input" maxLength="8" size="10" id="NPCPIN" name="NPCPIN" value="" placeholder="請輸入自然人憑證PIN碼" />
										</div>
									</span>
								</div>
							</div>
							<!-- 確認新使用者名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.Captcha" />
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<spring:message code="LB.Captcha" var="labelCapCode" />
										<input id="capCode" type="text" class="text-input input-width-125" name="capCode" placeholder="${labelCapCode}" maxlength="6" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/g,'').toUpperCase();" autocomplete="off"> 
										<img name="kaptchaImage" src="" class="verification-img"/> 
										<input type="button" name="reshow" class="ttb-sm-btn btn-flat-gray" onclick="refreshCapCode()" value='<spring:message code="LB.Regeneration"/>' /> 
										<span class="input-remarks">請注意：英文不分大小寫，限半形字元</span>
									</div>
								</span>
							</div>
						</div>
						<!-- 按鈕 -->
						<input class="ttb-button btn-flat-gray" type="button" name="BACK" id="CMBACK" value="<spring:message code="LB.Back_to_previous_page" />"  />
						<input type="button" id="CMSUBMIT" value="<spring:message code="LB.Confirm" />" class="ttb-button btn-flat-orange"/>
					</div>
				</div>
			</form>
			<ol class="list-decimal description-list">
				<p>
					<spring:message code="LB.Description_of_page" />
				</p>
				<li><span>數位存款帳戶第三類升級為數位存款帳戶第一類，無法取消申請或復原。</span></li>
				<li><span>升級後之非約定轉帳限額: 每筆限新臺幣5萬元整，每日限新臺幣10萬元整，每月限新臺幣20萬元整; 本人親臨服務分行申請「約定轉帳」功能後，則比照貴行一般存款帳戶臨櫃約定轉帳之交易限額，依不同安控機制訂定之每日交易限額。</span></li>
			</ol>
		</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
<script type="text/javascript">
	function checkTC() {
		component_isikeyuser(); // 判斷使用者是否ikey使用者
		component_version(); // 取得各元件的最新版本號
		component_platform(); // 取得裝置作業系統
		component_initKeyBoard(); // 動態鍵盤初始化
		// setTimeout("component_init()", 50);
	}
</script>
</html>