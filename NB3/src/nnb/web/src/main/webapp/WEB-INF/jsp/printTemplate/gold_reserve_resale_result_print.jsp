<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust:exact">
	<br/><br/>
	<div style="text-align:center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/>
	</div>
	<br/><br/><br/>
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<table class="print">
		<tr>
<!-- 資料時間 -->
			<td><spring:message code= "LB.D0339" /></td>
		 	<td>${CMQTIME}</td>
		</tr>
		<tr>
<!-- 黃金轉出帳號 -->
  			<td><spring:message code= "LB.W1515" /></td>
			<td>${ACN}</td>
		</tr>
		<tr>
<!-- 臺幣轉入帳號 -->
  			<td><spring:message code= "LB.W1518" /></td>
			<td>${SVACN}</td>
		</tr>
		<tr>
<!-- 賣出公克數 -->
  			<td><spring:message code= "LB.W1519" /></td>
<!-- 公克 -->
			<td>${TRNGDFormat}<spring:message code= "LB.W1435" /></td>
		</tr>
	</table>
	<div>
		<p style="text-align:left;">
<!-- 說明 -->
			<spring:message code= "LB.Description_of_page" /><br/>
			<!-- 預約黃金回售將以 預約後的第一個營業日的第一次牌告買進價格交易。 -->
				1.<spring:message code= "LB.gold_resale_PP1_D4" /><font color="red"><spring:message code= "LB.gold_resale_PP1_D5" /></font><br/>
			<!-- 電子簽章：為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章（載具i－key）拔除並登出系統。 -->
				2.<spring:message code= "LB.Gold_Reserve_Resale_P4_D2" />
		</p>
	</div>
</body>
</html>