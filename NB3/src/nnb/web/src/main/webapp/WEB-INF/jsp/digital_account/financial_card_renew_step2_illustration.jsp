<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
<head>
<title>臺灣中小企業銀行股份有限公司履行個人資料保護法第8條第1項告知義務</title>
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:新細明體;
	panose-1:2 2 3 0 0 0 0 0 0 0;}
@font-face
	{font-family:"\@新細明體";
	panose-1:2 2 3 0 0 0 0 0 0 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0pt;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman";}
@page Section1
	{size:595.3pt 841.9pt;
	margin:35.95pt 28.3pt 35.95pt 27.0pt;
	layout-grid:18.0pt;}
div.Section1
	{page:Section1;}
-->
</style>

</head>

<body lang=ZH-TW style='text-justify-trim:punctuation'>

<div class="WordSection1" style="layout-grid:18.0pt">

<p class="MsoNormal" align="center" style="text-align:center;line-height:12.0pt"><b><span style="font-family:&quot;新細明體&quot;,serif">臺灣中小企業銀行股份有限公司履行個人資料保護法第<span lang="EN-US">8</span>條第<span lang="EN-US">1</span>項告知義務<span lang="EN-US"><o:p></o:p></span></span></b></p>

<p class="MsoNormal" align="center" style="text-align:center;line-height:12.0pt"><span lang="EN-US"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal" style="margin-left:37.8pt;text-indent:-19.8pt;line-height:
12.0pt"><span style="font-family:&quot;新細明體&quot;,serif">ㄧ<span style="color:black">、</span>由於個人資料之蒐集，涉及
臺端的隱私權益，臺灣中小企業銀行股份有限公司<span lang="EN-US">(</span>以下稱本行<span lang="EN-US">)</span>向 臺端蒐集個人資料時，依據個人資料保護法<span lang="EN-US">(</span>以下稱個資法<span lang="EN-US">)</span>第<span lang="EN-US">8</span>條第<span lang="EN-US">1</span>項規定，應明確告知 臺端下列事項：<span lang="EN-US">1.</span>非公務機關名稱<span lang="EN-US">2.</span>蒐集之目的<span lang="EN-US">3.</span>個人資料之類別<span lang="EN-US">4.</span>個人資料利用之期間、地區、對象及方式<span lang="EN-US">5.</span>當事人依個資法第<span lang="EN-US">3</span>條規定得行使之權利及方式<span lang="EN-US">6.</span>當事人得自由選擇提供個人資料時，不提供將對其權益之影響。</span></p>

<p class="MsoNormal" style="margin-left:37.8pt;text-indent:-19.8pt;line-height:
12.0pt"><span style="font-family:&quot;新細明體&quot;,serif">二<span style="color:black">、</span>有關本行蒐集
臺端個人資料之目的、個人資料類別及個人資料利用之期間、地區、對象及方式等內容，請 臺端詳閱如後附表。</span></p>

<p class="MsoNormal" style="margin-left:37.8pt;text-indent:-19.8pt;line-height:
12.0pt"><span style="font-family:&quot;新細明體&quot;,serif">三<span style="color:black">、</span>依據個資法第<span lang="EN-US">3</span>條規定，臺端就本行保有 臺端之個人資料得行使下列權利：</span></p>

<p class="MsoNormal" style="margin-left:46.8pt;text-indent:-10.8pt;line-height:
12.0pt"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">1.</span><span style="font-family:&quot;新細明體&quot;,serif">除有個資法第<span lang="EN-US">10</span>條所規定之例外情形外，得向本行查詢、請求閱覽或請求製給複製本，惟本行依<span style="letter-spacing:-.3pt">個資</span>法<span style="letter-spacing:-.3pt">第<span lang="EN-US">14</span>條規定</span>得酌收必要成本費用。</span></p>

<p class="MsoNormal" style="margin-left:46.8pt;text-indent:-10.8pt;line-height:
12.0pt"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">2.</span><span style="font-family:&quot;新細明體&quot;,serif">得向本行請求補充或更正，惟依個資法施行細則第<span lang="EN-US">19</span>條規定，臺端應適當釋明其原因及事實。</span></p>

<p class="MsoNormal" style="margin-left:46.8pt;text-indent:-10.8pt;line-height:
12.0pt"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">3.</span><span style="font-family:&quot;新細明體&quot;,serif">本行如有違反個資法規定蒐集、處理或利用<span lang="EN-US">&nbsp; </span>臺端之個人資料，依個資法第<span lang="EN-US">11</span>條第<span lang="EN-US">4</span>項規定，臺端得向本行請求停止蒐集。</span></p>

<p class="MsoNormal" style="margin-left:46.8pt;text-indent:-10.8pt;line-height:
12.0pt"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">4.</span><span style="font-family:&quot;新細明體&quot;,serif">依個資法第<span lang="EN-US">11</span>條第<span lang="EN-US">2</span>項規定，個人資料正確性有爭議者，得向本行請求停止處理或利用 臺端之個人資料。惟依該項但書規定，本行因執行業務所必須</span><span lang="ZH-HK" style="font-family:&quot;新細明體&quot;,serif;mso-fareast-language:ZH-HK">或經 臺端書面同意，並經註明其爭議者</span><span style="font-family:&quot;新細明體&quot;,serif">，不在此限。</span></p>

<p class="MsoNormal" style="margin-left:46.8pt;text-indent:-10.8pt;line-height:
12.0pt"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">5.</span><span style="font-family:&quot;新細明體&quot;,serif">依個資法第<span lang="EN-US">11</span>條第<span lang="EN-US">3</span>項規定，個人資料蒐集之特定目的消失或期限屆滿時，得向本行請求刪除、停止處理或利用 臺端之個人資料。惟依該項但書規定，本行因執行業務所必須或經
臺端書面同意者，不在此限。</span></p>

<p class="MsoNormal" style="margin-left:37.8pt;text-indent:-19.8pt;line-height:
12.0pt"><span style="font-family:&quot;新細明體&quot;,serif">四<span style="color:black">、</span>臺端如欲行使上述個資法第<span lang="EN-US">3</span>條規定之各項權利，有關如何行使之方式，得向本行客服<span lang="EN-US">(0800-00-7171</span>）詢問或於本行網站（網址：<span lang="EN-US">http://www.tbb.com.tw</span>）查詢。</span></p>

<p class="MsoNormal" style="margin-left:37.8pt;text-indent:-19.8pt;line-height:
12.0pt"><span style="font-family:&quot;新細明體&quot;,serif">五<span style="color:black">、</span>臺端得自由選擇是否提供相關個人資料及類別，惟
臺端所拒絕提供之個人資料及類別，如果是辦理業務審核或作業所需之資料，本行可能無法進行必要之業務審核或作業而無法提供 臺端相關服務或無法提供較佳之服務，敬請見諒。</span></p>

<p class="MsoNormal" style="margin-left:37.8pt;text-indent:-19.8pt;line-height:
12.0pt"><i><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">&nbsp;</span></i></p>

<table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" align="left" width="0" style="width:551.15pt;border-collapse:collapse;mso-yfti-tbllook:1184;
 mso-table-lspace:9.0pt;margin-left:6.75pt;mso-table-rspace:9.0pt;margin-right:
 6.75pt;mso-table-anchor-vertical:paragraph;mso-table-anchor-horizontal:column;
 mso-table-left:left;mso-padding-alt:0cm 0cm 0cm 0cm">
 <tbody><tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;height:20.65pt">
  <td width="284" colspan="3" style="width:210.95pt;border:solid black 1.0pt;
  border-bottom:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:20.65pt">
  <p class="MsoNormal" align="center" style="text-align:center;line-height:12.0pt;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;
  mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">特定目的說明</span><span lang="EN-US"><o:p></o:p></span></b></p>
  </td>
  <td width="112" rowspan="2" style="width:3.0cm;border:solid black 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt;height:20.65pt">
  <p class="MsoNormal" style="line-height:12.0pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">蒐集之個人資料類別</span><span lang="EN-US"><o:p></o:p></span></b></p>
  </td>
  <td width="113" rowspan="2" style="width:3.0cm;border:solid black 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt;height:20.65pt">
  <p class="MsoNormal" style="line-height:12.0pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">個人資料利用之期間</span><span lang="EN-US"><o:p></o:p></span></b></p>
  </td>
  <td width="57" rowspan="2" style="width:42.55pt;border:solid black 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt;height:20.65pt">
  <p class="MsoNormal" style="line-height:12.0pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">個人資料利用之地區</span><span lang="EN-US"><o:p></o:p></span></b></p>
  </td>
  <td width="113" rowspan="2" style="width:3.0cm;border-top:solid black 1.0pt;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.65pt">
  <p class="MsoNormal" style="line-height:12.0pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">個人資料利用之對象</span><span lang="EN-US"><o:p></o:p></span></b></p>
  </td>
  <td width="57" rowspan="2" style="width:42.5pt;border-top:solid black 1.0pt;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.65pt">
  <p class="MsoNormal" style="line-height:12.0pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">個人資料利用之方式</span><span lang="EN-US"><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:1;height:56.55pt">
  <td width="31" style="width:20.85pt;border:solid black 1.0pt;border-top:none;
  padding:0cm 5.4pt 0cm 5.4pt;height:56.55pt">
  <p class="MsoNormal" align="center" style="text-align:center;line-height:12.0pt;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;
  mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">業務類別</span><span lang="EN-US"><o:p></o:p></span></b></p>
  </td>
  <td width="121" style="width:90.9pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:56.55pt">
  <p class="MsoNormal" style="line-height:12.0pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">業務特定目的及代號</span><span lang="EN-US"><o:p></o:p></span></b></p>
  </td>
  <td width="131" style="width:99.2pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:56.55pt">
  <p class="MsoNormal" align="center" style="text-align:center;line-height:12.0pt;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;
  mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">共通特定目的及代號</span><span lang="EN-US"><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:2;height:255.95pt">
  <td width="31" valign="top" style="width:20.85pt;border-top:none;border-left:
  solid black 1.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:255.95pt">
  <p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
  line-height:12.0pt;mso-element:frame;mso-element-frame-hspace:9.0pt;
  mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:
  column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">存匯業務</span><span lang="EN-US"><o:p></o:p></span></b></p>
  </td>
  <td width="121" valign="top" style="width:90.9pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:255.95pt">
  <p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
  line-height:12.0pt;mso-element:frame;mso-element-frame-hspace:9.0pt;
  mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:
  column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">022</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">外匯業務</span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:19.1pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-19.1pt;line-height:12.0pt;mso-element:frame;
  mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:
  paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">036</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">存款與匯款業務
  </span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:17.75pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-17.75pt;line-height:12.0pt;mso-element:frame;
  mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:
  paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">067</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">信用卡、轉帳卡或電子票證業務</span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:16.45pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-16.45pt;line-height:12.0pt;mso-element:frame;
  mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:
  paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">082</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">借款戶與存款戶存借作業綜合管理
  </span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:19.55pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-19.55pt;line-height:12.0pt;mso-element:frame;
  mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:
  paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">112</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">票據交換業務
  </span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:18.2pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-18.45pt;line-height:12.0pt;mso-element:frame;
  mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:
  paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">181</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">其他經營合於營業登記項目或組織章程所定之業務（例如：黃金存摺業務、電子金融業務、代理收付業務、共同行銷或合作推廣業務等。）</span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:18.0pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-18.0pt;line-height:12.0pt;mso-element:frame;
  mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:
  paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">044</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">投資管理<span lang="EN-US">(</span>限輕鬆理財帳戶適用<span lang="EN-US">)</span></span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:18.2pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-18.45pt;line-height:12.0pt;mso-element:frame;
  mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:
  paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">068</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">信託業務<span lang="EN-US">(</span>限輕鬆理財帳戶適用<span lang="EN-US">)</span></span><span lang="EN-US"><o:p></o:p></span></b></p>
  </td>
  <td width="131" rowspan="2" valign="top" style="width:99.2pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:255.95pt">
  <p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
  line-height:12.0pt;mso-element:frame;mso-element-frame-hspace:9.0pt;
  mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:
  column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">040</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">行銷
  </span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:18.9pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-18.9pt;line-height:12.0pt;mso-element:frame;
  mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:
  paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">059</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">金融服務業依法令規定及金融監理需要，所為之蒐集處理及利用</span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:18.9pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-18.9pt;line-height:12.0pt;mso-element:frame;
  mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:
  paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">060</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">金融爭議處理</span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:18.25pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-18.25pt;line-height:12.0pt;mso-element:frame;
  mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:
  paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">063</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">非公務機關依法定義務所進行個人資料之蒐集處理及利用
  </span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:18.25pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-18.25pt;line-height:12.0pt;mso-element:frame;
  mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:
  paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">069</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">契約、類似契約或其他法律關係管理之事務</span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:20.6pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-20.6pt;line-height:12.0pt;mso-element:frame;
  mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:
  paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">090</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">消費者、客戶管理與服務
  </span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
  line-height:12.0pt;mso-element:frame;mso-element-frame-hspace:9.0pt;
  mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:
  column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">091</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">消費者保護
  </span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
  line-height:12.0pt;mso-element:frame;mso-element-frame-hspace:9.0pt;
  mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:
  column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">098</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">商業與技術資訊
  </span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:20.6pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-20.6pt;line-height:12.0pt;mso-element:frame;
  mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:
  paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">104</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">帳務管理及債權交易業務
  </span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:20.6pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-20.6pt;line-height:12.0pt;mso-element:frame;
  mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:
  paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">136</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">資<span lang="EN-US">(</span>通<span lang="EN-US">)</span>訊與資料庫管理 </span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
  line-height:12.0pt;mso-element:frame;mso-element-frame-hspace:9.0pt;
  mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:
  column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">137</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">資通安全與管理</span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:14.2pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-14.2pt;line-height:12.0pt;mso-element:frame;
  mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:
  paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">157</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">調查、統計與研究分析
  </span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:14.2pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-14.2pt;line-height:12.0pt;mso-element:frame;
  mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:
  paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">182</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">其他諮詢與顧問服務</span><span lang="EN-US"><o:p></o:p></span></b></p>
  </td>
  <td width="112" rowspan="2" valign="top" style="width:3.0cm;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:255.95pt">
  <p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
  line-height:12.0pt;mso-element:frame;mso-element-frame-hspace:9.0pt;
  mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:
  column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;letter-spacing:-.1pt">姓名、身分證統一編號、性別、出生年月日、通訊方式及其他詳如相關業務申請書或契約書之內容，並以本行與客戶往來之相關業務、帳戶或服務及</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">自客戶或第三人處（例如：財團法人金融聯合徵信中心）<span style="letter-spacing:-.1pt">所實際蒐集之個人資料為準。</span></span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
  line-height:12.0pt;mso-element:frame;mso-element-frame-hspace:9.0pt;
  mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:
  column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">&nbsp;</span><span lang="EN-US"><o:p></o:p></span></b></p>
  </td>
  <td width="113" rowspan="2" valign="top" style="width:3.0cm;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:255.95pt">
  <p class="MsoNormal" style="margin-left:16.85pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-16.85pt;line-height:12.0pt;mso-element:frame;
  mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:
  paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">ㄧ、特定目的存續期間。</span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:17.45pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-17.45pt;line-height:12.0pt;mso-element:frame;
  mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:
  paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">二、依相關法令所定（例如商業會計法等<span lang="EN-US">)</span>或因執行業務所必須之保存期間或依個別契約就資料之保存所定之保存年限。</span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:17.45pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-17.45pt;line-height:12.0pt;mso-element:frame;
  mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:
  paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">（以期限最長者為準）</span><span lang="EN-US"><o:p></o:p></span></b></p>
  </td>
  <td width="57" rowspan="2" valign="top" style="width:42.55pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:255.95pt">
  <p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
  line-height:12.0pt;mso-element:frame;mso-element-frame-hspace:9.0pt;
  mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:
  column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">右邊「個人資料利用之對象」欄位所列之利用對象其國內及國外所在地。</span><span lang="EN-US"><o:p></o:p></span></b></p>
  </td>
  <td width="113" rowspan="2" valign="top" style="width:3.0cm;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:255.95pt">
  <p class="MsoNormal" style="margin-left:18.55pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-18.55pt;line-height:12.0pt;mso-element:frame;
  mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:
  paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">ㄧ、本行<span lang="EN-US">(</span>含受本行委託處理事務之委外機構<span lang="EN-US">)</span>。</span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:17.65pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-17.65pt;line-height:12.0pt;mso-element:frame;
  mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:
  paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">二、依法令規定利用之機構（例如：本行母公司或所屬金融控股公司等）。</span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:17.65pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-17.65pt;line-height:12.0pt;mso-element:frame;
  mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:
  paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">三、其他業務相關之機構（例如：通匯行、財團法人金融聯合徵信中心、財團法人聯合信用卡處理中心、台灣票據交換所、財金資訊股份有限公司、信用保證機構、信用卡國際組織、收單機構暨特約商店等）。</span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:17.65pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-17.65pt;line-height:12.0pt;mso-element:frame;
  mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:
  paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">四、依法有權機關或金融監理機關。</span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:17.65pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-17.65pt;line-height:12.0pt;mso-element:frame;
  mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:
  paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">五、客戶所同意之對象（例如本行共同行銷或交互運用客戶資料之公司、與本行合作推廣業務之公司等）。</span><span lang="EN-US"><o:p></o:p></span></b></p>
  </td>
  <td width="57" rowspan="2" valign="top" style="width:42.5pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:255.95pt">
  <p class="MsoNormal" style="line-height:12.0pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">符合個人資料保護相關法令以自動化機器或其他非自動化之利用方式。</span><span lang="EN-US"><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style="mso-yfti-irow:3;mso-yfti-lastrow:yes;height:187.4pt">
  <td width="31" valign="top" style="width:20.85pt;border:solid black 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:187.4pt">
  <p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
  line-height:12.0pt;mso-element:frame;mso-element-frame-hspace:9.0pt;
  mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:
  column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">外匯業務</span><span lang="EN-US"><o:p></o:p></span></b></p>
  </td>
  <td width="121" valign="top" style="width:90.9pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:187.4pt">
  <p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
  line-height:12.0pt;mso-element:frame;mso-element-frame-hspace:9.0pt;
  mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:
  column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">022</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">外匯業務
  </span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
  line-height:12.0pt;mso-element:frame;mso-element-frame-hspace:9.0pt;
  mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:
  column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">036</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">存款與匯款業務</span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:16.45pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-16.45pt;line-height:12.0pt;mso-element:frame;
  mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:
  paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">082</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">借款戶與存款戶存借作業綜合管理</span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
  line-height:12.0pt;mso-element:frame;mso-element-frame-hspace:9.0pt;
  mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:
  column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">088</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">核貸與授信業務
  </span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
  line-height:12.0pt;mso-element:frame;mso-element-frame-hspace:9.0pt;
  mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:
  column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">106</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">授信業務</span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
  line-height:12.0pt;mso-element:frame;mso-element-frame-hspace:9.0pt;
  mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:
  column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">154</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">徵信</span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:18.2pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-18.45pt;line-height:12.0pt;mso-element:frame;
  mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:
  paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">181</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif">其他經營合於營業登記項目或組織章程所定之業務（例如：共同行銷或合作推廣業務等。）</span><span lang="EN-US"><o:p></o:p></span></b></p>
  </td>
 </tr>
</tbody></table>

<p class="MsoNormal" style="margin-left:21.0pt;text-indent:-21.0pt;line-height:
12.0pt"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">&nbsp;</span><span lang="EN-US"><o:p></o:p></span></b></p>

<p class="MsoNormal" style="margin-left:18.0pt;text-indent:-18.0pt;line-height:
12.0pt"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">&nbsp;</span><span lang="EN-US"><o:p></o:p></span></b></p>

<table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" align="left" width="0" style="width:551.15pt;border-collapse:collapse;mso-yfti-tbllook:1184;
 mso-table-lspace:9.0pt;margin-left:6.75pt;mso-table-rspace:9.0pt;margin-right:
 6.75pt;mso-table-anchor-vertical:paragraph;mso-table-anchor-horizontal:column;
 mso-table-left:left;mso-padding-alt:0cm 0cm 0cm 0cm">
 <tbody><tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes;
  height:233.45pt">
  <td width="31" valign="top" style="width:22.35pt;border:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:233.45pt">
  <p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;
  mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;color:black">信用卡<span lang="EN-US">/</span>金融卡業務</span><span lang="EN-US"><o:p></o:p></span></b></p>
  </td>
  <td width="119" valign="top" style="width:89.4pt;border:solid black 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt;height:233.45pt">
  <p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;
  mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif;color:black">022</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;
  color:black">外匯業務</span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:18.65pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-18.65pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif;
  color:black">036</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;color:black">存款與匯款業務</span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:19.55pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-19.55pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif;
  color:black">067</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;color:black">信用卡、轉帳卡或電子票證業務</span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:19.55pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-19.55pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif;
  color:black">082</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;color:black">借款戶與存款戶存借作業綜合管理 </span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:19.55pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-19.55pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif;
  color:black">088</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;color:black">核貸與授信業務 </span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;
  mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif;color:black">106</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;
  color:black">授信業務 </span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;
  mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif;color:black">154</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;
  color:black">徵信 </span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:18.2pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-18.45pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif;
  color:black">181</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;color:black">其他經營合於營業登記項目或組織章程所定之業務（例如：共同行銷或合作推廣業務等。）</span><span lang="EN-US"><o:p></o:p></span></b></p>
  </td>
  <td width="132" valign="top" style="width:99.2pt;border-top:solid black 1.0pt;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:233.45pt">
  <p class="MsoNormal" style="margin-left:14.2pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-14.2pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif;
  color:black">040</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;color:black">行銷 </span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:14.2pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-14.2pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif;
  color:black">059</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;color:black">金融服務業依法令規定及金融監理需要，所為之蒐集處理及利用</span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:14.2pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-14.2pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif;
  color:black">060</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;color:black">金融爭議處理</span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:14.2pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-14.2pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif;
  color:black">063</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;color:black">非公務機關依法定義務所進行個人資料之蒐集處理及利用 </span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:14.2pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-14.2pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif;
  color:black">069</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;color:black">契約、類似契約或其他法律關係管理之事務</span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:14.2pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-14.2pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif;
  color:black">090</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;color:black">消費者、客戶管理與服務 </span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:14.2pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-14.2pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif;
  color:black">091</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;color:black">消費者保護 </span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:14.2pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-14.2pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif;
  color:black">098</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;color:black">商業與技術資訊 </span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:14.2pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-14.2pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif;
  color:black">104</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;color:black">帳務管理及債權交易業務 </span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:14.2pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-14.2pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif;
  color:black">136</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;color:black">資<span lang="EN-US">(</span>通<span lang="EN-US">)</span>訊與資料庫管理 </span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:14.2pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-14.2pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif;
  color:black">137</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;color:black">資通安全與管理</span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:14.2pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-14.2pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif;
  color:black">157</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;color:black">調查、統計與研究分析 </span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:14.2pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-14.2pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif;
  color:black">182</span></b><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;color:black">其他諮詢與顧問服務</span><span lang="EN-US"><o:p></o:p></span></b></p>
  </td>
  <td width="113" valign="top" style="width:3.0cm;border:solid black 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt;height:233.45pt">
  <p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;
  mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;color:black">姓名、身分證統一編號、性別、出生年月日、通訊方式及其他詳如相關業務申請書或契約書之內容，並以本行與客戶往來之相關業務、帳戶或服務及自客戶或第三人處（例如：財團法人金融聯合徵信中心）所實際蒐集之個人資料為準。</span><span lang="EN-US"><o:p></o:p></span></b></p>
  </td>
  <td width="113" valign="top" style="width:3.0cm;border-top:solid black 1.0pt;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:233.45pt">
  <p class="MsoNormal" style="margin-left:10.6pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-10.6pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;
  color:black">ㄧ、特定目的存續期間。</span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:10.6pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-10.6pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;
  color:black">二、依相關法令所定（例如商業會計法等<span lang="EN-US">)</span>或因執行業務所必須之保存期間或依個別契約就資料之保存所定之保存年限。</span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:10.6pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-10.6pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;
  color:black">（以期限最長者為準）</span><span lang="EN-US"><o:p></o:p></span></b></p>
  </td>
  <td width="57" valign="top" style="width:42.55pt;border-top:solid black 1.0pt;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:233.45pt">
  <p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph;
  mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;
  mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;
  mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;color:black">右邊「個人資料利用之對象」欄位所列之利用對象其國內及國外所在地。</span><span lang="EN-US"><o:p></o:p></span></b></p>
  </td>
  <td width="113" valign="top" style="width:3.0cm;border-top:solid black 1.0pt;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:233.45pt">
  <p class="MsoNormal" style="margin-left:17.65pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-17.65pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;
  color:black">ㄧ、本行<span lang="EN-US">(</span>含受本行委託處理事務之委外機構<span lang="EN-US">)</span>。</span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:17.65pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-17.65pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;
  color:black">二、依法令規定利用之機構（例如：本行母公司或所屬金融控股公司等）。</span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:17.65pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-17.65pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;
  color:black">三、其他業務相關之機構（例如：通匯行、財團法人金融聯合徵信中心、財團法人聯合信用卡處理中心、台灣票據交換所、財金資訊股份有限公司、信用保證機構、信用卡國際組織、收單機構暨特約商店等）。</span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:17.65pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-17.65pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;
  color:black">四、依法有權機關或金融監理機關。</span><span lang="EN-US"><o:p></o:p></span></b></p>
  <p class="MsoNormal" style="margin-left:17.65pt;text-align:justify;text-justify:
  inter-ideograph;text-indent:-17.65pt;mso-element:frame;mso-element-frame-hspace:
  9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;
  mso-element-anchor-horizontal:column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;
  color:black">五、客戶所同意之對象（例如本行共同行銷或交互運用客戶資料之公司、與本行合作推廣業務之公司等）。</span><span lang="EN-US"><o:p></o:p></span></b></p>
  </td>
  <td width="57" valign="top" style="width:42.5pt;border-top:solid black 1.0pt;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:233.45pt">
  <p class="MsoNormal" style="mso-element:frame;mso-element-frame-hspace:9.0pt;
  mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:
  column;mso-height-rule:exactly"><b style="mso-bidi-font-weight:normal"><span style="font-family:&quot;新細明體&quot;,serif;color:black">符合個人資料保護相關法令以自動化機器或其他非自動化之利用方式。</span><span lang="EN-US"><o:p></o:p></span></b></p>
  </td>
 </tr>
</tbody></table>

<p class="MsoNormal"><span lang="EN-US" style="font-family:&quot;新細明體&quot;,serif">&nbsp;</span></p>

</div>

</body>

</html>