<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
	
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>	
</head>
 <body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>

	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	
	<!--   IDGATE -->
	<%@ include file="../idgate_tran/idgateAlert.jsp" %>
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 黃金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2272" /></li>
    <!-- 繳納定期扣款失敗手續費     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1533" /></li>
		</ol>
	</nav>

 	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
	<main class="col-12"> 
	<!-- 		主頁內容  -->
		<section id="main-content" class="container">
<!-- 繳納定期扣款失敗手續費 -->
			<h2><spring:message code= "LB.W1533" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>						
<!-- 繳納手續費成功 -->
<%-- 			<p style="text-align: center;color: red;"><spring:message code= "LB.W1545" /></p> --%>
			<form method="post" id="formId">
				<input type="hidden" id="TOKEN" name="TOKEN" value="${sessionScope.transfer_confirm_token}" /><!-- 防止重複交易 -->
                
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
                            <div class="ttb-message">
								<!-- 請確認取消資料 -->						
                                <span><spring:message code= "LB.W1545" /></span>
                            </div>
						
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
<!-- 交易時間 -->
											<spring:message code= "LB.D0019" />
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>${ result_data.data.CMTXTIME }</p>
									</div>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
<!-- 黃金存摺帳號 -->
											<spring:message code= "LB.D1090" />
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>${ result_data.data.ACN }</p>
									</div>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
<!-- 扣款失敗手續費 -->
<!-- 繳款金額 -->
											<spring:message code= "LB.W1542" /><br />(<spring:message code= "LB.D0021" />)
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
<!-- 元 -->
										<p>${ result_data.data.TRNAMT }<spring:message code= "LB.Dollar" /></p>
									</div>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
<!-- 轉出帳號 -->
											<spring:message code= "LB.D0465" />
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>${ result_data.data.SVACN }</p>
									</div>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
<!-- 轉出帳號帳戶餘額 -->
											<spring:message code= "LB.W1550" />
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
<!-- 新台幣 -->
<!-- 元 -->
										<p><spring:message code= "LB.NTD" />${ result_data.data.O_TOTBAL }<spring:message code= "LB.Dollar" /></p>
									</div>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
<!-- 轉出帳號可用餘額 -->
											<spring:message code= "LB.W0283" />
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
<!-- 新台幣 -->
<!-- 元 -->
										<p><spring:message code= "LB.NTD" />${ result_data.data.O_AVLBAL }<spring:message code= "LB.Dollar" /></p>
									</div>
								</span>
							</div>

							
						</div>
                        <spring:message code="LB.Print" var="printbtn"></spring:message>
                        <input type="button" id="printbtn" value="${printbtn}" class="ttb-button btn-flat-orange" />							
					</div>
				</div>
				</form>
			<div class="text-left">
			    <!-- 		說明： -->
				
			    <ol class="list-decimal description-list">
			   		<p><spring:message code="LB.Description_of_page"/></p>
			    <!-- 電子簽章:為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章(載具i-key)拔除並登出系統。 -->
			    	<li><spring:message code="LB.Pay_Fail_Fee_P3_D1"/></li>
			    </ol>
			</div>
		</section>
		<!-- 		main-content END -->
	</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
	<!--   Js function -->
    <script type="text/JavaScript">
		$(document).ready(function() {
			init();
		});
		
	    function init(){
	    	//列印
        	$("#printbtn").click(function(){
				var params = {
					"jspTemplateName":"pay_fail_fee_result_print",
// 繳納定期扣款失敗手續費
					"jspTitle":'<spring:message code= "LB.W1533" />',
					"CMTXTIME":"${result_data.data.CMTXTIME}",
					"ACN":"${result_data.data.ACN}",
					"TRNAMT":"${result_data.data.TRNAMT}",
					"SVACN":"${result_data.data.SVACN}",
					"O_TOTBAL":"${result_data.data.O_TOTBAL}",
					"O_AVLBAL":"${result_data.data.O_AVLBAL}"
				};
				openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
	    }	
 	</script>
</body>
</html>
 