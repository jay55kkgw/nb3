<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!-- 使用者基本資料 -->
<section id="update-content" class="modal fade active show more-info-block" role="dialog" aria-labelledby="" aria-hidden="true" tabindex="-1">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<!-- 客戶資料更新 -->
			<div class="modal-header">
				<p class="ttb-pup-header"><spring:message code='LB.X2187'/></p>
			</div>
			<div id="update-content-1">
				<form method="post" id="formId" action="#">
					<div class="modal-body w-auto m-0 pl-5">
						<p><spring:message code='LB.X0219'/>
							<br />
							<spring:message code='LB.X2188'/>
							<span class="high-light">
								<spring:message code='LB.X2189'/>
							</span>
							<spring:message code='LB.X2190'/>
							<span class="high-light">
							<spring:message code='LB.X2191'/>
							</span>
							<spring:message code='LB.X2192'/>
						</p>
						
						<div class="ttb-input-block">
							<input type="hidden" id="PARAM1" name="PARAM1" value="">
							<input type="hidden" id="PARAM2" name="PARAM2" value="NNB">
							<input type="hidden" id="PARAM3" name="PARAM3" value="${CONPNAM}">
							<input type="hidden" id="PARAM4" name="PARAM4" value="${CAREER1}">
							<input type="hidden" id="PARAM5" name="PARAM5" value="${CAREER2}">
							<input type="hidden" id="PINNEW" name="PINNEW" value="">
							<input type="hidden" id="FUN_TYPE" name="FUN_TYPE" value="U"><!-- 修改 -->
						
						<c:if test="${empty CAREER1}">
							<!-- 職業 -->
							<div id="CAREER1_DIV" class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code='LB.D1132'/></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<input type="text" class="text-input" name="CAREER1_TEXT" id="CAREER1_TEXT" value="" 
												placeholder="<spring:message code='LB.X1091'/>" readonly>
										<input type="hidden" class="text-input" name="CAREER1" id="CAREER1" value="" >
										<button type="button" class="btn-flat-gray" onclick="openNameList()"><spring:message code='LB.X2194'/></button>
									</div>
								</span>
							</div>
						</c:if>
							
						<c:if test="${empty CAREER2}">	
							<!-- 職稱代號 -->
							<div id="CAREER2_DIV" class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code='LB.D1133'/></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<input type="text" class="text-input" name="CAREER2_TEXT" id="CAREER2_TEXT" value=""
												placeholder="<spring:message code='LB.X2195'/>" readonly>
										<input type="hidden" class="text-input" name="CAREER2" id="CAREER2" value="" >
										<button type="button" class="btn-flat-gray" onclick="openCodeList()"><spring:message code='LB.X2194'/></button>
									</div>
								</span>
							</div>
						</c:if>
						
						<c:if test="${empty CONPNAM}">
							<!--任職機構 -->
							<div id="CONPNAM_DIV" class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code='LB.D1074'/></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<input type="text" class="text-input" name="COMPANYNAME" id="COMPANYNAME"
												placeholder="<spring:message code='LB.X2196'/>" maxlength="33" size="33" />
									</div>
								</span>
							</div>
						</c:if>
	
							<!-- 交易密碼 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code='LB.SSL_password_1'/></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<input id="CMPASSWORD" name="CMPASSWORD" type="password" class="text-input validate[required]"
											size="8" maxlength="8" placeholder="<spring:message code='LB.Please_enter_password'/>" >
									</div>
								</span>
							</div>
						
							<!-- 驗證碼 -->
							<div class="ttb-input-item row" id="chaBlock">
			                   	<span class="input-title">
			                   		<label>
										<h4><spring:message code="LB.D0032"/></h4>
			                          </label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<input id="capCode" type="text" class="text-input input-width-125"
											name="capCode" placeholder="<spring:message code='LB.X1702'/>" maxlength="6" autocomplete="off">
										<img name="kaptchaImage" class="verification-img"/>
										<input type="button" name="reshow" class="ttb-sm-btn btn-flat-gray validate[required]" 
												onclick="changeCode()" value="<spring:message code='LB.Regeneration'/>" />
										<span class="input-remarks"><spring:message code="LB.X1972"/></span>
									</div>
								</span>
		                   </div>
			                   
						</div>
			
					</div>
				</form>
				
				<div class="modal-footer ttb-pup-footer">
					<input type="button" class="ttb-pup-btn btn-flat-gray" data-dismiss="modal" value="<spring:message code='LB.X2197'/>" />
					<input type="button" class="ttb-pup-btn btn-flat-orange" value="<spring:message code='LB.X2198'/>" onclick="updateA106()" />
				</div>
			</div>
			
			<div id="update-content-2" style="display: none;">
				<div class="modal-body w-auto m-0 pl-5">
					<!--請選擇符合您的職業名稱 -->
					<p><spring:message code='LB.X2203'/></p>
					<ul class="ttb-pup-item d-flex">
						<li>
							<label class="radio-block"><spring:message code='LB.D0871'/>
								<input type="radio" name="CAREER1_radio" id="061100" value="<spring:message code='LB.D0871'/>" >
								<span class="ttb-radio"></span>
							</label>
						</li>
						<li>
							<label class="radio-block"><spring:message code='LB.D0872'/>
								<input type="radio" name="CAREER1_radio" id="061200" value="<spring:message code='LB.D0872'/>" >
								<span class="ttb-radio"></span>
							</label>
						</li>
						<li>
							<label class="radio-block"><spring:message code='LB.D0873'/>
								<input type="radio" name="CAREER1_radio" id="061300" value="<spring:message code='LB.D0873'/>" >
								<span class="ttb-radio"></span>
							</label>
						</li>
						<li>
							<label class="radio-block"><spring:message code='LB.D0874'/>
								<input type="radio" name="CAREER1_radio" id="061400" value="<spring:message code='LB.D0874'/>" >
								<span class="ttb-radio"></span>
							</label>
						</li>
						<li>
							<label class="radio-block"><spring:message code='LB.D0081'/>
								<input type="radio" name="CAREER1_radio" id="061410" value="<spring:message code='LB.D0081'/>" >
								<span class="ttb-radio"></span>
							</label>
						</li>
						<li>
							<label class="radio-block"><spring:message code='LB.D0876'/>
								<input type="radio" name="CAREER1_radio" id="061500" value="<spring:message code='LB.D0876'/>" >
								<span class="ttb-radio"></span>
							</label>
						</li>
						<li>
							<label class="radio-block"><spring:message code='LB.D0877'/>
								<input type="radio" name="CAREER1_radio" id="0615A0" value="<spring:message code='LB.D0877'/>" >
								<span class="ttb-radio"></span>
							</label>
						</li>
						<li>
							<label class="radio-block"><spring:message code='LB.D0878'/>
								<input type="radio" name="CAREER1_radio" id="0615B0" value="<spring:message code='LB.D0878'/>" >
								<span class="ttb-radio"></span>
							</label>
						</li>
						<li>
							<label class="radio-block"><spring:message code='LB.D0879'/>
								<input type="radio" name="CAREER1_radio" id="0615C0" value="<spring:message code='LB.D0879'/>" >
								<span class="ttb-radio"></span>
							</label>
						</li>
						<li>
							<label class="radio-block"><spring:message code='LB.D0880'/>
								<input type="radio" name="CAREER1_radio" id="0615D0" value="<spring:message code='LB.D0880'/>" >
								<span class="ttb-radio"></span>
							</label>
						</li>
						<li>
							<label class="radio-block"><spring:message code='LB.D0881'/>
								<input type="radio" name="CAREER1_radio" id="0615E0" value="<spring:message code='LB.D0881'/>" >
								<span class="ttb-radio"></span>
							</label>
						</li>
						<li>
							<label class="radio-block"><spring:message code='LB.D0882'/>
								<input type="radio" name="CAREER1_radio" id="0615F0" value="<spring:message code='LB.D0882'/>" >
								<span class="ttb-radio"></span>
							</label>
						</li>
						<li>
							<label class="radio-block"><spring:message code='LB.D0883'/>
								<input type="radio" name="CAREER1_radio" id="0615G0" value="<spring:message code='LB.D0883'/>" >
								<span class="ttb-radio"></span>
							</label>
						</li>
						<li>
							<label class="radio-block"><spring:message code='LB.D0884'/>
								<input type="radio" name="CAREER1_radio" id="0615H0" value="<spring:message code='LB.D0884'/>" >
								<span class="ttb-radio"></span>
							</label>
						</li>
						<li>
							<label class="radio-block"><spring:message code='LB.D0885'/>
								<input type="radio" name="CAREER1_radio" id="0615I0" value="<spring:message code='LB.D0885'/>" >
								<span class="ttb-radio"></span>
							</label>
						</li>
						<li>
							<label class="radio-block"><spring:message code='LB.D0886'/>
								<input type="radio" name="CAREER1_radio" id="0615J0" value="<spring:message code='LB.D0886'/>" >
								<span class="ttb-radio"></span>
							</label>
						</li>
						<li>
							<label class="radio-block"><spring:message code='LB.D0887'/>
								<input type="radio" name="CAREER1_radio" id="061610" value="<spring:message code='LB.D0887'/>" >
								<span class="ttb-radio"></span>
							</label>
						</li>
						<li>
							<label class="radio-block"><spring:message code='LB.D0888'/>
								<input type="radio" name="CAREER1_radio" id="061620" value="<spring:message code='LB.D0888'/>" >
								<span class="ttb-radio"></span>
							</label>
						</li>
						<li>
							<label class="radio-block"><spring:message code='LB.X1319'/>
								<input type="radio" name="CAREER1_radio" id="061630" value="<spring:message code='LB.X1319'/>" >
								<span class="ttb-radio"></span>
							</label>
						</li>
						<li>
							<label class="radio-block"><spring:message code='LB.X1320'/>
								<input type="radio" name="CAREER1_radio" id="061640" value="<spring:message code='LB.X1320'/>" >
								<span class="ttb-radio"></span>
							</label>
						</li>
						<li>
							<label class="radio-block"><spring:message code='LB.X1321'/>
								<input type="radio" name="CAREER1_radio" id="061650" value="<spring:message code='LB.X1321'/>" >
								<span class="ttb-radio"></span>
							</label>
						</li>
						<li>
							<label class="radio-block"><spring:message code='LB.D0892'/>
								<input type="radio" name="CAREER1_radio" id="061660" value="<spring:message code='LB.D0892'/>" >
								<span class="ttb-radio"></span>
							</label>
						</li>
						<li>
							<label class="radio-block"><spring:message code='LB.D0893'/>
								<input type="radio" name="CAREER1_radio" id="061670" value="<spring:message code='LB.D0893'/>" >
								<span class="ttb-radio"></span>
							</label>
						</li>
						<li>
							<label class="radio-block"><spring:message code='LB.X1322'/>
								<input type="radio" name="CAREER1_radio" id="061680" value="<spring:message code='LB.X1322'/>" >
								<span class="ttb-radio"></span>
							</label>
						</li>
						<li>
							<label class="radio-block"><spring:message code='LB.D0082'/>
								<input type="radio" name="CAREER1_radio" id="061690" value="<spring:message code='LB.D0082'/>" >
								<span class="ttb-radio"></span>
							</label>
						</li>
						<li>
							<label class="radio-block"><spring:message code='LB.D0083'/>
								<input type="radio" name="CAREER1_radio" id="061691" value="<spring:message code='LB.D0083'/>" >
								<span class="ttb-radio"></span>
							</label>
						</li>
						<li>
							<label class="radio-block"><spring:message code='LB.X0571'/>
								<input type="radio" name="CAREER1_radio" id="061692" value="<spring:message code='LB.X0571'/>" >
								<span class="ttb-radio"></span>
							</label>
						</li>
						<li>
							<label class="radio-block"><spring:message code='LB.X1323'/>
								<input type="radio" name="CAREER1_radio" id="061700" value="<spring:message code='LB.X1323'/>" >
								<span class="ttb-radio"></span>
							</label>
						</li>
						<li>
							<label class="radio-block"><spring:message code='LB.D0899'/>
								<input type="radio" name="CAREER1_radio" id="069999" value="<spring:message code='LB.D0899'/>" >
								<span class="ttb-radio"></span>
							</label>
						</li>
					</ul>
				</div>
				
				<div class="modal-footer ttb-pup-footer">
					<input type="button" class="ttb-pup-btn btn-flat-gray" onclick="closeNameList()" value="<spring:message code='LB.D0171'/>" />
					<input type="button" class="ttb-pup-btn btn-flat-orange" onclick="chooseName()" value="<spring:message code='LB.D0374'/>" />
				</div>
			</div>
			
			<div id="update-content-3" style="display: none;">
				<div class="modal-body w-auto m-0 pl-5">
					<p><spring:message code='LB.X2193'/></p>
					
					<div class="ttb-input-block">
					
						<div class="ttb-input-item row">
							<span class="input-title">
								<label class="radio-block" for="1"><spring:message code='LB.D0973'/>1
									<input type="radio" name="CAREER2_radio" id="1" 
										value="<spring:message code='LB.X0179'/>" />
									<span class="ttb-radio"></span>
								</label>
							</span>
							<span class="input-block">
								<div class="ttb-input">
									<spring:message code='LB.X0179'/>
								</div>
							</span>
						</div>
						
						<div class="ttb-input-item row">
							<span class="input-title">
								<label class="radio-block" for="2"><spring:message code='LB.D0973'/>2
									<input type="radio" name="CAREER2_radio" id="2" 
										value="<spring:message code='LB.X0180'/>" />
									<span class="ttb-radio"></span>
								</label>
							</span>
							<span class="input-block">
								<div class="ttb-input">
									<spring:message code='LB.X0180'/>
								</div>
							</span>
						</div>
						
						<div class="ttb-input-item row">
							<span class="input-title">
								<label class="radio-block" for="3"><spring:message code='LB.D0973'/>3
									<input type="radio" name="CAREER2_radio" id="3" 
										value="<spring:message code='LB.X0181'/>" />
									<span class="ttb-radio"></span>
								</label>
							</span>
							<span class="input-block">
								<div class="ttb-input">
									<spring:message code='LB.X0181'/>
								</div>
							</span>
						</div>
						
						<div class="ttb-input-item row">
							<span class="input-title">
								<label class="radio-block" for="4"><spring:message code='LB.D0973'/>4
									<input type="radio" name="CAREER2_radio" id="4" 
										value="<spring:message code='LB.X0182'/>" />
									<span class="ttb-radio"></span>
								</label>
							</span>
							<span class="input-block">
								<div class="ttb-input">
									<spring:message code='LB.X0182'/>
								</div>
							</span>
						</div>
						
						<div class="ttb-input-item row">
							<span class="input-title">
								<label class="radio-block" for="5"><spring:message code='LB.D0973'/>5
									<input type="radio" name="CAREER2_radio" id="5" 
										value="<spring:message code='LB.X0183'/>" />
									<span class="ttb-radio"></span>
								</label>
							</span>
							<span class="input-block">
								<div class="ttb-input">
									<spring:message code='LB.X0183'/>
								</div>
							</span>
						</div>
						
						<div class="ttb-input-item row">
							<span class="input-title">
								<label class="radio-block" for="6"><spring:message code='LB.D0973'/>6
									<input type="radio" name="CAREER2_radio" id="6" 
										value="<spring:message code='LB.X0184'/>" />
									<span class="ttb-radio"></span>
								</label>
							</span>
							<span class="input-block">
								<div class="ttb-input">
									<spring:message code='LB.X0184'/>
								</div>
							</span>
						</div>
						
						<div class="ttb-input-item row">
							<span class="input-title">
								<label class="radio-block" for="7"><spring:message code='LB.D0973'/>7
									<input type="radio" name="CAREER2_radio" id="7" 
										value="<spring:message code='LB.X0185'/>" />
									<span class="ttb-radio"></span>
								</label>
							</span>
							<span class="input-block">
								<div class="ttb-input">
									<spring:message code='LB.X0185'/>
								</div>
							</span>
						</div>
						
						<div class="ttb-input-item row">
							<span class="input-title">
								<label class="radio-block" for="8"><spring:message code='LB.D0973'/>8
									<input type="radio" name="CAREER2_radio" id="8" 
										value="<spring:message code='LB.X0186'/>" />
									<span class="ttb-radio"></span>
								</label>
							</span>
							<span class="input-block">
								<div class="ttb-input">
									<spring:message code='LB.X0186'/>
								</div>
							</span>
						</div>
						
						<div class="ttb-input-item row">
							<span class="input-title">
								<label class="radio-block" for="9"><spring:message code='LB.D0973'/>9
									<input type="radio" name="CAREER2_radio" id="9" 
										value="<spring:message code='LB.X0187'/>" />
									<span class="ttb-radio"></span>
								</label>
							</span>
							<span class="input-block">
								<div class="ttb-input">
									<spring:message code='LB.X0187'/>
								</div>
							</span>
						</div>
					</div>
				</div>
				
				<div class="modal-footer ttb-pup-footer">
					<input type="button" class="ttb-pup-btn btn-flat-gray" onclick="closeCodeList()" value="<spring:message code='LB.D0171'/>" />
					<input type="button" class="ttb-pup-btn btn-flat-orange" onclick="chooseCode()" value="<spring:message code='LB.D0374'/>" />
				</div>
			</div>

		</div>
	</div>
</section>			
			
	
<script type="text/javascript">
	// 呈現使用者基本資料畫面
	$('#update-content').modal('show');
	// 初始化驗證碼
	initKapImg();
	// 生成驗證碼
	newKapImg();
	
	// 交易機制使用讀卡機
	function checkCap(){
		console.log("checkCap...");
		// 驗證碼驗證
		var capData = $("#formId").serializeArray();
		var capResult = fstop.getServerDataEx('${__ctx}' + "/CAPCODE/captcha_valided_trans", capData, false);
		console.log("chaCode_valid: " + JSON.stringify(capResult) );
		
		// 驗證結果
		if (capResult.result) {
			// 開始讀卡機驗證流程
			updateA106_aj();
			
		} else {
			// 失敗重新產生驗證碼
			alert("驗證碼有誤");
			$("#CMPASSWORD").val('');
			changeCode();
		}
	}
	
	// 表單驗證後送交
	function updateA106() {
		// 表單驗證
		$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
		
		if ( !$('#formId').validationEngine('validate') ) {
			e = e || window.event; // for IE
			e.preventDefault();
			
		} else {
			// 資料檢核，過了則解除驗證送出表單
			$("#formId").validationEngine('detach');
			checkCap();
		}
	}
	
	
	// 送交
	function updateA106_aj() {
		$('#PINNEW').val( pin_encrypt( $('#CMPASSWORD').val() ) );
		
		uri = '${__ctx}' + "/INDEX/updateA106_aj";

		var rdata = {
			PARAM1 : $("#PARAM1").val(),
			PARAM2 : $("#PARAM2").val(),
			PARAM3 : $("#PARAM3").val(),
			PARAM4 : $("#PARAM4").val(),
			PARAM5 : $("#PARAM5").val(),
			PINNEW : $("#PINNEW").val(),
			FUN_TYPE : $("#FUN_TYPE").val(),
			CAREER1 : $("#CAREER1").val(),
			CAREER2 : $("#CAREER2").val(),
			COMPANYNAME : $("#COMPANYNAME").val()
		};

		console.log("updateA106_aj.uri: " + uri);
		console.log("updateA106_aj.rdata: " + rdata);

		// 遮罩
		initBlockUI();
		
		data = fstop.getServerDataEx(uri, rdata, false, updateA106_aj_Callback);
		
	}
	
	// 轉出帳號onChange後打Ajax的callback
	function updateA106_aj_Callback(data) {
		// 解遮罩
		unBlockUI(initBlockId)
		
		console.log("data: " + data);
		if (data) {
			console.log("updateA106_aj_Callback.data.json: " + JSON.stringify(data));
			
			if(data.msgCode == '0'){
				errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.D0182' />"], 
					'<spring:message code= "LB.Confirm" />', 
					null
				);
				
			} else {
				errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Transaction_code' />: " + data.msgCode , "\n" , "<spring:message code='LB.Transaction_message' />: " + data.message], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
			}
		}
		
		// 關閉A106視窗
		$('#update-content').modal('hide');
		
	}
	
	// 顯示職業名稱
	function openNameList() {
		document.getElementById("update-content-1").style.display = "none";
		document.getElementById("update-content-2").style.display = "block";
	}
	
	// 選擇職業名稱
	function chooseName() {
		$("#CAREER1_TEXT").val($("input[name=CAREER1_radio]:checked").val());
		$("#CAREER1").val($("input[name=CAREER1_radio]:checked").attr("id"));
		closeNameList();
	}
	
	// 取消選擇職業名稱
	function closeNameList() {
		document.getElementById("update-content-1").style.display = "block";
		document.getElementById("update-content-2").style.display = "none";
	}
	
	// 顯示職稱代號
	function openCodeList() {
		document.getElementById("update-content-1").style.display = "none";
		document.getElementById("update-content-3").style.display = "block";
	}
	
	// 選擇職稱代號
	function chooseCode() {
		$("#CAREER2_TEXT").val($("input[name=CAREER2_radio]:checked").val());
		$("#CAREER2").val($("input[name=CAREER2_radio]:checked").attr("id"));
		closeCodeList();
	}
	
	// 取消選擇職稱代號
	function closeCodeList() {
		document.getElementById("update-content-1").style.display = "block";
		document.getElementById("update-content-3").style.display = "none";
	}
	
	// 驗證碼刷新
	function changeCode() {
		$('input[name="capCode"]').val('');
		// 大小版驗證碼用同一個
		console.log("changeCode...");
		$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

		// 登入失敗解遮罩
		unBlockUI(initBlockId);
	}
	
	// 初始化驗證碼
	function initKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
	}
	
	// 生成驗證碼
	function newKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').click(function() {
			$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
		});
	}
</script>	
