<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp"%>
	<link rel="stylesheet" type="text/css"
		href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript"
		src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript"
		src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript"
		src="${__ctx}/js/jquery.datetimepicker.js"></script>
		<script type="text/JavaScript">
			$(document).ready(function () {
				tabEvent();
				$("#cmback").click(function() {
					var action = '${__ctx}/nb3/login';
					$("form").attr("action", action);
					$("form").submit();
				});
				$("#CMSUBMIT").click( function(e) {
					var TYPE = $("input[name='TYPE']:checked").val();
						switch (TYPE){
						case "3":
							$('#CN19-now-apply').hide();
							$('#MESSAGE').hide();
							$('#TYPE3REQ').show();
							$('#SUBSUBMIT').show();
							break;
						case "4":
							$('#CN19-now-apply').hide();
							$('#MESSAGE').hide();
							$('#TYPE4REQ').show();
							$('#SUBSUBMIT').show();
							break;
						case "8":
							$('#CN19-now-apply').hide();
							$('#MESSAGE').hide();
							$('#TYPE8REQ').show();
							$('#SUBSUBMIT').show();
							break;
						case "9":
							$('#CN19-now-apply').hide();
							$('#MESSAGE').hide();
							$('#TYPE9REQ').show();
							$('#SUBSUBMIT').show();
							break;
						case "18":
							window.open('https://nnb.tbb.com.tw/TBBNBAppsWeb/eeloanweb/NA05.jsp', '_blank');
							break;
						default:
							$('#formId').submit();
						}
				
				});
				
				$("#CMCANCEL").click( function(e) {
					$("#CN19-now-apply, #CN19-description, #CN19-button").show();
					$("#CN19-how-apply").hide();
					$('#TYPE3REQ').hide();
					$('#TYPE4REQ').hide();
					$('#TYPE8REQ').hide();
					$('#TYPE9REQ').hide();
					$('#SUBSUBMIT').hide();
				})
				
				$("#CMSUBMIT2").click( function(e) {
					var TYPE = $("input[name='TYPE']:checked").val();
					switch (TYPE){
					case "3":
						$('#formId').attr("action","${__ctx}/login?ADOPID=G210");
						break;
					case "4":
						$('#formId').attr("action","${__ctx}/login?ADOPID=G200");
						break;
					case "8":
						$('#formId').attr("action","${__ctx}/login?ADOPID=N814");
						break;
					case "9":
						$('#formId').attr("action","${__ctx}/login?ADOPID=CK01");
						break;
					}
					$('#formId').submit();
				});
				
				$("#CMSUBMIT2").click( function(e) {
					$('#formId').attr("action","${__ctx}/ONLINE/APPLY/online_apply");
				});
			});
			// 立刻、如何申请标签切换事件
			function tabEvent() {
				// 立刻申请
				$("#now-apply").click(function() {
					$("#CN19-now-apply, #CN19-description, #CN19-button").show();
					$("#CN19-how-apply").hide();
					$('#TYPE3REQ').hide();
					$('#TYPE4REQ').hide();
					$('#TYPE8REQ').hide();
					$('#TYPE9REQ').hide();
					$('#SUBSUBMIT').hide();
				})
				// 如何申请
				$("#how-apply").click(function() {
					$("#CN19-how-apply").show();
					$('#applyTable').DataTable({
						"bPaginate" : false,
						"bLengthChange": false,
						"scrollX": true,
						"sScrollX": "99%",
						"scrollY": "80vh",
				        "bFilter": false,
				        "bDestroy": true,
				        "bSort": false,
				        "info": false,
				        "scrollCollapse": true,
				    });
					$("#CN19-now-apply, #CN19-description, #CN19-button").hide();
					$('#TYPE3REQ').hide();
					$('#TYPE4REQ').hide();
					$('#TYPE8REQ').hide();
					$('#TYPE9REQ').hide();
					$('#SUBSUBMIT').hide();
				})
			}
			
			
			$(window).bind('resize', function (){
				$('#applyTable').DataTable({
					"bPaginate" : false,
					"bLengthChange": false,
					"scrollX": true,
					"sScrollX": "99%",
					"scrollY": "80vh",
			        "bFilter": false,
			        "bDestroy": true,
			        "bSort": false,
			        "info": false,
			        "scrollCollapse": true,
			    });
			});
			
			function goPage(url){
				window.open(url, '_blank');
			}

		</script>
	</head>
	<body >
		<header>
			<%@ include file="../index/header_logout.jsp"%>
		</header>
		<!-- 功能清单及登入资讯 -->
		<div class="content row">
			<!-- 快速选单及主页内容 -->
			<main class="col-12">
				<!-- 主页内容  -->
				<section id="main-content" class="container" >
				<form id="formId" method="post"
					action="${__ctx}/ONLINE/APPLY/online_apply" target="_blank">
					<input type="hidden" id="FROM_NB3" name="FROM_NB3" value="Y">
					<!-- 功能名称 -->
					<h2>线上申请</h2>
					<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
					<!-- 功能内容 -->
					<div class="main-content-block row radius-50">
						<!-- 即时、预约导引标签 -->
						<nav style="width: 100%;">
							<div class="nav nav-tabs" id="nav-tab" role="tablist">
								<a class="nav-item nav-link active" id="now-apply"
									data-toggle="tab" href="#now-apply" role="tab"
									aria-controls="nav-home" aria-selected="false">立即申请
								</a>
								<a class="nav-item nav-link" id="how-apply"
									data-toggle="tab" href="#how-apply" role="tab"
									aria-controls="nav-profile" aria-selected="true">如何申请
								</a>
<!-- 								<a class="triple-nav-item nav-item nav-link" id="return-login" -->
<%-- 									href="#" onclick="fstop.getPage('${pageContext.request.contextPath}'+'/login','', '')">回登入页 --%>
<!-- 								</a> -->
							</div>
						</nav>
<!-- 						<div class="col-12"> -->
<!-- 							<div class="CN19-1-header"> -->
<!-- 								<div class="logo"> -->
<%-- 									<img src="${__ctx}/img/logo-1.svg"> --%>
<!-- 								</div> -->
<!-- 								常用网址超链接 -->
<!-- 								<div class="text-right hyperlink"> -->
<!-- 									<a href="#" onClick="window.open('https://www.tbb.com.tw');" style="text-decoration: none"> 台湾企银首页 </a> <strong><font color="#e65827">|</font></strong>  -->
<!-- 									<a href="#" onClick="window.open('https://eatm.tbb.com.tw/');" style="text-decoration: none"> 网路ATM </a><strong><font color="#e65827">|</font></strong> 	 -->
<!-- 									<a href="#" onClick="window.open('https://www.tbb.com.tw/q3.html?');" style="text-decoration: none"> 意见信箱 </a> -->
<!-- 								</div> -->
<!-- 							</div> -->
<!-- 						</div> -->
						<!-- 如何申请 页面 -->
						<div class="col-12 tab-content" id="CN19-how-apply" style="display:none;">
							<div class="ttb-input-block">
								<div class="CN19-caption1">
									壹、网路银行申请方式及服务功能
								</div>
								<div class="CN19-caption2">
									<span style="FONT-FAMILY: Wingdings;">u</span>
									<span>申请方式</span>
								</div>
								<div class="CN19-caption3">
									<ul>
										<li>线上申请：限个人户</li>
									</ul>
									<div>
										本行芯片金融卡存户：<br>
 										未曾临柜申请之存户，可线上申请<font color="#FF6600">查询</font>服务，请使用<b>本行芯片金融卡 + 芯片读卡机</b> ，直接线上申请网路银行，如该芯片金融卡已具备非约定转帐功能者，得线上申请「金融卡线上申请/取消交易服务功能」，执行新台币非约定转帐交易、缴费稅交易、线上更改通讯地址/电话及使用者名称/簽入密码/交易密码线上解锁等功能。<br>
										<br>
										本行信用卡正卡客户（不含商务卡、采购卡、附卡及VISA金融卡）：<br>
 										请您直接线上申请网路银行，只要输入个人验证资料，即可立刻使用网路银行信用卡相关服务。<br>
 										<br>
									</div>
									<ul>
										<li>临柜申请：</li>
									</ul>
									<div>
										法人存户：<br>
 										请负责人携带<b>公司登记证件、身分证、存款印鉴及存摺</b>至开户行申请网路银行。<br>
										<br>
										个人存户：<br>
 										请本人携带<b>身分证、存款印鉴及存摺</b>至开户行申请网路银行，全行收付户（即通储户），亦可至各营业单位申请。<br>
 										<br>
									</div>
								</div>
								<div class="CN19-caption2">
									<span style="FONT-FAMILY: Wingdings;">u</span>
									<span>服务功能</span>
								</div>
								<div>
									<table id="applyTable" class="stripe table-striped ttb-table dtable" data-toggle-column="first">
										<thead>				
											<tr>
												<!-- 功能比较-->
												<th>功能比较</th>
												<!-- 临柜申请-->
	 											<th>临柜申请</th>
												<!-- 芯片金融卡线上申请-->
												<th>芯片金融卡线上申请</th>
												<!-- 芯片金融卡线上申请且线上申请交易服务功能 -->
												<th>芯片金融卡线上申请且线上申请交易服务功能</th>
												<!-- 信用卡线上申请 -->
												<th>信用卡线上申请</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>
													信用卡帐务查询
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
											</tr>
											<tr>
												<td>
													信用卡电子帐单
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
											</tr>
											<tr>
												<td>
													补寄信用卡帐单
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
											</tr>
											<tr>
												<td>
													信用卡缴款Email通知
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
											</tr>
											<tr>
												<td>
													存、放款、外汇帐务查询
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													基金查询
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													黄金存折查询
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													黄金申购/回售及变更
												</td>
												<td>O(注1)</td>
												<td>X</td>
												<td>X</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													转帐、缴费稅、定存
												</td>
												<td>O</td>
												<td>X</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													新台币非约定转帐(注2)
												</td>
												<td>O</td>
												<td>X</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													基金申购及变更
												</td>
												<td>O</td>
												<td>X</td>
												<td>X</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													申请代扣缴费用
												</td>
												<td>O</td>
												<td>X</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													掛失服务
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													变更通讯资料(注3)
												</td>
												<td>O</td>
												<td>X</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													使用者名称/簽入/交易密码线上解锁(注4)
												</td>
												<td>O</td>
												<td>X</td>
												<td>O</td>
												<td>X</td>
											</tr>
											<tr>
												<td>
													理财试算
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
											</tr>
											<tr>
												<td>
													国内台币汇入汇款通知设定
												</td>
												<td>O</td>
												<td>O</td>
												<td>O</td>
												<td>X</td>
											</tr>
										</tbody>
									</table>
									</div>
									<div>
										<b>备注：</b><br>
										<ol>
											<li>临柜申请黄金存折帐户客户，需於临柜或迳自本行网路银行申请黄金存折网路交易功能，始得执行黄金申购/回售及变更等功能。</li>
											<li>倘已线上申请交易功能者，且该芯片金融卡已具备非约定转帐功能者，并得以本人帐户之芯片金融卡之主帐号为转出帐户，执行新台币非约定转帐交易、缴费稅交易。</li>
											<li>交易机制为「电子簽章」或「芯片金融卡」，即可执行线上更改通讯地址/电话。</li>
											<li>易机制为「电子簽章」或「芯片金融卡」，即可执行线上簽入/交易密码线上解锁。</li>
											<li>O：表示可使用功能，X：表示不可使用功能。(原注2)</li>
										</ol>
									</div>
								<div class="CN19-caption1">
									贰、黄金存折线上申请资格、服务功能及费用收取
								</div>
								<div class="CN19-caption2">
									<span style="FONT-FAMILY: Wingdings;">u</span>
									<span>线上申请黄金存折帐户</span>
								</div>
								<div class="CN19-caption3">
									<ul>
										<li>申请资格</li>
									</ul>
									<div>
										年满20岁之本国人。<br>
 										已申请使用本行网路银行且已约定新台币活期性存款（不含支票存款）帐户为转出帐号者。<br>
										已申请使用本行芯片金融卡＋芯片读卡机或电子簽章（凭证载具）。<br>
									</div>
								</div>
								<div class="CN19-caption2">
									<span style="FONT-FAMILY: Wingdings;">u</span>
									<span>线上申请黄金存折网路交易功能</span>
								</div>
								<div class="CN19-caption3">
									<ul>
										<li>申请资格</li>
									</ul>
									<div>
										年满20岁之本国人。<br>
 										已申请使用本行网路银行且已约定新台币活期性存款（不含支票存款）帐户为转出帐号者。<br>
										已申请使用本行芯片金融卡＋芯片读卡机或电子簽章（凭证载具）。<br>
									</div>
								</div>
								<div class="CN19-caption3">
									<ul>
										<li>服务功能</li>
									</ul>
									<div>
										查询服务：黄金存折余额、明细、当日、历史价格查询功能。<br> 
 										交易服务：黄金买进、回售、缴纳定期扣款失败手续费功能。<br>
										预约服务：预约买进、回售、取消、查询功能。<br>
									 	定期定额服务：定期定额申购、定期定额变更、定期定额查询功能。<br>
									 	线上申请：线上申请黄金存折帐户、黄金存折网路交易功能。<br>
									</div>
								</div>
								<div class="CN19-caption3">
									<ul>
										<li>费用收取</li>
									</ul>
									<div>
										线上「申请黄金存折帐户」及「定期定额投资」扣帐成功，收取新台币50元手续费，其余交易免收。<br>
									</div>
								</div>
							</div>
						</div>
						<!-- 立即申请 页面 -->
						<div class="col-12 tab-content" id="CN19-now-apply" >
							<div class="ttb-input-block tab-pane fade show active"
								 role="tabpanel" aria-labelledby="nav-home-tab">
								<!-- 帐号区块-->
								<br>
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<!-- 网路银行-->
											网路银行
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
												使用台湾企银芯片金融卡 + 读卡机 
												<input type="radio" name="TYPE" id="" value="0" checked />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="radio-block">
												使用台湾企银信用卡 
												<input type="radio" name="TYPE" id="" value="1"/>
												<span class="ttb-radio"></span>
											</label>
										</div>
									</span>
								</div>
								<!-- 帐号区块-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<!-- 黄金存折 -->
											黄金存折
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
												申请黄金存折帐户(含黄金网路交易功能)
												<input type="radio" name="TYPE" id="" value="3"/>
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="radio-block">
												申请黄金网路交易功能—已有台湾企银实体黄金存折者
												<input type="radio" name="TYPE" id="" value="4"/>
												<span class="ttb-radio"></span>
											</label>
										</div>
									</span>
								</div>
								<!-- 帐号区块-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<!-- 开户申请 -->
											开户申请
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
												线上预约开立存款户
												<input type="radio" name="TYPE" id="" value="5"/>
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="radio-block">
												预约开立基金户
												<input type="radio" name="TYPE" id="" value="6"/>
												<span class="ttb-radio"></span>
											</label>
										</div>
									</span>
								</div>
								<!-- 帐号区块-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<!-- 信用卡 -->
											信用卡
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
												线上申请信用卡
												<input type="radio" name="TYPE" id="" value="7"  />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="radio-block">
												申请信用卡进度查询
												<input type="radio" name="TYPE" id="" value="20"  />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="radio-block">
												签署信用卡分期付款同意书
												<input type="radio" name="TYPE" id="" value="8"  />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="radio-block">
												长期使用循环信用客户线上申请分期还款
												<input type="radio" name="TYPE" id="" value="9"  />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="radio-block">
												信用卡额度调升
												<input type="radio" name="TYPE" id="" value="19"  />
												<span class="ttb-radio"></span>
											</label>
										</div>
									</span>
								</div>
								<!-- 帐号区块-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<!-- 数位存款帐户 -->
											数位存款帐户
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
												线上开立数位存款帐户
												<input type="radio" name="TYPE" id="" value="10"  />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="radio-block">
												线上数位存款帐户补上传身分证件
												<input type="radio" name="TYPE" id="" value="11"  />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="radio-block">
												修改数位存款帐户资料
												<input type="radio" name="TYPE" id="" value="12"  />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="radio-block">
												数位存款帐户芯片金融卡开卡及变更密码
												<input type="radio" name="TYPE" id="" value="13"  />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="radio-block">
												数位存款帐户补申请芯片金融卡
												<input type="radio" name="TYPE" id="" value="16"  />
												<span class="ttb-radio"></span>
											</label>
										</div>
									</span>
								</div>
								<!-- 帐号区块-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<!-- 随护神盾 -->
											随护神盾
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
												申请随护神盾
												<input type="radio" name="TYPE" id="" value="14"  />
												<span class="ttb-radio"></span>
											</label>
										</div>
									</span>
								</div>
								<!-- 帐号区块-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<!-- 无卡提款 -->
											无卡提款
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
												线上注销无卡提款功能
												<input type="radio" name="TYPE" id="" value="15"  />
												<span class="ttb-radio"></span>
											</label>
										</div>
									</span>
								</div>
								<!-- 帐号区块-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<!-- 扫码提款 -->
											扫码提款
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
												线上注销台湾Pay扫码提款功能
												<input type="radio" name="TYPE" id="" value="17"  />
												<span class="ttb-radio"></span>
											</label>
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<!-- 掃碼提款 -->
											<spring:message code= "LB.X2384" />
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code= "LB.X2385" />
												<input type="radio" name="TYPE" id="" value="18"  />
												<span class="ttb-radio"></span>
											</label>
										</div>
									</span>
								</div>
							</div>
							<!-- 确定 -->
							<spring:message code="LB.Confirm" var="cmSubmit"></spring:message>
							<input type="button" class="ttb-button btn-flat-gray" onclick="fstop.getPage('${pageContext.request.contextPath}'+'/login','', '')" value="回首页"/>	
							<input type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" class="ttb-button btn-flat-orange">
							
						</div>
						<div class="CN19-1-main text-left" id="TYPE3REQ" style="display: none;margin: 10px 200px;">
					    <font color="#FF6600" size="5">
					      	<B><span style="FONT-FAMILY: Wingdings;">u</span>本服务需具备下列申请要件：</B><br>
				      	</font><BR>
					    <font size="4">
						      	 一、	年满20岁之本国人。<br>
  								 二、	已申请使用本行网路银行且已约定新台币活期性存款（不含支票存款）帐户为转出帐号者。<br>
   								 三、	已申请使用本行芯片金融卡或电子簽章(凭证载具)。<br>
								如符合要件并已完成本行客户基本资料及投资属性测验问卷(KYC)者，请登入网路银行继续操作。<br>
					    </font>
					</div>
					<div class="CN19-1-main text-left" id="TYPE4REQ" style="display: none;margin: 10px 200px;">
					    <font color="#FF6600" size="5">
					      	<B><span style="FONT-FAMILY: Wingdings;">u</span>本服务需具备下列申请要件：</B><br>
				      	</font><BR>
					    <font size="4">
						      	一、		年满20岁之本国人。<br>
   								二、		已申请使用本行网路银行且已约定新台币活期性存款（不含支票存款）帐户为转出帐号者。<br>
								三、		已临柜开立黄金存折帐户，但尚未申请黄金存折网路交易。<br>
								四、		已申请使用本行芯片金融卡或电子簽章(凭证载具)。<br>
								符合上述要件并已完成本行客户基本资料及投资属性测验问卷(KYC)者， 请备妥本人之「台湾企银芯片金融卡＋读卡机」，或电子簽章（凭证载具）。<br>
					    </font>
					</div>
					<div class="CN19-1-main text-left" id="TYPE7REQ" style="display: none;margin: 10px 200px;">
					    <font color="#FF6600" size="5">
					      <B><span style="FONT-FAMILY: Wingdings;">u</span>本服务需具备下列申请要件：</B><br>
				      	</font><BR>
					    <font size="4">
						      	  一、  年满20岁之自然人，且持有本行信用卡正卡或为本行存款户。<br>
   								  二、  已申请使用本行网路银行。<br>
   								  三、  已申请使用本行芯片金融卡或电子簽章(凭证载具)。<br>
   								  
					    </font>
					</div>
					<div class="CN19-1-main text-left" id="TYPE8REQ" style="display: none;margin: 10px 200px;">
					    <font color="#FF6600" size="5">
					      <B><span style="FONT-FAMILY: Wingdings;">u</span>本服务需具备下列申请要件：</B><br>
				      	</font><BR>
					    <font size="4">
						      	 一、  持有本行信用卡正卡者。<br>
   								 二、  已申请使用本行网路银行。<br>
					    </font>
					</div>
					<div class="CN19-1-main text-left" id="TYPE9REQ" style="display: none;margin: 10px 200px;">
					    <font color="#FF6600" size="5">
					      	<B><span style="FONT-FAMILY: Wingdings;">u</span>本服务需具备下列申请要件：</B><br>
				      	</font><BR>
					    <font size="4">
						      	 一、  持有本行信用卡正卡者。<br>
  					 			 二、  已申请使用本行网路银行。<br>
   								 三、  连续使用循环信用达一年以上且最近一年内於财团法人金融联合征信中心无帐款迟缴或信用不良纪录。<br>
					    </font>
					</div>
					<div id="SUBSUBMIT" style="display: none;margin: auto">
							<input type="button" name="CMCANCEL" id="CMCANCEL" value="取消" class="ttb-button btn-flat-gray">
							<input type="button" name="CMSUBMIT2" id="CMSUBMIT2" value="登入网路银行" class="ttb-button btn-flat-orange">
					</div>
					</div>
					</form>
						<!-- 说明 -->
						<div id="CN19-description" class="text-left">
							<spring:message code="LB.Description_of_page" />:
							<ol class="list-decimal text-left">
								<li>网路银行：使用芯片金融卡申请者，限个人存户申请；信用卡申请者，请使用一般信用卡正卡申请（不含商务卡、采购卡、附卡及VISA金融卡）。查询<a href="#"  onclick="goPage('${pageContext.request.contextPath}'+'/ONLINE/APPLY/online_apply_menu_sub')">网路银行申请方式及服务功能</a></li>            
				                <li>黄金存折：已申请使用<font color=red>本行网路银行、年满20岁</font>之本国人,并<font color=red>已约定新台币活期性存款帐户为转出帐号</font>（不含支票存款）及已完成本行客户基本资料及投资属性测验问卷(KYC)者并具台湾企银芯片金融卡或电子簽章（凭证载具）者，每家分行限开乙户，<a href="#"  onclick="goPage('${pageContext.request.contextPath}'+'/ONLINE/APPLY/online_apply_menu_sub','', '')">黄金存折线上申请资格、服务功能及费用收取</a>。</li>
				                <li>如果您为本行存户但不符合线上申请条件，请您携带身分证、存款印鉴及存折至往来分行申请。</li>
				                <li>线上申请信用卡：申请者须已申请使用本行网路银行、年满20岁且持有本行信用卡正卡或为本行存款户，并具台湾企银芯片金融卡或电子簽章（凭证载具）者。</li>
				                <li>签署信用卡分期付款同意书：申请者须已申请使用本行网路银行且持有本行信用卡正卡者。</li>
				                <li>长期使用循环信用客户线上申请分期还款：申请者须已申请使用本行网路银行且持有本行信用卡正卡者。</li>
							</ol>
						</div>
					</div>
				</section>
			</main>
		</div>
		<%@ include file="../index/footer.jsp"%>
	</body>

</html>