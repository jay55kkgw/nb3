<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
	    <script type="text/javascript">
	    var width = $(window).width();
		$(window).resize(function() {
			width = $(this).width();
			changeDisplay();
		});
	    
        $(document).ready(function () {
			// HTML載入完成後0.1秒開始遮罩
			setTimeout("initBlockUI()", 100);

			// 初始化驗證碼
			setTimeout("initKapImg()", 200);
			// 生成驗證碼
			setTimeout("newKapImg()", 300);
			
			
            init();
			
			// 過0.5秒解遮罩
			setTimeout("unBlockUI(initBlockId)", 800);
        });
		
		/**
		 * 初始化BlockUI
		 */
		function initBlockUI() {
			initBlockId = blockUI();
		}
		
		
        function init() {
	    	$("#CMSUBMIT").click(function(e){
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
// 		        	initBlockUI();
		        	var action = '${__ctx}/ONLINE/APPLY/apply_deposit_account_result';
					$("form").attr("action", action);
	    			$("form").submit();
				}
			});

			$("#CMBACK").click( function(e) {
				console.log("submit~~");
    			$("#formId").attr("action", "${__ctx}/ONLINE/APPLY/apply_deposit_account_p4");
	
	         	initBlockUI();
	            $("#formId").submit();
			});
			$("#CMRESET").click(function(e){
				$("#formId")[0].reset();
				getTmr();
			});
			
			//img值初始化
			if("${input_data.FILE1}" != ""){
            	initImage("${input_data.FILE1}", '#img1');
			}
			if("${input_data.FILE2}" != ""){
            	initImage("${input_data.FILE2}", '#img2');
			}
			if("${input_data.FILE3}" != ""){
            	initImage("${input_data.FILE3}", '#img3');
			}
			if("${input_data.FILE4}" != ""){
            	initImage("${input_data.FILE4}", '#img4');
			}
			changeDisplay();
        }

		function ValidateValue(textbox) {
			var IllegalString = "+-_＿︿%@[`~!#$^&*()=|{}':;',\\[\\].<>/?~！#￥……&*（）——|{}【】‘；：”“'。，、？]‘'";
			var textboxvalue = textbox.value;
			var index = textboxvalue.length - 1;
			var s = textbox.value.charAt(index);
			if (IllegalString.indexOf(s) >= 0) {
				s = textboxvalue.substring(0, index);
				textbox.value = s;
			}
		}
		
		function changeDisplay(){
			<%-- 避免小版跑版 --%>
			if(width < 900){
				$("#bigBlock1").css("display","inline-block");
				$("#bigBlock2").css("display","inline-block");
			}
			else{
				$("#bigBlock1").css("display","flex");
				$("#bigBlock2").css("display","flex");
			}
		}
		
		// 刷新驗證碼
		function refreshCapCode() {
			console.log("refreshCapCode...");

			// 驗證碼
			$('input[name="capCode"]').val('');
			
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

			// 登入失敗解遮罩
			unBlockUI(initBlockId);
		}
		
		// 刷新輸入欄位
		function changeCode() {
			console.log("changeCode...");
			
			// 清空輸入欄位
			$('input[name="capCode"]').val('');
			
			// 刷新驗證碼
			refreshCapCode();
		}

		// 初始化驗證碼
		function initKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
		}

		// 生成驗證碼
		function newKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').click(function() {
				refreshCapCode();
			});
		}
        function initImage(fileName, imgFilter){
            $.ajax({
                url: "${__ctx}/ONLINE/APPLY/apply_deposit_account_get_image",
                type: "POST",
                data: {
                	"filename": fileName
                },
		        dataType: "json",
                success: function (res) {
                	console.log(res);
                 if(res.data.validated){
 					 $(imgFilter).attr('src',res.data.picFile);
                     $(imgFilter+"_close_btn").show();
                 } else {
					$(imgFilter).after("<p class='photo-error-message' id='" + type + "_error'>" + res.data.summary + "</p>");
                     $(imgFilter+"_close_btn").hide();
                 }
                }
            });
        	
        }
    </script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
    <!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><a href="#">開戶申請</a></li>
			<li class="ttb-breadcrumb-item"><a href="#">預約開立存款戶</a></li>
		</ol>
	</nav>
	<div class="content row">
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>預約開立存款戶</h2>
                <div id="step-bar">
                    <ul>
                     	<li class="finished">注意事項與權益</li>
						<li class="finished">開戶資料</li>
						<li class="active">確認資料</li>
						<li class="">完成預約</li>
                    </ul>
                </div>
                <form id="formId" method="post">
					
					<input type="hidden" id="TOKEN" name="TOKEN" value="${input_data.TOKEN}" /><!-- 防止重複交易 -->
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12 tab-content">
							<div class="ttb-input-block">
								<div class="ttb-message">
	                                <p>確認資料</p>
	                            </div>
								<p class="form-description">請再次確認您填寫的資料</p>
								<!-- ******************************* -->
	                            <div class="classification-block">
	                                <p>基本資料</p>
	                            </div>
								<!-- ******************************* -->
	                            <!-- 中文姓名 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D1105"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.CUSNAME}
											<input type="hidden" name="CUSNAME" value="${input_data.CUSNAME}">
										</div>
									</span>
								</div>
								<!-- ********** -->
								<!-- 身分證統一編號 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0519"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.CUSIDN}
											<input type="hidden" name="UID" value="${input_data.CUSIDN}">
										</div>
									</span>
								</div>
								<!-- ********** -->
								<!-- 出生日期 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0582"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<spring:message code="LB.D0583"/>&nbsp;${input_data.CCBIRTHDATEYY}<spring:message code="LB.Year"/>&nbsp;${input_data.CCBIRTHDATEMM}<spring:message code="LB.Month"/>&nbsp;${input_data.CCBIRTHDATEDD}<spring:message code="LB.D0586"/>&nbsp;
											<input type="hidden" name="BIRTHDAY" value="${input_data.BIRTHDAY}">
										</div>
									</span>
								</div>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											身分證發證資訊
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<spring:message code="LB.D0583"/>&nbsp;${input_data.CHGE_DT.substring(0,3)}<spring:message code="LB.Year"/>&nbsp;
											${input_data.CHGE_DT.substring(3,5)}<spring:message code="LB.Month"/>&nbsp;
											${input_data.CHGE_DT.substring(5,7)}<spring:message code="LB.D0586"/>&nbsp;
											(${input_data.CHGE_CYNAME})
											<c:if test="${ input_data.ID_CHGE.equals('1') }">
												<spring:message code="LB.D1113"/>
											</c:if>
											<c:if test="${ input_data.ID_CHGE.equals('2') }">
												<spring:message code="LB.W1664"/>
											</c:if>
											<c:if test="${ input_data.ID_CHGE.equals('3') }">
												<spring:message code="LB.W1665"/>
											</c:if>
											<input type="hidden" name="ID_CHGE" value="${input_data.ID_CHGE}">
											<input type="hidden" name="CHGE_DT" value="${input_data.CHGE_DT}">
											<input type="hidden" name="CHGE_CY" value="${input_data.CHGE_CY}">
											<input type="hidden" name="HAS_PIC" value="${input_data.HAS_PIC}">
										</div>
									</span>
								</div>
								<!-- ********** -->
								<!--學歷 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D1136"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<c:if test="${ input_data.DEGREE.equals('1') }">
												<spring:message code="LB.D0588"/>
											</c:if>
											<c:if test="${ input_data.DEGREE.equals('2') }">
												<spring:message code="LB.D0589"/>
											</c:if>
											<c:if test="${ input_data.DEGREE.equals('3') }">
												<spring:message code="LB.D0590"/>
											</c:if>
											<c:if test="${ input_data.DEGREE.equals('4') }">
												<spring:message code="LB.D0591"/>
											</c:if>
											<c:if test="${ input_data.DEGREE.equals('5') }">
												<spring:message code="LB.D1141"/>
											</c:if>
											<c:if test="${ input_data.DEGREE.equals('6') }">
												<spring:message code="LB.D0866"/>
											</c:if>
											<input type="hidden" name="DEGREE" value="${input_data.DEGREE}">
										</div>
									</span>
								</div>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											婚姻狀況
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<c:if test="${ input_data.MARRY.equals('0') }">
												<spring:message code="LB.D0596"/>
											</c:if>
											<c:if test="${ input_data.MARRY.equals('1') }">
												<spring:message code="LB.D0595"/>
											</c:if>
											<c:if test="${ input_data.MARRY.equals('2') }">
												<spring:message code="LB.D0572"/>
											</c:if>
											<input type="hidden" name="MARRY" value="${input_data.MARRY}">
										</div>
									</span>
								</div>
								<!-- ********** -->
								<c:if test="${ !input_data.MARRY.equals('0') }">
									<div class="ttb-input-item row">
										<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.D1149"/>
											</h4>
										</label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												${input_data.CHILD} 人
												<input type="hidden" name="CHILD" value="${input_data.CHILD}">
											</div>
										</span>
									</div>
								</c:if>
								<!-- ********** -->
								<!-- 個人/家庭年收入 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.X0170"/><spring:message code="LB.D0625_1"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.SALARY}<spring:message code="LB.D1144"/>
											<input type="hidden" name="SALARY" value="${input_data.SALARY}">
										</div>
									</span>
								</div>
								<!-- ******************************* -->
	                            <div class="classification-block">
	                                <p>聯絡資訊</p>
	                            </div>
								<!-- ******************************* -->
								<!-- E-mail -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											E-mail
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.MAILADDR}
											<input type="hidden" name="MAILADDR" value="${input_data.MAILADDR}">
										</div>
									</span>
								</div>
								<!-- ********** -->
								<!-- 行動電話 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0069"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.CELPHONE}
											<input type="hidden" name="CELPHONE" value="${input_data.CELPHONE}">
										</div>
									</span>
								</div>
								<!-- ********** -->
								<!-- 居住地電話 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D1127"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.ARACOD1}-${input_data.TELNUM1}
											<input type="hidden" name="ARACOD1" value="${input_data.ARACOD1}">
											<input type="hidden" name="TELNUM1" value="${input_data.TELNUM1}">
										</div>
									</span>
								</div>
								<!-- ********** -->
								<!-- 公司電話 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0094"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.ARACOD2}-${input_data.TELNUM2}
											<input type="hidden" name="ARACOD2" value="${input_data.ARACOD2}">
											<input type="hidden" name="TELNUM2" value="${input_data.TELNUM2}">
										</div>
									</span>
								</div>
								<!-- ********** -->
								<!-- 傳真 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D1131"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.ARACOD3}-${input_data.FAX}
											<input type="hidden" name="ARACOD3" value="${input_data.ARACOD3}">
											<input type="hidden" name="FAX" value="${input_data.FAX}">
										</div>
									</span>
								</div>
								<!-- ********** -->
								<!-- 戶籍地址 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0143"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.POSTCOD1} ${input_data.PMTADR}
											<input type="hidden" name="POSTCOD1" value="${input_data.POSTCOD1}">
											<input type="hidden" name="PMTADR" value="${input_data.PMTADR}">
										</div>
									</span>
								</div>
								<!-- ********** -->
								<!-- 居住地址 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0376"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.POSTCOD2} ${input_data.CTTADR}
											<input type="hidden" name="POSTCOD2" value="${input_data.POSTCOD2}">
											<input type="hidden" name="CTTADR" value="${input_data.CTTADR}">
										</div>
									</span>
								</div>
								<!-- ******************************* -->
	                            <div class="classification-block">
	                                <p>職業資訊</p>
	                            </div>
								<!-- ******************************* -->
								<!-- 職業 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D1132"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.CAREER1_str}
											<input type="hidden" name="CAREER1" value="${input_data.CAREER1}">
										</div>
									</span>
								</div>
								<!-- ********** -->
								<!-- 職稱 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0087"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
<%-- 											${input_data.data.CAREER2_str} --%>
											<c:if test="${ input_data.CAREER2.equals('1') }">
												<spring:message code="LB.X0179"/>
											</c:if>
											<c:if test="${ input_data.CAREER2.equals('2') }">
												<spring:message code="LB.X0180"/>
											</c:if>
											<c:if test="${ input_data.CAREER2.equals('3') }">
												<spring:message code="LB.X0181"/>
											</c:if>
											<c:if test="${ input_data.CAREER2.equals('4') }">
												<spring:message code="LB.X0182"/>
											</c:if>
											<c:if test="${ input_data.CAREER2.equals('5') }">
												<spring:message code="LB.X0183"/>
											</c:if>
											<c:if test="${ input_data.CAREER2.equals('6') }">
												<spring:message code="LB.X0184"/>
											</c:if>
											<c:if test="${ input_data.CAREER2.equals('7') }">
												<spring:message code="LB.X0185"/>
											</c:if>
											<c:if test="${ input_data.CAREER2.equals('8') }">
												<spring:message code="LB.X0186"/>
											</c:if>
											<c:if test="${ input_data.CAREER2.equals('9') }">
												<spring:message code="LB.X0187"/>
											</c:if>
											<input type="hidden" name="CAREER2" value="${input_data.CAREER2}">
										</div>
									</span>
								</div>
								<!-- ********** -->
								<!-- 任職機構 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D1074"/>
										</h4>
									</label>
									</span> 
										<div class="ttb-input">
											${input_data.EMPLOYER}
										</div>
									</span>
								</div>
								<!-- ******************************* -->
	                            <div class="classification-block">
	                                <p>身份資料</p>
	                            </div>
								<!-- ******************************* -->
								
	                            <div class="photo-block" id="bigBlock1">
	                                <div>
										<img id="img1" src="${__ctx}/img/upload_empty.svg" />
										<input type="hidden" name="FILE1" value="${input_data.FILE1}">
	                                    <p><spring:message code="LB.D0111"/></p>
	                                </div>
	                                <div>
										<img id="img2" src="${__ctx}/img/upload_empty.svg" />
										<input type="hidden" name="FILE2" value="${input_data.FILE2}">
	                                    <p>身分證背面圖片</p>
	                                </div>
	                            </div>
								<!-- ******************************* -->
								<div class="classification-block">
								    <p>第二證件資料</p>
								</div>
								<!-- ******************************* -->
	                            <div class="photo-block" id="bigBlock2">
	                                <div>
										<img id="img3" src="${__ctx}/img/upload_empty.svg" />
										<input type="hidden" name="FILE3" value="${input_data.FILE3}">
	                                    <p><spring:message code="LB.D1159"/></p>
	                                </div>
	                                <div>
										<img id="img4" src="${__ctx}/img/upload_empty.svg" />
										<input type="hidden" name="FILE4" value="${input_data.FILE4}">
	                                    <p>第二證件背面圖片</p>
	                                </div>
	                            </div>
								<!-- ******************************* -->
	                            <div class="classification-block">
	                                <p>帳戶約定事項</p>
	                            </div>
								<!-- ******************************* -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											臺幣綜合存款質借功能
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>
												<c:if test="${ input_data.ODFTYN.equals('Y') }">
													同意申請
												</c:if>
												<c:if test="${ input_data.ODFTYN.equals('N') }">
													不同意
												</c:if>
											</span>
											<input type="hidden" name="ODFTYN" value="${input_data.ODFTYN}">
										</div>
									</span>
								</div>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											非約定帳號轉帳功能
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>
												<c:if test="${ input_data.LMTTYN.equals('Y') }">
													同意申請
												</c:if>
												<c:if test="${ input_data.LMTTYN.equals('N') }">
													不同意
												</c:if>
											</span>
											<input type="hidden" name="LMTTYN" value="${input_data.LMTTYN}">
										</div>
									</span>
								</div>
								<!-- ********** -->
								<c:if test="${ input_data.IBHPWD_SHOW.equals('1') }">
									<div class="ttb-input-item row">
										<span class="input-title"> 
										<label>
											<h4>
												全行收付功能
											</h4>
										</label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												<span>同意申請</span>
											</div>
										</span>
									</div>
									<div class="ttb-input-item row">
										<span class="input-title"> 
										<label>
											<h4>
												全行收付功能密碼設定
											</h4>
										</label>
										</span> 
										<span class="input-block">
		                                    <div class="ttb-input">
		                                        <span>全行收付功能密碼</span>
		                                    </div>
											<div class="ttb-input">
												<span>
														${ input_data.IBHPW }
												</span>
												<input type="hidden" name="IBHPW" value="${input_data.IBHPW}">
											</div>
										</span>
									</div>
								</c:if>
								<c:if test="${ input_data.IBHPWD_SHOW.equals('0') }">
									<div class="ttb-input-item row">
										<span class="input-title"> 
										<label>
											<h4>
												全行收付功能
											</h4>
										</label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												<span>不同意</span>
												<input type="hidden" name="IBHPW" value="">
											</div>
										</span>
									</div>
								</c:if>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title">
									<label>
										<h4>是否申請為起家金帳戶</h4>
									</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												<c:if test="${ input_data.STAEDYN.equals('Y') }">
													同意申請
												</c:if>
												<c:if test="${ input_data.STAEDYN.equals('N') }">
													不同意
												</c:if>
											</span>
											<input type="hidden" name="STAEDYN" value="${input_data.STAEDYN}">
										</div>
									</span>
								</div>

								<c:if test="${ input_data.STAEDYN.equals('Y') }">
									<div class="ttb-input-item row">
										<span class="input-title">
										<label>
											<h4>北港朝天宮起家金帳戶活動憑證號碼5位數字</h4>
										</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>${ input_data.STAEDSQ }</span>
												<input type="hidden" name="STAEDSQ" value="${input_data.STAEDSQ}">
											</div>
										</span>
									</div>
								</c:if>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											網路銀行使用者名稱設定
										</h4>
									</label>
									</span> 
									<span class="input-block">
	                                    <div class="ttb-input">
	                                        <span>使用者名稱</span>
	                                    </div>
										<div class="ttb-input">
											<span>${ input_data.USERID }</span>
											<span class="input-remarks">(<spring:message code="LB.D1184"/>)</span>
											<input type="hidden" name="USERID" value="${input_data.USERID}">
										</div>
									</span>
								</div>
								<!-- ********** -->
								<!-- 指定開戶分行 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D1135"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.BRHCOD} ${input_data.BRHNAME}
											<input type="hidden" name="BRHCOD" value="${input_data.BRHCOD}">
											<input type="hidden" name="BRHNAME" value="${input_data.BRHNAME}">
										</div>
									</span>
								</div>
								
								<!-- 確認新使用者名稱 -->
<!-- 								<div class="ttb-input-item row"> -->
<!-- 									<span class="input-title">  -->
<!-- 										<label> -->
<!-- 											<h4> -->
<%-- 												<spring:message code="LB.Captcha"/> --%>
<!-- 											</h4> -->
<!-- 										</label> -->
<!-- 									</span>  -->
<!-- 									<span class="input-block"> -->
<!-- 										<div class="ttb-input"> -->
											
<%-- 											<spring:message code="LB.Captcha" var="labelCapCode" /> --%>
<!-- 											<input id="capCode" type="text" class="ttb-input" -->
<%-- 												name="capCode" placeholder="${labelCapCode}" maxlength="6" autocomplete="off"> --%>
<!-- 											<img name="kaptchaImage" src="" /> -->
<%-- 											&nbsp;<font color="#FF0000"><spring:message code="LB.Captcha_refence"/> </font> --%>
<!-- 											<div class="login-input-block"> -->
<%-- 												<input type="button" name="reshow" class="ttb-sm-btn btn-flat-orange" onclick="refreshCapCode()" value="<spring:message code="LB.Regeneration"/>" /> --%>
<!-- 											</div> -->
<!-- 										</div> -->
<!-- 									</span> -->
<!-- 								</div> -->
								
                           	</div>
	                        <!--button 區域 -->
							<!-- 重新輸入 -->
							<input type="button" name="CMBACK" id="CMBACK" value="<spring:message code="LB.X0318"/>" class="ttb-button btn-flat-gray">
                            <input id="CMSUBMIT" name="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.X0080"/>" />
                        <!--                     button 區域 -->
                        </div>  
                    </div>
                </form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>