<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {
		// 將.table變更為footable
		initFootable();
		init()
	});

	function init() {
		//$("#formId").validationEngine({binded: false,promptPosition: "inline"});
		//btn 
		$("#CMSUBMIT").click(function (e) {
			if($("#ACN").val() != ""){
				e = e || window.event;
				$("#formId").attr("action", "${__ctx}/GOLD/TRANSACTION/pay_fail_fee_confirm");
				$("#formId").submit();
			}

		});
	} // init END
	
	//下拉式選單
 	function formReset() {
 		if ($('#actionBar').val()=="reEnter"){
	 		document.getElementById("formId").reset();
	 		$('#actionBar').val("");
 		}
	}
</script>
</head>
	<body>
		<!-- header     -->
		<header>
			<%@ include file="../index/header.jsp"%>
		</header>
		<!--   IDGATE -->
		<%@ include file="../idgate_tran/idgateAlert.jsp" %>
	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 黃金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2272" /></li>
    <!-- 繳納定期扣款失敗手續費     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1533" /></li>
		</ol>
	</nav>

		<!-- menu、登出窗格 -->
		<div class="content row">
			<%@ include file="../index/menu.jsp"%>
			<!-- 	快速選單內容 -->
			<!-- content row END -->
			<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<!--  繳納定期扣款失敗手續費 -->
				<h2><spring:message code="LB.W1533"/></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="">
					<input type="hidden" id="TOKEN" name="TOKEN" value="${sessionScope.transfer_confirm_token}" /><!-- 防止重複交易 -->
					<input type="hidden" name="TXID" value="N094_1">		
					<input type="hidden" name="FGTXWAY" value="0">		
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<!-- 輕鬆理財自動申購基金明細 -->
							<div class="ttb-input-block">							
								<!-- 帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
										<!-- 請選擇黃金存摺帳號 -->
											<h4><spring:message code="LB.W1534"/></h4>
										</label>
									</span> 
									<!-- 帳號 的下拉式選單-->
									<span class="input-block">
										<div class="ttb-input">
										<!--帳號 -->
											<select class="custom-select select-input half-input" name="ACN" id="ACN">
												<option value="">
												<!-- 請選擇黃金存摺帳號 -->
													<spring:message code="LB.W1534"/>
												</option>
												<c:forEach var="dataList" items="${result_data.data.REC}">
													<option> ${dataList.ACN}</option>
												</c:forEach>
											</select>
										</div>
									</span>
								</div>				
							</div>
							<!-- 網頁顯示-->
							<input type="button" name="CMSUBMIT" id="CMSUBMIT"  class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm"/>"/>
						</div>
					</div>
				</form>
				</section>
			</main>
			<!-- 		main-content END -->
			<!-- 	content row END -->
		</div>
		<%@ include file="../index/footer.jsp"%>
	</body>
</html>