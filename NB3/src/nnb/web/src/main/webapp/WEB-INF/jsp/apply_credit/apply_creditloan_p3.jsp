<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

     <%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
   <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">    
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">

<script type="text/javascript">
	$(document).ready(function() {
		// 將.table變更為footable
		initFootable();
		
		//表單驗證
		$("#formId").validationEngine({
			binded : false,
			promptPosition : "inline"
		});
		
		$("#CMSUBMIT").click(function(e){
			
			if($('input:radio[name=agreeCheck]:checked').val() == "Y"){
				$("#validate").prop("checked",true)
			}
			console.log("submit~~");
			if (!$('#formId').validationEngine('validate')) {
				e.preventDefault();
				$(".validateformError").css("text-align","center");
			} else {
				initBlockUI();
				$("#formId").validationEngine('detach');
				$("form").submit();
			}
		})
		$("#PREVIOUS").click(function(e) {
			$("#formId").validationEngine('detach');
			action = '${__ctx}/APPLY/CREDIT/apply_creditloan_p2'
			$("form").attr("action", action);
			$("form").submit();
		});
		
	});
	function buttonEvent(){
		if($('input:radio[name=agreeCheck]:checked').val()=='N'){
			$('#CMSUBMIT').attr("disabled",true)
			$('#CMSUBMIT').removeClass('btn-flat-orange');
			$('#CMSUBMIT').addClass('btn-flat-gray')
			$('#PREVIOUS').removeClass('btn-flat-gray');
			$('#PREVIOUS').addClass('btn-flat-orange')
		}
		if($('input:radio[name=agreeCheck]:checked').val()=='Y'){
			$('#CMSUBMIT').removeAttr("disabled")
			$('#CMSUBMIT').addClass('btn-flat-orange');
			$('#CMSUBMIT').removeClass('btn-flat-gray');
			$('#PREVIOUS').removeClass('btn-flat-orange');
			$('#PREVIOUS').addClass('btn-flat-gray')
		}
	}
	
</script>
</head>
	<body>
		<!-- header     -->
		<header>
			<%@ include file="../index/header.jsp"%>
		</header>
	
		<!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<!--申請小額信貸 -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0543" /></a></li>
		</ol>
	</nav>
		<!-- menu、登出窗格 -->
		<div class="content row">
			<%@ include file="../index/menu.jsp"%>
			<!-- 	快速選單內容 -->
			<!-- content row END -->
			<main class="col-12">
				<section id="main-content" class="container">
				
					<h2><!--申請小額信貸 -->
					<spring:message code= "LB.D0543" />
					</h2>
					<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
					<form id="formId" method="post" action="${__ctx}/APPLY/CREDIT/apply_creditloan_p4">
						<input type="hidden" id="LOANTYPE" name ="LOANTYPE" value="${bs.data.LOANTYPE}">
						<div class="main-content-block row">
							<div class="col-12">
			            <div class="ttb-message">
			            <!-- 臺灣中小企業銀行履行個人資料保護法告知義務告知書 -->
                            <p><spring:message code= "LB.D1065" /></p>
                        </div>
							<br>
							<div class="CN19-clause">
								<div class="NA01_3">
									
									<ol>
										<li>
											<div>ㄧ、</div>
											<div>親愛的客戶您好，由於個人資料之蒐集，涉及 臺端的隱私權益，臺灣中小企業銀行股份有限公司(以下稱本行)向 臺端蒐集個人資料時，依據個人資料保護法以下稱個資法第八條第一項規定，應明確告知 臺端下列事項：（一）非公務機關名稱（二）蒐集之目的（三）個人資料之類別（四）個人資料利用之期間、地區、對象及方式（五）當事人依個資法第三條規定得行使之權利及方式（六）當事人得自由選擇提供個人資料時，不提供將對其權益之影響。</div>
										</li>
										<li>
											<div>二、</div>
											<div>有關本行蒐集 臺端個人資料之目的、個人資料類別及個人資料利用之期間、地區、對象及方式等內容，請 臺端詳閱如後附表。</div>
										</li>
										<li>
											<div>三、</div>
											<div>依據個資法第三條規定，臺端就本行保有 臺端之個人資料得行使下列權利：</div>
										</li>
										<li style="margin-left:20px;">
											<div>（一）</div>
											<div>除有個資法第十條所規定之例外情形外，得向本行查詢、請求閱覽或請求製給複製本，惟本行依個資法第十四條規定得酌收必要成本費用。</div>
										</li>
										<li style="margin-left:20px;">
											<div>（二）</div>
											<div>得向本行請求補充或更正，惟依個資法施行細則第十九條規定，臺端應原因及事實。</div>
										</li>
										<li style="margin-left:20px;">
											<div>（三）</div>
											<div>本行如有違反個資法規定蒐集、處理或利用臺端之個人資料，依個資法第十一條第四項規定，臺端得向本行請求停止蒐集。</div>
										</li>
										<li style="margin-left:20px;">
											<div>（四）</div>
											<div>依個資法第十一條第二項規定，個人資料正確性有爭議者，得向本行請求停止處理或利用臺端之個人資料。惟依該項但書規定，本行因執行業務所必須並註明其爭議或經 臺端書面同意者，不在此限。</div>
										</li>
										<li style="margin-left:20px;">
											<div>（五）</div>
											<div>依個資法第十一條第三項規定，個人資料蒐集之特定目的消失或期限屆滿時，得向本行請求刪除、停止處理或利用臺端之個人資料。惟依該項但書規定，本行因執行業務所必須或經 臺端書面同意者，不在此限。</div>
										</li>
										<li>
											<div>四、</div>
											<div>臺端如欲行使上述個資法第三條規定之各項權利，有關如何行使之方式，得向本行客服(0800-00-7171）詢問或於本行網站（網址：https://www.tbb.com.tw）查詢。</div>
										</li>
										<li>
											<div>五、</div>
											<div>臺端得自由選擇是否提供相關個人資料及類別，惟臺端所拒絕提供之個人資料及類別，如果是辦理業務審核或作業所需之資料，本行可能無法進行必要之業務審核或作業而無法提供 臺端相關服務或無法提供較佳之服務，敬請見諒。</div>
										</li>
									</ol>
								<table class="table" data-toggle-column="first">
									<thead>
										<tr>
											<td colspan="3">特定目的說明</td>
											<td>蒐集之個人資料類別</td>
											<td>個人資料利用之期間</td>
											<td>個人資料利用之地區</td>
											<td>個人資料利用之對象</td>
											<td>個人資料利用之方式</td>
										</tr>
										<tr>
											<td nowrap>業務類別</td>
											<td nowrap>業務特定目的及代號</td>
											<td nowrap>共通特定目的及代號</td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>授信業務</td>
											<td>
												<table class="NA01_3-inner-table">
													<tbody>
														<tr>
															<td>022</td>
															<td>外匯業務</td>
														</tr>
														<tr>
															<td>067</td>
															<td>信用卡、現金卡、轉帳卡或電子票證業務</td>
														</tr>
														<tr>
															<td>082</td>
															<td>借款戶與存款戶存借作業綜合管理</td>
														</tr>
														<tr>
															<td>088</td>
															<td>核貸與授信業務</td>
														</tr>
														<tr>
															<td>106</td>
															<td>授信業務</td>
														</tr>
														<tr>
															<td>111</td>
															<td>票券業務</td>
														</tr>
														<tr>
															<td>126</td>
															<td>債權整貼現及收買業務</td>
														</tr>
														<tr>
															<td>154</td>
															<td>徵信</td>
														</tr>
														<tr>
															<td>181</td>
															<td>其他經營合於營業登記項目或組織章程所定之業務（例如：共同行銷或合作推廣業務等。）</td>
														</tr>
													</tbody>
												</table>
											</td>
											<td>
												<table class="NA01_3-inner-table">
													<tbody>
														<tr>
															<td>040</td>
															<td>行銷</td>
														</tr>
														<tr>
															<td>059</td>
															<td>金融服務業依法令規定及金融監理需要，所為之蒐集處理及利用</td>
														</tr>
														<tr>
															<td>060</td>
															<td>借金融爭議處理</td>
														</tr>
														<tr>
															<td>063</td>
															<td>非公務機關依法定義務所進行個人資料之蒐集處理及利用</td>
														</tr>
														<tr>
															<td>069</td>
															<td>契約、類似契約或其他法律關係管理之事務</td>
														</tr>
														<tr>
															<td>090</td>
															<td>消費者、客戶管理與服務</td>
														</tr>
														<tr>
															<td>091</td>
															<td>消費者保護</td>
														</tr>
														<tr>
															<td>098</td>
															<td>商業與技術資訊</td>
														</tr>
														<tr>
															<td>104</td>
															<td>帳務管理及債權交易業務</td>
														</tr>
														<tr>
															<td>136</td>
															<td>資(通)訊與資料庫管理</td>
														</tr>
														<tr>
															<td>137</td>
															<td>資通安全與管理</td>
														</tr>
														<tr>
															<td>182</td>
															<td>其他諮詢與顧問服務</td>
														</tr>
													</tbody>
												</table>
											</td>
											<td>
												姓名、身分證統一編號、性別、出生年月日、通訊方式及其他詳如相關業務申請書或契約書之內容，並以本行與客戶往來之相關業務、帳戶或服務及自客戶或第三人處（例如：財團法人金融聯合徵信中心）所實際蒐集之個人資料為準。
											</td>
											<td>
												<table class="NA01_3-inner-table">
													<tbody>
														<tr>
															<td>ㄧ、</td>
															<td>特定目的存續期間。</td>
														</tr>
														<tr>
															<td>二、</td>
															<td>依相關法令所定（例如商業會計法等）或因執行業務所必須之保存期間或依個別契約就資料之保存所定之保存年限。（以期限最長者為準）</td>
														</tr>
													</tbody>
												</table>
											</td>
											<td>
												右邊「個人資料利用之對象」欄位所列之利用對象其國內及國外所在地。
											</td>
											<td>
												<table class="NA01_3-inner-table">
													<tbody>
														<tr>
															<td>ㄧ、</td>
															<td>本行(含受本行委託處理事務之委外機構)。</td>
														</tr>
														<tr>
															<td>二、</td>
															<td>依法令規定利用之機構（例如：本行母公司或所屬金融控股公司等）。</td>
														</tr>
														<tr>
															<td>三、</td>
															<td>其他業務相關之機構（例如：通匯行、財團法人金融聯合徵信中心、財團法人聯合信用卡處理中心、台灣票據交換所、財金資訊股份有限公司、信用保證機構、信用卡國際組織、收單機構暨特約商店等）。</td>
														</tr>
														<tr>
															<td>四、</td>
															<td>依法有權機關或金融監理機關。</td>
														</tr>
														<tr>
															<td>五、</td>
															<td>客戶所同意之對象（例如本行共同行銷或交互運用客戶資料之公司、與本行合作推廣業務之公司等）。</td>
														</tr>
													</tbody>
												</table>
	  										</td>
											<td>
												符合個人資料保護相關法令以自動化機器或其他非自動化之利用方式。
											</td>
										</tr>
									</tbody>
								</table>
								</div>
								</div>
								<br>
								<div>
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
											<!-- 已詳閱，同意提供個人資料並進行申請  -->
												<spring:message code= "LB.D0548" />
												<input type="radio" name="agreeCheck" id="AG" value="Y" onclick="buttonEvent()" />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="radio-block">
											<!-- 已詳閱，不同意提供個人資料，並返回  -->
												<spring:message code= "LB.D0549" />
												<input type="radio" name="agreeCheck" id="NAG" value="N" onclick="buttonEvent()" />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<div style="text-align: center">
												<div class="radio-block">
													<input type="radio" name="agreeCheck" id="validate" class="validate[required]"/>
												</div>
											</div>
										</div>
									</span>
								</div>
								<!-- 上一步-->
									<input class="ttb-button btn-flat-gray" type="button" value=" <spring:message code= "LB.X0318" />" id="PREVIOUS" >
									<!-- 下一步-->
				                   	<input class="ttb-button btn-flat-orange" type="button" value=" <spring:message code= "LB.X0080" />" name="CMSUBMIT" id="CMSUBMIT">
							</div>
						</div>
					</form>
				</section>
			</main>
			<!-- 		main-content END -->
			<!-- 	content row END -->
		</div>
		<%@ include file="../index/footer.jsp"%>
	</body>
</html>