<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript">
	
		$(document).ready(function() {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
	
		// 畫面初始化
		function init() {
			// 表單驗證初始化
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
			// 確認鍵 click
			goOn();
			// 上一頁按鈕 click
			goBack();
			// 驗證是否點選 續存方式radio
			TYPE1Event();
		}
		
		
		// 確認鍵 Click
		function goOn() {
			$("#CMSUBMIT").click( function(e) {
				console.log("submit~~");
	
				// 表單驗證
				if ( !$('#formId').validationEngine('validate') ) {
					e.preventDefault();
				} else {
					// 解除表單驗證
					$("#formId").validationEngine('detach');
					
		         	// 通過表單驗證準備送出
					processQuery();
				}
			});
		}
		
		
		// 通過表單驗證準備送出
		function processQuery(){
			
			// 檢查radio選項
			// 是否選擇本金轉期，利息轉入帳號
			var TYPE1 = $('input[type=radio][name=TYPE1]:checked').val() == '0' ? true : false;
			console.log("TYPE1 >> "  + TYPE1);
			// 是否選擇利息轉入帳號
			var FYTSFAN1 = $("#FYTSFAN1").find(":selected").val() == "#" ? true : false;
			console.log("FYTSFAN1 >> "  + FYTSFAN1);
			
			if (TYPE1 && FYTSFAN1) 
			{
				// 未選擇
				//<!-- 請選擇利息轉入帳號 -->
				//alert('<spring:message code= "LB.Alert007" />');
				errorBlock(
							null, 
							null,
							['<spring:message code= 'LB.Alert007' />'], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
		   		return false;   	
			}
			
			if (TYPE1) 
			{
				var value = $("#FYTSFAN1").find(":selected").val();
				console.log("FYTSFAN1 selected value >> "  + value);
				// 利息轉入帳號 hidden 設值
				$("#FYTSFAN").val(value);
			}	
			// 遮罩
         	initBlockUI();
            $("#formId").submit();
		}
		
		// 上一頁按鈕 click
		function goBack() {
			// 上一頁按鈕
			$("#CMBACK").click(function() {
				// 遮罩
				initBlockUI();
				// 解除表單驗證
				$("#formId").validationEngine('detach');
				// 讓Controller知道是回上一頁
				$('#back').val("Y");
				// 回上一頁
				var action = '${__ctx}/FCY/ACCT/TDEPOSIT/f_order_renewal';
				$("#formId").attr("action", action);
				$("#formId").submit();
			});
		}
		
		// 驗證是否點選 續存方式radio
		function TYPE1Event() {
			$('input[type=radio][name=TYPE1]').change(function () {
				console.log(this.value );
				if (this.value == '0') {
					$("#FYTSFAN1").addClass("validate[required]")
				}
				else if (this.value == '1') {
					$("#FYTSFAN1").removeClass("validate[required]");
				}
			});
		}
		
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 定存服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Current_Deposit_To_Time_Deposit" /></li>
    <!-- 定存單到期續存     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Renewal_At_Maturity_Of_FX_Time_Deposit" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
	</div>
	<!-- 主頁內容  -->
	<main class="col-12">
		<section id="main-content" class="container">
			<h2>
				<!--  外匯定存單到期續存 -->
				<spring:message code="LB.Renewal_At_Maturity_Of_FX_Time_Deposit" />
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<!-- 下拉式選單-->
			<div class="print-block no-l-display-btn">
				<select class="minimal" id="actionBar" onchange="formReset()">
					<option value=""><spring:message code="LB.Execution_option" /></option>
						<!-- 重新輸入-->
					<option value="reEnter"><spring:message code="LB.Re_enter"/></option>
				</select>
			</div>
			<form id="formId" method="post" action="${__ctx}/FCY/ACCT/TDEPOSIT/f_order_renewal_confirm">
				<input type="hidden" id="back" name="back" value="">
				<c:set var="BaseResultData" value="${f_order_renewal_step1.data }"></c:set>
				<input type="hidden" name="REQPARAMJSON" value='${BaseResultData.fgselectJson}' />
				<!-- 利息轉入帳號 -->
				<input type="hidden" name="FYTSFAN" id="FYTSFAN" value='${BaseResultData.TSFACN}' />
				
				<!--交易步驟 -->
				<div id="step-bar">
					<ul>
						<!--輸入資料 -->
						<li class="active"><spring:message code="LB.Enter_data"/></li>
						<!-- 確認資料 -->
						<li class=""><spring:message code="LB.Confirm_data"/></li>
						<!-- 交易完成 -->
						<li class=""><spring:message code="LB.Transaction_complete"/></li>
					</ul>
       			</div>
				<!-- 表單顯示區  -->
				<div class="main-content-block row">
					<div class="col-12">
						<div class="ttb-input-block">
							<!-- 存單帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Account_no" /></h4>
									</label>
								</span>
								<span class="input-block">
									${BaseResultData.fgselectMap.ACN }
								</span>
							</div>
							<!-- 存單號碼 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Certificate_no" /></h4>
									</label>
								</span>
								<span class="input-block">
									${BaseResultData.fgselectMap.FDPNO }
								</span>
							</div>
							<!-- 存單金額 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Certificate_amount" /></h4>
									</label>
								</span>
									<span class="input-block">
									<div class="ttb-input">
	                                    <span class="high-light">
		                                    <!-- 幣別 &&存單金額 -->
	                                     	<span class="input-unit"> ${BaseResultData.fgselectMap.CUID }</span></span>
	                                     <p>
											<fmt:formatNumber type="number" minFractionDigits="2" value="${BaseResultData.fgselectMap.BALANCE }" />
		                                    <!--元 -->
		                                    <span class="input-unit"><spring:message code="LB.Dollar"/></span>
	                                 	</p>
                                 	</div>
                                 </span>
							</div>
							<!-- 計息方式 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Interest_calculation" /></h4>
									</label>
								</span>
								<span class="input-block">
									<!--機動 -->
									<label class="radio-block">
										<spring:message code="LB.Floating" />
										<input type="radio" name="INTMTH" value="0" checked>
										<span class="ttb-radio"></span>
									</label>
									<!--  固定 -->
									<label class="radio-block">
										<spring:message code="LB.Fixed" />
										<input type="radio" name="INTMTH" value="1">
										<span class="ttb-radio"></span>
									</label>
								</span>
							</div>
							<!--續存方式 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<!--  續存方式 -->
										<h4><spring:message code="LB.Renew_method" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block">
											<!--本金轉期，利息轉入帳號 -->
											<spring:message code="LB.Principal_rollover_interest_to_account"/>
											<input type="radio" name="TYPE1" value="0" checked="checked" />
											<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<select name="FYTSFAN1" id="FYTSFAN1" class="custom-select w-auto select-input half-input validate[required]">
											<option value="#">
												<!--  請選擇帳號 -->
												---<spring:message code="LB.Select_account" />---
											</option>
											<c:forEach var="row" items="${ BaseResultData.REC }">
												<option value='${row.ACN}'><c:out value='${row.TEXT}' /></option>
											</c:forEach>
										</select>
									</div>
									<div class="ttb-input">
										<label class="radio-block">
											<!--  本金及利息一併轉期 -->
											<spring:message code="LB.Renew_principal_and_interest" />
											<input type="radio" name="TYPE1" id="DPSVTYPE2" value="1" />
											<span class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>
						</div>
					<!-- button -->
							<!--回上頁 -->
							<spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>
							<input type="button" name="CMBACK" id="CMBACK" value="${cmback}" class="ttb-button btn-flat-gray">
<!-- 							重新輸入 -->
<%-- 							<spring:message code="LB.Re_enter" var="cmRest"></spring:message> --%>
<%-- 							<input type="reset" name="CMRESET" id="CMRESET" value="${cmRest}" class="ttb-button btn-flat-gray no-l-disappear-btn"> --%>
							<!-- 確定 -->
							<spring:message code="LB.Confirm" var="cmSubmit"></spring:message>
							<input type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" class="ttb-button btn-flat-orange">
					<!-- button -->
					</div>
				</div>
				<div class="text-left">
					<!-- 		說明： -->
					<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page"/></p>					
						<li><spring:message code="LB.F_order_renewal_P2_D1"/> </li>
						<li><spring:message code="LB.F_order_renewal_P2_D2"/></li>
						<li><spring:message code="LB.F_order_renewal_P2_D3"/></li>
					</ol>
				</div>
			</form>
		</section>
		<!-- main-content END -->
	</main>
	<%@ include file="../index/footer.jsp"%>
</body>

</html>