<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {
		// 將.table變更為footable
		initFootable();
	});
	
	function getPage(url, breadcrumb, funcId){
    	breadcrumb="";
    	//url  = getPath() + url + "?b=" + encodeURI(encodeURIComponent(breadcrumb));
    	if(url.indexOf("?") != -1){
    		url = url + "b=" + encodeURI(encodeURIComponent(breadcrumb) + "&fcid=" + funcId);
    	}else{
    		url = url + "?b=" + encodeURI(encodeURIComponent(breadcrumb) + "&fcid=" + funcId);
    	}
    	// 超連結
    	window.open(url, '_blank');
    }

	

	//上一頁按鈕
	$("#CMBACK").click(function() {
		var action = '${__ctx}/NT/ACCT/demand_deposit_detail';
		$('#back').val("Y");
		$("form").attr("action", action);
		initBlockUI();
		$("form").submit();
	});
</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Register" /></li>
    <!-- 隨護神盾申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1573" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
<%-- 		<%@ include file="../index/menu.jsp"%> --%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			
			<h2><spring:message code="LB.D1573"/></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="${__ctx}/ONLINE/APPLY/shield_apply_p2">
				<div class="main-content-block row">
					<div class="col-12 ">
<!-- 						<div class="CN19-1-header"> -->
<!-- 							<div class="logo"> -->
<%-- 								<img src="${__ctx}/img/logo-1.svg"> --%>
<!-- 							</div> -->
<!-- 							常用網址超連結 -->
<!-- 							<div class="text-right hyperlink"> -->
<%-- 								<a href="#" onClick="window.open('https://www.tbb.com.tw');" style="text-decoration: none"> <spring:message code="LB.TAIWAN_BUSINESS_BANK"/> </a> <strong><font color="#e65827">|</font></strong>  --%>
<%-- 								<a href="#" onClick="window.open('https://eatm.tbb.com.tw/');" style="text-decoration: none"> <spring:message code="LB.Web_ATM"/> </a><strong><font color="#e65827">|</font></strong> 	 --%>
<%-- 								<a href="#" onClick="window.open('https://www.tbb.com.tw/q3.html?');" style="text-decoration: none"> <spring:message code="LB.Customer_service"/> </a> --%>
<!-- 							</div> -->
<!-- 						</div> -->
						<div class="CN19-1-main text-left">
							<font size="5">
								<B><spring:message code="LB.X0334"/></B><BR>
				      		</font>
				      		<font size="5" color="#FF6600">
					      		<B><spring:message code="LB.X0335"/></B>
				      		</font>
				      		<BR>
					      	<font size="4">
						      	<spring:message code="LB.X0336"/>
					      	</font>
					      	<BR><BR>
					      	<font color="#FF6600" size="5">
					      		<B><spring:message code="LB.X0337"/></B>
				      		</font><BR>
					      	<font size="4">
						      	1.<spring:message code="LB.X0338"/><BR>
						      	2.<spring:message code="LB.X0339"/><BR>
						      	3.<spring:message code="LB.X0340"/>
					      	</font>
					      	<font color="#FF6600" size="3">
					      		<BR>
					      		<B><spring:message code="LB.X0341"/></B>
				      		</font>
						</div>
						<!-- 隨護神盾Q&A -->
						<input type="button" id="QA"class="ttb-button btn-flat-gray" value="<spring:message code="LB.X2068"/>" onclick="getPage('${pageContext.request.contextPath}'+'/ONLINE/APPLY/shield_apply_Q&A','', '')"/>
						<!-- 立即申請 -->
						<input type="button" name="CMSUBMIT" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.X0342"/>" onclick="submit()"/>
					</div>
				</div>
			</form>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>