<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
<br/><br/>
<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
<br/><br/><br/>
<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
<br/><br/>
<c:if test="${RESULT == true}">
<div style="text-align:center">
	<font style="font-weight:bold;font-size:1.2em">
		<spring:message code="LB.W0284" />
	</font>
</div>
</c:if>
<br/>
<table class="print">
	<tr>
		<td style="text-align:center"><spring:message code="LB.Data_time" /></td>
		<td>${CMTXTIME }</td>
	</tr>
	<tr>	
		<td style="text-align:center"><spring:message code="LB.Transfer_date" /></td>
		<td>${CMDATE }</td>
	</tr>
	<tr>	
		<td style="text-align:center"><spring:message code="LB.Payers_account_no" /></td>
		<td>${OUTACN }</td>
	</tr>
	<tr>	
		<td style="text-align:center"><spring:message code="LB.W0407" /></td>
		<td>${TSFACN }</td>		
	</tr>
	<tr>	
		<td style="text-align:center"><spring:message code="LB.Payment_amount" /></td>
		<td><spring:message code="LB.NTD" />${AMOUNT}<spring:message code="LB.Dollar" /></td>
	</tr>
	<tr>	
		<td style="text-align:center"><spring:message code="LB.Transfer_note" /></td>
		<td>${CMTRMEMO}</td>
	</tr>
</table>
<br/><br/>
	<div class="text-left">
		<spring:message code="LB.Description_of_page" />
		<ol class="list-decimal text-left">
			<li><spring:message code="LB.Tuition_Fee_P3_D1" /></li>
			<li><spring:message code="LB.Tuition_Fee_P3_D2" /></li>
			<li><spring:message code="LB.Tuition_Fee_P3_D3" /></li>
			<li><spring:message code="LB.Tuition_Fee_P3_D6" /></li>
			<li><spring:message code="LB.Tuition_Fee_P3_D7" /></li>
		</ol>
	</div>
</body>
</html>