<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
<br/><br/>
<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
<br/><br/><br/>
<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
<br/><br/><br/>
<div style="text-align:center"><spring:message code="LB.Transaction_successful"/></div>
<br/><br/><br/>
<table class="print">
	<tr>
		<th class="text-center"><spring:message code="LB.Trading_time"/></th>
		<th>${CMQTIME}</th>
	</tr>
	<tr>
		<th class="text-center"><spring:message code="LB.W1060" /></th>
		<th>${FDTXTYPE}</th>
	</tr>
	<tr>
		<th class="text-center"><spring:message code="LB.W0025" /></th>
		<th>(${TRANSCODE})&nbsp;${FUNDLNAME}</th>
	</tr>
	<tr>
		<th class="text-center"><spring:message code="LB.X0377" /></th>
		<th>${TRADEDATE_1}</th>
	</tr>
	<tr>
		<th class="text-center"><spring:message code="LB.W1074" /></th>
		<th>${ADCCYNAME}&nbsp;${AMT3_1}</th>
	</tr>
	<tr>
		<th class="text-center"><spring:message code="LB.D0507" /></th>
		<th>${ADCCYNAME}&nbsp;${FCA2_1}</th>
	</tr>
	<tr>
		<th class="text-center"><spring:message code="LB.Payers_account_no"/></th>
		<th>${OUTACN}</th>
	</tr>
	<tr>
		<th class="text-center"><spring:message code="LB.W1079" /></th>
		<th>${ADCCYNAME}&nbsp;${AMT5_1}</th>
	</tr>
	<tr>
		<th class="text-center"><spring:message code="LB.W1034" /></th>
		<th>${FCAFEE_1}%</th>
	</tr>
</table>
<br/><br/>
</body>
</html>