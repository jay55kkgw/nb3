<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
	    <script type="text/javascript">
        $(document).ready(function () {
            initFootable(); // 將.table變更為footable 
            init();
        });

        function init() {
    		$("#TSFACN").children().each(function(){
    		    if ($(this).val()=='${backenData.TSFACN}'){
    		        $(this).prop("selected", true);
    		    }
    		});
    		$("#TYPNUM").children().each(function(){
    		    if ($(this).val()=='${backenData.TYPNUM}'){
    		        $(this).prop("selected", true);
    		    }
    		});
			$("#formId").validationEngine({binded: false,promptPosition: "inline"});
	    	$("#pageshow").click(function(e){
	        	if($('#TYPNUM').val() == '1'){
	        		$('#UNTNUM1').addClass("validate[required,funcCall[validate_CheckNumber['<spring:message code= "LB.W0706" />',UNTNUM1]]");
	        		$('#CUSIDN6').removeClass('validate[required,funcCall[validate_checkSYS_IDNO[CUSIDN6]]');
	        	}else{
	        		$('#CUSIDN6').addClass('validate[required,funcCall[validate_checkSYS_IDNO[CUSIDN6]]');
	        		$('#UNTNUM1').removeClass("validate[required,funcCall[validate_CheckNumber['<spring:message code= "LB.W0706" />',UNTNUM1]]");
	        	}
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
		        	initBlockUI();
		        	var action = '${__ctx}/OTHER/FEE/other_health_insurance_detail';
					$("form").attr("action", action);
	    			$("form").submit();
				}
			});
			$("#CMRESET").click(function(e){
				$("#formId")[0].reset();
				getTmr();
			});
			


			if(${ result_data.data.REC.size() } <= 0)
			{
				//alert("<spring:message code= "LB.Alert168" />");
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert168' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
				$("#pageshow").attr("disabled",true);
				$("#pageshow").removeClass('btn-flat-orange');
			}
			
        }

		function ValidateValue(textbox) {
			var IllegalString = "+-_＿︿%@[`~!#$^&*()=|{}':;',\\[\\].<>/?~！#￥……&*（）——|{}【】‘；：”“'。，、？]‘'";
			var textboxvalue = textbox.value;
			var index = textboxvalue.length - 1;
			var s = textbox.value.charAt(index);
			if (IllegalString.indexOf(s) >= 0) {
				s = textboxvalue.substring(0, index);
				textbox.value = s;
			}
		}
    </script>
</head>
<body>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %> 
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 自動扣繳服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2360" /></li>
    <!-- 健保費代扣繳申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0700" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!-- 預約黃金交易查詢/取消 -->
					<!-- <spring:message code="LB.NTD_Demand_Deposit_Detail" /> -->
					<spring:message code="LB.W0700" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>						
				
                <form id="formId" method="post">
					<input type="hidden" name="action" value="forward">
				    <input type="hidden" name="FLAG" value="1">
				    <input type="hidden" name="TYPE" value="01">
				    <input type="hidden" name="ITMNUM" value="1">
				    <input type="hidden" name="ADOPID" value="N8301">
		            <input type="hidden" name="HLHBRH" value="1"/>
		            <input type=hidden name="UNTNUM6" value=""/>
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
							<div class="ttb-input-block">
								<div class="ttb-message">
	                                <span><spring:message code="LB.W0701" /></span>
	                            </div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0168" />
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<select name="TSFACN" id="TSFACN" class="custom-select select-input half-input  validate[required]">
												<option value="">
													----------<spring:message code="LB.Select" />----------
												</option>
												<c:forEach var="dataList" items="${result_data.data.REC}">
													<option value="${dataList.ACN}"> ${dataList.ACN}</option>
												</c:forEach>
											</select>
										</div>
									</span>
								</div>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.W0716" />
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<select name="TYPNUM" id="TYPNUM" class="custom-select select-input half-input  validate[required]">
												<option value="1">
													<spring:message code="LB.W0704" />
												</option>
												<option value="2">
													<spring:message code="LB.W0705" />
												</option>
											</select>
										</div>
									</span>
								</div>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.W0706" />
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											 <input type="text" id="UNTNUM1" name="UNTNUM1" class="text-input validate[required,funcCall[validate_CheckNumber['<spring:message code= "LB.W0706" />',UNTNUM1]]" value="${backenData.UNTNUM1}" maxLength="10" size="10" />
											<br>
	                                        <span class="input-unit"><spring:message code="LB.W0707" /></span>
										</div>
									</span>
								</div>
								
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.W0708" />
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											 <input type="text" id="CUSIDN1" name="CUSIDN1" class="text-input"  value="${backenData.CUSIDN1}" maxLength="10" size="10" />
											 <br>
	                                         <span class="input-unit"><spring:message code="LB.W0707" /></span>
										</div>
									</span>
								</div>
								
								
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.W0709" />
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											 <input type="text" id="CUSIDN6" name="CUSIDN6" class="text-input" value="${backenData.CUSIDN6}" maxLength="10" size="10" />
											<br>
	                                        <span class="input-unit"><spring:message code="LB.W0710" /></span>
											
										</div>
									</span>
								</div>
							</div>
                           
	                        <!--button 區域 -->
	                            <input id="pageshow" name="pageshow" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.X0080" />" />
                        <!--                     button 區域 -->
                        </div>  
                    </div>
                </form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>