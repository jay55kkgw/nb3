<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
<br/><br/>
<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
<br/><br/><br/>
<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
<br/><br/><br/>
<label><spring:message code="LB.System_time" /> ：</label><label>${CMQTIME}</label>
<br/><br/>

<table class="print">
	<tr>
		<td style="text-align:center"><spring:message code="LB.Status" /></td>
		<td style="text-align:center"><spring:message code="LB.Account" /></td>
		<td style="text-align:center"><spring:message code="LB.Currency" /></td>
		<td style="text-align:center"><spring:message code="LB.Certificate_amount" /></td>
		<td style="text-align:center"><spring:message code="LB.Certificate_no" /></td>
		<td style="text-align:center"><spring:message code="LB.Start_date" /></td>
		<td style="text-align:center"><spring:message code="LB.Expired_date" /></td>
		<td style="text-align:center"><spring:message code="LB.Interest_calculation" /></td>
		<td style="text-align:center"><spring:message code="LB.Interest_rate1" /></td>
		<td style="text-align:center"><spring:message code="LB.Number_of_rotated" /></td>
		<td style="text-align:center"><spring:message code="LB.number_of_unrotated" /></td>
	</tr>
	<c:forEach items="${dataListMap}" var="map">
	<tr>
		<td class="text-center">
			<c:if test="${map.STATUS == '掛失成功'}">
            	<spring:message code="LB.Report_successful" />
            </c:if>
            <c:if test="${map.STATUS != '掛失成功'}">
            	${map.STATUS }
            </c:if>
       	</td>
		<td class="text-center">${map.ACN}</td>
		<td class="text-center">${map.CRY}</td>
		<td class="text-right">${map.AMTFDP}</td>
		<td class="text-center">${map.FDPNO}</td>
		<td class="text-center">${map.DPISDT}</td>
		<td class="text-center">${map.DUEDAT}</td>
		<td class="text-center"><spring:message code="${map.INTMTH}" /></td>
		<td class="text-right">${map.ITR}</td>
		<td class="text-center">${map.ILAZLFTM}</td>
		<td class="text-center">
			<c:if test="${map.AUTXFTM == '無限次'}">
            	<spring:message code="LB.Unlimited" />
           	</c:if>
            <c:if test="${map.AUTXFTM != '無限次'}">
            	${map.AUTXFTM }
            </c:if>
		</td>
	</tr>
	</c:forEach>
</table>
<br/><br/>
	<div class="text-left">
		<spring:message code="LB.Description_of_page" />
		<ol class="list-decimal text-left">
			<li><spring:message code="LB.Demand_Deposit_slip_loss_fcy_P1_D1" />
          	<li><spring:message code="LB.Demand_Deposit_slip_loss_fcy_P1_D2" />
          	<li><spring:message code="LB.Demand_Deposit_slip_loss_fcy_P1_D3" />
          	<li><spring:message code="LB.Demand_Deposit_slip_loss_fcy_P1_D4" />
		</ol>
	</div>
</body>
</html>