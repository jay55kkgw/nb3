<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<!--舊版驗證-->
<script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	$("#formID").validationEngine({binded: false,promptPosition: "inline"});
	$("#CMSUBMIT").click(function(e){
		e = e || window.event;
		if(!$('#formID').validationEngine('validate')){
			e.preventDefault();
		}
		else{
// 			if(!confirm("<spring:message code='LB.Confirm004' />"+"${result_data.data.TRADEDATEFormat}"+"\n<spring:message code='LB.Confirm005' />"))
// 	    		return false;
			
			if(!CheckPuzzle("CMPWD")){
				return false;
			}
			
			if($("#riskConfirmCheckBox").prop("checked") == false){
				errorBlock(
						null, 
						null,
						['<spring:message code="LB.Alert092" />'], 
						'<spring:message code="LB.Quit" />', 
						null
				);
				return;
			}
			
			$("#CMPASSWORD").val($("#CMPWD").val());
			
			var PINNEW = pin_encrypt($("#CMPASSWORD").val());
			$("#PINNEW").val(PINNEW);
			
			$("#formID").attr("action","${__ctx}/FUND/RESERVE/TRANSFER/fund_reserve_transfer_result");
			$("#formID").submit();
		}
	});
	$("#resetButton").click(function(){
		$("#CMPWD").val("");
	});
	$("#cancelButton").click(function(){
		$("#formID").attr("action","${__ctx}/FUND/TRANSFER/query_fund_transfer_data");
		$("#formID").submit();
	});
});
</script>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	<!--麵包屑-->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><a href="#"><spring:message code= "LB.Funds" /></a></li>
			<li class="ttb-breadcrumb-item"><a href="#"><spring:message code= "LB.W1064" /></a></li>
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code= "LB.W1062" /></li>
		</ol>
	</nav>
	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
				<h2><spring:message code= "LB.W1062" /></h2>
				<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
				
					<form id="formID" method="post">
						<input type="hidden" name="ADOPID" value="C032"/>
						<input type="hidden" id="CMPASSWORD" name="CMPASSWORD"/>
						<input type="hidden" id="PINNEW" name="PINNEW"/>
						<input type="hidden" name="TYPE" value="01"/>
						<input type="hidden" name="INTSACN" value="1111111111111111"/>
						<input type="hidden" name="TRANSCODE" value="${result_data.data.TRANSCODE}"/>
						<input type="hidden" name="CDNO" value="${result_data.data.CDNO}"/>
						<input type="hidden" name="TRADEDATE" value="${result_data.data.TRADEDATE}"/>
						<input type="hidden" name="INTRANSCODE" value="${result_data.data.INTRANSCODE}"/>
						<input type="hidden" name="UNIT" value="${result_data.data.UNIT}"/>
						<input type="hidden" name="FCA1" value="${result_data.data.FCA1}"/>
						<input type="hidden" name="AMT3" value="${result_data.data.AMT3}"/>
						<input type="hidden" name="FCA2" value="${result_data.data.FCA2}"/>
						<input type="hidden" name="AMT5" value="${result_data.data.AMT5}"/>
						<input type="hidden" name="OUTACN" value="${result_data.data.OUTACN}"/>
						<input type="hidden" name="BILLSENDMODE" value="${result_data.data.BILLSENDMODE}"/>
						<input type="hidden" name="SSLTXNO" value="${result_data.data.SSLTXNO}"/>
						<input type="hidden" name="FUNDAMT" value="${result_data.data.FUNDAMTNoComma}"/>
						<input type="hidden" name="PRO" value="${result_data.data.PRO}"/>
						<input type="hidden" name="RSKATT" value="${result_data.data.RSKATT}"/>
						<input type="hidden" name="RRSK" value="${result_data.data.RRSK}"/>
						<input type="hidden" name="SHORTTRADE" value="${result_data.data.SHORTTRADE}"/>
						<input type="hidden" name="SHORTTUNIT" value="${result_data.data.SHORTTUNIT}"/>
						<input type="hidden" name="FDAGREEFLAG" value="${result_data.data.FDAGREEFLAG}"/>
						<input type="hidden" name="FDNOTICETYPE" value="${result_data.data.FDNOTICETYPE}"/>
						<input type="hidden" name="FDPUBLICTYPE" value="${result_data.data.FDPUBLICTYPE}"/>
						<input type="hidden" name="SHWD" value="${result_data.data.SHWD}">
						<input type="hidden" name="XFLAG" value="${result_data.data.XFLAG}">
						<input type="hidden" name="FEE_TYPE" value="${result_data.data.FEE_TYPE}">
						<div class="main-content-block row">
						<div class="col-12">

							<div class="ttb-input-block">
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.Id_no"/>
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${result_data.data.hiddenCUSIDN}
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.Name"/>
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${result_data.data.hiddenNAME}
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.W1111"/>
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${result_data.data.hiddenCDNO}
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.W0025"/>
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											（${result_data.data.TRANSCODE}）${result_data.data.fundName}
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.W0950"/>
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											（${result_data.data.INTRANSCODE}）${result_data.data.inFundName}
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.W1115"/>
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${result_data.data.BILLSENDMODEChinese}
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.W1122"/>
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${result_data.data.UNITDot}
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.W1116"/>
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<c:if test="${__i18n_locale eq 'en' }">
				                   				${result_data.data.ADCCYENGNAME}&nbsp;&nbsp;
				                    		</c:if>
				                    		<c:if test="${__i18n_locale eq 'zh_TW' }">
				                   				${result_data.data.ADCCYNAME}&nbsp;&nbsp;
				                    		</c:if>
				                    		<c:if test="${__i18n_locale eq 'zh_CN' }">
				                   				${result_data.data.ADCCYCHSNAME}&nbsp;&nbsp;
				                    		</c:if>${result_data.data.FUNDAMTFormat}
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.W1123"/>
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<spring:message code="LB.NTD" /> ${result_data.data.AMT3Integer}
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.W1125"/>
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<spring:message code="LB.NTD" /> ${result_data.data.FCA2Format}
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.W0974"/>
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<spring:message code="LB.NTD" /> ${result_data.data.FCA1Format}
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.D0168"/>
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${result_data.data.OUTACN}
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.W1079"/>
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<spring:message code="LB.NTD" /> ${result_data.data.AMT5}
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
										   <label><h4><spring:message code= "LB.W1143" /></h4></label>
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span class='high-light'>${result_data.data.TRADEDATEFormat}</span>
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.Transaction_security_mechanism"/>
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
												<input type="radio" name="FGTXWAY" id="CMSSL" value="0" checked/><spring:message code= "LB.SSL_password_1" />（SSL）&nbsp;
												<span class="ttb-radio"></span>
											</label>
	                             			<input type="password" class="text-input validate[required, pwvd[PW, CMPWD], custom[onlyLetterNumber]]" id="CMPWD" name="CMPWD" size="8" maxlength="8"/>
										</div>
									</span>
								</div>
								<c:if test="${result_data.data.FEE_TYPE == 'A'}" >
                            	    <div class="ttb-input-item row">
                            	    <span class="input-title">
                                	</span>
                            	    <span class="input-block" >
	                                    <div class="ttb-input">
                            	    	<label class="check-block">
                                       	<input type="checkbox" id="riskConfirmCheckBox"/><strong><spring:message code= "LB.X2493" /><font color="red"><spring:message code= "LB.X2495" /></font></strong>
										<span class="ttb-check"></span>
										</label>
										</div>
									</span>
									</div>
                                </c:if>
							</div>
<!-- 							<div> -->
<!-- 								<p style="text-align:left;"> -->
<%-- 									<spring:message code= "LB.fund_redeem_data_P4_D1" />：<br/><br/> --%>
<%-- 									<strong><spring:message code= "LB.Fund_Redeem_Data_P2_D1" /></strong><br/> --%>
<%-- 									<strong><spring:message code= "LB.Fund_Redeem_Data_P2_D2" /></strong><br/> --%>
<!-- 								</p> -->
<!-- 								<p style="text-align:left;"> -->
<%-- 									<spring:message code= "LB.Fund_Redeem_Data_P2_D3-1" />「<a href="https://nnb.tbb.com.tw/TBBNBAppsWeb/fund/index.html" target="_blank"><spring:message code= "LB.Fund_Redeem_Data_P2_D3-2" /></a><spring:message code= "LB.Fund_Redeem_Data_P2_D3-3" />	 --%>
<!-- 								</p> -->
<!-- 							</div> -->
							
								<input type="button" id="cancelButton" value="<spring:message code="LB.Back_to_function_home_page" />" class="ttb-button btn-flat-gray"/>
<%-- 								<input type="button" id="resetButton" value="<spring:message code="LB.Re_enter"/>" class="ttb-button btn-flat-gray"/> --%>
								<input type="button" id="CMSUBMIT" value="<spring:message code="LB.Confirm"/>" class="ttb-button btn-flat-orange"/>
					
						</div>
					</div>
				</form>
					
				<div class="text-left">
					
						<ol class="list-decimal text-left description-list">
							<p><spring:message code="LB.Description_of_page"/></p>
							<li><span><spring:message code= "LB.fund_redeem_data_P4_D1" />：<br>
								<ol>
									<li style="text-indent:-43px;">（1）<span><font color="red"><spring:message code= "LB.fund_redeem_data_P4_D2" /></font></span></li>
									<li style="text-indent:-43px;">（2）<span><font color="red"><spring:message code= "LB.fund_redeem_data_P4_D4" /></font></span></li>
								</ol>
								</span></li>
									
							<li><span><spring:message code= "LB.fund_redeem_data_P4_D5" /><a href="https://nnb.tbb.com.tw/TBBNBAppsWeb/fund/index.html" target="_blank"><spring:message code= "LB.fund_redeem_data_P4_D5-1" /></a><spring:message code= "LB.fund_redeem_data_P4_D5-2" /></span></li>
						</ol>
					</div>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>