<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	
	$("#print").click(function(){
		var params = {
			"jspTemplateName":"withholding_cancel_C_result_print",
			"jspTitle":'<spring:message code="LB.W0768"/>',
			"CMQTIME":"${result_data.data.CMQTIME}",
			"CARDNUM":"${input_data.CARDNUM}",
			"TYPE_str":"${input_data.TYPE_str}",
			"CUSNUM":"${input_data.CUSNUM}",
		};
		openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
	});
});
</script>
</head>
<body>
	<!--   IDGATE -->
	<%@ include file="../idgate_tran/idgateAlert.jsp" %>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 自動扣繳服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2360" /></li>
    <!-- 自動扣繳查詢/取消     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0768" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2><spring:message code="LB.W0768" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				
				<form id="formId" method="post" action="">
					<input type="hidden" id="TOKEN" name="TOKEN" value="${input_data.TOKEN}">
					
					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-input-block">
								<div class="ttb-message">
	                                <span><spring:message code="LB.D0183" /></span>
	                            </div>
								
								<!-- 交易機制 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.Inquiry_time" />
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p> ${ result_data.data.CMQTIME }</p>
										</div>
									</span>
								</div>
								<!-- 卡號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.Card_number" />
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p> ${ input_data.CARDNUM }</p>
										</div>
									</span>
								</div>
								<!-- 業務項目 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.X2265" />
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p> 
												<c:if test="${!input_data.TYPE_str.equals('')}">
													<spring:message code="${input_data.TYPE_str}" />
												</c:if>
											</p>
										</div>
									</span>
								</div>
								<!-- 用戶編號-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.W0691" />
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p> ${ input_data.CUSNUM }</p>
										</div>
									</span>
								</div>
								
							</div>
							<!-- 確定-->
							<input type="button" id="print" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Print" />" />
						</div>
					</div>
					<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /></p><!-- 說明 -->
						<li><spring:message code= "LB.Withhold_Cancel_P5_D1" /></li>
						<li><spring:message code= "LB.Withhold_Cancel_P5_D2" /></li>
					</ol>
				</form>
			</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
								
</body>
</html>