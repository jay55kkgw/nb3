<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<script type="text/javascript">
		$(document).ready(function () {});
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 空白票據     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0528" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!--  空白票據申請 -->
					<spring:message code="LB.D0528" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="">
					<c:set var="BaseResultData" value="${blank_bill_result.data}"></c:set>
					<!-- 表單顯示區 -->
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<div class="ttb-message">
									<span>
										<spring:message code="LB.D0542"/>
									</span>
								</div>
								<!--帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.Account"/>
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span> ${BaseResultData.ACN}</span>
										</div>
									</span>
								</div>
								<!-- 身份證/統一編號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.D0531"/>
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span> ${BaseResultData.CUSIDN}</span>
										</div>
									</span>
								</div>
								<!-- 票據種類 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.D0532" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span> ${BaseResultData.CHKTYPE}</span>
										</div>
									</span>
								</div>
								<!-- 票據格式 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.X0366" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span> ${BaseResultData.CHKFORM}</span>
										</div>
									</span>
								</div>
								<!-- 申請張數 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.D0537"/>
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span> ${BaseResultData.APPLYNUM}</span>
										</div>
									</span>
								</div>
								<!-- 作廢票據總張數 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.D0538"/>
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${BaseResultData.CHKCNT}</span>
										</div>
									</span>
								</div>
								<!-- 聯絡電話 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.D0539"/>
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${BaseResultData.TEL}</span>
										</div>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="text-left">
					</div>
				</form>
			</section>
			<!-- 		main-content END -->
		</main>
		<!-- 	content row END -->
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>