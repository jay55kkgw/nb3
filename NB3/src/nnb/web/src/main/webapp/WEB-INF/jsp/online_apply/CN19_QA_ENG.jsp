<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

	<script type="text/javascript">
		$(function(){
			// 幫 #qaContent 的 ul 子元素加上 .accordionPart
			// 接著再找出 li 中的第一個 div 子元素加上 .qa_title
			// 並幫其加上 hover 及 click 事件
			// 同時把兄弟元素加上 .qa_content 並隱藏起來
			$('#qaContent ul').addClass('accordionPart').find('li div:nth-child(1)').addClass('qa_title').hover(function(){
				$(this).addClass('qa_title_on');
			}, function(){
				$(this).removeClass('qa_title_on');
			}).click(function(){
				// 當點到標題時，若答案是隱藏時則顯示它，同時隱藏其它已經展開的項目
				// 反之則隱藏
				var $qa_content = $(this).next('div.qa_content');
				if(!$qa_content.is(':visible')){
					$('#qaContent ul li div.qa_content:visible').slideUp();
				}
				$qa_content.slideToggle();
			}).siblings().addClass('qa_content').hide();
		});
	</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Register" /></li>
    <!-- 隨護神盾     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0343" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<!-- 申請隨護神盾 -->
			<h2><spring:message code="LB.D1573"/></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="">
				<div class="main-content-block row">
					<div class="col-12">
<!-- 						<div class="CN19-1-header"> -->
<!-- 							<div class="logo"> -->
<%-- 								<img src="${__ctx}/img/logo-1.svg"> --%>
<!-- 							</div> -->
<!-- 							常用網址超連結 -->
<!-- 							<div class="text-right hyperlink"> -->
<!-- 							臺灣企銀首頁 -->
<%-- 								<a href="#" onClick="window.open('https://www.tbb.com.tw');" style="text-decoration: none"> <spring:message code="LB.TAIWAN_BUSINESS_BANK"/> </a> <strong><font color="#e65827">|</font></strong>  --%>
<!-- 								網路ATM -->
<%-- 								<a href="#" onClick="window.open('https://eatm.tbb.com.tw/');" style="text-decoration: none"> <spring:message code="LB.Web_ATM"/> </a><strong><font color="#e65827">|</font></strong> 	 --%>
<!-- 								意見信箱  -->
<%-- 								<a href="#" onClick="window.open('https://www.tbb.com.tw/q3.html?');" style="text-decoration: none"> <spring:message code="LB.Customer_service"/> </a> --%>
<!-- 							</div> -->
<!-- 						</div> -->
						<!-- 數位存款帳戶補申請晶片金融卡 -->
						<br><br>
						<div id="qaContent">
							<ul>
								<li>
									<div>Q1. What is the "TBB Security Code"？</div>
									<div>The "TBB Security Code" is combined with a smart device for identity authentication. The customer can confirm the transaction by entering a password, and the transaction can be easily completed with the mobile device anytime and anywhere.
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q2. What are the functions of "TBB Security Code"？</div>
									<div>
										Provide customers with the following services in the Mobile Banking：<br> 
										1. Non-contracted transfer<br> 
										2. Taiwan Pay Action Payment<br>
										3. No card withdrawal function<br> 
										4. Payment of tuition and fees and enterprise charge
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q3. What is the "download password" used for？</div>
									<div>Used when the mobile device is bound to the "Mmobile Bankung" for downloading password.</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q4. What is the password used for？</div>
									<div>The password set when the voucher is downloaded is used by the mobile banking to verify the transaction through the "TBB Security Code".</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q5. Who can apply for？</div>
									<div>One who has applied for TBB internet-banking and activate mobile-banking, and set up the transfer-out account.</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q6. How to apply？</div>
									<div>
										1.TBB online application area: I applied for the chip financial card of my bank to the application page and activated it to the Mobile Banking within 48 hours.( Limited to individual )<BR>
										&nbsp;&nbsp;(1) Select to apply for "TBB Security Code"<BR> 
										&nbsp;&nbsp;(2) Set 6~8 digits of "Download Password"<BR> 
										&nbsp;&nbsp;(3) Insert the Bank's chip financial card and enter the financial card password for verification<BR>
										&nbsp;&nbsp;(4) Login The Mobile Banking selects "TBB Security Code", enter "Download Password" and set "Password". After the certificate is successfully downloaded, the activation process is completed. <BR> 
										2.Internet Banking: Log in to the general Internet Bank and select "Application / Reporting Service". Click "Apply for "TBB Security Code"" and use my own chip financial card verification application, and activate it within 48 hours to the Mobile Banking. ( Limited to individual) Third, the cabinet: Pro-encognition documents and the original seal, contact the original online banking account ( Note ) , and within 30 days to the Mobile Banking to activate.<BR>
										3.Arrival of Business Spot: take your ID card/documents and the original seal, contact the original online banking account ( Note ) , and within 30 days to the Mobile Banking to activate.
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q7. How to terminate the "TBB Security Code" service？</div>
									<div>
										1. Arrival of Business Spot: take your ID card/documents and the original seals to check with the original online banking account ( Note )<BR> 
										2.Customer Service : call customer service line 0800-01-7171 , 02-2357-7171 press 3 ( mobile phone please dial the phone ) , contact customer service staff to terminate ( limited to individual ) .
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q8. What should I do if I replace or lose my mobile phone？</div>
									<div>
										When replacing the mobile phone, set the replacement device download password through the old mobile phone, and then download it with the new mobile phone according to the program.<BR>
										If the mobile phone is lost and wants to terminate the "TBB Security Code" service, you can terminate it at the original online banking bank ( Note ) or contact customer service personnel ( limited to individual ) .
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q9. What is the limit of continuous error with "TBB Security Code"？</div>
									<div>
										In order to prevent people who are interested in testing at will, the password will be invalid when the "Download Password" is incorrectly 5 times, and you need to apply for "TBB Security Code". The "care password" will automatically terminate the service when it is 5 consecutive errors .
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q10. What should I do after the "TBB Security Code" download password exceeds the aging time？</div>
									<div>Re-apply to the Bank online application area ( limited to individual ) , general online banking ( limited to individual ) or the original online banking account bank ( Note ) application.</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q11. Can I reapply after I stop the "TBB Security Code"？</div>
									<div>
										After terminating the "TBB Security Code" service, you can apply online ( limited to individual ) , general online banking ( limited to individual ) or the original online banking account to re-apply ( Note ) .
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q12. What should I do if I forget to download the password, the password or the password for 5 consecutive errors？</div>
									<div>
										1. When you forget the download password: You can directly apply for a new application in the bank's online application area (for natural persons only), general online banking (for natural persons only) or the original online banking account opening bank (Note). <BR>
										2. When you forget the guardian password or the password is incorrect for 5 consecutive times: <BR>
										&nbsp;&nbsp;(1) If the validity period of the certificate has not expired, it is necessary to terminate the Aegis Aegis service before applying again (see Q7. How to terminate TBB Security Code service?). <BR>
										&nbsp;&nbsp;(2) If the certificate has expired, you can directly apply for a new application in the bank's online application area (for natural persons only), general online banking (for natural persons), or online banking account opening bank (Note)<BR>
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q13. How many "TBB Security Code" can anyone apply for？</div>
									<div>"TBB Security Code" is assigned to the identity card number, and each person can only bind one mobile device.</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q14. Does it cost to apply/renew for the "TBB Security Code"？</div>
									<div>
										The annual fee for "TBB Security Code" is free .
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q15. Is there a limit on the amount of transactions for non-contracted account transfers with "TBB Security Code"？</div>
									<div>
										1. The default limit: NT$ 50,000 per copy , NT$ 100,000 per day, NT$ 200,000 per month .<BR> 
										2. This limit is calculated by combining the transfer transactions of the chip financial card and the dynamic password card.
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q16. Is there a time limit for the "TBB Security Code"？</div>
									<div>
										Apply for the "TBB Security Code", the period of use is one year from the date of download. Within one month before the expiration of "TBB Security Code", the system automatically prompts the renewal message with eMail . The customer must update the application within the time limit. If the renewal period is unsuccessful, please re-apply for the "TBB Security Code" service.
									</div>
								</li>
							</ul>
							<ul>
								<li>
									<div>Q17. I received a notification letter about the expiration of the "TBB Security Code" , how do I update ?</div>
									<div>
										Please log in to our mobile bank within one month before it expires, click on "Aegis" and follow the instructions to update .
									</div>
								</li>
							</ul>
							Note: Individuals who have applied for bank-wide payment can go to any branch of Taiwan Enterprise Bank, and the legal person is limited to the bank.
						</div>
					</div>
				</div>
			</form>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>