<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript" src="${__ctx}/js/jquery.maskedinput.js"></script>
<!-- 交易機制所需JS -->
<script type="text/javascript" src="${__ctx}/component/util/checkIdentity.js"></script>
<!-- DIALOG會用到 -->
<script type="text/javascript" src="${__ctx}/js/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery-ui.css">

<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
<!-- 讀卡機所需JS -->
<script type="text/javascript" src="${__ctx}/component/util/VAckisIEbrower.js"></script>
<script type="text/javascript" src="${__ctx}/component/util/Pkcs11ATLAgent.js"></script>

<style type="text/css">
#bankTable{
	border:solid 2px;
}
#bankTable tr{
	border:solid 2px;
}
#bankTable td{
	border:solid 2px;
}
</style>
<script type="text/javascript">
//自然人憑證的物件
var notCheckIKeyUser = true; // 不驗證是否是IKey使用者
// var myobj = null; // 自然人憑證用

$(document).ready(function(){
	// HTML載入完成後開始遮罩
// 	setTimeout("initBlockUI()", 10);
	initBlockUI();
	// 開始查詢資料並完成畫面
	setTimeout("init()", 20);
	// 解遮罩
	setTimeout("unBlockUI(initBlockId)", 500);
});
	
function init(){
	$("#formId").validationEngine({
		binded: false,
		promptPosition: "inline"
	});
	initNatural();
	$("#hideblock").hide();
	var Year = new Date().getFullYear();
	
	var EXPIREDYEARHTML = "<option value='#'>---<spring:message code="LB.X1897" />---</option>";
	for(var x=0;x<10;x++){
		EXPIREDYEARHTML += "<option value='" + Year + "'>" + Year + "</option>";
		
		Year = parseInt(Year) + 1;
	}
	
	var cusidn = '${result_data.data.CUSIDN}';
	if(cusidn != null && cusidn != ''){
		$("#CUSIDN").val(cusidn);
	}
// 	$("#EXPIREDYEAR").html(EXPIREDYEARHTML);
	
// 	$("#cooperBank").click(function(){
// // 	    $("#bankDialog").dialog("open");
// 	    $("#bankDialog").show();
// 	});
// 	$("#bankDialog").dialog({
// 		autoOpen:false,
// 		modal:true,
// 		closeOnEscape:true,
// 		width:850,
// 		maxHeight:570,
// 		position:{
// 			my:"center",at:"top-500px",of:window
// 		}
// 	});
// 	changeCode();
	birthday();
	initKapImg();
	newKapImg();
	
	if(!checkBrowser()){
		$("#pcDIV").prop("style","display:none");
		$("#pcDIV2").prop("style","display:none");
	}
	$("#CMSUBMIT").click(function(e){
		var FGTXWAY = $("input[name=FGTXWAY]:checked").val();
		if($("#allCheckBox").prop("checked") == false){
			errorBlock(null, null, ["<spring:message code='LB.onlineapply_creditcard_PIPN' />"],
					'<spring:message code= "LB.Confirm" />', null);
		}else{
			$("#CUSIDN").val($("#CUSIDN").val().toUpperCase());
			var reg = new RegExp("\/","g");
			var chk = $("#CARDNUM").val();
			chk = chk.replace(reg,"");
			$("#CARDNUMCHK").val(chk);
			if(FGTXWAY == "3"){
				var craddate = $("#EXPIREDYM").val();
				$("#EXPIREDYEAR").val(craddate.substring(3));
				$("#EXPIREDDATEMONTHS").val(craddate.substring(0,2));
			}
			$("#hideblock").show();
			e = e || window.event;
			if(!$('#formId').validationEngine('validate')){
	    		e.preventDefault();
	    	}
			else{
				okFunction();
			}
		}
	});
}

function test(){
	$("#bankDialog").show();
}

function birthday(){
	var Today = new Date();
	var y = Today.getFullYear();
	var m = (Today.getMonth()+1<10 ? '0' : '')+(Today.getMonth()+1);
	var d = (Today.getDate()<10 ? '0' : '')+Today.getDate();
	y = y - 1911;
	var min_y = y-20;
	var max_y = y-100;
	
	for(var i=min_y;i>=max_y;i--){
		var YYtext = i;
		if(i.toString().length < 2){
			YYtext = "00"+YYtext;
		}
		else if(i.toString().length < 3){
			YYtext = "0"+YYtext;
		}
		//民國年
// 		$("#YY").append( $("<option></option>").attr("value", YYtext).text("<spring:message code='LB.D0583' />" + i + "<spring:message code='LB.Year' />"));
		//西元年
		if('${transfer}' == 'en'){
			$("#YY").append( $("<option></option>").attr("value", YYtext).text( i + 1911));
		}
		else{
			$("#YY").append( $("<option></option>").attr("value", YYtext).text((i + 1911) + "年"));
		}
	}
	orderby();
// 	$("#YY").val(y);
	for(var i=1;i<=12;i++){
		var j = i;
		if(j<10){
			j = "0"+j;
		}
		//月
		if('${transfer}' == 'en'){
			switch (j) {
			case '01':
			　$("#MM").append( $("<option></option>").attr("value", j).text("January"));
			　break;
			case '02':
			　$("#MM").append( $("<option></option>").attr("value", j).text("February"));
			　break;
			case '03':
			　$("#MM").append( $("<option></option>").attr("value", j).text("March"));
			　break;
			case '04':
			　$("#MM").append( $("<option></option>").attr("value", j).text("April"));
			　break;
			case '05':
			　$("#MM").append( $("<option></option>").attr("value", j).text("May"));
			　break;
			case '06':
			　$("#MM").append( $("<option></option>").attr("value", j).text("June"));
			　break;
			case '07':
			　$("#MM").append( $("<option></option>").attr("value", j).text("July"));
			　break;
			case '08':
			　$("#MM").append( $("<option></option>").attr("value", j).text("August"));
			　break;
			case '09':
			　$("#MM").append( $("<option></option>").attr("value", j).text("September"));
			　break;
			case 10:
			　$("#MM").append( $("<option></option>").attr("value", j).text("October"));
			　break;
			case 11:
			　$("#MM").append( $("<option></option>").attr("value", j).text("November"));
			　break;
			case 12:
			　$("#MM").append( $("<option></option>").attr("value", j).text("December"));
			　break;
			default:
			　x="沒有符合的條件";
			}
		}
		else{
			$("#MM").append( $("<option></option>").attr("value", j).text(j + "<spring:message code='LB.Month' />"));
		}
	}
// 	$("#MM").val(m);
	for(var i=1;i<=31;i++){
		var j = i;
		if(j<10){
			j = "0"+j;
		}
		//日
		if('${transfer}' == 'en'){
			$("#DD").append( $("<option></option>").attr("value", j).text(j));
		}
		else{
			$("#DD").append( $("<option></option>").attr("value", j).text(j + "<spring:message code='LB.D0586' />"));
		}
	}
// 	$("#DD").val(d);
}
function changeday(){
	var year = ($("#YY").val()+1911);
	var month = $("#MM").val();
	console.log(year+"/"+month);
	//alert(year+"/"+month);
	errorBlock(
			null, 
			null,
			[year+"/"+month], 
			'<spring:message code= "LB.Quit" />', 
			null
		);
}

function openinput(){
	//自然人憑證
	if($("#CMNPC").prop("checked") == true){
		$("#CARDNUM").val("");
		$("#EXPIREDYM").val("");
		$("#CVCTWO").val("");
		$("#EXPIREDYEAR").val("");
		$("#EXPIREDDATEMONTHS").val("");
		$("#NPCPIN").addClass("validate[required]");
// 		$("#CARDNUM").removeClass("validate[required]");
		$("#CARDNUM").removeClass("validate[required,funcCall[validate_CheckNumWithDigit[<spring:message code='LB.W0639' />,CARDNUMCHK,16]]]");
		$("#EXPIREDYM").removeClass("validate[required,funcCall[validate_chkperiod['sFieldName',EXPIREDYEAR,EXPIREDDATEMONTHS]]]");
		$("#CVCTWO").removeClass("validate[required,funcCall[validate_CheckNumber[<spring:message code='LB.X1971' />,CVCTWO]]]");
		$("#row1").show();
		$("#row2").hide();
	}
	//晶片金融卡
	if($("#CMCARD").prop("checked") == true){
		$("#NPCPIN").val("");
		$("#CARDNUM").val("");
		$("#EXPIREDYM").val("");
		$("#CVCTWO").val("");
		$("#EXPIREDYEAR").val("");
		$("#EXPIREDDATEMONTHS").val("");
		
		$("#NPCPIN").removeClass("validate[required]");
// 		$("#CARDNUM").removeClass("validate[required]");
		$("#CARDNUM").removeClass("validate[required,funcCall[validate_CheckNumWithDigit[<spring:message code='LB.W0639' />,CARDNUMCHK,16]]]");
		$("#EXPIREDYM").removeClass("validate[required,funcCall[validate_chkperiod['sFieldName',EXPIREDYEAR,EXPIREDDATEMONTHS]]]");
		$("#CVCTWO").removeClass("validate[required,funcCall[validate_CheckNumber[<spring:message code='LB.X1971' />,CVCTWO]]]");
		$("#row1").hide();
		$("#row2").hide();
	}	
	//它行信用卡
	if($("#CMCREDIT").prop("checked") == true){
		$("#NPCPIN").val("");
		$("#NPCPIN").removeClass("validate[required]");
// 		$("#CARDNUM").addClass("validate[required]");
		$("#CARDNUM").addClass("validate[required,funcCall[validate_CheckNumWithDigit[<spring:message code='LB.W0639' />,CARDNUMCHK,16]]]");
		$("#EXPIREDYM").addClass("validate[required,funcCall[validate_chkperiod['sFieldName',EXPIREDYEAR,EXPIREDDATEMONTHS]]]");
		$("#CVCTWO").addClass("validate[required,funcCall[validate_CheckNumber[<spring:message code='LB.X1971' />,CVCTWO]]]");
		$("#row1").hide();
		$("#row2").show();
	}
}

// 刷新驗證碼
function refreshCapCode() {
	console.log("refreshCapCode...");

	// 驗證碼
	$('input[name="capCode"]').val('');
	
	// 大小版驗證碼用同一個
	$('img[name="kaptchaImage"]').hide().attr(
			'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

	// 登入失敗解遮罩
	unBlockUI(initBlockId);
}

// 刷新輸入欄位
function changeCode() {
	console.log("changeCode...");
	
	// 清空輸入欄位
	$('input[name="capCode"]').val('');
	
	// 刷新驗證碼
	refreshCapCode();
}

// 初始化驗證碼
function initKapImg() {
	// 大小版驗證碼用同一個
	$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
}

// 生成驗證碼
function newKapImg() {
	// 大小版驗證碼用同一個
	$('img[name="kaptchaImage"]').click(function() {
		refreshCapCode();
	});
}

// function changeCode(){
// 	$("#capCode").val("");
// 	$("#kaptchaImage").hide().attr("src","${__ctx}/CAPCODE/captcha_image_trans?" + Math.floor(Math.random() * 100)).fadeIn();
// }
function yyyymmdd(date){
	var yyyy = date.getFullYear();
	var mm = date.getMonth() + 1;//getMonth() is zero-based
	var dd  = date.getDate();
	
	return String(10000 * yyyy + 100 * mm + dd);// Leading zeros for mm and dd
}
//控制信用卡卡號當輸入完一個欄位，游標移動到下一個欄位
function setBlur(obj,target2){
        
    var target=document.getElementById(target2);    
    if(obj.value.length == obj.getAttribute('maxlength')){
        target.focus();
    }
    
    return;
}
var urihost = "${__ctx}";
//	initNatural();
// var isconfirm = false;
// 		$("#errorBtn1").click(function(){
// 			if(!isconfirm)
// 				return false;
// 			isconfirm = false;
// 			$("#naturalComponent")[0].click();
// 		});
// 		$("#errorBtn2").click(function(){
// 			isconfirm = false;
// 			$('#error-block').hide();
// 		});
// // 初始化自然人元件
// function initNatural() {
// 	var Brow1 = new ckBrowser1();
// 	if(!Brow1.isIE){	
// 		if((navigator.userAgent.indexOf("Chrome") > -1) || (navigator.userAgent.indexOf("Firefox") > -1) || (navigator.userAgent.indexOf("Safari") > -1)) {
// 			myobj = initCapiAgentForChrome();
// 			try {
// 				if(sessionToken == null){
// 					myobj.initChrome();
// 					console.log("myobj.initChrome...");
// 				}
// 			} catch (e) {
// // 				if(confirm("<spring:message code= "LB.X1216" />")){
// // 					$("#naturalComponent")[0].click();
// // 				}
//                 isconfirm = true;
// 				errorBlock(
// 						null, 
// 						null,
// 						['<spring:message code= "LB.X1216" />'], 
// 						'<spring:message code= "LB.Confirm" />', 
// 						'<spring:message code= "LB.Cancel" />'
// 					);

// 			}			
// 		}
// 	} else {
// 		var strObject = "";
//     	// 使用 Script 判斷 win64 或 win32, 執行時間有點久
//  		if (navigator.platform.toLowerCase() == "win64"){
// 			// "Windows 64 位元";
//  			document.getElementById("obj").outerHTML = '<object id="myobj" classid="CLSID:32FDE779-F9DC-4D39-97FB-434102000101" codebase="${__ctx}/component/windows/natural/TBBankPkcs11ATLx64.cab" width="0px" height="0px"></object>';
// 		} else {
// 			// "Windows 32 位元 或 模擬 64 位元(WOW64)";
// 			document.getElementById("obj").outerHTML = '<object id="myobj" classid="CLSID:32FDE779-F9DC-4D39-97FB-434102000101" codebase="${__ctx}/component/windows/natural/TBBankPkcs11ATL.cab" width="0px" height="0px"></object>';
// 		}
// 	    myobj = document.myobj;
// 	}
// 	console.log("initNatural.finish...");
// }

function okFunction(){
	$("#formId").validationEngine('detach');
	$("#CPRIMBIRTHDAY").val($("#YY").val()+""+$("#MM").val()+""+$("#DD").val());
	$("#CPRIMBIRTHDAYshow").val($("#YY").find(':selected').text()+" "+$("#MM").find(':selected').text()+" "+$("#DD").find(':selected').text());
//		$("#CUSIDN").val($("#CUSIDN").val().toUpperCase());
	var CUSIDN = $("#CUSIDN").val();
	CUSIDN = CUSIDN.toUpperCase();
	$("#UID").val(CUSIDN);
	
	var FGTXWAY = $("input[name=FGTXWAY]:checked").val();
	
	
	//卡片有效日期
	if(FGTXWAY == "3"){
		var cardnum = $("#CARDNUM").val();
		$("#CARDNUM1").val(cardnum.substring(0,4));
		$("#CARDNUM2").val(cardnum.substring(5,9));
		$("#CARDNUM3").val(cardnum.substring(10,14));
		$("#CARDNUM4").val(cardnum.substring(15));
		var craddate = $("#EXPIREDYM").val();
		$("#EXPIREDYEAR").val("20"+craddate.substring(3));
		$("#EXPIREDDATEMONTHS").val(craddate.substring(0,2));
		if($("#EXPIREDDATEMONTHS").val().startsWith("0")){
			var M = $("#EXPIREDDATEMONTHS").val();
			M = M.replace("0","");
			$("#EXPIREDDATEMONTHS").val(M);
		}
	}
	
	//驗證碼驗證
	var capURI = "${__ctx}/CAPCODE/captcha_valided_trans";
	var capData = $("#formId").serializeArray();
	var capResult = fstop.getServerDataEx(capURI,capData,false);
	
	//驗證結果
	if(capResult.result == true){
		//晶片金融卡
		if(FGTXWAY == "2"){
			useCardReader();
		}
		//自然人憑證
		if(FGTXWAY == "4"){
			try{
				var ret=myobj.GetServerAuth();
				if(ret){
					useNatural();
				}
				else{
					errorBlock(null, null, ["<spring:message code='LB.X1668' />"],
							'<spring:message code= "LB.Confirm" />', null);
				}
			}
			catch(e){
				errorBlock(null, null, ["<spring:message code='LB.X1668' />"],
						'<spring:message code= "LB.Confirm" />', null);
			}
		}
		//信用卡
		if(FGTXWAY == "3"){
			if(!CheckSelect("EXPIREDYEAR","<spring:message code="LB.X1898" />","#")){
				return false;
			}
			if(!CheckSelect("EXPIREDDATEMONTHS","<spring:message code="LB.X1899" />","#")){
				return false;
			}
			if($("#CPRIMBIRTHDAY").val() == ""){
				//alert("<spring:message code= "LB.Alert029" />");
				errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert029' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				return false;
			}
			initBlockUI();
			$("#formId").submit();
		}
	}
	else{
		//失敗重新產生驗證碼
		errorBlock(null, null, ["<spring:message code='LB.X1082' />"],
			'<spring:message code= "LB.Quit" />', null);
//			alert("<spring:message code= "LB.X1082" />");
		changeCode();
	}
}

//取得卡片主帳號結束
function getMainAccountFinish(result){
	//成功
	if(result != "false"){
		var cardACN = result;
		if(cardACN.length > 11){
			cardACN = cardACN.substr(cardACN.length - 11);
		}
		var UID = $("#CUSIDN").val();
		
		var uri = urihost+"/COMPONENT/component_without_id_aj";
		var rdata = { ACN: cardACN ,UID: UID };
		fstop.getServerDataEx(uri, rdata, true, CheckIdResult);
	}
	//失敗
	else{
		showTempMessage(500,"<spring:message code= "LB.X1250" />","","MaskArea",false);
	}
}

function orderby(){
	$('#YY>option').sort(function(a,b){
        //按option中的值排序
        var aText = $(a).val()*1;
        var bText = $(b).val()*1;
        if(aText > bText) return -1;
        if(aText < bText) return 1;
        return 0;
    }).appendTo('#YY');
    $('#YY>option').eq(0).attr("selected","selected");
}


jQuery(function($) {//,{autoclear: false}
	$("#CARDNUM").mask("9999/9999/9999/9999",{autoclear: false});
	$("#EXPIREDYM").mask("99/99",{autoclear: false});
});


function checkitemAll(){
	if($("#allCheckBox").prop("checked") == true){
		$('#main-content').show();
		$('#CMSUBMIT').removeClass('btn-flat-gray');
		$('#CMSUBMIT').addClass('btn-flat-orange');
	}else{
		$('#CMSUBMIT').removeClass('btn-flat-orange');
		$('#CMSUBMIT').addClass('btn-flat-gray');
	}
}



</script>
</head>
<body>
	<%@ include file="../customer_service/PersonalInformationProtectionAct.jsp"%>
	<div id="obj"></div>
	<section id="bankDialog" class="error-block" style="display:none">
		<div class="error-for-message">
			<!-- 合作銀行 -->
			<p class="error-title"><spring:message code="LB.D1231_1" /></p>
			<p class="error-content">
				<spring:message code="LB.X0509" />、<!-- 臺灣中小企銀 -->
				<spring:message code="LB.X0510" />、<!-- 玉山銀行 -->
				<spring:message code="LB.X0511" />、<!-- 聯邦商銀 -->
				<spring:message code="LB.X0512" />、<!-- 永豐銀行 -->
				<spring:message code="LB.X0513" />、<!-- 遠東商銀 -->
				<spring:message code="LB.X0514" />、<!-- 日盛商銀 -->
				<spring:message code="LB.X0515" />、<!-- 星展銀行 -->
				<spring:message code="LB.X0516" />、<!-- 新光商銀 -->
				<spring:message code="LB.X0517" />、<!-- 元大銀行 -->
				<spring:message code="LB.X0518" />、<!-- 陽信銀行 -->
				<spring:message code="LB.X0519" />、<!-- 華泰銀行 -->
				<spring:message code="LB.X0520" />、<!-- 三信商銀 -->
				<spring:message code="LB.X0521" />、<!-- 華南商銀 -->
				<spring:message code="LB.X0522" />、<!-- 樂天信用卡 -->
				<spring:message code="LB.X0523" />、<!-- 台中商銀 -->
				<spring:message code="LB.X0524" />、<!-- 匯豐銀行 -->
				<spring:message code="LB.X0525" />、<!-- 花旗銀行 -->
				<spring:message code="LB.X0526" />、<!-- 土地銀行 -->
				<spring:message code="LB.X0527" />、<!-- 彰化銀行 -->
				<spring:message code="LB.X0528" />、<!-- 高雄銀行 -->
				<spring:message code="LB.X0529" />、<!-- 臺灣銀行 -->
				<spring:message code="LB.X0530" />、<!-- 台北富邦銀行 -->
				<spring:message code="LB.X0531" />、<!-- 中國信託 -->
				<spring:message code="LB.X0532" />、<!-- 第一銀行 -->
				<spring:message code="LB.X0533" />、<!-- 凱基商銀 -->
				<spring:message code="LB.X0534" />、<!-- 台新商銀 -->
				<spring:message code="LB.X0535" />、<!-- 永旺信用卡 -->
				<spring:message code="LB.X0536" />、<!-- 合作金庫 -->
				<spring:message code="LB.X0537" />、<!-- 兆豐商銀 -->
				<spring:message code="LB.X0538" /><!-- 國泰世華 -->
			</p>
			<input type="button" id="" value="<spring:message code='LB.X2019' />" class="ttb-button btn-flat-orange " onclick=" $('#bankDialog').hide();"/>
		</div>
	</section>
	<!--交易機制所需畫面-->
	<%@ include file="../component/trading_component.jsp"%>
	<script type="text/javascript">
		//自然人憑證的物件
// 		var notCheckIKeyUser = true; // 不驗證是否是IKey使用者
// 		var myobj = null; // 自然人憑證用
		

	</script>
	<!-- 自然人憑證元件載點 -->
<%-- 	<a href="${__ctx}/component/windows/natural/TBBankPkcs11ATL.exe" id="naturalComponent" target="_blank" style="display:none"></a> --%>
	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 申請信用卡     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0666" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
<%-- 		<%@ include file="../index/menu.jsp"%> --%>
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!--線上申請信用卡 -->
				<h2><spring:message code="LB.D0022" /></h2>
				<div id="step-bar">
                    <ul>
                        <li class="active"><spring:message code="LB.D0851" /></li><!-- 身份驗證 -->
                        <li class=""><spring:message code="LB.X1966" /></li><!-- 信用卡與權益 -->
                        <li class=""><spring:message code="LB.X1967" /></li><!-- 申請資料 -->
                        <li class=""><spring:message code="LB.Confirm_data" /></li><!-- 確認資料 -->
                        <li class=""><spring:message code="LB.X1968" /></li><!-- 完成申請 -->
                    </ul>
                </div>
				<form method="post" id="formId" action="${__ctx}/CREDIT/APPLY/apply_creditcard_p2">
					<input type="hidden" name="ADOPID" value="NA03"/>
					<input type="hidden" id="NOTIEINSTALL" name="NOTIEINSTALL"/>
					<input type="hidden" id="AUTHCODE" name="AUTHCODE"/>
					<input type="hidden" id="AUTHCODE1" name="AUTHCODE1"/>
					<input type="hidden" id="CertFinger" name="CertFinger"/>
					<input type="hidden" id="CertB64" name="CertB64"/>
					<input type="hidden" id="CertSerial" name="CertSerial"/>
					<input type="hidden" id="CertSubject" name="CertSubject"/>
					<input type="hidden" id="CertIssuer" name="CertIssuer"/>
					<input type="hidden" id="CertNotBefore" name="CertNotBefore"/>
					<input type="hidden" id="CertNotAfter" name="CertNotAfter"/>
					<input type="hidden" id="HiCertType" name="HiCertType"/>
					<input type="hidden" id="CUSIDN4" name="CUSIDN4"/>
					<input type="hidden" id="UID" name="UID"/>
					<input type="hidden" id="ACN" name="ACN"/>
					<input type="hidden" id="OUTACN" name="OUTACN"/>
					<input type="hidden" id="QRCODE" name="QRCODE" value="${result_data.data.QRCODE}"/>
					<input type="hidden" id="BRANCH" name="BRANCH" value="${result_data.data.branch}"/>
					<!--交易機制所需欄位-->
					<input type="hidden" id="jsondc" name="jsondc" value="${result_data.data.jsondc}"/>
					<input type="hidden" id="ISSUER" name="ISSUER"/>
					<input type="hidden" id="ACNNO" name="ACNNO"/>
					<input type="hidden" id="TRMID" name="TRMID"/>
					<input type="hidden" id="iSeqNo" name="iSeqNo"/>
					<input type="hidden" id="ICSEQ" name="ICSEQ"/>
					<input type="hidden" id="TAC" name="TAC"/>
					<input type="hidden" id="pkcs7Sign" name="pkcs7Sign"/>
					<input type="hidden" id="TOKEN" name="TOKEN" value="${sessionScope.transfer_confirm_token}"/>
					<!-- 表單顯示區  -->
					<div class="main-content-block pl-0 pr-0 row">
						<div class="col-12">
							<div class="ttb-input-block">
								<div class="ttb-message">
									<!-- 身份驗證 -->
									<p><spring:message code="LB.D0851" /></p>
								</div>
								<!--請輸入您的身分證字號並選擇身份驗證的方式 -->
								<p><spring:message code="LB.X1969" /></p>
							
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
<!-- 											身分證字號 -->
											<spring:message code="LB.D0581" />
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="CUSIDN" name="CUSIDN" class="text-input validate[required,funcCall[validate_checkSYS_IDNO[CUSIDN]]]" maxlength="10" size="10" placeholder='<spring:message code="LB.D0025" />'/>
										</div>
									</span>
								</div>
							
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
<!-- 											出生日期 -->
											<spring:message code="LB.D0582" />
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<select id="YY" name="YY" class="custom-select select-input input-width-100">
												<option value="#" style="">---</option>
											</select>
											<select id="MM" name="MM" class="custom-select select-input input-width-60">
												<option value="#">---</option>
											</select>
											<select id="DD" name="DD" class="custom-select select-input input-width-60">
												<option value="#">---</option>
											</select>
										</div>
										<input type="text" id="checkDD" class="validate[funcCallRequired[validate_CheckSelects['<spring:message code="LB.D0582" />',YY,MM,DD,#]]]" 
										 style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;"/>
										<input type="hidden" id="CPRIMBIRTHDAY" name="CPRIMBIRTHDAY"/>
										<input type="hidden" id="CPRIMBIRTHDAYshow" name="CPRIMBIRTHDAYshow"/>
									</span>
								</div>
							
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
<!-- 											認證機制 -->
											<spring:message code="LB.D0028" />
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input" id="pcDIV">
											<label class="radio-block" for="CMNPC"><spring:message code="LB.X0504" />
												<input type="radio" id="CMNPC" name="FGTXWAY" value="4" onclick="openinput()"/><!-- 自然人憑證 --><font color="red">（<spring:message code="LB.X0506" />）</font>
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!-- 自然人憑證PIN碼 -->
										<div class="ttb-input" id="row1" style="display:none">
											<div >
												<span class="input-subtitle subtitle-color"><spring:message code="LB.D1540" /></span>
												<input type="PASSWORD" class="text-input" maxLength="8" size="10" id="NPCPIN" name="NPCPIN" value="" placeholder="<spring:message code="LB.X2039" />"/>
											</div>
										</div>
										
										<div class="ttb-input" id="pcDIV2">
											<label class="radio-block" for="CMCARD"><spring:message code="LB.X0505" />
												<input type="radio" id="CMCARD" name="FGTXWAY" value="2" onclick="openinput()"/><!-- 本行晶片金融卡 --><font color="red">（<spring:message code="LB.X0506" />）</font>
												<span class="ttb-radio"></span>
											</label>
										</div>
                                    	<div class="ttb-input">
											<label class="radio-block" for="CMCREDIT"><spring:message code="LB.Credit_Card" />
												<input type="radio" id="CMCREDIT" name="FGTXWAY" value="3" onclick="openinput()"/><!-- 信用卡 -->
												<span class="ttb-radio"></span>
											</label>
											<input type="button" onclick="test()" id="cooperBank" value="<spring:message code="LB.D1231_1" />" class="ttb-sm-btn btn-flat-gray"/><!-- 合作銀行 -->
										</div>
										<div class="ttb-input" id="row2" style="display:none">
											<div class="ttb-input">
												<span class="input-subtitle subtitle-color"><spring:message code="LB.W0639" /></span><!-- 信用卡號 -->
												<input type="text" id="CARDNUM" name="CARDNUM" class="text-input" placeholder="____ / ____ / ____ / ____" style="text-align:center"/>
												<input type="hidden" id="CARDNUM1" name="CARDNUM1"/>
												<input type="hidden" id="CARDNUM2" name="CARDNUM2"/>
												<input type="hidden" id="CARDNUM3" name="CARDNUM3"/>
												<input type="hidden" id="CARDNUM4" name="CARDNUM4"/>
												<input type="hidden" id="CARDNUMCHK"/>
		<!-- 										<input type="text" id="CARDNUM1" name="CARDNUM1" class="text-input card-input" maxlength="4" size="4" onKeyUp="setBlur(this,'CARDNUM2')"/> -->
		<!-- 										<input type="text" id="CARDNUM2" name="CARDNUM2" class="text-input card-input" maxlength="4" size="4" onKeyUp="setBlur(this,'CARDNUM3')"/> -->
		<!-- 										<input type="text" id="CARDNUM3" name="CARDNUM3" class="text-input card-input" maxlength="4" size="4" onKeyUp="setBlur(this,'CARDNUM4')"/> -->
		<!-- 										<input type="text" id="CARDNUM4" name="CARDNUM4" class="text-input card-input" maxlength="4" size="4"/> -->
											</div>
											<div class="ttb-input d-flex">
												<div>
		                                            <span class="input-subtitle subtitle-color"><spring:message code="LB.X1970" /></span><!-- 有效年月 -->
		                                            <input type="text" class="text-input" name="EXPIREDYM" id="EXPIREDYM" value="" placeholder="MM/YY" style="text-align:center"/>
		                                            <input type="hidden" name="EXPIREDYEAR" id="EXPIREDYEAR" value="">
		                                            <input type="hidden" name="EXPIREDDATEMONTHS" id="EXPIREDDATEMONTHS" value="">
		                                        </div>
		                                        <div>
		                                            <span class="input-subtitle subtitle-color"><spring:message code="LB.X1971" /></span><!-- 卡片背面末三碼 -->
		                                            <input type="text" class="text-input" name="CVCTWO" id="CVCTWO" maxlength="3" value="" placeholder="CVV" style="text-align:center"/>
		                                        </div>
											</div>
										</div>
										<span id="hideblock">
											<input type="text" id="chkradio" class="validate[funcCallRequired[validate_Radio[<spring:message code= 'LB.Alert028' />,FGTXWAY]]]"
											style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;"/>
										</span>
									</span>
								</div>
							<!-- 確認新使用者名稱 -->
							
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
<!-- 												驗證碼 -->
												<spring:message code="LB.Captcha" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input id="capCode" type="text" class="ttb-input text-input input-width-125"
												name="capCode" placeholder="<spring:message code='LB.X1702' />" maxlength="6" autocomplete="off">
											<img name="kaptchaImage" src="" class="verification-img"/>
											<!-- 重新產生驗證碼 -->
											<input type="button" name="reshow" class="ttb-sm-btn btn-flat-gray" onclick="refreshCapCode()" value="<spring:message code='LB.Regeneration' />" /><!-- 重新產生驗證碼 -->
<%-- 											<input type="button" name="reshow" class="ttb-sm-btn btn-flat-orange" onclick="refreshCapCode()" value="<spring:message code="LB.Regeneration" />" /> --%>
											<span class="input-remarks"><spring:message code="LB.X1972" /></span><!-- 請注意：英文不分大小寫，限半形字元 -->
										</div>
									</span>
								</div>
								<div>
	                                <div class="ttb-message">
	                                	<!-- 已同意個人資料保護法告知事項 -->
	                                    <label class="check-block">
	                                    	<a onclick="checkitemAll()"> 
	                                    		<spring:message code="LB.onlineapply_creditcard_PIPNchk" />	                                    	
	                                    	</a>
	                                        <input type="checkbox" name="allCheckBox" id="allCheckBox" onclick="checkitemAll()" class="validate[funcCallRequired[validate_chkClickboxKind[<spring:message code= "LB.onlineapply_creditcard_PIPNchk" />,1,allCheckBox]]]">
	                                        <span class="ttb-check"></span>
	                                    </label>
	                                </div>
								</div>
							</div>
							<!-- 取消 -->
							<input type="button" name="CMBACK" id="CMBACK" value="<spring:message code="LB.Cancel" />" class="ttb-button btn-flat-gray" onclick="window.close();"/>
							<input type="button" id="CMSUBMIT" value="<spring:message code="LB.X0080" />" class="ttb-button btn-flat-orange"/><!-- 下一步 -->
						</div>
					</div>
				</form>
			</section>
		</main>
	</div>
	
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>