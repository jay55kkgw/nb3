<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
		initFootable();
		// 確認鍵 click
		submit();
	}
	
	// 確認鍵 Click
	function submit() {
		$("#CMSUBMIT").click( function(e) {
			console.log("submit~~");
			$("#formId").attr("action","${__ctx}/login");
	 	  	$("#formId").submit();
		});
	}
	
	function processQuery() {
		try {
			newwindow = window.open("${__ctx}/login");
			newwindow.focus();
			window.opener.closeWindow();
		} catch (e) {

		}
		closeWindow();
		return false;
	}
	
</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Register" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<!-- 信用卡申請網路銀行 -->
				<h2><spring:message code= "LB.X1086" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form id="formId" method="post" action="">
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
                            
								<!--交易時間-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D0019" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
										<span>${use_creditcard_result.data.CCTXTIME}</span>
                                    </div>
                                </span>
                            </div>
							
							<!--電子郵箱-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D0346" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
										<span>${use_creditcard_result.data.MAIL}</span>
                                    </div>
                                </span>
                            </div>
							
							<!--通知項目-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4> <spring:message code= "LB.D1016" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
										<span>${use_creditcard_result.data.notify}</span>
                                    </div>
                                </span>
                            </div>

							<c:if test="${use_creditcard_result.data.NOTIFY_CCARDBILL == 'Y'}">
								<!--電子帳單密碼-->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4> <spring:message code= "LB.D0294" /></h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
										<!-- 身份證字號末8位數字 -->
											<span><font color="#FF0000"><b><spring:message code= "LB.D1061" /></b></font></span>
										</div>
									</span>
								</div>
							</c:if>

						</div>
						<!-- 登入網路銀行 -->
                        <input type="button" class="ttb-button btn-flat-orange" id="CMSUBMIT" name="CMSUBMIT" value="<spring:message code= "LB.X1087" />"/>
                        
                    </div>
                </div>
				</form>
				
                <ol class="list-decimal description-list">
                <p><spring:message code="LB.Description_of_page" /></p>
                <!-- 請於一個月內登入網路銀行，首次登入需變更密碼，逾期請洽客服專線0800-01-7171後，重新線上申請。 -->
                  <li><spring:message code= "LB.Creditcard_Apply_Controller_P4_D1" /> -</li>
                </ol>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>