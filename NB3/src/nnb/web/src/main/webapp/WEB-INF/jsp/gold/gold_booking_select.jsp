<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<!--舊版驗證-->
<script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	
	$("#CMSUBMIT").click(function(){
//臺幣轉出帳號 -->
		if(!CheckSelect("SVACN","<spring:message code= "LB.W1496" />","#")){ 
	   		return false;
		}
//黃金轉入帳號
	   	if(!CheckSelect("ACN","<spring:message code= "LB.W1497" />","#")){ 
	   		return false;
	   	}
	   	var TRNGD = $("#TRNGD").val();
	   	
		if(TRNGD.indexOf(".") != -1){
			//請輸入整數的買進公克數
			//alert("<spring:message code= "LB.X0944" />");
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.X0944' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
			);
			return false;
		}
//買進公克數
	   	if (!CheckNumber("TRNGD","<spring:message code= "LB.W1498" />",false)){
	   	    return false;   	 	
	   	}
		if(parseInt(TRNGD) < 1 || parseInt(TRNGD) > 50000){
			//每筆最低交易數量為 1 公克，最高交易數量為 50,000 公克。
			//alert("<spring:message code= "LB.X0945" />");
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.X0945' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
			);
			return false;
		}
		$('#formID').attr("action","${__ctx}/GOLD/TRANSACTION/gold_booking_confirm");
		$('#formID').removeAttr("target");
		$("#formID").submit();
	});
	$("#resetButton").click(function(){
		$("#SVACN").val("#");
		$("#ACN").val("#");
		$("#TRNGD").val("");
	});
});
function displayAcn(){
	if($("#SVACN").val() != "#"){
		var URI = "${__ctx}/GOLD/TRANSACTION/getGoldTradeTWAccountListAjax";
		var rqData = {SVACN:$("#SVACN").val()};
		fstop.getServerDataEx(URI,rqData,false,getGoldTradeTWAccountListAjaxFinish);
	}
}
function getGoldTradeTWAccountListAjaxFinish(data){
	if(data.result == true){
		var goldTradeTWAccountList = $.parseJSON(data.data);
		
		$("#ACN").html("");
 //請選擇帳號
		var ACNHTML = "<option value='#'>---<spring:message code= "LB.W0257" />---</option>";
		for(var x=0;x<goldTradeTWAccountList.length;x++){
			ACNHTML += "<option value='" + goldTradeTWAccountList[x].ACN + "'>" + goldTradeTWAccountList[x].ACN + "</option>";
		}
		$("#ACN").html(ACNHTML);
	}
	else{
		//無法取得黃金轉入帳號資料
		//alert("<spring:message code= "LB.X0946" />");
		errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.X0946' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
		);
	}
}

function priceQuery(){
	$('#formID').attr("action","${__ctx}/GOLD/PASSBOOK/history_price_query_result");
	$('#formID').attr("target","_blank");
	$('#formID').submit();
	
}
</script>
</head>
<body>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %> 
	<!--header-->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 黃金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2272" /></li>
    <!-- 黃金申購     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1493" /></li>
		</ol>
	</nav>

	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!--快速選單及主頁內容-->
		<main class="col-12"> 
			<!--主頁內容-->
			<section id="main-content" class="container">
			<!-- 預約黃金買進 -->
				<h2><spring:message code= "LB.X0949" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form id="formID" action="${__ctx}/GOLD/TRANSACTION/gold_booking_confirm" method="post">
                	<input type="hidden" name="ADOPID" value="N09101"/>
					<input type="hidden" id="TOKEN" name="TOKEN" value="${sessionScope.transfer_confirm_token}" /><!-- 防止重複交易 -->
					<input type="hidden" id="DPMYEMAIL" name="DPMYEMAIL" value="${sessionScope.dpmyemail}" />
					<input type="hidden" name="QUERYTYPE" value="LASTMON">
	                <div class="main-content-block row">
	                    <div class="col-12 tab-content">
	                        <div class="ttb-input-block">
								<!--臺幣轉出帳號-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
		                                <label>
<!-- 臺幣轉出帳號 -->
											<h4><spring:message code= "LB.W1496" /></h4>
										</label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
	                                       	<select name="SVACN" id="SVACN" onchange="displayAcn()" class="custom-select select-input half-input">
<!-- 請選擇帳號 -->
												<option value="#">---<spring:message code= "LB.W0257" />---</option>
												<c:forEach var="dataMap" items="${goldTradeAccountList}">
													<option value="${dataMap.SVACN}">${dataMap.SVACN}</option>
												</c:forEach>
											</select>
										</div>
									</span>
								</div>
								 <!--黃金轉入帳號-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
										<label>
<!-- 黃金轉入帳號 -->
											<h4><spring:message code= "LB.W1497" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<select name="ACN" id="ACN" class="custom-select select-input half-input">
<!-- 請選擇帳號 -->
												<option value="#">---<spring:message code= "LB.W0257" />---</option>
											</select>
										</div>
									</span>
								</div>
								<!--買進公克數-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
										<label>
<!-- 買進公克數 -->
											<h4><spring:message code= "LB.W1498" /></h4>
										</label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input d-flex" >
<!-- 公克 -->
	                                       	<input type="text" id="TRNGD" name="TRNGD" maxLength="5" size="5" class="text-input fit-unit-height"><span class="input-unit"><spring:message code= "LB.W1435" /></span>&nbsp;&nbsp;
										</div>
									</span>
								</div>
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
									</span>
	                                <span class="input-block">
										<div class="ttb-input d-flex" >
<!-- 黃金重量換算表 -->
											<a href="https://www.tbb.com.tw/web/guest/-260" target="_blank"><spring:message code= "LB.W1500" /></a>&nbsp;&nbsp;
<!-- 走勢 -->
											<a href="javascript:void(0)" onclick="priceQuery()"><spring:message code= "LB.W1501" /></a>
										</div>
									</span>
								</div>
								<div>
							</div>
							</div>
							
<!-- 重新輸入 -->
	                				<input type="button" id="resetButton" value="<spring:message code= "LB.Re_enter" />" class="ttb-button btn-flat-gray"/>
<!-- 確定 -->
	                				<input type="button" id="CMSUBMIT" value="<spring:message code= "LB.Confirm" />" class="ttb-button btn-flat-orange"/>
	                			
	                    </div>
	                </div>
							<ol class="description-list list-decimal">
								<p><spring:message code="LB.Description_of_page" /></p><!-- 說明 -->
									<!-- 「預約黃金買進」免手續費。 -->
									<li><spring:message code= "LB.Gold_Booking_P1_D1" /></li>
									<!-- 本服務限約定帳戶，如尚未約定，請洽往來分行辦理。 -->
									<li><spring:message code= "LB.Gold_Booking_P1_D2" /></li>
									<!-- 預約時間為營業日00：00〜08：59、15：30〜24：00及非營業日00：00〜24：00。 -->
									<li><font style="color:red"><spring:message code= "LB.Gold_Booking_P1_D3" /></font></li>
									<!-- 每筆最低交易數量為1公克，每日累計買進最高交易數量為50,000公克。 -->
									<!-- 預約交易將於買進日與即時交易及定期定額投資日之交易量併計每日累計買進最高數量。 -->
									<li><spring:message code= "LB.Gold_Booking_P1_D4-1" /><br>
									<spring:message code= "LB.Gold_Booking_P1_D4-2" /></li>
									<!-- 預約黃金買進將以 -->
									<!-- 預約後的第一個營業日的第一次牌告賣出價格交易，請於扣款前一日存足款項於轉出帳戶備扣。 -->
									<li><spring:message code= "LB.Gold_Booking_P1_D5-1" /><font style="color:red"><spring:message code= "LB.Gold_Booking_P1_D5-2" /></font></li>
									<!-- 申購黃金數量達3,000公克(含)以上時，特予以優惠折讓，查看折讓率請按 折讓表 -->
									<li><spring:message code= "LB.Gold_Booking_P1_D6-1" /><a href="https://www.tbb.com.tw/web/guest/-264" target="_blank"><spring:message code= "LB.Gold_Booking_P1_D6-2" /></a></li>
									<!-- 本服務限約定帳戶，如尚未約定，請洽往來分行辦理。 -->
									<li><spring:message code= "LB.Gold_Booking_P1_D7-1" /><spring:message code= "LB.Gold_Booking_P1_D7-2" /></li>
								</ol>
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>