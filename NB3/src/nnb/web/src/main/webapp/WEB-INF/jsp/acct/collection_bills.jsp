<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
</head>
 <body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>



    <!-- 麵包屑     -->
    <nav id="header-breadcrumb-nav" aria-label="breadcrumb">
        <ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
            <li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
            <li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 帳戶查詢     -->
            <li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Account_Inquiry" /></li>
    <!-- 託收票據明細     -->
            <li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Bill_For_Collection_BC" /></li>
        </ol>
    </nav>
		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 --> 
 		<div class="content row">
 		   <%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->

 		
		
			<!-- 		主頁內容  -->
			<main class="col-12">	

			<section id="main-content" class="container">
				<h2><spring:message code="LB.Bill_For_Collection_BC" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 						下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
<!-- 						下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
<!-- 						下載為舊txt檔 -->
						<option value="oldtxt"><spring:message code="LB.Download_oldversion_txt_file" /></option>
					</select>
				</div>				
				<form method="post" id="formId" action="${__ctx}/FCY/ACCT/demand_deposit_result">
					<!-- 下載用 -->
					<input type="hidden" name="downloadFileName" value="<spring:message code="LB.Bill_For_Collection_BC" />"/>
					<input type="hidden" name="downloadType" id="downloadType"/> 					
					<input type="hidden" name="templatePath" id="templatePath"/> 	
					<input type="hidden" name="hasMultiRowData" value="true"/> 	
					<!-- EXCEL下載用 -->
					<input type="hidden" name="headerRightEnd" value="1" />
                    <input type="hidden" name="headerBottomEnd" value="9" />
                    <input type="hidden" name="multiRowStartIndex" value="13" />
                    <input type="hidden" name="multiRowEndIndex" value="13" />
                    <input type="hidden" name="multiRowCopyStartIndex" value="10" />
                    <input type="hidden" name="multiRowCopyEndIndex" value="14" />
                    <input type="hidden" name="multiRowDataListMapKey" value="TABLE" />
                    <input type="hidden" name="rowRightEnd" value="10" />
					<!-- TXT下載用 -->
					<input type="hidden" name="txtHeaderBottomEnd" value="10"/>
					<input type="hidden" name="txtMultiRowStartIndex" value="14"/>
					<input type="hidden" name="txtMultiRowEndIndex" value="14"/>
					<input type="hidden" name="txtMultiRowCopyStartIndex" value="11"/>
					<input type="hidden" name="txtMultiRowCopyEndIndex" value="16"/>
					<input type="hidden" name="txtMultiRowDataListMapKey" value="TABLE"/>
					<input type="hidden" name="baseDate"  id="baseDate" value="${time_now}"/>
					
					<div class="main-content-block row">
						<div class="col-12 tab-content"> 		
							<div class="ttb-input-block">
					        <!-- 帳號區塊-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<!-- 帳號 -->
											<h4><spring:message code="LB.Account"/></h4>
										</label>
									</span>
									<div class="input-block">
										<select name="ACN" id="ACN" class="custom-select select-input half-input">
											<option value=""><spring:message code="LB.Select_account" /></option>
											<option value=""><spring:message code="LB.All" /></option>
										</select>
										<div id="acnoIsShow">
											<span id = "showText" class="input-unit "></span>
										</div>
									</div>
								</div>
								<!-- 查詢項目區塊 -->
								<div class="ttb-input-item row">
									<!--託收行區間  -->
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Collection_bank" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block"><spring:message code="LB.All" />
												<input type="radio" name="FGBRHCOD" id="DPBHALL" value="DPBHALL" checked/>
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="radio-block"><spring:message code="LB.X2123" />
												<input type="radio" name="FGBRHCOD" id="DPBHENT" value="DPBHENT" />
												<span class="ttb-radio"></span>
											</label>										
										</div>
										<div class="ttb-input">
		                                	<input type="hidden" class="text-input" name="DPBHNO" id="DPBHNO">
		                                	<input type="text" class="text-input" name="DPBHNO_str" id="DPBHNO_str">
		                                	<button type="button" class="btn-flat-orange" name="DPBHFM" id="DPBHFM" onclick="window.open('${__ctx}/NT/ACCT/collection_bills_get_branch_list')"><spring:message code="LB.X0261" /></button>
		                                </div>
									</span>
								</div>
								<!-- 查詢項目區塊 -->
								<div class="ttb-input-item row">
									<!--查詢項目  -->
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Query_item" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block"><spring:message code="LB.X0071" />
												<input type="radio" name="FGTYPE" id="DPCKTYPE1" value="DPCKTYPE1" checked/>
												<span class="ttb-radio"></span>
											</label>
											<label class="radio-block"><spring:message code="LB.X0072" />
												<input type="radio" name="FGTYPE" id="DPCKTYPE2" value="DPCKTYPE2" />
												<span class="ttb-radio"></span>
											</label>
											<label class="radio-block"><spring:message code="LB.X0073" />
												<input type="radio" name="FGTYPE" id="DPCKTYPE3" value="DPCKTYPE3" />
												<span class="ttb-radio"></span>
											</label>
											<label class="radio-block"><spring:message code="LB.X0074" />
												<input type="radio" name="FGTYPE" id="DPCKTYPE4" value="DPCKTYPE4" />
												<span class="ttb-radio"></span>
											</label>
											<label class="radio-block"><spring:message code="LB.All" />
												<input type="radio" name="FGTYPE" id="DPCKTYPE5" value="DPCKTYPE5" />
												<span class="ttb-radio"></span>
											</label>
										</div>
									</span>
								</div>
								<!-- 查詢區間區塊 -->
								<div class="ttb-input-item row">
									<!--查詢區間  -->
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Inquiry_period" /></h4>
										</label>
									</span>
									<span class="input-block">
										<!--全部 -->
										<div class="ttb-input">
											<label class="radio-block"><spring:message code="LB.X0075" />
												<input type="radio" name="FGPERIOD" id="DPPERIOD1" value="DPPERIOD1" checked/>
												<span class="ttb-radio"></span>
											</label>
										</div>
									<!--  指定日期區塊 -->
										<!--  指定託收日 -->
										<div class="ttb-input">								
											<label class="radio-block"><spring:message code="LB.Collection_date" />：
												<input type="radio" name="FGPERIOD" id="DPPERIOD2" value="DPPERIOD2" /> 
												<span class="ttb-radio"></span>
											</label>
											<!--期間起日 -->		
											<div class="ttb-input DPPERIOD2_div"  style="display:none">
												<span class="input-subtitle subtitle-color">
													<spring:message code="LB.D0013" />
												</span>
												<div class="ttb-input">
													<input type="text" id="CMSDATE2" name="CMSDATE2" class="text-input datetimepicker" size="10" value="" /> 
													<span class="input-unit CMSDATE2">
														<img src="${__ctx}/img/icon-7.svg" />
													</span>
												</div>
												<!-- 不在畫面上顯示的span -->
												<span id="hideblocka" >
													<!-- 驗證用的input -->
													<input id="Monthly_DateA" name="Monthly_DateA" type="text" class="text-input validate[required, verification_date[Monthly_DateA]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												</span>
											</div>
											<!--期間迄日 -->		
											<div class="ttb-input DPPERIOD2_div"  style="display:none">
												<span class="input-subtitle subtitle-color">
													<spring:message code="LB.Period_end_date" />
												</span>
												<div class="ttb-input">
													<input type="text" id="CMEDATE2" name="CMEDATE2" class="text-input datetimepicker" size="10" value="" /> 
													<span class="input-unit CMEDATE2">
														<img src="${__ctx}/img/icon-7.svg" />
													</span>
												</div>
												<!-- 不在畫面上顯示的span -->
												<span id="hideblockb" >
												<!-- 驗證用的input -->
													<input id="Monthly_DateB" name="Monthly_DateB" type="text" class="text-input validate[required, verification_date[Monthly_DateB] ,funcCall[validate_CheckDateScope['<spring:message code="LB.X1452" />', baseDate, Monthly_DateA, Monthly_DateB, true, null, null]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												</span>
											</div>
										</div>
										<!--  指定到期日 -->	
										<div class="ttb-input">								
											<label class="radio-block">
												<spring:message code="LB.Expired_date" />：
												<input type="radio" name="FGPERIOD" id="DPPERIOD3" value="DPPERIOD3" /> 
												<span class="ttb-radio"></span>
											</label>	
											<!--期間起日 -->		
											<div class="ttb-input DPPERIOD3_div"  style="display:none">
												<span class="input-subtitle subtitle-color">
													<spring:message code="LB.D0013" />
												</span>
												<div class="ttb-input">
													<input type="text" id="CMSDATE3" name="CMSDATE3" class="text-input datetimepicker" size="10" value="" /> 
													<span class="input-unit CMSDATE3">
														<img src="${__ctx}/img/icon-7.svg" />
													</span>
												</div>
												<!-- 不在畫面上顯示的span -->
												<span id="hideblockc" >
													<!-- 驗證用的input -->
													<input id="Monthly_DateC" name="Monthly_DateC" type="text" class="text-input validate[required, verification_date[Monthly_DateC]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												</span><br>
											</div>
											<!--期間起日 -->		
											<div class="ttb-input DPPERIOD3_div"  style="display:none">
												<span class="input-subtitle subtitle-color">
													<spring:message code="LB.Period_end_date" />
												</span>
												<div class="ttb-input">
													<input type="text" id="CMEDATE3" name="CMEDATE3" class="text-input datetimepicker" size="10" value="" /> 
													<span class="input-unit CMEDATE3">
														<img src="${__ctx}/img/icon-7.svg" />
													</span>
												</div>
												<!-- 不在畫面上顯示的span -->
												<span id="hideblockd" >
												<!-- 驗證用的input -->
													<input id="Monthly_DateD" name="Monthly_DateD" type="text" class="text-input validate[required, verification_date[Monthly_DateD], funcCall[validate_CheckDateScope['<spring:message code="LB.X1453" />', baseDate, Monthly_DateC, Monthly_DateD, true, null, null]]]"
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												</span>
											</div>
										</div>
										<!--  指定入帳日 -->	
										<div class="ttb-input">								
											<label class="radio-block">
											<!--  指定入帳日 -->
												<spring:message code="LB.Accounted_date" />：
												<input type="radio" name="FGPERIOD" id="DPPERIOD4" value="DPPERIOD4" /> 
												<span class="ttb-radio"></span>
											</label>
											<!--期間起日 -->		
											<div class="ttb-input DPPERIOD4_div"  style="display:none">
												<span class="input-subtitle subtitle-color">
													<spring:message code="LB.D0013" />
												</span>
												<div class="ttb-input">
													<input type="text" id="CMSDATE4" name="CMSDATE4" class="text-input datetimepicker" size="10" value="" /> 
													<span class="input-unit CMSDATE4">
														<img src="${__ctx}/img/icon-7.svg" />
													</span>
												</div>
												<!-- 不在畫面上顯示的span -->
												<span id="hideblocke" >
													<!-- 驗證用的input -->
													<input id="Monthly_DateE" name="Monthly_DateE" type="text" class="text-input validate[required, verification_date[Monthly_DateE]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												</span><br>
											</div>
											<!--期間起日 -->		
											<div class="ttb-input DPPERIOD4_div"  style="display:none">
												<span class="input-subtitle subtitle-color">
													<spring:message code="LB.Period_end_date" />
												</span>
												<div class="ttb-input">
													<input type="text" id="CMEDATE4" name="CMEDATE4" class="text-input datetimepicker" size="10" value="" /> 
													<span class="input-unit CMEDATE4">
														<img src="${__ctx}/img/icon-7.svg" />
													</span>
												</div>
												<!-- 不在畫面上顯示的span -->
												<span id="hideblockf" >
												<!-- 驗證用的input -->
													<input id="Monthly_DateF" name="Monthly_DateF" type="text" class="text-input
													validate[funcCallRequired[validate_CheckDateScope['<spring:message code="LB.X1443" />',baseDate,Monthly_DateE, Monthly_DateF,true,null,null]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												</span>
											</div>
										</div>
									</span>
								</div>
							</div>
							<!-- 重新輸入 -->
							<spring:message code="LB.Re_enter" var="cmRest"></spring:message>
							<input type="button" name="CMRESET" id="CMRESET" value="${cmRest}" class="ttb-button btn-flat-gray">
							<input type="button" class="ttb-button btn-flat-orange" id="pageshow" value="<spring:message code="LB.Display_as_web_page" />"/>
						</div>
					</div>
				</form>					
				<!-- 說明： -->
		        <ol class="description-list list-decimal">
		        	<p><spring:message code="LB.Description_of_page"/></p>
		            <li><span>
		            	<spring:message code="LB.Download_txt_file" />
						<a target="_blank" href='${pageContext.request.contextPath}/public/N860_TXT.htm'>
							<spring:message code="LB.File_description" />
						</a> 
						<spring:message code="LB.X0079" />。
 					</span></li>
 					<li><span>
		            	<spring:message code="LB.Download_oldversion_txt_file" />
						<a target="_blank" href='${pageContext.request.contextPath}/public/N860_OLDTXT.htm'>
							<spring:message code="LB.File_description" />
						</a>。
 					</span></li>
		        </ol>
			</section>
			<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->
    <%@ include file="../index/footer.jsp"%>
<!--   Js function -->
    <script type="text/JavaScript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		initBlockUI();
		// 開始跑下拉選單並完成畫面
		init();
		// 解遮罩
		unBlockUI(initBlockId);
	});
	
	function init(){
		// 初始化時隱藏span
		$("#hideblocka").hide();
		$("#hideblockb").hide();
		$("#hideblockc").hide();
		$("#hideblockd").hide();
		$("#hideblocke").hide();
		$("#hideblockf").hide();
		
		creatACNO();
		
		datetimepickerEvent();
		
		getTmr();
		
		initFootable();
		
		$("#formId").validationEngine({
			binded: false,
			promptPosition: "inline"
		});	
		$("#pageshow").click(function(e){
			//打開驗證隱藏欄位
			$("#hideblocka").show();
			$("#hideblockb").show();
			$("#hideblockc").show();
			$("#hideblockd").show();
			$("#hideblocke").show();
			$("#hideblockf").show();
			//塞值進span內的input
			$("#Monthly_DateA").val($("#CMSDATE2").val());
			$("#Monthly_DateB").val($("#CMEDATE2").val());
			$("#Monthly_DateC").val($("#CMSDATE3").val());
			$("#Monthly_DateD").val($("#CMEDATE3").val());
			$("#Monthly_DateE").val($("#CMSDATE4").val());
			$("#Monthly_DateF").val($("#CMEDATE4").val());
			
			if(	$("input[type=radio][name=FGTYPE]:checked").val() =='DPCKTYPE1' ||
				$("input[type=radio][name=FGTYPE]:checked").val() =='DPCKTYPE3' ||
				$("input[type=radio][name=FGTYPE]:checked").val() =='DPCKTYPE4'){
				$("#Monthly_DateF").removeClass("validate[funcCallRequired[validate_CheckDateScope['<spring:message code="LB.X1443" />',baseDate,Monthly_DateE, Monthly_DateF,true,null,null]]]");
				$("#Monthly_DateF").addClass("validate[funcCallRequired[validate_CheckDateScope['<spring:message code="LB.X1443" />',baseDate,Monthly_DateE, Monthly_DateF,false,2,null]]]");
			}else{
				$("#Monthly_DateF").removeClass("validate[funcCallRequired[validate_CheckDateScope['<spring:message code="LB.X1443" />',baseDate,Monthly_DateE, Monthly_DateF,false,2,null]]]");
				$("#Monthly_DateF").addClass("validate[funcCallRequired[validate_CheckDateScope['<spring:message code="LB.X1443" />',baseDate,Monthly_DateE, Monthly_DateF,true,null,null]]]");
			}
			
			e = e || window.event;
			if( $('#CMPERIOD').prop('checked') )
			{
				if(checkTimeRange2() == false )
				{
					return false;
				}
				if(checkTimeRange3() == false )
				{
					return false;
				}
				if(checkTimeRange4() == false )
				{
					return false;
				}
			}
			
			if(!$('#formId').validationEngine('validate')){
	        	e.preventDefault();
 			}else{
 				$("#formId").validationEngine('detach');
 				initBlockUI();
 				$("#formId").attr("action","${__ctx}/NT/ACCT/collection_bills_result");
 	  			$("#formId").submit(); 
 			}		
  		});
		$("#CMRESET").click(function(e){
			$("#formId")[0].reset();
			getTmr();
		});

		$("input[type=radio][name=FGPERIOD]").change(function(){
			console.log(this.value);
			if(this.value == 'DPPERIOD1'){
				$(".DPPERIOD2_div").css("display","none");
				$(".DPPERIOD3_div").css("display","none");
				$(".DPPERIOD4_div").css("display","none");
			}else if(this.value == 'DPPERIOD2'){
				$(".DPPERIOD2_div").css("display","");
				$(".DPPERIOD3_div").css("display","none");
				$(".DPPERIOD4_div").css("display","none");
			}else if(this.value == 'DPPERIOD3'){
				$(".DPPERIOD2_div").css("display","none");
				$(".DPPERIOD3_div").css("display","");
				$(".DPPERIOD4_div").css("display","none");
			}else if(this.value == 'DPPERIOD4'){
				$(".DPPERIOD2_div").css("display","none");
				$(".DPPERIOD3_div").css("display","none");
				$(".DPPERIOD4_div").css("display","");
			}
		});
	}
	
	function getTmr() {
		var today = new Date("${time_now}");
		today.setDate(today.getDate());
		var y = today.getFullYear();
		var m = today.getMonth() + 1;
		var d = today.getDate();
		if(m<10){
			m = "0"+m
		}
		if(d<10){
			d="0"+d
		}
		var tmr = y + "/" + m + "/" + d
		$('#CMSDATE2').val(tmr);
		$('#CMEDATE2').val(tmr);
		$('#CMSDATE3').val(tmr);
		$('#CMEDATE3').val(tmr);
		$('#CMSDATE4').val(tmr);
		$('#CMEDATE4').val(tmr);
	}
    
	// 日曆欄位參數設定
	function datetimepickerEvent() {
		$(".CMSDATE2").click(function (event) {
			$('#CMSDATE2').datetimepicker('show');
		});
		$(".CMEDATE2").click(function (event) {
			$('#CMEDATE2').datetimepicker('show');
		});
		$(".CMSDATE3").click(function (event) {
			$('#CMSDATE3').datetimepicker('show');
		});
		$(".CMEDATE3").click(function (event) {
			$('#CMEDATE3').datetimepicker('show');
		});
		$(".CMSDATE4").click(function (event) {
			$('#CMSDATE4').datetimepicker('show');
		});
		$(".CMEDATE4").click(function (event) {
			$('#CMEDATE4').datetimepicker('show');
		});
		jQuery('.datetimepicker').datetimepicker({
			timepicker: false,
			closeOnDateSelect: true,
			scrollMonth: false,
			scrollInput: false,
			format: 'Y/m/d',
			lang: '${transfer}'
		});
	}	
	
	function checkTimeRange2()
	{
// 		var now = Date.now();
// 		var twoYm = 63115200000;
// 		var twoMm = 5259600000;
		
// 		var startT = new Date( $('#CMSDATE2').val() );
// 		var endT = new Date( $('#CMEDATE2').val() );
// 		var distance = now - startT;
// 		var range = endT - startT;
		
// 		var limitS = new Date(now - twoYm);
// 		var limitE = new Date(startT.getTime() + twoMm); 
// 		if(distance > twoYm)
// 		{
// 			var m = limitS.getMonth() + 1;
// 			var time = limitS.getFullYear() + '/' + m + '/' + limitS.getDate();
// 			// 起始日不能小於
// 			var msg = '<spring:message code="LB.Start_date_check_note_1" />' + time;
// 			alert(msg);
// 			return false;
// 		}
// 		else
// 		{
// 			if(range > twoMm)
// 			{
// 				var m = limitE.getMonth() + 1;
// 				var time = limitE.getFullYear() + '/' + m + '/' + limitE.getDate();
// 				// 終止日不能大於
// 				var msg = '<spring:message code="LB.End_date_check_note_1" />' + time;
// 				alert(msg);
// 				return false;
// 			}
// 		}
		return true;
	}
	
	function checkTimeRange3()
	{
// 		var now = Date.now();
// 		var twoYm = 63115200000;
// 		var twoMm = 5259600000;
		
// 		var startT = new Date( $('#CMSDATE3').val() );
// 		var endT = new Date( $('#CMEDATE3').val() );
// 		var distance = now - startT;
// 		var range = endT - startT;
		
// 		var limitS = new Date(now - twoYm);
// 		var limitE = new Date(startT.getTime() + twoMm); 
// 		if(distance > twoYm)
// 		{
// 			var m = limitS.getMonth() + 1;
// 			var time = limitS.getFullYear() + '/' + m + '/' + limitS.getDate();
// 			// 起始日不能小於
// 			var msg = '<spring:message code="LB.Start_date_check_note_1" />' + time;
// 			alert(msg);
// 			return false;
// 		}
// 		else
// 		{
// 			if(range > twoMm)
// 			{
// 				var m = limitE.getMonth() + 1;
// 				var time = limitE.getFullYear() + '/' + m + '/' + limitE.getDate();
// 				// 終止日不能大於
// 				var msg = '<spring:message code="LB.End_date_check_note_1" />' + time;
// 				alert(msg);
// 				return false;
// 			}
// 		}
		return true;
	}
	
	function checkTimeRange4()
	{
// 		var now = Date.now();
// 		var twoYm = 63115200000;
// 		var twoMm = 5259600000;
		
// 		var startT = new Date( $('#CMSDATE4').val() );
// 		var endT = new Date( $('#CMEDATE4').val() );
// 		var distance = now - startT;
// 		var range = endT - startT;
		
// 		var limitS = new Date(now - twoYm);
// 		var limitE = new Date(startT.getTime() + twoMm); 
// 		if(distance > twoYm)
// 		{
// 			var m = limitS.getMonth() + 1;
// 			var time = limitS.getFullYear() + '/' + m + '/' + limitS.getDate();
// 			// 起始日不能小於
// 			var msg = '<spring:message code="LB.Start_date_check_note_1" />' + time;
// 			alert(msg);
// 			return false;
// 		}
// 		else
// 		{
// 			if(range > twoMm)
// 			{
// 				var m = limitE.getMonth() + 1;
// 				var time = limitE.getFullYear() + '/' + m + '/' + limitE.getDate();
// 				// 終止日不能大於
// 				var msg = '<spring:message code="LB.End_date_check_note_1" />' + time;
// 				alert(msg);
// 				return false;
// 			}
// 		}
		return true;
	}
	
	//選項
	 function formReset() {
		//打開驗證隱藏欄位
		$("#hideblocka").show();
		$("#hideblockb").show();
		$("#hideblockc").show();
		$("#hideblockd").show();
		$("#hideblocke").show();
		$("#hideblockf").show();
		//塞值進span內的input
		$("#Monthly_DateA").val($("#CMSDATE2").val());
		$("#Monthly_DateB").val($("#CMEDATE2").val());
		$("#Monthly_DateC").val($("#CMSDATE3").val());
		$("#Monthly_DateD").val($("#CMEDATE3").val());
		$("#Monthly_DateE").val($("#CMSDATE4").val());
		$("#Monthly_DateF").val($("#CMEDATE4").val());
		
		if($('#CMPERIOD').prop('checked')){
			if(checkTimeRange2() == false){
				return false;
			}
			if(checkTimeRange3() == false){
				return false;
			}
			if(checkTimeRange4() == false){
				return false;
			}
		}
		if(!$("#formId").validationEngine("validate")){
			e = e || window.event;//forIE
			e.preventDefault();
		}
 		else{
// 		 	initBlockUI();
			if ($('#actionBar').val()=="excel"){
				$("#downloadType").val("OLDEXCEL");
				$("#templatePath").val("/downloadTemplate/collection_bills.xls");
		 	}else if ($('#actionBar').val()=="txt"){
				$("#downloadType").val("TXT");
				$("#templatePath").val("/downloadTemplate/collection_bills.txt");
		 	}else if ($('#actionBar').val()=="oldtxt"){
				$("#downloadType").val("OLDTXT");
				$("#templatePath").val("/downloadTemplate/collection_billsOLD");
		 	}

			$("#formId").attr("target", "");
            $("#formId").attr("action", "${__ctx}/NT/ACCT/collection_bills_ajaxDirectDownload");
            $("#formId").submit();
            $('#actionBar').val("");
// 			ajaxDownload("${__ctx}/NT/ACCT/collection_bills_ajaxDirectDownload","formId","finishAjaxDownload()");
 		}
	}
	
	function finishAjaxDownload(){
		$("#actionBar").val("");
		unBlockUI(initBlockId);
	}
		
	function creatACNO(){
		var options = { keyisval:true ,selectID:'#ACN'};
		uri = '${__ctx}'+"/NT/ACCT/getAcno_aj"
		console.log("getSelectData>>" + uri);
		rdata = {type: 'acno' };
		console.log("rdata>>" + rdata);
		data = fstop.getServerDataEx(uri,rdata,false);
		console.log("data>>", data);  
		if(data !=null && data.result == true ){
			fstop.creatSelect(data.data,options);
		}
	}
 	</script>
</body>
</html>
