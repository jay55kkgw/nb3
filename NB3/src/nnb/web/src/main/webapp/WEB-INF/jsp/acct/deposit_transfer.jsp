<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
		<!-- 讀卡機所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/cardReaderMethod.js"></script>
	<script type="text/javascript">
		var i18nValue = {};
		i18nValue['LB.X2593'] = '<spring:message code="LB.X2593" />';//個月
		i18nValue['LB.Year'] = '<spring:message code="LB.Year" />';//年
		i18nValue['time_s'] = '&nbsp;' + '<spring:message code="LB.Time_s" /> ';//空白+次數
		i18nValue['LB.Unlimited_1'] = '<spring:message code="LB.Unlimited_1" /> ';//無限
		var sMinDate = '${deposit_transfer.data.tmr}';
		var sMaxDate = '${deposit_transfer.data.nextYearDay}';
		var sMinMonth = '${deposit_transfer.data.nextMonth}'
		// 初始化
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 50);
			// 開始跑下拉選單並完成畫面
			setTimeout("init()", 400);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);

		});
		function init() {
			console.log("init>>");
			// 初始化時隱藏span
			$("#hideblock").hide();
			//表單驗證
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
			// 預約日期change事件 ，檢核日期邏輯
			fgtxdateEvent();	
			// 及時及預約標籤切換事件
			tabEvent();		
			// 把TXTIMES下拉選單選中的文字塞到隱藏欄位
			changeTxtimesDisplay();	
			// 轉出帳號change事件，要秀出可用餘額
			acnoEvent();		
			// 日曆欄位參數設定
			datetimepickerEvent();	
			// 根據選取存款種類改變存款期別 
			fdpaccEvent();
			// 建立到期是否轉期下拉選單  
			creatTXTIMES_Select();	
			// 存款期別或指定到期日 change事件 
			fgTdperiodEvent();		
			// 建立月天數的下拉選單
			creatCMDD_Select();		
			//轉出帳號切換事件
			transferTypeChange();
			// 到期是否轉期
			codeEvent();			
			// 確認鍵 click
			goOn();
			//重新整理
			cmreset();
//			建立約定及常用轉入帳號下拉選單
			creatInACNO();
			$("#DPTDTYPE3").trigger("change");
			$("#TransferType_01").trigger("change");
			fstop.setRadioChecked("FDPACC", "3", true);
			
			//若前頁有帶acn，轉出預設為此acn
			var getacn = '${deposit_transfer.data.Acn}';
			if(getacn != null && getacn != ''){
				$("#ACNO option[value= '"+ getacn +"' ]").prop("selected", true);
				$("#ACNO").change();
			}
			backData();
		}
		
//		建立約定及常用轉入帳號下拉選單
		function creatInACNO(){
			data = null;
			uri = '${__ctx}'+"/NT/ACCT/TRANSFER/getInAcno_aj"
			rdata = {type: 'CERTAGREEACNOLIST' };
			options = { keyisval:false ,selectID:'#DPAGACNO'}
			data = fstop.getServerDataEx(uri,rdata,false);
			
			console.log("creatInACNO.data: " + JSON.stringify(data));
			
			if(data !=null && data.data !=null && data.result == true && data.data.REC !=null){
				fstop.creatSelect(data.data.REC,options);
			}else{
				//alert('<spring:message code= "LB.Alert005" />');
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert005' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			}
		}
		
		//重新整理
		function cmreset(){
			//btn 
			$("#CMRESET").click(function () {
				$("#formId")[0].reset();
				$("#acnoIsShow").hide();
				$(".formError").remove();
				$('form').find('*').filter(':input:visible:first').focus();
			});
		}
		// 確認鍵 click
		function goOn(){
			$("#CMSUBMIT").click(function (e) {
				changeDisplay();// 判斷約定-常用非約定切換radio 
				//打開驗證隱藏欄位
				$("input[name='hideblock']").show();
				//塞值進span內的input
				$("#H_CMDATE").val($("#CMDATE").val());
				$("#H_AMOUNT").val($("#AMOUNT").val());
				$("#H_CMTRMAIL").val($("#CMTRMAIL").val());
				$("#H_DPACNO").val($("#DPACNO").val());
				if (!$('#formId').validationEngine('validate')) {
					e = e || window.event; // for IE
					e.preventDefault();
				} else {
					processQuery();
					$("#formId").validationEngine('detach');
					initBlockUI();//遮罩
					$("#formId").submit();
				}
			});
		}

		function processQuery(){
			 var TransferType = $('input[name=TransferType]:checked').val();
			    console.log('TransferType >>> ' + TransferType);
			if('PD'==TransferType){
				// 轉出帳號為約定
				$("#OUTACN").val($("#ACNO").val());
			}else if( $('input[name=TransferType]:checked').val() == 'NPD'){
				// 轉出帳號為非約定
				$("#OUTACN").val($("#OUTACN_NPD").val());
			}
		}
		
		//及時及預約標籤切換事件
		function tabEvent() {
			$("#nav-trans-now").click(function () {
				console.log("hi tabEvent>>");
				$("#transfer-date").hide();
				fstop.setRadioChecked("FGTXDATE", "1", true);
			})
			$("#nav-trans-future").click(function () {
				$("#transfer-date").show();
				fstop.setRadioChecked("FGTXDATE", "2", true);
			})
		}
		// 轉入帳號
		// 判斷約定-常用非約定切換radio 
		//if fgsvacno==1  把下拉選單選中的文字塞到隱藏欄位
		//if fgsvacno==2  '050-臺灣企銀 '+ $("#DPACNO").val()塞到隱藏欄位
		function changeDisplay() {
		    var fgsvacno = $('input[name=FGSVACNO]:checked').val();
		    console.log('fgsvacno >>> ' + fgsvacno);
		    if ('1' == fgsvacno) {
		        console.log("val" + $("#DPAGACNO").find(":selected").val())
		        $("#DPAGACNO_TEXT").val($("#DPAGACNO").find(":selected").text());
		        //約定
	        	$("#DPAGACNO").addClass("validate[funcCallRequired[validate_DPAGACNO[DPAGACNO]]]");
	        	//非約定
	        	$("#H_DPACNO").removeClass("validate[required ,custom[integer]");
		    } else if ('2'== fgsvacno) {
		    	console.log('050-<spring:message code= "LB.W0605" /> ' + $("#DPACNO").val());
		        $("#DPAGACNO_TEXT").val('050-<spring:message code= "LB.W0605" /> ' + $("#DPACNO").val());
		       	//非約定
		        $("#H_DPACNO").addClass("validate[required ,custom[integer]");
	        	//約定
	        	$("#DPAGACNO").removeClass("validate[funcCallRequired[validate_DPAGACNO[DPAGACNO]]]");
		    }
		}
		// 把TERM下拉選單選中的次數塞到隱藏欄位
		function changeTermDisplay() {
			var termval = $("#TERM").find(":selected").val();
			console.log("termval" + termval)
			if (termval == null) {
				$("#TERM_TEXT").val('1' + i18nValue['LB.Year']);
			} else {
				$("#TERM_TEXT").val($("#TERM").find(":selected").text());
			}
		}
		// 把TXTIMES下拉選單事件
		// 	 選取無限次數val=99，則轉存方式顯示可選 『本金轉期，利息轉入帳號』及『本金及利息一併轉期』(僅有整存整付 val =3 )
		//  選取有限次數，則轉存方式顯示僅可選 『本金轉期，利息轉入帳號』
		function changeTxtimesDisplay() {
			var txtimesval = $("#TXTIMES").find(":selected").val();
			console.log("txtimesval" + txtimesval);
			if (txtimesval == null) {
				$("#TXTIMES_TEXT").val('1' + i18nValue['time_s']);// 預設為1次
			} else {
				$("#TXTIMES_TEXT").val($("#TXTIMES").find(":selected").text() + i18nValue['time_s']);// 把TXTIMES下拉選單選中的文字塞到隱藏欄位
			}
			var fdpaccval = $('input[name=FDPACC]:checked').val();
			if ('99' == txtimesval && '3' == fdpaccval) {
				oDPSVTYPE2_TXT.style.display = "";//整存整付，可選 『本金轉期，利息轉入帳號』及『本金及利息一併轉期』。
			} else {
				oDPSVTYPE2_TXT.style.display = "none";//非整存整付，則僅可選 『本金轉期，利息轉入帳號』。
			}
			if(oDPSVTYPE2_TXT.style.display == 'none'){
		 		$("#DPSVTYPE1").click();
		 	}
		}

		// 根據選取種類改變存款期別 
		//  定存的存款種類有整存整付、存本取息、定期存款。
		// – 整存整付，可選 『本金轉期，利息轉入帳號』及『本金及利息一併轉期』。
		// – 非整存整付，則僅可選 『本金轉期，利息轉入帳號』。
		function fdpaccEvent() {
			$('input[type=radio][name=FDPACC]').change(function () {
				$('#TERM').find("option").remove();
				var options = {selectID: '#TERM'}
				var termdf = {};
			 	termdf[''] = '---'+'<spring:message code="LB.Select"/>'+'---'; //因為選擇清除下拉選單資料所以要加上  請選擇
			 	fstop.creatSelect(termdf, options);//建立請選擇
				if (this.value == '1') {
					creatTermYear();
					creatTermMathLessYear();
					oDPSVTYPE2_TXT.style.display = "none";//非整存整付，則僅可選 『本金轉期，利息轉入帳號』。
					creatTermMath();
				} else if (this.value == '2') {
					creatTermYear();
					creatTermMath();
					oDPSVTYPE2_TXT.style.display = "none";//非整存整付，則僅可選 『本金轉期，利息轉入帳號』。
				} else if (this.value == '3') {
					creatTermYear();
					creatTermMath();
					oDPSVTYPE2_TXT.style.display = "none";//整存整付，可選 『本金轉期，利息轉入帳號』 轉期非無限次 不可選 『本金及利息一併轉期』。
					var txtimesval = $("#TXTIMES").find(":selected").val();
					if(txtimesval == '99'){
						oDPSVTYPE2_TXT.style.display = "";//整存整付&轉期無限次 才可選 『本金轉期，利息轉入帳號』及『本金及利息一併轉期』。
					}
					
				}
			 	
			 	if(oDPSVTYPE2_TXT.style.display == 'none'){
			 		$("#DPSVTYPE1").click();
			 	}
			 	$('input[type=radio][name=FGTDPERIOD]').change();
			});
		}
		// 存款期別或指定到期日 change事件 
		function fgTdperiodEvent() {

			$('input[type=radio][name=FGTDPERIOD]').change(function () {
				if ($('input[type=radio][name=FGTDPERIOD]:checked').val() == '1') {
					console.log("fgTdperiodEvent 1");
					$("#TERM").addClass("validate[required]");
					if($('input[type=radio][name=FDPACC]:checked').val() == '1'){
						$("#CMDATE1").removeClass("validate[required ,funcCall[validate_CheckDate[<spring:message code= "LB.Designated_due_date" />,CMDATE1," + sMinMonth + "," + null + "]]]");
					}else{
						$("#CMDATE1").removeClass("validate[required ,funcCall[validate_CheckDate[<spring:message code= "LB.Designated_due_date" />,CMDATE1," + sMaxDate + "," + null + "]]]");
					}					
				}
				else if ($('input[type=radio][name=FGTDPERIOD]:checked').val() == '2') {
					console.log("fgTdperiodEvent 2");
					$("#TERM").removeClass("validate[required]");
					$("#CMDATE1").removeClass("validate[required ,funcCall[validate_CheckDate[<spring:message code= "LB.Designated_due_date" />,CMDATE1," + sMaxDate + "," + null + "]]]");
					$("#CMDATE1").removeClass("validate[required ,funcCall[validate_CheckDate[<spring:message code= "LB.Designated_due_date" />,CMDATE1," + sMinMonth + "," + null + "]]]");
					if($('input[type=radio][name=FDPACC]:checked').val() == '1'){
						$("#CMDATE1").addClass("validate[required ,funcCall[validate_CheckDate[<spring:message code= "LB.Designated_due_date" />,CMDATE1," + sMinMonth + "," + null + "]]]");
					}
					else{
						$("#CMDATE1").addClass("validate[required ,funcCall[validate_CheckDate[<spring:message code= "LB.Designated_due_date" />,CMDATE1," + sMaxDate + ",null]]]");
					}						
				}
			});
		}
		//轉出帳號切換事件
		function transferTypeChange(){
			$('input[type=radio][name=TransferType]').change(function () {
				console.log(this.value);
				if (this.value == 'PD') {
					console.log('TransferType01');
					$("#ACNO").addClass("validate[required ,funcCall[validate_CheckSelect[<spring:message code= "LB.Payers_account_no" />, ACNO, #]]]");
					$("#OUTACN_NPD").removeClass("validate[required ,funcCall[validate_CheckSelect[<spring:message code= "LB.Payers_account_no" />, OUTACN_NPD, '']]]");
					$("#OUTACN_NPD").val();
				}
				else if (this.value == 'NPD') {
					console.log('TransferType02');					
					$("#ACNO").val('#');
					$("#acnoIsShow").hide();
					$("#ACNO").removeClass("validate[required ,funcCall[validate_CheckSelect[<spring:message code= "LB.Payers_account_no" />, ACNO, #]]]");
					$("#OUTACN_NPD").addClass("validate[required ,funcCall[validate_CheckSelect[<spring:message code= "LB.Payers_account_no" />, OUTACN_NPD, '']]]");
				}
			});
		} 
		// 預約日期change事件 ，檢核日期邏輯
		function fgtxdateEvent() {
			$('input[type=radio][name=FGTXDATE]').change(function () {
				console.log("fgtxdateEvent value " + this.value);
				if (this.value == '1') {
					console.log("tomorrow");
					$("#H_CMDATE").removeClass();
				}else if (this.value == '2') {
					console.log("Transfer Thai Gayo");
					$("#H_CMDATE").addClass("validate[required ,funcCall[validate_CheckDate[<spring:message code= "LB.X1449" />,CMDATE," + sMinDate + "," + sMaxDate + "]]]");
					
				}
			});
			 $('input[type=radio][name=FGTXDATE]').trigger('change');
		}
		/**
		 * 到期是否轉
		 *	– 選取無限次數，則轉存方式顯示可選 『本金轉期，利息轉入帳號』及 『本金及利息一併轉期』(僅有整存整付)
		 *	– 選取有限次數，則轉存方式顯示僅可選 『本金轉期，利息轉入帳號』
		 *	– 選取消自動轉期，則沒有轉存方式並不顯示。
		 */
		function codeEvent() {
			$('input[type=radio][name=CODE]').change(function () {
				if (this.value == '0') {
					$('#TURNTYPE').hide();////選取消自動轉期，則沒有轉存方式並不顯示。
					$("#TXTIMES").removeClass("validate[required]")
				}
				else if (this.value == '1') {
					$('#TURNTYPE').show();
					$("#TXTIMES").addClass("validate[required]")
				}
			});
		}
		
		// 日曆欄位參數設定
		function datetimepickerEvent() {
			$(".CMDATE").click(function (event) {
				$('#CMDATE').datetimepicker('show');
			});
			$(".CMSDATE").click(function (event) {
				$('#CMSDATE').datetimepicker('show');
			});
			$(".CMEDATE").click(function (event) {
				$('#CMEDATE').datetimepicker('show');
			});
			jQuery('.datetimepicker').datetimepicker({
				timepicker: false,
				closeOnDateSelect: true,
				scrollMonth: false,
				scrollInput: false,
				format: 'Y/m/d',
				lang: '${transfer}'
			});
		}
		//轉出帳號change事件，要秀出可用餘額
		function acnoEvent() {
			$("#ACNO").change(function () {
				var acno = $('#ACNO :selected').val();
				console.log("acno>>" + acno);
				getACNO_Data(acno);
				$('#TransferType_01').trigger('click');
			});
		}
		//取得轉出帳號餘額資料
		function getACNO_Data(acno) {
			// 變更轉出帳號就隱藏重置再重新顯示
			$("#acnoIsShow").hide();
			uri = '${__ctx}' + "/NT/ACCT/TRANSFER/getACNO_Data_aj"
			console.log("getACNO_Data>>" + uri);
			rdata = { acno: acno };
			console.log("rdata>>" + rdata);
			fstop.getServerDataEx(uri, rdata, true, isShowACNO_Data);
		}
		//顯示轉出帳號餘額
		function isShowACNO_Data(data) {
			if (data != null && data.data.accno_data != null) {
				$("#acnoIsShow").show();
				var str = fstop.formatAmt(data.data.accno_data.ADPIBAL);//格式化金額
				console.log("str :" + str);
				$("#showText").html(str);
			} else {
				$("#acnoIsShow").hide();
			}
		}

		//年份選單資料
		function creatTermYear() {
			var options = {selectID: '#TERM'}
			//宣告變數，資料類型為物件 
			var dataTerm_1 = {}
			//建立選單資料
			dataTerm_1['12'] = '1' + i18nValue['LB.Year'];
			dataTerm_1['24'] = '2' + i18nValue['LB.Year'];
			dataTerm_1['36'] = '3' + i18nValue['LB.Year'];
			fstop.creatSelect(dataTerm_1, options);
		}
		//小於一年選單資料
		function creatTermMathLessYear() {
			var options = { selectID: '#TERM' }
			var dataTerm_2 = {}
			var dataTerm_3 = {}
			for (i = 1; i <= 9; i++) {
				dataTerm_2['0' + i] = i + i18nValue['LB.X2593'];
			}
			for (i = 10; i <= 11; i++) {
				dataTerm_3[i] = i + i18nValue['LB.X2593'];
			}
			fstop.creatSelect(dataTerm_2, options);
			fstop.creatSelect(dataTerm_3, options);
		}
		//大於一年的選單資料
		function creatTermMath() {
			var options = {selectID: '#TERM'}
			var dataTerm_4 = {}
			for (i = 13; i <= 35; i++) {
				if (i % 12 === 0) {
					continue;
				} else {
					dataTerm_4[i] = i + i18nValue['LB.X2593'];
				}
			}
			fstop.creatSelect(dataTerm_4, options);
		}
		
		//建立到期是否轉期下拉選單 
		function creatTXTIMES_Select() {
			//宣告變數，資料類型為物件 
			var dataTxTimes_1 = {}
			var dataTxTimes_2 = {}
			var options = { selectID: '#TXTIMES' }
			for (i = 1; i <= 9; i++) {
				dataTxTimes_1['0' + i] = i;
			}
			for (i = 10; i <= 36; i++) {
				dataTxTimes_2[i] = i;
			}
			dataTxTimes_2[99] = i18nValue['LB.Unlimited_1'];
			fstop.creatSelect(dataTxTimes_1, options);
			fstop.creatSelect(dataTxTimes_2, options);
		}
		//建立月天數的下拉選單
		function creatCMDD_Select() {
			var day = new Object();
			var options = { keyisval: false, selectID: '#CMDD' }
			for (var i = 1; i <= 31; i++) {
				day[i] = i;
			}
			fstop.creatSelect(day, options);
		}
		

		//copyTN
		function copyTN() {
			$("#CMMAILMEMO").val($("#CMTRMEMO").val());
		}
		//開啟通訊錄email選單
		function openAddressbook() {
			window.open('${__ctx}/NT/ACCT/TRANSFER/showAddressbook');
		}
		
// 		驗證轉入帳號下拉選單
	    function validate_DPAGACNO(field, rules, i, options){
			 var inputAttr=rules[i+2];
			 console.log("inputAttr>>"+inputAttr );
			 var dpagacno_val = $("#"+inputAttr).find(":selected").val()
			 
			 console.log("funccall test" );
			 console.log("funccall test>>"+options.allrules.required.alertText );
			 if(fstop.isEmptyString(dpagacno_val) || dpagacno_val.indexOf('#') > -1){
				 return options.allrules.required.alertText
			 }
		}
		
		function backData(){
			if('${deposit_transfer.data.back}' == "Y"){
				//即時 or 預約
				if('${deposit_transfer.data.FGTXDATE}' == '1'){
					$("#nav-trans-now").click();
				}
				else{
					$("#nav-trans-future").click();
					$("#CMDATE").val('${deposit_transfer.data.CMDATE}');
				}
				
				//轉出帳號
				if('${deposit_transfer.data.TransferType}' == "PD"){
					$("#TransferType_01").click();
					$("#ACNO").val('${deposit_transfer.data.ACN}');
					$("#ACNO").change();
				}
				else{
					$("#TransferType_02").click();
				}
				//轉入帳號
				if('${deposit_transfer.data.FGSVACNO}' == "1"){
					$("#FGSVACNO_01").click();
					$("#DPAGACNO").val('${deposit_transfer.data.DPAGACNO}');
				}
				else{
					$("#FGSVACNO_02").click();
					$("#DPACNO").val('${deposit_transfer.data.DPACNO}');
					if('${deposit_transfer.data.ADDACN}' == 'true'){
						$("#ADDACN").prop('checked',true);	
					}
				}
				//存款種類
				if('${deposit_transfer.data.FDPACC}' == "1"){
					$("#DPTDTYPE1").click();
				}
				else if('${deposit_transfer.data.FDPACC}' == "2") {
					$("#DPTDTYPE2").click();
				}
				else{
					$("#DPTDTYPE3").click();
				}
				//存款期別或指定到期日
				if('${deposit_transfer.data.FGTDPERIOD}' == "2"){
					$("#DPTDPERIOD2").click();
					$("#CMDATE1").val('${deposit_transfer.data.CMDATE1}');
				}
				else{
					$("#DPTDPERIOD1").click();
					$("#TERM").val('${deposit_transfer.data.TERM}');
				}
				//轉帳金額
				$("#AMOUNT").val('${deposit_transfer.data.AMOUNT}');
				//計息方式
				if('${deposit_transfer.data.INTMTH}' == "0"){
					$("#DPINTTYPE1").click();
				}
				else{
					$("#DPINTTYPE2").click();
				}
				//到期是否轉期
				if('${deposit_transfer.data.CODE}' == "1"){
					$("#DPREN1").click();
					$("#TXTIMES").val('${deposit_transfer.data.TXTIMES}');
					//轉存方式
					if('${deposit_transfer.data.FGSVTYPE}' == '2'){
						$("#DPSVTYPE2").click();
					}
					else{
						$("#DPSVTYPE1").click();
					}
					$("#TXTIMES").change();
				}
				else{
					$("#DPREN2").click();
				}
				//交易備註
				$("#CMTRMEMO").val('${deposit_transfer.data.CMTRMEMO}');
				//Email
				$("#CMTRMAIL").val('${deposit_transfer.data.CMTRMAIL}');
				//Email摘要
				$("#CMMAILMEMO").val('${deposit_transfer.data.CMMAILMEMO}');
			}
		}

	</script>
</head>

<body>
    
	<!-- 交易機制所需畫面    -->
	<%@ include file="../component/trading_component.jsp"%>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 定存服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Current_Deposit_To_Time_Deposit" /></li>
    <!-- 轉入綜存定存     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Open_Taiwan_Currency_Time_Deposit" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!--轉入臺幣綜存定存 -->
				<h2>
					<spring:message code="LB.Open_Taiwan_Currency_Time_Deposit" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form autocomplete="off" method="post" id="formId" action="${__ctx}/NT/ACCT/TDEPOSIT/deposit_transfer_confirm">
					<input type="hidden" name="DPAGACNO_TEXT" id="DPAGACNO_TEXT" />
					<input type="hidden" name="TERM_TEXT" id="TERM_TEXT" />
					<input type="hidden" name="TXTIMES_TEXT" id="TXTIMES_TEXT" />
					<!-- 轉出帳號 -->
					<input type="hidden" id="OUTACN" name="OUTACN" value="">
					<!--交易步驟 -->
					<div id="step-bar">
						<ul>
							<!--輸入資料 -->
							<li class="active"><spring:message code="LB.Enter_data" /></li>
							<!-- 確認資料 -->
							<li class=""><spring:message code="LB.Confirm_data" /></li>
							<!-- 交易完成 -->
							<li class=""><spring:message code="LB.Transaction_complete" /></li>
						</ul>
					</div>
					<!--交易步驟-END -->
					<!-- 表單顯示區 -->
					<div class="main-content-block row radius-50">
						<nav style="width: 100%;">
							<div class="nav nav-tabs" id="nav-tab" role="tablist">
								<!-- 即時 -->
								<a class="nav-item nav-link active" id="nav-trans-now" data-toggle="tab" href="#nav-trans-now" role="tab"
								 aria-controls="nav-home" aria-selected="true">
									<spring:message code="LB.Immediately" />
								</a>
								<!-- 預約 -->
								<a class="nav-item nav-link" id="nav-trans-future" data-toggle="tab" href="#nav-trans-future" role="tab"
								 aria-controls="nav-profile" aria-selected="false">
									<spring:message code="LB.Booking" />
								</a>
							</div>
						</nav>
						<div class="col-12 tab-content" id="nav-tabContent">
							<div class="tab-pane fade " id="nav-trans-future" role="tabpanel" aria-labelledby="nav-profile-tab">
							</div>
							<div class="ttb-input-block tab-pane fade show active" id="nav-trans-now" role="tabpanel" aria-labelledby="nav-home-tab">
								<div class="ttb-message">
									<span></span>
								</div>
								<!-- 這是即時的屬性  預設就是隱藏 -->
								<div class="ttb-input" style="display: none;">
									<label class="radio-block">
										<!-- 即時 -->
										<spring:message code="LB.Immediately" />
										<input type="radio" name="FGTXDATE" value="1" checked>
										<span class="ttb-radio"></span>
									</label>
								</div>
								<!-- 轉帳日期 -->
								<div id="transfer-date" class="ttb-input-item row" style="display: none;">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Transfer_date" /></h4>
										</label>
									</span>
									<span class="input-block">
										<!-- 預約 -->
										<div class="ttb-input ">
											<label class="radio-block">
												<spring:message code="LB.Booking" />
												<input type="radio" name="FGTXDATE" value="2">
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<input  type="text"  id="CMDATE" name="CMDATE" class="text-input datetimepicker validate[required]" value="${deposit_transfer.data.tmr}" autocomplete="off">
											<!-- 日曆元件 -->
											<span class="input-unit CMDATE"><img src="${__ctx}/img/icon-7.svg" /></span>
											<!-- 驗證用的span預設隱藏 -->
											<span name="hideblock" >
											<!-- 驗證用的input -->
											<input id="H_CMDATE" name="H_CMDATE" type="text" class="text-input validate[required, validate[required,funcCall[validate_CheckDate['<spring:message code= "LB.X1449" />', H_CMDATE ,'${deposit_transfer.data.tmr}', '${deposit_transfer.data.nextYearDay}']]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" autocomplete="off" />
											</span>
										</div>
									</span>
								</div>
								<!-- 轉出帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Payers_account_no" /></h4>
										</label>
									</span>
									<span class="input-block">
										<!-- 已約定 -->
										<div class="ttb-input">
											<label class="radio-block">
												<input type="radio" id="TransferType_01" name="TransferType" value="PD" checked>
												<spring:message code="LB.Designated_account" />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input ">
											<select name="ACNO" id="ACNO" class="custom-select select-input half-input">
												<!-- 請選擇約定帳號 -->
												<option value="#">----<spring:message code="LB.Select_designated_account" />-----</option>
												<c:forEach var="dataList" items="${ deposit_transfer.data.REC}">
													<option value="${dataList.ACN}">${dataList.ACN}</option>
												</c:forEach>
											</select>
											<!-- 可用餘額 : -->
											<div id="acnoIsShow" style="display: none">
												<span class="input-unit">
													<spring:message code="LB.Available_balance" />
													<span id="showText" class="input-unit "></span>
												</span>
											</div>
										</div>
										<!-- 非約定 -->
										<div class="ttb-input">
											<label class="radio-block">
											<spring:message code="LB.Out_nondesignated_account" />
											<input type="radio" id="TransferType_02" name="TransferType" value="NPD" onclick="listReaders();" />
											<span class="ttb-radio"></span>
											</label>
										</div>
										<!-- (晶片金融卡主帳號) -->
										<div class="ttb-input">
										<input type="text" name="OUTACN_NPD" id="OUTACN_NPD" class="text-input" readonly="readonly" value="" autocomplete="off"/>
										<span class="input-subtitle subtitle-color">
											<spring:message code="LB.X2361" />
										</span>
										</div>
									</span>
								</div>
								<!-- 轉入帳戶 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<!-- 轉入帳號（限綜合存款帳號） -->
											<h4><spring:message code="LB.Payees_account_no-Omnibus_account_no" /></h4>
										</label>
									</span>
									<span class="input-block">
										<!-- 已約定 -->
										<div class="ttb-input">
											<label class="radio-block">
											<spring:message code="LB.Designated_account" />/<spring:message code="LB.Common_non-designated_account" />
											<input type="radio" id="FGSVACNO_01" name="FGSVACNO" value="1" checked>
											<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
												<select id="DPAGACNO" name="DPAGACNO" class="custom-select select-input half-input" >
												</select>
										  </div>
										<!-- 非約定帳號 -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Non-designated_account" />
												<input type="radio"  id="FGSVACNO_02" name="FGSVACNO" value="2">
												<span class="ttb-radio"></span>
												050-
												<spring:message code= "LB.W0605" />
											</label>
										</div>
										<div class="ttb-input">
											<spring:message code="LB.Enter_Account" var="entAcn" />
											<input type="text" name="DPACNO" id="DPACNO" value="" class="text-input" placeholder="${entAcn}" size="11"maxlength="11" autocomplete="off">
											<label class="check-block">
												<!-- 加入常用帳號 -->
												<spring:message code="LB.Join_common_account" />
												<input type="checkbox" name="ADDACN" id="ADDACN" >
												<span class="ttb-check"></span>
											</label>
											<span  name="hideblock">
											<!-- 驗證用的input -->
												<input id="H_DPACNO" name="H_DPACNO" type="text" class="text-input " 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"  autocomplete="off"/>
											</span>
										</div>
									</span>
								</div>
								<!-- 存款種類 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Deposit_type" /></h4>
										</label>
									</span>
									<span class="input-block">
									<!-- 公司戶不得轉"存本取息"及"整存整付" -->
									<c:if test="${deposit_transfer.data.cidlength == '10'}">
										<!-- 定期儲蓄存款整存整付 -->
										<label class="radio-block">
											<input type="radio" name="FDPACC" id="DPTDTYPE3" value="3" />
											<spring:message code="LB.Regular_savings_deposits_Whole_deposit" />
											<span class="ttb-radio"></span>
										</label>
										<!-- 定期儲蓄存款存本取息 -->
										<label class="radio-block">
											<input type="radio" name="FDPACC" id="DPTDTYPE2" value="2" />
											<spring:message code="LB.Regular_savings_deposits_Deposit_interest" />
											<span class="ttb-radio"></span>
										</label>
									</c:if>	
										<!-- 定期存款 -->
										<label class="radio-block ">
											<input type="radio" name="FDPACC" id="DPTDTYPE1" value="1" />
											<spring:message code="LB.Time_deposit" />
											<span class="ttb-radio"></span>
										</label>
									</span>
								</div>
								<!-- 存款期別或指定到期日 -->
								<div class="ttb-input-item row">
									<!-- 存款期別或指定到期日 -->
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Deposit_period_or_Designated_due_date" /></h4>
										</label>
									</span>
									<span class="input-block">
										<!-- 存款期別 -->
										<div class="ttb-input">
											<label class="radio-block">
												<input type="radio" name="FGTDPERIOD" id="DPTDPERIOD1" value="1" checked />
												<spring:message code="LB.Deposit_period" />
												<!--空白置中 -->
												&nbsp;&nbsp;
												<span class="ttb-radio"></span>
											</label>
											<select class="custom-select select-input half-input validate[required]" name="TERM" id="TERM" onchange="changeTermDisplay()">
											</select>
										</div>
										<!-- 指定到期日 -->
										<div class="ttb-input">
											<label class="radio-block">
												<input type="radio" name="FGTDPERIOD" id="DPTDPERIOD2" value="2" />
												<spring:message code="LB.Designated_due_date" />
												<span class="ttb-radio"></span>
											</label>
											<input class="text-input datetimepicker " type="text" name="CMDATE1" id="CMDATE1" autocomplete="off"/>
										</div>
									</span>
								</div>
								<!-- 轉帳金額 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Amount" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<!-- 新台幣 -->
											<span class="input-unit">
												<spring:message code="LB.NTD" />
											</span>
											<input type="text" id="AMOUNT" name="AMOUNT" class="text-input" size="8" maxlength="8" value="" autocomplete="off">
											<!--元 -->
											<span class="input-unit">
												<spring:message code="LB.Dollar" />
											</span>
												<!-- 不在畫面上顯示的span -->
											<span name="hideblock" >
												<!-- 驗證用的input -->
												<input id="H_AMOUNT" name="H_AMOUNT" type="text" class="text-input validate[required ,custom[integer,min[10000] ,max[1000000]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" autocomplete="off" />
											</span>
										</div>
									</span>
								</div>
								<!-- 計息方式 -->
								<div class="ttb-input-item row">
									<!--  計息方式 -->
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Interest_calculation" /></h4>
										</label>
									</span>
									<span class="input-block">
										<!-- 機動 -->
										<label class="radio-block">
											<spring:message code="LB.Floating" />
											<input type="radio" name="INTMTH" id="DPINTTYPE1" value="0" checked="checked">
											<span class="ttb-radio"></span>
										</label>
										<!-- 固定 -->
										<label class="radio-block">
											<spring:message code="LB.Fixed" />
											<input type="radio" name="INTMTH" id="DPINTTYPE2" value="1">
											<span class="ttb-radio"></span>
										</label>
									</span>
								</div>
								<!-- 到期是否轉期 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Whether_the_automatic_rollover_is_due" /></h4>
										</label>
									</span>
									<span class="input-block">
										<!-- 轉期 -->
										<div class="ttb-input">
											<label class="radio-block">
												<input type="radio" name="CODE" id="DPREN1" value="1" checked />
												<spring:message code="LB.Is_renewal" />
												<span class="ttb-radio"></span>
											</label>
											<select class="custom-select select-input half-input w-auto" name="TXTIMES" id="TXTIMES" onchange="changeTxtimesDisplay()">
											</select>
											<!--次 　＊請參閱說明4 -->
											<font style="font-size: .875rem;"><spring:message code="LB.Time_s" />＊<spring:message code="LB.Reference_4" /></font>
										</div>
										<!-- 不轉期 -->
										<div class="ttb-input">
											<label class="radio-block">
												<input type="radio" name="CODE" id="DPREN2" value="0" />
												<spring:message code="LB.Not_automatic_renewal" />
												<span class="ttb-radio"></span>
											</label>
										</div>
									</span>
								</div>
								<!-- 轉存方式區塊 -->
								<div id="TURNTYPE" class="ttb-input-item row">
									<!--  轉存方式 -->
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Rollover_method" /></h4>
										</label>
									</span>
									<span class="input-block">
										<!-- 本金轉期，利息轉入綜存活存 -->
										<div class="ttb-input">
											<label class="radio-block">
												<input type="radio" name="FGSVTYPE" id="DPSVTYPE1" value="1" checked />
												<spring:message code="LB.Principal_rollover_interest_to_omnibus_account" />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!--  本金及利息一併轉期 -->
										<div class="ttb-input">
											<label id="oDPSVTYPE2_TXT" class="radio-block" style="display:none">
												<input type="radio" name="FGSVTYPE" id="DPSVTYPE2" value="2" />
												<spring:message code="LB.Renew_principal_and_interest" />
												<span class="ttb-radio"></span>
											</label>
										</div>
									</span>
								</div>
								<!-- 交易備註 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Transfer_note" /></h4>
										</label>
									</span> 
									<!--(可輸入20字的用途摘要，您可於「轉出紀錄查詢」功能中查詢) -->
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="CMTRMEMO" name="CMTRMEMO" class="text-input" maxlength="20" value="" autocomplete="off">
											<span class="input-unit"></span>
											<span class="input-remarks"><spring:message code="LB.Transfer_note_note" /></span>
										</div>
									</span>
								</div>
								<!-- 轉出成功 Email通知 -->
								<div class="ttb-input-item row">
									<!-- 轉出成功Email 通知 -->
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.X0479" />
											:</h4>
										</label>
									</span>
									<span class="input-block">
										<!-- 通知本人 -->
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color">
												<spring:message code="LB.Notify_me" />
												：
											</span>
											<span class="input-subtitle subtitle-color">
												${sessionScope.dpmyemail}
											</span>
										</div>
										<!--另通知 -->
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color">
												<spring:message code="LB.Another_notice" />
												：
											</span>
										</div>
										<!-- 通訊錄 -->
										<div class="ttb-input">
											<!--非必填 -->
										<spring:message code="LB.Not_required" var="notrequired"></spring:message>
											<input class="text-input"  type="text" id="CMTRMAIL" name="CMTRMAIL"  maxlength="500" placeholder="${notrequired}" autocomplete="off"> 
											<span class="input-unit"></span>
											<!-- window open 去查 TxnAddressBook  參數帶入身分證字號 -->
											<button class="btn-flat-orange" type="button" onclick="openAddressbook()">
												<spring:message code="LB.Address_book" />
											</button>
												<!-- 不在畫面上顯示的span -->
											<span name="hideblock" >
												<!-- 驗證用的input -->
												<input id="H_CMTRMAIL" name="H_CMTRMAIL" type="text" class="text-input validate[funcCall[validate_EmailCheck[CMTRMAIL]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" autocomplete="off" />
											</span>
										</div>
										<!--摘要內容-->
										<div class="ttb-input">
											<spring:message code="LB.Summary" var="summary"></spring:message>
											<input type="text" id="CMMAILMEMO" name="CMMAILMEMO"  class="text-input"  maxlength="20" placeholder="${summary}" autocomplete="off">
											<span class="input-unit"></span>
											<!-- 同交易備註 -->
											<button class="btn-flat-orange" type="button" onclick="copyTN()">
												<spring:message code="LB.As_transfer_note" />
											</button>
										</div>
									</span>
								</div>
							</div>
							<!-- button -->
   								<!--重新輸入 -->
								<spring:message code="LB.Re_enter" var="cmRest" />
								<input type="button" name="CMRESET" id="CMRESET" value="${cmRest}" class="ttb-button btn-flat-gray">
								<!-- 確定 -->
								<spring:message code="LB.Confirm" var="cmSubmit" />
								<input type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" class="ttb-button btn-flat-orange">
							<!-- 						buttonEND -->
						</div>
					</div>
					<div class="text-left">
						<!-- 說明： -->
					 
						<ol class="description-list list-decimal">
							<p><spring:message code="LB.Description_of_page"/></p>
							<li>
							<span><spring:message code="LB.Deposit_transfer_P1_D1-1"/>
							<a href="https://www.tbb.com.tw/web/guest/-82" target="_blank"><spring:message code="LB.Deposit_transfer_P1_D1-2"/></a>
							<a href="${__ctx}/CUSTOMER/SERVICE/rate_table" target="_blank"><spring:message code="LB.Deposit_transfer_P1_D1-3"/></a>
							</span>
							</li>
							<li><span><spring:message code="LB.Deposit_transfer_P1_D2"/></span></li>
							<li><span><spring:message code="LB.Deposit_transfer_P1_D3"/></span></li>
							<li><span><spring:message code="LB.Deposit_transfer_P1_D4"/></span></li>
							<li><span><spring:message code="LB.Deposit_transfer_P1_D5"/></span></li>
							<li><span><spring:message code="LB.Deposit_transfer_P1_D6"/></span></li>
							<li><span><spring:message code="LB.Deposit_transfer_P1_D7"/></span></li>
						</ol>
					</div>
				</form>
			</section>
			<!-- main-content END -->
		</main>
	</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>