<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()",10);
		// 開始查詢資料並完成畫面
		setTimeout("init()",20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)",500);
		
		setTimeout("initDataTable()",100);
	});
	
	function init(){
		//initFootable();		
		//上一頁按鈕
		$("#CMBACK").click(function() {
			initBlockUI();
			fstop.getPage('${pageContext.request.contextPath}'+'/NT/ACCT/BOND/bond_detail','', '');
		});
		//列印
		$("#printbtn").click(function(){
			var params = {
					"jspTemplateName":"bond_detail_result_print",
					"jspTitle":"<spring:message code= "LB.W0042" />",
					"CMQTIME":"${bond_detail_result.data.CMQTIME}",
					"CMPERIOD":"${bond_detail_result.data.CMPERIOD}",
					"CMRECNUM":"${bond_detail_result.data.CMRECNUM}"
				};
				openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
		});				
	}
	//繼續查詢
	function processQuery(){
 		$("#reRec").attr("action","${__ctx}/NT/ACCT/BOND/bond_detail_result");
 		$("#reRec").submit();
	}
	//下載
 	function formReset() {
 		initBlockUI();
		if ($('#actionBar').val()=="excel"){
			$("#downloadType").val("OLDEXCEL");
			$("#templatePath").val("/downloadTemplate/bond_detail.xls");
 		}else if ($('#actionBar').val()=="txt"){
			$("#downloadType").val("TXT");
			$("#templatePath").val("/downloadTemplate/bond_detail.txt");
 		}
		ajaxDownload("${__ctx}/ajaxDownload","formId","finishAjaxDownload()");
	}
	function finishAjaxDownload(){
		$("#actionBar").val("");
		unBlockUI(initBlockId);
	}
</script>
</head>
	<body>
		<!-- header     -->
		<header>
			<%@ include file="../index/header.jsp"%>
		</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 中央登錄債券查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0033" /></li>
    <!-- 明細查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0042" /></li>
		</ol>
	</nav>



		<!-- menu、登出窗格 -->
		<div class="content row">
			<%@ include file="../index/menu.jsp"%>
			<!-- 	快速選單內容 -->
			<!-- content row END -->
			<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2><spring:message code="LB.W0042" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 						下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
<!-- 						下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
					</select>
				</div>
<%-- 				<c:if test="${bond_detail_result.data.TOPMSG == 'OKOV'}"> --%>
<%-- 					<div class="MessageBar"><spring:message code="LB.W0076" /> --%>
<%-- 	       				<input id="CMCONTINUEQ" type="button" value="<spring:message code="LB.W0151" />" name="CMCONTINUEQ" class="ttb-sm-btn btn-flat-orange" onClick="processQuery()"> --%>
<!-- 	       			</div> -->
<%-- 	       		</c:if> --%>
				<form id="formId" method="post" action="${__ctx}/download">
				<!-- 下載用 -->
				<input type="hidden" name="downloadFileName" value="<spring:message code="LB.W0042" />"/>
				<input type="hidden" name="downloadType" id="downloadType"/> 					
				<input type="hidden" name="templatePath" id="templatePath"/> 
				<input type="hidden" name="CMQTIME" value="${bond_detail_result.data.CMQTIME}"/>
				<input type="hidden" name="CMPERIOD" value="${bond_detail_result.data.CMPERIOD}"/>
				<input type="hidden" name="CMRECNUM" value="${bond_detail_result.data.CMRECNUM}"/>
				<input type="hidden" name="hasMultiRowData" value="true"/>
				<!-- EXCEL下載用 -->
				<input type="hidden" name="headerRightEnd" value="1" />
				<input type="hidden" name="headerBottomEnd" value="5" />
				<input type="hidden" name="multiRowStartIndex" value="9" />
                <input type="hidden" name="multiRowEndIndex" value="9" />
                <input type="hidden" name="multiRowCopyStartIndex" value="6" />
                <input type="hidden" name="multiRowCopyEndIndex" value="10" />
                <input type="hidden" name="multiRowDataListMapKey" value="rowListMap" />
                <input type="hidden" name="rowRightEnd" value="9" />
				<!-- TXT下載用 -->
				<input type="hidden" name="txtHeaderBottomEnd" value="5"/>
				<input type="hidden" name="txtMultiRowStartIndex" value="12"/>
				<input type="hidden" name="txtMultiRowEndIndex" value="12"/>
				<input type="hidden" name="txtMultiRowCopyStartIndex" value="7"/>
				<input type="hidden" name="txtMultiRowCopyEndIndex" value="12"/>
				<input type="hidden" name="txtMultiRowDataListMapKey" value="rowListMap"/>
				<!-- 顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
							<ul class="ttb-result-list">
								<li>
									<!-- 查詢時間 -->
									<h3><spring:message code="LB.Inquiry_time" />：</h3>
									<p>
										${bond_detail_result.data.CMQTIME}
									</p>
								</li>
								<li>
									<!-- 交易起迄日 -->
									<h3><spring:message code="LB.Inquiry_period_1"/>：</h3>
									<p>
										${bond_detail_result.data.CMPERIOD}
									</p>
								</li>
								<li>
									<!-- 單位 -->
									<h3><spring:message code="LB.W1466" />：</h3>
									<p>
										<spring:message code="LB.X0423" />
									</p>
								</li>
								<li>	
									<!-- 資料總數 -->
									<h3><spring:message code="LB.Total_records"/>：</h3>
									<p>
										${bond_detail_result.data.CMRECNUM}&nbsp;<spring:message code="LB.Rows"/>
									</p>
								</li>
<!-- 								<li>	 -->
<!-- 									帳號 -->
<%-- 									<h3><spring:message code="LB.Account"/>：</h3> --%>
<!-- 									<p> -->
<%-- 										${bond_detail_result.data.labelList.ACN} --%>
<!-- 									</p> -->
<!-- 								</li> -->
							</ul>
							<!-- 中央登錄債券明細 -->
							<c:forEach var="tableList" items="${bond_detail_result.data.labelList}">
								<ul class="ttb-result-list">
									<!-- 帳號 -->
									<li>
										<h3><spring:message code="LB.Account"/></h3>
										<p>${tableList.ACN}</p>
									</li>
								</ul>
								<table class="stripe table-striped ttb-table dtable" data-show-toggle="first">
									<thead>
<%-- 										<tr> --%>
<!-- 											帳號 -->
<%-- 											<th><spring:message code="LB.Account"/></th> --%>
<%-- 											<th class="text-left" colspan="8">${tableList.ACN}</th> --%>
<%-- 										</tr> --%>
										<tr>
											<!-- 交易日期 -->
											<th>
												<spring:message code="LB.Transaction_date"/>
											</th>
											<!-- 債券代號-->
											<th>
												<spring:message code="LB.W0036" />
											</th>
											<!-- 摘要 -->
											<th>
												<spring:message code="LB.Summary_1"/>
											</th>
											<!-- 轉出面額-->
											<th>
												<spring:message code="LB.W0045" />
											</th>
											<!-- 轉入面額 -->
											<th>
												<spring:message code="LB.W0046" />
											</th>
											<!-- 餘額-->
											<th>
												<spring:message code="LB.Account_balance_2"/>
											</th>
											<!-- 限制性轉出餘額 -->
											<th>
												<spring:message code="LB.W0039" />
											</th>
											<!-- 限制性轉入餘額 -->
											<th>
												<spring:message code="LB.W0040" />
											</th>
											<!-- 附條件簽發餘額-->
											<th>
												<spring:message code="LB.W0041" />
											</th>
										</tr>
									</thead>
									<tbody>
										<c:if test="${not empty tableList.TOPMSG}">
											<tr>
												<td class="text-center">${tableList.TOPMSG}</td>
												<td class="text-center">${tableList.ADMSGOUT}</td>
												<td>
												<td>
												<td>
												<td>
												<td>
												<td>
												<td>
											</tr>
										</c:if>
										<c:if test="${empty tableList.TOPMSG}">
											<c:forEach var="dataList" items="${tableList.rowListMap}">
												<tr>
													<td class="text-center">${dataList.PAYDATE}</td>
													<td class="text-center">${dataList.BONCOD}</td>
													<td class="text-center">${dataList.TRNTYP}</td>
													<td class="text-right">${dataList.OUTAMT}</td>
													<td class="text-right">${dataList.INPAMT}</td>
													<td class="text-right">${dataList.DPIBAL}</td>
													<td class="text-right">${dataList.LMTSFO}</td>
													<td class="text-right">${dataList.LMTSFI}</td>
													<td class="text-right">${dataList.RPBAL}</td>
												</tr>
											</c:forEach>
										</c:if>
									</tbody>
								</table>
							</c:forEach>
						<c:if test="${bond_detail_result.data.TOPMSG == 'OKOV'}">
							<div class="MessageBar">
								<spring:message code="LB.X0076" />
								<input id="CMCONTINUEQ" type="button"
									value="<spring:message code="LB.X0151" />" name="CMCONTINUEQ"
									class="ttb-sm-btn btn-flat-orange" onClick="processQuery()">
							</div>
						</c:if>
						<!-- 回上頁-->
                            <input type="button" class="ttb-button btn-flat-gray" name="CMBACK" id="CMBACK" value="<spring:message code="LB.Back_to_previous_page"/>"/>
							<!-- 列印鈕-->					
							<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>	
						</div>
					</div>
				</form>
				<form method="post" id="reRec">
					<input type="hidden" id="USERDATA_X50" name="USERDATA_X50" value="${bond_detail_result.data.USERDATA_X50}">
                    <input type="hidden" id="ACN" name="ACN" value="${bond_detail_result.data.ACN}" />
                    <input type="hidden" id="CMSDATE" name="CMSDATE" value="${bond_detail_result.data.CMSDATE}" />
                    <input type="hidden" id="CMEDATE" name="CMEDATE" value="${bond_detail_result.data.CMEDATE}" />
                </form>
				</section>
			</main>
			<!-- 		main-content END -->
			<!-- 	content row END -->
		</div>
		<%@ include file="../index/footer.jsp"%>
	</body>
</html>