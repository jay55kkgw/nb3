<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
	    <script type="text/javascript">
        $(document).ready(function () {
        	init();
        });

        function init() {
			$("#formId").validationEngine({binded: false,promptPosition: "inline"});
        	tabEvent();
        	acnoEvent();
	    	$("#pageshow").click(function(e){		
	    		if(eval($("#PALPAY").val()) > 2000000)
	    		{
	    			//alert("<spring:message code= "LB.Alert132" />");
	    			errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert132' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
	    			return false;
	    		}
// 	    		if($("#CMTRMAIL").val().length > 0){
// 	    			$("#CMTRMAIL").addClass("validate[funcCallRequired[validate_EmailCheck[CMTRMAIL]]]");
// 	    		}else{
// 	    			$("#CMTRMAIL").removeClass("validate[funcCallRequired[validate_EmailCheck[CMTRMAIL]]]");
// 	    		}
				var fgtxdate = $('input[name="FGTXDATE"]:checked').val();
				if(fgtxdate == '1'){
					$("#PAYDATE").val($("#DATADATE").val());
				}else if(fgtxdate == '2'){
					$("#PAYDATE").val($("#CMTRDATE").val());
				}
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					$("#CUSIDN2").prop("disabled",false);
		        	initBlockUI();
					var action = '${__ctx}/LOAN/ADVANCE/repay_advance_p3';
					$("form").attr("action", action);
	    			$("form").submit();
				}
			});
			$("#CMRESET").click(function(e){
				$("#formId")[0].reset();
			});
        }
		function copy(){
			$("#CMMAILMEMO").val($("#CMTRMEMO").val());
		}
		

		// 即時、預約標籤切換事件
		function tabEvent(){
			var flag = '${ inputData.disabledFlag }';
			if(flag == 'Y'){
				// 即時
				$("#nav-trans-now").addClass('active');
				$("#nav-trans-future").addClass('disabled');
				$("#nav-trans-future").css('background-color','#e8e8e8');
				$("#transfer-date-now").show();
				$("#transfer-date-future").hide();
			}else{
				// 預約
				$("#nav-trans-now").addClass('disabled');
				$("#nav-trans-now").css('background-color','#e8e8e8');
				$("#nav-trans-future").addClass('active');
				$("#transfer-date-now").hide();
				$("#transfer-date-future").show();
			}
		}
		
		// 轉出帳號change事件，要秀出選擇的轉出帳號可用餘額
		function acnoEvent(){
			$("#ACN_OUT").change(function() {
				var acno = $('#ACN_OUT :selected').val();
				console.log("acnoEvent.acno: " + acno);
				getACNO_Data(acno);
			});
		}
		// 取得轉出帳號餘額資料
		function getACNO_Data(acno){
			var options = { keyisval:true ,selectID:'#OUTACN'};
			
			uri = '${__ctx}'+"/NT/ACCT/TRANSFER/getACNO_Data_aj"
			rdata = {acno: acno};
			console.log("getACNO_Data.uri: " + uri);
			console.log("getACNO_Data.rdata: " + rdata);
			
			fstop.getServerDataEx(uri,rdata,false,isShowACNO_Data);
		}
		// 顯示轉出帳號餘額
		function isShowACNO_Data(data){
			var i18n= new Object();
			i18n['available_balance'] = '<spring:message code="LB.Available_balance" />: ';
			console.log("isShowACNO_Data.data: " + JSON.stringify(data));
			
			if(data && data.result){
				// 顯示可用餘額DIV
				$("#acnoIsShow").show();
				
				// 可用餘額
				console.log("data.data.accno_data: " + data.data.accno_data.ADPIBAL);
				
				// 格式化金額欄位
				i18n['available_balance'] += fstop.formatAmt(data.data.accno_data.ADPIBAL);
				$("#showText").html(i18n['available_balance']);
			}else{
				$("#acnoIsShow").hide();
			}
		}
    </script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 貸款服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Loan_Service" /></li>
    <!-- 借款查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Loan_Inquiry" /></li>
    <!-- 借款明細查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Loan_Detail_Inquiry" /></li>
    <!-- 貸款提前償還本金     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0906" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!-- 預約黃金交易查詢/取消 -->
					<!-- <spring:message code="LB.NTD_Demand_Deposit_Detail" /> -->
					<spring:message code="LB.X0906" /><!-- 貸款提前償還本金-->
				</h2>
				
					
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 交易流程階段 -->
				<div id="step-bar">
					<ul>
						<li class="active"><spring:message code="LB.Enter_data" /></li>
						<li class=""><spring:message code="LB.Confirm_data" /></li>
						<li class=""><spring:message code="LB.Transaction_complete" /></li>
					</ul>
				</div>
					
                <form id="formId" method="post">
   					<input type="hidden" id="PAYDATE" name="PAYDATE" value="">
   					<input type="hidden" id="DATADATE" name="DATADATE" value="${inputData.nowDay}">
   					<input type="hidden" id="DPMYEMAIL" name="DPMYEMAIL" value="${inputData.DPMYEMAIL}">
                    <!-- 顯示區  -->
                    <div class="main-content-block row radius-50">
						<!-- 即時、預約導引標籤 -->
						<nav style="width: 100%;">
							<div class="nav nav-tabs" id="nav-tab" role="tablist">
								<a class="nav-item nav-link" id="nav-trans-now" data-toggle="tab" href="#nav-trans-now" 
									role="tab" aria-controls="nav-home" aria-selected="false">
									<spring:message code="LB.Immediately" />
								</a> 
								<a class="nav-item nav-link" id="nav-trans-future" data-toggle="tab" href="#nav-trans-future" 
									role="tab" aria-controls="nav-profile" aria-selected="true">
									<spring:message code="LB.Booking" />
								</a>
							</div>
						</nav>
                        <div class="col-12">
							<div class="NA01_3">
								<div class="head-line"><spring:message code="LB.X0906" /></div><!-- 貸款提前償還本金-->
							</div>
							
							<div class="ttb-input-block">
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.Transfer_date" /><!-- 轉帳日期 -->
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<div id="transfer-date-now" class="ttb-input-item row" style="display: none;">
												<label class="radio-block">
													<spring:message code="LB.Immediately" /><!-- 即時 -->
													<c:if test="${ inputData.disabledFlag.equals('Y') }">
														<input type="radio" name="FGTXDATE" id="FGTXDATE1" value="1" checked />
													</c:if>
													<c:if test="${ !inputData.disabledFlag.equals('Y') }">
														<input type="radio" name="FGTXDATE" id="FGTXDATE1" value="1" disabled />
													</c:if>
													<span class="ttb-radio"></span>
												</label>
											</div>
											<div id="transfer-date-future" class="ttb-input-item row" style="display: none;">
												<label class="radio-block">
													<spring:message code="LB.Booking" /><!-- 預約 -->
													<c:if test="${ inputData.disabledFlag.equals('N') }">
														<input type="radio" name="FGTXDATE" id="FGTXDATE2" value="2" checked />
													</c:if>
													<c:if test="${ !inputData.disabledFlag.equals('N') }">
														<input type="radio" name="FGTXDATE" id="FGTXDATE2" value="2" disabled/>
													</c:if>
													<span class="ttb-radio"></span>
												</label>
												<input type="text" id="CMTRDATE" name="CMTRDATE" class="text-input" value="${inputData.nextDay}" size="10" maxlength="10" disabled/>
											</div>
										</div>
									</span>
								</div>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.Payers_account_no" /><!-- 轉出帳號 -->
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<select name="ACN_OUT" id="ACN_OUT" class="custom-select select-input validate[required]">
												<option value="">
													---<spring:message code="LB.Select_account" /><!-- 請選擇帳號 -->---
												</option>
												<c:forEach var="dataList" items="${n920_data.data.REC}">
													<option> ${dataList.ACN}</option>
												</c:forEach>
											</select>
<!-- 											約定 轉出帳號 餘額 -->
											<div id="acnoIsShow">
												<span id="showText" class="input-unit"></span>
											</div>
										</div>
									</span>
								</div>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.W0891" /><!-- 貸款帳號 -->
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${inputData.LoanACN}
											<input type="hidden" id="ACN" name="ACN" value="${inputData.LoanACN}" />
										</div>
									</span>
								</div>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.W1707" /><!-- 貸款分號 -->
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${inputData.LoanSEQ}
											<input type="hidden" id="SEQ" name="SEQ" value="${inputData.LoanSEQ}" />
										</div>
									</span>
								</div>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.Amount" /><!-- 轉帳金額 -->
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span class="validate[funcCallRequired[validate_Check_Amount[轉帳金額,PALPAY,1000,9999999999,ture]]]">
												<spring:message code="LB.NTD" /><!-- 新臺幣 -->
												<input type="text" id="PALPAY" name="PALPAY" class="text-input" size="13" maxlength="11"/> 
												<spring:message code="LB.Dollar_1" /><!-- 元 -->
											</span>
        									<!-- (還本金額至少須大於或等於「應繳金額」，倘還本小於「應繳金額」則還本金額須為千元之倍數，惟當日限額NTD200萬元) -->
        									<span class="input-remarks" style="color:red"><spring:message code="LB.W1710" /></span>
										</div>
									</span>
								</div>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.Transfer_note" /><!-- 交易備註 -->
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="CMTRMEMO" name="CMTRMEMO" class="text-input" size="56" maxlength="20"/>
										</div>
									</span>
								</div>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.Email_notification" /><!-- Email通知(可不填) -->
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<c:if test="${ !inputData.DPMYEMAIL.equals('') && inputData.sendMe.equals('Y') }">
												<span class="input-subtitle subtitle-color"><spring:message code="LB.Notify_me" /><!-- 通知本人 -->：</span>
												<span class="input-subtitle subtitle-color">${ inputData.DPMYEMAIL }</span>
											</c:if>
										</div>
										<div class="ttb-input">
											<span>
												<c:if test="${ !inputData.DPMYEMAIL.equals('') && inputData.sendMe.equals('Y') }">
													<span class="input-subtitle subtitle-color"><spring:message code="LB.Another_notice" />：<!-- 另通知 --></span>
												</c:if>
												<c:if test="${ inputData.DPMYEMAIL.equals('') || inputData.sendMe.equals('N') }">
													<span class="input-subtitle subtitle-color"><spring:message code="LB.Email" />：<!-- 電子信箱 --></span>
												</c:if>
												<input type="text" id="CMTRMAIL" name="CMTRMAIL" placeholder="<spring:message code="LB.Not_required" />" class="text-input validate[funcCallRequired[validate_CheckMail['<spring:message code= "LB.e-mail" />',CMTRMAIL,true]]]" size="40" maxlength="500"/>
												<input type="button" value="<spring:message code="LB.Address_book" />" name="CMADDRESS"  class="ttb-sm-btn btn-flat-orange" onClick="window.open('${__ctx}/LOAN/ADVANCE/AddressBook')"><!-- 通訊錄 -->
											</span>
										</div>
										<div class="ttb-input">
											<span>
												<input type="text" id="CMMAILMEMO" name="CMMAILMEMO" class="text-input" placeholder="<spring:message code="LB.Summary" />" size="56" maxlength="20"/>
												<input type="button" value="<spring:message code="LB.As_transfer_note" />" name="CMCOPY"  class="ttb-sm-btn btn-flat-orange" onClick="copy();"><!-- 同交易備註 -->
											</span>
										</div>
									</span>
								</div>
							</div>
                           
	                        <!--button 區域 -->
								<!-- 重新輸入 -->
								<spring:message code="LB.Re_enter" var="cmRest"></spring:message>
								<input type="button" name="CMRESET" id="CMRESET" value="${cmRest}" class="ttb-button btn-flat-gray">
	                            <input id="pageshow" name="pageshow" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm"/>" />
                        <!--                     button 區域 -->
                        </div>  
                    </div>
					<!--說明： -->
					<div class="text-left">
						
						<ol class="list-decimal text-left description-list">
						<p><spring:message code="LB.Description_of_page" /></p>
							<li><spring:message code="LB.repay_advance_P1_D1" /></li>
						</ol>
					</div>
                </form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>