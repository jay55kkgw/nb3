<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ include file="../__import_js_u2.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<!-- <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script> -->
		<title>${jspTitle}</title>
		<script type="text/javascript">
			$(document).ready(function(){
				window.print();
			});
		</script>
	</head>
	<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
		<br><br>
		<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
		<br>
		<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
		<!-- 查詢時間 -->
		<p>
		<spring:message code="LB.Inquiry_time" />：<label>${CMQTIME}</label>
		</p>
		<!-- 交易起迄日 -->
		<p>
		<spring:message code="LB.Inquiry_period_1"/>：<label>${CMPERIOD}</label>
		</p>
		<!-- 單位 -->
		<p>
		<spring:message code= "LB.W1466" />：<spring:message code= "LB.X0423" />
		</p>	
		<!-- 資料總數 -->
		<p>
		<spring:message code="LB.Total_records"/>：<label>${CMRECNUM} <spring:message code="LB.Rows" /></label>
		</p>
		<main class="col-12">					
			<!-- 中央登錄債券明細 -->
			<c:forEach var="labelListMap" items="${print_datalistmap_data}">
				<table class="print" style="text-align:center; margin-bottom:20px;">
					<thead>
						<tr>
							<!-- 帳號 -->
							<th><spring:message code="LB.Account"/></th>
							<th class="text-left" colspan="8">${labelListMap.ACN}</th>
						</tr>
						<tr>
							<!-- 交易日期 -->
							<th><spring:message code="LB.Transaction_date"/></th>
							<!-- 債券代號-->
							<th><spring:message code= "LB.W0036" /></th>
							<!-- 摘要 -->
							<th><spring:message code="LB.Summary_1"/></th>
							<!-- 轉出面額-->
							<th><spring:message code= "LB.W0045" /></th>
							<!-- 轉入面額 -->
							<th><spring:message code= "LB.W0046" /></th>
							<!-- 餘額-->
							<th><spring:message code="LB.Account_balance_2"/></th>
							<!-- 限制性轉出餘額 -->
							<th><spring:message code= "LB.W0039" /></th>
							<!-- 限制性轉入餘額 -->
							<th><spring:message code= "LB.W0040" /></th>
							<!-- 附條件簽發餘額-->
							<th><spring:message code= "LB.W0041" /></th>
						</tr>
					</thead>
					<tbody>
						<c:if test="${not empty labelListMap.TOPMSG}">
							<tr>
								<td class="text-center">${labelListMap.TOPMSG}</td>
								<td class="text-center">${labelListMap.ADMSGOUT}</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</c:if>
						<c:if test="${empty labelListMap.TOPMSG}">
							<c:forEach var="dataList" items="${labelListMap.rowListMap}">
								<tr>
									<td class="text-center">${dataList.PAYDATE}</td>
									<td class="text-center">${dataList.BONCOD}</td>
									<td class="text-center">${dataList.TRNTYP}</td>
									<td class="text-right">${dataList.OUTAMT}</td>
									<td class="text-right">${dataList.INPAMT}</td>
									<td class="text-right">${dataList.DPIBAL}</td>
									<td class="text-right">${dataList.LMTSFO}</td>
									<td class="text-right">${dataList.LMTSFI}</td>
									<td class="text-right">${dataList.RPBAL}</td>
								</tr>
							</c:forEach>
						</c:if>
					</tbody>
				</table>
			</c:forEach>
		</main>
	</body>
</html>