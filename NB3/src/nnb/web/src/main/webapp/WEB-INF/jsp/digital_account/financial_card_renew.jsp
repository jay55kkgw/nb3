<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript"
	src="${__ctx}/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css"
	href="${__ctx}/css/validationEngine.jquery.css">
<link rel="stylesheet" type="text/css"
	href="${__ctx}/css/jquery.datetimepicker.css">

<script type="text/javascript">
	$(document).ready(function() {
		init();
		//initKapImg();
	});
	function init() {
		$("#CMSUBMIT").click(
				function(e) {
			    	$("#formId").validationEngine({binded:false,promptPosition: "inline" });
					
					$('#CUSIDN').val($('#CUSIDN').val().toUpperCase())
					//TODO 驗證
					if(!$('#formId').validationEngine('validate')){
			        	e.preventDefault();
		 			}else{
						console.log("submit~~");
						initBlockUI(); //遮罩
						$("#formId").attr("action",
								"${__ctx}/DIGITAL/ACCOUNT/financial_card_renew_step1");
						$("#formId").submit();
		 			}
				});
	}
	// 刷新驗證碼
	function refreshCapCode() {
		console.log("refreshCapCode...");

		// 驗證碼
		$('input[name="capCode"]').val('');
		
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

		// 登入失敗解遮罩
		unBlockUI(initBlockId);
	}

	// 刷新輸入欄位
	function changeCode() {
		console.log("changeCode...");
		
		// 清空輸入欄位
		$('input[name="capCode"]').val('');
		
		// 刷新驗證碼
		refreshCapCode();
	}

	// 初始化驗證碼
	function initKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
	}

	// 生成驗證碼
	function newKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').click(function() {
			refreshCapCode();
		});
	}
</script>
</head>

<body>
<!-- 	 header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Register" /></li>
    <!-- 數位存款帳戶     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0348" /></li>
    <!-- 數位存款帳戶補申請晶片金融卡     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1553" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<!-- 數位存款帳戶補申請晶片金融卡 -->
			<h2>補申請晶片金融卡</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<div id="step-bar">
            	<ul>
                	<li class="active">身分驗證</li>
                    <li class="">顧客權益</li>
                    <li class="">申請資料</li>
					<li class="">確認資料</li>
					<li class="">完成申請</li>
               </ul>
            </div>
            
			
										
			<form id="formId" method="post" action="">
				<div class="main-content-block row">
					<div class="col-12 tab-content">
                        <div class="ttb-input-block">
                            <div class="ttb-message">
                                <p>身份驗證</p>
                            </div>
                            <p class="form-description">請輸入您的身分證字號</p>

                             <!-- 身分證字號 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D0581"/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <input type="text" id="CUSIDN" name="CUSIDN" class="text-input validate[required, funcCall[validate_checkSYS_IDNO[CUSIDN]]]" maxlength="10" value="" placeholder="請輸入身分證字號"/>
                                    </div>
                                </span>
                            </div>
                            
                            
                        </div>
                        <spring:message code="LB.Confirm" var="cmSubmit"></spring:message>
                        <input type="BUTTON" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Cancel"/>" name="" onclick="window.close();">
						<input type="BUTTON" class="ttb-button btn-flat-orange"name="CMSUBMIT" id="CMSUBMIT" value="<spring:message code="LB.X0080"/>" />

                    </div>					
				</div>
			</form>
		</section>
		</main>
	<!-- 		main-content END -->
	<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>

</html>