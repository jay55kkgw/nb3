<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">


<script type="text/javascript">
$(document).ready(function() {
	// HTML載入完成後開始遮罩
	setTimeout("initBlockUI()", 50);
	// 開始跑下拉選單並完成畫面
	setTimeout("init()", 400);
	// 解遮罩
	setTimeout("unBlockUI(initBlockId)", 500);
	// 初始化時隱藏span
	$("#hideblock").hide();
	// 將.table變更為footable
	//initFootable();
	setTimeout("initDataTable()",100);
});

function init(){
	
	$("#formId").validationEngine({
		binded: false,
		promptPosition: "inline"
	});
	
	//確定 
	$("#CMSUBMIT").click(function() {
		
		//計算勾選筆數
		var count = 0;
		$("input[type='checkbox']:checkbox:checked").map(function(){
			count++;
			return $(this).val();
		})
		
		if(count == 0){
			//alert("<spring:message code= "LB.Alert167" />");	
			errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert167' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			e.preventDefault();
		}else if(count > 10){
			//alert("<spring:message code= "LB.Alert165" />");	
			errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert165' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			e.preventDefault();
			count = 0;
		}
		
	    if (!$('#formId').validationEngine('validate')) {
	        e.preventDefault();
	    } else {
	    	$("#formId").validationEngine('detach');
				initBlockUI();
				$("#formId").attr("action","${__ctx}/ONLINE/SERVING/predesignated_account_logout_s");
	  			$("#formId").submit(); 
	    }
	});
	
}

	//全選
	function allCheck() {
		var check = $("#check").val();
		if(${predesignated_account_logout.data.REC.size()} <= 10){
		    if(check == 0) {
		        $("input[type='checkbox']").each(function() {
		            $(this).prop("checked", true);
		        });
		        $("#check").val(1);
		     } else {
		        $("input[type='checkbox']").each(function() {
		            $(this).prop("checked", false);
		        });
		        $("#check").val(0);
		     }
		}else{
			//筆數超過10筆，請勿點選全選按鈕
	    	//alert("<spring:message code= 'LB.X2125' />");
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.X2125' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
		}
	};


</script>
</head>
<body>
	<!--   IDGATE -->
	<%@ include file="../idgate_tran/idgateAlert.jsp" %>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 約定轉入帳號取消     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0268" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code="LB.X0268"/></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<input type="hidden" id="check" value="0"/>
			<form id="formId" method="post" action="">
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<!-- 線上約定轉入帳號註銷表 -->
						<ul class="ttb-result-list"></ul>
						<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
							<thead>
							<tr>
								<th><input type="button" id="checkAll" name="checkAll" class="ttb-sm-btn btn-flat-orange" onclick="allCheck()" value="<spring:message code="LB.All_select"/>"></th>
								<th><spring:message code="LB.Bank_name"/></th>
								<th><spring:message code="LB.D0227" /></th>
								<th><spring:message code="LB.Favorite_name"/></th>
								<th><spring:message code="LB.D0239" /></th>
							</tr>
							</thead>
							
							<c:if test="${empty predesignated_account_logout.data.REC}"> 
							<tbody>
							<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							</tr>
							</tbody>
							</c:if>
							<c:if test="${not empty predesignated_account_logout.data.REC}"> 
							<tbody>
							<c:forEach var="dataList" items="${predesignated_account_logout.data.REC}" varStatus="data">
									<tr>
										<td align="center">&nbsp;&nbsp;&nbsp;&nbsp;
											<label class="check-block">
												<input type="checkbox" name="Checkbox${data.count}" value="${dataList.ACN},${dataList.BNKCOD},<c:out value='${dataList.DPGONAME}' />"/>
	                                            <span class="ttb-check"></span>&nbsp;
	                                        </label>
										</td>
										<td class="text-center">${dataList.BNKCOD}</td>
										<td class="text-center">${dataList.ACN}</td>
										<td class="text-center"><c:out value='${dataList.DPGONAME}' /></td>
										<td class="text-center">${dataList.RAFLAG}</td>
									</tr>
								</c:forEach>		
							</tbody>
							</c:if>
								
							
						</table>
						<!-- 不在畫面上顯示的span -->
<!-- 						<span id="hideblock" > -->
<!-- 						驗證用的input -->
<!-- 						<input id="ErrorMsg" name="ErrorMsg" type="text" class="text-input validate[groupRequired[Checkbox]]"  -->
<!-- 							style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" value="" /> -->
<!-- 						</span> -->
						<!-- 重新輸入 -->
						<input type="reset" id="reset" onclick="clearData()" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter"/>"/>
						<!-- 確定 -->
                        <input type="button" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm"/>" >
					</div>
				</div>
			</form>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>