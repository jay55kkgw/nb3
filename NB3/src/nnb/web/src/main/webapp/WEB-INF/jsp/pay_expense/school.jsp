<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=BIG5">
		<%@ include file="../__import_head_tag.jsp"%>
		<%@ include file="../__import_js.jsp" %>
		<script type="text/javascript">
			$(document).ready(function() {
				setTimeout("initDataTable()",100);
	
			});
			</script>
		
	</head>
	<body>
		<main class="col-12"> 
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
				<form method="post" id="formId" action="">
					<h2>代收學校列表</h2>
					<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
					<div class="main-content-block row">
						<div class="col-12 printClass">
							<br>
						    <table class="stripe table-striped ttb-table dtable" data-toggle-column="first">

	                            <tbody>
					      			<tr>
										<td class="text-left Line-break">美奇幼兒園</td>
									</tr>	
					      			<tr>
										<td class="text-left Line-break">亞太創意技術學院</td>
									</tr>	
					      			<tr>
										<td class="text-left Line-break">中華科技大學</td>
									</tr>	
					      			<tr>
										<td class="text-left Line-break">華岡幼兒園</td>
									</tr>	
					      			<tr>
										<td class="text-left Line-break">明新科技大學</td>
									</tr>	
					      			<tr>
										<td class="text-left Line-break">國立彰化女子高級中學</td>
									</tr>	
					      			<tr>
										<td class="text-left Line-break">國立二林高級工商職業學校</td>
									</tr>	
					      			<tr>
										<td class="text-left Line-break">巨人高級中學</td>
									</tr>	
					      			<tr>
										<td class="text-left Line-break">吳鳳科技大學</td>
									</tr>	
					      			<tr>
										<td class="text-left Line-break">文藻外語大學</td>
									</tr>	
					      			<tr>
										<td class="text-left Line-break">復華高級中學</td>
									</tr>	
					      			<tr>
										<td class="text-left Line-break">正修科技大學</td>
									</tr>	
					      			<tr>
										<td class="text-left Line-break">高雄應用科技大學</td>
									</tr>	
								</tbody>
							</table>
							<br>
						</div>
					</div>
				</form>	
			</section>
		</main> <!-- 		main-content END -->
	</body>
</html>