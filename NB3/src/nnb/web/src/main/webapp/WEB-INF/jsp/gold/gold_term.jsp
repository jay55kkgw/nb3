<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	
	$("#okButton").click(function(){
		$("#GOLDAGREEFLAG").val("Y");
		//黃金買進
		if(${ADOPID == "N09001"}){
			$("#formID").attr("action","${__ctx}/GOLD/TRANSACTION/gold_buy_select");
		}
		//黃金回售
		else if(${ADOPID == "N09002"}){
			$("#formID").attr("action","${__ctx}/GOLD/TRANSACTION/gold_resale_select");
		}
		//黃金預約買進
		else if(${ADOPID == "N09101"}){
			//BEN
			$("#formID").attr("action","${__ctx}/GOLD/TRANSACTION/gold_booking_select");
		}
		//黃金預約回售
		else if(${ADOPID == "N09102"}){
			$("#formID").attr("action","${__ctx}/GOLD/TRANSACTION/gold_reserve_resale_select");
		}
		$("#formID").submit();
	});
	$("#cancelButton").click(function(){
		$("#formID").attr("action","${__ctx}/INDEX/index");
		$("#formID").submit();
	});
});
</script>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 黃金交易     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X2272" /></li>
		</ol>
	</nav>

	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
			<!-- 臺灣企銀網路銀行辦理黃金存摺約定條款 -->
				<h2><spring:message code= "LB.X0927" /></h2>
				<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
					<form id="formID" method="post">
					<input type="hidden" id="GOLDAGREEFLAG" name="GOLDAGREEFLAG"/>
					<input type="hidden" name="ADOPID" value="${ADOPID}"/>
					<input type="hidden" name="ACN" value="${reqACN}"/>
					<div class="main-content-block row">
						<div class="col-12">
						<div style="width: 75%; margin: auto;">
						<div class="ttb-message">
							<h4>
							<!-- 黃金存摺網路交易申請暨約定書 -->
								<b><spring:message code= "LB.X0928" /></b>
							</h4>
						</div>
						<div class="ttb-message">
							<p align="left">
								<font color="royalblue" size="3"><b><spring:message code= "LB.X1208" /><span
										class="Cron"><spring:message code="LB.W1554"/></span><spring:message code= "LB.X1209" /><span
										class="Cron"><spring:message code="LB.Cancel"/></span><spring:message code= "LB.X1210" />
								</b></font>
							</P>
						</div>
						<table style="text-align: left">
							<tr>
								<td style="width: 5em; text-align: center" rowspan=2>第一條</td>
								<td colspan=2><b>服務時間</b></td>
							</tr>
							<tr>
								<td colspan=5>本人（以下簡稱立約人）以臺灣中小企業銀行（以下簡稱貴行）之網路銀行服	務系統（以下簡稱本系統），辦理黃金存摺之買進、回售、預約買進、預約回售、定期定額申購、定期定額約定變更或查詢資料等服務時，應於貴行網站公告之服務時間內為之。</td>
							</tr>
							<tr>
								<td style="width: 5em; text-align: center" rowspan=2>第二條</td>
								<td colspan=3><b>約定帳號（即網路銀行交易指定帳戶）</b></td>
							</tr>
							<tr>
								<td colspan=5>
									以本系統辦理黃金存摺之買進、回售時，立約人之約定轉出帳號及約定轉入帳號，即黃金存摺帳號與新臺幣存款帳號，限立約人於貴行開立之帳戶，立約人之扣款帳號如未預先約定或因結清銷戶或其他任何原因，致該扣款帳號不存在時，立約人應以臨櫃方式辦理買進或回售。
									<br>
									<font color="red">立約人若於完成黃金存摺之預約買進、預約回售後，辦理變更約定新臺幣存款帳號者，該筆預約交易於預約後的第一個營業日仍依原約定新臺幣存款帳號作為黃金買進扣款帳號、黃金回售入帳帳號。</font>
								</td>
							</tr>
							<tr>
								<td style="width: 5em; text-align: center" rowspan=2>第三條</td>
								<td colspan=2><b>不可抗力</b></td>
							</tr>
							<tr>
								<td colspan=5>
									如因不可抗力事由或其他原因，包括但不限於斷電、斷線、電信壅塞、網路傳輸干擾、貴行之電腦系統故障或第三人破壞等，致使立約人所為交易或其他指示遲延完成或無法按立約人指示完成、或致	使貴行未能提供本系統服務者，立約人同意貴行不負任何賠償責任。立約人預約交易後第一營業日若因天然災害或其他不可抗力之原因，致無黃金牌告價時，則該預約交易無效。
								</td>
							</tr>
							<tr>
								<td style="width: 5em; text-align: center" rowspan=2>第四條</td>
								<td colspan=2><b>系統服務</b></td>
							</tr>
							<tr>
								<td colspan=5>
									立約人同意貴行於貴行網站公告本系統新增或異動（含調整、變更或取消）之服務項目及其相關規定時，除貴行另有規定外，立約人無須另填申請書，即可使用本系統新增或異動後之服務項目；立約人一經進入本系統並使用該新增或異動後之服務項目時，即視為立約人同意依貴行網站所公告本系統新增或異動服務項目之相關規定辦理，且同意受該規定拘束。
								</td>
							</tr>
							<tr>
								<td style="width: 5em; text-align: center" rowspan=2>第五條</td>
								<td colspan=2><b>投資風險</b></td>
							</tr>
							<tr>
								<td colspan=5>
									<b><U>國際黃金價格有漲有跌，存戶投資黃金可能產生本金收益或損失，最大可能損失為買進金額之全部，請自行審慎判斷投資時機並承擔投資風險，辦理黃金存摺各項交易，如有涉及贈與、繼承及應繳稅捐等情事，悉由存戶或繼承人自行申報與負擔，黃金存摺不計算利息，亦非屬存款保險條例規定之標的，不受存款保險保障。</U></b>
								</td>
							</tr>
							<tr>
								<td style="width: 5em; text-align: center" rowspan=2>第六條</td>
								<td colspan=2>立約人應一併遵守其與貴行訂定之黃金存摺開戶約定條款之約定及相關法令之規定。</td>
							</tr>
							<tr>
								<td colspan=5>
									
								</td>
							</tr>
							<tr>
								<td style="width: 5em; text-align: center" rowspan=2>第七條</td>
								<td colspan=2>立約人同意貴行得依業務需要，隨時修改本約定書開立帳戶之相關服務內容，並在貴行網站上公告其內容，以代通知，修改後之交易，立約人願自動適用異動後之服務內容，毋須另行約定。</td>
							</tr>
							<tr>
								<td colspan=5>
									
								</td>
							</tr>
							<tr>
								<td style="width: 5em; text-align: center" rowspan=2>第八條</td>
								<td colspan=2><b>手續費收費標準</b></td>
							</tr>
							<tr>
								<td colspan=2>
									立約人同意依貴行下列所訂之收費標準繳納相關費用，並授權貴行自立約人之帳戶內自動扣繳；收費標準於訂約後如有變更或調整，貴行應於生效日六十日前以顯著方式於營業場所、貴行網站公告其內容，並以電子郵件方式使立約人得知調整費用，立約人若對於該變更或調整有異議時，得於前開公告期間內終止本約定事項，逾期未終止者，視為同意該變更或調整：一、線上申請黃金存摺帳戶，每戶收費新台幣50元。二、黃金存摺網路交易定期定額扣款成功，每戶收費新台幣50元
								</td>
							</tr>
						</table>
						<br>
						</div>	
	                  			<input type="button" id="cancelButton" value="<spring:message code="LB.Cancel"/>" class="ttb-button btn-flat-gray"/>
	                  			<input type="button" id="okButton" value="<spring:message code="LB.W1554"/>" class="ttb-button btn-flat-orange"/>
					</div>
					</div>
					</form>
				</div>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>