<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="card-detail-block">
	<div class="card-center-block">

		<!-- Q1 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-1" aria-expanded="true"
				aria-controls="popup4-1">
				<div class="row">
					<span class="col-1">Q1</span>
					<div class="col-11">
						<span>憑證註冊中心服務項目包括哪些？</span>
					</div>
				</div>
			</a>
			<div id="popup4-1" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>提供金融XML憑證申請、更新、暫禁、解禁及廢止等服務。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q2 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-2" aria-expanded="true"
				aria-controls="popup4-2">
				<div class="row">
					<span class="col-1">Q2</span>
					<div class="col-11">
						<span>XML憑證的使用範圍？</span>
					</div>
				</div>
			</a>
			<div id="popup4-2" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>
								可使用於網路銀行、電子票據等系統各項功能之交易放行，與交易密碼之主要差別為可應用於非約定轉帳及非本人費用之繳費等高風險交易。
							</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q3 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-3" aria-expanded="true"
				aria-controls="popup4-3">
				<div class="row">
					<span class="col-1">Q3</span>
					<div class="col-11">
						<span>如何申請使用XML憑證？</span>
					</div>
				</div>
			</a>
			<div id="popup4-3" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>
								請親攜身分證明文件、存款立約印鑑及存摺，至本行填寫「臺灣企銀金融XML憑證申請/暫禁/解禁/廢止申請書」辦理憑證相關約定手續。客戶完成憑證約定手續，取得本行發給之憑證識別資料（Common
								Name，CN）後，可登入憑證註冊中心申請簽發憑證。身分證明文件，法人為公司登記證明文件、負責人身分證件；自然人為身分證件。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q4 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-4" aria-expanded="true"
				aria-controls="popup4-4">
				<div class="row">
					<span class="col-1">Q4</span>
					<div class="col-11">
						<span>如何登入憑證註冊中心？</span>
					</div>
				</div>
			</a>
			<div id="popup4-4" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>
								請上網至本行網路銀行入口網站(https://ebank.tbb.com.tw)-&gt;輸入網路銀行登入資料-&gt;點選e化服務項下之「憑證註冊中心」-&gt;輸入身分證/營利事業統一編號及網路銀行交易密碼，即可登入憑證註冊中心。
							</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q5 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-5" aria-expanded="true"
				aria-controls="popup4-5">
				<div class="row">
					<span class="col-1">Q5</span>
					<div class="col-11">
						<span>登入憑證註冊中心申請憑證時，電腦是否須設定或調整？</span>
					</div>
				</div>
			</a>
			<div id="popup4-5" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>

<!-- 							<p> -->
<!-- 								A5：請由網路銀行入口網登入後，再行登入「憑證註冊中心」，參考線上說明之「憑證載具驅動程式安裝流程」及常見問題中之「網路環境設定篇」。<br> -->
<!-- 								<img alt="" src="./fstop/images/RAimage054.jpg" border="0">&nbsp;&nbsp;&nbsp;&nbsp; -->
<!-- 								<img alt="" src="./fstop/images/RAimage055.jpg" border="0"> -->
<!-- 							</p> -->
							<p>
								請由網路銀行入口網登入後，再行登入「憑證註冊中心」，參考線上說明之「憑證載具驅動程式安裝流程」及常見問題中之「網路環境設定篇」。<br>
							</p>

						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q6 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-6" aria-expanded="true"
				aria-controls="popup4-6">
				<div class="row">
					<span class="col-1">Q6</span>
					<div class="col-11">
						<span>憑證申請、更新、暫禁..等各項功能如何操作？</span>
					</div>
				</div>
			</a>
			<div id="popup4-6" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
<!-- 							<p> -->
<!-- 								A6：客戶可登入憑證註冊中，點選「線上說明」，即可得知各項作業之操作流程。<br> <img alt="" -->
<!-- 									src="./fstop/images/RA01.jpg" border="0"> -->
<!-- 							</p> -->
							<p>
								客戶可登入憑證註冊中，點選「線上說明」，即可得知各項作業之操作流程。
							</p>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q7 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-7" aria-expanded="true"
				aria-controls="popup4-7">
				<div class="row">
					<span class="col-1">Q7</span>
					<div class="col-11">
						<span>憑證有效期限多長？</span>
					</div>
				</div>
			</a>
			<div id="popup4-7" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>目前為1年，憑證到期前30日客戶應登入憑證註冊中心辦理憑證更新（展期）。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q8 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-8" aria-expanded="true"
				aria-controls="popup4-8">
				<div class="row">
					<span class="col-1">Q8</span>
					<div class="col-11">
						<span>申請/更新憑證需支付之費用金額？</span>
					</div>
				</div>
			</a>
			<div id="popup4-8" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>

							<p>法人戶憑證每張NTD900元，個人戶每張NTD150元，使用期限1年。</p>

						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q9 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-9" aria-expanded="true"
				aria-controls="popup4-9">
				<div class="row">
					<span class="col-1">Q9</span>
					<div class="col-11">
						<span>如何查詢憑證是否已到期？</span>
					</div>
				</div>
			</a>
			<div id="popup4-9" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div class="ttb-result-list terms">
							<ol>
								<li>方式一：於臺灣企銀網路銀行首頁(https://ebank.tbb.com.tw)，點選『環境設定』,下載「9.臺灣企銀憑證載具讀取工具下載」,依「臺灣企銀憑證載具讀取工具介紹」操作說明查看「憑證有效到期日」。
								</li>
								<li>方式二：查詢/更新/下載憑證-&gt;點「內容」，查看「憑證有效到期日」。</li>
							</ol>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>

	</div>

	<!-- Q10 -->
	<div class="ttb-pup-block" role="tab">
		<a role="button" class="ttb-pup-title collapsed d-block"
			data-toggle="collapse" href="#popup4-10" aria-expanded="true"
			aria-controls="popup4-10">
			<div class="row">
				<span class="col-1">Q10</span>
				<div class="col-11">
					<span>如何進行憑證更新展期？</span>
				</div>
			</div>
		</a>
		<div id="popup4-10" class="ttb-pup-collapse collapse" role="tabpanel">
			<div class="ttb-pup-body">
				<ul class="ttb-pup-list">
					<li>

<!-- 						<p> -->
<!-- 							A10：客戶可於憑證到期日前30日，登入憑證註冊中心，點選「用戶更新憑證」，並選取欲展期之憑證後方之鈕，即可進行憑證展期的動作。（詳細操作流程請參考憑證註冊中心之線上說明）。<br> -->
<!-- 							<img src="./fstop/images/RA03.jpg"> -->
<!-- 						</p> -->
						<p>
							客戶可於憑證到期日前30日，登入憑證註冊中心，點選「用戶更新憑證」，並選取欲展期之憑證後方之鈕，即可進行憑證展期的動作。（詳細操作流程請參考憑證註冊中心之線上說明）。<br>
						</p>

					</li>
				</ul>
			</div>
		</div>
	</div>

	<!-- Q11-->
	<div class="ttb-pup-block" role="tab">
		<a role="button" class="ttb-pup-title collapsed d-block"
			data-toggle="collapse" href="#popup4-11" aria-expanded="true"
			aria-controls="popup4-11">
			<div class="row">
				<span class="col-1">Q11</span>
				<div class="col-11">
					<span>憑證過期時(無法執行憑證更新)如何處理？</span>
				</div>
			</div>
		</a>
		<div id="popup4-11" class="ttb-pup-collapse collapse" role="tabpanel">
			<div class="ttb-pup-body">
				<ul class="ttb-pup-list">
					<li>

						<p>請參照Q3至本行申請憑證，取得本行發給之憑證識別資料（Common
							Name，CN）後，即可登入憑證註冊中心申請簽發憑證。</p>

					</li>
				</ul>
			</div>
		</div>
	</div>

	<!-- Q12 -->
	<div class="ttb-pup-block" role="tab">
		<a role="button" class="ttb-pup-title collapsed d-block"
			data-toggle="collapse" href="#popup4-12" aria-expanded="true"
			aria-controls="popup4-12">
			<div class="row">
				<span class="col-1">Q12</span>
				<div class="col-11">
					<span>若憑證申請/更新(展期)時發生扣款失敗，應如何處理？</span>
				</div>
			</div>
		</a>
		<div id="popup4-12" class="ttb-pup-collapse collapse" role="tabpanel">
			<div class="ttb-pup-body">
				<ul class="ttb-pup-list">
					<li>

<!-- 						<p> -->
<!-- 							若扣款失敗原因為“帳戶餘額不足”，請先確定銀行帳戶內有足夠的金額可供扣取費用；失敗原因若為“查無扣款帳號”，請洽本行設定扣帳帳號或繳交憑證費用，上述作業完成後請按下列步驟重新申請/更新憑證：<br> -->
<!-- 							1、點選「查詢/更新/下載憑證」。<br> <img src=""><br> 2、點選憑證後方之<img -->
<!-- 								src="">鈕。<br> 3、點選<img src="">鈕，再次申請/更新憑證。 -->
<!-- 						</p> -->
						<p>
							若扣款失敗原因為“帳戶餘額不足”，請先確定銀行帳戶內有足夠的金額可供扣取費用；失敗原因若為“查無扣款帳號”，請洽本行設定扣帳帳號或繳交憑證費用，上述作業完成後請按下列步驟重新申請/更新憑證：
						</p>
						<ol>
							<li>1、點選「查詢/更新/下載憑證」。</li>
							<li>2、點選憑證後方之小圖示按鈕。</li>
							<li>3、點選重送申請憑證鈕，再次申請/更新憑證。</li>
						</ol>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<!-- Q12 -->
	<div class="ttb-pup-block" role="tab">
		<a role="button" class="ttb-pup-title collapsed d-block"
			data-toggle="collapse" href="#popup4-13" aria-expanded="true"
			aria-controls="popup4-13">
			<div class="row">
				<span class="col-1">Q13</span>
				<div class="col-11">
					<span>若客戶更換電腦或更換電腦作業系統，可繼續使用原憑證嗎？</span>
				</div>
			</div>
		</a>
		<div id="popup4-13" class="ttb-pup-collapse collapse" role="tabpanel">
			<div class="ttb-pup-body">
				<ul class="ttb-pup-list">
					<li>

						<p>客戶需執行「網路環境設定」及「載具驅動程式安裝」，即可繼續使用原憑證，請參考Q5之電腦設定操作說明。</p>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<!-- Q14 -->
	<div class="ttb-pup-block" role="tab">
		<a role="button" class="ttb-pup-title collapsed d-block"
			data-toggle="collapse" href="#popup4-14" aria-expanded="true"
			aria-controls="popup4-14">
			<div class="row">
				<span class="col-1">Q14</span>
				<div class="col-11">
					<span>若憑證有被冒用、暴露及遺失等不安全之顧慮時，應如何辦理？</span>
				</div>
			</div>
		</a>
		<div id="popup4-14" class="ttb-pup-collapse collapse" role="tabpanel">
			<div class="ttb-pup-body">
				<ul class="ttb-pup-list">
					<li>

						<p>客戶應即登入憑證註冊中心或洽本行辦理憑證暫時停用或廢止。</p>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<!-- Q15 -->
	<div class="ttb-pup-block" role="tab">
		<a role="button" class="ttb-pup-title collapsed d-block"
			data-toggle="collapse" href="#popup4-15" aria-expanded="true"
			aria-controls="popup4-15">
			<div class="row">
				<span class="col-1">Q15</span>
				<div class="col-11">
					<span>載具密碼錯誤幾次會鎖住？</span>
				</div>
			</div>
		</a>
		<div id="popup4-15" class="ttb-pup-collapse collapse" role="tabpanel">
			<div class="ttb-pup-body">
				<ul class="ttb-pup-list">
					<li>

						<p>載具密碼錯誤超過5次會鎖住。</p>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<!-- Q16 -->
	<div class="ttb-pup-block" role="tab">
		<a role="button" class="ttb-pup-title collapsed d-block"
			data-toggle="collapse" href="#popup4-16" aria-expanded="true"
			aria-controls="popup4-16">
			<div class="row">
				<span class="col-1">Q16</span>
				<div class="col-11">
					<span>載具密碼鎖住時，應如何處裡？</span>
				</div>
			</div>
		</a>
		<div id="popup4-16" class="ttb-pup-collapse collapse" role="tabpanel">
			<div class="ttb-pup-body">
				<ul class="ttb-pup-list">
					<li>
						<p>請將載具送回營業單位並辦理重置作業相關手續。</p>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<!-- Q17 -->
	<div class="ttb-pup-block" role="tab">
		<a role="button" class="ttb-pup-title collapsed d-block"
			data-toggle="collapse" href="#popup4-17" aria-expanded="true"
			aria-controls="popup4-17">
			<div class="row">
				<span class="col-1">Q17</span>
				<div class="col-11">
					<span>如何補印「憑證費用收據」？</span>
				</div>
			</div>
		</a>
		<div id="popup4-17" class="ttb-pup-collapse collapse" role="tabpanel">
			<div class="ttb-pup-body">
				<ul class="ttb-pup-list">
					<li>

<!-- 						<p> -->
<!-- 							A17：查詢/更新/下載憑證-&gt;點「內容」，選「列印憑證費用收據」。<br> <img -->
<!-- 								src="./fstop/images/RAimage050.jpg"><br> <img -->
<!-- 								src="./fstop/images/RAimage051.jpg"><br> <img -->
<!-- 								src="./fstop/images/RAimage053.jpg"> -->
<!-- 						</p> -->
						<p>
							查詢/更新/下載憑證-&gt;點「內容」，選「列印憑證費用收據」。
						</p>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>