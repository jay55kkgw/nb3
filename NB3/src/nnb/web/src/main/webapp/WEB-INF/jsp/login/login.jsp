<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports_login.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_login.jsp"%>
<link rel="stylesheet" type="text/css" href="${__ctx}/keyboard/css/keyboard.css">
<script type="text/javascript" src="${__ctx}/keyboard/js/plugins_login.js"></script>
<script type="text/javascript" src="${__ctx}/js/loginAds.js"></script>
<style id="bg-for-banner"></style>
<style type="text/css">
.ttb-pup-btn-doublemax{
	width: 300px;
}
@media screen and (max-width: 992px) 
.ttb-pup-btn-doublemax{
    width: 100%;
}
</style>
<script type="text/javascript">

	// 初始資料
	var uriCtx = '${__ctx}';
	var bannerImgs = '${bannerImgs}'; // banner廣告數量
	var errorMsg = "${errorMsg}";
	var uri = "";
	var rdata, blockId;

	// 載入後初始化JS
	$(document).ready(function() {
		
		initBlockUI();	// HTML載入完成後0.1秒開始遮罩

		// 被踢退後進入登入頁的提示訊息
		var logged = "${logged}";
		if (logged == 'true') {
			$("#formId").hide();
			$("#logged-text").show();
			
		} else {
			setTimeout("init()", 100);		// 初始化
		}
		
		setTimeout("i18n()", 200);
		setTimeout("initError()", 300);
		setTimeout("initBanner()", 400);
		setTimeout("getBulletin()", 500);
		setTimeout("getTxt()", 600);
		setTimeout("unBlockUI(initBlockId)", 700);
				
	});
	
	function initBanner() {
		var adsudt = "${adsudt}";
		showBanner(bannerImgs, uriCtx, adsudt); // 顯示Banner廣告
	}

	function initError() {
		// 被踢退後進入登入頁的提示訊息
		if(errorMsg == 'logout'){
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert138' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
		}
	}
	
	// 清資料
	function cleanTmp() {
		fstop.getServerDataEx('${__ctx}'+'/logout_aj', null, true, init);
		$("#logged-text").hide();
		refreshCapCode();
		$("#formId").show();
		return false;
	}
	
	// 初始化
	function init() {
		browserDetect(); // E2EE可支援之瀏覽器
		dynamicKeyboard();	// 初始化動態鍵盤
		KeyBoardF();		// 讓動態鍵盤不指定浮標也可以輸入
		initKapImg();		// 初始化驗證碼
		newKapImg();		// 生成驗證碼
		autoExit();			// 關閉窗口時自動退出
		loginInit();		// 登入按鈕觸發-按下登入鍵後會取消觸發事件，所以每種登入流程最後得重新Init
		enterClick();		// CLICK ENTER KEY
		$("#lg").val("/nb3/login?locale="+'${__i18n_locale}');
	}
	
	function browserDetect() {
		if (checkBrowserE2EE()) {
			errorBlock(
					null, 
					['<spring:message code= "LB.X2477" />'],
					[
						'<spring:message code= "LB.X2478" />', 
						'IE11、Firefox31、Chrome37、Safari6、' + '<spring:message code= "LB.X2479" />'
					], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
			return false;
		}
		return true;
	}
	
	function IEdetect() {
		if (!IEdetection()) {
			errorBlock(
					null, 
					null,
					['<spring:message code= "LB.X2458" />'], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
			return false;
		}
		return true;
	}

	// 顯示後端回傳訊息
	function showData(data) {
		console.log("showData.now: " + new Date());
		
		console.log("data: " + data);
		if (data) {
			// login_aj回傳資料
			console.log("data.json: " + JSON.stringify(data));
			
			// LOCAL開發
			if("local" == data.msgCode) {
					$("#formId").attr("action", "${__ctx}" + "/INDEX/index");
					initBlockUI();
					$("#formId").submit();
					return; // 不加return雖然會submit，但還是會往下執行			
			}
			
			// 測試環境
			if("TEST" == data.msgCode) {	
					$("#formId").attr("action", "${__ctx}" + "/INDEX/index");
					initBlockUI();
					$("#formId").submit();
					return; // 不加return雖然會submit，但還是會往下執行		
			}

			console.log("data.msgCode: " + data.msgCode);
			console.log("data.result: " + data.result);
			
			// 正式環境
			if ("0" == data.msgCode) {
				// 登入成功
				var target = data.next;			// form action
				console.log("target: " + target);
				var acradio = data.previous;	// 需變更簽入密碼或是交易密碼
				console.log("acradio: " + acradio);
				
				// 送交表單
				processQuery(target, acradio);

			} // E239-密碼錯誤超過次數
			else if ('E239' == data.msgCode) {
				
				clearForm();		// 清空輸入欄位
				refreshCapCode();	// 變更驗證碼
				loginInit();		// 避免重複送出
				
				$('#e239-block').show();
				
			} // E240-使用者名稱錯誤超過次數
			else if ('E240' == data.msgCode) {
				
				clearForm();		// 清空輸入欄位
				refreshCapCode();	// 變更驗證碼
				loginInit();		// 避免重複送出

				$('#e240-block').show();
				
			} // 登入失敗 
			else {
				clearForm(); // 清空輸入欄位
				refreshCapCode(); // 變更驗證碼
				loginInit(); // 避免重複送出
				
				// 登入失敗
				errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Transaction_code' />: " + data.msgCode , "\n" , "<spring:message code='LB.Transaction_message' />: " + data.message], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
			}
		}
	}
	
	// 送交表單
	function processQuery(target, acradio) {
		console.log("processQuery.now: " + new Date());

		$('input[name="ACRADIO"]').val(acradio); // 判斷該導向變更簽入密碼或是交易密碼
		$("#formId").attr("action", "${__ctx}" + target);
		initBlockUI();
		$("#formId").submit();	
	}
	
	// 刷新驗證碼
	function refreshCapCode() {
		console.log("refreshCapCode...");

		// 驗證碼
		$('input[name="capCode"]').val('');
		
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image?' + Math.floor(Math.random() * 100)).fadeIn();
		
		// 登入失敗解遮罩
		unBlockUI(blockId);
	}
	
	// 初始化驗證碼
	function initKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image");
	}

	// 生成驗證碼
	function newKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').click(function() {
			refreshCapCode();
		});
	}

	// 關閉窗口時自動退出  
	function autoExit() {
		window.onbeforeunload = function(event) {
			if (event.clientX > 360 && event.clientY < 0 || event.altKey) {
				//alert(parent.document.location);
				errorBlock(
						null, 
						null,
						[parent.document.location], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			}
		};
	}

	// 登入按鈕觸發-按下登入鍵後會取消觸發事件，所以每種登入流程最後得重新Init
	function loginInit() {
		unBlockUI(blockId); // 取消登入解遮罩
		
		$('#login').off('click'); // 避免重複送出
		// for web
		$("#login").click(function() {
			console.log("loginInit.now: " + new Date());
			
			$('#login').off('click'); // 避免重複送出
			if(form_validate($("#cusidn"), $("#userName"), $("#webpw"))){
				login();
			}
		});
	}

	// 表單驗證
	function form_validate(cusidn, username, pw){
		console.log("form_validate.now: " + new Date());
		// 合法字元
		var gACCOUNT_CHAR = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890_#";
		
		// 身份證/營利事業統一編號
		if (!jsChkString2(cusidn, "<spring:message code= "LB.D0488" />/<spring:message code= "LB.D0489" />", gACCOUNT_CHAR, true)) {
			loginInit(); // 避免重複送出
			return false;
		}
		// 使用者名稱驗證
		if (!jsChkString2(username, "<spring:message code= "LB.Loginpage_User_name" />", gACCOUNT_CHAR, true)) {
			loginInit(); // 避免重複送出
			return false;
		}
		// 簽入密碼驗證
 		if (!chkPassword(pw, "<spring:message code= "LB.login_password" />", true)) {
 			loginInit(); // 避免重複送出
 			return false;
 		}
		
 		return true;
	}

	// CLICK ENTER KEY
	function enterClick() {
		$(document).keypress(function(event) {
			// 觸發回車鍵(手機也有)
			if (event.which == 13) {
				console.log("enterClick...");
				
				// 沒有遮罩下ENTER即送交
				if ( $('#error-block').is(":hidden") ){
					$("#login").click();
					
				} // 有遮罩ENTER即關閉遮罩
				else {
					$("#errorBtn1").click();
				}
			}
		});
	}
	
	var confirmType = 0;
	
	function errorBlock_login(errortitle, errorcontent, errorinfo, errorBtn1, errorBtn2) {
		$('p[id^=error-]').html("");
		wait1=true;
		setTimeout("wait1=false;", 500);
		$('#error-block').show();
		
		deadOrAlive("error-title", errortitle);
		
		errorBlockMsg("error-content", errorcontent);
		errorBlockMsg("error-info", errorinfo);
		
		
 		deadOrAlive("errorBtn1", errorBtn1);
 		deadOrAlive("errorBtn2", errorBtn2);
		
 		$("#errorBtn1").click(function(){
 			if(confirmType == 0){
 				$('#error-block').hide();
 				if(k_focus) {
 					k_focus.focus();
 				}
 				loginInit(); // 避免重複送出
 			}
 			else if(confirmType == 1){
 				initBlockUI();
 				logoutforce();
 			}
 			else if(confirmType == 2){
 				initBlockUI();
 				kickPortal_aj();
 			}
 			else if(confirmType == 3){
 				initBlockUI();
 				kickMB3_aj();
 			}
 			confirmType = 0;
 			
 		});
 		
 		$("#errorBtn2").click(function(){
 			confirmType = 0;
 			$('#error-block').hide();
 			refreshCapCode();
 			loginInit(); // 避免重複送出
 		});
		
	}
	
	// 登入
	function login() {
		if(!IEdetect()) {
			loginInit(); // 避免重複送出
			return false; // 不支援IE9(含)以下版本
		}
		
		console.log("login.now: " + new Date());
		
		initBlockUI();

		if('${isTest}'){
			console.log("isTest");
			
		} else {
			console.log("notTest");
			// 驗證碼為空
			if($("#capCode").val() == "") {
				errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.X1702' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
				
				// 驗證碼未輸入，需清空使用者名稱、簽入密碼、驗證碼欄位
				$('input[name="userName"]').val('');	// 使用者名稱
				$('input[name="password"]').val('');	// 簽入密碼
				$('#userName').focus();
				refreshCapCode();
				loginInit(); // 避免重複送出
				return false;
			}
		}
		
		if ('${isTest}'!='Y') {
			checkSys_aj();
			
		} else {
			console.log('pass check sys status !!!');
			login_aj(); // 可直接登入
		}
			
	}

	
	// 檢查是否需要後踢前
	function checkSys_aj(){
		var rdata;
		var checkSys_aj_uri = '${__ctx}' + "/checkSys_aj";
		console.log("checkSys_aj_uri.uri: " + checkSys_aj_uri);
		
		rdata = $("#formId").serializeArray();

		fstop.getServerDataEx(checkSys_aj_uri, rdata, true, checkSysCallBack);

	}
	
	function checkSysCallBack(data){
		console.log("checkResult: " + JSON.stringify(data) );
		
		if (data.result != true) {
			confirmType = data.data.resultType;
			console.log('checkSysResult.confirmType: ' + confirmType);
			errorBlock_login(
				null, 
				null,
				['<spring:message code= "LB.Confirm028" />'], 
				'<spring:message code= "LB.Confirm" />', 
				'<spring:message code= "LB.Cancel" />'
			);
			
		} else {
			login_aj(); // 可直接登入
		}
	}
	
	// 先做強迫登出再做登入
	function logoutforce(){
		var logoutforce_uri = '${__ctx}' + "/logoutforce_aj";
		var rdata;
		console.log("logoutforce_aj.uri: " + logoutforce_uri);
		rdata = $("#formId").serializeArray();	
		var logoutforce_result = fstop.getServerDataEx(logoutforce_uri, rdata, false);
		console.log("logoutforce_result: " + JSON.stringify(logoutforce_result) );
		
		// 強制登出成功後做登入
		if(logoutforce_result.result){
			login_aj();
			
		} else {
			errorBlock(
				null, 
				null,
				["error: " + logoutforce_result.message], 
				'<spring:message code= "LB.Quit" />', 
				null
			);
			loginInit(); // 避免重複送出
		}
	}
	
	// 先做登出Portal再做登入
	function kickPortal_aj(){
		var rdata;
		var kickPortal_aj_uri = '${__ctx}' + "/PORTAL/kickportal";
		console.log("kickPortal_aj.uri: " + kickPortal_aj_uri);
		
		rdata = {
			cusidn : $("#cusidn").val(),
			account : $("#userName").val()
		};

		var checkResult = fstop.getServerDataEx(kickPortal_aj_uri, rdata, false);
		console.log("checkResult: " + JSON.stringify(checkResult) );
		
		// 登出Portal成功後做登入
		if(checkResult.result){
			login_aj();
			
		} else {
			errorBlock(
				null, 
				null,
				["error: " + logoutforce_result.message], 
				'<spring:message code= "LB.Quit" />', 
				null
			);
			loginInit(); // 避免重複送出
		}
	}
	
	// 先做登出MB3再做登入
	function kickMB3_aj(){
		var rdata;
		var kickMb3_aj_uri = '${__ctx}' + "/MB3/kickmobile";
		console.log("kickMb3_aj.uri: " + kickMb3_aj_uri);
		
		rdata = {
			cusidn : $("#cusidn").val(),
		};

		var checkResult = fstop.getServerDataEx(kickMb3_aj_uri, rdata, false);
		console.log("checkResult: " + JSON.stringify(checkResult) );
		
		// 登出MB3成功後做登入
		if(checkResult.result){
			login_aj();
			
		} else {
			errorBlock(
				null, 
				null,
				["error: " + logoutforce_result.message], 
				'<spring:message code= "LB.Quit" />', 
				null
			);
			loginInit(); // 避免重複送出
		}
	}
	
	// 登入
	function login_aj(){
		console.log("login_aj.now: " + new Date());
		
		var rdata;
		var login_uri = '${__ctx}' + "/login_aj";
		console.log("login_aj.login_uri: " + login_uri);
		
		var userNameTmp = $("#userName").val();
		$('#webpw').val( pin_encrypt( $('#webpw').val() ) );
		console.log("pin_encrypt.now: " + new Date());
		$("#userName").val(userNameTmp);
		
		rdata = $("#formId").serializeArray();
		fstop.getServerDataEx(login_uri, rdata, true, showData);
	}

	// Escape
	$(document).keydown(function(event){
    	if(event.keyCode == 27) {
    		if ( !$('#error-block').is(":hidden") ){
    			confirmType = 0;
        		$('#error-block').hide();
        		refreshCapCode();
        		loginInit(); // 避免重複送出
    		}
       	}
	});

	// 切換語系
	function i18n() {
		var locale = "${__i18n_locale}";
		if (locale == "zh_TW") {
			$("#selectLg").append($("<option></option>").attr("value", "${__i18n_en}").text("En"));
			$("#selectLg").append($("<option></option>").attr("value", "${__i18n_zh_TW}").text("繁"));
			$("#selectLg").append($("<option></option>").attr("value", "${__i18n_zh_CN}").text("简"));
			$("#selectLg").val('${__i18n_zh_TW}');
		} else if (locale == "zh_CN") {
			$("#selectLg").append($("<option></option>").attr("value", "${__i18n_en}").text("En"));
			$("#selectLg").append($("<option></option>").attr("value", "${__i18n_zh_TW}").text("繁"));
			$("#selectLg").append($("<option></option>").attr("value", "${__i18n_zh_CN}").text("简"));
			$("#selectLg").val('${__i18n_zh_CN}');
		} else if (locale == "en") {
			$("#selectLg").append($("<option></option>").attr("value", "${__i18n_en}").text("En"));
			$("#selectLg").append($("<option></option>").attr("value", "${__i18n_zh_TW}").text("繁"));
			$("#selectLg").append($("<option></option>").attr("value", "${__i18n_zh_CN}").text("简"));
			$("#selectLg").val('${__i18n_en}');
		}
	}

	// 清空輸入表單
	function clearForm() {
		$('input[name="cusidn"]').val('');		// 身分證字號/統一編號
		$('input[name="userName"]').val('');	// 使用者名稱
		$('input[name="password"]').val('');	// 簽入密碼
		$("#cusidn").focus();
	}
	
	// 隱藏使用者名稱
	function hideUserName(){
		if($('#hid').prop('checked')){
			$('#userName').attr("type","password");
		}else{
			$('#userName').attr("type","text");
		}
	}
	

	/**
	 * 欄位檢查
	 * obj(form.field): 要檢查的物件
	 * field_hit: 要顯示的提示欄位名
	 * allow_char: 允許的字元
	 * bCheck: 是否檢查空字串(不輸入), true:警告 false:略過
	 */
	function jsChkString2(obj, field_hit, allow_char, bCheck) {
		if (bCheck) {
			if (!jsChkString(obj, field_hit)){
				loginInit(); // 避免重複送出
				return false;
			}
		}

		// 若欄位不存在則傳回true
		var val;
		field_hit = '[' + field_hit + ']';

		try {
			val = fnJSTrim(obj.val());
		} catch (ex) {
			return true;
		}

		var jump = 0;
		var str = '';
		while(val.length > 0) {
			var c = val.substring(0, 1);
			val = val.substring(1, val.length);
			if (allow_char.indexOf(c)<0) {
				str = field_hit + '<spring:message code="LB.X2071" />' + c;
				break;
			}
			if (jump++>1000) break;
		}
		return (doAlert(obj, str));
	}

	/**
	 * 去字串空白
	 * obj(form.field): 要檢查的物件
	 * field_hit: 要顯示的提示欄位名
	 */
	function jsChkString(obj, field_hit) {
		var val;
		var str = "";
		field_hit = '[' + field_hit + ']';

		// 若欄位不存在則傳回true
		try {
			val = fnJSTrim(obj.val());
		} catch (ex) {
			return true;
		}

		if (val == 'undefined') {
			str = field_hit + '<spring:message code="LB.X2070" />';
		} else if (val == '') {
			str = '<spring:message code="LB.Alert193" />' + field_hit + '';
		}

		return (doAlert(obj, str));
	}

	/**
	 * 去字串空白
	 */
	function fnJSTrim(sVar) {
		while (sVar.indexOf(" ")==0) {
		 	sVar = sVar.substring(1, sVar.length);
		}
		while (sVar.indexOf(" ")==sVar.length) {
			sVar = sVar.substring(0, sVar.length-1);
		}
		return sVar;
	}

	/**
	 * 簽入密碼檢核
	 */
	function chkPassword(pw, field_name, engNum) {
		var val;
		var field_name = '[' + field_name + ']';
		var result = true;
		
		var sPass = fnJSTrim(pw.val());
		if(sPass.length < 6 && sPass.length > 0) {
			pw.focus();
			loginInit(); // 避免重複送出
			result = false;
		}
		if(sPass.length >=9 || sPass.length <=5) {
			pw.focus();
			loginInit(); // 避免重複送出
			result = false;
		}	
		if(sPass=='' ) {
			pw.focus();
			loginInit(); // 避免重複送出
			result = false;
		}	
		
		var iCode = 0;
		for(var i=0;i<sPass.length;i++) {
			iCode = sPass.charCodeAt(i);
			if(engNum) {
				if(!((iCode >=48 && iCode <= 57) || (iCode >=65 && iCode <= 90) || (iCode >=97 && iCode <= 122))) {
					pw.focus();
					loginInit(); // 避免重複送出
					result = false;
				}
			}else {
				if(!(iCode >=48 && iCode <= 57)) {
					pw.focus();
					loginInit(); // 避免重複送出
					result = false;
				}	
			}
		}
		
		// 跳出提示字，並將游標停在該欄位上
		if (!result) {
			return doAlert(pw, '<spring:message code="LB.X2069" />');
		}
		
		return result;
	}

	/**
	 * 跳出提示字，並將游標停在該欄位上
	 */
	function doAlert(obj, str) {
		if (str != '') {
			//alert(str);
			errorBlock(
				null, 
				null,
				[str], 
				'<spring:message code= "LB.Quit" />', 
				null
			);
			obj.focus();
			loginInit(); // 避免重複送出
			return false;
		}
		return true;
	}

	function chkp(data){
		if(data.length >= 8){
			errorBlock(
					null, 
					null,
					['<spring:message code= "LB.X2362" />'], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
			loginInit(); // 避免重複送出
			return false;
		}
	}
</script>
</head>

<body>
	<input id="hostname" name="hostname" type="hidden" value="${hostname}" />

	<div id="dialog_box" class="Dialog_box" style ="display: none;
		z-index: 9999;
	    padding-top: 22%;
	    height: 100%;
	    top: 0;
	    position: absolute;
	    width: 100%;
	    text-align: center;
	    background-color: rgba(0,0,0,0.6);">
		<div class="sk-fading-circle">
			<div class="sk-circle1 sk-circle"></div>
			<div class="sk-circle2 sk-circle"></div>
			<div class="sk-circle3 sk-circle"></div>
			<div class="sk-circle4 sk-circle"></div>
			<div class="sk-circle5 sk-circle"></div>
			<div class="sk-circle6 sk-circle"></div>
			<div class="sk-circle7 sk-circle"></div>
			<div class="sk-circle8 sk-circle"></div>
			<div class="sk-circle9 sk-circle"></div>
			<div class="sk-circle10 sk-circle"></div>
			<div class="sk-circle11 sk-circle"></div>
			<div class="sk-circle12 sk-circle"></div>
		</div>
	</div>
	<div id="loadingBox" class="Loading_box">
		<div class="sk-fading-circle">
			<div class="sk-circle1 sk-circle"></div>
			<div class="sk-circle2 sk-circle"></div>
			<div class="sk-circle3 sk-circle"></div>
			<div class="sk-circle4 sk-circle"></div>
			<div class="sk-circle5 sk-circle"></div>
			<div class="sk-circle6 sk-circle"></div>
			<div class="sk-circle7 sk-circle"></div>
			<div class="sk-circle8 sk-circle"></div>
			<div class="sk-circle9 sk-circle"></div>
			<div class="sk-circle10 sk-circle"></div>
			<div class="sk-circle11 sk-circle"></div>
			<div class="sk-circle12 sk-circle"></div>
		</div>
	</div>
	<section id="error-block" class="error-block" style="display:none">
		<div class="error-for-message">
			<p id="error-title" class="error-title" style="display:none"></p>
			<p id="error-content0" class="error-content"></p>
			<p id="error-info0" class="error-info"></p>
			<button id="errorBtn1" class="btn-flat-orange ttb-pup-btn" style="display:none"></button>
			<button id="errorBtn2" class="btn-flat-orange ttb-pup-btn" style="display:none"></button>
		</div>
	</section>

	<!-- 訊息公告 -->
	<section id="text-block" class="error-block" style="display:none">
		<div class="error-for-message">
			<p class="error-title"><spring:message code= "LB.Message" /></p>
			<p id="text-info" class="error-content">
			</p>
			<button class="btn-flat-orange ttb-pup-btn" onclick="$('#text-block').hide();"><spring:message code='LB.Quit' /></button>
		</div>
	</section>
	
	<!-- E239 -->
	<section id="e239-block" class="error-block" style="display:none">
		<div class="error-for-message">
			<p id="e239-title" class="error-title" style="display:none"></p>
			<p id="e239-info0" class="error-info">
				<spring:message code= 'LB.Transaction_code' />: E239
				<br/>
				<spring:message code='LB.Transaction_message' />: <spring:message code='LB.X2234' />
			</p>
			<p id="e239-content0" class="error-content">
				<spring:message code='LB.Fund_Regular_Comfirm_P2_D3-1' />：<br/>
				1. <a href="${__ctx}/RESET/user_reset" target="_blank" style="color:red;"><spring:message code='LB.X2237' /></a>。<br/>
				2. <spring:message code='LB.X2238' /><br/>
				3. <spring:message code='LB.X2239' /><br/>
			</p>
			<button id="e239Btn1" class="btn-flat-orange ttb-pup-btn" onclick="$('#e239-block').hide();"><spring:message code='LB.Quit' /></button>
		</div>
	</section>
	
	<!-- E240 -->
	<section id="e240-block" class="error-block" style="display:none">
		<div class="error-for-message">
			<p id="e240-title" class="error-title" style="display:none"></p>
			<p id="e240-info0" class="error-info">
				<spring:message code= 'LB.Transaction_code' />: E240
				<br/>
				<spring:message code='LB.Transaction_message' />: <spring:message code='LB.X2235' />
			</p>
			<p id="e240-content0" class="error-content">
				<spring:message code='LB.Fund_Regular_Comfirm_P2_D3-1' />：<br/>
				1. <a href="${__ctx}/RESET/user_reset" target="_blank" style="color:red;"><spring:message code='LB.X2237' /></a>。<br/>
				2. <spring:message code='LB.X2238' /><br/>
				3. <spring:message code='LB.X2239' /><br/>
			</p>
			<button id="e240Btn1" class="btn-flat-orange ttb-pup-btn" onclick="$('#e240-block').hide();"><spring:message code='LB.Quit' /></button>
		</div>
	</section>
	
	<!-- 忘記密碼 -->
	<%@ include file="../reset/forgotPWD.jsp"%>
	
	<c:choose>
		<c:when test="${__i18n_locale eq 'zh_TW'}">
			<%@ include file="../customer_service/financial_information.jsp"%>
		</c:when>
		<c:when test="${__i18n_locale eq 'en'}">
			<%@ include file="../customer_service/financial_information_EN.jsp"%>
		</c:when>
		<c:otherwise>
			<%@ include file="../customer_service/financial_information_CN.jsp"%>
		</c:otherwise>
	</c:choose>
	
	<!-- 最新資訊 more -->
	<section id="news-more" class="modal fade more-info-block" role="dialog" aria-labelledby="" aria-hidden="true" tabindex="-1">
		<div class="modal-dialog modal-dialog-centered calendar-modal" role="document">
			<div class="modal-content">
				<div class="modal-header mb-0">
					<p class="ttb-pup-header"><spring:message code="LB.News" /></p>
				</div>
				<div class="modal-body">
					<ul id="GENmore">
					</ul>
				</div>
				<div class="modal-footer ttb-pup-footer">
					<input type="submit" class="ttb-pup-btn btn-flat-gray" data-dismiss="modal" value="<spring:message code='LB.X1572' />" />
				</div>
			</div>
		</div>
	</section>
	
	<!-- 好康消息more -->
	<section id="good-news" class="modal fade more-info-block" role="dialog" aria-labelledby="" aria-hidden="true" tabindex="-1">
		<div class="modal-dialog modal-dialog-centered calendar-modal" role="document">
			<div class="modal-content">
				<div class="modal-header mb-0">
					<p class="ttb-pup-header"><spring:message code="LB.X2157" /></p>
				</div>
				<div class="modal-body">
					<c:if test="${empty newsTitle}">
						<ul>
							<li>
								<a href="#">
									<span>2018/05/23</span>
									<span>
										<spring:message code="LB.Advertisement_1" />
									</span>
								</a>
							</li>
						</ul>
					</c:if>
					<c:if test="${!empty newsTitle}">
						<ul>
							<c:forEach items="${newsTitle}" var="item" varStatus="countStatus">
								<!-- 連結廣告 -->
								<c:if test="${item.TARGETTYPE == '1'}">
									<li title="${item.TITLE}">
										<a href="${item.URL}" target="_blank" <c:if test="${item.URL==' '}">onclick="return false"</c:if>>
											<fmt:parseDate var="parseDate" value="${item.STARTDATE}" pattern="yyyyMMdd"/>
											<span class="news-day"><fmt:formatDate value="${parseDate}" pattern="yyyy/MM/dd"/></span>
											<span class="news-content">
												${item.CONTENT}
											</span>
										</a>
									</li>
								</c:if>
								<!-- PDF廣告 -->
								<c:if test="${item.TARGETTYPE == '2'}">
									<li title="${item.TITLE}">
										<a href="${__ctx}/getAds/${item.newsId}" target="_blank">
											<fmt:parseDate var="parseDate" value="${item.STARTDATE}" pattern="yyyyMMdd"/>
											<span class="news-day"><fmt:formatDate value="${parseDate}" pattern="yyyy/MM/dd"/></span>
											<span class="news-content">
												${item.CONTENT}
											</span>
										</a>
									</li>
								</c:if>
								<!-- 文字廣告 -->
								<c:if test="${item.TARGETTYPE == '3'}">
									<li onclick="$('#text-info').html('${item.TARGETCONTENT}');$('#text-block').show();"">
										<a href="#">
											<fmt:parseDate var="parseDate" value="${item.STARTDATE}" pattern="yyyyMMdd"/>
											<span class="news-day"><fmt:formatDate value="${parseDate}" pattern="yyyy/MM/dd"/></span>
											<span class="news-content">
												${item.CONTENT}
											</span>
										</a>
									</li>
								</c:if>
							</c:forEach>
						</ul>
					</c:if>
				</div>
				<div class="modal-footer ttb-pup-footer">
					<input type="submit" class="ttb-pup-btn btn-flat-gray" data-dismiss="modal" value="<spring:message code='LB.X1572' />" />
				</div>
			</div>
		</div>
	</section>
	
	<!-- LOADING遮罩區塊 -->
	<div id="loadingBox" class="Loading_box">
		<div class="sk-fading-circle">
			<div class="sk-circle1 sk-circle"></div>
			<div class="sk-circle2 sk-circle"></div>
			<div class="sk-circle3 sk-circle"></div>
			<div class="sk-circle4 sk-circle"></div>
			<div class="sk-circle5 sk-circle"></div>
			<div class="sk-circle6 sk-circle"></div>
			<div class="sk-circle7 sk-circle"></div>
			<div class="sk-circle8 sk-circle"></div>
			<div class="sk-circle9 sk-circle"></div>
			<div class="sk-circle10 sk-circle"></div>
			<div class="sk-circle11 sk-circle"></div>
			<div class="sk-circle12 sk-circle"></div>
		</div>
	</div>

	<div class="container login-container">
		<div class="row">
			<div class="col-lg-8 col-md-12 col-sm-12 order-lg-1 order-2 p-0">
				<div class="login-top d-none d-lg-block">
					<div class="header-area">
						<div class="header-ele">
							<div class="ele-info">
								<!--最新資訊-->
								<div class="header-info">
									<spring:message code="LB.News" />
								</div>
								<div class="header-depiction tcontainer">
									<div name="genl" class="ticker-wrap">
										<ul id="GEN">
										</ul>
									</div>
								</div>
								<a href="#" data-toggle="modal" data-target="#news-more"><img class="more-btn" src="${__ctx}/img/icon-more-${__i18n_locale}.svg?a=${jscssDate}"></a>
							</div>
							<div class="ele-info">
								<!--重要公告-->
								<div class="header-info">
									<spring:message code="LB.Announcements" />
								</div>
								<div class="header-depiction tcontainer">
									<div name="impl" class="ticker-wrap">
										<ul id="IMP">
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
	
				<div class="login-banner">
					<div id="photo-carousel" class="banner-slogan carousel slide"
						data-ride="carousel">
						<!-- 輪播圖片-->
						<div class="carousel-inner">
							
							<c:if test="${bannerImgs==0}">
								<!-- 預設圖片顯示 -->
								<div class="carousel-item active">
									<img class="d-block">
								</div>
							</c:if>
							
							<c:if test="${!(bannerImgs==0)}">	
								<!-- 第一張開始算其他相片 -->
								<div class="carousel-item active" id="adsT1" onclick="getB(1)">
										<img class="d-block">
									</div>
								<c:forEach var="i" begin="2" end="${bannerImgs}">
									<div class="carousel-item" id="adsT${i}" onclick="getB(${i})">
										<img class="d-block">
									</div>
								</c:forEach>
							</c:if>
							<a id="tt" href="" target="_blank" style="display:none"></a>
						</div>
						<!-- 相片選擇鈕 -->
<!-- 						<ol class="carousel-indicators"> -->
<!-- 							<li data-target="#photo-carousel" data-slide-to="0" class="active"></li> -->
<!-- 							<li data-target="#photo-carousel" data-slide-to="1"></li> -->
<!-- 							<li data-target="#photo-carousel" data-slide-to="2"></li> -->
<!-- 							<li data-target="#photo-carousel" data-slide-to="3"></li> -->
<!-- 							<li data-target="#photo-carousel" data-slide-to="4"></li> -->
<!-- 						</ol> -->
						<!-- 相片選擇鈕 -->
						<ol class="carousel-indicators">
							<c:if test="${bannerImgs == 0}">
								<li data-target="#photo-carousel" data-slide-to="0" class="active"></li>
							</c:if>
							<c:if test="${bannerImgs != 0}">
								<c:forEach var="i" begin="0" end="${bannerImgs-1}">
									<c:if test="${i == 0}">
										<li data-target="#photo-carousel" data-slide-to="${i}" class="active"></li>
									</c:if>
									<c:if test="${i != 0}">
										<li data-target="#photo-carousel" data-slide-to="${i}"></li>
									</c:if>
								</c:forEach>
							</c:if>
						</ol>
						<!-- 相片瀏覽鈕 -->
						<a class="carousel-control-prev" href="#photo-carousel"
							role="button" data-slide="prev"> <span
							class="carousel-control-prev-icon" aria-hidden="true"></span> <span
							class="sr-only">Previous</span>
						</a> <a class="carousel-control-next" href="#photo-carousel"
							role="button" data-slide="next"> <span
							class="carousel-control-next-icon" aria-hidden="true"></span> <span
							class="sr-only">Next</span>
						</a>
					</div>
				</div>
				<div class="login-icon">
					<div class="element-area">
						<div class="icon-arrow">
							<img src="${__ctx}/img/arrow-left.png?a=${jscssDate}">
						</div>
						<!-- 新手上路 -->
						<div class="element-item">
							<a href="${__ctx}/CUSTOMER/SERVICE/novice" target="_self">
								<img src="${__ctx}/img/icon-01.svg?a=${jscssDate}">
								<span><spring:message code="LB.New_User" /></span>
							</a>
						</div>
						<!-- 常見問題 -->
						<div class="element-item">
							<a href="${__ctx}/CUSTOMER/SERVICE/common_problem" target="_self">
								<img src="${__ctx}/img/icon-02.svg?a=${jscssDate}">
								<span><spring:message code="LB.FAQs" /></span>
							</a>
						</div>
						<!-- 網銀申辦 / 一般網銀 線上申請(原) -->
						<div class="element-item">
							<a href="#" onclick="fstop.getPage('${pageContext.request.contextPath}'+'/ONLINE/APPLY/online_apply_menu','', '')">
								<img src="${__ctx}/img/icon-03.svg?a=${jscssDate}">
								<span><spring:message code="LB.Register" /></span>
							</a>
						</div>
						<!-- 元件下載 -->
						<div class="element-item">
							<a href="${__ctx}/CUSTOMER/SERVICE/environment_setting" target="_self">
								<img src="${__ctx}/img/icon-06.svg?a=${jscssDate}">
								<span><spring:message code="LB.X2246" /></span>
							</a>
						</div>
						<!-- 金融資訊 -->
						<div class="element-item">
							<a href="#" onclick="$('#main-content').show();"> 
								<img src="${__ctx}/img/icon-05.svg?a=${jscssDate}">
								<span><spring:message code="LB.Information_inquiry" /></span>
							</a>
						</div>
						<div class="icon-arrow">
							<img src="${__ctx}/img/arrow-right.png?a=${jscssDate}">
						</div>
					</div>
					<div class="login-menu  d-block d-lg-none">
						<ul>
							<li>
								<!-- 臺灣企銀首頁 --> 
								<c:choose>
									<c:when test="${__i18n_locale eq 'en'}">
										<a href="https://www.tbb.com.tw/web/guest/english2" target="_blank" role="button">
											<spring:message code="LB.TAIWAN_BUSINESS_BANK" />
										</a>
									</c:when>
									<c:when test="${__i18n_locale eq 'zh_TW'}">
										<a href="https://www.tbb.com.tw/" target="_blank" role="button">
											<spring:message code="LB.TAIWAN_BUSINESS_BANK" />
										</a>
									</c:when>
									<c:otherwise>
										<a href="https://www.tbb.com.tw/" target="_blank" role="button">
											<spring:message code="LB.TAIWAN_BUSINESS_BANK" />
										</a>
									</c:otherwise>
								</c:choose>
							</li>
							<li>
								<!-- 網路安全 --> 
								<a href="${__ctx}/CUSTOMER/SERVICE/network_security" target="_self" role="button">
									<spring:message code="LB.Network_security" />
								</a>
							</li>
							<li>
								<!-- 網站導覽 -->
								<a href="${__ctx}/CUSTOMER/SERVICE/site_guide" target="_self" role="button">
									<spring:message code="LB.Service_overview" />
								</a>
							</li>
							<li>
								<!-- 意見信箱 --> <c:choose>
									<c:when test="${__i18n_locale eq 'en' }">
										<a href="https://www.tbb.com.tw/web/guest/contact-us" target="_blank" role="button">
											<spring:message code="LB.Customer_service" />
										</a>
									</c:when>
									<c:when test="${__i18n_locale eq 'zh_TW'}">
										<a href="https://www.tbb.com.tw/-47" target="_blank" role="button">
											<spring:message code="LB.Customer_service" />
										</a>
									</c:when>
									<c:otherwise>
										<a href="https://www.tbb.com.tw/-47" target="_blank" role="button">
											<spring:message code="LB.Customer_service" />
										</a>
									</c:otherwise>
								</c:choose>
							</li>
						</ul>
					</div>
				</div>
				
				
				<div class="login-news">
					<!-- 好康消息 -->
					<p><spring:message code="LB.X2157" /></p>
					
					<c:if test="${empty newsTitle}">
						<!-- 預設 -->
						<div class="news-area">
							<a href="#">
								<div class="news-img">
									<img style="background-image:url(${__ctx}/login/default/news-lg-0.png)">
								</div>
								<div class="news-info">
									<div class="news-day">2018/05/23</div>
									<div class="news-content">
										<spring:message code="LB.Advertisement_1" />
									</div>
								</div>
							</a>
						</div>
					</c:if>
					
					<c:if test="${!empty newsTitle}">
						<!-- 其他 -->
						<c:forEach items="${newsTitle}" var="item" varStatus="countStatus" begin="0" end="1">
							<div class="news-area" title="${item.TITLE}">
								<!-- 連結廣告 -->
								<c:if test="${item.TARGETTYPE == '1'}">
									<a href="${item.URL}" target="_blank" <c:if test="${item.URL==' '}">onclick="return false"</c:if>>
										<div class="news-img">
											<img style="background-image:url(${__ctx}/login/news/${imgSrc}/news-lg-${countStatus.count}.png?udt=${adsnewsudt})">
										</div>
										<div class="news-info">
											<fmt:parseDate var="parseDate" value="${item.STARTDATE}" pattern="yyyyMMdd"/>
											<span class="news-day"><fmt:formatDate value="${parseDate}" pattern="yyyy/MM/dd"/></span>
											<span class="news-content">
												${item.CONTENT}
											</span>
										</div>
									</a>
								</c:if>
								<!-- PDF廣告 -->
								<c:if test="${item.TARGETTYPE == '2'}">
									<a href="${__ctx}/getAds/${item.newsId}" target="_blank">
										<div class="news-img">
											<img style="background-image:url(${__ctx}/login/news/${imgSrc}/news-lg-${countStatus.count}.png?udt=${adsnewsudt})">
										</div>
										<div class="news-info">
											<fmt:parseDate var="parseDate" value="${item.STARTDATE}" pattern="yyyyMMdd"/>
											<span class="news-day"><fmt:formatDate value="${parseDate}" pattern="yyyy/MM/dd"/></span>
											<span class="news-content">
												${item.CONTENT}
											</span>
										</div>
									</a>
								</c:if>
								<!-- 文字廣告 -->
								<c:if test="${item.TARGETTYPE == '3'}">
									<a href="javascript:void(0)" onclick="$('#text-info').html('${item.TARGETCONTENT}');$('#text-block').show();">
										<div class="news-img">
											<img style="background-image:url(${__ctx}/login/news/${imgSrc}/news-lg-${countStatus.count}.png?udt=${adsnewsudt})">
										</div>
										<div class="news-info">
											<fmt:parseDate var="parseDate" value="${item.STARTDATE}" pattern="yyyyMMdd"/>
											<span class="news-day"><fmt:formatDate value="${parseDate}" pattern="yyyy/MM/dd"/></span>
											<span class="news-content">
												${item.CONTENT}
											</span>
										</div>
									</a>
								</c:if>
							</div>
						</c:forEach>
					</c:if>
					
					<a href="#" class="login-news-more" data-toggle="modal" data-target="#good-news"><img class="more-btn" src="${__ctx}/img/icon-more-${__i18n_locale}.svg?a=${jscssDate}"></a>
				</div>
				<div class="login-bottom">
					<p class="tbb-login-name"><spring:message code="LB.X2245"/></p><!-- 臺灣中小企業銀行 -->
					<ul>
						<!-- 隱私權聲明 -->
						<li><a href="https://www.tbb.com.tw/web/guest/-173" target="_blank"><spring:message code="LB.X2152"/></a></li>
						<!-- 安全政策 -->
						<li><a href="https://www.tbb.com.tw/web/guest/-551" target="_blank"><spring:message code="LB.X2153"/></a></li>
						<!-- 系統/瀏覽器需求 -->
						<li><a href="${__ctx}/CUSTOMER/SERVICE/common_problem?tag=0" target="_blank"><spring:message code="LB.X2244"/></a></li>

						<li>
							<a href="https://www.cdic.gov.tw/main_ch/chinese.html" title="外網連結 - 另開視窗連結至存款保險" target="_blank">
								<img src="${__ctx}/img/saving-insurance.svg?a=${jscssDate}" width="42" height="42" alt="存款保險標章" style="margin:0px 0px 0px;height:auto;">
							</a>
						</li>
					</ul>
					<div class="enterprise-info d-inline-block d-lg-none">
						<p>
							<!-- 客服 -->
							<spring:message code="LB.X2154" />
							<a href="tel:0800-017-171">
								0800-01-7171
							</a>
							<!-- 限市話 -->
							<spring:message code="LB.X2211" />
							<c:if test="${__i18n_locale eq 'en'}">,</c:if>
							<a href="tel:02-2357-7171">
								02-2357-7171
							</a>
						</p>
					</div>
					<div style="text-align: center;">
						<a href="https://www.facebook.com/tbbdreamplus/" target="_blank">
							<span class="facebook-icon d-lg-none">&nbsp;</span>
						</a>
					</div>
					<!-- 版權所有 2018 臺灣中小企業銀行 Taiwan Business Bank -->
<%-- 					<spring:message code="LB.Copyright_2018_Taiwan_Business_Bank" /> --%>
				</div>
			</div>
			<div class="col-lg-4 col-md-12 col-sm-12 order-lg-2 order-1 lg-login">
				<div class="login-title-block">
					<!-- 多語系選擇 -->
					<div class="multi-lang-select">
						<select id="lg" class="login-custom-select select-input half-input" onchange="location = this.value">
							<option value="${__i18n_zh_TW}"><a href="${__i18n_zh_TW}">繁</a></option>
							<option value="${__i18n_zh_CN}"><a href="${__i18n_zh_CN}">简</a></option>
							<option value="${__i18n_en}"><a href="${__i18n_en}">En</a></option>
						</select>
					</div>
					<img class="login-logo" src="${__ctx}/img/tbb-logo-white.svg?a=${jscssDate}">
				</div>
				<div class="login-top d-lg-none">
					<div class="header-area">
						<div class="header-ele">
							<div class="ele-info">
								<!--最新資訊-->
								<div class="header-info">
									<spring:message code="LB.News" />
								</div>
								<div class="header-depiction tcontainer">
									<div name="genl" class="ticker-wrap">
										<ul id="GEN1">
										</ul>
									</div>
								</div>
								<a href="#" data-toggle="modal" data-target="#news-more"><img class="more-btn" src="${__ctx}/img/icon-more-${__i18n_locale}.svg?a=${jscssDate}"></a>
							</div>
							<div class="ele-info">
								<!--重要公告-->
								<div class="header-info">
									<spring:message code="LB.Announcements" />
								</div>
								<div class="header-depiction tcontainer">
									<div name="impl" class="ticker-wrap">
										<ul id="IMP1">
										</ul>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
				<div class="web-switch">
					<a href="#" class="active">
						<spring:message code="LB.Internet_banking" />
					</a>
					<a href="https://eatm.tbb.com.tw/" target="_blank" role="button">
						<spring:message code="LB.Web_ATM" />
					</a>
				</div>
				
				<div id="logged-text" class="login-submit-block keyboard" style="display:none; font-size: 30px; margin: 15px; text-align: left; padding: 10px;">
					您可能已在其他頁籤或相同瀏覽器上登入，請使用已登入的網路銀行視窗，或前次未正常登出，請按此
					<a href="#" data-toggle="modal" data-target="" style="font-size: 30px; color: #f3720c;" onclick="cleanTmp();">
						登出
					</a>
					後，再請重新登入
				</div>
								
				<form id="formId" method="post" action="${__ctx}/INDEX/index">
					<input name="ACRADIO" type="hidden" value="" />
					<!-- 需要變更密碼 -->
					<div class="login-input-block  login-checkbox">
						<!-- 身分證字號/統一編號 -->
						<spring:message code="LB.Id_no" var="labelCusidn" />
						<input type="text" id="cusidn" name="cusidn" onkeyup="this.value=this.value.replace(/\s+/g,'')"
							placeholder="${labelCusidn}" maxlength="10" autocomplete="off">
					</div>
					<div class="login-input-block  login-checkbox">
						<!-- 使用者名稱 -->
						<spring:message code="LB.Loginpage_User_name" var="labelUserName" />
						<input type="text" id="userName" name="userName" onkeyup="this.value=this.value.replace(/\s+/g,'')"
							placeholder="${labelUserName}" maxlength="16" autocomplete="off">
						<label class="check-block">
							<input type="checkbox" id="hid" name="hid" onclick="hideUserName()" tabindex="-1" />
							<span class="ttb-check"></span>
						</label>
					</div>
					<div class="login-input-block login-checkbox">
						<!-- 簽入密碼 -->
						<spring:message code="LB.Loginpage_password" var="labelPW" />
						<input type="password" id="webpw" name="password" onkeyup="this.value=this.value.replace(/\s+/g,'')"
							placeholder="${labelPW}" maxlength="8" autocomplete="off" onkeypress="chkp(this.value)">
<%-- 						<button type="button" class="question-mark" onclick="fstop.getPage('${pageContext.request.contextPath}'+'/RESET/forgotPWD','', '')" tabindex="-1"></button> --%>
					</div>
					<!-- 驗證碼那一塊的程式 -->
					<div class="login-input-block">
						<spring:message code="LB.D0032" var="labelCapCode" />
						<input id="capCode" type="text" class="veri-code-input" onkeyup="this.value=this.value.replace(/\s+/g,'')"
							name="capCode" placeholder="${labelCapCode}" maxlength="4" autocomplete="off">
						
						<button class="code-update" type="button" name="reshow" onclick="refreshCapCode()">
							<img name="kaptchaImage"/>
						</button>
					</div>
					
					<!-- 動態鍵盤 -->
					<input id="openKeyboardBtn" class="login-btn openKeyboardBtn d-none d-lg-block m-auto" type="button" value="" data-toggle="tooltip" data-placement="right" title="<spring:message code="LB.X1703" />" />
					<div id="keyboardBlock" class="keyboardBlock login-keyboard" style="display:none;">
						<p><spring:message code="LB.X1704" /><img id="closeKeyboardBtn" class="closeKeyboardBtn" src="${__ctx}/keyboard/images/close.svg"></p>
				
						<div class="keyboard">
							<ul class="keyboardButtons letterKeyboard">
								<li data-role="letter" data-key="a" data-key-caps="A"></li>
								<li data-role="letter" data-key="b" data-key-caps="B"></li>
								<li data-role="letter" data-key="c" data-key-caps="C"></li>
								<li data-role="letter" data-key="d" data-key-caps="D"></li>
								<li data-role="letter" data-key="e" data-key-caps="E"></li>
								<li data-role="letter" data-key="f" data-key-caps="F"></li>
								<li data-role="letter" data-key="g" data-key-caps="G"></li>
								<li class="btnDelete" data-role="delete" data-key="" data-key-caps=""><img src="${__ctx}/keyboard/images/delete.svg" /></li>
								<li data-role="letter" data-key="h" data-key-caps="H"></li>
								<li data-role="letter" data-key="i" data-key-caps="I"></li>
								<li data-role="letter" data-key="j" data-key-caps="J"></li>
								<li data-role="letter" data-key="k" data-key-caps="K"></li>
								<li data-role="letter" data-key="l" data-key-caps="L"></li>
								<li data-role="letter" data-key="m" data-key-caps="M"></li>
								<li data-role="letter" data-key="n" data-key-caps="N"></li>
								<li class="btnCapslock" data-role="capslock" data-key="" data-key-caps=""><img src="${__ctx}/keyboard/images/cap.svg" /></li>
								<li data-role="letter" data-key="o" data-key-caps="O"></li>
								<li data-role="letter" data-key="p" data-key-caps="P"></li>
								<li data-role="letter" data-key="q" data-key-caps="Q"></li>
								<li data-role="letter" data-key="r" data-key-caps="R"></li>
								<li data-role="letter" data-key="s" data-key-caps="S"></li>
								<li data-role="letter" data-key="t" data-key-caps="T"></li>
								<li data-role="letter" data-key="u" data-key-caps="U"></li>
								<li class="btnReset" data-role="reset" data-key="" data-key-caps=""><img src="${__ctx}/keyboard/images/reset.svg" style="width: 20px; color: #f3720c;"/></li>
								<li data-role="letter" data-key="v" data-key-caps="V"></li>
								<li data-role="letter" data-key="w" data-key-caps="W"></li>
								<li data-role="letter" data-key="x" data-key-caps="X"></li>
								<li data-role="letter" data-key="y" data-key-caps="Y"></li>
								<li data-role="letter" data-key="z" data-key-caps="Z"></li>
								<li class="btnConfirm" data-role="confirm" data-key="" data-key-caps=""><spring:message code= "LB.Login" /></li>
							</ul>
						</div>
						<div class="keyboard numberKeyboard">
							<ul class="keyboardButtons">
								<li data-role="symbol" data-key="1" data-key-caps="1"></li>
								<li data-role="symbol" data-key="2" data-key-caps="2"></li>
								<li data-role="symbol" data-key="3" data-key-caps="3"></li>
								<li data-role="symbol" data-key="4" data-key-caps="4"></li>
								<li data-role="symbol" data-key="5" data-key-caps="5"></li>
								<li data-role="symbol" data-key="6" data-key-caps="6"></li>
								<li data-role="symbol" data-key="7" data-key-caps="7"></li>
								<li data-role="symbol" data-key="8" data-key-caps="8"></li>
								<li data-role="symbol" data-key="9" data-key-caps="9"></li>
								<li data-role="symbol" data-key="0" data-key-caps="0"></li>
							</ul>
						</div>
					</div>
					
					<!-- 登入 -->
					<div class="login-submit-block">
<%-- 						<a href="#" onclick="fstop.getPage('${pageContext.request.contextPath}'+'/RESET/forgotPWD','', '')"><spring:message code="LB.X2156" />/<spring:message code="LB.D1019" />?</a><!-- 忘記帳號/密碼? --> --%>
						<a href="#" data-toggle="modal" data-target="#password-miss"><spring:message code="LB.X2156" />/<spring:message code="LB.D1019" />?</a><!-- 忘記帳號/密碼? -->
						<spring:message code="LB.Login" var="labelSubmit"></spring:message>
						<input id="login" class="login-btn" type="button" value="${labelSubmit}" />
					</div>
				</form>
				<div class="login-menu d-none d-lg-block">
					<ul>
						<li>
							<!-- 臺灣企銀首頁 --> 
							<c:choose>
								<c:when test="${__i18n_locale eq 'en'}">
									<a href="https://www.tbb.com.tw/web/guest/english2" target="_blank" role="button">
										<spring:message code="LB.TAIWAN_BUSINESS_BANK" />
									</a>
								</c:when>
								<c:when test="${__i18n_locale eq 'zh_TW'}">
									<a href="https://www.tbb.com.tw/" target="_blank" role="button">
										<spring:message code="LB.TAIWAN_BUSINESS_BANK" />
									</a>
								</c:when>
								<c:otherwise>
									<a href="https://www.tbb.com.tw/" target="_blank" role="button">
										<spring:message code="LB.TAIWAN_BUSINESS_BANK" />
									</a>
								</c:otherwise>
							</c:choose>
						</li>
						<li>
							<!-- 網路安全 --> 
							<a href="${__ctx}/CUSTOMER/SERVICE/network_security" target="_self" role="button">
								<spring:message code="LB.Network_security" />
							</a>
						</li>
						<li>
							<!-- 網站導覽 -->
							<a href="${__ctx}/CUSTOMER/SERVICE/site_guide" target="_self" role="button">
								<spring:message code="LB.Service_overview" />
							</a>
						</li>
						<li>
							<!-- 意見信箱 --> <c:choose>
								<c:when test="${__i18n_locale eq 'en' }">
									<a href="https://www.tbb.com.tw/web/guest/contact-us" target="_blank" role="button">
										<spring:message code="LB.Customer_service" />
									</a>
								</c:when>
								<c:when test="${__i18n_locale eq 'zh_TW'}">
									<a href="https://www.tbb.com.tw/-47" target="_blank" role="button">
										<spring:message code="LB.Customer_service" />
									</a>
								</c:when>
								<c:otherwise>
									<a href="https://www.tbb.com.tw/-47" target="_blank" role="button">
										<spring:message code="LB.Customer_service" />
									</a>
								</c:otherwise>
							</c:choose>
						</li>
					</ul>
				</div>
				
				<div class="enterprise-info  d-none d-lg-block">
					<p>
						<!-- 客服 -->
						<spring:message code="LB.X2154" />
						<a href="tel:0800-017-171">
							0800-01-7171
						</a>
						<!-- 限市話 -->
						<spring:message code="LB.X2211" />
						<c:if test="${__i18n_locale eq 'en'}">,</c:if>
						<a href="tel:02-2357-7171">
							02-2357-7171
						</a>
					</p>
					<div style="text-align: center;">
						<a href="https://www.facebook.com/tbbdreamplus/" target="_blank">
							<span class="facebook-icon">&nbsp;</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>