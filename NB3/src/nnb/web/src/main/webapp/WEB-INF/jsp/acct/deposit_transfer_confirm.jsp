<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
    <%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>   
	<style>
		.noZhCn{
			-ms-ime-mode: disabled;
		}
	</style>
	
    <script type="text/javascript">
//     var idgatesubmit= $("#formId");
//     var idgateadopid="N074";
//     var idgatetitle="臺灣中小企銀臺幣轉入綜存定存";
    
    $(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 初始化驗證碼
		setTimeout("initKapImg()", 200);
		// 生成驗證碼
		setTimeout("newKapImg()", 300);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
		
    });
    
 // 畫面初始化
	function init() {
	 
    	focusBHO();
		// 表單驗證初始化
		$("#formId").validationEngine({binded: false,promptPosition: "inline"});
		// 確認鍵 click
		goOn();
		// 上一頁按鈕 click
		goBack();
		//交易類別change 事件
		changeFgtxway();
		
		var checkaddAcn = ${deposit_transfer_confirm.data.ADDACN};
    	if(checkaddAcn){
    		console.log("checkaddAcn >>>> "+ checkaddAcn)
    		addacn();//新增常用帳號
    	}else{
    		console.log("checkaddAcn >>>> "+ checkaddAcn)
    	}
	}
    	
    function focusBHO(){
    	//一開始先FOCUS到BHO
    	$("#transInAccountText1").focus();
      	//跳格
		$("#transInAccountText1").keyup(function(event){
			if(event.which != 8 && event.which != 46 && event.which != 9){
				$("#transInAccountText2").focus();
			}
		});
		$("#transInAccountText2").keyup(function(event){
			if(event.which != 8 && event.which != 46 && event.which != 9){
				$("#transInAccountText3").focus();
			}
		});
    }
    
	
 // 確認鍵 Click
	function goOn() {
		 // form Submit 
        $("#CMSUBMIT").click(function() {
        	//送出進表單驗證前將span顯示
    		$("#hideblock").show();
    		console.log("submit~~");
    		//塞值進span內的input
    		$("#CARDNUM_TOTAL").val($("#transInAccountText1").val()+$("#transInAccountText2").val()+$("#transInAccountText3").val());
            if (!$('#formId').validationEngine('validate')) {
                e.preventDefault();
            } else {
                $("#formId").validationEngine('detach');
                //送出
                var uri = "${__ctx}/checkBHO"
                rdata = {
                    keyInBHO: $("#transInAccountText1").val() + $("#transInAccountText2").val() + $("#transInAccountText3").val(),
                    functionName: "deposit_transfer"
                };
                fstop.getServerDataEx(uri, rdata, false, checkBHOFinish);
            }
        });
	}
	//檢查BHO
    function checkBHOFinish(data) {
        if (data.result == true) {
            processQuery();
        } else {
            //alert(data.message);
            errorBlock(
					null, 
					null,
					[data.message], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
        }
    }
 
	// 交易機制選項
	function processQuery() {
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
		console.log("fgtxway: " + fgtxway);
		if(!checkAmountByFGTXWAY(fgtxway)){
			return false;
		}
		switch (fgtxway) {
			case '0':
				$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
				$('#CMPASSWORD').val("");
				initBlockUI();//遮罩
				$("#formId").submit();
				break;
			// IKEY
			case '1':
				useIKey();
				break;
			// 晶片金融卡
			case '2':
				var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
				useCardReader(capUri);
				break;
			case '7'://IDGATE認證
			    idgatesubmit= $("#formId");
                showIdgateBlock();
                break;
			default:
			//alert("nothing...");
			errorBlock(
					null, 
					null,
					["nothing..."], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
		}
	}
	//交易類別change 事件
	function changeFgtxway() {
		$('input[type=radio][name=FGTXWAY]').change(function () {
			console.log(this.value);
			if (this.value == '0') {
				$("#CMPASSWORD").addClass("validate[required]")
				// 若非晶片金融卡則隱藏驗證碼欄位
				$("#chaBlock").hide();
			} else if (this.value == '1') {
				$("#CMPASSWORD").removeClass("validate[required]");
				// 若非晶片金融卡才顯示驗證碼欄位
				$("#chaBlock").hide();
			} else if (this.value == '2') {
				$("#CMPASSWORD").removeClass("validate[required]");
				// 若為晶片金融卡則隱藏驗證碼欄位
				$("#chaBlock").show();
			}else if(this.value == '7'){
				$("#CMPASSWORD").removeClass("validate[required]");
				$("#chaBlock").hide();
			}
		});
	}
	// 上一頁按鈕 click
	function goBack() {
		$("#CMBACK").click(function() {
			var action = '${__ctx}/NT/ACCT/TDEPOSIT/deposit_transfer';
			$('#back').val("Y");
			$("form").attr("action", action);
			$("#formId").validationEngine('detach');
			initBlockUI();
			$("form").submit();
		});
	}
	// 驗證碼刷新
	function changeCode() {
		$('input[name="capCode"]').val('');
		// 大小版驗證碼用同一個
		console.log("changeCode...");
		$('img[name="kaptchaImage"]').hide().attr(
			'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

		// 登入失敗解遮罩
		unBlockUI(initBlockId);
	}

	// 初始化驗證碼
	function initKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
	}

	// 生成驗證碼
	function newKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').click(function () {
			$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
		});
	}
	
// 	根據交易機制檢查金額
	function checkAmountByFGTXWAY(fgtxway){
		var retStr = "";
		var transferType = $('#TransferType').val();
		var fgsvacno = $('#FGSVACNO').val();
		var amount = $('#AMOUNT').val();
			amount = fstop.unFormatAmtToInt(amount);
			console.log("amount: " + amount);
			console.log("transferType: " + transferType);
			console.log("fgsvacno: " + fgsvacno);
		if('2' == fgtxway){
			if( (transferType =='NPD' || fgsvacno =='2')  &&  amount > 100000){
				retStr = "<spring:message code= "LB.X1413" />\n\n <spring:message code= "LB.X1417" />";
				errorBlock(
							null, 
							null,
							["<spring:message code= "LB.X1413" />", "\n", "\n", "<spring:message code= "LB.X1417" />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				return false;
			}
		}
		else if('7' == fgtxway){
			//TODO 金額欄位錯誤訊息
            if( (transferType =='NPD' || fgsvacno =='2')  &&  amount > 100000){
                retStr = "<spring:message code= "LB.X1413" />\n\n <spring:message code= "LB.X1417" />";
                errorBlock(
                            null, 
                            null,
                            ["<spring:message code= "LB.X1413" />", "\n", "\n", "<spring:message code= "LB.X1417" />"], 
                            '<spring:message code= "LB.Quit" />', 
                            null
                        );
                return false;
            }
        }
		
		return true;
		
	}
	
		/* 加入常用帳號
		DPGONAME 好記名稱 EX:_DPGONAME : 050-臺灣企銀 <br>
		DPPHOTOID 好記圖片 EX: _DPPHOTOID : 050-臺灣企銀 <br>
		DPTRACNO 約定否 :1：約定 2：非約定 <br>
		DPTRIBANK 代碼 <br>
		DPTRDACNO 帳號
		*/
	function addacn() {
	    var acno = '${deposit_transfer_confirm.data.INTSACN}';
	    var dpgoname = '050-<spring:message code= "LB.W0605" />';
	    var dptracno = '2';
	    var dptribank = '050';
	    uri = '${__ctx}' + "/NT/ACCT/TDEPOSIT/addAcn_aj"
	    console.log("uri>>" + uri);
	    rdata = {
	        DPGONAME: dpgoname,
	        DPPHOTOID: dpgoname,
	        DPTRACNO: dptracno,
	        DPTRIBANK: dptribank,
	    	DPTRDACNO: acno
	    };
	    var data = fstop.getServerDataEx(uri, rdata, false);
	    console.log("data>>" + data);
        console.log("data.MsgCode: " + data.data.MsgCode);
        console.log("data.MsgName: " + data.data.MsgName);
	    if(data != null && data.result == true){
	    	
	    }else{
	    	//alert(data.data.MsgName);
	    	errorBlock(
					null, 
					null,
					[data.data.MsgName], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
	    }
	}

	//重新輸入
 	function formReset() {
 		if ($('#actionBar').val()=="reEnter"){
	 		document.getElementById("formId").reset();
 			$('#actionBar').val("");
 		}
	}
    </script>
</head>

<body>
	<!-- 交易機制所需畫面    -->
	<%@ include file="../component/trading_component.jsp"%>
    <!--   IDGATE -->
    <%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %> 
    <!-- header     -->
    <header>
        <%@ include file="../index/header.jsp"%>
    </header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 定存服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Current_Deposit_To_Time_Deposit" /></li>
    <!-- 轉入綜存定存     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Open_Taiwan_Currency_Time_Deposit" /></li>
		</ol>
	</nav>



    <!-- menu、登出窗格 -->
    <div class="content row">
        <!-- 功能選單內容 -->
        <%@ include file="../index/menu.jsp"%>
        <!-- 		主頁內容  -->
        <main class="col-12">
            <section id="main-content" class="container">
                <h2>
                    <!--  轉入臺幣綜存定存 -->
                    <spring:message code="LB.Open_Taiwan_Currency_Time_Deposit" />
                </h2>
                <i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block no-l-display-btn">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Execution_option" /></option>
						<!-- 重新輸入-->
						<option value="reEnter"><spring:message code="LB.Re_enter"/></option>
					</select>
				</div>
                <form method="post" id="formId" action="${__ctx}/NT/ACCT/TDEPOSIT/deposit_transfer_result">
                	<%-- 回上一頁參數 --%>
					<input type="hidden" id="back" name="back" value="">
					<input type="hidden" id="ADDACN" name="ADDACN" value="${deposit_transfer_confirm.data.ADDACN}">
					<%-- 交易代號 --%>
					<input type="hidden" name="ADOPID" value="N074">
					<%-- 驗證相關 --%>
					<input type="hidden" id="PINNEW" name="PINNEW" value="">
					<input type="hidden" id="jsondc" name="jsondc" value='${deposit_transfer_confirm.data.jsondc}'>
					<input type="hidden" id="ISSUER" name="ISSUER" value="">
					<input type="hidden" id="ACNNO" name="ACNNO" value="">
					<input type="hidden" id="TRMID" name="TRMID" value="">
					<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
					<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
					<input type="hidden" id="TAC" name="TAC" value="">
					<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">

                    <c:set var="BaseResultData" value="${deposit_transfer_confirm.data}"></c:set>
                     <%--  TXTOKEN  防止重送代碼--%>
                    <input type="hidden" name="TXTOKEN" value="${BaseResultData.TXTOKEN}" />
                    <%-- 約定非約定註記 1 約定帳號, 2 非約定帳號 --%>
                    <input type="hidden" id="FGSVACNO" name="FGSVACNO" value="${BaseResultData.FGSVACNO}">
                    <%-- 約定轉入帳號 JSON --%>
                    <input type="hidden" name="DPAGACNO" value='${BaseResultData.DPAGACNO}'>
                    <%--非約定轉入帳號 --%>
                    <input type="hidden" name="DPACNO" value='${BaseResultData.DPACNO}'>
                    <%-- 元件所需轉入帳號 --%>
                    <input type="hidden" name="INTSACN" value='${BaseResultData.INTSACN}'>
                    <%-- 轉出帳號 --%>
                    <input type="hidden" name="ACN" value='${BaseResultData.OUTACN}'>
                    <%-- 轉帳金額 --%>
                    <input type="hidden" id="AMOUNT" name="AMOUNT" value="${BaseResultData.AMOUNT}">
                    <%-- 存款種類 --%>
                    <input type="hidden" name="FDPACC" value="${BaseResultData.FDPACC}">
                    <%-- 存單期別 --%>
                    <input type="hidden" name="TERM" value="${BaseResultData.TERM}">
                    <%-- 計息方式，0機動，1固定 --%>
                    <input type="hidden" name="INTMTH" value="${BaseResultData.INTMTH}">
                    <%-- 是否到期轉期 --%>
                    <input type="hidden" name="CODE" value="${BaseResultData.CODE}">
                    <%-- 轉存方式 1本金轉期，利息轉入帳號；2本金及利息一併轉期--%>
                    <input type="hidden" name="FGSVTYPE" value="${BaseResultData.FGSVTYPE}">
                    <%-- 到期轉期次數 --%>
                    <input type="hidden" name="TXTIMES" value="${BaseResultData.TXTIMES}">
                    <%-- 存款期別或指定到期日 --%>
                    <input type="hidden" name="FGTDPERIOD" value="${BaseResultData.FGTDPERIOD}">
                    <%--  指定到期日-轉帳日期 --%>
                    <input type="hidden" name="CMDATE1" value="${BaseResultData.CMDATE1}">
                    <%--  預約日期註記 1:即時 2:預約--%>
                    <input type="hidden" name="FGTXDATE" value="${BaseResultData.FGTXDATE}">
                    <%--  預約日期--%>
                    <input type="hidden" name="CMDATE" value="${BaseResultData.CMDATE}">
                    <%-- 交易備註 --%>
                    <input type="hidden" name="CMTRMEMO" value="${BaseResultData.CMTRMEMO}">
                    <%-- Email信箱 --%>
                    <input type="hidden" name="CMTRMAIL" value="${BaseResultData.CMTRMAIL}">
                    <%-- Email摘要內容 --%>
                    <input type="hidden" name="CMMAILMEMO" value="${BaseResultData.CMMAILMEMO}">
                    <%--存款期別顯示 --%>
                    <input type="hidden" name="TERM_TEXT" value="${BaseResultData.TERM_TEXT}">
                    <%--到期是否轉期 --%>
                    <input type="hidden" name="CODENAME" value="${BaseResultData.CODENAME}">
					<%--續存方式 --%>
                    <input type="hidden" name="DPSVTYPENAME" value="${BaseResultData.DPSVTYPENAME}">
					<%--轉入帳號--%>
                    <input type="hidden" name="DPAGACNO_TEXT" value="${BaseResultData.DPAGACNO_TEXT}">
					<%--轉帳日期--%>
                    <input type="hidden" name="transfer_date" value="${BaseResultData.transfer_date}">
                    
                    <input type="hidden" id="TransferType" name="TransferType" value="${BaseResultData.TransferType}">
                    
                    <!--交易步驟 -->
                    <div id="step-bar">
                        <ul>
                            <!--輸入資料 -->
                            <li class="finished"><spring:message code="LB.Enter_data" /></li>
                            <!-- 確認資料 -->
                            <li class="active"><spring:message code="LB.Confirm_data" /></li>
                            <!-- 交易完成 -->
                            <li class=""> <spring:message code="LB.Transaction_complete" /></li>
                        </ul>
                    </div>
                    <!--交易步驟-END -->
                    <!-- 表單顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
                            <div class="ttb-input-block">
								<!--show message -->
                               <div class="ttb-message">
									<!--顯示即時OR預約 -->
                                		<c:choose>
					      				<c:when test="${BaseResultData.FGTXDATE == '1'}">
					      					 <p ><spring:message code="LB.Immediately" /></p>
					      				</c:when>
					      				<c:otherwise>
					      					 <p ><spring:message code="LB.Booking" /></p>
					      				</c:otherwise>
					      				</c:choose>
                                    <!-- 請確認轉帳資料 -->
                                    <span>
                                        <spring:message code="LB.Confirm_transfer_data" />
                                    </span>
                                </div>
                                <!-- 轉帳日期 -->
                                <div class="ttb-input-item row">
                                    <span class="input-title">
                                        <label>
                                           <h4><spring:message code="LB.Transfer_date" /></h4>
                                        </label>
                                    </span>
                                    <span class="input-block">
                                        <div class="ttb-input">
                                            <p>                                             
					      					 ${BaseResultData.transfer_date }
                                            </p>
                                        </div>
                                    </span>
                                </div>
                                <!-- 轉出帳號 -->
                                <div class="ttb-input-item row">
                                    <span class="input-title">
                                        <label>
                                            <h4><spring:message code="LB.Payers_account_no" /></h4>
                                        </label>
                                    </span>
                                    <span class="input-block">
                                        <div class="ttb-input">
                                            <p>
                                                ${BaseResultData.OUTACN }
                                            </p>
                                        </div>
                                    </span>
                                </div>
                                <!-- 轉入帳戶 -->
                                <div class="ttb-input-item row">
                                    <span class="input-title">
                                        <label>
                                          <h4><spring:message code="LB.Payees_account_no" /></h4>
                                        </label>
                                    </span>
                                    <span class="input-block">
                                        <div class="ttb-input">
                                            <p>
                                                ${BaseResultData.DPAGACNO_TEXT}
                                            </p>
                                        </div>
                                    </span>
                                </div>
                                 <!-- 轉入帳號確認 -->
						        <div class="ttb-input-item row">
						            <span class="input-title">
						                <label>
						                    <h4>
						                        <spring:message code="LB.Confirm_payers_account_no" />
						                    </h4>
						                </label>
						            </span>
						            <span class="input-block">
						                <div class="ttb-input">
						                    <img src="${__ctx}/getBHO?transferInAccount=${BaseResultData.transferInAccount}&functionName=deposit_transfer" />
						                    <br /><br />
						                </div>
						                <div class="BHOInput">
						                    <input type="text" id="transInAccountText1" maxlength="1" class="text-input noZhCn" />
						                    -
						                    <input type="text" id="transInAccountText2" maxlength="1" class="text-input noZhCn" />
						                    -
						                    <input type="text" id="transInAccountText3" maxlength="1" class="text-input noZhCn" />
						                    <!-- （請以半型字輸入黃色標記之轉入帳號數字） -->
						                    <span class="input-unit">
						                            <spring:message code="LB.Anti-blocking_BHO_attack"/>
						                    </span>
						                    <!-- 不在畫面上顯示的span -->
											<span id="hideblock" >
											<!-- 驗證用的input -->
											<input id="CARDNUM_TOTAL" name="CARDNUM_TOTAL" type="text" maxlength="3"  class="text-input validate[required,minSize[3],custom[integer]]" 
											style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
						                </div>
						            </span>
						        </div>
                                <!-- 轉帳金額 -->
                                <div class="ttb-input-item row">
                                    <span class="input-title">
                                        <label>
                                            <h4><spring:message code="LB.Amount" /></h4>
                                        </label>
                                    </span>
                                    <span class="input-block">
                                        <div class="ttb-input">
                                            <p class="high-light">
	                                            <!-- 新台幣 -->
	                                            <span class="input-unit"><spring:message code="LB.NTD"/></span>
	                                            <!--顯示金額 -->
	                                                <fmt:formatNumber type="number" minFractionDigits="2" value="${BaseResultData.AMOUNT }" />
	                                            <!--元 -->
	                                            <span class="input-unit"><spring:message code="LB.Dollar"/></span>
                                            </p>
                                        </div>
                                    </span>
                                </div>
                                <!-- 存款種類-->
                                <div class="ttb-input-item row">
                                    <span class="input-title">
                                        <label>
                                            <h4><spring:message code="LB.Deposit_type" /></h4>
                                        </label>
                                    </span>
                                    <span class="input-block">
                                        <div class="ttb-input">
                                            <p>
                                                ${BaseResultData.FDPACCNAME }
                                            </p>
                                        </div>
                                    </span>
                                </div>
                                <!-- 存款期別或指定到期日-->
                                <div class="ttb-input-item row">
                                    <span class="input-title">
                                        <label>
                                            <h4><spring:message code="LB.Deposit_period_or_Designated_due_date" /></h4>
                                        </label>
                                    </span>
                                    <span class="input-block">
                                        <div class="ttb-input">
                                            <p>
                                                ${BaseResultData.TERM_TEXT }
                                            </p>
                                        </div>
                                    </span>
                                </div>
                                <!-- 計息方式-->
                                <div class="ttb-input-item row">
                                    <span class="input-title">
                                        <label>
                                            <h4><spring:message code="LB.Interest_calculation" /></h4>
                                        </label>
                                    </span>
                                    <span class="input-block">
                                        <div class="ttb-input">
                                            <p>
                                                ${BaseResultData.INTMTHNAME }
                                            </p>
                                        </div>
                                    </span>
                                </div>
                                <!-- 到期轉期-->
                                <div class="ttb-input-item row">
                                    <span class="input-title">
                                        <label>
                                            <h4><spring:message code="LB.Roll-over_when_expiration" /></h4>
                                        </label>
                                    </span>
                                    <span class="input-block">
                                        <div class="ttb-input">
                                            <p>
                                                ${BaseResultData.CODENAME }
                                            </p>
                                        </div>
                                    </span>
                                </div>
                                <!-- 轉存方式 ;不轉期時不顯示轉存方式-->
                                <c:if test="${BaseResultData.CODE != '0'}">
	                                <div class="ttb-input-item row">
	                                    <span class="input-title">
	                                        <label>
	                                            <h4><spring:message code="LB.Rollover_method" /></h4>
	                                        </label>
	                                    </span>
	                                    <span class="input-block">
	                                        <div class="ttb-input">
	                                            <p>
	                                                ${BaseResultData.DPSVTYPENAME }
	                                            </p>
	                                        </div>
	                                    </span>
	                                </div>
                                </c:if>
                            <!-- 交易機制 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Transaction_security_mechanism" /></h4>
									</label>
								</span>
								<span class="input-block">
									<!-- 交易密碼SSL -->
									<div class="ttb-input">
										<label class="radio-block">
											<spring:message code="LB.SSL_password" />
											<input type="radio" name="FGTXWAY" checked="checked" value="0">
											<span class="ttb-radio"></span>
										</label>
									</div>
									<!--請輸入密碼 -->
									<div class="ttb-input">
										<spring:message code="LB.Please_enter_password" var="pleaseEnterPin" />
										<input type="password" id="CMPASSWORD" name="CMPASSWORD" class="text-input validate[required]" maxlength="8" placeholder="${plassEntpin}">
									</div>
									<!-- 使用者是否可以使用IKEY -->
									<c:if test="${sessionScope.isikeyuser}">
									<!-- 即使使用者可以用IKEY，若轉出帳號為晶片金融卡帶出的非約定帳號，也不給用 (By 景森)-->
										<c:if test = "${BaseResultData.TransferType != 'NPD'}">
										<!--電子簽章(請載入載具i-key) -->
											<div class="ttb-input">
												<label class="radio-block">
													<spring:message code="LB.Electronic_signature" />
													<input type="radio" name="FGTXWAY" id="CMIKEY" value="1" />
													<span class="ttb-radio"></span>
												</label>
											</div>
										</c:if>
									</c:if>
									<!-- 晶片金融卡 -->
									<div class="ttb-input">
										<label class="radio-block">
											<spring:message code="LB.Financial_debit_card" />
											<input type="radio" name="FGTXWAY" id="CMCARD" value="2" />
											<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input" name="idgate_group" style="display:none">
                                        <label class="radio-block">裝置推播認證(請確認您的行動裝置網路連線是否正常，及推播功能是否已開啟)
                                        <input type="radio" id="IDGATE" name="FGTXWAY" value="7"> <span class="ttb-radio"></span></label>
                                    </div>
								</span>
							</div>
							<!-- 交易機制區塊 END -->
							<!-- 驗證碼-->
							<div class="ttb-input-item row" id="chaBlock" style="display:none">
								<span class="input-title">
									<label>
										<!-- 驗證碼 -->
										<h4><spring:message code="LB.Captcha" /></h4>
									</label>
								</span>
								<span class="input-block">
									<spring:message code="LB.Captcha" var="labelCapCode" />
									<input id="capCode" name="capCode" type="text" class="text-input" placeholder="${labelCapCode}" maxlength="6" autocomplete="off">
									<img name="kaptchaImage" class = "verification-img" src="" />
									<button class="ttb-sm-btn btn-flat-orange" type="button" name="reshow" onclick="changeCode()">
										<spring:message code="LB.Regeneration" />
									</button>
								</span>
							</div>
							<!-- 驗證碼 END-->

                            </div>
                            <!-- button -->
                                 <!--重新輸入 -->
								<spring:message code="LB.Re_enter" var="cmRest"></spring:message>
								<input type="reset" name="CMRESET" id="CMRESET" value="${cmRest}" class="ttb-button btn-flat-gray no-l-disappear-btn">
                                <!--回上頁 -->
                                <spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>
                                <input type="button" name="CMBACK" id="CMBACK" value="${cmback}" class="ttb-button btn-flat-gray">
                                 <!-- 確定 -->
                                <spring:message code="LB.Confirm" var="cmSubmit"></spring:message>
                                <input type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" class="ttb-button btn-flat-orange">
                            <!-- button -->
                        </div>
                    </div>
                </form>
            </section>
            <!-- 		main-content END -->
        </main>
    </div><!-- content row END -->
    <%@ include file="../index/footer.jsp"%>
</body>

</html>