<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp"%>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<!-- 元件驗證身分JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 50);
			// 開始跑下拉選單並完成畫面
			setTimeout("init()", 100);
			// 初始化驗證碼
			setTimeout("initKapImg()", 200);
			// 生成驗證碼
			setTimeout("newKapImg()", 300);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
			// 將.table變更為footable
			//initFootable();
			setTimeout("initDataTable()",100);
	});

    function init(){
    	//初始化表單
    	$("#formId").validationEngine({
			binded: false,
			promptPosition: "inline"
		});	
    	
		$("#CMSUBMIT").click(function(e){		
			
			if($("input[type='checkbox']:checkbox:checked") > 10){
				//alert("<spring:message code= "LB.Alert165" />");	
				errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert165' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				e.preventDefault();
			}
			
			e = e || window.event;
			
			if(!$('#formId').validationEngine('validate')){
	        	e.preventDefault();
			}
			else{
 				$("#formId").validationEngine('detach');
 				initBlockUI();
 				processQuery(); 
//  			$("#formId").attr("action","${__ctx}/ONLINE/SERVING/predesignated_account_logout_r");
// 	  			$("#formId").submit();
 			}		
  		});
		
	
		//上一頁按鈕
		$("#previous").click(function() {
			initBlockUI();
			fstop.getPage('${pageContext.request.contextPath}'+'/ONLINE/SERVING/predesignated_account_logout','', '');
		});

		// 判斷顯不顯示驗證碼
		chaBlock();
		// 交易機制 click
		fgtxwayClick();
		
   	}

	// 交易機制選項
	function processQuery() {
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
		console.log("fgtxway: " + fgtxway);
		switch (fgtxway) {
				// IKEY
			case '1':
				useIKey();
				unBlockUI(initBlockId);
				break;
				// 晶片金融卡
			case '2':
				var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
				useCardReader(capUri);
				unBlockUI(initBlockId);
				break;
			case '7'://IDGATE認證
				showIdgateBlock();
				break;
			default:
				//alert("<spring:message code= "LB.Alert001" />");
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert001' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);	
			unBlockUI(initBlockId);
		}
	}
	
	// 使用者選擇晶片金融卡要顯示驗證碼區塊
 	function fgtxwayClick() {
 		$('input[name="FGTXWAY"]').change(function(event) {
 			// 判斷交易機制決定顯不顯示驗證碼區塊
 			chaBlock();
		});
	}
 	// 判斷交易機制決定顯不顯示驗證碼區塊
	function chaBlock() {
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);	
			
			switch(fgtxway) {
			case '1':
				$("#chaBlock").hide();
				break;
			case '2':
				$("#chaBlock").show();
		    	break;
		    	
			default:
				$("#chaBlock").hide();
		}	
 	}
	// 驗證碼刷新
	function changeCode() {
		$('input[name="capCode"]').val('');
		// 大小版驗證碼用同一個
		console.log("changeCode...");
		$('img[name="kaptchaImage"]').hide().attr(
			'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

		// 登入失敗解遮罩
		unBlockUI(initBlockId);
	}

	// 初始化驗證碼
	function initKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
	}

	// 生成驗證碼
	function newKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').click(function () {
			$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100))
			.fadeIn();
		});
	}
	
	//複寫commonMethod.js
	//卡片押碼結束
	function generateTACFinish(result){
		if(window.console){console.log("generateTACFinish...");}
		//成功
		if(result != "false"){
			// e.x. E000,00000551,6BF84A4B9319B145A64F3866506D3313594B10D08CDEA863BFA8F9D6
			var TACData = result.split(",");
			
			var formId = document.getElementById("formId");
			formId.iSeqNo.value = TACData[1];
			formId.ICSEQ.value = TACData[1];
			formId.TAC.value = TACData[2];

	 		var ACN_Str1 = formId.ACNNO.value;
			if(ACN_Str1.length > 11){
				ACN_Str1 = ACN_Str1.substr(ACN_Str1.length - 11);
			}
			formId.ACNNO.value = ACN_Str1;           
			
			// 遮罩
			initBlockUI();
			
			formId.submit();
		}
		//失敗
		else{
			FinalSendout("MaskArea",false);
		}
	}
	
</script>
</head>
<body>
	 <!--   IDGATE -->
    <%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %>	
	<!-- 	晶片金融卡 -->
	<%@ include file="../component/trading_component.jsp"%>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 約定轉入帳號取消     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0268" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code="LB.X0268"/></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="${__ctx}/ONLINE/SERVING/predesignated_account_logout_r">
				<!-- 			晶片金融卡 -->
				<input type="hidden" id="jsondc" name="jsondc" value='${predesignated_account_logout.data.jsondc}'>
				<input type="hidden" id="ISSUER" name="ISSUER" value="">
				<input type="hidden" id="ACNNO" name="ACNNO" value="">
				<input type="hidden" id="TRMID" name="TRMID" value="">
				<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
				<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
				<input type="hidden" id="TAC" name="TAC" value="">
				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
				<input type="hidden" id="data" name="data" value="<c:out value='${predesignated_account_logout.data.REC}' />">
				<input type="hidden" id="COUNT" name="COUNT" value="${predesignated_account_logout.data.COUNT}">
				
				
				<input type="hidden" name="TYPE" id="TYPE" value="02">
				<input type="hidden" name="KIND" id="KIND" value="">
				<input type="hidden" name="ACNINFO" id="ACNINFO" value="">
				<div class="main-content-block row">
					<div class="col-12">
						<!-- 線上約定轉入帳號註銷表 -->
						<ul class="ttb-result-list"></ul>
						<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
							<thead>
							<tr>
								<th><spring:message code="LB.Bank_name"/></th>
								<th><spring:message code="LB.D0227" /></th>
								<th><spring:message code="LB.Favorite_name"/></th>
							</tr>
							</thead>
							<tbody>
								<c:forEach var="dataList" items="${predesignated_account_logout.data.REC}">
									<tr>
										<td>${dataList.BNKCOD}</td>
										<td>${dataList.ACN}</td>
										<td><c:out value='${dataList.DPGONAME}' /></td>
									</tr>
								</c:forEach>		
							</tbody>
						</table>
						<div class="ttb-input-block">
							<!-- 交易機制 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.Transaction_security_mechanism" />
									</label>
								</span>
								<span class="input-block">
									<!-- 使用者是否可以使用IKEY -->
									<c:if test="${sessionScope.isikeyuser}">
										<!--電子簽章(請載入載具i-key) -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Electronic_signature" />
												<input type="radio" name="FGTXWAY" id="CMIKEY" value="1" />
												<span class="ttb-radio"></span>
											</label>
										</div>
									</c:if>
									<div class="ttb-input" name="idgate_group" style="display:none">
                                        <label class="radio-block">裝置推播認證(請確認您的行動裝置網路連線是否正常，及推播功能是否已開啟)<input type="radio" id="IDGATE"
                                            name="FGTXWAY" value="7"> <span class="ttb-radio"></span></label>
                                    </div>
									<!-- 晶片金融卡 -->
									<div class="ttb-input">
										<label class="radio-block">
											<spring:message code="LB.Financial_debit_card" />
											<input type="radio" name="FGTXWAY" id="CMCARD" value="2" />
											<span class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>
							<!-- 驗證碼-->
							<div class="ttb-input-item row" id="chaBlock" style="display:none">
								<span class="input-title">
									<label>
										<!-- 驗證碼 -->
										<spring:message code="LB.Captcha" />
									</label>
								</span>
								<span class="input-block">
                                    <div class="ttb-input">
										<img name="kaptchaImage" class="verification-img" src="" />
										<button class="ttb-sm-btn btn-flat-orange" type="button" name="reshow"
											onclick="changeCode()">
											<spring:message code="LB.Regeneration" />
										</button>
                                    </div>
                                    <div class="ttb-input">
										<spring:message code="LB.Captcha" var="labelCapCode" />
										<input id="capCode" name="capCode" type="text" class="text-input"
											placeholder="${labelCapCode}" maxlength="6" autocomplete="off">
									</div>
								</span>
							</div>
						</div>
						<!-- 重新輸入 -->
						<input type="reset" id="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter"/>"/>
						<!--回上頁 -->
						<input id="previous" name="previous" type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page"/>"/>
						<!-- 確定 -->
                        <input type="button" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm"/>" >
					</div>
				</div>
			</form>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>