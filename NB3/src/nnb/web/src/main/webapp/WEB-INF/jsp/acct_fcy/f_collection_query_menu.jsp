<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 帳戶查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Account_Inquiry" /></li>
    <!-- 進口/出口託收查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0176" /></li>
		</ol>
	</nav>



	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">

		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->

		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2>
				<spring:message code="LB.W0176" /><!-- 進口/出口託收查詢 -->
			</h2> 
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" action="${__ctx}/FCY/ACCT/f_collection_query_type" method="post">
				<div class="main-content-block row">
					<div class="col-12">
						<div class="ttb-input-block">
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label><spring:message code="LB.W0177" /></label>
								</span>
							 	<span class="input-block">
										<!-- 進口託收 -->
									<div class="ttb-input">
										<label class="radio-block">
										<spring:message code="LB.W0178" />
											<input type="radio" name="FGMSGCODE" value="N559" checked="checked" />
											<span class="ttb-radio"></span>
										</label> 
									</div>
									<div class="ttb-input">
									<!-- 信用狀項下出口託收 -->
										<label class="radio-block">
											<spring:message code="LB.W0179" />
											<input type="radio" name="FGMSGCODE" value="N560" />
											<span class="ttb-radio"></span>
										</label> 
									</div>
									<div class="ttb-input">
										<label class="radio-block">
											<spring:message code="LB.X0030" />
											<input type="radio" name="FGMSGCODE" value="N563" />
											<span class="ttb-radio"></span>
										</label> 
									</div>
								</span>
							</div>
						</div>
						<div>
						</div>
						
						<input type="submit" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" onclick="initBlockUI();">
							<!-- 確定 -->
					</div>
					</div>
				</form>
			</section>
		</main>
	</div>	
<%@ include file="../index/footer.jsp"%>
</body>
</html>