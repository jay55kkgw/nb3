<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="card-detail-block">
	<div class="card-center-block">
		<!-- Q1 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block" data-toggle="collapse" href="#popup2-1" aria-expanded="true" aria-controls="popup2-1">
	  		  <div class="row">
	        		<span class="col-1">Q1</span>
	            		<div class="col-11">
	                		<span>Report on "settlement foreign exchange sources"</span>
	            		</div>
	    		</div>
			</a>
			<div id="popup2-1" class="ttb-pup-collapse collapse" role="tabpanel">
	    		<div class="ttb-pup-body">
	        		<ul class="ttb-pup-list">
	            		<li>
	                		<p>Fill in the source of the settlement foreign exchange.</p>
	            		</li>
	    		</div>
			</div>
		</div>
		<!-- Q2 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block" data-toggle="collapse" href="#popup2-2" aria-expanded="true" aria-controls="popup2-2">
	  		  <div class="row">
	        		<span class="col-1">Q2</span>
	            		<div class="col-11">
	                		<span>Report on "declaration date"</span>
	            		</div>
	    		</div>
			</a>
			<div id="popup2-2" class="ttb-pup-collapse collapse" role="tabpanel">
	    		<div class="ttb-pup-body">
	        		<ul class="ttb-pup-list">
	            		<li>
	                		<p>Fill in the date of the declaration of settlement foreign exchange.</p>
	            		</li>
	    		</div>
			</div>
		</div>
		<!-- Q3 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block" data-toggle="collapse" href="#popup2-3" aria-expanded="true" aria-controls="popup2-3">
				<div class="row">
					<span class="col-1">Q3</span>
					<div class="col-11">
						<span>Reporting of the "reporting obligor"</span>
					</div>
				</div>
			</a>
			<div id="popup2-3" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div class="ttb-result-list terms">
								<ol>
									<li>1. The owner of the capital of the NT$500,000 or equivalent foreign exchange receipts and payments or transactions in the Republic of China shall be the reporting obligor, and shall declare it in his own name except with the consent of the Central Bank or the Central Bank has other regulations.
									</li>
										
									<li>2. When the reporting obligor entrusts others to handle the NTD settlement report, it shall fill in the report in the name of the consignor, and issue a power of attorney and attach the identity documents of the consignor and the consignee.
									</li>
									
									<li>3. A non-resident civil legal person shall issue a power of attorney authorizing the natural or legal person in the territory of the Republic of China to be the reporting obligor, and to report in the name of the authorized representative or agent and to state the facts of the agent. However, non-Republic of China financial institutions may not use the remittance funds for settlement.
									</li>
								</ol>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q4 -->
		<div class="ttb-pup-block" role="tab">
	    	<a role="button" class="ttb-pup-title collapsed d-block" data-toggle="collapse" href="#popup2-4" aria-expanded="true" aria-controls="popup2-4">
	        	<div class="row">
	            	<span class="col-1">Q4</span>
	                	<div class="col-11">
	                    	<span>Reporting of the "Reporting Obligor Registration Certificate Number"</span>
	                    </div>
	            </div>
	        </a>
	        <div id="popup2-4" class="ttb-pup-collapse collapse" role="tabpanel">
	        	<div class="ttb-pup-body">
	            	<ul class="ttb-pup-list">
	                	<li>
							<div class="ttb-result-list terms">
							<ol>
								<li>(1) Company corporation number
									<ol>
										<li>Fill in the tax ID number approved by the competent authority and provide a copy of the registration form or the most recent change registration form.
										</li>
										<li>For the preparatory office for overseas investment, please fill in the approved investment number and the name of the competent authority.
										</li>
									</ol>
								</li>
								<li>(2) Groups
									<ol>
										<li>Please fill in the tax ID number on the license issued by the competent authority.
										</li>
										<li>If there is no tax ID number on the above-mentioned license, please fill in the name of the registration authority, the registration number and the taxID number of the withholding unit assigned by the tax collection unit.
										</li>
										<li>Accounting firms, law firms, clinics, etc, are reported by reference groups.
										</li>
									</ol>
								</li>
								<li>(3) Taiwanese
									<ol>
										<li>People with national identity cards: Please fill in the ID number and date of birth of the National Identification Card.
										</li>
										<li>Overseas Chinese who have issued a Resident certificate for the Taiwan area from the Immigration Office National Police Agency: Please fill in the residence permit number (if the residence permit has a ID number, please fill in the ID number) and the date of birth.
										</li>
										<li>If you have the certificate of overseas Chinese identity issued by the Overseas Community Affairs Council and the certificate of returning to the country to purchase a house, please fill in the certificate number of the overseas Chinese identity.
										</li>
									</ol>
								</li>
								<li>(4) Foreigners
									<ol>
										<li>Holder of Alien Resident Certificate: Please fill in the residence permit number (if the residence permit has a ID number, please fill in the ID number), the date of birth and the date of issue and the date of expiration.
										</li>
										<li>Foreign passport holders: Please fill in the country and passport number in the (No Alien Resident Certificate) column.
										</li>
										<li>Overseas Chinese who hold a passport of the Republic of China but do not have a national identity card of the Republic of China: Please fill in the country where the issuing place and the passport number in the column (with no alien resident certificate) and add the issuing unit.
										</li>
										<li>Residents of Hong Kong and Macao: Please fill in the country Hong Kong or Macao in the (No Alien  Resident Certificate) column, and the license number is the entry and exit permit or temporary entry notice number.
										</li>
										<li>Mainlanders: Please fill in the country as the mainland area in the (No Alien  Resident Certificate) column, and the license number is the travel permit or residence permit number.
										</li>
									</ol>
								</li>
								</ol>
							</div>
						</li>
	                </ul>
	        	</div>
	      	</div>
		</div>
	</div>
</div>