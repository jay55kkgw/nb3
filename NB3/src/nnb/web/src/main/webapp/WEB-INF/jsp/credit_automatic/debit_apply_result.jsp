<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/checkutil.js"></script>
	
	<script type="text/javascript">
	$(document).ready(function(){
		$("#printbtn").click(function(){
			var i18n = new Object();
			i18n['jspTitle']='<spring:message code= "LB.D0166" />';
			var params = {
				"jspTemplateName":"debit_apply_result_print",
				"jspTitle":i18n['jspTitle'],
				"CMQTIME":"${debit_apply_result.data.CMQTIME}",
				"APPLYFLAG":"${debit_apply_result.data.APPLYFLAG}",
				"TSFACN":"${debit_apply_result.data.TSFACN}",
				"display_PAYFLAG":"${debit_apply_result.data.display_PAYFLAG}",
			}
			openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
		
		});
	});
		
		
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 信用卡     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Credit_Card" /></li>
    <!-- 自動扣款申請/取消     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0166" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<spring:message code="LB.D0166" /><!-- 信用卡款自動扣繳申請/取消 -->
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" action="" method="post">
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
							<h4 style="margin-top:10px;color: red;font-weight:bold;">
								<c:if test="${debit_apply_result.data.APPLYFLAG =='1'}">
									<spring:message code="LB.D0181" /><!-- 申請成功 -->
									</c:if>
									<c:if test="${debit_apply_result.data.APPLYFLAG =='2'}">
									 <spring:message code="LB.D0182" /><!-- 取消成功 -->
									</c:if>
									<c:if test="${debit_apply_result.data.APPLYFLAG =='3'}">
									<spring:message code="LB.Change_successful" /><!-- 變更成功 --> 
								</c:if>
							</h4>
							<div class="ttb-input-block">
								<!--交易時間區塊 -->
								<div class="ttb-input-item row">
									<!--交易時間  -->
									<span class="input-title">
										<label>
											<spring:message code="LB.Trading_time" /><!-- 交易時間 -->
										</label>
									</span>
									<span class="input-block">
										${debit_apply_result.data.CMQTIME}
									</span>
								</div>
								<!-- 扣款帳號區塊 -->
								<c:if test="${debit_apply_result.data.APPLYFLAG != '2'}">
								<div class="ttb-input-item row">
									<!--扣款帳號  -->
									<span class="input-title">
										<label>
											<spring:message code="LB.D0168" /><!-- 扣款帳號 -->
										</label>
									</span>
									<span class="input-block">
										${debit_apply_result.data.TSFACN}
									</span>
								</div>
								</c:if>
								<!-- 申請項目區塊 -->
								<div class="ttb-input-item row">
									<!--申請項目  -->
									<span class="input-title">
										<label>
											<spring:message code="LB.D0169" /><!-- 申請項目 -->
										</label>
									</span>
									<span class="input-block">
										<c:if test="${debit_apply_result.data.APPLYFLAG =='1'}">
										 <spring:message code="LB.Apply" /><!-- 申請 -->
										</c:if>
										<c:if test="${debit_apply_result.data.APPLYFLAG =='2'}">
										 <spring:message code="LB.Cancel" /><!-- 取消 -->
										</c:if>
										<c:if test="${debit_apply_result.data.APPLYFLAG =='3'}">
										 <spring:message code="LB.D0172" /><!-- 變更扣繳方式 -->
										</c:if>
									</span>
								</div>
								<!-- 扣繳方式區塊 -->
								<c:if test="${debit_apply_result.data.APPLYFLAG != '2'}">
								<div class="ttb-input-item row">
									<!--扣繳方式  -->
									<span class="input-title">
										<label>
											<spring:message code="LB.Payment_method" /><!-- 扣繳方式 -->
										</label>
									</span>
									<span class="input-block">
										<c:if test="${debit_apply_result.data.display_PAYFLAG == '1'}">
											<spring:message code="LB.D0174" /><!-- 應繳總額 -->
										</c:if>
										<c:if test="${debit_apply_result.data.display_PAYFLAG == '2'}">
											<spring:message code="LB.D0175" /><!-- 最低應繳額 -->
										</c:if>
									</span>
								</div>
								</c:if>
							</div>
							<!-- 列印 button-->
							<!--列印 -->
							<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
							
						</div>
					</div>
						<!-- 		說明： -->
						<ol class="description-list list-decimal">
						<p><spring:message code="LB.Remind_you"/></p>
							<li>
								<spring:message code="LB.Debit_Apply_P3_D2" />
<!-- 							電子簽章:為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章(載具i-key)拔除並登出系統。 -->
							</li>
						</ol>
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
      
</body>

</html>