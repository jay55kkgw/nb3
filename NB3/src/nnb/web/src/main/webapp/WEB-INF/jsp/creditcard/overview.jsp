<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
<style>
.spbutton {
    display: inline;
    margin: 0 10px 3px 0;
    padding: 0 7px;
    font-size: .875rem;
    border: none;
    border-radius: 12.5px;
    color: #fff;
}
</style>
<script type="text/javascript">
$(document).ready(function () {
	// 將.table變更為datatable
	$('#table1').DataTable({
        scrollX: true,
        sScrollX: "99%",
        bPaginate: false,
        bFilter: false,
        bDestroy: true,
        bSort: false,
        info: false,
        scrollCollapse: true,
        //fixedColumns為固定欄位
        fixedColumns: {
            leftColumns: 1
        },
    });
	$('#table2').DataTable({
        scrollX: true,
        sScrollX: "99%",
        bPaginate: false,
        bFilter: false,
        bDestroy: true,
        bSort: false,
        info: false,
        scrollCollapse: true,
        //fixedColumns為固定欄位
        fixedColumns: {
            leftColumns: 1
        },
    });
	$('#table3').DataTable({
        scrollX: true,
        sScrollX: "99%",
        bPaginate: false,
        bFilter: false,
        bDestroy: true,
        bSort: false,
        info: false,
        scrollCollapse: true,
        //fixedColumns為固定欄位
        fixedColumns: {
            leftColumns: 1
        },
    });
	
	
	$(window).bind('resize', function (){
		$('#table1').DataTable({
	        scrollX: true,
	        sScrollX: "99%",
	        scrollY: '80vh',
	        bPaginate: false,
	        bFilter: false,
	        bDestroy: true,
	        bSort: false,
	        info: false,
	        scrollCollapse: true,
	        //fixedColumns為固定欄位
	        fixedColumns: {
	            leftColumns: 1
	        },
	    });
		$('#table2').DataTable({
	        scrollX: true,
	        sScrollX: "99%",
	        scrollY: '80vh',
	        bPaginate: false,
	        bFilter: false,
	        bDestroy: true,
	        bSort: false,
	        info: false,
	        scrollCollapse: true,
	        //fixedColumns為固定欄位
	        fixedColumns: {
	            leftColumns: 1
	        },
	    });
		$('#table3').DataTable({
	        scrollX: true,
	        sScrollX: "99%",
	        scrollY: '80vh',
	        bPaginate: false,
	        bFilter: false,
	        bDestroy: true,
	        bSort: false,
	        info: false,
	        scrollCollapse: true,
	        //fixedColumns為固定欄位
	        fixedColumns: {
	            leftColumns: 1
	        },
	    });
		
	});
	//友善列印
	$("#printbtn").click(function(){
		var i18n = new Object();
		i18n['jspTitle']='<spring:message code="LB.Credit_Card_Holder_Overview" />'
		var params = {
				"jspTemplateName":"overview_print",
				"jspTitle":i18n['jspTitle'],
				"CMQTIME":"${card_overview.data.CMQTIME}",
				"COUNT":"${card_overview.data.RECNUM}",
				"CRLIMIT":"${card_overview.data.CRLIMITFMT}",
				"CSHLIMIT":"${card_overview.data.CSHLIMITFMT}",
				"ACCBAL":"${card_overview.data.ACCBALFMT}",
				"CSHBAL":"${card_overview.data.CSHBALFMT}",
				"INT_RATE":"${card_overview.data.INT_RATEFMT}",
				"PAYMT_ACCT":"${card_overview.data.PAYMT_ACCT_COVER}",
				"DBCODE":"${card_overview.data.DBCODE}",
				"STOP_DAY":"${card_overview.data.STOP_DAY}",
				"CURR_BAL":"${card_overview.data.CURR_BALFMT}",
				"TOTL_DUE":"${card_overview.data.TOTL_DUEFMT}",
				"STMT_DAY":"${card_overview.data.STMT_DAY}",
				"STOP_DAY":"${card_overview.data.STOP_DAY}"
		}
		openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);

	});
	
});


	function goBILL( fgperiod, cardtype ){
		initBlockUI();
		$("#FGPERIOD").val(fgperiod);
		$("#CARDTYPE").val(cardtype);
		$('#QUERYTYPE').val("BHWSHtml");
		if($('#CARDTYPE').val()=='999'){
			$('#BILL_NO').val('1');
		}
		if($('#CARDTYPE').val()=='700'){
			$('#BILL_NO').val('5');
		}
		if($('#CARDTYPE').val()=='ALL'){
			$('#BILL_NO').val('3');
		}
		
		$("#formId").attr("action", "${__ctx}"+"/CREDIT/INQUIRY/card_history_online");
		$("#formId").submit();
	}
	

</script>

</head>
<body>
   	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

	<!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 信用卡     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Credit_Card" /></li>
    <!-- 帳務查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Inquiry_Service" /></li>
    <!-- 信用卡持卡總覽     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Credit_Card_Holder_Overview" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
	

	<main class="col-12">
		<section id="main-content" class="container"><!-- 主頁內容  -->
		
			<h2><spring:message code="LB.Credit_Card_Holder_Overview" /></h2>
			
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="">
				<div class="main-content-block row">
					<input id="SYSTIME" name="SYSTIME" type="hidden" value="" />
					<input id="FGPERIOD" name="FGPERIOD" type="hidden" value="" />
					<input id="CARDTYPE" name="CARDTYPE" type="hidden" value="" />
					<input id="BILL_NO" name="BILL_NO" type="hidden" value="" />
					<input id="QUERYTYPE" name="QUERYTYPE" type="hidden" value="" />
					<input type="hidden" name="FUNCTION" value="N810"/>
					<div class="col-12">
						<div>
							<ul class="ttb-result-list">
								<li>
									<!-- 查詢時間  -->
									<h3>
										<spring:message code="LB.Inquiry_time" />
									</h3>
									<p>	
										${card_overview.data.CMQTIME}
									</p>
								</li>
								<li>
									<h3>
										<spring:message code="LB.Total_records" />
									</h3>
									<p>
										${card_overview.data.RECNUM}&nbsp;<spring:message code="LB.Rows" />
									</p>
								</li>
							</ul>
									
									<table id="table1" class="stripe table-striped ttb-table dtable" data-toggle-column="first">
										<thead>
										<tr>
							 				<th data-title='<spring:message code="LB.Types_of_Credit_Card"/>'><spring:message code="LB.Types_of_Credit_Card" /></th><!-- 卡別 -->
											<th data-title='<spring:message code="LB.Credit_card_number"/>' > <spring:message code="LB.Credit_card_number" /></th><!-- 卡號 -->
											<th data-title='<spring:message code="LB.Card_holder"/>'> <spring:message code="LB.Card_holder" /> </th><!-- 持卡人 -->
										</tr>
										</thead>
										<tbody>
											<c:forEach var="dataTable" items="${card_overview.data.REC}">
											<tr>
												<td  class="text-center">
												${dataTable.TYPENAME}
												</td>
												<%-- ${dataTable._CARDNUM} 卡號 --%>
												<td  class="text-center">
												${dataTable.CCNCOVER}
												<c:if test="${dataTable.PT_FLG == '0'}">（<!-- 附卡＊ --><spring:message code="LB.Attached_card" /> ）</c:if>
												</td>
												<td  class="text-center">
						                    	${dataTable.CR_NAME}
						                    	</td>
						                    	</tr>
										</c:forEach>
										
										</tbody>
									
									
									</table>
							<br>	
							<table id="table2" class="stripe table-striped ttb-table dtable" data-toggle-column="first">
								<thead>
								<tr>
									<th  data-title='<spring:message code="LB.Credit_line"/>'>
										<!-- 信用額度  -->
										<spring:message code="LB.Credit_line" />
									</th>
									<th data-title='<spring:message code="LB.Cash_advanced_line"/>' >
										<!-- 預借現金額度 -->
										<spring:message code="LB.Cash_advanced_line" />
									</th>
									<th data-title='<spring:message code="LB.Current_balance"/>'>
										<!-- 目前已使用額度  -->
										<spring:message code="LB.Current_balance" />
									</th>
									<th data-title='<spring:message code="LB.Cash_advanceUsed"/>' >
										<!-- 已動用預借現金  -->
										<spring:message code="LB.Cash_advanceUsed" />
									</th>
									<th data-title='<spring:message code="LB.Current_revolving_interest"/>' >
										<!-- 本期循環信用利率  -->
										<spring:message code="LB.Current_revolving_interest" />
									</th>
									<th data-title='<spring:message code="LB.Auto-pay_account_number"/>'>
										<!-- 自動扣繳帳號  -->
										<spring:message code="LB.Auto-pay_account_number" />
									</th>
									<th data-title='<spring:message code="LB.Payment_method"/>' >
										<!-- 扣繳方式  -->
										<spring:message code="LB.Payment_method" />
									</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td class="text-right">
										<!-- 信用額度  -->
										${card_overview.data.CRLIMITFMT}
									</td>
									<td class="text-right">
										<!-- 預借現金額度 -->
										${card_overview.data.CSHLIMITFMT}
									</td>
									<td class="text-right"> 
										<!-- 目前已使用額度  -->
										${card_overview.data.ACCBALFMT}
									</td>
									<td class="text-right">
										<!-- 已動用預借現金  -->
										${card_overview.data.CSHBALFMT}
									</td>
									<td class="text-right"> 
										<!-- 本期循環信用利率  -->
										${card_overview.data.INT_RATEFMT} %
									</td>
									<td class="text-center">
										<!-- 自動扣繳帳號  -->
										${card_overview.data.PAYMT_ACCT_COVER}
									</td>
									<td  class="text-center">
										<!-- 扣繳方式  -->
										${card_overview.data.DBCODE}
									</td>
								</tr>
								</tbody>
							</table>
							
							<br />
							
							<div class="text-left">
							<spring:message code="LB.Overview_P1_note" />
							<!-- 註：「目前已使用額度」不含已刷卡授權通過，但商店尚未請款之交易金額；如須查詢「可用餘額」請洽本行客服人員 -->
							 </div>

							<ul class="ttb-result-list">
								<li>
									<!-- 查詢時間  -->
									<h3>
										<spring:message code="LB.Latest_statement" />
									</h3>
									<p>	
										<spring:message code="LB.Latest_statement" />
									</p>
								</li>
							</ul>
							<table id="table3" class="stripe table-striped ttb-table dtable" data-toggle-column="first">
								<thead>
<%-- 								<tr> --%>
<%-- 									<th class="text-left" colspan="6"><spring:message code="LB.Latest_statement" />(${card_overview.data.STOP_DAY})</th> --%>
<%-- 								</tr> --%>
								<tr>
									<!-- 應繳總金額  -->
									<th data-title='<spring:message code="LB.The_total_amount_payable"/>' ><spring:message code="LB.The_total_amount_payable" /></th>
									<!-- 本期最低金額  -->
									<th data-title='<spring:message code="LB.Minimum_payment"/>'><spring:message code="LB.Minimum_payment" /></th>
									<!-- 結帳日  -->
									<th data-title='<spring:message code="LB.Statement_date"/>' ><spring:message code="LB.Statement_date" /></th>
									<!-- 繳款截止日  -->
									<th data-title='<spring:message code="LB.Due_date"/>'><spring:message code="LB.Due_date" /></th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td class="text-right" >
										<!-- 應繳總金額  -->
										${card_overview.data.CURR_BALFMT}
										<!-- 繳納本行信用卡款連結按鈕  -->
										<input type="button" class="spbutton btn-flat-orange" onclick="fstop.getPage('${pageContext.request.contextPath}'+'/CREDIT/PAY/payment','', '')" value="<spring:message code="LB.Pay" />" />
									</td>
									<td class="text-right">
										<!-- 本期最低金額  -->
										${card_overview.data.TOTL_DUEFMT}
									</td>
									<td class="text-center">
										<!-- 結帳日  -->
										${card_overview.data.STMT_DAY}
									</td>
									<td class="text-center">
										<!-- 繳款截止日  -->
										${card_overview.data.STOP_DAY}
									</td>
								</tr>
								</tbody>
							</table>
							
							<c:if test="${card_overview.data.count_result == 'a'}">
								<ul class="ttb-result-list full-list">
										<li class=" full-list" style="margin-bottom: 10px;">
										<h3><spring:message code="LB.Bank_card" />&nbsp;<spring:message code="LB.Statement" /></h3><!-- 一般卡 --><br>
										<input name="BILL1" type="button" class="spbutton btn-flat-orange" value="<spring:message code="LB.This_period" />" onClick="goBILL('CMCURMON','999');" />
										<input name="BILL2" type="button" class="spbutton btn-flat-orange" value="<spring:message code="LB.Previous_period" />" onClick="goBILL('CMLASTMON','999');" />
										<input name="BILL3" type="button" class="spbutton btn-flat-orange" value="<spring:message code="LB.Last_period" />" onClick="goBILL('CMLAST2MON','999');" />
										</li>
										<li style="margin-bottom: 10px;">
										<h3><spring:message code="LB.Business_card" />&nbsp;<spring:message code="LB.Statement" /></h3><!-- 商務卡 --><br>
										<input name="BILL1" type="button" class="spbutton btn-flat-orange" value="<spring:message code="LB.This_period" />" onClick="goBILL('CMCURMON','ALL');" />
										<input name="BILL2" type="button" class="spbutton btn-flat-orange" value="<spring:message code="LB.Previous_period" />" onClick="goBILL('CMLASTMON','ALL');" />
										<input name="BILL3" type="button" class="spbutton btn-flat-orange" value="<spring:message code="LB.Last_period" />" onClick="goBILL('CMLAST2MON','ALL');" />
										</li>
								</ul >
							</c:if>
							<c:if test="${card_overview.data.count_result == 'b'}">
								<ul class="ttb-result-list full-list">
										<li class=" full-list" style="margin-bottom: 10px;">
										<h3><spring:message code="LB.Bank_card" />&nbsp;<spring:message code="LB.Statement" /></h3><!-- 一般卡 -->
										<input name="BILL1" type="button" class="spbutton btn-flat-orange" value="<spring:message code="LB.This_period" />" onClick="goBILL('CMCURMON','999');" />
										<input name="BILL2" type="button" class="spbutton btn-flat-orange" value="<spring:message code="LB.Previous_period" />" onClick="goBILL('CMLASTMON','999');" />
										<input name="BILL3" type="button" class="spbutton btn-flat-orange" value="<spring:message code="LB.Last_period" />" onClick="goBILL('CMLAST2MON','999');" />
										</li>
								</ul >
							</c:if>
							<c:if test="${card_overview.data.count_result == 'c'}">
								<ul class="ttb-result-list full-list">
										<li class=" full-list" style="margin-bottom: 10px;">
										<h3><spring:message code="LB.Business_card" />&nbsp;<spring:message code="LB.Statement" /></h3><!-- 商務卡 --><br>
										<input name="BILL1" type="button" class="spbutton btn-flat-orange" value="<spring:message code="LB.This_period" />" onClick="goBILL('CMCURMON','ALL');" />
										<input name="BILL2" type="button" class="spbutton btn-flat-orange" value="<spring:message code="LB.Previous_period" />" onClick="goBILL('CMLASTMON','ALL');" />
										<input name="BILL3" type="button" class="spbutton btn-flat-orange" value="<spring:message code="LB.Last_period" />" onClick="goBILL('CMLAST2MON','ALL');" />
										</li>
								</ul >
							</c:if>
						</div>

						<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
					</div>
				</div>
				<ol class="description-list list-decimal">
								<p><spring:message code="LB.Description_of_page" /></p><!-- 說明 -->
									<li>
									<spring:message code="LB.Overview_P1_D1" />
									<!-- 若附卡持卡人查詢時，應繳總額、最低應繳金額、繳款截止日、扣帳帳號、本期已繳金額等欄位均顯示為 0。 -->
									</li>
									<li>
									<spring:message code="LB.Overview_P1_D2" />
									<!-- 各正、附卡帳單應繳總額係正卡身分證字號合併計算。 -->
									</li>
									<li>
									<spring:message code="LB.Overview_P1_D3" />
									<!-- 為提升服務品質，本行自100年5月起信用卡帳單將由現行各卡帳單改為「依正卡持卡人身分證號碼歸戶方式之整合式帳單」，故若您原約定之轉入帳號為本行信用卡卡號者，將於100年5月1日失效，造成不便，敬請見諒！ -->
									</li>
								</ol>
			</form>
		</section>
	</main><!-- main-content END --> 
	</div><!-- content row END -->

	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>