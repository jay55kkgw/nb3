<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ include file="../__import_js.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
	$(document).ready(function() {
		window.print();
	});
</script>
</head>

<body class="bodymargin watermark" style="-webkit-print-color-adjust:exact">
	<br />
	<br />
	<div style="text-align: center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br />
	<br />
	<div style="text-align: center">
		<font style="font-weight: bold; font-size: 1.2em">${jspTitle}</font>
	</div>
	<br />
	<div style="text-align: center">
		<spring:message code= "LB.D0183" />
	</div>
	<br />
	<table class="print">
		<!-- 交易時間 -->
		<tr>
			<td style="width: 8em">
				<!--         交易時間 --> <spring:message code= "LB.System_time" />
			</td>
			<td>${CMQTIME}</td>
		</tr>
		<!-- 申請項目 -->
		<tr>
			<td class="ColorCell"><spring:message code= "LB.D0169" /></td>
			<td><c:if test="${TYPE4 =='4'}">
				<spring:message code= "LB.D0280" /><br>
				</c:if> 
			<c:if test="${TYPE6 =='6'}">
				<spring:message code= "LB.D0274" /><br>
			</c:if> 
			<c:if test="${TYPE5 =='5'}">
				<spring:message code= "LB.D0275" /><br>
			</c:if>
			</td>
		</tr>
		<!-- 郵寄方式 -->
		<tr>
			<td class="ColorCell">
				<spring:message code= "LB.X0886" />
			</td>
			<td>
				<spring:message code= "LB.X0887" />
			</td>
		</tr>
	</table>
	<br>
	<br>
</body>

</html>