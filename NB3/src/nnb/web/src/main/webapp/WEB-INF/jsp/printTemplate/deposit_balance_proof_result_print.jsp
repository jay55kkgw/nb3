<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
<br/><br/>
<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
<br/><br/><br/>
<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
<br/><br/><br/>
<table class="print">
	<!-- 								申請帳號 -->
	<tr>
		<td style="text-align:center"><spring:message code="LB.D0487" /></td>
		<td>${ACN}</td>
	</tr>
	<!--								身份證/營利事業統一編號     -->
	<tr>
		<td style="text-align:center"><spring:message code="LB.D0488" />/<spring:message code="LB.D0489" /></td>
		<td>${CUSIDN}</td>
	</tr>
	<!-- 					       		申請份數   -->	
	<tr>
		<td style="text-align:center"><spring:message code="LB.D0490" /></td>
		<td>${COPY}</td>
	</tr>
	<!-- 					           	 申請用途	-->	
	<tr>
		<td style="text-align:center"><spring:message code="LB.D0491" /></td>
		<td>${PURPOSE1}</td>
	</tr>
	<!-- 					            證明日期     -->
	<tr>
		<td style="text-align:center"><spring:message code="LB.D0492" /></td>
		<td>${DATE1}</td>
	</tr>
	<!-- 					            證明書種類     -->
	<tr>
		<td style="text-align:center"><spring:message code="LB.D0495" /></td>
		<td>
		<c:if test="${INQ_TYPE == '1'}">
		<spring:message code="LB.D0496" />			       			
    	</c:if>
    	<c:if test="${INQ_TYPE == '2'}">
		<spring:message code="LB.D0497" />			       			
    	</c:if>
    	</td>
	</tr>
	<!-- 					           幣別    -->	
	<tr>
		<td style="text-align:center"><spring:message code="LB.Currency" /></td>
		<td>${CRY}</td>
	</tr>
	<c:if test="${INQ_TYPE == '2'}">
	<!-- 					           證明金額     -->		
	<tr>
		<td style="text-align:center"><spring:message code="LB.D0499" /></td>
		<td>${AMT}</td>
	</tr>
	</c:if>
	<!-- 					          提供文件版本                    -->
	<tr>
		<td style="text-align:center"><spring:message code="LB.D0500" /></td>
		<td>
		<c:if test="${VER == '1'}">
		<spring:message code="LB.D0394" />						       			
     	</c:if>
     	<c:if test="${VER == '2'}">
		<spring:message code="LB.D0396" />					       			
     	</c:if>
     	</td>
	</tr>
	<!-- 					          英文戶名                    -->
	<c:if test="${VER == '2'}">
	<tr>
		<td style="text-align:center"><spring:message code="LB.X0363" /></td>
		<td>${ENGNAME}</td>
	</tr>
	</c:if>
	<!-- 					         領取方式                    -->
	<tr>
		<td style="text-align:center"><spring:message code="LB.D0503" /></td>
		<td>${DATA}</td>
	</tr>
	<!-- 					         手續費扣帳帳號                    -->
	<tr>
		<td style="text-align:center"><spring:message code="LB.D0506" /></td>
		<td>${FEEACN}</td>
	</tr>
	<!-- 					         手續費                    -->
	<tr>
		<td style="text-align:center"><spring:message code="LB.D0507" /></td>
		<td><spring:message code="LB.D0508" />${FEAMT}<spring:message code="LB.Dollar_1" /></td>
	</tr>
</table>
<br/><br/>
<!-- 	<div class="text-left"> -->
<!-- 		說明: -->
<!-- 		<ol class="list-decimal text-left"> -->
<!-- 			<li>晶片金融卡:為保護您的交易安全，結束交易或離開電腦時，請務必將晶片金融卡抽離讀卡機並登出系統。</li> -->
<!-- 			<li>電子簽章:為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章(載具i-key)拔除並登出系統。</li> -->
<!-- 		</ol> -->
<!-- 	</div>	 -->
</body>
</html>