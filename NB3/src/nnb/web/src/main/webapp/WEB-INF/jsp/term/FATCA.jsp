<!-- <html> -->

<!-- <head> -->
<!-- <meta http-equiv=Content-Type content="text/html; charset=utf-8"> -->
<!-- <meta name=Generator content="Microsoft Word 11 (filtered)"> -->
<!-- </head> -->
<!-- <body> -->
<%-- 	<object data="${__ctx}/public/FATCA.pdf" type="application/pdf"  style='width:100%;height: 100%;' > --%>
<!-- 		<center> -->
<!-- 			<p> -->
<!-- 				It appears you don't have Adobe Reader or PDF support in this Web browser. -->
<%-- 				<a href="${__ctx}/public/FATCA.pdf">Click here to download the PDF</a> --%>
<!-- 			</p> -->
<!-- 		</center> -->
<%-- 		<embed scr="${__ctx}/public/FATCA.pdf" type="application/pdf" /> --%>
<!-- 	</object> -->
<!-- </body> -->
<!-- </html> -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<body>
	<div style="margin: 2rem;">
		<p class="terms-text-bold terms-text-center">聲明內容</p>
		<p>本人聲明本自我聲明書暨個人資料同意書之內容均屬真實正確及完整。倘爾後有情事變更致使本自我聲明書暨個人資料同意書之內容已不正確，本人承諾於變更之日起30天內通知臺灣中小企業銀行，並承諾提供更新後之自我聲明書暨個人資料同意書予臺灣中小企業銀行。本自我聲明書暨個人資料同意書除美國海外帳戶稅收遵從法案（Foreign Account Tax Compliance Act，以下簡稱「FATCA法案」）等相關法令外，應以臺灣之法令為準據法。倘開戶申請書之內容與本自我聲明書暨個人資料同意書有衝突時，以本自我聲明書暨個人資料同意書為準。</p>
		<p>I hereby certify that all statements made in this self-certification and personal information consent form are true, correct and complete. I undertake to notify Taiwan Business Bank, (the “Bank”) promptly of any change in circumstances which causes the information contained herein to become incorrect and to provide the Bank with an updated self-certification and personal information consent form within 30 days of such change in circumstances. In addition to the U.S. Foreign Account Tax Compliance Act (hereinafter referred to as “FATCA”) and its related laws and regulations, the governing law of this self-certification and personal information consent form shall be the laws of Taiwan. In the event of any discrepancy between the account opening form and this self-certification and personal information consent form, this self-certification and personal information consent form shall prevail.</p>
		<br/>
		<p>另本人於　貴公司開立帳戶並進行交易，為符合個人資料保護法下個人資料之合理使用，並配合　貴公司遵循FATCA法案、駐美國台北經濟文化代表處與美國在台協會合作促進外國帳戶稅收遵從法執行協定等相關規定，本人確認已收受並充分瞭解 貴公司所提供之遵循FATCA法案蒐集、處理及利用個人資料告知事項（下簡稱「告知事項」）之全部內容，並同意 貴公司依據告知事項所載內容，對本人相關個人資料為蒐集、處理及利用：</p>
		<p>Whereas I intend to establish an account and to proceed transactions with Taiwan Business Bank, (the “Bank”), and whereas it is regulated to comply with the proper use of personal information stipulated by the Personal Information Protection Act, and it is necessary to cooperate with the Bank to comply with FATCA, Agreement between the American institute in Taiwan and Taipei Economic And Cultural Representative Office In The United States and the related regulations.
			In witness thereof, I hereby confirm that I have duly received and understood all the content of the Notice for the Collection, Processing and Use of Personal Information for FATCA Compliance (the ”Notice”) as provided by the Bank, and I agree that the Bank may collect, process and use the personal information of the Customer in accordance with the Notice.
		</p>
	</div>
</body>

</html>