<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
<br/><br/>
<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
<br/><br/><br/>
<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
<br/><br/><br/>
	<table class="print">
		<tbody>
			<tr>
				<td style="text-align:center"><spring:message code="LB.System_time" /></td>
				<td>${CMQTIME}</td>
			</tr>
			<tr>
				<td style="text-align:center"><spring:message code="LB.D0191" /></td>
				<td>
					<c:if test="${!MSG.equals('')}">
						<spring:message code="${MSG}" />
					</c:if>
				</td>
			</tr>
			<c:if test="${CCY == ''}">
			<tr>			
				<td style="text-align:center"><spring:message code="LB.D0621" /></td>
				<td>${CUSIDN}</td>
			</tr>
			<tr>			
				<td style="text-align:center"><spring:message code="LB.X0269" /></td>
				<td>${BNKCOD}</td>
			</tr>
			</c:if>
			<tr>			
				<td style="text-align:center"><spring:message code="LB.Account" /></td>
				<td>${ACN}</td>
			</tr>
			<c:if test="${CCY != ''}">
			<tr>			
				<td style="text-align:center"><spring:message code="LB.Currency" /></td>
				<td>${CCY}</td>
			</tr>
			</c:if>
		</tbody>
	</table>
<br/><br/>
		<div class="text-left">
			<spring:message code="LB.Description_of_page" /> 
			<ol class="list-decimal text-left">
				<li><font color="red"><B><spring:message code="LB.Predesignated_Account_Cancellation_P2_D1" /></B></font></li>
				<li><font color="red"><B><spring:message code="LB.Predesignated_Account_Cancellation_P2_D2" /></B></font></li>
				<li><spring:message code="LB.Predesignated_Account_Cancellation_P3_D3" /></li>
			</ol>
		</div>
</body>
</html>