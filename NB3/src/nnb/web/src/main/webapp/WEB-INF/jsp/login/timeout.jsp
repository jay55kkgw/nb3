<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	
	<script type="text/javascript">
		$(document).ready(function(){
			// 回到登入頁按鈕
			$("#CMSUBMIT").click( function(e) {
				$("#formId").submit();
			});
		});
	</script>
</head>
<body>
    <!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 登出網銀     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X1920" /></li>
		</ol>
	</nav>

	
	<!-- 功能清單及登入資訊 -->
	<div class="content row">
		<!-- 快速選單及主頁內容 -->
		<main class="col-12" style="margin-top: 60px;"> 
			<!-- 主頁內容  -->
			<section id="main-content" class="container m-auto" style="max-width: 720px">
				<form method="post" id="formId" name="formId" action="${__ctx}/login">
		            <div class="main-content-block row">
						<div class="col-12 logout-block">
							<p class="logout-title">
								<spring:message code="LB.X1548" />
							</p>
							<p class="logout-content">
								<spring:message code="LB.X1705" /><spring:message code="LB.X1706" />
							</p>
							<p class="logout-content">
								<spring:message code="LB.X1707" /><spring:message code="LB.X1708" /><spring:message code="LB.X1918" />
							</p>
							<!-- 確認 -->
							<input type="button" class="ttb-button btn-flat-orange" id="CMSUBMIT" value="<spring:message code="LB.X1919" />"/>
						</div>
					</div>
				</form>
	        </section>    
		</main>
	</div>

	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>