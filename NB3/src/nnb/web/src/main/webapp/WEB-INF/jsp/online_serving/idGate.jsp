<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<style type="text/css">
		.fooicon-plus:before{
			position: absolute;
	    	left: -10px;
		}
		.spbutton {
		   margin: 0 10px 3px 0;
		    padding: 0 7px;
		    font-size: .875rem;
		    border: none;
		    border-radius: 12.5px;
		    color: #fff;
		}
	</style>
<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
		setTimeout("initDataTable()",100);
	});
	
	// 畫面初始化
	function init() {
		//initFootable();
// 		$('#but_Agree').prop('checked', false);
		// 確認鍵 click
		submit();
// 		switchRadioEvent();
	
		var s = '${idGateData}'.replace(/[\|\[|\]]/g, "");
		if(s.length == 0){
			$("#div1").hide();
			$("#div2").show();
		}
		else{
			$("#div2").hide();
			$("#div1").show();
		}
	}
	
	// 確認鍵 Click
	function submit() {
		$("#CMSUBMIT").click( function(e) {
			console.log("submit~~");
			// 遮罩
         	initBlockUI();
            $("#formId").submit();
		});
	}
	function buttonEvent(){
		if($('input[type=checkbox][name=MB_Agree]').is(':checked')){
			$('#CMSUBMIT').removeAttr("disabled");
			$('#CMSUBMIT').addClass('btn-flat-orange');
			$('#CMSUBMIT').removeClass('btn-flat-gray');
		}
		else{
			$('#CMSUBMIT').attr("disabled",true);
			$('#CMSUBMIT').removeClass('btn-flat-orange');
			$('#CMSUBMIT').addClass('btn-flat-gray');
		}
	}
	
// 	function switchRadioEvent(){
// 		$('input[type=radio][name^=MB_Switch-]').change(function() {
// 			console.log("hi>>"+this.value);
// 			console.log("id>>"+this.id);
// 			console.log("id>>"+this.name);
// 			var rdata = { pk:this.name , action:this.value};
// 			var options = null;
// 			var pk = this.name.replace("MB_Switch-","");
// 			options = {action:this.value ,fontid:'#font_'+pk};
// 			if('2' == this.value){
// 				options =$.extend( {trid:'#tr_'+pk} , options);
// 			}
// 			uri = '${__ctx}'+"/ONLINE/SERVING/idGateChange_aj";
// 			setTimeout("initBlockUI()", 10);
// 			fstop.getServerDataEx(uri,rdata,false,showMessage ,null , options );
// 		});
// 	}

// function showMessage(data , options){
// 		setTimeout("unBlockUI(initBlockId)", 500);
// 		if(null == data || 'undefined' == data ){
// 			alert('系統異常');
// 			return;
// 		}
		
// 		if( data.result){
// 			if(null != options && '2' == options.action){
// 				console.log("trid>>"+options.trid)
// 				$(options.trid).remove();
// 			}
// 			if(null != options && '1' == options.action){
// 				console.log("fontid>>"+options.fontid)
// 				$(options.fontid).css('color','red').text('已停用');
// 			}
// 			if(null != options && '0' == options.action){
// 				console.log("fontid>>"+options.fontid)
// 				$(options.fontid).css('color','green').text('已啟用');
// 			}
// 		}
		
// 	}



	function MBCancelEvent(idgateId,devId){
		console.log("pk>>"+pk);
		var pk = idgateId+"#"+devId;
		var vpk = idgateId+"-"+devId;
		var rdata = { pk:pk , action:'2'};
		var options = null;
// 		var pk = this.name.replace("MB_Switch-","");
		options = {action:'2' ,fontid:'#font_'+vpk};
	    options =$.extend( {trid:'#tr_'+vpk} , options);
	    console.log("MBCancelEvent.options>>"+options);
		uri = '${__ctx}'+"/ONLINE/SERVING/idGateChange_aj";
		setTimeout("initBlockUI()", 10);
		fstop.getServerDataEx(uri,rdata,false,showMessage ,null , options );
	}
	
	function showMessage(data , options){
		setTimeout("unBlockUI(initBlockId)", 500);
		if(null == data || 'undefined' == data ){
			alert('系統異常');
			return;
		}
		 console.log("options.action>>"+options.action);
		 console.log("options.trid>>"+options.trid);
		 console.log("data.result>>"+data.result);
		if( data.result){
			if(null != options && '2' == options.action){
				console.log("trid>>"+options.trid)
				$(options.trid).remove();
			}
// 			if(null != options && '1' == options.action){
// 				console.log("fontid>>"+options.fontid)
// 				$(options.fontid).css('color','red').text('已停用');
// 			}
// 			if(null != options && '0' == options.action){
// 				console.log("fontid>>"+options.fontid)
// 				$(options.fontid).css('color','green').text('已啟用');
// 			}
		}
		
	}
	
	//開啟裝置名稱修改彈跳視窗
	function editName(GateID, DID, DNAME){
		$("#OLDNAME").text(DNAME);
		$("#editGateID").val(GateID);
		$("#editDID").val(DID);
		$("#NEWNAME").val("");
		$("#main-content-edit").show();
	}
	
	//修改裝置名稱
	function editEvent(){
		var idgateId = $("#editGateID").val();
		var devId = $("#editDID").val();
		var name = $("#NEWNAME").val();
		console.log("pk>>"+pk);
		if(!name || name === ''){
			alert("新裝置名稱不可為空");
			return;
		}
		var pk = idgateId+"#"+devId;
		var vpk = idgateId+"-"+devId;
		var rdata = { pk:pk , action:'3', NEWNAME:name};
		uri = '${__ctx}'+"/ONLINE/SERVING/idGateChange_aj";
		setTimeout("initBlockUI()", 10);
		$('#main-content-edit').hide();
		$("#NEWNAME").val("");
		fstop.getServerDataEx(uri,rdata,false,showEdit);
	}
	
	function showEdit(data){
		console.log("data.json: " + JSON.stringify(data));
		setTimeout("unBlockUI(initBlockId)", 500);
		if(null == data || 'undefined' == data ){
			alert('系統異常');
			return;
		}
		 console.log("data.result>>"+data.result);
		if( data.result){
			window.location.reload();
		}
		else{
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Transaction_code' />: " + data.msgCode , "\n" , "<spring:message code='LB.Transaction_message' />: " + data.message], 
					'<spring:message code= "LB.Quit" />', 
					null
			);
		}
	}
	
	//註銷
	function test(GID, DID, DNAME){
		console.log("#tr_"+GID+DID);
		var tt = $("#tr_" + GID + "-" + DID).find('td').eq(1).text().trim();
		$("#DEVICENAME").val(DNAME);
		$("#IDGATEID").val(GID);
		$("#DEVICEID").val(DID);
		$("#formId").attr("action","${__ctx}/ONLINE/SERVING/idgate_cancel");
		$("#formId").submit();
	}
	
</script>
</head>
	<body>
		<!-- header -->
		<header>
			<%@ include file="../index/header.jsp"%>
		</header>
	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 裝置查詢服務     -->
			<li class="ttb-breadcrumb-item active" aria-current="page">裝置查詢服務</li>
		</ol>
	</nav>

		<!-- menu、登出窗格 -->
		<div class="content row">
			<!-- 快速選單內容 -->
			<%@ include file="../index/menu.jsp"%>
			<main class="col-12">
			<section id="main-content" class="container">
			
				<div class="pupup-block" id="main-content-edit" style="display:none">
					<div class="card-block shadow-box terms-pup-blcok">
						<h2 class="ttb-pup-h2">裝置名稱修改</h2>
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<div class="card-detail-block">
									<div class="card-center-block d-flex">
										<!-- 舊裝置名稱 -->
										<div class="ttb-input-item row">
											<span class="input-title">
												<label><h4>舊裝置名稱</h4></label>
											</span> 
											<span class="input-block">
												<div class="ttb-input">
													<span id="OLDNAME" name="OLDNAME"></span>
												</div>
											</span>
										</div>
										<!-- 新裝置名稱 -->
										<div class="ttb-input-item row">
											<span class="input-title">
												<label><h4>新裝置名稱</h4></label>
											</span> 
											<span class="input-block">
												<div class="ttb-input">
													<input type="text" id="NEWNAME" name="NEWNAME" class="text-input" size="8" maxlength="30" value="" autocomplete="off"> 
												</div>
											</span>
										</div>
									</div>
								</div>
							</div>
							<input type="BUTTON" class="ttb-button btn-flat-gray" value="返回" name="" onclick="$('#main-content-edit').hide();">
							<input type="BUTTON" class="ttb-button btn-flat-orange" value="確定" name="" onclick="editEvent()">
							<input type="hidden" name="editGateID" id="editGateID" value="">
							<input type="hidden" name="editDID" id="editDID" value="">
						</div>
					</div>
				</div>
			
				<!-- 主頁內容  -->
				<h2>裝置查詢服務</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="${__ctx}/ONLINE/SERVING/idgate_confirm">
					
					<input type="hidden" name="DEVICENAME" id="DEVICENAME" value="">
					<input type="hidden" name="IDGATEID" id="IDGATEID" value="">
					<input type="hidden" name="DEVICEID" id="DEVICEID" value="">
					<div class="main-content-block row">
						<div class="col-12" id="div1" style="display: none;">
							<!-- 線上申請IDGATE服務 -->
							<ul class="ttb-result-list"></ul>
							<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
								<thead>
									<tr>
										<!-- 目前設定狀態 -->
										<th data-title="<spring:message code="LB.D0319"/>"><spring:message code="LB.D0319"/></th>
										<!-- 裝置名稱-->
										<th data-title="裝置名稱"></th>
										<!-- 裝置型號-->
										<th data-title="裝置型號"></th>
										<!-- 變更設定 -->
										<th data-title="<spring:message code="LB.D0321"/>"><spring:message code="LB.D0321"/></th>
									</tr>
								</thead>
								<tbody>
								<c:forEach var="dataList" items="${ idGateData }" varStatus="data">
									<tr id="tr_${dataList.pks.IDGATEID}-${dataList.DEVICEID}">
<%-- 									<tr id="tr_${dataList.pks.CUSIDN}-${dataList.pks.IDGATEID}-${dataList.DEVICEID}"> --%>
										<!-- 狀態-->
										<td class="text-center">
											<c:choose>
												<c:when test="${ '0' == dataList.DEVICESTATUS}">
													<font id="font_${dataList.pks.IDGATEID}-${dataList.DEVICEID}" style="color:green; font-weight:bold;">已啟用</font>
												</c:when>
												<c:when test="${ '1' == dataList.DEVICESTATUS}">
													<font id="font_${dataList.pks.IDGATEID}-${dataList.DEVICEID}"style="color:red; font-weight:bold;">已停用</font>
												</c:when>
												<c:when test="${ '2' == dataList.DEVICESTATUS}">
													<font id="font_${dataList.pks.IDGATEID}-${dataList.DEVICEID}"style="color:red; font-weight:bold;">已鎖定</font>
												</c:when>
												<c:when test="${ '9' == dataList.DEVICESTATUS}">
													<font id="font_${dataList.pks.IDGATEID}-${dataList.DEVICEID}"style="color:red; font-weight:bold;">已註銷</font>
												</c:when>
												<c:otherwise>
													<font id="font_${dataList.pks.IDGATEID}-${dataList.DEVICEID}"style="color:red; font-weight:bold;">狀態不明</font>
												</c:otherwise>
											</c:choose>
										</td>
										
										<!--裝置名稱 -->
										<td class="text-center">
											${dataList.DEVICENAME}
											<img src="${__ctx}/img/icon-edit.svg" onclick="editName('${dataList.pks.IDGATEID}','${dataList.DEVICEID}','${dataList.DEVICENAME}')"/>
										</td>
										<!--裝置型號 -->
										<td class="text-center">
											${dataList.DEVICEBRAND}
										</td>

										<td class="text-center">
<%-- 										<c:choose> --%>
<%-- 											<c:when test="${ '0' == dataList.DEVICESTATUS}"> --%>
<%-- 												<label class="radio-block"><spring:message code="LB.D0324"/> --%>
<%-- 													<input type="radio" id="MB_Switch_On" checked="checked" name="MB_Switch-${dataList.pks.CUSIDN}-${dataList.pks.IDGATEID}-${dataList.DEVICEID}" value="0" 啟用> --%>
<!-- 													<span class="ttb-radio"></span> -->
<!-- 												</label> -->
<%-- 	      										<label class="radio-block"><spring:message code="LB.D0325"/> --%>
<%-- 	      											<input type="radio" id="MB_Switch_Off" name="MB_Switch-${dataList.pks.CUSIDN}-${dataList.pks.IDGATEID}-${dataList.DEVICEID}" value="1" 停用> --%>
<!-- 	      											<span class="ttb-radio"></span> -->
<!-- 	      										</label> -->
<%-- 	      									</c:when>	 --%>
<%-- 											<c:otherwise> --%>
<%-- 												<label class="radio-block"><spring:message code="LB.D0324"/> --%>
<%-- 													<input type="radio" id="MB_Switch_On"  name="MB_Switch-${dataList.pks.CUSIDN}-${dataList.pks.IDGATEID}-${dataList.DEVICEID}" value="0" 啟用> --%>
<!-- 													<span class="ttb-radio"></span> -->
<!-- 												</label> -->
<%-- 	      										<label class="radio-block"><spring:message code="LB.D0325"/> --%>
<%-- 	      											<input type="radio" id="MB_Switch_Off" checked="checked" name="MB_Switch-${dataList.pks.CUSIDN}-${dataList.pks.IDGATEID}-${dataList.DEVICEID}" value="1" 停用> --%>
<!-- 	      											<span class="ttb-radio"></span> -->
<!-- 	      										</label> -->
<%-- 											</c:otherwise> --%>
<%--    										</c:choose> --%>
      										
      										
<!--       										<label class="radio-block">註銷 -->
<%--       											<input type="radio" id="MB_Switch_Cancel" name="MB_Switch-${dataList.pks.CUSIDN}-${dataList.pks.IDGATEID}-${dataList.DEVICEID}" value="2" 註銷> --%>
<!--       											<span class="ttb-radio"></span> -->
<!--       										</label> -->
<%--       											<input type="button" id="MBCancel"  class="spbutton btn-flat-orange" value="註銷" onclick="MBCancelEvent('${dataList.pks.IDGATEID}','${dataList.DEVICEID}')"> --%>
      											<input type="button" id="MBCancel"  class="spbutton btn-flat-orange" value="註銷" onclick="test('${dataList.pks.IDGATEID}','${dataList.DEVICEID}','${dataList.DEVICENAME}')">
										</td>
									</tr>
								</c:forEach>	
								</tbody>
							</table>
<!-- 							<div class="text-center"> -->
<!-- 			   				申請啟用碼 -->
<!-- 	                        	<input type="button" id="CMSUBMIT" class="ttb-button btn-flat-orange"  value="申請啟用碼"/> -->
<!-- 	                    	</div>	 -->
						</div>
						<div class="col-12 tab-content" id="div2" style="display: none;">
							<div class="ttb-input-block">
								<div class="ttb-input">
									<span><p class="text-center">您尚未申請任何裝置</p></span>
								</div>
							</div>
							<input type="button" id="CMSUBMIT" name="CMSUBMIT" class="ttb-button btn-flat-orange" value="立即申請" />
						</div>
					</div>
				</form>
				</section>
			</main>
			<!-- main-content END -->
			<!-- content row END -->
		</div>
		<%@ include file="../index/footer.jsp"%>
	</body>
</html>