<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>	
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
</head>
 <body>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %> 

	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
	<!-- 查詢服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.use_component_P1_D1-1" /></li>
    <!-- 預約取消/查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1491" /></li>
		</ol>
	</nav>

 	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
	<main class="col-12"> 
	<!-- 		主頁內容  -->
		<section id="main-content" class="container">
		<!-- 預約黃金交易查詢/取消 -->
			<h2><spring:message code= "LB.X0948" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>						
<!-- 取消成功 -->
<%-- 			<p style="text-align: center;color: red;"><spring:message code= "LB.D0183" /></p> --%>
			<form method="post" id="formId">
                <input type="hidden" id="back" name="back" value="">
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
                            <div class="ttb-message">
								<!-- 請確認取消資料 -->						
                                <span><spring:message code= "LB.D0183" /></span>
                            </div>
							<c:if test="${ result_data.data.APPTYPE.equals('01') }">
								<!-- 交易種類 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 交易種類 -->
											<spring:message code= "LB.W1060" />
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
										<!-- 預約黃金買進 -->
											<p><spring:message code= "LB.X0949" /></p>
										</div>
									</span>
								</div>
								<!-- 預約日期 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code= "LB.X0377" />
										</h4>
									</label>
									</span> <span class="input-block">
										<div class="ttb-input">
											<p>${ result_data.data.APPDATE }</p>
										</div>
									</span>
								</div>
								<!-- 台幣轉出帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 臺幣轉出帳號 -->
											<spring:message code= "LB.W1496" />
										</h4>
									</label>
									</span> <span class="input-block">
										<div class="ttb-input">
											<p>${ result_data.data.SVACN }</p>
										</div>
									</span>
								</div>
								<!-- 確認新使用者名稱 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
<!-- 黃金轉入帳號 -->
												<spring:message code= "LB.W1497" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<p>${ result_data.data.ACN }</p>
										</div>
									</span>
								</div>
								<!-- 確認新使用者名稱 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
<!-- 買進公克數 -->
												<spring:message code= "LB.W1498" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
<!-- 公克 -->
											<p>${ result_data.data.TRNGD } <spring:message code= "LB.W1435" /></p>
										</div>
									</span>
								</div>
							</c:if>
							
							<c:if test="${ result_data.data.APPTYPE.equals('02') }">
								<!-- 交易種類 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
	<!-- 交易種類 -->
												<spring:message code= "LB.W1060" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
										<!-- 預約黃金回售 -->
											<p><spring:message code= "LB.X0952" /></p>
										</div>
									</span>
								</div>
								<!-- 預約日期 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code= "LB.X0377" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<p>${ result_data.data.APPDATE }</p>
										</div>
									</span>
								</div>
								<!-- 台幣轉出帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 黃金轉出帳號 -->
											<spring:message code= "LB.W1515" />
										</h4>
									</label>
									</span> <span class="input-block">
										<div class="ttb-input">
											<p>${ result_data.data.ACN }</p>
										</div>
									</span>
								</div>
								<!-- 確認新使用者名稱 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
<!-- 臺幣轉入帳號 -->
												<spring:message code= "LB.W1518" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<p>${ result_data.data.SVACN }</p>
										</div>
									</span>
								</div>
								<!-- 確認新使用者名稱 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
<!-- 賣出公克數 -->
												<spring:message code= "LB.W1519" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
<!-- 公克 -->
											<p>${ result_data.data.TRNGD } <spring:message code= "LB.W1435" /></p>
										</div>
									</span>
								</div>
							</c:if>
							
							
						</div>
                        <!-- 列印  -->
                        <spring:message code="LB.Print" var="printbtn"></spring:message>
                        <input type="button" id="printbtn" value="${printbtn}" class="ttb-button btn-flat-orange" />
					
					</div>
				</div>
				</form>
				<div class="text-left">
				    <!-- 		說明： -->
				    <ol class="list-decimal description-list">
                       	<p><spring:message code="LB.Description_of_page"/></p>
				    <!-- 電子簽章:為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章(載具i-key)拔除並登出系統。 -->
				        <li><spring:message code="LB.Gold_Reservation_Query_P3_D1"/></li>
				    </ol>
				</div>
		</section>
		<!-- 		main-content END -->
	</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
	<!--   Js function -->
    <script type="text/JavaScript">
		$(document).ready(function() {
			init();
		});
    

	    function init(){
            //列印
        	$("#printbtn").click(function(){
				var params = {
					"jspTemplateName":"gold_reservation_query_result_print",
					//預約黃金交易查詢/取消
					"jspTitle":'<spring:message code= "LB.X0948" />',
					"APPTYPE":"${result_data.data.APPTYPE}",
					"APPDATE":"${result_data.data.APPDATE}",
					"APPTIME":"${result_data.data.APPTIME}",
					"ACN":"${result_data.data.ACN}",
					"SVACN":"${result_data.data.SVACN}",
					"TRNGD":"${result_data.data.TRNGD}"
				};
				openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
	    }	
 	</script>
</body>
</html>
 