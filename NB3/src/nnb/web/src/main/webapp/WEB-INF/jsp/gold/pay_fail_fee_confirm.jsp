<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
	
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
    <script type="text/javascript">
    
		$(document).ready(function() {
            //initFootable(); // 將.table變更為footable 
            setTimeout("initDataTable()",100);
			init();
		});

		// 交易機制選項
		function processQuery(){
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
		
			switch(fgtxway) {
				case '0':
// 					alert("交易密碼(SSL)...");
					$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
	    			$("#formId").submit();
					break;
					
				case '1':
// 					alert("IKey...");
					var jsondc = $("#jsondc").val();
					
					// 遮罩後不給捲動
// 					document.body.style.overflow = "hidden";

					// 呼叫IKEY元件
// 					uiSignForPKCS7(jsondc);
					
					// 解遮罩後給捲動
// 					document.body.style.overflow = 'auto';
					
					useIKey();
	 				unBlockUI();
					break;
					
				case '2':
// 晶片金融卡
					//alert("<spring:message code= "LB.D0234" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.D0234' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
					);

					// 遮罩後不給捲動
					document.body.style.overflow = "hidden";
					
					// 呼叫讀卡機元件
					listReaders();

					// 解遮罩後給捲動
					document.body.style.overflow = 'auto';
					
			    	break;
				case '7'://IDGATE認證
				
					acn=$('#ACN').val();
					svacn=$('#SVACN').val();
					trnamt=$('#TRNAMT').val();
					adopid='${idgateAdopid}';
					var uri = "${__ctx}/GOLD/TRANSACTION/n094_idgate_set_value";
					var rdata = { ACN:acn , SVACN:svacn , TRNAMT:trnamt , ADOPID:adopid};
					data = fstop.getServerDataEx(uri,rdata,false);
// 					alert(data);
					if(data !=null && data.result == true ){
						showIdgateBlock();
					}else{
// 						alert("QQ")
					}
					break;
			    	
				default:
					//alert("nothing...");
					errorBlock(
							null, 
							null,
							["nothing..."], 
							'<spring:message code= "LB.Quit" />', 
							null
					);
			}
			
		}
		//交易類別change 事件
		function changeFgtxway() {
			$('input[type=radio][name=FGTXWAY]').change(function () {
				console.log(this.value);
				if (this.value == '0') {
					$("#CMPASSWORD").addClass("validate[required,pwvd[PW,CMPASSWORD],custom[onlyLetterNumber]]")
				} else if (this.value == '1') {
					$("#CMPASSWORD").removeClass("validate[required,pwvd[PW,CMPASSWORD],custom[onlyLetterNumber]]");
				}else if (this.value == '7') {
					$("#CMPASSWORD").removeClass("validate[required,pwvd[PW,CMPASSWORD],custom[onlyLetterNumber]]");
				}
			});
		}

	    function init(){
	    	$("#pageshow").click(function(e){			
		    	$("#formId").validationEngine({binded:false,promptPosition: "inline" });
				console.log("submit~~");
				if(!$('#formId').validationEngine('validate')){
		        	e.preventDefault();
	 			}else{
	 				$("#formId").validationEngine('detach');
	 				initBlockUI();
	    			var action = '${__ctx}/GOLD/TRANSACTION/pay_fail_fee_result';
	    			$("#formId").attr("action", action);
	    			processQuery();
	 			}
			});
    		//上一頁按鈕
    		$("#CMBACK").click(function() {
				$("#formId").validationEngine('detach');
    			var action = '${__ctx}/GOLD/TRANSACTION/pay_fail_fee';
    			$('#back').val("Y");
    			$("#formId").attr("action", action);
    			$("#formId").submit();
    		});
			changeFgtxway();
	    }	
 	</script>
</head>
 <body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>

	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	<!--   IDGATE -->
    <%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %>	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 黃金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2272" /></li>
    <!-- 繳納定期扣款失敗手續費     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1533" /></li>
		</ol>
	</nav>

 	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<main class="col-12"> 
		<!-- 		主頁內容  -->
			<section id="main-content" class="container">
			<!--  繳納定期扣款失敗手續費 -->
				<h2><spring:message code="LB.W1533"/></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form method="post" id="formId">
					<input type="hidden" name="ADOPID" value="N094">	
					<input type="hidden" name="TXID" value="N094">	
	                <input type="hidden" id="back" name="back" value="">
					<input type="hidden" name="TXNTYPE" value="02">
	                <input type="hidden" id="ACN" name="ACN" value="${ result_data.data.ACN }">
	                <input type="hidden" id="TRNAMT" name="TRNAMT" value="${ result_data.data.TRNAMT }">
	                <input type="hidden" id="PINNEW" name="PINNEW" value="">
					<input type="hidden" id="TOKEN" name="TOKEN" value="${result_data.data.TOKEN}" /><!-- 防止重複交易 -->
	                <input type="hidden" id="TRNAMT" name="TRNAMT" value="${result_data.data.TRNAMT}" /><!-- 防止重複交易 -->
	                
	                <!-- ikey -->
					<input type="hidden" id="jsondc" name="jsondc" value='${ result_data.data.jsondc }'>
					<input type="hidden" id="ISSUER" name="ISSUER" value="">
					<input type="hidden" id="ACNNO" name="ACNNO" value="">
					<input type="hidden" id="TRMID" name="TRMID" value="">
					<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
					<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
					<input type="hidden" id="TAC" name="TAC" value="">
					<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
					<input type="hidden" name="CMTRANPAGE" value="1">
					
					<div class="main-content-block row">
						<div class="col-12">
	                        <ul class="ttb-result-list">
	                            <li>
									<!-- 黃金存摺帳號 -->
	                                <h3><spring:message code= "LB.D1090" /> :</h3>
	                                <p>
	                                    ${result_data.data.ACN }
	                                </p>
	                            </li>
	                            <li>
									<!-- 扣款失敗手續費合計 -->
	                                <h3><spring:message code= "LB.W1536" />:</h3>
	                                <p>
										<!-- 新台幣 -->
										<!-- 元 -->
										<spring:message code= "LB.NTD" /> ${result_data.data.TRNAMT}<spring:message code= "LB.Dollar" />
	                                </p>
	                            </li>
	                        </ul>
	                        <!-- 表格區塊 -->
	                    
	                        <table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
	                            <thead>
	                                <tr>
	<!-- 約定扣款帳號 -->
								      	<th><spring:message code= "LB.W1539" /></th>      
	<!-- 扣款日期 -->
								      	<th><spring:message code= "LB.D0368" /></th>
	<!-- 約定扣款金額 -->
								      	<th><spring:message code= "LB.W1541" /></th>
	<!-- 扣款失敗手續費 -->
								      	<th><spring:message code= "LB.W1542" /></th>
	<!-- 申購/變更來源 -->
								      	<th><spring:message code= "LB.W1543" /></th>
	                                </tr>
	                            </thead>
	                            <tbody>
		                            <c:forEach var="dataList" items="${ result_data.data.REC }">
	                           			<tr>
		                                    <!--預約日期-->
		                                    <td style="text-align:center">${dataList.TSFACN }</td>
		                                    <!--台幣轉出帳號-->
		                                    <td style="text-align:center">${dataList.FEELTD }</td>
		                                	<!-- 黃金轉入帳號 -->
		                                    <td class="text-right">${dataList.QTAMT }</td>
		                                 	<!-- 買進公克數 -->
		                                    <td class="text-right">${dataList.FEEAMT }</td>
		                                    <!-- 狀態 -->
		                                    <td style="text-align:center">${dataList.SOURCE}</td>
		                                 </tr>
		                            </c:forEach>
	                            </tbody>
	                        </table>
							<div class="ttb-input-block">
								
								<!-- 確認新使用者名稱 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
	<!-- 請選擇轉出帳號 -->
												<spring:message code= "LB.W1544" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<select name="SVACN" id="SVACN" class="custom-select select-input half-input validate[required]">
												<option value="">
	<!-- 請選擇帳號 -->
													---<spring:message code= "LB.W0257" />---
												</option>
												<c:forEach var="dataList" items="${n920_data.data.REC}">
													<option> ${dataList.ACN}</option>
												</c:forEach>
											</select>
										</div>
									</span>
								</div>
								
								<!--交易機制  SSL 密碼 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.Transaction_security_mechanism" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block" for="CMSSL">
									       		<input type="radio" name="FGTXWAY" id="CMSSL" value="0" checked /><spring:message code="LB.SSL_password" />
                                            	<span class="ttb-radio"></span>
									       	</label>
									    </div>
										<div class="ttb-input">
											<input type="password" id="CMPASSWORD"  maxlength="8" class="text-input validate[required,pwvd[PW,CMPASSWORD],custom[onlyLetterNumber]]">
										</div>
										
										<!-- IKEY -->
										<!-- 使用者是否可以使用IKEY -->
										<!-- 即使使用者可以用IKEY，若轉出帳號為晶片金融卡帶出的非約定帳號，也不給用 (By 景森)-->
										<c:if test = "${sessionScope.isikeyuser}">
<%-- 											<c:if test = "${transfer_data.data.TransferType != 'NPD'}"> --%>
												<div class="ttb-input">
													<label class="radio-block" for="CMIKEY">
														<input type="radio" name="FGTXWAY" id="CMIKEY" value="1">
														<spring:message code="LB.Electronic_signature" />
														<span class="ttb-radio"></span>
													</label>
												</div>
										
<%-- 											</c:if> --%>
										</c:if>
										<div class="ttb-input" name="idgate_group" style="display:none">
                                        <label class="radio-block">裝置推播認證(請確認您的行動裝置網路連線是否正常，及推播功能是否已開啟)<input type="radio" id="IDGATE"
                                            name="FGTXWAY" value="7"> <span class="ttb-radio"></span></label>
                                    	</div>
									</span>
								</div>
							</div>
							
							<!--回上頁 -->
	                        <spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>
	                        <input type="button" name="CMBACK" id="CMBACK" value="${cmback}" class="ttb-button btn-flat-gray">
							<input id="pageshow" name="pageshow" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />				
						</div>
					</div>
				</form>
			</section>
		</main>
	</div>

	<%@ include file="../index/footer.jsp"%>
	<!--   Js function -->
</body>
</html>
 