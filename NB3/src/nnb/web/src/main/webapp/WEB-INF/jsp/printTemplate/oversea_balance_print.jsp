<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function () {
			window.print();
		});
	</script>
</head>

<body class="watermark" style="-webkit-print-color-adjust: exact">
	<br /><br />
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif" /></div>
	<br /><br />
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<br />
	<div>
		<!-- 查詢時間 -->
		<p>
			<spring:message code="LB.Inquiry_time" />：
			${DATE} ${TIME}
		</p>
		<!-- 資料總數 -->
		<p>
			<spring:message code="LB.Total_records" />：
			${TOTRECNO}&nbsp;<spring:message code="LB.Rows"/>
		</p>
		<table class="print" data-toggle-column="first">
			<thead>
				<tr>
					<!-- 姓名   -->
					<th><spring:message code="LB.Name" /></th>
					<th class="text-left" colspan="6">${CUSNAME}</th>
				</tr>
				<tr>
					<th><spring:message code="LB.W1009" /><BR><spring:message code="LB.W0944" /></th>
			      	<th><spring:message code="LB.W1011" /><BR><spring:message code="LB.W1012" /></th>
			      	<th><spring:message code="LB.W0026" /><BR><spring:message code="LB.W1014" /></th>
			      	<th>參考贖回報價<br><spring:message code="LB.W1016" /></th>      	
			      	<th><spring:message code="LB.W0915" /><br><spring:message code="LB.W0909" /></th>
			      	<th><spring:message code="LB.W0918" /><br><spring:message code="LB.W1018" /></th>
			      	<th><spring:message code="LB.W1019" /><BR><spring:message code="LB.W0919" /></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="dataList" items="${print_datalistmap_data}">
					<c:if test="${ dataList.O18.equals('1')  }">
						<tr>
							<td class="text-center">${dataList.O15}<br>${dataList.HO01}</td>
							<td class="text-center">${dataList.O02}<br>${dataList.O03}</td>
							<td class="text-right">${dataList.O07}<br>${dataList.O05}</td>
							<td class="text-center">${dataList.O09}<br>${dataList.O10}</td>
							<td class="text-center">${dataList.O12}<br>${dataList.O08}</td>
							<td class="text-right">${dataList.O11}<br>${dataList.O16}</td>
							<td class="text-right">${dataList.O17}<br>${dataList.O14}</td>
						</tr>
					</c:if>
				</c:forEach>
			</tbody>
		</table>
		<br>
		<table class="print" data-toggle-column="first">
			<thead>
				<tr>
					<!-- 姓名   -->
					<th class="text-left" colspan="7"><spring:message code="LB.X0393" /></th>
				</tr>
				<tr>
					<th><spring:message code="LB.W0908" /></th>
			      	<th><spring:message code="LB.W0933" /></th>
			      	<th><spring:message code="LB.W0934" /></th>
			      	<th><spring:message code="LB.W1018" /></th>      	
			      	<th>(不含息參考)<br>投資損益<br>報酬率</th>
			      	<th><spring:message code="LB.W0918" /></th>
			      	<th>(含息參考)<br>投資損益<br>報酬率</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="dataList" items="${print_datalistmap_data}">
					<c:if test="${ dataList.O18.equals('2')  }">
						<tr>
							<td class="text-center">${dataList.O08}</td>
							<td class="text-right">${dataList.O07}</td>
							<td class="text-right">${dataList.O12}</td>
							<td class="text-right">${dataList.O16}</td>
							<td class="text-right">${dataList.O20}<br>${dataList.O13}</td>
							<td class="text-right">${dataList.O11}</td>
							<td class="text-right">${dataList.O17}<br>${dataList.O14}</td>
						</tr>
					</c:if>
				</c:forEach>
			</tbody>
		</table>
		<br /><br />
		<div class="text-left">
			<!-- 		說明： -->
			<ol class="description-list list-decimal">
				<p><spring:message code="LB.Description_of_page" /></p>
				<li>
	            	<spring:message code="LB.oversea_balance_P1_D1" /><br>
	                (1)<spring:message code="LB.oversea_balance_P1_D11" /><br>
	                (2)<spring:message code="LB.oversea_balance_P1_D12" />
	            </li>
				<li><spring:message code="LB.oversea_balance_P1_D2" /></li>
			</ol>
		</div>
	</div>
</body>

</html>