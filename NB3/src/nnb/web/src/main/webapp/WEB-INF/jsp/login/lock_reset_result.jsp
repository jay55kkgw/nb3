<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
</head>
<body>
	<div class="content row">
		<main class="col-12"> 
			<section id="main-content" class="container">
				<h2>
					<spring:message code='LB.X2242' />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
									
				<!-- 交易流程階段 -->
				<div id="step-bar">
					<ul>
						<li class="finished"><spring:message code="LB.Enter_data" /></li>
						<li class="finished"><spring:message code="LB.Confirm_data" /></li>
						<li class="active"><spring:message code="LB.Transaction_complete" /></li>
					</ul>
				</div>
				
				<!-- 功能內容 -->
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<h4 style="margin-top:10px;font-weight:bold;color:red">
							<spring:message code='LB.D0268' />
						</h4>
							
						<div class="ttb-input-block">
							
							<!-- 身分證字號 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code='LB.D0581' /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<p>${cusidn}</p>
									</div>
								</span>
							</div>
							
						</div>
						
						<div class="text-left">
							<font color="#FF0000"><spring:message code='LB.X2243' /></font>
						</div>
						
					</div>
				</div>
				
				<div class="text-left">
					<ol class="list-decimal text-left description-list">
					<p><spring:message code="LB.Description_of_page" /></p>
						<li><spring:message code='LB.X0853' /></li>
						<li><spring:message code='LB.W1584' /></li>
					</ol>
				</div>
				
			</section>
		</main>
	</div>
</body>
</html>
 