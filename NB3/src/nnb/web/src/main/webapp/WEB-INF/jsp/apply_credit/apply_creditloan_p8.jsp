<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<style type="text/css">
.table-1 {
    width: 100%;
    border-spacing: 0;
    border-collapse: collapse;
    margin: 0 auto;
    border:
}

.table-1>thead>tr>th,
.table-1>thead>tr>td {
    background-color: #EEEEEE;
    border-top: none;
    text-shadow: 0 1px 0 rgba(255, 255, 255, .5);
    font-weight: bold;
}

.table-1>tbody>tr>td,
.table-1>thead>tr>th {
    border: 1px solid #DDD;
    padding: 8px;
    text-align: left;
}

.table-1 tbody tr:first-child td:first-child {
    text-align: center;
}

.table-1 td:hover {
    background-color: #fbf8e9;
}
</style>
<script type="text/JavaScript">
$(document).ready(function() {
	initFootable();
	//列印
	$("#printButton").click(function(){
		var params = {
			"jspTemplateName":"apply_creditloan_p8_print",
			//線上申請小額信貸
			"jspTitle":"<spring:message code= "LB.D0554" />",
			"CUSIDN":"${bs.data.CUSIDN}",
			"CMQTIME":"${bs.data.CMQTIME}",
			"APPKINDNAME":"${bs.data.APPKINDNAME}",
			"APPAMT":"${bs.data.APPAMT}",
			"PURPOSENAME":"${bs.data.PURPOSENAME}",
			"PERIODS":"${bs.data.PERIODS}",
			"BANKNA":"${bs.data.BANKNA}",
			"APPNAME":"${bs.data.APPNAME}",
			"SEXNAME":"${bs.data.SEXNAME}",
			"CUSIDN":"${bs.data.CUSIDN}",
			"BRTHDT":"${bs.data.BRTHDT}",
			"EDUSNAME":"${bs.data.EDUSNAME}",
			"MARITALNA":"${bs.data.MARITALNA}",
			"CHILDNO":"${bs.data.CHILDNO}",
			"CPHONE":"${bs.data.CPHONE}",
			"PHONE_01":"${bs.data.PHONE_01}",
			"PHONE_02":"${bs.data.PHONE_02}",
			"PHONE_03":"${bs.data.PHONE_03}",
			"PHONE_11":"${bs.data.PHONE_11}",
			"PHONE_12":"${bs.data.PHONE_12}",
			"HOUSENAME":"${bs.data.HOUSENAME}",
			"MONEY11":"${bs.data.MONEY11}",
			"MONEY12":"${bs.data.MONEY12}",
			"ZIP2":"${bs.data.ZIP2}",
			"CITY2":"${bs.data.CITY2}",
			"ADDR2":"${bs.data.ADDR2}",
			"ZIP1":"${bs.data.ZIP1}",
			"CITY1":"${bs.data.CITY1}",
			"ADDR1":"${bs.data.ADDR1}",
			"EMAIL":"${bs.data.EMAIL}",
			"RELTYPE1":"${bs.data.RELTYPE1}",
			"RELNAME1":"${bs.data.RELNAME1}",
			"RELID1":"${bs.data.RELID1}",
			"RELTYPE2":"${bs.data.RELTYPE2}",
			"RELNAME2":"${bs.data.RELNAME2}",
			"RELID2":"${bs.data.RELID2}",
			"RELTYPE3":"${bs.data.RELTYPE3}",
			"RELNAME3":"${bs.data.RELNAME3}",
			"RELID3":"${bs.data.RELID3}",
			"RELTYPE4":"${bs.data.RELTYPE4}",
			"RELNAME4":"${bs.data.RELNAME4}",
			"RELID4":"${bs.data.RELID4}",
			"OTHCONM1":"${bs.data.OTHCONM1}",
			"OTHCOID1":"${bs.data.OTHCOID1}",
			"MOTHCONM1":"${bs.data.MOTHCONM1}",
			"MOTHCOID1":"${bs.data.MOTHCOID1}",
			"CAREERNAME":"${bs.data.CAREERNAME}",
			"CAREERIDNO":"${bs.data.CAREERIDNO}",
			"INCOME":"${bs.data.INCOME}",
			"ZIP4":"${bs.data.ZIP4}",
			"CITY4":"${bs.data.CITY4}",
			"ADDR4":"${bs.data.ADDR4}",
			"PHONE_41":"${bs.data.PHONE_41}",
			"PHONE_42":"${bs.data.PHONE_42}",
			"PHONE_43":"${bs.data.PHONE_43}",
			"PROFNONAME":"${bs.data.PROFNONAME}",
			"OFFICEY":"${bs.data.OFFICEY}",
			"OFFICEM":"${bs.data.OFFICEM}",
			"PRECARNAME":"${bs.data.PRECARNAME}",
			"PRETKY":"${bs.data.PRETKY}",
			"PRETKM":"${bs.data.PRETKM}"
		};
		openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
	});
});
</script>
</head>
<body>
<!-- header -->
<header>
	<%@ include file="../index/header.jsp"%>
</header>	
<!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<!--申請小額信貸 -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0543" /></a></li>
		</ol>
	</nav>
<!--左邊menu及登入資訊-->
<div class="content row">
	<%@ include file="../index/menu.jsp"%>
<!--快速選單及主頁內容-->
	<main class="col-12">
		<!--主頁內容-->
		<section id="main-content" class="container">
					<h2><!--申請小額信貸 -->
					<spring:message code= "LB.D0543" />
					</h2>
			<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
			<br/>
			<form method="post" id="formID">
			<div class="main-content-block row">
				<div class="col-12">
					<div class="ttb-message">
<!-- 申請資料上傳成功 -->
						<font color="red"><b><spring:message code= "LB.D0120" /></b></font>
					</div>
					<table class="table-1 four-col" data-toggle-column="first">
						<thead>
							<tr>
<!-- 系統時間 -->
								<th style="text-align: left"><spring:message code= "LB.D0121" />:${bs.data.CMQTIME}</th>
							</tr>
						</thead>
						<thead>
							<tr>
<!-- 貸款資料 -->
								<th style="text-align: center"><b><spring:message code= "LB.D0558" /></b></th>
							</tr>
						</thead>
						<tbody>
							<tr>
<!-- 貸款種類 -->
								<td style="text-align: left"><spring:message code= "LB.D0544" />:<spring:message code="${bs.data.APPKINDNAME}" /></td>
							</tr>
							<tr>
								<td style="text-align: left">
<!-- 申請金額：新臺幣 -->
<!-- 萬元整 -->
									<spring:message code= "LB.W0803" />&nbsp;${bs.data.APPAMT}&nbsp;<spring:message code= "LB.D0562" /></td>
							</tr>
							<tr>
								<td style="text-align: left">
<!-- 資金用途 -->
									<spring:message code= "LB.D0564" />：<spring:message code="${bs.data.PURPOSENAME}" /></td>
							</tr>
							<tr>
<!-- 申請還款期數 XX 個月-->
								<td  style="text-align: left"><spring:message code= "LB.D0573" />：
									${bs.data.PERIODS}<spring:message code= "LB.D0574" /></td>
							</tr>
							<tr>
<!-- 指定對保分行 -->
								<td style="text-align: left"><spring:message code= "LB.D0575" />：
									${bs.data.BANKNA}</td>
							</tr>
						</tbody>
						<thead>
							<tr>
<!-- 個人資料 -->
								<th style="text-align: center"><b><spring:message code= "LB.D0576" /></b></th>
							</tr>
						</thead>
						<tbody>
							<tr>
<!-- 姓名 -->
								<td style="text-align: left"><spring:message code= "LB.D0203" />：${bs.data.APPNAME}</td>
							</tr>
							<tr>
<!-- 性別 -->
								<td style="text-align: left"><spring:message code= "LB.D0578" />：<spring:message code="${bs.data.SEXNAME}" /></td>
							</tr>
							<tr>
								<td style="text-align: left">
<!-- 身分證字號 -->
										<spring:message code= "LB.D0581" />：${bs.data.CUSIDN}</td>
							</tr>
							<tr>
								<td style="text-align: left">
							<!-- 出生日期 -->
									<spring:message code= "LB.D0582" />：${bs.data.BRTHDT}</td>
							</tr>
							<tr>
								<td style="text-align: left">
<!-- 教育程度 -->
									<spring:message code= "LB.D0055" />：<spring:message code="${bs.data.EDUSNAME}" /></td>
							</tr>
							<tr>
<!-- 婚姻狀況 -->
								<td style="text-align: left"><spring:message code= "LB.D0057" />：<spring:message code="${bs.data.MARITALNA}" /></td>
							</tr>
							<tr>
<!-- 子女數 -->
<!-- 人 -->
								<td style="text-align: left"><spring:message code= "LB.D0602_1" />：${bs.data.CHILDNO}<spring:message code= "LB.D0602_2" /></td>
							</tr>
							<tr>
<!-- 行動電話 -->
								<td style="text-align: left"><spring:message code= "LB.D0069" />：${bs.data.CPHONE}</td>
							</tr>
							<tr>
								<td style="text-align: left">
<!-- 通訊電話 -->
									<spring:message code= "LB.D0599" />：（${bs.data.PHONE_01}）${bs.data.PHONE_02}
<!-- 分機 -->
									<spring:message code= "LB.D0095" />：${bs.data.PHONE_03}</td>
							</tr>
							<tr>
								<td style="text-align: left">
<!-- 戶籍電話 -->
									<spring:message code= "LB.D0144" />：（${bs.data.PHONE_11}）${bs.data.PHONE_12}</td>
							</tr>
							<tr>
								<td style="text-align: left">
<!-- 住宅狀況 -->
									<spring:message code= "LB.D0603" />：<spring:message code="${bs.data.HOUSENAME}" />
									<br /> 
								<c:if test="${bs.data.MONEY11 != ''}">
<!-- 房貸月繳 -->
<!-- 元 -->
									<spring:message code= "LB.D0609_1" />：${bs.data.MONEY11}<spring:message code= "LB.Dollar" />
								</c:if> 
								<c:if test="${bs.data.MONEY12 != ''}">
<!-- 房租月繳 -->
<!-- 仟元 -->
									<spring:message code= "LB.D0610_1" />：${bs.data.MONEY12}<spring:message code= "LB.D0610_2" />
								</c:if>
								</td>
							</tr>
							<tr>
								<td  style="text-align: left">
<!-- 現住地址 -->
									<spring:message code= "LB.D0611" />：${bs.data.ZIP2}${bs.data.CITY2}${bs.data.ADDR2}
								</td>
							</tr>
								<tr>
								<td  style="text-align: left">
<!-- 戶籍地址 -->
									<spring:message code= "LB.D0143" />：${bs.data.ZIP1}${bs.data.CITY1}${bs.data.ADDR1}
								</td>
							</tr>
							<tr>
								<td style="text-align: left">
									E-MAIL：${bs.data.EMAIL}</td>
							</tr>
						</tbody>
						<thead>
							<tr>
							<!-- ※配偶及二親等以內血親：為遵循銀行法第33條之3所定授信限額之規定而取得「同一關係人」之基本資料包括祖（外祖）父母、父母、兄弟姊妹、子女、孫（外孫）子女 -->
								<th  style="text-align: left"><b>※<spring:message code= "LB.D0616" />：<spring:message code= "LB.D0617" /></b>
								</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="text-align: left">
<!-- 稱謂 -->
<!-- 姓名 -->
									<spring:message code= "LB.D0618" />：${bs.data.RELTYPE1} <spring:message code= "LB.D0203" />：${bs.data.RELNAME1}</td>
							</tr>
							<tr>
								<td  style="text-align: left">
<!-- 身分證字號 -->
									<spring:message code= "LB.D0581" />：${bs.data.RELID2}</td>
							</tr>
							<tr>
								<td style="text-align: left">
<!-- 稱謂 -->
<!-- 姓名 -->
									<spring:message code= "LB.D0618" />：${bs.data.RELTYPE3} <spring:message code= "LB.D0203" />：${bs.data.RELNAME3}</td>
							</tr>
							<tr>
								<td style="text-align: left">
<!-- 身分證字號 -->
									<spring:message code= "LB.D0581" />：${bs.data.RELID3}</td>
							</tr>
							<tr>
								<td style="text-align: left">
<!-- 稱謂 -->
<!-- 姓名 -->
									<spring:message code= "LB.D0618" />：${bs.data.RELTYPE4} <spring:message code= "LB.D0203" />：${bs.data.RELNAME4}</td>
							</tr>
							<tr>
								<td style="text-align: left">
<!-- 身分證字號 -->
									<spring:message code= "LB.D0581" />：${bs.data.RELID4}</td>
							</tr>
						</tbody>
						<thead>
							<tr>
<!-- 職 業 資 料 -->
								<th style="text-align: center"><b><spring:message code= "LB.W0831" /></b></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="text-align: left">
<!-- 本人擔任負責人之其他企業名稱 -->
									<spring:message code= "LB.D0620" />：${bs.data.OTHCONM1}</td>
							</tr>
							<tr>
								<td style="text-align: left">
<!-- 統一編號 -->
									<spring:message code= "LB.D0621" />：${bs.data.OTHCOID1}</td>
							</tr>
							<tr>
								<td style="text-align: left">
<!-- 配偶擔任負責人之企業名稱 -->
									<spring:message code= "LB.D0622" />：${bs.data.MOTHCONM1}</td>
							</tr>
							<tr>
								<td style="text-align: left">
<!-- 統一編號 -->
									<spring:message code= "LB.D0621" />：${bs.data.MOTHCOID1}</td>
							</tr>
							</tbody>
						<thead>
							<tr>
<!-- 本人服務單位 -->
								<th style="text-align: center"><b><spring:message code= "LB.D0623" /></b></th>
							</tr>
						</thead>
						<tbody>

							<tr>
								<td style="text-align: left">
<!-- 公司名稱 -->
									<spring:message code= "LB.D0086" />：${bs.data.CAREERNAME}</td>
							</tr>
							<tr>
								<td style="text-align: left">
<!-- 統一編號 -->
									<spring:message code= "LB.D0621" />：${bs.data.CAREERIDNO}</td>
							</tr>
							<tr>
								<td style="text-align: left">
<!-- 年收入 -->
<!-- 萬元 -->
									<spring:message code= "LB.D0625_1" />：${bs.data.INCOME}<spring:message code= "LB.D0088_2" /></td>
							</tr>
							<tr>
<!-- 公司地址 -->
								<td style="text-align: left"><spring:message code= "LB.D0090" />：
									${bs.data.ZIP4}${bs.data.CITY4}${bs.data.ADDR4}
								</td>
							</tr>
							<tr>
								<td style="text-align: left">
<!-- 公司電話 -->
									<spring:message code= "LB.D0094" />：（${bs.data.PHONE_41}）${bs.data.PHONE_42} &nbsp;&nbsp;&nbsp;
<!-- 分機 -->
									<spring:message code= "LB.D0095" />：${bs.data.PHONE_43}</td>
							</tr>
							<tr>
								<td style="text-align: left">
<!-- 職稱 -->
									<spring:message code= "LB.D0087" />：${bs.data.PROFNONAME}</td>
							</tr>
							<tr>
								<td style="text-align: left">
<!-- 到職日：民國 -->
<!-- 年 -->
<!-- 月 -->
									<spring:message code= "LB.W0842" />&nbsp;${bs.data.OFFICEY}&nbsp;<spring:message code= "LB.D0089_2" />&nbsp;${bs.data.OFFICEM}&nbsp;<spring:message code= "LB.D0089_3" /></td>
							</tr>
							<tr>
								<td style="text-align: left">
<!-- 前職公司名稱 -->
										<spring:message code= "LB.D0630" />：${bs.data.PRECARNAME}
								</td>
							</tr>
							<tr>
<!-- 前職工作年資 -->
								<td style="text-align: left"><spring:message code= "LB.D0631" />： <c:if test="${bs.data.PRETKY != ''}">
<!-- 年 -->
									${bs.data.PRETKY}<spring:message code= "LB.D0089_2" /></c:if> 
									<c:if test="${bs.data.PRETKM != ''}">
<!-- 個月 -->
									${bs.data.PRETKM}<spring:message code= "LB.D0574" />
									</c:if>
								</td>
							</tr>
						</tbody>
					</table>
				
						<input type="button" id="printButton" value="<spring:message code="LB.Print"/>" class="ttb-button btn-flat-orange"/>
			
				</div>
				</div>
			</form>
		</section>
	</main>
</div>
<%@ include file="../index/footer.jsp"%>
</body>
</html>