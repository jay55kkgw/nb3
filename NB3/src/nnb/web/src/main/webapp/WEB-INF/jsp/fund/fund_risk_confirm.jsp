<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	//alert("result_data.data.COUNTRY >>"+'${result_data.data.COUNTRY}');
	//alert("result_data.data.FUS98E >> "+'${result_data.data.FUS98E}');
	
	$("#okButton").click(function(){
		var allCheck = true;
		
		$(".riskConfirmCheckBox").each(function(){
			if($(this).prop("checked") == false){
				allCheck = false;
			}
		});
		if(allCheck == true){
			$("#FDAGREEFLAG").val("Y");
			
			$("#formID").attr("action","${__ctx}/FUND/TRANSFER/fund_public");
			$("#formID").submit();
		}
		else{
			$("#FDAGREEFLAG").val("");
			//alert('<spring:message code="LB.Alert123"/>');
			errorBlock(
					null, 
					null,
					['<spring:message code="LB.Alert123" />'], 
					'<spring:message code="LB.Quit" />', 
					null
			);
		}
	});
	$("#cancelButton").click(function(){
		$("#formID").attr("action","${__ctx}/FUND/TRANSFER/fund_risk_confirmN");
		$("#formID").submit();
	});
});

</script>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	<!--麵包屑-->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<!-- 基金    -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0901" /></li>
			<!-- 基金交易 -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1064" /></li>
			<c:if test="${result_data.data.COUNTRY == 'B' || result_data.data.COUNTRY == 'C'}">
				<c:if test="${result_data.data.FUS98E != 'Y'}">
					<!-- 基金風險確認 -->
					<li class="ttb-breadcrumb-item active" aria-current="page">基金風險確認</li>
				</c:if>
				<c:if test="${result_data.data.FUS98E == 'Y'}">
					<!-- 非投資等級債券基金風險預告書 -->
					<li class="ttb-breadcrumb-item active" aria-current="page">非投資等級債券基金風險預告書</li>
				</c:if>
			</c:if>
			<c:if test="${result_data.data.COUNTRY != 'B' && result_data.data.COUNTRY != 'C'}">
				<c:if test="${result_data.data.FUS98E != 'Y'}">
					<!-- 主動購買及基金風險確認 -->
					<li class="ttb-breadcrumb-item active" aria-current="page">主動購買及基金風險確認</li>
				</c:if>
				<c:if test="${result_data.data.FUS98E == 'Y'}">
					<!-- 主動購買確認 -->
					<li class="ttb-breadcrumb-item active" aria-current="page">主動購買確認</li>
				</c:if>
			</c:if>
		</ol>
	</nav>
	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
				
					<form id="formID" method="post">
						<input type="hidden" name="TXID" value="${result_data.data.TXID}"/>
						<input type="hidden" name="TRANSCODE" value="${result_data.data.TRANSCODE}"/>
						<input type="hidden" name="CDNO" value="${result_data.data.CDNO}"/>
						<input type="hidden" name="UNIT" value="${result_data.data.UNIT}"/>
						<input type="hidden" name="BILLSENDMODE" value="${result_data.data.BILLSENDMODE}"/>
						<input type="hidden" name="SHORTTRADE" value="${result_data.data.SHORTTRADE}"/>
						<input type="hidden" name="SHORTTUNIT" value="${result_data.data.SHORTTUNIT}"/>
						<input type="hidden" name="INTRANSCODE" value="${result_data.data.INTRANSCODE}"/>
						<input type="hidden" name="INFUNDSNAME" value="${result_data.data.INFUNDSNAME}"/>
						<input type="hidden" name="FUNDAMT" value="${result_data.data.FUNDAMT}"/>
						<input type="hidden" name="RRSK" value="${result_data.data.RRSK}"/>
						<input type="hidden" name="RSKATT" value="${result_data.data.RSKATT}"/>
						<input type="hidden" name="GETLTD" value="${result_data.data.GETLTD}"/>
						<input type="hidden" name="RISK7" value="${result_data.data.RISK7}"/>
						<input type="hidden" name="GETLTD7" value="${result_data.data.GETLTD7}"/>
						<input type="hidden" name="FDINVTYPE" value="${result_data.data.FDINVTYPE}"/>
						<input type="hidden" id="FDAGREEFLAG" name="FDAGREEFLAG"/>
						<input type="hidden" name="FDNOTICETYPE" value="5"/>
						<input type="hidden" name="ACN1" value="${result_data.data.ACN1}"/>
						<input type="hidden" name="ACN2" value="${result_data.data.ACN2}"/>
						<input type="hidden" name="HTELPHONE" value="${result_data.data.HTELPHONE}"/>
						<input type="hidden" name="COUNTRYTYPE" value="${result_data.data.COUNTRYTYPE}"/>
						<input type="hidden" name="COUNTRYTYPE1" value="${result_data.data.COUNTRYTYPE1}"/>
						<input type="hidden" name="CUTTYPE" value="${result_data.data.CUTTYPE}"/>
						<input type="hidden" name="BRHCOD" value="${result_data.data.BRHCOD}"/>
						<input type="hidden" name="RISK" value="${result_data.data.RISK}"/>
						<input type="hidden" name="SALESNO" value="${result_data.data.SALESNO}"/>
						<input type="hidden" name="PRO" value="${result_data.data.PRO}"/>
						<input type="hidden" name="TYPE" value="${result_data.data.TYPE}"/>
						<input type="hidden" name="COMPANYCODE" value="${result_data.data.COMPANYCODE}"/>
						<input type="hidden" name="AMT3" value="${result_data.data.AMT3}"/>
						<input type="hidden" name="YIELD" value="${result_data.data.YIELD}"/>
						<input type="hidden" name="STOP" value="${result_data.data.STOP}"/>
						<input type="hidden" name="FUNDLNAME" value="${result_data.data.FUNDLNAME}"/>
						<input type="hidden" name="PAYTYPE" value="${result_data.data.PAYTYPE}"/>
						<input type="hidden" name="FUNDACN" value="${result_data.data.FUNDACN}"/>
						<input type="hidden" name="MIP" value="${result_data.data.MIP}"/>
						<input type="hidden" name="PAYDAY1" value="${result_data.data.PAYDAY1}"/>
						<input type="hidden" name="PAYDAY2" value="${result_data.data.PAYDAY2}"/>
						<input type="hidden" name="PAYDAY3" value="${result_data.data.PAYDAY3}"/>
						<input type="hidden" name="PAYDAY4" value="${result_data.data.PAYDAY4}"/>
						<input type="hidden" name="PAYDAY5" value="${result_data.data.PAYDAY5}"/>
						<input type="hidden" name="PAYDAY6" value="${result_data.data.PAYDAY6}"/>
						<input type="hidden" name="PAYDAY7" value="${result_data.data.PAYDAY7}"/>
						<input type="hidden" name="PAYDAY8" value="${result_data.data.PAYDAY8}"/>
						<input type="hidden" name="PAYDAY9" value="${result_data.data.PAYDAY9}"/>
						<input type="hidden" name="INVTYPE" value="${result_data.data.INVTYPE}"/>
						<input type="hidden" name="FEE_TYPE" value="${result_data.data.FEE_TYPE}"/>
						<input type="hidden" name="SLSNO" value="${result_data.data.SLSNO}"/>
						<input type="hidden" name="KYCNO" value="${result_data.data.KYCNO}"/>
						<input type="hidden" name="SAL01" value="${result_data.data.SAL01}"/>
						<input type="hidden" name="SAL03" value="${result_data.data.SAL03}"/>
						<input type="hidden" id="FUNDT" name="FUNDT" value="${result_data.data.FUNDT}"/>
						
					<div class="main-content-block row">
						<div class="col-12">
							<!--內容-->
							<!-- 前收邏輯不變 先框起來  START-->
							<c:if test="${result_data.data.FEE_TYPE == 'B'}">
							<c:if test="${result_data.data.COUNTRY == 'B' || result_data.data.COUNTRY == 'C'}">
								<c:if test="${result_data.data.FUS98E != 'Y'}">
									<div class="ttb-message">
										<p>基金風險確認</p>
	                                </div>
	                                <ul class="ttb-result-list" style="list-style:none;">
									<li class="full-list">
												<strong style="font-size:15px">
													本人已充分瞭解申購／轉換投資商品之所有可能風險（包含高收益債券基金主要係投資於非投資等級之高風險債券，且是類基金之配息政策可能致配息來源為本金），並願意承擔該基金之相關風險。
												</strong>
												<br/>
	                                </li>
	                                </ul>
	                                <label class="check-block">
									<input type="checkbox" class="riskConfirmCheckBox"/>已閱讀基金風險。
									<span class="ttb-check"></span>
									</label>
									<br/>
								</c:if>
								<c:if test="${result_data.data.FUS98E == 'Y'}">
									<div class="ttb-message">
										<p>非投資等級債券基金風險預告書</p>
	                                </div>
	                                <ul class="ttb-result-list" style="list-style:none;">
									<li class="full-list">
											<strong style="font-size:15px">
													台端於決定投資前，應充分瞭解下列以投資非投資等級債券為訴求之基金之特有風險：<br/><br/>
											一、信用風險：由於非投資等級債券之信用評等未達投資等級或未經信用評等，可能面臨債券發行機構違約不支付本金、利息或破產之風險。<br/>
											二、利率風險：由於債券易受利率之變動而影響其價格，故可能因利率上升導致債券價格下跌，而蒙受虧損之風險，非投資等級債券亦然。<br/>
											三、流動性風險：非投資等級債券可能因市場交易不活絡而造成流動性下降，而有無法在短期內依合理價格出售的風險。<br/>
											四、匯率風險：台端以新臺幣兌換外幣申購外幣計價基金時，需自行承擔新臺幣兌換外幣之匯率風險，取得收益分配或買回價金轉換回新臺幣時亦自行承擔匯率風險，當新臺幣兌換外幣匯率相較於原始投資日之匯率升值時，台端將承受匯兌損失。<br/>
											五、投資人投資以非投資等級債券為訴求之基金不宜占其投資組合過高之比重，且不適合無法承擔相關風險之投資人。<br/>
											六、若非投資等級債券基金為配息型，基金的配息可能由基金的收益或本金中支付。任何涉及由本金支出的部份，可能導致原始投資金額減損。本基金進行配息前未先扣除行政管理相關費用。<br/>
											七、非投資等級債券基金可能投資美國144A債券（境內基金投資比例最高可達基金總資產30%；境外基金不限），該債券屬私募性質，易發生流動性不足，財務訊息揭露不完整或價格不透明導致高波動性之風險。<br/>
											八、請台端注意申購基金前應詳閱公開說明書，充分評估基金投資特性與風險，更多基金評估之相關資料 (如年化標準差、Alpha、Beta及Sharp值等) 可至中華民國證券投資信託暨顧問商業同業公會網站之「基金績效及評估指標查詢專區 (<a href="https://www.sitca.org.tw/index_pc.aspx" target="_blank">https://www.sitca.org.tw/index_pc.aspx</a>) 查詢。
											</strong>
											<br/>
	                                </li>
	                                </ul>
	                                <label class="check-block">
									<input type="checkbox" class="riskConfirmCheckBox"/>本人已詳細閱讀前述風險告知，並已充分瞭解以投資非投資等級債券為訴求之基金之特有風險。
									<span class="ttb-check"></span>
									</label>
									<br/>
                                </c:if>
							</c:if>
							<c:if test="${result_data.data.COUNTRY != 'B' && result_data.data.COUNTRY != 'C'}">
								<c:if test="${result_data.data.FUS98E != 'Y'}">
									
										<div class="ttb-message">
											<p>主動購買及基金風險確認</p>
	                                	</div>
										<ul class="ttb-result-list" style="list-style:none;">
										<li class="full-list">
													<strong style="font-size:15px">
														本人確認本次申購／轉換之境外信託商品，係基於個人資產規劃之安排，經本人審慎考慮後所為之決定，且係本人主動要求貴行辦理，而非由貴行推介。
													</strong>
		                                </li>
		                                <br><br>
		                                <li class="full-list">
													<strong style="font-size:15px">
														本人已充分瞭解申購／轉換投資商品之所有可能風險（包含高收益債券基金主要係投資於非投資等級之高風險債券，且是類基金之配息政策可能致配息來源為本金），並願意承擔該基金之相關風險。
													</strong>
		                                </li>
		                                </ul>
		                                <label class="check-block">
										<input type="checkbox" class="riskConfirmCheckBox"/>已閱讀主動購買及基金風險確認說明
										<span class="ttb-check"></span>
										</label>
										<br/>
								</c:if>
								<c:if test="${result_data.data.FUS98E == 'Y'}">
										<div class="ttb-message">
											<p>主動購買確認</p>
	                                	</div>
	                                	<ul class="ttb-result-list" style="list-style:none;">
										<li class="full-list">
													<strong style="font-size:15px">
														本人確認本次申購／轉換之境外信託商品，係基於個人資產規劃之安排，經本人審慎考慮後所為之決定，且係本人主動要求貴行辦理，而非由貴行推介。
													</strong>
		                                </li>
		                                </ul>
										<label class="check-block">
										<input type="checkbox" class="riskConfirmCheckBox"/>已閱讀主動購買確認說明
										<span class="ttb-check"></span>
										</label>
										<br/>
										<br/>
										<div class="ttb-message">
											<p>非投資等級債券基金風險預告書</p>
	                                	</div>
	                                	<ul class="ttb-result-list" style="list-style:none;">
											<li class="full-list">
												<strong style="font-size:15px">
													台端於決定投資前，應充分瞭解下列以投資非投資等級債券為訴求之基金之特有風險：<br/>
												一、信用風險：由於非投資等級債券之信用評等未達投資等級或未經信用評等，可能面臨債券發行機構違約不支付本金、利息或破產之風險。<br/>
												二、利率風險：由於債券易受利率之變動而影響其價格，故可能因利率上升導致債券價格下跌，而蒙受虧損之風險，非投資等級債券亦然。<br/>
												三、流動性風險：非投資等級債券可能因市場交易不活絡而造成流動性下降，而有無法在短期內依合理價格出售的風險。<br/>
												四、匯率風險：台端以新臺幣兌換外幣申購外幣計價基金時，需自行承擔新臺幣兌換外幣之匯率風險，取得收益分配或買回價金轉換回新臺幣時亦自行承擔匯率風險，當新臺幣兌換外幣匯率相較於原始投資日之匯率升值時，台端將承受匯兌損失。<br/>
												五、投資人投資以非投資等級債券為訴求之基金不宜占其投資組合過高之比重，且不適合無法承擔相關風險之投資人。<br/>
												六、若非投資等級債券基金為配息型，基金的配息可能由基金的收益或本金中支付。任何涉及由本金支出的部份，可能導致原始投資金額減損。本基金進行配息前未先扣除行政管理相關費用。<br/>
												七、非投資等級債券基金可能投資美國144A債券（境內基金投資比例最高可達基金總資產30%；境外基金不限），該債券屬私募性質，易發生流動性不足，財務訊息揭露不完整或價格不透明導致高波動性之風險。<br/>
												八、請台端注意申購基金前應詳閱公開說明書，充分評估基金投資特性與風險，更多基金評估之相關資料 (如年化標準差、Alpha、Beta及Sharp值等) 可至中華民國證券投資信託暨顧問商業同業公會網站之「基金績效及評估指標查詢專區 (<a href="https://www.sitca.org.tw/index_pc.aspx" target="_blank">https://www.sitca.org.tw/index_pc.aspx</a>) 查詢。
												</strong>
												<br/>
											</li>
										</ul>
										<div>
										<label class="check-block">
										<input type="checkbox" class="riskConfirmCheckBox"/>本人已詳細閱讀前述風險告知，並已充分瞭解以投資非投資等級債券為訴求之基金之特有風險。
										<span class="ttb-check"></span>
										</label>
										</div>
								</c:if>
									</c:if>
								</c:if>
								<!-- 前收邏輯END-->
								
								<!-- 後收邏輯  START-->
								<c:if test="${result_data.data.FEE_TYPE == 'A'}">
									<c:if test="${result_data.data.COUNTRY == 'B' || result_data.data.COUNTRY == 'C'}">
									<!-- 後收國內基金  共有 :特別約定事項 -->
<!-- 	                                    <div class="ttb-message"> -->
<!-- 											<p>特別約定事項</p> -->
<!-- 			                            </div> -->
<!-- 			                            <div> -->
<!-- 			                            	<div class="CN19-clause" style="width: 100%; height: 500px; margin: 20px auto; border: 1px solid #D7D7D7; border-radius: 6px;"> -->
<%--            									<%@ include file="../term/AfterFund_D.jsp"%> --%>
<!-- 	                                    	</div> -->
<!-- 			                            </div> -->
<!-- 		                                <label class="check-block"> -->
<!-- 											<input type="checkbox" class="riskConfirmCheckBox"/>已閱讀「特別約定事項」並了解收費情形 -->
<!-- 												<span class="ttb-check"></span> -->
<!-- 										</label> -->
										<!-- 後收國內基金  非高收益 :基金風險確認 -->
										<c:if test="${result_data.data.FUS98E != 'Y'}">
											<div class="ttb-message">
												<p>基金風險確認</p>
			                                </div>
			                                <ul class="ttb-result-list" style="list-style:none;">
											<li class="full-list">
														<strong style="font-size:15px">
															本人已充分瞭解申購／轉換投資商品之所有可能風險（包含高收益債券基金主要係投資於非投資等級之高風險債券，且是類基金之配息政策可能致配息來源為本金），並願意承擔該基金之相關風險。
														</strong>
														<br/>
			                                </li>
			                                </ul>
			                                <label class="check-block">
											<input type="checkbox" class="riskConfirmCheckBox"/>已閱讀基金風險。
											<span class="ttb-check"></span>
											</label>
											<br/>
										</c:if>
										<!-- 後收國內基金  高收益 :高收益債券風險預告書 -->
										<c:if test="${result_data.data.FUS98E == 'Y'}">
											<div class="ttb-message">
												<p>非投資等級債券基金風險預告書</p>
			                                </div>
			                                <ul class="ttb-result-list" style="list-style:none;">
											<li class="full-list">
												<strong style="font-size:15px">
														台端於決定投資前，應充分瞭解下列以投資非投資等級債券為訴求之基金之特有風險：<br/><br/>
													一、信用風險：由於非投資等級債券之信用評等未達投資等級或未經信用評等，可能面臨債券發行機構違約不支付本金、利息或破產之風險。<br/>
													二、利率風險：由於債券易受利率之變動而影響其價格，故可能因利率上升導致債券價格下跌，而蒙受虧損之風險，非投資等級債券亦然。<br/>
													三、流動性風險：非投資等級債券可能因市場交易不活絡而造成流動性下降，而有無法在短期內依合理價格出售的風險。<br/>
													四、匯率風險：台端以新臺幣兌換外幣申購外幣計價基金時，需自行承擔新臺幣兌換外幣之匯率風險，取得收益分配或買回價金轉換回新臺幣時亦自行承擔匯率風險，當新臺幣兌換外幣匯率相較於原始投資日之匯率升值時，台端將承受匯兌損失。<br/>
													五、投資人投資以非投資等級債券為訴求之基金不宜占其投資組合過高之比重，且不適合無法承擔相關風險之投資人。<br/>
													六、若非投資等級債券基金為配息型，基金的配息可能由基金的收益或本金中支付。任何涉及由本金支出的部份，可能導致原始投資金額減損。本基金進行配息前未先扣除行政管理相關費用。<br/>
													七、非投資等級債券基金可能投資美國144A債券（境內基金投資比例最高可達基金總資產30%；境外基金不限），該債券屬私募性質，易發生流動性不足，財務訊息揭露不完整或價格不透明導致高波動性之風險。<br/>
													八、請台端注意申購基金前應詳閱公開說明書，充分評估基金投資特性與風險，更多基金評估之相關資料 (如年化標準差、Alpha、Beta及Sharp值等) 可至中華民國證券投資信託暨顧問商業同業公會網站之「基金績效及評估指標查詢專區 (<a href="https://www.sitca.org.tw/index_pc.aspx" target="_blank">https://www.sitca.org.tw/index_pc.aspx</a>) 查詢。
												</strong>
												<br/>
			                                </li>
			                                </ul>
			                                <label class="check-block">
											<input type="checkbox" class="riskConfirmCheckBox"/>本人已詳細閱讀前述風險告知，並已充分瞭解以投資非投資等級債券為訴求之基金之特有風險。
											<span class="ttb-check"></span>
											</label>
											<br/>
	                                    </c:if>
									</c:if>
									<c:if test="${result_data.data.COUNTRY != 'B' && result_data.data.COUNTRY != 'C'}">
										<!-- 後收國外基金 共有:特別約定事項 , 後收級別費用結構聲明書 -->
<!-- 										<div class="ttb-message"> -->
<!-- 											<p>特別約定事項</p> -->
<!-- 			                            </div> -->
<!-- 			                            <div> -->
<!-- 			                            	<div class="CN19-clause" style="width: 100%; height: 500px; margin: 20px auto; border: 1px solid #D7D7D7; border-radius: 6px;"> -->
<%--            									<%@ include file="../term/AfterFund_F.jsp"%> --%>
<!-- 	                                    	</div> -->
<!-- 			                            </div> -->
<!-- 		                                <label class="check-block"> -->
<!-- 											<input type="checkbox" class="riskConfirmCheckBox"/>已閱讀「特別約定事項」並了解收費情形 -->
<!-- 												<span class="ttb-check"></span> -->
<!-- 										</label> -->
										<div class="ttb-message">
											<p>後收級別費用結構聲明書</p>
			                            </div>
			                            <ul class="ttb-result-list" style="list-style:none;">
											<li class="full-list">
														<strong style="font-size:15px">
															本人於申購本後收級別基金前，已充分瞭解「<a style="color: blue;" target="_blank" href="https://tbb.moneydj.com/w/CustFundIDMap.djhtm?A=${result_data.data.TRANSCODE}&DownFile=8">境外基金前收及後收級別費用結構</a>」，並明瞭後收級別基金手續費雖可遞延收取，惟每年仍需支付分銷費，可能造成實際負擔費用增加。
														</strong>
														<br/>
			                                </li>
			                             </ul>
		                                <label class="check-block">
											<input type="checkbox" class="riskConfirmCheckBox"/>已閱讀「後收級別費用結構聲明書」
												<span class="ttb-check"></span>
										</label>
										<!-- 後收國外基金 非高收益:主動購買及基金風險確認 -->
										<c:if test="${result_data.data.FUS98E != 'Y'}">
											<div class="ttb-message">
												<p>主動購買及基金風險確認</p>
		                                	</div>
											<ul class="ttb-result-list" style="list-style:none;">
											<li class="full-list">
														<strong style="font-size:15px">
															本人確認本次申購／轉換之境外信託商品，係基於個人資產規劃之安排，經本人審慎考慮後所為之決定，且係本人主動要求貴行辦理，而非由貴行推介。
														</strong>
			                                </li>
			                                <br><br>
			                                <li class="full-list">
														<strong style="font-size:15px">
															本人已充分瞭解申購／轉換投資商品之所有可能風險（包含高收益債券基金主要係投資於非投資等級之高風險債券，且是類基金之配息政策可能致配息來源為本金），並願意承擔該基金之相關風險。
														</strong>
			                                </li>
			                                </ul>
			                                <label class="check-block">
											<input type="checkbox" class="riskConfirmCheckBox"/>已閱讀主動購買及基金風險確認說明
											<span class="ttb-check"></span>
											</label>
											<br/>
										</c:if>
										<!-- 後收國外基金 高收益:主動購買,非投資等級債券基金風險預告書 -->
										<c:if test="${result_data.data.FUS98E == 'Y'}">
											<div class="ttb-message">
											<p>主動購買確認</p>
	                                	</div>
	                                	<ul class="ttb-result-list" style="list-style:none;">
										<li class="full-list">
													<strong style="font-size:15px">
														本人確認本次申購／轉換之境外信託商品，係基於個人資產規劃之安排，經本人審慎考慮後所為之決定，且係本人主動要求貴行辦理，而非由貴行推介。
													</strong>
		                                </li>
		                                </ul>
										<label class="check-block">
										<input type="checkbox" class="riskConfirmCheckBox"/>已閱讀主動購買確認說明
										<span class="ttb-check"></span>
										</label>
										<br/>
										<br/>
										<div class="ttb-message">
											<p>非投資等級債券基金風險預告書</p>
	                                	</div>
	                                	<ul class="ttb-result-list" style="list-style:none;">
											<li class="full-list">
												<strong style="font-size:15px">
													台端於決定投資前，應充分瞭解下列以投資非投資等級債券為訴求之基金之特有風險：<br/>
												一、信用風險：由於非投資等級債券之信用評等未達投資等級或未經信用評等，可能面臨債券發行機構違約不支付本金、利息或破產之風險。<br/>
												二、利率風險：由於債券易受利率之變動而影響其價格，故可能因利率上升導致債券價格下跌，而蒙受虧損之風險，非投資等級債券亦然。<br/>
												三、流動性風險：非投資等級債券可能因市場交易不活絡而造成流動性下降，而有無法在短期內依合理價格出售的風險。<br/>
												四、匯率風險：台端以新臺幣兌換外幣申購外幣計價基金時，需自行承擔新臺幣兌換外幣之匯率風險，取得收益分配或買回價金轉換回新臺幣時亦自行承擔匯率風險，當新臺幣兌換外幣匯率相較於原始投資日之匯率升值時，台端將承受匯兌損失。<br/>
												五、投資人投資以非投資等級債券為訴求之基金不宜占其投資組合過高之比重，且不適合無法承擔相關風險之投資人。<br/>
												六、若非投資等級債券基金為配息型，基金的配息可能由基金的收益或本金中支付。任何涉及由本金支出的部份，可能導致原始投資金額減損。本基金進行配息前未先扣除行政管理相關費用。<br/>
												七、非投資等級債券基金可能投資美國144A債券（境內基金投資比例最高可達基金總資產30%；境外基金不限），該債券屬私募性質，易發生流動性不足，財務訊息揭露不完整或價格不透明導致高波動性之風險。<br/>
												八、請台端注意申購基金前應詳閱公開說明書，充分評估基金投資特性與風險，更多基金評估之相關資料 (如年化標準差、Alpha、Beta及Sharp值等) 可至中華民國證券投資信託暨顧問商業同業公會網站之「基金績效及評估指標查詢專區 (<a href="https://www.sitca.org.tw/index_pc.aspx" target="_blank">https://www.sitca.org.tw/index_pc.aspx</a>) 查詢。
												</strong>
												<br/>
											</li>
										</ul>
										<div>
										<label class="check-block">
										<input type="checkbox" class="riskConfirmCheckBox"/>本人已詳細閱讀前述風險告知，並已充分瞭解以投資高收益債券為訴求之基金之特有風險。
										<span class="ttb-check"></span>
										</label>
										</div>
										</c:if>
									</c:if>
								</c:if>
								
	                			<input type="button" id="cancelButton" value="<spring:message code="LB.Cancel"/>" class="ttb-button btn-flat-gray"/>
	                			<input type="button" id="okButton" value="<spring:message code="LB.Confirm_1"/>" class="ttb-button btn-flat-orange"/>	
	                		</div>
						</div>
					</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>