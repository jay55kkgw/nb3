<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
    
	<script type="text/JavaScript">
		$(document).ready(function() {
			init();
		});
    
	    function init(){
		    // 初始化表單驗證
	    	$("#formId").validationEngine({binded:false,promptPosition: "inline" });

	    	// submit前
	    	$("#pageshow").click(function(e){			
				console.log("before submit...");

				if (!$('#formId').validationEngine('validate')) {
			      	e.preventDefault();
			      	
				} else {
					$("#formId").validationEngine('detach');
					
					// 送出前先把變更的密碼做加密塞入
					$('#PINOLD').val($('#DPSIPD').val());
					$('#PINNEW').val($('#DPNSIPD').val());
					
					var pinold = $('#PINOLD').val();
					var pinnew = $('#PINNEW').val();
					
					initBlockUI();
					ChangeIKeyPin(pinold, pinnew);

				}
			});
	    }

	    function ChangeIKeyPin(pinCode, newPinCode)
	    {	
	    	try
	    	{
	    		//----------------------------------------------------------------------
	    		// call NativeAgentHost's 'ChangeIKeyPin'
	    		var rpcName = "ChangeIKeyPin";
	    	
	    		topWebSocketUtil.invokeRpcDispatcher(ChangeIKeyPin_Callback,
	    											 "ChangeIKeyPin",
	    											 pinCode,
	    											 newPinCode);
	    	}
	    	catch(e)
	    	{
	    		alertBlock("ChangeIKeyPin exception : "+ e.message)
	    		unBlockUI(initBlockId);
	    		$("#reset").click();
	    		return;
	    	}
	    	
	        //--------------------------------------------------------------------------
	        /// Name: ChangeIKeyPin_Callback
	        function ChangeIKeyPin_Callback(rpcStatus, rpcReturn)
	        {
	            try {
	                //------------------------------------------------------------------
	                topWebSocketUtil.tryRpcStatus(rpcStatus);
	                //------------------------------------------------------------------			
// 	    			document.getElementById('textResult').value = rpcReturn;
	    			
	    			if (rpcReturn === "E000")
	    			{
	    				// rpcReturn = 'E000'
	    			    alertBlock('<spring:message code= "LB.X2347" />');
	    			    unBlockUI(initBlockId);
	    			    $("#reset").click();
	    				return;
	    			}
	    			
	    			if ( rpcReturn.indexOf("A0") >= 0)
	    			{
	    				// rpcReturn = 'ERROR:: SetPin (162, 0x000000A0)'
	    				//iKey 舊密碼錯誤 !
	    				alertBlock('<spring:message code= "LB.X2348" />');
	    				unBlockUI(initBlockId);
	    				$("#reset").click();
	    				return;
	    			}
	    			
	    			if ( rpcReturn.indexOf("A1") >= 0)
	    			{
	    				// rpcReturn = 'ERROR:: SetPin (162, 0x000000A1)'
	    				//iKey 新密碼值不合規定 !
	    				alertBlock('<spring:message code= "LB.X2349" />');
	    				unBlockUI(initBlockId);
	    				$("#reset").click();
	    				return;
	    			}
	    			
	    			if ( rpcReturn.indexOf("A2") >= 0)
	    			{
	    				// rpcReturn = 'ERROR:: SetPin (162, 0x000000A2)'
	    				//iKey 新密碼長度不對 !
	    				alertBlock('<spring:message code= "LB.X2350" />');
	    				unBlockUI(initBlockId);
	    				$("#reset").click();
	    				return;
	    			}

	    			if ( rpcReturn.indexOf("A3") >= 0)
	    			{
	    				// rpcReturn = 'ERROR:: SetPin (164, 0x000000A3)'
	    				//iKey 密碼已過期 !
	    				alertBlock('<spring:message code= "LB.X2351" />');
	    				unBlockUI(initBlockId);
	    				$("#reset").click();
	    				return;
	    			}

	    			if ( rpcReturn.indexOf("A4") >= 0)
	    			{
	    				// rpcReturn = 'ERROR:: SetPin (164, 0x000000A4)'
	    				//iKey 密碼已被鎖定 !
	    				alertBlock('<spring:message code= "LB.X2352" />');
	    				unBlockUI(initBlockId);
	    				$("#reset").click();
	    				return;
	    			}
	    			
	    			if ( rpcReturn.indexOf("-7") >= 0)
	    			{
	    			    // rpcReturn = 'ERROR:: ChangePin (-7, 0xFFFFFFF9)'
	    			    //未輸入舊密碼!
	    				alertBlock('<spring:message code= "LB.X2353" />');
	    				unBlockUI(initBlockId);
	    				$("#reset").click();
	    				return;
	    			}
	    			
	    			if ( rpcReturn.indexOf("-8") >= 0)
	    			{
	    			    // rpcReturn = 'ERROR:: ChangePin (-8, 0xFFFFFFF8)'
	    			    //未輸入新密碼!
	    				alertBlock('<spring:message code= "LB.X2354" />');
	    				unBlockUI(initBlockId);
	    				$("#reset").click();
	    				return;
	    			}

	    			//尚未安裝元件(rpcReturn)
	    			alertBlock('<spring:message code= "LB.X1668" />（' + rpcReturn + "）");
	    			unBlockUI(initBlockId);
	    			$("#reset").click();

	            } catch (exception) {
	                alertBlock("exception: " + exception);
	                unBlockUI(initBlockId);
	                $("#reset").click();
	            }
	        }
	    }
	    
	    function alertBlock(text) {
			errorBlock(
					null, 
					null, 
					[text],
					'<spring:message code= "LB.Confirm" />', 
					null
				);
	    }
 	</script>
</head>
<body>

	<%@ include file="../component/trading_component.jsp"%>
	
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 憑證管理     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2148" /></li>
    <!-- 憑證載具密碼變更作業     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X2342" /></li>
		</ol>
	</nav>

	<!-- 快速選單及主頁內容 -->
	<!-- menu、登出窗格 --> 
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
		<main class="col-12">	
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.X2342" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				
				<form method="post" id="formId" action="${__ctx}/TRUST/changepw_result">

					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
							
								<!-- 舊憑證密碼 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4><spring:message code="LB.X2343" /></h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="hidden" id="PINOLD" name="PINOLD" />
											<input type="password" id="DPSIPD" name="DPSIPD" 
												class="text-input validate[required] " size="18" maxlength="16" value=""/>
										</div>
									</span>
								</div>
								
								<!--新憑證密碼 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4><spring:message code="LB.X2344" /></h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="hidden" id="PINNEW" name="PINNEW" />
											<input type="password" id="DPNSIPD" name="DPNSIPD" size="18" maxlength="16" value=""
												class="text-input validate[chkikeypwd[DPNSIPD]]" />
<%-- 											<p><spring:message code="LB.User_Change_password_note" /></p> --%>
										</div>
									</span>
								</div>
								
								<!-- 確認新憑證密碼 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
					                 			<spring:message code="LB.X2345" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="password" id="DPNSSIPD" name="DPNSSIPD" size="18" maxlength="16" value=""
												class="text-input validate[required ,recolumn[CP, DPSIPD, DPNSIPD, DPNSSIPD]]" />
											<p><spring:message code="LB.X2346" /></p>
										</div>
									</span>
								</div>
							</div>
							<input id="reset" name="reset" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" />	
							<input type="button" id="pageshow" name="pageshow" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />						
						</div>
					</div>	
				</form>

				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>
					<li><spring:message code="LB.Change_pw_P1_D1" /></li>
					<li><spring:message code="LB.Change_pw_P1_D2" /></li>
				</ol>
				
			</section>
		</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
    
</body>
</html>
