<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">

	<script type="text/javascript">
		$(document).ready(function () {
			init();
			getTmr();
			datetimepickerEvent();
		});

		function init() {
			$("#formId").validationEngine({binded: false,promptPosition: "inline"});
			$("#CMSUBMIT")
				.click(
					function (e) {
						//TODO 驗證
						e = e || window.event;
						//打開驗證隱藏欄位
						$("#hideblock_CMSDATE").show();
						$("#hideblock_CMEDATE").show();
						//塞值進span內的input
						$("#validate_CMSDATE").val($("#CMSDATE").val());
						$("#validate_CMEDATE").val($("#CMEDATE").val());
						console.log("submit~~");
						if (!$('#formId').validationEngine('validate')) {
							e.preventDefault();
						} else {
							$("#formId").validationEngine('detach');
							initBlockUI(); //遮罩
							$("#formId").removeAttr("target");
							$("#formId").attr("action", "${__ctx}/FUND/QUERY/oversea_data_result");
							$("#formId").submit();
						}
// 						initBlockUI(); //遮罩
// 						$("#formId").attr("action","${__ctx}/FUND/QUERY/oversea_data_result");
// 						$("#formId").submit();
					});
			$("#CMRESET").click(function(e){
				console.log("CMRESET submit");
				$("#formId")[0].reset();
				getTmr();
	  		});
		}
		//預約自動輸入今天
		function getTmr() {
			var today = "${bs.data.Today}";
			$('#CMSDATE').val(today);
			$('#CMEDATE').val(today);
			$('#odate').val(today);
		}
		//	日曆欄位參數設定
		function datetimepickerEvent() {

			$(".CMSDATE").click(function (event) {
				$('#CMSDATE').datetimepicker('show');
			});
			$(".CMEDATE").click(function (event) {
				$('#CMEDATE').datetimepicker('show');
			});

			jQuery('.datetimepicker').datetimepicker({
				timepicker: false,
				closeOnDateSelect: true,
				scrollMonth: false,
				scrollInput: false,
				format: 'Y/m/d',
				lang: '${transfer}'
			});
		}
// 		日期驗證
// 		function checkTimeRange() {
// 			console.log($('#CMEDATE').val());
// 			console.log($('#CMSDATE').val());
// 			var now = new Date(Date.now());
// 			var twoYm = 63115200000;
// 			var twoMm = 5259600000;
// 			var startT = new Date($('#CMSDATE').val());
// 			var endT = new Date($('#CMEDATE').val());
// 			var distance = now - startT;
// 			var range = endT - startT;
// 			var distanceE = now - endT;

// 			var limitS = new Date(now - twoYm);
// 			var limitE = new Date(startT.getTime() + twoMm);
// 			if (distance > twoYm) {
// 				var m = limitS.getMonth() + 1;
// 				var time = limitS.getFullYear() + '/' + m + '/' + limitS.getDate();
// 				// 起始日不能小於
// 				var msg = '<spring:message code="LB.Start_date_check_note_1" />' +
// 					time;
// 				alert(msg);
// 				return false;
// 			} else if (distance < 0) {
// 				var m = (now.getMonth()) + 1;
// 				var time = now.getFullYear() + '/' + m + '/' + now.getDate();
// 				// 起始日不能大於
// 				var msg = '起始日不能大於' + time;
// 				alert(msg);
// 				return false;
// 			} else {
// 				if (range < 0) {
// 					var msg = '終止日不能小於起始日';
// 					alert(msg);
// 					return false;
// 				} else if (distanceE < 0) {
// 					var m = now.getMonth() + 1;
// 					var time = now.getFullYear() + '/' + m + '/' + now.getDate();
// 					// 終止日不能大於
// 					var msg = '<spring:message code="LB.End_date_check_note_1" />' +
// 						time;
// 					alert(msg);
// 					return false;
// 				}
// 			}
// 			return true;
// 		}
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金/海外債券     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0368" /></li>
    <!-- 帳務查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Inquiry_Service" /></li>
    <!-- 海外債券交易明細查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1021" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2><spring:message code="LB.W1021" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="">

					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-input-block">
								<div class="ttb-input-item row">
<!-- 								查詢時間 -->
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.Inquiry_time" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input" id="nowtime">
										${bs.data.CMQTIME}
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<!--查詢期間  -->
									<span class="input-title"> 
									    <label>
									        <h4>
											    <spring:message code="LB.Inquiry_period_1" />
											</h4>
										</label>
									</span> <span class="input-block">
										<!--  指定日期區塊 -->
										<div class="ttb-input">
											<!--期間起日 -->
											<div class="ttb-input">
												<spring:message code="LB.D0013" />
												<input type="text" id="CMSDATE" name="CMSDATE" class="text-input datetimepicker validate[required,verification_date[CMSDATE]]" maxlength="10" value="" /> 
													<span class="input-unit CMSDATE">
													 	<img src="${__ctx}/img/icon-7.svg" />
													</span> 
												<br><br>
												<spring:message code="LB.Period_end_date" />
												<input type="text" id="CMEDATE" name="CMEDATE" class="text-input datetimepicker validate[required,verification_date[CMEDATE]]" maxlength="10" value="" /> 
													<span class="input-unit CMEDATE"> 
													    <img src="${__ctx}/img/icon-7.svg" />
													</span>
													<!-- 驗證用的span預設隱藏 -->
												<span id="hideblock_CheckDateScope" >
												<!-- 驗證用的input -->
												<input id="odate" name="odate" type="text" class="text-input validate[required,funcCall[validate_CheckDateScope['<spring:message code= "LB.Inquiry_period_1" />', odate, CMSDATE, CMEDATE, false, null,6]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												</span>
											</div>
										</div>
									</span>
								</div>
							</div>

							<!-- 重新輸入 -->
							<spring:message code="LB.Re_enter" var="cmRest"></spring:message>
							<input type="button" class="ttb-button btn-flat-gray" name="CMRESET" id="CMRESET" value="${cmRest}" />
														<!-- 網頁顯示-->
							<spring:message code="LB.Display_as_web_page" var="cmSubmit"></spring:message>
							<input type="button" class="ttb-button btn-flat-orange" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" />
						</div>
					</div>
					<div class="text-left">
					<ol class="description-list list-decimal text-left">
						<!-- 說明 -->
						<p><spring:message code="LB.Description_of_page" /></p>
					    <li><span><spring:message code="LB.oversea_data_P1_D1" /></span></li>
					</ol>
					</div>
				</form>
			</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>

</html>