<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
</head>
<script type="text/javascript">
	$(document).ready(function() {
		// Start masking after HTML loading is complete
		setTimeout("initBlockUI()", 10);
		// Start querying the data and complete the screen
		setTimeout("init()", 20);
		// unmask
		setTimeout("unBlockUI(initBlockId)", 500);
	});

	function init() {
		$("#CMBACK").click(function(e) {
			$("#formId").attr("action", "${__ctx}/login");
			$("#formId").submit();
		});
	}
</script>
<body>
	   
	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	<!-- Breadcrumbs -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i
					class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><a href="#">Network Security</a></li>
		</ol>
	</nav>

	<!-- left menu and login information -->
	<div class="content row">
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- Home Content -->
			<h2>Network security</h2>
			<form id="formId" method="post" action="">
				<div class="card-block shadow-box terms-pup-blcok">
					<div class="col-12 tab-content">
						<div class="ttb-input-block tab-pane fade show active"
							id="nav-trans-1" role="tabpanel" aria-labelledby="nav-home-tab">
							<div class="card-select-block"></div>
							<div class="card-detail-block">
								<div class="card-center-block">
									<!-- Q1 -->
									<div class="ttb-pup-block" role="tab">
										<a role="button" class="ttb-pup-title d-block"
											data-toggle="collapse" href="#popup-1" aria-expanded="true"
											aria-controls="popup-1">
											<div class="row">
												<span class="col-1">Q1</span>
												<div class="col-10 row">
													<span class="col-12">What is a fraudulent websites?
													</span>
												</div>
											</div>
										</a>
										<div id="popup-1" class="ttb-pup-collapse collapse show"
											role="tabpanel">
											<div class="ttb-pup-body">
												<ul class="ttb-pup-list">
													<li>
														<p>The fraud ring forges fake websites that are
															exactly the same as well-known websites, defrauding
															customers to enter these websites, and inputting personal
															information (such as ID number, account number, credit
															card number, password, etc.), in order to steal personal
															data, illegal use, and damage the rights of customers.</p>
													</li>
													<li>
														<p>Measure to respond</p>
														<ol style="list-style-type: circle; margin-left: 1rem;">
															<li><p>
																	Taiwan Business Online Bank website <a
																		href="https://ebank.tbb.com.tw">https://ebank.tbb.com.tw</a>,
																	please check the URL verbatim to ensure access to the
																	correct website before login. <a
																		onclick="window.open('${__ctx}/img/newsample1.png','','menubar=no,status=no,scrollbars=yes,top=20,left=50,toolbar=no,width=800,height=600')"
																		href="#">(Graphical description)</a>
																</p></li>
															<li>
																<p>
																	Please check the security lock logo in the lower right
																	corner of the computer screen. <a
																		onclick="window.open('https://portal.tbb.com.tw/tbbportal/fstop/images/sample2.gif','','menubar=no,status=no,scrollbars=yes,top=20,left=50,toolbar=no,width=800,height=600')"
																		href="#">(Graphical description)</a> Click the
																	security lock to display the voucher information. <a
																		onclick="window.open('https://portal.tbb.com.tw/tbbportal/fstop/images/sample3.gif','','menubar=no,status=no,scrollbars=yes,top=20,left=50,toolbar=no,width=800,height=600')"
																		href="#">(Graphical description)</a>
																</p>
															</li>
															<li><p>Please do not click on the hyperlink in
																	the unidentified email.</p></li>
															<li><p>Please change your online banking
																	password regularly to protect your personal
																	information.</p></li>
														</ol>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<!-- Q2 -->
									<div class="ttb-pup-block" role="tab">
										<a role="button" class="ttb-pup-title d-block"
											data-toggle="collapse" href="#popup-2" aria-expanded="true"
											aria-controls="popup-2">
											<div class="row">
												<span class="col-1">Q2.</span>
												<div class="col-10 row">
													<span class="col-12">What is a fraudulent email? </span>
												</div>
											</div>
										</a>
										<div id="popup-2" class="ttb-pup-collapse collapse show"
											role="tabpanel">
											<div class="ttb-pup-body">
												<ul class="ttb-pup-list">
													<li>
														<p>Thefraud ring counterfeit bank or online service
															provider notifies the customer that the data is out of
															date, invalid, needs to be updated, or is based on
															security reasons for identity verification, requiring the
															customer to enter personal information (such as ID
															number, account number, credit card number, password,
															etc.). If customers click the URLs that are directed by
															e-mail, they can link to the fake website to input
															personal data. Not serious, it becomes a list of
															spammers, seriously, the computer may be implanted with a
															Trojan horse program, which may damage the system or the
															privacy of personal data.</p>
													</li>
													<li>
														<p>Measure to respond</p>
														<ol style="list-style-type: circle; margin-left: 1rem;">
															<li><p>Don't worry about unsolicited e-mails,
																	and don't click on the hyperlinks it provided and
																	delete them immediately.</p></li>
															<li>
																<p>Please do not reply to suspicious emails that
																	require personal confidential information.</p>
															</li>
															<li><p class="text-danger">Please note! Taiwan
																	Business Bank will never ask customers to enter
																	personal confidential information via email. If you
																	receive such suspicious emails, please inform the
																	Taiwan Business Banking Service: 0800-00-7171. Please
																	install and regularly update your antivirus software.</p></li>
														</ol>
													</li>
												</ul>
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>
						<input type="BUTTON" class="ttb-button btn-flat-gray" value="Back"
							name="CMBACK" id="CMBACK">
					</div>
				</div>
			</form>
		</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>

</body>
</html>