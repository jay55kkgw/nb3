<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
    
<script>
	$(document).ready(function () {
		initFootable();
		$("#printbtn").click(function(){
			var i18n = new Object();
			i18n['jspTitle']='<spring:message code="LB.Historical_Statement_Detail_Inquiry" />'
			var params = {
					"jspTemplateName":"history_online_print",
					"jspTitle":i18n['jspTitle'],
					"CARDTYPE":"${history_online.data.CARDTYPE}",
					"BILL_NO":"${history_online.data.BILL_NO}",
					"FGPERIOD":"${history_online.data.FGPERIOD}",
					"QUERYTYPE":"BHWSHtml"
					
			}
			openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);

		});
		
		//上一頁按鈕
		$("#previous").click(function() {
			initBlockUI();
			if('${history_online.data.FUNCTION}'=="N810"){
				$("form").attr("action","${__ctx}/CREDIT/INQUIRY/card_overview");
			}else{
				$("form").attr("action","${__ctx}/CREDIT/INQUIRY/card_history");
			}
			$("form").submit();
		});
		
		$('#emailbtn').click(function(){
			$('#QUERYTYPE').val("BHWSReissue");
			$('#formId').attr("action","${__ctx}/CREDIT/INQUIRY/card_history_email")
			$("form").submit();
		});
		
		$('#onlinepaybtn').click(function(){
			$('#QUERYTYPE').val("BHWSHtmlImg");
			$('#formId').attr("action","${__ctx}/CREDIT/INQUIRY/card_history_onlinePay");
			$("form").submit();
		});
	});
	
	
	
</script>
</head>
<body style="background-color: #f2f2f2 !important;">
   	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 信用卡     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Credit_Card" /></li>
    <!-- 帳務查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Inquiry_Service" /></li>
    <!-- 歷史帳單明細查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Historical_Statement_Detail_Inquiry" /></li>
		</ol>
	</nav>

	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
	</div><!-- content row END -->
		
	<main class="col-12">
		<section id="main-content" class="container"><!-- 主頁內容  -->
			<h2>
				<spring:message code="LB.Historical_Statement_Detail_Inquiry" /><!-- 歷史帳單明細查詢 -->
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="">
				<input type="hidden" name="FGPERIOD" value="${history_online.data.FGPERIOD}">
				<input type="hidden" name="CARDTYPE" value="${history_online.data.CARDTYPE}">
				<input type="hidden" name="BILL_NO" value="${history_online.data.BILL_NO}">
				<input type="hidden" id="QUERYTYPE" name="QUERYTYPE" value="${history_online.data.QUERYTYPE}">
				<input type="hidden" id="FUNCTION" name="FUNCTION" value="${history_online.data.FUNCTION}">
				<input type="hidden" id="USEREMAIL" name="USEREMAIL" value="${sessionScope.dpmyemail}"/>
				<div class="main-content-block row">
					<div class="col-12">
						<div>
							<ul class="ttb-result-list">
								<li>
									<!-- 查詢時間  -->
									<h3>
										<spring:message code="LB.Inquiry_time" /><!-- 查詢時間  -->
									</h3>
									<p>
										${history_online.data.CMQTIME}
									</p>
								</li>
								<li>
									<h3>
										<spring:message code="LB.Inquiry_period" />
									</h3>
									<p>
										<!-- 查詢期間  -->
										<c:if test="${history_online.data.FGPERIOD == 'CMCURMON'}">
					                    	<spring:message code="LB.This_period" /><!-- 本期 -->
					                    </c:if>
					                    <c:if test="${history_online.data.FGPERIOD == 'CMLASTMON'}">
					                    	<spring:message code="LB.Previous_period" /><!-- 上期 -->
					                    </c:if>
					                    <c:if test="${history_online.data.FGPERIOD == 'CMLAST2MON'}">
					                    	<spring:message code="LB.Last_period" /><!-- 上上期 -->
					                    </c:if>
									</p>
								</li>
							</ul>
						</div>

						<table class="table" data-toggle-column="first">
							<thead>
								<tr>
									<th>
										<spring:message code="LB.Content_of_statement" /><!-- 帳單內容  -->
									</th>
								</tr>
						</thead>
						<tbody style="display:none">
						</tbody>
						</table>
					<div id="content" style="overflow:auto;">
						${history_online.data.INWARDS}
					</div>
						<ul class="ttb-result-list">
							<li style="margin-bottom: 10px;">
							<c:if test="${history_online.data.FGPERIOD == 'CMCURMON'}">
								<input type="button" class="ttb-sm-btn btn-flat-orange" id="overviewbtn" value="<spring:message code="LB.Pay" />" onclick="fstop.getPage('${pageContext.request.contextPath}'+'/CREDIT/PAY/payment','', '')">
								<input type="button" class="ttb-sm-btn btn-flat-orange" id="onlinepaybtn" value="<spring:message code="LB.Online_to_inquiry_of_payment" />" onclick="">
							</c:if>
							<input type="button" class="ttb-sm-btn btn-flat-orange" id="emailbtn" value="<spring:message code="LB.Online_to_email_of_statement" />" onclick=""><!-- EMAIL帳單 -->
							</li>
						</ul >
						
						<input type="button" id="previous" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />" />
						<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />" /><!-- 列印帳單 -->
					</div>
				</div>
			</form>
		</section>
	</main><!-- main-content END -->

	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>