<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="card-detail-block">
	<div class="card-center-block">
		<!-- Q1 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block" data-toggle="collapse" href="#popup2-1" aria-expanded="true" aria-controls="popup2-1">
	  		  <div class="row">
	        		<span class="col-1">Q1</span>
	            		<div class="col-11">
	                		<span>「结售外汇来源」之填报</span>
	            		</div>
	    		</div>
			</a>
			<div id="popup2-1" class="ttb-pup-collapse collapse" role="tabpanel">
	    		<div class="ttb-pup-body">
	        		<ul class="ttb-pup-list">
	            		<li>
	                		<p>依结售外汇之来源填写</p>
	            		</li>
	    		</div>
			</div>
		</div>
		<!-- Q2 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block" data-toggle="collapse" href="#popup2-2" aria-expanded="true" aria-controls="popup2-2">
	  		  <div class="row">
	        		<span class="col-1">Q2</span>
	            		<div class="col-11">
	                		<span>「申报日期」之填报</span>
	            		</div>
	    		</div>
			</a>
			<div id="popup2-2" class="ttb-pup-collapse collapse" role="tabpanel">
	    		<div class="ttb-pup-body">
	        		<ul class="ttb-pup-list">
	            		<li>
	                		<p>依申报结售外汇之日期填写。</p>
	            		</li>
	    		</div>
			</div>
		</div>
		<!-- Q3 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block" data-toggle="collapse" href="#popup2-3" aria-expanded="true" aria-controls="popup2-3">
				<div class="row">
					<span class="col-1">Q3</span>
					<div class="col-11">
						<span>「申报义务人」之填报</span>
					</div>
				</div>
			</a>
			<div id="popup2-3" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div class="ttb-result-list terms">
								<ol>
									<li>(一)、中华民国境内新台币五十万元以上等值外汇收支或交易之资金所有者为申报义务人，除经中央银行同意或中央银行另有规定外，应以自己名义申报。
									</li>
									
									<li>(二)、申报义务人委托他人办理新台币结汇申报时，应以委托人名义填报，并出具委托书及检附委托人、受托人之身分证明文件。
									</li>
									
									<li>(三)、非居住民法人应出具授权书，授权其在中华民国境内之自然人或法人为申报义务人，并以经授权之代表人或代理人名义填报，及叙明代理之事实。但境外非中华民国金融机构不得以汇入款项办理结售。
									</li>
								</ol>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q4 -->
		<div class="ttb-pup-block" role="tab">
	    	<a role="button" class="ttb-pup-title collapsed d-block" data-toggle="collapse" href="#popup2-4" aria-expanded="true" aria-controls="popup2-4">
	        	<div class="row">
	            	<span class="col-1">Q4</span>
	                	<div class="col-11">
	                    	<span>申报义务人登记证号」之填报</span>
	                    </div>
	            </div>
	        </a>
	        <div id="popup2-4" class="ttb-pup-collapse collapse" role="tabpanel">
	        	<div class="ttb-pup-body">
	            	<ul class="ttb-pup-list">
	                	<li>
	            			<div class="ttb-result-list terms">
	            			<ol>
	            				<li>(一)、 公司、行号
									<ol>
										<li>填列主管机关核准设立之统一编号，并提供设立登记表或最近之变更登记表影本。
										</li>
										<li>侨外投资事业筹备处，请填列主管机关核准投资文号及主管机关名称。
										</li>
									</ol>
								</li>
								<li>(二)、 团体
									<ol>
										<li>请填列主管机关核准设立证照上之统一编号。
										</li>
										<li>前述证照上无统一编号者，请填列设立登记主管机关名称、登记证号以及税捐稽征单位编配之扣缴单位统一编号。
										</li>
										<li>会计师事务所、律师事务所、诊所等比照团体填报。
										</li>
									</ol>
								</li>
								<li>(三)、 我国国民
									<ol>
										<li>领有国民身分证者：请填列中华民国国民身分证统一编号及出生日期。
										</li>
										<li>持内政部入出境管理局核发之中华民国台湾地区居留证之华侨：请填列居留证号码(如居留证上载有统一证号者，请填列统一证号)及出生日期。
										</li>
										<li>持侨务委员会核发之华侨身分证明文件及回国投资购置房屋证明文件者：请填列华侨身分证明文号。
										</li>
									</ol>
								</li>
								<li>(四)、 外国人
									<ol>
										<li>持外侨居留证者：请填列居留证号码(如居留证上载有统一证号者，请填列统一证号)、出生日期及居留证发给日期、到期日期。
										</li>
										<li>持外国护照者：请于(无外侨居留证者)栏内填列国别及护照号码。
										</li>
										<li>持中华民国护照，但未领有中华民国国民身分证之华侨：请于(无外侨居留证者)栏内填列国别为发证地所在国及护照号码，并加注发证单位。
										</li>
										<li>香港及澳门地区居民：请于(无外侨居留证者)栏内填列国别为香港地区或澳门地区，及证照号码为入出境证或临时入境停留通知单号码。
										</li>
										<li>大陆人士：请于(无外侨居留证者)栏内填列国别为大陆地区，及证照号码为旅行证或居留证号码。
										</li>
									</ol>
								</li>
			            	</ol>
			            	</div>
	                    </li>
	                </ul>
	        	</div>
	      	</div>
		</div>
	</div>
</div>
