<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
	
	<script type="text/javascript">
		$(document).ready(function() {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		
		// 畫面初始化
		function init() {
			// 表單驗證初始化
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
			// 確認鍵 click
			goOn();
			// 上一頁按鈕 click
			goBack();
			// 交易類別change 事件
			changeFgtxway();
		}
		
		// 確認鍵 Click
		function goOn() {
			$("#CMSUBMIT").click( function(e) {
				// 送出進表單驗證前將span顯示
				$("#hideblock").show();
				
				console.log("submit~~");
	
				// 表單驗證
				if ( !$('#formId').validationEngine('validate') ) {
					e.preventDefault();
				} else {
					// 解除表單驗證
					$("#formId").validationEngine('detach');
					
					// 通過表單驗證準備送出
					processQuery();
				}
			});
		}
		
		
		// 通過表單驗證準備送出
		function processQuery(){
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
			// 交易機制選項
			switch(fgtxway) {
				case '0':
					// SSL
					// 交易密碼sha1
					$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
					// 清除SSL密碼欄，避免儲存或傳送明碼到後端
					$('#CMPASSWORD').val("");
					// 遮罩
		         	initBlockUI();
		            $("#formId").submit();
		         	
					break;
				case '1':
					// IKEY
					useIKey();
					
					break;
				case '2':
					// 晶片金融卡
					var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
					useCardReader(capUri);
					
			    	break;
				case '7':
		            idgatesubmit= $("#formId");		 
		            showIdgateBlock();		 
		            break;
				default:
					//<!-- 請選擇交易機制 -->
					//alert("<spring:message code= "LB.Alert001" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert001' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			}
		}
		
		
		// 上一頁按鈕 click
		function goBack() {
			// 上一頁按鈕
			$("#CMBACK").click(function() {
				// 遮罩
				initBlockUI();
				
				// 解除表單驗證
				$("#formId").validationEngine('detach');
				
				// 讓Controller知道是回上一頁
				$('#back').val("Y");
				// 回上一頁
				var action = '${__ctx}/FCY/ACCT/TDEPOSIT/f_deposit_cancel';
				$("#formId").attr("action", action);
				$("#formId").submit();
			});
		}
		
		
		// 交易類別change 事件
		function changeFgtxway(){
			$('input[type=radio][name=FGTXWAY]').change(function(){
				console.log(this.value);
				if(this.value=='0'){
					$("#CMPASSWORD").addClass("validate[required]")
				}else if(this.value=='1'){
					$("#CMPASSWORD").removeClass("validate[required]");
				}else if(this.value=='2'){
					$("#CMPASSWORD").removeClass("validate[required]");
				}else if(this.value=='7'){
					$("#CMPASSWORD").removeClass("validate[required]");
				}
			});
		}
		
		// 重新輸入
	 	function formReset() {
	 		if ($('#actionBar').val()=="reEnter"){
		 		$('#actionBar').val("");
		 		document.getElementById("formId").reset();
	 		}
		}
    		
	</script>

</head>

<body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %> 
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 定存服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Current_Deposit_To_Time_Deposit" /></li>
    <!-- 綜存定存解約     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.FX_Time_Deposit_Termination" /></li>
		</ol>
	</nav>



    <!-- menu、登出窗格 -->
    <div class="content row">
        <!-- 功能選單內容 -->
        <%@ include file="../index/menu.jsp"%>
    <!-- 		主頁內容  -->
    <main class="col-12">
		<section id="main-content" class="container">
			<h2>
				<!--外匯綜存定存解約 -->
				<spring:message code="LB.FX_Time_Deposit_Termination" />
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<!-- 下拉式選單-->
			<div class="print-block no-l-display-btn">
				<select class="minimal" id="actionBar" onchange="formReset()">
					<option value=""><spring:message code="LB.Execution_option" /></option>
						<!-- 重新輸入-->
					<option value="reEnter"><spring:message code="LB.Re_enter"/></option>
				</select>
			</div>
			<form name="formId" id="formId" method="post" action="${__ctx}/FCY/ACCT/TDEPOSIT/f_deposit_cancel_result">
				<input type="hidden" id="back" name="back" value="">
				<c:set var="BaseResultData" value="${f_deposit_cancel_confirm.data}"></c:set>
				<input type="hidden" id="PINNEW" name="PINNEW"  value="">
				<%--json字串要用單引用 --%>
				<input type="hidden" name="JSONSTR" id="JSONSTR" value='${BaseResultData.JSONSTR}' />
				<input type="hidden" name="TXTOKEN" id="TXTOKEN" value="${BaseResultData.TXTOKEN}" />
				<!-- 交易機制所需欄位 -->
				<input type="hidden" id="jsondc" name="jsondc" value='${f_deposit_cancel_confirm.data.jsondc}'>
				<input type="hidden" id="ISSUER" name="ISSUER" value="">
				<input type="hidden" id="ACNNO" name="ACNNO" value="">
				<input type="hidden" id="TRMID" name="TRMID" value="">
				<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
				<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
				<input type="hidden" id="TAC" name="TAC" value="">
				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
				
				<!--交易步驟 -->
				<div id="step-bar">
					<ul>
				<!-- 確認資料 -->
						<li class="finished"><spring:message code="LB.Confirm_data"/></li>
				<!-- 交易完成 -->
						<li class=""><spring:message code="LB.Transaction_complete"/></li>
					</ul>
        		</div>
				<!-- 表單顯示區  -->
				<div class="main-content-block row">
					<div class="col-12">
						<div class="ttb-input-block">
							<div class="ttb-message">
								<span>
									<!-- 請確認預約資料 -->
									<spring:message code="LB.Confirm_the_Scheduled_Transaction_data" />
								</span>
							</div>
							<!-- 轉帳日期 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Transfer_date" /></h4>
									</label>
								</span>
								<span class="input-block">
									${BaseResultData.SHOW_TRDATE}
								</span>
							</div>
							<!-- 帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Account" /></h4>
									</label>
								</span>
								<span class="input-block">
									${BaseResultData.ACN}
								</span>
							</div>
							<!-- 存單號碼 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Certificate_no" /></h4>
									</label>
								</span>
								<span class="input-block">
									${BaseResultData.FDPNUM}
								</span>
							</div>
							<!-- 幣別 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Currency" /></h4>
									</label>
								</span>
								<span class="input-block">
									${BaseResultData.CUID}
								</span>
							</div>
							<!-- 存單金額 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Certificate_amount" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
	                                    <span class="high-light">
		                                    <!--顯示金額 -->
		                                    ${BaseResultData.SHOW_AMT }</span>
		                                    <!--元 -->
		                                    <span class="input-unit"><spring:message code="LB.Dollar"/></span>
                                 	</div>
                                 </span>	
							</div>
							<!-- 計息方式 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Interest_calculation" /></h4>
									</label>
								</span>
								<span class="input-block">
									${BaseResultData.SHOW_INTMTH}
								</span>
							</div>
							<!-- 起存日 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Start_date" /></h4>
									</label>
								</span>
								<span class="input-block">
									${BaseResultData.SHOW_DPISDT}
								</span>
							</div>
							<!-- 到期日 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Maturity_date" /></h4>
									</label>
								</span>
								<span class="input-block">
									${BaseResultData.SHOW_DUEDAT}
								</span>
							</div>
							<!-- 交易備註 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Transfer_note" /></h4>
									</label>
								</span>
								<span class="input-block">
									 ${BaseResultData.CMTRMEMO }
								</span>
							</div>
							<!--解約成功Email通知(可不填) -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Successfully_termination_the_email_notification" /></h4>
									</label>
								</span>
								<span class="input-block">
									<!-- 電子信箱 -->
									<spring:message code="LB.Email" /> :
									${BaseResultData.CMTRMAIL }
									<br />
									<!-- 摘要內容 -->
									<spring:message code="LB.Summary" />
									:
									${BaseResultData.CMMAILMEMO }
								</span>
							</div>
							<!-- 交易機制 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<!-- 交易機制 -->
										<h4><spring:message code="LB.Transaction_security_mechanism" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block">
											<!-- 交易密碼(SSL) -->
											<spring:message code="LB.SSL_password" />
											 <input type="radio" name="FGTXWAY" checked="checked" value="0">
											<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<!--請輸入密碼 -->
										<spring:message code="LB.Please_enter_password" var="pleaseEnterPin"/>
										<input type="password" id="CMPASSWORD" name="CMPASSWORD" class="text-input validate[required]" maxlength="8" placeholder="${pleaseEnterPin}">
									</div>
									
									<!-- 電子簽章(載具i-key) -->
									<!-- 使用者是否可以使用IKEY -->
									<c:if test = "${sessionScope.isikeyuser}">
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Electronic_signature" />
												<input type="radio" name="FGTXWAY" id="CMIKEY" value="1">
												<span class="ttb-radio"></span>
											</label>
										</div>
									</c:if>
									<div class="ttb-input" name="idgate_group" style="display:none">		 
                                       <label class="radio-block">裝置推播認證(請確認您的行動裝置網路連線是否正常，及推播功能是否已開啟)
	                                       <input type="radio" id="IDGATE" 	name="FGTXWAY" value="7"> 
	                                       <span class="ttb-radio"></span>
                                       </label>		 
    	                            </div>
								</span>
							</div>
							<!-- 驗證碼-->
							<div class="ttb-input-item row" style="display: none;">
								<span class="input-title">
									<label>
										<!-- 驗證碼 -->
										<h4><spring:message code="LB.Captcha" /></h4>
									</label>
								</span>
								<span class="input-block">
									<img src="" align="top" class ="verification-img" id="random">
									<input maxLength="6" size="7" name="CHECKPIC" value="">
									<font color="#FF0000">
										<!-- ※英文不分大小寫，限半型字 -->
										<spring:message code="LB.Captcha_refence" />
									</font>
									<!-- 重新產生驗證碼 -->
									<input type="button" name="reshow" value="<spring:message code="LB.Regeneration" />" onclick="" />
								</span>
							</div>
						</div>
					<!-- button -->

							<!--回上頁 -->
							<spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>
							<input type="button" name="CMBACK" id="CMBACK" value="${cmback}" class="ttb-button btn-flat-gray" >
							<!--重新輸入 -->
<%-- 							<spring:message code="LB.Re_enter" var="cmRest"></spring:message> --%>
<%-- 							<input type="reset" name="CMRESET" id="CMRESET" value="${cmRest}" class="ttb-button btn-flat-gray no-l-disappear-btn"> --%>
							<!-- 確定 -->
							<spring:message code="LB.Confirm" var="cmSubmit"></spring:message>
							<input type="button" id="CMSUBMIT" value="${cmSubmit}" class="ttb-button btn-flat-orange">

					</div>
				</div>
				<div class="text-left">
					<!-- 		說明： -->
					<ol class="description-list list-decimal text-left">
					    <p><spring:message code="LB.Description_of_page"/></p>
						<li><strong style="font-weight: 400"><spring:message code="LB.F_deposit_cancel_P2_D1"/></strong></li>
					</ol>
				</div>
			</form>
		</section>
		<!-- 		main-content END -->
	</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>

</html>