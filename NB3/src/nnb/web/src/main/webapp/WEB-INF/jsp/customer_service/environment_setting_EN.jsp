<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<script type="text/javascript" src="${__ctx}/component/windows/IKey/ikeyTopWebSocketUtil.js"></script>
	<script type="text/javascript" src="${__ctx}/component/windows/IKey/IKeyMethod.js"></script>
	<script type="text/javascript" src="${__ctx}/component/windows/noneIE/noneIECard.js"></script>
	<script type="text/javascript" src="${__ctx}/component/windows/noneIE/topWebSocketUtil.js"></script>
	<script type="text/javascript" src="${__ctx}/keyboard/js/plugins.js"></script>
</head>
<script type="text/javascript">
	var isIkeyUser; // 使用者是否ikey使用者
	var comVersion; // 各元件的最新版本號
	var platformNew; // 作業系統
	var hasAskDownload = false; // 因為現在WINDOW多瀏覽器元件的安裝檔整合成一個，所以要多一個變數，只詢問使用者下載一次

	$(document).ready(function(){
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	function init(){
		$("#CMBACK").click(function(e){
			$("#formId").attr("action","${__ctx}/login");
			$("#formId").submit();
		});
		$("#win").click(function(e){
			jQuery("#componentPath").val("win");
			jQuery("#componentForm").submit();
		});
		$("#mac").click(function(e){
			jQuery("#componentPath").val("mac");
			jQuery("#componentForm").submit();
		});
		component_version(); // 取得各元件的最新版本號
		component_platform(); // 取得裝置作業系統
		if(platformNew.indexOf("Win") > -1){
			$("#winos").show();
		}
		else{
			$("#macos").show();
		}
		
		// 元件邏輯 (進畫面後延遲提示，避免影響其他資源載入)
 		setTimeout(component_init(),1500);
	}
	
	// 取得各元件的最新版本號
	function component_version() {
		var uri = '${__ctx}' + "/COMPONENT/component_version_aj";
		var bs = fstop.getServerDataEx(uri, null, false);
		console.log("comVersion_bs: " + JSON.stringify(bs));

		comVersion = bs.data;
		console.log("comVersion: " + JSON.stringify(comVersion));
	}

	// 判斷裝置
	function checkBrowser() {
		var checkBrowserResult = false;
		if (navigator.userAgent.match(/Android/i)
				|| navigator.userAgent.match(/webOS/i)
				|| navigator.userAgent.match(/iPhone/i)
				|| navigator.userAgent.match(/iPad/i)
				|| navigator.userAgent.match(/iPod/i)
				|| navigator.userAgent.match(/BlackBerry/i)
				|| navigator.userAgent.match(/Windows Phone/i)) {
			// 行動裝置
			console.log("component checkBrowser not pass!!!");
		} else {
			// 可使用元件之裝置
			checkBrowserResult = true;
			console.log("component checkBrowser pass...");
		}

		console.log("checkBrowser: " + checkBrowserResult);

		return checkBrowserResult;
	}

	// 取得裝置作業系統
	function component_platform() {
		platformNew = navigator.platform;
		if (window.console) {
			console.log("component_platform: " + platformNew);
		}
	}
	
	// 元件初始化
	function component_init(){
		// 行動裝置不能做，所以要先判斷
		if (checkBrowser()) {
			// 改在此頁面載入時引入
// 			$.loadScript(
// 				'${__ctx}/component/combo/topWebSocketUtil.js', function() {
// 					console.log("loading...topWebSocketUtil.js");
// 			});
				
			topWebSocketUtil.setWssUrl("wss://localhost:9203/", 
		            ValidateLegalURL_Callback_Both);
		}
	}

	// 底下必須
	//------------------------------------------------------------------------------
	// 底下必須有，這是第一關
	// 等到 ValidateLegalURL_Callback 回呼方法被呼叫後，
	// 再經由 topWebSocketUtil.invokeRpcDispatcher 呼叫元件方法
	//------------------------------------------------------------------------------
	// Name: ValidateLegalURL_Callback
	function ValidateLegalURL_Callback_Both(rpcStatus, rpcReturn) {	
		try {
			//------------------------------------------------------------------
			topWebSocketUtil.tryRpcStatus(rpcStatus);
			//------------------------------------------------------------------
			
			// 未偵測到元件(未安裝或關閉)
			if (rpcReturn == "E_Send_11_OnError_1006") {
        		hasAskDownload = true;
        		$("#winstatus").text("Not Installed");
        		$("#macstatus").text("Not Installed");
    		
        	} // 安裝完成,檢查更新
        	else if (rpcReturn =="E000"||rpcReturn == null){
        		rpcName = "GetVersion";
        		topWebSocketUtil.invokeRpcDispatcher(GetVersion_Callback,rpcName);
        		
        		// GetVersion_Callback
        		function GetVersion_Callback(rpcStatus,rpcReturn){
        			if(window.console){console.log("GetVersion_Callback...");}
        			try{
        				topWebSocketUtil.tryRpcStatus(rpcStatus);
        				
        				var currentVersion = rpcReturn;
        				if(window.console){console.log("currentVersion: " + currentVersion);}
        				if(window.console){console.log("newVersion: " + comVersion.NoneIEReaderVersion);}
        				
        				var currentVersionNO = parseInt(currentVersion.replace(/\./g,""));
                        var newVersionNO;
                        //依系統取得版本號20210312因元件版本號不同修正
                        if(platformNew.indexOf("Win") > -1){
                            newVersionNO = parseInt(comVersion.NoneIEReaderVersion.replace(/\./g,""));
                        }
                        else{
                            newVersionNO = parseInt(comVersion.MacReaderVersion.replace(/\./g,""));
                        }         				
        				if(window.console){console.log("currentVersionNO="+currentVersionNO);}
        				if(window.console){console.log("newVersionNO="+newVersionNO);}
        				
        				// 元件版本需要更新
        				if(currentVersionNO < newVersionNO){
        					if(platformNew.indexOf("Win") > -1){
	        					$("#winstatus").text("Version needs to be updated");
	        	        		$("#macstatus").text("Not Installed");
        					}
        					else{
        						$("#winstatus").text("Not Installed");
	        	        		$("#macstatus").text("Version needs to be updated");
        					}
        				}
        				else{
        					if(platformNew.indexOf("Win") > -1){
	        	        		$("#winstatus").text("It has been installed");
	        	        		$("#macstatus").text("Not Installed");
        					}
        					else{
        						$("#winstatus").text("Not Installed");
	        	        		$("#macstatus").text("It has been installed");
        					}
        	        	}
        			} catch (exception){
        				if(window.console){console.log("exception="+exception);}
        				//alert("<spring:message code= "LB.X1655" />");
						errorBlock(
								null, 
								null,
								["<spring:message code= "LB.X1655" />"], 
								'<spring:message code= "LB.Quit" />', 
								null
							);
        			}
        		}
        	}
				
		} catch (exception) {
		        alert("exception: " + exception);
		}
	}
</script>
<body>
   	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	<!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><a href="#">Component download</a></li>
		</ol>
	</nav>
	
	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">
		<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2>
					Component download
				</h2>
				<form id="formId" method="post" action="">
					<div class="card-block shadow-box terms-pup-blcok">
	                    <div class="col-12 tab-content">
	                        <div class="ttb-input-block tab-pane fade show active" id="nav-trans-1" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <!-- Q1 -->
	                                    <div id="winos" class="ttb-pup-block" role="tab" style="display:none">
	                                        <a role="button" class="ttb-pup-title collapsed d-block" data-toggle="collapse" href="#popup-1" aria-expanded="true" aria-controls="popup-1">
	                                            <div class="row">
	                                                <span class="col-1">Q1</span>
	                                                <div class="col-11 row">
	                                                    <span class="col-12">General online banking, online ATM and electronic signature component download</span>
	                                                </div>
	                                            </div>
	                                        </a>
	                                        <div id="popup-1" class="ttb-pup-collapse collapse" role="tabpanel">
	                                            <div class="ttb-pup-body">
	                                                <ul class="ttb-pup-list">
	                                                    <li>
	                                                        <p>Windows platform</p>
	                                                        <table class="ttb-pup-table">
	                                                            <thead>
	                                                                <tr>
	                                                                    <th>Support browser</th>
	                                                                    <th>Browser component</th>
	                                                                    <th>Installation status</th>
	                                                                    <th>Browser component download</th>
	                                                                    <th>Installation instruction manual</th>
	                                                                </tr>
	                                                            <tbody>
	                                                                <tr>
	                                                                    <td>IE、Edge、Chrome、FireFox</td>
	                                                                    <td>2-in-1 component</td>
	                                                                    <td><font id="winstatus">Checking...</font></td>
	                                                                    <td><input type="button" id="win" class="component-download-link" value="Click to download" /></td>
	                                                                    <td><a style="color: #007bff" download href="${__ctx}/component/operation_manual/NBComponentOperateBook.pdf">Click to download</a></td>
	                                                                </tr>
	                                                            </tbody>
	                                                        </table>
	                                                    </li>
	                                                    <li>
	                                                        <p>Notices</p>
	                                                        <ol style="list-style-type: circle; margin-left: 1rem;">
	                                                            <li><p>If you have installed an AntiVirus software or Firewall, be sure to enable the permission of installation browser components in the software.</p>
	                                                            </li>
	                                                            <li><p>If you have installed a firewall, please use Port 9203 for the chip financial card component and electronic signature component.</p>
	                                                            </li>
	                                                            <li><p>The installation order of browsers and browser components: Download and install the browser >> Download the browser components >> Install the browser components</p>
	                                                            </li>
	                                                        </ol>
	                                                    </li>
	                                                </ul>
	                                            </div>
	                                        </div>
	                                    </div>
	                                    <!-- Q1 -->
	                                    <div id="macos" class="ttb-pup-block" role="tab" style="display:none">
	                                        <a role="button" class="ttb-pup-title collapsed d-block" data-toggle="collapse" href="#popup-2" aria-expanded="true" aria-controls="popup-2">
	                                            <div class="row">
	                                                <span class="col-1">Q1</span>
	                                                <div class="col-11 row">
	                                                    <span class="col-12">General online banking, online ATM and electronic signature component download</span>
	                                                </div>
	                                            </div>
	                                        </a>
	                                        <div id="popup-2" class="ttb-pup-collapse collapse" role="tabpanel">
	                                            <div class="ttb-pup-body">
	                                                <ul class="ttb-pup-list">
	                                                    <li>
	                                                        <p>Mac platform</p>
	                                                        <table class="ttb-pup-table">
	                                                            <thead>
	                                                                <tr>
	                                                                    <th>Support browser</th>
	                                                                    <th>Browser component</th>
	                                                                    <th>Installation status</th>
	                                                                    <th>Browser component download</th>
	                                                                    <th>Installation instruction manual</th>
	                                                                </tr>
	                                                            <tbody>
	                                                                <tr>
	                                                                    <td>Safari、Chrome、FireFox</td>
	                                                                    <td>2-in-1 componentt</td>
	                                                                    <td><font id="macstatus">Checking...</font></td>
	                                                                    <td><input type="button" id="mac" class="component-download-link" value="Click to download" /></td>
	                                                                    <td><a style="color: #007bff" download href="${__ctx}/component/operation_manual/NBComponentOperateBook.pdf">Click to download</a></td>
	                                                                </tr>
	                                                            </tbody>
	                                                        </table>
	                                                    </li>
	                                                    <li>
	                                                        <p>Notices</p>
	                                                        <ol style="list-style-type: circle; margin-left: 1rem;">
	                                                            <li><p>If you have installed an AntiVirus software or Firewall, be sure to enable the permission of installation browser components in the software.</p>
	                                                            </li>
	                                                            <li><p>If you have installed a firewall, please use Port 9203 for the chip financial card component and electronic signature component.</p>
	                                                            </li>
	                                                            <li><p>The installation order of browsers and browser components: Download and install the browser >> Download the browser components >> Install the browser components</p>
	                                                            </li>
	                                                        </ol>
	                                                    </li>
	                                                </ul>
	                                            </div>
	                                        </div>
	                                    </div>
	                                    <!-- Q2 -->
	                                    <div class="ttb-pup-block" role="tab">
	                                        <a role="button" class="ttb-pup-title collapsed d-block" data-toggle="collapse" href="#popup-4" aria-expanded="true" aria-controls="popup-4">
	                                            <div class="row">
	                                                <span class="col-1">Q2</span>
	                                                <div class="col-10 row">
	                                                    <span class="col-12">Natural person certificate component download</span>
	                                                </div>
	                                            </div>
	                                        </a>
	                                        <div id="popup-4" class="ttb-pup-collapse collapse" role="tabpanel">
	                                            <div class="ttb-pup-body">
	                                                <ul class="ttb-pup-list">
	                                                    <li>
	                                                        <p><a style="color: #007bff" href="${__ctx}/component/windows/natural/TBBankPkcs11ATL.exe">Click to download</a></p>
	                                                    </li>
	                                                    <li>
	                                                        <p>Notices</p>
	                                                        <ol style="list-style-type: circle; margin-left: 1rem;">
	                                                            <li><p>This component is only available for the Windows platform.</p>
	                                                            </li>
	                                                        </ol>
	                                                    </li>
	                                                </ul>
	                                            </div>
	                                        </div>
	                                    </div>
	
	                                </div>
	                            </div>
	                        </div>
	                        <input type="BUTTON" class="ttb-button btn-flat-gray" value="Back" name="CMBACK" id="CMBACK">
	                    </div>
	                </div>
				</form>
				<form id="componentForm" action="${__ctx}/COMPONENT/component_download" method="post">
					<input type="hidden" name="componentPath" id="componentPath" />
					<input type="hidden" name="trancode" value="ComponentDownload" />
				</form>	
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>
