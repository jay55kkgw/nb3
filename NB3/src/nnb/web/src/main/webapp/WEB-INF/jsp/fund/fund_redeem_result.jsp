<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript">

function eb(){
	confirmType = 2;
	var debitstatus = $('input[name=debitstatus]:checked').val();
	if(debitstatus == "terminate"){
		errorBlock(
				null,
				null,
				['<spring:message code= "LB.Confirm025" />'], 
				'<spring:message code= "LB.Confirm" />', 
				'<spring:message code= "LB.X0318" />'
			);
	}else{
		$('#error-block').hide();
	}
}
var confirmType=0;
$("#errorBtn1").click(function(){
	if(confirmType == 1){
		setTimeout("eb()",20);
	}
	else if(confirmType == 2){
		setTimeout("fundStopPayAjax()",20);
	}
	else {
		$('#error-block').hide();
	}
});
$("#errorBtn2").click(function(){
	if(confirmType == 2){
		//上一步
		confirmType = 1;
		errorBlock(
					null,
					null,
					['<label class="radio-block" for="debitstatus1"><spring:message code="LB.Confirm030" /><input type="radio" name="debitstatus" id="debitstatus1" value="terminate" checked/><span class="ttb-radio"></span></label><br><br><label class="radio-block" for="debitstatus2"><spring:message code="LB.Confirm031" /><input type="radio" name="debitstatus" id="debitstatus2" value="notterminate" /><span class="ttb-radio"></span></label>'],
					'<spring:message code= "LB.Confirm" />', 
					null
				);
	}else{
		$('#error-block').hide();
	}
});
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	
	var stopFlag = "${bs.data.STOPFLAG}";
	var BILLSENDMODE = "${bs.data.BILLSENDMODE}";
	
	//當全部贖回時，須詢問使用者是否執行終止扣款
	if(BILLSENDMODE == "1" && stopFlag == "Y"){
		//if(true){
		confirmType = 1;
		errorBlock(
					null,
					null,
					 ['<label class="radio-block" for="debitstatus1"><spring:message code="LB.Confirm030" /><input type="radio" name="debitstatus" id="debitstatus1" value="terminate" checked/><span class="ttb-radio"></span></label><br><br><label class="radio-block" for="debitstatus2"><spring:message code="LB.Confirm031" /><input type="radio" name="debitstatus" id="debitstatus2" value="notterminate" /><span class="ttb-radio"></span></label>'],
					'<spring:message code= "LB.Confirm" />', 
					null
				);
	}
	//if(true){
// 		var answer = window.confirm("<spring:message code= "LB.Confirm024" />");
// 		if(answer){
// 			if(window.confirm("<spring:message code= "LB.Confirm025" />"))
// 				fundStopPayAjax();
// 		}	
	
	
	$("#backButton").click(function(){
		$("#formID").attr("action","${__ctx}/FUND/QUERY/profitloss_balance");
		$("#formID").submit();
	});
	$("#printButton").click(function(){
		var params = {
			"jspTemplateName":"fund_redeem_result_print",
			"jspTitle":"<spring:message code= "LB.W0929" />",
			"CMQTIME":"${bs.data.CMQTIME}",
			"hiddenCUSIDN":"${bs.data.hiddenCUSIDN}",
			"hiddenNAME":"${bs.data.hiddenNAME}",
			"hiddenCDNO":"${bs.data.hiddenCDNO}",
			"TRANSCODE":"${bs.data.TRANSCODE}",
			"FUNDLNAME":"${bs.data.FUNDLNAME}",
			"BILLSENDMODEChinese":"${bs.data.BILLSENDMODEChinese}",
			"UNITFormat":"${bs.data.UNITFormat}",
			"FUNDACN":"${bs.data.FUNDACN}",
			"TRADEDATEFormat":"${bs.data.TRADEDATEFormat}",
			"ADCCYNAME":"${bs.data.ADCCYNAME}",
			"FUNDAMTFormat":"${bs.data.FUNDAMTFormat}"
		};
		openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
	});
});
function fundStopPayAjax(){
	var URI = "${__ctx}/FUND/REDEEM/fundStopPayAjax";
	var rqData = {CDNO:"${bs.data.CDNO}",TRANSCODE:"${bs.data.TRANSCODE}", SKIP_P:"${bs.data.SKIP_P}"};
	fstop.getServerDataEx(URI,rqData,false,fundStopPayAjaxFinish);
}
function fundStopPayAjaxFinish(data){
	confirmType = 0;
	if(data.result == true){
		var bsData = $.parseJSON(data.data);
		if(bsData.RESPCOD == "0000"){
			//alert("<spring:message code= "LB.Alert117" />");
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert117' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
			);
		}
		else{
			//alert('<spring:message code= "LB.Alert118-1" />/r/n<spring:message code= "LB.Alert118-2" />' + bsData.RESPCOD);
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert118-1' />","/r","/n","<spring:message code= 'LB.Alert118-2' />" + bsData.RESPCOD], 
					'<spring:message code= "LB.Quit" />', 
					null
			);
		}
	}
	else{
		//alert("<spring:message code= "LB.Alert119" />");
		errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.Alert119' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
		);
	}
}
</script>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金    -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0901" /></li>
    <!-- 基金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1064" /></li>
    <!-- 贖回交易     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1133" /></li>
		</ol>
	</nav>

	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.W0929"/></h2>
				<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
					<form id="formID" method="post">
						<input type="hidden" name="ADOPID" value="C024"/>
					<div class="main-content-block row">
						<div class="col-12 tab-content">
						<div class="ttb-input-block">
						    <div class="ttb-message">
                                <span><spring:message code="LB.X0412" /></span>
                            </div>
                            	
								<!-- 交易時間 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code="LB.Trading_time"/></h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>${bs.data.CMQTIME}</span>
										</div>
									</span>
								</div>
								<!-- 身分證字號/統一編號 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code="LB.Id_no"/></h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>${bs.data.hiddenCUSIDN}</span>
										</div>
									</span>
								</div>
								<!-- 姓名 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code="LB.Name"/></h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>${bs.data.hiddenNAME}</span>
										</div>
									</span>
								</div>
								<!-- 信託號碼 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code="LB.W1111"/></h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>${bs.data.hiddenCDNO}</span>
										</div>
									</span>
								</div>
								<!-- 基金名稱 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code="LB.W0025"/></h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>（${bs.data.TRANSCODE}）${bs.data.FUNDLNAME}</span>
										</div>
									</span>
								</div>
								<!-- 贖回方式 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code="LB.W1140"/></h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>${bs.data.BILLSENDMODEChinese}</span>
										</div>
									</span>
								</div>
								<!-- 贖回單位數 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code="LB.W0979"/></h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>${bs.data.UNITFormat}</span>
										</div>
									</span>
								</div>
								<!-- 入帳帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code="LB.W0135"/></h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>${bs.data.FUNDACN}</span>
										</div>
									</span>
								</div>
								<!-- 生效日期 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code="LB.W1080"/></h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>${bs.data.TRADEDATEFormat}</span>
										</div>
									</span>
								</div>
								<!-- 贖回信託金額 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label><h4><spring:message code="LB.W0978"/></h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span>${bs.data.ADCCYNAME}${bs.data.FUNDAMTFormat}</span>
										</div>
									</span>
								</div>
							</div>
<!-- 							<table class="table" data-toggle-column="first"> -->
<!-- 								<tr> -->
<%-- 									<td><spring:message code="LB.Trading_time"/></td> --%>
<%-- 								 	<td colspan="5">${bs.data.CMQTIME}</td> --%>
<!-- 								</tr> -->
<!-- 								<tr> -->
<%-- 									<td><spring:message code="LB.Id_no"/></td> --%>
<%-- 								 	<td colspan="5">${bs.data.hiddenCUSIDN}</td> --%>
<!-- 								</tr> -->
<!-- 								<tr> -->
<%-- 									<td><spring:message code="LB.Name"/></td> --%>
<%-- 								 	<td colspan="5">${bs.data.hiddenNAME}</td> --%>
<!-- 								</tr> -->
<!-- 								<tr> -->
<!--         							<td>信託號碼</td> -->
<%-- 									<td colspan="5">${bs.data.hiddenCREDITNO}</td> --%>
<!-- 								</tr> -->
<!-- 								<tr> -->
<!--         							<td>基金名稱</td> -->
<%-- 									<td colspan="5">（${bs.data.TRANSCODE}）${bs.data.FUNDLNAME}</td>             --%>
<!-- 								</tr> -->
<!-- 								<tr> -->
<!-- 									<td>贖回方式</td> -->
<%-- 									<td colspan="5">${bs.data.BILLSENDMODEChinese}</td> --%>
<!-- 								</tr> -->
<!-- 								<tr> -->
<!-- 									<td>贖回單位數</td> -->
<%-- 									<td colspan="5">${bs.data.UNITFormat}</td> --%>
<!-- 								</tr> -->
<!-- 								<tr> -->
<!-- 									<td>入帳帳號</td> -->
<%-- 									<td colspan="5">${bs.data.FUNDACN}</td> --%>
<!-- 								</tr> -->
<!-- 								<tr> -->
<!-- 									<td>生效日期</td> -->
<%-- 									<td colspan="5">${bs.data.TRADEDATEFormat}</td> --%>
<!-- 								</tr> -->
<!-- 								<tr>  -->
<!-- 									<td>贖回信託金額</td> -->
<!-- 									<td colspan="5"> -->
<%-- 										${bs.data.ADCCYNAME}${bs.data.FUNDAMTFormat} --%>
<!-- 									</td> -->
<!-- 								</tr> -->
<!-- 							</table> -->
<!-- 							<div> -->
<!-- 								<p style="text-align:left;"> -->
<%-- 									<b><spring:message code="LB.X1234" />：</b><br/> --%>
<%-- 	             								<spring:message code="LB.Fund_Redeem_Data_P3_D1"/> --%>
<!-- 								</p> -->
<!-- 							</div> -->

								<input type="button" id="backButton" value="<spring:message code="LB.X0413" />" class="ttb-button btn-flat-gray"/>
								<input type="button" id="printButton" value="<spring:message code="LB.Print"/>" class="ttb-button btn-flat-orange"/>

							</div>
						</div>
					</form>
					<div class="text-left">
					
						<ol class="list-decimal text-left description-list">
							<p><spring:message code="LB.Description_of_page"/></p>
							<li><span><spring:message code="LB.X1234" />：<br>
								<ol>
									<li style="text-indent:-43px;">（1）<spring:message code="LB.Fund_Redeem_Data_P3_D1"/></span></li>
								</ol>
								</span>
							</li>
									
						</ol>
					</div>
				
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
<c:if test="${showDialog == 'true'}">
	<%@ include file="../index/txncssslog.jsp"%>
	</c:if>
</html>