<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<body>
	<p style="text-align: center;">
		<strong>特定金錢信託投資後收型基金投資說明書暨特別約定事項-境內基金</strong>
	</p>
	<p>
		本特別約定事項係委託人與臺灣中小企業銀行簽訂之「特定金錢信託投資國內外有價證券信託約定書」之特別約定條款，<strong><font color="red">委託人同意投資證券投資信託股份有限公司(下稱投信公司)所發行之後收型基金(柏瑞N級別、瀚亞S級別、群益N級別、安聯N級別、路博邁N級別、鋒裕匯理N級別、台新N級別、富蘭克林華美N級別、野村N級別、第一金N級別、聯博N級別、瑞銀N級別)，並遵守下列約定，</font></strong>下列<strong><font color="red">事項</font></strong>若有更新或未規定之事項，則依上開之約定書及基金公開說明書之規定辦理。
	</p>
	<div class="ttb-result-list terms">
		<ul>
			<li>一、申購</li>
			<ol>
				<li data-num="1、">以單筆投資方式辦理。</li>
				<li data-num="2、">申購手續費：委託人於申購時無須繳交任何申購手續費。</li>
				<li data-num="3、">每筆最低投資金額：</li>
				<li>
				<table class="terms-table">
					<tr>
						<th></th>
						<th><p>新台幣</p></th>
						<th><p>美元</p></th>
						<th><p>歐元</p></th>
						<th><p>澳幣</p></th>
						<th><p>南非幣</p></th>
						<th><p>人民幣</p></th>
					</tr>
					<tr>
						<th><p>每筆最低投資金額</p></th>
						<th>120,000 元</th>
						<th>5,000元</th>
						<th>5,000元</th>
						<th>5,000元</th>
						<th>50,000元</th>
						<th>20,000元</th>
					</tr>
				</table>
				</li>
			</ol>
			<li>二、轉換</li>
			<ol>
				<li data-num="1、"><strong><font color="red">後收型基金</font></strong>於轉換時僅接受每筆全部基金單位數一次轉換。</li>
				<li data-num="2、"><strong><font color="red">後收型基金</font></strong>僅能轉換至同一系列之其他同類型及同幣別基金。<br>
				 <strong><font color="red">(如: 甲投信公司A基金人民幣N級別轉甲投信公司B基金人民幣N級別)</font></strong>
				</li>
				<li data-num="3、">轉換手續費：委託人申請轉換至同一基金系列其他同類型之基金(N級別轉N級別)，
				<strong><font color="red">投信</font></strong>公司之轉換手續費依各基金公開說明書規定收取，另受託人將收取轉換手續費每筆新台幣 500 元，
				<strong><font color="red">倘嗣後本行調降手續費時，則依本行規定辦理。</font></strong></li>
			</ol>
			<li>三、贖回</li>
			<ol>
				<li data-num="1、"><strong><font color="red">後收型基金</font></strong>於贖回時僅接受每筆全部基金單位數一次贖回。</li>
				<li data-num="2、">遞延銷售手續費（CDSC：Contingent Deferred Sale Charge）<br>
					<strong><font color="red">
					按每受益權單位申購日發行價格或贖回日單位淨資產價值孰低者，乘以買回單位數，再依持有期間乘以對應之遞延銷售手續費率，由投信公司自贖回淨資產價值中扣收，遞延銷售手續費率如下：
					</font></strong>
				</li>
				<li>
				<table class="terms-table" style="text-align: center;">
					<tbody>
						<tr>
							<td rowspan="2" style="vertical-align: middle;"> 
								<p>
									<strong><font color="red">持有期間</font></strong>
								</p>
							</td>
							<td colspan="2">
								<p>
									<strong><font color="red">遞延銷售手續費率</font></strong>
								</p>
							</td>
							<td style="display: none;"></td>
						</tr>
						<tr>
							<td>
								<p>
									<strong><font color="red">安聯N級別</font></strong>
								</p>
							</td>
							<td>
								<p>
									<strong><font color="red">其他後收型基金</font></strong>
								</p>
							</td>
						</tr>
						<tr>
							<td>
								<p>持有期間未滿一年者</p>
							</td>
							<td>
								2％
							</td>
							<td>
								3%
							</td>
						</tr>
						<tr>
							<td>
								<p>持有期間一年以上而未滿二年者</p>
							</td>
							<td>
								1.5％
							</td>
							<td>
								2%
							</td>
						</tr>
						<tr>
							<td>
								<p>持有期間二年以上而未滿三年者</p>
							</td>
							<td>
								1％
							</td>
							<td>
								1%
							</td>
						</tr>
						<tr>
							<td>
								<p>持有期間三年以上者</p>
							</td>
							<td>
								0%
							</td>
							<td>
								0%
							</td>
						</tr>
					</tbody>
				</table>
				</li>
			</ol>
			<li>四、基金分銷費用（Distribution Fee）：無。</li>
			<li>五、短線交易費用：<br>
				<strong><font color="red">
				委託人持有基金未滿短線交易天期者，除遞延銷售手續費外，另應依公開說明書短線交易規範，按短線交易費率於贖回淨資產價值扣收短線交易費用。
				</font></strong>
			</li>
			<li>六、特約事項：<br>
				<strong><font color="red">
				委託人已審閱各該手續費後收型境內基金公開說明書及交易文件相關內容，且充分明瞭手續費後收型境內基金之交易特性及風險，願同意接受所有交易條件及承擔一切投資風險，並瞭解投資最大可能損失為原始本金。
				</font></strong>
			</li>
		</ul>
</body>
</html>