<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ include file="../__import_js.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
	$(document).ready(function() {
		window.print();
	});
</script>
</head>
<body class="bodymargin watermark"
	style="-webkit-print-color-adjust: exact">
	<br />
	<br />
	<div style="text-align: center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br />
	<div style="text-align: center">
		<font style="font-weight: bold; font-size: 1.2em">${jspTitle}</font>
	</div>
	<br />
	<div class="ttb-message">
		<c:if test="${STATUS !='0'}">
			<p style="color: red; font-weight: bold">${STATUS}</p>
		</c:if>
		<c:if test="${STATUS =='0'}">
			<p style="color: red; font-weight: bold">
				<spring:message code="LB.Active_successful" />
			</p>
		</c:if>
	</div>
	<table class="print">
		<tr>
			<td style="text-align: center"><spring:message
					code="LB.Trading_time" /></td>
			<td style="text-align: center;">${CMQTIME}</td>
		</tr>
		<tr>
			<td style="text-align: center"><spring:message
					code="LB.Credit_card_number" /></td>
			<td style="text-align: center">${CARDNUM}</td>
		</tr>
		<tr>
			<td style="text-align: center"><spring:message
					code="LB.Card_validity" /></td>
			<td style="text-align: center">${EXP_DATE}</td>
		</tr>
	</table>
	<br />
	<c:if test="${STATUS !='0'}">
		<ol class="description-list list-decimal">
		<p><spring:message code="LB.Description_of_page" /></p><!-- 說明 -->
						<li><spring:message code="LB.Activate_P3_D1" /></li>
				</ol>
	</c:if>
	<br />
	<br />
</body>
</html>