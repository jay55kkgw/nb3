<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function(){
			window.print();
		});
	</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br/><br/>
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
	<br/><br/><br/>
	<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
	<br/><br/><br/>
	<div> 
		<div id="step-bar">
			<ul>
				<li class="finished">注意事項與權益</li>
				<li class="finished">開戶資料</li>
				<li class="finished">確認資料</li>
				<li class="active">完成預約</li>
			</ul>
		</div>
	
		<!-- 資料 -->
		<!-- 表格區塊 -->
		<div class="ttb-input-block">
			<div class="ttb-message">
				<p>完成預約</p>
			</div>
			
			<div class="text-left">
				<p>親愛的客戶您好：
					謝謝您完成『預約開立存款戶』申請作業流程，請親自攜帶
					<span style="color:#FF8000">
						印鑑、身分證、第二證件 (健保卡、駕照等)及供開戶起存的現金 (至少1000元)
					</span>
					，於營業時間至指定分行辦理開戶。為節省您開戶作業時間，建議您可列印本預約成功頁面。
				</p>
				
				<c:if test="${STAEDYN.equals('Y')}">
					<p>提醒您若申辦為起家金帳戶
						<span style="color:#FF8000">『本行備有專屬起家金悠遊Debit卡、請至往來分行服務台申辦。』</span>
					</p>
				</c:if>

				<p>提醒您！在
					<span style="color:#FF8000">
						『${DUEDT}(${WEEK})』
					</span>
					前未來辦理開戶，本行將取消您的預約開戶資料。 若有任何問題，您可撥電話至您所指定的服務分行：
					<span style="color:#FF8000">
						${BRHNAME}
					</span>
					(${BRHTEL}) 詢問，我們將竭誠為您服務。
				</p>

				<p>祝您事業順心，萬事如意！</p>
			</div>
		</div>
		
	</div>
	
</body>
</html>