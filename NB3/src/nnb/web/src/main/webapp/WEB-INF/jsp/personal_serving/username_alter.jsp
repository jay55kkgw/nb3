<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>	
</head>
 <body>

	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 使用者名稱變更     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Change_User_Name" /></li>
		</ol>
	</nav>

 	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
	<main class="col-12"> 
	<!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<h2><spring:message code="LB.Change_User_Name" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>						
			<form method="post" id="formId" action="${__ctx}/PERSONAL/SERVING/username_alter_result">
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
							
							<!-- 使用者名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
										<h4>
											<spring:message code="LB.User_name" />
										</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text" value="${userId}" class="text-input" readonly disabled>
										<input type="hidden" id="OLDUID" name="OLDUID" value="${userId}" class="text-input">
									</div>
								</span>
							</div>
							<!-- 新使用者名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
										<h4>
											<spring:message code="LB.New_user_name" />
										</h4>
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<input type="text" name = "NEWUID" id = "NEWUID"  maxlength="16" class="text-input  validate[required, newcolumn[ACCT, OLDUID, NEWUID],custom[onlyLetterNumber]] ">
										<br>
										<span class="input-unit"><spring:message code="LB.N9422_new_user_name_description" /></span>
									</div>
								</span>
							</div>
							<!-- 確認新使用者名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.Confirm_new_user_name" />
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text" name = "RENEWUID" id="RENEWUID"  maxlength="16" class="text-input  validate[required, recolumn[ACCT, OLDUID, NEWUID, RENEWUID], custom[onlyLetterNumber]] ">
										<br>
										<span class="input-unit"><spring:message code="LB.Please_Enter_New_User_Name_again" /></span>
									</div>
								</span>
							</div>
							<!--交易機制  SSL 密碼 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.Transaction_security_mechanism" />
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block" for="CMSSL">
								       		<input type="radio" name="FGTXWAY" id="CMSSL" value="0" checked /><spring:message code="LB.SSL_password" />
                                            <span class="ttb-radio"></span>
								       	</label>
								    </div>
									<div class="ttb-input">
										<input type="password" name = "CMPASSWORD" id="CMPASSWORD"  maxlength="8" class="text-input validate[required, pwvd[PW, CMPASSWORD], custom[onlyLetterNumber]]">
										<input type="hidden" name = "PINNEW" id="PINNEW" value="">
									</div>
								</span>
							</div>
						</div>
						<input id="reset" name="reset" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" />	
						<input id="pageshow" name="pageshow" type="submit" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />						
					</div>
				</div>
			</form>
			<div class="text-left">
				<ol class="list-decimal description-list">
					<p><spring:message code="LB.Description_of_page" /></p>
					<li><spring:message code="LB.Username_alter_P1_D1" /></li>
					<li><spring:message code="LB.Username_alter_P1_D2" /></li>
					<li><spring:message code="LB.Username_alter_P1_D3" /></li>
					<li><spring:message code="LB.Username_alter_P1_D4" /></li>
				</ol>
			</div>
		</section>
		<!-- 		main-content END -->
	</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
	<!--   Js function -->
    <script type="text/JavaScript">
		$(document).ready(function() {
			init();
		});
    

	    function init(){
	    	$("#formId").validationEngine({binded:false,promptPosition: "inline" });
	    	$('#formId').submit(function(e){			
					console.log("submit~~");
					if(!$('#formId').validationEngine('validate')){
			        	e.preventDefault();
		 			}else{
						$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
		 				$("#formId").validationEngine('detach');
		 				initBlockUI();
		 			}		
			});
	    }	
 	</script>
</body>
</html>
 