<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="page7" class="card-detail-block">
	<div class="card-center-block">
		<!-- Q1 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-71" aria-expanded="true"
				aria-controls="popup1-71">
				<div class="row">
					<span class="col-1">Q1</span>
					<div class="col-11">
						<span>Why do you need to pay the voucher fee when applying for an electronic voucher, and how long is the voucher valid?</span>
					</div>
				</div>
			</a>
			<div id="popup1-71" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>The online banking electronic signature transaction uses the financial XML certificate of TWCA, and it must pay TWCA's voucher fee. The current annual fee for the certificate is NT$150 for the individual and NT$900 for the legal person. The validity period is one year.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q2 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-72" aria-expanded="true"
				aria-controls="popup1-72">
				<div class="row">
					<span class="col-1">Q2</span>
					<div class="col-11">
						<span>In addition to the annual fee payable for the use of electronic signature transactions, what are the costs associated with the voucher?</span>
					</div>
				</div>
			</a>
			<div id="popup1-72" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>According to the financial XML voucher operation, the use of financial XML voucher requires to use the storage of voucher vehicle and public and private secret keys. The "voucher vehicle" is NT$600 per piece. If the "voucher vehicle" is not damaged, it can be unlimited duration use.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q3 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-73" aria-expanded="true"
				aria-controls="popup1-73">
				<div class="row">
					<span class="col-1">Q3</span>
					<div class="col-11">
						<span>Online bank transfer fees and related offers?</span>
					</div>
				</div>
			</a>
			<div id="popup1-73" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div style="overflow: auto">
								<table class="question-table">
									<thead>
										<tr>
											<th colspan="4">Service items</th>
											<th>Charges standard</th>
											<th>Remarks</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th rowspan="4">NTD</th>
											<th colspan="3">Self-Transfer</th>
											<td>Free of charge</td>
											<td></td>
										</tr>
										<tr>
											<th rowspan="3">Interbank Transfer</th>
											<th colspan="2">Below (inclusive)NTD500</th>
											<td>NTD 10/each transaction<br>(every day per account  first fee is free)</td>
											<td class="white-spacing text-left">The number of unused coupons on the day is not accumulated to the next day</td>
										</tr>
										<tr>
											<th colspan="2">NTD 501 ~ 1,000(inclusive)</th>
											<td>NTD 10/each transaction</td>
											<td></td>
										</tr>
										<tr>
											<th colspan="2">NTD 1,000 or more</th>
											<td>NTD 15/each transaction</td>
											<td></td>
										</tr>
										<tr>
											<th rowspan="3">Tax payment</th>
											<th colspan="3">Tax payment</th>
											<td>Free of charge</td>
											<td></td>
										</tr>
										<tr>
											<th rowspan="2">Payment</th>
											<th colspan="2">The Bank's collector</th>
											<td>Free of charge</td>
											<td rowspan="2" class="white-spacing text-left">Payment item contains: electricity fee, water fee in Taipei City, water fee in Taiwan Province, labor insurance fee, health insurance fee, national pension insurance fee, new labor retirement reserve, credit card fee of the Bank, Xinxin gas fee, tuition fee, and other expenses.</td>
										</tr>
										<tr>
											<th colspan="2">Inter-Bank payment</th>
											<td>NTD 15/each transaction</td>
										</tr>
										<tr>
											<th rowspan="28">Foreign exchange</th>
											<th rowspan="7">Buying & Selling<br>foreign currency<br>(Purchase & Sale)</th>
											<th colspan="2">USD</th>
											<td>Are offered at a listed exchange rate of 0.015</td>
											<td rowspan="7" class="white-spacing text-left">The above exchange rate discounts shall not be better than the cost exchange rate of the Bank's business units.</td>
										</tr>
										<tr>
											<th colspan="2">AUD</th>
											<td>Are offered at a listed exchange rate of 0.015</td>
										</tr>
										<tr>
											<th colspan="2">EUR</th>
											<td>Are offered at a listed exchange rate of 0.015</td>
										</tr>
										<tr>
											<th colspan="2">NZD</th>
											<td>Are offered at a listed exchange rate of 0.01</td>
										</tr>
										<tr>
											<th colspan="2">ZAR</th>
											<td>Are offered at a listed exchange rate of 0.006</td>
										</tr>
										<tr>
											<th colspan="2">HKD</th>
											<td>Are offered at a listed exchange rate of 0.002</td>
										</tr>
										<tr>
											<th colspan="2">JPY</th>
											<td>Are offered at a listed exchange rate of 0.0002</td>
										</tr>
										<tr>
											<th colspan="3">Comprehensive deposit</th>
											<td class="white-spacing text-left">Each currency shall be added at a rate of 0.05% according to the Bank's interest rates</td>
											<td class="white-spacing text-left">
												<ol class="list-decimal">
													<li>Including foreign exchange comprehensive deposits and their automatic transfer cases</li>
													<li>The above interest rate discounts shall not be better than the cost interest rate of the Bank's business units.</li>
												</ol>
											</td>
										</tr>
										<tr>
											<th rowspan="2">Self-transfer</th>
											<th colspan="2">Handling fee</th>
											<td>Free of charge</td>
											<td class="white-spacing text-left">Domestic Banking Unit(DBU)and Offshore Banking Unit(OBU) accounts exchanged is remitted as outward remittances, the rates are detailed as foreign exchange outward remittances.</td>
										</tr>
										<tr>
											<th colspan="2">Transfer fee</th>
											<td>Free of charge<br>(Transfer foreign currency of different account names)</td>
											<td></td>
										</tr>
										<tr>
											<th rowspan="8">Outward remittance<br>(DBU/Domestic Banking Unit)</th>
											<th colspan="2">Handling fee</th>
											<td>Calculated according to 0.05%<br>of the remittance amount</td>
											<td class="white-spacing text-left">The minimum fee equivalent is NTD 100, and the maximum charge is NTD 800.</td>
										</tr>
										<tr>
											<th colspan="2">Post and telecommunications charges</th>
											<td>Equivalence NTD 300/each transaction</td>
											<td class="white-spacing text-left">In addition, according to the actual number of power generation, the telegraph fee is charged.</td>
										</tr>
										<tr>
											<th rowspan="6" class="white-spacing text-left">Foreign fees</th>
											<th colspan="3" class="white-spacing text-left">If there are fee, please choose "OUR", and each foreign fee is equivalent to the "deductible debit currency":</th>
										</tr>
										<tr>
											<th>EUR remittance</th>
											<td>According to the <br>remittance amount of 0.1%</td>
											<td>The minimum is charge EUR 30</td>
										</tr>
										<tr>
											<th>JPY remittance</th>
											<td>According to the <br>remittance amount of 0.05%</td>
											<td>The minimum is charge JPY 5,000</td>
										</tr>
										<tr>
											<th>GBP remittance</th>
											<td>According to the <br>remittance amount of 0.1%</td>
											<td>The minimum is charge GBP 25</td>
										</tr>
										<tr>
											<th>HKD remittance</th>
											<td>HKD 300/each transaction</td>
											<td></td>
										</tr>
										<tr>
											<th>Other currency remittance</th>
											<td>Equivalence USD 35/each transaction</td>
											<td></td>
										</tr>
										<tr>
											<th rowspan="8">Outward remittance<br>(OBU/Offshore Banking Unit)</th>
											<th colspan="2">Handling fee</th>
											<td>According to the <br>remittance amount of 0.05%</td>
											<td class="white-spacing text-left">The minimum fee equivalent is USD 10, and the maximum charge is USD 40。</td>
										</tr>
										<tr>
											<th colspan="2">Post and telecommunications charges</th>
											<td>Equivalence USD 15/each transaction</td>
											<td class="white-spacing text-left">In addition, according to the actual number of power generation, the telegraph fee is charged.</td>
										</tr>
										<tr>
											<th rowspan="6">Foreign fees</th>
											<th colspan="3" class="white-spacing text-left">If there are fee, please choose "OUR", and each foreign fee is equivalent to the "deductible debit currency":</th>
										</tr>
										<tr>
											<th>EUR remittance</th>
											<td>According to the <br>remittance amount of 0.1%</td>
											<td>The minimum is charge EUR 40</td>
										</tr>
										<tr>
											<th>JPY remittance</th>
											<td>According to the <br>remittance amount of 0.05%</td>
											<td>The minimum is charge JPY 5,000</td>
										</tr>
										<tr>
											<th>GBP remittance</th>
											<td>According to the <br>remittance amount of 0.1%</td>
											<td>The minimum is charge GBP 25</td>
										</tr>
										<tr>
											<th>HKD remittance</th>
											<td>HKD 300/each transaction</td>
											<td></td>
										</tr>
										<tr>
											<th>Other currency remittance</th>
											<td>Equivalence USD 35/each transaction</td>
											<td></td>
										</tr>
										<tr>
											<th>Inward Remittance/<br>Online payment<br>(DBU/Domestic Banking Unit)</th>
											<th colspan="2">Handling fee</th>
											<td>Calculated according to <br>the deposit amount of 0.05%</td>
											<td class="white-spacing text-left">the deposit amount of NTD 200, the highest charge equivalent NTD 800.</td>
										</tr>
										<tr>
											<th>Inward Remittance/<br>Online payment<br>(OBU/Offshore Banking Unit)</th>
											<th colspan="2">Handling fee</th>
											<td>Calculated according to <br>the deposit amount of 0.05%</td>
											<td class="white-spacing text-left">the deposit amount of USD 10, the highest charge equivalent USD 40.</td>
										</tr>
										<tr>
											<th rowspan="2">Fund</th>
											<th colspan="3">Domestic</th>
											<td>Are 45% off</td>
											<td></td>
										</tr>
										<tr>
											<th colspan="3">Foreign</th>
											<td>Are 50% off</td>
											<td></td>
										</tr>
									</tbody>
								</table>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>