<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust:exact">
	<br/><br/>
	<div style="text-align:center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/>
	</div>
	<br/><br/><br/>
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<table class="print">
		<tr>
			<td><spring:message code="LB.Trading_time"/></td>
		 	<td>${CMQTIME}</td>
		</tr>
		<tr>
			<td><spring:message code="LB.Id_no"/></td>
		 	<td>${hiddenCUSIDN}</td>
		</tr>
		<tr>
			<td><spring:message code="LB.Name"/></td>
		 	<td>${hiddenNAME}</td>
		</tr>
		<tr>
  			<td><spring:message code= "LB.W1111" /></td>
			<td>${hiddenCDNO}</td>
		</tr>
		<tr>
  			<td><spring:message code= "LB.W0025" /></td>
			<td>（${TRANSCODE}）${fundName}</td>            
		</tr>
		<tr>
			<td><spring:message code= "LB.W0950" /></td>
			<td>（${INTRANSCODE}）${inFundName}</td>         
		</tr>
		<tr>
			<td><spring:message code= "LB.W1115" /></td>
			<td>${BILLSENDMODEChinese}</td>
		</tr>
		<tr>
			<td><spring:message code= "LB.W1122" /></td>
			<td>${UNITFormat}</td>
		</tr>
		<tr>
			<td><spring:message code= "LB.W1116" /></td>
			<td>${ADCCYNAME}${FUNDAMTFormat}</td>
		</tr>
		<tr>
			<td><spring:message code= "LB.W1123" /></td>
			<td><spring:message code= "LB.NTD" />${AMT3Integer}</td>
		</tr>
		<tr>
			<td><spring:message code= "LB.W1125" /></td>
			<td><spring:message code= "LB.NTD" />${FCA2Format}</td>
		</tr>
		<tr>
  			<td><spring:message code= "LB.W0974" /></td>
			<td><spring:message code= "LB.NTD" />${FCA1Format}</td>
		</tr>
		<tr>
			<td><spring:message code= "LB.D0168" /></td>
			<td>${OUTACN}</td>
		</tr>
		<tr>
			<td><spring:message code= "LB.W1079" /></td>
			<td><spring:message code= "LB.NTD" />${AMT5Format}</td>
		</tr>
		<tr>
			<td><spring:message code= "LB.W1143" /></td>
  			<td>${TRADEDATEFormat}</td>
		</tr>
	</table>
	<div>
		<p style="text-align:left">
			<spring:message code= "LB.X1508" /><br/>
			<spring:message code= "LB.X1509" />
		</p>
		<br/><br/>
		<p style="text-align:left">
			<b><spring:message code= "LB.X1234" />：</b><br/>
			<spring:message code= "LB.X1632" /><br/>
			<spring:message code= "LB.X1633" />
		</p>
	</div>
</body>
</html>