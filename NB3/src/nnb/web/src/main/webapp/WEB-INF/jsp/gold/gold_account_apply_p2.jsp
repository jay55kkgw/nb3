<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	    <script type="text/javascript">
        $(document).ready(function () {
            initFootable(); // 將.table變更為footable 
            init();
        });

        function init() {
	    	$("#pageshow").click(function(e){			
	        	initBlockUI();
				var action = '${__ctx}/GOLD/APPLY/gold_account_apply_p3';
				$("form").attr("action", action);
    			$("form").submit();
			});
	    	$("#back").click(function(e){			
	        	initBlockUI();
				var action = '${__ctx}/GOLD/APPLY/gold_account_apply';
				$("form").attr("action", action);
    			$("form").submit();
			});
        }

        function checkReadFlag()
        {
        	console.log($("#ReadFlag").prop('checked') && $("#ReadFlag1").prop('checked'));
            if ($("#ReadFlag").prop('checked') && $("#ReadFlag1").prop('checked'))    		
          	{
           		$("#pageshow").prop('disabled',false);
           		$("#pageshow").addClass("btn-flat-orange");
          	}
          	else
          	{
           		$("#pageshow").prop('disabled',true);
           		$("#pageshow").removeClass("btn-flat-orange");
       	  	}	
        }

    </script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 黃金存摺帳戶     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2273" /></li>
    <!-- 黃金存摺帳戶申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1655" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
				<!-- 線上申請黃金存摺帳戶 -->
					<spring:message code="LB.W1655"/>
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <div id="step-bar">
                    <ul>
                        <li class="active">注意事項與權益</li>
                        <li class="">開戶資料</li>
                        <li class="">確認資料</li>
                        <li class="">申請結果</li>
                    </ul>
                </div>
                <form id="formId" method="post">
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12 terms-block">
	                        <div class="ttb-message">
	                            <p>顧客權益</p>
	                        </div>
                       		<p class="form-description">請您審閱以下顧客權益與說明。</p>
                       		
	                        <div class="text-left">
	
	                            <ul class="ttb-result-list terms">
									<!-- 臺灣中小企業銀行股份有限公司履行個人資料保護法第8條第1項告知義務 -->
	                                <li data-num="">
	                                    <span class="input-subtitle subtitle-color"><spring:message code= "LB.X0163" /></span>
	                                    <div class="CN19-clause" style="width: 100%; height: 210px; margin: 20px auto; border: 1px solid #D7D7D7; border-radius: 6px;">
           									<%@ include file="../term/N201_1.jsp"%>
	                                    </div>
	                                </li>
									<!-- 臺灣中小企業銀行數位存款帳戶約定書 -->
	                                <li data-num="">
	                                    <span class="input-subtitle subtitle-color">臺灣中小企業銀行數位存款帳戶約定書</span>
	                                    <div class="CN19-clause" style="width: 100%; height: 210px; margin: 20px auto; border: 1px solid #D7D7D7; border-radius: 6px;">
           									<%@ include file="./gold_account_apply_N201_3.jsp"%>
	                                    </div>
	                                </li>
                           			<!--  臺灣中小企業銀行股份有限公司遵循FATCA法案蒐集、處理及利用個人資料告知事項-->
	                                <li data-num="">
	                                    <span class="input-subtitle subtitle-color"><spring:message code= "LB.X1205" /></span>
	                                    <div class="CN19-clause" style="width: 100%; height: 210px; margin: 20px auto; border: 1px solid #D7D7D7; border-radius: 6px;">
        									<%@ include file="../term/N201_2.jsp"%>
	                                    </div>
	                                </li>
									<!-- FATCA聲明書內容 -->
	                                <li data-num="">
	                                    <span class="input-subtitle subtitle-color"><spring:message code= "LB.X0202" /></span>
	                                    <div class="CN19-clause" style="width: 100%; height: 210px; margin: 20px auto; border: 1px solid #D7D7D7; border-radius: 6px;">
           									<%@ include file="../term/FATCA.jsp"%>
	                                    </div>
	                                </li>
	                            </ul>
                            	<span class="input-block">
                                	<div class="ttb-input">
			                            <label class="check-block"  for="ReadFlag1"> 
										<!-- 本人聲明僅為臺灣之稅務居民且非屬美國或其他國家稅務居民身分 -->
											<input type="checkbox" name="ReadFlag1" id="ReadFlag1" onclick="checkReadFlag()"><spring:message code= "LB.X1206" /><br>
											<span class="ttb-check"></span>
										</label>
                                	</div>
                                	<div class="ttb-input">
			                            <label class="check-block"  for="ReadFlag"> 
										<!-- 本人已詳細閱讀並清楚以上所有內容 -->
											<input type="checkbox" name="ReadFlag" id="ReadFlag" onclick="checkReadFlag()"><spring:message code="LB.D1064"/>
											<span class="ttb-check"></span>
										</label>
                                	</div>
                            	</span>
	                        </div>
	                        <!--button 區域 -->
                            <!-- 列印  -->
                            <!-- 不同意 -->
                            <input id="back" name="back" type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.D0041"/>" />
                            <!-- 同意 -->
                            <input id="pageshow" name="pageshow" type="button" class="ttb-button" value="<spring:message code="LB.D0097"/>" disabled/>
                        	<!-- button 區域 -->
                        </div>  
                    </div>
                </form>

	
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>