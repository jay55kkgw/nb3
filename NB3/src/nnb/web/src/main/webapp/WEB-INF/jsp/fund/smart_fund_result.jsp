<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript">
		// 初始化
		$(document).ready(function () {
			init();
		});

		function init() {
			//表單驗證
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
		}
		//設定通知服務
		function goNotification(){
			
			var action = '${__ctx}/PERSONAL/SERVING/notification_service';
			$("form").attr("action", action);
			initBlockUI();
			$("form").submit();
		}
		//設定我的EMAIL
		function goMailSetting(){
			var action = '${__ctx}/PERSONAL/SERVING/mail_setting';
			$("form").attr("action", action);
			initBlockUI();
			$("form").submit();
			
		}
		
		//列印
		function print() {
			var params = {
				"jspTemplateName": 'smart_fund_result_print',
				"MsgName":"<spring:message code= "LB.Change_successful" />",
				"jspTitle":'SMART FUND<spring:message code= "LB.X0418" />',
				"CMQTIME": '${smart_fund_result.data.CMQTIME}',
				"hideid_CUSIDN": '${smart_fund_result.data.hideid_CUSIDN}',
				"hideid_CDNO": '${smart_fund_result.data.hideid_CDNO}',
				"FUNDLNAME": '${smart_fund_result.data.FUNDLNAME}',
				"str_AC202": '${smart_fund_result.data.str_AC202}',
				"ADCCYNAME": '${smart_fund_result.data.ADCCYNAME}',
				"FUNDAMT": '${smart_fund_result.data.FUNDAMT}',
				"display_ACUCOUNT": '${smart_fund_result.data.display_ACUCOUNT}',
				"display_NAMT": '${smart_fund_result.data.display_NAMT}',
				"display_AMT": '${smart_fund_result.data.display_AMT}',
				"display_RTNRATE": '${smart_fund_result.data.display_RTNRATE}',
				"display_REFVALUE1": '${smart_fund_result.data.display_REFVALUE1}',
				"display_FXRATE": '${smart_fund_result.data.display_FXRATE}',
				"NOOPT":'${smart_fund_result.data.NOOPT}',
				"NSTOPPROF": '${smart_fund_result.data.NSTOPPROF}',
				"NSTOPLOSS": '${smart_fund_result.data.NSTOPLOSS}'
			};
			openWindowWithPost("${__ctx}/print",
				"height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",
				params);
		}
		
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金    -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0901" /></li>
    <!-- 基金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1064" /></li>
    <!-- SMART FUND自動贖回設定     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1184" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!--SMART FUND自動贖回設定 -->
				<h2>
					<spring:message code="LB.W1184" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form autocomplete="off" method="post" id="formId" action="">
						<c:set var="BaseResultData" value="${smart_fund_result.data}"></c:set>
					<!-- 表單顯示區 -->
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<div class="ttb-message">
									<span><spring:message code="LB.W1213" /></span>
									<c:if test="${BaseResultData.setNotify &&''== BaseResultData.NOOPT}">
										<span><spring:message code="LB.W1214" /></span>
									</c:if>
									
									<c:if test="${BaseResultData.setMyemail &&''== BaseResultData.NOOPT}">
										<samp><spring:message code="LB.W1215" /></samp>
									</c:if>
								</div>
								
								<!--交易時間-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.Trading_time" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${ BaseResultData.CMQTIME}</span>
										</div>
									</span>
								</div>

								<!--身分證字號/統一編號-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.Id_no" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${ BaseResultData.hideid_CUSIDN}</span>
										</div>
									</span>
								</div>


								<!--信託帳號-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0944" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${ BaseResultData.hideid_CDNO}</span>
										</div>
									</span>
								</div>

								<!--基金名稱-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0025" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${ BaseResultData.FUNDLNAME}</span>
										</div>
									</span>
								</div>

								<!--類別-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.D0973" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${ BaseResultData.str_AC202}</span>
										</div>
									</span>
								</div>

								<!--信託金額-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0026" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
											<!-- 幣別 -->
											${ BaseResultData.ADCCYNAME}
											<!--金額 -->
											${ BaseResultData.FUNDAMT}
											</span>
										</div>
									</span>
								</div>

								<!--單位數-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0027" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${ BaseResultData.display_ACUCOUNT}</span>
										</div>
									</span>
								</div>

								<!--未分配金額-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0921" /><spring:message code="LB.Deposit_amount_1" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${ BaseResultData.display_NAMT}</span>
										</div>
									</span>
								</div>

								<!--參考現值-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0915" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${ BaseResultData.display_AMT}</span>
										</div>
									</span>
								</div>

								<!--基準報酬率-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W1195" /><spring:message code="LB.W1196" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${ BaseResultData.display_RTNRATE}%</span>
										</div>
									</span>
								</div>

								<!--參考淨值-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W1198" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${ BaseResultData.display_REFVALUE1}</span>
										</div>
									</span>
								</div>
								<!--參考匯率-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0030" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${ BaseResultData.display_FXRATE}</span>
										</div>
									</span>
								</div>
								<!--自動贖回設定-->
									<div class="ttb-input-item row">
										<span class="input-title"><label>
												<h4><spring:message code="LB.X0418" /></h4>
											</label></span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													<c:if test="${BaseResultData.NOOPT == ''}">
														<spring:message code="LB.D0324" />
												  </c:if>
													  <c:if test="${BaseResultData.NOOPT != ''}">
														<spring:message code="LB.D0995" />
												  </c:if>
												</span>
											</div>
										</span>
									</div>
									   <c:if test="${BaseResultData.NOOPT == ''}">
								<!--自動贖回出場點設定(停利報酬率)-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W1210" />(<spring:message code="LB.W1211" />)</h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>+${ BaseResultData.NSTOPPROF}%</span>
										</div>
									</span>
								</div>
								<!--自動贖回出場點設定(停損報酬率)-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W1210" />(<spring:message code="LB.X0419" />)</h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>-${ BaseResultData.NSTOPLOSS}%</span>
										</div>
									</span>
								</div>
							  </c:if>
							</div>
							<!-- button -->
								<c:if test="${BaseResultData.setNotify &&''== BaseResultData.NOOPT}">
										<span><spring:message code="LB.W1214" /></span>
										<input class="ttb-button btn-flat-orange" type="button" value="<spring:message code="LB.X0421" />" name="BTN1" onClick=""/>
									</c:if>
									
									<c:if test="${BaseResultData.setMyemail &&''== BaseResultData.NOOPT}">
										<samp><spring:message code="LB.W1215" /></samp>
										<input class="ttb-button btn-flat-orange" type="button" value="<spring:message code="LB.X0422" />" name="BTN2" onClick=""/>
									</c:if>
								<!-- 列印 -->
								<input class="ttb-button btn-flat-orange" type="button" id="printbtn" onclick="print()" value="<spring:message code="LB.Print" />" />
							<!-- button -->
						</div>
					</div>
				</form>
			</section>
			<!-- main-content END -->
		</main>
	</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>