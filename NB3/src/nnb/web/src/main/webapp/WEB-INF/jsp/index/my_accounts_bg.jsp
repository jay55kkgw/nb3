<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="accts_loadingBox" style="display: none;">
	<div class="sk-fading-circle">
		<div class="sk-circle1 sk-circle" style="width: 200%; height: 200%"></div>
		<div class="sk-circle2 sk-circle" style="width: 200%; height: 200%"></div>
		<div class="sk-circle3 sk-circle" style="width: 200%; height: 200%"></div>
		<div class="sk-circle4 sk-circle" style="width: 200%; height: 200%"></div>
		<div class="sk-circle5 sk-circle" style="width: 200%; height: 200%"></div>
		<div class="sk-circle6 sk-circle" style="width: 200%; height: 200%"></div>
		<div class="sk-circle7 sk-circle" style="width: 200%; height: 200%"></div>
		<div class="sk-circle8 sk-circle" style="width: 200%; height: 200%"></div>
		<div class="sk-circle9 sk-circle" style="width: 200%; height: 200%"></div>
		<div class="sk-circle10 sk-circle" style="width: 200%; height: 200%"></div>
		<div class="sk-circle11 sk-circle" style="width: 200%; height: 200%"></div>
		<div class="sk-circle12 sk-circle" style="width: 200%; height: 200%"></div>
	</div>
</div>

 
<script type="text/JavaScript">

function showAcctsBlock(type) {
// 	$(".main-account .accts_loadingBox").remove();
	$(".accts_loadingBox").clone().appendTo("#" + type + " .main-account");
	$("#" + type + " .accts_loadingBox").show();
}

function unAcctsBlock(type) {
	$("#" + type + " .accts_loadingBox").remove();
}

</script>