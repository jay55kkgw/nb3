<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/JavaScript">
$(document).ready(function() {
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	
	if(${requestParam.CFU2 == "2"}){
		$("#CFU2TD").html("<spring:message code= "LB.D0107" />");
	}
	else{
		$("#CFU2TD").html("<spring:message code= "LB.X1640" />");
	}
	
	if(${requestParam.CN != ""}){
		<!-- Avoid Reflected XSS All Clients -->
		//$("#CNTD").html("${requestParam.CN}${requestParam.CARDDESC}");
		$("#CNTD").html("<c:out value='${fn:escapeXml(requestParam.CN)}' /><c:out value='${fn:escapeXml(requestParam.CARDDESC)}' />");
	}
	
	if(${requestParam.VARSTR3 == "11"}){
		$("#V3").prop("checked",true);
	}
	
	if(${requestParam.CPRIMJOBTYPE != ""}){
		<!-- Avoid Reflected XSS All Clients -->
		var CPRIMJOBTYPE = "<c:out value='${fn:escapeXml(requestParam.CPRIMJOBTYPE)}' />";
		if(CPRIMJOBTYPE == "1"){
			$("input[type=checkbox][name=CJT][value=1]").prop("checked",true);
		}
		else if(CPRIMJOBTYPE == "2"){
			$("input[type=checkbox][name=CJT][value=2]").prop("checked",true);
		}
		else if(CPRIMJOBTYPE == "3"){
			$("input[type=checkbox][name=CJT][value=3]").prop("checked",true);
		}
		else if(CPRIMJOBTYPE == "4"){
			$("input[type=checkbox][name=CJT][value=4]").prop("checked",true);
		}
	}
	
	<!-- Avoid Reflected XSS All Clients -->
	var MCASH = "<c:out value='${fn:escapeXml(requestParam.MCASH)}' />";
	if(MCASH == "1"){
		$("input[type=checkbox][name=C2][value=1]").prop("checked",true);
	}
	else{
		$("input[type=checkbox][name=C2][value=2]").prop("checked",true);
	}
	
	if(${requestParam.VARSTR2 == "2"}){
		$("#C5").prop("checked",true);
	}
	<!-- Avoid Reflected XSS All Clients -->
	var CNOTE1 = "<c:out value='${fn:escapeXml(requestParam.CNOTE1)}' />";
	if(CNOTE1 == "1"){
		$("input[type=checkbox][name=C3][value=1]").prop("checked",true);
	}
	else{
		$("input[type=checkbox][name=C3][value=2]").prop("checked",true);
	}
	<!-- Avoid Reflected XSS All Clients -->
	var CNOTE3 = "<c:out value='${fn:escapeXml(requestParam.CNOTE3)}' />";
	if(CNOTE3 == "1"){
		$("input[type=checkbox][name=C4][value=1]").prop("checked",true);
	}
	else{
		$("input[type=checkbox][name=C4][value=2]").prop("checked",true);
	}
	$("#printButton").click(function(){
// 		window.print();

<!-- Avoid Reflected XSS All Clients -->
		var params = {
			"jspTemplateName":"creditcard_apply_p6_print",
			"jspTitle":"<spring:message code= 'LB.D0022' />",
			"DATETIME":"<c:out value='${fn:escapeXml(requestParam.DATETIME)}' />",
			"CPRIMCHNAME":"<c:out value='${fn:escapeXml(requestParam.CPRIMCHNAME)}' />",
			"CPRIMENGNAME":"<c:out value='${fn:escapeXml(requestParam.CPRIMENGNAME)}' />",
			"CHENAMEChinese":"<c:out value='${fn:escapeXml(requestParam.CHENAMEChinese)}' />",
			"CUSNAME":"<c:out value='${fn:escapeXml(requestParam.CUSNAME)}' />",
			"ROMANAME":"<c:out value='${fn:escapeXml(requestParam.ROMANAME)}' />",
			"CTRYDESC":"<c:out value='${fn:escapeXml(requestParam.CTRYDESC)}' />",
			"CPRIMBIRTHDAY":"<c:out value='${fn:escapeXml(requestParam.CPRIMBIRTHDAYshow)}' />",
			"MPRIMEDUCATIONChinese":"<c:out value='${fn:escapeXml(requestParam.MPRIMEDUCATIONChinese)}' />",
			"CUSIDN":"<c:out value='${fn:escapeXml(requestParam.CUSIDN)}' />",
			"CHDATEY":"<c:out value='${fn:escapeXml(requestParam.CHDATEY)}' />",
			"CHDATEM":"<c:out value='${fn:escapeXml(requestParam.CHDATEM)}' />",
			"CHDATED":"<c:out value='${fn:escapeXml(requestParam.CHDATED)}' />",
			"CHANCITY":"<c:out value='${fn:escapeXml(requestParam.CHANCITY)}' />",
			"CHANTYPEChinese":"<c:out value='${fn:escapeXml(requestParam.CHANTYPEChinese)}' />",
			"MPRIMMARRIAGEChinese":"<c:out value='${fn:escapeXml(requestParam.MPRIMMARRIAGEChinese)}' />",
			"CPRIMADDR2":"<c:out value='${fn:escapeXml(requestParam.CPRIMADDR2)}' />",
			"CPRIMHOMETELNO2A":"<c:out value='${fn:escapeXml(requestParam.CPRIMHOMETELNO2A)}' />",
			"CPRIMHOMETELNO2B":"<c:out value='${fn:escapeXml(requestParam.CPRIMHOMETELNO2B)}' />",
			"CPRIMADDR":"<c:out value='${fn:escapeXml(requestParam.CPRIMADDR)}' />",
			"CPRIMHOMETELNO1A":"<c:out value='${fn:escapeXml(requestParam.CPRIMHOMETELNO1A)}' />",
			"CPRIMHOMETELNO1B":"<c:out value='${fn:escapeXml(requestParam.CPRIMHOMETELNO1B)}' />",
			"MPRIMADDR1CONDChinese":"<c:out value='${fn:escapeXml(requestParam.MPRIMADDR1CONDChinese)}' />",
			"CPRIMCELLULANO1":"<c:out value='${fn:escapeXml(requestParam.CPRIMCELLULANO1)}' />",
			"MBILLTOChinese":"<c:out value='${fn:escapeXml(requestParam.MBILLTOChinese)}' />",
			"MCARDTOChinese":"<c:out value='${fn:escapeXml(requestParam.MCARDTOChinese)}' />",
			"CPRIMEMAIL":"<c:out value='${fn:escapeXml(requestParam.CPRIMEMAIL)}' />",
			"PAYMONEY":"<c:out value='${fn:escapeXml(requestParam.PAYMONEY)}' />",
			"FDRSGSTAFF":"<c:out value='${fn:escapeXml(requestParam.FDRSGSTAFF)}' />",
			"BANKERNO":"<c:out value='${fn:escapeXml(requestParam.BANKERNO)}' />",
			"MEMO":"<c:out value='${fn:escapeXml(requestParam.MEMO)}' />",
			"CPRIMCOMPANY":"<c:out value='${fn:escapeXml(requestParam.CPRIMCOMPANY)}' />",
			"CPRIMJOBTITLEChinese":"<c:out value='${fn:escapeXml(requestParam.CPRIMJOBTITLEChinese)}' />",
			"CPRIMJOBTYPEChinese":"<c:out value='${fn:escapeXml(requestParam.CPRIMJOBTYPEChinese)}' />",
			"CPRIMSALARY":"<c:out value='${fn:escapeXml(requestParam.CPRIMSALARY)}' />",
			"WORKYEARS":"<c:out value='${fn:escapeXml(requestParam.WORKYEARS)}' />",
			"WORKMONTHS":"<c:out value='${fn:escapeXml(requestParam.WORKMONTHS)}' />",
			"CPRIMADDR3":"<c:out value='${fn:escapeXml(requestParam.CPRIMADDR3)}' />",
			"CPRIMOFFICETELNO1A":"<c:out value='${fn:escapeXml(requestParam.CPRIMOFFICETELNO1A)}' />",
			"CPRIMOFFICETELNO1B":"<c:out value='${fn:escapeXml(requestParam.CPRIMOFFICETELNO1B)}' />",
			"CPRIMOFFICETELNO1C":"<c:out value='${fn:escapeXml(requestParam.CPRIMOFFICETELNO1C)}' />",
			"C2":"<c:out value='${fn:escapeXml(requestParam.MCASH)}' />",
			"C3":"<c:out value='${fn:escapeXml(requestParam.CNOTE1)}' />",
			"C4":"<c:out value='${fn:escapeXml(requestParam.CNOTE3)}' />",
			"C5":"<c:out value='${fn:escapeXml(requestParam.VARSTR2)}' />",
			"V3":"<c:out value='${fn:escapeXml(requestParam.VARSTR3)}' />",
			"CFU2TD":"<c:out value='${fn:escapeXml(requestParam.CFU2)}' />",
			"CNTD":"<c:out value='${fn:escapeXml(requestParam.CN)}' /><c:out value='${fn:escapeXml(requestParam.CARDDESC)}' />"
		};
		openWindowWithPost("${__ctx}/ONLINE/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
	});
	$("#pdfButton").click(function(){
		if('${transfer}' == 'en'){
			$("#templatePath").val("/pdfTemplate/Creditcard_Apply_EN.html");
		}
		else if('${transfer}' == 'zh'){
			$("#templatePath").val("/pdfTemplate/Creditcard_Apply_CN.html");
		}
		else{
			$("#templatePath").val("/pdfTemplate/Creditcard_Apply2.html");
		}
		$("#formID").attr("action","${__ctx}/CREDIT/APPLY/downloadCreditCardPDF");
		$("#formID").submit();
	});
	$("#na033Button").click(function(){
		window.open("${__ctx}/term/NA03_3.pdf");
	});
	$("#na036Button").click(function(){
		window.open("${__ctx}/term/NA03_6.pdf");
	});
	$("#na034Button").click(function(){
		window.open("${__ctx}/term/NA03_4.pdf");
	});
	$("#na035Button").click(function(){
		window.open("${__ctx}/term/NA03_5.pdf");
	});
	$("#backButton").click(function(){
		$("#formID").attr("action","${__ctx}/CREDIT/APPLY/apply_creditcard");
		$("#formID").submit();
	});
});
//選項
// function formReset() {
// 	if ($('#actionBar').val()=="pdfButton"){
// 		$("#formID").attr("action","${__ctx}/CREDIT/APPLY/downloadCreditCardPDF");
// 		$("#formID").submit();
//  	}else if ($('#actionBar').val()=="na033Button"){
// 		window.open("${__ctx}/term/NA03_3.pdf");
//  	}else if ($('#actionBar').val()=="na036Button"){
// 		window.open("${__ctx}/term/NA03_6.pdf");
//  	}else if ($('#actionBar').val()=="na034Button"){
// 		window.open("${__ctx}/term/NA03_4.pdf");
//  	}else if ($('#actionBar').val()=="na035Button"){
// 		window.open("${__ctx}/term/NA03_5.pdf");
//  	}
// }
</script>
</head>
<body>
<!-- header -->
<header>
	<%@ include file="../index/header_logout.jsp"%>
</header>	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 申請信用卡     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0666" /></li>
		</ol>
	</nav>

<!--左邊menu及登入資訊-->
<div class="content row">
<%-- 	<%@ include file="../index/menu.jsp"%> --%>
<!--快速選單及主頁內容-->
	<main class="col-12">
		<!--主頁內容-->
		<section id="main-content" class="container">
			<!--線上申請信用卡 -->
			<h2><spring:message code="LB.D0022" /></h2>
			<div id="step-bar">
				<ul>
					<li class="finished">信用卡選擇</li><!-- 信用卡 -->
					<li class="finished">身份驗證與權益</li><!-- 身份驗證與權益 -->
					<li class="finished"><spring:message code="LB.X1967" /></li><!-- 申請資料 -->
					<li class="finished"><spring:message code="LB.Confirm_data" /></li><!-- 確認資料 -->
					<li class="active"><spring:message code="LB.X1968" /></li><!-- 完成申請 -->
				</ul>
			</div>
<!-- 			<div class="print-block"> -->
<!-- 				<select class="minimal" id="actionBar" onchange="formReset()"> -->
<%-- 					<option value=""><spring:message code="LB.Downloads" /></option> --%>
<%-- 					<option value="pdfButton"><spring:message code="LB.X0845" /></option><!--下載申請書 --> --%>
<%-- 					<option value="na033Button"><spring:message code="LB.X0846" /></option><!--下載信用卡約定條款 --> --%>
<%-- 					<option value="na036Button"><spring:message code="LB.X0847" /></option><!--下載電子帳單約定條款 --> --%>
<%-- 					<c:if test="${requestParam.CARDMEMO == '1'}"> --%>
<%-- 						<option value="na034Button"><spring:message code="LB.X0848" /></option><!--下載悠遊聯名卡特別約定條款 --> --%>
<%-- 					</c:if> --%>
<%-- 					<c:if test="${requestParam.CARDMEMO == '2'}"> --%>
<%-- 						<option value="na035Button"><spring:message code="LB.X0849" /></option><!--下載一卡通聯名卡特別約定條款 --> --%>
<%-- 					</c:if> --%>
<!-- 				</select> -->
<!-- 			</div> -->
			<form method="post" id="formID">
				<input type="hidden" name="ADOPID" value="NA03"/>
<%-- 				<input type="hidden" name="CFU2" value="${requestParam.CFU2}"/><!-- 是否持有本行信用卡1否2是 --> --%>
				<input type="hidden" name="CFU2" value="<c:out value='${fn:escapeXml(requestParam.CFU2)}' />" />
<%-- 				<input type="hidden" name="FGTXWAY" value="${requestParam.FGTXWAY}"/> --%>
				<input type="hidden" name="FGTXWAY" value="<c:out value='${fn:escapeXml(requestParam.FGTXWAY)}' />"/>
<%-- 				<input type="hidden" name="CARDNAME" value="${requestParam.CARDNAME}"/> --%>
				<input type="hidden" name="CARDNAME" value="<c:out value='${fn:escapeXml(requestParam.CARDNAME)}' />"/>
<%-- 				<input type="hidden" name="CARDMEMO" value="${requestParam.CARDMEMO}"/> --%>
				<input type="hidden" name="CARDMEMO" value="<c:out value='${fn:escapeXml(requestParam.CARDMEMO)}' />"/>
<%-- 				<input type="hidden" name="CN" value="${requestParam.CN}"/> --%>
				<input type="hidden" name="CN" value="<c:out value='${fn:escapeXml(requestParam.CN)}' />"/>
<%-- 				<input type="hidden" name="CARDDESC" value="${requestParam.CARDDESC}"/> --%>
				<input type="hidden" name="CARDDESC" value="<c:out value='${fn:escapeXml(requestParam.CARDDESC)}' />"/>
<%-- 				<input type="hidden" name="VARSTR2" value="${requestParam.VARSTR2}"/><!-- 悠遊卡是否同意預設開啟flag --> --%>
				<input type="hidden" name="VARSTR2" value="<c:out value='${fn:escapeXml(requestParam.VARSTR2)}' />"/>
<%-- 				<input type="hidden" name="OLAGREEN1" value="${requestParam.OLAGREEN1}"/> --%>
				<input type="hidden" name="OLAGREEN1" value="<c:out value='${fn:escapeXml(requestParam.OLAGREEN1)}' />"/>
<%-- 				<input type="hidden" name="OLAGREEN2" value="${requestParam.OLAGREEN2}"/> --%>
				<input type="hidden" name="OLAGREEN2" value="<c:out value='${fn:escapeXml(requestParam.OLAGREEN2)}' />"/>
<%-- 				<input type="hidden" name="OLAGREEN3" value="${requestParam.OLAGREEN3}"/> --%>
				<input type="hidden" name="OLAGREEN3" value="<c:out value='${fn:escapeXml(requestParam.OLAGREEN3)}' />"/>
<%-- 				<input type="hidden" name="OLAGREEN4" value="${requestParam.OLAGREEN4}"/> --%>
			    <input type="hidden" name="OLAGREEN4" value="<c:out value='${fn:escapeXml(requestParam.OLAGREEN4)}' />"/>
			    
			    <input type="hidden" id="ECERT" name="ECERT"/><!-- 交易機制 -->
			    <input type="hidden" name="STATUS" value="0"/>
			    <input type="hidden" name="VERSION" value="10808"/>
			    
<%-- 			    <input type="hidden" name="IP" value="${requestParam.IP}"/> --%>
			    <input type="hidden" name="IP" value="<c:out value='${fn:escapeXml(requestParam.IP)}' />"/>
<%-- 			    <input type="hidden" name="BRANCHNAME" value="${requestParam.BRHNAME}"/> --%>
			    <input type="hidden" name="BRANCHNAME" value="<c:out value='${fn:escapeXml(requestParam.BRHNAME)}' />"/>
<%-- 			    <input type="hidden" name="CPRIMCHNAME" value="${requestParam.CPRIMCHNAME}"/> --%>
				<input type="hidden" name="CPRIMCHNAME" value="<c:out value='${fn:escapeXml(requestParam.CPRIMCHNAME)}' />"/>
<%-- 				<input type="hidden" name="CPRIMENGNAME" value="${requestParam.CPRIMENGNAME}"/> --%>
				<input type="hidden" name="CPRIMENGNAME" value="<c:out value='${fn:escapeXml(requestParam.CPRIMENGNAME)}' />"/>
<%-- 				<input type="hidden" name="CHENAME" value="${requestParam.CHENAME}"/> --%>
				<input type="hidden" name="CHENAME" value="<c:out value='${fn:escapeXml(requestParam.CHENAME)}' />"/>
<%-- 				<input type="hidden" name="CHENAMEChinese" value="${requestParam.CHENAMEChinese}"/> --%>
				<input type="hidden" name="CHENAMEChinese" value="<c:out value='${fn:escapeXml(requestParam.CHENAMEChinese)}' />"/>
<%-- 				<input type="hidden" name="CHENAMEpdf" value="${requestParam.CHENAMEpdf}"/> --%>
				<input type="hidden" name="CHENAMEpdf" value="<c:out value='${fn:escapeXml(requestParam.CHENAMEpdf)}' />"/>
<%-- 				<input type="hidden" name="CTRYDESC" value="${requestParam.CTRYDESC}"/> --%>
				<input type="hidden" name="CTRYDESC" value="<c:out value='${fn:escapeXml(requestParam.CTRYDESC)}' />"/>
<%-- 				<input type="hidden" name="CPRIMBIRTHDAY" value="${requestParam.CPRIMBIRTHDAY}"/> --%>
				<input type="hidden" name="CPRIMBIRTHDAY" value="<c:out value='${fn:escapeXml(requestParam.CPRIMBIRTHDAY)}' />"/>
<%-- 				<input type="hidden" name="CPRIMBIRTHDAYshow" value="${requestParam.CPRIMBIRTHDAYshow}"/> --%>
				<input type="hidden" name="CPRIMBIRTHDAYshow" value="<c:out value='${fn:escapeXml(requestParam.CPRIMBIRTHDAYshow)}' />"/>
<%-- 				<input type="hidden" name="MPRIMEDUCATION" value="${requestParam.MPRIMEDUCATION}"/> --%>
				<input type="hidden" name="MPRIMEDUCATION" value="<c:out value='${fn:escapeXml(requestParam.MPRIMEDUCATION)}' />"/>
<%-- 				<input type="hidden" name="CPRIMID" value="${requestParam.CUSIDN}"/> --%>
				<input type="hidden" name="CPRIMID" value="<c:out value='${fn:escapeXml(requestParam.CUSIDN)}' />"/>
<%-- 				<input type="hidden" name="CUSIDN" value="${requestParam.CUSIDN}"/> --%>
				<input type="hidden" name="CUSIDN" value="<c:out value='${fn:escapeXml(requestParam.CUSIDN)}' />"/>
<%-- 				<input type="hidden" name="MPRIMMARRIAGE" value="${requestParam.MPRIMMARRIAGE}"/> --%>
				<input type="hidden" name="MPRIMMARRIAGE" value="<c:out value='${fn:escapeXml(requestParam.MPRIMMARRIAGE)}' />"/>
<%-- 				<input type="hidden" name="CPRIMADDR2" value="${requestParam.CPRIMADDR2}"/> --%>
				<input type="hidden" name="CPRIMADDR2" value="<c:out value='${fn:escapeXml(requestParam.CPRIMADDR2)}' />"/>
<%-- 				<input type="hidden" name="CPRIMHOMETELNO2A" value="${requestParam.CPRIMHOMETELNO2A}"/> --%>
				<input type="hidden" name="CPRIMHOMETELNO2A" value="<c:out value='${fn:escapeXml(requestParam.CPRIMHOMETELNO2A)}' />"/>
<%-- 				<input type="hidden" name="CPRIMHOMETELNO2B" value="${requestParam.CPRIMHOMETELNO2B}"/> --%>
				<input type="hidden" name="CPRIMHOMETELNO2B" value="<c:out value='${fn:escapeXml(requestParam.CPRIMHOMETELNO2B)}' />"/>
<%-- 				<input type="hidden" name="CPRIMHOMETELNO2" value="${requestParam.CPRIMHOMETELNO2}"/> --%>
				<input type="hidden" name="CPRIMHOMETELNO2" value="<c:out value='${fn:escapeXml(requestParam.CPRIMHOMETELNO2)}' />"/>
<%-- 				<input type="hidden" name="CPRIMADDR" value="${requestParam.CPRIMADDR}"/> --%>
				<input type="hidden" name="CPRIMADDR" value="<c:out value='${fn:escapeXml(requestParam.CPRIMADDR)}' />"/>
<%-- 				<input type="hidden" name="CPRIMHOMETELNO1A" value="${requestParam.CPRIMHOMETELNO1A}"/> --%>
				<input type="hidden" name="CPRIMHOMETELNO1A" value="<c:out value='${fn:escapeXml(requestParam.CPRIMHOMETELNO1A)}' />"/>
<%-- 				<input type="hidden" name="CPRIMHOMETELNO1B" value="${requestParam.CPRIMHOMETELNO1B}"/> --%>
				<input type="hidden" name="CPRIMHOMETELNO1B" value="<c:out value='${fn:escapeXml(requestParam.CPRIMHOMETELNO1B)}' />"/>
<%-- 				<input type="hidden" name="CPRIMHOMETELNO" value="${requestParam.CPRIMHOMETELNO}"/> --%>
				<input type="hidden" name="CPRIMHOMETELNO" value="<c:out value='${fn:escapeXml(requestParam.CPRIMHOMETELNO)}' />"/>
<%-- 				<input type="hidden" name="MPRIMADDR1COND" value="${requestParam.MPRIMADDR1COND}"/> --%>
				<input type="hidden" name="MPRIMADDR1COND" value="<c:out value='${fn:escapeXml(requestParam.MPRIMADDR1COND)}' />"/>
<%-- 				<input type="hidden" name="CPRIMCELLULANO1" value="${requestParam.CPRIMCELLULANO1}"/> --%>
				<input type="hidden" name="CPRIMCELLULANO1" value="<c:out value='${fn:escapeXml(requestParam.CPRIMCELLULANO1)}' />"/>
<%-- 				<input type="hidden" name="MBILLTO" value="${requestParam.MBILLTO}"/> --%>
				<input type="hidden" name="MBILLTO" value="<c:out value='${fn:escapeXml(requestParam.MBILLTO)}' />"/>
<%-- 				<input type="hidden" name="MCARDTO" value="${requestParam.MCARDTO}"/> --%>
				<input type="hidden" name="MCARDTO" value="<c:out value='${fn:escapeXml(requestParam.MCARDTO)}' />"/>
<%-- 				<input type="hidden" name="CPRIMEMAIL" value="${requestParam.CPRIMEMAIL}"/> --%>
				<input type="hidden" name="CPRIMEMAIL" value="<c:out value='${fn:escapeXml(requestParam.CPRIMEMAIL)}' />"/>
<%-- 				<input type="hidden" name="VARSTR3" value="${requestParam.VARSTR3}"/> --%>
				<input type="hidden" name="VARSTR3" value="<c:out value='${fn:escapeXml(requestParam.VARSTR3)}' />"/>
<%-- 				<input type="hidden" name="FDRSGSTAFF" value="${requestParam.FDRSGSTAFF}"/> --%>
				<input type="hidden" name="FDRSGSTAFF" value="<c:out value='${fn:escapeXml(requestParam.FDRSGSTAFF)}' />"/>
<%-- 				<input type="hidden" name="BANKERNO" value="${requestParam.BANKERNO}"/> --%>
				<input type="hidden" name="BANKERNO" value="<c:out value='${fn:escapeXml(requestParam.BANKERNO)}' />"/>
<%-- 				<input type="hidden" name="MEMO" value="${requestParam.MEMO}"/> --%>
				<input type="hidden" name="MEMO" value="<c:out value='${fn:escapeXml(requestParam.MEMO)}' />"/>
<%-- 				<input type="hidden" name="CPRIMJOBTYPE" value="${requestParam.CPRIMJOBTYPE}"/> --%>
				<input type="hidden" name="CPRIMJOBTYPE" value="<c:out value='${fn:escapeXml(requestParam.CPRIMJOBTYPE)}' />"/>
<%-- 				<input type="hidden" name="CPRIMCOMPANY" value="${requestParam.CPRIMCOMPANY}"/> --%>
				<input type="hidden" name="CPRIMCOMPANY" value="<c:out value='${fn:escapeXml(requestParam.CPRIMCOMPANY)}' />"/>
<%-- 				<input type="hidden" name="CPRIMJOBTITLE" value="${requestParam.CPRIMJOBTITLE}"/> --%>
				<input type="hidden" name="CPRIMJOBTITLE" value="<c:out value='${fn:escapeXml(requestParam.CPRIMJOBTITLE)}' />"/>
<%-- 				<input type="hidden" name="CPRIMSALARY" value="${requestParam.CPRIMSALARY}"/> --%>
				<input type="hidden" name="CPRIMSALARY" value="<c:out value='${fn:escapeXml(requestParam.CPRIMSALARY)}' />"/>
<%-- 				<input type="hidden" name="WORKYEARS" value="${requestParam.WORKYEARS}"/> --%>
				<input type="hidden" name="WORKYEARS" value="<c:out value='${fn:escapeXml(requestParam.WORKYEARS)}' />"/>
<%-- 				<input type="hidden" name="WORKMONTHS" value="${requestParam.WORKMONTHS}"/> --%>
				<input type="hidden" name="WORKMONTHS" value="<c:out value='${fn:escapeXml(requestParam.WORKMONTHS)}' />"/>
<%-- 				<input type="hidden" name="CPRIMADDR3" value="${requestParam.CPRIMADDR3}"/> --%>
				<input type="hidden" name="CPRIMADDR3" value="<c:out value='${fn:escapeXml(requestParam.CPRIMADDR3)}' />"/>
<%-- 				<input type="hidden" name="CPRIMOFFICETELNO1A" value="${requestParam.CPRIMOFFICETELNO1A}"/> --%>
				<input type="hidden" name="CPRIMOFFICETELNO1A" value="<c:out value='${fn:escapeXml(requestParam.CPRIMOFFICETELNO1A)}' />"/>
<%-- 				<input type="hidden" name="CPRIMOFFICETELNO1B" value="${requestParam.CPRIMOFFICETELNO1B}"/> --%>
				<input type="hidden" name="CPRIMOFFICETELNO1B" value="<c:out value='${fn:escapeXml(requestParam.CPRIMOFFICETELNO1B)}' />"/>
<%-- 				<input type="hidden" name="CPRIMOFFICETELNO1C" value="${requestParam.CPRIMOFFICETELNO1C}"/> --%>
				<input type="hidden" name="CPRIMOFFICETELNO1C" value="<c:out value='${fn:escapeXml(requestParam.CPRIMOFFICETELNO1C)}' />"/>
<%-- 				<input type="hidden" name="CPRIMOFFICETELNO1" value="${requestParam.CPRIMOFFICETELNO1}"/> --%>
				<input type="hidden" name="CPRIMOFFICETELNO1" value="<c:out value='${fn:escapeXml(requestParam.CPRIMOFFICETELNO1)}' />"/>
<%-- 				<input type="hidden" name="MCASH" value="${requestParam.MCASH}"/> --%>
				<input type="hidden" name="MCASH" value="<c:out value='${fn:escapeXml(requestParam.MCASH)}' />"/>
<%-- 				<input type="hidden" name="CNOTE4" value="${requestParam.VARSTR2}"/> --%>
				<input type="hidden" name="CNOTE4" value="<c:out value='${fn:escapeXml(requestParam.VARSTR2)}' />"/>
<%-- 				<input type="hidden" name="CNOTE1" value="${requestParam.CNOTE1}"/> --%>
				<input type="hidden" name="CNOTE1" value="<c:out value='${fn:escapeXml(requestParam.CNOTE1)}' />"/>
<%-- 				<input type="hidden" name="CNOTE3" value="${requestParam.CNOTE3}"/> --%>
				<input type="hidden" name="CNOTE3" value="<c:out value='${fn:escapeXml(requestParam.CNOTE3)}' />"/>
<%-- 				<input type="hidden" name="oldcardowner" value="${requestParam.oldcardowner}"/> --%>
				<input type="hidden" name="oldcardowner" value="<c:out value='${fn:escapeXml(requestParam.oldcardowner)}' />"/>
<%-- 				<input type="hidden" name="BIRTHY" value="${requestParam.BIRTHY}"/> --%>
			  	<input type="hidden" name="BIRTHY" value="<c:out value='${fn:escapeXml(requestParam.BIRTHY)}' />"/>
<%-- 			  	<input type="hidden" name="BIRTHM" value="${requestParam.BIRTHM}"/> --%>
			  	<input type="hidden" name="BIRTHM" value="<c:out value='${fn:escapeXml(requestParam.BIRTHM)}' />"/>
<%-- 			  	<input type="hidden" name="BIRTHD" value="${requestParam.BIRTHD}"/> --%>
			  	<input type="hidden" name="BIRTHD" value="<c:out value='${fn:escapeXml(requestParam.BIRTHD)}' />"/>
<%-- 			  	<input type="hidden" name="CTTADR" value="${requestParam.CTTADR}"/> --%>
			  	<input type="hidden" name="CTTADR" value="<c:out value='${fn:escapeXml(requestParam.CTTADR)}' />"/>
<%-- 			  	<input type="hidden" name="PMTADR" value="${requestParam.PMTADR}"/> --%>
			  	<input type="hidden" name="PMTADR" value="<c:out value='${fn:escapeXml(requestParam.PMTADR)}' />"/>
<%-- 			  	<input type="hidden" name="MPRIMEDUCATIONChinese" value="${requestParam.MPRIMEDUCATIONChinese}"/> --%>
			  	<input type="hidden" name="MPRIMEDUCATIONChinese" value="<c:out value='${fn:escapeXml(requestParam.MPRIMEDUCATIONChinese)}' />"/>
<%-- 			  	<input type="hidden" name="MPRIMEDUCATIONpdf" value="${requestParam.MPRIMEDUCATIONpdf}"/> --%>
				<input type="hidden" name="MPRIMEDUCATIONpdf" value="<c:out value='${fn:escapeXml(requestParam.MPRIMEDUCATIONpdf)}' />"/>
<%-- 				<input type="hidden" name="MPRIMMARRIAGEChinese" value="${requestParam.MPRIMMARRIAGEChinese}"/> --%>
				<input type="hidden" name="MPRIMMARRIAGEChinese" value="<c:out value='${fn:escapeXml(requestParam.MPRIMMARRIAGEChinese)}' />"/>
<%-- 				<input type="hidden" name="MPRIMMARRIAGEpdf" value="${requestParam.MPRIMMARRIAGEpdf}"/> --%>
				<input type="hidden" name="MPRIMMARRIAGEpdf" value="<c:out value='${fn:escapeXml(requestParam.MPRIMMARRIAGEpdf)}' />"/>
<%-- 				<input type="hidden" name="MPRIMADDR1CONDChinese" value="${requestParam.MPRIMADDR1CONDChinese}"/> --%>
				<input type="hidden" name="MPRIMADDR1CONDChinese" value="<c:out value='${fn:escapeXml(requestParam.MPRIMADDR1CONDChinese)}' />"/>
<%-- 				<input type="hidden" name="MPRIMADDR1CONDpdf" value="${requestParam.MPRIMADDR1CONDpdf}"/> --%>
				<input type="hidden" name="MPRIMADDR1CONDpdf" value="<c:out value='${fn:escapeXml(requestParam.MPRIMADDR1CONDpdf)}' />"/>
<%-- 				<input type="hidden" name="MBILLTOChinese" value="${requestParam.MBILLTOChinese}"/> --%>
				<input type="hidden" name="MBILLTOChinese" value="<c:out value='${fn:escapeXml(requestParam.MBILLTOChinese)}' />"/>
<%-- 				<input type="hidden" name="MBILLTOpdf" value="${requestParam.MBILLTOpdf}"/> --%>
				<input type="hidden" name="MBILLTOpdf" value="<c:out value='${fn:escapeXml(requestParam.MBILLTOpdf)}' />"/>
<%-- 				<input type="hidden" name="MCARDTOChinese" value="${requestParam.MCARDTOChinese}"/> --%>
				<input type="hidden" name="MCARDTOChinese" value="<c:out value='${fn:escapeXml(requestParam.MCARDTOChinese)}' />"/>
<%-- 				<input type="hidden" name="MCARDTOpdf" value="${requestParam.MCARDTOpdf}"/> --%>
				<input type="hidden" name="MCARDTOpdf" value="<c:out value='${fn:escapeXml(requestParam.MCARDTOpdf)}' />"/>
<%-- 				<input type="hidden" name="CPRIMJOBTITLEChinese" value="${requestParam.CPRIMJOBTITLEChinese}"/> --%>
				<input type="hidden" name="CPRIMJOBTITLEChinese" value="<c:out value='${fn:escapeXml(requestParam.CPRIMJOBTITLEChinese)}' />"/>
<%-- 				<input type="hidden" name="CPRIMJOBTITLEpdf" value="${requestParam.CPRIMJOBTITLEpdf}"/> --%>
				<input type="hidden" name="CPRIMJOBTITLEpdf" value="<c:out value='${fn:escapeXml(requestParam.CPRIMJOBTITLEpdf)}' />"/>
<%-- 				<input type="hidden" name="COUNTRYNAME" value="${requestParam.COUNTRYNAME}"/> --%>
				<input type="hidden" name="COUNTRYNAME" value="<c:out value='${fn:escapeXml(requestParam.COUNTRYNAME)}' />"/>
<%-- 				<input type="hidden" name="CARDDESCpdf" value="${requestParam.CARDDESCpdf}"/> --%>
				<input type="hidden" name="CARDDESCpdf" value="<c:out value='${fn:escapeXml(requestParam.CARDDESCpdf)}' />"/>
				<input type="hidden" name="CUSNAME" value="<c:out value='${fn:escapeXml(requestParam.CUSNAME)}' />"/>
				<input type="hidden" name="ROMANAME" value="<c:out value='${fn:escapeXml(requestParam.ROMANAME)}' />"/>
				<input type="hidden" name="PAYMONEY" value="<c:out value='${fn:escapeXml(requestParam.PAYMONEY)}' />"/>
				<input type="hidden" name="CPRIMJOBTYPEChinese" value="<c:out value='${fn:escapeXml(requestParam.CPRIMJOBTYPEChinese)}' />"/>
				<input type="hidden" name="CPRIMJOBTYPEpdf" value="<c:out value='${fn:escapeXml(requestParam.CPRIMJOBTYPEpdf)}' />"/>
<%-- 				<input type="hidden" name="CHDATA" value="<c:out value='${fn:escapeXml(requestParam.CHDATA)}' />"/> --%>
				<input type="hidden" name="CHDATEY" value="<c:out value='${fn:escapeXml(requestParam.CHDATEY)}' />"/>
				<input type="hidden" name="CHDATEM" value="<c:out value='${fn:escapeXml(requestParam.CHDATEM)}' />"/>
				<input type="hidden" name="CHDATED" value="<c:out value='${fn:escapeXml(requestParam.CHDATED)}' />"/>
				<input type="hidden" name="CHANCITY" value="<c:out value='${fn:escapeXml(requestParam.CHANCITY)}' />"/>
				<input type="hidden" name="CHANTYPEChinese" value="<c:out value='${fn:escapeXml(requestParam.CHANTYPEChinese)}' />"/>
				<!--信用卡PDF會用到-->
				<input type="hidden" name="pdfName" value="<spring:message code= "LB.X1641" />"/>
				<input type="hidden" id="templatePath" name="templatePath" value=""/>
                <div class="main-content-block row">
					<div class="col-12">
						<div class="ttb-input-block">
							<div class="ttb-message">
								<!-- 完成申請 -->
								<p><spring:message code="LB.X1968" /></p>
							</div>
							<div class="text-left">
								<p><spring:message code="LB.X0152" />：</p>
<!-- 								謝謝您完成信用卡申請。申請後，審核約需10個工作天。本行將以E-mail通知信用卡之審核結果。 -->
<!-- 								若有任何問題，您可撥電話至本行信用卡服務專線詢問 (0800-01-7171 或 02-2357-7171)，我們將竭誠為您服務。 -->
<!-- 								祝您事業順心，萬事如意！ -->
								<p><spring:message code="LB.X2028" /></p>
								<p><spring:message code="LB.X2029" /></p>
								<p><spring:message code="LB.X2030" /></p>
							</div>
							<!--申請時間-->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.D1097" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span><c:out value='${fn:escapeXml(requestParam.DATETIME)}' /></span>
									</div>
								</span>
							</div>
							<!-- 申請卡別 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.X2031" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span><c:out value='${fn:escapeXml(requestParam.CN)}' /><c:out value='${fn:escapeXml(requestParam.CARDDESC)}' /></span>
									</div>
								</span>
							</div>
							<!--中文姓名-->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.D0049" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span><c:out value='${fn:escapeXml(requestParam.CPRIMCHNAME)}' /></span>
									</div>
								</span>
							</div>
							<!--卡片郵寄地址-->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.onlineapply_creditcard_cttadrtitle" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
											<c:if test="${result_data.data.MBILLTO =='1'}">
												<!-- 戶籍地址 -->
												【<spring:message code="LB.D0143" />】(${result_data.data.strZIP})${result_data.data.CPRIMADDR0}
											</c:if>
											<c:if test="${result_data.data.MBILLTO =='2'}">
												<!-- 居住地址 -->
												【<spring:message code="LB.D0063" />】(${result_data.data.strZIP})${result_data.data.CPRIMADDR0}
											</c:if>
											<c:if test="${result_data.data.MBILLTO =='3'}">
												<!-- 公司地址 -->
												【<spring:message code="LB.D0090" />】(${result_data.data.strZIP})${result_data.data.CPRIMADDR0}
											</c:if>
									</div>
								</span>
							</div>
							<!--下載申請書與約定條款-->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.X2032" /></h4>
									</label>
								</span>
								<span class="input-block multi-download-block">
									<div class="ttb-input">
<!-- 										申請書 -->
										<span><spring:message code="LB.X2033" /></span>
										<input type="button" class="ttb-sm-btn btn-flat-gray" id="pdfButton" name="pdfButton" value="<spring:message code='LB.X2037' />" />
									</div>
									<div class="ttb-input">
<!-- 										信用卡約定條款 -->
										<span><spring:message code="LB.X2034" /></span>
										<input type="button" class="ttb-sm-btn btn-flat-gray" id="na033Button" name="na033Button" value="<spring:message code='LB.X2037' />" />
									</div>
									<div class="ttb-input">
										<!-- 電子帳單約定條款 -->
										<span><spring:message code="LB.X0556" /></span>
										<input type="button" class="ttb-sm-btn btn-flat-gray" id="na036Button" name="na036Button" value="<spring:message code='LB.X2037' />" />
									</div>
									<c:if test="${requestParam.CARDMEMO == '1'}">
										<div class="ttb-input">
<!-- 											悠遊聯名卡特別約定條款  -->
											<span><spring:message code="LB.X2035" /></span> 
											<input type="button" class="ttb-sm-btn btn-flat-gray" id="na034Button" name="na034Button" value="<spring:message code='LB.X2037' />" />
										</div>
									</c:if>
									<c:if test="${requestParam.CARDMEMO == '2'}">
										<div class="ttb-input">
<!-- 											一卡通聯名卡特別約定條款  -->
											<span><spring:message code="LB.X2036" /></span> 
											<input type="button" class="ttb-sm-btn btn-flat-gray" id="na035Button" name="na035Button" value="<spring:message code='LB.X2037' />" />
										</div>
									</c:if>
								</span>
							</div>
						</div>
						<input type="button" id="backButton" value="<spring:message code="LB.X0956" />" class="ttb-button btn-flat-gray"/><!-- 回首頁 -->
						<input type="button" id="printButton" value="<spring:message code="LB.Print" />" class="ttb-button btn-flat-orange"/>
					</div>
				</div>
			</form>
		</section>
	</main>
</div>
<%@ include file="../index/footer.jsp"%>
</body>
</html>