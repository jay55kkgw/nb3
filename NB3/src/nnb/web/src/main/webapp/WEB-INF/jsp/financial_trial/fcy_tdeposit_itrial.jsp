<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	
	<script type="text/JavaScript">
	
		//小數點四捨五入
		function formatFloat(num, pos){
		  var size = Math.pow(10, pos);
		  return Math.round(num * size) / size;
		}
	
		function chkdata() {
			if ($("#principal").val() == "") {
				//alert("<spring:message code= "LB.Alert082" />");
				errorBlock(
							null, 
							null,
							["<spring:message code= "LB.Alert082" />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				$("#principal").focus();
				return false;
			}

			if (isNaN($("#principal").val())) {
				//alert("<spring:message code= "LB.Alert083" />");
				errorBlock(
							null, 
							null,
							["<spring:message code= "LB.Alert083" />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				$("#principal").focus();
				return false;
			}

			if ($("#period").val() == "") {
				//alert("<spring:message code= "LB.Alert084" />");
				errorBlock(
							null, 
							null,
							["<spring:message code= "LB.Alert084" />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				$("#period").focus();
				return false;
			}

			if (isNaN($("#period").val())) {
				//alert("<spring:message code= "LB.Alert085" />");
				errorBlock(
						null, 
						null,
						["<spring:message code= "LB.Alert085" />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
				$("#period").focus();
				return false;
			}

			if($("#yrate").val() == ""){
				//alert("<spring:message code= "LB.Alert086" />");
				errorBlock(
							null, 
							null,
							["<spring:message code= "LB.Alert086" />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				 $("#yrate").focus();
				return false;
			}

			  if(isNaN($("#yrate").val())){
				//alert("<spring:message code= "LB.Alert087" />");
				errorBlock(
							null, 
							null,
							["<spring:message code= "LB.Alert087" />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				$("#yrate").focus();
				return false;
			}

			return true;
		}

		function sum() {

			//本金
			var principal = validate_IntString($("#principal").val(), 0);

			//年利率
			var yrate = validate_IntString($("#yrate").val(), 1);

			//期間
			var period = validate_IntString($("#period").val(), 0);

			//月利率
			var mrate = yrate / 1200;

			//週利率
			var wrate = yrate / 5200;

			//利息
			var interest = 0;

			//期數TYPE
			pType =$("#pType").val();

			var bcheck = chkdata();

			if (bcheck == true) {
																			
				if (pType == "w") {
					interest = formatFloat(principal * wrate * period, 2);
					//整數四捨五入
					//interest=Math.round(principal*wrate*period);
				} else {
					//alert("尚未四捨五入 " +principal*mrate*period );
					var interest = principal * mrate * period;

					//有小數點
					if (interest.toString().indexOf(".") > 0) {
						var Str1 = interest.toString().indexOf(".");
						var Str2 = interest.toString().length;
						//小數點超過2位數，格式化
						if ((Str2 - Str1) > 3) {
							interest = formatFloat(interest, 2);
						}
					}

					//整數四捨五入
					//interest=Math.round(principal*mrate*period);
				}
				total = principal + interest;
				total = fstop.formatAmt(String(total));
				$("#result").val(total);
				$("#principal").val(principal); 
			}
		}
	</script>
</head>
 <body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 理財試算服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0087" /></li>
    <!-- 外匯定期存款利息試算     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0099" /></li>
		</ol>
	</nav>


	<!-- 快速選單及主頁內容 -->
	<!-- menu、登出窗格 --> 
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
		<main class="col-12">	
			<!--主頁內容  -->
		<section id="main-content" class="container">
			<!--<h2><spring:message code="LB.Change_User_Name" /></h2>--> 
			<h2><spring:message code="LB.X0099" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>						
			<form  id="formId">
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
						        <!-- 幣別-->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
										<h4><spring:message code="LB.Currency" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<select id="currency" name="currency"  class="custom-select select-input half-input">
										<!-- 美金 -->
										<option value="USD">
											<spring:message code="LB.USD_United_States_dollar" />
										</option>
										<!-- 澳幣 -->
										<option value="AUD">
											<spring:message code="LB.AUD_Australian_dollar" />
										</option>	
										<!-- 加拿大幣 -->
										<option value="CAD">
											<spring:message code="LB.CAD_Canadian_dollar" />
										</option>			
										<!-- 港幣 -->
										<option value="HKD">
											<spring:message code="LB.HKD_Hong_Kong_dollar" />
										</option>	
										<!-- 英鎊 -->
										<option value="GBP">
											<spring:message code="LB.GBP_Pound_sterling" />
										</option>	
    									<!-- 新加坡幣 -->
    									<option value="SGD">
											<spring:message code="LB.SGD_Singapore_dollar" />
										</option>	
    									<!-- 南非幣 -->
    									<option value="ZAR">
											<spring:message code="LB.ZAR_South_African_rand" />
										</option>												
    									<!-- 瑞典幣 -->
    									<option value="SEK">
											<spring:message code="LB.SEK_Swedish_krona" />
										</option>													
    									<!-- 瑞士法郎 -->
    									<option value="CHF">
											<spring:message code="LB.CHF_Swiss_franc" />
										</option>													
    									<!-- 日圓 -->
    									<option value="JPY">
											<spring:message code="LB.JPY_Japanese_Yen" />
										</option>													
										<!-- 泰幣 -->
										<option value="THB">
											<spring:message code="LB.THB_Thai_baht" />
										</option>		
										<!-- 歐元 -->
										<option value="EUR">
											<spring:message code="LB.EUR_Euro" />
										</option>	
										<!-- 紐幣 -->
										<option value="NZD">
											<spring:message code="LB.NZD_New_Zealand_dollar" />
										</option>	
										<!-- 人民幣 -->
										<option value="CNY">
											<spring:message code="LB.CNY_Renminbi" />
										</option>	    
										</select>
									</div>
								</span>
							</div>
							<!-- 您現在存入新臺幣-->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
										<!-- <h4><spring:message code="LB.User_name" /></h4> -->
										<h4><spring:message code="LB.X1152" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text"  name = "principal"  id = "principal"  maxlength="10"  size="10"  class="text-input  validate[required, newcolumn[ACCT, OLDUID, NEWUID],custom[onlyLetterNumber]] ">
										<span class="input-unit"><spring:message code="LB.Dollar" /></span>
									</div>
								</span>
							</div>
							<!-- 期數 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
										<!-- <h4><spring:message code="LB.User_name" /></h4> -->
									 <h4><spring:message code="LB.X0100" /></h4>
								</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<input id="period"  name="period"  type="text"  value=""  size="10"  maxlength="10"  class="text-input  validate[required, newcolumn[ACCT, OLDUID, NEWUID],custom[onlyLetterNumber]] ">
									</div>
									<div class="ttb-input">
										<select id="pType" name="pType"  class="custom-select select-input half-input">
											<option value="w"><spring:message code="LB.Week" /></option>
											<option value="m"><spring:message code="LB.Month" /></option>
										</select>
									</div>
								</span>								
							</div>
							<!-- 年利率 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
										<!-- <h4><spring:message code="LB.User_name" /></h4> -->
										<h4><spring:message code="LB.X0091" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text"  name = "yrate"  id = "yrate"  maxlength="10"  size="10"  class="text-input  validate[required, newcolumn[ACCT, OLDUID, NEWUID],custom[onlyLetterNumber]] ">
											<span class="input-unit">%</span>
										 <br> 
										 <span class="input-unit">
										 	「<a href="https://www.tbb.com.tw/web/guest/-84"  target="_blank"><spring:message code="LB.X0101" /></a>」
										 </span>
									</div>
								</span>
							</div>
							<!--到期可領本利和外幣金額-->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
										<!-- <h4><spring:message code="LB.User_name" /></h4> -->
										<h4><spring:message code="LB.X1153" /> </h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text"  name = "result"  id = "result"   size="20"  readonly="readonly" class="text-input  validate[required, newcolumn[ACCT, OLDUID, NEWUID],custom[onlyLetterNumber]] ">
										<span class="input-unit"><spring:message code="LB.Dollar" /></span>
									</div>
								</span>
							</div>
						</div>
						<!-- <input id="pageshow" name="pageshow" type="submit" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" /> -->
						<input id="btnReset" name="btnReset" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" />	
						<input id="btnCount" name="btnCount" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.X0094" />"  onclick="sum()" />							
					</div>
				</div>
				</form>
						<ol class="list-decimal description-list">
							<!--<spring:message code="LB.Username_alter_P1_D1" />-->
							<p><spring:message code="LB.Description_of_page" /></p>
							<spring:message code="LB.Fcy_Tdeposit_Itrial_P1_D1" />
						</ol>
		</section>
		<!-- 		main-content END -->
	</main>
	</div>
	<!-- content row END -->
    <%@ include file="../index/footer.jsp"%>
</body>
</html>
