<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js_u2.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>${jspTitle}</title>
  <script type="text/javascript">
    $(document).ready(function () {
      window.print();
    });
  </script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
  <br /><br />
  <div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif" /></div>
  <br /><br />
  <div style="text-align:center">
    <font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
  </div>
  <br/>
  <div style="text-align:center">
    ${MsgName}
  </div>
  <br />
     <table class="print">
    <!-- 交易時間 -->
    <tr>
      <td style="width:8em">
<!--         交易時間 -->
                      <spring:message code="LB.Trading_time" />
      </td>
      <td>${CMTXTIME} </td>
    </tr>
    <!-- 轉出帳號 -->
    <tr>
      <td class="ColorCell">
<!--         轉出帳號 -->
                <spring:message code="LB.Payers_account_no"/>
      </td>
      <td>${OUTACN}</td>
    </tr>
    <!-- 存單帳號  -->
    <tr>
      <td class="ColorCell">
<!--         存單帳號 -->
          <spring:message code="LB.Account_no"/>
      </td>
      <td> ${FDPACN}</td>
    </tr>
    <!-- 存單號碼 -->
    <tr>
      <td class="ColorCell">
<!--         存單號碼 -->
      <spring:message code="LB.Certificate_no" />
      </td>
      <td class="DataCell">
        ${FDPNUM}
      </td>
    </tr>
    <!-- 轉帳金額 -->
    <tr>
      <td class="ColorCell">
<!--         轉帳金額 -->
        <spring:message code="LB.Amount" />
      </td>
      <td>
          <!--           新台幣 -->
                            <spring:message code="LB.NTD" />
         ${AMOUNT }
<!--           元 -->
                            <spring:message code="LB.Dollar"/>
      </td>
    </tr>
    <!-- 已存入總期數 -->
    <tr>
      <td class="ColorCell">
<!--         已存入總期數 -->
        <spring:message code="LB.Total_number_of_periods_deposited" />
      </td>
      <td>
        ${TERMS}
      </td>
    </tr>
    <!-- 已存入總金額 -->
    <tr>
      <td class="ColorCell">
<!--         已存入總金額 -->
        <spring:message code="LB.Total_amount_deposited" />
      </td>
      <td>
<!--           新台幣 -->
                            <spring:message code="LB.NTD" />
         ${TOTAMT}
<!--           元 -->
                            <spring:message code="LB.Dollar"/>
      </td>
    </tr>
    <!-- 欠繳期數  -->
    <tr>
      <td class="ColorCell">
<!--         欠繳期數 -->
        <spring:message code="LB.Number_of_outstanding_periods" />
      </td>
      <td>
          ${RETERMS}
      </td>
    </tr>
    <!-- 轉出帳號帳戶餘額 -->
    <tr>
      <td class="ColorCell">
<!--         轉出帳號帳戶餘額 -->
        <spring:message code="LB.Payers_account_balance" />
      </td>
      <td>
<!--           新台幣 -->
          						 <spring:message code="LB.NTD" />
        ${TOTBAL}
<!--           元 -->
                            <spring:message code="LB.Dollar"/>
      </td>
    </tr>
    <!-- 轉出帳號可用餘額 -->
    <tr>
      <td class="ColorCell">
<!--         轉出帳號可用餘額 -->
        <spring:message code="LB.Payers_available_balance" />
      </td>
      <td>
<!--           新台幣 -->
          <spring:message code="LB.NTD" />
         ${AVLBAL}
<!--           元 -->
          <spring:message code="LB.Dollar"/>
      </td>
    </tr>
  </table>
  <br>
  <br>
<!--   <div class="text-left"> -->
<!--     說明： -->
<!--     <ol class="list-decimal text-left"> -->
<!--       <li>晶片金融卡:為保護您的交易安全，結束交易或離開電腦時，請務必將晶片金融卡抽離讀卡機並登出系統。</li> -->
<!--       <li>電子簽章:為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章(載具i-key)拔除並登出系統。</li> -->
<!--     </ol> -->
<!--   </div> -->
</body>

</html>