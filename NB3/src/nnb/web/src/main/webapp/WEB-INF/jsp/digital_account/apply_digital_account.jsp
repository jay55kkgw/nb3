<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>

	<style>
		.divider{
			background-color: #ed6d00;
		    height: 0.5rem;
		    border: none;
		}
	</style>
	<script type="text/javascript">
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		
		// 畫面初始化
		function init() {
			$("#CMSUBMIT").click(function(e) {
				if(!chkclick()){
					errorBlock(null, null, ["請審閱條款及勾選"],
							'<spring:message code= "LB.Confirm" />', null);
				}
				else{
					console.log("submit~~");
		
		         	initBlockUI();
		            $("#formId").submit();
				}
			});
			$("#CMBACK").click( function(e) {
				window.close();
			});
			
			$("input[name^='ReadFlag']").on('click', function() {
				var clickName = this.id;
				if($("#"+clickName).prop('checked')){
					if(clickName == "ReadFlag1"){
						$("#terms1").show();
					}
					else if(clickName == "ReadFlag2"){
						$("#terms2").show();
					}
					else if(clickName == "ReadFlag3"){
						$("#terms3").show();
					}
					return false;
				}
			});
			
		}

        function checkReadFlag()
        {
        	console.log($("#ReadFlag").prop('checked') && $("#ReadFlag1").prop('checked'));
            if ($("#ReadFlag").prop('checked') && $("#ReadFlag1").prop('checked'))    		
          	{
           		$("#CMSUBMIT").prop('disabled',false);
           		$("#CMSUBMIT").addClass("btn-flat-orange");
          	}
          	else
          	{
           		$("#CMSUBMIT").prop('disabled',true);
           		$("#CMSUBMIT").removeClass("btn-flat-orange");
       	  	}	
        }
		function openMenu() {
			var main = document.getElementById("main");
			window.open('${__ctx}/public/OpenAccount.pdf');		   
		}
		
		function agreeRead(data){
			var chk = "ReadFlag" + data.replace("read","");
			$("#"+chk).prop('checked',true);
		}
		
		function chkclick(){
			if($('#ReadFlag1').prop('checked') && $('#ReadFlag2').prop('checked') && $('#ReadFlag3').prop('checked') && $('#Flag').prop('checked')){
				return true;
			}
			else{
				return false;
			}
		}
		
	</script>
    <style>
    	.DataCell{
    	    text-align: left;
    	}
		@media screen and (max-width:767px) {
			.ttb-button {
					width: 38%;
					left: 36px;
			}
			#CMBACK.ttb-button{
					border-color: transparent;
					box-shadow: none;
					color: #333;
					background-color: #fff;
			}


	}
    </style>
</head>

<body>
	<section id="terms1" class="error-block" style="display:none">
		<div class="error-for-message" style="max-width:80%;">
			<p class="error-title" style="margin-bottom: 0px">數位存款帳戶約定書、開戶總約定書</p>
<!-- 			<p class="error-content"> -->
				<ul class="ttb-result-list terms">
					<li data-num="" style="overflow-x: auto; padding-bottom: 0px;">
						<span class="input-subtitle subtitle-color">
						<div class="CN19-clause" style="width: 100%; border: 1px solid #D7D7D7; border-radius: 6px; overflow-x: auto;">
							<%@ include file="../term/N203.jsp"%>
							<hr class="divider">
							<%@ include file="../term/OpenAccountD.jsp"%>
						</div>
					</li>
				</ul>
<!-- 			</p> -->
			<input type="button" id="read1" value="已審閱並同意" class="ttb-button btn-flat-orange " onclick="agreeRead(this.id);$('#terms1').hide();"/>
		</div>
	</section>
	<section id="terms2" class="error-block" style="display:none">
		<div class="error-for-message" style="max-width:80%">
			<p class="error-title" style="margin-bottom: 0px">個人資料運用告知聲明、FATCA法案蒐集、處理及利用個人資料告知事項(自然人客戶)</p>
<!-- 			<p class="error-content"> -->
				<ul class="ttb-result-list terms">
					<li data-num="" style="overflow-x: auto; padding-bottom: 0px;">
						<span class="input-subtitle subtitle-color">
						<div class="CN19-clause" style="width: 100%; border: 1px solid #D7D7D7; border-radius: 6px; overflow-x: auto;">
							<p style="text-align: center;margin-bottom: -1rem;">個人資料運用告知聲明</p>
							<%@ include file="../term/N201_1.jsp"%>
							<hr class="divider">
							<%@ include file="../term/N201_2.jsp"%>
						</div>
					</li>
				</ul>
<!-- 			</p> -->
			<input type="button" id="read2" value="已審閱並同意" class="ttb-button btn-flat-orange " onclick="agreeRead(this.id);$('#terms2').hide();"/>
		</div>
	</section>
	<section id="terms3" class="error-block" style="display:none">
		<div class="error-for-message" style="max-width:80%">
			<p class="error-title"  style="margin-bottom: 0px">FATCA聲明書內容</p>
<!-- 			<p class="error-content"> -->
				<ul class="ttb-result-list terms">
					<li data-num="" style="overflow-x: auto; padding-bottom: 0px;">
						<span class="input-subtitle subtitle-color">
						<div class="CN19-clause" style="width: 100%; border: 1px solid #D7D7D7; border-radius: 6px; overflow-x: auto;">
							<%@ include file="../term/FATCA.jsp"%>
						</div>
					</li>
				</ul>
<!-- 			</p> -->
			<input type="button" id="read3" value="已審閱並同意" class="ttb-button btn-flat-orange " onclick="agreeRead(this.id);$('#terms3').hide();"/>
		</div>
	</section>

	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上開立數位存款帳戶     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1361" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!--基金線上預約開戶作業 -->
<!-- 					線上開立數位存款帳戶 -->
					<spring:message code="LB.D1361"/>
				</h2>
                <div id="step-bar">
                    <ul>
                        <li class="active">注意事項與權益</li>
                        <li class="">開戶認證</li>
                        <li class="">開戶資料</li>
                        <li class="">確認資料</li>
                        <li class="">完成申請</li>
                    </ul>
                </div>
				<form method="post" id="formId" name="formId" action="${__ctx}/DIGITAL/ACCOUNT/apply_digital_account_p2">
					<input type="hidden" name="back" id="back" value=>	
					<input type="hidden" name="TYPE" value="10">
					<input type="hidden" name="outside_source" value="${ outside_source }">
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12 terms-block">
							<div class="ttb-input-block">
	                       		<div class="ttb-message">
		                            <p>注意事項與權益</p>
		                        </div>
		                        <div class="text-left">
		                            <p><spring:message code="LB.X0197"/></p>
		                            <ul class="list-decimal" style="margin-left: 1rem;">
		                                <li data-num="<spring:message code="LB.X0237"/>">
		                                    <span><spring:message code="LB.X0238"/><b>(<spring:message code="LB.X0239"/>)</b>。</span>
		                                </li>
		                                <li data-num="<spring:message code="LB.X0240"/>">
		                                    <span><spring:message code="LB.X0155"/><b class="high-light"><spring:message code="LB.X0242"/></b>。</span>
		                                </li>
		                                <li data-num="<spring:message code="LB.X0243"/>">
		                                    <span><spring:message code="LB.X0244"/></span>
		                                </li>
		                                <li data-num="<spring:message code="LB.X0245"/>">
		                                    <span><spring:message code="LB.X0246"/></span>
		                                </li>
		                                <li data-num="<spring:message code="LB.X0247"/>">
		                                    <span>「數位存款帳戶」晶片金融卡額度與「一般帳戶之晶片金融卡額度相同」；同一客戶之「數位存款帳戶」合計非約定轉帳金額，每筆限新臺幣5萬元整，每日限新臺幣10萬元整，每月限新臺幣20萬元整；惟交易機制採用「他行帳戶驗證」，其同一客戶之「數位存款帳戶」合計非約定轉帳金額，每筆限新臺幣1萬元整，每日限新臺幣3萬元整，每月限新臺幣5萬元整。</span>
		                                </li>
		                                <li data-num="<spring:message code="LB.X0249"/>">
		                                    <span><spring:message code="LB.X0250"/></span>
		                                </li>
		                                <li data-num="<spring:message code="LB.X0251"/>">
		                                    <span><B><spring:message code="LB.X0252"/></B></span>
		                                </li>
		                                <li data-num="<spring:message code="LB.X0253"/>">
		                                    <span><B><spring:message code="LB.X0254"/></B></span>
		                                </li>
	                        	        <li data-num="<spring:message code="LB.X0255"/>"> 
		                                    <span><B><spring:message code="LB.X0256"/></B></span>
		                                </li>
		                            </ul>
		                        </div>
	                        	<div class="classification-block">
									<p>請審閱以下條款</p>
								</div>
		                        <div class="text-left">
									<span class="input-block">
		                                <div class="ttb-input">
												<label class="check-block" for="ReadFlag1">
													<input type="checkbox" name="ReadFlag" id="ReadFlag1">
													<b>本人已審閱並同意<font class="high-light">數位存款帳戶約定書</font>、<font class="high-light">開戶總約定書</font></b>
					                                <span class="ttb-check"></span>
												</label>
											</div>
											<div class="ttb-input">
												<label class="check-block" for="ReadFlag2">
													<input type="checkbox" name="ReadFlag" id="ReadFlag2">
													<b>本人已審閱並同意<font class="high-light">個人資料運用告知聲明</font>、<font class="high-light">FATCA法案搜集</font>、<font class="high-light">處理及利用個人資料告知事項（自然人客戶）</font></b>
					                                <span class="ttb-check"></span>
												</label>
											</div>
											<div class="ttb-input">
												<label class="check-block" for="ReadFlag3">
													<input type="checkbox" name="ReadFlag" id="ReadFlag3">
													<b>本人已審閱並同意<font class="high-light">FATCA聲明書內容</font></b>
					                                <span class="ttb-check"></span>
												</label>
											</div>
											<div class="ttb-input">
												<label class="check-block" for="Flag">
													<input type="checkbox" name="Flag" id="Flag">
													<b>本人聲明<font class="high-light">非屬美國納稅義務人</font></b>
					                                <span class="ttb-check"></span>
												</label>
											</div>
		                            </span>
		                        </div>
							</div>
	
							<!-- 確定 -->
							<input type="button" id="CMBACK" value="<spring:message code="LB.Cancel"/>" class="ttb-button btn-flat-gray"/>
							<input type="button" id="CMSUBMIT" value="<spring:message code="LB.X0080"/>" class="ttb-button btn-flat-orange"/>
						</div>
					</div>
						
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>
