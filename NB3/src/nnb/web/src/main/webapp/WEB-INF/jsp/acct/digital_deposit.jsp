<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp" %>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});

	// 畫面初始化
	function init() {
		$("#formId").validationEngine({
			binded: false,
			promptPosition: "inline"
		});
		$("#CMSUBMIT").click(function(e){
			e = e || window.event;
			if(!$('#formId').validationEngine('validate')){
        		e.preventDefault();
        	}
			else{
				$("#formId").attr("action","${__ctx}/NT/ACCT/digital_deposit_result");
				$("#formId").submit();
			}
		})
	}
	

</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
	<!-- 帳戶查詢 -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Account_Inquiry" /></li>
    <!-- 數位存款帳戶存摺封面下載     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X2455" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
<%-- 				<h2><spring:message code="LB.Virtual_Account_Detail" /></h2> --%>
				<h2><spring:message code="LB.X2455" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form method="post" id="formId" action="">
					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-input-block">
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.X0348" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input ">
											<select name="ACN" id="ACN" class="custom-select select-input half-input validate[funcCall[validate_CheckSelect[<spring:message code='LB.X0348' />,ACN,#]]]">
												<option value="#">----<spring:message code="LB.Select_account" />-----</option>
												<c:forEach var="dataList" items="${digital_deposit.data.REC}">
													<option> ${dataList.ACN}</option>
												</c:forEach>
											</select>
										</div>
									</span>
								</div>
  							</div>
<%--   							<input id="CMRESET" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" />  --%>
							<input id="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
  						</div>
  					</div>
  				</form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>