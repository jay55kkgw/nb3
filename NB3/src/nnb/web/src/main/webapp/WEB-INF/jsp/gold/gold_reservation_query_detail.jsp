<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
	
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>	
</head>
 <body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
    <!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %>  

	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
	<!-- 查詢服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.use_component_P1_D1-1" /></li>
    <!-- 預約取消/查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1491" /></li>
		</ol>
	</nav>

 	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
	<main class="col-12"> 
	<!-- 		主頁內容  -->
		<section id="main-content" class="container">
		<!-- 預約黃金交易查詢/取消 -->
			<h2><spring:message code= "LB.X0948" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<!-- 請確認取消資料 -->						
<%-- 			<p style="text-align: center;color: red;"><spring:message code= "LB.X0955" /></p> --%>
			<form method="post" id="formId">
				<input type="hidden" name="ADOPID" value="N092">		
                <input type="hidden" id="back" name="back" value="">
                <input type="hidden" id="APPTYPE" name="APPTYPE" value="${ result_data.APPTYPE }">
                <input type="hidden" id="APPDATE" name="APPDATE" value="${ result_data.APPDATE }">
                <input type="hidden" id="APPTIME" name="APPTIME" value="${ result_data.APPTIME }">
                <input type="hidden" id="ACN" name="ACN" value="${ result_data.ACN }">
                <input type="hidden" id="SVACN" name="SVACN" value="${ result_data.SVACN }">
                <input type="hidden" id="TRNGD" name="TRNGD" value="${ result_data.TRNGD }">
                <input type="hidden" id="DPMYEMAIL" name="DPMYEMAIL" value="">
                <input type="hidden" id="PINNEW" name="PINNEW" value="">
                
                <!-- ikey -->
				<input type="hidden" id="jsondc" name="jsondc" value='${ result_data.jsondc }'>
				<input type="hidden" id="ISSUER" name="ISSUER" value="">
				<input type="hidden" id="ACNNO" name="ACNNO" value="">
				<input type="hidden" id="TRMID" name="TRMID" value="">
				<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
				<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
				<input type="hidden" id="TAC" name="TAC" value="">
				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
				<input type="hidden" name="CMTRANPAGE" value="1">
				
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
                            <div class="ttb-message">
								<!-- 請確認取消資料 -->						
                                <span><spring:message code= "LB.X0955" /></span>
                            </div>
							<c:if test="${ result_data.APPTYPE.equals('01') }">
								<!-- 交易種類 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 交易種類 -->
											<spring:message code= "LB.W1060" />
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
										<!-- 預約黃金買進 -->
											<p><spring:message code= "LB.X0949" /></p>
										</div>
									</span>
								</div>
								<!-- 台幣轉出帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 臺幣轉出帳號 -->
											<spring:message code= "LB.W1496" />
										</h4>
									</label>
									</span> <span class="input-block">
										<div class="ttb-input">
											<p>${ result_data.SVACN }</p>
										</div>
									</span>
								</div>
								<!-- 確認新使用者名稱 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
<!-- 黃金轉入帳號 -->
												<spring:message code= "LB.W1497" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<p>${ result_data.ACN }</p>
										</div>
									</span>
								</div>
								<!-- 確認新使用者名稱 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
<!-- 買進公克數 -->
												<spring:message code= "LB.W1498" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
<!-- 公克 -->
											<p>${ result_data.TRNGD_str } <spring:message code= "LB.W1435" /></p>
										</div>
									</span>
								</div>
							</c:if>
							
							<c:if test="${ result_data.APPTYPE.equals('02') }">
								<!-- 交易種類 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 交易種類 -->
											<spring:message code= "LB.W1060" />
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
										<!-- 預約黃金回售 -->
											<p><spring:message code= "LB.X0952" /></p>
										</div>
									</span>
								</div>
								<!-- 台幣轉出帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 黃金轉出帳號 -->
											<spring:message code= "LB.W1515" />
										</h4>
									</label>
									</span> <span class="input-block">
										<div class="ttb-input">
											<p>${ result_data.ACN }</p>
										</div>
									</span>
								</div>
								<!-- 確認新使用者名稱 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
<!-- 臺幣轉入帳號 -->
												<spring:message code= "LB.W1518" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<p>${ result_data.SVACN }</p>
										</div>
									</span>
								</div>
								<!-- 確認新使用者名稱 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
<!-- 賣出公克數 -->
												<spring:message code= "LB.W1519" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
<!-- 公克 -->
											<p>${ result_data.TRNGD_str } <spring:message code= "LB.W1435" /></p>
										</div>
									</span>
								</div>
							</c:if>
							
							<!--交易機制  SSL 密碼 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.Transaction_security_mechanism" />
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block">
								       		<input type="radio" name="FGTXWAY" id="CMSSL" value="0" checked /><spring:message code="LB.SSL_password" />
											<span class="ttb-radio"></span>
								       	</label>
								    </div>
									<div class="ttb-input">
										<input type="password" id="CMPASSWORD"  maxlength="8" class="text-input validate[required,pwvd[PW,CMPASSWORD],custom[onlyLetterNumber]]">
										<input type="hidden" name = "PINNEW" id="PINNEW" value="">
									</div>
									<!-- 使用者是否可以使用IKEY -->
									<c:if test = "${sessionScope.isikeyuser}">
										<!-- 即使使用者可以用IKEY，若轉出帳號為晶片金融卡帶出的非約定帳號，也不給用 (By 景森)-->
										<c:if test = "${transfer_data.data.TransferType != 'NPD'}">
									
											<!-- IKEY -->
											<div class="ttb-input">
												<label class="radio-block">
													<input type="radio" name="FGTXWAY" id="CMIKEY" value="1">
													<spring:message code="LB.Electronic_signature" />
													<span class="ttb-radio"></span>
												</label>
											</div>
									
										</c:if>
									</c:if>
									<div class="ttb-input" name="idgate_group" style="display:none" onclick="hideCapCode('Y')">		 
                                       <label class="radio-block">裝置推播認證(請確認您的行動裝置網路連線是否正常，及推播功能是否已開啟)
	                                       <input type="radio" id="IDGATE" 	name="FGTXWAY" value="7"> 
	                                       <span class="ttb-radio"></span>
                                       </label>		 
                                   </div>
								</span>
							</div>
						</div>
						<!--回上頁 -->
                        <spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>
                        <input type="button" name="CMBACK" id="CMBACK" value="${cmback}" class="ttb-button btn-flat-gray">	
						<input id="CMSUBMIT" name="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />				
					</div>
				</div>
				</form>
		</section>
		<!-- 		main-content END -->
	</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
	<!--   Js function -->
    <script type="text/JavaScript">
		$(document).ready(function() {
			init();
		});

		// 交易機制選項
		function processQuery(){
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
		
			switch(fgtxway) {
				case '0':
// 					alert("交易密碼(SSL)...");
					$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
	    			$("form").submit();
					break;
					
				case '1':
// 					alert("IKey...");
					var jsondc = $("#jsondc").val();
					
					// 遮罩後不給捲動
// 					document.body.style.overflow = "hidden";

					// 呼叫IKEY元件
// 					uiSignForPKCS7(jsondc);
					
					// 解遮罩後給捲動
// 					document.body.style.overflow = 'auto';
					
					useIKey();
					break;
					
				case '2':
					// 晶片金融卡
					//alert("<spring:message code= "LB.D0234" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.D0234' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
					);

					// 遮罩後不給捲動
					document.body.style.overflow = "hidden";
					
					// 呼叫讀卡機元件
					listReaders();

					// 解遮罩後給捲動
					document.body.style.overflow = 'auto';
					
			    	break;

		        case '7'://IDGATE認證		 
	                idgatesubmit= $("#formId");		 
	                showIdgateBlock();		 
	                break;
				default:
					//alert("nothing...");
					errorBlock(
							null, 
							null,
							["nothing..."], 
							'<spring:message code= "LB.Quit" />', 
							null
					);
			}
			
		}

	    function init(){
// 	    	$("#formId").validationEngine({binded:false,promptPosition: "inline" });
	    	$("#CMSUBMIT").click(function(e){			
				console.log("submit~~");
				if(!$('#formId').validationEngine('validate') && $('input[name="FGTXWAY"]:checked').val() == 0){
		        	e.preventDefault();
	 			}else{
	 				$("#formId").validationEngine('detach');
// 	 				initBlockUI();
	    			var action = '${__ctx}/GOLD/PASSBOOK/gold_reservation_query_result';
	    			$("#formId").attr("action", action);
	    			processQuery();
	 			}
			});
    		//上一頁按鈕
    		$("#CMBACK").click(function() {
    			var action = '${__ctx}/GOLD/PASSBOOK/gold_reservation_query';
    			$('#back').val("Y");
    			$("#formId").attr("action", action);
    			$("#formId").submit();
    		});
    		$("#CMIKEY").click(function(){
    			$('#CMPASSWORD').removeClass("validate[required,pwvd[PW,CMPASSWORD],custom[onlyLetterNumber]]");
    		});
    		$("#CMSSL").click(function(){
    			$('#CMPASSWORD').addClass("validate[required,pwvd[PW,CMPASSWORD],custom[onlyLetterNumber]]");
    		});
	    }	
 	</script>
</body>
</html>
 