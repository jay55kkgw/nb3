<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<script type="text/javascript">

    var n320 = false;
	// column's title i18n
	// n320-借款明細查詢
	//臺幣
	i18n['ACNCOVER'] = '<spring:message code="LB.Loan_account" />'; // 帳號
	i18n['SEQ'] = '<spring:message code="LB.Seq_of_account" />'; // 分號
	i18n['AMTORLN'] = '<spring:message code="LB.Original_loan_amount" />'; // 原貸金額
	i18n['BAL'] = '<spring:message code="LB.Loan_Outstanding" />'; // 貸款餘額
	i18n['ITR'] = '<spring:message code="LB.Interest_rate1" />'; // 利率(%)
	i18n['DATFSLN'] = '<spring:message code="LB.Loan_drawdown_date" /><br><spring:message code="LB.Loan_drawdown_date2" />'; // 初貸日（額度起日）
	i18n['DDT'] = '<spring:message code="LB.Expired_date" /><br><spring:message code="LB.Expired_date2" />'; // 到期日（額度迄日）
	i18n['AWT'] = '<spring:message code="LB.Months_of_deferred_principal" /><hr><spring:message code="LB.End_date_of_repay_interest" />'; // 開始償還本金月份
// 	i18n['DATITPY'] = '<spring:message code="LB.End_date_of_repay_interest" />'; // 繳息迄日
	i18n['QM'] = '<spring:message code="LB.Quick_Menu" />';//快速選單
	//外幣
	i18n['ACNCOVER'] = '<spring:message code="LB.Loan_account" />'; // 帳號
	i18n['LNREFNO'] = ' <spring:message code= "LB.W0134" />'; // 交易編號
	i18n['LNCCY'] = '<spring:message code="LB.Currency"/>'; // 幣別
	i18n['LNCOS'] = ' <spring:message code= "LB.Loan_Balance" />'; // 放款餘額
	i18n['LNINTST'] = ' <spring:message code= "LB.X0965" />'; // 動撥日
	i18n['LNDUEDT'] = '<spring:message code="LB.Expired_date"/>'; // 到期日
	i18n['NXTINTD'] = ' <spring:message code= "LB.X0966" />'; // 下次繳息日
	i18n['LNFIXR'] = ' <spring:message code= "LB.X0967" />'; // 適用利率
	i18n['LNUPDATE'] = '<spring:message code="LB.Data_date"/>'; // 資料日期
	i18n['TYPEA'] = '<spring:message code="LB.Note"/>'; // 備註
    i18n['DOLLAR'] = '<spring:message code="LB.Dollar"/>';//元
    i18n['NODATA'] = '<spring:message code="LB.Check_no_data"/>';//查無資料
    // DataTable Ajax tables
	var n320tw_columns = [
		{ "data":"ACNCOVER", "title":i18n['ACNCOVER'] },
		{ "data":"SEQ", "title":i18n['SEQ'] },
		{ "data":"AMTORLN", "title":i18n['AMTORLN'] },
		{ "data":"BAL", "title":i18n['BAL'] },
		{ "data":"ITR", "title":i18n['ITR'] },
		{ "data":"DATFSLN", "title":i18n['DATFSLN'] },
		{ "data":"DDT", "title":i18n['DDT'] },
		{ "data":"AWT", "title":i18n['AWT'] },
// 		{ "data":"DATITPY", "title":i18n['DATITPY'] },
		{ "data":"QM", "title":i18n['QM'] }
	];
	
	var n320fx_columns = [
		{ "data":"ACNCOVER", "title":i18n['ACNCOVER'] },
		{ "data":"LNREFNO", "title":i18n['LNREFNO'] },
		{ "data":"LNCCY", "title":i18n['LNCCY'] },
		{ "data":"LNCOS", "title":i18n['LNCOS'] },
		{ "data":"LNINTST", "title":i18n['LNINTST'] },
		{ "data":"LNDUEDT", "title":i18n['LNDUEDT'] },
		{ "data":"NXTINTD", "title":i18n['NXTINTD'] },
		{ "data":"LNFIXR", "title":i18n['LNFIXR'] },
		{ "data":"LNUPDATE", "title":i18n['LNUPDATE'] },
		{ "data":"TYPEA", "title":i18n['TYPEA'] }
	];
	

	//0置中 1置右
	var n320tw_align = [0,0,1,1,1,0,0,0,0,0];
	var n320fx_align = [0,0,0,1,0,0,0,1,0,0];
	// Ajax_n320-借款明細查詢
	function getMyAssets320() {
		$("#n320").show();
		$("#tw_title").show();
		$("#n320_fx").show();
		$("#fx_title").show();
		$("#n320_TWBOX").html("")
		$("#n320_FXBOX").html("")
		createLoadingBox("n320_TWBOX",'');
		createLoadingBox("n320_FXBOX",'');
		uri = '${__ctx}' + "/OVERVIEW/allacct_n320aj";
		console.log("allacct_n320aj_uri: " + uri);
		var Data = $("#formN320").serializeArray();
		fstop.getServerDataEx(uri, Data, true, countAjax_n320, null, "n320");
	}

	// Ajax_callback
	function countAjax_n320(data){
		console.log(data);
		if(data.result)
			{
			// 資料處理後呈現畫面
			$('#N320GO').val(data.data.N320GO)
			$('#N552GO').val(data.data.N552GO)
			$('#USERDATA_X50_552').val(data.data.USERDATA_X50_552)
			$('#USERDATA_X50_320').val(data.data.USERDATA_X50_320)
			deal(data);
			}
		else
			{
			n320tw_columns = [ {
				"data" : "MSGCODE",
				"title" : '<spring:message code="LB.Transaction_code" />'
			}, {
				"data" : "MESSAGE",
				"title" : '<spring:message code="LB.Transaction_message" />'
			}];
			n320fx_columns = n320tw_columns;
			if(data.msgCode){
			n320tw_rows = [ {
				"MSGCODE" : data.msgCode,
				"MESSAGE" : data.message
			} ];
			}else
				{
				
				}
			n320fx_rows=n320tw_rows;
			if(!n320)
			{
			createTable("n320_TWBOX",n320tw_rows,n320tw_columns,n320tw_align);
			createTable("n320_FXBOX",n320fx_rows,n320fx_columns,n320tw_align);
			}
			}
		
	}
		
function deal(data)
{
	$("#TWSUM").html("");
	$("#FXSUM").html("");
	$("#query").hide();
	n320 = true;
	//台幣
	if (data.data.TOPMSG_320=='OKLR'||data.data.TOPMSG_320=='OKOV'||data.data.TOPMSG_320=='ERDB') {
		n320tw_rows = data.data.TW;
		n320tw_rows.forEach(function(d,i){
			d["AWT"]=d["AWT"]+"<hr>"+d["DATITPY"];
			var options = [];
			if(d.PAYTYPE == 'Y')
			{
				options.push(new Option("<spring:message code='LB.X0972' />", "repay_advance_p1"));	
				options.push(new Option("<spring:message code='LB.W0876' />", "interest_list"));	
				options.push(new Option("<spring:message code='LB.Paid_principal_and_interest_inquiry' />", "paid_query"));	
			}
			if(d.PAYTYPE == 'A')
			{
				options.push(new Option("<spring:message code='LB.X0972' />", "repay_advance_p1"));	
				options.push(new Option("<spring:message code='LB.X0973' />", "settlement_advance_p1"));	
				options.push(new Option("<spring:message code='LB.W0876' />", "interest_list"));	
				options.push(new Option("<spring:message code='LB.Paid_principal_and_interest_inquiry' />", "paid_query"));	
			}
			if(d.PAYTYPE == 'B')
			{
				options.push(new Option("<spring:message code='LB.X0972' />(<spring:message code='LB.X2278' />)", "repay_advance_p1"));	
				options.push(new Option("<spring:message code='LB.X0973' />", "settlement_advance_p1"));	
			}
			if(d.PAYTYPE == 'C')
			{
				options.push(new Option("<spring:message code='LB.X0972' />(<spring:message code='LB.X2278' />)", "repay_advance_p1"));	
			}
			if(options.length!=0)
				{
				var qm = $("#actionBar").clone();
				qm.children()[0].id = "n320";
				qm.children()[0].name=i;
				qm.show();
				options.forEach(function(x,y){
					if((d.PAYTYPE == 'B'||d.PAYTYPE == 'C') && y==1){
						qm[0].children[0].options.add(x);
						qm[0].children[0].options[y].style="color:red";
					}else{
						qm[0].children[0].options.add(x);
					}
				});
				
				d["QM"]=qm.html();
				}
			else{
				d["QM"]="";
			}
			
			
			
		});
		
		var twsum = "<spring:message code='LB.Total_records'/>"+"&nbsp;"+ data.data.TWNUM + "&nbsp;"+"<spring:message code='LB.Rows'/>" +　"<br>";
		twsum += "<spring:message code='LB.NTD'/>"+"&nbsp;"+data.data.TOTALBAL_320+"&nbsp;" + "<spring:message code='LB.Dollar'/>" +　"<br>";
		twsum += "<spring:message code='LB.Loan_detail_P1_NTD_note'/>";
		$("#TWSUM").html(twsum);
		
		if(data.data.TOPMSG_552 == "OKOV" || data.data.TOPMSG_320 == "OKOV")
			{
			//繼續查詢
			$("#query").show();
			}
	
		
	}else if(data.data.MSGFLG_320=='01'){
		// 錯誤訊息
		
		n320tw_columns = [ {
				"data" : "MSGCODE",
				"title" : '<spring:message code="LB.Transaction_code" />'
			}, {
				"data" : "MESSAGE",
				"title" : '<spring:message code="LB.Transaction_message" />'
			}];
		n320tw_rows = [ {
			"MSGCODE" : 'MSGFLG=01',
			"MESSAGE" : '<spring:message code="LB.Loan_contact_note"/>'
		} ];
	}else if (data.data.TOPMSG_320 =='NoCall'){
		
		n320tw_rows = [ {
			"ACNCOVER" : "",
			"SEQ" : "",
			"AMTORLN" : "",
			"BAL" : "",
			"ITR" : "",
			"DATFSLN" : "",
			"DDT" : "",
			"AWT" : "",
			"DATITPY":"",
			"QM":""
		} ];
	}else{
		n320tw_columns = [ {
			"data" : "MSGCODE",
			"title" : '<spring:message code="LB.Transaction_code" />'
		}, {
			"data" : "MESSAGE",
			"title" : '<spring:message code="LB.Transaction_message" />'
		}];
		n320tw_rows = [ {
			"MSGCODE" : data.data.TOPMSG_320,
			"MESSAGE": data.data.errorMsg320
		} ];
	}
	console.log("n320tw_rows");
	console.log(n320tw_rows);
	createTable("n320_TWBOX",n320tw_rows,n320tw_columns,n320tw_align);
	//外幣
	if (data.data.TOPMSG_552=='OKLR'||data.data.TOPMSG_552=='OKOV') {
		n320fx_rows = data.data.FX;
		var fxsum = "<spring:message code='LB.Total_records'/>"+"&nbsp;"+ data.data.FXNUM + "&nbsp;"+"<spring:message code='LB.Rows'/>" +　"<br>";
		data.data.CRY.forEach(function(k){
			fxsum += k.AMTLNCCY+"&nbsp;"+k.FXTOTAMT+"&nbsp;"+ i18n['DOLLAR']+"<br>";
		});
		$("#FXSUM").html(fxsum);
	}else if(data.data.MSGFLG_552=='01'){
		// 錯誤訊息
		n320fx_columns = [ {
			"data" : "MSGCODE",
			"title" : '<spring:message code="LB.Transaction_code" />'
		}, {
			"data" : "MESSAGE",
			"title" : '<spring:message code="LB.Transaction_message" />'
		}];
		n320fx_rows = [ {
			"MSGCODE" : 'MSGFLG=01',
			"MESSAGE" : '<spring:message code="LB.Loan_contact_note"/>'
		} ];
	}else if (data.data.TOPMSG_552 =='NoCall'){
		n320fx_rows = [ {
			"ACNCOVER" : "",
			"LNREFNO" : "",
			"LNCCY" : "",
			"LNCOS" : "",
			"LNINTST" : "",
			"LNDUEDT" : "",
			"NXTINTD" : "",
			"LNFIXR" : "",
			"LNUPADTE":"",
			"TYPEA":""
		} ];
	}else{
		n320fx_columns = [ {
			"data" : "MSGCODE",
			"title" : '<spring:message code="LB.Transaction_code" />'
		}, {
			"data" : "MESSAGE",
			"title" : '<spring:message code="LB.Transaction_message" />'
		}];
		n320fx_rows = [ {
			"MSGCODE" : data.data.TOPMSG_552,
			"MESSAGE": data.data.errorMsg552
		} ];
	}
	console.log("n320fx_rows");
	console.log(n320fx_rows);
	createTable("n320_FXBOX",n320fx_rows,n320fx_columns,n320fx_align);
}


</script>
<p id="tw_title" class="home-title" style="display: none"><spring:message code="LB.NTD_loan_detail" /></p>
<div id="n320" class="main-content-block" style="display: none">

	<div id="n320_TWBOX" style="display: none"></div>
	<div class="text-left">
		<p id="TWSUM"></p>

		<div class="ttb-input" id="query" style="display: none">
			<font color="red"><spring:message code="LB.X0076" /></font>
			<!-- 查詢結果超出每頁可顯示最大筆數，如欲顯示後續資料，請按 -->
			<button type="button" class="btn-flat-orange" id="Query"
				onclick="getMyAssets320()" />
			<spring:message code="LB.X0151" />
			</button>
			<!-- 繼續查詢 -->
		</div>
	</div>
</div>


<p id="fx_title" class="home-title" style="display: none"><spring:message code="LB.FX_loan_detail" /></p>
<div id="n320_fx" class="main-content-block" style="display: none">
	<div id="n320_FXBOX" style="display: none"></div>
	<div class="text-left">
		<p id="FXSUM"></p>

		<div class="ttb-input" id="query" style="display: none">
			<font color="red"><spring:message code="LB.X0076" /></font>
			<!-- 查詢結果超出每頁可顯示最大筆數，如欲顯示後續資料，請按 -->
			<button type="button" class="btn-flat-orange" id="Query"
				onclick="getMyAssets320()" />
			<spring:message code="LB.X0151" />
			</button>
			<!-- 繼續查詢 -->
		</div>
	</div>

</div>



<form id="formN320" method="post" action="">
	<input type="hidden" id="N320GO" name="N320GO" value="" /> <input
		type="hidden" id="N552GO" name="N552GO" value="" /> <input
		type="hidden" id="USERDATA_X50_552" name="USERDATA_X50_552" value='' />
	<input type="hidden" id="USERDATA_X50_320" name="USERDATA_X50_320"
		value='' /> <input type="hidden" id="loanACN" name="loanACN" /> <input
		type="hidden" id="loanSEQ" name="loanSEQ" /> <input type="hidden"
		id="loanAMT" name="loanAMT" />
</form>

