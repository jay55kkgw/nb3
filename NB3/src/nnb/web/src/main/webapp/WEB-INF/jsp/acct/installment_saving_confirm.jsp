<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
	<style>
		.noZhCn{
			-ms-ime-mode: disabled;
		}
	</style>
	<script type="text/javascript">
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 初始化驗證碼
			setTimeout("initKapImg()", 200);
			// 生成驗證碼
			setTimeout("newKapImg()", 300);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		//畫面初始化
		function init() {
			//一開始先FOCUS到BHO
			focusBHO();
			// 表單驗證初始化
			$("#formId").validationEngine({binded: false,promptPosition: "inline"});
			// 確認鍵 click
			goOn();
			// 上一頁按鈕 click
			goBack();
			//交易類別change 事件
			changeFgtxway();
		}
		//一開始先FOCUS到BHO
		function focusBHO() {
			//一開始先FOCUS到BHO
			$("#transInAccountText1").focus();
			//跳格
			$("#transInAccountText1").keyup(function (event) {
				if (event.which != 8 && event.which != 46 && event.which != 9) {
					$("#transInAccountText2").focus();
				}
			});
			$("#transInAccountText2").keyup(function (event) {
				if (event.which != 8 && event.which != 46 && event.which != 9) {
					$("#transInAccountText3").focus();
				}
			});
		}
		// 確認鍵 Click
		function goOn() {
			$("#CMSUBMIT").click(function () {
				//送出進表單驗證前將span顯示
				$("#hideblock").show();
				console.log("submit~~");
				//塞值進span內的input
				$("#CARDNUM_TOTAL").val($("#transInAccountText1").val() + $("#transInAccountText2").val() + $(
					"#transInAccountText3").val());
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
					//解除驗證
					$("#formId").validationEngine('detach');
					var uri = "${__ctx}/checkBHO"
					rdata = {
						keyInBHO: $("#transInAccountText1").val() + $("#transInAccountText2").val() + $(
							"#transInAccountText3").val(),
						functionName: "installment_saving"
					};
					fstop.getServerDataEx(uri, rdata, false, checkBHOFinish);
				}
			});
		}

		//檢查BHO
		function checkBHOFinish(data) {
			if (data.result == true) {
				processQuery();
			} else {
				//alert(data.message);
				errorBlock(
						null, 
						null,
						[data.message], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			}
		}
		// 交易機制選項
		function processQuery() {
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
			switch (fgtxway) {
				case '0':
					$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
					$('#CMPASSWORD').val("");
					initBlockUI(); //遮罩
					$("#formId").submit();
					break;
					// IKEY
				case '1':
					useIKey();
					break;
					// 晶片金融卡
				case '2':
					var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
					useCardReader(capUri);
					break;
				case '7':
					idgatesubmit= $("#formId");		 
		            showIdgateBlock();
					break;
				default:
					// 請選擇交易機制
					//alert('<spring:message code= "LB.Alert001" />');
					errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert001' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
						);
			}
		}
		//交易類別change 事件
		function changeFgtxway() {
			$('input[type=radio][name=FGTXWAY]').change(function () {
				console.log(this.value);
				if (this.value == '0') {
					$("#CMPASSWORD").addClass("validate[required]")
					// 若非晶片金融卡則隱藏驗證碼欄位
					$("#chaBlock").hide();
				} else if (this.value == '1') {
					$("#CMPASSWORD").removeClass("validate[required]");
					// 若為晶片金融卡才顯示驗證碼欄位
					$("#chaBlock").hide();
				} else if (this.value == '2') {
					$("#CMPASSWORD").removeClass("validate[required]");
					// 若非晶片金融卡則隱藏驗證碼欄位
					$("#chaBlock").show();
				} else if (this.value == '7') {
					$("#CMPASSWORD").removeClass("validate[required]");
					// 若非晶片金融卡則隱藏驗證碼欄位
					$("#chaBlock").hide();
				}
			});
		}
		// 上一頁按鈕 click
		function goBack() {
			$("#CMBACK").click(function () {
				// 遮罩
				initBlockUI();
				// 解除表單驗證
				$("#formId").validationEngine('detach');
				$('#back').val("Y");
				var action = '${__ctx}/NT/ACCT/TDEPOSIT/installment_saving';
				$("#formId").attr("action", action);
				$("#formId").submit();
			});
		}
		// 驗證碼刷新
		function changeCode() {
			$('input[name="capCode"]').val('');
			// 大小版驗證碼用同一個
			console.log("changeCode...");
			$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

			// 登入失敗解遮罩
			unBlockUI(initBlockId);
		}
		// 初始化驗證碼
		function initKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
		}
		// 生成驗證碼
		function newKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').click(function () {
				$('img[name="kaptchaImage"]').hide().attr(
						'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100))
					.fadeIn();
			});
		}
		//重新輸入
		function formReset() {
			if ($('#actionBar').val() == "reEnter") {
				$('#actionBar').val("");
				document.getElementById("formId").reset();
			}
		}
	</script>
</head>

<body>
	<!-- 交易機制所需畫面    -->
	<%@ include file="../component/trading_component.jsp"%>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %>  
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 定存服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Current_Deposit_To_Time_Deposit" /></li>
    <!-- 零存整付按月繳存     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Periodical_Deposits_And_Lumpsum_Payment_monthly" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
	</div>
	<!-- content row END -->
	<!-- 		主頁內容  -->
	<main class="col-12">
		<section id="main-content" class="container">
			<!--  臺幣零存整付按月繳存 -->
			<h2>
				<spring:message code="LB.Periodical_Deposits_And_Lumpsum_Payment_monthly" />
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<!-- 下拉式選單-->
			<div class="print-block no-l-display-btn">
				<select class="minimal" id="actionBar" onchange="formReset()">
					<option value="">
						<spring:message code="LB.Execution_option" />
					</option>
					<!-- 重新輸入-->
					<option value="reEnter">
						<spring:message code="LB.Re_enter" />
					</option>
				</select>
			</div>
			<form method="post" id="formId" action="${__ctx}/NT/ACCT/TDEPOSIT/installment_saving_result">
				<%-- 回上一頁參數 --%>
				<input type="hidden" id="back" name="back" value="">
				<%--  TXTOKEN  防止重送代碼--%>
				<input type="hidden" name="TXTOKEN" id="TXTOKEN" value="${installment_saving_confirm.data.TXTOKEN}" />
				<%-- 驗證相關 --%>
				<input type="hidden" id="PINNEW" name="PINNEW" value="">
				<input type="hidden" id="jsondc" name="jsondc" value='${installment_saving_confirm.data.jsondc}'>
				<input type="hidden" id="ISSUER" name="ISSUER" value="">
				<input type="hidden" id="ACNNO" name="ACNNO" value="">
				<input type="hidden" id="TRMID" name="TRMID" value="">
				<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
				<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
				<input type="hidden" id="TAC" name="TAC" value="">
				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
				<%--  轉出帳號 --%>
				<input type="hidden" name="OUTACN" value='${installment_saving_confirm.data.ACNO}'>
				<%-- 存單帳號--%>
				<input type="hidden" name="FDPACN" value='${installment_saving_confirm.data.FDPACN}'>
				<%-- 轉帳金額--%>
				<input type="hidden" name="AMOUNT" value='${installment_saving_confirm.data.AMOUNT}'>
				<%-- 存單號碼 --%>
				<input type="hidden" name="FDPNUM" value='${installment_saving_confirm.data.FDPNUM}'>
				<%-- 交易備註 --%>
				<input type="hidden" name="CMTRMEMO" value='${installment_saving_confirm.data.CMTRMEMO}'>
				<%-- Email信箱 --%>
				<input type="hidden" name="CMTRMAIL" value='${installment_saving_confirm.data.CMTRMAIL}'>
				<%-- Email摘要內容 --%>
				<input type="hidden" name="CMMAILMEMO" value='${installment_saving_confirm.data.CMMAILMEMO}'>
				<!--交易步驟 -->
				<div id="step-bar">
					<ul>
						<!--輸入資料 -->
						<li class="finished">
							<spring:message code="LB.Enter_data" />
						</li>
						<!-- 確認資料 -->
						<li class="active">
							<spring:message code="LB.Confirm_data" />
						</li>
						<!-- 交易完成 -->
						<li class="">
							<spring:message code="LB.Transaction_complete" />
						</li>
					</ul>
				</div>
				<!-- 表單顯示區  -->
				<div class="main-content-block row">
					 <div class="col-12 tab-content">
						<div class="ttb-input-block">
							<div class="ttb-message">
								<span>
									<!--  請確認轉帳資料-->
									<spring:message code="LB.Confirm_transfer_data" />
								</span>
							</div>
							<!-- 轉出帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Payers_account_no" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<p>
											${installment_saving_confirm.data.ACNO}
										</p>
									</div>
								</span>
							</div>
							<!-- 存單帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Account_no" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<p>
											${installment_saving_confirm.data.FDPACN_TEXT}
										</p>
									</div>
								</span>
							</div>
							<!-- 轉入帳號確認 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Confirm_payers_account_no" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<img
											src="${__ctx}/getBHO?transferInAccount=${installment_saving_confirm.data.FDPACN}&functionName=installment_saving" />
										<br /><br />
									</div>
									<div class="BHOInput">
										<input type="text" id="transInAccountText1" maxlength="1" class="text-input noZhCn" />
										-
										<input type="text" id="transInAccountText2" maxlength="1" class="text-input noZhCn" />
										-
										<input type="text" id="transInAccountText3" maxlength="1" class="text-input noZhCn" />
										<!-- （請以半型字輸入黃色標記之轉入帳號數字） -->
										<span class="input-unit">
											<spring:message code="LB.Anti-blocking_BHO_attack" />
										</span>
										<!-- 不在畫面上顯示的span -->
										<span id="hideblock">
											<!-- 驗證用的input -->
											<input id="CARDNUM_TOTAL" name="CARDNUM_TOTAL" type="text" maxlength="3"
												class="text-input validate[required,minSize[3],custom[integer]]"
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</span>
									</div>
								</span>
							</div>
							<!-- 存單號碼 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Certificate_no" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<p>
											${installment_saving_confirm.data.FDPNUM}
										</p>
									</div>
								</span>
							</div>
							<!-- 轉帳金額 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Amount" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<p class="high-light">
											<!-- 新台幣 -->
											<span class="input-unit">
												<spring:message code="LB.NTD" /></span>
											<fmt:formatNumber type="number" minFractionDigits="2"
												value="${installment_saving_confirm.data.AMOUNT }" />
											<!--元 -->
											<span class="input-unit">
												<spring:message code="LB.Dollar" /></span>
										</p>
									</div>
								</span>
							</div>
							<!-- 交易機制 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Transaction_security_mechanism" /></h4>
									</label>
								</span>
								<span class="input-block">
									<!-- 交易密碼SSL -->
									<div class="ttb-input">
										<label class="radio-block">
											<spring:message code="LB.SSL_password" />
											<input type="radio" name="FGTXWAY" checked="checked" value="0">
											<span class="ttb-radio"></span>
										</label>
									</div>
									<!--請輸入密碼 -->
									<div class="ttb-input">
										<spring:message code="LB.Please_enter_password" var="pleaseEnterPin" />
										<input type="password" id="CMPASSWORD" name="CMPASSWORD"
											class="text-input validate[required]" maxlength="8"
											placeholder="${plassEntpin}">
									</div>
									<!-- 使用者是否可以使用IKEY -->
									<c:if test="${sessionScope.isikeyuser}">
										<!--電子簽章(請載入載具i-key) -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Electronic_signature" />
												<input type="radio" name="FGTXWAY" id="CMIKEY" value="1" />
												<span class="ttb-radio"></span>
											</label>
										</div>
									</c:if>
									<div class="ttb-input" name="idgate_group" style="display:none" onclick="hideCapCode('Y')">		 
                                        <label class="radio-block">裝置推播認證(請確認您的行動裝置網路連線是否正常，及推播功能是否已開啟)
	                                        <input type="radio" id="IDGATE" name="FGTXWAY" value="7"> 
	                                        <span class="ttb-radio"></span>
                                        </label>		 
                                    </div>
									<!-- 晶片金融卡 -->
									<div class="ttb-input">
										<label class="radio-block">
											<spring:message code="LB.Financial_debit_card" />
											<input type="radio" name="FGTXWAY" id="CMCARD" value="2" />
											<span class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>
							<!-- 交易機制區塊 END -->
							<!-- 驗證碼-->
							<div class="ttb-input-item row" id="chaBlock" style="display:none">
								<span class="input-title">
									<label>
										<!-- 驗證碼 -->
										<h4><spring:message code="LB.Captcha" /></h4>
									</label>
								</span>
								<span class="input-block">
									<spring:message code="LB.Captcha" var="labelCapCode" />
									<input id="capCode" name="capCode" type="text" class="text-input"
										placeholder="${labelCapCode}" maxlength="6" autocomplete="off">
									<img name="kaptchaImage" class = "verification-img" src="" />
									<button class="ttb-sm-btn btn-flat-orange" type="button" name="reshow"
										onclick="changeCode()">
										<spring:message code="LB.Regeneration" />
									</button>
								</span>
							</div>
							<!-- 驗證碼 END-->
						</div>
						<!--button 區域 -->
							<!--回上頁 -->
							<spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>
							<input type="button" name="CMBACK" id="CMBACK" value="${cmback}" class="ttb-button btn-flat-gray">
							<!--重新輸入 -->
							<spring:message code="LB.Re_enter" var="cmRest"></spring:message>
							<input type="reset" name="CMRESET" id="CMRESET" value="${cmRest}" class="ttb-button btn-flat-gray no-l-disappear-btn">
							<!-- 確定 -->
							<spring:message code="LB.Confirm" var="cmSubmit"></spring:message>
							<input type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" class="ttb-button btn-flat-orange">
						<!--button 區域 -->
					</div>
				</div>
			</form>
		</section>
		<!-- 		main-content END -->
	</main>
	<!-- 	content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>