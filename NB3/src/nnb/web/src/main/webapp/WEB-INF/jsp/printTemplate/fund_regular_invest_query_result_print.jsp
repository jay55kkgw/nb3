<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ include file="../__import_js.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<style type="text/css">
@page{size:auto}!important;
</style>
<script type="text/javascript">
	$(document).ready(function() {
		window.print();
	});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact;margin-left: -30px">
	<br />
	<div style="text-align: center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br />
	<br />
	<div style="text-align: center">
		<font style="font-weight: bold; font-size: 1.2em"><spring:message code= "LB.W1039" /></font>
	</div>
	<br />
	<br />
	<label><spring:message code="LB.Inquiry_time" />：${CMQTIME}</label>
	<br />
	<label><spring:message code="LB.Total_records" />：${TOTRECNO}</label>
	<label><spring:message code="LB.Rows" /></label>
	<br />
	<table class="print" style="">
		<tr>
			<th><spring:message code="LB.Name" /></th>
			<th colspan="12" style="text-align: left">${NAME}</th>
		</tr>
		<tr>
			<th nowrap><spring:message code="LB.D0127"/></th>
			<th nowrap><spring:message code="LB.W1041"/></th>
			<th nowrap><spring:message code="LB.D0973"/></th>
			<th nowrap><spring:message code="LB.W1043"/>+<br> <spring:message code="LB.W1044"/>
			</th>
			<th nowrap><spring:message code="LB.D0173"/></th>
			<th nowrap><spring:message code="LB.W1046"/></th>
			<th nowrap><spring:message code="LB.W1047"/></th>
			<th nowrap><spring:message code="LB.W0906"/></th>
			<th nowrap><spring:message code="LB.W1049"/><br> <spring:message code="LB.W1050"/>
			</th>
			<th nowrap><spring:message code="LB.W1051"/></th>
			<th nowrap><spring:message code="LB.W1052"/></th>
			<th nowrap><spring:message code="LB.W1053"/></th>
			<th nowrap><spring:message code="LB.W0944"/><br> <spring:message code="LB.W1055"/>
			</th>
		</tr>
		<c:forEach items="${dataListMap}" var="map">
			<tr>
				<td class="text-center">${map.CONDAY}</td>
				<td class="text-center">${map.PAYTAG}</td>
				<td class="text-center"><spring:message code="${map.MIP}"/></td>
				<td class="text-center">${map.PAYCUR}<br>${map.PAYAMT}</td>
				<td class="text-center"><spring:message code="${map.DBTTYPE}"/></td>
				<td class="text-center">${map.PAYACNO}</td>
				<td class="text-center">${map.PAYDAY1} ${map.PAYDAY2} ${map.PAYDAY3}
					${map.PAYDAY4} ${map.PAYDAY5} ${map.PAYDAY6}
					${map.PAYDAY7} ${map.PAYDAY8} ${map.PAYDAY9}</td>
				<td class="text-center">${map.FSTDAY}</td>
				<td class="text-center">${map.STOPBEGDAY}<br>${map.STOPENDDAY}</td>
				<td class="text-center">${map.STPDAY}</td>
				<td class="text-center">
					<c:if test="${map.REMARK1 !=''}">
						<spring:message code="${map.REMARK1}"/>
					</c:if>
					<c:if test="${map.REMARK1 ==''}">
						${map.REMARK1}
					</c:if>
				</td>
				<td class="text-center">${map.REMARK2}</td>
				<td class="text-center">${map.CDNOCover}
				</td>
			</tr>
		</c:forEach>
	</table>
	<br />
	<br />
	<div class="text-left">
		<spring:message code="LB.Description_of_page" />
		<ol class="list-decimal text-left">
			<li><spring:message code="LB.Regular_Invest_P1_D1" /></li>
			<li><spring:message code="LB.Regular_Invest_P1_D2" /></li>
		</ol>
	</div>
</body>
</html>