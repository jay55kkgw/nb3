<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<script type="text/javascript">
	$(document).ready(function () {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
		// 將.table變更為footable
		//initFootable();
		setTimeout("initDataTable()",100);
	});
</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 修改好記名稱     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0226" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code="LB.D0226"/></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="">
				<div class="main-content-block row">
					<div class="col-12">
										<ul class="ttb-result-list">
						<li>
							<!-- 查詢時間 -->
							<h3><spring:message code="LB.System_time"/>：</h3>
							<p>
							${predesignated_account_modify.data.CMQTIME}
							</p>
						</li>
						<li>
							<!-- 查詢筆數 -->
							<h3><spring:message code="LB.D0191"/>：</h3>
							<p>
								<c:choose>
									<c:when test="${ predesignated_account_modify.data.MSG == '交易成功'}">
										<spring:message code="LB.X0920"/>
									</c:when>
									<c:otherwise>
										${predesignated_account_modify.data.MSG}
								  	</c:otherwise>
							  	</c:choose>
							</p>							
						</li>
					</ul>					
						<!-- 修改好記名稱-->
						<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
							<thead>
								<tr>
									<th>
										<spring:message code="LB.Bank_name"/>
									</th>
									<!-- 約定轉入帳號 -->
									<th>
										<spring:message code="LB.D0227"/>
									</th>
									<!-- 好記名稱 -->
									<th>
										<spring:message code="LB.Favorite_name" />
									</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>${predesignated_account_modify.data.BNKCOD}</td>
									<td>${predesignated_account_modify.data.ACN}</td>
									<td><c:out value='${predesignated_account_modify.data.DPGONAME}' /></td>
								</tr>						
							</tbody>
						</table>
					</div>
				</div>
			</form>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>