<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
	<head>
		<%@ include file="../__import_head_tag.jsp"%>
		<%@ include file="../__import_js.jsp" %>
	</head>
	<script type="text/javascript">
		$(document).ready(function(){
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		
		function init(){
			$("a").click(function(){
				if(this.id == "logobtn"){
					window.close();
				}
			});
		}
	</script>
	<body>
		<!-- header -->
		<header>
			<%@ include file="../index/header_logout.jsp"%>
		</header>
		
		<!-- 左邊menu 及登入資訊 -->
		<div class="content row">
			<main class="col-12">
				<section id="main-content" class="container">
					<!-- 主頁內容  -->
					<h2>
						<c:choose>
							<c:when test="${pageContext.response.locale=='zh_CN'}">
								最低申购投资金额
							</c:when>
							<c:when test="${pageContext.response.locale=='en'}">
								Minimum purchase investment amount
							</c:when>
							<c:otherwise>
								最低申購投資金額
							</c:otherwise>
						</c:choose>
					</h2>
					<form id="formId" method="post" action="">
						<div class="main-content-block row">
							<div class="col-12 tab-content">
								<div class="ttb-input-block">
									<c:choose>
										<c:when test="${pageContext.response.locale=='zh_CN'}">
											<div style="overflow: auto">
												<table class="question-table" style="width: 100%">
													<thead>
														<tr>
															<th colspan="3">投资目标</th>
															<th>最低投资金额</th>
															<th>增加单位</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<th rowspan="19">单笔</th>
															<th colspan="2">国内货币型基金</th>
															<td colspan="2">视个别基金规定（上午10：30截止申购作业）</td>
														</tr>
														<tr>
															<th rowspan="5">国内基金</th>
															<th>台币信托</th>
															<td>新台币10,000元</td>
															<td>新台币1,000元</td>
														</tr>
														<tr>
															<th rowspan="4" align="center" class="f13px lineH_01 coR">外币信托</th>
															<td>南非币10,000元</td>
															<td>南非币1,000元</td>
														</tr>
														<tr>
															<td>美元1,000元</td>
															<td>美元100元</td>
														</tr>
														<tr>
															<td>人民币5,000元</td>
															<td>人民币1,000元</td>
														</tr>
														<tr>
															<td>澳币1,000元</td>
															<td>澳币100元</td>
														</tr>
														<tr>
															<th rowspan="13">国外基金</th>
															<th>台币信托</th>
															<td>新台币30,000元</td>
															<td>新台币10,000元</td>
														</tr>
														<tr>
															<th rowspan="12">外币信托</th>
															<td>美元1,000元</td>
															<td>美元100元</td>
														</tr>
														<tr>
															<td>欧元1,000元</td>
															<td>欧元100元</td>
														</tr>
														<tr>
															<td>英镑1,000元</td>
															<td>英镑100元</td>
														</tr>
														<tr>
															<td>澳币1,000元</td>
															<td>澳币100元</td>
														</tr>
														<tr>
															<td>瑞士法郎1,000元</td>
															<td>瑞士法郎100元</td>
														</tr>
														<tr>
															<td>加拿大币1,000元</td>
															<td>加拿大币100元</td>
														</tr>
														<tr>
															<td>瑞典币10,000元</td>
															<td>瑞典币1,000元</td>
														</tr>
														<tr>
															<td>日圆100,000元</td>
															<td>日圆10,000元</td>
														</tr>
														<tr>
															<td>新加坡币1,000元</td>
															<td>新加坡币100元</td>
														</tr>
														<tr>
															<td>纽币1,000元</td>
															<td>纽币100元</td>
														</tr>
														<tr>
															<td>港币10,000元</td>
															<td>港币1,000元</td>
														</tr>
														<tr>
															<td>南非币10,000元</td>
															<td>南非币1,000元</td>
														</tr>
														<tr>
															<th rowspan="19">定额/不定额</th>
															<th colspan="2">国内货币型基金</th>
															<td colspan="2">暂未开放</td>
														</tr>
														<tr>
															<th colspan="2" rowspan="5">国内基金</th>
															<td>新台币3,000元</td>
															<td>新台币1,000元</td>
														</tr>
														<tr>
															<td>南非币2,000元</td>
															<td>南非币500元</td>
														</tr>
														<tr>
															<td>美元200元</td>
															<td>美元50元</td>
														</tr>
														<tr>
															<td>人民币1,000元</td>
															<td>人民币500元</td>
														</tr>
														<tr>
															<td>澳币200元</td>
															<td>澳币50元</td>
														</tr>
					
														<tr>
															<th rowspan="13">国外基金</th>
															<th>台币信托</th>
															<td>新台币3,000/5,000元</td>
															<td>新台币1,000元</td>
														</tr>
														<tr>
															<th rowspan="12">外币信托</th>
															<td>美元200/300元</td>
															<td>美元50元</td>
														</tr>
														<tr>
															<td>欧元200/300元</td>
															<td>欧元50元</td>
														</tr>
														<tr>
															<td>英镑200/300元</td>
															<td>英镑50元</td>
														</tr>
														<tr>
															<td>澳币200/300元</td>
															<td>澳币50元</td>
														</tr>
														<tr>
															<td>瑞士法郎200/300元</td>
															<td>瑞士法郎50元</td>
														</tr>
														<tr>
															<td>加拿大币200/300元</td>
															<td>加拿大币50元</td>
														</tr>
														<tr>
															<td>瑞典币2,000/3,000元</td>
															<td>瑞典币500元</td>
														</tr>
														<tr>
															<td>日圆20,000/30,000元</td>
															<td>日圆5,000元</td>
														</tr>
														<tr>
															<td>纽币200/300元</td>
															<td>纽币50元</td>
														</tr>
														<tr>
															<td>港币2,000元</td>
															<td>港币500元</td>
														</tr>
														<tr>
															<td>新加坡币200元</td>
															<td>新加坡币50元</td>
														</tr>
														<tr>
															<td>南非币2,000元</td>
															<td>南非币500元</td>
														</tr>
													</tbody>
												</table>
											</div>
										</c:when>
										<c:when test="${pageContext.response.locale=='en'}">
											<div style="overflow: auto">
												<table class="question-table" style="width: 100%">
													<thead>
														<tr>
															<th colspan="3">Investment targets</th>
															<th>Minimum <br>investment amount
															</th>
															<th>Increase unit</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<th rowspan="19">Single</th>
															<th colspan="2">Domestic Currency Fund</th>
															<td class="white-spacing" colspan="2">Subject
																to individual fund regulations (closed at 10:30 am)</td>
														</tr>
														<tr>
															<th rowspan="5">Domestic Fund</th>
															<th>Taiwan Dollar Trust</th>
															<td>NTD 10,000</td>
															<td>NTD 1,000</td>
														</tr>
														<tr>
															<th rowspan="4" align="center" class="f13px lineH_01 coR">
																Foreign Currency Trust</th>
															<td>ZAR 10,000</td>
															<td>ZAR 1,000</td>
														</tr>
														<tr>
															<td>USD 1,000</td>
															<td>USD 100</tt>
														</tr>
														<tr>
															<td>CNY 5,000</td>
															<td>CNY 1,000</td>
														</tr>
														<tr>
															<td>AUD 1,000</td>
															<td>AUD 100</td>
														</tr>
														<tr>
															<th rowspan="13">Foreign Fund</th>
															<th>Taiwan Dollar Trust</th>
															<td>NTD 30,000</td>
															<td>NTD 10,000</td>
														</tr>
														<tr>
															<th rowspan="12">Foreign Currency Trust</th>
															<td>USD 1,000</td>
															<td>USD 100</tt>
														</tr>
														<tr>
															<td>EUR 1,000</td>
															<td>EUR 100</td>
														</tr>
														<tr>
															<td>GBP 1,000</td>
															<td>GBP 100</td>
														</tr>
														<tr>
															<td>AUD 1,000</td>
															<td>AUD 100</td>
														</tr>
														<tr>
															<td>CHF 1,000</td>
															<td>CHF 100</td>
														</tr>
														<tr>
															<td>CAD 1,000</td>
															<td>CAD 100</td>
														</tr>
														<tr>
															<td>SEK 10,000</td>
															<td>SEK 1,000</td>
														</tr>
														<tr>
															<td>JPY 100,000</td>
															<td>JPY 10,000</td>
														</tr>
														<tr>
															<td>SGD 1,000</td>
															<td>SGD 100</td>
														</tr>
														<tr>
															<td>NZD 1,000</td>
															<td>NZD 100</td>
														</tr>
														<tr>
															<td>HKD 10,000</td>
															<td>HKD 1,000</td>
														</tr>
														<tr>
															<td>ZAR 10,000</td>
															<td>ZAR 1,000</td>
														</tr>
														<tr>
															<th rowspan="19">Quota / Irregular</th>
															<th colspan="2">Domestic Currency Fund</th>
															<td colspan="2">Not yet open</td>
														</tr>
														<tr>
															<th colspan="2" rowspan="5">Domestic Fund</th>
															<td>NTD 3,000</td>
															<td>NTD 1,000</td>
														</tr>
														<tr>
															<td>ZAR 2,000</td>
															<td>ZAR 500</td>
														</tr>
														<tr>
															<td>USD 200</tt>
															<td>USD 50</td>
														</tr>
														<tr>
															<td>RMB 1,000</td>
															<td>RMB 500</td>
														</tr>
														<tr>
															<td>AUD 200</td>
															<td>AUD 50</td>
														</tr>
				
														<tr>
															<th rowspan="13">Foreign Fund</th>
															<th>Taiwan Dollar Trust</th>
															<td>NTD 3,000/5,000</tt>
															<td>NTD 1,000</td>
														</tr>
														<tr>
															<th rowspan="12">Foreign Currency Trust</th>
															<td>USD 200/300</td>
															<td>USD 50</td>
														</tr>
														<tr>
															<td>EUR 200/300</td>
															<td>EUR 50</td>
														</tr>
														<tr>
															<td>GBP 200/300</td>
															<td>GBP 50</td>
														</tr>
														<tr>
															<td>AUD 200/300</td>
															<td>AUD 50</td>
														</tr>
														<tr>
															<td>CHF 200/300</td>
															<td>CHF 50</td>
														</tr>
														<tr>
															<td>CAD 200/300</td>
															<td>CAD 50</td>
														</tr>
														<tr>
															<td>SEK 2,000/3,000</td>
															<td>SEK 500</td>
														</tr>
														<tr>
															<td>JPY 20,000/30,000</td>
															<td>JPY 5,000</td>
														</tr>
														<tr>
															<td>NZD 200/300</td>
															<td>NZD 50</td>
														</tr>
														<tr>
															<td>HKD 2,000</td>
															<td>HKD 500</td>
														</tr>
														<tr>
															<td>SGD 200</td>
															<td>SGD 50</td>
														</tr>
														<tr>
															<td>ZAR 2,000</td>
															<td>ZAD 500</td>
														</tr>
													</tbody>
												</table>
											</div>
										</c:when>
										<c:otherwise>
											<div style="overflow: auto">
												<table class="question-table" style="width: 100%">
													<thead>
														<tr>
															<th colspan="3">投資標的</th>
															<th>最低投資金額</th>
															<th>增加單位</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<th rowspan="19">單筆</th>
															<th colspan="2">國內貨幣型基金</th>
															<td colspan="2">視個別基金規定（上午10：30截止申購作業）</td>
														</tr>
														<tr>
															<th rowspan="5">國內基金</th>
															<th>臺幣信託</th>
															<td>新臺幣10,000元</td>
															<td>新臺幣1,000元</td>
														</tr>
														<tr>
															<th rowspan="4" align="center" class="f13px lineH_01 coR">外幣信託</th>
															<td>南非幣10,000元</td>
															<td>南非幣1,000元</td>
														</tr>
														<tr>
															<td>美元1,000元</td>
															<td>美元100元</td>
														</tr>
														<tr>
															<td>人民幣5,000元</td>
															<td>人民幣1,000元</td>
														</tr>
														<tr>
															<td>澳幣1,000元</td>
															<td>澳幣100元</td>
														</tr>
														<tr>
															<th rowspan="13">國外基金</th>
															<th>臺幣信託</th>
															<td>新臺幣30,000元</td>
															<td>新臺幣10,000元</td>
														</tr>
														<tr>
															<th rowspan="12">外幣信託</th>
															<td>美元1,000元</td>
															<td>美元100元</td>
														</tr>
														<tr>
															<td>歐元1,000元</td>
															<td>歐元100元</td>
														</tr>
														<tr>
															<td>英鎊1,000元</td>
															<td>英鎊100元</td>
														</tr>
														<tr>
															<td>澳幣1,000元</td>
															<td>澳幣100元</td>
														</tr>
														<tr>
															<td>瑞士法郎1,000元</td>
															<td>瑞士法郎100元</td>
														</tr>
														<tr>
															<td>加拿大幣1,000元</td>
															<td>加拿大幣100元</td>
														</tr>
														<tr>
															<td>瑞典幣10,000元</td>
															<td>瑞典幣1,000元</td>
														</tr>
														<tr>
															<td>日圓100,000元</td>
															<td>日圓10,000元</td>
														</tr>
														<tr>
															<td>新加坡幣1,000元</td>
															<td>新加坡幣100元</td>
														</tr>
														<tr>
															<td>紐幣1,000元</td>
															<td>紐幣100元</td>
														</tr>
														<tr>
															<td>港幣10,000元</td>
															<td>港幣1,000元</td>
														</tr>
														<tr>
															<td>南非幣10,000元</td>
															<td>南非幣1,000元</td>
														</tr>
														<tr>
															<th rowspan="19">定額/不定額</th>
															<th colspan="2">國內貨幣型基金</th>
															<td colspan="2">暫未開放</td>
														</tr>
														<tr>
															<th colspan="2" rowspan="5">國內基金</th>
															<td>新臺幣3,000元</td>
															<td>新臺幣1,000元</td>
														</tr>
														<tr>
															<td>南非幣2,000元</td>
															<td>南非幣500元</td>
														</tr>
														<tr>
															<td>美元200元</td>
															<td>美元50元</td>
														</tr>
														<tr>
															<td>人民幣1,000元</td>
															<td>人民幣500元</td>
														</tr>
														<tr>
															<td>澳幣200元</td>
															<td>澳幣50元</td>
														</tr>
														<tr>
															<th rowspan="13">國外基金</th>
															<th>臺幣信託</th>
															<td>新臺幣3,000/5,000元</td>
															<td>新臺幣1,000元</td>
														</tr>
														<tr>
															<th rowspan="12">外幣信託</th>
															<td>美元200/300元</td>
															<td>美元50元</td>
														</tr>
														<tr>
															<td>歐元200/300元</td>
															<td>歐元50元</td>
														</tr>
														<tr>
															<td>英鎊200/300元</td>
															<td>英鎊50元</td>
														</tr>
														<tr>
															<td>澳幣200/300元</td>
															<td>澳幣50元</td>
														</tr>
														<tr>
															<td>瑞士法郎200/300元</td>
															<td>瑞士法郎50元</td>
														</tr>
														<tr>
															<td>加拿大幣200/300元</td>
															<td>加拿大幣50元</td>
														</tr>
														<tr>
															<td>瑞典幣2,000/3,000元</td>
															<td>瑞典幣500元</td>
														</tr>
														<tr>
															<td>日圓20,000/30,000元</td>
															<td>日圓5,000元</td>
														</tr>
														<tr>
															<td>紐幣200/300元</td>
															<td>紐幣50元</td>
														</tr>
														<tr>
															<td>港幣2,000元</td>
															<td>港幣500元</td>
														</tr>
														<tr>
															<td>新加坡幣200元</td>
															<td>新加坡幣50元</td>
														</tr>
														<tr>
															<td>南非幣2,000元</td>
															<td>南非幣500元</td>
														</tr>
													</tbody>
												</table>
											</div>
										</c:otherwise>
									</c:choose>
								</div>
								<input type="button" class="ttb-button btn-flat-orange" id="CLOSE" value="<spring:message code="LB.X0194" />" onclick="window.close();"/>
							</div>
						</div>
					</form>
				</section>
			</main>
		</div>
		<%@ include file="../index/footer.jsp"%>
	</body>
</html>