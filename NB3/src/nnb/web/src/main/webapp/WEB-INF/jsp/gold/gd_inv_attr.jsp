<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
<script type="text/javascript">
function processQuery(flag){
	var cusidn = "${UID}";
	
	if(flag == "Y"){
		if(cusidn.length == 10){
			$("#formID").attr("action","${__ctx}/FUND/TRANSFER/fund_invest_attr1");
		}
		else if(cusidn.length < 10){
			$("#formID").attr("action","${__ctx}/FUND/TRANSFER/fund_invest_attr1");
		}
	}
	if(flag == "C"){
		if(${TXID == 'N09001'}){
			$("#formID").attr("action","${__ctx}/GOLD/TRANSACTION/gold_buy_select");
		}
		if(${TXID == 'N09101'}){
			$("#formID").attr("action","${__ctx}/GOLD/TRANSACTION/gold_booking_select");
		}
		if(${TXID == 'N09002'}){
			$("#formID").attr("action","${__ctx}/GOLD/TRANSACTION/gold_resale_select");
		}
		if(${TXID == 'N09102'}){
			$("#formID").attr("action","${__ctx}/GOLD/TRANSACTION/gold_reserve_resale_select");
		}
	}
	if(flag == "D"){
		//alert("請客戶至往來分行填寫投資屬性問卷調查表或備妥本人之「臺灣企銀晶片金融卡＋讀卡機」，或電子簽章（憑證載具）於線上填寫投資屬性問卷調查表。");
		errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.Alert131' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
		);
		if(${TXID == 'N09001'}){
			$("#formID").attr("action","${__ctx}/GOLD/TRANSACTION/gold_buy_select");
		}
		if(${TXID == 'N09101'}){
			$("#formID").attr("action","${__ctx}/GOLD/TRANSACTION/gold_booking_select");
		}
		if(${TXID == 'N09002'}){
			$("#formID").attr("action","${__ctx}/GOLD/TRANSACTION/gold_resale_select");
		}
		if(${TXID == 'N09102'}){
			$("#formID").attr("action","${__ctx}/GOLD/TRANSACTION/gold_reserve_resale_select");
		}
	}                    
	$("#formID").submit();
}
</script>
</head>
<body>
<!--header-->
<header>
	<%@ include file="../index/header.jsp"%>
</header>
<!--麵包屑-->
<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
	<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
		<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
		<li class="ttb-breadcrumb-item active" aria-current="page">客戶投資屬性問卷調查</li>
	</ol>
</nav> 
<div class="content row">
<%@ include file="../index/menu.jsp"%>
	<main class="col-12"> 
	<!--主頁內容-->
		<section id="main-content" class="container">
			<h2>客戶投資屬性問卷調查</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form method="post" id="formID">
				<input type="hidden" name="TXID" value="${TXID}"/>
				<input type="hidden" name="KYC" value="PASS"/>
				<div class="main-content-block row">
					<div class="col-12">
						<c:if test="${ALERTTYPE == '1'}">
							<div class="col-12 tab-content">
								<ul class="ttb-result-list terms">
								<li>
									<span class="input-subtitle subtitle-color">親愛的客戶，您好：</span>
									<p>提醒您，您尚未辦理投資屬性測驗問卷（KYC），為配合金融消費者保護法施行，黃金存摺客戶須辦理本行了解客戶基本資料及投資屬性測驗問卷（KYC），便於辦理投資理財業務時檢視及評估所投資產品是否符合您可承受之風險範圍，確保該商品對於您之適合度，請您準備「讀卡機+本行晶片金融卡」或「電子簽章」，填寫「線上客戶投資屬性問卷調查表」，以確保您的投資權益。</p>
								</li>
								</ul>
							</div>
							<input type="button" value="立即填寫" onclick="processQuery('Y')" class="ttb-button btn-flat-orange"/>
							<c:if test="${TXID != 'N09301' && TXID != 'N09302' && TXID != 'NA50'}">
								<input type="button" value="繼續交易" onclick="processQuery('D')" class="ttb-button btn-flat-orange"/>
							</c:if>
						</c:if>
						<c:if test="${ALERTTYPE == '2'}">
							<div class="col-12 tab-content">
								<ul class="ttb-result-list terms">
									<li>
									<span class="input-subtitle subtitle-color">親愛的客戶，您好：</span>
									提醒您！您距上次填寫本行客戶基本資料及投資屬性測驗問卷（KYC）之時間已達1年，為配合金融消費者保護法施行，黃金存摺客戶須辦理本行了解客戶基本資料及投資屬性測驗問卷（KYC），便於辦理投資理財業務時檢視及評估所投資產品是否符合您可承受之風險範圍，確保該商品對於您之適合度，為重新評估您的投資屬性，請您填寫「線上客戶投資屬性問卷調查表」，以確保您的投資權益。
									</li>
								</ul>
							</div>
							<input type="button" value="立即填寫" onclick="processQuery('Y')" class="ttb-button btn-flat-orange"/>
							<c:if test="${TXID != 'N09301' && TXID != 'N09302' && TXID != 'NA50'}">
								<input type="button" value="稍後填寫" onclick="processQuery('C')" class="ttb-button btn-flat-orange"/>
							</c:if>
						</c:if>
						<c:if test="${ALERTTYPE == '3' && TXID == 'NA50'}">    
							<div class="col-12 tab-content">   
								<div class="ttb-input-block">
									親愛的客戶，您好：<br/>
									很抱歉！您尚未辦理投資屬性測驗問卷（KYC），為配合「金融消費者保護法」
									施行，黃金存摺客戶須辦理本行了解客戶基本資料及投資屬性測驗問卷（KYC）
									<c:if test="${TXID == 'NA50'}">
										方可辦理開戶
									</c:if>
									，便於辦理投資理財業務時檢視及評估所投資產品是否符合您
									可承受之風險範圍，確保該商品對於您之適合度，請您準備「讀卡機＋本行晶
									片金融卡」或「電子簽章」，填寫「線上客戶投資屬性問卷調查表」，以確保
									您的投資權益。
								</div>
							</div>
							<input type="button" value="立即填寫" onclick="processQuery('Y')" class="ttb-button btn-flat-orange"/>
						</c:if>
						<c:if test="${ALERTTYPE == '3' && (TXID == 'N09301' || TXID == 'N09302')}"> 
							<div class="col-12 tab-content">
								<div class="ttb-input-block">      
									<p>親愛的客戶，您好：</p>
									<p>
									很抱歉！您尚未辦理投資屬性測驗問卷（KYC），為配合「金融消費者保護法」
									施行，黃金存摺客戶須辦理本行了解客戶基本資料及投資屬性測驗問卷（KYC）
									，便於辦理投資理財業務時檢視及評估所投資產品是否符合您可承受之風險範
									圍，確保該商品對於您之適合度，請您準備「讀卡機＋本行晶片金融卡」或 
									「電子簽章」，填寫「線上客戶投資屬性問卷調查表」，以確保您的投資權益。</p>
								</div>
								<c:if test="${ALERTTYPE == '3' && TXID == 'NA50'}">    
									<font color="red">如不填寫線上客戶投資屬性問卷調查表，請客戶至往來分行填寫，且無法辦理線上開戶作業</font>
								</c:if>
								<c:if test="${ALERTTYPE == '3' && (TXID == 'N09301' || TXID == 'N09302')}"> 
									<font color="red">如不填寫線上客戶投資屬性問卷調查表，請客戶至往來分行填寫，且無法繼續定期定額申購或變更交易</font>
								</c:if>
							</div>
							<input type="button" value="立即填寫" onclick="processQuery('Y')" class="ttb-button btn-flat-orange"/>
						</c:if>
					</div>
				</div>
			</form>
		</section>
	</main>
</div>
<%@ include file="../index/footer.jsp"%>
</body>
</html>