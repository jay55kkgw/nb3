<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="BaseResultData" value="${f_deposit_cancel.data}"></c:set>

<!DOCTYPE html>
<html>

<head>
    <%@ include file="../__import_head_tag.jsp"%>
    <%@ include file="../__import_js.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
    <script type="text/javascript" src="${__ctx}/js/checkutil.js"></script>
    <script type="text/javascript">
	    $(document).ready(function() {
	    	// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
	    
	 	// 初始化
	    function init() {
	    	// 初始化後隱藏span( 表單驗證提示訊息用的 )
	    	$("#hideblock_CMTRDATE").hide();
			// 表單驗證
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
			
	    	// 將.table變更為DataTable
	 		initDataTable();
	 		// 西元年日期輸入欄位 click
	 		datetimepickerEvent();
			// 確認鍵 Click
			goOn();
	 	}
	 	
	 	// 確認鍵 Click
	    function goOn()
	    {
	    	// form Submit 
            $("#CMSUBMIT").click(function () {
                //打開驗證隱藏欄位
                $("#hideblock_CMTRDATE").show();
                //塞值進span內的input
                $("#validate_CMTRDATE").val($("#CMTRDATE").val());
					// jQuery表單驗證
//                 if (!$('#formId').validationEngine('validate')) {
//                     e.preventDefault();
                //檢查欄位
				var checkResult = processQuery();
                if (checkResult == false) {
					return false;
                } else {
                	$("#formId").validationEngine('detach');
                	initBlockUI(); //遮罩
                	$("#formId").submit();
                }
            });
	    	
	    	var defaultCheck = '${f_deposit_cancel.data.defaultCheck}';
	    	if(defaultCheck != null && defaultCheck != "" && defaultCheck == "TRUE"){
	    		$("input[name='FGSELECT']")[0].checked = true;
	    	}
	    }
    	
        // 舊式表單驗證 使用alert
	    function processQuery()
	    {
	    	var result = true;
	    	var nextSystemDate = "${nextSystemDate}";
	    	var nextYearDay = "${nextYearDay}";
	    	
	    	var radioflag = $('input[type=radio][name=FGSELECT]').is(':checked');
            console.log('radioflag :' + radioflag);
            if (!radioflag) {
                //錯誤!!尚未選取資料
                //alert('<spring:message code="LB.Field_check_note"/>');
                errorBlock(
							null, 
							null,
							["<spring:message code='LB.Field_check_note'/>"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
                return false;
            }
            
            var main = document.getElementById("formId");
            
         	if (!CheckMail("<spring:message code= "LB.e-mail" />", main.CMTRMAIL, true)) return false;
         	
            if (document.getElementById("DPTXDATE2").checked)
         	{
				if (!CheckDate("<spring:message code= "LB.X0377" />", main.CMTRDATE, nextSystemDate, nextYearDay)) 
				{
					return false;
				}
         	}
	    }
	    
        // 日曆欄位參數設定
        function datetimepickerEvent() {
            $(".CMTRDATE").click(function (event) {
                $('#CMTRDATE').datetimepicker('show');
            });

            jQuery('.datetimepicker').datetimepicker({
                timepicker: false,
                closeOnDateSelect: true,
                scrollMonth: false,
                scrollInput: false,
                format: 'Y/m/d',
                lang: '${transfer}'

            });
        }
        //copyTN
        function copyTN() {
            $("#CMMAILMEMO").val($("#CMTRMEMO").val());
        }

        //開啟通訊錄email選單
        function openAddressbook() {
            window.open('${__ctx}/NT/ACCT/TRANSFER/showAddressbook');
        }

    </script>
</head>

<body>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %> 
    <!-- header -->
    <header>
        <%@ include file="../index/header.jsp"%>
    </header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 定存服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Current_Deposit_To_Time_Deposit" /></li>
    <!-- 綜存定存解約     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.FX_Time_Deposit_Termination" /></li>
		</ol>
	</nav>



    <!-- menu、登出窗格 -->
    <div class="content row">
        <!-- 功能選單內容 -->
        <%@ include file="../index/menu.jsp"%>
        <!-- 主頁內容  -->
        <main class="col-12">
            <section id="main-content" class="container">
                <h2>
                    <!--外匯綜存定存解約 -->
                    <spring:message code="LB.FX_Time_Deposit_Termination" />
                </h2>
                <i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form autocomplete="off" id="formId" method="post" action="${__ctx}/FCY/ACCT/TDEPOSIT/f_deposit_cancel_confirm">
                    <!-- 表單顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
                            <ul class="ttb-result-list">
                                <!-- 查詢時間 -->
                                <li>
                                        <h3><spring:message code="LB.Inquiry_time" />:</h3>
                                        <p>${BaseResultData.CMQTIME }</p>
                                </li>
                                    <!-- 資料總數 : -->
                                <li>
                                       <h3><spring:message code="LB.Total_records" />:</h3>
                                       <p>${BaseResultData.COUNT }
                                        <!-- 筆 -->
                                        <spring:message code="LB.Rows" />
                                       </p>
                                </li>
                            </ul>
                                <!-- 表格區塊 -->
                                <table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
                                    <thead>
                                        <tr>
                                            <!--解約  -->
                                            <th>
                                                <spring:message code="LB.Termination" />
                                            </th>
                                            <!--帳號-->
                                            <th>
                                                <spring:message code="LB.Account" />
                                            </th>
                                            <!--存單號碼  -->
                                            <th>
                                                <spring:message code="LB.Account_no" />
                                            </th>
                                            <!--幣別  -->
                                            <th>
                                                <spring:message code="LB.Currency" />
                                            </th>
                                            <!--存單金額 -->
                                            <th>
                                                <spring:message code="LB.Certificate_amount" />
                                            </th>
                                            <!--利率(%)-->
                                            <th>
                                                <spring:message code="LB.Interest_rate1" />
                                            </th>
                                            <!--計息 方式-->
                                            <th>
                                                <spring:message code="LB.Interest_calculation" />
                                            </th>
                                            <!--起存日-->
                                            <th>
                                                <spring:message code="LB.Start_date" />
                                            </th>
                                            <!--到期日-->
                                            <th>
                                                <spring:message code="LB.Maturity_date" />
                                            </th>
                                            <!--自動轉期已轉次數/自動轉期未轉次數 -->
                                            <th>
                                                <spring:message code="LB.Automatic_number_of_rotations" /><hr/><spring:message code="LB.Automatic_number_of_unrotated" />
                                            </th>
                                            <!--狀態 -->
                                            <th>
                                                <spring:message code="LB.Status" />
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="dataList" items="${ BaseResultData.REC }">
                                            <tr>
                                                <td class="text-center">
													<label class="radio-block">
						                    			&nbsp; 
						                                <input type="radio" name="FGSELECT" value='${dataList.JSON}' />
						                                <span class="ttb-radio"></span> 
						                             </label> 
                                   				</td>
                                                <!-- 帳號 -->
                                                <td class="text-center"> ${dataList.ACN }</td>
                                                <!-- 存單號碼-->
                                                <td class="text-center"> ${dataList.FDPNUM }</td>
                                                <!-- 幣別-->
                                                <td class="text-center"> ${dataList.CUID }</td>
                                                <!-- 存單金額 -->
                                                <td class="text-right">${dataList.SHOW_AMT }</td>
                                                <!-- 利率(%) -->
                                                <td class="text-right"> ${dataList.ITR }</td>
                                                <!-- 計息方式 -->
                                                <td class="text-center"> ${dataList.SHOW_INTMTH }</td>
                                                <!-- 起存日 -->
                                                <td class="text-center"> ${dataList.SHOW_DPISDT }</td>
                                                <!-- 到期日-->
                                                <td class="text-center"> ${dataList.SHOW_DUEDAT }</td>
                                                <!-- 自動轉期已轉次數/自動轉期未轉次數 -->
                                                <td class="text-center"> ${dataList.ILAZLFTM }<hr/>${dataList.AUTXFTM }</td>
                                                <!-- 狀態 -->
                                                <td class="text-center"> ${dataList.EVTMARK }</td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                                <div class="ttb-input-block">
                                <!-- 轉帳日期 -->
                                <div class="ttb-input-item row">
                                    <span class="input-title">
                                        <label>
                                            <h4><spring:message code="LB.Transfer_date" /></h4>
                                        </label>
                                    </span>
                                    <span class="input-block">
                                        <div class="ttb-input">
                                            <!-- 即時 -->
                                            <label class="radio-block">
                                                <input name="FGTRDATE" id="DPTXDATE1" type="radio" value="0" checked />
                                                <spring:message code="LB.Immediately" />
                                                <span class="ttb-radio"></span>
                                            </label>
                                        </div>
                                        <!--預約 -->
                                        <div class="ttb-input ">
                                            <label class="radio-block">
                                                <spring:message code="LB.Booking" />
                                                <input name="FGTRDATE" id="DPTXDATE2" type="radio" value="1">
                                                <span class="ttb-radio"></span>
                                            </label>
                                        </div>
                                        <div class="ttb-input">
                                            <input type="text" id="CMTRDATE" name="CMTRDATE"
												value="${nextSystemDate }" class="text-input datetimepicker">
                                            <!--  要改用日曆元件 -->
                                            <span class="input-unit CMTRDATE">
                                                <img src="${__ctx}/img/icon-7.svg" />
                                            </span>
                                            <!-- 驗證用的span預設隱藏 -->
                                            <span id="hideblock_CMTRDATE">
                                                <!-- 驗證用的input -->
                                                <input id="validate_CMTRDATE" name="validate_CMTRDATE" type="text"
                                                    class="text-input validate[required, verification_date[validate_CMTRDATE]]"
                                                    style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
                                            </span>
                                        </div>
                                    </span>
                                </div>
                                    <!-- 交易備註 -->
                                    <div class="ttb-input-item row">
                                        <span class="input-title">
                                            <label>
                                                <h4><spring:message code="LB.Transfer_note" /></h4>
                                            </label>
                                        </span>
                                        <!--(可輸入20字的用途摘要，您可於「轉出紀錄查詢」功能中查詢) -->
                                        <span class="input-block">
                                            <div class="ttb-input">
												<input type="text" id="CMTRMEMO" name="CMTRMEMO" class="text-input" value="" maxlength="20" >
                                                <span class="input-unit">
                                                </span>
                                                <span class="input-remarks"><spring:message code="LB.Transfer_note_note" /></span>
                                            </div>
                                        </span>
                                    </div>
                                    <!-- 轉出成功 Email通知 -->
                                    <div class="ttb-input-item row">
                                        <!-- 轉出成功Email 通知 -->
                                        <span class="input-title">
                                            <label>
                                                <h4><spring:message code="LB.X0479" />：</h4>
                                            </label>
                                        </span>
                                        <span class="input-block">
                                            <!-- 通知本人 -->
                                            <div class="ttb-input">
                                                <span class="input-subtitle subtitle-color">
                                                    <spring:message code="LB.Notify_me" />：
                                                </span>
                                                <span class="input-subtitle subtitle-color">
                                                    ${sessionScope.dpmyemail}
                                                </span>
                                            </div>
                                            <!-- 另通知 -->
                                            <div class="ttb-input">
                                                <span class="input-subtitle subtitle-color">
                                                    <spring:message code="LB.Another_notice" />：
                                                </span>
                                            </div>
                                            <!-- 通訊錄 -->
                                            <div class="ttb-input">
												<!-- 非必填 -->
                                                <spring:message code="LB.Not_required" var="notrequired"></spring:message>
												<input type="text" id="CMTRMAIL" name="CMTRMAIL" class="text-input" 
                                                    placeholder="<spring:message code="LB.Not_required" />"
                                                    value="" maxlength="500" >
                                                <span class="input-unit"></span>
												<!-- window open 去查 TxnAddressBook  參數帶入身分證字號 -->
                                                <button class="btn-flat-orange" type="button" id="AddressbookBtn" onclick="openAddressbook()">
                                                    <spring:message code="LB.Address_book" /></button>
                                            </div>
                                            <div class="ttb-input">
                                                <!--摘要內容-->
                                                <spring:message code="LB.Summary" var="summary"></spring:message>
												<input type="text" id="CMMAILMEMO" name="CMMAILMEMO" class="text-input" 
													value="" maxlength="20" placeholder="${summary}" >
                                                <span class="input-unit">
                                                </span>
                                                <!-- 同交易備註 -->
                                                <button class="btn-flat-orange" type="button" onclick="copyTN()">
                                                    <spring:message code="LB.As_transfer_note" />
                                                </button>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                                <!-- 						button -->
                                    <!--重新輸入 -->
                                    <spring:message code="LB.Re_enter" var="cmRest"></spring:message>
                                    <input type="reset" name="CMRESET" id="CMRESET" value="${cmRest}" class="ttb-button btn-flat-gray">
                                    <!-- 確定 -->
                                    <spring:message code="LB.Confirm" var="cmSubmit"></spring:message>
                                    <input type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" class="ttb-button btn-flat-orange">
                                <!-- 						button -->
                            </div>
                        </div>
                        <div class="text-left">
                            <!-- 		說明： -->
                            <ol class="description-list list-decimal">
                            	<p><spring:message code="LB.Description_of_page" /></p>
                                <li><spring:message code="LB.F_deposit_cancel_P1_D1" /></li>
                                <li><spring:message code="LB.F_deposit_cancel_P1_D2" /></li>
                                <li>
                                    <spring:message code="LB.F_deposit_cancel_P1_D3" /><br>
                                    <spring:message code="LB.F_deposit_cancel_P1_D31" /><br>
                                    <spring:message code="LB.F_deposit_cancel_P1_D32" />
                                </li>
                                <li><spring:message code="LB.F_deposit_cancel_P1_D321" /></li>
                                <li><spring:message code="LB.F_deposit_cancel_P1_D322" /></li>
                                <li><spring:message code="LB.F_deposit_cancel_P1_D323" /></li>
                                <li><spring:message code="LB.F_deposit_cancel_P1_D324" /></li>
                                <li><spring:message code="LB.F_deposit_cancel_P1_D325" /></li>
                                <li><spring:message code="LB.F_deposit_cancel_P1_D4" /></li>
                            </ol>
                        </div>
                </form>
            </section>
        </main>
        <!-- main-content END -->
    </div>
    <!-- content row END -->
    <%@ include file="../index/footer.jsp"%>
</body>

</html>