<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">

	<script type="text/javascript">
	
	var idgatesubmit= $("#formId");
    var idgateadopid="N833";
    var idgatetitle="臺灣中小企銀自動扣繳查詢/取消-信用卡";
	
	$(document).ready(function() {
		// 開始跑下拉選單並完成畫面
		setTimeout("init()", 400);
		// 初始化驗證碼
		setTimeout("initKapImg()", 200);
		// 生成驗證碼
		setTimeout("newKapImg()", 300);

	});
	

    function init(){
    	$("#CMSUBMIT").click(function(e){			
			console.log("submit~~");
			var capResultData = false;
			if($('input[name="FGTXWAY"]:checked').val() == '1'){
				capResultData = true;
			}else if($('input[name="FGTXWAY"]:checked').val() == '2'){
				var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
				var capData = $("#formId").serializeArray();
				var capResult = fstop.getServerDataEx(capUri, capData, false);
				capResultData = capResult.result;
			}else if($('input[name="FGTXWAY"]:checked').val() == '7'){
				capResultData = true;
			}
			if (capResultData) {
				if(!$('#formId').validationEngine('validate') && $('input[name="FGTXWAY"]:checked').val() == 0){
		        	e.preventDefault();
	 			}else{
// 					$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
	 				$("#formId").validationEngine('detach');
    				var action = '${__ctx}/OTHER/FEE/withholding_cancel_C_result';
	    			$("form").attr("action", action);
	    			processQuery();
	 			}
			} else {
				//驗證碼有誤
				//alert("<spring:message code= "LB.X1082" />");
				errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.X1082' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
				changeCode();
			}
		});
    	
    	// 判斷顯不顯示驗證碼
		chaBlock();
		// 交易機制 click
		fgtxwayClick();
    }	

	// 交易機制選項
	function processQuery(){
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
		console.log("fgtxway: " + fgtxway);
	
		switch(fgtxway) {
			case '0':
//				alert("交易密碼(SSL)...");
    			$("form").submit();
				break;
				
			case '1':
//				alert("IKey...");
				var jsondc = $("#jsondc").val();
				
				useIKey();
				break;
				
			case '2':
//				alert("晶片金融卡");
				var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
				useCardReader(capUri);
		    	break;
			case '7'://IDGATE認證
				showIdgateBlock();
				break;
			default:
				//alert("nothing...");
				errorBlock(
					null, 
					null,
					["nothing..."], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
		}
		
	}
	// 使用者選擇晶片金融卡要顯示驗證碼區塊
 	function fgtxwayClick() {
 		$('input[name="FGTXWAY"]').change(function(event) {
 			// 判斷交易機制決定顯不顯示驗證碼區塊
 			chaBlock();
		});
	}
	// 判斷交易機制決定顯不顯示驗證碼區塊
	function chaBlock() {
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);	
			
			switch(fgtxway) {
			case '1':
				$("#capCodeDiv").hide();
				break;
			case '2':
				$("#capCodeDiv").show();
		    	break;
			case '7':
				$("#capCodeDiv").hide();
				break;
			default:
				$("#capCodeDiv").hide();
		}	
 	}
	
	
	// 驗證碼刷新
	function changeCode() {
		$('input[name="capCode"]').val('');
		// 大小版驗證碼用同一個
		console.log("changeCode...");
		$('img[name="kaptchaImage"]').hide().attr(
			'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

		// 登入失敗解遮罩
		unBlockUI(initBlockId);
	}

	// 初始化驗證碼
	function initKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
	}

	// 生成驗證碼
	function newKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').click(function () {
			$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100))
			.fadeIn();
		});
	}

	</script>
</head>
<body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	
	<!--   IDGATE -->
    <%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %>	
	
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 自動扣繳服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2360" /></li>
    <!-- 自動扣繳查詢/取消     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0768" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2><spring:message code="LB.W0768" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				
				<form id="formId" method="post" action="">
					<input type="hidden" name="ADOPID" id="ADOPID" value="N815">
					<input type="hidden" name="ITMNUM" id="ITMNUM" value="2">
					<input type="hidden" name="CARDNUM" id="CARDNUM" value="${withholding_cancel.CARDNUM}">
					<input type="hidden" name="TYPE" id="TYPE" value="${withholding_cancel.TYPE}">
					<input type="hidden" name="TYPE_str" id="TYPE_str" value="${withholding_cancel.TYPE_str}">
					<input type="hidden" name="CUSNUM" id="CUSNUM" value="${withholding_cancel.CUSNUM}">
				    <!-- 			晶片金融卡 -->
					<input type="hidden" id="jsondc" name="jsondc" value='${withholding_cancel.jsondc}'>
					<input type="hidden" id="ISSUER" name="ISSUER" value="">
					<input type="hidden" id="ACNNO" name="ACNNO" value="">
					<input type="hidden" id="TRMID" name="TRMID" value="">
					<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
					<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
					<input type="hidden" id="TAC" name="TAC" value="">
					<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
					<input type="hidden" id="TOKEN" name="TOKEN" value="${withholding_cancel.TOKEN}">

					<input type="hidden" name="ACN" value="">
					<input type="hidden" name="TSFACN" value="">
					<input type="hidden" name="OUTACN" value="">
					
					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-input-block">
								
								<!-- 交易機制 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.Inquiry_time" />
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p> ${ withholding_cancel.CMQTIME }</p>
										</div>
									</span>
								</div>
								<!-- 卡號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.Card_number" />
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p> ${ withholding_cancel.CARDNUM }</p>
										</div>
									</span>
								</div>
								<!-- 業務項目 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.X2265" />
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p> 
												<c:if test="${!withholding_cancel.TYPE_str.equals('')}">
													<spring:message code="${withholding_cancel.TYPE_str}" />
												</c:if>
											</p>
										</div>
									</span>
								</div>
								<!-- 用戶編號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.W0691" />
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p> ${ withholding_cancel.CUSNUM }</p>
										</div>
									</span>
								</div>
							
							
								<!-- 交易機制 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.Transaction_security_mechanism" />
										</label>
									</span>
									<span class="input-block">
										<!-- 使用者是否可以使用IKEY -->
										<c:if test="${sessionScope.isikeyuser}">
											<!--電子簽章(請載入載具i-key) -->
											<div class="ttb-input">
												<label class="radio-block">
													<spring:message code="LB.Electronic_signature" />
													<input type="radio" name="FGTXWAY" id="CMIKEY" value="1" />
													<span class="ttb-radio"></span>
												</label>
											</div>
										</c:if>
										<div class="ttb-input" name="idgate_group" style="display: none" >
											<label class="radio-block">裝置推播認證<input type="radio"
												id="IDGATE" name="FGTXWAY" value="7"> <span
												class="ttb-radio"></span></label>
										</div>
										<!-- 晶片金融卡 -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Financial_debit_card" />
												<input type="radio" name="FGTXWAY" id="CMCARD" value="2" checked="checked"/>
												<span class="ttb-radio"></span>
											</label>
										</div>
									</span>
								</div>
								<!-- 驗證碼  -->
								<div class="ttb-input-item row" id="capCodeDiv" style="display:none">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.Captcha" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
	                                    <div class="ttb-input">
	                                    <spring:message code="LB.Captcha" var="labelCapCode" />
											<input id="capCode" type="text" class="text-input input-width-125"
												name="capCode" placeholder="${labelCapCode}" maxlength="6" autocomplete="off">
											<img name="kaptchaImage" class="verification-img" src="" />
											<input type="button" name="reshow" class="ttb-sm-btn btn-flat-orange" onclick="refreshCapCode()" value="<spring:message code="LB.Regeneration"/>" />
	                                    	<span class="input-remarks"><spring:message code="LB.Captcha_refence" /></span>
	                                    </div>
									</span>
								</div>
							</div>
							<!-- 確定-->
							<input type="button" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
						</div>
					</div>
				</form>
			</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>

</body>
</html>