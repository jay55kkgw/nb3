<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript" src="${__ctx}/js/checkutil.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
		initFootable();
		$("#formId").validationEngine({
			binded: false,
			promptPosition: "inline"
		});
		$("#hideblock_C06").hide();
		$("#hideblock_C16").hide();
		$("#hideblock_C26").hide();
		$("#CMSUBMIT").click(function(e){
			var C06 = $("#AMT_06").val();
			var C16 = $("#AMT_16").val();
			var C26 = $("#AMT_26").val();
			
			//打開驗證隱藏欄位
			$("#hideblock_CA").show();
			if(C06.length > 0){
				$("#hideblock_C06").show();
			}
			if(C16.length > 0){
				$("#hideblock_C16").show();
			}
			if(C26.length > 0){
				$("#hideblock_C26").show();
			}
		
			e = e || window.event;
			if(!$('#formId').validationEngine('validate')){
	        	e.preventDefault();
	        }
	        else{
 				submit(); 
 			}		
  		});
		// 確認鍵 click
// 		submit();
	}
	
	// 確認鍵 Click
	function submit() {
// 		$("#CMSUBMIT").click( function(e) {
			console.log("submit~~");
			// 遮罩
         	initBlockUI();
            $("#formId").attr("action","${__ctx}/GOLD/AVERAGING/averaging_purchase_confirm");
	 	  	$("#formId").submit();
// 		});
	}
	
	//臺幣轉出帳號onchange
	function displayAcn() {
		uri = '${__ctx}' + "/GOLD/TRANSACTION/goldacn_ajax";
	    if ($("#SVACN").val() != "#") {
			rdata = {
				ACN : $("#SVACN").val()
			};
			
			console.log("creatOutAcn.uri: " + uri);
			console.log("creatOutAcn.rdata: " + rdata);
			
			data = fstop.getServerDataEx(uri, rdata, false,
					callback);
	    }	
	}
	
	function callback(data) {
		  console.log("data: " + data);
		  if (data) {
			  //ajax回傳資料型態: List<Map<String, String>>
			  console.log("data.json: " + JSON.stringify(data));
			  
			//先清空原有之"OPTION"內容,並加上一個 "---請選擇帳號---" 欄位
			$("#ACN").empty();
			$("#ACN").append( $("<option></option>").attr("value", "#").text("---<spring:message code="LB.Select_account"/>---"));
			
			//迴圈帶出"黃金轉入帳號"之下拉式選單	
			data.forEach(function(map) {
				console.log(map);
				$("#ACN").append( $("<option></option>").attr("value", map.ACN).text(map.ACN));
			});
		  }  
	  }

</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 定期定額交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1552" /></li>
    <!-- 定期定額申購     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1553" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<!-- 黃金定期定額申購 -->
			<section id="main-content" class="container">
				<h2><spring:message code= "LB.X0930" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form id="formId" method="post" action="">
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
                            
                             <!--約定扣款帳號-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.W1539" /></h4>
                                    </label></span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <select class="custom-select select-input half-input  validate[funcCall[validate_CheckSelect['<spring:message code= "LB.W1539" />',SVACN,#]]]" name="SVACN" id="SVACN" onchange="displayAcn()">
                                            <option value="#">---<spring:message code="LB.Select_account" />---</option>
                                            <c:forEach var="dataList" items="${averaging_purchase.data.SVACN}">
												<option> ${dataList.SVACN}</option>
											</c:forEach>
                                        </select>
                                    </div>
                                </span>
                            </div>

                             <!--黃金存摺帳號-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.D1090" /></h4>
                                    </label></span>
                                <span class="input-block">
									<div class="ttb-input">
                                        <select class="custom-select select-input half-input  validate[funcCall[validate_CheckSelect['<spring:message code= "LB.D1090" />',ACN,#]]]" name="ACN" id="ACN"">
                                            <option value="#">---<spring:message code="LB.Select_account" />---</option>
                                        </select>
                                    </div>
                                </span>
                            </div>
                              
							 <!--每月投資日/金額-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.W1557" /></h4>
                                    </label></span>
                                <span class="input-block">
                                	<span id="hideblock_CA"> 
										<!-- 驗證用的input 投資金額--> 
										<input id="checkAll" name="checkAll" type="text"
										class="text-input validate[funcCallRequired[validate_chkOne[<spring:message code= "LB.X1198" />,AMT_06,AMT_16,AMT_26]]]"
										style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;" />
									</span>
									<div class="ttb-input">
                                        <span class="input-subtitle subtitle-color">06<spring:message code="LB.Day" />&nbsp;<spring:message code="LB.NTD" /></span>
										<input type="text" class="text-input" value="" name="AMT_06" id="AMT_06">
										<span class="input-unit"><spring:message code="LB.Dollar" /></span>
                                     	<span id="hideblock_C06"> 
											<!-- 驗證用的input --> 
											<input id="check06" name="check06" type="text"
												class="text-input validate[funcCallRequired[validate_Check_Amount['06<spring:message code= "LB.Day" />' , AMT_06, 3000, null,1000]]]"
												style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;" />
										</span>
                                   	</div>
									<div class="ttb-input">
										<span class="input-subtitle subtitle-color">16<spring:message code="LB.Day" />&nbsp;<spring:message code="LB.NTD" /></span>
										<input type="text" class="text-input" value="" name="AMT_16" id="AMT_16">
										<span class="input-unit"><spring:message code="LB.Dollar" /></span>
										<span id="hideblock_C16"> 
											<!-- 驗證用的input --> 
											<input id="check16" name="check16" type="text"
												class="text-input validate[funcCallRequired[validate_Check_Amount['16<spring:message code= "LB.Day" />' , AMT_16, 3000, null,1000]]]"
												style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;" />
										</span>
									</div>
									<div class="ttb-input">
										 <span class="input-subtitle subtitle-color">26<spring:message code="LB.Day" />&nbsp;<spring:message code="LB.NTD" /></span>
										<input type="text" class="text-input" value="" name="AMT_26" id="AMT_26">
										<span class="input-unit"><spring:message code="LB.Dollar" /></span>
										<span id="hideblock_C26"> 
											<!-- 驗證用的input --> 
											<input id="check26" name="check26" type="text"
												class="text-input validate[funcCallRequired[validate_Check_Amount['26<spring:message code= "LB.Day" />' , AMT_26, 3000, null,1000]]]"
												style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;" />
										</span>
									</div>
								</span>
                            </div>
							
                        </div>
                        <input class="ttb-button btn-flat-gray" name="CMRESET" type="reset" value="<spring:message code="LB.Re_enter" />" />
                        <input class="ttb-button btn-flat-orange" id="CMSUBMIT" name="CMSUBMIT" type="button" value="<spring:message code="LB.Confirm" />" />
                        <input type="hidden" name="action" id="action" value="forward">
						<input type="hidden" name="urlPath" id="urlPath" value="">
						<input type="hidden" name="ADOPID" id="ADOPID" value="N09301">
						<input type="hidden" name="FGTXWAY" id="FGTXWAY" value="0">	
						<input type="hidden" name="DPMYEMAIL" id="DPMYEMAIL" value="${sessionScope.dpmyemail}">
                    </div>
                </div>
				</form>
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>
					<!--  若於投資日或投資日以後始申購定期定額投資者，自下次投資日起開始扣款。 -->
					<li><span><spring:message code="LB.Averaging_Purchase_P1_D1" /></span></li>
					<!-- 每次投資金額至少為新臺幣3,000元，且得為新臺幣1,000元之整倍數增加。 -->
			 		<li><span><spring:message code="LB.Averaging_Purchase_P1_D2" /></span></li>
			 		<!-- 網路銀行定期定額每次扣款優惠手續費為新臺幣50元，連同投資金額一併扣繳。 -->
					<li><span><spring:message code="LB.Averaging_Purchase_P1_D3" /></span></li>
					<!-- 黃金存摺不支付利息，黃金價格有漲有跌，投資時可能產生收益或損失，請慎選買賣時機，並承擔風險。 -->
			 		<li><span><spring:message code="LB.Averaging_Purchase_P1_D4" /></span></li>
			 		<!-- 每日累計買進最高交易數量為50,000公克。 -->
			 		<li><span><spring:message code="LB.Averaging_Purchase_P1_D5" /></span></li>
	            </ol>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>