<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
</head>
 <body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 掛失服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0441" /></li>
    <!-- 外幣存單掛失     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0445" /></li>
		</ol>
	</nav>



	
	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
	<main class="col-12"> 
	<!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<h2><spring:message code="LB.X0445" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<!-- 顯示區  -->
			<form method="post" id="formId">
				<div class="main-content-block row">
					<div class="col-12">
						<div class="ttb-message">
							<span><spring:message code="LB.X0458" /></span>
						</div>
                    	<!-- 表格區塊 -->
                        <table class="stripe table-striped ttb-table dtable" data-show-toggle="first">
                        	<thead>
                            	<tr>
                                	<!-- 勾選框 -->
                                	<th>
                                    	&nbsp;
                                   	</th>
                                	<!-- 狀態 -->
                                	<th data-title='<spring:message code="LB.Status" />'>
                                    	<spring:message code="LB.Status" />
                                   	</th>
                                    <!-- 帳號 -->
                                    <th data-title='<spring:message code="LB.Account" />'>
                                    	<spring:message code="LB.Account" />
                                    </th>
                                    <!-- 幣別 -->
                                    <th data-title='<spring:message code="LB.Currency" />'>
                                    	<spring:message code="LB.Currency" />
                                    </th>
                                    <!-- 存單金額 -->
                                    <th data-title='<spring:message code="LB.Certificate_amount" />'>
                                   		<spring:message code="LB.Certificate_amount" />
                                    </th>
                                    <!-- 存單號碼 -->
                                    <th data-title='<spring:message code="LB.Certificate_no" />'>
                                    	<spring:message code="LB.Certificate_no" />
                                    </th>
                                    <!-- 起存日 -->
                                    <th data-title='<spring:message code="LB.Start_date" />'>
                                    	<spring:message code="LB.Start_date" />
                                    </th>
                                    <!-- 到期日  -->
                                    <th data-title='<spring:message code="LB.Expired_date" />'>
                                    	<spring:message code="LB.Expired_date" />
                                    </th>
                                    <!-- 計息方式  -->
                                    <th data-title='<spring:message code="LB.Interest_calculation" />'>
                                    	<spring:message code="LB.Interest_calculation" />
                                    </th>
                                    <!-- 利率(%)  -->
                                    <th data-title='<spring:message code="LB.Interest_rate1" />'>
                                    	<spring:message code="LB.Interest_rate1" />
                                    </th>
                                    <!-- 已轉期數  -->
                                    <th data-title='<spring:message code="LB.Number_of_rotated" />'>
                                    	<spring:message code="LB.Number_of_rotated" />
                                    </th>
                                    <!-- 未轉期數  -->
                                    <th data-title='<spring:message code="LB.number_of_unrotated" />'>
                                    	<spring:message code="LB.number_of_unrotated" />
                                    </th>
                            	</tr>
                    		</thead>
                            <tbody>
                            	<c:if test="${empty reporting_loss_deposit_slip_fcy.data.REC}">
									<tr style="display:none;">
										<td></td>
									</tr>
								</c:if>
                            	<c:forEach var="dataList" items="${reporting_loss_deposit_slip_fcy.data.REC }">
                                	<tr>
                                		<!-- 勾選框 -->
										<td class="text-center">
											<label class="check-block">&nbsp;
												<input type="checkbox" name="ArrayParam1" value='${dataList.Value}'  
													<c:if test="${dataList.EVTMARK == '*'}">
														disabled
		                                        	</c:if>/>
                                        		<span class="ttb-check"></span>
                                        	</label>
                                        </td>
                                    	<!-- 狀態 -->
                                    	<td class="text-center">
                                    		<c:if test="${dataList.checkStatus == '已掛失'}">
                                    			<spring:message code="LB.X0461" />
                                    		</c:if>
                                    		<c:if test="${dataList.checkStatus != '已掛失'}">
                                    			${dataList.checkStatus }
                                    		</c:if>
                                    	</td>
                                        <!-- 帳號 -->
                                        <td class="text-center">${dataList.ACN }</td>
                                        <!-- 幣別 -->
                                        <td class="text-center">${dataList.CUID }</td>
                                        <!-- 存單金額 -->
                                        <td class="text-right">${dataList.BALANCE }</td>
                                        <!-- 存單號碼 -->
                                        <td class="text-center">${dataList.FDPNO }</td>
                                        <!-- 起存日 -->
                                        <td class="text-center">${dataList.DPISDT }</td>
                                        <!-- 到期日  -->
                                        <td class="text-center">${dataList.DUEDAT }</td>
                                        <!-- 計息方式  -->
                                        <td class="text-center"><spring:message code="${dataList.INTMTH }" /></td>
                                        <!-- 利率(%) -->
                                        <td class="text-right">${dataList.ITR }</td>
                                        <!-- 已轉期數  -->
                                        <td class="text-center">${dataList.ILAZLFTM }</td>
                                        <!-- 未轉期數  -->
                                        <td class="text-center">
                                        	<c:if test="${dataList.AUTXFTM == '無限次'}">
                                        		<spring:message code="LB.Unlimited" />
                                        	</c:if>
                                        	<c:if test="${dataList.AUTXFTM != '無限次'}">
                                        		${dataList.AUTXFTM }
                                        	</c:if>
                                        </td>
                                  	</tr>
                            	</c:forEach>
                    		</tbody>
                  		</table>
<%--                   		<table class="table" data-toggle-column="first"> --%>
<%-- 							<tr> --%>
<%-- 								<td style="vertical-align:middle"><spring:message code="LB.Transaction_security_mechanism" /></td> --%>
<%-- 								<td class="text-left"  style="text-align:left;"> --%>
<%-- 									<input type="radio" checked> <spring:message code="LB.SSL_password" />&nbsp;  --%>
<!-- 									<input class="text-input validate[required]" type="password" name="CMPASSWORD" id="CMPASSWORD" size="8" maxlength="8" autocomplete="off"> -->
<%-- 								</td> --%>
<%-- 							</tr> --%>
<%-- 						</table>	 --%>
                  		
                  		<!-- 交易機制 -->
						<div class="ttb-input-block">
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
										<h4>
											<spring:message code="LB.Transaction_security_mechanism" />
											<!-- 交易機制 -->
										</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block"> 
											<spring:message code="LB.SSL_password" />
											<input type="radio" checked> 
											<span class="ttb-radio"></span>
										</label>
										<input class="text-input validate[required]" type="password" name="CMPASSWORD" id="CMPASSWORD" size="8" maxlength="8" autocomplete="off">
									</div>
								</span>
							</div>
						</div>
                  		<input id="reset" name="reset" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" />
                  		<input type="button" class="ttb-button btn-flat-orange" id="CMSUBMIT" value="<spring:message code="LB.Confirm" />"/>
                  		<!-- 幣別,統編,掛失種類,?,?,SSL  -->
  						<input type="hidden" name="LOSTYPE" value="012">
  						<input type="hidden" name="ADOPID" value="N8421">
  						<input type="hidden" name="FGTXWAY" id="FGTXWAY" value="0">
  						<input type="hidden" name="NeedSHA1" value=" ">
    					<input type="hidden" name="PINNEW" id="PINNEW" value="">
    					<input type="hidden" name="ArrayParam" value="">
					</div>
				</div>
				</form>
				<div class="text-left">
					
					<ol class="list-decimal text-left description-list">
					<p><spring:message code="LB.Description_of_page" /></p>
						<li><spring:message code="LB.Demand_Deposit_slip_loss_fcy_P1_D1" />
          				<li><spring:message code="LB.Demand_Deposit_slip_loss_fcy_P1_D2" />
          				<li><spring:message code="LB.Demand_Deposit_slip_loss_fcy_P1_D3" />
          				<li><spring:message code="LB.Demand_Deposit_slip_loss_fcy_P1_D4" />
					</ol>
				</div>

<!-- 				</form> -->
			</section>
		<!-- 		main-content END -->
		</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
<!--   Js function -->
    <script type="text/JavaScript">
		$(document).ready(function() {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()",10);
			// 開始查詢資料並完成畫面
			setTimeout("init()",20);
			setTimeout("initDataTable()",100);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)",500);
		});
		function init(){				
			//initFootable();
// 			fgtxdateEvent();
			$("#formId").validationEngine({
				binded: false,
				promptPosition: "inline"
			});
			$("#CMSUBMIT").click(function(e){
				var checkflag = $('input[type=checkbox][name=ArrayParam1]').is(':checked') ;
				console.log('checkflag :' + checkflag);
				e = e || window.event;
				if(!checkflag){
					//alert("<spring:message code= "LB.Alert197" />!!!");
					errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert197' />" + "!!!"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
					e.preventDefault();
				}
				else if(!$('#formId').validationEngine('validate')){
		        	e.preventDefault();
		        }
		        else{
		        	var cbxVehicle = new Array();
		        	var main = document.getElementById("formId");
		        	$('input:checkbox:checked[name="ArrayParam1"]').each(function(i) { cbxVehicle[i] = this.value; });
		        	for(var x = 0; x < cbxVehicle.length -1; x++)
		        	{
		        		cbxVehicle[x]= cbxVehicle[x]+ ";";
		        	}
		        	main.ArrayParam.value = cbxVehicle;
	 				$("#formId").validationEngine('detach');
	 				initBlockUI();
	 				$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
	 				$("#formId").attr("action","${__ctx}/LOSS/SERVING/reporting_loss_deposit_slip_fcy_result");
	 	  			$("#formId").submit(); 
	 			}		
	  		});
	    }
		
 	</script>
</body>
</html>
