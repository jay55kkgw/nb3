<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:block>
<jsp:body>

<c:set var="__resPath" value="${__ctx}/site/admin/main"/>
<c:set var="__pageTitle" value="Form Test"/>

<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
    	<meta http-equiv="Pragma" content="no-cache" />
    	<meta http-equiv="Cache-Control" content="no-cache" />
		<meta http-equiv="Expires" CONTENT="-1">		
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta name="description" content="Site Administration Login Page" />
        <meta name="keywords" content="login, form, input, submit, button, html5, placeholder" />
        <title>${__pageTitle}</title>
        <link rel="shortcut icon" href="${__resPath}/dist/img/favicon.png" />
    	<!-- Bootstrap 3.3.2 -->
    	<link type="text/css" rel="stylesheet" href="${__resPath}/bootstrap/css/bootstrap.min.css"/>
        <link type="text/css" rel="stylesheet" href="${__resPath}/plugins/font-awesome/font-awesome.min.css">
        <style>
        </style>
 
         
    </head>
    <body>
    	<div class="container">    	
    			${__nav}
    	
    			${__pageHeader}
    		
    			${__content}
    		        
    			${__footer}
    			
    	</div> <!-- container -->
    	
    	<!-- jQuery 2.1.3 -->
    	<script type="text/javascript" src="${__resPath}/plugins/jQuery/jQuery-2.1.3.min.js"></script>
    	<!-- Bootstrap 3.3.2 JS -->
    	<script type="text/javascript" src="${__resPath}/bootstrap/js/bootstrap.min.js"></script>
    	<script type="text/javascript">
     	</script>
		${__js}
    	
    </body>
    
</html>

</jsp:body>
</t:block>
