<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %> 
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/checkIdentity.js"></script>
	
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
	<style>
	
		@media screen and (max-width:767px) {
		.ttb-button {
				width: 30%;
				left: 36px;
		}
		#CMBACK.ttb-button{
				border-color: transparent;
				box-shadow: none;
				color: #333;
				background-color: #fff;
		}
 

	}
	</style>
	<!-- 讀卡機所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/VAckisIEbrower.js"></script>
	<script type="text/javascript" src="${__ctx}/component/util/Pkcs11ATLAgent.js"></script>
	    <script type="text/javascript">
	    var notCheckIKeyUser = true; // 不驗證是否是IKey使用者
		//var myobj = null; // 自然人憑證用
		var urihost = "${__ctx}";
        $(document).ready(function () {
			// HTML載入完成後0.1秒開始遮罩
			setTimeout("initBlockUI()", 100);

			
			// 初始化驗證碼
			setTimeout("initKapImg()", 200);
			// 生成驗證碼
			setTimeout("newKapImg()", 300);
			

			setTimeout("init()", 20);
			// 過0.5秒解遮罩
			setTimeout("unBlockUI(initBlockId)", 800);
        });
//         var isconfirm = false;
//     	$("#errorBtn1").click(function(){
//     		if(!isconfirm)
//     			return false;
//     		isconfirm = false;
//     		$("#naturalComponent")[0].click();
//     	});
//     	$("#errorBtn2").click(function(){
//     		isconfirm = false;
//     		$('#error-block').hide();
//     	});

    	// 初始化自然人元件
//     	function initNatural() {
//     		var Brow1 = new ckBrowser1();
//     		if(!Brow1.isIE){	
//     			if((navigator.userAgent.indexOf("Chrome") > -1) || (navigator.userAgent.indexOf("Firefox") > -1) || (navigator.userAgent.indexOf("Safari") > -1)) {
//     				myobj = initCapiAgentForChrome();
//     				try {
//     					if(sessionToken == null){
//     						myobj.initChrome();
//     						console.log("myobj.initChrome...");
//     					}
//     				} catch (e) {
// //     					if(confirm("<spring:message code= "LB.X1216" />")){
// //     						$("#naturalComponent")[0].click();
// //     					}
//     					isconfirm = true;
// 						errorBlock(
// 								null, 
// 								null,
// 								['<spring:message code= "LB.X1216" />'], 
// 								'<spring:message code= "LB.Confirm" />', 
// 								'<spring:message code= "LB.Cancel" />'
// 							);
//     				}			
//     			}
//     		} else {
//     			var strObject = "";
//     	    	// 使用 Script 判斷 win64 或 win32, 執行時間有點久
//     	 		if (navigator.platform.toLowerCase() == "win64"){
//     				// "Windows 64 位元";
//     	 			document.getElementById("obj").outerHTML = '<object id="myobj" classid="CLSID:32FDE779-F9DC-4D39-97FB-434102000101" codebase="${__ctx}/component/windows/natural/TBBankPkcs11ATLx64.cab" width="0px" height="0px"></object>';
//     			} else {
//     				// "Windows 32 位元 或 模擬 64 位元(WOW64)";
//     				document.getElementById("obj").outerHTML = '<object id="myobj" classid="CLSID:32FDE779-F9DC-4D39-97FB-434102000101" codebase="${__ctx}/component/windows/natural/TBBankPkcs11ATL.cab" width="0px" height="0px"></object>';
//     			}
//     		    myobj = document.myobj;
//     		}
//     		console.log("initNatural.finish...");
//     	}
    	
    	
		/**
		 * 初始化BlockUI
		 */
		function initBlockUI() {
			initBlockId = blockUI();
		}

		// 交易機制選項
		function processQuery(){
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
		
			switch(fgtxway) {
				case '0':
//						alert("交易密碼(SSL)...");
	    			$("form").submit();
					break;
					
				case '1':
//						alert("IKey...");
					var jsondc = $("#jsondc").val();
					
					// 遮罩後不給捲動
//						document.body.style.overflow = "hidden";

					// 呼叫IKEY元件
//						uiSignForPKCS7(jsondc);
					
					// 解遮罩後給捲動
//						document.body.style.overflow = 'auto';
					
					useIKey();
					break;
					
				case '2':
//						alert("晶片金融卡");

					// 遮罩後不給捲動
//						document.body.style.overflow = "hidden";
					
					// 呼叫讀卡機元件
					useCardReader();

					// 解遮罩後給捲動
//						document.body.style.overflow = 'auto';
					
			    	break;

				case '4':
//					自然人憑證
					useNatural();
			    	break;
				case '5':
					//跨行帳戶驗證
					initBlockUI();
					$("#formId").submit();
					break;	
				case '6':
					//跨行帳戶驗證
					initBlockUI();
					$("#formId").submit();
					break;
				default:
					alert("nothing...");
			}
			
		}
		
        function init() {
	    	$("#CMSUBMIT").click(function(e){
				var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
				var capData = $("#formId").serializeArray();
				var capResult = fstop.getServerDataEx(capUri, capData, false);
				if (capResult.result) {
					if (!$('#formId').validationEngine('validate')) {
						e.preventDefault();
					} else {
						$("#formId").validationEngine('detach');
	// 		        	initBlockUI();
			        	var action = '${__ctx}/DIGITAL/ACCOUNT/modify_digital_data_result';
						$("form").attr("action", action);
		    			unBlockUI(initBlockId);
		    			processQuery();
					}
				} else {
					//驗證碼有誤
					//alert("<spring:message code= "LB.X1082" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= "LB.X1082" />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
					changeCode();
				}
			});
			$("#CMBACK").click(function(e){
	        	var action = '${__ctx}/DIGITAL/ACCOUNT/modify_digital_data_p2_2';
	        	$("#back").val('Y');
				$("form").attr("action", action);
    			$("form").submit();
			});
			
    		// 初始化自然人元件
			var acntype = "${input_data.data.FGTXWAY}";
			if (acntype == "4") {
				initNatural();
			}
	
        }

		function ValidateValue(textbox) {
			var IllegalString = "+-_＿︿%@[`~!#$^&*()=|{}':;',\\[\\].<>/?~！#￥……&*（）——|{}【】‘；：”“'。，、？]‘'";
			var textboxvalue = textbox.value;
			var index = textboxvalue.length - 1;
			var s = textbox.value.charAt(index);
			if (IllegalString.indexOf(s) >= 0) {
				s = textboxvalue.substring(0, index);
				textbox.value = s;
			}
		}
		
		// 刷新驗證碼
		function refreshCapCode() {
			console.log("refreshCapCode...");

			// 驗證碼
			$('input[name="capCode"]').val('');
			
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

			// 登入失敗解遮罩
			unBlockUI(initBlockId);
		}
		
		// 刷新輸入欄位
		function changeCode() {
			console.log("changeCode...");
			
			// 清空輸入欄位
			$('input[name="capCode"]').val('');
			
			// 刷新驗證碼
			refreshCapCode();
		}

		// 初始化驗證碼
		function initKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
		}

		// 生成驗證碼
		function newKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').click(function() {
				refreshCapCode();
			});
		}


		//取得卡片主帳號結束
		function getMainAccountFinish(result){
			//成功
			if(result != "false"){
				var cardACN = result;
				if(cardACN.length > 11){
					cardACN = cardACN.substr(cardACN.length - 11);
				}
				var UID = $("#CUSIDN").val();
//				var x = { method: 'get', parameters: '', onComplete: CheckIdResult };	
//				var myAjax = new Ajax.Request( "simpleform?trancode=N953&TXID=N953&ACN=" + cardACN, x);
				
				var uri = urihost+"/COMPONENT/component_without_id_aj";
				var rdata = { ACN: cardACN ,UID: UID };
				fstop.getServerDataEx(uri, rdata, true, CheckIdResult);
			}
			//失敗
			else{
				showTempMessage(500,"<spring:message code= "LB.X1250" />","","MaskArea",false);
			}
		}
    </script>

</head>
<body>
	<div id="obj"></div>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	<!-- 自然人憑證元件載點 -->
	<a href="${__ctx}/component/windows/natural/TBBankPkcs11ATL.exe" id="naturalComponent" target="_blank" style="display:none"></a>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 修改數位存款帳戶資料     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1474" /></li>
		</ol>
	</nav>

	
	<!-- 自然人憑證元件載點 -->
	<a href="${__ctx}/component/windows/natural/TBBankPkcs11ATL.exe" id="naturalComponent" target="_blank" style="display:none"></a>
	<div class="content row">
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!-- 修改數位存款帳戶資料 -->
				<h2>
					 <spring:message code= "LB.D1474" />
				</h2>
				<div id="step-bar">
                    <ul>
                        <li class="finished">身分驗證</li>
                        <li class="finished">開戶資料</li>
                        <li class="active">確認資料</li>
						<li class="">完成修改</li>
                    </ul>
                </div>
                <form id="formId" method="post">
					<input type="hidden" name="TXID" value="N104">
                    <!-- ikey -->
					<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
					<input type="hidden" id="jsondc" name="jsondc" value='${input_data.data.jsondc }'>
					<input type="hidden" name="CMTRANPAGE" value="1">
					<input type="hidden" id="CUSIDN" name="CUSIDN" value="${input_data.data.CUSIDN}">
					<input type="hidden" id="UID" name="UID" value="${input_data.data.CUSIDN}">
					<input type="hidden" id="BRHCOD" name="BRHCOD" value="${input_data.data.BRHCOD}">
					<input type="hidden" id="IP" name="IP" value="${input_data.data.IP}">
					<input type="hidden" id="ISSUER" name="ISSUER" value="">
					<input type="hidden" id="ACNNO" name="ACNNO" value="">
					<input type="hidden" id="TRMID" name="TRMID" value="">
					<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
					<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
					<input type="hidden" id="TAC" name="TAC" value="">
					<input type="hidden" id="OUTACN" name="OUTACN" value="">
					<input type="hidden" id="AUTHCODE" name="AUTHCODE" value="">
					<input type="hidden" id="AUTHCODE1" name="AUTHCODE1" value="">
					<input type="hidden" id="CODE" name="CODE" value="A">
					
					<input type="hidden" id="ACN" name="ACN" value="">
					<input type="hidden" id="NOTIEINSTALL" name="NOTIEINSTALL" value="">
					<input type="hidden" id="CertFinger" name="CertFinger" value="">
					<input type="hidden" id="CertB64" name="CertB64" value="">
					<input type="hidden" id="CertSerial" name="CertSerial" value="">
					<input type="hidden" id="CertSubject" name="CertSubject" value="">
					<input type="hidden" id="CertIssuer" name="CertIssuer" value="">
					<input type="hidden" id="CertNotBefore" name="CertNotBefore" value="">
					<input type="hidden" id="CertNotAfter" name="CertNotAfter" value="">
					<input type="hidden" id="HiCertType" name="HiCertType" value="">
					<input type="hidden" id="CUSIDN4" name="CUSIDN4" value="">
					<input type="hidden" id="CHIP_ACN" name="CHIP_ACN" value="">
					
					<input type="hidden" id="back" name="back" value="">
						
					<input type="hidden" id="TOKEN" name="TOKEN" value="${input_data.data.TOKEN}" /><!-- 防止重複交易 -->
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
                        
							<div class="ttb-input-block">
	                            <div class="ttb-message">
                                	<p>確認資料</p>
	                            </div>
								<p class="form-description">請再次確認您填寫的資料</p>
								<!-- ***************************************** -->
								<div class="classification-block">
	                                <p>開戶資訊</p>
<!-- 	                                <a href="#" class="classification-edit-btn">編輯</a> -->
	                            </div>
	                            
	                            <!-- ********************** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
<!-- 									本次開戶目的 -->
											<h4><spring:message code="LB.D1075"/></h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<c:if test="${ input_data.data.PURPOSE.equals('A') }">
<!-- 												儲蓄 -->
												<spring:message code="LB.D1076"/>
											</c:if>
											<c:if test="${ input_data.data.PURPOSE.equals('B') }">
<!-- 												薪轉 -->
												<spring:message code="LB.D1077"/>
											</c:if>
											<c:if test="${ input_data.data.PURPOSE.equals('C') }">
<!-- 												資金調撥 -->
												<spring:message code="LB.D1078"/>
											</c:if>
											<c:if test="${ input_data.data.PURPOSE.equals('D') }">
<!-- 												投資 -->
												<spring:message code="LB.D1079"/>
											</c:if>
											<c:if test="${ input_data.data.PURPOSE.equals('E') }">
<!-- 												其他： -->
												<spring:message code="LB.D0572"/>: ${input_data.data.PREASON}
											</c:if>
											<input type="hidden" name="PURPOSE" value="${input_data.data.PURPOSE}">
											<input type="hidden" name="PREASON" value="${input_data.data.PREASON}">
										</div>
									</span>
								</div>
								<!-- 與本行已往來業務 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D1391"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.BDEALING_str}
											<input type="hidden" name="BDEALING" value="${input_data.data.BDEALING}">
											<input type="hidden" name="BREASON" value="${input_data.data.BREASON}">
										</div>
									</span>
								</div>
								<!--主要資金/財產來源 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D1397"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<c:if test="${ input_data.data.MAINFUND.equals('A') }">
<!-- 												薪資 -->
												<spring:message code="LB.D1398"/>
											</c:if>
											<c:if test="${ input_data.data.MAINFUND.equals('B') }">
<!-- 												儲蓄 -->
												<spring:message code="LB.D1076"/>
											</c:if>
											<c:if test="${ input_data.data.MAINFUND.equals('C') }">
<!-- 												營運 -->
												<spring:message code="LB.D1399"/>
											</c:if>
											<c:if test="${ input_data.data.MAINFUND.equals('D') }">
<!-- 												投資 -->
												<spring:message code="LB.D1079"/>
											</c:if>
											<c:if test="${ input_data.data.MAINFUND.equals('E') }">
<!-- 												銷售 -->
												<spring:message code="LB.D1400"/>
											</c:if>
											<c:if test="${ input_data.data.MAINFUND.equals('F') }">
<!-- 												家業繼承 -->
												<spring:message code="LB.D1401"/>
											</c:if>
											<c:if test="${ input_data.data.MAINFUND.equals('G') }">
<!-- 												退休金 -->
												<spring:message code="LB.D0910"/>
											</c:if>
											<c:if test="${ input_data.data.MAINFUND.equals('H') }">
<!-- 												其他： -->
												<spring:message code="LB.D0572"/>: ${input_data.data.MREASON}
											</c:if>
											<input type="hidden" name="MAINFUND" value="${input_data.data.MAINFUND}">
											<input type="hidden" name="MREASON" value="${input_data.data.MREASON}">
										</div>
									</span>
								</div>
								<!-- 個人/家庭 年收入 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.X0170"/><spring:message code="LB.D0625_1"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.SALARY}<spring:message code="LB.D1144"/>
											<input type="hidden" name="SALARY" value="${input_data.data.SALARY}">
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 										預期往來金額(近一年) -->
											<spring:message code="LB.D1081_1"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.SMONEY} <spring:message code="LB.X0423"/>
											<input type="hidden" name="SMONEY" value="${input_data.data.SMONEY}">
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 										推薦碼 -->
											推薦碼
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.EMPNO}
											<input type="hidden" name="EMPNO" value="${input_data.data.EMPNO}">
										</div>
									</span>
								</div>
	                            <div class="classification-block">
	                                <p>基本資料</p>
<!-- 	                                <a href="#" class="classification-edit-btn">編輯</a> -->
	                            </div>
								<!-- ***************************************** -->
								<!-- 中文戶名 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D1105"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.CUSNAME}
											<input type="hidden" name="CUSNAME" value="${input_data.data.CUSNAME}">
											<input type="hidden" name="RENAME" value="${input_data.data.RENAME}">
										</div>
									</span>
								</div>
								<!-- 身分證統一編號 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0519"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.CUSIDN}
											<input type="hidden" name="UID" id="UID" value="${input_data.data.CUSIDN}">
										</div>
									</span>
								</div>
								<!-- ********** -->
								<!-- 出生日期 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0582"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
<!-- 											民國 -->
											<spring:message code="LB.D0583"/>&nbsp;${input_data.data.BIRTHDAY.substring(0,3)}<spring:message code="LB.Year"/>&nbsp;${input_data.data.BIRTHDAY.substring(3,5)}<spring:message code="LB.Month"/>&nbsp;${input_data.data.BIRTHDAY.substring(5,7)}<spring:message code="LB.D0586"/>&nbsp;
											<input type="hidden" name="BIRTHDAY" value="${input_data.data.BIRTHDAY}">
										</div>
									</span>
								</div>
								<!-- ********** -->
								<!-- 身分證發證資訊 -->
<!-- 								<div class="ttb-input-item row"> -->
<!-- 									<span class="input-title">  -->
<!-- 									<label> -->
<!-- 										<h4> -->
<!-- 											身分證發證資訊 -->
<!-- 										</h4> -->
<!-- 									</label> -->
<!-- 									</span>  -->
<!-- 									<span class="input-block"> -->
<!-- 										<div class="ttb-input"> -->
<%-- 											<spring:message code="LB.D0583"/>&nbsp;${input_data.data.CHGE_DT.substring(0,3)}<spring:message code="LB.Year"/>&nbsp;${input_data.data.CHGE_DT.substring(3,5)}<spring:message code="LB.Month"/>&nbsp;${input_data.data.CHGE_DT.substring(5,7)}<spring:message code="LB.D0586"/>&nbsp; --%>
<%-- 											(${input_data.data.CHGE_CYNAME}) --%>
<%-- 											<c:if test="${ input_data.data.ID_CHGE.equals('1') }"> --%>
<!-- <!-- 												初發 --> 
<%-- 												<spring:message code="LB.B0002"/> --%>
<%-- 											</c:if> --%>
<%-- 											<c:if test="${ input_data.data.ID_CHGE.equals('3') }"> --%>
<!-- <!-- 												換發 -->
<%-- 												<spring:message code="LB.W1664"/> --%>
<%-- 											</c:if> --%>
<%-- 											<c:if test="${ input_data.data.ID_CHGE.equals('2') }"> --%>
<!-- <!-- 												補發 -->
<%-- 												<spring:message code="LB.W1665"/> --%>
<%-- 											</c:if> --%>
<%-- 											<input type="hidden" name="ID_CHGE" value="${input_data.data.ID_CHGE}"> --%>
<%-- 											<input type="hidden" name="CHGE_DT" value="${input_data.data.CHGE_DT}"> --%>
<%-- 											<input type="hidden" name="CHGE_CY" value="${input_data.data.CHGE_CY}"> --%>
<%-- 											<input type="hidden" name="HAS_PIC" value="${input_data.data.HAS_PIC}"> --%>
<!-- 										</div> -->
<!-- 									</span> -->
<!-- 								</div> -->
								<!-- ********** -->
<!-- 								<div class="ttb-input-item row"> -->
<!-- 									<span class="input-title">  -->
<!-- 									<label> -->
<!-- 										<h4> -->
<!-- 										身分證有無相片 -->
<%-- 											<spring:message code="LB.D1115"/> --%>
<!-- 										</h4> -->
<!-- 									</label> -->
<!-- 									</span>  -->
<!-- 									<span class="input-block"> -->
<!-- 										<div class="ttb-input"> -->
<%-- 											<c:if test="${ input_data.data.HAS_PIC.equals('Y') }"> --%>
<%-- 												<spring:message code="LB.D1070_1"/> --%>
<%-- 											</c:if> --%>
<%-- 											<c:if test="${ input_data.data.HAS_PIC.equals('N') }"> --%>
<%-- 												<spring:message code="LB.D1070_2"/> --%>
<%-- 											</c:if> --%>
<!-- 										</div> -->
<!-- 									</span> -->
<!-- 								</div> -->
								<!--學歷 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D1136"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<c:if test="${ input_data.data.DEGREE.equals('1') }">
<!-- 												博士 -->
												<spring:message code="LB.D0588"/>
											</c:if>
											<c:if test="${ input_data.data.DEGREE.equals('2') }">
<!-- 												碩士 -->
												<spring:message code="LB.D0589"/>
											</c:if>
											<c:if test="${ input_data.data.DEGREE.equals('3') }">
<!-- 												大學 -->
												<spring:message code="LB.D0590"/>
											</c:if>
											<c:if test="${ input_data.data.DEGREE.equals('4') }">
<!-- 												專科 -->
												<spring:message code="LB.D0591"/>
											</c:if>
											<c:if test="${ input_data.data.DEGREE.equals('5') }">
<!-- 												高中高職 -->
												<spring:message code="LB.D1141"/>
											</c:if>
											<c:if test="${ input_data.data.DEGREE.equals('6') }">
<!-- 												國中以下 -->
												<spring:message code="LB.D0866"/>
											</c:if>
											<input type="hidden" name="DEGREE" value="${input_data.data.DEGREE}">
										</div>
									</span>
								</div>
								<!--婚姻狀況 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0057"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<c:if test="${ input_data.data.MARRY.equals('0') }">
<!-- 												未婚 -->
												<spring:message code="LB.D0596"/>
											</c:if>
											<c:if test="${ input_data.data.MARRY.equals('1') }">
<!-- 												已婚 -->
												<spring:message code="LB.D0595"/>
											</c:if>
											<c:if test="${ input_data.data.MARRY.equals('2') }">
<!-- 												其他 -->
												<spring:message code="LB.D0572"/>
											</c:if>
											<input type="hidden" name="MARRY" value="${input_data.data.MARRY}">
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 											子女人數 -->
											<spring:message code="LB.D1149"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<c:if test="${!input_data.data.CHILD.equals('')}">
												${input_data.data.CHILD}人
											</c:if>
											<input type="hidden" name="CHILD" value="${input_data.data.CHILD}">
										</div>
									</span>
								</div>
								<!-- ***************************************** -->
	                            <div class="classification-block">
	                                <p>聯絡資訊</p>
<!--                                 	<a href="#" class="classification-edit-btn">編輯</a> -->
	                            </div>
								<!-- ***************************************** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											E-mail
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.MAILADDR}
											<input type="hidden" name="MAILADDR" value="${input_data.data.MAILADDR}">
										</div>
									</span>
								</div>
								<!-- 行動電話 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0069"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.CELPHONE}
											<input type="hidden" name="CELPHONE" value="${input_data.data.CELPHONE}">
										</div>
									</span>
								</div>
								<!--通訊地電話 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.B0020"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.HOMEZIP}-${input_data.data.HOMETEL}
											<input type="hidden" name="ARACOD1" value="${input_data.data.HOMEZIP}">
											<input type="hidden" name="TELNUM1" value="${input_data.data.HOMETEL}">
											<input type="hidden" name="HOMEZIP" value="${input_data.data.HOMEZIP}">
											<input type="hidden" name="HOMETEL" value="${input_data.data.HOMETEL}">
										</div>
									</span>
								</div>
								<!--公司電話 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0094"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.BUSZIP}-${input_data.data.BUSTEL}
											<input type="hidden" name="ARACOD2" value="${input_data.data.BUSZIP}">
											<input type="hidden" name="TELNUM2" value="${input_data.data.BUSTEL}">
											<input type="hidden" name="BUSZIP" value="${input_data.data.BUSZIP}">
											<input type="hidden" name="BUSTEL" value="${input_data.data.BUSTEL}">
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<!--傳真 -->
											<spring:message code="LB.D1131"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.ARACOD3}-${input_data.data.FAX}
											<input type="hidden" name="ARACOD3" value="${input_data.data.ARACOD3}">
											<input type="hidden" name="FAX" value="${input_data.data.FAX}">
										</div>
									</span>
								</div>
								<!-- 戶籍地址 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0143"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.POSTCOD1} ${input_data.data.PMTADR}
											<input type="hidden" name="POSTCOD1" value="${input_data.data.POSTCOD1}">
											<input type="hidden" name="PMTADR" value="${input_data.data.PMTADR}">
										</div>
									</span>
								</div>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 										通訊地址 -->
											<spring:message code="LB.D0376"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.POSTCOD2} ${input_data.data.CTTADR}
											<input type="hidden" name="POSTCOD2" value="${input_data.data.POSTCOD2}">
											<input type="hidden" name="CTTADR" value="${input_data.data.CTTADR}">
										</div>
									</span>
								</div>
								<!-- ***************************************** -->
	                            <div class="classification-block">
	                                <p>職業資訊</p>
<!-- 	                                <a href="#" class="classification-edit-btn">編輯</a> -->
	                            </div>
								<!-- ***************************************** -->
								<!--職業 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D1132"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.CAREER1_str}
											<input type="hidden" name="CAREER1" value="${input_data.data.CAREER1}">
											
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0087"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
<%-- 											${input_data.data.CAREER2_str} --%>
											<c:if test="${ input_data.data.CAREER2.equals('1') }">
												<spring:message code="LB.X0179"/>
											</c:if>
											<c:if test="${ input_data.data.CAREER2.equals('2') }">
												<spring:message code="LB.X0180"/>
											</c:if>
											<c:if test="${ input_data.data.CAREER2.equals('3') }">
												<spring:message code="LB.X0181"/>
											</c:if>
											<c:if test="${ input_data.data.CAREER2.equals('4') }">
												<spring:message code="LB.X0182"/>
											</c:if>
											<c:if test="${ input_data.data.CAREER2.equals('5') }">
												<spring:message code="LB.X0183"/>
											</c:if>
											<c:if test="${ input_data.data.CAREER2.equals('6') }">
												<spring:message code="LB.X0184"/>
											</c:if>
											<c:if test="${ input_data.data.CAREER2.equals('7') }">
												<spring:message code="LB.X0185"/>
											</c:if>
											<c:if test="${ input_data.data.CAREER2.equals('8') }">
												<spring:message code="LB.X0186"/>
											</c:if>
											<c:if test="${ input_data.data.CAREER2.equals('9') }">
												<spring:message code="LB.X0187"/>
											</c:if>
											<input type="hidden" name="CAREER2" value="${input_data.data.CAREER2}">
										</div>
									</span>
								</div>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 										任職機構 -->
											<spring:message code="LB.D1074"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${input_data.data.EMPLOYER}
											<input type="hidden" name="EMPLOYER" value="${input_data.data.EMPLOYER}">
											<input type="hidden" name="CONPNAM" value="${input_data.data.EMPLOYER}">
										</div>
									</span>
								</div>
								
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											您所任職機構是否涉及高風險項目
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<c:if test="${ input_data.data.HIRISK0.equals('N') }">
												否
											</c:if>
											<c:if test="${ input_data.data.HIRISK0.equals('Y') }">
												是
											</c:if>
										</div>
									</span>
								</div>
								<c:if test="${ input_data.data.HIRISK0.equals('Y') }">
									<div class="ttb-input-item row">
										<span class="input-title"> 
										<label>
											<h4>
												項目內容
											</h4>
										</label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												<c:if test="${ input_data.data.HIRISK.equals('E') }">
													第三方支付服務業
												</c:if>
												<c:if test="${ input_data.data.HIRISK.equals('F') }">
													油品海運貿易業
												</c:if>
												<input type="hidden" name="HIRISK" value="${input_data.data.HIRISK}">
											</div>
										</span>
									</div>
								</c:if>
								
	                            <!-- ********************** -->
	                            
								<!-- *********************************** -->
	                            
								<!-- *********************************** -->
								
<!-- 								<div class="ttb-input-item row"> -->
<!-- 									<span class="input-title">  -->
<!-- 										<label> -->
<!-- 											<h4> -->
<%-- 												<spring:message code="LB.D1431_3"/> --%>
<!-- 											</h4> -->
<!-- 										</label> -->
<!-- 									</span>  -->
<!-- 									<span class="input-block"> -->
<!-- 										<div class="ttb-input"> -->
<%-- 											<c:if test="${ input_data.data.CARDAPPLY.equals('Y') }"> --%>
<!-- <!-- 													是 --> 
<%-- 												<spring:message code="LB.D0034_2"/> --%>
<%-- 											</c:if> --%>
<%-- 											<c:if test="${ input_data.data.CARDAPPLY.equals('N') }"> --%>
<!-- <!-- 													否 --> 
<%-- 												<spring:message code="LB.D0034_3"/> --%>
<%-- 											</c:if> --%>
<!-- 										</div> -->
<!-- 									</span>  -->
<!-- 								</div> -->
								
								<!-- *********************************** -->
								
								<!-- ***************** image ****************** -->
								<!-- 確認新使用者名稱 -->
<!-- 								<div class="ttb-input-item row"> -->
<!-- 									<span class="input-title">  -->
<!-- 										<label> -->
<!-- 											<h4> -->
<!-- <!-- 												驗證碼 -->
<%-- 												<spring:message code="LB.Captcha" /> --%>
<!-- 											</h4> -->
<!-- 										</label> -->
<!-- 									</span>  -->
<!-- 									<span class="input-block"> -->
<!-- 										<div class="ttb-input"> -->
											
<%-- 											<spring:message code="LB.Captcha" var="labelCapCode" /> --%>
<!-- 											<input id="capCode" type="text" class="ttb-input" -->
<%-- 												name="capCode" placeholder="${labelCapCode}" maxlength="6" autocomplete="off"> --%>
<!-- 											<img name="kaptchaImage" src="" /> -->
<%-- 											&nbsp;<font color="#FF0000"><spring:message code="LB.Captcha_refence" /> </font> --%>
<!-- 											<div class="login-input-block"> -->
<%-- 												<input type="button" name="reshow" class="ttb-sm-btn btn-flat-orange" onclick="refreshCapCode()" value="<spring:message code="LB.Regeneration"/>" /> --%>
<!-- 											</div> -->
<!-- 										</div> -->
<!-- 									</span> -->
<!-- 								</div> -->
								
						
								
                           	<div class="classification-block">
	                                <p>身分驗證</p>
	                            </div>
								<!-- *********************************** -->
								
							<!--交易機制  SSL 密碼 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.B0001" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<c:if test="${input_data.data.FGTXWAY.equals('4')}">
											<!-- 自然人憑證 -->
											<div class="ttb-input">
												<label class="radio-block">
<!-- 													自然人憑證 -->
													<spring:message code="LB.D0437"/>
												 	<input type="radio" name="FGTXWAY" id="CMNPC" value="4" checked="checked">
														
													<span class="ttb-radio"></span>
													<input type="hidden" name="ACNTYPE" value="A1">
												</label>
											</div>
										</c:if>
										<c:if test="${input_data.data.FGTXWAY.equals('2')}">
											<!-- 晶片金融卡 -->
											<div class="ttb-input">
												<label class="radio-block">
													<spring:message code="LB.Financial_debit_card" />
													
												 	<input type="radio" name="FGTXWAY" id="CMCARD" value="2" checked="checked">
														
													<span class="ttb-radio"></span>
													<input type="hidden" name="ACNTYPE" value="A2">
												</label>
											</div>
										</c:if>
										<c:if test="${ input_data.data.FGTXWAY.equals('5') }">
											<!-- 跨行金融帳戶認證 -->
											<div class="ttb-input">
												<label class="radio-block" > 
													<spring:message code="LB.Fisc_inter-bank_acc" /> 
													<input type="radio" name="FGTXWAY" id="CMFISCACC" value="5"  checked="checked"> 
													<span class="ttb-radio"></span>
													<input type="hidden" name="ACNTYPE" value="A3">
												</label>
											</div>
										</c:if>
										<c:if test="${input_data.data.FGTXWAY.equals('6')}">
											<!-- 跨行金融帳戶認證 -->
											<div class="ttb-input">
												<label class="radio-block">
													自行帳戶認證 
												 	<input type="radio" name="FGTXWAY" id="LOISCACC" value="6" checked="checked">														
													<span class="ttb-radio"></span>
													<input type="hidden" name="ACNTYPE" value="A4">
												</label>
											</div>
										</c:if>										
										
									</span>
								</div>
								<!-- 確認新使用者名稱 -->
<!-- 								<div class="ttb-input-item row"> -->
<!-- 									<span class="input-title">  -->
<!-- 										<label> -->
<!-- 											<h4> -->
<!-- <!-- 												驗證碼 -->
<%-- 												<spring:message code="LB.Captcha" /> --%>
<!-- 											</h4> -->
<!-- 										</label> -->
<!-- 									</span>  -->
<!-- 									<span class="input-block"> -->
<!-- 										<div class="ttb-input"> -->
											
<%-- 											<spring:message code="LB.Captcha" var="labelCapCode" /> --%>
<!-- 											<input id="capCode" type="text" class="ttb-input" -->
<%-- 												name="capCode" placeholder="${labelCapCode}" maxlength="6" autocomplete="off"> --%>
<!-- 											<img name="kaptchaImage" src="" /> -->
<%-- 											&nbsp;<font color="#FF0000"><spring:message code="LB.Captcha_refence" /> </font> --%>
<!-- 											<div class="login-input-block"> -->
<%-- 												<input type="button" name="reshow" class="ttb-sm-btn btn-flat-orange" onclick="refreshCapCode()" value="<spring:message code="LB.Regeneration"/>" /> --%>
<!-- 											</div> -->
<!-- 										</div> -->
<!-- 									</span> -->
<!-- 								</div> -->
								
							
								<c:if test="${input_data.data.FGTXWAY.equals('4')}">
									<!-- 自然人憑證PIN碼 -->
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4><spring:message code="LB.D1540" /></h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<input type="PASSWORD" class="text-input" maxLength="8" size="10" id="NPCPIN" name="NPCPIN" value=""/>
											</div>
										</span>
									</div>
								</c:if>
								
								<!-- 確認新使用者名稱 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.Captcha" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<spring:message code="LB.Captcha" var="labelCapCode" />
											<input id="capCode" type="text" class="text-input input-width-125"
												name="capCode" placeholder="${labelCapCode}" maxlength="6" autocomplete="off">
											<img name="kaptchaImage" src=""  class="verification-img"/>
											<button type="button" name="reshow" class="btn-flat-gray" onclick="refreshCapCode()"><spring:message code="LB.Regeneration"/></button>
											<!-- <span class="input-remarks">請注意：英文不分大小寫，限半形字元</span> -->
										</div>
									</span>
								</div>
                           	</div>
	                        <!--button 區域 -->
							<!-- 重新輸入 -->
							<input type="button" name="CMBACK" id="CMBACK" value="<spring:message code="LB.X0318"/>" class="ttb-button btn-flat-gray">
<%-- 	                            <input id="CMPRINT" name="CMPRINT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Print" />" /> --%>
                            <input id="CMSUBMIT" name="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="確定送出" />
                        <!--                     button 區域 -->
                        </div>  
                    </div>
                </form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>