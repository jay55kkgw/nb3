<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
</head>
<script type="text/javascript">
	$(document).ready(function(){
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	function init(){
		$("#CMBACK").click(function(e){
			$("#formId").attr("action","${__ctx}/login");
			$("#formId").submit();
		});
		$('#toggleAccordions').on('click', function(e) {
			var status = $('#toggleAccordions').val();
			if(status == '全部展開'){
				<c:forEach var="dataListLayer1" items="${ menuFilterList.data.sort_menu_only_name[0].SEED_LAYER }">
					<c:if test="${ dataListLayer1.ADOPALIVE.equals('1') && dataListLayer1.ISANONYMOUS.equals('0') && !dataListLayer1.DPACCSETID.equals('1100') }">
						<c:forEach var="dataListLayer2" items="${ dataListLayer1.SEED_LAYER }">
							<c:if test="${ dataListLayer2.ADOPALIVE.equals('1') && dataListLayer2.ISANONYMOUS.equals('0') }">
							$('#popup-${dataListLayer2.DPACCSETID}.collapse').removeAttr("data-parent").collapse('show');
							</c:if>
						</c:forEach>
					</c:if>
				</c:forEach>
		        $('#toggleAccordions').val("全部收折");
			}
			else if(status == '全部收折'){
				<c:forEach var="dataListLayer1" items="${ menuFilterList.data.sort_menu_only_name[0].SEED_LAYER }">
					<c:if test="${ dataListLayer1.ADOPALIVE.equals('1') && dataListLayer1.ISANONYMOUS.equals('0') && !dataListLayer1.DPACCSETID.equals('1100') }">
						<c:forEach var="dataListLayer2" items="${ dataListLayer1.SEED_LAYER }">
							<c:if test="${ dataListLayer2.ADOPALIVE.equals('1') && dataListLayer2.ISANONYMOUS.equals('0') }">
								$('#popup-${dataListLayer2.DPACCSETID}.collapse').attr("data-parent", "#popup-1").collapse('hide');
							</c:if>
						</c:forEach>
					</c:if>
				</c:forEach>
		        $('#toggleAccordions').val("全部展開");
			}
	    })
	}
</script>
<body>
   	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 網站導覽     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Service_overview" /></li>
		</ol>
	</nav>

	
	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">
		<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2>
					<spring:message code="LB.Service_overview"></spring:message>
				</h2>
				<form id="formId" method="post" action="">
					<div class="card-block shadow-box terms-pup-blcok">
	                    <div class="col-12 tab-content">
	                        <div class="ttb-input-block">
	                            <div class="card-detail-block">
	                                <input id="toggleAccordions" type="button" class="ttb-guide-btn sm-ttb-button btn-flat-gray" href="#carouselGuideIndicator" role="button" data-toggle="collapse" data-slide="prev" value="全部展開">
	                                <div class="card-center-block d-flex">
	                                <c:forEach var="dataListLayer1" items="${ menuFilterList.data.sort_menu_only_name[0].SEED_LAYER }">
	                                	<c:if test="${ dataListLayer1.ADOPALIVE.equals('1') && dataListLayer1.ISANONYMOUS.equals('0') && !dataListLayer1.DPACCSETID.equals('1100') && !dataListLayer1.ADOPID.equals('SIT') && !dataListLayer1.DPACCSETID.equals('463') }">
	                                	
	                                    	<div class="ttb-pup-block ttb-guide-block">	                                  		
	                                        	<h3>
													<c:if test="${__i18n_locale eq 'en' }">
														${dataListLayer1.ADOPENGNAME}
													</c:if>
													<c:if test="${__i18n_locale eq 'zh_TW' }">
														${dataListLayer1.ADOPNAME}
													</c:if>
													<c:if test="${__i18n_locale eq 'zh_CN' }">
														${dataListLayer1.ADOPCHSNAME}
													</c:if>
												</h3>
												<c:forEach var="dataListLayer2" items="${ dataListLayer1.SEED_LAYER }">
	                                            	<c:if test="${ dataListLayer2.ADOPALIVE.equals('1') && dataListLayer2.ISANONYMOUS.equals('0') }">
		                                        		<div role="tab">
		                                            		<h4><a role="button" class="collapsed" data-toggle="collapse" href="#popup-${dataListLayer2.DPACCSETID}" aria-expanded="true" aria-controls="popup-${dataListLayer2.DPACCSETID}">
		                                            			<c:if test="${__i18n_locale eq 'en' }">
																	${dataListLayer2.ADOPENGNAME}
																</c:if>
																<c:if test="${__i18n_locale eq 'zh_TW' }">
																	${dataListLayer2.ADOPNAME}
																</c:if>
																<c:if test="${__i18n_locale eq 'zh_CN' }">
																	${dataListLayer2.ADOPCHSNAME}
																</c:if>
		            		                                </a></h4>
			                    	                        <div id="popup-${dataListLayer2.DPACCSETID}" class="collapse" role="tabpanel">
			                                                <ul class="ttb-guide-list">
			                                                	<c:forEach var="dataListLayer3" items="${ dataListLayer2.SEED_LAYER }">
			                                                		<c:if test="${ dataListLayer3.ADOPALIVE.equals('1') && dataListLayer3.ISANONYMOUS.equals('0') }">
			                                                    		<li> 
			                                                    			<c:if test="${__i18n_locale eq 'en' }">
																				${dataListLayer3.ADOPENGNAME}
																			</c:if>
																			<c:if test="${__i18n_locale eq 'zh_TW' }">
																				${dataListLayer3.ADOPNAME}
																			</c:if>
																			<c:if test="${__i18n_locale eq 'zh_CN' }">
																				${dataListLayer3.ADOPCHSNAME}
																			</c:if>
																		</li>
																	</c:if>
																</c:forEach>		
			                                                </ul>
			                                            </div>
		                                        	</div>
		                                        </c:if>
	                                        </c:forEach>
	           	                       	</div>
	                               	
	                              	</c:if>
	                            </c:forEach>
	                            </div>
	                            </div>
	                        </div>
	                        <input type="BUTTON" class="ttb-button btn-flat-gray" value="<spring:message code="LB.X1962" />" name="CMBACK" id="CMBACK">
	                    </div>
	                </div>
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>
