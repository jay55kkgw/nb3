<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title><spring:message code= "LB.FX_Exchange_Transfer_1" /></title>

<Script language="JavaScript">
//列印不顯示按鈕
function print_the_page(){
	//先回到頂端再列印
	document.body.scrollTop = document.documentElement.scrollTop = 0;
    document.getElementById("CMPRINT").style.visibility = "hidden";    //顯示按鈕
    javascript:print();
    document.getElementById("CMPRINT").style.visibility = "visible";   //不顯示按鈕
}
</Script>


<style>
.watermark.zin:before {
z-index: -1;
}
</style>
</head>

<body class="bodymargin watermark zin" style="-webkit-print-color-adjust: exact">

	<br/>

	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>

	<br/>

	<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em"><spring:message code= "LB.FX_Exchange_Transfer_1" /></font></div>

	<br/>

	<table class="print">
		<tr>
			<td class="ColorCell" width="13%"><spring:message code= "LB.X0040" /></td>
			<td class="DataCell0" colspan="5">${fxcertpreview.data.curentDateTime}</td>
		</tr>
		<tr>
			<td class="ColorCell" width="13%"><spring:message code= "LB.X0041" /></td>
			<td class="DataCell1" width="24%" colspan="2">${fxcertpreview.data.UID}</td>
			<td class="ColorCell" width="19%"><spring:message code= "LB.Name" /></td>
			<td class="DataCell1" width="41%" colspan="2">${fxcertpreview.data.NAME}</td>
		</tr>
		<tr>
			<td class="ColorCell" width="13%"><spring:message code= "LB.W0289" /></td>
			<td class="DataCell1" width="24%" colspan="2">${fxcertpreview.data.currentDate}</td>
			<td class="ColorCell" width="19%"><spring:message code= "LB.D0050" /></td>
			<td class="DataCell1" width="41%" colspan="2">${fxcertpreview.data.ENAME}</td>
		</tr>
		<tr>
			<td class="ColorCell" width="13%"><spring:message code= "LB.X1461" /></td>
			<td class="DataCell1" colspan="5">${fxcertpreview.data.STAN}</td>
		</tr>
		<tr>
			<td class="ColorCell" width="13%"><spring:message code= "LB.Payers_account_no" /></td>
			<td class="DataCell0" width="15%">${fxcertpreview.data.CUSTACC}</td>
			<td class="ColorCell" width="9%"><spring:message code= "LB.Currency_o" /></td>
			<td class="DataCell0" width="19%">${fxcertpreview.data.ORGCCY}</td>
			<td class="ColorCell" width="15%"><spring:message code= "LB.Deducted" /></td>
			<td class="DataCell0" width="26%">${fxcertpreview.data.OUTAMT}</td>
		</tr>
		<tr>
			<td class="ColorCell" width="13%"><spring:message code= "LB.Payees_account_no" /></td>
			<td class="DataCell1" width="15%">${fxcertpreview.data.BENACC}</td>
			<td class="ColorCell" width="9%"><spring:message code= "LB.Currency_i" /></td>
			<td class="DataCell1" width="19%">${fxcertpreview.data.PMTCCY}</td>
			<td class="ColorCell" width="15%"><spring:message code= "LB.Buy" /></td>
			<td class="DataCell1" width="26%">${fxcertpreview.data.INAMT}</td>
		</tr>
		<tr>
			<td class="ColorCell" width="13%"><spring:message code= "LB.Exchange_rate" /></td>
			<td class="DataCell0" width="24%" colspan="2">${fxcertpreview.data.str_Rate}</td>
			<td class="ColorCell" width="19%"><spring:message code= "LB.Bargaining_number" /></td>
			<td class="DataCell0" width="41%" colspan="2">${fxcertpreview.data.BGROENO}</td>
		</tr>
		<tr>
			<td class="ColorCell" width="13%"><spring:message code= "LB.X1462" /></td>
			<td class="DataCell1" width="24%" colspan="2">${fxcertpreview.data.BENTYPE}</td>
			<td class="ColorCell" width="19%"><spring:message code= "LB.X0043" /></td>
			<td class="DataCell1" width="41%" colspan="2">${fxcertpreview.data.str_Country}</td>
		</tr>
		<tr>
			<td class="ColorCell" width="13%"><spring:message code= "LB.W0299" /></td>
			<td class="DataCell0" width="84%" colspan="5">${fxcertpreview.data.SRCFUNDDESC}</td>
		</tr>
		<tr>
			<td class="ColorCell" width="13%"><spring:message code= "LB.Service_Charges_Account" /></td>
			<td class="DataCell1" width="24%" colspan="2">${fxcertpreview.data.COMMACC}</td>
			<td class="ColorCell" width="19%"><spring:message code= "LB.Service_Chargess_Currency" /></td>
			<td class="DataCell1" width="41%" colspan="2">${fxcertpreview.data.COMMCCY}</td>
		</tr>
		<tr>
			<td class="ColorCell" width="13%"><spring:message code= "LB.X1463" /></td>
			<td class="DataCell0" width="24%" colspan="2">${fxcertpreview.data.FEEAMT}</td>
			<td class="ColorCell" width="19%"><spring:message code= "LB.X1464" /></td>
			<td class="DataCell0" width="41%" colspan="2">${fxcertpreview.data.CABAMT}</td>
		</tr>
	</table>

	<br/>

	<div style="text-align:center">
		<spring:message code= "LB.X1465" />
	</div>
	
	<div style="text-align:center">
		<input type="button" class="ttb-button btn-flat-orange" style="z-index:10000" id="CMPRINT" value="<spring:message code= "LB.Print_thepage" />" onclick="print_the_page()" />
	</div>
	
	<br/><br/><br/><br/>
	
	
</body>
</html>