<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript">
		// 初始化
		$(document).ready(function () {
			init();
		});

		function init() {
			//表單驗證
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
			// 確認鍵 click
			goOn();
			//上一頁按鈕
			back();
			//重新整理
			cmrest();
		}

		//上一頁按鈕
		function back() {
			$("#CMBACK").click(function () {
				var action = '${__ctx}/FUND/ALTER/stop_loss_notice';
				$('#back').val("Y");
				$("form").attr("action", action);
				initBlockUI();
				$("#formId").validationEngine('detach');
				$("form").submit();
			});
		}

		//重新整理
		function cmrest() {
			$("#CMRESET").click(function () {
				$("#formId")[0].reset();
			});
		}

		// 確認鍵 click
		function goOn() {
			$("#CMSUBMIT").click(function (e) {
				if (!$('#formId').validationEngine('validate')) {
					e = e || window.event; // for IE
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					initBlockUI(); //遮罩
					$("#formId").submit();
				}
			});
		}
		
		//"變更後停損設定請勿超過 100%"
		function validate_Amount(field, rules, i, options){
			var nstoploss = $('#NSTOPLOSS').val(); 
			console.log("nstoploss>>" + nstoploss);
			if(nstoploss>100){
				return  '<spring:message code= "LB.X1510" />';
			}
		}
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金    -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0901" /></li>
    <!-- 基金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1064" /></li>
    <!-- 停損停利通知設定     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0414" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!--停損停利通知設定 -->
				<h2>
					<spring:message code="LB.X0414" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form autocomplete="off" method="post" id="formId"
					action="${__ctx}/FUND/ALTER/stop_loss_notice_confirm">
					<c:set var="BaseResultData" value="${stop_loss_notice_step1.data}"></c:set>
					<!-- 表單顯示區 -->
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<!--變更種類 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W1167" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span><spring:message code="LB.W1166" /></span>
										</div>
									</span>
								</div>
								<!-- 身分證字號/統一編號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Id_no" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.hideid_CUSIDN}
											</span>
										</div>
									</span>
								</div>
								<!-- 信託帳號-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0944" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.hideid_CDNO}
											</span>
										</div>
									</span>
								</div>
								<!--基金名稱 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0025" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.FUNDLNAME}
											</span>
										</div>
									</span>
								</div>

								<!--類別 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.D0973" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${BaseResultData.str_AC202}</span>
										</div>
									</span>
								</div>
								<!--信託金額 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0026" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												<c:if test="${BaseResultData.FUNDAMT != '0.00'}">
													${BaseResultData.display_FUNDCUR }
													${BaseResultData.display_FUNDAMT }
												</c:if>
											</span>
										</div>
									</span>
								</div>
								<!--每次定額申購金額＋<br>不定額基準申購金額 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>${BaseResultData.show_PAYAMT}</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												<c:if test="${BaseResultData.PAYAMT != '0'}">
													${BaseResultData.display_PAYCUR }
													${BaseResultData.display_PAYAMT }
												</c:if>
											</span>
										</div>
									</span>
								</div>
								<!--原停利通知設定 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W1178" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span> ${BaseResultData.display_STOPPROF}</span>
										</div>
									</span>
								</div>
								<!--原停損通知設定 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W1179" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span> ${BaseResultData.display_STOPLOSS}</span>
										</div>
									</span>
								</div>
								<!--變更後停利通知設定 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W1180" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span class="validate[funcCallRequired[validate_CheckNumWithDigit2[<spring:message code='LB.W1180' />,NSTOPPROF,ture]]]">
													<!-- <input class="text-input card-input validate[required,funcCall[validate_CheckNumber['checknumber',NSTOPPROF]]]" type="text" -->
<!-- 													id="NSTOPPROF" NAME="NSTOPPROF" size="4" maxlength="3">&nbsp;% -->
													<!--不驗證 -->
												<input class="text-input card-input" type="text" id="NSTOPPROF" NAME="NSTOPPROF" size="4" maxlength="3">&nbsp;%
											</span>
										</div>
									</span>
								</div>
								<!--變更後停損通知設定 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.X0415" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span class="validate[funcCallRequired[validate_CheckNumWithDigit2[<spring:message code='LB.X0415' />,NSTOPLOSS,false]]]">
													<!-- <input class="text-input card-input validate[required,funcCall[validate_CheckNumber['checknumber',NSTOPLOSS]]] validate[funcCall[validate_Amount[AMOUNT]]]" type="text" -->
													<!--id="NSTOPLOSS"NAME="NSTOPLOSS" size="4" maxlength="3">&nbsp;% -->
													<!--不驗證 -->
												<input class="text-input card-input" type="text" id="NSTOPLOSS"NAME="NSTOPLOSS" size="4" maxlength="3">&nbsp;%
											</span>
										</div>
									</span>
								</div>
							</div>
							<!-- button -->
							<input class="ttb-button btn-flat-gray" name="CMBACK" id="CMBACK" type="button"
								value="<spring:message code="LB.Back_to_function_home_page" />" />
							
							<input class="ttb-button btn-flat-orange" name="CMSUBMIT" id="CMSUBMIT" type="button"
								value="<spring:message code="LB.Confirm" />" />
								<!-- button -->
						</div>
					</div>
					<div class="text-left">
						<!-- 						說明： -->
						 <ol class="description-list list-decimal">
							<p>
								<spring:message code="LB.Description_of_page" />
							</p>
							<li>
								<span>
									<strong style="FONT-WEIGHT: 400"><spring:message code="LB.Stop_Loss_Notice_P2_D1" /></strong>
								</span>
							</li>
							<li>
								<span>
									<strong style="FONT-WEIGHT: 400"><spring:message code="LB.Stop_Loss_Notice_P2_D2" /></strong>
								</span>
							</li>
						</ol>
					</div>
				</form>
			</section>
			<!-- main-content END -->
		</main>
	</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>