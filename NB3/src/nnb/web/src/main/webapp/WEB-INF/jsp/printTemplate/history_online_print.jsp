<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<title>${jspTitle}</title>
<script type="text/javascript">
	$(document).ready(function(){
		getBHWSHtml();
		window.print();
	});
	
	function getBHWSHtml() {
		uri = '${__ctx}' + "/CREDIT/INQUIRY/card_history_online_aj"
		rdata = {
			CARDTYPE: '${CARDTYPE}',
			BILL_NO:'${BILL_NO}',
			FGPERIOD:'${FGPERIOD}',
			QUERYTYPE:'${QUERYTYPE}'
		};
		fstop.getServerDataEx(uri, rdata, false,add_Data);
		
	}
	function add_Data(data){
		$('#HTMLCONTENT').html(data.data.INWARDS);
	}
</script>
</head>
<body class="bodymargin" style="-webkit-print-color-adjust: exact ; background-color: #f2f2f2 !important;">
	<br/>
	<br/>
	<div style="text-align:center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/>
	</div>
	<br/>
	<br/>
	<br/>
	<!-- Title  -->
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<br/>
	<br/>
	<br/>
	<div id="HTMLCONTENT">
	</div>
	<br />
	<br />
</body>
</html>