<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="__imports.jsp" %>

    		<section class="main">
    		    <c:if test = "${form != null}">
         			<form class="form-horizontal" onsubmit="" action="${form.action}" method="${form.method}" enctype="${form.encType}">
         				<c:forEach var="field" items="${form.fields}">
         				
         					<c:if test = "${field.type == 'hidden'}">
         						<input type="${field.type}" id="${field.id}" name="${field.name}" value="${field.value}">         							
         					</c:if>

         					<c:if test = "${field.type == 'text' || field.type == 'number'}">
         					    <div class="form-group ${status.error ? 'has-error' : ''}">
         					      <label class="col-sm-2 control-label" for="${field.id}">								
								  		${field.label}
								  </label>
								  <div class="col-sm-${field.width}">
         						  	<input type="${field.type}" id="${field.id}" name="${field.name}"
         						  	    value="${field.value}" 
         						  		maxlength="${field.maxLength}" 
         						  		${field.required ? 'required' : ''} ${field.disabled ? 'disabled' : ''} 
         						  		placeholder="${field.placeHolder}" 
         						  		title="${field.title}" 
         						  		accesskey="${field.accessKey}"
         						  		tabindex="${field.tabIndex}"
         						  		class="form-control" >         							
         						  </div>
         						</div>         				
         					</c:if>

         					<c:if test = "${field.type == 'password'}">
         					    <div class="form-group ${status.error ? 'has-error' : ''}">
         					      <label class="col-sm-2 control-label" for="${field.id}">								
								  		${field.label}
								  </label>
								  <div class="col-sm-${field.width}">
         						  	<input type="${field.type}" id="${field.id}" name="${field.name}"
         						  	    value="${field.value}" 
         						  		maxlength="${field.maxLength}" 
         						  		${field.required ? 'required' : ''} ${field.disabled ? 'disabled' : ''} 
         						  		placeholder="${field.placeHolder}" 
         						  		title="${field.title}" 
         						  		accesskey="${field.accessKey}"
         						  		tabindex="${field.tabIndex}"
         						  		autocomplete="off"
         						  		class="form-control" >         							
         						  </div>
         						</div>         				
         					</c:if>

         					<c:if test = "${field.type == 'checkbox' && field.fields == null}">
								<div class="form-group ${status.error ? 'has-error' : ''}">
									<label class="col-sm-2 control-label" for="${field.id}">${field.label}</label>
									<div class="col-sm-10">
										<div class="checkbox">
											<label> 
												<input type="${field.type}" id="${field.id}" name="${field.name}"
													value="${field.value}" 
													${field.selected ? 'checked' : ''} ${field.disabled ? 'disabled' : ''}
													title="${field.title}" 
													accesskey="${field.accessKey}" 
													tabindex="${field.tabIndex}"
													>
											</label>
										</div>
									</div>
								</div>
         					</c:if>         				

         					<c:if test = "${field.type == 'checkbox' && field.fields != null}">         					
								<div class="form-group ${status.error ? 'has-error' : ''}">
									<label class="col-sm-2 control-label">${field.label}</label>
									<div class="col-sm-10">
										<c:forEach var="subField" items="${field.fields}">					
											<div class="checkbox-inline">
												<label> 
													<input type="${subField.type}" id="${subField.id}" name="${subField.name}" 
														value="${subField.value}" 
														${subField.selected ? 'checked' : ''} ${field.disabled ? 'disabled' : ''}
														title="${subField.title}" 
														accesskey="${subField.accessKey}"
														tabindex="${subField.tabIndex}"
														>
														${subField.label}
												</label>
											</div>
											<br />
										</c:forEach>
									</div>
								</div>
         					</c:if>         				
         				
         					<c:if test = "${field.type == 'radio' && field.fields != null}">         					
								<div class="form-group ${status.error ? 'has-error' : ''}">
									<label class="col-sm-2 control-label">${field.label}</label>
									<div class="col-sm-10">
										<c:forEach var="subField" items="${field.fields}">					
											<div class="radio-inline">
												<label> 
													<input type="${subField.type}" id="${subField.id}" name="${subField.name}"
														value="${subField.value}" 
														${subField.selected ? 'checked' : ''} ${field.disabled ? 'disabled' : ''}
														title="${subField.title}" 
														accesskey="${field.accessKey}"
														tabindex="${field.tabIndex}"
														>
														${subField.label}
												</label>
											</div>
											<br />
										</c:forEach>
									</div>
								</div>
         					</c:if>         				

         					<c:if test = "${field.type == 'select'}">         					
								<div class="form-group ${status.error ? 'has-error' : ''}">
									<label class="col-sm-2 control-label">${field.label}</label>
									<div class="col-sm-${field.width}">									
										<select name="${field.name}" id="${field.id}" 
												${field.disabled ? 'disabled' : ''} 
												title="${field.title}" 
												accesskey="${field.accessKey}"
												tabindex="${field.tabIndex}"
												class="form-control select2" >
											<c:forEach var="subField" items="${field.fields}">
												<option value="${subField.value}" ${subField.selected ? 'selected' : ''}>${subField.label}</option>					
											</c:forEach>
										</select>										
									</div>
								</div>
         					</c:if>         				
         				         											
         					<c:if test = "${field.type == 'textarea'}">
         					    <div class="form-group ${status.error ? 'has-error' : ''}">
         					      <label class="col-sm-2 control-label" for="${field.id}">								
								  		${field.label}
								  </label>
								  <div class="col-sm-${field.width}">
         						  	<textarea id="${field.id}" name="${field.name}"  
         						  		maxlength="${field.maxLength}" 
         						  	    cols="${field.cols}" rows="${field.rows}"
         						  		dir="${field.dir}" wrap="${field.wrap}"
										${field.readonly ? 'readonly' : ''}         						  	    
         						  		${field.required ? 'required' : ''} ${field.disabled ? 'disabled' : ''} 
         						  		title="${field.title}" 
         						  		accesskey="${field.accessKey}"
         						  		tabindex="${field.tabIndex}"
         						  		class="form-control" 
         						  		>${field.value}</textarea>       							
         						  </div>
         						</div>         				
         					</c:if>
         				         				
         					<c:if test = "${field.type == 'file'}">
         					    <div class="form-group ${status.error ? 'has-error' : ''}">
         					      <label class="col-sm-2 control-label" for="${field.id}">								
								  		${field.label}
								  </label>
								  <div class="col-sm-${field.width}">
         						  	<input type="${field.type}" id="${field.id}" name="${field.name}"  
         						  		${field.multiple ? 'multiple' : ''} 
         						  		${field.disabled ? 'disabled' : ''}
         						  		title="${field.title}" 
         						  		accesskey="${field.accessKey}"
         						  		tabindex="${field.tabIndex}"
         						  		class="form-control" >         							
         						  </div>
         						</div>         				
         					</c:if>
         				
         				         				
         					<c:if test = "${field.type == 'range'}">
         					    <div class="form-group ${status.error ? 'has-error' : ''}">
         					      <label class="col-sm-2 control-label" for="${field.id}">								
								  		${field.label}
								  </label>
								  <div class="col-sm-${field.width}">
         						  	<input type="${field.type}" id="${field.id}" name="${field.name}" 
         						  		value="${field.value}"
         						  	    min="${field.min}" max="${field.max}" 
         						  		maxlength="${field.maxLength}" 
         						  		${field.required ? 'required' : ''} ${field.disabled ? 'disabled' : ''}
         						  		placeholder="${field.placeHolder}" 
         						  		title="${field.title}" 
         						  		accesskey="${field.accessKey}"
         						  		tabindex="${field.tabIndex}"
         						  		class="form-control" >         							
         						  </div>
         						</div>         				
         					</c:if>
         				
         					<c:if test = "${field.type == 'reset'}">
         					    <div class="form-group ${status.error ? 'has-error' : ''}">
         					      <label class="col-sm-2 control-label" for="${field.id}">								
								  		${field.label}
								  </label>
								  <div class="col-sm-${field.width}">
         						  	<input type="${field.type}" id="${field.id}" name="${field.name}" 
         						  		value="${field.value}"
         						  		${field.disabled ? 'disabled' : ''}
         						  		title="${field.title}" 
         						  		accesskey="${field.accessKey}"
         						  		tabindex="${field.tabIndex}"
         						  		class="form-control" >         							
         						  </div>
         						</div>         				
         					</c:if>
         				
         					<c:if test = "${field.type == 'email'}">
         					    <div class="form-group ${status.error ? 'has-error' : ''}">
         					      <label class="col-sm-2 control-label" for="${field.id}">								
								  		${field.label}
								  </label>
								  <div class="col-sm-${field.width}">
         						  	<input type="${field.type}" id="${field.id}" name="${field.name}"  
         						  		value="${field.value}"
         						  		maxlength="${field.maxLength}" 
         						  		${field.required ? 'required' : ''} ${field.disabled ? 'disabled' : ''}
         						  		placeholder="${field.placeHolder}" 
         						  		title="${field.title}" 
         						  		accesskey="${field.accessKey}"
         						  		tabindex="${field.tabIndex}"
         						  		class="form-control" >         							
         						  </div>
         						</div>         				
         					</c:if>

         					<c:if test = "${field.type == 'date' || field.type == 'datetime-local' || field.type == 'month' 
         									|| field.type == 'week' || field.type == 'time'}">
         					    <div class="form-group ${status.error ? 'has-error' : ''}">
         					      <label class="col-sm-2 control-label" for="${field.id}">								
								  		${field.label}
								  </label>
								  <div class="col-sm-${field.width}">
         						  	<input type="${field.type}" id="${field.id}" name="${field.name}"  
         						  		value="${field.value}"
         						  		maxlength="${field.maxLength}" 
         						  		${field.required ? 'required' : ''} ${field.disabled ? 'disabled' : ''}
         						  		placeholder="${field.placeHolder}" 
         						  		title="${field.title}" 
         						  		accesskey="${field.accessKey}"
         						  		tabindex="${field.tabIndex}"
         						  		class="form-control" >         							
         						  </div>
         						</div>         				
         					</c:if>

         					<c:if test = "${field.type == 'image'}">
         					    <div class="form-group ${status.error ? 'has-error' : ''}">
         					      <label class="col-sm-2 control-label" for="${field.id}">								
								  		${field.label}
								  </label>
								  <div class="col-sm-${field.width}">
         						  	<input type="${field.type}" id="${field.id}" name="${field.name}"
         						  		src="${field.src}"
         						  		width="${field.imgWidth}" height="${field.imgHeight}"  
         						  		alt="${field.alt}"
         						  		${field.disabled ? 'disabled' : ''}
         						  		title="${field.title}" 
         						  		accesskey="${field.accessKey}"
         						  		tabindex="${field.tabIndex}"
         						  		class="form-control" >         							
         						  </div>
         						</div>         				
         					</c:if>
         				
         					<c:if test = "${field.type == 'submit'}">
								<div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
										<button type="submit"  
												${field.disabled ? 'disabled' : ''}
												title="${field.title}" 
												accesskey="${field.accessKey}"
												tabindex="${field.tabIndex}"
												class="btn-lg btn-primary pull-left" >
											<span>${field.label}</span>
										</button>
									</div>
								</div>         					
         					</c:if> 
         					        				
         				</c:forEach>
         				
					</form>
      			</c:if>
			</section>
