<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
<br/><br/>
<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
<br/><br/><br/>
<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
<br/><br/><br/>
<label><spring:message code="LB.System_time" /> ：</label><label>${CMQTIME}</label>
<br/><br/>
<label><spring:message code="LB.W1060" /> ：</label><label><spring:message code="LB.W1063" /></label>
<br/><br/>
<label><spring:message code="LB.X0380" /> ：</label><label>${REC_NO}</label><label><spring:message code="LB.Rows"/></label>
<br/><br/>

<table class="print">
	<tr>
		<th><spring:message code="LB.Name"/></th>
		<th class="text-left" colspan="8">${NAME}</th>
	</tr>
	<tr>
		<td style="text-align:center"><spring:message code="LB.X0377" /></td>
		<td style="text-align:center"><spring:message code="LB.W0944"/></td>
		<td style="text-align:center"><spring:message code="LB.W0025"/></td>
		<td style="text-align:center"><spring:message code="LB.W0978"/></td>
		<td style="text-align:center"><spring:message code="LB.W0979"/></td>
		<td style="text-align:center"><spring:message code="LB.W0135" /></td>
		<td style="text-align:center"><spring:message code="LB.W1140" /></td>
	</tr>
	<c:forEach items="${dataListMap}" var="map">
	<tr>
		<td style="text-align:center">${map.TRADEDATE_1}</td>
		<td style="text-align:center">${map.CDNO_1}</td>
		<td style="text-align:center">(${map.TRANSCODE})&nbsp;${map.FUNDLNAME}</td>
		<td style="text-align:center">
			${map.ADCCYNAME}<br>
			${map.FUNDAMT_1}
		</td>
		<td style="text-align:right">${map.UNIT_1}</td>
		<td style="text-align:center">${map.FUNDACN_1}</td>
		<td style="text-align:center">
			<c:if test="${map.BILLSENDMODE == '1'}">
				<spring:message code="LB.All"/>
			</c:if>
			<c:if test="${map.BILLSENDMODE == '2'}">
				<spring:message code="LB.X1852"/>
			</c:if>
		</td>
	</tr>
	</c:forEach>
</table>
<br/><br/>
	<div class="text-left">
		<spring:message code="LB.Description_of_page" />
		<ol class="list-decimal text-left">
			<li><spring:message code= "LB.fund_reservation_query_P2_D1" /></li>
          	<li><spring:message code= "LB.fund_reservation_query_P2_D2" /></li>
		</ol>
	</div>
</body>
</html>