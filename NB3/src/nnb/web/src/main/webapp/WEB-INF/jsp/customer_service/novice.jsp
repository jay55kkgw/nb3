<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
</head>
<script type="text/javascript">
	$(document).ready(function(){
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	function init(){
		$("#CMBACK").click(function(e){
			$("#formId").attr("action","${__ctx}/login");
			$("#formId").submit();
		});
	}
	
	function pdfdownload(data){
		window.open("${__ctx}/CUSTOMER/SERVICE/pdf_download?pdf="+data,"_black");
	}
</script>
<body>
   	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	<!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><a href="#">新手上路</a></li>
		</ol>
	</nav>
	
	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">
		<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2>
					新手上路
				</h2>
				<form id="formId" method="post" action="">
					<div class="card-block shadow-box terms-pup-blcok">
	                    <nav class="nav card-select-block text-center d-block" style="padding-top: 30px;" id="nav-tab" role="tablist">
	                        <input type="button" class="nav-item ttb-sm-btn active" id="nav-trans-1-tab" data-toggle="tab" href="#nav-trans-1" role="tab" aria-selected="false" value="首次登入" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-2-tab" data-toggle="tab" href="#nav-trans-2" role="tab" aria-selected="true" value="轉帳交易" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-3-tab" data-toggle="tab" href="#nav-trans-3" role="tab" aria-selected="true" value="帳戶餘額查詢" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-4-tab" data-toggle="tab" href="#nav-trans-4" role="tab" aria-selected="true" value="重設使用者名稱/簽入密碼" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-5-tab" data-toggle="tab" href="#nav-trans-5" role="tab" aria-selected="true" value="轉入綜存定存" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-6-tab" data-toggle="tab" href="#nav-trans-6" role="tab" aria-selected="true" value="買賣外幣/約定轉帳" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-7-tab" data-toggle="tab" href="#nav-trans-7" role="tab" aria-selected="true" value="繳稅" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-8-tab" data-toggle="tab" href="#nav-trans-8" role="tab" aria-selected="true" value="定期投資約定變更" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-9-tab" data-toggle="tab" href="#nav-trans-9" role="tab" aria-selected="true" value="定期投資申購" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-10-tab" data-toggle="tab" href="#nav-trans-10" role="tab" aria-selected="true" value="信用卡持卡總覽" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-11-tab" data-toggle="tab" href="#nav-trans-11" role="tab" aria-selected="true" value="繳本行信用卡費" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-12-tab" data-toggle="tab" href="#nav-trans-12" role="tab" aria-selected="true" value="我的Email設定" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-13-tab" data-toggle="tab" href="#nav-trans-13" role="tab" aria-selected="true" value="常用帳號設定" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-14-tab" data-toggle="tab" href="#nav-trans-14" role="tab" aria-selected="true" value="申請電子帳單" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-15-tab" data-toggle="tab" href="#nav-trans-15" role="tab" aria-selected="true" value="國內臺幣匯入匯款通知設定" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-16-tab" data-toggle="tab" href="#nav-trans-16" role="tab" aria-selected="true" value="操作手冊下載" />
	                    </nav>
	                    <div class="col-12 tab-content" id="nav-tabContent">
	                    	<!-- 首次登入 -->
	                    	<div class="ttb-input-block tab-pane fade show active" id="nav-trans-1" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">首次登入</h3>
	                                    <p>功能介紹</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>提供您首次登入後，變更簽入密碼及交易密碼。</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/firsttime.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/firsttime-2.png" alt="Second slide">
	                                        </div>
<!-- 	                                        <div class="carousel-item"> -->
<%-- 	                                            <img class="d-block" src="${__ctx}/img/ntd/P4.png" alt="Third slide"> --%>
<!-- 	                                        </div> -->
<!-- 	                                        <div class="carousel-item"> -->
<%-- 	                                            <img class="d-block" src="${__ctx}/img/ntd/P4.png" alt="Four slide"> --%>
<!-- 	                                        </div> -->
<!-- 	                                        <div class="carousel-item"> -->
<%-- 	                                            <img class="d-block" src="${__ctx}/img/ntd/P4.png" alt="Five slide"> --%>
<!-- 	                                        </div> -->
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicatorWord" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟一</h5>
	                                                <p>1.請輸入舊的簽入密碼及新簽入密碼。</p>
	                                                <p>2.請輸入舊的交易密碼及新交易密碼。</p>
	                                                <p>3.輸入完後，按『確定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟二</h5>
	                                                <p>顯示變更後之結果。</p>
	                                                <p>按『確定』後，回到登入頁，再以變更後簽入密碼登入。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
<!-- 	                                        <div class="carousel-item"> -->
<!-- 	                                            <div class="carousel-caption"> -->
<!-- 	                                                <p>操作步驟</p> -->
<!-- 	                                                <h5>步驟三</h5> -->
<!-- 	                                                <p>晶片金融卡插拔後，按『確定』。</p> -->
<!-- 	                                                <span class="input-block"> -->
<!-- 	                                                    <div class="ttb-input"> -->
<!-- 	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator" role="button" data-slide="prev" value="上一步" /> -->
<!-- 	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator" role="button" data-slide="next" value="下一步" /> -->
<!-- 	                                                    </div> -->
<!-- 	                                                </span> -->
<!-- 	                                            </div> -->
<!-- 	                                        </div> -->
<!-- 	                                        <div class="carousel-item"> -->
<!-- 	                                            <div class="carousel-caption"> -->
<!-- 	                                                <p>操作步驟</p> -->
<!-- 	                                                <h5>步驟四</h5> -->
<!-- 	                                                <p>輸入密碼後，按『送出』。</p> -->
<!-- 	                                                <span class="input-block"> -->
<!-- 	                                                    <div class="ttb-input"> -->
<!-- 	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator" role="button" data-slide="prev" value="上一步" /> -->
<!-- 	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator" role="button" data-slide="next" value="下一步" /> -->
<!-- 	                                                    </div> -->
<!-- 	                                                </span> -->
<!-- 	                                            </div> -->
<!-- 	                                        </div> -->
<!-- 	                                        <div class="carousel-item"> -->
<!-- 	                                            <div class="carousel-caption"> -->
<!-- 	                                                <p>操作步驟</p> -->
<!-- 	                                                <h5>步驟五</h5> -->
<!-- 	                                                <p>顯示交易結果。</p> -->
<!-- 	                                                <span class="input-block"> -->
<!-- 	                                                    <div class="ttb-input"> -->
<!-- 	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator" role="button" data-slide="prev" value="上一步" /> -->
<!-- 	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator" role="button" data-slide="next" value="下一步" /> -->
<!-- 	                                                    </div> -->
<!-- 	                                                </span> -->
<!-- 	                                            </div> -->
<!-- 	                                        </div> -->
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 轉帳交易 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-2" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">轉帳交易</h3>
	                                    <p>功能介紹</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>提供您即時或預約轉帳交易。</p>
	                                                </li>
	                                                <li>
	                                                    <p>已約定轉入帳號，可以『交易密碼(SSL)』、『電子簽章(載具i-Key)』、『晶片金融卡』、『裝置推播認證』進行轉帳；未約定轉入帳號，請以『電子簽章(載具i-Key)』、『晶片金融卡』、『裝置推播認證』進行轉帳。</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator2" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/N070-1.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/N070-2.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/N070-5.png" alt="Third slide">
	                                        </div>
<!-- 	                                        <div class="carousel-item"> -->
<%-- 	                                            <img class="d-block" src="${__ctx}/img/ntd/p4-b.png" alt="Four slide"> --%>
<!-- 	                                        </div> -->
<!-- 	                                        <div class="carousel-item"> -->
<%-- 	                                            <img class="d-block" src="${__ctx}/img/ntd/p5-b.png" alt="Five slide"> --%>
<!-- 	                                        </div> -->
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator2word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟一</h5>
	                                                <p>輸入轉出帳號、轉入帳號、轉帳金額等必要欄位後，按『確定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator2" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator2" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟二</h5>
	                                                <p>輸入以黃色標記之轉入帳號，選擇交易機制。若為晶片金融卡(需輸入驗證碼)，按『確定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator2" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator2" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
<!-- 	                                         <div class="carousel-item"> -->
<!-- 	                                            <div class="carousel-caption"> -->
<!-- 	                                                <p>操作步驟</p> -->
<!-- 	                                                <h5>步驟三</h5> -->
<!-- 	                                                <p>輸入密碼後，按『送出』。</p> -->
<!-- 	                                                <span class="input-block"> -->
<!-- 	                                                    <div class="ttb-input"> -->
<!-- 	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator" role="button" data-slide="prev" value="上一步" /> -->
<!-- 	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator" role="button" data-slide="next" value="下一步" /> -->
<!-- 	                                                    </div> -->
<!-- 	                                                </span> -->
<!-- 	                                            </div> -->
<!-- 	                                        </div> -->
<!-- 	                                        <div class="carousel-item"> -->
<!-- 	                                            <div class="carousel-caption"> -->
<!-- 	                                                <p>操作步驟</p> -->
<!-- 	                                                <h5>步驟四</h5> -->
<!-- 	                                                <p>晶片金融卡插拔。</p> -->
<!-- 	                                                <span class="input-block"> -->
<!-- 	                                                    <div class="ttb-input"> -->
<!-- 	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator" role="button" data-slide="prev" value="上一步" /> -->
<!-- 	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator" role="button" data-slide="next" value="下一步" /> -->
<!-- 	                                                    </div> -->
<!-- 	                                                </span> -->
<!-- 	                                            </div> -->
<!-- 	                                        </div> -->
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟三</h5>
	                                                <p>顯示交易結果。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator2" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator2" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 存款帳戶查詢 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-3" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">帳戶餘額查詢</h3>
	                                    <p>功能介紹</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>提供您查詢所有臺幣活期帳戶之餘額。</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator3" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/balance_query_1.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/balance_query_2.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/balance_query_3.png" alt="Second slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator3word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟一</h5>
	                                                <p>點選『臺幣服務』項目下『帳戶餘額查詢』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator3" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator3" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟二</h5>
	                                                <p>顯示所有臺幣活期帳戶餘額。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator3" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator3" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟三</h5>
	                                                <p>點選快速選單可直接導引至其他臺幣服務交易。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator3" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator3" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 重設使用者名稱/簽入密碼 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-4" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">重設使用者名稱/簽入密碼</h3>
	                                    <p>功能介紹</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>提供您忘記使用者名稱或簽入密碼時使用。</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator4" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/username_alert_1.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/username_alert_2.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/username_alert_3.png" alt="Third slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/username_alert_4.png" alt="Four slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator4word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟一</h5>
	                                                <p>點選『忘記使用者名稱/密碼』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator4" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator4" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟二</h5>
	                                                <p>點選『重設使用者名稱/簽入密碼』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator4" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator4" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟三</h5>
	                                                <p>1.請輸入身分證字號、使用者名稱及簽入密碼。</p>
	                                                <p>2.按『取得簡訊驗證碼』，待手機收到簡訊後，輸入簡訊驗證碼。</p>
	                                                <p>3.請輸入圖形驗證碼並確認插入本行晶片金融卡後，點選『確定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator4" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator4" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟四</h5>
	                                                <p>顯示重設之結果。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator4" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator4" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 轉入綜存定存 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-5" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">轉入綜存定存</h3>
	                                    <p>功能介紹</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>提供您即時或預約轉入綜存定存。</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator5" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/deposit_transfer_1.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/deposit_transfer_2.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/deposit_transfer_3.png" alt="Third slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/deposit_transfer_4.png" alt="Four slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator5word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟一</h5>
	                                                <p>點選『轉入綜存定存』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator5" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator5" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟二</h5>
	                                                <p>輸入轉出帳號、轉入帳號、轉帳金額等必要欄位後，按『確定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator5" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator5" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟三</h5>
	                                                <p>輸入以黃色標記之轉入帳號，選擇交易機制。若為晶片金融卡(需輸入驗證碼)，按『確定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator5" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator5" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟四</h5>
	                                                <p>顯示交易結果。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator5" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator5" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 買賣外幣/約定轉帳 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-6" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">買賣外幣/約定轉帳</h3>
	                                    <p>功能介紹</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>提供您即時或預約換匯及轉帳交易。</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator6" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/f_transfer_1.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/f_transfer_2.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/f_transfer_3.png" alt="Third slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/f_transfer_4.png" alt="Four slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/f_transfer_5.png" alt="Five slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/f_transfer_6.png" alt="Six slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/f_transfer_7.png" alt="Seven slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator6word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟一</h5>
	                                                <p>點選『買賣外幣/約定轉帳』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator6" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator6" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟二</h5>
	                                                <p>詳閱注意事項後，按『確定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator6" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator6" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟三</h5>
	                                                <p>輸入轉出帳號、轉出幣別、轉入帳號、轉入幣別、轉帳金額等必要欄位後，按『確定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator6" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator6" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟四</h5>
	                                                <p>確認轉帳資料後，按『取得匯率/議價』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator6" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator6" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟五</h5>
	                                                <p>確認匯率資料後，按『確定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator6" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator6" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟六</h5>
	                                                <p>輸入以黃色標記之轉入帳號，選擇交易機制，按『確定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator6" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator6" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟七</h5>
	                                                <p>顯示交易結果。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator6" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator6" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 繳稅 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-7" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">繳稅</h3>
	                                    <p>功能介紹</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>提供您即時或預約繳稅。</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator7" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/pay_taxes_1.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/pay_taxes_2.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/pay_taxes_3.png" alt="Third slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/pay_taxes_4.png" alt="Four slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator7word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟一</h5>
	                                                <p>點選『繳稅』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator7" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator7" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟二</h5>
	                                                <p>輸入轉出帳號、稅別、繳稅金額等必要欄位後，按『確定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator7" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator7" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟三</h5>
	                                                <p>選擇交易機制。若為晶片金融卡(需輸入驗證碼)，按『確定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator7" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator7" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟四</h5>
	                                                <p>顯示交易結果。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator7" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator7" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 定期投資約定變更 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-8" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">定期投資約定變更</h3>
	                                    <p>功能介紹</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>提供您基金定期投資約定變更。</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator8" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/regular_investment_1.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/regular_investment_2.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/regular_investment_3.png" alt="Third slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/regular_investment_4.png" alt="Four slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/regular_investment_5.png" alt="Five slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator8word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟一</h5>
	                                                <p>點選『定期投資約定變更』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator8" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator8" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟二</h5>
	                                                <p>選擇欲變更資料，按『確定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator8" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator8" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟三</h5>
	                                                <p>選擇變更項目，並輸入必要欄位，按『確定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator8" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator8" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟四</h5>
	                                                <p>確認資料，輸入交易密碼後，按『確定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator8" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator8" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟五</h5>
	                                                <p>顯示變更結果。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator8" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator8" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 定期投資申購 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-9" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">定期投資申購</h3>
	                                    <p>功能介紹</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>提供您基金定期定額申購。</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator9" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/fund_regular_1.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/fund_regular_2.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/fund_regular_3.png" alt="Third slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/fund_regular_4.png" alt="Four slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/fund_regular_5.png" alt="Five slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/fund_regular_6.png" alt="Six slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/fund_regular_7.png" alt="Seven slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator9word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟一</h5>
	                                                <p>點選『定期投資申購』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator9" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator9" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟二</h5>
	                                                <p>輸入基金公司名稱、基金名稱、申購金額等必要欄位後，按『確定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator9" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator9" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟三</h5>
	                                                <p>閱讀基金風險，勾選已閱讀基金風險後，按『確定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator9" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator9" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟四</h5>
	                                                <p>選擇公開說明書/投資人須知取得方式後，按『確定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator9" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator9" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟五</h5>
	                                                <p>閱讀報酬說明，勾選已閱讀後，按『確定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator9" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator9" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟六</h5>
	                                                <p>確認資料並輸入交易密碼，按『確定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator9" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator9" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟七</h5>
	                                                <p>顯示交易結果。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator9" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator9" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 信用卡持卡總覽 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-10" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">信用卡持卡總覽</h3>
	                                    <p>功能介紹</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>提供您查看已持有之信用卡。</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator10" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/card_overview_1.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/card_overview_2.png" alt="Second slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator10word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟一</h5>
	                                                <p>點選『信用卡持卡總覽』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator10" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator10" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟二</h5>
	                                                <p>顯示查詢之結果。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator10" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator10" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 繳本行信用卡費 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-11" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">繳本行信用卡費</h3>
	                                    <p>功能介紹</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>提供您繳交本行信用卡之帳單。</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator11" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/card_payment_1.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/card_payment_2.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/card_payment_3.png" alt="Third slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/card_payment_4.png" alt="Four slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator11word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟一</h5>
	                                                <p>點選『繳本行信用卡費』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator11" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator11" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟二</h5>
	                                                <p>1.選擇或輸入『轉出帳號』、『轉入信用卡帳號』、『繳款金額』等必要欄位。</p>
	                                                <p>2.輸入完後，點選『確定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator11" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator11" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟三</h5>
	                                                <p>輸入以黃色標記之轉入帳號，選擇交易機制。若為晶片金融卡(需輸入驗證碼)，按『確定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator11" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator11" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟四</h5>
	                                                <p>顯示繳費之結果。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator11" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator11" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 我的Email設定 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-12" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">我的Email設定</h3>
	                                    <p>功能介紹</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>提供您設定您個人的Email。</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator12" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/email_setting_1.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/email_setting_2.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/email_setting_3.png" alt="Third slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/email_setting_4.png" alt="Four slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator12word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟一</h5>
	                                                <p>點選『Email設定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator12" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator12" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟二</h5>
	                                                <p>選擇『我的Email』，點選『確定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator12" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator12" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟三</h5>
	                                                <p>輸入您的電子郵箱，選擇交易機制，按『確定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator12" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator12" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟四</h5>
	                                                <p>顯示設定之結果。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator12" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator12" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 常用帳號設定 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-13" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">常用帳號設定</h3>
	                                    <p>功能介紹</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>提供您設定您常用的帳號。</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator13" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/account_setting_1.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/account_setting_2.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/account_setting_3.png" alt="Third slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator13word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟一</h5>
	                                                <p>點選『常用帳號設定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator13" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator13" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟二</h5>
	                                                <p>輸入『轉入帳號』及『好記名稱』，點選『確定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator13" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator13" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟三</h5>
	                                                <p>顯示設定之結果。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator13" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator13" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 申請電子帳單 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-14" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">申請電子帳單</h3>
	                                    <p>功能介紹</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>提供您申請電子帳單。</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator14" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/electronic_reconciliation_1.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/electronic_reconciliation_2.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/electronic_reconciliation_3.png" alt="Third slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/electronic_reconciliation_4.png" alt="Four slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/electronic_reconciliation_5.png" alt="Five slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator14word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟一</h5>
	                                                <p>點選『電子對帳單申請』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator14" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator14" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟二</h5>
	                                                <p>選擇欲申請項目，點選『確定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator14" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator14" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟三</h5>
	                                                <p>詳閱約定條款，若同意請點選『同意並繼續』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator14" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator14" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟四</h5>
	                                                <p>輸入交易密碼，點選『確定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator14" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator14" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟五</h5>
	                                                <p>顯示申請之結果。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator14" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator14" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 國內臺幣匯入匯款通知設定 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-15" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">國內臺幣匯入匯款通知設定</h3>
	                                    <p>功能介紹</p>
	                                    <ul class="ttb-pup-list">
	                                        <li>
	                                            <ol style="list-style-type: decimal; margin-left: 1rem;">
	                                                <li>
	                                                    <p>提供您設定匯入匯款通知。</p>
	                                                </li>
	                                            </ol>
	                                        </li>
	                                    </ul>
	                                </div>
	                                <div id="carouselGuideIndicator15" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/ntd/N935_1.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/N935_2.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/ntd/N935_3.png" alt="Third slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator15word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟一</h5>
	                                                <p>點選『國內臺幣匯入匯款通知設定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator15" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator15" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟二</h5>
	                                                <p>勾選欲設定之『帳號』及填寫『金額』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator15" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator15" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟三</h5>
	                                                <p>顯示設定之結果。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator15" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator15" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 操作手冊下載 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-16" role="tabpanel" aria-labelledby="nav-home-tab">
	                        	<h3 class="guide-function-title">操作手冊下載</h3>
								<div style="display: flex">
									<div class="element-area" style="background-color: #ffffff">
										<!-- 登入 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('login')">
												<img src="${__ctx}/img/icon-ob_login.svg?a=${jscssDate}" style="border-radius:0%;">
												<span><spring:message code="LB.Login" /></span>
											</a>
										</div>
										<!-- 我的首頁 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('Myhome')">
												<img src="${__ctx}/img/menu-icon-01.svg?a=${jscssDate}">
												<span><spring:message code="LB.My_home" /></span>
											</a>
										</div>
										<!-- 帳戶總覽 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('AccountOverview')">
												<img src="${__ctx}/img/menu-icon-02.svg?a=${jscssDate}">
												<span><spring:message code="LB.Account_Overview" /></span>
											</a>
										</div>
										<!-- 臺幣服務 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('NTDService')">
												<img src="${__ctx}/img/menu-icon-03.svg?a=${jscssDate}">
												<span><spring:message code="LB.NTD_Services" /></span>
											</a>
										</div>
										<!-- 外幣服務 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('ForeignCurrency')">
												<img src="${__ctx}/img/menu-icon-04.svg?a=${jscssDate}">
												<span><spring:message code="LB.FX_Service" /></span>
											</a>
										</div>
										<!-- 繳費繳稅 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('PaymentofTaxes')">
												<img src="${__ctx}/img/menu-icon-05.svg?a=${jscssDate}">
												<span><spring:message code="LB.W0366" /></span>
											</a>
										</div>
										<!-- 貸款專區 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('LoanService')">
												<img src="${__ctx}/img/menu-icon-06.svg?a=${jscssDate}">
												<span><spring:message code="LB.Loan_Service" /></span>
											</a>
										</div>
									</div>
								</div>
								<div style="display: flex">
									<div class="element-area" style="background-color: #ffffff">
										<!-- 基金 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('FundsBond')">
												<img src="${__ctx}/img/menu-icon-07.svg?a=${jscssDate}">
												<span><spring:message code="LB.Funds" /></span>
											</a>
										</div>
										<!-- 黃金存摺 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('GoldPassbook')">
												<img src="${__ctx}/img/menu-icon-08.svg?a=${jscssDate}">
												<span><spring:message code="LB.W1428" /></span>
											</a>
										</div>
										<!-- 信用卡 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('CreditCard')">
												<img src="${__ctx}/img/menu-icon-09.svg?a=${jscssDate}">
												<span><spring:message code="LB.Credit_Card" /></span>
											</a>
										</div>
										<!-- 線上服務專區 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('OnlineServices')">
												<img src="${__ctx}/img/menu-icon-10.svg?a=${jscssDate}" style="border-radius:0%;">
												<span><spring:message code="LB.X0262" /></span>
											</a>
										</div>
										<!-- 個人服務 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('PersonalServices')">
												<img src="${__ctx}/img/menu-icon-11.svg?a=${jscssDate}">
												<span><spring:message code="LB.Personal_Service" /></span>
											</a>
										</div>
										<!-- 線上申請 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('Register')">
												<img src="${__ctx}/img/icon-03.svg?a=${jscssDate}">
												<span><spring:message code="LB.Register" /></span>
											</a>
										</div>
										<!-- 附件 -->
										<div class="element-item">
											<a href="javascript:void(0)" onclick="pdfdownload('Attachment')">
												<img src="${__ctx}/img/icon-attach.svg?a=${jscssDate}">
												<span>附件</span>
											</a>
										</div>
									</div>
								</div>
	                        </div>
	                        <input type="BUTTON" class="ttb-button btn-flat-gray" value="返回" name="CMBACK" id="CMBACK">
	                    </div>
	                </div>
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
	<script type="text/javascript">
		//步驟說明變換
		var carousel1 = $('#carouselGuideIndicator').carousel();
		var carousel2= $('#carouselGuideIndicatorWord').carousel();
		carousel1.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel2.carousel(to);
		});
	
		var carousel3 = $('#carouselGuideIndicator2').carousel();
		var carousel4= $('#carouselGuideIndicator2word').carousel();
		carousel3.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel4.carousel(to);
		});
	
		var carousel5 = $('#carouselGuideIndicator3').carousel();
		var carousel6= $('#carouselGuideIndicator3word').carousel();
		carousel5.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel6.carousel(to);
		});
	
		var carousel7 = $('#carouselGuideIndicator4').carousel();
		var carousel8= $('#carouselGuideIndicator4word').carousel();
		carousel7.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel8.carousel(to);
		});
	
		var carousel9 = $('#carouselGuideIndicator5').carousel();
		var carousel10= $('#carouselGuideIndicator5word').carousel();
		carousel9.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel10.carousel(to);
		});
	
		var carousel11 = $('#carouselGuideIndicator6').carousel();
		var carousel12= $('#carouselGuideIndicator6word').carousel();
		carousel11.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel12.carousel(to);
		});
	
		var carousel13 = $('#carouselGuideIndicator7').carousel();
		var carousel14= $('#carouselGuideIndicator7word').carousel();
		carousel13.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel14.carousel(to);
		});
	
		var carousel15 = $('#carouselGuideIndicator8').carousel();
		var carousel16= $('#carouselGuideIndicator8word').carousel();
		carousel15.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel16.carousel(to);
		});
	
		var carousel17 = $('#carouselGuideIndicator9').carousel();
		var carousel18= $('#carouselGuideIndicator9word').carousel();
		carousel17.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel18.carousel(to);
		});
		
		var carousel19 = $('#carouselGuideIndicator10').carousel();
		var carousel20= $('#carouselGuideIndicator10word').carousel();
		carousel19.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel20.carousel(to);
		});
		
		var carousel21 = $('#carouselGuideIndicator11').carousel();
		var carousel22= $('#carouselGuideIndicator11word').carousel();
		carousel21.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel22.carousel(to);
		});
		
		var carousel23 = $('#carouselGuideIndicator12').carousel();
		var carousel24= $('#carouselGuideIndicator12word').carousel();
		carousel23.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel24.carousel(to);
		});
		
		var carousel25 = $('#carouselGuideIndicator13').carousel();
		var carousel26= $('#carouselGuideIndicator13word').carousel();
		carousel25.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel26.carousel(to);
		});
		
		var carousel27 = $('#carouselGuideIndicator14').carousel();
		var carousel28= $('#carouselGuideIndicator14word').carousel();
		carousel27.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel28.carousel(to);
		});
		
		var carousel29 = $('#carouselGuideIndicator15').carousel();
		var carousel30= $('#carouselGuideIndicator15word').carousel();
		carousel29.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel30.carousel(to);
		});
	</script>
</body>
</html>
