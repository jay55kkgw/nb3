<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript">
		// 初始化
		$(document).ready(function () {
			init();
		});

		function init() {
			//表單驗證
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
			// 回約定變更首頁 click
			cmback();
			// 回基金損益餘額查詢頁 click
			cmback2();

		}
		
		//列印
		function print() {
			var params = {
				"jspTemplateName": 'regular_investment_result_print',
				"MsgName":'<spring:message code= "LB.Change_successful" />',
				"jspTitle": '<spring:message code= "LB.W1057" />',
				"hide_CDNO": '${regular_investment_result.data.hide_CDNO}',
				"FUNDLNAME": '${regular_investment_result.data.FUNDLNAME}',
				"str_MIP": '${regular_investment_result.data.str_MIP}',
				"str_ALTERTYPE1": '${regular_investment_result.data.str_ALTERTYPE1}',
				"str_ALTERTYPE2": '${regular_investment_result.data.str_ALTERTYPE2}',
				"str_ALTERTYPE3": '${regular_investment_result.data.str_ALTERTYPE3}',
				"str_OPAYAMT": '${regular_investment_result.data.str_OPAYAMT}',
				"str_NPAYAMT": '${regular_investment_result.data.str_NPAYAMT}',
				"ADCCYNAME": '${regular_investment_result.data.ADCCYNAME}',
				"display_OPAYAMT": '${regular_investment_result.data.display_OPAYAMT}',
				"display_PAYAMT": '${regular_investment_result.data.display_PAYAMT}',
				"DAMT": '${regular_investment_result.data.DAMT}',
				"OPAYDAY1": '${regular_investment_result.data.OPAYDAY1}',
				"OPAYDAY2": '${regular_investment_result.data.OPAYDAY2}',
				"OPAYDAY3": '${regular_investment_result.data.OPAYDAY3}',
				"OPAYDAY4": '${regular_investment_result.data.OPAYDAY4}',
				"OPAYDAY5": '${regular_investment_result.data.OPAYDAY5}',
				"OPAYDAY6": '${regular_investment_result.data.OPAYDAY6}',
				"OPAYDAY7": '${regular_investment_result.data.OPAYDAY7}',
				"OPAYDAY8": '${regular_investment_result.data.OPAYDAY8}',
				"OPAYDAY9": '${regular_investment_result.data.OPAYDAY9}',
				"PAYDAY1": '${regular_investment_result.data.PAYDAY1}',
				"PAYDAY2": '${regular_investment_result.data.PAYDAY2}',
				"PAYDAY3": '${regular_investment_result.data.PAYDAY3}',
				"PAYDAY4": '${regular_investment_result.data.PAYDAY4}',
				"PAYDAY5": '${regular_investment_result.data.PAYDAY5}',
				"PAYDAY6": '${regular_investment_result.data.PAYDAY6}',
				"PAYDAY7": '${regular_investment_result.data.PAYDAY7}',
				"PAYDAY8": '${regular_investment_result.data.PAYDAY8}',
				"PAYDAY9": '${regular_investment_result.data.PAYDAY9}',
				"str_Before": '${regular_investment_result.data.str_Before}',
				"str_After": '${regular_investment_result.data.str_After}',
			};
			openWindowWithPost("${__ctx}/print",
				"height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",
				params);
		}
		
		// 回約定變更首頁 click
		function cmback() {
			$("#CMBACK").click(function (e) {
				var action = '${__ctx}/FUND/ALTER/regular_investment';
				initBlockUI();
				$("form").attr("action", action);
				$("#formId").validationEngine('detach');
				$("form").submit();
			});
		}
		// 回基金損益餘額查詢頁 click
		function cmback2() {
			$("#CMBACK2").click(function (e) {
				var action = '${__ctx}/FUND/QUERY/profitloss_balance';
				initBlockUI();
				$("form").attr("action", action);
				$("#formId").validationEngine('detach');
				$("form").submit();
			});
		}
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金    -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0901" /></li>
    <!-- 基金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1064" /></li>
    <!-- 定期投資約定變更     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1057" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!--定期投資約定變更 -->
				<h2>
					<spring:message code="LB.W1057" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form autocomplete="off" method="post" id="formId" action="${__ctx}/">
					<c:set var="BaseResultData" value="${regular_investment_result.data}"></c:set>
					<!-- 表單顯示區 -->
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<div class="ttb-message">
									<span><spring:message code="LB.Change_successful" /></span>
								</div>
								<!-- 信託帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0944" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${BaseResultData.hide_CDNO}</span>
										</div>
									</span>
								</div>

								<!--扣款標的-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W1041" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.FUNDLNAME}
											</span>
										</div>
									</span>
								</div>
								<!--類別 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.D0973" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												<spring:message code="${BaseResultData.str_MIP}" />
											</span>
										</div>
									</span>
								</div>
								<!-- 變更項目 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W1153" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
											<c:if test="${BaseResultData.str_ALTERTYPE1 != ''}">
												<spring:message code="LB.Deposit_amount_1" />
											</c:if>
											<c:if test="${BaseResultData.str_ALTERTYPE2 != ''}">
												<spring:message code="LB.W1155" />
											</c:if>
											<c:if test="${BaseResultData.str_ALTERTYPE3 != ''}">
												<spring:message code="LB.W1156" />
											</c:if>
												
											</span>
										</div>
									</span>
								</div>
								<c:if test="${BaseResultData.str_ALTERTYPE1 != ''}">
								<!--原每次定期申購金額 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4>
												<spring:message code="${BaseResultData.str_OPAYAMT}" />
											 </h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.ADCCYNAME}
												${BaseResultData.display_OPAYAMT}
											</span>
										</div>
									</span>
								</div>
								<!--變更後每次定期申購金額 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4>
											<spring:message code="${BaseResultData.str_NPAYAMT}" />
											 </h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.ADCCYNAME}
												${BaseResultData.display_PAYAMT}
											</span>
										</div>
									</span>
								</div>
								</c:if>
								<c:if test="${BaseResultData.str_ALTERTYPE2 != ''}">
									<c:if test="${BaseResultData.DAMT == '2'}">
								<!--原日期 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W1158" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.OPAYDAY4}
												${BaseResultData.OPAYDAY5}
												${BaseResultData.OPAYDAY6}
												${BaseResultData.OPAYDAY1}
												${BaseResultData.OPAYDAY2}
												${BaseResultData.OPAYDAY3}
											</span>
										</div>
									</span>
								</div>
								<!--變更後日期 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W1159" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.PAYDAY4}
												${BaseResultData.PAYDAY5}
												${BaseResultData.PAYDAY6}
												${BaseResultData.PAYDAY1}
												${BaseResultData.PAYDAY2}
												${BaseResultData.PAYDAY3}
											</span>
										</div>
									</span>
								</div>
									</c:if>
									<c:if test="${BaseResultData.DAMT != '2'}">
										<!--原日期 -->
										<div class="ttb-input-item row">
											<span class="input-title">
												<label>
													<h4><spring:message code="LB.W1158" /></h4>
												</label>
											</span>
											<span class="input-block">
												<div class="ttb-input">
													<span>
														${BaseResultData.OPAYDAY1}
														${BaseResultData.OPAYDAY2}
														${BaseResultData.OPAYDAY3}
														${BaseResultData.OPAYDAY4}
														${BaseResultData.OPAYDAY5}
														${BaseResultData.OPAYDAY6}
														${BaseResultData.OPAYDAY7}
														${BaseResultData.OPAYDAY8}
														${BaseResultData.OPAYDAY9}
													</span>
												</div>
											</span>
										</div>
										<!--變更後日期 -->
										<div class="ttb-input-item row">
											<span class="input-title"><label>
													<h4><spring:message code="LB.W1159" /></h4>
												</label></span>
											<span class="input-block">
												<div class="ttb-input">
													<span>
														${BaseResultData.PAYDAY1}
														${BaseResultData.PAYDAY2}
														${BaseResultData.PAYDAY3}
														${BaseResultData.PAYDAY4}
														${BaseResultData.PAYDAY5}
														${BaseResultData.PAYDAY6}
														${BaseResultData.PAYDAY7}
														${BaseResultData.PAYDAY8}
														${BaseResultData.PAYDAY9}
													</span>
												</div>
											</span>
										</div>
									</c:if>
								</c:if>
								<c:if test="${BaseResultData.str_ALTERTYPE3 != ''}">
								<!--原扣款狀態 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.X0371" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												<spring:message code="${BaseResultData.str_Before}" />											
											</span>
										</div>
									</span>
								</div>
								<!--變更後扣款狀態 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W1161" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												<spring:message code="${BaseResultData.str_After}" />
											</span>
										</div>
									</span>
								</div>
							</c:if>
							</div>
							<!-- button -->
							<!-- 列印 -->
							<input class="ttb-button btn-flat-orange" type="button" id="printbtn" onclick="print()" value="<spring:message code="LB.Print" />" />
							<input class="ttb-button btn-flat-gray" id="CMBACK" type="button" value="<spring:message code="LB.Back_to_function_home_page" />"  />
							<input class="ttb-button btn-flat-gray" id="CMBACK2" type="button" value="<spring:message code="LB.X0413" />" >
							<!-- button -->
						</div>
					</div>
					
					<div class="text-left">
						<!-- 						說明： -->
						<!-- <spring:message code="LB.Description_of_page"/>: -->
					</div>
				</form>
			</section>
			<!-- main-content END -->
		</main>
	</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>