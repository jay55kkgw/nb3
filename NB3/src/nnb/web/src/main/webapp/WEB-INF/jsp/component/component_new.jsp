<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${__ctx}/component/combo/topWebSocketUtil.js"></script>
</head>
<body>
<!-- MAC版多合一元件的載點 -->
<a href="${__ctx}/component/combo/Setup-TbbComboMacNativeAgentHost.pkg"
	id="MacComponent" style="display: none"></a>
	
<!-- Windows版多合一元件的載點 -->
<a href="${__ctx}/component/combo/Install_TbbComboNativeAgentHost.exe"
	id="windowComponent" style="display: none"></a>

<form id="componentForm" action="${__ctx}/COMPONENT/component_download" method="post">
	<input type="hidden" name="componentPath" id="componentPath" /> <input
		type="hidden" name="trancode" value="ComponentDownload" />
</form>

<script type="text/JavaScript">
	// 動態載入JavaScript
	jQuery.loadScript = function(url, callback) {
		jQuery.ajax({
			url : url,
			dataType : 'script',
			success : callback,
			async : false
		});
	}
	
	$(document).ready(function() {
		var isIkeyUser; // 使用者是否ikey使用者
		var comVersion; // 各元件的最新版本號
		var platformNew; // 作業系統
		var hasAskDownload = false; // 因為現在WINDOW多瀏覽器元件的安裝檔整合成一個，所以要多一個變數，只詢問使用者下載一次
		
		// 載入FSTOP.js使用自定義Ajax
		$.loadScript('${__ctx}/js/fstop.js', function() {
			console.log("loading...fstop.js");
		});
		
		// 判斷使用者是否ikey使用者
		component_isikeyuser();
		// 取得各元件的最新版本號
		component_version();
		// 取得裝置作業系統
		component_platform();
		
 		// 元件邏輯 (進畫面後1秒提示)
 		setTimeout(component_init(),1000);
	});

	// 判斷使用者是否ikey使用者
	function component_isikeyuser() {
		var uri = '${__ctx}' + "/COMPONENT/isikeyuser_aj";
		var bs = fstop.getServerDataEx(uri, null, false);
		console.log("isIkeyUser_bs: " + JSON.stringify(bs));

		isIkeyUser = bs.result;
		console.log("isIkeyUser: " + isIkeyUser);
	}

	// 取得各元件的最新版本號
	function component_version() {
		var uri = '${__ctx}' + "/COMPONENT/component_version_aj";
		var bs = fstop.getServerDataEx(uri, null, false);
		console.log("comVersion_bs: " + JSON.stringify(bs));

		comVersion = bs.data;
		console.log("comVersion: " + JSON.stringify(comVersion));
	}

	// 判斷裝置
	function checkBrowser() {
		var checkBrowserResult = false;
		if (navigator.userAgent.match(/Android/i)
				|| navigator.userAgent.match(/webOS/i)
				|| navigator.userAgent.match(/iPhone/i)
				|| navigator.userAgent.match(/iPad/i)
				|| navigator.userAgent.match(/iPod/i)
				|| navigator.userAgent.match(/BlackBerry/i)
				|| navigator.userAgent.match(/Windows Phone/i)) {
			// 行動裝置
			console.log("component checkBrowser not pass!!!");
		} else {
			// 可使用元件之裝置
			checkBrowserResult = true;
			console.log("component checkBrowser pass...");
		}

		console.log("checkBrowser: " + checkBrowserResult);

		return checkBrowserResult;
	}

	// 取得裝置作業系統
	function component_platform() {
		platformNew = navigator.platform;
		if (window.console) {
			console.log("component_platform: " + platformNew);
		}
	}
	
	function component_init(){
		// 行動裝置不能做，所以要先判斷
		if (checkBrowser()) {
// 				$.loadScript(
// 					'${__ctx}/component/combo/topWebSocketUtil.js', function() {
// 						console.log("loading...topWebSocketUtil.js");
// 				});
				
				topWebSocketUtil.setWssUrl("wss://localhost:9203/", 
			            ValidateLegalURL_Callback_Both);
		}
	}

	// 底下必須
	//------------------------------------------------------------------------------
	// 底下必須有，這是第一關
	// 等到 ValidateLegalURL_Callback 回呼方法被呼叫後，
	// 再經由 topWebSocketUtil.invokeRpcDispatcher 呼叫元件方法
	//------------------------------------------------------------------------------
	// Name: ValidateLegalURL_Callback
	function ValidateLegalURL_Callback_Both(rpcStatus, rpcReturn)
	{	
		try {
	            //------------------------------------------------------------------
		       		topWebSocketUtil.tryRpcStatus(rpcStatus);
		        //------------------------------------------------------------------
		     // 未安裝或關閉元件
		     //alert("Client's ValidateLegalURL_Callback rpcReturn: " + rpcReturn);
				if (rpcReturn == "E_Send_11_OnError_1006") {
	        		hasAskDownload = true;
	    			if (confirm("<spring:message code= "LB.Confirm012" />")) {
	    				//修改 HTTP Response Splitting
	    				if(platformNew.indexOf("Win") > -1){
	    					//WINDOWS
	    					if (window.console) {
		    					console.log("windowComponent href="
		    							+ jQuery("#windowComponent").attr(
		    									"href"));
		    				}
		    				jQuery("#componentPath").val("win");
		    				jQuery("#componentForm").submit();
	    				}else{
	    					//MAC
		    				if (window.console) {
		    					console.log("MacComponent href="
		    							+ jQuery("#MacComponent").attr(
		    									"href"));
		    				}
		    				jQuery("#componentPath").val("mac");
		    				jQuery("#componentForm").submit();
		    			}
	    			}
	    		//安裝完成,檢查更新
	        	}else if (rpcReturn =="E000"||rpcReturn == null){
	        		rpcName = "GetVersion";
	        		topWebSocketUtil.invokeRpcDispatcher(GetVersion_Callback,rpcName);
	        		
	        		// GetVersion_Callback
	        		function GetVersion_Callback(rpcStatus,rpcReturn){
	        			if(window.console){console.log("GetVersion_Callback...");}
	        			try{
	        				topWebSocketUtil.tryRpcStatus(rpcStatus);
	        				
	        				var currentVersion = rpcReturn;
	        				if(window.console){console.log("currentVersion: " + currentVersion);}
	        				if(window.console){console.log("newVersion: " + comVersion.NoneIEReaderVersion);}
	        				
	        				var currentVersionNO = parseInt(currentVersion.replace(/\./g,""));
	        				var newVersionNO = parseInt(comVersion.NoneIEReaderVersion.replace(/\./g,""));
	        				
	        				if(window.console){console.log("currentVersionNO="+currentVersionNO);}
	        				if(window.console){console.log("newVersionNO="+newVersionNO);}
	        				
	        				// 不用更新
	        				if(currentVersionNO >= newVersionNO){
	        					//
	        				}
	        				// 要更新
	        				else{
	        					if (confirm("<spring:message code= "LB.Confirm013" />")) {
	        						//修改 HTTP Response Splitting
	        						if(platformNew.indexOf("Win") > -1){
	        	    					//WINDOWS
	        	    					if (window.console) {
	        		    					console.log("windowComponent href="
	        		    							+ jQuery("#windowComponent").attr(
	        		    									"href"));
	        		    				}
	        		    				jQuery("#componentPath").val("win");
	        		    				jQuery("#componentForm").submit();
	        	    				}else{
	        	    					//MAC
	        		    				if (window.console) {
	        		    					console.log("MacComponent href="
	        		    							+ jQuery("#MacComponent").attr(
	        		    									"href"));
	        		    				}
	        		    				jQuery("#componentPath").val("mac");
	        		    				jQuery("#componentForm").submit();
	        		    			}
	        					}
	        		        }
	        				
	        			} catch (exception){
	        				if(window.console){console.log("exception="+exception);}
	        				//alert("<spring:message code= "LB.X1655" />");
	        				errorBlock(
	    							null, 
	    							null,
	    							["<spring:message code= 'LB.X1655' />"], 
	    							'<spring:message code= "LB.Quit" />', 
	    							null
	    						);
	        			}
	        		}
	        	}
				
		} catch (exception) {
		        alert("exception: " + exception);
		}
	}
</script>
</body>
</html>