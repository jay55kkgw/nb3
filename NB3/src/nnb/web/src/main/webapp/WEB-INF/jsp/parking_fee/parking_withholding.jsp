<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>

	<script type="text/javascript">
		$(document).ready(function () {
			init();
		});
		function init() {
			//若前頁有帶acn，預設為此acn
			var getacn = '${result_data.data.Acn}';
			if(getacn != null && getacn != ''){
				$("#getAcn").val(getacn);
			}
			
			//btn 
			$("#CMSUBMIT").click(function (e) {
				console.log($("input[name*=FXLCCTYPE]:checked").val());
// 				window.location.href =  "./parking_withholding_" + $("input[name*=FXLCCTYPE]:checked").val();

// 	        	initBlockUI();
	        	var action = '${__ctx}/PARKING/FEE/parking_withholding_' + $("input[name*=FXLCCTYPE]:checked").val();
	        	$('#formId').attr("action", action);
				$('#formId').submit();
			});
		} // init END
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 自動扣繳服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2360" /></li>
    <!-- 停車費     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0330" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!--匯入匯款查詢(未輸入) -->
					<!-- <spring:message code="LB.NTD_Demand_Deposit_Detail" /> -->
					<spring:message code="LB.D0330"/>
				</h2>
				
				<form id="formId" method="post">
				<input type="hidden" id="getAcn" name="getAcn" value="">
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-input-block">
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<!-- 信用狀性質 -->
											<spring:message code="LB.D0169"/>
										</label>
									</span>
									<span class="input-block">
										<!-- 停車費代扣繳申請-->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code= "LB.X0276" />
												<input type="radio" name="FXLCCTYPE" id="apply" value="apply" checked />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!--  資料維護/註銷申請-->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.D0333"/>
												<input type="radio" name="FXLCCTYPE" id="modify" value="modify" />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<select name="ZoneCode" id="ZoneCode" class="custom-select select-input half-input  validate[required]">
												<option value="">
													<spring:message code="LB.All"/>
												</option>
												<c:forEach var="dataList" items="${result_data.data.REC_sort}">
													<option value="${dataList.ZoneCode}"> ${dataList.ZoneName}</option>
												</c:forEach>
											</select>
										</div>
										<div class="ttb-input">
											<input class="text-input" type="text" name="cardid" id="cardid" placeholder="(<spring:message code="LB.Not_required"/>)">
										</div>
										<!--  擔保信用狀/保障函 -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.D0335"/>
												<input type="radio" name="FXLCCTYPE" id="query" value="query" />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<select name="QueryTerms_Str" id="QueryTerms_Str" class="custom-select select-input half-input validate[required]">
												<option value="All">
													<spring:message code="LB.D0336"/>
												</option>
												<option value="CarId">
													<spring:message code="LB.D0337"/>
												</option>
												<option value="Account">
													<spring:message code="LB.D0338"/>
												</option>
											</select>
										</div>
									</span>
								</div>
							</div>
							<!-- 網頁顯示 button-->
							
								<!--網頁顯示 -->
								<spring:message code="LB.Display_as_web_page" var="cmSubmit"></spring:message>
								<input type="button" name="CMSUBMIT" id="CMSUBMIT" value="<spring:message code="LB.Confirm_1"/>" class="ttb-button btn-flat-orange">
							
						</div>
					</div>
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>