<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
</head>
<body>
	<!--   IDGATE -->
	<%@ include file="../idgate_tran/idgateAlert.jsp" %>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 自動扣繳服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2360" /></li>
    <!-- 自動扣繳查詢/取消     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0768" /></li>
		</ol>
	</nav>

		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 --> 
 		<div class="content row">
 		   <%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
 		
 		<main class="col-12">	

		<!-- 		主頁內容  -->

			<section id="main-content" class="container">
		
				<h2><spring:message code="LB.W0768" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
<!-- 				<div id="step-bar"> -->
<!-- 					<ul> -->
<!-- 						<li class="finished">輸入資料</li> -->
<!-- 						<li class="finished">確認資料</li> -->
<!-- 						<li class="active">交易完成</li> -->
<!-- 					</ul> -->
<!-- 				</div> -->
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
                            <div class="ttb-message">
                                <span><spring:message code="LB.D0183" /></span>
                            </div>
							<c:set var="dataSet" value="${withholding_cancel_result.data}" />
<!-- 								系統時間 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.Data_time" />
									</label>
								</span>
								<span class="input-block">
									<label>
										${dataSet.CMQTIME}		
									</label>
								</span>
							</div>
<!-- 								代繳類別 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W0781" />
									</label>
								</span>
								<span class="input-block">
									<label>
										<spring:message code="${withholding_cancel_result.data.MEMO_C}" />	
									</label>
								</span>
							</div>
<!--								 扣帳帳號     -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W0702" />
									</label>
								</span>
								<span class="input-block">
									<label>
										${dataSet.TSFACN}
									</label>
								</span>
							</div>
					
							<c:if test="${withholding_cancel_result.data.MEMO_C == 'ILD'}">
	<!-- 					           	保險證字號    -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.W0722" />
										</label>
									</span>
									<span class="input-block">
										<label>
											${dataSet.UNTNUM}
										</label>
									</span>
								</div>
							</c:if>
						
							<c:if test="${withholding_cancel_result.data.MEMO_C == 'CKD'}">
	<!-- 					               扣帳金額	-->								
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.W0742" />
										</label>
									</span>
									<span class="input-block">
										<label>
											${dataSet.CUSNUM}
										</label>
									</span>
								</div>
	<!-- 					            投保單位代號     -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.W0706" />
										</label>
									</span>
									<span class="input-block">
										<label>
											${dataSet.UNTNUM}
										</label>
									</span>
								</div>
							</c:if>	
						
<!-- 					          投保單位營利事業/負責人<br>身分證統一編號     -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W0725" /><br><spring:message code="LB.D0519" />
									</label>
								</span>
								<span class="input-block">
									<label>
										${dataSet.CUSIDNUN}
									</label>
								</span>
							</div>
<!-- 					            投保單位電話號碼    -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.W0729" />
									</label>
								</span>
								<span class="input-block">
									<label>
										${dataSet.UNTTEL}
									</label>
								</span>
							</div>
						</div>
						<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
					</div>
				</div>
				<div class="text-left">
					<ol class="list-decimal description-list">
                    	<p><spring:message code="LB.Description_of_page" /></p>
						<li><spring:message code="LB.Withhold_Cancel_P5_D1" /></li>
						<li><spring:message code="LB.Withhold_Cancel_P5_D2" /></li>
					</ol>
				</div>
			</section>
			<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
	
	<script type="text/javascript">
			$(document).ready(function(){
				// HTML載入完成後開始遮罩
				setTimeout("initBlockUI()", 50);
				// 開始跑下拉選單並完成畫面
				setTimeout("init()", 400);
				// 解遮罩
				setTimeout("unBlockUI(initBlockId)", 500);
			});
			
			function init(){
				$("#printbtn").click(function(){
					var params = {
						"jspTemplateName":"withholding_cancel_3_print",
						"jspTitle":"<spring:message code= "LB.W0768" />",
						"CMQTIME":"${dataSet.CMQTIME}",
						"MEMO_C":"${withholding_cancel_result.data.MEMO_C}",
						"TSFACN":"${dataSet.TSFACN}",
						"UNTNUM":"${dataSet.UNTNUM}",
						"CUSNUM":"${dataSet.CUSNUM}",
						"CUSIDNUN":"${dataSet.CUSIDNUN}",
						"UNTTEL":"${dataSet.UNTTEL}"
					};
					openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
				});

			}
	</script>
</body>
</html>