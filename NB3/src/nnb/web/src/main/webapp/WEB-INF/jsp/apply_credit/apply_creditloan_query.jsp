<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>



<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript">
	$(document).ready(function() {
		setTimeout("initDataTable()",100);
		
		// 將.table變更為footable
		//initFootable();
		$("#printbtn").click(function(){
			//小額信貸申請資料查詢
			var params = {
					jspTemplateName:	"apply_creditloan_query_print",
					jspTitle:			"<spring:message code= "LB.D0654" />",
					CMQTIME:			"${BaseResult.data.CMQTIME}",
					CMRECNUM:			"${BaseResult.data.CMRECNUM}"
				};
				openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});	
	});
</script>
</head>
	<body>
		<!-- header -->
		<header>
			<%@ include file="../index/header.jsp"%>
		</header>
	
		<!-- 麵包屑 -->
		<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
			<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
				<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
				
				<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0654" /></li><!-- 小額信貸申請資料查詢 -->
			</ol>
		</nav>
		<div class="content row">
			<!-- menu、登出窗格 -->
			<%@ include file="../index/menu.jsp"%>
			<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2><spring:message code="LB.D0654" /></h2><!-- 小額信貸申請資料查詢 -->
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<ul class="ttb-result-list">
								<li>
									<!-- 查詢時間 -->
									<h3>
									<spring:message code="LB.Inquiry_time" />：
									</h3>
									<p>
									${BaseResult.data.CMQTIME}
									</p>
								</li>
								<li>
									<!-- 資料總數 -->
									<h3>
									<spring:message code="LB.Total_records" />：
									</h3>
									<p>
									${BaseResult.data.CMRECNUM}&nbsp;<spring:message code="LB.Rows"/>
									</p>	
								</li>
								<li>
								<!-- 表名 -->
								    <h3>
								    <spring:message code="LB.Report_name" />
								    </h3>
								    <p>
								    <spring:message code="LB.D0654" />
								    </p>
								</li>
							</ul>
							<!-- 小額信貸申請資料查詢-->
							<table class="stripe table-striped ttb-table dtable" id="table1" data-show-toggle="first">
								<thead>
								<!--  
									<tr>
										
										<th data-title="<spring:message code="LB.Report_name" />"><spring:message code="LB.Report_name" /></th>
									
										<th data-title="<spring:message code="LB.D0654" />" class="text-left" colspan="5"><spring:message code="LB.D0654" /></th>
									</tr>
							-->
									<tr>
										<!-- 案件編號-->
										<th  data-title="<spring:message code="LB.Loan_CaseNo"/>"><spring:message code="LB.Loan_CaseNo"/></th>
										<!-- 貸款種類-->
										<th data-title="<spring:message code="LB.D0544" />"><spring:message code="LB.D0544" /></th>
										<!-- 貸款金額-->
										<th data-title="<spring:message code="LB.D0660" />"><spring:message code="LB.D0660" /></th>
										<!-- 貸款期數-->
										<th data-title="<spring:message code="LB.D0661" />"><spring:message code="LB.D0661" /></th>
										<!-- 對保分行-->
										<th data-title="<spring:message code="LB.D0662" />"><spring:message code="LB.D0662" /></th>
										<!-- 審核狀況 -->
										<th data-title="<spring:message code="LB.D0128" />"><spring:message code="LB.D0128" /></th>
									</tr>
								</thead>
								<tbody>
								<c:if test="${empty BaseResult.data.REC}">
								<tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								</tr>
								</c:if>
								<c:if test="${not empty BaseResult.data.REC}">
									<c:forEach var="row" items="${BaseResult.data.REC}">
										<tr>
											<!-- 案件編號-->
											<td class="text-center">${row.RCVNO}</td>
											<!-- 貸款種類-->
											<td class="text-center"><spring:message code="${row.APPKIND}" /></td>
											<!-- 貸款金額-->
											<td class="text-right">${row.APPAMT} <spring:message code="LB.D0088_2" /></td><!-- 萬元 -->
											<!-- 貸款期數-->
											<td class="text-center">${row.PERIODS} <spring:message code="LB.W0854" /></td><!-- 期-->
											<!-- 對保分行-->
											<td class="text-center">${row.BANKNA}</td>
											<!-- 審核狀況-->
											<td class="text-center"><spring:message code="${row.STATUS}" /></td>
										</tr>
									</c:forEach>
								</c:if>
								</tbody>
							</table>
							<input type="button" class="ttb-button btn-flat-orange" id ="printbtn" value="<spring:message code="LB.Print" />"/>
						</div>
					</div>
				</form>
				</section>
			</main>
			<!-- main-content END -->
		</div>
		<!-- content row END -->
		<%@ include file="../index/footer.jsp"%>
	</body>
</html>