<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>


<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
		initFootable();
		$('#but_Agree').prop('checked', false);
		// 確認鍵 click
		submit();
	}
	
	// 確認鍵 Click
	function submit() {
		$("#CMSUBMIT").click( function(e) {
			console.log("submit~~");
			// 遮罩
         	initBlockUI();
			var next = "${x_article.data.type}"+"_confirm";
            $("#formId").attr("action","${__ctx}/OTHER/FEE/"+next);
	 	  	$("#formId").submit();
		});
		//上一頁按鈕
		$("#CMBACK").click(function() {
			var back = "${x_article.data.type}";
			initBlockUI();
			fstop.getPage('${pageContext.request.contextPath}'+'/OTHER/FEE/'+ back,'', '');
		});
	}

</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

	<!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i
					class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><a href="#"></a></li>
			<li class="ttb-breadcrumb-item active" aria-current="page"></li>
		</ol>

	</nav>
	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<form id="formId" method="post" action="">
				<div class="main-content-block row">
                    <div class="col-12">
                        <div class="ttb-message">
							<p><spring:message code="LB.X0085" /></p>
							<span>下列為申請委託轉帳代繳公用事業費用(水、電、瓦斯、電信費用..等)應遵守之約定條款內容，您如接受本約定條款則請按<span class="Cron">我同意約定條款</span>鍵，以完成申請作業，您如不同意條款內容，則請按<span class="Cron">回上頁</span>鍵，本行將不受理您的代扣繳申請。</span>	
                        </div>
						<div class="ttb-message"><p align="left"><font color="royalblue" size="3"><b></b></font></P></div>
                        <ul class="ttb-result-list terms">
                            <li data-num="一、">
                            	<p>本人【公司】(以下簡稱立約人)委託臺灣中小企業銀行(以下簡稱貴行)自指定之存款帳戶(即指前頁之扣帳帳號，以下簡稱轉帳代繳帳戶)轉帳代繳公用事業費用，並自行依據最近月份公用事業費用繳款單據內容填寫代扣繳資料，如因代扣繳申請書內容填寫不全、錯誤或其他原因，致貴行無法辦理轉帳，則本約定書不生效力，所受損失由立約人自行負責。</p>
                            </li>
							<li data-num="二、">
                            	<p>立約人申請代繳本人【公司】或指定第三人公用事業費用，自貴行同意接受委託，並將轉帳代繳檔案資料送至公用事業機構審核，經公用事業機構電腦處理並按繳款日遞送扣繳資料起履行代繳義務，在貴行未收到扣繳資料前各月份之費用，仍由公用事業費用繳款人自行繳納。</p>
                            </li>
                            <li data-num="三、">
                            	<p>貴行代繳義務，以立約人轉帳代繳帳戶可用餘額足敷各項公用事業當期應繳費用為限(即轉帳代繳帳戶須保持足夠之可用餘額以供備付)。轉帳代繳帳戶餘額不敷繳付時，立約人及貴行均應依據各公用事業機構轉帳代繳作業規定辦理，貴行不負墊款或部份付款之義務。貴行未收到繳費資料而無法代繳時，亦不負通知立約人之義務。</p>
                            </li>
                            <li data-num="四、">
                            	<p>立約人委託貴行代繳公用事業費用之用戶編號或號碼，倘貴行接獲有關公用事業機構改號通知時，立約人同意貴行以異動後之用戶編號或號碼，繼續委託代繳。</p>
                            </li>
                            <li data-num="五、">
                            	<p>立約人委託代繳公用事業費用，倘轉帳代繳帳戶存款不足、遭法院扣押、發生繼承或其它事故，致無法代繳時，貴行得終止代繳之約定，並將繳費資料退回各公用事業機構，因此而遭遇罰款、停用等情事所引起之損失及責任，概由立約人自行負責處理。/p>
                            </li>
                            <li data-num="六、">
                            	<p>立約人委託代繳公用事業費用，在未終止委託前，不得藉故拒絕繳費，因此引起之損失及責任，概由立約人自行負責。</p>
                            </li>
                            <li data-num="七、">
                            	<p>立約人委託代繳公用事業費用，在未終止委託前，自行結清轉帳代繳帳戶時，視同當然終止代繳之約定，應繳納之公用事業費用需由公用事業費用繳款人自行持繳款單至公用事業機構指定之繳費處所繳納，因此須負擔之滯納金，概由立約人自行負責。</p>
                            </li>
                            <li data-num="八、">
                            	<p>貴行或立約人皆得隨時以書面通知對方終止代繳契約。立約人終止代繳時應填具「註銷委託轉帳代繳公用事業費用約定書」，並自貴行接受註銷委託，將轉帳代繳檔案資料送至公用事業機構審核，且完成變更通知之月份起，終止以該轉帳代繳帳戶轉帳代繳公用事業費用。因註銷委託須負擔之滯納金，概由立約人負責。</p>
                            </li>
                            <li data-num="九、">
                            	<p>立約人對公用事業費用、費率、費額之計算暨退補費等事項如有疑義，應自行與公用事業機構洽詢。</p>
                            </li>
                            <li data-num="十、">
                            	<p>立約人指定之轉帳代繳帳戶為支票存款帳戶者，倘因扣繳公用事業費用而致存款不足，發生退票情事，概由立約人負責。</p>
                            </li>
                            <li data-num="十一、">
                            	<p>倘貴行之電腦系統發生故障或電信中斷等因素致無法執行轉帳代繳時，貴行得順延至系統恢復正常，始予扣款，其因上開事由所致之損失及責任，由立約人自行負擔。</p>
                            </li>
                            <li data-num="十二、">
                            	<p>貴行於同一日需自轉帳代繳帳戶執行多筆轉帳扣繳作業而立約人存款不足時，立約人同意由貴行自行選定扣款順序。</p>
                            </li>
                            <li data-num="十三、">
                            	<p>立約人委託代繳公用事業費用之收據由公用事業機構寄發。</p>
                            </li>
                            <li data-num="十四、">
                            	<p>立約人同意貴行得將立約人個人根據特定目的填列之相關基本資料提供貴行電腦處理及利用。</p>
                            </li>
    					</ul>
                      	<input class="ttb-button btn-flat-orange" type="submit" name="CMSUBMIT" id="CMSUBMIT" value="<spring:message code="LB.W1554"/>" />
						<input class="ttb-button btn-flat-gray" type="button" name="CMBACK" id="CMBACK" value="<spring:message code="LB.Back_to_previous_page"/>"/>
						<input type="hidden" name="type" value="${x_article.data.type}">
						<input type="hidden" name="FGTXDATE" value="${x_article.data.FGTXDATE}">
                    </div>
                </div>
				</form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>