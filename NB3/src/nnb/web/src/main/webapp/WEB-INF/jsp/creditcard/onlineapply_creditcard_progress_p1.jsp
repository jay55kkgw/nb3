<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
	    <script type="text/javascript">
        $(document).ready(function () {
            init();
        });

        function init() {
        	$('.ttb-unit').attr("style","display:none");
	    	$("#CMSUBMIT").click(function(e){
	        	$("#formId").validationEngine({binded:false,promptPosition: "inline" });
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
// 		        	initBlockUI();
		        	var action = '${__ctx}/CREDIT/APPLY/onlineapply_creditcard_progress_p2';
					$("form").attr("action", action);
	    			$("form").submit();
				}
			});
        }





    	/**************************************/
    	/*     			 發送簡訊   			      */
    	/**************************************/
    	var sec = 120;
    	var the_Timeout;

    	function countDown()
    	{
    	     var main = document.getElementById("formId");
    	                
    	      var counter = document.getElementById("CountDown");                      
    	       counter.innerHTML = sec;
    	       sec--;
    	       
    	       if (sec == -1) 
    	       {              		
    				$("#getotp").prop("disabled",false);
    				$("#getotp").removeClass("btn-flat-gray");
    				$("#getotp").addClass("btn-flat-orange");
    			   sec =120;

    	           return;                
    	       }                                     
    	           
    	       //網頁倒數計時(120秒)              
    	       the_Timeout = setTimeout("countDown()", 1000);    
    	} 
    	    //取得otp並發送簡訊
    	    function getsmsotp() {
    	    	var cusidn = '${sessionScope.cusidn_online_apply}';
    	    	cusidn = atob(cusidn);
    			var main = document.getElementById("formId");
    			$("#getotp").prop("disabled",true);
    			$("#getotp").removeClass("btn-flat-orange");
    			$("#getotp").addClass("btn-flat-gray");
    			
    			sec = 120;
    			countDown();
    			$.ajax({
    		        type :"POST",
    		        url  : "${__ctx}/RESET/smsotpwithphone_ajax",
    		        data : { 
    		        	CUSIDN: cusidn,
    		        	ADOPID: 'NA03',
    		        	MSGTYPE: 'OLAPPLY'
    		        },
    		        async: false ,
    		        dataType: "json",
    		        success : function(msg) {
    		        	callbackgetsmsotp(msg)
    		        }
    		    })
    		}
    	    
    	    
    		function callbackgetsmsotp(response) {
    			//alert(response.responseText);
    		    var main = document.getElementById("formId");
    			//eval("var result = " + response.responseText);
    			console.log(response);
    			var msgCode = response[0].MSGCOD;
    			var msgName = response[0].MSGSTR;
    			var phone = '${sessionScope.mobile}';
    			if(msgCode=="0000")
    			{	
    				phone = phone = phone.substring(0,6)+"***"+phone.substring(9);
    			    //alert("<spring:message code= "LB.Alert198" />");
    			    errorBlock(
    					null, 
    					null,
    					["<spring:message code= 'LB.X2280' />"+phone+"<br/><spring:message code= 'LB.X2281' />"], 
    					'<spring:message code= "LB.Quit" />', 
    					null
    				);
    			    main.OTP.focus();	
    			    $('.ttb-unit').attr("style","display:block");		
    			}
    			else
    			{
    				sec = 0;
    	      		var counter = document.getElementById("CountDown");                      
    	       		counter.innerHTML = sec;		
    				$("#getotp").prop("disabled",false);
    				$("#getotp").removeClass("btn-flat-gray");
    				$("#getotp").addClass("btn-flat-orange");
    				errorBlock(
    						null, 
    						null,
    						["<spring:message code= 'LB.error_code' />" + ":" + msgCode + " " + msgName], 
    						'<spring:message code= "LB.Quit" />', 
    						null
    					);
    			}				
    		}
    	



        
    </script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	<!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<!--信用卡 -->
			<li class="ttb-breadcrumb-item"><spring:message code= "LB.Credit_Card" /></a></li>
			<!--信用卡補上傳資料 -->
			<li class="ttb-breadcrumb-item"><a href="#"><spring:message code= "LB.onlineapply_creditcard_progress_TITLE" /></a></li>
		</ol>
	</nav>
	<div class="content row">
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2><spring:message code= "LB.onlineapply_creditcard_progress_TITLE" /></h2>
				<div id="step-bar">
                    <ul>
                        <li class="finished"><spring:message code="LB.D0851" /></li><!-- 身份驗證 -->
                        <li class="active"><spring:message code="LB.onlineapply_creditcard_progress_OTP" /></li><!-- OTP驗證 -->
                        <li class=""><spring:message code="LB.D0359" /></li><!-- 查詢結果 -->
                    </ul>
                </div>
                <form id="formId" method="post">
					<input type="hidden" name="ADOPID" value="NA03"/>
				  	<input type="hidden" name="HLOGINPIN" id="HLOGINPIN" value="">
				  	<input type="hidden" name="HTRANSPIN" id="HTRANSPIN" value="">
					<input type="hidden" name="LMTTYN" id="LMTTYN" value="">	
					
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
                        
							<div class="ttb-input-block">
								<div class="ttb-message">
	                                <p><spring:message code="LB.onlineapply_creditcard_progress_OTP" /></p>
	                            </div>
									
								<!--輸入簡訊驗證碼-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                    <label>
	                                        <h4><spring:message code= "LB.X1574" /></h4>
	                                    </label>
	                                </span>
	                                <span class="input-block">
	                                    <div class="ttb-input">
	                                      <input type="text" maxLength="8" size="10" class="ttb-input text-input input-width-125 validate[required,funcCall[validate_CheckLenEqual[<spring:message code="LB.Captcha"/>,OTP,false,8,8]],funcCall[validate_CheckNumber['LB.Captcha',OTP]]]" id="OTP" name="OTP" value=""/>
	                                      <input class="ttb-sm-btn btn-flat-orange" type="button" id="getotp" name="getotp" value="<spring:message code= "LB.X1576" />" onclick="javascript:getsmsotp()" />
	                                    </div>
										<span class="ttb-unit"><spring:message code= "LB.X1575" />：<font id="CountDown" color="red"></font><spring:message code= "LB.Second" /></span>
	                                </span>
	                            </div>								
                           	</div>  
	                       	<input type="button" id="CMBACK" value="<spring:message code="LB.D0171"/>" class="ttb-button btn-flat-gray" onClick="window.close();"/>
	                        <input id="CMSUBMIT" name="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code= "LB.X0080" />" />
                        </div>
                    </div>
                </form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>