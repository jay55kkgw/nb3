<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<!--舊版驗證-->
<script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
<style type="text/css">
.table-1 {
    width: 100%;
    border-spacing: 0;
    border-collapse: collapse;
    margin: 0 auto;
    border:
}

.table-1>thead>tr>th,
.table-1>thead>tr>td {
    background-color: #EEEEEE;
    border-top: none;
    text-shadow: 0 1px 0 rgba(255, 255, 255, .5);
    font-weight: bold;
}

.table-1>tbody>tr>td,
.table-1>thead>tr>th {
    border: 1px solid #DDD;
    padding: 8px;
    text-align: left;
}

.table-1 tbody tr:first-child td:first-child {
    text-align: center;
}

.table-1 td:hover {
    background-color: #fbf8e9;
}
</style>
<script type="text/JavaScript">
$(document).ready(function() {
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	initFootable();
	if(${bs.data.errorMessage != null && bs.data.errorMessage != ""}){
		//alert("${bs.data.errorMessage}");
		errorBlock(
				null, 
				null,
				["${bs.data.errorMessage}"], 
				'<spring:message code= "LB.Quit" />', 
				null
			);
	}
	
	$("#okButton").click(function(){
		if($("#picFile1").val() == ""){
			//請上傳身分證正面圖檔
			//alert("<spring:message code= "LB.X1074" />");
			errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1074' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			return false;
		}
		if($("#picFile11").val() == ""){
			//請上傳身分證反面圖檔
			//alert("<spring:message code= "LB.X1075" />");
			errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1075' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			return false;
		}
		if($("#picFile2").val() == ""){
			//請上傳財力證明圖檔
			//alert("<spring:message code= "LB.X1076" />");
			errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1076' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			return false;
		}
		
		//交易密碼
		if(!CheckPuzzle("CMPWD")){
			return false;
		}
		$("#CMPASSWORD").val($("#CMPWD").val());
		
		var PINNEW = pin_encrypt($("#CMPASSWORD").val());
		$("#PINNEW").val(PINNEW);
		initBlockUI();
		$("#formId").submit();

	});
});
function previewImage(sourceID,targetID){
	var fileReader = new FileReader();
	fileReader.readAsDataURL(document.getElementById(sourceID).files[0]);

	fileReader.onload = function(e){
		document.getElementById(targetID).src = e.target.result;
	};
}
</script>
</head>
<body>
<!-- header -->
<header>
	<%@ include file="../index/header.jsp"%>
</header>	
<!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<!--申請小額信貸 -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0543" /></a></li>
		</ol>
	</nav>
<!--左邊menu及登入資訊-->
<div class="content row">
	<%@ include file="../index/menu.jsp"%>
<!--快速選單及主頁內容-->
	<main class="col-12">
		<!--主頁內容-->
		<section id="main-content" class="container">
			<h2><!--申請小額信貸 -->
					<spring:message code= "LB.D0543" />
					</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form method="post" id="formId" action="${__ctx}/APPLY/CREDIT/apply_creditloan_p8" enctype="multipart/form-data">
				<input type="hidden" name="ADOPID" value="NA01"/>
				<input type="hidden" name="APPKIND" value="${bs.data.APPKIND}"/>
				<input type="hidden" name="APPAMT" value="${bs.data.APPAMT}"/>
				<input type="hidden" name="PURPOSE" value="${bs.data.PURPOSE}"/>
				<input type="hidden" name="PERIODS" value="${bs.data.PERIODS}"/>
				<input type="hidden" name="BANKID" value="${bs.data.BANKID}"/>
				<input type="hidden" name="BANKNA" value="${bs.data.BANKNA}"/>
				<input type="hidden" name="APPNAME" value="${bs.data.APPNAME}"/>
				<input type="hidden" name="SEX" value="${bs.data.SEX}"/>
				<input type="hidden" name="APPIDNO" value="${bs.data.cusidn}"/>
				<input type="hidden" name="BRTHDT" value="${bs.data.BRTHDT}"/>
				<input type="hidden" name="EDUS" value="${bs.data.EDUS}"/>
				<input type="hidden" name="MARITAL" value="${bs.data.MARITAL}"/>
				<input type="hidden" name="CHILDNO" value="${bs.data.CHILDNO}"/>
				<input type="hidden" name="CPHONE" value="${bs.data.CPHONE}"/>
				<input type="hidden" name="PHONE_01" value="${bs.data.PHONE_01}"/>
				<input type="hidden" name="PHONE_02" value="${bs.data.PHONE_02}"/>
				<input type="hidden" name="PHONE_03" value="${bs.data.PHONE_03}"/>
				<input type="hidden" name="PHONE_11" value="${bs.data.PHONE_11}"/>
				<input type="hidden" name="PHONE_12" value="${bs.data.PHONE_12}"/>
				<input type="hidden" name="HOUSE" value="${bs.data.HOUSE}"/>
				<input type="hidden" name="MONEY11" value="${bs.data.MONEY11}"/>
				<input type="hidden" name="MONEY12" value="${bs.data.MONEY12}"/>
				<input type="hidden" name="ZIP2" value="${bs.data.ZIP2}"/>
				<input type="hidden" name="CITY2" value="${bs.data.CITY2}"/>
				<input type="hidden" name="ZONE2" value="${bs.data.ZONE2}"/>
				<input type="hidden" name="ADDR2" value="${bs.data.ADDR2}"/>
				<input type="hidden" name="ZIP1" value="${bs.data.ZIP1}"/>
				<input type="hidden" name="CITY1" value="${bs.data.CITY1}"/>
				<input type="hidden" name="ZONE1" value="${bs.data.ZONE1}"/>
				<input type="hidden" name="ADDR1" value="${bs.data.ADDR1}"/>
				<input type="hidden" name="EMAIL" value="${bs.data.EMAIL}"/>
				<input type="hidden" name="CKS1" value="${bs.data.CKS1}"/>
				<input type="hidden" name="RELTYPE1" value="${bs.data.RELTYPE1}"/>
				<input type="hidden" name="RELNAME1" value="${bs.data.RELNAME1}"/>
				<input type="hidden" name="RELID1" value="${bs.data.RELID1}"/>
				<input type="hidden" name="CKS2" value="${bs.data.CKS2}"/>
				<input type="hidden" name="RELTYPE2" value="${bs.data.RELTYPE2}"/>
				<input type="hidden" name="RELNAME2" value="${bs.data.RELNAME2}"/>
				<input type="hidden" name="RELID2" value="${bs.data.RELID2}"/>
				<input type="hidden" name="CKS3" value="${bs.data.CKS3}"/>
				<input type="hidden" name="RELTYPE3" value="${bs.data.RELTYPE3}"/>
				<input type="hidden" name="RELNAME3" value="${bs.data.RELNAME3}"/>
				<input type="hidden" name="RELID3" value="${bs.data.RELID3}"/>
				<input type="hidden" name="CKS4" value="${bs.data.CKS4}"/>
				<input type="hidden" name="RELTYPE4" value="${bs.data.RELTYPE4}"/>
				<input type="hidden" name="RELNAME4" value="${bs.data.RELNAME4}"/>
				<input type="hidden" name="RELID4" value="${bs.data.RELID4}"/>
				<input type="hidden" name="CKP1" value="${bs.data.CKP1}"/>
				<input type="hidden" name="OTHCONM1" value="${bs.data.OTHCONM1}"/>
				<input type="hidden" name="OTHCOID1" value="${bs.data.OTHCOID1}"/>
				<input type="hidden" name="CKM1" value="${bs.data.CKM1}"/>
				<input type="hidden" name="MOTHCONM1" value="${bs.data.MOTHCONM1}"/>
				<input type="hidden" name="MOTHCOID1" value="${bs.data.MOTHCOID1}"/>
				<input type="hidden" name="CAREERNAME" value="${bs.data.CAREERNAME}"/>
				<input type="hidden" name="CAREERIDNO" value="${bs.data.CAREERIDNO}"/>
				<input type="hidden" name="INCOME" value="${bs.data.INCOME}"/>
				<input type="hidden" name="ZIP4" value="${bs.data.ZIP4}"/>
				<input type="hidden" name="CITY4" value="${bs.data.CITY4}"/>
				<input type="hidden" name="ZONE4" value="${bs.data.ZONE4}"/>
				<input type="hidden" name="ADDR4" value="${bs.data.ADDR4}"/>
				<input type="hidden" name="PHONE_41" value="${bs.data.PHONE_41}"/>
				<input type="hidden" name="PHONE_42" value="${bs.data.PHONE_42}"/>
				<input type="hidden" name="PHONE_43" value="${bs.data.PHONE_43}"/>
				<input type="hidden" name="PROFNO" value="${bs.data.PROFNO}"/>
				<input type="hidden" name="OFFICEY" value="${bs.data.OFFICEY}"/>
				<input type="hidden" name="OFFICEM" value="${bs.data.OFFICEM}"/>
				<input type="hidden" name="PRECARNAME" value="${bs.data.PRECARNAME}"/>
				<input type="hidden" name="PRETKY" value="${bs.data.PRETKY}"/>
				<input type="hidden" name="PRETKM" value="${bs.data.PRETKM}"/>
				<input type="hidden" name="PAYSOURCE" value=" ${bs.data.PAYSOURCE}"/>
  				<input type="hidden" name="STATUS" value="0"/>
  				<input type="hidden" name="ECERT" value="0"/>
    			<input type="hidden" name="VERSION" value="10405"/>
    			<input type="hidden" name="IP" value="${bs.data.IP}"/>
    			<input type="hidden" name="APPKINDNAME" value="${bs.data.APPKINDNAME}"/>
    			<input type="hidden" name="PURPOSENAME" value="${bs.data.PURPOSENAME}"/>
    			<input type="hidden" name="SEXNAME" value="${bs.data.SEXNAME}"/>
    			<input type="hidden" name="EDUSNAME" value="${bs.data.EDUSNAME}"/>
    			<input type="hidden" name="MARITALNA" value="${bs.data.MARITALNA}"/>
    			<input type="hidden" name="HOUSENAME" value="${bs.data.HOUSENAME}"/>
    			<input type="hidden" name="PROFNONAME" value="${bs.data.PROFNONAME}"/>
  				<!--交易機制所需欄位-->
  				<input type="hidden" id="CMPASSWORD" name="CMPASSWORD"/>
				<input type="hidden" id="PINNEW" name="PINNEW"/>
				<div class="main-content-block row">
					<div class="col-12">
						<div class="ttb-message">
							<!-- 請確認申請資料 -->
							<font color="red"><b><spring:message code= "LB.D0104" /></b></font>
						</div>
						<table class="table-1 four-col" data-toggle-column="first">
							<thead>
								<tr>
									<!-- 貸款資料 -->
									<th style="text-align: center"><b><spring:message code= "LB.D0558" /></b></th>
								</tr>
							</thead>
							<tbody>
								<tr>
<!-- 貸款種類 -->
									<td  style="text-align: left"><spring:message code= "LB.D0544" />：
										<spring:message code="${bs.data.APPKINDNAME}" /></td>
								</tr>
								<tr>
									<td  style="text-align: left">
<!-- 申請金額：新臺幣 -->
<!-- 萬元整 -->
										<spring:message code= "LB.W0803" />${bs.data.APPAMT}<spring:message code= "LB.D0562" /></td>
								</tr>
								<tr>
									<td  style="text-align: left">
<!-- 資金用途 -->
										<spring:message code= "LB.D0564" />：<spring:message code="${bs.data.PURPOSENAME}" /></td>
								</tr>
								<tr>
<!-- 申請還款期數 -->
									<td  style="text-align: left"><spring:message code= "LB.D0573" />：
<!-- 個月 -->
										${bs.data.PERIODS}<spring:message code= "LB.D0574" /></td>
								</tr>
								<tr>
<!-- 指定對保分行 -->
									<td  style="text-align: left"><spring:message code= "LB.D0575" />：
										${bs.data.BANKNA}</td>
								</tr>
							</tbody>
							<thead>
								<tr>
<!-- 個人資料 -->
									<th  style="text-align: center"><b><spring:message code= "LB.D0576" /></b></th>
								</tr>
							</thead>
							<tbody>
								<tr>
								<!-- 姓名 -->
									<td style="text-align: left"><spring:message code= "LB.D0203" />：${bs.data.APPNAME}</td>
								</tr>
								<tr>
<!-- 性別 -->
									<td style="text-align: left"><spring:message code= "LB.D0578" />：<spring:message code="${bs.data.SEXNAME}" /></td>
								</tr>
								<tr>
<!-- 身分證字號 -->
									<td style="text-align: left"><spring:message code= "LB.D0581" />：${bs.data.cusidn}</td>
								</tr>
								<tr>
									<td  style="text-align: left">
<!-- 出生日期 -->
										<spring:message code= "LB.D0582" />：${bs.data.BRTHDT}</td>
								</tr>
								<tr>
									<td style="text-align: left">
<!-- 教育程度 -->
										<spring:message code= "LB.D0055" />：<spring:message code="${bs.data.EDUSNAME}" /></td>
								</tr>
								<tr>
<!-- 婚姻狀況 -->
									<td style="text-align: left"><spring:message code= "LB.D0057" />：<spring:message code="${bs.data.MARITALNA}" /></td>
								</tr>
								<tr>
<!-- 子女數 -->
<!-- 人 -->
									<td style="text-align: left"><spring:message code= "LB.D0602_1" />：${bs.data.CHILDNO}<spring:message code= "LB.D0602_2" /></td>
								</tr>
								<tr>
<!-- 行動電話 -->
									<td style="text-align: left"><spring:message code= "LB.D0069" />：${bs.data.CPHONE}</td>
								</tr>
								<tr>
									<td style="text-align: left">
<!-- 通訊電話 -->
										<spring:message code= "LB.D0599" />：（${bs.data.PHONE_01}）${bs.data.PHONE_02}
<!-- 分機 -->
										<spring:message code= "LB.D0095" />：${bs.data.PHONE_03}</td>
								</tr>
								<tr>
									<td style="text-align: left">
<!-- 戶籍電話 -->
										<spring:message code= "LB.D0144" />：（${bs.data.PHONE_11}）${bs.data.PHONE_12}</td>
								</tr>
								<tr>
									<td  style="text-align: left">
<!-- 住宅狀況 -->
										<spring:message code= "LB.D0603" />：<spring:message code="${bs.data.HOUSENAME}" /> <br /> <c:if
											test="${bs.data.MONEY11 != ''}">
<!-- 房貸月繳 -->
<!-- 元 -->
										<spring:message code= "LB.D0609_1" />：${bs.data.MONEY11}<spring:message code= "LB.Dollar" />
									</c:if> <c:if test="${bs.data.MONEY12 != ''}">
<!-- 房租月繳 -->
<!-- 仟元 -->
										<spring:message code= "LB.D0610_1" />：${bs.data.MONEY12}<spring:message code= "LB.D0610_2" />
									</c:if>
									</td>
								</tr>
								<tr>
									<td  style="text-align: left">
<!-- 現住地址 -->
										<spring:message code= "LB.D0611" />：${bs.data.ZIP2}${bs.data.CITY2}${bs.data.ADDR2}
									</td>
								</tr>
								<tr>
									<td  style="text-align: left">
<!-- 戶籍地址 -->
										<spring:message code= "LB.D0143" />：${bs.data.ZIP1}${bs.data.CITY1}${bs.data.ADDR1}
									</td>
								</tr>
								<tr>
									<td  style="text-align: left">
										E-MAIL：${bs.data.EMAIL}</td>
								</tr>
							</tbody>
							<thead>
								<tr>
								<!-- ※配偶及二親等以內血親：為遵循銀行法第33條之3所定授信限額之規定而取得「同一關係人」之基本資料包括祖（外祖）父母、父母、兄弟姊妹、子女、孫（外孫）子女 -->
									<th  style="text-align: left"><b>※<spring:message code= "LB.D0616" />：<spring:message code= "LB.D0617" /></b>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr>
								<!-- 稱謂 -->
									<td style="text-align: left"><spring:message code= "LB.D0618" />：${bs.data.RELTYPE1}</td>
								</tr>
								<tr>
<!-- 姓名 -->
									<td style="text-align: left"><spring:message code= "LB.D0203" />：${bs.data.RELNAME1}</td>
								</tr>
								<tr>
<!-- 身分證字號 -->
									<td style="text-align: left"><spring:message code= "LB.D0581" />：${bs.data.RELID1}</td>
								</tr>
								<tr>
<!-- 稱謂 -->
									<td style="text-align: left"><spring:message code= "LB.D0618" />：${bs.data.RELTYPE2}</td>
								</tr>
								<tr>
<!-- 姓名 -->
									<td style="text-align: left"><spring:message code= "LB.D0203" />：${bs.data.RELNAME2}</td>
								</tr>
								<tr>
<!-- 身分證字號 -->
									<td style="text-align: left"><spring:message code= "LB.D0581" />：${bs.data.RELID2}</td>
								</tr>
								<tr>
<!-- 稱謂 -->
									<td style="text-align: left"><spring:message code= "LB.D0618" />：${bs.data.RELTYPE3}</td>
								</tr>
								<tr>
<!-- 姓名 -->
									<td style="text-align: left"><spring:message code= "LB.D0203" />：${bs.data.RELNAME3}</td>
								</tr>
								<tr>
<!-- 身分證字號 -->
									<td style="text-align: left"><spring:message code= "LB.D0581" />：${bs.data.RELID3}</td>
								</tr>
								<tr>
<!-- 稱謂 -->
									<td style="text-align: left"><spring:message code= "LB.D0618" />：${bs.data.RELTYPE4}</td>
								</tr>
								<tr>
<!-- 姓名 -->
									<td style="text-align: left"><spring:message code= "LB.D0203" />：${bs.data.RELNAME4}</td>
								</tr>
								<tr>
<!-- 身分證字號 -->
									<td style="text-align: left"><spring:message code= "LB.D0581" />：${bs.data.RELID4}</td>
								</tr>
							</tbody>
							<thead>
								<tr>
<!-- 職 業 資 料 -->
									<th style="text-align: center"><b><spring:message code= "LB.W0831" /></b></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="text-align: left">
<!-- 本人擔任負責人之其他企業名稱 -->
										<spring:message code= "LB.D0620" />：${bs.data.OTHCONM1}</td>
								</tr>
								<tr>
<!-- 統一編號 -->
									<td style="text-align: left"><spring:message code= "LB.D0621" />：${bs.data.OTHCOID1}</td>
								</tr>
								<tr>
									<td style="text-align: left">
<!-- 配偶擔任負責人之企業名稱 -->
										<spring:message code= "LB.D0622" />：${bs.data.MOTHCONM1}</td>
								</tr>
								<tr>
<!-- 統一編號 -->
									<td style="text-align: left"><spring:message code= "LB.D0621" />：${bs.data.MOTHCOID1}</td>	
								</tr>
							</tbody>
							<thead>
								<tr>
								<!-- 本人服務單位 -->
									<th style="text-align: center"><b><spring:message code= "LB.D0623" /></b></th>
								</tr>
							</thead>
							<tbody>
								<tr>
<!-- 公司名稱 -->
									<td style="text-align: left"><spring:message code= "LB.D0086" />：${bs.data.CAREERNAME}</td>
								</tr>
								<tr>
<!-- 統一編號 -->
									<td style="text-align: left"><spring:message code= "LB.D0621" />：${bs.data.CAREERIDNO}</td>
								</tr>
								<tr>
									<td  style="text-align: left">
<!-- 年收入 -->
<!-- 萬元 -->
										<spring:message code= "LB.D0625_1" />：${bs.data.INCOME}<spring:message code= "LB.D0088_2" /></td>
								</tr>
								<tr>
<!-- 公司地址 -->
									<td  style="text-align: left"><spring:message code= "LB.D0090" />：
										${bs.data.ZIP4}${bs.data.CITY4}${bs.data.ADDR4}
									</td>
								</tr>
								<tr>
									<td style="text-align: left">
<!-- 公司電話 -->
										<spring:message code= "LB.D0094" />：（${bs.data.PHONE_41}）${bs.data.PHONE_42}
<!-- 分機 -->
										<spring:message code= "LB.D0095" />：${bs.data.PHONE_43}</td>
								</tr>
								<tr>
<!-- 職稱 -->
									<td style="text-align: left"><spring:message code= "LB.D0087" />：${bs.data.PROFNONAME}</td>
								</tr>
								<tr>
									<td style="text-align: left">
<!-- 到職日：民國 -->
<!-- 年 -->
<!-- 月 -->
										<spring:message code= "LB.W0842" />${bs.data.OFFICEY}<spring:message code= "LB.D0089_2" />${bs.data.OFFICEM}<spring:message code= "LB.D0089_3" /></td>
								</tr>
								<tr>
<!-- 前職公司名稱 -->
									<td style="text-align: left"><spring:message code= "LB.D0630" />：${bs.data.PRECARNAME}
									</td>
								</tr>
								<tr>
<!-- 前職工作年資 -->
									<td style="text-align: left"><spring:message code= "LB.D0631" />： 
									<c:if test="${bs.data.PRETKY != ''}">
<!-- 年 -->
										${bs.data.PRETKY}<spring:message code= "LB.D0089_2" /></c:if> 
									<c:if test="${bs.data.PRETKM != ''}">
<!-- 個月 -->
											${bs.data.PRETKM}<spring:message code= "LB.D0574" /></c:if>
									</td>
								</tr>
							</tbody>
							<thead>
								<tr>
								<!-- 線上資料傳輸 -->
									<th style="text-align: center"><b><spring:message code= "LB.D0110" /></b></th>
								</tr>
							</thead>
							<tbody>
								<tr>
<!-- 身分證正面圖片 -->
									<td style="text-align: center"><spring:message code= "LB.D0111" />：</td>
								</tr>
								<tr>
									<td style="text-align: center"><img id="msgPic1"
										src="${__ctx}/img/MissingPic.gif" width="300px" height="225px" />
									</td>
								</tr>
								<tr>
								<!-- 預覽 -->
									<td  style="text-align: left">
										
										 <!-- <input
										type="button" value="<spring:message code= "LB.D0644" />"
										onclick="previewImage('picFile1','msgPic1')" />
										 -->
										<div class="ttb-input">
                                       <input type="file" id="picFile1" name="picFile1" />
                                        <button type="button" class="btn-flat-orange" name="reshow" onclick="previewImage('picFile1','msgPic1')"><spring:message code= "LB.D0644" /></button>
                                    </div>
										<!-- 圖片上傳注意事項 -->
										 <br /><spring:message code= "LB.D0114" />：<br />
										 <!-- 圖檔大小不可超過3.3MB -->
										1﹒<spring:message code= "LB.D0115" /><br> 
										<!-- 圖片尺寸不可超過(寬)640px(長)480px. -->
										2﹒<spring:message code= "LB.D0116" /></td>
								</tr>

								<tr>
<!-- 身分證反面圖片 -->
									<td style="text-align: center"><spring:message code= "LB.D0118" />：</td>
								</tr>
								<tr>
									<td style="text-align: center"><img id="msgPic11"
										src="${__ctx}/img/MissingPic.gif" width="300px" height="225px" />
									</td>
								</tr>
								<tr>
								<!-- 預覽 -->
									<td style="text-align: left">
										<!-- 
										<input type="button"
										value="<spring:message code= "LB.D0644" />" onclick="previewImage('picFile11','msgPic11')" />
										-->
										<div class="ttb-input">
                                         <input type="file"
										id="picFile11" name="picFile11" />
                                        <button type="button" class="btn-flat-orange" name="reshow" onclick="previewImage('picFile11','msgPic11')"><spring:message code= "LB.D0644" /></button>
                                    </div>
											<!-- 圖片上傳注意事項 -->
										 <br /><spring:message code= "LB.D0114" />：<br />
										 <!-- 圖檔大小不可超過3.3MB -->
										1﹒<spring:message code= "LB.D0115" /><br> 
										<!-- 圖片尺寸不可超過(寬)640px(長)480px. -->
										2﹒<spring:message code= "LB.D0116" /></td>
								</tr>
								<tr>
<!-- 財力證明圖片 -->
									<td style="text-align: center"><spring:message code= "LB.D0119" />：</td>
								</tr>
								<tr>
									<td style="text-align: center"><img id="msgPic2"
										src="${__ctx}/img/MissingPic.gif" width="300px" height="225px" />
									</td>
								</tr>
								<tr>
								<!-- 預覽 -->
									<td style="text-align: left">
										<!-- 
										<input type="button"
										value="<spring:message code= "LB.D0644" />" onclick="previewImage('picFile2','msgPic2')" /> 
										-->
										<div class="ttb-input">
                                        <input type="file"
										id="picFile2" name="picFile2" />
                                        <button type="button" class="btn-flat-orange" name="reshow" onclick="previewImage('picFile2','msgPic2')"><spring:message code= "LB.D0644" /></button>
                                    </div>
											<!-- 圖片上傳注意事項 -->
										 <br /><spring:message code= "LB.D0114" />：<br />
										 <!-- 圖檔大小不可超過3.3MB -->
										1﹒<spring:message code= "LB.D0115" /><br> 
										<!-- 圖片尺寸不可超過(寬)640px(長)480px. -->
										2﹒<spring:message code= "LB.D0116" /><br>
										<!-- 最近6個月薪轉存摺明細含封面或在職證明或勞保投保明細資料 -->
										3﹒<spring:message code= "LB.D0117" /></td>
								</tr>
								<tr>
<!-- 年資資料圖片 -->
									<td  style="text-align: center"><spring:message code= "LB.D0651" />：</td>
								</tr>
								<tr>
									<td style="text-align: center"><img id="msgPic3"
										src="${__ctx}/img/MissingPic.gif" width="300px" height="225px" />
									</td>
								</tr>
								<tr>
								<!-- 預覽 -->
									<td style="text-align: left">
										<!-- 
										<input
										type="button" value="<spring:message code= "LB.D0644" />"
										onclick="previewImage('picFile3','msgPic3')" />	
										-->
										<div class="ttb-input">
                                       <input
										type="file" id="picFile3" name="picFile3" />
                                        <button type="button" class="btn-flat-orange" name="reshow" onclick="previewImage('picFile3','msgPic3')"><spring:message code= "LB.D0644" /></button>
                                    </div>
										<!-- 圖片上傳注意事項 -->
										 <br /><spring:message code= "LB.D0114" />：<br />
										 <!-- 圖檔大小不可超過3.3MB -->
										1﹒<spring:message code= "LB.D0115" /><br> 
										<!-- 圖片尺寸不可超過(寬)640px(長)480px. -->
										2﹒<spring:message code= "LB.D0116" /><br>
										<!-- 最近6個月薪轉存摺明細含封面或在職證明或勞保投保明細資料 -->
										3﹒<spring:message code= "LB.D0117" /></td>
								</tr>
							</tbody>
							<thead>
								<tr>
<!-- 交易機制 -->
									<th class="text-center"><spring:message code= "LB.D0178" /></th>
								</tr>
							</thead>
							<tbody>
								<tr>
								<!-- 交易密碼 -->
									<td><input type="radio" value="0"
										name="FGTXWAY" id="CMSSL" checked />&nbsp;<spring:message code= "LB.D1443" />（SSL）&nbsp; <input
										type="password" class="text-input" id="CMPWD" name="CMPWD" maxLength="8" size="8" /><br /></td>
								</tr>
							</tbody>	

						</table>
			
							<input type="button" id="okButton" value="<spring:message code="LB.Confirm"/>" class="ttb-button btn-flat-orange"/>
				
					</div>
				</div>
			</form>
		</section>
	</main>
</div>
<%@ include file="../index/footer.jsp"%>
</body>
</html>