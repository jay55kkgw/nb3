<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
	$(document).ready(function() {
		window.print();
	});
</script>
</head>
<body  class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br />
	<br />
	<div style="text-align: center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br />
	<br />
	<br />
	<div style="text-align: center">
		<font style="font-weight: bold; font-size: 1.2em">${jspTitle}</font>
	</div>
	<br />
	<br />
	<br />
	<label><spring:message code="LB.Inquiry_time" />
		<!-- 查詢時間 -->：</label>
	<label>${CMQTIME}</label>
	<br />
	<br />
	<label><spring:message code="LB.Total_records" />
		<!-- 資料總數 -->：</label>
			<label>${COUNT} <spring:message code="LB.Rows"/></label>			
			<!-- 筆 -->
	<br />
	<br />
	<label><spring:message code="LB.Note" />：<!-- 備註 --></label>
	<label>
	<spring:message code="LB.Remind_you" /><!-- 提醒您 -->！
	<spring:message code="LB.NextLoan_P1_note" />
	<!-- 提醒您！「應繳金額」係以查詢日之適用利率計算，於下期繳款日前遇有利率調整情事，「應繳金額」將隨同調整。 -->
	</label>
	<br>
		${NOTE}
	<br>
	<br>
	<div  style="margin-top: 5%">
	<table class="print">
		<thead>
		<tr>
			<td><spring:message code="LB.Report_name" />
				<!-- 表名 --></td>
			<td class="text-left" colspan="6"><spring:message
					code="LB.NTD_loan_detail_inquiry_next_period" />
				<!-- 臺幣下期借款本息查詢 --></td>
		</tr>
		<tr>
			<td><spring:message code="LB.Loan_account" />
				<!-- 帳號 --></td>
			<td><spring:message code="LB.Seq_of_account" />
				<!-- 分行 --></td>
			<td><spring:message code="LB.Original_loan_amount" />
				<!-- 原貸金額 --></td>
			<td><spring:message code="LB.Loan_Outstanding" />
				<!-- 貸款餘額 --></td>
			<td><spring:message code="LB.End_date_of_repay_interest" />
				<!-- 繳息迄日 --></td>
			<td><spring:message code="LB.Repayment_of_every_month" />
				<!-- 應繳金額 --></td>
			<td><spring:message code="LB.Repay_Date_of_every_month" />
				<!-- 下期繳款日 --></td>
		</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${TOPMSG_014 != 'OKLR'}">
					<tr>
						<td>${TOPMSG_014}</td>
						<td  colspan="6">${ADMSGOUT_014}</td>
					</tr>
				</c:when>
				<c:when test="${TOPMSG_014 == 'OKLR'}">
					<c:choose>
						<c:when test="${MSGFLG_014 eq '01'}">
							<tr>
								<td>MSGFLG=01</td>
								<!-- 請洽櫃台 -->
								<td  colspan="6"><spring:message code= "LB.Loan_contact_note" /></td>
							</tr>
						</c:when>
						<c:when test="${MSGFLG_014 eq '00'}">
							<c:forEach var="dataList" items="${dataListMap[0]}">
								<tr>
									<td class="text-center">${dataList.ACN}</td>
									<td class="text-center">${dataList.SEQ}</td>
									<td class="text-right">${dataList.AMTORLN}</td>
									<td class="text-right">${dataList.BAL}</td>
									<td style="text-align:center">${dataList.DATITPY}</td>
									<td class="text-right">${dataList.AMTAPY}</td>
									<td class="text-center">${dataList.DDT}</td>
								</tr>
							</c:forEach>
						</c:when>
					</c:choose>
				</c:when>
			</c:choose>
		</tbody>
	</table>
	</div>
	<br>
	<br>
	<div  style="margin-top: 5%">
	<table class="print">
		<thead>
		<tr>
			<td><spring:message code="LB.Report_name" />
				<!-- 表名 --></td>
			<td class="text-left" colspan="7">
				<spring:message code= "LB.X1422" />
				<!-- 外幣下期借款本息查詢 --></td>
		</tr>
		<tr>
			<td><spring:message code="LB.Loan_account" />
				<!-- 帳號 --></td>
			<td><spring:message code= "LB.Loan_CaseNo" />
				<!-- 案件編號 --></td>
			<td><spring:message code= "LB.Currency" />
				<!-- 幣別 --></td>
			<td><spring:message code="LB.Loan_Outstanding" />
				<!-- 貸款餘額 --></td>
			<td><spring:message code="LB.End_date_of_repay_interest" />
				<!-- 繳息迄日 --></td>
			<td><spring:message code="LB.Repayment_of_every_month" />
				<!-- 應繳金額 --></td>
			<td><spring:message code="LB.Repay_Date_of_every_month" />
				<!-- 下期繳款日 --></td>
			<td><spring:message code= "LB.Data_date" />
				<!-- 資料日期 --></td>
		</tr>
		</thead>
		<tbody>
			<c:if test="${TOPMSG_553 eq 'OKLR'}">
				<c:forEach var="dataList2" items="${dataListMap[1]}">
					<tr>
						<td class="text-center">${dataList2.LNACCNO}</td>
						<td class="text-center">${dataList2.LNREFNO}</td>
						<td class="text-center">${dataList2.LNCCY}</td>
						<td class="text-right">${dataList2.LNCOS}</td>
						<td class="text-center">${dataList2.CUSPYDT}</td>
						<td class="text-right">${dataList2.PRNDAMT}</td>
						<td class="text-center">${dataList2.NXTINTD}</td>
						<td class="text-center">${dataList2.LNUPDATE}</td>
					</tr>
				</c:forEach>
			</c:if>
			<c:if test="${TOPMSG_553 != 'OKLR'}">
				<tr>
					<td>${TOPMSG_553}</td>
					<td  colspan="7">${ADMSGOUT_553}</td>
				</tr>
			</c:if>
		</tbody>
			</table>
			</div>
	<br />
	<br />
	<div class="text-left">
		<p><spring:message code="LB.Description_of_page" /><!-- 說明 --></p>
			<ol class="list-decimal text-left">
				<li><spring:message code="LB.NextLoan_P1_D1" />
					<!-- 本項查詢不包含存單質借、透支、個人投資理財貸款及好享貸回復型額度。 --></li>
				<li><spring:message code="LB.NextLoan_P1_D2" />
					<!-- 延滯繳息還本者，請逕洽原承貸分行辦理。 --></li>
				<li><spring:message code= "LB.NextLoan_P1_D3" /></li>
				<li><spring:message code= "LB.NextLoan_P1_D4" /></li>
				<li>
					<spring:message code="LB.NextLoan_P1_D5" />
					<!-- 消費者新台幣借款之利率調整通知為貸款利率引用本行２年定儲、 本行定儲指數、本行基準利率、本行１年定儲、本行定儲指數 、郵一定儲利率、行員中期利率、郵二定儲利率 、行員消貸利率、 基準利率月調 ，利率引用標準非屬上開者，請逕洽原貸分行。 -->
				</li>
				<li><spring:message code="LB.NextLoan_P1_D6" />
					<!-- 消費者新台幣借款倘延滯繳息還本者，利率引用標準之調整通知請逕洽原貸分行。 --></li>
			</ol>
	</div>
	<br />
</body>
</html>