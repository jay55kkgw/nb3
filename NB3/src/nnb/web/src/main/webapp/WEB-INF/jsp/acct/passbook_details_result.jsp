<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>

	<script type="text/javascript">
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		function init() {
			initDataTable();
			//上一頁按鈕
			$("#previous").click(function () {
				initBlockUI();
				fstop.getPage('${pageContext.request.contextPath}' + '/NT/ACCT/MANAGEMENT/passbook_details', '', '');
			});
			$("#printbtn").click(function () {
				var params = {
					"jspTemplateName": "passbook_details_result_print",
					"jspTitle": "<spring:message code="LB.W0005" />",
					"CMQTIME": "${pbook_details_result.data.CMQTIME}",
					"CMPERIOD": "${pbook_details_result.data.CMPERIOD}",
					"ACN": "${pbook_details_result.data.ACN}",
					"CMRECNUM": "${pbook_details_result.data.CMRECNUM }"
				};
				openWindowWithPost("${__ctx}/print", "height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1", params);
			});
			//繼續查詢
			$("#CMCONTINU").click(function (e) {
				console.log("submit~~");
				initBlockUI(); //遮罩
				$("#formId").attr("action", "${__ctx}/NT/ACCT/MANAGEMENT/passbook_details_result");
				$("#formId").submit();
			});
		}
		//選項
		function formReset() {
			if ($('#actionBar').val() == "excel") {
				$("#downloadType").val("OLDEXCEL");
				$("#templatePath").val("/downloadTemplate/passbook_details.xls");
			} else if ($('#actionBar').val() == "txt") {
				$("#downloadType").val("TXT");
				$("#templatePath").val("/downloadTemplate/passbook_details.txt");
			}
			// 下載EXCEL,TXT submit
			$("#formId").attr("target", "${__ctx}/download");
			$("#formId").submit();
			$('#actionBar').val("");
		}

	</script>

</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 輕鬆理財戶     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0004" /></li>
    <!-- 存摺明細     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0005" /></li>
		</ol>
	</nav>



	<!-- 	快速選單及主頁內容 -->
	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 功能選單內容 -->

		<main class="col-12">
			<!-- 		主頁內容  -->

			<section id="main-content" class="container">
				<h2>
					<spring:message code="LB.W0005" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value="">
							<spring:message code="LB.Downloads" />
						</option>
						<!-- 						下載Excel檔 -->
						<option value="excel">
							<spring:message code="LB.Download_excel_file" />
						</option>
						<!-- 						下載為txt檔 -->
						<option value="txt">
							<spring:message code="LB.Download_txt_file" />
						</option>
					</select>
				</div>
				<div class="main-content-block row">
					<div class="col-12">
						<ul class="ttb-result-list">
						<!-- 查詢時間 -->
                            <li>
                                <h3><spring:message code="LB.Inquiry_time" /> ：</h3>
                                <p>${pbook_details_result.data.CMQTIME }</p>
                            </li>
                        <!-- 查詢期間 -->
                            <li>
                                <h3><spring:message code="LB.Inquiry_period" /> ：</h3>
                                <p>${pbook_details_result.data.CMPERIOD }</p>
                            </li>
                        <!-- 資料總數 -->
                            <li>
                                <h3><spring:message code="LB.Total_records" /> ：</h3>
                                <p>${pbook_details_result.data.CMRECNUM } <spring:message code="LB.Rows" /></p>
                            </li>
                        </ul>
						<c:forEach var="data_rec" items="${pbook_details_result.data.REC}">
							<ul class="ttb-result-list">
	                            <li>
	                                <h3><spring:message code="LB.Account" /></h3>
	                                <p>${data_rec.ACN}</p>
	                            </li>
	                        </ul>
							<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
<%-- 								<thead> --%>
<%-- 									<tr> --%>
<%-- 										<th> --%>
<%-- 											<spring:message code="LB.Account" /> --%>
<%-- 										</th> --%>
<%-- 										<th>${data_rec.ACN}</th> --%>
<%-- 									</tr> --%>
<%-- 								</thead> --%>
								<thead>
									<tr>
										<!--異動日 -->
										<th>
											<spring:message code="LB.Change_date" />
										</th>
										<!--摘要 -->
										<th>
											<spring:message code="LB.Summary_1" />
										</th>
										<!--支出 -->
										<th>
											<spring:message code="LB.W0008" />
										</th>
										<!--收入 -->
										<th>
											<spring:message code="LB.D0088_1" />
										</th>
										<!--結存-->
										<th>
											<spring:message code="LB.W0010" />
										</th>
										<!--資料內容-->
										<th>
											<spring:message code="LB.W0011" />
										</th>
										<!--備註 -->
										<th>
											<spring:message code="LB.Note" />
										</th>
									</tr>
								</thead>
								<!--          列表區 -->
								<tbody>
									<c:forEach var="dataList" items="${data_rec.TABLE }">
										<tr>
											<!--異動日 -->
											<td class="text-center">${dataList.LSTLTD}</td>
											<!--摘要 -->
											<td class="text-center">${dataList.MEMO }</td>
											<!-- 當FORMAT=0時，支出放CODDB，收入放CODCR，否則支出和收入欄合併放DATA -->
											<c:choose>
												<c:when test="${dataList.FORMAT == 0}">
													<!--支出 -->
													<td class="text-right">${dataList.CODDB }</td>
													<!--收入 -->
													<td class="text-right">${dataList.CODCR }</td>					               
												</c:when>
												<c:otherwise>
													<td class="text-right">${dataList.DATA }</td>	
														<td></td>
												</c:otherwise>
											</c:choose>
											<!--結存-->
											<td class="text-right">${dataList.BAL }</td>
											<!--資料內容-->
											<td class="text-center">${dataList.DTA16 }</td>
											<!--備註 -->
											<td class="text-center">${dataList.TRNBRH }</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</c:forEach>
						<!--回上一頁 -->
						<input id="previous" name="previous" type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />" />
						<!--列印 -->
						<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
						<!-- 繼續查詢 -->
						<c:if test="${pbook_details_result.data.TOPMSG == 'OKOV' }">
							<input type="button" name="CMCONTINU" id="CMCONTINU" value="<spring:message code="LB.X0151" />"
								class="ttb-button btn-flat-orange">
						</c:if>
					</div>
				</div>
				<form method="post" id="formId" action="${__ctx}/download">
					<!--繼續查詢用 -->
					<input type="hidden" name="CMSDATE" value='${pbook_details_result.data.CMSDATE}'>
					<input type="hidden" name="CMEDATE" value='${pbook_details_result.data.CMEDATE}'>
					<input type="hidden" name="QUERYNEXT" value='${pbook_details_result.data.QUERYNEXT}'>
					<input type="hidden" name="DONEACNOS" value='${pbook_details_result.data.DONEACNOS}'>
					
					<!-- 下載用 -->
					<input type="hidden" name="downloadFileName" value="<spring:message code="LB.W0005" />" />
					<input type="hidden" name="downloadType" id="downloadType" />
					<input type="hidden" name="templatePath" id="templatePath" />
					<input type="hidden" name="hasMultiRowData" value="true" />
					<input type="hidden" name="CMQTIME" value="${pbook_details_result.data.CMQTIME}" />
					<input type="hidden" name="CMPERIOD" value="${pbook_details_result.data.CMPERIOD}" />
					<input type="hidden" name="ACN" value="${pbook_details_result.data.ACN}" />
					<input type="hidden" name="CMRECNUM" value="${pbook_details_result.data.CMRECNUM}" />
					<!-- EXCEL下載用 -->
					<input type="hidden" name="headerRightEnd" value="1" />
					<input type="hidden" name="headerBottomEnd" value="4" />
					<input type="hidden" name="multiRowStartIndex" value="8" />
					<input type="hidden" name="multiRowEndIndex" value="8" />
					<input type="hidden" name="multiRowCopyStartIndex" value="6" />
					<input type="hidden" name="multiRowCopyEndIndex" value="8" />
					<input type="hidden" name="multiRowDataListMapKey" value="TABLE" />
					<input type="hidden" name="rowRightEnd" value="6" />
					<!-- TXT下載用 -->
					<input type="hidden" name="txtHeaderBottomEnd" value="6" />
					<input type="hidden" name="txtMultiRowStartIndex" value="11" />
					<input type="hidden" name="txtMultiRowEndIndex" value="11" />
					<input type="hidden" name="txtMultiRowCopyStartIndex" value="8" />
					<input type="hidden" name="txtMultiRowCopyEndIndex" value="11" />
					<input type="hidden" name="txtMultiRowDataListMapKey" value="TABLE" />
				</form>
			</section>
			<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->


	<%@ include file="../index/footer.jsp"%>

	
</body>

</html>