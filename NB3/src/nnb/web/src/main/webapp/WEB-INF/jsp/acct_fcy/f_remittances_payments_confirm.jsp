<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
	<script type="text/javascript">
		// 初始化
		$(document).ready(function () {
			init();
		});

		function init() {
			//表單驗證
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
			// 確認鍵 click
			goOn();
			//倒數
			countDown();
			//交易類別change 事件
			changeFgtxway();

		}
		// 確認鍵 click
		function goOn() {
			$("#CMSUBMIT").click(function () {
				var back =  $('#back').val();
				if('Y'===back){
					$("#formId").submit();
				} else if (!$('#formId').validationEngine('validate')) {
					e = e || window.event; // for IE
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					initBlockUI(); //遮罩
					processQuery();
				}
			});
		}
		// 交易機制選項
		function processQuery() {
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
			switch (fgtxway) {
				case '0':
					$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
					$('#CMPASSWORD').val("");
					initBlockUI();//遮罩
					$("#formId").submit();
					break;
				// IKEY
				case '1':
					useIKey();
					break;

	           case '7'://IDGATE認證		 
	               idgatesubmit= $("#formId");		 
	               showIdgateBlock();		 
	               break;
				default:
					//<!-- 請選擇交易機制 -->
					//alert("<spring:message code= "LB.Alert001" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert001' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			}
		}
		//交易類別change 事件
		function changeFgtxway() {
			$('input[type=radio][name=FGTXWAY]').change(function () {
				console.log(this.value);
				if (this.value == '0') {
					$("#CMPASSWORD").addClass("validate[required]")
				} else if (this.value == '1') {
					$("#CMPASSWORD").removeClass("validate[required]");
				} else if (this.value == '7') {
					$("#CMPASSWORD").removeClass("validate[required]");
				}
			});
		}
		//網頁倒數計時(180秒)
		var sec = 60;
		var the_Timeout;
		function countDown() {
			var counter = document.getElementById("CountDown");
			counter.innerHTML = sec;
			sec--;
			if (sec == -1) {
				//<!-- 回編輯頁 -->
				$('#CMSUBMIT').val("<spring:message code="LB.X1291" />");
				$("#formId").removeAttr("target");
				$('#back').val("Y");
				$("#formId").validationEngine('detach');
				$("#formId").attr("action", "${__ctx}/FCY/ACCT/ONLINE/f_remittances_payments_step1");
				return false;
			}
			//網頁倒數計時(180秒)      	  	
			the_Timeout = setTimeout("countDown()", 1000);
		}
		clearTimeout(the_Timeout);
	</script>
</head>

<body>
	<!-- 交易機制所需畫面    -->
	<%@ include file="../component/trading_component.jsp"%>
    <!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %>  
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
	<!-- 匯入匯款     -->
    		<li class="ttb-breadcrumb-item"><spring:message code="LB.W0125" /></li>
    <!-- 線上解款     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0317" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!--外匯匯出匯款 -->
				<h2>
					<spring:message code="LB.W0317" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form autocomplete="off" method="post" id="formId" action="${__ctx}/FCY/ACCT/ONLINE/f_remittances_payments_result">
					<input type="hidden" id="ADOPID" name="ADOPID" value="F003">
					<input type="hidden" id="back" name="back" value="N">
					<input type="hidden" id="CMTRANPAGE" name="CMTRANPAGE" value="1">
					<input type="hidden" id="REMITAMT" name="REMITAMT" value="${f_remittances_payments_confirm.data.ATRAMT}">
					<%--  TXTOKEN  防止重送代碼--%>
					<input type="hidden" name="TXTOKEN" value="${f_remittances_payments_confirm.data.TXTOKEN}" />
					<%-- 驗證相關 --%>
					<input type="hidden" id="PINNEW" name="PINNEW" value="">
					<input type="hidden" id="jsondc" name="jsondc" value='${f_remittances_payments_confirm.data.jsondc}'>
					<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
					<!-- 				表單顯示區 -->
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<div class="ttb-message">
									<span>
										<spring:message code="LB.W0288" />
										<font id="CountDown" color="red"></font>
										<spring:message code="LB.Second" />
									</span>
								</div>
								<!--議價編號 -->
								<c:if test="${f_remittances_payments_confirm.data.SameCurrency == 'N'}">
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Bargaining_number" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${f_remittances_payments_confirm.data.BGROENO }
												</span>
											</div>
										</span>
									</div>
								</c:if>
								<!--匯率-->
								<c:if test="${f_remittances_payments_confirm.data.SameCurrency == 'N'}">
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Exchange_rate" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${f_remittances_payments_confirm.data.RATE }
												</span>
											</div>
										</span>
									</div>
								</c:if>
								<!--匯款金額-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.W0150" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${f_remittances_payments_confirm.data.TXCCY }
												<c:if test="${f_remittances_payments_confirm.data.SameCurrency == 'N'}">
													${f_remittances_payments_confirm.data.showCURAMT }
												</c:if>
												<c:if test="${f_remittances_payments_confirm.data.SameCurrency == 'Y'}">
													${f_remittances_payments_confirm.data.showTXAMT }
												</c:if>
											</span>
											<span class="ttb-unit">
												<spring:message code="LB.Dollar" /></span>
										</div>
									</span>
								</div>
								<!--解款帳號-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.W0329" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${f_remittances_payments_confirm.data.BENACC }
											</span>
										</div>
									</span>
								</div>
								<!--解款金額-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.W0330" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${f_remittances_payments_confirm.data.RETCCY }
												${f_remittances_payments_confirm.data.showTXAMT }
											</span>
											<span class="ttb-unit">
												<spring:message code="LB.Dollar" /></span>
										</div>
									</span>
								</div>
								<!-- 交易機制 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.Transaction_security_mechanism" />
										</label>
									</span>
									<span class="input-block">
										<!-- 交易密碼SSL -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.SSL_password" />
												<input type="radio" name="FGTXWAY" checked="checked" value="0">
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!--請輸入密碼 -->
										<div class="ttb-input">
											<spring:message code="LB.Please_enter_password" var="pleaseEnterPin" />
											<input type="password" id="CMPASSWORD" name="CMPASSWORD"
												class="text-input validate[required]" maxlength="8"
												placeholder="${plassEntpin}">
										</div>
										<!--電子簽章(請載入載具i-key) -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Electronic_signature" />
												<input type="radio" name="FGTXWAY" id="CMIKEY" value="1" />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input" name="idgate_group" style="display:none">		 
	                                       <label class="radio-block">裝置推播認證(請確認您的行動裝置網路連線是否正常，及推播功能是否已開啟)
		                                       <input type="radio" id="IDGATE" 	name="FGTXWAY" value="7"> 
		                                       <span class="ttb-radio"></span>
	                                       </label>		 
	                                   </div>
									</span>
								</div>
							</div>
							<!-- button -->
							
								<!-- 確定 -->
								<spring:message code="LB.Confirm" var="cmSubmit" />
								<input type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}"
									class="ttb-button btn-flat-orange">
							
						</div>
					</div>
					<div class="text-left">
						<ol class="description-list list-decimal">
							<li>
								<spring:message code="LB.Exchange_rate_Fail_note" />
							</li>
						</ol>
					</div>
				</form>
			</section>
			<!-- main-content END -->
		</main>
	</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>