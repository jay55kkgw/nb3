<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<script type="text/javascript" src="${__ctx}/js/jquery-3.3.1.js"></script>
<script type="text/javascript" src="${__ctx}/js/bootstrap-4.1.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/bootstrap-4.1.1.css">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/tbb_common.css">
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${transfer_record_query_FXCERT.data.FXTRANFUNC}</title>
<script type="text/javascript">
$(document).ready(function() {
	$("#CMPRINT").click(function(e){
		window.print();
	});
});
</script>
<style>
.ColorCell{
	background-color: lightgray;
}
</style>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
${transfer_record_query_FXCERT.data.FXCERT}
</body>

