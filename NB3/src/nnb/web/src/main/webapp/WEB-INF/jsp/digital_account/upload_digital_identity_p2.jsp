<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<meta http-equiv="Content-Type" content="multipart/form-data; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/heic2any.js"></script>
	<script type="text/javascript" src="${__ctx}/js/compressor.min.js"></script>
	
	<script type="text/javascript">
		var allowImageSize = 3300 * 1024;
		var width = $(window).width();
		$(window).resize(function() {
			width = $(this).width();
			changeDisplay();
		});
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		
		// 畫面初始化
		function init() {
			uploadSetting();
			$("#CMSUBMIT").click(function(e) {
				console.log("submit~~");
	
	         	initBlockUI();
	            $("#formId").submit();
			});
			$("#CMBACK").click( function(e) {
				console.log("submit~~");
    			$("#formId").attr("action", "${__ctx}/DIGITAL/ACCOUNT/upload_digital_identity_p1");
    			$("#back").val('Y');
	         	initBlockUI();
	            $("#formId").submit();
			});
			changeDisplay();
		}
		
		function changeDisplay(){
			<%-- 避免小版跑版 --%>
			if(width < 900){
				$("#bigBlock1").css("display","inline-block");
				$("#bigBlock2").css("display","inline-block");
// 				$("#hideBlock").hide();
			}
			else{
				$("#bigBlock1").css("display","flex");
				$("#bigBlock2").css("display","flex");
// 				$("#hideBlock").show();
			}
		}
		
		/**
		 * 讀取 文件的副檔名
		 * @param filename
		 */
		 function getFileExtension(filename) {
			  return filename.slice((filename.lastIndexOf('.') - 1 >>> 0) + 2).toLowerCase();
		}
		//上傳圖片
		function onUpLoad(fileFilter,imgFilter,type,postFile,orgImg) {
        	if(document.getElementById(type + "_error")){
        		$("#" + type + "_error").remove();
        	}
        	if($(postFile).val() != ""){
        		deleteUpLoad(imgFilter,postFile,orgImg,type);
        	}
        	
            var fileData = {};
            var file1 = $(fileFilter).get(0);
            fileData["type"] = type;
            fileData["oldPath"] = $(postFile).val();
            fileData["CUSIDN"] = '${result_data.data.CUSIDN}';
            if (file1.files.length == 0 ) {
				$(imgFilter).after("<p class='photo-error-message' id='" + type + "_error'><spring:message code='LB.Alert052' /></p>");
                return;
            }
            const reader = new FileReader();
            var file = file1.files[0];
         	initBlockUI();
        	try {
// 	            if (file.size < allowImageSize) {
		            if (this.getFileExtension(file.name) === 'heic' || this.getFileExtension(file.name) === 'heif') {
		            	console.log("cover to jepg");
		            	// 轉成 Blob
		                heic2any({
		                  blob: file,
		                  toType: 'image/jpeg',
		                  quality: 0.5
		                }).then(function(conversionResult){
		                    console.log('壓縮 中 .....');
							new Compressor(conversionResult, {
								quality: 0.6,
								success:function(result) {
				                    reader.readAsDataURL(result);
				                    reader.onload=function () {
					                    fileData["picFile"] = reader.result;
					        			console.log(fileData);
					                    onSubmitImg(fileData,imgFilter,type,postFile,orgImg);
				                    };
								},
								error:function(err) {
									console.log(err.message);
					        		onErrorImg(type,imgFilter,postFile,orgImg);
								},
							});
		                }).catch(function(e){
		                    console.log('getImgURL error ' + e);
		            		onErrorImg(type,imgFilter,postFile,orgImg);
		                });
		            }else{
	                    console.log('壓縮 中 .....');
						new Compressor(file, {
							quality: 0.6,
							success:function(result) {
				            	reader.readAsDataURL(result);
				                reader.onload=function () {
				                    fileData["picFile"] = reader.result;
				        			console.log(fileData);
				                    onSubmitImg(fileData,imgFilter,type,postFile,orgImg);
				                };
							},
							error:function(err) {
								console.log(err.message);
				        		onErrorImg(type,imgFilter,postFile,orgImg);
							},
						});
		            }
// 	            }else{
// 					$(imgFilter).after("<p class='photo-error-message' id='" + type + "_error'><spring:message code='LB.X1811' />" + allowImageSize / 1024 + "KB</p>");
// 	                $(imgFilter+"_close_btn").hide();
// 	                unBlockUI(initBlockId);
// 	                deleteUpLoad(imgFilter,postFile,orgImg,type);
// 	            }
        	}catch (e) {
        		console.log(e);
        		onErrorImg(type,imgFilter,postFile,orgImg);
                return;
        	}
        }
		//拖移上傳圖片
        function onUpLoadByDrop(file1,imgFilter,type,postFile,orgImg) {
			console.log("onUpLoadByDrop start");
        	if(document.getElementById(type + "_error")){
        		$("#" + type + "_error").remove();
        	}
        	if($(postFile).val() != ""){
        		deleteUpLoad(imgFilter,postFile,orgImg,type);
        	}

            var fileData = {};
            fileData["type"] = type;
            fileData["oldPath"] = $(postFile).val();
            fileData["CUSIDN"] = '${result_data.data.CUSIDN}';
            if (file1.length == 0 ) {
				$(imgFilter).after("<p class='photo-error-message' id='" + type + "_error'><spring:message code='LB.Alert052' /></p>");
                return;
            }
            const reader = new FileReader();
            var file = file1[0];
         	initBlockUI();
        	try {
// 	            if (file.size < allowImageSize) {
		            if (this.getFileExtension(file.name) === 'heic' || this.getFileExtension(file.name) === 'heif') {
		                console.log("cover to jepg");
		                heic2any({
		                  blob: file,
		                  toType: 'image/jpeg',
		                  quality: 0.5
		                }).then(function(conversionResult){
		                    console.log('壓縮 中 .....');
							new Compressor(conversionResult, {
								quality: 0.6,
								success:function(result) {
				                    reader.readAsDataURL(result);
				                    reader.onload=function () {
					                    fileData["picFile"] = reader.result;
					        			console.log(fileData);
					                    onSubmitImg(fileData,imgFilter,type,postFile,orgImg);
				                    };
								},
								error:function(err) {
									console.log(err.message);
					        		onErrorImg(type,imgFilter,postFile,orgImg);
								},
							});
		                }).catch(function(e){
		                    console.log('getImgURL error ' + e);
		            		onErrorImg(type,imgFilter,postFile,orgImg);
		                });
		            }else{
	                    console.log('壓縮 中 .....');
						new Compressor(file, {
							quality: 0.6,
							success:function(result) {
				            	reader.readAsDataURL(result);
				                reader.onload=function () {
				                    fileData["picFile"] = reader.result;
				        			console.log(fileData);
				                    onSubmitImg(fileData,imgFilter,type,postFile,orgImg);
				                };
							},
							error:function(err) {
								console.log(err.message);
				        		onErrorImg(type,imgFilter,postFile,orgImg);
							},
						});
		            }
// 	            }else{
// 					$(imgFilter).after("<p class='photo-error-message' id='" + type + "_error'><spring:message code='LB.X1811' />" + allowImageSize / 1024 + "KB</p>");
// 	                $(imgFilter+"_close_btn").hide();
// 	                unBlockUI(initBlockId);
// 	                deleteUpLoad(imgFilter,postFile,orgImg,type);
// 	            }
        	}catch (e) {
        		console.log(e);
        		onErrorImg(type,imgFilter,postFile,orgImg);
                return;
        	}
        }

        function onErrorImg(type,imgFilter,postFile,orgImg){
			$(imgFilter).after("<p class='photo-error-message' id='" + type + "_error'><spring:message code='LB.X2131' /></p>");
			unBlockUI(initBlockId);
            $(imgFilter+"_close_btn").hide();
            deleteUpLoad(imgFilter,postFile,orgImg,type);
        }
        function onSubmitImg(fileData,imgFilter,type,postFile,orgImg){
			console.log(fileData);
            $.ajax({
                url: "${__ctx}/ONLINE/APPLY/apply_deposit_account_fileupload_base64",
                type: "POST",
                data: fileData,
		        dataType: "json",
                success: function (res) {
                	console.log(res);
                    if(res.data.validated){
                    	initImage(res.data.url, imgFilter);
                        $(postFile).val(res.data.url);
                    } else {
						$(imgFilter).after("<p class='photo-error-message' id='" + type + "_error'>" + res.data.summary + "</p>");
                        $(imgFilter+"_close_btn").hide();
                        deleteUpLoad(imgFilter,postFile,orgImg,type);
                    }
					$(imgFilter).attr('src',res.data.picBase);
					unBlockUI(initBlockId);
                }
            });
        }
        
        function initImage(fileName, imgFilter){
            $.ajax({
                url: "${__ctx}/ONLINE/APPLY/apply_deposit_account_get_image",
                type: "POST",
                data: {
                	"filename": fileName
                },
		        dataType: "json",
                success: function (res) {
                	console.log(res);
                 if(res.data.validated){
 					 $(imgFilter).attr('src',res.data.picFile);
                     $(imgFilter+"_close_btn").show();
                 } else {
					$(imgFilter).after("<p class='photo-error-message' id='" + type + "_error'>" + res.data.summary + "</p>");
                     $(imgFilter+"_close_btn").hide();
                 }
                }
            });
        	
        }
        //移除上傳圖片
        function deleteUpLoad(imgFilter,postFile,orgImg, type){
            console.log($(postFile).val());
            $.ajax({
                url: "${__ctx}/ONLINE/APPLY/apply_deposit_account_fileupload_delete_base64",
                type: "POST",
                data: { 
                	Path:$(postFile).val()
		        },
                success: function (res) {
                	console.log(res);
                    if(res.data.sessulte){
                        $(imgFilter).attr('src',"${__ctx}/img/"+orgImg+".svg");
                        $(postFile).val("");
                        $(imgFilter+"_close_btn").hide();
                    } else {
//                     	alert(res.data.message);    
						$(imgFilter).after("<p class='photo-error-message' id='" + type + "_error'>" + res.data.summary + "</p>");
                    }
                }
            });
        	
        }
        
        function uploadSetting(){

			$('#img1').on("dragenter", function(e){
				console.log('dragenter');
				e = e || window.event;
		        e.preventDefault();
		        e.returnValue = false;
		     });
			$('#img1').on('dragover', function(e){
				console.log('dragover');
				e = e || window.event;
			    e.preventDefault();
			    e.returnValue = false;
			  })
			$('#img1').on('drop', function(e){
				console.log('drop');
				e = e || window.event;
				e.stopPropagation();
				e.preventDefault();
		        var file = e.originalEvent.dataTransfer.files;
				onUpLoadByDrop(file,'#img1', '01', '#FILE1', 'upload_id_front');
			})
			$('#img1').click(function(e){
				$("#file1").click();
			})
			$("#file1").change("change", function(e) {
				onUpLoad('#file1','#img1', '01', '#FILE1', 'upload_id_front');
			});
			if("${result_data.data.FILE1}" != ""){
				initImage("${result_data.data.FILE1}", '#img1');
                $("#img1_close_btn").show();
			}else{
                $("#img1_close_btn").hide();
			}

			$('#img2').on("dragenter", function(e){
				console.log('dragenter');
				e = e || window.event;
		        e.preventDefault();
		        e.returnValue = false;
		     });
			$('#img2').on('dragover', function(e){
				console.log('dragover');
				e = e || window.event;
			    e.preventDefault();
			    e.returnValue = false;
			  })
			$('#img2').on('drop', function(e){
				console.log('drop');
				e = e || window.event;
				e.stopPropagation();
				e.preventDefault();
		        var file = e.originalEvent.dataTransfer.files;
				onUpLoadByDrop(file,'#img2', '02', '#FILE2', 'upload_id_back');
			})
			$('#img2').click(function(e){
				$("#file2").click();
			})
			$("#file2").change("change", function(e) {
				onUpLoad('#file2','#img2', '02', '#FILE2', 'upload_id_back');
			});
			if("${result_data.data.FILE2}" != ""){
				initImage("${result_data.data.FILE2}", '#img2');
                $("#img2_close_btn").show();
			}else{
                $("#img2_close_btn").hide();
			}
			
			$('#img3').on("dragenter", function(e){
				console.log('dragenter');
				e = e || window.event;
		        e.preventDefault();
		        e.returnValue = false;
		     });
			$('#img3').on('dragover', function(e){
				console.log('dragover');
				e = e || window.event;
			    e.preventDefault();
			    e.returnValue = false;
			  })
			$('#img3').on('drop', function(e){
				console.log('drop');
				e = e || window.event;
				e.stopPropagation();
				e.preventDefault();
		        var file = e.originalEvent.dataTransfer.files;
				onUpLoadByDrop(file,'#img3', '03', '#FILE3', 'upload_sec_id_back_front');
			})
			$('#img3').click(function(e){
				$("#file3").click();
			})
			$("#file3").change("change", function(e) {
				onUpLoad('#file3','#img3', '03', '#FILE3', 'upload_sec_id_back_front');
			});
			if("${result_data.data.FILE3}" != ""){
				initImage("${result_data.data.FILE3}", '#img3');
                $("#img3_close_btn").show();
			}else{
                $("#img3_close_btn").hide();
			}
			
			$('#img4').on("dragenter", function(e){
				console.log('dragenter');
				e = e || window.event;
		        e.preventDefault();
		        e.returnValue = false;
		     });
			$('#img4').on('dragover', function(e){
				console.log('dragover');
				e = e || window.event;
			    e.preventDefault();
			    e.returnValue = false;
			  })
			$('#img4').on('drop', function(e){
				console.log('drop');
				e = e || window.event;
				e.stopPropagation();
				e.preventDefault();
		        var file = e.originalEvent.dataTransfer.files;
				onUpLoadByDrop(file,'#img4', '04', '#FILE4', 'upload_sec_id_back');
			})
			$('#img4').click(function(e){
				$("#file4").click();
			})
			$("#file4").change("change", function(e) {
				onUpLoad('#file4','#img4', '04', '#FILE4', 'upload_sec_id_back');
			});
			if("${result_data.data.FILE4}" != ""){
				initImage("${result_data.data.FILE4}", '#img4');
                $("#img4_close_btn").show();
			}else{
                $("#img4_close_btn").hide();
			}

        }
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	<!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<!--數位存款帳戶 -->
			<li class="ttb-breadcrumb-item"><spring:message code= "LB.X0348" /></a></li>
			<!--修改數位存款帳戶資料 -->
			<li class="ttb-breadcrumb-item"><a href="#">補上傳身分證件</a></li>
		</ol>
	</nav>
	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
				補上傳身分證件
				</h2>
				<div id="step-bar">
                    <ul>
                        <li class="finished">身分驗證</li>
                        <li class="active">上傳證件</li>
                        <li class="">確認資料</li>
                        <li class="">完成補件</li>
                    </ul>
                </div>
				<form method="post" id="formId" name="formId" action="${__ctx}/DIGITAL/ACCOUNT/upload_digital_identity_p3">
					
				    <input type="hidden" name="FILE1" id="FILE1" value="${result_data.data.FILE1}">
				    <input type="hidden" name="FILE2" id="FILE2" value="${result_data.data.FILE2}">
				    <input type="hidden" name="FILE3" id="FILE3" value="${result_data.data.FILE3}">
				    <input type="hidden" name="FILE4" id="FILE4" value="${result_data.data.FILE4}">
					<input type="hidden" name="CUSIDN" id="CUSIDN" value="${result_data.data.CUSIDN}">
					<input type="hidden" id="back" name="back" value="">
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-input-block">
							
								<div class="ttb-message">
                                <p>上傳證件</p>
                            	</div>
                            	<p class="form-description">請上傳您的證件。</p>
	                            <div class="classification-block">
	                                <p>身分證資料</p>
	                            </div>
	                            <p class="form-description">請上傳您的身分證正反面。<br />注意事項：<br />- 圖片格式僅允許：JPG、PNG、BMP、HEIC (不支援 IE)</p>
							
							<div class="photo-block" id="bigBlock1">
	                                <div>
	                                	<img src="${__ctx}/img/upload_id_front.svg" id="img1" />
	                                    <a id="img1_close_btn" class="photo-close-btn" onClick="deleteUpLoad('#img1', '#FILE1', 'upload_id_front', '01')"></a>
										<!-- 身分證正面圖片 -->
	                                    <p><spring:message code="LB.D0111"/></p>
	                                </div>
	                                <div>
										<!-- 身分證反面圖片 -->
	                                	<img src="${__ctx}/img/upload_id_back.svg" id="img2" />
	                                    <a id="img2_close_btn" class="photo-close-btn" onClick="deleteUpLoad('#img2', '#FILE2', 'upload_id_back', '02')"></a>
	                                    <p>身分證背面圖片</p>
	                                </div>
									<input type="file" name="file1" id="file1" accept="image/jpeg,image/jpg,image/pjpeg, image/png, image/heic, image/heif, image/bmp"
										style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
									<input type="file" name="file2" id="file2" accept="image/jpeg,image/jpg,image/pjpeg, image/png, image/heic, image/heif, image/bmp"
										style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
	                            </div>
	                            <div class="classification-block">
	                                <p>第二證件資料</p>
	                            </div>
	                            <p class="form-description">請上傳您的第二證件資料正反面。<br />注意事項：<br />- 圖片格式僅允許：JPG、PNG、BMP、HEIC (不支援 IE)</p>
	
	                            <div class="photo-block" id="bigBlock2">
	                                <div>
	                                	<img src="${__ctx}/img/upload_sec_id_back_front.svg" id="img3" />
	                                    <a id="img3_close_btn" class="photo-close-btn" onClick="deleteUpLoad('#img3', '#FILE3', 'upload_sec_id_back_front', '03')"></a>
										<!--第二證件正面圖片 -->
	                                    <p><spring:message code="LB.D1159"/></p>
	                                </div>
									<div>
	                                	<img src="${__ctx}/img/upload_sec_id_back.svg"  id="img4"/>
	                                    <a id="img4_close_btn" class="photo-close-btn" onClick="deleteUpLoad('#img4', '#FILE4', 'upload_sec_id_back', '04')"></a>
										<!--第二證件反面圖片 -->
	                                    <p>第二證件背面圖片</p>
	                                </div>
									<input type="file" name="file3" id="file3" accept="image/jpeg,image/jpg,image/pjpeg, image/png, image/heic, image/heif, image/bmp"
										style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
									<input type="file" name="file4" id="file4" accept="image/jpeg,image/jpg,image/pjpeg, image/png, image/heic, image/heif, image/bmp"
										style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
	                            </div>
							</div>
							
<!-- 							<div class="text-left" style="width: 80%;margin-left: 10%;"> -->
<%-- 								<b><spring:message code="LB.Note"/>:<spring:message code="LB.D1162"/></b> --%>
<!-- 								<ol class="list-decimal text-left" style="width: 90%;margin-left: 5%;"> -->
<%-- 									<li><spring:message code="LB.Apply_Digital_Account_P5_D1"/></li> --%>
<%-- 									<li><spring:message code="LB.Apply_Digital_Account_P5_D2"/></li> --%>
<%-- 									<li><spring:message code="LB.Apply_Digital_Account_P5_D3"/></li> --%>
<%-- 									<li><spring:message code="LB.Apply_Digital_Account_P5_D1"/></li> --%>
<!-- 								</ol> -->
<!-- 							</div> -->
							<!-- 確定 -->
							<input type="button" id="CMSUBMIT" value="<spring:message code="LB.X0080" />" class="ttb-button btn-flat-orange"/>
						</div>
					</div>
						
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>