<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript" src="${__ctx}/js/checkutil.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()",20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
		//initFootable();
		//若前頁有帶acn，轉出預設為此acn
		var getacn = '${averaging_alter.data.alert_ACN}';
		if(getacn != null && getacn != ''){
			$("#ACN option[value= '"+ getacn +"' ]").prop("selected", true);
			displayAmt();
		}
		$("#formId").validationEngine({
			binded: false,
			promptPosition: "inline"
		});		
		$("#hideblock_CA").hide();
		$("#hideblock_C06").hide();
		$("#hideblock_R06").hide();
		$("#hideblock_S06").hide();
		$("#hideblock_C16").hide();
		$("#hideblock_R16").hide();
		$("#hideblock_S16").hide();
		$("#hideblock_C26").hide();
		$("#hideblock_R26").hide();
		$("#hideblock_S26").hide();
		$("#CMSUBMIT").click( function(e) {
			$("#hideblock_CA").show();
			if($("#DATE_06").prop("checked")){
				$("#hideblock_C06").show();
				$("#hideblock_R06").show();
				$("#hideblock_S06").show();
			}
			else{
				$("#hideblock_C06").hide();
				$("#hideblock_R06").hide();
				$("#hideblock_S06").hide();
			}
			if($("#DATE_16").prop("checked")){
				$("#hideblock_C16").show();
				$("#hideblock_R16").show();
				$("#hideblock_S16").show();
			}
			else{
				$("#hideblock_C16").hide();
				$("#hideblock_R16").hide();
				$("#hideblock_S16").hide();
			}
			if($("#DATE_26").prop("checked")){
				$("#hideblock_C26").show();
				$("#hideblock_R26").show();
				$("#hideblock_S26").show();
			}
			else{
				$("#hideblock_C26").hide();
				$("#hideblock_R26").hide();
				$("#hideblock_S26").hide();
			}
			
			e = e || window.event;
			if(!$('#formId').validationEngine('validate')){
        		e.preventDefault();
        	}
			else{
				submit(); 
			}
		});
		// 確認鍵 click
// 		submit();
	}
	
	// 確認鍵 Click
	function submit() {
			console.log("submit~~");
			// 遮罩
// 			if(!processQuery()){
//          		return false;
//          	}
         	initBlockUI();
         	var main = document.getElementById("formId");
         	main.AMT_06_N.disabled = false;   	    
           	main.AMT_16_N.disabled = false;   	    
           	main.AMT_26_N.disabled = false;
            $("#formId").attr("action","${__ctx}/GOLD/AVERAGING/averaging_alter_confirm");
	 	  	$("#formId").submit();
	}
	
	//取得資料onchange
	function displayAmt() {
		uri = '${__ctx}' + "/GOLD/AVERAGING/get_data_ajax";
	    if ($("#ACN").val() != "#") {
			rdata = {
				ACN : $("#ACN").val()
			};
			
			console.log("creatOutAcn.uri: " + uri);
			console.log("creatOutAcn.rdata: " + rdata);
			
			data = fstop.getServerDataEx(uri, rdata, false, callback);
	    }	
	}
	
	
	function callback(data) {
		console.log("data: " + data);
		if (data) {
			//ajax回傳資料型態: List<Map<String, String>>
			console.log("data.json: " + JSON.stringify(data));
			if (data[0].MSGCOD != undefined) {
				$("#formId").attr("action","${__ctx}/GOLD/AVERAGING/g_error");
		 	  	$("#formId").submit();
		 	  	return;
			}

			if (data[0].SVACN == '') {
				//alert(data[0].SVACN);
				errorBlock(
						null, 
						null,
						[data[0].SVACN], 
						'<spring:message code= "LB.Quit" />', 
						null
				);
// 				location.href = "simpleform?trancode=GD_ApplyConfirm&ADOPID=N09302&MSGTYPE=N093";
				$("#formId").attr("action","${__ctx}/GOLD/AVERAGING/g_error");
		 	  	$("#formId").submit();
				return;
			}

			data.forEach(function(map) {
				console.log(map);
				var AMT_06 = document.getElementById("AMT_06");
				var AMT_16 = document.getElementById("AMT_16");
				var AMT_26 = document.getElementById("AMT_26");
				var FLAG_06 = document.getElementById("FLAG_06");
				var FLAG_16 = document.getElementById("FLAG_16");
				var FLAG_26 = document.getElementById("FLAG_26");
				var SVACN_Display = document.getElementById("SVACN_Display");

				AMT_06.innerHTML = map.AMT_06_DESC;
				AMT_16.innerHTML = map.AMT_16_DESC;
				AMT_26.innerHTML = map.AMT_26_DESC;
				FLAG_06.innerHTML = map.FLAG_06_DESC;
				FLAG_16.innerHTML = map.FLAG_16_DESC;
				FLAG_26.innerHTML = map.FLAG_26_DESC;
				SVACN_Display.innerHTML = map.SVACN;

				var main = document.getElementById("formId");

				document.getElementById("AMT_06_O").value = map.AMT_06;
				document.getElementById("AMT_16_O").value = map.AMT_16;
				document.getElementById("AMT_26_O").value = map.AMT_26;
				// 			    	main.AMT_06_O.value = map.AMT_06;
				// 			    	main.AMT_16_O.value = map.AMT_16;
				// 			    	main.AMT_26_O.value = map.AMT_26;   
				main.FLAG_06_O.value = map.FLAG_06;
				main.FLAG_16_O.value = map.FLAG_16;
				main.FLAG_26_O.value = map.FLAG_26;
				main.CHA_06.value = map.CHA_06;
				main.CHA_16.value = map.CHA_16;
				main.CHA_26.value = map.CHA_26;
				main.SVACN.value = map.SVACN;
				// 			    	alert(main.AMT_06_O.value);
				// 			    	alert(document.getElementById("AMT_06_O").value);
				if (map.CHA_06 == 'Y')
					main.DATE_06.disabled = false;
				else {
					main.DATE_06.disabled = true;
					main.AMT_06_N.disabled = true;
					main.PAYSTATUS_06[0].disabled = true;
					main.PAYSTATUS_06[1].disabled = true;
					main.PAYSTATUS_06[2].disabled = true;
				}

				if (map.CHA_16 == 'Y')
					main.DATE_16.disabled = false;
				else {
					main.DATE_16.disabled = true;
					main.AMT_16_N.disabled = true;
					main.PAYSTATUS_16[0].disabled = true;
					main.PAYSTATUS_16[1].disabled = true;
					main.PAYSTATUS_16[2].disabled = true;
				}

				if (map.CHA_26 == 'Y')
					main.DATE_26.disabled = false;
				else {
					main.DATE_26.disabled = true;
					main.AMT_26_N.disabled = true;
					main.PAYSTATUS_26[0].disabled = true;
					main.PAYSTATUS_26[1].disabled = true;
					main.PAYSTATUS_26[2].disabled = true;
				}
			});

			/*** 清空 [變更後每月投資日/金額] 所有欄位 ***/
			clearNewFields();

			/*** 顯示[黃金存摺帳號]以下畫面欄位 ***/
			var row1 = document.getElementById("test1");
			var CMSUBMIT = document.getElementById("CMSUBMIT");
			var CMRESET = document.getElementById("CMRESET");

			row1.style.display = "block";
			CMRESET.style.display = "inline";
			CMSUBMIT.style.display = "inline";
		}
	}

	function clearNewFields() {

		var main = document.getElementById("formId");

		main.DATE_06.checked = false;
		main.AMT_06_N.value = "";
		main.PAYSTATUS_06[0].checked = false;
		main.PAYSTATUS_06[1].checked = false;
		main.PAYSTATUS_06[2].checked = false;
		main.AMT_06_N.disabled = false;

		main.DATE_16.checked = false;
		main.AMT_16_N.value = "";
		main.PAYSTATUS_16[0].checked = false;
		main.PAYSTATUS_16[1].checked = false;
		main.PAYSTATUS_16[2].checked = false;
		main.AMT_16_N.disabled = false;

		main.DATE_26.checked = false;
		main.AMT_26_N.value = "";
		main.PAYSTATUS_26[0].checked = false;
		main.PAYSTATUS_26[1].checked = false;
		main.PAYSTATUS_26[2].checked = false;
		main.AMT_26_N.disabled = false;
	}

	function displayInfo(obj) {

		var main = document.getElementById("formId");

		if (obj.checked) {
			if (obj.name == 'DATE_06') {
				main.AMT_06_N.value = main.AMT_06_O.value;

				if (main.FLAG_06_O.value != '')
					main.PAYSTATUS_06[main.FLAG_06_O.value - 1].checked = true;

				if ((main.FLAG_06_O.value == '2')
						|| (main.FLAG_06_O.value == '3'))
					main.AMT_06_N.disabled = true;
			} else if (obj.name == 'DATE_16') {
				main.AMT_16_N.value = main.AMT_16_O.value;

				if (main.FLAG_16_O.value != '')
					main.PAYSTATUS_16[main.FLAG_16_O.value - 1].checked = true;

				if ((main.FLAG_16_O.value == '2')
						|| (main.FLAG_16_O.value == '3'))
					main.AMT_16_N.disabled = true;
			} else if (obj.name == 'DATE_26') {
				main.AMT_26_N.value = main.AMT_26_O.value;

				if (main.FLAG_26_O.value != '')
					main.PAYSTATUS_26[main.FLAG_26_O.value - 1].checked = true;

				if ((main.FLAG_26_O.value == '2')
						|| (main.FLAG_26_O.value == '3'))
					main.AMT_26_N.disabled = true;
			}
		} else {
			if (obj.name == 'DATE_06') {
				main.AMT_06_N.disabled = false;
				main.AMT_06_N.value = "";
				main.PAYSTATUS_06[0].checked = false;
				main.PAYSTATUS_06[1].checked = false;
				main.PAYSTATUS_06[2].checked = false;
			} else if (obj.name == 'DATE_16') {
				main.AMT_16_N.disabled = false;
				main.AMT_16_N.value = "";
				main.PAYSTATUS_16[0].checked = false;
				main.PAYSTATUS_16[1].checked = false;
				main.PAYSTATUS_16[2].checked = false;
			} else if (obj.name == 'DATE_26') {
				main.AMT_26_N.disabled = false;
				main.AMT_26_N.value = "";
				main.PAYSTATUS_26[0].checked = false;
				main.PAYSTATUS_26[1].checked = false;
				main.PAYSTATUS_26[2].checked = false;
			}
		}
	}

	function displayInfo2(obj) {

		var main = document.getElementById("formId");
		if ((obj.name == "PAYSTATUS_06") && (!main.DATE_06.checked)) {
			main.DATE_06.checked = true;
			main.AMT_06_N.value = main.AMT_06_O.value;
		} else if ((obj.name == "PAYSTATUS_16") && (!main.DATE_16.checked)) {
			main.DATE_16.checked = true;
			main.AMT_16_N.value = main.AMT_16_O.value;
		} else if ((obj.name == "PAYSTATUS_26") && (!main.DATE_26.checked)) {
			main.DATE_26.checked = true;
			main.AMT_26_N.value = main.AMT_26_O.value;
		}

		if ((obj.value == '2') || (obj.value == '3')) {
			if (obj.name == "PAYSTATUS_06") {
				main.AMT_06_N.disabled = true;
				main.AMT_06_N.value = main.AMT_06_O.value;
			} else if (obj.name == "PAYSTATUS_16") {
				main.AMT_16_N.disabled = true;
				main.AMT_16_N.value = main.AMT_16_O.value;
			} else if (obj.name == "PAYSTATUS_26") {
				main.AMT_26_N.disabled = true;
				main.AMT_26_N.value = main.AMT_26_O.value;
			}
		} else {
			if (obj.name == "PAYSTATUS_06")
				main.AMT_06_N.disabled = false;
			else if (obj.name == "PAYSTATUS_16")
				main.AMT_16_N.disabled = false;
			else if (obj.name == "PAYSTATUS_26")
				main.AMT_26_N.disabled = false;
		}
	}

	function displayInfo1(obj) {

		var main = document.getElementById("formId");

		if ((obj.name == "AMT_06_N") && (obj.value == "")) {
			main.DATE_06.checked = true;
			main.AMT_06_N.value = main.AMT_06_O.value;

			if (main.FLAG_06_O.value != '')
				main.PAYSTATUS_06[main.FLAG_06_O.value - 1].checked = true;

			if ((main.FLAG_06_O.value == '2') || (main.FLAG_06_O.value == '3'))
				main.AMT_06_N.disabled = true;
		} else if ((obj.name == "AMT_16_N") && (obj.value == "")) {
			main.DATE_16.checked = true;
			main.AMT_16_N.value = main.AMT_16_O.value;

			if (main.FLAG_16_O.value != '')
				main.PAYSTATUS_16[main.FLAG_16_O.value - 1].checked = true;

			if ((main.FLAG_16_O.value == '2') || (main.FLAG_16_O.value == '3'))
				main.AMT_16_N.disabled = true;
		} else if ((obj.name == "AMT_26_N") && (obj.value == "")) {
			main.DATE_26.checked = true;
			main.AMT_26_N.value = main.AMT_26_O.value;

			if (main.FLAG_26_O.value != '')
				main.PAYSTATUS_26[main.FLAG_26_O.value - 1].checked = true;

			if ((main.FLAG_26_O.value == '2') || (main.FLAG_26_O.value == '3'))
				main.AMT_26_N.disabled = true;
		}
	}

</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 定期定額交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1552" /></li>
    <!-- 定期定額變更     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1564" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<!-- 黃金定期定額變更 -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.W1565"/></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form id="formId" method="post" action="">
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
                            
                             <!--黃金存摺帳號-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.D1090"/></h4>
                                    </label></span>
                                <span class="input-block">
                                	<div class="ttb-input">
                                        <select class="custom-select select-input half-input validate[funcCall[validate_CheckSelect['<spring:message code= "LB.D1090" />',ACN,#]]]" name="ACN" id="ACN" onchange="displayAmt()">
                                           	<!-- 請選擇帳號 -->
                                           	<option value="#">---<spring:message code="LB.W0257"/>---</option>
                                        	<c:forEach var="dataList" items="${averaging_alter.data.ACN}">
												<option value="${dataList.ACN}"> ${dataList.ACN}</option>
											</c:forEach>
                                        </select>
                                	</div>
                                </span>
                            </div>
							<div id="test1" style="display:none">
                             <!--約定扣款帳號-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.W1539"/></h4>
                                    </label></span>
                                <span class="input-block">
									<div class="ttb-input">
                                       <span><font id="SVACN_Display"></font></span>
                                     </div>
                                </span>
                            </div>
                              
							 <!--原每月投資日/金額-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.W1568"/><br><spring:message code="LB.W1569"/></h4>
                                    </label></span>
                                <span class="input-block">
									<div class="ttb-input">
										<span>06<spring:message code="LB.Day"/> <spring:message code="LB.NTD" />&nbsp;<font id="AMT_06"></font>&nbsp;<spring:message code="LB.Dollar"/> &nbsp;<font id="FLAG_06"></font></span>
                                     </div>
									<div class="ttb-input">
										<span>16<spring:message code="LB.Day"/> <spring:message code="LB.NTD" />&nbsp;<font id="AMT_16"></font>&nbsp;<spring:message code="LB.Dollar"/> &nbsp;<font id="FLAG_16"></font></span>
                                     </div>
									<div class="ttb-input">
										<span>26<spring:message code="LB.Day"/> <spring:message code="LB.NTD" />&nbsp;<font id="AMT_26"></font>&nbsp;<spring:message code="LB.Dollar"/> &nbsp;<font id="FLAG_26"></font></span>
                                     </div>
                                </span>
                            </div>
							
							 <!--變更後每月投資日/金額-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.W1572"/><br><spring:message code="LB.W1569"/></h4>
                                    </label></span>
                                <span class="input-block">
                                <span id="hideblock_CA"> 
									<!-- 驗證用的input 一筆資料--> 
									<input id="checkAll" name="checkAll" type="text"
										class="text-input validate[funcCallRequired[validate_chkClickboxKind[<spring:message code= "LB.X1199" />,3,DATE_06,DATE_16,DATE_26]]]"
										style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;" />
								</span>
									<div class="ttb-input">
										<label class="check-block" for="DATE_06"><input value="1" name="DATE_06" id="DATE_06" type="checkbox" onclick="displayInfo(this)">06<spring:message code="LB.Day"/> 
										<span class="ttb-check"></span>
											<spring:message code="LB.NTD" />&nbsp; 
										</label><input type="text" class="text-input" maxLength="11" size="11" value="" id="AMT_06_N" name="AMT_06_N" onclick="displayInfo1(this)">&nbsp;<spring:message code="LB.Dollar"/>  
										<span id="hideblock_C06"> 
											<!-- 驗證用的input --> 
											<input id="check06" name="check06" type="text"
												class="text-input validate[funcCallRequired[validate_Check_Amount['06<spring:message code= "LB.Day" />' , AMT_06_N, 3000, null,1000]]]"
												style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;" />
										</span>
									</div>	
									<div class="ttb-input">
									<!-- 正常扣款 -->
										<label class="radio-block"><spring:message code="LB.W1573"/> 
                                            <input type="radio" name="PAYSTATUS_06" value='1' onclick="displayInfo2(this)">
												 <span class="ttb-radio"></span>
                                        </label>
                                        <!-- 暫停扣款 -->
										<label class="radio-block"><spring:message code="LB.W1163"/> 
                                            <input type="radio" name="PAYSTATUS_06" value='2' onclick="displayInfo2(this)">
												 <span class="ttb-radio"></span>
                                        </label>
                                        <!-- 終止扣款 -->
										<label class="radio-block"><spring:message code="LB.W1164"/> 	
                                            <input type="radio" name="PAYSTATUS_06" value='3' onclick="displayInfo2(this)">
												 <span class="ttb-radio"></span>
                                        </label>
                                        <span id="hideblock_R06"> 
											<!-- 驗證用的input --> 
											<input id="checkR06" name="checkR06" type="text"
												class="text-input validate[funcCallRequired[validate_Radio[06<spring:message code= "LB.X1200" />,PAYSTATUS_06]]]"
												style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;" />
										</span>
                                        <span id="hideblock_S06"> 
											<!-- 驗證用的input --> 
											<input id="checkS06" name="checkS06" type="text"
												class="text-input validate[funcCallRequired[validate_chkStatus[06<spring:message code= "LB.Day" />,FLAG_06_O,PAYSTATUS_06,1,2]]]"
												style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;" />
										</span>
									</div>
									<div class="ttb-input">
										<label class="check-block" for="DATE_16"><input value="1" name="DATE_16" id="DATE_16" type="checkbox" onclick="displayInfo(this)">16<spring:message code="LB.Day"/> 
										<span class="ttb-check"></span>
											<spring:message code="LB.NTD" />&nbsp; 
										</label><input type="text" class="text-input" maxLength="11" size="11" value="" id="AMT_16_N" name="AMT_16_N" onclick="displayInfo1(this)">&nbsp;<spring:message code="LB.Dollar"/> 
										<span id="hideblock_C16"> 
											<!-- 驗證用的input --> 
											<input id="check16" name="check16" type="text"
												class="text-input validate[funcCallRequired[validate_Check_Amount['16<spring:message code= "LB.Day" />' , AMT_16_N, 3000, null,1000]]]"
												style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;" />
										</span>
									</div>
									<div class="ttb-input">
									<!-- 正常扣款 -->
										<label class="radio-block"><spring:message code="LB.W1573"/> 
                                            <input type="radio" name="PAYSTATUS_16" value='1' onclick="displayInfo2(this)">
												 <span class="ttb-radio"></span>
                                        </label>
                                        <!--  暫停扣款 -->
										<label class="radio-block"><spring:message code="LB.W1163"/> 
                                            <input type="radio" name="PAYSTATUS_16" value='2' onclick="displayInfo2(this)">
												 <span class="ttb-radio"></span>
                                        </label>
                                        <!-- 終止扣款 -->
										<label class="radio-block"><spring:message code="LB.W1164"/> 	
                                            <input type="radio" name="PAYSTATUS_16" value='3' onclick="displayInfo2(this)">
												 <span class="ttb-radio"></span>
                                        </label>
                                        <span id="hideblock_R16"> 
											<!-- 驗證用的input --> 
											<input id="checkR16" name="checkR16" type="text"
												class="text-input validate[funcCallRequired[validate_Radio[16<spring:message code= "LB.X1200" />,PAYSTATUS_16]]]"
												style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;" />
										</span>
										<span id="hideblock_S16"> 
											<!-- 驗證用的input --> 
											<input id="checkS16" name="checkS16" type="text"
												class="text-input validate[funcCallRequired[validate_chkStatus[16<spring:message code= "LB.Day" />,FLAG_16_O,PAYSTATUS_16,1,2]]]"
												style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;" />
										</span>
									</div>
									
									<div class="ttb-input">
										<label class="check-block" for="DATE_26"><input value="1" name="DATE_26" id="DATE_26" type="checkbox" onclick="displayInfo(this)">26<spring:message code="LB.Day"/> 
										<span class="ttb-check"></span>
											<spring:message code="LB.NTD" />&nbsp; 
										</label><input type="text" class="text-input" maxLength="11" size="11" value="" id="AMT_26_N" name="AMT_26_N" onclick="displayInfo1(this)">&nbsp;<spring:message code="LB.Dollar"/> 
										<span id="hideblock_C26"> 
											<!-- 驗證用的input --> 
											<input id="check26" name="check26" type="text"
												class="text-input validate[funcCallRequired[validate_Check_Amount['26<spring:message code= "LB.Day" />' , AMT_26_N, 3000, null,1000]]]"
												style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;" />
										</span>
									</div>
									<div class="ttb-input">
									<!--  正常扣款 -->
										<label class="radio-block"><spring:message code="LB.W1573"/> 
                                            <input type="radio" name="PAYSTATUS_26" value='1' onclick="displayInfo2(this)">
												 <span class="ttb-radio"></span>
                                        </label>
                                        <!-- 暫停扣款 -->
										<label class="radio-block"><spring:message code="LB.W1163"/> 
                                            <input type="radio" name="PAYSTATUS_26" value='2' onclick="displayInfo2(this)">
												 <span class="ttb-radio"></span>
                                        </label>
                                        <!-- 終止扣款 -->
										<label class="radio-block"><spring:message code="LB.W1164"/> 	
                                            <input type="radio" name="PAYSTATUS_26" value='3' onclick="displayInfo2(this)">
												 <span class="ttb-radio"></span>
                                        </label>
                                        <span id="hideblock_R26"> 
											<!-- 驗證用的input --> 
											<input id="checkR26" name="checkR26" type="text"
												class="text-input validate[funcCallRequired[validate_Radio[26<spring:message code= "LB.X1200" />,PAYSTATUS_26]]]"
												style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;" />
										</span>
										<span id="hideblock_S26"> 
											<!-- 驗證用的input --> 
											<input id="checkS26" name="checkS26" type="text"
												class="text-input validate[funcCallRequired[validate_chkStatus[26<spring:message code= "LB.Day" />,FLAG_26_O,PAYSTATUS_26,1,2]]]"
												style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;" />
										</span>
									</div>
									
                                </span>
                            </div>
							</div>
                        </div>
                        <input class="ttb-button btn-flat-gray" id="CMRESET" name="CMRESET" type="reset" value="<spring:message code="LB.Re_enter" />" style="display:none"/>
                        <input class="ttb-button btn-flat-orange" id="CMSUBMIT" name="CMSUBMIT" type="button" value="<spring:message code="LB.Confirm" />" style="display:none"/>
                        <input type="hidden" id="action" name="action" value="forward">
						<input type="hidden" id="urlPath" name="urlPath" value="">
						<input type="hidden" id="ADOPID" name="ADOPID" value="N09302">
						<input type="hidden" id="FGTXWAY" name="FGTXWAY" value="0">	
						<input type="hidden" id="DPMYEMAIL" name="DPMYEMAIL" value="${sessionScope.dpmyemail}">				
						<input type="hidden" id="AMT_06_O" name="AMT_06_O" value="">	
						<input type="hidden" id="AMT_16_O" name="AMT_16_O" value="">
						<input type="hidden" id="AMT_26_O" name="AMT_26_O" value="">
						<input type="hidden" id="FLAG_06_O" name="FLAG_06_O" value="">	
						<input type="hidden" id="FLAG_16_O" name="FLAG_16_O" value="">
						<input type="hidden" id="FLAG_26_O" name="FLAG_26_O" value="">
						<input type="hidden" id="CHA_06" name="CHA_06" value="">	
						<input type="hidden" id="CHA_16" name="CHA_16" value="">
						<input type="hidden" id="CHA_26" name="CHA_26" value="">
						<input type="hidden" id="SVACN" name="SVACN" value="">	
                    </div>
                </div>
				</form>
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>
					<!-- 投資日上午凌晨0〜10點(遇假日則自原投資日起至次一營業日上午凌晨0〜10點)，因系統進行批次扣款作業，暫時停止變更服務。 -->
					<li><span><spring:message code="LB.Averaging_Alter_P1_D1" /></span></li>
		 			<!-- 每次投資金額至少為新臺幣3,000元，且得以新臺幣1,000元之整倍數增加。  -->
		 			<li><span><spring:message code="LB.Averaging_Alter_P1_D2" /></span></li>
		 			<!-- 網路銀行定期定額每次扣款優惠手續費為新臺幣50元，連同投資金額一併扣繳。 -->
					<li><span><spring:message code="LB.Averaging_Alter_P1_D3" /></span></li>
					<!-- 黃金存摺不支付利息，黃金價格有漲有跌，投資時可能產生收益或損失，請慎選買賣時機，並承擔風險。 -->
					<li><span><spring:message code="LB.Averaging_Alter_P1_D4" /></span></li>
                </ol>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>