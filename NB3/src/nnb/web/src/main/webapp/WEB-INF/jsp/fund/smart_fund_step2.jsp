<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript">
		// 初始化
		$(document).ready(function () {
			init();
		});

		function init() {
			//表單驗證
			$("#formId").validationEngine({binded: false,promptPosition: "inline"});
			// 確認鍵 click
			goOn();
			// 回上一頁 click
			back();
			//重新整理
			cmrest();
		}

		//上一頁按鈕
		function back() {
			$("#CMBACK").click(function () {
				var action = '${__ctx}/FUND/ALTER/smart_fund';
				$('#back').val("Y");
				$("#formId").validationEngine('detach');
				$("#formId").attr("action", action);
				$("#formId").submit();
			});
		}
		
		//重新整理
		function cmrest() {
			$("#CMRESET").click(function () {
				$("#formId")[0].reset();
			});
		}
		// 確認鍵 click
		function goOn() {
			$("#CMSUBMIT").click(function (e) {
				if(!processQuery()){
				}else{
					//
					if (!$('#formId').validationEngine('validate')) {
						e = e || window.event; // for IE
						e.preventDefault();
					} else {
						$("#formId").validationEngine('detach');
						initBlockUI(); //遮罩
						$("#formId").submit();
					}
				}
			});
		}
		//自動贖回出場點設定
		function clearset(flag) {
			if (flag == '1')
				$("#NSTOPPROF2").val("");
			if (flag == '2')
				$("#NSTOPLOSS2").val("");
		}
		//交易檢核
		function processQuery() {
			//
			var noopt1 = $("input[name=NOOPT1]:checked").val();
			if(typeof(noopt1)=="undefined"){
				//alert("<spring:message code="LB.Alert125" />");
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert125' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
				);
	   			return false;
			}
			
			$("#NOOPT").val(noopt1);
			//檢核切換
			if ($("input[name=NOOPT1]:checked").val() == '') {
				$("#validate_NSTOPPROF1").addClass(
					"validate[funcCall[validate_Radio[<spring:message code= "LB.W1210" /> <spring:message code= "LB.W1211" />,NSTOPPROF1]]]");
				$("#validate_NSTOPLOSS1").addClass(
					"validate[funcCall[validate_Radio[<spring:message code= "LB.W1210" /> <spring:message code= "LB.X0419" />,NSTOPLOSS1]]]");
			} else {
				$("#validate_NSTOPPROF1").removeClass(
					"validate[funcCall[validate_Radio[<spring:message code= "LB.W1210" /> <spring:message code= "LB.W1211" />,NSTOPPROF1]]]");
				$("#validate_NSTOPLOSS1").removeClass(
					"validate[funcCall[validate_Radio[<spring:message code= "LB.W1210" /> <spring:message code= "LB.X0419" />,NSTOPLOSS1]]]");
			}
			//塞值
			var radios = document.getElementsByName("NSTOPPROF1");
			var radios1 = document.getElementsByName("NSTOPLOSS1");
			var counter = 0;
			var counter1 = 0;
			for (var i = 0; i < radios.length; i++) {
				if (radios[i].checked) {
					if (!radios[5].checked)
						$("#NSTOPPROF").val(radios[i].value);
					counter++;
				}
			}
			for (var j = 0; j < radios1.length; j++) {
				if (radios1[j].checked) {
					if (!radios1[5].checked)
						$("#NSTOPLOSS").val(radios[j].value);
					counter1++;
				}
			}
	
			//變更後停利設定
			var nstopprof1 = $("input[name=NSTOPPROF1]:checked").val();
			console.log("nstopprof1: " + nstopprof1);
			//變更後停損設定
			var nstoploss1 = $('input[name=NSTOPLOSS1]:checked').val();
			console.log("nstoploss1: " + nstoploss1);
			//變更後停利設定 20200605 改為停利停損都要填
// 			if(typeof(nstopprof1)=="undefined"){
				
// 			}else{
// 				$("#validate_NSTOPLOSS1").removeClass(
// 				"validate[funcCall[validate_Radio[<spring:message code= "LB.W1210" /> <spring:message code= "LB.X0419" />,NSTOPLOSS1]]]");
// 			}
// 			//變更後停損設定
// 			if(typeof(nstoploss1)=="undefined"){
				
// 			}else{
// 				$("#validate_NSTOPPROF1").removeClass(
// 				"validate[funcCall[validate_Radio[<spring:message code= "LB.W1210" /> <spring:message code= "LB.X0419" />,NSTOPPROF1]]]");
// 			}
			
			//檢核自訂
			if (nstopprof1 == '0') {
				var nstopprof2 = $("#NSTOPPROF2").val();
				$("#NSTOPPROF").val(nstopprof2);
				if(nstopprof2 < 5){
					//變更後自動贖回設定請勿小於 5%
					//alert("<spring:message code='LB.X2124' />");
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X2124' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
					);
					return false;
				}
			}
			if (nstoploss1 == '0') {
				var nstoploss2 = $("#NSTOPLOSS2").val();
				$("#NSTOPLOSS").val(nstoploss2);
				if(nstoploss2 < 5){
					//變更後自動贖回設定請勿小於 5%
					//alert("<spring:message code='LB.X2124' />");
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X2124' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
					);
					return false;
				}
			}
			//檢核自訂 END
			
			var nstoploss = $("#NSTOPLOSS").val();
			var nstopprof = $("#NSTOPPROF").val();
			
			if (nstoploss == "")
				$("#NSTOPLOSS").val('0');
			if (nstopprof == "")
				$("#NSTOPPROF").val('0');
			
			if (nstopprof > 999 && noopt1 != 'Y') {
				//alert("<spring:message code= "LB.Alert126" />");
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert126' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
				);
				return false;
			}
			if (nstoploss > 100 && noopt1 != 'Y') {
				//alert("<spring:message code= "LB.Alert127" />");
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert127' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
				);
				return false;
			}

			return	true;
		}
		//啟用或是不啟用切換
		function change(flag) {
			//啟用
			if (flag == 'N') {
				//
				$("#NSTOPPROF1_1").attr("disabled", false);
				$("#NSTOPPROF1_2").attr("disabled", false);
				$("#NSTOPPROF1_3").attr("disabled", false);
				$("#NSTOPPROF1_4").attr("disabled", false);
				$("#NSTOPPROF1_5").attr("disabled", false);
				$("#NSTOPPROF1_6").attr("disabled", false);
				//
				$("#NSTOPLOSS1_1").attr("disabled", false);
				$("#NSTOPLOSS1_2").attr("disabled", false);
				$("#NSTOPLOSS1_3").attr("disabled", false);
				$("#NSTOPLOSS1_4").attr("disabled", false);
				$("#NSTOPLOSS1_5").attr("disabled", false);
				$("#NSTOPLOSS1_6").attr("disabled", false);
				//
				$("#NSTOPPROF2").attr("disabled", false);
				$("#NSTOPLOSS2").attr("disabled", false);
			} 
			//不啟用
			else 
			{
				$("#NSTOPPROF1_1").attr("disabled", true);
				$("#NSTOPPROF1_2").attr("disabled", true);
				$("#NSTOPPROF1_3").attr("disabled", true);
				$("#NSTOPPROF1_4").attr("disabled", true);
				$("#NSTOPPROF1_5").attr("disabled", true);
				$("#NSTOPPROF1_6").attr("disabled", true);
				//
				$("#NSTOPLOSS1_1").attr("disabled", true);
				$("#NSTOPLOSS1_2").attr("disabled", true);
				$("#NSTOPLOSS1_3").attr("disabled", true);
				$("#NSTOPLOSS1_4").attr("disabled", true);
				$("#NSTOPLOSS1_5").attr("disabled", true);
				$("#NSTOPLOSS1_6").attr("disabled", true);
				//
				$("#NSTOPPROF2").attr("disabled", true);
				$("#NSTOPLOSS2").attr("disabled", true);
				//
				$("#NSTOPPROF2").val("");
				$("#NSTOPLOSS2").val("");
			}
		}
		//自訂切換事件
		function changecheck(orderno) {
			var NSTOPPROF2 = $("#NSTOPPROF2").val();
			var NSTOPLOSS2 = $("#NSTOPLOSS2").val();
			if (NSTOPPROF2.indexOf("-") > -1 || NSTOPPROF2.indexOf("+") > -1){
				//alert("<spring:message code= "LB.Alert128" />");
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert128' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
				);
			}
			if (NSTOPLOSS2.indexOf("-") > -1 || NSTOPLOSS2.indexOf("+") > -1){
				//alert("<spring:message code= "LB.Alert129" />");
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert129' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
				);
			}
			if (NSTOPPROF2.indexOf(".") > -1 || NSTOPLOSS2.indexOf(".") > -1){
				//alert("<spring:message code= "LB.Alert130" />");
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert130' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
				);
			}
			if (orderno == '1' && NSTOPPROF2.length > 0) {
				$("#NSTOPPROF").val(NSTOPPROF2);
			}
			if (orderno == '2' && NSTOPLOSS2.length > 0) {
				$("#NSTOPLOSS").val(NSTOPLOSS2);
			}
		}
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金    -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0901" /></li>
    <!-- 基金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1064" /></li>
    <!-- SMART FUND自動贖回設定     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1184" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!--SMART FUND自動贖回設定 -->
				<h2>
					<spring:message code="LB.W1184" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form autocomplete="off" method="post" id="formId" action="${__ctx}/FUND/ALTER/smart_fund_confirm">
					<c:set var="BaseResultData" value="${smart_fund_step2.data}"></c:set>
					<input type="hidden" name="ADOPID" value="C118">
					<input type="hidden" id="NOOPT" name="NOOPT" value="">
					<input type="hidden" id="NSTOPPROF" name="NSTOPPROF" value="">
					<input type="hidden" id="NSTOPLOSS" name="NSTOPLOSS" value="">
					<input type="hidden" id="AC202" name="AC202" value="${ BaseResultData.AC202}">
					<!-- 表單顯示區 -->
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<!--變更種類-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W1167" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span><spring:message code="LB.W1186" /></span>
										</div>
									</span>
								</div>
								<!--身分證字號/統一編號-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.Id_no" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.hideid_CUSIDN }
											</span>
										</div>
									</span>
								</div>
								<!--信託帳號-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0944" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.hideid_CDNO }
											</span>
										</div>
									</span>
								</div>
								<!--基金名稱-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0025" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.FUNDLNAME }
											</span>
										</div>
									</span>
								</div>
								<!--類別-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.D0973" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.str_AC202 }
											</span>
										</div>
									</span>
								</div>
								<!--信託金額-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0026" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												<!-- 幣別 -->
												${BaseResultData.ADCCYNAME }
												<!--金額 -->
												${BaseResultData.FUNDAMT }
											</span>
										</div>
									</span>
								</div>
								<!--單位數-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0027" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.display_ACUCOUNT }
											</span>
										</div>
									</span>
								</div>
								<!--未分配金額-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0921" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.display_NAMT }
											</span>
										</div>
									</span>
								</div>
								<!--參考現值-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0915" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.display_AMT }
											</span>
										</div>
									</span>
								</div>
								<!--基準報酬率-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W1195" /><spring:message code="LB.W1196" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.RTNC}
												${BaseResultData.display_RTNRATE } %
											</span>
										</div>
									</span>
								</div>
								<!--參考淨值-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W1198" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.display_REFVALUE1 }
											</span>
										</div>
									</span>
								</div>

								<!--參考匯率-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0030" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.display_FXRATE }
											</span>
										</div>
									</span>
								</div>
								<!--原停利贖回設定-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W1206" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.display_STOPPROF }
											</span>
										</div>
									</span>
								</div>
								<!--原停損贖回設定-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W1207" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.display_STOPLOSS }
											</span>
										</div>
									</span>
								</div>
								<!--自動贖回設定-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.X0418" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.D0324" />
												<input type="radio" value="" name="NOOPT1" onclick="change('N');">
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.D0995" />
												<input ${BaseResultData.NOOPTFLAG }  type="radio" value="Y" name="NOOPT1" onclick="change('Y');">
												<span class="ttb-radio"></span></label>
										</div>
										<span class="hideblock">
											<input id="validate_NOOPT1" name="NOOPT1" type="radio"
												class="validate[funcCall[validate_Radio['<spring:message code="LB.X0418" />',NOOPT1]]]"
												style="visibility:hidden" />
										</span>
									</span>
								</div>
								<!--自動贖回出場點設定-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W1210" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color">
												<spring:message code="LB.W1211" />：
											</span>
										</div>
										<div class="ttb-input">
											<label class="radio-block">
												<input type="radio" value="10" name="NSTOPPROF1" id="NSTOPPROF1_1"
													onClick="clearset('1');">
												<span class="ttb-radio">
												</span>
												+10%
											</label>
											<label class="radio-block">
												<input type="radio" value="15" name="NSTOPPROF1" id="NSTOPPROF1_2"
													onClick="clearset('1');">
												<span class="ttb-radio">
												</span>
												+15%
											</label>
											<label class="radio-block">
												<input type="radio" value="20" name="NSTOPPROF1" id="NSTOPPROF1_3"
													onClick="clearset('1');">
												<span class="ttb-radio"></span>
												+20%
											</label>
											<label class="radio-block">
												<input type="radio" value="25" name="NSTOPPROF1" id="NSTOPPROF1_4"
													onClick="clearset('1');">
												<span class="ttb-radio"></span>
												+25%
											</label>
											<label class="radio-block">
												<input type="radio" value="30" name="NSTOPPROF1" id="NSTOPPROF1_5"
													onClick="clearset('1');">
												<span class="ttb-radio"></span>
												+30%
											</label>
											<div class="ttb-input">
												<label class="radio-block">
													<input type="radio" value="0" name="NSTOPPROF1" id="NSTOPPROF1_6">
													<span class="ttb-radio"></span>
													<spring:message code="LB.W1212" />
												</label>
												<input class="text-input" type="text" id="NSTOPPROF2" NAME="NSTOPPROF2"
													size="4" maxlength="3" onBlur="changecheck('1')">%
											</div>
											<span class="hideblock">
												<input id="validate_NSTOPPROF1" name="NSTOPPROF1" type="radio"
													class="validate[funcCall[validate_Radio[<spring:message code= "LB.W1210" /> <spring:message code= "LB.W1211" />,NSTOPPROF1]]]"
													style="visibility:hidden" />
											</span>
										</div>
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color"> <spring:message code= "LB.X0419" />：</span>
										</div>
										<div class="ttb-input">
											<label class="radio-block">
												<input type="radio" value="10" name="NSTOPLOSS1" id="NSTOPLOSS1_1"
													onClick="clearset('2');">
												<span class="ttb-radio"></span>
												-10%
											</label>
											<label class="radio-block">
												<input type="radio" value="15" name="NSTOPLOSS1" id="NSTOPLOSS1_2"
													onClick="clearset('2');">
												<span class="ttb-radio"></span>
												-15%
											</label>
											<label class="radio-block">
												<input type="radio" value="20" name="NSTOPLOSS1" id="NSTOPLOSS1_3"
													onClick="clearset('2');">
												<span class="ttb-radio"></span>
												-20%
											</label>
											<label class="radio-block">
												<input type="radio" value="25" name="NSTOPLOSS1" id="NSTOPLOSS1_4"
													onClick="clearset('2');">
												<span class="ttb-radio"></span>
												-25%
											</label>
											<label class="radio-block">
												<input type="radio" value="30" name="NSTOPLOSS1" id="NSTOPLOSS1_5"
													onClick="clearset('2');">
												<span class="ttb-radio"></span>
												-30%
											</label>
										</div>
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.W1212" />
												<input type="radio" value="0" name="NSTOPLOSS1" id="NSTOPLOSS1_6">
												<span class="ttb-radio"></span>
											</label>
											<input class="text-input" type="text" id="NSTOPLOSS2" NAME=NSTOPLOSS2
												size="4" maxlength="3" onBlur="changecheck('2')">%
										</div>
										<span class="hideblock">
											<input id="validate_NSTOPLOSS1" name="NSTOPLOSS1" type="radio"
												class="validate[funcCall[validate_Radio[<spring:message code= "LB.W1210" /> <spring:message code= "LB.X0419" />,NSTOPLOSS1]]]"
												style="visibility:hidden" />
										</span>
									</span>
								</div>
							</div>
							<!-- button -->
							<input class="ttb-button btn-flat-gray" name="CMRESET" id="CMRESET" type="button" value="<spring:message code="LB.Re_enter" />" />
							<input class="ttb-button btn-flat-gray" name="CMBACK" id="CMBACK" type="button" value="<spring:message code="LB.X0420" />" />
							<input class="ttb-button btn-flat-orange" name="CMSUBMIT" id="CMSUBMIT" type="button" value="<spring:message code="LB.Confirm" />" />
							<!-- button -->
						</div>
					</div>
					<div class="text-left">
						<ol class="description-list list-decimal">
						<!-- 						說明： -->
						<p>
							<spring:message code="LB.Description_of_page" />
						</p>
						<ol>
							<li>
								<span>
									<strong style="FONT-WEIGHT: 400"><spring:message code="LB.Smart_Fund_P2_D1" /></strong>
								</span>
							</li>
							<li>
								<span>
									<strong style="FONT-WEIGHT: 400"><spring:message code="LB.Smart_Fund_P2_D2" /></strong>
								</span>
							</li>
						</ol>
					</div>
				</form>
			</section>
			<!-- main-content END -->
		</main>
	</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>