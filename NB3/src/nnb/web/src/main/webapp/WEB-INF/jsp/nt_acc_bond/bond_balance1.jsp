<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {
		// 將.table變更為footable
		initFootable();
	});
	
	//下拉式選單
 	function formReset() {
 		if ($('#actionBar').val()=="reEnter"){
	 		document.getElementById("formId").reset();
	 		$('#actionBar').val("");
 		}
	}
</script>
</head>
	<body>
		<!-- header     -->
		<header>
			<%@ include file="../index/header.jsp"%>
		</header>
	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 貸款服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Loan_Service" /></li>
    <!-- 借款查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Loan_Inquiry" /></li>
		</ol>
	</nav>

		<!-- menu、登出窗格 -->
		<div class="content row">
			<%@ include file="../index/menu.jsp"%>
			<!-- 	快速選單內容 -->
			<!-- content row END -->
			<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2><spring:message code= "LB.W0034" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="">
					<div class="main-content-block row">
						<div class="col-12">
							<!-- 中央登錄債券餘額 -->
							<table class="table" data-toggle-column="first">
								<tbody>
									<tr>
										<!-- 中央登錄債券帳號 -->
										<td><spring:message code= "LB.X1566" /></td>
										<!-- 帳號 的下拉式選單-->
										<td>
											<form>
												<select name="dptrdacno">
													<option value="00">---<spring:message code= "LB.Select" /><spring:message code= "LB.Execution_option" />---</option>
													<option value="01"><spring:message code= "LB.Account" />1</option>
													<option value="02"><spring:message code= "LB.Account" />2</option>
												</select>
											</form>
										</td>
									</tr>
								</tbody>
							</table>
							<!-- 網頁顯示-->
							<input type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Display_as_web_page"/>"/>
							<!-- 重新輸入-->
	                        <input type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Re_enter"/>"/>
						</div>
					</div>
				</form>
				</section>
			</main>
			<!-- 		main-content END -->
			<!-- 	content row END -->
		</div>
		<%@ include file="../index/footer.jsp"%>
	</body>
</html>