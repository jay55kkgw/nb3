<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page import = "java.io.*"%>
<%@ page import = "java.util.*"%>
<c:set var="__ctx" value="${pageContext.request.contextPath}" />
<c:set var="__path" value="${requestScope['javax.servlet.forward.request_uri']}" />
<c:set var="__pathII" value="${fn:replace( requestScope['javax.servlet.forward.request_uri'], __ctx, '')}" />
<c:set var="__i18n_en" value="${__path}?locale=en" />
<c:set var="__i18n_zh_TW" value="${__path}?locale=zh_TW" />
<c:set var="__i18n_zh_CN" value="${__path}?locale=zh_CN" />
<c:set var="__i18n_locale" value="${pageContext.response.locale}" />
<c:set var="transfer" value="${pageContext.response.locale}" />
<c:choose>
	<c:when test="${transfer == 'en'}"> 
		<c:set var="transfer" value="en" />
	</c:when>
	<c:when test="${transfer == 'zh_TW'}"> 
		<c:set var="transfer" value="zh-TW" />
	</c:when>
	<c:when test="${transfer == 'zh_CN'}"> 
		<c:set var="transfer" value="zh" />
	</c:when>
</c:choose>
<!-- 列印無邊界 -->
<style type="text/css" media="print">
@page {
	size:auto;
    margin: 5% ;  /* this affects the margin in the printer settings */
}
</style>

