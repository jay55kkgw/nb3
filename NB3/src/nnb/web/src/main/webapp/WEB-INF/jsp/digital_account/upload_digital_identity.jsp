<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %> 
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
	    <script type="text/javascript">
        $(document).ready(function () {
            init();
        });

        function init() {
	    	$("#CMSUBMIT").click(function(e){
	        	// $("#formId").validationEngine({binded:false,promptPosition: "inline" });
				$("#formId").validationEngine({
					validationEventTriggers:'keyup blur', 
					binded:true,
					scroll:true,
					promptPosition: "inline" });
				$("#CUSIDN").val($("#CUSIDN").val().toUpperCase());
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
// 		        	initBlockUI();
		        	var action = '${__ctx}/DIGITAL/ACCOUNT/upload_digital_identity_p1';
					$("form").attr("action", action);
	    			$("form").submit();
				}
			});
        }
    </script>
	<style>
	
				@media screen and (max-width:767px) {
				.ttb-button {
						width:30%;
						left: 0px;
				}
				#CMBACK.ttb-button{
						border-color: transparent;
						box-shadow: none;
						color: #333;
						background-color: #fff;
				}
		 
		
			}
		</style>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	<!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<!--數位存款帳戶 -->
			<li class="ttb-breadcrumb-item"><spring:message code= "LB.X0348" /></a></li>
			<!--修改數位存款帳戶資料 -->
			<li class="ttb-breadcrumb-item"><a href="#">補上傳身分證件</a></li>
		</ol>
	</nav>
	<div class="content row">
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					補上傳身分證件
				</h2>
				<div id="step-bar">
                    <ul>
                        <li class="active">身分驗證</li>
                        <li class="">上傳證件</li>
                        <li class="">確認資料</li>
                        <li class="">完成補件</li>
                    </ul>
                </div>
                <form id="formId" method="post">
					<input type="hidden" name="ADOPID" value="N203">
				  	<input type="hidden" name="HLOGINPIN" id="HLOGINPIN" value="">
				  	<input type="hidden" name="HTRANSPIN" id="HTRANSPIN" value="">
					<input type="hidden" name="LMTTYN" id="LMTTYN" value="">	
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
                        
							<div class="ttb-input-block">
							<div class="ttb-message">
                                <p>身份驗證</p>
                            </div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0025" />
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" name="CUSIDN"  id="CUSIDN"  value="" class="text-input validate[required, funcCall[validate_checkSYS_IDNO[CUSIDN]]]" placeholder="請輸入身分證字號">
										</div>
									</span>
								</div>
                           	</div>  
	                       	<input type="button" id="CMBACK" value="<spring:message code="LB.D0171"/>" class="ttb-button btn-flat-gray" onClick="window.close();"/>
	                        <input id="CMSUBMIT" name="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code= "LB.X0080" />" />
                        </div>
                    </div>
                </form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>