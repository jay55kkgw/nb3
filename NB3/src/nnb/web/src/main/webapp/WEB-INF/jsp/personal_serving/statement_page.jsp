<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript">
$(document).ready(function(){

	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	
	//表單驗證
	$("#formId").validationEngine({binded:false,promptPosition:"inline"});
	
	$("#CMSUBMIT").click(function(){
		//表單驗證
		if(!$("#formId").validationEngine("validate")){
			e = e || window.event;//for IE
			e.preventDefault();
		}
		$("#formId").validationEngine("detach");
		$("#READST").val("Y");
		$("#formId").attr("action","${__ctx}/PERSONAL/SERVING/mailChg_comfirm");
		$("#formId").submit();
	});
	$("#cancelButton").click(function(){
		$("#formId").validationEngine("detach");
		$("#formId").attr("action","${__ctx}/PERSONAL/SERVING/mail_setting_choose?type=DPSETUPE");
		$("#formId").submit();
	});
});
function goNotificationService(){
	console.log("go Notification Service")
	$('#formId').attr("action","${__ctx}"+"/PERSONAL/SERVING/notification_service");
	$('#formId').submit();
}
</script>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	<!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- Email設定     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Email_Setting" /></li>
    <!-- 我的Email設定     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.My_Email_setting" /></li>
		</ol>
	</nav>
	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!--快速選單及主頁內容-->
		<main class="col-12"> <!--主頁內容-->
		<section id="main-content" class="container">
			<h2><spring:message code="LB.My_Email_setting" /></h2>
			<form id="formId" method="post">
				<input type="hidden" id="READST" name="READST" value=""/>
				<div class="main-content-block row">
					<div class="col-12">
						<div class="ttb-message">
							<p><spring:message code="LB.X2622" /></p>
						</div>
						<div class="ttb-message" style="margin-right: 3%;margin-left: 3%; text-align: left;">
							<p><spring:message code="LB.X2623" />
							</p>
						</div>
						<input type="button" id="cancelButton" value="<spring:message code="LB.D0041" />" class="ttb-button btn-flat-gray"/>	
					<!-- 同意 -->
			        <input type="button" id="CMSUBMIT" value="<spring:message code="LB.D0097" />" class="ttb-button btn-flat-orange"/>
					</div>
				</div>
			</form>
			<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /></p>
						<li><span><spring:message code="LB.Mail_set_P2_D1" /></span><u><input onClick="goNotificationService();" class="ttb-sm-btn btn-flat-orange" type="button" value='<spring:message code="LB.Notification_Service" />' name="CMSUBMIT1"></u></li>
						<li><span><spring:message code="LB.Mail_set_P2_D2" /></span></li>
						<li><span><spring:message code="LB.Mail_set_P2_D3" /></span></li>
						<li><span><spring:message code="LB.Mail_set_P2_D4" /></span></li>
						<li><span><font color="red"><spring:message code="LB.Mail_set_P2_D5" /></font></span></li>
					</ol>
	</div>
	</section>
	</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>