<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">    
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
</head>
 <body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 輕鬆理財戶     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0004" /></li>
    <!-- 存摺明細     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0005" /></li>
		</ol>
	</nav>



	
	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<main class="col-12"> 
	<!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<h2><spring:message code="LB.W0005" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 						下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
<!-- 						下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
					</select>
				</div>						
			<form method="post" id="formId">
				<!-- 下載用 -->
				<input type="hidden" name="downloadFileName" value="<spring:message code="LB.W0005" />"/>
				<input type="hidden" name="downloadType" id="downloadType"/> 					
				<input type="hidden" name="templatePath" id="templatePath"/>
				<input type="hidden" name="hasMultiRowData" value="true"/>
				<!-- EXCEL下載用 -->
                <input type="hidden" name="headerRightEnd" value="1" />
                <input type="hidden" name="headerBottomEnd" value="4" />
                <input type="hidden" name="multiRowStartIndex" value="8" />
                <input type="hidden" name="multiRowEndIndex" value="8" />
                <input type="hidden" name="multiRowCopyStartIndex" value="6" />
                <input type="hidden" name="multiRowCopyEndIndex" value="8" />
                <input type="hidden" name="multiRowDataListMapKey" value="TABLE" />
                <input type="hidden" name="rowRightEnd" value="6" />
				<!-- TXT下載用 -->
                <input type="hidden" name="txtHeaderBottomEnd" value="6"/>
				<input type="hidden" name="txtMultiRowStartIndex" value="11"/>
				<input type="hidden" name="txtMultiRowEndIndex" value="11"/>
				<input type="hidden" name="txtMultiRowCopyStartIndex" value="8"/>
				<input type="hidden" name="txtMultiRowCopyEndIndex" value="11"/>
				<input type="hidden" name="txtMultiRowDataListMapKey" value="TABLE"/>
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">							
							<!-- 帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
								<h4><spring:message code="LB.Account" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
									<!--帳號 -->
										<select class="custom-select select-input half-input" name="ACN" id="ACN">
												<c:forEach var="dataList" items="${pbook_details.data.REC}">
													<option>${dataList.ACN}</option>
												</c:forEach>
												<option value=""><spring:message code="LB.All" /></option>
										</select>
									</div>
								</span>
							</div>				
							 
							<div class="ttb-input-item row">
								<!--查詢區間  -->
								<span class="input-title">
									<label>
										<spring:message code="LB.Inquiry_period" />
									</label>
								</span>
								<span class="input-block">
									<!--  指定日期區塊 -->
									<div class="ttb-input">
											<!--期間起日 -->
											<div class="ttb-input">
												<span class="input-subtitle subtitle-color">
													<spring:message code="LB.Start_date" />
												</span>
												<input type="text" id="CMSDATE" name="CMSDATE" class="text-input datetimepicker validate[required,verification_date[CMSDATE]]" maxlength="10" value="${pbook_details.data.TODAY}" /> 
												<span class="input-unit CMSDATE">
													<img src="${__ctx}/img/icon-7.svg" />
												</span>
<!-- 												<span>~</span> -->
												
											</div>
											<div class="ttb-input">
												<span class="input-subtitle subtitle-color">
													<spring:message code="LB.Period_end_date" />
												</span>
												<input type="text" id="CMEDATE" name="CMEDATE" class="text-input datetimepicker validate[required,verification_date[CMEDATE]]" maxlength="10" value="${pbook_details.data.TODAY}" /> 
												<span class="input-unit CMEDATE">
													<img src="${__ctx}/img/icon-7.svg" />
												</span>
												<span id="hideblockc" >
												<!-- 驗證用的input -->
													<input id="odate" name="odate" value="${pbook_details.data.TODAY}"  type="text" class="text-input validate[required,funcCall[validate_CheckDateScope['<spring:message code="LB.X1445" />', odate, CMSDATE, CMEDATE, false, 24, null]]]" 
														style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												</span>
											</div>
									</div>
								</span>
							</div>
						</div>
						<input type="button" class="ttb-button btn-flat-orange" id="pageshow" value="<spring:message code="LB.Display_as_web_page" />"/>
					</div>
				</div>
					<div class="text-left">
						<ol class="description-list list-decimal">
							<p>
								<spring:message code="LB.Description_of_page" />
							</p>
							<li><spring:message code="LB.Passbook_Details_P1_D1" /></li>
						</ol>
					</div>								
				</form>
			</section>
		<!-- 		main-content END -->
		</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
<!--   Js function -->
    <script type="text/JavaScript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 50);
		// 開始跑下拉選單並完成畫面
		setTimeout("init()", 400);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	
		datetimepickerEvent();
		$("#formId").validationEngine({binded: false,promptPosition: "inline"});
		$("#pageshow").click(function(e){
			e = e || window.event;
			if(!$('#formId').validationEngine('validate')){
	        	e.preventDefault();
 			}else{
 				$("#formId").validationEngine('detach');
 				initBlockUI();
 				$("#formId").attr("action","${__ctx}/NT/ACCT/MANAGEMENT/passbook_details_result");
 	  			$("#formId").submit(); 
 			}		
  		});
	});
	
//		日曆欄位參數設定
	function datetimepickerEvent(){
	    $(".CMSDATE").click(function(event) {
			$('#CMSDATE').datetimepicker('show');
		});
	    $(".CMEDATE").click(function(event) {
			$('#CMEDATE').datetimepicker('show');
		});
		
		jQuery('.datetimepicker').datetimepicker({
			timepicker:false,
			closeOnDateSelect : true,
			scrollMonth : false,
			scrollInput : false,
		 	format:'Y/m/d',
		 	lang: '${transfer}'
		});
	}
		//選項
		 	function formReset() {
		 		if( $('#CMPERIOD').prop('checked') )
				{
					
				}
		 		if(!$("#formId").validationEngine("validate")){
					e = e || window.event;//forIE
					e.preventDefault();
				}
		 		else{
			 		//initBlockUI();
					if ($('#actionBar').val()=="excel"){
						$("#downloadType").val("OLDEXCEL");
						$("#templatePath").val("/downloadTemplate/passbook_details.xls");
			 		}else if ($('#actionBar').val()=="txt"){
						$("#downloadType").val("TXT");
					    $("#templatePath").val("/downloadTemplate/passbook_details.txt");
			 		}
					$("#formId").attr("target", "");
					$("#formId").attr("action", "${__ctx}/NT/ACCT/MANAGEMENT/passbook_details_DirectDownload");
					$("#formId").submit();
					$('#actionBar').val("");
		 		}
			}
		
 	</script>
</body>
</html>
