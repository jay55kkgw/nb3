<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<!--舊版驗證-->
<script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
<!-- DIALOG會用到 -->
<script type="text/javascript" src="${__ctx}/js/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery-ui.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	//
	$("#formID").validationEngine({binded: false,promptPosition: "inline", scroll: false});
	
	if(${MARK1 == 'Y'}){
		$("input[name=MARK1][value=Y]").attr("checked",true);
	}else{
		$("input[name=MARK1][value=N]").attr("checked",true);
	}
	//當日僅能填寫三次評估，造成您的不便，敬請見諒
	if(${todayUserKYCCountBoolean == false}){
		var message = "<spring:message code= 'LB.Alert093' />";
		callErrorBlock(message);
		// 複寫errorBtn1 事件	
		$("#errorBtn1").click(function(e) {
			$("#formID").validationEngine('detach');
	    	var action = '${__ctx}/INDEX/index';
			$("#formID").attr("action", action);
			$("#formID").submit();
		});
		
		$("#CMSUBMIT").prop("disabled",true);
		$("#CMSUBMIT").removeClass("btn-flat-orange");
	}
	else{
		errorBlock(
				["<spring:message code= 'LB.X2375' />"], 
				["<li style='list-style:none;'>1.<spring:message code= 'LB.X2376' /></li>","<li style='list-style:none;padding-top: 20px'>2.<spring:message code= 'LB.X2377' /></li>","<li style='list-style:none;padding-top: 20px'>3.<spring:message code= 'LB.X2378' /></li>"],
				null, 
				'<spring:message code= "LB.Quit" />', 
				null
		);
		// 複寫errorBtn1 事件	
		$("#errorBtn1").click(function(e) {
			$('#error-block').hide();
			var email = '${sessionScope.dpmyemail}';
			if(email.length == 0){
				email = "<spring:message code= 'LB.X2599' />";
			}
			errorBlock(
					["<spring:message code= 'LB.X0152' />"], 
					["<li style='list-style:none;'><spring:message code= 'LB.X2594' /></li>","<li style='list-style:none;padding-top: 20px'><b><font color='red'><spring:message code= 'LB.X2595' />？</font><b></li>","<li style='list-style:none;padding-top: 20px'><spring:message code= 'LB.X2596' />&nbsp;<a href='#' style='color: blue;' onclick='gotoEmailSetting()''><spring:message code= 'LB.Internet_banking' /></a> / <spring:message code= 'LB.X2597' /></li>","<li style='list-style:none;padding-top: 20px'><spring:message code= 'LB.X2598' />。</li>","<li style='list-style:none;padding-top: 20px'><spring:message code= 'LB.Email' />："+email+"</li>"],
					null, 
					'<spring:message code= "LB.Confirm" />', 
					null
			);
		});
	}
	//	
	$("#CAREERNO").click(function(){
	    var age = parseInt("${age}");
	    $("#careerDialog").dialog("open");
	});
	//
	$("#careerDialog").dialog({
		autoOpen:false,
		modal:true,
		closeOnEscape:true,
		width:850,
		maxHeight:570,
		position:{
			my:"center",at:"top-500px",of:window
		}
	});
	//
	$("#CMSUBMIT").click(function(e){
		var age = parseInt("${age}");
		var degree = "${Q2A}";
		var career = "${CAREER}";
		var salary = parseInt("${salary}");
		var flag1 = "N";
		var flag2 = "N";
		var flag3 = "N";
		
		var Q2 = $("input[name=Q2]:checked").val();
		if(window.console){console.log("Q2=" + Q2);}
	
		if(Q2 != degree){
			flag1 = "Y";
		}
	
		if($("#SRCFUND").val() != career){
			flag2 = "Y";
		}
		
		var newsalary = parseInt($("#newsalary").val());
		if(window.console){console.log("newsalary=" + newsalary);}
		
		if(newsalary * 10000 != salary){
			flag3 = "Y";
		}
		
		$("#UPD_FLG").val(flag1 + flag2 + flag3);
		$("#Q2A").val(degree);
		$("#Q3A").val(career);
		$("#Q4A").val(salary);
		$("#validate_Q4").val(newsalary);
		$("#validate_Q4_2").val(newsalary);
		
		  if (!CheckRadio("<spring:message code= 'LB.X1518' />", "Q2")){
			  $('input[name=Q2]').focus();
		  	  return false;
		  }
		  if (!CheckRadio("<spring:message code= 'LB.X1519' />", "Q3")){
			  $('input[name=Q3]').focus();			  
		  	  return false;
		  }
		  if (!CheckRadio("<spring:message code= 'LB.X1521' />", "Q4")){
			  $('input[name=Q4]').focus();			  
		  	  return false;
		  }
		  if (!CheckRadio("<spring:message code= 'LB.X1522' />", "Q5")){
			  $('input[name=Q5]').focus();			  
		  	  return false;
		  }
		  if (!CheckRadio("<spring:message code= 'LB.X1523' />", "Q6")){
			  $('input[name=Q6]').focus();		  
		      return false;
		  }
		  if (!CheckRadio("<spring:message code= 'LB.X1524' />", "Q7")){
			  $('input[name=Q7]').focus();			  
		  	  return false;
		  }
		  if (!CheckRadio("<spring:message code= 'LB.X1525' />", "Q8")){
			  $('input[name=Q8]').focus();			  
		  	  return false;
		  }
		  if (!CheckRadio("<spring:message code= 'LB.X1526' />", "Q9")){
			  $('input[name=Q9]').focus();			  
		  	  return false;
		  }
		  	  
		  if (!CheckRadio("<spring:message code= 'LB.X1527' />", "Q10")){
			  $('input[name=Q10]').focus();			  
		  	  return false;
		  }

		//請點選問題十一
		if($("#Q111").prop("checked") == false && $("#Q112").prop("checked") == false && $("#Q113").prop("checked") == false && $("#Q114").prop("checked") == false){
			var message = "<spring:message code= 'LB.Alert095' />";
			callErrorBlock(message);
			return false;      
		}
		//問題11：因您已勾選A或B或C選項，不能選擇D選項
		if($("#Q111").prop("checked") == true || $("#Q112").prop("checked") == true || $("#Q113").prop("checked") == true){	  
			if($("#Q114").prop("checked") == true){
				var message = "<spring:message code= 'LB.Alert096' />";
				callErrorBlock(message);
				return false;    
			}
		}
		 if (!CheckRadio("<spring:message code= 'LB.X1528' />", "Q12"))
		      return false;	
		//點選問題十三
		if($("#Q131").prop("checked") == false && $("#Q132").prop("checked") == false && $("#Q133").prop("checked") == false && $("#Q134").prop("checked") == false){
			var message = "<spring:message code= 'LB.Alert097' />";
			callErrorBlock(message);
			return false;
		} 
		//問題13：因您已勾選A或B或C選項，不能選擇D選項
		if($("#Q131").prop("checked") == true || $("#Q132").prop("checked") == true || $("#Q133").prop("checked") == true){	  
			if($("#Q134").prop("checked") == true){
				var message = "<spring:message code= 'LB.Alert098' />";
				callErrorBlock(message);
				return false;    
			}
		}
		
		  if (!CheckRadio("<spring:message code= 'LB.X1529' />", "Q14")){
			  $('input[name=Q14]').focus();
		      return false;	
		  }
		  if (!CheckRadio("<spring:message code= 'LB.X1530' />", "Q15")){
			  $('input[name=Q15]').focus();
		      return false;	
		  }
		  
// 		檢核問題12：您的投資時間需小於您的年齡，請重新選擇。
		var age = parseInt("${age}");
		if(window.console){console.log("age=" + age);}
		var Q12Value = $("input[name=Q12]:checked").val();
		if(window.console){console.log("Q12Value=" + Q12Value);}
// 		問題12：您的投資時間需小於您的年齡，請重新選擇。
		if(Q12Value == "B"){
			if(1 >= age){		
				var message = "<spring:message code= 'LB.Alert099' />";
				callErrorBlock(message);
				return false;
			}
		}
// 		問題12：您的投資時間需小於您的年齡，請重新選擇。
		if(Q12Value == "C"){
			if(2 >= age){
				var message = "<spring:message code= 'LB.Alert099' />";
				callErrorBlock(message);
				return false;
			}
		}
		//問題12：您的投資時間需小於您的年齡，請重新選擇。"
		if(Q12Value == "D"){
			if(5 >= age){
				var message = "<spring:message code= 'LB.Alert099' />";
				callErrorBlock(message);
				return false;
			} 
		}
		//檢核問題2
		var Q2Value = $("input[name=Q2]:checked").val();
		if(window.console){console.log("Q2Value=" + Q2Value);}
		//問題2：博士學歷與年齡不符，請重新選擇。
		if(Q2Value == "A"){
			if(age < 24){
				var message = "<spring:message code= 'LB.Alert100' />";
				callErrorBlock(message);
				return false;
			}
		} 
		//問題2：碩士學歷與年齡不符，請重新選擇。
		if(Q2Value == "B"){
			if(age < 22){		
				var message = "<spring:message code= 'LB.Alert101' />";
				callErrorBlock(message);
				return false;
			}
		}
		//問題2：大學學歷與年齡不符，請重新選擇。
		if(Q2Value == "C"){
			if(age < 18){
				var message = "<spring:message code= 'LB.Alert102' />";
				callErrorBlock(message);
				return false;
			}
		}
		//問題2：專科學歷與年齡不符，請重新選擇
		if(Q2Value == "D"){
			if(age < 16){
				var message = "<spring:message code= 'LB.Alert103' />";
				callErrorBlock(message);
				return false;
			}
		}
		//問題2：高中職學歷與年齡不符，請重新選擇
		if(Q2Value == "E"){
			if(age < 16){
				var message = "<spring:message code= 'LB.Alert104' />";
				callErrorBlock(message);
				return false;
			} 
		}
		// 問題3：職業與年齡不符，請重新選擇
		var Q3Value = $("input[name=Q3]:checked").val();
		if(window.console){console.log("Q3Value=" + Q3Value);}
		if(Q3Value == "A"){
			if(age < 16){		
				var message = "<spring:message code= 'LB.Alert105' />";
				callErrorBlock(message);
				return false;
			}
		}
		if(Q3Value == "B"){
			if(age < 16){
				var message = "<spring:message code= 'LB.Alert105' />";
				callErrorBlock(message);
				return false;
			}
		}
		// 未選取是否有全民健保重大傷病證明
		if(!CheckRadio("<spring:message code="LB.X1312" />","MARK1")){	  
			return false;
		}

		e = e || window.event;
		//進行 validation 並送出
		if (!$('#formID').validationEngine('validate')) {
			e.preventDefault();
		} else {
			$("#formID").validationEngine('detach');
			// 若有指定 ACTION 則 submit 到指定路徑
			var next = "${next}";
			if(next != "")
			{
				action = "${__ctx}" + next;
				$("#formID").attr("action", action);
			}
			
		 	$("#formID").submit();
		}
	});
	$("#previous").click(function(){
		// 讓Controller知道是回上一頁
		$("#formID").append('<input type="hidden" name="back" value="Y" />');	
		$("#formID").attr("action", "${__ctx}" + "${previous}");
		$("#formID").submit();
	});
	
});
//
function fillData(ID,desc){					
	var age = parseInt("${age}");
	$("#SRCFUND").val(ID);
	$("#SRCFUNDDESC").val(ID + "-" + desc);
	
	if(ID == "0615I0"){
		$("input[name=Q3][value=A]").prop("disabled",false);
		$("input[name=Q3][value=A]").prop("checked",true);
		$("input[name=Q3][value=B]").prop("disabled",true);
		$("input[name=Q3][value=C]").prop("disabled",true);
	}
	else if(ID=="061410" || ID=="061700" || ID =="061690"|| ID =="061691" || ID =="061692"){
		$("input[name=Q3][value=C]").prop("disabled",false);
		$("input[name=Q3][value=C]").prop("checked",true);
		$("input[name=Q3][value=A]").prop("disabled",true);
		$("input[name=Q3][value=B]").prop("disabled",true);
	}
	else{
		$("input[name=Q3][value=B]").prop("disabled",false);
		$("input[name=Q3][value=B]").prop("checked",true);
		$("input[name=Q3][value=A]").prop("disabled",true);
		$("input[name=Q3][value=C]").prop("disabled",true);
	}
	$("#careerDialog").dialog("close");
}		
//
function switchq4(){
	if(!CheckAmount()) {
		return false;
	}
	var newsalary = parseInt($("#newsalary").val());
	if(window.console){console.log("newsalary=" + newsalary);}
	if(newsalary * 10000 < 500000){
		$("input[name=Q4][value=A]").prop("disabled",false);
		$("input[name=Q4][value=A]").prop("checked",true);
		$("input[name=Q4][value=B]").prop("disabled",true);
		$("input[name=Q4][value=C]").prop("disabled",true);
		$("input[name=Q4][value=D]").prop("disabled",true);
	}
	else if(newsalary * 10000 >= 500000 && newsalary * 10000 < 700000){
		$("input[name=Q4][value=B]").prop("disabled",false);
		$("input[name=Q4][value=B]").prop("checked",true);
		$("input[name=Q4][value=A]").prop("disabled",true);
		$("input[name=Q4][value=C]").prop("disabled",true);
		$("input[name=Q4][value=D]").prop("disabled",true);
	}	
	else if(newsalary * 10000 >= 700000 && newsalary * 10000 < 1000000){
		$("input[name=Q4][value=C]").prop("disabled",false);
		$("input[name=Q4][value=C]").prop("checked",true);
		$("input[name=Q4][value=A]").prop("disabled",true);
		$("input[name=Q4][value=B]").prop("disabled",true);
		$("input[name=Q4][value=D]").prop("disabled",true);
	}
	else{
		$("input[name=Q4][value=D]").prop("disabled",false);
		$("input[name=Q4][value=D]").prop("checked",true);
		$("input[name=Q4][value=A]").prop("disabled",true);
		$("input[name=Q4][value=B]").prop("disabled",true);
		$("input[name=Q4][value=C]").prop("disabled",true);
	}
}
//
function CheckAmount() {
	var sAmount = $("#newsalary").val();
	try {
		
		var message = '<spring:message code= 'LB.X2230' />';
		if ( isNaN(sAmount)) {//數字
// 			var message = '欄位請輸入正確的金額格式';
			callErrorBlock(message);
			return false;
		}
		//先TRIM掉，不要用startsWith與endsWith，在舊版瀏覽器會出錯
		sAmount = $.trim(sAmount);
		if (sAmount == "") {
// 			var message = '欄位請輸入正確的金額格式';
			callErrorBlock(message);
			return false;
		}
		if (sAmount.indexOf("+") == 0) {
// 			var message = '欄位請勿輸入加號';
			callErrorBlock(message);
			return false;
		}
		if (sAmount.charAt(0) == "0" && sAmount.length > 1) {//check不以零為開頭
// 			var message = '欄位請勿以零為開頭';
			callErrorBlock(message);
			return false;
		}
		var regex =  /^[0-9]*[1-9][0-9]*$/; 
		if (!regex.test(sAmount)) {
// 			var message = '欄位請輸入整數';
			callErrorBlock(message);
			return false;
		}

		return true;
	} catch (exception) {
 		console.log("檢核時發生錯誤:" + exception);
	}

}
//建立只有離開按鈕的ErrorBlock
function callErrorBlock(message){
	errorBlock(
			null, 
			null,
			[message], 
			'<spring:message code= "LB.Quit" />', 
			null
	);
	
}

//對於您過去的投資經驗？（本題可複選，取得分最高者計算）
function checkQ11(){
	// 選了不認識
	if($("#Q114").prop("checked")) {
		// 其他選項不可選
		$("#Q111").prop("checked", false);
		$("#Q112").prop("checked", false);
		$("#Q113").prop("checked", false);
		
		$("#Q111").prop('disabled', true);
		$("#Q112").prop('disabled', true);
		$("#Q113").prop('disabled', true);
		
	}
	// 沒有選不認識
	else {
		// 可以選其他選項
		$("#Q111").prop('disabled', false);
		$("#Q112").prop('disabled', false);
		$("#Q113").prop('disabled', false);
	}
}

//對金融商品的認識
function checkQ13(){
	// 選了不認識
	if($("#Q134").prop("checked")) {
		// 其他選項不可選
		$("#Q131").prop("checked", false);
		$("#Q132").prop("checked", false);
		$("#Q133").prop("checked", false);
		
		$("#Q131").prop('disabled', true);
		$("#Q132").prop('disabled', true);
		$("#Q133").prop('disabled', true);
		
	} // 沒有選不認識
	else {
		// 可以選其他選項
		$("#Q131").prop('disabled', false);
		$("#Q132").prop('disabled', false);
		$("#Q133").prop('disabled', false);
	}
}

function gotoEmailSetting(){
	$('#error-block').hide();
	fstop.getPage('${pageContext.request.contextPath}'+'/PERSONAL/SERVING/mail_setting','', '');//導頁去改信箱
}


</script>
</head>
<body>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %> 
	<c:if test="${sessionScope.onlineApply != 'true'}">
		<header>
			<%@ include file="../index/header.jsp"%>
		</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 投資屬性評估調查表－（自然人版）     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1218" /></li>
		</ol>
	</nav>

	</c:if>
	<!--左邊menu及登入資訊-->
	<div class="content row">
		<c:if test="${sessionScope.onlineApply != 'true'}">
			<%@ include file="../index/menu.jsp"%>
		</c:if>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.W1218" /></h2>
				<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
				<form id="formID" action="${__ctx}/FUND/TRANSFER/fund_invest_attr2" method="post">
					<input type="hidden" name="TXID" value="${TXID}"/>
					<input type="hidden" name="RTC" value="${TXID}"/>
					<input type="hidden" id="UPD_FLG" name="UPD_FLG"/>
           			<input type="hidden" id="Q2A" name="Q2A"/>
  					<input type="hidden" id="Q3A" name="Q3A"/>
  					<input type="hidden" id="Q4A" name="Q4A"/>
  					<input type="hidden" name="RISK" value="${RISK}"/>
  					<input type="hidden" name="OMARK1" value="${MARK1}"/>
  					<input type="hidden" name="ONLINEAPPLY" value="${sessionScope.onlineApply}"/>
      				<div class="main-content-block row">
						<div class="col-12 terms-block questionnaire-block">
<!-- 						線上客戶投資屬性問卷調查表－（自然人版） -->
							<div class="ttb-message">
								<p><spring:message code="LB.W1218" /></p>
							</div>
<!-- 							倘您不清楚本風險屬性問卷之內容，應不填寫，另可洽本行人員說明。 -->
							<p class="form-description"><spring:message code="LB.W1219" /></p>
							<div class="text-left">
								<!-- 您的年齡為何？（本題免勾選） -->
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>1﹒<spring:message code="LB.W1220" /></p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												<input type="hidden" value="${Q1A}" name="Q1">${age}<spring:message code="LB.W1221" />
											</p>
										</div>
									</li>
								</ul>
								<!-- 您的教育程度為何？ -->
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>2﹒<spring:message code="LB.W1222" /></p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												<label class="radio-block">
													<input type="radio" value="A" name="Q2"/>A﹒ <spring:message code="LB.D0588" />
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="B" name="Q2"/>B﹒ <spring:message code="LB.D0589" />
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="C" name="Q2"/>C﹒ <spring:message code="LB.D0590" />
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="D" name="Q2"/>D﹒ <spring:message code="LB.D0591" />
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="E" name="Q2"/>E﹒ <spring:message code="LB.D0592" />
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="F" name="Q2"/>F﹒ <spring:message code="LB.D0866" />
													<span class="ttb-radio"></span>
												</label>
<!-- 												不在畫面上顯示的span -->
<!-- 												<span class="hideblock"> -->
<!-- 													驗證用的input -->
<%-- 													<input id="validate_Q2" name="fund_invest_attr2" type="radio" class=" --%>
<%-- 														validate[funcCall[validate_Radio['<spring:message code="LB.X1299" />',Q2]]]" --%>
<!-- 														style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
<!-- 												</span> -->
											</p>
										</div>
									</li>
								</ul>
<!-- 								您的職業為何？ -->
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>3﹒<spring:message code="LB.W1229" /></p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												<div class="ttb-input">
													<input type="text" id="SRCFUND" name="SRCFUND" style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
													<label><input id="SRCFUNDDESC" name="SRCFUNDDESC" class="text-input" type="text" size="30" value="<spring:message code="LB.W1230" />" disabled/></label>
													<input type="button" value="<spring:message code="LB.Menu" />" class="ttb-sm-btn btn-flat-gray" id="CAREERNO" name="CAREERNO"/>
												</div>
												<div class="ttb-input">
													<label class="radio-block">
														<input type="radio" value="A" name="Q3" disabled/>A﹒ <spring:message code="LB.D0900" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="B" name="Q3" disabled/>B﹒ <spring:message code="LB.D0901" />
														<span class="ttb-radio"></span>
													</label>
													<label class="radio-block">
														<input type="radio" value="C" name="Q3" disabled/>C﹒ <spring:message code="LB.D0902" />
														<span class="ttb-radio"></span>
													</label>
												</div>
											</p>
										</div>
									</li>
								</ul>
<!-- 								您個人／家庭年收入為（新臺幣） -->
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>4﹒<spring:message code="LB.D0903" />？</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<div class="ttb-input">
												<input type="text" class="text-input" id="newsalary" name="newsalary" size="7" maxlength="5" onblur="switchq4()"/>&nbsp;
												<spring:message code="LB.D0088_2" />
											</div>
											<div class="ttb-input">
												<label class="radio-block">
													<input type="radio" value="A" name="Q4" disabled/>A﹒ <spring:message code="LB.D0906_1" />
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="B" name="Q4" disabled/>B﹒ <spring:message code="LB.D0906_2" />
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="C" name="Q4" disabled/>C﹒ <spring:message code="LB.D0906_3" />
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="D" name="Q4" disabled/>D﹒ <spring:message code="LB.D0906_4" />
													<span class="ttb-radio"></span>
												</label>
											</div>
<!-- 											不在畫面上顯示的span -->
<!-- 											<span class="hideblock"> -->
<!-- 												驗證用的input -->
<%-- 												<input id="validate_Q4" name="validate_Q4" type="text" class="text-input validate[required,min[1],max[999999],funcCall[validate_CheckNumber['<spring:message code="LB.X1299" />',newsalary]]]" --%>
<!-- 													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
<%-- 												<input id="validate_Q4_2" name="validate_Q4_2" type="text" class="text-input validate[funcCallRequired[validate_CheckAmount['<spring:message code= "LB.D0625_1" />',newsalary]]" --%>
<!-- 													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
<!-- 											</span> -->
										</div>
									</li>
								</ul>
<!-- 								您個人所得與資金來源 -->
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>5﹒<spring:message code="LB.D0907" />？</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<div class="ttb-input">
												<label class="radio-block">
													<input type="radio" value="A" name="Q5"/>A﹒ <spring:message code="LB.D0908" />
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="B" name="Q5"/>B﹒ <spring:message code="LB.D0909" />
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="C" name="Q5"/>C﹒ <spring:message code="LB.D0910" />
													<span class="ttb-radio"></span>
												</label>
												<!-- 不在畫面上顯示的span -->
<!-- 												<span class="hideblock"> -->
<!-- 													驗證用的input -->
<%-- 													<input id="validate_Q5" name="Q5" type="radio" class=" --%>
<%-- 														validate[funcCall[validate_Radio['<spring:message code="LB.X1303" />',Q5]]]" --%>
<!-- 														style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
<!-- 												</span> -->
											</div>
										</div>
									</li>
								</ul>
								<!--您目前擁有之有價證券與存款合計數為（新臺幣） -->
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>6﹒<spring:message code="LB.D0911" />？</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<div class="ttb-input">
												<label class="radio-block">
													<input type="radio" value="A" name="Q6"/>A﹒ <spring:message code="LB.D0906_1" />
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="B" name="Q6"/>B﹒ <spring:message code="LB.W1247" />
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="C" name="Q6"/>C﹒ <spring:message code="LB.W1248" />
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="D" name="Q6"/>D﹒ <spring:message code="LB.W1249" />
													<span class="ttb-radio"></span>
												</label>
												<!-- 不在畫面上顯示的span -->
<!-- 												<span class="hideblock"> -->
<!-- 													驗證用的input -->
<%-- 													<input id="validate_Q6" name="Q6" type="radio" class=" --%>
<%-- 														validate[funcCall[validate_Radio['<spring:message code="LB.X1304" />',Q6]]]" --%>
<!-- 														style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
<!-- 												</span> -->
											</div>
										</div>
									</li>
								</ul>
								<!--您投資的主要目的與需求為？ -->
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>7﹒<spring:message code="LB.D0912" /></p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<div class="ttb-input">
												<label class="radio-block">
													<input type="radio" value="A" name="Q7"/>A﹒ <spring:message code="LB.D0913" />
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="B" name="Q7"/>B﹒ <spring:message code="LB.D0914" />
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="C" name="Q7"/>C﹒ <spring:message code="LB.D0915" />
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="D" name="Q7"/>D﹒ <spring:message code="LB.D0916" />
													<span class="ttb-radio"></span>
												</label>
												<!-- 不在畫面上顯示的span -->
<!-- 												<span class="hideblock"> -->
<!-- 													驗證用的input -->
<%-- 													<input id="validate_Q7" name="Q7" type="radio" class=" --%>
<%-- 														validate[funcCall[validate_Radio['<spring:message code="LB.X1305" />',Q7]]]" --%>
<!-- 														style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
<!-- 												</span> -->
											</div>
										</div>
									</li>
								</ul>
<!-- 								您計劃從何時開始提領您投資的部份金額﹖（現金流量期望） -->
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>8﹒<spring:message code="LB.D0917" /></p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<div class="ttb-input">
												<label class="radio-block">
													<input type="radio" value="A" name="Q8"/>A﹒ <spring:message code="LB.W1256" />
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="B" name="Q8"/>B﹒ <spring:message code="LB.W1257" />
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="C" name="Q8"/>C﹒ <spring:message code="LB.W1258" />
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="D" name="Q8"/>D﹒ <spring:message code="LB.W1259" />
													<span class="ttb-radio"></span>
												</label>
												<!-- 不在畫面上顯示的span -->
<!-- 												<span class="hideblock"> -->
<!-- 													驗證用的input -->
<%-- 													<input id="validate_Q8" name="Q8" type="radio" class=" --%>
<%-- 														validate[funcCall[validate_Radio['<spring:message code="LB.X1306" />',Q8]]]" --%>
<!-- 														style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
<!-- 												</span> -->
											</div>
										</div>
									</li>
								</ul>
								<!-- 您對投資之期望報酬率為？ -->
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>9﹒<spring:message code="LB.W1260" /></p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<div class="ttb-input">
												<label class="radio-block">
													<input type="radio" value="A" name="Q9"/>A﹒ 0％～5％
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="B" name="Q9"/>B﹒ 6％～10％
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="C" name="Q9"/>C﹒  11％～20％
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="D" name="Q9"/>D﹒ <spring:message code="LB.X2503" /> 20％
													<span class="ttb-radio"></span>
												</label>
												<!-- 不在畫面上顯示的span -->
<!-- 												<span class="hideblock"> -->
<!-- 													驗證用的input -->
<%-- 													<input id="validate_Q9" name="Q9" type="radio" class=" --%>
<%-- 														validate[funcCall[validate_Radio['<spring:message code="LB.X1307" />',Q9]]]" --%>
<!-- 														style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
<!-- 												</span> -->
											</div>
										</div>
									</li>
								</ul>
<!-- 								您投資金融商品預計投資期限多長？ -->
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>10﹒<spring:message code="LB.W1261" /></p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<div class="ttb-input">
												<label class="radio-block">
													<input type="radio" value="A" name="Q10"/>A﹒ <spring:message code="LB.W1256" />
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="B" name="Q10"/>B﹒ <spring:message code="LB.W1257" />
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="C" name="Q10"/>C﹒ <spring:message code="LB.W1262" />
													<span class="ttb-radio"></span>
												</label>
												<!-- 不在畫面上顯示的span -->
<!-- 												<span class="hideblock"> -->
<!-- 													驗證用的input -->
<%-- 													<input id="validate_Q10" name="Q10" type="radio" class=" --%>
<%-- 														validate[funcCall[validate_Radio['<spring:message code="LB.X1308" />',Q10]]]" --%>
<!-- 														style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
<!-- 												</span> -->
											</div>
										</div>
									</li>
								</ul>
								
							<!--您過去的投資經驗？（本題可複選，取得分最高者計算） -->
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>11﹒<spring:message code="LB.D0920" /></p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<div class="ttb-input">
												<label class="check-block">
													<input type="checkbox" value="A" id="Q111" name="Q111" />
														A﹒ <spring:message code="LB.D0921" />
													<span class="ttb-check"></span>
												</label>
											</div>
											<div class="ttb-input">
												<label class="check-block">
													<input type="checkbox" value="B" id="Q112" name="Q112"/>
														B﹒ <spring:message code="LB.D0922" />
													<span class="ttb-check"></span>
												</label>
											</div>
											<div class="ttb-input">
												<label class="check-block">
													<input type="checkbox" value="C" id="Q113" name="Q113"/>
														C﹒ <spring:message code="LB.D0923" />
													<span class="ttb-check"></span>
												</label>
											</div>
											<div class="ttb-input">
												<label class="check-block">
													<input type="checkbox" value="D" id="Q114" name="Q114" onclick="checkQ11()"/>
														D﹒ <spring:message code="LB.D0924" />
													<span class="ttb-check"></span>
												</label>
											</div>
										</div>
									</li>
								</ul>
<!-- 								請問您從事投資理財之時間？（投資時間） -->
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>12﹒<spring:message code="LB.D0925" /></p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<div class="ttb-input">
												<label class="radio-block">
													<input type="radio" value="A" name="Q12"/>A﹒ <spring:message code="LB.W1256" />
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="B" name="Q12"/>B﹒ <spring:message code="LB.W1257" />
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="C" name="Q12"/>C﹒ <spring:message code="LB.W1258" />
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="D" name="Q12"/>D﹒ <spring:message code="LB.W1259" />	
													<span class="ttb-radio"></span>
												</label>	
												<!-- 不在畫面上顯示的span -->
<!-- 												<span class="hideblock"> -->
<!-- 													驗證用的input -->
<%-- 													<input id="validate_Q12" name="Q12" type="radio" class=" --%>
<%-- 														validate[funcCall[validate_Radio['<spring:message code="LB.X1309" />',Q12]]]" --%>
<!-- 														style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
<!-- 												</span> -->
											</div>
										</div>
									</li>
								</ul>
								<!-- 對金融商品的認識 -->
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>13﹒<spring:message code="LB.D0926" /></p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<div class="ttb-input">
												<label class="check-block">
													<input type="checkbox" value="A" id="Q131" name="Q131" />
														A﹒ <spring:message code="LB.D0927" />
													<span class="ttb-check"></span>
												</label>
											</div>
											<div class="ttb-input">
												<label class="check-block">
													<input type="checkbox" value="B" id="Q132" name="Q132"/>
														B﹒ <spring:message code="LB.D0928" />
													<span class="ttb-check"></span>
												</label>
											</div>
											<div class="ttb-input">
												<label class="check-block">
													<input type="checkbox" value="C" id="Q133" name="Q133"/>
														C﹒ <spring:message code="LB.D0929" />
													<span class="ttb-check"></span>
												</label>
											</div>
											<div class="ttb-input">
												<label class="check-block">
													<input type="checkbox" value="D" id="Q134" name="Q134" onclick="checkQ13()"/>
														D﹒ <spring:message code="LB.D0930" />
													<span class="ttb-check"></span>
												</label>
											</div>
										</div>
									</li>
								</ul>
								<!--您可承受的投資損失風險波動範圍？ -->
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>14﹒<spring:message code="LB.D0931" /></p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<div class="ttb-input">
												<label class="radio-block">
													<input type="radio" value="A" name="Q14"/>A﹒ <spring:message code="LB.D0932" />
													<span class="ttb-radio"></span>
												</label>
											</div>
											<div class="ttb-input">
												<label class="radio-block">
													<input type="radio" value="B" name="Q14"/>B﹒ <spring:message code="LB.D0933" />
													<span class="ttb-radio"></span>
												</label>
											</div>
											<div class="ttb-input">
												<label class="radio-block">
													<input type="radio" value="C" name="Q14"/>C﹒ <spring:message code="LB.D0934" />
													<span class="ttb-radio"></span>
												</label>
											</div>
											<div class="ttb-input">
												<label class="radio-block">
													<input type="radio" value="D" name="Q14"/>D﹒ <spring:message code="LB.D0935" />
													<span class="ttb-radio"></span>
												</label>
											</div>
											<div class="ttb-input">
												<label class="radio-block">
													<input type="radio" value="E" name="Q14"/>E﹒ <spring:message code="LB.X2503" /> 20％
													<span class="ttb-radio"></span>
												</label>
											</div>
												<!-- 不在畫面上顯示的span -->
<!-- 											<span class="hideblock"> -->
<!-- 												驗證用的input -->
<%-- 												<input id="validate_Q14" name="Q14" type="radio" class=" --%>
<%-- 													validate[funcCall[validate_Radio['<spring:message code="LB.X1310" />',Q14]]]" --%>
<!-- 													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
<!-- 											</span> -->
										</div>
									</li>
								</ul>
								<!--您偏好以下列何種商品做為您投資理財配置 -->
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>15﹒<spring:message code="LB.D0936" /></p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<div class="ttb-input">
												<label class="radio-block">
													<input type="radio" value="A" name="Q15"/>A﹒ <spring:message code="LB.D0937" />
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="B" name="Q15"/>B﹒ <spring:message code="LB.D0938" />
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="C" name="Q15"/>C﹒ <spring:message code="LB.D0939" />
													<span class="ttb-radio"></span>
												</label>
												<!-- 不在畫面上顯示的span -->
<!-- 												<span class="hideblock"> -->
<!-- 													驗證用的input -->
<%-- 													<input id="validate_Q15" name="Q15" type="radio" class=" --%>
<%-- 														validate[funcCall[validate_Radio['<spring:message code="LB.X1311" />',Q15]]]" --%>
<!-- 														style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
<!-- 												</span> -->
											</div>
										</div>
									</li>
								</ul>
								<!--是否領有全民健康保險重大傷病證明 -->
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p><spring:message code="LB.D0940_1" />？</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<div class="ttb-input">
												<label class="radio-block">
													<input type="radio" name="MARK1" value="Y"/><spring:message code="LB.D0034_2" />
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" name="MARK1" value="N"/><spring:message code="LB.D0034_3" />
													<span class="ttb-radio"></span>
												</label>
											</div>
										</div>
									</li>
								</ul>
							</div>
							<c:if test="${sessionScope.n361 == 1}">
								<!-- 上一步 -->
	                  			<input type="button" id="previous" value="<spring:message code="LB.X0318" />" class="ttb-button btn-flat-orange"/>
							</c:if>
	                 		<input type="button" id="CMSUBMIT" value="<spring:message code="LB.Confirm"/>" class="ttb-button btn-flat-orange"/>		
						</div>
					</div>
				</form>
			</section>
		</main>
	</div>
	<div id="careerDialog" title="<spring:message code="LB.D0870" />">
		<div style="text-align:center">
			<table id="careerTable" class="stripe table-striped ttb-table dtable" data-toggle-column="first">
                <thead>
					<tr>
						<th><spring:message code="LB.D0868" /></th>
						<th><spring:message code="LB.D0869" /></th>
					</tr>
				</thead>
                <tbody>
					<tr onclick="fillData('061100','<spring:message code= "LB.D0871" />')">
						<td><a href="#!">061100</a></td>
						<td><spring:message code="LB.D0871" /></td>
					</tr>	
					<tr onclick="fillData('061200','<spring:message code= "LB.D0872" />')">
						<td><a href="#!">061200</a></td>
						<td><spring:message code="LB.D0872" /></td>
					</tr>
					<tr onclick="fillData('061300','<spring:message code= "LB.D0873" />')">
						<td><a href="#!">061300</a></td>
						<td><spring:message code="LB.D0873" /></td>
					</tr>	  		  
					<tr onclick="fillData('061400','<spring:message code= "LB.D0874" />')">
						<td><a href="#!">061400</a></td>
						<td><spring:message code="LB.D0874" /></td>
					</tr>
					<tr onclick="fillData('061410','<spring:message code= "LB.D0081" />')">
						<td><a href="#!">061410</a></td>
						<td><spring:message code="LB.D0081" /></td>
					</tr>
					<tr onclick="fillData('061500','<spring:message code= "LB.D0876" />')">
						<td><a href="#!">061500</a></td>
						<td><spring:message code="LB.D0876" /></td>
					</tr>
					<tr onclick="fillData('0615A0','<spring:message code= "LB.D0877" />')">
						<td><a href="#!">0615A0</a></td>
						<td><spring:message code="LB.D0877" /></td>
					</tr>
					<tr onclick="fillData('0615B0','<spring:message code= "LB.D0878" />')">
						<td><a href="#!">0615B0</a></td>
						<td><spring:message code="LB.D0878" /></td>
					</tr>
					<tr onclick="fillData('0615C0','<spring:message code= "LB.D0879" />')">
						<td><a href="#!">0615C0</a></td>
						<td><spring:message code="LB.D0879" /></td>
					</tr>
					<tr onclick="fillData('0615D0','<spring:message code= "LB.D0880" />')">
						<td><a href="#!">0615D0</a></td>
						<td><spring:message code="LB.D0880" /></td>
					</tr>
					<tr onclick="fillData('0615E0','<spring:message code= "LB.D0881" />')">
						<td><a href="#!">0615E0</a></td>
						<td><spring:message code="LB.D0881" /></td>
					</tr>
					<tr onclick="fillData('0615F0','<spring:message code= "LB.D0882" />')">
						<td><a href="#!">0615F0</a></td>
						<td><spring:message code="LB.D0882" /></td>
					</tr>
					<tr onclick="fillData('0615G0','<spring:message code= "LB.D0883" />')">
						<td><a href="#!">0615G0</a></td>
						<td><spring:message code="LB.D0883" /></td>
					</tr>
					<tr onclick="fillData('0615H0','<spring:message code= "LB.D0884" />')">
						<td><a href="#!">0615H0</a></td>
						<td><spring:message code="LB.D0884" /></td>
					</tr>
					<tr onclick="fillData('0615I0','<spring:message code= "LB.D0885" />')">
						<td><a href="#!">0615I0</a></td>
						<td><spring:message code="LB.D0885" /></td>
					</tr>
					<tr onclick="fillData('0615J0','<spring:message code= "LB.D0886" />')">
						<td><a href="#!">0615J0</a></td>
						<td><spring:message code="LB.D0886" /></td>
					</tr>
					<tr onclick="fillData('061610','<spring:message code= "LB.D0887" />')">
						<td><a href="#!">061610</a></td>
						<td><spring:message code="LB.D0887" /></td>
					</tr>
					<tr onclick="fillData('061620','<spring:message code= "LB.D0888" />')">
						<td><a href="#!">061620</a></td>
						<td><spring:message code="LB.D0888" /></td>
					</tr>
					<tr onclick="fillData('061630','<spring:message code= "LB.D0889" />')">
						<td><a href="#!">061630</a></td>
						<td><spring:message code="LB.D0889" /></td>
					</tr>
					<tr onclick="fillData('061640','<spring:message code= "LB.D0890" />')">
						<td><a href="#!">061640</a></td>
						<td><spring:message code="LB.D0890" /></td>
					</tr>
					<tr onclick="fillData('061650','<spring:message code= "LB.D0891" />')">
						<td><a href="#!">061650</a></td>
						<td><spring:message code="LB.D0891" /></td>
					</tr>
					<tr onclick="fillData('061660','<spring:message code= "LB.D0892" />')">
						<td><a href="#!">061660</a></td>
						<td><spring:message code="LB.D0892" /></td>
					</tr>
					<tr onclick="fillData('061670','<spring:message code= "LB.D0893" />')">
						<td><a href="#!">061670</a></td>
						<td><spring:message code="LB.D0893" /></td>
					</tr>
					<tr onclick="fillData('061680','<spring:message code= "LB.D0894" />')">
						<td><a href="#!">061680</a></td>
						<td><spring:message code="LB.D0894" /></td>
					</tr>
					<tr onclick="fillData('061690','<spring:message code= "LB.D0082" />')">
						<td><a href="#!">061690</a></td>
						<td><spring:message code="LB.D0082" /></td>
					</tr>
					<tr onclick="fillData('061691','<spring:message code= "LB.D0083" />')">
						<td><a href="#!">061691</a></td>
						<td><spring:message code="LB.D0083" /></td>
					</tr>
					<tr onclick="fillData('061692','<spring:message code= "LB.D0897" />')">
						<td><a href="#!">061692</a></td>
						<td><spring:message code="LB.D0897" /></td>
					</tr>
					<tr onclick="fillData('061700','<spring:message code= "LB.X1323" />')">
						<td><a href="#!">061700</a></td>
						<td><spring:message code="LB.D0898" /></td>
					</tr>
					<tr onclick="fillData('069999','<spring:message code= "LB.D0899" />')">
						<td><a href="#!">069999</a></td>
						<td><spring:message code="LB.D0899" /></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>
