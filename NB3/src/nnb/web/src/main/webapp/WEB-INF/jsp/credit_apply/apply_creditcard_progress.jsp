<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:if test="${not empty BaseResult }">
	<c:set var="bsData" value="${BaseResult.data}"></c:set>
</c:if>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()",10);
		// 開始查詢資料並完成畫面
		setTimeout("init()",20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)",500);
		setTimeout("initDataTable()",100);
	});
	
	//列印
	function init(){
		// 將.table變更為footable
		//initFootable();
		
		$("#printbtn").click(function(){
			var params = {
					jspTemplateName:	"apply_creditcard_progress_print",
					jspTitle:			"<spring:message code="LB.X1421"/>",
					CMQTIME: 			"${bsData.CMQTIME}",
					COUNT: 				"${bsData.CMRECNUM}"
				};
			openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
		});
	}
	
	// 閱覽申請書
	function openApplication(rcvno)
	{
		console.log(rcvno);
		var url = "${__ctx}" + "${openApplicationUrl}";
		var params = {
			"rcvno": rcvno
		};
		// 在新分頁開啟申請書
		openWindowWithPost(url, "", params);
	}
	
	
</script>
</head>
	<body>
		<!-- header -->
		<header>
			<%@ include file="../index/header.jsp"%>
		</header>
	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 申請信用卡進度查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0122" /></li>
		</ol>
	</nav>

		<!-- menu、登出窗格 -->
		<div class="content row">
			<%@ include file="../index/menu.jsp"%>
			<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2><spring:message code="LB.D0122" /></h2><!-- 申請信用卡進度查詢 -->
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post">
					<div class="main-content-block row">
						<div class="col-12">
							<ul class="ttb-result-list">
								<li>
									<!-- 查詢時間 -->
									<h3><spring:message code="LB.Inquiry_time" /></h3>
									<p>
										
										${bsData.CMQTIME}
									</p>
								</li>
								<li>
									<!-- 資料總數 -->
									<h3><spring:message code="LB.Total_records" /></h3>
									<p>
										
										${bsData.CMRECNUM}&nbsp;<spring:message code="LB.Rows"/>
									</p>
								</li>
							</ul>
							<!-- 申請信用卡進度查詢 -->
							<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
								<thead>
									<tr>
										<!-- 案件編號 -->
										<th><spring:message code="LB.Loan_CaseNo"/></th>
										<!-- 卡片1名稱-->
										<th data-breakpoints="sm xs"><spring:message code="LB.X0850" />1<spring:message code="LB.X0059" /></th>
										<!-- 卡片2名稱 -->
										<th data-breakpoints="sm xs"><spring:message code="LB.X0850" />2<spring:message code="LB.X0059" /></th>
										<!-- 卡片3名稱-->
										<th data-breakpoints="sm xs"><spring:message code="LB.X0850" />3<spring:message code="LB.X0059" /></th>
										<!-- 申請日期 -->
										<th data-breakpoints="xs"><spring:message code="LB.D0127" /><!-- 申請日期 --></th>
										<!-- 審核狀況-->
										<th><spring:message code="LB.D0128" /><!-- 審核狀況--></th>
										<!-- 申請書閱覽 -->
										<th><spring:message code="LB.D0129" /><!-- 申請書閱覽 --></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="row" items="${bsData.REC}">
										<tr>
											<td class="text-center">${row.RCVNO}</td>
											<td class="text-center">${row.CARDNAME1}</td>
											<td class="text-center">${row.CARDNAME2}</td>
											<td class="text-center">${row.CARDNAME3}</td>
											<td class="text-center">${row.LASTDATE}</td>
											<td class="text-center">
												<c:if test="${!row.STATUS.equals('')}">
													<spring:message code="${row.STATUS}"/>
												</c:if>
											</td>
											<td class="text-center">
												<input type="button" class="ttb-sm-btn btn-flat-orange" value="<spring:message code= "LB.X1239" />" onClick="openApplication('${row.RCVNO}')"/>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
							<!-- 列印鈕-->					
							<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>	
						</div>
					</div>
				</form>
				</section>
			</main>
			<!-- main-content END -->
		</div>
		<!-- content row END -->
		<%@ include file="../index/footer.jsp"%>
	</body>
</html>