<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>
<script type="text/javascript" src="${__ctx}/js/jquery.maskedinput.js"></script>
<!-- DIALOG會用到 -->
<script type="text/javascript" src="${__ctx}/js/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery-ui.css">

<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		// HTML載入完成後開始遮罩
	// 	setTimeout("initBlockUI()", 10);
		initBlockUI();
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
		
	function init(){
		$("#formId").validationEngine({
			binded: false,
			promptPosition: "inline"
		});
		$("#back").val("");
		
		$("#CMSUBMIT").click(function(e){
			
			e = e || window.event;
			if(!$('#formId').validationEngine('validate')){
	    		e.preventDefault();
	    	}
			else{
				$("#formId").attr("action","${__ctx}/ONLINE/APPLY/creditcard_limit_increase_result");
				$("#formId").submit();
			}
		});
		
		$("#CMBACK").click(function(e){
			$("#formId").validationEngine('detach');
			$("#back").val("Y");
			$("#formId").attr("action","${__ctx}/ONLINE/APPLY/creditcard_limit_increase_back");
			$("#formId").submit();
		});
		
	}
	
	function checkitem(){
		if($("#CheckBox1").prop("checked") == true){
			$('#CMSUBMIT').removeAttr("disabled");
			$('#CMSUBMIT').removeClass('btn-flat-gray');
			$('#CMSUBMIT').addClass('btn-flat-orange');	
		}
		else{
			$('#CMSUBMIT').attr("disabled",true);
			$('#CMSUBMIT').removeClass('btn-flat-orange');
			$('#CMSUBMIT').addClass('btn-flat-gray');
		}
	}
	
</script>
</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Credit_Card_Limit_Increase" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2><spring:message code="LB.Credit_Card_Limit_Increase" /></h2>
				<div id="step-bar">
                    <ul>
                        <li class="finished"><spring:message code="LB.D0851" /></li><!-- 身份驗證 -->
                        <li class="finished"><spring:message code="LB.SMS_Verification" /></li>
                        <li class="finished"><spring:message code="LB.X1967" /></li><!-- 申請資料 -->
                        <li class="active"><spring:message code="LB.Confirm_data" /></li><!-- 確認資料 -->
                        <li class=""><spring:message code="LB.X1968" /></li><!-- 完成申請 -->
                    </ul>
                </div>
				<form method="post" id="formId" action="">
					<input type="hidden" name="ADOPID" id="ADOPID" value="N205"/>
					<input type="hidden" name="CUSIDN" id="CUSIDN" value=""/>
					<input type="hidden" name="back" id="back" value=""/>
					<input type="hidden" name="Oday" id="Oday" value="${Oday}"/>
					<input type="hidden" name="REASON" id="REASON" value="${limit_increase.data.REASON}">
					<input type="hidden" name="FGTXSTATUS" id="FGTXSTATUS" value="${limit_increase.data.FGTXSTATUS}">
					<input type="hidden" name="QUOTA" id="QUOTA" value="${limit_increase.data.QUOTA}">
					<input type="hidden" name="RESULT" id="RESULT" value="${limit_increase.data.RESULT}">
					<input type="hidden" id="FROM_NB3" name="FROM_NB3" value="${FROM_NB3}"/>
					<!-- 表單顯示區  -->
					<div class="main-content-block pl-0 pr-0 row">
						<div class="col-12">
							<div class="ttb-input-block">
								<div class="ttb-message">
									<p><spring:message code="LB.Confirm_data" /></p>
								</div>
							
								<div class="classification-block">
		                        	<!-- 申請資料 -->
		                        	<p><spring:message code="LB.X1967"/></p>
		                        </div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.D0049" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${limit_increase.data.NAMEshow}
										</div>
									</span>
								</div>
							
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.D0581" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${limit_increase.data.CUSIDNshow}
										</div>
									</span>
								</div>
							
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.D1610" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${limit_increase.data.QUOTA_VIEW}
											<span class="input-unit m-0"><spring:message code='LB.D0509' /></span>
										</div>
									</span>
								</div>
							
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.D1604" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${limit_increase.data.REASON_VIEW}
											<c:if test="${limit_increase.data.REASON == '9'}">
												${limit_increase.data.REASONIN}
											</c:if>
										</div>
									</span>
								</div>
							
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.D1606" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${limit_increase.data.FGTXSTATUS_VIEW}
										</div>
									</span>
								</div>
								
								<c:if test="${limit_increase.data.RESULT != 'Y11' && limit_increase.data.RESULT != 'Y21'}">
									<div class="classification-block">
			                        	<!-- 財力證明文件 -->
			                        	<p><spring:message code="LB.X2024"/></p>
			                        </div>
			                        <div class="photo-block" style="justify-content: flex-start; margin-left: 1rem;">
			                        	<ol style="list-style-type: decimal;">
			                        		<li>
			                        			<c:if test="${ !limit_increase.data.FILE3.equals('') }">
			                        				<input type="text" class="text-input" style="width:250px" readonly id="msgPic3" value="${limit_increase.data.FILE3}" disabled />
												</c:if>
												<c:if test="${ limit_increase.data.FILE3.equals('') }">
													<input type="text" class="text-input" style="width:250px" readonly value="" disabled />
												</c:if>
												<input type="hidden" name="FILE3" value="${limit_increase.data.FILE3}">
				                                <p></p>
			                        		</li>
			                        		<li>
			                        			<c:if test="${ !limit_increase.data.FILE4.equals('') }">
													<input type="text" class="text-input" style="width:250px" readonly id="msgPic4" value="${limit_increase.data.FILE4}" disabled />
												</c:if>
												<c:if test="${ limit_increase.data.FILE4.equals('') }">
													<input type="text" class="text-input" style="width:250px" readonly value="" disabled />
												</c:if>
												<input type="hidden" name="FILE4" value="${limit_increase.data.FILE4}">
				                                <p></p>
			                        		</li>
			                        		<li>
			                        			<c:if test="${ !limit_increase.data.FILE5.equals('') }">
													<input type="text" class="text-input" style="width:250px" readonly id="msgPic5" value="${limit_increase.data.FILE5}" disabled />
												</c:if>
												<c:if test="${ limit_increase.data.FILE5.equals('') }">
													<input type="text" class="text-input" style="width:250px" readonly value="" disabled />
												</c:if>
												<input type="hidden" name="FILE5" value="${result_data.data.FILE5}">
				                                <p></p>
			                        		</li>
			                        	</ol>	                       		
			                       	</div>
		                       	</c:if>
		                       	
		                       	<div class="ttb-input-item row">
			                       	<div class="ttb-input">
	                                    <label class="check-block" style="font-size: 1rem"><spring:message code="LB.D1611" />
	                                        <input type="checkbox" name="CheckBox1" id="CheckBox1" onclick="checkitem()" class="validate[funcCallRequired[validate_chkClickboxKind[<spring:message code='LB.D1611' />,1,CheckBox1]]]">
	                                        <span class="ttb-check"></span>
		                                    <ul style="list-style: decimal">
												<li style="margin-left: 1.25rem;"><spring:message code="LB.D1612" /></li>
												<li style="margin-left: 1.25rem;"><spring:message code="LB.D1613" /></li>
												<li style="margin-left: 1.25rem;"><spring:message code="LB.D1614" /></li>
											</ul>
	                                    </label>
	                                </div>
                                </div>
							</div>
							<!-- 取消 -->
							<input type="button" name="CMBACK" id="CMBACK" value="<spring:message code="LB.X0318" />" class="ttb-button btn-flat-gray"/>
							<input type="button" id="CMSUBMIT" value="<spring:message code='LB.W1746' />" class="ttb-button btn-flat-gray" disabled/><!-- 下一步 -->
						</div>
					</div>
				</form>
			</section>
		</main>
	</div>
	
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>