<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<!-- 交易機制所需JS -->
<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/tbb_common.css?a=2021082519">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
<!--舊版驗證-->
<script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
<style type="text/css">
	input[type="file"]{
		overflow: hidden;
	}
</style>
<script type="text/javascript">
	$(document).ready(function () {
		init();
	});

    function init() {
    	$('.ttb-unit').attr("style","display:none");
/*
    	$("#CMSUBMIT").click(function(e){
        	$("#formId").validationEngine({binded:false,promptPosition: "inline" });
			if (!$('#formId').validationEngine('validate')) {
				e.preventDefault();
			} else {
				$("#formId").validationEngine('detach');
	        	var action = '${__ctx}/CREDIT/APPLY/upload_creditcard_identity_p2';
				$("form").attr("action", action);
    			$("form").submit();
			}
		});
*/
    }


 	function processQuery(FLG,RCVNO) {

    	$("#formId").validationEngine({binded:false,promptPosition: "inline" });
		if (!$('#formId').validationEngine('validate')) {
			e.preventDefault();
		} else {
			$("#formId").validationEngine('detach');
        	var action = '${__ctx}/CREDIT/APPLY/upload_creditcard_identity_p2';
        	$("#FLG").val(FLG);
        	$("#RCVNO").val(RCVNO);
			$("form").attr("action", action);
			$("form").submit();
		}
  			
		return false; 		
 	} 
    

    </script>
</head>
<body>
<!-- header -->
<header>
	<%@ include file="../index/header_logout.jsp"%>
</header>	
 
	<!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<!--信用卡 -->
			<li class="ttb-breadcrumb-item"><spring:message code= "LB.Credit_Card" /></a></li>
			<!--申請信用卡進度查詢-->
			<li class="ttb-breadcrumb-item"><a href="#"><spring:message code= "LB.onlineapply_creditcard_progress_TITLE" /></a></li>
		</ol>
	</nav>
	
	<div class="content row">
		<!-- 主頁內容  -->
	<main class="col-12">
		<!--主頁內容-->
		<section id="main-content" class="container">
			<h2><spring:message code= "LB.onlineapply_creditcard_progress_TITLE" /></h2>
			<div id="step-bar">
				<ul>
					<li class="finished"><spring:message code="LB.D0851" /></li><!-- 身份驗證 -->
					<li class="finished"><spring:message code="LB.onlineapply_creditcard_progress_OTP" /></li><!-- OTP驗證 -->
					<li class="active"><spring:message code="LB.D0359" /></li><!-- 查詢結果 -->
					<c:if test="${YNFLG == 'Y'}">
						<li class=""><spring:message code="LB.onlineapply_creditcard_progress_UploadDocuments" /></li><!-- 上傳證件 -->
						<li class=""><spring:message code="LB.Confirm_data" /></li><!-- 確認資料 -->
						<li class=""><spring:message code="LB.onlineapply_creditcard_progress_COMPLETE" /></li><!-- 完成補件 -->
					</c:if>
				</ul>
			</div>	
                <form id="formId" method="post">
					<input type="hidden" name="ADOPID" value="NA03"/>
				  	<input type="hidden" name="HLOGINPIN" id="HLOGINPIN" value="">
				  	<input type="hidden" name="HTRANSPIN" id="HTRANSPIN" value="">
					<input type="hidden" name="LMTTYN" id="LMTTYN" value="">	
					<input type="hidden" id="FLG" name="FLG" value="" /> 
					<input type="hidden" id="RCVNO" name="RCVNO" value="" />
				<div class="main-content-block row">
					<div class="col-12">
                           <ul class="ttb-result-list">
								<!-- 查詢時間 -->
								<li>
									<h3> 
										<spring:message code="LB.Inquiry_time" /> :
									</h3>
									<p>
										${time_now}
									</p>
								</li>
								<!-- 資料總數 : -->
								<li>
									<h3>
										<spring:message code="LB.Total_records" /> :
									</h3>
									<p>
										${TOTALCOUNT}&nbsp;<spring:message code="LB.Rows" />
										<!--筆 -->                         
									</p>
								</li>
							</ul>	
							
							
							
							
							
											
					

 							<table class="stripe table-striped ttb-table dtable" data-toggle-column="first" id="datatableid">
 							
								<thead>
					            <tr>
					            	<th data-title='<spring:message code="LB.onlineapply_creditcard_progress_D1"/>'>
					               		<spring:message code="LB.onlineapply_creditcard_progress_D1" />
					                </th>
					            	<th data-title='<spring:message code="LB.onlineapply_creditcard_progress_D2"/>'>
					               		<spring:message code="LB.onlineapply_creditcard_progress_D2" />
					                </th>
					                <th data-title='<spring:message code="LB.onlineapply_creditcard_progress_D5"/>'>
					               		<spring:message code="LB.onlineapply_creditcard_progress_D5" />
					                </th>
					                <th data-title='<spring:message code="LB.onlineapply_creditcard_progress_D6"/>'>
					               		<spring:message code="LB.onlineapply_creditcard_progress_D6" />
					                </th>
					                <th data-title='<spring:message code="LB.onlineapply_creditcard_progress_D7"/>'>
					               		<spring:message code="LB.onlineapply_creditcard_progress_D7" />
					                </th>
					                <c:if test="${YNFLG == 'Y'}">
					                <th data-title='<spring:message code="LB.onlineapply_creditcard_progress_D8"/>'>
					               		<spring:message code="LB.onlineapply_creditcard_progress_D8" />
					                </th>
					                </c:if>
					            </tr>
					            </thead>
					            <tbody>
					            <c:forEach var="dataTable" items="${listProducts}">
					            <tr>
					                <td class="text-center">
										${dataTable.RCVNO}
					                </td>
					                <td class="text-center">
					                	${dataTable.CUSTID}
					                </td>
					                <td  class="text-center">
                                         ${dataTable.RCVDATE}
					                </td>
					                <td  class="text-center">
                                         ${dataTable.DOCSTATUS}
					                </td>
					                <td  class="text-center">
                                         ${dataTable.CENCARD}
					                </td>
					                <c:if test="${YNFLG == 'Y'}">
					                <td  class="text-center">
					                	<c:if test="${dataTable.YNFLG == 'Y' && (dataTable.PFLG != '0' && dataTable.PFLG != '4' ) }">
                                         	<input type="button" class="ttb-sm-btn btn-flat-orange" name="AAA" value="<spring:message code="LB.onlineapply_creditcard_X1938" />" onclick="processQuery('${dataTable.PFLG}','${dataTable.RCVNO}')">
                                        </c:if>
                                        <c:if test="${dataTable.YNFLG == 'Y' && dataTable.PFLG == '4' }">
                                         	超過當日允許補件次數
                                        </c:if>
					                </td>
                                    </c:if>
					            </tr>
					            </c:forEach>
					            </tbody>
							</table>
							
							<div class="text-left">
                                	註：
                                	<ol class="list-decimal text-left">
                                		<li>查詢結果為近六個月內資訊。</li>
										<li>點擊需補件得直接上傳補身分證件或財力證明，請備妥資料拍照上傳或將文件傳真或郵寄。</li>
                                	</ol>
							 </div>
							 <input type="button" id="CMBACK" value="<spring:message code="LB.D0171"/>" class="ttb-button btn-flat-gray" onClick="window.close();"/>
					</div>
				</div>
				
			</form>
		</section>
	</main>
</div>
<%@ include file="../index/footer.jsp"%>
</body>
</html>