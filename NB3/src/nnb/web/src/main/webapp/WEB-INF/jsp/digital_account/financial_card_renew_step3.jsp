<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript"
	src="${__ctx}/js/jquery.datetimepicker.js"></script>
<!-- 變更密碼所需JS --> 
<script type="text/javascript" src="${__ctx}/component/util/cardReaderChangePin2.js"></script>
<script type="text/javascript" src="${__ctx}/component/util/cardReaderChangePin.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		init();
		putDataInit();
	});
	function init(){
		$("#formId").validationEngine({binded: false,promptPosition: "inline"});
		var main = document.getElementById("formId");
	//var CRDTYP ="${financial_card_renew_step3.data.CRDTYP}";
	//var SEND ="${financial_card_renew_step3.data.SEND}";
	//var CChargeApply ="${financial_card_renew_step3.data.CChargeApply}";
	//var NAATAPPLY="${financial_card_renew_step3.data.NAATAPPLY}";
	//var CROSSAPPLY ="${financial_card_renew_step3.data.CROSSAPPLY}";
// 		if(CRDTYP=="1")
//     	{		
//     		main.CRDTYP[0].checked=true;
//     		main.CRDTYP[1].checked=false;
// 		}
// 		else
// 		{
// 			main.CRDTYP[1].checked=true;
// 			main.CRDTYP[0].checked=false;
// 		}
			
// 		if(SEND=="1")
//     	{		
//     		main.SEND[0].checked=true;
//     		main.SEND[1].checked=false;
// 		}
// 		else
// 		{
// 			main.SEND[1].checked=true;
// 			main.SEND[0].checked=false;
// 		}
			
// 		if(CChargeApply =="Y")
//     	{		
//     		main.CChargeApply[0].checked=true;
//     		main.CChargeApply[1].checked=false;
// 		}
// 		else
// 		{
// 			main.CChargeApply[1].checked=true;
// 			main.CChargeApply[0].checked=false;
// 		}
// 		if(NAATAPPLY=="Y")
//     	{		
//     		main.NAATAPPLY[0].checked=true;
//     		main.NAATAPPLY[1].checked=false;
// 		}
// 		else
// 		{
// 			main.NAATAPPLY[1].checked=true;
// 			main.NAATAPPLY[0].checked=false;
// 		}
			
// 		if(CROSSAPPLY=="Y")
//     	{		
//     		main.CROSSAPPLY[0].checked=true;
//     		main.CROSSAPPLY[1].checked=false;
// 		}
// 		else
// 		{
// 			main.CROSSAPPLY[0].checked=true;
// 			main.CROSSAPPLY[1].checked=false;
// 		}
		$("#CChargeApply_Block").css('display','none');
		$("#NAATAPPLY_Block").css('display','none');
		$("#CROSSAPPLY_Block").css('display','none');
		$("#SEND_Block").css('display','none');
		cChargeApplyCheck();
		//showCRD_TEXT();
		openinput()

 }
// 上一頁按鈕
function goBack(){
		// 遮罩
		initBlockUI();
		// 解除表單驗證
		$("#formId").validationEngine('detach');
		// 讓Controller知道是回上一頁
		$('#isBack').val("Y");
		// 回上一頁
		var action = "${__ctx}/DIGITAL/ACCOUNT/financial_card_renew_step2";
		$("#formId").attr("action", action);
		$("#formId").submit();
}
function processQuery()
{
	$("#ACN").val($("#ACNLIST").val());
	var main = document.getElementById("formId");
  	if(main.CChargeApply[0].checked==true){
		if(main.LMT.value>10){
			//alert("<spring:message code="LB.Alert067"/>");
			errorBlock(
					null, 
					null,
					["<spring:message code="LB.Alert067"/>"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
			return false;
		}
		if(main.LMT.value==""){
			//alert("<spring:message code="LB.Alert068"/>");
			errorBlock(
					null, 
					null,
					["<spring:message code="LB.Alert068"/>"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
			return false;
		}
		if(main.LMT.value==0){
			//alert("<spring:message code="LB.Alert069"/>");
			errorBlock(
					null, 
					null,
					["<spring:message code="LB.Alert069"/>"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
			return false;
		}
		
		if(main.LMT.value<0){
			//alert("<spring:message code="LB.Alert070"/>");
			errorBlock(
					null, 
					null,
					["<spring:message code="LB.Alert070"/>"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
			return false;
		}
		var reNumber = /[^0-9]/;
		if (reNumber.test(main.LMT.value))
		{
		    //alert( "<spring:message code="LB.Alert071"/>");
		    errorBlock(
					null, 
					null,
					["<spring:message code="LB.Alert071"/>"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
		    return false;
		}
		
	    
  	}
	//main.CMSUBMIT.disabled = true;
	main.LMT.value=document.getElementById("LMT").value;	
	//進行 validation 並送出
	if (!$('#formId').validationEngine('validate')) {
		//e.preventDefault();
		console.log("can't submit");
		main.CMSUBMIT.disabled = false;
	} else {
		console.log("submit");
		$("#formId").validationEngine('detach');
		main.setAttribute("action", '${__ctx}/DIGITAL/ACCOUNT/financial_card_renew_step4'); 
		main.submit();
	}	 				
} 

function cChargeApplyCheck()
{
	var main = document.getElementById("formId");	
	if(main.CChargeApply[0].checked==true){
		$("#LMTLABLE").show();
// 		main.LMT.style.display='';
// 		document.getElementById("LMTLABLE").style.display='';
		//document.getElementById("LMTLABLE1").style.display='';
		//document.getElementById("LMTLABLE2").style.display='';

// 		main.LMT.value="0";
	} else {
		$("#LMTLABLE").hide();
// 		main.LMT.style.display='none';
// 		 document.getElementById("LMTLABLE").style.display='none';
		 //document.getElementById("LMTLABLE1").style.display='none';
		 //document.getElementById("LMTLABLE2").style.display='none';
// 		main.LMT.value="0";
	}
}

function carTypeClick(){
	
	var check = document.getElementById("CRDTYP_FLAG2");
	if(document.getElementsByName("CRDTYP")[1].checked || document.getElementsByName("CRDTYP")[0].checked){
		$("#CChargeApply_Block").css('display','');
		$("#NAATAPPLY_Block").css('display','');
		$("#CROSSAPPLY_Block").css('display','');
		$("#SEND_Block").css('display','');
	}
	
}

//初始化資料自填
function putDataInit(){
	if('${financial_card_renew_step3.data.CRDTYP}' == '1'){
		$("#CRDTYP_FLAG1").prop('checked',true);
		carTypeClick();
	}else if('${financial_card_renew_step3.data.CRDTYP}' == '2'){
		$("#CRDTYP_FLAG2").prop('checked',true);
		carTypeClick();
	}
	if('${financial_card_renew_step3.data.CChargeApply}' == 'N'){
		$("#CChargeApply_FLAG2").prop('checked',true);
		cChargeApplyCheck();
	}else if('${financial_card_renew_step3.data.CChargeApply}' == 'Y'){
		$("#CChargeApply_FLAG1").prop('checked',true);
		cChargeApplyCheck();
	}
	if('${financial_card_renew_step3.data.NAATAPPLY}' == 'N'){
		$("#NAATAPPLY_FLAG2").prop('checked',true);
	}else if('${financial_card_renew_step3.data.NAATAPPLY}' == 'Y'){
		$("#NAATAPPLY_FLAG1").prop('checked',true);
	}
	if('${financial_card_renew_step3.data.CROSSAPPLY}' == 'N'){
		$("#CROSSAPPLY_FLAG2").prop('checked',true);
	}else if('${financial_card_renew_step3.data.CROSSAPPLY}' == 'Y'){
		$("#CROSSAPPLY_FLAG1").prop('checked',true);
	}
	if('${financial_card_renew_step3.data.SEND}' == '2'){
		$("#SEND_FLAG2").prop('checked',true);
	}else if('${financial_card_renew_step3.data.SEND}' == '1'){
		$("#SEND_FLAG1").prop('checked',true);
	}
}
// function naatApplyCheck()
// {
// 	var main = document.getElementById("formId");	
// 	if(main.NAATAPPLY[0].checked==true){
// 	  document.getElementById("NAATAPPLYLABLE").style.display='';
// 	}
// 	else{
// 	  document.getElementById("NAATAPPLYLABLE").style.display='none';
// 	}
// }
function openinput() 
{
	var main = document.getElementById("formId");
	//var nadd = document.getElementById("NADD");
	//var postcod = document.getElementById("POSTCOD_ID");
	//var miladr = document.getElementById("MILADR_ID");
	if(document.getElementsByName("SEND")[0].checked)
	{		
		//nadd.style.display = "block";
		//postcod.style.display = "";
		//miladr.style.display = "";
	}
	if(document.getElementsByName("SEND")[1].checked)
	{		
		//nadd.style.display = "none";
		//postcod.style.display = "none";
		//miladr.style.display = "none";
		main.ADDRS.checked=false
		main.MILADR.value="";
		main.POSTCOD.value="";		
	}		
}
function ADDRSchecked(){
	var main = document.getElementById("formId");	
	var addr = "${financial_card_renew_step3.data.MILADR}";
	var postc = "${financial_card_renew_step3.data.POSTCOD}";
	if(main.ADDRS.checked=true){
		main.MILADR.value=addr;
		main.POSTCOD.value=postc;
		main.MILADR.disabled=true;
		main.POSTCOD.disabled=true;		
	}
}

function ADDRSunchecked(){
	var main = document.getElementById("formId");	
		main.MILADR.value="";
		main.POSTCOD.value="";
		main.MILADR.disabled=false;
		main.POSTCOD.disabled=false;				

	
}
/*
function showCRD_TEXT(){
	var text = document.getElementById("CRD_TEXT");
	var check = document.getElementById("CRDTYP_FLAG2");
	if(document.getElementsByName("CRDTYP")[1].checked){
		text.style.display='';
	}
	else if (document.getElementsByName("CRDTYP")[0].checked){
		text.style.display='none';
	}
	
}*/

</script>
</head>

<body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Register" /></li>
    <!-- 數位存款帳戶     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0348" /></li>
    <!-- 數位存款帳戶補申請晶片金融卡     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1553" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<!-- 數位存款帳戶補申請晶片金融卡 -->
			<h2>補申請晶片金融卡</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<div id="step-bar">
            	<ul>
                	<li class="finished">身分驗證</li>
                    <li class="finished">顧客權益</li>
                    <li class="active">申請資料</li>
					<li class="">確認資料</li>
					<li class="">完成申請</li>
               </ul>
            </div>
			<form id="formId" method="post" action="">
				<input type="hidden" name="isBack" value="">
				<input type="hidden" value="NB31" name="ADOPID"> 
<%-- 				<input type="hidden" value="${financial_card_renew_step3.data.ACN}" name="ACN">  --%>
				<input type="hidden" value="${financial_card_renew_step3.data.FGTXWAY}" name="FGTXWAY"> 
				<input type="hidden" value="${financial_card_renew_step3.data.CUSIDN}" name="CUSIDN"> 
				<input type="hidden" value="${financial_card_renew_step3.data.UID}" name="UID"> 
				<input type="hidden" value="02" name="TYPE">
				<input type="hidden" name="CARDNAME" id="CARDNAME" value="">
				<input type="hidden" name="BANKID" id="BANKID" value="">
				<input type="hidden" name="ACNNO" id="ACNNO" value="">
				<div class="main-content-block row">
					<div class="col-12 tab-content">
					<!-- 修改客戶資料 -->
<%-- 					<div class="mt-4"> <h3><spring:message code="LB.D1560"/></h3></div> --%>
						<div class="ttb-input-block">
							<div class="ttb-message">
								<p>申請資料</p>
							</div>
							<div class="classification-block">
								<p>晶片金融卡申請</p>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<!-- 是否申請消費扣款(Smart Pay) -->
									<h4>
										數位存款帳戶
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<select class="custom-select select-input half-input" name="ACNLIST" id="ACNLIST" value="">
												<c:forEach var="dataList" items="${financial_card_renew_step3.data.ACNLIST}">
													<option value="${dataList}">${dataList}</option>
												</c:forEach>
										</select>
									</div>
									<input type="hidden" value="" name="ACN" id="ACN"> 
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<!-- 金融卡種類 -->
										<h4>
											申請晶片金融卡
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
<%-- 										<label class="radio-block" ><spring:message code="LB.D1432"/> --%>
<!-- 											<input type="radio" name="CRDTYP"  id="CRDTYP_FLAG1"  value="1" onclick="carTypeClick()"/> -->
<!-- 											<span class="ttb-radio"></span> -->
<!-- 										</label> -->
										<label class="radio-block" ><spring:message code="LB.Financial_debit_card"/>
											<input type="radio" name="CRDTYP"  id="CRDTYP_FLAG2"  value="2" onclick="carTypeClick()"/>
											<span class="ttb-radio"></span>
										</label>
										<span id="CRD_TEXT"class="input-remarks">您申請的晶片金融卡使用範圍包含 ：餘額查詢、存/提款、非約定轉帳、繳費稅、消費扣款及跨國交易等功能。</span>
									</div>
									
									<span class="hideblock">
											<input id="validate_CRDTYP" name="CRDTYP" type="radio"  
												class="validate[funcCall[validate_Radio[申請晶片金融卡 , CRDTYP]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
										</span>
									
								</span>
								
							</div>
							
							<div class="ttb-input-item row" id="CChargeApply_Block">
								<span class="input-title"> 
								<label>
									<!-- 是否申請消費扣款(Smart Pay) -->
									<h4>
										申請消費扣款限額
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block" ><spring:message code="LB.D0034_2"/>
											<input type="radio" name="CChargeApply"  id="CChargeApply_FLAG1" value="Y" onClick="cChargeApplyCheck()" />
											<span class="ttb-radio"></span>
										</label>
										<label class="radio-block"><spring:message code="LB.D0034_3"/>
											<input type="radio" name="CChargeApply"  id="CChargeApply_FLAG2" value="N" onClick="cChargeApplyCheck()"/>
											<span class="ttb-radio"></span>
										</label>
									</div>
									<span class="hideblock">
											<input id="validate_CChargeApply" name="CChargeApply" type="radio"  
												class="validate[funcCall[validate_Radio[申請消費扣款限額 , CChargeApply]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
										</span>
								</span>
							</div>
							<div class="ttb-input-item row" id='LMTLABLE'>
								<span class="input-title">
									<label>每日交易限額</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<input type="text" class="text-input" name="LMT" id="LMT" value="${ financial_card_renew_step3.data.LMT }" maxlength="2" size="11" placeholder="請輸入每日交易限額(新台幣)">
										<span id='LMTLABLE1' class="input-unit"><spring:message code="LB.D0088_2"/></span>
										<label  id='LMTLABLE2'><span class="input-remarks">(每日交易限額以萬元為單位，最高不得超過新台幣10萬元。)</span></label>
									</div>
									
									
								</span>
							</div>
							
						<div class="ttb-input-item row" id="NAATAPPLY_Block">
								<span class="input-title"> 
								<label>
									<!-- 是否申請非約定帳號轉帳 -->
									<h4>
										申請非約定帳號轉帳
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block"><spring:message code="LB.D0034_2"/>
											<input type="radio" name="NAATAPPLY"  id="NAATAPPLY_FLAG1" value="Y" />
											<span class="ttb-radio"></span>
										</label>
										<label class="radio-block"><spring:message code="LB.D0034_3"/>
											<input type="radio" name="NAATAPPLY"   id="NAATAPPLY_FLAG2" value="N" />
											<span class="ttb-radio"></span>
										</label>
										
										<span class="hideblock">
											<input id="validate_NAATAPPLY" name="NAATAPPLY" type="radio"  
												class="validate[funcCall[validate_Radio[申請非約定帳號轉帳 ,NAATAPPLY]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
										</span>
									</div>
								</span>
							</div>
							<div class="ttb-input-item row" id="CROSSAPPLY_Block">
								<span class="input-title"> 
								<label>
									<!-- 是否跨國交易 -->
									<h4>
										申請跨國交易
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block" ><spring:message code="LB.D0034_2"/>
											<input type="radio" name="CROSSAPPLY"   id="CROSSAPPLY_FLAG1" value="Y" />
											<span class="ttb-radio"></span>
										</label>
										<label class="radio-block" ><spring:message code="LB.D0034_3"/>
											<input type="radio" name="CROSSAPPLY"   id="CROSSAPPLY_FLAG2" value="N" />
											<span class="ttb-radio"></span>
										</label>
										
									</div>
									
									<span class="hideblock">
											<input id="validate_CROSSAPPLY" name="CROSSAPPLY" type="radio"  
												class="validate[funcCall[validate_Radio[申請跨國交易 ,CROSSAPPLY]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
										</span>
								</span>
							</div>
							<div class="ttb-input-item row" id="SEND_Block">
								<span class="input-title"> 
								<label>
									<!-- 寄送方式 -->
									<h4>
										領取方式
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block"><spring:message code="LB.D1433"/>
											<!-- 郵寄 -->
											<input type="radio" name="SEND"  id="SEND_FLAG1" value="1" onClick="openinput();"/>
											<span class="ttb-radio"></span>
										</label>
										<label class="radio-block"><spring:message code="LB.D0072"/>
											<input type="radio" name="SEND"  id="SEND_FLAG2" value="2" onClick="openinput();"/>
											<span class="ttb-radio"></span>
										</label>
										<span class="input-remarks">選擇郵寄我們將以雙掛號寄至通訊地址；親領則寄至指定之服務分行。</span>
										
<!-- 										<div id="NADD" style="display:none;"> -->
<%-- 											<label class="check-block"><spring:message code="LB.X0352"/> --%>
<!-- 												<input type="checkbox" name="ADDRS" id="ADDRS" value="Y" onchange="if(this.checked){ADDRSchecked();}else if(!this.checked){ADDRSunchecked();}" /> -->
<!-- 												<span class="ttb-check"></span> -->
<!-- 											</label> -->
<!-- 										</div>	 -->
									</div>
									
									<span class="hideblock">
											<input id="validate_SEND" name="SEND" type="radio"  
												class="validate[funcCall[validate_Radio[領取方式 ,SEND]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
										</span>
								</span>
							</div>
<!-- 							<div class="ttb-input-item row" id='POSTCOD_ID'> -->
<!-- 								<span class="input-title"> -->
<%-- 									<label ><spring:message code="LB.D0061"/></label> --%>
<!-- 								</span> -->
<!-- 								<span class="input-block"> -->
<!-- 									<div class="ttb-input"> -->
<!-- 										<input class="text-input" type="text" name="POSTCOD" maxLength="5" size="5" /> -->
<!-- 									</div> -->
<!-- 								</span> -->
<!-- 							</div> -->
<!-- 							<div class="ttb-input-item row" id='MILADR_ID'> -->
<!-- 								<span class="input-title"> -->
<%-- 									<label ><spring:message code="LB.X0353"/></label> --%>
<!-- 								</span> -->
<!-- 								<span class="input-block"> -->
<!-- 									<div class="ttb-input"> -->
<!-- 										<input class="text-input" type="text" name="MILADR" maxLength="68" size="68" /> -->
<!-- 									</div> -->
<!-- 								</span> -->
<!-- 							</div> -->
						</div>
<%-- 						<input type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.X0194" />" name="CLOSEPAGE" onClick="open(location, '_self').close();" > --%>
						<input type="button" class="ttb-button btn-flat-gray"name="CMBACK" id="CMBACK" value="<spring:message code="LB.X0318" />" onclick="goBack()" />
<%-- 						<spring:message code="LB.Confirm" var="cmSubmit"></spring:message> --%>
						<input type="button" class="ttb-button btn-flat-orange"name="CMSUBMIT" id="CMSUBMIT" value="<spring:message code="LB.X0080" />" onclick="processQuery()" />
					</div>
				</div>
<!-- 				<div class="text-left"> -->
<!-- 					說明： -->
<!-- 					<ol class="description-list list-decimal"> -->
<%-- 							<p><spring:message code="LB.Description_of_page"/></p>					 --%>
<!-- 							使用晶片金融卡製妥後，選擇郵寄則以雙掛號郵件方式寄送至通訊地址；親領則寄送至開立數位存款帳戶時指定之服務分行 -->
<%-- 							<li><spring:message code="LB.Financial_Card_Renew_P3_D1"/></li> --%>
<!-- 							收到晶片金融卡及密碼單時，請您先於線上點選「確認領用申請及變更密碼」，即可使用 -->
<%-- 							<li><spring:message code="LB.Financial_Card_Renew_P3_D2"/></li> --%>
<!-- 							晶片金融卡使用範圍：餘額查詢、存/提款、非約定轉帳、繳費稅、消費扣款及跨國交易功能。 -->
<%-- 							<li><spring:message code="LB.Financial_Card_Renew_P3_D3"/></li> --%>
<!-- 					</ol> -->
<!-- 				</div> -->
			</form>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>

</html>
