<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="page5" class="card-detail-block">
	<div class="card-center-block">
		<!-- Q1 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-51" aria-expanded="true"
				aria-controls="popup1-51">
				<div class="row">
					<span class="col-1">Q1</span>
					<div class="col-11">
						<span>如何確保網路銀行交易之安全?</span>
					</div>
				</div>
			</a>
			<div id="popup1-51" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<ol class="ttb-result-list terms">
								<li>1、網路銀行各類密碼請勿告知他人。</li>
								<li>2、請避免以身分證統一編號、出生日期、公司或住家電話等易遭他人知悉的資料設定密碼。</li>
								<li>3、個人電腦避免Browser設定記憶使用者身分證統一編號及密碼。</li>
								<li>4、請避免利用公共場所之電腦（如網咖）執行網路銀行交易，以免遭不法人士利用鍵盤輸入側錄程式記錄使用者所輸入資料，竊取客戶個人帳號及密碼等私密資料。</li>
								<li>5、請定期變更密碼。</li>
								<li>6、離開座位時，請記得登出網路銀行系統，並關閉瀏覽器。</li>
							</ol>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q2 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-52" aria-expanded="true"
				aria-controls="popup1-52">
				<div class="row">
					<span class="col-1">Q2</span>
					<div class="col-11">
						<span>為什麼每次進入網路銀行網頁作登入動作，會顯示上次輸入資料，是否系統設定有問題，這樣是不是造成電腦記憶ID和密碼，該如何處理？</span>
					</div>
				</div>
			</a>
			<div id="popup1-52" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>
								此為客戶端IE為方便您不用每次輸入，而自動記憶的功能，並非本行網路銀行系統所能控制範圍，為防範您的密碼外流或其他原因，請關閉此功能如下：<br>
								點選IE的工具列中的『工具』→『網際網路選項』→『內容』，在「個人資訊」中有一個『自動完成』的按鈕，後將『表單上使用者名稱和密碼』的選項'取消'並按『清除表單』、『清除密碼』，再按『確定』即可。<font color="red">建議不要使用自動記憶功能，以防ID及密碼外洩</font>。
							</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>