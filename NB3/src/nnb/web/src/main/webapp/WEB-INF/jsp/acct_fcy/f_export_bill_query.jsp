<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<meta http-equiv="Content-Language" content="zh-tw">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css"href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 帳戶查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Account_Inquiry" /></li>
    <!-- 出口押匯查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Export_Bills_Negotiation_Inquiry" /></li>
		</ol>
	</nav>



	<!-- 	快速選單及主頁內容 -->
	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
		<main class="col-12">
		<!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<!-- 出口押匯查詢 -->
			<h2><spring:message code="LB.Export_Bills_Negotiation_Inquiry" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<!-- 下拉式選單-->
						<div class="print-block">
							<select class="minimal" id="actionBar" onchange="formReset()">
								<option value=""><spring:message code="LB.Downloads" /></option>
								<!-- 						下載Excel檔 -->
								<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
								<!-- 						下載為txt檔 -->
								<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
							</select>
						</div>
			<div class="main-content-block row">
				<div class="col-12">
					<form name="main" id="main" method="post"
						action="${__ctx}/FCY/ACCT/export_bill_query_result">
						<!-- 下載用 -->
						<input type="hidden" name="downloadFileName" value="<spring:message code="LB.Export_Bills_Negotiation_Inquiry" />" /> 
						<input type="hidden" name="downloadType" id="downloadType" /> 
						<input type="hidden" name="templatePath" id="templatePath" /> 
						<!-- EXCEL下載用 -->
						<input type="hidden" name="headerRightEnd" value="9" /> 
						<input type="hidden" name="headerBottomEnd" value="8"/>
						<input type="hidden" name="rowStartIndex" value="9"/>
						<input type="hidden" name="rowRightEnd" value="9"/>
						<input type="hidden" name="footerStartIndex" value="11" />
						<input type="hidden" name="footerEndIndex" value="13" />
						<input type="hidden" name="footerRightEnd" value="10" />
						<!-- TXT下載用 -->
						<input type="hidden" name="txtHeaderBottomEnd" value="9" /> 
						<input type="hidden" name="txtHasRowData" value="true" /> 
						<input type="hidden" name="txtHasFooter" value="true" />
						<div class="ttb-input-block">

							<!-- 查詢區間區塊 -->
							<div class="ttb-input-item row">
								<!--查詢區間  -->
								<span class="input-title"> <label> <spring:message code="LB.Inquiry_period" />
								</label>
								</span> <span class="input-block"> <!--  指定日期區塊 -->
									<div class="ttb-input">

										<!--期間起日 -->
										<div class="ttb-input">
										<!-- 押匯起日 -->
											<span class="input-subtitle subtitle-color"> <spring:message code="LB.Negotiation_SDate" /> </span> 
											<input type="text" id="FDATE" name="CMSDATE"
												class="text-input datetimepicker" maxlength="10" value="${export_bill_query.data.TODAY}" />
											<span class="input-unit FDATE"> 
											<img src="${__ctx}/img/icon-7.svg" />
											</span>
											<!-- 不在畫面上顯示的span -->
											<span id="hideblocka"> <!-- 驗證用的input --> 
											<input id="Monthly_DateA" name="Monthly_DateA" type="text"
												class="text-input validate[required, verification_date[Monthly_DateA]]"
												style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;" />
											</span>
										</div>
										<div class="ttb-input">
											<!-- 押匯迄日 -->
											<span class="input-subtitle subtitle-color"> <spring:message code="LB.Negotiation_EDate" /> </span> 
												<input type="text" id="TDATE" name="CMEDATE"
													class="text-input datetimepicker" maxlength="10" value="${export_bill_query.data.TODAY}" />
											<span class="input-unit TDATE"> 
												<img src="${__ctx}/img/icon-7.svg" />
											</span>
											<!-- 不在畫面上顯示的span -->
											<span id="hideblockb"> <!-- 驗證用的input --> 
											<input id="Monthly_DateB" name="Monthly_DateB" type="text"
												class="text-input validate[required, verification_date[Monthly_DateB]]"
												style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;" />
											</span>
											
											<span id="hideblock_CheckDateScope">
												<!--驗證用的input -->
												<input id="odate" name="odate" type="text" value="${export_bill_query.data.TODAY}" class="text-input validate[required,funcCall[validate_CheckDateScope['<spring:message code= "LB.D0008" />',odate , FDATE ,TDATE, false,12,null]]]" style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
									</div>
								</span>
							</div>
							<!-- 信用狀號碼區塊 -->
							<div class="ttb-input-item row">
								<!--信用狀號碼  -->
								<span class="input-title"> <label> <spring:message code="LB.L/C_no" /> </label>

								</span> <input type="text" id="LCNO" name="LCNO" class="text-input"
									maxlength="20" value="" /> &nbsp;&nbsp;&nbsp;&nbsp; <label>
									<spring:message code="LB.Blank_For_ALL" /></label>
							</div>
							<!-- 通知編號區塊 -->
							<div class="ttb-input-item row">
								<!--押匯編號  -->
								<span class="input-title"> <label> <spring:message code="LB.Ref_no" /> </label>
								</span> <input type="text" id="REFNO" name="REFNO" class="text-input"
									maxlength="20" value="" /> &nbsp;&nbsp;&nbsp;&nbsp; <label>
									<spring:message code="LB.Blank_For_ALL" /> </label>
							</div>
						</div>
					</form>
					<input type="button" class="ttb-button btn-flat-orange"	id="CMSUBMIT" value="<spring:message code="LB.Confirm" />" />
				</div>
			</div>
			<ol class="description-list list-decimal">
				<p><spring:message code="LB.Description_of_page" /></p>
					<li><spring:message code="LB.F_Export_Bill_Query_P1_D1" /></li>
					<li><spring:message code="LB.F_Export_Bill_Query_P1_D2" /></li>
				</ol>
			<!-- 				</form> -->
		</section>
		<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->
	<%@ include file="../index/footer.jsp"%>
	<!--   Js function -->
	<script type="text/JavaScript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 50);
		// 開始跑下拉選單並完成畫面
		setTimeout("init()", 400);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	function init(){
		// 初始化時隱藏span
		$("#hideblocka").hide();
		$("#hideblockb").hide();
		$("#hideblock_CheckDateScope").hide();
		
		datetimepickerEvent();
		
		
		initFootable();
		
		$("#main").validationEngine({
			binded: false,
			promptPosition: "inline"
		});	
		$("#CMSUBMIT").click(function(e){
			//打開驗證隱藏欄位
			$("#hideblocka").show();
			$("#hideblockb").show();
			$("#hideblock_CheckDateScope").show();
			//塞值進span內的input
			$("#Monthly_DateA").val($("#FDATE").val());
			$("#Monthly_DateB").val($("#TDATE").val());
			
			
			if(!$('#main').validationEngine('validate')){
				e = e || window.event;
	        	e.preventDefault();
 			}else{
 				$("#main").validationEngine('detach');
 				initBlockUI();
 				$("#main").attr("action","${__ctx}/FCY/ACCT/f_export_bill_query_result");
 	  			$("#main").submit(); 
 			}		
  		});
	}
	
	// 日曆欄位參數設定
	function datetimepickerEvent() {
		$(".FDATE").click(function (event) {
			$('#FDATE').datetimepicker('show');
		});
		$(".TDATE").click(function (event) {
			$('#TDATE').datetimepicker('show');
		});
		jQuery('.datetimepicker').datetimepicker({
			timepicker: false,
			closeOnDateSelect: true,
			scrollMonth: false,
			scrollInput: false,
			format: 'Y/m/d',
			lang: '${transfer}'
		});
	}	
	
	
	 	function formReset() {
	 		//打開驗證隱藏欄位
			$("#hideblocka").show();
			$("#hideblockb").show();
			//塞值進span內的input
			$("#Monthly_DateA").val($("#FDATE").val());
			$("#Monthly_DateB").val($("#TDATE").val());
			
		
			if(!$("#main").validationEngine("validate")){
				e = e || window.event;
				e.preventDefault();
			}
	 		else{
				if ($('#actionBar').val()=="excel"){
					$("#downloadType").val("OLDEXCEL");
					$("#templatePath").val("/downloadTemplate/f_export_bill_query_result.xls");
		 		}else if ($('#actionBar').val()=="txt"){
					$("#downloadType").val("TXT");
					$("#templatePath").val("/downloadTemplate/f_export_bill_query_result.txt");
		 		}
				//ajaxDownload("${__ctx}/FCY/ACCT/f_export_bill_query_ajaxDirectDownload","main","finishAjaxDownload()");
				$("#main").attr("action", "${__ctx}/FCY/ACCT/f_export_bill_query_directDownload");
				$("#main").submit();
				$('#actionBar').val("");
	 		}
		}
		
		
 	</script>
</body>
</html>
