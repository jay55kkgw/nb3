<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function(){
			window.print();
		});
	</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br/><br/>
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
	<br/><br/><br/>
	<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
	<br/><br/><br/>
	<div> 
		<!-- 資料 -->
		<!-- 表格區塊 -->
		<div class="ttb-input-block">
			<c:if test="${ APPTYPE.equals('01') }">
				<table class="print">
					<tr>
						<td style="text-align: center">
<!-- 交易種類 -->
							<spring:message code= "LB.W1060" />
						</td>
<!-- 預約黃金買進 -->
						<td>
							<spring:message code= "LB.X0949" />
						</td>
					</tr>
					<tr>
						<td style="text-align: center">
<!-- 預約日期 -->
							<spring:message code= "LB.X0377" />
						</td>
						<td>
							${APPDATE}
						</td>
					</tr>
					<tr>
						<td style="text-align: center">
<!-- 臺幣轉出帳號 -->
							<spring:message code= "LB.W1496" />
						</td>
						<td>
							${SVACN}
						</td>
					</tr>
					<tr>
						<td style="text-align: center">
<!-- 黃金轉入帳號 -->
							<spring:message code= "LB.W1497" />
						</td>
						<td>
							${ACN}
						</td>
					</tr>
					<tr>
						<td style="text-align: center">
<!-- 買進公克數 -->
							<spring:message code= "LB.W1498" />
						</td>
						<td>
<!-- 公克 -->
							${TRNGD} <spring:message code= "LB.W1435" />
						</td>
					</tr>
				</table>
			</c:if>
			
			<c:if test="${ APPTYPE.equals('02') }">
				<table class="print">
					<tr>
						<td style="text-align: center">
<!-- 交易種類 -->
							<spring:message code= "LB.W1060" />
						</td>
						<td>
<!-- 預約黃金回售 -->						
							<spring:message code= "LB.X0952" />
						</td>
					</tr>
					<tr>
						<td style="text-align: center">
<!-- 預約日期 -->
							<spring:message code= "LB.X0377" />
						</td>
						<td>
							${APPDATE}
						</td>
					</tr>
					<tr>
						<td style="text-align: center">
<!-- 黃金轉出帳號 -->
							<spring:message code= "LB.W1515" />
						</td>
						<td>
							${ACN}
						</td>
					</tr>
					<tr>
						<td style="text-align: center">
<!-- 臺幣轉入帳號 -->
							<spring:message code= "LB.W1518" />
						</td>
						<td>
							${SVACN}
						</td>
					</tr>
					<tr>
						<td style="text-align: center">
<!-- 買進公克數 -->
							<spring:message code= "LB.W1498" />
						</td>
						<td>
<!-- 公克 -->
							${TRNGD} <spring:message code= "LB.W1435" />
						</td>
					</tr>
				</table>
			</c:if>
			<br>
			<br>
			<div class="text-left">
				<spring:message code="LB.Description_of_page" />
				<ol class="list-decimal text-left">
				<!-- 電子簽章:為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章(載具i-key)拔除並登出系統。 -->
			            <li><spring:message code="LB.Gold_Reservation_Query_P3_D1"/></li>
				</ol>
			</div>
		</div>

	</div>
</body>
</html>