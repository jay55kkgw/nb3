<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp" %>
<link rel="stylesheet" type="text/css"
	href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
	<style>
		.noZhCn{
			-ms-ime-mode: disabled;
		}
	</style>
<script type="text/javascript">
	var transferType = '${transfer_data.data.TransferType }'
	var fgsvacno = '${transfer_data.data.FGSVACNO }'
	var agreeflag = '${transfer_data.data.agreeflag }'
	var isikeyuser = '${sessionScope.isikeyuser}';
    
	var idgatesubmit= $("#formId");
    var idgateadopid="N070C";
    var idgatetitle="臺灣中小企銀手機轉帳";
    
	var isConfirm_s = false;
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 1000);
		// 初始化驗證碼
		setTimeout("initKapImg()", 200);
		// 生成驗證碼
		setTimeout("newKapImg()", 300);
		
		// 初始化時隱藏span
		$("#hideblock").hide();
		$("#errorBtn1").click(function(){
			if(isConfirm_s){
				isConfirm_s = false;
				checkTC();
			}
		});
		<c:if test="${transfer_data.data.isHasUse.equals('Y') && transfer_data.data.FGTXDATE.equals('1')}">
		    isConfirm_s = true;
			errorBlock(
				null, 
				["<spring:message code='LB.X2201' /> ${transfer_data.data.HasUseTime} <spring:message code='LB.X2202' />"],
				null, 
				'<spring:message code= "LB.Confirm" />', 
				null
			);
		</c:if>
		<c:if test="${!transfer_data.data.isHasUse.equals('Y') || !transfer_data.data.FGTXDATE.equals('1')}">
		checkTC();
		</c:if>
	});

	function init() {
		
		var i18n = new Object();
		if ("${transfer_data.data.FGTXDATE}" == "1") {
			i18n['Immediately'] = '<spring:message code="LB.Immediately" />';
			$("#FGTXDATE").html(i18n['Immediately']);
		} else {
			i18n['Booking'] = '<spring:message code="LB.Booking" />';
			$("#FGTXDATE").html(i18n['Booking']);
		}

		
		//表單驗證
		$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
		//一開始focusBHO
// 		focusBHO();

		$("#CMSUBMIT").click(function(e) {
// 			TODO 測試用
// 			var fgtxway = $('input[name="FGTXWAY"]:checked').val();			
// 			checkAmountByFGTXWAY(fgtxway);
			
			
					//送出進表單驗證前將span顯示
// 					$("#hideblock").show();
					console.log("submit~~");
// 					//塞值進span內的input
// 					$("#CARDNUM_TOTAL").val(""+$("#transInAccountText1").val()+ $("#transInAccountText2").val()+ $("#transInAccountText3").val());
									
					if (!$('#formId').validationEngine('validate')) {
						e.preventDefault();
					} 
					$("#formId").validationEngine('detach');
		 			processQuery();
// 					else {
// 						var uri = "${__ctx}/checkBHO"

// 						rdata = {
// 							keyInBHO : $("#CARDNUM_TOTAL").val(),
// 							functionName : "transfer",
// 						};
// 						fstop.getServerDataEx(uri, rdata, false, checkBHOFinish);
// 					}
				});

		//上一頁按鈕
		$("#pageback").click(function() {

			var action = '${pageContext.request.contextPath}' + '${previous}'
			$('#back').val("Y");
			$('#jsondc').attr('disabled', true);
			$("#formId").attr("action", action);
			initBlockUI();
			$("#formId").validationEngine('detach');
			$("#formId").submit();

		});

		// 判斷顯不顯示驗證碼
		chaBlock();
		// 交易機制 click
		fgtxwayClick();
		
		showRadio();
		
	}//init END
	
	// 	根據轉帳帳號類型顯示對應的交易方式
	function showRadio(){
		console.log("transferType>>"+transferType+",fgsvacno>>"+fgsvacno+",agreeflag>>"+agreeflag+",isikeyuser>>"+isikeyuser);
		var key = 3; //預設選用晶片金融卡
	    var idgateUserFlag = "${idgateUserFlag}";
	    
		if(idgateUserFlag == "Y") {
			key = 2;
		}
		if(transferType == 'PD' &&   fgsvacno == '1' && agreeflag=='1'){
			$("div[name='pw_group']").show();
			key = 0;
		}
		if(transferType == 'PD' &&   isikeyuser == 'true'){
			$("div[name='ikey_group']").show();
			if(key != 0){
				key =1 ;
			}
		}
		$('input[name="FGTXWAY"]:eq('+key+')').prop("checked", true).trigger("change");
	}
	
	
// 	function focusBHO(){
// 		//一開始先FOCUS到BHO
// 		$("#transInAccountText1").focus();

// 		$("#transInAccountText1").keyup(
// 				function(event) {
// 					if (event.which != 8 && event.which != 46
// 							&& event.which != 9 && event.which != 16
// 							&& event.which != 20) {
// 						$("#transInAccountText2").focus();
// 					}
// 				});
// 		$("#transInAccountText2").keyup(
// 				function(event) {
// 					if (event.which != 8 && event.which != 46
// 							&& event.which != 9 && event.which != 16
// 							&& event.which != 20) {
// 						$("#transInAccountText3").focus();
// 					}
// 				});
// 	}

// 	function checkBHOFinish(data) {
// 		if (data.result == true) {
// 			$("#formId").validationEngine('detach');
// 			processQuery();
// 		} else {
// 			//alert(data.message);
// 			errorBlock(
// 					null, 
// 					null,
// 					[data.message], 
// 					'<spring:message code= "LB.Quit" />', 
// 					null
// 				);
// 		}
// 	}
	
	// 交易機制選項
	function processQuery(){
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
		console.log("fgtxway: " + fgtxway);
		if(!checkAmountByFGTXWAY(fgtxway)){
			return false;
		}
		switch(fgtxway) {
			case '0':
				$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
				initBlockUI();
				$("#formId").submit();
// 				alert("交易密碼(SSL)...");
				break;
				
			case '1':
// 				alert("IKey...");
				var jsondc = $("#jsondc").val();
				uiSignForPKCS7(jsondc);
				break;
				
			case '2':
// 				alert("晶片金融卡");
				// 讀卡機...
// 				listReaders();
				// 晶片金融卡
				var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
				useCardReader(capUri);
		    	break;
			case '7'://IDGATE認證
				showIdgateBlock();
				break;
			default:
				//alert("nothing...");
				errorBlock(
						null, 
						null,
						["nothing..."], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
		}		
	}
	
// 	根據交易機制檢查金額
	function checkAmountByFGTXWAY(fgtxway){
		var retStr = "";
		var amount = '${transfer_data.data.AMOUNT }'
			amount = fstop.unFormatAmtToInt(amount);  
			console.log("amount: " + amount);
			console.log("transferType: " + transferType);
			console.log("fgsvacno: " + fgsvacno);
		if(('0' == fgtxway ) && amount > 5000000 ){
			retStr = "<spring:message code= "LB.X1413" />\n\n <spring:message code= "LB.X1414" />";
			//alert(retStr);
			errorBlock(
							null, 
							null,
							["<spring:message code= "LB.X1413" />", "\n", "\n", "<spring:message code= "LB.X1414" />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			return false;
		}
		if('1' == fgtxway  ){
			if(amount > 15000000){
				retStr = "<spring:message code= "LB.X1413" />\n\n <spring:message code= "LB.X1415" />";
				//alert(retStr);
				errorBlock(
						null, 
						null,
						["<spring:message code= "LB.X1413" />", "\n", "\n", "<spring:message code= "LB.X1415" />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
				return false;
			}
			
// 			非約2非約
			if( transferType =='NPD' && fgsvacno =='2'  &&  amount > 3000000){
				retStr = "<spring:message code= "LB.X1413" />\n\n <spring:message code= "LB.X1416" />";
				//alert(retStr);
				errorBlock(
						null, 
						null,
						["<spring:message code= "LB.X1413" />", "\n", "\n", "<spring:message code= "LB.X1416" />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
				return false;
				
			}
		}
		
		if('2' == fgtxway){
			
			if( amount > 5000000){
				retStr = "<spring:message code= "LB.X1413" />\n\n <spring:message code= "LB.X1570" />";
				//alert(retStr);
				errorBlock(
							null, 
							null,
							["<spring:message code= "LB.X1413" />", "\n", "\n", "<spring:message code= "LB.X1570" />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				return false;
			}
			if( (transferType =='NPD' || fgsvacno =='2')  &&  amount > 50000){
				retStr = "<spring:message code= "LB.X1413" />\n\n <spring:message code= "LB.X1448" />";
				//alert(retStr);
				errorBlock(
							null, 
							null,
							["<spring:message code= "LB.X1413" />", "\n", "\n", "<spring:message code= "LB.X1448" />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				return false;
			}
		}
	    if('7' == fgtxway){
            //TODO 裝置推播的error訊息
            if( amount > 5000000){
                retStr = "<spring:message code= "LB.X1413" />\n\n <spring:message code= "LB.X1570" />";
                //alert(retStr);
                errorBlock(
                            null, 
                            null,
                            ["<spring:message code= "LB.X1413" />", "\n", "\n", "<spring:message code= "LB.X1570" />"], 
                            '<spring:message code= "LB.Quit" />', 
                            null
                        );
                return false;
            }
            if( (transferType =='NPD' || fgsvacno =='2')  &&  amount > 50000){
                retStr = "<spring:message code= "LB.X1413" />\n\n <spring:message code= "LB.X1448" />";
                //alert(retStr);
                errorBlock(
                            null, 
                            null,
                            ["<spring:message code= "LB.X1413" />", "\n", "\n", "<spring:message code= "LB.X1448" />"], 
                            '<spring:message code= "LB.Quit" />', 
                            null
                        );
                return false;
            }
        }		
		return true;
		
	}
	
	
	
	//下拉式選單
 	function formReset() {
 		if ($('#actionBar').val()=="reEnter"){
	 		document.getElementById("formId").reset();
	 		$('#actionBar').val("");
 		}
	}
	
	

	// 使用者選擇晶片金融卡要顯示驗證碼區塊
 	function fgtxwayClick() {
 		$('input[name="FGTXWAY"]').change(function(event) {
 			// 判斷交易機制決定顯不顯示驗證碼區塊
 			chaBlock();
		});
	}
 	// 判斷交易機制決定顯不顯示驗證碼區塊
	function chaBlock() {
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
			
			
			switch(fgtxway) {
			case '0':
				$('#CMPASSWORD').addClass("validate[required]")
				$("#chaBlock").hide();
				break;
			case '2':
				$('#CMPASSWORD').removeClass("validate[required]")
				$("#chaBlock").show();
		    	break;
		    	
			default:
				$('#CMPASSWORD').removeClass("validate[required]")
				$("#chaBlock").hide();
		}		
			
			
			
			
			
		// 交易機制選項
// 		if(fgtxway == '2'){
// 			// 若為晶片金融卡才顯示驗證碼欄位
// 			$("#chaBlock").show();
// 		}else{
// 			// 若非晶片金融卡則隱藏驗證碼欄位
// 			$("#chaBlock").hide();
// 		}
 	}

	// 驗證碼刷新
	function changeCode() {
		$('input[name="capCode"]').val('');
		// 大小版驗證碼用同一個
		console.log("changeCode...");
		$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

		// 登入失敗解遮罩
		unBlockUI(initBlockId);
	}
	
	// 初始化驗證碼
	function initKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
	}
	
	// 生成驗證碼
	function newKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').click(function() {
			$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
		});
	}
	
	
	
</script>

</head>
<body>
	<!-- 	晶片金融卡 -->
	<%@ include file="../component/trading_component.jsp"%>
    <!--   IDGATE -->
    <%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %>	
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
	<!-- 轉帳交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Transfer" /></li>
    <!-- 轉帳交易     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.NTD_Transfer" /></li>
		</ol>
	</nav>




	<!--     左邊menu 及登入資訊-->
	<div class="content row">

		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> <!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<form method="post" name="formId" id="formId"
				action="${__ctx}/NT/ACCT/PHONE_TRANSFER/p_transfer_result">
				<input type="hidden" name="TXTOKEN" value="${transfer_data.data.TXTOKEN}">
				<input type="hidden" name="FGTXDATE" value="${transfer_data.data.FGTXDATE}"> 
				<input type="hidden" id="PINNEW" name="PINNEW"  value="">				
				<input type="hidden" id="back" name="back" value="">
				<input type="hidden" id="ACCOUNTNAME" name="ACCOUNTNAME" value="${transfer_data.data.ACCOUNTNAME}">
				<input type="hidden" id="DPBHNO" name="DPBHNO" value="${transfer_data.data.DPBHNO}">
				<input type="hidden" id="ACNO" name="ACNO" value="${transfer_data.data.ACNO}">
				<input type="hidden" id="OUTACN_NPD" name="OUTACN_NPD" value="${transfer_data.data.OUTACN_NPD}">
				<input type="hidden" id="DPACNO" name="DPACNO" value="${transfer_data.data.DPACNO}">
				<input type="hidden" id="AMOUNT" name="AMOUNT" value="${transfer_data.data.AMOUNT}">
				
				<!-- 			晶片金融卡 -->
				<input type="hidden" id="jsondc" name="jsondc" value='${transfer_data.data.jsondc}'>
				<input type="hidden" id="ISSUER" name="ISSUER" value="">
				<input type="hidden" id="ACNNO" name="ACNNO" value="">
				<input type="hidden" id="TRMID" name="TRMID" value="">
				<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
				<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
				<input type="hidden" id="TAC" name="TAC" value="">
				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
				<h2>
					<spring:message code="LB.NTD_Transfer" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block no-l-display-btn">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option><spring:message code="LB.Execution_option" /></option>
						<!-- 重新輸入-->
						<option value="reEnter">
							<spring:message code="LB.Re_enter" />
						</option>
					</select>
				</div>
				<div id="step-bar">
					<ul>
						<li class="finished"><spring:message code="LB.Enter_data" /></li>
						<li class="active"><spring:message code="LB.Confirm_data" /></li>
						<li class=""><spring:message code="LB.Transaction_complete" /></li>
					</ul>
				</div>

				<div class="main-content-block row">
					<div class="col-12">
						<div class="ttb-input-block">
							<div class="ttb-message">
								<p id="FGTXDATE"></p>
								<span><spring:message code="LB.Confirm_transfer_data" /></span>
							</div>
							<!-- 交易類型 -->
							<div class="ttb-input-item row">
								<span class="input-title">
								<label><h4>交易類型</h4></label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
									<p>非約定轉帳</p>
									</div>
								</span>
							</div>
							<!-- 轉帳日期 -->
							<div class="ttb-input-item row">
								<span class="input-title">
								<label><h4>轉帳日期</h4></label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
				      						<p>${transfer_data.data.transferdate}</p>
									</div>
								</span>
							</div>
							<!-- 轉出帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title">
								<label><h4>轉出帳號</h4></label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
				      				<c:choose>
										<c:when test="${transfer_data.data.TransferType == 'NPD'}">
											<p>${transfer_data.data.OUTACN_NPD}</p>
			      						</c:when>

										<c:otherwise>
				      						<p>${transfer_data.data.ACNO}</p>
				      					</c:otherwise>
									</c:choose>
									</div>
								</span>
							</div>
							
<%-- 							<c:if test="${(not empty transfer_data.data.DPBHNO) && (transfer_data.data.DPBHNO.length() > 3)}"> --%>
							<!-- 收款銀行 -->
							<div class="ttb-input-item row">
								<span class="input-title">
								<label><h4>收款銀行</h4></label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
				      						<p>${transfer_data.data.DPBHNO}</p>
									</div>
								</span>
							</div>
<%-- 							</c:if> --%>
							
							<!-- 收款人戶名 -->
							<div class="ttb-input-item row">
								<span class="input-title">
								<label><h4>收款人戶名</h4></label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
				      						<p>${transfer_data.data.ACCOUNTNAME}</p>
									</div>
								</span>
							</div>
							
							<!-- 手機門號 -->
							<div class="ttb-input-item row">
								<span class="input-title">
								<label><h4>手機門號 </h4></label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
				      						<p>${transfer_data.data.DPACNO}</p>
									</div>
								</span>
							</div>
								<!-- 轉帳金額 -->
							<div class="ttb-input-item row">
								<span class="input-title">
										<label><h4>
											<spring:message code="LB.Amount" />
										</h4></label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p class="high-light">
											<span class="input-unit"><spring:message code="LB.NTD" /></span>
											${transfer_data.data.AMOUNT }
											<span class="input-unit">
											<spring:message code="LB.Dollar" /></span>
										</p>
									</div>
								</span>
							</div>
							<!-- 交易機制 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label><h4>
											<spring:message code="LB.Transaction_security_mechanism" />
										</h4></label>
								</span> 
								<span class="input-block">
									<div class="ttb-input" name="pw_group" style="display:none">
										<label class="radio-block"> 
											<spring:message	code="LB.SSL_password" /> 
											<input type="radio"  name="FGTXWAY"  value="0"> 
											<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input" name="pw_group" style="display:none">
										<input id="CMPASSWORD"  type="password"
											class="text-input" maxlength="8"
											placeholder="<spring:message code="LB.Please_enter_password"/>">
									</div>

									<div class="ttb-input" name="ikey_group" style="display:none">
										<label class="radio-block"><spring:message
												code="LB.Electronic_signature" /> <input type="radio"
											id="CMIKEY" name="FGTXWAY" value="1"> <span class="ttb-radio"></span></label>
									</div>
									<!-- IDGATE -->
									<%@ include file="../idgate_tran/idgateComponent.jsp" %>
									<div class="ttb-input" name="card_group">
										<label class="radio-block"><spring:message
												code="LB.Financial_debit_card" /> <input type="radio" id="CMCARD"
											name="FGTXWAY" value="2"> <span class="ttb-radio"></span></label>
									</div>
								</span>
							</div>
							
							<!-- 如果是非約定則不能使用IKEY，只能選晶片金融卡，使用者選擇晶片金融卡後才顯示驗證碼-->
							<div class="ttb-input-item row" id="chaBlock" style="display:none">
								<!-- 驗證碼 -->
								<span class="input-title">
									<label>
										<h4><spring:message code= "LB.Captcha" /></h4>
									</label>
								</span>
								<span class="input-block">
									<spring:message code="LB.Captcha" var="labelCapCode" />
									<input id="capCode" name="capCode" type="text"
										class="text-input" maxlength="6" autocomplete="off">
									<img name="kaptchaImage" class = "verification-img" src="" />
									<input type="button" name="reshow" class="ttb-sm-btn btn-flat-orange"
										onclick="changeCode()" value="<spring:message code= "LB.Regeneration" />" />
								</span>
							</div>
							
						</div>
						<input type="button" id="pageback" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />" />
						<input type="button" id="CMSUBMIT" name="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
					</div>
				</div>
				<ol class="list-decimal description-list">
						<li><p>	<font color="red">*轉帳送出謹慎按，防止詐騙免麻煩*</font></p></li>
			  </ol>
			</form>

		</section>
		<!-- 		main-content END --> </main>
	</div>
	<!-- 	content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>