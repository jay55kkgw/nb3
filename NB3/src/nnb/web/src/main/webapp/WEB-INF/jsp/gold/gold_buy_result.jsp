<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	
	$("#printButton").click(function(){
		var params = {
			"jspTemplateName":"gold_buy_result_print",
			//黃金買進
			"jspTitle":"<spring:message code="LB.W1493"/>",
			"CMQTIME":"${bsData.CMQTIME}",
			"SVACN":"${bsData.SVACN}",
			"ACN":"${bsData.ACN}",
			"TRNGDFormat":"${bsData.TRNGDFormat}",
			"PRICEFormat":"${bsData.PRICEFormat}",
			"DISPRICEFormat":"${bsData.DISPRICEFormat}",
			"PERDISFormat":"${bsData.PERDISFormat}",
			"TRNFEEFormat":"${bsData.TRNFEEFormat}",
			"TRNAMTFormat":"${bsData.TRNAMTFormat}"
		};
		openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
	});
	$("#certButton").click(function(){
		var params = {
			"jspTemplateName":"gold_buy_result_print2",
			//黃金買進
			"jspTitle":"<spring:message code="LB.W1493"/>",
			"CMQTIME":"${bsData.CMQTIME}",
			"DPUSERNAME":"${DPUSERNAME}",
			"SVACN":"${bsData.SVACN}",
			"ACN":"${bsData.ACN}",
			"TRNGDFormat":"${bsData.TRNGDFormat}",
			"PRICEFormat":"${bsData.PRICEFormat}",
			"DISPRICEFormat":"${bsData.DISPRICEFormat}",
			"PERDISFormat":"${bsData.PERDISFormat}",
			"TRNFEEFormat":"${bsData.TRNFEEFormat}",
			"TRNAMTFormat":"${bsData.TRNAMTFormat}"
		};
		openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
	});
	$("#kycButton").click(function(){
		var UID = "${bsData.CUSIDN}";
		
		if(UID.length == 10){
			$("#formID").attr("action","${__ctx}/FUND/TRANSFER/fund_invest_attr1?TXID=G");
		}
		else if(UID.length < 10){
			$("#formID").attr("action","${__ctx}/FUND/TRANSFER/fund_invest_attr3?TXID=G");
		}
		$("#formID").submit();
	});
});
</script>
</head>
<body>
	<!--header-->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %> 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 黃金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2272" /></li>
    <!-- 黃金申購     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1493" /></li>
		</ol>
	</nav>

	<!--左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!--快速選單及主頁內容-->
		<main class="col-12"> 
			<!--主頁內容-->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.W1493"/></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form id="formID" method="post">
                	<input type="hidden" name="ADOPID" value="N09001"/>
	                <div class="main-content-block row">
	                    <div class="col-12 tab-content">
	                        <div class="ttb-input-block">
	                        	<div class="ttb-message">
	                        	<!-- 交易成功 -->
	                        		<span><spring:message code="LB.Transaction_successful"/></span>
	                        	</div>
								<!--交易日期-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
										<label>
	                                        <h4><spring:message code="LB.Transaction_date"/></h4>
	                                    </label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
	                                       	<span>${bsData.CMQTIME}</span>
										</div>
									</span>
								</div>
								<!--台幣轉出帳號-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                	<label>
	                                        <h4><spring:message code= "LB.W1496" /></h4>
	                                    </label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
	                                       	<span>${bsData.SVACN}</span>
										</div>
									</span>
								</div>
								<!--黃金轉入帳號-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
		                                <label>
											<h4><spring:message code="LB.W1497"/></h4>
										</label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
	                                       	<span>${bsData.ACN}</span>
										</div>
									</span>
								</div>
								<!--買進公克數-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
										<label>
	                                        <h4><spring:message code="LB.W1498"/></h4>
	                                    </label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
	                                       	<span>${bsData.TRNGDFormat}</span>
	                                       	<!-- 公克 -->
											<span class="ttb-unit"><spring:message code="LB.W1435"/></span>
										</div>
									</span>
								</div>
								<!--牌告單價-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
										<label>
	                                        <h4><spring:message code="LB.W1524"/></h4>
	                                    </label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
										<!-- 新台幣 -->
	                                       	<span class="ttb-unit"><spring:message code="LB.NTD"/></span>
											<span>${bsData.PRICEFormat}</span>
											<!-- 元/公克 -->
											<span class="ttb-unit"><spring:message code="LB.W1511"/></span>
										</div>
									</span>
								</div>
								<!--折讓後單價-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                	<label>
											<h4><spring:message code="LB.W1504"/></h4>
	                                    </label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
										<!-- 新台幣 -->
	                                       	<span class="ttb-unit"><spring:message code="LB.NTD"/></span>
											<span>${bsData.DISPRICEFormat}</span>
											<!-- 元/公克 -->
											<span class="ttb-unit"><spring:message code="LB.W1511"/></span>
										</div>
									</span>
								</div>
								<!--折讓率-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
										<label>
	                                        <h4><spring:message code="LB.W1506"/></h4>
	                                    </label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
											<span>${bsData.PERDISFormat}％</span>
										</div>
									</span>
								</div>
								<!--手續費-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                	<label>
	                                        <h4><spring:message code="LB.D0507"/></h4>
	                                    </label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
										<!-- 新台幣 -->
	                                       	<span class="ttb-unit"><spring:message code="LB.NTD"/></span>
											<span>${bsData.TRNFEEFormat}</span>
											<!-- 元 -->
											<span class="ttb-unit"><spring:message code="LB.Dollar"/></span>
										</div>
									</span>
								</div>
								<!--總扣款金額-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                	<label>
	                                        <h4><spring:message code="LB.W1509"/></h4>
	                                    </label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
										<!-- 新台幣 -->
	                                       	<span class="ttb-unit"><spring:message code="LB.NTD"/></span>
											<span>${bsData.TRNAMTFormat}</span>
												<!-- 元 -->
											<span class="ttb-unit"><spring:message code="LB.Dollar"/></span>
										</div>
									</span>
								</div>
	                        </div>
	                        
	                        <!-- 列印 -->
	                  			<input type="button" id="printButton" value="<spring:message code="LB.Print"/>" class="ttb-button btn-flat-orange"/>
	                			<!-- 交易單據 -->
	                			<input type="button" id="certButton" value="<spring:message code="LB.Transaction_document"/>" class="ttb-button btn-flat-orange"/>
	                			<c:if test="${KYC == 'PASS'}">
	                			<!-- 填寫KYC問卷 -->
	                				<input type="button" id="kycButton" value="<spring:message code="LB.X0947"/>" class="ttb-button btn-flat-orange"/>
	                			</c:if>
	                		
	                    </div>
	                </div>
	                <ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /></p><!-- 說明 -->
						<!-- 電子簽章：為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章（載具i－key）拔除並登出系統。 -->
							<li><spring:message code="LB.Gold_Buy_P3_D1"/></li>
						</ol>
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>