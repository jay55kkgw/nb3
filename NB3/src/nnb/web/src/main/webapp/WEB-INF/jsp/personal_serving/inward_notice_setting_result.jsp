<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>

	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp"%>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			init();
			setTimeout("initDataTable()",100);
		});

		function init() {
			//initDataTable();//使用DataTable
			//表單驗證初始化
			$("#formId").validationEngine({binded: false,promptPosition: "inline"});
			//送出事件
			$("#CMSUBMIT").click(function (e) {
				console.log("submit~~");
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					initBlockUI();
					if (processQuery()) {
						$("#formId").submit();
					} else {
						unBlockUI(initBlockId);
					}
				}
			});
		}
	</script>
</head>

<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 國內臺幣匯入匯款通知設定     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0437" /></li>
		</ol>
	</nav>

	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">

		<%@ include file="../index/menu.jsp"%>
		<!-- 功能選單內容 -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2>
					<spring:message code="LB.X0437" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" name="formId" method="post" action="${__ctx}/PERSONAL/SERVING/inward_notice_setting_result">
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-message">
								<span><spring:message code="LB.Change_Settings_Successfully"/></span>
							</div>
							<!-- 表格區塊 -->
							<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
								<tr>
									<td class=""><spring:message code="LB.D0121"/>:</td>
									<td class="">${ inward_notice_setting_result.data.CMQTIME }</td>
								</tr>
								<tr>
									<td class=""><spring:message code="LB.D0941"/> :</td>
									<td class="">
										<c:forEach var="dataList" items="${ inward_notice_setting_result.data.result }">
											<c:choose>
												<c:when test="${dataList[1]==1}">
													<font color=blue><spring:message code="LB.X0440" /></font>
												</c:when>
												<c:otherwise>
													<font color=red><spring:message code="LB.Cancel"/></font>
												</c:otherwise>
											</c:choose>
											<spring:message code="LB.Loan_account"/>：${dataList[0]}
											<spring:message code="LB.Deposit_amount_1"/>：
											<fmt:formatNumber type="number" minFractionDigits="2" value="${dataList[2]}" />
											<br>
										</c:forEach>
									</td>
								</tr>
							</table>
							<!-- Button -->
							<div>
							
							</div>
						</div>
					</div>
					<!-- main-content-block END -->
				</form>
			</section>
		</main>
	</div>
</body>
</html>