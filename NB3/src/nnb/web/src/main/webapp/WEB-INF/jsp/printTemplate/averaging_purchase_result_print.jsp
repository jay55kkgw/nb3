<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ include file="../__import_js.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
	$(document).ready(function() {
		window.print();
	});
</script>
</head>
<body class="bodymargin watermark"
	style="-webkit-print-color-adjust: exact">
	<br />
	<br />
	<div style="text-align: center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br />
	<br />
	<br />
	<div style="text-align: center">
		<font style="font-weight: bold; font-size: 1.2em">${jspTitle}</font>
	</div>
	<br />
	<br />
	<br />
	<div style="text-align: center">
	<!-- 定期定額申購成功 -->
		<font color="red"><spring:message code= "LB.X0931" /></font>
	</div>
	<br />
	<table class="print">
		<tr>
<!-- 約定扣款帳號 -->
			<th class="text-center"><spring:message code= "LB.W1539" /></th>
			<th>${SVACN}</th>
		</tr>
		<tr>
<!-- 黃金存摺帳號 -->
			<th class="text-center"><spring:message code= "LB.D1090" /></th>
			<th>${ACN}</th>
		</tr>
		<tr>
<!-- 每月投資日/金額 -->
			<th colspan="2" class="text-center"><spring:message code= "LB.W1557" /></th>
		</tr>
		<tr>
<!-- 日 -->
			<th class="text-center">06<spring:message code= "LB.Day" /></th>
<!-- 新臺幣 -->
<!-- 元 -->
			<th><spring:message code= "LB.NTD" />&nbsp;${AMT_06_1}&nbsp;<spring:message code= "LB.Dollar" /></th>
		</tr>
		<tr>
<!-- 日 -->
			<th class="text-center">16<spring:message code= "LB.Day" /></th>
<!-- 新臺幣 -->
<!-- 元 -->
			<th><spring:message code= "LB.NTD" />&nbsp;${AMT_16_1}&nbsp;<spring:message code= "LB.Dollar" /></th>
		</tr>
		<tr>
<!-- 日 -->
			<th class="text-center">26<spring:message code= "LB.Day" /></th>
<!-- 新臺幣 -->
<!-- 元 -->
			<th><spring:message code= "LB.NTD" />&nbsp;${AMT_26_1}&nbsp;<spring:message code= "LB.Dollar" /></th>
		</tr>
	</table>
	<br>
	<div class="text-left" style="margin-left: 20px;">
		<spring:message code="LB.Description_of_page" />
		<ol class="list-decimal text-left">
		<!-- 若於投資日或投資日以後始申購定期定額投資者，自下次投資日起開始扣款。 -->
			<li><spring:message code= "LB.Averaging_Purchase_P3_D1" /></li>
       <!-- 網路銀行定期定額每次扣款優惠手續費為新臺幣50元，連同投資金額一併扣繳。 -->
			<li><spring:message code= "LB.Averaging_Purchase_P3_D2" /></li>
			<!-- 電子簽章:為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章(載具i-key)拔除並登出系統。 -->
			<li><spring:message code= "LB.Averaging_Purchase_P3_D3" /></li>
		</ol>
	</div>
	<br />
	<br />
</body>
</html>