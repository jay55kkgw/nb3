<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="card-detail-block">
	<div class="card-center-block">

		<!-- Q1 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				Data-toggle="collapse" href="#popup3-1" aria-expanded="true"
				Aria-controls="popup3-1">
				<div class="row">
					<span class="col-1">Q1</span>
					<div class="col-11">
						<span>Reception time</span>
					</div>
				</div>
			</a>
			<div id="popup3-1" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div style="overflow: auto">
								<table class="question-table" style="width: 100%">
									<thead>
										<tr>
											<th>Cabinet</th>
											<th>Network</th>
											<th>Voice</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Business hours 9 am to 3 pm</td>
											<td class="white-spacing text-left" colspan="2">
												Transaction service time: 7:00 am to 3:00 pm on business day<br>
												Appointment service time: outside the transaction service
												hours<br> (After the closing of the trading service
												period and before the start of the trading service time on
												the next business day)
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q2 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				Data-toggle="collapse" href="#popup3-2" aria-expanded="true"
				Aria-controls="popup3-2">
				<div class="row">
					<span class="col-1">Q2</span>
					<div class="col-11">
						<span>Minimum purchase investment amount</span>
					</div>
				</div>
			</a>
			<div id="popup3-2" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div style="overflow: auto">
								<table class="question-table" style="width: 100%">
									<thead>
										<tr>
											<th colspan="3">Investment targets</th>
											<th>Minimum <br>investment amount
											</th>
											<th>Increase unit</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th rowspan="19">Single</th>
											<th colspan="2">Domestic Currency Fund</th>
											<td class="white-spacing text-left" colspan="2">Subject
												to individual fund regulations (closed at 10:30 am)</td>
										</tr>
										<tr>
											<th rowspan="5">Domestic Fund</th>
											<th>Taiwan Dollar Trust</th>
											<td>NTD 10,000</td>
											<td>NTD 1,000</td>
										</tr>
										<tr>
											<th rowspan="4" align="center" class="f13px lineH_01 coR">
												Foreign Currency Trust</th>
											<td>ZAR 10,000</td>
											<td>ZAR 1,000</td>
										</tr>
										<tr>
											<td>USD 1,000</td>
											<td>USD 100</tt>
										</tr>
										<tr>
											<td>CNY 5,000</td>
											<td>CNY 1,000</td>
										</tr>
										<tr>
											<td>AUD 1,000</td>
											<td>AUD 100</td>
										</tr>
										<tr>
											<th rowspan="13">Foreign Fund</th>
											<th>Taiwan Dollar Trust</th>
											<td>NTD 30,000</td>
											<td>NTD 10,000</td>
										</tr>
										<tr>
											<th rowspan="12">Foreign Currency Trust</th>
											<td>USD 1,000</td>
											<td>USD 100</tt>
										</tr>
										<tr>
											<td>EUR 1,000</td>
											<td>EUR 100</td>
										</tr>
										<tr>
											<td>GBP 1,000</td>
											<td>GBP 100</td>
										</tr>
										<tr>
											<td>AUD 1,000</td>
											<td>AUD 100</td>
										</tr>
										<tr>
											<td>CHF 1,000</td>
											<td>CHF 100</td>
										</tr>
										<tr>
											<td>CAD 1,000</td>
											<td>CAD 100</td>
										</tr>
										<tr>
											<td>SEK 10,000</td>
											<td>SEK 1,000</td>
										</tr>
										<tr>
											<td>JPY 100,000</td>
											<td>JPY 10,000</td>
										</tr>
										<tr>
											<td>SGD 1,000</td>
											<td>SGD 100</td>
										</tr>
										<tr>
											<td>NZD 1,000</td>
											<td>NZD 100</td>
										</tr>
										<tr>
											<td>HKD 10,000</td>
											<td>HKD 1,000</td>
										</tr>
										<tr>
											<td>ZAR 10,000</td>
											<td>ZAR 1,000</td>
										</tr>
										<tr>
											<th rowspan="19">Quota / Irregular</th>
											<th colspan="2">Domestic Currency Fund</th>
											<td colspan="2">Not yet open</td>
										</tr>
										<tr>
											<th colspan="2" rowspan="5">Domestic Fund</th>
											<td>NTD 3,000</td>
											<td>NTD 1,000</td>
										</tr>
										<tr>
											<td>ZAR 2,000</td>
											<td>ZAR 500</td>
										</tr>
										<tr>
											<td>USD 200</tt>
											<td>USD 50</td>
										</tr>
										<tr>
											<td>RMB 1,000</td>
											<td>RMB 500</td>
										</tr>
										<tr>
											<td>AUD 200</td>
											<td>AUD 50</td>
										</tr>

										<tr>
											<th rowspan="13">Foreign Fund</th>
											<th>Taiwan Dollar Trust</th>
											<td>NTD 3,000/5,000</tt>
											<td>NTD 1,000</td>
										</tr>
										<tr>
											<th rowspan="12">Foreign Currency Trust</th>
											<td>USD 200/300</td>
											<td>USD 50</td>
										</tr>
										<tr>
											<td>EUR 200/300</td>
											<td>EUR 50</td>
										</tr>
										<tr>
											<td>GBP 200/300</td>
											<td>GBP 50</td>
										</tr>
										<tr>
											<td>AUD 200/300</td>
											<td>AUD 50</td>
										</tr>
										<tr>
											<td>CHF 200/300</td>
											<td>CHF 50</td>
										</tr>
										<tr>
											<td>CAD 200/300</td>
											<td>CAD 50</td>
										</tr>
										<tr>
											<td>SEK 2,000/3,000</td>
											<td>SEK 500</td>
										</tr>
										<tr>
											<td>JPY 20,000/30,000</td>
											<td>JPY 5,000</td>
										</tr>
										<tr>
											<td>NZD 200/300</td>
											<td>NZD 50</td>
										</tr>
										<tr>
											<td>HKD 2,000</td>
											<td>HKD 500</td>
										</tr>
										<tr>
											<td>SGD 200</td>
											<td>SGD 50</td>
										</tr>
										<tr>
											<td>ZAR 2,000</td>
											<td>ZAD 500</td>
										</tr>
									</tbody>
								</table>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q3 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup3-3" aria-expanded="true"
				aria-controls="popup3-3">
				<div class="row">
					<span class="col-1">Q3</span>
					<div class="col-11">
						<span>Purchase fee</span>
					</div>
				</div>
			</a>
			<div id="popup3-3" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div style="overflow: auto">
								<table class="question-table" style="width: 100%">
									<thead>
										<tr>
											<th colspan="3">Investment targets</th>
											<th colspan="2">Minimum handling fee</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th colspan="3">Single</th>
											<td class="white-spacing text-left" colspan="2">Based on
												the rates stipulated by each fund company</td>
										</tr>
										<tr>
											<th rowspan="10">Regular <br> (Not) Quota
											</th>
											<th colspan="2" rowspan="3">Domestic <br>non-monetary
												fund
											</th>
											<td>NTD 50</td>
											<td>ZAR 60</td>
										</tr>
										<tr>
											<td>USD 6</td>
											<td>RMB 30</td>
										</tr>
										<tr>
											<td>AUD 6</td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<th rowspan="7">Foreign Fund</th>
											<th>Taiwan Dollar Trust</th>
											<td colspan="2">NTD 150</td>
										</tr>
										<tr>
											<th rowspan="6">Foreign Currency Trust</th>
											<td>USD 6</td>
											<td>CHF 6</td>
										</tr>
										<tr>
											<td>EUR 6</td>
											<td>CAD 6</td>
										</tr>
										<tr>
											<td>GBP 6</td>
											<td>SEK 60</td>
										</tr>
										<tr>
											<td>AUD 6</td>
											<td>JPY 600</td>
										</tr>
										<tr>
											<td>HKD 60</td>
											<td>NZD 6</td>
										</tr>
										<tr>
											<td>SGP 6</td>
											<td>ZAR 60</td>
										</tr>
									</tbody>
								</table>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q4 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup3-4" aria-expanded="true"
				aria-controls="popup3-4">
				<div class="row">
					<span class="col-1">Q4</span>
					<div class="col-11">
						<span>Regular Quota/No Quota investment considerations</span>
					</div>
				</div>
			</a>
			<div id="popup3-4" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<ol class="ttb-result-list terms">
								<li data-num="1.">In order to ensure the success of the
									investment, if you choose to debit the deposit account, please
									keep enough debit amount on the business day before the
									designated debit date, and choose the credit card debit, the
									consignor and the cardholder should be two business days before
									the deduction date to confirm whether the credit card balance
									is sufficient for the fund investment deduction; if the
									consignor has several investment funds at the same time and the
									balance is insufficient, then it is agreed that the Bank shall
									prevail in the order in which the debiting operations are
									organized.</li>

								<li data-num="2.">If the line is interrupted or other
									reasons, the Bank may not conduct the debit investment
									operation on the debit date designated by the consignor, and
									the consignor agrees to postpone the bank business day after
									the obstacle is excluded, and begins after the debit is
									completed and make an investment.</li>

								<li data-num="3.">Description of "Regular Quota Agreement
									Change"
									<ul class="ttb-result-list terms">
										<li data-num="a.">The agreement under the “Dollar Cost
											Averaging Agreement Changes” is implemented on the date of
											application. If the trust data to be changed is already in
											the process of related projects, or the debit operation has
											been completed, the agreement will be postponed.</li>
										<li data-num="b.">If you want to change the trust data,
											the Bank's computer system has been unable to provide
											services for you during the processing of related projects.
											Please apply again on the next business day (two business
											days involving credit card transactions).</li>
										<li data-num="c.">If there is more than two debits for a
											single voucher, the network/system cannot provide the "dollar
											cost averaging deduction amount" change service. Please
											contact the business unit.</li>
									</ul>
								</li>
							</ol>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q5 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup3-5" aria-expanded="true"
				aria-controls="popup3-5">
				<div class="row">
					<span class="col-1">Q5</span>
					<div class="col-11">
						<span>Investment target transfer and book balance amount
							limit</span>
					</div>
				</div>
			</a>
			<div id="popup3-5" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div style="overflow: auto">
								<table class="question-table" style="width: 100%">
									<thead>
										<tr>
											<th colspan="3">Investment targets</th>
											<th>Minimum redemption and<br>book balance amount
											</th>
											<th>Increase unit</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th rowspan="19">Single</th>
											<th colspan="2">Domestic Currency Fund</th>
											<td class="white-spacing text-left" colspan="2">
												Depending on individual fund regulations</td>
										</tr>
										<tr>
											<th rowspan="5">Domestic Fund</th>
											<th>Taiwan Dollar Trust</th>
											<td>NTD 10,000</td>
											<td>NTD 1,000</td>
										</tr>
										<tr>
											<th rowspan="4">Foreign Currency Trust</th>
											<td>ZAR 10,000</td>
											<td>ZAR 1,000</td>
										</tr>
										<tr>
											<td>USD 1,000</td>
											<td>USD 100</tt>
										</tr>
										<tr>
											<td>RMB 5,000</td>
											<td>RMB 1,000</td>
										</tr>
										<tr>
											<td>AUD 1,000</td>
											<td>AUD 100</td>
										</tr>
										<tr>
											<th rowspan="13">Foreign Fund</th>
											<th>Taiwan Dollar Trust</th>
											<td>NTD 30,000</td>
											<td>NTD 10,000</td>
										</tr>
										<tr>
											<th rowspan="12">Foreign Currency Trust</th>
											<td>USD 1,000</td>
											<td>USD 100</tt>
										</tr>
										<tr>
											<td>EUR 1,000</td>
											<td>EUR 100</td>
										</tr>
										<tr>
											<td>GBP 1,000</td>
											<td>GBP 100</td>
										</tr>
										<tr>
											<td>AUD 1,000</td>
											<td>AUD 100</td>
										</tr>
										<tr>
											<td>CHF 1,000</td>
											<td>CHF 100</td>
										</tr>
										<tr>
											<td>CAD 1,000</td>
											<td>CAD 100</td>
										</tr>
										<tr>
											<td>SEK 10,000</td>
											<td>SEK 1,000</td>
										</tr>
										<tr>
											<td>JPY 100,000</td>
											<td>JPY 10,000</td>
										</tr>
										<tr>
											<td>SGD 1,000</td>
											<td>SGD 100</td>
										</tr>
										<tr>
											<td>NZD 1,000</td>
											<td>NZD 100</td>
										</tr>
										<tr>
											<td>HKD 10,000</td>
											<td>HKD 1,000</td>
										</tr>
										<tr>
											<td>ZAR 10,000</td>
											<td>ZAR 1,000</td>
										</tr>
										<tr>
											<th colspan="3">Regular (Not) Quota</th>
											<td class="white-spacing text-left" colspan="2">All
												investment targets must be transferred out at once, and
												can't apply partial transfer.</td>
										</tr>
									</tbody>
								</table>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q6 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				Data-toggle="collapse" href="#popup3-6" aria-expanded="true"
				Aria-controls="popup3-6">
				<div class="row">
					<span class="col-1">Q6</span>
					<div class="col-11">
						<span>Conversion fee</span>
					</div>
				</div>
			</a>
			<div id="popup3-6" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<ol class="ttb-result-list terms">
								<li data-num="1.">The Bank charges a conversion fee of
									NT$500 per transaction.</li>
								<li data-num="2.">The principle of conversion fees of each
									fund is handled by means of internal deduction and is
									calculated according to the regulations of each fund company.</li>
							</ol>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q7 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				Data-toggle="collapse" href="#popup3-7" aria-expanded="true"
				Aria-controls="popup3-7">
				<div class="row">
					<span class="col-1">Q7</span>
					<div class="col-11">
						<span>Partial redemption and book balance amount limit</span>
					</div>
				</div>
			</a>
			<div id="popup3-7" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div style="overflow: auto">
								<table class="question-table" style="width: 100%">
									<thead>
										<tr>
											<th colspan="3">Investment targets</th>
											<th>Minimum redemption and<br>book balance amount
											</th>
											<th>Increase unit</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th rowspan="19">Single (Not) Quota</th>
											<th colspan="2">Domestic Currency Fund</th>
											<td class="white-spacing text-left" colspan="2">
												Depending on individual fund regulations</td>
										</tr>
										<tr>
											<th rowspan="5">Domestic Fund</th>
											<th>Taiwan Dollar Trust</th>
											<td>NTD 10,000</td>
											<td>NTD 1,000</td>
										</tr>
										<tr>
											<th rowspan="4">Foreign Currency Trust</th>
											<td>ZAR 10,000</td>
											<td>ZAR 1,000</td>
										</tr>
										<tr>
											<td>USD 1,000</td>
											<td>USD 100</tt>
										</tr>
										<tr>
											<td>CNY 5,000</td>
											<td>CNY 1,000</td>
										</tr>
										<tr>
											<td>AUD 1,000</td>
											<td>AUD 100</td>
										</tr>
										<tr>
											<th rowspan="13">Foreign Funds</th>
											<th>Taiwan Dollar Trust</th>
											<td>NTD 30,000</td>
											<td>NTD 10,000</td>
										</tr>
										<tr>
											<th rowspan="12">Foreign Currency Trust</th>
											<td>USD 1,000</td>
											<td>USD 100</tt>
										</tr>
										<tr>
											<td>EUR 1,000</td>
											<td>EUR 100</td>
										</tr>
										<tr>
											<td>GBP 1,000</td>
											<td>GBP 100</td>
										</tr>
										<tr>
											<td>AUD 1,000</td>
											<td>AUD 100</td>
										</tr>
										<tr>
											<td>CHF 1,000</td>
											<td>CHF 100</td>
										</tr>
										<tr>
											<td>CAD 1,000</td>
											<td>CAD 100</td>
										</tr>
										<tr>
											<td>SEK 10,000</td>
											<td>SEK 1,000</td>
										</tr>
										<tr>
											<td>JPY 100,000</td>
											<td>JPY 10,000</td>
										</tr>
										<tr>
											<td>SGD 1,000</td>
											<td>SGD 100</td>
										</tr>
										<tr>
											<td>NZD 1,000</td>
											<td>NZD 100</td>
										</tr>
										<tr>
											<td>HKD 10,000</td>
											<td>HKD 1000</td>
										</tr>
										<tr>
											<td>ZAR 10,000</td>
											<td>ZAR 1,000</td>
										</tr>
									</tbody>
								</table>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q8 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				Data-toggle="collapse" href="#popup3-8" aria-expanded="true"
				Aria-controls="popup3-8">
				<div class="row">
					<span class="col-1">Q8</span>
					<div class="col-11">
						<span>Trust management fee</span>
					</div>
				</div>
			</a>
			<div id="popup3-8" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div style="overflow: auto">
								<table class="question-table" style="width: 100%">
									<thead>
										<tr>
											<th width="17%">Investment targets</th>
											<th>Trust management fee</th>
											<th>Minimum trust management fee</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th>Single</th>
											<td class="white-spacing text-left" rowspan="2">The Bank
												shall deliver the trust funds from consignor of the
												designated investment target (ie the trust amount) from the
												day after the expiration of one year to the redemption date
												of the designated investment target. According to the
												balance of the trust fund account, the trust management fee
												is calculated at an annual rate of two thousandths per year.
												(Parts less than one year are counted based on actual days).
												<br>If the consignor specifies the type of investment
												subscription target as a domestic currency fund, it doesn't
												need to collect the trust management fee.
											</td>
											<td>Equivalent to NTD 100</td>
										</tr>
										<tr>
											<th>Regular Quota/No Quota</th>
											<td>Equivalent to NTD 200</td>
										</tr>
									</tbody>
								</table>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q9 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				Data-toggle="collapse" href="#popup3-9" aria-expanded="true"
				Aria-controls="popup3-9">
				<div class="row">
					<span class="col-1">Q9</span>
					<div class="col-11">
						<span>Purchase unit number allocation day</span>
					</div>
				</div>
			</a>
			<div id="popup3-9" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div style="overflow: auto">
								<table class="question-table" style="width: 100%">
									<thead>
										<tr>
											<th>Fund type</th>
											<th>Purchase unit number allocation time</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th>Domestic Fund</th>
											<td class="white-spacing text-left" valign="top"
												align="center" class=" f13px lineH_01 coR">About 3-5
												financial institution business days after the effective date
												of the investment</td>
										</tr>
										<tr>
											<th>Offshore Fund</th>
											<td class="white-spacing text-left">About 3-7 financial
												institution business days after the effective date of the
												investment</td>
										</tr>
									</tbody>
								</table>
							</div>
							<p>※The above allocation period is only for reference. If the
								fund company operates or each funds have other regulations,
								otherwise, the allocation time is not limited in here.</p>

						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q10 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				Data-toggle="collapse" href="#popup3-10" aria-expanded="true"
				Aria-controls="popup3-10">
				<div class="row">
					<span class="col-1">Q10</span>
					<div class="col-11">
						<span>Redemption amount entry date</span>
					</div>
				</div>
			</a>
			<div id="popup3-10" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div style="overflow: auto">
								<table class="question-table" style="width: 100%">
									<thead>
										<tr>
											<th>Fund type</th>
											<th>Purchase unit number allocation time</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th>Domestic Fund</th>
											<td class="white-spacing text-left">About 3-10 financial
												institution business days after the effective date of the
												redemption <br> (Monetary fund is the next day of the
												financial institution business day)
											</td>
										</tr>
										<tr>
											<th>Offshore Fund</th>
											<td class="white-spacing text-left">About 3-7 financial
												institution business days after the effective date of the
												investment</td>
										</tr>
									</tbody>
								</table>
							</div>
							<p>※ The redemption amount entry date of each fund is not the
								same, and it must be subject to the regulations and operations
								of each fund. The redemption rate is subject to the exchange
								rate of the actual purchase or selling of the foreign exchange
								from consignee during the reasonable processing period.</p>

						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q11 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				Data-toggle="collapse" href="#popup3-11" aria-expanded="true"
				Aria-controls="popup3-11">
				<div class="row">
					<span class="col-1">Q11</span>
					<div class="col-11">
						<span>Exchange instructions</span>
					</div>
				</div>
			</a>
			<div id="popup3-11" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<ol class="ttb-result-list terms">
								<li data-num="1.">Purchase transaction: According to the
									effective date of the transaction (the holiday is postponed to
									the next business day), the spot selling exchange rate of the
									consignee at around 3 pm will be used and the appropriate
									discount will be calculated.</li>
								<li data-num="2.">Redemption transaction: When the
									redemption money is remitted to the consignee's designated
									account, the spot buying exchange rate of the consignee at
									around 10:00 am will be used and the appropriate discount will
									be calculated.</li>
							</ol>
						</li>
					</ul>
				</div>
			</div>

		</div>
		<p>If the contents are wrong due to factors such as typesetting
			and proofreading, the Bank or the fund company shall still be subject
			to the actual operation specifications and contents. The consignor
			may consult the Bank at any time if there has any doubts.</p>
	</div>
</div>