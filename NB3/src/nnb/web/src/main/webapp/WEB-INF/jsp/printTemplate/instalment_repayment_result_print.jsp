<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ include file="../__import_js.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<title>${jspTitle}</title>
<script type="text/javascript">
	$(document).ready(function() {
		window.print();
	});
</script>
</head>
<body class="bodymargin watermark"
	style="-webkit-print-color-adjust: exact">
	<br />
	<div style="text-align: center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br />
	<div style="text-align: center">
		<font style="font-weight: bold; font-size: 1.2em">${jspTitle}</font>
	</div>
	<br />
	<div class="ttb-input-block">
		<div class="ttb-message">
			<span style="color: red; font-weight: bold"><spring:message code= "LB.D0220" />。</span>
		</div>
		<div class="ttb-input-item row">
			<span class="input-title"> <label><h4><spring:message code= "LB.Name" /></h4></label>
			</span> <span class="input-block"> ${CUSNAME} </span>
		</div>
		<div class="ttb-input-item row">
			<span class="input-title"> <label><h4><spring:message code= "LB.D0581" /></h4></label>
			</span> <span class="input-block"> ${CUSTID} </span>
		</div>
		<div class="ttb-input-item row">
			<span class="input-title"> <label><h4><spring:message code= "LB.D0205" /></h4></label>
			</span> <span class="input-block"> <c:if test="${HPHONE !=''}">
									<spring:message code= "LB.D0206" />：${HPHONE} <br>
				</c:if> <c:if test="${MPFONE !=''}">
									<spring:message code= "LB.D0207" />：${MPFONE}<br>
				</c:if> <c:if test="${OPHONE !=''}">
									<spring:message code= "LB.D0094" />：${OPHONE} <br>
				</c:if>
			</span><br>
		</div>
		<div class="ttb-input-item row">
			<span class="input-title"> <label><h4><spring:message code= "LB.Credit_card_number" /></h4></label>
			</span> <span class="input-block"> ${CARDNUM} </span>
		</div>
		<div class="ttb-input-item row">
			<span class="input-title"> <label><h4><spring:message code= "LB.D0209" /></h4></label>
			</span> <span class="input-block"> ${CURRBAL} </span>
		</div>
		<div class="ttb-input-item row">
			<span class="input-title"> <label><h4><spring:message code= "LB.D0210" /></h4></label>
			</span> <span class="input-block"> ${RATE}%&nbsp;${POTDESC} </span>
		</div>
		<div class="ttb-input-item row">
			<span class="input-title"> <label><h4><spring:message code= "LB.D0211" /></h4></label>
			</span> <span class="input-block" id="AMOUNT_IUPUT">
				${AMOUNT}&nbsp;&nbsp;&nbsp;<spring:message code= "LB.Dollar" /> </span>
		</div>

		<div class="ttb-input-item row">
			<span class="input-title"> <label><h4><spring:message code= "LB.D0212" /></h4></label>
			</span> <span class="input-block" id="PERIOD_TEXT"> ${PERIOD}&nbsp;<spring:message code= "LB.W0854" />
			</span>
		</div>
		<div class="ttb-input-item row">
			<span class="input-title"> <label><h4><spring:message code= "LB.D0213" /></h4></label>
			</span> <span class="input-block"> ${APPLY_RATE}% </span>
		</div>
		<div class="ttb-input-item row">
			<span class="input-title"> <label><h4><spring:message code= "LB.D0214" /></h4></label>
			</span> <span class="input-block"> ${FIRST_AMOUNT}&nbsp;<spring:message code= "LB.Dollar" /><br>
				(<spring:message code= "LB.X1583" />)
			</span>
		</div>
		<div class="ttb-input-item row">
			<span class="input-title"> <label><h4><spring:message code= "LB.D0216" /></h4></label>
			</span> <span class="input-block"> ${FIRST_INTEREST}&nbsp;<spring:message code= "LB.Dollar" /><br>
				(<spring:message code= "LB.D0217" />)
			</span>
		</div>
	</div>
	<div class="ttb-message text-left" style="width: 85%;">
		<span><spring:message code= "LB.X0880" />：1.<spring:message code= "LB.X0881" />/span>
		<br> 
		<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.<spring:message code= "LB.X0874" /></span><br> 
		<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.<spring:message code= "LB.X0883" /></span><br>
		<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4.<spring:message code= "LB.X0884" /></span>
	</div>
	<table class="print">
		<thead>
			<tr>
				<th data-title="<spring:message code= "LB.D0212" />" rowspan="2"><spring:message code="LB.D0212" /></th>
				<th data-title="<spring:message code= "LB.D0199" />" colspan="5" class="text-center"><spring:message code="LB.D0199" /></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th></th>
				<td nowrap><spring:message code= "LB.X0875" /></td>
				<td nowrap><spring:message code= "LB.X0876" /></td>
				<td nowrap><spring:message code= "LB.X0877" /></td>
				<td nowrap><spring:message code= "LB.X0878" /></td>
				<td nowrap><spring:message code= "LB.X0879" /></td>
			</tr>
			<tr data-expanded="false">
				<td class="expand">6<spring:message code= "LB.W0854" /></td>
				<td><spring:message code= "LB.X0858" />1.25%</td>
				<td><spring:message code= "LB.X0858" />1.25%</td>
				<td><spring:message code= "LB.X0858" />1.25%</td>
				<td><spring:message code= "LB.X0859" />1.25%</td>
				<td></td>
			</tr>
			<tr data-expanded="false">
				<td class="expand">12<spring:message code= "LB.W0854" /></td>
				<td><spring:message code= "LB.X0858" />1%</td>
				<td><spring:message code= "LB.X0858" />1%</td>
				<td><spring:message code= "LB.X0858" />1%</td>
				<td><spring:message code= "LB.X0859" />1%</td>
				<td></td>
			</tr>
			<tr data-expanded="false">
				<td class="expand">18<spring:message code= "LB.W0854" /></td>
				<td><spring:message code= "LB.X0858" />0.75%</td>
				<td><spring:message code= "LB.X0858" />0.75%</td>
				<td><spring:message code= "LB.X0858" />0.75%</td>
				<td><spring:message code= "LB.X0859" />0.75%</td>
				<td></td>
			</tr>
			<tr data-expanded="false">
				<td class="expand">24<spring:message code= "LB.W0854" /></td>
				<td><spring:message code= "LB.X0858" />0.5%</td>
				<td><spring:message code= "LB.X0858" />0.5%</td>
				<td><spring:message code= "LB.X0858" />0.5%</td>
				<td><spring:message code= "LB.X0859" />0.5%</td>
				<td></td>
			</tr>
			<tr data-expanded="false">
				<td class="expand">30<spring:message code= "LB.W0854" /></td>
				<td><spring:message code= "LB.X0858" />0.25%</td>
				<td><spring:message code= "LB.X0858" />0.25%</td>
				<td><spring:message code= "LB.X0858" />0.25%</td>
				<td><spring:message code= "LB.X0859" />0.25%</td>
				<td></td>
			</tr>
			<tr data-expanded="false">
				<td class="expand">36<spring:message code= "LB.W0854" /></td>
				<td><spring:message code= "LB.X0858" />0.2%</td>
				<td><spring:message code= "LB.X0858" />0.2%</td>
				<td><spring:message code= "LB.X0858" />0.2%</td>
				<td><spring:message code= "LB.X0859" />0.2%</td>
				<td></td>
			</tr>
			<tr data-expanded="false">
				<td class="expand">48<spring:message code= "LB.W0854" /></td>
				<td><spring:message code= "LB.X0858" />0.15%</td>
				<td><spring:message code= "LB.X0858" />0.15%</td>
				<td><spring:message code= "LB.X0858" />0.15%</td>
				<td><spring:message code= "LB.X0859" />0.2%</td>
				<td></td>
			</tr>
			<tr data-expanded="false">
				<td class="expand">60<spring:message code= "LB.W0854" /></td>
				<td><spring:message code= "LB.X0858" />0.05%</td>
				<td><spring:message code= "LB.X0858" />0.05%</td>
				<td><spring:message code= "LB.X0858" />0.05%</td>
				<td><spring:message code= "LB.X0859" />0.4%</td>
				<td></td>
			</tr>
		</tbody>
	</table>
	<br />
</body>
</html>