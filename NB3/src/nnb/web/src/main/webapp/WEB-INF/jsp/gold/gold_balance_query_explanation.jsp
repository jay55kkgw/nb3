<%@ page language="java" contentType="text/html; charset=BIG5"
    pageEncoding="BIG5"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=BIG5">
<title>參考平均成本說明</title>
</head>
<body>
<h2 style="text-align: center">參考平均成本說明</h2>
<br><br>
假設A客戶4月30日黃金存摺結存數量為0公克，
5月1日買進黃金10公克(成本1000元/公克)，
5月2日買進黃金10公克(成本1020元/公克)，
5月3日回售黃金10公克，
5月4日買進黃金5公克(成本1025元/公克)，
5月4日A客戶黃金存摺結存數量為15公克，其每公克原始投入參考平均成本為1015元，計算如下：
5月1日及5月2日共買進黃金20公克，黃金存摺結存數量為20公克<br>
(1000*10+1020*10)/20=1010<br>
<br>
5月3日回售黃金10公克，黃金存摺結存數量為10公克<br>
<br>
(1010*20-1010*10)/10=1010<br>
<br>
5月4日買進黃金5公克，黃金存摺結存數量為15公克<br>
<br>
(1010*10+1025*5)/15=1015
</body>
</html>