<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<section id="main-content" class="container position-absolute" style="display:none">
	<div class="pupup-block">

		<div class="card-block shadow-box terms-pup-blcok">
			<!-- n_e_e_d t_o f_i_x p_o_s_i_t_i_o_n -->
			<button type="button" class="popup-close-btn d-none"
				data-dismiss="modal" aria-label="Close">×</button>
			<h2 class="ttb-pup-h2">金融資訊</h2>

			<nav class="nav card-select-block text-center d-block" id="nav-tab"
				role="tablist">
				<input type="button" class="nav-item ttb-sm-btn active"
					id="nav-trans-1-tab" data-toggle="tab" href="#nav-trans-1"
					role="tab" aria-selected="false" value="匯率" /> 
				<input type="button" class="nav-item ttb-sm-btn" id="nav-trans-2-tab"
					data-toggle="tab" href="#nav-trans-2" role="tab"
					aria-selected="true" value="利率" /> 
				<input type="button" class="nav-item ttb-sm-btn" id="nav-trans-3-tab" data-toggle="tab"
					href="#nav-trans-3" role="tab" aria-selected="true" value="基金淨值" />
				<input type="button" class="nav-item ttb-sm-btn"
					id="nav-trans-4-tab" data-toggle="tab" href="#nav-trans-4"
					role="tab" aria-selected="true" value="黃金存摺" /> 
			</nav>
			<div class="col-12 tab-content" id="nav-tabContent">
				<div class="ttb-input-block tab-pane fade show active"
					id="nav-trans-1" role="tabpanel" aria-labelledby="nav-profile-tab">
					<div class="card-detail-block">
						<div class="card-center-block d-flex">
							<!-- 當日匯率查詢 -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="https://www.tbb.com.tw/exchange_rate" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>當日<br />匯率查詢
								</span>
								</a>
							</div>
							<!-- 歷史匯率查詢 -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="https://www.tbb.com.tw/web/guest/-1" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>歷史<br />匯率查詢
								</span>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="ttb-input-block tab-pane fade" id="nav-trans-2"
					role="tabpanel" aria-labelledby="nav-profile-tab">
					<div class="card-detail-block">
						<div class="card-center-block d-flex">
							<!-- 新台幣存款利率查詢 -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="https://www.tbb.com.tw/web/guest/-82" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>新台幣存款<br />利率查詢
								</span>
								</a>
							</div>
							<!-- 新台幣放款利率查詢 -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="https://www.tbb.com.tw/web/guest/-83" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>新台幣放款<br />利率查詢
								</span>
								</a>
							</div>
							<!-- 外幣存款利率查詢 -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="https://www.tbb.com.tw/web/guest/-84" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>外幣存款<br />利率查詢
								</span>
								</a>
							</div>
							<!-- 外幣放款利率查詢 -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="https://www.tbb.com.tw/web/guest/-85" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>外幣放款<br />利率查詢
								</span>
								</a>
							</div>
							<!-- 債券及票券利率查詢 -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="https://www.tbb.com.tw/web/guest/-86" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>債券及票券<br />利率查詢
								</span>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="ttb-input-block tab-pane fade" id="nav-trans-3"
					role="tabpanel" aria-labelledby="nav-profile-tab">
					<div class="card-detail-block">
						<div class="card-center-block d-flex">
							<!-- 國內基金淨值查詢 -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="/nb3/CUSTOMER/SERVICE/FundWeb?TYPE=D" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank" rel="noopener noreferrer"> <span>國內基金<br />淨值查詢
								</span>
								</a>
							</div>
							<!-- 國外基金淨值查詢 -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="/nb3/CUSTOMER/SERVICE/FundWeb?TYPE=F" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank" rel="noopener noreferrer"> <span>國外基金<br />淨值查詢
								</span>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="ttb-input-block tab-pane fade" id="nav-trans-4"
					role="tabpanel" aria-labelledby="nav-profile-tab">
					<div class="card-detail-block">
						<div class="card-center-block d-flex">
							<!-- 當日黃金牌價查詢 -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="${__ctx}/GOLD/OUT/PASSBOOK/day_price_query" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>當日<br />黃金牌價查詢
								</span>
								</a>
							</div>
							<!-- 歷史黃金牌價查詢 -->
							<div class="ttb-pup-block square-style" role="tab">
								<a href="${__ctx}/GOLD/OUT/PASSBOOK/history_price_query" role="button"
									class="ttb-pup-title ttb-pup-link d-block" target="_blank"> <span>歷史<br />黃金牌價查詢
								</span>
								</a>
							</div>
						</div>
					</div>
				</div>
				<input type="BUTTON" class="ttb-button btn-flat-gray" value="返回" name="" onclick="$('#main-content').hide();">
			</div>
		</div>
	</div>
</section>
