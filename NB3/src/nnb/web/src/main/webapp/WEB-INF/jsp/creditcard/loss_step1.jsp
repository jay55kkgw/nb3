<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
    
<script type="text/javascript">
	$(document).ready(function(){
		setTimeout("initDataTable()",100);
		//initFootable();		
		$("#printbtn").click(function(){
			var i18n = new Object();
			i18n['jspTitle']='<spring:message code="LB.Card_Report_Loss" />'
			var params = {
					"jspTemplateName":"loss_step1_print",
					"jspTitle":i18n['jspTitle'],
					"SUCCEEDCOUNT":"${card_loss_step1.data.SUCCEEDCOUNT}",
					"FAILEDCOUNT":"${card_loss_step1.data.FAILEDCOUNT}",
					"SYSTIME":'${card_loss_step1.data.CMQTIME}',
			}
			openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);

		});
	});
</script>
</head>
<body>
   	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 信用卡     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Credit_Card" /></li>
    <!-- 信用卡掛失     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Card_Report_Loss" /></li>
		</ol>
	</nav>

	
	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
	
		
	<main class="col-12">
		<section id="main-content" class="container"><!-- 主頁內容  -->
		
			<h2><spring:message code="LB.Card_Report_Loss" /></h2><!-- 信用卡掛失 -->
			
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="${__ctx}/CREDIT/APPLY/card_loss_step1">
				<div class="main-content-block row">
					<div class="col-12 printClass">
						<div>
							<ul class="ttb-result-list">
								<li>
								<h3>
									<spring:message code="LB.System_time" /><!-- 系統時間 -->
								</h3>
									<!-- 系統時間  -->
									<p>
										${card_loss_step1.data.CMQTIME}
									</p>
								</li>
							</ul>
							<c:if test="${card_loss_step1.data.SUCCEEDCOUNT !='0'}">
							<table class="stripe table-striped ttb-table dtable">
								<thead>
								<tr>
									<th data-title='<spring:message code="LB.Status" />'><spring:message code="LB.Status" /></th><!-- 狀態 -->
									<th data-title='<spring:message code="LB.Card_kind" />'><spring:message code="LB.Card_kind" /></th><!-- 卡片種類 -->
									<th data-title='<spring:message code="LB.Card_number" />' data-breakpoints="xs"><spring:message code="LB.Card_number" /></th><!-- 卡號 -->
								</tr>
								</thead>
								<c:forEach var="suList" items="${card_loss_step1.data.SUCCEED}">
								<tbody>
					            <tr>
					                <td class="text-center" style="color:green;font-weight: bold">
					                	<!-- 狀態  -->
					                		<c:if test="${suList.STATUS =='掛失成功'}">
											<spring:message code="LB.Report_successful" /><!-- 掛失成功 -->
											</c:if>
									</td>
					                <td class="text-center">
					                	<!-- 卡片種類  -->
					               		${suList.TYPENAME}
					                </td>
					                <td class="text-center">
					                	<!-- 卡號  -->
					               		${suList.CARDNUM_COVER}
					                </td>
					            </tr>
					            </tbody>
					            </c:forEach>
							</table>
							</c:if>
							<c:if test="${card_loss_step1.data.FAILEDCOUNT !='0'}">
							<table class="stripe table-striped ttb-table dtable" >
								<thead>
								<tr>
									<th data-title='<spring:message code="LB.Status" />'><spring:message code="LB.Status" /></th><!-- 狀態 -->
									<th data-title='<spring:message code="LB.Card_kind" />'><spring:message code="LB.Card_kind" /></th><!-- 卡片種類 -->
									<th data-title='<spring:message code="LB.Card_number" />' data-breakpoints="xs"><spring:message code="LB.Card_number" /></th><!-- 卡號 -->
								</tr>
								</thead>
								<c:forEach var="flList" items="${card_loss_step1.data.FAILED}">
								<tbody>
					            <tr>
					            	
					                <td class="text-center" style="color:red;font-weight: bold">
					                	<!-- 狀態  -->
					                	<c:if test="${flList.STATUS =='掛失失敗'}">
											<spring:message code="LB.Report_loss_fail" /><!-- 掛失失敗 -->
										</c:if>
									</td>
					                
					                <td class="text-center">
					                	<!-- 卡片種類  -->
					               		${flList.TYPENAME}
					                </td>
					                <td class="text-center">
					                	<!-- 卡號  -->
					               		${flList.CARDNUM_COVER}
					                </td>
					            </tr>
					            </tbody>
					            </c:forEach>
							</table>
							</c:if>
						</div>
						
						<input type="button" id="printbtn" class="ttb-button btn-flat-orange" name="CMSUBMIT" value="<spring:message code="LB.Print" />" ><!-- 列印 -->
					</div>
				</div>
				<c:if test="${card_loss_step1.data.SUCCEEDCOUNT !='0'}">
						<ol class="description-list list-decimal">
									<p><spring:message code="LB.Description_of_page" /><!-- 說明 --></p>
								        <li>
								        	<spring:message code= "LB.Loss_P3_D1" />
								        </li>
									</ol>
					</c:if>
					<c:if test="${card_loss_step1.data.FAILEDCOUNT !='0'}">
						<ol class="description-list list-decimal">
									<p><spring:message code="LB.Description_of_page" /><!-- 說明 --></p>
								        <li><spring:message code="LB.Loss_P3_D2" />
								        <!-- 系統忙碌中請稍後再試，或洽本行客服02-2357-7171、0800-01-7171。 -->
								        </li>
									</ol>
					</c:if>
			</form>
		</section>
	</main><!-- main-content END --> 
</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>
