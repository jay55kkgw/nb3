<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 取消約定轉入帳號     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0243" /></li>
		</ol>
	</nav>

		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 --> 
 		<div class="content row">
 		   <%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
 		
 		<main class="col-12">	

		<!-- 		主頁內容  -->

			<section id="main-content" class="container">
		
				<h2><spring:message code="LB.D0243" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
<!-- 				<div id="step-bar"> -->
<!-- 					<ul> -->
<!-- 						<li class="finished">輸入資料</li> -->
<!-- 						<li class="finished">確認資料</li> -->
<!-- 						<li class="active">交易完成</li> -->
<!-- 					</ul> -->
<!-- 				</div> -->
				<div class="main-content-block row">
					<div class="col-12 tab-content printClass"> 
						<div class="ttb-input-block">
						<c:set var="dataSet" value="${predesignated_account_cancellation.data}" />
<!-- 								系統時間-->
							<div class="ttb-input-item row">
							<span class="input-title">
									<label>
									<spring:message code="LB.System_time" />
									</label>
								</span>
								<span class="input-block">
									<label>
									${dataSet.CMQTIME}		
									</label>
								</span>
							</div>
<!--								 回應訊息     -->
							<div class="ttb-input-item row">
							<span class="input-title">
								<label>
									<spring:message code="LB.D0191" />
								</label>
							</span>
							<span class="input-block">
								<label>
									<c:if test="${!dataSet.MSG.equals('')}">
									
										<c:choose>
											<c:when test="${ dataSet.MSG == 'LB.D0237'}">
												<spring:message code="LB.X0920"/>
											</c:when>
											<c:otherwise>
												${dataSet.MSG}
									 		</c:otherwise>
									  	</c:choose>
								
									</c:if>
								</label>
							</span>
							</div>
						<c:if test="${dataSet.CCY == ''}">
<!-- 					           	 統一編號    -->
							<div class="ttb-input-item row">
							<span class="input-title">
								<label>
									 <spring:message code="LB.D0621" />	
								</label>
							</span>
							<span class="input-block">
								<label>
									${dataSet.CUSIDN}
								</label>
							</span>
							</div>
<!-- 					               行庫別	-->								
							<div class="ttb-input-item row">
							<span class="input-title">
								<label>
									<spring:message code="LB.X0269" />
								</label>
							</span>
							<span class="input-block">
								<label>
									${dataSet.BNKCOD}
								</label>
							</span>
							</div>
						</c:if>
<!-- 					            帳號     -->
							<div class="ttb-input-item row">
							<span class="input-title">
								<label>
									<spring:message code="LB.Account" />
								</label>
							</span>
							<span class="input-block">
								<label>
									${dataSet.ACN}
								</label>
							</span>
							</div>	
						<c:if test="${dataSet.CCY != ''}">
<!-- 					  	  幣別     -->
							<div class="ttb-input-item row">
							<span class="input-title">
								<label>
									<spring:message code="LB.Currency" />
								</label>
							</span>
							<span class="input-block">
								<label>
									${dataSet.CCY}
								</label>
							</span>
							</div>
						</c:if>					
					</div>
					<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
					</div>
				</div>
			<div class="text-left">
				<ol class="list-decimal description-list">
					<p><spring:message code="LB.Description_of_page" /></p>
					<li><font color="red"><B><spring:message code="LB.Predesignated_Account_Cancellation_P3_D1" /></B></font></li>
					<li><font color="red"><B><spring:message code="LB.Predesignated_Account_Cancellation_P3_D2" /></B></font></li>
					<li><spring:message code="LB.Predesignated_Account_Cancellation_P3_D3" /></li>
				</ol>
			</div>
			</section>			
			<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
	
	<script type="text/javascript">
			$(document).ready(function(){
				// HTML載入完成後開始遮罩
				setTimeout("initBlockUI()", 50);
				// 開始跑下拉選單並完成畫面
				setTimeout("init()", 400);
				// 解遮罩
				setTimeout("unBlockUI(initBlockId)", 500);
			});
			//<!-- 取消約定轉入帳號 -->
			function init(){
				$("#printbtn").click(function(){
					var params = {
						"jspTemplateName":"predesignated_account_cancellation_print",
						"jspTitle":"<spring:message code= "LB.D0243" />",
						"CMQTIME":"${dataSet.CMQTIME}",
						"MSG":"${dataSet.MSG}",
						"CCY":"${dataSet.CCY}",
						"CUSIDN":"${dataSet.CUSIDN}",
						"BNKCOD":"${dataSet.BNKCOD}",
						"ACN":"${dataSet.ACN}"
					};
					openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
				});


			}
	</script>
</body>
</html>