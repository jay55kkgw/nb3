<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<c:set var="res" value="${__ctx}/site/admin/main"/>
<c:set var="txtTitle" value="Form Test"/>

 
<t:block template="site_admin_page_template">
    <jsp:attribute name="__pageTitle">Site Administration</jsp:attribute>
    <jsp:attribute name="__siteName">Site Administration</jsp:attribute>    
    <jsp:attribute name="__userDisplayInfo">${USER_ID}</jsp:attribute>
    <jsp:attribute name="__appName">${APP_NAME}</jsp:attribute>
    <jsp:attribute name="__appDesc"></jsp:attribute>
    <jsp:attribute name="__funcName">${FUNC_NAME}</jsp:attribute>
    <jsp:attribute name="__userLocale">zh-TW</jsp:attribute>

 	<jsp:attribute name="__sidebarMenu">
 		${USER_MENU}	
 	</jsp:attribute>

    <jsp:attribute name="__content" trim="false">
          <div class="row">
		  
            <!-- columns -->
            <div class="col-md-12">
						
			  <!-- box styles : box-primary, box-success, box-danger, box-info, box-warning-->
              <div class="box box-primary">
			  
				<!-- general form elements -->
                <div class="box-header">
                  <h3 class="box-title">${FUNC_NAME}</h3>
                </div><!-- /.box-header -->
				
				
                <div class="box-body">
				  
				  <!-- form -->
				  <jsp:include page='__auto_form.jsp'/>
				  
				  
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        
    </jsp:attribute>
        
    <jsp:attribute name="__footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0
        </div>
        <strong>Copyright &copy; 1953 <a href="#">FSTOP Inc</a>.</strong> All rights reserved.      	
    </jsp:attribute>

    
    <jsp:attribute name="__js">
    </jsp:attribute>
    
</t:block>

