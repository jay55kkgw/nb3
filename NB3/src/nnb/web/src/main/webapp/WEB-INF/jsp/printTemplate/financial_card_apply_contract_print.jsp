<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function(){
			window.print();
		});
	</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
<body>
	<br/><br/>
	<div style="text-align:center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br/><br/>
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<br/>
	
	<table class="print">
		<form id="formId" method="post" action="${__ctx}/ONLINE/SERVING/financial_card_apply">
				
			<div class="main-content-block row">
				<div class="col-12">
					<div class="ttb-message">
					<p>網路銀行業務總契約書<br>
					</div>
						<div style="padding-right: 10px;padding-left: 15px;">
						<p style="text-align:left">為確保您的權益，請詳細閱讀本約定條款所載事項，您如接受本約定條款請按同意約定條款鍵，以完成申請作業，您如不同意條款內容，則請按取消鍵，
						本行將不受理您的申請。
						</p>
						</div>
					</p>
				
                       <!-- 壹、共通約定事項 START -->
					<div class="ttb-message">
                           <p>壹、共通約定事項</p>
                           <span></span>
                       </div>
                       <ul class="ttb-result-list" style="list-style: decimal;">
                           <ul><span class="input-subtitle subtitle-color">第一條、貴行資訊</span>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
									一、貴行名稱： 臺灣中小企業銀行 <br>

									二、申訴及客服專線：0800-00-7171#5(申訴)，#1(客服)  <br>


									三、網址：https://www.tbb.com.tw<br>

									四、地址： 台北市塔城街30號 <br>

									五、傳真號碼：02-2550-8338<br>
									
									六、貴行電子信箱：臺灣企銀網站 https://www.tbb.com.tw 客服信箱 （tbb@mail.tbb.com.tw） <br>

							</ul>
						</ul>
						
						
						<ul>
							<span class="input-subtitle subtitle-color">第二條、適用範圍</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							本契約係網路銀行業務服務之一般性共同約定，除個別契約另有約定外，悉依本契約之約定。個別契約不得牴觸本契約。但個別契約對立約人之保護更有利者，從其約定，所指之保護，不含貴行基於業務考量所為之各項作業推廣措施。	  
							</ul>
						</ul>
                           <ul><span class="input-subtitle subtitle-color">第三條、名詞定義</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
								一、「網路銀行業務」：指立約人端電腦經由網際網路或行動網路與貴行電腦連線，無須親赴貴行櫃台，即可直接取得貴行所提供之各項金融服務。<br>
								二、「電子文件」：指貴行或立約人經由網路連線傳遞之文字、聲音、圖片、影像、符號或其他資料，以電子或其他以人之知覺無法直接認識之方式，所製成足以表示其用意之紀錄，而供電子處理之用者。 <br>
								三、「數位簽章」：指將電子文件以數學演算法或其他方式運算為一定長度之數位資料，以簽署人之私密金鑰對其加密，形成電子簽章，並得以公開金鑰加以驗證者。<br>
								四、「憑證」：指載有簽章驗證資料，用以確認簽署人身分、資格之電子形式證明。<br>
								五、「私密金鑰」：係指具有配對關係之數位資料中，由簽署人保有，用以製作數位簽章者。<br>
								六、「公開金鑰」：係指具有配對關係之數位資料中，對外公開，用以驗證數位簽章者。<br>
								七、「行動銀行」：指立約人下載貴行所提供之行動銀行軟體，並使用智慧型手機經由網際網路或行動網路與貴行電腦連線，無須親赴貴行櫃台，即可直接取得貴行所提供之各項金融服務。<br>
								八、「服務時間」：除了另行公告服務時間之項目外，提供二十四小時服務。<br>
							</ul>
						</ul>
                           <ul><span class="input-subtitle subtitle-color">第四條、網頁之確認</span><br>
                           	<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
                           	立約人使用網路銀行前，請先確認網路銀行正確之網址，才使用網路銀行服務；如有疑問，請電客服電話詢問。貴行應以一般民眾得認知之方式，告知立約人網路銀行應用環境之風險。貴行應盡善良管理人之注意義務，隨時維護網站的正確性與安全性，並隨時注意有無偽造之網頁，以避免立約人之權益受損。 
							</ul>
						</ul>
                           <ul><span class="input-subtitle subtitle-color">第五條、服務項目</span><br>
                           	<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
                           	貴行應於本契約載明提供之服務項目，如於網路銀行網站呈現相關訊息者，並應確保該訊息之正確性，其對立約人所負之義務不得低於網站之內容。 
							</ul>
						</ul>
                           <ul><span class="input-subtitle subtitle-color">第六條、連線所使用之網路</span><br>
                           	<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
                           	貴行及立約人同意使用網路進行電子文件傳送及接收。貴行及立約人應分別就各項權利義務關係與各該網路業者簽訂網路服務契約，並各自負擔網路使用之費用。
							</ul>
						</ul>
						
						<ul><span class="input-subtitle subtitle-color">第七條、電子文件之接收與回應</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							貴行接收含數位簽章或經貴行及立約人同意用以辨識身分之電子文件後，除查詢之事項外，貴行應提供該交易電子文件中重要資訊之網頁供立約人再次確認後，即時進行檢核及處理，並將檢核及處理結果，以電子文件之方式通知立約人。貴行或立約人接收來自對方任何電子文件，若無法辨識其身分或內容時，視為自始未傳送。但貴行可確定立約人身分時，應立即將內容無法辨識之事實，以電子文件之方式通知立約人。
							</ul>
						</ul>
						
						<ul><span class="input-subtitle subtitle-color">第八條、電子文件之不執行</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
								如有下列情形之一，貴行得不執行任何接收之電子文件：
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 25px;margin-right: 10px;">

									一、有具體理由懷疑電子文件之真實性或所指定事項之正確性者。<br>

									二、貴行依據電子文件處理，將違反相關法令之規定者。<br>

									三、貴行因立約人之原因而無法於帳戶扣取立約人所應支付之費用者。<br>
				
							</ul>
								貴行不執行前項電子文件者，應同時將不執行之理由及情形，以電子文件之方式通知立約人，立約人受通知後得以電話或電子郵件方式向貴行確認。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第九條、電子文件交換作業時限</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							電子文件係由貴行電腦自動處理，立約人發出電子文件，經立約人依第七條第一項貴行提供之再確認機制確定其內容正確性後，傳送至貴行後即不得撤回。但未到期之預約交易在貴行規定之期限內，得撤回、修改。<br>若電子文件經由網路傳送至貴行後，於貴行電腦自動處理中已逾貴行營業時間時（營業時間：為星期一至星期五（例假日除外）及政府公告補上班日，上午九時至下午三時三十分），貴行應即以電子文件通知立約人，該筆交易將改於次一營業日處理或依其他約定方式處理。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第十條、系統故障之權宜處理</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人同意當連線設備或貴行系統或第三人網路服務業系統發生故障時，貴行得採行必要之權宜措施處理。
							</ul>
						</ul>
						
						<ul><span class="input-subtitle subtitle-color">第十一條、費用</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人自使用本契約服務之日起，同意依貴行「一般網路銀行收費標準表」(如附件)所訂定之收費標準繳納相關費用，並授權貴行自立約人之帳戶內自動扣繳；如未記載者，貴行不得收取。<br>前項收費標準於訂約後如有調整者，貴行應於貴行網站之明顯處公告其內容，並以電子文件之方式使立約人得知（以下稱通知）調整之內容。<br>第二項之調整如係調高者，貴行應於網頁上提供立約人表達是否同意費用調高之選項。立約人未於調整生效日前表示同意者，貴行將於調整生效日起暫停立約人使用網路銀行一部或全部之服務。立約人於調整生效日後，同意費用調整者，貴行應立即恢復網路銀行契約相關服務。<br>前項貴行之公告及通知應於調整生效六十日前為之，且調整生效日不得早於公告及通知後次一年度之起日。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第十二條、立約人軟硬體安裝與風險</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人申請使用本契約之服務項目，應自行安裝所需之電腦軟體、硬體，以及其他與安全相關之設備。安裝所需之費用及風險，由立約人自行負擔。<br>第一項軟硬體設備及相關文件如係由貴行所提供，貴行僅同意立約人於約定服務範圍內使用，不得將之轉讓、轉借或以任何方式交付第三人。貴行並應於網站及所提供軟硬體之包裝上載明進行本服務之最低軟硬體需求，且負擔所提供軟硬體之風險。<br>立約人於契約終止時，如貴行要求返還前項之相關設備，應以契約特別約定者為限。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第十三條、立約人連線與責任</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人申請使用本契約之服務項目，應自行安裝所需之電腦軟體、硬體，以及其他與安全相關之設備。安裝所需之費用及風險，由立約人自行負擔。<br>貴行與立約人有特別約定者，必須為必要之測試後，始得連線。<br>立約人對貴行所提供之使用者代號、密碼、憑證及其它足以識別身分之工具，應負保管之責。<br>立約人輸入前項密碼連續錯誤達五次時，貴行電腦即自動停止立約人使用本契約之服務。立約人如擬恢復使用，應依約定辦理相關手續。惟網路銀行未屆轉帳日之各項預約轉帳仍屬有效，將於轉帳日依預約指示轉帳。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第十四條、交易核對</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							貴行於每筆交易指示處理完畢後，以電子文件、電話或書面之方式通知立約人，立約人應核對其結果有無錯誤。如有不符，應於使用完成之日起四十五日內，以電話或電子郵件方式通知貴行查明。<br>貴行應於每月對立約人以書面或電子文件方式寄送上月之交易對帳單（該月無交易時不寄）。立約人核對後如認為交易對帳單所載事項有錯誤時，應於收受之日起四十五日內，以電話或電子郵件方式通知貴行查明。<br>貴行對於立約人之通知，應即進行調查，並於通知到達貴行之日起三十日內，將調查之情形或結果以書面方式覆知立約人。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第十五條、跨行／跨網交易</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							電子轉帳為跨行或跨網交易時，貴行執行電子訊息之傳送後，不負責他行或交換中心之行為或不行為及因該行為或不行為所造成之損害。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第十六條、電子文件錯誤之處理</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人利用本契約之服務，其電子文件如因不可歸責於立約人之事由而發生錯誤時，貴行應協助立約人更正，並提供其他必要之協助。
							<br>
							前項服務因可歸責於貴行之事由而發生錯誤時，貴行應於知悉時，立即更正，並同時以電子文件或雙方約定之方式通知立約人。
							<br>
							立約人利用本契約之服務，其電子文件因可歸責於立約人之事由而發生錯誤時，倘屬立約人申請或操作轉入之金融機構代號、存款帳號或金額錯誤，致轉入他人帳戶或誤轉金額時，一經立約人通知貴行，貴行應即辦理以下事項：
								<ul style="list-style: decimal; list-style-position: inside;margin-left: 25px;margin-right: 10px;">
								一、依據相關法令提供該筆交易之明細及相關資料。
								<br>
								二、通知轉入行協助處理。
								<br>
								三、回報處理情形。
								</ul>
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第十七條、電子文件之合法授權與責任</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							貴行及立約人應確保所傳送至對方之電子文件均經合法授權。
							<br>
							貴行或立約人於發現有第三人冒用或盜用使用者代號、密碼、憑證、私密金鑰，或其他任何未經合法授權之情形，應立即以電話、電子文件或書面方式通知他方停止使用該服務並採取防範之措施。
							<br>
							貴行接受前項通知前，對第三人使用該服務已發生之效力，由貴行負責。但有下列任一情形者，不在此限：
								<ul style="list-style: decimal; list-style-position: inside;margin-left: 25px;margin-right: 10px;">

										一、貴行能證明立約人有故意或過失。<br>

										二、貴行依電子文件、電話或書面方式通知交易核對資料或帳單後超過四十五日。惟立約人有特殊事由致無法通知者，以該特殊事由結束日起算四十五日，但貴行有故意或過失者，不在此限。
										<br>針對第二項冒用、盜用事實調查所生之鑑識費用由貴行負擔。

								</ul>
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第十八條、資料系統安全</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							貴行及立約人應各自確保所使用資訊系統之安全，防止非法入侵、取得、竄改、毀損業務紀錄或立約人個人資料。
							<br>
							第三人破解貴行資訊系統之保護措施或利用資訊系統之漏洞爭議，由貴行就該事實不存在負舉證責任。
							<br>
							第三人入侵貴行資訊系統對立約人所造成之損害，由貴行負擔。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第十九條、保密義務</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							除其他法律規定外，貴行應確保所交換之電子文件因使用或執行本契約服務而取得立約人之資料，不洩漏予第三人，亦不可使用於與本契約無關之目的，且於經立約人同意告知第三人時，應使第三人負本條之保密義務。
							<br>
							前項第三人如不遵守此保密義務者，視為本人義務之違反。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第二十條、資料完整</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人使用貴行所提供之各項服務，如需以書面方式為之者，立約人仍須補足書面資料後方屬完成手續，惟在尚未補足書面資料前，仍應對於完成訊息傳送之交易負責。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第二十一條、損害賠償責任</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							貴行及立約人同意依本契約傳送或接收電子文件，因可歸責於當事人一方之事由，致有遲延、遺漏或錯誤之情事，而致他方當事人受有損害時，該當事人應就他方所生之損害負賠償責任。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第二十二條、紀錄保存</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							貴行及立約人應保存所有交易指示類電子文件紀錄，並應確保其真實性及完整性。<br>貴行對前項紀錄之保存，應盡善良管理人之注意義務。保存期限為五年以上，但其他法令有較長規定者，依其規定。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第二十三條、電子訊息之效力</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							貴行及立約人同意以電子文件作為表示方法，依本契約交換之電子文件，其效力與書面文件相同。但法令另有排除適用者，不在此限。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第二十四條、立約人終止契約及變更本服務之內容</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人得隨時終止本契約及變更本服務之內容，但應親自、書面或雙方約定方式辦理。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第二十五條、銀行終止契約</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							貴行終止本契約時，須於終止日三十日前以書面通知立約人。<br>立約人如有下列情事之一者，貴行得隨時以書面或雙方約定方式通知立約人終止本契約：
							<br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 25px;margin-right: 10px;">
							一、立約人未經貴行同意，擅自將契約之權利或義務轉讓第三人者。
							<br>
							二、立約人依破產法聲請宣告破產或消費者債務清理條例聲請更生、清算程序者。
							<br>
							三、立約人違反本「共通約定事項」（十七）至（十九）之規定者。
							<br>
							四、立約人違反本契約之其他約定，經催告改善或限期請求履行未果者。
							<br>
							</ul>
							本網路銀行約定事項之終止，以貴行辦妥註銷登錄始為生效，對於終止前發送訊息所需完成或履行之義務不生任何影響，惟網路銀行未屆轉帳日之各項預約轉帳自終止生效起即停止轉帳作業。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第二十六條、個人資料之利用</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人同意貴行及與貴行有業務往來之機構，於符合其營業登記項目或章程所定業務之需要，得蒐集、處理或國際傳輸及利用立約人之個人資料。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第二十七條、委外作業</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人同意貴行得於主管機關核定或核准得委外之作業事項範圍內，將涉及本約定書有關之資訊作業得委託適當之第三人處理。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第二十八條、異常提領規定</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							貴行因電腦故障或線路中斷，以致不能提供本網路銀行服務時，立約人仍能親赴貴行櫃台提領帳戶款項；但因貴行無法得知電腦故障或線路中斷前立約人已發送之付款指示，因此立約人同意貴行得視立約人往來狀況彈性酌予提領，如有溢領情事應即時返還。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第二十九條、契約修訂</span>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							本契約條款如有修改或增刪時，貴行以書面或雙方約定方式通知立約人後，立約人於七日內不為異議者，視同承認該修改或增刪約款。但下列事項如有變更，應於變更前六十日以書面或雙方約定方式通知立約人，並於該書面或雙方約定方式以顯著明確文字載明其變更事項、新舊約款內容，暨告知立約人得於變更事項生效前表示異議，及立約人未於該期間內異議者，視同承認該修改或增刪約款；並告知立約人如有異議，應於前項得異議時間內通知貴行終止契約：
								<ul style="list-style: decimal; list-style-position: inside;margin-left: 25px;margin-right: 10px;">

										一、第三人冒用或盜用使用者代號、密碼、憑證、私密金鑰，或其他任何未經合法授權之情形，貴行或立約人通知他方之方式。<br>

										二、其他經主管機關規定之事項。<br>

								</ul>
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第三十條、文書送達</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人同意以契約中載明之地址為相關文書之送達處所，倘立約人之地址變更，應即以書面或其他約定方式通知貴行，並同意改依變更後之地址為送達處所；如立約人未以書面或依約定方式通知變更地址時，貴行仍以契約中立約人載明之地址或最後通知貴行之地址為送達處所。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第三十一條、法令適用</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							本契約準據法，依中華民國法律。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第三十二條、法院管轄</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							因本契約而涉訟者，貴行及立約人同意以<font color=red><b>存款帳戶開戶行所在地之地方法院</b></font>為第一審管轄法院。但不得排除消費者保護法第四十七條或民事訴訟法第四百三十六條之九小額訴訟管轄法院之適用。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第三十三條、標題</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							本契約各條標題，僅為查閱方便而設，不影響契約有關條款之解釋、說明及瞭解。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第三十四條、申訴管道</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人如對本契約有爭議，申訴管道如下：<br>
							<div style="margin-left:25px;">
								1.免付費服務電話：0800-00-7171#5。<br>
								2.電子信箱(e-mail)：臺灣企銀網站 https://www.tbb.com.tw 客服信箱。
							</div>	
							</ul>				
						</ul>
					</ul>
					<!-- 壹、共通約定事項 END -->
				
				
					<!-- 貳、使用電子憑證約定事項 START -->
					<div class="ttb-message">
                           <p>貳、使用電子憑證約定事項</p>
                           <span></span>
                       </div>
                       <ul class="ttb-result-list" style="list-style: decimal;">
						<ul><span class="input-subtitle subtitle-color">第一條、名詞定義</span>
						   
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">

									一、用戶憑證機構：指提供憑證服務之憑證機構。<br>

									二、註冊中心：指驗證憑證申請人之身分或其他屬性，但不簽發憑證之單位或機構。<br>貴行為本約定書所稱，擔任用戶憑證機構之註冊中心。<br>

									三、憑證用戶：為憑證中識別之主體，持有與憑證中所載公開金鑰相對應之私密金鑰。憑證用戶，係憑證作業系統及註冊作業系統之使用者，即本約定書所稱之立約人。<br>

								       四、信賴憑證者：指信賴所收受之憑證用戶憑證及以憑證中所載公開金鑰加以驗證之數位簽章者，或信賴憑證用戶憑證主體之識別身分（或其他屬性）及憑證所載公開金鑰之對應關係者。<br>

									五、電子票據：指以電子方式製成之票據，包括電子支票、電子本票及電子匯票。<br>

									六、憑證實務作業基準：指由用戶憑證機構對外公告，用以陳述憑證機構據以簽發憑證及處理其他認證業務之準則。網址為：http://www.taica.com.tw/cps.asp。<br>

							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第二條、金融XML憑證</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							戶憑證機構提供立約人金融用戶憑證（以下簡稱金融XML憑證）服務，使立約人得以利用用戶憑證機構所簽發之金融XML憑證從事金融相關電子交易，立約人申請金融XML憑證之註冊及身分識別與鑑別作業由貴行建置之註冊認證服務相關系統（以下簡稱註冊中心）執行。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第三條、憑證之使用範圍</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							用戶憑證機構依據憑證實務作業基準所規範及簽發予立約人之憑證，立約人只可使用於網路銀行相關業務之應用（包含電子票據業務應用）及臺灣網路認證公司網站公告之應用，不可使用於電子簽章法與相關法律規範、主管機關、台灣票據交換所及銀行公會明訂禁止之應用或業務。
							<br>
							若憑證使用範圍及與憑證有關作業規定事項變更時，得逕於貴行網站公告。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第四條、註冊中心服務範圍</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							註冊中心負責傳遞立約人金融XML憑證申請、更新、暫時停用、解禁及廢止等服務。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第五條、憑證用戶註冊</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人應親赴貴行，提供身分證件，供貴行確認身分及驗證申請資格後，始完成憑證用戶註冊，並均應留存影本於貴行。
							<br>
							立約人完成憑證用戶註冊後，使用貴行交付之網路銀行密碼及憑證用戶身分識別（CN）代碼單供立約人憑以登入註冊中心申請簽發憑證。
							<br>
							立約人於尚未申請憑證前即遺失貴行發給憑以登錄註冊中心之交易密碼或該交易密碼連續輸入錯誤達五次時，應重新至貴行辦理密碼重置手續。
							<br>
							立約人載具密碼輸入錯誤超逾次數時，應重新至貴行辦理申請手續。
						</ul>
						<ul><span class="input-subtitle subtitle-color">第六條、憑證申請及簽發</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人登錄註冊中心，依憑證申請流程產生金鑰對及憑證申請檔，經註冊中心驗證無誤後，由註冊中心將立約人之身分識別及相關申請資料傳送至用戶憑證機構簽發憑證。用戶憑證機構有權決定是否簽發憑證，貴行無權干涉。
							<br>
							立約人於接受用戶憑證機構簽發之用戶憑證時，必須確認憑證資訊之內容為立約人註冊申請之資訊。如憑證註冊訊息有異動或私密金鑰有安全顧慮時，必須重新註冊、產生新金鑰對，並向註冊中心申請新憑證之簽發。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第七條、憑證之使用及保管</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人必須妥善保管及儲存與憑證相對應之私密金鑰及保護密碼，避免遺失、曝露、被篡改或為第三者任意使用或竊用。當有被冒用、暴露及遺失等不安全之顧慮時，或憑證內立約人相關之資訊有異動時，或不再使用該憑證時，必須即刻向註冊中心辦理憑證暫時停用或廢止。
							<br>
							立約人應正確使用用戶憑證機構核發之憑證，以電子簽章進行各項查詢、轉帳或其他金融服務。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第八條、憑證效期及憑證更新</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							用戶憑證機構簽發予立約人之憑證有效期限，依貴行與用戶憑證機構所議期限為準，至少為一年。
							<br>
							立約人憑證有效期限屆滿前一個月起至到期日止，應向註冊中心申請憑證更新。憑證若已過期則無法執行更新，必須重新依本約定事項第五條及第六條規定辦理。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第九條、憑證暫時停用（暫禁）</span>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
									一、立約人於憑證有效期間遇有下列情事之一，可向註冊中心申請憑證暫時停用：
									<ul style="list-style: decimal; list-style-position: inside;margin-left: 25px;margin-right: 10px;">
										<li>憑證之私密金鑰有可能遺失、洩露的不安全疑慮時。</li>
										<li>立約人欲暫時停止使用該憑證一段時間。</li>
									</ul>
									二、立約人於憑證有效期間遇有下列情事之一，註冊中心可主動辦理憑證暫時停用：
									<ul style="list-style: decimal; list-style-position: inside;margin-left: 25px;margin-right: 10px;">
										<li>立約人使用憑證而為有權第三者（例如：用戶憑證機構）宣告未履行應盡義務，或不當使用憑證而有可能違反政府法律、規章、憑證實務作業基準或業務使用規範之疑慮時。</li>
										<li>註冊中心發現立約人申請註冊時提供不實資料，或違反法令或依其他法令規定而不宜發給憑證時。</li>
										<li>立約人帳戶經貴行研判有疑似不當使用情事時。</li>
									</ul>
									憑證暫時停用之時效最長為用戶憑證機構簽發予立約人憑證之有效期限，如超逾憑證有效期限仍未執行憑證解禁時，則此張憑證即為廢止憑證。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第十條、憑證解除暫時停用（解禁）</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人憑證暫時停用後，未解除暫時停用（解禁）或廢止憑證或憑證效期結束，本憑證不得再申請簽發新憑證。
							<br>
							立約人完成憑證暫禁後，於憑證有效期間終止前，欲繼續使用該張憑證，必須親赴貴行申請解禁並完成登錄，該張憑證始為有效憑證。
							</ul>
						</ul>
						
						<ul><span class="input-subtitle subtitle-color">第十一條、憑證廢止</span>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
									一、立約人於憑證有效期間內，發生下述情況時，必須親赴貴行辦理憑證廢止：
									<ul style="list-style: decimal; list-style-position: inside;margin-left: 25px;margin-right: 10px;">
										<li>憑證內容之憑證用戶相關資訊有更動時。</li>
										<li>與憑證相關之私密金鑰有毀損、遺失、曝露、被篡改或為第三者竊用之疑慮時。</li>
										<li>憑證內容之立約人相關資訊，不符合憑證實務作業基準、銀行公會規定之相關憑證政策或業務使用規範時。</li>
									</ul>

									二、立約人於憑證有效期間內，發生下述情況時，用戶憑證機構或註冊中心得主動辦理憑證廢止：
									<ul style="list-style: decimal; list-style-position: inside;margin-left: 25px;margin-right: 10px;">
										<li>立約人申請註冊時提供不實資料，或違反法令或依其他法令規定而不宜發給憑證時。</li>
										<li>用戶憑證機構因憑證管理系統之不適用或憑證系統之整合需求。</li>
										<li>立約人使用憑證而為有權第三者宣告未履行應盡義務，或不當使用憑證而違反政府法律、規章、憑證實務作業基準或業務使用規範時。</li>
										<li>主管機關或法院，因業務之需求依正式合法作業程序申請。</li>
										<li>法院因訴訟與仲裁向註冊中心提出廢止立約人憑證之申請，經貴行核驗為合法之申請者；或其他第三者或主管機關，符合相關法令與規範之申請。</li>
									</ul>
									三、立約人於憑證廢止前所執行未屆轉帳日之各項預約轉帳仍屬有效。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第十二條、退費</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人依共通約定事項第十一條「一般網路銀行收費標準表」應支付憑證費用，除另有規定外，立約人不得要求貴行退還已自立約人帳戶扣取之任何使用憑證費用。
							<br>
							立約人產生本約定事項第十一條第二項各款情事，並由用戶憑證機構或註冊中心主動辦理該已簽發憑證之廢止時，各項憑證費用均不退還。
							</ul>
						</ul>
						
						<ul><span class="input-subtitle subtitle-color">第十三條、依據法令要求之資訊提供</span>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							除非符合下列之一之條件，否則立約人之註冊基本資料與身分認證相關資料絕不任意提供予權責管理單位，或其他任何人知悉使用：
								<ul style="list-style: decimal; list-style-position: inside;margin-left: 25px;margin-right: 10px;">
										一、政府法律、規章之規定並經由權責管理單位合法之授權。<br>
										二、法院處理因使用憑證產生的糾紛與仲裁而合法之申請需求。<br>
										三、具有合法司法管轄權的訴訟仲裁機構之正式申請。<br>
										四、立約人以電子簽章方式或親筆簽名之文件證明方式授權。<br>
								</ul>
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第十四條、代理</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人與註冊中心、或立約人與用戶憑證機構之權責關係均屬直接關係，無代理之關係存在。
							</ul>
						</ul>
						
						<ul><span class="input-subtitle subtitle-color">第十五條、賠償限額</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							貴行與用戶憑證機構訂定之「認證服務作業合約書」，如因可歸責於用戶憑證機構之事由致立約人受有損害者，如該損害得以補行程序方式加以填補者，以補行程序為之；如損害不得以補行程序方式加以填補者，其單一憑證累積賠償金額以新臺幣二十五萬元為上限。前項情形，立約人能證明用戶憑證機構有故意或重大過失者，不受最高賠償金額限制。第一項所稱損害，以該次交易所產生之積極損失（不包括所失利益）及其利息為限。
							<br>
							前述規定，亦適用於註冊中心。
							</ul>
						</ul>
						
						<ul><span class="input-subtitle subtitle-color">第十六條、賠償責任區分</span>
							<ul style="list-style: decimal; list-style-position: inside;">
									<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
									一、立約人向貴行申請註冊時，因故意、過失、或不正當意圖而提供不實資料，致造成用戶憑證機構、貴行或第三者遭受損害時，立約人自負一切損害賠償責任。<br>

									二、立約人必須妥善保管與憑證相對應之私密金鑰及保護密碼，不得洩漏或交付予他人使用，如因故意或過失，致造成用戶憑證機構、貴行或第三者遭受損害時，立約人自負一切損害賠償責任。<br>

									三、前項情形，立約人因而所致之損害，用戶憑證機構及貴行均無需負任何賠償責任。<br>

									四、立約人使用憑證或使用信賴憑證者憑證，有違反用戶憑證機構憑證政策與憑證實務作業基準或銀行公會相關憑證政策之規範，或憑證使用於非憑證實務作業基準規定之其他業務範圍時，立約人自負一切損害賠償責任。<br>

									五、因立約人或信賴憑證者之故意或過失，而非為用戶憑證機構或貴行之過失，所造成第三者財務、信譽及其他各方面之損失時，用戶憑證機構或貴行擁有賠償責任豁免權。<br>

									六、因立約人或信賴憑證者之故意或過失，而造成用戶憑證機構或貴行或其他第三者財務、信譽及其他各方面之損失時，立約人或信賴憑證者必須負損害賠償責任，用戶憑證機構或貴行可依照相關法律之規定向立約人或信賴憑證者請求賠償。<br>

									七、因其他與立約人、貴行、用戶憑證機構三方之任一方連線之電信事業所屬電信設備及線路設備故障、阻斷或其他不可歸責於該方之事由，以致發生錯誤、遲滯、中斷或不能傳遞而造成損害時，其所生之損害，該方不負任何責任。<br>
									</ul>
							</ul>
						</ul>
						
						<ul><span class="input-subtitle subtitle-color">第十七條、爭議之處理</span>
							<ul style="list-style: decimal; list-style-position: inside;">
									<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
									一、立約人與用戶憑證機構或註冊中心因使用憑證所引發之任何爭議，如可歸責於用戶憑證機構或註冊中心者，應由立約人分別與用戶憑證機構或註冊中心協議解決。<br>

									二、前項爭議責任歸屬不明時，應由用戶憑證機構及註冊中心共同與立約人協議解決。<br>

									三、於爭議協商、訴訟處理過程所發生之費用分擔，依據協商或相關之法律規範處理。<br>

									四、如為跨國或跨區域之爭議處理，無法以上述之處理方式解決時，依照相關之跨國或跨區域糾紛仲裁規範處理。<br>
									</ul>
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第十八條、其他</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人同意註冊中心可基於業務考量，逕行選擇提供憑證服務之用戶憑證機構，簽發立約人憑證及相關憑證服務，立約人對各憑證機構之權利義務，除有特別規定者外，均依據本使用電子憑證約定事項辦理。
							</ul>
						</ul>
                       </ul>
					<!-- 貳、使用電子憑證約定事項 END -->
				
				
					<!-- 參、使用動態密碼約定事項 START -->
					<div class="ttb-message">
                           <p>參、使用動態密碼約定事項</p>
                           <span></span>
                       </div>
					
						<ul><span class="input-subtitle subtitle-color">第一條、名詞定義</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							動態密碼（One Time Password，簡稱OTP）：指一次性動態密碼之交易安控機制，立約人每次交易須使用貴行提供之動態密碼卡(係用以產生動態密碼之設備)產生動態密碼，該組密碼內容每次均為不同，且各次密碼僅能使用一次。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第二條、動態密碼使用範圍</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							動態密碼可使用於特定金額範圍內之非約定轉入帳號交易，及主管機關與中華民國銀行商業同業公會聯合會訂定之金融機構辦理電子銀行業務安全控管作業基準明訂之應用或業務，使用範圍及有關作業規定事項變更時，得逕於貴行網站公告。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第三條、動態密碼(卡)之申請、使用與保管</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人申請使用動態密碼服務應親赴貴行，提供身分證件，供貴行確認身分及驗證申請資格。立約人應自行妥善保管貴行提供之動態密碼卡，並遵守貴行提供之動態密碼服務相關說明文件操作使用。動態密碼卡已達使用年限，立約人應至貴行重新辦理始得繼續使用本項服務。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第四條、動態密碼(卡)之異動作業</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">

									一、立約人之動態密碼資料及動態密碼卡若遭他人得知、洩漏、竊用或遺失等情形時，應立即向貴行辦理掛失、暫時停用等相關手續；惟在尚未依上開方式辦妥掛失、暫時停用手續業遭冒用所生之損害，立約人應自行負責。解除掛失及解除暫時停用應親赴貴行辦理，未辦妥前不得繼續使用本項服務。<br>

									二、倘因立約人之連續誤按動態密碼卡設備按鈕產生密碼達規定次數之情形時，應立即向貴行辦理同步作業相關手續，始得繼續使用本項服務。<br>

							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第五條、動態密碼服務之註銷作業</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人擬註銷動態密碼服務，應親赴貴行辦理註銷手續，並經貴行完成電腦登錄後始生效。如欲恢復使用時，應重新辦理申請手續。
							</ul>
						</ul>
					
					<!-- 參、使用動態密碼約定事項 END -->
				
				
					<!-- 肆、網路銀行一般業務約定事 START -->
					<div class="ttb-message">
                           <p>肆、網路銀行一般業務約定事</p>
                           <span></span>
                       </div>
					<ul><span class="input-subtitle subtitle-color">第一條、往來申請</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人可以存款帳號原留印鑑，向貴行申請使用貴行網路銀行服務系統（以下簡稱本系統）所提供之各項服務；惟貴行有權決定立約人於本系統之使用項目及使用權利。
							<br>
							立約人向貴行申請後，將自貴行取得網路銀行密碼單，供進入及使用本系統各項服務。
							</ul>
					</ul>
						<ul><span class="input-subtitle subtitle-color">第二條、密碼管理</span>
							<br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人首次登入網路銀行應進行簽入密碼及交易密碼之變更。因遺忘使用者名稱、簽入密碼、交易密碼或連續輸入錯誤達五次遭系統停止使用時，
							<font color=red><b>得以本行晶片金融卡解除鎖定，解鎖成功後，即可以原密碼登網路銀行或執行交易，倘再連續錯誤超過5次，</b></font>
							立約人應親赴貴行辦理密碼重置手續。
							<br>
							網路銀行簽入密碼變更、交易密碼變更、密碼重置或各項密碼輸入錯誤超過次數致遭系統鎖定，未屆轉帳日之各項預約轉帳仍屬有效，將於轉帳日依預約指示轉帳。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第三條、系統功能</span>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
						   	本系統提供電子轉帳及交易指示、帳務查詢及金融資訊等三類服務。貴行得視業務需要隨時修訂本系統功能。
						   	</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第四條、使用系統功能限制</span>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人進入本系統輸入身分識別代號、簽入密碼及交易密碼並經貴行檢核正確無誤後，可以使用本系統所提供之低風險性電子轉帳及交易指示類服務、帳務查詢類服務及金融資訊類服務等各項服務
							<font color=red><b>；倘立約人已申請貴行晶片金融卡非約定交易服務功能者，除可使用上述各項服務，並得執行費用代扣繳申請及取消、線上更改通訊地址/電話及簽入/交易密碼線上解鎖等功能，如該晶片金融卡已具備非約定轉帳者，得以該晶片金融卡之主帳號為轉出帳戶，執行新臺幣非約定轉帳交易。</b></font>
							<br>
							立約人進入本系統輸入身分識別代號及網路銀行簽入密碼並經貴行檢核正確無誤後，再憑有效的憑證作為身分之認證，可以使用本系統所提供之高風險性電子轉帳及交易指示類服務與具有帳務撥轉性質之業務申請類服務，或其他應使用憑證之服務。
							<br>
							貴行得視業務需要隨時修訂使用系統功能限制。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第五條、憑證效力</span>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							除遵守本網路銀行約定事項一、共通約定事項（二十三）規範之電子訊息之效力外，立約人使用憑證傳送之訊息（含付款指示），經貴行檢核正確後，即與立約人親赴貴行填具相關交易憑條或申請書、加蓋原留印鑑所為之交易或申請具同等效力。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第六條、各項系統功能作業規定</span>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人使用本系統進行各項交易後，所持存摺或存單資料與貴行記載數額不符時，以貴行帳載之數額為準。但經立約人核對貴行所提出之交易紀錄，其不符部分經貴行查證，確為貴行帳載數額有錯誤時，貴行應更正之。
							<br>
							立約人如擬取消預約轉帳，可逕以本系統執行取消預約轉帳，倘因各項密碼輸入錯誤超過次數致系統停用或憑證更新不成功、到期、廢止、暫時停用而無法以本系統執行取消預約轉帳交易時，立約人應親赴貴行櫃台辦理取消全部未屆轉帳日之預約轉帳。
							<br>
							立約人註銷約定轉出帳號，則該帳號下未屆轉帳日之預約轉帳，均停止轉帳作業；註銷約定轉入帳號，則該帳號下未屆轉帳日之預約轉入已約定帳號，停止轉帳作業。
							<br>
							立約人於貴行網站登錄電子郵箱位址後，可不定期收到貴行主動提供之帳戶訊息通知及重要公告通知等服務。
							<br>
							立約人使用本系統進行業務申請或掛失服務時，除應遵循網頁畫面補充說明之規定外，其所為之申請，無論是否涉及帳務撥轉，均與立約人親赴貴行營業櫃台填具相關業務書面申請書具有相同效力。
							<br>
							立約人使用本系統支票存款開戶及空白票據申請等服務，貴行保留是否接受之權利。
							<br>
							立約人使用本系統各項代扣繳申請服務，應自行輸入各委託機構印列於繳費單據上之資料，貴行僅將立約人所輸入之委繳資料傳送至各委託機構，並不負責核對所輸入之資料是否正確。貴行依各代扣繳申請項目之屬性，分別訂定相關約定條款張貼於網站，立約人一旦確定申請該代扣繳項目，即表示同意履行所申請項目約定條款之作業規範。
							<br>
							立約人使用貴行所提供非上述之系統功能時，同意遵守貴行作業規範。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第七條、支存入扣帳時間</span>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							辦理支票存款帳戶款項之撥轉，以貴行收到入扣帳指示並完成交易之時間為準，立約人應自行衡酌交易時間，避免因電腦或網路系統運作異常，影響入帳時間。如因轉入款項之遲延或撥轉之誤失而遭致退票時，由立約人自行負責，與貴行無涉。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第八條、交易金額</span>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人使用本系統各項轉帳、繳費及匯出匯款交易，其每一轉出帳戶之交易限額，均依主管機關及貴行業務規章辦理，貴行並得視業務需要隨時調整。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第九條、停止系統服務</span>
							有下列情形之一者，貴行得停止立約人使用本系統服務：
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">

									一、立約人有任何非正常使用或其他違反約定之情事者。<br>

									二、立約人輸入貴行所提供之使用者名稱或密碼連續錯誤達五次者（惟未屆轉帳日之各項預約轉帳仍屬有效）。<br>

									三、發生本網路銀行約定事項一、共通約定事項第二十四條、第二十五條契約終止情事者。<br>

							</ul>
						</ul>
					</ul>
					<!-- 肆、網路銀行一般業務約定事 END -->
				
				
					<!-- 伍、行動銀行服務 START -->
					<div class="ttb-message">
                           <p>伍、行動銀行服務</p>
                           <span></span>
                       </div>
					<ul class="ttb-result-list" style="list-style: decimal;">
						<ul>
							<span class="input-subtitle subtitle-color">第一條、申請及註銷</span>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							申請行動銀行服務前需先申請網路銀行服務。
							<br>
							申請一般網路銀行之立約人得臨櫃或線上申請啟用及停用行動銀行服務。申請人如停用行動銀行，得再臨櫃或線上申請重新啟用行動銀行服務。申請人如註銷網路銀行，行動銀行服務亦隨同註銷。
							<br>
							立約人申請行動銀行服務需自行準備行動裝置、下載貴行所提供之行動銀行軟體，並同意憑網路銀行之使用者名稱及密碼簽入行動銀行使用各項服務功能，但無法以相同之使用者名稱與密碼同時登入網路銀行與行動銀行。
							<br>
							使用者名稱及密碼錯誤次數與網路銀行合併計算。
							<br>
							使用行動銀行進行非約定轉帳、繳費等服務時，須使用動態密碼（OTP）。
							</ul>
						</ul>
						<ul>
							<span class="input-subtitle subtitle-color">第二條、服務內容及規範</span>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							客戶申請與變更網路銀行服務時，與貴行約定之各項服務內容及相關費用均適用行動銀行服務；交易限額合併計算行動銀行及網路銀行之交易金額。
							<br>
							貴行得視業務需要隨時修訂行動銀行服務功能，服務項目悉依 貴行網站公告為準。
							</ul>
						</ul>
					</ul>
					<!-- 伍、行動銀行服務 END -->
				
				
					<!-- 陸、網路銀行外匯業務約定事 START -->
					<div class="ttb-message">
                           <p>陸、網路銀行外匯業務約定事</p>
                           <span></span>
                       </div>
					<ul class="ttb-result-list" style="list-style: decimal;">
						<ul><span class="input-subtitle subtitle-color">第一條、交易限制</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人不得利用本系統辦理需檢附核准函或交易文件之外匯轉帳及匯款交易。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第二條、交易時間</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							即時轉帳及匯款交易為貴行總行營業日9:30-15:30。（遇交易之相關營業單位停止營業時，則不提供外匯交易服務）。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第三條、外匯存款業務</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							即時轉帳及匯款交易為貴行總行營業日9:30-15:30。（遇交易之相關營業單位停止營業時，則不提供外匯交易服務）。
								<ul style="list-style: decimal; list-style-position: inside;margin-left: 25px;margin-right: 10px;">

										一、立約人以存款轉帳方式辦理結購、結售時，轉出帳戶應為存戶本人之新臺幣活期性存款或外匯活期性存款帳戶。新臺幣轉外幣或外幣轉新臺幣限同一存戶辦理。<br>

										二、立約人轉帳之金額、匯率、幣別、預約交易均悉依貴行規定辦理。<br>

										三、同一存戶之外幣互轉，其轉出、轉入帳戶每日無最高轉帳金額限制；不同存戶之外幣互轉，其交易限額依貴行規定辦理，貴行可視情況隨時調整限額。<br>

								</ul>
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第四條、匯入匯款業務</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">

									一、立約人同意由網路上辦理匯入匯款解款交易時，應俟該筆匯入匯款屆生效日，且於營業日9:30~15:30內辦理，扣除相關費用後，始得依匯入匯款電文指示轉入立約人本人之帳戶。解款帳號若為外幣帳號，則以原幣解款，不得幣轉；解款帳號若非為外幣帳號則以台幣解款。<br>

									二、立約人同意匯入匯款解款申請及註銷將於申請日或註銷日之次營業日生效。<br>

									三、立約人於網路上收到匯入匯款通知後，若因故未於網路上解款而改向  貴行往來營業單位辦理臨櫃解款時，該臨櫃解款與網路解款具有同一效力，立約人並喪失該筆匯入匯款於網路上解款之權利。<br>

									四、匯入匯款通知不論係以網路解款或臨櫃解款，如  貴行未獲匯款行補償或有糾葛，立約人同意於接到  貴行通知後，立即退還全部或超收之款項。<br>
							</ul>
						</ul>
						
						<ul><span class="input-subtitle subtitle-color">第五條、匯出匯款業務</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">

									一、立約人辦理匯出匯款，應先約定轉出及匯入帳戶，其轉出帳戶限立約人在貴行開立之存款帳戶（定期性存款除外）。<br>

									二、本項業務之匯款手續費及郵電費等相關費用，悉依貴行相關規定收取，並授權貴行逕自指定轉出帳戶內代為扣繳。<br>

									三、立約人之匯出匯款指示經 貴行檢核無誤後，即由貴行依匯款指示逕自指定之轉出帳戶內代為扣繳。<br>

									四、授權貴行或貴行之通匯銀行，得以認為合適之任何方法匯出匯款，並得以任何國外通匯銀行為解款銀行或轉匯銀行。如因國外解款銀行或轉匯銀行所致誤失，不論該行係由立約人或　貴行所指定，貴行均不負任何責任。<br>

									五、貴行如應立約人之請求協助辦理追蹤、查詢，其所需之郵電費及國外銀行收取之費用，均由立約人負擔。<br>

									六、立約人同意轉出金額即為匯出金額，手續費及郵電費另行計算，手續費幣別未指定時，以轉出金額之幣別為手續費幣別，立約人絕無異議。<br>

									、立約人同意匯出匯款於國外銀行解款或轉匯行自匯款金額內扣取之費用依立約人於匯款交易所勾取手續費負擔方式辦理，立約人絕無異議。<br>

							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第六條、外匯匯率之折算</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">

									一、交易匯率：立約人同意於辦理網路匯款及新臺幣與外幣間之轉帳業務時，有關匯率之適用除另有議定外，依貴行營業時間中受理當時之貴行即時牌告匯率為折算基準；外幣間轉帳則依「轉換匯率」承做。<br>

									二、外匯預約交易匯率：以指定交易日貴行9:30之即時牌告匯率承做。<br>

									三、如遇外匯市場波動劇烈時，貴行得視實際情況需要，暫停受理網路外匯業務服務。<br>

							</ul>		
						</ul>
						<ul><span class="input-subtitle subtitle-color">第七條、外匯業務申報</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">

									一、立約人利用本系統辦理有關之匯款及轉帳交易，應遵照中央銀行公佈之「外匯收支或交易申報辦法」辦理。<br>

									二、立約人申辦本項服務時，須領有　貴行認可合乎規定及資格之相關證明文件。<br>

									三、立約人之外匯收支或交易未辦理新臺幣結匯者，以貴行掣發之其他交易憑證視同申報書。<br>

									四、立約人利用網際網路辦理結匯申報經查獲有申報不實情形者，貴行即終止立約人使用本項連線服務辦理外匯業務。<br>

									五、立約人辦理外匯業務時，應審慎據實填報匯款性質，如有未據實填報者，依據管理外匯條例第二十條第一項規定，將收新臺幣三萬元以上、六十萬元以下之罰鍰。<br>

									六、貴行有權逕依有關外匯法令之規定，據實將水單或交易憑證彙報，立約人應悉數承認，絕不異議。如獲悉立約人已超出其當年累積結匯金額或法令不得辦理時，貴行有權拒絕受理。<br>

							</ul>				
						</ul>
						<ul><span class="input-subtitle subtitle-color">第八條</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人與貴行議定匯率後，如未依約定完成交易或要求取消交易時，致貴行受有損失，貴行得向立約人收取損失金額，並授權貴行自立約人之約定轉出帳號逕行扣款。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第九條</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人當日臨櫃交易及網路交易之匯款金額，倘達大額結匯金額時，依「外匯收支或交易申報辦法」第5條規定，立約人應提供相關交易文件，並改以臨櫃交易同時辦理大額結匯申報，如有故意規避大額結匯申報之事實者，一經查獲，日後應至貴行櫃台辦理。
							</ul>
						</ul>
					</ul>
					<!-- 陸、網路銀行外匯業務約定事 END -->
				
				
					<!-- 柒、黃金存摺網路交易約定事 START -->
					<div class="ttb-message">
                           <p>柒、黃金存摺網路交易約定事</p>
                           <span></span>
                       </div>
					<ul class="ttb-result-list" style="list-style: decimal;">
						<ul><span class="input-subtitle subtitle-color">第一條、申請要件</span><br>
							立約人以貴行之網路銀行服務系統，線上申請黃金存摺帳戶，須具備下列要件：<br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">

									一、年滿20歲之本國人。<br>

									二、已申請使用本行網路銀行且已約定新臺幣活期性存款（不含支票存款）帳戶為轉出帳號者。<br>

									三、已臨櫃開立黃金存摺帳戶，但尚未申請黃金存摺網路交易。<br>

									四、已申請使用本行晶片金融卡或電子簽章(憑證載具)。<br>

							</ul>				
						</ul>
						<ul><span class="input-subtitle subtitle-color">第二條、服務時間</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人以貴行之網路銀行服務系統（以下簡稱本系統），辦理黃金存摺之買進、回售、預約買進、預約回售、定期定額申購、定期定額約定變更或查詢資料等服務時，應於貴行網站公告之服務時間內為之。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第三條、約定帳號（即網路銀行交易指定帳戶）</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							以本系統辦理黃金存摺之買進、回售時，立約人之約定轉出帳號及約定轉入帳號，即黃金存摺帳號與新臺幣存款帳號，限立約人於貴行開立之帳戶，立約人之扣款帳號如未預先約定或因結清銷戶或其他任何原因，致該扣款帳號不存在時，立約人應以臨櫃方式辦理買進或回售。<br>
							立約人若於完成黃金存摺之預約買進、預約回售後，辦理變更約定新臺幣存款帳號者，該筆預約交易於預約後的第一個營業日仍依原約定新臺幣存款帳號作為黃金買進扣款帳號、黃金回售入帳帳號。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第四條、不可抗力</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							如因不可抗力事由或其他原因，包括但不限於斷電、斷線、電信壅塞、網路傳輸干擾、貴行之電腦系統故障或第三人破壞等，致使立約人所為交易或其他指示遲延完成或無法按立約人指示完成、或致使貴行未能提供本系統服務者，立約人同意貴行不負任何賠償責任。<br>
							立約人預約交易後第一營業日若因天然災害或其他不可抗力之原因，致無黃金牌告價時，則該預約交易無效。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第五條、系統服務</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人同意貴行於貴行網站公告本系統新增或異動（含調整、變更或取消）之服務項目及其相關規定時，除貴行另有規定外，立約人無須另填申請書，即可使用本系統新增或異動後之服務項目；立約人一經進入本系統並使用該新增或異動後之服務項目時，即視為立約人同意依貴行網站所公告本系統新增或異動服務項目之相關規定辦理，且同意受該規定拘束。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第六條、投資風險</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							國際黃金價格有漲有跌，存戶投資黃金可能產生本金收益或損失，最大可能損失為買進金額之全部，請自行審慎判斷投資時機並承擔投資風險，辦理黃金存摺各項交易，如有涉及贈與、繼承及應繳稅捐等情事，悉由存戶或繼承人自行申報與負擔，黃金存摺不計算利息，亦非屬存款保險條例規定之標的，不受存款保險保障。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第七條</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人應一併遵守其與貴行訂定之黃金存摺開戶約定條款之約定及相關法令之規定。
							</ul>
						</ul>
						<ul><span class="input-subtitle subtitle-color">第八條</span><br>
							<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
							立約人於貴行網路銀行辦理新臺幣黃金存摺帳戶申請及交易，同意 貴行於網路銀行說明及揭露黃金存摺重要內容及風險。
							</ul>
						</ul>
					</ul>
					<!-- 柒、黃金存摺網路交易約定事 END -->
					
				</div>
			</div>
		</form>
	</table>
	<br/><br/><br/><br/>
</body>
</html>