<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
	<head>
		<%@ include file="../__import_head_tag.jsp"%>
		<%@ include file="../__import_js.jsp" %>
	</head>
	<script type="text/javascript">
		$(document).ready(function(){
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		
		function init(){
			$("a").click(function(){
				if(this.id == "logobtn"){
					window.close();
				}
			});
		}
	</script>
	<body>
		<!-- header -->
		<header>
			<%@ include file="../index/header_logout.jsp"%>
		</header>
		
		<!-- 左邊menu 及登入資訊 -->
		<div class="content row">
			<main class="col-12">
				<section id="main-content" class="container">
					<!-- 主頁內容  -->
					<h2>
						<c:choose>
							<c:when test="${pageContext.response.locale=='zh_CN'}">
								部分赎回及账面剩余金额之限制
							</c:when>
							<c:when test="${pageContext.response.locale=='en'}">
								Partial redemption and book balance amount limit
							</c:when>
							<c:otherwise>
								部分贖回及帳面剩餘金額之限制
							</c:otherwise>
						</c:choose>
					</h2>
					<form id="formId" method="post" action="">
						<div class="main-content-block row">
							<div class="col-12 tab-content">
								<div class="ttb-input-block">
									<c:choose>
										<c:when test="${pageContext.response.locale=='zh_CN'}">
											<div style="overflow: auto">
												<table class="question-table" style="width: 100%">
													<thead>
														<tr>
															<th colspan="3">投资目标</th>
															<th>最低部份赎回及账面剩余金额</th>
															<th>增加单位</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<th rowspan="19">单笔(不)定额</th>
															<th colspan="2">国内货币型基金</th>
															<td colspan="2">视个别基金规定</td>
														</tr>
														<tr>
															<th rowspan="5">国内基金</th>
															<th>台币信托</th>
															<td>新台币10,000元</td>
															<td>新台币1,000元</td>
														</tr>
														<tr>
															<th rowspan="4">外币信托</th>
															<td>南非币10,000元</td>
															<td>南非币1,000元</td>
														</tr>
														<tr>
															<td>美元1,000元</td>
															<td>美元100元</td>
														</tr>
														<tr>
															<td>人民币5,000元</td>
															<td>人民币1,000元</td>
														</tr>
														<tr>
															<td>澳币1,000元</td>
															<td>澳币100元</td>
														</tr>
														<tr>
															<th rowspan="13">国外基金</th>
															<th>台币信托</th>
															<td>新台币30,000元</td>
															<td>新台币10,000元</td>
														</tr>
														<tr>
															<th rowspan="12">外币信托</th>
															<td>美元1,000元</td>
															<td>美元100元</td>
														</tr>
														<tr>
															<td>欧元1,000元</td>
															<td>欧元100元</td>
														</tr>
														<tr>
															<td>英镑1,000元</td>
															<td>英镑100元</td>
														</tr>
														<tr>
															<td>澳币1,000元</td>
															<td>澳币100元</td>
														</tr>
														<tr>
															<td>瑞士法郎1,000元</td>
															<td>瑞士法郎100元</td>
														</tr>
														<tr>
															<td>加拿大币1,000元</td>
															<td>加拿大币100元</td>
														</tr>
														<tr>
															<td>瑞典币10,000元</td>
															<td>瑞典币1,000元</td>
														</tr>
														<tr>
															<td>日圆100,000元</td>
															<td>日圆10,000元</td>
														</tr>
														<tr>
															<td>新加坡币1,000元</td>
															<td>新加坡币100元</td>
														</tr>
														<tr>
															<td>纽币1,000元</td>
															<td>纽币100元</td>
														</tr>
														<tr>
															<td>港币10,000元</td>
															<td>港币1,000元</td>
														</tr>
														<tr>
															<td>南非币10,000元</td>
															<td>南非币1,000元</td>
														</tr>
													</tbody>
												</table>
											</div>
										</c:when>
										<c:when test="${pageContext.response.locale=='en'}">
											<div style="overflow: auto">
												<table class="question-table" style="width: 100%">
													<thead>
														<tr>
															<th colspan="3">Investment targets</th>
															<th>Minimum redemption and<br>book balance amount
															</th>
															<th>Increase unit</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<th rowspan="19">Single (Not) Quota</th>
															<th colspan="2">Domestic Currency Fund</th>
															<td class="white-spacing" colspan="2">
																Depending on individual fund regulations</td>
														</tr>
														<tr>
															<th rowspan="5">Domestic Fund</th>
															<th>Taiwan Dollar Trust</th>
															<td>NTD 10,000</td>
															<td>NTD 1,000</td>
														</tr>
														<tr>
															<th rowspan="4">Foreign Currency Trust</th>
															<td>ZAR 10,000</td>
															<td>ZAR 1,000</td>
														</tr>
														<tr>
															<td>USD 1,000</td>
															<td>USD 100</tt>
														</tr>
														<tr>
															<td>CNY 5,000</td>
															<td>CNY 1,000</td>
														</tr>
														<tr>
															<td>AUD 1,000</td>
															<td>AUD 100</td>
														</tr>
														<tr>
															<th rowspan="13">Foreign Funds</th>
															<th>Taiwan Dollar Trust</th>
															<td>NTD 30,000</td>
															<td>NTD 10,000</td>
														</tr>
														<tr>
															<th rowspan="12">Foreign Currency Trust</th>
															<td>USD 1,000</td>
															<td>USD 100</tt>
														</tr>
														<tr>
															<td>EUR 1,000</td>
															<td>EUR 100</td>
														</tr>
														<tr>
															<td>GBP 1,000</td>
															<td>GBP 100</td>
														</tr>
														<tr>
															<td>AUD 1,000</td>
															<td>AUD 100</td>
														</tr>
														<tr>
															<td>CHF 1,000</td>
															<td>CHF 100</td>
														</tr>
														<tr>
															<td>CAD 1,000</td>
															<td>CAD 100</td>
														</tr>
														<tr>
															<td>SEK 10,000</td>
															<td>SEK 1,000</td>
														</tr>
														<tr>
															<td>JPY 100,000</td>
															<td>JPY 10,000</td>
														</tr>
														<tr>
															<td>SGD 1,000</td>
															<td>SGD 100</td>
														</tr>
														<tr>
															<td>NZD 1,000</td>
															<td>NZD 100</td>
														</tr>
														<tr>
															<td>HKD 10,000</td>
															<td>HKD 1000</td>
														</tr>
														<tr>
															<td>ZAR 10,000</td>
															<td>ZAR 1,000</td>
														</tr>
													</tbody>
												</table>
											</div>
										</c:when>
										<c:otherwise>
											<div style="overflow: auto">
												<table class="question-table" style="width: 100%">
													<thead>
														<tr>
															<th colspan="3">投資標的</th>
															<th>最低部份贖回及帳面剩餘金額</th>
															<th>增加單位</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<th rowspan="19">單筆(不)定額</th>
															<th colspan="2">國內貨幣型基金</th>
															<td colspan="2">視個別基金規定</td>
														</tr>
														<tr>
															<th rowspan="5">國內基金</th>
															<th>臺幣信託</th>
															<td>新臺幣10,000元</td>
															<td>新臺幣1,000元</td>
														</tr>
														<tr>
															<th rowspan="4">外幣信託</th>
															<td>南非幣10,000元</td>
															<td>南非幣1,000元</td>
														</tr>
														<tr>
															<td>美元1,000元</td>
															<td>美元100元</td>
														</tr>
														<tr>
															<td>人民幣5,000元</td>
															<td>人民幣1,000元</td>
														</tr>
														<tr>
															<td>澳幣1,000元</td>
															<td>澳幣100元</td>
														</tr>
														<tr>
															<th rowspan="13">國外基金</th>
															<th>臺幣信託</th>
															<td>新臺幣30,000元</td>
															<td>新臺幣10,000元</td>
														</tr>
														<tr>
															<th rowspan="12">外幣信託</th>
															<td>美元1,000元</td>
															<td>美元100元</td>
														</tr>
														<tr>
															<td>歐元1,000元</td>
															<td>歐元100元</td>
														</tr>
														<tr>
															<td>英鎊1,000元</td>
															<td>英鎊100元</td>
														</tr>
														<tr>
															<td>澳幣1,000元</td>
															<td>澳幣100元</td>
														</tr>
														<tr>
															<td>瑞士法郎1,000元</td>
															<td>瑞士法郎100元</td>
														</tr>
														<tr>
															<td>加拿大幣1,000元</td>
															<td>加拿大幣100元</td>
														</tr>
														<tr>
															<td>瑞典幣10,000元</td>
															<td>瑞典幣1,000元</td>
														</tr>
														<tr>
															<td>日圓100,000元</td>
															<td>日圓10,000元</td>
														</tr>
														<tr>
															<td>新加坡幣1,000元</td>
															<td>新加坡幣100元</td>
														</tr>
														<tr>
															<td>紐幣1,000元</td>
															<td>紐幣100元</td>
														</tr>
														<tr>
															<td>港幣10,000元</td>
															<td>港幣1,000元</td>
														</tr>
														<tr>
															<td>南非幣10,000元</td>
															<td>南非幣1,000元</td>
														</tr>
													</tbody>
												</table>
											</div>
										</c:otherwise>
									</c:choose>
								</div>
								<input type="button" class="ttb-button btn-flat-orange" id="CLOSE" value="<spring:message code="LB.X0194" />" onclick="window.close();"/>
							</div>
						</div>
					</form>
				</section>
			</main>
		</div>
		<%@ include file="../index/footer.jsp"%>
	</body>
</html>