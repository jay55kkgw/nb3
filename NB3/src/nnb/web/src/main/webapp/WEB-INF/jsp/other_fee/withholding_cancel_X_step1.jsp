<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp"%>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<!-- 元件驗證身分JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 50);
		// 開始跑下拉選單並完成畫面
		setTimeout("init()", 400);
		// 初始化驗證碼
		setTimeout("initKapImg()", 200);
		// 生成驗證碼
		setTimeout("newKapImg()", 300);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
		// 將.table變更為footable
		//initFootable();
		// 初始化時隱藏span
		$("#hideblock").hide();
		setTimeout("initDataTable()",100);

	});
	
	function init(){
		
		$("#formId").validationEngine({
			binded : false,
			promptPosition : "inline",
		});
		
		$("#CMSUBMIT").click(function(e){
			//打開驗證欄位
			$("#hideblock").show();
			
			//驗證是否勾選
			var count = 0;
			$("input[name='ROWDATA']:radio:checked").map(function(){
				count++;
				return $(this).val();
			});
			if(count != 0){
// 		 		alert('lock');
		 		$("#ErrorMsg").val('lock');
			}
			
			e = e || window.event;
		
			if(!$('#formId').validationEngine('validate')){
	        	e.preventDefault();
 			}
			else{
 				$("#formId").validationEngine('detach');
 				initBlockUI();
 				$("#formId").attr("action","${__ctx}/OTHER/FEE/withholding_cancel_X_confirm");
 	  			$("#formId").submit(); 
 			}		
  		});
		
	}
	
</script>
</head>
<body>
	<!-- 	晶片金融卡 -->
	<%@ include file="../component/trading_component.jsp"%>

	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 自動扣繳服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2360" /></li>
    <!-- 自動扣繳查詢/取消     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0768" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code="LB.W0768" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="${__ctx}/OTHER/FEE/withholding_cancel_X_confirm">
			    <input type="hidden" name="ITMNUM" value="2">
			    <!-- 			晶片金融卡 -->
				<input type="hidden" id="jsondc" name="jsondc" value='${withholding_cancel_X_step1.data.jsondc}'>
				<input type="hidden" id="ISSUER" name="ISSUER" value="">
				<input type="hidden" id="ACNNO" name="ACNNO" value="">
				<input type="hidden" id="TRMID" name="TRMID" value="">
				<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
				<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
				<input type="hidden" id="TAC" name="TAC" value="">
				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
				<div class="main-content-block row">
					<div class="col-12">
						<!-- 其他費用代扣繳取消  表格 -->
						<ul class="ttb-result-list"></ul>
						<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
							<thead>
								<tr>
									<!-- 留白 -->
									<th><spring:message code="LB.D0223" /></th> 
									<!-- 扣帳帳號-->
									<th><spring:message code="LB.W0702" /></th>
									<!-- 查詢代繳類別-->
									<th><spring:message code="LB.W0771" /></th>
									<!-- 用戶編號-->
									<th><spring:message code="LB.W0691" /></th>
								</tr>
							</thead>
							<tbody>
							<c:forEach var="dataList" items="${withholding_cancel_X_step1.data.REC}" varStatus="data">
								<tr>									
									<td class="text-center">
										<label class="radio-block">&nbsp;
											<input type="radio" name = "ROWDATA" value="${dataList}">
											<span class="ttb-radio"></span>
										</label>
									</td>								
									<td class="text-center">${dataList.ACN}</td>
									<td class="text-center">
										<c:if test="${!dataList.MEMO_C.equals('')}">		
											<spring:message code="${dataList.MEMO_C}" />
										</c:if>
									</td>
									<td class="text-center">${dataList.UNTNUM}</td>
								</tr>
							</c:forEach>
							</tbody>
						</table>
						
						<!-- 不在畫面上顯示的span -->
						<span id="hideblock" >
						<!-- 驗證用的input -->
						<input id="ErrorMsg" name="ErrorMsg" type="text" class="text-input validate[groupRequired[ROWDATA]]" 
							style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" value="" />
						</span>
						<!-- 確定 -->
						<input type="button" name="CMSUBMIT" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm"/>"/>
					</div>
				</div>
			</form>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>