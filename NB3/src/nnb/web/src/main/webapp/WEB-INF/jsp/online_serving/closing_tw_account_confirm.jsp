<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:if test="${not empty sessionScope.closing_tw_account }">
	<c:set var="ACN"   value="${sessionScope.closing_tw_account.N366_REST_RS.ACN }"></c:set>
	<c:set var="INACN" value="${sessionScope.closing_tw_account.INACN }"></c:set>
</c:if>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>

	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/checkIdentityData.js"></script>
	
	<script type="text/javascript">
	var urihost = "${__ctx}";
		$(document).ready(function() {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 初始化驗證碼
			setTimeout("initKapImg()", 200);
			// 生成驗證碼
			setTimeout("newKapImg()", 300);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		
		// 畫面初始化
		function init() {
			// 表單驗證初始化
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
			// 確認鍵 click
			goOn();
			// 判斷顯不顯示驗證碼
			chaBlock();
			// 交易機制 click
			fgtxwayClick();
			//
			initNatural();
		}
		
		// 確認鍵 Click
		function goOn() {
			$("#CMSUBMIT").click( function(e) {
				// 送出進表單驗證前將span顯示
				$("#hideblock").show();
				
				console.log("submit~~");
	
				// 表單驗證
				if ( !$('#formId').validationEngine('validate') ) {
					e.preventDefault();
				} else {
					// 解除表單驗證
					$("#formId").validationEngine('detach');
					// 表單 submit 路徑
					var next = "${__ctx}" + "${next}";
		            $("#formId").attr("action", next);
		            
					// 通過表單驗證
					processQuery();
				}
			});
		}
		
		
		// 通過表單驗證準備送出
		function processQuery(){
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
			// 交易機制選項
			switch(fgtxway) {
				case '1':
					// IKEY
					useIKey();
					break;
					
				case '2':
					// 晶片金融卡
					var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
					useCardReader(capUri);
			    	break;
			    	
				case '4':
					// 自然人憑證
					useNatural();
					break;
					
				default:
					//alert("<spring:message code="LB.Alert001" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert001' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			}
		}
		
		
		// 使用者選擇晶片金融卡要顯示驗證碼區塊
	 	function fgtxwayClick() {
	 		$('input[name="FGTXWAY"]').change(function(event) {
	 			// 判斷交易機制決定顯不顯示驗證碼區塊
	 			chaBlock();
			});
		}
	 	// 判斷交易機制決定顯示或隱藏驗證碼區塊
		function chaBlock() {
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
 			console.log("fgtxway: " + fgtxway);
			// 交易機制選項
			switch(fgtxway) {
				case '2':
					// 晶片金融卡，顯示驗證碼欄位、隱藏自然人憑證PIN碼
					$("#chaBlock").show();
					$("#row1").hide();
			    	break;
				case '4':
					// 自然人憑證，顯示驗證碼欄位、自然人憑證PIN碼
					$("#chaBlock").show();
					$("#row1").show();
					break;
				default:
					// 若非晶片金融卡、自然憑證則隱藏
					$("#chaBlock").hide();
					$("#row1").hide();
			}
	 	}
	 	
		// 驗證碼刷新
		function changeCode() {
			$('input[name="capCode"]').val('');
			// 大小版驗證碼用同一個
			console.log("changeCode...");
			$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

			// 登入失敗解遮罩
			unBlockUI(initBlockId);
		}
		
		// 初始化驗證碼
		function initKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
		}
		
		// 生成驗證碼
		function newKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').click(function() {
				$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
			});
		}
		
		function yyyymmdd(date){
			var yyyy = date.getFullYear();
			var mm = date.getMonth() + 1;//getMonth() is zero-based
			var dd  = date.getDate();
			
			return String(10000 * yyyy + 100 * mm + dd);// Leading zeros for mm and dd
		}
	</script>
</head>
<body>
	<div id="obj"></div>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	
	
	<!-- 自然人憑證元件載點 -->
	<a href="${__ctx}/component/windows/natural/TBBankPkcs11ATL.exe" id="naturalComponent" target="_blank" style="display:none"></a>
	
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 臺幣存款帳戶結清銷戶申請    -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0422" /></li>
		</ol>
	</nav>

	
	<!-- 左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.D0422" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                
                <form id="formId" method="post" action="">
                	<!-- 交易機制所需欄位 -->
					<input type="hidden" id="jsondc" name="jsondc" value='${transfer_data}'>
					<input type="hidden" id="ISSUER" name="ISSUER" value="">
					<input type="hidden" id="ACNNO" name="ACNNO" value="">
					<input type="hidden" id="TRMID" name="TRMID" value="">
					<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
					<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
					<input type="hidden" id="TAC" name="TAC" value="">
					<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
                	<!-- 自然人憑證所需欄位 -->
					<input type="hidden" id="AUTHCODE" />
                	<input type="hidden" id="AUTHCODE1" />
                	<input type="hidden" id="CertFinger" />
					<input type="hidden" id="CertB64" />
					<input type="hidden" id="CertSerial" />
					<input type="hidden" id="CertSubject" />
					<input type="hidden" id="CertIssuer" />
					<input type="hidden" id="CertNotBefore" />
					<input type="hidden" id="CertNotAfter" />
					<input type="hidden" id="HiCertType" />
					<input type="hidden" id="CUSIDN4" />
					<input type="hidden" id="CUSIDN" value="${sessionScope.closing_tw_account.CUSIDN }"/>
					<input type="hidden" name="UID" id="UID" value="${sessionScope.cusidn }">
					
					
	                <div class="main-content-block row">
	                    <div class="col-12 tab-content">
	                        <div class="ttb-input-block">
	                            <div class="ttb-message">
	                                <span><spring:message code="LB.D0104" /></span>
	                            </div>
	
	                        	<!-- 欲結清帳號 -->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                    <label>
	                                    	<h4><spring:message code="LB.D0424" /></h4>
	                                    </label>
	                                </span>
	                                <span class="input-block" >
	                                    <div class="ttb-input">
				                        	<span>${ACN }</span>
	                                    </div>
	                                </span>
	                            </div>  
	                            
								<!-- 轉入帳號 -->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                    <label>
	                                        <h4><spring:message code="LB.Payees_account_no" /></h4>
	                                    </label>
	                                </span>
	                                <span class="input-block" >
	                                    <div class="ttb-input">
				                        	<span>
				                        		<c:if test="${INACN =='#'}">
				                        			<spring:message code="LB.D1070_2" />
				                        		</c:if>
				                        		<c:if test="${INACN !='#'}">
				                        			${INACN }
				                        		</c:if>
				                        	</span>
	                                    </div>
	                                </span>
	                            </div>  
								
	                            <!--交易機制-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                	<label>
	                                        <h4><spring:message code="LB.Transaction_security_mechanism" /></h4>
	                                    </label>
									</span>
									
									<!-- 交易機制選項 -->
	                                <span class="input-block">
	                                	<!-- 使用者是否可以使用IKEY -->
										<c:if test = "${sessionScope.isikeyuser}">
									
											<!-- IKEY -->
											<div class="ttb-input">
												<label class="radio-block">
													<spring:message code="LB.Electronic_signature" />
													<input type="radio" name="FGTXWAY" id="CMIKEY" value="1">
													<span class="ttb-radio"></span>
												</label>
											</div>
												
										</c:if>
										
	                                	<!-- 晶片金融卡 -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Financial_debit_card" />
												<input type="radio" name="FGTXWAY" id="CMCARD" value="2">
												<span class="ttb-radio"></span>
											</label>
										</div>
	                                	
										<div class="ttb-input">
	                                       	<label class="radio-block"><spring:message code="LB.D0437" />
	                                            <input type="radio" name="FGTXWAY" id="CMNPC" value="4">
	                                            <span class="ttb-radio"></span>
	                                        </label>
	                                   	</div>
									</span>
								</div>
								
								<!-- 如果是非約定則不能使用IKEY，只能選晶片金融卡，使用者選擇晶片金融卡後才顯示驗證碼-->
								<div class="ttb-input-item row" id="chaBlock" style="display:none">
									<!-- 驗證碼 -->
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Captcha" /></h4>
										</label>
									</span>
									<span class="input-block">
										<spring:message code="LB.Captcha" var="labelCapCode" />
										<input id="capCode" name="capCode" type="text"
											class="text-input" maxlength="6" autocomplete="off">
										<img name="kaptchaImage" class="verification-img" src="" />
										<input type="button" name="reshow" class="ttb-sm-btn btn-flat-orange"
											onclick="changeCode()" value="<spring:message code="LB.Regeneration_1" />" />
									</span>
								</div>
								
								<!-- 自然人憑證PIN碼 -->
	                            <div class="ttb-input-item row" id="row1">
	                                <span class="input-title">
	                                    <label>
	                                        <h4><spring:message code="LB.D1540" /></h4>
	                                    </label>
	                                </span>
	                                <span class="input-block" >
	                                    <div class="ttb-input">
				                        	<input type="password" id="NPCPIN" name="NPCPIN" class="text-input">
	                                    </div>
	                                </span>
	                            </div>  
								
	                        </div>
	                        <!-- 確定  -->
							<spring:message code="LB.Confirm" var="Confirm"></spring:message>
							<input type="button" id="CMSUBMIT" value="${Confirm}" class="ttb-button btn-flat-orange" />
	                    </div>
	                </div>
                </form>
			</section>
		</main>
	</div><!-- content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>