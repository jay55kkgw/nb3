<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

 <%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
   <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">    
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">

<script type="text/javascript">
	$(document).ready(function() {
		// 將.table變更為footable
		initFootable();
		
		//表單驗證
		$("#formId").validationEngine({
			binded : false,
			promptPosition : "inline"
		});
		
		$("#CMSUBMIT").click(function(e){
			console.log("submit~~");
			if($('input:radio[name=agreeCheck]:checked').val()=="Y"){
				$("#validate").prop("checked",true)
			}
			if (!$('#formId').validationEngine('validate')) {
				e.preventDefault();
			} else {
				initBlockUI();
				$("#formId").validationEngine('detach');
				$("form").submit();
			}
		})
		$("#PREVIOUS").click(function(e) {
			$("#formId").validationEngine('detach');
			action = '${__ctx}/APPLY/CREDIT/apply_creditloan_p3';
			$("form").attr("action", action);
			$("form").submit();
		});
	});
	
	
</script>
</head>
	<body>
		<!-- header     -->
		<header>
			<%@ include file="../index/header.jsp"%>
		</header>
	
		<!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<!--申請小額信貸 -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0543" /></a></li>
		</ol>
	</nav>
		<!-- menu、登出窗格 -->
		<div class="content row">
			<%@ include file="../index/menu.jsp"%>
			<!-- 	快速選單內容 -->
			<!-- content row END -->
			<main class="col-12">
				<section id="main-content" class="container">
					<!-- 申請人聲明事項 -->
					<h2><spring:message code= "LB.D0551" /></h2>
					<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
					<form id="formId" method="post" action="${__ctx}/APPLY/CREDIT/apply_creditloan_p5">
					<input type="hidden" id="LOANTYPE" name ="LOANTYPE" value="${sessionScope.loantype}">
						<div class="main-content-block row">
							<div class="col-12">
							 <div class="ttb-message">
			             <!-- 申請人聲明事項 -->
                            <p><spring:message code= "LB.D0551" /></p>
                              </div>
						 		<div class="NA01_3">
									<ol>
									<!-- 本人(即申請人)茲聲明本次信用貸款申請過程，本人已充分瞭解並同意包括但不限於下述事項： -->
										<li>
											<div><spring:message code= "LB.X1177" /></div>
										<li>
										<li>
										<!-- ㄧ、 依主管機關規定，縱經　貴行核貸，　貴行於撥款時將再次查詢聯徵 中心資料，倘發現申請人有其他新增應計入DBR22倍規範之授信額度者， 貴行仍保留撥款與否之權利。-->
											<div><spring:message code= "LB.X0237" /></div>
											<div><spring:message code= "LB.X1178" /></div>
										</li>
										<li>
										<!-- 二 、本次信用貸款申請，  貴行於核准對保後將立即報送財團法人金融聯合 徵信中心，供各金融機構依規定查詢。-->
											<div><spring:message code= "LB.X0240" /></div>
											<div><spring:message code= "LB.X1179" /></div>
										</li>
										<li>
										<!-- 三 、本次信用貸款之申請作業並非透過代辦業者轉介辦理。-->
											<div><spring:message code= "LB.X0243" /></div>
											<div><spring:message code= "LB.X1180" /></div>
										</li>
										<li>
										<!-- 四、本次提供  貴行信用貸款申請之相關資料、證明文件等，均為未經變造或偽造之真實文件，本人若有聲明不實之情事，將對本次申貸造成負面影響，本人亦須負擔聲明不實之相關法律責任。 -->
											<div><spring:message code= "LB.X0245" /></div>
											<div><spring:message code= "LB.X1181" /></div>
										</li>
										<li>
										<!-- 五 、貴行保有核准貸款與否及貸款條件之權利。-->
											<div><spring:message code= "LB.X0247" /></div>
											<div><spring:message code= "LB.X1182" /></div>
										</li>
									</ol>
								</div>
								<br>
								<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block">
										<!-- 本人已詳細閱讀「申請人聲明事項 -->
											<spring:message code= "LB.D0550" />
											<input type="radio" name="agreeCheck" id="" value="Y"/>
											<span class="ttb-radio"></span>
										</label>
									</div>
								</span>
								<div class="ttb-input">
									<div style="text-align: center">
										<div class="radio-block">
											<input type="radio" id="validate" class="validate[required]"/>
										</div>
									</div>
								</div>
								<br>
								<!-- 上一步-->
								<input class="ttb-button btn-flat-gray" type="button" value="<spring:message code= "LB.X0318" />" id="PREVIOUS">
								<!-- 下一步-->
			                   	<input class="ttb-button btn-flat-orange" type="button" value="<spring:message code= "LB.X0080" />" id="CMSUBMIT">
							</div>
						</div>
					</form>
				</section>
			</main>
			<!-- 		main-content END -->
			<!-- 	content row END -->
		</div>
		<%@ include file="../index/footer.jsp"%>
	</body>
</html>