<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
	<script type="text/javascript">
		$(document).ready(function () {
			//列印
			$("#printbtn").click(function () {

				var params = {
					"jspTemplateName": "renewal_apply_result_print",
					"jspTitle": '<spring:message code="LB.NTD_Time_Deposit_Automatic_Rollover_Application_Change"/>',
					"MsgName": '${renewal_apply_result.data.MsgName}',
					"CMQTIME": '${renewal_apply_result.data.CMQTIME}',
					"FDPACN": '${renewal_apply_result.data.FDPACN}',
					"FDPNUM": '${renewal_apply_result.data.FDPNUM}',
					"FDPACC": '${renewal_apply_result.data.FDPACC}',
					"AMOUNT": '${renewal_apply_result.data.AMOUNT}',
					"TYPE": '${renewal_apply_result.data.TYPE}',
					"TERM": '${renewal_apply_result.data.TERM}',
					"INTMTH": '${renewal_apply_result.data.INTMTH}',
					"FGRENNAME": '${renewal_apply_result.data.FGRENNAME}',
					"FGSVNAME": '${renewal_apply_result.data.FGSVNAME}',
					"DPSVACNO": '${renewal_apply_result.data.DPSVACNO}',
					"FGRENCNT": '${renewal_apply_result.data.FGRENCNT}',
				};

				openWindowWithPost("${__ctx}/print",
					"height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",
					params);

			});
		});
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 	<!--   IDGATE -->
	<%@ include file="../idgate_tran/idgateAlert.jsp" %>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 定存服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Current_Deposit_To_Time_Deposit" /></li>
    <!-- 定存自動轉期申請/變更     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.NTD_Time_Deposit_Automatic_Rollover_Application_Change" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!--  臺幣定存自動轉期申請/變更 -->
					<spring:message code="LB.NTD_Time_Deposit_Automatic_Rollover_Application_Change" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="" onSubmit="return processQuery()">
					<c:set var="BaseResultData" value="${renewal_apply_result.data}"></c:set>
					<!-- 交易步驟 -->
					<div id="step-bar">
						<ul>
							<!-- 輸入資料 -->
							<li class="finished">
								<spring:message code="LB.Enter_data" />
							</li>
							<!-- 確認資料 -->
							<li class="finished">
								<spring:message code="LB.Confirm_data" />
							</li>
							<!-- 交易完成 -->
							<li class="active">
								<spring:message code="LB.Transaction_complete" />
							</li>
						</ul>
					</div>
					<!-- 表單顯示區 -->
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<div class="ttb-message">
									<span>
										<c:choose>
											<c:when test="${BaseResultData.FGRENCNT=='3'}">
												<p><spring:message code= "LB.Cancel" /><spring:message code= "LB.Automatic_rollover_successful" /></p>
											</c:when>
											<c:otherwise>
											<!-- 自動轉期成功 -->
												<p><spring:message code= "LB.Automatic_rollover_successful" /></p>
											</c:otherwise>
										</c:choose>
									</span>
								</div>
								<!-- 交易時間 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.Trading_time" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${BaseResultData.CMQTIME}</span>
										</div>
									</span>
								</div>
								<!-- 存單帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.Account_no" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${BaseResultData.FDPACN}</span>
										</div>
									</span>
								</div>
								<!-- 								存單號碼 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.Certificate_no" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${BaseResultData.FDPNUM}</span>
										</div>
									</span>
								</div>
								<!-- 								存單金額 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.Certificate_amount" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												<!-- 新臺幣 -->
												<spring:message code="LB.NTD" />
												<fmt:formatNumber type="number" minFractionDigits="2"
													value="${BaseResultData.AMOUNT }" />
												<!--元 -->
												<spring:message code="LB.Dollar" />
											</span>
										</div>
									</span>
								</div>
								<!-- 								存單種類 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.Deposit_certificate_type" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.FDPACC}
											</span>
										</div>
									</span>
								</div>
								<!-- 存單期別 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.Fixed_duration" />

											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.TERM}
												<!--月 -->
												<spring:message code="LB.Month" />
											</span>
										</div>
									</span>
								</div>
								<!-- 計息方式 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.Interest_calculation" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.INTMTH}
												<!--月 -->
												<spring:message code="LB.Month" />
											</span>
										</div>
									</span>
								</div>
								<!-- 轉期次數 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.Number_of_rotations" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.FGRENNAME}
											</span>
										</div>
									</span>
								</div>
								<!-- 								轉存方式 -->
								<c:if test="${BaseResultData.FGRENCNT != '3'}">
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Rollover_method" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.FGSVNAME} ${BaseResultData.DPSVACNO}
												</span>
											</div>
										</span>
									</div>
								</c:if>
							</div>
							<!-- button -->
								<!-- 列印  -->
								<spring:message code="LB.Print" var="printbtn"></spring:message>
								<input class="ttb-button btn-flat-orange" type="button" id="printbtn" value="${printbtn}"  />
							<!--button 區域 -->
						</div>
					</div>
					<div class="text-left">
						<!-- 					說明： -->
						<!-- 					<ol class="list-decimal text-left"> -->
						<!-- 						<li>晶片金融卡:為保護您的交易安全，結束交易或離開電腦時，請務必將晶片金融卡抽離讀卡機並登出系統。</li> -->
						<!-- 						<li>電子簽章:為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章(載具i-key)拔除並登出系統。</li> -->
						<!-- 					</ol> -->
					</div>
				</form>
			</section>
			<!-- 		main-content END -->
		</main>
		<!-- 	content row END -->
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>