<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="page4" class="card-detail-block">
	<div class="card-center-block">
		<!-- Q1 -->
		<div class="ttb-pup-block" role="tab">
			<a id="popup1-Q1" role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-41" aria-expanded="true"
				aria-controls="popup1-41">
				<div class="row">
					<span class="col-1">Q1</span>
					<div class="col-11">
						<span>What kind of computer hardware and software devices do you need to use on your bank's online banking?</span>
					</div>
				</div>
			</a>
			<div id="popup1-41" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>It is recommended that you use the following specifications, the higher the computer hardware level, the faster the processing speed:</p>
							<p>Hardware: Computer CPU 1GHz, 2GB or more memory.</p>
							<p>Software: </p>
							<ol>
								<li>First, the Mac system can use the browser: Safari, Chrome, FireFox</li>
								<li>Second, the Window operating system (Window7 (inclusive) or above) can use the browser: IE 11 (inclusive) or higher, Chrome, FireFox, Edge</li>
							</ol>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>