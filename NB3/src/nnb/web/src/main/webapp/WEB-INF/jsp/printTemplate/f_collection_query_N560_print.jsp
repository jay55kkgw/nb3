<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
	<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br/><br/>
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
	<br/><br/><br/>
	<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
	<br/><br/><br/>
	<label><spring:message code="LB.Inquiry_time" />：</label><label>${CMQTIME}</label>
	<br/><br/>
	<label><spring:message code="LB.Inquiry_period_1" />：</label><label>${CMPERIOD}</label>
	<br/><br/>
	<label><spring:message code= "LB.L/C_no" />：</label>
	<label>
	<c:if test="${LCNO =='' }">
	<spring:message code="${LCNO_SHOW}" />
	</c:if>
	<c:if test="${LCNO !='' }">
		${LCNO_SHOW}
	</c:if>
	</label>
	<br/><br/>
	<label><spring:message code= "LB.W0183" />：</label>
	<label>
	<c:if test="${REFNO =='' }">
	<spring:message code="${REFNO_SHOW}" />
	</c:if>
	<c:if test="${REFNO !='' }">
		${REFNO_SHOW}
	</c:if>
	</label>
	<br/><br/>
	<label><spring:message code="LB.Total_records" />：</label><label>${CMRECNUM} <spring:message code="LB.Rows" /></label>
	<br/><br/>
	<table class="print">
		<tr>
			<td style="text-align:center"><spring:message code= "LB.W0186" /></td>
			<td style="text-align:center"><spring:message code= "LB.W0183" /></td>
			<td style="text-align:center"><spring:message code= "LB.L/C_no" /></td>
			<td style="text-align:center"><spring:message code= "LB.Currency" /></td>
			<td style="text-align:center"><spring:message code= "LB.W0189" /></td>
			<td style="text-align:center"><spring:message code= "LB.W0195" /></td>
			<td style="text-align:center"><spring:message code= "LB.W0196" /></td>
			<td style="text-align:center"><spring:message code= "LB.W0198" /></td>
			<td style="text-align:center"><spring:message code= "LB.W0203" /></td>
			<td style="text-align:center"><spring:message code= "LB.W0199" /></td>
		</tr>
		<c:forEach items="${dataListMap[0]}" var="map">
		<tr>
			<td style="text-align:center">${map.RNREMDAT}</td>		
			<td style="text-align:center">${map.RREFNO}</td>
			<td style="text-align:center">${map.RLCNO}</td> 
			<td style="text-align:center">${map.RBILLCCY}</td>
			<td style="text-align: right">${map.RBILLAMT }</td>
			<td style="text-align:center">${map.RVALDATE}</td>
			<td style="text-align:center"><spring:message code="${map.RUPSTAT}"/></td>
			<td style="text-align:center">${map.RCOUNTRY}</td>
			<td style="text-align:center">${map.RBILLTYP}</td>
			<td style="text-align:center">${map.RCMDATE1}</td>
		</tr>
		</c:forEach>
	</table>
	<br/>
	<div class="text-left">
		<p><spring:message code= "LB.W0209" />：</p>
		<c:forEach var="map2" items="${dataListMap[1]}">
			<p>&nbsp;${map2.AMTRBILLCCY}&nbsp;${map2.FXTOTAMT}&nbsp;<spring:message code= "LB.W0158" />&nbsp;${map2.FXTOTAMTRECNUM}&nbsp;<spring:message code= "LB.Rows" /></p>
		</c:forEach>
	</div>
	</body>
</html>