<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
</head>
<body>
        <!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
	<!-- 帳戶查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Account_Inquiry" /></li>
    <!-- 活存明細查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.FX_Demand_Deposit_Detail" /></li>
		</ol>
	</nav>



		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 --> 
 		<div class="content row">
 		   <%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
 		
 		<main class="col-12">	

		<!-- 		主頁內容  -->

			<section id="main-content" class="container">
		
				<h2><spring:message code="LB.FX_Demand_Deposit_Detail" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 						下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
<!-- 						下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
<!-- 						下載為舊txt檔 -->
						<option value="oldtxt"><spring:message code="LB.Download_oldversion_txt_file" /></option>
					</select>
				</div>
				<div class="main-content-block row">
					<div class="col-12 printClass">
						<ul class="ttb-result-list">
							<!-- 查詢時間 -->
                            <li>
                                <h3><spring:message code="LB.Inquiry_time" />：</h3>
                                <p>${demand_deposit_result.data.CMQTIME }</p>
                            </li>
                            <!-- 查詢期間 -->
                            <li>
                                <h3><spring:message code="LB.Inquiry_period_1" />：</h3>
                                <p>	${demand_deposit_result.data.cmqdate}</p>
                            </li>
                            <!-- 資料總數 -->
                            <li>
                                <h3><spring:message code="LB.Total_records" />：</h3>
                                <p> 
                                	${demand_deposit_result.data.COUNT}
									<spring:message code="LB.Rows" />
								</p>
                            </li>
                        </ul>
					<!-- 全部 -->
					<c:forEach var="labelList" items="${ demand_deposit_result.data.labelList }">	
						<ul class="ttb-result-list">
                            <li>
                                <h3><spring:message code="LB.Account" /></h3>
                                <p>${labelList.ACN }</p>
                            </li>
                        </ul>
						<table class="stripe table-striped ttb-table dtable" data-show-toggle="first">
						<thead>
<%-- 							<tr> --%>
<%-- 								<th><spring:message code="LB.Account" /></th> --%>
<%-- 								<th>${labelList.ACN }</th> --%>
<%-- 							</tr> --%>
							<tr>
								<th><spring:message code="LB.Currency" /></th>
								<th data-breakpoints="xs sm"><spring:message code="LB.Change_date" /></th>
								<th data-breakpoints="xs sm"><spring:message code="LB.Summary" /></th>
								<th data-breakpoints="xs sm"><spring:message code="LB.Withdrawal_amount" /></th>
								<th data-breakpoints="xs sm"><spring:message code="LB.Deposit_amount" /></th>
								<th><spring:message code="LB.Account_balance" /></th>
								<th data-breakpoints="xs sm"><spring:message code="LB.Data_content" /></th>
								<th data-breakpoints="xs sm"><spring:message code="LB.Note" /></th>
								<th data-breakpoints="xs sm"><spring:message code="LB.Trading_time" /></th>								
							</tr>
							</thead>
							<tbody>
							<c:forEach var="dataList" items="${ labelList.rowListMap }">
								<tr>
					                <td class="text-center">${dataList.CUID }</td>
					                <td class="text-center">${dataList.display_TXDATE }</td>
					                <td class="text-center">${dataList.MEMO }</td>
					                <td class="text-right">${dataList.display_FXPAYAMT }</td>
					                <td class="text-right">${dataList.display_FXRECAMT }</td>
					                <td class="text-right">${dataList.display_BALANCE }</td>
					                <td class="text-center">${dataList.DATA12 }</td>
					                <td class="text-center">${dataList.TEXT }</td>
					                <td class="text-center">${dataList.display_TRNTIME }</td>					               
								</tr>
							</c:forEach>
							</tbody>
						</table>
					</c:forEach>
						<input type="button" id="previous" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />"/>
						<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
					</div>
				</div>
					<div class="text-left">
					 <ol class="description-list list-decimal">
					 	<p>
							<spring:message code="LB.Description_of_page" /> 
						</p>
							<li><spring:message code="LB.F_demand_deposit_P2_D1" /></li>
						</ol>
					</div>
				<form id="formId" action="${__ctx}/download" method="post">
					<!-- 下載用 -->
					<input type="hidden" name="downloadFileName" value="LB.FX_Demand_Deposit_Detail"/>
					<input type="hidden" name="CMQTIME" value="${demand_deposit_result.data.CMQTIME}"/>
					<input type="hidden" name="cmqdate" value="${demand_deposit_result.data.cmqdate}"/>
					<input type="hidden" name="COUNT" value="${demand_deposit_result.data.COUNT}"/>
					<input type="hidden" name="downloadType" id="downloadType"/> 					
					<input type="hidden" name="templatePath" id="templatePath"/>
					<input type="hidden" name="hasMultiRowData" value="true"/> 	
					<!-- EXCEL下載用 -->
<!-- 					標題到右邊最大長度 -->
					<input type="hidden" name="headerRightEnd" value="1"/>
<!-- 					標題到rowdata的長度 -->
					<input type="hidden" name="headerBottomEnd" value="4"/>
<!-- 					rowdata起始 -->
					<input type="hidden" name="multiRowStartIndex" value="8" />
<!-- 					rowdata到右邊最大長度 -->
					<input type="hidden" name="multiRowEndIndex" value="8" />
<!-- 					rowdata結束(與上面可能一樣) -->
                    <input type="hidden" name="multiRowCopyStartIndex" value="5" />
                    <input type="hidden" name="multiRowCopyEndIndex" value="8" />
                    <input type="hidden" name="multiRowDataListMapKey" value="rowListMap" />
                    <input type="hidden" name="rowRightEnd" value="8" />
					<!-- TXT下載用 -->
					<input type="hidden" name="txtHeaderBottomEnd" value="7"/>
					<input type="hidden" name="txtMultiRowStartIndex" value="11"/>
					<input type="hidden" name="txtMultiRowEndIndex" value="11"/>
					<input type="hidden" name="txtMultiRowCopyStartIndex" value="8"/>
					<input type="hidden" name="txtMultiRowCopyEndIndex" value="11"/>
					<input type="hidden" name="txtMultiRowDataListMapKey" value="rowListMap"/>				
				</form>
<!-- 				</form> -->
			</section>
			<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->
	

	<%@ include file="../index/footer.jsp"%>
      
    	<script type="text/javascript">
			$(document).ready(function(){
				// HTML載入完成後開始遮罩
				setTimeout("initBlockUI()",10);
				// 開始查詢資料並完成畫面
				setTimeout("init()",20);
				// 解遮罩
				setTimeout("unBlockUI(initBlockId)",500);
				setTimeout("initDataTable()",100);
			});
			function init(){
				//initFootable();
				
				//上一頁按鈕
				$("#previous").click(function() {
					initBlockUI();
					fstop.getPage('${pageContext.request.contextPath}'+'/FCY/ACCT/f_demand_deposit_detail','', '');
				});

				$("#printbtn").click(function(){
					var params = {
						"jspTemplateName":"f_demand_deposit_result_print",
						"jspTitle":"<spring:message code='LB.FX_Demand_Deposit_Detail' />",
						"CMQDATE":"${demand_deposit_result.data.cmqdate}",
						"CMQTIME":"${demand_deposit_result.data.CMQTIME}",
						"COUNT":"${demand_deposit_result.data.COUNT}",
					};
					openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
				});		
				
			}
				//選項
			 	function formReset() {
// 			 		initBlockUI();
		 			if ($('#actionBar').val()=="excel"){
						$("#downloadType").val("OLDEXCEL");
						$("#templatePath").val("/downloadTemplate/f_demand_deposit.xls");
			 		}else if ($('#actionBar').val()=="txt"){
						$("#downloadType").val("TXT");
						$("#templatePath").val("/downloadTemplate/f_demand_deposit.txt");
			 		}else if ($('#actionBar').val()=="oldtxt"){
						$("#downloadType").val("OLDTXT");
						$("#templatePath").val("/downloadTemplate/f_demand_depositOLD.txt");
			 		}
// 		    		ajaxDownload("${__ctx}/ajaxDownload","formId","finishAjaxDownload()");
		            $("#formId").attr("target", "");
		            $("#formId").submit();
		            $('#actionBar').val("");
				}
				function finishAjaxDownload(){
					$("#actionBar").val("");
					unBlockUI(initBlockId);
				}
		</script>
</body>
	<c:if test="${showDialog == 'true'}">
 		<%@ include file="../index/txncssslog.jsp"%>
 	</c:if>
</html>