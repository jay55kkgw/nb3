<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:if test="${ not empty f_deposit_transfer}">
	<c:set var="TOMORROW" value="${f_deposit_transfer.data.TOMORROW}"/>
	<c:set var="NextYearDay" value="${f_deposit_transfer.data.NextYearDay}"/>
</c:if>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript">
	var i18nValue = {} ;
	i18nValue['LB.Select'] = '<spring:message code="LB.Select"/>';//請選擇
	i18nValue['LB.Month'] = '<spring:message code="LB.Month"/>';//月
	i18nValue['LB.Week'] = '<spring:message code="LB.Week"/>';//週
	i18nValue['available_balance'] = '<spring:message code="LB.Available_balance" />: ';
	var crynoTable = {};//幣別對應 格式[cry]=cryno
	var vipNumf = '${f_deposit_transfer.data.VIPNUMF}';
	
		// 初始化
		$(document).ready(function () {
			init();
			// 初始化時隱藏span
			$("input[name='hideblock']").hide();
			$("#hideblock_CMDATE").hide();
			$("#hideblock_CMEDATE").hide();
			$("#hideblock_CMEDATE").hide();
		});

		function init() {
			BGROENOdivShow();//若是WsN920.getFxDepOutAcnoList的VIPNUMF欄位 !=null && =Y時，顯示此欄，反之隱藏此欄
			IsHoliday();
			tabEvent(); //標籤切換事件
			datetimepickerEvent(); // 日曆欄位參數設定
			fgtrdateEvent(); // 預約日期change事件 ，檢核日期邏輯
			creatCMPERIOD_Select(); // 建立月天數的下拉選單
			creatTXTIMES_Select(); //建立轉期下拉選單  
			FYTSFANEvent(); //轉出帳號change事件，要產生轉帳幣別下拉選單
			INACNOEvent();//轉入帳號change事件
			OutCryEvent(); //轉出帳號幣別change事件，要秀出可用餘額
			//acn
			var getacn = '${f_deposit_transfer.data.urlID}';
			if(getacn != null && getacn != ''){
				$("#FYTSFAN option[value= '"+ getacn +"' ]").prop("selected", true);
				creatCurrency(getacn);
			}
			//表單驗證
			$("#formId").validationEngine({binded: false,promptPosition: "inline"});
			
			refillData();//回上頁資料回填
			
			// form Submit 
			$("#CMSUBMIT").click(function (e) {
				
				// 打開驗證隱藏欄位
				$("input[name='hideblock']").show();
	 			$("#hideblock_CMDATE").show();
	 			$("#hideblock_CMSDATE").show();
	 			$("#hideblock_CMEDATE").show();
				// 塞值到 驗證欄位input
				$("#validate_CMDATE").val($("#CMDATE").val());
				$("#validate_CMSDATE").val($("#CMSDATE").val());
				$("#validate_CMEDATE").val($("#CMEDATE").val());
				$("#H_CMTRMAIL").val($("#CMTRMAIL").val());
				
				if($("#FGTRDATE2").prop('checked')){
					$("#validate_CMEDATE2").show();
				}else{
					$("#validate_CMEDATE2").hide();
				}
				
				console.log("submit~~");
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					initBlockUI();//遮罩
					$("input").prop("disabled",false);//避免屬性被移除後端接不到值
					$("#formId").submit();
				}
			});
		}
		//回上頁資料回填
		function refillData(){
			if('${bk_key}'=='Y'){				
				//預約及時判斷
				if('${requestScope.back_data.FGTRDATE}'!='0'){
					$("#nav-trans-future").click();
					//預約時間回填
					$("input:radio[name='FGTRDATE'][value='${requestScope.back_data.FGTRDATE}']").click();
					
					if('${requestScope.back_data.FGTRDATE}'=='1'){
					//預約單日
					$('#CMDATE').val('${requestScope.back_data.CMDATE}');
					}
					else if('${requestScope.back_data.FGTRDATE}'=='2'){
					//預約定期每月的第幾日					
						$("#CMPERIOD option[value= '"+ ${requestScope.back_data.CMPERIOD} +"' ]").prop("selected", true);
						$('#CMSDATE').val('${requestScope.back_data.CMSDATE}')
						$('#CMEDATE').val('${requestScope.back_data.CMEDATE}') 
					}
					
				}
				//轉出帳號回填				
				var getmp = '${requestScope.back_data.FYTSFAN}';
				if(getmp != null && getmp != ''){
					$("#FYTSFAN option[value= '"+ getmp +"' ]").prop("selected", true);
					$("#FYTSFAN").change();
				}
				//轉帳幣別回填			
				getmp = '${requestScope.back_data.OUTCRY}';
				if(getmp != null && getmp != ''){
					$("#OUTCRY option[value= '"+ getmp +"' ]").prop("selected", true);
					$("#OUTCRY").change();
				}
				//轉入帳號回填 			
				getmp = '${requestScope.back_data.INACNO1}';
				if(getmp != null && getmp != ''){
					$("#INACNO1 option[value= '"+ getmp +"' ]").prop("selected", true);
					$("#INACNO1").change();
				}
				//存款期別回填			
				getmp = '${requestScope.back_data.TYPCOD}';
				if(getmp != null && getmp != ''){
					$("#TYPCOD option[value= '"+ getmp +"' ]").prop("selected", true);
					$("#TYPCOD").change();
				}
				//轉帳金額回填
				$('#AMOUNT').val('${requestScope.back_data.AMOUNT}');
				$('#AMOUNT_DIG').val('${requestScope.back_data.AMOUNT_DIG}');
				//計息方式回填
				var tmpvalue='${requestScope.back_data.INTMTH}';
				if('${requestScope.back_data.INTMTH}'!='')
					$($('input[name ="INTMTH"]')[tmpvalue]).click();
				//轉存方式回填
				$($("input[name=CODE][value=${requestScope.back_data.CODE}]")).click();
				//議價編號回填
				$('#BGROENO').val('${requestScope.back_data.BGROENO}');
				//交易備註回填
				$('#CMTRMEMO').val('${requestScope.back_data.CMTRMEMO}')
			 	$('#CMTRMAIL').val('${requestScope.back_data.CMTRMAIL}');
			 	$('#CMMAILMEMO').val('${requestScope.back_data.CMMAILMEMO}');
				console.log("leo is handsome");
			}
			console.log("LEO >> ${bk_key}");
		}		
		//若是WsN920.getFxDepOutAcnoList的VIPNUMF欄位 !=null && =Y時，顯示此欄，反之隱藏此欄
		function BGROENOdivShow() {
		    if (vipNumf != null && vipNumf == 'Y') {
		        BGROENOdiv.style.display = "";
		    } else {
		    }
		}
		//非營業時間隱藏即時功能
		function IsHoliday(){
			console.log("isholidayflag"+$("#IsHolidayFlag").val());
			if($("#IsHolidayFlag").val()=="true"){
				$("#transfer-date").show();
				fstop.setRadioChecked("FGTRDATE", "1", true);
				$("#nav-trans-future").click();
				//TODO非營業時間提示
				//alert("<spring:message code= "LB.Alert006" />");
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert006' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			}
			else{
				$("#nav-trans-now").click();
			}
		}
		//把下拉選單選中的文字塞到隱藏欄位
		function changeDisplay() {
			$("#INACNO1_TEXT").val($("#INACNO1").find(":selected").text());
		}
		//標籤切換事件
		function tabEvent() {
			if($("#IsHolidayFlag").val()!="true"){
				$("#nav-trans-now").click(function () {
					console.log("hi>>");
					$("#transfer-date").hide();
					fstop.setRadioChecked("FGTRDATE", "0", true);
				})
				$("#nav-trans-future").click(function () {
				$("#transfer-date").show();
				fstop.setRadioChecked("FGTRDATE", "1", true);
				})
			}
		}
		// 日曆欄位參數設定
		function datetimepickerEvent() {
			$(".CMDATE").click(function (event) {
				$('#CMDATE').datetimepicker('show');
			});
			$(".CMSDATE").click(function (event) {
				$('#CMSDATE').datetimepicker('show');
			});
			$(".CMEDATE").click(function (event) {
				$('#CMEDATE').datetimepicker('show');
			});
			jQuery('.datetimepicker').datetimepicker({
				timepicker: false,
				closeOnDateSelect: true,
				scrollMonth: false,
				scrollInput: false,
				format: 'Y/m/d',
				lang: '${transfer}'
			});
		}
		//預約日期change事件 ，檢核日期邏輯
		function fgtrdateEvent() {
			$('input[type=radio][name=FGTRDATE]').change(function () {
				if (this.value == '1') {
					console.log("tomorrow");
					// id="CMDATE" type="text" name="CMDATE" class="text-input validate[required]"
					$("#CMDATE").addClass("validate[required]")
					$("#CMPERIOD").removeClass("validate[required]");
					$("#CMSDATE").removeClass("validate[required]");
					$("#CMEDATE").removeClass("validate[required]");
				} else if (this.value == '2') {
					console.log("Transfer Thai Gayo");
					$("#CMDATE").removeClass("validate[required]");
					$("#CMPERIOD").addClass("validate[required]")
					$("#CMSDATE").addClass("validate[required , past[#CMEDATE] ,future[now]]")
					$("#CMEDATE").addClass("validate[required , future[#CMSDATE]]")

				}
			});
		}
		//轉入帳號change事件
		function INACNOEvent() {
			$('input[type=radio][name=FGINACNO]').change(function () {
				if (this.value == 'CMDAGREE') {
					$("#INACNO1").addClass("validate[required]")
					$("#INACNO2").removeClass("validate[required]");
				} else{
					$("#INACNO1").removeClass("validate[required]");
					$("#INACNO2").addClass("validate[required]")
				}
			});
		}
		// 建立月天數的下拉選單
		function creatCMPERIOD_Select() {
			var day = new Object();
			var options = {keyisval: false,selectID: '#CMPERIOD'}
			for (var i = 1; i <= 31; i++) {
				day[i] = i;
			}
			fstop.creatSelect(day, options);
		}
		//建立轉期下拉選單  
		function creatTXTIMES_Select() {
			//宣告變數，資料類型為物件 
			var dataTxTimes_1 = {}
			var dataTxTimes_2 = {}
			var options = {selectID: '#AUTXFTM'}
			// 先建立01-09
			for (i = 1; i <= 9; i++) {
				dataTxTimes_1['0' + i] = i;
			}
			// 再建立10-98
			for (i = 10; i <= 98; i++) {
				dataTxTimes_2[i] = i;
			}
			fstop.creatSelect(dataTxTimes_1, options);
			fstop.creatSelect(dataTxTimes_2, options);
		}
		//轉出帳號change事件，要產生轉帳幣別下拉選單
		function FYTSFANEvent() {
			$("#FYTSFAN").change(function () {
				var FYTSFAN = $('#FYTSFAN :selected').val();
				console.log("FYTSFAN>>" + FYTSFAN);
				creatCurrency(FYTSFAN);
			});
		}
		//建立轉帳幣別下拉選單
		function creatCurrency(FYTSFAN) {
			var uri = '${__ctx}' + "/FCY/ACCT/TDEPOSIT/getCurrency_aj"
			console.log("getSelectData>>" + uri);
			var rdata = {acno: FYTSFAN};
			var data = fstop.getServerDataEx(uri, rdata, false);
			if (data != null && data.result == true) {
				//宣告變數，資料類型為物件 
				var dataTerm = {}
				var rowDatas = $.parseJSON(data.data);
				
				//建立選單資料
				for(var x=0;x<rowDatas.length;x++){
					var cryNo = rowDatas[x].ADCCYNO;
					var cry = rowDatas[x].ADCURRENCY;
					var cryName = rowDatas[x].ADCCYNAME;
					//
					if("00"===cryNo){
						continue;
					}
					dataTerm[cryNo] = cry + ' ' + cryName;
					crynoTable[cryNo] = cry;
					
				}
				console.log('建立選單資料END----------------')
				//建立幣別下拉選單 
				$('#OUTCRY').find("option").remove(); //先清除下拉選單資料
				var options = {keyisval: false,selectID: '#OUTCRY'}
				var termdf = {};
			 	termdf[''] = '---'+i18nValue['LB.Select']+'---'; //因為選擇清除下拉選單資料所以要加上
				fstop.creatSelect(termdf, options);//建立請選擇
				fstop.creatSelect(dataTerm, options);//建立幣別
			}
		}
		// 建立存款期別下拉選單 當幣別選美金
		function creatTYPCOD_SelectByUSD() {
			var options = {selectID: '#TYPCOD'}
			//宣告變數，資料類型為物件 
			var dataTerm = {}
			//建立選單資料
			dataTerm[''] = '---'+i18nValue['LB.Select']+'---';//請選擇
			dataTerm['W001'] = '1'+i18nValue['LB.Week'];
			dataTerm['W002'] = '2'+i18nValue['LB.Week'];
			dataTerm['W003'] = '3'+i18nValue['LB.Week'];
			dataTerm['M001'] = '1<spring:message code= "LB.D0574" />';
			dataTerm['M002'] = '2<spring:message code= "LB.Months" />';
			dataTerm['M003'] = '3<spring:message code= "LB.Months" />';
			dataTerm['M004'] = '4<spring:message code= "LB.Months" />';
			dataTerm['M005'] = '5<spring:message code= "LB.Months" />';
			dataTerm['M006'] = '6<spring:message code= "LB.Months" />';
			dataTerm['M009'] = '9<spring:message code= "LB.Months" />';
			dataTerm['M012'] = '12<spring:message code= "LB.Months" />';
			fstop.creatSelect(dataTerm, options);
		}
		// 建立存款期別下拉選單 
		function creatTYPCOD_Select() {
			var options = {
				selectID: '#TYPCOD'
			}
			//宣告變數，資料類型為物件 
			var dataTerm = {}
			//建立選單資料
			dataTerm[''] = '---'+i18nValue['LB.Select']+'---';
			dataTerm['W001'] = '1'+i18nValue['LB.Week'];
			dataTerm['W002'] = '2'+i18nValue['LB.Week'];
			dataTerm['W003'] = '3'+i18nValue['LB.Week'];
			dataTerm['M001'] = '1<spring:message code= "LB.D0574" />';
			dataTerm['M003'] = '3<spring:message code= "LB.Months" />';
			dataTerm['M006'] = '6<spring:message code= "LB.Months" />';
			dataTerm['M009'] = '9<spring:message code= "LB.Months" />';
			dataTerm['M012'] = '12<spring:message code= "LB.Months" />';
			fstop.creatSelect(dataTerm, options);
		}
		// 根據選取幣別改變存款期別
		function changeTYPCOD_Select(cryNo) {
			//先清除TYPCOD Select
			$('#TYPCOD').find("option").remove();
			if ('01' === cryNo) {
				creatTYPCOD_SelectByUSD();
			} else {
				creatTYPCOD_Select();
			}
		}
		//判斷是否要DISABLE轉帳金額的小數點
		function checkDigit(cryNo) {
			if(cryNo == "15"){
				$("#AMOUNT_DIG").prop("disabled",true);
			}
			else{
				$("#AMOUNT_DIG").prop("disabled",false);
			}
		}
		//存款期別change事件
		function typcodChange(){
			var typcotext = $("#TYPCOD").find(":selected").text();
			console.log("typcotext >>" + typcotext)
			//修改隱藏欄位存款期別名稱
			$("#TYPCOD_TEXT").val(typcotext);
			
			changeRate();
		}			
		//轉出帳號幣別change事件，要秀出可用餘額
		function OutCryEvent() {
			$("#OUTCRY").change(function () {
				if($("#OUTCRY").val() != ""){
					var FYTSFAN = $('#FYTSFAN :selected').val();
					var cryNo = $('#OUTCRY :selected').val();
					var cry = crynoTable[cryNo];
					console.log("FYTSFAN>>" + FYTSFAN);
					console.log("cryNo>>" + cryNo);
					console.log("cry>>" + cry);
					//顯示轉帳金額欄之幣別  
					var cryName = $('#OUTCRY :selected').text();
					$("#TRCRY").text(cryName);
					//修改隱藏欄位幣別名稱
					$("#OUTCRY_TEXT").val(cryName);
					// 取得轉出幣別餘額資料
					try{
						getCry_Data(FYTSFAN, cry);
					}
					catch(e){
						console.log("e=" + e);
					}
					changeTYPCOD_Select(cryNo);
					checkDigit(cryNo);
					checkOutcry(cryNo);
				}
			});
		}
		//取得轉出幣別餘額資料
		function getCry_Data(FYTSFAN, cry) {
			uri = '${__ctx}' + "/FCY/COMMON/getACNO_Currency_Data_aj"
			console.log("getACNO_Data>>" + uri);
			rdata = {acno: FYTSFAN,cry: cry};
			console.log("rdata>>" + rdata);
			fstop.getServerDataEx(uri, rdata, true, isShowACNO_Data);
		}
		//取得轉出幣別餘額資料(非傳入參數)
		function getCry_Data() {
			var FYTSFAN = $('#FYTSFAN :selected').val();
			var cryNo = $('#OUTCRY :selected').val();
			var cry = crynoTable[cryNo];
			console.log("FYTSFAN>>" + FYTSFAN);
			console.log("cryNo>>" + cryNo);
			console.log("cry>>" + cry);
			uri = '${__ctx}' + "/FCY/COMMON/getACNO_Currency_Data_aj"
			console.log("getACNO_Data>>" + uri);
			rdata = {acno: FYTSFAN,cry: cry};
			console.log("rdata>>" + rdata);
			fstop.getServerDataEx(uri, rdata, true, isShowACNO_Data);
		}
		//顯示轉出帳號幣別餘額資料
		function isShowACNO_Data(data) {
			console.log("isShowACNO_Data.data >>> " + data);
			$("#FYTSFANIsShow").hide();
			if (data && data.result) {
				var bal = "0.00";
				if(data.data.accno_data) {
					bal = fstop.formatAmt(data.data.accno_data.AVAILABLE); //格式化金額
				}
				console.log("bal :" + bal);
				// 格式化金額欄位
				var showBal = i18nValue['available_balance'] + bal;
				$("#FYTSFANIsShow").show();
				$("#showText").html(showBal);
			} else {
				$("#FYTSFANIsShow").hide();
			}
		}
		
		
		
		function checkOutcry(cryNo){
			if(cryNo != '01' && cryNo !='19'){
				$("#DPINTTYPE2").prop("checked",true);
				$("#DPINTTYPE1").prop("disabled",true);
				$("#DPINTTYPE2").prop("disabled",false);
			}
			else{
				$("#DPINTTYPE1").prop("disabled",false);
				$("#DPINTTYPE2").prop("disabled",false);
			}
		}
		
		function changeRate(){
			var cryNo = $("#OUTCRY").val();
			var type = $("#TYPCOD").val();
			
			if(type.substring(0,1) == "W"){
				$("#DPINTTYPE2").prop("checked",true);
				$("#DPINTTYPE1").prop("disabled",true);
				$("#DPINTTYPE2").prop("disabled",true);
			}  	
			else{
			 	if(cryNo == '01' || cryNo =='19'){
			 		$("#DPINTTYPE1").prop("disabled",false);
			 		$("#DPINTTYPE2").prop("disabled",false);
			    }
			    else{
			    	$("#DPINTTYPE2").prop("checked",true);
			  		$("#DPINTTYPE1").prop("disabled",true);
			  		$("#DPINTTYPE2").prop("disabled",false);
			    }
			}
		}
		function changeCODE(){
			var codeValue = $("input[name=CODE]:checked").val();
			
			if(codeValue == 'C'){
				$("#FGAUTXFTM").val("N");
				$("#AUTXFTM").prop("disabled",true);
			}	
		  	else if(codeValue == 'D'){
		  		$("#FGAUTXFTM").val("N");
		  		$("#AUTXFTM").prop("disabled",true);
		  	}
		  	else if(codeValue == 'A' || codeValue == 'B' || codeValue == 'E'){ 	  
		  		$("#FGAUTXFTM").val("Y");
		  		$("#AUTXFTM").prop("disabled",false);
		  	}
		}
		
		//檢核機動利率
		function checkRate(){
			var BGROENOvalue = $("#BGROENO").val();
			
			if(BGROENOvalue != ''){
				$("#DPINTTYPE2").prop("checked",true);
		  		$("#DPINTTYPE1").prop("disabled",true);
		  		$("#DPINTTYPE2").prop("disabled",true);
			}  	
			else{
				$("#DPINTTYPE1").prop("disabled",false);
		  		$("#DPINTTYPE2").prop("disabled",false);
			}
		}
		//copyTN
		function copyTN(){
			$("#CMMAILMEMO").val($("#CMTRMEMO").val());
		}
		//開啟通訊錄email選單
		function openAddressbook(){
			window.open('${__ctx}/NT/ACCT/TRANSFER/showAddressbook');
		}
		 

	</script>
</head>

<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 定存服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Current_Deposit_To_Time_Deposit" /></li>
    <!-- 轉入外幣綜存定存     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Open_Foreign_Currency_Time_Deposit" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
	</div>
	<!-- 主頁內容  -->
	<main class="col-12">
		<section id="main-content" class="container">
			<h2>
				<!--轉入外匯綜存定存 -->
				<spring:message code="LB.Open_Foreign_Currency_Time_Deposit" />
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form autocomplete="off" method="post" id="formId" action="${__ctx}/FCY/ACCT/TDEPOSIT/f_deposit_transfer_confirm">
				<input type="hidden" name="INACNO1_TEXT" id="INACNO1_TEXT" />
				<input type="hidden" name="OUTCRY_TEXT" id="OUTCRY_TEXT" />
				<input type="hidden" name="TYPCOD_TEXT" id="TYPCOD_TEXT" />
				<input type="hidden" name="FGAUTXFTM" id="FGAUTXFTM" value="Y"/>
				<!--交易步驟 -->
				<div id="step-bar">
					<ul>
						<!--輸入資料 -->
						<li class="active"><spring:message code="LB.Enter_data"/></li>
						<!-- 確認資料 -->
						<li class=""><spring:message code="LB.Confirm_data"/></li>
						<!-- 交易完成 -->
						<li class=""><spring:message code="LB.Transaction_complete"/></li>
					</ul>
        		</div>
				<!--表單顯示區 -->
				<div class="main-content-block row radius-50">
					<nav style="width: 100%;">
						<div class="nav nav-tabs" id="nav-tab" role="tablist">
						       <!--  是否為營業時間 -->
						<input type="hidden" id="IsHolidayFlag" value="${f_deposit_transfer.data.IsHoliday}">
							<a class="nav-item nav-link" id="nav-trans-now" data-toggle="tab" href="#nav-trans-now" role="tab" aria-controls="nav-home"
							 aria-selected="true">
								<!--及時 -->
								<spring:message code="LB.Immediately"/>
							</a>
							<a class="nav-item nav-link" id="nav-trans-future" data-toggle="tab" href="#nav-trans-future" role="tab"
							 aria-controls="nav-profile" aria-selected="false">
								<!-- 預約 -->
								<spring:message code="LB.Booking"/>
							</a>
						</div>
					</nav>
					<div class="col-12 tab-content" id="nav-tabContent">
						<div class="tab-pane fade " id="nav-trans-future" role="tabpanel" aria-labelledby="nav-profile-tab">
						</div>
						<div class="ttb-input-block tab-pane fade show active" id="nav-trans-now" role="tabpanel" aria-labelledby="nav-home-tab">
							<div class="ttb-message">
								<span></span>
							</div>
							<!-- 這是即時的屬性  預設就是隱藏 -->
							<div class="ttb-input" style="display: none;">
								<label class="radio-block">
									<!-- 即時 -->
									<spring:message code="LB.Immediately"/>
									<input type="radio" name="FGTRDATE" value="0" checked>
									<span class="ttb-radio"></span>
								</label>
							</div>
							<!-- 轉帳日期 -->
							<div id="transfer-date" class="ttb-input-item row" style="display: none;">
								<span class="input-title">
									<!-- 轉帳日期 -->
									<label>
										<h4><spring:message code="LB.Transfer_date" /></h4>
									</label>
								</span>
								 <span class="input-block">
									<div class="ttb-input ">
										<label class="radio-block">
											<!-- 預約 -->
											<spring:message code="LB.Booking" />
											<input type="radio" name="FGTRDATE" value="1">
											<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<input id="CMDATE" name="CMDATE"  type="text" class="text-input datetimepicker validate[required]"
										  value="${TOMORROW}">
										<!-- 日曆元件 -->
										<span class="input-unit CMDATE"><img src="${__ctx}/img/icon-7.svg" /></span>
										<!-- 驗證用的span預設隱藏 -->
										<span id="hideblock_CMDATE" >
										<!-- 驗證用的input -->
										<input id="validate_CMDATE" name="validate_CMDATE" type="text" 
											class="text-input validate[funcCall[validate_CheckDate['<spring:message code= "LB.X0377" />', validate_CMDATE, ${TOMORROW}, ${NextYearDay}]]]"
											style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</span>
									</div>
									<div class="ttb-input">
										<label class="radio-block"> 
											<!-- 預約 -->
											<spring:message code="LB.Booking"/>
											<input type="radio" name="FGTRDATE" id="FGTRDATE2" value="2">
											<!-- 每月 -->
											<spring:message code="LB.Monthly"/>
											<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<select id="CMPERIOD" name="CMPERIOD" class="custom-select select-input half-input">
											<!-- 請選擇 -->
											<option value=""><spring:message code="LB.Select" /></option>
										</select> 
										<!--日 -->
										<span class="input-unit"><spring:message code="LB.Day"/></span>
									</div>
									<div class="ttb-input">
										<span class="input-subtitle subtitle-color">
											<!-- 期間起日 -->
											<spring:message code="LB.Period_start_date" />
										</span>
										<input name="CMSDATE" id="CMSDATE" type="text" class="text-input datetimepicker" value="${TOMORROW}">
										<span class="input-unit CMSDATE"><img src="${__ctx}/img/icon-7.svg" /></span>
										<!-- 驗證用的span預設隱藏 -->
										<span id="hideblock_CMSDATE" >
										<!-- 驗證用的input -->
											<input id="validate_CMSDATE" name="validate_CMSDATE" type="text" class="text-input validate[required, verification_date[validate_CMSDATE]]" 
											style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</span>
									</div>
									<div class="ttb-input">
										<span class="input-subtitle subtitle-color">
											<!-- 迄日 -->
											<spring:message code="LB.Period_end_date" />
										</span>
										<input type="text" name="CMEDATE" id="CMEDATE" type="text" class="text-input datetimepicker" value="${TOMORROW}">
										<span class="input-unit CMEDATE">
											<img src="${__ctx}/img/icon-7.svg" />
										</span>
										<!-- 驗證用的span預設隱藏 -->
										<span id="hideblock_CMEDATE" >
											<!-- 驗證用的input -->
										<input id="validate_CMEDATE" name="validate_CMEDATE" type="text" class="text-input validate[required,verification_date[validate_CMEDATE]]" 
											style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										<input id="validate_CMEDATE2" name="validate_CMEDATE2" type="text" class="text-input validate[funcCallRequired[validate_CheckPerMonthDay['<spring:message code= "LB.X1455" />',CMPERIOD,CMSDATE,CMEDATE]]" 
											style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</span>
									</div>
								</span>
							</div>
							<!-- 轉出帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Payers_account_no" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<select name="FYTSFAN" id="FYTSFAN" class="custom-select select-input half-input validate[required]" onchange="getCry_Data()">
											<!--  請選擇帳號 -->
											<option value="">---<spring:message code="LB.Select_account" />---</option>
											<c:forEach var="dataList" items="${f_deposit_transfer.data.outAcnoList}">
												<option value="${dataList}"> ${dataList}</option>
											</c:forEach>
										</select>
									</div>
								</span>
							</div>
							<!-- 轉出幣別 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code= "LB.X1487" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input ">
										<select name="OUTCRY" id="OUTCRY" class="custom-select select-input half-input validate[required]" onchange="getCry_Data()" >
											<!-- 請選擇 -->
											<option value="">
												<spring:message code="LB.Select" />
											</option>
										</select>
										<!-- 可用餘額 : -->
										<div id="FYTSFANIsShow" style="display: none">
											<span class="input-unit">
											<span id="showText" class="input-unit "></span>
											</span>
										</div>
									</div>
								</span>
							</div>
							<!-- 轉入帳戶 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Payees_account_no" /></h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input" style="display:none">
										<label class="radio-block">
											<!-- 已約定 -->
											<spring:message code="LB.Designated_account" />
											<input type="radio" name="FGINACNO" value="CMDAGREE" checked>
											<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<select id="INACNO1" name="INACNO1" class="custom-select select-input half-input validate[required,funcCall[validate_CheckSelect[<spring:message code='LB.Payees_account_no' />,INACNO1,#]]]" onchange="changeDisplay()">
											<!-- 請選擇帳號 -->
										<option value="">----<spring:message code="LB.Select_designated_or_common_non-designated_account" />-----</option>
											<c:forEach var="dataList" items="${f_deposit_transfer.data.inAcnoList}">
												<option value='${dataList.VALUE}'> <c:out value='${dataList.TEXT}' /></option>
											</c:forEach>
										</select>
										<!-- TODO	這裡還有一個button 去查詢約定轉入帳號資料  call N933電文 -->
										<!-- <input type="text" class="text-input" placeholder="請選擇約定/常用非約定帳號" /> -->
									</div>
<!-- 									<div class="ttb-input"> -->
<!-- 										<label class="radio-block"> -->
<!-- 											非約定帳號 -->
<%-- 											<spring:message code="LB.Non-designated_account" /> --%>
<!-- 											<input type="radio" name="FGINACNO" value="CMDISAGREE"> -->
<!-- 											<span class="ttb-radio"></span> -->
<!-- 										</label> -->
<!-- 									</div> -->
<!-- 									<div class="ttb-input"> -->
<!-- 										請輸入帳號 -->
<%-- 										<input type="text" name="INACNO2" id="INACNO2" size="16" maxlength="16" placeholder="<spring:message code="LB.Enter_Account"/>"/>  					     --%>
<!-- 										<label class="check-block"> -->
<!-- 											加入常用帳號 -->
<%-- 											<spring:message code="LB.Join_common_account" /> --%>
<!-- 											<input type="checkbox" name="ADDACN" id="ADDACN" value="checked"/> -->
<!-- 											<span class="ttb-check"></span> -->
<!-- 										</label> -->
<!-- 									</div> -->
								</span>
							</div>
							<!-- 存款期別 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<!-- 存款期別 -->
										<h4><spring:message code="LB.Deposit_period" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
											<select class="custom-select select-input half-input validate[required]" name="TYPCOD" id="TYPCOD" onchange="typcodChange()">
											<!-- 請選擇 -->
											<option value="">
												<spring:message code="LB.Select" />
											</option>
											</select>
									</div>
								</span>
							</div>
							<!-- 轉帳金額 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<!-- 轉帳金額 -->
										<h4><spring:message code="LB.Amount" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<p id="TRCRY"></p>
										<input class="text-input validate[required ,custom[integer,min[1] ,max[null]]]" size="11" maxlength="11" id="AMOUNT" name="AMOUNT">
										.
										<input class="text-input" maxLength="2" size="3" value="00" id="AMOUNT_DIG" name="AMOUNT_DIG">
										<span class="input-unit">
										</span>
									</div>
								</span>
							</div>
							<!-- 計息方式 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<!--  計息方式 -->
										<h4><spring:message code="LB.Interest_calculation" /></h4>
									</label>
								</span>
								<span id="INTMTHDIV" class="input-block">
									<label class="radio-block">
										<!-- 機動 -->
										<spring:message code="LB.Floating" />
										<input type="radio" name="INTMTH" id="DPINTTYPE1" value="0">
										<span class="ttb-radio"></span>
									</label>
									<label class="radio-block">
										<!-- 固定 -->
										<spring:message code="LB.Fixed" />
										<input type="radio" name="INTMTH" id="DPINTTYPE2" value="1" checked="checked">
										<span class="ttb-radio"></span>
									</label>
								</span>
							</div>
							<!-- 轉存方式 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<!--  轉存方式 -->
										<h4><spring:message code="LB.Rollover_method" /></h4>
									</label>
								</span>
								<span class="input-block">
									<!-- 第一區 -->
										<label>
											<!-- 自動轉期 -->
											<spring:message code="LB.Automatic_renewal" />
										</label>
									<div class="ttb-input">
										<label>
											<!-- 轉期 -->
											<spring:message code="LB.Is_renewal"/>
											<select class="custom-select select-input half-input w-auto" NAME="AUTXFTM" id="AUTXFTM">
												<option value="99">
													<!--  無限 -->
													<spring:message code="LB.Unlimited_1" />
												</option>
											</select>
													<!--次 -->
													<spring:message code="LB.Time_s"/>
										</label>
									</div>
									<!-- 本金利息一併轉期 -->
									<div class="ttb-input sen-input">
										<label class="radio-block">
											<spring:message code="LB.Principal_and_Interest_rollover1"/>
											<input type="radio" value="A" name="CODE" checked onclick="changeCODE()"/>
											<span class="ttb-radio"></span>
										</label>
									</div>
									<!-- 本金轉期－利息按月轉入活存 -->
									<div class="ttb-input sen-input">
										<label class="radio-block">
											<spring:message code="LB.Principal_rollover_interest_monthly_to_demand_deposit"/>
											<input type="radio" value="B" name="CODE" onclick="changeCODE()">
											<span class="ttb-radio"></span>
										</label>
									</div>
									<!--本金轉期－利息於定存到期或解約轉入活存 -->
									<div class="ttb-input sen-input">
										<label class="radio-block">
											<!-- 本金轉期 -->
											<spring:message code="LB.Principal_rollover"/>
											－
											<!-- 利息於定存到期或解約轉入活存 -->
											<spring:message code="LB.Interest_to_demand_deposit_at_maturity_or_termination"/>
											<input type="radio" value="E" name="CODE" onclick="changeCODE()">
											<span class="ttb-radio"></span>
										</label>
									</div>
									<!-- 第二區 -->
									<div class="ttb-input">
										<label>
											<!-- 不轉期 -->
											<spring:message code="LB.Not_automatic_renewal" />
										</label>
									</div>
									<!--  利息按月轉入活存 -->
									<div class="ttb-input sen-input">
										<label class="radio-block">
											<spring:message code="LB.Interest_monthly_to_demand_deposit" />
											<input type="radio" value="C" name="CODE" onclick="changeCODE()">
											<span class="ttb-radio"></span>
										</label>
									</div>
									<!--  利息於定存到期或解約轉入活存 -->
									<div class="ttb-input sen-input">
										<label class="radio-block">
											<spring:message code="LB.Interest_to_demand_deposit_at_maturity_or_termination" />
											<input type="radio" value="D" name="CODE" onclick="changeCODE()">
											<span class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>
							 <!-- 議價編號-->
							<div id="BGROENOdiv" class="ttb-input-item row" style="display: none;">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Bargaining_number"/></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										 <input type="text" class="text-input" id="BGROENO" name="BGROENO" size="10" maxLength="10" onblur="checkRate()"/> 
									</div>
								</span>
							</div>
							<!-- 交易備註 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Transfer_note" /></h4>
									</label>
								</span>
								<!--(可輸入20字的用途摘要，您可於「轉出紀錄查詢」功能中查詢) -->
								<span class="input-block">
									<div class="ttb-input">
										<input type="text" id="CMTRMEMO" name="CMTRMEMO" class="text-input" maxlength="20"  value="">
										<span class="input-unit">
										</span>
										<span class="input-remarks"><spring:message code="LB.Transfer_note_note" /></span>
									</div>
								</span>
							</div>
							<!-- 轉出成功 Email通知 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<!-- 轉出成功Email 通知 -->
										<h4><spring:message code="LB.X0479" />
										：</h4>
									</label>
								</span>
								<span class="input-block">
									<c:if test="${f_deposit_transfer.data.sendMe == true && sessionScope.dpmyemail != ''}">
										<!--通知本人 -->
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color">
											<spring:message code="LB.Notify_me"/>
											：
											</span>
											<span class="input-subtitle subtitle-color">${sessionScope.dpmyemail}</span>
										</div>
										<!-- 另通知 -->
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color">
											<spring:message code="LB.Another_notice"/>
											：
											</span>
										</div>
									</c:if>
									<c:if test="${f_deposit_transfer.data.sendMe == false || sessionScope.dpmyemail == ''}">
										<!-- 電子信箱 -->
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color">
											<spring:message code="LB.Email"/>
											：
											</span>
										</div>
									</c:if>
									<!-- 通訊錄 -->
									<div class="ttb-input">
										<!--非必填 -->
										<spring:message code="LB.Not_required" var="notrequired"></spring:message>
										<input type="text" class="text-input" id="CMTRMAIL" name="CMTRMAIL"  maxlength="500" placeholder="${notrequired}">
										<span class="input-unit"></span>
										<!--window open 去查 TxnAddressBook  參數帶入身分證字號 -->
										<button class="btn-flat-orange" type="button" onclick="openAddressbook()">
											<spring:message code="LB.Address_book" />
										</button>
										<!-- 不在畫面上顯示的span -->
										<span name="hideblock" >
											<!-- 驗證用的input -->
											<input id="H_CMTRMAIL" name="H_CMTRMAIL" type="text" class="text-input validate[funcCall[validate_EmailCheck[CMTRMAIL]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</span>
										
									</div>
									<!-- 摘要內容 -->
									<div class="ttb-input">
										<spring:message code="LB.Summary" var="summary"></spring:message>
										<input type="text" id="CMMAILMEMO" name="CMMAILMEMO"  class="text-input"  maxlength="20" placeholder="${summary}">
										<span class="input-unit"></span>
											<!-- 同交易備註 -->
								        	<button class="btn-flat-orange" type="button" onclick="copyTN()">
								                <spring:message code="LB.As_transfer_note" />
								            </button>
									</div>
								</span>
							</div>
						</div>
						<!-- button -->
							<!--重新輸入 -->
							<spring:message code="LB.Re_enter" var="cmRest"></spring:message>
							<input type="reset" name="CMRESET" id="CMRESET" value="${cmRest}" class="ttb-button btn-flat-gray">
							<!-- 確定 -->
							<spring:message code="LB.Confirm" var="cmSubmit"></spring:message>
							<input type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" class="ttb-button btn-flat-orange">
						<!-- 						button -->
					</div>
				</div>
				<div class="text-left">
					<!-- 		說明： -->
					<ol class="list-decimal description-list">
						<p><spring:message code="LB.Description_of_page"/></p>
						<li><span>
						<spring:message code="LB.F_deposit_transfer_P1_D1-1"/>
<!-- 						交易最高限額表 -->
						<a href="${__ctx}/CUSTOMER/SERVICE/rate_table" target="_blank">
						<spring:message code="LB.F_deposit_transfer_P1_D1-2"/>
						</a>
<!-- 							 外匯存款最低起存額 -->
						<a href="https://www.tbb.com.tw/web/guest/-205" target="_blank">
							 <spring:message code="LB.F_deposit_transfer_P1_D1-3"/>
						</a>
<!-- 							 交易手續費及相關優惠 -->
						<a href="${__ctx}/public/transferFeeRate.htm" target="_blank">
							 <spring:message code="LB.F_deposit_transfer_P1_D1-4"/>
						</a>
<!-- 							 外幣存款利率查詢 -->
						<a href="https://www.tbb.com.tw/web/guest/-84" target="_blank">
						<spring:message code="LB.F_deposit_transfer_P1_D1-5"/>
						</a>
						<!--中文空白 ,英文有字 -->
						<spring:message code="LB.F_deposit_transfer_P1_D1-6"/>
						</span>
						</li>
						<li><span><spring:message code="LB.F_deposit_transfer_P1_D2"/></span></li>
						<li><span><spring:message code="LB.F_deposit_transfer_P1_D3"/></span></li>
						<li><span><spring:message code="LB.F_deposit_transfer_P1_D4"/></span></li>
						<li><span><spring:message code="LB.F_deposit_transfer_P1_D5"/></span></li>
<!-- 						<li>外匯預約交易可預約次日起1年內之交易。預約交易日如為曆法所無之日期，以該月之末日為交易日，如遇例假日/非銀行營業日，則順延至次一營業日。例如：預約交易日為6月31日，但6月無31日，故交易日為6月30日，又如6月30日為例假日則順延至7月1日。<font -->
<!-- 							 color="red">惟如遇交易分行颱風天等天災則預約交易視為交易失敗</font>。</li> -->
						<li><span><spring:message code="LB.F_deposit_transfer_P1_D6"/></span></li>
						<li><span><spring:message code="LB.F_deposit_transfer_P1_D7"/></span></li>
						<li><span><spring:message code="LB.F_deposit_transfer_P1_D9"/></span></li>
					</ol>	
				</div>
			</form>
		</section>
		<!-- main-content END -->
	</main>
	<%@ include file="../index/footer.jsp"%>
</body>

</html>