<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
		// 確認鍵 click
		goOn();
	}
	
	// 確認鍵 Click
	function goOn() {
		$("#CMOK").click( function(e) {
			console.log("submit~~");
			// 遮罩
         	initBlockUI();
			var next = "${__ctx}" + "${next}";
            $("#formId").attr("action", next);
	 	  	$("#formId").submit();
		});
	}

</script>
</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 臺幣存款帳戶結清銷戶申請    -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0422" /></li>
		</ol>
	</nav>

	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.D0422" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
               
                <form id="formId" method="post" action="${__ctx}${next}">
                <div class="main-content-block row">
                    <div class="col-12">
	                    <div class="ttb-input-block">
	                        <div class="ttb-message">
	                            <p><spring:message code= "LB.X0152" />：</p>
								<p><spring:message code= "LB.X1257" /></p>
	                        </div>
	                        <ul class="ttb-result-list terms">
								<li data-num="<spring:message code= "LB.X0237" />"><span><spring:message code= "LB.X1258" /></span></li>
								<li data-num="<spring:message code= "LB.X0240" />"><span><spring:message code= "LB.X1259" /></span></li>
								<li data-num="<spring:message code= "LB.X0243" />"><span><spring:message code= "LB.X1260" /></span></li>
								<li data-num="<spring:message code= "LB.X0245" />"><span><spring:message code= "LB.X1261" /></span></li>
								<li data-num="<spring:message code= "LB.X0247" />"><span><spring:message code= "LB.X1262" /></span></li>
								<li data-num="<spring:message code= "LB.X0249" />"><span><spring:message code= "LB.X1263" /></span></li>
								<li data-num="<spring:message code= "LB.X0251" />"><span><spring:message code= "LB.X1264" /></span></li>
								<li data-num="<spring:message code= "LB.X0253" />"><span><spring:message code= "LB.X1265" /></span></li>
	                        </ul>
                        </div>
                      	<!-- 確認  -->
						<spring:message code="LB.Confirm_1" var="Confirm"></spring:message>
						<input type="button" id="CMOK" value="${Confirm}" class="ttb-button btn-flat-orange" />
                    </div>
                </div>
				</form>
			</section>
		</main>
	</div><!-- content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>