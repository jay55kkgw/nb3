<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<!-- 讀卡機所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/cardReaderMethod.js"></script>
	
	<script type="text/JavaScript">
		// 遮罩用
		var blockId;
		
	    $(document).ready(function() {
	    	// HTML載入完成後開始遮罩
			initBlockUI();
			// 開始查詢資料並完成畫面
			setTimeout("init()", 100);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});

	 	// 初始化
	    function init(){
	    	// 即時、預約標籤 click
	    	tabEvent();
	    	// 建立約定轉出帳號下拉選單
	    	creatOutAcn();
	    	// 轉出帳號 change
	    	acnoEvent();
	    	// 預約自動輸入明天
	    	getTmrDate();
	    	// 預約日期 change
	    	fgtxdateEvent();
	    	// 西元年日期輸入欄位 click
	    	datetimepickerEvent();
	    	// 通訊錄 click
			addressbookClickEvent();
	    	
	    	// 回上一頁填入資料
			refillData();

			// 初始化後隱藏span( 表單驗證提示訊息用的 )
			$(".hideblock").hide();
			// 表單驗證
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
			// submit觸發事件
			finish();
	    }
	 	
	 	// 確認按鈕點擊觸發submit
		function finish(){
			$("#CMSUBMIT").click(function (e) {
				// 去掉舊的錯誤提示訊息
				$(".formError").remove();
				// 顯示驗證隱藏欄位( 為了讓提示訊息對齊 )
				$(".hideblock").show();
				
				console.log("submit~~");
				
				// 塞值進隱藏span內的input
				$("#CMDATE").val($("#CMDATE_TMP").val());
				$("#LIMDAT").val($("#LIMDAT_TMP").val());
				$("#AMOUNT").val($("#AMOUNT_TMP").val());
				// 塞值進隱藏span內的input--約定/非約定 之轉出帳號
				if( $('input[name=TransferType]:checked').val() == 'PD'){
					// 轉出帳號為約定
					$("#OUTACN").val($("#OUTACN_PD").val());
				}else if( $('input[name=TransferType]:checked').val() == 'NPD'){
					// 轉出帳號為非約定
					$("#OUTACN").val($("#OUTACN_NPD").val());
				}

			   	// 表單驗證--即時/預約
			   	var fgtxdate = $('input[name="FGTXDATE"]:checked').val();
			   	console.log('fgtxdate: ' + fgtxdate);
			   	
				var sMinDate = '${str_SystemDate}';
				var sMaxDate = '${sNextYearDay}';
				console.log("processQuery.sMinDate: " + sMinDate);
				console.log("processQuery.sMaxDate: " + sMaxDate);
				
				if (fgtxdate == '2'){
					// 驗證是否是可預約單日之日期
					$("#CMDATE").addClass("validate[required ,funcCall[validate_CheckDate[<spring:message code="LB.X0377" />,CMDATE,"+sMinDate+","+sMaxDate+"]]]");
			   	} else {
			   		// 取消驗證是否是可預約單日之日期
			   		$("#CMDATE").removeClass("validate[required ,funcCall[validate_CheckDate[<spring:message code="LB.X0377" />,CMDATE,"+sMinDate+","+sMaxDate+"]]]");
				}

				// 表單驗證--轉出帳號
				var transferType = $('input[name="TransferType"]:checked').val();
				console.log('transferType: ' + transferType);
				if(transferType == 'PD') {
					$("#OUTACN_PD").addClass("validate[required, funcCall[validate_CheckSelect['<spring:message code= "LB.Payers_account_no" />',OUTACN_PD,'']]]");
					$("#OUTACN_NPD").removeClass("validate[required, funcCall[validate_CheckSelect['<spring:message code= "LB.Payers_account_no" />',OUTACN_NPD,'']]]");
				} else if(transferType == 'NPD') {
					$("#OUTACN_PD").removeClass("validate[required, funcCall[validate_CheckSelect['<spring:message code= "LB.Payers_account_no" />',OUTACN_PD,'']]]");
					$("#OUTACN_NPD").addClass("validate[required, funcCall[validate_CheckSelect['<spring:message code= "LB.Payers_account_no" />',OUTACN_NPD,'']]]");
				}

				// 表單驗證
				if ( !$('#formId').validationEngine('validate') ) {
					e = e || window.event; // for IE
					e.preventDefault();
					
				} else {
					// 資料檢核，過了則送出表單
					processQuery();
				}
					
			});
	 	}

		// 即時、預約標籤切換事件
		function tabEvent(){
			// 即時
			$("#nav-trans-now").click(function() {
				$("#transfer-date").hide();
				fstop.setRadioChecked("FGTXDATE" , "1" ,true);
			})
			// 預約
			$("#nav-trans-future").click(function() {
				$("#transfer-date").show();
				fstop.setRadioChecked("FGTXDATE" , "2" ,true);
			})
		}

		// 建立約定轉出帳號下拉選單
		function creatOutAcn(){
			var options = { keyisval:false ,selectID:'#OUTACN_PD'};
			
			uri = '${__ctx}'+"/PAY/EXPENSE/getOutAcn_aj"
			rdata = {type: 'acno' };
			console.log("creatOutAcn.uri: " + uri);
			console.log("creatOutAcn.rdata: " + rdata);
			
			data = fstop.getServerDataEx(uri,rdata,false);

			console.log("creatOutAcn.data: " + JSON.stringify(data));
			
			if(data !=null && data.result == true ){
				fstop.creatSelect( data.data, options);
			}
		}
		
		// 轉出帳號change事件，要秀出選擇的轉出帳號可用餘額
		function acnoEvent(){
			$('input[type=radio][name=TransferType]').change(function() {
				if (this.value == 'PD') { //約定
					$("#OUTACN_NPD").val("");
				} else if (this.value == 'NPD') { //非約
					$("#OUTACN_PD").val("");
					$("#acnoIsShow").hide();
				}
			});
			$("#OUTACN_PD").change(function() {
				var acno = $('#OUTACN_PD :selected').val();
				console.log("acnoEvent.acno: " + acno);
// 				getACNO_Data(acno);
// 				如果此下拉選單有異動 radio 就改成checked
				if(fstop.isNotEmptyString(acno)){
					$("#OUTACN").val("");
					$("#OUTACN_NPD").val("");
					fstop.setRadioChecked("TransferType" , "PD" ,true);
					getACNO_Data(acno);
					$("#acnoIsShow").show();
				}
				else{
					$("#acnoIsShow").hide();
				}
			});
		}
		// 取得轉出帳號餘額資料
		function getACNO_Data(acno){
			// 變更轉出帳號就隱藏重置再重新顯示
			$("#acnoIsShow").hide();
			$("#showText").html('');
			
			var options = { keyisval:true ,selectID:'#OUTACN'};
			
			uri = '${__ctx}'+"/NT/ACCT/TRANSFER/getACNO_Data_aj"
			rdata = {acno: acno};
			console.log("getACNO_Data.uri: " + uri);
			console.log("getACNO_Data.rdata: " + rdata);
			
			fstop.getServerDataEx(uri,rdata,false,isShowACNO_Data);
		}
		// 顯示轉出帳號餘額
		function isShowACNO_Data(data){
			var i18n= new Object();
			i18n['available_balance'] = '<spring:message code="LB.Available_balance" />: ';
			console.log("isShowACNO_Data.data: " + JSON.stringify(data));
			
			if(data && data.result){
				// 可用餘額
				console.log("data.data.accno_data: " + data.data.accno_data.ADPIBAL);
				
				// 格式化金額欄位
				i18n['available_balance'] += fstop.formatAmt(data.data.accno_data.ADPIBAL);
				$("#showText").html(i18n['available_balance']);
				$("#acnoIsShow").show();
			}else{
				$("#acnoIsShow").hide();
			}
		}

		// 預約自動輸入明天
		function getTmrDate() {
			// 預約日期欄位顯示明天
			$('#CMDATE_TMP').val("${tmrDate}");
		}
		// 預約日期change事件 ，檢核日期邏輯
		function fgtxdateEvent(){
			 $('input[type=radio][name=FGTXDATE]').change(function() {
		        if (this.value == '2') { // 預約
		            console.log("tomorrow");
		            $("#CMDATE").addClass("validate[required]");
		        }else if (this.value == '1') { // 即時
		            $("#CMDATE").removeClass("validate[required]");
		        }
		    });
		}

		// 西元小日曆click
		function datetimepickerEvent(){
		    $(".CMDATE").click(function(event) {
				$('#CMDATE_TMP').datetimepicker('show');
			});
		    $(".LIMDAT").click(function(event) {
				$('#LIMDAT_TMP').datetimepicker('show');
			});
			jQuery('.datetimepicker').datetimepicker({
				timepicker:false,
				closeOnDateSelect : true,
				scrollMonth : false,
				scrollInput : false,
			 	format:'Y/m/d',
			 	lang: '${transfer}'
			});
		}

		// 通訊錄click
		function addressbookClickEvent() {
			// 通訊錄btn
			$('#getAddressbook').click(function() {
				openAddressbook();
			});
			// 同交易備註btn
			$('#CMMAILMEMO_btn').click(function() {
				$('#CMMAILMEMO').val( $('#CMTRMEMO').val() );
			});
			//範例
			$('#EXAMPLE').click(function() {
				openexample();
			});
		}
		// 開啟通訊錄email選單
		function openAddressbook() {
			window.open('${__ctx}/NT/ACCT/TRANSFER/showAddressbook');
		}
		//開啟範例
		function openexample() {
			window.open('${__ctx}/img/ElecFeeBillSample.jpg');
		}

		function processQuery(){
			var formId = document.getElementById("formId");

			// 檢核電費輸入資料是否符合電費公式
			if (!ChkEleOut(
					GetDateYYYMMDD( $("#LIMDAT").val() ), 
					$("#ELENUM").val(), 
					$("#AMOUNT").val(), 
					$("#CHKNUM").val() )
			){
				// 隱藏錯誤提示訊息span
				$(".hideblock").hide();
				return false;
			}
			
			// 隱藏表單按鈕
			$("#CMTRMAIL").hide();
			$("#CMSUBMIT").hide();
			$("#CMRESET").hide();
			$("#CMMAILMEMO_btn").hide();

			// 通過資料檢核解除表單驗證
			$("#formId").validationEngine('detach');
			
			// 畫面遮罩
			initBlockUI();
			// 送交
			$("#formId").submit();
		}

		/*
	 	 * 檢核繳交電費資料
	 	 * PARAM LIMDAT:7碼代收截止日(YYYMMDD)
	 	 * PARAM ELENUM:電號
	 	 * PARAM TXN_AMOUNT:應繳總金額
	 	 * PARAM CHKNUM:查核碼
	 	 * return
		 */
		function ChkEleOut(LIMDAT, ELENUM, TXN_AMOUNT, CHKNUM) {

			var date = LIMDAT;
		 	var elenum = ELENUM;
		 	var amount = TXN_AMOUNT;

		 	var chknum = CHKNUM + "";
		 	
		 	var chknum1;
		 	var chknum2;
		 	chknum1 = chknum.substring(0, 1);
		 	chknum2 = chknum.substring(1, 3);

		 	var zero = "000000000";
		 	if (amount.length < 9){
		 		amount = zero.substring(0, 9 - amount.length) + amount;
		 	}
		 	var s = date + elenum + chknum2 + amount;

		 	var A = 0, B = 0;
		 	var tot = 0;
		 	var chk;
		 	var ch = new Array(s.length);
		 	var ch_now = "";
		 	for (var i = 0; i < ch.length; i++) {// 奇數位*2
		 		if ((i % 2) == 0) {
		 			ch[i] = s.charAt(i) * 2;
		 			if (ch[i] > 9) {
		 				A = ch[i].toString().charAt(0);
		 				B = ch[i].toString().charAt(1);
		 				ch[i] = eval(A) + eval(B);
		 			}
		 		} else {
		 			ch[i] = s.charAt(i);
				}
		 		tot = tot + eval(ch[i]);
		 	}

		 	chk = tot % 10;

		 	if (chk == chknum1) {
		 		return true;
		 	} else {
		 		//alert("<spring:message code= "LB.Alert169" />");
		 		errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert169' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
		 		return false;
		 	}
		}
	 	 
		// 表單重置
		function formReset() {
			$("#acnoIsShow").hide();
			$(".formError").remove();
			$('form').find('*').filter(':input:visible:first').focus();
			$('#formId').trigger("reset");
		}
		
		// 回上一頁填入資料
		function refillData() {
			var isBack = '${isBack}';
			console.log('isBack: ' + isBack);
			var jsondata = '${previousdata}';
			console.log('jsondata: ' + jsondata);
			
			if ('Y' == isBack && jsondata != null) {
				JSON.parse(jsondata, function(key, value) {
					if(key) {
						var obj = $("input[name='"+key+"']");
						var type = obj.attr('type');
						
						console.log('--------------------------------');
						console.log('type: ' + type);
						console.log('key: ' + key);
						console.log('value: ' + value);
						console.log('--------------------------------');
						
						if(type == 'text'){
							obj.val(value);
						}
						
						if(type == 'hidden'){
							obj.val(value);
						}

						if(type == 'radio'){
							obj.filter('[value='+value+']').prop('checked', true);
						}
						
						if(type == 'checkbox'){
							obj.prop('checked', true);
						}
						
						// SELECT例外處理--約定轉出帳號
						if(key == 'OUTACN_PD'){
							$("#OUTACN_PD option").filter(function() {
								return $(this).val() == value;
							}).attr('selected', true);
						}
						
						// 即時或預約標籤切換
						if(key == 'FGTXDATE' && value == '2'){
							$("#nav-trans-future").click();
						}
					}
				});
				
				// TOKEN值要換一個，不可用上一次的
				$("#TOKEN").val('${sessionScope.transfer_confirm_token}');
			}
		}
		
	</script>
</head>
<body>
	<!-- 交易機制元件所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
    <!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 臺灣電力公司     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0425" /></li>
		</ol>
	</nav>

	
	<!-- 功能清單及登入資訊 -->
	<div class="content row">
	
		<!-- 功能清單 -->
		<%@ include file="../index/menu.jsp"%>

		<!-- 快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
				<!-- 功能內容 -->
				<form method="post" id="formId" action="${__ctx}/PAY/EXPENSE/electricity_fee_confirm">
					<input type="hidden" id="TOKEN" name="TOKEN" value="${sessionScope.transfer_confirm_token}" /><!-- 防止重複交易 -->
					<input type="hidden" name="FLAG" value="1"><!-- 約定\非約定 -->
					
					<!-- 功能名稱 -->
					<h2><spring:message code="LB.W0425" /></h2>
					<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
					
					<!-- 交易流程階段 -->
					<div id="step-bar">
						<ul>
							<li class="active"><spring:message code="LB.Enter_data" /></li>
							<li class=""><spring:message code="LB.Confirm_data" /></li>
							<li class=""><spring:message code="LB.Transaction_complete" /></li>
						</ul>
					</div>
					
					<!-- 功能內容 -->
					<div class="main-content-block row radius-50">
						<!-- 即時、預約導引標籤 -->
						<nav style="width: 100%;">
							<div class="nav nav-tabs" id="nav-tab" role="tablist">
								<a class="nav-item nav-link active" id="nav-trans-now" data-toggle="tab" href="#nav-trans-now" 
									role="tab" aria-controls="nav-home" aria-selected="false">
									<spring:message code="LB.Immediately" />
								</a> 
								<a class="nav-item nav-link" id="nav-trans-future" data-toggle="tab" href="#nav-trans-future" 
									role="tab" aria-controls="nav-profile" aria-selected="true" >
									<spring:message code="LB.Booking" />
								</a>
							</div>
						</nav>
						
						<!-- 交易流程階段 -->
						<div class="col-12 tab-content" id="nav-tabContent">
							<div class="tab-pane fade " id="nav-trans-future" role="tabpanel"
								aria-labelledby="nav-profile-tab"></div>

							<!-- 預約才顯示轉帳日期 -->
							<div class="ttb-input-block tab-pane fade show active"
								id="nav-trans-now" role="tabpanel" aria-labelledby="nav-home-tab">
								<div class="ttb-message">
									<span></span>
								</div>
								
								<!-- 即時的屬性(不須顯示預設隱藏) -->
								<div class="ttb-input" style="display: none;">
									<label class="radio-block">
										<spring:message code="LB.Immediately" />
										<input type="radio" name="FGTXDATE" value="1" checked /> 
										<span class="ttb-radio"></span>
									</label>
								</div>
								
								<!-- 轉帳日期 -->
								<div id="transfer-date" class="ttb-input-item row" style="display: none;">
									<span class="input-title">
										<label><h4><spring:message code="LB.Transfer_date" /></h4></label>
									</span>
									<span class="input-block">
										<!-- 預約的屬性(必須顯示) -->
										<div class="ttb-input ">
											<label class="radio-block">
												<spring:message code="LB.Booking" /> 
												<input type="radio" name="FGTXDATE" value="2" /> 
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!-- 預約日期 -->
										<div class="ttb-input">
											<input type="text" id="CMDATE_TMP" name="CMDATE_TMP" value="" size="10"
												maxlength="10" class="text-input datetimepicker" />
											<span class="input-unit CMDATE">
												<img src="${__ctx}/img/icon-7.svg" />
											</span>
											<!-- 為了讓提示訊息對齊用，會在初始化時先隱藏 -->
											<span class="hideblock">
												<!-- 驗證用的input -->
												<input id="CMDATE" name="CMDATE" type="text" class="text-input" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
									</span>
								</div>
								
								<!-- 轉出帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label><h4><spring:message code="LB.Payers_account_no" /></h4></label>
									</span>
									<span class="input-block">
										<!-- 約定 轉出帳號 -->
										<div class="ttb-input">
											<label class="radio-block"> 
												<input type="radio" id="TransferType_01" name="TransferType" value="PD" checked>
													<spring:message code="LB.Designated_account" />
												<span class="ttb-radio"></span>
											</label>
										</div> 
										<div class="ttb-input ">
											<select name="OUTACN_PD" id="OUTACN_PD" class="custom-select select-input half-input">
												<option value="">----<spring:message code="LB.Select_account" />-----</option>
											</select>
											<!-- 約定 轉出帳號 餘額 -->
											<div id="acnoIsShow">
												<span id="showText" class="input-unit"></span>
											</div>
										</div>
										
										<!-- 非約定 轉出帳號 -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Out_nondesignated_account" /> 
												<input type="radio" id="TransferType_02" name="TransferType" value="NPD" onclick="listReaders();" />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<input type="text" name="OUTACN_NPD" id="OUTACN_NPD" class="text-input" readonly="readonly" value="" />
											<span class="input-subtitle subtitle-color">
												<spring:message code="LB.X2361" />
											</span>
											<!-- 為了讓提示訊息對齊用，會在初始化時先隱藏；只會顯示提示訊息不會顯示欄位 -->
											<span class="hideblock">
												<!-- 轉出帳號 -->
												<input id="OUTACN" name="OUTACN" type="text" class="text-input" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
									</span>
								</div>
								
								<!-- 代收截止日 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label><h4><spring:message code="LB.W0434" /></h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<!-- 選擇代收截止日期 -->
											<input type="text" id="LIMDAT_TMP" name="LIMDAT_TMP" value="" size="10" maxlength="10"
												placeholder="<spring:message code="LB.W0435" />" class="text-input datetimepicker" />
											<span class="input-unit LIMDAT">
												<img src="${__ctx}/img/icon-7.svg" />
											</span>
											<!-- 為了讓提示訊息對齊用，會在初始化時先隱藏；只會顯示提示訊息不會顯示欄位 -->
											<span class="hideblock">
												<!-- 代收截止日期 -->
												<input id="LIMDAT" name="LIMDAT" type="text"
													class="text-input validate[required, verification_date[LIMDAT], funcCall[validate_CheckDate['<spring:message code= "LB.W0434" />',LIMDAT,${str_Today}]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
									</span>
								</div>
								
								<!-- 電號 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label><h4><spring:message code="LB.W0436" /></h4></label></span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="ELENUM" name="ELENUM" size="11" maxlength="11" value=""
												class="text-input validate[required, funcCall[validate_CheckNumWithDigit['<spring:message code= "LB.W0436" />',ELENUM,11]]]" />
											<span class="input-unit"><spring:message code="LB.W0437" /></span>
											
											<button type="button" class="btn-flat-orange" name="EXAMPLE" id="EXAMPLE">
												<spring:message code="LB.W0438" />											
											</button>
										</div>
									</span>
								</div>
								
								<!-- 查核碼 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label><h4><spring:message code="LB.W0439" /></h4></label></span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="CHKNUM" name="CHKNUM" size="3" maxlength="3" value=""
												class="text-input validate[required, funcCall[validate_CheckNumWithDigit['<spring:message code= "LB.W0439" />',CHKNUM,3]]]" />
											<span class="input-unit"><spring:message code="LB.W0440" /></span>
										</div>
									</span>
								</div>
								
								<!-- 應繳總金額 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label><h4><spring:message code="LB.W0441" /></h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="AMOUNT_TMP" name="AMOUNT_TMP" class="text-input" size="8" maxlength="8" value="" /> 
											<span class="input-unit"><spring:message code="LB.Dollar" /></span>
										</div>
										<!-- 為了讓提示訊息對齊用，會在初始化時先隱藏；只會顯示提示訊息不會顯示欄位 -->
										<span class="hideblock">
											<!-- 應繳總金額 -->
											<input id="AMOUNT" name="AMOUNT" type="text" readonly="readonly" 
												class="text-input validate[required,min[1],max[9999999999],funcCall[validate_CheckAmount['<spring:message code= "LB.The_total_amount_payable" />',AMOUNT]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</span>
									</span>
								</div>
								
								<!-- 交易備註 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label><h4><spring:message code="LB.Transfer_note" /></h4></label></span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="CMTRMEMO" name="CMTRMEMO" class="text-input" size="56" maxlength="20" value="" />
											<span class="input-remarks"><spring:message code="LB.Transfer_note_note" /></span>
										</div>
									</span>
								</div>
	
								<!-- 轉出成功Email通知 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4><spring:message code="LB.X0479" /></h4>
										</label>
									</span>
									<span class="input-block">
									
								<!-- 寄信通知 -->
								<c:choose>
								    <c:when test="${sendMe} && !${sessionScope.dpmyemail}.equals('')">
										    
								        <!-- 通知本人 -->
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color"><spring:message code="LB.Notify_me" />：</span>
											<span class="input-subtitle subtitle-color">${sessionScope.dpmyemail}</span>
										</div>
										
										<!-- 另通知 -->
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color"><spring:message code="LB.Another_notice" />：</span>
										</div>
												
								    </c:when>
						    		<c:otherwise>
										    
								        <!-- 通訊錄 -->
										<div class="ttb-input">
											<input type="text" id="CMTRMAIL" name="CMTRMAIL" placeholder="<spring:message code="LB.Email" />"
												class="text-input validate[funcCall[validate_EmailCheck[CMTRMAIL]]]" /> 
											<span class="input-unit"></span>
											<!-- window open 去查 TxnAddressBook 參數帶入身分證字號 -->
											<button type="button" class="btn-flat-orange" id="getAddressbook"><spring:message code="LB.Address_book" /></button>
										</div>
												
								    </c:otherwise>
								</c:choose>
										
										<!-- 摘要內容 -->
										<div class="ttb-input">
											<input type="text" id="CMMAILMEMO" name="CMMAILMEMO" class="text-input"
												 placeholder="<spring:message code="LB.Summary" />" value="" />
											<span class="input-unit"></span>
											<button type="button" class="btn-flat-orange" name="CMMAILMEMO_btn" id="CMMAILMEMO_btn">
												<spring:message code="LB.As_transfer_note" />
											</button>
										</div>
									</span>
								</div>
							</div>
							
							<!-- 重新輸入、確認按鈕 -->
							<input id="CMRESET" type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" onclick="formReset();" />
							<input id="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
						
						</div>
					</div>
					
					<!-- 說明 -->
					<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /></p>
						<li><spring:message code="LB.Electricity_Fee_P1_D1-1" /><a href="${__ctx}/CUSTOMER/SERVICE/rate_table" target="_blank"><spring:message code="LB.Electricity_Fee_P1_D1-2" /></a> </li>
						<li><spring:message code="LB.Electricity_Fee_P1_D2" /> </li>
						<li><spring:message code="LB.Electricity_Fee_P1_D3" /> </li>
						<li><spring:message code="LB.Electricity_Fee_P1_D4" /></li>
						<li><spring:message code="LB.Electricity_Fee_P1_D5" /> </li>
						<li><spring:message code="LB.Electricity_Fee_P1_D6" /> </li>
						<li><spring:message code="LB.Electricity_Fee_P1_D7" /> </li>
<!-- 							<li>如欲取消預約，請於轉帳日之前1日辦理。交易密碼(SSL)預約交易，請以交易密碼(SSL)取消交易；晶片金融卡預約交易，請以晶片金融卡取消交易；電子簽章(i-key)預約交易，請以電子簽章(i-key)取消預約。</li> -->
			        </ol>
					
				</form>
			</section>
		</main>
	</div>

	<%@ include file="../index/footer.jsp"%>

</body>
</html>
