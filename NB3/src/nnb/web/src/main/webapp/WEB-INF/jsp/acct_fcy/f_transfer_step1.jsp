<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<!-- 讀卡機所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/cardReaderMethod.js"></script>
	
	<script type="text/JavaScript">
		var i18nValue = {};
		i18nValue['LB.Select'] = '<spring:message code="LB.Select"/>'; //請選擇
		i18nValue['available_balance'] = '<spring:message code="LB.Available_balance" />: ';
	
		$(document).ready(function() {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 檢查有沒有約定轉出轉入帳號
			setTimeout("checkAcount()", 50);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		
		
		function checkAcount(){
			var custaccSize = $("#CUSTACC option").length;
			var acnoSize = $("#INACNO1 option").length;
			
			// 沒有約定轉出帳號
			if(custaccSize==1){
		   		errorBlock(
					null,
					null,
					['<spring:message code= "LB.X2271" />'], 
					'<spring:message code= "LB.X0956" />', 
					null
				);
		   		$("#errorBtn1").click(function(){
					fstop.getPage('${pageContext.request.contextPath}'+'/INDEX/index','', '');
				});
	   	    	return false;
			}
			
			// 沒有約定轉入帳號
			if(acnoSize==1){
				errorBlock(
					null,
					null,
					['<spring:message code= "LB.X2270" />'], 
					'<spring:message code= "LB.X0956" />', 
					null
				);
				$("#errorBtn1").click(function(){
					fstop.getPage('${pageContext.request.contextPath}'+'/INDEX/index','', '');
				});
				return false;
			}
			
		}
		

	    function init(){
			// 判斷是否可以進行即時交易
			isFxBizTime();
		    
	    	// 即時、預約標籤 click
	    	tabEvent();
	    	// 預約自動輸入明天
	    	getTmrDate();
	    	// 預約日期 change
	    	fgtrdateEvent();
	    	// 西元年日期輸入欄位 click
	    	datetimepickerEvent();
	    	// 通訊錄 click
			addressbookClickEvent();
			//acn
			var getacn ="";
			var getacn1 = '${f_transfer_step1.data.urlID}';
			var getacn2 = '${f_transfer_step1_r.data.urlID}';
			if(getacn1 != null && getacn1 != ''){
				getacn = getacn1;
			}
			else{
				getacn = getacn2;
			}
			if(getacn != null && getacn != ''){
				$("#CUSTACC option[value= '"+ getacn +"' ]").prop("selected", true);
			}
	    	
			// 取得約定/常用非約定轉入帳號
			creatInACNO();
			// 設定轉入帳號勾選項目表單驗證
			changeFginAcno();
			
			// 初始化後隱藏span( 表單驗證提示訊息用的 )
			$(".hideblock").hide();
			// 表單驗證
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
			// submit觸發事件
			finish();
	    }

	 	// 判斷是否可以進行即時交易
		function isFxBizTime(){
			var isFxBizTime = '${f_transfer_step1.data.ISFXBIZTIME}';
			console.log("isFxBizTime: " + isFxBizTime);

			// 不可進行即時交易
			if(isFxBizTime=='N'){
				// 顯示預約狀態的欄位
				$("#transfer-date").show();
				fstop.setRadioChecked("FGTRDATE", "1", true);

				// 切換成預約，即時標籤不可使用
				$("#nav-trans-now").removeClass("active");
				$("#nav-trans-future").addClass("active");
				$("#nav-trans-now").css("pointer-events", "none");
				$("#nav-trans-now").css("display", "inline-block");
			}
		}

		
	 	// 確認按鈕點擊觸發submit
		function finish(){
			$("#CMSUBMIT").click(function (e) {
				// 去掉舊的錯誤提示訊息
				$(".formError").remove();
				// 顯示驗證隱藏欄位( 為了讓提示訊息對齊 )
				$(".hideblock").show();
				
				console.log("submit~~");
				
				// 塞值進隱藏span內的input
				$("#CMTRDATE").val($("#CMTRDATE_TMP").val());
				$("#CMSDATE").val($("#CMSDATE_TMP").val());
				$("#CMEDATE").val($("#CMEDATE_TMP").val());
				$("#INACNO2").val($("#INACNO2_TMP").val());

				// 表單驗證--Start
				var main = document.getElementById("formId");
				
			   	// 表單驗證--預約單日
				if (main.FGTRDATE[1].checked) {
					var sMinDate = '${str_SystemDate}';
					var sMaxDate = '${sNextYearDay}';

					console.log("processQuery.sMinDate: " + sMinDate);
					console.log("processQuery.sMaxDate: " + sMaxDate);

					// 驗證是否是可預約單日之日期
					$("#CMTRDATE").addClass("validate[required ,funcCall[validate_CheckDate[<spring:message code= "LB.X1449" />,CMTRDATE,"+sMinDate+","+sMaxDate+"]]]");
			   		
			   	} // 非預約單日
				else {
				   	// 取消驗證是否是可預約單日之日期
			   		$("#CMTRDATE").removeClass("validate[required ,funcCall[validate_CheckDate[<spring:message code= "LB.X1449" />,CMTRDATE,"+sMinDate+","+sMaxDate+"]]]");
				}


			   	// 表單驗證--預約週期
				if (main.FGTRDATE[2].checked) {
					// 驗證週期日
					$("#CMPERIOD").addClass("validate[required ,funcCall[validate_CheckSelect['<spring:message code= "LB.X1445" />', CMPERIOD, '']]]");
					
					// 驗證日期區間
					var sMinDate = '${str_SystemDate}';
					var sMaxDate = '${sNextYearDay}';
					$("#CMSDATE").addClass("validate[required ,funcCall[validate_CheckDate[<spring:message code= "LB.X1449" />, CMSDATE," + sMinDate + "," + sMaxDate+"]]]");
					$("#CMEDATE").addClass("validate[required ,funcCall[validate_CheckDate[<spring:message code= "LB.X1449" />, CMEDATE," + sMinDate + "," + sMaxDate+"]]]");
					
				} // 非預約週期
				else {
					// 取消驗證週期日期、區間
			   		$("#CMPERIOD").removeClass("validate[required ,funcCall[validate_CheckSelect['<spring:message code= "LB.X1445" />', CMPERIOD, '']]]");
			   		$("#CMSDATE").removeClass("validate[required ,funcCall[validate_CheckDate[<spring:message code= "LB.X1449" />, CMSDATE," + sMinDate + "," + sMaxDate+"]]]");
					$("#CMEDATE").removeClass("validate[required ,funcCall[validate_CheckDate[<spring:message code= "LB.X1449" />, CMEDATE," + sMinDate + "," + sMaxDate+"]]]");
				}

				if($("#FGTRDATE3").prop('checked')){
					$("#validate_CMEDATE2").show();
				}else{
					$("#validate_CMEDATE2").hide();
				}
				
				// 表單驗證
				if ( !$('#formId').validationEngine('validate') ) {
					e = e || window.event; // for IE
					e.preventDefault();
					
				} else {
					// 資料檢核，過了則送出表單
					processQuery();
				}
					
			});
	 	}
	 	
		// 轉入帳號類別change 事件
		function changeFginAcno() {
			$('input[type=radio][name=FGINACNO]').change(function () {
				console.log(this.value);
				if (this.value == 'CMDAGREE') {
					$("#INACNO1").addClass("validate[required]");
					$("#INACNO2").removeClass("validate[required]");
				} else if (this.value == 'CMDISAGREE') {
					$("#INACNO1").removeClass("validate[required]");
					$("#INACNO2").addClass("validate[required]");
				}
			});
		}
	 	
	 	// 表單驗證
	 	function chkQuery(){
			var main = document.getElementById("formId");
			
			// 轉入帳號未選擇類型
			if (!main.FGINACNO[0].checked && !main.FGINACNO[1].checked) {
		   		errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert167' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
				return false;   
 			}
		   	       	    		
		    var outacc = main.CUSTACC.options[main.CUSTACC.selectedIndex].value;
		    var inacc = main.BENACC.value;
		    
		    // 表單驗證--OBU轉DBU之轉入幣別不可為新台幣
		   	if (((outacc.substring(0 ,3) == '893') && (inacc.substring(0 ,3) != '893'))
		   	    	&& (main.REMITCY.options[main.REMITCY.selectedIndex].value == 'TWD')) {
		   		//alert("<spring:message code= "LB.Alert016" />");
		   		errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert016' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
		   	    return false;      		   	       		   	
		   	}
		   	
		   	// 表單驗證--轉出及轉入幣別不可同時為新台幣
		   	if ((main.REMITCY.options[main.REMITCY.selectedIndex].value == main.PAYCCY.options[main.PAYCCY.selectedIndex].value) 
		   	    	&& (main.PAYCCY.options[main.PAYCCY.selectedIndex].value == 'TWD')) {
		   		//alert("<spring:message code= "LB.Alert017" />");
		   		errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert017' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
		   	    return false;      		   	       		   	
		   	}

		   	// 舊驗證CheckFxAmount==CheckFxAmount1故新驗證只用validate_CheckFxAmount
//			   	if((main.REMITCY.options[main.REMITCY.selectedIndex].value != main.PAYCCY.options[main.PAYCCY.selectedIndex].value) 
//					   	&& (main.REMITCY.options[main.REMITCY.selectedIndex].value == 'TWD')) {
//			   		$("#CURAMT").removeClass("validate[required, funcCall[validate_CheckFxAmount['轉帳金額', CURAMT, false, FXTRCURRENCY, PAYREMIT]]]");
//			   		$("#CURAMT").addClass("validate[required, funcCall[validate_CheckFxAmount1['轉帳金額', CURAMT, false, FXTRCURRENCY, PAYREMIT]]]");
//			   	} else {
//			   		$("#CURAMT").removeClass("validate[required, funcCall[validate_CheckFxAmount1['轉帳金額', CURAMT, false, FXTRCURRENCY, PAYREMIT]]]");
//			   		$("#CURAMT").addClass("validate[required, funcCall[validate_CheckFxAmount['轉帳金額', CURAMT, false, FXTRCURRENCY, PAYREMIT]]]");
//			   	}
		   	    		   	
		   	// 表單驗證--匯款分類編號
		   	if (main.SRCFUNDDESC.value == "" || main.SRCFUNDDESC.value == "<spring:message code= "LB.Please_select_code" />") {
		   		//alert("<spring:message code= "LB.Alert146" />");
		   		errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert146' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
		   		return false;
			}
			
			// 表單驗證--驗證手續費
		   	if ((main.COMMACC.options[main.COMMACC.selectedIndex].value != '')	
				   	|| (main.COMMCCY.options[main.COMMCCY.selectedIndex].value != '')) {
		   	    
		   	    if (main.PAYCCY.options[main.PAYCCY.selectedIndex].value != 'TWD') {
		   	    	if ((main.COMMCCY.options[main.COMMCCY.selectedIndex].value != 'TWD') &&
		   	    	    (main.CUSTACC.options[main.CUSTACC.selectedIndex].value != main.COMMACC.options[main.COMMACC.selectedIndex].value)) {
						//alert("<spring:message code= "LB.Alert018" />");
						errorBlock(
								null, 
								null,
								["<spring:message code= 'LB.Alert018' />"], 
								'<spring:message code= "LB.Quit" />', 
								null
							);
				   		return false;      	    
		   	    	}
		   	    	
		   	    } else {
		   	    	if (main.CUSTACC.options[main.CUSTACC.selectedIndex].value != main.COMMACC.options[main.COMMACC.selectedIndex].value) {
						//alert("<spring:message code= "LB.Alert018" />");
						errorBlock(
								null, 
								null,
								["<spring:message code= 'LB.Alert018' />"], 
								'<spring:message code= "LB.Quit" />', 
								null
							);
				   		return false;  				
					}	
		   	    
		   	    	if (main.COMMCCY.options[main.COMMCCY.selectedIndex].value != 'TWD') {
						//alert("<spring:message code= "LB.Alert009" />");
						errorBlock(
								null, 
								null,
								["<spring:message code= 'LB.Alert009' />"], 
								'<spring:message code= "LB.Quit" />', 
								null
							);
				   		return false;  				
					}	
		   	    }   	     	    

		   	    // 表單驗證--手續費幣別
		   	    if ((main.COMMACC.options[main.COMMACC.selectedIndex].value != '')	&& 
		   	    		(main.COMMCCY.options[main.COMMCCY.selectedIndex].value == '')) {
		   	    	//alert("<spring:message code= "LB.Alert010" />");
		   	    	errorBlock(
								null, 
								null,
								["<spring:message code= 'LB.Alert010' />"], 
								'<spring:message code= "LB.Quit" />', 
								null
							);
			   		return false;
				}
		   	}     
		   	
		   	// 表單驗證--轉出與轉入帳號相同時限作幣別轉換
		   	if ((main.REMITCY.options[main.REMITCY.selectedIndex].value == main.PAYCCY.options[main.PAYCCY.selectedIndex].value)
					&& (outacc == inacc) ) {
				//alert("<spring:message code= "LB.Alert019" />");
				errorBlock(
								null, 
								null,
								["<spring:message code= 'LB.Alert019' />"], 
								'<spring:message code= "LB.Quit" />', 
								null
							);
				return false;  				   	
		   	}

		   	// 表單驗證--已超過人民幣限額2萬元
			var uidCheck = '${uidCheck}'; 
			//修正 Client Potential Code Injection
			if(uidCheck=='true' && main.FXTRCURRENCY.value=='CNY' && parseInt(main.CURAMT.value)>20000 && (main.PAYCCY.value!=main.REMITCY.value)) {
				//alert("<spring:message code= "LB.Alert020" />");
				errorBlock(
								null, 
								null,
								["<spring:message code= 'LB.Alert020' />"], 
								'<spring:message code= "LB.Quit" />', 
								null
							);
				return false;
			}
			
			// 表單驗證--收款人附言限制欄位長度不可超過指定byte
			if(!checkMEMO('MEMO1', 10)) {
				return false;
			}
			
			return true;
	 	}

		// 資料檢核，過了則送出表單
		function processQuery() {
			var main = document.getElementById("formId");

			// 表單驗證
			if(!chkQuery()){
				// 隱藏錯誤提示訊息span
				$(".hideblock").hide();
				return false;
			};
			transferConfirm();
		}

		var isconfirm = false;
		$("#errorBtn1").click(function(){
			if(!isconfirm)
				return false;
			isconfirm = false;
			var main = document.getElementById("formId");
			main.ADTXAMT.value = main.CURAMT.value; 
			
			main.COMMACC.disabled = false;
			main.COMMCCY.disabled = false;	   		    	   
		    main.CMSUBMIT.disabled = true;  
			main.BENTYPE.disabled = false;
			main.SRCFUNDDESC.disabled = false;
			        
		    //將各欄位值處理成下頁呈現所需之格式
		    prepareNextPageData();
		    
			var payCcy = main.PAYCCY.value;
			var remitCy = main.REMITCY.value;

			// form.action
			if ((payCcy == remitCy) && (main.FGTRDATE[0].checked)) {
				// 同幣別之即時轉帳交易
				var action = "${__ctx}" + "/FCY/TRANSFER/f_transfer_t_confirm";
				$("#formId").attr("action", action);

			} else if ((payCcy != remitCy) && (main.FGTRDATE[0].checked)) {
				// 不同幣別之即時轉帳交易
				var action = "${__ctx}" + "/FCY/TRANSFER/f_transfer_r_step2";
				$("#formId").attr("action", action);

			} else {
				// 預約交易
				var action = "${__ctx}" + "/FCY/TRANSFER/f_transfer_s_confirm";
				$("#formId").attr("action", action);
			}

			main.submit();

			return false;
		});
		$("#errorBtn2").click(function(){
			isconfirm = false;
			$('#error-block').hide();
		});
		function transferConfirm(){
			var main = document.getElementById("formId");
			isconfirm = true;
			if (main.FGTRDATE[0].checked) {
// 				if(!confirm("<spring:message code= "LB.Confirm003" />"))
// 					return false;
				errorBlock(
						null, 
						null,
						['<spring:message code='LB.Confirm003' />'], 
						'<spring:message code= "LB.Confirm" />', 
						'<spring:message code= "LB.Cancel" />'
					);
			} else if (main.FGTRDATE[1].checked) {
// 				if(!confirm("<spring:message code= "LB.Confirm004" />"+main.CMTRDATE.value+"\n<spring:message code= "LB.Confirm005" />")) {
// 			    	return false;
// 			    }
				errorBlock(
						null, 
						null,
						["<spring:message code='LB.Confirm004' />"+main.CMTRDATE.value,"<spring:message code='LB.Confirm005' />"], 
						'<spring:message code= "LB.Confirm" />', 
						'<spring:message code= "LB.Cancel" />'
					);
			} else if (main.FGTRDATE[2].checked){
// 				if (!confirm("<spring:message code= "LB.Confirm006" />" + main.CMSDATE.value + "~" + main.CMEDATE.value 
// 			    			+ "\n<spring:message code= "LB.Confirm007" />" + main.CMPERIOD.value + "<spring:message code= "LB.Confirm008-1" />\n<spring:message code= "LB.Confirm008-2" />"))
// 			    	return false;
				errorBlock(
						null, 
						null,
						["<spring:message code='LB.Confirm006' />",main.CMSDATE.value+"~"+main.CMEDATE.value,"<spring:message code='LB.Confirm007' />"+main.CMPERIOD.value+"<spring:message code= "LB.Confirm008-1" />","<spring:message code= "LB.Confirm008-2" />"], 
						'<spring:message code= "LB.Confirm" />', 
						'<spring:message code= "LB.Cancel" />'
					);
			    }
				return true;
		}
		function prepareNextPageData() {
			var main = document.getElementById("formId");

			var obj;

			// 即時   	   
			if (main.FGTRDATE[0].checked) {
				main.PAYDATE.value = "${str_CurrentSystemDate}";
			}
			//預約
			else if (main.FGTRDATE[1].checked) {
				main.PAYDATE.value = main.CMTRDATE.value;
			}
			//預約周期
			else if (main.FGTRDATE[2].checked) {
				main.PAYDATE.value = '<spring:message code= "LB.X1457" />' + main.CMSDATE.value + '~' + main.CMEDATE.value 
						+ '<spring:message code= "LB.X1458" />' + main.CMPERIOD.value + '<spring:message code= "LB.X1459" />';
			}

			// 20210716-外匯轉帳-開放非約定轉入帳號、交易機制加上晶片金融卡
			// 約定轉入帳號
			if (main.FGINACNO[0].checked) {
				//修正 Client Potential Code Injection
				var acno_obj = $("#INACNO1").find(":selected").val();
				main.BENACC.value = JSON.parse(acno_obj)["ACN"];
 			}
			// 非約定轉入帳號   	
 			else if (main.FGINACNO[1].checked) {
 				$("#INACNO2").val($("#INACNO2_TMP").val());
 				main.BENACC.value = main.INACNO2.value;
 			}

			main.display_PAYCCY.value = main.PAYCCY.options[main.PAYCCY.selectedIndex].text;
			main.display_REMITCY.value = main.REMITCY.options[main.REMITCY.selectedIndex].text;
			main.display_COMMCCY.value = main.COMMCCY.options[main.COMMCCY.selectedIndex].text;
		}

		// 即時、預約標籤切換事件
		function tabEvent() {
			// 即時
			$("#nav-trans-now").click(function() {
				$("#transfer-date").hide();
				fstop.setRadioChecked("FGTRDATE", "0", true);
			})
			// 預約
			$("#nav-trans-future").click(function() {
				$("#transfer-date").show();
				fstop.setRadioChecked("FGTRDATE", "1", true);
			})
		}

		// 預約自動輸入明天
		function getTmrDate() {
			// 預約日期欄位顯示明天
			$('#CMTRDATE_TMP').val("${tmrDate}");
			$('#CMSDATE_TMP').val("${tmrDate}");
			$('#CMEDATE_TMP').val("${tmrDate}");
			$('#odate').val("${str_SystemDate}");
		}
		// 預約日期change事件 ，檢核日期邏輯
		function fgtrdateEvent() {
			$('input[type=radio][name=FGTRDATE]').change(function() {
				if (this.value == '1') { // 預約
					console.log("tomorrow");
					$("#CMTRDATE").addClass("validate[required]");

					$("#CMSDATE").removeClass("validate[required]");
					$("#CMEDATE").removeClass("validate[required]");

				} else if (this.value == '0') { // 即時
					$("#CMTRDATE").removeClass("validate[required]");
					$("#CMSDATE").removeClass("validate[required]");
					$("#CMEDATE").removeClass("validate[required]");

				} else if (this.value == '2') { // 預約 固定每月的
					$("#CMSDATE").addClass("validate[required]");
					$("#CMEDATE").addClass("validate[required]");

					$("#CMTRDATE").removeClass("validate[required]");
				}
			});
		}

		// 西元小日曆click
		function datetimepickerEvent() {
			$(".CMTRDATE").click(function(event) {
				$('#CMTRDATE_TMP').datetimepicker('show');
			});
			$(".CMSDATE").click(function(event) {
				$('#CMSDATE_TMP').datetimepicker('show');
			});
			$(".CMEDATE").click(function(event) {
				$('#CMEDATE_TMP').datetimepicker('show');
			});

			jQuery('.datetimepicker').datetimepicker({
				timepicker : false,
				closeOnDateSelect : true,
				scrollMonth : false,
				scrollInput : false,
				format : 'Y/m/d',
				lang: '${transfer}'
			});
		}

		// 通訊錄click
		function addressbookClickEvent() {
			// 通訊錄btn
			$('#getAddressbook').click(function() {
				openAddressbook();
			});
			// 同交易備註btn
			$('#CMMAILMEMO_btn').click(function() {
				$('#CMMAILMEMO').val($('#CMTRMEMO').val());
			});
		}
		// 開啟通訊錄email選單
		function openAddressbook() {
			window.open('${__ctx}/NT/ACCT/TRANSFER/showAddressbook');
		}

		// 轉出帳號onChange
		function onChangeCustacc() {
			uri = '${__ctx}' + "/FCY/TRANSFER/currency_ajax";

			// 若是請選擇就不發Ajax問幣別
			if ($("#CUSTACC").val() != "") {
				rdata = {
					ACN : $("#CUSTACC").val()
				};

				console.log("creatOutAcn.uri: " + uri);
				console.log("creatOutAcn.rdata: " + rdata);

				data = fstop.getServerDataEx(uri, rdata, false,
						onChangeCustaccCallback);
			}

		}
		// 轉出帳號onChange後打Ajax的callback
		function onChangeCustaccCallback(data) {
			console.log("data: " + data);
			if (data) {
				// currency_ajax回傳資料型態: List<Map<String, String>>
				console.log("onChangeCustaccCallback.data.json: "
						+ JSON.stringify(data));

				// 先清空原有之"OPTION"內容,並加上一個 "---請選擇---" 欄位
				$('#PAYCCY').empty();
				$("#PAYCCY").append(
						$("<option></option>").attr("value", "").text("---<spring:message code= "LB.Select" />---"));

				// 迴圈塞轉出幣別
				data.forEach(function(map) {
					console.log(map);
					$("#PAYCCY").append(
							$("<option></option>").attr("value", map.ADCURRENCY).text( map.ADCURRENCY + " " + map.ADCCYNAME));
				});
			}
		}

		// 轉入帳號onChange
		function onChangeINACNO1() {
			uri = '${__ctx}' + "/FCY/TRANSFER/currency_ajax";
			$("#FGSVACNO_01").prop("checked", true);
			$("#INACNO2_TMP").val("");
			$("#INACNO2").removeClass("validate[required]");
			// 若是請選擇就不發Ajax問幣別
			if ($("#INACNO1").val() != "" && $("#INACNO1").val().indexOf("#") == -1) {
				var acn = JSON.parse($("#INACNO1").val())["ACN"];
				console.log("onChangeINACNO1.ACN: " + acn);
				
				rdata = {
					ACN : acn
				};

				console.log("creatOutAcn.uri: " + uri);
				console.log("creatOutAcn.rdata: " + rdata);

				data = fstop.getServerDataEx(uri, rdata, false,
						onChangeINACNO1Callback);
			}

		}
		// 轉入帳號onChange後打Ajax的callback
		function onChangeINACNO1Callback(data) {
			console.log("data: " + data);
			if (data) {
				// currency_ajax回傳資料型態: List<Map<String, String>>
				console.log("onChangeINACNO1Callback.data.json: "
						+ JSON.stringify(data));

				// 先清空原有之"OPTION"內容,並加上一個 "---請選擇---" 欄位
				$('#REMITCY').empty();
				$("#REMITCY").append($("<option></option>").attr("value", "").text("---<spring:message code= "LB.Select" />---"));

				// 迴圈塞轉出幣別
				data.forEach(function(map) {
					console.log(map);
					$("#REMITCY").append(
							$("<option></option>").attr("value", map.ADCURRENCY).text( map.ADCURRENCY + " " + map.ADCCYNAME) );
				});
			}

		}

		// 顯示轉帳金額欄之幣別
		function fillTRCRY(obj) {
			var main = document.getElementById("formId");

			var curr = obj.options[obj.selectedIndex].value;
			var payccy = main.PAYCCY.options[main.PAYCCY.selectedIndex].value;
			var remitccy = main.REMITCY.options[main.REMITCY.selectedIndex].value;

			if (curr == '') {
				return;
			} else if (curr == '1') {
				main.FXTRCURRENCY.value = payccy;
			} else if (curr == '2') {
				main.FXTRCURRENCY.value = remitccy;
			}
		}

		function ChangeDisplay(flag) {
			var main = document.getElementById("formId");

			var oINPCSTROW = document.getElementById("INPCSTROW");
			if (oINPCSTROW != undefined) {
				oINPCSTROW.style.display = flag == 'Y' ? "" : "none";
			}
		}

		// 匯款分類編號
		function openMenu() {
			var main = document.getElementById("formId");

			var payccy = main.PAYCCY.options[main.PAYCCY.selectedIndex].value;
			var remitccy = main.REMITCY.options[main.REMITCY.selectedIndex].value;
			var outacc = main.CUSTACC.options[main.CUSTACC.selectedIndex].value;
			var inacc = "";
			var type;

			// 約定轉入帳號
			if (main.FGINACNO[0].checked) {
				var obj = document.getElementById("INACNO1");

				if (obj.options[obj.selectedIndex].value != '' && obj.options[obj.selectedIndex].value.indexOf('#') ==-1 ) {
					//修正 Client Potential Code Injection
					var acno_obj = $("#INACNO1").find(":selected").val();
					inacc = JSON.parse(acno_obj)["ACN"];
				} else {
					inacc = "";
				}
 			}
			// 非約定轉入帳號
 			else if (main.FGINACNO[1].checked) {
 				$("#INACNO2").val($("#INACNO2_TMP").val());
 				inacc = main.INACNO2.value;
 			}

			if ((outacc == '') || (inacc == '') || (payccy == '')
					|| (remitccy == '')) {
				//alert("<spring:message code= "LB.Alert021" />");
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert021' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
				return false;
			}

			// {D-D}
			if ((outacc.substring(0, 3) != '893')
					&& (inacc.substring(0, 3) != '893')) {
				if (((payccy != '') && (payccy != 'TWD'))
						&& (remitccy == 'TWD')) {
					type = '2';
				} else {
					type = '1';
				}
			}
			// {O-D}
			else if ((outacc.substring(0, 3) == '893')
					&& (inacc.substring(0, 3) != '893')) {
				type = '2';
			}
			// {D-O,O-O}	
			else {
				type = '1';
			}

			console.log("openMenu.FIXEDFLAG: " + main.FIXEDFLAG.value);
			console.log("openMenu.NULFLAG: " + main.NULFLAG.value);
			console.log("openMenu.ADRMTTYPE: " + type);
			console.log("openMenu.CUTTYPE: " + '${f_transfer_step1.data.CUSTYPE}');
			
			if (main.FIXEDFLAG.value == 'Y') {
				window.open('${__ctx}/FCY/TRANSFER/fxremitquery_fix');
			} else {
				window.open('${__ctx}/FCY/COMMON/FxRemitQuery?' + 'NULFLAG=' + main.NULFLAG.value
						+ '&ADRMTTYPE=' + type + '&CUTTYPE=${f_transfer_step1.data.CUSTYPE}');
			}
		}

		// 開啟常用匯款分類
		function openMyRemitMenu() {
			console.log("openMyRemitMenu...");
			window.open('${__ctx}/FCY/COMMON/MyRemitMenu');
			
		}

		// 一般網銀分行聯絡方式資料檔擷取
		function displayBhContact() {
			var bhid = $("#CUSTACC").val().substring(0, 3);
			console.log("displayBhContact_bhid: " + bhid);

			rdata = {
				bhid : bhid
			};

			uri = '${__ctx}/FCY/COMMON/getBhContactResult';
			console.log("displayBhContact_uri: " + uri);

			fstop.getServerDataEx(uri, rdata, true, bc_callback);

		}
		function bc_callback(data) {
			if (data) {
				// login_aj回傳資料
				console.log("bc_callback.data.json: " + JSON.stringify(data));
				var adcontacttel = data.data.REC[0].adcontacttel;
				console.log("bc_callback.adcontacttel: " + adcontacttel);
				document.getElementById("ADCONTACTTEL").innerHTML = adcontacttel;
			}
		}

		//顯示轉帳金額欄之幣別 & 判斷匯款分類 & 判斷身分別
		function fillTRCRYandSRC(obj) {
			var main = document.getElementById("formId");

			var curr = obj.options[obj.selectedIndex].value;
			var payccy = main.PAYCCY.options[main.PAYCCY.selectedIndex].value;
			var remitccy = main.REMITCY.options[main.REMITCY.selectedIndex].value;

			if (curr == '') {
				//return;
			} else if (curr == '1') {
				main.FXTRCURRENCY.value = payccy;
			} else if (curr == '2') {
				main.FXTRCURRENCY.value = remitccy;
			}

			var outacc = main.CUSTACC.options[main.CUSTACC.selectedIndex].value;
			var inacc = '';

			// 約定轉入帳號   	
			if (main.FGINACNO[0].checked) {
				var inacno1 = document.getElementById("INACNO1");
				if (inacno1.options[inacno1.selectedIndex].value.indexOf("#") != -1 ) {
					return;
				}

				//修正 Client Potential Code Injection
				var acno_obj = $("#INACNO1").find(":selected").val();
				inacc = JSON.parse(acno_obj)["ACN"];
				
 			} // 非約定轉入帳號   	
			else if (main.FGINACNO[1].checked) {
				$("#INACNO2").val($("#INACNO2_TMP").val());
 				inacc = main.INACNO2.value;
 			}

			main.BENACC.value = inacc;
			main.BENTYPE.disabled = false;
			main.FXQREMITNO.disabled = false;
			$("#FXQREMITNO").addClass("btn-flat-orange");
			main.FXQREMITNO2.disabled = false;
			$("#FXQREMITNO2").addClass("btn-flat-orange");

			// {D-D, O-O}
			if (((outacc.substring(0, 3) != '893') && (inacc.substring(0, 3) != '893'))
					|| ((outacc.substring(0, 3) == '893') && (inacc.substring(0, 3) == '893'))) {

				main.COMMACC.disabled = true;
				main.COMMCCY.disabled = true;

				main.SRCFUND.value = '';
				main.SRCFUNDDESC.value = '';
				document.getElementById('SHOW_SRCFUNDDESC').innerHTML = '';
				main.FIXEDFLAG.value = 'N';
				main.NULFLAG.value = 'N';

				if (payccy == remitccy) { //f-f
					//SPC
					main.FIXEDFLAG.value = 'Y';
					main.FXQREMITNO2.disabled = true;
					$("#FXQREMITNO2").removeClass("btn-flat-orange");
				} else {
					if ((payccy != 'TWD') && (remitccy != 'TWD')) {

						if (inacc == outacc) {
							main.SRCFUND.value = '694';
							main.SRCFUNDDESC.value = '694';
							document.getElementById('SHOW_SRCFUNDDESC').innerHTML = '694';

							main.FXQREMITNO.disabled = true;
							$("#FXQREMITNO").removeClass("btn-flat-orange");
							main.FXQREMITNO2.disabled = true;
							$("#FXQREMITNO2").removeClass("btn-flat-orange");
						} else {
							//SPC
							main.FIXEDFLAG.value = 'Y';
							main.FXQREMITNO2.disabled = true;
							$("#FXQREMITNO2").removeClass("btn-flat-orange");
						}
					} else {
						//NUL
						main.NULFLAG.value = 'Y';
					}
				}

				//清除帶出之手續費帳號  	
				for (var i = 0; i < main.COMMACC.length; i++) {
					if (main.COMMACC.options[i].value == '') {
						main.COMMACC.options[i].selected = true;

						break;
					}
				} //end for i  	 

				//清除帶出之手續費幣別  	
				for (var i = 0; i < main.COMMCCY.length; i++) {
					if (main.COMMCCY.options[i].value == '') {
						main.COMMCCY.options[i].selected = true;

						break;
					}
				} //end for i  	 

				//自行轉帳時顯示 "收款人附言" 欄
				ChangeDisplay('Y');
				main.SELFFLAG.value = 'Y';

			} else {
				//{D-O, O-D}

				main.COMMACC.disabled = false;
				main.COMMCCY.disabled = false;

				main.NULFLAG.value = 'N';
				main.FIXEDFLAG.value = 'N';

				//自動帶出手續費帳號  	
				for (var i = 0; i < main.COMMACC.length; i++) {
					if (main.COMMACC.options[i].value == outacc) {
						main.COMMACC.options[i].selected = true;

						break;
					}
				} //end for i  	   	  

				//處理手續費幣別	  	 	
				processCOMMCCY(main.PAYCCY);

				//他行轉帳時顯示 "收款人附言" 欄
				ChangeDisplay('Y');
				main.SELFFLAG.value = 'Y';

			}// end if {D-O, O-D}

			// {D-D}
			if ((outacc.substring(0, 3) != '893')
					&& (inacc.substring(0, 3) != '893')) {
				//收款人身分別 
				$("#BENTYPE").append($("<option></option>").attr( {"value": "6" , selected :"selected"}  ).text("<spring:message code= "LB.Otherwise" />"));
				$('#BENTYPE').attr('disabled','disabled');
				
			} else {
				// 產生"收款人身分別"之下拉式選單，先清空原有之"OPTION"內容,並加上一個 "---請選擇---" 欄位
				$('#BENTYPE').empty();
				$("#BENTYPE").append($("<option></option>").attr("value", "").text("---<spring:message code= "LB.Select" />---"));
				$("#BENTYPE").append($("<option></option>").attr("value", "1").text("<spring:message code= "LB.Government" />"));
				$("#BENTYPE").append($("<option></option>").attr("value", "2").text("<spring:message code= "LB.Public_enterprise" />"));
				$("#BENTYPE").append($("<option></option>").attr("value", "3").text("<spring:message code= "LB.Private_enterprise" />"));
				$("#BENTYPE").append($("<option></option>").attr("value", "4").text("<spring:message code= "LB.Other_Account" />"));
				$("#BENTYPE").append($("<option></option>").attr("value", "5").text("<spring:message code= "LB.My_Account" />"));

			} //end else  	
		}

		function processCOMMCCY(obj) {
			var main = document.getElementById("formId");
			var payccy = obj.options[obj.selectedIndex].value;
			var payccyText = obj.options[obj.selectedIndex].text;
			var comacc = main.COMMACC.options[main.COMMACC.selectedIndex].value;

			// 產生"手續費幣別"之下拉式選單，先清空原有之"OPTION"內容,並加上一個 "---請選擇---" 欄位	
			$('#COMMCCY').empty();
			$("#COMMCCY").append($("<option></option>").attr("value", "").text("---<spring:message code= "LB.Select" />---"));
			$("#COMMCCY").append($("<option></option>").attr( {"value": payccy , selected :"selected"}  ).text(payccyText));

			if ((comacc.substring(3, 4) != '5') && (payccy != 'TWD')) {
				$("#COMMCCY").append($("<option></option>").attr("value", "TWD").text("TWD <spring:message code= "LB.NTD" />"));
			}
		}

		// 取得轉出帳號幣別餘額資料
		function getACNO_Data() {
			// 變更轉出帳號就隱藏重置再重新顯示
			$("#acnoIsShow").hide();
			$("#showText").html('');
			
			uri = '${__ctx}' + "/FCY/COMMON/getACNO_Currency_Data_aj"
			console.log("getACNO_Data URL >>" + uri);
			
			//轉出帳號
			var acno = $("#CUSTACC").find(":selected").val();
			console.log("getACNO_Data.acno: " + acno);
			
			//轉出幣別			
			var cry = $("#PAYCCY").find(":selected").val();
			console.log("getACNO_Data.cry: " + cry);
			
			// 轉出帳號、轉出幣別皆有值才執行查詢餘額
			if(acno&&cry){
				console.log("getACNO_Data_ajax...");
				rdata = {acno: acno,cry: cry};
				console.log("rdata>>" + rdata);
				fstop.getServerDataEx(uri, rdata, true, isShowACNO_Data);
			}
		}
		// 顯示轉出帳號幣別餘額資料
		function isShowACNO_Data(data) {
			console.log("isShowACNO_Data.data >>> {}", data );
			if (data && data.result) {
				var bal = "0.00";
				if(data.data.accno_data) {
					bal = fstop.formatAmt(data.data.accno_data.AVAILABLE); //格式化金額
				}
				console.log("bal :" + bal);
				// 格式化金額欄位
				var showBal = i18nValue['available_balance'] + bal;
				
				// 重新顯示餘額
				$("#showText").html(showBal);
				$("#acnoIsShow").show();
				
			} else {
				$("#acnoIsShow").hide();
			}
		}
		
		// 表單重置
		function formReset() {
			$("#acnoIsShow").hide();
			$(".formError").remove();
			$('form').find('*').filter(':input:visible:first').focus();
			$('#formId').trigger("reset");
		}
		
		// 收款人附言限制欄位長度不可超過指定byte
		function checkMEMO(id, byteLength)
		{
			var oINPCST = document.getElementById(id);
			var sValue = oINPCST.value;
			var rePattern = /[^\x00-\xff]/ig;
			var oArr = sValue.match(rePattern); 
			var valueLength = oArr == null ? sValue.length : sValue.length + oArr.length; 

			while (valueLength > byteLength)
			{
			    sValue = sValue.substring(0, sValue.length - 1);
			    oArr = sValue.match(rePattern); 
			    valueLength = oArr == null ? sValue.length : sValue.length + oArr.length;
			}
			
			// 收款人附言，不可超過5個中文字。
			if (sValue != oINPCST.value)
		    {
				errorBlock(null,null,["<spring:message code= "LB.X2228" />"],"<spring:message code= "LB.Confirm" />",null);
				$("#"+id).focus();
				return false;
		    }
			return true;
		}
		
//		建立約定及常用轉入帳號下拉選單
		function creatInACNO(){
			data = null;
			uri = '${__ctx}'+"/FCY/TRANSFER/getInAcno_aj"
			rdata = {type: 'FX_TRANSFER' };
			options = { keyisval:false , selectID:'#INACNO1'}
			data = fstop.getServerDataEx(uri,rdata,false);
			
			console.log("creatInACNO.data: " + JSON.stringify(data));
			if(data !=null && data.data !=null && data.result == true && data.data.REC !=null){
				fstop.creatSelect(data.data.REC,options);
				$("#INACNO1").children().each(
					function(){
						if($(this).val()=="#1" || $(this).val()=="#2"){
							$(this).css("background-color","orange");
							$(this).css("font-weight","bold");
						}
					}
				);
			}else{
				//alert('<spring:message code= "LB.Alert005" />');
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert005' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			}
		}
		
		//點擊約定帳號radio 清空非約定欄位 
		function radio_FGSVACNO_01_click(){
			$("#INACNO2_TMP").val("");
		}
		
		//點擊非約定帳號radio trigger點文字框 約定帳號下拉選單回到請選擇 
		function radio_FGSVACNO_02_click(){
			$("#FGSVACNO_02").prop("checked", true);
			$('#INACNO2_TMP').focus();
			$("#INACNO1").val("#0");
		}
		
		//點擊非約定帳號 TEXT 非約定radio check 約定帳號下拉選單回到請選擇
		function text_INACNO2_TMP_click(){
			$("#FGSVACNO_02").prop("checked", true);
			$("#INACNO1").val("#0");
			$("#INACNO2").addClass("validate[required]");
		}
		
	</script>
</head>
<body>
	<!-- 交易機制元件所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	
	
    <!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 買賣外幣/約定轉帳     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.FX_Exchange_Transfer" /></li>
		</ol>
	</nav>



	
	<!-- 功能清單及登入資訊 -->
	<div class="content row">
	
		<!-- 功能清單 -->
		<%@ include file="../index/menu.jsp"%>

		<!-- 快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
				<!-- 功能內容 -->
				<form method="post" name="formId" id="formId" action="${__ctx}/FCY/TRANSFER/f_transfer_t_confirm">
					<input type="hidden" id="PAYDATE" name="PAYDATE" value="">
					<input type="hidden" id="BENACC" name="BENACC" value="">
					<input type="hidden" id="SRCFUND" name="SRCFUND" value="">
					<input type="hidden" id="NULFLAG" name="NULFLAG" value="N">
					<input type="hidden" id="FIXEDFLAG" name="FIXEDFLAG" value="N">
					<input type="hidden" id="CUSTYPE" name="CUSTYPE" value="${f_transfer_step1.data.CUSTYPE}">
					<input type="hidden" id="display_PAYCCY" name="display_PAYCCY" value="">
					<input type="hidden" id="display_REMITCY" name="display_REMITCY" value="">
					<input type="hidden" id="display_COMMCCY" name="display_COMMCCY" value="">
					<input type="hidden" id="NAME" name="NAME" value="${f_transfer_step1.data.NAME}">
					<input type="hidden" id="SELFFLAG" name="SELFFLAG" value="">
					<input type="hidden" id="ADTXAMT" name="ADTXAMT" value="">
					<input type="hidden" id="TOKEN" name="TOKEN" value="${sessionScope.transfer_confirm_token}" /><!-- 防止重複交易 -->
					
					<!-- 功能名稱 -->
					<h2><spring:message code="LB.FX_Exchange_Transfer" /></h2>
					<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
					
					<!-- 交易流程階段 -->
<!-- 					<div id="step-bar"> -->
<!-- 						<ul> -->
<%-- 							<li class="active"><spring:message code="LB.Enter_data" /></li> --%>
<%-- 							<li class=""><spring:message code="LB.Confirm_data" /></li> --%>
<%-- 							<li class=""><spring:message code="LB.Transaction_complete" /></li> --%>
<!-- 						</ul> -->
<!-- 					</div> -->
					
					<!-- 功能內容 -->
					<div class="main-content-block row radius-50">
						<!-- 即時、預約導引標籤 -->
						<nav style="width: 100%;">
							<div class="nav nav-tabs" id="nav-tab" role="tablist">
								<a class="nav-item nav-link active" id="nav-trans-now" data-toggle="tab" href="#nav-trans-now" 
									role="tab" aria-controls="nav-home" aria-selected="false" >
									<spring:message code="LB.Immediately" />
								</a> 
								<a class="nav-item nav-link" id="nav-trans-future" data-toggle="tab" href="#nav-trans-future" 
									role="tab" aria-controls="nav-profile" aria-selected="true" >
									<spring:message code="LB.Booking" />
								</a>
							</div>
						</nav>
						
						<!-- 交易流程階段 -->
						<div class="col-12 tab-content" id="nav-tabContent">
							<div class="tab-pane fade " id="nav-trans-future" role="tabpanel"
								aria-labelledby="nav-profile-tab"></div>

							<!-- 預約才顯示轉帳日期 -->
							<div class="ttb-input-block tab-pane fade show active"
								id="nav-trans-now" role="tabpanel" aria-labelledby="nav-home-tab">
								<div class="ttb-message">
									<span></span>
								</div>
								
								<!-- 即時的屬性(不須顯示預設隱藏) -->
								<div class="ttb-input" style="display: none;">
									<label class="radio-block">
										<spring:message code="LB.Immediately" />
										<input type="radio" name="FGTRDATE" value="0" checked /> 
										<span class="ttb-radio"></span>
									</label>
								</div>
								
								<!-- 轉帳日期 -->
								<div id="transfer-date" class="ttb-input-item row" style="display: none;">
									<span class="input-title">
										<label><h4><spring:message code="LB.Transfer_date" /></h4></label>
									</span>
									<span class="input-block">
										<!-- 預約的屬性(必須顯示) -->
										<div class="ttb-input ">
											<label class="radio-block">
												<spring:message code="LB.Booking" /> 
												<b><span class="text-danger"><spring:message code="LB.W0256" /></span></b>
												<input type="radio" name="FGTRDATE" value="1" /> 
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!-- 預約日期 -->
										<div class="ttb-input">
											<input type="text" id="CMTRDATE_TMP" value="" size="10"
												maxlength="10" class="text-input datetimepicker" />
											<span class="input-unit CMTRDATE">
												<img src="${__ctx}/img/icon-7.svg" />
											</span>
											<!-- 為了讓提示訊息對齊用，會在初始化時先隱藏 -->
											<span class="hideblock">
												<!-- 驗證用的input -->
												<input id="CMTRDATE" name="CMTRDATE" type="text" class="text-input" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>

										<!-- 預約 固定每月的 -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Booking" />
												<br />
												<input type="radio" name="FGTRDATE" id="FGTRDATE3" value="2">
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!-- 日 -->
										<div class="ttb-input">
											<select id="CMPERIOD" name="CMPERIOD" class="custom-select select-input half-input ">
												<option value=""><spring:message code="LB.Select" /></option>
												<c:forEach var="day" begin="1" end="31"> 
											         <option value="${day}">${day}</option> 	
												</c:forEach>
											</select>
											<span class="input-unit">
												<spring:message code="LB.Day" />
											</span>
										</div>
										<!-- 期間起日 -->
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color">
												<spring:message code="LB.Period_start_date" /></span>
											<input type="text" id="CMSDATE_TMP" name="CMSDATE_TMP" class="text-input datetimepicker" value="" />
											<span class="input-unit CMSDATE">
												<img src="${__ctx}/img/icon-7.svg" />
											</span>
											<!-- 不在畫面上顯示的span -->
											<span class="hideblock">
												<!-- 驗證用的input -->
												<input id="CMSDATE" name="CMSDATE" type="text" class="text-input"
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
										<!-- 迄日 -->
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color">
												<spring:message code="LB.Period_end_date" /></span>
											<input type="text" id="CMEDATE_TMP" name="CMEDATE_TMP" class="text-input datetimepicker" value="" />
											<span class="input-unit CMEDATE">
												<img src="${__ctx}/img/icon-7.svg" />
											</span>
											<!-- 不在畫面上顯示的span -->
											<span class="hideblock">
												<!-- 驗證用的input -->
												<input id="CMEDATE" name="CMEDATE" type="text" class="text-input"
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
										
										<!-- 驗證日期區間用的span預設隱藏 -->
										<span id="hideblock_CheckDateScope" >
										<!-- 驗證用的input -->
										<input id="odate" name="odate" type="text" class="text-input" 
											style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										<input id="validate_CMEDATE2" name="validate_CMEDATE2" type="text" class="text-input validate[funcCallRequired[validate_CheckPerMonthDay['<spring:message code= "LB.X1455" />',CMPERIOD,CMSDATE_TMP,CMEDATE_TMP]]" 
											style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</span>
										
									</span>
								</div>
								
								<!-- 轉出帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Payers_account_no" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<select class="custom-select select-input half-input  validate[required, funcCall[validate_CheckSelect['<spring:message code= "LB.Payers_account_no" />', CUSTACC, '']]]"
													id="CUSTACC" name="CUSTACC" onchange="onChangeCustacc();displayBhContact();getACNO_Data()">
												<option value="">---<spring:message code="LB.Select_account" />---</option>
												<c:forEach var="dataList" items="${f_transfer_step1.data.ACNOLIST}">
													<option value='${dataList }' >${dataList }</option>
												</c:forEach>
											</select>
										</div>
									</span>
								</div>
								
								<!-- 轉出幣別 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Currency_o" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<select class="custom-select select-input half-input  validate[required, funcCall[validate_CheckSelect['<spring:message code= "LB.Currency_o" />', PAYCCY, '']]]" 
													name="PAYCCY" id="PAYCCY" size="1" onchange="fillTRCRYandSRC(formId.PAYREMIT);getACNO_Data()">
												<option value="">---<spring:message code="LB.Select" />--</option>
												<c:forEach var="dataList" items="${f_transfer_step1.data.ADCURRENCYLIST}">
													<option value='${dataList.ADCURRENCY }' >${dataList.ADCURRENCY }&nbsp;${dataList.ADCCYNAME }</option>
												</c:forEach>
											</select>
										</div>
										<div id="acnoIsShow">
											<span id="showText" class="input-unit "></span>
										</div>
									</span>
								</div>
								
								<!-- 轉入帳號 -->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                    <label>
	                                        <h4><spring:message code="LB.Payees_account_no" /></h4>
	                                    </label>
	                                </span>
	                                <span class="input-block">
										<!-- 約定轉入帳號 -->
										<div class="ttb-input">
											<label class="radio-block">
											<spring:message code="LB.Designated_account" />/<spring:message code="LB.Common_non-designated_account" />
											<input type="radio" id="FGSVACNO_01" name="FGINACNO" value="CMDAGREE" onclick="radio_FGSVACNO_01_click()"> 
											<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<select id="INACNO1" name="INACNO1" class="custom-select select-input half-input" onchange="onChangeINACNO1()">
	<%-- 											<option value = ""><spring:message code="LB.Select_designated_or_common_non-designated_account" /></option> --%>
											</select>
										</div>
	                                    <%-- <div class="ttb-input">
	                                        <select class="custom-select select-input half-input " name="INACNO1" id="INACNO1" onchange="onChangeINACNO1()">
												<option value="">--<spring:message code="LB.Select_designated_account" />--</option>
												<c:forEach var="dataList" items="${f_transfer_step1.data.ACNREDLIST}">
													<option value='${dataList.VALUE }' class="red">${dataList.TEXT }</option>
												</c:forEach>
												<c:forEach var="dataList" items="${f_transfer_step1.data.ACNLIST}">
													<option value='${dataList.VALUE }' class="red">${dataList.TEXT }</option>
												</c:forEach>
											</select>
	                                    </div> --%>
	                                    
	                                    <!-- 非約定轉入帳號 -->
	                                    <div class="ttb-input">
											<label class="radio-block">
											非約定帳號
											<input type="radio" id="FGSVACNO_02" name="FGINACNO" value="CMDISAGREE" onclick="radio_FGSVACNO_02_click()"> 
											<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<input type="text" name="INACNO2_TMP" id="INACNO2_TMP" value="" class="text-input" 
												placeholder="<spring:message code="LB.Enter_Account" />" size="11" maxlength="11" autocomplete="off" onclick="text_INACNO2_TMP_click()">
											<label class="check-block"><spring:message code="LB.Join_common_account"  />
											<input type="checkbox" name="ADDACN" id="ADDACN" value="1"> 
											<span class="ttb-check"></span>
										</div>
										<!-- 為了讓提示訊息對齊用，會在初始化時先隱藏；只會顯示提示訊息不會顯示欄位 -->
										<span class="hideblock">
											<!-- 轉出帳號 -->
											<input id="INACNO2" name="INACNO2" type="text" class="text-input" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</span>
	                                </span>
	                            </div>
								
								<!-- 轉入幣別 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Currency_i" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<select class="custom-select select-input half-input  validate[required, funcCall[validate_CheckSelect['<spring:message code= "LB.Currency_i" />', REMITCY, '']]]" 
													name="REMITCY" id="REMITCY" size="1" onchange="fillTRCRYandSRC(formId.PAYREMIT)">
												<option value="">---<spring:message code="LB.Select" />---</option>
												<c:forEach var="dataList" items="${f_transfer_step1.data.ADCURRENCYLIST}">
													<option value='${dataList.ADCURRENCY }' >${dataList.ADCURRENCY }&nbsp;${dataList.ADCCYNAME }</option>
												</c:forEach>
											</select>
										</div>
									</span>
								</div>
								
								<!-- 轉帳金額 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Amount" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<select class="custom-select select-input half-input  validate[required, funcCall[validate_CheckSelect['<spring:message code= "LB.X1460" />', PAYREMIT, '']]]" 
													id="PAYREMIT" name="PAYREMIT" size="1" onchange="fillTRCRY(this)">
												<option value="" selected>--- <spring:message code="LB.Select" /> ---</option>
												<option value="1"><spring:message code="LB.Deducted" /></option>
												<option value="2"><spring:message code="LB.Buy" /></option>
											</select>
										</div>
										<div class="ttb-input">
											<input class="text-input input-width-100" name="FXTRCURRENCY" id="FXTRCURRENCY" value="" disabled />
											<input class="text-input input-width-125 validate[required, min[1], funcCall[validate_CheckFxAmount['<spring:message code= "LB.Amount" />', CURAMT, false, FXTRCURRENCY, PAYREMIT]]]"
												name="CURAMT" id="CURAMT" value="" />
										</div>
									</span>
								</div>
								
								<!-- 匯款分類編號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Remittance_Classification" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<font id="SHOW_SRCFUNDDESC"><spring:message code="LB.Please_select_code" /></font>
											<input class="text-input" id="SRCFUNDDESC" name="SRCFUNDDESC" type="hidden" size="30" value="<spring:message code="LB.Please_select_code" />" disabled />
											<br />
											<button type="button" class="btn-flat-orange" value="<spring:message code="LB.Menu" />" name="FXQREMITNO" id="FXQREMITNO" onclick="openMenu()"><spring:message code="LB.Menu" /></button>
											<button type="button" class="btn-flat-orange" value="<spring:message code="LB.Common_menu" />" name="FXQREMITNO2" id="FXQREMITNO2" onclick="openMyRemitMenu()"><spring:message code="LB.Common_menu" /></button>
											<br />
											<span class="input-unit"><spring:message code="LB.Remittance_Classification_Note" /></span>
											<br />
											<font color="#0000FF">
												<spring:message code="LB.Consulting_line" /> : <font id="ADCONTACTTEL"></font>
											</font>
										</div>
									</span>
								</div>
								
								<!-- 收款人身份別 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Payee_identity" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<select name="BENTYPE" id="BENTYPE" size="1"
													class="custom-select select-input half-input  validate[required, funcCall[validate_CheckSelect['<spring:message code= "LB.Payee_identity" />', BENTYPE, '']]]" >
												<option value="">---<spring:message code="LB.Select" />---</option>
												<option value="1"><spring:message code="LB.Government" /></option>
												<option value="2"><spring:message code="LB.Public_enterprise" /></option>
												<option value="3"><spring:message code="LB.Private_enterprise" /></option>
												<option value="4"><spring:message code="LB.Other_Account" /></option>
												<option value="5"><spring:message code="LB.My_Account" /></option>
												<option value="6"><spring:message code="LB.Otherwise" /></option>
											</select>
										</div>
									</span>
								</div>
								
								<!-- 收款人附言 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0280" /></h4><spring:message code="LB.W0273" />
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<!--  檢查輸入框內容的位元組數(英文1，中文2)是否超過指定的最大值 -->
											<input id="MEMO1" name="MEMO1" type="text" size="10" maxlength="10" class="text-input" onblur="checkMEMO('MEMO1', 10);"/>
											<br />
											<span class="input-unit"><spring:message code="LB.Payees_postscript_Note_1" /></span>
										</div>
									</span>
								</div>
								
								<!-- 手續費帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Service_Charges_Account" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<select class="custom-select select-input half-input " name="COMMACC" onchange="processCOMMCCY(formId.PAYCCY)">
												<option value="">---<spring:message code="LB.Select_account" />---</option>
												<c:forEach var="dataList" items="${f_transfer_step1.data.ACNOLIST}">
													<option value='${dataList }' >${dataList }</option>
												</c:forEach>
											</select>
											<br />
											<span class="input-unit"><spring:message code="LB.Service_Charges_Account_note" /></span>
										</div>
									</span>
								</div>
								
								<!-- 手續費幣別 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Service_Chargess_Currency" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<select class="custom-select select-input half-input " name="COMMCCY" id="COMMCCY" size="1">
												<option value="">---<spring:message code="LB.Select" />---</option>
												<c:forEach var="dataList" items="${f_transfer_step1.data.ADCURRENCYLIST}">
													<option value='${dataList.ADCURRENCY }' >${dataList.ADCURRENCY }&nbsp;${dataList.ADCCYNAME }</option>
												</c:forEach>
											</select>
										</div>
									</span>
								</div>
								
								<!-- 交易備註 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Transfer_note" /></h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="CMTRMEMO" name="CMTRMEMO" class="text-input" size="56" maxlength="20" value="" />
											<span class="input-remarks"><spring:message code="LB.Transfer_note_note" /></span>
										</div>
									</span>
								</div>
								
								<!-- 轉出成功Email通知 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4><spring:message code="LB.X0479" /></h4>
										</label>
									</span>
									<span class="input-block">
									
								<!-- 寄信通知 -->
								<c:choose>
								    <c:when test="${sendMe} && !${sessionScope.dpmyemail}.equals('')">
										    
								        <!-- 通知本人 -->
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color"><spring:message code="LB.Notify_me" />：</span>
											<span class="input-subtitle subtitle-color">${sessionScope.dpmyemail}</span>
										</div>
										
										<!-- 另通知 -->
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color"><spring:message code="LB.Another_notice" />：</span>
										</div>
												
								    </c:when>
						    		<c:otherwise>
										    
								        <!-- 通訊錄 -->
										<div class="ttb-input">
											<input type="text" id="CMTRMAIL" name="CMTRMAIL" placeholder="<spring:message code="LB.Email" />"
												class="text-input validate[funcCall[validate_EmailCheck[CMTRMAIL]]]" />
											<span class="input-unit"></span>
											<!-- window open 去查 TxnAddressBook 參數帶入身分證字號 -->
											<button type="button" class="btn-flat-orange" id="getAddressbook"><spring:message code="LB.Address_book" /></button>
										</div>
												
								    </c:otherwise>
								</c:choose>
										
										<!-- 摘要內容 -->
										<div class="ttb-input">
											<input type="text" id="CMMAILMEMO" name="CMMAILMEMO" class="text-input"
												 placeholder="<spring:message code="LB.Summary" />" value="" />
											<span class="input-unit"></span>
											<button type="button" class="btn-flat-orange" name="CMMAILMEMO_btn" id="CMMAILMEMO_btn">
												<spring:message code="LB.As_transfer_note" />
											</button>
										</div>
									</span>
								</div>

							</div>
							<input id="CMRESET" type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" onclick="formReset();" />
							<input id="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
						</div>
					</div>
					
					<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /></p>
						<li><spring:message code="LB.F_Transfer_P1_D1-1" /><a href="${__ctx}/CUSTOMER/SERVICE/rate_table" target="_blank"><spring:message code="LB.F_Transfer_P1_D1-2" /></a><a href="${__ctx}/public/transferFeeRate.htm" target="_blank"><spring:message code="LB.F_Transfer_P1_D1-3" /></a><a href="https://www.tbb.com.tw/exchange_rate" target="_blank"><spring:message code="LB.F_Transfer_P1_D1-4" /></a></li>
						<li><spring:message code="LB.B0003_1" /><br><spring:message code="LB.B0003_2" /></li>
						<li><spring:message code="LB.F_Transfer_P1_D3" /></li>
						<li><spring:message code="LB.F_Transfer_P1_D4" /></li>
						<li><spring:message code="LB.F_Transfer_P1_D5" /></li>
						<li><spring:message code="LB.F_Transfer_P1_D6" /></li>
						<li><spring:message code="LB.F_Transfer_P1_D7-1" /><span class="text-danger"><spring:message code="LB.F_Transfer_P1_D7-2" /></span>。</li>
						<li><spring:message code="LB.F_Transfer_P1_D8" /></li>
						<li><spring:message code="LB.F_Transfer_P1_D9" /></li>
						<li><spring:message code="LB.F_Transfer_P1_D10" /></li>
						<li><spring:message code="LB.F_Transfer_P1_D11" /></li>
						<li><spring:message code="LB.F_Transfer_P1_D12" /></li>
						<c:if test="${f_transfer_step1.data.isProd eq 3}"><li><span><font color="red"><spring:message code="LB.X2405" /></font></span></li></c:if>
						<!--併行作業期間:倘您已曾經登入新版網路銀行後，前於舊版網銀已預約之轉帳交易，統一將移轉至新版網路銀行執行及查詢;另新、舊版網路銀行執行預約交易，需回原執行之網路銀行查詢，無法於兩系統間相互查詢。 -->
					</ol>
					
				</form>
			</section>
		</main>
	</div>

	<%@ include file="../index/footer.jsp"%>

</body>
</html>
