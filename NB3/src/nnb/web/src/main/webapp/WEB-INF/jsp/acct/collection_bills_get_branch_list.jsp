<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=BIG5">
		<%@ include file="../__import_head_tag.jsp"%>
		<%@ include file="../__import_js_u2.jsp" %>
		<script type="text/javascript">
		
			function fillData(id, name) 
			{
				var oDpbhno = self.opener.document.getElementById("DPBHNO");
				var oDpbhno_str = self.opener.document.getElementById("DPBHNO_str");
				var oDPBHENT = self.opener.document.getElementById("DPBHENT");
					
				if (oDpbhno != undefined)
					oDpbhno.value = id;
				if (oDpbhno_str != undefined)
					oDpbhno_str.value = id + " " + name;
				if (oDPBHENT != undefined)
					oDPBHENT.checked = true;
				
				self.close();
			}
		</script>
	</head>
	<body>
		<main class="col-12"> 
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
				<form method="post" id="formId" action="">
					<!-- 我的通訊錄 -->
					<h2><spring:message code="LB.X1444" /></h2>
					<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
					<div class="main-content-block row">
						<div class="col-12 printClass">
							<br>
						    <table class="table table-striped" data-toggle-column="first">
								<c:forEach var="dataList" items="${collection_bills.data.data.data}" varStatus="status">
									<c:if test="${status.count % 5 == 1}">	
						    			<tr>
						    		</c:if>
										<td>
											<a href="#" onClick="fillData('${dataList.ADBRANCHID}','${dataList.ADBRANCHNAME}');">
												${dataList.ADBRANCHID} ${dataList.ADBRANCHNAME}
											</a>
										</td>
									<c:if test="${status.count % 5 == 0}">	
										</tr>
						    		</c:if>
								</c:forEach>
							</table>
							<br>
						</div>
					</div>
				</form>	
			</section>
		</main> <!-- 		main-content END -->
	</body>
</html>