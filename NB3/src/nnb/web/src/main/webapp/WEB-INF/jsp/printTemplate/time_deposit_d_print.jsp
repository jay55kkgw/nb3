<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
  <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.5.1.min.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
<br/><br/>
<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
<br/><br/><br/>
<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
<br/><br/><br/>
<label><spring:message code="LB.Inquiry_time" />：</label><label>${CMQTIME}</label>
<br/><br/>
<label><spring:message code="LB.Total_records" />：</label><label>${COUNT}　<spring:message code="LB.Rows" /></label>
<br/><br/>
<label><spring:message code="LB.Total_amount" />：</label><label>${TOTAMT}</label>
<br/><br/>
<table align="center" border="1" cellpadding="6" cellspacing="0" bordercolor="#bfbfbf">
	<tr>
		<td style="text-align:center"><spring:message code="LB.Account" /></td>
		<td style="text-align:center"><spring:message code="LB.Deposit_type" /></td>
		<td style="text-align:center"><spring:message code="LB.Certificate_no" /></td>
		<td style="text-align:center"><spring:message code="LB.Certificate_amount" /></td>
		<td style="text-align:center"><spring:message code="LB.Interest_rate1" /></td>
		<td style="text-align:center"><spring:message code="LB.Interest_calculation" /></td>
		<td style="text-align:center"><spring:message code="LB.Start_date" /></td>
		<td style="text-align:center"><spring:message code="LB.Maturity_date" /></td>
		<td style="text-align:center"><spring:message code="LB.Interest_transfer_to_account" /></td>
		<td style="text-align:center"><spring:message code="LB.Automatic_number_of_rotations" /></td>
		<td style="text-align:center"><spring:message code="LB.Automatic_number_of_unrotated" /></td>
		<td style="text-align:center"><spring:message code="LB.Rollover_method" /></td>
		<td style="text-align:center"><spring:message code="LB.Non_Automatic_unrotated_application" /></td>
		<td style="text-align:center"><spring:message code="LB.Note" /></td>
	</tr>
	<c:forEach items="${dataListMap}" var="map">
	<tr>
		<td style="text-align:center">${map.ACN}</td>
		<td style="text-align:center">${map.TYPE}</td>
		<td style="text-align:center">${map.FDPNUM}</td>
		<td style="text-align:right">${map.AMTFDP}</td>
		<td style="text-align:right">${map.ITR}</td>
		<td style="text-align:center">${map.DPINTTYPE}</td>
		<td style="text-align:center">${map.DPISDT}</td>
		<td style="text-align:center">${map.DUEDAT}</td>
		<td style="text-align:center">${map.TSFACN}</td>
		<td style="text-align:center">${map.ILAZLFTM}</td>
		<td style="text-align:center">${map.AUTXFTM}</td>
		<td style="text-align:center">${map.TYPE1}</td>
		<td style="text-align:center">${map.TYPE2}</td>
		<!-- 備註欄用來顯示連結，列印頁不必填 -->
		<td></td>
	</tr>
	</c:forEach>
</table>
<br/><br/>
	<div class="text-left">
		<spring:message code="LB.Description_of_page" /> 
		</br>
		<spring:message code="LB.Time_deposit_P1_Desc" />
		</br>
		<ol class="list-decimal text-left">
			<li><spring:message code="LB.Time_deposit_P1_D1" /></li>
	        <li><spring:message code="LB.Time_deposit_P1_D2" /></li>
	        <li><spring:message code="LB.Time_deposit_P1_D3" /></li>
	        <li><B><spring:message code="LB.Time_deposit_P1_D4" /></B></li>
		</ol>
	</div>
</body>
</html>