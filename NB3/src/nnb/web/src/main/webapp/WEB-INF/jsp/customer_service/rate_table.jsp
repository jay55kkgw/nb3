<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
	<head>
		<%@ include file="../__import_head_tag.jsp"%>
		<%@ include file="../__import_js.jsp" %>
	</head>
	<script type="text/javascript">
		$(document).ready(function(){
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		
		function init(){
			$("a").click(function(){
				if(this.id == "logobtn"){
					window.close();
				}
			});
		}
	</script>
	<body>
		<!-- header -->
		<header>
			<%@ include file="../index/header_logout.jsp"%>
		</header>
		
		<!-- 左邊menu 及登入資訊 -->
		<div class="content row">
			<main class="col-12">
				<section id="main-content" class="container">
					<!-- 主頁內容  -->
					<h2>
					</h2>
					<form id="formId" method="post" action="">
						<div class="main-content-block row">
							<div class="col-12 tab-content">
								<div class="ttb-input-block">
									<c:choose>
										<c:when test="${pageContext.response.locale=='zh_CN'}">
											<p>一、依本行现行安控机制，以统一编号归户，订定已约定及非约定转帐帐号每日交易限额如下：</p>
											<div style="overflow: auto">
												<table class="question-table" style="width: 100%">
													<thead>
														<tr>
															<th rowspan="2">安控机制</th>
															<th colspan="2">约定转账</th>
															<th colspan="3">非约定转账</th>
															<th colspan="3">在线约定转入账号</th>
														</tr>
														<tr>
															<th>自行</th>
															<th>跨行</th>
															<th>自行</th>
															<th>跨行</th>
															<th>合并</th>
															<th>自行</th>
															<th>跨行</th>
															<th>合并</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<th>交易密码</th>
															<td rowspan="5">500 万</td>
															<td rowspan="5">200 万</td>
															<td>-</td>
															<td>-</td>
															<td>-</td>
															<td rowspan="5" colspan="3">单笔5 万<br>每日10 万<br>每月20 万元整。</td>
														</tr>
														<tr>
															<th>芯片金融卡</th>
															<td rowspan="4" colspan="3">单笔5 万<br>每日10 万<br>每月20 万元整。</td>
														</tr>
														<tr>
															<th>动态密码卡</th>
														</tr>
														<tr>
															<th>随护神盾</th>
														</tr>
														<tr>
															<th>台湾Pay<br>(快速交易)</th>
														</tr>
														<tr>
															<th>电子签章</th>
															<td>1,500 万</td>
															<td>500 万(每笔200 万元)</td>
															<td>300 万</td>
															<td>200 万(不含外汇汇出汇款)</td>
															<td>-</td>
															<td>300 万</td>
															<td>200 万</td>
															<td>-</td>
														</tr>
														<tr>
															<th>备注</th>
															<td colspan="8" style="text-align: left;">
																<ol class="list-decimal">
																	<li data-num="1.">上列密码/电子签章/芯片金融卡/动态密码卡/随护神盾之每日转账限额合并计算以2000万元为上限。</li>
																	<li data-num="2.">台外币交易限额合并计算。</li>
																	<li data-num="3.">行动银行交易限额与一般网银合并计算。</li>
																</ol>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
				
											<div class="ttb-result-list terms">
												<p>二、其他交易限额及相关规定：</p>
												<ol>
													<li>（一）数位存款帐户转帐限额：同一客户之「数位存款帐户」合计非约定转帐金额，每笔限新台币5万元整，每日限新台币10万元整，每月限新台币20万元；客户亲临服务分行申请「约定转帐」功能后，则比照贵行一般存款帐户临柜约定转帐之交易限额，依不同安控机制订定之每日交易限额。
													</li>
													<li>（二）转入综存定期约定与非约定帐户两者合计每一转入帐户，当月累计限额100万元。 (自104年9月1日适用)</li>
													<li>（三）「缴费/税」类：
														<ol>
															<li data-num="1.">以「晶片金融卡」、「动态密码卡」及「随护神盾」方式交易者与「密码方式（SSL机制）」约定转帐之跨行转帐交易合并计算限额。
															</li>
															<li data-num="2.">以「电子签章」方式交易者与电子签章非约定转帐之跨行转帐交易合并计算限额。</li>
														</ol>
													</li>
													<li>（四）基金申购：每人每营业日以电子化交易方式（含网路/语音）进行基金交易转出之金额，合计不得逾等值新台币伍佰万元整。
													</li>
													<li>（五）黄金申购：每笔最低交易量为1公克，每日累计买进最高交易数量每一帐户为50,000公克。</li>
													<li>（六）外汇交易：
														<ol>
															<li data-num="1.">外汇转帐/外汇汇出汇款：
																<ol>
																	<li data-num="(1)">同一户名外币与台币互转，外币结购或结售应个别累积，每日累积最高限额应低于新台币50万元，并与电话银行转帐、金融卡提领外币合并计算限额。
																	</li>
																	<li data-num="(2)">未满20岁自然人，同一外汇存款帐户不同币别间户转、不同帐户间汇转帐（含汇出汇款）分别计算，每日累积最高限额应低于新台币50万元。
																	</li>
																	<li data-num="(3)">个人户结购或结售人民币应个别累积，每人每日透过帐户买卖之金额，不得逾人民币二万元。
																	</li>
																	<li data-num="(4)">领有中华民国国民身分证之个人始能申办人民币汇款至大陆地区，且每人每日之限额为人民币八万元。
																	</li>
																</ol> ＊上述(3)(4)项若涉及人民币买卖者，个人户仍应受人民币2万元之限制。
															</li>
															<li data-num="2.">外汇汇入汇款线上解款：
																<ol>
																	<li data-num="(1)">）存入台币帐户应低于新台币50万元。</li>
																	<li data-num="(2)">存入外存帐户无金额限制，惟未满20岁自然人每日应低于等值新台币50万元。
																	</li>
																</ol>
															</li>
														</ol>
													</li>
													<li>（七）转出帐户无交易金额限制项目：
														<ol>
															<li data-num="1.">同一外汇存款帐户不同币别之转帐交易。</li>
															<li data-num="2.">同一外汇综合存款帐户之活期存款与定期存款间转帐交易。</li>
														</ol>
													</li>
												</ol>
											</div>
										</c:when>
										<c:when test="${pageContext.response.locale=='en'}">
											<p>First, According to the current security mechanism of the Bank, the daily transaction amount for the designated and non-predesignated transfer accounts are as follows:</p>
											<div style="overflow: auto">
												<table class="question-table">
													<thead>
														<tr>
															<th rowspan="2">Security mechanism</th>
															<th colspan="2">Designated transfer</th>
															<th colspan="3">Non-predesignated transfer</th>
															<th colspan="3">Online designated transfer to account</th>
														</tr>
														<tr>
															<th>Self bank</th>
															<th>Interbank transfer</th>
															<th>Self bank</th>
															<th>Interbank transfer</th>
															<th>Merge</th>
															<th>Self bank</th>
															<th>Interbank transfer</th>
															<th>Merge</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<th>Transaction password</th>
															<td rowspan="5">5 million</td>
															<td rowspan="5">2 million</td>
															<td>-</td>
															<td>-</td>
															<td>-</td>
															<td rowspan="5" colspan="3">Single amount 50,000<br>100,000 daily<br>200,000 yuan per month.</td>
														</tr>
														<tr>
															<th>Chip financial card</th>
															<td rowspan="4" colspan="3">Single amount 50,000<br>100,000 daily<br>200,000 yuan per month.</td>
														</tr>
														<tr>
															<th>Dynamic password card</th>
														</tr>
														<tr>
															<th>Aegis with care</th>
														</tr>
														<tr>
															<th>Taiwan Pay<br>(Fast Trading)</th>
														</tr>
														<tr>
															<th>Electronic Signature</th>
															<td>15 million</td>
															<td class="white-spacing">5 million(2 million yuan each)</td>
															<td>3 million</td>
															<td class="white-spacing">2 million(Excluding foreign exchange outward remittance)</td>
															<td>-</td>
															<td>3 million</td>
															<td>2 million</td>
															<td>-</td>
														</tr>
														<tr>
															<th>Remarks</th>
															<td colspan="8" class="white-spacing text-left">
																<ol class="list-decimal">
																	<li data-num="1.">The above password / electronic signature / chip financial card / dynamic password card / Aegis shield daily transfer limit calculation combined with 20 million yuan as the upper limit.</li>
																	<li data-num="2.">The foreign currency transaction limits are combined.</li>
																	<li data-num="3.">The mobile banking transaction limit is calculated in conjunction with the general online banking.</li>
																</ol>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
													
											<div class="ttb-result-list terms">
												<p>Second, other transaction limits and related regulations:</p>
												<ol>
													<li>(A) Digital deposit account transfer limit: The totals "digital deposit account" of the same customer the non-contracted transfer amount, each limit is NT$50,000, and the daily limit is NT$100,000, and the monthly limit is NT$ 200,000; after the customer visits the service branch to apply for the "contract transfer" function, it will set a daily transaction limit according to the different security mechanism, according to the transaction limit of the general deposit account of the bank.</li>
													<li>(B) Transferring each of the contracted regular deposits and non-contracted accounts of the comprehensive deposits into each account, and the cumulative limit for the month is 1 million. (Applicable from September 1, 104)</li>
													<li>(C) "Payment/Tax" category:
														<ol>
															<li data-num="1.">The  Inter-bank transfer transaction with the "Financial Card", "Dynamic Password Card" and "Aegis with care" method and the "Password Method (SSL Mechanism)" to calculate the limit of inter bank contracted transfer amount.</li>
															<li data-num="2.">The "electronic signature" method combines the calculation of the inter-bank transfer transaction with the electronic signature and non-contracted transfer.</li>
														</ol>
													</li>
													<li>(D) Fund purchase: The amount of funds transferred by the electronic transaction method (including Internet/voice) per person per business day, the total amount should not exceed the equivalent of NT$5 million.</li>
													<li>(E) Gold purchase: The minimum transaction volume is 1 gram per transaction, and the maximum number of transactions per day is 50,000 grams per account.</li>
													<li>(F) Foreign exchange transactions:
														<ol>
															<li data-num="1.">Foreign exchange transfer / foreign exchange outward remittance:
																<ol>
																	<li data-num="(1)">The foreign currency and Taiwanese currency of the same household name transfer, the foreign currency purchase or settlement shall be accumulated separately. The daily cumulative maximum limit shall be less than NT$500,000, and the calculation shall be made in conjunction with the bank telephone transfer and the financial card.</li>
																	<li data-num="(2)">Natural persons under the age of 20, the same foreign exchange deposit account, the transfer of accounts between different currencies, and the transfer of accounts between different accounts (including outward remittances) are calculated separately. The daily cumulative maximum limit should be less than NT$500,000.</li>
																	<li data-num="(3)">Individuals who purchase or close the RMB should accumulate separately, and the amount of each person's daily purchase through the account must not exceed CNY 20,000.</li>
																	<li data-num="(4)">Individuals with a National Identity Card of the Republic of China can apply for RMB remittances to the mainland, and the daily limit for each person is CNY 80,000.</li>
																</ol>
																* If (3)(4) above refers to RMB traders, individual households should still be subject to a limit of CNY 20,000.
															</li>
															<li data-num="2.">Foreign exchange inward remittance online payment:
																<ol>
																	<li data-num="(1)">The deposit into the Taiwanese currency account should be less than NT$500,000.</li>
																	<li data-num="(2)">There is no limit on the amount of deposits into external accounts, but natural persons under the age of 20 should be lower than the equivalent of NT$500,000 per day.</li>
																</ol>
															</li>
														</ol>
													</li>
													<li>(G) Transferring the account without transaction amount limit items:
														<ol>
															<li data-num="1.">Transfer transactions for different currencies in the same foreign exchange deposit account.</li>
															<li data-num="2.">Transfer transactions between demand deposits and time deposits in the same foreign exchange comprehensive deposit account.</li>											
														</ol>
													</li>
												</ol>
											</div>
										</c:when>
										<c:otherwise>
											<p>一、依本行現行安控機制，以統一編號歸戶，訂定已約定及非約定轉帳帳號每日交易限額如下：</p>
												<div style="overflow: auto">
													<table class="question-table" style="width: 100%">
														<thead>
															<tr>
																<th rowspan="2">安控機制</th>
																<th colspan="2">約定轉帳</th>
																<th colspan="3">非約定轉帳</th>
																<th colspan="3">線上約定轉入帳號</th>
															</tr>
															<tr>
																<th>自行</th>
																<th>跨行</th>
																<th>自行</th>
																<th>跨行</th>
																<th>合併</th>
																<th>自行</th>
																<th>跨行</th>
																<th>合併</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<th>交易密碼</th>
																<td rowspan="5">500 萬</td>
																<td rowspan="5">200 萬</td>
																<td>-</td>
																<td>-</td>
																<td>-</td>
																<td rowspan="5" colspan="3">單筆5 萬<br>每日10 萬<br>每月20 萬元整。</td>
															</tr>
															<tr>
																<th>晶片金融卡</th>
																<td rowspan="4" colspan="3">單筆5 萬<br>每日10 萬<br>每月20 萬元整。</td>
															</tr>
															<tr>
																<th>動態密碼卡</th>
															</tr>
															<tr>
																<th>隨護神盾</th>
															</tr>
															<tr>
																<th>台灣Pay<br>(快速交易)</th>
															</tr>
															<tr>
																<th>電子簽章</th>
																<td>1,500 萬</td>
																<td>500 萬(每筆200 萬元)</td>
																<td>300 萬</td>
																<td>200 萬(不含外匯匯出匯款)</td>
																<td>-</td>
																<td>300 萬</td>
																<td>200 萬</td>
																<td>-</td>
															</tr>
															<tr>
																<th>備註</th>
																<td colspan="8" style="text-align: left;">
																	<ol class="list-decimal">
																		<li data-num="1.">上列密碼/電子簽章/晶片金融卡/動態密碼卡/隨護神盾之每日轉帳限額合併計算以2000萬元為上限。</li>
																		<li data-num="2.">臺外幣交易限額合併計算。</li>
																		<li data-num="3.">行動銀行交易限額與一般網銀合併計算。</li>
																	</ol>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
												<div class="ttb-result-list terms">
													<p>二、其他交易限額及相關規定：</p>
													<ol>
														<li>（一）數位存款帳戶轉帳限額：同一客戶之「數位存款帳戶」合計非約定轉帳金額，每筆限新臺幣5萬元整，每日限新臺幣10萬元整，每月限新臺幣20萬元；客戶親臨服務分行申請「約定轉帳」功能後，則比照貴行一般存款帳戶臨櫃約定轉帳之交易限額，依不同安控機制訂定之每日交易限額。</li>
														<li>（二）轉入綜存定期約定與非約定帳戶兩者合計每一轉入帳戶，當月累計限額100萬元。(自104年9月1日適用)</li>
														<li>（三）「繳費/稅」類：
															<ol>
																<li data-num="1.">以「晶片金融卡」、「動態密碼卡」及「隨護神盾」方式交易者與「密碼方式（SSL機制）」約定轉帳之跨行轉帳交易合併計算限額。</li>
																<li data-num="2.">以「電子簽章」方式交易者與電子簽章非約定轉帳之跨行轉帳交易合併計算限額。</li>
															</ol>
														</li>
														<li>（四）基金申購：每人每營業日以電子化交易方式（含網路/語音）進行基金交易轉出之金額，合計不得逾等值新臺幣伍佰萬元整。</li>
														<li>（五）黃金申購：每筆最低交易量為1公克，每日累計買進最高交易數量每一帳戶為50,000公克。</li>
														<li>（六）外匯交易：
															<ol>
																<li data-num="1.">外匯轉帳/外匯匯出匯款：
																	<ol>
																		<li data-num="(1)">同一戶名外幣與臺幣互轉，外幣結購或結售應個別累積，每日累積最高限額應低於新臺幣50萬元，並與電話銀行轉帳、金融卡提領外幣合併計算限額。</li>
																		<li data-num="(2)">未滿20歲自然人，同一外匯存款帳戶不同幣別間戶轉、不同帳戶間匯轉帳（含匯出匯款）分別計算，每日累積最高限額應低於新臺幣50萬元。</li>
																		<li data-num="(3)">個人戶結購或結售人民幣應個別累積，每人每日透過帳戶買賣之金額，不得逾人民幣二萬元。</li>
																		<li data-num="(4)">領有中華民國國民身分證之個人始能申辦人民幣匯款至大陸地區，且每人每日之限額為人民幣八萬元。</li>
																	</ol>
																	＊上述(3)(4)項若涉及人民幣買賣者，個人戶仍應受人民幣2萬元之限制。
																</li>
																<li data-num="2.">外匯匯入匯款線上解款：
																	<ol>
																		<li data-num="(1)">存入臺幣帳戶應低於新臺幣50萬元。</li>
																		<li data-num="(2)">存入外存帳戶無金額限制，惟未滿20歲自然人每日應低於等值新臺幣50萬元。</li>
																	</ol>
																</li>
															</ol>
														</li>
														<li>（七）轉出帳戶無交易金額限制項目：
															<ol>
																<li data-num="1.">同一外匯存款帳戶不同幣別之轉帳交易。</li>
																<li data-num="2.">同一外匯綜合存款帳戶之活期存款與定期存款間轉帳交易。</li>											
															</ol>
														</li>
													</ol>
												</div>
										</c:otherwise>
									</c:choose>
								</div>
								<input type="button" class="ttb-button btn-flat-orange" id="CLOSE" value="<spring:message code="LB.X0194" />" onclick="window.close();"/>
							</div>
						</div>
					</form>
				</section>
			</main>
		</div>
		<%@ include file="../index/footer.jsp"%>
	</body>
</html>