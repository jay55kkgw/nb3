<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<script type="text/javascript">
	</script>
</head>

<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %> 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 匯入匯款     -->
    		<li class="ttb-breadcrumb-item"><spring:message code="LB.W0125" /></li>
    <!-- 線上解款申請/註銷     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0318" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 主頁內容 -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<spring:message code="LB.W0318" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="">
					<!-- 表單顯示區 -->
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-result-block">
								<div class="ttb-result-block">
									<p>
										<c:choose>
											<c:when test="${f_remittances_apply_result.data.TXTYPE == 'A'}">
												<!-- 申請 -->
												<spring:message code="LB.D0170" />
											</c:when>
											<c:otherwise>
												<!-- 取消 -->
												<spring:message code="LB.Cancel" />
											</c:otherwise>
										</c:choose>
										&nbsp;
										<!--成功 -->
										<spring:message code="LB.D1099" />
										&nbsp;
									</p>
								</div>
								<div class="ttb-result-block">
									<p>
										<!--申請/取消匯入匯款線上解款功能皆於次一營業日生效。 -->
										<spring:message code="LB.F_Remittances_Apply_P1_D2" />
										<!--外匯匯入匯款線上解款申請/註銷 -->
										<spring:message code="LB.W0318" />
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="text-left">
						<!-- 		說明： -->
						<ol class="description-list list-decimal">
							<p>
								<spring:message code="LB.Description_of_page" />
							</p>
							<li>
								<spring:message code="LB.F_Remittances_Apply_P3_D1" />
							</li>
						</ol>
					</div>
				</form>
			</section>
			<!-- main-content END -->
		</main>
	</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>