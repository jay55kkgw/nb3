<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript">
		var i18nValue = {};
		i18nValue['LB.Select'] = '<spring:message code="LB.Select"/>'; //請選擇
		// 初始化
		$(document).ready(function () {
			init();
		});

		function init() {
			//表單驗證
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });

		}
		//選項
		function formReset() {
			//initBlockUI();
			if ($('#actionBar').val() == "excel") {
				$("#downloadType").val("OLDEXCEL");
				$("#templatePath").val("/downloadTemplate/f_remittances_payments.xls");
			} else if ($('#actionBar').val() == "txt") {
				$("#downloadType").val("TXT");
				$("#templatePath").val("/downloadTemplate/f_remittances_payments.txt");
			}
			$("#formId").attr("target", "");
			$("#formId").submit();
			$('#actionBar').val("");
		}
		//交易單據
		function openFxRemitQuery() {
			window.open('${__ctx}/FCY/COMMON/fxcertpreview?TXID=F003&ADTXNO=${f_remittances_payments_result.data.ADTXNO}');
		}

		//列印 <!-- 外匯匯入匯款線上解款 -->
		function print() {
			var params = {
				"jspTemplateName": "f_remittances_payments_result_print",
				"jspTitle": '<spring:message code= "LB.W0317" />',
				"COMMCCY2": '${f_remittances_payments_result.data.COMMCCY2}',
				"str_CommAmt": '${f_remittances_payments_result.data.str_CommAmt}',
				"RATE": '${f_remittances_payments_result.data.RATE}',
				"ORGCCY": '${f_remittances_payments_result.data.ORGCCY}',
				"str_OrgAmt": '${f_remittances_payments_result.data.str_OrgAmt}',
				"PMTCCY": '${f_remittances_payments_result.data.PMTCCY}',
				"str_PmtAmt": '${f_remittances_payments_result.data.str_PmtAmt}',
				"str_PmtAmt": '${f_remittances_payments_result.data.str_PmtAmt}',
				"str_Rate": '${f_remittances_payments_result.data.str_Rate}',
				"BENACC": '${f_remittances_payments_result.data.BENACC}'
			};
			openWindowWithPost("${__ctx}/print",
				"height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",
				params);
		}

	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
  	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %> 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
	<!-- 匯入匯款     -->
    		<li class="ttb-breadcrumb-item"><spring:message code="LB.W0125" /></li>
    <!-- 線上解款     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0317" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!--外匯匯入匯款線上解款 -->
				<h2>
					<spring:message code="LB.W0317" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value="">
							<spring:message code="LB.Downloads" />
						</option>
						<!-- 						下載Excel檔 -->
						<option value="excel">
							<spring:message code="LB.Download_excel_file" />
						</option>
						<!-- 						下載為txt檔 -->
						<option value="txt">
							<spring:message code="LB.Download_txt_file" />
						</option>
					</select>
				</div>
				<form autocomplete="off" method="post" id="formId" action="${__ctx}/download">
					<!-- 下載用 -->
					<input type="hidden" name="downloadFileName" value="<spring:message code="LB.W0317" />" />
					<input type="hidden" name="COMMCCY2" value="${f_remittances_payments_result.data.COMMCCY2}" />
					<input type="hidden" name="str_CommAmt" value="${f_remittances_payments_result.data.str_CommAmt}" />
					<input type="hidden" name="str_Rate" value="${f_remittances_payments_result.data.str_Rate}" />
					<input type="hidden" name="ORGCCY" value="${f_remittances_payments_result.data.str_OrgAmt}" />
					<input type="hidden" name="PMTCCY" value="${f_remittances_payments_result.data.PMTCCY}" />
					<input type="hidden" name="str_PmtAmt" value="${f_remittances_payments_result.data.str_PmtAmt}" />
					<input type="hidden" name="BENACC" value="${f_remittances_payments_result.data.BENACC}" />
					<!-- 下載用 -->
					<input type="hidden" name="downloadType" id="downloadType" />
					<input type="hidden" name="templatePath" id="templatePath" />
					<!-- EXCEL下載用 -->
					<input type="hidden" name="headerRightEnd" value="2" /><!-- headerRightEnd 資料列以前的右方界線 -->
					<input type="hidden" name="headerBottomEnd" value="10" /><!-- headerBottomEnd 資料列到第幾列 從0開始 -->
					<!-- TXT下載用-總行數 -->
					<input type="hidden" name="txtHeaderBottomEnd" value="17" />
					<input type="hidden" name="txtHasRowData" value="false" />
					<input type="hidden" name="txtHasFooter" value="false" />
					<!--電文回應 -->
					<c:set var="BaseResultData" value="${f_remittances_payments_result.data}"></c:set>
					<!-- 表單顯示區 -->
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<div class="ttb-message">
									<span>
										<spring:message code="LB.W0332" />
									</span>
								</div>
								<!-- 手續費-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.D0507" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.COMMCCY2}
												${BaseResultData.str_CommAmt}
											</span>
										</div>
									</span>
								</div>
								<!-- 費用合計-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.W0334" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.COMMCCY2}
												${BaseResultData.str_CommAmt}
											</span>
										</div>
									</span>
								</div>
								<!-- 匯率-->
								<c:if test="${BaseResultData.SameCurrency == 'N'}">
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Exchange_rate" />
												</h4>
											</label>
										</span>
										<span class="input-block">
											<div class="ttb-input">
												<span>
													${BaseResultData.str_Rate }
												</span>
											</div>
										</span>
									</div>
								</c:if>
								<!--匯款金額-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.W0150" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.ORGCCY}
												${BaseResultData.str_OrgAmt}
												<span class="ttb-unit">
													<spring:message code="LB.Dollar" /></span>

											</span>
										</div>
									</span>
								</div>
								<!--解款金額-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.W0330" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>

												${BaseResultData.PMTCCY}
												${BaseResultData.str_PmtAmt}
												<span class="ttb-unit">
													<spring:message code="LB.Dollar" /></span>
											</span>
										</div>
									</span>
								</div>
								<!--解款帳號-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.W0329" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.BENACC}
											</span>
										</div>
									</span>
								</div>
							</div>

								<!-- 列印 -->
								<input class="ttb-button btn-flat-orange" TYPE="button" id="printbtn" id="printbtn" onclick="print()" value="<spring:message code="LB.Print" />" />
								<!-- 交易單據 -->
								<input class="ttb-button btn-flat-orange" TYPE="button" id="printbtnT" name="printbtnT" onclick="openFxRemitQuery()" value="<spring:message code="LB.Transaction_document" />" />

						</div>
					</div>
					<div class="text-left">
						<ol class="description-list list-decimal">
							<!-- 		說明： -->
							<p>
								<spring:message code="LB.Description_of_page" />
							</p>
							<li>
								<spring:message code="LB.F_Remittances_Payments_P5_D1" />
							</li>
						</ol>
					</div>
				</form>
			</section>
			<!-- main-content END -->
		</main>
	</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>