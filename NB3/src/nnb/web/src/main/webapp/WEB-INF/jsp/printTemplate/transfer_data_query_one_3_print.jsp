<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function(){
			window.print();
		});
	</script>
</head>

<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br/><br/>
	<div style="text-align:center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br/><br/><br/>
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<br/><br/><br/>
	
	<table class="print">
		<tr>
			<!-- 交易日期 -->
			<th><spring:message code="LB.Transaction_date" /></th>
			<th>${TRADEDATE_F }</th>
		</tr>
		<tr>
			<!-- 入帳日期 -->
			<th><spring:message code="LB.The_account_credited_date" /></th>
			<th>${DATE_F }</th>
		</tr>
		<tr>
			<!-- 信託編號 -->
			<th><spring:message code="LB.W0904" /></th>
			<th>${CDNO_F }</th>
		</tr>
		<tr>
			<!-- 基金名稱 -->
			<th><spring:message code="LB.W0025" /></th>
			<th>${TRANSCODE_FUNDLNAME }</th>
		</tr>
		<tr>
			<!-- 贖回信託金額 -->
			<th><spring:message code="LB.W0978" /></th>
			<th>${CRY_CRYNAME } ${FUNDAMT_F }</th>
		</tr>
		<tr>
			<!-- 贖回單位數 -->
			<th><spring:message code="LB.W0979" /></th>
			<th>${UNIT_F }</th>
		</tr>
		<tr>
			<!-- 贖回單位淨值 -->
			<th><spring:message code="LB.W0980" /></th>
			<th>${TRANSCRY_CRYNAME } ${PRICE1_F }</th>
		</tr>
		<tr>
			<!-- 匯率 -->
			<th><spring:message code="LB.Exchange_rate" /></th>
			<th>${EXRATE_F }</th>
		</tr>
		<tr>
			<!-- 贖回收益 -->
			<th><spring:message code="LB.W0981" /></th>
			<th>${CRY_CRYNAME } ${redemptionIncome }</th>
		</tr>
		<tr>
			<!-- 信託管理費 -->
			<th><spring:message code="LB.W0982" /></th>
			<th>${CRY_CRYNAME } ${FCA2_F }</th>
		</tr>
		<tr>
			<!-- 短線交易費用 -->
			<th><spring:message code="LB.W0975" /></th>
			<th>${CRY_CRYNAME } ${SHORTTRADEFEE_F }</th>
		</tr>
		<tr>
			<!-- 遞延申購手續費 -->
			<th><spring:message code="LB.W0984" /></th>
			<th>${AMT1_F }</th>
		</tr>
		<tr>
			<!-- 給付淨額 -->
			<th><spring:message code="LB.W0985" /></th>
			<th>${CRY_CRYNAME } ${AMT7_F }</th>
		</tr>
		<tr>
			<!-- 自動贖回註記 -->
			<th><spring:message code="LB.W0986" /></th>
			<th>${AUTOR_F }</th>
		</tr>
	</table>
	<br/><br/><br/><br/>
</body>
</html>