<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
   <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">    
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
</head>
 <body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 存款餘額證明申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0485" /></li>
		</ol>
	</nav>

	
	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
	<main class="col-12"> 
	<!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<h2><spring:message code="LB.D0485" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>					
			<form method="post" id="formId">
			
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
							<div class="ttb-message">
                                <span><spring:message code="LB.Transaction_successful" /></span>
                            </div>							
							<!-- 帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
								<h4><spring:message code="LB.D0487" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${deposit_balance_proof_result.data.ACN}</span>
									</div>
								</span>
							</div>
							
							<!-- 身份證/營利事業統一編號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
								<h4><spring:message code="LB.D0488" />/<spring:message code="LB.D0489" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${deposit_balance_proof_result.data.CUSIDN}</span>
									</div>
								</span>
							</div>								
							
							<!-- 申請份數 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0490" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${deposit_balance_proof_result.data.COPY}</span>
									</div>
								</span>
							</div>	
							 
							<!-- 申請用途 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0491" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${deposit_balance_proof_result.data.PURPOSE}</span>
									</div>
								</span>
							</div>
							 
							<div class="ttb-input-item row">
								<!--證明日期  -->
								<span class="input-title">
									<label>
										<spring:message code="LB.D0492" />
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">										
										<div class="ttb-input">	
											<span>${deposit_balance_proof_result.data.DATE}</span>											
										</div>
									</div>
								</span>
							</div>
							
							<!-- 證明書種類 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0495" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<c:if test="${deposit_balance_proof_result.data.INQ_TYPE == '1'}">
									<span><spring:message code="LB.D0496" /></span>					       			
					       			</c:if>
					       			<c:if test="${deposit_balance_proof_result.data.INQ_TYPE == '2'}">
									<span><spring:message code="LB.D0497" /></span>				       			
					       			</c:if>									
								</span>
							</div>
							
							<!-- 幣別 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.Currency" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${deposit_balance_proof_result.data.CRY}</span>
									</div>
								</span>
							</div>
							
							<!-- 證明金額 -->
							<c:if test="${deposit_balance_proof_result.data.INQ_TYPE == '2'}">
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0499" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${deposit_balance_proof_result.data.AMT}</span>
									</div>
								</span>
							</div>
							</c:if>
							
							<!-- 提供文件版本 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0500" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
									<c:if test="${deposit_balance_proof_result.data.FORM == '1'}">
									<span><spring:message code="LB.D0394" /></span>					       			
					       			</c:if>
					       			<c:if test="${deposit_balance_proof_result.data.FORM == '2'}">
									<span><spring:message code="LB.D0396" /></span>					       			
					       			</c:if>
									</div>
								</span>
							</div>
							
							<!-- 英文戶名 -->
							<c:if test="${deposit_balance_proof_result.data.FORM == '2'}">	
							<div class="ttb-input-item row" style="display:none;">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.X0363" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${deposit_balance_proof_result.data.ENGNAME}</span>
									</div>
								</span>
							</div>
							</c:if>
							<!--領取方式 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0503" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${deposit_balance_proof_result.data.DATA}</span>
									</div>
								</span>
							</div>
							
							<!--手續費扣帳帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0506" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${deposit_balance_proof_result.data.OUTACN}</span>
									</div>
								</span>
							</div>
							
							<!--手續費 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0507" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>
											<span class="input-unit"><spring:message code="LB.D0508" /></span>
											${deposit_balance_proof_result.data.FEAMT}
											<span class="input-unit"><spring:message code="LB.Dollar_1" /></span>
										</span>
									</div>
								</span>
							</div>

						</div>
						<input type="button" class="ttb-button btn-flat-orange" id = "printbtn" value="<spring:message code="LB.Print" />"/>
					</div>
				</div>								
				</form>

<!-- 				</form> -->
			</section>
		<!-- 		main-content END -->
		</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
<!--   Js function -->
    <script type="text/JavaScript">
		$(document).ready(function() {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 50);
			// 開始跑下拉選單並完成畫面
			setTimeout("init()", 400);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
			
		});
		
		function init(){
			$("#printbtn").click(function(){
				var params = {
						"jspTemplateName":"deposit_balance_proof_result_print",
						"jspTitle":"<spring:message code= "LB.D0485"/>",
						"CUSIDN":"${deposit_balance_proof_result.data.CUSIDN}",
						"ACN":"${deposit_balance_proof_result.data.ACN}",
						"COPY":"${deposit_balance_proof_result.data.COPY}",
						"PURPOSE1":"${deposit_balance_proof_result.data.PURPOSE}",
						"DATE1":"${deposit_balance_proof_result.data.DATE}",
						"INQ_TYPE":"${deposit_balance_proof_result.data.INQ_TYPE}",
						"CRY":"${deposit_balance_proof_result.data.CRY}",
						"AMT":"${deposit_balance_proof_result.data.AMT}",
						"VER":"${deposit_balance_proof_result.data.FORM}",
						"ENGNAME":"${deposit_balance_proof_result.data.ENGNAME}",
						"DATA":"${deposit_balance_proof_result.data.DATA}",
						"FEEACN":"${deposit_balance_proof_result.data.OUTACN}",
						"FEAMT":"${deposit_balance_proof_result.data.FEAMT}"
					};
					openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
		}
		
		
 	</script>
</body>
</html>
