<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	
	<script type="text/javascript">
		$(document).ready(function(){
			// 列印
			$("#printbtn").click(function(){
				var params = {
					"jspTemplateName":"f_transfer_s_print",
					"jspTitle":"<spring:message code= "LB.FX_Exchange_Transfer" />",
					"CMTXTIME":"${transfer_result_data.data.CMTXTIME }",
					"PAYDATE":"${transfer_result_data.data.PAYDATE }",
					"CUSTACC":"${transfer_result_data.data.CUSTACC }",
					"BENACC":"${transfer_result_data.data.BENACC }",
					"PAYREMIT":"${transfer_result_data.data.PAYREMIT }",
					"display_PAYCCY":"${transfer_result_data.data.display_PAYCCY }",
					"CURAMT":"${transfer_result_data.data.CURAMT }",
					"display_REMITCY":"${transfer_result_data.data.display_REMITCY }",
					"SELFFLAG":"${transfer_result_data.data.SELFFLAG }",
					"MEMO1":"${transfer_result_data.data.MEMO1 }",
					"CMTRMEMO":"${transfer_result_data.data.CMTRMEMO }",
					"RESULT":"${transfer_result_data.result}"
				};
				openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});

		});

		// 下載
	 	function formReset() {
			// 沒選擇項目則不Submit
	 		if ($('#actionBar').val()==""){
				return false;
	 		}
		 	
		 	// EXCEL
    		if ($('#actionBar').val()=="excel"){
    			// 顯示轉出金額
	    		if('${transfer_result_data.data.PAYREMIT}' == '1'){
	    			$("#downloadType").val("OLDEXCEL");
					$("#templatePath").val("/downloadTemplate/f_transfer_s_o_result.xls");
	    		}// 顯示轉入金額
				else if('${transfer_result_data.data.PAYREMIT}' == '2'){
					$("#downloadType").val("OLDEXCEL");
					$("#templatePath").val("/downloadTemplate/f_transfer_s_i_result.xls");
			    }
				
	 		}// .TXT
	 		else if ($('#actionBar').val()=="txt"){
	 			// 顯示轉出金額
	    		if('${transfer_result_data.data.PAYREMIT}' == '1'){
	    			$("#downloadType").val("TXT");
					$("#templatePath").val("/downloadTemplate/f_transfer_s_o_result.txt");
	    		}// 顯示轉入金額
				else if('${transfer_result_data.data.PAYREMIT}' == '2'){
					$("#downloadType").val("TXT");
					$("#templatePath").val("/downloadTemplate/f_transfer_s_i_result.txt");
			    }
				
	 		}
    		$("#formId").attr("target", "");
			$("#formId").submit();
			$('#actionBar').val("");
		}
	</script>
</head>
<body>
    <!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	
 	<!--   IDGATE --> 		 
	<%@ include file="../idgate_tran/idgateAlert.jsp" %> 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 買賣外幣/約定轉帳     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.FX_Exchange_Transfer" /></li>
		</ol>
	</nav>



	
	<!-- 功能清單及登入資訊 -->
	<div class="content row">

		<!-- 功能清單 -->
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
			
				<!-- 功能名稱 -->
	            <h2><spring:message code="LB.FX_Exchange_Transfer" /></h2>
	            <i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
	            
				<!-- 交易流程階段 -->
<!-- 	            <div id="step-bar"> -->
<!-- 	                <ul> -->
<%-- 	                    <li class="finished"><spring:message code="LB.Enter_data" /></li> --%>
<%-- 	                    <li class="finished"><spring:message code="LB.Confirm_data" /></li> --%>
<%-- 	                    <li class="active"><spring:message code="LB.Transaction_complete" /></li> --%>
<!-- 	                </ul> -->
<!-- 	            </div> -->

				<!-- 下載 -->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<!-- 下載 -->
						<option value=""><spring:message code="LB.Downloads" /></option>
						<!-- 下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
						<!-- 下載txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
					</select>
				</div>

	            <form id="formId" action="${__ctx}/download" method="post">
					<!-- 下載用 -->
                    <input type="hidden" name="downloadFileName" value="<spring:message code="LB.FX_Exchange_Transfer" />" />
                    <input type="hidden" name="CMTXTIME" value="${transfer_result_data.data.CMTXTIME }" />
                    <input type="hidden" name="PAYDATE" value="${transfer_result_data.data.PAYDATE }" />
					<input type="hidden" name="CUSTACC" value="${transfer_result_data.data.CUSTACC }" />
					<input type="hidden" name="PAYREMIT" value="${transfer_result_data.data.PAYREMIT }" />
					<input type="hidden" name="display_PAYCCY" value="${transfer_result_data.data.display_PAYCCY }" />
					<input type="hidden" name="CURAMT" value="${transfer_result_data.data.CURAMT }" />
					<input type="hidden" name="BENACC" value="${transfer_result_data.data.BENACC }" />
					<input type="hidden" name="display_REMITCY" value="${transfer_result_data.data.display_REMITCY }" />
					<input type="hidden" name="SELFFLAG" value="${transfer_result_data.data.SELFFLAG }" />
					<input type="hidden" name="MEMO1" value="${transfer_result_data.data.MEMO1 }" />
					<input type="hidden" name="CMTRMEMO" value="${transfer_result_data.data.CMTRMEMO }" />
                    <input type="hidden" name="downloadType" id="downloadType" />
                    <input type="hidden" name="templatePath" id="templatePath" />
                    <!-- EXCEL下載用 -->
					<input type="hidden" name="headerRightEnd" value="2"/><!-- headerRightEnd 資料列以前的右方界線 -->
					<input type="hidden" name="headerBottomEnd" value="11"/><!-- headerBottomEnd 資料列到第幾列 從0開始 -->
                    <!-- TXT下載用-總行數 -->
					<input type="hidden" name="txtHeaderBottomEnd" value="11"/> 					
					<input type="hidden" name="txtHasRowData" value="false"/>
					<input type="hidden" name="txtHasFooter" value="false"/>
					
					<!-- 功能內容 -->
		            <div class="main-content-block row">
		                <div class="col-12">
		                
		                <!-- 預約成功 -->
						<c:if test="${transfer_result_data.result == true}">
		                    <h4 style="margin-top:10px;font-weight:bold;color:red">
								<spring:message code="LB.W0284" /><!-- 預約成功 -->
							</h4>
							<div class="ttb-input-block">
								<!-- 資料時間 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.Data_time" />
										</label>
									</span>
									<span class="input-block">
										${transfer_result_data.data.CMTXTIME }
									</span>
								</div>
								
								<!-- 轉帳日期 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.Transfer_date" />
										</label>
									</span>
									<span class="input-block">
										${transfer_result_data.data.PAYDATE }
									</span>
								</div>
								
								<!-- 轉出帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.Payers_account_no" />
										</label>
									</span>
									<span class="input-block">
										${transfer_result_data.data.CUSTACC }
									</span>
								</div>
								
								<!-- 轉入帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.Payees_account_no" />
										</label>
									</span>
									<span class="input-block">
										${transfer_result_data.data.BENACC }
									</span>
								</div>
								
							<c:if test="${transfer_result_data.data.PAYREMIT eq '1' }">
		                    
		                        <!-- 轉出金額 -->
		                        <div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.Deducted" />
										</label>
									</span>
									<span class="input-block">
										${transfer_result_data.data.display_PAYCCY }&nbsp;
		                            	${transfer_result_data.data.CURAMT }&nbsp;
		                            	<spring:message code="LB.Dollar" />
									</span>
								</div>
		                        
		                    </c:if>
								
							<c:if test="${transfer_result_data.data.PAYREMIT eq '2' }">
		                    
		                        <!-- 轉入金額 -->
		                        <div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.Buy" />
										</label>
									</span>
									<span class="input-block">
										${transfer_result_data.data.display_REMITCY }&nbsp;
		                            	${transfer_result_data.data.CURAMT }&nbsp;
		                            	<spring:message code="LB.Dollar" />
									</span>
								</div>
		                        
		                    </c:if>
								
							<!-- 自行轉帳才要顯示收款人附言  -->
	                    	<c:if test="${transfer_result_data.data.SELFFLAG eq 'Y' }">
		                    
		                        <!-- 收款人附言 -->
		                        <div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.W0280" />
										</label>
									</span>
									<span class="input-block">
										${transfer_result_data.data.MEMO1 }
									</span>
								</div>
		                        
		                    </c:if>	
								
								<!-- 交易備註 -->
		                        <div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.Transfer_note" />
										</label>
									</span>
									<span class="input-block">
										${transfer_result_data.data.CMTRMEMO }
									</span>
								</div>
								
							</div>
								
							<!-- 列印 -->
							<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
							
							<!-- 下載選項移到選單 -->
	<!-- 				        <input type="button" class="ttb-button btn-flat-orange" id="CMEXCEL" name="CMEXCEL" value="下載Excel檔" /> -->
	<!--         				<input type="button" class="ttb-button btn-flat-orange" id="CMTXT" name="CMTXT" value="下載.txt檔" /> -->
						</c:if>
						
		                </div>
		            </div>
		           
					<!-- 說明 -->
					<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /></p>
						<li><strong style="font-weight: 400"><spring:message code="LB.F_Transfer_P6_D1" /></strong></li>
						<li><strong style="font-weight: 400"><spring:message code="LB.F_Transfer_P6_D2" /></strong></li>
						<li><strong style="font-weight: 400"><spring:message code="LB.F_Transfer_P6_D3" /></strong></li>
						<li><strong style="font-weight: 400"><spring:message code="LB.F_Transfer_P6_D4" /></strong></li>
						<li><strong style="font-weight: 400"><spring:message code="LB.F_Transfer_P6_D5" /></strong></li>
						<li><strong style="font-weight: 400"><spring:message code="LB.F_Transfer_P6_D6" /></strong></li>
						<li><strong style="font-weight: 400"><spring:message code="LB.F_Transfer_P6_D7" /></strong></li>			
				        <li><spring:message code="LB.F_Transfer_P6_D8" /></li>
					</ol>

				</form>
	        </section>    
		</main>
	</div>

	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>