<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="card-detail-block">
	<div class="card-center-block">

		<!-- Q1 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup3-1" aria-expanded="true"
				aria-controls="popup3-1">
				<div class="row">
					<span class="col-1">Q1</span>
					<div class="col-11">
						<span>受理时间</span>
					</div>
				</div>
			</a>
			<div id="popup3-1" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div style="overflow: auto">
								<table class="question-table" style="width: 100%">
									<thead>
										<tr>
											<th>临柜</th>
											<th>网络</th>
											<th>语音</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>营业日上午9 时至下午3 时</td>
											<td colspan="2">交易服务时间：营业日上午7 时至下午3 时 <br>
												预约服务时间：交易服务时间以外 <br> （交易服务时间截止后至次一营业日交易服务时间开始前）
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q2 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup3-2" aria-expanded="true"
				aria-controls="popup3-2">
				<div class="row">
					<span class="col-1">Q2</span>
					<div class="col-11">
						<span>最低申购投资金额</span>
					</div>
				</div>
			</a>
			<div id="popup3-2" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div style="overflow: auto">
								<table class="question-table" style="width: 100%">
									<thead>
										<tr>
											<th colspan="3">投资目标</th>
											<th>最低投资金额</th>
											<th>增加单位</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th rowspan="19">单笔</th>
											<th colspan="2">国内货币型基金</th>
											<td colspan="2">视个别基金规定（上午10：30截止申购作业）</td>
										</tr>
										<tr>
											<th rowspan="5">国内基金</th>
											<th>台币信托</th>
											<td>新台币10,000元</td>
											<td>新台币1,000元</td>
										</tr>
										<tr>
											<th rowspan="4" align="center" class="f13px lineH_01 coR">外币信托</th>
											<td>南非币10,000元</td>
											<td>南非币1,000元</td>
										</tr>
										<tr>
											<td>美元1,000元</td>
											<td>美元100元</td>
										</tr>
										<tr>
											<td>人民币5,000元</td>
											<td>人民币1,000元</td>
										</tr>
										<tr>
											<td>澳币1,000元</td>
											<td>澳币100元</td>
										</tr>
										<tr>
											<th rowspan="13">国外基金</th>
											<th>台币信托</th>
											<td>新台币30,000元</td>
											<td>新台币10,000元</td>
										</tr>
										<tr>
											<th rowspan="12">外币信托</th>
											<td>美元1,000元</td>
											<td>美元100元</td>
										</tr>
										<tr>
											<td>欧元1,000元</td>
											<td>欧元100元</td>
										</tr>
										<tr>
											<td>英镑1,000元</td>
											<td>英镑100元</td>
										</tr>
										<tr>
											<td>澳币1,000元</td>
											<td>澳币100元</td>
										</tr>
										<tr>
											<td>瑞士法郎1,000元</td>
											<td>瑞士法郎100元</td>
										</tr>
										<tr>
											<td>加拿大币1,000元</td>
											<td>加拿大币100元</td>
										</tr>
										<tr>
											<td>瑞典币10,000元</td>
											<td>瑞典币1,000元</td>
										</tr>
										<tr>
											<td>日圆100,000元</td>
											<td>日圆10,000元</td>
										</tr>
										<tr>
											<td>新加坡币1,000元</td>
											<td>新加坡币100元</td>
										</tr>
										<tr>
											<td>纽币1,000元</td>
											<td>纽币100元</td>
										</tr>
										<tr>
											<td>港币10,000元</td>
											<td>港币1,000元</td>
										</tr>
										<tr>
											<td>南非币10,000元</td>
											<td>南非币1,000元</td>
										</tr>
										<tr>
											<th rowspan="19">定额/不定额</th>
											<th colspan="2">国内货币型基金</th>
											<td colspan="2">暂未开放</td>
										</tr>
										<tr>
											<th colspan="2" rowspan="5">国内基金</th>
											<td>新台币3,000元</td>
											<td>新台币1,000元</td>
										</tr>
										<tr>
											<td>南非币2,000元</td>
											<td>南非币500元</td>
										</tr>
										<tr>
											<td>美元200元</td>
											<td>美元50元</td>
										</tr>
										<tr>
											<td>人民币1,000元</td>
											<td>人民币500元</td>
										</tr>
										<tr>
											<td>澳币200元</td>
											<td>澳币50元</td>
										</tr>
	
										<tr>
											<th rowspan="13">国外基金</th>
											<th>台币信托</th>
											<td>新台币3,000/5,000元</td>
											<td>新台币1,000元</td>
										</tr>
										<tr>
											<th rowspan="12">外币信托</th>
											<td>美元200/300元</td>
											<td>美元50元</td>
										</tr>
										<tr>
											<td>欧元200/300元</td>
											<td>欧元50元</td>
										</tr>
										<tr>
											<td>英镑200/300元</td>
											<td>英镑50元</td>
										</tr>
										<tr>
											<td>澳币200/300元</td>
											<td>澳币50元</td>
										</tr>
										<tr>
											<td>瑞士法郎200/300元</td>
											<td>瑞士法郎50元</td>
										</tr>
										<tr>
											<td>加拿大币200/300元</td>
											<td>加拿大币50元</td>
										</tr>
										<tr>
											<td>瑞典币2,000/3,000元</td>
											<td>瑞典币500元</td>
										</tr>
										<tr>
											<td>日圆20,000/30,000元</td>
											<td>日圆5,000元</td>
										</tr>
										<tr>
											<td>纽币200/300元</td>
											<td>纽币50元</td>
										</tr>
										<tr>
											<td>港币2,000元</td>
											<td>港币500元</td>
										</tr>
										<tr>
											<td>新加坡币200元</td>
											<td>新加坡币50元</td>
										</tr>
										<tr>
											<td>南非币2,000元</td>
											<td>南非币500元</td>
										</tr>
									</tbody>
								</table>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q3 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup3-3" aria-expanded="true"
				aria-controls="popup3-3">
				<div class="row">
					<span class="col-1">Q3</span>
					<div class="col-11">
						<span>申购手续费</span>
					</div>
				</div>
			</a>
			<div id="popup3-3" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div style="overflow: auto">
								<table class="question-table" style="width: 100%">
									<thead>
										<tr>
											<th colspan="3">投资目标</th>
											<th colspan="2">最低手续费</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th colspan="3">单笔</th>
											<td colspan="2">系依各基金公司规定之费率计收</td>
										</tr>
										<tr>
											<th rowspan="10">定期 <br> (不)定额
											</th>
											<th colspan="2" rowspan="3">国内非货币型基金</th>
											<td>新台币50元</td>
											<td>南非币60元</td>
										</tr>
										<tr>
											<td>美元6元</td>
											<td>人民币30元</td>
										</tr>
										<tr>
											<td>澳币6元</td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<th rowspan="7">国外基金</th>
											<th>台币信托</th>
											<td colspan="2">新台币150元</td>
										</tr>
										<tr>
											<th rowspan="6">外币信托</th>
											<td>美元6元</td>
											<td>瑞士法郎6元</td>
										</tr>
										<tr>
											<td>欧元6元</td>
											<td>加拿大币6元</td>
										</tr>
										<tr>
											<td>英镑6元</td>
											<td>瑞典币60元</td>
										</tr>
										<tr>
											<td>澳币6元</td>
											<td>日圆600元</td>
										</tr>
										<tr>
											<td>港币60元</td>
											<td>纽币6元</td>
										</tr>
										<tr>
											<td>新加坡币6元</td>
											<td>南非币60元</td>
										</tr>
									</tbody>
								</table>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q4 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup3-4" aria-expanded="true"
				aria-controls="popup3-4">
				<div class="row">
					<span class="col-1">Q4</span>
					<div class="col-11">
						<span>定期定额／不定额投资注意事项</span>
					</div>
				</div>
			</a>
			<div id="popup3-4" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<ol class="ttb-result-list terms">
								<li data-num="1.">
									为确保投资成功，选择以存款帐户扣款者，请于指定扣帐日前一营业日留存足够扣帐金额，选择以信用卡扣款者，委托人及持卡人应于扣款日前二个营业日，确认信用卡余额是否足够基金投资扣款；另委托人如同时有数笔应投资款项而余额不足时，则同意本行以扣帐作业整理之先后顺序为准。</li>

								<li data-num="2.">
									如因线路中断或其他事由，致本行不及于委托人指定之扣款日进行扣款投资作业，委托人同意顺延至障碍事由排除后之银行营业日进行扣帐，并于完成扣帐后始进行投资。</li>

								<li data-num="3.">关于「定期定额约定变更」之说明
									<ul class="ttb-result-list terms">
										<li data-num="a.">
											本系统执行「定期定额约定变更」项下之各项约定以申请日当天生效为原则，惟欲变更的信托数据已在进行相关项目处理中，或已完成扣款作业，该约定生效将顺延。
										</li>
										<li data-num="b.">
											若欲变更的信托数据，本行计算机系统已在进行相关项目处理中，将暂时无法为您提供服务，烦请于次一营业日(涉及信用卡交易须次二营业日)再行申请。
										</li>
										<li data-num="c.">
											单一凭证有两支以上扣款目标者，网络／系统无法提供「定期定额扣款金额」变更服务，烦请洽营业单位临柜办理。</li>
									</ul>
								</li>
							</ol>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q5 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup3-5" aria-expanded="true"
				aria-controls="popup3-5">
				<div class="row">
					<span class="col-1">Q5</span>
					<div class="col-11">
						<span>投资目标转出及账面剩余金额限制</span>
					</div>
				</div>
			</a>
			<div id="popup3-5" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div style="overflow: auto">
								<table class="question-table" style="width: 100%">
									<thead>
										<tr>
											<th colspan="3">投资目标</th>
											<th>最低部份赎回及账面剩余金额</th>
											<th>增加单位</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th rowspan="19">单笔</th>
											<th colspan="2">国内货币型基金</th>
											<td colspan="2">视个别基金规定</td>
										</tr>
										<tr>
											<th rowspan="5">国内基金</th>
											<th>台币信托</th>
											<td>新台币10,000元</td>
											<td>新台币1,000元</td>
										</tr>
										<tr>
											<th rowspan="4">外币信托</th>
											<td>南非币10,000元</td>
											<td>南非币1,000元</td>
										</tr>
										<tr>
											<td>美元1,000元</td>
											<td>美元100元</td>
										</tr>
										<tr>
											<td>人民币5,000元</td>
											<td>人民币1,000元</td>
										</tr>
										<tr>
											<td>澳币1,000元</td>
											<td>澳币100元</td>
										</tr>
										<tr>
											<th rowspan="13">国外基金</th>
											<th>台币信托</th>
											<td>新台币30,000元</td>
											<td>新台币10,000元</td>
										</tr>
										<tr>
											<th rowspan="12">外币信托</th>
											<td>美元1,000元</td>
											<td>美元100元</td>
										</tr>
										<tr>
											<td>欧元1,000元</td>
											<td>欧元100元</td>
										</tr>
										<tr>
											<td>英镑1,000元</td>
											<td>英镑100元</td>
										</tr>
										<tr>
											<td>澳币1,000元</td>
											<td>澳币100元</td>
										</tr>
										<tr>
											<td>瑞士法郎1,000元</td>
											<td>瑞士法郎100元</td>
										</tr>
										<tr>
											<td>加拿大币1,000元</td>
											<td>加拿大币100元</td>
										</tr>
										<tr>
											<td>瑞典币10,000元</td>
											<td>瑞典币1,000元</td>
										</tr>
										<tr>
											<td>日圆100,000元</td>
											<td>日圆10,000元</td>
										</tr>
										<tr>
											<td>新加坡币1,000元</td>
											<td>新加坡币100元</td>
										</tr>
										<tr>
											<td>纽币1,000元</td>
											<td>纽币100元</td>
										</tr>
										<tr>
											<td>港币10,000元</td>
											<td>港币1,000元</td>
										</tr>
										<tr>
											<td>南非币10,000元</td>
											<td>南非币1,000元</td>
										</tr>
										<tr>
											<th colspan="3">定期(不)定额</th>
											<td colspan="2">各投资目标须一次全部转出，不得申请部份转出。</td>
										</tr>
									</tbody>
								</table>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q6 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup3-6" aria-expanded="true"
				aria-controls="popup3-6">
				<div class="row">
					<span class="col-1">Q6</span>
					<div class="col-11">
						<span>转换手续费</span>
					</div>
				</div>
			</a>
			<div id="popup3-6" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<ol class="ttb-result-list terms">
								<li data-num="1.">本行收取转换手续费每笔新台币500元。</li>
								<li data-num="2.">各基金之转换费用原则以内扣方式办理，并依各基金公司规定计收。</li>
							</ol>
						</li>
					</ul>
				</div>
			</div>
		</div>
		
		<!-- Q7 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup3-7" aria-expanded="true"
				aria-controls="popup3-7">
				<div class="row">
					<span class="col-1">Q7</span>
					<div class="col-11">
						<span>部分赎回及账面剩余金额之限制</span>
					</div>
				</div>
			</a>
			<div id="popup3-7" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div style="overflow: auto">
								<table class="question-table" style="width: 100%">
									<thead>
										<tr>
											<th colspan="3">投资目标</th>
											<th>最低部份赎回及账面剩余金额</th>
											<th>增加单位</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th rowspan="19">单笔(不)定额</th>
											<th colspan="2">国内货币型基金</th>
											<td colspan="2">视个别基金规定</td>
										</tr>
										<tr>
											<th rowspan="5">国内基金</th>
											<th>台币信托</th>
											<td>新台币10,000元</td>
											<td>新台币1,000元</td>
										</tr>
										<tr>
											<th rowspan="4">外币信托</th>
											<td>南非币10,000元</td>
											<td>南非币1,000元</td>
										</tr>
										<tr>
											<td>美元1,000元</td>
											<td>美元100元</td>
										</tr>
										<tr>
											<td>人民币5,000元</td>
											<td>人民币1,000元</td>
										</tr>
										<tr>
											<td>澳币1,000元</td>
											<td>澳币100元</td>
										</tr>
										<tr>
											<th rowspan="13">国外基金</th>
											<th>台币信托</th>
											<td>新台币30,000元</td>
											<td>新台币10,000元</td>
										</tr>
										<tr>
											<th rowspan="12">外币信托</th>
											<td>美元1,000元</td>
											<td>美元100元</td>
										</tr>
										<tr>
											<td>欧元1,000元</td>
											<td>欧元100元</td>
										</tr>
										<tr>
											<td>英镑1,000元</td>
											<td>英镑100元</td>
										</tr>
										<tr>
											<td>澳币1,000元</td>
											<td>澳币100元</td>
										</tr>
										<tr>
											<td>瑞士法郎1,000元</td>
											<td>瑞士法郎100元</td>
										</tr>
										<tr>
											<td>加拿大币1,000元</td>
											<td>加拿大币100元</td>
										</tr>
										<tr>
											<td>瑞典币10,000元</td>
											<td>瑞典币1,000元</td>
										</tr>
										<tr>
											<td>日圆100,000元</td>
											<td>日圆10,000元</td>
										</tr>
										<tr>
											<td>新加坡币1,000元</td>
											<td>新加坡币100元</td>
										</tr>
										<tr>
											<td>纽币1,000元</td>
											<td>纽币100元</td>
										</tr>
										<tr>
											<td>港币10,000元</td>
											<td>港币1,000元</td>
										</tr>
										<tr>
											<td>南非币10,000元</td>
											<td>南非币1,000元</td>
										</tr>
									</tbody>
								</table>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		
		<!-- Q8 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup3-8" aria-expanded="true"
				aria-controls="popup3-8">
				<div class="row">
					<span class="col-1">Q8</span>
					<div class="col-11">
						<span>信托管理费</span>
					</div>
				</div>
			</a>
			<div id="popup3-8" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div style="overflow: auto">
								<table class="question-table" style="width: 100%">
									<thead>
										<tr>
											<th width="17%">投资目标</th>
											<th>信托管理费</th>
											<th>最低信托管理费</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th>单笔</th>
											<td class="white-spacing text-left" rowspan="2">本行自委托人交付指定投资目标信托资金（即信托金额）届满一年之次日起至该指定投资目标赎回日止，依信托资金帐载余额每年按年率千分之二计收信托管理费（未满一年之部份依实际天数计收）。<br>委托人指定投资申购目标类型为国内货币型基金者，免收信托管理费。
											</td>
											<td>等值新台币100元</td>
										</tr>
										<tr>
											<th>定期定额/不定额</th>
											<td>等值新台币200元</td>
										</tr>
									</tbody>
								</table>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q9 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup3-9" aria-expanded="true"
				aria-controls="popup3-9">
				<div class="row">
					<span class="col-1">Q9</span>
					<div class="col-11">
						<span>申购单位数分配日</span>
					</div>
				</div>
			</a>
			<div id="popup3-9" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div style="overflow: auto">
								<table class="question-table" style="width: 100%">
									<thead>
										<tr>
											<th>基金种类</th>
											<th>申购单位数分配时间</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th>国内基金</th>
											<td valign="top" align="center" class=" f13px lineH_01 coR">投资生效日后约3-5个金融机构营业日
											</td>
										</tr>
										<tr>
											<th>境外基金</th>
											<td>投资生效日后约3-7个金融机构营业日</td>
										</tr>
									</tbody>
								</table>
							</div>
							<p>※上述分配期间仅提供参考，若因基金公司作业或各基金另有规定，则分配时间不在此限。</p>

						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q10 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup3-10" aria-expanded="true"
				aria-controls="popup3-10">
				<div class="row">
					<span class="col-1">Q10</span>
					<div class="col-11">
						<span>赎回款入账日</span>
					</div>
				</div>
			</a>
			<div id="popup3-10" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div style="overflow: auto"><div>
								<table class="question-table" style="width: 100%">
									<thead>
										<tr>
											<th>基金种类</th>
											<th>申购单位数分配时间</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th>国内基金</th>
											<td>赎回生效日后约3-10个金融机构营业日 <br> （货币型基金为次1金融机构营业日）
											</td>
										</tr>
										<tr>
											<th>境外基金</th>
											<td>投资生效日后约3-7个金融机构营业日</td>
										</tr>
									</tbody>
								</table>
							</div>
							<p>※各基金赎回款之入账日不尽相同，须依各基金之规定及作业为准，赎回汇率悉依受托人于合理处理期间内实际办理买汇或卖汇之汇率为准。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q11 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup3-11" aria-expanded="true"
				aria-controls="popup3-11">
				<div class="row">
					<span class="col-1">Q11</span>
					<div class="col-11">
						<span>换汇说明</span>
					</div>
				</div>
			</a>
			<div id="popup3-11" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<ol class="ttb-result-list terms">
								<li data-num="1.">申购交易:依交易生效日（遇例假日顺延至次一营业日），采用受托人下午三时左右之银行牌告实时卖汇汇率为准并予以适当优惠计算。</li>
								<li data-num="2.">赎回交易:依赎回款项汇入受托人指定账户时，采用受托人上午十时左右之银行牌告实时买汇汇率为准并予以适当优惠计算。</li>
							</ol>
						</li>
					</ul>
				</div>
			</div>

		</div>
		<p>所载内容如因排版、校对等因素所致之错误，仍应以本行或基金公司实际作业规范及刊登内容为准，委托人如有疑义可随时向本行咨询。</p>
	</div>
</div>
