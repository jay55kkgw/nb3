<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="RS" value="${transfer_data_query_result.data.RS}"></c:set>

<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<style>
	.Line-break{
		white-space:normal;
		word-break:break-all;
		width:100px;
		word-wrap:break-word;
	}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		setTimeout("initDataTable()",100);
		// show table
		setTimeout("$('.table').show();", 50);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
		
		jQuery(function($) {
    		$('.dtablet').DataTable({
    			scrollX: true,
    			sScrollX: "99%",
    			scrollY: true,
    			bPaginate: false,
    			bFilter: false,
    			bDestroy: true,
    			bSort: false,
    			info: false,
    		});
    	});
	});

	function init() {
		//	initFootable();

		$("#printbtn")
				.click(
						function() {
							var params = {
								jspTemplateName : "transfer_data_query_result_print",
								jspTitle : "<spring:message code= "LB.W0943" />",
								CMQTIME : "${RS.CMQTIME }",
								CMPERIOD : "${RS.CMPERIOD }",
								CMRECNUM : "${RS.CMRECNUM }",
								CUSIDN_F : "${RS.CUSIDN_F }",
								CUSNAME_F : "${RS.CUSNAME_F }"
							};
							openWindowWithPost(
									"${__ctx}/print",
									"height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",
									params);
						});

	}

	// 單筆交易明細
	function processQuery(TRANSCODE, CDNO, TRADEDATE, FUNDTYPE, INTRANSCODE) {
		console.log('processQuery');
		var params = {
			TRANSCODE : TRANSCODE,
			CDNO : CDNO,
			TRADEDATE : TRADEDATE,
			FUNDTYPE : FUNDTYPE,
			INTRANSCODE : INTRANSCODE
		};
		var url = '${__ctx}/FUND/QUERY/transfer_data_query_one';
		submitForm(params, url);
	}

	// 歷史交易明細
	function processQueryHistory(CDNO) {
		console.log('processQueryHistory');
		var url = '${__ctx}/FUND/QUERY/transfer_data_query_history';
		var params = {
			CDNO : CDNO
		};
		submitForm(params, url);
	}

	function submitForm(params, url) {
		initBlockUI();
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("action", url);
		form.setAttribute("target", "_self");

		for ( var param in params) {
			if (params.hasOwnProperty(param)) {
				var input = document.createElement("input");
				input.type = "hidden";
				input.name = param;
				input.value = params[param];
				form.appendChild(input);
			}
		}
		document.body.appendChild(form);
		form.submit();
	}
</script>
</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金    -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0901" /></li>
    <!-- 基金查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0902" /></li>
    <!-- 基金交易資料查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0943" /></li>
		</ol>
	</nav>

	<!-- 左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>

		<!-- 快速選單及主頁內容 -->
		<main class="col-12"> <!-- 主頁內容  -->
		<section id="main-content" class="container">
			<h2>
				<spring:message code="LB.W0943" />
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>

			<div class="main-content-block row">
				<div class="col-12">
					<ul class="ttb-result-list">
						<li>
							<!-- 查詢時間 -->
							<h3>
								<spring:message code="LB.Inquiry_time" />
								
							</h3>
							<p>${RS.CMQTIME }</p>
						</li>
						<li>
							<!-- 查詢期間 -->
							<h3>
								<spring:message code="LB.Inquiry_period_1" />
								
							</h3>
							<p>${RS.CMPERIOD }</p>
						</li>
						<li>
							<!-- 資料總數 -->
							<h3>
								<spring:message code="LB.Total_records" />
								
							</h3>
							<p>
								${RS.CMRECNUM }
								<spring:message code="LB.Rows" />
							</p>
						</li>
						<li>
							<!-- 身分證字號/統一編號 -->
							<h3>
								<spring:message code="LB.Id_no" />
								
							</h3>
							<p>${RS.CUSIDN_F }</p>
						</li>
						<li>
							<!-- 姓名 -->
							<h3>
								<spring:message code="LB.Name" />
								
							</h3>
							<p>${RS.CUSNAME_F }</p>
						</li>
					</ul>
					<table class="stripe table-striped ttb-table dtablet" id="example" data-show-toggle="first">
						<thead>
							<tr>
								<!-- 交易日期 -->
								<th><spring:message code="LB.Transaction_date" /></th>
								<!-- 信託帳號 -->
								<th><spring:message code="LB.W0944" /></th>
								<!-- 基金名稱 -->
								<th><spring:message code="LB.W0025" /></th>
								<!-- 投資方式 --><!-- 交易類別 -->
								<th><spring:message code="LB.W0946" /><hr><spring:message code="LB.Transaction_type" /></th>
								<!-- 信託/除息金額幣別 -->
								<th><spring:message code="LB.W0947" /></th>
								<!-- 單位數 -->
								<th><spring:message code="LB.W0027" /></th>
								<!-- 淨值 --><!-- 匯率 -->
								<th><spring:message code="LB.W0949" /><hr><spring:message code="LB.Exchange_rate" /></th>
								<!-- 轉入基金名稱 -->
								<th><spring:message code="LB.W0950" /></th>
								<!-- 轉入單位數 --><!-- 轉入價格 -->
								<th><spring:message code="LB.W0951" /><hr><spring:message code="LB.W0952" /></th>
								<!-- 詳細 -->
								<th><spring:message code="LB.W0953" /></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="row" items="${RS.REC }">
								<tr>
									<td class="text-center">${row.TRADEDATE_F }</td>
									<!-- 交易日期 -->
									<td class="text-center">${row.CDNO_F }</td>
									<!-- 信託帳號 -->
									<td class="text-center Line-break">${row.TRANSCODE_FUNDLNAME }</td>
									<!-- 基金名稱 -->
									<td class="text-center">${row.TR106_F }<hr>${row.FUNDTYPE_F }</td>
									<!-- 投資方式 --><!-- 交易類別 -->
									<td class="text-center">${row.CRY_CRYNAME }<br /> <!-- 信託/除息金額幣別 -->
										${row.FUNDAMT_F } <!-- 信託/除息金額 -->
									</td>
									<td class="text-right">${row.UNIT_F }</td>
									<!-- 單位數 -->
									<td class="text-right">${row.PRICE1_F }<hr>${row.EXRATE_F }</td>
									<!-- 淨值 --><!-- 匯率 -->
									<td class="text-center Line-break">${row.INTRANSCODE_FUNDLNAME }</td>
									<!-- 轉入基金名稱 -->
									<td class="text-right">${row.TXUNIT_F }<hr>${row.INTRANSCRY_CRYNAME }${row.PRICE2_F }</td>
									<!-- 轉入單位數 --><!-- 轉入價格幣別 --><!-- 轉入價格 -->
									<td class="text-center">
										<!-- 詳細 --> 
										<input type="button" value="<spring:message code= "LB.X1246" />" class="ttb-sm-btn btn-flat-orange"
											onclick="processQuery(
											'${row.TRANSCODE}',
											'${row.CDNO}',
											'${row.TRADEDATE}',
											'${row.FUNDTYPE}',
											'${row.INTRANSCODE}')" />
										<br/>
										<input type="button" style="margin-top: 5px;" value="<spring:message code="LB.W0931" />" class="ttb-sm-btn btn-flat-orange" onclick="processQueryHistory('${row.CDNO}')" />
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<input type="button" class="ttb-button btn-flat-orange"
						id="printbtn" value="<spring:message code="LB.Print" />" />
				</div>
			</div>
		</section>
		</main>
	</div>
	<!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>


</body>
</html>