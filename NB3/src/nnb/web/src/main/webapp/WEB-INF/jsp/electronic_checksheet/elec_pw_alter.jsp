<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
   
	<script type="text/JavaScript">
		$(document).ready(function() {
			init();
		});
    
		function init(){
		    // 初始化表單驗證
	    	$("#formId").validationEngine({binded:false,promptPosition: "inline" });

	    	// submit前
	    	$("#formId").submit(function(e){			
				console.log("before submit...");
				
				if('${sessionScope.dpmyemail}'==''){
					//alert("<spring:message code= "LB.EmailAlert" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.EmailAlert' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
					e.preventDefault();
				}else if(!$('#formId').validationEngine('validate')){
			      	e.preventDefault();
				}else{
					$("#formId").validationEngine('detach');
// 					這邊要比pin_encrypt 先 否則會被遮罩
					$('#Newpwd').val($('#Newpwd_SHOW').val());
					$('#Oldpwd').val($('#Oldpwd_SHOW').val());
					// 表單驗證後，送出前，先把SSL的密碼做加密塞入
					$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
					initBlockUI();
				}
			});
	    }
		
 	</script>	
</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 電子對帳單     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1396" /></li>
    <!-- 密碼變更     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Change_Password" /></li>
		</ol>
	</nav>

	<!-- 快速選單及主頁內容 -->
	<!-- menu、登出窗格 --> 
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
 		<main class="col-12">	
		<!-- 主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.D0305" /><!-- 電子帳單密碼變更 --></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form method="post" id="formId" action="${__ctx}/ELECTRONIC/CHECKSHEET/elec_pw_alter_result">
					<input type="hidden" id="PINNEW" name="PINNEW"  value="">
					<input type="hidden" name="Cust_id" value="">
   					<input type="hidden" name="Status" value="IU">
   					<input type="hidden" name="Email" value="${sessionScope.dpmyemail}">
   					<input type="hidden" name="sys_ip" value="">
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
							<div class="ttb-input-item row">
								<span class="input-title"> <label><h4><spring:message code="LB.D0169" /><!-- 申請項目 -->：</h4></label></span> <!-- 系統時間 -->
								<span class="input-block">
									<div class="ttb-input">
										<spring:message code="LB.Change_Password" /><!-- 密碼變更 -->
									</div>
								</span>
							</div>			        
				        		<!--原電子對帳單密碼 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4><spring:message code="LB.D0308" /><!-- 原電子對帳單密碼 --></h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="hidden" id="Oldpwd" name="Oldpwd" />
											<input type="password" name="Oldpwd_SHOW" id="Oldpwd_SHOW" class="text-input validate[required, custom[onlyLetterNumber]] " size="16" maxlength="16" value="">
										</div>
									</span>
								</div>
								
								<!--新電子對帳單密碼 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4><spring:message code="LB.D0309" /><!-- 新電子對帳單密碼--></h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="hidden" id="Newpwd" name="Newpwd" />
											<input type="password" name = "Newpwd_SHOW" id = "Newpwd_SHOW" class="text-input validate[required , notEquals[N1010_CP,Oldpwd_SHOW], custom[onlyLetterNumber],chkpwd[Newpwd_SHOW]] " size="16" maxlength="16" value="">
											<p>(<spring:message code="LB.D0310" /><!-- 請輸入6-16位英、數字或英數字混合，如為英文字，請注意大小寫-->)</p>
										</div>
									</span>	
								</div>
								
								<!-- 確認新電子對帳單密碼 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
					                 			<spring:message code="LB.D0311" /><!-- 確認新電子對帳單密碼-->
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="password" name = "ConfirmNewpwd" id = "ConfirmNewpwd" class="text-input  validate[required ,equals2[N1010_CP,Newpwd_SHOW], custom[onlyLetterNumber]] " size="16" maxlength="16" value="">
											<p>(<spring:message code="LB.D0312" /><!-- 請再次輸入「新密碼」-->)</p>
										</div>
									</span>
								</div>
								
								<div class="ttb-input-item row">
								<span class="input-title"> <spring:message code="LB.Transaction_security_mechanism" /> </span> <!--交易機制-->
								<span class="input-block">
									<div class="ttb-input">	
										<label class="radio-block">
										<input type="radio" name="FGTXWAY" value="0" checked>
										<span><spring:message code="LB.SSL_password"/>：</span>
										<span class="ttb-radio"></span></label>
									</div>
									<div class="ttb-input">
										<input type="password" name="CMPASSWORD" id="CMPASSWORD" maxlength="8" value="" class="text-input validate[required,custom[onlyLetterNumber]]"
											placeholder="<spring:message code="LB.Please_enter_password" />">
									</div>
								</span>
							</div>
							</div>
							
							<input id="reset" name="reset" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" />	
							<input id="CMSUBMIT" name="CMSUBMIT" type="submit" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />						
						</div>
					</div>
				</form>
			
				<div class="text-left">
					<ol class="list-decimal text-left description-list">
					<p><spring:message code="LB.Description_of_page" /> </p>
						<li><font style="color:red"><b><spring:message code= "LB.Elec_Pw_Alter_P1_D1" /></b></font></li>
					</ol>
				</div>
				
			</section>
		</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
    
</body>
</html>
