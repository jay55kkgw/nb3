<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript">
		// 初始化
		$(document).ready(function () {
			init();
		});

		function init() {
			// 確認鍵 click
			goOn();
			//上一頁按鈕
			back();
			//重新整理
			cmrest();
		}
		//上一頁按鈕
		function back() {
			$("#CMBACK").click(function () {
				var action = '${__ctx}/FUND/ALTER/smart_fund_step2';
				$('#back').val("Y");
				$("#formId").validationEngine('detach');
				$("#formId").attr("action", action);
// 				initBlockUI();
				$("#formId").submit();
			});
		}
		//重新整理
		function cmrest() {
			$("#CMRESET").click(function () {
				$("#formId")[0].reset();
			});
		}
		// 確認鍵 click
		function goOn() {
			$("#CMSUBMIT").click(function (e) {
				//表單驗證
				$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
				if (!$('#formId').validationEngine('validate')) {
					e = e || window.event; // for IE
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					processQuery();
				}
			});
		}
		// 交易機制選項
		function processQuery() {
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
			switch (fgtxway) {
				case '0':
					$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
					$('#CMPASSWORD').val("");
					initBlockUI();//遮罩
					$("#formId").submit();
					break;
				default:
					//alert("<spring:message code="LB.Alert001" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert001' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
					);
			}
		}
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金    -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0901" /></li>
    <!-- 基金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1064" /></li>
    <!-- SMART FUND自動贖回設定     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1184" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!--SMART FUND自動贖回設定 -->
				<h2>
					<spring:message code="LB.W1184" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form autocomplete="off" method="post" id="formId" action="${__ctx}/FUND/ALTER/smart_fund_result">
					<c:set var="BaseResultData" value="${smart_fund_confirm.data}"></c:set>
					<input type="hidden" name="ADOPID" value="F002">
					<%--  TXTOKEN  防止重送代碼--%>
					<input type="hidden" name="TXTOKEN" id="TXTOKEN" value="${BaseResultData.TXTOKEN}" />
					<%-- 驗證相關 --%>
					<input type="hidden" id="PINNEW" name="PINNEW" value="">
	 				<%--回上一頁 --%>
					<input type="hidden" id="back" name="back" value="">
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								
								 <!--變更種類-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W1167" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
										   <span><spring:message code="LB.W1186" /></span>
										 </div>
									</span>
								</div>
								
								 <!--身分證字號/統一編號-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.Id_no" /></h4>
										</label></span>
									<span class="input-block">
											<div class="ttb-input">
												   <span>${ BaseResultData.hideid_CUSIDN}</span>
											</div>
									</span>
								</div>
								 <!--信託帳號-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0944" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${ BaseResultData.hideid_CDNO}</span>
										 </div>
									</span>
								</div>
								
								 <!--基金名稱-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0025" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
										<span>${ BaseResultData.FUNDLNAME}</span>	
										</div>
									</span>
								</div>
								
								 <!--類別-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.D0973" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${ BaseResultData.str_AC202}</span>	
										</div>
									</span>
								</div>
								
								 <!--信託金額-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0026" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
											<!-- 幣別 -->
											${BaseResultData.ADCCYNAME }
											<!--金額 -->
											${BaseResultData.FUNDAMT }
											</span>	
										</div>
									</span>
								</div>
								
								 <!--單位數-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0027" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
												${BaseResultData.display_ACUCOUNT }
											</span>	
										</div>
									</span>
								</div>
								
								 <!--未分配金額-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0921" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
											${BaseResultData.display_NAMT }
											</span>	
										</div>
									</span>
								</div>
								
								 <!--參考現值-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0915" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
											${BaseResultData.display_AMT }
											</span>	
										</div>
									</span>
								</div>
								
								 <!--基準報酬率-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W1195" /><spring:message code="LB.W1196" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
											${BaseResultData.RTNC}
											${BaseResultData.display_RTNRATE } %
											</span>	
										</div>
									</span>
								</div>
								
								 <!--參考淨值-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W1198" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${ BaseResultData.display_REFVALUE1}</span>	
										</div>
									</span>
								</div>
								
								 <!--參考匯率-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0030" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${ BaseResultData.display_FXRATE}</span>	
										</div>
									</span>
								</div>
								 <!--原停利贖回設定-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W1206" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${ BaseResultData.display_STOPPROF}</span>	
										</div>
									</span>
								</div>
								 <!--原停損贖回設定-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W1207" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${ BaseResultData.display_STOPLOSS}</span>	
										</div>
									</span>
								</div>
								
								 <!-- 自動贖回設定 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.X0418" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<span>
											${BaseResultData.display_NOOPT }
											</span>
										</div>
									</span>
								</div>
								
							<c:if test="${BaseResultData.NOOPT == ''}">
								 <!--自動贖回出場點設定(停利報酬率)-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W1210" />(<spring:message code="LB.W1211" />)</h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
										<c:if test="${BaseResultData.NSTOPPROF != ''}">
												+
										</c:if>
											<span>${ BaseResultData.NSTOPPROF}%</span>
											</div>
									</span>
								</div>  
								 <!--自動贖回出場點設定(停損報酬率)-->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W1210" />(<spring:message code="LB.X0419" />)</h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<c:if test="${BaseResultData.NSTOPLOSS != ''}">
												-
											</c:if>
											<span>${ BaseResultData.NSTOPLOSS}%</span>
										</div>
									</span>
								</div>  
							</c:if>
								<!-- 交易機制 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.Transaction_security_mechanism" />
									</label>
								</span>
								<span class="input-block">
									<!-- 交易密碼SSL -->
									<div class="ttb-input">
										<label class="radio-block">
											<spring:message code="LB.SSL_password" />
											<input type="radio" name="FGTXWAY" checked="checked" value="0">
											<span class="ttb-radio"></span>
										</label>
									</div>
									<!--請輸入密碼 -->
									<div class="ttb-input">
										<spring:message code="LB.Please_enter_password" var="pleaseEnterPin" />
										<input type="password" id="CMPASSWORD" name="CMPASSWORD" class="text-input validate[required]"
											maxlength="8" placeholder="${plassEntpin}">
									</div>
								</span>
							</div>
							</div>
							<!-- button -->
							<input class="ttb-button btn-flat-gray" name="CMBACK" id="CMBACK" type="button" value="<spring:message code="LB.X0420" />" />
							<input class="ttb-button btn-flat-gray" name="CMRESET" id="CMRESET" type="button" value="<spring:message code="LB.Re_enter" />" />
							<input class="ttb-button btn-flat-orange" name="CMSUBMIT" id="CMSUBMIT" type="button" value="<spring:message code="LB.Confirm" />" />
							<!-- button -->
						</div>
					</div>
				</form>
			</section>
			<!-- main-content END -->
		</main>
	</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>