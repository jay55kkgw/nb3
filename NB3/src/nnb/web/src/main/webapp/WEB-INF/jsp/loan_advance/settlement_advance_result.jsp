<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
	
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>	
</head>
 <body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>

	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 貸款服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Loan_Service" /></li>
    <!-- 借款查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Loan_Inquiry" /></li>
    <!-- 借款明細查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Loan_Detail_Inquiry" /></li>
    <!-- 貸款提前結清     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1721" /></li>
		</ol>
	</nav>

 	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
	<main class="col-12"> 
	<!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<h2><spring:message code="LB.W1721" /><!-- 貸款提前結清--></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<!-- 交易流程階段 -->
			<div id="step-bar">
				<ul>
					<li class="finished"><spring:message code="LB.Enter_data" /></li>
					<li class="finished"><spring:message code="LB.Confirm_data" /></li>
					<li class="active"><spring:message code="LB.Transaction_complete" /></li>
				</ul>
			</div>
			<c:if test="${ inputData.FGTXDATE.equals('1') }">						
				<p style="text-align: center;color: red;"><spring:message code="LB.W1718" /><!-- 還款成功 --></p>
			</c:if>
			<c:if test="${ !inputData.FGTXDATE.equals('1') }">						
				<p style="text-align: center;color: red;"><spring:message code="LB.W1719" /><!-- 預約還款成功 --></p>
			</c:if>
			<form method="post" id="formId">
				<input type="hidden" id="back" name="back" value="">
				<input type="hidden" id="TOKEN" name="TOKEN" value="${inputData.TOKEN}" /><!-- 防止重複交易 -->
                
				
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
						
							<!-- 交易種類 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.Trading_time" /><!-- 交易時間 -->
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>${ result_data.data.CMQTIME }</p>
									</div>
								</span>
							</div>
							<!-- 交易種類 -->
							<c:if test="${ !inputData.FGTXDATE.equals('1') }">
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.Transfer_date" /><!-- 轉帳日期 -->
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<p>${ inputData.PAYDATE }</p>
										</div>
									</span>
								</div>
							</c:if>
							<!-- 交易種類 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.Payers_account_no" /><!-- 轉出帳號 -->
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>${ inputData.ACN_OUT }</p>
									</div>
								</span>
							</div>
							<!-- 交易種類 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.W0891" /><!-- 貸款帳號 -->
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>${ inputData.ACN }</p>
									</div>
								</span>
							</div>
							<!-- 交易種類 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.W1707" /><!-- 貸款分號 -->
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>${ inputData.SEQ }</p>
									</div>
								</span>
							</div>
							<!-- 交易種類 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.Amount" /><!-- 轉帳金額 -->
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>${ inputData.PALPAY }</p>
									</div>
								</span>
							</div>
							<!-- 交易種類 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.Transfer_note" /><!-- 交易備註 -->
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>${ inputData.CMTRMEMO }</p>
									</div>
								</span>
							</div>
							
						</div>
                        <!-- 列印  -->
                        <spring:message code="LB.Print" var="printbtn"></spring:message>
                        <input type="button" id="printbtn" value="${printbtn}" class="ttb-button btn-flat-orange" />					
					</div>
				</div>
			</form>
		</section>
		<!-- 		main-content END -->
	</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
	<!--   Js function -->
    <script type="text/JavaScript">
		$(document).ready(function() {
			init();
		});
		
	    function init(){
	    	//列印
        	$("#printbtn").click(function(){
				var params = {
					"jspTemplateName":"settlement_advance_result_print",
					"jspTitle":'<spring:message code= "LB.W1721" />',
					"FGTXDATE":"${inputData.FGTXDATE}",
					"CMQTIME":"${result_data.data.CMQTIME}",
					"PAYDATE":"${inputData.PAYDATE}",
					"ACN_OUT":"${inputData.ACN_OUT}",
					"ACN":"${inputData.ACN}",
					"SEQ":"${inputData.SEQ}",
					"PALPAY":"${inputData.PALPAY}",
					"CMTRMEMO":"${inputData.CMTRMEMO}"
				};
				openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
	    }	
	    

 	</script>
</body>
</html>
 
