<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
	    <script type="text/javascript">
	    $(document).ready(function () {
            initFootable(); // 將.table變更為footable 
            init();
        });

        function init() {
	    	//列印
        	$("#print").click(function(){
				var params = {
					"jspTemplateName":"card_sign_print",
					"jspTitle":'<spring:message code= "LB.B0004" />'
				};
				openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
	    	$("#pageshow").click(function(e){			
	        	initBlockUI();

				var action = "${__ctx}/CREDIT/SIGN/card_sign_apply_confirm";
				$("form").attr("action", action);
    			$("form").submit();
			});
	    	$("#back").click(function(e){			
	        	initBlockUI();
				var action = '${__ctx}/CREDIT/SIGN/card_sign_cancle_confirm';
				$("form").attr("action", action);
    			$("form").submit();
			});
        }
    </script>
    <style>
    	.DataCell{
    	    text-align: left;
    	}
    	.ColorCell{
    		width:7%;
    	}
    </style>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 簽署信用卡分期付款同意書     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.B0005" /></li>
		</ol>
	</nav>


	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!-- 預約黃金交易查詢/取消 -->
					<!-- <spring:message code="LB.NTD_Demand_Deposit_Detail" /> -->
					<spring:message code="LB.B0005"/>
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>						
				
                <form id="formId" method="post">
					<input type="hidden" name="action" value="forward">
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
							<div class="NA01_3">
								<div class="head-line"><spring:message code="LB.B0004"/></div>
							</div>
							<br>
							<p align="left"><font size="3"><b>下列為申請信用卡分期付款應遵守之約定事項內容，如您已充分審閱並瞭解且願意確實遵守本約定事項，請按【同意並繼續】鍵，以完成信用卡分期付款同意書簽署作業。</b></font></p>
							<br>
							<table width="100%" class="DataBorder"  style='font-family:標楷體'>
								<tr>
									<td colspan=2 class="DataCell">
                    					<p>持卡人茲向臺灣中小企業銀行<span lang=EN-US>(</span>以下簡稱本行<span lang=EN-US>)</span>申請信用卡分期付款功能，除遵守本行信用卡約定條款外，並同意下列約定事項：</p>   
									</td>
								</tr>
								<tr>
						        	<td class="ColorCell">一、</td>
						        	<td class="DataCell">持卡人簽署本同意書後，分期付款仍尚未正式成立，須透過本行申請分期付款之通路確認執行各項分期交易，始得正式成立。</td>
						  		<tr>
								<tr>
						        	<td class="ColorCell">二、</td>
						        	<td class="DataCell">本行分期付款分為單筆分期及帳單分期，限本行信用卡正卡持卡人申請，正、附卡之消費均可申請<span lang=EN-US>(</span>採購卡及簽帳金融卡之消費不適用<span lang=EN-US>)</span>，惟分期總金額不得超過永久信用額度。</td>
						  		<tr>
								<tr>
						        	<td class="ColorCell">三、</td>
						        	<td class="DataCell">申請分期金額以新臺幣為單位，單筆分期須全額申請；帳單分期申請金額為當期信用卡帳單應繳總金額扣除最低應繳金額之餘額。</td>
						  		<tr>
								<tr>
						        	<td class="ColorCell">四、</td>
						        	<td class="DataCell"><b>分期期數及收費方式：<span lang=EN-US>(</span>一<span lang=EN-US>)</span>期數：3~30期&nbsp;&nbsp;<span lang=EN-US>(</span>二<span lang=EN-US>)</span>收費方式：分期年利率0%~15%，免收手續費，故總費用百分率即分期年利率，計算基準日為110年7月9日。本行分期年百分率係按主管機關備查之標準計算範例予以計算，實際條件仍以本行提供之內容為準。</b></td>
						  		<tr>
								<tr>
						        	<td class="ColorCell">五、</td>
						        	<td class="DataCell">每期應繳金額
				                        <ul>
				                          <li style="font-size: 0;">
				                            <span style="display: inline-block; vertical-align: top; font-size: 16px; width: 30px;">(一)&nbsp;</span>
				                            <span style="display: inline-block; vertical-align: top; font-size: 16px; width: calc(100% - 30px);">每期應繳金額以持卡人申請分期金額及分期期數按每個月為一期平均攤還，平均後無法整除之餘數將併入首期，分期利息則按分期剩餘本金依約定年利率計收。若有溢繳情形，則該溢繳金額將列入次期信用卡帳單抵扣應繳金額，不會提前清償未到期之分期本金。</span>
				                          </li>
				                          <li style="font-size: 0;">
				                            <span style="display: inline-block; vertical-align: top; font-size: 16px; width: 30px;">(二)&nbsp;</span>
				                            <span style="display: inline-block; vertical-align: top; font-size: 16px; width: calc(100% - 30px);">每期應繳金額將併入當期信用卡帳單之最低應繳金額，倘持卡人未繳足當期最低應繳金額，本行得依信用卡約定條款收取違約金，並自當期帳單繳款截止日起依持卡人當期適用循環信用利率計收未清償分期本金之遲延利息。</span>
				                          </li>
				                          <li style="font-size: 0;">
				                            <span style="display: inline-block; vertical-align: top; font-size: 16px; width: 30px;">(三)&nbsp;</span>
				                            <span style="display: inline-block; vertical-align: top; font-size: 16px; width: calc(100% - 30px);">持卡人若發生信用卡延滯繳款或其他違反本行信用卡約定條款之情事，本行得將未到期之分期本金全數列於次期帳單，持卡人應立即清償。</span>
				                          </li>
				                        </ul>
				                    </td>
						  		<tr>
								<tr>
						        	<td class="ColorCell">六、</td>
						        	<td class="DataCell">持卡人申請分期成功後，可於七日內來電取消，無須負擔任何費用。惟不得於分期繳款期間申請變更分期期數及信用卡帳單結帳日。</td>
						  		<tr>
								<tr>
						        	<td class="ColorCell">七、</td>
						        	<td class="DataCell">持卡人可申請提前清償，本行免收違約金，並將未到期分期本金一次入帳且併入次期信用卡帳單。惟仍須繳納當期分期利息，且已繳付之利息不予退還。</td>
						  		<tr>
								<tr>
						        	<td class="ColorCell">八、</td>
						        	<td class="DataCell">本同意書內容如有調整請以本行官網公告為準。其餘未約定事項，悉依本行信用卡約定條款辦理。本行保留核准各筆分期金額、期數及利率之權利。</td>
						  		<tr>
							</table>
							<p class=MsoNormal align=center style='text-align:center'>
								<b><span style='font-size:10.0pt;font-family:標楷體'>★</span></b><b><span >提醒您！本同意書簽署後分期交易尚未成立，仍須電洽<span lang=EN-US>(02)2357-7171</span>辦理。</span></b>
							</p>						
	                        <!--button 區域 -->
	                        <input id="print" name="print" type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Print"/>" />
	                        <input id="pageshow" name="pageshow" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.W1554"/>"/>
	                        
                        <!--                     button 區域 -->
                        </div>  
                    </div>
                </form>
<!-- 				<div class="text-left"> -->
<!-- 				    		說明： -->
<%-- 					<spring:message code="LB.Description_of_page"/>: --%>
<!-- 				    <ol class="list-decimal text-left"> -->
<!-- 				        <li>您所提供的身分證資料，經本行查詢後，如有疑義，本行得暫停您的黃金存摺網路銀行功能。 </li> -->
<!--           				<li>.線上申請黃金存摺帳戶手續費優惠為新台幣50元。 </li> -->
<!-- 				    </ol> -->
<!-- 				</div> -->

	
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>