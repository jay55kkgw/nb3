<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp"%>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
	
	<script type="text/javascript">
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 50);
			// 開始跑下拉選單並完成畫面
			setTimeout("init()", 400);
			// 初始化驗證碼
			setTimeout("initKapImg()", 200);
			// 生成驗證碼
			setTimeout("newKapImg()", 300);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
			// 初始化時隱藏span
			$("#hideblock").hide();
		});

		function init() {		
			// 表單驗證初始化
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
			// 確認鍵 click
			goOn();
			// 判斷顯不顯示驗證碼
			chaBlock();
			// 交易機制 click
			fgtxwayClick();
		}
		
		
		// 確認鍵 Click
		function goOn() {
			$("#CMSUBMIT").click( function(e) {
				// 送出進表單驗證前將span顯示
				$("#hideblock").show();
				
				console.log("submit~~");
	
				// 表單驗證
				if ( !$('#formId').validationEngine('validate') ) {
					e.preventDefault();
				} else {
					// 解除表單驗證
					$("#formId").validationEngine('detach');
					
					// 通過表單驗證
					processQuery();
// TEST
// $("#formId").submit();
				}
			});
		}
		
		// 通過表單驗證準備送出
		function processQuery(){
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
			// 交易機制選項
			switch(fgtxway) {
				case '2':
					// 晶片金融卡
					var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
					useCardReader(capUri);
					
			    	break;
				default:
					//alert("<spring:message code= "LB.Alert001" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert001' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			}
		}
		
		
		// 使用者選擇晶片金融卡要顯示驗證碼區塊
	 	function fgtxwayClick() {
	 		$('input[name="FGTXWAY"]').change(function(event) {
	 			// 判斷交易機制決定顯不顯示驗證碼區塊
	 			chaBlock();
			});
		}
	 	// 判斷交易機制決定顯不顯示驗證碼區塊
		function chaBlock() {
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
 			console.log("fgtxway: " + fgtxway);
			// 交易機制選項
			if(fgtxway == '2'){
				// 若為晶片金融卡才顯示驗證碼欄位
				$("#chaBlock").show();
			}else{
				// 若非晶片金融卡則隱藏驗證碼欄位
				$("#chaBlock").hide();
			}
	 	}

		// 驗證碼刷新
		function changeCode() {
			$('input[name="capCode"]').val('');
			// 大小版驗證碼用同一個
			console.log("changeCode...");
			$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

			// 登入失敗解遮罩
			unBlockUI(initBlockId);
		}
		
		// 初始化驗證碼
		function initKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
		}
		
		// 生成驗證碼
		function newKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').click(function() {
				$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
			});
		}
		
</script>

</head>
<body>

	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 轉帳功能申請/取消     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0327" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 快速選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code="LB.X0327"/></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="${__ctx}/ONLINE/SERVING/financial_card_apply_result">
				<!-- 交易機制所需欄位 -->
				<input type="hidden" id="jsondc" name="jsondc" value="">
				<input type="hidden" id="ISSUER" name="ISSUER" value="">
				<input type="hidden" id="ACNNO" name="ACNNO" value="">
				<input type="hidden" id="TRMID" name="TRMID" value="">
				<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
				<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
				<input type="hidden" id="TAC" name="TAC" value="">
				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
				<input type="hidden" id="TYPE" name="TYPE" value="${transfer_data.data.TYPE}">
			
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
							<!-- 類別 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0973" /></h4>
								</label>
								</span> 
								<span> 
									<label>
										<c:if test="${transfer_data.data.TYPE == '01'}">
											<spring:message code="LB.Apply" />
										</c:if>
										<c:if test="${transfer_data.data.TYPE == '02'}">
											<spring:message code="LB.Cancel" />
										</c:if>
									</label>
								</span>
							</div>
							
							<!-- 交易機制 -->						
							<div class="ttb-input-item row">

								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Transaction_security_mechanism" /></h4>
									</label>
								</span> 
								<!-- 交易機制選項 -->
								<span class="input-block">
									<!-- 晶片金融卡 -->
									<div class="ttb-input">
										<label class="radio-block">
											<spring:message code="LB.Financial_debit_card" />
											<input type="radio" name="FGTXWAY" id="CMCARD" value="2" checked="checked">
											<span class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>	<!-- ttb-input-item row -->
							
							<!-- 如果是非約定則不能使用IKEY，只能選晶片金融卡，使用者選擇晶片金融卡後才顯示驗證碼-->
							<div class="ttb-input-item row" id="chaBlock" style="display:none">
								<!-- 驗證碼 -->
								<span class="input-title">
									<label><h4><spring:message code="LB.Captcha" /></h4></label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<spring:message code="LB.Captcha" var="labelCapCode" />
										<img name="kaptchaImage" class="verification-img" src="" />
										<input type="button" name="reshow" class="ttb-sm-btn btn-flat-orange"
											onclick="changeCode()" value="<spring:message code="LB.Regeneration_1" />" />
									</div>
									<div class="ttb-input">
										<input id="capCode" name="capCode" type="text"
											class="text-input" maxlength="6" autocomplete="off">
										<br>
										<span class="input-unit">
											<font color="#FF0000">
												<!-- ※英文不分大小寫，限半型字 -->
												<spring:message code="LB.Captcha_refence" />
											</font>
										</span>
									</div>
								</span>
							</div>
								
								<!-- 不在畫面上顯示的span -->
<!-- 								<span id="hideblock" class="ttb-input"> -->
<!-- 									驗證用的input -->
<!-- 									<input id="ErrorMsg" name="ErrorMsg" type="text" class="text-input validate[groupRequired[FGTXWAY]]"  -->
<!-- 										style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" value="" /> -->
<!-- 								</span> -->

						</div>
						<input id="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
					</div>	
				</div>
			</form>
		</section>
		</main>
		<!-- main-content END -->
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>