<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript"
	src="${__ctx}/component/windows/IKey/ikeyTopWebSocketUtil.js"></script>
<script type="text/javascript"
	src="${__ctx}/component/windows/IKey/IKeyMethod.js"></script>
<script type="text/javascript"
	src="${__ctx}/component/windows/noneIE/noneIECard.js"></script>
<script type="text/javascript"
	src="${__ctx}/component/windows/noneIE/topWebSocketUtil.js"></script>
<script type="text/javascript" src="${__ctx}/keyboard/js/plugins.js"></script>
</head>
<script type="text/javascript">
	var isIkeyUser; // 使用者是否ikey使用者
	var comVersion; // 各元件的最新版本号
	var platformNew; // 作业系统
	var hasAskDownload = false; // 因为现在WINDOW多浏览器元件的安装档整合成一个，所以要多一个变数，只询问使用者下载一次

	$(document).ready(function() {
		// HTML载入完成后开始遮罩
		setTimeout("initBlockUI()", 10);
		// 开始查询资料并完成画面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});

	function init() {
		$("#CMBACK").click(function(e) {
			$("#formId").attr("action", "${__ctx}/login");
			$("#formId").submit();
		});
		$("#win").click(function(e){
			jQuery("#componentPath").val("win");
			jQuery("#componentForm").submit();
		});
		$("#mac").click(function(e){
			jQuery("#componentPath").val("mac");
			jQuery("#componentForm").submit();
		});
		component_version(); // 取得各元件的最新版本号
		component_platform(); // 取得装置作业系统
		if(platformNew.indexOf("Win") > -1){
			$("#winos").show();
		}
		else{
			$("#macos").show();
		}
		
		// 元件逻辑 (进画面后延迟提示，避免影响其他资源载入)
		setTimeout(component_init(), 1500);
	}

	// 取得各元件的最新版本号
	function component_version() {
		var uri = '${__ctx}' + "/COMPONENT/component_version_aj";
		var bs = fstop.getServerDataEx(uri, null, false);
		console.log("comVersion_bs: " + JSON.stringify(bs));

		comVersion = bs.data;
		console.log("comVersion: " + JSON.stringify(comVersion));
	}

	// 判断装置
	function checkBrowser() {
		var checkBrowserResult = false;
		if (navigator.userAgent.match(/Android/i)
				|| navigator.userAgent.match(/webOS/i)
				|| navigator.userAgent.match(/iPhone/i)
				|| navigator.userAgent.match(/iPad/i)
				|| navigator.userAgent.match(/iPod/i)
				|| navigator.userAgent.match(/BlackBerry/i)
				|| navigator.userAgent.match(/Windows Phone/i)) {
			// 行动装置
			console.log("component checkBrowser not pass!!!");
		} else {
			// 可使用元件之装置
			checkBrowserResult = true;
			console.log("component checkBrowser pass...");
		}

		console.log("checkBrowser: " + checkBrowserResult);

		return checkBrowserResult;
	}

	// 取得装置作业系统
	function component_platform() {
		platformNew = navigator.platform;
		if (window.console) {
			console.log("component_platform: " + platformNew);
		}
	}

	// 元件初始化
	function component_init() {
		// 行动装置不能做，所以要先判断
		if (checkBrowser()) {
			// 改在此页面载入时引入
			// $.loadScript(
			// '${__ctx}/component/combo/topWebSocketUtil.js', function() {
			// console.log("loading...topWebSocketUtil.js");
			// });

			topWebSocketUtil.setWssUrl("wss://localhost:9203/",
					ValidateLegalURL_Callback_Both);
		}
	}

	// 底下必须
	//------------------------------------------------ ------------------------------
	// 底下必须有，这是第一关
	// 等到 ValidateLegalURL_Callback 回呼方法被呼叫后，
	// 再经由 topWebSocketUtil.invokeRpcDispatcher 呼叫元件方法
	//------------------------------------------------ ------------------------------
	// Name: ValidateLegalURL_Callback
	function ValidateLegalURL_Callback_Both(rpcStatus, rpcReturn) {
		try {
			//------------------------------------------------ ------------------
			topWebSocketUtil.tryRpcStatus(rpcStatus);
			//------------------------------------------------ ------------------

			// 未侦测到元件(未安装或关闭)
			if (rpcReturn == "E_Send_11_OnError_1006") {
				hasAskDownload = true;
				$("#winstatus").text("未安装");
				$("#macstatus").text("未安装");

			} // 安装完成,检查更新
			else if (rpcReturn == "E000" || rpcReturn == null) {
				rpcName = "GetVersion";
				topWebSocketUtil.invokeRpcDispatcher(GetVersion_Callback,
						rpcName);

				// GetVersion_Callback
				function GetVersion_Callback(rpcStatus, rpcReturn) {
					if (window.console) {
						console.log("GetVersion_Callback...");
					}
					try {
						topWebSocketUtil.tryRpcStatus(rpcStatus);

						var currentVersion = rpcReturn;
						if (window.console) {
							console.log("currentVersion: " + currentVersion);
						}
						if (window.console) {
							console.log("newVersion: "
									+ comVersion.NoneIEReaderVersion);
						}

						var currentVersionNO = parseInt(currentVersion.replace(
								/\./g, ""));
                        var newVersionNO;
                        //依系統取得版本號20210312因元件版本號不同修正
                        if(platformNew.indexOf("Win") > -1){
                            newVersionNO = parseInt(comVersion.NoneIEReaderVersion.replace(/\./g,""));
                        }
                        else{
                            newVersionNO = parseInt(comVersion.MacReaderVersion.replace(/\./g,""));
                        } 
						if (window.console) {
							console.log("currentVersionNO=" + currentVersionNO);
						}
						if (window.console) {
							console.log("newVersionNO=" + newVersionNO);
						}

						// 元件版本需要更新
						if (currentVersionNO < newVersionNO) {
							if (platformNew.indexOf("Win") > -1) {
								$("#winstatus").text("版本需要更新");
								$("#macstatus").text("未安装");
							} else {
								$("#winstatus").text("未安装");
								$("#macstatus").text("版本需要更新");
							}
						} else {
							if (platformNew.indexOf("Win") > -1) {
								$("#winstatus").text("已安装");
								$("#macstatus").text("未安装");
							} else {
								$("#winstatus").text("未安装");
								$("#macstatus").text("已安装");
							}
						}
					} catch (exception) {
						if (window.console) {
							console.log("exception=" + exception);
						}
						//alert("<spring:message code= "LB.X1655" />");
						errorBlock(
								null, 
								null,
								["<spring:message code= 'LB.X1655' />"], 
								'<spring:message code= "LB.Quit" />', 
								null
							);
					}
				}
			}

		} catch (exception) {
			alert("exception: " + exception);
		}
	}
</script>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	<!-- 面包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i
					class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><a href="#">组件下载</a></li>
		</ol>
	</nav>

	<!-- 左边menu 及登入资讯 -->
	<div class="content row">
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主页内容 -->
			<h2>组件下载</h2>
			<form id="formId" method="post" action="">
				<div class="card-block shadow-box terms-pup-blcok">
					<div class="col-12 tab-content">
						<div class="ttb-input-block tab-pane fade show active"
							id="nav-trans-1" role="tabpanel" aria-labelledby="nav-home-tab">
							<div class="card-select-block"></div>
							<div class="card-detail-block">
								<div class="card-center-block">
									<!-- Q1 -->
									<div id="winos" class="ttb-pup-block" role="tab" style="display:none">
										<a role="button" class="ttb-pup-title collapsed d-block"
											data-toggle="collapse" href="#popup-1" aria-expanded="true"
											aria-controls="popup-1 ">
											<div class="row">
												<span class="col-1">Q1</span>
												<div class="col-11 row">
													<span class="col-12">一般网银、网络ATM及电子签章元件下载</span>
												</div>
											</div>
										</a>
										<div id="popup-1" class="ttb-pup-collapse collapse"
											role="tabpanel">
											<div class="ttb-pup-body">
												<ul class="ttb-pup-list">
													<li>
														<p>Windows平台</p>
														<table class="ttb-pup-table">
															<thead>
																<tr>
																	<th>支援浏览器</th>
																	<th>浏览器元件</th>
																	<th>安装状态</th>
																	<th>浏览器元件下载</th>
																	<th>安装说明手册</th>
																</tr>
															<tbody>
																<tr>
																	<td>IE、Edge、Chrome、FireFox</td>
																	<td>二合一元件</td>
																	<td><font id="winstatus">检查中...</font></td>
																	<td><input type="button" id="win" class="component-download-link" value="点此下载" /></td>
																	<td><a style="color: #007bff" download href="${__ctx}/component/operation_manual/NBComponentOperateBook.pdf">点此下载</a></td>
																</tr>
															</tbody>
														</table>
													</li>
													<li>
														<p>注意事项</p>
														<ol style="list-style-type: circle; margin-left: 1rem;">
															<li><p>若您有安装防毒软体或防火墙，请务必在软体中开启浏览器元件的安装权限。</p></li>
															<li><p>若您有安装防火墙，请开启Port 9203
																	供晶片金融卡元件及电子签章元件使用。</p></li>
															<li><p>浏览器与浏览器元件的安装顺序：下载并安装浏览器 >> 下载浏览器元件 >>
																	安装浏览器元件</p></li>
														</ol>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<!-- Q1 -->
									<div id="macos" class="ttb-pup-block" role="tab" style="display:none">
										<a role="button" class="ttb-pup-title collapsed d-block"
											data-toggle="collapse" href="#popup-2" aria-expanded="true"
											aria-controls="popup-2 ">
											<div class="row">
												<span class="col-1">Q1</span>
												<div class="col-11 row">
													<span class="col-12">一般网银、网络ATM及电子签章元件下载</span>
												</div>
											</div>
										</a>
										<div id="popup-2" class="ttb-pup-collapse collapse"
											role="tabpanel">
											<div class="ttb-pup-body">
												<ul class="ttb-pup-list">
													<li>
														<p>Mac平台</p>
														<table class="ttb-pup-table">
															<thead>
																<tr>
																	<th>支援浏览器</th>
																	<th>浏览器元件</th>
																	<th>安装状态</th>
																	<th>浏览器元件下载</th>
																	<th>安装说明手册</th>
																</tr>
															<tbody>
																<tr>
																	<td>Safari、Chrome、FireFox</td>
																	<td>二合一元件</td>
																	<td><font id="macstatus">检查中...</font></td>
																	<td><input type="button" id="mac" class="component-download-link" value="点此下载" /></td>
																	<td><a style="color: #007bff" download href="${__ctx}/component/operation_manual/NBComponentOperateBook.pdf">点此下载</a></td>
																</tr>
															</tbody>
														</table>
													</li>
													<li>
														<p>注意事项</p>
														<ol style="list-style-type: circle; margin-left: 1rem;">
															<li><p>若您有安装防毒软体或防火墙，请务必在软体中开启浏览器元件的安装权限。</p></li>
															<li><p>若您有安装防火墙，请开启Port 9203
																	供晶片金融卡元件及电子签章元件使用。</p></li>
															<li><p>浏览器与浏览器元件的安装顺序：下载并安装浏览器 >> 下载浏览器元件 >>
																	安装浏览器元件</p></li>
														</ol>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<!-- Q2 -->
									<div class="ttb-pup-block" role="tab">
										<a role="button" class="ttb-pup-title collapsed d-block"
											data-toggle="collapse" href="#popup-4" aria-expanded="true"
											aria-controls="popup-4 ">
											<div class="row">
												<span class="col-1">Q2</span>
												<div class="col-10 row">
													<span class="col-12">自然人凭证元件下载</span>
												</div>
											</div>
										</a>
										<div id="popup-4" class="ttb-pup-collapse collapse"
											role="tabpanel">
											<div class="ttb-pup-body">
												<ul class="ttb-pup-list">
													<li>
														<p>
															<a style="color: #007bff" href="${__ctx}/component/windows/natural/TBBankPkcs11ATL.exe">点此下载</a>
														</p>
													</li>
													<li>
														<p>注意事项</p>
														<ol style="list-style-type: circle; margin-left: 1rem;">
															<li><p>本元件仅适用Windows平台。</p></li>
														</ol>
													</li>
												</ul>
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>
						<input type="BUTTON" class="ttb-button btn-flat-gray" value="返回" name="CMBACK" id="CMBACK">
					</div>
				</div>
			</form>
			<form id="componentForm" action="${__ctx}/COMPONENT/component_download" method="post">
				<input type="hidden" name="componentPath" id="componentPath" />
				<input type="hidden" name="trancode" value="ComponentDownload" />
			</form>	
		</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>

</body>
</html>