<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
	    <script type="text/javascript">
	    $(document).ready(function () {
            initFootable(); // 將.table變更為footable 
            init();
        });

        function init() {
	    	$("#pageshow").click(function(e){			
	        	initBlockUI();

				var action = "${__ctx}/PARKING/FEE/parking_withholding_apply_confirm";
				$("form").attr("action", action);
    			$("form").submit();
			});
	    	$("#back").click(function(e){			
	        	initBlockUI();
				var action = '${__ctx}/PARKING/FEE/parking_withholding';
				$("form").attr("action", action);
    			$("form").submit();
			});
        }
    </script>
    <style>
    	.DataCell{
    	    text-align: left;
    	}
    </style>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 自動扣繳服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2360" /></li>
    <!-- 停車費     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.D0330" /></li>
    <!-- 網路授權自動扣款繳納新北市路邊停車費約定條款     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X1160" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!-- 預約黃金交易查詢/取消 -->
					<!-- <spring:message code="LB.NTD_Demand_Deposit_Detail" /> -->
					<spring:message code="LB.X1160"/>
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>						
				
                <form id="formId" method="post">
					<input type="hidden" name="action" value="forward">
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
							<div class="NA01_3">
								<div class="head-line"><spring:message code="LB.X1160"/></div>
							</div>
							
                           	<table width="100%" class="DataBorder"> 

								<tr>
						
						        		<td class="ColorCell" style="width:6em">第一條</td>
						
						       		    <td class="DataCell">立約人(以下簡稱本人)同意遵守臺灣中小企業銀行（以下簡稱貴行）網路銀行線上授權自動扣款繳納新北市政府路邊停車管理中心路邊停車費之服務規範，並同意授權貴行依照新北市政府路邊停車管理中心之扣款指示，逕自本人指定授權扣款帳號扣繳新北市路邊停車費。</td>
						
						        </tr>
						
						      	<tr>
						
						        		<td class="ColorCell">第二條</td>
						
						        		<td class="DataCell">本人指定於貴行開立之活期性存款帳戶為授權扣款帳號，並且同意將車號、電子郵件位址（e-mail）等資料登錄在貴行，供相關待繳費停車單資料之查詢及代扣停車費，並以貴行接收新北市政府路邊停車管理中心回傳資料後視為登錄成功與否之準據，貴行應以電子郵件通知本人，生效日期後之應繳停車單據方始扣款。如本人電子信箱錯誤、傳輸失敗等各項因素，致未收到生效通知郵件時，概以貴行網路銀行、網路ATM提供查詢之資料為準。</td>
						
						        </tr>
						
							  	<tr>
						
						        		<td class="ColorCell">第三條</td>
						
						        		<td class="DataCell">貴行代收新北市路邊停車費，不論扣款成功與否均應以本人登錄之電子信箱通知本人，如本人電子信箱錯誤、傳輸失敗，致未收到扣款成功或失敗通知，概由本人自行於貴行網路銀行、網路ATM提供之查詢功能查詢，貴行無通知義務。</td>
						
						        </tr>
						
							  	<tr>
						
						        		<td class="ColorCell">第四條</td>
						
						        		<td class="DataCell">本人所指定之授權扣款帳號應隨時保持可供扣繳之存款，在貴行扣款之時點如發生存款不足扣繳待繳之停車費時，則貴行對該筆停車費無扣繳義務，倘因而致本人發生任何損失（如逾期未繳而遭罰款），概由本人自行負責。</td>
						
						        </tr>
						
							  	<tr>
						
						        		<td class="ColorCell">第五條</td>
						
						        		<td class="DataCell">本項網路授權自動扣款繳納新北市政府路邊停車管理中心路邊停車費之扣款作業程序，採事先約定登錄車號（不限本人車號），並自指定授權扣款帳戶批次扣款方式辦理；如有車號等資料，因本人登錄錯誤致發生誤扣、無法轉帳等糾紛或損害等情形時，除可歸責於貴行者外，概由本人負責處理，與貴行無涉。</td>
						
						       	</tr>
						
							  	<tr>
						
						        		<td class="ColorCell">第六條</td>
						
						        		<td class="DataCell">本人所指定之授權扣款帳號，在貴行扣款之時點，如本人有其他應扣、待扣款項，貴行可自行選擇扣款順序，如因扣取路邊停車費後，導致其他應扣取之各種款項不足扣時，概由本人自行負責。</td>
						
						       	</tr>
						
							  	<tr>
						
						        		<td class="ColorCell">第七條</td>
						
						       		    <td class="DataCell">本人如欲變更授權扣款帳號、註銷約定代繳之車號，應於貴行網路銀行或網路ATM自行變更或註銷，無須再以書面通知貴行。終止約定代繳之車號，其生效期間同約定條款第二條。</td>
						
						       	</tr>
						
							  	<tr>
						
						        		<td class="ColorCell">第八條</td>
						
						        		<td class="DataCell">本人如有重複繳納停車費，以貴行接收新北市政府路邊停車管理中心回傳銷帳結果為重複繳納者，由貴行自動退費至扣款帳號；其他情形之重複繳納或對於繳款內容有所疑義時，應直接向新北市政府路邊停車管理中心查詢或洽辦相關退費事宜。</td>
						
						        </tr>
						
							  	<tr>
						
						        		<td class="ColorCell">第九條</td>
						
						       		    <td class="DataCell">指定授權扣款帳號結清或遭法院強制執行或其他不可歸責於貴行之事由，致無法扣款轉帳代繳時，貴行無須查明原因即得自行決定不予扣繳，因此發生之一切損失，概由本人自行負責。</td>
						
						        </tr>
						
							  	<tr>
						
						        		<td class="ColorCell">第十條</td>
						
						        		<td class="DataCell">本人利用本項服務，如因電子訊息受不可抗力事由或其他原因（包括但不限於斷電、斷線、電信壅塞、網路傳輸干擾、貴行電腦系統故障或第三人破壞等），致使網路授權扣款作業無法如期辦理時，本人同意依新北市政府路邊停車管理中心所指定之人工繳費方式自行繳納處理。</td>
						
						        </tr>
						
							  	<tr>
						
						        		<td class="ColorCell">第十一條</td>
						
						        		<td class="DataCell">本人使用本項服務以處理停車費資料為限，如企圖利用本項業務處理他人資料或有不良使用紀錄或有任何破壞、不當或有任何不法情事之虞時，貴行得隨時取消本人之使用資格。</td>
						
						        </tr>
						
							  	<tr>
						
						        		<td class="ColorCell">第十二條</td>
						
						        		<td class="DataCell">貴行得視需要隨時修訂或終止本約定條款之內容，並經電子郵件告知或於貴行網站公告即可，本人絕不以未收到電子郵件或電子信箱變更或未上網點閱等事由，主張任何要求。</td>
						
							  	</tr>
						
							  	<tr>
						
						        		<td class="ColorCell">第十三條</td>
						
						        		<td class="DataCell">本約定條款如有未盡事宜，悉依照貴行相關業務規定及一般金融機構慣例辦理。</td>
						
							  	</tr>
						
							  	<tr>
						
						        		<td class="ColorCell">第十四條</td>
						
						        		<td class="DataCell">因本約定所生之一切爭議，雙方同意以臺灣臺北地方法院為第一審管轄法院。</td>
						
							  	</tr>
						
							 </table>
	                        <!--button 區域 -->
	                        <input id="back" name="back" type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Cancel"/>" />
	                        <input id="pageshow" name="pageshow" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.W1554"/>"/>
	                        
                        <!--                     button 區域 -->
                        </div>  
                    </div>
                </form>
<!-- 				<div class="text-left"> -->
<!-- 				    		說明： -->
<%-- 					<spring:message code="LB.Description_of_page"/>: --%>
<!-- 				    <ol class="list-decimal text-left"> -->
<!-- 				        <li>您所提供的身分證資料，經本行查詢後，如有疑義，本行得暫停您的黃金存摺網路銀行功能。 </li> -->
<!--           				<li>.線上申請黃金存摺帳戶手續費優惠為新台幣50元。 </li> -->
<!-- 				    </ol> -->
<!-- 				</div> -->

	
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>