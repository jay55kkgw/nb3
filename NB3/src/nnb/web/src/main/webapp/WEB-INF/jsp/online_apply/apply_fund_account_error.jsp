<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="bs" value="${error.data}"></c:set>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<script type="text/javascript">
	var action = '${__ctx}';
	$(document).ready(function() {
		init();
	});
	
	function init() {
		
		$("#next").click(function() {
			console.log("next action>>" + action);
			action = action + '${error.next}'
			submitForm(action);
		});
		
		// 重新輸入
		previous();
		
		$("#forward").click(function() {
			action = action + '${bs.URL }';
			submitForm(action);
		});
		
		// 關閉視窗
		closePage();
	}
	
	
	// 重新輸入
	function previous() {
		$("#PREVIOUS").click(function() {
			initBlockUI();
			console.log("previous action>>" + action);
			action = action + '${error.previous}'
			$("form").attr("action", action);
			
			$("form").append('<input type="hidden" name="TYPE" value="${PREVIOUS_TYPE }" />');
			$("form").submit();
		});			
	}
	
	
	// 關閉視窗
	function closePage() {
		$("#CLOSEPAGE").click( function(e) {
			window.close();
		});			
	}
	
	
	function submitForm(action) 
	{
		initBlockUI();
		$("form").attr("action", action);
		// 讓 online_apply 判斷導向哪支 Controller
		$("form").append('<input type="hidden" name="TYPE" value="${TYPE}" />');
		$("form").submit();
	}
	
</script>
</head>
	<body>
		<header>
			<%@ include file="../index/header_logout.jsp"%>
		</header>
	 
	    <!-- 麵包屑     -->
		<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
			<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
				<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
	    <!-- 線上申請     -->
				<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0847" /></li>
			</ol>
		</nav>
		<main class="col-12">
		<!-- 主頁內容  --> 
			<section id="main-content" class="container">
				<h2><spring:message code="LB.Transaction_result" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="">
					
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<ul class="ttb-result-list">
								<li>
									<h3><spring:message code="LB.Transaction_code" /></h3>
									<p>${error.msgCode}</p>
								</li>
								<li>
									<h3><spring:message code="LB.Transaction_message" /></h3>
									<p>${error.message}
									<br />
									
									<c:choose>
										<c:when test="${error.msgCode eq 'Z611'}">
											<spring:message code= "LB.X1720" />
											<font color=red><a id="forward" href="#">【<spring:message code= "LB.X1087" />】</a></font><!-- 登入網路銀行 -->
										</c:when>
										<c:when test="${error.msgCode eq 'Z612'}">
											<spring:message code= "LB.X0329" /><!-- 未申請網路銀行或者是以信用卡申請網路銀行之客戶請點選下述連結線上升級網路銀行及開通轉帳功能 -->
											<font color=red><a id="forward" href="#">【<spring:message code= "LB.X0305" />】</a></font><!-- 使用臺灣企銀晶片金融卡 + 讀卡機申請網路銀行 -->
										</c:when>
										<c:when test="${error.msgCode eq 'Z613'}">
											<spring:message code= "LB.X1166" /><!-- 請線上預約開立存款戶，請點選下述連結申請 -->
											<font color=red><a id="forward" href="#">【<spring:message code= "LB.D1100" />】</a></font><!-- 線上預約開立存款戶 -->
										</c:when>
									</c:choose>
									</p>
								</li>
							</ul>
							
							<!-- 重新輸入 -->
							<button type="button" id="PREVIOUS" class="ttb-button btn-flat-orange">
								<spring:message code= "LB.Re_enter" />
							</button>
							<!-- 關閉視窗 -->
							<button type="button" id="CLOSEPAGE" class="ttb-button btn-flat-orange">
								<spring:message code= "LB.X0194" />
							</button>
							
						</div>
					</div>
				</form>
			</section>
		</main>
		<!-- main-content END --> 
		<%@ include file="../index/footer.jsp"%>
	</body>
</html>