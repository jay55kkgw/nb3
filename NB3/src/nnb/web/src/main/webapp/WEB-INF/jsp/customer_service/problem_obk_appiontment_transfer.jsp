<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="page3" class="card-detail-block">
	<div class="card-center-block">
		<!-- Q1 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-31" aria-expanded="true"
				aria-controls="popup1-31">
				<div class="row">
					<span class="col-1">Q1</span>
					<div class="col-11">
						<span>網路銀行預約轉帳，是否需另外臨櫃申請?</span>
					</div>
				</div>
			</a>
			<div id="popup1-31" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>如已約定轉出帳號，即具有即時轉帳及預約轉帳功能，不需另外臨櫃提出預約轉帳之申請。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q2 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-32" aria-expanded="true"
				aria-controls="popup1-32">
				<div class="row">
					<span class="col-1">Q2</span>
					<div class="col-11">
						<span>網路銀行預約轉帳交易限額有何限制?</span>
					</div>
				</div>
			</a>
			<div id="popup1-32" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>網路銀行預約轉帳交易限額，於轉帳日與即時轉帳合併計算當日累計轉帳額度。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q3 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-33" aria-expanded="true"
				aria-controls="popup1-33">
				<div class="row">
					<span class="col-1">Q3</span>
					<div class="col-11">
						<span>網路銀行預約轉帳之預約期限?</span>
					</div>
				</div>
			</a>
			<div id="popup1-33" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>預約轉帳期限最長一年，至遲須於轉帳日之前一日完成預約；
							如預約之轉帳日為曆法所無之日期，以該月之末日為轉帳日。新台幣之預約轉帳，
							如遇例假日仍照常執行交易；外幣之預約轉帳，如遇例假日則順延至次一營業日執行交易。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q4 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-34" aria-expanded="true"
				aria-controls="popup1-34">
				<div class="row">
					<span class="col-1">Q4</span>
					<div class="col-11">
						<span>外幣預約轉帳之匯率?</span>
					</div>
				</div>
			</a>
			<div id="popup1-34" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>轉帳匯率採實際轉帳日上午9：10之掛牌匯率，並比照二：【服務項目】A10提供匯率優惠。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q5 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-35" aria-expanded="true"
				aria-controls="popup1-35">
				<div class="row">
					<span class="col-1">Q5</span>
					<div class="col-11">
						<span>如何得知預約轉帳之轉帳結果?</span>
					</div>
				</div>
			</a>
			<div id="popup1-35" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>請利用網路銀行「帳務查詢」－「預約交易查詢/取消」的「預約交易結果查詢」，
							可查詢最近六個月的預約交易轉帳結果，如查詢轉帳日當日轉帳結果，請於當日上午10:30後執行，
							才為當日轉帳之最終結果（不成功交易將於當日上午10:00再次執行轉帳作業）。對於已登錄「我的Email」者，
							將傳送轉帳結果至貴戶的電子郵箱，故務請登錄電子郵箱，如有變更郵箱位址，亦請更新以利傳送訊息。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q6 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-36" aria-expanded="true"
				aria-controls="popup1-36">
				<div class="row">
					<span class="col-1">Q6</span>
					<div class="col-11">
						<span>網路銀行簽入密碼、交易密碼變更或密碼重置，已登錄之預約轉帳是否仍屬有效?</span>
					</div>
				</div>
			</a>
			<div id="popup1-36" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>密碼變更及重置，已登錄之預約轉帳仍屬有效，將於轉帳日依預約指示執行轉帳作業。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q7 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-37" aria-expanded="true"
				aria-controls="popup1-37">
				<div class="row">
					<span class="col-1">Q7</span>
					<div class="col-11">
						<span>網路銀行使用者名稱、簽入密碼或交易密碼輸入錯誤超過次數致遭系統停用，已登錄之預約轉帳是否仍屬有效?</span>
					</div>
				</div>
			</a>
			<div id="popup1-37" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>上列資料輸入錯誤超過次數致遭系統停用時，原已登錄之預約轉帳仍屬有效，將於轉帳日依預約指示執行轉帳作業。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q8 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-38" aria-expanded="true"
				aria-controls="popup1-38">
				<div class="row">
					<span class="col-1">Q8</span>
					<div class="col-11">
						<span>電子憑證更新不成功、註銷或到期，已登錄之電子簽章預約轉帳交易是否仍屬有效?</span>
					</div>
				</div>
			</a>
			<div id="popup1-38" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>電子憑證更新不成功、註銷或到期，已登錄之電子簽章預約轉帳交易仍屬有效，將於轉帳日依預約指示執行轉帳作業。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q9 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-39" aria-expanded="true"
				aria-controls="popup1-39">
				<div class="row">
					<span class="col-1">Q9</span>
					<div class="col-11">
						<span>已成功登錄網路銀行預約轉帳交易，如何辦理取消預約轉帳?</span>
					</div>
				</div>
			</a>
			<div id="popup1-39" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>客戶如擬取消預約轉帳，至遲應於預約之轉帳日前一日使用網路銀行完成取消預約轉帳程序。
							以交易密碼交易(SSL)做的預約轉帳，須以交易密碼交易取消；以電子簽章交易(i-Key)做的預約轉帳，
							須以電子簽章交易取消。若客戶使用者名稱、密碼輸入錯誤超過次數遭系統停用或憑證更新不成功、註銷、到期致憑證無法使用，
							原已登錄之預約轉帳仍屬有效，如擬取消預約轉帳而因前述原因無法進入網路銀行取消時，應親赴網路銀行開戶行申辦取消未屆轉帳日之全部預約轉帳。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q10 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-310" aria-expanded="true"
				aria-controls="popup1-310">
				<div class="row">
					<span class="col-1">Q10</span>
					<div class="col-11">
						<span>申辦註銷已約定轉出帳號或轉入帳號，已登錄之預約轉帳是否仍屬有效?</span>
					</div>
				</div>
			</a>
			<div id="popup1-310" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>客戶臨櫃申辦註銷網路銀行轉出帳號，該註銷帳號下未屆轉帳日之預約轉帳均停止轉帳作業；註銷轉入帳號，未屆轉帳日之預約轉入已約定該帳號即停止轉帳作業。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>