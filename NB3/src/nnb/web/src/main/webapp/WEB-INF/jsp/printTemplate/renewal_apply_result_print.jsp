<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js_u2.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <title>${jspTitle}</title>
  <script type="text/javascript">
    $(document).ready(function () {
      window.print();
    });
  </script>
</head>

<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
  <br /><br />
  <div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif" /></div>
  <br /><br />
  <div style="text-align:center">
    <font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
  </div>
  <br/>
  <div style="text-align:center">
    ${MsgName}
  </div>
  <br />
    <table class="print">
    <!-- 交易時間 -->
    <tr>
      <td style="width:8em">
<!--         交易時間 -->
                      <spring:message code="LB.Trading_time" />
      </td>
      <td>${CMQTIME} </td>
    </tr>
    <!-- 存單帳號 -->
    <tr>
      <td class="ColorCell">
                <spring:message code="LB.Account_no" />
      </td>
      <td>${FDPACN}</td>
    </tr>
    <!-- 存單號碼 -->
    <tr>
      <td class="ColorCell">
                      <spring:message code="LB.Certificate_no" />
      </td>
      <td> ${FDPNUM}</td>
    </tr>
    <!-- 存單金額 -->
    <tr>
      <td class="ColorCell">
             <spring:message code="LB.Certificate_amount" />
      </td>
      <td>
<!--         新臺幣 -->
        <spring:message code="LB.NTD" />
        <fmt:formatNumber type="number" minFractionDigits="2" value="${AMOUNT}" />
<!--         元 -->
        									<spring:message code="LB.Dollar" />       
      </td>
    </tr>
    <!-- 存單種類 -->
    <tr>
      <td class="ColorCell">
        <spring:message code="LB.Deposit_certificate_type" />
      </td>
      <td>
        ${FDPACC}
      </td>
    </tr>
    <!-- 存單期別  -->
    <tr>
      <td class="ColorCell">
        <spring:message code="LB.Fixed_duration" />
      </td>
      <td>
        ${TERM}
<!--         月 -->
       <spring:message code="LB.Month"/>
      </td>
    </tr>
    <!-- 計息方式 -->
    <tr>
      <td class="ColorCell">
        <spring:message code="LB.Interest_calculation" />
      </td>
      <td>
       ${INTMTH}
      </td>
    </tr>
    <!-- 轉期次數 -->
    <tr>
      <td class="ColorCell">
        <spring:message code="LB.Number_of_rotations" />
      </td>
      <td>
        ${FGRENNAME}
      </td>
    </tr>
    <!-- 轉存方式 -->
    <c:if test="${FGRENCNT != '3'}">
	    <tr>
	      <td class="ColorCell">
	<!--         轉存方式 -->
	        <spring:message code="LB.Rollover_method" />
	      </td>
	      <td>
	          <!--轉存方式 -->
	          ${FGSVNAME}${DPSVACNO}
	      </td>
	    </tr>
    </c:if>
  </table>
  <br>
  <br>
<!--   <div class="text-left"> -->
<!--     說明： -->
<!--     <ol class="list-decimal text-left"> -->
<!--       <li>晶片金融卡:為保護您的交易安全，結束交易或離開電腦時，請務必將晶片金融卡抽離讀卡機並登出系統。</li> -->
<!--       <li>電子簽章:為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章(載具i-key)拔除並登出系統。</li> -->
<!--     </ol> -->
<!--   </div> -->
</body>

</html>