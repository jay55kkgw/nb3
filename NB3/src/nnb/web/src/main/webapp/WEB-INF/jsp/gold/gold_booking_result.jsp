<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	
	$("#printButton").click(function(){
		var params = {
			"jspTemplateName":"gold_booking_result_print",
			//預約黃金買進
			"jspTitle":"<spring:message code= "LB.X0949" />",
			"CMQTIME":"${gold_booking_result_data.data.CMQTIME}",
			"SVACN":"${gold_booking_result_data.data.SVACN}",
			"ACN":"${gold_booking_result_data.data.ACN}",
			"TRNGD":"${gold_booking_result_data.data.TRNGD}"
		};
		openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
	});
	$("#kycButton").click(function(){
		var UID = "${gold_booking_result_data.data.CUSIDN}";
		
		if(UID.length == 10){
			$("#formID").attr("action","${__ctx}/FUND/TRANSFER/fund_invest_attr1?TXID=G");
		}
		else if(UID.length < 10){
			$("#formID").attr("action","${__ctx}/FUND/TRANSFER/fund_invest_attr3?TXID=G");
		}
		$("#formID").submit();
	});
});
</script>
</head>
<body>
	<!--header-->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %> 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 黃金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2272" /></li>
    <!-- 黃金申購     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1493" /></li>
		</ol>
	</nav>

	<!--左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!--快速選單及主頁內容-->
		<main class="col-12"> 
			<!--主頁內容-->
			<section id="main-content" class="container">
<!-- 預約黃金買進 -->
				<h2><spring:message code= "LB.X0949" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form id="formID" method="post">
                	<input type="hidden" name="ADOPID" value="N09001"/>
	                <div class="main-content-block row">
	                    <div class="col-12 tab-content">
	                        <div class="ttb-input-block">
	                        	<div class="ttb-message">
<!-- 預約成功 -->
	                        		<span><spring:message code= "LB.W0284" /></span>
	                        	</div>
								<!--交易日期-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
										<label>
<!-- 資料日期 -->
	                                        <h4><spring:message code= "LB.W0875" /></h4>
	                                    </label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
	                                       	<span>${gold_booking_result_data.data.CMQTIME}</span>
										</div>
									</span>
								</div>
								<!--台幣轉出帳號-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                	<label>
<!-- 臺幣轉出帳號 -->
	                                        <h4><spring:message code= "LB.W1496" /></h4>
	                                    </label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
	                                       	<span>${gold_booking_result_data.data.SVACN}</span>
										</div>
									</span>
								</div>
								<!--黃金轉入帳號-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
		                                <label>
<!-- 黃金轉入帳號 -->
											<h4><spring:message code= "LB.W1497" /></h4>
										</label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
	                                       	<span>${gold_booking_result_data.data.ACN}</span>
										</div>
									</span>
								</div>
								<!--買進公克數-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
										<label>
<!-- 買進公克數 -->
	                                        <h4><spring:message code= "LB.W1498" /></h4>
	                                    </label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
	                                       	<span>${gold_booking_result_data.data.TRNGD}</span>
<!-- 公克 -->
											<span class="ttb-unit"><spring:message code= "LB.W1435" /></span>
										</div>
									</span>
								</div>
							
	                        </div>
	                     
<!-- 列印 -->
	                  			<input type="button" id="printButton" value="<spring:message code= "LB.Print" />" class="ttb-button btn-flat-orange"/>
	                			<c:if test="${sessionScope.dpmyemail == 'PASS'}">
	                			<!-- 填寫KYC問卷 -->
	                				<input type="button" id="kycButton" value="<spring:message code= "LB.X0947" />" class="ttb-button btn-flat-orange"/>
	                			</c:if>
	                		
	                    </div>
	                </div>
					<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /></p><!-- 說明 -->
						<!-- 將以預約後的第一個營業日的第一次牌告賣出價格交易，請於扣款前一日存足款項於轉出帳戶備扣。 -->
							<li><spring:message code= "LB.Gold_Booking_P3_D1-1" /><font color=red><spring:message code= "LB.Gold_Booking_P3_D1-2" /></font></li>
							<li><spring:message code= "LB.Gold_Booking_P3_D2" /></li><!-- 	電子簽章：為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章（載具i－key）拔除並登出系統。 -->
							<li><spring:message code= "LB.Gold_Booking_P3_D3" /></li><!-- 	預約成功不代表交易已完成，請至黃金存摺-查詢服務-黃金存摺明細查詢，以確認交易結果。 -->
						</ol>

			</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>