<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	    <script type="text/javascript">
        $(document).ready(function () {
            initFootable(); // 將.table變更為footable 
            init();
        });

        function init() {
	    	$("#pageshow").click(function(e){			
	        	initBlockUI();
				var action = '${__ctx}/GOLD/APPLY/gold_account_apply_p4';
				$("form").attr("action", action);
    			$("form").submit();
			});
	    	$("#back").click(function(e){			
	        	initBlockUI();
				var action = '${__ctx}/GOLD/APPLY/gold_account_apply_p2';
				$("form").attr("action", action);
    			$("form").submit();
			});
        }

        function checkReadFlag()
        {
        	console.log($("#ReadFlag").prop('checked') && $("#ReadFlag1").prop('checked'));
            if ($("#ReadFlag").prop('checked') && $("#ReadFlag1").prop('checked'))    		
          	{
           		$("#pageshow").prop('disabled',false);
           		$("#pageshow").addClass("btn-flat-orange");
          	}
          	else
          	{
           		$("#pageshow").prop('disabled',true);
           		$("#pageshow").removeClass("btn-flat-orange");
       	  	}	
        }

    </script>
    <style>
    	.DataCell{
    	    text-align: left;
    	}
    </style>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 黃金存摺帳戶     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2273" /></li>
    <!-- 黃金存摺帳戶申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1655" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!-- 線上申請黃金存摺帳戶 -->
					<spring:message code="LB.W1655"/>
					
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form id="formId" method="post">
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12 terms-block">
							<div class="ttb-message">
							<!-- 臺灣企銀 網路銀行申請黃金存摺帳戶約定書 -->
								<p><spring:message code= "LB.X0939" /></p>
                            	<span><font color="royalblue" size="3"><b><spring:message code= "LB.X1208" /><font color=red><spring:message code="LB.W1554"/></font><spring:message code= "LB.X1209" /><font color=red><spring:message code="LB.Cancel"/></font><spring:message code= "LB.X1210" /></b></font></span>
							</div>
							<br>
							
                        	<h4>壹、申請黃金存摺帳戶約定事項</h4>
							<ul class="ttb-result-list terms">
	                            <li data-num="第一條">
	                                <span class="input-subtitle subtitle-color">申請要件</span>
	                                <p>
										申請人暨立約定書人(以下簡稱立約人)以臺灣中小企業銀行（以下簡稱貴行）之網路銀行服務系統，申請黃金存摺帳戶，須具備下列要件：
	                                </p>
	                                <ul>
	                                    <li data-num="一、">年滿20歲且有完全行為能力之本國人。</li>
	                                    <li data-num="二、">已申請使用貴行網路銀行且已約定新臺幣活期性存款（不含支票存款）帳戶為轉出帳號者。</li>
	                                    <li data-num="三、">已申請使用貴行晶片金融卡或電子簽章(憑證載具)。</li>
	                                </ul>
	                            </li>
	                            <li data-num="第二條">
	                                <span class="input-subtitle subtitle-color">系統服務</span>
	                                <p>立約人於貴行網路銀行申請黃金存摺帳戶完成後，黃金存摺功能以貴行網路銀行所提供之服務為限，如要辦理網路銀行未提供之黃金存摺功能，立約人應親持身分證件、新臺幣活期性存款存摺及原留印鑑至開戶行辦理領取黃金存摺手續後，始得辦理臨櫃交易。</p>
	                            </li>
	                            <li data-num="第三條">
	                                <span class="input-subtitle subtitle-color">指定網路銀行交易買進／回售之新臺幣存款帳戶（以下簡稱網路銀行交易指定帳戶）</span>
	                                <p>存戶為自然人時，其網路銀行交易指定帳戶（不含支票存款）應為存戶本人於貴行營業單位開立之新臺幣活期存款、活期儲蓄存款或綜合存款帳戶。</p>
	                            </li>
	                            <li data-num="第四條">
	                                <span class="input-subtitle subtitle-color">黃金存摺明細</span>
	                                <p>網路銀行黃金存摺明細查詢所記載之交易單價資料係每筆交易之成交價格，並不代表黃金帳戶內黃金餘額價值。</p>
	                            </li>
	                            <li data-num="第五條">
	                                <span class="input-subtitle subtitle-color">暫停服務</span>
	                                <p>立約人之指定網路銀行交易申購/回售之本人新臺幣帳戶，如經法院、檢察署或司法警察機關通報為警示帳戶，或經貴行依據「銀行對疑似不法或顯屬異常交易之存款帳戶管理辦法」研判有疑似不法或顯屬異常交易者，貴行得暫停立約人之黃金存摺網路銀行功能服務。</p>
	                            </li>
	                            <li data-num="第六條">
	                                <span class="input-subtitle subtitle-color">個人資料之利用</span>
	                                <p>立約人同意貴行及財團法人金融聯合徵信中心、中小企業信用保證基金、票據交換所、財金資訊股份有限公司或其他與貴行有業務往來之機構，於符合其營業登記項目或章程所定業務之需要，得蒐集、電腦處理或國際傳遞及利用立約人之個人資料。</p>
	                            </li>
	                        </ul>
                        	<h4>貳、黃金存摺開戶約定書約定事項</h4>
							<ul class="ttb-result-list terms">
	                            <li data-num="第一條">
	                                <span class="input-subtitle subtitle-color">存入</span>
	                                <ul>
	                                    <li data-num="(一)">立約人買進黃金存入時，應按當時貴行掛牌賣出價格繳交買進黃金價款。</li>
	                                    <li data-num="(二)">除定期投資外，每次存入之黃金數量不得低於1公克，並應為1公克的整倍數。</li>
	                                </ul>
	                            </li>
	                            <li data-num="第二條">
	                                <span class="input-subtitle subtitle-color">定期定額投資</span>
	                                <p>立約人辦理定期定額投資買進黃金存入本存摺者，各項事宜悉依黃金存摺定期定額投資約定條款辦理。</p>
	                            </li>
	                            <li data-num="第三條">
	                                <span class="input-subtitle subtitle-color">回售</span>
	                                <ul>
	                                    <li data-num="(一)">立約人每次回售黃金數量不得低於1公克，並應為1公克的整倍數，但將帳戶餘額全數回售或銷戶者，不在此限。</li>
	                                    <li data-num="(二)">立約人回售黃金之價款應存入本人在貴行約定之黃金買賣入、扣帳新臺幣活期（儲）存款帳戶，回售黃金之價款為提領現金方式時，須回臨櫃辦理，並依稅法相關規定繳納印花稅。</li>
	                                </ul>
	                            </li>
	                            <li data-num="第四條">
	                                <span class="input-subtitle subtitle-color">預約交易</span>
	                                <p>立約人預約交易後第一營業日若因天然災害或其他不可抗力之原因，致無黃金牌告價時，則該預約交易無效。</p>
	                            </li>
	                            <li data-num="第五條">
	                                <span class="input-subtitle subtitle-color">手續費收費標準</span>
	                                <p>立約人同意依貴行下列所訂之收費標準繳納相關費用，並授權貴行自立約人之帳戶內自動扣繳；收費標準於訂約後如有變更或調整，貴行應於生效日六十日前以顯著方式於營業場所、貴行網站公告其內容，並以電子郵件方式使立約人得知調整費用，立約人若對於該變更或調整有異議時，得於前開公告期間內終止本約定事項，逾期未終止者，視為同意該變更或調整：</p>
	                                <ul>
	                                    <li data-num="一、">線上申請黃金存摺帳戶，每戶收費新台幣50元。</li>
	                                    <li data-num="二、">黃金存摺網路交易定期定額扣款成功，每戶收費新台幣50元。</li>
	                                </ul>
	                            </li>
	                            <li data-num="第六條">
	                                <span class="input-subtitle subtitle-color">銷戶</span>
	                                <p>本存摺帳戶餘額為零，得結清銷戶，並應由立約人本人親自臨櫃辦理；如無法親自辦理而委任代理人時，應出具授權書及可資確認本人及代理人身分之證明文件。</p>
	                            </li>
	                            <li data-num="第七條">
	                                <p>本存摺表彰之權利不得轉讓或質押予第三者。</p>
	                            </li>
	                            <li data-num="第八條">
	                                <span class="input-subtitle subtitle-color">投資風資</span>
	                                <p><font color=red><b>國際黃金價格有漲有跌，立約人投資黃金可能產生本金收益或損失，最大可能損失為買進金額之全部，請自行審慎判斷投資時機並承擔投資風險，辦理黃金存摺各項交易，如有涉及贈與、繼承及應繳稅捐等情事，悉由立約人或繼承人自行申報與負擔，黃金存摺不計算利息，亦非屬存款保險條例規定之標的，不受存款保險保障。<b></font></p>
	                            </li>
	                            <li data-num="第九條">
	                                <p>立約人與貴行往來期間，如遇有貴行或他人聲請假扣押、假處分、強制執行或有疑似洗錢不法使用之情 事，貴行得逕 行終止本約定，立約人申請給付時，依法處理。</p>
	                            </li>
	                            <li data-num="第十條">
	                                <p>貴行對立約人所為之通知或函件，依立約人於貴行留存之地址或其最後以書面指定之地址郵寄後，經通常之郵遞期間，即視為已送達於存戶。</p>
	                            </li>
	                            <li data-num="第十一條">
	                                <p>本約定書各約定事項，貴行得視業務需要隨時增修，貴行得在各地營業單位公告或以業務簡介方式置於營業單位供索閱以代公告，不另通知，立約人若對增修事項不同意者，應以書面向貴行終止使用各該服務項目，但終止前立約人所為交易帳款及其他衍生之債務，立約人仍負清償責任。</p>
	                            </li>
	                            <li data-num="第十二條">
	                                <p>本約定事項如有未盡事宜，悉依中華民國法令辦理。</p>
	                            </li>
	                            <li data-num="第十三條">
	                                <p>雙方同意如因本約定書涉訟時，適用中華民國法令，並以指定網路銀行交易買進／回售之新臺幣存款帳戶契約約定之地方法院為第一審管轄法院，但法律有專屬管轄之規定者，從其規定。</p>
	                            </li>
	                            <li data-num="第十四條">
	                                <p>立約人同意貴行得依業務需要，隨時修改本約定書開立帳戶之相關服務內容，並在貴行網站上公告其內容，以代通知，修改後之交易，立約人願自動適用異動後之服務內容，毋須另行約定。</p>
	                            </li>
	                        </ul>
							
							
	                        <!--button 區域 -->
	                        <div>
	                            <!-- 列印  -->
	                            <!-- 取消 -->
	                            <input id="back" name="back" type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Cancel"/>" />
	                            <!--  同意 -->
	                            <input id="pageshow" name="pageshow" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.W1554"/>"/>
	                        </div>
                        <!--                     button 區域 -->
                        </div>  
                    </div>
                </form>

	
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>