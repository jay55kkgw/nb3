<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
    
<script type="text/javascript">
/* $(document).ready(function(){
		alert("${card_unbilled.data.n810cardList}"+"end");
	}) */
	function showCardNo(id){
		console.log("radio: " + id);
		
		var oCNUM = document.getElementById("CNUM");
		if (oCNUM != undefined)
		{
			if (document.getElementById("RAD2").checked)
			{
				oCNUM.style.display = "";
			}else{
			        oCNUM.style.display = "none";
			}		
		}
		
		if (obj.value == '0')
			$('#H_CARDTYPE').val("0");
		else
			$('#H_CARDTYPE').val("1");
		if ($('#CARDTYPE1').val()="0")
		  $('#CARDNUM').val("");
	}
	
</script>
</head>
<body>
   	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 信用卡     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Credit_Card" /></li>
    <!-- 帳務查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Inquiry_Service" /></li>
    <!-- 未出帳單明細查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Credit_Card_Un-billed_Transactions" /></li>
		</ol>
	</nav>

	
	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
	
	
	<main class="col-12">
		<section id="main-content" class="container"><!-- 主頁內容  -->
			<!-- 信用卡未出帳單明細查詢 -->
			<h2><spring:message code="LB.Credit_Card_Un-billed_Transactions" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="${__ctx}/CREDIT/INQUIRY/card_unbilled_result">
			<input type="hidden" id="CARDTYPE" name="CARDTYPE" value=" ${card_unbilled.data.n810cardList}">
			<input type="hidden" id="CARDNUMLIST" name="CARDNUMLIST" value=" ${card_unbilled.data.n810cardNumList}">
			<input type="hidden" id="H_CARDTYPE"name="H_CARDTYPE" value="">
			<input type="hidden" name="CARDNUM" value="1">
				<div class="main-content-block row">
					<div class="col-12 tab-content" id="nav-tabContent">
					<!-- 主頁內容  -->
					<div class="col-12">
					<div class="ttb-input-block">
						<div class="ttb-message">
						<p><spring:message code="LB.Credit_Card_Un-billed_Transactions" /></p><!-- 信用卡未出帳單明細查詢 -->
							
						</div>
						<div class="ttb-input-item row">
							<span class="input-title"> <label><h4><spring:message code="LB.Inquiry_time" /></h4></label><!-- 查詢時間 -->
							</span> <span class="input-block">
								<div class="ttb-input">
									<p>${querytime}</p>
									<input type="hidden" name="SYSTIME" value="${querytime}" readonly="readonly"/>
								</div>
							</span>
						</div>
						
						<div class="ttb-input-item row">
							<span class="input-title"> <label><h4><spring:message code="LB.Card_type" /></h4></label><!-- 卡別 -->
							</span> <span class="input-block">
								
								<div class="ttb-input">
								<!-- 顯示一般卡  -->
									<c:if test="${card_unbilled.data.N810MsgCode != 'E091'}">
										<label class="radio-block"><spring:message code="LB.Bank_card" />
											<input type="radio" name="CARDTYPE1" value="0" id="RAD1" onclick="showCardNo(this.id)" checked />
											<span class="ttb-radio"></span>
										</label>
									</c:if>
									<span class="input-unit">
									<c:if test="${card_unbilled.data.N810MsgCode != '0' and card_unbilled.data.N810MsgCode != ''}">
												<span class="input-unit">
												<font color=red>
												(<spring:message code="LB.ErrMsg" />：&nbsp;${card_unbilled.data.N810MsgCode} &nbsp;${card_unbilled.data.N810MsgCode}）
												</font>
												<script>
													var obj = document.getElementById("RAD1");
													obj.disabled = true;
												</script>
												</span>
									</c:if>
									</span>
								
									
									<!-- 顯示VISA金融卡  -->
										<c:if test="${card_unbilled.data.N813MsgCode !='E091'}">
											<c:if test="${card_unbilled.data.N810MsgCode !='E091'}">
												<br>
											</c:if>
											<label class="radio-block"><spring:message code="LB.VISA_debit_card" />
												<input type="radio" name="CARDTYPE1" value="1" id="RAD2" onclick="showCardNo(this.id)" />
												<span class="ttb-radio"></span>
											</label><!-- VISA金融卡 -->
										
										<span class="input-unit">
											<!-- 顯示錯誤訊息  -->
										<c:if test="${card_unbilled.data.N813MsgCode == '' and card_unbilled.data.N813MsgCode != 'E091' }">
												<font color=red>
													（<spring:message code="LB.ErrMsg" />：&nbsp;${card_unbilled.data.N813MsgCode} &nbsp;${card_unbilled.data.N813MsgCode}）
												</font>
												<script>
													var obj = document.getElementById("RAD2");
													obj.disabled = true;
												</script>
										</c:if>
										</span>
										</c:if>
								</div>
								
								<div id="CNUM" class="ttb-input" style="display:none">
									
									<spring:message code="LB.Card_number" />
									
								    <select name="CARDNUM" id="CARDNUM">
					                		<!-- 預設值  -->
						    				<option value="" selected>------<spring:message code="LB.Select_account" />------</option> <!-- ------ 請選擇(信用卡)帳號 ------ -->
						    				
						    				<!-- 卡號_List迴圈  -->
						    				<c:set var="count" value="0" scope="page" />
					                		<c:forEach var="cardnumList" items="${card_unbilled.data.n813cadNumList}">
					                			<c:if test="${cardnumList != null}">
					                				<option value="${cardnumList}"> ${cardnumList} </option>
					                				<c:set var="count" value="${count + 1}" scope="page"/>
					                			</c:if>
					                		</c:forEach>
					                		<!-- 若卡號不只一個則新增 [全部]選項  -->
					                		<c:if test="${count > 1}">
					                			<option value="ALL">------ <spring:message code="LB.All" /> ------</option><!-- 全部＊ -->
					                		</c:if>
						    			</select>
									</div>
								</div>
								
								<div class="ttb-input-item row">
									<span class="input-title"> <label><h4><spring:message code="LB.Inquiry_type" /></h4></label></span>
									<!-- 查詢方式 -->
									<span class="input-block">
										<div class="ttb-input">
											<!-- 網頁顯示 -->
											<label class="radio-block"><spring:message code="LB.Display_as_web_page" />
												<input type="radio" name="QueryType" value="0" checked> <span 
												class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input ">						
											<!-- 電子郵件 -->
											<label class="radio-block"> <spring:message code="LB.e-mail" /> <!-- 電子郵件 -->
												<input type="radio" name="QueryType" value="1"> 
												<input class="text-input" type="text" size="25" name="CMMAIL" value="${sessionScope.dpmyemail}">
												<span class="ttb-radio"></span>
											</label>
										</div> 
									</span>
								</div>
						</div>
					</div>
					<input id="CMRESET" name="CMRESET" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" /> 
					<input id="CMSUBMIT" name="CMSUBMIT" type="submit" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
				</div>
			</div>
			<div class="text-left">
				<spring:message code="LB.Description_of_page" /><!-- 說明 -->
				<ol class="list-decimal text-left">
					<li><spring:message code="LB.Unbilled_P1_D1" /><!-- 正卡持卡人歸戶名下所有信用卡〈含附卡〉之交易明細。 --></li>
				</ol>
					</div>
					</form>
				</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>
