<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function(){
			window.print();
		});
	</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br/><br/>
	<div style="text-align:center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br/><br/><br/>
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<br/><br/><br/>
	
	<!-- 查詢時間 -->
	<label><spring:message code="LB.Inquiry_time" />：${CMQTIME }</label>
	<br/><br/>

	<!-- 查詢期間 -->
	<label><spring:message code="LB.Inquiry_period_1" />：${CMPERIOD } </label>
	<br/><br/>
	
	<!-- 資料總數 -->
	<label><spring:message code="LB.Total_records" />：${CMRECNUM } <spring:message code="LB.Rows" /></label>
	<br/><br/>
	
	<table class="print">
		<tr>
			<!-- 身份證字號/統一編號-->
			<th><spring:message code="LB.Id_no" /></th>
			<th class="text-left" colspan="13">${CUSIDN_F }</th>
		</tr>
		<tr>
			<!-- 姓名 -->
			<th><spring:message code="LB.Name" /></th>
			<th class="text-left" colspan="13">${CUSNAME_F }</th>
		</tr>
		<tr>
			<!-- 交易日期 -->
			<th nowrap><spring:message code="LB.Transaction_date" /></th>
			<th nowrap><spring:message code="LB.W0944" /></th>
			<th nowrap><spring:message code="LB.W0025" /></th>
			<th nowrap><spring:message code="LB.W0946" /></th>
			<!-- 交易類別 -->
			<th nowrap><spring:message code="LB.Transaction_type" /></th>
			<th><spring:message code="LB.W0947" /></th>
			<th nowrap><spring:message code="LB.W0027" /></th>
			<th nowrap><spring:message code="LB.W0949" /></th>
			<!-- 匯率 -->
			<th nowrap><spring:message code="LB.Exchange_rate" /></th>
			<th nowrap ><spring:message code="LB.W0950" /></th>
			<th nowrap ><spring:message code="LB.W0951" /></th>
			<th nowrap ><spring:message code="LB.W0952" /></th>
		</tr>
		<c:forEach var="row" items="${dataListMap }">
			<tr>
				<td class="text-center">${row.TRADEDATE_F }</td>					<!-- 交易日期 -->
				<td class="text-center">${row.CDNO_F }</td>						<!-- 信託帳號 -->
				<td class="text-center">${row.TRANSCODE_FUNDLNAME }</td>			<!-- 基金名稱 -->
				<td class="text-center">${row.TR106_F }</td>						<!-- 投資方式 -->
				<td class="text-center">${row.FUNDTYPE_F }</td>						<!-- 交易類別 -->
				<td class="text-center">
					${row.CRY_CRYNAME } 						<!-- 信託/除息金額幣別 -->
					<br />
					${row.FUNDAMT_F }							<!-- 信託/除息金額 -->
				</td>
				<td class="text-right">${row.UNIT_F }</td>		<!-- 單位數 -->
				<td class="text-right">${row.PRICE1_F }</td>	<!-- 淨值 -->
				<td class="text-right">${row.EXRATE_F }</td>	<!-- 匯率 -->
				<td>${row.INTRANSCODE_FUNDLNAME }</td>			<!-- 轉入基金名稱 -->
				<td class="text-right">${row.TXUNIT_F }</td>	<!-- 轉入單位數 -->
				<td class="text-center">							
					${row.INTRANSCRY_CRYNAME } 					<!-- 轉入價格幣別 -->
					<c:if test = "${row.INTRANSCRY_CRYNAME != ''}"><br /></c:if>
					${row.PRICE2_F }							<!-- 轉入價格 -->
				</td>
			</tr>
		</c:forEach>
	</table>
	<br/><br/><br/><br/>
</body>
</html>