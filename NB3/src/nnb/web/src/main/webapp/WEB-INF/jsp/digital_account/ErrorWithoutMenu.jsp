<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<!DOCTYPE html>
<html>
<head>

	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>

<script type="text/javascript">
	var action = '${__ctx}';
	$(document).ready(function() {
	
		init();
		$("form").submit(function(event) {

			console.log("submit>>");
		});

	});
	
	function init() {
		
		$("#next").click(function() {
			console.log("next action>>" + action);
			action = action + '${error.next}'
			submitForm(action);
		});
		$("#previous").click(function() {
			console.log("previous action>>" + action);
			action = action + '${error.previous}'
			submitForm(action);
		});
	}
	
	function submitForm(action) 
	{
		initBlockUI();
		$("form").attr("action", action);
		$("form").submit();
	}
</script>
</head>
	<body>
		<!-- header     -->
		<header>
		<%@ include file="../index/header_logout.jsp"%>
		</header>
		<!-- 麵包屑 -->
		<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
			<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
				<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			</ol>
		</nav>

		<!-- content row END -->
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> <!-- 		主頁內容  --> <%--     <form id="formId" method="post" action="${__ctx}/index"> --%>
			<section id="main-content" class="container">
				<h2><spring:message code="LB.Transaction_result" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="${__ctx}/INDEX/index">
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-result-block">
								<%--                         <img src="${__ctx}/img/icon-success.svg"> --%>
								
							</div>
							<ul class="ttb-result-list">
								<li>
									<h3><spring:message code="LB.Transaction_code" /></h3>
									<p>${error.msgCode}</p>
								</li>
								<li>
									<h3><spring:message code="LB.Transaction_message" /></h3>
									<p>${error.message}</p>
								</li>
							</ul>
							<div class="text-left">
								<ol class="list-decimal text-left">
								</ol>
							</div>
							<c:if test="${ not empty error.previous}">
								<input type="hidden" name="previous" value="${error.previous}">
								<button type="button" id="previous"
									class="ttb-button btn-flat-orange"><spring:message code="LB.Back_to_previous_page" /></button>
							</c:if>
							<c:if test="${ not empty error.next}">
								<button type="button" id="next"
									class="ttb-button btn-flat-orange"><spring:message code="LB.Back_to_function_home_page" /></button>
							</c:if>
							
						</div>
					</div>
				</form>
			</section>
		</main>
		<!-- 		main-content END --> 
	<!-- 	content row END -->
		<%@ include file="../index/footer.jsp"%>
	</body>
</html>