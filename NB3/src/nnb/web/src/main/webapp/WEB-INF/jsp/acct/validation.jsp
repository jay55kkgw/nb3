<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TBB-${pageContext.response.locale}.js"></script>
	
	<script type="text/JavaScript">
		// 遮罩用
		var blockId;
		
	    $(document).ready(function() {
	    	// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});

	 	// 初始化
	    function init(){
	    	// 即時、預約標籤 click
	    	tabEvent();
	    	// 建立約定轉出帳號下拉選單
	    	creatOutAcn();
	    	// 轉出帳號 change
	    	acnoEvent();
	    	// 預約自動輸入明天
	    	getTmrDate();
	    	// 預約日期 change
	    	fgtxdateEvent();
	    	// 西元年日期輸入欄位 click
	    	datetimepickerEvent();
	    	// 通訊錄 click
			addressbookClickEvent();

			// 初始化後隱藏span( 表單驗證提示訊息用的 )
			$(".hideblock").hide();
			// 表單驗證
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
			// submit觸發事件
			finish();
	    }
	 	
	 	// 確認按鈕點擊觸發submit
		function finish(){
			$("#CMSUBMIT").click(function (e) {
				// 顯示驗證隱藏欄位( 為了讓提示訊息對齊 )
				$(".hideblock").show();
				
				console.log("submit~~");
				
				// 塞值進隱藏span內的input
				$("#CMDATE").val($("#CMDATE_TMP").val());
				$("#LIMDAT").val($("#LIMDAT_TMP").val());
				$("#AMOUNT").val($("#AMOUNT_TMP").val());
				$("#datet").val($("#date").val());
				// 塞值進隱藏span內的input--約定/非約定 之轉出帳號
				if( $('input[name=TransferType]:checked').val() == 'PD'){
					// 轉出帳號為約定
					$("#OUTACN").val($("#OUTACN_PD").val());
				}else if( $('input[name=TransferType]:checked').val() == 'NPD'){
					// 轉出帳號為非約定
					$("#OUTACN").val($("#OUTACN_NPD").val());
				}

				//  送出前處理--主要是表單驗證先跳過
// 				processQuery();
				
				if ( !$('#formId').validationEngine('validate') ) {
					e = e || window.event; // for IE
					e.preventDefault();
				} else {
					// 解除表單驗證
					$("#formId").validationEngine('detach');
					// 畫面遮罩
					initBlockUI();
					// 送交
					$("#formId").submit();
				}
			});
	 	}

		// 即時、預約標籤切換事件
		function tabEvent(){
			// 即時
			$("#nav-trans-now").click(function() {
				$("#transfer-date").hide();
				fstop.setRadioChecked("FGTXDATE" , "1" ,true);
			})
			// 預約
			$("#nav-trans-future").click(function() {
				$("#transfer-date").show();
				fstop.setRadioChecked("FGTXDATE" , "2" ,true);
			})
		}

		// 建立約定轉出帳號下拉選單
		function creatOutAcn(){
			var options = { keyisval:true ,selectID:'#OUTACN_PD'};
			
			uri = '${__ctx}'+"/PAY/EXPENSE/getOutAcn_aj"
			rdata = {type: 'acno' };
			console.log("creatOutAcn.uri: " + uri);
			console.log("creatOutAcn.rdata: " + rdata);
			
			data = fstop.getServerDataEx(uri,rdata,false);

			console.log("creatOutAcn.data: " + JSON.stringify(data));
			
			if(data !=null && data.result == true ){
				fstop.creatSelect( data.data, options);
			}
		}
		
		// 轉出帳號change事件，要秀出選擇的轉出帳號可用餘額
		function acnoEvent(){
			$("#OUTACN_PD").change(function() {
				var acno = $('#OUTACN_PD :selected').val();
				console.log("acnoEvent.acno: " + acno);
				getACNO_Data(acno);
			});
		}
		// 取得轉出帳號餘額資料
		function getACNO_Data(acno){
			var options = { keyisval:true ,selectID:'#OUTACN'};
			
			uri = '${__ctx}'+"/NT/ACCT/TRANSFER/getACNO_Data_aj"
			rdata = {acno: acno};
			console.log("getACNO_Data.uri: " + uri);
			console.log("getACNO_Data.rdata: " + rdata);
			
			fstop.getServerDataEx(uri,rdata,false,isShowACNO_Data);
		}
		// 顯示轉出帳號餘額
		function isShowACNO_Data(data){
			var i18n= new Object();
			i18n['available_balance'] = '<spring:message code="LB.Available_balance" />: ';
			console.log("isShowACNO_Data.data" + JSON.stringify(data));
			if(data && data.result){
				$("#acnoIsShow").show();
				// 可用餘額
				console.log("data.data.accno_data" + data.data.accno_data.BDPIBAL);
				// 格式化金額欄位
				i18n['available_balance'] += fstop.formatAmt(data.data.accno_data.BDPIBAL);
				$("#showText").html(i18n['available_balance']);
			}else{
				$("#acnoIsShow").hide();
			}
		}

		// 預約自動輸入明天
		function getTmrDate() {
			// 預約日期欄位顯示明天
			$('#CMDATE_TMP').val("${tmrDate}");
		}
		// 預約日期change事件 ，檢核日期邏輯
		function fgtxdateEvent(){
			 $('input[type=radio][name=FGTXDATE]').change(function() {
		        if (this.value == '2') { // 預約
		            console.log("tomorrow");
		            $("#CMDATE").addClass("validate[required]")
		        }else if (this.value == '1') { // 即時
		            $("#CMDATE").removeClass("validate[required]");
		        }
		    });
		}

		// 西元小日曆click
		function datetimepickerEvent(){
		    $(".CMDATE").click(function(event) {
				$('#CMDATE_TMP').datetimepicker('show');
			});
		    $(".LIMDAT").click(function(event) {
				$('#LIMDAT_TMP').datetimepicker('show');
			});
			jQuery('.datetimepicker').datetimepicker({
				timepicker:false,
				closeOnDateSelect : true,
				scrollMonth : false,
				scrollInput : false,
			 	format:'Y/m/d',
			 	lang: '${transfer}'
			});
		}

		// 通訊錄click
		function addressbookClickEvent() {
			// 通訊錄btn
			$('#getAddressbook').click(function() {
				openAddressbook();
			});
			// 同交易備註btn
			$('#CMMAILMEMO_btn').click(function() {
				$('#CMMAILMEMO').val( $('#CMTRMEMO').val() );
			});
		}
		// 開啟通訊錄email選單
		function openAddressbook() {
			window.open('${__ctx}/NT/ACCT/TRANSFER/showAddressbook');
		}
	    
	</script>
</head>
<body>
    <!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	
    <!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i
					class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><a href="#"><spring:message code= "LB.W0366" /></a></li>
			<li class="ttb-breadcrumb-item"><a href="#"><spring:message code= "LB.W0632" /></a></li>
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code= "LB.W0425" /></li>
		</ol>
	</nav>
	
	<!-- 功能清單及登入資訊 -->
	<div class="content row">
	
		<!-- 功能清單 -->
		<%@ include file="../index/menu.jsp"%>

		<!-- 快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
				<!-- 功能內容 -->
				<form method="post" id="formId" action="${__ctx}/PAY/EXPENSE/electricity_fee_confirm">
					<input type="hidden" id="TOKEN" name="TOKEN" value="${sessionScope.transfer_confirm_token}" /><!-- 防止重複交易 -->
					<input type="hidden" name="FLAG" value="1"><!-- 約定\非約定 -->
					
					<!-- 功能名稱 -->
					<h2><spring:message code= "LB.W0425" /></h2>
					<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
					
					<!-- 交易流程階段 -->
					<div id="step-bar">
						<ul>
							<li class="active"><spring:message code="LB.Enter_data" /></li>
							<li class=""><spring:message code="LB.Confirm_data" /></li>
							<li class=""><spring:message code="LB.Transaction_complete" /></li>
						</ul>
					</div>
					
					<!-- 功能內容 -->
					<div class="main-content-block row">
						<!-- 即時、預約導引標籤 -->
						<nav style="width: 100%;">
							<div class="nav nav-tabs" id="nav-tab" role="tablist">
								<a class="nav-item nav-link active" id="nav-trans-now" data-toggle="tab" href="#nav-trans-now" 
									role="tab" aria-controls="nav-home" aria-selected="false">
									<spring:message code="LB.Immediately" />
								</a> 
							</div>
						</nav>
						
						<!-- 交易流程階段 -->
						<div class="col-12 tab-content" id="nav-tabContent">
							<div class="tab-pane fade " id="nav-trans-future" role="tabpanel"
								aria-labelledby="nav-profile-tab"></div>

							<!-- 預約才顯示轉帳日期 -->
							<div class="ttb-input-block tab-pane fade show active"
								id="nav-trans-now" role="tabpanel" aria-labelledby="nav-home-tab">
								<div class="ttb-message">
									<span></span>
								</div>
								
								<!-- 即時的屬性(不須顯示預設隱藏) -->
								<div class="ttb-input" style="display: none;">
									<label class="radio-block">
										<spring:message code="LB.Immediately" />
										<input type="radio" name="FGTXDATE" value="1" checked /> 
										<span class="ttb-radio"></span>
									</label>
								</div>
								
								<!-- 轉帳日期 -->
								<div id="transfer-date" class="ttb-input-item row" style="display: none;">
									<span class="input-title">
										<label><h4><spring:message code="LB.Transfer_date" /></h4></label>
									</span>
									<span class="input-block">
										<!-- 預約的屬性(必須顯示) -->
										<div class="ttb-input ">
											<label class="radio-block">
												<spring:message code="LB.Booking" /> 
												<input type="radio" name="FGTXDATE" value="2" /> 
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!-- 預約日期 -->
										<div class="ttb-input">
											<input type="text" id="CMDATE_TMP" value="" size="10"
												maxlength="10" class="text-input datetimepicker" />
											<span class="input-unit CMDATE">
												<img src="${__ctx}/img/icon-7.svg" />
											</span>
											<!-- 為了讓提示訊息對齊用，會在初始化時先隱藏 -->
											<span class="hideblock">
												<!-- 驗證用的input -->
												<input id="CMDATE" name="CMDATE" type="text" class="text-input validate[required, verification_date[BOOKING_DATE]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
									</span>
								</div>
								
								<%-- <!-- 代收截止日 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label><h4>代收截止日</h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<!-- 選擇代收截止日期 -->
											<input type="text" id="LIMDAT_TMP" value="" size="10" maxlength="10"
												placeholder="請選擇右側日曆" class="text-input datetimepicker" />
											<span class="input-unit LIMDAT">
												<img src="${__ctx}/img/icon-7.svg" />
											</span>
											<!-- 為了讓提示訊息對齊用，會在初始化時先隱藏；只會顯示提示訊息不會顯示欄位 -->
											<span class="hideblock">
												<!-- 代收截止日期 -->
												<input id="LIMDAT" name="LIMDAT" type="text" class="text-input validate[required,funcCall[validate_CheckDate['預約日', LIMDAT ,'2019/05/09', '2020/05/09']]] 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
									</span>
								</div> --%>
								
									<%-- if (!CheckDate("預約日", main.CMDATE, "<%= str_SystemDate %>", "<%= sNextYearDay %>")) return false; --%>						

								<div class="ttb-input-item row">
									<span class="input-title">
										<label><h4><spring:message code= "LB.D0374" /></h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
												<input type="radio" value="A" name="Q14"/>A﹒<spring:message code= "LB.X1695" />
												<input type="radio" value="B" name="Q14"/>B﹒<spring:message code= "LB.W1353" />
												<input type="radio" value="C" name="Q14"/>C﹒<spring:message code= "LB.X1696" />
												<input type="radio" value="D" name="Q14"/>D﹒<spring:message code= "LB.D1393" />
										</div>
										<span class="hideblock">
											<spring:message code= "LB.The_total_amount_payable" />
											<input id="Q14" name="Q14" type="radio"  
												class="validate[funcCall[validate_Radio['<spring:message code= "LB.D0374" />',Q14]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
										</span>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"><label><h4>checknumber</h4></label></span> 
									<span class="input-block">
										<div class="ttb-input">
											<input  id="num"  type="text" class="text-input validate[required,funcCall[validate_CheckNumber['checknumber',num]]]"/>
										</div>
									</span>
								</div>
								
								<!-- 應繳金額 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label><h4><spring:message code= "LB.Repayment_of_every_month" /></h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="AMOUNT_TMP" class="text-input" size="8" maxlength="8" value="" /> 
											<span class="input-unit"><spring:message code="LB.Dollar" /></span>
										</div>
										<!-- 為了讓提示訊息對齊用，會在初始化時先隱藏；只會顯示提示訊息不會顯示欄位 -->
										<span class="hideblock">
											<!-- 應繳總金額 -->
										<input id="AMOUNT" name="AMOUNT" type="text"
												class="text-input validate[required,min[20],max[20000],funcCall[validate_CheckAmount['checkamount',AMOUNT]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</span>
									</span>
								</div>
								
								
								<div class="ttb-input-item row">
									<span class="input-title"><label><h4>CheckTxnPassword</h4></label></span> 
									<span class="input-block">
										<div class="ttb-input">
											<input  id="txn"  type="password" class="text-input validate[required,funcCall[validate_CheckTxnPassword[txn]]]"/>
										</div>
									</span>
								</div>
								
								<div class="ttb-input-item row">
									<span class="input-title"><label><h4>Checkselect</h4></label></span> 
									<span class="input-block">
										<div class="ttb-input">
											<select name="ACNO" id="ACNO" class="select-input validate[funcCall[validate_CheckSelect['CheckSelect',ACNO,#]]]">
											<option value="#">----<spring:message code="LB.Select_account" />-----</option>
											<option value="1">1</option>
											<option value="2">2</option>
										</select>
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"><label><h4>CheckDate</h4></label></span> 
									<span class="input-block">
										<div class="ttb-input">
											<input  id="LIMDAT2"  type="text" class="text-input validate[funcCall[validate_CheckDate['CheckDate',LIMDAT2,2019/05/09,2019/05/12]]]"/>
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"><label><h4>CheckCDate</h4></label></span> 
									<span class="input-block">
										<div class="ttb-input">
											<input  id="LIMCDAT"  type="text" class="text-input validate[funcCall[validate_CheckCDate['CheckCDate',LIMCDAT,097/05/09,097/05/12]]]"/>
										</div>
									</span>
								</div>
								
								<div class="ttb-input-item row">
									<span class="input-title"><label><h4>CheckNumWithDigit</h4></label></span> 
									<span class="input-block">
										<div class="ttb-input">
											<input  id="checknum"  type="text" class="text-input validate[required,funcCall[validate_CheckNumWithDigit['CheckNumWithDigit',checknum,3]]]"/>
										</div>
									</span>
								</div>
								
								
								<div class="ttb-input-item row">
									<span class="input-title"><label><h4>EmailCheck</h4></label></span> 
									<span class="input-block">
										<div class="ttb-input">
											<input  id="email"  type="text" class="text-input validate[required,funcCall[validate_EmailCheck[email]]]"/>
										</div>
									</span>
								</div>
								
								<div class="ttb-input-item row">
									<span class="input-title"><label><h4>CheckDateScope</h4></label></span> 
									<span class="input-block">
										<div class="ttb-input">
											<input  id="startodate"  type="text" class="text-input"/>		
											<input  id="endodate"  type="text" class="text-input"/>	
											<input  id="odate"  type="text" class="text-input validate[required,funcCall[validate_CheckDateScope['<spring:message code= "LB.Inquiry_period_1" />', odate, startodate, endodate, false, 24, 2]]]"/>
										</div>
									</span>
								</div>
								
								<div class="ttb-input-item row">
									<span class="input-title"><label><h4>CheckCDateScope</h4></label></span> 
									<span class="input-block">
										<div class="ttb-input">		
											<input  id="startobdate"  type="text" class="text-input"/>		
											<input  id="endobdate"  type="text" class="text-input"/>		
											<input  id="obdate"  type="text" class="text-input validate[required,funcCall[validate_CheckCDateScope['<spring:message code= "LB.X1697" />', obdate, startobdate, endobdate, false, 24, 2]]]"/>
										</div>
									</span>
								</div>
								
								<div class="ttb-input-item row">
									<span class="input-title"><label><h4>checkSYS_IDNO</h4></label></span> 
									<span class="input-block">
										<div class="ttb-input">				
											<input type="text" name="INTSACN2" id="INTSACN2" class="text-input validate[required,funcCall[validate_checkSYS_IDNO[INTSACN2]]]"  />
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"><label><h4>CheckLenEqual</h4></label></span> 
									<span class="input-block">
										<div class="ttb-input">				
											<input type="text" name="lenequal" id="lenequal" class="text-input validate[required,funcCall[validate_CheckLenEqual['<spring:message code= "LB.X1698" />',lenequal,false,2]]]"  />
										</div>
									</span>
								</div>
								
								<div class="ttb-input-item row">
									<span class="input-title"><label><h4>chkChrNum</h4></label></span> 
									<span class="input-block">
										<div class="ttb-input">				
											<input type="text" name="chrnum" id="chrnum" class="text-input validate[required,funcCall[validate_chkChrNum['<spring:message code= "LB.X1699" />',chrnum]]]"  />
										</div>
									</span>
								</div>
								
								<div class="ttb-input-item row">
									<span class="input-title"><label><h4>CheckNotEmpty</h4></label></span> 
									<span class="input-block">
										<div class="ttb-input">				
											<input type="text" name="NotEmpty" id="NotEmpty" class="text-input validate[required]"  />
										</div>
									</span>
								</div>
								
								<div class="ttb-input-item row">
									<span class="input-title"><label><h4>CheckCardNum</h4></label></span> 
									<span class="input-block">
										<div class="ttb-input">				
											<input type="text" name="cardnum" id="cardnum" class="text-input validate[required,funcCall[validate_CheckNumWithDigit[ '<spring:message code= "LB.Credit_Card" />',cardnum,16]]]"  />
										</div>
									</span>
								</div>
								
							</div>
							
							<!-- 重新輸入、確認按鈕 -->
							<input id="CMRESET" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" /> 
							<input id="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
						
						</div>
					</div>
					
					<!-- 說明 -->
					<div class="text-left">
						<spring:message code="LB.Description_of_page" />:
						<ol class="list-decimal text-left">
							<li><spring:message code= "LB.Inquiry" />「<a href="${__ctx}/CUSTOMER/SERVICE/rate_table" target="_blank"><spring:message code= "LB.Transfer_P1_D1-2" /></a>」。 </li>
							<li><spring:message code= "LB.Electricity_Fee_P1_D2" /> </li>
							<li><spring:message code= "LB.Electricity_Fee_P1_D3" /></li>
							<li><spring:message code= "LB.Electricity_Fee_P1_D4" /></li>
							<li><spring:message code= "LB.Electricity_Fee_P1_D5" /> </li>
							<li><spring:message code= "LB.Electricity_Fee_P1_D6" /> </li>
<!-- 							<li><spring:message code= "LB.Electricity_Fee_P1_D7" /></li> -->
				        </ol>
					</div>
					
				</form>
			</section>
		</main>
	</div>

	<%@ include file="../index/footer.jsp"%>

</body>
</html>


					