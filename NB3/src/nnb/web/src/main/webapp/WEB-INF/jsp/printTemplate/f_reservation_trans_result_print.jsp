<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
<br/><br/>
<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
<br/><br/><br/>
<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
<br/><br/><br/>
<!-- 查詢時間 -->
<label><spring:message code="LB.Inquiry_time" /> ：</label><label>${CMQTIME}</label>
<br/><br/>
<!-- 查詢期間 -->
<label><spring:message code="LB.Inquiry_period_1" /> ：</label><label>${cmdate}</label>
<br/><br/>
<!-- 交易狀態-->
<label><spring:message code="LB.W0054" /> ：</label>
<label>
	<c:choose>
		<c:when test="${FGTXSTATUS == 'All'}">
			<spring:message code="LB.All" />
		</c:when>
		<c:when test="${FGTXSTATUS == '0'}">
			<spring:message code="LB.D1099" />
		</c:when>
		<c:when test="${FGTXSTATUS == '1'}">
			<spring:message code="LB.W0056" />
		</c:when>
		<c:otherwise>
			<spring:message code="LB.W0057" />
		</c:otherwise>
	</c:choose>
</label>
<br/><br/>
<!-- 資料總數 -->
<label><spring:message code="LB.Total_records" /> ：</label><label>${COUNT}</label> <spring:message code="LB.Rows" />
<br/><br/>
<table class="print">
			<thead>
				<tr>
		        	<th style="text-align:center";><spring:message code="LB.Report_name" /></th>
		        	<!-- 外匯轉帳/匯款結果（預約） -->
		        	<th colspan="11" class="DataCell"><spring:message code="LB.W0341" /></th>
		     	</tr>
			</thead>
			<thead>
			<tr style="text-align:center">
				<!-- 預約編號 -->
				<th><spring:message code="LB.Booking_number" /></th>
				<!-- 轉帳日期 -->								
				<th><spring:message code="LB.Transfer_date" /></th>
				<!-- 轉出帳號 -->
				<th data-breakpoints="xs sm"><spring:message code="LB.Payers_account_no" /></th>
				<!-- 轉出金額 -->
				<th data-breakpoints="xs sm"><spring:message code="LB.Deducted" /></th>
				<!-- 銀行名稱/轉入帳號 -->
				<th data-breakpoints="xs sm"><spring:message code="LB.Bank_name" /><br><spring:message code="LB.Payees_account_no" /></th>
				<!-- 轉入金額 -->
				<th><spring:message code="LB.Buy" /></th>
				<!-- 匯率-->
				<th data-breakpoints="xs sm"><spring:message code="LB.Exchange_rate" /></th>
				<!-- 手續費/郵電費 -->
				<th data-breakpoints="xs sm"><spring:message code="LB.D0507" /><br><spring:message code="LB.W0345" /></th>
				<!-- 國外費用 -->
				<th data-breakpoints="xs sm"><spring:message code="LB.W0346" /></th>
				<!-- 交易類別 -->
				<th data-breakpoints="xs sm"><spring:message code="LB.Transaction_type" /></th>
				<!-- 備註 -->
				<th data-breakpoints="xs sm"><spring:message code="LB.Note" /></th>
				<!-- 轉帳結果 -->
				<th><spring:message code="LB.W0063" /></th>
			</tr>
			</thead>	
		<c:forEach items="${dataListMap}" var="map">
			<tr style="text-align:center">
				<td>${map.FXSCHNO}</td>
				<td>${map.FXTXDATE}</td>                
				<td>${map.FXWDAC}</td>		
				<td>${map.FXWDCURR}<br>${map.FXWDAMT}</td>		          	               
				<td>
					${map.FXSVBH}
					<br>
					${map.FXSVAC}
				</td>								                
				<td>${map.FXSVCURR}<br>${map.FXSVAMT}</td>                
				<td>${map.FXEXRATE}</td>
				<td>${map.FXEFEECCY}<br>${map.FXEFEE}<br>${map.FXTELFEE}</td>
				<td>${map.FXEFEECCY}<br>${map.str_OURCHG}</td>
				<td>${map.ADOPID}</td>
				<td>${map.FXTXMEMO}</td>
				<td>
					<spring:message code="${map.FXTXSTATUS}" />
					${map.FXEXCODE}
				</td>
			</tr>
		</c:forEach>
</table>
<br/><br/>
	<!-- 說明 -->
	<div class="text-left">
		<spring:message code="LB.Description_of_page" />
		<ol class="list-decimal text-left">
			<li style="font-weight:bold; color:#FF0000;"><spring:message code="LB.F_Reservation_Trans_P2_D1" /></li>
			<li><spring:message code="LB.F_Reservation_Trans_P2_D2" /></li>
			<li><spring:message code="LB.F_Reservation_Trans_P2_D3" /></li>
		</ol>
	</div>
</body>
</html>