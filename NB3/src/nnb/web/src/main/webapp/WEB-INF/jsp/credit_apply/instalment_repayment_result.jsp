<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript" src="${__ctx}/js/checkutil.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<style>
   /* footable */
.table {
    border-collapse: separate;
    border-spacing: 0;
    width: 100%;
    border: solid #DDD 1px;
    -moz-border-radius: 6px;
    -webkit-border-radius: 6px;
    border-radius: 6px;
    margin: 20px auto;
}

.table th {
    cursor: n-resize;
}

.table td a {
    text-decoration: none;
    color: #000;
}

.table td a.origin {
    text-decoration: underline;
    color: blue;
}

.table td a:hover {}

.table.breakpoint>tbody>tr>td.expand {
    background: none;
    /*background: url('../img/plus.png') no-repeat 5px center;*/
    padding-left: 40px;
}

.table.breakpoint>tbody>tr>td.expand:before {
    content: "";
}

.table.breakpoint>tbody>tr.table-detail-show>td.expand {
    background: none;
    /*background: url('minus.png') no-repeat 5px center;*/
}

.table.breakpoint>tbody>tr.table-detail-show>td.expand:before {
    content: "";
}

.table.breakpoint>tbody>tr.table-row-detail {
    background: #FAFAFA;
}

.table>tbody>tr:hover {
    background: #fbf8e9;
}

.table.breakpoint>tbody>tr:hover:not (.table-row-detail) {
    cursor: pointer;
}

.table>tbody>tr>td, .table>thead>tr>th {
    border-left: 1px solid #DDD;
    border-top: 1px solid #DDD;
    padding: 8px;
    text-align: center;
}

.table>tbody>tr>td.table-cell-detail {
    border-left: none;
}

table>tbody>tr>td>span.table-toggle {
    display: inline;
}

.table>tbody>tr td:first-child {
    padding-left: 20px;
}

.table>thead>tr>th, .table>thead>tr>td {
    background-color: #EEEEEE;
    border-top: none;
    text-shadow: 0 1px 0 rgba(255, 255, 255, .5);
    font-weight: bold;
}

.table>thead>tr:first-child>th.table-first-column, .table>thead>tr:first-child>td.table-first-column {
    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;
    border-radius: 6px 0 0 0;
}

.table>thead>tr:first-child>th.table-last-column, .table>thead>tr:first-child>td.table-last-column {
    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;
    border-radius: 0 6px 0 0;
}

.table>thead>tr:first-child>th.table-first-column.table-last-column,
.table>thead>tr:first-child>td.table-first-column.table-last-column {
    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;
    border-radius: 6px 6px 0 0;
}

.table>tbody>tr:last-child>td.table-first-column {
    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;
    border-radius: 0 0 0 6px;
}

.table>tbody>tr:last-child>td.table-last-column {
    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;
    border-radius: 0 0 6px 0;
}

.table>tbody>tr:last-child>td.table-first-column.table-last-column {
    -moz-border-radius: 0 0 6px 6px;
    -webkit-border-radius: 0 0 6px 6px;
    border-radius: 0 0 6px 6px;
}

.table>thead>tr>th.table-first-column, .table>thead>tr>td.table-first-column,
.table>tbody>tr>td.table-first-column {
    border-left: none;
}

.table>tbody img {
    vertical-align: middle;
}

.table>tfoot>tr>th, .table>tfoot>tr>td {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc),
        to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image: -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image: -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image: -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image: linear-gradient(to bottom, #ebf3fc, #dce9f9);
    -webkit-box-shadow: 0 1px 0 rgba(255, 255, 255, .8) inset;
    -moz-box-shadow: 0 1px 0 rgba(255, 255, 255, .8) inset;
    box-shadow: 0 1px 0 rgba(255, 255, 255, .8) inset;
    border-top: 1px solid #ccc;
    text-shadow: 0 1px 0 rgba(255, 255, 255, .5);
    padding: 10px;
}

.table tbody>tr:nth-child(odd)>td, .table tbody>tr:nth-child(odd)>th {
    background-color: transparent;
}

.table-nav {
    list-style: none;
    flex: 1 0 70%;
    margin: 0;
}

.table-nav li {
    display: inline-block;
}

.table-nav li a {
    display: inline-block;
    padding: 5px 10px;
    text-decoration: none;
    font-weight: bold;
    color: #000;
}

.table-nav .table-page-current {
    border-radius: 50%;
    background-color: #FF6600;
}

.table-nav .table-page-current a {
    color: #fff;
}

table.table-details {
    text-align: left;
}

table.table-details th, table.table-details td {
    padding: 0px;
}

table.table-details>tbody>tr>th:nth-child(1), table.table-details>tbody>tr>td:nth-child(2) {
    display: inline-block !important;
    text-align: left !important;
    border-top: none;
}

table.table-details>tbody>tr>th:nth-child(1) {
    width: max-content;
    padding-right: 10px;
    width: -moz-max-content;
    width: -webkit-max-content;
    width: -o-max-content;
    width: -ms-max-content;
}
h3{
	margin-bottom:20px; !important
}
</style>
<script type="text/javascript">
$(document).ready(function(){
	$("#printbtn").click(function(){
		var i18n = new Object();
		i18n['jspTitle']='<spring:message code= "LB.D0194" />';
		var params = {
			"jspTemplateName":"instalment_repayment_result_print",
			"jspTitle":i18n['jspTitle'],
			"CUSTID":"${instalment_repayment_result.data.CUSTID}",
			"CUSNAME":"${instalment_repayment_result.data.CUSNAME}",
			"HPHONE":"${instalment_repayment_result.data.HPHONE}",
			"OPHONE":"${instalment_repayment_result.data.OPHONE}",
			"MPFONE":"${instalment_repayment_result.data.MPFONE}",
			"CARDNUM":"${instalment_repayment_result.data.CARDNUM}",
			"CURRBAL":"${instalment_repayment_result.data.CURRBAL}",
			"POT":"${instalment_repayment_result.data.POT}",
			"RATE":"${instalment_repayment_result.data.RATE}",
			"AMOUNT":"${instalment_repayment_result.data.AMOUNT}",
			"PERIOD":"${instalment_repayment_result.data.PERIOD}",
			"APPLY_RATE":"${instalment_repayment_result.data.APPLY_RATE}",
			"FIRST_AMOUNT":"${instalment_repayment_result.data.FIRST_AMOUNT}",
			"FIRST_INTEREST":"${instalment_repayment_result.data.FIRST_INTEREST}",
			"PERIOD_AMOUNT":"${instalment_repayment_result.data.PERIOD_AMOUNT}",
			"POTDESC":"${instalment_repayment_result.data.POTDESC}"
		}
		openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
	
	});
});
</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 長期使用循環信用申請分期還款     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0194" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>

		<!-- 	快速選單及主頁內容 -->
		 <main class="col-12">
            <section id="main-content" class="container">
				<form id="formId" method="post" action="${__ctx}/CREDIT/APPLY/instalment_repayment_result" >
				  
				  
                <h2><spring:message code="LB.D0194" /><!-- 長期使用循環信用申請分期還款 --></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>

                <div class="main-content-block row">
                    <div class="col-12">
                    	<div class="ttb-input-block">
                    		<div class="ttb-message">
                                <span style="color:red;font-weight:bold"><spring:message code= "LB.D0220" /></span>
                            </div>
                    		<div class="ttb-input-item row">
								<span class="input-title">
									<label><h4><spring:message code="LB.D0203" /><!-- 姓名 --></h4></label>
								</span>
								<span class="input-block">
									${instalment_repayment_result.data.CUSNAME}
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title">
									<label><h4><spring:message code="LB.D0204" /><!-- 身分證字號 --></h4></label>
								</span>
								<span class="input-block">
									${instalment_repayment_result.data.CUSTID}
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title">
									<label><h4><spring:message code="LB.D0205" /><!-- 連絡電話 --></h4></label>
								</span>
								<span class="input-block">
									<c:if test="${instalment_repayment_result.data.HPHONE !=''}">
									<spring:message code="LB.D0206" /><!-- 住家電話 -->：${instalment_repayment_result.data.HPHONE} <br>
									</c:if>
									<c:if test="${instalment_repayment_result.data.MPFONE !=''}">
									<spring:message code="LB.D0207" />：${instalment_repayment_result.data.MPFONE}<br>
									</c:if>
									<c:if test="${instalment_repayment_result.data.OPHONE !=''}">
									<spring:message code="LB.D0094" /><!-- 公司電話 -->：${instalment_repayment_result.data.OPHONE} <br>
									</c:if>
								</span><br>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title">
									<label><h4><spring:message code="LB.D0208" /><!-- 信用卡卡號 --></h4></label>
								</span>
								<span class="input-block">
										${instalment_repayment_result.data.CARDNUM}
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title">
									<label><h4><spring:message code="LB.D0209" /><!-- 可申請分期金額 --></h4></label>
								</span>
								<span class="input-block">
									${instalment_repayment_result.data.CURRBAL}
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title">
									<label><h4><spring:message code="LB.D0210" /><!-- 目前適用利率 --></h4></label>
								</span>
								<span class="input-block">
									${instalment_repayment_result.data.RATE}%&nbsp;${instalment_repayment_result.data.POTDESC}
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title">
									<label><h4><spring:message code="LB.D0211" /><!-- 分期總金額 --></h4></label>
								</span>
								<span class="input-block" id="AMOUNT_IUPUT">
									${instalment_repayment_result.data.AMOUNT}&nbsp;&nbsp;&nbsp;<spring:message code="LB.Dollar" /><!-- 元 -->
								</span>
							</div>
							
                    		<div class="ttb-input-item row">
								<span class="input-title">
									<label><h4><spring:message code="LB.D0212" /><!-- 分期期數 --></h4></label>
								</span>
								<span class="input-block" id="PERIOD_TEXT">
									${instalment_repayment_result.data.PERIOD}&nbsp;<spring:message code="LB.W0854" /><!-- 期 -->
								</span>
                        </div>
                        <div class="ttb-input-item row">
								<span class="input-title">
									<label><h4><spring:message code="LB.D0213" /><!-- 核定利率 --></h4></label>
								</span>
								<span class="input-block">
									${instalment_repayment_result.data.APPLY_RATE}%
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title">
									<label><h4><spring:message code="LB.D0214" /><!-- 本金一期 --></h4></label>
								</span>
								<span class="input-block">
									${instalment_repayment_result.data.FIRST_AMOUNT}&nbsp;<spring:message code="LB.Dollar" /><!-- 元 --><br>
									(<spring:message code="LB.D0215" /><!-- 不含利息，分期總金額除以分期期數後之剩餘款項將併入第一期分期金額 -->)
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title">
									<label><h4><spring:message code="LB.D0216" /><!-- 第一期利息 --></h4></label>
								</span>
								<span class="input-block">
									${instalment_repayment_result.data.FIRST_INTEREST}&nbsp;<spring:message code="LB.Dollar" /><!-- 元 --><br>
									(<spring:message code="LB.D0217" /><!-- 此金額為試算，以帳單為主 -->)
								</span>
							</div>
							<br><br>
							<div class="text-center">
							<input class="ttb-button btn-flat-orange"  type="button" value="<spring:message code= "LB.Print" />" id="printbtn" >
							</div>
							<br>
						 <div class="ttb-message text-left" style="width: 85%;">
                            <span><spring:message code= "LB.X0880" />：1.<spring:message code= "LB.X0881" /></span><br>
      	  					<span>　　2.<spring:message code= "LB.X0874" /></span><br>
      	  					<span>　　3.<spring:message code= "LB.X0883" /></span><br>      	  
      	  					<span>　　4.<spring:message code= "LB.X0884" /></span> 
                        </div>
						<div class="CN19-clause" style="width: 85%;margin:auto;overflow:auto;">
                        <table class="table" data-show-toggle="false">
                            <thead>
                                <tr>
                                    <th data-title="分期期數" rowspan="2"><spring:message code="LB.D0212" /><!-- 分期期數 --></th>
                                    <th data-title="差別利率" colspan="5"><spring:message code="LB.D0199" /><!-- 差別利率 --></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th></th>
									<td nowrap><spring:message code="LB.X0875" /></td><!-- 第一級 -->
                                    <td nowrap><spring:message code="LB.X0876" /></td><!-- 第二級 -->
                                    <td nowrap><spring:message code="LB.X0877" /></td><!-- 第三級 -->
                                    <td nowrap><spring:message code="LB.X0878" /></td><!-- 第四級 -->
                                    <td nowrap><spring:message code="LB.X0879" /></td><!-- 第五級 -->
                                </tr>
								 <tr data-expanded="false">
                                    <td class="expand">6<spring:message code="LB.W0854" /></td><!-- 期 -->
									<td><spring:message code="LB.X0858" />1.25%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />1.25%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />1.25%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0859" />1.25%</td><!-- 減降至第三級再減 -->
                                    <td></td>
                                </tr>
								<tr data-expanded="false">
                                    <td class="expand">12<spring:message code="LB.W0854" /></td><!-- 期 -->
									<td><spring:message code="LB.X0858" />1%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />1%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />1%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0859" />1%</td><!-- 減降至第三級再減 -->
                                    <td></td>
                                </tr>
								<tr data-expanded="false">
                                    <td class="expand">18<spring:message code="LB.W0854" /></td><!-- 期 -->
									<td><spring:message code="LB.X0858" />0.75%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.75%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.75%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0859" />0.75%</td><!-- 減降至第三級再減 -->
                                    <td></td>
                                </tr>
								<tr data-expanded="false">
                                    <td class="expand">24<spring:message code="LB.W0854" /></td><!-- 期 -->
									<td><spring:message code="LB.X0858" />0.5%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.5%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.5%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0859" />0.5%</td><!-- 減降至第三級再減 -->
                                    <td></td>
                                </tr>
								<tr data-expanded="false">
                                    <td class="expand">30<spring:message code="LB.W0854" /></td><!-- 期 -->
									<td><spring:message code="LB.X0858" />0.25%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.25%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.25%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0859" />0.25%</td><!-- 減降至第三級再減 -->
                                    <td></td>
                                </tr>
								<tr data-expanded="false">
                                    <td class="expand">36<spring:message code="LB.W0854" /></td><!-- 期 -->
									<td><spring:message code="LB.X0858" />0.2%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.2%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.2%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0859" />0.2%</td><!-- 減降至第三級再減 -->
                                    <td></td>
                                </tr>
								<tr data-expanded="false">
                                    <td class="expand">48<spring:message code="LB.W0854" /></td><!-- 期 -->
									<td><spring:message code="LB.X0858" />0.15%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.15%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.15%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0859" />0.2%</td><!-- 減降至第三級再減 -->
                                    <td></td>
                                </tr>
								<tr data-expanded="false">
                                    <td class="expand">60<spring:message code="LB.W0854" /></td><!-- 期 -->
									<td><spring:message code="LB.X0858" />0.05%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.05%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0858" />0.05%</td><!-- 減 -->
                                    <td><spring:message code="LB.X0859" />0.4%</td><!-- 減降至第三級再減 -->
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
						</div>
					
                    </div>
                </div>
                </form>
            </section>
        </main>
	</div>
	<!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>