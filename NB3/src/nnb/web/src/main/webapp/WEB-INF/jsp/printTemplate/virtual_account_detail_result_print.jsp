<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js_u2.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
<br/><br/>
<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
<br/><br/><br/>
<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
<br/><br/><br/>
<label><spring:message code="LB.Inquiry_time" /> ：</label><label>${CMQTIME}</label>
<br/><br/>
<label><spring:message code="LB.Account" /> ：</label><label>${ACN}</label>
<br/><br/>
<label><spring:message code="LB.Inquiry_period" /> ：</label><label>${CMPERIOD}</label>
<br/><br/>
<label><spring:message code="LB.Total_records" /> ：</label><label>${CMRECNUM}</label> <label><spring:message code="LB.Rows" /></label>
<br/><br/>

<table class="print">
	<tr>
		<td style="text-align:center"><spring:message code="LB.Transaction_date" /></td>
		<td style="text-align:center"><spring:message code="LB.Summary_1" /></td>
		<td style="text-align:center"><spring:message code="LB.Deposit_amount_1" /></td>
		<td style="text-align:center"><spring:message code="LB.Account_entry_time" /></td>
		<td style="text-align:center"><spring:message code="LB.Virtual_account" /></td>
		<td style="text-align:center"><spring:message code="LB.Note" /></td>
	</tr>
	<c:forEach items="${dataListMap}" var="map">
	<tr>
		<td style="text-align:center">${map.LSTLTD_c}</td>
		<td style="text-align:center">${map.MEMO_C}</td>
		<td style="text-align: right">${map.AMTTRN}</td>
		<td style="text-align:center">${map.LSTIME_c}</td>
		<td style="text-align:center">${map.DATA25}</td>
		<td style="text-align:center">${map.TRNSRC_C}</td>
	</tr>
	</c:forEach>
</table>
<br/><br/>
	<div class="text-left">
		<spring:message code="LB.Description_of_page" />
		<ol class="list-decimal text-left">
<%-- 			<li><spring:message code="LB.Demand_Virtual_detail_P2_D1" /></li> --%>
			<li><spring:message code="LB.Demand_Virtual_detail_P2_D1" /></li>
		</ol>
	</div>
</body>
</html>