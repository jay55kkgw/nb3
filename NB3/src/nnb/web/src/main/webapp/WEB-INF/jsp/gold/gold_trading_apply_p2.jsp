<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	    <script type="text/javascript">
        $(document).ready(function () {
            initFootable(); // 將.table變更為footable 
            init();
        });

        function init() {
	    	$("#pageshow").click(function(e){			
	        	initBlockUI();
				var action = '${__ctx}/GOLD/APPLY/gold_trading_apply_p3';
				$("form").attr("action", action);
    			$("form").submit();
			});
	    	$("#back").click(function(e){			
	        	initBlockUI();
				var action = '${__ctx}/INDEX/index';
				$("form").attr("action", action);
    			$("form").submit();
			});
        }

        function checkReadFlag()
        {
        	console.log($("#ReadFlag").prop('checked'));
            if ($("#ReadFlag").prop('checked'))    		
          	{
           		$("#pageshow").prop('disabled',false);
           		$("#pageshow").addClass("btn-flat-orange");
          	}
          	else
          	{
           		$("#pageshow").prop('disabled',true);
           		$("#pageshow").removeClass("btn-flat-orange");
       	  	}	
        }
    </script>
    <style>
    	.DataCell{
    	    text-align: left;
    	}
    </style>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 黃金存摺帳戶     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2273" /></li>
    <!-- 黃金存摺網路交易申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1687" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!-- 線上申請黃金存摺網路交易 -->
					<spring:message code="LB.W1687"/>
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <div id="step-bar">
                    <ul>
                       <li class="active">注意事項與權益</li>
                        <li class="">設定帳戶</li>
                        <li class="">確認資料</li>
                        <li class="">申請結果</li>
                    </ul>
                </div>
                <form id="formId" method="post">
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12 terms-block">
	                        <div class="ttb-message">
	                            <p>顧客權益</p>
	                        </div>
	                        <p class="form-description">請您審閱以下顧客權益與說明。</p><div class="text-left">
                            <ul>
                                <li data-num="">
                                    <span class="input-subtitle subtitle-color">黃金存摺網路交易申請暨約定書</span>
							<!-- 為確保您的權益，請詳細閱讀本約定條款所載事項，您如接受本約定條款請按 -->
							<!-- 我同意約定條款 -->
							<!-- 鍵，以完成申請作業，您如不同意條款內容，則請按 -->
							<!-- 取消 -->
							<!-- 鍵，本行將不受理您的申請。 -->
														
					    	
		                      		<div class="CN19-clause" style="width: 100%; height: 210px; margin: 20px auto; border: 1px solid #D7D7D7; border-radius: 6px;">
<%-- 		                      			<div class="MessageBar"><p align="left"><font color="royalblue" size="3"><b><spring:message code= "LB.X1208" /><font color=red><spring:message code="LB.W1554"/></font><spring:message code= "LB.X1209" /><font color=red><spring:message code="LB.Cancel"/></font><spring:message code= "LB.X1210" /></b></font></P></div> --%>
								    	<table width="100%" class="DataBorder">
								
							    	    	<tr>
			                                   <!-- 第一條 -->  
									    		<td class="ColorCell" style="width:5em" >第一條</td>
									           <!-- 服務時間-->  
									        	<td class="DataCell"><b>服務時間</b></td>    		
									
									    	</tr>    	
									
									    	<tr>
									
									        	<td class="ColorCell"></td>
									            <!-- 本人（以下簡稱立約人）以臺灣中小企業銀行（以下簡稱貴行）之網路銀行服	務系統（以下簡稱本系統），辦理黃金存摺之買進、回售、預約買進、預約回售、定期定額申購、定期定額約定變更或查詢資料等服務時，應於貴行網站公告之服務時間內為之。 -->
									        	<td class="DataCell">本人（以下簡稱立約人）以臺灣中小企業銀行（以下簡稱貴行）之網路銀行服	務系統（以下簡稱本系統），辦理黃金存摺之買進、回售、預約買進、預約回售、定期定額申購、定期定額約定變更或查詢資料等服務時，應於貴行網站公告之服務時間內為之。</td>
									
									        </tr>
									
									    	<tr>
									           <!-- 第二條 -->
									    		<td class="ColorCell" style="width:5em" >第二條</td>
									 <!-- 約定帳號（即網路銀行交易指定帳戶） -->
									        	<td class="DataCell"><b>約定帳號（即網路銀行交易指定帳戶）</b></td>    		
									
									    	</tr>         
									
									      	<tr>
									
									       		<td class="ColorCell"></td>
									<!-- 以本系統辦理黃金存摺之買進、回售時，立約人之約定轉出帳號及約定轉入帳號，即黃金存摺帳號與新臺幣存款帳號，限立約人於貴行開立之帳戶，立約人之扣款帳號如未預先約定或因結清銷戶或其他任何原因，致該扣款帳號不存在時，立約人應以臨櫃方式辦理買進或回售。 -->
									        	<td class="DataCell">以本系統辦理黃金存摺之買進、回售時，立約人之約定轉出帳號及約定轉入帳號，即黃金存摺帳號與新臺幣存款帳號，限立約人於貴行開立之帳戶，立約人之扣款帳號如未預先約定或因結清銷戶或其他任何原因，致該扣款帳號不存在時，立約人應以臨櫃方式辦理買進或回售。<br>
									<!-- 立約人若於完成黃金存摺之預約買進、預約回售後，辦理變更約定新臺幣存款帳號者，該筆預約交易於預約後的第一個營業日仍依原約定新臺幣存款帳號作為黃金買進扣款帳號、黃金回售入帳帳號。 -->
									        		<font color=red>立約人若於完成黃金存摺之預約買進、預約回售後，辦理變更約定新臺幣存款帳號者，該筆預約交易於預約後的第一個營業日仍依原約定新臺幣存款帳號作為黃金買進扣款帳號、黃金回售入帳帳號。</font>        	
									
									        	</td>
									
									      	</tr>
									
									    	<tr>
									<!-- 第三條 -->
									    		<td class="ColorCell" style="width:5em" >第三條</td>
									<!-- 不可抗力 -->
									        	<td class="DataCell"><b>不可抗力</b></td>    		
									
									    	</tr>               	
									
									     	<tr>
									
									        	<td class="ColorCell"></td>
									<!-- 如因不可抗力事由或其他原因，包括但不限於斷電、斷線、電信壅塞、網路傳輸干擾、貴行之電腦系統故障或第三人破壞等，致使立約人所為交易或其他指示遲延完成或無法按立約人指示完成、或致	使貴行未能提供本系統服務者，立約人同意貴行不負任何賠償責任。 -->
									        	<td class="DataCell">如因不可抗力事由或其他原因，包括但不限於斷電、斷線、電信壅塞、網路傳輸干擾、貴行之電腦系統故障或第三人破壞等，致使立約人所為交易或其他指示遲延完成或無法按立約人指示完成、或致	使貴行未能提供本系統服務者，立約人同意貴行不負任何賠償責任。<br>
									<!-- 立約人預約交易後第一營業日若因天然災害或其他不可抗力之原因，致無黃金牌告價時，則該預約交易無效。 -->
									        						立約人預約交易後第一營業日若因天然災害或其他不可抗力之原因，致無黃金牌告價時，則該預約交易無效。
									
									        	</td>
									
									      	</tr>
									
									    	<tr>
									<!-- 第四條 -->
									    		<td class="ColorCell" style="width:5em" >第四條</td>
									<!-- 系統服務 -->
									        	<td class="DataCell"><b>系統服務</b></td>    		
									
									    	</tr>               	
									
									      	<tr>
									
									        	<td class="ColorCell"></td>
									<!-- 立約人同意貴行於貴行網站公告本系統新增或異動（含調整、變更或取消）之服務項目及其相關規定時，除貴行另有規定外，立約人無須另填申請書，即可使用本系統新增或異動後之服務項目；立約人一經進入本系統並使用該新增或異動後之服務項目時，即視為立約人同意依貴行網站所公告本系統新增或異動服務項目之相關規定辦理，且同意受該規定拘束。 -->
									        	<td class="DataCell">立約人同意貴行於貴行網站公告本系統新增或異動（含調整、變更或取消）之服務項目及其相關規定時，除貴行另有規定外，立約人無須另填申請書，即可使用本系統新增或異動後之服務項目；立約人一經進入本系統並使用該新增或異動後之服務項目時，即視為立約人同意依貴行網站所公告本系統新增或異動服務項目之相關規定辦理，且同意受該規定拘束。</td>
									
									        </tr>
									
									    	<tr>
									<!-- 第五條 -->
									    		<td class="ColorCell" style="width:5em" >第五條</td>
									<!-- 投資風險 -->
									    		<td class="DataCell"><b>投資風險</b></td>	
									
									    	</tr>
									
									    	<tr>
									
									        	<td class="ColorCell"></td>
									
									        	<td class="DataCell">
									        	<!-- 國際黃金價格有漲有跌，存戶投資黃金可能產生本金收益或損失，最大可能損失為買進金額之全部，請自行審慎判斷投資時機並承擔投資風險，辦理黃金存摺各項交易，如有涉及贈與、繼承及應繳稅捐等情事，悉由存戶或繼承人自行申報與負擔，黃金存摺不計算利息，亦非屬存款保險條例規定之標的，不受存款保險保障。 -->
												<font color=red><b>國際黃金價格有漲有跌，存戶投資黃金可能產生本金收益或損失，最大可能損失為買進金額之全部，請自行審慎判斷投資時機並承擔投資風險，辦理黃金存摺各項交易，如有涉及贈與、繼承及應繳稅捐等情事，悉由存戶或繼承人自行申報與負擔，黃金存摺不計算利息，亦非屬存款保險條例規定之標的，不受存款保險保障。</b></font></td>
									
									        </tr>
									
									        <tr>
									<!-- 第六條 -->
									    		<td class="ColorCell" style="width:5em" >第六條</td>
									<!-- 立約人應一併遵守其與貴行訂定之黃金存摺開戶約定條款之約定及相關法令之規定。 -->
									    		<td class="DataCell">立約人應一併遵守其與貴行訂定之黃金存摺開戶約定條款之約定及相關法令之規定。</td>	
									
									    	</tr>
									
									    	<tr>
									<!-- 第七條 -->
									    		<td class="ColorCell" style="width:5em" >第七條</td>
									<!-- 立約人同意貴行得依業務需要，隨時修改本約定書開立帳戶之相關服務內容，並在貴行網站上公告其內容，以代通知，修改後之交易，立約人願自動適用異動後之服務內容，毋須另行約定。 -->
									    		<td class="DataCell">立約人同意貴行得依業務需要，隨時修改本約定書開立帳戶之相關服務內容，並在貴行網站上公告其內容，以代通知，修改後之交易，立約人願自動適用異動後之服務內容，毋須另行約定。</td>	
									
									    	</tr>           	        
									        
											<tr>
									<!-- 第八條 -->
									    		<td class="ColorCell" style="width:5em" >第八條</td>
									<!-- 手續費收費標準 -->
									    		<td class="DataCell"><b>手續費收費標準</b></td>	
									
									    	</tr>
											
											<tr>
									            <td class="ColorCell"></td>
												
									    		<td class="DataCell">
												<!-- 立約人同意依貴行下列所訂之收費標準繳納相關費用，並授權貴行自立約人之帳戶內自動扣繳；收費標準於訂約後如有變更或調整，貴行應於生效日六十日前以顯著方式於營業場所、貴行網站公告其內容，並以電子郵件方式使立約人得知調整費用，立約人若對於該變更或調整有異議時，得於前開公告期間內終止本約定事項，逾期未終止者，視為同意該變更或調整： -->
												立約人同意依貴行下列所訂之收費標準繳納相關費用，並授權貴行自立約人之帳戶內自動扣繳；收費標準於訂約後如有變更或調整，貴行應於生效日六十日前以顯著方式於營業場所、貴行網站公告其內容，並以電子郵件方式使立約人得知調整費用，立約人若對於該變更或調整有異議時，得於前開公告期間內終止本約定事項，逾期未終止者，視為同意該變更或調整：<br />
												<!-- 一、線上申請黃金存摺帳戶，每戶收費新台幣50元。 -->
												一、線上申請黃金存摺帳戶，每戶收費新台幣50元。<br />
												<!-- 二、黃金存摺網路交易定期定額扣款成功，每戶收費新台幣50元 -->
												二、黃金存摺網路交易定期定額扣款成功，每戶收費新台幣50元
												
												</td>	
									
									    	</tr>  
								    	</table>
									</div>
									</li>
	                            </ul>
	                           
                            	<span class="input-block">
                                	<div class="ttb-input">
			                            <label class="check-block"  for="ReadFlag"> 
										<!-- 本人已詳細閱讀並清楚以上所有內容 -->
											<input type="checkbox" name="ReadFlag" id="ReadFlag" onclick="checkReadFlag()"><spring:message code="LB.D1064"/>
											<span class="ttb-check"></span>
										</label>
                                	</div>
                            	</span>
	                        </div>
	                        <!--button 區域 -->
                           <!-- 取消 -->
                            <input id="back" name="back" type="button" class="ttb-button btn-flat-gray" value="不同意並離開" />
                           <!-- 我同意約定條款 -->
                            <input id="pageshow" name="pageshow" type="button" class="ttb-button" value="同意並繼續" disabled/>
                        <!--                     button 區域 -->
                        </div>  
                    </div>
                </form>

	
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>