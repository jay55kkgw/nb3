<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
</head>
<body>
	<div class="ttb-message">
	<!-- 電子銀行業務總契約書 -->
	<!-- 為確保您的權益，請詳細閱讀本約定條款所載事項，您如接受本約定條款請按 我同意約定條款-->
	<!-- 鍵，以完成申請作業，您如不同意條款內容，則請按 取消-->
	<!-- 鍵，本行將不受理您的申請。 -->
		<p><spring:message code= "LB.X1078" /></p>
		<p align="left">
			<font color="blue" size="3"> <b><spring:message code= "LB.X1208" /><span
					style="border: 1px solid #000000; background-color: #FF0000; color: #FFFFFF;"><spring:message code= "LB.W1554" /></span><spring:message code= "LB.X1209" /><span
					style="border: 1px solid #000000; background-color: #FF0000; color: #FFFFFF;"><spring:message code= "LB.Cancel" /></span><spring:message code= "LB.X1210" />
			</b>
			</font>
		</p>
	</div>

	<div align="left">
		<h4>壹、線上申請網路銀行約定事項</h4>
	</div>
	
	<ul class="ttb-result-list" style="list-style: decimal;">
		<ul><span class="input-subtitle subtitle-color">第一條、服務範圍</span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">使用晶片金融卡申請網路銀行：係指立約人經由連結網際網路之電腦，以身分證字號/統一編號、出生年月日、晶片金融卡密碼等真實之立約人資料，向貴行線上申請網路銀行服務後，即可使用網路銀行查詢立約人的存款、放款、基金、外匯、信用卡等資料、掛失及申請信用卡的電子帳單、補寄帳單、電子郵件繳款通知等服務。<br>服務項目如有變動，由貴行於貴行網站公告，除貴行另有規定外，無須另行通知立約人，亦無須另立書面約定。</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color">第二條、掛失</span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">立約人利用貴行網路銀行辦理金融卡、存摺、存單及印鑑等掛失後，仍應儘速於貴行營業時間內持身分證、存款印鑑（印鑑掛失者另攜新印鑑），親自向原開戶行辦理書面掛失手續。</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color">第三條、密碼管理</span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">使用晶片金融卡申請網路銀行：立約人首次登入網路銀行應進行密碼變更。因遺忘使用者名稱、簽入密碼、交易密碼或連續輸入錯誤達五次遭系統停止使用時，立約人應親赴貴行辦理重置密碼手續。<br>若使用者名稱或簽入密碼或交易密碼有遭他人得知、洩漏或竊用之虞者，立約人應自行登入網路銀行變更密碼，並即依上開手續辦理。</ul>
		</ul>
	</ul>
	<div align="left">
		<h4>貳、共通約定事項</h4>
	</div>
	<ul class="ttb-result-list" style="list-style: decimal;  display:inherit;">
	    <ul><span class="input-subtitle subtitle-color">第一條、貴行資訊</span>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
			一、貴行名稱： 臺灣中小企業銀行 <br>
			二、申訴及客服專線：0800-00-7171<br>
			三、網址：https://www.tbb.com.tw<br>
			四、地址： 台北市塔城街30號 <br>
			五、傳真號碼：02-2550-8338<br>
			六、貴行電子信箱：臺灣企銀網站 https://www.tbb.com.tw 客服信箱 （tbb@mail.tbb.com.tw） <br>
			</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color">第二條、適用範圍</span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">本契約係網路銀行業務（含行動銀行）服務之一般性共同約定，除個別契約另有約定外，悉依本契約之約定。 <br>
			個別契約不得牴觸本契約。但個別契約對立約人之保護更有利者，從其約定，所指之保護，不含貴行基於業務考量所為之各項作業推廣措施。 <br>
			本契約條款如有疑義時，應為有利於消費者之解釋。<br>			  
			</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color">第三條、名詞定義</span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
			一、「網路銀行業務」：指立約人端電腦經由網際網路或行動網路與貴行電腦連線，無須親赴貴行櫃台，即可直接取得貴行所提供之各項金融服務。<br>
			二、「電子文件」：指貴行或立約人經由網路連線傳遞之文字、聲音、圖片、影像、符號或其他資料，以電子或其他以人之知覺無法直接認識之方式，所製成足以表示其用意之紀錄，而供電子處理之用者。 <br>
			三、「數位簽章」：指將電子文件以數學演算法或其他方式運算為一定長度之數位資料，以簽署人之私密金鑰對其加密，形成電子簽章，並得以公開金鑰加以驗證者。<br>
			四、<font color=red>「憑證」：指載有簽章驗證資料，用以確認簽署人身分、資格之電子形式證明。</font><br>
			五、「私密金鑰」：係指具有配對關係之數位資料中，由簽署人保有，用以製作數位簽章者。<br>
			六、「公開金鑰」：係指具有配對關係之數位資料中，對外公開，用以驗證數位簽章者。<br>
			七、「行動銀行」：指立約人下載貴行所提供之行動銀行軟體，並使用智慧型手機經由網際網路或行動網路與貴行電腦連線，無須親赴貴行櫃台，即可直接取得貴行所提供之各項金融服務。<br>
			八、「服務時間」：除了另行公告服務時間之項目外，提供二十四小時服務。<br>
			</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color"><font color=red>第四條、網頁之確認</font></span><br>
	         <ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
	        	 <font color=red>立約人使用網路銀行前，請先確認網路銀行正確之網址，才使用網路銀行服務；如有疑問，請電客服電話詢問。貴行應以一般民眾得認知之方式，告知立約人網路銀行應用環境之風險。貴行應盡善良管理人之注意義務，隨時維護網站的正確性與安全性，並隨時注意有無偽造之網頁，以避免立約人之權益受損。 </font>
	         </ul>
		</ul>
         <ul><span class="input-subtitle subtitle-color">第五條、服務項目</span><br>
	         <ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
	         貴行應於本契約載明提供之服務項目，如於網路銀行網站呈現相關訊息者，並應確保該訊息之正確性，其對立約人所負之義務不得低於網站之內容。 
	         </ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color">第六條、連線所使用之網路</span><br>
	    	<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
	    	貴行及立約人同意使用網路進行電子文件傳送及接收。貴行及立約人應分別就各項權利義務關係與各該網路業者簽訂網路服務契約，並各自負擔網路使用之費用。
			</ul>
		</ul>	
		<ul><span class="input-subtitle subtitle-color"><font color=red>第七條、電子文件之接收與回應</font></span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
			<font color=red>貴行接收含數位簽章或經貴行及立約人同意用以辨識身分之電子文件後，除查詢之事項外，貴行應提供該交易電子文件中重要資訊之網頁供立約人再次確認後，即時進行檢核及處理，並將檢核及處理結果，以電子文件之方式通知立約人。貴行或立約人接收來自對方任何電子文件，若無法辨識其身分或內容時，視為自始未傳送。但貴行可確定立約人身分時，應立即將內容無法辨識之事實，以電子文件之方式通知立約人。</font>
			</ul>
		</ul>					
		<ul><span class="input-subtitle subtitle-color"><font color=red>第八條、電子文件之不執行</font></span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
				<font color=red>如有下列情形之一，貴行得不執行任何接收之電子文件：</font>
				<ul style="list-style: decimal; list-style-position: inside;margin-left: 25px;margin-right: 10px;">
					<font color=red>一、有具體理由懷疑電子文件之真實性或所指定事項之正確性者。<br>
					二、貴行依據電子文件處理，將違反相關法令之規定者。<br>
					三、貴行因立約人之原因而無法於帳戶扣取立約人所應支付之費用者。<br></font>
				</ul>
				<font color=red>貴行不執行前項電子文件者，應同時將不執行之理由及情形，以電子文件之方式通知立約人，立約人受通知後得以電話或電子郵件方式向貴行確認。</font>
			</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color"><font color=red>第九條、電子文件交換作業時限</font></span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
			<font color=red>電子文件係由貴行電腦自動處理，立約人發出電子文件，經立約人依第七條第一項貴行提供之再確認機制確定其內容正確性後，傳送至貴行後即不得撤回。但未到期之預約交易在貴行規定之期限內，得撤回、修改。<br>若電子文件經由網路傳送至貴行後，於貴行電腦自動處理中已逾貴行營業時間時（營業時間：為星期一至星期五（例假日除外）及政府公告補上班日，上午九時至下午三時三十分），貴行應即以電子文件通知立約人，該筆交易將改於次一營業日處理或依其他約定方式處理。</font>
			</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color">第十條、系統故障之權宜處理</span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
				立約人同意當連線設備或貴行系統或第三人網路服務業系統發生故障時，貴行得採行必要之權宜措施處理。
			</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color"><font color=red>第十一條、費用</font></span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
			<font color=red>
				立約人自使用本契約服務之日起，同意依貴行「一般網路銀行收費標準表」(如附件)所訂定之收費標準繳納相關費用，並授權貴行自立約人之帳戶內自動扣繳；如未記載者，貴行不得收取。<br>
				前項收費標準於訂約後如有調整者，貴行應於貴行網站之明顯處公告其內容，並以電子文件之方式使立約人得知（以下稱通知）調整之內容。<br>
				第二項之調整如係調高者，貴行應於網頁上提供立約人表達是否同意費用調高之選項。立約人未於調整生效日前表示同意者，貴行將於調整生效日起暫停立約人使用網路銀行一部或全部之服務。立約人於調整生效日後，同意費用調整者，貴行應立即恢復網路銀行契約相關服務。<br>
				前項貴行之公告及通知應於調整生效六十日前為之，且調整生效日不得早於公告及通知後次一年度之起日。
           </font>
			</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color">第十二條、立約人軟硬體安裝與風險</span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
			立約人申請使用本契約之服務項目，應自行安裝所需之電腦軟體、硬體，以及其他與安全相關之設備。安裝所需之費用及風險，由立約人自行負擔。<br>
			<font color=red>第一項軟硬體設備及相關文件如係由貴行所提供，貴行僅同意立約人於約定服務範圍內使用，不得將之轉讓、轉借或以任何方式交付第三人。</font><br>
			貴行並應於網站及所提供軟硬體之包裝上載明進行本服務之最低軟硬體需求，且負擔所提供軟硬體之風險。<br>
			立約人於契約終止時，如貴行要求返還前項之相關設備，應以契約特別約定者為限。
			</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color">第十三條、立約人連線與責任</span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">立約人申請使用本契約之服務項目，應自行安裝所需之電腦軟體、硬體，以及其他與安全相關之設備。安裝所需之費用及風險，由立約人自行負擔。<br>
			貴行與立約人有特別約定者，必須為必要之測試後，始得連線。<br>
			<font color=red>立約人對貴行所提供之使用者代號、密碼、憑證及其它足以識別身分之工具，應負保管之責。</font><br>
			<font color=red>立約人輸入前項密碼連續錯誤達五次時，貴行電腦即自動停止立約人使用本契約之服務。立約人如擬恢復使用，應依約定辦理相關手續。惟網路銀行未屆轉帳日之各項預約轉帳仍屬有效，將於轉帳日依預約指示轉帳。</font>
			</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color"><font color=red>第十四條、交易核對</font></span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
			<font color=red>貴行於每筆交易指示處理完畢後，以電子文件、電話或書面之方式通知立約人，立約人應核對其結果有無錯誤。如有不符，應於使用完成之日起四十五日內，以電話或電子郵件方式通知貴行查明。<br></font>
			<font color=red>貴行應於每月對立約人以書面或電子文件方式寄送上月之交易對帳單（該月無交易時不寄）。立約人核對後如認為交易對帳單所載事項有錯誤時，應於收受之日起四十五日內，以電話或電子郵件方式通知貴行查明。<br></font>
			<font color=red>貴行對於立約人之通知，應即進行調查，並於通知到達貴行之日起三十日內，將調查之情形或結果以書面方式覆知立約人。</font>
			</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color">第十五條、跨行／跨網交易</span><br>
		    <ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
		           電子轉帳為跨行或跨網交易時，貴行執行電子訊息之傳送後，不負責他行或交換中心之行為或不行為及因該行為或不行為所造成之損害。
		    </ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color">第十六條、電子文件錯誤之處理</span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
			立約人利用本契約之服務，其電子文件如因不可歸責於立約人之事由而發生錯誤時，貴行應協助立約人更正，並提供其他必要之協助。<br>
			前項服務因可歸責於貴行之事由而發生錯誤時，貴行應於知悉時，立即更正，並同時以電子文件或雙方約定之方式通知立約人。<br>
			<font color=red></font>立約人利用本契約之服務，其電子文件因可歸責於立約人之事由而發生錯誤時，倘屬立約人申請或操作轉入之金融機構代號、存款帳號或金額錯誤，致轉入他人帳戶或誤轉金額時，一經立約人通知貴行，貴行應即辦理以下事項：</font>
				<ul style="list-style: decimal; list-style-position: inside;margin-left: 25px;margin-right: 10px;">
				<font color=red>
					一、依據相關法令提供該筆交易之明細及相關資料。<br>
					二、通知轉入行協助處理。<br>
					三、回報處理情形。
				</font>
				</ul>
		
			</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color"><font color=red>第十七條、電子文件之合法授權與責任</font></span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
				<font color=red>貴行及立約人應確保所傳送至對方之電子文件均經合法授權。</font><br>
				<font color=red>貴行或立約人於發現有第三人冒用或盜用使用者代號、密碼、憑證、私密金鑰，或其他任何未經合法授權之情形，應立即以電話、電子文件或書面方式通知他方停止使用該服務並採取防範之措施。</font><br>
				<font color=red>貴行接受前項通知前，對第三人使用該服務已發生之效力，由貴行負責。但有下列任一情形者，不在此限：</font>
				<ul style="list-style: decimal; list-style-position: inside;margin-left: 25px;margin-right: 10px;">
					<font color=red>一、貴行能證明立約人有故意或過失。</font><br>
					<font color=red>二、貴行依電子文件、電話或書面方式通知交易核對資料或帳單後超過四十五日。惟立約人有特殊事由致無法通知者，以該特殊事由結束日起算四十五日，但貴行有故意或過失者，不在此限。<br>
					針對第二項冒用、盜用事實調查所生之鑑識費用由貴行負擔。</font>
				</ul>
			</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color"><font color=red>第十八條、資訊系統安全</font></span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
			<font color=red>貴行及立約人應各自確保所使用資訊系統之安全，防止非法入侵、取得、竄改、毀損業務紀錄或立約人個人資料。<br>
			第三人破解貴行資訊系統之保護措施或利用資訊系統之漏洞爭議，由貴行就該事實不存在負舉證責任。<br>
			第三人入侵貴行資訊系統對立約人所造成之損害，由貴行負擔。
			</font>
			</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color"><font color=red>第十九條、保密義務</font></span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
			<font color=red>
				除其他法律規定外，貴行應確保所交換之電子文件因使用或執行本契約服務而取得立約人之資料，不洩漏予第三人，亦不可使用於與本契約無關之目的，且於經立約人同意告知第三人時，應使第三人負本條之保密義務。<br>
				前項第三人如不遵守此保密義務者，視為本人義務之違反。
			</font>
			</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color">第二十條、資料完整</span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
			立約人使用貴行所提供之各項服務，如需以書面方式為之者，立約人仍須補足書面資料後方屬完成手續，惟在尚未補足書面資料前，仍應對於完成訊息傳送之交易負責。
			</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color"><font color=red>第二十一條、損害賠償責任</font></span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
			<font color=red>貴行及立約人同意依本契約傳送或接收電子文件，因可歸責於當事人一方之事由，致有遲延、遺漏或錯誤之情事，而致他方當事人受有損害時，該當事人應就他方所生之損害負賠償責任。</font>
			</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color">第二十二條、紀錄保存</span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
				貴行及立約人應保存所有交易指示類電子文件紀錄，並應確保其真實性及完整性。<br>
				貴行對前項紀錄之保存，應盡善良管理人之注意義務。保存期限為五年以上，但其他法令有較長規定者，依其規定。 <br>
			</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color">第二十三條、電子文件之效力</span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
				貴行及立約人同意以電子文件作為表示方法，依本契約交換之電子文件，其效力與書面文件相同。但法令另有排除適用者，不在此限。
			</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color"><font color=red>第二十四條、立約人終止契約及變更本服務之內容</font></span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
				<font color=red>立約人得隨時終止本契約及變更本服務之內容，但應親自、書面或雙方約定方式辦理。</font>
			</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color"><font color=red>第二十五條、銀行終止契約</font></span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
			<font color=red>
				貴行終止本契約時，須於終止日三十日前以書面通知立約人。<br>
				立約人如有下列情事之一者，貴行得隨時以書面或雙方約定方式通知立約人終止本契約：<br>
				<ul style="list-style: decimal; list-style-position: inside;margin-left: 25px;margin-right: 10px;">
					一、立約人未經貴行同意，擅自將契約之權利或義務轉讓第三人者。<br>
					二、立約人依破產法聲請宣告破產或消費者債務清理條例聲請更生、清算程序者。<br>
					三、立約人違反本「共通約定事項」（十七）至（十九）之規定者。<br>
					四、立約人違反本契約之其他約定，經催告改善或限期請求履行未果者。<br>
				</ul>
				本網路銀行約定事項之終止，以貴行辦妥註銷登錄始為生效，對於終止前發送訊息所需完成或履行之義務不生任何影響，惟網路銀行未屆轉帳日之各項預約轉帳自終止生效起即停止轉帳作業。
			</font>
			</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color"><font color=red>第二十六條、個人資料之利用</font></span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
				<font color=red>立約人同意貴行及與貴行有業務往來之機構，於符合其營業登記項目或章程所定業務之需要，得蒐集、處理或國際傳輸及利用立約人之個人資料。</font>
			</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color">第二十七條、委外作業</span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
				立約人同意貴行得於主管機關核定或核准得委外之作業事項範圍內，將涉及本約定書有關之資訊作業得委託適當之第三人處理。
			</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color">第二十八條、異常提領規定</span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
				貴行因電腦故障或線路中斷，以致不能提供本網路銀行服務時，立約人仍能親赴貴行櫃台提領帳戶款項；但因貴行無法得知電腦故障或線路中斷前立約人已發送之付款指示，因此立約人同意貴行得視立約人往來狀況彈性酌予提領，如有溢領情事應即時返還。
			</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color"><font color=red>第二十九條、契約修訂</font></span>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
			<font color=red>
			本契約條款如有修改或增刪時，貴行以書面或雙方約定方式通知立約人後，立約人於七日內不為異議者，視同承認該修改或增刪約款。但下列事項如有變更，應於變更前六十日以書面或雙方約定方式通知立約人，並於該書面或雙方約定方式以顯著明確文字載明其變更事項、新舊約款內容，暨告知立約人得於變更事項生效前表示異議，及立約人未於該期間內異議者，視同承認該修改或增刪約款；並告知立約人如有異議，應於前項得異議時間內通知貴行終止契約：
				</font>
				<ul style="list-style: decimal; list-style-position: inside;margin-left: 25px;margin-right: 10px;">
				<font color=red>
				一、第三人冒用或盜用使用者代號、密碼、憑證、私密金鑰，或其他任何未經合法授權之情形，貴行或立約人通知他方之方式。<br>
				二、其他經主管機關規定之事項。<br>
				</font>
				</ul>
			</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color">第三十條、文書送達</span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
				立約人同意以契約中載明之地址為相關文書之送達處所，倘立約人之地址變更，應即以書面或其他約定方式通知貴行，並同意改依變更後之地址為送達處所；如立約人未以書面或依約定方式通知變更地址時，貴行仍以契約中立約人載明之地址或最後通知貴行之地址為送達處所。
			</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color">第三十一條、法令適用</span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
				本契約準據法，依中華民國法律。
			</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color">第三十二條、法院管轄</span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
				因本契約而涉訟者，貴行及立約人同意以立約所在地之地方法院為第一審管轄法院，惟不得排除消費者保護法第47條或民事訴訟法第436條之9規定小額訴訟管轄法院之適用。
			</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color">第三十三條、標題</span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
				本契約各條標題，僅為查閱方便而設，不影響契約有關條款之解釋、說明及瞭解。
			</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color">第三十四條、契約分存</span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
				本契約壹式貳份，由貴行及立約人各執壹份為憑。
			</ul>
		</ul>
		<ul><span class="input-subtitle subtitle-color"><font color=red>第三十五條、申訴管道</font></span><br>
			<ul style="list-style: decimal; list-style-position: inside;margin-left: 20px;margin-right: 10px;">
			<font color=red>
				立約人如對本契約有爭議，申訴管道如下：<br>
				<div style="margin-left:25px;">
					1.免付費服務電話：0800-00-7171#5。<br>
					2.電子信箱(e-mail)：臺灣企銀網站 https://www.tbb.com.tw 客服信箱。
				</div>	
				</font>
			</ul>				
		</ul>
</ul>
</body>
</html>