<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="page6" class="card-detail-block">
	<div class="card-center-block">
		<!-- Q1 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-61" aria-expanded="true"
				aria-controls="popup1-61">
				<div class="row">
					<span class="col-1">Q1</span>
					<div class="col-11">
						<span>成功申请台湾企银网络银行后，分行给我的密码单要怎么用？</span>
					</div>
				</div>
			</a>
			<div id="popup1-61" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>
								成功申请网络银行后，分行会给您约定书（客户收执联）及密码单各一份。使用方式如下：密码单包含用户名称、签入密码及交易密码，请于一个月内至本行网站https://ebank.tbb.com.tw点选『网络银行』，输入<font color="red">统一编号</font>、<font color="red">使用者名称</font>及<font color="red">签入密码</font>即可登入网络银行，首次登入请变更密码。<br>
								登入后执行各项查询、转账、缴费交易及登入凭证注册中心申请金融XML凭证，请键入<font color="red">交易密码</font>。
							</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q2 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-62" aria-expanded="true"
				aria-controls="popup1-62">
				<div class="row">
					<span class="col-1">Q2</span>
					<div class="col-11">
						<span>为什么有签入密码、交易密码，有何不同？</span>
					</div>
				</div>
			</a>
			<div id="popup1-62" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>
								签入密码：为您登入网络银行时所须键入的密码，初次使用必须在线变更密码。<br>
		    					交易密码：为您执行各种交易密码(SSL)或登入凭证注册中心申请金融XML凭证时所须键入的密码，初次使用必须在线变更密码。<br>
		    					以上密码在您申请网络银行时，由本行制作密码单给您使用，为保障您的安全，您可以随时于本行网络银行『个人化设定』办理密码变更。
							</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q3 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-63" aria-expanded="true"
				aria-controls="popup1-63">
				<div class="row">
					<span class="col-1">Q3</span>
					<div class="col-11">
						<span>密码忘记了，该怎么办？</span>
					</div>
				</div>
			</a>
			<div id="popup1-63" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<ol class="ttb-result-list terms">
								<li>1.本行提供留存手机号码之一般网络银行客户，以本行有效芯片金融卡搭配卡片阅读机，重新设定一般网络银行用户名称、签入及交易密码。</li>
								<li>2.如需回临柜办理重置密码，请依下列办理:
									<ol>
										<li>一、本行存户：
											<ol>
												<li>法人户：请负责人亲携公司登记证件、身份证、存款印鉴及存折至开户行办理『重置密码』，领取密码单后请于１个月内登入网络银行并变更密码，逾期密码失效。</li>
												<li>个人户：请本人亲携身份证件、存款印鉴及存折至开户行办理『重置密码』，全行收付户(即通储户)，亦可至国内各分行办理，领取密码单后请于１个月内登入网络银行并变更密码，逾期密码失效。</li>
											</ol>
										</li>
										<li>二、本行芯片金融卡在线申请户：请本人携带身份证件及存款印鉴至开户行办理『重置密码』，全行收付户(即通储户)，亦可至国内各分行办理，领取密码单后请于１个月内登入网络银行并变更密码，逾期密码失效。</li>
										<li>三、本行信用卡在线申请户：请电洽本行客服0800-01-7171处理后之次日起重新在线申请网络银行。</li>
									</ol>
								</li>
							</ol>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q4 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-64" aria-expanded="true"
				aria-controls="popup1-64">
				<div class="row">
					<span class="col-1">Q4</span>
					<div class="col-11">
						<span>『使用者名称』应如何办理变更？</span>
					</div>
				</div>
			</a>
			<div id="popup1-64" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>使用者名称为6－16位数之英数字，英文至少2位且区分大小写，不可仅设定数字或设定相同的英数字或连号（例如：不可设定AAAAAA）。如需变更，请签入网络银行之「个人化设定」变更。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q5 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-65" aria-expanded="true"
				aria-controls="popup1-65">
				<div class="row">
					<span class="col-1">Q5</span>
					<div class="col-11">
						<span>已向分行申请金融XML凭证及购买凭证载具(i-Key)，计算机需如何操作下载凭证？</span>
					</div>
				</div>
			</a>
			<div id="popup1-65" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>请参照「新手上路」→「凭证注册中心」，安装凭证载具(i-Key)驱动程序，再登入「网络银行」→「凭证注册中心」→「凭证管理」→「用户申请凭证」申请及下载凭证。完成申请后，即可使用电子签章交易。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q6 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-66" aria-expanded="true"
				aria-controls="popup1-66">
				<div class="row">
					<span class="col-1">Q6</span>
					<div class="col-11">
						<span>为什么凌晨使用网络银行速度会变慢？</span>
					</div>
				</div>
			</a>
			<div id="popup1-66" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>本行每日凌晨时段（03：01以后）不定时进行系统维护作业，暂停客户交易约20至30分钟，此时交易会等待作业完成，致处理变慢，请贵客户稍后再试，敬请见谅！</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q7 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-67" aria-expanded="true"
				aria-controls="popup1-67">
				<div class="row">
					<span class="col-1">Q7</span>
					<div class="col-11">
						<span>交易不成功讯息代码如何看？</span>
					</div>
				</div>
			</a>
			<div id="popup1-67" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>每一讯息代码皆有中文说明，若要知道更清楚的说明，请参阅网络银行公告之『错误代码』。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q8 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-68" aria-expanded="true"
				aria-controls="popup1-68">
				<div class="row">
					<span class="col-1">Q8</span>
					<div class="col-11">
						<span>为什么登入网络银行，会显示『无法显示网页』之讯息，该如何处理？</span>
					</div>
				</div>
			</a>
			<div id="popup1-68" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>本行网络银行之「网络安全认证」，加密长度达 256 位， 符合「金融机构办理电子银行业务安全控管作业基准」。为保障您个人或企业数据的安全，如您的浏览器无法处理 256 位加密，将无法使用本行之网络银行服务；如果无法登入网络银行，请点选超链接至微软网站更新，但请先确认使用IE的版本为5.5版本以上。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q9 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-69" aria-expanded="true"
				aria-controls="popup1-69">
				<div class="row">
					<span class="col-1">Q9</span>
					<div class="col-11">
						<span>为什么登入网络银行，会出现「无法显示网页」或「程序即将关闭」或没有反应，是否系统设定有问题，该如何处理？</span>
					</div>
				</div>
			</a>
			<div id="popup1-69" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>此可能为客户端IE设定的问题，方法如下：</p>
							<p>请开启IE，点选IE的工具栏中的『工具』→『因特网选项』再选『进阶』，在『设定』中→『安全性』→『使用TLS 1.0』选项不要打勾，请按下《确定》即可，再重试登入。 </p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q10 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-610" aria-expanded="true"
				aria-controls="popup1-610">
				<div class="row">
					<span class="col-1">Q10</span>
					<div class="col-11">
						<span>为什么上贵行网络银行登入画面时，会显示「你未开启cookie功能，无法执行网络银行交易！请开启Cookie功能后，重新执行本网页」，该如何处理？</span>
					</div>
				</div>
			</a>
			<div id="popup1-610" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>请在「工具」→「因特网选项」选择「隐私」找到cookie选项，并勾选「永远允许会话Cookie」即可。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
