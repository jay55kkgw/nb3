<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
<br/><br/>
<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
<br/><br/><br/>
<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
<br/><br/><br/>
<!-- 變更成功 -->
<div style="text-align:center"><font color="red"><spring:message code= "LB.D0182" /></font></div>
<br/>
<table class="print">
	<tr>
	<!-- 約定扣款帳號 -->
		<th class="text-center"><spring:message code= "LB.W1539" /></th>
		<th>${SVACN}</th>
	</tr>
	<tr>
<!-- 黃金存摺帳號 -->
		<th class="text-center"><spring:message code= "LB.D1090" /></th>
		<th>${ACN}</th>
	</tr>
	<tr>
<!-- 原每月投資日/金額 -->
		<th colspan="2" class="text-center"><spring:message code= "LB.W1581" /></th>
	</tr>
	<tr>
<!-- 日 -->
		<th class="text-center">06<spring:message code= "LB.Day" /></th>
<!-- 新臺幣 -->
<!-- 元 -->
		<th>
			<spring:message code= "LB.D0508" />&nbsp;
			${AMT_06_O}&nbsp;
			<spring:message code= "LB.Dollar" />&nbsp;
			<c:if test="${!FLAG_06_O.equals('')}">
				<spring:message code="${FLAG_06_O}" />
			</c:if>
		</th>
	</tr>
	<tr>
<!-- 日 -->
		<th class="text-center">16<spring:message code= "LB.Day" /></th>
<!-- 新臺幣 -->
<!-- 元 -->
		<th>
			<spring:message code= "LB.D0508" />&nbsp;
			${AMT_16_O}&nbsp;
			<spring:message code= "LB.Dollar" />&nbsp;
			<c:if test="${!FLAG_16_O.equals('')}">
				<spring:message code="${FLAG_16_O}" />
			</c:if>
		</th>
	</tr>
	<tr>
<!-- 日 -->
		<th class="text-center">26<spring:message code= "LB.Day" /></th>
<!-- 新臺幣 -->
<!-- 元 -->
		<th>
			<spring:message code= "LB.D0508" />&nbsp;
			${AMT_26_O}&nbsp;
			<spring:message code= "LB.Dollar" />&nbsp;
			<c:if test="${!FLAG_26_O.equals('')}">
				<spring:message code="${FLAG_26_O}" />
			</c:if>
		</th>
	</tr>
	<tr>
<!-- 變更後每月投資日/金額 -->
		<th colspan="2" class="text-center"><spring:message code= "LB.W1582" /></th>
	</tr>
	<tr>
<!-- 日 -->
		<th class="text-center">06<spring:message code= "LB.Day" /></th>
		<c:if test = "${empty DATE_06}">
<!-- 新臺幣 -->
<!-- 元 -->
			<th>
				<spring:message code= "LB.D0508" />&nbsp;
				${AMT_06_O}&nbsp;
				<spring:message code= "LB.Dollar" />&nbsp;
				<c:if test="${!FLAG_06_O.equals('')}">
					<spring:message code="${FLAG_06_O}" />
				</c:if>
			</th>
		</c:if>
		<c:if test = "${not empty DATE_06}">
<!-- 新臺幣 -->
<!-- 元 -->
			<th>
				<spring:message code= "LB.D0508" />&nbsp;
				${AMT_06_N}&nbsp;
				<spring:message code= "LB.Dollar" />&nbsp;
				<c:if test="${!PAYSTATUS_06.equals('')}">
					<spring:message code="${PAYSTATUS_06}" />
				</c:if>
			</th>
		</c:if>
	</tr>
	<tr>
<!-- 日 -->
		<th class="text-center">16<spring:message code= "LB.Day" /></th>
		<c:if test = "${empty DATE_16}">
<!-- 新臺幣 -->
<!-- 元 -->
			<th>
				<spring:message code= "LB.D0508" />&nbsp;
				${AMT_16_O}&nbsp;
				<spring:message code= "LB.Dollar" />&nbsp;
				<c:if test="${!FLAG_16_O.equals('')}">
					<spring:message code="${FLAG_16_O}" />
				</c:if>
			</th>
		</c:if>
		<c:if test = "${not empty DATE_16}">
<!-- 新臺幣 -->
<!-- 元 -->
			<th>
				<spring:message code= "LB.D0508" />&nbsp;
				${AMT_16_N}&nbsp;
				<spring:message code= "LB.Dollar" />&nbsp;
				<c:if test="${!FLAG_16_O.equals('')}">
					<spring:message code="${PAYSTATUS_16}" />
				</c:if>
			</th>
		</c:if>
	</tr>
	<tr>
<!-- 日 -->
		<th class="text-center">26<spring:message code= "LB.Day" /></th>
		<c:if test = "${empty DATE_26}">
<!-- 新臺幣 -->
<!-- 元 -->
			<th>
				<spring:message code= "LB.D0508" />&nbsp;
				${AMT_26_O}&nbsp;
				<spring:message code= "LB.Dollar" />&nbsp;
				<c:if test="${!FLAG_26_O.equals('')}">
					<spring:message code="${FLAG_26_O}" />
				</c:if>
			</th>
		</c:if>
		<c:if test = "${not empty DATE_26}">
<!-- 新臺幣 -->
<!-- 元 -->
			<th>
				<spring:message code= "LB.D0508" />&nbsp;
				${AMT_26_N}&nbsp;
				<spring:message code= "LB.Dollar" />&nbsp;
				<c:if test="${!PAYSTATUS_26.equals('')}">
					<spring:message code="${PAYSTATUS_26}" />
				</c:if>
			</th>
		</c:if>
	</tr>
</table>
<br>
	<div class="text-left" style="margin-left: 20px;">
		<spring:message code="LB.Description_of_page" />
		<ol class="list-decimal text-left">
		<!-- 投資日上午凌晨0〜10點(遇假日則自原投資日起至次一營業日上午凌晨0〜10點)，因系統進行批次扣款作業，暫時停止變更服務。 -->
					<li><spring:message code="LB.Averaging_Alter_P3_D1" /></li>
                <!-- 每次投資金額至少為新臺幣3,000元，且得以新臺幣1,000元之整倍數增加。  -->
					<li><spring:message code="LB.Averaging_Alter_P3_D2" /></li>
                <!-- 網路銀行定期定額每次扣款優惠手續費為新臺幣50元，連同投資金額一併扣繳。 -->
					<li><spring:message code="LB.Averaging_Alter_P3_D3" /></li>
					<!-- 黃金存摺不支付利息，黃金價格有漲有跌，投資時可能產生收益或損失，請慎選買賣時機，並承擔風險。 -->
					<li><spring:message code="LB.Averaging_Alter_P3_D4" /></li>
					<!-- 電子簽章:為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章(載具i-key)拔除並登出系統。 -->
					<li><spring:message code="LB.Averaging_Alter_P3_D5" /></li>		 
		</ol>
	</div>
<br/><br/>
</body>
</html>