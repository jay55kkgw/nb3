<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="account-right">
    <div class="account-announcement-block">
        <div class="title"><img src="${__ctx}/img/search-solid.svg" /></i><spring:message code="LB.X2311"/></div>
        <div class="content">
            <ul id="bulletinUL">
<!--                 <li> -->
<!--                     <span>台灣Pay跨行轉帳免手續費優惠活動</span> -->
<!--                     <a href="#">立即了解</a> -->
<!--                 </li> -->
            </ul>
        </div>
    </div>
</div>
        
        
<script type="text/JavaScript">
// 取得公告訊息
function getBulletin(){
	console.log("getBulletin.now: " + new Date());
	
	uri = '${__ctx}'+"/INDEX/bulletin_aj";
// 	console.log("bulletin_uri: " + uri);
	fstop.getServerDataEx(uri, null, true, showBulletin);
}

// 顯示公告訊息
function showBulletin(data){
	console.log("showBulletin.now: " + new Date());
	
// 	console.log("showBulletin.data...");
	if (data) {
		for (i = 0; i < data.length; i++) {
			var id = data[i].id; // 訊息類型
			var type = data[i].type; // 訊息類型
			var title = data[i].title; // 訊息標題
			var contenttype = data[i].contenttype; // 訊息內容類型
			
			var urlLink = data[i].url; // 訊息內容類型--超連結
			var content = data[i].content; // 訊息內容類型--文字
			var srccontent = data[i].srccontent; // 訊息內容類型--pdf
			
			// 訊息類型--1:重要、0:一般、其他:已讀
// 			console.log("data["+i+"].type: " + data[i].type);
			
			//變更換行符號
			if(content != null){
				content=content.replace(/\n\r/g,"<br/>");
				content=content.replace(/\r\n/g,"<br/>");
				content=content.replace(/\n/g,"<br/>");
				content=content.replace(/\r/g,"<br/>");
			}
			
			// 訊息類型--1:重要
// 			if (type == '1') {
				// 訊息公告內容為超連結
				if (contenttype == '1') {
					var url = ' onclick="window.open(' + "'" + urlLink + "'" + ');"';
					
					$("#bulletinUL").append(
						  '<li>'
						+ '<span>' + title + '</span>'
						+ '<a href="#" ' + url + '><spring:message code="LB.X2312"/></a>'
						+ '</li>'
					);
					
				} // 訊息公告內容為pdf
				else if (contenttype == '2') {
					var uri = "${__ctx}/getAnn/"+id;
					var url = ' onclick="window.open(' + "'" + uri + "'" + ');"';
					
					$("#bulletinUL").append(
						  '<li>'
						+ '<span>' + title + '</span>'
						+ '<a href="#" ' + url + '><spring:message code="LB.X2312"/></a>'
						+ '</li>'
					);
					
				} // 訊息公告內容為文字訊息
				else if (contenttype == '3') {
	 				var url = ' onclick="' + "$('#text-info').html(" + "'" + content + "'" + '); ' + "$('#text-block').show(); " + '"';

					$("#bulletinUL").append(
						  '<li>'
						+ '<span>' + title + '</span>'
						+ '<a href="#" ' + url + '><spring:message code="LB.X2312"/></a>'
						+ '</li>'
					);
				}
				
// 			} // 訊息類型--0:一般
// 			else if(type == '0'){

// 			} // 訊息類型--其他:已讀
// 			else {
// 			}
		}
// 		console.log("showBulletin...Finish!!!");
		console.log("showBulletin.end: " + new Date());
		
	} else {
		console.log("Oops");
	}
	
}
</script>