<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript">
		// 初始化
		$(document).ready(function () {
			init();
		});
		function init() {
			//表單驗證
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
			// 確認鍵 click
			goOn();
			//倒數
			countDown();
			//上一頁
			back();
		}
		// 確認鍵 click
		function goOn() {
			$("#CMSUBMIT").click(function (e) {
				//塞值進span內的input
				if (!$('#formId').validationEngine('validate')) {
					e = e || window.event; // for IE
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					initBlockUI(); //遮罩
					$("#formId").submit();
				}
			});
		}
		//上一頁按鈕
		function back(){
			//此處ID可能不一樣
			$("#CMCANCEL").click(function() {
				$("#formId").validationEngine('detach');
				$("#formId").removeAttr("target");
				$("#formId").attr("action", "${__ctx}/FCY/ACCT/ONLINE/f_remittances_payments_step1");
				$('#back').val("Y");
				initBlockUI();
				$("form").submit();
			});
		}
		//網頁倒數計時(30秒)    
		var sec = 30;
		var the_Timeout;
		function countDown() {
			var counter = document.getElementById("CountDown");		   		
		   	  counter.innerHTML = sec;
			sec--;
			if (sec == -1) {
				$("#CMSUBMIT").val("<spring:message code= "LB.X1291" />");
				$("#formId").removeAttr("target");
				$("#formId").attr("action", "${__ctx}/FCY/ACCT/ONLINE/f_remittances_payments_step1");
				return false;
			}
			//網頁倒數計時(30秒)      	  	
			the_Timeout = setTimeout("countDown()", 1000);
		}
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
	<!-- 匯入匯款     -->
    		<li class="ttb-breadcrumb-item"><spring:message code="LB.W0125" /></li>
    <!-- 線上解款     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0317" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!--匯入匯款線上解款 -->
				<h2>
					<spring:message code="LB.W0317" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form autocomplete="off" method="post" id="formId" action="${__ctx}/FCY/ACCT/ONLINE/f_remittances_payments_confirm">
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<div class="ttb-message">
									<span>
										<spring:message code="LB.Time_limit_for_exchange_rate" /> <spring:message code="LB.Time_limit_time_passed" /><font id="CountDown" color="red"></font> <spring:message code="LB.Second" />
									</span>
								</div>
								
								<!-- 匯入匯款金額-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title"><label>
	                                        <h4><spring:message code="LB.X0062"/></h4>
	                                    </label></span>
	                                <span class="input-block">
	                                    <div class="ttb-input">
	                                   <span>
	                                   ${f_remittances_payments_step3.data.TXCCY}
	                                   ${f_remittances_payments_step3.data.showCURAMT}
	                                   </span>
	                                   <span class="ttb-unit"><spring:message code="LB.Dollar" /></span>
	                                    </div>
	                                </span>
	                            </div>
	                            <!-- 解款金額 -->
	                            <div class="ttb-input-item row">
	                                <span class="input-title"><label>
	                                        <h4> <spring:message code="LB.W0330" /></h4>
	                                    </label></span>
	                                <span class="input-block">
	                                    <div class="ttb-input">
	                                        <span>
	                                          ${f_remittances_payments_step3.data.RETCCY}
	                                          ${f_remittances_payments_step3.data.showTXAMT}
	                                        </span><span class="ttb-unit"><spring:message code="LB.Dollar" /></span>
	                                    </div>
	                                </span>
	                            </div>
	                            <!--  匯率 -->
	                            <div class="ttb-input-item row">
	                                <span class="input-title"><label>
	                                        <h4> <spring:message code="LB.Exchange_rate" /></h4>
	                                    </label></span>
	                                <span class="input-block">
	                                    <div class="ttb-input">
	                                     <span>
	                                       ${f_remittances_payments_step3.data.RATE}
	                                     </span>
	                                    </div>
	                                </span>
	                            </div>
	                            <!--議價編號 -->
	                            <div class="ttb-input-item row">
	                                <span class="input-title"><label>
	                                        <h4><spring:message code="LB.Bargaining_number" /></h4>
	                                    </label></span>
	                                <span class="input-block">
	                                    <div class="ttb-input">
	                                        <span>
	                                          ${f_remittances_payments_step3.data.BGROENO}
	                                        </span>
	                                    </div>
	                                </span>
	                            </div>
							</div>
							<input class="ttb-button btn-flat-gray" id="CMCANCEL" name="CMCANCEL" type="button" value="<spring:message code="LB.Cancel_transaction" />"/>
							<input class="ttb-button btn-flat-orange" id="CMSUBMIT" name="CMSUBMIT" type="button" value="<spring:message code="LB.Confirm" />" />
						</div>
					</div>
				</form>
			</section>
			<!-- main-content END -->
		</main>
	</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>