<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp"%>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">

	<fmt:setLocale value="${__i18n_locale}"/>
<script type="text/javascript">
	//新增	
	var acnoCnt=1;
	var flag = 0;
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 50);
		// 開始跑下拉選單並完成畫面
		setTimeout("init()", 400);
// 		setTimeout("initDataTable()",100);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
		$("#newData").hide();
		
	});
	
	function init(){
		
		//取得銀行代號
		getBankList();
		
		$("#formId").validationEngine({
			binded: false,
			promptPosition: "inline"
		});
		
		//確定 
		$("#CMSUBMIT").click(function(e) {
			
			for(var i = 1;i <= acnoCnt;i++){
				$("#DPACNO"+i).removeClass(" validate[required,funcCall[validate_CheckNumber['<spring:message code= "LB.Account" />',DPACNO"+i+"]]]");
				$("#DPACNO"+i).removeClass(" validate[required,funcCall[validate_checkSYS_IDNO[DPACNO"+i+"]]]");
				DPACNO = $("#DPACNO"+i).val();
				DPBHNO = $("#DPBHNO"+i).val();
				if(DPBHNO == "050" && DPACNO.length==10){
					$("#DPACNO"+i).addClass(" validate[required,funcCall[validate_checkSYS_IDNO[DPACNO"+i+"]]]");
				}else{
					$("#DPACNO"+i).addClass(" validate[required,funcCall[validate_CheckNumber['<spring:message code= "LB.Account" />',DPACNO"+i+"]]]");
				}
			}
			
			
			//頁面勾選值
			$("#RowData").val(
				$("input[type='checkbox']:checkbox:checked").map(function(){					
					return $(this).val();
				}).get().join(';')				
			);
			
			//計算勾選筆數
			var count = 0;
			$("input[type='checkbox']:checkbox:checked").map(function(){
				count++;
				return $(this).val();
			})
			count += acnoCnt;
						//送出進表單驗證前將span顯示
			$("#hideblock").show();
			console.log("submit~~");
			
			if($("#RowData").val() == "" && $("#DPACNO1").val() == ""){
				//alert("<spring:message code= "LB.Alert164" />");	
				errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert164' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				e.preventDefault();
			}else if(count > 10){
				//alert("<spring:message code= "LB.Alert165" />");	
				errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert165' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				e.preventDefault();
				count = 0;
			}else if (!$('#formId').validationEngine('validate')) {
		        e.preventDefault();
		    }else {
		    	$("#formId").validationEngine('detach');
 				initBlockUI();
 				$("#formId").attr("action","${__ctx}/ONLINE/SERVING/predesignated_account_apply_s");
 	  			$("#formId").submit(); 
		    }
		});
		
	}
	
	//選項控制
	function changeRadio(checkNewdata){
		//var checkNewdata = $('#checkNewdata1').is(':checked');		
		if(checkNewdata){
			$("#newData").show();
			$("#addBtn").show();
			//紀錄目前額外新增筆數
			$("#addCount").val("1");
			if(flag == 0){
				initDataTable();
				flag = 1;
			}
		}else{
			$("#newData").hide();
			$("#addBtn").hide();
			//紀錄目前額外新增筆數
			$("#addCount").val("");
		}
	}
	
	//clear
	function clearRadio(){
			$("#newData").hide();
			$("#addBtn").hide();
			//紀錄目前額外新增筆數
			$("#addCount").val("");
	}
	
	
	function onAddAcno() {

        acnoCnt++;

        var bankDiv = $("#template").clone();

        var bank = bankDiv.children().find("select:eq(0)");

        var inacno = bankDiv.children().find("input:eq(0)");

        var memo = bankDiv.children().find("input:eq(1)");


        bankDiv.attr("id","");

        bank.attr("id", "DPBHNO"+acnoCnt);

        bank.attr("name", "DPBHNO"+acnoCnt);

        bank.val("");

        inacno.attr("id", "DPACNO"+acnoCnt);

        inacno.attr("name", "DPACNO"+acnoCnt);

//         inacno.removeClass(" validate[required,funcCall[validate_CheckNumber['<spring:message code= "LB.Account" />',DPACNO1]]]");
//         inacno.addClass(" validate[required,funcCall[validate_CheckNumber['<spring:message code= "LB.Account" />',DPACNO"+acnoCnt+"]]]");

        inacno.val("");

        memo.attr("id", "DPACGONAME"+acnoCnt);

        memo.attr("name", "DPACGONAME"+acnoCnt);

        memo.val("");       
		if(acnoCnt > 10){
			//alert("<spring:message code= "LB.Alert166" />");
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert166' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);

	        acnoCnt--;
		}else{
        	$('#template').parent().append(bankDiv);			
		}
        //紀錄目前額外新增筆數
		$("#addCount").val(acnoCnt);
    }
	
//刪除
    function onDelete() {

        if ( acnoCnt == 1 ) {        	
            return;        	
        }

        acnoCnt--;
        $("#addCount").val(acnoCnt);
        $('#template').parent().children().last().remove();

    }

	function getBankList(){
		uri = '${__ctx}' + "/PERSONAL/SERVING/getBankList_aj";
		var data = fstop.getServerDataEx(uri, null, false);
		console.log("data: " + JSON.stringify(data));
		$.each(data, function(key, val) {
        	<c:if test="${__i18n_locale eq 'en' }">
				if(val.adbankid =='050'){
					$('#DPBHNO1').append($("<option></option>").attr("value" , val.adbankid
						).text(val.adbankid+'-'+val.adbankengname).prop("selected","selected"));
				}else{
					$('#DPBHNO1').append($("<option></option>").attr("value" , val.adbankid
							).text(val.adbankid+'-'+val.adbankengname));
				}
			</c:if>
			<c:if test="${__i18n_locale eq 'zh_TW' }">
				if(val.adbankid =='050'){
					$('#DPBHNO1').append($("<option></option>").attr("value" , val.adbankid
						).text(val.adbankid+'-'+val.adbankname).prop("selected","selected"));
				}else{
					$('#DPBHNO1').append($("<option></option>").attr("value" , val.adbankid
							).text(val.adbankid+'-'+val.adbankname));
				}
			</c:if>
			<c:if test="${__i18n_locale eq 'zh_CN' }">
				if(val.adbankid =='050'){
					$('#DPBHNO1').append($("<option></option>").attr("value" , val.adbankid
						).text(val.adbankid+'-'+val.adbankchsname).prop("selected","selected"));
				}else{
					$('#DPBHNO1').append($("<option></option>").attr("value" , val.adbankid
							).text(val.adbankid+'-'+val.adbankchsname));
				}
			</c:if>
		});
	}
	
</script>
</head>
<body>
	<!--   IDGATE -->
	<%@ include file="../idgate_tran/idgateAlert.jsp" %>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 約定轉入帳號設定申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0263" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code="LB.X0263"/></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="" autocomplete="off">
				<input type="hidden" id="RowData" name="RowData" value="">
				<input type="hidden" id="addCount" name="addCount" value="">
				<div class="main-content-block row">
                	<div class="col-12">
						<div class="ttb-input-block">
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
									<!-- 約定轉入帳號-->
										<spring:message code="LB.D0227" />
									</label>
								</span>
								<span class="input-block">
				                	<c:if test="${predesignated_account_apply.data.COUNT != '0'}">
					                	<c:forEach var="dataList" items="${predesignated_account_apply.data.REC}" varStatus="data">
					                		<label class="check-block" for="INTRACN${data.count}">
					                			<input type="checkbox" name="INTRACN${data.count}" id="INTRACN${data.count}" value="${dataList.ACN}" />${dataList.ACN}&nbsp;
	                                            <span class="ttb-check"></span>
	                                        </label>
				                			<br>
					                	</c:forEach>
				                	</c:if>
				              			
									<spring:message code="LB.D0228_1" />
									<label class="radio-block" for="checkNewdata1" onClick="changeRadio(true)" >
										<input type="radio" id="checkNewdata1" name="checkNewdata" /><spring:message code="LB.D0034_2" />
                                        <span class="ttb-radio"></span>
                                    </label>
									<label class="radio-block" for="checkNewdata2"  onClick="changeRadio(false)" >
										<input type="radio" id="checkNewdata2" name="checkNewdata"/>
										<spring:message code="LB.D0034_3" />
                                        <span class="ttb-radio"></span>
                                    </label>
			                	</span>
							</div>
						</div>		
				<!-- 線上約定轉入帳號申請表-->
	                	<div class="ttb-input-block">
							<div id="newData" class="ttb-input-item row">
								<center  style="width: 100%;">
									<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
										<thead>
											<tr>
												<!-- 銀行名稱-->
												<th>
													<spring:message code="LB.Bank_name"/>
												</th>
												<!-- 約定輸入帳號-->
												<th>
													<spring:message code="LB.D0227" />
												</th>
												<!-- 好記名稱-->
												<th>
													<spring:message code="LB.Favorite_name"/>
												</th>
											</tr>
										</thead>
										<tbody id="template">
											<tr>
												<td>
													<select id="DPBHNO1" name="DPBHNO1" class="custom-select select-input half-input validate[required]">
						                                <option value="">--<spring:message code="LB.Select"/>--</option>
						                            </select>
					                            </td>
												<td>
													<input type="text" id="DPACNO1" name="DPACNO1" class="text-input" maxlength="16" autocomplete="off"/>
												</td>
												<td>
													<input type="text" id="DPACGONAME1" name="DPACGONAME1" class="text-input" maxlength="16" autocomplete="off" />
												</td>
											</tr>
										</tbody>
									</table>
								</center>
							</div>
							<div class="ttb-input-item row">
								<center style="width: 100%;">
									<table id="addBtn" style="display:none">
										<tbody>
											<tr>
												<td colspan = "3">				
							                   		<input type="button" id="Delete" class="ttb-sm-btn btn-flat-gray" value="<spring:message code="LB.X0266"/>" onclick="onDelete()" />
							                   		<input type="button" id="AddAcno" class="ttb-sm-btn btn-flat-orange" value="<spring:message code="LB.X0265"/>" onclick="onAddAcno()" />
												</td>
											</tr>
										</tbody>
									</table>
								</center>
							</div>
						</div>				         		 	
		<!-- 				col-10 -->               	
						<!-- 確定 -->
					</div>
	           		<div class="col-12">
		                <input type="button" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm"/>" >
					</div>
				</div>
				<div class="text-left">
					<ol class="list-decimal description-list">
						<p><spring:message code="LB.Note" /></p>
						<li><spring:message code="LB.predesignated_account_P2-D1"/>
							<ol>
							<li><spring:message code="LB.predesignated_account_P2-D11"/></li>
							<li><spring:message code="LB.predesignated_account_P2-D12"/></li>
							<li><spring:message code="LB.predesignated_account_P2-D13"/></li>
							<li><spring:message code="LB.predesignated_account_P2-D14"/></li>
							</ol>						
						</li>
						<li><spring:message code="LB.predesignated_account_P2-D2"/></li>
						<li><spring:message code="LB.predesignated_account_P2-D3"/></li>
						<li><spring:message code="LB.predesignated_account_P2-D4"/></li>
						<li><spring:message code="LB.W0412"/><a target='_blank' href='${__ctx}/CUSTOMER/SERVICE/rate_table'><spring:message code="LB.predesignated_account_P2-D5"/></a></li>
					</ol>
				</div>		
			</form>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>