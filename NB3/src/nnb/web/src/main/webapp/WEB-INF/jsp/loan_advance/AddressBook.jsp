<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=BIG5">
		<%@ include file="../__import_head_tag.jsp"%>
		<%@ include file="../__import_js.jsp" %>
		<script type="text/javascript">
		$(document).ready(function(){
			setTimeout("initDataTable()",100);
		});
			function fillData() 
			{
				var sAddressList ='';

				$("input[name='CHECKBOX']:checked").each(function(){
					sAddressList += $(this).val() + ";";
				});
				
				var CMTRMAIL = self.opener.document.getElementById("CMTRMAIL");
					
				if (CMTRMAIL != undefined){
					CMTRMAIL.value = sAddressList;
				}
				self.close();
			}
			function selectAll()
			{
				$("input[name='CHECKBOX']").each(function(){
					$(this).attr('checked',true);
				});
			} 
			function cancleAll()
			{
				$("input[name='CHECKBOX']").each(function(){
					$(this).attr('checked',false);
				});
			} 
		</script>
	</head>
	<body>
		<main class="col-12"> 
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
				<form method="post" id="formId" action="">
					<!-- 我的通訊錄 -->
					<h2><spring:message code= "LB.My_Address_Book" /></h2>
					<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
					<div class="main-content-block row">
						<div class="col-12 printClass">
							<br>
						    <table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
							    <thead>
								    <tr>
								    <!-- 勾選 -->
					                    <th><spring:message code= "LB.Check" /></th>
								      	<!-- 好記名稱 -->
								      	<th><spring:message code= "LB.Favorite_name" /></th>                
								      	<!-- 電子郵箱 -->
								      	<th><spring:message code= "LB.Mail_address" /></th>
					      			</tr>
				      			</thead>
				      			<c:if test="${empty result_data.data.REC}">
				      			<tbody>
				      				<tr>
				      					<td></td>
				      					<td></td>
				      					<td></td>
				      				</tr>
				      			</tbody>
				      			</c:if>
				      			<c:if test="${not empty result_data.data.REC}">
				      			<tbody>
									<c:forEach var="dataTable" items="${result_data.data.REC}">
										<tr>
						                    <th><input type="checkbox" name="CHECKBOX" value="${dataTable.DPABMAIL}" /></th>
									      	<th>${dataTable.DPGONAME}</th>                
									      	<th>${dataTable.DPABMAIL}</th>
						      			</tr>
									</c:forEach>
								</tbody>
								</c:if>
<%-- 								<tr> --%>
<%-- 				                    <th><input type="checkbox" name="CHECKBOX" value="aa532199@gmail.com" /></th> --%>
<%-- 							      	<th>測試資料(記得刪)</th>                 --%>
<%-- 							      	<th>aa532199@gmail.com</th> --%>
<%-- 				      			</tr> --%>
<%-- 								<tr> --%>
<%-- 									<td><a href="#" onclick="fillData('9');">9</a></td> --%>
<%-- 									<td>退役軍警、退休公務員、退休教員、退休職員、宗教服務、家管</td> --%>
<%-- 								</tr> --%>
							</table>
							<br>
							<input class="ttb-button btn-flat-orange" type="button" name="CMSUBMIT" id="CMSUBMIT" value="<spring:message code= "LB.All_select" />" onClick="selectAll();" />
							<input class="ttb-button btn-flat-orange" type="button" name="CMSUBMIT" id="CMSUBMIT" value="<spring:message code= "LB.Confirm" />" onClick="fillData();" />
						</div>
					</div>
					
				</form>	
			</section>
		</main> <!-- 		main-content END -->
	</body>
</html>