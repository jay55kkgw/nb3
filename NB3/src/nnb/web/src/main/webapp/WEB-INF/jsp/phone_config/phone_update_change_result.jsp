<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
</head>
<script type="text/javascript">
	$(document).ready(function() {
		$("#BACKSUBMIT").click(function(e){	
			//結果頁返回功能首頁改為共用進入點
			window.location.href =  "${__ctx}/PNONE/CONFIG/checkbind_bind";
		});
	});
</script>
<body>
    <!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

	<!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
	<!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 手機門號收款帳號設定    -->
			<li class="ttb-breadcrumb-item active" aria-current="page">手機門號收款帳號設定</li>
		</ol>
	</nav>

	<!-- 快速選單及主頁內容 -->
	<!-- menu、登出窗格 --> 
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
		<main class="col-12">	
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
			<!-- 手機號碼轉帳設定-->
				<h2>
				變更轉入帳號
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
<!-- 				<div id="step-bar"> -->
<!-- 					<ul> -->
<%-- 						<li class="finished"><spring:message code="LB.Enter_data" /></li> --%>
<%-- 						<li class="finished"><spring:message code="LB.Confirm_data" /></li> --%>
<%-- 						<li class="active"><spring:message --%>
<%-- 								code="LB.Transaction_complete" /></li> --%>
<!-- 					</ul> -->
<!-- 				</div> -->
					<div class="main-content-block row">
					<div class="col-12">
					
							<div class="ttb-message">
								<span>
								<!-- 變更成功 -->
								<spring:message code="LB.D0182"/>
								</span>
							</div>
										<!-- 變更時間 -->
							<div class="ttb-input-block">
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											變更時間
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${result_locale_data.data.datetime}
										</div>
									</span>
								</div>
							</div>
						<!-- 手機號碼 -->
							<div class="ttb-input-block">
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											手機號碼
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${result_locale_data.data.mobilephone}
										</div>
									</span>
								</div>
							</div>
							
							<!-- 綁定轉入帳號 -->
							<div class="ttb-input-block">
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											綁定收款帳號
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${result_locale_data.data.txacn}
										</div>
									</span>
								</div>
							</div>
							
							<!-- 同意作為預設收款帳戶 -->
							<div class="ttb-input-block">
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>同意作為預設收款帳號</h4>
										</label>
									</span>
									<span class="input-block">
										<c:if test="${result_locale_data.data.binddefault == 'Y'}">
											<div class="ttb-input">
											<!-- 是 -->
												是
											</div>
										</c:if>
										<c:if test="${result_locale_data.data.binddefault == 'N'}">
											<div class="ttb-input">
											<!-- 否-->
												否
											</div>
										</c:if>
									</span>
								</div>
							</div>
													<!-- 確定 -->
						<input type="button" id="BACKSUBMIT" value="返回首頁" class="ttb-button btn-flat-orange"/>
					</div>
				</div>
			</section>
		</main>
	</div>

	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>