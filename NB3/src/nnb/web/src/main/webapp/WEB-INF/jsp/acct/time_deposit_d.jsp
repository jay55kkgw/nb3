<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
	<style>
		.Line-break{
			white-space:normal;
			word-break:break-all;
			width:100px;
			word-wrap:break-word;
		}
	</style>
</head>
<body>
    
        <!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
	<!-- 定存服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Current_Deposit_To_Time_Deposit" /></li>
    <!-- 定存明細查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.NTD_Time_Deposit_Detail" /></li>
		</ol>
	</nav>



		<!--  快速選單及主頁內容 -->
		<!-- menu、登出窗格 --> 
 		<div class="content row">
 			<!-- 功能選單內容 -->
 		   <%@ include file="../index/menu.jsp"%>
 		<main class="col-12">
 		   
		<!-- 主頁內容  -->

			<section id="main-content" class="container">
		
				<h2><spring:message code="LB.NTD_Time_Deposit_Detail" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
						<!-- 下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
						<!-- 下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
					</select>
				</div>
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<ul class="ttb-result-list">
							<li>
								<!-- 查詢時間 -->
								<h3>
								<spring:message code="LB.Inquiry_time" />
								</h3>
								<p>							
								${time_deposit_details.data.CMQTIME }
								</p>
							</li>
							<li>
								
								<!-- 資料總數 -->
								<h3>
								<spring:message code="LB.Total_records" />
								</h3>
								<p>
								${time_deposit_details.data.CMRECNUM } <spring:message code="LB.Rows" />
								</p>
							</li>
							<li>
								<!-- 總計金額 -->
								<h3>
								<spring:message code="LB.Total_amount" />
								</h3>
								<p>
								${TOTAMT }
								</p>
							</li>
						</ul>
						<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">							<thead>
							<tr>
								<!-- 帳號 -->
								<th>
									<spring:message code="LB.Account" />
								</th>
								<!-- 存款種類/存單號碼 -->
								<th>
									<spring:message code="LB.Deposit_type" /><hr/><spring:message code="LB.Certificate_no" />
								</th>
								<!-- 存單金額 -->
								<th>
									<spring:message code="LB.Certificate_amount" />
								</th>
								<!-- 計息方式/利率(%) -->
								<th>
									<spring:message code="LB.Interest_calculation" /><hr/><spring:message code="LB.Interest_rate1" />
								</th>
								<!-- 起存日/到期日 -->
								<th>
									<spring:message code="LB.Start_date" /><hr/><spring:message code="LB.Maturity_date" />
								</th>
								<!-- 利息轉入帳號 -->
								<th>
									<spring:message code="LB.Interest_transfer_to_account" />
								</th>
								<!-- 自動轉期已轉次數/自動轉期未轉次數  -->
								<th>
									<spring:message code="LB.Automatic_number_of_rotations" /><hr/><spring:message code="LB.Automatic_number_of_unrotated" />
								</th>
								<!-- 轉存方式/申請不轉期 -->
								<th>
									<spring:message code="LB.Rollover_method" /><hr><spring:message code="LB.Non_Automatic_unrotated_application" />
								</th>
								<!-- 備註/快速選單功能 -->
								<th>
									<spring:message code="LB.Note" /><hr/><spring:message code="LB.Quick_Menu" />
								</th>
							</tr>
							</thead>
							<tbody>
							<c:forEach var="dataList" items="${ time_deposit_details.data.REC }" varStatus="data">
								<tr>
									<!-- 帳號 -->
									<td class="text-center">${dataList.ACN }</td>
                					<!-- 存款種類/存單號碼 -->
                					<td class="text-center">${dataList.TYPENAME }<hr/>${dataList.FDPNUM }</td>
					                <!-- 存單金額 -->
					                <td class="text-right">${dataList.AMTFDPFMT }</td>
					                <!-- 計息方式/利率(%) -->
					                <td class="text-center">${dataList.INTMTH }<hr/>${dataList.ITR }</td>
					                <!-- 起存日/到期日 -->
					                <td class="text-center">${dataList.DPISDT }<hr/>${dataList.DUEDAT }</td>
					                <!-- 利息轉入帳號 -->
					                <td class="text-center">${dataList.TSFACN }</td>
					                <!-- 自動轉期已轉次數/自動轉期未轉次數 -->
					                <td class="text-center">${dataList.ILAZLFTM }<hr/>${dataList.AUTXFTM }</td>
					                <!-- 轉存方式/申請不轉期 -->
					                <td class="text-center">
						                <c:if test="${not empty dataList.TYPE1 }">${dataList.TYPE1 }</c:if>
						                <c:if test="${empty dataList.TYPE1 }">-</c:if>
						                <hr/>
						                 <c:if test="${not empty dataList.TYPE2 }">${dataList.TYPE2 }</c:if>
						                <c:if test="${empty dataList.TYPE2 }">-</c:if>
					                </td>
					                <!-- 快速選單 -->
					                <td class="text-center">
					                	<c:if test="${dataList.REMIND eq true}">
											<!-- 說明連結 -->
											<a href="#"	class="origin"
											onclick="script:window.open('${__ctx}/Deposit.html');">
											<c:if test="${__i18n_locale == 'en'}">
												<spring:message code="LB.Note" />&nbsp;<spring:message code="LB.Description_of_page" />
											</c:if>
											<c:if test="${__i18n_locale != 'en'}">
												<spring:message code="LB.Note" /><spring:message code="LB.Description_of_page" />
											</c:if>
											</a><br>
					      				</c:if>
					                	<c:if test="${dataList.REMIND != true}">
						                	<c:if test="${__i18n_locale == 'en'}">
					                			No note
					                		</c:if>
						                	<c:if test="${__i18n_locale == 'zh_TW'}">
					                			無備註
					                		</c:if>
						                	<c:if test="${__i18n_locale == 'zh_CN'}">
					                			无备注
					                		</c:if>
					                		<br/>
					      				</c:if>
						                <c:choose>
	    									<c:when test="${dataList.OPTIONTYPE == '2'}">
	    										<!-- 下拉式選單-->
	    										<input type="button" class="d-lg-none" id="click" onclick="hd2('actionBar2${data.index}')" value="..." />
	<%--     										<input type="button" class="d-sm-none d-block" id="click" value="Click" onclick="hd2('actionBar2${data.index}')"/><!-- 快速選單測試 --> --%>
	    										<select class="d-none d-lg-inline-block custom-select fast-select" id="selectionBar" onchange="selectionBar(this.value,'${dataList.FDPNUM}','${dataList.ACN}')" style="width: 125px;">
													<option value="">-<spring:message code="LB.Select"/>-</option>
													<!-- 轉入臺幣零存整付按月存繳 -->
													<option value="installment_saving"><spring:message code="LB.Periodical_Deposits_And_Lumpsum_Payment_monthly"/></option>
												</select>
												<!-- 快速選單測試新增div -->
												<div id="actionBar2${data.index}" class="fast-div-grey" style="visibility: hidden">
													<div class="fast-div">
														<img src="${__ctx}/img/icon-close-2.svg" class="top-close-btn" onclick="hd2('actionBar2${data.index}')">
		                              					<p><spring:message code= "LB.X1592" /></p>
															<ul>
																<li onclick="selectionBar('installment_saving','${dataList.FDPNUM}','${dataList.ACN}')"><spring:message code="LB.Periodical_Deposits_And_Lumpsum_Payment_monthly"/><img src="${__ctx}/img/icon-10.svg" align="right"></li>
															</ul>
														<input type="button" class="bottom-close-btn" onclick="hd2('actionBar2${data.index}')" value="<spring:message code= "LB.X1572" />"/>
													</div>
												</div>
											</c:when>
											<c:when test="${dataList.OPTIONTYPE == '0'}">
	    										<!-- 下拉式選單-->
	    										<input type="button" class="d-lg-none" id="click" onclick="hd2('actionBar2${data.index}')" value="..." />
	<%--     										<input type="button" class="d-sm-none d-block" id="click" value="Click" onclick="hd2('actionBar2${data.index}')"/><!-- 快速選單測試 -->				 --%>
												<select class="d-none d-lg-inline-block custom-select fast-select" id="selectionBar" onchange="selectionBar(this.value,'${dataList.FDPNUM}','${dataList.ACN}','${dataList.MapValue}');"  style="width: 125px;">
													<option value=""><spring:message code="LB.Select"/></option>
													<!-- 臺幣綜存定存解約 -->
													<option value="deposit_cancel"><spring:message code="LB.NTD_Time_Deposit_Termination"/></option>
	    											<!-- 臺幣定存自動轉期申請/變更 -->
													<option value="renewal_apply"><spring:message code="LB.NTD_Time_Deposit_Automatic_Rollover_Application_Change"/></option>
												</select>
												<!-- 快速選單測試新增div -->
												<div id="actionBar2${data.index}" class="fast-div-grey" style="visibility: hidden">
													<div class="fast-div">
														<img src="${__ctx}/img/icon-close-2.svg" class="top-close-btn" onclick="hd2('actionBar2${data.index}')">
		                              					<p><spring:message code= "LB.X1592" /></p>
														<ul>
															<li onclick="selectionBar('deposit_cancel','${dataList.FDPNUM}','${dataList.ACN}','${dataList.MapValue}');"><spring:message code="LB.NTD_Time_Deposit_Termination"/><img src="${__ctx}/img/icon-10.svg" align="right"></li>
															<li onclick="selectionBar('renewal_apply','${dataList.FDPNUM}','${dataList.ACN}','${dataList.MapValue}');"><spring:message code="LB.NTD_Time_Deposit_Automatic_Rollover_Application_Change"/><img src="${__ctx}/img/icon-10.svg" align="right"></li>
														</ul>
														<input type="button" class="bottom-close-btn" onclick="hd2('actionBar2${data.index}')" value="<spring:message code= "LB.X1572" />"/>
													</div>
												</div>
											</c:when>
											<c:otherwise>
	    										<!-- 下拉式選單-->
	    										<input type="button" class="d-lg-none" id="click" onclick="hd2('actionBar2${data.index}')" value="..." />
	<%--     										<input type="button" class="d-sm-none d-block" id="click" value="Click" onclick="hd2('actionBar2${data.index}')"/><!-- 快速選單測試 -->				 --%>
												<select class="d-none d-lg-inline-block custom-select fast-select" id="selectionBar" onchange="selectionBar(this.value,'${dataList.FDPNUM}','${dataList.ACN}','${dataList.MapValue}');"  style="width: 125px;">
													<option value=""><spring:message code="LB.Select"/></option>
	    											<!-- 臺幣定存自動轉期申請/變更 -->
													<option value="renewal_apply"><spring:message code="LB.NTD_Time_Deposit_Automatic_Rollover_Application_Change"/></option>
												</select>
												<!-- 快速選單測試新增div -->
												<div id="actionBar2${data.index}" class="fast-div-grey" style="visibility: hidden">
													<div class="fast-div">
														<img src="${__ctx}/img/icon-close-2.svg" class="top-close-btn" onclick="hd2('actionBar2${data.index}')">
		                              					<p><spring:message code= "LB.X1592" /></p>
														<ul>
															<li onclick="selectionBar('renewal_apply','${dataList.FDPNUM}','${dataList.ACN}','${dataList.MapValue}');"><spring:message code="LB.NTD_Time_Deposit_Automatic_Rollover_Application_Change"/><img src="${__ctx}/img/icon-10.svg" align="right"></li>
														</ul>
														<input type="button" class="bottom-close-btn" onclick="hd2('actionBar2${data.index}')" value="<spring:message code= "LB.X1572" />"/>
													</div>
												</div>
											</c:otherwise>
										</c:choose>
									</td>
								</tr>
							</c:forEach>
						</tbody>
						</table>
						<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
					</div>
				</div>
					<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page"/></p>
						<span><spring:message code="LB.Time_deposit_P1_Desc" />：</span>
							<li><span><spring:message code="LB.Time_deposit_P1_D1" /></span></li>
					        <li><span><spring:message code="LB.Time_deposit_P1_D2" /></span></li>
					        <li><span><spring:message code="LB.Time_deposit_P1_D3" /></span></li>
					        <li><span><B><spring:message code="LB.Time_deposit_P1_D4" /></B></span></li>
					</ol>
				<form method="post" id="formId" action="${__ctx}/download">
					<!-- 下載用 -->
					<input type="hidden" name="downloadFileName" value="LB.NTD_Time_Deposit_Detail"/>
					<input type="hidden" name="CMQTIME" value="${time_deposit_details.data.CMQTIME }"/>
					<input type="hidden" name="COUNT" value="${COUNT}"/>
					<input type="hidden" name="TOTAMT" value="${TOTAMT}"/>
					<input type="hidden" name="downloadType" id="downloadType"/> 					
					<input type="hidden" name="templatePath" id="templatePath"/>
					<!-- EXCEL下載用 -->
					<input type="hidden" name="headerRightEnd" value="14"/>
					<input type="hidden" name="headerBottomEnd" value="5"/>
					<input type="hidden" name="rowStartIndex" value="6"/>
					<input type="hidden" name="rowRightEnd" value="14"/>				
					<!-- TXT下載用 -->
<!-- 					上到下長度 -->
					<input type="hidden" name="txtHeaderBottomEnd" value="16"/> 					
					<input type="hidden" name="txtHasRowData" value="true"/>
					<input type="hidden" name="txtHasFooter" value="true"/> 
					
								
				</form>
				<!-- for selectionBar -->
				<form method="post" id="fastBarAction" action="">
					<input type="hidden" id="FDPNUM" name="FDPNUM" value="">
					<input type="hidden" id="ACN" name="ACN" value="">
					<input type="hidden" id="FGSELECT" name="FGSELECT" value="">
				</form>
			</section>
		</main><!-- main-content END -->
	</div><!-- content row END -->	

	<%@ include file="../index/footer.jsp"%>
	<!-- 快速選單測試CSS -->
		<style type="text/css">
			.fast-btn-blcok {
				position: absolute;
				top: auto;
				bottom: auto;
				left: auto;
				right: 20%;
				min-width: 40%;
				margin: 0 auto;
				background-color: #fbf8e9;
				z-index: 999;
			}
			/* 箭頭上-邊框 */
			.fast-btn-blcok .arrow_t_out {
				width: 0px;
				height: 0px;
				border-width: 15px;
				border-style: solid;
				border-color: transparent transparent #fddd9a transparent;
				position: absolute;
				top: -29px;
				right: 20px;
			}

			.fast-btn-blcok ul {
				text-align: left;
				margin: 0 auto;
				padding: 5%;
				background-color: #fddd9a;
				border-radius: 5px;
			}

			.fast-btn-blcok ul li a {
				color: #374140;
			}
		
		</style>
		<script type="text/javascript">
			$(document).ready(function(){
				// HTML載入完成後開始遮罩
				setTimeout("initBlockUI()",10);
				// 開始查詢資料並完成畫面
				setTimeout("init()",20);
				
				setTimeout("initDataTable()",100);
				// 解遮罩
				setTimeout("unBlockUI(initBlockId)",500);		
			
			});
			
			function init(){				
					
				$("#printbtn").click(function(){
					var params = {
						"jspTemplateName":"time_deposit_d_print",
						"jspTitle":"<spring:message code='LB.NTD_Time_Deposit_Detail' />",
						"CMQTIME":"${time_deposit_details.data.CMQTIME}",
						"COUNT":"${COUNT}",
						"TOTAMT":"${TOTAMT}"
					};
					openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
				});
			}
			//選項
		 	function formReset() {
	 			if ($('#actionBar').val()=="excel"){
					$("#downloadType").val("OLDEXCEL");
					$("#templatePath").val("/downloadTemplate/time_deposit.xls");
		 		}else if ($('#actionBar').val()=="txt"){
					$("#downloadType").val("TXT");
					$("#templatePath").val("/downloadTemplate/time_deposit.txt");		 		
				}
				// 下載EXCEL,TXT submit
	 			$("#formId").attr("target", "${__ctx}/download");
				$("#formId").submit();
				$('#actionBar').val("");
			}
		 	
		 	// 快速選單
		 	function selectionBar(value,FDPNUM,ACN,FGSELECT){
				switch(value){
					case "deposit_cancel":
						$('#FDPNUM').val(FDPNUM);
						$('#ACN').val(ACN);
						$('#FGSELECT').attr("disabled",true);
						$('#fastBarAction').attr("action","${__ctx}/NT/ACCT/TDEPOSIT/deposit_cancel");
						$('#fastBarAction').submit();
						break;
					case "renewal_apply":
						$('#FDPNUM').attr("disabled",true);
						$('#ACN').attr("disabled",true);
						$('#FGSELECT').val(FGSELECT);
						$('#fastBarAction').attr("action","${__ctx}/NT/ACCT/TDEPOSIT/renewal_apply_step1");
						$('#fastBarAction').submit();
						break;
					case "installment_saving":
						$('#FDPNUM').val(FDPNUM);
						$('#ACN').val(ACN);
						$('#FGSELECT').attr("disabled",true);
						$('#fastBarAction').attr("action","${__ctx}/NT/ACCT/TDEPOSIT/installment_saving");
						$('#fastBarAction').submit();
						break;
				}
			}
		 	
		 	//快速選單測試新增
		 	//將actionbar select 展開閉合
			$(function(){
		   		$("#click").on('click', function(){
		       	var s = $("#actionBar2").attr('size')==1?8:1
		       	$("#actionBar2").attr('size', s);
		   		});
		   		$("#actionBar2 option").on({
		       		click: function() {
		           	$("#actionBar2").attr('size', 1);
		       		},
		   		});
			});
			function hd2(T){
				var t = document.getElementById(T);
				if(t.style.visibility === 'visible'){
					t.style.visibility = 'hidden';
				}
				else{
					$("div[id^='actionBar2']").each(function() {
						var d = document.getElementById($(this).attr('id'));
						d.style.visibility = 'hidden';
				    });
					t.style.visibility = 'visible';
				}
			}
		
		 	
		</script>
</body>
</html>