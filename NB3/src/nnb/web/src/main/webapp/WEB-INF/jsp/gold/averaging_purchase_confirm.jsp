<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<!-- 交易機制所需JS -->
<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
		initFootable();
		$("#formId").validationEngine({
			binded: false,
			promptPosition: "inline"
		});
		chaBlock();
		// 確認鍵 click
// 		submit();
		$("#CMSUBMIT").click( function(e) {
			console.log("submit~~");
			e = e || window.event;
			
         	if(!$('#formId').validationEngine('validate') && $('input[name="FGTXWAY"]:checked').val() == 0){
	        	e.preventDefault();
 			}else{
// 				$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
 				$("#formId").validationEngine('detach');
//     			$("#formId").attr("action","${__ctx}/GOLD/AVERAGING/averaging_purchase_result");
    			processQuery();
 			}
		});
		//上一頁按鈕
		$("#CMBACK").click(function() {
			initBlockUI();
			fstop.getPage('${pageContext.request.contextPath}'+'/GOLD/AVERAGING/averaging_purchase','', '');
		});
	}
	
	// 確認鍵 Click
	function submit() {
	}
	
	// 通過表單驗證準備送出
	function processQuery(){
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
		console.log("fgtxway: " + fgtxway);
		// 交易機制選項
		switch(fgtxway) {
			case '0':
				// 遮罩
	         	initBlockUI();
	         	$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
				$("#formId").submit();
				break;
			case '1':
				// IKEY
				useIKey();
			
				break;
           case '7'://IDGATE認證		 
               idgatesubmit= $("#formId");		 
               showIdgateBlock();		 
               break;
			default:
				//alert("nothing...")
				errorBlock(
						null, 
						null,
						["nothing..."], 
						'<spring:message code= "LB.Quit" />', 
						null
				);
		}
	}
	
	// 判斷交易機制決定顯不顯示驗證碼區塊
	function chaBlock() {
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
			
			
			switch(fgtxway) {
			case '0':
				$('#CMSSL').attr('checked',true);
				$('#CMPASSWORD').addClass("validate[required]");
				break;
			case '1':
				$('#CMSSL').removeAttr('checked');
				$('#CMPASSWORD').removeClass("validate[required]");
		    	break;
			case '7':
				$('#CMSSL').removeAttr('checked');
				$('#CMPASSWORD').removeClass("validate[required]");
		    	break;
		    	
			default:
				$('#CMSSL').removeAttr('checked');
				$('#CMPASSWORD').removeClass("validate[required]");
		}
	}

</script>
</head>
<body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
    <!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %>  
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 定期定額交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1552" /></li>
    <!-- 定期定額申購     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1553" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
			<!--  黃金定期定額 -->
				<h2><spring:message code= "LB.X0930" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form id="formId" method="post" action="${__ctx}/GOLD/AVERAGING/averaging_purchase_result">
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
                        	<c:set value="${averaging_purchase_confirm.data}" var="dataList"></c:set>
                            <!--約定扣款帳號 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.W1539" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                       <span>${dataList.SVACN}</span>
                                    </div>
                                </span>
                            </div>    
                                
                            <!--黃金存摺帳號 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D1090" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                     	<span>${dataList.ACN}</span>
                                    </div>
                                </span>
                            </div>

                            <!--每月投資日/金額-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.W1557" /></h4>
                                    </label></span>
                                <span class="input-block">
									<div class="ttb-input">
                                         <span>06<spring:message code="LB.Day" />&nbsp;<spring:message code="LB.NTD" />&nbsp;${dataList.AMT_06_1}&nbsp;<spring:message code="LB.Dollar" /></span>
                                     </div>
									<div class="ttb-input">
										<span>16<spring:message code="LB.Day" />&nbsp;<spring:message code="LB.NTD" />&nbsp;${dataList.AMT_16_1}&nbsp;<spring:message code="LB.Dollar" /></span>
									</div>
									<div class="ttb-input">
										 <span>26<spring:message code="LB.Day" />&nbsp;<spring:message code="LB.NTD" />&nbsp;${dataList.AMT_26_1}&nbsp;<spring:message code="LB.Dollar" /></span>
									</div>
                                </span>
                            </div>
                            
							<!--交易機制-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.Transaction_security_mechanism" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
                                       <label class="radio-block"><spring:message	code="LB.SSL_password" /> 
                                            <input type="radio" name="FGTXWAY" id="CMSSL" value="0" checked onclick="chaBlock()">
                                            <span class="ttb-radio"></span>
                                        </label>
                                    </div>
									<div class="ttb-input">
										<input type="password" class="text-input" name="CMPASSWORD" id="CMPASSWORD" autocomplete="off" size="8" maxlength="8" value="">
									</div>
									<!-- 使用者是否可以使用IKEY -->
									<c:if test = "${sessionScope.isikeyuser}">
                                    	<div class="ttb-input">
                                       		<label class="radio-block"><spring:message code="LB.Electronic_signature" />
                                           		<input type="radio" name="FGTXWAY" id="CMIKEY" value="1" onclick="chaBlock()">
                                           		<span class="ttb-radio"></span>
                                        	</label>
                                    	</div>
                                    </c:if>             
                                    
									<div class="ttb-input" name="idgate_group" style="display:none" onclick="chaBlock()">		 
                                       <label class="radio-block">裝置推播認證(請確認您的行動裝置網路連線是否正常，及推播功能是否已開啟)
	                                       <input type="radio" id="IDGATE" 	name="FGTXWAY" value="7"> 
	                                       <span class="ttb-radio"></span>
                                       </label>		 
                                   </div>
                                </span>
                            </div>
							
                        </div>
                        <input class="ttb-button btn-flat-gray" type="button" name="CMBACK" id="CMBACK" value="<spring:message code="LB.Back_to_previous_page" />" onclick="back()"/>    
                        <input class="ttb-button btn-flat-gray" name="CMRESET" type="reset" value="<spring:message code="LB.Re_enter" />" />
                        <input class="ttb-button btn-flat-orange" id="CMSUBMIT" name="CMSUBMIT" type="button" value="<spring:message code="LB.Confirm" />" />
                        <input type="hidden" name="ADOPID" id="ADOPID" value="N09301">
						<input type="hidden" id="SVACN" name="SVACN" value="${dataList.SVACN}">
						<input type="hidden" id="ACN" name="ACN" value="${dataList.ACN}">
						<input type="hidden" id="AMT_06" name="AMT_06" value="${dataList.AMT_06}">
						<input type="hidden" id="AMT_16" name="AMT_16" value="${dataList.AMT_16}">
						<input type="hidden" id="AMT_26" name="AMT_26" value="${dataList.AMT_26}">
						<input type="hidden" name="DPMYEMAIL" value="${sessionScope.dpmyemail}">
                        <!-- ikey -->
						<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
						<input type="hidden" id="jsondc" name="jsondc" value='${dataList.jsondc }'>
						<input type="hidden" name="CMTRANPAGE" value="1">
						<input type="hidden" id="ISSUER" name="ISSUER" value="">
						<input type="hidden" id="ACNNO" name="ACNNO" value="">
						<input type="hidden" id="TRMID" name="TRMID" value="">
						<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
						<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
						<input type="hidden" id="TAC" name="TAC" value="">
						
                        <input type="hidden" id="PINNEW" name="PINNEW" value="">
                    </div>
                </div>
				</form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>