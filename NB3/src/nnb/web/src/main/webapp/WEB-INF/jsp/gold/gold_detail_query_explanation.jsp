<%@ page language="java" contentType="text/html; charset=BIG5"
    pageEncoding="BIG5"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>檔案欄位說明</title>
<link href="../css/css-reset.css" rel="stylesheet" type="text/css" />
<link href="../css/layout.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="../script/utility.js"></script>
</head>

<body>

  <div id="PrintLogo" class="NoScreen"> <img src="../imgs/logo1.gif" alt="Logo" /> </div>
  <div id="PrintArea" class="WidthS">
  	<div id="TitleBar">
      <h4>檔案欄位說明</h4>
    </div>
    <table width="100%" class="DataBorder">
      <tr>
      	<th>欄位起迄位置</th>                
      	<th>欄位長度</th>
      	<th>欄位說明</th>
      </tr>
      <col align="left" />
      <col align="left" />
      <col align="left" />
      
		  <tr class="Row0">
				<td>第001位-----第010位</td>
				<td>10</td>
				<td>異動日</td>
		  </tr>
		  <tr class="Row1">
				<td>第011位-----第021位</td>
				<td>11</td>
				<td>摘要</td>
		  </tr>
		  <tr class="Row0">
				<td>第022位-----第040位</td>
				<td>19</td>
				<td>支出金額</td>
		  </tr>
		  <tr class="Row1">
				<td>第041位-----第059位</td>
				<td>19</td>
				<td>收入金額</td>
		  </tr>
		  <tr class="Row0">
				<td>第060位-----第078位</td>
				<td>19</td>
				<td>餘額</td>
		  </tr>
		  <tr class="Row1">
				<td>第079位-----第087位</td>
				<td>09</td>
				<td>票據號碼</td>
		  </tr>
		  <tr class="Row0">
				<td>第088位-----第104位</td>
				<td>17</td>
				<td>資料內容</td>
		  </tr>
		  <tr class="Row1">
				<td>第105位-----第111位</td>
				<td>07</td>
				<td>收付行</td>
		  </tr>
		  <tr class="Row0">
				<td>第112位-----第121位</td>
				<td>10</td>
				<td>交易時間</td>
		  </tr>
		  
  </table>
  <div class="ButtonBar NoPrint">
		<input type="button" name="CMPRINT" id="CMPRINT" value="列印" onClick="FriendlyPrint()" />
  </div>
  </div>

</body>
</html>