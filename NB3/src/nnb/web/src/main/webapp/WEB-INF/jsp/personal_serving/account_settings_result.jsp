<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
		initFootable();
		$('#but_Agree').prop('checked', false);
		// 確認鍵 click
		'${inward_notice_setting_result.data.DPOVERVIEW}'.split(',').forEach(function(i){
			$("#cho" + i).attr("checked",true);
		});
	}
	

</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
	<!-- 帳戶總覽設定 -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0959" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<!-- 帳戶總覽設定 -->
				<h2><spring:message code= "LB.X0959" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="">
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
							<div class="ttb-message">
<!-- 變更設定成功 -->
                    			<span><spring:message code= "LB.Change_Settings_Successfully" /></span>
                			</div>
                            <!-- 系統時間:-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
<!-- 系統時間 -->
                                        <h4><spring:message code= "LB.D0121" />:</h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                    	<span>${inward_notice_setting_result.data.DATETIME}</span>
                                    </div>
                                </span>
                            </div> 
                                
                            <!-- 項目 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
<!-- 項目 -->
                                        <h4><spring:message code= "LB.D0271" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                    	<label  class="check-block" for="cho1">
                                        <!-- 臺幣存款 -->
                                        	<spring:message code= "LB.X0960" />
											<input id="cho1" checked type="checkbox"  name="r" value="ON" disabled name="r">
											<span class="ttb-check"></span>
                                        </label>
                                       
                                    </div>
                                    
                                    <div class="ttb-input">
                                    	<label class="check-block" for="cho2">
                                        <!-- 外匯存款 -->
                                        	<spring:message code= "LB.X0961" />
											<input id="cho2" type="checkbox" name="r" value="ON" disabled name="r2">
											<span class="ttb-check"></span>
                                        </label>
                                        
                                    </div>
                                    <div class="ttb-input">
                                        <label class="check-block" for="cho3">
                                        <!-- 借款 -->
                                        	<spring:message code= "LB.X0962" />
											<input id="cho3"  type="checkbox"   name="r" value="3" disabled name="r3">
											<span class="ttb-check"></span>
                                        </label>
                                    </div>
                                    <div class="ttb-input">
<!-- 基金 -->								<label class="check-block" for="cho4">
                                        <!-- 基金 -->
                                        	<spring:message code= "LB.Funds" />
											<input id="cho4" type="checkbox"  name="r" value="4" disabled name="r4">
											<span class="ttb-check"></span>
                                        </label>
                                    </div>
                                    <div class="ttb-input">
<!-- 信用卡 -->								<label class="check-block" for="cho5">
                                        <!-- 信用卡 -->
                                        	<spring:message code= "LB.D0001" />
											<input id="cho5"  type="checkbox"  name="r" value="5" disabled name="r5">
											<span class="ttb-check"></span>
                                        </label>
                                    </div>
                                    <div class="ttb-input">
                                       <label class="check-block" for="cho6">
                                        <!-- 債票劵 -->
                                        	<spring:message code= "LB.X1211" />
											<input id="cho6"  type="checkbox"  name="r" value="6" disabled name="r6">
											<span class="ttb-check"></span>
                                        </label>
                                    </div>
                                    <div class="ttb-input">
<!-- 黃金存摺 -->
                                       <label class="check-block" for="cho7">
										<!-- 黃金存摺 -->
											<spring:message code= "LB.W1428" />
											<input id="cho7"  type="checkbox"  name="r" value="7" disabled name="r7">
											<span class="ttb-check"></span>
                                        </label>
                                    </div>
                                </span>
                            </div> 
                        </div>
                    </div>
                </div>
				</form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>