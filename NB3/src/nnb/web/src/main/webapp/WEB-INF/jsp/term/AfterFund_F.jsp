<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<body>
	<p style="text-align: center;">
		<strong>特定金錢信託投資後收型基金投資說明書暨特別約定事項-境外基金</strong>
	</p>
	<p style="text-align: left;">
		本特別約定事項係委託人與臺灣中小企業銀行簽訂之「特定金錢信託投資國內外有價證券信託約定書」之特別約定條款，
		<strong><font color="red">委託人同意投資證券投資信託/顧問股份有限公司(下稱投信/投顧公司)所發行之後收型基金(鋒裕匯理T/U股、路博邁C2/E/B股、NN(L)Y股、野村愛爾蘭B股、富蘭克林F股、駿利亨德森V股、先機C2股、摩根F股、聯博E股、施羅德U股、安聯B股)，並遵守下列約定，</font></strong>
		下列<strong><font color="red">事項</font></strong>若有更新或未規定之事項，則依上開之約定書及基金公開說明書之規定辦理。
	</p>
	<div class="ttb-result-list terms">
		<ul>
			<li>一、申購</li>
			<ol>
				<li data-num="1、">以單筆投資方式辦理。</li>
				<li data-num="2、">申購手續費：委託人於申購時無須繳交任何申購手續費。</li>
				<li data-num="3、">每筆最低投資金額：</li>
				<li>
				<table class="terms-table">
					<tr>
						<th></th>
						<th><p>新台幣</p></th>
						<th><p>美元</p></th>
						<th><p>歐元</p></th>
						<th><p>澳幣</p></th>
						<th><p>南非幣</p></th>
					</tr>
					<tr>
						<th><p>每筆最低投資金額</p></th>
						<th>120,000 元</th>
						<th>5,000元</th>
						<th>5,000元</th>
						<th>5,000元</th>
						<th>50,000元</th>
					</tr>
				</table>
				</li>
			</ol>
			<li>二、轉換</li>
			<ol>
				<li data-num="1、"><strong><font color="red">後收型基金</font></strong>於轉換時僅接受每筆全部基金單位數一次轉換。</li>
				<li data-num="2、"><strong><font color="red">後收型基金</font></strong>僅能轉換至同一系列之其他同類型基金。<br>
				 <strong><font color="red">(如: 甲投信/投顧公司基金U股轉甲投信/投顧公司基金U股)</font></strong>
				</li>
				<li data-num="3、">轉換手續費：委託人申請轉換至同一基金系列其他同類型之基金(U股轉U股)，<strong><font color="red">投信/投顧</font></strong>
					公司之轉換手續費依各基金公開說明書規定收取，另受託人將收取轉換手續費每筆新台幣 500 元，
					<strong><font color="red">倘嗣後本行調降手續費時，則依本行規定辦理。</font></strong></li>
			</ol>
			<li>三、贖回</li>
			<ol>
				<li data-num="1、"><strong><font color="red">後收型基金</font></strong>於贖回時僅接受每筆全部基金單位數一次贖回。</li>
				<li data-num="2、">遞延銷售手續費（CDSC：Contingent Deferred Sale Charge）<br>
					<strong><font color="red">
					(1)鋒裕匯理、路博邁、NN(L)、野村愛爾蘭、富蘭克林、駿利亨德森、聯博、施羅德、安聯：計算方式係按申購時淨資產價值與贖回時市價孰低者，乘以下表所載費率，於基金贖回時由基金公司自贖回總額中扣收。<br>
					(2)先機：計算方式係按申購時淨資產價值乘以下表所載費率，於基金贖回時由基金公司自贖回總額中扣收。<br>
					(3)摩根：計算方式係按贖回時市價乘以下表所載費率，於基金贖回時由基金公司自贖回總額中扣收。<br>
					</font></strong>
				</li>
				<li><p align="center">基金公司<strong><font color="red">遞延銷售手續費率表</font></strong></p></li>
				<li>
				<table class="terms-table">
					<tbody>
						<tr style="text-align: center;vertical-align: middle;">
							<td style="vertical-align: middle;"> 
								<p>於基金公司之持有期間/遞延銷售手續費率/類別</p>
							</td>
							<td style="vertical-align: middle;">
								<p>未滿1年</p>
							</td>
							<td style="vertical-align: middle;">
								<p>1年(含)-2年</p>
							</td>
							<td style="vertical-align: middle;">
								<p>2年(含)-3年</p>
							</td>
							<td style="vertical-align: middle;">
								<p>3年(含)-4年</p>
							</td>
							<td style="vertical-align: middle;">
								<p>4年以上</p>
							</td>
						</tr>
						<tr style="text-align: center;vertical-align: middle;">
							<td style="vertical-align: middle;">
								<p>路博邁B股</p>
							</td>
							<td style="vertical-align: middle;">4%</td>
							<td style="vertical-align: middle;">3%</td>
							<td style="vertical-align: middle;">2%</td>
							<td style="vertical-align: middle;">1%</td>
							<td style="vertical-align: middle;">0%</td>
						</tr>
						<tr style="text-align: center;">
							<td style="vertical-align: middle;">
								<p>駿利亨德森V股/NN(L)Y股/<br>鋒裕匯理U股/路博邁E股/富蘭克林F股/<br>摩根F股/野村愛爾蘭B股/<br>聯博E股/施羅德U股/安聯B股</p>
							</td>
							<td style="vertical-align: middle;">3%</td>
							<td style="vertical-align: middle;">2%</td>
							<td style="vertical-align: middle;">1%</td>
							<td style="vertical-align: middle;">0%</td>
							<td style="vertical-align: middle;">0%</td>
						</tr>
						<tr style="text-align: center;">
							<td style="vertical-align: middle;">
								<p>路博邁C2股/先機C2股/鋒裕匯理T股</p>
							</td>
							<td style="vertical-align: middle;">2%</td>
							<td style="vertical-align: middle;">1%</td>
							<td style="vertical-align: middle;">0%</td>
							<td style="vertical-align: middle;">0%</td>
							<td style="vertical-align: middle;">0%</td>
						</tr>
						<tr style="text-align: left;">
							<td colspan="6" style="vertical-align: middle;">
								<p>※上開手續費率如有變動悉依基金公司最新之規定辦理。</p>
							</td>
						</tr>
					</tbody>
				</table>
				</li>
			</ol>
			<li><strong><font color="red">四、基金分銷費用(Distribution Fee，或稱為代銷費、經銷費)</font></strong></li>
				<ol>
					<li>該分銷費係基金公開說明書所規定，依持有期間每日計算至贖回日為止，並非受託人額外收取，由基金公司逕自從持有基金資產價值中扣除。</li>
					<li><p align="center">基金公司基金分銷費費率表</p></li>
					<li>
					<table class="terms-table">
						<tbody>
							<tr style="text-align: center;vertical-align: middle;">
								<td style="vertical-align: middle; width: 60%"> 
									<p>基金系列</p>
								</td>
								<td style="vertical-align: middle;">
									<p>年費率</p>
								</td>
								
							</tr>
							<tr style="text-align: center;vertical-align: middle;">
								<td style="vertical-align: middle;">
									<p>聯博E股債券型/股票型</p>
								</td>
								<td style="vertical-align: middle;">
									0.5% / 1%
								</td>
							</tr>
							<tr style="text-align: center;">
								<td style="vertical-align: middle;">
									<p>鋒裕匯理T/U股、路博邁C2/E/B股、NN(L)Y股、<br>野村愛爾蘭B股、富蘭克林F股、先機C2股、<br>摩根F股、施羅德U股、安聯B股等全系列</p>
								</td>
								<td style="vertical-align: middle;">
									1%
								</td>
							</tr>
						</tbody>
					</table>
					</li>
				</ol>
			<li><strong><font color="red">五、短線交易費用:</font></strong><br>
				<strong><font color="red">
					委託人持有基金未滿短線交易天期者，除遞延銷售手續費外，另應依公開說明書短線交易規範，按短線交易費率於贖回淨資產價值扣收短線交易費用。
				</font></strong>
			</li>
			<li><strong><font color="red">六、特約事項：</font></strong></li>
			<li data-num="（一）">
				<ol>
				<strong><font color="red">委託人已審閱各該手續費後收型境外基金公開說明書及交易文件相關內容，且充分明瞭手續費後收型境外基金之交易特性及風險，願同意接受所有交易條件及承擔一切投資風險，並瞭解投資最大可能損失為原始本金。</font></strong>
				</ol>
			</li>
			<li data-num="（二）">
				<ol>
					<strong><font color="red">
						手續費後收型境外基金，委託人於持有一段期間後，可能自動轉換為前收型境外基金，委託人申購前應詳閱該基金公開說明書及投資人須知相關規定。
					</font></strong>
				</ol>
			</li>
		</ul>
</body>
</html>