<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<!--舊版驗證-->
<script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//若前頁有帶acn，預設為此acn
	var getacn = '${reqACN}';
	if(getacn != null && getacn != ''){
		$("#ACN option[value= '"+ getacn +"' ]").prop("selected", true);
		displayGoldBal();
	}
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	
// 	$("#ACN").val("${reqACN}");
	
	$("#CMSUBMIT").click(function(){
		//欄位檢核   	  
		//黃金轉出帳號
		if(!CheckSelect("ACN","<spring:message code= "LB.W1515" />","#")){
	   		return false;
		}
		//若為部分賣出，須檢核賣出公克數
		if($("input[name=SELLFLAG]:checked").val() == "P"){
			if($("#TRNGD").val().indexOf(".") != -1){
				//請輸入整數的賣出公克數
				//alert("<spring:message code= "LB.X0940" />");
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.X0940' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
				);
				return false;
			}
			//賣出公克數
		   	if(!CheckNumber("TRNGD","<spring:message code= "LB.W1519" />",false)){
		   	    return false;
		   	}
			if (parseInt($("#TRNGD").val()) < 1 || parseFloat($("#TRNGD").val()) > parseFloat($("#TSFBAL_H").val())){
				//每筆最低交易數量為 1 公克，最高交易數量不可超過黃金存摺可用餘額。
				//alert("<spring:message code= "LB.X0941" />");
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.X0941' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
				);
				return false;	
			}
		}
		initBlockUI();
		$("#formID").submit();
	});
	$("#resetButton").click(function(){
		$("#ACN").val("#");
		$("#TRNGD").val("");
	});
});
function displayGoldBal(){
	if($("#ACN").val() != "#"){
		var URI = "${__ctx}/GOLD/TRANSACTION/getGoldTradeTWAccountListAjax2";
		var rqData = {ACN:$("#ACN").val()};
		fstop.getServerDataEx(URI,rqData,false,getGoldTradeTWAccountListAjax2Finish);
	}
}
function getGoldTradeTWAccountListAjax2Finish(data){
	if(data.result == true){
		var goldData = $.parseJSON(data.data);
			
			if(goldData.TSFBAL){
				//公克
				$("#TSFBAL").html(goldData.TSFBAL + "<spring:message code="LB.W1435"/>");
				//公克
				$("#GDBAL").html(goldData.GDBAL + "<spring:message code="LB.W1435"/>");
				$("#IN_ACN").html(goldData.SVACN);
				$("#GOLDRMBAL").show();
				$("#GOLDAVBAL").show();
				$("#TSFBAL_H").val(goldData.TSFBAL);
				$("#GDBAL_H").val(goldData.GDBAL);
				$("#SVACN").val(goldData.SVACN);
			}else{
				//公克
				$("#TSFBAL").html("<spring:message code="LB.W1435"/>");
				//公克
				$("#GDBAL").html("<spring:message code="LB.W1435"/>");
				$("#IN_ACN").html("");
				$("#GOLDRMBAL").show();
				$("#GOLDAVBAL").show();
				$("#TSFBAL_H").val("");
				$("#GDBAL_H").val("");
				$("#SVACN").val("");
			}
			
		
		if (goldData.TSFBAL != goldData.GDBAL || goldData.TSFBAL == "0.00") {
			$("input[name=SELLFLAG][value=A]").prop("disabled", true);
		} else {
			$("input[name=SELLFLAG][value=A]").prop("disabled", false);
		}
	} else {
		//無法取得黃金存摺帳號資料
		//alert("<spring:message code= "LB.X0942" />");
		errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.X0942' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
		);
	}
}
</script>
</head>
<body>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %> 
	<!--header-->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 黃金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2272" /></li>
    <!-- 黃金回售     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1512" /></li>
		</ol>
	</nav>

	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!--快速選單及主頁內容-->
		<main class="col-12"> 
			<!--主頁內容-->
			<section id="main-content" class="container">
			<!-- 黃金回售 -->
				<h2><spring:message code="LB.W1512"/></h2><i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00"></i>
                <form id="formID" action="${__ctx}/GOLD/TRANSACTION/gold_resale_confirm" method="post">
                	<input type="hidden" name="ADOPID" value="N09002_C"/>
					<input type="hidden" name="TRNCOD" value="01"/>
					<input type="hidden" name="FGTXWAY" value="0"/>
					<input type="hidden" id="TSFBAL_H" name="TSFBAL_H" value="0"/>
					<input type="hidden" id="GDBAL_H" name="GDBAL_H" value="0"/>
					<input type="hidden" id="SVACN" name="SVACN"/>
	                <div class="main-content-block row">
	                    <div class="col-12 tab-content">
	                        <div class="ttb-input-block">
								<!--交易日期-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
										<label>
	                                        <h4><spring:message code="LB.Transaction_date"/></h4>
	                                    </label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
	                                       	<span>
	                                       		${nowDate}<br/>
	                                       		<!-- (營業日09:00分起至15:30分) -->
	                                       		<font color="red">(<spring:message code= "LB.X0943" />)</font>
	                                       	</span>
										</div>
									</span>
								</div>
								 <!--黃金轉出帳號-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
										<label>
											<h4><spring:message code="LB.W1515"/></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<select name="ACN" id="ACN" onchange="displayGoldBal()" class="custom-select select-input half-input">
												<!-- 請選擇帳號 -->
												<option value="#">---<spring:message code="LB.Select_account"/>---</option>
												<c:forEach var="dataMap" items="${goldTradeAccountList}">
													<option value="${dataMap.ACN}">${dataMap.ACN}</option>
												</c:forEach>
											</select>
										</div>
									</span>
								</div>
								<!--黃金存摺帳戶餘額-->
	                            <div id="GOLDRMBAL" class="ttb-input-item row" style="display:none">
	                                <span class="input-title">
										<label>
											<h4><spring:message code="LB.W1516"/></h4>
										</label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
	                                       	<font id="GDBAL"></font>
										</div>
									</span>
								</div>
								<!--黃金存摺可用餘額-->
	                            <div id="GOLDAVBAL" class="ttb-input-item row" style="display:none">
	                                <span class="input-title">
										<label>
											<h4><spring:message code="LB.W1517"/></h4>
										</label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
	                                       	<font id="TSFBAL"></font>
										</div>
									</span>
								</div>
								<!--台幣轉入帳號-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
										<label>
											<h4><spring:message code="LB.W1518"/></h4>
										</label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
	                                       	<font id="IN_ACN"></font>
										</div>
									</span>
								</div>
								<!--賣出公克數-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
										<label>
											<h4><spring:message code="LB.W1519"/></h4>
										</label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
											<!-- 部分賣出 -->
												<input type="radio" name="SELLFLAG" value="P" checked/><spring:message code="LB.W1520"/>
											<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<!-- 公克 -->
	                                       	<input type="text" id="TRNGD" name="TRNGD" maxLength="5" style="width:70px;" class="text-input"/>&nbsp;&nbsp;<spring:message code="LB.W1435"/><br/>
	                                     </div>
	                                     <div class="ttb-input">
											<label class="radio-block">
											<!-- 全部賣出 -->
											<input type="radio" name="SELLFLAG" value="A"/><spring:message code="LB.W1522"/>
											<span class="ttb-radio"></span>
											</label>
										</div>
									</span>
								</div>
							</div>
	                    </div>
	                    <div style="margin: auto;">
	                  				<!-- 重新輸入 -->
	                				<input type="button" id="resetButton" value="<spring:message code="LB.Re_enter"/>" class="ttb-button btn-flat-gray"/>	
	                				<!-- 確定 -->
	                				<input type="button" id="CMSUBMIT" value="<spring:message code="LB.Confirm"/>" class="ttb-button btn-flat-orange"/>
	                		</div>
	                </div>
	                <ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /></p><!-- 說明 -->
						<!-- 每筆最低交易數量為1公克，庫存黃金帳戶餘額如不足1公克須全部一次賣出。 -->
							<li><spring:message code="LB.Gold_Resale_P1_D1"/></li>
							<!-- 黃金存摺不支付利息，黃金價格有漲有跌，投資時可能產生收益或損失，敬請慎選買賣時機，並承擔風險。 -->
							<li><spring:message code="LB.Gold_Resale_P1_D2"/></li>
							<!-- 「黃金回售」免手續費。 -->
							<li><spring:message code="LB.Gold_Resale_P1_D3"/></li>
						</ol>
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>