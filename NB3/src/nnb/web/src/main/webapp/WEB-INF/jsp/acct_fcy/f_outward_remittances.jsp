<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp"%>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<!-- 讀卡機所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/cardReaderMethod.js"></script>
	
	<script type="text/JavaScript">
		$(document).ready(function() {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 100);
		});

		// 畫面初始化
		function init() {
			// 確認鍵 click
			processQuery();
		}
		
		// 送出表單
		function processQuery() {
			$("#CMSUBMIT").click( function(e) {
				var main = document.getElementById("formId");
				main.CMSUBMIT.disabled = true;
				main.submit();
				return false;
			});
		}
	
	</script>
</head>
<body>
	<!-- 交易機制元件所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>

	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 匯出匯款     -->
    		<li class="ttb-breadcrumb-item"><spring:message code="LB.W0286" /></li>
    <!-- 匯出匯款     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0286" /></li>
		</ol>
	</nav>




	<!-- 功能清單及登入資訊 -->
	<div class="content row">

		<!-- 功能清單 -->
		<%@ include file="../index/menu.jsp"%>

		<!-- 快速選單及主頁內容 -->
		<main class="col-12"> <!-- 主頁內容  -->
			<section id="main-content" class="container">
				<!-- 功能內容 -->
				<form method="post" id="formId" action="${__ctx}/FCY/REMITTANCES/f_outward_remittances_step1?ACN="+ "${f_outward_remittances.data.urlID}">
					<input type="hidden" name="FXAGREEFLAG" value="Y">
					<input type="hidden" id="ACN"name="URLID" value="${f_outward_remittances.data.urlID}">
					<!-- 功能名稱 -->
					<h2><spring:message code="LB.W0286" /></h2>
					<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
	
					<div class="main-content-block row">
						<div class="col-12">
						<c:choose>
							<c:when test="${pageContext.response.locale=='zh_CN'}">
									<%@ include file="fcy_description_zh_CN.jsp"%>
							</c:when>
							<c:when test="${pageContext.response.locale=='en'}">
									<%@ include file="fcy_description_en.jsp"%>									
							</c:when>
							<c:otherwise>
									<%@ include file="fcy_description_zh_TW.jsp"%>		
							</c:otherwise>
						</c:choose>
				
							<input type="button" id="CMSUBMIT" name="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
						</div>
					</div>
				</form>
			</section>
		</main>
	</div>

	<%@ include file="../index/footer.jsp"%>

</body>
</html>
