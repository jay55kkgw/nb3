<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<link rel="stylesheet" type="text/css"
	href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript"<input Type="radio" name="TYPE" id="" value="7" />
	src="${__ctx}/js/jquery.datetimepicker.js"></script>
<script type="text/JavaScript">
	$(document).ready(function() {
		tabEvent();
		$("#cmback").click(function() {
			var action = '${__ctx}/nb3/login';
			$("form").attr("action", action);
			$("form").submit();
		});
		$("#CMSUBMIT").click(function(e) {
			var TYPE = $("input[name='TYPE']:checked").val();
			switch (TYPE) {
			case "3":
				$('#CN19-now-apply').hide();
				$('#MESSAGE').hide();
				$('#TYPE3REQ').show();
				$('#SUBSUBMIT').show();
				break;
			case "4":
				$('#CN19-now-apply').hide();
				$('#MESSAGE').hide();
				$('#TYPE4REQ').show();
				$('#SUBSUBMIT').show();
				break;
			case "8":
				$('#CN19-now-apply').hide();
				$('#MESSAGE').hide();
				$('#TYPE8REQ').show();
				$('#SUBSUBMIT').show();
				break;
			case "9":
				$('#CN19-now-apply').hide();
				$('#MESSAGE').hide();
				$('#TYPE9REQ').show();
				$('#SUBSUBMIT').show();
				break;
			case "18":
				window.open('https://nnb.tbb.com.tw/TBBNBAppsWeb/eeloanweb/NA05.jsp', '_blank');
				break;
			default:
				$('#formId').submit();
			}

		});

		$("#CMCANCEL").click(function(e) {
			$("#CN19-now-apply, #CN19-description, #CN19-button").show();
			$("#CN19-how-apply").hide();
			$('#TYPE3REQ').hide();
			$('#TYPE4REQ').hide();
			$('#TYPE8REQ').hide();
			$('#TYPE9REQ').hide();
			$('#SUBSUBMIT').hide();
		})

		$("#CMSUBMIT2").click(function(e) {
			var TYPE = $("input[name='TYPE']:checked").val();
			switch (TYPE) {
			case "3":
				$('#formId').attr("action", "${__ctx}/login?ADOPID=G210");
				break;
			case "4":
				$('#formId').attr("action", "${__ctx}/login?ADOPID=G200");
				break;
			case "8":
				$('#formId').attr("action", "${__ctx}/login?ADOPID=N814");
				break;
			case "9":
				$('#formId').attr("action", "${__ctx}/login?ADOPID=CK01");
				break;
			}
			$('#formId').submit();
		});

		$("#CMSUBMIT2").click(function(e) {
			$('#formId').attr("action", "${__ctx}/ONLINE/APPLY/online_apply");
		});
	});
	// 立刻、如何申請標籤切換事件
	function tabEvent() {
		// 立刻申請
		$("#now-apply").click(function() {
			$("#CN19-now-apply, #CN19-description, #CN19-button").show();
			$("#CN19-how-apply").hide();
			$('#TYPE3REQ').hide();
			$('#TYPE4REQ').hide();
			$('#TYPE8REQ').hide();
			$('#TYPE9REQ').hide();
			$('#SUBSUBMIT').hide();
		})
		// 如何申請
		$("#how-apply").click(function() {
			$("#CN19-how-apply").show();
			$('#applyTable').DataTable({
				"bPaginate" : false,
				"bLengthChange": false,
				"scrollX": true,
				"sScrollX": "99%",
				"scrollY": "80vh",
		        "bFilter": false,
		        "bDestroy": true,
		        "bSort": false,
		        "info": false,
		        "scrollCollapse": true,
			});
			$("#CN19-now-apply, #CN19-description, #CN19-button").hide();
			$('#TYPE3REQ').hide();
			$('#TYPE4REQ').hide();
			$('#TYPE8REQ').hide();
			$('#TYPE9REQ').hide();
			$('#SUBSUBMIT').hide();
		})
	}

	$(window).bind('resize', function() {
		$('#applyTable').DataTable({
			"bPaginate" : false,
			"bLengthChange": false,
			"scrollX": true,
			"sScrollX": "99%",
			"scrollY": "80vh",
	        "bFilter": false,
	        "bDestroy": true,
	        "bSort": false,
	        "info": false,
	        "scrollCollapse": true,
		});
	});

	function goPage(url) {
		window.open(url, '_blank');
	}
</script>
</head>
<body>
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	<!-- 功能清單及登入資訊 -->
	<div class="content row">
		<!-- 快速選單及主頁內容 -->
		<main class="col-12"> <!-- 主頁內容  -->
		<section id="main-content" class="container">
			<form id="formId" method="post"
				action="${__ctx}/ONLINE/APPLY/online_apply" target="_blank">
					<input type="hidden" id="FROM_NB3" name="FROM_NB3" value="Y">
				<!-- 功能名稱 -->
				<h2>Online application</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 功能內容 -->
				<div class="main-content-block row radius-50">
					<!-- 即時、預約導引標籤 -->
					<nav style="width: 100%;">
						<div class="nav nav-tabs" id="nav-tab" role="tablist">
							<a class="nav-item nav-link active" id="now-apply"
								data-toggle="tab" href="#now-apply" role="tab"
								aria-controls="nav-home" aria-selected="false">apply immediately</a> <a
								class="nav-item nav-link" id="how-apply" data-toggle="tab"
								href="#how-apply" role="tab" aria-controls="nav-profile"
								aria-selected="true">how to apply </a>
							<!-- 								<a class="triple-nav-item nav-item nav-link" id="return-login" -->
							<%-- 									href="#" onclick="fstop.getPage('${pageContext.request.contextPath}'+'/login','', '')">回登入頁 --%>
							<!-- 								</a> -->
						</div>
					</nav>
					<!-- 						<div class="col-12"> -->
					<!-- 							<div class="CN19-1-header"> -->
					<!-- 								<div class="logo"> -->
					<%-- 									<img src="${__ctx}/img/logo-1.svg"> --%>
					<!-- 								</div> -->
					<!-- 								常用網址超連結 -->
					<!-- 								<div class="text-right hyperlink"> -->
					<!-- 									<a href="#" onClick="window.open('https://www.tbb.com.tw');" style="text-decoration: none"> 臺灣企銀首頁 </a> <strong><font color="#e65827">|</font></strong>  -->
					<!-- 									<a href="#" onClick="window.open('https://eatm.tbb.com.tw/');" style="text-decoration: none"> 網路ATM </a><strong><font color="#e65827">|</font></strong> 	 -->
					<!-- 									<a href="#" onClick="window.open('https://www.tbb.com.tw/q3.html?');" style="text-decoration: none"> 意見信箱 </a> -->
					<!-- 								</div> -->
					<!-- 							</div> -->
					<!-- 						</div> -->
					<!-- 如何申請 頁面 -->
					<div class="col-12 tab-content" id="CN19-how-apply"
						style="display: none;">
						<div class="ttb-input-block">
							<div class="CN19-caption1">One, online banking application
								methods and service functions</div>
							<div class="CN19-caption2">
								<span style="FONT-FAMILY: Wingdings;">u</span> <span>How
									to apply</span>
							</div>
							<div class="CN19-caption3">
								<ul>
									<li>Online application: limited to individual households</li>
								</ul>
								<div>
									Bank's chip financial card depositors: <br>  If you have
									not applied for the deposit, you can apply for the <font
										color="#FF6600">inquiry</font> service online. Please use <b>Bank
										Financial Card + Chip Reader</b> to apply online directly. Lu
									Bank, if the chip financial card already has non-contracted
									transfer function, you can apply for the "financial card online
									application / cancel transaction service function" online,
									execute NT non-contract transfer transaction, payment tax
									transaction, online change of communication address / phone And
									user name / check-in password / transaction password line
									unlock and other functions. <br> <br> Credit card
									customers of the Bank (excluding business cards, purchasing
									cards, attached cards and VISA financial cards):<br>
									 Please apply online banking directly online, just enter your
									personal verification information, you can immediately use
									online banking credit card related services. <br>  <br>
								</div>
								<ul>
									<li>Application for cabinet:</li>
								</ul>
								<div>
									Legal depositor:<br>  The person in charge is required to
									bring <b>company registration certificate, identity card,
										deposit seal and deposit plaque</b> to the bank to apply for
									online banking. <br> <br> Personal depositors:<br>
									 Please bring your <b>identity card, deposit seal and
										deposit plaque</b> to the bank to apply for online banking, the
									bank's collection and payment households (ie, depositors), or
									apply to each business unit. <br>  <br>
								</div>
							</div>
							<div class="CN19-caption2">
								<span style="FONT-FAMILY: Wingdings;">u</span> <span>Service
									Features</span>
							</div>
							<div>
								<table id="applyTable"
									class="stripe table-striped ttb-table dtable"
									data-toggle-column="first">
									<thead>
										<tr>
											<!-- Function comparison -->
											<th>Feature comparison</th>
											<!-- Application for the cabinet-->
											<th>Application for the cabinet</th>
											<!-- Wafer Financial Card Online Application -->
											<th>Chip Financial Card Online Application</th>
											<!-- Wafer financial card online application and online application for trading service function -->
											<th>Chip Financial Card Online Application and Online
												Application for Trading Service</th>
											<!-- Credit Card Online Application -->
											<th>Credit Card Online Application</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Credit Card Account Inquiry</td>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>O</td>
										</tr>
										<tr>
											<td>Credit Card Electronic Billing</td>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>O</td>
										</tr>
										<tr>
											<td>Replenish credit card bills</td>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>O</td>
										</tr>
										<tr>
											<td>Credit Card Payment Email Notification</td>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>O</td>
										</tr>
										<tr>
											<td>deposit, loan, foreign exchange accounting inquiry</td>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<td>Fund inquiry</td>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<td>Gold Passbook Query</td>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<td>Gold subscription/return and change</td>
											<td>O(Note 1)</td>
											<td>X</td>
											<td>X</td>
											<td>X</td>
										</tr>
										<tr>
											<td>Transfer, Payment Tax, Deposit</td>
											<td>O</td>
											<td>X</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<td>New Taiwan Dollar Non-conventional Transfer (Note 2)</td>
											<td>O</td>
											<td>X</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<td>Fund subscription and change</td>
											<td>O</td>
											<td>X</td>
											<td>X</td>
											<td>X</td>
										</tr>
										<tr>
											<td>Apply for withholding fees</td>
											<td>O</td>
											<td>X</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<td>Reporting Loss Service</td>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<td>Change communication materials (Note 3)</td>
											<td>O</td>
											<td>X</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<td>Username/checkin/transaction password online unlock
												(Note 4)</td>
											<td>O</td>
											<td>X</td>
											<td>O</td>
											<td>X</td>
										</tr>
										<tr>
											<td>Financial Trial</td>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>O</td>
										</tr>
										<tr>
											<td>Taiwan Dollar Remittance Remittance Notification
												Settings</td>
											<td>O</td>
											<td>O</td>
											<td>O</td>
											<td>X</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div>
								<b>Remarks:</b><br>
								<ol>
									<li>Customers applying for a gold passbook account need to
										apply for the gold passbook online trading function in the
										online banking or the bank, and have to perform functions such
										as gold purchase/return and change.</li>
									<li>If the online transaction function is applied, and the
										chip financial card already has the non-contracted transfer
										function, and the main account of the chip financial card of
										the account is the transfer account, the NTD non-contract
										transfer transaction and payment will be executed. Tax
										transactions.</li>
									<li>The trading mechanism is "electronic signature" or
										"chip financial card", you can perform online change of
										communication address / phone.</li>
									<li>Easy mechanism is "electronic signature" or "chip
										financial card", you can perform online check-in/transaction
										password line unlock.</li>
									<li>O: indicates that the function is available, and X:
										indicates that the function is not available. (original note
										2)</li>
								</ol>
							</div>
							<div class="CN19-caption1">Two, gold passbook online
								application qualification, service function and fee collection</div>
							<div class="CN19-caption2">
								<span style="FONT-FAMILY: Wingdings;">u</span> <span>
									Apply online for a gold passbook</span>
							</div>
							<div class="CN19-caption3">
								<ul>
									<li>Application eligibility</li>
								</ul>
								<div>
									Nationals who are 20 years of age or older. <br> The
									account that has applied for the Bank's online banking and has
									agreed to NTD demand deposits (excluding check deposits) is the
									transfer account. <br> Has applied for the use of the
									Bank's chip financial card + chip reader or electronic
									signature (voucher vehicle). <br>
								</div>
							</div>
							<div class="CN19-caption2">
								<span style="FONT-FAMILY: Wingdings;">u</span> <span>Online
									application for gold passbook online trading function</span>
							</div>
							<div class="CN19-caption3">
								<ul>
									<li>Application eligibility</li>
								</ul>
								<div>
									Nationals who are 20 years of age or older. <br> The
									account that has applied for the Bank's online banking and has
									agreed to NTD demand deposits (excluding check deposits) is the
									transfer account. <br> Has applied for the use of the
									Bank's chip financial card + chip reader or electronic
									signature (voucher vehicle). <br>
								</div>
							</div>
							<div class="CN19-caption3">
								<ul>
									<li>Service Features</li>
								</ul>
								<div>
									Query service: Gold passbook balance, detail, current day,
									historical price inquiry function. <br> Trading services:
									Gold purchase, resale, and payment of regular deduction failure
									fee function. <br> Reservation Service: Appointment to
									buy, sell, cancel, and query functions. <br> Regular fixed
									service: regular fixed purchase, regular fixed amount change,
									regular fixed amount inquiry function. <br> Online
									application: online application for gold passbook account, gold
									passbook network transaction function. <br>
								</div>
							</div>
							<div class="CN19-caption3">
								<ul>
									<li>Fees charged</li>
								</ul>
								<div>
									The online "Application for Gold Passbook Account" and "Regular
									Fixed Investment" will be debited successfully, and a handling
									fee of NT$50 will be charged. The remaining transactions will
									be exempted. <br>
								</div>
							</div>
						</div>
					</div>
					<!-- Apply Now Page -->
					<div class="col-12 tab-content" id="CN19-now-apply">
						<div class="ttb-input-block tab-pane fade show active"
							Role="tabpanel" aria-labelledby="nav-home-tab">
							<!-- Account Block -->
							<br>
							<div class="ttb-input-item row">
								<span class="input-title"> <label> <!-- Online Banking -->
										Online banking
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<label class="radio-block"> Use Taiwan ICBC Chip
											Financial Card + Card Reader <input Type="radio" name="TYPE"
											id="" value="0" checked /> <span Class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<label class="radio-block"> Use Taiwanese Corporate
											Credit Card <input Type="radio" name="TYPE" id="" value="1" />
											<span Class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>
							<!-- Account Block -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label> <!-- Gold Passbook -->
										Gold passbook
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<label class="radio-block"> Apply for a gold passbook
											account (including gold online trading function) <input
											Type="radio" name="TYPE" id="" value="3" /> <span
											Class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<label class="radio-block"> Apply for Gold Network
											Trading Function - already has a Taiwan bank entity gold
											passbook <input Type="radio" name="TYPE" id="" value="4" />
											<span Class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>
							<!-- 帳號區塊-->
							<div class="ttb-input-item row">
								<span class="input-title"> <label> <!-- Account Opening Application -->
										Account opening application
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<label class="radio-block"> Online reservation to open
											a deposit account <input Type="radio" name="TYPE" id=""
											value="5" /> <span Class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<label class="radio-block"> Appointment to open a fund
											account <input Type="radio" name="TYPE" id="" value="6" /> <span
											Class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>
							<!-- Account Block -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label> <!-- Credit Card -->
										credit card
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<label class="radio-block"> Apply online for credit
											card <input Type="radio" name="TYPE" id="" value="7" /> <span
											Class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<label class="radio-block"> Credit Card Application Progress Inquiry <input
											Type="radio" name="TYPE" id="" value="20" /> <span
											Class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<label class="radio-block"> Sign the Credit Card Installment Agreement <input
											Type="radio" name="TYPE" id="" value="8" /> <span
											Class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<label class="radio-block"> Long-term use of revolving
											credit customers online to apply for installment repayments <input
											Type="radio" name="TYPE" id="" value="9" /> <span
											Class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<label class="radio-block">Credit card limit increase
											<input Type="radio" name="TYPE" id="" value="19" /> 
											<span Class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>
							<!-- Account Block -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label> <!-- Digital Deposit Account -->
										Digital deposit account
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<label class="radio-block"> Open a digital deposit
											account online <input Type="radio" name="TYPE" id=""
											value="10" /> <span Class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<label class="radio-block"> Online digital deposit
											account to upload identity documents <input Type="radio"
											name="TYPE" id="" value="11" /> <span Class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<label class="radio-block"> Modify digital deposit
											account information <input Type="radio" name="TYPE" id=""
											value="12" /> <span Class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<label class="radio-block"> Activate Digital Deposit Account IC ATM Card and Change password <input Type="radio" name="TYPE" id="" value="13" />
											<span Class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<label class="radio-block"> Digital Deposit Account
											Supplement Application Chip Financial Card <input
											Type="radio" name="TYPE" id="" value="16" /> <span
											Class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>
							<!-- Account Block -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label> <!-- Guardian Aegis -->
										Guardian shield
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<label class="radio-block"> Apply for Aegis <input
											type="radio" Name="TYPE" id="" value="14" /> <span
											class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>
							<!-- Account Block -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label> <!-- No card withdrawal -->
										No card withdrawal
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<label class="radio-block"> Online logout without card
											withdrawal function <input Type="radio" name="TYPE" id=""
											value="15" /> <span Class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>
							<!-- Account Block -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label> <!-- Scanning code withdrawal -->
										Scan code withdrawal
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<label class="radio-block"> Online cancellation of
											Taiwan Pay scan code withdrawal function <input Type="radio"
											name="TYPE" id="" value="17" /> <span Class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<!-- 掃碼提款 -->
										<spring:message code= "LB.X2384" />
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block">
												<spring:message code= "LB.X2385" />
											<input type="radio" name="TYPE" id="" value="18"  />
											<span class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>
						</div>
						<!-- OK -->
						<spring:message code="LB.Confirm" var="cmSubmit"></spring:message>
						<input type="button" class="ttb-button btn-flat-gray"
							Onclick="fstop.getPage('${pageContext.request.contextPath}'+'/login','', '')"
							Value="back to home page" /> <input type="button"
							name="CMSUBMIT" Id="CMSUBMIT" value="${cmSubmit}"
							Class="ttb-button btn-flat-orange">

					</div>
					<div class="CN19-1-main text-left" id="TYPE3REQ"
						Style="display: none; margin: 10px 200px;">
						<font color="#FF6600" size="5"> <B><span
								Style="FONT-FAMILY: Wingdings;">u</span>This service requires
								the following application requirements:</B><br>
						</font><BR> <font size="4"> 1. A native of 20 years of age. <br>
							Second, An account that has applied for the Bank's online banking
							and has agreed to a new NTD current deposit (excluding check
							deposit) is the transfer account. <br> Third, Has applied
							for the use of the Bank's chip financial card or electronic
							signature (voucher vehicle). <br> If you meet the
							requirements and have completed the Bank's Customer Basic
							Information and Investment Attributes Test Questionnaire (KYC),
							please log in to the Internet Bank to continue. <br>
						</font>
					</div>
					<div class="CN19-1-main text-left" id="TYPE4REQ"
						Style="display: none; margin: 10px 200px;">
						<font color="#FF6600" size="5"> <B><span
								Style="FONT-FAMILY: Wingdings;">u</span>This service requires
								the following application requirements:</B><br>
						</font><BR> <font size="4"> 1. A native of 20 years of age. <br>
							Second, An account that has applied for the Bank's online banking
							and has agreed to a new NTD current deposit (excluding check
							deposit) is the transfer account. <br> Third, A gold
							passbook account has been opened, but the gold passbook network
							transaction has not yet been applied. <br> IV. Has applied
							for the use of the Bank's chip financial card or electronic
							signature (voucher vehicle). <br> Those who meet the above
							requirements and have completed the Bank's Customer Basic
							Information and Investment Attributes Test Questionnaire (KYC),
							Please prepare your own "Taiwan Enterprise Banking Chip Financial
							Card + Card Reader" or electronic signature (voucher carrier). <br>
						</font>
					</div>
					<div class="CN19-1-main text-left" id="TYPE7REQ"
						Style="display: none; margin: 10px 200px;">
						<font color="#FF6600" size="5"> <B><span
								Style="FONT-FAMILY: Wingdings;">u</span>This service requires
								the following application requirements:</B><br>
						</font><BR> <font size="4"> 1. A natural person who is 20
							years of age or older and holds a credit card of the Bank or a
							deposit account of the Bank. <br> Second, have applied to
							use the Bank's online banking. <br> Third, have applied for
							the use of the Bank's chip financial card or electronic signature
							(voucher vehicle). <br>

						</font>
					</div>
					<div class="CN19-1-main text-left" id="TYPE8REQ"
						Style="display: none; margin: 10px 200px;">
						<font color="#FF6600" size="5"> <B><span
								Style="FONT-FAMILY: Wingdings;">u</span>This service requires
								the following application requirements:</B><br>
						</font><BR> <font size="4"> I. Hold the credit card of the
							Bank. <br> Second, Has applied to use the Bank's online
							banking. <br>
						</font>
					</div>
					<div class="CN19-1-main text-left" id="TYPE9REQ"
						Style="display: none; margin: 10px 200px;">
						<font color="#FF6600" size="5"> <B><span
								Style="FONT-FAMILY: Wingdings;">u</span>This service requires
								the following application requirements:</B><br>
						</font><BR> <font size="4"> I. Hold the credit card of the
							Bank. <br> Second, Has applied to use the Bank's online
							banking. <br> Third, Continuous use of revolving credit for
							more than one year and no late payment or bad credit record at
							the Financial Joint Credit Information Center of the Corporation.
							<br>
						</font>
					</div>
					<div id="SUBSUBMIT" style="display: none; margin: auto">
						<input type="button" name="CMCANCEL" id="CMCANCEL" value="Cancel"
							Class="ttb-button btn-flat-gray"> <input type="button"
							Name="CMSUBMIT2" id="CMSUBMIT2" value="Login to online banking"
							Class="ttb-button btn-flat-orange">
					</div>
				</div>
			</form>
			<!-- Description -->
			<div id="CN19-description" class="text-left">
				<spring:message code="LB.Description_of_page" />
				:
				<ol class="list-decimal text-left">
					<li>Internet Banking: Applicants who use the chip financial
						card are limited to individual depositors. For credit card
						applicants, please use the general credit card application
						(excluding business cards, purchasing cards, attached cards and
						VISA financial cards). Query <a Href="#"
						Onclick="goPage('${pageContext.request.contextPath}'+'/ONLINE/APPLY/online_apply_menu_sub')">Online
							banking application and service features</a>
					</li>
					<li>Gold Passbook: You have applied for <font color=red>National
							Bank of the Bank, 20 years old</font>, and <font Color=red>Consulted
							NTD Current Deposit Account as Transferred Account</font> (excluding
						check deposit) and completed the Bank's Customer Basic Information
						and Investment Attributes Test Questionnaire (KYC) with Taiwanese
						Enterprise Banking Chip Finance Cards or electronic signatures
						(vouchers), each branch is restricted to B, <a Href="#"
						Onclick="goPage('${pageContext.request.contextPath}'+'/ONLINE/APPLY/online_apply_menu_sub','', '')">Gold
							passbook online eligibility, service features and fees charged</a>.
					</li>
					<li>If you are a depositor of the Bank but do not meet the
						online application requirements, please bring your ID card,
						deposit seal and passbook to the branch.</li>
					<li>Online application for credit card: Applicants must have
						applied for the Bank's online banking, at least 20 years old and
						hold a credit card of the Bank or a bank account holder, and have
						a Taiwanese ICBC chip financial card or electronic signature.
						(Voucher vehicle).</li>
					<li>Sign the Credit Card Installment Agreement:
						The applicant must have applied for the Bank's online banking and
						holds the Bank's credit card.</li>
					<li>Long-term use of revolving credit customers online
						application for installment repayment: Applicants must have
						applied for the Bank's online banking and hold the Bank's credit
						card.</li>
				</ol>
			</div>
	</div>
	</section>
	</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>

</html>