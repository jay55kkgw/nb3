<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<!-- 元件驗證身分JS -->
	<script type="text/javascript" src="${__ctx}/component/util/checkIdentity.js"></script>
	
	<script type="text/javascript">
	$(document).ready(function() {
		$("#CMSUBMIT").click(function(e){
			
			$("#formId").validationEngine({
				binded : false,
				promptPosition : "inline"
			});
			
			e = e || window.event;
		
			if(!$('#formId').validationEngine('validate')){
	        	e.preventDefault();
				}
			else{
					$("#formId").validationEngine('detach');
					$("#formId").attr("action","${__ctx}/PERSONAL/SERVING/vouchers_print_list");
					processQuery(); 
				}
		});
	});
	// 交易機制選項
	function processQuery(){
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
		console.log("fgtxway: " + fgtxway);
		switch(fgtxway) {			
			case '2':
// 				alert("晶片金融卡");
				// 晶片金融卡
				urihost = "${__ctx}";
				console.log("urihost: " + urihost);
				useCardReader();
		    	break;

	        case '7'://IDGATE認證		 
	            idgatesubmit= $("#formId");		 
	            showIdgateBlock();		 
	            break;
			default:
				//alert("nothing...");
				errorBlock(
						null, 
						null,
						["nothing..."], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
		}		
	}
	
	function useCardReader(){
		console.log("CardReader...");
		listReaders();
	}
	
	function findOKReaderFinish(okReaderName){
		//ASSIGN到全域變數
		OKReaderName = okReaderName;
		getCardBankID();
	}
	</script>
	
</head>
<body>
	
	
	<!-- 	晶片金融卡 -->
	<%@ include file="../component/trading_component.jsp"%>
    <!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %> 

	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 各類所得扣繳憑單列印     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0141" /></li>
		</ol>
	</nav>

		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code="LB.X0141" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post">
			    <input type="hidden" name="CUSIDN" value="${CUSIDN}">
			    <input type="hidden" id="ADOPID" name="ADOPID" value="N106">
			    <!-- 			晶片金融卡 -->
				<input type="hidden" id="ISSUER" name="ISSUER" value="">
				<input type="hidden" id="ACNNO" name="ACNNO" value="">
				<input type="hidden" id="TRMID" name="TRMID" value="">
				<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
				<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
				<input type="hidden" id="TAC" name="TAC" value="">
				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
							    
				<div class="main-content-block row">
					<div class="col-12">
						<div class="ttb-input-block">
							<!-- 各類所得扣繳憑單列印 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.Transaction_security_mechanism" /></h4>
								</label>
								</span> 
								<div class="ttb-input">
									<span class="input-block">
											<label class="radio-block">
								           	<input type="radio" name="FGTXWAY" id="FGTXWAY" value="2" checked>
								           	<spring:message code="LB.Financial_debit_card" />
											<span class="ttb-radio"></span>
										</label>							           	
									</span>
								</div>
								<div class="ttb-input" name="idgate_group" style="display:none" onclick="hideCapCode('Y')">		 
                                      <label class="radio-block">裝置推播認證(請確認您的行動裝置網路連線是否正常，及推播功能是否已開啟)
                                       <input type="radio" id="IDGATE" 	name="FGTXWAY" value="7"> 
                                       <span class="ttb-radio"></span>
                                      </label>		 
                                </div>
							</div>
						</div>
						<!-- 確定 -->
						<input type="button" name="CMSUBMIT" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm"/>"/>
					</div>
				</div>
			</form>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>