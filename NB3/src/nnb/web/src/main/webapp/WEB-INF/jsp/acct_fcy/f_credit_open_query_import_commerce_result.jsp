<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	    <script type="text/javascript">
        $(document).ready(function () {
            initFootable(); // 將.table變更為footable 
            init();
        });

        function init() {
            //列印
        	$("#printbtn").click(function(){
				var params = {
					"jspTemplateName":"f_letter_of_credit_opening_inquiry_commerce_detail_print",
					"jspTitle":'<spring:message code= "LB.X0004" />',
					"CMQTIME":"${base_result.data.CMQTIME}",
					"RLCNO":"${base_result.data.RLCNO}",
					"ROPENDAT":"${base_result.data.ROPENDAT}",
					"RAMDNO":"${base_result.data.RAMDNO}",
					"RLCCCY":"${base_result.data.RLCCCY}",
					"RORIGAMT":"${base_result.data.RORIGAMT}",
					"RALLOW":"${base_result.data.RALLOW}",
					"RMARGOS":"${base_result.data.RMARGOS}",
					"RMAXDRAW":"${base_result.data.RMAXDRAW}",
					"RLCOS":"${base_result.data.RLCOS}",
					"RADVBK":"${base_result.data.RADVBK}",
					"RADVNM":"${base_result.data.RADVNM}",
					"RBENNAME":"${base_result.data.RBENNAME}",
					"RTXTENOR":"${base_result.data.RTXTENOR}",
					"RTENOR":"${base_result.data.RTENOR}",
					"REXPDATE":"${base_result.data.REXPDATE}",
					"RSHPDATE":"${base_result.data.RSHPDATE}",
					"RMARK":"${base_result.data.RMARK}"
				};
				openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
    		//上一頁按鈕
    		$("#CMBACK").click(function() {
    			var action = '${__ctx}/FCY/ACCT/f_credit_open_query_date_result';
    			$('#back').val("Y");
    			$("#formId").attr("action", action);
    			initBlockUI();
    			$("#formId").submit();
    		});
        }
       
	 	function finishAjaxDownload(){
			$("#actionBar").val("");
			unBlockUI(initBlockId);
		}
    </script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 帳戶查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Account_Inquiry" /></li>
    <!-- 開狀查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0098" /></li>
    <!-- 帳戶明細查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Demand_Deposit_Detail" /></li>
    <!-- 商業信用狀明細     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0004" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!-- 商業信用狀明細(未輸入) -->
					<!-- <spring:message code="LB.NTD_Demand_Deposit_Detail" /> -->
					<spring:message code="LB.X0004" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                
                <form id="formId" method="post">
                	<input type="hidden" id="back" name="back" value="">
                    
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
                            
                            <!-- 表格區塊 -->
                            <div class="ttb-input-block">
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Inquiry_time" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${base_result.data.CMQTIME }
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.L/C_no" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${base_result.data.RLCNO }
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0089" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${base_result.data.ROPENDAT }
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.X0021" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${base_result.data.RAMDNO } <spring:message code="LB.Time_s" />
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0094" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${base_result.data.RLCCCY } ${base_result.data.RORIGAMT }
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.X0011" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${base_result.data.RALLOW } %
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.X0012" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${base_result.data.RMARGOS }
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.X0013" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${base_result.data.RLCCCY } ${base_result.data.RMAXDRAW }
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.X0014" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${base_result.data.RLCCCY } ${base_result.data.RLCOS }
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0118" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${base_result.data.RADVBK } ${base_result.data.RADVNM }
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.X0015" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${base_result.data.RBENNAME }
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.X0016" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<spring:message code="LB.X0018" />${base_result.data.RTXTENOR }<spring:message code="LB.W1121" /><br><spring:message code="LB.X0017" />${base_result.data.RTENOR }<spring:message code="LB.W1121" />
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.X0020" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${base_result.data.REXPDATE }
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.X0019" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${base_result.data.RSHPDATE }
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Note" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${base_result.data.RMARK }
										</div>
									</span>
								</div>
							</div>
	                        <!--button 區域 -->
	                        
	                            <!--回上頁 -->
	                            <spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>
	                            <input type="button" name="CMBACK" id="CMBACK" value="${cmback}" class="ttb-button btn-flat-gray">
	                            <!-- 列印  -->
	                            <spring:message code="LB.Print" var="printbtn"></spring:message>
	                            <input type="button" id="printbtn" value="${printbtn}" class="ttb-button btn-flat-orange" />
	                        
                        </div>  
                    </div>
                </form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>