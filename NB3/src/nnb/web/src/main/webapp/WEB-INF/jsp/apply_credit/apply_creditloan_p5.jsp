<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
   <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">    
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">


<script type="text/javascript">
$(document).ready(function() {
	// 將.table變更為footable
	initFootable();
	
	//表單驗證
	$("#formId").validationEngine({
		binded : false,
		promptPosition : "inline"
	});
	
	$("#CMSUBMIT").click(function(e){
		console.log("submit~~");
		if($('input:radio[name=agreeCheck]:checked').val() == "Y"){
			$("#validate").prop("checked",true)
		}
		if (!$('#formId').validationEngine('validate')) {
			e.preventDefault();
			$(".validateformError").css("text-align","center");
		} else {
			initBlockUI();
			$("#formId").validationEngine('detach');
			$("form").submit();
		}
	})
	$("#PREVIOUS").click(function(e) {
		$("#formId").validationEngine('detach');
		action = '${__ctx}/APPLY/CREDIT/apply_creditloan_p4'
		$("form").attr("action", action);
		$("form").submit();
	});
});

function buttonEvent(){
	if($('input:radio[name=agreeCheck]:checked').val()=='N'){
		$('#CMSUBMIT').attr("disabled",true)
		$('#CMSUBMIT').removeClass('btn-flat-orange');
		$('#CMSUBMIT').addClass('btn-flat-gray')
		$('#PREVIOUS').removeClass('btn-flat-gray');
		$('#PREVIOUS').addClass('btn-flat-orange')
	}
	if($('input:radio[name=agreeCheck]:checked').val()=='Y'){
		$('#CMSUBMIT').removeAttr("disabled")
		$('#CMSUBMIT').addClass('btn-flat-orange');
		$('#CMSUBMIT').removeClass('btn-flat-gray');
		$('#PREVIOUS').removeClass('btn-flat-orange');
		$('#PREVIOUS').addClass('btn-flat-gray')
	}
}
</script>
</head>
	<body>
		<!-- header     -->
		<header>
			<%@ include file="../index/header.jsp"%>
		</header>
	
		<!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<!--申請小額信貸 -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0543" /></a></li>
		</ol>
	</nav>
		<!-- menu、登出窗格 -->
		<div class="content row">
			<%@ include file="../index/menu.jsp"%>
			<!-- 	快速選單內容 -->
			<!-- content row END -->
			<main class="col-12">
				<section id="main-content" class="container">
					<!-- 申請人聲明事項 -->
					<h2><spring:message code= "LB.D0551" /></h2>
					<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
					<form id="formId" method="post" action="${__ctx}/APPLY/CREDIT/apply_creditloan_p6">
					<input type="hidden" id="LOANTYPE" name ="LOANTYPE" value="${LOANTYPE}">
						<div class="main-content-block row">
							<div class="col-12">
							 <div class="ttb-message">
			             <!-- 信用查詢與資料之蒐集處理與利用 -->
                            <p><spring:message code= "LB.X1002" /></p>
                              </div>
								<br>
								<div class="text-left">
								<!-- 茲同意 貴行、財團法人金融聯合徵信中心、財團法人中小企業信用保證基金、財團法人農業信用保證基金、財金資訊股份有限公司、台灣票據交換所及其他經金融監督管理委員會指定或與  貴行因業務需要訂有契約之機構（以下簡稱前揭機構），於其營業登記項目或章程所定業務之需要等之特定目的範圍內，得依法令規定蒐集、處理及利用(含國際傳輸)本人之個人資料，且亦授權 貴行得向前揭機構蒐集本人資料，特此聲明。 -->
											<spring:message code= "LB.X1183" />
								</div>
								<br>
								<div class="text-right">
								<!-- 版號：10405 -->
								<spring:message code= "LB.X1184" />
								</div>
								<br>
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
											<!-- 本人已詳閱，同意「信用查詢與資料之蒐集處理與利用」 -->
												<spring:message code= "LB.D0552" />
												<input type="radio" name="agreeCheck" id="agreeCheck" value="Y" onclick="buttonEvent()"/>
												<span class="ttb-radio"></span>
											</label>
										</div>
								
										<div class="ttb-input">
											<label class="radio-block">
											<!--本人已詳閱，不同意「信用查詢與資料之蒐集處理與利用」並返回-->
												<spring:message code= "LB.D0553" /> 						
												<input type="radio" name="agreeCheck" id="agreeCheck" value="N" onclick="buttonEvent()" />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<div style="text-align: center">
												<div class="radio-block">
													<input type="radio" id="validate" class="validate[required]"/>
												</div>
											</div>
										</div>
									</span>
								<!-- 上一步-->
								<input class="ttb-button btn-flat-gray" type="button" value=" <spring:message code= "LB.X0318" />" id="PREVIOUS">
								<!-- 下一步-->
			                   	<input class="ttb-button btn-flat-orange" type="button" value="<spring:message code= "LB.X0080" />" id="CMSUBMIT">
							</div>
						</div>
					</form>
				</section>
			</main>
			<!-- 		main-content END -->
			<!-- 	content row END -->
		</div>
		<%@ include file="../index/footer.jsp"%>
	</body>
</html>