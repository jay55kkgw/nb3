<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>
<script type="text/javascript" src="${__ctx}/js/jquery.maskedinput.js"></script>
<!-- DIALOG會用到 -->
<script type="text/javascript" src="${__ctx}/js/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery-ui.css">

<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	// HTML載入完成後開始遮罩
// 	setTimeout("initBlockUI()", 10);
	initBlockUI();
	// 開始查詢資料並完成畫面
	setTimeout("init()", 20);
	// 解遮罩
	setTimeout("unBlockUI(initBlockId)", 500);
});
	
function init(){
	$("#formId").validationEngine({
		binded: false,
		promptPosition: "inline"
	});
	$("#hideblock").hide();
	
// 	var cusidn = '${result_data.data.CUSIDN}';
// 	if(cusidn != null && cusidn != ''){
// 		$("#CUSIDN").val(cusidn);
// 	}
	
	birthday();
	initKapImg();
	newKapImg();
	
	$("#CMSUBMIT").click(function(e){
		$("#CUSIDN").val($("#CUSIDN").val().toUpperCase());
		var cardnum = $("#CARDNUM1").val() + $("#CARDNUM2").val() + $("#CARDNUM3").val() + $("#CARDNUM4").val();
		$("#CARDNUMCHK").val(cardnum);
		
		$("#hideblock").show();
		e = e || window.event;
		if(!$('#formId').validationEngine('validate')){
    		e.preventDefault();
    	}
		else{
			okFunction();
		}
	});
	
	$("#CMBACK").click(function(e){
		$("#formId").validationEngine('detach');
		
		// 如果是從NB3進入此頁面，按取消會回到線上申請清單
		if($("#FROM_NB3").val() == 'Y') {
			top.location = '${pageContext.request.contextPath}'+'/ONLINE/APPLY/online_apply_menu';	
		} else { // 如果是從官網進入此頁面，按取消會開啟新分頁到官方網站
			window.open("https://www.tbb.com.tw/web/guest/-2", "_self");
		}
	});
}

function birthday(){
	var Today = new Date();
	var y = Today.getFullYear();
	var m = (Today.getMonth()+1<10 ? '0' : '')+(Today.getMonth()+1);
	var d = (Today.getDate()<10 ? '0' : '')+Today.getDate();
	y = y - 1911;
	var min_y = y-20;
	var max_y = y-100;
	
	for(var i=min_y;i>=max_y;i--){
		var YYtext = i;
		if(i.toString().length < 2){
			YYtext = "00"+YYtext;
		}
		else if(i.toString().length < 3){
			YYtext = "0"+YYtext;
		}
		//民國年
// 		$("#YY").append( $("<option></option>").attr("value", YYtext).text("<spring:message code='LB.D0583' />" + i + "<spring:message code='LB.Year' />"));
		//西元年
		if('${transfer}' == 'en'){
			$("#YY").append( $("<option></option>").attr("value", YYtext).text( i + 1911));
		}
		else{
			$("#YY").append( $("<option></option>").attr("value", YYtext).text((i + 1911) + "年"));
		}
	}
	orderby();
// 	$("#YY").val(y);
	for(var i=1;i<=12;i++){
		var j = i;
		if(j<10){
			j = "0"+j;
		}
		//月
		if('${transfer}' == 'en'){
			switch (j) {
			case '01':
			　$("#MM").append( $("<option></option>").attr("value", j).text("January"));
			　break;
			case '02':
			　$("#MM").append( $("<option></option>").attr("value", j).text("February"));
			　break;
			case '03':
			　$("#MM").append( $("<option></option>").attr("value", j).text("March"));
			　break;
			case '04':
			　$("#MM").append( $("<option></option>").attr("value", j).text("April"));
			　break;
			case '05':
			　$("#MM").append( $("<option></option>").attr("value", j).text("May"));
			　break;
			case '06':
			　$("#MM").append( $("<option></option>").attr("value", j).text("June"));
			　break;
			case '07':
			　$("#MM").append( $("<option></option>").attr("value", j).text("July"));
			　break;
			case '08':
			　$("#MM").append( $("<option></option>").attr("value", j).text("August"));
			　break;
			case '09':
			　$("#MM").append( $("<option></option>").attr("value", j).text("September"));
			　break;
			case 10:
			　$("#MM").append( $("<option></option>").attr("value", j).text("October"));
			　break;
			case 11:
			　$("#MM").append( $("<option></option>").attr("value", j).text("November"));
			　break;
			case 12:
			　$("#MM").append( $("<option></option>").attr("value", j).text("December"));
			　break;
			default:
			　x="<spring:message code='LB.D1615' />";
			}
		}
		else{
			$("#MM").append( $("<option></option>").attr("value", j).text(j + "<spring:message code='LB.Month' />"));
		}
	}
// 	$("#MM").val(m);
	for(var i=1;i<=31;i++){
		var j = i;
		if(j<10){
			j = "0"+j;
		}
		//日
		if('${transfer}' == 'en'){
			$("#DD").append( $("<option></option>").attr("value", j).text(j));
		}
		else{
			$("#DD").append( $("<option></option>").attr("value", j).text(j + "<spring:message code='LB.D0586' />"));
		}
	}
// 	$("#DD").val(d);
}

// 刷新驗證碼
function refreshCapCode() {
	console.log("refreshCapCode...");

	// 驗證碼
	$('input[name="capCode"]').val('');
	
	// 大小版驗證碼用同一個
	$('img[name="kaptchaImage"]').hide().attr(
			'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

	// 登入失敗解遮罩
	unBlockUI(initBlockId);
}

// 刷新輸入欄位
function changeCode() {
	console.log("changeCode...");
	
	// 清空輸入欄位
	$('input[name="capCode"]').val('');
	
	// 刷新驗證碼
	refreshCapCode();
}

// 初始化驗證碼
function initKapImg() {
	// 大小版驗證碼用同一個
	$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
}

// 生成驗證碼
function newKapImg() {
	// 大小版驗證碼用同一個
	$('img[name="kaptchaImage"]').click(function() {
		refreshCapCode();
	});
}

// function changeCode(){
// 	$("#capCode").val("");
// 	$("#kaptchaImage").hide().attr("src","${__ctx}/CAPCODE/captcha_image_trans?" + Math.floor(Math.random() * 100)).fadeIn();
// }
function yyyymmdd(date){
	var yyyy = date.getFullYear();
	var mm = date.getMonth() + 1;//getMonth() is zero-based
	var dd  = date.getDate();
	
	return String(10000 * yyyy + 100 * mm + dd);// Leading zeros for mm and dd
}
//控制信用卡卡號當輸入完一個欄位，游標移動到下一個欄位
function setBlur(obj,target2){
        
    var target=document.getElementById(target2);    
    if(obj.value.length == obj.getAttribute('maxlength')){
        target.focus();
    }
    
    return;
}
var urihost = "${__ctx}";

function okFunction(){
	$("#formId").validationEngine('detach');
	$("#BIRTH").val($("#YY").val()+""+$("#MM").val()+""+$("#DD").val());
	$("#CPRIMBIRTHDAYshow").val($("#YY").find(':selected').text()+" "+$("#MM").find(':selected').text()+" "+$("#DD").find(':selected').text());
//		$("#CUSIDN").val($("#CUSIDN").val().toUpperCase());
	var CUSIDN = $("#CUSIDN").val();
	CUSIDN = CUSIDN.toUpperCase();
	$("#UID").val(CUSIDN);
	
	var card = $("#CARDNUMCHK").val();
	$("#CARDNUM").val(card);
	
	//驗證碼驗證
	var capURI = "${__ctx}/CAPCODE/captcha_valided_trans";
	var capData = $("#formId").serializeArray();
	var capResult = fstop.getServerDataEx(capURI,capData,false);
	
	//驗證結果
	if(capResult.result == true){
		//信用卡
		if(!CheckSelect("EXPIREDYEAR","<spring:message code="LB.X1898" />","#")){
			return false;
		}
		if(!CheckSelect("EXPIREDDATEMONTHS","<spring:message code="LB.X1899" />","#")){
			return false;
		}
		if($("#BIRTH").val() == ""){
			errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert029' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			return false;
		}
		initBlockUI();
		$("#formId").submit();
	}
	else{
		//失敗重新產生驗證碼
		errorBlock(null, null, ["<spring:message code='LB.X1082' />"],
			'<spring:message code= "LB.Quit" />', null);
		changeCode();
	}
}

function orderby(){
	$('#YY>option').sort(function(a,b){
        //按option中的值排序
        var aText = $(a).val()*1;
        var bText = $(b).val()*1;
        if(aText > bText) return -1;
        if(aText < bText) return 1;
        return 0;
    }).appendTo('#YY');
    $('#YY>option').eq(0).attr("selected","selected");
}


jQuery(function($) {//,{autoclear: false}
	$("#CARDNUM").mask("9999/9999/9999/9999",{autoclear: false});
	$("#EXPIREDYM").mask("99/99",{autoclear: false});
});

function checkitemAll(){
	if($("#CheckBox1").prop("checked") == true && $("#CheckBox2").prop("checked") == true){
		$('#CMSUBMIT').removeAttr("disabled");
		$('#CMSUBMIT').removeClass('btn-flat-gray');
		$('#CMSUBMIT').addClass('btn-flat-orange');
	}
	else{
		$('#CMSUBMIT').attr("disabled",true);
		$('#CMSUBMIT').removeClass('btn-flat-orange');
		$('#CMSUBMIT').addClass('btn-flat-gray');
	}
}

</script>
</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Credit_Card_Limit_Increase" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2><spring:message code="LB.Credit_Card_Limit_Increase" /></h2>
				<div id="step-bar">
                    <ul>
                        <li class="active"><spring:message code="LB.D0851" /></li><!-- 身份驗證 -->
                        <li class=""><spring:message code="LB.SMS_Verification" /></li>
                        <li class=""><spring:message code="LB.X1967" /></li><!-- 申請資料 -->
                        <li class=""><spring:message code="LB.Confirm_data" /></li><!-- 確認資料 -->
                        <li class=""><spring:message code="LB.X1968" /></li><!-- 完成申請 -->
                    </ul>
                </div>
				<form method="post" id="formId" action="${__ctx}/ONLINE/APPLY/creditcard_limit_increase_otp">
					<input type="hidden" name="ADOPID" value="N822"/>
					<input type="hidden" id="UID" name="UID"/>
					<input type="hidden" id="CARDNUM" name="CARDNUM"/>
					<input type="hidden" id="TOKEN" name="TOKEN" value="${sessionScope.transfer_confirm_token}"/>
					<!--交易機制所需欄位-->
					<input type="hidden" id="jsondc" name="jsondc" value=""/>
					<input type="hidden" id="ISSUER" name="ISSUER"/>
					<input type="hidden" id="ACNNO" name="ACNNO"/>
					<input type="hidden" id="TRMID" name="TRMID"/>
					<input type="hidden" id="iSeqNo" name="iSeqNo"/>
					<input type="hidden" id="ICSEQ" name="ICSEQ"/>
					<input type="hidden" id="TAC" name="TAC"/>
					<input type="hidden" id="pkcs7Sign" name="pkcs7Sign"/>
					<input type="hidden" id="FROM_NB3" name="FROM_NB3" value="${FROM_NB3}"/>
					<!-- 表單顯示區  -->
					<div class="main-content-block pl-0 pr-0 row">
						<div class="col-12">
							<div class="ttb-input-block">
								<div class="ttb-message">
									<!-- 身份驗證 -->
									<p><spring:message code="LB.D0851" /></p>
								</div>
							
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
<!-- 											身分證字號 -->
											<spring:message code="LB.D0581" />
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" id="CUSIDN" name="CUSIDN" class="text-input validate[required,funcCall[validate_checkSYS_IDNO[CUSIDN]]]" maxlength="10" size="10" placeholder='<spring:message code="LB.D0025" />'/>
										</div>
									</span>
								</div>
							
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
<!-- 											出生日期 -->
											<spring:message code="LB.D0582" />
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<select id="YY" name="YY" class="custom-select select-input input-width-100">
												<option value="#" style="">---</option>
											</select>
											<select id="MM" name="MM" class="custom-select select-input input-width-60">
												<option value="#">---</option>
											</select>
											<select id="DD" name="DD" class="custom-select select-input input-width-60">
												<option value="#">---</option>
											</select>
										<input type="text" id="checkDD" class="validate[funcCallRequired[validate_CheckSelects['<spring:message code="LB.D0582" />',YY,MM,DD,#]]]" style="visibility:hidden"/>
										</div>
										<input type="hidden" id="BIRTH" name="BIRTH"/>
										<input type="hidden" id="CPRIMBIRTHDAYshow" name="CPRIMBIRTHDAYshow"/>
									</span>
								</div>
							
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.W0639" /><!-- 信用卡號 -->
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<div class="ttb-input">
												<input type="text" id="CARDNUM1" name="CARDNUM1" class="text-input card-input" maxlength="4" size="4" onKeyUp="setBlur(this,'CARDNUM2')" value=""/>
												<input type="text" id="CARDNUM2" name="CARDNUM2" class="text-input card-input" maxlength="4" size="4" onKeyUp="setBlur(this,'CARDNUM3')" value=""/>
												<input type="text" id="CARDNUM3" name="CARDNUM3" class="text-input card-input" maxlength="4" size="4" onKeyUp="setBlur(this,'CARDNUM4')" value=""/>
												<input type="text" id="CARDNUM4" name="CARDNUM4" class="text-input card-input" maxlength="4" size="4" value=""/>
												<input type="text" id="CARDNUMCHK" class="validate[required,funcCall[validate_CheckNumWithDigit[<spring:message code='LB.W0639' />,CARDNUMCHK,16]]]" style="visibility: hidden"/>
											</div>
										</div>
									</span>
								</div>
							
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
<!-- 												驗證碼 -->
												<spring:message code="LB.Captcha" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input id="capCode" type="text" class="ttb-input text-input input-width-125"
												name="capCode" placeholder="<spring:message code='LB.X1702' />" maxlength="6" autocomplete="off">
											<img name="kaptchaImage" src="" class="verification-img"/>
											<!-- 重新產生驗證碼 -->
											<input type="button" name="reshow" class="ttb-sm-btn btn-flat-gray" onclick="refreshCapCode()" value="<spring:message code='LB.Regeneration' />" /><!-- 重新產生驗證碼 -->
											<span class="input-remarks"><spring:message code="LB.X1972" /></span><!-- 請注意：英文不分大小寫，限半形字元 -->
										</div>
									</span>
								</div>
								
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
											</h4>
										</label>
									</span> 
									<span class="input-block">
		                                <div class="ttb-input">
		                                    <label class="check-block"><spring:message code="LB.X0203" />&nbsp;<a href="${__ctx}/ONLINE/APPLY/creditcard_limit_article?article=1" target="_blank" style="color:blue">個人資料運用告知同意聲明書</a>
		                                        <input type="checkbox" name="CheckBox1" id="CheckBox1" onclick="checkitemAll()" class="validate[funcCallRequired[validate_chkClickboxKind[<spring:message code= "LB.D1064" />,1,CheckBox1]]]">
		                                        <span class="ttb-check"></span>
		                                    </label>
		                                </div>
		                                <div class="ttb-input">
		                                    <label class="check-block"><spring:message code="LB.X0203" />&nbsp;<a href="${__ctx}/ONLINE/APPLY/creditcard_limit_article?article=2" target="_blank" style="color:blue">個人網路銀行暨行動銀行服務條款</a>
		                                        <input type="checkbox" name="CheckBox2" id="CheckBox2" onclick="checkitemAll()" class="validate[funcCallRequired[validate_chkClickboxKind[<spring:message code= "LB.D1064" />,1,CheckBox2]]]">
		                                        <span class="ttb-check"></span>
		                                    </label>
		                                </div>
		                            </span>
								</div>
							</div>
							<!-- 取消 -->
							<input type="button" name="CMBACK" id="CMBACK" value="<spring:message code="LB.Cancel" />" class="ttb-button btn-flat-gray"/>
							<input type="button" id="CMSUBMIT" value="<spring:message code="LB.X0080" />" class="ttb-button btn-flat-gray" disabled/><!-- 下一步 -->
						</div>
					</div>
					<ol class="description-list list-decimal">
						<p>【<spring:message code="LB.D1590" />】 </p>
						<li>
							<spring:message code="LB.D1591" />
						</li>
						<li>
							<spring:message code="LB.D1592" />
							<ul style="list-style: disc">
								<li>
									<spring:message code="LB.D1592_1" />
								</li>
								<li>
									<spring:message code="LB.D1592_2" />
								</li>
							</ul>
						</li>
						<li>
							<spring:message code="LB.D1593" />
						</li>
						<li>
							<spring:message code="LB.D1592" />
						</li>
					</ol>
				</form>
			</section>
		</main>
	</div>
	
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>