<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/checkutil.js"></script>
	
	<script type="text/javascript">
	$(document).ready(function(){
		//alert('${apply_result_data.data}');
		$("#printbtn").click(function(){
			var i18n = new Object();
			//<!-- 申請電子帳單 -->
			i18n['jspTitle']='<spring:message code= "LB.D0279" />';
			var params = {
				"jspTemplateName":"elec_bill_apply_result_print",
				"jspTitle":i18n['jspTitle'],
				"CMQTIME":"${apply_result_data.data.CMQTIME}",
				"TYPE1":"${apply_result_data.data.TYPE1}",
				"TYPE3":"${apply_result_data.data.TYPE3}",
				"TYPE2":"${apply_result_data.data.TYPE2}",
				"EMAIL":"${sessionScope.dpmyemail }"
			}
			openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
		
		});
	});
		
		
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 電子對帳單     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1396" /></li>
    <!-- 電子對帳單申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0270" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<spring:message code="LB.D0279" /><!-- 申請電子帳單 -->
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" action="" method="post">
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
							<h4 style="margin-top:10px;color: red;font-weight:bold;">
								<spring:message code="LB.D0181" /><!-- 申請成功 -->
							</h4>
							<div class="ttb-input-block">
								<!--交易時間區塊 -->
								<div class="ttb-input-item row">
									<!--交易時間  -->
									<span class="input-title">
										<label>
											<spring:message code="LB.System_time" /><!-- 系統時間 -->
										</label>
									</span>
									<span class="input-block">
										${apply_result_data.data.CMQTIME}
									</span>
								</div>
								<!-- 項目區塊 -->
								<div class="ttb-input-item row">
									<!--項目  -->
									<span class="input-title">
										<label>
											<spring:message code="LB.D0271" /><!-- 項目 -->
										</label>
									</span>
									<span class="input-block">
										<c:if test="${apply_result_data.data.TYPE1 =='1'}">
											<p><spring:message code="LB.D0280" /><!-- 電子交易對帳單 --> </p>
										</c:if>
										<c:if test="${apply_result_data.data.TYPE3 =='3'}">
											<p><spring:message code="LB.D0274" /><!-- 基金電子對帳單 --></p>
										</c:if>
										<c:if test="${apply_result_data.data.TYPE2 =='2'}">
											<p><spring:message code="LB.D0275" /><!-- 信用卡電子帳單 --></p>
										</c:if>
									</span>
								</div>
								<!-- 電子帳單傳送位址區塊 -->
								<div class="ttb-input-item row">
									<!--電子帳單傳送位址  -->
									<span class="input-title">
										<label>
											<spring:message code="LB.D0292" /><!-- 電子帳單傳送位址 --><br>
											(<spring:message code="LB.D0293" /><!-- 我的Email -->)
										</label>
									</span>
									<span class="input-block">
										<p>${sessionScope.dpmyemail }</p>
									</span>
								</div>
								<div class="ttb-input-item row">
									<!--電子帳單傳送位址  -->
									<span class="input-title">
										<label>
											<spring:message code="LB.D0294" /><!-- 電子帳單密碼-->
										</label>
									</span>
									<span class="input-block">
										<p><spring:message code="LB.D0204" /><!-- 身份證字號 --><a style="color: #007bff" href="${__ctx}/ELECTRONIC/CHECKSHEET/elec_pw_alter" target="_self"><spring:message code="LB.X2370" /></a></p>
									</span>
								</div>
							<!-- 列印 button-->
							
						</div>
						<div>
								<!--列印 -->
							<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
						</div>
					</div>
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
      
</body>

</html>