<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript"
	src="${__ctx}/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css"
	href="${__ctx}/css/validationEngine.jquery.css">
<link rel="stylesheet" type="text/css"
	href="${__ctx}/css/jquery.datetimepicker.css">

<script type="text/javascript">
	$(document).ready(function() {
		init();
		var main = document.getElementById("formId");
   	  	main.ReadFlag.value="N";
   	  	//main.CMSUBMIT.disabled = true;  
	});
	function checkReadFlag()
	{
		  var main = document.getElementById("formId");

		  if ($("#ReadFlag").prop('checked'))    		
	      	{
	       		$("#CMSUBMIT").prop('disabled',false);
	       		$("#CMSUBMIT").addClass("btn-flat-orange");
	      	}
	      else
	      	{
	       		$("#CMSUBMIT").prop('disabled',true);
	       		$("#CMSUBMIT").removeClass("btn-flat-orange");
	   	  	}
	}
	
	function init() {
		$("#CMSUBMIT").click(
				function(e) {
					//TODO 驗證
					console.log("submit~~");
					initBlockUI(); //遮罩
					$("#formId").attr("action",
							"${__ctx}/DIGITAL/ACCOUNT/financial_card_renew_step3");
					$("#formId").submit();
				});
	}
	function closewindow(){
		window.close();
	}
</script>
</head>

<body>
<!-- 	 header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Register" /></li>
    <!-- 數位存款帳戶     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0348" /></li>
    <!-- 數位存款帳戶補申請晶片金融卡     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1553" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<!-- 數位存款帳戶補申請晶片金融卡 -->
			<h2>補申請晶片金融卡</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<div id="step-bar">
            	<ul>
                	<li class="finished">身分驗證</li>
                    <li class="active">顧客權益</li>
                    <li class="">申請資料</li>
					<li class="">確認資料</li>
					<li class="">完成申請</li>
               </ul>
            </div>
			<form id="formId" method="post" action="">
				<input type="hidden" name="isBack" value="">
				<input type="hidden" name="ADOPID" value="NB31_2">	
    			<input type="hidden" name="CUSIDN" value="${ financial_card_renew_step2.data.CUSIDN}">	
    			<input type="hidden" name="ACN" value="${ financial_card_renew_step2.data.ACN}">	    
				<input type="hidden" name="MILADR" value="${ financial_card_renew_step2.data.MILADR}">	    
				<input type="hidden" name="POSTCOD" value="${ financial_card_renew_step2.data.POSTCOD}">		
				
				<div class="main-content-block row">
					<div class="col-12 terms-block">
						<div class="ttb-message">
							<p>顧客權益</p>
						</div>
						<p class="form-description">請您審閱以下顧客權益與說明。</p>
						<div class="text-left">


							<span class="input-subtitle subtitle-color">臺灣中小企業銀行股份有限公司履行個人資料保護法第8條第1項告知義務</span>
							<div class="CN19-clause" style="width: 100%; height: 210px; margin: 20px auto; border: 1px solid #D7D7D7; border-radius: 6px;">
   								<%@ include file="../term/N201_1.jsp"%>
							</div>
						
							<span class="input-block">
								<div class="ttb-input">
									<label class="check-block">本人已詳細閱讀並清楚以上所有內容。
										<input type="checkbox" value="" name="ReadFlag" id="ReadFlag" onclick="checkReadFlag()">    
										<span class="ttb-check"></span>
									</label>
								</div>
							</span>
						</div>
						
						
						
						<input type="button" class="ttb-button btn-flat-gray" value="不同意並離開" name="CLOSEPAGE" onClick="closewindow();" >
						<!-- 確定-->
<%-- 							<spring:message code="LB.Confirm" var="cmSubmit"></spring:message> --%>
						<input type="button" class="ttb-button "name="CMSUBMIT" id="CMSUBMIT" value="同意並繼續" disabled/>
						
					</div>
				</div>
			</form>
		</section>
		</main>
	<!-- 		main-content END -->
	<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>

</html>