<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<style type="text/css">
.table-1 {
    width: 100%;
    border-spacing: 0;
    border-collapse: collapse;
    margin: 0 auto;
    border:
}

.table-1>thead>tr>th,
.table-1>thead>tr>td {
    background-color: #EEEEEE;
    border-top: none;
    text-shadow: 0 1px 0 rgba(255, 255, 255, .5);
    font-weight: bold;
}

.table-1>tbody>tr>td,
.table-1>thead>tr>th {
    border: 1px solid #DDD;
    padding: 8px;
    text-align: left;
}

.table-1 tbody tr:first-child td:first-child {
    text-align: center;
}

.table-1 td:hover {
    background-color: #fbf8e9;
}
</style>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	// HTML載入完成後開始遮罩
	setTimeout("initBlockUI()", 50);
	// 開始跑下拉選單並完成畫面
	setTimeout("init()", 400);
	// 解遮罩
	setTimeout("unBlockUI(initBlockId)", 500);		
});

	function init(){
		
		//列印
		//房屋擔保借款繳息清單
		$("#printbtn").click(function() {
			var params = {
				"jspTemplateName" : "interest_list_result_print",
				"jspTitle" : '<spring:message code= "LB.W0876" />',
				"BRHNUM":"${interest_list_result.data.BRHNUM}",
				"BRHADR":"${interest_list_result.data.BRHADR}",
				"TELNUM1":"${interest_list_result.data.TELNUM1}",
				"NAME":"${interest_list_result.data.NAME}",
				"CUSIDN":"${interest_list_result.data.CUSIDN}"
				};
				openWindowWithPost("${__ctx}/print",
					"height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",
					params);
				});

		//上一頁按鈕
		$("#CMBACK").click(function() {
			var action = '${__ctx}/HOUSE/GUARANTEE/interest_list_confirm';
			$("form").attr("action", action);
			initBlockUI();
			$("form").submit();
		});
	}


</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 貸款服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Loan_Service" /></li>
    <!-- 房屋擔保借款繳息清單     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0876" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code="LB.W0876" /><!-- 房屋擔保借款繳息清單 --></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="">
				<input type="hidden" id="TOKEN" name="TOKEN" value="${sessionScope.transfer_confirm_token}" />
			    <input type="hidden" name="CUSIDN" value="${interest_list_result.data.CUSIDN}">
			    <input type="hidden" name="ACN" value="">
			    <input type="hidden" name="SEQ" value="">
			    <input type="hidden" name="CURDATE" value="">
				<input type="hidden" name="TYPE" value="00">
				<input type="hidden" name="ENDCOD" value="N">
				<div class="main-content-block row">
				<c:if test="${interest_list_result.data.MSGFLG == '00'}">
					<div class="col-12 tab-content">
						<ul class="ttb-result-list">
							<!-- 分行名稱 -->
                            <li>
                                <h3><spring:message code="LB.W0883" />：</h3>
                                <p>${interest_list_result.data.BRHNUM}</p>
                            </li>
                            <!-- 地　　址 -->
                            <li>
                                <h3><spring:message code="LB.W0884" />：</h3>
                                <p>${interest_list_result.data.BRHADR}</p>
                            </li>
                            <!-- 電　　話 -->
                            <li>
                                <h3><spring:message code="LB.W0885" />：</h3>
                                <p> 
                                	${interest_list_result.data.TELNUM1}
								</p>
                            </li>
                        </ul>
						<!-- 房屋擔保借款繳息清單  表格 -->
						<c:forEach var="row" items="${interest_list_result.data.REC}">
						<table class="table-1" data-show-toggle="first">
							<tr>
								<!-- 借戶姓名 -->
								<td><spring:message code="LB.W0886" /></td>
								<td class="font1" style="text-align: left; -ms-word-break: break-all;">${interest_list_result.data.NAME}</td>
								<!-- 統一編號 -->
								<td rowspan="2"><spring:message code="LB.D0621" /></td>
								<td rowspan="2">${interest_list_result.data.CUSIDN}</td>
								<!-- 房屋座落-->
								<td rowspan="2" nowrap="nowrap"><spring:message code="LB.W0888" /></td>
								<td rowspan="2" colspan="2" align="left">${row.ADDR}</td>
							</tr>
							<tr>
							<!-- 房屋所有權人姓名 -->
								<td><spring:message code="LB.W0889" /></td>
								<td align="center">${row.OWNERNM}</td>
							</tr>
							<tr>
								<!-- 房屋所有權取得日 -->
								<td><spring:message code="LB.W0890" /></td>
								<td>
									<!-- 貸款帳號 -->
									<spring:message code="LB.W0891" /><br>
									<!-- 序號 -->
									<spring:message code="LB.W0892" />
								</td>
								<!-- 最初貸款金額(元) -->
								<td colspan="2"><spring:message code="LB.W0893" /></td>
								<!-- 貸款起日 -->
								<td><spring:message code="LB.W0880" /></td>
								<!-- 貸款迄日 -->
								<td><spring:message code="LB.W0881" /></td>
								<td>
								<!-- 本期末未償還 -->
									<spring:message code="LB.W0896" />
									<br>
								<!-- 本金餘額(元) -->
									<spring:message code="LB.W0897" />
								</td>
							</tr>
							<tr>
								<!-- 房屋所有權取得日 -->
								<td align="center">${row.OWNDATE}</td>
								<td align="center">
									<!-- 貸款帳號 -->
									${row.ACN}<br>
										<!-- 序號 -->
									${row.SEQ}
								</td>
								<!-- 最初貸款金額(元) -->
								<td colspan="2">${row.AMTORLN}</td>
								<!-- 貸款起日 -->
								<td nowrap="nowrap">${row.DATFSLN}</td>
								<!-- 貸款迄日 -->
								<td>${row.DDT}</td>
								<!-- 本期末未償還 --> <!-- 本金餘額(元) -->
								<td>${row.BAL_E}</td>
							</tr>
							<tr>
								<!-- 繳息所屬年月 -->
								<td><spring:message code="LB.W0898" /></td>
								<td colspan="3" align="center">
								${row.SINQ}
								~
								${row.DINQ}									
								</td>
								<!-- 繳息金額 -->
								<td nowrap="nowrap"><spring:message code="LB.W0899" /></td>
								<td colspan="2">${row.TOTAMT_E}</td>
							</tr>
						</table>
						</c:forEach>
						<!-- 回上一頁 -->
						<input type="button" id="CMBACK" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />" />
						<!-- 列印 -->
						<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
					</div>
					</c:if>
					<c:if test="${interest_list_result.data.MSGFLG == '01'}">
						<div class="col-12 tab-content">
							<div class="ttb-result-block">
							</div>
							<ul class="ttb-result-list">
								<li>
									<h3><spring:message code="LB.Transaction_message" /></h3>
									<p style="font-weight:bold; color:#FF0000;"><spring:message code="LB.W0900" /></p><!-- 請逕洽原承貸分行辦理！ -->
								</li>
							</ul>
							<div class="text-left">
								<ol class="list-decimal text-left">
								</ol>
							</div>
							<input type="button" id="CMBACK" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />" />
						</div>
					</c:if>
					<c:if test="${interest_list_result.data.MSGFLG == '02'}">
						<div class="col-12 tab-content">
							<div class="ttb-result-block">
							</div>
							 <ol class="description-list list-decimal">
			                    <p><spring:message code="LB.Transaction_message" /></p>
			                    <!-- 請逕洽原承貸分行辦理！ -->
			                    <!-- 無繳息資料無法列印，請與原承貸分行洽詢！ -->
			                    <li style="font-weight:bold; color:#FF0000;"><spring:message code= "LB.X0914" />
			                        </li>
               				 </ol>
							<div class="text-left">
								<ol class="list-decimal text-left">
								</ol>
							</div>
							<input type="button" id="CMBACK" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />" />
						</div>
					</c:if>
				</div>
			</form>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>