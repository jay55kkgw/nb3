<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
		initFootable();
		$('#but_Agree').prop('checked', false);
		// 確認鍵 click
		submit();
	}
	
	// 確認鍵 Click
	function submit() {
		$("#CMSUBMIT").click( function(e) {
			console.log("submit~~");
			// 遮罩
         	initBlockUI();
         	var next = "${c_article.data.type}"+"_confirm";
            $("#formId").attr("action",next);
	 	  	$("#formId").submit();
		});
		//上一頁按鈕
		$("#CMBACK").click(function() {
			initBlockUI();
			var back = "${c_article.data.type}";
			fstop.getPage('${pageContext.request.contextPath}'+'/OTHER/FEE/'+ back,'', '');
		});
	}

</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

	<!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i
					class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><a href="#"></a></li>
			<li class="ttb-breadcrumb-item active" aria-current="page"></li>
		</ol>

	</nav>
	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<form id="formId" method="post" action="">
				<div class="main-content-block row">
                    <div class="col-12">
                        <div class="ttb-message">
								<h4><spring:message code="LB.X0029"/></h4>
                        </div>
						<div class="ttb-message"><p align="left"><font color="royalblue" size="3"><b>下列為申請委託信用卡代繳公用事業費用(水、電、瓦斯、電信費用..等)應遵守之約定條款內容，您如接受本約定條款則請按<span class="Cron">我同意約定條款</span>鍵，以完成申請作業，您如不同意條款內容，則請按<span class="Cron">回上頁</span>鍵，本行將不受理您的代扣繳申請。</b></font></P></div>
                        <table style="text-align:left">
							<tr>
								<td style="width:5em;text-align:center">一、</td>
								<td>立約人委託臺灣中小企業銀行(以下簡稱貴行)自指定之信用卡代繳公用事業費用，並自行依據最近月份公用事業費用繳款單據內容填寫代扣繳資料，如因代扣繳申請書內容填寫不全、錯誤或其他原因，致貴行無法辦理代繳，則本約定書不生效力，所受損失由立約人自行負責。</td>
							</tr>
							<tr>
								<td style="text-align:center">二、</td>
								<td>立約人申請代繳本人或指定第三人公用事業費用，自貴行同意接受委託，並將代繳檔案資料送至公用事業機構審核，經公用事業機構電腦處理並按繳款日遞送扣繳資料起履行代繳義務，在貴行未收到扣繳資料前各月份之費用，仍由公用事業費用繳款人自行繳納。</td>
							</tr>
							<tr>
								<td style="text-align:center">三、</td>
								<td>貴行代繳義務，係以立約人指定之信用卡正卡戶信用額度內足敷委託代繳為條件；如其信用卡遇有額度不足、停用或其他信用貶落事項，或欠繳電信費用經停話處理者，視同自動終止信用卡委託代繳，貴行即將繳費資料退回各公用事業機構，如遭遇罰款、停用等情事所引起之損失及責任概由立約人自行負責處理，貴行無墊款之義務。</br>
																貴行未收到公用事業之繳費資料時，自無法代繳，並不負通知立約人之義務。</td>
							</tr>
							<tr>
								<td style="text-align:center">四、</td>
								<td>立約人委託貴行代繳公用事業費用之用戶編號或號碼，倘貴行接獲有關公用事業機構改號通知時，立約人同意貴行以異動後之用戶編號或號碼，繼續委託代繳。</td>
							</tr>
							<tr>
								<td style="text-align:center">五、</td>
								<td>立約人如擬註銷委託時，除應填具「註銷委託信用卡代繳公用事業費用約定書」外，需俟貴行通知收費機構，並辦妥有關手續後，自貴行接到收費機構通知之日起生效。</td>
							</tr>
							<tr>
								<td style="text-align:center">六、</td>
								<td>立約人委託代繳費用，在未終止委託前，不得藉故拒絕繳費，否則引起之損失及責任，概由立約人自行負責。</td>
							</tr>
							<tr>
								<td style="text-align:center">七、</td>
								<td>經立約人終止委託代繳後，如要再利用信用卡辦理代繳，須重新辦理委託手續。</td>
							</tr>
							<tr>
								<td style="text-align:center">八、</td>
								<td>立約人對公用事業費用、費率、費額之計算暨退補費用等事項，如有疑義，應自行向公用事業機構洽詢。</td>
							</tr>
							<tr>
								<td style="text-align:center">九、</td>
								<td>貴行受託繳訖之金額，視同一般刷卡消費交易，併入當期結帳之消費明細通知持卡人繳還。</td>
							</tr>
							<tr>
								<td style="text-align:center">十、</td>
								<td>立約人對公用事業費用、費率、費額之計算暨退補費等事項如有疑義，應自行與公用事業機構洽詢。</td>
							</tr>
							<tr>
								<td style="text-align:center">十一、</td>
								<td>倘貴行之電腦系統發生故障或電信中斷等因素致無法執行代繳時，貴行得順延至系統恢復正常，始予扣款，其因上開事由所致之損失及責任，由立約人自行負擔。</td>
							</tr>
							<tr>
								<td style="text-align:center">十二、</td>
								<td>立約人同意貴行得將立約人根據特定目的填列之相關基本資料提供貴行電腦處理及利用。</td>
							</tr>
							<tr>
								<td style="text-align:center">十三、</td>
								<td>立約人委託代繳公用事業費用之收據由公用事業機構寄發。</td>
							</tr>
						</table>
                      	<input class="ttb-button btn-flat-orange" type="submit" name="CMSUBMIT" id="CMSUBMIT" value="<spring:message code="LB.W1554"/>" />
						<input class="ttb-button btn-flat-gray" type="button" name="CMBACK" id="CMBACK" value="<spring:message code="LB.Back_to_previous_page"/>"/>
						<input type="hidden" name="type" value="${c_article.data.type}">
						<input type="hidden" name="FGTXDATE" value="${c_article.data.FGTXDATE}">
                    </div>
                </div>
				</form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>