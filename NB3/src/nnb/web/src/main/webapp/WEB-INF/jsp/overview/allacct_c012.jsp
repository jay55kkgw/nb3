<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script type="text/javascript">

	// column's title i18n
	
	
	// c012-基金餘額及損益查詢
	
	i18n['CRLIMITFMT'] = '<spring:message code="LB.Credit_line" />'; // 信用額度
	i18n['CSHLIMITFMT'] = '<spring:message code="LB.Cash_advanced_line" />'; // 預借現金額度
	i18n['ACCBALFMT'] = '<spring:message code="LB.Current_balance" />'; // 目前已使用額度
	i18n['CSHBALFMT'] = '<spring:message code="LB.Cash_advanceUsed" />'; // 已動用預借現金
	i18n['INT_RATEFMT'] = '<spring:message code="LB.Current_revolving_interest" />'; // 本期循環信用利率
	i18n['PAYMT_ACCT_COVER'] = '<spring:message code="LB.Auto-pay_account_number" />'; // 自動扣繳帳號
	i18n['DBCODE'] = '<spring:message code="LB.Payment_method" />'; // 扣繳方式
	i18n['UNALLOT'] = '<spring:message code="LB.W0921"/>';//未分配
	
	//DataTable Ajax tables
	var c012_columns = [
		// 信託編號<br>基金名稱
		{ "data":"COL1", "title":'<spring:message code="LB.W0904"/><br><spring:message code="LB.W0025"/>' },
		// 首次申購<hr>申購方式
		{ "data":"COL2", "title":'<spring:message code="LB.X2247"/><hr><spring:message code= "LB.X2248" />' },
		// 投資幣別<hr>計價幣別
		{ "data":"COL3", "title":'<spring:message code="LB.W0908"/><hr><spring:message code="LB.W0909"/>' },
		// 信託金額<hr>參考現值
		{ "data":"COL4", "title":'<spring:message code="LB.W0026"/><hr><spring:message code="LB.W0915"/>' },
		// 單位數/淨值<hr>淨值日
		{ "data":"COL5", "title":'<spring:message code="LB.W0027"/>/<spring:message code="LB.W1198"/><hr><spring:message code="LB.W0912"/>' },
		// 匯率
		{ "data":"COL6", "title":'<spring:message code="LB.X2388"/><br><spring:message code="LB.Exchange_rate"/>' },
		// 參考損益<hr>累積配息
		{ "data":"COL7", "title":'<spring:message code="LB.W0916"/><hr><spring:message code="LB.X2249"/>' },
		// 損益報酬率<hr>含息報酬率
		{ "data":"COL8", "title":'<spring:message code="LB.X2388"/><spring:message code="LB.W0936"/><hr><spring:message code="LB.X2388"/><spring:message code="LB.W0938"/>' },
		// 快速選單
		{ "data":"QM", "title":'<spring:message code="LB.W1055"/>' }
	];
	
	var c012_sum_columns = [
		// 投資幣別
		{ "data":"ADCCYNAME", "title":'<spring:message code= "LB.W0908" />' },
		// 總信託金額
		{ "data":"SUBTOTAMTFormat", "title":'<spring:message code= "LB.W0933" />' },
		// 總參考現值
		{ "data":"SUBREFVALUEFormat", "title":'<spring:message code= "LB.W0934" />' },
		// 不含息總損益
		{ "data":"SUBDIFAMTFormat", "title":'<spring:message code= "LB.W0935" />' },
		// 損益報酬率
		{ "data":"SUBLCYRATFormat", "title":'<spring:message code= "LB.W0936" />' },
		// 含息總損益
		{ "data":"SUBDIFAMT2Format", "title":'<spring:message code= "LB.W0937" />' },
		// 含息報酬率
		{ "data":"SUBFCYRATFormat", "title":'<spring:message code= "LB.W0938" />' }
	];
	
	var c012_sum2_columns = [
		// 投資幣別
		{ "data":"UNALLOTAMTCRYADCCYNAME", "title":'' },
		// 總信託金額
		{ "data":"UNALLOTAMTFormat", "title":'' },
		{ "data":"UNALLOT", "title":'' },

	];
	
	
	//0置中 1置右
	var c012_tb = [1,0,0,0,0,0,0,0,0,0];
	var c012_align = [0,0,0,1,0,1,1,1,1,0];
	var c012_sum_align = [0,1,1,1,1,1,1];
	var c012_sum2_align = [0,1,0];
	

	
	// Ajax_c012-臺幣活期性存款餘額
	function getMyAssets012() {
		$("#c012").show();
		$("#c012_title").show();
		createLoadingBox("c012_BOX",'');
		createLoadingBox("c012_sum_BOX",'<spring:message code="LB.X0393" />');
		uri = '${__ctx}' + "/OVERVIEW/allacct_c012aj";
		console.log("allacct_c012aj_uri: " + uri);
		fstop.getServerDataEx(uri, null, true, countAjax_c012, null, "c012");
	}

	// Ajax_callback
	function countAjax_c012(data){
		var error = false;
		// 資料處理後呈現畫面
		if (data.result) {
			console.log("c012");
			console.log(data);
			c012_rows = data.data.OverviewREC;		
			c012_sum_rows=data.data.OverviewREC2;
			c012_sum2_rows=data.data.thirdRows;
			
			SHWD=data.data.SHWD;
			shwd_prompt_init(false);
			
			c012_rows.forEach(function(d,i){
				var options = [];
				options.push(new Option("<spring:message code='LB.W0923' />", "1"));
				options.push(new Option("<spring:message code='LB.W0924' />", "2"));
				options.push(new Option("<spring:message code='LB.W0925' />", "7"));
				options.push(new Option("<spring:message code='LB.W0926' />", "8"));
				options.push(new Option("<spring:message code='LB.W0927' />", "9"));
				options.push(new Option("<spring:message code='LB.W0928' />", "10"));
				if(d.ACUCOUNTFormat != '0.0000'&& data.data.txnFlag == true)
					{
					options.push(new Option("<spring:message code='LB.W0929' />", "3"));
					options.push(new Option("<spring:message code='LB.W0930' />", "4"));
					}
				options.push(new Option("<spring:message code='LB.W0931' />", "5"));
				if(d.AC202 == '2')
					{
				    options.push(new Option("<spring:message code='LB.W0932' />", "6"));
					}
				var qm = $("#actionBar").clone();
				qm.children()[0].id = "c012";
				qm.children()[0].name=i;
				qm.show();
				options.forEach(function(x){
					qm[0].children[0].options.add(x);
				});
				
				d["QM"]=qm.html();
			});
			
			c012_sum2_rows.forEach(function(d,i){
				d["UNALLOT"]=i18n['UNALLOT'];
			});
		} else {
			error=true;
			// 錯誤訊息
			c012_columns = [ {
					"data" : "MSGCODE",
					"title" : '<spring:message code="LB.Transaction_code" />'
				}, {
					"data" : "MESSAGE",
					"title" : '<spring:message code="LB.Transaction_message" />'
			} ];
			c012_rows = [ {
				"MSGCODE" : data.msgCode,
				"MESSAGE" : data.message
			} ];
			// 錯誤訊息
			c012_sum_columns = [ {
					"data" : "MSGCODE",
					"title" : '<spring:message code="LB.Transaction_code" />'
				}, {
					"data" : "MESSAGE",
					"title" : '<spring:message code="LB.Transaction_message" />'
			} ];
			c012_sum_rows = [ {
				"MSGCODE" : data.msgCode,
				"MESSAGE" : data.message
			} ];
			c012_sum_align = [0,0];
			
		}
		createTable("c012_BOX",c012_rows,c012_columns,c012_align,c012_tb);
		createTable("c012_sum_BOX",c012_sum_rows,c012_sum_columns,c012_sum_align);
		//
		if(!error && c012_sum2_rows.length>0){
			$("#thirdDataCount").show();
			var lb = $("#table_temp").clone();
			lb[0].id="thirdData_table";
			$("#thirdDataCount").append(lb);
			createTable("thirdData",c012_sum2_rows,c012_sum2_columns,c012_sum2_align);
		}
		
	}
		

		
</script>
<p id="c012_title" class="home-title" style="display: none"><spring:message code="LB.W0903" /></p>
<div id="c012" class="main-content-block" style="display:none"> 
<div id="c012_BOX" style="display:none"></div>
<ul class="ttb-result-list">
	<li class="full-list"><p><spring:message code="LB.Note"/>：<spring:message code="LB.W0915"/>=<spring:message code="LB.W0027"/>*<spring:message code="LB.W1198"/>*<spring:message code="LB.X2388"/><spring:message code="LB.Exchange_rate"/></p></li>
</ul>
<div id="c012_sum_BOX" style="display:none"></div>

<div id="thirdDataCount" class="col-12" style="display:none">
<ul class="ttb-result-list">
<li class="full-list text-center"><p><spring:message code="LB.W0941"/></p></li>
<br><br>
<br><br>
<li class="full-list"><p><spring:message code="LB.W0942"/></p></li>

</div>
	                            
</ul>
<form id="formC012" method="post">
<input type="hidden" name="ADOPID" value="C012"/>
</form>
</div>