<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function(){
			window.print();
		});
	</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br/><br/>
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
	<br/><br/><br/>
	<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
	<br/><br/><br/>
	
	<div> 
		<!-- 資料 -->
		<!-- 表格區塊 -->
		<div class="ttb-input-block">
<!-- 完成修改數位存款帳戶資料交易 -->
	        <center><h4><p>完成修改</p></h4></center>
			
			<br>
			<br>
			<div class="text-left">
					<p><spring:message code= "LB.X0152" />:</p>
					<p>謝謝您完成修改數位存款帳戶資料交易。<br>
					<spring:message code= "LB.X0221" /><B>${BRHNAME}(<spring:message code= "LB.X0222" />${BRHTEL})</B><spring:message code= "LB.X0223" /></p>
					<p><spring:message code= "LB.X0224" /></p>
			</div>
			<div class="text-right">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<spring:message code= "LB.W0605" />&nbsp;&nbsp;&nbsp;<spring:message code= "LB.X0193" /></font>
			</div>    
			<div class="text-left">
				${errMsgStr}
			</div>    
		</div>
		
	</div>
	
</body>
</html>