<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="bs" value="${f_order_renewal_confirm.data}"></c:set>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
	
	<script type="text/javascript">
		$(document).ready(function() {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		
		// 畫面初始化
		function init() {
			// 表單驗證初始化
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });

			// 確認鍵 click
			goOn();
			// 上一頁按鈕 click
			goBack();
			// 交易類別change 事件
			changeFgtxway();
		}
		
		
		// 確認鍵 Click
		function goOn() {
			$("#CMSUBMIT").click( function(e) {
				console.log("submit~~");
				// 表單驗證
				if ( !$('#formId').validationEngine('validate') ) {
					e.preventDefault();
				} else {
					// 解除表單驗證
					$("#formId").validationEngine('detach');
					
					// 通過表單驗證
					processQuery();
				}
			});
		}
		
		
		// 通過表單驗證準備送出
		function processQuery(){
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
			// 交易機制選項
			switch(fgtxway) {
				case '0':
					// SSL
					// 交易密碼sha1
					$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
					// 清除SSL密碼欄，避免儲存或傳送明碼到後端
					$('#CMPASSWORD').val("");
					// 遮罩
		         	initBlockUI();
		            $("#formId").submit();
		         	
					break;
				case '1':
					// IKEY
					useIKey();
					
					break;
		        case '7'://IDGATE認證		 
		            idgatesubmit= $("#formId");		 
		            showIdgateBlock();		 
		            break;
				default:
					//請選擇交易機制
					//alert("<spring:message code= "LB.Alert001" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert001' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			}
		}
		
		
		// 上一頁按鈕 click
		function goBack() {
			// 上一頁按鈕
			$("#CMBACK").click(function() {
				// 遮罩
				initBlockUI();
				// 解除表單驗證
				$("#formId").validationEngine('detach');
				// 讓Controller知道是回上一頁
				$('#back').val("Y");
				// 回上一頁
				var action = '${__ctx}/FCY/ACCT/TDEPOSIT/f_order_renewal_step1';
				$("#formId").attr("action", action);
				$("#formId").submit();
			});
		}
		
		
		// 交易類別change 事件
		function changeFgtxway(){
			$('input[type=radio][name=FGTXWAY]').change(function(){
				console.log(this.value);
				if(this.value=='0'){
					$("#CMPASSWORD").addClass("validate[required]")
				}else if(this.value=='1'){
					$("#CMPASSWORD").removeClass("validate[required]");
				}else if(this.value=='2'){
					$("#CMPASSWORD").removeClass("validate[required]");
				}else if(this.value=='7'){
					$("#CMPASSWORD").removeClass("validate[required]");
				}
				
			});
		}
		
		
		//重新輸入
	 	function formReset() {
	 		if ($('#actionBar').val()=="reEnter"){
		 		$('#actionBar').val("");
		 		document.getElementById("formId").reset();
	 		}
		}
	</script>
</head>

<body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %> 
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 定存服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Current_Deposit_To_Time_Deposit" /></li>
    <!-- 定存單到期續存     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Renewal_At_Maturity_Of_FX_Time_Deposit" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
	</div>
	<!-- 主頁內容  -->
	<main class="col-12">
		<section id="main-content" class="container">
			<h2>
				<!--  外匯定存單到期續存 -->
				<spring:message code="LB.Renewal_At_Maturity_Of_FX_Time_Deposit" />
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<!-- 下拉式選單-->
			<div class="print-block no-l-display-btn">
				<select class="minimal" id="actionBar" onchange="formReset()">
					<option value=""><spring:message code="LB.Execution_option" /></option>
						<!-- 重新輸入-->
					<option value="reEnter"><spring:message code="LB.Re_enter"/></option>
				</select>
			</div>
			<form id="formId" method="post" action="${__ctx}/FCY/ACCT/TDEPOSIT/f_order_renewal_result">
				<input type="hidden" id="back" name="back" value="">
				<!-- 存單帳號 -->
				<input type="hidden" name="FYACN" value="${bs.FYACN}" />
				<!-- 存單號碼 -->
				<input type="hidden" name="FDPNUM" value="${bs.FDPNUM}" />
				<!-- 期別 -->
				<input type="hidden" name="TERM" value="${bs.TERM}" />
				<!-- 期別種類 -->
				<input type="hidden" name="TYPCOD" value="${bs.TYPCOD}" />
				<!-- 健保費 -->
				<input type="hidden" name="NHITAX" value="${bs.NHITAX}" />
				<!-- 續存方式 -->
				<input type="hidden" name="TYPE1" value="${bs.TYPE1}" />
				<!-- 利息轉入帳號 -->
				<input type="hidden" name="FYTSFAN" value="${bs.FYTSFAN}" />
				<!-- 計息方式 -->
				<input type="hidden" name="INTMTH" value="${bs.INTMTH}" />
				<!-- 存單金額 -->
				<input type="hidden" name="AMTFDP" value="${bs.AMTTSF}" />
				<!-- TXTOKEN -->
				<input type="hidden" name="TXTOKEN" value="${bs.TXTOKEN}" />
				<!-- 原存單利息  -->
				<input type="hidden" name="INT" value="${bs.INT }" />
				<!-- 所得稅 -->
				<input type="hidden" name="FYTAX" value="${bs.FYTAX }" />
				
				<!-- SSL所需欄位 交易密碼SHA1值 -->
				<input type="hidden" id="PINNEW" name="PINNEW"  value="">
				<!-- 交易機制所需欄位 -->
				<input type="hidden" id="jsondc" name="jsondc" value='${bs.jsondc}'>
				<input type="hidden" id="ISSUER" name="ISSUER" value="">
				<input type="hidden" id="ACNNO" name="ACNNO" value="">
				<input type="hidden" id="TRMID" name="TRMID" value="">
				<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
				<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
				<input type="hidden" id="TAC" name="TAC" value="">
				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
				
				
				<!--交易步驟 -->
				<div id="step-bar">
					<ul>
						<!--輸入資料 -->
						<li class="finished"><spring:message code="LB.Enter_data"/></li>
						<!-- 確認資料 -->
						<li class="active"><spring:message code="LB.Confirm_data"/></li>
						<!-- 交易完成 -->
						<li class=""><spring:message code="LB.Transaction_complete"/></li>
					</ul>
        		</div>
				<!-- 表單顯示區  -->
				<div class="main-content-block row">
					<div class="col-12">
						<div class="ttb-input-block">
							<div class="ttb-message">
								<span>
									<!--  請確認續存資料 -->
									<spring:message code="LB.Confirm_renewal_data" />
								</span>
							</div>
							<!-- 存單帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Account_no" /></h4>
									</label>
								</span>
								<span class="input-block">
									${bs.FYACN}
								</span>
							</div>
							<!-- 存單號碼 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Certificate_no" /></h4>
									</label>
								</span>
								<span class="input-block">
									${bs.FDPNUM}
								</span>
							</div>
							<!-- 存單金額 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Certificate_amount" /></h4>
									</label>
								</span>
								<!-- 幣別 & 存單金額 -->
								<span class="input-block">
									<div class="ttb-input">
	                                    <span class="high-light">
	                                     <span class="input-unit"> ${bs.CRYNAME}</span></span>
		                                    <!--顯示金額 -->
		                                <p>
		                                    ${bs.SHOW_AMTTSF }
		                                    <!--元 -->
		                                    <span class="input-unit"><spring:message code="LB.Dollar"/></span>
	                                 	</p>
                                 	</div>
                                 </span>
							</div>
							<!-- 存單種類 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Deposit_certificate_type" /></h4>
									</label>
								</span>
								<span class="input-block">
									<!-- 定期存款 -->
									<spring:message code="LB.NTD_deposit_type_1" />
								</span>
							</div>
							
							<!-- 存款期別 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Deposit_period" /></h4>
									</label>
								</span>
								<span class="input-block">
									${bs.SHOW_TERM}
								</span>
							</div>
							
							
							<!-- 起存日 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Start_date" /></h4>
									</label>
								</span>
								<span class="input-block">
									${bs.SHOW_DPISDT}
								</span>
							</div>
							<!-- 到期日 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Maturity_date"/></h4>
									</label>
								</span>
								<span class="input-block">
									${bs.SHOW_DUEDAT}
								</span>
							</div>
							<!-- 計息方式 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Interest_calculation"/></h4>
									</label>
								</span>
								<span class="input-block">
									${bs.SHOW_INTMTH}
								</span>
							</div>
							<!-- 利率 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Interest_rate"/></h4>
									</label>
								</span>
								<span class="input-block">
									${bs.ITR} %
								</span>
							</div>
							<!-- 利息轉入帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Interest_transfer_to_account" /></h4>
									</label>
								</span>
								<span class="input-block">
									${bs.FYTSFAN }
								</span>
							</div>
							<!-- 原存單金額 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Original_amount" /></h4>
									</label>
								</span>
								<!-- 幣別 & 原存單金額 -->
								<span class="input-block">
									${bs.CRYNAME } ${bs.SHOW_AMTFDP }
								</span>
							</div>
							<!--原存單利息  -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Original_interest" /></h4>
									</label>
								</span>
								<span class="input-block">
									${bs.SHOW_INT }
								</span>
							</div>
							<!--代扣利息所得稅  -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Withholding_interest_income_tax" /></h4>
									</label>
								</span>
								<span class="input-block">
									${bs.SHOW_FYTAX }
								</span>
							</div>
							<!-- 健保費  -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.NHI_premium" /></h4>
									</label>
								</span>
								<span class="input-block">
									${bs.SHOW_NHITAX }
								</span>
							</div>
							<!-- 交易機制 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<!-- 交易機制 -->
										<h4><spring:message code="LB.Transaction_security_mechanism" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<!-- 交易密碼(SSL) -->
										<label class="radio-block">
											<spring:message code="LB.SSL_password" />
											  <input type="radio" name="FGTXWAY" checked="checked" value="0">
											<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<!--請輸入密碼 -->
                                    	<spring:message code="LB.Please_enter_password" var="plassEntpin"></spring:message>
										<input type="password" id="CMPASSWORD" name="CMPASSWORD" class="text-input validate[required]" maxlength="8" placeholder="${plassEntpin}">
									</div>
									
									<!-- 使用者是否可以使用IKEY -->
									<c:if test = "${sessionScope.isikeyuser}">
										<!-- IKEY -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Electronic_signature" />
												<input type="radio" name="FGTXWAY" id="CMIKEY" value="1">
												<span class="ttb-radio"></span>
											</label>
										</div>
									</c:if>
									<div class="ttb-input" name="idgate_group" style="display:none" onclick="hideCapCode('Y')">		 
                                       <label class="radio-block">裝置推播認證(請確認您的行動裝置網路連線是否正常，及推播功能是否已開啟)
	                                       <input type="radio" id="IDGATE" 	name="FGTXWAY" value="7">
	                                       <span class="ttb-radio"></span>
                                       </label>		 
                                   	</div>
								</span>
							</div>
						</div>
						<!-- button -->
							<!--回上頁 -->
							<spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>
							<input type="button" name="CMBACK" id="CMBACK" value="${cmback}" class="ttb-button btn-flat-gray" >
							<!--重新輸入 -->
							<spring:message code="LB.Re_enter" var="cmRest"></spring:message>
							<input type="reset" name="CMRESET" id="CMRESET" value="${cmRest}" class="ttb-button btn-flat-gray no-l-disappear-btn">
							<!-- 確定 -->
							<spring:message code="LB.Confirm" var="cmSubmit"></spring:message>
							<input type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" class="ttb-button btn-flat-orange">
						</div>
					</div>
				</div>
			</form>
		</section>
		<!-- main-content END -->
	</main>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>

</body>

</html>