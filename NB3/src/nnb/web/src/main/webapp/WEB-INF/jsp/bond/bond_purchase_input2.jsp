<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>
<!--舊版驗證-->
<script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
<script type="text/javascript">
$(document).ready(function(){
// 	alert(jQuery.parseJSON('${bond_purchase_input2.data.BONDDATA}'))
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",100);
	//開始查詢資料並完成畫面
	setTimeout("init()",200);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	
	$("#CMSUBMIT").click(function(){
		
		var BONDDATA = jQuery.parseJSON('${bond_purchase_input2.data.BONDDATA}');
		var min = BONDDATA.MINBPRICE;
		var plus = BONDDATA.PROGRESSIVEBPRICE;
		var min_fmt = BONDDATA.MINBPRICE_FMT
		var plus_fmt = BONDDATA.PROGRESSIVEBPRICE_FMT
		
		min = min.replace(/,/g, "");
		plus = plus.replace(/,/g, "");
		
		if(!CheckAmountBond("AMOUNT",'<spring:message code="LB.X2511" />',min,plus,min_fmt,plus_fmt,'1')){
			return false;
		}
		
		if( $('#I12').val()!=''){
			if(!CheckNumberAndLen("I12",'<spring:message code="LB.X2512" />',true,6)){
				return false;
			}
		}
		
		$("#formID").attr("action","${__ctx}/BOND/PURCHASE/bond_purchase_detail_check");
		initBlockUI();//遮罩
		$("#formID").submit();	
	
	});
	
	$("#CMCANCEL").click(function(){
		$("#formID").attr("action","${__ctx}/BOND/PURCHASE/bond_purchase_input");
		$("#formID").submit();	
	});
	
});
function init(){
	var BONDDATA = jQuery.parseJSON('${bond_purchase_input2.data.BONDDATA}');
	$('#BONDNAME').html(BONDDATA.BONDCODE+" "+BONDDATA.BONDNAME);
	$('#BONDCRY_SHOW').html(BONDDATA.BONDCRY_SHOW);
	$('#CRYSHOW').html(BONDDATA.BONDCRY_SHOW+" ");
	$('#I13').val(BONDDATA.BONDCRY);

	$('#ALERTMSG').html('<spring:message code="LB.X2513" />'+"："+ BONDDATA.BONDCRY_SHOW +' '+BONDDATA.MINBPRICE_FMT + "<br>"+'<spring:message code="LB.X2489" />'+"　　：" + BONDDATA.BONDCRY_SHOW +' '+BONDDATA.PROGRESSIVEBPRICE_FMT);

}


</script>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 海外債券     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2391" /></li>
    <!-- 海外債券交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2515" /></li>
    <!-- 海外債券申購     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X2516" /></li>
		</ol>
	</nav>

	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
<!-- 				海外債券申購 -->
				<h2><spring:message code="LB.X2516" /></h2>
				<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
					<form id="formID" method="post">
					<input type="hidden" id="TOKEN" name = "TOKEN" value="${sessionScope.transfer_confirm_token}">
					<input type="hidden" id="I13" name = "I13" value="">
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<!-- 客戶姓名 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1066" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${bond_purchase_input2.data.hiddenNAME}</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 客戶投資屬性 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1067" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${bond_purchase_input2.data.FDINVTYPE_SHOW} </span>
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 外幣扣帳帳號 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
<!--                                 	外幣扣帳帳號 -->
                                        <h4><spring:message code="LB.X2517" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${bond_purchase_input2.data.ACN2}</span>
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 債券名稱 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1012" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                			<span id="BONDNAME"></span>
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 投資幣別 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W0908" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                			<span id="BONDCRY_SHOW"></span>
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 委託買價 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.X2518" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                		${bond_purchase_input2.data.BUYPRICE_FMT}%
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 委買面額 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.X2511" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                			<span id="CRYSHOW"></span>
                                			<input type="text" id="AMOUNT" name="AMOUNT" class="text-input" size="10" maxlength="10" value="" placeholder="" autocomplete="off"> 
                                		</div>
                                		<span id="ALERTMSG"></span>
                               		</span>
                            	</div>
                            	<!-- 推薦行員(6碼) -->
                            	<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.X2519" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                			<input type="text" id="I12" name="I12" class="text-input" size="6" maxlength="6" value="${bond_purchase_input2.data.EMPNO}" placeholder="" autocomplete="off">
                                		</div>
                               		</span>
                            	</div>
                            </div>
							<input type="button" id="CMCANCEL" value="<spring:message code="LB.Cancel"/>" class="ttb-button btn-flat-gray"/>	
							<input type="button" id="CMSUBMIT" value="<spring:message code="LB.Confirm"/>" class="ttb-button btn-flat-orange"/>
						</div>
					</div>	
				</form>
<!-- 				<ol class="description-list list-decimal"> -->
<!-- 					說明區 -->
<!-- 				</ol> -->
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>
