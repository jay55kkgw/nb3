<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!-- 登出前的遮罩，詢問是否繼續使用 -->
<section id="logout-block" class="error-block" style="display:none">
    <div class="error-for-message">
    	<!-- 自動登出提醒 -->
        <p class="error-title"><spring:message code= "LB.X1552" /></p>
        <p class="error-content"><spring:message code= "LB.X1553" /><br /><spring:message code= "LB.X1554" /></p>
        <!-- 登出倒數 -->
        <p class="error-info"><img class="error-icon" src="${__ctx}/img/clock-circular-outline.svg?a=${jscssDate}" /><spring:message code= "LB.X1912" />
        	<span class="high-light pl-2">
        		<font id="countdownheader"></font><spring:message code= "LB.Second" />
			</span>
        </p>
        <!-- 繼續使用 -->
        <button class="btn-flat-orange ttb-pup-btn" name="CMCONTINUE" id="CMCONTINUE" onClick="keepLogin();">
        	<spring:message code= "LB.X1555" />
        </button>
        <!-- 登出 -->
        <button class="btn-flat-orange ttb-pup-btn" name="CMLOGOUT" id="CMLOGOUT" 
        	onClick="fstop.logout( '${__ctx}'+'/logout_aj' , '${__ctx}'+'/login');">
        	<spring:message code= "LB.Logout" />
        </button>
    </div>
</section>

<div id="desk-header" class="row">
	<div class="header-logo col-3">
		<a id="logobtn" href="${__ctx}/login"><img src="${__ctx}/img/tbb-logo-onlinebanking.svg?a=${jscssDate}"></a>
	</div>
	<div class="col-9">
		<ul id="header-nav" style="line-height:78px;">
			<!-- 臺灣企銀首頁 -->
			
            <!-- 憑證管理 -->
            <li><a>&nbsp;</a>
				
  			</li>
			<!-- 網站導覽 -->
			
		</ul>
	</div>
	
</div>
<div id="mobile-header">
<!-- 	<a href="#fast-menu-block" data-toggle="collapse" data-parent="#fast-menu" aria-expanded="true" aria-controls="fast-menu-block" role="button"> -->
<%-- 		<img src="${__ctx}/img/group-2.svg?a=${jscssDate}"> --%>
<!-- 	</a> -->
	
	<img class="mobile-logo" src="${__ctx}/img/logo-white.svg?a=${jscssDate}"> 
	<!-- 小版多語系 --> 	
<!-- 	<select id="selectLg2" onchange="location = this.value">			   -->
<!-- 	</select> -->
<!-- 	<a href="#" role="button"> -->
<%-- 		<img src="${__ctx}/img/menu.svg?a=${jscssDate}"> --%>
<!-- 	</a> -->
	<!-- for mobile menu -->
<!-- 	<button class="hamburger hamburger--spring" type="button" href="#header-content" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="header-content"> -->
<!--     	<span class="hamburger-box"> -->
<!--         	<span class="hamburger-inner"></span> -->
<!--         </span> -->
<!--    	</button> -->
<%-- 	<a data-toggle="collapse" href="#header-content" role="button" aria-expanded="false" aria-controls="header-content"><img src="${__ctx}/img/menu.svg?a=${jscssDate}"></a> --%>


</div>





<!-- LOADING遮罩區塊改放到__import.jsp -->
<!-- <div id="loadingBox" class="Loading_box"> -->
<!--     <div class="sk-fading-circle"> -->
<!--       <div class="sk-circle1 sk-circle"></div> -->
<!--       <div class="sk-circle2 sk-circle"></div> -->
<!--       <div class="sk-circle3 sk-circle"></div> -->
<!--       <div class="sk-circle4 sk-circle"></div> -->
<!--       <div class="sk-circle5 sk-circle"></div> -->
<!--       <div class="sk-circle6 sk-circle"></div> -->
<!--       <div class="sk-circle7 sk-circle"></div> -->
<!--       <div class="sk-circle8 sk-circle"></div> -->
<!--       <div class="sk-circle9 sk-circle"></div> -->
<!--       <div class="sk-circle10 sk-circle"></div> -->
<!--       <div class="sk-circle11 sk-circle"></div> -->
<!--       <div class="sk-circle12 sk-circle"></div> -->
<!--     </div> -->
<!-- </div> -->
<script type="text/JavaScript">
 		function i18n(){
 			// 動態加入多語系下拉選單
 			$("#header-nav").append('<li><div><select class="custom-select select-input half-input" id="selectLg" onchange="location = this.value"></select></div></li>');
 			
  			var locale = "${__i18n_locale}";
  			if(locale == "zh_TW"){
  				$("#selectLg").append($("<option></option>").attr("value", "#").text("Language"));
   				$("#selectLg").append($("<option></option>").attr("value", "${__i18n_en}").text("English"));
   				$("#selectLg").append($("<option></option>").attr("value", "${__i18n_zh_TW}").text("繁體"));
   				$("#selectLg").append($("<option></option>").attr("value", "${__i18n_zh_CN}").text("简体"));
   				$("#selectLg").val('${__i18n_zh_TW}');
   				
  			}else if(locale == "zh_CN"){
  				$("#selectLg").append($("<option></option>").attr("value", "#").text("Language"));
   				$("#selectLg").append($("<option></option>").attr("value", "${__i18n_en}").text("English"));
   				$("#selectLg").append($("<option></option>").attr("value", "${__i18n_zh_TW}").text("繁體"));
   				$("#selectLg").append($("<option></option>").attr("value", "${__i18n_zh_CN}").text("简体"));
  			 	$("#selectLg").val('${__i18n_zh_CN}');
  			 	
  			}else if(locale == "en"){
  				$("#selectLg").append($("<option></option>").attr("value", "#").text("Language"));
   				$("#selectLg").append($("<option></option>").attr("value", "${__i18n_en}").text("English"));
   				$("#selectLg").append($("<option></option>").attr("value", "${__i18n_zh_TW}").text("繁體"));
   				$("#selectLg").append($("<option></option>").attr("value", "${__i18n_zh_CN}").text("简体"));
   				$("#selectLg").val('${__i18n_en}');
  			}  
  			
  			$("#selectLg").show();
 		}
 		
 		// Look for .hamburger
 		var hamburger = document.querySelector(".hamburger");
 		// On click
 		hamburger.addEventListener("click", function() {
	 		// Toggle class "is-active"
	 		hamburger.classList.toggle("is-active");
	 		// Do something else, like open/close menu
 		});
</script>