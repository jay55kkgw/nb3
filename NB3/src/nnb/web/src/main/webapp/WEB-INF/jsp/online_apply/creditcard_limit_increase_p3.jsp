<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>
<script type="text/javascript" src="${__ctx}/js/jquery.maskedinput.js"></script>
<!-- DIALOG會用到 -->
<script type="text/javascript" src="${__ctx}/js/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery-ui.css">

<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		// HTML載入完成後開始遮罩
	// 	setTimeout("initBlockUI()", 10);
		initBlockUI();
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	function init(){
		$("#formId").validationEngine({
			binded: false,
			promptPosition: "inline"
		});
		$("#back").val("");
		$("#CMSUBMIT").click(function(e){
			
			$("#ckqu").val($("#QUOTA").val());
			
			e = e || window.event;
			if(!$('#formId').validationEngine('validate')){
	    		e.preventDefault();
	    	}
			else{
				$("#formId").attr("action","${__ctx}/ONLINE/APPLY/creditcard_limit_increase_p3_1");
				$("#formId").submit();
			}
		});
		
		$("#CMBACK").click(function(e){
			$("#formId").validationEngine('detach');
			$("#back").val("Y");
			$("#formId").attr("action","${__ctx}/ONLINE/APPLY/creditcard_limit_increase_otp");
			$("#formId").submit();
		});
		
		datetimepickerEvent();
		openinput();
		addata();
	}
	
	function chgreason(){
		var reason = $("#REASON").val();
		if(reason == "9"){
			$("#inputREASON").show();
			$("#REASONIN").addClass("validate[required]");
		}
		else{
			$("#inputREASON").hide();
			$("#REASONIN").removeClass("validate[required]");
		}
	}
	
	var oday = '${limit_increase.data.Oday}';
	var maxdate = '${limit_increase.data.maxDate}';
	function openinput(){
		//臨時
		if($("#TEMPORARY").prop("checked") == true){
			$("#temporarydate").show();
			$("#cksd").addClass("validate[funcCall[validate_CheckDate[期間起日,CMSDATE," + oday + "," + maxdate + "]]]");
			$("#cked").addClass("validate[funcCall[validate_CheckDate[期間迄日,CMEDATE," + oday + "," + maxdate + "]]]");
		}
		else{
			$("#temporarydate").hide();
			$("#cksd").removeClass("validate[funcCall[validate_CheckDate[期間起日,CMSDATE," + oday + "," + maxdate + "]]]");
			$("#cked").removeClass("validate[funcCall[validate_CheckDate[期間迄日,CMEDATE," + oday + "," + maxdate + "]]]");
		}
	}
	
	//日曆欄位參數設定
	function datetimepickerEvent(){

	    $(".CMSDATE").click(function(event) {
			$('#CMSDATE').datetimepicker('show');
		});
	    $(".CMEDATE").click(function(event) {
			$('#CMEDATE').datetimepicker('show');
		});
		
		jQuery('.datetimepicker').datetimepicker({
			timepicker:false,
			closeOnDateSelect : true,
			scrollMonth : false,
			scrollInput : false,
		 	format:'Y/m/d',
		 	lang: '${transfer}'
		});
	}
	
	function addata(){
		if("${limit_increase.data.QUOTA}" != ""){
			$("#QUOTA").val('${limit_increase.data.QUOTA}');
		}
		if("${limit_increase.data.REASON}" != ""){
			$("#REASON").val('${limit_increase.data.REASON}');
			chgreason();
		}
		if("${limit_increase.data.FGTXSTATUS}" != ""){
			if("${limit_increase.data.FGTXSTATUS}" == "1"){
				$("#TEMPORARY").prop("checked",true);
				$("#PERMANENT").prop("checked",false);
				$("#CMSDATE").val('${limit_increase.data.CMSDATE}');
				$("#CMEDATE").val('${limit_increase.data.CMEDATE}');
				openinput();
			}
			else if("${limit_increase.data.FGTXSTATUS}" == "2"){
				$("#TEMPORARY").prop("checked",false);
				$("#PERMANENT").prop("checked",true);
				openinput();
			}
		}
	}
	
</script>
</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Credit_Card_Limit_Increase" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2><spring:message code="LB.Credit_Card_Limit_Increase" /></h2>
				<div id="step-bar">
                    <ul>
                        <li class="finished"><spring:message code="LB.D0851" /></li><!-- 身份驗證 -->
                        <li class="finished"><spring:message code="LB.SMS_Verification" /></li>
                        <li class="active"><spring:message code="LB.X1967" /></li><!-- 申請資料 -->
                        <li class=""><spring:message code="LB.Confirm_data" /></li><!-- 確認資料 -->
                        <li class=""><spring:message code="LB.X1968" /></li><!-- 完成申請 -->
                    </ul>
                </div>
				<form method="post" id="formId" action="">
					<input type="hidden" name="ADOPID" id="ADOPID" value=""/>
					<input type="hidden" name="back" id="back" value=""/>
					<input type="hidden" name="CUSIDN" id="CUSIDN" value="${limit_increase.data.CUSIDN }"/>
					<input type="hidden" name="Oday" id="Oday" value="${limit_increase.data.Oday}"/>
					<input type="hidden" name="maxDate" id="maxDate" value="${limit_increase.data.maxDate}"/>
					<input type="hidden" id="FROM_NB3" name="FROM_NB3" value="${FROM_NB3}"/>
					<!-- 表單顯示區  -->
					<div class="main-content-block pl-0 pr-0 row">
						<div class="col-12">
							<div class="ttb-input-block">
								<div class="ttb-message">
									<p><spring:message code="LB.X1967" /></p>
								</div>
							
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.D0049" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${limit_increase.data.NAMEshow }
										</div>
									</span>
								</div>
							
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.D0581" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${limit_increase.data.CUSIDNshow }
										</div>
									</span>
								</div>
							
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.D1601" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${limit_increase.data.LIMITshow }
											<span class="input-unit m-0"><spring:message code='LB.D0509' /></span>
										</div>
									</span>
								</div>
							
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<span class="high-light">*</span>
												<spring:message code='LB.D1602' />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input id="QUOTA" type="text" class="ttb-input text-input" name="QUOTA" placeholder="<spring:message code='LB.D1616' />" maxlength="8" autocomplete="off">
											<span class="input-unit m-0"><spring:message code='LB.D0509' /></span>
											<span class="input-unit m-0">（<spring:message code='LB.D1603' />）</span>
											<input id="ckqu" type="text" class="text-input validate[funcCallRequired[validate_Check_Amount_int[<spring:message code='LB.D1602' />, ckqu, ${limit_increase.data.LIMIT }, 1000000,1000]]]" style="border:white; margin:0px; padding:0%; height: 0px; width: 0px; visibility: hidden;" value="" />
											
										</div>
									</span>
								</div>
							
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<span class="high-light">*</span>
												<spring:message code='LB.D1604' />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<select class="custom-select select-input validate[funcCall[validate_CheckSelect[申請原因,REASON,#]]]" name="REASON" id="REASON" onchange="chgreason()">
	                                            <option value="#"><spring:message code='LB.D1605' /></option>
									 			<option value="1"><spring:message code='LB.W1740' /></option>
									 			<option value="2"><spring:message code='LB.W1741' /></option>
									 			<option value="3"><spring:message code='LB.W1742' /></option>
									 			<option value="4"><spring:message code='LB.W0367' /></option>
									 			<option value="5"><spring:message code='LB.W1743' /></option>
									 			<option value="7"><spring:message code='LB.W1744' /></option>
									 			<option value="8"><spring:message code='LB.W1745' /></option>
									 			<option value="9"><spring:message code='LB.D0572' /></option>
	                                        </select>
										</div>
										<div class="ttb-input" id="inputREASON" style="display: none">
	                                        <input id="REASONIN" type="text" class="ttb-input text-input" name="REASONIN" placeholder="請輸入申請原因" maxlength="10" autocomplete="off" />
	                                    </div>
									</span>
								</div>
							
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<span class="high-light">*</span>
												<spring:message code='LB.D1606' />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block"><spring:message code='LB.D1607' />
												<input type="radio" name="FGTXSTATUS" id="TEMPORARY" value="1" onclick="openinput()" checked/>
												<span class="ttb-radio"></span>
											</label>										
										</div>
										
										<div class="ttb-input" id="temporarydate" style="display: none">
											<!--期間起日 -->
											<div class="ttb-input">
												<span class="input-subtitle subtitle-color">
													<spring:message code="LB.Period_start_date" />
												</span>
												<input type="text" id="CMSDATE" name="CMSDATE" class="text-input datetimepicker" value="${limit_increase.data.Oday}" />
												<span class="input-unit CMSDATE">
													<img src="${__ctx}/img/icon-7.svg" />
												</span>
												<input id="cksd" name="cksd" type="text" class="text-input" style="border:white; margin:0px; padding:0%; height: 0px; width: 0px; visibility: hidden;" value="${limit_increase.data.Oday}" />
											</div>
											<!--期間迄日 -->
											<div class="ttb-input">
												<input type="text" id="CMEDATE" name="CMEDATE" class="text-input datetimepicker" value="${limit_increase.data.Oday}" />
												<span class="input-unit CMEDATE">
													<img src="${__ctx}/img/icon-7.svg" />
												</span>
												<input id="cked" name="cked" type="text" class="text-input" style="border:white; margin:0px; padding:0%; height: 0px; width: 0px; visibility: hidden;" value="${limit_increase.data.Oday}" />
											</div>
											<!-- 驗證用的input -->
											<input id="odate" name="odate" type="text" value="${limit_increase.data.Oday}" class="text-input validate[required,funcCall[validate_CheckDateScope['sFieldName', odate, CMSDATE, CMEDATE, true, null, null]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px; visibility: hidden;" />
											<span class="input-unit m-0">（<spring:message code='LB.D1608' />）</span>
										</div>
										
										<div class="ttb-input">
											<label class="radio-block"><spring:message code='LB.D1609' />
												<input type="radio" name="FGTXSTATUS" id="PERMANENT" value="2" onclick="openinput()" />
												<span class="ttb-radio"></span>
											</label>										
										</div>
									</span>
								</div>
							</div>
							<!-- 取消 -->
							<input type="button" name="CMBACK" id="CMBACK" value="<spring:message code="LB.X0318" />" class="ttb-button btn-flat-gray"/>
							<input type="button" id="CMSUBMIT" value="<spring:message code="LB.X0080" />" class="ttb-button btn-flat-orange"/><!-- 下一步 -->
						</div>
					</div>
				</form>
			</section>
		</main>
	</div>
	
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>