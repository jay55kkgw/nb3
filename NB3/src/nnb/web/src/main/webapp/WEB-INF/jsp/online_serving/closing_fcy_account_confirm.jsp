<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
    <%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>   
    <script type="text/javascript">
    $(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 初始化驗證碼
		setTimeout("initKapImg()", 200);
		// 生成驗證碼
		setTimeout("newKapImg()", 300);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
		
    });
    
 // 畫面初始化
	function init() {
	 
		// 表單驗證初始化
		$("#formId").validationEngine({binded: false,promptPosition: "inline"});
		// 確認鍵 click
		goOn();
		//交易類別change 事件
		 changeFgtxway();
	}
    	
 // 確認鍵 Click
	function goOn() {
		 // form Submit 
        $("#CMSUBMIT").click(function() {
        	//送出進表單驗證前將span顯示
    		$("#hideblock").show();
    		console.log("submit~~");
            if (!$('#formId').validationEngine('validate')) {
                e.preventDefault();
            } else {
                $("#formId").validationEngine('detach');
                processQuery();
            }
        });
	}
	// 交易機制選項
	function processQuery() {
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
		console.log("fgtxway: " + fgtxway);
		switch (fgtxway) {
			// IKEY
			case '1':
				useIKey();
				break;
			// 晶片金融卡
			case '2':
				var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
				useCardReader(capUri);
				break;
			default:
				//alert("<spring:message code="LB.Alert001" />");
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert001' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
		}
	}
	
	//交易類別change 事件
	function changeFgtxway() {
		$('input[type=radio][name=FGTXWAY]').change(function () {
			console.log(this.value);
			 if (this.value == '1') {
				// 若非晶片金融卡則隱藏驗證碼欄位
				$("#chaBlock").hide();
			} else if (this.value == '2') {
				// 若為晶片金融卡才顯示驗證碼欄位
				$("#chaBlock").show();
			}
		});
	}
	// 驗證碼刷新
	function changeCode() {
		$('input[name="capCode"]').val('');
		// 大小版驗證碼用同一個
		console.log("changeCode...");
		$('img[name="kaptchaImage"]').hide().attr(
			'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

		// 登入失敗解遮罩
		unBlockUI(initBlockId);
	}

	// 初始化驗證碼
	function initKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
	}

	// 生成驗證碼
	function newKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').click(function () {
			$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
		});
	}
	

</script>
</head>
<body>
	<!-- 交易機制所需畫面    -->
	<%@ include file="../component/trading_component.jsp"%>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 外匯存款帳戶結清銷戶申請    -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0299" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.X0299" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form method="post" id="formId" action="${__ctx}/ACCOUNT/CLOSING/closing_fcy_account_result">
                <c:set var="BaseResultData" value="${closing_fcy_account_confirm.data}"></c:set>
                	<input type="hidden" id="previousPageJson" name="previousPageJson" value='${BaseResultData.previousPageJson}'>
                	 <%--  TXTOKEN  防止重送代碼--%>
                    <input type="hidden" name="TXTOKEN" value="${BaseResultData.TXTOKEN}" />
                    <input type="hidden" name="ATRAMT" value="${BaseResultData.ATRAMT}" />
                	<%-- 驗證相關 --%>
					<input type="hidden" id="jsondc" name="jsondc" value='${BaseResultData.jsondc}'>
					<input type="hidden" id="ISSUER" name="ISSUER" value="">
					<input type="hidden" id="ACNNO" name="ACNNO" value="">
					<input type="hidden" id="TRMID" name="TRMID" value="">
					<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
					<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
					<input type="hidden" id="TAC" name="TAC" value="">
					<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">      
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
                             <div class="ttb-message">
                                <span><spring:message code="LB.D0464" /></span>
                            </div>
                            <!--轉出帳號-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.Payers_account_no" /></h4>
                                    </label></span>
                                <span class="input-block">
                                    <div class="ttb-input">
										<span>
										${BaseResultData.FYACN}
										</span>
									</div>
                                </span>		
                            </div>
							<!--轉出幣別-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                	<label>
                                        <h4><spring:message code="LB.Currency_o" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                   <span>
                                   ${BaseResultData.CRY}
                                   </span>
                                    </div>
                                </span>
                            </div>
							<!--轉出金額-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                	<label>
                                        <h4><spring:message code="LB.Deducted" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                   <span>${BaseResultData.display_AMTDHID}</span>
                                    </div>
                                </span>
                            </div>
							<!--轉入帳號-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.Payees_account_no" /></h4>
                                    </label></span>
                                <span class="input-block">
                                    <div class="ttb-input">
										<span>${BaseResultData.TSFAN}</span>
									</div>
                                </span>		
                            </div>
							<!--付匯款幣別-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.D0472" /></h4>
                                    </label></span>
                                <span class="input-block">
                                    <div class="ttb-input">
										<span>${BaseResultData.CURCODE}</span>
									</div>
                                </span>		
                            </div>
							<!--使用匯率-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.D0473" /></h4>
                                    </label></span>
                                <span class="input-block">
                                    <div class="ttb-input">
										<span>${BaseResultData.display_ROE}</span>
									</div>
                                </span>		
                            </div>
							<!--利息-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.Interest" /></h4>
                                    </label></span>
                                <span class="input-block">
                                    <div class="ttb-input">
										<span>${BaseResultData.display_INT}</span>
									</div>
                                </span>		
                            </div>
							
							<!--稅款-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.D0455" /></h4>
                                    </label></span>
                                <span class="input-block">
                                    <div class="ttb-input">
										<span>${BaseResultData.display_FYTAX}</span>
									</div>
                                </span>		
                            </div>
							
							<!--外幣補充保費-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.D0476" /></h4>
                                    </label></span>
                                <span class="input-block">
                                    <div class="ttb-input">
										<span>${BaseResultData.display_FYNHITAX}</span>
									</div>
                                </span>		
                            </div>
							
							 <!-- 交易機制 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.Transaction_security_mechanism" />
									</label>
								</span>
								<span class="input-block">
									<!-- 使用者是否可以使用IKEY -->
									<c:if test="${sessionScope.isikeyuser}">
										<!--電子簽章(請載入載具i-key) -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Electronic_signature" />
												<input type="radio" name="FGTXWAY" id="CMIKEY" value="1" />
												<span class="ttb-radio"></span>
											</label>
										</div>
									</c:if>
									<!-- 晶片金融卡 -->
									<div class="ttb-input">
										<label class="radio-block">
											<spring:message code="LB.Financial_debit_card" />
											<input type="radio" name="FGTXWAY" id="CMCARD" value="2" />
											<span class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>
							<!-- 交易機制區塊 END -->
							<!-- 驗證碼-->
							<div class="ttb-input-item row" id="chaBlock" style="display:none">
								<span class="input-title">
									<label>
										<!-- 驗證碼 -->
										<spring:message code="LB.Captcha" />
									</label>
								</span>
								<span class="input-block">
									<spring:message code="LB.Captcha" var="labelCapCode" />
									<input id="capCode" name="capCode" type="text" class="text-input" placeholder="${labelCapCode}" maxlength="6" autocomplete="off">
									<img name="kaptchaImage" class="verification-img" src="" />
									<button class="ttb-sm-btn btn-flat-orange" type="button" name="reshow" onclick="changeCode()">
										<spring:message code="LB.Regeneration" />
									</button>
								</span>
							</div>
							<!-- 驗證碼 END-->
						</div>
						  <input class="ttb-button btn-flat-orange" type="button" name="CMSUBMIT" id="CMSUBMIT" value="<spring:message code="LB.X0302" />" />
						</div>
                    </div>
              </form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>