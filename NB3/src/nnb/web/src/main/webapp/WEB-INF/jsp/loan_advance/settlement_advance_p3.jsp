<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
	    <script type="text/javascript">
        $(document).ready(function () {
        	init();
        });

        function init() {
	    	$("#pageshow").click(function(e){
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
					$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
					$("#formId").validationEngine('detach');
					$("#CUSIDN2").prop("disabled",false);
		        	initBlockUI();
					var action = '${__ctx}/LOAN/ADVANCE/settlement_advance_result';
					$("form").attr("action", action);
	    			$("form").submit();
				}
			});
			$("#CMRESET").click(function(e){
				var action = '${__ctx}/LOAN/ADVANCE/settlement_advance_p2';
				$("form").attr("action", action);
    			$("form").submit();
			});
        }
    </script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 貸款服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Loan_Service" /></li>
    <!-- 借款查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Loan_Inquiry" /></li>
    <!-- 借款明細查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Loan_Detail_Inquiry" /></li>
    <!-- 貸款提前結清     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1721" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!-- 預約黃金交易查詢/取消 -->
					<!-- <spring:message code="LB.NTD_Demand_Deposit_Detail" /> -->
					<spring:message code="LB.W1721" /><!-- 貸款提前結清-->
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 交易流程階段 -->
				<div id="step-bar">
					<ul>
						<li class="finished"><spring:message code="LB.Enter_data" /></li>
						<li class="active"><spring:message code="LB.Confirm_data" /></li>
						<li class=""><spring:message code="LB.Transaction_complete" /></li>
					</ul>
				</div>
                <form id="formId" method="post">
   					<input type="hidden" id="PAYDATE" name="PAYDATE" value="${inputData.PAYDATE}">
   					<input type="hidden" id="DATADATE" name="DATADATE" value="${inputData.DATADATE}">
   					<input type="hidden" id="ACN_OUT" name="ACN_OUT" value="${inputData.ACN_OUT}">
					<input type="hidden" id="PALPAY" name="PALPAY" value="${inputData.PALPAY}" />
					<input type="hidden" id="CMTRMEMO" name="CMTRMEMO" value="${inputData.CMTRMEMO}" />
					<input type="hidden" id="CMTRMAIL" name="CMTRMAIL" value="${inputData.CMTRMAIL}" />
					<input type="hidden" id="CMMAILMEMO" name="CMMAILMEMO" value="${inputData.CMMAILMEMO}" />
					<input type="hidden" id="ACN" name="ACN" value="${inputData.ACN}" />
					<input type="hidden" id="LoanACN" name="LoanACN" value="${inputData.ACN}" />
					<input type="hidden" id="SEQ" name="SEQ" value="${inputData.SEQ}" />
					<input type="hidden" id="LoanSEQ" name="LoanSEQ" value="${inputData.SEQ}" />
   					<input type="hidden" id="showLoanAMT" name="showLoanAMT" value="${inputData.showLoanAMT}">
					<input type="hidden" name="PINNEW" id="PINNEW" value="">
    				<input type="hidden" name="CHKPAGE" value="30031_2">
    				<input type="hidden" name="ADOPID" value="N3003">
   					<input type="hidden" id="DPMYEMAIL" name="DPMYEMAIL" value="${inputData.DPMYEMAIL}">
					<input type="hidden" id="TOKEN" name="TOKEN" value="${inputData.TOKEN}" /><!-- 防止重複交易 -->
   					<input type="hidden" id="FGTXDATE" name="FGTXDATE" value="${inputData.FGTXDATE}">
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
							<div class="NA01_3">
								<div class="head-line"><spring:message code="LB.Confirm_transfer_data" /><!-- 請確認轉帳資料 --></div>
							</div>
							<div class="ttb-input-block">
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.Transfer_date" /><!-- 轉帳日期 -->
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${inputData.PAYDATE}
										</div>
									</span>
								</div>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.Payers_account_no" /><!-- 轉出帳號 -->
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${inputData.ACN_OUT}
										</div>
									</span>
								</div>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.W0891" /><!-- 貸款帳號 -->
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${inputData.ACN}
										</div>
									</span>
								</div>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.W1707" /><!-- 貸款分號 -->
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											${inputData.SEQ}
										</div>
									</span>
								</div>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.Amount" /><!-- 轉帳金額 -->
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<spring:message code="LB.NTD" /><!-- 新臺幣 --> ${inputData.PALPAY_str} <spring:message code="LB.Dollar_1" /><!-- 元 -->
										</div>
									</span>
								</div>
<!-- 								********** -->
<!-- 								<div class="ttb-input-item row"> -->
<!-- 									<span class="input-title">  -->
<!-- 									<label> -->
<!-- 										<h4> -->
<!-- 											交易備註 -->
<!-- 										</h4> -->
<!-- 									</label> -->
<!-- 									</span>  -->
<!-- 									<span class="input-block"> -->
<!-- 										<div class="ttb-input"> -->
<!-- 											<input type="text" id="CMTRMEMO" name="CMTRMEMO" class="text-input validate[required]" size="56" maxlength="20"/> -->
<!-- 										</div> -->
<!-- 									</span> -->
<!-- 								</div> -->
<!-- 								********** -->
<!-- 								<div class="ttb-input-item row"> -->
<!-- 									<span class="input-title">  -->
<!-- 									<label> -->
<!-- 										<h4> -->
<!-- 											Email通知<br />(可不填) -->
<!-- 										</h4> -->
<!-- 									</label> -->
<!-- 									</span>  -->
<!-- 									<span class="input-block"> -->
<!-- 										<div class="ttb-input"> -->
<%-- 											<c:if test="${ !inputData.DPMYEMAIL.equals('') && inputData.sendMe.equals('Y') }"> --%>
<%-- 												通知本人：${ inputData.DPMYEMAIL }<br /> --%>
<!-- 			　									另通知： -->
<%-- 											</c:if> --%>
<%-- 											<c:if test="${ inputData.DPMYEMAIL.equals('') || inputData.sendMe.equals('N') }"> --%>
<!-- 												電子信箱： -->
<%-- 											</c:if> --%>
<!-- 											<input type="text" id="CMTRMAIL" name="CMTRMAIL" class="text-input validate[required]" size="40" maxlength="500"/> -->
<%-- 											<input type="button" value="通訊錄" name="CMADDRESS"  onClick="window.open('${__ctx}/LOAN/ADVANCE/AddressBook')"> --%>
<!-- 											<br/>摘要內容 : -->
<!-- 											<input type="text" id="CMMAILMEMO" name="CMMAILMEMO" class="text-input validate[required]" size="56" maxlength="20"/> -->
<!-- 											<input type="button" value="同交易備註" name="CMCOPY"  onClick="copy();"> -->
<!-- 										</div> -->
<!-- 									</span> -->
<!-- 								</div> -->
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<input type="radio" name="FGTXWAY" id="CMSSL" value="0" checked> <spring:message code="LB.SSL_password"/>&nbsp; 
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input type="password" name = "CMPASSWORD" id="CMPASSWORD"  maxlength="8" class="text-input validate[required, pwvd[PW, CMPASSWORD], custom[onlyLetterNumber]]">
										</div>
									</span>
								</div>
							</div>
                           
	                        <!--button 區域 -->
	                        <div>
								<!-- 重新輸入 -->
								<spring:message code="LB.Re_enter" var="cmRest"></spring:message>
								<input type="button" name="CMRESET" id="CMRESET" value="${cmRest}" class="ttb-button btn-flat-gray">
	                            <input id="pageshow" name="pageshow" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm"/>" />
	                        </div>
                        <!--                     button 區域 -->
                        </div>  
                    </div>
                </form>
	
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>