<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/checkutil.js"></script>
	
	<script type="text/javascript">
	$(document).ready(function(){
		$("#printbtn").click(function(){
			var i18n = new Object();
			i18n['jspTitle']='<spring:message code="LB.Historical_Statement_Detail_Inquiry" />';
			var params = {
				"jspTemplateName":"history_email_print",
				"jspTitle":i18n['jspTitle'],
				"CMQTIME":"${history_email.data.CMQTIME}",
				"CUSIDN":"${history_email.data.CUSIDN}",
				"DPMYEMAIL":"${sessionScope.dpmyemail}",
				"CUSIDNLength":"${history_email.data.CUSIDNLength}",
				
			}
			openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
		
		});
		
		//上一頁按鈕
		$("#previous").click(function() {
			initBlockUI();
			$("form").attr("action","${__ctx}/CREDIT/INQUIRY/card_history")
			$("form").submit();
		});
	});
		
		
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 信用卡     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Credit_Card" /></li>
    <!-- 帳務查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Inquiry_Service" /></li>
    <!-- 歷史帳單明細查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Historical_Statement_Detail_Inquiry" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<spring:message code="LB.Historical_Statement_Detail_Inquiry" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" action="" method="post">
				<input type="hidden" name="FGPERIOD" value="${history_email.data.FGPERIOD}">
				<input type="hidden" name="CARDTYPE" value="${history_email.data.CARDTYPE}">
				<input type="hidden" name="BILL_NO" value="${history_email.data.BILL_NO}">
				<input type="hidden" id="QUERYTYPE" name="QUERYTYPE" value="${history_email.data.QUERYTYPE}">
				<input type="hidden" id="FUNCTION" name="FUNCTION" value="${history_email.data.FUNCTION}"/>
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
							<h4 style="margin-top:10px;color: red;font-weight:bold;">
								<spring:message code= "LB.X0885" />
							</h4>
							<div class="ttb-input-block">
								<!--查詢時間區塊 -->
								<div class="ttb-input-item row">
									<!--查詢時間  -->
									<span class="input-title">
										<label>
											<spring:message code="LB.Inquiry_time" />
										</label>
									</span>
									<span class="input-block">
										${history_email.data.CMQTIME}
									</span>
								</div>
								<!-- 身分證字號/統一編號區塊 -->
								<div class="ttb-input-item row">
									<!--身分證字號/統一編號  -->
									<span class="input-title">
										<label>
											<spring:message code="LB.Id_no" />
										</label>
									</span>
									<span class="input-block">
										${history_email.data.CUSIDN}
									</span>
								</div>
								<!-- 電子郵箱區塊 -->
								<div class="ttb-input-item row">
									<!--電子郵箱	  -->
									<span class="input-title">
										<label>
											<spring:message code="LB.Mail_address" />	
										</label>
									</span>
									<span class="input-block">
										${history_email.data.EMAIL}
									</span>
								</div>
								<!-- 電子帳單密碼區塊 -->
								<div class="ttb-input-item row">
									<!--電子帳單密碼  -->
									<span class="input-title">
										<label>
											<spring:message code="LB.E-statement_password" />
										</label>
									</span>
									
									<span class="input-block" >
									<c:if test="${history_email.data.CUSIDNLength == '10'}">
										<font style="color: red;font-weight: bold;"><spring:message code="LB.Id_or_16" /></font>
									</c:if>
									<c:if test="${history_email.data.CUSIDNLength != '10'}">
										<font style="color: red;font-weight: bold;"><spring:message code="LB.ENI_or_16" /></font>
									</c:if>
									</span>
								</div>
							</div>
								<input type="button" id="previous" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />" />	
								<!--列印 -->
								<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>						
						</div>
					</div>
					<ol class="description-list list-decimal">
						<!-- 		說明： -->
						<p><spring:message code="LB.Remind_you"/></p>
							<li><spring:message code="LB.History_P23_D1" /></li>
					</ol>
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
      
</body>

</html>