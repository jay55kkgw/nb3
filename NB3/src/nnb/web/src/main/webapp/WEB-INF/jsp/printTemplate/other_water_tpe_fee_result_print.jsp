<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function(){
			window.print();
		});
	</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br/><br/>
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
	<br/><br/><br/>
	<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
	<br/><br/><br/>
	
	<div> 
		<!-- 資料 -->
		<!-- 表格區塊 -->
		<div class="ttb-input-block">
			

			<table class="print">
				<tr>
					<td style="text-align: center"><spring:message code= "LB.D1097" /></td>
					<td>${CMQTIME}</td>
				</tr>
				<c:if test="${ R1.equals('V1') }">
					<tr>
						<td style="text-align: center"><spring:message code= "LB.D0168" /></td>
						<td>${TSFACN}</td>
					</tr>
				</c:if>
				<c:if test="${ R1.equals('V2') }">
					<tr>
						<td style="text-align: center"><spring:message code= "LB.W0639" /></td>
						<td>${CARDNUM}</td>
					</tr>
				</c:if>
				<tr>
					<td style="text-align: center">
						<spring:message code= "LB.W0691" />
					</td>
					<td>${CUSNUM}</td>
				</tr>
			</table>
		</div>
		
		<br>
		<br>
		<div class="text-left">
			<spring:message code="LB.Description_of_page" />
			<ol class="list-decimal text-left">
	            <li><spring:message code="LB.Other_Water_Tpe_Fee_P3_D1" /></li>
            	   	<li><spring:message code="LB.Other_Water_Tpe_Fee_P3_D2" /></li>
                    <li><spring:message code="LB.Other_Water_Tpe_Fee_P3_D3" /></li>
			</ol>
		</div>
	</div>
	
</body>
</html>