<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>

	<script type="text/javascript">
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		
		// 畫面初始化
		function init() {
			$("#CMSUBMIT").click( function(e) {
    			$("#formId").attr("action", "${__ctx}/login");
				console.log("submit~~");
	         	initBlockUI();
	            $("#formId").submit();
			});
			$("#back").click( function(e) {
    			$("#formId").attr("action", "${__ctx}/ONLINE/APPLY/online_apply_menu");
				console.log("submit~~");
	         	initBlockUI();
	            $("#formId").submit();
			});
		}
		
	</script>
</head>

<body>
	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!-- 簽署信用卡分期付款同意書 -->
					<spring:message code= "LB.B0005" />
				</h2>
				<form method="post" id="formId" name="formId">
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
						
							<div class="ttb-input-block">
								<div class="ttb-input">
							
									<div class="text-left">
										<spring:message code= "LB.D1322" />：
										<ol class="list-decimal text-left">
											<li><spring:message code= "LB.D1323" /></li>            
											<li><spring:message code= "LB.D1324" /></li>
										</ol>
									</div>
								</div>
							</div>
							<!-- 確定 -->
							<input type="button" id="CMSUBMIT" value="<spring:message code= "LB.X1087" />" class="ttb-button btn-flat-orange" />
							<!-- 確定 -->
							<input type="button" id="back" value="<spring:message code= "LB.Cancel" />" class="ttb-button btn-flat-gray" />
						</div>
					</div>
						
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>