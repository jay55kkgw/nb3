<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
	<!-- 讀卡機所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/VAckisIEbrower.js"></script>
	<script type="text/javascript" src="${__ctx}/component/util/Pkcs11ATLAgent.js"></script>
	
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/checkIdProcess.js"></script>

	<script type="text/javascript">
	var notCheckIKeyUser = true; // 不驗證是否是IKey使用者
	var myobj = null; // 自然人憑證用
	var urihost = "${__ctx}";
	$(document).ready(function() {
		// HTML載入完成後0.1秒開始遮罩
		setTimeout("initBlockUI()", 100);

		// 初始化驗證碼
		setTimeout("initKapImg()", 200);
		// 生成驗證碼
		setTimeout("newKapImg()", 300);
		
		initFootable(); // 將.table變更為footable 
		init();
		
		// 過0.5秒解遮罩
		setTimeout("unBlockUI(initBlockId)", 800);
	});
	
	/**
	 * 初始化BlockUI
	 */
	function initBlockUI() {
		initBlockId = blockUI();
	}

	// 交易機制選項
	function processQuery(){
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
		console.log("fgtxway: " + fgtxway);
	
		switch(fgtxway) {
			case '0':
//					alert("交易密碼(SSL)...");
    			$('#formId').submit();
				break;
				
			case '1':
//					alert("IKey...");
				var jsondc = $("#jsondc").val();
				
				// 遮罩後不給捲動
//					document.body.style.overflow = "hidden";

				// 呼叫IKEY元件
//					uiSignForPKCS7(jsondc);
				
				// 解遮罩後給捲動
//					document.body.style.overflow = 'auto';
				
				useIKey();
				break;
				
			case '2':
//					alert("晶片金融卡");

				// 遮罩後不給捲動
//					document.body.style.overflow = "hidden";
				
				// 呼叫讀卡機元件
				useCardReader();

				// 解遮罩後給捲動
//					document.body.style.overflow = 'auto';
				
		    	break;
		    	
			case '4':
//				自然人憑證
				$('#UID').val($('#CUSIDN').val());
				useNatural();
		    	break;
		    	
			default:
				//alert("nothing...");
				errorBlock(
						null, 
						null,
						["nothing..."], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
		}
		
	}

    function init(){
		$("#formId").validationEngine({binded: false,promptPosition: "inline"});
    	$("#CMSUBMIT").click(function(e){			
			console.log("submit~~");
			$("#CUSIDN").val($("#CUSIDN").val().toUpperCase());
	
			if (!$('#formId').validationEngine('validate')){
	        	e.preventDefault();
 			}else{
				$('#HLOGINPIN').val(pin_encrypt($('#LOGINPIN').val()));
				$("#UID").val($("#CUSIDN").val().toUpperCase());
 				console.log("submit~~");
 				$("#formId").validationEngine('detach');
 				initBlockUI();
   				var action = '${__ctx}/RESET/ssl_reset_result';
   				$('#formId').attr("action", action);
    			unBlockUI(initBlockId);
    			processQuery();
    			$('#LOGINPIN').val("");
    			$('#CKLOGINPIN').val("");
    			$("#formId").validationEngine({binded: false,promptPosition: "inline"});
 			}
			
		});
    }	
    
    
	/**************************************/
	/*          	驗證碼驗證   		          */
	/**************************************/
	// 刷新驗證碼
	function refreshCapCode() {
		console.log("refreshCapCode...");

		// 驗證碼
		$('input[name="capCode"]').val('');
		
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

		// 登入失敗解遮罩
		unBlockUI(initBlockId);
	}
	
	// 刷新輸入欄位
	function changeCode() {
		console.log("changeCode...");
		
		// 清空輸入欄位
		$('input[name="capCode"]').val('');
		
		// 刷新驗證碼
		refreshCapCode();
	}

	// 初始化驗證碼
	function initKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
	}

	// 生成驗證碼
	function newKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').click(function() {
			refreshCapCode();
		});
	}
		
	function hideNpcDiv(flag){
		console.log(flag);
		if(flag == 'Y'){
			$("#NPCDIV").show();
		}else{
			$("#NPCDIV").hide();
		}
	}
	
	/**************************************/
	/*          複寫checkIdProcess         */
	/**************************************/
	//取得卡片主帳號結束
	function getMainAccountFinish(result){
		//成功
		if(result != "false"){
			var cardACN = result;
			$("#ACNNO").val(cardACN);
			if(cardACN.length > 11){
				cardACN = cardACN.substr(cardACN.length - 11);
			}
			var UID = $("#CUSIDN").val();
//			var x = { method: 'get', parameters: '', onComplete: CheckIdResult };	
//			var myAjax = new Ajax.Request( "simpleform?trancode=N953&TXID=N953&ACN=" + cardACN, x);
			
			var uri = urihost+"/COMPONENT/component_without_id_aj";
			var rdata = { ACN: cardACN ,UID: UID };
			fstop.getServerDataEx(uri, rdata, true, CheckIdResult);
		}
		//失敗
		else{
			showTempMessage(500,"<spring:message code= "LB.X1250" />","","MaskArea",false);
		}
	}
	//卡片押碼結束
	function generateTACFinish(result){
		//成功
		if(result != "false"){
			var TACData = result.split(",");
			
			var main = document.getElementById("formId");
			main.iSeqNo.value = TACData[1];
			main.ICSEQ.value = TACData[1];
			main.TAC.value = TACData[2];

			var ACN_Str1 = main.ACNNO.value;
			main.CHIP_ACN.value = ACN_Str1;
			main.OUTACN.value = ACN_Str1;
			main.ACNNO.value = ACN_Str1;
			main.submit();
		}
		//失敗
		else{
			FinalSendout("MaskArea",false);
		}
	}
	

	/**************************************/
	/*     			 發送簡訊   			      */
	/**************************************/
	var sec = 120;
	var the_Timeout;

	function countDown()
	{
	     var main = document.getElementById("formId");
	                
	      var counter = document.getElementById("CountDown");                      
	       counter.innerHTML = sec;
	       sec--;
	       
	       if (sec == -1) 
	       {              		
				$("#getotp").prop("disabled",false);
				$("#getotp").removeClass("btn-flat-gray");
				$("#getotp").addClass("btn-flat-orange");
			   sec =120;

	           return;                
	       }                                     
	           
	       //網頁倒數計時(120秒)              
	       the_Timeout = setTimeout("countDown()", 1000);    
	} 
	    //取得otp並發送簡訊
	    function getsmsotp() {
			var main = document.getElementById("formId");
	 		var cusidn = $("#CUSIDN").val().toUpperCase();
			if(cusidn.length==0)
		    {
		    	//alert("<spring:message code= "LB.D0025" />");
		    	errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.D0025' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
		    	main.CUSIDN.focus();
		    	return false;
		    }				
			$("#getotp").prop("disabled",true);
			$("#getotp").removeClass("btn-flat-orange");
			$("#getotp").addClass("btn-flat-gray");
			sec = 120;
			countDown();
			$.ajax({
		        type :"POST",
		        url  : "${__ctx}/RESET/smsotp_ajax",
		        data : { 
		        	CUSIDN: cusidn,
		        	ADOPID: 'NA721'
		        },
		        async: false ,
		        dataType: "json",
		        success : function(msg) {
		        	callbackgetsmsotp(msg)
		        }
		    })
		}
		function callbackgetsmsotp(response) {
			//alert(response.responseText);
		    var main = document.getElementById("formId");
			//eval("var result = " + response.responseText);
			console.log(response);
			var msgCode = response[0].MSGCOD;
			var msgName = response[0].MSGSTR;
			var phone = response[0].phone;
			if(msgCode=="0000")
			{	
				phone = phone = phone.substring(0,6)+"***"+phone.substring(9);
			    //alert("<spring:message code= "LB.Alert198" />");
			    errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.X2280' />"+phone+"<br/><spring:message code= 'LB.X2281' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
			    main.OTP.focus();			
			}
			else
			{
				sec = 0;
	      		var counter = document.getElementById("CountDown");                      
	       		counter.innerHTML = sec;		
				$("#getotp").prop("disabled",false);
				$("#getotp").removeClass("btn-flat-gray");
				$("#getotp").addClass("btn-flat-orange");
// 				if((ResultStr.trim()).length>6)
// 				{
// 					var msgstr = (ResultStr.trim()).substring(ResultStr.indexOf("==")+2);
// 					alert((ResultStr.trim()).substring(0,ResultStr.indexOf("=="))+"："+msgstr);
// 				}
				//alert("<spring:message code= "LB.error_code" />：" + msgCode + " " + msgName);
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.error_code' />" + ":" + msgCode + " " + msgName], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			}				
		}
	</script>
</head>
<body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	<!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
	<!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 重設交易密碼    -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X1579" /></li>
		</ol>
	</nav>
	<!-- 自然人憑證元件載點 -->
	<a href="${__ctx}/component/windows/natural/TBBankPkcs11ATL.exe" id="naturalComponent" target="_blank" style="display:none"></a>
	
	<!-- menu、登出窗格 -->
	<div class="content row">
		
		<c:if test="${sessionScope.cusidn != null}">
			<%@ include file="../index/menu.jsp"%>
		</c:if>
		
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<spring:message code= "LB.X1579" />
				</h2>
				<form method="post" id="formId">
					<input type="hidden" name="ADOPID" id="ADOPID" value="NA721"/>
					<input type="hidden" name="HLOGINPIN" id="HLOGINPIN" value="">
					<input type="hidden" name="CHIP_ACN" id="CHIP_ACN" value="">
					<input type="hidden" name="UID" id="UID" value="">
					<input type="hidden" name="ACN" id="ACN" value="">
					<input type="hidden" name="ISSUER" id="ISSUER" value="">
					<input type="hidden" name="ACNNO" id="ACNNO" value="">
					<input type="hidden" name="OUTACN" id="OUTACN" value="">
					<input type="hidden" name="iSeqNo" id="iSeqNo" value="">
					<input type="hidden" name="ICSEQ" id="ICSEQ" value="">
					<input type="hidden" name="TAC" id="TAC" value="">
					<input type="hidden" name="TRMID" id="TRMID" value="">
					<input type="hidden" name="TRANSEQ" id="TRANSEQ" value="2500">
					<input type="hidden" name="IP" value="${result_data.data.IP}">
					<input type="hidden" name="TXTOKEN" id="TXTOKEN" value="${result_data.data.TXTOKEN}">
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
							<c:if test="${sessionScope.cusidn == null}">
								<div class="ttb-input-block">
		                            <div class="ttb-input-item row">
		                                <span class="input-title">
		                                    <label>
		                                        <h4><spring:message code= "LB.D0025" /></h4>
		                                    </label>
		                                </span>
		                                <span class="input-block">
		                                    <div class="ttb-input">
		                                          <input class="text-input validate[required,funcCall[validate_checkSYS_IDNO[CUSIDN]]" type="text" name="CUSIDN" id="CUSIDN" maxlength="10" size="11">
		                                    </div>
		                                </span>
		                            </div>
	                            </div>
                            </c:if>
							<c:if test="${sessionScope.cusidn != null}">
								<div class="ttb-input-block">
		                            <div class="ttb-input-item row">
		                                <span class="input-title">
		                                    <label>
		                                        <h4><spring:message code= "LB.D0025" /></h4>
		                                    </label>
		                                </span>
		                                <span class="input-block">
		                                    <div class="ttb-input">
		                                          <input class="text-input validate[required,funcCall[validate_checkSYS_IDNO[CUSIDN]]" type="text" name="CUSIDN" id="CUSIDN" maxlength="10" size="11" disabled value="${result_data.data.cusidn}">
		                                    </div>
		                                </span>
		                            </div>
	                            </div>
                            </c:if>
							<div class="ttb-input-block">
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                    <label>
	                                        <h4><spring:message code= "LB.SSL_password_1" /></h4>
	                                    </label>
	                                </span>
	                                <span class="input-block">
	                                    <div class="ttb-input">
	                                          <input class="text-input validate[required,funcCall[validate_CheckPwd['<spring:message code= "LB.SSL_password_1" />',LOGINPIN]]]" type="password" name="LOGINPIN" id="LOGINPIN" maxlength="8" size="10" autocomplete="off">
	                                    </div>
	                                </span>
	                            </div>
                            </div>
                            <div class="ttb-input-block">
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                    <label>
	                                        <h4><spring:message code="LB.D1011" /></h4>
	                                    </label>
	                                </span>
	                                <span class="input-block">
	                                    <div class="ttb-input">
	                                      	<input type="password" maxLength="8" autocomplete="off" class="text-input validate[required,funcCall[validate_CheckPwd['<spring:message code= "LB.D1011" />',CKLOGINPIN]],funcCall[validate_CheckSameInput['<spring:message code= "LB.D1011" />',LOGINPIN,CKLOGINPIN]]]" id="CKLOGINPIN" value="" >
	                                    </div>
	                                </span>
	                            </div>
                            </div>
                            
							<div class="ttb-input-block">
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                    <label>
	                                        <h4><spring:message code= "LB.X1574" /></h4>
	                                    </label>
	                                </span>
	                                <span class="input-block">
	                                    <div class="ttb-input">
	                                        <input class="text-input validate[required,funcCall[validate_CheckLenEqual['<spring:message code= "LB.Captcha" />',OTP,false,8,8]],funcCall[validate_CheckNumber['<spring:message code= "LB.Captcha" />',OTP]]]" type="text" name="OTP" id="OTP" maxlength="8" size="10">
	                                        <br>&nbsp;<spring:message code= "LB.X1575" />： <font id="CountDown" color="#FF0000"></font><spring:message code= "LB.Second" />
											<div class="login-input-block">
												<input type="button" name="getotp" id="getotp" class="ttb-sm-btn btn-flat-orange" onclick="getsmsotp()" value="<spring:message code= "LB.X1576" />" />
											</div>
	                                    </div>
	                                </span>
	                            </div>
                            </div>
							
							
							<!--交易機制-->
							<div class="ttb-input-block">
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code="LB.Transaction_security_mechanism" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<!-- 晶片金融卡 -->
										<div class="ttb-input" onclick="hideNpcDiv('N')">
											<label class="radio-block">
												<spring:message code="LB.Financial_debit_card" />
												
											 	<input type="radio" name="FGTXWAY" id="CMCARD" value="2" checked="checked" >
													
												<span class="ttb-radio"></span>
											</label>
										</div>
										
									</span>
								</div>
							</div>
							
							<!-- 確認新使用者名稱 -->
							<div class="ttb-input-block">
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4>
												<spring:message code= "LB.Captcha" />
											</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											
											<spring:message code="LB.Captcha" var="labelCapCode" />
											<input id="capCode" type="text" class="text-input input-width-125"
												name="capCode" placeholder="${labelCapCode}" maxlength="6" autocomplete="off">
											<img name="kaptchaImage" class="verification-img" src="" />
											<input type="button" name="reshow" class="ttb-sm-btn btn-flat-gray" onclick="refreshCapCode()" value="<spring:message code= "LB.Regeneration" />" />
											<span class="input-remarks"><spring:message code= "LB.D0033" /></span>
										</div>
									</span>
								</div>
							</div>
							
							<!-- 確定 -->
							<input type="button" id="CMSUBMIT" value="<spring:message code= "LB.Confirm" />" class="ttb-button btn-flat-orange"/>
						</div>
					</div>
						
				</form>
				<div class="text-left">
				    <!-- 		說明： -->
					<spring:message code="LB.Description_of_page"/>
				    <ol class="list-decimal text-left">
				        <li><spring:message code= "LB.Ssl_Reset_P1_D1" /></li>
        				<li><spring:message code= "LB.Ssl_Reset_P1_D2" /></li>
        				<li><span><font color=red><B><spring:message code= "LB.Ssl_Reset_P1_D3" /></B></font></span></li>
        				<li><spring:message code= "LB.Ssl_Reset_P1_D4" /></li>
        				<li><span><font color=red><B><spring:message code= "LB.X2231" /></B></font></span></li>
				    </ol>
				</div>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>