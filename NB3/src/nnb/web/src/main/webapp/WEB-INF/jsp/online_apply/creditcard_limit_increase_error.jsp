<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>
<script type="text/javascript" src="${__ctx}/js/jquery.maskedinput.js"></script>
<!-- DIALOG會用到 -->
<script type="text/javascript" src="${__ctx}/js/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery-ui.css">

<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>

<script type="text/javascript">
	var action = '${__ctx}';
	$(document).ready(function(){
		// HTML載入完成後開始遮罩
	// 	setTimeout("initBlockUI()", 10);
		initBlockUI();
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
		
	function init(){
		$("#previous").click(function() {
			console.log("previous action>>" + action);
			action = action + '${error.previous}'
			submitForm(action);
		});
	}
	function submitForm(action) 
	{
		initBlockUI();
		$('#formId').attr("action", action);
		$('#formId').submit();
	}

</script>
</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Credit_Card_Limit_Increase" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2><spring:message code="LB.Credit_Card_Limit_Increase" /><h2>
				<form method="post" id="formId" action="${__ctx}/CREDIT/APPLY/creditcard_limit_increase_p2">
					<input type="hidden" id="FROM_NB3" name="FROM_NB3" value="${FROM_NB3}"/>
					<!-- 表單顯示區  -->
					<div class="main-content-block pl-0 pr-0 row">
						<div class="col-12">
							<div class="ttb-input-block">
								<ul class="ttb-result-list">
									<c:choose>
										<c:when test="${error.data.errType == '1'}">
											<li>
												<h5 style="text-align:left;margin-top:10px;text-al">
													<spring:message code="LB.Credit_Card_Limit_Increase_Error_1" />
												</h5>
											</li>
										</c:when>
										<c:when test="${error.data.errType == '2'}">
											<li>
												<h5 style="text-align:left;margin-top:10px;text-al">
													<spring:message code="LB.Credit_Card_Limit_Increase_Error_2" />
												</h5>
											</li>
										</c:when>
										<c:when test="${error.data.errType == '3'}">
											<li>
												<h5 style="text-align:left;margin-top:10px;text-al">
													<spring:message code="LB.Credit_Card_Limit_Increase_Error_3"/>
												</h5>
											</li>
										</c:when>
										<c:when test="${error.data.errType == '4'}">
											<li>
												<h5 style="text-align:left;margin-top:10px;text-al">
													<spring:message code="LB.Credit_Card_Limit_Increase_Error_4" />
												</h5>
											</li>
										</c:when>
										<c:when test="${error.data.errType == '5'}">
											<li>
												<h5 style="text-align:left;margin-top:10px;text-al">
													<spring:message code="LB.Credit_Card_Limit_Increase_Error_5" />
												</h5>
											</li>
										</c:when>
										<c:otherwise>
											<li>
												<h3><spring:message code="LB.Transaction_code" /></h3>
												<p>${error.msgCode}</p>
											</li>
											<li>
												<h3><spring:message code="LB.Transaction_message" /></h3>
												<p>${error.message}</p>
											</li>
										</c:otherwise>
									</c:choose>
								</ul>
							</div>
							<button type="button" id="previous" class="ttb-button btn-flat-orange"><spring:message code="LB.X1962" /></button>
						</div>
					</div>
				</form>
			</section>
		</main>
	</div>
	
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>