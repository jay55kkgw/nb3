<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>

<head>
    <%@ include file="../__import_head_tag.jsp" %>
    <%@ include file="../__import_js_u2.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.twzipcode-1.6.1.js"></script>
    <script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
    <style>
        .zipcode {
            display: none;
        }

        select.tbb-disabled,
        input.tbb-disabled {
            background-color: #d8d8d8 !important;
            border-color: #bebec0 !important;
            color: #000 !important;
            pointer-events: none !important;
            cursor: no-drop !important;
        }

        @media screen and (max-width: 767px) {
            .ttb-button {
                width: 38%;
                left: 36px;
            }

            #CMBACK.ttb-button {
                border-color: transparent;
                box-shadow: none;
                color: #333;
                background-color: #fff;
            }


        }
    </style>
    <script type="text/javascript">
        var isTimeout = "${not empty sessionScope.timeout}";
        var countdownObjheader = isTimeout == 'true' ? "${sessionScope.timeout}" : 600;
        var countdownSecHeader = isTimeout == 'true' ? "${sessionScope.timeout}" : 600;
        $(document).ready(function () {
            initFootable(); // 將.table變更為footable 
            init();
            datetimepickerEvent();
            getTmr();
            genDateList();
            setCITY();
			timeLogout();
        });

        function init() {
            // $("#formId").validationEngine({binded: false,promptPosition: "inline", scroll: false});
            $("#formId").validationEngine({
                validationEventTriggers: 'keyup blur',
                binded: true,
                scroll: true,
                addFailureCssClassToField: "isValid",
                promptPosition: "inline"
            });
            //補換發縣市 欄位資料填入
            AddCityOptions1(document.getElementById("CITYCHA"));

            if ('${result_data.data.HIRISK0}' == 'Y') {
                showHIRISK(false);
                $("#HIRISK_Y").attr("checked", true);

                $("#HIRISK1").children().each(function () {
                    if ($(this).val() == '${result_data.data.HIRISK}') {
                        $(this).prop("selected", true);
                    }
                });
            } else {
                showHIRISK(true);
                $("#HIRISK_N").attr("checked", true);
            }

            //身分證有無相片 (預設設定)
            if ('${result_data.data.HAS_PIC}' == 'N')
                $("#PIC_FLAG2").attr("checked", true);

            //更換過姓名 (預設設定)
            if ('${result_data.data.RENAME}' == 'Y')
                $("#RENAME1").attr("checked", true);
            if ('${result_data.data.RENAME}' == 'N')
                $("#RENAME2").attr("checked", true);

            //身分證為補發/換發/新發 (預設設定)
            $("#TYPECHA").children().each(function () {
                if ($(this).val() == '${result_data.data.TYPECHA}') {
                    $(this).prop("selected", true);
                }
            });
            //補換發縣市 (預設設定)
            $("#CITYCHA").children().each(function () {
                if ($(this).val() == '${result_data.data.CITYCHA}') {
                    $(this).prop("selected", true);
                }
            });
            //職業 (預設設定)
            $("#CAREER1").children().each(function () {
                if ($(this).val() == '${result_data.data.CAREER1}') {
                    $(this).prop("selected", true);
                }
            });
            //學歷 (預設設定)
            $("#DEGREE").children().each(function () {
                if ($(this).val() == '${result_data.data.DEGREE}') {
                    $(this).prop("selected", true);
                }
            });

            //婚姻狀況 (預設設定)
            if ('${result_data.data.MARRY}' == '0') {
                $("#MARRY0").attr("checked", true);
                showChild(true);
            }
            if ('${result_data.data.MARRY}' == '1') {
                $("#MARRY1").attr("checked", true);
                showChild(false);
            }
            if ('${result_data.data.MARRY}' == '2') {
                $("#MARRY2").attr("checked", true);
                showChild(false);
            }


            //子女人數 (預設設定)
            var CHILD = '${result_data.data.CHILD}' == '' ? '99' : '${result_data.data.CHILD}';
            for (var i = 0; i <= 20; i++) {
                if (CHILD == i) {
                    $("#CHILD").append("<option value='" + i + "' selected>" + i + "人</option>");
                } else {
                    $("#CHILD").append("<option value='" + i + "'>" + i + "人</option>");
                }
            }

            //職稱代號 (預設設定)
//     		$("#CAREER2").children().each(function(){
//     		    if ($(this).text()=='${result_data.data.CAREER2_str}'){
//    		        $(this).attr("selected", true);
//     		    }
//     		});

            //消除單-
            if ($("#HOMEP").val() == "-")
                $("#HOMEP").val("");
            if ($("#BUSP").val() == "-")
                $("#BUSP").val("");
            if ($("#FAXP").val() == "-")
                $("#FAXP").val("");

            $("#CMSUBMIT").click(function (e) {

                //身分證為補發/換發/新發日期 日期驗證
                $("#hideblock_CMDATE2").show();
                console.log((parseInt($("#CMDATE2YY").val()) + 1911).toString() + "/" + $("#CMDATE2MM").val() + "/" + $("#CMDATE2DD").val());
                $("#validate_CMDATE2").val((parseInt($("#CMDATE2YY").val()) + 1911).toString() + "/" + $("#CMDATE2MM").val() + "/" + $("#CMDATE2DD").val());

                //出生日期 資料整理 ，填入BIRTHDAY隱藏欄位
                $("#BIRTHDAY").val($("#CCBIRTHDATEYY").val() + $("#CCBIRTHDATEMM").val() + $("#CCBIRTHDATEDD").val());
                if ($("#CCBIRTHDATEYY").val().toString().length == 1)
                    $("#BIRTHDAY").val("00" + $("#BIRTHDAY").val());
                if ($("#CCBIRTHDATEYY").val().toString().length == 2)
                    $("#BIRTHDAY").val("0" + $("#BIRTHDAY").val());

                //身分證為補發/換發/新發 ，填入ID_CHGE隱藏欄位
                $("#ID_CHGE").val($("#TYPECHA").val());
                //身分證為補發/換發/新發日期 ，填入CHGE_DT隱藏欄位
                $("#CHGE_DT").val($("#CMDATE2YY").val() + $("#CMDATE2MM").val() + $("#CMDATE2DD").val());
                if ($("#CMDATE2YY").val().toString().length == 1)
                    $("#CHGE_DT").val("00" + $("#CHGE_DT").val());
                if ($("#CMDATE2YY").val().toString().length == 2)
                    $("#CHGE_DT").val("0" + $("#CHGE_DT").val());

                //補換發縣市 ，填入 CHGE_CY 隱藏欄位 與 CHGE_CYNAME 隱藏欄位
                $("#CHGE_CY").val($("#CITYCHA").val());
                $("#CHGE_CYNAME").val($("#CITYCHA").find(":selected").text());

                // 戶籍/通訊地址，填入 POSTCOD1 隱藏欄位 與 PMTADR 隱藏欄位
                $("#POSTCOD1").val($("#ZIP1").val());
                $("#PMTADR").val($("#CITY1").val() + $("#ZONE1").val());
                $("#PMTADR").val($("#PMTADR").val() + $("#ADDR1").val());

                //通訊地址，填入 POSTCOD2 隱藏欄位 與 CTTADR 隱藏欄位
                //如果SAMEADDRCHK為Y，則與 戶籍/通訊地址 相同
                if ($("#SAMEADDRCHK").val() == 'Y') {
                    $("#POSTCOD2").val($("#POSTCOD1").val());
                    $("#CTTADR").val($("#PMTADR").val());
                } else {
                    $("#POSTCOD2").val($("#ZIP2").val());
                    $("#CTTADR").val($("#CITY2").val() + $("#ZONE2").val());
                    $("#CTTADR").val($("#CTTADR").val() + $("#ADDR2").val());
                }

                //職業(文字)，填入 CAREER1_str 隱藏欄位
                $("#CAREER1_str").val($("#CAREER1").find(":selected").text());

                //職稱代號，塞入驗證欄位驗證
                $("#validate_CAREER2").val($("#CAREER2").val());
// 				$("#CAREER2_str").val($("#CAREER2").find(":selected").text());

                //切割 HOMEP 至 HOMEZIP 與 HOMETEL
                var inputData = $("#HOMEP").val();
                var HOMEP = matchPhone(inputData);
                if (HOMEP != null) {
                    $('#HOMEZIP').val(HOMEP[0]);
                    $('#HOMETEL').val(HOMEP[1]);
                }

                //通訊地電話 ，如果不為空則加入validate
                if ($('#HOMEP').val().length > 0) {
                    $('#HOMEP').addClass("validate[funcCallRequired[validate_OneInputPhone['通訊地電話格式錯誤',HOMEP]]]");
                } else {
                    $('#HOMEP').removeClass("validate[funcCallRequired[validate_OneInputPhone['通訊地電話格式錯誤',HOMEP]]]");
                }
                if ($('#HOMEZIP').val().length > 0) {
                    $('#HOMEZIP').addClass("validate[required,funcCall[validate_chkChrNum['<spring:message code= "LB.B0022" />',HOMEZIP]]");
                    $('#HOMEZIP').addClass("validate[funcCallRequired[validate_CheckLenEqual2['<spring:message code= "LB.B0022" />',HOMEZIP,true,0,3]]");
                } else {
                    $('#HOMEZIP').removeClass("validate[required,funcCall[validate_chkChrNum['<spring:message code= "LB.B0022" />',HOMEZIP]]");
                    $('#HOMEZIP').removeClass("validate[funcCallRequired[validate_CheckLenEqual2['<spring:message code= "LB.B0022" />',HOMEZIP,true,0,3]]");
                }
                if ($('#HOMETEL').val().length > 0) {
                    $('#HOMETEL').addClass("validate[required,funcCall[validate_chkChrNum['<spring:message code= "LB.B0020" />',HOMETEL]]");
                    $('#HOMETEL').addClass("validate[funcCallRequired[validate_CheckLenEqual2['<spring:message code= "LB.B0020" />',HOMETEL,true,0,10]]");
                } else {
                    $('#HOMETEL').removeClass("validate[required,funcCall[validate_chkChrNum['<spring:message code= "LB.B0020" />',HOMETEL]]");
                    $('#HOMETEL').removeClass("validate[funcCallRequired[validate_CheckLenEqual2['<spring:message code= "LB.B0020" />',HOMETEL,true,0,10]]");
                }

                //切割 BUSP 至 BUSZIP 與 BUSTEL12
                var inputData = $("#BUSP").val();
                var BUSP = matchPhone(inputData);
                if (BUSP != null) {
                    $('#BUSZIP').val(BUSP[0]);
                    $('#BUSTEL11').val(BUSP[1]);
                }

                //公司電話，填入 BUSTEL 隱藏欄位
                if ($("#BUSTEL12").val().length > 0)
                    $("#BUSTEL").val($("#BUSTEL11").val() + "#" + $("#BUSTEL12").val());
                else
                    $("#BUSTEL").val($("#BUSTEL11").val());


                //公司電話，如果不為空則加入validate
                if ($('#BUSP').val().length > 0) {
                    $('#BUSP').addClass("validate[funcCallRequired[validate_OneInputPhone['公司電話格式錯誤',BUSP]]]");
                } else {
                    $('#BUSP').removeClass("validate[funcCallRequired[validate_OneInputPhone['公司電話格式錯誤',BUSP]]]");
                }
                if ($('#BUSZIP').val().length > 0)
                    $('#BUSZIP').addClass("validate[required, funcCall[validate_chkChrNum['<spring:message code= "LB.X1218" />',BUSZIP]]");
                else
                    $('#BUSZIP').removeClass("validate[required, funcCall[validate_chkChrNum['<spring:message code= "LB.X1218" />',BUSZIP]]");
                if ($('#BUSTEL12').val().length > 0)
                    $('#BUSTEL12').addClass("validate[required, funcCall[validate_chkChrNum['<spring:message code= "LB.X1219" />',BUSTEL12]]");
                else
                    $('#BUSTEL12').removeClass("validate[required, funcCall[validate_chkChrNum['<spring:message code= "LB.X1219" />',BUSTEL12]]");
                if ($('#BUSTEL11').val().length > 0)
                    $('#BUSTEL11').addClass("validate[required, funcCall[validate_chkChrNum['<spring:message code= "LB.D0094" />',BUSTEL11]]");
                else
                    $('#BUSTEL11').removeClass("validate[required, funcCall[validate_chkChrNum['<spring:message code= "LB.D0094" />',BUSTEL11]]");

                //行動電話，如果不為空則加入validate
                if ($('#CELPHONE').val().length > 0)
                    $('#CELPHONE').addClass("validate[required, funcCall[validate_chkChrNum['<spring:message code= "LB.D0069" />',CELPHONE]]");
                else
                    $('#CELPHONE').removeClass("validate[required, funcCall[validate_chkChrNum['<spring:message code= "LB.D0069" />',CELPHONE]]");

                //切割 FAXP 至 FAX 與 ARACOD3
                var inputData = $("#FAXP").val();
                var FAXP = matchPhone(inputData);
                if (FAXP != null) {
                    $('#ARACOD3').val(FAXP[0]);
                    $('#FAX').val(FAXP[1]);
                }

                //傳真，如果不為空則加入validate
                if ($('#FAXP').val().length > 0) {
                    $('#FAXP').addClass("validate[funcCallRequired[validate_OneInputPhone['傳真格式錯誤',FAXP]]]");
                } else {
                    $('#FAXP').removeClass("validate[funcCallRequired[validate_OneInputPhone['傳真格式錯誤',FAXP]]]");
                }
                if ($('#ARACOD3').val().length > 0)
                    $('#ARACOD3').addClass("validate[required, funcCall[validate_chkChrNum['<spring:message code= "LB.X1220" />',ARACOD3]]");
                else
                    $('#ARACOD3').removeClass("validate[required, funcCall[validate_chkChrNum['<spring:message code= "LB.X1220" />',ARACOD3]]");
                $("#validate_FAX").val('');
                $('#FAX').removeClass("validate[required, funcCall[validate_chkChrNum['<spring:message code= "LB.X1221" />',FAX]]");
                $('#validate_FAX').removeClass("validate[required, funcCall[validate_chkChrNum['<spring:message code= "LB.X1221" />234',validate_FAX]]");
                if ($('#FAX').val().length > 0) {
                    if (($('#FAX').val()).indexOf("#") == -1) {
                        $('#FAX').addClass("validate[required, funcCall[validate_chkChrNum['<spring:message code= "LB.X1221" />',FAX]]");
                    } else {
                        $('#validate_FAX').addClass("validate[required, funcCall[validate_chkChrNum['<spring:message code= "LB.X1221" />234',validate_FAX]]");
                        var splits = ($('#FAX').val()).split("#");
                        $("#validate_FAX").val(splits[0]);
                    }
                }

                //email是否為行員ID之驗證
                if ($("#ISCUSIDN").val() == "N") {  //是否為行員ID
                    $('#MAILADDR').addClass("validate[funcCallRequired[validate_TmailNotEmp[MAILADDR]]]");
                } else {
                    $('#MAILADDR').removeClass("validate[funcCallRequired[validate_TmailNotEmp[MAILADDR]]]");
                }

                //CUSIDN轉大寫
                $("#CUSIDN").val($("#CUSIDN").val().toUpperCase());

                e = e || window.event;
                //進行 validation 並送出
                if (!$('#formId').validationEngine('validate')) {
                    e.preventDefault();
                } else {
                    $("#formId").validationEngine('detach');
                    var action = '${__ctx}/DIGITAL/ACCOUNT/apply_digital_account_p3_2';
                    $("#formId").attr("action", action);
                    $("#formId").submit();
                }
            });
            $("#CMRESET").click(function (e) {
                $("#formId")[0].reset();
                getTmr();
            });
            $("#CMBACK").click(function (e) {
                $("#formId").validationEngine('detach');
                console.log("submit~~");
                $("#formId").attr("action", "${__ctx}/DIGITAL/ACCOUNT/apply_digital_account_p2");
                $("#back").val('Y');
                $("#formId").submit();
            });
        }

        //任職機構 文字驗證用
        function ValidateValue(textbox) {
            var IllegalString = "+-_＿︿%@[`~!#$^&*()=|{}':;',\\[\\].<>/?~！#￥……&*（）——|{}【】‘；：”“'。，、？]‘'";
            var textboxvalue = textbox.value;
            var index = textboxvalue.length - 1;
            var s = textbox.value.charAt(index);
            if (IllegalString.indexOf(s) >= 0) {
                s = textboxvalue.substring(0, index);
                textbox.value = s;
            }
        }

        function genDateList() {
            console.log("CMDATE2:${result_data.data.CMDATE2}");
            var thisYear = new Date().getFullYear();
            var thisMonth = new Date().getMonth() + 1;
            var thisDay = new Date().getDate();
            var CCBIRTHDATEYY = '${ result_data.data.CCBIRTHDATEYY}';
// 			CCBIRTHDATEYY = CCBIRTHDATEYY == '' ? '' : CCBIRTHDATEYY;
            var CCBIRTHDATEMM = '${ result_data.data.CCBIRTHDATEMM}';
// 			CCBIRTHDATEMM = CCBIRTHDATEMM == '' ? '' : CCBIRTHDATEMM;
            var CCBIRTHDATEDD = '${ result_data.data.CCBIRTHDATEDD}';
// 			CCBIRTHDATEDD = CCBIRTHDATEDD == '' ? '' : CCBIRTHDATEDD;
            for (var i = thisYear - 1911; i > 0; i--) {
                j = i.toString();
                for (k = 0; k <= 2 - i.toString().length; k++) j = '0' + j;
                if (CCBIRTHDATEYY == j) {
                    $("#CCBIRTHDATEYY").append("<option value='" + j + "' selected>民國 " + i + "年</option>");
                } else {
                    $("#CCBIRTHDATEYY").append("<option value='" + j + "' >民國" + i + "年</option>");
                }
            }
            for (var i = 1; i <= 12; i++) {
                j = i.toString();
                for (k = 0; k <= 1 - i.toString().length; k++) j = '0' + j;
                if (CCBIRTHDATEMM == j) {
                    $("#CCBIRTHDATEMM").append("<option value='" + j + "' selected>" + i + "月 </option>");
                } else {
                    $("#CCBIRTHDATEMM").append("<option value='" + j + "' >" + i + "月</option>");
                }
            }
            for (var i = 1; i <= 31; i++) {
                j = i.toString();
                for (k = 0; k <= 1 - i.toString().length; k++) j = '0' + j;
                if (CCBIRTHDATEDD == j) {
                    $("#CCBIRTHDATEDD").append("<option value='" + j + "' selected>" + i + "日 </option>");
                } else {
                    $("#CCBIRTHDATEDD").append("<option value='" + j + "' >" + i + "日 </option>");
                }
            }
            var CMDATE2YY = '${ result_data.data.CMDATE2YY}';
// 			CMDATE2YY = CMDATE2YY == '' ? thisYear - 1911 : CMDATE2YY;
            var CMDATE2MM = '${ result_data.data.CMDATE2MM}';
// 			CMDATE2MM = CMDATE2MM == '' ? thisMonth : CMDATE2MM;
            var CMDATE2DD = '${ result_data.data.CMDATE2DD}';
// 			CMDATE2DD = CMDATE2DD == '' ? thisDay : CMDATE2DD;
            for (var i = thisYear - 1911; i > 0; i--) {
                j = i.toString();
                for (k = 0; k <= 2 - i.toString().length; k++) j = '0' + j;
                if (CMDATE2YY == j) {
                    $("#CMDATE2YY").append("<option value='" + j + "' selected>民國 " + i + "年</option>");
                } else {
                    $("#CMDATE2YY").append("<option value='" + j + "' >民國" + i + "年</option>");
                }
            }
            for (var i = 1; i <= 12; i++) {
                j = i.toString();
                for (k = 0; k <= 1 - i.toString().length; k++) j = '0' + j;
                if (CMDATE2MM == j) {
                    $("#CMDATE2MM").append("<option value='" + j + "' selected>" + i + "月 </option>");
                } else {
                    $("#CMDATE2MM").append("<option value='" + j + "' >" + i + "月</option>");
                }
            }
            for (var i = 1; i <= 31; i++) {
                j = i.toString();
                for (k = 0; k <= 1 - i.toString().length; k++) j = '0' + j;
                if (CMDATE2DD == j) {
                    $("#CMDATE2DD").append("<option value='" + j + "' selected>" + i + "日 </option>");
                } else {
                    $("#CMDATE2DD").append("<option value='" + j + "' >" + i + "日 </option>");
                }
            }
        }

        // 日曆欄位參數設定
        function datetimepickerEvent() {
            $(".CMDATE2").click(function (event) {
                $('#CMDATE2').datetimepicker('show');
            });
            $(".CCBIRTHDATE").click(function (event) {
                $('#CCBIRTHDATE').datetimepicker('show');
            });
            jQuery('.datetimepicker').datetimepicker({
                timepicker: false,
                closeOnDateSelect: true,
                scrollMonth: false,
                scrollInput: false,
                format: 'Y/m/d',
                lang: '${transfer}'
            });
        }

        //預約自動輸入今天
        function getTmr() {
            var today = new Date();
            today.setDate(today.getDate());
            var y = today.getFullYear();
            var m = today.getMonth() + 1;
            var d = today.getDate();
            if (m < 10) {
                m = "0" + m
            }
            if (d < 10) {
                d = "0" + d
            }
            var tmr = y + "/" + m + "/" + d
            if ("${result_data.data.CMDATE2}" == "")
                $('#CMDATE2').val(tmr);
        }

        function doquery2(cityFilter, areaFilter) {
            if ($("#SAMEADDRCHK").val() == 'Y') {
                $("#CITY2").val($("#CITY1").val());
            }
            $.ajax({
                type: "POST",
                url: "${__ctx}/ONLINE/APPLY/apply_deposit_account_get_area",
                data: {
                    CITY: $(cityFilter).val()
                },
                async: false,
                dataType: "json",
                success: function (msg) {
                    $(areaFilter).html("<option value='#'><spring:message code= "LB.X0546" /></option> ");
                    msg.data.Area.forEach(function (area) {
                        console.log(area.AREA);
                        $(areaFilter).append("<option>" + area.AREA + "</option>");
                    });
                    if ($("#SAMEADDRCHK").val() == 'Y') {
                        $('#ZONE2').html("<option value='#'><spring:message code= "LB.X0546" /></option> ");
                        msg.data.Area.forEach(function (area) {
                            console.log(area.AREA);
                            $("#ZONE2").append("<option>" + area.AREA + "</option>");
                        });
                    }
                }
            })
        }


        function doquery3(cityFilter, areaFilter, zipFilter1) {
            if ($("#SAMEADDRCHK").val() == 'Y') {
                console.log('SAMEADDRCHK~');
                $("#ZONE2").val($("#ZONE1").val());
            }
            $.ajax({
                type: "POST",
                url: "${__ctx}/ONLINE/APPLY/apply_deposit_account_get_zip_code",
                data: {
                    CITY: $(cityFilter).val(),
                    AREA: $(areaFilter).val()
                },
                async: false,
                dataType: "json",
                success: function (msg) {
                    console.log(msg.data);
                    $(zipFilter1).val(msg.data.Zip);
                    if ($("#SAMEADDRCHK").val() == 'Y') {
                        $("#ZIP2").val(msg.data.Zip);
                    }
                }
            })
        }

        function doquery9() {
            console.log($("#SAMEADDRCHK").prop("checked"));

            if ($("#SAMEADDRCHK").prop("checked")) {
                $("#SAMEADDRCHK").val('Y');
                $("#CITY2").children().each(function () {
                    console.log($(this).val());
                    if ($(this).val() == $("#CITY1").val()) {
                        $(this).prop("selected", true);
                    }
                });
                $('#ZONE2').html("<option value='#'><spring:message code= "LB.X0546" /></option> ");

                $("#ZONE1").children().each(function () {
                    console.log($(this).val());
                    if ($(this).val() != $("#ZONE1").val()) {
                        $("#ZONE2").append("<option>" + $(this).val() + "</option>");
                    } else {
                        $("#ZONE2").append("<option selected>" + $(this).val() + "</option>");
                    }
                });

                $("#ADDR2").val($("#ADDR1").val());
                $("#ZIP2").val($("#ZIP1").val());

                //勾選同戶籍地址
                $("#CITY2, #ZONE2, #ADDR2").addClass("tbb-disabled");
            } else {
                $("#SAMEADDRCHK").val('N');

                //勾選同戶籍地址
                $("#CITY2, #ZONE2, #ADDR2").removeClass("tbb-disabled");
            }
        }

        function setCITY() {

            llo = "${__ctx}/js/";
            $('#twzipcode1').twzipcode({
                countyName: 'CITY1',
                districtName: 'ZONE1',
                zipcodeName: 'ZIP1',
                language: 'zip_${pageContext.response.locale}',
                countySel: '${result_data.data.CITY1}',
                districtSel: '${result_data.data.ZONE1}'
            });
            $('#twzipcode2').twzipcode({
                countyName: 'CITY2',
                districtName: 'ZONE2',
                zipcodeName: 'ZIP2',
                language: 'zip_${pageContext.response.locale}',
                countySel: '${result_data.data.CITY2}',
                districtSel: '${result_data.data.ZONE2}'
            });
//     		$("#CITY1").children().each(function(){
//     		    if ($(this).text()=='${result_data.data.CITY1}'){
//     		        console.log("TRUE");
//     		        $(this).attr("selected", true);
//     		    }
//     		});

//     		doquery2('#CITY1','#ZONE1');

//     		$("#CITY2").children().each(function(){
//     		    if ($(this).text()=='${result_data.data.CITY2}'){
//     		        console.log("TRUE");
//     		        $(this).attr("selected", true);
//     		    }
//     		});
//     		doquery2('#CITY2','#ZONE2');
//     		$("#ZONE2").children().each(function(){
//     		    if ($(this).text()=='${result_data.data.ZONE2}'){
//     		        console.log("TRUE");
//     		        $(this).attr("selected", true);
//     		    }
//     		});
//     		$("#ZONE1").children().each(function(){
//     		    if ($(this).text()=='${result_data.data.ZONE1}'){
//     		        console.log("TRUE");
//     		        $(this).attr("selected", true);
//     		    }
//     		});

        }

        function matchPhone(input) {
            var phoneList = [];
            var re = new RegExp(/^\d{1,3}-\d{1,10}$/)
            if (re.test(input)) {
                var vList1 = input.split("-");
                phoneList.push(vList1[0]);
                phoneList.push(vList1[1]);
                return phoneList;
            }
            return null;
        }

        function changeMatchPhone(input) {
            var inputData = $("#" + input).val();
            var phoneList = matchPhone(inputData);
            if (phoneList != null)
                $("#" + input).val(phoneList[0] + '-' + phoneList[1]);
        }

        function showChild(flag) {
            if (flag) {
                $("#CHILD_block").hide();
                $("#CHILD").val("");
            } else {
                $("#CHILD_block").show();
            }
        }

        function showHIRISK(flag) {
            if (flag) {
                $("#HIRISK_block").hide();
                $("#HIRISK").val("");
            } else {
                $("#HIRISK_block").show();
            }
        }

        function hideErrorMsg() {
            console.log($("input[name=RENAME]").prop('checked'));
            if ($("input[name=RENAME][checked]")) {
                $('.hideblock').hide()

            }
        }

		function timeLogout(){
			// 刷新session
			var uri = '${__ctx}/login_refresh';
			console.log('refresh.uri: '+uri);
			var result = fstop.getServerDataEx( uri, null, false, null);
			console.log('refresh.result: '+JSON.stringify(result));
			// 初始化登出時間
			$("#countdownheader").html(parseInt(countdownSecHeader)+1);
			$("#countdownMin").html("");
			$("#mobile-countdownMin").html("");
			$("#countdownSec").html("");
			$("#mobile-countdownSec").html("");
			// 倒數
			startIntervalHeader(1, refreshCountdownHeader, []);
		}

		function refreshCountdownHeader(){
			// timeout剩餘時間
			var nextSec = parseInt($("#countdownheader").html()) - 1;
			$("#countdownheader").html(nextSec);

			// 提示訊息--即將登出，是否繼續使用
			if(nextSec == 120){
				initLogoutBlockUI();
			}
			// timeout
			if(nextSec == 0){
				// logout
				fstop.logout( '${__ctx}'+'/logout_aj' , '${__ctx}'+'/timeout_logout');
			}
			if (nextSec >= 0){
				// 倒數時間以分秒顯示
				var minutes = Math.floor(nextSec / 60);
				$("#countdownMin").html(('0' + minutes).slice(-2));
				$("#mobile-countdownMin").html(('0' + minutes).slice(-2));

				var seconds = nextSec - minutes * 60;
				$("#countdownSec").html(('0' + seconds).slice(-2));
				$("#mobile-countdownSec").html(('0' + seconds).slice(-2));
			}
		}
		function startIntervalHeader(interval, func, values){
			clearInterval(countdownObjheader);
			countdownObjheader = setRepeater(func, values, interval);
		}
		function setRepeater(func, values, interval){
			return setInterval(function(){
				func.apply(this, values);
			}, interval * 1000);
		}
		/**
		 * 初始化logoutBlockUI
		 */
		function initLogoutBlockUI() {
			logoutblockUI();
		}

		/**
		 * 畫面BLOCK
		 */
		function logoutblockUI(timeout){
			$("#logout-block").show();

			// 遮罩後不給捲動
			document.body.style.overflow = "hidden";

			var defaultTimeout = 60000;
			if(timeout){
				defaultTimeout = timeout;
			}
		}

		/**
		 * 畫面UNBLOCK
		 */
		function unLogoutBlockUI(timeoutID){
			if(timeoutID){
				clearTimeout(timeoutID);
			}
			$("#logout-block").hide();

			// 解遮罩後給捲動
			document.body.style.overflow = 'auto';
		}
		/**
		 *繼續使用
		 */
		function keepLogin(){
			unLogoutBlockUI(); // 解遮罩
			timeLogout(); // 刷新倒數計時
		}

    </script>
</head>
<body>
<!-- header     -->
<header>
    <%@ include file="../index/header_logout.jsp" %>
</header>

<!-- 麵包屑     -->
<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
    <ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
        <li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
        <!-- 線上開立數位存款帳戶     -->
        <li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1361"/></li>
    </ol>
</nav>


<div class="content row">
    <!-- 		主頁內容  -->
    <main class="col-12">
        <section id="main-content" class="container">
            <h2>
                <!-- 					線上開立數位存款帳戶 -->
                <spring:message code="LB.D1361"/>
            </h2>
            <i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
            <div id="step-bar">
                <ul>
                    <li class="finished">注意事項與權益</li>
                    <li class="finished">開戶認證</li>
                    <li class="active">開戶資料</li>
                    <li class="">確認資料</li>
                    <li class="">完成申請</li>
                </ul>
            </div>
            <form id="formId" method="post">
                <input type="hidden" name="ADOPID" id="ADOPID" value="N203">
                <input type="hidden" name="FGTXWAY" id="FGTXWAY" value="${result_data.data.FGTXWAY}">
                <input type="hidden" name="HAS_PIC" id="HAS_PIC" value="Y">
                <input type="hidden" name="BIRTHDAY" id="BIRTHDAY" value="">
                <input type="hidden" name="CMDATE2" id="CMDATE2" value="">
                <input type="hidden" name="POSTCOD1" id="POSTCOD1" value="">
                <input type="hidden" name="PMTADR" id="PMTADR" value="">
                <input type="hidden" name="POSTCOD2" id="POSTCOD2" value="">
                <input type="hidden" name="CTTADR" id="CTTADR" value="">
                <input type="hidden" name="BUSTEL" id="BUSTEL" value="">
                <input type="hidden" name="ID_CHGE" id="ID_CHGE" value="">
                <input type="hidden" name="CHGE_DT" id="CHGE_DT" value="">
                <input type="hidden" name="CHGE_CY" id="CHGE_CY" value="">
                <input type="hidden" name="CHGE_CYNAME" id="CHGE_CYNAME" value="">
                <input type="hidden" name="ISBRANCH" id="ISBRANCH" value="">
                <input type="hidden" name="CAREER1_str" id="CAREER1_str" value="">
                <input type="hidden" name="TODAY" id="TODAY" value="${ today }">
                <input type="hidden" name="back" id="back" value=>
                <input type="hidden" name="ISCUSIDN" id="ISCUSIDN" value="${result_data.data.ISCUSIDN}">
                <!-- timeout -->
                <section id="id-and-fast">
						<span class="id-name">
							<spring:message code="LB.Welcome_1"/>&nbsp;
							<c:if test="${empty result_data.data.CUSNAME}">
                                <span id="username" name="username_show"><spring:message code="LB.X2190"/></span>
                            </c:if>
							<c:if test="${not empty result_data.data.CUSNAME}">
                                <span id="username" name="username_show">${result_data.data.CUSNAME}</span>
                            </c:if>
							&nbsp;<spring:message code="LB.Welcome_2"/>
						</span>
                    <div id="id-block">
                        <div>
							<span class="id-time">
								<fmt:setLocale value="${pageContext.response.locale}"/>
								<c:if test="${!empty sessionScope.logindt && !empty sessionScope.logintm}">
                                    <fmt:parseDate var="parseDate"
                                                   value="${sessionScope.logindt} ${sessionScope.logintm}"
                                                   pattern="yyyy-MM-dd HH:mm:ss"/>
                                    <fmt:formatDate value="${parseDate}" dateStyle="full"
                                                    type="both"/>&nbsp;<spring:message code="LB.X2250"/>
                                    <br/>
                                </c:if>

								<fmt:formatDate value="${sessionScope.login_time}" type="both" dateStyle="full"/>
								&nbsp;<spring:message code="LB.Login_successful"/>
							</span>
                            <!-- 自動登出剩餘時間 -->
                            <span class="id-time"><spring:message code="LB.X1912"/>
								<span class="high-light ml-1">
									<!-- 分 -->
									<font id="countdownMin"></font>
									:
                                    <!-- 秒 -->
									<font id="countdownSec"></font>
								</span>
							</span>
                        </div>
                        <button type="button" class="btn-flat-orange" onclick="timeLogout()"><spring:message
                                code="LB.X1913"/></button>
                        <button type="button" class="btn-flat-darkgray"
                                onClick="fstop.logout( '${__ctx}'+'/logout_aj' , '${__ctx}'+'/login')"><spring:message
                                code="LB.Logout"/></button>
                    </div>
                </section>
                <!-- 顯示區  -->
                <div class="main-content-block row">
                    <div class="col-12">
                        <!-- 							中文戶名 -->
                        <div class="ttb-input-block">
                            <div class="ttb-message">
                                <p>開戶資料 (1/4)</p>
                            </div>


                            <div class="classification-block">
                                <p>基本資料</p>
                                <p>( <span class="high-light">*</span> 為必填)</p>
                            </div>

                            <!-- 中文戶名  -->
                            <div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D1105"/> <span class="high-light">*</span>
										</h4>
									</label>
									</span>
                                <span class="input-block">
										<div class="ttb-input">
											<input type="text" id="CUSNAME" name="CUSNAME" placeholder="請輸入中文戶名"
                                                   class="text-input validate[required,funcCall[validate_isChinese[<spring:message code= "LB.X1056" />,CUSNAME]]"
                                                   maxLength="50" size="100" value="${result_data.data.CUSNAME}"/>
											<span class="input-remarks">請輸入與您身分證姓名相同的中文戶名，以利審核的進行。</span>
										</div>
									</span>
                            </div>
                            <!-- ********** -->
                            <!-- 更換過姓名  -->
                            <div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0052_3"/> <span class="high-light">*</span>
										</h4>
									</label>
									</span>
                                <span class="input-block">
										<div class="ttb-input">
											
											<label class="radio-block">
<!-- 												是 -->
												<spring:message code="LB.D0034_2"/>
												<input type="radio" name="RENAME" id="RENAME1" value="Y"
                                                       onclick="hideErrorMsg()"/>
												<span class="ttb-radio"></span>
											</label>
											<label class="radio-block">
<!-- 												否 -->
												<spring:message code="LB.D0034_3"/>
												<input type="radio" name="RENAME" id="RENAME2" value="N"
                                                       onclick="hideErrorMsg()"/>
												<span class="ttb-radio"></span>
											</label>
											<span class="hideblock">
												<input id="validate_RENAME" name="RENAME" type="radio"
                                                       class="validate[funcCall[validate_Radio['<spring:message code= "LB.D0052" />',RENAME]]]"
                                                       style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
											</span>
										</div>
									</span>
                            </div>
                            <!-- ********** -->
                            <!-- 身分證統一編號 -->
                            <div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0519"/> <span class="high-light">*</span>
										</h4>
									</label>
									</span>
                                <span class="input-block">
										<div class="ttb-input">
											<input type="text" id="CUSIDN" name="CUSIDN" placeholder="請輸入身分證統一編號 "
                                                   class="text-input validate[required, funcCall[validate_checkSYS_IDNO[CUSIDN]]]"
                                                   maxLength="10" size="11" value="${result_data.data.CUSIDN}"
                                                   disabled/>
										</div>
									</span>
                            </div>
                            <!-- ********** -->
                            <!-- 國籍 -->
                            <div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.X0205"/> <span class="high-light">*</span>
										</h4>
									</label>
									</span>
                                <span class="input-block">
										<div class="ttb-input">
											<input type="hidden" name="COUNTRY" value="TW"/><input type="TEXT"
                                                                                                   name="COUNTRY1"
                                                                                                   class="text-input"
                                                                                                   size="3"
                                                                                                   maxlength="2"
                                                                                                   value="TW" disabled/>
										</div>
									</span>
                            </div>
                            <!-- 出生日期 -->
                            <div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0582"/> <span class="high-light">*</span>
										</h4>
									</label>
									</span>
                                <span class="input-block">
										<div class="ttb-input">
											<!-- 年 -->
											<select name="CCBIRTHDATEYY" id="CCBIRTHDATEYY"
                                                    class="custom-select select-input input-width-100 validate[required]"
                                                    value="${result_data.data.CCBIRTHDATEYY}">
												<option value="#">
													---
												</option>
											</select>
                                            <!-- 月 -->
											<select name="CCBIRTHDATEMM" id="CCBIRTHDATEMM"
                                                    class="custom-select select-input input-width-60 validate[required]"
                                                    value="${result_data.data.CCBIRTHDATEMM}">
												<option value="#">
													---
												</option>
											</select>
                                            <!-- 日 -->
											<select name="CCBIRTHDATEDD" id="CCBIRTHDATEDD"
                                                    class="custom-select select-input input-width-60 validate[required]"
                                                    value="${result_data.data.CCBIRTHDATEDD}">
												<option value="#">
													---
												</option>
											</select>
                                            <!-- 												驗證用的span預設隱藏 -->
											<span id="hideblock_BIRTHDATE">
<!-- 												驗證用的input -->
											<input id="validate_BIRTHDATE" name="validate_BIRTHDATE" type="text"
                                                   class="text-input validate[funcCallRequired[validate_age[<spring:message code= "LB.X1222" />,BIRTHDAY,TODAY,20,true]]]"
                                                   style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
											</span>
											<input id="validate_CCBIRTHDATEYY" name="validate_CCBIRTHDATEYY" type="text"
                                                   class="
												validate[funcCallRequired[validate_CheckSelect[年,CCBIRTHDATEYY,#]]]"
                                                   style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
											<input id="validate_CCBIRTHDATEMM" name="validate_CCBIRTHDATEMM" type="text"
                                                   class="
												validate[funcCallRequired[validate_CheckSelect[月,CCBIRTHDATEMM,#]]]"
                                                   style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
											<input id="validate_CCBIRTHDATEDD" name="validate_CCBIRTHDATEDD" type="text"
                                                   class="
												validate[funcCallRequired[validate_CheckSelect[日,CCBIRTHDATEDD,#]]]"
                                                   style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
										</div>
									</span>
                            </div>
                            <!-- ********** -->
                            <!-- 身分證發證資訊 -->
                            <div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											 身分證發證資訊 <span class="high-light">*</span>
										</h4>
									</label>
									</span>
                                <span class="input-block">
										<div class="ttb-input">
											<!-- 年 -->
											<select name="CMDATE2YY" id="CMDATE2YY"
                                                    class="custom-select select-input input-width-100 validate[required]">
												<option value="#">
													---
												</option>
											</select>
                                            <!-- 月 -->
											<select name="CMDATE2MM" id="CMDATE2MM"
                                                    class="custom-select select-input input-width-60 validate[required]">
												<option value="#">
													---
												</option>
											</select>
                                            <!-- 日 -->
											<select name="CMDATE2DD" id="CMDATE2DD"
                                                    class="custom-select select-input input-width-60 validate[required]">
												<option value="#">
													---
												</option>
											</select>

<%-- 											<input type="text" id="CMDATE2" name="CMDATE2" class="text-input datetimepicker" value="${result_data.data.CMDATE2}" /> --%>
                                            <!-- 											<span class="input-unit CMDATE2"> -->
<%-- 												<img src="${__ctx}/img/icon-7.svg" /> --%>
                                            <!-- 											</span> -->
                                            <!-- 驗證用的span預設隱藏 -->
                                            <!-- 											<span id="hideblock_CMDATE2" > -->
                                            <!-- 											驗證用的input -->
                                            <!-- 											<input id="validate_CMDATE2" name="validate_CMDATE2" type="text" class="text-input validate[required, verification_date[validate_CMDATE2]]"  -->
                                            <!-- 												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
                                            <!-- 											</span> -->
												
											<input id="validate_CMDATE2YY" name="validate_CMDATE2YY" type="text" class="
												validate[funcCallRequired[validate_CheckSelect[年,CMDATE2YY,#]]]"
                                                   style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
											<input id="validate_CMDATE2MM" name="validate_CMDATE2MM" type="text" class="
												validate[funcCallRequired[validate_CheckSelect[月,CMDATE2MM,#]]]"
                                                   style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
											<input id="validate_CMDATE2DD" name="validate_CMDATE2DD" type="text" class="
												validate[funcCallRequired[validate_CheckSelect[日,CMDATE2DD,#]]]"
                                                   style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
										</div>
										<div class="ttb-input">
											<select name="CITYCHA" id="CITYCHA"
                                                    class="custom-select select-input input-width-125 validate[required]">
								          		<option value="#"><spring:message code="LB.W1670"/></option>
											</select>
											<select name="TYPECHA" id="TYPECHA"
                                                    class="custom-select select-input input-width-100 validate[required]">
								          		<option value="1"><spring:message code="LB.B0002"/></option>
								          		<option value="3" selected><spring:message code="LB.W1664"/></option>
								          		<option value="2"><spring:message code="LB.W1665"/></option>
											</select>
                                   			<span class="input-remarks">請輸入與您身分證相同的發證資訊。</span>
											<input id="validate_CITYCHA" name="validate_CITYCHA" type="text" class="
												validate[funcCallRequired[validate_CheckSelect[<spring:message code="LB.D1071" />,CITYCHA,#]]]"
                                                   style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
										</div>
									</span>
                            </div>
                            <!-- ********** -->
                            <!-- 								<div class="ttb-input-item row"> -->
                            <!-- 									<span class="input-title">  -->
                            <!-- 									<label> -->
                            <!-- 										<h4> -->
                            <!-- 										身分證有無相片 -->
                            <%-- 											<spring:message code="LB.D1115"/> <span class="high-light">*</span> --%>
                            <!-- 										</h4> -->
                            <!-- 									</label> -->
                            <!-- 									</span>  -->
                            <!-- 									<span class="input-block"> -->
                            <!-- 										<div class="ttb-input"> -->
                            <!-- 											<label class="radio-block"> -->
                            <!--  												有 -->
                            <%-- 												<spring:message code="LB.D1070_1"/> --%>
                            <!-- 												<input type="radio" name="HAS_PIC" id="PIC_FLAG1" value="Y" checked /> -->
                            <!-- 												<span class="ttb-radio"></span> -->
                            <!-- 											</label>&nbsp; -->
                            <!-- 											<label class="radio-block"> -->
                            <!--  												無 -->
                            <%-- 												<spring:message code="LB.D1070_2"/> --%>
                            <!-- 												<input type="radio" name="HAS_PIC" id="PIC_FLAG2" value="N" /> -->
                            <!-- 												<span class="ttb-radio"></span> -->
                            <!-- 											</label> -->
                            <!-- 										</div> -->
                            <!-- 									</span> -->
                            <!-- 								</div> -->
                            <!-- ********** -->
                            <div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 										學歷 -->
											<spring:message code="LB.D1136"/> <span class="high-light">*</span>
										</h4>
									</label>
									</span>
                                <span class="input-block">
										<div class="ttb-input">
											<select name="DEGREE" id="DEGREE"
                                                    class="custom-select select-input validate[required]">
												<option value=""><spring:message code="LB.Select"/><spring:message
                                                        code="LB.D1136"/></option>
									 			<option value="1"><spring:message code="LB.D0588"/></option>
									 			<option value="2"><spring:message code="LB.D0589"/></option>
									 			<option value="3"><spring:message code="LB.D0590"/></option>
									 			<option value="4"><spring:message code="LB.D0591"/></option>
									 			<option value="5"><spring:message code="LB.D1141"/></option>
									 			<option value="6"><spring:message code="LB.D0866"/></option>						
											</select>
										</div>
									</span>
                            </div>
                            <!-- ********** -->
                            <div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 										婚姻狀況 -->
											<spring:message code="LB.D0057"/>
										</h4>
									</label>
									</span>
                                <span class="input-block">
	                                    <div class="ttb-input">
	                                        <label class="radio-block" for="MARRY1"
                                                   onclick="showChild(false);"><spring:message code="LB.D0595"/>
	                                            <input type="radio" name="MARRY" id="MARRY1" value="1"/>
	                                            <span class="ttb-radio"></span>
	                                        </label>
	                                        <label class="radio-block" for="MARRY0"
                                                   onclick="showChild(true);"><spring:message code="LB.D0596"/>
	                                            <input type="radio" name="MARRY" id="MARRY0" value="0"/>
	                                            <span class="ttb-radio"></span>
	                                        </label>
	                                        <label class="radio-block" for="MARRY2"
                                                   onclick="showChild(false);"><spring:message code="LB.D0572"/>
	                                            <input type="radio" name="MARRY" id="MARRY2" value="2"/>
	                                            <span class="ttb-radio"></span>
	                                        </label>
	                                    </div>
									</span>
                            </div>
                            <!-- ********** -->
                            <div class="ttb-input-item row" id="CHILD_block">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 											子女人數 -->
											<spring:message code="LB.D1149"/>
										</h4>
									</label>
									</span>
                                <span class="input-block">
										<div class="ttb-input">
											<select name="CHILD" id="CHILD" class="custom-select select-input">
												<option value="">請輸入<spring:message code="LB.D1149"/></option> 
											</select>
<%-- 											<input type="number" inputmode="numeric" id="CHILD" name="CHILD" class="text-input" value="${result_data.data.CHILD}" maxLength="2" size="3"/> --%>
										</div>
									</span>
                            </div>
                            <!-- ***************************************** -->
                            <div class="classification-block">
                                <p>聯絡資訊</p>
                                <p>( <span class="high-light">*</span> 為必填)</p>
                            </div>
                            <!-- ***************************************** -->
                            <!-- E-mail -->
                            <div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											E-mail <span class="high-light">*</span>
										</h4>
									</label>
									</span>
                                <span class="input-block">
										<div class="ttb-input">
											<input type="text" id="MAILADDR" name="MAILADDR" placeholder="請輸入E-mail"
                                                   class="text-input validate[required,funcCall[validate_EmailCheck[MAILADDR]]]"
                                                   value="${result_data.data.MAILADDR}" maxLength="60" size="61"/>
											<span class="check-block"
                                                  style="padding-left: 0px; font-size: inherit"><spring:message
                                                    code="LB.X2619"></spring:message></span>
										</div>
									</span>
                            </div>
                            <!-- ********** -->
                            <div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 											行動電話 -->
											<spring:message code="LB.D0069"/> <span class="high-light">*</span>
										</h4>
									</label>
									</span>
                                <span class="input-block">
										<div class="ttb-input">
											<input type="tel" inputmode="numeric" id="CELPHONE" name="CELPHONE"
                                                   class="text-input validate[funcCallRequired[validate_cellPhone[手機號碼格式錯誤,CELPHONE]]]"
                                                   value="${result_data.data.CELPHONE}" placeholder="例如：0981212123"
                                                   maxLength="10" size="11"/>
											
											<span id="hideblock_celphone">
												<input id="validate_celphone" name="validate_celphone" type="text"
                                                       value="#" class="text-input
													validate[funcCallRequired[validate_chkCelPhome['<spring:message code= "LB.X1229" />',CELPHONE]]]"
                                                       style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
											</span>
										</div>
									</span>
                            </div>
                            <!-- ********** -->
                            <div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 											通訊地電話 -->
											<spring:message code="LB.B0020"/>
										</h4>
									</label>
									</span>
                                <span class="input-block">
										<div class="ttb-input">
											<input type="tel" inputmode="numeric" id="HOMEP" name="HOMEP"
                                                   class="text-input"
                                                   value="${result_data.data.HOMEZIP}-${result_data.data.HOMETEL}"
                                                   placeholder="例如：02-7818218" maxLength="14" size="11"
                                                   onchange="changeMatchPhone('HOMEP')"/>
											<input type="tel" inputmode="numeric" id="HOMEZIP" name="HOMEZIP"
                                                   value="${result_data.data.HOMEZIP}" maxLength="3" size="4"
                                                   style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
											<input type="tel" inputmode="numeric" id="HOMETEL" name="HOMETEL"
                                                   value="${result_data.data.HOMETEL}" maxLength="10" size="11"
                                                   style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
										</div>
									</span>
                            </div>
                            <!-- ********** -->
                            <div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 											公司電話 -->
											<spring:message code="LB.D0094"/>
										</h4>
									</label>
									</span>
                                <span class="input-block">
										<div class="ttb-input">
										
											<input type="tel" inputmode="numeric" id="BUSP" name="BUSP"
                                                   class="text-input"
                                                   value="${result_data.data.BUSZIP}-${result_data.data.BUSTEL11}"
                                                   placeholder="例如：02-78182181" maxLength="14" size="11"
                                                   onchange="changeMatchPhone('BUSP')"/>
											<spring:message code="LB.D0095"/>
											<input type="tel" inputmode="numeric" id="BUSTEL12" name="BUSTEL12"
                                                   class="text-input card-input" value="${result_data.data.BUSTEL12}"
                                                   maxLength="5" size="5"/>
											
											<input type="tel" inputmode="numeric" id="BUSZIP" name="BUSZIP"
                                                   value="${result_data.data.BUSZIP}" maxLength="3" size="4"
                                                   style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
											<input type="tel" inputmode="numeric" id="BUSTEL11" name="BUSTEL11"
                                                   value="${result_data.data.BUSTEL11}" maxLength="10" size="11"
                                                   style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
                                            <!-- 												驗證用的span預設隱藏 -->
											<span id="hideblock_phone">
												<input id="validate_phone" name="validate_phone" type="text" value="#"
                                                       class="text-input
													validate[funcCallRequired[validate_chkPhoneOne['<spring:message code= "LB.X1229" />',HOMEP,BUSP,CELPHONE]]]"
                                                       style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
											</span>
										</div>
									</span>
                            </div>
                            <!-- ********** -->
                            <div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 											傳真 -->
											<spring:message code="LB.D1131"/>
										</h4>
									</label>
									</span>
                                <span class="input-block">
										<div class="ttb-input">
											<input type="tel" inputmode="numeric" id="FAXP" name="FAXP"
                                                   class="text-input"
                                                   value="${result_data.data.ARACOD3}-${result_data.data.FAX}"
                                                   placeholder="例如：02-7818218" maxLength="21" size="18"
                                                   onchange="changeMatchPhone('FAXP')"/>
											<input type="tel" inputmode="numeric" id="ARACOD3" name="ARACOD3"
                                                   value="${result_data.data.ARACOD3}" maxLength="3" size="4"
                                                   style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
											<input type="tel" inputmode="numeric" id="FAX" name="FAX"
                                                   value="${result_data.data.FAX}" maxLength="17" size="18"
                                                   style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>

                                            <!-- 												驗證用的span預設隱藏 -->
											<span id="hideblock_FAX">
												<input id="validate_FAX" name="validate_FAX" type="text" value="#"
                                                       class="text-input"
                                                       style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
											</span>
										</div>
									</span>
                            </div>
                            <!-- ********** -->
                            <!-- 戶籍/通訊地址 -->
                            <div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.D0143"/> <span class="high-light">*</span>
										</h4>
									</label>
									</span>
                                <span class="input-block">
<%-- 										<input type="hidden" name="ZIP1" id="ZIP1" value="${result_data.data.ZIP1}" size=4 maxlength=3> --%>
										
										<div class="ttb-input" id="twzipcode1">
											<span data-role="county"
                                                  data-style="custom-select select-input input-width-125"></span>
											<span data-role="district"
                                                  data-style="custom-select select-input input-width-100"></span>
											<span data-role="zipcode" data-style="zipcode"></span>
										</div>

                                    <!-- 										<div class="ttb-input"> -->
<%-- 											<select name="CITY1" id="CITY1" class="custom-select select-input input-width-125"  onchange="doquery2('#CITY1','#ZONE1')" value="${result_data.data.CITY1}"> --%>
<%-- 								          		<option value="#"><spring:message code="LB.Select"/><spring:message code="LB.D0059"/></option>   --%>
<%-- 								          		<c:forEach var="dataList" items="${city_data}"> --%>
<%-- 													<option>${dataList.CITY}</option> --%>
<%-- 												</c:forEach> --%>
                                    <!-- 											</select> -->
                                    <!-- 											<select name="ZONE1" id="ZONE1" class="custom-select select-input input-width-100" onchange="doquery3('#CITY1','#ZONE1', '#ZIP1')"> -->
<%-- 								          		<option value="#"><spring:message code="LB.Select"/><spring:message code="LB.D0060"/></option>  --%>
                                    <!-- 											</select> -->
<%-- 											<input type="text" id="ADDR11" name="ADDR11" class="text-input card-input" value="${result_data.data.ADDR11}" maxLength="4" size="4"/><spring:message code="LB.D1121"/> --%>
<%-- 											<input type="text" id="ADDR12" name="ADDR12" class="text-input card-input" value="${result_data.data.ADDR12}" maxLength="4" size="4"/><spring:message code="LB.D1122"/> --%>
<%-- 											<input type="text" id="ADDR13" name="ADDR13" class="text-input card-input" value="${result_data.data.ADDR13}" maxLength="2" size="4"/><spring:message code="LB.D1123"/> --%>
                                    <!-- 										</div> -->
                                    	<div class="ttb-input">
											<input type="text" id="ADDR1" name="ADDR1" placeholder="請輸入戶籍/通訊地址 "
                                                   class="text-input validate[required]"
                                                   value="${result_data.data.ADDR1}" maxLength="24" size="42"/>
											
											<span id="hideblock_ADDR1">
												<input id="validate_CITY1" name="validate_CITY1" type="text" value="#"
                                                       class="text-input
													validate[funcCall[validate_CheckSelect['<spring:message code= "LB.D1118" />',CITY1,#]]]"
                                                       style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
												<input id="validate_ZONE1" name="validate_ZONE1" type="text" value="#"
                                                       class="text-input
													validate[funcCall[validate_CheckSelect['<spring:message code= "LB.D1490" />',ZONE1,#]]]"
                                                       style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
<%-- 												<input id="validate_ADDR11" name="validate_ADDR11" type="text" value="#" class="text-input  --%>
<%-- 													validate[funcCallRequired[validate_isChinese['<spring:message code= "LB.X1226" />',ADDR11]]]"  --%>
                                                <!-- 													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
<%-- 												<input id="validate_ADDR12" name="validate_ADDR12" type="text" value="#" class="text-input  --%>
<%-- 													validate[funcCallRequired[validate_isChinese['<spring:message code= "LB.X1227" />',ADDR12]]]"  --%>
                                                <!-- 													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
<%-- 												<input id="validate_ADDR13" name="validate_ADDR13" type="text" value="#" class="text-input  --%>
<%-- 													validate[funcCallRequired[validate_CheckNumber['<spring:message code= "LB.X1228" />',ADDR13]]]"  --%>
                                                <!-- 													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
												<input id="validate_ADDR132" name="validate_ADDR132" type="text"
                                                       value="#" class="text-input
													validate[funcCallRequired[validate_requiredWith['<spring:message code= "LB.X1228" />',ADDR13]]]"
                                                       style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
											</span>
										</div>
									</span>
                            </div>
                            <!-- ********** -->
                            <div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 										通訊地址 -->
                                            <spring:message code="LB.D0376"/> <span class="high-light">*</span>
										</h4>
									</label>
									</span>
                                <span class="input-block">
										<div class="ttb-input">
                                        	<label class="check-block" for="SAMEADDRCHK">
												<input type="checkbox" name="SAMEADDRCHK" id="SAMEADDRCHK"
                                                       onClick="doquery9()" onblur="doquery9()">同戶籍地址
                                            	<span class="ttb-check"></span>
											</label>
										</div>
										
										<div class="ttb-input" id="twzipcode2">
											<span data-role="county"
                                                  data-style="custom-select select-input input-width-125 validate[required]"></span>
											<span data-role="district"
                                                  data-style="custom-select select-input input-width-100 validate[required]"></span>
											<span data-role="zipcode" data-style="zipcode"></span>
										</div>
										
	                                    <div class="ttb-input">
<%-- 											<input type="hidden" name="ZIP2" id="ZIP2" value="${result_data.data.ZIP2}" size=4 maxlength=3> --%>
                                            <!-- 											<select name="CITY2" id="CITY2" class="custom-select select-input input-width-125"  onchange="doquery2('#CITY2','#ZONE2')"> -->
<%-- 								          		<option value="#"><spring:message code="LB.Select"/><spring:message code="LB.D0059"/></option>   --%>
<%-- 								          		<c:forEach var="dataList" items="${city_data}"> --%>
<%-- 													<option>${dataList.CITY}</option> --%>
<%-- 												</c:forEach> --%>
                                            <!-- 											</select> -->
                                            <!-- 											<select name="ZONE2" id="ZONE2" class="custom-select select-input input-width-100" onchange="doquery3('#CITY2','#ZONE2', '#ZIP2')"> -->
<%-- 								          		<option value="#"><spring:message code="LB.Select"/><spring:message code="LB.D0060"/></option>  --%>
                                            <!-- 											</select> -->
<%-- 											<input type="text" id="ADDR21" name="ADDR21" class="text-input card-input" value="${result_data.data.ADDR21}" maxLength="4" size="4"/><spring:message code="LB.D1121"/> --%>
<%-- 											<input type="text" id="ADDR22" name="ADDR22" class="text-input card-input" value="${result_data.data.ADDR22}" maxLength="4" size="4"/><spring:message code="LB.D1122"/> --%>
<%-- 											<input type="text" id="ADDR23" name="ADDR23" class="text-input card-input" value="${result_data.data.ADDR23}" maxLength="2" size="4"/><spring:message code="LB.D1123"/> --%>
											
										</div>
	                                    <div class="ttb-input">
											<input type="text" id="ADDR2" name="ADDR2" placeholder="請輸入通訊地址 "
                                                   class="text-input validate[required]"
                                                   value="${result_data.data.ADDR2}" maxLength="24" size="42"/>
											
											<span id="hideblock_ADDR2">
												<input id="validate_CITY2" name="validate_CITY2" type="text" value="#"
                                                       class="text-input
													validate[funcCall[validate_CheckSelect['<spring:message code= "LB.D1118" />',CITY2,#]]]"
                                                       style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
												<input id="validate_ZONE2" name="validate_ZONE2" type="text" value="#"
                                                       class="text-input
													validate[funcCall[validate_CheckSelect['<spring:message code= "LB.D1490" />',ZONE2,#]]]"
                                                       style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
<%-- 												<input id="validate_ADDR21" name="validate_ADDR21" type="text" value="#" class="text-input  --%>
<%-- 													validate[funcCallRequired[validate_isChinese['<spring:message code= "LB.X1226" />',ADDR21]]]"  --%>
                                                <!-- 													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
<%-- 												<input id="validate_ADDR22" name="validate_ADDR22" type="text" value="#" class="text-input  --%>
<%-- 													validate[funcCallRequired[validate_isChinese['<spring:message code= "LB.X1227" />',ADDR22]]]"  --%>
                                                <!-- 													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
<%-- 												<input id="validate_ADDR23" name="validate_ADDR23" type="text" value="#" class="text-input  --%>
<%-- 													validate[funcCallRequired[validate_CheckNumber['<spring:message code= "LB.X1228" />',ADDR23]]]"  --%>
                                                <!-- 													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
											</span>
										</div>
									</span>
                            </div>
                            <!-- *********************************** -->
                            <div class="classification-block">
                                <p>職業資訊</p>
                                <p>( <span class="high-light">*</span> 為必填)</p>
                            </div>
                            <!-- *********************************** -->
                            <div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 										職業 -->
											<spring:message code="LB.D1132"/> <span class="high-light">*</span>
										</h4>
									</label>
									</span>
                                <span class="input-block">
										<div class="ttb-input">
											<select name="CAREER1" id="CAREER1"
                                                    class="custom-select select-input validate[required]">
												<option value=""><spring:message code="LB.Select"/><spring:message
                                                        code="LB.D1132"/></option>
									 			<option value="061100"><spring:message code="LB.D0871"/></option>
									 			<option value="061200"><spring:message code="LB.D0872"/></option>
									 			<option value="061300"><spring:message code="LB.D0873"/></option>  	     	
									 			<option value="061400"><spring:message code="LB.D0874"/></option>  	     	
									 			<option value="061410"><spring:message code="LB.D0081"/></option>    	     	
									 			<option value="061500"><spring:message code="LB.D0876"/></option>  	     	
									 			<option value="0615A0"><spring:message code="LB.D0877"/></option>  	     	
									 			<option value="0615B0"><spring:message code="LB.D0878"/></option>  	     	
									 			<option value="0615C0"><spring:message code="LB.D0879"/></option>  	     	
									 			<option value="0615D0"><spring:message code="LB.D0880"/></option> 	     	
									 			<option value="0615E0"><spring:message code="LB.D0881"/></option>  	     	
									 			<option value="0615F0"><spring:message code="LB.D0882"/></option>  	     	
									 			<option value="0615G0"><spring:message code="LB.D0883"/></option>  	     	
									 			<option value="0615H0"><spring:message code="LB.D0884"/></option>  	     	
									 			<option value="0615I0"><spring:message code="LB.D0885"/></option>  	     	
									 			<option value="0615J0"><spring:message code="LB.D0886"/></option>  	     	
									 			
									 			<option value="061610"><spring:message code="LB.D0887"/></option>  	     	
									 			<option value="061620"><spring:message code="LB.D0888"/></option>  	  
									 			<option value="061630"><spring:message code="LB.D0889"/></option>
									 			<option value="061640"><spring:message code="LB.D0890"/></option>
									 			<option value="061650"><spring:message code="LB.D0891"/></option>
									 			<option value="061660"><spring:message code="LB.D0892"/></option>
									 			<option value="061670"><spring:message code="LB.D0893"/></option>
									 			<option value="061680"><spring:message code="LB.D0894"/></option>
									 			<option value="061690"><spring:message code="LB.D0082"/></option>
									 			<option value="061691"><spring:message code="LB.D0083"/></option>
									 			<option value="061692"><spring:message code="LB.D0897"/></option>
									 			
									 			<option value="061700"><spring:message code="LB.D0898"/></option>  	     	
									 			<option value="069999"><spring:message code="LB.D0899"/></option>       											
											</select>
										</div>
									</span>
                            </div>
                            <!-- ********** -->
                            <div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<!-- 職稱代號  -->
											<spring:message code="LB.D1133"/> <span class="high-light">*</span>
										</h4>
									</label>
									</span>
                                <span class="input-block">
										<div class="ttb-input">
											
<%-- 											<select name="CAREER2" id="CAREER2" class="custom-select select-input" value="${result_data.data.CAREER2}"> --%>
                                            <!-- 												<option value="">請選擇職稱代號</option>  -->
                                            <!-- 									 			<option value="1">上/中/少將</option> -->
                                            <!-- 									 			<option value="1">院上/中/少將</option> -->
                                            <!-- 									 			<option value="1">院/部/次長</option> -->
                                            <!-- 									 			<option value="1">大專校長</option> -->
                                            <!-- 									 			<option value="1">總裁</option> -->
                                            <!-- 									 			<option value="1">醫/會計師</option> -->
                                            <!-- 									 			<option value="1">導演</option> -->
                                            <!-- 									 			<option value="1">中央民意代表</option> -->
                                            <!-- 									 			<option value="1">中研院院士</option> -->
                                            <!-- 									 			<option value="1">董/理事長</option> -->
                                            <!-- 									 			<option value="1">律/建築師</option> -->
                                            <!-- 									 			<option value="1">製作人</option> -->

                                            <!-- 									 			<option value="2">上/中/少校</option> -->
                                            <!-- 									 			<option value="2">地方縣市長</option> -->
                                            <!-- 									 			<option value="2">地方民意代表</option> -->
                                            <!-- 									 			<option value="2">教授/副教授</option> -->
                                            <!-- 									 			<option value="2">主任</option> -->
                                            <!-- 									 			<option value="2">總經理/幹事</option> -->
                                            <!-- 									 			<option value="2">董監事</option> -->
                                            <!-- 									 			<option value="2">電子/資訊/機械/土木技師</option> -->
                                            <!-- 									 			<option value="2">音樂/戲劇/舞蹈表演</option> -->

                                            <!-- 									 			<option value="3">上/中/少尉</option> -->
                                            <!-- 									 			<option value="3">局/處/司長</option> -->
                                            <!-- 									 			<option value="3">助理教授</option> -->
                                            <!-- 									 			<option value="3">大專講師</option> -->
                                            <!-- 									 			<option value="3">協理/處長/主任</option> -->
                                            <!-- 									 			<option value="3">專業顧問</option> -->
                                            <!-- 									 			<option value="3">飛機駕駛</option> -->
                                            <!-- 									 			<option value="3">代書</option> -->
                                            <!-- 									 			<option value="3">媒體記者/播報/主持</option> -->

                                            <!-- 									 			<option value="4">士/官兵</option> -->
                                            <!-- 									 			<option value="4">參事</option> -->
                                            <!-- 									 			<option value="4">專門委員</option> -->
                                            <!-- 									 			<option value="4">高中/國中/小學校長</option> -->
                                            <!-- 									 			<option value="4">經理</option> -->
                                            <!-- 									 			<option value="4">廠長</option> -->
                                            <!-- 									 			<option value="4">藥劑/醫技/護理人員</option> -->
                                            <!-- 									 			<option value="4">服裝/造型/美容設計</option> -->

                                            <!-- 									 			<option value="5">警政首長/局長</option> -->
                                            <!-- 									 			<option value="5">組/科/課長</option> -->
                                            <!-- 									 			<option value="5">高中/國中/小學主任</option> -->
                                            <!-- 									 			<option value="5">科/課長</option> -->
                                            <!-- 									 			<option value="5">襄理</option> -->
                                            <!-- 									 			<option value="5">機電/土木/汽車修護技工</option> -->
                                            <!-- 									 			<option value="5">翻譯</option> -->
                                            <!-- 									 			<option value="5">寫作</option> -->
                                            <!-- 									 			<option value="5">攝影</option> -->
                                            <!-- 									 			<option value="5">圖畫</option> -->

                                            <!-- 									 			<option value="6">警局隊長/巡佐</option> -->
                                            <!-- 									 			<option value="6">科員</option> -->
                                            <!-- 									 			<option value="6">基層公務員</option> -->
                                            <!-- 									 			<option value="6">高中/國中/小學教師</option> -->
                                            <!-- 									 			<option value="6">專門技術人員</option> -->
                                            <!-- 									 			<option value="6">廚師</option> -->
                                            <!-- 									 			<option value="6">專業汽車駕駛</option> -->
                                            <!-- 									 			<option value="6">計程車駕駛</option> -->
                                            <!-- 									 			<option value="6">空服員</option> -->
                                            <!-- 									 			<option value="6">船員</option> -->

                                            <!-- 									 			<option value="7">基層警員</option> -->
                                            <!-- 									 			<option value="7">法/檢察/書記官</option> -->
                                            <!-- 									 			<option value="7">職/辦事/店員</option> -->
                                            <!-- 									 			<option value="7">保全人員</option> -->
                                            <!-- 									 			<option value="7">大樓管理員</option> -->
                                            <!-- 									 			<option value="7">攤商/直銷/仲介</option> -->

                                            <!-- 									 			<option value="8">軍警/公職約聘人員</option> -->
                                            <!-- 									 			<option value="8">學生</option> -->
                                            <!-- 									 			<option value="8">工友</option> -->
                                            <!-- 									 			<option value="8">駕駛</option> -->
                                            <!-- 									 			<option value="8">清潔人員</option> -->
                                            <!-- 									 			<option value="8">職業運動員</option> -->
                                            <!-- 									 			<option value="8">教練</option> -->
                                            <!-- 									 			<option value="8">農/漁民</option> -->
                                            <!-- 									 			<option value="8">屠宰飼養</option> -->

                                            <!-- 									 			<option value="9">退役軍警</option> -->
                                            <!-- 									 			<option value="9">退休公務員</option> -->
                                            <!-- 									 			<option value="9">退休教員</option> -->
                                            <!-- 									 			<option value="9">退休職員</option> -->
                                            <!-- 									 			<option value="9">宗教服務</option> -->
                                            <!-- 									 			<option value="9">家管</option> -->
                                            <!-- 											</select> -->
											<label><input class="text-input validate[required]"
                                                          value="${result_data.data.CAREER2}" name="SRCFUNDDESC"
                                                          id="SRCFUNDDESC" type="text" size="12" placeholder="(請選擇職稱項目)"
                                                          disabled/></label>
											<input type="button" value="選擇" class="ttb-sm-btn btn-flat-gray"
                                                   name="CAREERNO"
                                                   onClick="window.open('${__ctx}/ONLINE/APPLY/apply_deposit_account_job_name')">
											<input type="hidden" name="CAREER2" id="CAREER2"
                                                   value="${result_data.data.CAREER2}">
											<input id="validate_CAREER2" name="validate_CAREER2" type="text"
                                                   class="text-input validate[required]"
                                                   style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;"/>
										</div>
									</span>
                            </div>
                            <!-- ********** -->
                            <div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 										任職機構 -->
											<spring:message code="LB.D1074"/> <span class="high-light">*</span>
										</h4>
									</label>
									</span>
                                <span class="input-block">
										<div class="ttb-input">
											<input type="text" id="EMPLOYER" name="EMPLOYER" placeholder="請輸入任職機構"
                                                   class="text-input validate[required]"
                                                   value="${result_data.data.EMPLOYER}" maxLength="30" size="30"
                                                   onkeyup="ValidateValue(this)"
                                                   onchange="value=value.replace(/[^\a-\z\A-\Z0-9\u4E00-\u9FA5]/g,'')"
                                                   onpaste="value=value.replace(/[^\a-\z\A-\Z0-9\u4E00-\u9FA5]/g,'')"/>
										</div>
									</span>
                            </div>
                            <!-- ********** -->
                            <div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
<!-- 										任職機構 -->
											您所任職機構是否涉及高風險項目<span class="high-light">*</span>
										</h4>
									</label>
									</span>
                                <span class="input-block">
										<div class="ttb-input">
	                                        <label class="radio-block" for="HIRISK_N">
	                                        	否
	                                            <input type="radio" name="HIRISK0" id="HIRISK_N" value="N"
                                                       onclick="showHIRISK(true);"/>
	                                            <span class="ttb-radio"></span>
	                                        </label>
	                                        <br>
	                                        <label class="radio-block" for="HIRISK_Y">
												是
	                                            <input type="radio" name="HIRISK0" id="HIRISK_Y" value="Y"
                                                       onclick="showHIRISK(false);"/>
	                                            <span class="ttb-radio"></span>
	                                        </label>
	                                        <div id="HIRISK_block">
		                                        項目內容<span class="high-light">*</span>
												<select name="HIRISK" id="HIRISK1"
                                                        class="custom-select select-input validate[required]">
													<option value="">請選擇項目</option> 
													<option value="E">第三方支付服務業</option> 
										 			<option value="F">油品海運貿易業</option>										
												</select>
								 			</div>
										</div>
									</span>
                            </div>
                            <!-- ********** -->

                        </div>
                        <!--button 區域 -->
                        <!-- 重新輸入 -->
                        <input type="button" name="CMBACK" id="CMBACK" value="<spring:message code="LB.X0318"/>"
                               class="ttb-button btn-flat-gray">
                        <input id="CMSUBMIT" name="CMSUBMIT" type="button" class="ttb-button btn-flat-orange"
                               value="<spring:message code="LB.X0080"/>"/>
                        <!--                     button 區域 -->
                    </div>
                </div>
            </form>
            <!-- 				20211028 Han 要求新增  -->
            <div class="text-left">
                <!-- 		說明： -->
                <ol class="list-decimal text-left description-list">
                    <p><spring:message code="LB.Description_of_page"/></p>
                    <li><spring:message code="LB.Apply_Deposit_Account_P3_D2"/></li>
                </ol>
            </div>

        </section>
    </main>
</div>
<!-- content row END -->
<%@ include file="../index/footer.jsp" %>
</body>
</html>