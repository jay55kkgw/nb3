<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
</head>
 <body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %>  
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 質借功能取消     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Pledged_Time_Deposit_Function_Application_Cancellation" /></li>
		</ol>
	</nav>



		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 --> 
 		<div class="content row">
 		   <%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->

 		
 		<main class="col-12">	
		
		<!-- 		主頁內容  -->

			<section id="main-content" class="container">
		
				<h2><spring:message code="LB.Pledged_Time_Deposit_Function_Application_Cancellation" /></h2>				
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block no-l-display-btn">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Execution_option" /></option>
						<!-- 重新輸入-->
						<option value="reEnter">
							<spring:message code="LB.Re_enter" />
						</option>
					</select>
				</div>
				<div id="step-bar">
				    <ul>
				       <ul>
						<li class="finished">
							<spring:message code="LB.Enter_data" /><!-- 輸入資料 -->
						</li>
						<li class="active">
							<spring:message code="LB.Confirm_data" /><!-- 確認資料 -->
						</li>
						<li class="">
							<spring:message code="LB.Transaction_complete" /><!-- 交易完成 -->
						</li>
					</ul>
				    </ul>
				</div>
				<div class="main-content-block row">
					<div class="col-12 tab-content">
					<div class="ttb-message">
						<p><spring:message code="LB.Confirm_transaction_data" /></p>
					</div>
				<form method="post" id="formId" action="${__ctx}/NT/ACCT/collateral_result">
					<input type="hidden" name="TXTOKEN" value="${transfer_data.data.TXTOKEN}"><!-- 防止不正常交易 -->
						<!-- 交易機制所需欄位 -->
					<input type="hidden" id="jsondc" name="JSON:DC" value='${collateral_confirm.data.jsondc}'>
					<input type="hidden" id="ISSUER" name="ISSUER" value="">
					<input type="hidden" id="ACNNO" name="ACNNO" value="">
					<input type="hidden" id="TRMID" name="TRMID" value="">
					<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
					<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
					<input type="hidden" id="TAC" name="TAC" value="">
					<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
						<div class="ttb-input-block">							
							<!-- 帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
								<h4><spring:message code="LB.Account" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										${transfer_data.data.FDPACN }
				           				<input name="FDPACN" type="hidden" value="${transfer_data.data.FDPACN}" />
									</div>
								</span>
							</div>
<!-- 					交易機制 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4><spring:message code="LB.Transaction_security_mechanism" /></h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block"> 
											<spring:message	code="LB.SSL_password" /> 
											<input type="radio" name="FGTXWAY" id="CMSSL" value="0" checked/> 
											<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input" >
										<input type="password" name="CMPASSWORD" id="CMPASSWORD" size="8" maxlength="8" class="text-input" value="" placeholder="<spring:message code="LB.Please_enter_password"/>" />
										<input type="hidden" name="PINNEW" id="PINNEW" vaule=""/>
									</div>
<!-- 								ikey用 -->
									<!-- 舊網銀邏輯 XMLCOD欄位為2,4,5,6，則代表使用者可以使用IKEY -->
								<c:if test = "${sessionScope.xmlcod eq '2' || sessionScope.xmlcod eq '4' ||sessionScope.xmlcod eq '5' ||sessionScope.xmlcod eq '6' }">
									<div class="ttb-input">
										<label class="radio-block">
											<input type="radio" name="FGTXWAY" id="CMIKEY" value="1" />
											<label><spring:message code="LB.Electronic_signature" /></label>
											<span class="ttb-radio"></span>
										</label>
									</div>
								</c:if>
									<div class="ttb-input" name="idgate_group" style="display:none" onclick="hideCapCode('Y')">		 
										<label class="radio-block">裝置推播認證(請確認您的行動裝置網路連線是否正常，及推播功能是否已開啟)
											<input type="radio" id="IDGATE" 	name="FGTXWAY" value="7"> 
											<span class="ttb-radio"></span>
										</label>		 
									</div>
								</span>
							</div>
						</div>
						<div>
						
						</div>					
					</form>
					<input type="button" id="previous" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />" />
<%-- 						<input type="reset"  class="ttb-button btn-flat-gray no-l-disappear-btn" value="<spring:message code="LB.Re_enter" />" /> --%>
						<input type="button" id="CMSUBMIT" name="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />						
					</div>
				</div>
<!-- 				</form> -->
			</section>
			<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->
    <%@ include file="../index/footer.jsp"%>
<!--   Js function -->
    <script type="text/JavaScript">
    $(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});

    function init(){
    	
    	initFootable();
		//上一頁按鈕
		$("#previous").click(function() {
			initBlockUI();
			fstop.getPage('${pageContext.request.contextPath}'+'/NT/ACCT/collateral','', '');
		});
    	
    	$("#formId").validationEngine({binded:false,promptPosition: "inline" });
    	
    	
    	// 確認鍵 Click
		goOn();
    	
    }
    
	//選項
 	function formReset() {
		if ($('#actionBar').val()=="reEnter"){
			 		document.getElementById("#formId").reset();
			 		$('#actionBar').val("");
		}
	}
	
 // 確認鍵 Click
	function goOn() {
		$("#CMSUBMIT").click( function(e) {
			// 送出進表單驗證前將span顯示
			$("#hideblock").show();
			console.log("submit~~");
			
			if($('input:radio[name="FGTXWAY"]:checked').val()=='0'){
				$('#CMPASSWORD').addClass("validate[required]")
			}else{
				$('#CMPASSWORD').removeClass("validate[required]")
			}
			// 表單驗證
			if ( !$('#formId').validationEngine('validate') ) {
				e.preventDefault();
			} else {
				// 通過表單驗證
				processQuery();
			}
		});
	}
	// 通過表單驗證準備送出
	function processQuery(){
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
		console.log("fgtxway: " + fgtxway);
		// 交易機制選項
		switch(fgtxway) {
			case '0':
 				$("#formId").validationEngine('detach');
				$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
				initBlockUI();
 	  			$("#formId").submit(); 
				break;
			case '1':
				//Ikey
				useIKey();
				break;
			case '7'://IDGATE認證		 
	            idgatesubmit= $("#formId");		 
	            showIdgateBlock();		 
	            break;
			default:
				//請選擇交易機制
				//alert('<spring:message code= "LB.Alert001" />');
				errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert001' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
		}
	}
 		
   	
 	</script>
</body>
</html>