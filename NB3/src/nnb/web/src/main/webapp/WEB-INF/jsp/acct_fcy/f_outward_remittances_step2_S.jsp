<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>  
	<script type="text/javascript">
		var idgatesubmit= $("#formId");		 
    
		// 初始化
		$(document).ready(function () {
			init();
		});

		function init() {
			//表單驗證
			$("#formId").validationEngine({binded: false,promptPosition: "inline"});
			// 確認鍵 click
			goOn();
			//上一頁按鈕
			back();
			//交易類別change 事件
			changeFgtxway();

		}
		
		//上一頁按鈕
		function back(){
			$("#CMBACK").click(function() {
				var action = '${__ctx}/FCY/REMITTANCES/f_outward_remittances_step1';
				$('#back').val("Y");
				$("form").attr("action", action);
				$("#formId").validationEngine('detach');
				initBlockUI();
				$("form").submit();
			});
		}
		// 確認鍵 click
		function goOn() {
			$("#CMSUBMIT").click(function (e) {
				if (!$('#formId').validationEngine('validate')) {
					e = e || window.event; // for IE
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					initBlockUI(); //遮罩
					processQuery();
				}
			});
		}
		
		// 交易機制選項
		function processQuery() {
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
			switch (fgtxway) {
				case '0':
					$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
					$('#CMPASSWORD').val("");
					initBlockUI();//遮罩
					$("#formId").submit();
					break;
				// IKEY
				case '1':
					useIKey();
					break;
	           case '7'://IDGATE認證		 
	               idgatesubmit= $("#formId");		 
	               showIdgateBlock();		 
	               break;
				default:
					//alert("<spring:message code= "LB.Alert001" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert001' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			}
		}
	

		//交易類別change 事件
		function changeFgtxway() {
			$('input[type=radio][name=FGTXWAY]').change(function () {
				console.log(this.value);
				if (this.value == '0') {
					$("#CMPASSWORD").addClass("validate[required]")
				} else if (this.value == '1' || this.value == '7') {
					$("#CMPASSWORD").removeClass("validate[required]");
				}
			});
		}
	</script>
</head>

<body>
	<!-- 交易機制所需畫面    -->
	<%@ include file="../component/trading_component.jsp"%>
	    <!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %>  
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
	<!-- 匯出匯款     -->
    		<li class="ttb-breadcrumb-item"><spring:message code="LB.W0286" /></li>
    <!-- 匯出匯款     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0286" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!--外匯匯出匯款 -->
				<h2>
					<spring:message code="LB.W0286" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form autocomplete="off" method="post" id="formId" action="${__ctx}/FCY/REMITTANCES/f_outward_remittances_result">
					<input type="hidden" id="TXID" name="TXID" value="F002S">
					<%--  TXTOKEN  防止重送代碼--%>
                    <input type="hidden" name="TXTOKEN" value="${f_outward_remittances_step2_S.data.TXTOKEN}" />
					<%-- 驗證相關 --%>
					<input type="hidden" id="PINNEW" name="PINNEW" value="">
					<input type="hidden" id="jsondc" name="jsondc" value='${f_outward_remittances_step2_S.data.jsondc}'>
					<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<div class="ttb-message">
									<span>
										<!-- 請確認匯出匯款資料 -->
										<spring:message code="LB.X0039" />
									</span>
								</div>
								<!-- 付款日期 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0289" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>${f_outward_remittances_step2_S.data.PAYDATE}</p>
										</div>
									</span>
								</div>
								<!-- 收款銀行資料 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.X0037" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color">SWIFT CODE</span>
										</div>
										<div class="ttb-input">
											<p>${f_outward_remittances_step2_S.data.FXRCVBKCODE}</p>
										</div>
									</span>
								</div>
								<!-- 付款帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0290" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												${f_outward_remittances_step2_S.data.CUSTACC}
											</p>
										</div>
									</span>
								</div>
								<!-- 付款幣別 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.D0448" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												${f_outward_remittances_step2_S.data.display_PAYCCY}
											</p>
										</div>
									</span>
								</div>
								<!-- 收款帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0292" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												${f_outward_remittances_step2_S.data.BENACC}
											</p>
										</div>
									</span>
								</div>
								<!-- 收款幣別 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0293" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												${f_outward_remittances_step2_S.data.display_REMITCY}
											</p>
										</div>
									</span>
								</div>
								<!-- 匯款金額 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0150" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p class="high-light d-inline-block">
												${f_outward_remittances_step2_S.data.display_PAYREMIT}
											</p>
										</div>
									</span>
								</div>
								<!-- 收款人身份別 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Payee_identity" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												${f_outward_remittances_step2_S.data.display_BENTYPE}
											</p>
										</div>
									</span>
								</div>
								<!-- 匯款分類項目 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0298" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												${f_outward_remittances_step2_S.data.SRCFUNDDESC}
											</p>
										</div>
									</span>
								</div>
								<!-- 匯款分類說明 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0299" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												${f_outward_remittances_step2_S.data.FXRMTDESC}
											</p>
										</div>
									</span>
								</div>
								<!-- Email通知 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0300" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color"><spring:message code="LB.Email" /></span>
										</div>
										<div class="ttb-input">
											<p>
												${f_outward_remittances_step2_S.data.CMTRMAIL}
											</p>
										</div>
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color"><spring:message code="LB.Summary" /></span>
										</div>
										<div class="ttb-input">
											<p>
												${f_outward_remittances_step2_S.data.CMMAILMEMO}
											</p>
										</div>
									</span>
								</div>
								<!-- 匯款附言 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0303" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												${f_outward_remittances_step2_S.data.MEMO1}
											</p>
										</div>
									</span>
								</div>
								<!-- 手續費負擔別 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0155" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												${f_outward_remittances_step2_S.data.DETCHG}
											</p>
										</div>
									</span>
								</div>

								<!-- 費用扣款帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0305" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												${f_outward_remittances_step2_S.data.COMMACC}
											</p>
										</div>
									</span>
								</div>
								<!-- 費用扣款幣別 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0306" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												${f_outward_remittances_step2_S.data.display_COMMCCY}
											</p>
										</div>
									</span>
								</div>
								<!-- 交易機制 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.Transaction_security_mechanism" />
									</label>
								</span>
								<span class="input-block">
									<!-- 交易密碼SSL -->
									<div class="ttb-input">
										<label class="radio-block">
											<spring:message code="LB.SSL_password" />
											<input type="radio" name="FGTXWAY" checked="checked" value="0">
											<span class="ttb-radio"></span>
										</label>
									</div>
									<!--請輸入密碼 -->
									<div class="ttb-input">
										<spring:message code="LB.Please_enter_password" var="pleaseEnterPin"/>
										<input type="password" id="CMPASSWORD" name="CMPASSWORD" class="text-input validate[required]" maxlength="8" placeholder="${plassEntpin}">
									</div>
									<div class="ttb-input" name="idgate_group" style="display:none" onclick="hideCapCode('Y')">		 
                                       <label class="radio-block">裝置推播認證(請確認您的行動裝置網路連線是否正常，及推播功能是否已開啟)
	                                       <input type="radio" id="IDGATE" 	name="FGTXWAY" value="7"> 
	                                       <span class="ttb-radio"></span>
                                       </label>		 
                                   </div>
									<!--電子簽章(請載入載具i-key) -->
									<div class="ttb-input">
										<label class="radio-block">
											<spring:message code="LB.Electronic_signature" />
											<input type="radio" name="FGTXWAY" id="CMIKEY" value="1" />
											<span class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>
							</div>
							<!--button 區域 -->
							<input class="ttb-button btn-flat-gray" name="CMBACK" id="CMBACK" type="button" value="<spring:message code="LB.Back_to_previous_page" />" />
							<input class="ttb-button btn-flat-orange" name="CMSUBMIT" id="CMSUBMIT" type="button" value="<spring:message code="LB.X1147" />" />
							<!--button 區域 -->
						</div>
					</div>
					<font color="#FF0000"><spring:message code="LB.Confirm_exchange_rate" />
					</font>
					<div class="text-left">
						<!-- 						說明： -->
						<!-- <spring:message code="LB.Description_of_page"/>: -->
					</div>
				</form>
			</section>
			<!-- main-content END -->
		</main>
	</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>