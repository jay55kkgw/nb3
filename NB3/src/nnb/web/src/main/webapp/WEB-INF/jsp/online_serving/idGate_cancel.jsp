<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp"%>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>

<!-- 讀卡機所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/VAckisIEbrower.js"></script>
	<script type="text/javascript" src="${__ctx}/component/util/Pkcs11ATLAgent.js"></script>

	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/checkIdProcess.js"></script>

<script type="text/javascript">
var urihost = "${__ctx}";
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 1000);
		// 初始化驗證碼
		setTimeout("initKapImg()", 200);
		// 生成驗證碼
		setTimeout("newKapImg()", 300);
		
	});

	function init() {
		
		//表單驗證
		$("#formId").validationEngine({ binded: false, promptPosition: "inline" });

		$("#CMSUBMIT").click(function(e) {
			if (!$('#formId').validationEngine('validate')){
	        	e.preventDefault();
 			}else{
 				errorBlock(
						null, 
						null,
						['請再次確認是否註銷此裝置?'], 
						'<spring:message code= "LB.Confirm" />', 
						'<spring:message code= "LB.Cancel" />'
					);
				//驗證碼驗證
// 				var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
// 				var capData = $("#formId").serializeArray();
// 				var capResult = fstop.getServerDataEx(capUri,capData,false);
				
// 				//驗證結果
// 				if (capResult.result) {
// 					initBlockUI();
// 					$("#formId").submit();
// 				}
// 				else{
// 					//失敗重新產生驗證碼
// 					errorBlock(null, null, ["<spring:message code='LB.X1082' />"],
// 						'<spring:message code= "LB.Quit" />', null);
// 					refreshCapCode();
// 				}
 			}
		});
		
		$("#errorBtn1").click(function(){
			initBlockUI();
// 			$("#formId").submit();
			
			var rdata = {};
			uri = '${__ctx}'+"/ONLINE/SERVING/idGateCancel_aj";
			setTimeout("initBlockUI()", 10);
			fstop.getServerDataEx(uri,rdata,false,doCancel);
		});
		
		$("#errorBtn2").click(function(){
			isconfirm_a = false;
			$('#error-block').hide();
		});
		
		$("#pageback").click(function(e) {
			$("#formId").validationEngine('detach');
			$("#formId").attr("action","${__ctx}/ONLINE/SERVING/idgate");
			$("#formId").submit();
		});

	}//init END

	function doCancel(data){
		console.log("data.json: " + JSON.stringify(data));
		setTimeout("unBlockUI(initBlockId)", 500);
		if(null == data || 'undefined' == data ){
			alert('系統異常');
			return;
		}
		 console.log("data.result>>"+data.result);
		if( data.result){
			$("#formId").attr("action","${__ctx}/ONLINE/SERVING/idgate");
			$("#formId").submit();
		}
		else{
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Transaction_code' />: " + data.msgCode , "\n" , "<spring:message code='LB.Transaction_message' />: " + data.message], 
					'<spring:message code= "LB.Quit" />', 
					null
			);
		}
	}
	
	// 驗證碼刷新
	function changeCode() {
		$('input[name="capCode"]').val('');
		// 大小版驗證碼用同一個
		console.log("changeCode...");
		$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

		// 登入失敗解遮罩
		unBlockUI(initBlockId);
	}
	
	// 初始化驗證碼
	function initKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
	}
	
	// 生成驗證碼
	function newKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').click(function() {
			$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
		});
	}
	
	
</script>

</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- IDGATE 申請   確認頁 (交易機制)   N960 拿手機號碼-->
			<li class="ttb-breadcrumb-item active" aria-current="page">註銷裝置</li>
		</ol>
	</nav>




	<!--     左邊menu 及登入資訊-->
	<div class="content row">

		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> <!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<form method="post" name="formId" id="formId" action="${__ctx}/ONLINE/SERVING/idgate_cancel_result">
				<h2>
					註銷裝置
				</h2>

				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
							
							<div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4>交易項目</h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                	註銷裝置
                                </span>
                            </div>
							
							<!--裝置名稱-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4>裝置名稱</h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                	${idGate_cancel.DEVICENAME}
                                </span>
                            </div>
							
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p style="color: red;">
											※提醒您，註銷此裝置後，此裝置無法使用交易裝置認證機制...
										</p>
									</div>
								</span>
							</div>
							
<!-- 							<div class="ttb-input-item row"> -->
<!-- 									<span class="input-title">  -->
<!-- 										<label> -->
<!-- 											<h4> -->
<!-- 												驗證碼 -->
<%-- 												<spring:message code="LB.Captcha" /> --%>
<!-- 											</h4> -->
<!-- 										</label> -->
<!-- 									</span>  -->
<!-- 									<span class="input-block"> -->
<!-- 										<div class="ttb-input"> -->
<!-- 											<input id="capCode" type="text" class="ttb-input text-input input-width-125" -->
<%-- 												name="capCode" placeholder="<spring:message code='LB.X1702' />" maxlength="6" autocomplete="off"> --%>
<!-- 											<img name="kaptchaImage" src="" class="verification-img"/> -->
<!-- 											重新產生驗證碼 -->
<%-- 											<input type="button" name="reshow" class="ttb-sm-btn btn-flat-gray" onclick="changeCode()" value="<spring:message code='LB.Regeneration' />" /><!-- 重新產生驗證碼 --> --%>
<%-- 											<span class="input-remarks"><spring:message code="LB.X1972" /></span><!-- 請注意：英文不分大小寫，限半形字元 --> --%>
<!-- 										</div> -->
<!-- 									</span> -->
<!-- 								</div> -->
							
							
						</div>
						<input type="button" id="pageback" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />" />
						<input type="button" id="CMSUBMIT" name="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
					</div>
				</div>
			</form>

		</section>
		<!-- 		main-content END --> </main>
	</div>
	<!-- 	content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>