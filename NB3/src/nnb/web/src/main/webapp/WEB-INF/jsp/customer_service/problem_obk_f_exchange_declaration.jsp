<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="card-detail-block">
	<div class="card-center-block">
		<!-- Q1 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block" data-toggle="collapse" href="#popup2-1" aria-expanded="true" aria-controls="popup2-1">
	  		  <div class="row">
	        		<span class="col-1">Q1</span>
	            		<div class="col-11">
	                		<span>「結售外匯來源」之填報</span>
	            		</div>
	    		</div>
			</a>
			<div id="popup2-1" class="ttb-pup-collapse collapse" role="tabpanel">
	    		<div class="ttb-pup-body">
	        		<ul class="ttb-pup-list">
	            		<li>
	                		<p>依結售外匯之來源填寫。</p>
	            		</li>
	    		</div>
			</div>
		</div>
		<!-- Q2 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block" data-toggle="collapse" href="#popup2-2" aria-expanded="true" aria-controls="popup2-2">
	  		  <div class="row">
	        		<span class="col-1">Q2</span>
	            		<div class="col-11">
	                		<span>「申報日期」之填報</span>
	            		</div>
	    		</div>
			</a>
			<div id="popup2-2" class="ttb-pup-collapse collapse" role="tabpanel">
	    		<div class="ttb-pup-body">
	        		<ul class="ttb-pup-list">
	            		<li>
	                		<p>依申報結售外匯之日期填寫。</p>
	            		</li>
	    		</div>
			</div>
		</div>
		<!-- Q3 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block" data-toggle="collapse" href="#popup2-3" aria-expanded="true" aria-controls="popup2-3">
				<div class="row">
					<span class="col-1">Q3</span>
					<div class="col-11">
						<span>「申報義務人」之填報</span>
					</div>
				</div>
			</a>
			<div id="popup2-3" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div class="ttb-result-list terms">
								<ol>
									<li>(一)、 中華民國境內新臺幣五十萬元以上等值外匯收支或交易之資金所有者為申報義務人，除經中央銀行同意或中央銀行另有規定外，應以自己名義申報。
									</li>
								
									<li>(二)、 申報義務人委託他人辦理新臺幣結匯申報時，應以委託人名義填報，並出具委託書及檢附委託人、受託人之身分證明文件。
									</li>
								
									<li>(三)、 非居住民法人應出具授權書，授權其在中華民國境內之自然人或法人為申報義務人，並以經授權之代表人或代理人名義填報，及敘明代理之事實。但境外非中華民國金融機構不得以匯入款項辦理結售。
									</li>
								</ol>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q4 -->
		<div class="ttb-pup-block" role="tab">
	    	<a role="button" class="ttb-pup-title collapsed d-block" data-toggle="collapse" href="#popup2-4" aria-expanded="true" aria-controls="popup2-4">
	        	<div class="row">
	            	<span class="col-1">Q4</span>
	                	<div class="col-11">
	                    	<span>「申報義務人登記證號」之填報</span>
	                    </div>
	            </div>
	        </a>
	        <div id="popup2-4" class="ttb-pup-collapse collapse" role="tabpanel">
	        	<div class="ttb-pup-body">
	            	<ul class="ttb-pup-list">
	            		
	            		<li>
	            			<div class="ttb-result-list terms">
	            			<ol>
	            				<li>(一)、 公司、行號
	            					<ol>
					                	<li>填列主管機關核准設立之統一編號，並提供設立登記表或最近之變更登記表影本。
					                    </li>
					                    <li>僑外投資事業籌備處，請填列主管機關核准投資文號及主管機關名稱。
					                    </li>
				                    </ol>
			                    </li>
			                    <li>(二)、 團體
	            					<ol>
					                	<li>請填列主管機關核准設立證照上之統一編號。
					                    </li>
					                    <li>前述證照上無統一編號者，請填列設立登記主管機關名稱、登記證號以及稅捐稽徵單位編配之扣繳單位統一編號。
					                    </li>
					                    <li>會計師事務所、律師事務所、診所等比照團體填報。
					                    </li>
				                    </ol>
			                    </li>
			                    <li>(三)、 我國國民
	            					<ol>
					                	<li>領有國民身分證者：請填列中華民國國民身分證統一編號及出生日期。
					                    </li>
					                    <li>持內政部入出境管理局核發之中華民國臺灣地區居留證之華僑：請填列居留證號碼(如居留證上載有統一證號者，請填列統一證號)及出生日期。
					                    </li>
					                    <li>持僑務委員會核發之華僑身分證明文件及回國投資購置房屋證明文件者：請填列華僑身分證明文號。
					                    </li>
					                    
				                    </ol>
			                    </li>
	            				<li>(四)、 外國人
	            					<ol>
					                	<li>持外僑居留證者：請填列居留證號碼(如居留證上載有統一證號者，請填列統一證號)、出生日期及居留證發給日期、到期日期。
					                    </li>
					                    <li>持外國護照者：請於(無外僑居留證者)欄內填列國別及護照號碼。
					                    </li>
					                    <li>持中華民國護照，但未領有中華民國國民身分證之華僑：請於(無外僑居留證者)欄內填列國別為發證地所在國及護照號碼，並加註發證單位。
					                    </li>
					                    <li>香港及澳門地區居民：請於(無外僑居留證者)欄內填列國別為香港地區或澳門地區，及證照號碼為入出境證或臨時入境停留通知單號碼。
					                    </li>
					                    <li>大陸人士：請於(無外僑居留證者)欄內填列國別為大陸地區，及證照號碼為旅行證或居留證號碼。
					                    </li>
				                    </ol>
			                    </li>
			            	</ol>
			            	</div>
	                    </li>
	                </ul>
	        	</div>
	      	</div>
		</div>
	</div>
</div>