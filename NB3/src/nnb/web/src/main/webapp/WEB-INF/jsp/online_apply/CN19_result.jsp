<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {
		// 將.table變更為footable
		initFootable();
	});

	

	function buttonAction(clicked_id){
		if(clicked_id=="BACKTO"){
			action = '${__ctx}/ONLINE/APPLY/online_apply'
			$("form").attr("action", action);
			$("form").submit();
		}else if(clicked_id=="PREVIOUS"){
			action = '${__ctx}/ONLINE/APPLY/shield_apply_p3'
			$("form").attr("action", action);
			$("form").submit();
		}else if(clicked_id=="CLOSEWIN"){
			window.close();
		}
	}

	
</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Register" /></li>
    <!-- 隨護神盾申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1573" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<!-- 申請隨護神盾 -->
			<h2><spring:message code="LB.D1573"/></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<div id="step-bar">
				<ul>
				    <li class="finished"><spring:message code="LB.D0176"/></li>
				    <li class="finished"><spring:message code="LB.X2066"/></li>
				    <li class="active"><spring:message code="LB.X2067"/></li>
			    </ul>
			</div>
			<form id="formId" method="post" action="">
				<input type="hidden" id="TYPE" name="TYPE" value="14"> 
				<div class="main-content-block row">
					<div class="col-12">
<!-- 						<div class="CN19-1-header"> -->
<!-- 							<div class="logo"> -->
<%-- 								<img src="${__ctx}/img/logo-1.svg"> --%>
<!-- 							</div> -->
<!-- 							常用網址超連結 -->
<!-- 							<div class="text-right hyperlink"> -->
<!-- 								臺灣企銀首頁 -->
<%-- 								<a href="#" onClick="window.open('https://www.tbb.com.tw');" style="text-decoration: none"> <spring:message code="LB.TAIWAN_BUSINESS_BANK"/> </a> <strong><font color="#e65827">|</font></strong>  --%>
<!-- 								網路ATM -->
<%-- 								<a href="#" onClick="window.open('https://eatm.tbb.com.tw/');" style="text-decoration: none"> <spring:message code="LB.Web_ATM"/> </a><strong><font color="#e65827">|</font></strong> 	 --%>
<!-- 								意見信箱  -->
<%-- 								<a href="#" onClick="window.open('https://www.tbb.com.tw/q3.html?');" style="text-decoration: none"> <spring:message code="LB.Customer_service"/> </a> --%>
<!-- 							</div> -->
<!-- 						</div> -->
						
						
						<div>
							<c:if test="${CN19_result.data.TOPMSG eq '0000' }">
							
							<h4><spring:message code="LB.D1583"/></h4>
							<p　style="color:red ; text-align: left"><spring:message code="LB.X1170"/>
							${CN19_result.data.DATAPL}&nbsp;${CN19_result.data.DATTIM}　<spring:message code="LB.X1171"/></p>
							<p style="color:red "><spring:message code="LB.X1172"/></p>
							</c:if>
							<c:if test="${CN19_result.data.TOPMSG != '0000' }">
								<ul class="ttb-result-list">
									<li>
										<h3><spring:message code="LB.Transaction_code" /></h3>
										<p>${CN19_result.data.msgCode}</p>
									</li>
									<li>
										<h3><spring:message code="LB.Transaction_message" /></h3>
										<p>${CN19_result.data.msgName}</p>
									</li>
								</ul>
							</c:if>
						</div>
						
						<c:if test="${CN19_result.data.TOPMSG != '0000' }">
						<!-- if 失敗 -->
						<input type="button" id="PREVIOUS" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" onclick="buttonAction(this.id)"/>
						<input type="button" id="CLOSEWIN" class="ttb-button btn-flat-gray" value="<spring:message code="LB.X0194" />" onclick="buttonAction(this.id)"/>
						</c:if>
						
						<c:if test="${CN19_result.data.TOPMSG eq '0000' }">
						<!-- if 成功 -->
						<input type="button" id="BACKTO" class="ttb-button btn-flat-orange" value="<spring:message code="LB.X2128"/> " onclick="buttonAction(this.id)"/>
						</c:if>
					</div>
				</div>
			</form>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>