<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript"
	src="${__ctx}/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css"
	href="${__ctx}/css/validationEngine.jquery.css">
<link rel="stylesheet" type="text/css"
	href="${__ctx}/css/jquery.datetimepicker.css">

<script type="text/javascript">
	$(document).ready(function() {
		init();
		initKapImg();
	});
	function init() {
		$("#CMSUBMIT").click( function(e) {
			// 送出進表單驗證前將span顯示
			setTimeout("initBlockUI()", 10);
			
			console.log("submit~~");

			// 表單驗證
			if ( !$('#formId').validationEngine('validate') ) {
				e.preventDefault();
			} else {
				// 解除表單驗證
				$("#formId").validationEngine('detach');
				
				// 通過表單驗證
				// 驗證碼驗證
				var capUri = '/nb3' + "/CAPCODE/captcha_valided_trans";
		        var capData = $("#formId").serializeArray();
		        var capResult = fstop.getServerDataEx(capUri, capData, false);
		        console.log("chaCode_valid: " + JSON.stringify(capResult) );
		// 驗證結果
		if (capResult.result) {
			
			$("#formId").attr("action","${__ctx}/ONLINE/APPLY/logout_taiwan_pay_step2");
			$("#formId").submit();
		} else {
			setTimeout("unBlockUI()", 10);
			// 失敗重新產生驗證碼
			//驗證碼有誤
			//alert("驗證碼有誤");
			errorBlock(
				null, 
				null,
				["驗證碼有誤"], 
				'<spring:message code= "LB.Quit" />', 
				null
			);
			changeCode();

		}
		}
		});
	}
	// 驗證碼刷新
	function changeCode() {
		$('input[name="capCode"]').val('');
		// 大小版驗證碼用同一個
		console.log("changeCode...");
		$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

		// 登入失敗解遮罩
		unBlockUI(initBlockId);
	}
	
	// 初始化驗證碼
	function initKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
	}
	
	// 生成驗證碼
	function newKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').click(function() {
			$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
		});
	}
	
	
</script>
</head>

<body>
<!-- 	 header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Register" /></li>
    <!-- 掃碼提款     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X1949" /></li>
    <!-- 線上註銷臺灣Pay掃碼提款功能     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1589" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code="LB.D1589"/></h2>
<!-- 			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i> -->
			<form id="formId" method="post" action="">
				<input type="hidden" name="ACN" value="${N580_step1.data.ACN}">
				<input type="hidden" name="CUSIDN" value="${N580_step1.data.CUSIDN}">
				<input type="hidden" name="VERNUM" value="${N580_step1.data.VERNUM}">
				<input type="hidden" name="TRANSEQ" value="${N580_step1.data.TRANSEQ}">
				<input type="hidden" name="ISSUER" value="${N580_step1.data.ISSUER}">
				<input type="hidden" name="ACNNO" value="${N580_step1.data.ACNNO}">
				<input type="hidden" name="ICDTTM" value="${N580_step1.data.ICDTTM}">
				<input type="hidden" name="ICSEQ" value="${N580_step1.data.ICSEQ}">
				<input type="hidden" name="TAC_Length" value="${N580_step1.data.TAC_Length}">
				<input type="hidden" name="TAC" value="${N580_step1.data.TAC}">
				<input type="hidden" name="TRMID" value="${N580_step1.data.TRMID}">
				<input type="hidden" name="UID" value="${N580_step1.data.UID}">


                 <div id="step-bar">
                    <ul>
                        <li class="finished"><spring:message code="LB.D1204"/></li>
                        <li class="active"><spring:message code="LB.D1585"/></li>
                        <li class=""><spring:message code="LB.X2060"/></li>
                    </ul>
                </div>
				<div class="main-content-block row">
				   <div class="col-12 tab-content">
                        <div class="ttb-input-block">
                            <div class="ttb-message">
                                <p><spring:message code="LB.D1585"/></p>
                            </div>
                             <p class="form-description"><spring:message code="LB.X1951"/><spring:message code="LB.X1952"/><spring:message code="LB.X1953"/><spring:message code="LB.X1954"/></p>

					    <!-- 註銷帳號 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D1585"/></h4>
                                    </label>
                                </span>
                              <ul>
                               <c:forEach var="dataList" items="${ N580_step1.data.ACNTEMP }" varStatus="data">
						       <input maxLengrh="11" size="12" name="ACN${data.index}" value="${dataList}" style="WIDTH: 300px;;HEIGHT:50px;font-size:30px;" hidden>
                                    <li>
                                       <span>${dataList}</span>
                                    </li>
					          </c:forEach>
					           </ul>
                               
                            </div>
                            
                            
					      
					
					       
					       
			
						<div class="ttb-input-item row" id="chaBlock">
							<!-- 驗證碼 -->
							<span class="input-title">
								<label>
									<h4><spring:message code="LB.Captcha"/></h4>
								</label>
							</span>
							<span class="input-block">
								<spring:message code="LB.Captcha" var="labelCapCode" />
								<input id="capCode" name="capCode" type="text"
											class="text-input" maxlength="6" autocomplete="off">
								<img name="kaptchaImage" class="verification-img" src="" />
								<input type="button" name="reshow" class="ttb-sm-btn btn-flat-orange"
											onclick="changeCode()" value="<spring:message code="LB.Regeneration_1"/>" />
							</span>
						</div>
						</div>
							   <button type="button" class="ttb-button btn-flat-orange"name="CMSUBMIT" id="CMSUBMIT"><spring:message code="LB.Confirm"/></button>
					</div>
				
			   </div>
			</form>
		</section>
		</main>
	<!-- 		main-content END -->
	<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>

</html>