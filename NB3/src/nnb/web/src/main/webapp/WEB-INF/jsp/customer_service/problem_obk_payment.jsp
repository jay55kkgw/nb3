<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="page7" class="card-detail-block">
	<div class="card-center-block">
		<!-- Q1 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-71" aria-expanded="true"
				aria-controls="popup1-71">
				<div class="row">
					<span class="col-1">Q1</span>
					<div class="col-11">
						<span>為什麼申請電子憑證要支付憑證費，憑證的有效期限為多久？</span>
					</div>
				</div>
			</a>
			<div id="popup1-71" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>網路銀行電子簽章交易使用臺灣網路認證公司金融XML憑證，需支付該公司憑證費用，目前收取之憑證年費為個人戶NT$150元，法人戶NT$900元，有效期間一年。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q2 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-72" aria-expanded="true"
				aria-controls="popup1-72">
				<div class="row">
					<span class="col-1">Q2</span>
					<div class="col-11">
						<span>除了使用電子簽章交易應支付憑證年費外，還有什麼與憑證相關之費用？</span>
					</div>
				</div>
			</a>
			<div id="popup1-72" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>依照金融XML憑證作業規定，使用金融XML憑證，必需使用憑證載具儲存憑證及公私密金鑰，「憑證載具」每支NT$600元，「憑證載具」若未有任何毀損，可以無限期使用。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q3 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-73" aria-expanded="true"
				aria-controls="popup1-73">
				<div class="row">
					<span class="col-1">Q3</span>
					<div class="col-11">
						<span>網路銀行轉帳手續費及相關優惠？</span>
					</div>
				</div>
			</a>
			<div id="popup1-73" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div style="overflow: auto">
								<table class="question-table">
									<thead>
										<tr>
											<th colspan="4">服務項目</th>
											<th>收費標準</th>
											<th>備註</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th rowspan="4">臺幣</th>
											<th colspan="3">自行轉帳</th>
											<td>免手續費</td>
											<td></td>
										</tr>
										<tr>
											<th rowspan="3">跨行轉帳</th>
											<th colspan="2">NTD 500以下 (含)</th>
											<td>NTD 10/每筆<br>(每帳戶每日第1筆免手續費)</td>
											<td>當日未使用之優惠次數不累計至隔日使用。</td>
										</tr>
										<tr>
											<th colspan="2">NTD 501 ~ 1,000(含)</th>
											<td>NTD 10/每筆</td>
											<td></td>
										</tr>
										<tr>
											<th colspan="2">NTD 1,000以上</th>
											<td>NTD 15/每筆</td>
											<td></td>
										</tr>
										<tr>
											<th rowspan="3">繳費稅</th>
											<th colspan="3">繳稅</th>
											<td>免手續費</td>
											<td></td>
										</tr>
										<tr>
											<th rowspan="2">繳費</th>
											<th colspan="2">本行代收者</th>
											<td>免手續費</td>
											<td rowspan="2" class="white-spacing text-left">繳費項目包含：電費、台北市水費、臺灣省水費、勞保費、健保費、國民年金保險費、新制勞工退休準備金、本行信用卡費、欣欣瓦斯費、學雜費、其他費用。</td>
										</tr>
										<tr>
											<th colspan="2">跨行繳費</th>
											<td>NTD 15/每筆</td>
										</tr>
										<tr>
											<th rowspan="28">外匯</th>
											<th rowspan="7">買賣外幣<br>(結構結售)</th>
											<th colspan="2">美金</th>
											<td>掛牌匯率優惠0.015</td>
											<td rowspan="7" class="white-spacing text-left">以上匯率之優惠不得優於本行營業單位之成本匯率。</td>
										</tr>
										<tr>
											<th colspan="2">澳幣</th>
											<td>掛牌匯率優惠0.015</td>
										</tr>
										<tr>
											<th colspan="2">歐元</th>
											<td>掛牌匯率優惠0.015</td>
										</tr>
										<tr>
											<th colspan="2">紐幣</th>
											<td>掛牌匯率優惠0.01</td>
										</tr>
										<tr>
											<th colspan="2">南非幣</th>
											<td>掛牌匯率優惠0.006</td>
										</tr>
										<tr>
											<th colspan="2">港幣</th>
											<td>掛牌匯率優惠0.002</td>
										</tr>
										<tr>
											<th colspan="2">日幣</th>
											<td>掛牌匯率優惠0.0002</td>
										</tr>
										<tr>
											<th colspan="3">綜存定存</th>
											<td>各幣別一律按本行牌告利率加計0.05%</td>
											<td class="white-spacing text-left">
												<ol class="list-decimal">
													<li>包含外匯綜存定存及其自動轉期案件</li>
													<li>以上利率之優惠不得優於本行營業單位之成本利率。</li>
												</ol>
											</td>
										</tr>
										<tr>
											<th rowspan="2">自行轉帳</th>
											<th colspan="2">手續費</th>
											<td>免手續費</td>
											<td class="white-spacing text-left">國內分行(DBU)與國際金融業務分行(OBU)帳戶互轉視同匯出匯款，費率詳如外匯匯出匯款。</td>
										</tr>
										<tr>
											<th colspan="2">轉讓費</th>
											<td>免手續費<br>(不同戶名之外幣間轉帳)</td>
											<td></td>
										</tr>
										<tr>
											<th rowspan="8">匯出匯款<br>(DBU/國內分行)</th>
											<th colspan="2">手續費</th>
											<td>依匯款金額<br>0.05%計收</td>
											<td>最低收費等值NTD 100，最高收費等值NTD 800。</td>
										</tr>
										<tr>
											<th colspan="2">郵電費</th>
											<td>等值NTD 300/每筆</td>
											<td>另依實際發電筆數加收電報費。</td>
										</tr>
										<tr>
											<th rowspan="6" class="white-spacing text-left">國外費用</th>
											<th colspan="3">如手續費負擔別選擇『OUR』，每筆依「費用扣款幣別」加收等值的國外費用：</th>
										</tr>
										<tr>
											<th>歐元匯款</th>
											<td>依匯款金額<br>0.1%計收</td>
											<td>最低收費EUR 30</td>
										</tr>
										<tr>
											<th>日幣匯款</th>
											<td>依匯款金額<br>0.05%計收</td>
											<td>最低收費JPY 5,000</td>
										</tr>
										<tr>
											<th>英鎊匯款</th>
											<td>依匯款金額<br>0.1%計收</td>
											<td>最低收費GBP 25</td>
										</tr>
										<tr>
											<th>港幣匯款</th>
											<td>HKD 300/每筆</td>
											<td></td>
										</tr>
										<tr>
											<th>其他幣別匯款</th>
											<td>等值USD 35/每筆</td>
											<td></td>
										</tr>
										<tr>
											<th rowspan="8">匯出匯款<br>(OBU/國際金融業務分行)</th>
											<th colspan="2">手續費</th>
											<td>依匯款金額<br>0.05%計收</td>
											<td>最低收費等值USD 10，最高收費等值USD 40。</td>
										</tr>
										<tr>
											<th colspan="2">郵電費</th>
											<td>等值USD 15/每筆</td>
											<td>另依實際發電筆數加收電報費。</td>
										</tr>
										<tr>
											<th rowspan="6">國外費用</th>
											<th colspan="3">如手續費負擔別選擇『OUR』，每筆依「費用扣款幣別」加收等值的國外費用：</th>
										</tr>
										<tr>
											<th>歐元匯款</th>
											<td>依匯款金額<br>0.1%計收</td>
											<td>最低收費EUR 40</td>
										</tr>
										<tr>
											<th>日幣匯款</th>
											<td>依匯款金額<br>0.05%計收</td>
											<td>最低收費JPY 5,000</td>
										</tr>
										<tr>
											<th>英鎊匯款</th>
											<td>依匯款金額<br>0.1%計收</td>
											<td>最低收費GBP 25</td>
										</tr>
										<tr>
											<th>港幣匯款</th>
											<td>HKD 300/每筆</td>
											<td></td>
										</tr>
										<tr>
											<th>其他幣別匯款</th>
											<td>等值USD 35/每筆</td>
											<td></td>
										</tr>
										<tr>
											<th>匯入匯款/線上解款<br>(DBU/國內分行)</th>
											<th colspan="2">手續費</th>
											<td>依匯入款金額<br>0.05%計收</td>
											<td>最低收費等值NTD 200、最高收費等值NTD 800。</td>
										</tr>
										<tr>
											<th>匯入匯款/線上解款<br>(OBU/國際金融業務分行)</th>
											<th colspan="2">手續費</th>
											<td>依匯入款金額<br>0.05%計收</td>
											<td>最低收費等值USD 10、最高收費等值USD 40。</td>
										</tr>
										<tr>
											<th rowspan="2">基金</th>
											<th colspan="3">國內</th>
											<td>4.5折起</td>
											<td></td>
										</tr>
										<tr>
											<th colspan="3">國外</th>
											<td>5折起</td>
											<td></td>
										</tr>
									</tbody>
								</table>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>