<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
    <%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<!--交易機制所需JS-->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		//表單驗證初始化
		$("#formId").validationEngine({binded: false,promptPosition: "inline"});
	    //送出事件
	    $("#CMSUBMIT").click(function(e) {
			console.log("submit~~");
	        
			var FGTXWAY = $("input[name=FGTXWAY]:checked").val();
	            
			//交易密碼
			if(FGTXWAY == "0"){
				if (!$('#formId').validationEngine('validate')){
		            e.preventDefault();
		        }
		        else{
		            $("#formId").validationEngine('detach');
		            
					$("#CMPASSWORD").val($("#pin").val());
					
					var PINNEW = pin_encrypt($("#CMPASSWORD").val());
					$("#PINNEW").val(PINNEW);
					initBlockUI();//遮罩
					$("#formId").submit();
		        }
			}
			//IKEY
			else if(FGTXWAY == "1"){
				var jsondc = $("#jsondc").val();

	            $("#formId").validationEngine('detach');
	            
				
				//IKEY驗證流程
				uiSignForPKCS7(jsondc);
			}else if(FGTXWAY =="7"){ 
				$("#formId").validationEngine('detach');
	            idgatesubmit= $("#formId");		 
	            showIdgateBlock();				
			}
	    });
		//上一頁按鈕
		$("#CMBACK").click(function() {
			var action = '${__ctx}/FCY/ACCT/TDEPOSIT/f_deposit_transfer';
			$('#back').val("Y");
			$("form").attr("action", action);
			initBlockUI();
			$("#formId").validationEngine('detach');
			$("form").submit();
		});
	    //交易類別change 事件
		$('input[tyoe=radio][name=FGTXWAY]').change(function(){
			console.log(this.value);
			if(this.value=='0'){
				$("#pin").addClass("validate[required]")
			}else if(this.value=='1'){
				$("#pin").removeClass("validate[required]");
			}else if(this.value=='2'){
				$("#pin").removeClass("validate[required]");
			}else if (this.value=='7'){
				$("#pin").removeClass("validate[required]");
			}
		});
	});
	//重新輸入
 	function formReset() {
 		if ($('#actionBar').val()=="reEnter"){
	 		$('#actionBar').val("");
	 		document.getElementById("formId").reset();
 		}
	}
    </script>
</head>

<body>
	<!--交易機制所需畫面-->
	<%@ include file="../component/trading_component.jsp"%>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %>  
    <!-- header     -->
    <header>
        <%@ include file="../index/header.jsp"%>
    </header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 定存服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Current_Deposit_To_Time_Deposit" /></li>
    <!-- 轉入外幣綜存定存     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Open_Foreign_Currency_Time_Deposit" /></li>
		</ol>
	</nav>



    <!-- menu、登出窗格 -->
    <div class="content row">
        <!-- 功能選單內容 -->
        <%@ include file="../index/menu.jsp"%>
    </div>
    <!-- 		主頁內容  -->
    <main class="col-12">
        <section id="main-content" class="container">
            <h2>
                <!--轉入外匯綜存定存 -->
                <spring:message code="LB.Open_Foreign_Currency_Time_Deposit" />
            </h2>
            <i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
            <!-- 下拉式選單-->
			<div class="print-block no-l-display-btn">
				<select class="minimal" id="actionBar" onchange="formReset()">
					<option value=""><spring:message code="LB.Execution_option" /></option>
						<!-- 重新輸入-->
					<option value="reEnter"><spring:message code="LB.Re_enter"/></option>
				</select>
			</div>
            <form method="post" id="formId" action="${__ctx}/FCY/ACCT/TDEPOSIT/f_deposit_transfer_result">
            	<input type="hidden" id="back" name="back" value="">
            	
                <c:set var="BaseResultData" value="${f_deposit_transfer_confirm.data}"></c:set>
                <%--  轉出帳號 --%>
                <input type="hidden" name="FYTSFAN" value="${BaseResultData.FYTSFAN}">
                <%--  約定/非約定記號 1:約定帳號 --%>
                <input type="hidden" name="FGINACNO" value="${BaseResultData.FGINACNO}">
                <%--  約轉轉入帳號JSON --%>
                <input type="hidden" name="INACNO1" value='${BaseResultData.INACNO1}'>
                <%--  約轉轉入帳號 --%>
                <input type="hidden" name="INACN" value='${BaseResultData.INACN}'>
                <%--  非約轉轉入帳號 --%>
                <input type="hidden" name="INACNO2" value='${BaseResultData.INACNO2}'>
                <%--  預約單筆轉帳日期 --%>
                <input type="hidden" name="CMTRDATE" value="${BaseResultData.CMTRDATE}">
                <%--  預約\日 --%>
                <input type="hidden" name="CMDATE" value="${BaseResultData.CMDATE}">
                <%--  預約週期起日 --%>
                <input type="hidden" name="CMSDATE" value="${BaseResultData.CMSDATE}">
                <%--  預約週期迄日 --%>
                <input type="hidden" name="CMEDATE" value="${BaseResultData.CMEDATE}">
                <%--  預約每月轉帳日期--%>
                <input type="hidden" name="CMPERIOD" value="${BaseResultData.CMPERIOD}">
                <%--  即時/預約記號--%>
                <input type="hidden" name="FGTRDATE" value="${BaseResultData.FGTRDATE}">
                <%--  轉帳幣別 --%>
                <input type="hidden" name="OUTCRY" value='${BaseResultData.OUTCRY}'>
                <%--  轉帳幣別中文 --%>
                <input type="hidden" name="OUTCRY_TEXT" value='${BaseResultData.OUTCRY_TEXT}'>
                <%--  轉帳金額(整數位)  --%>
                <input type="hidden" name="AMOUNT" value="${BaseResultData.AMOUNT}">
                <%--  轉帳金額(小數位)  --%>
                <input type="hidden" name="AMOUNT_DIG" value="${BaseResultData.AMOUNT_DIG}">
                <%--  議價編號  --%>
                <input type="hidden" name="BGROENO" value="${BaseResultData.BGROENO}">
                <%--  交易備註 --%>
                <input type="hidden" name="CMTRMEMO" value="${BaseResultData.CMTRMEMO}">
                <input type="hidden" name="CMTRMAIL" value="${BaseResultData.CMTRMAIL}">
                <%-- Email摘要內容 --%>
                <input type="hidden" name="CMMAILMEMO" value="${BaseResultData.CMMAILMEMO}">
                <%-- 存單期別 --%>
                <input type="hidden" name="TYPCOD" value="${BaseResultData.TYPCOD}">
                <%-- 存單期別中文 --%>
                <input type="hidden" name="TYPCOD_TEXT" value="${BaseResultData.TYPCOD_TEXT}">
                <%-- 計息方式，0機動，1固定 --%>
                <input type="hidden" name="INTMTH" value="${BaseResultData.INTMTH}">
                <%-- 計息方式中文 --%>
                <input type="hidden" name="INTMTHNAME" value="${BaseResultData.INTMTHNAME}">
                <%-- 轉存方式 --%>
                <input type="hidden" name="CODE" value="${BaseResultData.CODE}">
                <%-- 轉存方式中文 --%>
                <input type="hidden" name="AUTXFTMNAME" value="${BaseResultData.AUTXFTMNAME}">
                <%-- 到期轉期日 --%>
                <input type="hidden" name="AUTXFTM" value="${BaseResultData.AUTXFTM}">
                <input type="hidden" name="FGAUTXFTM" value="${BaseResultData.FGAUTXFTM}">
                <%-- 到期轉期日中文 --%>
                <input type="hidden" name="CODENAME" value="${BaseResultData.CODENAME}">
                <%-- TXTOKEN --%>
                <input type="hidden" name="TXTOKEN" value="${BaseResultData.TXTOKEN}" />
                <%-- CAPTCHA --%>
                <input type="hidden" name="CAPTCHA" value="${BaseResultData.CAPTCHA}" />
                <%-- 交易日期 --%>
                <input type="hidden" name="TRDATE" value="${BaseResultData.transfer_date}" />
                <!--交易機制所需欄位-->
				<input type="hidden" id="CMPASSWORD" name="CMPASSWORD"/>
				<input type="hidden" id="PINNEW" name="PINNEW"/>
				<input type="hidden" id="jsondc" name="jsondc" value="${BaseResultData.jsondc}"/>
				<input type="hidden" id="ISSUER" name="ISSUER"/>
				<input type="hidden" id="ACNNO" name="ACNNO"/>
				<input type="hidden" id="TRMID" name="TRMID"/>
				<input type="hidden" id="iSeqNo" name="iSeqNo"/>
				<input type="hidden" id="ICSEQ" name="ICSEQ"/>
				<input type="hidden" id="TAC" name="TAC"/>
				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign"/>
				

                <!--交易步驟 -->
				<div id="step-bar">
					<ul>
				<!--輸入資料 -->
						<li class="finished"><spring:message code="LB.Enter_data"/></li>
				<!-- 確認資料 -->
						<li class="active"><spring:message code="LB.Confirm_data"/></li>
				<!-- 交易完成 -->
						<li class=""><spring:message code="LB.Transaction_complete"/></li>
					</ul>
				</div>
                <!-- 表單顯示區  -->
                <div class="main-content-block row">
                    <div class="col-12">
                        <div class="ttb-input-block">
                            <!--show message -->
                            <div class="ttb-message">
									<!--顯示即時OR預約 -->
                                		<c:choose>
					      				<c:when test="${BaseResultData.FGTRDATE == '0'}">
					      					 <p ><spring:message code="LB.Immediately" /></p>
					      				</c:when>
					      				<c:otherwise>
					      					 <p ><spring:message code="LB.Booking" /></p>
					      				</c:otherwise>
					      				</c:choose>
                                    <!-- 請確認轉帳資料 -->
                                    <span>
                                        <spring:message code="LB.Confirm_transfer_data" />
                                    </span>
                             </div>
                            <!-- 轉帳日期 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.Transfer_date" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <p>
                                            ${BaseResultData.transfer_date }
                                        </p>
                                    </div>
                                </span>
                            </div>
                            <!-- 轉出帳號 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.Payers_account_no" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <p>
                                            ${BaseResultData.FYTSFAN}
                                        </p>
                                    </div>
                                </span>
                            </div>
                            <!-- 轉入帳號 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.Payees_account_no" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <p>
                                        	<c:if test="${BaseResultData.FGINACNO == 'CMDAGREE'}">
                                        		${BaseResultData.INACN}
                                        	</c:if>
                                        	<c:if test="${BaseResultData.FGINACNO == 'CMDISAGREE'}">
                                        		${BaseResultData.INACNO2}
                                        	</c:if>
                                            
                                        </p>
                                    </div>
                                </span>
                            </div>
                            <!-- 轉帳金額 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.Amount" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <span class="high-light">
                                            <!--          幣別+金額+小數點 -->
                                            ${BaseResultData.OUTCRY_TEXT }
                                            <fmt:formatNumber type="number" value="${BaseResultData.AMOUNT }" />
                                            .${BaseResultData.AMOUNT_DIG }
                                        </span>
                                    </div>
                                </span>
                            </div>
                            <!-- 存款期別-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.Deposit_period" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <p>
                                            ${BaseResultData.TYPCOD_TEXT}
                                        </p>
                                    </div>
                                </span>
                            </div>
                            <!-- 計息方式-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.Interest_calculation" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <p>
                                            ${BaseResultData.INTMTHNAME}
                                        </p>
                                    </div>
                                </span>
                            </div>
                            <!-- 轉存方式 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.Rollover_method" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <p >
                                            ${BaseResultData.AUTXFTMNAME}
                                        </p>
                                    </div>
                                </span>
                            </div>
                            <!-- 到期轉期-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.Roll-over_when_expiration" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <p >
                                            ${BaseResultData.CODENAME}
                                        </p>
                                    </div>
                                </span>
                            </div>
                            <!-- 議價編號-->
                            <c:if test="${BaseResultData.BGROENOflag == 'true'}">
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Bargaining_number"/></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											 <p >
	                                            ${BaseResultData.BGROENO}
	                                        </p>
										</div>
									</span>
								</div>
                            </c:if>
                            <!-- 交易機制 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <!-- 交易機制 -->
                                        <h4><spring:message code="LB.Transaction_security_mechanism" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
									<div class="ttb-input">
										<label class="radio-block">
											<!-- 交易密碼(SSL) -->
											<spring:message code="LB.SSL_password" />
											<input type="radio" name="FGTXWAY" checked="checked" value="0">
											<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<!--請輸入密碼 -->
	                                    <spring:message code="LB.Please_enter_password" var="plassEntpin"></spring:message>
										<input type="password" id="pin" name="pin" class="text-input validate[required]" maxlength="8" placeholder="${plassEntpin}">
									</div>
									<c:if test = "${BaseResultData.XMLCOD == '2' || BaseResultData.XMLCOD == '4' || BaseResultData.XMLCOD == '5' || BaseResultData.XMLCOD == '6'}">
										<div class="ttb-input">
											<label class="radio-block">
												<!-- 電子簽章(載具i-key) -->
												<spring:message code="LB.Electronic_signature" />
												<input type="radio" name="FGTXWAY" value="1">
												<span class="ttb-radio"></span>
											</label>
										</div>
									</c:if>
									<div class="ttb-input" name="idgate_group" style="display:none">		 
                                       <label class="radio-block">裝置推播認證(請確認您的行動裝置網路連線是否正常，及推播功能是否已開啟)
	                                       <input type="radio" id="IDGATE" 	name="FGTXWAY" value="7"> 
	                                       <span class="ttb-radio"></span>
                                       </label>		 
                                   </div>
                                </span>
                            </div>
                        </div>
                        <!-- 						button -->
                            <!--回上頁 -->
                            <spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>
                            <input type="button" name="CMBACK" id="CMBACK" value="${cmback}" class="ttb-button btn-flat-gray" >
                            <!--重新輸入 -->
                            <spring:message code="LB.Re_enter" var="cmRest"></spring:message>
                        	<input type="reset" name="CMRESET" id="CMRESET" value="${cmRest}" class="ttb-button btn-flat-gray no-l-disappear-btn">
                            <!-- 確定 -->
                            <spring:message code="LB.Confirm" var="cmSubmit"></spring:message>
                            <input type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" class="ttb-button btn-flat-orange">
                            <!-- 						button -->
                    </div>
                </div>
            </form>
        </section>
        <!-- 		main-content END -->
    </main>
    <%@ include file="../index/footer.jsp"%>
</body>

</html>