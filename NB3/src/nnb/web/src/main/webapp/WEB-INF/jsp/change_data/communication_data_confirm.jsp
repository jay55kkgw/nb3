<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">

<style type="text/css">
.table-1 {
    width: 100%;
    border-spacing: 0;
    border-collapse: collapse;
    margin: 5% auto;
    border:
}

.table-1>thead>tr>th,
.table-1>thead>tr>td {
    background-color: #EEEEEE;
    border-top: none;
    text-shadow: 0 1px 0 rgba(255, 255, 255, .5);
    font-weight: bold;
}

.table-1>tbody>tr>td,
.table-1>thead>tr>th {
    border: 1px solid #DDD;
    padding: 8px;
    text-align: left;
}

.table-1 tbody tr:first-child td:first-child {
    text-align: center;
}

.table-1 td:hover {
    background-color: #fbf8e9;
}
</style>

<script type="text/javascript">
$(document).ready(function() {
	// HTML載入完成後開始遮罩
	setTimeout("initBlockUI()", 50);
	// 開始跑下拉選單並完成畫面
	setTimeout("init()", 400);
	// 解遮罩
	setTimeout("unBlockUI(initBlockId)", 500);
	
	// 初始化時隱藏span
	$("#hideblock").hide();
	// 將.table變更為footable
// 	initFootable();
	var datasets = getData();
	drawTable(datasets);
	
	
});
function getData(){
	//i18n
	var datasets = [];
	var tel = "<spring:message code='LB.D0094'/>";
	var tel2 = "<spring:message code='LB.D0065'/>";
	var mobilePhone = "<spring:message code='LB.D0069'/>";
	//
	var data = ${communication_data.data.DATAJSON};
	var dl = [];
	dl = dl.concat(data.data.dataSet);
	dl = dl.concat(data.data.cloneRec);
	
	dl.forEach(function(d){
		var dataset = ["",d.BRHNAME+"("+d.BRHCOD+")",d.POSTCOD2 + d.CTTADR,tel+":"+"("+d.ARACOD2+")"+d.TELNUM2+"<br>"+tel2+":"+"("+d.ARACOD+")"+d.TELNUM+"<br>"+mobilePhone+":"+d.MOBTEL];
		//console.log(d);
		//console.log(dataset);
		datasets.push(dataset);
	});
	return datasets;
}

function drawTable(dataSets){
	$('#table1').DataTable({
		data: dataSets,
		"scrollX": true,
		"sScrollX": "90%",
		"scrollY": "80vh",
		"bPaginate": false,
		"bFilter": false,
		"bDestroy": true,
		"bSort": false,
		"info": false,
		"scrollCollapse": true,
		fixedColumns: {
		leftColumns: 0
		},
		columnDefs: [{
			targets: [0],
			createdCell: function(td, cellData, rowData, row, col) {
				//var rowspan = span[row][col];
				//console.log(row,col);
				if (row == 0) {
					$(td).attr('rowspan',dataSets.length);
				}
				if (row != 0) {
					$(td).remove();
				}
				if(row==0 && col==0){
				$(td).html("<label class=\"radio-block\"><input type=\"radio\" name=\"r1\" onclick=\"addData('lock')\"><span class=\"ttb-radio\"></span></label>");
				$(td).css("vertical-align","middle");
				}
			}
		}]
	});
}

function dTable(){
	$('.dtable').DataTable({
		"scrollX": true,
		"sScrollX": "90%",
		"scrollY": "80vh",
		"bPaginate": false,
		"bFilter": false,
		"bDestroy": true,
		"bSort": false,
		"info": false,
		"scrollCollapse": true,
	});
}

function init(){
	$("#formId").validationEngine({
		binded: false,
		promptPosition: "inline"
	});
	
	//確定 
	$("#CMSUBMIT").click(function() {
		
		//打開驗證欄位
		$("#hideblock").show();
		
	    if (!$('#formId').validationEngine('validate')) {
	        e.preventDefault();
	    } else {
	    	$("#formId").validationEngine('detach');
				initBlockUI();
				$("#formId").attr("action","${__ctx}/CHANGE/DATA/communication_data_plural");					
		  		$("#formId").submit(); 
				
	    }
	});
	
	var show = "${communication_data.data.show900}";
	if(show == "Y"){
		var SMSA = "${communication_data900.data.SMSA}";
		if(SMSA == "0001"){
			$("#strBILLTITLE").html("<spring:message code='LB.D0143' />");
		}
		else if(SMSA == "0002"){
			$("#strBILLTITLE").html("<spring:message code='LB.D0063' />");
		}
		else{
			$("#strBILLTITLE").html("<spring:message code='LB.D0090' />");
		}
		
		$("#900table").show();
		dTable();
	}
	else{
		$("#900table").hide();
	}
	
}

//表單驗證
function addData(value){
//alert(value);
	$("#ErrorMsg").val(value);
}

</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

	<!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 通訊資料變更     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0359" /></li>
    <!-- 往來帳戶及信託業務通訊地址/電話變更     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0370" /></li>
		</ol>
	</nav>
	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code="LB.D0370"/></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="">
				<div class="main-content-block row">
					<div class="col-12">
						<ul class="ttb-result-list">
                        	<li>
                            	<!-- 資料總數 -->
								<h3><spring:message code="LB.Total_records" /> :</h3>
								<p>
                                	${communication_data.data.COUNT}
                                	<spring:message code="LB.Rows" />
                               	</p>
                          	</li>
                     	</ul>
						
						<div id="900table" style="display: none">
							<table class="stripe table-striped ttb-table dtable" data-show-toggle="first">
								<thead>
									<tr>
										<th data-title=""></th>
										<th data-title=""></th>
										<th data-title="<spring:message code="LB.X2506" />"><spring:message code="LB.X2506" /></th>
										<th data-title="<spring:message code="LB.Telephone" />"><spring:message code="LB.Telephone" /></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><spring:message code="LB.Credit_Card" /></td>
	
										<td>
											<spring:message code="LB.D0149" /><br>(<font id="strBILLTITLE"></font>)
										</td>
										<td style="text-align:left"><c:out value='${fn:escapeXml(communication_data900.data.strZIP)}' /><br><c:out value='${fn:escapeXml(communication_data900.data.strBILLADDR1)}' /><c:out value='${fn:escapeXml(communication_data900.data.strBILLADDR2)}' /></td>
										<td style="text-align:left">
											<spring:message code="LB.D0206" />：<c:out value='${fn:escapeXml(communication_data900.data.HOMEPHONE)}' /><br>
											<spring:message code="LB.D0094" />：<c:out value='${fn:escapeXml(communication_data900.data.strOfficeTel)}' /><br>
											<spring:message code="LB.D0413" />：<c:out value='${fn:escapeXml(communication_data900.data.strOfficeExt)}' /><br>
											<spring:message code="LB.D0069" />：<c:out value='${fn:escapeXml(communication_data900.data.MOBILEPHONE)}' />
	
										</td>
	
									</tr>
								</tbody>
							</table>
						</div>
					
						<!-- 變更通訊地址/電話 -->
						<table class="stripe table-striped ttb-table" id="table1" data-toggle-column="first">
							<thead>
							<tr>
								<th class="text-center"><spring:message code="LB.D0374"/></th>
								<th class="text-center"><spring:message code="LB.D0375"/></th>
								<th class="text-center"><spring:message code="LB.D0376"/></th>
								<th class="text-center"><spring:message code="LB.Telephone"/></th>
							</tr>
							</thead>
<%-- 							<tbody> --%>
<%-- 								<c:set var="dataSet" value="${ communication_data.data.dataSet }" /> --%>
<%-- 								<tr> --%>
<%-- 									<td rowspan="${communication_data.data.ROWCOUNT}" style="vertical-align:middle"> --%>
<!-- 										<label class="radio-block"> -->
<!-- 											<input type="radio" name="r1" onclick="addData('lock')"> -->
<!-- 											<span class="ttb-radio"></span> -->
<!-- 										</label> -->
<%-- 									</td> --%>
<%-- 									<td> --%>
<%-- 										${dataSet.BRHNAME} --%>
<%-- 										(${dataSet.BRHCOD}) --%>
<%-- 									</td> --%>
<%-- 									<td> --%>
<%-- 										${dataSet.POSTCOD2} --%>
<%-- 										${dataSet.CTTADR} --%>
<%-- 									</td> --%>
<%-- 									<td> --%>
<%-- 										<spring:message code="LB.Telephone"/>1: --%>
<%-- 										(${dataSet.ARACOD}) --%>
<%-- 										${dataSet.TELNUM} --%>
<!-- 										<br> -->
<%-- 										<spring:message code="LB.Telephone"/>2: --%>
<%-- 										(${dataSet.ARACOD2}) --%>
<%-- 										${dataSet.TELNUM2}											 --%>
<!-- 										<br> -->
<%-- 										<spring:message code="LB.D0069"/>: --%>
<%-- 										${dataSet.MOBTEL} --%>
<!-- 										<br>										 -->
<%-- 									</td> --%>
<%-- 								</tr> --%>
<%-- 								<c:forEach var="dataList" items="${communication_data.data.cloneRec}" varStatus="data"> --%>
<%-- 									<tr> --%>
<%-- 										<td> --%>
<%-- 											${dataList.BRHNAME} --%>
<%-- 											(${dataList.BRHCOD}) --%>
<%-- 										</td> --%>
<%-- 										<td> --%>
<%-- 											${dataList.POSTCOD2} --%>
<%-- 											${dataList.CTTADR} --%>
<%-- 										</td> --%>
<%-- 										<td> --%>
<%-- 											<spring:message code="LB.Telephone"/>1: --%>
<%-- 											(${dataList.ARACOD}) --%>
<%-- 											${dataList.TELNUM} --%>
<!-- 											<br> -->
<%-- 											<spring:message code="LB.Telephone"/>2: --%>
<%-- 											(${dataList.ARACOD2}) --%>
<%-- 											${dataList.TELNUM2}											 --%>
<!-- 											<br> -->
<%-- 											<spring:message code="LB.D0069"/>: --%>
<%-- 											${dataList.MOBTEL} --%>
<!-- 											<br>										 -->
<%-- 										</td> --%>
<%-- 									</tr> --%>
<%-- 								</c:forEach> --%>
<%-- 							</tbody> --%>
						</table>
						
						<!-- 不在畫面上顯示的span -->
						<span id="hideblock" >
						<!-- 驗證用的input -->
						<input id="ErrorMsg" name="ErrorMsg" type="text" class="text-input validate[groupRequired[r1]]" 
							style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" value="" />
						</span>
						
						<!-- 重新輸入 -->
						<input type="reset" id="reset" class="ttb-button btn-flat-gray" onclick="clearData()" value="<spring:message code="LB.Re_enter"/>"/>
						<!-- 確定 -->
                        <input type="button" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm"/>" >
					</div>
				</div>
			</form>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>