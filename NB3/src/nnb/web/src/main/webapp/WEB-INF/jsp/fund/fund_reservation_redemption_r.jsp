<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
		initFootable();
		$("#printbtn").click(function(){
			var params = {
					"jspTemplateName":"fund_reservation_redemption_r_print",
					"jspTitle":"<spring:message code= "LB.X0376" />",
					"CMQTIME":"${fund_reservation_redemption_r.data.CMQTIME}",
					"FDTXTYPE":"${fund_reservation_redemption_r.data.FDTXTYPE}",
					"CDNO":"${fund_reservation_redemption_r.data.CDNO_1}",
					"TRANSCODE":"${fund_reservation_redemption_r.data.TRANSCODE}",
					"FUNDLNAME":"${fund_reservation_redemption_r.data.FUNDLNAME}",
					"TRADEDATE_1":"${fund_reservation_redemption_r.data.TRADEDATE_1}",
					"BILLSENDMODE_1":"${fund_reservation_redemption_r.data.BILLSENDMODE}",
					"UNIT_1":"${fund_reservation_redemption_r.data.UNIT_1}",
					"FUNDACN":"${fund_reservation_redemption_r.data.FUNDACN_1}",
					"ADCCYNAME":"${fund_reservation_redemption_r.data.ADCCYNAME}",
					"FUNDAMT_1":"${fund_reservation_redemption_r.data.FUNDAMT_1}"
					
			};
			openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
		});
		// 確認鍵 click
		submit();
	}
	
	// 確認鍵 Click
	function submit() {
		$("#CMSUBMIT").click( function(e) {
			console.log("submit~~");
			// 遮罩
         	initBlockUI();
            $("#formId").attr("action","");
	 	  	$("#formId").submit();
		});
	}

</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Funds" /></li>
    <!-- 基金查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0902" /></li>
    <!-- 取消預約贖回交易     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0382" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.X0376" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form id="formId" method="post" action="">
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
                        	<div class="ttb-message">
                                <span><spring:message code="LB.Transaction_successful" /></span>
                            </div>
                            
                            <!--交易時間-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
									<label>
                                        <h4><spring:message code="LB.Trading_time" /></h4>
                                    </label>
								</span>
                                <span class="input-block">
									<div class="ttb-input">
                                       	<span>${fund_reservation_redemption_r.data.CMQTIME}</span>
                                  	</div>
								</span>
							</div>
                        	
							<!--交易種類-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
									<label>
                                        <h4><spring:message code="LB.W1060" /></h4>
                                    </label>
								</span>
                                <span class="input-block">
									<div class="ttb-input">
                                       	<span>${fund_reservation_redemption_r.data.FDTXTYPE}</span>
                                  	</div>
								</span>
							</div>
							
							<!--信託號碼-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.W0944" /></h4>
                                    </label></span>
                                <span class="input-block">
									<div class="ttb-input">
										<span>${fund_reservation_redemption_r.data.CDNO_1}</span>
									</div>
								</span>
							</div>
							
							<!--基金名稱-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.W0025" /></h4>
                                    </label></span>
                                <span class="input-block">
									<div class="ttb-input">
										<span>(${fund_reservation_redemption_r.data.TRANSCODE})${fund_reservation_redemption_r.data.FUNDLNAME}</span>
									</div>
								</span>
							</div>
							
							 <!--預約日期-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.X0377" /></h4>
                                    </label></span>
                                <span class="input-block">
									<div class="ttb-input">
                                       	<span>${fund_reservation_redemption_r.data.TRADEDATE_1}</span>
									</div>
								</span>
							</div>
							
							<!--贖回方式-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
									<label>
                                        <h4><spring:message code="LB.W1140" /></h4>
                                    </label></span>
                                <span class="input-block">
									<div class="ttb-input">
										<span>
											<c:if test="${fund_reservation_redemption_r.data.BILLSENDMODE == '1'}">
												<spring:message code="LB.All"/>
											</c:if>
											<c:if test="${fund_reservation_redemption_r.data.BILLSENDMODE == '2'}">
												<spring:message code="LB.X1852"/>
											</c:if>
										</span>
									</div>
								</span>
							</div>
							
							<!--贖回單位數-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
									<label>
                                        <h4><spring:message code="LB.W0979" /></h4>
                                    </label>
								</span>
                                <span class="input-block">
									<div class="ttb-input">
										<span>${fund_reservation_redemption_r.data.UNIT_1}</span>
									</div>
								</span>
							</div>
							
							<!--入帳帳號-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.W0135" /></h4>
                                    </label></span>
                                <span class="input-block">
									<div class="ttb-input">
                                       	<span>${fund_reservation_redemption_r.data.FUNDACN_1}</span>
									</div>
								</span>
							</div>
							
							<!--生效日期-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
									<label>
                                        <h4><spring:message code="LB.W1080" /></h4>
                                    </label>
								</span>
                                <span class="input-block">
									<div class="ttb-input">
										<span>${fund_reservation_redemption_r.data.TRADEDATE_1}</span>
									</div>
								</span>
							</div>
							
							<!--贖回信託金額-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                        <h4><spring:message code="LB.W0978" /></h4>
                                    </label></span>
                                <span class="input-block">
									<div class="ttb-input">
										<span>${fund_reservation_redemption_r.data.ADCCYNAME} ${fund_reservation_redemption_r.data.FUNDAMT_1}</span>
									</div>
								</span>
							</div> 
							
                        </div>
						<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
                    </div>
                </div>
				</form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>