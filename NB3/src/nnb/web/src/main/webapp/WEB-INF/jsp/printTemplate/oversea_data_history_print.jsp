<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ include file="../__import_js.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
	$(document).ready(function() {
		window.print();
	});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br />
	<br />
	<div style="text-align: center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br />
	<br />
	<br />
	<div style="text-align: center">
		<font style="font-weight: bold; font-size: 1.2em">${jspTitle}</font>
	</div>
	<br />
	<br />
	<br />
	<label><spring:message code="LB.Inquiry_time" />：</label>
	<label>${CMQTIME}</label>
	<br />
	<br />
	<label><spring:message code="LB.Total_records" />：</label>
	<label>${COUNT} <spring:message code="LB.Rows" /></label>
	<br />
	<br />
	<label><spring:message code="LB.Id_no" />:</label>
	<label>${hiddencusidn}</label>
	<br />
	<br />
	<label><spring:message code="LB.Name" />:</label>
	<label>${hiddenname}</label>
	<br />
	<br />
	<table class="print" data-toggle-column="first">
		<thead>
				<tr>
					<td class="text-center"><spring:message code="LB.W1022" /><br><spring:message code="LB.W0023" /></td>
      				<td class="text-center"><spring:message code="LB.W1011" /><br><spring:message code="LB.W1012" /></td>
      				<td class="text-center"><spring:message code="LB.Transaction_type" /></td>
      				<td class="text-center"><spring:message code="LB.W1032" /><br><spring:message code="LB.W1014" /></td>
      				<td class="text-center"><spring:message code="LB.W1018" /><br><spring:message code="LB.W1033" /></td>
      				<td class="text-center"><spring:message code="LB.W1034" /><br><spring:message code="LB.D0507" /></td>
      				<td class="text-center"><spring:message code="LB.X0369" /><br><spring:message code="LB.X0370" /></td>
      				<td class="text-center"><spring:message code="LB.W1037" /><br><spring:message code="LB.W0909" /></td>
      				<td class="text-center"><spring:message code="LB.W0977" /><br><spring:message code="LB.W1038" /></td>       
				</tr>
		</thead>
		<tbody>
			<c:forEach var="dataList" items="${dataListMap}">
									<tr>
										<td class="text-center">${dataList.O01}<br>${dataList.O03}</td>
										<td class="text-center">${dataList.O04}<br>${dataList.O05}</td>
										<td class="text-center">${dataList.O06}</td>
										<td class="text-center">${dataList.O12}<br>${dataList.O11}</td>
										<td class="text-center">${dataList.O16}<br>${dataList.O13}</td>
										<td class="text-center">${dataList.O14}<br>${dataList.O15}</td>
										<td class="text-center">${dataList.O20}<br>${dataList.O21}</td>
										<td class="text-center">${dataList.O17}<br>${dataList.O18}</td>
										<td class="text-center">${dataList.O22}<br>${dataList.O19}</td>
									</tr>
			</c:forEach>
		</tbody>
	</table>
	<br />
	<br />
</body>
</html>