<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">    
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript" src="${__ctx}/js/checkutil.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 初始化驗證碼
		setTimeout("initKapImg()", 200);
		// 生成驗證碼
		setTimeout("newKapImg()", 300);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
// 		initFootable();
		$("#formId").validationEngine({
			binded: false,
			promptPosition: "inline"
		});
		// 確認鍵 click
		submit();
	}
	function submit(){
		$("#CMSUBMIT").click(function(){
			$("#formId").attr("action","${__ctx}/login");
			$("#formId").submit();
		});
	}
</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code= "LB.X1573" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="">
                	<div class="main-content-block row">
						<div class="col-12">
							<h4 style="margin:30px auto; text-align: center;"><spring:message code="LB.Setting_result" /></h4>
							<h5 style="text-align:left;margin-top:10px;text-al">
								<spring:message code= "LB.X0152" />:
								<br />
								<spring:message code= "LB.X1577" />
								<br />
								<spring:message code= "LB.X1578" />
							</h5>
								
							<!-- 確認 -->
							<input type="button" class="ttb-button btn-flat-orange" id="CMSUBMIT" value="<spring:message code="LB.X0956" />"/>
							
						</div>
						
						
                	</div>
                </form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>