<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<script type="text/javascript">
	var countdownObjheader, countdownSecHeader = 420;
// 	var minutes, seconds;
	
	$(document).ready(function(){
		console.log('login_time: ' + '${sessionScope.tokenid}');
		var isLocal = '${sessionScope.tokenid}';
		console.log("isLocal: " + isLocal);
		console.log(isLocal != "LOCAL");
		// 非本機開發
		if( isLocal != "LOCAL" ){
			timeLogout();
		}
		// 初始化
		initMenu();
	});
	
	// 初始化
	function initMenu(){
		// contorl nav-menu left/right-btn
		$('#nav-left-btn').click(function() {
		    $('#nav-menu').animate({
		        scrollLeft: "-=151"
		    }, "fast", "swing");
		});
		$('#nav-right-btn').click(function() {
		    $('#nav-menu').animate({
		        scrollLeft: "+=151"
		    }, "fast", "swing");
		});
	}
	
	function timeLogout(){
		// 刷新session
		var uri = '${__ctx}/login_refresh';
		console.log('refresh.uri: '+uri);
		var result = fstop.getServerDataEx( uri, null, false, null);
		console.log('refresh.result: '+JSON.stringify(result));
		// 初始化登出時間
		$("#countdownheader").html(countdownSecHeader);
		$("#countdownMin").html("07");
		$("#mobile-countdownMin").html("07");
		$("#countdownSec").html("00");
		$("#mobile-countdownSec").html("00");
		// 倒數
		startIntervalHeader(1, refreshCountdownHeader, []);
	}

	function isValid(str) {
		// 長度 = 0 || 非數字	               
		return !(str.length === 0 || str.replace(/\D/g,'') !== str );
	}
	function refreshCountdownHeader(){
		// timeout剩餘時間
		var nextSec = parseInt($("#countdownheader").html()) - 1;
		$("#countdownheader").html(nextSec);
		
		// timeout
		if(nextSec == 0){
			// logout
			fstop.logout( '${__ctx}'+'/logout_aj' , '${__ctx}'+'/login');
		}
		if (nextSec >= 0){
			// 倒數時間以分秒顯示
			var minutes = Math.floor(nextSec / 60);
			var seconds = nextSec - minutes * 60;
			$("#countdownMin").html(('0' + minutes).slice(-2));
			$("#mobile-countdownMin").html(('0' + minutes).slice(-2));
			$("#countdownSec").html(('0' + seconds).slice(-2));
			$("#mobile-countdownSec").html(('0' + seconds).slice(-2));
		}
	}
	function startIntervalHeader(interval, func, values){
		clearInterval(countdownObjheader);
		countdownObjheader = setRepeater(func, values, interval);
	}
	function setRepeater(func, values, interval){
		return setInterval(function(){
			func.apply(this, values);
		}, interval * 1000);
	}
	
	//contorl click outside can close menu
// 	$(document).click(function(e) {
// 		if (!$(e.target).is('.sec-menu')) {
// 	    	$('.collapse').collapse('hide');
// 	    }
// 	});
	
	
	
	//contorl click outside can close menu
	$(document).click(function(e) {
		if (!$(e.target).is('.sec-menu')) {
			$('.sec-menu.collapse').collapse('hide');
		}
	});

// 	jQuery(function($) {
// 		$('.table').footable();
// 	});

</script>



<!-- for mobile menu -->
        <%@ include file="../index/mobile_menu_auto.jsp"%>
<!-- for mobile menu End-->




<!-- 快速選單 -->
<section id="id-and-fast">
    <div id="id-block">
    	<div style="display:none;"><font color="red" id="countdownheader"></font></div>
	    <!-- 歡迎標頭 -->
		<span class="id-name">
			<spring:message code="LB.Welcome_1"/> ${sessionScope.dpusername} <spring:message code="LB.Welcome_2"/>
			<button type="button" onClick="fstop.logout( '${__ctx}'+'/logout_aj' , '${__ctx}'+'/login')"><spring:message code="LB.Logout"/></button>
		</span>
		
		<!-- 登入成功時間 -->
		<span class="id-time">
			<fmt:setLocale value="${pageContext.response.locale}"/>
			<fmt:formatDate value="${sessionScope.login_time}" type="both" dateStyle="full"/>
			&nbsp;<spring:message code="LB.Login_successful"/></span>
		
		<!-- 自動登出剩餘時間 -->
		<span class="id-time">
			<spring:message code="LB.Remaining"/>
			<!-- 分 -->
			<font color="red" id="countdownMin"></font>
			:
			<!-- 秒 -->
			<font color="red" id="countdownSec"></font>
			
			<img src="${__ctx}/img/reset-timeout-${pageContext.response.locale}.svg" onclick="timeLogout()">
		</span>
	</div>
	
	<!-- 快速選單目前先不開放
		<div id="fast-menu" class="collapse" role="tabpanel">
			<a href="#fast-menu-block" class="fast-menu-btn" data-toggle="collapse" data-parent="#fast-menu" aria-expanded="true" aria-controls="fast-menu-block" role="button">
			<spring:message code="LB.Quick_Menu"/></a>
		</div>
		<div id="fast-menu-block" class="collapse" role="tabpanel">
			<div id="mobile-header">
			    <img style="visibility: hidden;" src="${__ctx}/img/group-2.svg">
			    <a href="#fast-menu-block" data-toggle="collapse" data-parent="#fast-menu" aria-expanded="true" aria-controls="fast-menu-block" role="button">
			        <img src="${__ctx}/img/icon-close.svg">
			    </a>
			</div>
			<div class="mobile-fast-menu-title"><spring:message code="LB.Quick_Menu"/></div>
		    <ul>
		        <li>
					<a href='#'><spring:message code="LB.NTD_Services"/></a>
					<span onclick="fstop.getPage('${pageContext.request.contextPath}'+'/NT/ACCT/balance_query','', '')"><spring:message code="LB.NTD_Demand_Deposit_Balance"/></span>
				</li>
		        <li>
					<a href='#'><spring:message code="LB.NTD_Services"/></a>
					<span onclick="fstop.getPage('${pageContext.request.contextPath}'+'/NT/ACCT/TRANSFER/transfer','', '')"><spring:message code="LB.NTD_Transfer"/></span>
				</li>
		    </ul>
		</div>
	-->
</section>

<!-- 功能選單 -->
<nav id="ttb-menu">
    <button id="nav-left-btn" class="triangle-left"></button>
    <ul id="nav-menu" class="menu-list d-inline-block">
        <li>
        	<!-- 我的首頁 -->
            <a href="#main-fuct-1" class="collapsed" onclick="fstop.getPage('${pageContext.request.contextPath}'+'/INDEX/index','', '')"
					data-toggle="collapse" role="button" aria-expanded="true" aria-controls="main-fuct-01">
                <div class="menu-img-block"><img src="${__ctx}/img/menu-icon-01.svg?a=${jscssDate}"></div>
                <p class="menu-title"><spring:message code="LB.My_home"/></p>
            </a>
        </li>
        <li>
        	<!-- 帳戶總覽 -->
            <a href="#main-fuct-2" class="collapsed" onclick="fstop.getPage('${pageContext.request.contextPath}'+'/OVERVIEW/initview','', '')" 
            		data-toggle="collapse" role="button" aria-expanded="true" aria-controls="main-fuct-02">
                <div class="menu-img-block"><img src="${__ctx}/img/menu-icon-02.svg?a=${jscssDate}"></div>
                <p class="menu-title"><spring:message code="LB.Account_Overview"/></p>
            </a>
        </li>
        	<!-- 臺幣服務 -->
        <li>
            <a href="#main-fuct-NTDService" class="collapsed" onclick="main-fuct-NTDService" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="main-fuct-NTDService">
                <div class="menu-img-block"><img src="${__ctx}/img/menu-icon-03.svg?a=${jscssDate}"></div>
                <p class="menu-title"><spring:message code="LB.NTD_Services"/></p>
            </a>
        </li>
        	<!-- 外幣服務 -->
        <li>
            <a href="#main-fuct-FXCurrency" class="collapsed" onclick="main-fuct-FXCurrency" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="main-fuct-FXCurrency">
                <div class="menu-img-block"><img src="${__ctx}/img/menu-icon-04.svg?a=${jscssDate}"></div>
                <p class="menu-title"><spring:message code="LB.FX_Service"/></p>
            </a>
        </li>
            <!-- 繳費繳稅 -->
        <li>
            <a href="#main-fuct-fee_tax" class="collapsed" onclick="main-fuct-fee_tax" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="main-fuct-fee_tax">
                <div class="menu-img-block"><img src="${__ctx}/img/menu-icon-05.svg?a=${jscssDate}"></div>
                <p class="menu-title"><spring:message code= "LB.W0366" /></p>
            </a>
        </li>
        	<!-- 貸款專區 -->
        <li>
            <a href="#main-fuct-loanservice" class="collapsed" onclick="main-fuct-loanservice" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="main-fuct-loanservice">
                <div class="menu-img-block"><img src="${__ctx}/img/menu-icon-06.svg?a=${jscssDate}"></div>
                <p class="menu-title"><spring:message code="LB.Loan_Service"/></p>
            </a>
        </li>
        	<!-- 基金 -->
        <li>
            <a href="#main-fuct-fundservice" class="collapsed" onclick="main-fuct-fundservice" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="main-fuct-fundservice">
                <div class="menu-img-block"><img src="${__ctx}/img/menu-icon-07.svg?a=${jscssDate}"></div>
                <p class="menu-title"><spring:message code="LB.Funds"/></p>
            </a>
        </li>
          <!-- 黃金存摺 -->
        <li>
            <a href="#main-fuct-goldqryservice" class="collapsed" onclick="main-fuct-goldqryservice" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="main-fuct-goldqryservice">
                <div class="menu-img-block"><img src="${__ctx}/img/menu-icon-08.svg?a=${jscssDate}"></div>
                <p class="menu-title"><spring:message code= "LB.W1428" /></p>
            </a>
        </li>
          <!-- 信用卡 -->
        <li>
            <a href="#main-fuct-creditcard" class="collapsed" onclick="main-fuct-creditcard" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="main-fuct-creditcard">
                <div class="menu-img-block"><img src="${__ctx}/img/menu-icon-09.svg?a=${jscssDate}"></div>
                <p class="menu-title"><spring:message code="LB.Credit_Card"/></p>
            </a>
        </li>
        	<!-- 線上服務專區 -->
        <li>
            <a href="#main-fuct-bank30service" class="collapsed" onclick="main-fuct-bank30service" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="main-fuct-bank30service">
                <div class="menu-img-block"><img src="${__ctx}/img/menu-icon-10.svg?a=${jscssDate}"></div>
                <p class="menu-title"><spring:message code= "LB.X0262" /></p>
            </a>
        </li>
        	<!-- 個人服務 -->
        <li>
            <a href="#main-fuct-personalservice" class="collapsed" onclick="main-fuct-personalservice" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="main-fuct-personalservice">
                <div class="menu-img-block"><img src="${__ctx}/img/menu-icon-11.svg?a=${jscssDate}"></div>
                <p class="menu-title"><spring:message code="LB.Personal_Service"/></p>
            </a>
        </li>
    </ul>
    <button id="nav-right-btn" class="triangle-right"></button>
</nav>

<!-- TODO menu之後會改成由DB來產生-->
<!-- 第二個區塊 -->
<nav id="ttb-sec-menu">
    <c:forEach var="dataListLayer1" items="${ sessionScope.menu.data.sort_menu[0].SEED_LAYER }">
	    <div class="sec-menu collapse" id="main-fuct-${dataListLayer1.ADOPID}" aria-labelledby="main-fuct-${dataListLayer1.ADOPID}" data-parent="#ttb-sec-menu">
	        <ul class="sec-menu-list">
    			<c:forEach var="dataListLayer2" items="${ dataListLayer1.SEED_LAYER }">
		            <li>
		                <p class="sec-menu-title">${dataListLayer2.ADOPNAME}</p>
	                	<ul class="third-menu-list">
	    					<c:forEach var="dataListLayer3" items="${ dataListLayer2.SEED_LAYER }">
			                    <li>
			                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('0') }">
				                    	<a href='#' onclick="fstop.getPage('${pageContext.request.contextPath}'+'${dataListLayer3.URL}','', '')">
				                   			${dataListLayer3.ADOPNAME}
				                   		</a>
			                   		</c:if>
			                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('1') }">
				                    	<a target="_blank" href="${pageContext.request.contextPath}${dataListLayer3.URL}">
											${dataListLayer3.ADOPNAME}
										</a>
			                    	</c:if>
			                    	<c:if test="${ dataListLayer3.ISPOPUP.equals('2') }">
				                    	<a target="_blank" href="${dataListLayer3.URL}${sessionScope.cusidn}">
											${dataListLayer3.ADOPNAME}
										</a>
			                    	</c:if>
			                   	</li>
	    					</c:forEach>
	                	</ul>
		            </li>
	            </c:forEach>
	        </ul>
	    </div>
    </c:forEach>
</nav>
<script type="text/javascript">

</script>