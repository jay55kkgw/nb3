<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
</head>

<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Register" /></li>
    <!-- 數位存款帳戶晶片金融卡確認領用申請及變更密碼     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1535" /></li>
		</ol>
	</nav>

	
	<div class="content row">
		<main class="col-12">
			<section id="main-content" class="container">
				<h2><spring:message code="LB.D1535"/></h2>
				<div id="step-bar">
					<ul>
						<li class="finished">身份驗證</li>
						<li class="finished">變更密碼</li>
						<li class="active">完成申請</li>
					</ul>
				</div>
				<div class="main-content-block row">
					<div class="col-12 terms-block">
						<div class="ttb-message">
							<p>完成申請</p>
						</div>
						<div class="text-left">
							<p>親愛的客戶您好：</p>
							<p>您已完成數位存款帳戶晶片金融卡啟用及變更密碼，如有任何問題，您可詢問指定的服務分行： ${financial_card_confirm_step4.data.BRHNAME} (聯絡電話： ${financial_card_confirm_step4.data.BRHTEL}) 詢問，我們將竭誠為您服務。</p>
							<p>祝您事業順心，萬事如意！</p>

						</div>
						
<!-- 						<input type="BUTTON" class="ttb-button btn-flat-gray" value="列印結果> -->
						<input type="button" id="CLOSEPAGE" name="CLOSEPAGE" class="ttb-button btn-flat-orange" value="繼續其他申請" onclick="window.close();">
					</div>
				</div>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>

</html>