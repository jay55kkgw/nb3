<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="card-detail-block">
	<div class="card-center-block">

		<!-- Q1 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-1" aria-expanded="true"
				aria-controls="popup4-1">
				<div class="row">
					<span class="col-1">Q1</span>
					<div class="col-11">
						<span>What are the voucher registry service items?</span>
					</div>
				</div>
			</a>
			<div id="popup4-1" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>Provide financial XML certificate application, update,
								suspension, lifting and abolition,etc.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q2 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-2" aria-expanded="true"
				aria-controls="popup4-2">
				<div class="row">
					<span class="col-1">Q2</span>
					<div class="col-11">
						<span>What is the scope of use of XML voucher?</span>
					</div>
				</div>
			</a>
			<div id="popup4-2" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>The transaction releasing for the functions of online
								banking, electronic bills and other systems, and the main
								difference from the transaction password is a high-risk
								transaction that can be applied to non-designated transfers and
								non-personal fee payment.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q3 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-3" aria-expanded="true"
				aria-controls="popup4-3">
				<div class="row">
					<span class="col-1">Q3</span>
					<div class="col-11">
						<span>How to apply for using XML voucher?</span>
					</div>
				</div>
			</a>
			<div id="popup4-3" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>Please bring your identity documents, deposit seals and
								passbook to the Bank to fill in the "Taiwan Business Bank
								Financial XML Voucher Application / Temporary Prohibition /
								Lifting / Abolition Application" to complete the relevant
								formalities for the voucher.</p>
							<p>After completing the voucher agreement procedure and
								obtaining the certificate identification information (Common
								Name, CN) issued by the Bank, the customer can log in to the
								Voucher Registry to apply for the issuance of the voucher. The
								identity document, the legal person of the company registration
								certificate, the person in charge of the identity card; the
								natural person is the identity document.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q4 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-4" aria-expanded="true"
				aria-controls="popup4-4">
				<div class="row">
					<span class="col-1">Q4</span>
					<div class="col-11">
						<span>How do I log in to the voucher registry?</span>
					</div>
				</div>
			</a>
			<div id="popup4-4" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>Please go to the Bank's online banking portal
								(https://ebank.tbb.com.tw) -&gt; Enter online banking login
								information -&gt; Click on "Voucher Registry" under e-Services
								-&gt; Enter the ID/Profit business unified number and online
								banking transaction password to log in to the voucher registry.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q5 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-5" aria-expanded="true"
				aria-controls="popup4-5">
				<div class="row">
					<span class="col-1">Q5</span>
					<div class="col-11">
						<span>Do I need to set or adjust the computer when I log in
							to the Voucher Registry to apply for a voucher?</span>
					</div>
				</div>
			</a>
			<div id="popup4-5" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>Please log in from the Internet Banking Portal and log in
								to the "Voucher Registry". Refer to the "Voucher Vehicle Driver
								Installation Process" on the online manual and the "Network
								Environment Settings" in the Q&A.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q6 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-6" aria-expanded="true"
				aria-controls="popup4-6">
				<div class="row">
					<span class="col-1">Q6</span>
					<div class="col-11">
						<span>How do you perform various functions such as voucher
							application, update, and suspension?</span>
					</div>
				</div>
			</a>
			<div id="popup4-6" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>Customers can log in to the voucher registration and click
								on “Online Description” to get the operation flow of each
								operation.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q7 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-7" aria-expanded="true"
				aria-controls="popup4-7">
				<div class="row">
					<span class="col-1">Q7</span>
					<div class="col-11">
						<span>How long is the validity period of the voucher?</span>
					</div>
				</div>
			</a>
			<div id="popup4-7" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>Currently one year, 30 days before the expiration of the
								voucher, the customer should log in to the voucher registry to
								process the voucher renewal (extension).</p>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q8 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-8" aria-expanded="true"
				aria-controls="popup4-8">
				<div class="row">
					<span class="col-1">Q8</span>
					<div class="col-11">
						<span>What is the amount of the fee to apply/update the
							voucher?</span>
					</div>
				</div>
			</a>
			<div id="popup4-8" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>The legal person certificate is NTD900 yuan per piece, and
								the individual household is NTD 150 yuan per piece. The service
								period is one year.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q9 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-9" aria-expanded="true"
				aria-controls="popup4-9">
				<div class="row">
					<span class="col-1">Q9</span>
					<div class="col-11">
						<span>How can I check if the voucher has expired?</span>
					</div>
				</div>
			</a>
			<div id="popup4-9" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div class="ttb-result-list terms">
								<ol>
									<li>Method one: On the homepage of Taiwan Business Bank
										(https://ebank.tbb.com.tw), click“Environment
										Settings” and download “9. Taiwan Business Bank Voucher
										Vehicle Reader Download”, according to the "Taiwan Business
										Bank Voucher Vehicle Reading Tool Introduction" operation
										instructions to view the "Voucher Valid Expiration Date".</li>
									<li>Method two: Query/Update/Download Voucher -&gt; Click
										"Content" to view the "Valid Voucher Expiration Date".</li>
								</ol>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q10 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-10" aria-expanded="true"
				aria-controls="popup4-10">
				<div class="row">
					<span class="col-1">Q10</span>
					<div class="col-11">
						<span>How to update the extension of the voucher?</span>
					</div>
				</div>
			</a>
			<div id="popup4-10" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>The customer can log in to the voucher registry 30 days
								before the expiration date of the voucher, click on the “User
								Update Voucher” and select the button behind the voucher to
								extension. (For detailed operation flow, please refer to the
								online description of the voucher registry).</p>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q11-->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-11" aria-expanded="true"
				aria-controls="popup4-11">
				<div class="row">
					<span class="col-1">Q11</span>
					<div class="col-11">
						<span>How to deal with when the voucher expires (cannot
							perform voucher update)?</span>
					</div>
				</div>
			</a>
			<div id="popup4-11" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>Please refer to Q3 to the Bank's application voucher,
								obtain the voucher identification information (Common Name, CN)
								issued by the Bank, and then log in to the Voucher Registry to
								apply for the issuance of the voucher.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q12 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-12" aria-expanded="true"
				aria-controls="popup4-12">
				<div class="row">
					<span class="col-1">Q12</span>
					<div class="col-11">
						<span>What should I do if the deduction fails when the
							voucher application/update (extension) occurs?</span>
					</div>
				</div>
			</a>
			<div id="popup4-12" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>If the reason for the failure of the deduction is
								“Insufficient account balance”, please make sure that there is
								enough money in the bank account to deduct the fee. If the
								reason for the failure is “Check for no debit account”, please
								contact the Bank to set up the debit account or pay voucher fee.
								After the above operations are completed, please re-apply/update
								the voucher by following the steps below:</p>
							<ol>
								<li>1、Click "Query / Update / Download Voucher".</li>
								<li>2、Click the small icon button behind the voucher.</li>
								<li>3、Click the Resend Request Voucher button to
									apply/update the voucher again.</li>
							</ol>

						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q12 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-13" aria-expanded="true"
				aria-controls="popup4-13">
				<div class="row">
					<span class="col-1">Q13</span>
					<div class="col-11">
						<span>If the customer replaces the computer or replaces the
							computer operating system, can I continue to use the original
							voucher?</span>
					</div>
				</div>
			</a>
			<div id="popup4-13" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>Customers need to perform the "Network Environment
								Settings" and "Device Driver Installation" to continue using the
								original voucher. Please refer to the computer settings
								instructions in Q5.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q14 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-14" aria-expanded="true"
				aria-controls="popup4-14">
				<div class="row">
					<span class="col-1">Q14</span>
					<div class="col-11">
						<span>What should I do if the voucher has unsafe concerns
							such as fraudulent use, exposure and loss?</span>
					</div>
				</div>
			</a>
			<div id="popup4-14" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>The customer should log in to the voucher registration
								center or contact the Bank for temporary suspension or abolition
								of the voucher.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q15 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-15" aria-expanded="true"
				aria-controls="popup4-15">
				<div class="row">
					<span class="col-1">Q15</span>
					<div class="col-11">
						<span>How many times will the vehicle password be locked
							incorrectly?</span>
					</div>
				</div>
			</a>
			<div id="popup4-15" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>If the vehicle password is incorrect for more than 5
								times, it will be locked.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q16 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-16" aria-expanded="true"
				aria-controls="popup4-16">
				<div class="row">
					<span class="col-1">Q16</span>
					<div class="col-11">
						<span>What should I do when the carrier code is locked?</span>
					</div>
				</div>
			</a>
			<div id="popup4-16" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>

							<p>Please return the vehicle to the business unit and go
								through the relevant procedures for resetting the operation.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q17 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-17" aria-expanded="true"
				aria-controls="popup4-17">
				<div class="row">
					<span class="col-1">Q17</span>
					<div class="col-11">
						<span>How to reprint the "Voucher Fee Receipt"?</span>
					</div>
				</div>
			</a>
			<div id="popup4-17" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>Click "Content" and select "Print Voucher Fee Receipt".</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>