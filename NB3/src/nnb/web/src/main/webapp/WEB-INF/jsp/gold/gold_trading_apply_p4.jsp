<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
	
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>	
</head>
 <body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>

	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 黃金存摺帳戶     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2273" /></li>
    <!-- 黃金存摺網路交易申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1687" /></li>
		</ol>
	</nav>

 	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
	<main class="col-12"> 
	<!-- 		主頁內容  -->
		<section id="main-content" class="container">
		<!-- 	線上申請黃金存摺網路交易 -->
			<h2><spring:message code="LB.W1687"/></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
            <div id="step-bar">
                <ul>
					<li class="finished">注意事項與權益</li>
                    <li class="finished">設定帳戶</li>
                    <li class="active">確認資料</li>
                    <li class="">申請結果</li>
                </ul>
            </div>
			<!-- 請確認申請資料 -->						
<%-- 			<p style="text-align: center;color: red;"><spring:message code="LB.D0104"/></p> --%>
			<form method="post" id="formId">
				<input type="hidden" id="back" name="back" value="">
				<input type="hidden" id="TOKEN" name="TOKEN" value="${result_data.TOKEN}" /><!-- 防止重複交易 -->
                
                <!-- ikey -->
				<input type="hidden" name="ADOPID" value="NA60">
				<input type="hidden" id="jsondc" name="jsondc" value='${ result_data.jsondc }'>
				<input type="hidden" id="ISSUER" name="ISSUER" value="">
				<input type="hidden" id="ACNNO" name="accNo" value="">
				<input type="hidden" id="TRMID" name="TRMID" value="">
				<input type="hidden" id="iSeqNo" name="icSeq" value="">
				<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
				<input type="hidden" id="TAC" name="TAC" value="">
				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
				<input type="hidden" name="CMTRANPAGE" value="1">
				<input type="hidden" name="braCode" value="">
				<input type="hidden" name="chk" value="">
				
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
							<div class="ttb-message">
								<p>確認資料</p>
							</div>
                            <div>
								<!-- 請確認取消資料 -->						
                                <p><spring:message code= "LB.D0104" /></p>
                            </div>
						
							<!-- ***************** image ****************** -->
                            <div class="classification-block">
                                <p>網路交易帳戶設定</p>
<!-- 	                                <a href="#" class="classification-edit-btn">編輯</a> -->
                            </div>
							<!-- ***************************************** -->
							<!-- 交易種類 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
								<!-- 黃金存摺帳號 -->
									<h4>
										<spring:message code="LB.D1090"/>
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>${fn:replace(result_data.ACN,',','<br>' ) }</p>
									</div>
								</span>
								<input type="hidden" id="ACN" name="ACN" value="${ result_data.ACN }">
							</div>
							
							<!-- 交易種類 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
								<!-- 指定網路銀行交易/申購/回售新台幣帳戶 -->
									<h4>
										網路交易指定新台幣帳戶
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>${ result_data.SVACN }</p>
									</div>
								</span>
								<input type="hidden" id="SVACN" name="SVACN" value="${ result_data.SVACN }">
							</div>
							
							<!-- ***************** image ****************** -->
                            <div class="classification-block">
                                <p>身份驗證</p>
<!-- 	                                <a href="#" class="classification-edit-btn">編輯</a> -->
                            </div>
							<!-- ***************************************** -->
							
							<!--交易機制  SSL 密碼 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.Transaction_security_mechanism" />
										</h4>
									</label>
								</span> 
								<span class="input-block">
<!-- 									<div class="ttb-input"> -->
<!-- 										<label> -->
<%-- 								       		<input type="radio" name="FGTXWAY" id="CMSSL" value="0" checked /><spring:message code="LB.SSL_password" /> --%>
<!-- 								       	</label> -->
<!-- 										<input type="password" name = "CMPASSWORD" id="CMPASSWORD"  maxlength="8" class="text-input validate[required, pwvd[PW, CMPASSWORD], custom[onlyLetterNumber]]"> -->
<!-- 										<input type="hidden" name = "PINNEW" id="PINNEW" value=""> -->
<!-- 									</div> -->
<!-- 									使用者是否可以使用IKEY -->
									<c:if test = "${sessionScope.isikeyuser}">
<!-- 										即使使用者可以用IKEY，若轉出帳號為晶片金融卡帶出的非約定帳號，也不給用 (By 景森) -->
<%-- 										<c:if test = "${transfer_data.data.TransferType != 'NPD'}"> --%>
									
<!-- 											IKEY -->
											<div class="ttb-input">
												<label class="radio-block">
													<input type="radio" name="FGTXWAY" id="CMIKEY" value="1">
													<spring:message code="LB.Electronic_signature" />
													<span class="ttb-radio"></span>
												</label>
											</div>
									
<%-- 										</c:if> --%>
									</c:if>
									<!-- 晶片金融卡 -->
									<div class="ttb-input">
										<label class="radio-block">
											<spring:message code="LB.Financial_debit_card" />
											
											<!-- 只能選晶片金融卡時，預設為非勾選讓使用者點擊後顯示驗證碼-->
											<c:choose>
												<c:when test="${result_data.TransferType == 'NPD'}">
													<input type="radio" name="FGTXWAY" id="CMCARD" value="2">
												</c:when>
												<c:when test="${!sessionScope.isikeyuser}">
													<input type="radio" name="FGTXWAY" id="CMCARD" value="2">
												</c:when>
												<c:otherwise>
												 	<input type="radio" name="FGTXWAY" id="CMCARD" value="2" checked="checked">
												</c:otherwise>
											</c:choose>
											
											<span class="ttb-radio"></span>
										</label>
									</div>
									
								</span>
							</div>
							
							<!-- 確認新使用者名稱 -->
							<div class="ttb-input-item row" id="capCodeDiv">
								<span class="input-title"> 
									<label>
									<!-- 驗證碼 -->
										<h4>
											<spring:message code="LB.Captcha"/>
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<spring:message code="LB.Captcha" var="labelCapCode" />
										<input id="capCode" type="text" class="text-input input-width-125"
											name="capCode" placeholder="${labelCapCode}" maxlength="6" autocomplete="off">
										<img name="kaptchaImage" src=""  class="verification-img"/>
										<input type="button" name="reshow" class="ttb-sm-btn btn-flat-gray" onclick="refreshCapCode()" value="<spring:message code="LB.Regeneration"/>" />
										<span class="input-remarks">請注意：英文不分大小寫，限半形字元</span>
									</div>
								</span>
							</div>
							
						</div>
						<!--回上頁 -->
                        <spring:message code="LB.X0318" var="cmback"></spring:message>
                        <input type="button" name="CMBACK" id="CMBACK" value="${cmback}" class="ttb-button btn-flat-gray">	
<%-- 						<input id="reset" name="reset" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" />	 --%>
						<input id="pageshow" name="pageshow" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />送出" />					
					</div>
				</div>
			</form>
			<div class="text-left">
			    <!-- 		說明： -->
				
			    <ol class="list-decimal description-list">
			    	<p><spring:message code="LB.Description_of_page"/></p>
			    <!-- 如選擇晶片金融卡機制，按「確定」鍵前，請將讀卡機正確連接電腦，並插入晶片金融卡。 -->
			        <li><spring:message code="LB.Gold_Trading_Apply_P4_D1"/></li>
			    </ol>
			</div>
		</section>
		<!-- 		main-content END -->
	</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
	<!--   Js function -->
    <script type="text/JavaScript">
		$(document).ready(function() {
			// HTML載入完成後0.1秒開始遮罩
			setTimeout("initBlockUI()", 100);

			// 初始化驗證碼
			setTimeout("initKapImg()", 200);
			// 生成驗證碼
			setTimeout("newKapImg()", 300);
			
			init();
			fgtxwayClick();
			// 過0.5秒解遮罩
			setTimeout("unBlockUI(initBlockId)", 800);
		});
		
		/**
		 * 初始化BlockUI
		 */
		function initBlockUI() {
			initBlockId = blockUI();
		}

		// 交易機制選項
		function processQuery(){
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
		
			switch(fgtxway) {
				case '0':
// 					alert("交易密碼(SSL)...");
	    			$("form").submit();
					break;
					
				case '1':
// 					alert("IKey...");
					var jsondc = $("#jsondc").val();
					
					// 遮罩後不給捲動
// 					document.body.style.overflow = "hidden";

					// 呼叫IKEY元件
// 					uiSignForPKCS7(jsondc);
					
					// 解遮罩後給捲動
// 					document.body.style.overflow = 'auto';
					
					useIKey();
					break;
					
				case '2':
// 					alert("晶片金融卡");

					// 遮罩後不給捲動
// 					document.body.style.overflow = "hidden";
					
					// 呼叫讀卡機元件
					var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
					useCardReader(capUri);

					// 解遮罩後給捲動
// 					document.body.style.overflow = 'auto';
					
			    	break;
			    	
				default:
					//alert("nothing...");
					errorBlock(
							null, 
							null,
							["nothing..."], 
							'<spring:message code= "LB.Quit" />', 
							null
					);
			}
			
		}

	    function init(){
// 	    	$("#formId").validationEngine({binded:false,promptPosition: "inline" });
	    	$("#pageshow").click(function(e){			
				console.log("submit~~");
				var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
				var capData = $("#formId").serializeArray();
				var capResult = fstop.getServerDataEx(capUri, capData, false);
				if (capResult.result || $('input[name="FGTXWAY"]:checked').val() != 2) {
					if(!$('#formId').validationEngine('validate') && $('input[name="FGTXWAY"]:checked').val() == 0){
			        	e.preventDefault();
		 			}else{
	// 					$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
		 				$("#formId").validationEngine('detach');
		 				initBlockUI();
		    			var action = '${__ctx}/GOLD/APPLY/gold_trading_apply_result';
		    			$("form").attr("action", action);
		    			unBlockUI(initBlockId);
		    			processQuery();
		 			}
				} else {
					//驗證碼有誤
					//alert("<spring:message code= "LB.X1082" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1082' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
					);
					changeCode();
				}
			});
    		//上一頁按鈕
    		$("#CMBACK").click(function() {
    			var action = '${__ctx}/GOLD/APPLY/gold_trading_apply_p3';
    			$('#back').val("Y");
    			$("form").attr("action", action);
    			$("form").submit();
    		});
	    }	
	    
		// 刷新驗證碼
		function refreshCapCode() {
			console.log("refreshCapCode...");

			// 驗證碼
			$('input[name="capCode"]').val('');
			
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

			// 登入失敗解遮罩
			unBlockUI(initBlockId);
		}
		
		// 刷新輸入欄位
		function changeCode() {
			console.log("changeCode...");
			
			// 清空輸入欄位
			$('input[name="capCode"]').val('');
			
			// 刷新驗證碼
			refreshCapCode();
		}

		// 初始化驗證碼
		function initKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
		}

		// 生成驗證碼
		function newKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').click(function() {
				refreshCapCode();
			});
		}

		// 使用者選擇晶片金融卡要顯示驗證碼區塊
	 	function fgtxwayClick() {
	 		$('input[name="FGTXWAY"]').change(function(event) {
	 			// 判斷交易機制決定顯不顯示驗證碼區塊
	 			chaBlock();
			});
		}
	 	// 判斷交易機制決定顯不顯示驗證碼區塊
		function chaBlock() {
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
 			console.log("fgtxway: " + fgtxway);
			// 交易機制選項
			if(fgtxway == '2'){
				// 若為晶片金融卡才顯示驗證碼欄位
				$("#capCodeDiv").show();
			}else{
				// 若非晶片金融卡則隱藏驗證碼欄位
				$("#capCodeDiv").hide();
			}
	 	}

 	</script>
</body>
</html>
 