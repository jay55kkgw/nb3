<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ include file="../__import_js.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
	$(document).ready(function() {
		window.print();
	});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br />
	<br />
	<div style="text-align: center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br />
	<br />
	<br />
	<div style="text-align: center">
		<font style="font-weight: bold; font-size: 1.2em">${jspTitle}</font>
	</div>
	<br />
	<br />
	<br />
<!-- 黃金存摺帳號 -->
	<label><spring:message code= "LB.D1090" /> ：</label>
	<label>${ACN}</label>
	<br />
	<!-- 查詢期間 -->
	<label><spring:message code="LB.Inquiry_period_1" /> ：</label>
	<label>
		<c:choose>
								<c:when test="${CMPERIOD == '最後一次異動' }">				
								<spring:message code="LB.W1597"/>
								</c:when>
								<c:otherwise>
								${CMPERIOD}							
     </c:otherwise>
	 </c:choose>
	</label>
	<br />
	<!-- 資料總數 -->
	<label><spring:message code="LB.Total_records" /> ：</label>
	<label>${COUNT} <spring:message code="LB.Rows" /></label>
	<br />
	<c:forEach var="dataList" items="${dataListMap}">
	<table class="print">
		<thead>
			<tr>
				<!--異動時間 -->
				<th rowspan="2"><spring:message code= "LB.W1588" /></th>
				<!--異動來源 -->
				<th rowspan="2"><spring:message code= "LB.W1589" /></th>
				<!--扣款帳號(限臨櫃變更) -->
				<th colspan="2"><spring:message code= "LB.W1590" /></th>
				<!--扣款手續費-->
				<th colspan="2"><spring:message code= "LB.W1591" /></th>
				<!--每月扣款日-->
				<th rowspan="2"><spring:message code= "LB.W1592" /></th>
				<!--扣款金額 -->
				<th colspan="2"><spring:message code= "LB.W1079" /></th>
				<!--投資狀態 -->
				<th colspan="2"><spring:message code= "LB.W1594" /></th>
			</tr>
			<tr>
				<!--異動前 -->
				<th><spring:message code= "LB.W1595" /></th>
				<!--異動後 -->
				<th><spring:message code= "LB.W1596" /></th>
				<!--異動前 -->
				<th><spring:message code= "LB.W1595" /></th>
				<!--異動後 -->
				<th><spring:message code= "LB.W1596" /></th>
				<!--異動前 -->
				<th><spring:message code= "LB.W1595" /></th>
				<!--異動後 -->
				<th><spring:message code= "LB.W1596" /></th>
				<!--異動前 -->
				<th><spring:message code= "LB.W1595" /></th>
				<!--異動後 -->
				<th><spring:message code= "LB.W1596" /></th>
			</tr>
		</thead>
		<!--          列表區 -->
		<tbody>

				<tr>
					<td rowspan="3" class="text-center">${dataList.LTD}<br>${dataList.TIME}</td>
					<td rowspan="3" class="text-center"><c:if test="${not empty dataList.SOURCE	}">
								${dataList.SOURCE}
								</c:if></td>
					<td rowspan="3">${dataList.BTSFACN}</td>
					<td rowspan="3" class="text-center">${dataList.ATSFACN}</td>
					<td rowspan="3" class="text-center">${dataList.BFEEAMT}</td>
					<td rowspan="3" class="text-center">${dataList.AFEEAMT}</td>

					<td class="text-center">06</td>
					<td class="text-right">${dataList.BQTAMT1}</td>
					<td class="text-right">${dataList.AQTAMT1}</td>
					<td class="text-center"><c:if test="${not empty dataList.BSTATUS1}">
								${dataList.BSTATUS1}
								</c:if></td>
					<td class="text-center"><c:if test="${not empty dataList.ASTATUS1}">
								${dataList.ASTATUS1}
								</c:if></td>
				</tr>
				<tr>
					<td class="text-center">16</td>
					<td class="text-right">${dataList.BQTAMT2}</td>
					<td class="text-right">${dataList.AQTAMT2}</td>
					<td class="text-center"><c:if test="${not empty dataList.BSTATUS2}">
								${dataList.BSTATUS2}
								</c:if></td>
					<td class="text-center"><c:if test="${not empty dataList.ASTATUS2}">
								${dataList.ASTATUS2}
								</c:if></td>
				</tr>
				<tr>
					<td class="text-center"	>26</td>
					<td class="text-right">${dataList.BQTAMT3}</td>
					<td class="text-right">${dataList.AQTAMT3}</td>
					<td class="text-center"><c:if test="${not empty dataList.BSTATUS3}">
								${dataList.BSTATUS3}
								</c:if></td>
					<td class="text-center"><c:if test="${not empty dataList.ASTATUS3}">
								${dataList.ASTATUS3}
								</c:if></td>
				</tr>

		</tbody>
	</table>
	<br />
	</c:forEach>
	<br />
	<br />
</body>
</html>