<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="col-lg-6 col-12 pr-lg-0">
	<p class="home-title"><spring:message code="LB.My_calendar"/></p>
	<div class="main-content-block">
		<div id='calendar'>
		</div>
	</div>
</div>

<!-- pop up -->
<div class="modal fade" id="ttbStatement" role="dialog" aria-labelledby="ttbStatementTitle" aria-hidden="true" tabindex="-1">
	<div class="modal-dialog modal-dialog-centered calendar-modal" role="document">
		<div class="modal-content">
			<div class="modal-header mb-0">
			<!-- 預約事項 -->
				<p class="ttb-pup-header"><spring:message code="LB.X2170"/></p>
			</div>
			<div class="modal-body ttb-input-block m-0 pl-5">

				<div class="ttb-input-item row">
					<span class="input-title"><label>
					<!-- 轉出帳號 -->
							<h4><spring:message code="LB.Payers_account_no"/></h4>
						</label></span>
					<span class="input-block">
						<div class="ttb-input">
							<span id="T_DPWDAC"></span>
						</div>
					</span>
				</div>
				<div class="ttb-input-item row">
					<span class="input-title"><label>
					<!-- 轉入行/轉入帳號 -->
							<h4><spring:message code="LB.X2171"/></h4>
						</label></span>
					<span class="input-block">
						<div class="ttb-input">
							<span id="T_DPSVAC"></span>
						</div>
					</span>
				</div>
				<div class="ttb-input-item row">
					<span class="input-title"><label>
					<!-- 交易金額 -->
							<h4 id="T_WDSV"><spring:message code='LB.W0016' /></h4>
						</label></span>
					<span class="input-block">
						<div class="ttb-input">
							<span id="T_DPTXAMT"></span>
						</div>
					</span>
				</div>
				<div class="ttb-input-item row">
					<span class="input-title"><label>
					<!-- 交易類別 -->
							<h4><spring:message code='LB.W0076' /></h4>
						</label></span>
					<span class="input-block">
						<div class="ttb-input">
							<span id="T_TYPE"></span>
						</div>
					</span>
				</div>
				<div class="ttb-input-item row">
					<span class="input-title"><label>
					<!-- 狀態 -->
							<h4><spring:message code='LB.Status' /></h4>
						</label></span>
					<span class="input-block">
						<div class="ttb-input">
							<span id="T_STATUS"></span>
						</div>
					</span>
				</div>
				<div class="ttb-input-item row">
					<span class="input-title"><label>
					<!-- 備註 -->
							<h4><spring:message code='LB.D0078' /></h4>
						</label></span>
					<span class="input-block">
						<div class="ttb-input">
							<span id="T_MEMO"></span>
						</div>
					</span>
				</div>
			</div>
			<div class="modal-footer ttb-pup-footer">
			<!-- 關閉 -->
				<input type="submit" class="ttb-pup-btn btn-flat-gray" data-dismiss="modal" value="<spring:message code='LB.X1572' />" />

			</div>
		</div>
	</div>
</div>

<script type="text/JavaScript">

// 行事曆內容
var showArr = [];

i18n['booking'] = '<spring:message code="LB.Booking" />' ; // 預約
i18n['nt_transfer'] = '<spring:message code="LB.NTD_Transfer" />' ; // 臺幣轉帳
i18n['fx_transfer'] = '<spring:message code="LB.FX_Exchange_Transfer" />' ; // 外匯結購售/轉帳

var titleTW = i18n['booking'] + ' ' + i18n['nt_transfer']; // 預約 臺幣轉帳
var titleFX = i18n['booking'] + ' ' + i18n['fx_transfer']; // 預約 外匯結購售/轉帳

// 取得臺幣預約轉帳查詢取消資料
function getReservDetail(){
	console.log("getReservDetail.now: " + new Date());
	
	uri = '${__ctx}'+"/INDEX/index_TW";
// 	console.log("index_tw_uri: " + uri);
	fstop.getServerDataEx(uri, null, true, reservDetail, null, titleTW);
}
var caldata = [];
// 取得臺幣預約轉帳查詢取消資料
function reservDetail(data, title){
	console.log("reservDetail." + title + ".now: " + new Date());
	
	if(data) {
		data = data.data.data;
		for (i = 0; i < data.length; i++) { 
			var d = {};
			if(locale =="zh_CN"){
				d["title"]=data[i][8];
			}
			else if(locale == "en"){
				d["title"]=data[i][7];
			}
			else{
				d["title"]=data[i][6];
			}
			d["start"]=data[i][0].substr(0,4)+"-"+data[i][0].substr(4,2)+"-"+data[i][0].substr(6,2);
			d["color"]="#FF8000";
			d["data"]=data[i];
			d["type"]="TW";
			caldata.push(d);
		}
		showFullCalendar();
	} else {
		console.log("Oops");
	}
	
	console.log("reservDetail." + title + ".end: " + new Date());
}

// 取得外幣預約轉帳查詢取消資料
function getFcyReservDetail(){
	console.log("getFcyReservDetail.now: " + new Date());
	
	uri = '${__ctx}'+"/INDEX/index_FX";
// 	console.log("index_fx_uri: " + uri);
	fstop.getServerDataEx(uri, null, true, fcyReservDetail, null, titleFX);
}
// 取得外幣預約轉帳查詢取消資料
function fcyReservDetail(data, title){
	console.log("fcyReservDetail." + title + ".now: " + new Date());
	
	if(data) {
		data = data.data.data;
		for (i = 0; i < data.length; i++) { 
			var d = {};
			if(locale =="zh_CN"){
				d["title"]=data[i][11];
			}
			else if(locale == "en"){
				d["title"]=data[i][10];
			}
			else{
				d["title"]=data[i][9];
			}
			d["start"]=data[i][0].substr(0,4)+"-"+data[i][0].substr(4,2)+"-"+data[i][0].substr(6,2);
			d["color"]="#FF8000";
			d["data"]=data[i];
			d["type"]="FX";
			caldata.push(d);
		}
		showFullCalendar();
	} else {
		console.log("Oops");
	}
	console.log("fcyReservDetail." + title + ".end: " + new Date());
}
function outputdollars(num) {
	   num = num.toString().replace(/\$|\,/g,'');
	    if(isNaN(num))
	        num = "0";
	    sign = (num == (num = Math.abs(num)));
	    num = Math.floor(num*100+0.50000000001);
	    cents = num%100;
	    num = Math.floor(num/100).toString();
	    if(cents<10)
	    cents = "0" + cents;
	    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
	    num = num.substring(0,num.length-(4*i+3))+','+
	    num.substring(num.length-(4*i+3));
	    return (((sign)?'':'-') + num + '.' + cents);
}

var calendar;
function createCalendar(){
	var initialLocaleCode = locale.replace("_","-"); // 選擇語系'zh-tw','zh-cn','zh-en'
	var localeSelectorEl = document.getElementById('locale-selector');
	var calendarEl = document.getElementById('calendar');
	calendar = new FullCalendar.Calendar(calendarEl, {
		plugins: ['interaction', 'dayGrid'],
		header: {
			left: 'prev, ,today', // 左邊放置上一頁、下一頁和今天
			center: 'title', // 中間放置標題
			right: 'dayGridMonth, next ' // 右邊放置月、周、天
		},
		defaultDate: new Date().toISOString().slice(0,10), // 起始日期
		locale: initialLocaleCode,
		navLinks: true,
		editable: false, // 啟動拖曳調整日期
		eventLimit: true,
		});
	calendar.render();
}
// 顯示行事曆
function showFullCalendar(){
	console.log("showFullCalendar.now: " + new Date());
	
	if(calendar != undefined){
		calendar.destroy();
	}
	var initialLocaleCode = locale.replace("_","-"); // 選擇語系'zh-tw','zh-cn','zh-en'
	var localeSelectorEl = document.getElementById('locale-selector');
	var calendarEl = document.getElementById('calendar');
	calendar = new FullCalendar.Calendar(calendarEl, {
		plugins: ['interaction', 'dayGrid'],
		header: {
			left: 'prev, ,today', // 左邊放置上一頁、下一頁和今天
			center: 'title', // 中間放置標題
			right: 'dayGridMonth, next ' // 右邊放置月、周、天
		},
		defaultDate: new Date().toISOString().slice(0,10), // 起始日期
		locale: initialLocaleCode,
		navLinks: true,
		editable: false, // 啟動拖曳調整日期
		eventLimit: true,
		events: caldata,
		eventClick: function(event) {
			var statusmap = {};
			statusmap[' ']='<spring:message code="LB.X2173" />';
			statusmap['0']='<spring:message code="LB.D1099" />';//成功
			statusmap['1']='<spring:message code="LB.W0056" />';//失敗
			statusmap['2']='<spring:message code="LB.W0057" />';//處理中
			//TW
			if(event.event.extendedProps.type=="TW"){
			$('#T_DPWDAC').text(event.event.extendedProps.data[1]);
			var DPSVAC = event.event.extendedProps.data[9]+"-";
			if(locale =="zh_CN"){
				DPSVAC+=event.event.extendedProps.data[12];
			}
			else if(locale == "en"){
				DPSVAC+=event.event.extendedProps.data[11];
			}
			else{
				DPSVAC+=event.event.extendedProps.data[10];
			}
			DPSVAC += " ";
			DPSVAC += event.event.extendedProps.data[3];
			$('#T_DPSVAC').text(DPSVAC);
			$('#T_DPTXAMT').text(outputdollars(event.event.extendedProps.data[4]));
			$('#T_WDSV').text("<spring:message code='LB.W0016' />");
			$('#T_STATUS').text(statusmap[event.event.extendedProps.data[13]]);
			$('#T_MEMO').text(event.event.extendedProps.data[5]);
			if(locale =="zh_CN"){
				$('#T_TYPE').text(event.event.extendedProps.data[8]);
			}
			else if(locale == "en"){
				$('#T_TYPE').text(event.event.extendedProps.data[7]);
			}
			else{
				$('#T_TYPE').text(event.event.extendedProps.data[6]);
			}
			
			}
			//FX
			if(event.event.extendedProps.type=="FX"){
				$('#T_DPWDAC').text(event.event.extendedProps.data[1]);
				var DPSVAC = event.event.extendedProps.data[2]+"-";
				if(locale =="zh_CN"){
					DPSVAC+=event.event.extendedProps.data[15];
				}
				else if(locale == "en"){
					DPSVAC+=event.event.extendedProps.data[14];
				}
				else{
					DPSVAC+=event.event.extendedProps.data[13];
				}
				DPSVAC += " ";
				DPSVAC += event.event.extendedProps.data[3];
				$('#T_DPSVAC').text(DPSVAC);
				//金額
				var amt = "";
				if(event.event.extendedProps.data[5]==""){
					amt +=event.event.extendedProps.data[6];
					amt +=" ";
					amt +=outputdollars(event.event.extendedProps.data[7]);
					$('#T_WDSV').text("<spring:message code='LB.Buy'/>");//轉入金額
				}
				if(event.event.extendedProps.data[7]==""){
					amt +=event.event.extendedProps.data[4];
					amt +=" ";
					amt +=outputdollars(event.event.extendedProps.data[5]);
					$('#T_WDSV').text("<spring:message code='LB.Deducted'/>");//轉出金額
				}
				$('#T_DPTXAMT').text(amt);
				$('#T_STATUS').text(statusmap[event.event.extendedProps.data[16]]);
				$('#T_MEMO').text(event.event.extendedProps.data[8]);
				if(locale =="zh_CN"){
					$('#T_TYPE').text(event.event.extendedProps.data[11]);
				}
				else if(locale == "en"){
					$('#T_TYPE').text(event.event.extendedProps.data[10]);
				}
				else{
					$('#T_TYPE').text(event.event.extendedProps.data[9]);
				}
				
			}
			$('#ttbStatement').modal('show');
			
		},
	});
	calendar.render();
	
	console.log("showFullCalendar.end: " + new Date());
}
</script>