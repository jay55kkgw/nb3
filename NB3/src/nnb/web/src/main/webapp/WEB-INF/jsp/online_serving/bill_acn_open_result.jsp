<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
   <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">    
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
</head>
 <body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i
					class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><spring:message code= "LB.X0262" /></a></li>
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0514" /></li>
		</ol>
	</nav>

	
	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
	<main class="col-12"> 
	<!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<h2><spring:message code="LB.D0514" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>					
			<form method="post" id="formId">
			<c:set var="dataSet" value="${bill_acn_open_result.data.dataSet}" />	
				<div class="main-content-block row">
					<div class="col-12 tab-content">
					<div class="ttb-message">
						<span><spring:message code="LB.D0542" /></span>	
						</div>	
						<div class="ttb-input-block">							
							<!-- 申請人 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
								<h4><spring:message code="LB.D0516" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${dataSet.CUSNAME}</span>										
									</div>
								</span>
							</div>
							
							<!-- 地址 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
								<h4><spring:message code="LB.D0517" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${dataSet.ADDRESS}</span>
									</div>
								</span>
							</div>								
							
							<!-- 營業種類或職(事)業 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0518" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${dataSet.BUSINESS}</span>
									</div>
								</span>
							</div>	
							 
							<!-- 身分證統一編號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0519" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${bill_acn_open_result.data.CUSIDN}</span>
									</div>
								</span>
							</div>
							 
							<div class="ttb-input-item row">
								<!--票信查詢費指定扣帳帳號  -->
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.D0520" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">										
										<span>${bill_acn_open_result.data.ACN}</span>
									</div>
								</span>
							</div>
							
							<!-- 生日 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0521" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${bill_acn_open_result.data.BIRHDAY}</span>
									</div>
								</span>
							</div>
							
							<!-- 國別 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0522" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
									<c:choose>
										<c:when test="${transfer == 'en'}">
											<span>${bill_acn_open_result.data.COUNTRYNAMEEN}</span>
										</c:when>
										<c:when test="${transfer == 'zh'}">
											<span>${bill_acn_open_result.data.COUNTRYNAMECN}(${dataSet.COUNTRY})</span>
										</c:when>
										<c:otherwise>
											<span>${bill_acn_open_result.data.COUNTRYNAME}(${dataSet.COUNTRY})</span>
										</c:otherwise>
									</c:choose>
									</div>
								</span>
							</div>
							
							<!-- 出生地 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0523" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${dataSet.BIRTHPLA}</span>
									</div>
								</span>
							</div>
							
							<!-- 電話號碼(公) -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0524" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${dataSet.TEL_O}</span>									
									</div>
								</span>
							</div>
							
							<!-- 電話號碼(宅) -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0525" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${dataSet.TEL_H}</span>
									</div>
								</span>
							</div>
							
							<!-- 票信查詢費指定扣帳帳號之分行名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.X1162" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${bill_acn_open_result.data.BRHNAME}</span>
									</div>
								</span>
							</div>
							
							<!-- 分行地址 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.X0364" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${bill_acn_open_result.data.BRHADDR}</span>
									</div>
								</span>
							</div>
							
							<!-- 分行連絡電話 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.X0365" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span>${bill_acn_open_result.data.BRHTEL}</span>
									</div>
								</span>
							</div>
							
						</div>
						<input type="button" class="ttb-button btn-flat-orange" id = "printbtn" value="<spring:message code="LB.Print" />"/>	
					</div>
				</div>

				</form>

<!-- 				</form> -->
			</section>
		<!-- 		main-content END -->
		</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
<!--   Js function -->
    <script type="text/JavaScript">
		$(document).ready(function() {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 50);
			// 開始跑下拉選單並完成畫面
			setTimeout("init()", 400);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
			
		});
		
		function init(){
			$("#printbtn").click(function(){
				var params = {
						"jspTemplateName":"bill_acn_open_result_print",
						"jspTitle":"<spring:message code= "LB.D0514" />",
						"NAME":"${dataSet.CUSNAME}",
						"ADDRESS":"${dataSet.ADDRESS}",
						"BUSINESS":"${dataSet.BUSINESS}",
						"CUSIDN":"${bill_acn_open_result.data.CUSIDN}",
						"ACN":"${bill_acn_open_result.data.ACN}",
						"BRHDAY":"${bill_acn_open_result.data.BIRHDAY}",
						"COUNTRY":"${dataSet.COUNTRY}",
						"COUNTRYNAME":"${bill_acn_open_result.data.COUNTRYNAME}",
						"COUNTRYNAMEEN":"${bill_acn_open_result.data.COUNTRYNAMEEN}",
						"COUNTRYNAMECN":"${bill_acn_open_result.data.COUNTRYNAMECN}",
						"BIRTHPLA":"${dataSet.BIRTHPLA}",
						"TEL_O":"${dataSet.TEL_O}",
						"TEL_H":"${dataSet.TEL_H}",
						"BRHNAME":"${bill_acn_open_result.data.BRHNAME}",
						"BRHADDR":"${bill_acn_open_result.data.BRHADDR}",
						"BRHTEL":"${bill_acn_open_result.data.BRHTEL}"
					};
					openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
		}

		
 	</script>
</body>
</html>
