<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()",10);
		// 開始查詢資料並完成畫面
		setTimeout("init()",20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)",500);
	});
	function init(){			
		initFootable();
		$("#formId").validationEngine({
			binded: false,
			promptPosition: "inline"
		});
	}
</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 網路銀行交易密碼線上解鎖     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0263" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code="LB.D0263"/></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="">
				<div class="main-content-block row">
					<div class="col-12">
						<div class="ttb-message">
							<span><spring:message code="LB.D0268"/></span>
						</div>
						<div class="ttb-input-block">
							<!-- 身份證字號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
										<h4>
											<spring:message code="LB.D0204"/>
										</h4>
								</label>
								</span> 
								<span class="input-block">
                                    <div class="ttb-input">
										<span>${unlock_ssl_pw_r.data.CUSIDN}</span>
									</div>
								</span>
							</div>
						</div>
					</div>
				</div>
				<div class="text-left">
					<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /></p>					
						<li style="font-weight:bold; color:#FF0000;"><spring:message code="LB.Unlock_Ssl_Pw_P1" /></li>
						<li><spring:message code="LB.Unlock_Ssl_Pw_P2_D2" /></li>
                		<li><spring:message code="LB.Unlock_Ssl_Pw_P2_D3" /></li>
					</ol>
 				</div>
			</form>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>