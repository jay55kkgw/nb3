<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
  <title>${jspTitle}</title>
  <script type="text/javascript">
    $(document).ready(function () {
      window.print();
    });
  </script>
</head>
<body class="watermark" style="-webkit-print-color-adjust: exact">
	<br /><br />
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif" /></div>
	<br /><br />
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<br />
	
	<div> 
		<!-- 查詢時間 -->
		<p>
			<spring:message code="LB.Inquiry_time" /> : ${CMQTIME}
		</p>
		<!-- 查詢帳號 : -->
        <p>
			<spring:message code= "LB.L/C_no" /> :
        	${LCNO}
		</p>
		<!-- 查詢帳號 : -->
        <p>
			<spring:message code= "LB.W0094" /> :
        	${RORIGAMT}
		</p>
		<!-- 查詢帳號 : -->
        <p>
			<spring:message code= "LB.W0174" />:
        	${RBENNAME}
		</p>
		<!-- 資料總數 : -->
		<p>
			<spring:message code="LB.Total_records" /> : ${CMRECNUM}
			<!--筆 -->
			<spring:message code="LB.Rows" />
		</p>
		<!-- 匯入金額總金額 : -->
		
		
		<!-- 資料Row -->
		
		<table class="print">
			<tr>
				<!--到單日期-->
                <th>
                    <!--<spring:message code="LB.Change_date" />-->
					<spring:message code= "LB.W0166" />
                </th>
                <!--到單號碼-->
                <th>
                    <!--<spring:message code="LB.Change_date" />-->
					<spring:message code= "LB.X0007" />
                </th>
                <!-- 幣別 -->
                <th>
                    <spring:message code="LB.Currency" />
                </th>
                <!--到單金額-->
                <th>
                    <!-- <spring:message code="LB.Deposit_amount" /> -->
					<spring:message code= "LB.W0169" />
                </th>
                <!-- 融資利率 -->
                <th>
                    <!-- <spring:message code="LB.Account_balance_2" /> -->
					<spring:message code= "LB.X0010" />
                </th>
                <!-- 融資起日 -->
                <th>
                    <!-- <spring:message code="LB.Account_balance_2" /> -->
					<spring:message code= "LB.W0170" />
                </th>
                <!-- 融資迄日 -->
                <th>
                    <!-- <spring:message code="LB.Account_balance_2" /> -->
					<spring:message code= "LB.W0171" />
                </th>
                <!-- 承諾到期日 -->
                <th>
                    <!--<spring:message code="LB.Data_content" />-->
					<spring:message code= "LB.X0009" />
                </th>
                <!-- 備註 -->
                <th>
                    <spring:message code="LB.Note" />
                </th>
			</tr>
			<c:forEach var="dataList" items="${print_datalistmap_data}">
				<tr>
	                <!-- 到單日期-->
	                <td style="text-align:center">${dataList.RIBDATE }</td>
	                <!--到單號碼-->
	                <td style="text-align:center">${dataList.RIBNO }</td>
	           		<!-- 幣別 -->
	                <td style="text-align:center">${dataList.RBILLCCY }</td>
	            	<!-- 到單金額 -->
	                <td style="text-align:right">${dataList.RBILLAMT }</td>
	             	<!--融資利率  -->
	                <td style="text-align:right">${dataList.RADVFIXR }</td>
	                <!-- 融資起日 -->
	                <td style="text-align:center">${dataList.RINTSTDT }</td>
	            	<!-- 融資迄日 -->
	                <td style="text-align:center">${dataList.RINTDUDT }</td>
	            	<!-- 承諾到期日 -->
	                <td style="text-align:center">${dataList.RCFMDATE }</td>
	            	<!-- 備註 -->
	                <td style="text-align:center">${dataList.RMARK }</td>
	            	
	            </tr>
			</c:forEach>
		</table>
		
	</div>
	
	<br>
	<br>
</body>
</html>