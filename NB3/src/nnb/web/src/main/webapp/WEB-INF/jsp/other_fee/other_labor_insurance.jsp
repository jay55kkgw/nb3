<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>

<script type="text/javascript">
$(document).ready(function () {
    initFootable(); // 將.table變更為footable 
    init();
});

function init() {
	$("#TSFACN").children().each(function(){
		if ($(this).val()=='${backenData.TSFACN}'){
			$(this).prop("selected", true);
		}
	});
	$("#formId").validationEngine({binded: false,promptPosition: "inline"});
	$("#pageshow").click(function(e){
		$("#UNTNUM").val($("#UNTNUM1").val()+"0"+$("#UNTNUM2").val());
		$("#UNTTEL").val($("#UNTTEL1").val()+$("#UNTTEL2").val());
		if (!$('#formId').validationEngine('validate')) {
			e.preventDefault();
		} else {
			$("#formId").validationEngine('detach');
        	initBlockUI();
        	var action = '${__ctx}/OTHER/FEE/labor_terms';
			$("form").attr("action", action);
			$("form").submit();
		}
	});
	$("#CMRESET").click(function(e){
		$("#formId")[0].reset();
		getTmr();
	});
	if(${ result_data.data.REC.size() } <= 0)
	{
		//未申請轉出帳號，請臨櫃辦理
		//alert("<spring:message code= "LB.Alert168" />");
		errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert168' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
		$("#pageshow").attr("disabled",true);
		$("#pageshow").removeClass('btn-flat-orange');
	}
	exampleClick();
}

function ValidateValue(textbox) {
	var IllegalString = "+-_＿︿%@[`~!#$^&*()=|{}':;',\\[\\].<>/?~！#￥……&*（）——|{}【】‘；：”“'。，、？]‘'";
	var textboxvalue = textbox.value;
	var index = textboxvalue.length - 1;
	var s = textbox.value.charAt(index);
	if (IllegalString.indexOf(s) >= 0) {
		s = textboxvalue.substring(0, index);
		textbox.value = s;
	}
}

function exampleClick(){
	//範例
	$('#EXAMPLE').click(function() {
		window.open('${__ctx}/img/Labor.jpg');
	});
}
</script>
</head>
<body>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %> 
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 自動扣繳服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2360" /></li>
    <!-- 勞保費用代扣繳申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0718" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.W0718" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form id="formId" method="post" action="">
					<input type="hidden" name="action" value="forward">
				    <input type="hidden" name="FLAG" value="1">
				    <input type="hidden" name="TYPE" value="02">
				    <input type="hidden" name="ITMNUM" value="1">
				    <input type="hidden" name="ADOPID" value="N8301">
		            <input type="hidden" name="HLHBRH" value="1"/>
				    <input type="hidden" name="UNTTEL" id="UNTTEL" >
				    <input type="hidden" name="UNTNUM" id="UNTNUM" >
	                <div class="main-content-block row">
	                    <div class="col-12 tab-content">
	                        <div class="ttb-input-block">
	                            <div class="ttb-message">
	                                <span><spring:message code="LB.W0719" /></span>
	                            </div>
	                            
	                             <!--扣款帳號-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title"><label>
	                                        <h4><spring:message code="LB.D0168" /></h4>
	                                    </label></span>
	                                <span class="input-block">
	                                    <div class="ttb-input">
	                                        <select class="custom-select select-input half-input validate[required]" name="TSFACN" id="TSFACN" >
	                                            <option value="">----------<spring:message code="LB.Select" />----------</option>
												<c:forEach var="dataList" items="${result_data.data.REC}">
													<option> ${dataList.ACN}</option>
												</c:forEach>
	                                        </select>
	                                    </div>
	                                </span>
	                            </div>
	
	                                <!--保險證字號 -->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                    <label>
	                                        <h4><spring:message code="LB.W0722" /></h4>
	                                    </label>
	                                </span>
	                                <span class="input-block">
	                                    <div class="ttb-input">
				                            <input class="text-input validate[required,funcCall[validate_chkChrNum['<spring:message code= "LB.W0722" />',UNTNUM1]]]" value="${backenData.UNTNUM1}" maxLength="8" type="text" id="UNTNUM1" name="UNTNUM1">
											<span class="ttb-unit">0</span>
	                                      	<input class="card-input text-input validate[required,funcCall[validate_chkChrNum['<spring:message code= "LB.X1438" />',UNTNUM2]]]" value="${backenData.UNTNUM2}" maxLength="1" type="text" id="UNTNUM2"  name="UNTNUM2">
											<span class="ttb-unit"><spring:message code="LB.W0723" /></span>
											<button type="button" class="btn-flat-orange" name="EXAMPLE" id="EXAMPLE">
												<spring:message code="LB.W0438" />											
											</button>
											<span id="hideblock_UNTNUM" >
												<input id="validate_UNTNUM" name="validate_UNTNUM" type="text" value="#" class="text-input 
													validate[funcCallRequired[validate_CheckLenEqual['<spring:message code= "LB.W0722" />',UNTNUM,false,10]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
	                                    </div>
	                                </span>
	                            </div>    
	                                
								<!--投保單位營利事業/負責人 身分證統一編號 -->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                    <label>
	                                        <h4><spring:message code="LB.W0725" /><br><spring:message code="LB.D0519" /></h4>
	                                    </label>
	                                </span>
	                                <span class="input-block">
	                                    <div class="ttb-input">
				                           	<input type="text" class="text-input validate[required,funcCall[validate_checkSYS_IDNO[CUSIDNUN]]]" value="${backenData.CUSIDNUN}" name="CUSIDNUN" id="CUSIDNUN"  maxLength="10" size="10">
											<br>
											<span class="ttb-unit"><spring:message code="LB.W0727" /></span>
	                                    </div>
	                                </span>
	                            </div>
								
								<!--投保單位電話區域碼 -->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                    <label>
	                                        <h4><spring:message code="LB.W0728" /></h4>
	                                    </label>
	                                </span>
	                                <span class="input-block">
	                                    <div class="ttb-input">
				                           	<input maxLength="2" type="text" class="card-input text-input validate[required]" value="${backenData.UNTTEL1}" name="UNTTEL1" id="UNTTEL1">
	                                    </div>
	                                </span>
	                            </div>
								
								<!--投保單位電話號碼 -->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                    <label>
	                                        <h4><spring:message code="LB.W0729" /></h4>
	                                    </label>
	                                </span>
	                                <span class="input-block">
	                                    <div class="ttb-input">
				                           	<input maxLength="8" type="text" class="text-input validate[required]" value="${backenData.UNTTEL2}" name="UNTTEL2" id="UNTTEL2">
											
											<span id="hideblock_TEL" >
												<input id="validate_TEL" name="validate_TEL" type="text" value="#" class="text-input 
													validate[funcCallRequired[validate_chkTelFull[UNTTEL1,UNTTEL2]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
	                                    </div>
	                                </span>
	                            </div>
								
	                        </div>
	                        <!--button 區域 -->
	                            <input id="pageshow" name="pageshow" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.X0080" />" />
                        	<!-- button 區域 -->
	                    </div>
	                </div>
				</form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>