<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript"
	src="${__ctx}/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css"
	href="${__ctx}/css/validationEngine.jquery.css">
<link rel="stylesheet" type="text/css"
	href="${__ctx}/css/jquery.datetimepicker.css">

<script type="text/javascript">
var urihost = "${__ctx}";
var cardReadersName;
var OKReaderName = "";
var pazzword = "";
$(document).ready(function() {
	init();
});
function init() {
	// 確認鍵 click
	goOn();
	// 上一頁按鈕 click
	goBack();
	
}
// 確認鍵 Click
function goOn() {
	$("#CMSUBMIT").click( function(e) {
		// 送出進表單驗證前將span顯示
		$("#hideblock").show();
		
		console.log("submit~~");

		// 表單驗證
		if ( !$('#formId').validationEngine('validate') ) {
			e.preventDefault();
		} else {
			// 解除表單驗證
			$("#formId").validationEngine('detach');
			
			// 通過表單驗證
			processQuery();
		}
	});
}
// 上一頁按鈕 click
function goBack() {
	// 上一頁按鈕
	$("#pageback").click(function() {
		// 遮罩
		initBlockUI();
		
		// 解除表單驗證
		$("#formId").validationEngine('detach');
		
		// 讓Controller知道是回上一頁
		$('#back').val("Y");
		// 回上一頁
		var action = '${pageContext.request.contextPath}' + '${previous}';
		$("#formId").attr("action", action);
		$("#formId").submit();
	});
}
// 通過表單驗證準備送出
function processQuery(){
			// 晶片金融卡
			useCardReader(); //開發用繞過去			
// 			$("#formId").submit();
}

//交易機制使用讀卡機
function useCardReader(){
	console.log("CardReader...");
	// 開始讀卡機驗證流程
	listReaders();		
}
//取得讀卡機
function listReaders(){
	if(window.console){console.log("listReaders...");}
	var CardReInsert = true;
	var GoQueryPass = true;
	ConnectCard("listReadersFinish");
}
//取得讀卡機結束
function listReadersFinish(result){
	if(window.console){console.log("listReadersFinish...");}
	//成功
	if(result != "false" && result != "E_Send_11_OnError_1006"){
		cardReadersName = result;
		//找出有插卡的讀卡機
		findOKReader();
	}
}
//找出有插卡的讀卡機
function findOKReader(){
	if(window.console){console.log("findOKReader...");}
	FindOKReader("findOKReaderFinish");
}

var OKReaderName = "";

//找出有插卡的讀卡機結束(有找到才會到此FUNCTION)
function findOKReaderFinish(okReaderName){
	if(window.console){console.log("findOKReaderFinish...");}
	//ASSIGN到全域變數
	OKReaderName = okReaderName;
	getCardBankID();
}

//取得卡片銀行代碼
function getCardBankID(){
	if(window.console){console.log("getCardBankID...");}
	GetUnitCode(OKReaderName,"getCardBankIDFinish");
}
//取得卡片銀行代碼結束
function getCardBankIDFinish(result){
	if(window.console){console.log("getCardBankIDFinish...");}
	if(window.console){console.log("result: " + result);}
	
	//成功
	if(result != "false"){
		//還要另外判斷是否為本行卡
		//是
		if(result == "05000000"){
			var formId = document.getElementById("formId");
			formId.ISSUER.value = result;
			
			// 驗證卡片密碼
			showDialog("verifyPin",OKReaderName,"verifyPinFinish");
		}
		//不是
		else{
			//alert(GetErrorMessage("E005"));
			errorBlock(
					null, 
					null,
					[GetErrorMessage("E005")], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
		}
	}
}

var pazzword = "";

//驗證卡片密碼
function verifyPin(readerName,password,outerCallBackFunction){
	//ASSIGN到全域變數
	pazzword = password;
	
	VerifyPin(readerName,password,outerCallBackFunction);
}
//驗證卡片密碼結束
function verifyPinFinish(result){
	//成功
	if(result == "true"){
		// 繼續做
		SubmitForm();
	}
}

// 送交前押碼
function SubmitForm(){
	if(window.console){console.log("SubmitForm...");}
	FinalSendout("MaskArea",true);
	getMainAccount();
}
//取得卡片主帳號
function getMainAccount(){
	if(window.console){console.log("getMainAccount...");}
	GetMainAccount(OKReaderName,"getMainAccountFinish");
}
//取得卡片主帳號結束
function getMainAccountFinish(result){
	if(window.console){console.log("getMainAccountFinish...");}
	//成功
	if(result != "false"){
		
		var formId = document.getElementById("formId");
		formId.ACNNO.value = result;
		formId.ACN.value = result;
		var cardACN = result;
		if(cardACN.length > 11){
			cardACN = cardACN.substr(cardACN.length - 11);
		}
		//這隻獨有的部分僅查驗回傳之CUSIDN的長度
		var uri = urihost+"/ONLINE/APPLY/logout_without_card_AcnCheck";
		console.log("cardACN...>>"+cardACN);
		var rdata = { ACN: cardACN };
		fstop.getServerDataEx(uri, rdata, true, CheckIdResult);
	}
	//失敗
	else{
		FinalSendout("MaskArea",false);
	}
}
function CheckIdResult(data) {
	console.log("data: " + data);
	if (data) {
		// login_aj回傳資料
		console.log("data.json: " + JSON.stringify(data));
		// 成功
		if(data.msgCode.length==10) {
			$("#CUSIDN").val(data.msgCode);
			//拔插卡
			removeThenInsertCard();
		} else {
			showTempMessage(500,"<spring:message code="LB.X1250"/>","","MaskArea",false);
			//alert("<spring:message code="LB.Alert143"/>");
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert143' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
		}
	}
}
//拔插卡
function removeThenInsertCard(){
	console.log("removeThenInsertCard.OKReaderName: " + OKReaderName);
	RemoveThenInsertCard("MaskArea", 60, OKReaderName, "removeThenInsertCardFinish");
}
//拔插卡結束(成功才會到此FUNCTION)
function removeThenInsertCardFinish(){
	//拔插後繼續
	preGenerateTAC();
}

// 準備押碼所需資料
function preGenerateTAC(){
	if(window.console){console.log("preGenerateTAC...");}
	
	var TRMID = MakeTRMID();
	
	var formId = document.getElementById("formId");
	formId.TRMID.value = TRMID;
	
	var transData = "2500" + TRMID + formId.ACNNO.value;
	
	// 小鍵盤輸入密碼
	generateTAC(cardReadersName, pazzword, transData);
	
}

//卡片押碼
function generateTAC(OKReaderName, pazzword, transData){
	if(window.console){console.log("generateTAC...");}
	GenerateTAC(OKReaderName, transData, pazzword, "generateTACFinish");
}

//卡片押碼結束
function generateTACFinish(result){
	if(window.console){console.log("generateTACFinish...");}
	//成功
	if(result != "false"){
		// e.x. E000,00000551,6BF84A4B9319B145A64F3866506D3313594B10D08CDEA863BFA8F9D6
		var TACData = result.split(",");
		
		var formId = document.getElementById("formId");
		formId.iSeqNo.value = TACData[1];
		formId.ICSEQ.value = TACData[1];
		formId.TAC.value = TACData[2];
		
		var ACN_Str1 = formId.ACNNO.value;
		if(ACN_Str1.length > 11){
			ACN_Str1 = ACN_Str1.substr(ACN_Str1.length - 11);
		}
		formId.CHIP_ACN.value = ACN_Str1;
		formId.OUTACN.value = ACN_Str1;
		formId.ACNNO.value = ACN_Str1;
		$("#formId").attr("action","${__ctx}/ONLINE/APPLY/logout_taiwan_pay_step1");
		formId.submit();
	}
	//失敗
	else{
		FinalSendout("MaskArea",false);
	}
}

// 從讀卡機名稱判斷是幾代機
function getReaderType(readerName,readerNameElementID,readerTypeElementID){
	if(window.console){console.log("getReaderType...");}
    var readerType = "1";
    
	if(readerName.indexOf("CASTLES EZpad") >= 0){
        readerType = "2";
    }
	else if(readerName.indexOf("Todos eCode Connectable") >= 0){
        readerType = "2";
    }
	else if(readerName.indexOf("Todos eCode Connectable II") >= 0){
        readerType = "2";
    }
	else if(readerName.indexOf("ACS ACR83U") >= 0){
        readerType = "2";
    }
	else if(readerName.indexOf("ACS APG8201") >= 0){
        readerType = "2";
    }
	if(readerNameElementID != ""){
		document.getElementById(readerNameElementID).value = readerName;
	}
	if(readerTypeElementID != ""){
		document.getElementById(readerTypeElementID).value = readerType;
	}
	
	if(window.console){console.log("readerType: " + readerType);}
    return readerType;
}
//
</script>
</head>

<body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
<!-- 	 header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Register" /></li>
    <!-- 掃碼提款     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X1949" /></li>
    <!-- 線上註銷臺灣Pay掃碼提款功能     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1589" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code="LB.D1589"/></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="${__ctx}/ONLINE/APPLY/logout_taiwan_pay_step1">
				<input type="hidden" id="jsondc" name="jsondc" value='${financial_card_renew_step1.data.REC.jsondc}'>
			<input type="hidden" id="ISSUER" name="ISSUER" value="">
			<input type="hidden" id="ACNNO" name="ACNNO" value="">
			<input type="hidden" id="TRMID" name="TRMID" value="">
			<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
			<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
			<input type="hidden" id="TAC" name="TAC" value="">
			<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">

				<input type="hidden" name="ACN" id="ACN" value="">
				<input type="hidden" name="ADOPID" value="N580"/>
				<input type="hidden" name="CHIP_ACN" value="">
				<input type="hidden" name="CUSIDN" id="CUSIDN" value="">
				<input type="hidden" name="UID" value="">
				<input type="hidden" name="OUTACN" value="">
				<input type="hidden" name="FGTXWAY" id="CMCARD" value="2"/>
					
			<!-- ---------------------------------------------------------------------------- -->
              
                <div id="step-bar">
                    <ul>
                        <li class="active"><spring:message code="LB.D1204"/></li>
                        <li class=""><spring:message code="LB.D1585"/></li>
                        <li class=""><spring:message code="LB.X2060"/></li>
                    </ul>
                </div>
		  <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
                            <div class="ttb-message">
                                <p><spring:message code="LB.D1204"/></p>
                            </div>
                            <p class="form-description"><spring:message code="LB.X2061"/></p>

                            
                            <!-- 驗證機制 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D0178"/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">

                                        <label class="radio-block" for="ATMTRAN1"><spring:message code="LB.D0234"/>
                                            <input type="radio" name="ATMTRAN1" id="ATMTRAN1" value="0" checked>
                                            <span class="ttb-radio"></span>
                                        </label>
                                    </div>
                                </span>
                            </div>
                          
                        </div>
                        
                        <input type="button" class="ttb-button btn-flat-orange"name="CMSUBMIT" id="CMSUBMIT" value="<spring:message code="LB.Confirm"/>" />

                    </div>
                </div>
                <ol class="list-decimal description-list">
                </ol>
				<!-- ---------------------------------------------------------------------------- -->

			</form>
		</section>
		</main>
	<!-- 		main-content END -->
	<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>

</html>