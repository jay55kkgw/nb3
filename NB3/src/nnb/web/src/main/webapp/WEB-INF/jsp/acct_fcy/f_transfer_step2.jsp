<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
	
	<style>
		.noZhCn{
			-ms-ime-mode: disabled;
		}
	</style>
	<script type="text/JavaScript">
		$(document).ready(function() {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 100);
		});
	</script>
</head>
<body>
	<!-- 交易機制元件所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	
    <!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 買賣外幣/約定轉帳     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.FX_Exchange_Transfer" /></li>
		</ol>
	</nav>



	
	<!-- 功能清單及登入資訊 -->
	<div class="content row">
	
		<!-- 功能清單 -->
		<%@ include file="../index/menu.jsp"%>

		<!-- 快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
				<!-- 功能內容 -->
				<form method="post" id="formId" action="${__ctx}/FCY/TRANSFER/f_transfer_result">
					<!-- 功能名稱 -->
					<h2><spring:message code="LB.FX_Exchange_Transfer" /></h2>
					<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
					
					<!-- 交易流程階段 -->
<!-- 					<div id="step-bar"> -->
<!-- 						<ul> -->
<%-- 							<li class="active"><spring:message code="LB.Enter_data" /></li> --%>
<%-- 							<li class=""><spring:message code="LB.Confirm_data" /></li> --%>
<%-- 							<li class=""><spring:message code="LB.Transaction_complete" /></li> --%>
<!-- 						</ul> -->
<!-- 					</div> -->
					
					<!-- 功能內容 -->
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
							
						<c:choose>
							<c:when test = "${str_FGTRDATE} eq '0' ">
								<div class="ttb-message">
									<span><spring:message code="LB.Confirm_transfer_data" /></span>
								</div>
							</c:when>
							<c:otherwise>
								<div class="ttb-message">
									<span><spring:message code="LB.Confirm_the_Scheduled_Transaction_data" /></span>
								</div>
							</c:otherwise>
						</c:choose>
							
								<!-- 轉帳日期 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Transfer_date" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${transfer_data.data.PAYDATE }
										</div>
									</span>
								</div>
	
								<!-- 轉出帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Payers_account_no" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${transfer_data.data.CUSTACC }
										</div>
									</span>
								</div>
	
								<!-- 轉帳金額 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Amount" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${transfer_data.data.str_Curr }
											${transfer_data.data.str_OutAmt }
											&nbsp;<spring:message code="LB.Dollar" />
										</div>
									</span>
								</div>
	
								<!-- 轉入帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Payees_account_no" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											${transfer_data.data.BENACC }
										</div>
									</span>
								</div>
	
								<!-- 轉入帳號確認 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Confirm_payers_account_no" /></h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<img
												src="${__ctx}/getBHO?transferInAccount=${transferInAccount}&functionName=transfer" />
											<br />
											<br />
										</div>
										<div class="BHOInput">
											<input type="text" id="transInAccountText1" maxlength="1" class="text-input noZhCn" /> 
											- 
											<input type="text" id="transInAccountText2" maxlength="1" class="text-input noZhCn" />
											- 
											<input type="text" id="transInAccountText3" maxlength="1" class="text-input noZhCn" /> 
											<span class="input-unit">
											<spring:message	code="LB.Anti-blocking_BHO_attack" />
											</span>
										</div>
										<!-- 不在畫面上顯示的span -->
										<span id="hideblock">
											<input id="CARDNUM_TOTAL" name="CARDNUM_TOTAL" type="text"
											class="text-input validate[required,minSize[3],custom[onlyNumberSp]]"
											style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;" />
										</span>
									</span>
								</div>
	
								<!-- 交易機制 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Transaction_security_mechanism" /></h4>
										</label>
									</span>
									<span class="input-block">
								
							<!-- 非約定 -->
							<c:choose>
								<c:when test="${hiddenCMSSL}">
									
										<!-- 交易密碼SSL -->
										<div class="ttb-input">
											<label class="radio-block"> 
												<spring:message	code="LB.SSL_password" /> 
												<input type="radio" name="FGTXWAY" checked="checked" value="0"> 
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<input id="CMPASSWORD" name="CMPASSWORD" type="password" size="8" maxlength="8"
												class="text-input"
												placeholder="<spring:message code="LB.Please_enter_password"/>">
										</div>
										
									<c:if test = "${sessionScope.isikeyuser}">
									
										<!-- 電子簽章(請載入載具i-key) -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Electronic_signature" />
												<input type="radio" name="FGTXWAY" id="CMIKEY" value="1">
												<span class="ttb-radio"></span>
											</label>
										</div>
										
									</c:if>	
										
								</c:when>
								<c:otherwise>
									
										<!-- 電子簽章(請載入載具i-key) -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Electronic_signature" />
												<input type="radio" name="FGTXWAY" id="CMIKEY" value="1">
												<span class="ttb-radio"></span>
											</label>
										</div>
								</c:otherwise>
							</c:choose>
								
									</span>
								</div>
							</div>
							
					<c:choose>
						<c:when test="${str_FGTRDATE} eq '0' ">
						
							<input class="ttb-button btn-flat-gray" name="CMBACK" type="button" value="<spring:message code="LB.Back_to_previous_page" />" />
							<!-- 取得匯率及議價編號 -->
							<input class="ttb-button btn-flat-orange" name="CMSUBMIT" type="button" value="<spring:message code="LB.Obtain_exchange_rate_and_bargaining_number" />" />
							
						</c:when>
						<c:otherwise>
						
							<input class="ttb-button btn-flat-gray" name="CMBACK" type="button" value="<spring:message code="LB.Back_to_previous_page" />" onclick="history.go(-1)" />
							<input class="ttb-button btn-flat-orange" name="CMSUBMIT" type="button" value="<spring:message code="LB.Confirm" />" />
							
						</c:otherwise>
					</c:choose>
					
						</div>
					</div>
					
				<c:if test="${str_FGTRDATE} eq '0' ">
					<ol class="list-decimal">
						<li><font color="#FF0000"><spring:message code="LB.Confirm_exchange_rate" /></font></li>
					</ol>
				</c:if>
					
				</form>
			</section>
		</main>
	</div>

	<%@ include file="../index/footer.jsp"%>

</body>
</html>
