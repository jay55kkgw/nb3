<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<script type="text/javascript">

	$(document).ready(function () {
	
		$("#CMSUBMIT").click(function(e) {
			console.log("submit~~");
			
			if('${sessionScope.dpmyemail}'==""){
				//alert('<spring:message code= "LB.Alert072" />');
				errorBlock(
						null, 
						null,
						['<spring:message code= 'LB.Alert072' />'], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
				$('#CMSUBMIT2').focus();
				return false;
			}else{
			// 遮罩
				initBlockUI();
				$("#formId").attr("action", "${__ctx}/ELECTRONIC/CHECKSHEET/apply_confirm");
				$("#formId").submit();
			}
		});
		//上一頁按鈕
		$("#CMBACK").click(
				function() {
					initBlockUI();
					fstop.getPage('${pageContext.request.contextPath}'
							+ '/ELECTRONIC/CHECKSHEET/apply_bill', '', '');
				});
		$("#CMSUBMIT2").click(function(e) {
			console.log("toMail~~");
			// 遮罩
			initBlockUI();
			$('#mail').val("DPSETUPE");
			$("#formId").attr("action", "${__ctx}/PERSONAL/SERVING/mail_setting_choose");
			$("#formId").submit();
		});
	});
</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 電子對帳單     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1396" /></li>
    <!-- 電子對帳單申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0270" /></li>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>

		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> <!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2>
				<spring:message code="LB.D0279" /><!-- 申請電子帳單 -->
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="">
			<input type="hidden" name="type" value="" id="mail"/>
				<div class="main-content-block row">
					<div class="col-12">
					<div style="margin-top: 30px;">
					<div style="display: inline; margin-top:30px">
					<span><b><spring:message code="LB.D0292" /><!-- 電子帳單傳送位址 -->(<spring:message code="LB.D0293" /><!-- 我的Email -->)：&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;
								<c:if test="${sessionScope.dpmyemail != ''}"> 
 									${sessionScope.dpmyemail}</b></span>
								</c:if> 
 								<c:if test="${sessionScope.dpmyemail =='' }">
 								</span> 
								</c:if> 
					</div>
					<div style="display: inline; margin-top:30px">
						<c:if test="${sessionScope.dpmyemail != ''}"> 
							&nbsp;&nbsp;&nbsp;&nbsp;<input class="ttb-button btn-flat-orange" style="bottom:0px;" type="button" id="CMSUBMIT2" value="<spring:message code= "LB.X0888" />" onclick="changeMail()"/>
						</c:if>
 						<c:if test="${sessionScope.dpmyemail =='' }"> 
 							&nbsp;&nbsp;&nbsp;&nbsp;<input class="ttb-button btn-flat-orange" style="bottom:0px;" type="button"  id="CMSUBMIT2" value="<spring:message code= "LB.X0889" />" onclick="changeMail()"/>  
						</c:if> 
					</div>
					</div>
					<div style="width: 75%; margin: auto;">
						<div class="ttb-message">
							<h4>
								<b><spring:message code= "LB.X1214" /></b>
							</h4>
						</div>
						<div class="ttb-message">
							<p align="left">
								<font color="royalblue" size="3"><b><spring:message code= "LB.X1215" /><span
										class="Cron"><spring:message code= "LB.W1554" /></span><spring:message code= "LB.X1209" /><span
										class="Cron"><spring:message code= "LB.Cancel" /></span><spring:message code= "LB.X1210" />
								</b></font>
							</P>
						</div>
						<table style="text-align: left">
							<tr>
								<td style="width: 5em; text-align: center" rowspan=2>第一條</td>
								<td colspan=2><b>適用範圍</b></td>
							</tr>
							<tr>
								<td colspan=2>本約定條款係電子帳單服務之一般性共通約定，除個別契約另有規定外，悉依本約定辦理。</td>
							</tr>
							<tr>
								<td style="width: 5em; text-align: center" rowspan=5>第二條</td>
								<td colspan=2><b>服務內容</b></td>
							</tr>
							<tr>
								<td colspan=2>
									本服務包含「電子交易對帳單」、「基金電子對帳單」及「信用卡電子帳單」三項服務。本人（以下簡稱立約人）申請臺灣中小企業銀行（以下簡稱貴行）以網際網路傳輸方式，傳送下列資料至立約人設定之電子郵箱位址。
								</td>
							</tr>
							<tr>
								<td >一、</td>
								<td >
									電子交易對帳單：貴行每月五日（含）以前將立約人上月之電子交易對帳單（即網路銀行、電話銀行、全國性繳費活期性帳戶ID＋ACCOUNT非約定繳費等電子銀行之各轉帳成功交易明細），傳送至立約人設定之電子郵箱（該月無交易時不寄），<font
									color="red">不另寄送書面對帳單</font>。
								</td>
							</tr>
							<tr>
								<td >二、</td>
								<td >
									基金電子對帳單：貴行按月將立約人之基金電子對帳單，傳送至立約人設定之電子郵箱，<font color="red">不另寄送書面對帳單</font>。
								</td>
							</tr>
							<tr>
								<td >三、</td>
								<td >
									信用卡電子帳單：貴行按月於立約人之貴行所有信用卡結帳日之後，傳送至立約人各信用卡之帳單內容至立約人設定之電子郵箱，<font
									color="red">不另寄送書面帳單</font>。
								</td>
							</tr>
							<tr>
								<td  style="width: 5em; text-align: center" rowspan=3>第三條</td>
								<td  colspan=2><b>服務啟用時間</b></td>
							</tr>
							<tr>
								<td >一、</td>
								<td >
									電子交易對帳單、基金電子對帳單：立約人申請完成後，將自最近一期帳單開始生效，惟當期對帳單已進行製作處理中，或已完成製作時，此申請將順延至下一期對帳單方能生效。
								</td>
							</tr>
							<tr>
								<td >二、</td>
								<td >信用卡電子帳單：若立約人係於當月結帳日前(不含結帳日當日)
									申請成功者，則本服務自立約人申請成功日當月之月結單結帳日起提供本服務；若立約人係於當月結帳日後(含結帳日當日)申請成功者，則本服務自客戶申請成功日之次一月結單結帳日起提供本服務。
									<br>立約人選擇信用卡電子帳單服務後，應每月查閱電子帳單內容。若立約人於當期繳款截止日起七日前仍未收到電子帳單，其應立即電洽貴行客服人員<font
									color="red">0800-01-7171或02-2357-7171按9處理</font>。
								</td>
							</tr>
							<tr>
								<td  style="width: 5em; text-align: center" rowspan=2>第四條</td>
								<td  colspan=2><b>電子郵箱位址變更設定</b></td>
							</tr>
							<tr>
								<td  colspan=2>
									倘立約人需變更其於申請本服務時所設定之電子郵箱位址，其應依據貴行所指示之程序及方式重新設定電子郵箱新位址。 <br>倘立約人之電子郵箱位址有變更而未依前項規定辦理變更者，貴行仍以立約人最後依前項程序及方式設定之電子郵箱位址為立約人應受送達之位址。
								</td>
							</tr>
							<tr>
								<td  style="width: 5em; text-align: center" rowspan=6>第五條</td>
								<td  colspan=2><b>電子帳單無法送達之處理</b></td>
							</tr>
							<tr>
								<td  colspan=2>
									如因立約人之電子郵箱或線路傳輸等因素致無法接收電子帳單，則立約人同意辦理如下：</td>
							</tr>
							<tr>
								<td >一、</td>
								<td >
									電子交易對帳單：立約人同意於該月底以前，自行於貴行網路銀行申請補發，貴行隨即再次傳送上月份電子交易對帳單；倘仍無法接收，同意以網路銀行或臨櫃查詢或補登存摺方式取得帳戶明細資料，貴行不另寄送書面對帳單。
								</td>
							</tr>
							<tr>
								<td >二、</td>
								<td >基金電子對帳單：立約人同意以網路銀行查詢或電洽貴行信託部<font
									color="red">(02)2559-7171分機5462補發</font>。
								</td>
							</tr>
							<tr>
								<td >三、</td>
								<td >
									信用卡電子帳單：立約人同意自行於貴行網路銀行申請補發，貴行隨即傳送信用卡電子帳單；或電洽貴行客服人員<font
									color="red">0800-01-7171或02-2357-7171按9處理</font>。
								</td>
							</tr>
							<tr>
								<td  colspan=2>
									立約人因電子郵箱或線路傳輸等因素致無法接收電子帳單，為保障己身權益，同意自行於貴行網站申請取消電子帳單，由貴行按月寄送書面帳單。
									<br>倘立約人未依上開方式辦理，如有任何損失，應自負其責任。
								</td>
							</tr>
							<tr>
								<td style="width: 5em; text-align: center" rowspan=2>第六條</td>
								<td  colspan=2><b>電子帳單之效力</b></td>
							</tr>
							<tr>
								<td  colspan=2>
									電子帳單之效力與書面帳單相同。因本服務所生之任何糾紛，於審判、仲裁、調解或其他法定爭議處理程序中，立約人均不得主張電子帳單不具書面要件而無效，或主張貴行未履行寄發帳單之義務。
									<br>貴行依立約人指定之電子郵箱位址傳送電子帳單時，以電子帳單進入電子郵箱所在系統時之收文時間視為送達，但因立約人本身之原因而造成傳送失敗者（包括但不限於立約人輸入錯誤之電子郵箱、立約人變更電子郵箱位址而未辦理更新、立約人取消電子郵箱位址、立約人端網路設備故障或運作不當等），則以貴行對外發送之時間視為送達。
								</td>
							</tr>
							<tr>
								<td style="width: 5em; text-align: center" rowspan=4>第七條</td>
								<td  colspan=2><b>密碼變更/重置</b></td>
							</tr>
							<tr>
								<td >一、</td>
								<td >申請人得自行登入本行網路銀行或於接收電子對帳單時辦理密碼變更作業。</td>
							</tr>
							<tr>
								<td >二、</td>
								<td >
									申請人遺忘密碼時辦理密碼重置，包括本行電子交易對帳單、基金電子對帳單及信用卡電子帳單之密碼重置。</td>
							</tr>
							<tr>
								<td >三、</td>
								<td >
									申請人辦理密碼重置/變更申請作業，本行將於交易完成後以電子郵件發送申請人密碼重置/變更確認信函，<font color=red>申請人需於接獲確認信函後點選回覆確認，密碼變更/重置始生效力，未於申請日起15日內點選回覆確認者，則申請失效</font>。
								</td>
							</tr>
							<tr>
								<td style="width: 5em; text-align: center" rowspan=2>第八條</td>
								<td  colspan=2><b>電子帳單錯誤之處理</b></td>
							</tr>
							<tr>
								<td  colspan=2>
									立約人使用本服務時，如其電子帳單因不可歸責於貴行之事由而發生錯誤時，貴行不負更正及賠償之責任。</td>
							</tr>

							<tr>
								<td style="width: 5em; text-align: center" rowspan=2>第九條</td>
								<td  colspan=2><b>修訂</b></td>
							</tr>
							<tr>
								<td  colspan=2>
									除另有約定外，貴行得隨時修訂本約定條款並以電子郵件傳送方式通知立約人，且於貴行網站公告。倘立約人不同意貴行新修訂之條款，得隨時依第九條規定取消電子帳單服務，惟立約人於貴行修訂本約定條款後仍繼續行使用電子帳單服務，即視為接受本約定條款之修改。
								</td>
							</tr>

							<tr>
								<td style="width: 5em; text-align: center" rowspan=2>第十條</td>
								<td  colspan=2><b>終止</b></td>
							</tr>
							<tr>
								<td  colspan=2>立約人得隨時以貴行網路銀行取消電子帳單服務。</td>
							</tr>
						</table>
					</div>
						<input
							class="ttb-button btn-flat-gray" type="button" name="CMBACK"
							id="CMBACK" value="<spring:message code="LB.Cancel" />" />
						<input class="ttb-button btn-flat-orange" type="button"
							name="CMSUBMIT" id="CMSUBMIT" value="<spring:message code="LB.W1554" />" /> <!-- 我同意約定條款 -->
					</div>
				</div>
			</form>
		</section>
		</main>
	</div>
	<!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>