<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>

	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp"%>

	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>

	<script type="text/javascript">
		$(document).ready(function () {
			init();
		});

		function init() {
			//表單驗證初始化
			$("#formId").validationEngine({binded: false,promptPosition: "inline"});
			//送出事件
			$("#CMSUBMIT").click(function (e) {
				console.log("submit~~");
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					processQuery();
				}
			});
		}
		// 交易機制選項
		function processQuery()
		{
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			switch(fgtxway) {
				case '0':
					$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
					$('#CMPASSWORD').val("");
		         	initBlockUI();//遮罩
		            $("#formId").submit();
					break;
				default:
					//alert("nothing...");
					errorBlock(
							null, 
							null,
							["nothing..."], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			}
		}
			
		//驗證申請張數
		function validate_applynum(field, rules, i, options)
		{
			var applynum = $('#APPLYNUM').val(); 
			 if(applynum > 500 || applynum <= 0)
			 {
				return "<spring:message code= 'LB.X2126' />";
			 }
		}
	</script>
</head>

<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 空白票據     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0528" /></li>
		</ol>
	</nav>

	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">

		<%@ include file="../index/menu.jsp"%>
		<!-- 功能選單內容 -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2>
					<spring:message code="LB.D0528" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" name="formId" method="post" action="${__ctx}/ONLINE/SERVING/blank_bill_result">
					<input type="hidden" name="TYPE" id="TYPE" value="02">
					<input type="hidden" name="ADOPID" id="ADOPID" value="N1012">
					<input type="hidden" name="NeedSHA1" id="NeedSHA1" value="">
					<input type="hidden" name="PINNEW" id="PINNEW" value="">
					<input type="hidden" name="TXTOKEN" id="TXTOKEN" value='${blank_bill.data.TXTOKEN}'>
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-input-block">
								<div class="ttb-message">
									<span>
										<!--請詳實填妥下列各欄資料，再詳閱說明，以決定是否提出申請。 -->
										<spring:message code="LB.D0486" />
									</span>
								</div>
								<!--帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.Account" />
										</label>
									</span>
									<select name="ACN" id="ACN" class="custom-select select-input half-input">
										<c:forEach var="dataList" items="${blank_bill.data.REC}">
											<option value="${dataList.ACN}">${dataList.ACN}</option>
										</c:forEach>
									</select>
								</div>
								<!-- 身分證/統一編號 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.D0531" />
										</label>
									</span>
									<span class="input-block">
										${blank_bill.data.CUSIDN}
									</span>
								</div>
								<!--  票據種類-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.D0532" />
										</label>
									</span>
									<span class="input-block">
										<!--支票 -->
										<div class="ttb-input">
											<label class="radio-block">
												<input type="radio" name="CHKTYPE" value="1" checked="checked" />
												<span class="ttb-radio"></span>
												<spring:message code="LB.D0533" />
											</label>
										<!--擔當付款本票 -->
											<label class="radio-block">
												<input type="radio" name="CHKTYPE" value="2" />
												<span class="ttb-radio"></span>
												<spring:message code="LB.D0534" />
											</label>
										</div>
									</span>
								</div>
								<!-- 票據格式 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.X0366" />
										</label>
									</span>
									<span class="input-block">
										<!--一般 -->
										<div class="ttb-input">
											<label class="radio-block">
												<input type="radio" name="CHKFORM" value="1" checked="checked" />
												<span class="ttb-radio"></span>
												<spring:message code="LB.D0535" />
											</label>
										<!--專戶支票 -->
											<label class="radio-block">
												<input type="radio" name="CHKFORM" value="2" />
												<span class="ttb-radio"></span>
												<spring:message code="LB.D0536" />
											</label>
										</div>
									</span>
								</div>
								<!-- 申請張數 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.D0537" />
										</label>
									</span>
									<span class="input-block">
										<input type="text" id="APPLYNUM" name="APPLYNUM" class="text-input validate[required,funcCall[validate_applynum[APPLYNUM]],funcCall[validate_CheckNumber['sFieldName',APPLYNUM]]]"  size="3" maxLength="3">
										<br>
										<span class="input-unit"><spring:message code="LB.X1163" /></span>
											
									</span>
								</div>
								<!-- 作廢票據總張數 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.D0538" />
										</label>
									</span>
									<span class="input-block">
										<input type="text" id="CHKCNT" name="CHKCNT" class="text-input validate[required,funcCall[validate_CheckNumber['checknumber',CHKCNT]]]" maxLength="3" size="10" value="0" >
										<span class="input-unit"></span>
									</span>
								</div>
								<!-- 聯絡電話 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.D0539" />
										</label>
									</span>
									<span class="input-block">
										<input type="text" id="TEL" name="TEL" class="text-input validate[required,funcCall[validate_CheckNumber['checknumber',TEL]]]" maxLength="15" size="20">
										<spring:message code="LB.D0095"/>
                                        <input type="text" id="TEL2" name="TEL2" class="text-input card-input validate[funcCallRequired[validate_CheckNumber['<spring:message code="LB.D0095"/>',TEL2]]]" value="" maxLength="4"/>
									</span>
								</div>
								<!-- 交易機制 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<spring:message code="LB.Transaction_security_mechanism" />
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
												<!--交易密碼SSL -->
												<spring:message code="LB.SSL_password" />
												<input type="radio" name="FGTXWAY" checked="checked" value="0">
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!--請輸入密碼 -->
										<div class="ttb-input">
											<spring:message code="LB.Please_enter_password" var="plassEntpin">
											</spring:message>
											<input type="password" id="CMPASSWORD" name="CMPASSWORD"
												class="text-input validate[required]" maxlength="8"
												placeholder="${plassEntpin}">
										</div>
									</span>
								</div>
							</div>
							<!-- Button -->
								<!-- 確定 -->
								<spring:message code="LB.Confirm" var="cmSubmit"></spring:message>
								<input type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" class="ttb-button btn-flat-orange">
						</div>
					</div>
						<!-- 		說明： -->
						
						<ol class="list-decimal description-list">
						<p><spring:message code="LB.Description_of_page" /></p>
							<li><spring:message code="LB.Blank_Bill_P1_D1" /></li>
							<li><spring:message code="LB.Blank_Bill_P1_D2" /></li>
							<li><spring:message code="LB.Blank_Bill_P1_D3" /></li>
						</ol>
					<!-- main-content-block END -->
				</form>
			</section>
		</main>
	</div>
</body>

</html>