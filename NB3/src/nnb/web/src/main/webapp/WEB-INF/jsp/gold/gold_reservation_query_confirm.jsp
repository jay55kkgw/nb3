<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
	
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>	
</head>
 <body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>

	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
	<!-- 查詢服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.use_component_P1_D1-1" /></li>
    <!-- 預約取消/查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1491" /></li>
		</ol>
	</nav>

		</ol>
	</nav> 
 	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
	<main class="col-12"> 
	<!-- 		主頁內容  -->
		<section id="main-content" class="container">
<!-- 			<h2>預約黃金交易查詢/取消</h2> -->
<!-- 			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i> -->
			<form method="post" id="formId">
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
							<h4>${result_data.data.MsgDesc}</h4>
						</div>
							<!-- 線上立即申請 -->
						<input id="CMSUBMIT" name="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code= "LB.X0954" />" />
						<!--取消 -->
                        <spring:message code="LB.Cancel" var="cmback"></spring:message>
                        <input type="button" name="CMBACK" id="CMBACK" value="${cmback}" class="ttb-button btn-flat-gray">						
					</div>
				</div>
				</form>
		</section>
		<!-- 		main-content END -->
	</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
	<!--   Js function -->
    <script type="text/JavaScript">
		$(document).ready(function() {
			init();
		});

	    function init(){
// 	    	$("#formId").validationEngine({binded:false,promptPosition: "inline" });
	    	$("#CMSUBMIT").click(function(e){			
				console.log("submit~~");
 				initBlockUI();
 				var action = "";
 				if('${result_data.data.Trancode}' == 'NA60')
    				action = '${__ctx}/GOLD/APPLY/gold_trading_apply';
   				if('${result_data.data.Trancode}' == 'NA50')
      				action = '${__ctx}/GOLD/APPLY/gold_account_apply';
    			$("#formId").attr("action", action);
    			$("#formId").submit();
	 			
			});
    		//上一頁按鈕
    		$("#CMBACK").click(function() {
    			var action = '${__ctx}/INDEX/index';
    			$('#back').val("Y");
    			$("#formId").attr("action", action);
    			$("#formId").submit();
    		});
	    }	
 	</script>
</body>
</html>
 