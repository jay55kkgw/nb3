<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	
	$("#printButton").click(function(){
		var params = {
			"jspTemplateName":"gold_resale_result_print",
<!-- 黃金回售 -->
			"jspTitle":"<spring:message code= "LB.W1512" />",
			"CMQTIME":"${bsData.CMQTIME}",
			"ACN":"${bsData.ACN}",
			"SVACN":"${bsData.SVACN}",
			"TRNGDFormat":"${bsData.TRNGDFormat}",
			"PRICEFormat":"${bsData.PRICEFormat}",
			"TRNAMTFormat":"${bsData.TRNAMTFormat}",
			"FEEAMT1Format":"${bsData.FEEAMT1Format}",
			"FEEAMT2Format":"${bsData.FEEAMT2Format}",
			"TRNAMT2Format":"${bsData.TRNAMT2Format}"
		};
		openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
	});
	$("#certButton").click(function(){
		var params = {
			"jspTemplateName":"gold_resale_result_print2",
<!-- 黃金回售 -->
			"jspTitle":"<spring:message code= "LB.W1512" />",
			"CMQTIME":"${bsData.CMQTIME}",
			"DPUSERNAME":"${DPUSERNAME}",
			"ACN":"${bsData.ACN}",
			"SVACN":"${bsData.SVACN}",
			"TRNGDFormat":"${bsData.TRNGDFormat}",
			"PRICEFormat":"${bsData.PRICEFormat}",
			"TRNAMTFormat":"${bsData.TRNAMTFormat}",
			"FEEAMT1Format":"${bsData.FEEAMT1Format}",
			"FEEAMT2Format":"${bsData.FEEAMT2Format}",
			"TRNAMT2Format":"${bsData.TRNAMT2Format}"
		};
		openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
	});
});
</script>
</head>
<body>
	<!--header-->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %> 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 黃金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2272" /></li>
    <!-- 黃金回售     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1512" /></li>
		</ol>
	</nav>

	<!--左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!--快速選單及主頁內容-->
		<main class="col-12"> 
			<!--主頁內容-->
			<section id="main-content" class="container">
<!-- 黃金回售 -->
				<h2><spring:message code= "LB.W1512" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form id="formID" method="post">
                	<input type="hidden" name="ADOPID" value="N09002"/>
	                <div class="main-content-block row">
	                    <div class="col-12 tab-content">
	                        <div class="ttb-input-block">
	                        	<div class="ttb-message">
<!-- 交易成功 -->
	                        		<span><spring:message code= "LB.D0237" /></span>
	                        	</div>
								<!--交易日期-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
										<label>
<!-- 交易日期 -->
	                                        <h4><spring:message code= "LB.D0450" /></h4>
	                                    </label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
	                                       	<span>${bsData.CMQTIME}</span>
										</div>
									</span>
								</div>
								<!--黃金轉出帳號-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                	<label>
<!-- 黃金轉出帳號 -->
	                                        <h4><spring:message code= "LB.W1515" /></h4>
	                                    </label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
	                                       	<span>${bsData.ACN}</span>
										</div>
									</span>
								</div>
								<!--台幣轉入帳號-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
		                                <label>
											<h4><spring:message code= "LB.W1518" /></h4>
										</label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
	                                       	<span>${bsData.SVACN}</span>
										</div>
									</span>
								</div>
								<!--賣出公克數-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
										<label>
<!-- 賣出公克數 -->
	                                        <h4><spring:message code= "LB.W1519" /></h4>
	                                    </label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
	                                       	<span>${bsData.TRNGDFormat}</span>
<!-- 公克 -->
											<span class="ttb-unit"><spring:message code= "LB.W1435" /></span>
										</div>
									</span>
								</div>
								<!--牌告單價-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
										<label>
<!-- 牌告單價 -->
	                                        <h4><spring:message code= "LB.W1524" /></h4>
	                                    </label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
<!-- 新台幣 -->
	                                       	<span class="ttb-unit"><spring:message code= "LB.NTD" /></span>
											<span>${bsData.PRICEFormat}</span>
<!-- 元 -->
<!-- 公克 -->
											<span class="ttb-unit"><spring:message code= "LB.W1511" /></span>
										</div>
									</span>
								</div>
								<!--回售金額-->
	                            <div class="ttb-input-item row">
	                                <span class="input-title">
	                                	<label>
<!-- 回售金額 -->
											<h4><spring:message code= "LB.W1527" /></h4>
	                                    </label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
<!-- 新台幣 -->
	                                       	<span class="ttb-unit"><spring:message code= "LB.NTD" /></span>
											<span>${bsData.TRNAMTFormat}</span>
<!-- 元 -->
											<span class="ttb-unit"><spring:message code= "LB.Dollar" /></span>
										</div>
									</span>
								</div>
								<c:if test="${bsData.FEEAMT1Format != '0'}">
		                            <div class="ttb-input-item row">
		                                <span class="input-title">
		                                	<label>
<!-- 定期投資扣款失敗 -->
<!-- 累計手續費 -->
												<h4><spring:message code= "LB.W1529" /><br/><spring:message code= "LB.W1530" /></h4>
		                                    </label>
										</span>
		                                <span class="input-block">
											<div class="ttb-input">
<!-- 新台幣 -->
		                                       	<span class="ttb-unit"><spring:message code= "LB.NTD" /></span>
												<span>${bsData.FEEAMT1Format}</span>
<!-- 元 -->
												<span class="ttb-unit"><spring:message code= "LB.Dollar" /></span>
											</div>
										</span>
									</div>
								</c:if>
								<c:if test="${bsData.FEEAMT2Format != '0'}">
									<div class="ttb-input-item row">
		                                <span class="input-title">
		                                	<label>
<!-- 黃金撲滿扣款失敗 -->
<!-- 累計手續費 -->
												<h4><spring:message code= "LB.W1531" /><br/><spring:message code= "LB.W1530" /></h4>
		                                    </label>
										</span>
		                                <span class="input-block">
											<div class="ttb-input">
<!-- 新台幣 -->
		                                       	<span class="ttb-unit"><spring:message code= "LB.NTD" /></span>
												<span>${bsData.FEEAMT2Format}</span>
<!-- 元 -->
												<span class="ttb-unit"><spring:message code= "LB.Dollar" /></span>
											</div>
										</span>
									</div>
								</c:if>
								<div class="ttb-input-item row">
	                                <span class="input-title">
	                                	<label>
	                                	<!-- 存入臺幣金額 -->
											<h4><spring:message code= "LB.W1532" /></h4>
	                                    </label>
									</span>
	                                <span class="input-block">
										<div class="ttb-input">
<!-- 新台幣 -->
	                                       	<span class="ttb-unit"><spring:message code= "LB.NTD" /></span>
											<span>${bsData.TRNAMT2Format}</span>
<!-- 元 -->
											<span class="ttb-unit"><spring:message code= "LB.Dollar" /></span>
										</div>
									</span>
								</div>
	                        </div>
	                        
	                        
<!-- 列印 -->
	                  			<input type="button" id="printButton" value="<spring:message code= "LB.Print" />" class="ttb-button btn-flat-orange"/>
<!-- 交易單據 -->
	                			<input type="button" id="certButton" value="<spring:message code= "LB.Transaction_document" />" class="ttb-button btn-flat-orange"/>
	                		
	                    </div>
	                </div>
							<div class="text-left">
								
								<ol class="list-decimal text-left description-list">
								<p><spring:message code="LB.Description_of_page" /><!-- 說明 --></p>
									<!-- 電子簽章：為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章（載具i－key）拔除並登出系統。-->
									<li><spring:message code= "LB.Gold_Resale_P3_D1" /></li>
								</ol>
							</div>
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>