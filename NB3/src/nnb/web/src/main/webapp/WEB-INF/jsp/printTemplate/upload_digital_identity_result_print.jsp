<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function(){
			window.print();
		});
	</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br/><br/>
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
	<br/><br/><br/>
	<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
	<br/><br/><br/>
	
	<div> 
		<!-- 資料 -->
		<!-- 表格區塊 -->
		<div class="ttb-input-block">
	        <center><h4><p>完成補件</p></h4></center>
<%-- 			<center><img src="${__ctx}/img/N204-4.jpg" alt="<spring:message code= "LB.X0495" />" /></center><br> --%>
			
			<br>
			<br>
			<div class="text-left">
					<p><spring:message code="LB.X0219" />:</p>
					<p><spring:message code="LB.X0499" />。<br>
					<p><spring:message code="LB.X0221" /><B>${BRHNAME}(<spring:message code="LB.D0539" />：${BRHTEL})</B><spring:message code="LB.X0503" />
					<spring:message code="LB.X0501" /></p> 
					<p><spring:message code="LB.X0224" /></p>
				<br><BR>
			</div>
<!-- 			<div class="text-right"> -->
<%-- 				<font color=red>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<spring:message code="LB.W0605" />&nbsp;&nbsp;&nbsp;<spring:message code="LB.X0193" /></font> --%>
<!-- 			</div>     -->
		</div>
		
	</div>
	
</body>
</html>