<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {
// 		// 將.table變更為footable
// 		initFootable();	
		// 將.table變更為dataTable
		initDataTable();
		
		// 友善列印
		$("#printbtn").click(function(){
			var params = {
				"jspTemplateName":"loan_detail_print",
				"jspTitle":"<spring:message code='LB.Loan_Detail_Inquiry'/>",
				"CMQTIME":" ${loan_detail.data.CMQTIME }",
				"CMRECNUM":"${loan_detail.data.CMRECNUM }",
				"TOTALBAL":"${loan_detail.data.TOTALBAL}",
				"TWNUM":"${loan_detail.data.TWNUM}",
				"FXNUM":"${loan_detail.data.FXNUM}",
				"note":"${loan_detail.data.note}",
				"TOTALBAL":"${loan_detail.data.TOTALBAL_320}",
				"TOPMSG_320":"${loan_detail.data.TOPMSG_320}",
				"MSGFLG_320":"${loan_detail.data.MSGFLG_320}",
				"TOPMSG_552":"${loan_detail.data.TOPMSG_552}",
				"errorMsg320":"${loan_detail.data.errorMsg320}",
				"errorMsg552":"${loan_detail.data.errorMsg552}"
				
			};
			openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
		});
		
		//$("#NOTE").html("<font style=\"color:red\">"+'${loan_detail.data.note}'+"</font>");
	});
	
// 	function barfunction(value,loanACN,loanSEQ,loanAMT){
// 			switch (value){
// 			case "01":
// 				$("#loanACN").val(loanACN);
// 				$("#loanSEQ").val(loanSEQ);
// 				$("#loanAMT").val(loanAMT);
// // 				fstop.getPage('${pageContext.request.contextPath}'+'/LOAN/ADVANCE/repay_advance_p1','', '');
//  				$("#formId").attr("action","${__ctx}/LOAN/ADVANCE/repay_advance_p1");
//  	  			$("#formId").submit(); 
// 				break;
// 			case "02":
// 				$("#loanACN").val(loanACN);
// 				$("#loanSEQ").val(loanSEQ);
// 				$("#loanAMT").val(loanAMT);
// // 				fstop.getPage('${pageContext.request.contextPath}'+'/LOAN/ADVANCE/repay_advance_p1','', '');
//  				$("#formId").attr("action","${__ctx}/LOAN/ADVANCE/settlement_advance_p1");
//  	  			$("#formId").submit(); 
// 				break;
// 			case "03":
// 				fstop.getPage('${pageContext.request.contextPath}'+'/HOUSE/GUARANTEE/interest_list','', '');
// 				break;
// 			case "04":
// 				fstop.getPage('${pageContext.request.contextPath}'+'/LOAN/QUERY/paid_query','', '');
// 				break;
// 			}
// 		}
	
	
	//快速選單
		 	function formReset(value,loanACN,loanSEQ,loanAMT){
				switch(value){
					case "repay_advance_p1":
		 				$("#loanACN").val(loanACN);
		 				$("#loanSEQ").val(loanSEQ);
		 				$("#loanAMT").val(loanAMT);
		// 				fstop.getPage('${pageContext.request.contextPath}'+'/LOAN/ADVANCE/repay_advance_p1','', '');
		  				$("#formId").attr("action","${__ctx}/LOAN/ADVANCE/repay_advance_p1");
		  	  			$("#formId").submit(); 
		 				break;
					case "settlement_advance_p1":
						$("#loanACN").val(loanACN);
		 				$("#loanSEQ").val(loanSEQ);
		 				$("#loanAMT").val(loanAMT);
		// 				fstop.getPage('${pageContext.request.contextPath}'+'/LOAN/ADVANCE/repay_advance_p1','', '');
		  				$("#formId").attr("action","${__ctx}/LOAN/ADVANCE/settlement_advance_p1");
		  	  			$("#formId").submit(); 
		 				break;
					case "interest_list":
						fstop.getPage('${pageContext.request.contextPath}'+'/HOUSE/GUARANTEE/interest_list','', '');
						break;
					case "paid_query":
						fstop.getPage('${pageContext.request.contextPath}'+'/LOAN/QUERY/paid_query','', '');
						break;
				}
			}
	//快速選單測試新增
 	//將actionbar select 展開閉合
	$(function(){
   		$("#click").on('click', function(){
       	var s = $("#actionBar2").attr('size')==1?8:1
       	$("#actionBar2").attr('size', s);
   		});
   		$("#actionBar2 option").on({
       		click: function() {
           	$("#actionBar2").attr('size', 1);
       		},
   		});
	});
	
	function hd2(T){
		var t = document.getElementById(T);
		if(t.style.visibility === 'visible'){
			t.style.visibility = 'hidden';
		}
		else{
			$("div[id^='actionBar2']").each(function() {
				var d = document.getElementById($(this).attr('id'));
				d.style.visibility = 'hidden';
		    });
			t.style.visibility = 'visible';
		}
	}
</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 貸款服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Loan_Service" /></li>
    <!-- 借款查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Loan_Inquiry" /></li>
    <!-- 借款明細查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Loan_Detail_Inquiry" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 		主頁內容  -->
			<h2><spring:message code="LB.Loan_Detail_Inquiry"/></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="${__ctx}/LOAN/QUERY/loan_detail">
				<input type="hidden" id="N320GO" name="N320GO" value="${loan_detail.data.N320GO}"/>
				<input type="hidden" id="N552GO" name="N552GO" value="${loan_detail.data.N552GO}"/>
				<input type="hidden" id="USERDATA_X50_552" name="USERDATA_X50_552" value='${loan_detail.data.USERDATA_X50_552}'/>
				<input type="hidden" id="USERDATA_X50_320" name="USERDATA_X50_320" value='${loan_detail.data.USERDATA_X50_320}'/>
				<input type="hidden" id="loanACN" name="loanACN" />
				<input type="hidden" id="loanSEQ" name="loanSEQ" />
				<input type="hidden" id="loanAMT" name="loanAMT" />
				
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<ul class="ttb-result-list">
							<li>
								<!-- 查詢時間 -->
								<h3>
								<spring:message code="LB.Inquiry_time" />
								</h3>
								<p>
								${loan_detail.data.CMQTIME}
								</p>
							</li>
							<li>	
								<!-- 資料總數 -->
								<h3>
								<spring:message code="LB.Total_records" />
								</h3>
								<p>
								${loan_detail.data.CMRECNUM}&nbsp;<spring:message code="LB.Rows"/>
								</p>	
							</li>
						</ul>
						<ul class="ttb-result-list" style="display:block;">
							<li>
							<h3>
								<table>
								<c:if test="${loan_detail.data.noteFlag =='Y'}">
								<tr>
								<td>
								<spring:message code="LB.Note" />:
								</td>
								<td>
								<font style="color:red">
								<!-- 備註 -->額外降息措施109/4/1至109/9/30，客戶免申辦。
								</font>
								</td>
								</tr>
								<tr><td><br></td><td><br></td></tr>
								</c:if>
								<tr>
								<td>
								<spring:message code="LB.Note" />:
								</td>
								<td id="NOTE">
								<font style="color:red">
								<!-- 備註 -->${loan_detail.data.note}
								</font>
								</td>
								</tr>
								</table>
							</h3>
							</li>
						</ul>
						<ul class="ttb-result-list">
							<li>
								<h3>
									<spring:message code="LB.Report_name" />
								</h3>
								<p>
									<spring:message code="LB.NTD_loan_detail"/>
								</p>
							</li>
						</ul>
						<!-- 台幣借款明細表-->
						<table  class="stripe table-striped ttb-table dtable" data-toggle-column="first">
							<thead>
								<tr>
									<!-- 帳號-->
									<th><spring:message code="LB.Loan_account" /></th>
									<!-- 分號-->
									<th>	
										<spring:message code="LB.Seq_of_account" />
									</th>
									<!-- 原貸金額-->
									<th><spring:message code="LB.Original_loan_amount" /></th>
									<!-- 貸款餘額-->
									<th>
										<spring:message code="LB.Loan_Outstanding" />
									</th>
									<!-- 利率(%)-->
									<th><spring:message code="LB.Interest_rate1" /></th>
									<!-- 初貸日（額度起日）-->
									<th><spring:message code="LB.Loan_drawdown_date" /><br><spring:message code="LB.Loan_drawdown_date2" /></th>
									<!-- 到期日（額度迄日）-->
									<th><spring:message code="LB.Expired_date" /><br><spring:message code="LB.Expired_date2" /></th>
									<!-- 開始償還本金月份-->
									<th><spring:message code="LB.Months_of_deferred_principal" /><hr><spring:message code="LB.End_date_of_repay_interest" /></th>
									<!-- 繳息迄日-->
<%-- 									<th><spring:message code="LB.End_date_of_repay_interest" /></th> --%>
									<!-- 快速選項-->
									<th><spring:message code="LB.Quick_Menu" /></th>
								</tr>
							</thead>
								<tbody>
								<c:choose>
									<c:when test="${loan_detail.data.TOPMSG_320 =='NoCall'}">
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
<%-- 										<td></td> --%>
										<td></td>
									</c:when>
									<c:when test="${loan_detail.data.TWNUM =='0' && loan_detail.data.TOPMSG_320 != 'OKLR' && loan_detail.data.TOPMSG_320 != 'OKOV' && loan_detail.data.TOPMSG_320 != 'ERDB'}">
										<tr>
											<td class="text-center">${loan_detail.data.TOPMSG_320}</td>
											<td class="text-center">${loan_detail.data.errorMsg320}</td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
<%-- 											<td></td> --%>
											<td></td>
										</tr>
									</c:when>
									<c:when test="${loan_detail.data.TWNUM !='0' || loan_detail.data.TOPMSG_320 eq 'OKLR' || loan_detail.data.TOPMSG_320 eq 'OKOV' || loan_detail.data.TOPMSG_320 eq 'ERDB'}">
										<c:choose>
											<c:when test="${loan_detail.data.MSGFLG_320=='01'}">
												<tr>
													<td class="text-center">MSGFLG=01</td>
													<td class="text-center"><spring:message code="LB.Loan_contact_note" /></td><!-- 請洽櫃檯-->
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
<%-- 													<td></td> --%>
													<td></td>
												</tr>
											</c:when>
											<c:when test="${loan_detail.data.MSGFLG_320=='00'}">
												<c:forEach var="dataList" items="${loan_detail.data.TW}" varStatus="data">
													<tr>
														<td class="text-center">${dataList.ACNCOVER}</td>
														<td class="text-center">${dataList.SEQ}</td>
														<!--原貸金額-->
														<td class="text-right">
															${dataList.AMTORLN}
														</td>
														<!-- 貸款餘額 -->
														<td class="text-right">
															${dataList.BAL}
														</td>
														<td class="text-right">${dataList.ITR}</td>
														<td class="text-center">${dataList.DATFSLN}</td>
														<td class="text-center">${dataList.DDT}</td>
														<td class="text-center">${dataList.AWT}<hr>${dataList.DATITPY}</td>
<%-- 														<td class="text-center">${dataList.DATITPY}</td> --%>
														<td class="text-center">
																<c:if test="${dataList.PAYTYPE eq 'Y'}">
																	<input type="button" class="d-lg-none" id="click" onclick="hd2('actionBar2${data.index}')" value="..." />
<%-- 																	<input type="button" class="d-sm-none d-block" id="click" value="Click" onclick="hd2('actionBar2${data.index}')" /> --%>
																	<!-- 快速選單測試 -->
																	<select class="d-none d-lg-inline-block custom-select fast-select" id="actionBar"
																		onchange="formReset(this.value,'${dataList.ACN}','${dataList.SEQ}','${dataList.AMTAPY}');">
																		<option value="">-<spring:message code="LB.Select" />-</option>
																		<option value="repay_advance_p1"><spring:message code="LB.X0972" /></option><!-- 提前償還本金 -->
																		<option value="interest_list"><spring:message code="LB.W0876" /></option><!-- 房屋擔保借款繳息清單 -->
																		<option value="paid_query"><spring:message code="LB.Paid_principal_and_interest_inquiry" /></option><!-- 已繳款本息查詢 -->
																	</select>
																	<!-- 快速選單測試新增div -->
												 					<div id="actionBar2${data.index}" class="fast-div-grey" style="visibility: hidden">
																		<div class="fast-div">
																			<img src="${__ctx}/img/icon-close-2.svg" class="top-close-btn" onclick="hd2('actionBar2${data.index}')">
                              												<p><spring:message code= "LB.X1592" /></p>
																			<ul>
																				<a href="javascript:void(0)" onclick="formReset('repay_advance_p1','${dataList.ACN}','${dataList.SEQ}','${dataList.AMTAPY}');"><li><spring:message code="LB.X0972" /><img src="${__ctx}/img/icon-10.svg" align="right"></li></a><!-- 提前償還本金 -->
																				<a href="${__ctx}/HOUSE/GUARANTEE/interest_list"><li><spring:message code="LB.W0876" /><img src="${__ctx}/img/icon-10.svg" align="right"></li></a><!-- 房屋擔保借款繳息清單 -->
																				<a href="${__ctx}/LOAN/QUERY/paid_query"><li><spring:message code="LB.Paid_principal_and_interest_inquiry" /><img src="${__ctx}/img/icon-10.svg" align="right"></li></a><!-- 已繳款本息查詢 -->
																			</ul>
																			<input type="button" class="bottom-close-btn" onclick="hd2('actionBar2${data.index}')" value="<spring:message code= "LB.X1572" />"/>
																		</div>
																	</div>
																</c:if>
																<c:if test="${dataList.PAYTYPE =='A'}">
																	<input type="button" class="d-lg-none" id="click" onclick="hd2('actionBar2${data.index}')" value="..." />
<%-- 																	<input type="button" class="d-sm-none d-block" id="click" value="Click" onclick="hd2('actionBar2${data.index}')" /> --%>
																	<!-- 快速選單測試 -->
																	<select class="d-none d-lg-inline-block custom-select fast-select" id="actionBar"
																		onchange="formReset(this.value,'${dataList.ACN}','${dataList.SEQ}','${dataList.AMTAPY}');">
																		<option value="">-<spring:message code="LB.Select" />-</option>
																		<option value="repay_advance_p1"><spring:message code="LB.X0972" /></option><!-- 提前償還本金 -->
																		<option value="settlement_advance_p1"><spring:message code="LB.X0973" /></option><!-- 貸款結清 -->
																		<option value="interest_list"><spring:message code="LB.W0876" /></option><!-- 房屋擔保借款繳息清單 -->
																		<option value="paid_query"><spring:message code="LB.Paid_principal_and_interest_inquiry" /></option><!-- 已繳款本息查詢 -->
																	</select>
																	<!-- 快速選單測試新增div -->
																	<div id="actionBar2${data.index}" class="fast-div-grey" style="visibility: hidden">
																		<div class="fast-div">
																			<img src="${__ctx}/img/icon-close-2.svg" class="top-close-btn" onclick="hd2('actionBar2${data.index}')">
                              												<p><spring:message code= "LB.X1592" /></p>
																			<ul>
																				<a href="javascript:void(0)" onclick="formReset('repay_advance_p1','${dataList.ACN}','${dataList.SEQ}','${dataList.AMTAPY}');"><li><spring:message code="LB.X0972" /><img src="${__ctx}/img/icon-10.svg" align="right"></li></a><!-- 提前償還本金 -->
																				<a href="javascript:void(0)" onclick="formReset('settlement_advance_p1','${dataList.ACN}','${dataList.SEQ}','${dataList.AMTAPY}');"><li><spring:message code="LB.X0973" /><img src="${__ctx}/img/icon-10.svg" align="right"></li></a><!-- 貸款結清 -->
																				<a href="${__ctx}/HOUSE/GUARANTEE/interest_list"><li><spring:message code="LB.W0876" /><img src="${__ctx}/img/icon-10.svg" align="right"></li></a><!-- 房屋擔保借款繳息清單 -->
																				<a href="${__ctx}/LOAN/QUERY/paid_query"><li><spring:message code="LB.Paid_principal_and_interest_inquiry" /><img src="${__ctx}/img/icon-10.svg" align="right"></li></a><!-- 已繳款本息查詢 -->
																			</ul>
																			<input type="button" class="bottom-close-btn" onclick="hd2('actionBar2${data.index}')" value="<spring:message code= "LB.X1572" />"/>
																		</div>
																	</div>
																</c:if>
																<c:if test="${dataList.PAYTYPE =='B'}">
																	<input type="button" class="d-lg-none" id="click" onclick="hd2('actionBar2${data.index}')" value="..." />
																	<select class="d-none d-lg-inline-block custom-select fast-select" id="actionBar"
																		onchange="formReset(this.value,'${dataList.ACN}','${dataList.SEQ}','${dataList.AMTAPY}');">
																		<option value="">-<spring:message code="LB.Select" />-</option>
																		<option style="color:red;" value="repay_advance_p1"><spring:message code="LB.X0972" />(<spring:message code="LB.X2278" />)</option><!-- 提前償還本金(符合降息) -->
																		<option value="settlement_advance_p1"><spring:message code="LB.X0973" /></option><!-- 貸款結清 -->
																	</select>
																	<div id="actionBar2${data.index}" class="fast-div-grey" style="visibility: hidden">
																		<div class="fast-div">
																			<img src="${__ctx}/img/icon-close-2.svg" class="top-close-btn" onclick="hd2('actionBar2${data.index}')">
                              												<p><spring:message code= "LB.X1592" /></p>
																			<ul>
																				<a href="javascript:void(0)" onclick="formReset('repay_advance_p1','${dataList.ACN}','${dataList.SEQ}','${dataList.AMTAPY}');"><li style="color:red"><spring:message code="LB.X0972" />(<spring:message code="LB.X2278" />)<img src="${__ctx}/img/icon-10.svg" align="right"></li></a><!-- 提前償還本金(符合降息) -->
																				<a href="javascript:void(0)" onclick="formReset('settlement_advance_p1','${dataList.ACN}','${dataList.SEQ}','${dataList.AMTAPY}');"><li><spring:message code="LB.X0973" /><img src="${__ctx}/img/icon-10.svg" align="right"></li></a><!-- 貸款結清 -->
																			</ul>
																			<input type="button" class="bottom-close-btn" onclick="hd2('actionBar2${data.index}')" value="<spring:message code= "LB.X1572" />"/>
																		</div>
																	</div>
																</c:if>
																<c:if test="${dataList.PAYTYPE =='C'}">
																	<input type="button" class="d-lg-none" id="click" onclick="hd2('actionBar2${data.index}')" value="..." />
																	<select class="d-none d-lg-inline-block custom-select fast-select" id="actionBar"
																		onchange="formReset(this.value,'${dataList.ACN}','${dataList.SEQ}','${dataList.AMTAPY}');">
																		<option value="">-<spring:message code="LB.Select" />-</option>
																		<option style="color:red;" value="repay_advance_p1"><spring:message code="LB.X0972" />(<spring:message code="LB.X2278" />)</option><!-- 提前償還本金 -->
																	</select>
																	<div id="actionBar2${data.index}" class="fast-div-grey" style="visibility: hidden">
																		<div class="fast-div">
																			<img src="${__ctx}/img/icon-close-2.svg" class="top-close-btn" onclick="hd2('actionBar2${data.index}')">
                              												<p><spring:message code= "LB.X1592" /></p>
																			<ul>
																				<a href="javascript:void(0)" onclick="formReset('repay_advance_p1','${dataList.ACN}','${dataList.SEQ}','${dataList.AMTAPY}');"><li style="color:red"><spring:message code="LB.X0972" />(<spring:message code="LB.X2278" />)<img src="${__ctx}/img/icon-10.svg" align="right"></li></a><!-- 提前償還本金(符合降息) -->
																			</ul>
																			<input type="button" class="bottom-close-btn" onclick="hd2('actionBar2${data.index}')" value="<spring:message code= "LB.X1572" />"/>
																		</div>
																	</div>
																</c:if>
														</td>
													</tr>
												</c:forEach>
											</c:when>
										</c:choose>
									</c:when>
								</c:choose>
								</tbody>
						</table>
						<c:if test="${(loan_detail.data.TOPMSG_320 =='OKOV' || loan_detail.data.TOPMSG_320 =='OKLR') && loan_detail.data.MSGFLG_320=='00' }">
						<div class="text-left">
							<!-- 總計XX筆-->
							<p><spring:message code="LB.Total_records"/>&nbsp;${loan_detail.data.TWNUM}&nbsp; <spring:message code="LB.Rows"/></p>
							<!-- 新臺幣XXXX元-->
							<p><spring:message code="LB.NTD"/> &nbsp; 
									${loan_detail.data.TOTALBAL_320} &nbsp;
							<spring:message code="LB.Dollar"/></p>							
							<spring:message code="LB.Loan_detail_P1_NTD_note"/>
							<!-- 可提前償還本金(新臺幣):繳息狀況正常之中長期貸款戶(不含青創、微創等創業貸款)，償還方式屬本息平均攤還者。 -->
						</div>
						</c:if>
						<br/>
						
			
						<c:if test="${loan_detail.data.TOPMSG_552 eq 'OKOV' || loan_detail.data.TOPMSG_320 eq 'OKOV'}">
							<div class="ttb-input">
								<font color="red"><spring:message code="LB.X0076" /></font><!-- 查詢結果超出每頁可顯示最大筆數，如欲顯示後續資料，請按 -->
								<button type="button" class="btn-flat-orange" id="Query" onclick="submit();"/><spring:message code="LB.X0151" /></button><!-- 繼續查詢 -->
							</div>
						</c:if>
						<br/>
						
						<ul class="ttb-result-list">
							<li>
								<h3>
									<spring:message code="LB.Report_name" />
								</h3>
								<p>
									<spring:message code="LB.FX_loan_detail"/>
								</p>
							</li>
						</ul>
						<!-- 外幣借款明細表-->
						<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
							<thead>
								<tr>
									<!-- 帳號-->
									<th data-title='<spring:message code="LB.Loan_account"/>'><spring:message code="LB.Loan_account"/></th>
									<!-- 交易編號-->
									<th  data-title='<spring:message code="LB.Transaction_Number"/>'>	
										<spring:message code="LB.Transaction_Number" />
									</th>
									<!-- 幣別-->
									<th data-title='<spring:message code="LB.Currency"/>' data-breakpoints="xs sm"><spring:message code="LB.Currency"/></th>
									<!-- 放款餘額-->
									<th data-title='<spring:message code="LB.Loan_Balance"/>' data-breakpoints="xs sm">
										<spring:message code="LB.Loan_Balance" />
									</th>
									<!-- 動撥日-->
									<th data-title='<spring:message code="LB.X0965"/>' data-breakpoints="xs sm"><spring:message code="LB.X0965" /></th>
									<!-- 到期日-->
									<th data-title='<spring:message code="LB.Expired_date"/>' data-breakpoints="xs sm"><spring:message code="LB.Expired_date"/></th>
									<!-- 下次繳息日-->
									<th data-title='<spring:message code="LB.X0966"/>' data-breakpoints="xs sm"><spring:message code="LB.X0966" /></th>
									<!-- 適用利率-->
									<th data-title='<spring:message code="LB.X0967"/>' data-breakpoints="xs sm"><spring:message code="LB.X0967" /></th>
									<!-- 資料日期-->
									<th data-title='<spring:message code="LB.Data_date"/>' data-breakpoints="xs sm" ><spring:message code="LB.Data_date"/></th>
									<!-- 備註-->
									<th data-title='<spring:message code="LB.Note"/>' data-breakpoints="xs sm" ><spring:message code="LB.Note"/></th>
								</tr>
							</thead>
							<tbody>
							<c:choose>
								<c:when test="${loan_detail.data.TOPMSG_552 =='NoCall'}">
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</c:when>
								<c:when test="${loan_detail.data.TOPMSG_552 eq 'OKLR'||loan_detail.data.TOPMSG_552 eq 'OKOV'}">
									<c:forEach var="dataList" items="${loan_detail.data.FX}">
									
										<tr>
											<td class="text-center">${dataList.ACNCOVER}</td>
											<td class="text-center">${dataList.LNREFNO}${dataList.lcMemo}</td>
											<td class="text-center">
												${dataList.LNCCY} <!-- MB5用 -->
											</td>
											<td class="text-right">${dataList.LNCOS}</td>
											<!--<td>${dataList._BAL }</td>-->
											<td class="text-center">${dataList.LNINTST}</td>
											<td class="text-center">${dataList.LNDUEDT}</td>
											<td class="text-center">${dataList.NXTINTD}</td>
											<td class="text-right">${dataList.LNFIXR}</td>
											<td class="text-center">${dataList.LNUPDATE}</td>
											<td class="text-center">${dataList.TYPEA}</td>
										</tr>
										
									</c:forEach>
								</c:when>
								<c:when test="${loan_detail.data.TOPMSG_552 != 'OKLR' && loan_detail.data.TOPMSG_552 != 'OKOV'}">
									<tr>
										<td>${loan_detail.data.TOPMSG_552}</td>
										<td class="text-center">${loan_detail.data.errorMsg552}</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
								</c:when>
							</c:choose>
							</tbody>
						</table>
						<c:if test="${loan_detail.data.TOPMSG_552 =='OKOV' || loan_detail.data.TOPMSG_552 =='OKLR'}">
						<div class="text-left">
							<!-- 總計XX筆-->
							<p><spring:message code="LB.Total_records"/> ${loan_detail.data.FXNUM} <spring:message code="LB.Rows"/></p>
							
							<!-- 外幣 各幣別金額總和-->
							<c:forEach var="dataList" items="${loan_detail.data.CRY}">
								<p>${dataList.AMTLNCCY} &nbsp; ${dataList.FXTOTAMT}&nbsp;
								<spring:message code="LB.Dollar"/></p>		
							</c:forEach>
							
						</div>
						</c:if>
						<br>
						<!-- 列印鈕-->					
						<input type="button" class="ttb-button btn-flat-orange" style="margin: auto;" id="printbtn" value="<spring:message code="LB.Print" />"/>
					</div>
				</div>
				
						<ol class="description-list list-decimal">
							<p><spring:message code="LB.Description_of_page" /></p><!-- 說明 -->
							<li><spring:message code="LB.Loan_detail_P1_D1"/>
							<!-- 延滯繳息還本者，請逕洽原承貸分行辦理。 -->
							</li>
							<li><spring:message code="LB.Loan_detail_P1_D2"/>
							<!--外幣借款提供至前一營業日交易資料。 -->
							</li>
							<li><spring:message code="LB.Loan_detail_P1_D3"/>
							<!--外幣借款項目：買入光票、貼現、短（擔）放、中（擔）放、長（擔）放。 -->
							</li>
							<li><spring:message code="LB.Loan_detail_P1_D4"/>
							<!-- 消費者新台幣借款之利率調整通知為貸款利率引用本行２年定儲、 本行定儲指數、本行基準利率、本行１年定儲、本行定儲指數 、郵一定儲利率、行員中期利率、郵二定儲利率 、行員消貸利率、 基準利率月調 ，利率引用標準非屬上開者，請逕洽原貸分行。 -->
							</li>
						</ol>
			</form>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>