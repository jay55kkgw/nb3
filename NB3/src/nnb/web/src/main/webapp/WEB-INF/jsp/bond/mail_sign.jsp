<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<!-- 元件驗證身分JS -->
	<script type="text/javascript" src="${__ctx}/component/util/checkIdProcess.js"></script>
	<script type="text/javascript">
		var urihost = "${__ctx}";
	    $(document).ready(function () {
	    	// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 1000);
			// 初始化驗證碼
			setTimeout("initKapImg()", 200);
			// 生成驗證碼
			setTimeout("newKapImg()", 300);
	    	
	    	$("#CMSUBMIT").click(function(){
	    		var allCheck = true;
	    		
	    		$(".riskConfirmCheckBox").each(function(){
	    			if($(this).prop("checked") == false){
	    				allCheck = false;
	    			}
	    		});
	    		if(allCheck == true){
	    			processQuery(); 
	    		}
	    		else{
	    			errorBlock(
	    					null, 
	    					null,
	    					['<spring:message code="LB.Alert123" />'], 
	    					'<spring:message code="LB.Quit" />', 
	    					null
	    			);
	    		}
	    	
	    		
        });
	    	
	    $("#CMCANCEL").click(function(){
		   	fstop.getPage('${pageContext.request.contextPath}'+'/INDEX/index','', '');
		});	
	    	
	 });
	    
	 function init(){
		// 判斷顯不顯示驗證碼
			chaBlock();
			// 交易機制 click
			fgtxwayClick();	
	 }
	 
	 function fgtxwayClick() {
	 	$('input[name="FGTXWAY"]').change(function(event) {
	 			// 判斷交易機制決定顯不顯示驗證碼區塊
	 		chaBlock();
		});
	}
	 	// 判斷交易機制決定顯不顯示驗證碼區塊
	function chaBlock() {
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
			switch(fgtxway) {
			case '0':
				$('#CMPASSWORD').addClass("validate[required]")
				$("#chaBlock").hide();
				break;
			case '2':
				$('#CMPASSWORD').removeClass("validate[required]")
				$("#chaBlock").show();
			    break;
			    	
			default:
				$('#CMPASSWORD').removeClass("validate[required]")
				$("#chaBlock").hide();
			}
	 	}
	// 驗證碼刷新
	function changeCode() {
		$('input[name="capCode"]').val('');
		// 大小版驗證碼用同一個
		console.log("changeCode...");
		$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

		// 登入失敗解遮罩
		unBlockUI(initBlockId);
	}
	
	// 初始化驗證碼
	function initKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
	}
	
	// 生成驗證碼
	function newKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').click(function() {
			$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
		});
	}
	
	// 交易機制選項
	function processQuery(){
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
		console.log("fgtxway: " + fgtxway);
		switch (fgtxway) {
				// IKEY
			case '1':
				useIKey();
				break;
				// 晶片金融卡
			case '2':
				useCardReader();
				break;
			default:
				//alert("<spring:message code= "LB.Alert001" />");
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert001' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
				unBlockUI(initBlockId);
			}
	}
	
	//IKEY簽章結束
	function uiSignForPKCS7Finish(result){
		var main = document.getElementById("main");
		
		//成功
		if(result != "false"){
			//SubmitForm();
			checkxmlcn();
		}
		//失敗
		else{
			alert("IKEY簽章失敗");
			ShowLoadingBoard("MaskArea",false);
		}
	}
	
	function checkxmlcn(){
		var cusidn = '${sessionScope.cusidn}';
		var jsondc = $('#jsondc').val();
		var pkcs7Sign = $('#pkcs7Sign').val();
		var bs64 = $('#bs64').val();
		var uri = "${__ctx}"+"/COMPONENT/ikey_without_login_aj";
		var rdata = { UID:cusidn, jsondc: jsondc, pkcs7Sign:pkcs7Sign ,bs64:bs64 };
		
		fstop.getServerDataEx(uri, rdata, true, IkeyCheckcallback);
	}
	
	function IkeyCheckcallback(response) {
		
		var checkflag = response.data.checkflag;
		if(!checkflag){
			alert(response.data.msgCode +" : "+ response.data.msgName);
			ShowLoadingBoard("MaskArea",false);
			return ; 
		}
		if(checkflag.indexOf("SUCCESSFUL")>-1){
			initBlockUI();
            $("#formId").submit();
		}else{
			alert("<spring:message code= 'LB.X2287' />");
			ShowLoadingBoard("MaskArea",false);
		}
	}
	
	//取得卡片主帳號結束
	function getMainAccountFinish(result){
		//成功
		if(result != "false"){
			var main = document.getElementById("formId");
			main.ACNNO.value = result;
			var cardACN = result;
			if(cardACN.length > 11){
				cardACN = cardACN.substr(cardACN.length - 11);
			}
			
//			var x = { method: 'get', parameters: '', onComplete: CheckIdResult };	
//			var myAjax = new Ajax.Request( "simpleform?trancode=N953&TXID=N953&ACN=" + cardACN, x);
			var cusidn = '${sessionScope.cusidn}';
			var uri =  "${__ctx}"+"/COMPONENT/component_checkid_aj";
			var rdata = { CUSIDN: cusidn, ACN: cardACN };
			
			fstop.getServerDataEx(uri, rdata, true, CheckIdResult);
		}
		//失敗
		else{
			showTempMessage(500,"晶片金融卡身份查驗","","MaskArea",false);
		}
	}
</script>
</head>
<body>
	<!-- 	晶片金融卡 -->
	<%@ include file="../component/trading_component.jsp"%>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2391" /></li>
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X2569" /></li>
		</ol>
	</nav>


	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
                <form id="formId" method="post" action="${__ctx}/BOND/PURCHASE/bond_purchase_input">
                <!-- 			晶片金融卡及IKEY  -->
				<input type="hidden" id="jsondc" name="jsondc" value='${mail_sign_data.data.jsondc}'>
				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
				<input type="hidden" id="ISSUER" 	name="ISSUER" />
				<input type="hidden" id="ACNNO" 	name="ACNNO" />
				<input type="hidden" id="TRMID" 	name="TRMID" />
				<input type="hidden" id="iSeqNo" 	name="iSeqNo" />
				<input type="hidden" id="ICSEQ" 	name="ICSEQ" />
				<input type="hidden" id="ICSEQ1" 	name="ICSEQ1" />
				<input type="hidden" id="TAC" 		name="TAC" />
				<input type="hidden" id="CHIP_ACN"  name="CHIP_ACN" />
				<input type="hidden" id="OUTACN"	name="OUTACN">
				<input type="hidden" id="bs64"	name="bs64" value="Y">
				<input type="hidden" id="mailSign"	name="mailSign" value="Y">
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
                        	<div style="margin:0% 5% 0% 5%">
								<div class="NA01_3">
								<div class="head-line">聲明書(EMAIL 重覆客戶使用)</div>
								<br>
								<table width="100%" class="DataBorder"  style='font-family:標楷體'>
									<tr>
										本人/本公司於 貴行辦理特定金錢信託投資國內
										外有價證券業務（含 OBU)，<br>本人/本公司所留存
										貴行之電子信箱，業經電子信箱所有人同意使
										用，以便本人/本公司收取 貴行寄發之各項通知
										及報告書，<br>特此聲明，倘日後發生任何糾紛、風
										險或損失，皆由本人/本公司自行承擔，與 貴行
										無涉，絕無異議。
									</tr>
								</table>
								<br>
								<label class="check-block">
								<input type="checkbox" class="riskConfirmCheckBox" id="allCheckBox"/>
<!-- 								本人已詳細閱讀並同意以上之規範事項 -->
									<spring:message code="LB.X2570" />
								<span class="ttb-check"></span>
								</label>
								<br>
								</div>
							 </div>
							 <div class="ttb-input-block">
							 <div class="ttb-input-item row">
							 	<span class="input-title"> 
									<h4><spring:message code="LB.Transaction_security_mechanism" />：</h4>
								</span>
								<span class="input-block">
								<c:if test = "${sessionScope.isikeyuser}">
								<div class="ttb-input">
									<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block"> <spring:message code="LB.Electronic_signature" />
										<input type="radio" id="CMIKEY" name="FGTXWAY" value="1"> 
										<span class="ttb-radio"></span>
										</label> <!-- 電子簽章(請載入載具i-key) --> 
									</div>
									</span>
								</div>
								</c:if>
								<div class="ttb-input">
									<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block">
								        <input type="radio" name="FGTXWAY" id="CMCARD" value="2" checked>
								        <spring:message code="LB.Financial_debit_card" />
										<span class="ttb-radio"></span>
										</label>
									</div>						           	
									</span>
								</div>
								</span>
								<div class="ttb-input-item row" id="chaBlock" style="display:none">
								<!-- 驗證碼 -->
								<span class="input-title">
									<label>
										<h4><spring:message code= "LB.Captcha" /></h4>
									</label>
								</span>
								<span class="input-block">
									<spring:message code="LB.Captcha" var="labelCapCode" />
									<input id="capCode" name="capCode" type="text"
										class="text-input" maxlength="6" autocomplete="off">
									<img name="kaptchaImage" class = "verification-img" src="" />
									<input type="button" name="reshow" class="ttb-sm-btn btn-flat-orange"
										onclick="changeCode()" value="<spring:message code= "LB.Regeneration" />" />
								</span>
							</div> 
							 </div>
							 </div>
							 
							 
							<input type="button" id="CMCANCEL" value="<spring:message code="LB.Cancel"/>" class="ttb-button btn-flat-gray"/>
							<input type="button" id="CMSUBMIT" name="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
			               
                        </div>
                    </div>
                </form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>