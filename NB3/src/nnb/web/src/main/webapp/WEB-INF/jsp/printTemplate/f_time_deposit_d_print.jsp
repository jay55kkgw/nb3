<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
	$(document).ready(function() {
		window.print();
	});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br />
	<br />
	<div style="text-align: center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br />
	<br />
	<br />
	<div style="text-align: center">
		<font style="font-weight: bold; font-size: 1.2em">${jspTitle}</font>
	</div>
	<br />
	<br />
	<br />
	<label><spring:message code="LB.Inquiry_time" /> <!-- 查詢時間 -->：</label>
	<label>${CMQTIME}</label>
	<br />
	<br />
	<label><spring:message code="LB.Total_records" /> <!-- 資料總數 -->：</label>
	<label>${COUNT}　<spring:message code="LB.Rows" /></label>
	<br />
	<br />
		<c:set var ="AmountOrig" value="${TOTAMT}" />
		<c:set var ="AmountAfter" value="${fn:replace( fn:replace(fn:replace(AmountOrig,', ','<br>'),']',''),'[','')}" />
	<table>
		<tr>
			<td>
				<spring:message code="LB.Total_amount" />： <!-- 總計金額 -->
			</td>
			<td>
				${AmountAfter}
			</td>
		</tr>
	</table>
	<br />
	<br />
	<table class="print">
		<tr>
			<td style="text-align: center"><spring:message code="LB.Account" /></td>
			<td style="text-align: center"><spring:message code="LB.Certificate_no" /></td>
			<td style="text-align: center"><spring:message code="LB.Currency" /></td>
			<td style="text-align: center"><spring:message code="LB.Certificate_amount" /></td>
			<td style="text-align: center"><spring:message code="LB.Interest_rate1" /></td>
			<td style="text-align: center"><spring:message code="LB.Interest_calculation" /></td>
			<td style="text-align: center"><spring:message code="LB.Start_date" /></td>
			<td style="text-align: center"><spring:message code="LB.Maturity_date" /></td>
			<td style="text-align: center"><spring:message code="LB.Interest_transfer_to_account" /></td>
			<td style="text-align: center"><spring:message code="LB.Automatic_number_of_rotations" /></td>
			<td style="text-align: center"><spring:message code="LB.Automatic_number_of_unrotated" /></td>
		</tr>
		<c:forEach items="${dataListMap}" var="map">
			<tr>
				<td class="text-center">${map.ACN }</td>
				<td class="text-center">${map.FDPNO }</td>
				<td class="text-center">${map.CUID }</td>
				<td class="text-right">${map.BALANCE}</td>
				<td class="text-right">${map.ITR }</td>
				<td class="text-center">${map.INTMTH }</td>
				<td class="text-center">${map.DPISDT }</td>
				<td class="text-center">${map.DUEDAT }</td>
				<td class="text-center">${map.TSFACN }</td>
				<td class="text-center">${map.ILAZLFTM }</td>
				<td class="text-center">${map.AUTXFTM }</td>
			</tr>
		</c:forEach>
	</table>
	<br />
	<br />
</body>
</html>