<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="RS" value="${closing_tw_account_result }"></c:set>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>

	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		
		// 畫面初始化
		function init() {
			//列印
			print();
		}
		
		//列印
		function print() {
			$("#printbtn").click(function () {
				var params = {
						jspTemplateName:	"closing_tw_account_result_print",
						jspTitle:			'<spring:message code= "LB.X1276" />',
						CMQTIME:			'${CMQTIME }',
						ACN:				'${RS.ACN }',
						INACN:				'${RS.INACN_F }',
						O_TOTBAL:			'${RS.getO_TOTBAL_F() }',
						O_AVLBAL:			'${RS.getO_AVLBAL_F() }',
						I_TOTBAL:			'${RS.getI_TOTBAL_F() }',
						I_AVLBAL:			'${RS.getI_AVLBAL_F() }'
					};
				openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
		}
		
	</script>
</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 臺幣存款帳戶結清銷戶申請    -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0422" /></li>
		</ol>
	</nav>

	<!-- 左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.X0298" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form id="formId" method="post" action="">
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
                            <div class="ttb-message">
                                <span><spring:message code="LB.Transaction_successful" /></span>
                            </div>
    
                            <!-- 交易時間 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                    	<h4><spring:message code="LB.Trading_time" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
			                        <span>${CMQTIME }</span>
                                    </div>
                                </span>
                            </div>  
							
							<!-- 欲結清帳號 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D0424" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
			                        	<span>${RS.ACN }</span>
                                    </div>
                                </span>
                            </div>  
							
							<!-- 結清轉入本行帳號 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D0441" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
			                         <span>${RS.INACN_F }</span>
                                    </div>
                                </span>
                            </div>  
							
							<!-- 結清帳號帳戶餘額 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D0442" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
			                         <span>${RS.getO_TOTBAL_F() }</span>
                                    </div>
                                </span>
                            </div>  
							
							<!-- 結清帳號可用餘額 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D0443" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
			                         <span>${RS.getO_AVLBAL_F() }</span>
                                    </div>
                                </span>
                            </div>  
							
							<!-- 轉入帳號帳戶餘額 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.Payees_account_balance" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
			                         <span>${RS.getI_TOTBAL_F() }</span>
                                    </div>
                                </span>
                            </div>  
							
							<!-- 轉入帳號可用餘額 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.Payees_available_balance" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
			                         <span>${RS.getI_AVLBAL_F() }</span>
                                    </div>
                                </span>
                            </div>  
							
                        </div>

                        <input class="ttb-button btn-flat-orange" type="button" id="printbtn" value="<spring:message code="LB.Print" />" />
                        
                    </div>
                </div>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>