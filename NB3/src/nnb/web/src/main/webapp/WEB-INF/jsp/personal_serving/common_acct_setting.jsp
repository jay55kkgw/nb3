<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="../__import_head_tag.jsp"%>	
    <%@ include file="../__import_js.jsp" %>
    
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<style type="text/css">
		.fooicon-plus:before{
			position: absolute;
	    	left: -10px;
		}
		.spbutton {
		   margin: 0 10px 3px 0;
		    padding: 0 7px;
		    font-size: .875rem;
		    border: none;
		    border-radius: 12.5px;
		    color: #fff;
		}
	</style>
    <script type="text/javascript">
    
		$(document).ready(function() {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 50);
			// 開始跑下拉選單並完成畫面
			setTimeout("init()", 400);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
			//initFootable();
			initDataTable();
		});
		
		function init(){
			//表單驗證
			$("#formId").validationEngine({
				binded : false,
				promptPosition : "inline",
			});
			
			//submit
			$("#CMSUBMIT").click(function(e) {
				var mySelect = $("select[id*=DPAGACNO]");
				if(mySelect.children("option").length==0 && $('input:radio[name="r1"]:checked').val()=='1'){
					return false;
				}
				console.log("submit~~");
				$('#DPGONAME').removeClass("validate[required,custom[notSPCha]]");
				$('#DPTRDACNO').removeClass("validate[required,custom[CapitalAndNum]]");
				$('#DPAGACNO').removeClass("validate[funcCall[validate_CheckSelect[<spring:message code='LB.Designated_account' />,DPAGACNO,#]]]");
				
				if($('input:radio[name="r1"]:checked').val()=='1'&& $('#EXECUTEFUNCTION').val()!="DELETE"){
					//選取約定帳號時表單驗證
					$('#DPAGACNO').addClass("validate[funcCall[validate_CheckSelect[<spring:message code='LB.Designated_account' />,DPAGACNO,#]]]");
					$('#DPGONAME').addClass("validate[required,custom[notSPCha]]");
					//選取約定hidden欄位賦值
					$('#DPTRACNO').val('1');
				}else if($('input:radio[name="r1"]:checked').val()=='2'&& $('#EXECUTEFUNCTION').val()!="DELETE"){
					//選取非約定帳號時表單驗證
					$('#DPTRDACNO').addClass("validate[required,custom[CapitalAndNum]]");
					$('#DPGONAME').addClass("validate[required,custom[notSPCha]]");
					//選取非約定hidden欄位賦值
					$('#DPTRIBANK').val($('#DPNGACNO').val());
					$('#DPTRACNO').val('2');
				}
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					initBlockUI();
					$('#formId').submit();
				}
			});

			getBankList();
			getInAcno();
			$("input[type=radio][value='1']").prop("checked","checked");
		}
		
		function getInAcno(){
			uri = '${__ctx}' + "/PERSONAL/SERVING/getN997AgInco_aj";
			var bs = fstop.getServerDataEx(uri, null, false);
			n921_rows = bs.data.REC;
			console.log("n921_rows: " + JSON.stringify(n921_rows));
			$.each(n921_rows, function(key, val) {
				if (val.TEXT == '- ') {//濾掉最後一筆無資料值  之後調整
				} else {
					$('#DPAGACNO').append(
						$("<option></option>").attr("value", val.VALUE).text(val.TEXT));
				}
			});
		}
		function getBankList(){
			uri = '${__ctx}' + "/PERSONAL/SERVING/getBankList_aj";
			var data = fstop.getServerDataEx(uri, null, false);
			console.log("data: " + JSON.stringify(data));
			$.each(data, function(key, val) {
				if(val.adbankid =='050'){
					$('#DPNGACNO').append($("<option></option>").attr("value" , val.adbankid
						).text(val.adbankid+'-'+val.adbankname).prop("selected","selected"));
				}else{
					$('#DPNGACNO').append($("<option></option>").attr("value" , val.adbankid
							).text(val.adbankid+'-'+val.adbankname));
				}
			});
		}
		
		var confirmType=0;
		$("#errorBtn1").click(function(){
			if(confirmType == 0){
				return false;
			}
			if(confirmType == 1){
				$('#CMSUBMIT').click();
			}
			confirmType=0;
		});
		$("#errorBtn2").click(function(){
			if(confirmType == 0){
				return false;
			}
			if(confirmType == 1){
				$('#EXECUTEFUNCTION').val('INSERT');
			}
			$('#error-block').hide();
			confirmType=0;
		});
				              //執行動作,資料庫內PK,好記名稱, 銀行代碼,帳戶,約定非約定,銀行中文名稱,表格第幾筆
		function chgdata(EXECUTEFUNCTION,DPACCSETID,DPGONAME,DPTRIBANK,DPTRDACNO,DPTRACNO,ADBANKNAME,number) {
			if (EXECUTEFUNCTION == 'DELETE') {
				$('#EXECUTEFUNCTION').val(EXECUTEFUNCTION);
				$('#DPACCSETID').val(DPACCSETID);
				$('#DPGONAME').val(DPGONAME);
				$('#DPTRIBANK').val(DPTRIBANK);
				$('#ADBANKNAME').val(ADBANKNAME);
 				$('#DPTRDACNO').val(DPTRDACNO);
// 				if(confirm( "<spring:message code="LB.X0918" />："+DPGONAME+"\n"+
// 			            	"<spring:message code="LB.X0431" />："+DPTRIBANK+"-"+ADBANKNAME+"\n"+
// 			            	"<spring:message code="LB.Payees_account_no" />："+DPTRDACNO+"\n"+ "<spring:message code="LB.Confirm029" />﹖")){
// 					$('#CMSUBMIT').click();
// 				}else{
// 					$('#EXECUTEFUNCTION').val('INSERT');
// 				}
 				confirmType = 1;
				errorBlock(
							null, 
							null,
							[ "<spring:message code="LB.X0918" />："+DPGONAME,"<spring:message code="LB.X0431" />："+DPTRIBANK+"-"+ADBANKNAME,"<spring:message code="LB.Payees_account_no" />："+DPTRDACNO+"\n"+ "<spring:message code="LB.Confirm029" />﹖"], 
							'<spring:message code= "LB.Confirm" />', 
							'<spring:message code= "LB.Cancel" />'
						);
			} else if (EXECUTEFUNCTION == 'UPDATE') {
				
				if(DPTRACNO == '1'){
					//將資料動態到表格
					//非約定radio chcked
					$("#r1[value='1']").prop("checked","checked");
					$("#DPTRDACNO").val("");
					//option選定
					var value = "{\"BNKCOD\":\""+DPTRIBANK+"\",\"AGREE\":\"1\",\"ACN\":\""+DPTRDACNO+"\"}";
					$("#DPAGACNO option[value='"+value+"']").prop("selected","selected");
					//input text
					$('#DPGONAME').val($("#name" + number + "").text());
					$('#DPGONAME').focus();
				}else if(DPTRACNO == '2'){
					//將資料動態到表格
					//非約定radio chcked
					$("#r1[value='2']").prop("checked","checked");
					$("#DPAGACNO").val("#");
					//option選定
					$("#DPNGACNO option[value="+DPTRIBANK+"]").prop("selected","selected");
					//input text
					$('#DPTRDACNO').val($("#acno" + number).text());
					$('#DPGONAME').val($("#name" + number).text());
					$('#DPGONAME').focus();
				}
				
				$('#CMSUBMIT').val('<spring:message code="LB.X1384" />');
				$('#EXECUTEFUNCTION').val(EXECUTEFUNCTION);
				$('#DPACCSETID').val(DPACCSETID);
				
					

			} 
		}
		
		function empty(){
			$('#DPTRDACNO').val("");
			$('#DPGONAME').val("");
			$("#DPNGACNO option[value='050']").prop("selected","selected");
			$("#DPAGACNO option[value='#']").prop("selected","selected");
			$('#EXECUTEFUNCTION').val("INSERT");
			$('#CMSUBMIT').val('<spring:message code="LB.Confirm" />');
		}
		
		function changeRadio1(){
			$('#DPTRDACNO').val('');
			$('#DPNGACNO').val('050');
			cleanErrorMsg();
			$("#r1[value='1']").prop("checked","checked");
						
		}
		
		function changeRadio2(){
			$("#r1[value='2']").prop("checked","checked");
			$("#DPAGACNO").val("#");
			cleanErrorMsg();
		}
		
		function cleanErrorMsg(){
			$('.formErrorContent').each(function(){
				$(this).click();
			})
		}
		
	</script>
    	
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 常用帳號設定     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0429" /></li>
		</ol>
	</nav>

		<!-- menu、登出窗格 --> 
 		   <div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單及主頁內容 -->
		 </div><!-- content row END --> 
        <main class="col-12">
		<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.X0429" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="${__ctx}/PERSONAL/SERVING/common_acct_setting_result" autocomplete="off">
				<input type="hidden" id="DPACCSETID" name="DPACCSETID" >
  				<input type="hidden" id="DPTRIBANK" name="DPTRIBANK" >
  				<input type="hidden" id="DPTRACNO" name="DPTRACNO" >
				<input type="hidden" id="EXECUTEFUNCTION" name="EXECUTEFUNCTION" value="INSERT">
		               <div class="main-content-block row">
							<div class="col-12">
								<div class="ttb-input-block">
								
									<div class="ttb-input-item row">
										<span class="input-title"> <spring:message code="LB.Payees_account_no" /></h4></label></span> <!-- 轉入帳號 -->
										<span class="input-block">
											<div class="ttb-input">
											<label class="radio-block"> 
											<input type="radio"  id="r1" name="r1" value="1" onclick="changeRadio1()">
											<!-- 約定帳號 -->
											 &nbsp;<spring:message code="LB.Designated_account" />
											<span class="ttb-radio"></span></label>
											</div>
										<div class="ttb-input">	
										<label>
										<select name="DPAGACNO" id="DPAGACNO" class="custom-select select-input half-input" onchange="changeRadio1()">
											<option value="#">----<spring:message code="LB.W0259" />-----</option>
										</select>
										</label>
										</div>
										<div class="ttb-input">
											<label class="radio-block">
												<input type="radio" id="r1" name="r1" value="2" onclick="changeRadio2()">
												<!-- 非約定  -->
												<spring:message code="LB.X0430" />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
										<label>
											<select name="DPNGACNO" id="DPNGACNO" class="custom-select select-input half-input" onchange="changeRadio2()"> 
											</select>
										</label>
										</div>
										<div class="ttb-input">
										<label>
											<input type="text" id="DPTRDACNO" name="DPTRDACNO" maxLength="16" class="text-input " placeholder="<spring:message code="LB.Enter_Account" />" autocomplete="off"/> 
										</label>
										</div>
									</div>
									<div class="ttb-input-item row">
										<span class="input-title"> <label><h4><spring:message code="LB.Favorite_name" /></h4></label></span> <!-- 好記名稱 -->
										<span class="input-block">
											<div class="ttb-input">
												<input type="text" id="DPGONAME" name="DPGONAME" class="text-input " maxLength="15" autocomplete="off"/>
											</div>
										</span>
									</div>
									<div class="text-center">
									<input type="button" class="spbutton btn-flat-gray" value="<spring:message code="LB.Re_enter" />" onclick="empty();"/>
									<input id="CMSUBMIT"  type="button" class="spbutton btn-flat-orange" value="<spring:message code="LB.Confirm" />"/>
									</div>
							</div>
								<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
										<!--常用帳號列表 -->
						        		<thead>
							        		<tr>
							        			<!-- -->
							        			<th data-title=''></th>
							        			<!--轉入銀行 -->
							        			<th data-title='<spring:message code="LB.X0431" />' data-breakpoints="xs sm"><spring:message code="LB.X0431" /></th>
							        			<!--轉入帳號 -->
							        			<th data-title='<spring:message code="LB.Payees_account_no" />'><spring:message code="LB.Payees_account_no"/></th>
							        			<!-- 好記名稱-->
												<th data-title='<spring:message code="LB.Favorite_name" />' data-breakpoints="xs sm"><spring:message code="LB.Favorite_name"/></th>
							        			<!--序號 -->
							        			<th data-title='<spring:message code="LB.Serial_number" />' nowrap><spring:message code="LB.Serial_number" /></th>
							        		</tr>
						        		</thead>
						        		<tbody>
											<c:if test="${common_acct_setting.data.txntraccsetListSize == '0'}">
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
											</c:if>
											<c:if test="${common_acct_setting.data.txntraccsetListSize != '0'}">
												<c:forEach var="dataList" items="${common_acct_setting.data.txntraccset}" varStatus="order">
													<tr>
													<td class="text-center" id='btn${order.count}'>
														<input type="button"
															class="spbutton btn-flat-orange" value="<spring:message code="LB.X0432" />"
															onclick="chgdata('UPDATE','${dataList.DPACCSETID}','<c:out value="${dataList.DPGONAME}" />','${dataList.DPTRIBANK}','${dataList.DPTRDACNO}','${dataList.DPTRACNO}','${dataList.ADBANKNAME}',${order.count});" />
															<input type="button" class="spbutton btn-flat-orange"
															value="<spring:message code="LB.X0433" />"
															onclick="chgdata('DELETE','${dataList.DPACCSETID}','<c:out value="${dataList.DPGONAME}" />','${dataList.DPTRIBANK}','${dataList.DPTRDACNO}','${dataList.DPTRACNO}','${dataList.ADBANKNAME}',${order.count});" />
													</td>
													<td class="text-center" id='bankName${order.count}'>${dataList.DPTRIBANK}-${dataList.ADBANKNAME}</td>
													<td class="text-center" id='acno${order.count}'>${dataList.DPTRDACNO}</td>
													<td class="text-center" id='name${order.count}'><c:out value="${dataList.DPGONAME}" /></td>
													
													<td class="text-center">${order.count}</td>
	<%-- 												<td class="text-center" style="display:none" id="bank${order.count}">${dataList.DPTRIBANK}</td> --%>
	<%-- 												<td class="text-center" style="display:none" id="dptracno${order.count}">${dataList.DPTRACNO}</td> --%>
													</tr>
												</c:forEach>
											</c:if>
										</tbody>
					        		</table>
					        	</div>					        
						</div>
						<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /></p><!-- 說明 -->
							<li><spring:message code="LB.Common_Acct_Setting_P1_D1"/></li>
							<li><font style="color:red"><spring:message code="LB.Common_Acct_Setting_P1_D2"/></font></li>
						</ol>
				</form>
			</section>
			<!-- 		main-content END -->
		</main>
	<%@ include file="../index/footer.jsp"%>
	
	</body>
</html>