<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=big5">
</HEAD>
<body>
<center>
<br>　</center>
<H3 ALIGN=CENTER></H>
<H3 ALIGN="CENTER"> 外 幣 利 率 查 詢    </H>
<c:choose>
<c:when test="${N022data.data.RECSIZE=='0'}">
查無資料!!!
</c:when>
<c:when test="${N022data.data.RECSIZE!='0'}">
<TABLE BORDER="2" cellspacing="0">
	<TR><TH COLSPAN="22">  查  詢  時  間:${N022data.data.CMQTIME}</th></tr> 
	<TR Align="left"><TH COLSPAN="22">資料更新日期：${N022data.data.UPDATEDATE}</th></tr>
	<TR><TH ROWSPAN="3">幣別</th><TH COLSPAN="21">存款期間</th></tr>
	<TR><TH ROWSPAN="2">代號</th><TH ROWSPAN="2">活期存款</th><TH ROWSPAN="2">1週</th><TH ROWSPAN="2">2週</th><TH ROWSPAN="2">3週</th><TH COLSPAN="2">1個月</th><TH COLSPAN="2">2個月</th><TH COLSPAN="2">3個月</th><TH COLSPAN="2">4個月</th><TH COLSPAN="2">5個月</th><TH COLSPAN="2">6個月</th><TH COLSPAN="2">9個月</th><TH COLSPAN="2">12個月</th></tr>
	<TR><TH>固定</th><TH>機動</th><TH>固定</th><TH>機動</th><TH>固定</th><TH>機動</th><TH>固定</th><TH>機動</th><TH>固定</th><TH>機動</th><TH>固定</th><TH>機動</th><TH>固定</th><TH>機動</th><TH>固定</th><TH>機動</th></tr>
	<c:forEach var="dataList" items="${N022data.data.REC}" varStatus="data">
	<TR BGCOLOR="${dataList.COLOR}">
		<TD Align="left"><FONT>${dataList.CRYNAME}</FONT>&nbsp;</td>
		<TD Align="left"><FONT>${dataList.CRY}</FONT>&nbsp;</td>
		<TD Align="right"><FONT>${dataList.ITR1}</FONT>&nbsp;</td>
		<TD Align="right"><FONT>${dataList.ITR2}</FONT>&nbsp;</td>
		<TD Align="right"><FONT>${dataList.ITR3}</FONT>&nbsp;</td>
		<TD Align="right"><FONT>${dataList.ITR4}</FONT>&nbsp;</td>
		<TD Align="right"><FONT>${dataList.ITR5}</FONT>&nbsp;</td>
		<TD Align="right"><FONT>${dataList.ITR6}</FONT>&nbsp;</td>
		<TD Align="right"><FONT>${dataList.ITR7}</FONT>&nbsp;</td>
		<TD Align="right"><FONT>${dataList.ITR8}</FONT>&nbsp;</td>
		<TD Align="right"><FONT>${dataList.ITR9}</FONT>&nbsp;</td>
		<TD Align="right"><FONT>${dataList.ITR10}</FONT>&nbsp;</td>
		<TD Align="right"><FONT>${dataList.ITR11}</FONT>&nbsp;</td>
		<TD Align="right"><FONT>${dataList.ITR12}</FONT>&nbsp;</td>
		<TD Align="right"><FONT>${dataList.ITR13}</FONT>&nbsp;</td>
		<TD Align="right"><FONT>${dataList.ITR14}</FONT>&nbsp;</td>
		<TD Align="right"><FONT>${dataList.ITR15}</FONT>&nbsp;</td>
		<TD Align="right"><FONT>${dataList.ITR16}</FONT>&nbsp;</td>
		<TD Align="right"><FONT>${dataList.ITR17}</FONT>&nbsp;</td>
		<TD Align="right"><FONT>${dataList.ITR18}</FONT>&nbsp;</td>
		<TD Align="right"><FONT>${dataList.ITR19}</FONT>&nbsp;</td>
		<TD Align="right"><FONT>${dataList.ITR20}</FONT>&nbsp;</td>
	</c:forEach>
	</TABLE>
</c:when>
</c:choose>
<P>
<P>
<P>
<P>
<H3 ALIGN=CENTER> 國 際 金 融 業 務 分 行 外 匯 存 款 利 率 查 詢    </H>
<c:choose>
	<c:when test="${N023data.data.RECSIZE==0}">
	查無資料!!!
	</c:when>
	<c:when test="${N023data.data.RECSIZE!=0}">
		<TABLE BORDER="2" cellspacing="0">
		<TR><TH COLSPAN="15">  查  詢  時  間:${N023data.data.CMQTIME}</th></tr>
		<TR><TH ROWSPAN="2">幣別</th><TH COLSPAN="14">存款期間</th></tr>
		<TR><TH>代號</th><TH>活期存款<TH>1週</th><TH>2週</th><TH>3週</th><TH>1個月</th><TH>2個月</th><TH>3個月</th><TH>4個月</th><TH>5個月</th><TH>6個月</th><TH>9個月</th><TH>12個月</th></tr>
		<c:forEach var="dataList1" items="${N023data.data.REC}" varStatus="data">
			<TR BGCOLOR="${dataList1.COLOR}">
			<TD Align="left"><FONT>${dataList1.CRYNAME}</FONT>&nbsp;</td>
			<TD Align="left"><FONT>${dataList1.CRY}</FONT>&nbsp;</td>
			<TD Align="right"><FONT>${dataList1.ITR1}</FONT>&nbsp;</td>
			<TD Align="right"><FONT>${dataList1.ITR2}</FONT>&nbsp;</td>
			<TD Align="right"><FONT>${dataList1.ITR3}</FONT>&nbsp;</td>
			<TD Align="right"><FONT>${dataList1.ITR4}</FONT>&nbsp;</td>
			<TD Align="right"><FONT>${dataList1.ITR5}</FONT>&nbsp;</td>
			<TD Align="right"><FONT>${dataList1.ITR6}</FONT>&nbsp;</td>
			<TD Align="right"><FONT>${dataList1.ITR7}</FONT>&nbsp;</td>
			<TD Align="right"><FONT>${dataList1.ITR8}</FONT>&nbsp;</td>
			<TD Align="right"><FONT>${dataList1.ITR9}</FONT>&nbsp;</td>
			<TD Align="right"><FONT>${dataList1.ITR10}</FONT>&nbsp;</td>
			<TD Align="right"><FONT>${dataList1.ITR11}</FONT>&nbsp;</td>
			<TD Align="right"><FONT>${dataList1.ITR12}</FONT>&nbsp;</td>
		</c:forEach>
		</TABLE>
	</c:when>
</c:choose>
<form>
<center><table border=0>
<tr><td><input type='button' value='回上一頁' OnClick='history.back()'></td><td><input type='button' value='登出' OnClick='javascript:self.close()'></td></tr></table></center>
</form>
</BODY>
</HTML>