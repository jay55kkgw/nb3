<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
<br/><br/>
<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
<br/><br/><br/>
<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
<br/><br/><br/>
<label><spring:message code="LB.Inquiry_time" />：</label><label>${CMQTIME}</label>
<br/><br/>
<label><spring:message code="LB.Inquiry_period_1" />：</label><label>${CMQDATE}</label>
<br/><br/>
<label><spring:message code="LB.Total_records" />：</label><label>${COUNT}　<spring:message code="LB.Rows" /></label>
<br/><br/>
<c:forEach var="labelListMap" items="${print_datalistmap_data}">
<table class="print">
	<tr>
		<td style="text-align: center">
			<spring:message code="LB.Account" />
		</td>
		<td colspan="8">${labelListMap.ACN}</td>
	</tr>
	<tr>
		<td style="text-align:center"><spring:message code="LB.Currency" /></td>
		<td style="text-align:center"><spring:message code="LB.Change_date" /></td>
		<td style="text-align:center"><spring:message code="LB.Summary" /></td>
		<td style="text-align:center"><spring:message code="LB.Withdrawal_amount" /></td>
		<td style="text-align:center"><spring:message code="LB.Deposit_amount" /></td>
		<td style="text-align:center"><spring:message code="LB.Account_balance" /></td>
		<td style="text-align:center"><spring:message code="LB.Data_content" /></td>
		<td style="text-align:center"><spring:message code="LB.Note" /></td>
		<td style="text-align:center"><spring:message code="LB.Trading_time" /></td>
	</tr>
	<c:forEach items="${labelListMap.rowListMap}" var="map">
	<tr>
		<td style="text-align:center">${map.CUID}</td>
		<td style="text-align:center">${map.display_TXDATE}</td>
		<td style="text-align:center">${map.MEMO}</td>
		<td style="text-align:right">${map.display_FXPAYAMT}</td>
		<td style="text-align:right">${map.display_FXRECAMT}</td>
		<td style="text-align:right">${map.display_BALANCE}</td>
		<td class="text-center">${map.DATA12}</td>
		<td style="text-align:center">${map.display_TRNTIME}</td>
		<td style="text-align:center">${map.TRNTIME}</td>
	</tr>
	</c:forEach>
</table>
</c:forEach>
<br/><br/>
	<div class="text-left">
		<spring:message code="LB.Description_of_page" /> 
		<ol class="list-decimal text-left">
			<li><spring:message code="LB.F_demand_deposit_P2_D1" /></li>
		</ol>
	</div>
</body>
</html>