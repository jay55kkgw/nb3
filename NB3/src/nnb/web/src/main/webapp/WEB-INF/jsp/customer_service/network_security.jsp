<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
</head>

<body>

						<c:choose>
							<c:when test="${pageContext.response.locale=='zh_CN'}">
									<%@ include file="network_security_zh_CN.jsp"%>
							</c:when>
							<c:when test="${pageContext.response.locale=='en'}">
									<%@ include file="network_security_en.jsp"%>									
							</c:when>
							<c:otherwise>
									<%@ include file="network_security_zh_TW.jsp"%>		
							</c:otherwise>
						</c:choose>	

	
</body>
</html>
