<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<!-- 元件驗證身分JS -->
<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>

<script type="text/javascript">


	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 50);
		// 開始跑下拉選單並完成畫面
		setTimeout("init()", 400);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
		//initFootable();
		setTimeout("initDataTable()",100);
	});

	function init(){
		
		// 交易類別change 事件
		changeFgtxway();
		
		$("#formId").validationEngine({
			binded: false,
			promptPosition: "inline"
		});
		
		//確定 
		$("#CMSUBMIT").click(function() {
			
		    if (!$('#formId').validationEngine('validate')) {
		        e.preventDefault();
		    } else {
		    	$("#formId").validationEngine('detach');
					initBlockUI();
// 					$("#formId").attr("action","${__ctx}/ONLINE/SERVING/predesignated_account_cancellation_r");
// 		  			$("#formId").submit(); 
					// 通過表單驗證準備送出
					processQuery();
		    }
		});
		
	}
	
	
	// 通過表單驗證準備送出
	function processQuery(){
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
		console.log("fgtxway: " + fgtxway);
		// 交易機制選項
		switch(fgtxway) {
			case '0':
				// SSL
				// 交易密碼sha1
				$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
				// 清除SSL密碼欄，避免儲存或傳送明碼到後端
				$('#CMPASSWORD').val("");
				// 遮罩
	         	initBlockUI();
	            $("#formId").submit();
				unBlockUI(initBlockId);
	         	
				break;
			case '1':
				// IKEY
				useIKey();
				unBlockUI(initBlockId);
				break;
				
			case '7'://IDGATE認證
				showIdgateBlock();
				break;
				
			default:
				//alert("<spring:message code= "LB.Alert001" />");
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert001' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
		}
	}
	
	// 交易類別change 事件
	function changeFgtxway(){
		$('input[type=radio][name=FGTXWAY]').change(function(){
			console.log(this.value);
			if(this.value=='0'){
				$("#CMPASSWORD").addClass("validate[required]")
			}else if(this.value=='1'){
				$("#CMPASSWORD").removeClass("validate[required]");
			}else if(this.value=='7'){
				$("#CMPASSWORD").removeClass("validate[required]");
			}
		});
	}
	
</script>
</head>
<body>
	<!-- 	晶片金融卡 -->
	<%@ include file="../component/trading_component.jsp"%>
	<!--   IDGATE -->
    <%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 取消約定轉入帳號     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0243" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code="LB.D0243" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="${__ctx}/ONLINE/SERVING/predesignated_account_cancellation_r">
				<div class="main-content-block row">
					<div class="col-12">
					<c:set var="dataSet" value="${predesignated_account_cancellation.data.dataSet}" />
					<div class="ttb-input-block">
						
							<ul class="ttb-result-list">
								<li>
									<!-- 帳號 -->
									<h3><spring:message code="LB.Account"/>：</h3>
									<p>
	               		  			
									${predesignated_account_cancellation.data.ACN}
									</p>
								</li>
								<li>
									<!-- 已預約資料總數 -->
									<h3><spring:message code="LB.D0251"/>：</h3>
									<p>
																	
									${dataSet.COUNT}
									<spring:message code="LB.Rows" />
	               					</p>
								</li>
							</ul>
							
						<c:if test="${dataSet.COUNT != '0'}">
							
<!-- 							台幣預約Table -->
							<c:if test="${predesignated_account_cancellation.data.twAcn != null}">
	                        <ul class="ttb-result-list">
	                            <li>
	                                <h3><spring:message code="LB.Report_name" /></h3>
	                                <p>
										<c:if test="${predesignated_account_cancellation.data.isToday > '0'}">
											<spring:message code="LB.X0270" /><FONT COLOR="RED">（<spring:message code="LB.X0271" />）</FONT>
										</c:if>
										<c:if test="${predesignated_account_cancellation.data.isToday == '0'}">
											<spring:message code="LB.X0272" /><FONT COLOR="RED">（<spring:message code="LB.X0273" />）</FONT>
										</c:if>
									</p>
	                            </li>
	                        </ul>
							<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
								<thead>
<%-- 								<tr> --%>
<%-- 									<th><spring:message code="LB.Report_name" /></th> --%>
<%-- 									<c:if test="${predesignated_account_cancellation.data.isToday > '0'}"> --%>
<%-- 									<th><spring:message code="LB.X0270" /><FONT COLOR="RED">（<spring:message code="LB.X0271" />）</FONT></th> --%>
<%-- 									</c:if> --%>
<%-- 									<c:if test="${predesignated_account_cancellation.data.isToday == '0'}"> --%>
<%-- 									<th colspan="11"><spring:message code="LB.X0272" /><FONT COLOR="RED">（<spring:message code="LB.X0273" />）</FONT></th> --%>
<%-- 									</c:if> --%>
<%-- 								</tr> --%>
									<tr>
		<!-- 							預約編號 -->
										<th><spring:message code="LB.Booking_number" /></th>
		<!-- 								週期 -->
										<th data-breakpoints="xs sm"><spring:message code="LB.Period" /></th>
		<!-- 								生效日/截止日 -->
										<th data-breakpoints="xs sm">
											<spring:message code="LB.Effective_date" />
											</br>
											<spring:message code="LB.Deadline" />
										</th>
		<!-- 								下次轉帳日 -->
										<th data-breakpoints="xs sm"><spring:message code="LB.Next_transfer_date" /></th>
		<!-- 								轉出帳號 -->
										<th data-breakpoints="xs sm"><spring:message code="LB.Payers_account_no" /></th>
		<!-- 								轉入帳號/繳費稅代號 -->
										<th data-breakpoints="xs sm">
											<spring:message code="LB.Payees_account_no" />/
											</br>
											<spring:message code="LB.Pay_taxes_fee_code" />
										</th>
		<!-- 								轉帳金額 -->
										<th><spring:message code="LB.Amount" /></th>
		<!-- 								交易類別 -->
										<th><spring:message code="LB.Transaction_type" /></th>
		<!-- 								備註 -->
										<th data-breakpoints="xs sm"><spring:message code="LB.Note" /></th>
		<!-- 								交易機制 -->
										<th><spring:message code="LB.Transaction_security_mechanism" /></th>
									</tr>
								</thead>								
								<tbody>												
									<c:forEach var="dataList" items="${predesignated_account_cancellation.data.REC}">
										<tr>            	
							                <td>${dataList.DPSCHNO}</td>
							                <td>${dataList.DPPERMTDATE}</td>                
							                <td>
							                	${dataList.DPFDATE} 
							                	<c:if test="${dataList.DPTDATE != ''}">
							                	<br>
							                	${dataList.DPTDATE}
							                	</c:if>
							                </td>				          	               
							                <td>${dataList.DPNEXTDATE}</td>                
							                <td>${dataList.DPWDAC}</td>                
							                <td>
							                	${dataList.DPSVBH}
							                	<br>
												${dataList.DPSVAC}						                
							                </td>
							                <td style="text-align: right">${dataList.DPTXAMTS}</td>
							                <td>${dataList.TXTYPE}</td>
							                <td>${dataList.DPTXMEMO}</td>
							                <td>${dataList.DPTXCODES}</td>
							        	</tr>	
									</c:forEach>					                   
								</tbody>
							</table>
						</c:if>
						
						<!-- 						外幣預約Table -->
						<c:if test="${predesignated_account_cancellation.data.fxAcn != null}">
						<table class="stripe table-striped ttb-table dtable" data-toggle-column="first" data-sorting="true">
						<thead>
							<tr>
								<th><spring:message code="LB.Report_name" /></th>
								<c:if test="${predesignated_account_cancellation.data.isToday > '0'}">
								<th><spring:message code="LB.X0274" /><FONT COLOR="RED">（<spring:message code="LB.X0271" />）</FONT></th>
								</c:if>
								<c:if test="${predesignated_account_cancellation.data.isToday == '0'}">
								<th colspan="11"><spring:message code="LB.X0275" /><FONT COLOR="RED">（<spring:message code="LB.X0273" />）</FONT></th>
								</c:if>
							</tr>
							<tr>
<!-- 							預約編號 -->
								<th data-breakpoints="xs sm"><spring:message code="LB.Booking_number" /></th>
<!-- 							週期 -->
								<th><spring:message code="LB.Period" /></th>
<!-- 							生效日/截止日 -->
								<th>
									<spring:message code="LB.Effective_date" />
									</br>
									<spring:message code="LB.Deadline" />
								</th>
<!-- 							下次轉帳日								 -->
								<th data-breakpoints="xs sm"><spring:message code="LB.Next_transfer_date" /></th>
<!-- 							轉出帳號 -->
								<th data-breakpoints="xs sm"><spring:message code="LB.Payers_account_no" /></th>
<!-- 							轉出金額 -->
								<th><spring:message code="LB.Deducted" /></th>
<!-- 							銀行名稱/轉入帳號 -->
								<th data-breakpoints="xs sm">
									<spring:message code="LB.Payees_account_no" />
									</br>
									<spring:message code="LB.Bank_name" />
								</th>
<!-- 							轉入金額 -->
								<th><spring:message code="LB.Buy" /></th>
<!-- 							交易類別 -->
								<th><spring:message code="LB.Transaction_type" /></th>
<!-- 							備註 -->
								<th data-breakpoints="xs sm"><spring:message code="LB.Note" /></th>
<!-- 							交易機制 -->
								<th data-breakpoints="xs sm"><spring:message code="LB.Transaction_security_mechanism" /></th>
							</tr>
						</thead>	
						<c:forEach var="listData" items="${predesignated_account_cancellation.data.REC}">													
						<tbody>
							<tr>            	
				                <td>${listData.FXSCHNO}</td>
				                <td>${listData.FXPERMTDATE}</td>                
				                <td>
				                	${listData.FXFDATE}							                	
				                	<c:if test="${listData.FXTDATE != ''}">
						            <br>${listData.FXTDATE}
						            </c:if>
						        </td>				          	               
				                <td>${listData.FXNEXTDATE}</td>                
				                <td>${listData.FXWDAC}</td>
				                <td>${listData.FXWDCURR}</br>${listData.FXWDAMTS}</td>  
				                <td>
				                	${listData.FXSVBH}
						            <br>
				                	${listData.FXSVAC}
				                </td>                
				                <td>${listData.FXSVCURR}</br>${listData.FXSVAMTS}</td>
				                <td>${listData.TXTYPE}</td>
				                <td>${listData.FXTXMEMO}</td>
				                <td>${listData.FXTXCODES}</td>
				           </tr>        
				       	</tbody>
				       	</c:forEach>
						</table>
						</c:if>
<!-- 						筆數不為零 -->
					</c:if>
						
						
						<!-- 交易機制 -->					
						<div class="ttb-input-item row">
							<span class="input-title">
								<label>
									<!-- 交易機制 -->
									<spring:message code="LB.Transaction_security_mechanism" />
								</label>
							</span>
							<!-- 交易機制選項 -->
							<span class="input-block">
								<div class="ttb-input">
									<label class="radio-block">
										<!-- 交易密碼(SSL) -->
										<spring:message code="LB.SSL_password" />
										 <input type="radio" name="FGTXWAY" checked="checked" value="0">
										<span class="ttb-radio"></span>
									</label>
								</div>
								<div class="ttb-input">
									<!--請輸入密碼 -->
									<spring:message code="LB.Please_enter_password" var="pleaseEnterPin"/>
									<input type="password" id="CMPASSWORD" name="CMPASSWORD" class="text-input validate[required]" maxlength="8" placeholder="${pleaseEnterPin}">
								</div>									
								<!-- 電子簽章(載具i-key) -->
								<!-- 使用者是否可以使用IKEY -->
								<c:if test = "${sessionScope.isikeyuser}">
									<div class="ttb-input">
										<label class="radio-block">
											<spring:message code="LB.Electronic_signature" />
											<input type="radio" name="FGTXWAY" id="CMIKEY" value="1">
											<span class="ttb-radio"></span>
										</label>
									</div>
								</c:if>
								<div class="ttb-input" name="idgate_group" style="display: none">
									<label class="radio-block">
										裝置推播認證(請確認您的行動裝置網路連線是否正常，及推播功能是否已開啟)
										<input type="radio" id="IDGATE" name="FGTXWAY" value="7"> 
										<span class="ttb-radio"></span>
									</label>
								</div>
								</span>
						</div>
						<!-- block -->					
						</div>
						<!-- 確定 -->
                        <input type="button" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm"/>" >
					</div>
				</div>
					<div class="text-left">
						<ol class="list-decimal description-list">
							<p><spring:message code="LB.Description_of_page" /></p>
							<li><font color="red"><B><spring:message code="LB.Predesignated_Account_Cancellation_P2_D1" /></B></font></li>
							<li><font color="red"><B><spring:message code="LB.Predesignated_Account_Cancellation_P2_D2" /></B></font></li>
						</ol>
					</div>					
				<!-- 			晶片金融卡 -->
				<input type="hidden" id="jsondc" name="jsondc" value='${predesignated_account_cancellation.data.jsondc}'>
				<input type="hidden" id="ISSUER" name="ISSUER" value="">
				<input type="hidden" id="ACNNO" name="ACNNO" value="">
				<input type="hidden" id="TRMID" name="TRMID" value="">
				<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
				<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
				<input type="hidden" id="TAC" name="TAC" value="">
				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
				<input type="hidden" id="PINNEW" name="PINNEW" value="">
				<input type="hidden" id="TYPE" name="TYPE" value="01">
<!-- 				<input type="hidden" id="FLAG" name="FLAG" value="1"> -->
				<input type="hidden" id="COUNT" name="COUNT" value="${predesignated_account_cancellation.data.COUNT}">
				<input type="hidden" id="ACN" name="ACN" value="${predesignated_account_cancellation.data.ACN}">
				<input type="hidden" id="BNKCOD" name="BNKCOD" value="${predesignated_account_cancellation.data.BANKCOD}">
				<input type="hidden" id="CCY" name="CCY" value="${predesignated_account_cancellation.data.CCY}">
				</form>
			</section>
			</main>
		<!-- 		main-content END -->
		</div>
		<!-- 	content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>