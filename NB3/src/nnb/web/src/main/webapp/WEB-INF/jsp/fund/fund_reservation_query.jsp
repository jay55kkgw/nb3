<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});

	// 畫面初始化
	function init() {
		initFootable();
		$('#but_Agree').prop('checked', false);
	}
	
	// 確認鍵 Click
	function processQuery()
	{
		console.log("submit~~");
		// 遮罩
     	initBlockUI();
		var main = document.getElementById("formId");
		var fm=document.forms[0];
	
		if (fm.reportingLoss[0].checked==true ){
	 		main.action = "${__ctx}/FUND/QUERY/fund_reservation_purchase";
		}else if (fm.reportingLoss[1].checked==true ){
	 		main.action = "${__ctx}/FUND/QUERY/fund_reservation_conversion";
		}else if (fm.reportingLoss[2].checked==true ){
	 		main.action = "${__ctx}/FUND/QUERY/fund_reservation_redemption";
		}
	  	main.submit();
	  	return false;
	}

</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Funds" /></li>
    <!-- 基金查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0902" /></li>
    <!-- 預約交易查詢/取消     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0375" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.X0375" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form method="post" id="formId" action="" onSubmit="return processQuery()">
					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-input-block">
    							<div class="ttb-input-item row">
									<span class="input-title" > <label><spring:message code="LB.Inquiry_time" /></label></span> 
									<span class="input-block">
										<jsp:useBean id="now" class="java.util.Date" />
										<fmt:formatDate value="${now}" type="both" dateStyle="medium" timeStyle="medium"  pattern="yyyy/MM/dd HH:mm:ss" var="today"/>
										${today}
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title" > <label><spring:message code="LB.W1060" /></label></span> 
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block"> <spring:message code="LB.W1061" />
												<input type="radio" name="reportingLoss" value="0" onclick="processQuery()"/> 
												<span class="ttb-radio"></span>
											</label>
											<label class="radio-block"> <spring:message code="LB.W1062" />
												<input type="radio" name="reportingLoss" value="1" onclick="processQuery()"/> 
												<span class="ttb-radio"></span>
											</label>
											<label class="radio-block"> <spring:message code="LB.W1063" />
												<input type="radio" name="reportingLoss" value="2" onclick="processQuery()"/> 
												<span class="ttb-radio"></span>
											</label>
										</div>
									</span>
								</div>
  							</div>
  							<div style="text-align:center;">
  								<input type="hidden" name="ADOPID" value="N381">  
  								<input type="hidden" name="ACN1" value="${fund_reservation_query.data.ACN1}">	
  								<input type="hidden" name="ACN2" value="${fund_reservation_query.data.ACN2}">
  								<input type="hidden" name="NAME" value="${fund_reservation_query.data.NAME}">	
  							</div>
  						</div>
  					</div>
  				</form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>