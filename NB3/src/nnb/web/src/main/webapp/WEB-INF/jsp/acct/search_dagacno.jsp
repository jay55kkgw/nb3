<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>


<script type="text/javascript">

	$(document).ready(function(){
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()",10);
		// 開始查詢資料並完成畫面
		setTimeout("init()",20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)",500);	
		setTimeout("initDataTable()",100);
	});
	
	function init(){
		//initFootable();
		$("#printbtn").click(function(){
			var params = {
					"jspTemplateName":"search_dagacno_print",
					"jspTitle":"<spring:message code= "LB.X1700" />",
					"COUNT":"${searchDagacno.data.COUNT}"
				};
				openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});		
	}

</script>
<title><spring:message code="LB.Title" /></title>
</head>
<body>
	<main class="col-12"> 
		<!-- 主頁內容  -->
		<section id="main-content" class="container">
			<form method="post" id="formId" action="">
				<!--約定轉入帳號查詢 -->
				<h2><spring:message code= "LB.X1700" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<div class="main-content-block row">
					<div class="col-12">
					<ul class="ttb-result-list"></ul>
					<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
<%-- 					<thead> --%>
<%-- 						<tr> --%>
<%-- 							<th colspan="4" style="text-align: right"> --%>
<%-- 								<spring:message code="LB.Total_records" /> ：${searchDagacno.data.COUNT}  --%>
<%-- 								<spring:message code="LB.Rows" />  --%>
<%-- 							</th> --%>
<%-- 						</tr> --%>
<%-- 					</thead> --%>
					<thead>
						<tr>
							<!-- 銀行名稱 -->

							<th><spring:message code= "LB.Bank_name" /></th>
							<!-- 約定轉入帳號 -->

							<th><spring:message code= "LB.D0227" /></th>
							<!-- 好記名稱 -->

							<th><spring:message code= "LB.Favorite_name" /></th>
							<!-- 臨櫃/線上約定帳號 -->

							<th><spring:message code= "LB.X1687" /></th>
						</tr>
					</thead>
					<c:if test="${empty searchDagacno.data.REC}">
						<tbody>
							<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							</tr>
						</tbody>
					</c:if>
					<c:if test="${not empty searchDagacno.data.REC}">
						<tbody>					
						<c:forEach var="dataList" items="${searchDagacno.data.REC}" >
						
							<tr>
								<td>${dataList.BNKCOD}</td>
								<td>${dataList.ACN}</td>
								<td><c:out value='${dataList.DPGONAME}' /></td>
<%-- 								<td>${dataList.TRAFLAG}</td> --%>
									<c:choose>
									    <c:when test="${empty dataList.TRAFLAG}">
									    	<td></td>
									    </c:when>
									    <c:when test="${dataList.TRAFLAG == '1'}">
									    	<td><spring:message code= "LB.X1688" /></td>
									    </c:when>
									    <c:otherwise>
									        <td><spring:message code= "LB.X1689" /></td>
									    </c:otherwise>
									</c:choose>
							</tr>
						
						</c:forEach>
						</tbody>
					</c:if>
					</table>
					<!-- 列印 -->
						<input type="button" class="ttb-button btn-flat-orange" id = "printbtn" value="<spring:message code="LB.Print" />"/>
					</div>
				</div>
			</form>	
		</section>
	</main> <!-- 		main-content END -->
</body>
</html>