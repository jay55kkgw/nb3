<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust:exact">
	<br/><br/>
	<div style="text-align:center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/>
	</div>
	<br/><br/><br/>
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<table class="print">
		<tr>
			<td><spring:message code="LB.Trading_time"/></td>
		 	<td>${CMQTIME}</td>
		</tr>
		<tr>
  			<td><spring:message code="LB.W1067" /></td>
			<td>${FDINVTYPEChinese}</td>
		</tr>
		<tr>
  			<td><spring:message code="LB.W0946" /></td>
			<td>${INVTYPEChinese}</td>
		</tr>
		<tr>
  			<td><spring:message code="LB.D0173" /></td>
			<td>${TYPEChinese}</td>
		</tr>
		<tr>
			<td><spring:message code="LB.Id_no"/></td>
		 	<td>${hiddenCUSIDN}</td>
		</tr>
		<tr>
			<td><spring:message code="LB.Name"/></td>
		 	<td>${hiddenNAME}</td>
		</tr>
		<tr>
  			<td><spring:message code="LB.W0025" /></td>
			<td>（${TRANSCODE}）${FUNDLNAME}</td>
		</tr>
		<tr>
  			<td><spring:message code="LB.W1073" /></td>
			<td>${RISK}</td>
		</tr>
		<tr>
  			<td><spring:message code="LB.W1074" /></td>
			<td>${ADCCYNAME}${AMT3Format}</td>
		</tr>
		<tr>
  			<td><spring:message code="LB.W1034" /></td>
			<td>${FCAFEEFormat}％</td>
		</tr>
		<tr>
  			<td><spring:message code="LB.D0507" /></td>
			<td>${ADCCYNAME}${FCA2Format}</td>
		</tr>
		<tr>
			<td><spring:message code="LB.X0406" /></td>
			<td>${ADCCYNAME}${AMT5Format}</td>
		</tr>
		<tr>
			<td><spring:message code="LB.W1046" /></td>
			<td>
				${FUNDACN}
			</td>
		</tr>
		<tr>
			<td><spring:message code="LB.D0127" /></td>
  			<td>${TRADEDATEFormat}</td>
		</tr>
		<tr>
			<td><spring:message code="LB.W1592" /></td>
  			<td>
  				<c:if test="${PAYTYPE == '3'}">
					<c:if test="${PAYDAY4 != '00'}">
						${PAYDAY4}<spring:message code="LB.Day" />&nbsp;
					</c:if>
					<c:if test="${PAYDAY1 != '00'}">
						${PAYDAY1}<spring:message code="LB.Day" />&nbsp;
					</c:if>
					<c:if test="${PAYDAY5 != '00'}">
						${PAYDAY5}<spring:message code="LB.Day" />&nbsp;
					</c:if>
					<c:if test="${PAYDAY2 != '00'}">
						${PAYDAY2}<spring:message code="LB.Day" />&nbsp;
					</c:if>
					<c:if test="${PAYDAY6 != '00'}">
						${PAYDAY6}<spring:message code="LB.Day" />&nbsp;
					</c:if>
					<c:if test="${PAYDAY3 != '00'}">
						${PAYDAY3}<spring:message code="LB.Day" />&nbsp;
					</c:if>
				</c:if>
				<c:if test="${PAYTYPE == '1' || PAYTYPE == '2'}">
					<c:if test="${PAYDAY1 != '00'}">
						${PAYDAY1}<spring:message code="LB.Day" />&nbsp;
					</c:if>
					<c:if test="${PAYDAY2 != '00'}">
						${PAYDAY2}<spring:message code="LB.Day" />&nbsp;
					</c:if>
					<c:if test="${PAYDAY3 != '00'}">
						${PAYDAY3}<spring:message code="LB.Day" />&nbsp;
					</c:if>
					<c:if test="${PAYDAY4 != '00'}">
						${PAYDAY4}<spring:message code="LB.Day" />&nbsp;
					</c:if>
					<c:if test="${PAYDAY5 != '00'}">
						${PAYDAY5}<spring:message code="LB.Day" />&nbsp;
					</c:if>
					<c:if test="${PAYDAY6 != '00'}">
						${PAYDAY6}<spring:message code="LB.Day" />&nbsp;
					</c:if>
					<c:if test="${PAYDAY7 != '00'}">
						${PAYDAY7}<spring:message code="LB.Day" />&nbsp;
					</c:if>
					<c:if test="${PAYDAY8 != '00'}">
						${PAYDAY8}<spring:message code="LB.Day" />&nbsp;
					</c:if>
					<c:if test="${PAYDAY9 != '00'}">
						${PAYDAY9}<spring:message code="LB.Day" />&nbsp;
					</c:if>
				</c:if>
  			</td>
		</tr>
		<tr>
  			<td><spring:message code="LB.W1108" /></td>
			<td>${DBDATEFormat}</td>
		</tr>
		<tr>
  			<td><spring:message code="LB.W1075" /></td>
			<td>${YIELDInteger}％</td>
		</tr>
		<tr>
  			<td><spring:message code="LB.W1076" /></td>
			<td>－${STOPInteger}％</td>
		</tr>
	</table>
	<div>
		<p style="text-align:left;">
			<spring:message code="LB.X1234" />：<br/>
			<spring:message code="LB.Fund_Redeem_Data_P3_D1" /><br/>
			<spring:message code="LB.Regular_Investment_PPP2_D1" />
		</p>
		<p style="text-align:left;">
			<spring:message code="LB.X0400" />：<br/><br/>
			<li><spring:message code="LB.D1424" /><br/>「<spring:message code="LB.Funds_NonMenu" />」<spring:message code="LB.X0401" />→「<spring:message code="LB.Inquiry_Service" />」→「<spring:message code="LB.W0927" />」</li>
			<li><spring:message code="LB.X0402" />（https://www.tbb.com.tw/）<br/>「<spring:message code="LB.X0403" />」→「<spring:message code="LB.X0404" />」→「<spring:message code="LB.W0927" />」</li>
		</p>
	</div>
</body>
</html>