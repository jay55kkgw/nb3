<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()",10);
		// 開始查詢資料並完成畫面
		setTimeout("init()",20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)",500);	
	});
	
	function init(){
		initFootable();	
		//列印
		$("#printbtn").click(function(){
			var params = {
					"jspTemplateName":"gold_trading_apply_result_print",
					//線上申請黃金存摺網路交易
					"jspTitle":'<spring:message code="LB.W1687"/>',
					"CMQTIME":"${result_data.data.CMQTIME}",
					"ACN":"${input_data.ACN}",
					"SVACN":"${ input_data.SVACN }"
				};
				openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
		});				
		//上一頁按鈕
		$("#cmback").click(function() {
			var action = '${__ctx}/INDEX/index';
			$("form").attr("action", action);
			$("form").submit();
		});
	}
</script>
</head>
	<body>
		<!-- header     -->
		<header>
			<%@ include file="../index/header.jsp"%>
		</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 黃金存摺帳戶     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2273" /></li>
    <!-- 黃金存摺網路交易申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1687" /></li>
		</ol>
	</nav>

		<!-- menu、登出窗格 -->
		<div class="content row">
			<%@ include file="../index/menu.jsp"%>
			<!-- 	快速選單內容 -->
			<!-- content row END -->
			<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<!-- 	線上申請黃金存摺網路交易 -->
			    <h2><spring:message code="LB.W1687"/></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <div id="step-bar">
                    <ul>
						<li class="finished">注意事項與權益</li>
                        <li class="finished">設定帳戶</li>
                        <li class="finished">確認資料</li>
                        <li class="active">申請結果</li>
                    </ul>
                </div>
				<form id="formId" method="post">
				
				<!-- 顯示區  -->
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<div class="ttb-message">
									<p>申請結果</p>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.D1097"/></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${result_data.data.CMQTIME}</span>
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.D1090"/></h4>
										</label>
									</span>
									<span class="input-block">
										<c:forEach var="dataList" items="${result_data.data.REC}">
											<div class="ttb-input">
												<span>${dataList.GDACN}</span>
												<c:if test="${dataList.msgName == null}">
													<!-- 成功 -->
													<span class="application-correct-prompt"><i class="fa fa-check-circle"></i>申請成功</span>
												</c:if>
												<c:if test="${dataList.msgName != null}">
													<span class="application-error-prompt"><i class="fa fa-times-circle"></i>申請失敗<span class="application-error-code">${dataList.msgName}(代碼：${dataList.msgCode})</span></span>
												</c:if>
											</div>
										</c:forEach>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>網路交易指定新台幣帳戶</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span>${ input_data.SVACN }</span>
										</div>
									</span>
								</div>
							</div>
<!-- 							<ul class="ttb-result-list"> -->
<!-- 								<li> -->
<!-- 									查詢時間 -->
<%-- 									<h3><spring:message code="LB.D1097"/></h3> --%>
<!-- 									<p> -->
									
<%-- 										${result_data.data.CMQTIME} --%>
<!-- 									</p> -->
<!-- 								</li> -->
<!-- 								<li> -->
<!-- 									交易起迄日 -->
<%-- 									<h3><spring:message code="LB.D1090"/>：</h3> --%>
<!-- 									<p> -->
										
<%-- 										<c:if test="${input_data.ACN.length() > 11}"> --%>
<%-- 											<spring:message code="LB.All"/> --%>
<%-- 										</c:if> --%>
<%-- 										<c:if test="${input_data.ACN.length() <= 11}"> --%>
<%-- 											${ input_data.SVACN } --%>
<%-- 										</c:if> --%>
<!-- 									</p> -->
									
<!-- 								</li> -->
<!-- 							</ul> -->
<%-- 							<table class="table table-striped" data-toggle-column="first"> --%>
<%-- 								<thead> --%>
<%-- 									<tr> --%>
<!-- 										黃金存摺帳號 -->
<%-- 										<th><spring:message code="LB.D1090"/></th> --%>
<!-- 										指定網路銀行交易申購/回售新臺幣帳戶 -->
<%-- 										<th><spring:message code="LB.D1091"/></th> --%>
<!-- 										申請狀態 -->
<%-- 										<th><spring:message code="LB.D1098"/></th> --%>
<%-- 									</tr> --%>
<%-- 								</thead> --%>
<%-- 								<tbody> --%>
<%-- 									<c:forEach var="dataList" items="${result_data.data.REC}"> --%>
<%-- 										<tr> --%>
<%-- 											<td>${dataList.GDACN}</td> --%>
<%-- 											<td>${dataList.SVACN}</td> --%>
<%-- 											<td> --%>
<%-- 												<c:if test="${dataList.msgName == null}"> --%>
<!-- 													成功 -->
<%-- 													<spring:message code="LB.D1099"/> --%>
<%-- 												</c:if> --%>
<%-- 												<c:if test="${dataList.msgName != null}"> --%>
<%-- 													<spring:message code="LB.W0056"/><br><a herf="#" title='${dataList.msgName}'>${dataList.msgCode}</a> --%>
<%-- 												</c:if> --%>
<%-- 											</td> --%>
<%-- 										</tr> --%>
<%-- 									</c:forEach> --%>
<%-- 								</tbody> --%>
<%-- 							</table> --%>
							<!-- 列印鈕-->					
							<input type="button" class="ttb-button btn-flat-gray" id="printbtn" value="<spring:message code="LB.Print" />"/>	
							<input type="button" class="ttb-button btn-flat-orange" id="cmback" value="回首頁"/>	
							
						</div>
					</div>
				</form>
				<div class="description-list list-decimal">
				    <!-- 		說明： -->
					
				    <ol class="list-decimal text-left">
				    	<p><spring:message code="LB.Description_of_page"/></p>
				        <li>為保護您的安全，交易結束或離開電腦時，請務必將晶片金融卡抽離讀卡機或是將電子簽章 (載具i-key)拔除，並登出系統。</li>
				    </ol>
				</div>
				</section>
			</main>
			<!-- 		main-content END -->
			<!-- 	content row END -->
		</div>
		<%@ include file="../index/footer.jsp"%>
	</body>
</html>