<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ include file="../__import_js.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
	<head>
		<%@ include file="../__import_head_tag.jsp"%>
		<%@ include file="../__import_js.jsp"%>
		<script type="text/javascript">
			$(document).ready(function(){
				window.print();
			});
		</script>
	</head>
	<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
		<br><br>
		<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
		<br>
		<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
		<br/><br/>
		<!-- content row END -->
		<main class="col-12">	
		<label><spring:message code="LB.System_time" /> ：</label><label>${CMQTIME}</label>
		<br/><br/>
<%-- 				<table class="print" style="text-align:center;"> --%>
<%-- 					<tr> --%>
<!-- 						系統時間 -->
<%-- 					 	<td><spring:message code="LB.System_time_for_N816L"/></td> --%>
<%-- 						<td>${CMQTIME}</td> --%>
<%-- 					</tr> --%>
<%-- 				</table> --%>
				<!-- 金融卡掛失 -->
				<table class="print" style="text-align:center;">
					<thead>
						<tr>
							<!-- 狀態 -->
						 	<td><spring:message code="LB.Status"/></td>
							<!-- 卡片種類-->
							<td><spring:message code="LB.Card_kind"/></td>
							<!-- 帳號 -->
							<td><spring:message code="LB.Account"/></td>
							<!-- 存摺餘額-->
							<td><spring:message code="LB.X0456" /></td>
							<!-- 本日交換票 -->
							<td><spring:message code="LB.Exchange_ticket_today"/></td>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${dataListMap}" var="map">
							<tr>
								<td>
									<c:if test="${map.STATUS == '掛失成功'}">
            							<spring:message code="LB.Report_successful" />
            						</c:if>
            						<c:if test="${map.STATUS != '掛失成功'}">
            							${map.STATUS }
           							</c:if>
								</td>
								<td><spring:message code="${map.LOSTYPE}" /></td>
								<td>${map.ACN}</td>
								<td>${map.BALANCE}</td>
								<td>${map.CLR}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<br>
				<div class="text-left"  style="margin-left:20px;">
					<spring:message code="LB.Description_of_page" />
					<ol class="list-decimal text-left">
						<li><spring:message code="LB.Demand_Debitcard_Loss_P2_D1"/></li>
					</ol>
				</div>
		</main>
	</body>
</html>