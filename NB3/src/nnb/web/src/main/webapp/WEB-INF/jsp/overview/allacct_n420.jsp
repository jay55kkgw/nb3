<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script type="text/javascript">

	// column's title i18n

	
	// n420-臺幣定期性存款明細
	i18n['ACN'] = '<spring:message code="LB.Account" />'; // 帳號
	i18n['TYPE'] = '<spring:message code="LB.Deposit_type" /><hr><spring:message code="LB.Certificate_no" />'; // 存單種類
// 	i18n['FDPNUM'] = '<spring:message code="LB.Certificate_no" />'; // 存單號碼
	i18n['AMTFDP'] = '<spring:message code="LB.Certificate_amount" />'; // 存單金額
// 	i18n['ITR'] = '<spring:message code="LB.Interest_rate1" />'; // 利率(%)
	i18n['INTMTH'] = '<spring:message code="LB.Interest_calculation" /><hr><spring:message code="LB.Interest_rate1" />'; // 計息方式
	i18n['DPISDT'] = '<spring:message code="LB.Start_date" /><hr><spring:message code="LB.Maturity_date" />'; // 起存日
// 	i18n['DUEDAT'] = '<spring:message code="LB.Maturity_date" />'; // 到期日
	i18n['TSFACN'] = '<spring:message code="LB.Interest_transfer_to_account" />'; // 利息轉入帳號
	i18n['ILAZLFTM'] = '<spring:message code="LB.Automatic_number_of_rotations" /><hr><spring:message code="LB.Automatic_number_of_unrotated" />'; // 自動轉期已轉次數
// 	i18n['AUTXFTM'] = '<spring:message code="LB.Automatic_number_of_unrotated" />'; // 自動轉期未轉次數
	i18n['TYPE1'] = '<spring:message code="LB.Rollover_method" /><hr><spring:message code="LB.Non_Automatic_unrotated_application" />'; // 轉存方式
// 	i18n['TYPE2'] = '<spring:message code="LB.Non_Automatic_unrotated_application" />'; // 申請不轉期
	i18n['REMIND'] = '<spring:message code="LB.Note" /><hr><spring:message code="LB.Quick_Menu" />'; // 備註
	i18n['QM'] = '<spring:message code="LB.Quick_Menu" />';//快速選單
	
	// DataTable Ajax tables
	var n420_columns = [
		{ "data":"ACN", "title":i18n['ACN'] },
		{ "data":"TYPENAME", "title":i18n['TYPE'] },
// 		{ "data":"FDPNUM", "title":i18n['FDPNUM'] },
		{ "data":"AMTFDPFMT", "title":i18n['AMTFDP'] },
// 		{ "data":"ITR", "title":i18n['ITR'] },
		{ "data":"INTMTH", "title":i18n['INTMTH'] },
		{ "data":"DPISDT", "title":i18n['DPISDT'] },
// 		{ "data":"DUEDAT", "title":i18n['DUEDAT'] },
		{ "data":"TSFACN", "title":i18n['TSFACN'] },
		{ "data":"ILAZLFTM", "title":i18n['ILAZLFTM'] },
// 		{ "data":"AUTXFTM", "title":i18n['AUTXFTM'] },
		{ "data":"TYPE1", "title":i18n['TYPE1'] },
// 		{ "data":"TYPE2", "title":i18n['TYPE2'] },
		{ "data":"REMIND", "title":i18n['REMIND'] },
// 		{ "data":"QM","title":i18n['QM'] }
	];
	
	// 成功查詢資料
	//0置中 1置右
	var n420_align = [0,0,0,1,1,0,0,0,0,0,0,0,0,0,0];
	
	// Ajax_n420-臺幣定期性存款明細
	function getMyAssets420() {
		createLoadingBox("n420_BOX",'');
		$("#n420").show();
		$("#n420_title").show();
		uri = '${__ctx}' + "/OVERVIEW/allacct_n420aj";
		console.log("allacct_n420aj_uri: " + uri);
		fstop.getServerDataEx(uri, null, true, countAjax_n420, null, "n420");		
	}

	// Ajax_callback
	function countAjax_n420(data){
		// 資料處理後呈現畫面
		if (data.result) {
			n420_rows = data.data.REC;
			n420_rows.forEach(function(d,i){
				d["TYPENAME"]=d["TYPENAME"]+"<hr>"+d["FDPNUM"];
				d["INTMTH"]=d["INTMTH"]+"<hr>"+d["ITR"];
				d["DPISDT"]=d["DPISDT"]+"<hr>"+d["DUEDAT"];
				d["ILAZLFTM"]=d["ILAZLFTM"]+"<hr>"+d["AUTXFTM"];
				if(d["TYPE1"] == null || d["TYPE1"] == ""){
					d["TYPE1"] = "-";
				}
				if(d["TYPE2"] == null || d["TYPE2"] == ""){
					d["TYPE2"] = "-";
				}
				
				d["TYPE1"]=d["TYPE1"]+"<hr>"+d["TYPE2"];
				
				var options = [];
				
				if(d.OPTIONTYPE==2){
					//轉入臺幣零存整付按月存繳 
					options.push(new Option("<spring:message code='LB.Periodical_Deposits_And_Lumpsum_Payment_monthly' />", "installment_saving"));
				}
				else if(d.OPTIONTYPE==0){
					//臺幣綜存定存解約 
					options.push(new Option("<spring:message code='LB.NTD_Time_Deposit_Termination' />", "deposit_cancel"));
					//臺幣定存自動轉期申請/變更
					options.push(new Option("<spring:message code='LB.NTD_Time_Deposit_Automatic_Rollover_Application_Change' />", "renewal_apply"));
				}
				else{
					//臺幣定存自動轉期申請/變更 
					options.push(new Option("<spring:message code='LB.NTD_Time_Deposit_Automatic_Rollover_Application_Change' />", "renewal_apply"));
				}
				var qm = $("#actionBar").clone();
				qm.children()[0].id = "n110";
				qm.children()[0].name=i;
				qm.show();
				options.forEach(function(x){
					qm[0].children[0].options.add(x);
				});
				
				d["QM"]=qm.html();

				if(d["REMIND"] == true ){
					d["REMIND"] = '<a href="#"	class="origin" onclick="script:window.open(\'${__ctx}/Deposit.html\');"><spring:message code="LB.Note" /><spring:message code="LB.Description_of_page" /></a>';
				}
				else{
					if("${__i18n_locale}"=='en'){
						d["REMIND"] = "No note";
					}
					else if("${__i18n_locale}"=='zh_TW'){
						d["REMIND"] = "無備註";
					}
					else{
						d["REMIND"] = "无备注";
					}
				}
				d["REMIND"]=d["REMIND"]+"<hr>"+d["QM"];
				
			});
			var totalMsg = "<spring:message code='LB.Total_records'/>"+"&nbsp;"+ data.data.CMRECNUM + "&nbsp;"+"<spring:message code='LB.Rows'/>" + "<br>";
			totalMsg += "<spring:message code='LB.NTD'/>"+"&nbsp;"+data.data.TOTAMT+"&nbsp;" + "<spring:message code='LB.Dollar'/>";
			$("#tMsg").html(totalMsg);
		} 
		else {
			// 錯誤訊息
			n420_columns = [ {
					"data" : "MSGCODE",
					"title" : '<spring:message code="LB.Transaction_code" />'
				}, {
					"data" : "MESSAGE",
					"title" : '<spring:message code="LB.Transaction_message" />'
				}
			];
			n420_rows = [ {
				"MSGCODE" : data.msgCode,
				"MESSAGE" : data.message
			} ];
		}
		createTable("n420_BOX",n420_rows,n420_columns,n420_align);
	}
		
		

</script>
<!-- for selectionBar -->
			
<p id="n420_title" class="home-title" style="display: none"><spring:message code="LB.X2356" /></p>
<div id="n420" class="main-content-block" style="display:none">
	<div id="n420_BOX" style="display:none"></div>
	<form method="post" id="fastBarAction" action="">
					<input type="hidden" id="FDPNUM" name="FDPNUM" value="">
					<input type="hidden" id="ACN" name="ACN" value="">
					<input type="hidden" id="FGSELECT" name="FGSELECT" value="">
				</form>  
	<div class="text-left">
		<p id="tMsg"></p>
	</div>
</div>