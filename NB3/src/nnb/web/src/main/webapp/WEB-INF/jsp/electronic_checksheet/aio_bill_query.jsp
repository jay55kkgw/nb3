<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<script type="text/javascript">

	$(document).ready(function() {
        setTimeout("initDataTable()",100);
		
	});
 	function resend(cDate){
		

		$.ajax({
	        type :"POST",
	        url  : "${__ctx}/ELECTRONIC/CHECKSHEET/aio_bill_resend",
	        data : { 
	        	cDate : cDate
	        },
	        async: false ,
	        dataType: "json",
	        success : function(msg) {
				// 解遮罩
				setTimeout("unBlockUI(initBlockId)", 500);
        		console.log(msg.data);
        		if(msg.data.msgCode == "0"){
	        		if(msg.data.rtnCode == "0"){
	        			errorBlock(
	        					null, 
	        					null,
	        					["發送成功"], 
	        					'確定', 
	        					null
	        			);
	        		}else{
	        			errorBlock(
	        					null, 
	        					null,
	        					["發送失敗"], 
	        					'確定', 
	        					null
	        			);
	        		}
        		}else{
        			errorBlock(
        					null, 
        					null,
        					["發送失敗"], 
        					'確定', 
        					null
        			);
        		}
	        },
	        error: function(msg) {
				setTimeout("unBlockUI(initBlockId)", 500);
    			errorBlock(
    					null, 
    					null,
    					["發送失敗"], 
    					'確定', 
    					null
    			);
	        }
	    })
 	}
	
</script>

</script>

</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 電子對帳單     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1396" /></li>
    <!-- 電子對帳單申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page">綜合電子對帳單</li>
		</ol>
	</nav>


	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->


		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->

			<h2>綜合電子對帳單</h2><!-- 電子帳單申請 -->

			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="">
				<div class="main-content-block row">
					<div class="col-12">
						<ul class="ttb-result-list">
							<li>
								<!-- 查詢時間 -->
								<h3>查詢結果</h3>
                              	<p>${bs_result.data.rtnDesc }</p>
							</li>
						</ul>
						
						<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
							<thead>
								<tr>
									<th  data-title='帳單月份'>帳單月份</th>
									<th  data-title='電子郵件'>電子郵件</th>
									<th  data-title='重新發送'>重新發送</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="dataList" items="${bs_result.data.REC}">
									<tr>
						                <td class="text-center">${dataList.cDate }</td>
						                <td class="text-center">${dataList.cEmail }</td>
						                <td class="text-center">
						                	<input type="button" class="ttb-sm-btn btn-flat-orange" value="發送" onclick="initBlockUI();resend('${dataList.cDate }')"/>
						                </td>			               
									</tr>
								</c:forEach>
							</tbody>
						</table>

						
					</div>
				</div>
			</form>
		</section>
		</main>
		<!-- main-content END -->
	</div>
	<!-- content row END -->

	<%@ include file="../index/footer.jsp"%>

</body>
</html>