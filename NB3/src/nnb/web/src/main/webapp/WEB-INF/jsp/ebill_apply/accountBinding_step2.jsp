<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
    <meta content="" name="description">
    <meta content="臺灣中小企業銀行" name="author">
    <title>臺灣中小企業銀行 - 全國性繳費平台線上約定作業</title>
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+TC&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <link href="${__ctx}/ebillApply/css/bootstrap.min.css" rel="stylesheet">
    <link href="${__ctx}/ebillApply/css/full-width-pics.css" rel="stylesheet">
    <link href="${__ctx}/ebillApply/css/e-style.css" rel="stylesheet">
    <link href="${__ctx}/ebillApply/css/all.css" rel="stylesheet">
</head>

<body class="page">
    <header>
        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg fixed-top">
            <div class="container">
                <div class="col-12 text-center">
                    <a class="navbar-brand" href=""><img src="${__ctx}/ebillApply/img/logo_tbb.svg"></a>
                </div>
            </div>
        </nav>
        <!-- Header section -->
    </header>
    <main class="px-3 px-sm-0 py-2">
        <form id="validationForm" method="post">
        <input type="hidden" name="IBPD_Param" value='${accountBinding_step2.data.IBPD_Param}'>
            <div class="container mt-3 mt-sm-3 px-0">

                <div class="row">
                    <div class="col-md-12 py-sm-4">
                        <div class="progressbar text-center">
                            <ul class="list-inline">
                                <li class="list-inline-item current"><span>01</span>
                                    <step>網銀認證</step>
                                </li>
                                <li class="list-inline-item"><span>02</span></li>
                                <li class="list-inline-item"><span>03</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <p class="main-content-desc text-left">請登入您的網路銀行</p>
            </div>
            <!--connect error start-->
            <div id="loginErrorBlock" class="container error-block mb-3 px-2" style="display:none;">
                <div class="row">
                    <div class="col-3">
                        <div class="error-group">
                            <img src="${__ctx}/ebillApply/img/apply-failed-white.svg" alt=""
                                class="pl-1 img-fluid error-icon float-right">
                        </div>
                    </div>
                    <div class="col-9">
                        <div class="error-txt">
                            <p class="error-lg">抱歉，連線有點問題</p>
                            <p class="error-xs">您與本行連線逾時，請重新登入</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--connect error over-->
            <div class="container form-table mt-3 mt-sm-3">
                <div class="row">
                    <div class="col-md-12 main-content pt-3 py-sm-4 px-sm-5">
                        <div class="form-group">
                            <p class="form-id form-title"><span class="form-required"> * </span>您的身分證字號</p>
                            <p class="user-information" style="margin-bottom: -10px;margin-top:-13px;">${accountBinding_step2.data.CUSIDN_sh}</p>
                        </div>
                        <div class="form-group">
                            <p class="form-id form-title"><span class="form-required"> * </span>使用者名稱 <span
                                    class="form-icon">
                                    <a class="ml-2" data-toggle="modal" data-target="#mv-onlinebanking-forgot">
                                        <i class="fas fa-question-circle"></i></a></span></p>
                            <input type="text" id="form_username" name="form_username" maxlength="16"
                                class="form-control" aria-label="form_username" placeholder="請輸入6-16碼的使用者名稱" required>
                        </div>
                        <div class="form-group">
                            <p class="form-id form-title"><span class="form-required"> * </span>簽入密碼 <span
                                    class="form-icon">
                                    <a class="ml-2" data-toggle="modal" data-target="#mv-onlinebanking-forgot">
                                        <i class="fas fa-question-circle"></i></a></span></p>

                            <input type="password" id="form_password" name="form_password" maxlength="8"
                                class="form-control" aria-label="form_password" placeholder="請輸入6-8碼的簽入密碼"
                                autocomplete="off" required>
                        </div>
                        <div class="form-group ">
                            <p class="form-id form-title "><span class="form-required"> * </span>驗證碼 <span
                                    class="form-icon"></span></p>

                            <input type="text" id="capCode" name="capCode" maxlength="8" class="form-control"
                                aria-label="form_captcha" placeholder="請輸入驗證碼" autocomplete="off" required style="display:inline-block">
                            <img id="kaptchaImage" name="kaptchaImage" class="" />
                            
                            <span class="form-icon ">
                                <a class="ml-2" href="javascript:refreshCapCode();">
                                    <i class="fas fa-redo-alt"></i>
                                </a>
                            </span>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <br><br>
            <div class="container px-0">
                <div class="row action-container">
                    <div class="col-sm-12">
                        <div class="float-right">
                            <button class="back">取消</button>
                            <button type="button" class="btn btn-primary submit" id="loginBtn">登入<i
                                    class="fas fa-chevron-right float-right mt-1"></i></button>
                        </div>
                    </div>
                </div>
                <div class="container mt-3 mb-4">
                    <div class="row">
                        <div class="col-sm-12 px-0">
                            <div class="notice-txt">
                                <p style="color: #aaa;font-size: 16px;">注意事項</p>
                                <li>1.本服務需要有本行存款帳戶、網路銀行會員，及已在本行留存Email與手機號碼，即可進行約定。 </li>
                                <li>2.如有疑問，請洽本行24小時客戶服務專線 0800-01-7171 (限市話)、(02)2357-7171。</li>
                            </div>
                        </div>
                    </div>
                </div>
        </form>
    </main>
    <footer class="py-sm-0 d-flex align-items-center align-middle">
        <div class="container">
            <div class="row d-flex align-items-center align-middle">
                <div class="col-12 mt-3 mt-sm-0 pr-sm-0 text-center order-last order-sm-first">
                    <h6 class="copyright mb-0">©臺灣中小企業銀行</h6>
                    <span class="small"><a href="https://www.tbb.com.tw/web/guest/-173" target="_blank">隱私權聲明</a> | <a
                            href="https://www.tbb.com.tw/web/guest/-551" target="_blank">安全政策</a></span>
                </div>
                <div class="col-12 mt-3 mt-sm-0 p-0 text-center d-sm-none">
                    <p class="mt-sm-4">24H服務專線 <a href="tel:0800-01-7171">0800-01-7171</a>(限市話), <a
                            href="tel:02-2357-7171">02-2357-7171</a></p>
                </div>
                <div class="col-12 pt-0 footer-sns-link">
                    <ul class="list-inline ml-auto mb-sm-0 text-center">
                        <li class="list-inline-item d-none d-sm-inline-block mr-2">
                            <p>24H服務專線 <a href="tel:0800-01-7171">0800-01-7171</a>(限市話), <a
                                    href="tel:02-2357-7171">02-2357-7171</a></p>
                        </li>
                        <li class="list-inline-item mr-2">
                            <a href="https://www.facebook.com/tbbdreamplus/" target="_blank"><i
                                    class="fab fa-facebook"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://www.youtube.com/channel/UCyRmUHjcJV3ROmXrF7q3xOA" target="_blank"><i
                                    class="fab fa-youtube"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!-- Bootstrap core JavaScript -->
    <script src="${__ctx}/ebillApply/js/jquery.min.js"></script>
    <script src="${__ctx}/ebillApply/js/bootstrap.bundle.min.js"></script>
    <script src="${__ctx}/ebillApply/js/jquery.validate.min.js"></script>
    <script src="${__ctx}/ebillApply/js/localization/messages_zh_TW.js"></script>
    <script src="${__ctx}/ebillApply/js/e-bill-2.js"></script>
    <script src="${__ctx}/js/fstop.js"></script>
    <script type="text/javascript" src="${__ctx}/js/CGJSCrypt_min.js?a=${jscssDate}"></script>
    <!--忘記/重設密碼 start-->
    <div class="modal fade" id="mv-onlinebanking-forgot" tabindex="-1" role="dialog"
        aria-labelledby="mv-onlinebanking-forgot" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="container text-center px-0">
                        <h4 class="modal-title mt-3">重設使用者名稱/簽入密碼</h4>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="text-left">
                        <div class="table form-table mt-0 mb-0">
                            <div class="row ">
                                <div class="col-12 col-sm-10 offset-sm-1">
                                    <ul class="my-2 message-group">
                                        <li class="py-1"><span
                                                class="font-weight-bold">1.晶片金融卡重設</span>：可至本行網銀以晶片金融卡登入並完成簡訊認證身分，即可重設密碼。<br>
                                            <button onclick="window.open('https://ebank.tbb.com.tw/nb3/');"
                                                type="submit" class="mt-1 btn btn-primary submit btn-adj">前往網路銀行<i
                                                    class="fas fa-chevron-right"></i></button></li>
                                        <li class="py-1 mt-2"><span class="font-weight-bold">2.
                                                臨櫃重設</span>：您可以攜帶身分證件、原留印鑑至往來分行辦理。</li class="py-1">
                                    </ul>
                                    <button class="close py-4" data-dismiss="modal">關閉</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--忘記/重設密碼 over-->
    <script type="text/javascript">
        var isE2E = '${isE2E}';

        // 載入後初始化JS
        $(document).ready(function () {
            //註冊按鈕
            buttonRegistry();
            // 初始化驗證碼
            $('#kaptchaImage').attr("src", "${__ctx}/CAPCODE/captcha_image");
        });
        
        /**
         * ----- Registry Area -----
         */
         
        //註冊按鈕
        function buttonRegistry() {
            $(".back").click(function () {          	
            	$("#capCode").addClass("ignore");
            	$("#form_username").addClass("ignore");
            	$("#form_password").addClass("ignore");
            	$("#validationForm").attr("action", "${accountBinding_step2.data.BackUrl}");
                $("#validationForm").submit();
            });
            $("#loginBtn").click(function () {
            	$("#loginErrorBlock").hide();
            	//表單驗證
                if (!$("#validationForm").valid())
                    return false;
                //登入aj
                var response = login_aj();

                if ("0" == response.msgCode) {
                    // 登入成功				
                    $("#validationForm").attr("action", "${__ctx}/EBILL/APPLY/accountBinding_step3");
                    $("#validationForm").submit();
                } else {
                    //顯示錯誤訊息
                    $("#loginErrorBlock .error-lg").text("登入失敗");
                    $("#loginErrorBlock .error-xs").text(response.msgCode + response.message);
                    $("#loginErrorBlock").show();
                    //刷新驗證碼
                    refreshCapCode();
                    //清空輸入
                    $('#form_password').val("");
                    $('#form_username').val("");
                    $('#capCode').val("");
                    $('#form_password').removeClass("ignore");
                }
            });
        }
      	//註冊enter
      	$(window).keydown(function(e){
			var curKey = window.event ? e.keyCode : e.which;
			if(curKey == 13){//enter键位:13
				e.preventDefault();
				$("#loginBtn").click();
			}
		})
        /**
         * ----- logic area -----
         */

        // 登入
        function login_aj() {
            console.log("login_aj.now: " + new Date());

            var rdata;
            var login_uri = '${__ctx}' + "/EBILL/APPLY/login_ajax";
            console.log("login_aj.login_uri: " + login_uri);
            $('#form_password').addClass("ignore");
            $('#form_password').val(pin_encrypt($('#form_password').val()));
            console.log("pin_encrypt.now: " + new Date());;

            rdata = $("#validationForm").serializeArray();
            console.log(rdata);
            //非同步 no callback
            var response = fstop.getServerDataEx(login_uri, rdata, false, false);
            console.log(response);
            return response;
        }
        //重整輸入
        function resetInput() {
            $("#form_username").val("");
            $("#form_password").val("");
        }
        // 刷新驗證碼
        function refreshCapCode() {
            console.log("refreshCapCode...");

            // 驗證碼
            $('#form_captcha').val("");

            // 大小版驗證碼用同一個
            $('img[name="kaptchaImage"]').hide().attr(
                'src', '${__ctx}' + '/CAPCODE/captcha_image?' + Math.floor(Math.random() * 100)).fadeIn();

        }
    </script>
</body>

</html>