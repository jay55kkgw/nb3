<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
	<style type="text/css">
		.legendLayer .background {
			fill: rgba(255, 255, 255, 0.85);
			stroke: rgba(0, 0, 0, 0.85);
			stroke-width: 1;
		}
		@media screen and (max-width:1024px){
			.flot-x-axis .tickLabel{
			    -o-transform:rotate(30deg);
			    -moz-transform:rotate(30deg);
			    -webkit-transform:rotate(30deg);
			    filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
			}
			.flot-x-axis{
				top:5px !important;
			}
		}
    </style>
	<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="${__ctx}/js/excanvas.min.js"></script><![endif]-->
	<script type="text/javascript" src="${__ctx}/js/jquery.colorhelpers-0.9.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.flot-0.9.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.flot.time-0.9.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		// 將.table變更為footable
		//initFootable();
		init();
		<c:if test="${result_data.data.REC.size() > 0}">
		createData();
		$(window).resize(function(){
			console.log("resize");
			createData();
		});
		</c:if>
		setTimeout("initDataTable()",100);
// 		initFlot();
	});

	//下拉式選單
	function formReset() {
// 		initBlockUI();
		if ($('#actionBar').val() == "excel") {
			$("#downloadType").val("OLDEXCEL");
			$("#templatePath").val(
					"/downloadTemplate/history_price_query_result_LD.xls");
		}
// 		ajaxDownload("${__ctx}/ajaxDownload", "formId", "finishAjaxDownload()");

		$("form").attr("action", "${__ctx}/download");
		$("#formId").attr("target", "");
        $("#formId").submit();
        $('#actionBar').val("");
	}
	function finishAjaxDownload() {
		$("#actionBar").val("");
// 		unBlockUI();
	}

	<c:if test="${result_data.data.REC.size() > 0}">
	function createData(){
		var sData = [];
		var bData = [];
		var minTime = null;
		var maxTime = null;
		//將資料塞入DateList
		<c:forEach var="dataList" items="${result_data.data.REC}">
			var time = "${dataList.QDATE} ${dataList.QTIME}";
			var sPrice = ${dataList.SPRICE};
			var bPrice = ${dataList.BPRICE};
			if(minTime == null)
				minTime = time;
			if(maxTime == null)
				maxTime = time;
			if( (new Date(time)).getTime() > (new Date(maxTime)).getTime())
				maxTime = time;
			if( (new Date(time)).getTime() < (new Date(minTime)).getTime())
				minTime = time;
			sData.push([new Date(time), sPrice]);
			bData.push([new Date(time), bPrice]);
	    </c:forEach>
	    //拉開X軸
	    var maxTimeDate = new Date((new Date(maxTime)).getTime() + 360000);
        var minTimeDate = new Date((new Date(minTime)).getTime() - 360000);
        var dMinMax = [[maxTimeDate,${result_data.data.B_MAX > result_data.data.S_MAX ? result_data.data.B_MAX : result_data.data.S_MAX}],[minTimeDate,${result_data.data.B_MIN < result_data.data.S_MIN ? result_data.data.B_MIN : result_data.data.S_MIN}]]
        //加入Dateset
        //買進
        var buyLable = "<spring:message code="LB.W1484"/>";
        //賣出
        var sellLable = "<spring:message code="LB.W1485"/>";
        var dataset = [	{label: buyLable,data: sData, color:"#0088FF"},
        				{label: sellLable,data: bData,color:"#FF8811"}];
        console.log(dataset);
        //flot option
        var options = {
			legend:{
				show: true
			},
			series: {
				lines: { 
					show: true
				},
				points: {
					show: true
				}
			},
			xaxis: {
				mode: "time",
				timeBase: "milliseconds",
				timeformat: " %H:%M:%S ",
				timezone: "browser",
				max: maxTimeDate,
				min: minTimeDate
			},
			yaxis:{
				max:${result_data.data.B_MAX > result_data.data.S_MAX ? result_data.data.B_MAX : result_data.data.S_MAX},
				min:${result_data.data.B_MIN < result_data.data.S_MIN ? result_data.data.B_MIN : result_data.data.S_MIN}
			},
			grid: { 
				hoverable: true,
				backgroundColor: { colors: ["#FFF5EE", "#FFC0CB"] }
			}// 开启 hoverable 事件
		};
		
        $.plot($("#flot-placeholder"), dataset, options);
			
		var previousPoint = null;
		// 绑定提示事件
		$("#flot-placeholder").bind("plothover", function (event, pos, item) {
			if (item) {
				if (previousPoint != item.dataIndex) {
					console.log(item);
					previousPoint = item.dataIndex;
					$("#tooltip").remove();
					var x = new Date(item.datapoint[0]);
					x = "<b style='color:blue;'>" + x.getHours() + ":"  + x.getMinutes() + "</b>";
					var y = item.datapoint[1].toFixed(0);
					var tip = item.series.label + "：";
					showTooltip(item.pageX, item.pageY,x + '<br>' + tip+y);
				}
			}
			else {
				$("#tooltip").remove();
				previousPoint = null;
			}
		});
	}
	</c:if>
	
	//折線圖標籤
	function showTooltip(x, y, contents) {
        $('<div id="tooltip">' + contents + '</div>').css( {
            position: 'absolute',
            display: 'none',
            top: y + 10,
            left: x + 10,
            border: '1px solid #fdd',
            padding: '2px',
            opacity: 0.80
        }).appendTo("body").fadeIn(200);
    }
	
	function init() {
		//上一頁按鈕
		$("#CMBACK").click(function() {
			var action = '${__ctx}/GOLD/OUT/PASSBOOK/history_price_query';
			$('#back').val("Y");
			$("form").attr("action", action);
			initBlockUI();
			$("form").submit();
		});
	}
	
	function showTooltip(x, y, contents, time) {
        $('<div id="tooltip">' + contents + '</div>').css( {
            position: 'absolute',
            display: 'none',
            top: y + 10,
            left: x + 10,
            border: '1px solid #fdd',
            padding: '2px',
            'background-color': '#dfeffc',
            opacity: 0.80
        }).appendTo("body").fadeIn(200);
    }

</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 查詢服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.use_component_P1_D1-1" /></li>
    <!-- 歷史價格查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1474" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
		
			<form id="formId" method="post" action="${__ctx}/download">
				<!-- 主頁內容  -->
				<!-- 黃金存摺歷史價格查詢 -->
				<h2><spring:message code="LB.W1474"/></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
						<!-- 下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
					</select>
				</div>
	
				<div class="main-content-block row">
					<div class="col-12">
						<ul class="ttb-result-list">
							<li>
								<!-- 系統時間 -->
								<h3><spring:message code="LB.System_time" /></h3>
								<p>
									${result_data.data.CMQTIME}
								</p> <!-- 查詢期間 -->
							</li>
							<li>
								<h3><spring:message code="LB.Inquiry_period_1" /></h3>
								<p>
									${result_data.data.PERIODFROM} ~ ${result_data.data.PERIODTO}
								</p>
							</li>
						</ul>
						<c:if test="${result_data.data.REC.size() > 0}">
							<div class="ttb-input-block">
								<span class="input-title"> 
									<label>
									<!-- 本行買進/本行賣出走勢圖 -->
										<b>
										<spring:message code="LB.W1483" />
										</b>
									</label>
								</span>
								
								<div id="flot-placeholder" style="height:300px"></div>
							</div>
							<!-- 單位：新台幣元/1公克 -->
							<div class="text-right"><spring:message code="LB.W1486" /></div>
							
							<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
								<thead>
									<tr>
										<!--日期-->
										<th><spring:message code="LB.W1155" /></th>
										<!-- 時間-->
										<th><spring:message code="LB.W1488" /></th>
										<!--本行買進價格-->
										<th><spring:message code="LB.W1471" /></th>
										<!-- 本行賣出價格-->
										<th><spring:message code="LB.W1470" /></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="dataList" items="${result_data.data.REC}">
										<tr>
											<td class="text-center">${dataList.QDATE}</td>
											<td class="text-center">${dataList.QTIME}</td>
											<td class="text-right">${dataList.SPRICEP}</td>
											<td class="text-right">${dataList.BPRICEP}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</c:if>
						
						<c:if test="${result_data.data.REC.size() <= 0}">
							<div>
							<!-- 查無資料 Check_no_data -->
								<spring:message code="LB.Check_no_data" />
							</div>
						</c:if>
						<!-- 回上頁-->
						<input type="button" class="ttb-button btn-flat-orange" name="CMBACK" id="CMBACK" value="<spring:message code="LB.Back_to_previous_page"/>" />
					</div>
				</div>
				<!-- 下載用 -->
				<!-- 檔名 黃金存摺歷史價格查詢 -->
				<input type="hidden" name="downloadFileName"value="<spring:message code="LB.W1474"/>" />
				<!--輕鬆理財證券交割明細 -->
				<input type="hidden" name="CMQTIME" value="${result_data.data.CMQTIME}" />
				<input type="hidden" name="PERIODFROM" value="${result_data.data.PERIODFROM}" /> 
				<input type="hidden" name="PERIODTO" value="${result_data.data.PERIODTO}" /> 
				<input type="hidden" name="downloadType" id="downloadType" /> 
				<input type="hidden" name="templatePath" id="templatePath" /> 
				<input type="hidden" name="hasMultiRowData" value="false" />

				<!-- EXCEL下載用 -->
				<!-- headerRightEnd  資料列以前的右方界線
						 headerBottomEnd 資料列到第幾列 從0開始
						 rowStartIndex 資料列第一列的位置
						 rowRightEnd 資料列用方的界線
					 -->
				<input type="hidden" name="headerRightEnd" value="3" /> 
				<input type="hidden" name="headerBottomEnd" value="5" /> 
				<input type="hidden" name="rowRightEnd" value="3" /> 
                <input type="hidden" name="rowStartIndex" value="6" />
			</form>
		</section>
		</main>
		
		
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>