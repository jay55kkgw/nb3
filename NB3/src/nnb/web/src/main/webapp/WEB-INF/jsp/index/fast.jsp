<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<section id="fast-menu">
	<a href="#fast-menu-block" class="fast-menu-btn" data-toggle="collapse"
		data-parent="#fast-menu" aria-expanded="true"
		aria-controls="fast-menu-block" role="button">快速選單<i
		class="fa fa-angle-up" style="margin-left: 21px;"></i></a>
</section>
<section id="fast-menu-block" class="collapse" role="tabpanel">
	<div class="mobile-header">
		<img class="mobile-logo"
			src="${__ctx}/img/logo-white.png">
		<a href="#fast-menu-block" data-toggle="collapse"
			data-parent="#fast-menu" aria-expanded="true"
			aria-controls="fast-menu-block" role="button"> <img
			src="${__ctx}/img/icon-close.svg">
		</a>


	</div>
	<div class="mobile-fast-menu-title">快速選單</div>
	<ul>
		<li><a href="#">帳戶總覽</a> <span>頁面名稱頁面名稱頁面名稱</span></li>
		<li><a href="#">帳戶總覽</a> <span>頁面名稱頁面名稱頁面名稱</span></li>
		<li><a href="#">帳戶總覽</a> <span>頁面名稱頁面名稱頁面名稱</span></li>
		<li><a href="#">帳戶總覽</a> <span>頁面名稱頁面名稱頁面名稱</span></li>
	</ul>
</section>