<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ include file="../__import_js.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
	$(document).ready(function() {
		window.print();
	});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br />
	<br />
	<div style="text-align: center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br />
	<br />
	<br />
	<div style="text-align: center">
		<font style="font-weight: bold; font-size: 1.2em">${jspTitle}</font>
	</div>
	<br />
	<div style="text-align: center">${MsgName}</div>
	<br />
	<table class="print">
		<c:if test="${FGTXDATE == '0'}">
<!-- 	交易時間 -->
			<tr>
				<td><spring:message code="LB.Trading_time"/></td>
				<td colspan="5" style="text-align:left">${CMQTIME}</td>
			</tr>
<!-- 	轉出帳號 -->
			<tr>
				<td><spring:message code="LB.Payers_account_no"/></td>
				<td colspan="5" style="text-align:left">${FYTSFAN}</td>
			</tr>
<!-- 	轉入帳號 -->
			<tr>
				<td><spring:message code="LB.Payees_account_no"/></td>
				<td colspan="5" style="text-align:left">${TSFACN}</td>
			</tr>
<!-- 	存單號碼 -->
			<tr>
				<td><spring:message code="LB.Certificate_no"/></td>
				<td colspan="5" style="text-align:left">${FDPNUM}</td>
			</tr>
<!-- 	幣別&&&轉帳金額 -->
			<tr>
				<td><spring:message code="LB.Amount"/></td>
				<td colspan="5" style="text-align:left">
					${OUTCRY_TEXT}<fmt:formatNumber type="number" minFractionDigits="2" value="${AMOUNT}"/>
				</td>
			</tr>
<!-- 	存款期別 -->
			<tr>
				<td><spring:message code="LB.Deposit_period"/></td>
				<td colspan="5" style="text-align:left">${TYPCOD_TEXT}</td>
			</tr>
<!-- 	起存日 -->
			<tr>
				<td><spring:message code="LB.Start_date"/></td>
				<td colspan="5" style="text-align:left">${SDT}</td>
			</tr>
<!-- 	到期日 -->
			<tr>
				<td><spring:message code="LB.Maturity_date"/></td>
				<td colspan="5" style="text-align:left">${DUEDAT}</td>
			</tr>
<!-- 	計息方式 -->
			<tr>
				<td><spring:message code="LB.Interest_calculation"/></td>
				<td colspan="5" style="text-align:left">${INTMTHNAME}</td>
			</tr>
<!-- 	利率 -->
			<tr>
				<td><spring:message code="LB.Interest_rate"/></td>
				<td colspan="5" style="text-align:left">${ITR}%</td>
			</tr>
<!-- 	轉存方式 -->
			<tr>
				<td><spring:message code="LB.Rollover_method"/></td>
				<td colspan="5" style="text-align:left">${AUTXFTMNAME}</td>
			</tr>
<!-- 	到期轉期 -->
			<tr>
				<td><spring:message code="LB.Roll-over_when_expiration"/></td>
				<td colspan="5" style="text-align:left">${CODENAME}</td>
			</tr>
<!-- 	交易備註 -->
			<tr>
				<td><spring:message code="LB.Transfer_note"/></td>
				<td colspan="5" style="text-align:left">${CMTRMEMO}</td>
			</tr>
<!-- 	轉帳帳號帳戶餘額 -->
			<tr>
				<td><spring:message code="LB.Payers_account_balance"/></td>
				<td colspan="5" style="text-align:left">
					<fmt:formatNumber type="number" minFractionDigits="2" value="${TOTBAL}"/>
				</td>
			</tr>
<!-- 	轉帳帳號可用餘額 -->
			<tr>
				<td><spring:message code="LB.Payers_available_balance"/></td>
				<td colspan="5" style="text-align:left">
					<fmt:formatNumber type="number" minFractionDigits="2" value="${AVLBAL}"/>
				</td>
			</tr>
		</c:if>
		<c:if test="${FGTXDATE != '0'}">
<!-- 	資料時間 -->
			<tr>
				<td><spring:message code= "LB.Data_time" /></td>
				<td colspan="5" style="text-align:left">${CMQTIME}</td>
			</tr>
<!-- 	轉帳日期 -->
			<tr>
				<td><spring:message code="LB.Transfer_date"/></td>
				<td colspan="5" style="text-align:left">${TRDATE}</td>
			</tr>
<!-- 	轉出帳號 -->
			<tr>
				<td><spring:message code="LB.Payers_account_no"/></td>
				<td colspan="5" style="text-align:left">${FYTSFAN}</td>
			</tr>
<!-- 	轉入帳號 -->
			<tr>
				<td><spring:message code="LB.Payees_account_no"/></td>
				<td colspan="5" style="text-align:left">${TSFACN}</td>
			</tr>
<!-- 	幣別&&&轉帳金額 -->
			<tr>
				<td><spring:message code="LB.Amount"/></td>
				<td colspan="5" style="text-align:left">
					${OUTCRY_TEXT}<fmt:formatNumber type="number" minFractionDigits="2" value="${AMOUNT}"/>
				</td>
			</tr>
<!-- 	存款期別 -->
			<tr>
				<td><spring:message code="LB.Deposit_period"/></td>
				<td colspan="5" style="text-align:left">${TYPCOD_TEXT}</td>
			</tr>
<!-- 	計息方式 -->
			<tr>
				<td><spring:message code="LB.Interest_calculation"/></td>
				<td colspan="5" style="text-align:left">${INTMTHNAME}</td>
			</tr>
<!-- 	轉存方式 -->
			<tr>
				<td><spring:message code="LB.Rollover_method"/></td>
				<td colspan="5" style="text-align:left">${AUTXFTMNAME}</td>
			</tr>
<!-- 	到期轉期 -->
			<tr>
				<td><spring:message code="LB.Roll-over_when_expiration"/></td>
				<td colspan="5" style="text-align:left">${CODENAME}</td>
			</tr>
<!-- 	交易備註 -->
			<tr>
				<td><spring:message code="LB.Transfer_note"/></td>
				<td colspan="5" style="text-align:left">${CMTRMEMO}</td>
			</tr>
		</c:if>
	</table>
	<br>
	<br>
	<div class="text-left">
		<spring:message code="LB.Description_of_page"/><br/><br/>
		<c:if test="${FGTXDATE == '0'}">
			1.<spring:message code= "LB.Deposit_transfer_P3_D2" />
		</c:if>
		<c:if test="${FGTXDATE != '0'}">
			<spring:message code="LB.F_deposit_transfer_PP3_D1"/><br/>
            <spring:message code="LB.F_deposit_transfer_PP3_D2"/><br/>
			<spring:message code="LB.F_deposit_transfer_PP3_D3"/>
		</c:if>
	</div>
</body>
</html>