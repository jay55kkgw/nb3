<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	    <script type="text/javascript">
        $(document).ready(function () {
            //initFootable(); // 將.table變更為footable 
            setTimeout("initDataTable()",100);
            init();
        });

        function init() {
        	//繼續查詢
        	$("#CMCONTINU").click(function (e) {
				e = e || window.event;
				console.log("submit~~");

				$("#formId").validationEngine('detach');
				initBlockUI(); //遮罩
				$("#formId").attr("action", "${__ctx}/FCY/ACCT/f_credit_open_query_date_result");
				$("#formId").submit();
			});
        	
            //列印
        	$("#printbtn").click(function(){
				var params = {
					"jspTemplateName":"f_letter_of_credit_opening_inquiry_date_result_print",
					"jspTitle":'<spring:message code= "LB.X0004" />',
					"CMQTIME":"${base_result.data.CMQTIME}",
					"LCNO":"${base_result.data.LCNO}",
					"CMPERIOD":"${base_result.data.CMPERIOD}",
					"CMRECNUM":"${base_result.data.CMRECNUM}",
					"SATM":"${base_result.data.SATM}",
					"SBAL":"${base_result.data.SBAL}"
				};
				openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
    		//上一頁按鈕
    		$("#CMBACK").click(function() {
    			var action = '${__ctx}/FCY/ACCT/f_credit_open_query_date';
    			$('#back').val("Y");
    			$("#formId").attr("action", action);
    			initBlockUI();
    			$("#formId").submit();
    		});
        }
        
    	//選項
	 	function formReset() {
// 	 		initBlockUI();
    		if ($('#actionBar').val()=="excel"){
				$("#USERDATA_X50").val("");
				$("#downloadType").val("OLDEXCEL");
				$("#templatePath").val("/downloadTemplate/f_letter_of_credit_opening_inquiry_date_result.xls");
	 		}else if ($('#actionBar').val()=="txt"){
				$("#USERDATA_X50").val("");
				$("#downloadType").val("TXT");
			    $("#templatePath").val("/downloadTemplate/f_letter_of_credit_opening_inquiry_date_result.txt");
	 		}

    		$("form").attr("action", "${__ctx}/FCY/ACCT/f_credit_open_query_date_ajaxDirectDownload");
    		$("#formId").attr("target", "");
            $("#formId").submit();
            $('#actionBar').val("");
//     		ajaxDownload("${__ctx}/FCY/ACCT/f_credit_open_query_date_ajaxDirectDownload","formId","finishAjaxDownload()");
		}
	 	function finishAjaxDownload(){
			$("#actionBar").val("");
			unBlockUI(initBlockId);
		}
	 	//到單查詢按鈕
	 	function import_detail_submit(LCNO,RORIGAMT,RBENNAME){
			$("#LCNO").val(LCNO);
			$("#RORIGAMT").val(RORIGAMT);
			$("#RBENNAME").val(RBENNAME);
			var action = '${__ctx}/FCY/ACCT/f_credit_open_query_import_detail';
			$("#formId").attr("action", action);
			initBlockUI();
			$("#formId").submit();
	 	}
	 	//信用狀明細查詢按鈕
	 	function commerce_detail_submit(LCNO,RORIGAMT,RBENNAME){
			$("#LCNO").val(LCNO);
			$("#RORIGAMT").val(RORIGAMT);
			$("#RBENNAME").val(RBENNAME);
			var action = '${__ctx}/FCY/ACCT/f_credit_open_query_commerce_detail';
			$("#formId").attr("action", action);
			initBlockUI();
			$("#formId").submit();
	 	}
	 	function hd2(T) {
	 	    var t = document.getElementById(T);
	 	    if (t.style.visibility === 'hidden') {
	 	        t.style.visibility = 'visible';
	 	    } else {
	 	        t.style.visibility = 'hidden';
	 	    }
	 	}
    </script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 帳戶查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Account_Inquiry" /></li>
    <!-- 開狀查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0098" /></li>
    <!-- 帳戶明細查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Demand_Deposit_Detail" /></li>
    <!-- 商業信用狀明細     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0004" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!-- 商業信用狀明細(未輸入) -->
					<!-- <spring:message code="LB.NTD_Demand_Deposit_Detail" /> -->
					<spring:message code="LB.X0004" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 						下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
<!-- 						下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
					</select>
				</div>	
                <form id="formId" method="post">
                	<input type="hidden" id="back" name="back" value="">
					<input type="hidden" id="LCNO" name="LCNO" />
					<input type="hidden" id="RORIGAMT" name="RORIGAMT" />
					<input type="hidden" id="RBENNAME" name="RBENNAME" />
                    <input type="hidden" id="USERDATA_X50" name="USERDATA_X50" value="${base_result.data.USERDATA_X50}" />
                    <input type="hidden" name="CMSDATE" value="${base_result.data.CMSDATE}" />
                    <input type="hidden" name="CMEDATE" value="${base_result.data.CMEDATE}" />
                    <input type="hidden" name="USERDATA_X50_import_detail" value="" />
                    <!-- 下載用 -->
                    <input type="hidden" name="downloadFileName" value="<spring:message code="LB.X0004" />" />
                    <input type="hidden" name="CMQTIME" value="${base_result.data.CMQTIME}" />
                    <input type="hidden" name="CMPERIOD" value="${base_result.data.CMPERIOD}" />
                    <input type="hidden" name="CMRECNUM" value="${base_result.data.CMRECNUM}" />
                    <input type="hidden" name="LCNO" value="${base_result.data.putLCNO}" />
                    <input type="hidden" name="SDATM" value="${base_result.data.SDATM}" />
                    <input type="hidden" name="SDBAL" value="${base_result.data.SDBAL}" />
                    <input type="hidden" name="downloadType" id="downloadType" />
                    <input type="hidden" name="templatePath" id="templatePath" />
                    <input type="hidden" name="hasMultiRowData" value="false"/>
                    <!-- EXCEL下載用 -->
                    <input type="hidden" name="headerRightEnd" value="9" />
                    <input type="hidden" name="headerBottomEnd" value="7" />
                    <input type="hidden" name="rowStartIndex" value="8" />
                    <input type="hidden" name="multiRowDataListMapKey" value="rowListMap" />
                    <input type="hidden" name="rowRightEnd" value="8" />
                    <input type="hidden" name="footerStartIndex" value="10" />
                    <input type="hidden" name="footerEndIndex" value="13" />
                    <input type="hidden" name="footerRightEnd" value="2" />
                    <!-- TXT下載用 -->
                    <input type="hidden" name="txtHeaderBottomEnd" value="11"/>
					<input type="hidden" name="txtHasRowData" value="true"/>
					<input type="hidden" name="txtHasFooter" value="true"/>
					<input type="hidden" name="txtMultiRowDataListMapKey" value="rowListMap"/>
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12 tab-content">
                            <ul class="ttb-result-list">
                                <li>
                                    <!-- 查詢時間 -->
                                    <h3><spring:message code="LB.Inquiry_time" /></h3>
                                    <p>${base_result.data.CMQTIME }</p>
	                            </li>
	                            <li>
                                    <!-- 查詢期間 -->
                                    <h3><spring:message code="LB.Inquiry_period" /></h3>
                                    <p>${base_result.data.CMPERIOD }</p>
	                            </li>
	                            <li>
                                    <!-- 信用狀號碼 : -->
									<h3><spring:message code="LB.L/C_no" /></h3>
                                    <p>${base_result.data.LCNO}</p>
	                            </li>
	                            <li>
                                    <!-- 資料總數 : -->
                                    <h3><spring:message code="LB.Total_records" /></h3>
                                    <p>
                                        ${base_result.data.CMRECNUM}
                                        <!--筆 -->
                                        <spring:message code="LB.Rows" />
                                    </p>
                                </li>
                            </ul>
                            <!-- 表格區塊 -->
                        
                            <table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
                                <thead>
                                    <tr>
                                    	<!--開狀日期-->
                                        <th data-title="<spring:message code="LB.W0089"/>">
                                            <!--<spring:message code="LB.Change_date" />-->
											<spring:message code="LB.W0089" />
                                        </th>
                                        <!--信用狀號碼-->
                                        <th data-title="<spring:message code="LB.W0085"/>">
                                            <!--<spring:message code="LB.Change_date" />-->
											<spring:message code="LB.W0085" />
                                        </th>
                                        <!-- 受益人 -->
                                        <th data-title="<spring:message code="LB.W0174"/>" data-breakpoints="xs sm">
                                            <!--  <spring:message code="LB.Summary_1" /> -->
											<spring:message code="LB.W0174" />
                                        </th>
                                        <!-- 幣別 -->
                                        <th data-title="<spring:message code="LB.Currency"/>" data-breakpoints="xs sm">
                                            <spring:message code="LB.Currency" />
                                        </th>
                                        <!--開狀金額 -->
                                        <!--開狀餘額 -->
                                        <th data-breakpoints="xs sm">
                                            <!-- <spring:message code="LB.Deposit_amount" /> -->
											<spring:message code="LB.W0094" /><br><spring:message code="LB.X0005" />
                                        </th>
                                        <!-- 信用狀到期日 -->
                                        <!-- 最後裝船日 -->
                                        <th data-breakpoints="xs sm">
                                            <!-- <spring:message code="LB.Account_balance_2" /> -->
											<spring:message code="LB.W0108" /><br><spring:message code="LB.W0109" />
                                        </th>
                                        <!-- 備註 -->
                                        <th data-title="<spring:message code="LB.Note"/>" data-breakpoints="xs sm">
                                            <spring:message code="LB.Note" />
                                        </th>
                                        <!-- 執行選項 -->
                                        <th data-title="<spring:message code="LB.Execution_option"/>">
                                            <spring:message code="LB.Execution_option" />
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="dataList" items="${ base_result.data.REC }">
                                        <tr>
                                            <!--開狀日期-->
                                            <td class="text-center">${dataList.ROPENDAT }</td>
                                            <!--信用狀號碼-->
                                            <td class="text-center">${dataList.RLCNO }</td>
                                       		<!-- 受益人 -->
                                            <td class="text-center">${dataList.RBENNAME }</td>
                                        	<!-- 幣別 -->
                                            <td class="text-center">${dataList.RLCCCY }</td>
	                                        <!--開狀金額 -->
                                        	<!--開狀餘額 -->
                                            <td  style="text-align: right;">${dataList.RORIGAMT }<br>${dataList.RLCOS }</td>
                                            <!-- 信用狀到期日 -->
                                        	<!-- 最後裝船日 -->
                                            <td class="text-center">
					                       		<c:set var="i18n_D1070_2">
					                       			<spring:message code='LB.D1070_2' />
					                       		</c:set>
                                            	${dataList.REXPDATE.equals("") ? i18n_D1070_2:dataList.REXPDATE}<br>${dataList.RSHPDATE.equals("") ? i18n_D1070_2: dataList.RSHPDATE}
                                            </td>
                                        	<!-- 備註 -->
                                            <td class="text-center">${dataList.RMARK }</td>
                                        	<!-- 執行選項 -->
                                            <td class="text-center">
					                            <!-- 到單查詢  -->
					                            <button type="button" class="ttb-sm-btn btn-flat-orange d-none d-lg-inline-block" onclick="import_detail_submit('${dataList.RLCNO}','${dataList.RORIGAMT }','${dataList.RBENNAME }')"><spring:message code="LB.W0111" /></button>
<%-- 					                            <input type="button" value="<spring:message code="LB.W0111" />" onclick="import_detail_submit('${dataList.RLCNO}','${dataList.RORIGAMT }','${dataList.RBENNAME }')" /> --%>
					                            
					                            <!-- 信用狀明細查詢 -->
					                            <button type="button" class="ttb-sm-btn btn-flat-orange d-none d-lg-inline-block" onclick="commerce_detail_submit('${dataList.RLCNO}','${dataList.RORIGAMT }','${dataList.RBENNAME }')"><spring:message code="LB.W0112" /></button>
<%-- 					                            <input type="button" value="<spring:message code="LB.W0112" />" onclick="commerce_detail_submit('${dataList.RLCNO}','${dataList.RORIGAMT }','${dataList.RBENNAME }')"  /> --%>

		                                        <input type="button" class="d-lg-none" onclick="hd2('actionBar${dataList.RLCNO}')" value="..." />
		                                        <div id="actionBar${dataList.RLCNO}" class="fast-div-grey" style="visibility: hidden">
		                                            <div class="fast-div">
		                                                <img src="${__ctx}/img/icon-close-2.svg" class="top-close-btn" onclick="hd2('actionBar${dataList.RLCNO}')">
		                                                <p><spring:message code= "LB.X1592" /></p>
		                                                <ul>
		                                                    <a onclick="import_detail_submit('${dataList.RLCNO}','${dataList.RORIGAMT }','${dataList.RBENNAME }')">
		                                                        <li><spring:message code="LB.W0111" /><img src="${__ctx}/img/icon-10.svg" align="right"></li>
		                                                    </a>
		                                                    <a onclick="commerce_detail_submit('${dataList.RLCNO}','${dataList.RORIGAMT }','${dataList.RBENNAME }')">
		                                                        <li><spring:message code="LB.W0112" /><img src="${__ctx}/img/icon-10.svg" align="right"></li>
		                                                    </a>
		                                                </ul>
		                                                <input type="button" class="bottom-close-btn" onclick="hd2('actionBar${dataList.RLCNO}')" value="<spring:message code= "LB.X1572" />"/>
		                                            </div>
		                                        </div>
											</td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>

                            <c:set var="total_i18n">
                       			<spring:message code='LB.W0123' />
                       		</c:set>
                       		<c:set var="row_i18n">
                       			<spring:message code='LB.Rows' />
                       		</c:set>

                            <ul class="ttb-result-list">
                                <li>
	                                <!-- 匯入金額總金額 -->
	                                <h3>
	                                    <!-- <spring:message code="LB.Inquiry_time" /> : -->
	                                   	 <spring:message code="LB.W0121" />
	                                </h3>
                                   	<p>
                                   		<c:set var="SATM_replace" value="${ fn:replace( base_result.data.SATM, 'i18n{LB.W0123}', total_i18n) }" />
                                   		<c:set var="SATM_replace2" value="${ fn:replace( SATM_replace, 'i18n{LB.Rows}', row_i18n) }" />
                                   		${SATM_replace2}
                               		</p>
	                            </li>
	                            <li>
	                                <!-- 匯入金額總金額 -->
	                                <h3>
	                                    <!-- <spring:message code="LB.Inquiry_time" /> : -->
	                                   	 <spring:message code="LB.W0122" />
	                                </h3>
                                   	<p>
                                   		<c:set var="SBAL_replace" value="${ fn:replace( base_result.data.SBAL, 'i18n{LB.W0123}', total_i18n) }" />
                                   		<c:set var="SBAL_replace2" value="${ fn:replace( SBAL_replace, 'i18n{LB.Rows}', row_i18n) }" />
                              			${SBAL_replace2}
                               		</p>
	                            </li>
	                        </ul>
	                        <br>
	                        <!--button 區域 -->
                            <!--回上頁 -->
                            <spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>
                            <input type="button" name="CMBACK" id="CMBACK" value="${cmback}" class="ttb-button btn-flat-gray">
                            <!-- 繼續查詢 -->
                            <c:if test="${ base_result.data.QUERYNEXT != null && !base_result.data.QUERYNEXT.equals('') }">
	                            <!--<spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>-->
	                            <input type="button" name="CMCONTINU" id="CMCONTINU" value=" <spring:message code="LB.X0151" />" class="ttb-button btn-flat-orange">
                            </c:if>
                            <!-- 列印  -->
                            <spring:message code="LB.Print" var="printbtn"></spring:message>
                            <input type="button" id="printbtn" value="${printbtn}" class="ttb-button btn-flat-orange" />
                        <!--                     button 區域 -->
                        </div>  
                    </div>
                    <c:if test="${ base_result.data.QUERYNEXT != null && !base_result.data.QUERYNEXT.equals('') }">
	                    <!-- 說明： -->
				        <ol class="description-list list-decimal">
				        	<p><spring:message code="LB.Description_of_page"/></p>
				            <li><span><spring:message code="LB.F_Credit_Open_Query_P3_D1"/></span></li>
				        </ol>
                    </c:if>
                </form>

	
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>