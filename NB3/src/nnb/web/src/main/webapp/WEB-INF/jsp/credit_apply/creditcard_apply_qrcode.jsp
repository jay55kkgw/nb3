<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<script type="text/javascript">
$(document).ready(function(){
	// HTML載入完成後開始遮罩
	setTimeout("initBlockUI()", 10);
	// 開始查詢資料並完成畫面
	setTimeout("init()", 20);
	// 解遮罩
	setTimeout("unBlockUI(initBlockId)", 500);
});

function init() {
	$("#CMSUBMIT").click( function() {
		console.log("submit~~");
		$("#formId").attr("action","${__ctx}/CREDIT/APPLY/apply_creditcard");
 	  	$("#formId").submit();
	});
}
</script>
</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 申請信用卡     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0666" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
<%-- 		<%@ include file="../index/menu.jsp"%> --%>
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<form method="post" id="formId">
					<input type="hidden" name="ADOPID" value="NA03"/>
					<input type="hidden" id="QRCODE" name="QRCODE" value="Y"/>
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-input-block">
								<img src="${__ctx}/img/bgSilver.jpg" width="100%">
							</div>
							<input type="button" id="CMSUBMIT" value="<spring:message code="LB.X0307" />" class="ttb-button btn-flat-orange"/><!-- 我要申請 -->
						</div>
					</div>
				</form>
			</section>
		</main>
	</div>
	
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>