<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="page6" class="card-detail-block">
	<div class="card-center-block">
		<!-- Q1 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-61" aria-expanded="true"
				aria-controls="popup1-61">
				<div class="row">
					<span class="col-1">Q1</span>
					<div class="col-11">
						<span>成功申請臺灣企銀網路銀行後，分行給我的密碼單要怎麼用？</span>
					</div>
				</div>
			</a>
			<div id="popup1-61" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>
								成功申請網路銀行後，分行會給您約定書（客戶收執聯）及密碼單各一份。使用方式如下：密碼單包含使用者名稱、簽入密碼及交易密碼，請於一個月內至本行網站https://ebank.tbb.com.tw點選『網路銀行』，輸入<font color="red">統一編號</font>、<font color="red">使用者名稱</font>及<font color="red">簽入密碼</font>即可登入網路銀行，首次登入請變更密碼。<br>
								登入後執行各項查詢、轉帳、繳費交易及登入憑證註冊中心申請金融XML憑證，請鍵入<font color="red">交易密碼</font>。
							</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q2 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-62" aria-expanded="true"
				aria-controls="popup1-62">
				<div class="row">
					<span class="col-1">Q2</span>
					<div class="col-11">
						<span>為什麼有簽入密碼、交易密碼，有何不同？</span>
					</div>
				</div>
			</a>
			<div id="popup1-62" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>
								簽入密碼：為您登入網路銀行時所須鍵入的密碼，初次使用必須線上變更密碼。<br>
		    					交易密碼：為您執行各種交易密碼(SSL)或登入憑證註冊中心申請金融XML憑證時所須鍵入的密碼，初次使用必須線上變更密碼。<br>
		    					以上密碼在您申請網路銀行時，由本行製作密碼單給您使用，為保障您的安全，您可以隨時於本行網路銀行『個人化設定』辦理密碼變更。
							</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q3 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-63" aria-expanded="true"
				aria-controls="popup1-63">
				<div class="row">
					<span class="col-1">Q3</span>
					<div class="col-11">
						<span>密碼忘記了，該怎麼辦？</span>
					</div>
				</div>
			</a>
			<div id="popup1-63" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<ol class="ttb-result-list terms">
								<li>1.本行提供留存手機號碼之一般網路銀行客戶，以本行有效晶片金融卡搭配讀卡機，重新設定一般網路銀行使用者名稱、簽入及交易密碼。</li>
								<li>2.如需回臨櫃辦理重置密碼，請依下列辦理:
									<ol>
										<li>一、本行存戶：
											<ol>
												<li>法人戶：請負責人親攜公司登記證件、身份證、存款印鑑及存摺至開戶行辦理『重置密碼』，領取密碼單後請於１個月內登入網路銀行並變更密碼，逾期密碼失效。</li>
												<li>個人戶：請本人親攜身份證件、存款印鑑及存摺至開戶行辦理『重置密碼』，全行收付戶(即通儲戶)，亦可至國內各分行辦理，領取密碼單後請於１個月內登入網路銀行並變更密碼，逾期密碼失效。</li>
											</ol>
										</li>
										<li>二、本行晶片金融卡線上申請戶：請本人攜帶身份證件及存款印鑑至開戶行辦理『重置密碼』，全行收付戶(即通儲戶)，亦可至國內各分行辦理，領取密碼單後請於１個月內登入網路銀行並變更密碼，逾期密碼失效。</li>
										<li>三、本行信用卡線上申請戶：請電洽本行客服0800-01-7171處理後之次日起重新線上申請網路銀行。</li>
									</ol>
								</li>
							</ol>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q4 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-64" aria-expanded="true"
				aria-controls="popup1-64">
				<div class="row">
					<span class="col-1">Q4</span>
					<div class="col-11">
						<span>『使用者名稱』應如何辦理變更？</span>
					</div>
				</div>
			</a>
			<div id="popup1-64" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>使用者名稱為6－16位數之英數字，英文至少2位且區分大小寫，不可僅設定數字或設定相同的英數字或連號（例如：不可設定AAAAAA）。如需變更，請簽入網路銀行之「個人化設定」變更。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q5 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-65" aria-expanded="true"
				aria-controls="popup1-65">
				<div class="row">
					<span class="col-1">Q5</span>
					<div class="col-11">
						<span>已向分行申請金融XML憑證及購買憑證載具(i-Key)，電腦需如何操作下載憑證？</span>
					</div>
				</div>
			</a>
			<div id="popup1-65" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>請參照「新手上路」→「憑證註冊中心」，安裝憑證載具(i-Key)驅動程式，再登入「網路銀行」→「憑證註冊中心」→「憑證管理」→「用戶申請憑證」申請及下載憑證。完成申請後，即可使用電子簽章交易。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q6 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-66" aria-expanded="true"
				aria-controls="popup1-66">
				<div class="row">
					<span class="col-1">Q6</span>
					<div class="col-11">
						<span>為什麼凌晨使用網路銀行速度會變慢？</span>
					</div>
				</div>
			</a>
			<div id="popup1-66" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>本行每日凌晨時段（03：01以後）不定時進行系統維護作業，暫停客戶交易約20至30分鐘，此時交易會等待作業完成，致處理變慢，請貴客戶稍後再試，敬請見諒！</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q7 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-67" aria-expanded="true"
				aria-controls="popup1-67">
				<div class="row">
					<span class="col-1">Q7</span>
					<div class="col-11">
						<span>交易不成功訊息代碼如何看？</span>
					</div>
				</div>
			</a>
			<div id="popup1-67" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>每一訊息代碼皆有中文說明，若要知道更清楚的說明，請參閱網路銀行公告之『錯誤代碼』。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q8 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-68" aria-expanded="true"
				aria-controls="popup1-68">
				<div class="row">
					<span class="col-1">Q8</span>
					<div class="col-11">
						<span>為什麼登入網路銀行，會顯示『無法顯示網頁』之訊息，該如何處理？</span>
					</div>
				</div>
			</a>
			<div id="popup1-68" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>本行網路銀行之「網路安全認證」，加密長度達 256 位元， 符合「金融機構辦理電子銀行業務安全控管作業基準」。為保障您個人或企業資料的安全，如您的瀏覽器無法處理 256 位元加密，將無法使用本行之網路銀行服務；如果無法登入網路銀行，請點選超連結至微軟網站更新，但請先確認使用IE的版本為5.5版本以上。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q9 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-69" aria-expanded="true"
				aria-controls="popup1-69">
				<div class="row">
					<span class="col-1">Q9</span>
					<div class="col-11">
						<span>為什麼登入網路銀行，會出現「無法顯示網頁」或「程式即將關閉」或沒有反應，是否系統設定有問題，該如何處理？</span>
					</div>
				</div>
			</a>
			<div id="popup1-69" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>此可能為客戶端IE設定的問題，方法如下：</p>
							<p>請開啟IE，點選IE的工具列中的『工具』→『網際網路選項』再選『進階』，在『設定』中→『安全性』→『使用TLS 1.0』選項不要打勾，請按下《確定》即可，再重試登入。 </p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q10 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-610" aria-expanded="true"
				aria-controls="popup1-610">
				<div class="row">
					<span class="col-1">Q10</span>
					<div class="col-11">
						<span>為什麼上貴行網路銀行登入畫面時，會顯示「你未開啟cookie功能，無法執行網路銀行交易！請開啟Cookie功能後，重新執行本網頁」，該如何處理？</span>
					</div>
				</div>
			</a>
			<div id="popup1-610" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>請在「工具」→「網際網路選項」選擇「隱私」找到cookie選項，並勾選「永遠允許工作階段Cookie」即可。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>