<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
<br/><br/>
<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
<br/><br/><br/>
<div class="ttb-message">
	<h4>臺灣企銀 網路銀行黃金存摺定期定額投資約定條款</h4>
</div>
<table class="print">
	<tr>
		<td style="width:5em;text-align:center">第一條</td>
		<td colspan=2><b>掛牌單位</b></td>
	</tr>
	<tr>
		<td style="text-align:center"></td>
		<td colspan=2>黃金存摺存戶本人（以下簡稱立約人）以臺灣中小企業銀行（以下簡稱貴行）之網路銀行服務系統，進行黃金存摺定期定額投資，貴行以1公克黃金為基本掛牌單位，重量之換算計算至小數點第2位(以下四捨五入)。貴行每一營業日訂定其買進和賣出價格並掛牌之。</td>
	</tr>
		<tr>
			<td style="text-align: center">第二條</td>
			<td colspan=2><b>定期定額投資</b></td>
		</tr>
		<tr>
			<td rowspan=9></td>
			<td>(一)</td>
			<td>立約人得約定每月6、16、26日中任一日或數日為投資日(遇例假日則為次一營業日)，定期定額辦理投資。每次投資金額至少為新臺幣3,000元，並應為新臺幣1,000元之整倍數。</td>
		</tr>
		<tr>
			<td>(二)</td>
			<td>立約人若於投資日或投資日以後始申購定期定額投資者，自下次投資日起開始扣款。</td>
		</tr>
		<tr>
			<td>(三)</td>
			<td>立約人若於臨櫃書面約定定期定額投資之扣款帳號者，該定期定額投資之扣款帳號即依臨櫃約定之扣款帳號；立約人若無臨櫃約定定期定額投資之扣款帳號者，依其臨櫃約定或於網路銀行約定之網路交易指定新臺幣存款帳戶為網路銀行定期定額投資之扣款帳號。</td>
		</tr>
		<tr>
			<td>(四)</td>
			<td><font color=red>立約人應於投資日前一日於指定帳戶留存足額投資款項，否則視為當次不委託辦理投資。</font></td>
		</tr>
		<tr>
			<td>(五)</td>
			<td>立約人同意倘投資日新臺幣指定帳戶同時有數筆款項待扣，而存款餘額不敷時，以貴行執行扣款作業之先後次序為準，存戶不得指定或異議。同一指定帳戶扣取多筆投資款項時，貴行將按黃金存摺帳號順序扣款。</td>
		</tr>
		<tr>
			<td>(六)</td>
			<td><font color=red>貴行於扣款作業完成後，將投資金額依當日貴行基本掛牌單位第1次掛牌賣出價格買進黃金存入存戶之帳戶。</font></td>
		</tr>
		<tr>
			<td>(七)</td>
			<td>如有下列情形之一者，貴行於投資日不辦理扣款投資：</td>
		</tr>
		<tr>
			<td style="text-align: right">1.</td>
			<td>立約人得申請暫停投資，亦得於其後申請恢復投資。</td>
		</tr>
		<tr>
			<td style="text-align: right">2.</td>
			<td><font color=red>立約人如未依本條第(四)款規定留存足額投資款項，因而連續3次未能辦理投資者，貴行將停止扣款投資，視同本契約終止。</font></td>
		</tr>
		<tr>
			<td style="text-align: center">第三條</td>
			<td colspan=2><b>手續費收費標準</b></td>
		</tr>
		<tr>
			<td></td>
			<td colspan=2>貴行依定期定額投資之方式（臨櫃或網路銀行）分別訂定手續費收費標準。屬臨櫃定期定額申購者，於每次投資時，不論扣帳是否成功，均收取定期定額投資手續費新台幣100元；屬網路銀行定期定額申購者，於每次投資時，扣帳成功收取定期定額投資手續費新台幣50元，如扣帳失敗則免。立約人若於定期定額投資後，辦理變更約定（含投資日期、金額、扣款帳號、恢復扣款），依最後變更約定之方式（臨櫃或網路銀行），計算後續定期定額投資手續費。<br />
				前開收費標準於訂約後如有變更或調整，貴行應於生效日六十日前以顯著方式於營業場所、貴行網站公告其內容，並以電子郵件方式使立約人得知調整費用，立約人若對於該變更或調整有異議時，得於前開公告期間內終止本約定事項，逾期未終止者，視為同意該變更或調整。
			</td>
		</tr>
		<tr>
			<td style="text-align: center">第四條</td>
			<td colspan=2><b>不可抗力</b></td>
		</tr>
		<tr>
			<td></td>
			<td colspan=2>如因不可抗力事由或其他原因，包括但不限於斷電、斷線、電信壅塞、網路傳輸干擾、貴行之電腦系統故障或第三人破壞等，致使立約人所為交易或其他指示遲延完成或無法按立約人指示完成、或致使貴行未能提供本系統服務者，立約人同意貴行不負任何賠償責任。</td>
		</tr>
		<tr>
			<td style="text-align: center">第五條</td>
			<td colspan=2><b>系統服務</b></td>
		</tr>
		<tr>
			<td></td>
			<td colspan=2>立約人同意貴行於貴行網站公告本系統新增或異動（含調整、變更或取消）之服務項目及其相關規定時，除貴行另有規定外，立約人無須另填申請書，即可使用本系統新增或異動後之服務項目；立約人一經進入本系統並使用該新增或異動後之服務項目時，即視為立約人同意依貴行網站所公告本系統新增或異動服務項目之相關規定辦理，且同意受該規定拘束。</td>
		</tr>
		<tr>
			<td style="text-align: center">第六條</td>
			<td colspan=2>立約人應一併遵守其與貴行訂定之黃金存摺開戶約定條款之約定及相關法令之規定。</td>
		</tr>
		<tr>
			<td style="text-align: center">第七條</td>
			<td colspan=2><b>投資風險</b></td>
		</tr>
		<tr>
			<td></td>
			<td colspan=2><font color=red><b>國際黃金價格有漲有跌，立約人投資黃金可能產生本金收益或損失，最大可能損失為買進金額之全部，請自行審慎判斷投資時機並承擔投資風險，辦理黃金存摺各項交易，如有涉及贈與、繼承及應繳稅捐等情事，悉由立約人或繼承人自行申報與負擔，黃金存摺不計算利息，亦非屬存款保險條例規定之標的，不受存款保險保障。</b></font>
			</td>
		</tr>
		<tr>
			<td style="text-align: center">第八條</td>
			<td colspan=2>立約人同意貴行得依業務需要，隨時修改本約定書開立帳戶之相關服務內容，並在貴行網站上公告其內容，以代通知，修改後之交易，立約人願自動適用異動後之服務內容，毋須另行約定。</td>
		</tr>
	</table>
<br/><br/>
</body>
</html>