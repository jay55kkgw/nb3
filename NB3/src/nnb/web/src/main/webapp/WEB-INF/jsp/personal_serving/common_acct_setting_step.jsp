<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
    <%@ include file="../__import_head_tag.jsp"%>	
    <%@ include file="../__import_js.jsp" %>	
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 帳戶查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.FX_Account_Inquiry" /></li>
		</ol>
	</nav>

		<!-- menu、登出窗格 --> 
 		   <div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單及主頁內容 -->
		 </div><!-- content row END --> 
        <main class="col-12">
		<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<table class="table" data-toggle-column="first">
					<tr>
						<!--查詢時間 -->
			        	<td><spring:message code="LB.Inquiry_time"/>:</td>
				        <td>${loan_detail.data.CMQTIME}</td>
			      	</tr>
				    <tr>
				    	<!--設定結果 -->
				        <td><spring:message code="LB.Setting_result"/></td>
				        <td><font color="red">stat</td>
				    </tr>
			      	<tr>
			      		<!--好記名稱 -->
				        <td><spring:message code="LB.Favorite_name"/></td>
				        <td>${loan_detail.data.DPGONAME}</td>
				    </tr>
			      	<tr>
			      		<!--轉入銀行 -->
				        <td class="ColorCell"><spring:message code="LB.X0431" /></td>
				        <td>${loan_detail.data.DPGONAME}</td>
			      	</tr>
			      	<tr>
			      		<!--轉入帳號 -->
				        <td><spring:message code="LB.Payees_account_no"/></td>
				        <td>${loan_detail.data.DPGONAME}</td>
			      	</tr>            
				</table>
				<input type="button" class="ttb-button btn-flat-orange" id="pageshow" value="<spring:message code="LB.Display_as_web_page"/>"/>
				<input id="reset" name="reset" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter"/>"/>	
			</section>
			<!-- 		main-content END -->
		</main>
	<%@ include file="../index/footer.jsp"%>
	<script type="text/javascript">
		$(document).ready(function(){
				initFootable();
					
				$("#printbtn").click(function(){
					var i18n = new Object();
					i18n['jspTitle']='<spring:message code="LB.FX_Demand_Deposit_Balance"/>'
					var params = {
						"jspTemplateName":"fcy_balance_query_print",
						"jspTitle":i18n['jspTitle'],
						"CMQTIME":"${fcy_balance_query.data.CMQTIME}",
						"COUNT":"${fn:length(list_Size)}"
					};
					openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
				});
			});
		</script>	
	</body>
</html>