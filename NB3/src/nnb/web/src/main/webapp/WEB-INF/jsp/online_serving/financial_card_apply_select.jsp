<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="bs" value="${financial_card_apply.data}"></c:set>

<!DOCTYPE html>
<html>
<head>

	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp"%>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">

	<script type="text/javascript">
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 50);
			// 開始跑下拉選單並完成畫面
			setTimeout("init()", 400);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
			// 初始化時隱藏span
			$("#hideblock").hide();
		});

		function init() {		
			
			$("#formId").validationEngine({
				binded: false,
				promptPosition: "inline"
			});
			
			
			$("#CMSUBMIT").click(function (e) {
				// 打開驗證欄位
				$("#hideblock").show();
				// 選取的欄位
				var type = $('input[name="TYPE"]:checked').val();
				$("#ErrorMsg").val(type);
				if(!$('#formId').validationEngine('validate')){
		        	e.preventDefault();
	 			}
				else {
	 				$("#formId").validationEngine('detach');
	 				initBlockUI();
	 				$("#formId").attr("action","${__ctx}/ONLINE/SERVING/financial_card_apply_confirm");
		 	  		$("#formId").submit();
	 			}
			});
		}
		
</script>

</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 轉帳功能申請/取消     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0327" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 快速選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code="LB.X0327"/></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="">
				<!-- 防止重複交易 -->
				<input type="hidden" id="TOKEN" name="TOKEN" value="${sessionScope.transfer_confirm_token}" />
				
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">							
							<div class="ttb-input-item row">
								<span class="input-title"><label><h4><spring:message code= "LB.D0973" /></h4></label></span> 
								<span class="input-block">
									<div class="ttb-input">
										<label class="radio-block">
											<!-- 申請 -->
											<spring:message code="LB.Apply"/>
											<input type="radio" name="TYPE" value="01"
												${bs.applyC } ${bs.applyD } >
											<span class="ttb-radio" ></span>
										</label>
									</div>									
									<div class="ttb-input">
										<label class="radio-block"> 
											<!-- 取消 -->
											<spring:message code="LB.Cancel"/>
											<input type="radio" name="TYPE" value="02"
												${bs.cancleC } ${bs.cancleD } >
											<span class="ttb-radio"></span>
										</label>
									</div>
									<!-- 不在畫面上顯示的span -->
									<span id="hideblock" class="ttb-input">
										<!-- 驗證用的input -->
										<input id="ErrorMsg" name="ErrorMsg" type="text" class="text-input validate[groupRequired[TYPE]]" 
											style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" value="" />
									</span>
								</span>
							</div>	

						</div>
						<input id="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
					</div>
				</div>
			</form>
		</section>
		</main>
		<!-- main-content END -->
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>