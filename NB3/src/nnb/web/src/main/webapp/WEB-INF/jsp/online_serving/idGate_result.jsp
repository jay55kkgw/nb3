<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
</head>
<body>

	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- IDGATE 申請   結果頁 -->
			<li class="ttb-breadcrumb-item active" aria-current="page">申請裝置認證服務</li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">

		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> <!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<h2>
				申請裝置認證服務
			</h2>

			<div class="main-content-block row">
				<div class="col-12 tab-content">
					<div class="ttb-input-block">
						<div class="ttb-message">
							<span> 啟用碼申請成功 </span>
						</div>
					
<!-- 						<div class="ttb-input-item row"> -->
<!-- 	                    	<span class="input-title"> -->
<!-- 	                       		<label> -->
<!-- 	                           		<h4>申請時間</h4> -->
<!-- 								</label> -->
<!-- 							</span> -->
<!-- 							<span class="input-block"> -->
<!-- 								<div class="ttb-input"> -->
<%-- 									<span>${idGate_result.data.applyTime}</span> --%>
<!-- 								</div> -->
<!-- 							</span> -->
<!-- 						</div> -->
						
						<div class="ttb-input-item row">
							<span class="input-title"><label>
									<h4>
										啟用碼
									</h4>
							</label></span> <span class="input-block"><div class="ttb-input">
									<span> 
										${idGate_result.data.opncode}
										（<font style="color: red;">10分鐘</font>內有效，逾期自動作廢）
									</span>
<!-- 									<span class="input-remarks" style="color:red;">啟用碼有效期間10分鐘</span> -->
								</div></span>
						</div>
						
						<div class="ttb-input-item row">
	                    	<span class="input-title">
	                       		<label>
	                           		<h4></h4>
								</label>
							</span>
							<span class="input-block">
								<div class="ttb-input" style="max-width: 500px;">
									<span>
										<p style="color: red;">請立刻登入本行行動銀行APP，於頁面底下功能項，點選「更多」＞「設定」＞「快速登入與安全快速交易設定」，新增裝置並輸入啟用碼完成設定。</p>
									</span>
								</div>
							</span>
						</div>
<!-- 						<div class="ttb-input"> -->
<!-- 							<span><p class="text-center" style="color: red;">※提醒您，請立刻登入本行行動銀行APP點選「註冊及快登快交設定」功能並完成新增裝置，逾10分鐘後註冊碼會自動作廢。</p></span> -->
<!-- 						</div> -->

					</div><!-- 					ttb-input-block END -->
				</div><!-- 				col-12 tab-content END -->
			</div><!-- 			main-content-block row  END-->
		</section><!-- 		main-content END --> 
		
		</main>
	</div>
	<!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>

	<script type="text/javascript">
			$(document).ready(function(){
			});
		</script>

</body>
</html>