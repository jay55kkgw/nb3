<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<meta http-equiv="Content-Language" content="zh-tw">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css"
	href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript"
	src="${__ctx}/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css"
	href="${__ctx}/css/jquery.datetimepicker.css">
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 帳戶查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Account_Inquiry" /></li>
    <!-- 進口到單查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0161" /></li>
		</ol>
	</nav>



	<!-- 	快速選單及主頁內容 -->
	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->

</div>

		<!-- 		主頁內容  -->

		<section id="main-content" class="container">
			<h2><spring:message code="LB.W0161" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<!-- 下拉式選單-->
			<div class="print-block">
				<select class="minimal" id="actionBar" onchange="formReset()">
					<option value="">
						<spring:message code="LB.Downloads" />
					</option>
					<option value="excel">
						<spring:message code="LB.Download_excel_file" />
					</option>
					<!-- 						下載為excel檔 -->
					<option value="txt">
						<spring:message code="LB.Download_txt_file" />
					</option>
					<!-- 						下載為txt檔 -->
				</select>
			</div>
			<div class="main-content-block row">
				<div class="col-12">
					<form id="formId" action="${__ctx}/download" method="post">
						<input type="hidden" id="back" name="back" value="">
						<!-- 下載用 -->
						<!-- 	TODO downloadFileName	要改為進口到單查詢 -->
						<input type="hidden" name="downloadFileName" value="<spring:message code="LB.W0161" />" /> 
						<input type="hidden" name="CMQTIME" value="${import_order_query_result.data.CMQTIME}" />
						<input type="hidden" name="CMPERIOD" value="${import_order_query_result.data.CMPERIOD}" /> 
						<input type="hidden" name="CMRECNUM" value="${import_order_query_result.data.CMRECNUM}" /> 
						<input type="hidden" name="downloadType" id="downloadType" /> 
						<input type="hidden" name="templatePath" id="templatePath" />
						<!--繼續查詢 -->
	                    <input type="hidden" id="USERDATA" name="USERDATA" value="${import_order_query_result.data.USERDATA}" />
	                    <input type="hidden" name="TDATE" value="${import_order_query_result.data.TDATE}" />
	                    <input type="hidden" name="FDATE" value="${import_order_query_result.data.FDATE}" />
	                    <input type="hidden" name="LCNO" value="${import_order_query_result.data.orgLCNO}" />
	                    
						<!-- EXCEL下載用 -->
						<input type="hidden" name="headerRightEnd" value="11" /> 
						<input type="hidden" name="headerBottomEnd" value="12" /> 
						<input type="hidden" name="rowStartIndex" value="13" /> 
						<input type="hidden" name="rowRightEnd" value="11" />

						<!-- TXT下載用 -->
						<input type="hidden" name="txtHeaderBottomEnd" value="9" /> <input
							type="hidden" name="txtHasRowData" value="true" /> <input
							type="hidden" name="txtHasFooter" value="false" />

						<ul class="ttb-result-list">
							<li>
								<!-- 查詢時間  -->
								<h3>
									<spring:message code="LB.Inquiry_time" />:
								</h3>
								<p>
									${import_order_query_result.data.CMQTIME}
								</p> 
							</li>
							<li>	
							<!--查詢期間 -->
								<h3>
									<spring:message code="LB.Inquiry_period_1" />：
								</h3>
								<p>
									 ${import_order_query_result.data.CMPERIOD }
								</p> 
							</li>
							<li>
								<!--信用狀號碼 -->
								<h3>
									<spring:message code="LB.W0085" />：  
								</h3>
								<p>	
									${import_order_query_result.data.LCNO} <!--託收編號 -->
								</p>
							</li>
							<li>
									<!-- 資料總數  -->
									<h3>
									<spring:message code="LB.Total_records" />：
									</h3>
									<p>
									  ${import_order_query_result.data.CMRECNUM }<input type="hidden"
										name="CMRECNUM" value=" ${import_order_query_result.data.CMRECNUM }"
										readonly="readonly" />
									<spring:message code="LB.Rows" />
								    </p>
							</li>

						</ul>
						<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
							<thead>
								<tr>
									<th ><spring:message code="LB.W0166" /></th>
									<th><spring:message code="LB.W0085" />/<spring:message code="LB.W0167" /></th>
									<th ><spring:message
											code="LB.Currency" /></th>
									<th ><spring:message code="LB.W0169" /></th>
									<th><spring:message code="LB.X0010" /></th>
									<th><spring:message code="LB.W0170" />/<spring:message code="LB.W0171" /></th>
									<th><spring:message code="LB.W0172" /></th>
									<th><spring:message code="LB.W0094" /></th>
									<th><spring:message code="LB.W0174" /></th>
									<th><spring:message code="LB.Note" /></th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="dataList"
									items="${import_order_query_result.data.REC }">
									<tr>
										<td class="text-center">${dataList.RIBDATE }</td>
										<td class="text-center">${dataList.RLCNO }<br>${dataList.RIBNO }</td>
										<td class="text-center">${dataList.RBILLCCY}</td>
										<td class="text-right">${dataList.RBILLAMT}</td>
										<td class="text-right">${dataList.RADVFIXR}</td>
										<td class="text-center">${dataList.RINTSTDT}<br>${dataList.RINTDUDT}</td>
										<td class="text-center">${dataList.RCFMDATE}</td>
										<td class="text-right">${dataList.RLCAMT }</td>
										<td class="text-center">${dataList.RBENNAME}</td>
										<td class="text-center">${dataList.RMARK}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</form>
					<spring:message code="LB.Back_to_previous_page" var="cBack"></spring:message>
						<input type="button" class="ttb-button btn-flat-gray"
							id="CMBACK" value="${cBack}" />

						<spring:message code="LB.Print" var="cmPrint"></spring:message>
						<input type="button" class="ttb-button btn-flat-orange"
							id="printbtn" value="${cmPrint}" />
							
							                        <!-- 繼續查詢 -->
                        <c:if test="${ import_order_query_result.data.TOPMSG != 'OKLR' }">
                         <!--<spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>-->
                         <input type="button" name="CMCONTINU" id="CMCONTINU" value="<spring:message code="LB.X0151" />" class="ttb-button btn-flat-orange">
                        </c:if>
				</div>
			</div>
			<!-- 				</form> -->
		</section>
		<!-- 		main-content END -->
		</main>

	<!-- 	content row END -->
	<%@ include file="../index/footer.jsp"%>
	<!--   Js function -->
	<script type="text/JavaScript">
		$(document).ready(function() {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 50);
			// 開始跑下拉選單並完成畫面
			setTimeout("init()", 400);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
			setTimeout("initDataTable()",100);

		});

		function init() {
			// 初始化時隱藏span
			$("#hideblocka").hide();
			$("#hideblockb").hide();

			datetimepickerEvent();

			getTmr();

			//initFootable();

			$("formId").validationEngine({
				binded : false,
				promptPosition : "inline"
			});

			//列印
			$("#printbtn")
					.click(
							function() {
								var params = {
									"jspTemplateName" : "import_order_query_result_print",
									"jspTitle" : "<spring:message code= "LB.W0161" />",
									"CMQTIME" : "${import_order_query_result.data.CMQTIME}",
									"CMPERIOD" : "${import_order_query_result.data.CMPERIOD}",
									"CMRECNUM" : "${import_order_query_result.data.CMRECNUM }"
								};
								openWindowWithPost(
										"${__ctx}/print",
										"height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",
										params);
							});
			//上一頁按鈕
			$("#CMBACK").click(function() {
				var action = '${__ctx}/FCY/ACCT/f_import_order_query';
				$('#back').val("Y");
				$("form").attr("action", action);
				initBlockUI();
				$("form").submit();
			});
			$("#CMCONTINU").click(function() {
				var action = '${__ctx}/FCY/ACCT/f_import_order_query_result';
				$('#back').val("Y");
				$("form").attr("action", action);
				initBlockUI();
				$("form").submit();
			});
		}

		function getTmr() {
			var today = new Date();
			today.setDate(today.getDate());
			var y = today.getFullYear();
			var m = today.getMonth() + 1;
			var d = today.getDate();
			if (m < 10) {
				m = "0" + m
			}
			if (d < 10) {
				d = "0" + d
			}
			var tmr = y + "/" + m + "/" + d
			$('#FDATE').val(tmr);
			$('#TDATE').val(tmr);
		}

		// 日曆欄位參數設定
		function datetimepickerEvent() {
			$(".FDATE").click(function(event) {
				$('#FDATE').datetimepicker('show');
			});
			$(".TDATE").click(function(event) {
				$('#TDATE').datetimepicker('show');
			});
			jQuery('.datetimepicker').datetimepicker({
				timepicker : false,
				closeOnDateSelect : true,
				scrollMonth : false,
				scrollInput : false,
				format : 'Y/m/d',
				lang: '${transfer}'
			});
		}

		function checkTimeRange() {
			var now = Date.now();
			var twoYm = 63115200000;
			var twoMm = 5259600000;

			var startT = new Date($('#FDATE').val());
			var endT = new Date($('#TDATE').val());
			var distance = now - startT;
			var range = endT - startT;

			var limitS = new Date(now - twoYm);
			var limitE = new Date(startT.getTime() + twoMm);
			if (distance > twoYm) {
				var m = limitS.getMonth() + 1;
				var time = limitS.getFullYear() + '/' + m + '/'
						+ limitS.getDate();
				// 起始日不能小於
				var msg = '<spring:message code="LB.Start_date_check_note_1" />'
						+ time;
				//alert(msg);
				return false;
			} else {
				if (range > twoMm) {
					var m = limitE.getMonth() + 1;
					var time = limitE.getFullYear() + '/' + m + '/'
							+ limitE.getDate();
					// 終止日不能大於
					var msg = '<spring:message code="LB.End_date_check_note_1" />'
							+ time;
					//alert(msg);
					return false;
				}
			}
			return true;
		}

		//選項
		function formReset() {
// 			initBlockUI();
			if ($('#actionBar').val() == "excel") {
				$("#downloadType").val("OLDEXCEL");
				$("#templatePath").val("/downloadTemplate/f_import_order.xls");
			} else if ($('#actionBar').val() == "txt") {
				$("#downloadType").val("TXT");
				$("#templatePath").val("/downloadTemplate/f_import_order.txt");
			}
// 			ajaxDownload("${__ctx}/ajaxDownload", "formId",
// 					"finishAjaxDownload()");
			$("#formId").attr("target", "");
			$("#formId").submit();
			$('#actionBar').val("");
		}
		function finishAjaxDownload() {
			$("#actionBar").val("");
			unBlockUI(initBlockId);
		}
	</script>
</body>
</html>
