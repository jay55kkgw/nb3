<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<style>
	.Line-break{
		white-space:normal;
		word-break:break-all;
		width:100px;
		word-wrap:break-word;
	}
</style>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 轉出記錄查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0002" /></li>
		</ol>
	</nav>



	<!-- 	快速選單及主頁內容 -->
	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->

		<main class="col-12"> <!-- 		主頁內容  -->

		<section id="main-content" class="container">

			<h2>
				<spring:message code="LB.X0002" />
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<!-- 下拉式選單-->
			<div class="print-block">
				<select class="minimal" id="actionBar" onchange="formReset()">
					<option value=""><spring:message code="LB.Downloads" /></option>
					<!-- 						下載Excel檔 -->
					<option value="excel"><spring:message
							code="LB.Download_excel_file" /></option>
					<!-- 						下載為txt檔 -->
					<option value="txt"><spring:message
							code="LB.Download_txt_file" /></option>
				</select>
			</div>
			<div class="main-content-block row">
				<div class="col-12 printClass">
					<ul class="ttb-result-list">
						<li>
							<!-- 查詢時間 -->
							<h3>
								<spring:message code="LB.Inquiry_time" />
								：
							</h3>
							<p>${f_transfer_record_query_result.data.CMQTIME}</p>
						</li>
						<li>
							<!-- 查詢期間 -->
							<h3>
								<spring:message code="LB.Inquiry_period_1" />
								：
							</h3>
							<p>${f_transfer_record_query_result.data.CMPERIOD}</p>
						</li>
						<li>
							<!-- 資料總數 -->
							<h3>
								<spring:message code="LB.Total_records" />
								：
							</h3>
							<p>
								${f_transfer_record_query_result.data.COUNTS}
								<spring:message code="LB.Rows" />
							</p>
						</li>
						<li>
							<h3>
								<spring:message code="LB.Report_name" />
							</h3>
							<p>
								<spring:message code="LB.W0351" />
							</p>
						</li>
					</ul>
					<!-- 全部 -->
					<table class="stripe table-striped ttb-table dtable" data-show-toggle="first">
						<thead>
							<tr>
								<th><spring:message code="LB.Transfer_date" /></th>
								<th><spring:message code="LB.Payers_account_no" /></th>
								<th><spring:message code="LB.Deducted" /></th>
								<th><spring:message code="LB.D0433" /></th>
								<th><spring:message code="LB.Buy" /></th>
								<th><spring:message code="LB.Exchange_rate" /></th>
								<th><spring:message code="LB.D0507" /><br> <spring:message code="LB.W0345" /></th>
								<th><spring:message code="LB.W0346" /></th>
								<th><spring:message code="LB.Transaction_type" /><hr/><spring:message code="LB.Note" /></th>
<%-- 								<th><spring:message code="LB.Note" /></th> --%>
								<th><spring:message code="LB.W0078" /><hr/><spring:message code="LB.W0365" /></th>
<%-- 								<th><spring:message code="LB.W0365" /></th> --%>
							</tr>
						</thead>
						<c:if test="${empty f_transfer_record_query_result.data.REC}">
							<tbody>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							</tbody>
						</c:if>
						<c:if test="${not empty f_transfer_record_query_result.data.REC}">
							<tbody>
								<c:forEach var="dataList"
									items="${ f_transfer_record_query_result.data.REC }"
									varStatus="data">
									<tr>
										<td class="text-center">${dataList.FXTXDATE}</td>
										<td class="text-center">${dataList.FXWDAC}</td>
										<td class="text-center">${dataList.FXWDCURR}<br>${dataList.FXWDAMT}</td>
										<td class="text-center">${dataList.FXSVAC}</td>
										<td class="text-center">${dataList.FXSVCURR}<br>${dataList.FXSVAMT}</td>

										<td class="text-right">${dataList.FXEXRATE}</td>
										<td class="text-center">${dataList.FXEFEECCY}<br>${dataList.FXEFEE}<br>${dataList.FXTELFEE}</td>
										<td class="text-right">${dataList.FXOURCHG}</td>
										<td class="text-center Line-break">${dataList.FXTRANFUNC}<hr/>
											<c:if test="${empty dataList.FXTXMEMO}">
												-
											</c:if>
											<c:if test="${not empty dataList.FXTXMEMO}">
												${dataList.FXTXMEMO}
											</c:if>
										</td>
										<td class="text-center">
											<c:if test="${dataList.FXREMAILFLAG.equals('')}">
												<input
													type="button" class="ttb-sm-btn btn-flat-orange"
													id="${data.count}mail" name="mail"
													onclick="sendMail(${data.count})"
													value="<spring:message code= "LB.X1493" />"
													${dataList.FXREMAILFLAG} /> <input type="hidden"
													id="${data.count}A" value="${dataList.ADTXNO}"/>
											</c:if>
											<c:if test="${dataList.FXREMAILFLAG.equals('disabled')}">
												<input
													type="button" class="ttb-sm-btn btn-flat-gray"
													id="${data.count}mail" name="mail"
													onclick="sendMail(${data.count})"
													value="<spring:message code= "LB.X1493" />"
													${dataList.FXREMAILFLAG} /> <input type="hidden"
													id="${data.count}A" value="${dataList.ADTXNO}"/>
											</c:if>
	
											<br/>
											<c:if test="${dataList.FXCERTFLAG.equals('')}">
												<input style="margin-top: 5px;"
													type="submit" class="ttb-sm-btn btn-flat-orange" name="mail"
													id="mail"
													value="<spring:message code="LB.Transaction_document" />"
													onclick="openPostWindow('${dataList.ADTXNO}','${dataList.ADOPID}')" />
												
											</c:if>
											<c:if test="${dataList.FXCERTFLAG.equals('disabled')}">
												<input style="margin-top: 5px;"
													type="submit" class="ttb-sm-btn btn-flat-gray" name="mail"
													id="mail"
													value="<spring:message code="LB.Transaction_document" />"
													disabled />
											</c:if>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</c:if>
					</table>
					<input type="button" id="previous" class="ttb-button btn-flat-gray"
						value="<spring:message code="LB.Back_to_previous_page" />" /> <input
						type="button" class="ttb-button btn-flat-orange" id="printbtn"
						value="<spring:message code="LB.Print" />" />
				</div>
			</div>
			<div class="text-left">
				<ol class="description-list list-decimal text-left">
					<p>
						<spring:message code="LB.Description_of_page" />
					</p>
					<li><span><spring:message
								code="LB.F_Transfer_Record_Query_P2_D1" /></span></li>
					<li><span><spring:message
								code="LB.F_Transfer_Record_Query_P2_D2" /></span></li>
					<li><span><spring:message
								code="LB.F_Transfer_Record_Query_P2_D3" /></span></li>
					<li><span><spring:message
								code="LB.F_Transfer_Record_Query_P2_D4" /></span></li>
					<li><span><spring:message
								code="LB.F_Transfer_Record_Query_P2_D5" /></span></li>
					<li style="color: red;"><span><spring:message
								code="LB.F_Transfer_Record_Query_P2_D6" /></span></li>
				</ol>
			</div>
			<form id="formId" action="${__ctx}/download" method="post">
				<!-- 下載用 -->
				<input type="hidden" name="downloadFileName"
					value="<spring:message code="LB.X0002" />" />
				<!--匯出匯款查詢 -->
				<input type="hidden" name="CMQTIME"
					value="${f_transfer_record_query_result.data.CMQTIME}" /> <input
					type="hidden" name="CMPERIOD"
					value="${f_transfer_record_query_result.data.CMPERIOD}" /> <input
					type="hidden" name="COUNT"
					value="${f_transfer_record_query_result.data.COUNTS}" /> <input
					type="hidden" name="downloadType" id="downloadType" /> <input
					type="hidden" name="templatePath" id="templatePath" /> <input
					type="hidden" name="hasMultiRowData" value="false" />
				<!-- EXCEL下載用 -->
				<!-- headerRightEnd  資料列以前的右方界線
						 headerBottomEnd 資料列到第幾列 從0開始
						 rowStartIndex 資料列第一列的位置
						 rowRightEnd 資料列用方的界線
					 -->
				<input type="hidden" name="headerRightEnd" value="10" /> <input
					type="hidden" name="headerBottomEnd" value="6" /> <input
					type="hidden" name="rowStartIndex" value="7" /> <input
					type="hidden" name="rowRightEnd" value="10" /> <input
					type="hidden" name="footerStartIndex" value="9" /> <input
					type="hidden" name="footerEndIndex" value="15" /> <input
					type="hidden" name="footerRightEnd" value="10" />
				<!-- TXT下載用
						txtHeaderBottomEnd需為資料第一列(從0開始)-->
				<input type="hidden" name="txtHeaderBottomEnd" value="11" /> <input
					type="hidden" name="txtHasRowData" value="true" /> <input
					type="hidden" name="txtHasFooter" value="true" />
			</form>
			<!-- 				</form> -->
		</section>
		<!-- 		main-content END --> </main>
	</div>
	<!-- 	content row END -->


	<%@ include file="../index/footer.jsp"%>

	<script type="text/javascript">

      

      </script>

	<script type="text/javascript">
			$(document).ready(function(){
				// HTML載入完成後開始遮罩
				setTimeout("initBlockUI()",10);
				// 開始查詢資料並完成畫面
				setTimeout("init()",20);
				// 解遮罩
				setTimeout("unBlockUI(initBlockId)",500);	
				setTimeout("initDataTable()",100);
			});
			function init(){
				//initFootable();
				//上一頁按鈕
				$("#previous").click(function() {
					initBlockUI();
					fstop.getPage('${pageContext.request.contextPath}'+'/FCY/ACCT/RESERVATION/f_transfer_record_query','', '');
				});

				$("#printbtn").click(function(){
					var params = {
						"jspTemplateName":"f_transfer_record_query_print",
						"jspTitle":"<spring:message code= "LB.W0064" />",//轉出紀錄查詢
						"CMPERIOD":"${f_transfer_record_query_result.data.CMPERIOD}",
						"CMQTIME":"${f_transfer_record_query_result.data.CMQTIME}",
						"COUNT":"${f_transfer_record_query_result.data.COUNTS}",
					};
					openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
				});		
				
			}
			// mail重送
		$("#errorBtn1").click(function() {
			$('#error-block').hide();
		});
			function sendMail(dataCount){
// 				initBlockUI();
				var reMail;
				var times = "${f_transfer_record_query_result.data.EMAILTIMES}";
				var data = $('#'+dataCount+'A').val();
// 				alert("ajax進行中"+data);
				uri = '${__ctx}'+"/NT/ACCT/RESERVATION/A3000_mail_aj";
				console.log("f_transfer_record_query_result_mail: " + uri);
				rdata = { ADTXNO: data, RECTYPE: "FX", TIMES: times };
				var bs = fstop.getServerDataEx(uri, rdata, false);				
				console.log("A3000_mail_aj_result: " + bs.result);
				// 錯誤訊息
				if(!bs.result){
					reMail = bs.data.faild;
					//alert(reMail);
					errorBlock(
							null, 
							null,
							[reMail], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				}else{
					reMail = bs.data.success;
					//執行過兩次將按鈕失效
					if(reMail >= times){
						$("#"+dataCount+'mail').prop("disabled", true);
						$("#"+dataCount+'mail').prop("class", "ttb-sm-btn btn-flat-gray");
						//<!-- 已發送2次 -->
						//alert("<spring:message code= "LB.Alert002" />");
						errorBlock(
								null, 
								null,
								['<spring:message code= 'LB.Alert003' />'], 
								'<spring:message code= "LB.Quit" />', 
								null
							);

					}else{
						//<!-- 已發送 -->
						
						//alert("<spring:message code= "LB.Alert003" />");		
						errorBlock(
								null, 
								null,
								['<spring:message code= 'LB.Alert003' />'], 
								'<spring:message code= "LB.Quit" />', 
								null
							);
					}
				}
// 				unBlockUI();
			}
				//選項
			 	function formReset() {
			 		//initBlockUI();
		 			if ($('#actionBar').val()=="excel"){
						$("#downloadType").val("OLDEXCEL");
						$("#templatePath").val("/downloadTemplate/f_transfer_record_query.xls");
			 		}else if ($('#actionBar').val()=="txt"){
						$("#downloadType").val("TXT");
						$("#templatePath").val("/downloadTemplate/f_transfer_record_query.txt");
			 		}
		    		//ajaxDownload("${__ctx}/ajaxDownload","formId","finishAjaxDownload()");
					$("#formId").attr("target", "");
					$("#formId").submit();
					$('#actionBar').val("");
				}
				function finishAjaxDownload(){
					$("#actionBar").val("");
					unBlockUI();
				}
				//交易單據顯示
				function openPostWindow(adtxno,txid){
					window.open('${__ctx}/FCY/COMMON/fxcertpreview?TXID='+txid+'&ADTXNO='+adtxno);
				}
// 				function openPostWindow(url, data, name)      
// 				  {     
// 				  	console.log("WORK");
// 				     var tempForm = document.createElement("form");     
// 				     tempForm.id="tempForm1";     		  
// 				     tempForm.method="post";     
// 				  	 //url
// 				     tempForm.action=url;     
// 				     //open方法不能设置请求方式，一般網页的post都是通過form来實现的。
// 				     //如果僅模拟form的提交方式，open方法里可设置分頁属性的参数又不能用。
// 				     //最后想办法两者结合的方式，将form的target设置成和open的name参数一样的值，通过浏览器自動識別實现了将内容post到新分頁中
// 				     tempForm.target=name;     
// 				     var hideInput = document.createElement("input");     
// 				     hideInput.type="hidden";     
// 				     //傳入参数名,相當於get请求中的content=
// 				     hideInput.name= "content";
// 				     //傳入数据
// 				     hideInput.value= data;   
// 				     tempForm.appendChild(hideInput);      
// 				     var event = new Event('onsubmit');
// 				     tempForm.addEventListener("onsubmit",function(){ openWindow(name); });   
// 				     document.body.appendChild(tempForm);     
// 				     tempForm.dispatchEvent(event);   
// 				     //必须手動觸發，否则只能看到頁面刷新而没有打开新分頁
// 				     tempForm.submit();   
// 				     document.body.removeChild(tempForm);   
// 				}   
				function openWindow(name)     
				{     
				     window.open('about:blank',name,'height=400, width=400, top=0, left=0, toolbar=yes, menubar=yes, scrollbars=yes, resizable=yes,location=yes, status=yes');      
				}     

		</script>

</body>
</html>