<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<!--舊版驗證-->
<script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>

<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>

<style type="text/css">
	#careerTable{
		border:solid 2px;
	}
	#careerTable tr{
		border:solid 2px;
	}
	#careerTable td{
		border:solid 2px;
	}
</style>

<script type="text/javascript">

$(document).ready(function(){
 	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	// 開始查詢資料並完成畫面
	setTimeout("init()", 20);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
});

function init(){
	
	// 初始化後隱藏span( 表單驗證提示訊息用的 )
	$(".hideblock").hide();
	// 表單驗證
	$("#formId").validationEngine({ binded: false, promptPosition: "inline" });

	// 是否領有全民健康保險重大傷病證明
	if(${MARK1 == 'Y'}){
		$("input[name=MARK1][value=Y]").attr("checked",true);
	}
	else{
		$("input[name=MARK1][value=N]").attr("checked",true);
	}
	
	// 回上一頁填入資料
	refillData();
	
	// 當日僅能填寫三次評估
	if(${todayUserKYCCountBoolean == false}){
		//alert("<spring:message code= "LB.Alert093" />");//當日僅能填寫三次評估，造成您的不便，敬請見諒
		errorBlock(
			null, 
			null,
			["<spring:message code= 'LB.Alert093' />"], 
			'<spring:message code= "LB.Quit" />', 
			null
		);
		$("#CMSUBMIT").prop("disabled",true);
	}else{
		errorBlock(
				["<spring:message code= 'LB.X2375' />"], 
				["<li style='list-style:none;'>1.<spring:message code= 'LB.X2376' /></li>","<li style='list-style:none;padding-top: 20px'>2.<spring:message code= 'LB.X2377' /></li>","<li style='list-style:none;padding-top: 20px'>3.<spring:message code= 'LB.X2378' /></li>"],
				null, 
				'<spring:message code= "LB.Quit" />', 
				null
		);
		// 複寫errorBtn1 事件	
		$("#errorBtn1").click(function(e) {
			$('#error-block').hide();
			var email = '${sessionScope.dpmyemail}';
			if(email.length == 0){
				email = "<spring:message code= 'LB.X2599' />";
			}
			errorBlock(
					["<spring:message code= 'LB.X0152' />"], 
					["<li style='list-style:none;'><spring:message code= 'LB.X2594' /></li>","<li style='list-style:none;padding-top: 20px'><b><font color='red'><spring:message code= 'LB.X2595' />？</font><b></li>","<li style='list-style:none;padding-top: 20px'><spring:message code= 'LB.X2596' />&nbsp;<a href='#' style='color: blue;' onclick='gotoEmailSetting()''><spring:message code= 'LB.Internet_banking' /></a> / <spring:message code= 'LB.X2597' /></li>","<li style='list-style:none;padding-top: 20px'><spring:message code= 'LB.X2598' />。</li>","<li style='list-style:none;padding-top: 20px'><spring:message code= 'LB.Email' />："+email+"</li>"],
					null, 
					'<spring:message code= "LB.Confirm" />', 
					null
			);
		});
	}
	
	// 回上一頁
	$("#previous").click(function(){
		$("#formId").append('<input type="hidden" name="back" value="Y" />');	
		$("#formId").attr("action", "${__ctx}" + "${previous}");
		$("#formId").submit();
	});
	
	// 確認按鈕
	$("#CMSUBMIT").click(function(e){
		// 去掉舊的錯誤提示訊息
		$(".formError").remove();
		// 顯示驗證隱藏欄位( 為了讓提示訊息對齊 )
		$(".hideblock").show();
		
		// KYC資料
		var age = parseInt("${age}");
		var degree = "${Q2A}";
		var career = "${CAREER}";
		var salary = parseInt("${salary}");
		var flag1 = "N";
		var flag2 = "N";
		var flag3 = "N";
		
		// 表單驗證隱藏欄位賦值
		$("#Q2").val($("input[name=radioQ2]:checked").val());
		$("#Q5").val($("input[name=radioQ5]:checked").val());
		$("#Q6").val($("input[name=radioQ6]:checked").val());
		$("#Q7").val($("input[name=radioQ7]:checked").val());
		$("#Q8").val($("input[name=radioQ8]:checked").val());
		$("#Q9").val($("input[name=radioQ9]:checked").val());
		$("#Q10").val($("input[name=radioQ10]:checked").val());
		$("#Q12").val($("input[name=radioQ12]:checked").val());
		$("#Q14").val($("input[name=radioQ14]:checked").val());
		$("#Q15").val($("input[name=radioQ15]:checked").val());
		$("#Q16").val($("input[name=MARK1]:checked").val());

		// 表單驗證
		if ( !$('#formId').validationEngine('validate') ) {
			e = e || window.event; // for IE
			e.preventDefault();
		} else {
			// 資料檢核
			if(!chkQuery()){
				return false;
			} else {
				// KYC隱藏資料
				if($("#Q2").val() != degree){
					flag1 = "Y";
				}
				if($("#SRCFUND").val() != career){
					flag2 = "Y";
				}
				var newsalary = parseInt($("#newsalary").val());
				if(window.console){console.log("newsalary=" + newsalary);}
				
				if(newsalary * 10000 != salary){
					flag3 = "Y";
				}
				$("#UPD_FLG").val(flag1 + flag2 + flag3);
				$("#Q2A").val(degree);
				$("#Q3A").val(career);
				$("#Q4A").val(salary);
				
				// 資料檢核，過了則送出表單
				$("#formId").submit();
			}
			
		}
	 	
	});
	
 	// 資料檢核
 	function chkQuery(){
 		// 投資經驗
		if($("#Q111").prop("checked") == false && $("#Q112").prop("checked") == false && $("#Q113").prop("checked") == false && $("#Q114").prop("checked") == false){
			//alert("<spring:message code= "LB.Alert095" />");//<!-- 請點選問題十一 -->
			errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.Alert095' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
			);
			$("#Q111").focus();
			return false;      
		}
		if($("#Q111").prop("checked") == true || $("#Q112").prop("checked") == true || $("#Q113").prop("checked") == true){	  
			if($("#Q114").prop("checked") == true){
				//alert("<spring:message code= "LB.Alert096" />");//<!-- 問題11：因您已勾選A或B或C選項，不能選擇D選項 -->
				errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert096' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
				$("#Q114").focus();
				return false;    
			}
		}
		
		// 金融商品的認識
		if($("#Q131").prop("checked") == false && $("#Q132").prop("checked") == false && $("#Q133").prop("checked") == false && $("#Q134").prop("checked") == false){
			//alert("<spring:message code= "LB.Alert097" />");//請點選問題十三
			errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.Alert097' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
			);
			$("#Q131").focus();
			return false;
		} 
		if($("#Q131").prop("checked") == true || $("#Q132").prop("checked") == true || $("#Q133").prop("checked") == true){	  
			if($("#Q134").prop("checked") == true){
				//alert("<spring:message code= "LB.Alert098" />");//問題13：因您已勾選A或B或C選項，不能選擇D選項  		
				errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert098' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
				$("#Q134").focus();
				return false;    
			}
		}
		
		// 投資時間需小於您的年齡
		var age = parseInt("${age}");
		if(window.console){console.log("age=" + age);}
		var Q12Value = $("input[name=radioQ12]:checked").val();
		if(window.console){console.log("Q12Value=" + Q12Value);}
		
		if(Q12Value == "B"){
			if(1 >= age){		
				//alert("<spring:message code= "LB.Alert099" />");//問題12：您的投資時間需小於您的年齡，請重新選擇
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert099' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			}
		}
		if(Q12Value == "C"){
			if(2 >= age){
				//alert("<spring:message code= "LB.Alert099" />");//問題12：您的投資時間需小於您的年齡，請重新選擇
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert099' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			}
		}
		if(Q12Value == "D"){
			if(5 >= age){
				//alert("<spring:message code= "LB.Alert099" />");//問題12：您的投資時間需小於您的年齡，請重新選擇
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert099' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			} 
		}
		
		// 學歷與年齡不符
		var Q2Value = $("input[name=radioQ2]:checked").val();
		if(window.console){console.log("Q2Value=" + Q2Value);}
		
		if(Q2Value == "A"){
			if(age < 24){
		   		//alert("<spring:message code= "LB.Alert100" />");//問題2：博士學歷與年齡不符，請重新選擇
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert100' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			}
		}      	        	  	  	  	      	  	  	  	  	        	  	  	  	  	  	  	  		      	  	  	  	  	  	  	  	  	  	
		if(Q2Value == "B"){
			if(age < 22){		
				//alert("<spring:message code= "LB.Alert101" />");//問題2：碩士學歷與年齡不符，請重新選擇
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert101' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			}
		}
		if(Q2Value == "C"){
			if(age < 18){
				//alert("<spring:message code= "LB.Alert102" />");//問題2：大學學歷與年齡不符，請重新選擇
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert102' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			}
		}
		if(Q2Value == "D"){
			if(age < 16){
				//alert("<spring:message code= "LB.Alert103" />");//問題2：專科學歷與年齡不符，請重新選擇
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert103' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			}
		}
		if(Q2Value == "E"){
			if(age < 16){
				//alert("<spring:message code= "LB.Alert104" />");//問題2：高中職學歷與年齡不符，請重新選擇
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert104' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			} 
		}
		
		// 職業與年齡不符
		var Q3Value = $("#Q3").val();
		if(window.console){console.log("Q3Value=" + Q3Value);}
	
		if(Q3Value == "A"){
			if(age < 16){		
				//alert("<spring:message code= "LB.Alert105" />");//問題3：職業與年齡不符，請重新選擇
				errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert105' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
				return false;
			}
		}
		if(Q3Value == "B"){
			if(age < 16){
				//alert("<spring:message code= "LB.Alert105" />");//問題3：職業與年齡不符，請重新選擇
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert105' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
				return false;
			}
		}
		
		return true;
 	}
 	
 	function gotoEmailSetting(){
 		$('#error-block').hide();
 		fstop.getPage('${pageContext.request.contextPath}'+'/PERSONAL/SERVING/mail_setting','', '');//導頁去改信箱
 	}
		
}

//職業
function fillData(){
	$("#SRCFUND_TEXT").val($("input[name=SRCFUND_radio]:checked").val());
	$("#SRCFUND").val($("input[name=SRCFUND_radio]:checked").attr("id"));
	var ID = $("#SRCFUND").val();
	var age = parseInt("${age}");
	
	if(ID == "0615I0"){
		$("#Q3").val("A");
		$("input[id=Q3_show][value=A]").prop("disabled",false);
		$("input[id=Q3_show][value=A]").prop("checked",true);
		$("input[id=Q3_show][value=B]").prop("disabled",true);
		$("input[id=Q3_show][value=C]").prop("disabled",true);
		$("input[id=Q3_show][value=B]").prop("checked",false);
		$("input[id=Q3_show][value=C]").prop("checked",false);
		
	}
	else if(ID=="061410" || ID=="061700" || ID =="061690"|| ID =="061691" || ID =="061692"){
		$("#Q3").val("C");
		$("input[id=Q3_show][value=C]").prop("disabled",false);
		$("input[id=Q3_show][value=C]").prop("checked",true);
		$("input[id=Q3_show][value=A]").prop("disabled",true);
		$("input[id=Q3_show][value=B]").prop("disabled",true);
		$("input[id=Q3_show][value=A]").prop("checked",false);
		$("input[id=Q3_show][value=B]").prop("checked",false);
	}
	else{
		$("#Q3").val("B");
		$("input[id=Q3_show][value=B]").prop("disabled",false);
		$("input[id=Q3_show][value=B]").prop("checked",true);
		$("input[id=Q3_show][value=A]").prop("disabled",true);
		$("input[id=Q3_show][value=C]").prop("disabled",true);
		$("input[id=Q3_show][value=A]").prop("checked",false);
		$("input[id=Q3_show][value=C]").prop("checked",false);
	}
	
}	

// 薪水
function fillSalary(){
	if(!CheckAmount()) {
		return false;
	}
	var newsalary = parseInt($("#newsalary").val());
	if(newsalary * 10000 < 500000){
		$("#Q4").val("A");
		$("input[id=Q4_show][value=A]").prop("disabled",false);
		$("input[id=Q4_show][value=A]").prop("checked",true);
		$("input[id=Q4_show][value=B]").prop("disabled",true);
		$("input[id=Q4_show][value=C]").prop("disabled",true);
		$("input[id=Q4_show][value=D]").prop("disabled",true);
		$("input[id=Q4_show][value=B]").prop("checked",false);
		$("input[id=Q4_show][value=C]").prop("checked",false);
		$("input[id=Q4_show][value=D]").prop("checked",false);
	}
	else if(newsalary * 10000 >= 500000 && newsalary * 10000 < 700000){
		$("#Q4").val("B");
		$("input[id=Q4_show][value=B]").prop("disabled",false);
		$("input[id=Q4_show][value=B]").prop("checked",true);
		$("input[id=Q4_show][value=A]").prop("disabled",true);
		$("input[id=Q4_show][value=C]").prop("disabled",true);
		$("input[id=Q4_show][value=D]").prop("disabled",true);
		$("input[id=Q4_show][value=A]").prop("checked",false);
		$("input[id=Q4_show][value=C]").prop("checked",false);
		$("input[id=Q4_show][value=D]").prop("checked",false);
	}	
	else if(newsalary * 10000 >= 700000 && newsalary * 10000 < 1000000){
		$("#Q4").val("C");
		$("input[id=Q4_show][value=C]").prop("disabled",false);
		$("input[id=Q4_show][value=C]").prop("checked",true);
		$("input[id=Q4_show][value=A]").prop("disabled",true);
		$("input[id=Q4_show][value=B]").prop("disabled",true);
		$("input[id=Q4_show][value=D]").prop("disabled",true);
		$("input[id=Q4_show][value=A]").prop("checked",false);
		$("input[id=Q4_show][value=B]").prop("checked",false);
		$("input[id=Q4_show][value=D]").prop("checked",false);
	}
	else{
		$("#Q4").val("D");
		$("input[id=Q4_show][value=D]").prop("disabled",false);
		$("input[id=Q4_show][value=D]").prop("checked",true);
		$("input[id=Q4_show][value=A]").prop("disabled",true);
		$("input[id=Q4_show][value=B]").prop("disabled",true);
		$("input[id=Q4_show][value=C]").prop("disabled",true);
		$("input[id=Q4_show][value=A]").prop("checked",false);
		$("input[id=Q4_show][value=B]").prop("checked",false);
		$("input[id=Q4_show][value=C]").prop("checked",false);
	}
}

// 投資經驗
function checkQ11(){
	// 選了無經驗
	if($("#Q114").prop("checked")) {
		// 其他經驗選項不可選
		$("#Q111").prop("checked", false);
		$("#Q112").prop("checked", false);
		$("#Q113").prop("checked", false);
		
		$("#Q111").prop('disabled', true);
		$("#Q112").prop('disabled', true);
		$("#Q113").prop('disabled', true);
		
	} // 沒有選無經驗
	else {
		// 可以選其他選項
		$("#Q111").prop('disabled', false);
		$("#Q112").prop('disabled', false);
		$("#Q113").prop('disabled', false);
	}
}

// 對金融商品的認識
function checkQ13(){
	// 選了不認識
	if($("#Q134").prop("checked")) {
		// 其他選項不可選
		$("#Q131").prop("checked", false);
		$("#Q132").prop("checked", false);
		$("#Q133").prop("checked", false);
		
		$("#Q131").prop('disabled', true);
		$("#Q132").prop('disabled', true);
		$("#Q133").prop('disabled', true);
		
	} // 沒有選不認識
	else {
		// 可以選其他選項
		$("#Q131").prop('disabled', false);
		$("#Q132").prop('disabled', false);
		$("#Q133").prop('disabled', false);
	}
}

function CheckAmount() {
	var sAmount = $("#newsalary").val();
	try {
		
		var message = '<spring:message code= 'LB.X2230' />';
		if ( isNaN(sAmount)) {//數字
// 			var message = '欄位請輸入正確的金額格式';
			callErrorBlock(message);
			return false;
		}
		//先TRIM掉，不要用startsWith與endsWith，在舊版瀏覽器會出錯
		sAmount = $.trim(sAmount);
		if (sAmount == "") {
// 			var message = '欄位請輸入正確的金額格式';
			callErrorBlock(message);
			return false;
		}
		if (sAmount.indexOf("+") == 0) {
// 			var message = '欄位請勿輸入加號';
			callErrorBlock(message);
			return false;
		}
		if (sAmount.charAt(0) == "0" && sAmount.length > 1) {//check不以零為開頭
// 			var message = '欄位請勿以零為開頭';
			callErrorBlock(message);
			return false;
		}
		var regex =  /^[0-9]*[1-9][0-9]*$/; 
		if (!regex.test(sAmount)) {
// 			var message = '欄位請輸入整數';
			callErrorBlock(message);
			return false;
		}

		return true;
	} catch (exception) {
 		console.log("檢核時發生錯誤:" + exception);
	}

}
//建立只有離開按鈕的ErrorBlock
function callErrorBlock(message){
	errorBlock(
			null, 
			null,
			[message], 
			'<spring:message code= "LB.Quit" />', 
			null
	);
	
}

//回上一頁填入資料
function refillData() {
	var jsondata = '${previousdata}';
	console.log(jsondata);
	
	if (jsondata) {
		JSON.parse(jsondata, function(key, value) {
			if(key) {
				
				if("SRCFUND"==key){
					$("input[name=SRCFUND_radio][id="+value+"]").prop("checked",true);
				}
				var obj = $("input[name='"+key+"']");
				var type = obj.attr('type');
				
				console.log('--------------------------------');
				console.log('type: ' + type);
				console.log('key: ' + key);
				console.log('value: ' + value);
				console.log('--------------------------------');
				
				if(type == 'text'){
					obj.val(value);
				}
				
				if(type == 'hidden'){
					obj.val(value);
				}

				if(type == 'radio'){
					if(key == 'SRCFUND_radio' ){
						$('#SRCFUND_TEXT').val(value);
					}else{ 
						obj.filter('[value='+value+']').prop('checked', true);
					}
				}
				
				if(type == 'checkbox'){
					obj.prop('checked', true);
				}
				
			}
		});
		
		// 上一步自動重填檢核
		checkQ11();
		checkQ13();
		fillData();
		fillSalary();
	}
}

function gotoEmailSetting(){
	$('#error-block').hide();
	fstop.getPage('${pageContext.request.contextPath}'+'/PERSONAL/SERVING/mail_setting','', '');//導頁去改信箱
}
</script>
</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0262" /></li>
		</ol>
	</nav>

	
	<div class="content row">
		<!-- menu、登出窗格 -->
		<%@ include file="../index/menu.jsp"%>
		
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>預約開立基金戶</h2>
				<div id="step-bar">
					<ul>
						<li class="finished">注意事項與權益</li>
						<c:if test="${FATCA_CONSENT == 'Y'}">
							<li class="finished">FATCA個人客戶身份識別聲明</li>
						</c:if>
						<li class="active">投資屬性調查</li>
						<li class="">開戶資料</li>
						<li class="">確認資料</li>
						<li class="">完成申請</li>
					</ul>
				</div>
				
				<form id="formId" action="${__ctx}${next}" method="post">
					<input type="hidden" name="TXID" value="${TXID}"/>
					<input type="hidden" name="RTC" value="${TXID}"/>
					<input type="hidden" id="UPD_FLG" name="UPD_FLG"/>
           			<input type="hidden" id="Q2A" name="Q2A"/>
    				<input type="hidden" id="Q3A" name="Q3A"/>
    				<input type="hidden" id="Q4A" name="Q4A"/>
    				<input type="hidden" name="RISK" value="${RISK}"/>
    				<input type="hidden" name="OMARK1" value="${MARK1}"/>
    				<!--FATCA所需欄位-->
    				<input type="hidden" name="FATCA_CONSENT" value="${FATCA_CONSENT}"/>
    				<input type="hidden" name="CITY" value="${CITY}"/>
    				
					<div class="main-content-block row">
						<div class="col-12 terms-block questionnaire-block">
							<div class="ttb-message">
								<p>線上客戶投資屬性問卷調查表－（自然人版）</p>
							</div>
							<p class="form-description">倘您不清楚本風險屬性問卷之內容，應不填寫，另可洽本行人員說明。</p>
							<div class="text-left">
								<ul class="questionnaire-list">
									<li>
										<div class="questionnaire-title">
											<p>1. 您的年齡為何？（本題免勾選）</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<p>
												${age}<spring:message code= "LB.W1221" /><!-- 歲 -->
												<input type="hidden" value="${age}" id="age" name="age" >
												<input type="hidden" value="${Q1A}" id="Q1" name="Q1" >
											</p>
										</div>
									</li>
									<li>
										<div class="questionnaire-title">
											<p>2. 您的教育程度為何?</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<ul>
												<li>
													<div class="ttb-input">
														<label class="radio-block">A﹒ 博士
															<input type="radio" value="A" name="radioQ2" />
															<span class="ttb-radio"></span>
														</label>
														<label class="radio-block">B﹒ 碩士
															<input type="radio" value="B" name="radioQ2" />
															<span class="ttb-radio"></span>
														</label>
														<label class="radio-block">C﹒ 大學
															<input type="radio" value="C" name="radioQ2" />
															<span class="ttb-radio"></span>
														</label>
														<label class="radio-block">D﹒ 專科
															<input type="radio" value="D" name="radioQ2" />
															<span class="ttb-radio"></span>
														</label>
														<label class="radio-block">E﹒ 高中職
															<input type="radio" value="E" name="radioQ2" />
															<span class="ttb-radio"></span>
														</label>
														<label class="radio-block">F﹒ 國中以下
															<input type="radio" value="F" name="radioQ2" />
															<span class="ttb-radio"></span>
														</label>
														<!-- 不在畫面上顯示的span -->
														<span class="hideblock">
															<!-- 驗證用的input -->
															<input id="Q2" name="Q2" type="text" class="text-input validate[required]"
																style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
														</span>
													</div>
												</li>
											</ul>
										</div>
									</li>
									<li>
										<div class="questionnaire-title">
											<p>3. 您的職業為何?</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<ul>
												<li>
													<div class="ttb-input">
														<input type="text" class="text-input" name="SRCFUND_TEXT" id="SRCFUND_TEXT" value="" placeholder="（請選擇職業項目）" disabled>
														<input type="hidden" class="text-input" name="SRCFUND" id="SRCFUND" value="" >
														<button type="button" class="sm-ttb-input btn-flat-gray" data-toggle="modal" data-target="#ttbStatement">選擇</button>
													</div>
													<!-- 不在畫面上顯示的span -->
													<span class="hideblock">
														<!-- 驗證用的input -->
														<input id="Q3" name="Q3" type="text" class="text-input validate[required]"
															style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
													</span>
													
													<!-- pop up -->
													<div class="modal fade" id="ttbStatement" role="dialog" aria-labelledby="ttbStatementTitle" aria-hidden="true" tabindex="-1">
														<div class="modal-dialog modal-dialog-centered" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<p class="ttb-pup-header">職業名稱</p>
																</div>
																<div class="modal-body" style="width:100%">
																	<p>請選擇符合您的職業名稱</p>
																	<ul class="ttb-pup-item d-flex">
																		<li>
																			<label class="radio-block">國防事業
																				<input type="radio" name="SRCFUND_radio" id="061100" value="國防事業" >
																				<span class="ttb-radio"></span>
																			</label>
																		</li>
																		<li>
																			<label class="radio-block">警察單位
																				<input type="radio" name="SRCFUND_radio" id="061200" value="警察單位" >
																				<span class="ttb-radio"></span>
																			</label>
																		</li>
																		<li>
																			<label class="radio-block">其他公共行政類
																				<input type="radio" name="SRCFUND_radio" id="061300" value="其他公共行政類" >
																				<span class="ttb-radio"></span>
																			</label>
																		</li>
																		<li>
																			<label class="radio-block">教育業
																				<input type="radio" name="SRCFUND_radio" id="061400" value="教育業" >
																				<span class="ttb-radio"></span>
																			</label>
																		</li>
																		<li>
																			<label class="radio-block">學生
																				<input type="radio" name="SRCFUND_radio" id="061410" value="學生" >
																				<span class="ttb-radio"></span>
																			</label>
																		</li>
																		<li>
																			<label class="radio-block">工、商及服務業
																				<input type="radio" name="SRCFUND_radio" id="061500" value="工、商及服務業" >
																				<span class="ttb-radio"></span>
																			</label>
																		</li>
																		<li>
																			<label class="radio-block">農林漁牧業
																				<input type="radio" name="SRCFUND_radio" id="0615A0" value="農林漁牧業" >
																				<span class="ttb-radio"></span>
																			</label>
																		</li>
																		<li>
																			<label class="radio-block">礦石及土石採取業
																				<input type="radio" name="SRCFUND_radio" id="0615B0" value="礦石及土石採取業" >
																				<span class="ttb-radio"></span>
																			</label>
																		</li>
																		<li>
																			<label class="radio-block">製造業
																				<input type="radio" name="SRCFUND_radio" id="0615C0" value="製造業" >
																				<span class="ttb-radio"></span>
																			</label>
																		</li>
																		<li>
																			<label class="radio-block">水電燃氣業
																				<input type="radio" name="SRCFUND_radio" id="0615D0" value="水電燃氣業" >
																				<span class="ttb-radio"></span>
																			</label>
																		</li>
																		<li>
																			<label class="radio-block">營造業
																				<input type="radio" name="SRCFUND_radio" id="0615E0" value="營造業" >
																				<span class="ttb-radio"></span>
																			</label>
																		</li>
																		<li>
																			<label class="radio-block">批發及零售業
																				<input type="radio" name="SRCFUND_radio" id="0615F0" value="批發及零售業" >
																				<span class="ttb-radio"></span>
																			</label>
																		</li>
																		<li>
																			<label class="radio-block">住宿及餐飲業
																				<input type="radio" name="SRCFUND_radio" id="0615G0" value="住宿及餐飲業" >
																				<span class="ttb-radio"></span>
																			</label>
																		</li>
																		<li>
																			<label class="radio-block">運輸、倉儲及通信業
																				<input type="radio" name="SRCFUND_radio" id="0615H0" value="運輸、倉儲及通信業" >
																				<span class="ttb-radio"></span>
																			</label>
																		</li>
																		<li>
																			<label class="radio-block">金融及保險業
																				<input type="radio" name="SRCFUND_radio" id="0615I0" value="金融及保險業" >
																				<span class="ttb-radio"></span>
																			</label>
																		</li>
																		<li>
																			<label class="radio-block">不動產及租賃業
																				<input type="radio" name="SRCFUND_radio" id="0615J0" value="不動產及租賃業" >
																				<span class="ttb-radio"></span>
																			</label>
																		</li>
																		<li>
																			<label class="radio-block">其他專業服務業
																				<input type="radio" name="SRCFUND_radio" id="061610" value="其他專業服務業" >
																				<span class="ttb-radio"></span>
																			</label>
																		</li>
																		<li>
																			<label class="radio-block">技術服務業
																				<input type="radio" name="SRCFUND_radio" id="061620" value="技術服務業" >
																				<span class="ttb-radio"></span>
																			</label>
																		</li>
																		<li>
																			<label class="radio-block">特定專業服務業(律師、會計師、地政士、公證人、記帳士、記帳及報稅代理人)
																				<input type="radio" name="SRCFUND_radio" id="061630" value="特定專業服務業(律師、會計師、地政士、公證人、記帳士、記帳及報稅代理人)" >
																				<span class="ttb-radio"></span>
																			</label>
																		</li>
																		<li>
																			<label class="radio-block">特定專業服務業(受聘於專業服務業之行政事務職員)
																				<input type="radio" name="SRCFUND_radio" id="061640" value="特定專業服務業(受聘於專業服務業之行政事務職員)" >
																				<span class="ttb-radio"></span>
																			</label>
																		</li>
																		<li>
																			<label class="radio-block">銀樓業(含珠寶、鐘錶及貴金屬之製造、批發及零售)
																				<input type="radio" name="SRCFUND_radio" id="061650" value="銀樓業(含珠寶、鐘錶及貴金屬之製造、批發及零售)" >
																				<span class="ttb-radio"></span>
																			</label>
																		</li>
																		<li>
																			<label class="radio-block">虛擬貨幣交易服務業
																				<input type="radio" name="SRCFUND_radio" id="061660" value="虛擬貨幣交易服務業" >
																				<span class="ttb-radio"></span>
																			</label>
																		</li>
																		<li>
																			<label class="radio-block">博弈業
																				<input type="radio" name="SRCFUND_radio" id="061670" value="博弈業" >
																				<span class="ttb-radio"></span>
																			</label>
																		</li>
																		<li>
																			<label class="radio-block">國防武器或戰爭設備相關行業(軍火)
																				<input type="radio" name="SRCFUND_radio" id="061680" value="國防武器或戰爭設備相關行業(軍火)" >
																				<span class="ttb-radio"></span>
																			</label>
																		</li>
																		<li>
																			<label class="radio-block">家管
																				<input type="radio" name="SRCFUND_radio" id="061690" value="家管" >
																				<span class="ttb-radio"></span>
																			</label>
																		</li>
																		<li>
																			<label class="radio-block">自由業
																				<input type="radio" name="SRCFUND_radio" id="061691" value="自由業" >
																				<span class="ttb-radio"></span>
																			</label>
																		</li>
																		<li>
																			<label class="radio-block">無業
																				<input type="radio" name="SRCFUND_radio" id="061692" value="無業" >
																				<span class="ttb-radio"></span>
																			</label>
																		</li>
																		<li>
																			<label class="radio-block">其他(無業家管退休)
																				<input type="radio" name="SRCFUND_radio" id="061700" value="其他(無業家管退休)" >
																				<span class="ttb-radio"></span>
																			</label>
																		</li>
																		<li>
																			<label class="radio-block">非法人組織授信戶負責人
																				<input type="radio" name="SRCFUND_radio" id="069999" value="非法人組織授信戶負責人" >
																				<span class="ttb-radio"></span>
																			</label>
																		</li>
																	</ul>
																</div>
																<div class="modal-footer ttb-pup-footer">
																	<input type="button" class="ttb-pup-btn btn-flat-gray" data-dismiss="modal" value="取消" />
																	<input type="button" class="ttb-pup-btn btn-flat-orange" data-dismiss="modal" value="確定" onclick="fillData();"/>
																</div>
															</div>
														</div>
													</div>
													
												</li>
											</ul>
											<div class="ttb-input">
												<label class="radio-block"> <input type="radio"
													value="A" id="Q3_show" disabled />A﹒ 金融相關行業（銀行、保險、證券等） <span
													class="ttb-radio"></span>
												</label> <label class="radio-block"> <input type="radio"
													value="B" id="Q3_show" disabled />B﹒ 非金融相關行業 <span
													class="ttb-radio"></span>
												</label> <label class="radio-block"> <input type="radio"
													value="C" id="Q3_show" disabled />C﹒ 學生／家管／退休／其他 <span
													class="ttb-radio"></span>
												</label>
											</div>
											<br>
										</div>
									</li>
									<li>
										<div class="questionnaire-title">
											<p>4. 您個人/家庭年收入為 (新臺幣)?</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<ul>
												<li>
													<div class="ttb-input">
														<input type="text" id="newsalary" name="newsalary" class="text-input" size="7" maxlength="5"
															placeholder="請輸入個人/家庭年收入 (新臺幣)" onblur="fillSalary()" value="" />
														<span class="input-unit">萬元</span>
														<!-- 不在畫面上顯示的span -->
														<span class="hideblock">
															<!-- 驗證用的input -->
															<input id="Q4" name="Q4" type="text" class="text-input validate[required]"
																style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
														</span>
													</div>
												</li>
											</ul>
											<div class="ttb-input">
												<label class="radio-block">
													<input type="radio" value="A" id="Q4_show" disabled/>A﹒ 未達50萬
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="B" id="Q4_show" disabled/>B﹒ 50萬以上 - 未達70萬
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="C" id="Q4_show" disabled/>C﹒ 70萬以上 - 未達100萬
													<span class="ttb-radio"></span>
												</label>
												<label class="radio-block">
													<input type="radio" value="D" id="Q4_show" disabled/>D﹒ 100萬以上
													<span class="ttb-radio"></span>
												</label>
											</div>
											<br>
										</div>
									</li>
									<li>
										<div class="questionnaire-title">
											<p>5. 您個人所得與資金來源?</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<ul>
												<li>
													<div class="ttb-input">
														<label class="radio-block">A﹒ 薪資、租金、資本利得等
															<input type="radio" value="A" name="radioQ5"/>
															<span class="ttb-radio"></span>
														</label>
													</div>
												</li>
												<li>
													<div class="ttb-input">
														<label class="radio-block">B﹒ 儲蓄所得
															<input type="radio" value="B" name="radioQ5"/>
															<span class="ttb-radio"></span>
														</label>
													</div>
												</li>
												<li>
													<div class="ttb-input">
														<label class="radio-block">C﹒ 退休金
															<input type="radio" value="C" name="radioQ5"/>
															<span class="ttb-radio"></span>
															
														</label>
														
														<!-- 不在畫面上顯示的span -->
														<span class="hideblock">
															<!-- 驗證用的input -->
															<input id="Q5" name="Q5" type="text" class="text-input validate[required]"
																style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
														</span>
													</div>
												</li>
											</ul>
										</div>
									</li>
									<li>
										<div class="questionnaire-title">
											<p>6. 您目前的有價證券與存款合計為 (新臺幣)?</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<ul>
												<li>
													<div class="ttb-input">
														<label class="radio-block">A﹒ 未達50萬
															<input type="radio" value="A" name="radioQ6"/>
															<span class="ttb-radio"></span>
														</label>
													</div>
												</li>
												<li>
													<div class="ttb-input">
														<label class="radio-block">B﹒ 50萬以上 ~ 未達100萬
															<input type="radio" value="B" name="radioQ6"/>
															<span class="ttb-radio"></span>
														</label>
													</div>
												</li>
												<li>
													<div class="ttb-input">
														<label class="radio-block">C﹒ 100萬以上 ~ 未達200萬
															<input type="radio" value="C" name="radioQ6"/>
															<span class="ttb-radio"></span>
														</label>
													</div>
												</li>
	
												<li>
													<div class="ttb-input">
														<label class="radio-block">D﹒ 200萬以上
															<input type="radio" value="D" name="radioQ6"/>
															<span class="ttb-radio"></span>
														</label>
														
														<!-- 不在畫面上顯示的span -->
														<span class="hideblock">
															<!-- 驗證用的input -->
															<input id="Q6" name="Q6" type="text" class="text-input validate[required]"
																style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
														</span>
													</div>
												</li>
											</ul>
										</div>
									</li>
									<li>
										<div class="questionnaire-title">
											<p>7. 您投資的主要目的與需求為何?</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<ul>
												<li>
													<div class="ttb-input">
														<label class="radio-block">A﹒ 投資理財
															<input type="radio" value="A" name="radioQ7"/>
															<span class="ttb-radio"></span>
														</label>
													</div>
												</li>
												<li>
													<div class="ttb-input">
														<label class="radio-block">B﹒ 退休計畫
															<input type="radio" value="B" name="radioQ7"/>
															<span class="ttb-radio"></span>
														</label>
													</div>
												</li>
												<li>
													<div class="ttb-input">
														<label class="radio-block">C﹒ 教育基金
															<input type="radio" value="C" name="radioQ7"/>
															<span class="ttb-radio"></span>
														</label>
													</div>
												</li>
	
												<li>
													<div class="ttb-input">
														<label class="radio-block">D﹒ 清償債務
															<input type="radio" value="D" name="radioQ7"/>
															<span class="ttb-radio"></span>
														</label>
														
														<!-- 不在畫面上顯示的span -->
														<span class="hideblock">
															<!-- 驗證用的input -->
															<input id="Q7" name="Q7" type="text" class="text-input validate[required]"
																style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
														</span>
													</div>
												</li>
											</ul>
										</div>
									</li>
									<li>
										<div class="questionnaire-title">
											<p>8. 您計畫何時開始提領您投資的部分金額?(現金流量期望)</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<ul>
												<li>
													<div class="ttb-input">
														<label class="radio-block">A﹒ 未達1年
															<input type="radio" value="A" name="radioQ8"/>
															<span class="ttb-radio"></span>
														</label>
													</div>
												</li>
												<li>
													<div class="ttb-input">
														<label class="radio-block">B﹒ 1年以上 ~ 未達2年
															<input type="radio" value="B" name="radioQ8"/>
															<span class="ttb-radio"></span>
														</label>
													</div>
												</li>
												<li>
													<div class="ttb-input">
														<label class="radio-block">C﹒ 2年以上 ~ 未達5年
															<input type="radio" value="C" name="radioQ8"/>
															<span class="ttb-radio"></span>
														</label>
													</div>
												</li>
	
												<li>
													<div class="ttb-input">
														<label class="radio-block">D﹒ 5年以上
															<input type="radio" value="D" name="radioQ8"/>
															<span class="ttb-radio"></span>
														</label>
														
														<!-- 不在畫面上顯示的span -->
														<span class="hideblock">
															<!-- 驗證用的input -->
															<input id="Q8" name="Q8" type="text" class="text-input validate[required]"
																style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
														</span>
													</div>
												</li>
											</ul>
										</div>
									</li>
									<li>
										<div class="questionnaire-title">
											<p>9. 您對投資之期望報酬率為?</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<ul>
												<li>
													<div class="ttb-input">
														<label class="radio-block">A﹒ 0% ~ 5%
															<input type="radio" value="A" name="radioQ9"/>
															<span class="ttb-radio"></span>
														</label>
													</div>
												</li>
												<li>
													<div class="ttb-input">
														<label class="radio-block">B﹒ 6% ~ 10%
															<input type="radio" value="B" name="radioQ9"/>
															<span class="ttb-radio"></span>
														</label>
													</div>
												</li>
												<li>
													<div class="ttb-input">
														<label class="radio-block">C﹒ 11% ~ 20%
															<input type="radio" value="C" name="radioQ9"/>
															<span class="ttb-radio"></span>
														</label>
													</div>
												</li>
												<li>
													<div class="ttb-input">
														<label class="radio-block">D﹒ 大於20%
															<input type="radio" value="D" name="radioQ9"/>
															<span class="ttb-radio"></span>
														</label>
														
														<!-- 不在畫面上顯示的span -->
														<span class="hideblock">
															<!-- 驗證用的input -->
															<input id="Q9" name="Q9" type="text" class="text-input validate[required]"
																style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
														</span>
													</div>
												</li>
											</ul>
										</div>
									</li>
									<li>
										<div class="questionnaire-title">
											<p>10. 您投資金融商品預計投資期限多長?</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<ul>
												<li>
													<div class="ttb-input">
														<label class="radio-block">A﹒ 未達1年
															<input type="radio" value="A" name="radioQ10"/>
															<span class="ttb-radio"></span>
														</label>
													</div>
												</li>
												<li>
													<div class="ttb-input">
														<label class="radio-block">B﹒ 1年以上 ~ 未達2年
															<input type="radio" value="B" name="radioQ10"/>
															<span class="ttb-radio"></span>
														</label>
													</div>
												</li>
												<li>
													<div class="ttb-input">
														<label class="radio-block">C﹒ 2年以上
															<input type="radio" value="C" name="radioQ10"/>
															<span class="ttb-radio"></span>
														</label>
														
														<!-- 不在畫面上顯示的span -->
														<span class="hideblock">
															<!-- 驗證用的input -->
															<input id="Q10" name="Q10" type="text" class="text-input validate[required]"
																style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
														</span>
													</div>
												</li>
											</ul>
										</div>
									</li>
									<li>
										<div class="questionnaire-title">
											<p>11. 您過去的投資經驗?(本題可複選，取得分最高者計算)</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<ul>
												<li>
													<div class="ttb-input">
														<label class="check-block">A﹒ 曾投資期貨、連動債、衍生性金融商品
															<input type="checkbox" value="A" id="Q111" name="Q111"/>
															<span class="ttb-check"></span>
														</label>
													</div>
												</li>
												<li>
													<div class="ttb-input">
														<label class="check-block">B﹒ 曾投資國內外共同基金、債券、股票、投資型保單
															<input type="checkbox" value="B" id="Q112" name="Q112"/>
															<span class="ttb-check"></span>
														</label>
													</div>
												</li>
												<li>
													<div class="ttb-input">
														<label class="check-block">C﹒ 對於投資有心得，偏好自行決定投資策略
															<input type="checkbox" value="C" id="Q113" name="Q113"/>
															<span class="ttb-check"></span>
														</label>
													</div>
												</li>
												<li>
													<div class="ttb-input">
														<label class="check-block">D﹒ 無經驗
															<input type="checkbox" value="D" id="Q114" name="Q114" onclick="checkQ11()" />
															<span class="ttb-check"></span>
														</label>
													</div>
												</li>
											</ul>
										</div>
									</li>
									<li>
										<div class="questionnaire-title">
											<p>12. 您從事投資理財的時間?（投資時間）</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<ul>
												<li>
													<div class="ttb-input">
														<label class="radio-block">A﹒ 未達1年
															<input type="radio" value="A" name="radioQ12"/>
															<span class="ttb-radio"></span>
														</label>
													</div>
												</li>
												<li>
													<div class="ttb-input">
														<label class="radio-block">B﹒ 1年以上 ~ 未達2年
															<input type="radio" value="B" name="radioQ12"/>
															<span class="ttb-radio"></span>
														</label>
													</div>
												</li>
												<li>
													<div class="ttb-input">
														<label class="radio-block">C﹒ 2年以上 ~ 未達5年
															<input type="radio" value="C" name="radioQ12"/>
															<span class="ttb-radio"></span>
														</label>
													</div>
												</li>
												<li>
													<div class="ttb-input">
														<label class="radio-block">D﹒ 5年以上
															<input type="radio" value="D" name="radioQ12"/>
															<span class="ttb-radio"></span>
														</label>
														
														<!-- 不在畫面上顯示的span -->
														<span class="hideblock">
															<!-- 驗證用的input -->
															<input id="Q12" name="Q12" type="text" class="text-input validate[required]"
																style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
														</span>
													</div>
												</li>
											</ul>
										</div>
									</li>
									<li>
										<div class="questionnaire-title">
											<p>13. 您對金融商品的認識?(本題可複選，取得分最高者計算)</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<ul>
												<li>
													<div class="ttb-input">
														<label class="check-block">A﹒ 對金融商品（如連動債等衍生性商品）了解
															<input type="checkbox" value="A" id="Q131" name="Q131"/>
															<span class="ttb-check"></span>
														</label>
													</div>
												</li>
												<li>
													<div class="ttb-input">
														<label class="check-block">B﹒ 對金融商品（如股票）了解
															<input type="checkbox" value="B" id="Q132" name="Q132"/>
															<span class="ttb-check"></span>
														</label>
													</div>
												</li>
												<li>
													<div class="ttb-input">
														<label class="check-block">C﹒ 對金融商品（如國內外共同基金）了解
															<input type="checkbox" value="C" id="Q133" name="Q133"/>
															<span class="ttb-check"></span>
														</label>
													</div>
												</li>
												<li>
													<div class="ttb-input">
														<label class="check-block">D﹒ 對金融商品不了解
															<input type="checkbox" value="D" id="Q134" name="Q134" onclick="checkQ13()" />
															<span class="ttb-check"></span>
														</label>
													</div>
												</li>
											</ul>
										</div>
									</li>
									<li>
										<div class="questionnaire-title">
											<p>14. 您可承受的投資損失風險波動範圍?</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<ul>
												<li>
													<div class="ttb-input">
														<label class="radio-block">A﹒ 無法忍受投資虧損
															<input type="radio" value="A" name="radioQ14"/>
															<span class="ttb-radio"></span>
														</label>
													</div>
												</li>
												<li>
													<div class="ttb-input">
														<label class="radio-block">B﹒ 5％以下
															<input type="radio" value="B" name="radioQ14"/>
															<span class="ttb-radio"></span>
														</label>
													</div>
												</li>
												<li>
													<div class="ttb-input">
														<label class="radio-block">C﹒ 6％～10％
															<input type="radio" value="C" name="radioQ14"/>
															<span class="ttb-radio"></span>
														</label>
													</div>
												</li>
												<li>
													<div class="ttb-input">
														<label class="radio-block">D﹒ 11％～20％
															<input type="radio" value="D" name="radioQ14"/>
															<span class="ttb-radio"></span>
														</label>
													</div>
												</li>
												<li>
													<div class="ttb-input">
														<label class="radio-block">E﹒ 大於20％
															<input type="radio" value="E" name="radioQ14"/>
															<span class="ttb-radio"></span>
														</label>
														
														<!-- 不在畫面上顯示的span -->
														<span class="hideblock">
															<!-- 驗證用的input -->
															<input id="Q14" name="Q14" type="text" class="text-input validate[required]"
																style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
														</span>
													</div>
												</li>
											</ul>
										</div>
									</li>
									<li>
										<div class="questionnaire-title">
											<p>15. 您偏好以下列何種商品做為您投資理財配置?</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<ul>
												<li>
													<div class="ttb-input">
														<label class="radio-block">A﹒ 期貨或衍生性金融商品
															<input type="radio" value="A" name="radioQ15"/>
															<span class="ttb-radio"></span>
														</label>
													</div>
												</li>
												<li>
													<div class="ttb-input">
														<label class="radio-block">B﹒ 國內外基金、股票或投資型保單
															<input type="radio" value="B" name="radioQ15"/>
															<span class="ttb-radio"></span>
														</label>
													</div>
												</li>
												<li>
													<div class="ttb-input">
														<label class="radio-block">C﹒ 存款、定存
															<input type="radio" value="C" name="radioQ15"/>
															<span class="ttb-radio"></span>
														</label>
														
														<!-- 不在畫面上顯示的span -->
														<span class="hideblock">
															<!-- 驗證用的input -->
															<input id="Q15" name="Q15" type="text" class="text-input validate[required]"
																style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
														</span>
													</div>
												</li>
											</ul>
										</div>
									</li>
									<li>
										<div class="questionnaire-title">
											<p>16. 是否領有全民健康保險重大傷病證明?</p>
										</div>
										<div class="questionnaire-answer ttb-input-item">
											<ul>
												<li>
													<div class="ttb-input">
														<label class="radio-block">否
															<input type="radio" name="MARK1" value="N"/>
															<span class="ttb-radio"></span>
														</label>
														<label class="radio-block">是
															<input type="radio" name="MARK1" value="Y"/>
															<span class="ttb-radio"></span>
														</label>
														<!-- 不在畫面上顯示的span -->
														<span class="hideblock">
															<!-- 驗證用的input -->
															<input id="Q16" type="text" class="text-input validate[required]"
																style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
														</span>
													</div>
												</li>
											</ul>
										</div>
									</li>
								</ul>
							</div>
							
							<input type="button" id="previous" class="ttb-button btn-flat-gray" value="上一步" name="previous">
							<input type="button" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="確定送出" name="CMSUBMIT">
							
						</div>
					</div>
				</form>
			</section>
		</main>
	</div>
	
	<%@ include file="../index/footer.jsp"%>
</body>
</html>