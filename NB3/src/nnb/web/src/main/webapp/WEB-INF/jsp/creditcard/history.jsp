<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<script type="text/javascript">
$(document).ready(function () {
$("#CMSUBMIT").click(function(e){
	var main = document.getElementById("formId");
    var obj1 = document.getElementById("RAD1");     
         
    if (obj1!=null){
    	 if (obj1.checked && !obj1.disabled) {
       main.CARDTYPE.value = "999"; 
       	main.BILL_NO.value = "1"; 
       }
    }
    
    var obj2 = document.getElementById("RAD2");          

    if (obj2!=null){      
       if (obj2.checked && !obj2.disabled) {
       	main.CARDTYPE.value = "700";           
       	main.BILL_NO.value = "5";

       }
    }
    var obj3 = document.getElementById("RAD3");          

    if (obj3!=null){      
       if (obj3.checked && !obj3.disabled) {
       	main.CARDTYPE.value = "ALL";           
       	main.BILL_NO.value = "3";
       	           
       }
    }
    
    $("form").submit();
});
});

</script>

</head>
<body>
   	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 信用卡     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Credit_Card" /></li>
    <!-- 帳務查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Inquiry_Service" /></li>
    <!-- 歷史帳單明細查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Historical_Statement_Detail_Inquiry" /></li>
		</ol>
	</nav>

	
	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
	
	
	<main class="col-12">
		<section id="main-content" class="container"><!-- 主頁內容  -->
			<h2>
				<spring:message code="LB.Historical_Statement_Detail_Inquiry" />
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="${__ctx}/CREDIT/INQUIRY/card_history_online">
				<input type="hidden" name="CARDTYPE"  value="${card_history.data.CARDTYPE}">
				<input type="hidden" name="bcardflag" value="${card_history.data.bcardflag}">
				<input type="hidden" name="BILL_NO" value="">
				<input type="hidden" name="FUNCTION" value="N820"/>
				<input type="hidden" name="QUERYTYPE" value="BHWSHtml">
				<div class="main-content-block row">
					<div class="col-12">
						<div class="ttb-input-block">
							<div class="ttb-message">
							<p><spring:message code="LB.Historical_Statement_Detail_Inquiry" /></p><!-- 歷史帳單明細查詢 -->
						</div>
						<div class="ttb-input-item row">
							<span class="input-title"> <label><h4><spring:message code="LB.Inquiry_time" /></h4></label><!-- 查詢時間 -->
							</span>
							<span class="input-block">
								<div class="ttb-input">
									<p>${card_history.data.CMQTIME}</p>
								</div>
							</span>
						</div>
						<div class="ttb-input-item row">
							<span class="input-title"> <label><h4><spring:message code="LB.Card_type" /></h4></label></span> <!-- 卡別 -->
							<span class="input-block">
								<div class="ttb-input">
									<!-- 顯示一般卡  -->
										<c:if test="${card_history.data.n810MsgCode != 'E091' && card_history.data.cardtype eq 'NORMAL' && card_history.data.bcardflag !='TRUE'}">
											<label class="radio-block">
												<spring:message code="LB.Bank_card" />
												<input type="radio" name="CARD_TYPE" value="0" id="RAD1" checked />
											<span class="ttb-radio"></span>
											</label> &nbsp;&nbsp;&nbsp;
										</c:if> 
									<!-- 顯示一般卡end  -->
									
									<!-- 顯示一般卡錯誤訊息  -->
										<c:if test="${card_history.data.n810MsgCode != '0' && card_history.data.n810MsgCode !='E091' && card_history.data.cardtype eq 'NORMAL' && card_history.data.bcardflag != 'TRUE'}">
												<font color=red>
													（<spring:message code="LB.ErrMsg" /><!-- 錯誤訊息 -->： ${card_history.data.n810MsgCode} 
													&nbsp;&nbsp; ${card_history.data.n810MsgName} )<br>
												</font><br>
												<script>
													var obj = document.getElementById("RAD1");
													obj.disabled = true;
												</script>
										</c:if>
									<!-- 顯示一般卡錯誤訊息end  -->
									
									<!-- 顯示商務卡  -->
									<c:if test="${card_history.data.n810MsgCode != 'E091' && card_history.data.cardtype eq 'BUSINESS' && card_history.data.bcardflag eq'TRUE'}">
										<label class="radio-block">
											<spring:message code="LB.Business_card" />
											<input type="radio" name="CARD_TYPE" value="2" id="RAD3" checked />
											<span class="ttb-radio"></span>
										</label> &nbsp;&nbsp;&nbsp;
									</c:if>
									<!-- 顯示商務卡end  -->
									
									<!-- 顯示商務卡錯誤訊息  -->
										<c:if test="${card_history.data.n810MsgCode != '0' && card_history.data.n810MsgCode !='E091' && card_history.data.cardtype eq 'BUSINESS' && card_history.data.bcardflag eq 'TRUE'}">
												<font color=red>
													（<spring:message code="LB.ErrMsg" /><!-- 錯誤訊息 -->： ${card_history.data.n810MsgCode} 
													&nbsp;&nbsp; ${card_history.data.n810MsgName} )<br>
												</font><br>
												<script>
													var obj = document.getElementById("RAD3");
													obj.disabled = true;
												</script>
										</c:if>
									<!-- 顯示商務卡錯誤訊息end  -->
									
									<!-- 顯示一般卡/商務卡  -->
									<c:if test="${card_history.data.n810MsgCode != 'E091' && card_history.data.cardtype eq 'BOTH' && card_history.data.bcardflag eq 'TRUE'}">
										<label class="radio-block">
												<spring:message code="LB.Bank_card" />
												<input type="radio" name="CARD_TYPE" value="0" id="RAD1" checked />
											<span class="ttb-radio"></span>
										</label> &nbsp;&nbsp;&nbsp;
										<label class="radio-block">
											<spring:message code="LB.Business_card" />
											<input type="radio" name="CARD_TYPE" value="2" id="RAD3" />
											<span class="ttb-radio"></span>
										</label> &nbsp;&nbsp;&nbsp;
									</c:if>
									<!-- 顯示一般卡/商務卡end  -->
									
									<!-- 顯示一般卡/商務卡錯誤訊息  -->
										<c:if test="${card_history.data.n810MsgCode != '0' && card_history.data.n810MsgCode !='E091' && card_history.data.cardtype eq 'BOTH' && card_history.data.bcardflag eq 'TRUE'}">
												<font color=red>
													（<spring:message code="LB.ErrMsg" /><!-- 錯誤訊息 -->： ${card_history.data.n810MsgCode} 
													&nbsp;&nbsp; ${card_history.data.n810MsgName} )<br>
												</font><br>
												<script>
													var obj = document.getElementById("RAD3");
													obj.disabled = true;
												</script>
										</c:if>
									<!-- 顯示一般卡/商務卡錯誤訊息end  -->
									
									<!-- 顯示VISA金融卡\悠遊Debit卡-->
									<c:if test="${card_history.data.n813MsgCode != 'E091'}">
										<label class="radio-block">
										<spring:message code="LB.VISA_debit_card" />\<spring:message code="LB.Debit_EasyCard" />
											<c:if test="${card_history.data.n810MsgCode != '0' || card_history.data.n810MsgCode eq 'E091'}">
												<input type="radio" name="CARD_TYPE" value="1" id="RAD2"  checked />
												<span class="ttb-radio"></span>
											</c:if>
											<c:if test="${card_history.data.n810MsgCode == '0'}">
												<input type="radio" name="CARD_TYPE" value="1" id="RAD2"  />
												<span class="ttb-radio"></span>
											</c:if>
										</label> &nbsp;&nbsp;&nbsp;
									</c:if>
									<!-- 顯示VISA金融卡\悠遊Debit卡end-->
									
									<!-- 顯示VISA金融卡\悠遊Debit卡錯誤訊息  -->
										<c:if test="${card_history.data.n813MsgCode != '0' && card_history.data.n813MsgCode !='E091'}">
											
												<font color=red>
												（<spring:message code="LB.ErrMsg" /><!-- 錯誤訊息 --> ： ${card_history.data.n813MsgCode} 
													&nbsp;&nbsp; ${card_history.data.n813MsgName} )<br>	
												</font>
												<script>
										          	 var obj = document.getElementById("RAD2");
										          	 obj.disabled = true;
												</script>
											
										</c:if>
									<!-- 顯示VISA金融卡\悠遊Debit卡錯誤訊息end  -->
								</div>
								</span>
							</div>
							<div class="ttb-input-item row">
							<span class="input-title"> <label><h4><spring:message code="LB.Inquiry_period_1" /></h4></label></span> <!-- 卡別 -->
								<span class="input-block">
								<div class="ttb-input">
									<label class="radio-block">
										<spring:message code="LB.This_period" /><!-- 本期 --> 
										<input TYPE="radio" NAME="FGPERIOD" VALUE="CMCURMON" checked>
										<span class="ttb-radio"></span>
									</label>&nbsp;&nbsp;&nbsp;
									<label class="radio-block">
										<spring:message code="LB.Previous_period" />
										<input TYPE="radio" NAME="FGPERIOD" VALUE="CMLASTMON" >
										<span class="ttb-radio"></span>
									</label>&nbsp;&nbsp;&nbsp;
									<label class="radio-block">
										<spring:message code="LB.Last_period" />
										<input TYPE="radio" NAME="FGPERIOD" VALUE="CMLAST2MON" >
										<span class="ttb-radio"></span>
									</label>&nbsp;&nbsp;&nbsp;
								</div>
								</span>
							</div>
					</div>
					<input id="CMRESET" name="CMRESET" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" /> 
					<input id="CMSUBMIT" name="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />	
					</div>
				</div>
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p><!-- 說明 -->
						<li><spring:message code="LB.History_P1_D1" /><!-- 正卡持卡人歸戶名下所有信用卡〈含附卡〉之交易明細。 --></li>
						<li><spring:message code="LB.History_P1_D2" /><!-- 信用卡客服：（02）2357-7171、0800-017171。 --></li>
					</ol>
				</div>
				</div>
			</form>
		</section>
	</main><!-- main-content END --> 
	</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>
