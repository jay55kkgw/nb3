<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust:exact">
	<br/><br/>
	<div style="text-align:center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/>
	</div>
	<br/><br/><br/>
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<table class="print">
		<tr>
<!-- 交易日期 -->
			<td><spring:message code= "LB.D0450" /></td>
		 	<td>${CMQTIME}</td>
		</tr>
		<tr>
<!-- 戶名 -->
  			<td><spring:message code= "LB.D1215" /></td>
			<td>${DPUSERNAME}</td>
		</tr>
		<tr>
<!-- 黃金轉出帳號 -->
  			<td><spring:message code= "LB.W1515" /></td>
			<td>${ACN}</td>
		</tr>
		<tr>
<!-- 臺幣轉入帳號 -->
  			<td><spring:message code= "LB.W1518" /></td>
			<td>${SVACN}</td>
		</tr>
		<tr>
<!-- 賣出公克數 -->
  			<td><spring:message code= "LB.W1519" /></td>
<!-- 公克 -->
			<td>${TRNGDFormat}<spring:message code= "LB.W1435" /></td>
		</tr>
		<tr>
<!-- 牌告單價 -->
			<td><spring:message code= "LB.W1524" /></td>
<!-- 新臺幣 -->
<!-- 元／公克 -->
		 	<td><spring:message code= "LB.D0508" />${PRICEFormat}<spring:message code= "LB.W1511" /></td>
		</tr>
		<tr>
<!-- 回售金額 -->
			<td><spring:message code= "LB.W1527" /></td>
<!-- 新臺幣 -->
<!-- 元 -->
		 	<td><spring:message code= "LB.D0508" />${TRNAMTFormat}<spring:message code= "LB.Dollar" /></td>
		</tr>
		<c:if test="${FEEAMT1Format != '0'}">
			<tr>
<!-- 定期投資扣款失敗 -->
<!-- 累計手續費 -->
	  			<td><spring:message code= "LB.W1529" /><br/><spring:message code= "LB.W1530" /></td>
<!-- 新臺幣 -->
<!-- 元 -->
				<td><spring:message code= "LB.D0508" />${FEEAMT1Format}<spring:message code= "LB.Dollar" /></td>
			</tr>
		</c:if>
		<c:if test="${FEEAMT2Format != '0'}">
			<tr>
<!-- 黃金撲滿扣款失敗 -->
<!-- 累計手續費 -->
	  			<td><spring:message code= "LB.W1531" /><br/><spring:message code= "LB.W1530" /></td>
<!-- 新臺幣 -->
<!-- 元 -->
				<td><spring:message code= "LB.D0508" />${FEEAMT2Format}<spring:message code= "LB.Dollar" /></td>
			</tr>
		</c:if>
		<tr>
<!-- 存入臺幣金額 -->
  			<td><spring:message code= "LB.W1532" /></td>
<!-- 新臺幣 -->
<!-- 元 -->
			<td><spring:message code= "LB.D0508" />${TRNAMT2Format}<spring:message code= "LB.Dollar" /></td>
		</tr>
	</table>
	<div>
		<p style="text-align:left;">
		<!-- 請注意：本資料僅供參考，實際交易結果以本行資料為主。 -->
			<spring:message code= "LB.X1195" />
		</p>
	</div>
</body>
</html>