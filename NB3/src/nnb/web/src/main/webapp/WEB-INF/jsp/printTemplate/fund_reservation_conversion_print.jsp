<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
<br/><br/>
<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
<br/><br/><br/>
<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
<br/><br/><br/>
<label><spring:message code="LB.System_time" /> ：</label><label>${CMQTIME}</label>
<br/><br/>
<label><spring:message code= "LB.W1060" /> ：</label><label><spring:message code= "LB.W1062" /></label>
<br/><br/>
<label><spring:message code="LB.X0380" /> ：</label><label>${REC_NO}</label><label><spring:message code="LB.Rows"/></label>
<br/><br/>

<table class="print">
	<tr>
		<th><spring:message code="LB.Name"/></th>
		<th class="text-left" colspan="11">${NAME}</th>
	</tr>
	<tr>
		<td style="text-align:center"><spring:message code="LB.X0377" /></td>
		<td style="text-align:center"><spring:message code="LB.W0963" /></td>
		<td style="text-align:center"><spring:message code="LB.W1116" /></td>
		<td style="text-align:center">
			<spring:message code="LB.W1122" />
		</td>
		<td style="text-align:center"><spring:message code="LB.W0967"/></td>
		<td style="text-align:center">
			<spring:message code="LB.W1123" />
		</td>
		<td style="text-align:center">
			<spring:message code="LB.W1125" />
		</td>
		<td style="text-align:center">
			<spring:message code="LB.W0974" />
		</td>
		<td style="text-align:center"><spring:message code="LB.W1079" /></td>
		<td style="text-align:center"><spring:message code="LB.D0168" /></td>
		<td style="text-align:center"><spring:message code= "LB.X1249" /></td>
		<td style="text-align:center"><spring:message code="LB.W0944" /></td>
	</tr>
	<c:forEach items="${dataListMap}" var="map">
	<tr>
		<td style="text-align:center">${map.TRADEDATE_1}</td>
		<td style="text-align:center">(${map.TRANSCODE})&nbsp;${map.FUNDLNAME}</td>
		<td style="text-align:center">
			${map.ADCCYNAME}<br>
			${map.FUNDAMT_1}
		</td>
		<td style="text-align:right">${map.UNIT_1}</td>
		<td style="text-align:center">(${map.INTRANSCODE})&nbsp;${map.I_FUNDLNAME}</td>
		<td style="text-align:right">
			<spring:message code="LB.NTD" /><br>
			${map.AMT3_1}
		</td>
		<td style="text-align:right">
			<spring:message code="LB.NTD" /><br>
			${map.FCA2_1}
		</td>
		<td style="text-align:right">
			<spring:message code="LB.NTD" /><br>
			${map.FCA1_1}
		</td>
		<td style="text-align:right">
			<spring:message code="LB.NTD" /><br>
			${map.AMT5_1}
		</td>
		<td style="text-align:center">${map.OUTACN_1}</td>
		<td style="text-align:center">
		
			<c:if test="${map.BILLSENDMODE == '1'}">
				<spring:message code="LB.All"/>
			</c:if>
			<c:if test="${map.BILLSENDMODE == '2'}">
				<spring:message code="LB.X1852"/>
			</c:if>
		</td>
		<td style="text-align:center">${map.CDNO_1}</td>
	</tr>
	</c:forEach>
</table>
<br/><br/>
	<div class="text-left">
		<spring:message code="LB.Description_of_page" />
		<ol class="list-decimal text-left">
			<li><spring:message code= "LB.fund_reservation_query_P2_D1" /></li>
          	<li><spring:message code= "LB.fund_reservation_query_P2_D2" /></li>
		</ol>
	</div>
</body>
</html>