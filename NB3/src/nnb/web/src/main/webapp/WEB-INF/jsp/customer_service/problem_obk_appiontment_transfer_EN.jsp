<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div id="page3" class="card-detail-block">
	<div class="card-center-block">
		<!-- Q1 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-31" aria-expanded="true"
				aria-controls="popup1-31">
				<div class="row">
					<span class="col-1">Q1</span>
					<div class="col-11">
						<span>Online bank predesignated transfer, do I need to
							apply for another cabinet?</span>
					</div>
				</div>
			</a>
			<div id="popup1-31" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>If you have designated transfer account, then will have
								the function of instant transfer and reservation transfer. You
								don't need to apply for an appointment transfe at cabinet.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q2 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-32" aria-expanded="true"
				aria-controls="popup1-32">
				<div class="row">
					<span class="col-1">Q2</span>
					<div class="col-11">
						<span>What are the restrictions on online banking
							predesignated transfer transaction limits?</span>
					</div>
				</div>
			</a>
			<div id="popup1-32" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>The predesignated transfer transaction limit of online
								bank, is to calculate the cumulative transfer amount on the day
								combines the transfer date with the instant transfer.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q3 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-33" aria-expanded="true"
				aria-controls="popup1-33">
				<div class="row">
					<span class="col-1">Q3</span>
					<div class="col-11">
						<span>What is the appointment period for online bank
							predesignated transfer?</span>
					</div>
				</div>
			</a>
			<div id="popup1-33" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>The appointment transfer period is up to one year, and the
								appointment must be completed one day before the transfer date;
								If the transfer date of the appointment is the date that the
								calendar does not have, the end date of the month is the
								transfer date. NTD predesignated transfer, if the case is on
								holiday, the transaction will be executed as usual; For the
								predesignated transfer of foreign currency, if the case is on
								holiday, the transaction will be postponed to the next business
								day.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q4 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-34" aria-expanded="true"
				aria-controls="popup1-34">
				<div class="row">
					<span class="col-1">Q4</span>
					<div class="col-11">
						<span>What is foreign currency predesignated transfer rate?</span>
					</div>
				</div>
			</a>
			<div id="popup1-34" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>The transfer rate take the listing exchange rate at 9:10
								am on the actual transfer date, and compares the second:
								[Service Items] A10 provides exchange rate concessions.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q5 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-35" aria-expanded="true"
				aria-controls="popup1-35">
				<div class="row">
					<span class="col-1">Q5</span>
					<div class="col-11">
						<span>How do I know the transfer result of my appointment
							transfer?</span>
					</div>
				</div>
			</a>
			<div id="popup1-35" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>Please use the "Reservation Transaction Result Enquiry" of
								the online banking "Accounting Enquiry" - "Reservation
								Transaction Enquiry / Cancellation" to check the results of the
								appointment transaction transfer in the last six months. For the
								result of the transfer on the day of the transfer, please do so
								in the morning, execution after 10:30 is the final result of the
								transfer of the day (unsuccessful transactions will be carried
								out again at 10:00 am on the same day). For those who have
								already logged in to "My Email", the transfer result will be
								sent to your e-mail address. Please log in to the e-mail
								address. If you change the e-mail address, please also update
								the e-mail.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q6 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-36" aria-expanded="true"
				aria-controls="popup1-36">
				<div class="row">
					<span class="col-1">Q6</span>
					<div class="col-11">
						<span>Online banking check-in password, transaction
							password change or password reset, is the registered account
							transfer still valid?</span>
					</div>
				</div>
			</a>
			<div id="popup1-36" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>The password change and reset, the registered appointment
								transfer is still valid, and the transfer operation will be
								performed according to the appointment instruction on the
								transfer date.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q7 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-37" aria-expanded="true"
				aria-controls="popup1-37">
				<div class="row">
					<span class="col-1">Q7</span>
					<div class="col-11">
						<span>Is the online bank user name, check-in password, or
							transaction password entered error exceeds the number of times
							and the system is deactivated, and is the registered account
							transfer still valid?</span>
					</div>
				</div>
			</a>
			<div id="popup1-37" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>When the above data entry error exceeds the number of
								times and the system is deactivated, the previously registered
								appointment transfer is still valid, and the transfer operation
								will be performed according to the reservation instruction on
								the transfer date.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q8 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-38" aria-expanded="true"
				aria-controls="popup1-38">
				<div class="row">
					<span class="col-1">Q8</span>
					<div class="col-11">
						<span>If the electronic voucher update is unsuccessful,
							cancelled or expired, is the registered electronic signature
							reservation transfer transaction still valid?</span>
					</div>
				</div>
			</a>
			<div id="popup1-38" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>The electronic voucher update is unsuccessful, cancelled
								or expired. The registered electronic signature reservation
								transfer transaction is still valid, and the transfer operation
								will be performed according to the appointment instruction on
								the transfer date.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q9 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-39" aria-expanded="true"
				aria-controls="popup1-39">
				<div class="row">
					<span class="col-1">Q9</span>
					<div class="col-11">
						<span>I have successfully logged in to the online bank to
							make an appointment transfer transaction. How can I cancel the
							appointment transfer?</span>
					</div>
				</div>
			</a>
			<div id="popup1-39" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>If the customer intends to cancel the appointment
								transfer, the cancellation of the reservation transfer procedure
								should be completed by using online banking on the day before
								the transfer date of the appointment. Appointment transfer by
								transaction password (SSL) must be cancelled by transaction
								password; reservation transfer by electronic signature
								transaction (i-Key) must be cancelled by electronic signature.
								If the customer's user name and password are entered error
								exceeds the number of times and the system is deactivated or the
								voucher update is unsuccessful, cancelled, and the expiration of
								the voucher cannot be used, the previously predesignated
								transfer is still valid. If you want to cancelled the transfer,
								and because the above reason cannot log in online banking to
								cancelled, you should go to the bank where you open online bank
								account to apply for cancellation of all the appointment
								transfers on the non-transfer date.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q10 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-310" aria-expanded="true"
				aria-controls="popup1-310">
				<div class="row">
					<span class="col-1">Q10</span>
					<div class="col-11">
						<span>The application for cancellation has been designated
							to transfer the account or transfer to the account. Is the
							registered transfer account still valid?</span>
					</div>
				</div>
			</a>
			<div id="popup1-310" class="ttb-pup-collapse collapse"
				role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>The customer counters apply to cancel the online bank
								transfer account, and the predesignated transfer of the
								non-transfer date under the cancellation account stops the
								transfer operation; Cancelled the transfer to the account, and
								the predesignated transfer of the non-transfer date will stop
								the transfer.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>