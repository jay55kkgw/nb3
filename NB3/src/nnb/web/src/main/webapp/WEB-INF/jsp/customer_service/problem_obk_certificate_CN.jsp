<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="card-detail-block">
	<div class="card-center-block">

		<!-- Q1 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-1" aria-expanded="true"
				aria-controls="popup4-1">
				<div class="row">
					<span class="col-1">Q1</span>
					<div class="col-11">
						<span>凭证注册中心服务项目包括哪些？</span>
					</div>
				</div>
			</a>
			<div id="popup4-1" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>提供金融XML凭证申请、更新、暂禁、解禁及废止等服务。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q2 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-2" aria-expanded="true"
				aria-controls="popup4-2">
				<div class="row">
					<span class="col-1">Q2</span>
					<div class="col-11">
						<span>XML凭证的使用范围？</span>
					</div>
				</div>
			</a>
			<div id="popup4-2" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>
								可使用于网络银行、电子票据等系统各项功能之交易放行，与交易密码之主要差别为可应用于非约定转账及非本人费用之缴费等高风险交易。
							</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q3 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-3" aria-expanded="true"
				aria-controls="popup4-3">
				<div class="row">
					<span class="col-1">Q3</span>
					<div class="col-11">
						<span>如何申请使用XML凭证？</span>
					</div>
				</div>
			</a>
			<div id="popup4-3" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>
								请亲携身分证明文件、存款立约印鉴及存折，至本行填写「台湾企银金融XML凭证申请/暂禁/解禁/废止申请书」办理凭证相关约定手续。客户完成凭证约定手续，取得本行发给之凭证识别资料（Common
								Name，CN）后，可登入凭证注册中心申请签发凭证。 身分证明文件，法人为公司登记证明文件、负责人身分证件；自然人为身分证件。

							</p>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q4 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-4" aria-expanded="true"
				aria-controls="popup4-4">
				<div class="row">
					<span class="col-1">Q4</span>
					<div class="col-11">
						<span>如何登入凭证注册中心？</span>
					</div>
				</div>
			</a>
			<div id="popup4-4" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>
								请上网至本行网络银行入口网站(https://ebank.tbb.com.tw)-&gt;输入网络银行登入数据-&gt;
								点选e化服务项下之「凭证注册中心」-&gt; 输入身分证/营利事业统一编号及网络银行交易密码，即可登入凭证注册中心。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q5 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-5" aria-expanded="true"
				aria-controls="popup4-5">
				<div class="row">
					<span class="col-1">Q5</span>
					<div class="col-11">
						<span>登入凭证注册中心申请凭证时，计算机是否须设定或调整？</span>
					</div>
				</div>
			</a>
			<div id="popup4-5" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>

<!-- 							<p> -->
<!-- 								A5：请由网络银行入口网登入后，再行登入「凭证注册中心」，参考联机帮助之「凭证载具驱动程序安装流程」及常见问题中之「网络环境设定篇」。<br> -->
<!-- 								<img alt="" src="./fstop/images/RAimage054.jpg" border="0">&nbsp;&nbsp;&nbsp;&nbsp; -->
<!-- 								<img alt="" src="./fstop/images/RAimage055.jpg" border="0"> -->
<!-- 							</p> -->
							<p>
								请由网络银行入口网登入后，再行登入「凭证注册中心」，参考联机帮助之「凭证载具驱动程序安装流程」及常见问题中之「网络环境设定篇」。
							</p>

						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q6 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-6" aria-expanded="true"
				aria-controls="popup4-6">
				<div class="row">
					<span class="col-1">Q6</span>
					<div class="col-11">
						<span>凭证申请、更新、暂禁..等各项功能如何操作？</span>
					</div>
				</div>
			</a>
			<div id="popup4-6" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
<!-- 							<p> -->
<!-- 								A6：客户可登入凭证注册中，点选「联机帮助」，即可得知各项作业之操作流程。<br> <img alt="" -->
<!-- 									src="./fstop/images/RA01.jpg" border="0"> -->
<!-- 							</p> -->
							<p>
								客户可登入凭证注册中，点选「联机帮助」，即可得知各项作业之操作流程。
							</p>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q7 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-7" aria-expanded="true"
				aria-controls="popup4-7">
				<div class="row">
					<span class="col-1">Q7</span>
					<div class="col-11">
						<span>凭证有效期限多长？</span>
					</div>
				</div>
			</a>
			<div id="popup4-7" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>目前为1年，凭证到期前30日客户应登入凭证注册中心办理凭证更新（展期）。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q8 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-8" aria-expanded="true"
				aria-controls="popup4-8">
				<div class="row">
					<span class="col-1">Q8</span>
					<div class="col-11">
						<span>申请/更新凭证需支付之费用金额？</span>
					</div>
				</div>
			</a>
			<div id="popup4-8" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>

							<p>法人户凭证每张NTD900元，个人户每张NTD150元，使用期限1年。</p>

						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- Q9 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup4-9" aria-expanded="true"
				aria-controls="popup4-9">
				<div class="row">
					<span class="col-1">Q9</span>
					<div class="col-11">
						<span>如何查询凭证是否已到期？</span>
					</div>
				</div>
			</a>
			<div id="popup4-9" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div class="ttb-result-list terms">
							<ol>
								<li>方式一：于台湾企银网络银行首页(https://ebank.tbb.com.tw)，点选『环境设定』,下载「9.台湾企银凭证载具读取工具下载」,依「台湾企银凭证载具读取工具介绍」操作说明查看「凭证有效到期日」。
								</li>
								<li>方式二：查询/更新/下载凭证-&gt;点「内容」，查看「凭证有效到期日」。</li>
							</ol>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>

	</div>

	<!-- Q10 -->
	<div class="ttb-pup-block" role="tab">
		<a role="button" class="ttb-pup-title collapsed d-block"
			data-toggle="collapse" href="#popup4-10" aria-expanded="true"
			aria-controls="popup4-10">
			<div class="row">
				<span class="col-1">Q10</span>
				<div class="col-11">
					<span>如何进行凭证更新展期？</span>
				</div>
			</div>
		</a>
		<div id="popup4-10" class="ttb-pup-collapse collapse" role="tabpanel">
			<div class="ttb-pup-body">
				<ul class="ttb-pup-list">
					<li>

<!-- 						<p> -->
<!-- 							A10：客户可于凭证到期日前30日，登入凭证注册中心，点选「用户更新凭证」，并选取欲展期之凭证后方之钮，即可进行凭证展期的动作。（详细操作流程请参考凭证注册中心之联机帮助）。<br> -->
<!-- 							<img src="./fstop/images/RA03.jpg"> -->
<!-- 						</p> -->
						<p>
							客户可于凭证到期日前30日，登入凭证注册中心，点选「用户更新凭证」，并选取欲展期之凭证后方之钮，即可进行凭证展期的动作。（详细操作流程请参考凭证注册中心之联机帮助）。
						</p>

					</li>
				</ul>
			</div>
		</div>
	</div>

	<!-- Q11-->
	<div class="ttb-pup-block" role="tab">
		<a role="button" class="ttb-pup-title collapsed d-block"
			data-toggle="collapse" href="#popup4-11" aria-expanded="true"
			aria-controls="popup4-11">
			<div class="row">
				<span class="col-1">Q11</span>
				<div class="col-11">
					<span>凭证过期时(无法执行凭证更新)如何处理？</span>
				</div>
			</div>
		</a>
		<div id="popup4-11" class="ttb-pup-collapse collapse" role="tabpanel">
			<div class="ttb-pup-body">
				<ul class="ttb-pup-list">
					<li>

						<p>请参照Q3至本行申请凭证，取得本行发给之凭证识别数据（Common
							Name，CN）后，即可登入凭证注册中心申请签发凭证。</p>

					</li>
				</ul>
			</div>
		</div>
	</div>

	<!-- Q12 -->
	<div class="ttb-pup-block" role="tab">
		<a role="button" class="ttb-pup-title collapsed d-block"
			data-toggle="collapse" href="#popup4-12" aria-expanded="true"
			aria-controls="popup4-12">
			<div class="row">
				<span class="col-1">Q12</span>
				<div class="col-11">
					<span>若凭证申请/更新(展期)时发生扣款失败，应如何处理？</span>
				</div>
			</div>
		</a>
		<div id="popup4-12" class="ttb-pup-collapse collapse" role="tabpanel">
			<div class="ttb-pup-body">
				<ul class="ttb-pup-list">
					<li>

<!-- 						<p> -->
<!-- 							A12：若扣款失败原因为“账户余额不足”，请先确定银行账户内有足够的金额可供扣取费用；失败原因若为“查无扣款账号”，请洽本行设定扣帐账号或缴交凭证费用，上述作业完成后请按下列步骤重新申请/更新凭证：<br> -->
<!-- 							1、点选「查询/更新/下载凭证」。<br> <img src="./fstop/images/RA04.jpg"><br> -->
<!-- 							2、点选凭证后方之<img src="./fstop/images/RA05.jpg">钮。<br> -->
<!-- 							3、点选<img src="./fstop/images/RA06.jpg">钮，再次申请/更新凭证。 -->
<!-- 						</p> -->
						<p>
							若扣款失败原因为“账户余额不足”，请先确定银行账户内有足够的金额可供扣取费用；失败原因若为“查无扣款账号”，请洽本行设定扣帐账号或缴交凭证费用，上述作业完成后请按下列步骤重新申请/更新凭证：<br>
						</p>
						<ol>
							<li>1、点选「查询/更新/下载凭证」。</li>
							<li>2、点选凭证后方之小图示钮。</li>
							<li>3、点选重送申请凭证钮，再次申请/更新凭证。</li>
						</ol>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<!-- Q12 -->
	<div class="ttb-pup-block" role="tab">
		<a role="button" class="ttb-pup-title collapsed d-block"
			data-toggle="collapse" href="#popup4-13" aria-expanded="true"
			aria-controls="popup4-13">
			<div class="row">
				<span class="col-1">Q13</span>
				<div class="col-11">
					<span>若客户更换计算机或更换计算机操作系统，可继续使用原凭证吗？</span>
				</div>
			</div>
		</a>
		<div id="popup4-13" class="ttb-pup-collapse collapse" role="tabpanel">
			<div class="ttb-pup-body">
				<ul class="ttb-pup-list">
					<li>

						<p>客户需执行「网络环境设定」及「载具驱动程序安装」，即可继续使用原凭证，请参考Q5之计算机设定操作说明。</p>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<!-- Q14 -->
	<div class="ttb-pup-block" role="tab">
		<a role="button" class="ttb-pup-title collapsed d-block"
			data-toggle="collapse" href="#popup4-14" aria-expanded="true"
			aria-controls="popup4-14">
			<div class="row">
				<span class="col-1">Q14</span>
				<div class="col-11">
					<span>若凭证有被冒用、暴露及遗失等不安全之顾虑时，应如何办理？</span>
				</div>
			</div>
		</a>
		<div id="popup4-14" class="ttb-pup-collapse collapse" role="tabpanel">
			<div class="ttb-pup-body">
				<ul class="ttb-pup-list">
					<li>

						<p>客户应即登入凭证注册中心或洽本行办理凭证暂时停用或废止。</p>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<!-- Q15 -->
	<div class="ttb-pup-block" role="tab">
		<a role="button" class="ttb-pup-title collapsed d-block"
			data-toggle="collapse" href="#popup4-15" aria-expanded="true"
			aria-controls="popup4-15">
			<div class="row">
				<span class="col-1">Q15</span>
				<div class="col-11">
					<span>载具密码错误几次会锁住？</span>
				</div>
			</div>
		</a>
		<div id="popup4-15" class="ttb-pup-collapse collapse" role="tabpanel">
			<div class="ttb-pup-body">
				<ul class="ttb-pup-list">
					<li>

						<p>载具密码错误超过5次会锁住。</p>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<!-- Q16 -->
	<div class="ttb-pup-block" role="tab">
		<a role="button" class="ttb-pup-title collapsed d-block"
			data-toggle="collapse" href="#popup4-16" aria-expanded="true"
			aria-controls="popup4-16">
			<div class="row">
				<span class="col-1">Q16</span>
				<div class="col-11">
					<span>载具密码锁住时，应如何处理？</span>
				</div>
			</div>
		</a>
		<div id="popup4-16" class="ttb-pup-collapse collapse" role="tabpanel">
			<div class="ttb-pup-body">
				<ul class="ttb-pup-list">
					<li>

						<p>请将载具送回营业单位并办理重置作业相关手续。</p>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<!-- Q17 -->
	<div class="ttb-pup-block" role="tab">
		<a role="button" class="ttb-pup-title collapsed d-block"
			data-toggle="collapse" href="#popup4-17" aria-expanded="true"
			aria-controls="popup4-17">
			<div class="row">
				<span class="col-1">Q17</span>
				<div class="col-11">
					<span>如何补印「凭证费用收据」？</span>
				</div>
			</div>
		</a>
		<div id="popup4-17" class="ttb-pup-collapse collapse" role="tabpanel">
			<div class="ttb-pup-body">
				<ul class="ttb-pup-list">
					<li>

<!-- 						<p> -->
<!-- 							A17：查询/更新/下载凭证-&gt;点「内容」，选「打印凭证费用收据」。<br> <img -->
<!-- 								src="./fstop/images/RAimage050.jpg"><br> <img -->
<!-- 								src="./fstop/images/RAimage051.jpg"><br> <img -->
<!-- 								src="./fstop/images/RAimage053.jpg"> -->
<!-- 						</p> -->
						<p>
							查询/更新/下载凭证-&gt;点「内容」，选「打印凭证费用收据」。
						</p>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>