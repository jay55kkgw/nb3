<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript"
	src="${__ctx}/js/jquery.datetimepicker.js"></script>
<!-- 變更密碼所需JS -->
<script type="text/javascript"
	src="${__ctx}/component/util/cardReaderChangePin2.js"></script>
<script type="text/javascript"
	src="${__ctx}/component/util/cardReaderChangePin.js"></script>
<script type="text/javascript">
	function goback(){
		location.href='${__ctx}/DIGITAL/ACCOUNT/financial_card_renew';
	}	
</script>
</head>

<body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Register" /></li>
    <!-- 數位存款帳戶     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0348" /></li>
    <!-- 數位存款帳戶補申請晶片金融卡     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1553" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<!-- 數位存款帳戶補申請晶片金融卡 -->
			<h2>補申請晶片金融卡</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="" onSubmit="return processQuery()">
				<input type="hidden" name="ADOPID" value="NB31"> 
				<input type="hidden" name="CUSIDN" value="${financial_card_renew_step5.data.CUSIDN}"> 
				<input type="hidden" name="ACN" value="${financial_card_renew_step5.data.ACN}"> 
				<input type="hidden" name="TRNTYP" value="02">
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<ul class="ttb-result-list">
							<li>
								<h3><spring:message code="LB.Transaction_code" /></h3>
								<p>${financial_card_renew_step5.msgCode}</p>
							</li>
							<li>
								<h3><spring:message code="LB.Transaction_message" /></h3>
								<p>${financial_card_renew_step5.message}</p>
							</li>
						</ul>
						<div class="text-left">
							<ol class="list-decimal text-left">
							</ol>
						</div>
						<!-- 重新輸入 -->
						<input type="button" class="ttb-button btn-flat-gray" name="back" value="<spring:message code="LB.Re_enter" />" onclick="goback();"/>
						<input type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.X0194"/>" name="CLOSEPAGE" onClick="'_self'.close();" >
					</div>
				</div>
			</form>
		</section>
		</main>

		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>

</html>