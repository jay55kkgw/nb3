<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<script type="text/javascript">
	$(document).ready(function () {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()",10);
		// 開始查詢資料並完成畫面
		setTimeout("init()",20);
		
		setTimeout("initDataTable()",100);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)",500);
		$("#printbtn").click(function(){
			var count = $('#count').html();
			var i18n = new Object();
			i18n['jspTitle']='<spring:message code="LB.Loan_Detail_Inquiry_Next_Period" />'
			var params = {
				"jspTemplateName":"next_loan_print",
				"jspTitle":i18n['jspTitle'],
				"CMQTIME":" ${nextloan_query.data.CMQTIME}",
				"COUNT":" ${nextloan_query.data.CMRECNUM}",
				"NOTE":"${nextloan_query.data.note}",
				"MSGFLG_014":"${nextloan_query.data.MSGFLG_014}",
				"TOPMSG_014":"${nextloan_query.data.TOPMSG_014}",
				"TOPMSG_553":"${nextloan_query.data.TOPMSG_553}",
				"ADMSGOUT_014":"${nextloan_query.data.errorMsg014}",
				"ADMSGOUT_553":"${nextloan_query.data.errorMsg553}"
			};
			openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
		});
	
	});
	// 將.table變更為footable
	
</script>

</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 貸款服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Loan_Service" /></li>
    <!-- 借款查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Loan_Inquiry" /></li>
    <!-- 下期借款本息查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Loan_Detail_Inquiry_Next_Period" /></li>
		</ol>
	</nav>

	
		<!-- menu 及登出窗格 -->
		<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 功能選單內容 -->
		
		<main class="col-12"> 
			<section id="main-content" class="container"> <!-- 主頁內容  -->
				<h2>
					<spring:message code="LB.Loan_Detail_Inquiry_Next_Period" /><!-- 下期借款本息查詢 -->
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>

			<div class=" main-content-block row">
				<div class="col-12 tab-content">
					<ul class="ttb-result-list">
						<li>
							<h3>
								<spring:message code="LB.Inquiry_time" /> 
							</h3>
							<p>
								 ${nextloan_query.data.CMQTIME}<!-- <h3>查詢時間</h3> -->
							</p>
						</li>
						<li>
								<!-- 查詢總數 -->
							<h3>
								<spring:message code="LB.Total_records" />
							</h3>
							<p>	
								<span  id="count">${nextloan_query.data.CMRECNUM}</span> <spring:message code="LB.Rows" /><!-- 筆 -->
							</p>
						</li>
					</ul>
					<ul class="ttb-result-list" style="display:block;">
					<li>
							<h3>
								<spring:message code="LB.Note" /> <!-- <h3>備註</h3> -->
							</h3>
							<p><spring:message code="LB.Remind_you" /><!-- 提醒您 -->！
								<spring:message code="LB.NextLoan_P1_note" />
							<!-- 「應繳金額」係以查詢日之適用利率計算，於下期繳款日前遇有利率調整情事，「應繳金額」將隨同調整。 -->
							</p>
							<br>
								<p style="color:red"> 
									${nextloan_query.data.note}
								</p>
<!-- 							<p style="color:red">借款分號13 1050329 本行１年定儲調整為1.160%。借款分號23 1040822 郵二定儲利率調整為10.000%。借款分號43 1040822 郵二定儲利率調整為10.000%。借款分號63 1040822 郵二定儲利率調整為10.000%。借款分號83 1040822 郵二定儲利率調整為10.000%。</p> -->					</li>
					</ul>
					<ul class="ttb-result-list">
						<li>
							<!-- 查詢時間 -->
							<h3>
								<spring:message code="LB.Report_name" />
							</h3>
							<p>
								<spring:message code="LB.NTD_loan_detail_inquiry_next_period" />
							</p>
						</li>
					</ul>
					<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
						<thead>
<%-- 						<tr> --%>
<%-- 							<th><spring:message code="LB.Report_name" /></th> <!-- 表名 --> --%>
<%-- 							<th class="text-left" colspan="6"><spring:message code="LB.NTD_loan_detail_inquiry_next_period" /></th><!-- 臺幣下期借款本息查詢 --> --%>
<%-- 						</tr> --%>
						<tr>
							<th data-title='<spring:message code="LB.Loan_account"/>'><spring:message code="LB.Loan_account" /></th> <!-- 帳號 -->
							<th data-title='<spring:message code="LB.Seq_of_account"/>'><spring:message code="LB.Seq_of_account" /></th><!-- 分號 -->
							<th data-title='<spring:message code="LB.Original_loan_amount"/>' data-breakpoints="xs sm"><spring:message code="LB.Original_loan_amount" /></th><!-- 原貸金額 -->
							<th data-title='<spring:message code="LB.Loan_Outstanding"/>' data-breakpoints="xs sm"><spring:message code="LB.Loan_Outstanding" /></th><!-- 貸款餘額 -->
							<th data-title='<spring:message code="LB.End_date_of_repay_interest"/>' data-breakpoints="xs sm"><spring:message code="LB.End_date_of_repay_interest" /></th><!-- 繳息迄日 -->
							<th data-title='<spring:message code="LB.Repayment_of_every_month"/>'><spring:message code="LB.Repayment_of_every_month" /></th><!-- 應繳金額 -->
							<th data-title='<spring:message code="LB.Repay_Date_of_every_month"/>' data-breakpoints="xs sm"><spring:message code="LB.Repay_Date_of_every_month" /></th><!-- 下期繳款日 -->
						</tr>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${nextloan_query.data.TOPMSG_014 != 'OKLR'}">
										<tr>
											<td class="text-center">${nextloan_query.data.TOPMSG_014}</td>
											<td class="text-center">${nextloan_query.data.errorMsg014}</td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
								</c:when>
								<c:when test="${nextloan_query.data.TOPMSG_014 == 'OKLR'}">
									<c:choose>
										<c:when test="${nextloan_query.data.MSGFLG_014 eq '01'}">
											<tr>
												<td class="text-center">MSGFLG=01</td>
												<td class="text-center"><spring:message code="LB.Loan_contact_note" /></td><!-- 請洽櫃台 -->
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
											</tr>
										</c:when>
										<c:when test="${nextloan_query.data.MSGFLG_014 eq '00'}">
											<c:forEach var="dataList" items="${nextloan_query.data.TW}">
												<tr>
													<td class="text-center">${dataList.ACN}</td>
													<td class="text-center">${dataList.SEQ}</td>
													<td class="text-right">${dataList.AMTORLN}</td>
													<td class="text-right">${dataList.BAL}</td>
													<td class="text-center">${dataList.DATITPY}</td>
													<td class="text-right">${dataList.AMTAPY}</td>
													<td class="text-center">${dataList.DDT}</td>
												</tr>
											</c:forEach>
										</c:when>
									</c:choose>
								</c:when>
							</c:choose>
						</tbody>
					</table>
					
					<ul class="ttb-result-list">
						<li>
							<!-- 查詢時間 -->
							<h3>
								<spring:message code="LB.Report_name" />
							</h3>
							<p>
								<spring:message code="LB.FX_loan_detail_inquiry_next_period" />
							</p>
						</li>
					</ul>
					<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
						<thead>
<%-- 						<tr> --%>
<%-- 							<th><spring:message code="LB.Report_name" /></th> <!-- 表名 --> --%>
<%-- 							<th class="text-left" colspan="7"><spring:message code="LB.FX_loan_detail_inquiry_next_period" /></th><!-- 外幣下期借款本息查詢 --> --%>
<%-- 						</tr> --%>
						<tr>
							<th data-title='<spring:message code="LB.Loan_account"/>'><spring:message code="LB.Loan_account" /></th> <!-- 帳號 -->
							<th data-title='<spring:message code="LB.Loan_CaseNo"/>'><spring:message code="LB.Loan_CaseNo" /></th><!-- 案件編號 -->
							<th data-title='<spring:message code="LB.Currency"/>' data-breakpoints="xs sm"><spring:message code="LB.Currency" /></th><!-- 幣別 -->
							<th data-title='<spring:message code="LB.Loan_Outstanding"/>' data-breakpoints="xs sm"><spring:message code="LB.Loan_Outstanding" /></th><!-- 貸款餘額 -->
							<th data-title='<spring:message code="LB.End_date_of_repay_interest"/>' data-breakpoints="xs sm"><spring:message code="LB.End_date_of_repay_interest" /></th><!-- 繳息迄日 -->
							<th data-title='<spring:message code="LB.Repayment_of_every_month"/>' data-breakpoints="xs sm"><spring:message code="LB.Repayment_of_every_month" /></th><!-- 應繳金額 -->
							<th data-title='<spring:message code="LB.Repay_Date_of_every_month"/>' data-breakpoints="xs sm"><spring:message code="LB.Repay_Date_of_every_month" /></th><!-- 下期繳款日 -->
							<th data-title='<spring:message code="LB.Data_date"/>' data-breakpoints="xs sm"><spring:message code="LB.Data_date" /></th><!-- 資料日期 -->
						</tr>
						</thead>
						<tbody>
								<c:if test="${nextloan_query.data.TOPMSG_553 eq 'OKLR'}">
									<c:forEach var="dataList2" items="${nextloan_query.data.FX}">
										<tr>
											<td class="text-center">${dataList2.LNACCNO}</td>
											<td class="text-center">${dataList2.LNREFNO}</td>
											<td class="text-center">${dataList2.LNCCY}</td>
											<td class="text-right">${dataList2.LNCOS}</td>
											<td class="text-center">${dataList2.CUSPYDT}</td>
											<td class="text-right">${dataList2.PRNDAMT}</td>
											<td class="text-center">${dataList2.NXTINTD}</td>
											<td class="text-center">${dataList2.LNUPDATE}</td>
										</tr>
									</c:forEach>
								</c:if>
								<c:if test="${nextloan_query.data.TOPMSG_553 != 'OKLR'}">
									<tr>
										<td class="text-center">${nextloan_query.data.TOPMSG_553}</td>
										<td class="text-center">${nextloan_query.data.errorMsg553}</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
								</c:if>
						</tbody>
					</table>
					
					<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />" /> <!-- 列印 -->
				</div>
			</div>
			<!-- 說明 -->
						<ol class="description-list list-decimal">
							<p><spring:message code="LB.Description_of_page" /><!-- 說明 --></p>
								<li><spring:message code="LB.NextLoan_P1_D1" />
								<!-- 本項查詢不包含存單質借、透支、個人投資理財貸款及好享貸回復型額度。 -->
								</li>
								<li><spring:message code="LB.NextLoan_P1_D2" />
								<!-- 延滯繳息還本者，請逕洽原承貸分行辦理。 -->
								</li>
								<li><spring:message code= "LB.NextLoan_P1_D3" /></li>
								<li><spring:message code= "LB.NextLoan_P1_D4" /></li> 
								<li><spring:message code="LB.NextLoan_P1_D5" />
									<!-- 消費者新台幣借款之利率調整通知為貸款利率引用本行２年定儲、 本行定儲指數、本行基準利率、本行１年定儲、本行定儲指數 、郵一定儲利率、行員中期利率、郵二定儲利率 、行員消貸利率、 基準利率月調 ，利率引用標準非屬上開者，請逕洽原貸分行。 -->
								</li>
								<li><spring:message code="LB.NextLoan_P1_D6" />
								<!-- 消費者新台幣借款倘延滯繳息還本者，利率引用標準之調整通知請逕洽原貸分行。 -->
								</li>
						</ol>
		</section>
		<!-- 		main-content END --> 
		</main>
	</div> 
		<!-- 	content row END -->
	
	
	<%@ include file="../index/footer.jsp"%>
</body>
</html>