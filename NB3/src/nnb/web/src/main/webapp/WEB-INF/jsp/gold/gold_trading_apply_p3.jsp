<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
	    <script type="text/javascript">
        $(document).ready(function () {
            initFootable(); // 將.table變更為footable 
            init();
        });

        function init() {
	    	//進行初始化
    		//網路交易帳戶設定
    		$("#ACN").children().each(function(){
    		    if ($(this).val()=='${input_data.ACN}'){
    		        $(this).attr("selected", true);
    		    }
    		});
    		//網路交易指定新台幣帳戶
    		$("#SVACN").children().each(function(){
    		    if ($(this).val()=='${input_data.SVACN}'){
    		        $(this).attr("selected", true);
    		    }
    		});
			$("#formId").validationEngine({binded: false,promptPosition: "inline"});
	    	$("#CMSUBMIT").click(function(e){
				e = e || window.event;
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					$("#CUSIDN2").prop("disabled",false);
		        	initBlockUI();
					var action = '${__ctx}/GOLD/APPLY/gold_trading_apply_p4';
					$("form").attr("action", action);
	    			$("form").submit();
				}
			});
	    	$("#CMBACK").click(function(e){		
				$("#formId").validationEngine('detach');	
// 	        	initBlockUI();
				var action = '${__ctx}/GOLD/APPLY/gold_trading_apply_p2';
				$("form").attr("action", action);
    			$("form").submit();
			});
        }

    </script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 黃金存摺帳戶     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2273" /></li>
    <!-- 黃金存摺網路交易申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1687" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!-- 	線上申請黃金存摺網路交易 -->
					<spring:message code="LB.W1687"/>
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <div id="step-bar">
                    <ul>
						<li class="finished">注意事項與權益</li>
                        <li class="active">設定帳戶</li>
                        <li class="">確認資料</li>
                        <li class="">申請結果</li>
                    </ul>
                </div>
                <form id="formId" method="post">
					<input type="hidden" name="AMT_FEE" value="${n920_data.data.fee}">
					<input type="hidden" name="action" value="forward">
					<input type="hidden" id="TOKEN" name="TOKEN" value="${sessionScope.transfer_confirm_token}" /><!-- 防止重複交易 -->
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
							<div class="ttb-input-block">
	                            <div class="ttb-message">
	                                <p>設定帳戶</p>
	                            </div>
	                            <div>
	                                <p>請選擇您的黃金存摺帳號與網路交易指定帳戶。</p>
	                            </div>
								<!-- ***************** image ****************** -->
	                            <div class="classification-block">
	                                <p>網路交易帳戶設定</p>
	<!-- 	                                <a href="#" class="classification-edit-btn">編輯</a> -->
	                            </div>
								<!-- ***************************************** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<!--黃金存摺帳號-->
											<spring:message code="LB.D1090"/>
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<select name="ACN" id="ACN" class="custom-select select-input half-input validate[required]">
												<option value="">
												<!-- 請選擇黃金帳號 -->
													<spring:message code="LB.W1688"/>
												</option>
												<c:forEach var="dataList" items="${result_data_acnos.data.REC}">
													<option> ${dataList.ACN}</option>
												</c:forEach>
												<c:if test="${ !result_data_acnos.data.ALL.equals('') }">
													<option value="${result_data_acnos.data.ALL}"><spring:message code="LB.All"/></option>
												</c:if>
											</select>
										</div>
									</span>
								</div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
										<!-- 指定網路銀行交易/申購/回售新臺幣帳戶 -->
											網路交易指定新台幣帳戶
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<select name="SVACN" id="SVACN" class="custom-select select-input half-input validate[required]">
												<option value="">
												<!-- 請選擇轉出帳號 -->
													<spring:message code="LB.W1544"/>
												</option>
												<c:forEach var="dataList" items="${result_data.data.REC}">
													<option> ${dataList.ACN}</option>
												</c:forEach>
											</select>
										</div>
									</span>
								</div>
							</div>
                           
	                        <!--button 區域 -->
							<!-- 重新輸入 -->
<%-- 								<spring:message code="LB.Re_enter" var="cmRest"></spring:message> --%>
<%-- 								<input type="reset" name="CMRESET" id="CMRESET" value="${cmRest}" class="ttb-button btn-flat-gray"> --%>
                            <input id="CMBACK" name="CMBACK" type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.X0318" />" />
                            <input id="CMSUBMIT" name="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.X0080"/>" />
                        <!--                     button 區域 -->
                        </div>  
                    </div>
                </form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>