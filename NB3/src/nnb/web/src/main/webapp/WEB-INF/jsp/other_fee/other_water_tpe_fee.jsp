<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="${__ctx}/js/TaxGovernment.js"></script>
	    <script type="text/javascript">
        $(document).ready(function () {
            initFootable(); // 將.table變更為footable 
            init();
        });

        function init() {
    		$("#TSFACN").children().each(function(){
    		    if ($(this).val()=='${backenData.TSFACN}'){
    		        $(this).prop("selected", true);
    		    }
    		});
    		$("#CARDNUM").children().each(function(){
    		    if ($(this).val()=='${backenData.CARDNUM}'){
    		        $(this).prop("selected", true);
    		    }
    		});
			if('${backenData.R1}' == 'V1')
    			$("#R1").attr("checked",true);
			if('${backenData.R1}' == 'V2')
    			$("#R2").attr("checked",true);
			$("#formId").validationEngine({binded: false,promptPosition: "inline"});
	    	$("#pageshow").click(function(e){
	        	if($('input[name="R1"]:checked').val() == 'V1'){
	        		$('#TSFACN').addClass('validate[required]');
	        		$('#CARDNUM').removeClass('validate[required]');
	        	}else{
	        		$('#CARDNUM').addClass('validate[required]');
	        		$('#TSFACN').removeClass('validate[required]');
	        	}
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
	        		$('#CUSNUM').val($('#CUSNUM1').val()+$('#CUSNUM2').val()+$('#CUSNUM3').val()+$('#CUSNUM4').val());
					$("#formId").validationEngine('detach');
		        	initBlockUI();
		        	var action = '';
		        	if($('input[name="R1"]:checked').val() == 'V1'){
						action = '${__ctx}/OTHER/FEE/x_article';
		        	}else{
						action = '${__ctx}/OTHER/FEE/c_article';
		        	}
					$("form").attr("action", action);
	    			$("form").submit();
				}
			});
			$("#CMRESET").click(function(e){
				$("#formId")[0].reset();
				getTmr();
			});

			if('${ result_data_n810.data.i_Record }' == '0' && ${ result_data_acnos.data.REC.size() } <= 0)
			{
				//alert("<spring:message code= "LB.Alert168" />");
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert168' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
				$("#pageshow").attr("disabled",true);
				$("#pageshow").removeClass('btn-flat-orange');
			}
			exampleClick();
        }
		function next(tObject,len,nObject){
			console.log($("#" + tObject).val().length)
			if($("#" + tObject).val().length >= len)
				$("#" + nObject).focus();
		}
		function exampleClick(){
			//範例
			$('#EXAMPLE').click(function() {
				window.open('${__ctx}/img/TaipeiWater.PNG');
			});
		}
    </script>
</head>
<body>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %> 
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 自動扣繳服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2360" /></li>
    <!-- 臺北市水費代扣繳申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0665" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!-- 預約黃金交易查詢/取消 -->
					<!-- <spring:message code="LB.NTD_Demand_Deposit_Detail" /> -->
					<spring:message code="LB.W0665" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>						
				
                <form id="formId" method="post">
					<input type="hidden" name="action" value="forward">
				    <input type="hidden" name="TYPE" value="04">
				    <input type="hidden" name="type_str" value="other_water_tpe_fee">
				    <input type="hidden" name="ITMNUM" value="1">
				    <input type="hidden" name="ADOPID" value="N8304">
   	 				<input type="hidden" name="CUSNUM" id="CUSNUM" >
                    <!-- 顯示區  -->
                    <div class="main-content-block row">
                        <div class="col-12">
							<div class="ttb-input-block">
								<div class="ttb-message">
	                                <span><spring:message code="LB.W0649" /></span>
	                            </div>
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.W0636" />
										</h4>
									</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<c:if test="${ result_data_acnos.data.REC.size() > 0 }">
												<label class="radio-block">
													<spring:message code="LB.D0988" />
													<input type="radio" name="R1" id="R1" value="V1" checked />
													<span class="ttb-radio"></span>
												</label>
										</div>
										<div class="ttb-input">
												<select name="TSFACN" id="TSFACN" class="custom-select select-input half-input  validate[required]">
													<option value="">
														----------<spring:message code="LB.Select" />----------
													</option>
													<c:forEach var="dataList" items="${result_data_acnos.data.REC}">
														<option value="${dataList.ACN}"> ${dataList.ACN}</option>
													</c:forEach>
												</select>
											</c:if>
											<c:if test="${ result_data_acnos.data.REC.size() <= 0 }">
												<label class="radio-block">
													<spring:message code="LB.D0988" /><spring:message code="LB.W0640" />
													<input type="radio" name="R1" id="R1" value="V1" disabled />
													<span class="ttb-radio"></span>
												</label>
											</c:if>
										</div>
										<div class="ttb-input">
										
											<c:if test="${ !result_data_n810.data.i_Record.equals('0') }">
												<label class="radio-block">
													<spring:message code="LB.W0639" />
													<c:if test="${ result_data_acnos.data.REC.size() > 0 }">
														<input type="radio" name="R1" id="R2" value="V2" />
													</c:if>	
													<c:if test="${ result_data_acnos.data.REC.size() <= 0 }">
														<input type="radio" name="R1" id="R2" value="V2" checked />
													</c:if>
													<span class="ttb-radio"></span>
												</label>
										</div>
										<div class="ttb-input">
												<select name="CARDNUM" id="CARDNUM" class="custom-select select-input half-input  validate[required]">
													<option value="">
														----------<spring:message code="LB.Select" />----------
													</option>
													<c:forEach var="dataList" items="${result_data_n810.data.REC}">
														<option value="${dataList.CARDNUM}"> ${dataList.CARDNUM}</option>
													</c:forEach>
												</select>
											</c:if>
											<c:if test="${ result_data_n810.data.i_Record.equals('0') }">
												<label class="radio-block">
													<spring:message code="LB.W0639" /> <spring:message code="LB.W0640" />
													<input type="radio" name="R1" id="R2" value="V2" disabled />
													<span class="ttb-radio"></span>
												</label>
											</c:if>
										</div>
									</span>
								</div>
								<!-- ********** -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.W0655" />
										</h4>
									</label>
									</span> 
									<span class="input-block one-line">
										<div class="ttb-input">
	                                        <span class="input-subtitle subtitle-color"><spring:message code="LB.W0673" />：</span>
	                                        <input maxLength="1" size="1" class="card-input text-input" name="CUSNUM1" id="CUSNUM1" value="${backenData.CUSNUM1}" onKeyUp="next('CUSNUM1',1,'CUSNUM2')"> 
	                                    </div>
	                                    <div class="ttb-input">
	                                     	<span class="input-subtitle subtitle-color"><spring:message code="LB.W0674" />：</span>
	                                        <input maxLength="2" size="2" class="card-input text-input validate[required,funcCall[validate_CheckLenEqual['<spring:message code= "LB.W0674" />',CUSNUM2,false,2]]]"  value="${backenData.CUSNUM2}" name="CUSNUM2" id="CUSNUM2" onKeyUp="next('CUSNUM2',2,'CUSNUM3')"> 
	                                    </div>
	                                    <div class="ttb-input">   
	                                        <span class="input-subtitle subtitle-color"><spring:message code="LB.W0675" />：</span>
	                                        <input maxLength="6" size="6" class="text-input validate[required,funcCall[validate_CheckLenEqual['<spring:message code= "LB.W0675" />',CUSNUM3,false,6]]]" value="${backenData.CUSNUM3}" name="CUSNUM3" id="CUSNUM3" onKeyUp="next('CUSNUM3',6,'CUSNUM4')">
	                                    </div>
	                                    <div class="ttb-input">
	                                        <span class="input-subtitle subtitle-color"><spring:message code="LB.W0658" />：</span>
	                                        <input maxLength="1" size="1" class="card-input text-input" name="CUSNUM4" id="CUSNUM4" value="${backenData.CUSNUM4}">
											
											<button type="button" class="btn-flat-orange" name="EXAMPLE" id="EXAMPLE">
												<spring:message code="LB.W0438" />											
											</button>
	                                    </div> 
									</span>
								</div>
							</div>
                           
	                        <!--button 區域 -->
	                        
	                            <input id="pageshow" name="pageshow" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.X0080" />" />
	                        
                        <!--                     button 區域 -->
                        </div>  
                    </div>
                </form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>
