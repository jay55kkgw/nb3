<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",100);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",200);
	
	$("#CMSUBMIT").click(function(){
		if($('#ageFlag').prop('checked')){
			initBlockUI();
			$("#formID").attr("action","${__ctx}/BOND/PURCHASE/bond_purchase_confirm");
			initBlockUI();//遮罩
			$("#formID").submit();
		}
		else{
			errorBlock(null, null, ['<spring:message code= "LB.X2559" />'],
					'<spring:message code= "LB.Confirm" />', null);
		}
	});

	$("#CMCANCEL").click(function(){
		initBlockUI();
		$("#formID").attr("action","${__ctx}/BOND/PURCHASE/bond_purchase_input");
		$("#formID").submit();
	});
	
});

</script>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 海外債券     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2391" /></li>
    <!-- 海外債券交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2515" /></li>
    <!-- 高齡聲明書     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X2560" /></li>
		</ol>
	</nav>

	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
<!-- 				<h2>高齡聲明書</h2> -->
				<h2><spring:message code="LB.X2560" /></h2>
				<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
				<form id="formID" method="post">
					<input type="hidden" id="TOKEN" name = "TOKEN" value="${sessionScope.transfer_confirm_token}">
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<div class="ttb-message"">
	                                <h4>聲 明 書(高齡客戶適用)</h4>
	                            </div>
								<div class="ttb-message" style="text-align: left; width: 50%; margin-left: 25%; margin-left: 25%; margin-right: 25%;">
                                	<span><b>本人欲透過貴行購買本商品已完全瞭解本金融商品之相關風險，且知悉並同意貴行基於保護投資人之立場針對投資人年齡加上投資產品約定到期最長年限若大於或等於70時，得予以婉拒之做法。但因本人已充分考量自身年齡與本產品天期之關係，並充分了解本產品相關投資風險，特別是流動性風險，即當產品不具備充分之市場流通性時，對提前贖回指示單無法保證成交，且實際交易價格可能會與產品本身之單位資產價值產生顯著之價差，造成投資人於到期前贖回，會發生可能損及原始投資本金之風險。本人特此聲明確實瞭解，並同意承擔承作上開產品所生之一切風險，特請貴行予以受理承作。倘日後該產品發生任何風險或本人有任何損失，將完全由本人自行承擔，與貴行無涉，絕無異議。</b></span>
                            	</div>
								<div class="ttb-message" style="text-align: center; width: 50%; margin-left: 25%; margin-left: 25%; margin-right: 25%;">
                                	<h5><b>(本人年齡 + 本產品約定到期最長年限≧70)</b></h5>
                            	</div>
                            	<div class="ttb-input" style="text-align: center;">
									<label class="check-block" for="ageFlag">
										<input type="checkbox" name="ageFlag" id="ageFlag">
<!-- 										<b>本人已詳閱並瞭解本聲明書之內容</b> -->
										<b><spring:message code="LB.X2561" /></b>
		                                <span class="ttb-check"></span>
									</label>
                                </div>
                            </div>
							<input type="button" id="CMCANCEL" value="<spring:message code="LB.Cancel"/>" class="ttb-button btn-flat-gray"/>	
							<input type="button" id="CMSUBMIT" value="<spring:message code="LB.Confirm"/>" class="ttb-button btn-flat-orange"/>
						</div>
					</div>	
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>