<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ include file="../__import_js.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
	$(document).ready(function() {
		window.print();
	});
</script>
</head>

<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br />
	<br />
	<div style="text-align:center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br />
	<br />
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<br />
	<div style="text-align:center">${MsgName}</div>
	<br />
	<table class="print">
		<c:if test="${FGTRDATE == '0'}">
			<tr>
				<td style="width: 8em">
					<!-- 交易時間 -->
					<spring:message code="LB.Trading_time" />
				</td>
				<td>${CMQTIME}</td>
			</tr>
		</c:if>
		<c:if test="${FGTRDATE == '1'}">
			<tr>
				<td style="width:8em">
					<!-- 資料時間 -->
					<spring:message code="LB.Data_time" />
				</td>
				<td>${CMQTIME}</td>
			</tr>
			<!-- 預約才有轉帳日期 -->
			<tr>
				<td style="width:8em">
					<!-- 轉帳日期 -->
					<spring:message code="LB.Transfer_date" />
				</td>
				<td>${SHOW_TRDATE}</td>
			</tr>
		</c:if>
		<tr>
			<td class="ColorCell">
				<!-- 存單帳號 -->
				<spring:message code="LB.Account" />
			</td>
			<td>${ACN}</td>
		</tr>
		<tr>
			<td class="ColorCell">
				<!-- 存單號碼 -->
				<spring:message code="LB.Certificate_no" />
			</td>
			<td>${FDPNUM}</td>
		</tr>
		<tr>
			<td class="ColorCell">
				<!-- 幣別 -->
				<spring:message code="LB.Currency" />
			</td>
			<td class="DataCell">${CUID}</td>
		</tr>
		<tr>
			<td class="ColorCell">
				<!-- 存單金額 -->
				<spring:message code="LB.Certificate_amount" />
			</td>
			<td>${AMT}</td>
		</tr>
		<tr>
			<td class="ColorCell">
				<!-- 計息方式 -->
				<spring:message code="LB.Interest_calculation" />
			</td>
			<td>${INTMTH}</td>
		</tr>
		<tr>
			<td class="ColorCell">
				<!-- 起存日 -->
				<spring:message code="LB.Start_date" />
			</td>
			<td class="DataCell">${SHOWDPISDT}</td>
		</tr>
		<tr>
			<td class="ColorCell">
				<!-- 到期日 -->
				<spring:message code="LB.Maturity_date" />
			</td>
			<td class="DataCell">${SHOWDUEDAT}</td>
		</tr>
		<c:if test="${FGTRDATE == '0'}">
			<tr>
				<td class="ColorCell">
					<!-- 利息 -->
					<spring:message code="LB.Interest" />
				</td>
				<td>${INT}</td>
			</tr>
			<tr>
				<td class="ColorCell">
					<!-- 所得稅 -->
					<spring:message code="LB.Income_tax" />
				</td>
				<td>${TAX}</td>
			</tr>
			
			<tr>
				<td class="ColorCell">
					<!-- 稅後本息 -->
					<spring:message	code="LB.After-tax_principal_and_interest" />
				</td>
				<td>${PAIAFTX }</td>
			</tr>
			
			<tr>
				<td class="ColorCell">
					<!-- 健保費 -->
					<spring:message code="LB.NHI_premium" />
				</td>
				<td>${NHITAX}</td>
			</tr>
		</c:if>
		<tr>
			<td class="ColorCell">
				<!-- 交易備註 -->
				<spring:message code="LB.Transfer_note" />
			</td>
			<td class="DataCell">${CMTRMEMO}</td>
		</tr>
	</table>
	<br>
	<br>
	<div class="text-left">
		<!-- 說明： -->
		<spring:message code="LB.Description_of_page" />
		<ol class="list-decimal text-left">
			<li>
				<strong style="font-weight: 400">
					<spring:message	code="LB.F_deposit_cancel_P3_D1" />
				</strong>
			</li>
		</ol>
	</div>
</body>

</html>