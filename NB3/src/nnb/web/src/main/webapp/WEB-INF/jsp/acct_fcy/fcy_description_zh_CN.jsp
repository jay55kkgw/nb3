<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div>
	<div class="ttb-message">
		<p>客户办理网路银行外汇结购及转帐、汇出汇款及汇入汇款解款应注意事项</p>
	</div>
	<ul class="ttb-result-list terms">
		<li data-num="">
			<ul class="">
				<li>
					<strong style="font-size:15px">
						1.本网路银行
						<font color="red">
							不提供需检附核准函或交易文件之各项外汇服务
						</font>
						，如申报义务人备有核准函或欲执行须检附交易文件之外汇服务请临柜办理。
					</strong>
				</li>
				<li>
					<strong style="font-size:15px">
						2.本网路银行
						<font color="red">
							涉及新台币之外汇业务
						</font>
							仅提供
						<font color="red">
							未达等值新台币50万元以下之转帐、汇出汇款及汇入汇款解款交易
						</font>
						。如超过交易限额请临柜办理。
					</strong>
				</li>
				<li>
					<strong style="font-size:15px">
						3.<font color="red">个人涉及人民币之结购(含币转)及结售(含币转)亦应个別累计不得逾2万元人民币。</font>
					</strong>
				</li>
				<li>
					<strong style="font-size:15px">
						4.<font color="red">如单笔交易或累计已达等值新台币50万元者，则该笔交易取消。</font>
					</strong>
				</li>
				<li>
					<strong style="font-size:15px">
						5.本网路银行
						<font color="red">未涉及新台币之外汇业务</font>
						，其每笔或每日交易限额
						<font color="red">依本行网路银行服务系统作业程序有关交易限额之规定办理</font>
						。（参看本行网站重要公告内容）
					</strong>
				</li>
				<li>
					<strong style="font-size:15px">
						6.申报义务人之外汇收支或交易未办理新台币结汇者，以本行掣发之其它交易凭证视同申报书。
					</strong>
				</li>
				<li>
					<strong style="font-size:15px">
						7.申报义务人利用网际网路办理新台币结汇申报，经查获有申报不实情形者，其日后办理新台币结汇申报事宜，应临柜办理。
					</strong>
				</li>
				<li>
					<strong style="font-size:15px">
						8.本网路银行提供之汇入汇款解款交易，倘因贵行有错付或产生任何纠纷，对于汇入款项及可能衍生之利息，本人（公司）愿负返还之责任。
					</strong>
				</li>
			</ul>
		</li>
	</ul>
</div>