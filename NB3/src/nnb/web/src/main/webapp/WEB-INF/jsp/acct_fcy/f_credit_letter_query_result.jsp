<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp"%>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
</head>
<body>
	<!-- header     -->
	<header><%@ include file="../index/header.jsp"%></header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 帳戶查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Account_Inquiry" /></li>
    <!-- 通知查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0081" /></li>
		</ol>
	</nav>



	<!-- 	快速選單及主頁內容 -->
	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!--信用狀通知查詢 -->
				<h2><spring:message code="LB.W0081" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
																	<!--下載為excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
																	<!-- 下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>									
					</select>
				</div>
				<form id="formId" action="${__ctx}/FCY/ACCT/f_credit_letter_query_SessionDownload" method="post">
					<input type="hidden" id="back" name="back" value="">
					<!-- 下載用 -->
																					<!--信用狀通知查詢 -->
					<input type="hidden" name="downloadFileName"value="<spring:message code="LB.W0081" />" /> 
					<input type="hidden"name="CMQTIME" value="${credit_letter_query_result.data.CMQTIME}" />
					<input type="hidden" name="CMPERIOD"value="${credit_letter_query_result.data.CMPERIOD}" /> 
					<input type="hidden" name="CMRECNUM"value="${credit_letter_query_result.data.CMRECNUM}" /> 
					<input type="hidden" name="QKIND_TXT"value="${credit_letter_query_result.data.QKIND_TXT}" />  
					<input type="hidden" name="LCNO_JSP"value="${credit_letter_query_result.data.LCNO_JSP}" />  
					<input type="hidden" name="REFNO_JSP"value="${credit_letter_query_result.data.REFNO_JSP}" />  
					<input type="hidden" name="sBAL"value="${credit_letter_query_result.data.sBAL}" />
					<input type="hidden" name="downloadType" id="downloadType" /> 
					<input type="hidden" name="templatePath" id="templatePath" />
					<!-- EXCEL下載用 -->
					<input type="hidden" name="headerRightEnd" value="11" /> 
					<input type="hidden" name="headerBottomEnd" value="8" /> 
					<input type="hidden" name="rowStartIndex" value="9" /> 
					<input type="hidden" name="rowRightEnd" value="11" />
					
					<input type="hidden" name="footerStartIndex" value="11" />
	            	<input type="hidden" name="footerEndIndex" value="12" />
	           	 	<input type="hidden" name="footerRightEnd" value="2" />
					<!-- TXT下載用 -->
					<input type="hidden" name="txtHeaderBottomEnd" value="9" /> 
					<input type="hidden" name="txtHasRowData" value="true" /> 
					<input type="hidden" name="txtHasFooter" value="true" />
					
					<input type="hidden" name="CMRECNUM" value=" ${credit_letter_query_result.data.CMRECNUM }" readonly="readonly" />
							
					<div class="main-content-block row">
						<div class="col-12 tab-content">
	                    	<ul class="ttb-result-list">
								<li>
															<!-- 查詢時間  -->
									<h3><spring:message code="LB.Inquiry_time" /></h3>			
									<p>${credit_letter_query_result.data.CMQTIME}</p> 
								</li>
								<li>
																				<!-- 開狀/修狀日期  -->
									<h3><spring:message code="${credit_letter_query_result.data.QKIND_JSP}" /></h3>
									<p> ${credit_letter_query_result.data.CMPERIOD }</p> 
								</li>
								<li>
														<!--信用狀號碼 -->
									<h3><spring:message code="LB.L/C_no" /></h3>
									<p>${credit_letter_query_result.data.LCNO_JSP}</p>
								</li>
								<li>
														<!--通知編號 -->
									<h3><spring:message code="LB.W0086" /></h3>
									<p>${credit_letter_query_result.data.REFNO_JSP}</p> 
								</li>
								<li>
																<!-- 資料總數  -->
									<h3><spring:message code="LB.Total_records" /></h3>            <!-- 筆  -->
									<p>	${credit_letter_query_result.data.CMRECNUM } <spring:message code="LB.Rows" /> </p>
								</li>
							</ul>
							<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
								<thead>
									<tr>
										<th><spring:message code="LB.W0086" /></th>
										<th><spring:message code="LB.L/C_no" /></th>
										<th><spring:message code="LB.W0091" /></th>
										<th><spring:message code="LB.W0092" /></th>
										<th><spring:message code="LB.Currency" /></th>
										<th><spring:message code="LB.W0094" /></th>
										<th><spring:message code="LB.W0095" /></th>
										<th><spring:message code="LB.W0096" /></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="dataList" items="${credit_letter_query_result.data.REC }">
										<tr>
											<td class="text-center">${dataList.RREFNO }</td>
											<td class="text-center">${dataList.RLCNO }</td>
											<td class="text-center">${dataList.RISSBK }</td>
											<td class="text-center">${dataList.RRCVDATE }</td>
											<td class="text-center">${dataList.RLCCCY }</td>
											<td class="text-right">${dataList.RLCTXAMT }</td>
											<td class="text-right">${dataList.RLCOSBAL }</td>
											<td class="text-center">${dataList.REXPDATE }</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
							<br>
							<br>
							<div class="text-left">
												<!--信用狀金額小計: -->
								<p><spring:message code="LB.W0097" />:</p>
								<table>
								<c:forEach var="dataList" items="${credit_letter_query_result.data.fcytotmat }">
									<tr>
										<td class="text-left">${dataList.currency} &nbsp;&nbsp;</td>
										<td class="text-right">${dataList.amount}</td>
									</tr>
								</c:forEach>
								</table>
							</div>											
							<c:if test="${credit_letter_query_result.data.TOPMSG eq 'OKOV'}">
								<div>						
									<spring:message code="LB.F_Collection_Query_N559_P2_D1" />
									<input type="button" class="ttb-sm-btn btn-flat-orange"  id="Query" value="<spring:message code="LB.X0151" />" onclick="QueryNext();"/>
								</div>
							</c:if>
							<input type="button" class="ttb-button btn-flat-gray" id="CMBACK" value="<spring:message code='LB.X0420' />" />
							<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />" /> 
	
						</div>
					</div>
				</form>
			</section>
		</main>
		<!-- main-content END -->
	</div>
	<!-- 	content row END -->
	<%@ include file="../index/footer.jsp"%>
	<!-- Js function -->
	<script type="text/JavaScript">
		$(document).ready(function() {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 50);
			// 開始跑下拉選單並完成畫面
			setTimeout("init()", 400);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
			setTimeout("initDataTable()",100);
			

		});

		function init() {
			//initFootable();
			//列印
			$("#printbtn").click(
				function() {
					var params = {
						"jspTemplateName" : "credit_letter_query_result_print",
						"jspTitle" : "<spring:message code="LB.X1471" />",
						"CMQTIME" : "${credit_letter_query_result.data.CMQTIME}",
						"QKIND" : "${credit_letter_query_result.data.QKIND_JSP}",
						"CMPERIOD" : "${credit_letter_query_result.data.CMPERIOD}",
						"CMRECNUM" : "${credit_letter_query_result.data.CMRECNUM }",
						"LCNO":"${credit_letter_query_result.data.LCNO_JSP }",
						"REFNO":"${credit_letter_query_result.data.REFNO_JSP }",
					};
					openWindowWithPost(
						"${__ctx}/print",
						"height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",
						params);
				});
			//上一頁按鈕
			$("#CMBACK").click(function() {
				var action = '${__ctx}/FCY/ACCT/f_credit_letter_query';
				$("form").attr("target","");
				$("form").attr("action", action);
				initBlockUI();
				$("form").submit();
			});

		}
		//選項
		function formReset() {
			if ($('#actionBar').val() == "excel") {
				$("#downloadType").val("OLDEXCEL");
				$("#templatePath").val("/downloadTemplate/f_credit_letter.xls");
			} else if ($('#actionBar').val() == "txt") {
				$("#downloadType").val("TXT");
				$("#templatePath").val("/downloadTemplate/f_credit_letter.txt");
			}
			$("#formId").attr("target", "");
			$("#formId").submit();
			$('#actionBar').val("");
		}
	</script>
</body>
</html>
