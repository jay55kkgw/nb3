<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=big5">
</HEAD>
<body>
<center>
<br>　</center>
<H3 ALIGN=CENTER></H>
<H3 ALIGN="CENTER">**臺灣企銀債券及票券參考利率表<H3 ALIGN="CENTER"></H>
<H3 ALIGN=CENTER>日期 : ${N026data.data.UPDATEDATE}
 &nbsp;&nbsp;時間 : ${N026data.data.UPDATETIME}
</H>
<c:choose>
<c:when test="${N026data.data.RECSIZE=='0'}">
查無資料!!!
</c:when>
<c:when test="${N026data.data.RECSIZE!='0'}">
	<TABLE BORDER="2" cellspacing="0" width="500">
	<TR><TH>　</th><TH COLSPAN="2">初級市場</th><TH COLSPAN="2">票券次級市場</th><TH COLSPAN="2">債券附條件</th></tr>
	<TR><TH>天數</th><TH>BA</th><TH>CP</th><TH>買價</th><TH>賣價</th><TH>買價</th><TH>賣價</th></tr>
	<c:forEach var="dataList" items="${N026data.data.REC}" varStatus="data">
	<TR BGCOLOR="${dataList.COLOR}">
		<TD Align="left"><FONT>${dataList.TERM}</FONT>&nbsp;</td>
		<TD Align="right"><FONT>${dataList.ITR1}</FONT>&nbsp;</td>
		<TD Align="right"><FONT>${dataList.ITR2}</FONT>&nbsp;</td>
		<TD Align="right"><FONT>${dataList.ITR3}</FONT>&nbsp;</td>
		<TD Align="right"><FONT>${dataList.ITR4}</FONT>&nbsp;</td>
		<TD Align="right"><FONT>${dataList.ITR5}</FONT>&nbsp;</td>
		<TD Align="right"><FONT>${dataList.ITR6}</FONT>&nbsp;</td>
	</TR>
	</c:forEach>
	</TABLE>
</c:when>
</c:choose>
<form>
<center><table border=0>
<tr><td><input type='button' value='回上一頁' OnClick='history.back()'></td><td><input type='button' value='登出' OnClick='javascript:self.close()'></td></tr></table></center>
</form>
</BODY>
</HTML>