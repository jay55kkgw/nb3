<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=big5">
<style type="text/css">
.content12-gy {
	FONT-SIZE: 12px; COLOR: #575757; LINE-HEIGHT: 16px; FONT-FAMILY: "Arial", "新細明體", "taipei"
}
.content12-gy A:link {
	COLOR: #575757; TEXT-DECORATION: none
}
.content12-gy A:visited {
	COLOR: #575757; TEXT-DECORATION: none
}
.content12-gy A:hover {
	COLOR: #ff6600; TEXT-DECORATION: underline
}
.content12-bk {
	FONT-SIZE: 12px; COLOR: #000000; LINE-HEIGHT: 16px; FONT-FAMILY: "Arial", "新細明體", "taipei"
}
</style>
</HEAD>
<body>
<c:choose>
<c:when test="${N021data.data.RECSIZE=='0'}">
查無資料!!!
</c:when>
<c:when test="${N021data.data.RECSIZE!='0'}">
	<table width='65%' cellpadding='1' cellspacing='1' bgcolor='FFBD57' ALIGN='CENTER'><FONT SIZE='-1'>
	<tr>
		<TH bgcolor='FC9A05' class='content12-bk'><FONT SIZE='-1'>級距</font><TH bgcolor='FC9A05' class='content12-bk' COLSPAN='2'><FONT SIZE='-1'>一般利率</font><TH bgcolor='FC9A05' class='content12-bk' COLSPAN='2'><FONT SIZE='-1'>大額300萬元(不含)以上</font>
	</tr>
	<tr>
		<TH bgcolor='FC9A05' class='content12-bk'><FONT SIZE='-1'>科目.期別</font><TH bgcolor='FC9A05' class='content12-bk'><FONT SIZE='-1'>固定</font><TH bgcolor='FC9A05' class='content12-bk'><FONT SIZE='-1'>機動</font><TH bgcolor='FC9A05' class='content12-bk'><FONT SIZE='-1'>固定</font><TH bgcolor='FC9A05' class='content12-bk'><FONT SIZE='-1'>機動</font>
	</tr>
	<c:forEach var="dataList" items="${N021data.data.RECNormal}" varStatus="data">
		<TR BGCOLOR="${dataList.COLOR}">
				<!--<TR BGCOLOR=white>-->
				<TD bgcolor='white' class='content12-gy' Align='left'>${dataList.ACC}&nbsp;</td>
				<TD bgcolor='white' class='content12-gy' Align='right'>${dataList.ITR1}&nbsp;</td>
				<TD bgcolor='white' class='content12-gy' Align='right'>${dataList.ITR2}&nbsp;</td>
				<TD bgcolor='white' class='content12-gy' Align='right'>${dataList.ITR3}&nbsp;</td>
				<TD bgcolor='white' class='content12-gy' Align='right'>${dataList.ITR4}&nbsp;</td>
		</TR>
	</c:forEach>
	</TABLE>
</c:when>
</c:choose>
<c:if test="${N021data.data.isAAColor == 'true'}">
	<c:if test="${ N021data.data.today eq N021data.data.yyymmdd }">
	<P>
	<center><table border=0 width='65%'>
	<tr><TH align="left"><FONT SIZE=-1>掛牌日期：${N021data.data.yyyyDay}
	</tr>
	</table></center>
	<center><table width='65%' cellpadding='1' cellspacing='1' bgcolor='FFBD57' ALIGN='CENTER'>
		<tr>
			<TH bgcolor='FC9A05' class='content12-bk' rowspan="2"><FONT SIZE='-1'>期　別</FONT>
			<c:forEach var="dataList" items="${N021data.data.ListAA}" varStatus="data">
				<TH bgcolor='FC9A05' class='content12-bk'><FONT SIZE='-1'>大額${dataList}億元（不含）以上</FONT>
			</c:forEach>
		</tr>
		<tr>
			<c:forEach var="dataList2" items="${N021data.data.ListAA}" varStatus="data2">
			<TH bgcolor='FC9A05' class='content12-bk'><FONT SIZE='-1'>固　　定</FONT>
			</c:forEach>
		</tr>
		<c:forEach var="dataList3" items="${N021data.data.LListAB}" varStatus="data3">
		<tr>
			<c:forEach var="dataList4" items="${dataList3}" varStatus="data3">
				<td bgcolor='white' class='content12-gy' Align='center'>${dataList4}</td>
			</c:forEach>
		</c:forEach>
		</tr>
		</table></center>
	</c:if>
</c:if>


<!-- N027part -->
<c:choose>
<c:when test="${N027data.data.RECSIZE=='0'}">
查無資料!!!
</c:when>
<c:when test="${N027data.data.RECSIZE!='0'}">
<H3 ALIGN=CENTER></H3>
	<H3 ALIGN=CENTER>
		${N027data.data.JSPTITLE}
	</H3>
	<P>
	<center>
		<table border=0 width='65%'>
			<tr>
				<TH align="left"><FONT SIZE=-1>掛牌日期： ${N027data.data.UPDATEDATE}
			</tr>
		</table>
	</center>
	<c:forEach var="dataList" items="${N027data.data.REC}" varStatus="data">
		<center>
		<table width='65%' cellpadding='1' cellspacing='1' bgcolor='FFBD57'
			ALIGN='CENTER'>
			<fmt:parseNumber var = "TITLE1num" type = "number" value = "${dataList.TITLE1}" />
			<fmt:parseNumber var = "TITLE2num" type = "number" value = "${dataList.TITLE2}" />
			<fmt:parseNumber var = "TITLE3num" type = "number" value = "${dataList.TITLE3}" />
			<TR>
				<TH bgcolor='FC9A05' class='content12-bk' rowspan="2"><FONT
					SIZE='-1'>期 別</FONT></TH>
				<c:if test="${TITLE1num > 0}">
					<TH bgcolor='FC9A05' class='content12-bk'><FONT
					SIZE='-1'>大額${dataList.amt1}億元(不含)以上&nbsp;
					</FONT></TH>
				</c:if>
				<c:if test="${TITLE2num > 0}">
					<TH bgcolor='FC9A05' class='content12-bk'><FONT
					SIZE='-1'>大額${dataList.amt2}億元(不含)以上&nbsp;
					</FONT></TH>
				</c:if>
				<c:if test="${TITLE3num > 0}">
					<TH bgcolor='FC9A05' class='content12-bk'><FONT
					SIZE='-1'>大額${dataList.amt3}億元(不含)以上&nbsp;
					</FONT></TH>
				</c:if>
			</tr>
			<TR>
				<c:if test="${TITLE1num > 0}">
					<TH bgcolor='FC9A05' class='content12-bk'><FONT
					SIZE='-1'>固 定</FONT></TH>
				</c:if>
				<c:if test="${TITLE2num > 0}">
					<TH bgcolor='FC9A05' class='content12-bk'><FONT
					SIZE='-1'>固 定</FONT></TH>
				</c:if>
				<c:if test="${TITLE3num > 0}">
					<TH bgcolor='FC9A05' class='content12-bk'><FONT
					SIZE='-1'>固 定</FONT></TH>
				</c:if>
			</TR>
			<TR>
				<td bgcolor='white' class='content12-gy' Align='center'>${dataList.TERM}&nbsp;</td>
				<c:if test="${TITLE1num > 0}">
					<td bgcolor='white' class='content12-gy'
					Align='center'>${dataList.ITR1}&nbsp;</td>
				</c:if>
				<c:if test="${TITLE2num > 0}">
					<td bgcolor='white' class='content12-gy'
					Align='center'>${dataList.ITR2}&nbsp;</td>
				</c:if>
				<c:if test="${TITLE3num > 0}">
					<td bgcolor='white' class='content12-gy'
					Align='center'>${dataList.ITR3}&nbsp;</td>
				</c:if>
			</TR>
		</table>
		</center>
		<BR>
		<BR>
	</c:forEach>
</c:when>
</c:choose>
<P>
<center><table border=0> 
<tr>
	<td><P><FONT SIZE=-1>註 :指定到期日之定期存款，其利率按實際存滿月數之相當期別同性質存款牌告利率計息。 如無該期別之牌告利率，則依較低期別牌告利率計息。</td>
</tr>
</table></center>

</BODY>
</HTML>