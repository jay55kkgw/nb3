<!-- <html> -->

<!-- <head> -->
<!-- <meta http-equiv=Content-Type content="text/html; charset=utf-8"> -->
<!-- <meta name=Generator content="Microsoft Word 11 (filtered)"> -->
<!-- </head> -->
<!-- <body> -->
<%-- 	<object data="${__ctx}/public/N201_2.pdf" type="application/pdf"  style='width:100%;height: 100%;' > --%>
<!-- 		<center> -->
<!-- 			<p> -->
<!-- 				It appears you don't have Adobe Reader or PDF support in this Web browser. -->
<%-- 				<a href="${__ctx}/public/N201_2.pdf">Click here to download the PDF</a> --%>
<!-- 			</p> -->
<!-- 		</center> -->
<%-- 		<embed scr="${__ctx}/public/N201_2.pdf" type="application/pdf" /> --%>
<!-- 	</object> -->
<!-- </body> -->
<!-- </html> -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<body>
	<div style="margin: 2rem;">
		<p class="terms-text-bold terms-text-center">
			臺灣中小企業銀行股份有限公司遵循FATCA法案蒐集、處理及利用個人資料告知事項
		</p>
		<p class="terms-text-bold terms-text-center">
			Taiwan Business Bank Co., Ltd.
		</p>
		<p class="terms-text-bold terms-text-center">
			The Notice for the Collection, Processing and Use of Personal Information for FATCA Compliance
		</p>
		<p>
			緣臺灣中小企業銀行股份有限公司（下簡稱「本公司」）因參與遵循美國海外帳戶稅收遵從法案（Foreign Account Tax Compliance Act，下簡稱「FATCA法案」），及駐美國台北經濟文化代表處與美國在台協會合作促進外國帳戶稅收遵從法執行協定（下稱「IGA協議」），而負辨識美國帳戶之義務，現因 台端於本公司開立帳戶及進行交易，為符合個人資料保護法下個人資料之合理使用，本公司茲請求 台端協力遵循FATCA法案及IGA協議之相關規定，特告知下列事項：
		</p>
		<p>
			In compliance with the U.S. Foreign Account Tax Compliance Act (hereinafter referred to as “FATCA”) and Agreement between the American institute in Taiwan and Taipei Economic And Cultural Representative Office In The United States (hereinafter referred to as the “IGA”), Taiwan Business Bank Co., Ltd. (hereinafter referred to as the “TBB”) has the obligation to identify U.S. accounts. In order to comply with the proper use of personal information in accordance with the Personal Information Protection Act for accounts you establish and transactions proceed with TBB, TBB hereby requests your cooperation with the compliance of FATCA and the relevant provisions under the Agreement, with notice as follows:
		</p>
		<br/>
		<p>
			一、個人資料蒐集、處理及利用之目的及類別 Purpose and Type of Collection, Processing and Use of Personal Information
			為辨識本公司內所有帳戶持有者之身分，並於必要時申報具有美國帳戶之持有者資訊予美國國稅局及臺灣權責主管機關，經 台端提供之相關個人資料及留存於本公司之一切交易資訊，包括但不限於姓名、出生地及出生日期、國籍、戶籍地址、住址及工作地址、電話號碼、美國稅籍編號、帳戶帳號及帳戶餘額、帳戶總收益金額與交易明細等，將因本公司遵循FATCA法案及IGA協議之需要，由本公司蒐集、處理及利用。
		</p>
		<p>
			In order to identify the account holders of TBB and to report accounts held by U.S. persons to the Internal Revenue Service (hereinafter referred to as “IRS”) and the competent authority in Taiwan R.O.C., all personal information provided by you and all transaction information kept by TBB, including but not limited to name, place of birth, date of birth, nationality, domicile address, residence address and work location, telephone number, U.S. tax identifying number (TIN), account number and account balance, the gross proceeds and statement of the account shall be collected, processed and used by TBB for the purpose of FATCA compliance and as required by the IGA.
		</p>
		<br>
		<p>
			二、個人資料利用之期間及方式 The Period and Method of Using Personal information
			為遵循FATCA法案及IGA協議之必要年限內，本公司所蒐集之 台端個人資料將由本公司為保存及利用，並於特定目的之範圍內，以書面、電子文件、電磁紀錄、簡訊、電話、傳真、電子或人工檢索等方式為處理、利用與國際傳輸。\
		</p>
		<br>

		<p>
			In compliance with the period required by FATCA and the IGA, the personal information collected by TBB will be kept and used by TBB and processed, used and transmitted internationally in writing, via email, electromagnetic record, text message, telephone, fax, electronic or manual search within the scope of the said specified purpose.
		</p>
		<br>

		<p>
			三、個人資料利用之地區 Geographical Limitation for Use of Personal Information
		</p>
		<br>
		<p>
			為履行FATCA法案及IGA協議下之相關義務，　台端個人資料將於臺灣及美國地區受利用。
		</p>
		<br>
		<p>
			In order to fulfill the obligations under FATCA and the IGA, your personal information will be used in both Taiwan R.O.C. and United States.
		</p>
		<br>

		<p>
			四、個人資料利用之對象 Parties Using the Personal Information
		</p>
		<br>
		<p>為履行FATCA法案及IGA協議下之相關義務， 台端個人資料將由本公司、臺灣權責主管機關及美國國稅局所利用。
		</p>
		<br>
		<p>
			In order to fulfill the obligations under FATCA and the IGA, your personal information will be used by TBB, the competent authority in Taiwan R.O.C. and the IRS.
		</p>
		<br>

		<p>五、個人資料之權利行使及其方式 Exercise of the Rights Regarding Personal Information
		</p>
		<br>
		<p>
			台端就本公司所蒐集、處理及利用之個人資料，得隨時向本公司請求查詢、閱覽、製給複製本、補充或更正、停止蒐集處理及利用或刪除。　台端如欲行使前述權利，有關如何行使之方式，得向本公司各分行臨櫃查詢。
		</p>
		<br>
		<p>
			With regard to the personal information collected, processed and used by TBB, you may request to search, review, make duplications, supplement or correct the personal information or to discontinue the collection, processing, and use of the personal information, or request to delete the personal information. If you would use abovementioned rights, please dial or find a counter-service in every branch for understanding how to use your rights.
		</p>

		<br>
		<p>
			六、不提供對其權益之影響 The Effect of Refusal to Provide Personal Information
		</p>
		<br>
		<p>
			台端若拒絕提供本公司為遵循FATCA法案及IGA協議所需之個人資料、或嗣後撤回、撤銷同意，本公司仍可能須將關於　台端之帳戶資訊申報予美國國稅局及臺灣權責主管機關。
		</p>
		<br>
		<p>
			台端已充分詳讀前揭告知事項，瞭解此一告知事項符合個人資料保護法及相關法規之要求。
		</p>
		<br>
		<p>
			You have read carefully and fully understand all that is stated above and understand that t In the event that you refuse to provide the personal information as required for the compliance of FATCA and the IGA, or withdraw or revoke your consent thereof, TBB may still report your account information to the competent authority in Taiwan R.O.C. and the IRS.
		</p>
		<br>
		<p>
			his notice is in accordance with the Personal Information Protection Act and the relevant laws and regulations.
		</p>
	</div>
</body>
</html>