<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript">
	$(document).ready(function() {
		//列印
		$("#printbtn").click(function() {
			var params = {
				"jspTemplateName" : "f_deposit_transfer_result_print",
				"jspTitle" : '<spring:message code="LB.Open_Foreign_Currency_Time_Deposit"/>',
				"MsgName" : '${f_deposit_transfer_result.data.MsgName}',
				"FGTXDATE" : '${f_deposit_transfer_result.data.FGTXDATE}',
				"TRDATE" : '${f_deposit_transfer_result.data.TRDATE}',
				"CMQTIME" : '${f_deposit_transfer_result.data.CMQTIME}',
				"FYTSFAN" : '${f_deposit_transfer_result.data.FYTSFAN}',
				"TSFACN" : '${f_deposit_transfer_result.data.TSFACN}',
				"FDPNUM" : '${f_deposit_transfer_result.data.FDPNUM}',
				"OUTCRY_TEXT" : '${f_deposit_transfer_result.data.OUTCRY_TEXT}',
				"AMOUNT" : '${f_deposit_transfer_result.data.AMOUNT}',
				"TYPCOD_TEXT" : '${f_deposit_transfer_result.data.TYPCOD_TEXT}',
				"SDT" : '${f_deposit_transfer_result.data.SDT}',
				"DUEDAT" : '${f_deposit_transfer_result.data.DUEDAT}',
				"INTMTHNAME" : '${f_deposit_transfer_result.data.INTMTHNAME}',
				"ITR" : '${f_deposit_transfer_result.data.ITR}',
				"AUTXFTMNAME" : '${f_deposit_transfer_result.data.AUTXFTMNAME}',
				"CODENAME" : '${f_deposit_transfer_result.data.CODENAME}',
				"CMTRMEMO" : '${f_deposit_transfer_result.data.CMTRMEMO}',
				"TOTBAL" : '${f_deposit_transfer_result.data.TOTBAL}',
				"AVLBAL" : '${f_deposit_transfer_result.data.AVLBAL}',
			};
			openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
		});
	});
</script>
</head>
<body>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 定存服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Current_Deposit_To_Time_Deposit" /></li>
    <!-- 轉入外幣綜存定存     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Open_Foreign_Currency_Time_Deposit" /></li>
		</ol>
	</nav>



	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
			<h2>
				<!--轉入外匯綜存定存 -->
				<spring:message code="LB.Open_Foreign_Currency_Time_Deposit" />
			</h2>
			<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
			<c:set var="BaseResultData" value="${f_deposit_transfer_result.data}"></c:set>
			<div class="main-content-block row">
                <div class="col-12 tab-content">
                    <div class="ttb-message">
							${BaseResultData.MsgName}
					</div>
					<div class="ttb-input-block">
					
					<c:if test="${BaseResultData.FGTXDATE == '0' && showDialog == 'true'}">
				 		<%@ include file="../index/txncssslog.jsp"%>
					</c:if>
						<c:if test="${BaseResultData.FGTXDATE == '0'}">
							<!--交易時間 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.Trading_time"/>
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
	                            			${BaseResultData.CMQTIME}
	                            		</p>
									</div>
								</span>
							</div>
							<!--轉出帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.Payers_account_no"/>
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
	                            			${BaseResultData.FYTSFAN}
	                            		</p>
									</div>
								</span>
							</div>
							<!--轉入帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.Payees_account_no" />
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
	                            			${BaseResultData.TSFACN}
	                            		</p>
									</div>
								</span>
							</div>
							<!--存單號碼 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.Certificate_no" />
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
	                            			${BaseResultData.FDPNUM}
	                            		</p>
									</div>
								</span>
							</div>
							<!--轉帳金額 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.Amount" />
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span class="high-light">
	                            			${BaseResultData.OUTCRY_TEXT}<fmt:formatNumber type="number" value="${BaseResultData.AMOUNT}"/>.${BaseResultData.AMOUNT_DIG}
	                            		</span>
									</div>
								</span>
							</div>
							<!--存款期別 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.Deposit_period" />
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
	                            			${BaseResultData.TYPCOD_TEXT}
	                            		</p>
									</div>
								</span>
							</div>
							<!--起存日 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.Start_date" />
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
	                            			${BaseResultData.SDT}
	                            		</p>
									</div>
								</span>
							</div>
							<!--到期日 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.Maturity_date" />
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
	                            			${BaseResultData.DUEDAT}
	                            		</p>
									</div>
								</span>
							</div>
							<!--計息方式 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.Interest_calculation" />
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
	                            			${BaseResultData.INTMTHNAME}
	                            		</p>
									</div>
								</span>
							</div>
							<!--利率 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.Interest_rate" />
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
	                            			${BaseResultData.ITR}%
	                            		</p>
									</div>
								</span>
							</div>
							<!--轉存方式 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.Rollover_method" />
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
	                            			${BaseResultData.AUTXFTMNAME}
	                            		</p>
									</div>
								</span>
							</div>
							<!--到期轉期 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.Roll-over_when_expiration" />
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
	                            			${BaseResultData.CODENAME}
	                            		</p>
									</div>
								</span>
							</div>
							<!-- 交易備註 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.Transfer_note" />
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
	                            			${BaseResultData.CMTRMEMO}
	                            		</p>
									</div>
								</span>
							</div>
							<!--轉帳帳號帳戶餘額 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.Payers_account_balance" />
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
	                            			${BaseResultData.TOTBAL}
	                            		</p>
									</div>
								</span>
							</div>
							<!--轉帳帳號可用餘額 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.Payers_available_balance" />
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
	                            			${BaseResultData.AVLBAL}
	                            		</p>
									</div>
								</span>
							</div>
						</c:if>
						<c:if test="${BaseResultData.FGTXDATE != '0'}">
							<!-- 資料時間 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code= "LB.Data_time" />
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
	                            			${BaseResultData.CMQTIME}
	                            		</p>
									</div>
								</span>
							</div>
							<!--轉帳日期 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.Transfer_date"/>
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
	                            			${BaseResultData.TRDATE}
	                            		</p>
									</div>
								</span>
							</div>
							<!--轉出帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.Payers_account_no"/>
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
	                            			${BaseResultData.FYTSFAN}
	                            		</p>
									</div>
								</span>
							</div>
							<!--轉入帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.Payees_account_no"/>
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
	                            			${BaseResultData.TSFACN}
	                            		</p>
									</div>
								</span>
							</div>
							<!--轉帳金額 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.Amount"/>
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
											${BaseResultData.OUTCRY_TEXT}<fmt:formatNumber type="number" value="${BaseResultData.AMOUNT}"/>.${BaseResultData.AMOUNT_DIG}
	                            		</p>
									</div>
								</span>
							</div>
							<!--存款期別 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.Deposit_period"/>
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
	                            			${BaseResultData.TYPCOD_TEXT}
	                            		</p>
									</div>
								</span>
							</div>
							<!--計息方式 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.Interest_calculation"/>
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
	                            			${BaseResultData.INTMTHNAME}
	                            		</p>
									</div>
								</span>
							</div>
							<!--轉存方式 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.Rollover_method"/>
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
	                            			${BaseResultData.AUTXFTMNAME}
	                            		</p>
									</div>
								</span>
							</div>
						<!--到期轉期 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.Roll-over_when_expiration"/>
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
	                            			${BaseResultData.CODENAME}
	                            		</p>
									</div>
								</span>
							</div>
							<!--交易備註 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<spring:message code="LB.Transfer_note"/>
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
	                            			${BaseResultData.CMTRMEMO}
	                            		</p>
									</div>
								</span>
							</div>
						</c:if>
					</div>
					<!-- 列印  -->
					<spring:message code="LB.Print" var="printbtn"></spring:message>
					<input type="button" id="printbtn" value="${printbtn}" class="ttb-button btn-flat-orange"/>
				</div>
			</div>
			<div class="text-left">
				<ol class="list-decimal description-list">
					<p><spring:message code="LB.Description_of_page" /> </p>
					<c:if test="${BaseResultData.FGTXDATE == '0'}">
						<li><spring:message code= "LB.F_deposit_transfer_P3_D1" /></li>
					</c:if>
					<c:if test="${BaseResultData.FGTXDATE != '0'}">
						<li><spring:message code="LB.F_deposit_transfer_PP3_D1"/></li>
              						<li><spring:message code="LB.F_deposit_transfer_PP3_D2"/></li>
									<li><spring:message code="LB.F_deposit_transfer_PP3_D3"/></li>
					</c:if>
				</ol>
			</div>
		</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>