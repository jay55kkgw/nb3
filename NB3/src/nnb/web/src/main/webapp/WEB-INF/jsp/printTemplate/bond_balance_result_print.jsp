<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js_u2.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <!-- <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script> -->
  <title>${jspTitle}</title>
  <script type="text/javascript">
    $(document).ready(function () {
      window.print();
    });
  </script>
</head>
<body class="watermark" style="-webkit-print-color-adjust: exact">
	<br /><br />
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif" /></div>
	<br /><br />
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<br />
	
	<div> 
		<!-- 查詢時間 -->
		<p>
			<spring:message code="LB.Inquiry_time" /> : ${CMQTIME}
		</p>
		<!-- 查詢期間 -->
		<p>
			<spring:message code= "LB.W1466" /> : <spring:message code= "LB.D0088_2" />
		</p>
		<!-- 資料總數 : -->
		<p>
			<spring:message code="LB.Total_records" /> : ${CMRECNUM}
			<!--筆 -->
			<spring:message code="LB.Rows" />
		</p>
		
		<!-- 資料Row -->
		<c:forEach var="tableList" items="${print_datalistmap_data}">
			<table class="print">
			
				<thead>
					<tr>
						<!-- 帳號 -->
						<th><spring:message code="LB.Account"/></th>
						<th class="text-left" colspan="5">${tableList.ACN}</th>
					</tr>
					<tr>
						<!-- 債券代號 -->
						<th><spring:message code= "LB.W0036" /> </th>
						<!-- 餘額-->
						<th><spring:message code= "LB.Account_balance_2" /></th>
						<!-- 可動支餘額 -->
						<th><spring:message code= "LB.W0038" /></th>
						<!-- 限制性轉出餘額 -->
						<th><spring:message code= "LB.W0039" /></th>
						<!-- 限制性轉入餘額 -->
						<th><spring:message code= "LB.W0040" /></th>
						<!-- 附條件簽發餘額-->
						<th><spring:message code= "LB.W0041" /></th>
					</tr>
				</thead>
				<tbody>
					<c:if test="${tableList.Table.size() > 0}">
						<c:forEach var="dataList" items="${tableList.Table}">
							<tr>
								<td class="text-center">${dataList.BONCOD}</td>
								<td class="text-right">${dataList.DPIBAL}</td>
								<td class="text-right">${dataList.TSFBAL_N870}</td>
								<td class="text-right">${dataList.LMTSFO}</td>
								<td class="text-right">${dataList.LMTSFI}</td>
								<td class="text-right">${dataList.RPBAL}</td>
							</tr>
						</c:forEach>
					</c:if>
					<c:if test="${tableList.Table.size() <= 0}">
						<tr>
							<td>${tableList.msgCode}</td>
							<td>${tableList.msgName}</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</c:if>
				</tbody>
			</table>
			<br >
		</c:forEach>
	</div>
	
	<br>
	<br>
</body>
</html>