<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
	<script type="text/javascript">
		$(document).ready(function () {
			//列印
			$("#printbtn").click(function () {

				var params = {
					"jspTemplateName": "order_renewal_result_print",
					"jspTitle": '<spring:message code="LB.Renewal_At_Maturity_Of_NTD_Time_Deposit"/>',
					"MsgName": '${order_renewal_result.data.MsgName}',
					"CMTXTIME": '${order_renewal_result.data.CMTXTIME}',
					"FDPACN": '${order_renewal_result.data.FDPACN}',
					"FDPNUM": '${order_renewal_result.data.FDPNUM}',
					"AMOUNT": '${order_renewal_result.data.AMOUNT}',
					"FDPACC": '${order_renewal_result.data.FDPACC}',
					"TERM": '${order_renewal_result.data.TERM}',
					"SDT": '${order_renewal_result.data.SDT}',
					"DUEDAT": '${order_renewal_result.data.DUEDAT}',
					"INTMTH": '${order_renewal_result.data.INTMTH}',
					"ITR": '${order_renewal_result.data.ITR}',
					"FGSVNAME": '${order_renewal_result.data.FGSVNAME}',
					"FDPAMT": '${order_renewal_result.data.FDPAMT}',
					"PYDINT": '${order_renewal_result.data.PYDINT}',
					"GRSTAX": '${order_renewal_result.data.GRSTAX}',
					"NHITAX": '${order_renewal_result.data.NHITAX}',
					"TSFACN": '${order_renewal_result.data.TSFACN}'
				};
				openWindowWithPost("${__ctx}/print",
					"height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",
					params);
			});
		});
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 	<!--   IDGATE -->
	<%@ include file="../idgate_tran/idgateAlert.jsp" %>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 定存服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Current_Deposit_To_Time_Deposit" /></li>
    <!-- 定存單到期續存     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Renewal_At_Maturity_Of_NTD_Time_Deposit" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
	</div>
	<!-- content row END -->
	<!-- 		主頁內容  -->
	<main class="col-12">
		<section id="main-content" class="container">
			<h2>
				<!-- 臺幣定存單到期續存 -->
				<spring:message code="LB.Renewal_At_Maturity_Of_NTD_Time_Deposit" />
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="" onSubmit="return processQuery()">
				<c:set var="BaseResultData" value="${order_renewal_result.data}"></c:set>
				<!-- 交易步驟 -->
				<div id="step-bar">
					<ul>
						<!-- 輸入資料 -->
						<li class="finished">
							<spring:message code="LB.Enter_data" />
						</li>
						<!-- 確認資料 -->
						<li class="finished">
							<spring:message code="LB.Confirm_data" />
						</li>
						<!-- 交易完成 -->
						<li class="active">
							<spring:message code="LB.Transaction_complete" />
						</li>
					</ul>
				</div>
				<!-- 表單顯示區 -->
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
							<div class="ttb-message">
							<!-- 續存成功 -->
								<span><spring:message code= "LB.Renewal_successful" /></span>
							</div>
							<!-- 交易時間 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Trading_time" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>${BaseResultData.CMTXTIME}</span>
									</div>
								</span>
							</div>
							<!-- 存單帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Account_no" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>${BaseResultData.FDPACN}</span>
									</div>
								</span>
							</div>


							<!-- 存單號碼 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Certificate_no" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											${BaseResultData.FDPNUM}
										</span>
									</div>
								</span>
							</div>
							<!-- 存單金額 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Certificate_amount" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											<!-- 								新台幣 : -->
											<spring:message code="LB.NTD" />
											<fmt:formatNumber type="number" minFractionDigits="2"
												value="${BaseResultData.AMOUNT }" />
											<!--元 -->
											<spring:message code="LB.Dollar" />
										</span>
									</div>
								</span>
							</div>
							<!-- 存單種類 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Deposit_type" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											${BaseResultData.FDPACC}
										</span>
									</div>
								</span>
							</div>
							<!--存單期別 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Fixed_duration" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											${BaseResultData.TERM}
										</span>
									</div>
								</span>
							</div>
							<!-- 起存日 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Start_date" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											${BaseResultData.SDT}
										</span>
									</div>
								</span>
							</div>
							<!-- 到期日 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Maturity_date" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											${BaseResultData.DUEDAT}
										</span>
									</div>
								</span>
							</div>
							<!-- 計息方式 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Interest_calculation" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											${BaseResultData.INTMTH}
										</span>
									</div>
								</span>
							</div>
							<!-- 利率 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Interest_rate" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											${BaseResultData.ITR}%
										</span>
									</div>
								</span>
							</div>
							<!-- 續存方式 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Renew_method" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											${BaseResultData.FGSVNAME}
											<!-- 帳號 -->
											${BaseResultData.TSFACN}
										</span>
									</div>
								</span>
							</div>
							<!-- 原存單金額 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Original_amount" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											<!-- 新台幣 : -->
											<spring:message code="LB.NTD" />
											<fmt:formatNumber type="number" minFractionDigits="2"
												value="${BaseResultData.FDPAMT }" />
											<!--元 -->
											<spring:message code="LB.Dollar" />
										</span>
									</div>
								</span>
							</div>
							<!-- 原存單利息 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Original_interest" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											<!-- 新台幣 : -->
											<spring:message code="LB.NTD" />
											<fmt:formatNumber type="number" minFractionDigits="2"
												value="${BaseResultData.PYDINT }" />
											<!--元 -->
											<spring:message code="LB.Dollar" />
										</span>
									</div>
								</span>
							</div>
							<!-- 代扣利息所得稅 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.Withholding_interest_income_tax" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span>
											<!-- 新台幣 : -->
											<spring:message code="LB.NTD" />
											<fmt:formatNumber type="number" minFractionDigits="2"
												value="${BaseResultData.GRSTAX }" />
											<!--元 -->
											<spring:message code="LB.Dollar" />
										</span>
									</div>
								</span>
							</div>
							<!--健保費 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.NHI_premium" />
										</h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<!-- 新台幣 : -->
										<spring:message code="LB.NTD" />
										<c:choose>
											<c:when test="${empty BaseResultData.NHITAX}">
												<fmt:formatNumber type="number" minFractionDigits="2" value="0" />
											</c:when>
											<c:otherwise>
												<fmt:formatNumber type="number" minFractionDigits="2"
													value="${BaseResultData.NHITAX }" />
											</c:otherwise>
										</c:choose>
										<!--元 -->
										<spring:message code="LB.Dollar" />
									</div>
								</span>
							</div>
						</div>
						<!-- button -->
							<!-- 列印  -->
							<spring:message code="LB.Print" var="printbtn"></spring:message>
							<input type="button" id="printbtn" value="${printbtn}" class="ttb-button btn-flat-orange" />
						<!--button 區域 -->
					</div>
				</div>
				<div class="text-left">
					<!-- 					說明： -->
					<!-- 					<ol class="list-decimal text-left"> -->
					<!-- 						<li>晶片金融卡:為保護您的交易安全，結束交易或離開電腦時，請務必將晶片金融卡抽離讀卡機並登出系統。</li> -->
					<!-- 						<li>電子簽章:為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章(載具i-key)拔除並登出系統。</li> -->
					<!-- 					</ol> -->
				</div>
			</form>
		</section>
		<!-- 		main-content END -->
	</main>
	<!-- 	content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>