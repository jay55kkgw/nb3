<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<!-- 海外債券     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2391" /></li>
   			 <!-- 海外債券交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2515" /></li>
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X2567" /></li>
		</ol>
	</nav>


	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
			
				 <div class="main-content-block row">
                 	<div class="col-12">
                    	<div class="ttb-message">  
                    		<span>  	
<!--                     		簽署成功 -->
                        	<b><font color="red"><spring:message code="LB.X2537" /></font></b>
                        	</span>
                        </div>
	                    <div class="ttb-input-block">
						<!--交易時間區塊 -->
							<div class="ttb-input-item row">
								<!--交易時間  -->
								<span class="input-title">
									<label>
										<spring:message code="LB.Trading_time" /><!-- 交易時間 -->
									</label>
								</span>
								<span class="input-block">
									${bond_sign_result.data.CMQTIME}
								</span>
							</div>
						</div>
                 	</div>
                 </div>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>