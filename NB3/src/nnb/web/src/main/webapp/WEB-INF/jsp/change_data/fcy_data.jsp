<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp"%>
  	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">    
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>	
	
<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 50);
		// 開始跑下拉選單並完成畫面
		setTimeout("init()", 400);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);		
		// 將.table變更為footable
		initFootable();
	});
	
	function init(){		
		
		$("#CMSUBMIT").click(function(e){
			
			$("#formId").validationEngine({
				binded : false,
				promptPosition : "inline"
			});
			
			e = e || window.event;
			
			if(!$('#formId').validationEngine('validate')){
	        	e.preventDefault();
 			}
			else{
 				$("#formId").validationEngine('detach');
 				$("#formId").attr("action","${__ctx}/CHANGE/DATA/fcy_data_query");
//  				$("#formId").submit(); 
 				processQuery(); 
 			}			
  		});	
		
		//上一頁按鈕
		$("#CMBACK").click(function() {
			var action = '${__ctx}/INDEX/index';
			$("form").attr("action", action);
			initBlockUI();
			$("form").submit();
		});

	}
	
	function processQuery(){
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
		console.log("fgtxway: " + fgtxway);
	
		switch (fgtxway) {
		// IKEY
		case '1':
			useIKey();
			break;
			// 晶片金融卡
		case '2':
			// 讀卡機...
			listReaders();
	    	break;
		default:
			//alert("<spring:message code="LB.Alert001" />");
			errorBlock(
					null, 
					null,
					["<spring:message code='LB.Alert001' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
				);
			unBlockUI(initBlockId);
		}	
	}

	// 交易機制元件--複寫後蓋前，不用拔插卡
	// 找出有插卡的讀卡機結束(有找到才會到此FUNCTION)
	function findOKReaderFinish(okReaderName){
		// ASSIGN到全域變數
		OKReaderName = okReaderName;
		
		// 此功能不拔插卡直接輸入密碼故註解
//	 	removeThenInsertCard();

		// 取得卡片銀行代碼
		getCardBankID();
	}
	// 交易機制元件--複寫後蓋前，顯示客製訊息
	// 驗證卡片密碼結束
	// 驗證卡片密碼結束
	function verifyPinFinish(result){
		// 成功
		if(result == "true"){
			// 繼續做
			CheckIdProcess();
		}
	}
	
	CheckIdProcess = function(){
		showTempMessage(500,"<spring:message code="LB.X1250" />","<br><p><spring:message code="LB.X1251" /></p><br><p><spring:message code="LB.X1252" /></p><br><br>","MaskArea",true);
		// 取得卡片主帳號
		getMainAccount();
	}
	
	//複寫取得卡片主帳號結束拔插卡動作取消
	function getMainAccountFinish(result){
		if(window.console){console.log("getMainAccountFinish...");}
		//成功
		if(result != "false"){
			var formId = document.getElementById("formId");
			formId.ACNNO.value = result;
			//卡片壓碼結束
			generateTACFinish(result);
		}
		//失敗
		else{
			FinalSendout("MaskArea",false);
		}
	}

</script>
</head>
<body>
	<!-- 	晶片金融卡 -->
	<%@ include file="../component/trading_component.jsp"%>

	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 通訊資料變更     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0359" /></li>
    <!-- 外匯進口/出口/匯兌通訊地址/電話變更     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0385" /></li>
		</ol>
	</nav>


	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code="LB.D0385" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post">
				<input type="hidden" id="TOKEN" name="TOKEN" value="${sessionScope.transfer_confirm_token}" /><!-- 防止重複交易 -->
				<input type="hidden" id="TXID" name="TXID" value="N570_1" /><!-- 先打查詢電文 -->
			    <!-- 			晶片金融卡 -->
				<input type="hidden" id="jsondc" name="jsondc" value='${fcy_data.data.jsondc}'>
				<input type="hidden" id="ISSUER" name="ISSUER" value="">
				<input type="hidden" id="ACNNO" name="ACNNO" value="">
				<input type="hidden" id="TRMID" name="TRMID" value="">
				<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
				<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
				<input type="hidden" id="TAC" name="TAC" value="">
				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
							    
				<div class="main-content-block row">
					<div class="col-12">
						<div class="ttb-input-block">
							<!-- 交易機制 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.Transaction_security_mechanism" />
									</label>
								</span>
								<span class="input-block">
									<!-- 使用者是否可以使用IKEY -->
									<c:if test="${sessionScope.isikeyuser}">
										<!--電子簽章(請載入載具i-key) -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Electronic_signature" />
												<input type="radio" name="FGTXWAY" id="CMIKEY" value="1" />
												<span class="ttb-radio"></span>
											</label>
										</div>
									</c:if>
									<!-- 晶片金融卡 -->
									<div class="ttb-input">
										<label class="radio-block">
											<spring:message code="LB.Financial_debit_card" />
											<input type="radio" name="FGTXWAY" id="CMCARD" value="2" checked="checked"/>
											<span class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>
						</div>
						<!-- 重新輸入 -->
						<input type="button" name="CMBACK" id="CMBACK" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page"/>"/>
						<!-- 確定 -->
						<input type="button" name="CMSUBMIT" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm"/>"/>
					</div>
				</div>
			</form>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>