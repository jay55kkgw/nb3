<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

</head>
<body>

	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- Email設定     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Email_Setting" /></li>
		</ol>
	</nav>

	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">

		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
		
		

		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2>
				<spring:message code="LB.Email_Setting" /><!-- Email設定 -->
			</h2> 
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" action="${__ctx}/PERSONAL/SERVING/mail_setting_choose" method="post">
				
				<div class="main-content-block row">
					<div class="col-12 tab-content" id="nav-tabContent">
						<div class="ttb-input-block">
						<div class="ttb-input-item row">
							<span class="input-title"> <label><h4><spring:message code="LB.Option_item" /></h4></label><!-- 選項 -->
							</span>
							 <span class="input-block">
								<!-- 我的Email --> 
								<div class="ttb-input">
									<!-- 我的Email -->
									<label class="radio-block">
										<spring:message code="LB.My_Email" />
										<input type="radio" name="type" value="DPSETUPE" checked="checked" />
										<span class="ttb-radio"></span>
									</label> 
								</div>
							</span>
						</div>
						<div class="ttb-input-item row">
							<span class="input-title"> <label></label></span>
							<span class="input-block">
								<div class="ttb-input">
									<!-- 我的通訊錄 -->
									<label class="radio-block">
										<spring:message code="LB.My_Address_Book" />
										<input type="radio" name="type" value="DPSETUPA" />
										<span class="ttb-radio"></span>
									</label> 
								</div>
							</span>
						</div>
					</div>
					<input type="submit" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" onclick="initBlockUI();">
					<!-- 確定 -->
				</div>
			</form>
		</section>
		</main>
	</div>	
	<%@ include file="../index/footer.jsp"%>
</body>
</html>