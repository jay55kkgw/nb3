<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<link rel="stylesheet" type="text/css"
	Href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript"
	Src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript"
	Src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript"
	Src="${__ctx}/js/jquery.datetimepicker.js"></script>
<script type="text/JavaScript">
	$(document).ready(function() {

		$('#applyTable').DataTable({
			"bPaginate" : false,
			"bLengthChange": false,
			"scrollX": true,
			"sScrollX": "99%",
			"scrollY": "80vh",
	        "bFilter": false,
	        "bDestroy": true,
	        "bSort": false,
	        "info": false,
	        "scrollCollapse": true,
		});

		$(window).bind('resize', function() {
			$('#applyTable').DataTable({
				"bPaginate" : false,
				"bLengthChange": false,
				"scrollX": true,
				"sScrollX": "99%",
				"scrollY": "80vh",
		        "bFilter": false,
		        "bDestroy": true,
		        "bSort": false,
		        "info": false,
		        "scrollCollapse": true,
			});
		});

	});
</script>
</head>
<body>
	<!-- Function list and login information -->
	<div class="content row">
		<!-- Quick Menu and Home Content -->
		<main class="col-12"> <!-- Home Content -->
		<section id="main-content" class="container">
			<!-- Function Name -->
			<h2>Online application</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<!-- Functional content -->
			<div class="main-content-block row">
				<div class="col-12">
					<div class="CN19-1-header">
						<div class="logo">
							<img src="${__ctx}/img/logo-1.svg">
						</div>
					</div>
				</div>
				<!-- How to apply for a page -->
				<div class="col-12 tab-content" id="CN19-how-apply">
					<div id="MESSAGE" style="margin: auto">
						<font color="red" size="5"> <B>Application Method</B><br>
						</font>
					</div>
					<div class="ttb-input-block">
						<div class="CN19-caption1">One, online banking application
							methods and service functions</div>
						<div class="CN19-caption2">
							<span style="FONT-FAMILY: Wingdings;">u</span> <span>How
								to apply</span>
						</div>
						<div class="CN19-caption3">
							<ul>
								<li>Online application: limited to individual households</li>
							</ul>
							<div>
								Bank's chip financial card depositors: <br>  If you have
								not applied for the deposit, you can apply for the <font
									color="#FF6600">inquiry</font> service online. Please use <b>Bank
									Financial Card + Chip Reader</b> to apply online directly. Lu Bank,
								if the chip financial card already has non-contracted transfer
								function, you can apply for the "financial card online
								application / cancel transaction service function" online,
								execute NT non-contract transfer transaction, payment tax
								transaction, online change of communication address / phone And
								user name / check-in password / transaction password line unlock
								and other functions. <br> <br> Credit card customers
								of the Bank (excluding business cards, purchasing cards,
								attached cards and VISA financial cards):<br>  Please apply
								online banking directly online, just enter your personal
								verification information, you can immediately use online banking
								credit card related services. <br>  <br>
							</div>
							<ul>
								<li>Application for cabinet:</li>
							</ul>
							<div>
								Legal depositor:<br>  The person in charge is required to
								bring <b>company registration certificate, identity card,
									deposit seal and deposit plaque</b> to the bank to apply for online
								banking. <br> <br> Personal depositors:<br>
								 Please bring your <b>identity card, deposit seal and
									deposit plaque</b> to the bank to apply for online banking, the
								bank's collection and payment households (ie, depositors), or
								apply to each business unit. <br>  <br>
							</div>
						</div>
						<div class="CN19-caption2">
							<span style="FONT-FAMILY: Wingdings;">u</span> <span>Service
								Features</span>
						</div>
						<div>
							<table id="applyTable"
								class="stripe table-striped ttb-table dtable"
								data-toggle-column="first">
								<thead>
									<tr>
										<!-- Function comparison -->
										<th>Feature comparison</th>
										<!-- Application for the cabinet-->
										<th>Application for the cabinet</th>
										<!-- Wafer Financial Card Online Application -->
										<th>Chip Financial Card Online Application</th>
										<!-- Wafer financial card online application and online application for trading service function -->
										<th>Chip Financial Card Online Application and Online
											Application for Trading Service</th>
										<!-- Credit Card Online Application -->
										<th>Credit Card Online Application</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Credit card account inquiry</td>
										<td>O</td>
										<td>O</td>
										<td>O</td>
										<td>O</td>
									</tr>
									<tr>
										<td>Credit card electronic bill</td>
										<td>O</td>
										<td>O</td>
										<td>O</td>
										<td>O</td>
									</tr>
									<tr>
										<td>Replenish credit card bills</td>
										<td>O</td>
										<td>O</td>
										<td>O</td>
										<td>O</td>
									</tr>
									<tr>
										<td>Credit card payment email notification</td>
										<td>O</td>
										<td>O</td>
										<td>O</td>
										<td>O</td>
									</tr>
									<tr>
										<td>Deposit, loan, foreign exchange account inquiry</td>
										<td>O</td>
										<td>O</td>
										<td>O</td>
										<td>X</td>
									</tr>
									<tr>
										<td>Fund inquiry</td>
										<td>O</td>
										<td>O</td>
										<td>O</td>
										<td>X</td>
									</tr>
									<tr>
										<td>Gold passbook inquiry</td>
										<td>O</td>
										<td>O</td>
										<td>O</td>
										<td>X</td>
									</tr>
									<tr>
										<td>Gold purchase/return and change</td>
										<td>O(Note 1)</td>
										<td>X</td>
										<td>X</td>
										<td>X</td>
									</tr>
									<tr>
										<td>Transfer, payment tax, deposit</td>
										<td>O</td>
										<td>X</td>
										<td>O</td>
										<td>X</td>
									</tr>
									<tr>
										<td>NTD non-contract transfer (Note 2)</td>
										<td>O</td>
										<td>X</td>
										<td>O</td>
										<td>X</td>
									</tr>
									<tr>
										<td>Fund subscription and change</td>
										<td>O</td>
										<td>X</td>
										<td>X</td>
										<td>X</td>
									</tr>
									<tr>
										<td>Apply for withholding fees</td>
										<td>O</td>
										<td>X</td>
										<td>O</td>
										<td>X</td>
									</tr>
									<tr>
										<td>Reporting loss service</td>
										<td>O</td>
										<td>O</td>
										<td>O</td>
										<td>X</td>
									</tr>
									<tr>
										<td>Change of communication materials (Note 3)</td>
										<td>O</td>
										<td>X</td>
										<td>O</td>
										<td>X</td>
									</tr>
									<tr>
										<td>Username/checkin/transaction password line unlocked
											(Note 4)</td>
										<td>O</td>
										<td>X</td>
										<td>O</td>
										<td>X</td>
									</tr>
									<tr>
										<td>Financial trial</td>
										<td>O</td>
										<td>O</td>
										<td>O</td>
										<td>O</td>
									</tr>
									<tr>
										<td>Domestic Taiwan dollar remittance notification
											setting</td>
										<td>O</td>
										<td>O</td>
										<td>O</td>
										<td>X</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div>
							<b>Remarks:</b><br>
							<ol class="list-decimal">
								<li>Customers applying for a gold passbook account need to
									apply for the gold passbook online trading function in the
									online banking or the bank, and have to perform functions such
									as gold purchase/return and change.</li>
								<li>If the online transaction function is applied, and the
									chip financial card already has the non-contracted transfer
									function, and the main account of the chip financial card of
									the account is the transfer account, the NTD non-contract
									transfer transaction and payment will be executed. Tax
									transactions.</li>
								<li>The trading mechanism is "electronic signature" or
									"chip financial card", you can perform online change of
									communication address / phone.</li>
								<li>The trading mechanism is "electronic signature" or "chip
									financial card", you can perform online check-in/transaction
									password line unlock.</li>
								<li>O: indicates that the function is available, and X:
									indicates that the function is not available. (original note 2)</li>
							</ol>
						</div>
						<div class="CN19-caption1">Two, gold passbook online
							application qualification, service function and fee collection</div>
						<div class="CN19-caption2">
							<span style="FONT-FAMILY: Wingdings;">u</span> <span>
								Apply online for a gold passbook account</span>
						</div>
						<div class="CN19-caption3">
							<ul>
								<li>Application eligibility</li>
							</ul>
							<div>
								Nationals who are 20 years of age or older. <br>  An
								account that has applied for the Bank's online banking and has
								agreed to a new NTD current deposit (excluding check deposit) is
								the transfer account. <br> Has applied for the use of the
								Bank's chip financial card + chip reader or electronic signature
								(voucher vehicle). <br>
							</div>
						</div>
						<div class="CN19-caption2">
							<span style="FONT-FAMILY: Wingdings;">u</span> <span>Online
								application for gold passbook online trading function</span>
						</div>
						<div class="CN19-caption3">
							<ul>
								<li>Application eligibility</li>
							</ul>
							<div>
								Nationals who are 20 years of age or older. <br>  An
								account that has applied for the Bank's online banking and has
								agreed to a new NTD current deposit (excluding check deposit) is
								the transfer account. <br> Has applied for the use of the
								Bank's chip financial card + chip reader or electronic signature
								(voucher vehicle). <br>
							</div>
						</div>
						<div class="CN19-caption3">
							<ul>
								<li>Service Features</li>
							</ul>
							<div>
								Query service: Gold passbook balance, detail, current day,
								historical price inquiry function. <br>  Trading services:
								Gold purchase, resale, and payment of regular deduction failure
								fee function. <br> Appointment service: Appointment to buy,
								sell, cancel, and query functions. <br> Regular fixed
								service: regular fixed purchase, regular fixed amount change,
								regular fixed amount inquiry function. <br> Online
								application: online application for gold passbook account, gold
								passbook network transaction function. <br>
							</div>
						</div>
						<div class="CN19-caption3">
							<ul>
								<li>Fees charged</li>
							</ul>
							<div>
								The online "Application for Gold Passbook Account" and "Regular
								Fixed Investment" will be debited successfully, and a handling
								fee of NT$50 will be charged. The remaining transactions will be
								exempted. <br>
							</div>
						</div>
					</div>
				</div>
			</div>
			</form>
	</div>
	</section>
	</main>
	</div>
</body>

</html>