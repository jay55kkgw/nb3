<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<link rel="stylesheet" type="text/css"
	href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine.js"></script>

<script type="text/JavaScript">


	var strErrPeriodIsZero  = "<spring:message code= "LB.X1377" />";
	
	function calculate2(){
		
 	    if($("#inv_yperiod").val() == ""){
	        //alert("<spring:message code= "LB.Alert075" />");
	        errorBlock(
							null, 
							null,
							["<spring:message code= "LB.Alert075" />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
	        $("#inv_yperiod").focus();
	        return false;
	    }
 	    
  	   if(isNaN($("#inv_yperiod").val())){
	        //alert("<spring:message code= "LB.Alert076" />");
	        errorBlock(
							null, 
							null,
							["<spring:message code= "LB.Alert076" />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
	       $("#inv_yperiod").focus();
	        return false;
	    }
  	   
 	   if($("#total_expense").val() == ""){
	        //alert("<spring:message code= "LB.Alert077" />");
	        errorBlock(
							null, 
							null,
							["<spring:message code= "LB.Alert077" />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
	        $("#total_expense").focus();
	        return false;
	    }

 	   if(isNaN($("#total_expense").val())){
 	        //alert("<spring:message code= "LB.Alert078" />");
 	        errorBlock(
							null, 
							null,
							["<spring:message code= "LB.Alert078" />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
 	       $("#total_expense").focus();
 	        return false;
 	    }
 	   
	    if($("#exp_interest").val() == ""){
	        //alert("<spring:message code= "LB.Alert079" />");
	        errorBlock(
							null, 
							null,
							["<spring:message code= "LB.Alert079" />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
	        $("#exp_interest").focus();
	        return false;
	    }
	    
	    if(isNaN($("#exp_interest").val())){
	        //alert("<spring:message code= "LB.Alert080" />");
	        errorBlock(
							null, 
							null,
							["<spring:message code= "LB.Alert080" />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
	        $("#exp_interest").focus();
	        return false;
	    }


	    var fBenefit    = parseFloat($("#total_expense").val());
	    var fYRate	 = parseFloat($("#exp_interest").val());
	    var nYear	 = Math.round($("#inv_yperiod").val());
	    var nMonth	 = Math.round(nYear * 12);
	    var fMRate   = fYRate/100/12.0;

	    if (nMonth <= 0)
	    {
	       //alert(strErrPeriodIsZero);
	       errorBlock(
							null, 
							null,
							[strErrPeriodIsZero], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
	       $("#inv_yperiod").focus();
	       return false;
	    }

		var fRatePower = 1.0;
		var fRateSum = 0.0;

	    for (var i=0; i < nMonth ; i++)
		{
			fRatePower = fRatePower*(1.0 + fMRate);
			fRateSum   = fRateSum + fRatePower;
		}

	    var fInvestMonthly = fBenefit/fRateSum;

	    $("#init_amt").val(fstop.formatAmt(String(Math.round(fInvestMonthly))));

		return true;
	}

	function chkNUM(getNUM){
	            var reNumber = /[^0-9]/;
		    var NUMSTR;
		    
		    if (getNUM.length>0)
		        NUMSTR = getNUM.substr(getNUM.length-1,1);
	    
		    if (reNumber.test(NUMSTR))
		    {
		        //alert("<spring:message code= "LB.Alert081" />");
		        errorBlock(
							null, 
							null,
							["<spring:message code= "LB.Alert081" />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
		        $("#inv_yperiod").val(getNUM.substr(0,getNUM.length-1));
		        $("#inv_yperiod").focus();
		        return false;
		    }
	return true;
	}


	</script>
</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 理財試算服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0087" /></li>
    <!-- 年金計畫試算     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0125" /></li>
		</ol>
	</nav>


	<!-- 快速選單及主頁內容 -->
	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
		<main class="col-12"> <!--主頁內容  -->
		<section id="main-content" class="container">
			<!--<h2><spring:message code="LB.Change_User_Name" /></h2>-->
			<h2><spring:message code="LB.X0125" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId">
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">

							<!-- 您想在-->
							<div class="ttb-input-item row">
								<span class="input-title"> <label> <!-- <h4><spring:message code="LB.User_name" /></h4> -->
										<h4><spring:message code="LB.X0126" /></h4>
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<input type="text" name="inv_yperiod" id="inv_yperiod"
											maxlength="100"  size="20"
											class="text-input  validate[required, newcolumn[ACCT, OLDUID, NEWUID],custom[onlyLetterNumber]] ">
										<span class="input-unit"><spring:message code="LB.X0127" /></span>
									</div>
								</span>
							</div>
							<!-- 擁有-->
							<div class="ttb-input-item row">
								<span class="input-title"> <label> <!-- <h4><spring:message code="LB.User_name" /></h4> -->
										<h4><spring:message code="LB.X0128" /></h4>
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<input type="text" name="total_expense" id="total_expense"
											maxlength="100"  size="20"
											class="text-input  validate[required, newcolumn[ACCT, OLDUID, NEWUID],custom[onlyLetterNumber]] ">
										<span class="input-unit"><spring:message code="LB.X0129" /></span>
									</div>
								</span>
							</div>
							<!-- 年利率 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label> <!-- <h4><spring:message code="LB.User_name" /></h4> -->
										<h4><spring:message code="LB.X0091" /></h4>
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<input type="text" name="exp_interest" id="exp_interest" maxlength="10"  size="20"
											class="text-input  validate[required, newcolumn[ACCT, OLDUID, NEWUID],custom[onlyLetterNumber]] ">
										<span class="input-unit">%</span> 
										<br>
										 <span class="input-unit">
											「<a href="https://www.tbb.com.tw/web/guest/-82" target="_blank"><spring:message code="LB.X0092" /></a>」
										</span>
									</div>
								</span>
							</div>
							<!--每月須存入新臺幣-->
							<div class="ttb-input-item row">
								<span class="input-title"> <label> <!-- <h4><spring:message code="LB.User_name" /></h4> -->
										<h4><spring:message code="LB.X1151" />&nbsp;&nbsp;<spring:message code="LB.NTD" /></h4>
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<input type="text" name="init_amt" id="init_amt"  size="20"
											readonly="readonly"
											class="text-input  validate[required, newcolumn[ACCT, OLDUID, NEWUID],custom[onlyLetterNumber]] ">
										<span class="input-unit">
										<spring:message code="LB.Dollar" /></span>
									</div>
								</span>
							</div>
						</div>
						<!-- <input id="pageshow" name="pageshow" type="submit" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" /> -->
						<input id="btnReset" name="btnReset" type="reset"
							class="ttb-button btn-flat-gray"
							value="<spring:message code="LB.Re_enter" />" />
						<input id="btnCount" name="btnCount" type="button"
							class="ttb-button btn-flat-orange" value="<spring:message code="LB.X0094" />" onclick="calculate2();" />
					</div>
				</div>
			</form>
				
				<ol class="list-decimal description-list">
					<!--<spring:message code="LB.Username_alter_P1_D1" />-->
					<p><spring:message code="LB.Description_of_page" /></p>
					<spring:message code="LB.Annuity_Plan_Trial_P1_D1" />
				</ol>
		</section>
		<!-- 		main-content END --> </main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>
</html>
