<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript">
	//因mail_set頁面同時include idgateAlert 及 idgateConfirmAlert 
	//但兩個的section id相同，故新增此jsp修改section id以免衝突
	var passAlert="";
	var openIdgate="${openIdgate}";
    jQuery(document).ready(function() {
    	//有開啟Idgate功能
    	if('Y' == openIdgate){
    		$("#idgate-block").bind('keydown', function(event) {
     	    	if (event.keyCode == 13) { // 如果按 enter     
     	            $("#idgateForward").click();
     	        }
     	    });
     	        //判斷是不是idgate使用者
     	    var idgateUserFlag = "${idgateUserFlag}";
     	    if(idgateUserFlag =="Y"){
     	        console.log("Is IDGATE user");           
     	    }else{
     	    	console.log("Not IDGATE user");
     	    	//確認 cookie , T:有cookie 跳過alert  F: 無cookie 顯示alert
     	        getPassCookie();
     	    	if("F" == passAlert){
     	    		showIdgateBlock();
     	    	}
     	    }
     	                
     	    //註冊btn
     	    $("#idgateForward").click(function (e){
     	    	setPassCookie();
     	    	//前往IDGATE申請路徑
   	        	setPassCookie();
     	        closeIdgateBlock();
   	        	window.open("${__ctx}/ONLINE/SERVING/idgate_confirm","_black");
     	    })
     	    $("#idgateKnowmore").click(function (e){
     	        setPassCookie();
     	        //TODO 操作手冊路徑
     	       setPassCookie();
     	       closeIdgateBlock();
     	       window.open("${__ctx}/ONLINE/SERVING/idgate_more","_black");
     	    })
     	    $("#idgateNexttime").click(function (e){
     	    	setPassCookie();
     	        closeIdgateBlock();
     	    })
    	}
    });
    
    function getPassCookie(){
		uri = '${__ctx}'+"/IDGATE/CHECK/has_pass_cookie_ajax";
	    rdata = {};
	    data = fstop.getServerDataEx(uri, rdata, false); //非同步
	    // T:有cookie 跳過alert  F: 無cookie 顯示alert
	    passAlert = data.data.hasPass;
	}
    
    function setPassCookie(){
		uri = '${__ctx}'+"/IDGATE/CHECK/set_pass_cookie_ajax"
	    rdata = {};
	    data = fstop.getServerDataEx(uri, rdata, false); //非同步
	}
    
    
    // 顯示showIdgateBlock
    function showIdgateBlock() {
        $("#windowMask").show(); 
        $("#idgate-block_other").show();
    }
    // 取消showIdgateBlock
    function closeIdgateBlock() {
        $("#idgate-block_other").hide();
        $('#windowMask').hide();
        unBlockUI(initBlockId);
    }
</script>


<!-- IDGATE pop訊息 -->
<section id="idgate-block_other" class="error-block" style="display: none">
    <div class="error-for-code">
        <!-- 為使交易更加快速安全，提供裝置認證交易機制 -->
        <p class="error-title">為使交易更加快速安全，提供裝置認證交易機制</p>
<!--         請輸入您的晶片金融卡密碼以繼續。 -->
<!--         <p class="error-content"><spring:message code="LB.X2059" /></p> -->
              
        <button id="idgateForward" class="btn-flat-orange ttb-pup-btn" >前往申請</button>
        <button id="idgateKnowmore" class="btn-flat-orange ttb-pup-btn" >了解詳情</button>
        <button id="idgateNexttime" class="btn-flat-gray ttb-pup-btn" >下次再說</button>
    </div>
</section>

<form id="idgate-block-form" action="" method="post">

</form> 
