<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    
	<script type="text/JavaScript">
		$(document).ready(function() {
			init();
		});
		
		function init(){
			
		   	$("#formId").validationEngine({binded:false,promptPosition: "inline" });
	
		   	// 確認
		   	$("#CMSUBMIT").click(function () {
				console.log("submit~~");
				
				// 表單驗證
				if(!$('#formId').validationEngine('validate')){
		        	e.preventDefault();
		        	
	 			} else {
	 				$("#formId").validationEngine('detach');
	 				initBlockUI();
	 				$("#formId").submit();
	 			}
			});
		   	
		   	$("#CUSIDN").focus();
		}	
	</script>
</head>
<body>
<div class="content row">
	<main class="col-12"> 
		<section id="main-content" class="container">
			<h2>
				<spring:message code='LB.X2242' />
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			
			<!-- 交易流程階段 -->
			<div id="step-bar">
				<ul>
					<li class="active"><spring:message code="LB.Enter_data" /></li>
					<li class=""><spring:message code="LB.Confirm_data" /></li>
					<li class=""><spring:message code="LB.Transaction_complete" /></li>
				</ul>
			</div>
			
			<!-- 功能內容 -->					
			<form method="post" id="formId" action="${__ctx}/lock_reset_confirm">
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
							
							<!-- 身分證字號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code='LB.D0581' />
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text" id="CUSIDN" name="CUSIDN" value="" maxlength="10" autocomplete="off"
											class="text-input validate[required,funcCall[validate_checkSYS_IDNO[CUSIDN]]">
									</div>
								</span>
							</div>
							
						</div>
						
						<!-- 確定 -->
						<input id="CMSUBMIT" name="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />						
					
					</div>
				</div>
			</form>
			
			<div class="text-left">
				<ol class="list-decimal text-left description-list">
				<p><spring:message code="LB.Description_of_page" /></p>
					<li><spring:message code='LB.X2241' /></li>
				</ol>
			</div>
			
		</section>
	</main>
</div>
</body>
</html>
 