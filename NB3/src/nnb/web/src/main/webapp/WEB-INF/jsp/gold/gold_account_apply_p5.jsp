<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<!-- 交易機制所需JS -->
	<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
	
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>	
</head>
 <body>
	<!-- 交易機制所需畫面 -->
	<%@ include file="../component/trading_component.jsp"%>

	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 黃金存摺帳戶     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2273" /></li>
    <!-- 黃金存摺帳戶申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1655" /></li>
		</ol>
	</nav>

 	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
	<main class="col-12"> 
	<!-- 		主頁內容  -->
		<section id="main-content" class="container">
<!-- 線上申請黃金存摺帳戶 -->
			<h2><spring:message code= "LB.W1655" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>			
                <div id="step-bar">
                    <ul>
                        <li class="finished">注意事項與權益</li>
                        <li class="finished">開戶資料</li>
                        <li class="active">確認資料</li>
                        <li class="">申請結果</li>
                    </ul>
                </div>
<%-- 			<p style="text-align: center;color: red;"><spring:message code= "LB.D0104" /></p> --%>
			<form method="post" id="formId">
				<input type="hidden" id="back" name="back" value="">
				<input type="hidden" id="TOKEN" name="TOKEN" value="${result_data.TOKEN}" /><!-- 防止重複交易 -->
                
                <!-- ikey -->
				<input type="hidden" id="jsondc" name="jsondc" value='${ result_data.jsondc }'>
				<input type="hidden" id="ISSUER" name="ISSUER" value="">
				<input type="hidden" id="ACNNO" name="accNo" value="">
				<input type="hidden" id="TRMID" name="TRMID" value="">
				<input type="hidden" id="iSeqNo" name="icSeq" value="">
				<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
				<input type="hidden" id="TAC" name="TAC" value="">
				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
				<input type="hidden" name="CMTRANPAGE" value="1">
				
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">			
<!-- 請確認申請資料 -->
                            <div class="ttb-message">
                                <p><spring:message code= "LB.D0104" /></p>
                            </div>
							<p class="form-description">請再次確認您填寫的資料。若需要修改，請點選上一步。</p>
							
							<!-- ***************************************** -->
							<div class="classification-block">
								<p>開戶資訊</p>
							</div>
							<!-- ***************************************** -->
						
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										網路交易指定新台幣帳戶
									</h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>${ result_data.SVACN }</p>
									</div>
								</span>
								<input type="hidden" id="SVACN" name="SVACN" value="${ result_data.SVACN }">
							</div>
							<!-- 確認新使用者名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
<!-- 本次開戶目的 -->
											<spring:message code= "LB.D1075" />
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
											<c:if test="${ result_data.PURPOSE.equals('1') }">
<!-- 儲蓄 -->
												<spring:message code= "LB.D1076" />
											</c:if>
											<c:if test="${ result_data.PURPOSE.equals('2') }">
<!-- 薪轉 -->
												<spring:message code= "LB.D1077" />
											</c:if>
											<c:if test="${ result_data.PURPOSE.equals('3') }">
<!-- 資金調撥 -->
												<spring:message code= "LB.D1078" />
											</c:if>
											<c:if test="${ result_data.PURPOSE.equals('4') }">
<!-- 投資 -->
												<spring:message code= "LB.D1079" />
											</c:if>
											<c:if test="${ result_data.PURPOSE.equals('5') }">
<!-- 其他 -->
												<spring:message code= "LB.D0572" />
											</c:if>
										</p>
									</div>
								</span>
								<input type="hidden" id="PURPOSE" name="PURPOSE" value="${ result_data.PURPOSE }">
							</div>
							<!-- 確認新使用者名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<!--  預期往來金額(近一年)-->
												<spring:message code= "LB.D1081_1" />
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p><spring:message code="LB.NTD"/> ${ result_data.S_MONEY } <spring:message code="LB.D0088_2"/></p>
									</div>
								</span>
								<input type="hidden" id="S_MONEY" name="S_MONEY" value="${ result_data.S_MONEY }">
							</div>
							<!-- ***************************************** -->
							<div class="classification-block">
								<p>基本資料</p>
							</div>
							<!-- ***************************************** -->
						
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<!-- 身分證統一編號 -->
										<spring:message code= "LB.D0519" />
									</h4>
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<p>${ result_data.CUSIDN2 }</p>
									</div>
								</span>
								<input type="hidden" id="CUSIDN2" name="CUSIDN2" value="${ result_data.CUSIDN2 }">
							</div>
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4>
										<!-- 更換過姓名 -->
										是否<spring:message code= "LB.D0052_3" />
									</h4>
								</label>
								</span> <span class="input-block">
									<div class="ttb-input">
										<p>
											<c:if test="${ result_data.RENAME.equals('Y') }">
												<!-- 有 -->
												<spring:message code= "LB.D1070_1" />
											</c:if>
											<c:if test="${ result_data.RENAME.equals('N') }">
												<!-- 無 -->
												<spring:message code= "LB.D1070_2" />
											</c:if>
										</p>
									</div>
								</span>
								<input type="hidden" id="RENAME" name="RENAME" value="${ result_data.RENAME }">
							</div>
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<!-- 出生年月日 -->
											<spring:message code= "LB.D0054" />
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
										<spring:message code="LB.D0583"/>&nbsp;${Integer.valueOf(result_data.CMDATE1.substring(0,4))-1911}&nbsp;<spring:message code="LB.Year"/>&nbsp;
										${result_data.CMDATE1.substring(5,7)}&nbsp;<spring:message code="LB.Month"/>&nbsp;
										${result_data.CMDATE1.substring(8,10)}&nbsp;<spring:message code="LB.D0586"/>&nbsp;
										</p>
									</div>
								</span>
								<input type="hidden" id="CMDATE1" name="CMDATE1" value="${ result_data.CMDATE1 }">
							</div>
							<!-- 確認新使用者名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<!-- 身分證為換發/補發/新發日期 -->
										<h4>身分證發證資訊</h4>
									
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>
											<spring:message code="LB.D0583"/>&nbsp;${Integer.valueOf(result_data.CMDATE2.substring(0,4))-1911}&nbsp;<spring:message code="LB.Year"/>&nbsp;
											${result_data.CMDATE2.substring(5,7)}&nbsp;<spring:message code="LB.Month"/>&nbsp;
											${result_data.CMDATE2.substring(8,10)}&nbsp;<spring:message code="LB.D0586"/>&nbsp;
											(${ result_data.CITYCHA_str })
											<c:if test="${ result_data.TYPECHA.equals('1') }">
												<!-- 初領 -->
												<spring:message code= "LB.D1113" />
											</c:if>
											<c:if test="${ result_data.TYPECHA.equals('2') }">
												<!-- 換發 -->
												<spring:message code= "LB.W1664" />
											</c:if>
											<c:if test="${ result_data.TYPECHA.equals('3') }">
												<!-- 補發 -->
												<spring:message code= "LB.W1665" />
											</c:if>
										</p>
									</div>
									<input type="hidden" id="TYPECHA" name="TYPECHA" value="${ result_data.TYPECHA }">
									<input type="hidden" id="CMDATE2" name="CMDATE2" value="${ result_data.CMDATE2 }">
									<input type="hidden" id="CITYCHA" name="CITYCHA" value="${ result_data.CITYCHA }">
									<input type="hidden" id="PIC_FLAG" name="PIC_FLAG" value="${ result_data.PIC_FLAG }">
								</span>
							</div>
							<!-- 確認新使用者名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
<!-- 連絡電話 -->
											<spring:message code= "LB.D0205" />
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>(${ result_data.ARACOD2 }) ${ result_data.TELNUM2 }</p>
									</div>
								</span>
								<input type="hidden" id="ARACOD2" name="ARACOD2" value="${ result_data.ARACOD2 }">
								<input type="hidden" id="TELNUM2" name="TELNUM2" value="${ result_data.TELNUM2 }">
							</div>
							
							<!-- 確認新使用者名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
<!-- 任職機構 -->
											<spring:message code= "LB.D1074" />
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<p>${ result_data.EMPLOYER }</p>
									</div>
								</span>
								<input type="hidden" id="EMPLOYER" name="EMPLOYER" value="${ result_data.EMPLOYER }">
							</div>
							
							<!-- ***************************************** -->
							<div class="classification-block">
								<p>開戶費用</p>
							</div>
							<!-- ***************************************** -->
							<!-- 確認新使用者名稱 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
<!-- 開戶手續費 -->
											<spring:message code= "LB.D1082" />
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
<!-- 新台幣 -->
<!-- 元 -->
										<span class="high-light"><spring:message code= "LB.NTD" /> ${ result_data.AMT_FEE } <spring:message code= "LB.Dollar" /></span>
									</div>
								</span>
								<input type="hidden" id="AMT_FEE" name="AMT_FEE" value="${ result_data.AMT_FEE }">
							</div>
							
							
							<!-- ***************************************** -->
                            <div class="classification-block">
                                <p>身份驗證</p>
<!-- 	                                <a href="#" class="classification-edit-btn">編輯</a> -->
                            </div>
							<!-- ***************************************** -->
							
							<!--交易機制  SSL 密碼 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4>
											<spring:message code="LB.Transaction_security_mechanism" />
										</h4>
									</label>
								</span> 
								<span class="input-block">
<!-- 									<div class="ttb-input"> -->
<!-- 										<label> -->
<%-- 								       		<input type="radio" name="FGTXWAY" id="CMSSL" value="0" checked /><spring:message code="LB.SSL_password" /> --%>
<!-- 								       	</label> -->
<!-- 										<input type="password" name = "CMPASSWORD" id="CMPASSWORD"  maxlength="8" class="text-input validate[required, pwvd[PW, CMPASSWORD], custom[onlyLetterNumber]]"> -->
<!-- 										<input type="hidden" name = "PINNEW" id="PINNEW" value=""> -->
<!-- 									</div> -->
<!-- 									使用者是否可以使用IKEY -->
									<c:if test = "${sessionScope.isikeyuser}">
<!-- 										即使使用者可以用IKEY，若轉出帳號為晶片金融卡帶出的非約定帳號，也不給用 (By 景森) -->
<%-- 										<c:if test = "${transfer_data.data.TransferType != 'NPD'}"> --%>
									
<!-- 											IKEY -->
											<div class="ttb-input">
												<label class="radio-block">
													<input type="radio" name="FGTXWAY" id="CMIKEY" value="1">
													<spring:message code="LB.Electronic_signature" />
													<span class="ttb-radio"></span>
												</label>
											</div>
									
<%-- 										</c:if> --%>
									</c:if>
									<!-- 晶片金融卡 -->
									<div class="ttb-input">
										<label class="radio-block">
											<spring:message code="LB.Financial_debit_card" />
											
											<!-- 只能選晶片金融卡時，預設為非勾選讓使用者點擊後顯示驗證碼-->
											<c:choose>
												<c:when test="${result_data.TransferType == 'NPD'}">
													<input type="radio" name="FGTXWAY" id="CMCARD" value="2">
												</c:when>
												<c:when test="${!sessionScope.isikeyuser}">
													<input type="radio" name="FGTXWAY" id="CMCARD" value="2">
												</c:when>
												<c:otherwise>
												 	<input type="radio" name="FGTXWAY" id="CMCARD" value="2" checked="checked">
												</c:otherwise>
											</c:choose>
											
											<span class="ttb-radio"></span>
										</label>
									</div>
									
								</span>
							</div>
								
							<div class="ttb-input-item row" id="capCodeDiv">
								<span class="input-title"> 
									<label>
										<h4>
											<!-- 驗證碼 -->
											<spring:message code= "LB.D0032" />
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<spring:message code="LB.Captcha" var="labelCapCode" />
										<input id="capCode" type="text" class="text-input input-width-125"
											name="capCode" placeholder="${labelCapCode}" maxlength="6" autocomplete="off">
										<img name="kaptchaImage" src=""  class="verification-img"/>
										<input type="button" name="reshow" class="ttb-sm-btn btn-flat-gray" onclick="refreshCapCode()" value="<spring:message code="LB.Regeneration"/>" />
										<span class="input-remarks">請注意：英文不分大小寫，限半形字元</span>
									</div>
								</span>
							</div>
							
						</div>
						<!--回上頁 -->
                        <spring:message code="LB.X0318" var="cmback"></spring:message>
                        <input type="button" name="CMBACK" id="CMBACK" value="${cmback}" class="ttb-button btn-flat-gray">		
<%-- 						<input id="reset" name="reset" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" />	 --%>
						<input id="pageshow" name="pageshow" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.X0080" />" />				
					</div>
				</div>
			</form>
			<div class="text-left">
			    <!-- 		說明： -->
			    <ol class="list-decimal description-list">
			    	<p><spring:message code="LB.Description_of_page"/></p>
<!-- 如選擇晶片金融卡機制，按「確定」鍵前，請將讀卡機正確連接電腦，並插入晶片金融卡。 -->
			        <li><spring:message code="LB.Gold_Account_Apply_P5_D1"/></li>
			    </ol>
			</div>
		</section>
		<!-- 		main-content END -->
	</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
	<!--   Js function -->
    <script type="text/JavaScript">
		$(document).ready(function() {
			// HTML載入完成後0.1秒開始遮罩
			setTimeout("initBlockUI()", 100);

			// 初始化驗證碼
			setTimeout("initKapImg()", 200);
			// 生成驗證碼
			setTimeout("newKapImg()", 300);
			
			init();
			fgtxwayClick();
			// 過0.5秒解遮罩
			setTimeout("unBlockUI(initBlockId)", 800);
		});
		
		/**
		 * 初始化BlockUI
		 */
		function initBlockUI() {
			initBlockId = blockUI();
		}

		// 交易機制選項
		function processQuery(){
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
			console.log("fgtxway: " + fgtxway);
		
			switch(fgtxway) {
				case '0':
// 					alert("交易密碼(SSL)...");
	    			$("form").submit();
					break;
					
				case '1':
// 					alert("IKey...");
					var jsondc = $("#jsondc").val();
					
					// 遮罩後不給捲動
// 					document.body.style.overflow = "hidden";

					// 呼叫IKEY元件
// 					uiSignForPKCS7(jsondc);
					
					// 解遮罩後給捲動
// 					document.body.style.overflow = 'auto';
					
					useIKey();
					break;
					
				case '2':
// 					alert("晶片金融卡");

					// 遮罩後不給捲動
// 					document.body.style.overflow = "hidden";
					
					// 呼叫讀卡機元件
					listReaders();

					// 解遮罩後給捲動
// 					document.body.style.overflow = 'auto';
					
			    	break;
			    	
				default:
					//alert("nothing...");
					errorBlock(
							null, 
							null,
							["nothing..."], 
							'<spring:message code= "LB.Quit" />', 
							null
					);
			}
			
		}

	    function init(){
// 	    	$("#formId").validationEngine({binded:false,promptPosition: "inline" });
	    	$("#pageshow").click(function(e){			
				console.log("submit~~");
				var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
				var capData = $("#formId").serializeArray();
				var capResult = fstop.getServerDataEx(capUri, capData, false);
				if (capResult.result || $('input[name="FGTXWAY"]:checked').val() != 2) {
					if(!$('#formId').validationEngine('validate') && $('input[name="FGTXWAY"]:checked').val() == 0){
			        	e.preventDefault();
		 			}else{
	// 					$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
		 				$("#formId").validationEngine('detach');
// 		 				initBlockUI();
		    			var action = '${__ctx}/GOLD/APPLY/gold_account_apply_result';
		    			$("form").attr("action", action);
// 		    			unBlockUI(initBlockId);
		    			processQuery();
		 			}
				} else {
					//驗證碼有誤
					//alert("<spring:message code= "LB.X1082" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X1082' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
					);
					changeCode();
				}
			});
    		//上一頁按鈕
    		$("#CMBACK").click(function() {
    			var action = '${__ctx}/GOLD/APPLY/gold_account_apply_p4';
    			$('#back').val("Y");
    			$("form").attr("action", action);
    			$("form").submit();
    		});
	    }	
	    
		// 刷新驗證碼
		function refreshCapCode() {
			console.log("refreshCapCode...");

			// 驗證碼
			$('input[name="capCode"]').val('');
			
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').hide().attr(
					'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

			// 登入失敗解遮罩
			unBlockUI(initBlockId);
		}
		
		// 刷新輸入欄位
		function changeCode() {
			console.log("changeCode...");
			
			// 清空輸入欄位
			$('input[name="capCode"]').val('');
			
			// 刷新驗證碼
			refreshCapCode();
		}

		// 初始化驗證碼
		function initKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
		}

		// 生成驗證碼
		function newKapImg() {
			// 大小版驗證碼用同一個
			$('img[name="kaptchaImage"]').click(function() {
				refreshCapCode();
			});
		}

		// 使用者選擇晶片金融卡要顯示驗證碼區塊
	 	function fgtxwayClick() {
	 		$('input[name="FGTXWAY"]').change(function(event) {
	 			// 判斷交易機制決定顯不顯示驗證碼區塊
	 			chaBlock();
			});
		}
	 	// 判斷交易機制決定顯不顯示驗證碼區塊
		function chaBlock() {
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
 			console.log("fgtxway: " + fgtxway);
			// 交易機制選項
			if(fgtxway == '2'){
				// 若為晶片金融卡才顯示驗證碼欄位
				$("#capCodeDiv").show();
			}else{
				// 若非晶片金融卡則隱藏驗證碼欄位
				$("#capCodeDiv").hide();
			}
	 	}
 	</script>
</body>
</html>
 