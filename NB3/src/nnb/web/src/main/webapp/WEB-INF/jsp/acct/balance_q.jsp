<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>

<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
	<!-- 帳戶查詢 -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Account_Inquiry" /></li>
    <!-- 帳戶餘額查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.NTD_Demand_Deposit_Balance" /></li>
		</ol>
	</nav>



	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>

		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> <!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<h2>
				<spring:message code="LB.NTD_Demand_Deposit_Balance" />
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>

			<div class="main-content-block row">
				<div class="col-12">
					<ul class="ttb-result-list">
						<!-- 查詢時間 -->
						<li>
							<h3>
								<spring:message code="LB.Inquiry_time" />
								：
							</h3>
							<p>${balance_query.data.CMQTIME }</p>
						</li>
						<li>
							<!-- 查詢筆數 -->
							<h3>
								<spring:message code="LB.Total_records" />
								：
							</h3>
							<p>
								${balance_query.data.CMRECNUM }&nbsp;<spring:message code="LB.Rows" />
							</p>
						</li>
					</ul>
					<ul class="ttb-result-list">

					</ul>
					<table
						class="stripe table-striped ttb-table dtable"
						id="example" data-show-toggle="first">
						<thead>
							<tr>
								<th class="text-center"><spring:message code="LB.Account" /></th>
								<th class="text-center"><spring:message code="LB.Deposit_type" /></th>
								<th class="text-center"><spring:message code="LB.Available_balance" /></th>
								<th class="text-center"><spring:message code="LB.Account_balance" /></th>
								<th class="text-center"><spring:message code="LB.Exchange_ticket_today" /></th>
								<!-- 快速選單功能 -->
								<th class="text-center"><spring:message code="LB.Quick_Menu" /></th>
								<!-- 快速選單測試 -->
							</tr>
						</thead>
						<c:if test="${empty balance_query.data.REC}">
								<tbody>
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
								</tbody>
						</c:if>
						<c:if test="${not empty balance_query.data.REC}">
						<tbody>
							<c:forEach var="dataList" items="${ balance_query.data.REC }"
								varStatus="data">
								<tr>
									<td class="text-center">${dataList.ACN }</td>
									<td class="text-center">${dataList.KIND }</td>
									<td class="text-right">${dataList.ADPIBAL }</td>
									<td class="text-right">${dataList.BDPIBAL }</td>
									<td class="text-center">${dataList.CLR }</td>
									<td class="text-center"><c:if
											test="${dataList.SHOWSELECT == 'Y'}">
											<!-- 下拉式選單-->
											<input type="button" class="d-lg-none" id="click"
												onclick="hd2('actionBar2${data.index}')" value="..." />
											<%--  <button class="d-lg-none" id="click" onclick="hd2('actionBar2${data.index}')">...</button> --%>
											<%--  <input type="button" class="d-sm-none d-block" id="click" value="..." onclick="hd2('actionBar2${data.index}')"/><!-- 快速選單測試 -->				 --%>
											<select
												class="custom-select d-none d-lg-inline-block fast-select"
												id="actionBar" onchange="formReset(this.value,'${dataList.ACN }');">
												<option value="">
													<spring:message code="LB.Select" />
												</option>
												<!-- 活期性存款明細 -->
												<option value="demand_deposit_detail">
													<spring:message code="LB.NTD_Demand_Deposit_Detail" />
												</option>
												<c:if test="${dataList.ACNGROUP == '01'}">
													<!-- 當日待補票據明細 -->
													<option value="checking_insufficient">
														<spring:message code="LB.X1571" />
													</option>
													<!-- 已兌現票據明細 -->
													<option value="checking_cashed">
														<spring:message code="LB.Cashed_Note_Details" />
													</option>
												</c:if>
												<c:if test="${dataList.OUTACN == 'Y'}">
													<!-- 轉帳 -->
													<option value="transfer">
														<spring:message code="LB.NTD_Transfer" />
													</option>
													<!-- 轉入台幣綜存定存 -->
													<option value="deposit_transfer">
														<spring:message
															code="LB.Open_Taiwan_Currency_Time_Deposit" />
													</option>
													<!-- 繳本行信用卡款 -->
													<option value="payment">
														<spring:message code="LB.Payment_The_Banks_Credit_Card" />
													</option>
													<!-- 停車費代扣繳申請 -->
													<option value="n614">
														<spring:message code="LB.X0276" />
													</option>
												</c:if>
											</select>
											<!-- 快速選單測試新增div -->
											<div id="actionBar2${data.index}" class="fast-div-grey"
												style="visibility: hidden">
												<div class="fast-div">
													<img src="${__ctx}/img/icon-close-2.svg"
														class="top-close-btn"
														onclick="hd2('actionBar2${data.index}')">
													<p>
														<spring:message code="LB.X1592" />
													</p>
													<ul>
														<a href="${__ctx}/NT/ACCT/demand_deposit_detail?Acn=${dataList.ACN }">
															<li><spring:message
																	code="LB.NTD_Demand_Deposit_Detail" /><img
																src="${__ctx}/img/icon-10.svg" align="right"></li>
														</a>
														<c:if test="${dataList.ACNGROUP == '01'}">
															<a href="${__ctx}/NT/ACCT/checking_insufficient?Acn=${dataList.ACN }">
																<li><spring:message code="LB.X1571" /><img
																	src="${__ctx}/img/icon-10.svg" align="right"></li>
															</a>
															<a href="${__ctx}/NT/ACCT/checking_cashed?Acn=${dataList.ACN }">
																<li><spring:message code="LB.Cashed_Note_Details" />
																	<img src="${__ctx}/img/icon-10.svg" align="right"></li>
															</a>
														</c:if>
														<c:if test="${dataList.OUTACN == 'Y'}">
															<a href="${__ctx}/NT/ACCT/TRANSFER/transfer?Acn=${dataList.ACN }">
																<li><spring:message code="LB.NTD_Transfer" /><img
																	src="${__ctx}/img/icon-10.svg" align="right"></li>
															</a>
															<a href="${__ctx}/NT/ACCT/TDEPOSIT/deposit_transfer?Acn=${dataList.ACN }">
																<li><spring:message
																		code="LB.Open_Taiwan_Currency_Time_Deposit" /> <img
																	src="${__ctx}/img/icon-10.svg" align="right"></li>
															</a>
															<a href="${__ctx}/CREDIT/PAY/payment?Acn=${dataList.ACN }">
																<li><spring:message
																		code="LB.Payment_The_Banks_Credit_Card" /> <img
																	src="${__ctx}/img/icon-10.svg" align="right"></li>
															</a>
															<a href="${__ctx}/PARKING/FEE/parking_withholding?Acn=${dataList.ACN }">
																<li><spring:message code="LB.X0276" /><img
																	src="${__ctx}/img/icon-10.svg" align="right"></li>
															</a>
														</c:if>
													</ul>
													<input type="button" class="bottom-close-btn"
														onclick="hd2('actionBar2${data.index}')"
														value="<spring:message code= "LB.X1572" />" />
												</div>
											</div>
										</c:if></td>
								</tr>
							</c:forEach>
						</tbody>
						</c:if>
					</table>
					<input type="button" class="ttb-button btn-flat-orange"
						id="printbtn" value="<spring:message code="LB.Print" />" />
				</div>
			</div>
			<div class="text-left">
				<ol class="description-list list-decimal">
					<p>
						<spring:message code="LB.Description_of_page" />
					</p>
					<li><spring:message code="LB.Balance_query_P1_D1" /></li>
					<li><spring:message code="LB.Balance_query_P1_D2" /></li>
				</ol>
			</div>
		</section>
		</main>
	</div>
	<!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
	<!-- 快速選單測試CSS -->

	<script type="text/javascript">
		$(document).ready(function() {

			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);

			setTimeout("initDataTable()",100);
		});

		function init() {

			// initFootable();

			$("#printbtn")
					.click(
							function() {
								var params = {
									"jspTemplateName" : "balance_q_print",
									"jspTitle" : "<spring:message code='LB.NTD_Demand_Deposit_Balance' />",
									"CMQTIME" : "${balance_query.data.CMQTIME }",
									"COUNT" : "${balance_query.data.CMRECNUM}"
								};
								openWindowWithPost(
										"${__ctx}/print",
										"height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",
										params);
							});

		}

		//快速選單
		function formReset(value,acn) {
			switch (value) {
			case "demand_deposit_detail":
				fstop.getPage('${pageContext.request.contextPath}'
						+ '/NT/ACCT/demand_deposit_detail?Acn='+acn+'&', '', '');
				break;
			case "checking_insufficient":
				fstop.getPage('${pageContext.request.contextPath}'
						+ '/NT/ACCT/checking_insufficient?Acn='+acn+'&', '', '');
				break;
			case "checking_cashed":
				fstop.getPage('${pageContext.request.contextPath}'
						+ '/NT/ACCT/checking_cashed?Acn='+acn+'&', '', '');
				break;
			case "transfer":
				fstop.getPage('${pageContext.request.contextPath}'
						+ '/NT/ACCT/TRANSFER/transfer?Acn='+acn+'&', '', '');
				break;
			case "deposit_transfer":
				fstop.getPage('${pageContext.request.contextPath}'
						+ '/NT/ACCT/TDEPOSIT/deposit_transfer?Acn='+acn+'&', '', '');
				break;
			case "payment":
				fstop.getPage('${pageContext.request.contextPath}'
						+ '/CREDIT/PAY/payment?Acn='+acn+'&', '', '');
				break;
			case "n614":
				fstop.getPage('${pageContext.request.contextPath}'
						+ '/PARKING/FEE/parking_withholding?Acn='+acn+'&', '', '');
				break;
			}
		}

		//快速選單測試新增
		//將actionbar select 展開閉合
		$(function() {
			$("#click").on('click', function() {
				var s = $("#actionBar2").attr('size') == 1 ? 8 : 1
				$("#actionBar2").attr('size', s);
			});
			$("#actionBar2 option").on({
				click : function() {
					$("#actionBar2").attr('size', 1);
				},
			});
		});

		function hd2(T) {
			var t = document.getElementById(T);
			if (t.style.visibility === 'visible') {
				t.style.visibility = 'hidden';
			} else {
				$("div[id^='actionBar2']").each(function() {
					var d = document.getElementById($(this).attr('id'));
					d.style.visibility = 'hidden';
				});
				t.style.visibility = 'visible';
			}
		}
	</script>
</body>

</html>