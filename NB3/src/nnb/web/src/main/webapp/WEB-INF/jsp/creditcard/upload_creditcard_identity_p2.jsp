<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<!-- 交易機制所需JS -->
<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
<!--舊版驗證-->
<script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
<style type="text/css">
	input[type="file"]{
		overflow: hidden;
	}
</style>
<script type="text/JavaScript">
$(document).ready(function () {
	// HTML載入完成後開始遮罩
	setTimeout("initBlockUI()", 10);
	// 開始查詢資料並完成畫面
	setTimeout("init()", 20);
	// 解遮罩
	setTimeout("unBlockUI(initBlockId)", 500);
});

// 畫面初始化
function init() {
	console.log("${result_data.data.CPRIMBIRTHDAYshow}");
	uploadSetting();
	
	$("#CMSUBMIT").click(function(e) {
		var ulflg = '${sessionScope.pflg}';

		if(ulflg == "1"){
			if($("#FILE1").val() == ""){
				errorBlock(null, null, ["<spring:message code='LB.X1074' />"],
						'<spring:message code= "LB.Quit" />', null);
				return false;
			}
			if($("#FILE2").val() == ""){
				errorBlock(null, null, ["<spring:message code='LB.X1075' />"],
						'<spring:message code= "LB.Quit" />', null);
				return false;
			}
		}
		if(ulflg == "2"){
			if($("#FILE3").val() == "" && $("#FILE4").val() == "" && $("#FILE5").val() == ""){
				errorBlock(null, null, ["<spring:message code='LB.X1076' />"],
						'<spring:message code= "LB.Quit" />', null);
				return false;
			}
		}		
		if(ulflg == "3"){
			var msg = '<spring:message code= "LB.onlineapply_creditcard_X0172" />';
			var msg1;
			if($("#FILE1").val() == "" || $("#FILE2").val() == ""){
				if($("#FILE3").val() == "" && $("#FILE4").val() == "" && $("#FILE5").val() == ""){
					msg1 = '<spring:message code= "LB.X2021" />、<spring:message code= "LB.X2024" />';
					errorBlock(null, null, [msg+msg1],
							'<spring:message code= "LB.Quit" />', null);
					return false;
				}
				
			}else if($("#FILE1").val() != "" && $("#FILE2").val() != ""){
				if($("#FILE3").val() == "" && $("#FILE4").val() == "" && $("#FILE5").val() == ""){
					msg1 = '<spring:message code= "LB.X2024" />';
					errorBlock(null, null, [msg+msg1],
							'<spring:message code= "LB.Quit" />', null);
					return false;
				}
				
			}

		}			
			
     	initBlockUI();
     	$("#formId").attr("action", "${__ctx}/CREDIT/APPLY/upload_creditcard_identity_p3");
        $("#formId").submit();

        
	});
	$("#CMBACK").click( function(e) {
		$("#formId").validationEngine('detach');
		console.log("submit~~");
		$("#formId").attr("action", "${__ctx}/CREDIT/APPLY/upload_creditcard_identity_p1");
		$("#back").val('Y');

     	initBlockUI();
        $("#formId").submit();
	});
}

function onUpLoad(fileFilter,imgFilter,type,postFile) {
	var BD = '${result_data.data.back}';
	if(document.getElementById(type + "_error")){
		$("#" + type + "_error").remove();
	}
	
    var fileData = new FormData();

    var file1 = $(fileFilter).get(0);
    if (file1.files.length > 0) {
        fileData.append("picFile", file1.files[0]);
    }
    fileData.append("type",type);
    fileData.append("oldPath",$(postFile).val());
    fileData.append("filetime",'${result_data.data.filetime}');
    if (file1.files.length == 0 ) {
//         alert("<spring:message code= "LB.Alert052" />");
		$(imgFilter).after("<p class='photo-error-message' id='" + type + "_error'><spring:message code='LB.Alert052' /></p>");
        return;
    }

    $.ajax({
        url: "${__ctx}/CREDIT/APPLY/upload_creditcard_identity_p2_get_zip_fileupload",
        type: "POST",
        contentType: false,  // Not to set any content header
        processData: false,  // Not to process data
        data: fileData,
        success: function (res) {
        	console.log(res);
            if(res.data.validated){
//                 alert("檔案匯入成功");   
                $(imgFilter).attr('src',"${__ctx}/upload/" + res.data.url + "?timestamp=" + new Date().getTime());
                $(postFile).val(res.data.url);
                $(imgFilter+"_close_btn").show();
            } else {
//             	alert(res.data.summary);  
				$(imgFilter).after("<p class='photo-error-message' id='" + type + "_error'>" + res.data.summary + "</p>");
                $(imgFilter+"_close_btn").hide();
            }
        }
    });
}
//拖移用
function onUpLoadByDrop(file1,imgFilter,type,postFile) {
	var BD = '${result_data.data.back}';
	if(document.getElementById(type + "_error")){
		$("#" + type + "_error").remove();
	}
	
    var fileData = new FormData();
    if (file1.length > 0) {
        fileData.append("picFile", file1[0]);
    }
    fileData.append("type",type);
    fileData.append("oldPath",$(postFile).val());
    fileData.append("filetime",'${result_data.data.filetime}');
    if (file1.length == 0 ) {
		$(imgFilter).after("<p class='photo-error-message' id='" + type + "_error'><spring:message code='LB.Alert052' /></p>");
        return;
    }

    $.ajax({
        url: "${__ctx}/CREDIT/APPLY/update_creditcard_identity_get_zip_fileupload",
        type: "POST",
        contentType: false,  // Not to set any content header
        processData: false,  // Not to process data
        data: fileData,
        success: function (res) {
        	console.log(res);
            if(res.data.validated){
                $(imgFilter).attr('src',"${__ctx}/upload/" + res.data.url + "?timestamp=" + new Date().getTime());
                $(postFile).val(res.data.url);
                $(imgFilter+"_close_btn").show();
            } else {
//             	alert(res.data.summary);  
				$(imgFilter).after("<p class='photo-error-message' id='" + type + "_error'>" + res.data.summary + "</p>");
                $(imgFilter+"_close_btn").hide();
            }
        }
    });
}

//移除上傳圖片
function deleteUpLoad(imgFilter,postFile,orgImg){
    console.log($(postFile).val());
    $.ajax({
        url: "${__ctx}/CREDIT/APPLY/upload_creditcard_identity_p2_fileupload_delete",
        type: "POST",
        data: { 
        	Path:$(postFile).val()
        },
        success: function (res) {
        	console.log(res);
            if(res.data.sessulte){
                $(imgFilter).attr('src',"${__ctx}/img/"+orgImg+".svg");
                $(postFile).val("");
                $(imgFilter+"_close_btn").hide();
            } else {
//             	alert(res.data.message);    
				$(imgFilter).after("<p class='photo-error-message' id='" + type + "_error'>" + res.data.summary + "</p>");
            }
        }
    });
	
}

function uploadSetting(){

	$('#img1').on("dragenter", function(e){
		console.log('dragenter');
		e = e || window.event;
        e.preventDefault();
        e.returnValue = false;
     });
	$('#img1').on('dragover', function(e){
		console.log('dragover');
		e = e || window.event;
	    e.preventDefault();
	    e.returnValue = false;
	  })
	$('#img1').on('drop', function(e){
		e = e || window.event;
		e.stopPropagation();
		e.preventDefault();
        var file = e.originalEvent.dataTransfer.files;
		onUpLoadByDrop(file,'#img1', '1', '#FILE1');
	})
	$('#img1').click(function(e){
		$("#file1").click();
	})
	$("#file1").change("change", function(e) {
		onUpLoad('#file1','#img1', '1', '#FILE1');
	});
	if("${result_data.data.FILE1}" != ""){
        $('#img1').attr('src',"${__ctx}/upload/${result_data.data.FILE1}?timestamp=" + new Date().getTime());
        $("#img1_close_btn").show();
	}else{
        $("#img1_close_btn").hide();
	}

	$('#img2').on("dragenter", function(e){
		console.log('dragenter');
		e = e || window.event;
        e.preventDefault();
        e.returnValue = false;
     });
	$('#img2').on('dragover', function(e){
		console.log('dragover');
		e = e || window.event;
	    e.preventDefault();
	    e.returnValue = false;
	  })
	$('#img2').on('drop', function(e){
		e = e || window.event;
		e.stopPropagation();
		e.preventDefault();
        var file = e.originalEvent.dataTransfer.files;
		onUpLoadByDrop(file,'#img2', '2', '#FILE2');
	})
	$('#img2').click(function(e){
		$("#file2").click();
	})
	$("#file2").change("change", function(e) {
		onUpLoad('#file2','#img2', '2', '#FILE2');
	});
	if("${result_data.data.FILE2}" != ""){
        $('#img2').attr('src',"${__ctx}/upload/${result_data.data.FILE2}?timestamp=" + new Date().getTime());
        $("#img2_close_btn").show();
	}else{
        $("#img2_close_btn").hide();
	}
	
	$('#img3').on("dragenter", function(e){
		console.log('dragenter');
		e = e || window.event;
        e.preventDefault();
        e.returnValue = false;
     });
	$('#img3').on('dragover', function(e){
		console.log('dragover');
		e = e || window.event;
	    e.preventDefault();
	    e.returnValue = false;
	  })
	$('#img3').on('drop', function(e){
		e = e || window.event;
		e.stopPropagation();
		e.preventDefault();
        var file = e.originalEvent.dataTransfer.files;
		onUpLoadByDrop(file,'#img3', '3', '#FILE3');
	})
	$('#img3').click(function(e){
		$("#file3").click();
	})
	$("#file3").change("change", function(e) {
		onUpLoad('#file3','#img3', '3', '#FILE3');
	});
	if("${result_data.data.FILE3}" != ""){
        $('#img3').attr('src',"${__ctx}/upload/${result_data.data.FILE3}?timestamp=" + new Date().getTime());
        $("#img3_close_btn").show();
	}else{
        $("#img3_close_btn").hide();
	}
	
	$('#img4').on("dragenter", function(e){
		console.log('dragenter');
		e = e || window.event;
        e.preventDefault();
        e.returnValue = false;
     });
	$('#img4').on('dragover', function(e){
		console.log('dragover');
		e = e || window.event;
	    e.preventDefault();
	    e.returnValue = false;
	  })
	$('#img4').on('drop', function(e){
		e = e || window.event;
		e.stopPropagation();
		e.preventDefault();
        var file = e.originalEvent.dataTransfer.files;
		onUpLoadByDrop(file,'#img4', '4', '#FILE4');
	})
	$('#img4').click(function(e){
		$("#file4").click();
	})
	$("#file4").change("change", function(e) {
		onUpLoad('#file4','#img4', '4', '#FILE4');
	});
	if("${result_data.data.FILE4}" != ""){
        $('#img4').attr('src',"${__ctx}/upload/${result_data.data.FILE4}?timestamp=" + new Date().getTime());
        $("#img4_close_btn").show();
	}else{
        $("#img4_close_btn").hide();
	}
	
	$('#img5').on("dragenter", function(e){
		console.log('dragenter');
		e = e || window.event;
        e.preventDefault();
        e.returnValue = false;
     });
	$('#img5').on('dragover', function(e){
		console.log('dragover');
		e = e || window.event;
	    e.preventDefault();
	    e.returnValue = false;
	  })
	$('#img5').on('drop', function(e){
		e = e || window.event;
		e.stopPropagation();
		e.preventDefault();
        var file = e.originalEvent.dataTransfer.files;
		onUpLoadByDrop(file,'#img5', '5', '#FILE5');
	})
	$('#img5').click(function(e){
		$("#file5").click();
	})
	$("#file5").change("change", function(e) {
		onUpLoad('#file5','#img5', '5', '#FILE5');
	});
	if("${result_data.data.FILE5}" != ""){
        $('#img5').attr('src',"${__ctx}/upload/${result_data.data.FILE5}?timestamp=" + new Date().getTime());
        $("#img5_close_btn").show();
	}else{
        $("#img5_close_btn").hide();
	}
}
function showwhat(){
	$("#financial").show();
}

</script>
</head>
<body>
	<section id="uploadNoData" class="error-block" style="display:none">
		<div class="error-for-message">
			<div class="error-content ttb-result-list terms">
				<p id="error-info0" class="error-info" style="width: 100%;text-align: center;">您尚未上傳任何資料，請重新選擇。</p>
			</div>
			<input type="button" id="" value="<spring:message code='LB.X2019' />" class="ttb-button btn-flat-orange " onclick=" $('#uploadNoData').hide();"/>
		</div>
	</section>
	<section id="financial" class="error-block" style="display:none">
		<div class="error-for-message">
			<!-- 財力證明包含哪些？ -->
			<p class="error-title"><spring:message code="LB.X2015" /></p>
			<div class="error-content ttb-result-list terms">
				<ul class="">
					<!-- 近一年各類扣繳憑單 -->
					<li data-num="１."><spring:message code="LB.X2016" /></li>
					<!-- 近6個月薪轉存褶明細含封面 -->
					<li data-num="２."><spring:message code="LB.X2017" /></li>
					<!-- 近一年綜合所得稅結算申報書(或附回執聯之二維條碼申報或網路申報書)加附繳款書。 -->
					<li data-num="３."><spring:message code="LB.X2018" /></li>
				</ul>
			</div>
			<input type="button" id="" value="<spring:message code='LB.X2019' />" class="ttb-button btn-flat-orange " onclick=" $('#financial').hide();"/>
		</div>
	</section>
<!-- header -->
<header>
	<%@ include file="../index/header_logout.jsp"%>
</header>	
 
	<!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<!--信用卡 -->
			<li class="ttb-breadcrumb-item"><spring:message code= "LB.Credit_Card" /></a></li>
			<!--信用卡補上傳資料 -->
			<li class="ttb-breadcrumb-item"><a href="#"><spring:message code= "LB.onlineapply_creditcard_progress_TITLE" /></a></li>
		</ol>
	</nav>
	
	<div class="content row">
		<!-- 主頁內容  -->
	<main class="col-12">
		<!--主頁內容-->
		<section id="main-content" class="container">
			<h2><spring:message code= "LB.onlineapply_creditcard_progress_TITLE" /></h2>
			<div id="step-bar">
				<ul>
					<li class="finished"><spring:message code="LB.D0851" /></li><!-- 身份驗證 -->
					<li class="finished"><spring:message code="LB.onlineapply_creditcard_progress_OTP" /></li><!-- OTP驗證 -->
					<li class="finished"><spring:message code="LB.D0359" /></li><!-- 查詢結果 -->
					<li class="active"><spring:message code="LB.onlineapply_creditcard_progress_UploadDocuments" /></li><!-- 上傳證件 -->
					<li class=""><spring:message code="LB.Confirm_data" /></li><!-- 確認資料 -->
					<li class=""><spring:message code="LB.onlineapply_creditcard_progress_COMPLETE" /></li><!-- 完成補件 -->
				</ul>
			</div>	
			<form method="post" id="formId" enctype="multipart/form-data">
				<input type="hidden" name="FILE1" id="FILE1" value="${result_data.data.FILE1}"/>
				<input type="hidden" name="FILE2" id="FILE2" value="${result_data.data.FILE2}"/>
				<input type="hidden" name="FILE3" id="FILE3" value="${result_data.data.FILE3}"/>
				<input type="hidden" name="FILE4" id="FILE4" value="${result_data.data.FILE4}"/>
				<input type="hidden" name="FILE5" id="FILE5" value="${result_data.data.FILE5}"/>
				<input type="hidden" name="filetime" id="filetime" value="${result_data.data.filetime}"/>
				

				<div class="main-content-block row">
					<div class="col-12">
						<div class="ttb-input-block">
							<div class="ttb-message">
								<!-- 上傳證件-->
                                <p><spring:message code="LB.onlineapply_creditcard_progress_UploadDocuments" /></p>
                            </div>						
							<c:if test = "${sessionScope.pflg == '1'||sessionScope.pflg == '3'}">
		                        <div class="classification-block">
		                        	<!-- 身分證資料 -->
		                        	<p><spring:message code="LB.X2021" /></p>
		                       	</div>
		                       	<!-- 請上傳您的身分證正背面。 -->
		                       	<!-- 圖片格式僅允許 -->
		                        <p class="form-description"><spring:message code="LB.X2022" /><br /><spring:message code="LB.D1101"/>：<br />- <spring:message code="LB.X2023" />：JPG (JPEG), PNG <br />- <spring:message code="LB.D0115" /></p>
		                        <div class="photo-block">
			                        <div>
		                              	<img src="${__ctx}/img/upload_id_front.svg" id="img1" />
		    	                            <a id="img1_close_btn" class="photo-close-btn" onClick="deleteUpLoad('#img1', '#FILE1', 'upload_id_front')"></a>
											<!-- 身分證正面圖片 -->
		                                    <p><spring:message code="LB.D0111"/></p>
		  	                        </div>
		                            <div>
										<!-- 身分證背面圖片 -->
		                               	<img src="${__ctx}/img/upload_id_back.svg" id="img2" />
			                                <a id="img2_close_btn" class="photo-close-btn" onClick="deleteUpLoad('#img2', '#FILE2', 'upload_id_back')"></a>
		                                    <p><spring:message code="LB.D0118"/></p>
		    		                </div>
									<input type="file" name="file1" id="file1"
										style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
									<input type="file" name="file2" id="file2"
										style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
		            		    </div>
	            		    </c:if>
	            		    <c:if test = "${sessionScope.pflg == '2'||sessionScope.pflg == '3'}">
		                        <div class="classification-block">
		                        	<!-- 財力證明文件 -->
		                        	<p><spring:message code="LB.X2024"/></p>
		                        </div>
		                        <p class="form-description">
		                        	<!-- 請上傳財力證明文件。 -->
		                        	<spring:message code="LB.X2025"/><br />
		                        	<spring:message code="LB.D1101"/>：<br /> 
		                        	- <spring:message code="LB.X2015" /> <img src="${__ctx}/img/icon-13.svg" onclick="showwhat()" /><br>
		                        	- <spring:message code="LB.X2023" />：JPG (JPEG), PNG
		                        	<br />- <spring:message code="LB.D0115" />
		                        </p>
	 	                        <div class="photo-block" style="justify-content: flex-start; margin-left: 1rem;">
									<ol style="list-style-type: decimal;">
										<li>
											<div>
					                          	<img src="${__ctx}/img/upload_financial_front.svg" id="img3" />
				                                    <a id="img3_close_btn" class="photo-close-btn" onClick="deleteUpLoad('#img3', '#FILE3', 'upload_financial_front')"></a>
													<!--財力證明文件 -->
				                                    <p></p>
				                          	</div>
											<input type="file" name="file3" id="file3"
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
				                        </li>
				                        <li>
				                        	<div>
				                                <img src="${__ctx}/img/upload_financial_front.svg" id="img4" />
				                                    <a id="img4_close_btn" class="photo-close-btn" onClick="deleteUpLoad('#img4', '#FILE4', 'upload_financial_front')"></a>
													<!--財力證明文件 -->
				                                    <p></p>
				                          	</div>
											<input type="file" name="file4" id="file4"
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
				                        </li>
				                        <li>
				                        	<div>
				                               	<img src="${__ctx}/img/upload_financial_front.svg" id="img5" />
				                                    <a id="img5_close_btn" class="photo-close-btn" onClick="deleteUpLoad('#img5', '#FILE5', 'upload_financial_front')"></a>
													<!--財力證明文件 -->
				                                    <p></p>
				                            </div>
											<input type="file" name="file5" id="file5"
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</li>
									</ol>
		                    	</div>
		                    </c:if>
		                    
						</div>
                       	<input type="button" id="CMBACK" value="<spring:message code="LB.D0171"/>" class="ttb-button btn-flat-gray" onClick="window.close();"/>
                        <input id="CMSUBMIT" name="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code= "LB.X0080" />" />
					</div>
				</div>
			</form>
		</section>
	</main>
</div>
<%@ include file="../index/footer.jsp"%>
</body>
</html>