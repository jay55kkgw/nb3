<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
  <title>${jspTitle}</title>
  <script type="text/javascript">
    $(document).ready(function () {
      window.print();
    });
  </script>
</head>

<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
  <br /><br />
  <div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif" /></div>
  <br /><br />
  <div style="text-align:center">
    <font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
  </div>
  <br />
  <div style="text-align:center">
    ${MsgName}
  </div>
  <br />
  <table class="print">
    <tr>
      <td><spring:message code="LB.D0507"/></td>
      <td>
        ${COMMCCY2}
        ${str_CommAmt}
      </td>
    </tr>
    <tr>
      <td><spring:message code="LB.W0334"/></td>
      <td>
        ${COMMCCY2}
        ${str_CommAmt}
      </td>
    </tr>

    <tr>
      <td><spring:message code="LB.Exchange_rate"/></td>
      <td>
        ${RATE }
      </td>
    </tr>

    <tr>
      <td><spring:message code="LB.W0150"/></td>
      <td>
        ${ORGCCY}
        ${str_OrgAmt}
        <spring:message code="LB.Dollar_1"/>
      </td>
    </tr>

    <tr>
      <td><spring:message code="LB.W0330"/></td>
      <td>
        ${PMTCCY}
        ${str_PmtAmt}
        <spring:message code="LB.Dollar_1"/>
      </td>
    </tr>
    <tr>
      <td><spring:message code="LB.W0329"/></td>
      <td>
        ${BENACC}
      </td>
    </tr>

  </table>
  <br>
  <br>
  <div class="text-left">
    <!-- 		說明： -->
    <spring:message code="LB.Description_of_page" />
    <ol class="list-decimal text-left">
    <!-- 電子簽章:為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章(載具i-key)拔除並登出系統。 -->
      <li><spring:message code= "LB.F_Remittances_Payments_P5_D1" /></li>
    </ol>
  </div>
</body>

</html>