<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>

<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//開始查詢資料並完成畫面
	setTimeout("init()",20);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	setTimeout("initDataTable()",100);
	
	var mail = '${sessionScope.dpmyemail}';
// 	mail='';
	
	if( ''== mail){
		errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.X2637' />"], 
				'<spring:message code= "LB.Confirm" />', 
				null
			);
		$("#errorBtn1").click(function(e) {
			$('#error-block').hide();
		});
	}
	
	shwd_prompt_init(false);

});
function init(){
	//initFootable();
}

function checkShortTrade(CDNO,TRANSCODE,CRY,FUNDAMT,UNIT,INDEX,SHORTTRADE,SHORTTUNIT,FUNDLNAME,TRADEDATE,FUNDACN,FEE_TYPE
						,TYPE10,FUNDT,C30RAMT,Y30RAMT,C302RAMT,Y302RAMT,C30AMT,Y30AMT,C302AMT,Y302AMT){
	$("#CDNO").val(CDNO);
	$("#TRANSCODE").val(TRANSCODE);
	$("#CRY").val(CRY);
  	$("#OFUNDAMT").val(FUNDAMT);
	$("#UNIT").val(UNIT);
  	$("#SHORTTRADE").val(SHORTTRADE);
  	$("#SHORTTUNIT").val(SHORTTUNIT);
	$("#FUNDLNAME").val(FUNDLNAME);
	$("#TRADEDATE").val(TRADEDATE);
	$("#FEE_TYPE").val(FEE_TYPE);
	$("#TYPE10").val(TYPE10);
	$("#FUNDT").val(FUNDT);
	$("#C30RAMT").val(C30RAMT);
	$("#Y30RAMT").val(Y30RAMT);
	$("#C302RAMT").val(C302RAMT);
	$("#Y302RAMT").val(Y302RAMT);
	$("#C30AMT").val(C30AMT);
	$("#Y30AMT").val(Y30AMT);
	$("#C302AMT").val(C302AMT);
	$("#Y302AMT").val(Y302AMT);
	
	if(CRY == "NTD"){
		$("#FUNDACN").val($("#ACN3").val());
	}
	else{
		$("#FUNDACN").val($("#ACN4").val());
	}
	
	var BILLSENDMODE = $("input[name=BILLSENDMODE_"+ INDEX +"]:checked").val();
	
	if(BILLSENDMODE == null){
		//alert("<spring:message code= "LB.Alert116" />");
		errorBlock(
				null, 
				null,
				["<spring:message code= 'LB.Alert116' />"], 
				'<spring:message code= "LB.Quit" />', 
				null
		);
		return;
	}
	$("#BILLSENDMODE").val($("input[name=BILLSENDMODE_"+ INDEX +"]:checked").val());
	
	$("#formID").submit();
}  

//營業時間
function workTime(){
	errorBlock(
			["<spring:message code= 'LB.X2371' />"], 
			["1.<spring:message code= 'LB.X2372' />","2.<spring:message code= 'LB.X2373' />"],
			null,
			'<spring:message code= "LB.Quit" />', 
			null
		);
}
</script>
</head>
<body>
    <c:set var="SHWD" value="${bs.data.SHWD}"></c:set>
    <%@ include file="fund_shwd.jsp"%>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
    
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金    -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0901" /></li>
    <!-- 基金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1064" /></li>
    <!-- 贖回交易     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1133" /></li>
		</ol>
	</nav>

	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
				<h2>
				<spring:message code="LB.W0929"/>
				</h2>
				<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
			
					<form id="formID" action="${__ctx}/FUND/REDEEM/fund_redeem_select" method="post">
						<input type="hidden" name="ADOPID" value="C024"/>
						<input type="hidden" name="CUSIDN" value="${bs.data.CUSIDN}"/>
  						<input type="hidden" name="CUSNAME" value="${bs.data.NAME}"/>
  						<input type="hidden" id="CDNO" name="CDNO"/>
						<input type="hidden" id="TRANSCODE" name="TRANSCODE"/>
						<input type="hidden" id="CRY" name="CRY"/>
						<input type="hidden" id="OFUNDAMT" name="OFUNDAMT"/>
						<input type="hidden" id="UNIT" name="UNIT"/>
						<input type="hidden" id="BILLSENDMODE" name="BILLSENDMODE"/>
						<input type="hidden" id="SHORTTRADE" name="SHORTTRADE"/>
						<input type="hidden" id="SHORTTUNIT" name="SHORTTUNIT"/>
						<input type="hidden" id="FUNDLNAME" name="FUNDLNAME"/>
						<input type="hidden" name="FDINVTYPE" value="${bs.data.FDINVTYPE}"/>
						<input type="hidden" id="TRADEDATE" name="TRADEDATE"/>
						<input type="hidden" id="FUNDACN" name="FUNDACN"/>
						<input type="hidden" id="FEE_TYPE" name="FEE_TYPE"/>
						<input type="hidden" id="ACN3" name="ACN3" value="${bs.data.ACN3}"/>
						<input type="hidden" id="ACN4" name="ACN4" value="${bs.data.ACN4}"/>
						<input type="hidden" id="SHWD" name="SHWD" value="${bs.data.SHWD}">
						<input type="hidden" id="TYPE10" name="TYPE10"/>
						<input type="hidden" id="FUNDT" name="FUNDT"/>
						<input type="hidden" id="C30RAMT" name="C30RAMT"/>
						<input type="hidden" id="Y30RAMT" name="Y30RAMT"/>
						<input type="hidden" id="C302RAMT" name="C302RAMT"/>
						<input type="hidden" id="Y302RAMT" name="Y302RAMT"/>
						<input type="hidden" id="C30AMT" name="C30AMT"/>
						<input type="hidden" id="Y30AMT" name="Y30AMT"/>
						<input type="hidden" id="C302AMT" name="C302AMT"/>
						<input type="hidden" id="Y302AMT" name="Y302AMT"/>
				<div class="main-content-block row">
						<div class="col-12 tab-content">
							<ul class="ttb-result-list">
								<li>
									<!--資料總數-->
									<h3>
									    <spring:message code="LB.Total_records"/>
									</h3>
									<p>
										${bs.data.dataCount}
										<spring:message code="LB.Rows"/>
									</p>
								</li>
										<li>
									<!--身分證字號/統一編號-->
									<h3>
									    <spring:message code="LB.Id_no"/>
									</h3>
									<p>
										${bs.data.hiddenCUSIDN}
									</p>
								</li>
										<li>
									<!--姓名-->
									<h3>
									    <spring:message code="LB.Name"/>
									</h3>
									<p>
										${bs.data.hiddenNAME}
									</p>
								</li>
							</ul>
							<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
								<thead>
								<!-- 
									<tr>
										<th><spring:message code="LB.Id_no"/></th>
								 		<th colspan="5">${bs.data.hiddenCUSIDN}</th>
									</tr>
									<tr>
										<th><spring:message code="LB.Name"/></th>
								 		<th colspan="5">${bs.data.hiddenNAME}</th>
									</tr>
							 -->
									<tr>
										<th><spring:message code="LB.W0944"/></th>
								 		<th><spring:message code="LB.W0025"/></th>
										<th><spring:message code="LB.W0026"/></th>
										<th><spring:message code="LB.W0027"/></th>
										<th><spring:message code="LB.W1140"/></th>
				 						<th><spring:message code="LB.X0407" /></th>
									</tr>
								</thead>
								<tbody>
								<c:if test="${empty bs.data.fundRows}">
								<tr style="display:none;"></tr>
								</c:if>
								<c:if test="${not empty bs.data.fundRows}">
								<c:forEach var="dataMap" items="${bs.data.fundRows}" varStatus="status">
										<tr>
											<td class="text-center">${dataMap.hiddenCDNO}</td>
											<td class="text-center">（${dataMap.TRANSCODE}）${dataMap.FUNDLNAME}
												<c:if test="${dataMap.FEE_TYPE == 'A'}"> <%-- 後收型基金 --%>
													<br>
													<strong><font color="red"><spring:message code="LB.X2497_1"/></font></strong>
												</c:if>
												<c:if test="${dataMap.FUNDT == '1'}"> <%-- 目標到期債基金 --%>
													<br>
													<strong><font color="red"><spring:message code="LB.X2497_2"/></font></strong>
												</c:if>
											</td>
											<td class="text-center">${dataMap.ADCCYNAME}<br/>${dataMap.formatFUNDAMT}</td>
											<td class="text-center">${dataMap.formatUNIT}</td>
											<td class="text-center">
											<c:if test="${dataMap.FEE_TYPE == 'B'}">
												<span class="input-block">
												<div class="ttb-input">
												<label class="radio-block"><spring:message code="LB.All"/>
	                                            <input type="radio" value="1"  name="BILLSENDMODE_${status.index}">
	                                            <span class="ttb-radio"></span>
	                                            </label>
	                                            <br/>
	                                            <label class="radio-block" ><spring:message code="LB.X0383" />
	                                            <input type="radio" value="2" name="BILLSENDMODE_${status.index}" >
	                                            <span class="ttb-radio"></span>
	                                            </label>
												</td class="text-center">						                                                                                                           
												<td class="text-center"><input type="button" value="<spring:message code="LB.Confirm"/>"
												onclick="checkShortTrade('${dataMap.CDNO}',
	                                      		'${dataMap.TRANSCODE}',
	                                      		'${dataMap.CRY}',
	                                      		'${dataMap.FUNDAMT}',
	                                      		'${dataMap.UNIT}',
	                                      		'${status.index}',
	                                      		'00000000',
	                                      		'${dataMap.SHORTTUNIT}',
	                                      		'${dataMap.FUNDLNAME}',
	                                      		'${dataMap.TRADEDATE}',
	                                      		'${dataMap.FUNDACN}',
	                                      		'${dataMap.FEE_TYPE}',
	                                      		'${dataMap.TYPE10}',
	                                      		'${dataMap.FUNDT}',
	                                      		'${dataMap.C30RAMT}',
	                                      		'${dataMap.Y30RAMT}',
	                                      		'${dataMap.C302RAMT}',
	                                      		'${dataMap.Y302RAMT}',
	                                      		'${dataMap.C30AMT}',
	                                      		'${dataMap.Y30AMT}',
	                                      		'${dataMap.C302AMT}',
	                                      		'${dataMap.Y302AMT}')" class="ttb-sm-btn btn-flat-orange"/>
	                                      		</td>
                                      		</c:if>
                                      		<c:if test="${dataMap.FEE_TYPE == 'A'}">
                                      			<span class="input-block">
												<div class="ttb-input">
												<label class="radio-block"><spring:message code="LB.All"/>
	                                            <input type="radio" value="1"  name="BILLSENDMODE_${status.index}">
	                                            <span class="ttb-radio"></span>
	                                            </label>
	                                            </td class="text-center">						                                                                                                           
												<td class="text-center"><input type="button" value="<spring:message code="LB.Confirm"/>"
												onclick="checkShortTrade('${dataMap.CDNO}',
	                                      		'${dataMap.TRANSCODE}',
	                                      		'${dataMap.CRY}',
	                                      		'${dataMap.FUNDAMT}',
	                                      		'${dataMap.UNIT}',
	                                      		'${status.index}',
	                                      		'00000000',
	                                      		'${dataMap.SHORTTUNIT}',
	                                      		'${dataMap.FUNDLNAME}',
	                                      		'${dataMap.TRADEDATE}',
	                                      		'${dataMap.FUNDACN}',
	                                      		'${dataMap.FEE_TYPE}')" class="ttb-sm-btn btn-flat-orange"/>
                                      		</c:if>
										</tr>
									</c:forEach>
								</c:if>		
								</tbody>
							</table>
						</div>
					</form>
				</div>
				<div class="text-left">
					
					<ol class="list-decimal text-left description-list">
					<p><spring:message code="LB.Description_of_page"/></p>
						<li><span><spring:message code="LB.Fund_Redeem_Data_P1_D1"/></span></li>
						<li><span><spring:message code="LB.Query_Fund_Transfer_Data_P1_D2-1"/><a href="javascript:void(0)" onclick="workTime()"><spring:message code="LB.Query_Fund_Transfer_Data_P1_D2-2"/></a></span></li>
						<li><span><spring:message code="LB.Query_Fund_Transfer_Data_P1_D3"/></span></li>
						<ul class="description-list">
							<li>(1)<spring:message code="LB.Query_Fund_Transfer_Data_P1_D4"/></li>
							<li>(2)<spring:message code="LB.Query_Fund_Transfer_Data_P1_D5"/></li>
							<li>(3)<spring:message code="LB.Query_Fund_Transfer_Data_P1_D6"/></li>
							<li>(4)<spring:message code="LB.Query_Fund_Transfer_Data_P1_D7"/></li>
						</ul>
						<li><span><spring:message code="LB.Query_Fund_Transfer_Data_P1_D8"/></span></li>
						<li><span><spring:message code="LB.Fund_Redeem_Data_P1_D3-1"/><a href="${__ctx}/CUSTOMER/SERVICE/redeem_table" target="_blank"><spring:message code="LB.Fund_Redeem_Data_P1_D3-2"/></a></span></li>
					</ol>
				</div>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>