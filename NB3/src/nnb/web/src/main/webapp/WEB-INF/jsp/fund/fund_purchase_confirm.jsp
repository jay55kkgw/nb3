<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);

	$("#formID").validationEngine({
		binded: false,
		promptPosition: "inline"
	});
	
	$("#CMSUBMIT").click(function(e){
		if($("#riskConfirmCheckBox").prop("checked") == false || $("#riskConfirmCheckBox2").prop("checked") == false){
			errorBlock(
					null, 
					null,
					['<spring:message code="LB.Alert092" />'], 
					'<spring:message code="LB.Quit" />', 
					null
			);
			return;
		}
		if($("#CMPWD").val().length == 0 ) {
			errorBlock(
					null, 
					null,
					['<spring:message code="LB.Please_enter_the_transaction_password" />'], 
					'<spring:message code="LB.Quit" />', 
					null
			);
			return;
		}
		
		e = e || window.event;
		if(!$('#formID').validationEngine('validate')){
			e.preventDefault();
		}
		else{
			$("#formID").validationEngine('detach');
			$("#CMPASSWORD").val($("#CMPWD").val());
			
			var PINNEW = pin_encrypt($("#CMPASSWORD").val());
			$("#PINNEW").val(PINNEW);
			$("#CMPASSWORD").val("");
			$("#CMPWD").prop('disabled',true);
			initBlockUI();
			$("#formID").attr("action","${__ctx}/FUND/PURCHASE/fund_purchase_result");
			$("#formID").submit();
		}
	});
	$("#resetButton").click(function(){
		$("#CMPWD").val("");
	});
	$("#cancelButton").click(function(){
		$("#formID").validationEngine('detach');
		$("#formID").attr("action","${__ctx}/FUND/PURCHASE/fund_purchase_select");
		$("#formID").submit();
	});
});
</script>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 基金     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Funds" /></li>
    <!-- 基金交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1064" /></li>
    <!-- 單筆申購     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1065" /></li>
		</ol>
	</nav>

	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.W1065" /></h2>
				<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
					<form id="formID" method="post">
						<input type="hidden" name="ADOPID" value="C021"/>
						<input type="hidden" id="CMPASSWORD" name="CMPASSWORD"/>
						<input type="hidden" id="PINNEW" name="PINNEW"/>
						<input type="hidden" name="TYPE" value="${TYPE}"/>
						<input type="hidden" name="TRANSCODE" value="${TRANSCODE}"/>
						<input type="hidden" name="COUNTRYTYPE" value="${COUNTRYTYPE}"/>
						<input type="hidden" name="TRADEDATE" value="${TRADEDATE}"/>
						<input type="hidden" name="AMT3" value="${AMT3}"/>
						<input type="hidden" name="FCA2" value="${FCA2}"/>
						<input type="hidden" name="AMT5" value="${AMT5}"/>
						<input type="hidden" name="OUTACN" value="${OUTACN}"/>
						<input type="hidden" name="INTSACN" value="${INTSACN}"/>
						<input type="hidden" name="BILLSENDMODE" value="${BILLSENDMODE}"/>
						<input type="hidden" name="FCAFEE" value="${FCAFEE}"/>
						<input type="hidden" name="SSLTXNO" value="${SSLTXNO}"/>
						<input type="hidden" name="PAYDAY1" value="${PAYDAY1}"/>
						<input type="hidden" name="PAYDAY2" value="${PAYDAY2}"/>
						<input type="hidden" name="PAYDAY3" value="${PAYDAY3}"/>
						<input type="hidden" name="BRHCOD" value="${BRHCOD}"/>
						<input type="hidden" name="CUTTYPE" value="${CUTTYPE}"/>
						<input type="hidden" name="CRY1" value="${CRY1}"/>
						<input type="hidden" name="HTELPHONE" value="${HTELPHONE}"/>
						<input type="hidden" name="DBDATE" value="${DBDATE}"/>
						<input type="hidden" name="SALESNO" value="${SALESNO}"/>
						<input type="hidden" name="STOP" value="${STOP}"/>
						<input type="hidden" name="YIELD" value="${YIELD}"/>
						<input type="hidden" name="PAYDAY4" value="${PAYDAY4}"/>
						<input type="hidden" name="PAYDAY5" value="${PAYDAY5}"/>
						<input type="hidden" name="PAYDAY6" value="${PAYDAY6}"/>
						<input type="hidden" name="MIP" value="${MIP}"/>
						<input type="hidden" name="PRO" value="${PRO}"/>
						<input type="hidden" name="RSKATT" value="${RSKATT}"/>
						<input type="hidden" name="RRSK" value="${RRSK}"/>
						<input type="hidden" name="PAYDAY7" value="${PAYDAY7}"/>
						<input type="hidden" name="PAYDAY8" value="${PAYDAY8}"/>
						<input type="hidden" name="PAYDAY9" value="${PAYDAY9}"/>
						<input type="hidden" name="FDINVTYPEChinese" value="${FDINVTYPEChinese}"/>
						<input type="hidden" name="TYPEChinese" value="${TYPEChinese}"/>
						<input type="hidden" name="FUNDLNAME" value="${FUNDLNAME}"/>
						<input type="hidden" name="RISK" value="${RISK}"/>
						<input type="hidden" name="SHWD" value="${SHWD}"/>
                        <input type="hidden" name="XFLAG" value="${XFLAG}"/>
                        <input type="hidden" name="NUM" value="${NUM}"/>
                        <input type="hidden" name="FEE_TYPE" value="${FEE_TYPE}"/>
                        <input type="hidden" name="SLSNO" value="${SLSNO}"/>
                        <input type="hidden" name="KYCNO" value="${KYCNO}"/>
                        <input type="hidden" name="OFLAG" value="${OFLAG}"/>
                        
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<!-- 客戶投資屬性 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1067" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${FDINVTYPEChinese}</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 扣款方式 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.D0173" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${TYPEChinese}</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 身分證字號/統一編號 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.Id_no"/></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${hiddenCUSIDN}</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 姓名 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.Name"/></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${hiddenNAME}</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 基金名稱 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W0025" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>（${TRANSCODE}）${FUNDLNAME}</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 商品風險等級 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1073" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${RISK}</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 申購金額 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1074" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${ADCCYNAME} ${AMT3Format}</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 手續費率 -->
									<div class="ttb-input-item row">
	                                	<span class="input-title"><label>
	                                        <h4><spring:message code="LB.W1034" /></h4>
	                                    </label></span>
	                                	<span class="input-block">
	                                		<div class="ttb-input">
	                                        	<span>${FCAFEEFormat}％</span>
	                                		</div>
	                               		</span>
	                            	</div>
									<!-- 手續費 -->
									<div class="ttb-input-item row">
	                                	<span class="input-title"><label>
	                                        <h4><spring:message code="LB.D0507" /></h4>
	                                    </label></span>
	                                	<span class="input-block">
	                                		<div class="ttb-input">
	                                        	<span>${ADCCYNAME} ${FCA2Format}</span>
	                                		</div>
	                               		</span>
	                            	</div>
								<!-- 扣款金額 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1079" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${ADCCYNAME} ${AMT5Format}</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 扣款帳號 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W0702" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${HTELPHONE}</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 生效日期 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1080" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${TRADEDATEFormat}</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 停利通知設定 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1075" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${YIELDInteger}％</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 停損通知設定 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1076" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>- ${STOPInteger}％</span>
                                		</div>
                               		</span>
                            	</div>
								<!--交易機制-->
                            	<div class="ttb-input-item row">
                                	<span class="input-title">
                                    	<label>
                                        	<h4><spring:message code="LB.Transaction_security_mechanism" /></h4>
                                    	</label>
                                	</span>
                                	<span class="input-block" >
	                                    <div class="ttb-input">
                                       		<label class="radio-block"><spring:message	code="LB.SSL_password" />
                                            	<input type="radio" name="FGTXWAY" id="CMSSL" value="0" checked >
                                            	<span class="ttb-radio"></span>
                                        	</label>
                                    	</div>
										<div class="ttb-input">
											<input type="password" class="text-input" name="CMPWD" id="CMPWD" autocomplete="off" size="8" maxlength="8" value="">
										</div>
                            	    </span>
                            	    <c:if test="${FEE_TYPE == 'A'}" >
	                            	    <div class="ttb-input-item row">
	                            	    <span class="input-title"></span>
	                            	    <span class="input-block" >
		                                    <div class="ttb-input">
		                            	    	<label class="check-block">
			                                       	<input type="checkbox" id="riskConfirmCheckBox"/><strong><spring:message code= "LB.X2493" /><font color="red"><spring:message code= "LB.X2494" /></font></strong>
													<span class="ttb-check"></span>
												</label>
											</div>
										</span>
										</div>
                                    </c:if>
                                    <c:if test="${FUNDT == '1'}">
                                    	<div class="ttb-input-item row">
	                            	    <span class="input-title"></span>
	                            	    <span class="input-block" >
		                                    <div class="ttb-input">
	                            	    	<label class="check-block">
	                                       	<input type="checkbox" id="riskConfirmCheckBox2"/><font color="red"><spring:message code= "LB.X2493_1" /><spring:message code= "LB.X2610" /></font>
											<span class="ttb-check"></span>
											</label>
											</div>
										</span>
										</div>
                                    </c:if>
                            	</div>
							</div>
							<input type="button" id="cancelButton" value="<spring:message code="LB.Back_to_function_home_page" />" class="ttb-button btn-flat-gray"/>
							<input type="button" id="resetButton" value="<spring:message code="LB.Re_enter"/>" class="ttb-button btn-flat-gray"/>
							<input type="button" id="CMSUBMIT" value="<spring:message code="LB.Confirm"/>" class="ttb-button btn-flat-orange"/>
						</div>
					</div>
				</form>
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>
					<spring:message code="LB.fund_purchase_P2_D1" />：
					<li><span><spring:message code="LB.fund_purchase_P2_D2-1" /><br/>「<spring:message code="LB.fund_purchase_P2_D2-2" />」<spring:message code="LB.fund_purchase_P2_D2-3" />→「<spring:message code="LB.fund_purchase_P2_D2-4" />」→「<spring:message code="LB.fund_purchase_P2_D2-5" />」</span></li>
					<li><span><spring:message code="LB.fund_purchase_P2_D3-1" />（https://www.tbb.com.tw/）<br/>「<spring:message code="LB.fund_purchase_P2_D3-2" />」→「<spring:message code="LB.fund_purchase_P2_D3-3" />」→「<spring:message code="LB.fund_purchase_P2_D3-4" />」</span></li>
				</ol>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>