<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ include file="../__import_js_u2.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
<br/><br/>
<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
<br/><br/><br/>
<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
<br/><br/><br/>
<div style="text-align:center">
<%-- 	<label><img src="${__ctx}/img/icon-success.svg"></label> --%>
	<br/>
	<label style="text-align:center">${message}</label>
</div>
<br/><br/>
<c:if test="${result ==true}">
	<table class="print">
		<!-- 								資料時間 -->
		<tr>
			<td style="text-align:center"><spring:message code="LB.Trading_time" /></td>
			<td>${CMQTIME}</td>
		</tr>
		<c:if test="${FGTXDATE != '1'}">
		<tr>
			<td style="text-align:center"><spring:message code="LB.Transfer_date" /></td>
			<td>${SCHDATE}</td>
		</tr>
		</c:if>
		<tr>
			<td style="text-align:center"><spring:message code="LB.Payers_account_no" /></td>
			<td>${OUTACN}</td>
		</tr>
		<tr>
			<td style="text-align:center">
				<spring:message code="LB.Payees_account_no" />
			</td>
			<td>${INTSACN}</td>
		</tr>
		<tr>
			<td style="text-align:center"><spring:message code="LB.Amount" /></td>
			<td>
				<spring:message code="LB.NTD" />　${AMOUNT}　<spring:message code="LB.Dollar" />
		    </td>
		</tr>
		<c:if test="${not empty FEE }">
		<tr>
			<td style="text-align:center">
				<spring:message code="LB.X2423" />
			</td>
			<td><spring:message code="LB.NTD" /> ${FEE} <spring:message code="LB.Dollar" /></br>
				<c:if test="${FEE eq '0' || FEE eq '000' || FEE eq '0.00'}">
						<span style="color:red"><spring:message code="LB.X2422" /></span>
				</c:if>	
			</td>
		</tr>
		</c:if>
		<c:if test="${HEADER eq 'N070' }">
		<tr>
			<td style="text-align:center">
				<spring:message code="LB.Payees_postscript" />
			</td>
			<td>${INPCST}</td>
		</tr>
		</c:if>
		<tr>
			<td style="text-align:center"><spring:message code="LB.Transfer_note" /></td>
			<td>${CMTRMEMO}</td>
		</tr>
		<c:if test="${FGTXDATE == '1'}">
			<c:if test="${O_TOTBAL != '0.00'}">
			<tr>
				<td style="text-align:center"><spring:message code="LB.Payers_account_balance" /></td>
				<td><spring:message code="LB.NTD" />${O_TOTBAL}<spring:message code="LB.Dollar" /></td>
			</tr>
			<!-- 					            備註                    -->
			<tr>
				<td style="text-align:center"><spring:message code="LB.Payers_available_balance" /></td>
				<td><spring:message code="LB.NTD" />${O_AVLBAL }<spring:message code="LB.Dollar" /></td>
			</tr>
			</c:if>
			<c:if test="${I_TOTBAL != '0.00'}">
			<tr>
				<td style="text-align:center"><spring:message code="LB.Payees_account_balance" /></td>
				<td>
					<spring:message code="LB.NTD" />
			       ${I_TOTBAL }
			       <spring:message code="LB.Dollar" />
			    </td>
			</tr>
			<tr>
				<td style="text-align:center"><spring:message code="LB.Payees_available_balance" /></td>
				<td>
					<spring:message code="LB.NTD" />
			        ${I_AVLBAL }
			        <spring:message code="LB.Dollar" />
			    </td>
			</tr>
			</c:if>
		</c:if>	
	</table>
</c:if>
<br/><br/>
<c:if test="${FGTXDATE == '1'}">
	<div class="text-left">
		<spring:message code="LB.Description_of_page" /> ：
		<ol class="list-decimal text-left">
			<li><spring:message code="LB.Transfer_P3_D1" /></li>
			<li><spring:message code="LB.Transfer_P3_D2" /></li>
		</ol>
	</div>
</c:if>
<c:if test="${FGTXDATE != '1'}">
		<div class="text-left">
			<spring:message code="LB.Description_of_page" />:
			<ol class="list-decimal text-left">
				<li><spring:message code="LB.Transfer_PP3_D1" /></li>
				<li><spring:message code="LB.Transfer_PP3_D2" /></li>							
			</ol>
		</div>
</c:if>
</body>
</html>