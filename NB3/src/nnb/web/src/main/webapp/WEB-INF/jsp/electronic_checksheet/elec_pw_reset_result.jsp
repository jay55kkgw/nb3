<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/checkutil.js"></script>
	
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 電子對帳單     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1396" /></li>
    <!-- 密碼重置     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0296" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<spring:message code="LB.D0302" /><!-- 電子帳單密碼重置 -->
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" action="" method="post">
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
							<h4 style="margin-top:10px;color: red;font-weight:bold;">
<%-- 								<spring:message code="${reset_result_data.data.BHRPResult}" /> --%>
								<c:if test="${reset_result_data.data.BHRPResult != null && !reset_result_data.data.BHRPResult.equals('')}">
									<spring:message code="${reset_result_data.data.BHRPResult}" />
									<c:if test="${reset_result_data.data.BHRPResult.equals('LB.Transaction_successful')}">
										<br>
										<spring:message code= "LB.X2393" /><spring:message code= "LB.Elec_Pw_Alter_P2_D1-2" />
									</c:if>
								</c:if>
							</h4>
							<div class="ttb-input-block">
								<!-- 申請項目區塊 -->
								<div class="ttb-input-item row">
									<!--申請項目  -->
									<span class="input-title">
										<label>
											<spring:message code="LB.D0169" /><!-- 申請項目 -->
										</label>
									</span>
									<span class="input-block">
										<p><spring:message code="LB.D0296" /><!-- 密碼重置 --></p>
									</span>
								</div>
							</div>
						</div>
					</div>
						<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /></p>
							<li><spring:message code= "LB.Elec_Pw_Reset_P2_D1" /><font style="color:red"><spring:message code= "LB.Elec_Pw_Reset_P2_D1-1" /></font><spring:message code= "LB.Elec_Pw_Reset_P2_D1-2" /></li>
							<li><spring:message code= "LB.Elec_Pw_Reset_P2_D2" /></li>
						</ol>
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
      
</body>

</html>