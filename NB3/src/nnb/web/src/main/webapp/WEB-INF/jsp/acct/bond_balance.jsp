<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {
		// 將.table變更為footable
		initFootable();
		init()
	});
	
	function init() {
		//$("#formId").validationEngine({binded: false,promptPosition: "inline"});
		//btn 
		$("#CMSUBMIT").click(function (e) {
			e = e || window.event;
			$("#formId").attr("action", "${__ctx}/NT/ACCT/BOND/bond_balance_result");
			$("#formId").submit();

		});
	} // init END
	
	//下拉式選單
 	function formReset() {
 		if ($('#actionBar').val()=="reEnter"){
	 		document.getElementById("formId").reset();
	 		$('#actionBar').val("");
 		}
	}
</script>
</head>
	<body>
		<!-- header     -->
		<header>
			<%@ include file="../index/header.jsp"%>
		</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 中央登錄債券查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0033" /></li>
    <!-- 餘額查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0034" /></li>
		</ol>
	</nav>



		<!-- menu、登出窗格 -->
		<div class="content row">
			<%@ include file="../index/menu.jsp"%>
			<!-- 	快速選單內容 -->
			<!-- content row END -->
			<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2><spring:message code="LB.W0034" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form id="formId" method="post" action="">
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">							
								<!-- 帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title"> 
										<label>
											<h4><spring:message code="LB.X0001" /></h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
										<!--帳號 -->
											<select class="custom-select select-input half-input" name="ACN1" id="ACN">
												<option value="ALL">
													<spring:message code="LB.All" />
												</option>
												<c:forEach var="dataList" items="${username_alter_result.data.REC}">
													<option> ${dataList.ACN}</option>
												</c:forEach>
											</select>
										</div>
									</span>
								</div>				
							</div>
							<!-- 重新輸入-->
	                        <input type="reset" name="CMRESET" id="CMRESET" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter"/>"/>
							<!-- 網頁顯示-->
							<input type="button" name="CMSUBMIT" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Display_as_web_page"/>"/>
						</div>
					</div>
				</form>
				</section>
			</main>
			<!-- 		main-content END -->
			<!-- 	content row END -->
		</div>
		<%@ include file="../index/footer.jsp"%>
	</body>
</html>