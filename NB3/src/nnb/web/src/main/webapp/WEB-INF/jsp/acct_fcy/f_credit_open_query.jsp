<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>

	<script type="text/javascript">
		$(document).ready(function () {
			init();
		});
		function init() {
			$("#formId").validationEngine({binded: false,promptPosition: "inline"});
			//btn 
			$("#CMSUBMIT").click(function (e) {
				console.log($("input[name*=FXLCCTYPE]:checked").val());
				window.location.href =  "./f_credit_open_query_" + $("input[name*=FXLCCTYPE]:checked").val();
			});
		} // init END
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 帳戶查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Account_Inquiry" /></li>
    <!-- 開狀查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0098" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!--匯入匯款查詢(未輸入) -->
					<!-- <spring:message code="LB.NTD_Demand_Deposit_Detail" /> -->
					<spring:message code="LB.W0098" />
				</h2>
				
				<form id="formId" method="post">
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<!-- 信用狀性質 -->
											<h4><spring:message code="LB.W0099" /></h4>
										</label>
									</span>
									<span class="input-block">
										<!-- 商業信用狀 -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.W0100" />
												<input type="radio" name="FXLCCTYPE" id="DATE" value="date" checked />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!--  擔保信用狀/保障函 -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.W0101" />
												<input type="radio" name="FXLCCTYPE" id="GUARANTEE" value="guarantee" />
												<span class="ttb-radio"></span>
											</label>
										</div>
									</span>
								</div>
							</div>
							<!-- 網頁顯示 button-->
							<!--網頁顯示 -->
							<spring:message code="LB.Display_as_web_page" var="cmSubmit"></spring:message>
							<input type="button" name="CMSUBMIT" id="CMSUBMIT" value="<spring:message code="LB.Confirm" />" class="ttb-button btn-flat-orange">
						</div>
					</div>
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>