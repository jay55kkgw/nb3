<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="page4" class="card-detail-block">
	<div class="card-center-block">
		<!-- Q1 -->
		<div class="ttb-pup-block" role="tab">
			<a id="popup1-Q1" role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-41" aria-expanded="true"
				aria-controls="popup1-41">
				<div class="row">
					<span class="col-1">Q1</span>
					<div class="col-11">
						<span>使用貴銀行的網路銀行需要怎麼樣的電腦軟硬體設備？</span>
					</div>
				</div>
			</a>
			<div id="popup1-41" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>建議您使用以下規格，當然電腦硬體等級越高，處理速度會越快：</p>
							<p>硬體：電腦的CPU 1GHz，2GB以上的記憶體。</p>
							<p>軟體：</p>
							<ol>
								<li>一、Mac系統  可使用瀏覽器：Safari、Chrome、FireFox </li>
								<li>二、Window作業系統(Window7(含)以上)  可使用瀏覽器：IE 11(含)以上的版本、Chrome、FireFox、Edge</li>
							</ol>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>