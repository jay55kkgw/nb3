<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	
	<script type="text/JavaScript">
		$(document).ready(function() {
			// 到變更密碼頁
			$("#pageshow").click(function(){ 			
				initBlockUI();
				$("#formId").submit(); 
			});
		});
	   
	</script>
</head>
 <body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 密碼變更     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Change_Password" /></li>
		</ol>
	</nav>

	<!-- 快速選單及主頁內容 -->
	<!-- menu、登出窗格 --> 
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
		<main class="col-12">	
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.Change_Password" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
<!-- 				<div id="step-bar"> -->
<!-- 					<ul> -->
<!-- 						<li class="active">Step1</li> -->
<!-- 						<li class="">Step2</li> -->
<!-- 						<li class="">Step3</li> -->
<!-- 					</ul> -->
<!-- 				</div> -->
				<div class="main-content-block row">
					<div class="col-12 tab-content"> 						
						<form method="post" id="formId" action="${__ctx}/PERSONAL/SERVING/pw_alter_step">
							<div class="ttb-input-block">							
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.Option_item" /></h4><!-- 選項-->
										</label>
									</span> 
									<span class="input-block">
										<label class="radio-block">
											<input type="radio" name="ACRADIO" id="pw" value="pw" checked/>
						                	<spring:message code="LB.Change_Login_Password" /><!-- 簽入密碼變更-->
						                	<span class="ttb-radio"></span>
					                	</label>
					                	
					                	<br>
					                	
					                	<label class="radio-block">
						                	<input type="radio" name="ACRADIO" id="ssl" value="ssl"/>
						                	<spring:message code="LB.Change_SSL_Password" /><!-- 交易密碼變更-->
						                	<span class="ttb-radio"></span>
					                	</label>
					                	
					                	<br>
					                	
					                	<label class="radio-block">
						                	<input type="radio" name="ACRADIO" id="elec" value="elec"/>
						                	電子對帳單密碼變更<!-- 電子對帳單密碼變更-->
						                	<span class="ttb-radio"></span>
					                	</label>
									</span>
								</div>						
							</div>
						</form>
						<input id="pageshow" name="pageshow" type="submit" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />						
					</div>
				</div>
			</section>
			<!-- main-content END -->
		</main>
	</div>
	<!-- content row END -->
    <%@ include file="../index/footer.jsp"%>
</body>
</html>
