<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js_u2.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script> -->
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
<br/><br/>
<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
<br/><br/><br/>
<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
<br/><br/><br/>
<label><spring:message code="LB.Inquiry_time" />：</label><label>${CMQTIME}</label>
<br/><br/>
<label><spring:message code="LB.Inquiry_period" />：</label><label>${cmdate}</label>
<br/><br/>
<label><spring:message code="LB.Total_records" />：</label><label>${COUNT} <spring:message code="LB.Rows" /></label>
<br/><br/>
<c:forEach var="labelListMap" items="${print_datalistmap_data}">
	<table class="print">
		<tr>
			<td style="text-align:center"><spring:message code="LB.Account" /></td>
			<td colspan="8">${labelListMap.ACN}</td>
		</tr>
		<tr>
			<td style="text-align:center"><spring:message code="LB.Change_date" /></td>
			<td style="text-align:center"><spring:message code="LB.Summary_1" /></td>
			<td style="text-align:center"><spring:message code="LB.Withdrawal_amount" /></td>
			<td style="text-align:center"><spring:message code="LB.Deposit_amount" /></td>
			<td style="text-align:center"><spring:message code="LB.Loan_Outstanding_1" /></td>
			<td style="text-align:center"><spring:message code="LB.Checking_account" /></td>
			<td style="text-align:center"><spring:message code="LB.Data_content" /></td>
			<td style="text-align:center"><spring:message code="LB.Receiving_Bank" /></td>
			<td style="text-align:center"><spring:message code="LB.Trading_time" /></td>
		</tr>
		<c:forEach items="${labelListMap.rowListMap}" var="map" varStatus="index">
			<tr>
				<td style="text-align:center">${map.LTD}</td>
				<td style="text-align:center">${map.MEMO_C}</td>
				<td style="text-align: right">${map.DPWDAMT}</td>
				<td style="text-align: right">${map.DPSVAMT}</td>
				<td style="text-align: right">${map.DPIBAL}</td>
				<td style="text-align:center">${map.CHKNUM}</td>
				<td style="text-align:center">${map.DATA1}</td>
				<td style="text-align:center">${map.ORNBRH}</td>
				<td style="text-align:center">${map.TIME}</td>
			</tr>
			<c:if test="${index.last}">
				<tr>
					<td colspan="2" align="center"><spring:message code="LB.Total_amount" /></td>
					<td style="text-align: right">${labelListMap.TOTAL_DPWDAMT}</td>
					<td style="text-align: right">${labelListMap.TOTAL_DPSVAMT}</td>
					<td style="text-align: right">${labelListMap.LASTDPIBAL}</td>
					<td colspan="4"></td>
				</tr>
			</c:if>
		</c:forEach>
	</table>
	<br/><br/>
</c:forEach>
<br/><br/>
	<div class="text-left">
		<spring:message code="LB.Description_of_page" />
		<ol class="list-decimal text-left">
			<li><spring:message code="LB.Checking_cashed_P2_D1" /></li>
		</ol>
	</div>
</body>
</html>