<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="bs" value="${closing_tw_account_input }"></c:set>

<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript">
		$(document).ready(function() {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		
		// 畫面初始化
		function init() {
			// 表單驗證初始化
			$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
			// 確認鍵 click
			goOn();
		}
		
		// 確認鍵 Click
		function goOn() {
			$("#CMSUBMIT").click( function(e) {
				console.log("submit~~");
				// 表單驗證
				if ( !$('#formId').validationEngine('validate') ) {
					e.preventDefault();
				} else {
					// 解除表單驗證
					$("#formId").validationEngine('detach');
					// 通過表單驗證準備送出
					processQuery();
				}
			});
		}
		
		// 通過表單驗證準備送出
		function processQuery() {
			// 遮罩
	     	initBlockUI();
	     	var next = "${__ctx}" + "${next}";
	        $("#formId").attr("action", next);
	        $("#formId").submit();
		}
	
	</script>
</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 臺幣存款帳戶結清銷戶申請    -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0422" /></li>
		</ol>
	</nav>

	<!-- 左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.D0422" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form id="formId" method="post" action="${__ctx}${next}">
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
                           
							<!-- 請選擇要結清的帳號 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D0423"/></h4>
                                    </label>
                                </span>
                              	<span class="input-block">
									<div class="ttb-input">
										<select class="custom-select select-input half-input validate[funcCall[validate_CheckSelect['<spring:message code= "LB.D0424" />',ACN,#]]]" name="ACN" id="ACN" >
											<option value="#">---<spring:message code="LB.Select_account"/>---</option>
											<c:forEach var="rec" items="${bs.REC }">
												<c:choose>
													<c:when test="${rec.ACN == forwardAcn }">
														<option value="${rec.ACN }" selected>${rec.ACN }</option>
													</c:when>
													<c:otherwise>
														<option value="${rec.ACN }">${rec.ACN }</option>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</select>
									</div>
                                </span>
                            </div>  
                        </div>
   
                        <input class="ttb-button btn-flat-orange" id="CMSUBMIT" type="button" value="<spring:message code= "LB.X0080" />" />
                        
                    </div>
                </div>
			</section>
		</main>
	</div><!-- content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>