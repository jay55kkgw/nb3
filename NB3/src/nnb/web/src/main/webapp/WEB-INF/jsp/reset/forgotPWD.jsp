<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<section id="password-miss" class="modal fade more-info-block" role="dialog" aria-labelledby="" aria-hidden="true" tabindex="-1">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header mb-0">
					<p class="ttb-pup-header">
						<spring:message code="LB.X1627"/>
					</p>
				</div>
				<!-- 表單顯示區  -->
				<div class="modal-body">
					<nav class="nav card-select-block" id="nav-tab1" role="tablist">
						<!-- 線上重設 -->
						<input type="button" class="nav-item ttb-sm-btn active"  data-toggle="tab" href="#nav-trans-11" role="tab" aria-selected="false" value="<spring:message code="LB.X2215" />" />
						<!-- 臨櫃重設 -->
						<input type="button" class="nav-item ttb-sm-btn"  data-toggle="tab" href="#nav-trans-12" role="tab" aria-selected="true" value="<spring:message code="LB.X2216" />" />
						<!-- 以信用卡重設 -->
						<input type="button" class="nav-item ttb-sm-btn"  data-toggle="tab" href="#nav-trans-13" role="tab" aria-selected="true" value="<spring:message code="LB.X2217" />" />
					</nav>
					<div class="tab-content" id="nav-tabContent1">
						<div class="ttb-input-block tab-pane fade show active" id="nav-trans-11" role="tabpanel" aria-labelledby="nav-home-tab">
							<div class="card-detail-block">
<!-- 								<p>您可透過晶片金融卡及簡訊驗證身份後，即可重設使用者名稱與密碼 (簡訊將發送至您留存本行的行動電話號碼)。請選擇下方欲重設的項目進行重設：</p> -->
								<p><spring:message code="LB.X2379" />：</p>
								<div class="text-center">
									<!-- 重設使用者名稱 -->
									<input type="submit" class="ttb-pup-btn btn-flat-orange ttb-pup-btn-doublemax" style="" value="<spring:message code="LB.X2205" />/<spring:message code="LB.login_password" />" onclick="fstop.getPage('${pageContext.request.contextPath}'+'/RESET/user_reset','', '')" />
									<!-- 重設簽入密碼 -->
<%-- 									<input type="submit" class="ttb-pup-btn btn-flat-orange" value="<spring:message code="LB.X2266" />" onclick="fstop.getPage('${pageContext.request.contextPath}'+'/RESET/user_reset','', '')" /> --%>
									<!-- 重設交易密碼 -->
									<input type="submit" class="ttb-pup-btn btn-flat-orange" value="<spring:message code="LB.X2267" />" onclick="fstop.getPage('${pageContext.request.contextPath}'+'/RESET/ssl_reset','', '')" />
								</div>
							</div>
						</div>
						<div class="ttb-input-block tab-pane fade" id="nav-trans-12" role="tabpanel" aria-labelledby="nav-home-tab">
							<div class="card-detail-block">
<!-- 								<p>您可以攜帶相關證件至開戶分行辦理，相關證件如下：</p> -->
								<p><spring:message code="LB.X2207" />：</p>
									<ol class="list-decimal description-list">
<!-- 										<li>1. 法人戶：公司登記證件、身分證、存款印鑑及存摺</li> -->
<!-- 										<li>2. 個人戶：身分證件、存款印鑑及存摺</li> -->
<!-- 										<li>1. <spring:message code="LB.X1597" />：<spring:message code="LB.X1599" />、<spring:message code="LB.X1600" />、<spring:message code="LB.X2208" /></li> -->
<!-- 										<li>2. <spring:message code="LB.X1604" />：<spring:message code="LB.X1606" />、<spring:message code="LB.X2208" /></li> -->
										<li><spring:message code="LB.X1597" />：<spring:message code="LB.X2380" /></li>
										<li><spring:message code="LB.X1604" />：<spring:message code="LB.X2381" /></li>
									</ol>
							</div>
						</div>
						<div class="ttb-input-block tab-pane fade" id="nav-trans-13" role="tabpanel" aria-labelledby="nav-home-tab">
							<div class="card-detail-block">
<!-- 								<p>若您是以本行信用卡申請網路銀行服務，可電洽本行客服0800-01-7171辦理以信用卡重設，次日起即可重新線上申請網路銀行。</p> -->
								<p><spring:message code="LB.X2209" /></p>
							</div>
						</div>
					</div>
					
				</div>
				<div class="modal-footer ttb-pup-footer">
					<input type="submit" class="ttb-pup-btn btn-flat-gray" data-dismiss="modal" value="<spring:message code= 'LB.X1572' />" />
				</div>
			</div>
		</div>
	</section>