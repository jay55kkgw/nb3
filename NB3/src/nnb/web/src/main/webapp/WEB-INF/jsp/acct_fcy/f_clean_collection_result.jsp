<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
</head>
<body>
        <!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 帳戶查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Account_Inquiry" /></li>
    <!-- 光票託收查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0230" /></li>
		</ol>
	</nav>



		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 --> 
 		<div class="content row">
 		   <%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
 		
 		<main class="col-12">	

		<!-- 		主頁內容  -->

			<section id="main-content" class="container">
<!-- 				光票託收查詢 -->
				<h2><spring:message code="LB.W0230" /></h2>
<!-- 				 IS8n -->
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 						下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
<!-- 						下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
					</select>
				</div>
				<c:if test="${clean_collection_result.data.TOPMSG == 'OKOV'}">
					<div class="MessageBar"><spring:message code="LB.F_Clean_Collection_P2_D1"/>
	       				<input id="CMCONTINUEQ" type="button" value="<spring:message code="LB.X0151" />" name="CMCONTINUEQ" class="ttb-sm-btn btn-flat-orange" onClick="processQuery()">
	       			</div>
	       		</c:if>		
				<div class="main-content-block row">
					<div class="col-12">
						<ul class="ttb-result-list">
							<li>
								<!-- 查詢時間 -->
								<h3><spring:message code="LB.Inquiry_time" />：</h3>
								<p>
									${clean_collection_result.data.CMQTIME}
								</p>
							</li>
							<li>
								<!-- 查詢期間 -->
								<h3><spring:message code="LB.Inquiry_period_1" />：</h3>
								<p>
									${clean_collection_result.data.CMPERIOD}
								</p>
							</li>
							<li>
								<!-- 資料總數 -->
								<h3><spring:message code="LB.Total_records" />：</h3>
								<p>
									${clean_collection_result.data.COUNT}
									<spring:message code="LB.Rows" />
								</p>								
							</li>
						</ul>
					<!-- 全部 -->
					<table class="stripe table-striped ttb-table dtable" data-show-toggle="first">
						<thead>
							<tr>
								<!-- 交易編號 -->
								<th data-title='<spring:message code="LB.Transaction_Number"/>'>
									<spring:message code="LB.Transaction_Number" />
								</th>
								<!-- 申請託收日 -->
								<th data-title='<spring:message code="LB.W0235"/>' data-breakpoints="xs sm">
									<spring:message code="LB.W0235" />
								</th>
								<!-- 票據號碼 -->
								<th data-title='<spring:message code="LB.W0236"/>' data-breakpoints="xs sm">
									<spring:message code="LB.W0236" />
								</th>
								<!-- 託收幣別 -->
								<th data-title='<spring:message code="LB.W0237"/>' data-breakpoints="xs sm">
									<spring:message code="LB.W0237" />
								</th>
								<!-- 託收金額 -->
								<th data-title='<spring:message code="LB.W0189"/>' data-breakpoints="xs sm">
									<spring:message code="LB.W0189" />
								</th>
								<!-- 國外入帳金額 -->
								<th data-title='<spring:message code="LB.W0239"/>'>
									<spring:message code="LB.W0239" />
								</th>
								<!-- 撥收日 -->
								<th data-title='<spring:message code="LB.W0240"/>' data-breakpoints="xs sm">
									<spring:message code="LB.W0240" />
								</th>
								<!-- 狀態 -->
								<th data-title='<spring:message code="LB.Status"/>' data-breakpoints="xs sm">
									<spring:message code="LB.Status" />
								</th>
								<%-- <th data-breakpoints="xs sm"><spring:message code="LB.Exchange_rate" /></th>	 --%>					
							</tr>
							</thead>
							<tbody>
							<c:forEach var="dataList" items="${ clean_collection_result.data.REC }">
								<tr>
					                <td class="text-center">${dataList.RREFNO}</td>
					                <td class="text-center">${dataList.RCREDATE}</td>
					                <td class="text-center">${dataList.RCHQNO}</td>
					                <td class="text-center">${dataList.RBILLCCY}</td>
					                <td class="text-right">${dataList.RBILLAMT}</td>
					                <td class="text-right">${dataList.RRETAMT}</td>
					                <td class="text-center">${dataList.RVALDATE}</td>
					                <td class="text-center">${dataList.RSTATE}</td>				               
								</tr>
							</c:forEach>
							</tbody>
						</table>
						<ul class="ttb-result-list">
							<li>
								<!-- 託收金額小計 -->
								<p>
<!-- 								 is8n -->
								<spring:message code="LB.W0209" />:
								</p>
								<c:set var="total">
									<spring:message code="LB.W0158" />
								</c:set>
								<c:set var="row">
									<spring:message code="LB.Rows" />
								</c:set>
								<c:set var="data_replace1" value="${fn:replace(clean_collection_result.data.CURRANCY_PRINT, 'i18n{LB.W0158}', total)}" />
								<c:set var="data_replace2" value="${fn:replace(data_replace1, 'i18n{LB.Rows}', row)}" />
								<p>
									${data_replace2}
								</p>
<%-- 									${clean_collection_result.data.CURRANCY_C} <spring:message code="LB.W0158" />   ${clean_collection_result.data.COUNT}   ${i18n.getMsg("LB.Rows")}														 --%>
													
							</li>
						</ul>
						<input type="button" id="previous" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />"/>
						<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
					</div>
				</div>
					<!--<div class="text-left">
						<spring:message code="LB.Description_of_page" /> ：
						<ol class="list-decimal text-left">
							<li><spring:message code="LB.F_demand_deposit_P2_D1" /></li>
						</ol>
					</div>-->
				<form id="formId" action="${__ctx}/download" method="post">
					<!-- 下載用 -->
					<!-- 光票託收查詢 -->
 					<input type="hidden" name="downloadFileName" value="LB.W0230"/>
					<input type="hidden" name="CMQTIME" value="${clean_collection_result.data.CMQTIME}"/>
					<input type="hidden" name="CMPERIOD" value="${clean_collection_result.data.CMPERIOD}"/>
					<input type="hidden" name="COUNT" value="${clean_collection_result.data.COUNT}"/>
					<input type="hidden" name="CURRANCY_txt" value="${clean_collection_result.data.CURRANCY_txt}"/>
					<input type="hidden" name="CURRANCY_xlsx" value="${clean_collection_result.data.CURRANCY_xlsx}"/>
					<input type="hidden" name="downloadType" id="downloadType"/> 					
					<input type="hidden" name="templatePath" id="templatePath"/>
					<input type="hidden" name="hasMultiRowData" value="false"/> 
					<input type="hidden" name="hasMultiRowData" value="false"/> 		
					<!-- EXCEL下載用 -->
					<!-- headerRightEnd  資料列以前的右方界線
						 headerBottomEnd 資料列到第幾列 從0開始
						 rowStartIndex 資料列第一列的位置
						 rowRightEnd 資料列用方的界線
					 -->
					<input type="hidden" name="headerRightEnd" value="7"/>
					<input type="hidden" name="headerBottomEnd" value="6"/>
					<input type="hidden" name="rowStartIndex" value="7" />
					<input type="hidden" name="rowRightEnd" value="7" />
					<input type="hidden" name="footerStartIndex" value="9" />
					<input type="hidden" name="footerEndIndex" value="11" />
					<input type="hidden" name="footerRightEnd" value="7" />
					<!-- TXT下載用
						txtHeaderBottomEnd需為資料第一列(從0開始)-->
					<input type="hidden" name="txtHeaderBottomEnd" value="9"/>
					<input type="hidden" name="txtHasRowData" value="true"/>
					<input type="hidden" name="txtHasFooter" value="true"/>
				</form>
				<form method="post" id="reRec">
					<input type="hidden" id="CMSDATE" name="CMSDATE" value="${clean_collection_result.data.CMSDATE}">
					<input type="hidden" id="CMEDATE" name="CMEDATE" value="${clean_collection_result.data.CMEDATE}">
					<input type="hidden" id="USERDATA" name="USERDATA" value="${clean_collection_result.data.USERDATA}">
				</form>
<!-- 				</form> -->
			</section>
			<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->
	

	<%@ include file="../index/footer.jsp"%>
      
    	<script type="text/javascript">
			$(document).ready(function(){
				// HTML載入完成後開始遮罩
				setTimeout("initBlockUI()",10);
				// 開始查詢資料並完成畫面
				setTimeout("init()",20);
				// 解遮罩
				setTimeout("unBlockUI(initBlockId)",500);
				setTimeout("initDataTable()",100);
			});
			function init(){
				//initFootable();
				//上一頁按鈕
				$("#previous").click(function() {
					initBlockUI();
					fstop.getPage('${pageContext.request.contextPath}'+'/FCY/ACCT/f_clean_collection','', '');
				});

				$("#printbtn").click(function(){
					var params = {
						"jspTemplateName":"f_clean_collection_result_print",
						"jspTitle":"<spring:message code='LB.W0230' />",//光票託收查詢
						"CMPERIOD":"${clean_collection_result.data.CMPERIOD}",
						"CMQTIME":"${clean_collection_result.data.CMQTIME}",
						"COUNT":"${clean_collection_result.data.COUNT}",
						"CURRANCY_PRINT":"${data_replace2}"
					};
					openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
				});		
				
			}
				//選項
			 	function formReset() {
// 			 		initBlockUI();
		 			if ($('#actionBar').val()=="excel"){
						$("#downloadType").val("OLDEXCEL");
						$("#templatePath").val("/downloadTemplate/f_clean_collection.xls");
			 		}else if ($('#actionBar').val()=="txt"){
						$("#downloadType").val("TXT");
						$("#templatePath").val("/downloadTemplate/f_clean_collection.txt");
			 		}
// 		    		ajaxDownload("${__ctx}/ajaxDownload","formId","finishAjaxDownload()");
		 			$("#formId").attr("target", "");
					$("#formId").submit();
					$('#actionBar').val("");
				}
// 				function finishAjaxDownload(){
// 					$("#actionBar").val("");
// 					unBlockUI(initBlockId);
// 				}
			//繼續查詢
			function processQuery(){
 				$("#reRec").attr("action","${__ctx}/FCY/ACCT/f_clean_collection_result");
 	  			$("#reRec").submit();
			}
		</script>
    
</body>
</html>