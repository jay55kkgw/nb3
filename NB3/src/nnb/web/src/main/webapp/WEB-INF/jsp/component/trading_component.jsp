<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript" src="${__ctx}/component/windows/IKey/ikeyTopWebSocketUtil.js"></script>
<script type="text/javascript" src="${__ctx}/component/windows/IKey/IKeyMethod.js"></script>
<script type="text/javascript" src="${__ctx}/component/windows/noneIE/noneIECard.js"></script>
<script type="text/javascript" src="${__ctx}/component/windows/noneIE/topWebSocketUtil.js"></script>
<script type="text/javascript" src="${__ctx}/keyboard/js/plugins.js"></script>
<html>
<!-- 自然人憑證元件載點 -->
	<a href="${__ctx}/component/windows/natural/TBBankPkcs11ATL.exe" id="naturalComponent" target="_blank" style="display:none"></a>
</html>
<!-- 讀卡機所需JS -->
<script type="text/javascript" src="${__ctx}/component/util/VAckisIEbrower.js"></script>
<script type="text/javascript" src="${__ctx}/component/util/Pkcs11ATLAgent.js"></script>
<script type="text/JavaScript">
	var isIkeyUser; // 使用者是否ikey使用者
	var comVersion; // 各元件的最新版本號
	var platformNew; // 作業系統
	var hasAskDownload = false; // 因為現在WINDOW多瀏覽器元件的安裝檔整合成一個，所以要多一個變數，只詢問使用者下載一次

	$(document).ready(function() {
			if(location.href.indexOf("/transfer_confirm")=='-1' && location.href.indexOf("fee_expose")=='-1'){
				checkTC();
		}
	
		
	});
	function checkTC(){
		component_isikeyuser(); // 判斷使用者是否ikey使用者
		component_version(); // 取得各元件的最新版本號
		component_platform(); // 取得裝置作業系統
		component_initKeyBoard(); // 動態鍵盤初始化
		setTimeout("component_init()", 50);
	}
	function component_init(){
		// 20190925更新  若登入權限為  CRD 不安裝元件，by 景森
		if("CRD"=='${sessionScope.authority}'){
			console.log("Can't Use Component")
			//跳過元件檢查 直接檢查自然人憑證
			if(checkNatural){
				checkNatural = false;
				initBlockId1 = initBlockUI(); 
				initNatural_EXCUTE();
			}
		}else{
			// 元件邏輯 (進畫面後延遲提示，避免影響其他資源載入)
	 		setTimeout(component_init_1(),1500);
		}
	}
	// 動態鍵盤初始化
	function component_initKeyBoard(){
		
		$('.keyboard').keyboard();
		$(".keyboardButtons li").mouseover(function(){
			$("#pwd").focus();
		});
	}
	
	// 判斷使用者是否ikey使用者
	function component_isikeyuser() {
		var uri = '${__ctx}' + "/COMPONENT/isikeyuser_aj";
		var bs = fstop.getServerDataEx(uri, null, false);
		console.log("isIkeyUser_bs: " + JSON.stringify(bs));

		isIkeyUser = bs.result;
		console.log("isIkeyUser: " + isIkeyUser);
	}
	//
	var checkNatural = false;
	function initNatural(){
		checkNatural = true;
	}
	var myobj = null;
	// 初始化自然人元件
	function initNatural_EXCUTE() {
		if (!checkBrowser()) {
			return 0;
		}
		var Brow1 = new ckBrowser1();
// 		if(!Brow1.isIE){	
// 			if((navigator.userAgent.indexOf("Chrome") > -1) || (navigator.userAgent.indexOf("Firefox") > -1) || (navigator.userAgent.indexOf("Safari") > -1)) {
				myobj = initCapiAgentForChrome();
				try {
					if(sessionToken == null){
						myobj.initChrome();
						console.log("myobj.initChrome...");
					}
				} catch (e) {
//						if(confirm("<spring:message code= "LB.X1216" />")){
//							$("#naturalComponent")[0].click();
//						}
					isconfirm = 1;
						errorBlock(
								null, 
								null,
								['<spring:message code= "LB.X1216" />'], 
								'<spring:message code= "LB.X2037" />', 
								'<spring:message code= "LB.Cancel" />'
							);
				}			
// 			}
// 		} else {
// 			var strObject = "";
// 	    	// 使用 Script 判斷 win64 或 win32, 執行時間有點久
// 	 		if (navigator.platform.toLowerCase() == "win64"){
// 				// "Windows 64 位元";
// 	 			document.getElementById("obj").outerHTML = '<object id="myobj" classid="CLSID:32FDE779-F9DC-4D39-97FB-434102000101" codebase="${__ctx}/component/windows/natural/TBBankPkcs11ATLx64.cab" width="0px" height="0px"></object>';
// 			} else {
// 				// "Windows 32 位元 或 模擬 64 位元(WOW64)";
// 				document.getElementById("obj").outerHTML = '<object id="myobj" classid="CLSID:32FDE779-F9DC-4D39-97FB-434102000101" codebase="${__ctx}/component/windows/natural/TBBankPkcs11ATL.cab" width="0px" height="0px"></object>';
// 			}
// 		    myobj = document.myobj;
// 		}
		console.log("initNatural.finish...");
		unBlockUI(initBlockId);
	}
	// 取得各元件的最新版本號
	function component_version() {
		var uri = '${__ctx}' + "/COMPONENT/component_version_aj";
		var bs = fstop.getServerDataEx(uri, null, false);
		console.log("comVersion_bs: " + JSON.stringify(bs));

		comVersion = bs.data;
		console.log("comVersion: " + JSON.stringify(comVersion));
	}

	// 判斷裝置
	function checkBrowser() {
		var checkBrowserResult = false;
		if (navigator.userAgent.match(/Android/i)
				|| navigator.userAgent.match(/webOS/i)
				|| navigator.userAgent.match(/iPhone/i)
				|| navigator.userAgent.match(/iPad/i)
				|| navigator.userAgent.match(/iPod/i)
				|| navigator.userAgent.match(/BlackBerry/i)
				|| navigator.userAgent.match(/Windows Phone/i)) {
			// 行動裝置
			console.log("component checkBrowser not pass!!!");
		} else {
			// 可使用元件之裝置
			checkBrowserResult = true;
			console.log("component checkBrowser pass...");
		}

		console.log("checkBrowser: " + checkBrowserResult);

		return checkBrowserResult;
	}
	
	var isconfirm = 0;
	
	function btn1(){
		//自然人憑證
		if(isconfirm==1){
			checkNatural = false;
			$("#naturalComponent")[0].click();
		}
		if(isconfirm==2){
				//修改 HTTP Response Splitting
			//WINDOWS
			if(platformNew.indexOf("Win") > -1){
				if (window.console) {
					console.log("windowComponent href="
							+ jQuery("#windowComponent").attr(
									"href"));
				}
				jQuery("#componentPath").val("win");
				jQuery("#componentForm").submit();
				
			} // MAC
			else {
				if (window.console) {
					console.log("MacComponent href="
							+ jQuery("#MacComponent").attr(
									"href"));
				}
				jQuery("#componentPath").val("mac");
				jQuery("#componentForm").submit();
				
			}
				
			$("#trading-block").show(); // 顯示遮罩提示立即安裝元件
		}
		if(isconfirm==3){
			// WINDOWS
			if(platformNew.indexOf("Win") > -1){
				if (window.console) {
				console.log("windowComponent href="
						+ jQuery("#windowComponent").attr(
								"href"));
				}
				jQuery("#componentPath").val("win");
				jQuery("#componentForm").submit();
			
			} // MAC
			else {
			
				if (window.console) {
					console.log("MacComponent href="
							+ jQuery("#MacComponent").attr(
									"href"));
				}
				jQuery("#componentPath").val("mac");
				jQuery("#componentForm").submit();
			}
			
			$("#trading-block").show(); // 顯示遮罩提示立即安裝元件
		}
		if(isconfirm==4){
			//
		}
		isconfirm = 0;
		if(checkNatural){
			checkNatural = false;
			checkNatural = false;
			setTimeout("blockUI()", 60);
			setTimeout("initNatural_EXCUTE()", 80);
		}
	}
	
	
	$("#errorBtn1").click(function(){
		setTimeout("$('#error-block').hide()", 20);
		setTimeout("unBlockUI()", 40);
		setTimeout("btn1()", 60);
	});
	$("#errorBtn2").click(function(){
		setTimeout("$('#error-block').hide()", 100);
		setTimeout("unBlockUI()", 200);
		isconfirm = 0;
		if(checkNatural){
			checkNatural = false;
			setTimeout("blockUI()", 300);
			setTimeout("initNatural_EXCUTE()", 400);
		}
	});
	// 取得裝置作業系統
	function component_platform() {
		platformNew = navigator.platform;
		if (window.console) {
			console.log("component_platform: " + platformNew);
		}
	}
	
	// 元件初始化
	function component_init_1(){
		// 行動裝置不能做，所以要先判斷
		if (checkBrowser()) {
			// 改在此頁面載入時引入
// 			$.loadScript(
// 				'${__ctx}/component/combo/topWebSocketUtil.js', function() {
// 					console.log("loading...topWebSocketUtil.js");
// 			});
				
			topWebSocketUtil.setWssUrl("wss://localhost:9203/", 
		            ValidateLegalURL_Callback_Both);
		}
	}

	// 底下必須
	//------------------------------------------------------------------------------
	// 底下必須有，這是第一關
	// 等到 ValidateLegalURL_Callback 回呼方法被呼叫後，
	// 再經由 topWebSocketUtil.invokeRpcDispatcher 呼叫元件方法
	//------------------------------------------------------------------------------
	// Name: ValidateLegalURL_Callback
	function ValidateLegalURL_Callback_Both(rpcStatus, rpcReturn) {	
		try {
			//------------------------------------------------------------------
			topWebSocketUtil.tryRpcStatus(rpcStatus);
			//------------------------------------------------------------------
			
			// 未偵測到元件(未安裝或關閉)
			if (rpcReturn == "E_Send_11_OnError_1006") {
        		hasAskDownload = true;
        		// 詢問使用者是否安裝元件
        		isconfirm = 2;
						errorBlock(
								'<spring:message code= "LB.X2418" />',
								null,
								['<spring:message code= "LB.X2419" />'], 
								'<spring:message code= "LB.X2037" />', 
								'<spring:message code= "LB.Cancel" />'
							);
						return 0;
//     			if (confirm("<spring:message code= "LB.Confirm012" />")) {
//     				//修改 HTTP Response Splitting
//    					//WINDOWS
//     				if(platformNew.indexOf("Win") > -1){
//     					if (window.console) {
// 	    					console.log("windowComponent href="
// 	    							+ jQuery("#windowComponent").attr(
// 	    									"href"));
// 	    				}
// 	    				jQuery("#componentPath").val("win");
// 	    				jQuery("#componentForm").submit();
	    				
//     				} // MAC
//     				else {
// 	    				if (window.console) {
// 	    					console.log("MacComponent href="
// 	    							+ jQuery("#MacComponent").attr(
// 	    									"href"));
// 	    				}
// 	    				jQuery("#componentPath").val("mac");
// 	    				jQuery("#componentForm").submit();
// 	    			}
//     			}
    		
        	} // 安裝完成,檢查更新
        	else if (rpcReturn =="E000"||rpcReturn == null){
        		rpcName = "GetVersion";
        		topWebSocketUtil.invokeRpcDispatcher(GetVersion_Callback,rpcName);
        		
        		// GetVersion_Callback
        		function GetVersion_Callback(rpcStatus,rpcReturn){
        			if(window.console){console.log("GetVersion_Callback...");}
        			try{
        				topWebSocketUtil.tryRpcStatus(rpcStatus);
        				
        				var currentVersion = rpcReturn;
        				if(window.console){console.log("currentVersion: " + currentVersion);}
        				if(window.console){console.log("newVersion: " + comVersion.NoneIEReaderVersion);}
        				if(window.console){console.log("Mac_newVersion: " + comVersion.MacReaderVersion);}
        				
        				var currentVersionNO = parseInt(currentVersion.replace(/\./g,""));
                        var newVersionNO;
                        //依系統取得版本號20210312因元件版本號不同修正
                        if(platformNew.indexOf("Win") > -1){
                            newVersionNO = parseInt(comVersion.NoneIEReaderVersion.replace(/\./g,""));
                        }
                        else{
                            newVersionNO = parseInt(comVersion.MacReaderVersion.replace(/\./g,""));
                        }        				
        				if(window.console){console.log("currentVersionNO="+currentVersionNO);}
        				if(window.console){console.log("newVersionNO="+newVersionNO);}
        				
        				// 元件版本需要更新
        				if(currentVersionNO < newVersionNO){
        					isconfirm = 3;
    						errorBlock(
    								'<spring:message code= "LB.X2421" />',
    								null,
    								['<spring:message code= "LB.X2419" />'], 
    								'<spring:message code= "LB.X2037" />', 
    								'<spring:message code= "LB.Cancel" />'
    							);
    						return 0;
        		
        				}
        				else{
        					if(checkNatural){
        	        			checkNatural = false;
        	        			setTimeout("blockUI()", 60);
        	        			setTimeout("initNatural_EXCUTE()", 80);
        	        		}
        				}
        				
        			} catch (exception){
        				if(window.console){console.log("exception="+exception);}
        				//alert("<spring:message code= "LB.X1655" />");
        				isconfirm = 4;
        				errorBlock(
    							null, 
    							null,
    							["<spring:message code= 'LB.X1655' />"], 
    							'<spring:message code= "LB.Quit" />', 
    							null
    						);
        				return;
        			}
        		}
        	}
				
		} catch (exception) {
		        alert("exception: " + exception);
		}
	}
	
	// 判斷讀卡機是一代或二代機
	function getReaderType(readerName) {
		var readerType = "1";

		if (readerName.indexOf("CASTLES EZpad") >= 0) {
			readerType = "2";
		} else if (readerName.indexOf("Todos eCode Connectable") >= 0) {
			readerType = "2";
		} else if (readerName.indexOf("Todos eCode Connectable II") >= 0) {
			readerType = "2";
		} else if (readerName.indexOf("ACS ACR83U") >= 0) {
			readerType = "2";
		} else if (readerName.indexOf("ACS APG8201") >= 0) {
			readerType = "2";
		}

		return readerType;
	}

	// 拿CardReaderControler_v1.0.js裡面的來用
	function GetErrorMessage(ErrCode) {
		var MessageWord = "";

		switch (ErrCode) {
		// 讀卡機控制物件載入失敗
		case "E999":
			MessageWord = "<spring:message code= "LB.X1656" />（E999）";
			break;
		// 讀卡機偵測失敗
		case "E001":
			MessageWord = "<spring:message code= "LB.X1657" />（E001）";
			break;
		// 無法連接讀卡機驅動程式
		case "E002":
			MessageWord = "<spring:message code= "LB.X1658" />（E002）";
			break;
		// 卡片偵測失敗
		case "E003":
			MessageWord = "<spring:message code= "LB.X1659" />（E003）";
			break;
		// 非本行晶片金融卡
		case "E005":
			MessageWord = "<spring:message code= "LB.X1660" />（E005）";
			break;
		// 無法取得發卡行資訊
		case "E006":
			MessageWord = "<spring:message code= "LB.Alert062" />（E006）";
			break;
		// 卡片密碼不可為空白
		case "E007":
			MessageWord = "<spring:message code= "LB.X1661" />（E007）";
			break;
		// 卡片密碼錯誤
		case "E008":
			MessageWord = "<spring:message code= "LB.X1662" />（E008）";
			break;
		// 卡片已遭鎖定
		case "E009":
			MessageWord = "<spring:message code= "LB.X1663" />（E009）";
			break;
		// 取得發卡行代碼失敗
		case "E010":
			MessageWord = "<spring:message code= "LB.X1664" />（E010）";
			break;
		// 取得帳號資料失敗
		case "E011":
			MessageWord = "<spring:message code= "LB.X1665" />（E011）";
			break;
		// 尚未通過密碼驗證
		case "E012":
			MessageWord = "<spring:message code= "LB.X2386" />（E012）";
			break;
		// 取得交易序號與TAC碼失敗
		case "E013":
			MessageWord = "<spring:message code= "LB.X1667" />（E013）";
			break;
		// 尚未安裝元件
		case "E_Send_11_OnError_1006":
			MessageWord = "<spring:message code= "LB.X1668" />（E_Send_11_OnError_1006）";
			break;
			// 密碼輸入操作錯誤
		case "E007|69F0":
			MessageWord = "<spring:message code= "LB.X2590" />（E007|69F0）";
			break;
			// 確認密碼操作錯誤
		case "E007|69F0,0":
			MessageWord = "<spring:message code= "LB.X2591" />（E007|69F0,0）";
			break;
			// 密碼輸入錯誤
		case "E007|6610":
			MessageWord = "<spring:message code= "LB.X2592" />（E007|6610）";
			break;
			// 確認密碼操作錯誤
		case "E005,69F0, 0":
			MessageWord = "<spring:message code= "LB.X2591" />（E005,69F0, 0）";
			break;
		// 未知錯誤
		default:
			MessageWord = "<spring:message code= "LB.X1669" />(" + ErrCode + ")";
			break;
		}
		return MessageWord;
	}

	// IKEY遮罩
	function ShowLoadingBoard(maskID, trigger) {
		var strMask = "<div id='windowMask' style='";
		strMask += "top:0px; ";
		strMask += "left:0px; ";
		strMask += "width:100%; ";
		strMask += "height:" + jQuery(document).height() + "px; ";
		strMask += "background:#000000; ";
		strMask += "filter: Alpha(Opacity=30); ";
		strMask += "-moz-opacity:.3; ";
		strMask += "opacity:0.3; ";
		strMask += "position:fixed; ";
		strMask += "z-index:100; ";
		strMask += "'></div>";

		var strLoadingObj = "<div style='top:0px; left:0px; width:100%; margin-top:200px; ";
		strLoadingObj += "font-family:arial; font-size:64px; text-align:center; ";
		strLoadingObj += "color:#ffffff; position:fixed; z-index:102; ";
		strLoadingObj += "'>Loading...</div>";

		var ContentStr = strLoadingObj + strMask
				+ document.getElementById(maskID).innerHTML;
		document.getElementById(maskID).innerHTML = trigger ? ContentStr : "";
	}

	// 顯示IKEY遮罩
	function SLBForSeconds(maskID, timeout) {
		ShowLoadingBoard(maskID, true);

		setTimeout(function() {
			ShowLoadingBoard(maskID, false);
		}, timeout);
	}

	// 晶片卡--是否顯示遮罩
	function FinalSendout(maskID, trigger) {
		var strMask = "<div id='windowMask' style='";
		strMask += "top:0px; ";
		strMask += "left:0px; ";
		strMask += "width:100%; ";
		strMask += "height:2000px; ";
		strMask += "background:#000000; ";
		strMask += "filter: Alpha(Opacity=30); ";
		strMask += "-moz-opacity:.3; ";
		strMask += "opacity:0.3; ";
		strMask += "position:fixed; ";
		strMask += "z-index:100; ";
		strMask += "'></div>";

		var strMask_New = strMask.substr(0, strMask.indexOf("background"));
		strMask_New += strMask.indexOf("position");

		document.getElementById(maskID).innerHTML = strMask_New;
	}
	
	// 押碼--MakeTRMID
	function MakeTRMID() {
		var TRMID = "";

		while (TRMID.length < 8) {
			TRMID += Math.floor(Math.random() * 10);
		}
		return TRMID;
	}
	
	// 身分查驗中遮罩說明
	function showTempMessage(messageBoardWidth, messageTitle, messageContent, maskID, trigger) {
		// 自然人憑證
		if(maskID == "validateMaskNatural") {
			if(trigger){
				$("#validateMaskNatural").show();
				
			} else {
				$("#validateMaskNatural").hide();
			}
		} else if(maskID == "validateMaskInterBankAcc") { 
			// 跨行金融帳戶認證
			if(trigger){
				$("#validateMaskInterBankAcc").show();
				
			} else {
				$("#validateMaskInterBankAcc").hide();
			}
			
		} // 晶片金融卡
		else {
			if(trigger){
				$("#validateMask").show();
			} else {
				$("#validateMask").hide();
			}
		}
	}
	
	// 拔插卡遮罩說明
	function showRemoveInsertPad(maskID, maxCountDown) {
		$("#CountdownNum").val(maxCountDown);
		$("#removeInsertMask").show();
		
	}

</script>

<script type="text/javascript">
	jQuery(document).ready(function() {
		$("#dynamicKeyboard").bind('keydown', function(event) {
			if (event.keyCode == 13) { // 如果按 enter     
				addPwd();
			}
		});
	});

	// 小鍵盤
	var dynamicKeyboardPswd = "";
	var callbackFunction = "";
	var readerLime = "";
	var getBacker = "";
	
	// 驗證密碼
	function addPwd() {
		// 遮罩
		var initBlockId = initBlockUI();
		
		var errorMsgs = "";
		if ($("#pwd").val().length < 6) {
			errorMsgs += "<spring:message code= "LB.X1674" />";
			$("#pwd").focus();
		}
		if (errorMsgs != "") {
			$("#pwd").focus();
			console.log("errorMsgs: " + errorMsgs);
			unBlockUI(initBlockId);
			//alert(errorMsgs);
			errorBlock(
							null, 
							null,
							[errorMsgs], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
			return;
		}

		dynamicKeyboardPswd = $("#pwd").val();
		$("#pwd").val("");

		$("#windowMask").hide();
		$("#dynamicKeyboard").hide();
		unBlockUI(initBlockId);
		
		// 輸入密碼後callbackFunction
		eval(callbackFunction + "('" + readerLime + "','" + dynamicKeyboardPswd + "','" + getBacker + "')");
	}
	
	// 顯示小鍵盤
	function showDialog(callback, readerName, outerCallBackFunction) {

		callbackFunction = callback;
		readerLime = readerName;
		getBacker = outerCallBackFunction;

		$("#windowMask").show(); // 背景遮罩
		$("#dynamicKeyboard").show(); // 動態鍵盤
		$("#pwd").focus();
	}

	// 小鍵盤取消
	function closeDialog() {
		$('#dynamicKeyboard').hide();
		$('#windowMask').hide();
		$('#pwd').val('');
		unBlockUI(initBlockId);
	}
</script>


<!-- 請於下載完成後立即安裝元件 -->
<section id="trading-block" class="error-block" style="display:none">
    <div class="error-for-message">
        <p class="error-title"></p>
        <p class="error-content"></p>
        <p class="error-info"><spring:message code="LB.X2420" /></p>
    </div>
</section>


<!-- 晶片金融卡動態鍵盤 -->
<section id="removeInsertMask" class="error-block" style="display: none">
    <div class="error-for-message">
    	<!-- 請插入讀卡機與晶片金融卡 -->
        <p class="error-title"><spring:message code="LB.X2048" /></p>
        <!-- 請您於x秒內將晶片金融卡移除，並重新置入讀卡機中，以繼續驗證程序。 -->
        <p class="error-content"><spring:message code="LB.X2049" />
	        <span class="high-light m-1"><font id="CountdownNum"></font>&nbsp;<spring:message code="LB.X2050" /></span>
			<spring:message code="LB.X2051" />
        </p>
    </div>
</section>

<!-- 晶片金融卡驗證中訊息遮罩 -->
<section id="validateMask" class="error-block" style="display: none">
    <div class="error-for-message">
    	<!-- 晶片金融卡身份查驗 -->
        <p class="error-title"><spring:message code="LB.X2052" /></p>
        <!-- 晶片金融卡身份查驗中，請稍候... -->
        <p class="error-content">
			<spring:message code="LB.X2053" />
        </p>
        <!-- 查驗期間請勿移除您的晶片金融卡，以免造成驗證錯誤。 -->
        <p class="error-content">
			<spring:message code="LB.X2054" />
        </p>
    </div>
</section>

<!-- 自然人憑證驗證中訊息遮罩 -->
<section id="validateMaskNatural" class="error-block" style="display: none">
    <div class="error-for-message">
    	<!-- 自然人憑證身份查驗 -->
        <p class="error-title"><spring:message code="LB.X2055" /></p>
        <!-- 自然人憑證身份查驗中，請稍候... -->
        <p class="error-content">
			<spring:message code="LB.X2056" />
        </p>
        <!-- 查驗期間請勿移除您的自然人憑證，以免造成驗證錯誤。 -->
        <p class="error-content">
			<spring:message code="LB.X2057" />
        </p>
    </div>
</section>

<!-- 跨行金融帳戶認證中訊息遮罩 -->
<section id="validateMaskInterBankAcc" class="error-block" style="display: none">
    <div class="error-for-message">
    	<!-- 跨行金融帳戶認證 -->
        <p class="error-title"><spring:message code="LB.Fisc_inter-bank_acc_verify" /></p>
        <!-- 跨行金融帳戶認證中，請稍候... -->
        <p class="error-content">
			<spring:message code="LB.Fisc_inter-bank_acc_verifying" />
        </p>
    </div>
</section>

<!-- 晶片金融卡動態鍵盤 -->
<section id="dynamicKeyboard" class="error-block" style="display: none">
	<div class="error-for-code">
		<!-- 請插入讀卡機與晶片金融卡 -->
		<p class="error-title"><spring:message code="LB.X2048" /></p>
		<!-- 請輸入您的晶片金融卡密碼以繼續。-->
		<p class="error-content"><spring:message code="LB.X2059" /></p>
		
		<input type="password" class="text-input" id="pwd" name="pwd" value="" maxlength='12' autocomplete='off' placeholder="<spring:message code="LB.X0492" />" />
		
		<div class="keyboardBlock">
			<p>
				<spring:message code="LB.Dynamic_keyboard" />
			</p>
			<div class="keyboard numberKeyboard">
				<ul class="keyboardButtons">
					<li data-role="symbol" data-key="1" data-key-caps="1"></li>
					<li data-role="symbol" data-key="2" data-key-caps="2"></li>
					<li data-role="symbol" data-key="3" data-key-caps="3"></li>
					<li data-role="symbol" data-key="4" data-key-caps="4"></li>
					<li data-role="symbol" data-key="5" data-key-caps="5"></li>
					<li data-role="symbol" data-key="6" data-key-caps="6"></li>
					<li data-role="symbol" data-key="7" data-key-caps="7"></li>
					<li data-role="symbol" data-key="8" data-key-caps="8"></li>
					<li data-role="symbol" data-key="9" data-key-caps="9"></li>
					<li data-role="symbol" data-key="0" data-key-caps="0"></li>
					<li class="btnReset" data-role="delete" data-key="" data-key-caps="" style="width: 80px;"><img src="${__ctx}/img/delete.svg" style="width: 20px; color: #f3720c;" /></li>
				</ul>
			</div>
		</div>
		
		<button id="keyboardCancel" class="btn-flat-gray ttb-pup-btn" onclick="closeDialog();"><spring:message code="LB.Cancel" /></button>
		<button id="keyboardCommit" class="btn-flat-orange ttb-pup-btn" onclick="addPwd();"><spring:message code="LB.X0290" /></button>
	</div>
</section>


<div id="MaskArea"></div>


<div id='windowMask' style='display: none; top: 0px; left: 0px; width: 100%; height: 2000px; background: #000000; filter: Alpha(Opacity = 30); -moz-opacity: .3; opacity: 0.3; position: fixed; z-index: 100;'></div>


<!-- MAC版多合一元件的載點 -->
<a href="${__ctx}/component/mac/combo/Setup-TbbComboMacNativeAgentHost.pkg"
	id="MacComponent" style="display: none"></a>
	
	
<!-- Windows版多合一元件的載點 -->
<a href="${__ctx}/component/windows/combo/Install_TbbComboNativeAgentHost.exe"
	id="windowComponent" style="display: none"></a>
	
	
<form id="componentForm" action="${__ctx}/COMPONENT/component_download" method="post">
	<input type="hidden" name="componentPath" id="componentPath" />
	<input type="hidden" name="trancode" value="ComponentDownload" />
</form>	
		
