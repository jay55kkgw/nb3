<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<link rel="stylesheet" type="text/css"
	href="${__ctx}/css/validationEngine.jquery.css">
<link rel="stylesheet" type="text/css"
	href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript"
	src="${__ctx}/js/jquery.datetimepicker.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		// initFootable(); // 將.table變更為footable 
		init();
	});

	function init() {
		//繼續查詢
		$("#CMCONTINU").click(
				function(e) {
					e = e || window.event;
					console.log("submit~~");

					$("#formId").validationEngine('detach');
					initBlockUI(); //遮罩
					$("#formId").attr("action",
							"${__ctx}/FCY/ACCT/f_inward_remittance_result");
					$("#formId").submit();
				});
		//列印
		$("#printbtn")
				.click(
						function() {
							var params = {
								"jspTemplateName" : "f_inward_remittance_result_print",
								"jspTitle" : '<spring:message code= "LB.W0125" />',
								"CMQTIME" : "${inward_remittance_result.data.CMQTIME}",
								"ACN" : "${inward_remittance_result.data.ACN}",
								"CMPERIOD" : "${inward_remittance_result.data.CMPERIOD}",
								"CMRECNUM" : "${inward_remittance_result.data.CMRECNUM}",
								"FXINAMTSUMSTRINGFORPRINTMS" : "${inward_remittance_result.data.FXINAMTSUMSTRINGFORPRINTMS}"
							};
							openWindowWithPost(
									"${__ctx}/print",
									"height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",
									params);
						});
		//上一頁按鈕
		$("#CMBACK").click(function() {
			var action = '${__ctx}/FCY/ACCT/f_inward_remittance';
			$('#back').val("Y");
			$("form").attr("action", action);
			initBlockUI();
			$("form").submit();
		});
	}

	//選項
	function formReset() {
		// 	 		initBlockUI();
		if ($('#actionBar').val() == "excel") {
			$("#downloadType").val("OLDEXCEL");
			$("#USERDATA").val("");
			$("#templatePath").val(
					"/downloadTemplate/f_inward_remittance_result.xls");
		} else if ($('#actionBar').val() == "txt") {
			$("#downloadType").val("TXT");
			$("#USERDATA").val("");
			$("#templatePath").val(
					"/downloadTemplate/f_inward_remittance_result.txt");
		}
		//     		ajaxDownload("${__ctx}/FCY/ACCT/f_inward_remittance_ajaxDirectDownload","formId","finishAjaxDownload()");

		$("form").attr("action",
				"${__ctx}/FCY/ACCT/f_inward_remittance_ajaxDirectDownload");
		$("#formId").attr("target", "");
		$("#formId").submit();
		$('#actionBar').val("");
	}
	function finishAjaxDownload() {
		$("#actionBar").val("");
		unBlockUI(initBlockId);
	}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=BIG5">
<title>Insert title here</title>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
    <!-- 匯入匯款     -->
    		<li class="ttb-breadcrumb-item"><spring:message code="LB.W0125" /></li>
    <!-- 匯入匯款查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0125" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
		<section id="main-content" class="container">
			<h2>
				<!--匯入匯款查詢(未輸入) -->
				<!-- <spring:message code="LB.NTD_Demand_Deposit_Detail" /> -->
				<spring:message code="LB.W0125" />
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<!-- 下拉式選單-->
			<div class="print-block">
				<select class="minimal" id="actionBar" onchange="formReset()">
					<option value=""><spring:message code="LB.Downloads" /></option>
					<!-- 						下載Excel檔 -->
					<option value="excel"><spring:message
							code="LB.Download_excel_file" /></option>
					<!-- 						下載為txt檔 -->
					<option value="txt"><spring:message
							code="LB.Download_txt_file" /></option>
				</select>
			</div>
			<form id="formId" method="post">
				<input type="hidden" id="back" name="back" value=""> <input
					type="hidden" name="ACNDownload"
					value="${inward_remittance_result.data.ACNDownload}" /> <input
					type="hidden" id="USERDATA" name="USERDATA"
					value="${inward_remittance_result.data.USERDATA}" /> <input
					type="hidden" name="CMSDATE"
					value="${inward_remittance_result.data.CMSDATE}" /> <input
					type="hidden" name="CMEDATE"
					value="${inward_remittance_result.data.CMEDATE}" /> <input
					type="hidden" name="FXSORT"
					value="${inward_remittance_result.data.FXSORT}" />
				<!-- 下載用 -->
				<input type="hidden" name="downloadFileName"
					value="<spring:message code="LB.W0125" />" /> <input type="hidden"
					name="CMQTIME" value="${inward_remittance_result.data.CMQTIME}" />
				<input type="hidden" name="CMPERIOD"
					value="${inward_remittance_result.data.CMPERIOD}" /> <input
					type="hidden" name="CMRECNUM"
					value="${inward_remittance_result.data.CMRECNUM}" /> <input
					type="hidden" name="ACN"
					value="${inward_remittance_result.data.ACN}" /> <input
					type="hidden" name="FXINAMTSUMSTRING"
					value="${inward_remittance_result.data.FXINAMTSUMSTRINGFORPRINT}" />
				<input type="hidden" name="downloadType" id="downloadType" /> <input
					type="hidden" name="templatePath" id="templatePath" /> <input
					type="hidden" name="hasMultiRowData" value="false" />

				<!-- EXCEL下載用 -->
				<input type="hidden" name="headerRightEnd" value="15" /> <input
					type="hidden" name="headerBottomEnd" value="7" /> <input
					type="hidden" name="multiRowStartIndex" value="8" /> <input
					type="hidden" name="multiRowEndIndex" value="8" /> <input
					type="hidden" name="multiRowCopyStartIndex" value="8" /> <input
					type="hidden" name="multiRowCopyEndIndex" value="8" /> <input
					type="hidden" name="rowStartIndex" value="8" /> <input
					type="hidden" name="multiRowDataListMapKey" value="rowListMap" />
				<input type="hidden" name="rowRightEnd" value="15" /> <input
					type="hidden" name="footerStartIndex" value="10" /> <input
					type="hidden" name="footerEndIndex" value="11" /> <input
					type="hidden" name="footerRightEnd" value="2" />

				<!-- TXT下載用 -->
				<input type="hidden" name="txtHeaderBottomEnd" value="11" /> <input
					type="hidden" name="txtHasRowData" value="true" /> <input
					type="hidden" name="txtHasFooter" value="true" /> <input
					type="hidden" name="txtMultiRowStartIndex" value="11" /> <input
					type="hidden" name="txtMultiRowEndIndex" value="11" /> <input
					type="hidden" name="txtMultiRowCopyStartIndex" value="7" /> <input
					type="hidden" name="txtMultiRowCopyEndIndex" value="11" /> <input
					type="hidden" name="txtMultiRowDataListMapKey" value="rowListMap" />

				<!-- 顯示區  -->
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<c:set var="all_i18n">
							<spring:message code='LB.All' />
						</c:set>
						<c:set var="ACN_replace"
							value="${fn:replace(inward_remittance_result.data.ACN, 'ALL', all_i18n) }" />
						<ul class="ttb-result-list">
							<li>
								<!-- 查詢時間 -->
								<h3>
									<spring:message code="LB.Inquiry_time" />
								</h3>
								<p>${inward_remittance_result.data.CMQTIME }</p>
							</li>
							<li>
								<!-- 查詢期間 -->
								<h3>
									<spring:message code="LB.Inquiry_period" />
								</h3>
								<p>${inward_remittance_result.data.CMPERIOD }</p>
							</li>
							<li>
								<!-- 查詢帳號 : -->
								<h3>
									<spring:message code="LB.W0132" />
								</h3>
								<p>${ACN_replace}</p>
							</li>
							<li>
								<!-- 資料總數 : -->
								<h3>
									<spring:message code="LB.Total_records" />
								</h3>
								<p>
									${inward_remittance_result.data.CMRECNUM} <spring:message code="LB.Rows" />
								</p>
							</li>
							<!--筆 -->
						</ul>
						<!-- 表格區塊 -->

						<table class="stripe table-striped ttb-table dtable"  data-show-toggle="first">
							<thead>
								<tr>
									<!--交易編號-->
									<th>
										<!--<spring:message code="LB.Change_date" />--> <spring:message
											code="LB.W0134" />
									</th>
									<!--入帳帳號-->
									<th data-breakpoints="xs sm">
										<!--<spring:message code="LB.Change_date" />--> <spring:message
											code="LB.W0135" />
									</th>
									<!-- 匯款人 -->
									<!-- 匯款銀行名稱 / SWIFT CODE -->
									<th data-breakpoints="xs sm">
										<!--  <spring:message code="LB.Summary_1" /> --> <spring:message
											code="LB.W0136" /><br>
									<spring:message code="LB.W0137" /> / SWIFT CODE
									</th>
									<!-- 匯入金額 -->
									<th data-breakpoints="xs">
										<!-- <spring:message code="LB.Withdrawal_amount" /> --> <spring:message
											code="LB.W0138" />
									</th>
									<!--通知日 -->
									<!--解款日 -->
									<th data-breakpoints="xs sm">
										<!-- <spring:message code="LB.Deposit_amount" /> --> <spring:message
											code="LB.W0139" /><br>
									<spring:message code="LB.W0140" />
									</th>
									<!-- 狀態 -->
									<th>
										<!-- <spring:message code="LB.Account_balance_2" /> --> <spring:message
											code="LB.Status" />
									</th>
									<!-- 匯款銀行  參考號碼 -->
									<th data-breakpoints="xs sm">
										<!--<spring:message code="LB.Checking_account" /> --> <spring:message
											code="LB.X0057" /><br>
									<spring:message code="LB.X1474" />
									</th>
									<!-- 匯款客戶名稱 -->
									<th data-breakpoints="xs sm">
										<!--<spring:message code="LB.Data_content" />--> <spring:message
											code="LB.X0024" />
									</th>
									<!-- 生效日 -->
									<!-- 退匯日期 -->
									<!-- 匯率 -->
									<th data-breakpoints="xs sm">
										<!--<spring:message code="LB.Receiving_Bank" />--> <spring:message
											code="LB.Effective_date" /><br>
									<spring:message code="LB.X0025" /><br>
									<spring:message code="LB.Exchange_rate" />
									</th>
									<!-- 附言 -->
									<th>
										<!--<spring:message code="LB.Trading_time" />--> <spring:message
											code="LB.W0156" />
									</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="dataList"
									items="${ inward_remittance_result.data.REC }">
									<tr>
										<!--交易編號-->
										<td class="text-center">${dataList.RREFNO }</td>
										<!--入帳帳號-->
										<td class="text-center">${dataList.RBANC }</td>
										<!-- 匯款人 -->
										<!-- 匯款銀行名稱 / SWIFT CODE -->
										<td class="text-center">${dataList.RORDNAME }<br>
											${dataList.RORDBAD1 }/${dataList.RORDBANK }
										</td>
										<!-- 匯入金額 -->
										<td class="text-center">${dataList.RREMITCY }<br>${dataList.RREMITAM }</td>
										<!--通知日 -->
										<!--解款日 -->
										<td class="text-center">${dataList.RADATE }<br>${dataList.RRTNDATE }</td>
										<!-- 狀態 -->
										<td class="text-center">
											<!--<c:choose>
                                            		<c:when test="${dataList.RSTATE == 'A' }">
                                            			通知
                                            		</c:when>
                                            		<c:when test="${dataList.RSTATE == 'S' }">
                                            			銷帳
                                            		</c:when>
                                            		<c:when test="${dataList.RSTATE == 'C' }">
                                            			退匯
                                            		</c:when>
                                            	</c:choose>--> <c:if
												test="${!dataList.RSTATE.equals('')}">
												<spring:message code="${dataList.RSTATE}" />
											</c:if>
										</td>
										<!-- 匯款銀行 參考號碼 -->
										<td class="text-center">${dataList.RREMBREF }</td>
										<!-- 匯款客戶名稱 -->
										<td class="text-center">${dataList.PORDCUS1 }</td>
										<!-- 生效日 -->
										<!-- 退匯日期 -->
										<!-- 匯率 -->
										<td class="text-center">${dataList.RVALDATE }<br>${dataList.RCNADATE }<br>${dataList.FEXRATE }</td>
										<!-- 附言 -->
										<td class="text-center">${dataList.RMEMO1 }<br>${dataList.RMEMO2 }<br>${dataList.RMEMO3 }<br>${dataList.RMEMO4 }</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>

						<ul class="ttb-result-list">
							<li>
								<!-- 匯入金額總金額 -->
								<h3>
									<!-- <spring:message code="LB.Inquiry_time" /> : -->
									<spring:message code="LB.W0142" />
								</h3> <!-- <spring:message code="LB.Inquiry_period" /> :--> <%--                                    	<c:forEach var="dataList" items="${inward_remittance_result.data.FXINAMTSUM}"> --%>
								<!--                                    		<p> --> <%--                                    			${ dataList.key } ${ dataList.value[0] } --%>
								<!-- <!-- 	                               			匯入總筆數 --> <%--                                    			<spring:message code="LB.D0360_1" /> --%>
								<%--                                    			<fmt:formatNumber minFractionDigits="0" maxFractionDigits="0" value="${ dataList.value[1] }" /> --%>
								<!-- <!-- 	                                        筆 --> <%-- 	                                        <spring:message code="LB.Rows" />  --%>
								<!--                                    		</p> --> <%--                                    	</c:forEach> --%>
								<p>
									<c:if test="${inward_remittance_result.data.AMTRREMITCY_1 != null && !inward_remittance_result.data.AMTRREMITCY_1.equals('')}">
                                   		${inward_remittance_result.data.AMTRREMITCY_1}&nbsp;
                                   		${inward_remittance_result.data.FXSUBAMT_1}&nbsp;
	                               		<!-- 匯入總筆數 -->
										<spring:message code="LB.D0360_1" />&nbsp;
										<fmt:formatNumber minFractionDigits="0" maxFractionDigits="0" value="${ inward_remittance_result.data.FXSUBAMTRECNUM_1 }" />&nbsp;
										<!--筆 -->
										<spring:message code="LB.Rows" />
										<br>
									</c:if>
									<c:if test="${inward_remittance_result.data.AMTRREMITCY_2 != null && !inward_remittance_result.data.AMTRREMITCY_2.equals('')}">
                                   		${inward_remittance_result.data.AMTRREMITCY_2}&nbsp;
                                   		${inward_remittance_result.data.FXSUBAMT_2}&nbsp;
	                               		<!-- 匯入總筆數 -->
										<spring:message code="LB.D0360_1" />&nbsp;
										<fmt:formatNumber minFractionDigits="0" maxFractionDigits="0" value="${ inward_remittance_result.data.FXSUBAMTRECNUM_2 }" />&nbsp;
										<!--筆 -->
										<spring:message code="LB.Rows" />
										<br>
									</c:if>
									<c:if test="${inward_remittance_result.data.AMTRREMITCY_3 != null && !inward_remittance_result.data.AMTRREMITCY_3.equals('')}">
                                   		${inward_remittance_result.data.AMTRREMITCY_3}&nbsp;
                                   		${inward_remittance_result.data.FXSUBAMT_3}&nbsp;
	                               		<!-- 匯入總筆數 -->
										<spring:message code="LB.D0360_1" />&nbsp;
										<fmt:formatNumber minFractionDigits="0" maxFractionDigits="0" value="${ inward_remittance_result.data.FXSUBAMTRECNUM_3 }" />&nbsp;
										<!--筆 -->
										<spring:message code="LB.Rows" />
										<br>
									</c:if>
									<c:if test="${inward_remittance_result.data.AMTRREMITCY_4 != null && !inward_remittance_result.data.AMTRREMITCY_4.equals('')}">
                                   		${inward_remittance_result.data.AMTRREMITCY_4}&nbsp;
                                   		${inward_remittance_result.data.FXSUBAMT_4}&nbsp;
	                               		<!-- 匯入總筆數 -->
										<spring:message code="LB.D0360_1" />&nbsp;
										<fmt:formatNumber minFractionDigits="0" maxFractionDigits="0" value="${ inward_remittance_result.data.FXSUBAMTRECNUM_4 }" />&nbsp;
										<!--筆 -->
										<spring:message code="LB.Rows" />
										<br>
									</c:if>
									<c:if test="${inward_remittance_result.data.AMTRREMITCY_5 != null && !inward_remittance_result.data.AMTRREMITCY_5.equals('')}">
                                   		${inward_remittance_result.data.AMTRREMITCY_1}&nbsp;
                                   		${inward_remittance_result.data.FXSUBAMT_5}&nbsp;
	                               		<!-- 匯入總筆數 -->
										<spring:message code="LB.D0360_1" />&nbsp;
										<fmt:formatNumber minFractionDigits="0" maxFractionDigits="0" value="${ inward_remittance_result.data.FXSUBAMTRECNUM_5 }" />&nbsp;
										<!--筆 -->
										<spring:message code="LB.Rows" />
										<br>
									</c:if>
									<c:if test="${inward_remittance_result.data.AMTRREMITCY_6 != null && !inward_remittance_result.data.AMTRREMITCY_6.equals('')}">
                                   		${inward_remittance_result.data.AMTRREMITCY_6}&nbsp;
                                   		${inward_remittance_result.data.FXSUBAMT_6}&nbsp;
	                               		<!-- 匯入總筆數 -->
										<spring:message code="LB.D0360_1" />&nbsp;
										<fmt:formatNumber minFractionDigits="0" maxFractionDigits="0" value="${ inward_remittance_result.data.FXSUBAMTRECNUM_6 }" />&nbsp;
										<!--筆 -->
										<spring:message code="LB.Rows" />
										<br>
									</c:if>
									<c:if test="${inward_remittance_result.data.AMTRREMITCY_7 != null && !inward_remittance_result.data.AMTRREMITCY_7.equals('')}">
                                   		${inward_remittance_result.data.AMTRREMITCY_7}&nbsp;
                                   		${inward_remittance_result.data.FXSUBAMT_7}&nbsp;
	                               		<!-- 匯入總筆數 -->
										<spring:message code="LB.D0360_1" />&nbsp;
										<fmt:formatNumber minFractionDigits="0" maxFractionDigits="0" value="${ inward_remittance_result.data.FXSUBAMTRECNUM_7 }" />&nbsp;
										<!--筆 -->
										<spring:message code="LB.Rows" />
										<br>
									</c:if>
									<c:if test="${inward_remittance_result.data.AMTRREMITCY_8 != null && !inward_remittance_result.data.AMTRREMITCY_8.equals('')}">
                                   		${inward_remittance_result.data.AMTRREMITCY_8}&nbsp;
                                   		${inward_remittance_result.data.FXSUBAMT_8}&nbsp;
	                               		<!-- 匯入總筆數 -->
										<spring:message code="LB.D0360_1" />&nbsp;
										<fmt:formatNumber minFractionDigits="0" maxFractionDigits="0" value="${ inward_remittance_result.data.FXSUBAMTRECNUM_8 }" />&nbsp;
										<!--筆 -->
										<spring:message code="LB.Rows" />
										<br>
									</c:if>
								</p>
							</li>
						</ul>
						<br>
						<!--button 區域 -->
						<!--回上頁 -->
						<spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>
						<input type="button" name="CMBACK" id="CMBACK" value="${cmback}"
							class="ttb-button btn-flat-gray">
						<!-- 繼續查詢 -->
						<c:if test="${ base_result.data.QUERYNEXT != null && !base_result.data.QUERYNEXT.equals('') }">
							<!--<spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>-->
							<input type="button" name="CMCONTINU" id="CMCONTINU" value="<spring:message code="LB.X0151" />"	class="ttb-button btn-flat-orange">
						</c:if>
						<!-- 列印  -->
						<spring:message code="LB.Print" var="printbtn"></spring:message>
						<input type="button" id="printbtn" value="${printbtn}"class="ttb-button btn-flat-orange" />
					</div>
				</div>
				<c:if
					test="${ base_result.data.QUERYNEXT != null && !base_result.data.QUERYNEXT.equals('') }">
					<!-- 說明： -->
					<ol class="description-list list-decimal">
						<p>
							<spring:message code="LB.Description_of_page" />
						</p>
						<li><span><spring:message
									code="LB.F_Inward_Remittance_P2_D1" /></span></li>
					</ol>
				</c:if>
			</form>


		</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>

	<script type="text/javascript">
		$(document).ready(function() {
			setTimeout("initDataTable()",100);
		});
	</script>

</body>
</html>