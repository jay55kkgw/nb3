<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function () {
			window.print();
		});
	</script>
</head>

<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br /><br />
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif" /></div>
	<br /><br />
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<br />
	<div>
	<!-- 查詢時間  -->
		<p>
			<spring:message code="LB.Inquiry_time" />
			 : ${CMQTIME}
		</p>
		<!--查詢期間 -->
		<p><spring:message code= "${QKIND}" /> : ${CMPERIOD }</p>
		<!--信用狀號碼 -->
		<p><spring:message code= "LB.L/C_no" /> : ${LCNO}</p>
		<!--託收編號 -->
		<p><spring:message code= "LB.W0086" />： ${REFNO}</p>
		<!--託收編號 -->
		<p>
		<!-- 資料總數  -->
		<spring:message code="LB.Total_records" />
		： ${CMRECNUM } <spring:message code="LB.Rows" />
		</p>
		
	
			<table class="print">

				<tr style="text-align: center">
					<!-- 通知編號-->
					<td>
						<spring:message code= "LB.W0086" />
					</td>
					<!-- 信用狀號碼 -->
					<td>
						<spring:message code= "LB.L/C_no" />
					</td>
					<!--開狀行-->
					<td>
						<spring:message code= "LB.W0091" />
					</td>
					<!--開狀/發電日期 -->
					<td>
						<spring:message code= "LB.W0092" />
					</td>
					<!-- 幣別 -->
					<td>
						<spring:message code="LB.Currency" />
					</td>
					<!-- 開狀金額-->
					<td>
						<spring:message code= "LB.W0094" />
					</td>
					<!-- 信用狀餘額 -->
					<td>
						<spring:message code= "LB.W0095" />
					</td>
					<!-- 有效日期 -->
					<td>
						<spring:message code= "LB.W0096" />
					</td>

				</tr>
				<c:forEach var="labelListMap" items="${print_datalistmap_data.REC}">
					<tr>
						<td>${labelListMap.RREFNO}</td>
						<td>${labelListMap.RLCNO}</td>
						<td>${labelListMap.RISSBK}</td>
						<td>${labelListMap.RRCVDATE}</td>
						<td>${labelListMap.RLCCCY}</td>
						<td>${labelListMap.RLCTXAMT}</td>
						<td>${labelListMap.RLCOSBAL}</td>
						<td>${labelListMap.REXPDATE}</td>
					</tr>
				</c:forEach>
			</table>
	</div>
	<div class="text-left">
		<p><spring:message code= "LB.W0097" />:</p>
		<table>
			<c:forEach var="dataList"
				items="${print_datalistmap_data.fcytotmat }">
				<tr>
					<td class="text-left">${dataList.currency}&nbsp;&nbsp;</td>
					<td class="text-right">${dataList.amount}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>

</html>