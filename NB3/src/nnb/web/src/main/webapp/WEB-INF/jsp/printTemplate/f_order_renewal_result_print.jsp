<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8" />
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function () {
			window.print();
		});
	</script>
</head>

<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br /><br />
	<div style="text-align:center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br /><br /><br />
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
  	<br/>
  	<div style="text-align:center">${MsgName}</div>
  	<br />
  	<table class="print">
	    <tr>
			<!-- 交易時間 -->
			<td style="width:8em"><spring:message code="LB.Trading_time" /></td>
			<td>${CMQTIME}</td>
	    </tr>
	    <tr>
			<!-- 存單帳號 -->
			<td class="ColorCell"><spring:message code="LB.Account_no" /></td>
	      	<td>${FYACN}</td>
	    </tr>
	    <tr>
		    <!-- 存單號碼 -->
	      	<td class="ColorCell"><spring:message code="LB.Certificate_no" /></td>
	      	<td>${FDPNUM}</td>
	    </tr>
	    <tr>
		    <!-- 存單金額 -->
	      	<td class="ColorCell"><spring:message code="LB.Certificate_amount" /></td>
	     	<td>
		        ${CRYNAME}
		        <fmt:formatNumber type="number" minFractionDigits="2" value="${AMTTSF}" />
				<!-- 元 -->
		        <spring:message code="LB.Dollar" />
	      	</td>
	    </tr>
	    <tr>
		    <!-- 存款種類 -->
	      	<td class="ColorCell"><spring:message code="LB.Deposit_certificate_type" /></td>
	     	<td>${DEPTYPE}</td>
	    </tr>
	    <tr>
		    <!-- 存款期別 -->
	      	<td class="ColorCell"><spring:message code="LB.Fixed_duration" /></td>
	      	<td class="DataCell">${TERM}</td>
	    </tr>
	    <tr>
		    <!-- 起存日 -->
	      	<td class="ColorCell"><spring:message code="LB.Start_date" /></td>
	      	<td class="DataCell">${SHOWDPISDT}</td>
	    </tr>
	    <tr>
		    <!-- 到期日 -->
	      	<td class="ColorCell"><spring:message code="LB.Maturity_date" /></td>
	      	<td class="DataCell">${SHOWDUEDAT}</td>
	    </tr>
	    <tr>
		    <!-- 計息方式 -->
			<td class="ColorCell"><spring:message code="LB.Interest_calculation" /></td>
	      	<td class="DataCell">${INTMTH}</td>
	    </tr>
	    <tr>
		    <!-- 利率 -->
	      	<td class="ColorCell"><spring:message code="LB.Interest_rate" /></td>
	      	<td class="DataCell">${ITR}%</td>
	    </tr>
	    <tr>
		    <!-- 利息轉入帳號 -->
	      	<td class="ColorCell"><spring:message code="LB.Interest_transfer_to_account" /></td>
			<td class="DataCell">${FYTSFAN}</td>
	    </tr>
	    <tr>
		    <!-- 原存單金額 -->
	      	<td class="ColorCell"><spring:message code="LB.Original_amount" /></td>
	      	<td>
		        <fmt:formatNumber type="number" minFractionDigits="2" value="${AMTFDP }" />
				<!-- 元 -->
		        <spring:message code="LB.Dollar" />
	      	</td>
	    </tr>
	    <tr>
		    <!-- 原存單利息 -->
			<td class="ColorCell"><spring:message code="LB.Original_interest" /></td>
	      	<td>
		        <fmt:formatNumber type="number" minFractionDigits="2" value="${INT}" />
				<!-- 元 -->
		        <spring:message code="LB.Dollar" />
	      	</td>
	    </tr>
	    <tr>
		    <!-- 代扣利息所得稅 -->
	      	<td class="ColorCell"><spring:message code="LB.Withholding_interest_income_tax"/></td>
	      	<td>
		        <fmt:formatNumber type="number" minFractionDigits="2" value="${FYTAX }" />
				<!-- 元 -->
		        <spring:message code="LB.Dollar" />
	      	</td>
	    </tr>
	    <tr>
			<!-- 健保費 -->
	      	<td class="ColorCell"><spring:message code="LB.NHI_premium" /></td>
	      	<td>
		        <fmt:formatNumber type="number" minFractionDigits="2" value="${NHITAX }" />
				<!-- 元 -->
		        <spring:message code="LB.Dollar" />
	      	</td>
	    </tr>
	</table>
	<br />
	<br />
	<div class="text-left">
		<!-- 說明  -->
		<spring:message code="LB.Description_of_page" />
		<ol class="list-decimal text-left">
			<li><spring:message code= "LB.F_order_renewal_P4_D1" /></li>
		</ol>
	</div>
	<br /><br /><br />
</body>

</html>