<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<style>
.content-faverte li::before {
    content: url(${__ctx}/img/caret-right-solid-gray.svg);
    display: inline-block;
    height: 20px;
    width: 8px;
    margin-right: 10px;
    vertical-align: sub;
    background-repeat: no-repeat;
}
</style>
<script type="text/javascript">
// 	var countdownObjheader, countdownSecHeader = 420;
// 	var minutes, seconds;
	var isTimeout = "${not empty sessionScope.timeout}";
	
	// var countdownObjheader = isTimeout=='true' ? "${sessionScope.timeout}" : 420;
	// var countdownSecHeader = isTimeout=='true' ? "${sessionScope.timeout}" : 420;
	var countdownObjheader = isTimeout=='true' ? "${sessionScope.timeout}" : 600;
	var countdownSecHeader = isTimeout=='true' ? "${sessionScope.timeout}" : 600;
	
	$(document).ready(function(){
		console.log('login_time: ' + '${sessionScope.tokenid}');
		var isLocal = '${sessionScope.tokenid}';
		console.log("isLocal: " + isLocal);
		console.log(isLocal != "LOCAL");
		// 非本機開發
		if( isLocal != "LOCAL" ){
			timeLogout();
		}
// 		$("#big_menu").show();
	});
	
	function timeLogout(){
		// 刷新session
		var uri = '${__ctx}/login_refresh';
		console.log('refresh.uri: '+uri);
		var result = fstop.getServerDataEx( uri, null, false, null);
		console.log('refresh.result: '+JSON.stringify(result));
		// 初始化登出時間
		$("#countdownheader").html(parseInt(countdownSecHeader)+1);
		$("#countdownMin").html("");
		$("#mobile-countdownMin").html("");
		$("#countdownSec").html("");
		$("#mobile-countdownSec").html("");
		// 倒數
		startIntervalHeader(1, refreshCountdownHeader, []);
	}

	function isValid(str) {
		// 長度 = 0 || 非數字	               
		return !(str.length === 0 || str.replace(/\D/g,'') !== str );
	}
	function refreshCountdownHeader(){
		// timeout剩餘時間
		var nextSec = parseInt($("#countdownheader").html()) - 1;
		$("#countdownheader").html(nextSec);
		
		// 提示訊息--即將登出，是否繼續使用
		if(nextSec == 120){
			initLogoutBlockUI();
		}
		// timeout
		if(nextSec == 0){
			// logout
			fstop.logout( '${__ctx}'+'/logout_aj' , '${__ctx}'+'/timeout_logout');
		}
		if (nextSec >= 0){
			// 倒數時間以分秒顯示
			var minutes = Math.floor(nextSec / 60);
			$("#countdownMin").html(('0' + minutes).slice(-2));
			$("#mobile-countdownMin").html(('0' + minutes).slice(-2));
			
			var seconds = nextSec - minutes * 60;
			$("#countdownSec").html(('0' + seconds).slice(-2));
			$("#mobile-countdownSec").html(('0' + seconds).slice(-2));
		}
	}
	function startIntervalHeader(interval, func, values){
		clearInterval(countdownObjheader);
		countdownObjheader = setRepeater(func, values, interval);
	}
	function setRepeater(func, values, interval){
		return setInterval(function(){
			func.apply(this, values);
		}, interval * 1000);
	}
	
	//contorl click outside can close menu
// 	$(document).click(function(e) {
// 		if (!$(e.target).is('.sec-menu')) {
// 	    	$('.collapse').collapse('hide');
// 	    }
// 	});
	
	
	

	/**
	 * 初始化logoutBlockUI
	 */
	function initLogoutBlockUI() {
		logoutblockUI();
	}

	/**
	 * 畫面BLOCK
	 */
	function logoutblockUI(timeout){
		$("#logout-block").show();
		
		// 遮罩後不給捲動
		document.body.style.overflow = "hidden";
		
		var defaultTimeout = 60000;
		if(timeout){	
			defaultTimeout = timeout;
		}
	}

	/**
	 * 畫面UNBLOCK
	 */
	function unLogoutBlockUI(timeoutID){
		if(timeoutID){
			clearTimeout(timeoutID);
		}
		$("#logout-block").hide();
		
		// 解遮罩後給捲動
		document.body.style.overflow = 'auto';
	}

	/**
	 *繼續使用
	 */
	function keepLogin(){
		unLogoutBlockUI(); // 解遮罩
		timeLogout(); // 刷新倒數計時
	}

</script>

<!-- 快速選單 -->
<section id="id-and-fast">
	<span class="id-name">
		<spring:message code="LB.Welcome_1"/>&nbsp;
		<c:if test="${empty sessionScope.susername}">
			<span id="username" name="username_show"><spring:message code="LB.X2190"/></span>
		</c:if>
		<c:if test="${not empty sessionScope.susername}">
			<span id="username" name="username_show">${sessionScope.susername}</span>
		</c:if>
		&nbsp;<spring:message code="LB.Welcome_2"/>
	</span> 
	<div id="id-block">
		<div>
		<span class="id-time">
			<fmt:setLocale value="${pageContext.response.locale}"/>
			
			<c:if test="${!empty sessionScope.logindt && !empty sessionScope.logintm}">
				<fmt:parseDate var="parseDate" value="${sessionScope.logindt} ${sessionScope.logintm}" pattern="yyyy-MM-dd HH:mm:ss" />
				<fmt:formatDate value="${parseDate}" dateStyle="full" type="both" />&nbsp;<spring:message code="LB.X2250"/>
				<br/>
			</c:if>
			
			<fmt:formatDate value="${sessionScope.login_time}" type="both" dateStyle="full" />
			&nbsp;<spring:message code="LB.Login_successful"/>
		</span>
		<!-- 自動登出剩餘時間 -->
		<span class="id-time"><spring:message code="LB.X1912"/>
			<span class="high-light ml-1">
				<!-- 分 -->
				<font id="countdownMin"></font>
				:
				<!-- 秒 -->
				<font id="countdownSec"></font>
			</span>
		</span>
		</div>
		<button type="button" class="btn-flat-orange" onclick="timeLogout()"><spring:message code="LB.X1913"/></button>
		<button type="button" class="btn-flat-darkgray" onClick="fstop.logout( '${__ctx}'+'/logout_aj' , '${__ctx}'+'/login')"><spring:message code="LB.Logout"/></button>
	</div>
	
	<!-- 快速選單目前先不開放
		<div id="fast-menu" class="collapse" role="tabpanel">
			<a href="#fast-menu-block" class="fast-menu-btn" data-toggle="collapse" data-parent="#fast-menu" aria-expanded="true" aria-controls="fast-menu-block" role="button">
			<spring:message code="LB.Quick_Menu"/></a>
		</div>
		<div id="fast-menu-block" class="collapse" role="tabpanel">
			<div id="mobile-header">
			    <img style="visibility: hidden;" src="${__ctx}/img/group-2.svg">
			    <a href="#fast-menu-block" data-toggle="collapse" data-parent="#fast-menu" aria-expanded="true" aria-controls="fast-menu-block" role="button">
			        <img src="${__ctx}/img/icon-close.svg">
			    </a>
			</div>
			<div class="mobile-fast-menu-title"><spring:message code="LB.Quick_Menu"/></div>
		    <ul>
		        <li>
					<a href='#'><spring:message code="LB.NTD_Services"/></a>
					<span onclick="fstop.getPage('${pageContext.request.contextPath}'+'/NT/ACCT/balance_query','', '')"><spring:message code="LB.NTD_Demand_Deposit_Balance"/></span>
				</li>
		        <li>
					<a href='#'><spring:message code="LB.NTD_Services"/></a>
					<span onclick="fstop.getPage('${pageContext.request.contextPath}'+'/NT/ACCT/TRANSFER/transfer','', '')"><spring:message code="LB.NTD_Transfer"/></span>
				</li>
		    </ul>
		</div>
	-->
</section>


<div class="fast-features-div">
    <button class="open-fast-features-btn" onclick="openFastFeatures()"><spring:message code='LB.Quick_Menu'/></button>
    <div id="fast-features-content" style="display: none;">
        <div>
            <span class="title"><spring:message code='LB.X2325'/> <span style="font-size: 16px;color:gray">(<spring:message code='LB.X2327'/> 5 <spring:message code='LB.Rows'/>)</span></span>
            <ul class="content content-faverte" id="usually_list">
            </ul>
        </div>
        <div>
            <span class="title"><spring:message code='LB.X2326'/> <span style="font-size: 16px;color:gray">(<spring:message code='LB.X2327'/> 10 <spring:message code='LB.Rows'/>)</span></span>
            <ul class="content content-faverte" id="favorite_list">
            </ul>
        </div>
    </div>
</div>

<input id="nb3login" name="nb3login" type="hidden" value="${sessionScope.tokenid}" />

<!-- 快速選單 -->
<script type="text/javascript">

	if (/*@cc_on!@*/false) { // check for Internet Explorer
		document.onfocusin = onFocus;
	} else {
		window.onfocus = onFocus;
	}
	
	function onFocus() {
		var checkUserResult = checkUser_aj();
		
		if (checkUserResult.result != true) {
			errorBlock(
				null, 
				null,
				['瀏覽器已於其他頁籤登入網路銀行，此頁籤將於三秒後回到登入頁'], 
				null,
				null
			);
			setTimeout(function(){ fstop.getPage('/nb3'+'/login','', ''); }, 3000);
		}
	}
	
	function checkUser_aj(){
		var checkUser_aj_uri = '${__ctx}' + "/MONITOR/checkUser";
		console.log("checkUser_aj.uri: " + checkUser_aj_uri);
		
		var rdata = {
			tokenid : $("#nb3login").val()
		};

		var checkResult = fstop.getServerDataEx(checkUser_aj_uri, rdata, false);
		console.log("checkResult: " + JSON.stringify(checkResult) );

		return checkResult;
	}
	
	
	function openFastFeatures() {
	    var x = document.getElementById("fast-features-content");
	    if (x.style.display === "none") {
	        x.style.display = "block";
	    } else {
	        x.style.display = "none";
	    }
	}
	
	
	function addFavorite() {
		var result = false;
		$.ajax({
			type :"POST",
	        url  : "${__ctx}/PERSONALIZE/add",
	        async: false ,
	        dataType: "json",
	        success : function(data) {
	        	console.log("addFavorite");
	        	console.log(data);
	        	if(data.result){
	        		if(data.data.overTen){
	    				errorBlock(
    						null, 
    						null,
    						["已超過10筆"], 
    						'<spring:message code= "LB.Confirm" />', 
    						null
    					);
	        		}
	        		result = !data.data.overTen;
	        	}
	        }
		});
		
       	getFavoriteList(); // 重新整理我的最愛清單
       	return result;
	}
	
	function deleteFavorite() {
		$.ajax({
			type :"POST",
	        url  : "${__ctx}/PERSONALIZE/delete",
	        async: false ,
	        dataType: "json",
	        success : function(data) {
	        	console.log("deleteFavorite");
	        	console.log(data);
	        }
		});
		
       	getFavoriteList(); // 重新整理我的最愛清單
	}
	
	function deleteFavoriteById(adopid) {
		$.ajax({
			type :"POST",
			data : { 
	        	ADOPID: adopid
	        },
	        url  : "${__ctx}/PERSONALIZE/deleteById",
	        async: false ,
	        dataType: "json",
	        success : function(data) {
	        	console.log("deleteFavorite");
	        	console.log(data);
	        }
		});
		
       	getFavoriteList(); // 重新整理我的最愛清單
       	
     	// 實心變空心
		if ($('.fa').hasClass("fa-star")) {
			$(".fa.fa-star").addClass("fa-star-o");
			$(".fa.fa-star").removeClass("fa-star");
		}
	}
	
	
	$(document).ready(function(){
		visibleStar();
		getUsuallyList();
		getFavoriteList();
	});
	
	
	function visibleStar() {
		var showStar = "${showStar}";
		
		// 顯示實心星星即可點擊
		if (showStar == 'true') {
			$(".fa-star").show();
			$('.fa').click(function() {
				// 實心變空心
				if ($('.fa').hasClass("fa-star")) {
					$(this).addClass("fa-star-o");
					$(this).removeClass("fa-star");
					
					deleteFavorite();
				}
				// 空心變實心
				else if ($('.fa').hasClass("fa-star-o")) {
					$(this).addClass("fa-star");
					$(this).removeClass("fa-star-o");
					
					if(!addFavorite()){
						$(this).addClass("fa-star-o");
						$(this).removeClass("fa-star");
					}
				}
			});
			
		} // 顯示空心星星即可點擊
		else if (showStar == 'false') {
			$(".fa-star").addClass("fa-star-o");
			$(".fa-star").removeClass("fa-star");
			$(".fa-star-o").show();
			
			$('.fa').click(function() {
				// 實心
				if ($('.fa').hasClass("fa-star")) {
					$(this).addClass("fa-star-o");
					$(this).removeClass("fa-star");
					
					deleteFavorite();
				}
				// 空心
				else if ($('.fa').hasClass("fa-star-o")) {
					$(this).addClass("fa-star");
					$(this).removeClass("fa-star-o");

					if(!addFavorite()){
						$(this).addClass("fa-star-o");
						$(this).removeClass("fa-star");
					}
				}
			});
			
		} else {
			// 不顯示星星
			$(".fa-star").hide();
			$(".fa-star-o").hide();
		}
		
	}
	
	function getUsuallyList() {
		$("#usually_list").empty();
		
		$.ajax({
			type :"POST",
	        url  : "${__ctx}/INDEX/getUsuallyList",
	        async: false ,
	        dataType: "json",
	        success : function(data) {
	        	console.log("getUsuallyList");
	        	console.log(data);
	        	data.data.usuallyList.forEach(function(e){
	        		if($("#menu_" + e.ADOPID).length > 0){
			        	console.log("title >>" + $("#" + $("#menu_" + e.ADOPID).parents(".sec-menu").attr('id') + "-title").text());
			        	console.log($("#menu_" + e.ADOPID).find($("a")).attr("target"));
			        	if( $("#menu_" + e.ADOPID).find($("a")).attr("target") == "_blank" ){

							<c:if test="${__i18n_locale eq 'en' }">
								$("#usually_list").append("<li><a target='_blank' href='${__ctx}" + e.URL + "'>" + $("#" + $("#menu_" + e.ADOPID).parents(".sec-menu").attr('id') + "-title").text() + "<br>&nbsp;&nbsp;&nbsp;－" + e.ADOPENGNAME + "</a></li>");
				    		</c:if>
				    		<c:if test="${__i18n_locale eq 'zh_TW' }">
				    			$("#usually_list").append("<li><a target='_blank' href='${__ctx}" + e.URL + "'>" + $("#" + $("#menu_" + e.ADOPID).parents(".sec-menu").attr('id') + "-title").text() + "<br>&nbsp;&nbsp;&nbsp;－" + e.ADOPNAME + "</a></li>");
				    		</c:if>
				    		<c:if test="${__i18n_locale eq 'zh_CN' }">
				    			$("#usually_list").append("<li><a target='_blank' href='${__ctx}" + e.URL + "'>" + $("#" + $("#menu_" + e.ADOPID).parents(".sec-menu").attr('id') + "-title").text() + "<br>&nbsp;&nbsp;&nbsp;－" + e.ADOPCHSNAME + "</a></li>");
				    		</c:if>
				    		
			        	}else{

							<c:if test="${__i18n_locale eq 'en' }">
								$("#usually_list").append("<li><a href='${__ctx}" + e.URL + "'>" + $("#" + $("#menu_" + e.ADOPID).parents(".sec-menu").attr('id') + "-title").text() + "<br>&nbsp;&nbsp;&nbsp;－" + e.ADOPENGNAME + "</a></li>");
				    		</c:if>
				    		<c:if test="${__i18n_locale eq 'zh_TW' }">
				    			$("#usually_list").append("<li><a href='${__ctx}" + e.URL + "'>" + $("#" + $("#menu_" + e.ADOPID).parents(".sec-menu").attr('id') + "-title").text() + "<br>&nbsp;&nbsp;&nbsp;－" + e.ADOPNAME + "</a></li>");
				    		</c:if>
				    		<c:if test="${__i18n_locale eq 'zh_CN' }">
				    			$("#usually_list").append("<li><a href='${__ctx}" + e.URL + "'>" + $("#" + $("#menu_" + e.ADOPID).parents(".sec-menu").attr('id') + "-title").text() + "<br>&nbsp;&nbsp;&nbsp;－" + e.ADOPCHSNAME + "</a></li>");
				    		</c:if>
			        	}
		    		}
	        	})
	        }
		})
	}
	
	function getFavoriteList() {
		$("#favorite_list").empty();
		
		$.ajax({
			type :"POST",
	        url  : "${__ctx}/INDEX/getFavoriteList",
	        async: false ,
	        dataType: "json",
	        success : function(data) {
	        	console.log("getFavoriteList");
	        	console.log(data);
	        	data.data.favoriteList.forEach(function(e){
	        		if($("#menu_" + e.ADOPID).length > 0){
			        	if( $("#menu_" + e.ADOPID).find($("a")).attr("target") == "_blank" ){
							<c:if test="${__i18n_locale eq 'en' }">
								$("#favorite_list").append("<li><a target='_blank' href='${__ctx}" + e.URL + "'>" + $("#" + $("#menu_" + e.ADOPID).parents(".sec-menu").attr('id') + "-title").text() + "<br>&nbsp;&nbsp;&nbsp;－" + e.ADOPENGNAME + "</a><button type='button' onclick='deleteFavoriteById(" + '"' + e.ADOPID + '"' + ")'><i class='fa fa-trash'></i></button></li>");
				    		</c:if>
				    		<c:if test="${__i18n_locale eq 'zh_TW' }">
				    			$("#favorite_list").append("<li><a target='_blank' href='${__ctx}" + e.URL + "'>" + $("#" + $("#menu_" + e.ADOPID).parents(".sec-menu").attr('id') + "-title").text() + "<br>&nbsp;&nbsp;&nbsp;－" + e.ADOPNAME + "</a><button type='button' onclick='deleteFavoriteById(" + '"' + e.ADOPID + '"' + ")'><i class='fa fa-trash'></i></button></li>");
				    		</c:if>
				    		<c:if test="${__i18n_locale eq 'zh_CN' }">
				    			$("#favorite_list").append("<li><a target='_blank' href='${__ctx}" + e.URL + "'>" + $("#" + $("#menu_" + e.ADOPID).parents(".sec-menu").attr('id') + "-title").text() + "<br>&nbsp;&nbsp;&nbsp;－" + e.ADOPCHSNAME + "</a><button type='button' onclick='deleteFavoriteById(" + '"' + e.ADOPID + '"' + ")'><i class='fa fa-trash'></i></button></li>");
				    		</c:if>
			        	}else{
							<c:if test="${__i18n_locale eq 'en' }">
								$("#favorite_list").append("<li><a href='${__ctx}" + e.URL + "'>" + $("#" + $("#menu_" + e.ADOPID).parents(".sec-menu").attr('id') + "-title").text() + "<br>&nbsp;&nbsp;&nbsp;－" + e.ADOPENGNAME + "</a><button type='button' onclick='deleteFavoriteById(" + '"' + e.ADOPID + '"' + ")'><i class='fa fa-trash'></i></button></li>");
				    		</c:if>
				    		<c:if test="${__i18n_locale eq 'zh_TW' }">
				    			$("#favorite_list").append("<li><a href='${__ctx}" + e.URL + "'>" + $("#" + $("#menu_" + e.ADOPID).parents(".sec-menu").attr('id') + "-title").text() + "<br>&nbsp;&nbsp;&nbsp;－" + e.ADOPNAME + "</a><button type='button' onclick='deleteFavoriteById(" + '"' + e.ADOPID + '"' + ")'><i class='fa fa-trash'></i></button></li>");
				    		</c:if>
				    		<c:if test="${__i18n_locale eq 'zh_CN' }">
				    			$("#favorite_list").append("<li><a href='${__ctx}" + e.URL + "'>" + $("#" + $("#menu_" + e.ADOPID).parents(".sec-menu").attr('id') + "-title").text() + "<br>&nbsp;&nbsp;&nbsp;－" + e.ADOPCHSNAME + "</a><button type='button' onclick='deleteFavoriteById(" + '"' + e.ADOPID + '"' + ")'><i class='fa fa-trash'></i></button></li>");
				    		</c:if>
			        	}
	        		}
	        	})
	        }
		})
	}
</script>
