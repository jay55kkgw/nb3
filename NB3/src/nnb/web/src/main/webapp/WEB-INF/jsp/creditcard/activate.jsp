<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
<script type="text/javascript" src="${__ctx}/js/jquery.maskedinput.js"></script>

<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		// 初始化時隱藏span
		$("#hideblock").hide();
		
// 		var minMonth = 1;
// 		var maxMonth = 12;
// 		var minYear = new Date().getFullYear();
// 		var maxYear = minYear + 7;

// 		selectMonth = document.getElementById('EXP_MON');
// 		selectYear = document.getElementById('EXP_YEAR');

// 		for (var i = minMonth; i <= maxMonth; i++) {
// 			var optMonth = document.createElement('option');
// 			if(i<10){
// 				optMonth.value = "0"+i;
// 				optMonth.innerHTML = "0"+i;
// 				selectMonth.appendChild(optMonth);
// 			}else{
// 				optMonth.value = i;
// 				optMonth.innerHTML = i;
// 				selectMonth.appendChild(optMonth);
// 			}
// 		}
// 		for (var i = minYear; i <= maxYear; i++) {
// 			var optYear = document.createElement('option');
			
// 			optYear.value = (i.toString()).substring(2);
// 			optYear.innerHTML = i;
// 			selectYear.appendChild(optYear);
// 		}
		init();

	});
	function init() {
		$("#formId").validationEngine({
			binded : false,
			promptPosition : "inline"
		});
		$('#CMSUBMIT').click(function(e){
			//送出進表單驗證前將span顯示
			$("#hideblock").show();
			console.log("submit~~");
			//塞值進span內的input
			open_creditcard();
			var card = $("#CARDNUM_1").val()+$("#CARDNUM_2").val()+$("#CARDNUM_3").val()+$("#CARDNUM_4").val()
			$("#CARDNUM_TOTAL").val(card);
			if (!$('#formId').validationEngine('validate')) {
				e.preventDefault();
			} else {
				var date = $("#EXP_YEAR").val()+$("#EXP_MON").val()
				$("#EXP_DATE").val(date);
				var ACT_PASS = $('#ACT_PASS').val();
				$('#PINNEW').val(pin_encrypt($('#CMPASSWORD').val()));
				$('#ACT_PASS').val(ACT_PASS);
				initBlockUI();
				$("#formId").validationEngine('detach');
				$("form").submit();
			}
		});
	}
	
	function open_creditcard(){
		var cardnum = $("#CARDNUM_input").val();
		$("#CARDNUM_1").val(cardnum.substring(0,4));
		$("#CARDNUM_2").val(cardnum.substring(5,9));
		$("#CARDNUM_3").val(cardnum.substring(10,14));
		$("#CARDNUM_4").val(cardnum.substring(15));
		var craddate = $("#EXPIREDYM").val();
		$("#EXP_YEAR").val(craddate.substring(3));
		$("#EXP_MON").val(craddate.substring(0,2));
	}
	
	jQuery(function($) {
		$("#CARDNUM_input").mask("9999/9999/9999/9999",{autoclear: false});
		$("#EXPIREDYM").mask("99/99",{autoclear: false});
	});
</script>
</head>
<body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 信用卡     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Credit_Card" /></li>
    <!-- 信用卡開卡     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Card_Activation" /></li>
		</ol>
	</nav>


	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->


		<main class="col-12">
		<section id="main-content" class="container">
			<h2><spring:message code="LB.Card_Activation" /></h2>
			
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<form id="formId" method="post" action="${__ctx}/CREDIT/APPLY/card_activate_step1">
				<input type="hidden" id="EXP_DATE" name="EXP_DATE" value="">
				<input type="hidden" id="PINNEW" name="PINNEW" value="">
				<div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">

                            <!-- 交易時間-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.Trading_time" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                     <span>${CMQTIME}</span>
                                    </div>
                                </span>
                            </div> 
                                
                                <!-- 信用卡卡號 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.Credit_card_number" /></h4>
                                    </label>
                                </span>
                                
                                <span class="input-block">
                                    <div class="ttb-input">
<!--                                     	<input type="text" id="CARDNUM_input" name="CARDNUM" class="text-input validate[required,funcCall[validate_CheckNumWithDigit[信用卡號,CARDNUMCHK,20]]]" placeholder="____ / ____ / ____ / ____" style="text-align:center"> -->
                                    	<input type="text" id="CARDNUM_input" class="text-input" placeholder="____ / ____ / ____ / ____" style="text-align:center">
										<input type="hidden" id="CARDNUM_1" name="CARDNUM_1"/>
										<input type="hidden" id="CARDNUM_2" name="CARDNUM_2"/>
										<input type="hidden" id="CARDNUM_3" name="CARDNUM_3"/>
										<input type="hidden" id="CARDNUM_4" name="CARDNUM_4"/>
										<input type="hidden" id="CARDNUMCHK"/>
<!-- 			                            <input type="hidden" class="card-input text-input" id="CARDNUM_1" name="CARDNUM_1" maxlength="4" onKeyUp="setBlur(this,'CARDNUM_2')"/> -->
<!--                                         <input type="hidden" class="card-input text-input" id="CARDNUM_2" name="CARDNUM_2" maxlength="4" onKeyUp="setBlur(this,'CARDNUM_3')"/> -->
<!--                                         <input type="hidden" class="card-input text-input" id="CARDNUM_3" name="CARDNUM_3" maxlength="4" onKeyUp="setBlur(this,'CARDNUM_4')"/> -->
<!--       	                                <input type="hidden" class="card-input text-input" id="CARDNUM_4" name="CARDNUM_4" maxlength="4"/> -->
                                    </div>
                                    <!-- 不在畫面上顯示的span -->
									<span id="hideblock" >
										<!-- 驗證用的input -->
										<input id="CARDNUM_TOTAL" name="CARDNUM" type="text" maxlength="16"  class="text-input validate[required,minSize[16],custom[onlyNumberSp]]" 
											style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
									</span>
                                </span>
                            </div>    
                            
                            <!-- 卡片效期 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.Card_validity" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                    <input type="text" class="text-input validate[required,funcCall[validate_chkperiod['sFieldName',EXP_YEAR,EXP_MON]]]" name="EXPIREDYM" id="EXPIREDYM" value="" placeholder="MM/YY" style="text-align:center">
<!--                                     <input type="text" class="text-input" name="EXPIREDYM" id="EXPIREDYM" value="" placeholder="MM/YY" style="text-align:center"> -->
                                    <input type="hidden" name="EXP_YEAR" id="EXP_YEAR" value="">
                                    <input type="hidden" name="EXP_MON" id="EXP_MON" value="">
<!--                                     <select name="EXP_MON" id="EXP_MON" class="custom-select" size="1" style="width: 90px !important;"> -->
<!--                                      </select>  -->
<%--                                         <span class="input-unit">&nbsp;(<spring:message code="LB.Month" />)&nbsp;&nbsp;/&nbsp;&nbsp;</span> --%>
<!--                                     <select name="EXP_YEAR" id="EXP_YEAR" class="custom-select" size="1" style="width: 110px !important;"> -->
<!--                                     </select> -->
<%--                                     <span class="input-unit">(<spring:message code="LB.Year" />)</span> --%>
                                    </div>
                                </span>
                            </div>
                                    
                                  <!-- 開卡密碼 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.Open_card_password" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
			                           <input class="text-input validate[required,custom[onlyLetterNumber]]" type="password" maxLength="6" name="ACT_PASS" id="ACT_PASS"/><br>
                                       <span class="input-unit"><spring:message code="LB.Activate_P1_note1" /></span>
                                    </div>
                                </span>
                            </div> 
                                
                            <!-- 交易機制 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.Transaction_security_mechanism" /></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <label class="radio-block"> 
                                           	<spring:message code="LB.SSL_password" />
                                            <input type="radio" name="FGTXWAY" value="0" checked>
                                            <span class="ttb-radio"></span>
                                        </label>
                                    </div>
                                    <div class="ttb-input"> 
                                        <input type="password" class="text-input validate[required,custom[onlyLetterNumber]]" name="CMPASSWORD" id="CMPASSWORD" maxlength="8" >
                                    </div>
                                </span>
                            </div>
                                
                        </div>
                        <input type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" />
                    	<input type="button" id="CMSUBMIT" class="ttb-button btn-flat-orange" name="CMSUBMIT" value="<spring:message code="LB.Confirm" />" />
                    </div>
                </div>
               				<ol class="description-list list-decimal">
								<p><spring:message code="LB.Remind_you" /></p>
									<li><spring:message code="LB.Activate_P1_D1" /></li>
									<!-- 本交易僅限本行發行各種有效信用卡持卡人使用（商務卡、採購卡、簽帳金融卡及悠遊聯名金融卡恕不適用此交易）。 -->
									<li><spring:message code="LB.Activate_P1_D2" /></li>
									<!-- 為安全起見，請核對卡號無誤後，使用原子筆或油性簽字筆於卡片背面簽名處簽名。 -->
									<li><spring:message code="LB.Activate_P1_D3" /></li>
									<!-- 本卡限本人使用，不可讓與、轉借，提供擔保或以其它方式將卡片轉交第三人使用。 -->
								</ol>
			</form>
		</section>
		</main>
	</div>
	
	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>