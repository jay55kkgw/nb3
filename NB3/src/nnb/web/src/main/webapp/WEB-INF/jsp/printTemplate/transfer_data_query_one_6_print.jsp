<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function(){
			window.print();
		});
	</script>
</head>

<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br/><br/>
	<div style="text-align:center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br/><br/><br/>
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<br/><br/><br/>
	
	<table class="print">
		<tr>
			<!-- 合併日期 -->
			<th><spring:message code="LB.W1001" /></th>
			<th>${TRADEDATE_F }</th>
		</tr>
		<tr>
			<!-- 信託編號 -->
			<th><spring:message code="LB.W0904" /></th>
			<th>${CDNO_F }</th>
		</tr>
		<tr>
			<!-- 轉出基金名稱 -->
			<th><spring:message code="LB.W0963" /></th>
			<th>${TRANSCODE_FUNDLNAME }</th>
		</tr>
		<tr>
			<!-- 基準日 -->
			<th><spring:message code="LB.W0028" /></th>
			<th>${TRADEDATE_F }</th>
		</tr>
		<tr>
			<!-- 轉出信託金額 -->
			<th><spring:message code="LB.W1116" /></th>
			<th>${CRY_CRYNAME } ${AMT1_F }</th>
		</tr>
		<tr>
			<!-- 轉出單位數 -->
			<th><spring:message code="LB.W1122" /></th>
			<th>${UNIT_F }</th>
		</tr>
		<tr>
			<!-- 轉出單位淨值 -->
			<th><spring:message code="LB.X0384" /></th>
			<th>${CRY_CRYNAME } ${PRICE1_F }</th>
		</tr>
		<tr>
			<!-- 併入基金名稱 -->
			<th><spring:message code="LB.W1002" /></th>
			<th>${INTRANSCODE_FUNDLNAME }</th>
		</tr>
		<tr>
			<!-- 併入後信託金額 -->
			<th><spring:message code="LB.W1003" /></th>
			<th>${NEWCRY } ${EXAMT_F }</th>
		</tr>
		<tr>
			<!-- 併入後信託單位數 -->
			<th><spring:message code="LB.W1004" /></th>
			<th>${TXUNIT_F } </th>
		</tr>
		<tr>
			<!-- 併入單位淨值 -->
			<th><spring:message code="LB.W1005" /></th>
			<th>${INTRANSCRY_CRYNAME } ${PRICE2_F }</th>
		</tr>
		<tr>
			<!-- 轉換匯率 -->
			<th><spring:message code="LB.W1006" /></th>
			<th>${EXRATE_F }</th>
		</tr>
	</table>
	<br/><br/><br/><br/>
</body>
</html>