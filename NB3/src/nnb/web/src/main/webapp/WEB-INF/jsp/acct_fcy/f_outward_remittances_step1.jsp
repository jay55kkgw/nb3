<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript">
		var i18nValue = {};
		i18nValue['LB.Select'] = '<spring:message code="LB.Select"/>'; //請選擇
		i18nValue['available_balance'] = '<spring:message code="LB.Available_balance" />: ';
		// 初始化
		$(document).ready(function () {
			init();
		});

		function init() {
			// 初始化時隱藏span
			$("#hideblock").hide();
			//表單驗證
			$("#formId").validationEngine({binded: false,promptPosition: "inline"});
			//日曆欄位參數設定
			datetimepickerEvent();
			// 判斷是否可以進行即時交易
			isFxBizTime();
			//預約日期change事件 ，檢核日期邏輯
			// fgtrdateEvent();
			// 及時及預約標籤切換事件
			// tabEvent();
			//預約自動輸入明天
			getTmr();
			// 確認鍵 click
			goOn();
			//每月日期下拉
			createCMPERIOD();
			//預設寫入幣別
			creatCryList('PAYCCY');
			creatCryList('REMITCY');
			creatCryList('COMMCCY');
			//acn
			var getacn ="";
			var getacn1 = '${f_outward_remittances_step1.data.urlID}';
			var getacn2 = '${f_outward_remittances_step1_r.data.urlID}';
			if(getacn1 != null && getacn1 != ''){
				getacn = getacn1;
			}
			else{
				getacn = getacn2;
			}
			if(getacn != null && getacn != ''){
				$("#CUSTACC option[value= '"+ getacn +"' ]").prop("selected", true);
			}
			
			// 手續費負擔別選擇OUR時直接跳出國外費用說明
			$('input[type=radio][name=DETCHG]').change(function() {
				if (this.value == 'OUR') {
					window.open('${__ctx}/public/ourFeeRate.htm');
				}
			});
		}
		
	 	// 判斷是否可以進行即時交易
		function isFxBizTime(){
			var uri = '${__ctx}' + "/FCY/COMMON/isFxBizTime_aj"
			var rdata = {};
			var data = fstop.getServerDataEx(uri, rdata, false, isFxBizTime_aj);
		}
		// 判斷是否可以進行即時交易
		function isFxBizTime_aj(data) {
			console.log("isFxBizTime_aj data: " + data);
			if (data && data.result) {
				// 及時及預約標籤切換事件
				console.log("<spring:message code= "LB.X1476" />");
				tabEvent();
			}else{
				console.log("<spring:message code= "LB.X1477" />");
				// 顯示預約狀態的欄位
				$("#transfer-date").show();
				fstop.setRadioChecked("FGTRDATE", "1", true);
				// 切換成預約，即時標籤不可使用
				$("#nav-trans-now").removeClass("active");
				$("#nav-trans-future").addClass("active");
				$("#nav-trans-now").css("pointer-events", "none");
				$("#nav-trans-now").css("display", "inline-block");
			}
		}

		//及時及預約標籤切換事件
		function tabEvent() {
			$("#nav-trans-now").click(function () {
				console.log("hi>>");
				$("#transfer-date").hide();
				$("input[name='FGTRDATE']:eq(0)").prop("checked", true).trigger("change");
			})
			$("#nav-trans-future").click(function () {
				$("#transfer-date").show();
				$("input[name='FGTRDATE']:eq(1)").prop("checked", true).trigger("change");
			})
		}
		// 預約日期change事件 ，檢核日期邏輯
		function fgtrdateEvent() {
			var sMinDate = '${f_outward_remittances_step1.data.nowDate}';
			var sMaxDate = '${f_outward_remittances_step1.data.sNextYearDay}';
			console.log("processQuery.sMinDate: " + sMinDate);
			console.log("processQuery.sMaxDate: " + sMaxDate);
			$('input[type=radio][name=FGTRDATE]').change(function () {
				console.log("fgtrdateEvent.value>>>>"+  this.value);
				if (this.value == '1') {
					console.log("tomorrow");
					// 驗證是否是可預約單日之日期
					$("#H_CMTRDATE").addClass("validate[required ,funcCall[validate_CheckDate[<spring:message code= "LB.X1449" />,CMTRDATE," + sMinDate + "," + sMaxDate + "]]]");
					// 取消驗證日期區間
					$("#H_CMSDATE").removeClass();
					// 取消驗證日期區間
					$("#H_CMEDATE").removeClass();
					// 取消驗證預約週期
					$("#H_CMPERIOD").removeClass();
				} 
				//
				else if (this.value == '2') {
					console.log("Transfer Thai Gayo");
					// 驗證日期區間
					$("#H_CMSDATE").addClass("validate[required ,funcCall[validate_CheckDate[<spring:message code= "LB.X1449" />, CMSDATE," + sMinDate + "," + sMaxDate + "]]]");
					$("#H_CMEDATE").addClass("validate[required ,funcCall[validate_CheckDate[<spring:message code= "LB.X1449" />, CMEDATE," + sMinDate + "," + sMaxDate + "]]]");
					// 驗證週期日
					$("#H_CMPERIOD").addClass("validate[funcCall[validate_CheckSelect[<spring:message code= "LB.X1445" />, H_CMPERIOD, #]]]");
					// 取消驗證是否是可預約單日之日期
					$("#H_CMTRDATE").removeClass();
				} else {
					// 取消驗證是否是可預約單日之日期
					$("#H_CMTRDATE").removeClass();
					// 取消驗證週期日期、區間
					$("#H_CMPERIOD").removeClass();
					$("#H_CMSDATE").removeClass();
					$("#H_CMEDATE").removeClass();
				}
			});
			
			// 20210322-表單驗證欄位檢核會異常
			// $('input[type=radio][name=FGTRDATE]').trigger('change');
		}
		
		function fgtrdateEvent2() {
			var sMinDate = '${f_outward_remittances_step1.data.nowDate}';
			var sMaxDate = '${f_outward_remittances_step1.data.sNextYearDay}';
			console.log("processQuery.sMinDate: " + sMinDate);
			console.log("processQuery.sMaxDate: " + sMaxDate);
			
			var main = document.getElementById("formId");
			if (main.FGTRDATE[1].checked) {
				console.log("tomorrow");
				// 驗證是否是可預約單日之日期
				$("#H_CMTRDATE").addClass("validate[required ,funcCall[validate_CheckDate[<spring:message code= "LB.X1449" />,CMTRDATE," + sMinDate + "," + sMaxDate + "]]]");
				// 取消驗證日期區間
				$("#H_CMSDATE").removeClass();
				// 取消驗證日期區間
				$("#H_CMEDATE").removeClass();
				// 取消驗證預約週期
				$("#H_CMPERIOD").removeClass();
			} 
			//
			else if (main.FGTRDATE[2].checked) {
				console.log("Transfer Thai Gayo");
				// 驗證日期區間
				$("#H_CMSDATE").addClass("validate[required ,funcCall[validate_CheckDate[<spring:message code= "LB.X1449" />, CMSDATE," + sMinDate + "," + sMaxDate + "]]]");
				$("#H_CMEDATE").addClass("validate[required ,funcCall[validate_CheckDate[<spring:message code= "LB.X1449" />, CMEDATE," + sMinDate + "," + sMaxDate + "]]]");
				// 驗證週期日
				$("#H_CMPERIOD").addClass("validate[funcCall[validate_CheckSelect[<spring:message code= "LB.X1445" />, H_CMPERIOD, #]]]");
				// 取消驗證是否是可預約單日之日期
				$("#H_CMTRDATE").removeClass();
			} else {
				// 取消驗證是否是可預約單日之日期
				$("#H_CMTRDATE").removeClass();
				// 取消驗證週期日期、區間
				$("#H_CMPERIOD").removeClass();
				$("#H_CMSDATE").removeClass();
				$("#H_CMEDATE").removeClass();
			}
		}

		// 確認鍵 click
		function goOn() {
			$("#CMSUBMIT").click(function (e) {
				// 去掉舊的錯誤提示訊息
				$(".formError").remove();
				//打開驗證隱藏欄位
				$("input[name='hideblock']").show();
				//塞值進span內的input
				//日期
				$("#H_CMPERIOD").val($("#CMPERIOD").val());
				//單日之日期
				$("#H_CMTRDATE").val($("#CMTRDATE").val());
				//日期區間-起
				$("#H_CMSDATE").val($("#CMSDATE").val());
				//日期區間-迄
				$("#H_CMEDATE").val($("#CMEDATE").val());
				
				$("#H_CMTRMAIL").val($("#CMTRMAIL").val());

				if($("#FGTRDATE2").prop('checked')){
					$("#validate_CMEDATE2").show();
				}else{
					$("#validate_CMEDATE2").hide();
				}
				
				fgtrdateEvent2();
				
				if (!$('#formId').validationEngine('validate')) {
					e = e || window.event; // for IE
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					prepareNextPageData();
					if(processQuery()){
						initBlockUI(); //遮罩
						//移除 disabled
						$('#REMITCY').removeAttr('disabled');
						$('#BENTYPE').removeAttr('disabled');
						$("#formId").submit();
					}
				}
			});
		}
		//檢核欄位
		function processQuery() {
			var payccy = $("#PAYCCY").find(":selected").val(); //付款幣別
			var remitcy = $("#REMITCY").find(":selected").val(); //收款幣別
			//欄位檢核 start   	       		   	
			// 表單驗證--Start
			var main = document.getElementById("formId");
			if ((main.COMMACC.options[main.COMMACC.selectedIndex].value != '#') ||
				(main.COMMCCY.options[main.COMMCCY.selectedIndex].value != '#')) {
				if (main.PAYCCY.options[main.PAYCCY.selectedIndex].value != 'TWD') {
					if ((main.COMMCCY.options[main.COMMCCY.selectedIndex].value != 'TWD') &&
						(main.CUSTACC.options[main.CUSTACC.selectedIndex].value != main.COMMACC.options[main.COMMACC
							.selectedIndex].value)) {
						//alert("<spring:message code= "LB.Alert008" />");
						errorBlock(
							null, 
							null,
							["<spring:message code= "LB.Alert008" />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
						return false;
					}
				} else {
					if (main.CUSTACC.options[main.CUSTACC.selectedIndex].value != main.COMMACC.options[main.COMMACC
							.selectedIndex].value) {
						//alert("<spring:message code= "LB.Alert008" />");
						errorBlock(
							null, 
							null,
							["<spring:message code= "LB.Alert008" />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
						return false;
					}
					if (main.COMMCCY.options[main.COMMCCY.selectedIndex].value != 'TWD') {
						//alert("<spring:message code= "LB.Alert009" />");
						errorBlock(
							null, 
							null,
							["<spring:message code= "LB.Alert009" />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
						return false;
					}
				}

				if ((main.COMMACC.options[main.COMMACC.selectedIndex].value != '#') &&
					(main.COMMCCY.options[main.COMMCCY.selectedIndex].value == '#')) {
					//alert("<spring:message code= "LB.Alert010" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= "LB.Alert010" />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
					return false;
				}
			}

			if (main.DETCHG[1].checked) {
				if (payccy != remitcy && payccy != "TWD") {
					//alert("<spring:message code= "LB.Alert011" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= "LB.Alert011" />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
					return false;
				}
				if (payccy == remitcy && payccy == "TWD") {
					//alert("<spring:message code= "LB.Alert011" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= "LB.Alert011" />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
					return false;
				}
				if (main.COMMCCY.options[main.COMMCCY.selectedIndex].value != main.FXTRCURRENCY.value) {
					//alert("<spring:message code= "LB.Alert012" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= "LB.Alert012" />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
					return false;
				}
			}
			
			// 檢核匯款附言
			if (!CheckMEMO('<spring:message code= "LB.W0303" />')) {
				return false;
			}
			
			//欄位檢核  end
			return true;
		}
		//塞資料到下一頁及將各欄位值處理成下頁呈現所需之格式
		function prepareNextPageData() {
			var fgtrDateval = $('input[name=FGTRDATE]:checked').val();
			var payccy = $("#PAYCCY").find(":selected").text(); //付款幣別
			var remitcy = $("#REMITCY").find(":selected").text(); //收款幣別
			var commcy = $("#COMMCCY").find(":selected").text(); //手續費幣別
			//填入下拉選單的值
			$('#display_PAYCCY').val(payccy);
			$('#display_REMITCY').val(remitcy);
			$('#display_COMMCCY').val(commcy);
			//即時   	  
			if (fgtrDateval == 0) {
				// 由後端傳入
				$('#PAYDATE').val('${f_outward_remittances_step1.data.nowDate}');
				$("#formId").removeAttr("target");
				if (payccy == remitcy) {
					$('#SameCurrency').val('Y');
					$("#formId").attr("action", "${__ctx}/FCY/REMITTANCES/f_outward_remittances_confirm");
				} else {
					$("#formId").attr("action", "${__ctx}/FCY/REMITTANCES/f_outward_remittances_step2");
				}
			}
			//預約   	
			else if (fgtrDateval == 1) {
				$('#PAYDATE').val($('#CMTRDATE').val());
				$("#formId").removeAttr("target");
				$("#formId").attr("action", "${__ctx}/FCY/REMITTANCES/f_outward_remittances_step2_S");
			}
			//預約周期
			else if (fgtrDateval == 2) {
				$('#PAYDATE').val('<spring:message code= "LB.X1457" />' + $('#CMSDATE').val() + '~' + $('#CMEDATE').val() + '<spring:message code= "LB.X1458" />' + $('#CMPERIOD').val() + '<spring:message code= "LB.X1480" />');
				$("#formId").removeAttr("target");
				$("#formId").attr("action", "${__ctx}/FCY/REMITTANCES/f_outward_remittances_step2_S");
			}
			//
			var payemit = $("#PAYREMIT").find(":selected").text(); //匯款金額中文
			var fxtrcurrency = $('#FXTRCURRENCY').val(); //匯款幣別
			var curamt = $('#CURAMT').val(); //匯款金額
			var display_payremit = payemit + ' ' + fxtrcurrency + ' ' + curamt;
			$('#display_PAYREMIT').val(display_payremit);
			//收款人身分別
			var bentype = $("#BENTYPE").find(":selected").text();
			$('#display_BENTYPE').val(bentype);

		}
		
		//檢查附言
		function CheckMEMO(sFieldName) {
			///<summary>檢查匯款附言是否為合法字元</summary>
			///<param name="oField">匯款附言輸入框</param>>
			///<returns>true:檢核成功 false:檢核失敗</returns>
			// abcdefghijklmnopqrstuvwxyz共二十六個字元
			// ABCDEFGHIJKLMNOPQRSTUVWXYZ共二十六個字元
			// 0123456789共十個字元
			// /-?:().,'+Space共十一個字元
			// 另須注意每一行之第一個字元不可輸入:或- (也就是第1、36、71、106碼，最多140字元)
			
			if (typeof (sFieldName) != "string") {
				alert("CheckMEMO含有無效的參數sFieldName");
				return false;
			}
			try{
				
				var sValue = $("#MEMO1").val();
				var reNumber = /[^a-zA-Z0-9\/\-\?:\(\)\.,\+\s]/;
				if (sValue != '' && (sValue.length > 105 || reNumber.test(sValue))) 
				{
					//alert("<spring:message code="LB.Alert013"/>");
					errorBlock(
							null, 
							null,
							["<spring:message code= "LB.Alert013" />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
					return false;
				}

				if ((sValue.length >= 1 && (sValue.substring(0, 1) == '-' || sValue.substring(0, 1) == ':')) ||
						(sValue.length >= 36 && (sValue.substring(35, 36) == '-' || sValue.substring(35, 36) == ':')) ||
						(sValue.length >= 71 && (sValue.substring(70, 71) == '-' || sValue.substring(70, 71) == ':')) ||
						(sValue.length >= 106 && (sValue.substring(105, 106) == '-' || sValue.substring(105, 106) == ':'))) 
					{
						//alert('<spring:message code="LB.Alert014"/>');
						errorBlock(null, 
							null,
							['<spring:message code= "LB.Alert014" />'], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
						return false;
					}
				
			}catch(exception) {
				var msg = "<spring:message code= "LB.X1482"/>" + sFieldName + "<spring:message code= "LB.X1483"/>:" + exception;
				//alert("<spring:message code= "LB.X1482"/>" + sFieldName + "<spring:message code= "LB.X1483"/>:" + exception);
				errorBlock(
							null, 
							null,
							[msg], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				return false;
				
			}
			return true;
		}
		
		//顯示匯款金額欄之幣別
		function fillTRCRY() {
			//PAYCCY付款幣別
			var name = $("#PAYCCY").find(":selected").text();
			var curr = $("#PAYCCY").find(":selected").val();
			if (name == 'PAYCCY' && curr == '#') {
				return false;
			}
			//PAYREMIT
			var payremitText = $("#PAYREMIT").find(":selected").text();
			var payremit = $("#PAYREMIT").find(":selected").val();
			if (payremit == '#') {
				return false;
			} else if (payremit == '1') {
				$("#FXTRCURRENCY").val($("#PAYCCY").find(":selected").val());
			} else if (payremit == '2') {
				$("#FXTRCURRENCY").val($("#REMITCY").find(":selected").val());
			}
			//自動帶出手續費帳號  	
			//轉出帳號
			var CUSTACC = $("#CUSTACC").find(":selected").val();
			console.log('CUSTACC >>>' + CUSTACC)
			//自動帶出手續費帳號  	
			$('#COMMACC option').filter(function () {
				return this.value == CUSTACC;
			}).prop("selected", true);
			//自動帶出手續費幣別  	
			$('#COMMCCY option').filter(function () {
				return this.value == curr;
			}).prop("selected", true);
		}
		//宣告幣別物件 
		var crydataList = {}
		//建立下幣別拉選單
		function creatCryList(selectID) {
			//判斷是不是 空的 才去打AJAX
			if ($.isEmptyObject(crydataList)) {
				crydataList = getCryList();
			}
			//建立幣別下拉選單 
			$('#' + selectID).find("option").remove(); //先清除下拉選單資料
			var options = {keyisval: false,selectID: '#' + selectID}
			var termdf = {};
			termdf['#'] = '---' + i18nValue['LB.Select'] + '---'; //因為選擇清除下拉選單資料所以要加上
			fstop.creatSelect(termdf, options); //建立請選擇
			fstop.creatSelect(crydataList, options); //建立幣別
		}
		//取得幣別資料
		function getCryList() {
			var uri = '${__ctx}' + "/FCY/COMMON/getCRYList"
			var data = fstop.getServerDataEx(uri, null, false);
			if (data != null && data.result == true) {
				//建立選單資料
				for (var key in data.data.REC) {
					//資料庫代號
					//var adccyno = data.data.REC[key].adccyno;
					//幣別代號(英文)
					var adcurrency = data.data.REC[key].adcurrency;
					//中文
					var adccyname = data.data.REC[key].adccyname;
					
					//console.log(key + 'adccyno' + adccyno);
					//console.log(key + 'adcurrency ' + adcurrency);
					//console.log(key + 'adccyname ' + adccyname);
					//console.log('adcurrency : ' + adcurrency + 'adccyname : ' + adccyname);
					crydataList[adcurrency] = adcurrency + ' ' + adccyname;
				}
			}
			console.log('建立選單資料END----------------')
			return crydataList;
		}

		//取得收款行(人)相關資訊
		function getRcvInfo() {
			var acn_val = $("#BENACC").find(":selected").val();
			var acno_text = $("#BENACC").find(":selected").text();
			console.log(acn_val);
			var array = acno_text.split("-");
			var remitcy = array[1];
			console.log(remitcy);
			//收款幣別
			$('#REMITCY option').filter(function () {
				return this.value == remitcy;
			}).prop("selected", true);
			$('#REMITCY').attr('disabled', 'disabled');
			getRcvInfoData(acn_val);
		}

		//取得收款行(人)相關資訊
		function getRcvInfoData(acn_val) {
			var uri = '${__ctx}' + "/FCY/COMMON/getRcvInfo"
			var rdata = {acn_val: acn_val};
			var data = fstop.getServerDataEx(uri, rdata, false, callback);
		}
		//處理 取得收款行(人)相關資訊
		function callback(data) {
			if (data != null && data.result == true) {
				//SWIFT CODE
				$('#FXRCVBKCODE').val(data.data.ACWBIC);
				//收款銀行名稱/地址
				$('#FXRCVBKADDR').val(data.data.ACWNAME + "\r\n" + data.data.ACWAD1 + "\r\n" +
					data.data.ACWAD2 + "\r\n" + data.data.ACWAD3);
				//收款人 名稱/地址
				$('#FXRCVADDR').val(data.data.BENNAME + "\r\n" + data.data.BENAD1 + "\r\n" +
					data.data.BENAD2 + "\r\n" + data.data.BENAD3);
				//國別
				$('#COUNTRY').val(data.data.CNTY);
				//收款人身份別 
				var benType = data.data.REMTYPE;
				//收款人身份別 
				$('#REMTYPE').val(benType);
				$('#BENTYPE option').filter(function () {
					return this.value == benType;
				}).prop("selected", true);
				$("display_BENTYPE").val($("#BENTYPE").find(":selected").text());
				$('#BENTYPE').attr('disabled', 'disabled');
				if (benType == '') {
					$("#BENTYPE_FLAG").val('Y');
					$('#BENTYPE').attr('disabled', 'disabled');
					//
					$("#BENTYPE").removeClass("select-input validate[funcCall[validate_CheckSelect[<spring:message code= "LB.Payee_identity" />,BENTYPE,#]]]");
				} else {
					$("#BENTYPE_FLAG").val('N');
					//
					$("#BENTYPE").addClass("select-input validate[funcCall[validate_CheckSelect[<spring:message code= "LB.Payee_identity" />,BENTYPE,#]]]");
				}
			}
		}
		// 轉出帳號onChange
		function onChangeCustacc() {
			uri = '${__ctx}' + "/FCY/TRANSFER/currency_ajax";
			// 若是請選擇就不發Ajax問幣別
			if ($("#CUSTACC").val() != "") {
				rdata = {ACN: $("#CUSTACC").val()};
				console.log("creatOutAcn.uri: " + uri);
				console.log("creatOutAcn.rdata: " + rdata);
				var data = fstop.getServerDataEx(uri, rdata, false, onChangeCustaccCallback);
			}
		}
		// 轉出帳號onChange後打Ajax的callback
		function onChangeCustaccCallback(data) {
			console.log("onChangeCustaccCallback data: " + data);
			if (data) {
				// currency_ajax回傳資料型態: List<Map<String, String>>
				console.log("data.json: " + JSON.stringify(data));
				// 先清空原有之"OPTION"內容,並加上一個 "---請選擇---" 欄位
				$('#PAYCCY').empty();
				$("#PAYCCY").append($("<option></option>").attr("value", "#").text("---<spring:message code= 'LB.Select' />---"));
				// 迴圈塞轉出幣別
				data.forEach(function (map) {
					console.log(map);
					$("#PAYCCY").append($("<option></option>").attr("value", map.ADCURRENCY).text(map.ADCURRENCY + " " + map.ADCCYNAME));
				});
			}
		}
		// 處理手續費幣別onChange
		function onChangeCOMMCCY() {
			uri = '${__ctx}' + "/FCY/TRANSFER/currency_ajax";
			// 若是請選擇就不發Ajax問幣別
			if ($("#COMMACC").val() != "") {
				rdata = {
					ACN: $("#COMMACC").val()
				};

				console.log("creatOutAcn.uri: " + uri);
				console.log("creatOutAcn.rdata: " + rdata);

				var data = fstop.getServerDataEx(uri, rdata, false, onChangeCommccyCallback);
			}
		}
		// 費用扣款帳號COMMACC欄位onChange後打Ajax的callback
		function onChangeCommccyCallback(data) {
			console.log("onChangeCommccyCallback data: " + data);
			if (data) {
				// currency_ajax回傳資料型態: List<Map<String, String>>
				console.log("data.json: " + JSON.stringify(data));
				// 先清空原有之"OPTION"內容,並加上一個 "---請選擇---" 欄位
				$('#COMMCCY').empty();
				$("#COMMCCY").append($("<option></option>").attr("value", "#").text("---<spring:message code= "LB.Select" />---"));
				// 迴圈塞轉出幣別
				data.forEach(function (map) {
					console.log(map);
					$("#COMMCCY").append($("<option></option>").attr("value", map.ADCURRENCY).text(map.ADCURRENCY +
						" " + map.ADCCYNAME));
				});
			}
		}
		// 一般網銀分行聯絡方式資料檔擷取
		function displayBhContact() {
			var bhid = $("#CUSTACC").val().substring(0, 3);
			console.log("displayBhContact_bhid: " + bhid);
			rdata = {bhid: bhid};
			uri = '${__ctx}/FCY/COMMON/getBhContactResult';
			console.log("displayBhContact_uri: " + uri);
			fstop.getServerDataEx(uri, rdata, true, bc_callback);
		}
		// 一般網銀分行聯絡方式資料檔擷取
		function bc_callback(data) {
			if (data) {
				// login_aj回傳資料
				console.log("bc_callback.data.json: " + JSON.stringify(data));
				var adcontacttel = data.data.REC[0].adcontacttel;
				console.log("bc_callback.adcontacttel: " + adcontacttel);
				document.getElementById("ADCONTACTTEL").innerHTML = adcontacttel;
			}
		}
		//取得轉出帳號幣別餘額資料
		function getACNO_Data() {
			uri = '${__ctx}' + "/FCY/COMMON/getACNO_Currency_Data_aj"
			console.log("getACNO_Data URL >>" + uri);
			//轉出帳號
			var acno = $("#CUSTACC").find(":selected").val();
			//轉出幣別			
			var cry = $("#PAYCCY").find(":selected").val();
			
			// 轉出帳號、轉出幣別皆有值才執行查詢餘額
			if(acno&&cry){
				rdata = {acno: acno,cry: cry};
				console.log("rdata>>" + rdata);
				fstop.getServerDataEx(uri, rdata, true, isShowACNO_Data);
			}
		}
		//顯示轉出帳號幣別餘額資料
		function isShowACNO_Data(data) {
			console.log("isShowACNO_Data.data >>> " + data);
			$("#acnoIsShow").hide();
			if (data && data.result) {
				var bal = "0.00";
				if(data.data.accno_data) {
					bal = fstop.formatAmt(data.data.accno_data.AVAILABLE); //格式化金額
				}
				console.log("bal :" + bal);
				// 格式化金額欄位
				var showBal = i18nValue['available_balance'] + bal;
				$("#acnoIsShow").show();
				$("#showText").html(showBal);
			} else {
				$("#acnoIsShow").hide();
			}
		}
	
		
		//copyTN
		function copyTN() {
			$("#CMMAILMEMO").val($("#MEMO1").val());
		}
		//開啟通訊錄email選單
		function openAddressbook() {
			window.open('${__ctx}/NT/ACCT/TRANSFER/showAddressbook');
		}
		//開啟匯款分類
		function openFxRemitQuery() {
			//國別
			var cnty = $('#COUNTRY').val();
			//收款人身份別 
			var remtype = $('#REMTYPE').val();
			//FROM_FCY_REMITTANCES=Y，表示是從外幣的匯出匯款頁面開啟此頁面
			var fxremitqueryUrl = '${__ctx}/FCY/COMMON/FxRemitQuery?ADRMTTYPE=1&CUTTYPE=${f_outward_remittances_step1.data.str_CUSTYPE}' + '&CNTY=' + cnty + '&REMTYPE=' + remtype + "&FROM_FCY_REMITTANCES=Y";
			console.log('fxremitqueryUrl >>>>>> '+ fxremitqueryUrl)
			window.open(fxremitqueryUrl);
		}
		//開啟常用匯款分類
		function openMyRemitMenu() {
			window.open('${__ctx}/FCY/COMMON/MyRemitMenu');
		}
		//每月日期下拉
		function createCMPERIOD() {
			var options = {selectID: '#CMPERIOD'}
			var h_options = {selectID: '#H_CMPERIOD'}
			var dataTerm = {}
			for (i = 1; i <= 31; i++) {
				dataTerm[i] = i;
			}
			fstop.creatSelect(dataTerm, options);
			fstop.creatSelect(dataTerm, h_options);
		}
		//預約自動輸入明天
		function getTmr() {
			var tmr = '${f_outward_remittances_step1.data.tmr}';
			$('#CMTRDATE').val(tmr);
			$('#CMSDATE').val(tmr);
			$('#CMEDATE').val(tmr);
		}
		//檢查輸入匯款分類編號
		function validate_Srcfunddesc(field, rules, i, options){
			var Srcfunddesc = $('#SRCFUNDDESC').val(); 
			console.log("Srcfunddesc >>" + Srcfunddesc);
			 if (Srcfunddesc === "") {
				 // <!-- 請輸入匯款分類編號 -->
		   	     return '<spring:message code= "LB.Alert146" />';   				
			  }	
		}

		
// 		日曆欄位參數設定
		function datetimepickerEvent(){
		    $(".CMDATE").click(function(event) {
				$('#CMDATE').datetimepicker('show');
			});
		    $(".CMSDATE").click(function(event) {
				$('#CMSDATE').datetimepicker('show');
			});
		    $(".CMEDATE").click(function(event) {
				$('#CMEDATE').datetimepicker('show');
			});
		    $(".CMTRDATE").click(function(event) {
				$('#CMTRDATE').datetimepicker('show');
			});
			
			jQuery('.datetimepicker').datetimepicker({
				timepicker:false,
				closeOnDateSelect : true,
				scrollMonth : false,
				scrollInput : false,
			 	format:'Y/m/d'
			 	
			});
		}
		
		
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
	<!-- 匯出匯款     -->
    		<li class="ttb-breadcrumb-item"><spring:message code="LB.W0286" /></li>
    <!-- 匯出匯款     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0286" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!--外匯匯出匯款 -->
				<h2>
					<spring:message code="LB.W0286" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form autocomplete="off" method="post" id="formId"
					action="${__ctx}/FCY/REMITTANCES/f_outward_remittances_step2">
					<input type="hidden" id="ADOPID" name="ADOPID" value="F002">
					<input type="hidden" id="PAYDATE" name="PAYDATE" value="">
					<input type="hidden" id="SRCFUND" name="SRCFUND" value="">
					<input type="hidden" id="COUNTRY" name="COUNTRY" value="">
					<input type="hidden" id="ADTXAMT" name="ADTXAMT" value="">
					<input type="hidden" id="REMTYPE" name="REMTYPE" value="" >
					<input type="hidden" id="display_PAYCCY" name="display_PAYCCY" value="">
					<input type="hidden" id="display_REMITCY" name="display_REMITCY" value="">
					<input type="hidden" id="display_COMMCCY" name="display_COMMCCY" value="">
					<input type="hidden" id="display_BENTYPE" name="display_BENTYPE" value="">
					<input type="hidden" id="display_PAYREMIT" name="display_PAYREMIT" value="">
					<input type="hidden" id="CUSTYPE" name="CUSTYPE" value='${f_outward_remittances_step1.data.str_CUSTYPE}'>
					<input type="hidden" id="NAME" name="NAME" value="${f_outward_remittances_step1.data.NAME}">
					<input type="hidden" id="nowDate" name="nowDate" value="${f_outward_remittances_step1.data.nowDate}">
					<input type="hidden" id="BENTYPE_FLAG" name="BENTYPE_FLAG" value="N">
					<input type="hidden" id="SameCurrency" name="SameCurrency" value="N">
					<!-- 表單顯示區 -->
					<div class="main-content-block row radius-50">
						<nav style="width: 100%;">
							<div class="nav nav-tabs" id="nav-tab" role="tablist">
								<!-- 即時 -->
								<a class="nav-item nav-link active" id="nav-trans-now" data-toggle="tab"
									href="#nav-trans-now" role="tab" aria-controls="nav-home" aria-selected="true">
									<spring:message code="LB.Immediately" />
								</a>
								<!-- 預約 -->
								<a class="nav-item nav-link" id="nav-trans-future" data-toggle="tab"
									href="#nav-trans-future" role="tab" aria-controls="nav-profile"
									aria-selected="false">
									<spring:message code="LB.Booking" />
								</a>
							</div>
						</nav>
						<div class="col-12 tab-content" id="nav-tabContent">
							<div class="tab-pane fade " id="nav-trans-future" role="tabpanel"
								aria-labelledby="nav-profile-tab">
							</div>
							<div class="ttb-input-block tab-pane fade show active" id="nav-trans-now" role="tabpanel"
								aria-labelledby="nav-home-tab">
								<div class="ttb-message">
									<span></span>
								</div>
								<!-- 這是即時的屬性  預設就是隱藏 -->
								<div class="ttb-input" style="display: none;">
									<label class="radio-block">
										<!-- 即時 -->
										<spring:message code="LB.Immediately" />
										<input type="radio" name="FGTRDATE" value="0" checked>
										<span class="ttb-radio"></span>
									</label>
								</div>
								<!-- 預約 轉帳日期 -->
								<div id="transfer-date" class="ttb-input-item row" style="display: none;">
									<span class="input-title">
										<label>
											<spring:message code="LB.Transfer_date" />
										</label>
									</span>
									<span class="input-block">
										<!-- 預約 -->
										<div class="ttb-input ">
											<label class="radio-block">
												<spring:message code="LB.Booking" />
												<input type="radio" name="FGTRDATE" value="1">
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<input type="text" id="CMTRDATE" name="CMTRDATE" class="text-input datetimepicker" value="">
											<!-- 日曆元件 -->
											<span class="input-unit CMTRDATE "><img src="${__ctx}/img/icon-7.svg" /></span>
											<!-- 驗證用的span預設隱藏 -->
											<span name="hideblock">
												<!-- 驗證用的input -->
												<input id="H_CMTRDATE" name="H_CMTRDATE" type="text" class="text-input"
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
										<!-- 預約　固定每月的 -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Booking" />
												</br>
												<input type="radio" name="FGTRDATE" id="FGTRDATE2" value="2">
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!-- 日 -->
										<div class="ttb-input">
											<span class="input-unit">
												<spring:message code="LB.Monthly" /></span>
											<select class="custom-select select-input half-input" id="CMPERIOD" name="CMPERIOD">
												<option value="#">
													<spring:message code="LB.Select" />
												</option>
											</select>
											<span class="input-unit">
												<spring:message code="LB.Day" />
											</span>
											<!-- 不在畫面上顯示的span -->
											<span name="hideblock">
												<!-- 驗證用的input -->
												<select class="custom-select select-input half-input" id="H_CMPERIOD" name="H_CMPERIOD"  style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;">
													<option value="#">
														<spring:message code="LB.Select" />
													</option>
												</select>
											</span>
										</div>
										<!-- 期間起日 -->
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color">
												<spring:message code="LB.Period_start_date" /></span>
											<input type="text" id="CMSDATE" name="CMSDATE" class="text-input datetimepicker" value="" />
											<span class="input-unit CMSDATE">
												<img src="${__ctx}/img/icon-7.svg" />
											</span>
											<!-- 不在畫面上顯示的span -->
											<span name="hideblock">
												<!-- 驗證用的input -->
												<input id="H_CMSDATE" name="H_CMSDATE" type="text" class="text-input"
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
										<!-- 迄日 -->
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color">
												<spring:message code="LB.Period_end_date" />
											</span>
											<input type="text" id="CMEDATE" name="CMEDATE" class="text-input datetimepicker" value="" />
											<span class="input-unit CMEDATE">
												<img src="${__ctx}/img/icon-7.svg" />
											</span>
											<!-- 不在畫面上顯示的span -->
											<span name="hideblock">
												<!-- 驗證用的input -->
												<input id="H_CMEDATE" name="H_CMEDATE" type="text" class="text-input"
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												<input id="validate_CMEDATE2" name="validate_CMEDATE2" type="text" class="text-input validate[funcCallRequired[validate_CheckPerMonthDay['<spring:message code= "LB.X1455" />',H_CMPERIOD,CMSDATE,CMEDATE]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
									</span>
								</div>
								<!-- 付款帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0290" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<select class="custom-select select-input half-input  validate[funcCall[validate_CheckSelect[<spring:message code= "LB.W0290" />,CUSTACC,#]]]" id="CUSTACC" name="CUSTACC"
												onchange="onChangeCustacc();displayBhContact();getACNO_Data()">
                                                <option value="#" readonly selected><spring:message code="LB.Select_account" /></option>
												<c:forEach var="dataList"
													items="${ f_outward_remittances_step1.data.outAcnoList}">
													<option value='${dataList.ACN}'> ${dataList.ACN}</option>
												</c:forEach>
											</select>
										</div>
									</span>
								</div>
								<!-- 付款幣別 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.D0448" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<select class="custom-select select-input half-input validate[funcCall[validate_CheckSelect[<spring:message code= "LB.D0448" />,PAYCCY,#]]]" name="PAYCCY" id="PAYCCY" size="1"
												onchange="fillTRCRY();getACNO_Data()">
											</select>
										</div>
										<div id="acnoIsShow">
											<span id="showText" class="input-unit "></span>
										</div>
									</span>
								</div>
								<!-- 收款帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0292" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<select class="custom-select select-input half-input validate[funcCall[validate_CheckSelect[<spring:message code= "LB.W0292" />,BENACC,#]]]"
												id="BENACC" name="BENACC" name="BENACC" onchange="getRcvInfo()">
												<option value="#" readonly selected><spring:message code="LB.Select_account" /></option>
												<c:forEach var="dataList"
													items="${ f_outward_remittances_step1.data.REC}">
													<option value='${dataList.BENACC}'> ${dataList.TEXT}</option>
												</c:forEach>
											</select>
										</div>
									</span>
								</div>
								<!-- 收款幣別 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0293" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<select class="custom-select select-input half-input validate[funcCall[validate_CheckSelect[<spring:message code= "LB.W0293" />,REMITCY,#]]]"
												id="REMITCY" name="REMITCY" size="1">
											</select>
										</div>
									</span>
								</div>
								<!-- 匯款金額 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0150" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<select class="custom-select select-input half-input select-input validate[funcCall[validate_CheckSelect[<spring:message code= "LB.X1484" />,PAYREMIT,#]]]"
												onchange="fillTRCRY()" id="PAYREMIT" name="PAYREMIT" size="1">
												<option value="#" selected>--- <spring:message code="LB.Select" /> ---</option>
												<option value="1"><spring:message code="LB.X0031" /></option>
												<option value="2"><spring:message code="LB.X0032" /></option>
											</select>
										</div>
										<div class="ttb-input">
											 <input class="text-input input-width-100" id="FXTRCURRENCY" name="FXTRCURRENCY" value="" readonly>
											 <input class="text-input input-width-125 validate[required, funcCall[validate_CheckFxAmount['<spring:message code= "LB.W0150" />', CURAMT, false, FXTRCURRENCY, PAYREMIT]]]"
												 id="CURAMT" name="CURAMT">
										</div>
									</span>
								</div>
								<!-- 收款人身份別 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.Payee_identity" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<select class="custom-select select-input half-input" id="BENTYPE" name="BENTYPE" size="1">
												<option value="#">---<spring:message code="LB.Select" />---</option>
												<option value="1"><spring:message code="LB.Government" /></option>
												<option value="2"><spring:message code="LB.Public_enterprise" /></option>
												<option value="3"><spring:message code="LB.Private_enterprise" /></option>
												<option value="4"><spring:message code="LB.Other_Account" /></option>
												<option value="5"><spring:message code="LB.My_Account" /></option>
											</select>
										</div>
									</span>
								</div>
								<!-- 匯款分類項目 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0298" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<font id="SHOW_SRCFUNDDESC"><spring:message code="LB.Please_select_code" /></font>
											<input class="text-input validate[funcCallRequired[validate_Srcfunddesc[SRCFUNDDESC]]]" type="text" name="SRCFUNDDESC" id="SRCFUNDDESC"  style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;">
											<br>
											<button class="btn-flat-orange" type="button" name="FXQREMITNO"
											value="<spring:message code="LB.Menu" />" onclick="openFxRemitQuery()"><spring:message code="LB.Menu" /></button>
											<button  class="btn-flat-orange" type="button" name="FXQREMITNO2"
												value="<spring:message code="LB.Common_menu" />" onclick="openMyRemitMenu()"><spring:message code="LB.Common_menu" /></button>
											<br>
											<span class="input-unit"><spring:message code="LB.Remittance_Classification_Note" /></span>
											<br>	
											<font color="#0000FF">
												<spring:message code="LB.Consulting_line" />：<font id="ADCONTACTTEL"></font>
											</font>
										</div>
									</span>
								</div>
								<!-- 匯款分類說明 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0299" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<font id="SHOW_FXRMTDESC"><spring:message code="LB.Please_select_code" /></font>
											<input class="text-input" name="FXRMTDESC" id="FXRMTDESC" type="hidden"
												value="" placeholder="<spring:message code="LB.Please_select_code" />" readonly>
											<br>
										</div>
									</span>
								</div>
								<!-- 匯款附言 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0303" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" class="text-input" name="MEMO1" id="MEMO1" onblur="CheckMEMO('<spring:message code= "LB.W0303" />')">
											<br>
											<span class="input-unit"><spring:message code="LB.X0033" /></span>
										</div>
									</span>
								</div>
								<!-- 手續費負擔別 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0155" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block"><spring:message code="LB.X0034" />
												<input name="DETCHG" type="radio" value="SHA" checked="" /><span
													class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="radio-block"><spring:message code="LB.X0035" />
												<input name="DETCHG" type="radio" value="BEN"><span
													class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="radio-block"><spring:message code="LB.X0036" />
												<input name="DETCHG" type="radio" value="OUR" />
												<span class="ttb-radio"></span>
											</label>
											<button class="btn-flat-orange" type="button" value="<spring:message code="LB.X1146" />" name="FEEDESC" onclick="window.open('${__ctx}/public/ourFeeRate.htm')"><spring:message code="LB.X1146" /></button>
										</div>
									</span>
								</div>
								<!-- 費用扣款帳號 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0305" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<select class="custom-select select-input half-input validate[funcCall[validate_CheckSelect[<spring:message code= "LB.W0305" />,CUSTACC,#]]]" id="COMMACC" name="COMMACC" onchange="onChangeCOMMCCY()">
											<option value="#" readonly selected><spring:message code="LB.Select_account" /></option>
												<c:forEach var="dataList"
													items="${ f_outward_remittances_step1.data.outAcnoList}">
													<option value='${dataList.ACN}'> ${dataList.ACN}</option>
												</c:forEach>
											</select>
										</div>
									</span>
								</div>
								<!-- 費用扣款幣別 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0306" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<select class="custom-select select-input half-input" name="COMMCCY" id="COMMCCY">
											</select>
										</div>
									</span>
								</div>
								<!-- 收款銀行資料 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.X0037" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color">SWIFT CODE</span>
										</div>
										<div class="ttb-input">
											<input type="text" class="text-input" id="FXRCVBKCODE" name="FXRCVBKCODE" size="22" value="" />
										</div>
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color"><spring:message code="LB.W0295" /></span>
										</div>
										<div class="ttb-input">
											<input type="text" class="text-input" id="FXRCVBKADDR" name="FXRCVBKADDR">
										</div>
									</span>
								</div>
								<!-- 收款人資料 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.W0294" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<span class="input-subtitle subtitle-color"><spring:message code="LB.W0295" /></span>
										</div>
										<div class="ttb-input">
											<input type="text" class="text-input" id="FXRCVADDR" name="FXRCVADDR">
										</div>
									</span>
								</div>
								<!-- Email通知 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4><spring:message code="LB.W0300" /></h4>
										</label></span>
									<span class="input-block">
										<div class="ttb-input">
											<input type="text" class="text-input"  name="CMTRMAIL" id="CMTRMAIL" maxlength="500" placeholder="<spring:message code="LB.Not_required" />" />
											<button class="btn-flat-orange" type="button" name="CMADDRESS" id="CMADDRESS" value="<spring:message code="LB.Address_book" />" onClick="openAddressbook()"><spring:message code="LB.Address_book" /></button>
										<!-- 不在畫面上顯示的span -->
										<span name="hideblock" >
											<!-- 驗證用的input -->
											<input id="H_CMTRMAIL" name="H_CMTRMAIL" type="text" class="text-input validate[funcCall[validate_EmailCheck[CMTRMAIL]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
										</span>
										</div>
										<div class="ttb-input">
											<input type="text" class="text-input" name="CMMAILMEMO" id="CMMAILMEMO">
											<button class="btn-flat-orange" type="button" name="CMCOPY" id="CMCOPY" value="<spring:message code="LB.X0038" />" onClick="copyTN()" /><spring:message code="LB.X0038" /></button>
										</div>
									</span>
								</div>
							</div>
							<!-- button -->
								<!--重新輸入 -->
								<spring:message code="LB.Re_enter" var="cmRest" />
								<input class="ttb-button btn-flat-gray" type="reset" name="CMRESET" id="CMRESET" value="${cmRest}"  >
								<!-- 確定 -->
								<spring:message code="LB.Confirm" var="cmSubmit" />
								<input class="ttb-button btn-flat-orange" type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" >
							<!-- buttonEND -->
						</div>
					</div>
					<!--說明： -->
					<div class="text-left">
						<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /></p>
							<li>
								<strong style="font-weight: 400">
									<spring:message code="LB.F_Outward_Remittances_P1_D1-1" />
								<a href="${__ctx}/CUSTOMER/SERVICE/rate_table" target="_blank">
									<spring:message code="LB.F_Outward_Remittances_P1_D1-2" />
								</a>
								<a href="${__ctx}/public/transferFeeRate.htm" target="_blank">
									<spring:message code="LB.F_Outward_Remittances_P1_D1-3" />
								</a><a href="https://www.tbb.com.tw/exchange_rate" target="_blank">
									<spring:message code="LB.F_Outward_Remittances_P1_D1-4" />
								</a>
							</strong>
							</li>
							<li><strong style="font-weight: 400"><spring:message code="LB.F_Outward_Remittances_P1_D2" /></strong></li>
							<li><strong style="font-weight: 400"><spring:message code="LB.F_Outward_Remittances_P1_D3" /></strong></li>
							<li><strong style="font-weight: 400"><spring:message code="LB.F_Outward_Remittances_P1_D4" /></strong></li>
							<li>
								<strong style="font-weight: 400"><spring:message code="LB.F_Outward_Remittances_P1_D5" />
								<font color="red"><spring:message code="LB.F_Transfer_P1_D7-2" />
								</font>。
								</strong>
							</li>
							<li><strong style="font-weight: 400"><spring:message code="LB.F_Outward_Remittances_P1_D6" /></strong></li>
							<li><strong style="font-weight: 400"><spring:message code="LB.F_Outward_Remittances_P1_D7" /></strong></li>
							<li><strong style="font-weight: 400"><spring:message code="LB.F_Outward_Remittances_P1_D8" /></strong></li>
							<li><strong style="font-weight: 400"><spring:message code="LB.F_Outward_Remittances_P1_D9" /></strong></li>
							<li><strong style="font-weight: 400"><spring:message code="LB.F_Outward_Remittances_P1_D10" /></strong></li>
						</ol>
					</div>
				</form>
			</section>
			<!-- main-content END -->
		</main>
	</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>