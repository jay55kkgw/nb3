<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust:exact">
	<br/><br/>
	<div style="text-align:center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/>
	</div>
	<br/><br/><br/>
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<br><br><br>

	<div class="ttb-message">
<!-- 預約成功 -->
		<span><spring:message code= "LB.W0284" /></span>
	</div>
	<table class="print">
		<tr>
<!-- 資料日期 -->
			<td><spring:message code= "LB.W0875" /></td>
		 	<td>${CMQTIME}</td>
		</tr>
		<tr>
<!-- 臺幣轉出帳號 -->
  			<td><spring:message code= "LB.W1496" /></td>
			<td>${SVACN}</td>
		</tr>
		<tr>
<!-- 黃金轉入帳號 -->
  			<td><spring:message code= "LB.W1497" /></td>
			<td>${ACN}</td>
		</tr>
		<tr>
<!-- 買進公克數 -->
  			<td><spring:message code= "LB.W1498" /></td>
<!-- 公克 -->
			<td>${TRNGD}<spring:message code= "LB.W1435" /></td>
		</tr>
	</table>
	<br>
	<div>
		<p style="text-align: left;">
<!-- 說明 -->
<!-- 預約黃金買進 -->
<!-- 將以預約後的第一個營業日的第一次牌告賣出價格交易，請於扣款前一日存足款項於轉出帳戶備扣。 -->
<!-- 電子簽章：為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章（載具i－key）拔除並登出系統。 -->
			<spring:message code="LB.Description_of_page" /><br /> 
			<spring:message code= "LB.Gold_Booking_P3_D1-1" /><font color=red><spring:message code= "LB.Gold_Booking_P3_D1-2" /></font><br>
			<spring:message code= "LB.Gold_Booking_P3_D2" />
		</p>
	</div>
</body>
</html>