<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp" %>
</head>
<body>

	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 	<!--   IDGATE -->
	<%@ include file="../idgate_tran/idgateAlert.jsp" %>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
	<!-- 轉帳交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Transfer" /></li>
    <!-- 轉帳交易     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.NTD_Transfer" /></li>
		</ol>
	</nav>





	<!--     左邊menu 及登入資訊-->
	<div class="content row">

		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> <!-- 		主頁內容  -->
		<section id="main-content" class="container">

			<h2>
				<spring:message code="LB.NTD_Transfer" />
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>

			<div id="step-bar">
				<ul>
					<li class="finished"><spring:message code="LB.Enter_data" /></li>
					<li class="finished"><spring:message code="LB.Confirm_data" /></li>
					<li class="active"><spring:message
							code="LB.Transaction_complete" /></li>
				</ul>
			</div>

			<div class="main-content-block row">
				<div class="col-12 tab-content">
					<div class="ttb-input-block">
						<div class="ttb-message">
							<span> ${transfer_result_data.data.TOPMSG} </span>
						</div>
					

					<c:if test="${transfer_result_data.result ==true}">
						<div class="ttb-input-item row">
							<span class="input-title"><label>
									<h4>
										<spring:message code="LB.Trading_time" />
									</h4>
							</label></span> <span class="input-block"><div class="ttb-input">
									<span> ${transfer_result_data.data.CMQTIME } </span>
								</div></span>
						</div>

						<!--                         預約才有轉帳日期 -->
						<c:if test="${transfer_result_data.data.FGTXDATE != '1'}">
							<div class="ttb-input-item row">
								<span class="input-title"><label>
										<h4>
											<spring:message code="LB.Transfer_date" />
										</h4>
								</label></span> <span class="input-block"><div class="ttb-input">
										<span> ${transfer_result_data.data.SCHDATE } </span>
									</div></span>
							</div>
						</c:if>
						<c:if test="${transfer_result_data.data.FGTXDATE == '1' && showDialog == 'true'}">
								 	<%@ include file="../index/txncssslog.jsp"%>
						</c:if>
						<div class="ttb-input-item row">
							<span class="input-title"><label>
									<h4>
										<spring:message code="LB.Payers_account_no" />
									</h4>
							</label></span> <span class="input-block"><div class="ttb-input">
									<span> ${transfer_result_data.data.OUTACN } </span>
								</div></span>
						</div>

						<div class="ttb-input-item row">
							<span class="input-title"><label>
									<h4>
										<spring:message code="LB.Payees_account_no" />
									</h4>
							</label></span> <span class="input-block"><div class="ttb-input">
									<span> ${transfer_result_data.data.INTSACN } </span>
								</div></span>
						</div>

						<div class="ttb-input-item row">
							<span class="input-title"><label>
									<h4>
										<spring:message code="LB.Amount" />
									</h4>
							</label></span> <span class="input-block"><div class="ttb-input">
									<span> <span class="input-unit"><spring:message
												code="LB.NTD" /></span> ${transfer_result_data.data.AMOUNT } <span
										class="input-unit"><spring:message code="LB.Dollar" /></span>
									</span>
								</div></span>
						</div>
						
						<c:if test="${not empty transfer_result_data.data.FEE }">
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											<spring:message code="LB.X2423" />
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span><spring:message code="LB.NTD" />${transfer_result_data.data.FEE }<spring:message code="LB.Dollar" /></span>
										<c:if test="${transfer_result_data.data.FEE eq '0' || transfer_result_data.data.FEE eq '000' || transfer_result_data.data.FEE eq '0.00'}">
										<br><span class="input-remarks" style="color:red"><spring:message code="LB.X2422" /></span>
										</c:if>	
									</div>
								</span>													
							</div>
						</c:if>
						
						<c:if test="${transfer_result_data.data.HEADER eq 'N070' }">
							<div class="ttb-input-item row">
								<span class="input-title"><label>
										<h4>
											<spring:message code="LB.Payees_postscript" />
										</h4>
								</label></span> <span class="input-block"><div class="ttb-input">
										<span> ${transfer_result_data.data.INPCST } </span>
									</div></span>
							</div>

						</c:if>

						<div class="ttb-input-item row">
							<span class="input-title"><label>
									<h4>
										<spring:message code="LB.Transfer_note" />
									</h4>
							</label></span> <span class="input-block"><div class="ttb-input">
									<span> ${transfer_result_data.data.CMTRMEMO } </span>
								</div></span>
						</div>


						<!--即時轉帳才顯示以下欄位 -->

						<c:if test="${transfer_result_data.data.FGTXDATE == '1'}">
							<c:if test="${transfer_result_data.data.O_TOTBAL != '0.00'}">
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
												<h4>
													<spring:message code="LB.Payers_account_balance" />
												</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
												<span> <spring:message code="LB.NTD" />
													${transfer_result_data.data.O_TOTBAL } <spring:message
														code="LB.Dollar" />
												</span>
										</div>
										</span>
								</div>

								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
												<h4>
													<spring:message code="LB.Payers_available_balance" />
												</h4>
										</label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span> <spring:message code="LB.NTD" />
												${transfer_result_data.data.O_AVLBAL } <spring:message
													code="LB.Dollar" />
											</span>
										</div>
									</span>
								</div>

							</c:if>

							<%-- 	                        <c:if test="${transfer_result_data.data.I_TOTBAL != '0.00'}"> --%>
							<c:if test="${not empty transfer_result_data.data.I_TOTBAL &&  transfer_result_data.data.I_TOTBAL != '' && transfer_result_data.data.I_TOTBAL != '0.00'}">
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4>
													<spring:message code="LB.Payees_account_balance" />
												</h4>
											</label>
										</span> 
										<span class="input-block">
											<div class="ttb-input">
												<span> 
													<spring:message code="LB.NTD" />
													${transfer_result_data.data.I_TOTBAL } 
													<spring:message code="LB.Dollar" />
												</span>
											</div>
										</span>
									</div>

									<div class="ttb-input-item row">
										<span class="input-title"><label>
												<h4>
													<spring:message code="LB.Payees_available_balance" />
												</h4>
										</label></span> <span class="input-block">
										<div class="ttb-input">
											<span> <spring:message code="LB.NTD" />
												${transfer_result_data.data.I_AVLBAL } <spring:message
													code="LB.Dollar" />
											</span>
										</div></span>
									</div>
							</c:if>
							
						</c:if>  <!--                         if FGTXDATE END -->
					</c:if> <!-- if transfer_result_data.result -->
<!-- 					</div> -->
					</div><!-- 					ttb-input-block END -->
					<input type="button" class="ttb-button btn-flat-orange"
						id="printbtn" value="<spring:message code="LB.Print" />" />
				</div><!-- 				col-12 tab-content END -->
			</div><!-- 			main-content-block row  END-->
			<c:if test="${transfer_result_data.data.FGTXDATE == '1'}">
					<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /></p>
							<li><spring:message code="LB.Transfer_P3_D1" /></li>
							<li><spring:message code="LB.Transfer_P3_D2" /></li>	
					</ol>
			</c:if>
			<c:if test="${transfer_result_data.data.FGTXDATE != '1'}">
					<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /></p>
							<li><spring:message code="LB.Transfer_PP3_D1" /></li>
							<li><spring:message code="LB.Transfer_PP3_D2" /></li>	
					</ol>
			</c:if>
		</section><!-- 		main-content END --> 
		
		</main>
	</div>
	<!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>

	<script type="text/javascript">
			$(document).ready(function(){
				$("#printbtn").click(function(){
					var params = {
						"jspTemplateName":"transfer_result_print",
						"jspTitle":"<spring:message code='LB.NTD_Transfer' />",
						"message":"${transfer_result_data.data.TOPMSG}",
						"CMQTIME":"${transfer_result_data.data.CMQTIME }",
						"SCHDATE":"${transfer_result_data.data.SCHDATE }",
						"OUTACN":"${transfer_result_data.data.OUTACN }",
						"SCHDATE":"${transfer_result_data.data.SCHDATE }",
						"INTSACN":"${transfer_result_data.data.INTSACN }",
						"AMOUNT":"${transfer_result_data.data.AMOUNT }",
						"HEADER":"${transfer_result_data.data.HEADER}",
						"INPCST":"${transfer_result_data.data.INPCST }",
						"CMTRMEMO":"${transfer_result_data.data.CMTRMEMO }",
						"O_TOTBAL":"${transfer_result_data.data.O_TOTBAL }",
						"O_AVLBAL":"${transfer_result_data.data.O_AVLBAL }",
						"I_TOTBAL":"${transfer_result_data.data.I_TOTBAL }",
						"I_AVLBAL":"${transfer_result_data.data.I_AVLBAL }",
						"result":"${transfer_result_data.result }",
						"FGTXDATE":"${transfer_result_data.data.FGTXDATE }",
						"FEE":"${transfer_result_data.data.FEE }"
					};
					openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
				});
			});
		</script>

</body>
</html>