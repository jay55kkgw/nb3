<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="../__import_head_tag.jsp" %>
    <%@ include file="../__import_js.jsp" %>
    <!-- 交易機制所需JS -->
    <script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
    <!--舊版驗證-->
    <script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
    <script type="text/JavaScript">
        var isTimeout = "${not empty sessionScope.timeout}";
        var countdownObjheader = isTimeout == 'true' ? "${sessionScope.timeout}" : 600;
        var countdownSecHeader = isTimeout == 'true' ? "${sessionScope.timeout}" : 600;
        $(document).ready(function () {
            //HTML載入完成後開始遮罩
            setTimeout("initBlockUI()", 10);
            // 開始查詢資料並完成畫面
            setTimeout("init()", 20);
            // 初始化驗證碼
// 	setTimeout("initKapImg()", 200);
// 	// 生成驗證碼
// 	setTimeout("newKapImg()", 300);
            //解遮罩
            setTimeout("unBlockUI(initBlockId)", 500);
            //timeout
            timeLogout();
        });

        function init() {
            //生日年開頭為0時，移除0
            var BHY = '${result_data.data.BIRTHY}';
            if (BHY.startsWith("0")) {
                BHY = BHY.substring(1);
            }
            $("#YY").text(BHY);

            $("#CPRIMJOBTYPE").find("option[value='${result_data.data.CPRIMJOBTYPE}']").attr("selected", true)
            $("#CPRIMJOBTYPEname").text($("#CPRIMJOBTYPE").find("option:selected").text());
            $("#CPRIMJOBTITLE").find("option[value='${result_data.data.CPRIMJOBTITLE}']").attr("selected", true)
            $("#CPRIMJOBTITLEname").text($("#CPRIMJOBTITLE").find("option:selected").text());
            $("#CPRIMCELLULANO1").val("${result_data.data.CPRIMCELLULANO1}");
            $("#CPRIMcellulanO1").text($("#CPRIMCELLULANO1").val().substring(0, 4) + "***" + $("#CPRIMCELLULANO1").val().substring(7));

            if (${result_data.data.CFU2 == "2"}) {
                $("#CFU2TD").html("<spring:message code= 'LB.D0107' />");
            } else {
                $("#CFU2TD").html("<spring:message code= 'LB.X1640' />");
            }

            if (${requestParam.CN != ""}) {
                $("#CNTD").html("${result_data.data.CN}${result_data.data.CARDDESC}");
            }

// 	if(${result_data.data.requestParam.VARSTR3 == "11"}){
// 		$("#V3").prop("checked",true);
// 	}

// 	if(${result_data.data.requestParam.CPRIMJOBTYPE != ""}){
// 		if(CPRIMJOBTYPE == "1"){
// 			$("input[type=checkbox][name=CJT][value=1]").prop("checked",true);
// 		}
// 		else if(CPRIMJOBTYPE == "2"){
// 			$("input[type=checkbox][name=CJT][value=2]").prop("checked",true);
// 		}
// 		else if(CPRIMJOBTYPE == "3"){
// 			$("input[type=checkbox][name=CJT][value=3]").prop("checked",true);
// 		}
// 		else if(CPRIMJOBTYPE == "4"){
// 			$("input[type=checkbox][name=CJT][value=4]").prop("checked",true);
// 		}
// 	}


// 	if(MCASH == "1"){
// 		$("input[type=checkbox][name=C2][value=1]").prop("checked",true);
// 	}
// 	else{
// 		$("input[type=checkbox][name=C2][value=2]").prop("checked",true);
// 	}

// 	if(${result_data.data.requestParam.VARSTR2 == "2"}){
// 		$("#C5").prop("checked",true);
// 	}


// 	if(CNOTE1 == "1"){
// 		$("input[type=checkbox][name=C3][value=1]").prop("checked",true);
// 	}
// 	else{
// 		$("input[type=checkbox][name=C3][value=2]").prop("checked",true);
// 	}


// 	if(CNOTE3 == "1"){
// 		$("input[type=checkbox][name=C4][value=1]").prop("checked",true);
// 	}
// 	else{
// 		$("input[type=checkbox][name=C4][value=2]").prop("checked",true);
// 	}

            $("#CMSUBMIT").click(function () {
                var FGTXWAY = "${result_data.data.FGTXWAY}";
                var CFU2 = "${result_data.data.CFU2}";

                //1為新戶須檢核身分證2為舊戶不須檢核身分證
// 		if(CFU2 == "1"){
// 			if($("#picFile").val() == ""){
// 				alert("<spring:message code= 'LB.X1074' />");
// 				return false;
// 			}
// 			if($("#picFile1").val() == ""){
// 				alert("<spring:message code= 'LB.X1075' />");
// 				return false;
// 			}
// 		}
                if (CFU2 == "1") {
                    if ($("#FILE1").val() == "") {
                        errorBlock(null, null, ["<spring:message code='LB.X1074' />"],
                            '<spring:message code= "LB.Quit" />', null);
// 				alert("<spring:message code= 'LB.X1074' />");
                        return false;
                    }
                    if ($("#FILE2").val() == "") {
                        errorBlock(null, null, ["<spring:message code='LB.X1075' />"],
                            '<spring:message code= "LB.Quit" />', null);
// 				alert("<spring:message code= 'LB.X1075' />");
                        return false;
                    }
                }
                //晶片金融卡
                if (FGTXWAY == "2") {
                    $("#ECERT").val("1");
                }
                //它行信用卡
                else if (FGTXWAY == "3") {
                    $("#ECERT").val("3");
                }
                //自然人憑證
                else if (FGTXWAY == "4") {
                    $("#ECERT").val("4");
                }
                //他行帳戶
                else if (FGTXWAY == "5") {
                    $("#ECERT").val("2");
                }

                if ('${transfer}' == 'en') {
                    $("#templatePath").val("/pdfTemplate/Creditcard_Apply_EN.html");
                } else if ('${transfer}' == 'zh') {
                    $("#templatePath").val("/pdfTemplate/Creditcard_Apply_CN.html");
                } else {
                    $("#templatePath").val("/pdfTemplate/Creditcard_Apply2.html");
                }

                $("#formId").attr("action", "${__ctx}/CREDIT/APPLY/apply_creditcard_p6");
                initBlockUI();
                $("#formId").submit();
// 		processQuery();
            });
            $("#backButton").click(function () {
                $("#back").val('Y');
                $("#formId").attr("action", "${__ctx}/CREDIT/APPLY/apply_creditcard_p4");
                $("#formId").submit();
            });
            chkLion();
        }

        /**
         * 初始化BlockUI
         */
        function initBlockUI() {
            initBlockId = blockUI();
        }

        // 交易機制選項
        function processQuery() {
            var fgtxway = $('input[name="FGTXWAY"]:checked').val();
            console.log("fgtxway: " + fgtxway);

            switch (fgtxway) {
                case '0':
//				alert("交易密碼(SSL)...");
                    $("form").submit();
                    break;

                case '1':
//				alert("IKey...");
                    var jsondc = $("#jsondc").val();

                    // 遮罩後不給捲動
//				document.body.style.overflow = "hidden";

                    // 呼叫IKEY元件
//				uiSignForPKCS7(jsondc);

                    // 解遮罩後給捲動
//				document.body.style.overflow = 'auto';

                    useIKey();
                    break;

                case '2':
//				alert("晶片金融卡");

                    // 遮罩後不給捲動
//				document.body.style.overflow = "hidden";

                    // 呼叫讀卡機元件
                    listReaders();

                    // 解遮罩後給捲動
//				document.body.style.overflow = 'auto';

                    break;

                default:
                    alert("nothing...");
            }

        }

        function previewImage(sourceID, targetID) {
            var fileReader = new FileReader();
            fileReader.readAsDataURL(document.getElementById(sourceID).files[0]);
            var max = document.getElementById(sourceID).files[0].size;
            fileReader.onprogress = function (evt) {
                console.log(evt.loaded / max * 100 + '%');
            }
            fileReader.onload = function (e) {
                document.getElementById(targetID).src = e.target.result;
            };
        }

        // 刷新驗證碼
        function refreshCapCode() {
            console.log("refreshCapCode...");

            // 驗證碼
            $('input[name="capCode"]').val('');

            // 大小版驗證碼用同一個
            $('img[name="kaptchaImage"]').hide().attr(
                'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();

            // 登入失敗解遮罩
            unBlockUI(initBlockId);
        }

        // 刷新輸入欄位
        function changeCode() {
            console.log("changeCode...");

            // 清空輸入欄位
            $('input[name="capCode"]').val('');

            // 刷新驗證碼
            refreshCapCode();
        }

        // 初始化驗證碼
        function initKapImg() {
            // 大小版驗證碼用同一個
            $('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
        }

        // 生成驗證碼
        function newKapImg() {
            // 大小版驗證碼用同一個
            $('img[name="kaptchaImage"]').click(function () {
                refreshCapCode();
            });
        }

        function chkLion() {
            var QR = "${result_data.data.QRCODE}";
            var ctype = "${result_data.data.CARDNAME}";
// 	if(QR != null && QR == 'L'){
// 		$("#liondata").show();
// 		if(ctype == '85'){
// 			$("#MemberID").show();
// 		}
// 	}
            if (ctype == '85' || ctype == '86') {
                $("#liondata").show();
                if (ctype == '85') {
                    $("#MemberID").show();
                }
            }
        }

        function timeLogout() {
            // 刷新session
            var uri = '${__ctx}/login_refresh';
            console.log('refresh.uri: ' + uri);
            var result = fstop.getServerDataEx(uri, null, false, null);
            console.log('refresh.result: ' + JSON.stringify(result));
            // 初始化登出時間
            $("#countdownheader").html(parseInt(countdownSecHeader) + 1);
            $("#countdownMin").html("");
            $("#mobile-countdownMin").html("");
            $("#countdownSec").html("");
            $("#mobile-countdownSec").html("");
            // 倒數
            startIntervalHeader(1, refreshCountdownHeader, []);
        }

        function refreshCountdownHeader() {
            // timeout剩餘時間
            var nextSec = parseInt($("#countdownheader").html()) - 1;
            $("#countdownheader").html(nextSec);

            // 提示訊息--即將登出，是否繼續使用
            if (nextSec == 120) {
                initLogoutBlockUI();
            }
            // timeout
            if (nextSec == 0) {
                // logout
                fstop.logout('${__ctx}' + '/logout_aj', '${__ctx}' + '/timeout_logout');
            }
            if (nextSec >= 0) {
                // 倒數時間以分秒顯示
                var minutes = Math.floor(nextSec / 60);
                $("#countdownMin").html(('0' + minutes).slice(-2));
                $("#mobile-countdownMin").html(('0' + minutes).slice(-2));

                var seconds = nextSec - minutes * 60;
                $("#countdownSec").html(('0' + seconds).slice(-2));
                $("#mobile-countdownSec").html(('0' + seconds).slice(-2));
            }
        }

        function startIntervalHeader(interval, func, values) {
            clearInterval(countdownObjheader);
            countdownObjheader = setRepeater(func, values, interval);
        }

        function setRepeater(func, values, interval) {
            return setInterval(function () {
                func.apply(this, values);
            }, interval * 1000);
        }

        /**
         * 初始化logoutBlockUI
         */
        function initLogoutBlockUI() {
            logoutblockUI();
        }

        /**
         * 畫面BLOCK
         */
        function logoutblockUI(timeout) {
            $("#logout-block").show();

            // 遮罩後不給捲動
            document.body.style.overflow = "hidden";

            var defaultTimeout = 60000;
            if (timeout) {
                defaultTimeout = timeout;
            }
        }

        /**
         * 畫面UNBLOCK
         */
        function unLogoutBlockUI(timeoutID) {
            if (timeoutID) {
                clearTimeout(timeoutID);
            }
            $("#logout-block").hide();

            // 解遮罩後給捲動
            document.body.style.overflow = 'auto';
        }

        /**
         *繼續使用
         */
        function keepLogin() {
            unLogoutBlockUI(); // 解遮罩
            timeLogout(); // 刷新倒數計時
        }
    </script>
</head>
<body>
<!-- 交易機制所需畫面 -->
<%@ include file="../component/trading_component.jsp" %>
<!-- header -->
<header>
    <%@ include file="../index/header_logout.jsp" %>
</header>

<!-- 麵包屑     -->
<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
    <ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
        <li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
        <!-- 申請信用卡     -->
        <li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0666"/></li>
    </ol>
</nav>

<!--左邊menu及登入資訊-->
<div class="content row">
    <%-- 	<%@ include file="../index/menu.jsp"%> --%>
    <!--快速選單及主頁內容-->
    <main class="col-12">
        <!--主頁內容-->
        <section id="main-content" class="container">
            <!--線上申請信用卡 -->
            <h2><spring:message code="LB.D0022"/></h2>
            <div id="step-bar">
                <ul>
                    <li class="finished">信用卡選擇</li><!-- 信用卡 -->
                    <li class="finished">身份驗證與權益</li><!-- 身份驗證與權益 -->
                    <li class="finished"><spring:message code="LB.X1967"/></li><!-- 申請資料 -->
                    <li class="active"><spring:message code="LB.Confirm_data"/></li><!-- 確認資料 -->
                    <li class=""><spring:message code="LB.X1968"/></li><!-- 完成申請 -->
                </ul>
            </div>
            <!-- timeout -->
            <section id="id-and-fast">
						<span class="id-name">
							<spring:message code="LB.Welcome_1"/>&nbsp;
							<c:if test="${empty result_data.data.CPRIMCHNAME}">
                                <span id="username" name="username_show"><spring:message code="LB.X2190"/></span>
                            </c:if>
							<c:if test="${not empty result_data.data.CPRIMCHNAME}">
                                <span id="username" name="username_show">${result_data.data.CPRIMCHNAME}</span>
                            </c:if>
							&nbsp;<spring:message code="LB.Welcome_2"/>
						</span>
                <div id="id-block">
                    <div>
							<span class="id-time">
								<fmt:setLocale value="${pageContext.response.locale}"/>
								<c:if test="${!empty sessionScope.logindt && !empty sessionScope.logintm}">
                                    <fmt:parseDate var="parseDate"
                                                   value="${sessionScope.logindt} ${sessionScope.logintm}"
                                                   pattern="yyyy-MM-dd HH:mm:ss"/>
                                    <fmt:formatDate value="${parseDate}" dateStyle="full"
                                                    type="both"/>&nbsp;<spring:message code="LB.X2250"/>
                                    <br/>
                                </c:if>

								<fmt:formatDate value="${sessionScope.login_time}" type="both" dateStyle="full"/>
								&nbsp;<spring:message code="LB.Login_successful"/>
							</span>
                        <!-- 自動登出剩餘時間 -->
                        <span class="id-time"><spring:message code="LB.X1912"/>
								<span class="high-light ml-1">
									<!-- 分 -->
									<font id="countdownMin"></font>
									:
                                    <!-- 秒 -->
									<font id="countdownSec"></font>
								</span>
							</span>
                    </div>
                    <button type="button" class="btn-flat-orange" onclick="timeLogout()"><spring:message
                            code="LB.X1913"/></button>
                    <button type="button" class="btn-flat-darkgray"
                            onClick="fstop.logout( '${__ctx}'+'/logout_aj' , '${__ctx}'+'/login')"><spring:message
                            code="LB.Logout"/></button>
                </div>
            </section>
            <form method="post" id="formId" enctype="multipart/form-data">
                <input type="hidden" name="ADOPID" value="NA03"/>
                <input type="hidden" name="back" id="back"/>
                <%-- 				<input type="hidden" name="CFU2" value="${result_data.data.requestParam.CFU2}"/><!-- 是否持有本行信用卡1否2是 --> --%>
                <%-- 				<input type="hidden" name="FGTXWAY" value="${result_data.data.requestParam.FGTXWAY}"/> --%>
                <%-- 				<input type="hidden" name="CARDNAME" value="${result_data.data.requestParam.CARDNAME}"/> --%>
                <%-- 				<input type="hidden" name="CARDMEMO" value="${result_data.data.requestParam.CARDMEMO}"/> --%>
                <%-- 				<input type="hidden" name="CN" value="${result_data.data.requestParam.CN}"/> --%>
                <%-- 				<input type="hidden" name="CARDDESC" value="${result_data.data.requestParam.CARDDESC}"/> --%>
                <%-- 				<input type="hidden" name="VARSTR2" value="${result_data.data.requestParam.VARSTR2}"/><!-- 悠遊卡是否同意預設開啟flag --> --%>
                <%-- 				<input type="hidden" name="OLAGREEN1" value="${result_data.data.requestParam.OLAGREEN1}"/> --%>
                <%-- 				<input type="hidden" name="OLAGREEN2" value="${result_data.data.requestParam.OLAGREEN2}"/> --%>
                <%-- 				<input type="hidden" name="OLAGREEN3" value="${result_data.data.requestParam.OLAGREEN3}"/> --%>
                <%-- 				<input type="hidden" name="OLAGREEN4" value="${result_data.data.requestParam.OLAGREEN4}"/> --%>
                <input type="hidden" id="ECERT" name="ECERT"/><!-- 交易機制 -->
                <input type="hidden" name="STATUS" value="0"/>
                <input type="hidden" name="VERSION" value="10808"/>
                <%-- 			    <input type="hidden" name="IP" value="${result_data.data.requestParam.IP}"/> --%>
                <%-- 			    <input type="hidden" name="BRANCHNAME" value="${result_data.data.requestParam.BRHNAME}"/> --%>
                <%-- 			    <input type="hidden" name="CPRIMCHNAME" value="${result_data.data.requestParam.CPRIMCHNAME}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMENGNAME" value="${result_data.data.requestParam.CPRIMENGNAME}"/> --%>
                <%-- 				<input type="hidden" name="CHENAME" value="${result_data.data.requestParam.CHENAME}"/> --%>
                <%-- 				<input type="hidden" name="CHENAMEChinese" value="${result_data.data.requestParam.CHENAMEChinese}"/> --%>
                <%-- 				<input type="hidden" name="CTRYDESC" value="${result_data.data.requestParam.CTRYDESC}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMBIRTHDAY" value="${result_data.data.requestParam.CPRIMBIRTHDAY}"/> --%>
                <%-- 				<input type="hidden" name="MPRIMEDUCATION" value="${result_data.data.requestParam.MPRIMEDUCATION}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMID" value="${result_data.data.requestParam.CUSIDN}"/> --%>
                <%-- 				<input type="hidden" name="CUSIDN" value="${result_data.data.requestParam.CUSIDN}"/> --%>
                <%-- 				<input type="hidden" name="MPRIMMARRIAGE" value="${result_data.data.requestParam.MPRIMMARRIAGE}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMADDR2" value="${result_data.data.requestParam.CPRIMADDR2}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMHOMETELNO2A" value="${result_data.data.requestParam.CPRIMHOMETELNO2}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMHOMETELNO2B" value="${result_data.data.requestParam.CPRIMHOMETELNO2}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMADDR" value="${result_data.data.requestParam.CPRIMADDR}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMHOMETELNO1A" value="${result_data.data.requestParam.CPRIMHOMETELNO}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMHOMETELNO1B" value="${result_data.data.requestParam.CPRIMHOMETELNO}"/> --%>
                <%-- 				<input type="hidden" name="MPRIMADDR1COND" value="${result_data.data.requestParam.MPRIMADDR1COND}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMCELLULANO1" value="${result_data.data.requestParam.CPRIMCELLULANO1}"/> --%>
                <%-- 				<input type="hidden" name="MBILLTO" value="${result_data.data.requestParam.MBILLTO}"/> --%>
                <%-- 				<input type="hidden" name="MCARDTO" value="${result_data.data.requestParam.MCARDTO}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMEMAIL" value="${result_data.data.requestParam.CPRIMEMAIL}"/> --%>
                <%-- 				<input type="hidden" name="VARSTR3" value="${result_data.data.requestParam.VARSTR3}"/> --%>
                <%-- 				<input type="hidden" name="FDRSGSTAFF" value="${result_data.data.requestParam.FDRSGSTAFF}"/> --%>
                <%-- 				<input type="hidden" name="BANKERNO" value="${result_data.data.requestParam.BANKERNO}"/> --%>
                <%-- 				<input type="hidden" name="MEMO" value="${result_data.data.requestParam.MEMO}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMJOBTYPE" value="${result_data.data.requestParam.CPRIMJOBTYPE}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMCOMPANY" value="${result_data.data.requestParam.CPRIMCOMPANY}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMJOBTITLE" value="${result_data.data.requestParam.CPRIMJOBTITLE}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMSALARY" value="${result_data.data.requestParam.CPRIMSALARY}"/> --%>
                <%-- 				<input type="hidden" name="WORKYEARS" value="${result_data.data.requestParam.WORKYEARS}"/> --%>
                <%-- 				<input type="hidden" name="WORKMONTHS" value="${result_data.data.requestParam.WORKMONTHS}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMADDR3" value="${result_data.data.requestParam.CPRIMADDR3}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMOFFICETELNO1A" value="${result_data.data.requestParam.CPRIMOFFICETELNO1}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMOFFICETELNO1B" value="${result_data.data.requestParam.CPRIMOFFICETELNO1}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMOFFICETELNO1C" value="${result_data.data.requestParam.CPRIMOFFICETELNO1}"/> --%>
                <%-- 				<input type="hidden" name="MCASH" value="${result_data.data.requestParam.MCASH}"/> --%>
                <%-- 				<input type="hidden" name="CNOTE4" value="${result_data.data.requestParam.VARSTR2}"/> --%>
                <%-- 				<input type="hidden" name="CNOTE1" value="${result_data.data.requestParam.CNOTE1}"/> --%>
                <%-- 				<input type="hidden" name="CNOTE3" value="${result_data.data.requestParam.CNOTE3}"/> --%>
                <%-- 				<input type="hidden" name="oldcardowner" value="${result_data.data.requestParam.oldcardowner}"/> --%>
                <%-- 				<input type="hidden" name="BIRTHY" value="${result_data.data.requestParam.BIRTHY}"/> --%>
                <%-- 			  	<input type="hidden" name="BIRTHM" value="${result_data.data.requestParam.BIRTHM}"/> --%>
                <%-- 			  	<input type="hidden" name="BIRTHD" value="${result_data.data.requestParam.BIRTHD}"/> --%>
                <%-- 			  	<input type="hidden" name="CTTADR" value="${result_data.data.requestParam.CTTADR}"/> --%>
                <%-- 			  	<input type="hidden" name="PMTADR" value="${result_data.data.requestParam.PMTADR}"/> --%>
                <%-- 			  	<input type="hidden" name="MPRIMEDUCATIONChinese" value="${result_data.data.requestParam.MPRIMEDUCATIONChinese}"/> --%>
                <%-- 				<input type="hidden" name="MPRIMMARRIAGEChinese" value="${result_data.data.requestParam.MPRIMMARRIAGEChinese}"/> --%>
                <%-- 				<input type="hidden" name="MPRIMADDR1CONDChinese" value="${result_data.data.requestParam.MPRIMADDR1CONDChinese}"/> --%>
                <%-- 				<input type="hidden" name="MBILLTOChinese" value="${result_data.data.requestParam.MBILLTOChinese}"/> --%>
                <%-- 				<input type="hidden" name="MCARDTOChinese" value="${result_data.data.requestParam.MCARDTOChinese}"/> --%>
                <%-- 				<input type="hidden" name="CPRIMJOBTITLEChinese" value="${result_data.data.requestParam.CPRIMJOBTITLEChinese}"/> --%>
                <%-- 				<input type="hidden" name="alldata" value="${result_data.data}"/> --%>
                <input type="hidden" name="ISSUER" value="">
                <input type="hidden" name="ACNNO" value="">
                <input type="hidden" name="iSeqNo" value="">
                <input type="hidden" name="ICSEQ" value="">
                <input type="hidden" name="TAC" value="">
                <input type="hidden" name="TRMID" value="">
                <!--信用卡PDF會用到-->
                <input type="hidden" id="templatePath" name="templatePath" value=""/>
                <div class="main-content-block row">
                    <div class="col-12">
                        <div class="ttb-input-block">
                            <div class="ttb-message">
                                <!-- 確認資料 -->
                                <p><spring:message code="LB.Confirm_data"/></p>
                            </div>
                            <!-- 請確認以下申請資料 -->
                            <p class="form-description"><spring:message code="LB.X2026"/></p>
                            <div class="classification-block">
                                <!-- 申請信用卡 -->
                                <p><spring:message code="LB.D0666"/></p>
                            </div>
                            <!-- 客戶狀態 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D0106"/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <span id="CFU2TD">
										</span>
                                    </div>
                                </span>
                            </div>
                            <!-- 申請信用卡卡別 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D0108"/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <span id="CNTD">
                                        </span>
                                    </div>
                                </span>
                            </div>

                            <span id="liondata" style="display:none">
	                            <div class="classification-block">
	                                <!-- 會員資料 -->
	                                <p><spring:message code='LB.X2363'/></p>
	                            </div>
                                <!-- 分區 -->
		                        <div id="Partition" class="ttb-input-item row">
		                        	<span class="input-title">
		                        		<label>
		                        			<h4><spring:message code='LB.X2364'/></h4>
		                        		</label>
		                        	</span>
		                        	<span class="input-block">
		                        		<div class="ttb-input">
			                        		<c:if test="${result_data.data.partition == '  '}">
                                                <span><spring:message code="LB.D0572"/></span>
                                            </c:if>
			                        		<c:if test="${result_data.data.partition != '  '}">
                                                <span>${result_data.data.partition}</span>
                                            </c:if>
		                        		</div>
		                        	</span>
		                        </div>
                                <!-- 分會 -->
		                        <div id="Branch" class="ttb-input-item row">
		                        	<span class="input-title">
		                        		<label>
		                        			<h4><spring:message code='LB.X2365'/></h4>
		                        		</label>
		                        	</span>
		                        	<span class="input-block">
		                        		<div class="ttb-input">
		                        			<span>${result_data.data.branch}</span>
		                        		</div>
		                        	</span>
		                        </div>
                                <!-- 會員編號 -->
		                        <div id="MemberID" class="ttb-input-item row" style="display:none">
		                        	<span class="input-title">
		                        		<label>
		                        			<h4><spring:message code='LB.X2366'/></h4>
		                        		</label>
		                        	</span>
		                        	<span class="input-block">
		                        		<div class="ttb-input">
		                        			<span>${result_data.data.memberId}</span>
		                        		</div>
		                        	</span>
		                        </div>
	                        </span>

                            <div class="classification-block">
                                <!-- 基本資料 -->
                                <p><spring:message code="LB.D0109"/></p>
                            </div>
                            <!-- 中文姓名 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D0049"/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <span>${result_data.data.CPRIMCHNAME}</span>
                                    </div>
                                </span>
                            </div>
                            <!--更換過中文姓名-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.X1984"/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
<%--                                         <span><spring:message code="${result_data.data.requestParam.CHENAMEChinese}" /></span> --%>
                                        <span>${result_data.data.CHENAMEChinese}</span>
                                    </div>
                                </span>
                            </div>
                            <!--身分證字號-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D0581"/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <span>${result_data.data.CUSIDN}</span>
                                    </div>
                                </span>
                            </div>
                            <!--國籍-->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.X0205"/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <span>${result_data.data.CTRYDESC}</span>
                                    </div>
                                </span>
                            </div>
                            <!-- 行動電話 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D0069"/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <span><font id="CPRIMcellulanO1"></font></span>
                                        <input type="hidden" name="CPRIMCELLULANO1" id="CPRIMCELLULANO1"/>
                                    </div>
                                </span>
                            </div>
                            <!-- 職業類別 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.X2437"/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                    <span id="CPRIMJOBTYPEname"></span>
                                       <select class="custom-select select-input" name="CPRIMJOBTYPE" id="CPRIMJOBTYPE"
                                               style="display: none;">
                                           <option value="#"><spring:message code='LB.X2447'/></option><!-- 請選擇職業類別 -->
                                           <option value="061100"><spring:message code='LB.X2439'/></option>
                                           <!-- 軍官、軍人 -->
                                           <option value="061200"><spring:message code='LB.X2440'/></option>
                                           <!-- 警官、員警 -->
                                           <option value="061300"><spring:message code='LB.D0873'/></option>
                                           <!-- 其它公共行政業 -->
                                           <option value="061400"><spring:message code='LB.D0874'/></option><!-- 教育業 -->
                                           <option value="061410"><spring:message code='LB.D0081'/></option><!-- 學生 -->
                                           <option value="061500"><spring:message code='LB.D0876'/></option>
                                           <!-- 工、商及服務業-->
                                           <option value="0615A0"><spring:message code='LB.D0877'/></option>
                                           <!-- 農林漁牧業 -->
                                           <option value="0615B0"><spring:message code='LB.D0878'/></option>
                                           <!-- 礦石及土石採取業 -->
                                           <option value="0615C0"><spring:message code='LB.D0879'/></option><!-- 製造業 -->
                                           <option value="0615D0"><spring:message code='LB.D0880'/></option>
                                           <!-- 水電燃氣業 -->
                                           <option value="0615E0"><spring:message code='LB.D0881'/></option><!-- 營造業 -->
                                           <option value="0615F0"><spring:message code='LB.D0882'/></option>
                                           <!-- 批發及零售業 -->
                                           <option value="0615G0"><spring:message code='LB.D0883'/></option>
                                           <!-- 住宿及餐飲業 -->
                                           <option value="0615H0"><spring:message code='LB.D0884'/></option>
                                           <!-- 運輸、倉儲及通信業 -->
                                           <option value="0615I0"><spring:message code='LB.D0885'/></option>
                                           <!-- 金融及保險業 -->
                                           <option value="0615J0"><spring:message code='LB.D0886'/></option>
                                           <!-- 不動產及租賃業 -->
                                           <option value="061610"><spring:message code='LB.X2441'/></option>
                                           <!-- 其他專業服務業 (建築、電腦資訊、設計、顧問、研發、醫護、社服等服務業) -->
                                           <option value="061620"><spring:message code='LB.X2442'/></option>
                                           <!-- 技術服務業 (出版、廣告、影視、休閒、保全、環保、維修、宗教、團體、美髮、殯葬、停車場…等) -->
                                           <option value="069999"><spring:message code='LB.D0899'/></option>
                                           <!-- 非法人組織授信戶負責人 -->
                                           <option value="061630"><spring:message code='LB.D0889'/></option>
                                           <!-- 特定專業服務業(律師、會計師、地政士、公證人、記帳士、記帳及報稅代理人) -->
                                           <option value="061640"><spring:message code='LB.D0890'/></option>
                                           <!-- 特定專業服務業(受聘於專業服務業之行政事務職員) -->
                                           <option value="061650"><spring:message code='LB.D0891'/></option>
                                           <!-- 銀樓業 (包含珠寶、鐘錶及貴金屬之製造、批發及零售) -->
                                           <option value="061660"><spring:message code='LB.D0892'/></option>
                                           <!-- 虛擬貨幣交易服務業 -->
                                           <option value="061670"><spring:message code='LB.D0893'/></option><!-- 博弈業 -->
                                           <option value="061680"><spring:message code='LB.D0894'/></option>
                                           <!-- 國防武器或戰爭設備相關行業(軍火) -->
                                           <option value="061690"><spring:message code='LB.D0082'/></option><!-- 家管 -->
                                           <option value="061691"><spring:message code='LB.D0083'/></option><!-- 自由業 -->
                                           <option value="061692"><spring:message code='LB.X0571'/></option><!-- 無業 -->
                                           <option value="061700"><spring:message code='LB.D0572'/></option><!-- 其他 -->
                                       </select>
                                    </div>
                                </span>
                            </div>
                            <!-- 職位名稱 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.X2438"/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                    <span id="CPRIMJOBTITLEname"></span>
                                       <select class="custom-select select-input" name="CPRIMJOBTITLE"
                                               id="CPRIMJOBTITLE" style="display: none;">
                                           <option value="#"><spring:message code="LB.X2448"/></option><!-- 請選擇職位名稱 -->
										<option value="01"><spring:message code='LB.X2443'/></option><!-- 董事長/負責人 -->
										<option value="02"><spring:message code='LB.X2444'/></option> <!--總經理 -->
										<option value="03"><spring:message code="LB.X0561"/></option><!-- 主管 -->
										<option value="04"><spring:message code="LB.X0588"/></option><!-- 專業人員 -->
										<option value="05"><spring:message code='LB.X2445'/></option><!-- 職員 -->
										<option value="06"><spring:message code='LB.X2446'/></option><!-- 業務/服務人員 -->
										<option value="07"><spring:message code='LB.D0572'/></option><!-- 其他 -->
                                       </select>
                                    </div>
                                </span>
                            </div>
                            <!-- 卡片郵寄地址 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.onlineapply_creditcard_cttadrtitle"/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
										<span class="ttb-input">
											<c:if test="${result_data.data.MBILLTO =='1'}">
                                                <!-- 戶籍地址 -->
                                                【<spring:message
                                                    code="LB.D0143"/>】&nbsp;${result_data.data.strZIP}&nbsp;${result_data.data.CPRIMADDR0}
                                            </c:if>
											<c:if test="${result_data.data.MBILLTO =='2'}">
                                                <!-- 居住地址 -->
                                                【<spring:message
                                                    code="LB.D0063"/>】&nbsp;${result_data.data.strZIP}&nbsp;${result_data.data.CPRIMADDR0}
                                            </c:if>
											<c:if test="${result_data.data.MBILLTO =='3'}">
                                                <!-- 公司地址 -->
                                                【<spring:message
                                                    code="LB.D0090"/>】&nbsp;${result_data.data.strZIP}&nbsp;${result_data.data.CPRIMADDR0}
                                            </c:if>
										</span>
                                    </div>
                                </span>
                            </div>
                        </div>
                        <input type="button" id="backButton" value="<spring:message code="LB.X0318" />"
                               class="ttb-button btn-flat-gray"/><!-- 上一步 -->
                        <input type="button" id="CMSUBMIT" value="<spring:message code="LB.X0302" />"
                               class="ttb-button btn-flat-orange"/><!-- 送出申請 -->
                    </div>
                </div>
            </form>
        </section>
    </main>
</div>
<%@ include file="../index/footer.jsp" %>
</body>
</html>