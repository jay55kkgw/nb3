<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!--  首次登入 -->
<section id="first-login" class="modal fade active show more-info-block" role="dialog" aria-labelledby="" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<p class="ttb-pup-header"><spring:message code= "LB.X2251" /></p><!-- 首次登入新網銀資料轉檔 -->
			</div>
			<div id="first-login-1">
				<div class="modal-body">
					<div class="ttb-input-block mt-0">
						<div class="ttb-input-item row">
							<span id="TXNTWRECORD" class="input-title cus-data">
							</span>
							<span class="input-block">
								<span><spring:message code= "LB.X2252" /></span><!-- 臺幣轉帳結果檔 -->
							</span>
						</div>
						<div class="ttb-input-item row">
							<span id="TXNTWSCHPAY" class="input-title cus-data">
							</span>
							<span class="input-block">
								<span><spring:message code= "LB.X2253" /></span><!-- 臺幣預約交易設定檔 -->
							</span>
						</div>
						<div class="ttb-input-item row">
							<span id="TXNFXRECORD" class="input-title cus-data">
							</span>
							<span class="input-block">
								<span><spring:message code= "LB.X2254" /></span><!--外匯轉帳結果檔 -->
							</span>
						</div>
						<div class="ttb-input-item row">
							<span id="TXNFXSCHPAY" class="input-title cus-data">
							</span>
							<span class="input-block">
								<span><spring:message code= "LB.X2255" /></span><!-- 外幣預約交易設定檔 -->
							</span>
						</div>
						<div class="ttb-input-item row">
							<span id="TXNGDRECORD" class="input-title cus-data">
							</span>
							<span class="input-block">
								<span><spring:message code= "LB.X2256" /></span><!-- 黃金交易結果檔 -->
							</span>
						</div>
						<div class="ttb-input-item row">
							<span id="TXNTRACCSET" class="input-title cus-data">
							</span>
							<span class="input-block">
								<span><spring:message code= "LB.X2257" /></span><!-- 常用轉入帳號設定檔 -->
							</span>
						</div>
						
						<!-- 電郵記錄檔--20200316 不即時同步，改用批次 -->
<!-- 						<div class="ttb-input-item row"> -->
<!-- 							<span id="maillog" class="input-title cus-data"> -->
<!-- 							</span> -->
<!-- 							<span class="input-block"> -->
<!-- 								<span><spring:message code= "LB.X2258" /></span>電郵記錄檔 -->
<!-- 							</span> -->
<!-- 						</div> -->
						
						<div class="ttb-input-item row">
							<span id="TXNADDRESSBOOK" class="input-title cus-data">
							</span>
							<span class="input-block">
								<span><spring:message code= "LB.X2259" /></span><!-- 使用者通訊錄檔 -->
							</span>
						</div>
						<div class="ttb-input-item row">
							<span id="TXNCUSINVATTRIB" class="input-title cus-data">
							</span>
							<span class="input-block">
								<span><spring:message code= "LB.X2260" /></span><!-- 基金客戶投資屬性線上調查紀錄檔 -->
							</span>
						</div>
						<div class="ttb-input-item row">
							<span id="TXNCUSINVATTRHIST" class="input-title cus-data">
							</span>
							<span class="input-block">
								<span><spring:message code= "LB.X2261" /></span><!-- 基金客戶投資屬性線上調查歷史紀錄檔 -->
							</span>
						</div>
						<div class="ttb-input-item row">
							<span id="TXNPHONETOKEN" class="input-title cus-data">
							</span>
							<span class="input-block">
								<span><spring:message code= "LB.X2262" /></span><!-- 使用者手機推撥設定檔 -->
							</span>
						</div>
						<div class="ttb-input-item row">
							<span id="NB3USER" class="input-title cus-data">
							</span>
							<span class="input-block">
								<span><spring:message code= "LB.X2263" /></span><!-- 新世代網銀客戶檔 -->
							</span>
						</div>
					</div>
					
					<ol class="description-list list-decimal">
						<p><spring:message code= "LB.X2264" /></p><!-- 首次登入新網銀，資料轉檔如若失敗，請洽客服，客服專線請撥 0800-017-171 -->
					</ol>

				</div>
				<div class="modal-footer ttb-pup-footer">
					<input id="CMCLOSE" type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Confirm" />" />
				</div>
			</div>
		</div>
		
					
	</div>
</section>	
			
			
<script type="text/javascript">
	
	// 第一階段首次登入轉檔視窗
	$('#first-login').modal('show');
	// 第一階段首次登入轉檔--20201209修正為頁面載入完畢後執行
// 	initData();

	// 檢查第一階段首次登入轉檔，避免重新整理
	function initData() {
		console.log('initData....................');
		
		uri = '${__ctx}' + "/FIRST/LOGIN/cusdata_aj";
		console.log("initData.uri: " + uri);

		fstop.getServerDataEx(uri, null, true, checkData);
	}
	
	// 判斷之前是否全部執行完畢
	function checkData(data) {
		console.log("checkData.data: " + JSON.stringify(data));
		
		// 已經成功執行
		if(data && data.result){
			$( ".cus-data" ).empty();
			$( ".cus-data" ).prepend('<img style="width: 16px;" src="/nb3/img/check-circle-regular.svg">');
			
			$("#CMCLOSE").removeClass("btn-flat-gray");
			$("#CMCLOSE").addClass("btn-flat-orange");
			
			// 全部執行完畢可關閉轉檔視窗
			$("#CMCLOSE").click(function (e) {
				$('#first-login').modal('hide');
				if($("#remind-block")) {
					$("#remind-block").show();
				}
			});
			
		} else {
			transferData_up(); // 改在SERVER一次完成
// 			transferData();
		}
	}
	
	// 第一階段首次登入轉檔視窗
	function transferData_up() {
		console.log('transferData_up.....................');
		
		$(".cus-data").empty();
		$(".cus-data").prepend('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
		
		uri = '${__ctx}' + "/FIRST/LOGIN/transfer_aj";
		console.log("transfer_aj.uri: " + uri);

		fstop.getServerDataEx(uri, null, true, checkFinish_up);
	}
	
// ----------------------------------------------------------------------------------------------------
	
	// 是否全部執行完畢
	function checkFinish_up(data) {
		console.log("checkFinish_up.data: " + JSON.stringify(data));
		
		// 成功執行
		if(data && data.result){
			$(".cus-data").empty();
			$(".cus-data").prepend('<img style="width: 16px;" src="/nb3/img/check-circle-regular.svg">');
			
			$("#CMCLOSE").removeClass("btn-flat-gray");
			$("#CMCLOSE").addClass("btn-flat-orange");
			
			
			// 20201209--轉檔成功首頁再初始化JS
			index_init();
			
			// 全部執行完畢可關閉轉檔視窗
			$("#CMCLOSE").click(function (e) {
				$('#first-login').modal('hide');
				if($("#remind-block")) {
					$("#remind-block").show();
				}
			});
			
		} else {
			var type = data.data;
			
			$(".cus-data").empty();
			$("#"+type).prepend('<img style="width: 16px;" src="/nb3/img/times-circle-solid.svg">');

			$("#CMCLOSE").removeClass("btn-flat-gray");
			$("#CMCLOSE").addClass("btn-flat-orange");
			
			// 其一失敗則需登出
			fstop.getServerDataEx('${__ctx}'+'/logout_aj', null, true); // 先做登出以免使用者直接輸入網址或重整
			$("#CMCLOSE").click(function (e) {
				fstop.logout( '${__ctx}'+'/logout_aj' , '${__ctx}'+'/login');
			});
		}
	}
	
</script>