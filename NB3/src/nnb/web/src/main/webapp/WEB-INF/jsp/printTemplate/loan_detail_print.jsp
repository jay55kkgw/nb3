<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ include file="../__import_js.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<script type="text/javascript"
			src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
		<title>${jspTitle}</title>
		<script type="text/javascript">
			$(document).ready(function() {
				window.print();
			});
			
			
		</script>
		<style>
		@page {
			margin: 7%; /*print邊界*/
		}
		</style>
</head>
	<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
		<c:set var="list_Size" value="${dataListMap}"></c:set>
		<br />
		<br />
		<div style="text-align: center">
			<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
		</div>
		<br />
		<div style="text-align: center">
			<font style="font-weight: bold; font-size: 1.2em">${jspTitle}</font>
		</div>
		<br />
		<!-- 查詢時間 -->
		<label><spring:message code="LB.Inquiry_time" /> ：</label>
		<label>${CMQTIME}</label>
		<br />
		<!-- 資料總數 -->
		<label><spring:message code="LB.Total_records" /> ：</label>
		<label> ${CMRECNUM}&nbsp;<spring:message
				code="LB.Rows" />
		</label>
		<br/>
		<label><spring:message code="LB.Note" /> ：</label>
		<label> ${note}&nbsp;
		</label>
		<br />
		<!-- 台幣借款明細表-->
		<table class="print" data-toggle-column="first">
			<thead>
				<tr>
					<!-- 表名-->
					<th><spring:message code="LB.Report_name" /></th>
					<th class="text-left" colspan="8">
						<!-- 台幣借款明細表--> <spring:message code="LB.NTD_loan_detail" />
					</th>
				</tr>
				<tr>
					<!-- 帳號-->
					<th><spring:message code="LB.Loan_account" /></th>
					<!-- 分號-->
					<th data-breakpoints="xs sm md"><spring:message
							code="LB.Seq_of_account" /></th>
					<!-- 原貸金額-->
					<th data-breakpoints="xs sm md"><spring:message
							code="LB.Original_loan_amount" /></th>
					<!-- 貸款餘額-->
					<th><spring:message code="LB.Loan_Outstanding" /></th>
					<!-- 利率(%)-->
					<th data-breakpoints="xs sm"><spring:message
							code="LB.Interest_rate1" /></th>
					<!-- 初貸日（額度起日）-->
					<th data-breakpoints="xs"><spring:message
							code="LB.Loan_drawdown_date" /><br>
					<spring:message code="LB.Loan_drawdown_date2" /></th>
					<!-- 到期日（額度迄日）-->
					<th data-breakpoints="xs"><spring:message
							code="LB.Expired_date" /><br>
					<spring:message code="LB.Expired_date2" /></th>
					<!-- 開始償還本金月份-->
					<th data-breakpoints="xs sm md"><spring:message
							code="LB.Months_of_deferred_principal" /></th>
					<!-- 繳息迄日-->
					<th data-breakpoints="xs"><spring:message
							code="LB.End_date_of_repay_interest" /></th>
				</tr>
			</thead>
			
			<c:choose>
				<c:when test="${TOPMSG_320 == 'NoCall'}">
					<tbody style="display:none"></tbody>
				</c:when>
				<c:when test="${TOPMSG_320 != 'OKLR' && TOPMSG_320 != 'OKOV'}">
				<tbody>
					<tr>
						<td>${TOPMSG_320}</td>
						<td colspan="8">${errorMsg320}</td>
					</tr>
				</tbody>
				</c:when>
				<c:when test="${TOPMSG_320 eq 'OKLR' || TOPMSG_320 eq 'OKOV'}">
					<c:choose>
						<c:when test="${MSGFLG_320=='01'}">
						<tbody>
							<tr>
								<td>MSGFLG=01</td>
								<!-- 請洽櫃檯 -->
								<td colspan="8"><spring:message code= "LB.Loan_contact_note" /></td>
							</tr>
						</tbody>
						</c:when>
						<c:when test="${MSGFLG_320=='00'}">
							<c:forEach var="dataList" items="${dataListMap[0]}">
							<tbody>
								<tr>
									<td class="text-center">${dataList.ACNCOVER}</td>
									<td class="text-center">${dataList.SEQ}</td>
									<!--原貸金額-->
									<td class="text-right">${dataList.AMTORLN}</td>
									<!-- 貸款餘額 -->
									<td class="text-right">${dataList.BAL}</td>
									<td class="text-right">${dataList.ITR}</td>
									<td class="text-center">${dataList.DATFSLN}</td>
									<td class="text-center">${dataList.DDT}</td>
									<td class="text-center">${dataList.AWT}</td>
									<td class="text-center">${dataList.DATITPY}</td>
								</tr>
							</tbody>
							</c:forEach>
						</c:when>
					</c:choose>
				</c:when>
			</c:choose>
		</table>
		<c:if test="${TOPMSG_320 =='OKOV' || TOPMSG_320 =='OKLR'}">
			<div class="text-left">
			<!-- 總計XX筆-->
				<p>
					<spring:message code="LB.Total_records" />
					${TWNUM} &nbsp;
					<spring:message code="LB.Rows" />
				</p>
			<!-- 新臺幣XXXX元-->
				<p>
					<spring:message code="LB.NTD" />
					&nbsp;${TOTALBAL}&nbsp;
					<spring:message code="LB.Dollar" />
				</p>
				<spring:message code="LB.Loan_detail_P1_NTD_note" />
				<!-- 可提前償還本金(新臺幣):繳息狀況正常之中長期貸款戶(不含青創、微創等創業貸款)，償還方式屬本息平均攤還者。 -->
			</div>
		</c:if>
		<br>
		<!-- 外幣借款明細表-->
		<table class="print" data-toggle-column="first">
			<thead>
				<tr>
					<th>
						<!-- 表名--> <spring:message code="LB.Report_name" />
					</th>
					<th class="text-left" colspan="9">
						<!-- 外幣借款明細表--> <spring:message code="LB.FX_loan_detail" />
					</th>
				</tr>
				<tr>
					<!-- 帳號-->
					<th><spring:message code="LB.Loan_account" /></th>
					<!-- 交易編號-->
					<th data-breakpoints="xs sm md"><spring:message code= "LB.Transaction_Number" /></th>
					<!-- 幣別-->
					<th data-breakpoints="xs sm md"><spring:message
							code="LB.Currency" /></th>
					<!-- 放款餘額-->
					<th><spring:message code= "LB.Loan_Balance" /></th>
					<!-- 動撥日-->
					<th data-breakpoints="xs sm"><spring:message code= "LB.X0965" /></th>
					<!-- 到期日-->
					<th data-breakpoints="xs"><spring:message
							code="LB.Expired_date" /></th>
					<!-- 下次繳息日-->
					<th data-breakpoints="xs"><spring:message code= "LB.X0966" /></th>
					<!-- 適用利率-->
					<th data-breakpoints="xs sm md"><spring:message code= "LB.X0967" /></th>
					<!-- 資料日期-->
					<th data-breakpoints="xs"><spring:message code="LB.Data_date" /></th>
					<!-- 備註-->
					<th data-breakpoints="xs"><spring:message code="LB.Note" /></th>
				</tr>
			</thead>
			
				<c:if test="${TOPMSG_552 eq 'OKLR'||TOPMSG_552 eq 'OKOV'}">
					<c:forEach var="dataList" items="${dataListMap[1]}">
					<tbody>
					<tr>
						<td class="text-center">${dataList.ACNCOVER}</td>
						<td class="text-center">${dataList.LNREFNO}${dataList.lcMemo}</td>
						<td class="text-center">${dataList.LNCCY} <!-- MB5用 -->
						</td>
						<td class="text-right">${dataList.LNCOS}</td>
						<!--<td>${dataList._BAL }</td>-->
						<td class="text-center">${dataList.LNINTST}</td>
						<td class="text-center">${dataList.LNDUEDT}</td>
						<td class="text-center">${dataList.NXTINTD}</td>
						<td class="text-right">${dataList.LNFIXR}</td>
						<td class="text-center">${dataList.LNUPDATE}</td>
						<td class="text-center">${dataList.TYPEA}</td>
					</tr>
					</tbody>
				</c:forEach>
				</c:if>
				<c:if test="${TOPMSG_552 != 'OKLR' && TOPMSG_552 != 'OKOV'}">
					<tbody>
					<tr>
						<td>${TOPMSG_552}</td>
						<td colspan="9">${errorMsg552}</td>
					</tr>
					</tbody>
				</c:if>
				<c:if test="${TOPMSG_552 eq 'NoCall'}">
							<tbody style="display:none"></tbody>
				</c:if>
		</table>
		<c:if test="${TOPMSG_552 =='OKOV' || TOPMSG_552 =='OKLR'}">
		<div class="text-left">
			<!-- 總計XX筆-->
			<p>
				<spring:message code="LB.Total_records" />
				${FXNUM}
				<spring:message code="LB.Rows" />
			</p>
			<!-- 外幣 各幣別金額總和-->
			<c:forEach var="dataList" items="${dataListMap[2]}">
				<p>${dataList.AMTLNCCY} &nbsp;${dataList.FXTOTAMT} &nbsp;
					<spring:message code="LB.Dollar"/></p>
			</c:forEach>
		</div>
		</c:if>
			<spring:message code="LB.Description_of_page" /><!-- 說明 -->
				<ol class="list-decimal text-left">
				<li><spring:message code="LB.Loan_detail_P1_D1"/>
				<!-- 延滯繳息還本者，請逕洽原承貸分行辦理。 -->
				</li>
				<!-- 外幣借款提供至前一營業日交易資料。 -->
				<li><spring:message code= "LB.Loan_detail_P1_D2" /></li>
				<!-- 外幣借款項目：買入光票、貼現、短（擔）放、中（擔）放、長（擔）放。 -->
				<li><spring:message code= "LB.Loan_detail_P1_D3" /></li>
				<li><spring:message code="LB.Loan_detail_P1_D4"/>
				<!-- 消費者新台幣借款之利率調整通知為貸款利率引用本行２年定儲、 本行定儲指數、本行基準利率、本行１年定儲、本行定儲指數 、郵一定儲利率、行員中期利率、郵二定儲利率 、行員消貸利率、 基準利率月調 ，利率引用標準非屬上開者，請逕洽原貸分行。 -->
				</li>
				</ol>
		
	</body>
</html>