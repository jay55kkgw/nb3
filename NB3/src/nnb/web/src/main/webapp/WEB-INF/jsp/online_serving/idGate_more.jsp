<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
</head>
<style>
	.application-block ul {
	    margin: 20px 40px 0 40px;
	}
	
	.application-block .application-list li {
	    margin: 15px 0 10px 0;
	}
</style>
<script type="text/javascript">
	$(document).ready(function(){
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	function init(){
		$("#CMBACK").click(function(e){
			window.close();
		});
	}
	
	function pdfdownload(data){
		window.open("${__ctx}/CUSTOMER/SERVICE/pdf_download?pdf="+data,"_black");
	}
</script>
<body>
   	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	<!-- 麵包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><a href="#">申請裝置認證服務</a></li>
		</ol>
	</nav>
	
	<!-- 左邊menu 及登入資訊 -->
	<div class="content row">
		<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2>
					申請裝置認證服務
				</h2>
				<form id="formId" method="post" action="">
					<div class="card-block shadow-box terms-pup-blcok">
	                    <nav class="nav card-select-block text-center d-block" style="padding-top: 30px;" id="nav-tab" role="tablist">
	                        <input type="button" class="nav-item ttb-sm-btn active" id="nav-trans-1-tab" data-toggle="tab" href="#nav-trans-1" role="tab" aria-selected="false" value="裝置認證的好處" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-2-tab" data-toggle="tab" href="#nav-trans-2" role="tab" aria-selected="true" value="裝置認證申請方式" />
	                        <input type="button" class="nav-item ttb-sm-btn" id="nav-trans-3-tab" data-toggle="tab" href="#nav-trans-3" role="tab" aria-selected="true" value="裝置認證使用說明" />
	                    </nav>
	                    <div class="col-12 tab-content" id="nav-tabContent">
	                    	<!-- 裝置認證的好處 -->
	                    	<div class="ttb-input-block tab-pane fade show active" id="nav-trans-1" role="tabpanel" aria-labelledby="nav-home-tab">
                            	<img style="width: 100%;text-align: center;" src="${__ctx}/img/idgate_more.png">
	                        </div>
	                        <!-- 裝置認證申請方式 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-2" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">裝置認證申請方式</h3>
	                                    <div class="application-block">
											<ul class="application-list">
												<li>
													一、網路銀行申請
												</li>
                                                <span>
                                                    <ul>
                                                        <li>
                                                            1、登入「臺企個人網路銀行」，點選「線上服務專區」。
                                                        </li>
                                                        <li>
                                                            2、選擇「裝置認證申請」，請連結晶片讀卡機至電腦並插入晶片卡。
                                                        </li>
                                                        <li>
                                                            3、成功申請啟用碼後，登入「新版行動銀行」於「更多」服務中選擇「快速登入與安全快速交易設定」後進行驗證設定。
                                                        </li>
                                                    </ul>
                                                </span>
												<li>
													二、行動銀行申請(以「隨護神盾」密碼)
												</li>
												<span>
													<ul>
														<li>
															1、登入「新版行動銀行」於「更多」服務中選擇「快速登入與安全快速交易設定」。
														</li>
														<li>
															2、進行「開啟安全快速交易」，點選「申請啟用碼」。
														</li>
														<li>
															3、輸入「隨護密碼」後，取得「啟用碼」後驗證。
														</li>
													</ul>
												</span>
											</ul>
										</div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- 裝置認證使用說明 -->
	                        <div class="ttb-input-block tab-pane fade" id="nav-trans-3" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <h3 class="guide-function-title">裝置認證使用說明</h3>
	                                </div>
	                                <div id="carouselGuideIndicator3" class="carousel guide-carousel slide" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <img class="d-block" src="${__ctx}/img/idgate/idgate_1.png" alt="First slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/idgate/idgate_2.png" alt="Second slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/idgate/idgate_3.png" alt="Third slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/idgate/idgate_4.png" alt="Four slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/idgate/idgate_5.png" alt="Five slide">
	                                        </div>
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/idgate/idgate_6.png" alt="Six slide">
	                                        </div>
<!-- 	                                        <div class="carousel-item"> -->
<%-- 	                                            <img class="d-block" src="${__ctx}/img/idgate/idgate_7.png" alt="Seven slide"> --%>
<!-- 	                                        </div> -->
	                                        <div class="carousel-item">
	                                            <img class="d-block" src="${__ctx}/img/idgate/idgate_8.png" alt="Eight slide">
	                                        </div>
	                                    </div>
	                                </div>
	                                <div id="carouselGuideIndicator3word" class="carousel guide-carousel guide-carousel-word" data-interval="false" data-ride="false" data-wrap="true">
	                                    <div class="carousel-inner">
	                                        <div class="carousel-item active">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟一</h5>
	                                                <p>點選欲操作之功能（本說明以轉帳交易為例）。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator3" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator3" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟二</h5>
	                                                <p>輸入轉出帳號、轉入帳號、轉帳金額等必要欄位後，按『確定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator3" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator3" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟三</h5>
	                                                <p>輸入以黃色標記之轉入帳號，交易機制請選擇『裝置推播認證』按『確定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator3" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator3" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟四</h5>
	                                                <p>出現裝置認證視窗後，請至綁定的行動裝置端接收推播訊息。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator3" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator3" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟五</h5>
	                                                <p>點選或開啟推播訊息。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator3" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator3" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟六</h5>
	                                                <p>確認交易訊息內容後，按『確定』。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator3" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator3" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                        <div class="carousel-item">
	                                            <div class="carousel-caption">
	                                                <p>操作步驟</p>
	                                                <h5>步驟七</h5>
	                                                <p>裝置認證成功後，顯示交易結果。</p>
	                                                <span class="input-block">
	                                                    <div class="ttb-input">
	                                                        <input type="button" class="guide-btn btn-flat-gray" href="#carouselGuideIndicator3" role="button" data-slide="prev" value="上一步" />
	                                                        <input type="button" class="guide-btn btn-flat-orange" href="#carouselGuideIndicator3" role="button" data-slide="next" value="下一步" />
	                                                    </div>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <input type="BUTTON" class="ttb-button btn-flat-gray" value="關閉" name="CMBACK" id="CMBACK">
	                    </div>
	                </div>
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
	<script type="text/javascript">
		//步驟說明變換
		var carousel5 = $('#carouselGuideIndicator3').carousel();
		var carousel6= $('#carouselGuideIndicator3word').carousel();
		carousel5.on('slide.bs.carousel', function(event) {
		    var to = $(event.relatedTarget).index();
		    carousel6.carousel(to);
		});
	</script>
</body>
</html>
