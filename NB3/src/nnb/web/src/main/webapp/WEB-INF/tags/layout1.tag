<%@ tag description="Page Layout template" language="java" pageEncoding="UTF-8"%>
<%@ tag trimDirectiveWhitespaces="true" %>

<%@ attribute name="__header" fragment="true" %>
<%@ attribute name="__nav" fragment="true" %>
<%@ attribute name="__footer" fragment="true" %>

<%@ attribute name="__title" required="true" %>
<%@ attribute name="__resPath" required="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
    	<meta http-equiv="Pragma" content="no-cache" />
    	<meta http-equiv="Cache-Control" content="no-cache" />
		<meta http-equiv="Expires" CONTENT="-1">		
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta name="description" content="Site Administration Login Page" />
        <meta name="keywords" content="login, form, input, submit, button, html5, placeholder" />
        <title>${__title}</title>
        <link rel="shortcut icon" href="${__resPath}/dist/img/favicon.png" />
    	<!-- Bootstrap 3.3.2 -->
    	<link type="text/css" rel="stylesheet" href="${__resPath}/bootstrap/css/bootstrap.min.css"/>
        <link type="text/css" rel="stylesheet" href="${__resPath}/plugins/font-awesome/font-awesome.min.css">
        <style>
        </style>
 
         
    </head>
    <body>
    	<div class="container">
    	
			<jsp:invoke fragment="__nav"/> 		
    	    	
			<jsp:invoke fragment="__header"/> 		
    		
    		<jsp:doBody/>
	    		    	
	    	<jsp:invoke fragment="__footer"/>
    
    	</div> <!-- container -->
    	
    	<!-- jQuery 2.1.3 -->
    	<script type="text/javascript" src="${__resPath}/plugins/jQuery/jQuery-2.1.3.min.js"></script>
    	<!-- Bootstrap 3.3.2 JS -->
    	<script type="text/javascript" src="${__resPath}/bootstrap/js/bootstrap.min.js"></script>
    	<script type="text/javascript">
    	</script>
    	
    </body>
    
</html>

