<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript">
	// 初始化
	$(document).ready(function() {
	    init();
		$('#DPSVACNO option[value=${renewal_apply_step1.data.fgselectMap.TSFACN}]').prop("selected", true);//預設選取
// 		$("#DPSVACNO").val('${renewal_apply_step1.data.fgselectMap.TSFACN}')
		if('${renewal_apply_step1.data.fgselectMap.AUTXFTM}' != '999' && '${renewal_apply_step1.data.fgselectMap.AUTXFTM}' !='<spring:message code="LB.Unlimited" />'){
			console.log("get daaaaataaa>>>"+'${renewal_apply_step1.data.fgselectMap.AUTXFTM}');
			var times = '${renewal_apply_step1.data.fgselectMap.AUTXFTM}';
			times = times.replace(/\s+/g, "");
			if(times != '0'){
				if(times < 10){
					times = "0" + times;
				}
				$("#DPRENCNT2").click();
				$("#DPRENCNT").val(times);
			}
			else{
				$("#DPRENCNT3").click();
			}
		}
	});

	function init() {
		var type = '${renewal_apply_step1.data.fgselectMap.TYPE}';
		createDprencnt();
		fsdpEvent(type);
		fgrencntEvent(type);
		changeFgsvType();
		$(".hideblock").hide();
	    $("#formId").validationEngine({binded: false,promptPosition: "inline"});
		//上一頁按鈕
		$("#CMBACK").click(function() {
			var action = '${__ctx}/NT/ACCT/TDEPOSIT/renewal_apply';
			$('#back').val("Y");
			$("form").attr("action", action);
			$("#formId").validationEngine('detach');
			initBlockUI();
			$("form").submit();
		});
		//確認
	    $("#CMSUBMIT").click(function() {
	        console.log("submit~~");
	        if (!$('#formId').validationEngine('validate')) {
	            e.preventDefault();
	        } else {
	            $("#formId").validationEngine('detach');
	            initBlockUI(); //遮罩
	            $("#formId").submit();
	        }
	     
	    });
	}
	//創建下拉選單
	function createDprencnt(){
	    //宣告變數，資料類型為物件 
	    var data1 = {}
	    var data2 = {}
	    var options = {selectID: '#DPRENCNT'}
	    //
	    for (i = 1; i <= 9; i++) {
	        data1['0' + i] = i;
	    }
	    //
	    for (i = 10; i <= 36; i++) {
	        data2[i] = i;
	    }
	    fstop.creatSelect(data1, options);
	    fstop.creatSelect(data2, options);
	}
	
	//(3) 綜存定存限整存整付可選擇本金利息併轉。
	function fsdpEvent(type){
	    //if 存款種類為3	 
	    if ('3' == type) {
	        $("#SVTYPE").show();
	    }
	}
	// 	轉期次數
	// 	1– 選取無限次數，則轉存方式顯示可選 『本金轉期，利息轉入帳號』及 	『本金及利息一併轉期』
	// 	2– 選取有限次數，則轉存方式顯示僅可選 『本金轉期，利息轉入帳號』
	// 	3– 選取消自動轉期，則轉存方式隱藏。
	// 	(1) 信息顯示
	// 	自動轉期時，顯示 請確認自動轉期資料
	// 	取消自動轉期，顯示 請確認取消自動轉期資料
	function fgrencntEvent(type) {
	    $('input[type=radio][name=FGRENCNT]').change(function() {
	        //(3) 綜存定存限整存整付可選擇本金利息併轉。
	        if ('3' == type) {
	            if (this.value == '1') {
	                $("#SVTYPE").show();
	                $("#SVTYPEALL").show();
	                $("#DPRENCNT").hide();
	            } else if (this.value == '2') {
	                $("#SVTYPEALL").show();
	                $("#SVTYPE").hide();
	                $("#DPRENCNT").show();
	            } else if (this.value == '3') {
	            	$("#SVTYPEALL").hide();
	            	$("#DPRENCNT").hide();
	            }
	        } else {
	            if (this.value == '3') {
	            	$("#SVTYPEALL").hide();
	            	$("#DPRENCNT").hide();
	            } else {
	            	$("#SVTYPEALL").show();
	            	$("#DPRENCNT").show();
	            }
	        }
	    });
	}
	    //change 事件
	function changeFgsvType(){
		$('input[type=radio][name=FGSVTYPE]').change(function(){
			console.log(this.value);
			if(this.value=='1'){
				$("#DPSVACNO").addClass("validate[required]")
			}else if(this.value=='2'){
				$("#DPSVACNO").removeClass("validate[required]");
			}
		});
	}
	
	//重新輸入
 	function formReset() {
 		if ($('#actionBar').val()=="reEnter"){
 			$('#actionBar').val("");
	 		document.getElementById("formId").reset();
 		}
	}
	</script>

</head>

<body>
	<!--   IDGATE -->
	<%@ include file="../idgate_tran/idgateAlert.jsp" %>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 定存服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Current_Deposit_To_Time_Deposit" /></li>
    <!-- 定存自動轉期申請/變更     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.NTD_Time_Deposit_Automatic_Rollover_Application_Change" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>

	<!-- 		主頁內容  -->
	<main class="col-12">
		<section id="main-content" class="container">
			<h2>
				<!--= 臺幣定存自動轉期申請/變更 -->
				<spring:message code="LB.NTD_Time_Deposit_Automatic_Rollover_Application_Change" />
			</h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<!-- 下拉式選單-->
			<div class="print-block no-l-display-btn">
				<select class="minimal" id="actionBar" onchange="formReset()">
					<option value=""><spring:message code="LB.Execution_option" /></option>
					<!-- 重新輸入-->
					<option value="reEnter"><spring:message code="LB.Re_enter"/></option>
				</select>
			</div>
			<form id="formId" method="post" action="${__ctx}/NT/ACCT/TDEPOSIT/renewal_apply_confirm">
			<input type="hidden" id="back" name="back" value="">
				<c:set var="BaseResultData" value="${renewal_apply_step1.data }"></c:set>
				<input type="hidden" name="REQPARAMJSON" value='${renewal_apply_step1.data.fgselctJson}' />
					<!--交易步驟 -->
				<div id="step-bar">
					<ul>
				<!--輸入資料 -->
						<li class="active"><spring:message code="LB.Enter_data"/></li>
				<!-- 確認資料 -->
						<li class=""><spring:message code="LB.Confirm_data"/></li>
				<!-- 交易完成 -->
						<li class=""><spring:message code="LB.Transaction_complete"/></li>
					</ul>
				</div>
				<!-- 表單顯示區  -->
				<div class="main-content-block row">
					<div class="col-12">
						<div class="ttb-input-block">
							<!--   存單帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Account_no" /></h4>
									</label>
								</span>
								<span class="input-block">
									${BaseResultData.fgselectMap.ACN }
								</span>
							</div>
							<!-- 存單號碼 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Certificate_no" /></h4>
									</label>
								</span>
								<span class="input-block">
									${BaseResultData.fgselectMap.FDPNUM }
								</span>
							</div>
							<!-- 			轉期次數 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<spring:message code="LB.Number_of_rotations" />
									</label>
								</span>
								<span class="input-block">
									<!--        無限次數 -->
									<div class="ttb-input">
										<label class="radio-block">
											<input type="radio" name="FGRENCNT" id="DPRENCNT1" value="1" checked="checked" onclick="$('#DPRENCNT').hide()" />
											<spring:message code="LB.Unlimited_number_of_times" />
											<span class="ttb-radio"></span>
										</label>
									</div>
									<!-- 	         有限次數 -->
									<span class="input-block">
										<div class="ttb-input">
											<label class="radio-block">
												<input type="radio" name="FGRENCNT" id="DPRENCNT2" value="2" onclick="$('#DPRENCNT').show()"/>
												<spring:message code="LB.Limited_number_of_times" />：
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<select name="DPRENCNT" id="DPRENCNT"  class="custom-select select-input half-input validate[required] hideblock"/>
											</select>
										</div>
									<!--   取消自動轉期 -->
									<div class="ttb-input">
										<label class="radio-block">
											<input type="radio" name="FGRENCNT" id="DPRENCNT3" value="3" />
											<spring:message code="LB.Termination_of_the_automatic_renewal" />
											<span class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>
							<!-- 			續存方式 -->
							<div id="SVTYPEALL" class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Renew_method" /></h4>
									</label>
								</span>
								<span class="input-block">
									<!--本金轉期，利息轉入帳號 -->
									<div class="ttb-input">
										<label class="radio-block">
<!-- 											本金轉期，利息轉入帳號 -->
											<spring:message code="LB.Principal_rollover_interest_to_account"/>
											<input type="radio" name="FGSVTYPE" id="DPSVTYPE1" value="1" checked="checked" />
											<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
										<!--  請選擇帳號 -->
											<select name="DPSVACNO" id="DPSVACNO" class="custom-select select-input half-input">
												<option value="${renewal_apply_step1.data.fgselectMap.TSFACN}">${renewal_apply_step1.data.fgselectMap.TSFACN}</option>
											</select>
									</div>
									<!--本金及利息一併轉期 -->
									<div id="SVTYPE" class="ttb-input" style="display: none;">
										<label class="radio-block">
											<input type="radio" name="FGSVTYPE" id="DPSVTYPE2" value="2" />
											<spring:message code="LB.Renew_principal_and_interest" />
											<span class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>
						</div>
							<!-- button  -->
							<!--回上頁 -->
							<spring:message code="LB.Back_to_previous_page" var="cmback"></spring:message>
							<input type="button" name="CMBACK" id="CMBACK" value="${cmback}" class="ttb-button btn-flat-gray">
							<!--重新輸入 -->
							<spring:message code="LB.Re_enter" var="cmRest"></spring:message>
							<input type="reset" name="CMRESET" id="CMRESET" value="${cmRest}" class="ttb-button btn-flat-gray no-l-disappear-btn">
							<!-- 確定 -->
							<spring:message code="LB.Confirm" var="cmSubmit"></spring:message>
							<input type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" class="ttb-button btn-flat-orange">
							<!--button 區域 -->
					</div>
				</div>
				<div class="text-left">
					<!-- 		說明： -->
					<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page"/></p>
<!-- 						<li>限本人<span class="style1">已到期</span>的定期存款、存本取息、整存整付儲蓄存款實體定存單，辦理到期續存。 </li> -->
						<li><span><spring:message code="LB.Renewal_apply_P2_D1"/></span></li>
						<li><span><spring:message code="LB.Renewal_apply_P2_D2"/></span></li>
						<li><span><spring:message code="LB.Renewal_apply_P2_D3"/></span></li>
						<li><span><spring:message code="LB.Renewal_apply_P2_D4"/></span></li>
					</ol>
				</div>
			</form>
		</section>
		<!-- main-content END -->
	</main>
		</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>