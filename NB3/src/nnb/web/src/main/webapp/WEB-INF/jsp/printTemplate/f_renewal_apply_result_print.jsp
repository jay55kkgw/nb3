<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
  <title>${jspTitle}</title>
  <script type="text/javascript">
    $(document).ready(function () {
      window.print();
    });
  </script>
</head>

<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
  <br /><br />
  <div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif" /></div>
  <br /><br />
  <div style="text-align:center">
    <font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
  </div>
  <br/>
  <div style="text-align:center">
    ${MsgName}
  </div>
  <br />
     <table class="print">
    <!-- 交易時間 -->
    <tr>
      <td style="width:8em">
<!--         交易時間 -->
                      <spring:message code="LB.Trading_time" />
      </td>
      <td>${CMQTIME} </td>
    </tr>
    <!-- 存單帳號 -->
    <tr>
      <td class="ColorCell">
<!--         存單帳號 -->
                <spring:message code="LB.Account_no" />
      </td>
      <td>${FYACN}</td>
    </tr>
    <!-- 存單號碼 -->
    <tr>
      <td class="ColorCell">
<!--         存單號碼 -->
                      <spring:message code="LB.Certificate_no" />
      </td>
      <td> ${FDPNUM}</td>
    </tr>
    <!-- 存單金額 -->
    <tr>
      <td class="ColorCell">
<!--         存單金額 -->
                       <spring:message code="LB.Certificate_amount" />
      </td>
      <td>
          ${CRY}
          <fmt:formatNumber type="number" minFractionDigits="2" value="${AMTFDP}" />
<!--           元 -->
          									<spring:message code="LB.Dollar" />       
      </td>
    </tr>
    <!-- 存單種類 -->
    <tr>
      <td class="ColorCell">
<!--         存單種類 -->
        <spring:message code="LB.Deposit_certificate_type" />
      </td>
      <td>
        ${DEPTYPE}
      </td>
    </tr>
    <!-- 計息方式 -->
    <tr>
      <td class="ColorCell">
<!--         計息方式 -->
        <spring:message code="LB.Interest_calculation" />
      </td>
      <td>
        ${INTMTH}
      </td>
    </tr>
    <!-- 轉期次數 -->
    <tr>
      <td class="ColorCell">
<!--         轉期次數 -->
        <spring:message code="LB.Number_of_rotations" />
      </td>
      <td>
        ${AUTXFTM}
      </td>
    </tr>
    <!-- 轉存方式 -->
    <tr>
      <td class="ColorCell">
<!--         轉存方式 -->
        <spring:message code="LB.Rollover_method" />
      </td>
      <td>
          <!--轉存方式 -->
          ${CODE}
          <!--利息轉入帳號 -->
          ${FYTSFAN}
      </td>
    </tr>
  </table>
  <br>
  <br>
  <div class="text-left">
    <spring:message code= "LB.Description_of_page" />
    <ol class="list-decimal text-left">
      <li><spring:message code= "LB.F_renewal_apply_P3_D1" /></li>
      <li><spring:message code= "LB.F_renewal_apply_P3_D2" /></li>
    </ol>
  </div>
</body>
</html>