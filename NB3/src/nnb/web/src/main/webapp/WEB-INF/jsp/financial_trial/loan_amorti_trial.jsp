<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>

<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/JavaScript">

	$(document).ready(function(){
		$("#CMSUBMIT").click(function(){
			caltype();
		});
	});

	//改變攤還方式
 	function changeSelect(){
	   if ($("#selPayment").val()==3){
	   	$("#principalGrace").val(0); 
		$("#principalGrace_Field").hide();
	   } else {
		$("#principalGrace_Field").show();
	  } 
	} 

	//檢查是否符合數值型態和範圍
	//num:  待檢查的數值
	//start:數值起始範圍
	//end:  數值結束範圍
	//type: I:Integer F:Float
	function IsNumeric(num, start, end, info, type) {
		var msg = "";

		if (type == "I") {
			var numValue = /^[0-9]+$/;
			if (!numValue.test(num)) {
				msg = "<spring:message code= "LB.X1568" />" + info + "<br>";
				return (msg);
			}
			num = parseInt(num);
			start = parseInt(start);
			end = parseInt(end);
		}

		if (type == "F") {
			num = parseFloat(num);
			start = parseFloat(start);
			end = parseFloat(end);

			if (isNaN(num)) {
				msg = "<spring:message code= "LB.X1568" />" + info + "<br>";
				return (msg);
			}
		}

		if (num<start || num>end) {
			msg = info + "<spring:message code= "LB.X1379" />" + start + "<spring:message code= "LB.X1380" />" + end + "<spring:message code= "LB.X1381" />\n"
		}
		return (msg);
	}
	

	//決定攤還方式(本息定額攤還/本金定額攤還/到期一次還本)
	function caltype(){
	  if ($("#yrate2").val()==""){
		  $("#yrate2").val(0);
	  }	  
	  if ($("#mperiod2").val()==""){
		  $("#mperiod2").val(0);
	  }	  
	  if ($("#yrate3").val()==""){
		  $("#yrate3").val(0);
	  }	  
	  if ($("#mperiod3").val()==""){
		  $("#mperiod3").val(0);
	  }
	  //檢查各數值資料
	  var msg = "";
	  //本金
	  msg=msg + IsNumeric($("#loan").val(),10000,100000000,"<spring:message code= "LB.Principal" />","I");
	  //<!-- 寬限期(月) -->
	  msg=msg + IsNumeric($("#principalGrace").val(),0,60,"<spring:message code= "LB.X0107" />","I");
	  msg=msg + IsNumeric($("#yrate1").val(),0,20,"<spring:message code= "LB.X0109" /><spring:message code= "LB.X0091" />","F");
	  msg=msg + IsNumeric($("#mperiod1").val(),1,360,"<spring:message code= "LB.X0109" /><spring:message code= "LB.X0110" />","I");
	  msg=msg + IsNumeric($("#yrate2").val(),0,20,"<spring:message code= "LB.X0111" /><spring:message code= "LB.X0091" />","F");
	  msg=msg + IsNumeric($("#mperiod2").val(),0,360,"<spring:message code= "LB.X0111" /><spring:message code= "LB.X0110" />","I");
	  msg=msg + IsNumeric($("#yrate3").val(),0,20,"<spring:message code= "LB.X0112" /><spring:message code= "LB.X0091" />","F");
	  msg=msg + IsNumeric($("#mperiod3").val(),0,360,"<spring:message code= "LB.X0112" /><spring:message code= "LB.X0110" />","I");
	  msg=msg + IsNumeric($("#otherfee").val(),0,200000,"<spring:message code= "LB.X0113" />","I");
	  msg=msg + IsNumeric(parseInt($("#mperiod1").val()) + parseInt($("#mperiod2").val() ) + parseInt($("#mperiod3").val()),1,360,"<spring:message code= "LB.X1382" />","I");

	  if ((parseInt($("#mperiod1").val()) + parseInt($("#mperiod2").val()) + parseInt($("#mperiod3").val()) - parseInt($("#principalGrace").val())) <= 0){
	    msg=msg + "<spring:message code= "LB.X1383" />";
	  }

	  //沒有錯誤訊息，執行計算
	  if (String(msg).length>0){
	    //alert(msg);
	    errorBlock(
							null, 
							null,
							[msg], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
	    return false;
	  } else {
		  $("#formId").submit();
	 }
	  return false;
	}

</script>
</head>
 <body>
	<!-- header -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 理財試算服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0087" /></li>
    <!-- 貸款攤還試算     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0102" /></li>
		</ol>
	</nav>


	<!-- 快速選單及主頁內容 -->
	<!-- menu、登出窗格 --> 
	<div class="content row">
		<%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
		<main class="col-12">	
			<!--主頁內容  -->
		<section id="main-content" class="container">
			<!--<h2><spring:message code="LB.Change_User_Name" /></h2>--> 
			<h2><spring:message code="LB.X0102" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>						
			<form method="post"  id="formId"  action="${__ctx}/FINANCIAL/TRIAL/loan_amorti_trial_result">
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
						        <!-- 攤還方式-->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
										<h4><spring:message code="LB.X0103" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<select id="selPayment"  name="selPayment"  class="custom-select select-input half-input" onChange=changeSelect()>
											<option value="1">
												<spring:message code="LB.X0104" />
											</option>
											<option value="2">
												<spring:message code="LB.X0105" />
											</option>
											<option value="3">
												<spring:message code="LB.X0106" />
											</option>
										</select>
									</div>
								</span>
							</div>
							<!-- 貸款金額-->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
										<!-- <h4><spring:message code="LB.User_name" /></h4> -->
										<h4><spring:message code="LB.D0660" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text"  name = "loan"  id = "loan"  maxlength="18"  size="20"  value="0"  class="text-input">
										<span class="input-unit"><spring:message code="LB.Dollar" /></span>
									</div>
								</span>
							</div>
							<!-- 寬限期(月)-->
							<div class="ttb-input-item row"  id="principalGrace_Field"  >
								<span class="input-title"> 
								<label>
										<!-- <h4><spring:message code="LB.User_name" /></h4> -->
										<h4><spring:message code="LB.X0107" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text"  name = "principalGrace"  id = "principalGrace"  maxlength="18"  size="20"  value="0"  class="text-input">
										<span class="input-unit"><spring:message code="LB.Month" /></span>
									</div>
								</span>
							</div>
							<!-- 貸款利率 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<!-- <h4><spring:message code="LB.User_name" /></h4> -->
										<h4><spring:message code="LB.X0108" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">
										<span><spring:message code="LB.X0109" /></span>
									</div>
									<div class="ttb-input">
										<!-- 請輸入利率 -->
										<input name="yrate1" id="yrate1" type="text" size="6" maxlength="6" value="" placeholder="<spring:message code='LB.X2064' />" class="text-input input-width-85">
										<span class="input-unit margin-4">%</span>
										<!-- 請輸入計息期間 -->
										<input name="mperiod1"  id="mperiod1"  type="text" size="10" maxlength="6" value="" placeholder="<spring:message code='LB.X2065' />" class="text-input input-width-115">
										<span class="input-unit"><spring:message code="LB.Months" /></span>
									</div>
									<div class="ttb-input">
										<span><spring:message code="LB.X0111" /></span>
									</div>
									<div class="ttb-input"> 
										<!-- 請輸入利率 -->
										<input name="yrate2" id="yrate2" type="text" size="6" maxlength="6" value="" placeholder="<spring:message code='LB.X2064' />" class="text-input input-width-85">
										<span class="input-unit margin-4">%</span>
										<!-- 請輸入計息期間 -->
										<input name="mperiod2"  id="mperiod2"  type="text" size="10" maxlength="6" value="" placeholder="<spring:message code='LB.X2065' />" class="text-input input-width-115">
										<span class="input-unit"><spring:message code="LB.Months" /></span>
									</div>
									<div class="ttb-input">
										<span><spring:message code="LB.X0112" /></span>
									</div>
									<div class="ttb-input">
										<!-- 請輸入利率 -->
										<input name="yrate3" id="yrate3" type="text" size="6" maxlength="6" value="" placeholder="<spring:message code='LB.X2064' />" class="text-input input-width-85">
										<span class="input-unit margin-4">%</span>
										<!-- 請輸入計息期間 -->
										<input name="mperiod3"  id="mperiod3" type="text" size="10" maxlength="6" value="" placeholder="<spring:message code='LB.X2065' />" class="text-input input-width-115">
										<span class="input-unit"><spring:message code="LB.Months" /></span>
									</div>
								</span>	
							</div>
							<!--各項相關費用總金額-->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
										<!-- <h4><spring:message code="LB.User_name" /></h4> -->
										<h4><spring:message code="LB.X0113" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text"  name = "otherfee"  id = "otherfee"   size="20"  class="text-input"  value="0">
										<span class="input-unit"><spring:message code="LB.Dollar" /></span>
									</div>
								</span>
							</div>
						</div>
						<!-- <input id="pageshow" name="pageshow" type="submit" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" /> -->
						<input id="btnReset" name="btnReset" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.X0114" />" />						
						<input id="CMSUBMIT" name="CMSUBMIT" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.X0115" />"/>
					</div>
				</div>
				</form>
						<ol class="list-decimal description-list">
							<!--<spring:message code="LB.Username_alter_P1_D1" />-->
							<p><spring:message code="LB.Description_of_page" /></p>
							<spring:message code="LB.Loan_Amorti_Trial_P1_D1" />
						</ol>
		</section>
		<!-- 		main-content END -->
	</main>
	</div>
	<!-- content row END -->
    <%@ include file="../index/footer.jsp"%>
</body>
</html>
