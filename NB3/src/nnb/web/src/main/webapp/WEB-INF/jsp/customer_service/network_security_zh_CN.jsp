<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
</head>
<script type="text/javascript">
	$(document).ready(function(){
		// HTML载入完成后开始遮罩
		setTimeout("initBlockUI()", 10);
		// 开始查询资料并完成画面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	function init(){
		$("#CMBACK").click(function(e){
			$("#formId").attr("action","${__ctx}/login");
			$("#formId").submit();
		});
	}
</script>
<body>
   	<!-- header -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	<!-- 面包屑 -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><a href="#">网路安全</a></li>
		</ol>
	</nav>
	
	<!-- 左边menu 及登入资讯 -->
	<div class="content row">
		<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主页内容  -->
				<h2>
					网路安全
				</h2>
				<form id="formId" method="post" action="">
					<div class="card-block shadow-box terms-pup-blcok">
	                    <div class="col-12 tab-content">
	                        <div class="ttb-input-block tab-pane fade show active" id="nav-trans-1" role="tabpanel" aria-labelledby="nav-home-tab">
	                            <div class="card-select-block">
	                            </div>
	                            <div class="card-detail-block">
	                                <div class="card-center-block">
	                                    <!-- Q1 -->
	                                    <div class="ttb-pup-block" role="tab">
	                                        <a role="button" class="ttb-pup-title d-block" data-toggle="collapse" href="#popup-1" aria-expanded="true" aria-controls="popup-1">
	                                            <div class="row">
	                                                <span class="col-1">Q1</span>
	                                                <div class="col-10 row">
	                                                    <span class="col-12">什么是伪冒网站？</span>
	                                                </div>
	                                            </div>
	                                        </a>
	                                        <div id="popup-1" class="ttb-pup-collapse collapse show" role="tabpanel">
	                                            <div class="ttb-pup-body">
	                                                <ul class="ttb-pup-list">
	                                                    <li>
	                                                        <p>诈骗集团伪造与知名网站一模一样的假网站，骗取客户进入这些网站，输入个人私密资料(如身份证字号、帐号、信用卡号、密码等)，借以盗取个人资料，非法使用，损害客户权益。</p>
	                                                    </li>
	                                                    <li>
	                                                        <p>因应措施</p>
	                                                        <ol style="list-style-type: circle; margin-left: 1rem;">
	                                                            <li><p>台湾企银网路银行网址 <a href="https://ebank.tbb.com.tw">https://ebank.tbb.com.tw</a> ，登入前请逐字核对网址确保进入正确的网站。
	                                                                <a onclick="window.open('${__ctx}/img/newsample1.png','','menubar=no,status=no,scrollbars=yes,top=20,left=50,toolbar=no,width=800,height=600')" href="#">(图示说明)</a></p>
	                                                            </li>
	                                                            <li>
	                                                                <p>请检查电脑萤幕右下角显示安全锁标志
	                                                                <a onclick="window.open('https://portal.tbb.com.tw/tbbportal/fstop/images/sample2.gif','','menubar=no,status=no,scrollbars=yes,top=20,left=50,toolbar=no,width=800,height=600')" href="#">(图示说明)</a>
	                                                               		 点选安全锁即显示凭证资讯。
	                                                                <a onclick="window.open('https://portal.tbb.com.tw/tbbportal/fstop/images/sample3.gif','','menubar=no,status=no,scrollbars=yes,top=20,left=50,toolbar=no,width=800,height=600')" href="#">(图示说明)</a></p>
	                                                            </li>
	                                                            <li><p>请勿点选来路不明可疑电子邮件内的超链接。</p></li>
	                                                            <li><p>请定期变更网路银行密码，以保护个人私密资料。</p></li>
	                                                        </ol>
	                                                    </li>
	                                                </ul>
	                                            </div>
	                                        </div>
	                                    </div>
	                                    <!-- Q2 -->
	                                    <div class="ttb-pup-block" role="tab">
	                                        <a role="button" class="ttb-pup-title d-block" data-toggle="collapse" href="#popup-2" aria-expanded="true" aria-controls="popup-2">
	                                            <div class="row">
	                                                <span class="col-1">Q2.</span>
	                                                <div class="col-10 row">
	                                                    <span class="col-12">什么是伪冒电邮？</span>
	                                                </div>
	                                            </div>
	                                        </a>
	                                        <div id="popup-2" class="ttb-pup-collapse collapse show" role="tabpanel">
	                                            <div class="ttb-pup-body">
	                                                <ul class="ttb-pup-list">
	                                                    <li>
	                                                        <p>诈骗集团假冒银行或线上服务业者名义通知客户资料过期、无效需要更新，或者是基於安全理由进行身分验证，
										                                                            要求客户输入个人私密资料（如身分证字号、帐号、信用卡号、密码等）。客户如一时不察经由电子邮件指引的网址，
										                                                            链接至伪冒网站输入个人资料，轻则成为垃圾邮件业者的名单，重则电脑可能被植入木马程式，破坏系统或个人私密资料遭窃。</p>
	                                                    </li>
	                                                    <li>
	                                                        <p>因应措施</p>
	                                                        <ol style="list-style-type: circle; margin-left: 1rem;">
	                                                            <li><p>请勿理会来路不明可疑的电子邮件，且勿点选所提供的超链接，并立即删除。</p>
	                                                            </li>
	                                                            <li>
	                                                                <p>请勿回覆要求提供个人机密资料的可疑电子邮件。</p>
	                                                            </li>
	                                                            <li><p class="text-danger">请注意！台湾企银绝对不会经由电子邮件，要求客户输入个人机密资料。 如收到此类可疑电子邮件，请立即通知台湾企银服务专线：0800-00-7171。请安装及定期更新您的防毒软体。</p></li>
	                                                        </ol>
	                                                    </li>
	                                                </ul>
	                                            </div>
	                                        </div>
	                                    </div>
	
	                                </div>
	                            </div>
	                        </div>
	                        <input type="BUTTON" class="ttb-button btn-flat-gray" value="返回" name="CMBACK" id="CMBACK">
	                    </div>
	                </div>
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
	
</body>
</html>
