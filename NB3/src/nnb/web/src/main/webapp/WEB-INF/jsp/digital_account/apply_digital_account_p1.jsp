<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>

	<script type="text/javascript">
		$(document).ready(function () {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 10);
			// 開始查詢資料並完成畫面
			setTimeout("init()", 20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
		});
		
		// 畫面初始化
		function init() {
			$("#CMSUBMIT").click(function(e) {
				console.log("submit~~");
	
	         	initBlockUI();
	            $("#formId").submit();
			});
			$("#CMBACK").click( function(e) {
				console.log("submit~~");
				$("#formId").attr("action", "${__ctx}/ONLINE/APPLY/online_apply");
				$("#back").val('Y');
	
	         	initBlockUI();
	            $("#formId").submit();
			});
		}

        function checkReadFlag()
        {
        	console.log($("#ReadFlag").prop('checked') && $("#ReadFlag1").prop('checked'));
            if ($("#ReadFlag").prop('checked') && $("#ReadFlag1").prop('checked'))    		
          	{
           		$("#CMSUBMIT").prop('disabled',false);
           		$("#CMSUBMIT").addClass("btn-flat-orange");
          	}
          	else
          	{
           		$("#CMSUBMIT").prop('disabled',true);
           		$("#CMSUBMIT").removeClass("btn-flat-orange");
       	  	}	
        }
		function openMenu() {
			var main = document.getElementById("main");
			window.open('${__ctx}/public/OpenAccount.pdf');		   
		}
		
	</script>
	<style>
			.ttb-button:disabled {
				background: #ddd;
			}
		@media screen and (max-width:767px) {
			.ttb-button {
				width: 38%;
				left: 36px;
			}
			#CMBACK.ttb-button{
				border-color: transparent;
				box-shadow: none;
				color: #333;
				background-color: #fff;
			}
 

		}
	</style>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上開立數位存款帳戶     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1361" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!--基金線上預約開戶作業 -->
<!-- 					線上開立數位存款帳戶 -->
					<spring:message code="LB.D1361"/>
				</h2>
                <div id="step-bar">
                    <ul>
                        <li class="active">注意事項與權益</li>
                        <li class="">開戶認證</li>
                        <li class="">開戶資料</li>
                        <li class="">確認資料</li>
                        <li class="">完成申請</li>
                    </ul>
                </div>
				<form method="post" id="formId" name="formId" action="${__ctx}/DIGITAL/ACCOUNT/apply_digital_account_p2">
					<input type="hidden" name="back" id="back" value=>	
					<input type="hidden" name="TYPE" value="10">
					<input type="hidden" name="outside_source" value="${ outside_source }">
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12 terms-block">
                       		<div class="ttb-message">
							    <p><spring:message code="LB.D1102"/></p>
							</div>
                        	<p class="form-description">請您審閱以下顧客權益與說明。</p>
	                        <div class="text-left">
	
	                            <ul class="ttb-result-list terms">
                                	<li data-num="" style="overflow-x: auto">
                                		<span class="input-subtitle subtitle-color"><spring:message code="LB.X0163"/></span>
                                		<div class="CN19-clause" style="width: 100%; height: 210px; margin: 20px auto; border: 1px solid #D7D7D7; border-radius: 6px;">
	           								<%@ include file="../term/N201_1.jsp"%>
                                		</div>
                                	</li>
                                	<li data-num="">
                                		<span class="input-subtitle subtitle-color"><spring:message code="LB.X0199"/></span>
                                		<div class="CN19-clause" style="width: 100%; height: 210px; margin: 20px auto; border: 1px solid #D7D7D7; border-radius: 6px;">
	           								<%@ include file="../term/N203.jsp"%>
                                		</div>
                                	</li>
                                	<li data-num="" style="overflow-x: auto">
                                		<span class="input-subtitle subtitle-color"><spring:message code="LB.X0166"/></span>
                                		<div class="CN19-clause" style="width: 100%; height: 210px; margin: 20px auto; border: 1px solid #D7D7D7; border-radius: 6px;">
	           								<%@ include file="../term/OpenAccountD.jsp"%>
                                		</div>
                                	</li>
                                	<li data-num="">
                                		<span class="input-subtitle subtitle-color"><spring:message code="LB.X0164"/></span>
                                		<div class="CN19-clause" style="width: 100%; height: 210px; margin: 20px auto; border: 1px solid #D7D7D7; border-radius: 6px;">
	           								<%@ include file="../term/N201_2.jsp"%>
                                		</div>
                                	</li>
                                	<li data-num="">
                                		<span class="input-subtitle subtitle-color"><spring:message code="LB.X0202"/></span>
                                		<div class="CN19-clause" style="width: 100%; height: 210px; margin: 20px auto; border: 1px solid #D7D7D7; border-radius: 6px;">
	           								<%@ include file="../term/FATCA.jsp"%>
                                		</div>
                                	</li>
                            	</ul>
                            <span class="input-block">
                                <div class="ttb-input">
									<label class="check-block" for="ReadFlag1">
		<!-- 							本人聲明非屬美國納稅義務人 -->
										<input type="checkbox" name="ReadFlag1" id="ReadFlag1" onclick="checkReadFlag()">
										<spring:message code="LB.D1063"/>
		                                <span class="ttb-check"></span>
									</label><br><br>
									<label class="check-block" for="ReadFlag">
		<!-- 							本人已詳細閱讀並清楚以上所有內容 -->
										<input type="checkbox" name="ReadFlag" id="ReadFlag" onclick="checkReadFlag()">
										<spring:message code="LB.D1064"/>
		                                <span class="ttb-check"></span>
									</label>
                                </div>
                            </span>
                        </div>

							<!-- 確定 -->
							<input type="button" id="CMBACK" value="<spring:message code="LB.D0041"/>" class="ttb-button btn-flat-gray"/>
							<input type="button" id="CMSUBMIT" value="<spring:message code="LB.D0097"/>" class="ttb-button" disabled/>
						</div>
					</div>
						
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>