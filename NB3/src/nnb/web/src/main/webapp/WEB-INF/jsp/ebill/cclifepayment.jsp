<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/component/util/cardReaderMethod.js"></script>
	<script type="text/javascript">
	// 初始化
	$(document).ready(function () {
		init();
	});

	function init() {
		//表單驗證
		$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
		// 確認鍵 click
		goOn();

	}

	// 確認鍵 click
	function goOn() {
		$("#CMSUBMIT").click(function (e) {
			
			// 20200106新增判斷: 如果本期應繳金額為0則不讓使用者繳費
			var payamt = '${cclifepayment.data.PAYAMT }';
			if ( payamt==null || payamt=='' || payamt=='0' || payamt==0 ) {
		   		errorBlock(
						null,
						null,
						['<spring:message code="LB.X2426" />'], 
						'<spring:message code= "LB.Confirm" />', 
						null
					);
		   		return false;
			} else {
				// 表單驗證
				if (!$('#formId').validationEngine('validate')) {
					e = e || window.event; // for IE
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					initBlockUI(); //遮罩
					$("#formId").submit();
				}
			}
			
		});
	}

	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	<!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<!-- 活存帳戶繳信用卡費 -->
			<li class="ttb-breadcrumb-item">
				<a href="#">
					<spring:message code="LB.X2427" />
				</a>
			</li>
			<!-- 活存帳戶繳信用卡費 -->
			<li class="ttb-breadcrumb-item active" aria-current="page">
				<spring:message code="LB.X2427" />
			</li>
		</ol>
	</nav>
	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!--活存帳戶繳信用卡費 -->
				<h2>
					<spring:message code="LB.X2427" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form autocomplete="off" method="post" id="formId" action="${__ctx}/EBILL/PAY/cclifepayment_step1">
					<input type='hidden' name='PAYAMT' value='${cclifepayment.data.PAYAMT } '>
					<input type='hidden' name='LPAYAMT' value='${cclifepayment.data.LPAYAMT }'>
					<input type='hidden' name='CARDNUM' value='${cclifepayment.data.CARDNUM }'>
					<!-- 表單顯示區 -->
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								<!--  銷帳編號: -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.W0407" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												${cclifepayment.data.CARDNUM }
											</p>
										</div>
									</span>
								</div>
								<!-- 本期應繳金額 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.This_period" /><spring:message code="LB.Repayment_of_every_month" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												NTD ${cclifepayment.data.PAYAMT } <spring:message code="LB.Dollar" />
											</p>
										</div>
									</span>
								</div>
								<!-- 最低應繳金額:-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.X2432" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<p>
												NTD ${cclifepayment.data.LPAYAMT } <spring:message code="LB.Dollar" />
											</p>
										</div>
									</span>
								</div>
								<div class="ttb-message">
									<span style='color:red;font-weight:bold' colspan='3' id='fittext7'>
										<!-- 請確認匯出匯款資料 -->
										為保障您的交易安全，若您為iPhone的使用者，請避免使用
										<img src="${__ctx}/img/QRCODEREADER.jpg" width="30" height="30" />
											「QR掃描器」此APP來執行交易。
									</span>
								</div>
								<div class="ttb-message">
									<P>使用全國性繳費(稅)業務繳納信用卡帳款注意事項</P>
									<span>本注意事項是您與臺灣中小企業銀行股份有限公司(以下稱本行)就使用全國性繳費(稅)業務繳納信用卡帳款(以下稱本服務)所成立之協議，為了保障您的權益，請於使用本服務前，詳細閱讀下列注意事項，若您對本服務尚有不瞭解或不同意注意事項之內容者，請勿執行相關交易。</span>
								</div>
								<h4>壹、 名詞說明</h4>
								<ul class="ttb-result-list terms">
									<li data-num="1.">
										<span class="input-subtitle subtitle-color">資料蒐集</span>
										<p>您使用本服務所輸入之相關資料，將由帳單業者、帳務代理、轉出、轉入金融機構及財金資訊股份有限公司在完成上述服務之特定目的內，蒐集、處理、利用及國際傳輸您的個人資料。
										</p>
									</li>
									<li data-num="2.">
										<span class="input-subtitle subtitle-color">繳費身分及範圍</span>
										<p>本服務限本行信用卡正卡持卡人本人以其本行或他行本人活期存款帳戶繳納本行本人信用卡(含附卡)帳單費用。</p>
									</li>
									<li data-num="3.">
										<span class="input-subtitle subtitle-color">繳費限額</span>
										<p>本服務繳費限額，自行轉出每筆/每日繳款金額上限為新臺幣10萬元，他行轉出則依轉出金融機構之規定辦理。</p>
									</li>
									<li data-num="4.">
										<span class="input-subtitle subtitle-color">服務收費</span>
										<p>使用本服務交易手續費:自行繳費免手續費、他行繳費每筆新臺幣10元，系統將併同繳款金額一併自您轉出帳戶扣取。</p>
									</li>
									<li data-num="5.">
										<span class="input-subtitle subtitle-color">入帳時間</span>
										<p>若您的帳款在銀行營業日下午3:30前繳納，將於當日入帳至您的信用卡帳戶；如在營業日下午3:30後或例假日繳納，則於次一營業日入帳至您的信用卡帳戶。</p>
									</li>
									<li data-num="6.">
										<span class="input-subtitle subtitle-color">免責聲明</span>
										<p>您於使用本服務時，需自行採取防護措施，本行對本網站全部或一部之正確性、完整性、可行性、即時性、相容性、無瑕疵性、無中斷性、無遺漏性及無損害性，不負任何擔保責任。您可能藉由本網站連結至第三人網站或網頁，但不表示本行與該等網站有任何關係，本行對該第三人網站及網頁內之所有內容及服務，不負任何擔保責任。
										</p>
									</li>
									<li data-num="7.">
										<span class="input-subtitle subtitle-color">交易糾紛</span>
										<p>倘您使用本服務發生疑義，請洽本行客服(0800-01-7171)或轉出金融機構處理。</p>
									</li>
									<li data-num="8.">
										<span class="input-subtitle subtitle-color">損害賠償</span>
										<p>您明確瞭解並同意，任何因使用或無法使用本服務全部或一部所造成之直接、間接、偶發及衍生之損害賠償，包括但不限於您及第三人之財產上、非財產上、身體上、生命上及其他有形無形損害等，本行不負任何損害賠償責任。
											如您違反本注意事項約定，本行除得拒絕或暫停您使用本服務之全部或一部外，您並應對本行負損害賠償責任（包括但不限於訴訟費用及律師費用）。</p>
									</li>
									<li data-num="9.">
										<span class="input-subtitle subtitle-color">準據法及管轄法院</span>
										<p>本服務之解釋與適用，適用中華民國法律。因使用本服務所生之任何爭議及糾紛，雙方同意以臺灣臺北地方法院為第一審管轄法院。</p>
									</li>
									<li data-num="10.">
										<span class="input-subtitle subtitle-color">修訂</span>
										<p>除另有約定外，本行得隨時修訂本注意事項之相關約定並於本行網站上公告。</p>
									</li>
									<li data-num="11.">
										<span class="input-subtitle subtitle-color"></span>
										<p>如有其他未盡事宜，依e-Bill全國繳費網使用聲明
											<div id='fittext7'> (網址：https://ebill.ba.org.tw/CPP/DesktopDefault.aspx)
											</div>
											及本行規定辦理。 </p>
									</li>
								</ul>
							</div>
							<!-- button -->
							<!-- 確定 -->
							<spring:message code="LB.Confirm" var="cmSubmit" />
							<input type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}"
								class="ttb-button btn-flat-orange">
							<!--buttonEND -->
						</div>
					</div>
					<div class="text-left">
						<!-- 說明： -->
					</div>
				</form>
			</section>
			<!-- main-content END -->
		</main>
	</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>