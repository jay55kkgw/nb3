<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ include file="../__import_js.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
	$(document).ready(function() {
		window.print();
	});
</script>
</head>

<body class="bodymargin watermark" style="-webkit-print-color-adjust:exact">
	<br />
	<br />
	<div style="text-align: center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br />
	<br />
	<div style="text-align: center">
		<font style="font-weight: bold; font-size: 1.2em">${jspTitle}</font>
	</div>
	<br />
	<div style="text-align: center">
		<c:if test="${APPLYFLAG =='1'}">
									<spring:message code= "LB.D0181" />
									</c:if>
		<c:if test="${APPLYFLAG =='2'}">
									<spring:message code= "LB.D0183" />
									</c:if>
		<c:if test="${APPLYFLAG =='3'}">
									 <spring:message code= "LB.Change_successful" />
								</c:if>
	</div>
	<br />
	<table class="print">
		<!-- 交易時間 -->
		<tr>
			<td style="width: 8em">
				<!--         交易時間 --> <spring:message code="LB.Trading_time" />
			</td>
			<td>${CMQTIME}</td>
		</tr>
		<!-- 扣款帳號 -->
		<c:if test="${APPLYFLAG != '2'}">
			<tr>
				<td class="ColorCell"><spring:message code= "LB.D0168" /></td>
				<td>${TSFACN}</td>
			</tr>
		</c:if>
		<!-- 申請項目 -->
		<tr>
			<td class="ColorCell"><spring:message code= "LB.D0169" /></td>
			<td><c:if test="${APPLYFLAG =='1'}">
				<spring:message code= "LB.Apply" />
				</c:if> 
			<c:if test="${APPLYFLAG =='2'}">
				<spring:message code= "LB.Cancel" />
			</c:if> 
			<c:if test="${APPLYFLAG =='3'}">
				<spring:message code= "LB.D0172" />
			</c:if>
			</td>
		</tr>
		<c:if test="${APPLYFLAG !='2'}">
		<!-- 扣繳方式 -->
		<tr>
			<td class="ColorCell">
				<spring:message code= "LB.Payment_method" />
			</td>
			<td>
				<c:if test="${display_PAYFLAG == '1'}">
				<spring:message code= "LB.D0174" />
				</c:if>
				<c:if test="${display_PAYFLAG == '2'}">
				<spring:message code= "LB.D0175" />
				</c:if>
			</td>
		</tr>
		</c:if>
	</table>
	<br>
	<br>
	<div class="text-left">
	    <spring:message code= "LB.Description_of_page" /> 
	    <ol class="list-decimal text-left">
	      <li><spring:message code= "LB.Debit_Apply_P3_D2" /></li>
	    </ol>
	  	</div>
</body>

</html>