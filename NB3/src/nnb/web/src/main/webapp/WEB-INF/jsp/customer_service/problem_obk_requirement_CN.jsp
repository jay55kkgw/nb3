<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="page4" class="card-detail-block">
	<div class="card-center-block">
		<!-- Q1 -->
		<div class="ttb-pup-block" role="tab">
			<a id="popup1-Q1" role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-41" aria-expanded="true"
				aria-controls="popup1-41">
				<div class="row">
					<span class="col-1">Q1</span>
					<div class="col-11">
						<span>使用贵银行的网络银行需要怎么样的计算机软硬件设备？</span>
					</div>
				</div>
			</a>
			<div id="popup1-41" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>建议您使用以下规格，当然计算机硬件等级越高，处理速度会越快：</p>
							<p>硬件：计算机的CPU 1GHz，2GB以上的内存。</p>
							<p>软件：</p>
							<ol>
								<li>一、Mac系统  可使用浏览器：Safari、Chrome、FireFox</li>
								<li>二、Window操作系统(Window7(含)以上)  可使用浏览器：IE 11(含)以上的版本、Chrome、FireFox、Edge</li>
							</ol>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
