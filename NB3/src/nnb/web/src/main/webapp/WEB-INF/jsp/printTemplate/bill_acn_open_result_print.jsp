<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
<br/><br/>
<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
<br/><br/><br/>
<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
<br/><br/><br/>
<table class="print">
	<!-- 								申請人 -->
	<tr>
		<td style="text-align:center"><spring:message code="LB.D0516" /></td>
		<td>${NAME}</td>
	</tr>
	<!--								  地址     -->
	<tr>
		<td style="text-align:center"><spring:message code="LB.D0517" /></td>
		<td>${ADDRESS}</td>
	</tr>
	<!-- 					           	 營業種類或職(事)業    -->	
	<tr>
		<td style="text-align:center"><spring:message code="LB.D0518" /></td>
		<td>${BUSINESS}</td>
	</tr>
	<!-- 								身分證統一編號	-->	
	<tr>
		<td style="text-align:center"><spring:message code="LB.D0519" /></td>
		<td>${CUSIDN}</td>
	</tr>
	<!--								票信查詢費指定扣帳帳號     -->
	<tr>
		<td style="text-align:center"><spring:message code="LB.D0520" /></td>
		<td>${ACN}</td>
	</tr>
	<!-- 					            生日     -->
	<tr>
		<td style="text-align:center"><spring:message code="LB.D0521" /></td>
		<td>${BRHDAY}</td>
	</tr>
	<!-- 					            國別    -->	
	<tr>
		<td style="text-align:center"><spring:message code="LB.D0522" /></td>
		<td>
			<c:choose>
				<c:when test="${transfer == 'en'}">
					${COUNTRYNAMEEN}
				</c:when>
				<c:when test="${transfer == 'zh'}">
					${COUNTRYNAMECN}(${COUNTRY})
				</c:when>
				<c:otherwise>
					${COUNTRYNAME}(${COUNTRY})
				</c:otherwise>
			</c:choose>
		</td>
	</tr>
	<!-- 					            出生地     -->		
	<tr>
		<td style="text-align:center"><spring:message code="LB.D0523" /></td>
		<td>${BIRTHPLA}</td>
	</tr>
	<!-- 					          電話號碼(公)                    -->
	<tr>
		<td style="text-align:center"><spring:message code="LB.D0524" /></td>
		<td>${TEL_O}</td>
	</tr>
	<!-- 					         電話號碼(宅)                    -->
	<tr>
		<td style="text-align:center"><spring:message code="LB.D0525" /></td>
		<td>${TEL_H}</td>
	</tr>
	<!-- 					          票信查詢費指定扣帳帳號之分行名稱                    -->
	<tr>
		<td style="text-align:center"><spring:message code="LB.X1162" /></td>
		<td>${BRHNAME}</td>
	</tr>
	<!-- 					         分行地址                    -->
	<tr>
		<td style="text-align:center"><spring:message code="LB.X0364" /></td>
		<td>${BRHADDR}</td>
	</tr>
	<!-- 					          分行連絡電話                    -->
	<tr>
		<td style="text-align:center"><spring:message code="LB.X0365" /></td>
		<td>${BRHTEL}</td>
	</tr>
</table>
<br/><br/>
<!-- 	<div class="text-left"> -->
<!-- 		說明: -->
<!-- 		<ol class="list-decimal text-left"> -->
<!-- 			<li>晶片金融卡:為保護您的交易安全，結束交易或離開電腦時，請務必將晶片金融卡抽離讀卡機並登出系統。</li> -->
<!-- 			<li>電子簽章:為保護您的交易安全，結束交易或離開電腦時，請務必將電子簽章(載具i-key)拔除並登出系統。</li> -->
<!-- 		</ol> -->
<!-- 	</div>	 -->
</body>
</html>