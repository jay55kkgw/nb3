<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/component/util/cardReaderMethod.js"></script>
	<script type="text/javascript">
	// 初始化
	$(document).ready(function () {
		init();
	});
	function init() {
		//表單驗證
		$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
		// 確認鍵 click
		goOn();
		////	建立非約定銀行代號下拉選單
		creatDpBHNO();
	}
	// 確認鍵 click
	function goOn() {
		$("#CMSUBMIT").click(function (e) {
			submitPay()
			if (!$('#formId').validationEngine('validate')) {
				e = e || window.event; // for IE
				e.preventDefault();
			} else {
				$("#formId").validationEngine('detach');
				initBlockUI(); //遮罩
				$("#formId").submit();
			}
		});
	}
	//
	function submitPay() {
		var ipayamt = $("#ipayamt").val();
		var BankID = $('#DPBHNO').val().substr(0,3);
		var bankname = $("#DPBHNO").find(":selected").text();
		$("#H_ipayamt").val(ipayamt)
		$("#BankID").val(BankID);
		$("#BANKNAME").val(bankname);
		
	
	}
	//繳款金額不能大於本期應繳金額
	function validate_payamt(){
		var payamt = $("tpayamt").val();
		var tpayamt = $("tpayamt").val();
		if (payamt > tpayamt) {
			return '繳款金額不能大於本期應繳金額';
		}	
	}
	
	
	//	建立非約定銀行代號下拉選單
	function creatDpBHNO() {
		var tmp_val = "";
		var dpbhno = "050";//預設值
		data = null;
		uri = '${__ctx}' + "/EBILL/PAY/getDpBHNO_aj"
		rdata = { type: 'dpbhno' };
		options = { keyisval: true, selectID: '#DPBHNO' }
		data = fstop.getServerDataEx(uri, rdata, false);
		if (data != null && data.result == true) {
			fstop.creatSelect(data.data, options);
			$("#DPBHNO option").each(function () {
				if (this.value.indexOf(dpbhno) != -1) {
					tmp_val = this.value;
				}
			});
			$("#DPBHNO").val(tmp_val);
		}
	}

	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>
	<!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<!-- 活存帳戶繳信用卡費 -->
			<li class="ttb-breadcrumb-item">
				<a href="#">
					<spring:message code="LB.X2427" />
				</a>
			</li>
			<!-- 活存帳戶繳信用卡費 -->
			<li class="ttb-breadcrumb-item active" aria-current="page">
				<spring:message code="LB.X2427" />
			</li>
		</ol>
	</nav>
	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!--活存帳戶繳信用卡費 -->
				<h2>
					<spring:message code="LB.X2427" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<form autocomplete="off" method="post" id="formId" action="${__ctx}/EBILL/PAY/cclifepayment_confirm">
					 <input type='hidden' id="BankID"   name="BankID" value="">
					 <input type='hidden' id="BANKNAME" name="BANKNAME" value="">
					 <input type='hidden' id="CARDNUM"  name="CARDNUM" value='${cclifepayment_step1.data.CARDNUM }'>
					 <input type='hidden' id="PAYAMT"   name="PAYAMT" value='${cclifepayment_step1.data.PAYAMT }'>
					 <input type='hidden' id="LPAYAMT"  name="LPAYAMT" value='${cclifepayment_step1.data.LPAYAMT }'>
					<!-- 表單顯示區 -->
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
									<!--  銷帳編號: -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.W0407" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
                                            <p>
                                               	${cclifepayment_step1.data.CARDNUM }
                                            </p>
                                        </div>
									</span>
								</div>
									<!-- 本期應繳金額 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.This_period" /><spring:message code="LB.Repayment_of_every_month" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
                                            <p>
                                             NTD ${cclifepayment_step1.data.PAYAMT } <spring:message code="LB.Dollar" />
                                            </p>
                                        </div>
									</span>
								</div>
								<!-- 最低應繳金額:-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4>
												<spring:message code="LB.X2432" />
											</h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
                                            <p>
                                             NTD ${cclifepayment_step1.data.LPAYAMT } <spring:message code="LB.Dollar" />
                                            </p>
                                        </div>
									</span>
								</div>
								<!-- 身分證字號/統編: -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label><spring:message code="LB.Id_no" /></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
										<input type="text" maxLength="10" size="11" class="text-input validate[required,funcCall[validate_checkSYS_IDNO[CUSIDN]]" id="CUSIDN" name="CUSIDN" value=""placeholder="請輸入身分證字號/統編" >
										</div>
									</span>
								</div>
								<!-- 轉出帳戶 -->
								<div class="ttb-input-item row">
									<span class="input-title"> <label><h4><spring:message code="LB.Payers_account_no" /></h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<select id="DPBHNO" name="DPBHNO" class="custom-select select-input half-input"></select>
										</div>
										<div class="ttb-input">
											<input type="text"  id="trin_acn" name="trin_acn" class="text-input" placeholder="<spring:message code="LB.Enter_Account" />" size="16" maxlength="16"> 
										</div>
									</span>
								</div>
								<!-- 繳款金額:-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label><h4><spring:message code="LB.Payment_amount" /></h4></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<span class="input-unit"><spring:message code="LB.NTD" /></span>
											<input type="text" id="ipayamt" name="ipayamt" class="text-input" size="8" maxlength="8" value=""> 
											<span class="input-unit"><spring:message code="LB.Dollar" /></span>
											<!-- 不在畫面上顯示的span -->
											<span name="hideblock" >
												<!-- 驗證用的input -->
												<input id="H_ipayamt" name="H_ipayamt" type="text" class="text-input validate[required ,custom[integer,min[1] ,max[100000]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
											</span>
										</div>
									</span>
								</div>
									<!-- 當轉帳交易成功，發送通知至您的信箱: -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label><spring:message code="LB.X2433" /></label>
									</span> 
									<span class="input-block">
										<div class="ttb-input">
											<input class="text-input validate[funcCall[validate_EmailCheck[CustEmail]]]"  name="CustEmail" id="CustEmail" value=""  placeholder="請輸入E-MAIL"   />
											</span>
										</div>
									</span>
								</div>
							</div>
							<!-- button -->
							<!--重新輸入 -->
							<spring:message code="LB.Re_enter" var="cmRest" />
							<input type="button" name="CMRESET" id="CMRESET" value="${cmRest}" class="ttb-button btn-flat-gray">
							<!-- 確定 -->
							<spring:message code="LB.Confirm" var="cmSubmit" />
							<input type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" class="ttb-button btn-flat-orange">
							<!--buttonEND -->
						</div>
					</div>
					<div class="text-left">
					</div>
				</form>
			</section>
			<!-- main-content END -->
		</main>
	</div><!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>