<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript"
	src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript"
	src="${__ctx}/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css"
	href="${__ctx}/css/validationEngine.jquery.css">
<link rel="stylesheet" type="text/css"
	href="${__ctx}/css/jquery.datetimepicker.css">

<script type="text/javascript">
	$(document).ready(function() {
		init();
	});
	function init() {
		$("#CMSUBMIT").click(
				function(e) {
					//TODO 驗證
					console.log("submit~~");
					initBlockUI(); //遮罩
					$("#formId").attr("action",
							"${__ctx}/DIGITAL/ACCOUNT/financial_card_renew_step1");
					$("#formId").submit();
				});
	}
</script>
</head>

<body>
<!-- 	 header     -->
	<header>
		<%@ include file="../index/header_logout.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上申請     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Register" /></li>
    <!-- 掃碼提款     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X1949" /></li>
    <!-- 線上註銷臺灣Pay掃碼提款功能     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D1589" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code="LB.D1589"/></h2>
<!-- 			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i> -->
			<form id="formId" method="post" action="">
			  <div id="step-bar">
                    <ul>
                        <li class="finished"><spring:message code="LB.D1204"/></li>
                        <li class="finished"><spring:message code="LB.D1585"/></li>
                        <li class="active"><spring:message code="LB.X2060"/></li>
                    </ul>
                </div>
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
						<div class="ttb-message">
                                <p><spring:message code="LB.X2060"/></p>
                        </div>
                        
                        
						
						<p class="form-description"><spring:message code="LB.X1955"/><spring:message code="LB.X1956"/></p>
						
						<div class="ttb-input-item row">
							<span class="input-title"> <label>
									<h4><spring:message code="LB.X2060"/></h4>
							</label>
							</span> 
							<span class="input-block">
								<div class="ttb-input">				
									<span><spring:message code="LB.X0292"/></span>
								</div>
							</span>
						</div>
						
						<!-- 註銷帳號 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.X1957"/></h4>
                                    </label>
                                </span>
                                <ul>
                                <c:forEach var="dataList" items="${ N580_step2.data.ACNTEMP }" varStatus="data">
                                    <li>
                                       <span>${dataList}</span>
                                    </li>
                               </c:forEach>
                               </ul>
                            </div>
					<!-- 註銷日期及時間 -->
					<!-- 註銷帳號 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.X1958"/></h4>
                                    </label>
                                </span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                       <span>${N580_step2.data.DATETIME}</span>
                                    </div>
                                </span>
                            </div>
					</div>
					<div class="col-12 text-center align-center" style="color:red;" >~<spring:message code="LB.X0333"/>~</div>
				</div>
			</form>
		</section>
		</main>
	<!-- 		main-content END -->
	<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>

</html>