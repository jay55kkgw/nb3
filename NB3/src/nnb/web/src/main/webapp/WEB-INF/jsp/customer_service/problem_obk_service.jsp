<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="page2" class="card-detail-block">
	<div class="card-center-block">
		<!-- Q1 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-21" aria-expanded="true"
				aria-controls="popup1-21">
				<div class="row">
					<span class="col-1">Q1</span>
					<div class="col-11">
						<span>網路銀行提供那些功能？</span>
					</div>
				</div>
			</a>
			<div id="popup1-21" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>網路銀行提供十類功能，介紹如下：</p>
							<div style="overflow: auto">
								<table class="question-table">
									<thead>
										<tr>
											<th>項目</th>
											<th>內容</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th>服務總覽</th>
											<td style="text-align: left;">帳戶總覽</td>
										</tr>
										<tr>
											<th>臺幣服務</th>
											<td class="white-spacing text-left">
												<ol class="list-decimal">
													<li data-num="1.">帳戶查詢：帳戶餘額查詢、帳戶明細查詢、虛擬帳號入帳明細</li>
													<li data-num="2.">轉帳交易：轉帳交易</li>
													<li data-num="3.">轉出記錄：轉出記錄查詢</li>
													<li data-num="4.">預約交易：預約交易查詢/取消、預約交易結果查詢</li>
													<li data-num="5.">定存服務：轉入綜存定存、定存明細查詢、定存自動轉期申請/變更、定存單到期續存、綜存定存解約、零存整付按月繳存</li>
													<li data-num="6.">輕鬆理財戶：存摺明細、證券交割明細、自動申購基金明細</li>
													<li data-num="7.">票據查詢：託收票據明細、支存當日不足扣票據明細、已兌現票據明細</li>
													<li data-num="8.">中央登錄債券：餘額查詢、明細查詢</li>
													<li data-num="9.">質借功能：質借功能取消 </li>
												</ol>
											</td>
										</tr>
										<tr>
											<th>外幣服務</th>
											<td class="white-spacing text-left">
												<ol class="list-decimal">
													<li data-num="1.">帳戶查詢：帳戶餘額查詢、活存明細查詢</li>
													<li data-num="2.">換匯/轉帳交易：買賣外幣/約定轉帳 </li>
													<li data-num="3.">轉出記錄：轉出記錄查詢 </li>
													<li data-num="4.">匯出匯款：匯出匯款 、匯出匯款查詢</li>
													<li data-num="5.">匯入匯款：匯入匯款查詢、線上解款、線上解款申請/註銷</li>
													<li data-num="6.">預約交易：預約交易查詢/取消、預約交易結果查詢</li>
													<li data-num="7.">定存服務：轉入外幣綜存定存、定存明細查詢、綜存定存解約、定存自動轉期申請/變更、存單到期續存</li>
													<li data-num="8.">進出口服務：進口到單查詢、出口押匯查詢</li>
													<li data-num="9.">信用狀服務：通知查詢、開狀查詢</li>
													<li data-num="10.">託收查詢：進口/出口託收查詢、光票託收查詢</li>
												</ol>
											</td>
										</tr>
										<tr>
											<th>繳費繳稅</th>
											<td class="white-spacing text-left">
												<ol class="list-decimal">
													<li data-num="1.">繳稅：繳稅</li>
													<li data-num="2.">公用事業費：臺灣電力公司、臺灣自來水公司、臺北自來水事業處、欣欣瓦斯 </li>
													<li data-num="3.">健保/勞保/國保/勞退金：健保費/補充保險費、勞保費、國民年金保險費、新制勞工退休金 </li>
													<li data-num="4.">學雜費：學雜費</li>
													<li data-num="5.">其他費用：期貨保證金、繳費</li>
													<li data-num="6.">自動扣繳服務：自動扣繳查詢/取消、臺灣電力公司、臺灣自來水公司、臺北自來水事業處、中華電信費、健保費、勞保費、新制勞工退休金、舊制勞工退休金、停車費</li>
												</ol>
											</td>
										</tr>
										<tr>
											<th>貸款服務</th>
											<td class="white-spacing text-left">
												<ol class="list-decimal">
													<li data-num="1.">小額信貸：申請小額信貸、申請資料查詢</li>
													<li data-num="2.">借款查詢：借款明細查詢、下期借款本息查詢、已繳款本息查詢 </li>
													<li data-num="3.">房屋擔保借款：繳息清單 </li>
												</ol>
											</td>
										</tr>
										<tr>
											<th>基金</th>
											<td class="white-spacing text-left">
												<ol class="list-decimal">
													<li data-num="1.">基金查詢：基金餘額/損益查詢、基金交易明細查詢、定期投資約定資料查詢、國內基金淨值查詢、國外基金淨值查詢、信託契約書查詢、預約查詢/取消</li>
													<li data-num="2.">海外債券查詢：海外債券餘額/損益查詢、海外債券交易明細查詢</li>
													<li data-num="3.">基金交易：單筆申購、定期投資申購、轉換交易、贖回交易、定期投資約定變更、停損/停利通知設定、SMART FUND自動贖回設定</li>
													<li data-num="4.">投資屬性評估 (KYC評估)：投資屬性評估調查表</li>
													<li data-num="5.">電子對帳單：帳單申請、密碼重置、密碼變更</li>
													<li data-num="6.">境外信託商品：商品推介申請、商品推介終止</li>
												</ol>
											</td>
										</tr>
										<tr>
											<th>黃金存摺</th>
											<td class="white-spacing text-left">
												<ol class="list-decimal">
													<li data-num="1.">查詢服務：黃金存摺餘額查詢、黃金存摺明細查詢、當日價格查詢、歷史價格查詢、預約取消/查詢</li>
													<li data-num="2.">黃金交易：黃金申購、黃金回售、繳納定期扣款失敗手續費</li>
													<li data-num="3.">定期定額交易：定期定額申購、定期定額變更、定期定額查詢</li>
													<li data-num="4.">黃金存摺帳戶：黃金存摺帳戶申請、黃金存摺網路交易申請</li>
													<li data-num="5.">投資屬性評估 (KYC評估)：投資屬性評估調查表</li>
												</ol>
											</td>
										</tr>
										<tr>
											<th>信用卡</th>
											<td class="white-spacing text-left">
												<ol class="list-decimal">
													<li data-num="1.">申請信用卡：申請信用卡、申請進度查詢</li>
													<li data-num="2.">帳務查詢：信用卡持卡總覽、未出帳單明細查詢、歷史帳單明細查詢、繳款紀錄查詢</li>
													<li data-num="3.">繳信用卡費：缴本行信用卡费、自動扣款申請/取消</li>
													<li data-num="4.">電子對帳單：電子對帳單申請、密碼重置、密碼變更</li>
													<li data-num="5.">實體對帳單：實體帳單補寄</li>
													<li data-num="6.">信用卡服務：信用卡開卡、信用卡掛失</li>
													<li data-num="7.">預借現金：密碼函補寄</li>
													<li data-num="8.">信用卡分期服務：簽署信用卡分期付款同意書、長期使用循環信用申請分期還款</li>
												</ol>
											</td>
										</tr>
										<tr>
											<th>線上服務專區</th>
											<td class="white-spacing text-left">
												<ol class="list-decimal">
													<li data-num="1.">行動銀行服務：啟用行動銀行服務</li>
													<li data-num="2.">約定轉入帳號服務：約定轉入帳號設定、約定轉入帳號取消</li>
													<li data-num="3.">電子對帳單：電子對帳單申請、密碼重置、密碼變更</li>
													<li data-num="4.">轉帳功能：轉帳功能申請/取消</li>
													<li data-num="5.">存款餘額證明：存款餘額證明申請</li>
													<li data-num="6.">票據服務：支票存款開戶申請、空白票據</li>
													<li data-num="7.">信用卡服務：申請信用卡、申請信用卡進度查詢、簽署信用卡分期付款同意書、長期使用循環信用申請分期還款</li>
													<li data-num="8.">小額信貸：申請小額信貸、申請資料查詢</li>
													<li data-num="9.">基金服務：預約開立基金戶</li>
													<li data-num="10.">投資屬性評估 (KYC評估)：投資屬性評估調查表</li>
													<li data-num="11.">境外信託商品：商品推介申請、商品推介終止</li>
													<li data-num="12.">隨護神盾：申請隨護神盾</li>
													<li data-num="13.">理財試算服務：臺幣定期存款試算、臺幣定存儲蓄存款試算、外匯定期存款利息試算、貸款攤還試算、年金計畫試算、定期定額投資試算</li>
												</ol>
											</td>
										</tr>
										<tr>
											<th>個人服務</th>
											<td class="white-spacing text-left">
												<ol class="list-decimal">
													<li data-num="1.">帳戶總覽設定：帳戶總覽設定</li>
													<li data-num="2.">帳戶設定：常用帳號設定、使用者名稱變更、密碼變更、網路銀行交易密碼線上解鎖、Email設定</li>
													<li data-num="3.">通訊資料變更：往來帳戶及信託業務通訊地址/電話變更、外匯進口/出口/匯兌通訊地址/電話變更、信用卡帳單地址/電話變更</li>
													<li data-num="4.">通知服務：通知服務設定、國內臺幣匯入匯款通知設定</li>
													<li data-num="5.">掛失/結清銷戶：掛失服務、臺幣存款帳戶結清銷戶申請、外匯存款帳戶結清銷戶申請</li>
													<li data-num="6.">扣繳憑單：各類所得扣繳憑單列印</li>
												</ol>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q2 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-22" aria-expanded="true"
				aria-controls="popup1-22">
				<div class="row">
					<span class="col-1">Q2</span>
					<div class="col-11">
						<span>網路銀行交易的最高限額？</span>
					</div>
				</div>
			</a>
			<div id="popup1-22" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>一、依本行現行安控機制，以統一編號歸戶，訂定已約定及非約定轉帳帳號每日交易限額如下：</p>
							<div style="overflow: auto">
								<table class="question-table" style="width: 100%">
									<thead>
										<tr>
											<th rowspan="2">安控機制</th>
											<th colspan="2">約定轉帳</th>
											<th colspan="3">非約定轉帳</th>
											<th colspan="3">線上約定轉入帳號</th>
										</tr>
										<tr>
											<th>自行</th>
											<th>跨行</th>
											<th>自行</th>
											<th>跨行</th>
											<th>合併</th>
											<th>自行</th>
											<th>跨行</th>
											<th>合併</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th>交易密碼</th>
											<td rowspan="5">500 萬</td>
											<td rowspan="5">200 萬</td>
											<td>-</td>
											<td>-</td>
											<td>-</td>
											<td rowspan="5" colspan="3">單筆5 萬<br>每日10 萬<br>每月20 萬元整。</td>
										</tr>
										<tr>
											<th>晶片金融卡</th>
											<td rowspan="4" colspan="3">單筆5 萬<br>每日10 萬<br>每月20 萬元整。</td>
										</tr>
										<tr>
											<th>動態密碼卡</th>
										</tr>
										<tr>
											<th>隨護神盾</th>
										</tr>
										<tr>
											<th>台灣Pay<br>(快速交易)</th>
										</tr>
										<tr>
											<th>電子簽章</th>
											<td>1,500 萬</td>
											<td>500 萬(每筆200 萬元)</td>
											<td>300 萬</td>
											<td>200 萬(不含外匯匯出匯款)</td>
											<td>-</td>
											<td>300 萬</td>
											<td>200 萬</td>
											<td>-</td>
										</tr>
										<tr>
											<th>備註</th>
											<td colspan="8" style="text-align: left;">
												<ol class="list-decimal">
													<li data-num="1.">上列密碼/電子簽章/晶片金融卡/動態密碼卡/隨護神盾之每日轉帳限額合併計算以2000萬元為上限。</li>
													<li data-num="2.">臺外幣交易限額合併計算。</li>
													<li data-num="3.">行動銀行交易限額與一般網銀合併計算。</li>
												</ol>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="ttb-result-list terms">
								<p>二、其他交易限額及相關規定：</p>
								<ol>
									<li>（一）數位存款帳戶轉帳限額: 同一客戶合計非約定轉帳金額，數位存款帳戶第一類及第二類每筆限新臺幣5萬元整，每日限新臺幣10萬元整，每月限新臺幣20萬元整;客戶親臨服務分行申請「約定轉帳」功能後，則比照本行一般存款帳戶臨櫃約定轉帳之交易限額，依不同安控機制訂定之每日交易限額。數位存款帳戶第三類其轉入非本人帳戶之約定、非約定轉帳及本人帳戶之跨行轉帳限額每筆限新臺幣1萬元整，每日限新臺幣3萬元整，每月限新臺幣5萬元整；轉入本人本行帳戶之非約定轉帳限額每筆限新臺幣5萬元整，每日限新臺幣10萬元整，每月限新臺幣20萬元整；轉入本人本行帳戶之臨櫃及線上約定轉帳之交易限額比照本行一般存款帳戶。</li>
									<li>（二）轉入綜存定期約定與非約定帳戶兩者合計每一轉入帳戶，當月累計限額100萬元。(自104年9月1日適用)</li>
									<li>（三）「繳費/稅」類：
										<ol>
											<li data-num="1.">以「晶片金融卡」、「動態密碼卡」及「隨護神盾」方式交易者與「密碼方式（SSL機制）」約定轉帳之跨行轉帳交易合併計算限額。</li>
											<li data-num="2.">以「電子簽章」方式交易者與電子簽章非約定轉帳之跨行轉帳交易合併計算限額。</li>
										</ol>
									</li>
									<li>（四）基金申購：每人每營業日以電子化交易方式（含網路/語音）進行基金交易轉出之金額，合計不得逾等值新臺幣伍佰萬元整。</li>
									<li>（五）黃金申購：每筆最低交易量為1公克，每日累計買進最高交易數量每一帳戶為50,000公克。</li>
									<li>（六）外匯交易：
										<ol>
											<li data-num="1.">外匯轉帳/外匯匯出匯款：
												<ol>
													<li data-num="(1)">同一戶名外幣與臺幣互轉，外幣結購或結售應個別累積，每日累積最高限額應低於新臺幣50萬元，並與電話銀行轉帳、金融卡提領外幣合併計算限額。</li>
													<li data-num="(2)">未滿20歲自然人，同一外匯存款帳戶不同幣別間戶轉、不同帳戶間匯轉帳（含匯出匯款）分別計算，每日累積最高限額應低於新臺幣50萬元。</li>
													<li data-num="(3)">個人戶結購或結售人民幣應個別累積，每人每日透過帳戶買賣之金額，不得逾人民幣二萬元。</li>
													<li data-num="(4)">領有中華民國國民身分證之個人始能申辦人民幣匯款至大陸地區，且每人每日之限額為人民幣八萬元。</li>
												</ol>
												＊上述(3)(4)項若涉及人民幣買賣者，個人戶仍應受人民幣2萬元之限制。
											</li>
											<li data-num="2.">外匯匯入匯款線上解款：
												<ol>
													<li data-num="(1)">存入臺幣帳戶應低於新臺幣50萬元。</li>
													<li data-num="(2)">存入外存帳戶無金額限制，惟未滿20歲自然人每日應低於等值新臺幣50萬元。</li>
												</ol>
											</li>
										</ol>
									</li>
									<li>（七）轉出帳戶無交易金額限制項目：
										<ol>
											<li data-num="1.">同一外匯存款帳戶不同幣別之轉帳交易。</li>
											<li data-num="2.">同一外匯綜合存款帳戶之活期存款與定期存款間轉帳交易。</li>											
										</ol>
									</li>
								</ol>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q3 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-23" aria-expanded="true"
				aria-controls="popup1-23">
				<div class="row">
					<span class="col-1">Q3</span>
					<div class="col-11">
						<span>網路銀行的服務時間</span>
					</div>
				</div>
			</a>
			<div id="popup1-23" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>外匯轉帳服務時間為週一至週五銀行營業日上午9：10至下午3：30。其他項目為24小時服務，惟跨行轉帳轉入時間依照各轉入行庫作業規定。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q4 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-24" aria-expanded="true"
				aria-controls="popup1-24">
				<div class="row">
					<span class="col-1">Q4</span>
					<div class="col-11">
						<span>申請網路銀行，是不是可以查到本人〈本公司〉的所有帳戶？</span>
					</div>
				</div>
			</a>
			<div id="popup1-24" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>是的，您可查詢到開立於本行您本人（本公司）全部帳戶交易明細資料。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q5 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-25" aria-expanded="true"
				aria-controls="popup1-25">
				<div class="row">
					<span class="col-1">Q5</span>
					<div class="col-11">
						<span>請問網路銀行提供非約定轉帳功能？</span>
					</div>
				</div>
			</a>
			<div id="popup1-25" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div class="ttb-result-list terms">
								<p>是的，客戶得使用「電子簽章」或「本行晶片金融卡」+「讀卡機」執行非約定轉帳交易。</p>
								<ol>
									<li data-num="1.">電子簽章: 需申請金融XML憑證，請依一:【申請資格】A04辦理申請及下載憑證。</li>
									<li data-num="2.">本行晶片金融卡:持本行晶片金融卡+讀卡機，且該晶片金融卡已具備非約定轉帳功能者:
										<ol>
											<li data-num="(1)">臨櫃申請一般網路銀行客戶，得以該晶片金融卡之主帳號執行新台幣非約定轉帳交易及其他交易類服務。</li>
											<li data-num="(2)">以「本行晶片金融卡」線上申請之客戶，僅具有查詢類功能，如您持有的「本行晶片金融卡」具備非約定轉帳功能者，可登入一般網路銀行線上申請交易服務功能，即得以該晶片金融卡之主帳號執行新台幣非約定轉帳交易及其他交易類服務。</li>											
										</ol>
									</li>											
								</ol>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q6 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-26" aria-expanded="true"
				aria-controls="popup1-26">
				<div class="row">
					<span class="col-1">Q6</span>
					<div class="col-11">
						<span>請問網路銀行提供哪些外匯功能？</span>
					</div>
				</div>
			</a>
			<div id="popup1-26" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>是。本行網路銀行提供外匯存款帳務查詢、外匯綜存定存、外匯綜存定存解約、外匯轉帳及外匯匯出匯款等功能。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q7 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-27" aria-expanded="true"
				aria-controls="popup1-27">
				<div class="row">
					<span class="col-1">Q7</span>
					<div class="col-11">
						<span>請問網路銀行外匯轉帳除了最高交易金額限制外，是否有最低交易金額及次數的限制？</span>
					</div>
				</div>
			</a>
			<div id="popup1-27" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>是。單一客戶外匯存款幣轉及結購、結售交易每筆在等值新台幣1,000元以下者，同一日交易累計次數(含一般網路銀行、企業網路銀行、行動銀行及電話銀行)合計以20次為限。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q8 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-28" aria-expanded="true"
				aria-controls="popup1-28">
				<div class="row">
					<span class="col-1">Q8</span>
					<div class="col-11">
						<span>我可以使用「一般網路銀行」扣繳停車費？如何申請?需支付手續費？</span>
					</div>
				</div>
			</a>
			<div id="popup1-28" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>可以，但限臺灣企銀存戶專用。請登入「一般網路銀行」約定帳戶代扣繳本人或他人之停車費，目前提供代繳台北市、新北市、高雄市路邊停車費，操作流程如下：</p>
							<p>※尚未申請網路銀行者，請先至營業單位申請「一般網路銀行」，並約定轉帳功能。</p>
							<p>※申請生效後，您的停車單將由系統自動扣款代繳，並E-MAIL通知您繳款結果，亦可登入「一般網路銀行」查詢。</p>
							<p>※扣繳停車費免支付手續費，歡迎多加利用。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q9 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-29" aria-expanded="true"
				aria-controls="popup1-29">
				<div class="row">
					<span class="col-1">Q9</span>
					<div class="col-11">
						<span>申請路邊停車費代扣繳服務何時生效扣款？假設開單日為99年4月2日，貴行何時代扣繳？</span>
					</div>
				</div>
			</a>
			<div id="popup1-29" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div class="ttb-result-list terms">
								<ol>
									<li data-num="1.">當天申請次日生效，舉例說明：
										<ol>
											<li data-num="(1)">客戶4月2日上本行網路銀行申請路邊停車費代扣繳，則客戶開單日為4月3日(含)以後之路邊停車費，本行將自約定之扣款帳號扣款。</li>
										</ol>
									</li>
									<li data-num="2.">依不同縣市停管處與本行約定之扣繳日不同，於不同日期扣款，就目前本行代扣台北市、新北市及高雄市路邊停車費時間分析如下：
										<ol>
											<li data-num="(1)">台北市：開單日起算之第5天扣款，不分例假日，以4月2日(週五)開單日為例，則扣款日為4月6日，若開單日為4月7日，則扣款日為4月11日(週日)。</li>
											<li data-num="(2)">新北市：開單日起算起之第6天扣款，不分例假日，以4月2日(週五)開單日為例，則扣款日為4月7日，若開單日為4月7日，則扣款日為4月12日(週一)。</li>											
											<li data-num="(3)">高雄市：開單日算起之第5天停管處傳檔，第6天扣款，惟傳檔日適逢例假日停管處不傳檔，則順延次日扣款，以4月2日(週五)開單日為例，則扣款日為4月7日，若開單日為4月7日，則扣款日為4月13日(週二，因4月11日為例假日，順延至4月12日)(週一收到代扣檔，隔日扣款)。</li>											
										</ol>
									</li>											
								</ol>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q10 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-210" aria-expanded="true"
				aria-controls="popup1-210">
				<div class="row">
					<span class="col-1">Q10</span>
					<div class="col-11">
						<span>重覆繳款如何處理？已註銷網路銀行路邊停車費代扣服務，註銷日前之重覆繳款，貴行是否自動退費？</span>
					</div>
				</div>
			</a>
			<div id="popup1-210" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div class="ttb-result-list terms">
								<ol>
									<li data-num="1.">客戶已申請本行網路銀行路邊停車費代扣繳服務，又持帳單至超商繳費，處理情形如下：
										<ol>
											<li data-num="(1)">如申請代扣繳台北市及新北市路邊停車費：由本行系統於接獲停車管理處退費通00過後逕行退入客戶與本行約定扣款帳戶。</li>
											<li data-num="(2)">如申請代扣繳高雄市路邊停車費：於99年6月30日(含)前，請客戶逕向高雄市政府交通局要求退費，自99年7月1日起，將由本行系統於接獲停車管理處退費通知，於當日24:00過後逕行退入客戶與本行約定扣款帳戶。</li>
										</ol>
									</li>
									<li data-num="2.">如於註銷日前收到停車費退款資料，系統於隔日退款至約定帳戶，惟如於註銷日後(含當日)收到停車費退款資料，請客戶逕行至停車單所屬縣市政府停車管理處申請退費。</li>
								</ol>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q11 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-211" aria-expanded="true"
				aria-controls="popup1-211">
				<div class="row">
					<span class="col-1">Q11</span>
					<div class="col-11">
						<span>假設客戶於99年4月2日已註銷網路銀行路邊停車費代扣服務，則貴行代扣至開單日為何日？</span>
					</div>
				</div>
			</a>
			<div id="popup1-211" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div class="ttb-result-list terms">
								<ol>
									<li data-num="1.">台北市於4月2日註銷，本行只處理至前一日收到之停車單資料，前一日收到之停車單資料為收檔日期減三天，因此只處理至開單日為3月30日為止之停車單。</li>
									<li data-num="2.">新北市於4月2日註銷，本行只處理至前一日收到之停車單資料，前一日收到之停車單資料為收檔日期減五天，因此只處理至開單日為3月28日為止之停車單。</li>
									<li data-num="3.">高雄市於4月2日註銷，本行只處理至前一日收到之停車單資料，前一日收到之停車單資料為收檔日期減五天，因此只處理至開單日為3月28日為止之停車單。</li>
								</ol>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q12 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-212" aria-expanded="true"
				aria-controls="popup1-212">
				<div class="row">
					<span class="col-1">Q12</span>
					<div class="col-11">
						<span>如何申請網路銀行電子帳單？</span>
					</div>
				</div>
			</a>
			<div id="popup1-212" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>網路銀行提供電子交易對帳單、基金電子對帳單及信用卡電子帳單，請登入一般網銀「線上服務專區」、「基金」或「信用卡」類選項，申請電子帳單；如電子郵箱位址變更，務請登錄變更，以利寄發電子帳單。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q13 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-213" aria-expanded="true"
				aria-controls="popup1-213">
				<div class="row">
					<span class="col-1">Q13</span>
					<div class="col-11">
						<span>如何開啟電子交易對帳單？</span>
					</div>
				</div>
			</a>
			<div id="popup1-213" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<div class="ttb-result-list terms">
								<ol>
									<li data-num="1.">初次使用前，請依電子對帳單郵件指示安裝瀏覽程式(同一台電腦安裝一次即可)。</li>
									<li data-num="2.">點選電子郵件之附加檔案並輸入密碼：依您的統一編號/身分證字號，如為10碼者請輸入後8碼；如小於10碼者，請輸入全部證號。</li>
								</ol>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q14 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-214" aria-expanded="true"
				aria-controls="popup1-214">
				<div class="row">
					<span class="col-1">Q14</span>
					<div class="col-11">
						<span>如何使用通知服務功能？</span>
					</div>
				</div>
			</a>
			<div id="popup1-214" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>請登入網路銀行「個人服務」－「Email設定」的「我的Email」，首次設定完成後，即啟用Email通知服務功能，提供轉帳成功、預約即將扣款、預約交易結果、定存即將到期通知、基金停利/停損點及相關服務及優惠訊息等多項通知服務。提醒您，為了能順利通知您，請確認您的Email位址的正確性，謝謝！</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q15 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-215" aria-expanded="true"
				aria-controls="popup1-215">
				<div class="row">
					<span class="col-1">Q15</span>
					<div class="col-11">
						<span>如何線上申請金融卡交易服務功能？</span>
					</div>
				</div>
			</a>
			<div id="popup1-215" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>提供以「本行晶片金融卡」線上申請一般網路銀行客戶，登入一般網銀「線上服務專區」服務點選「轉帳功能申請/取消」申請即可。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Q16 -->
		<div class="ttb-pup-block" role="tab">
			<a role="button" class="ttb-pup-title collapsed d-block"
				data-toggle="collapse" href="#popup1-216" aria-expanded="true"
				aria-controls="popup1-216">
				<div class="row">
					<span class="col-1">Q16</span>
					<div class="col-11">
						<span>人在國外忘記網路銀行密碼或者網路銀行密碼遭鎖定？</span>
					</div>
				</div>
			</a>
			<div id="popup1-216" class="ttb-pup-collapse collapse" role="tabpanel">
				<div class="ttb-pup-body">
					<ul class="ttb-pup-list">
						<li>
							<p>為了網路交易安全的控管，網路銀行交易密碼鎖住，須本人親臨至櫃台辦理密碼重置。若身處國外，您可委託家人處理，但需請您洽駐外單位辦理授權書認證，並將此認證授權書寄回台灣給您的代理人。代理人收到認證授權書後，再請代理人持此認證授權書、授權人之開戶印鑑、身分證明文件以及代理人的印鑑、身分證明文件於營業時間至分行辦理密碼重置。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>