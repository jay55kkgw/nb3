<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">


<script type="text/javascript">
$(document).ready(function() {
	// HTML載入完成後開始遮罩
	setTimeout("initBlockUI()", 50);
	// 開始跑下拉選單並完成畫面
	setTimeout("init()", 400);
	// 解遮罩
	setTimeout("unBlockUI(initBlockId)", 500);
	// 初始化時隱藏span
	$("#hideblock").hide();
	// 將.table變更為footable
	//initFootable();
	setTimeout("initDataTable()",100);
});

function init(){
	
	$("#formId").validationEngine({
		binded: false,
		promptPosition: "inline"
	});
	
	
	//確定 
	$("#CMSUBMIT").click(function(e) {
		
		//打開驗證欄位
		$("#hideblock").show();
		
		e = e || window.event;		
		
	    if (!$('#formId').validationEngine('validate')) {
	        e.preventDefault();
	    } else {
	    	$("#formId").validationEngine('detach');
				initBlockUI();
	  			$("#formId").submit(); 
	    }
	});
	
}

//表單驗證
function addData(value){
// 	alert(value);
	$("#ErrorMsg").val(value);
}

</script>
</head>
<body>
	<!--   IDGATE -->
	<%@ include file="../idgate_tran/idgateAlert.jsp" %>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 取消約定轉入帳號     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0243" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code="LB.D0243" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<input type="hidden" id="check" value="0"/>
			<form id="formId" method="post" action="${__ctx}/ONLINE/SERVING/predesignated_account_cancellation_s">
				<div class="main-content-block row">
					<div class="col-12">
                        <div class="ttb-input-block">
                            <div class="ttb-message">
                                <span><spring:message code="LB.D0244" /></span>
                            </div>
						<!-- 台幣 -->
							<ul class="ttb-result-list"></ul>
							<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
								<thead>
								<tr>
									<th></th>
									<th class="text-center"><spring:message code="LB.D0245" /></th>
									<th class="text-center"><spring:message code="LB.D0227" /></th>
								</tr>
								</thead>
								<tbody>
									<c:if test="${predesignated_account_cancellation.data.REC.size() > 0}">
										<c:forEach var="dataList" items="${predesignated_account_cancellation.data.REC}" varStatus="data">
											<tr>
												<td class="text-center">
													<label class="radio-block">
														<input type="radio" name="r1" value="${dataList}" onclick="addData('lock')"/> &nbsp;
														<span class="ttb-radio"></span>
			                                        </label>
		                                        </td>
												<td class="text-center"><c:out value='${dataList.NAME}' /></td>
												<td class="text-center">${dataList.BNKCOD} ${dataList.ACN}</td>
											</tr>
										</c:forEach>				
									</c:if>
									<c:if test="${predesignated_account_cancellation.data.REC.size() <= 0}">
										<tr style="display:none;"></tr>
									</c:if>
								</tbody>		
								</tbody>
							</table>
						
						<!-- 外幣 -->
							<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
								<thead>
								<tr>
									<th class="text-center"></th>
									<th class="text-center"><spring:message code="LB.D0247" /></th>
									<th class="text-center"><spring:message code="LB.D0248" /></th>
									<th class="text-center"><spring:message code="LB.Currency" /></th>
								</tr>
								</thead>
								<tbody>
									<c:if test="${f_predesignated_account_cancellation.data.REC.size() > 0}">
										<c:forEach var="dataList2" items="${f_predesignated_account_cancellation.data.REC}" varStatus="data">
											<tr>
												<td class="text-center">
													<label class="radio-block">
														<input type="radio" name="r1" value="${dataList2}" onclick="addData('lock')"/>&nbsp;
														<span class="ttb-radio"></span>
			                                        </label>
												</td>
												<td class="text-center">${dataList2.ACWNAME}</td>
												<td class="text-center">${dataList2.BENACC}</td>
												<td class="text-center">${dataList2.CCY}</td>
											</tr>
										</c:forEach>
									</c:if>
									<c:if test="${f_predesignated_account_cancellation.data.REC.size() <= 0}">
										<tr style="display:none;"></tr>
									</c:if>		
								</tbody>
							</table>
						
							<!-- 不在畫面上顯示的span -->
							<span id="hideblock">
	<!-- 						驗證用的input -->
								<input id="ErrorMsg" name="ErrorMsg" type="text" class="text-input validate[groupRequired[r1]]" 
									style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" value="" />
							</span>
	                    </div>
						<!-- 確定 -->
                        <input type="button" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm"/>" >
					</div>
				</div>
					<div class="text-left">
						
						<ol class="list-decimal description-list">
							<p><spring:message code="LB.Description_of_page" /></p>
							<li><font color="red"><B><spring:message code="LB.Predesignated_Account_Cancellation_P1_D1" /></B></font></li>
							<li><font color="red"><B><spring:message code="LB.Predesignated_Account_Cancellation_P1_D2" /></B></font></li>
						</ol>
					</div>
			</form>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>