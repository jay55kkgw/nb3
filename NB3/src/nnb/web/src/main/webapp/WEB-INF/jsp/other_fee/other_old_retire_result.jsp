<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>

<script type="text/javascript">
$(document).ready(function() {
	init();
});

// 畫面初始化
function init() {
	//列印
	$("#printbtn").click(function(){
		var params = {
			"jspTemplateName":"other_old_retire_result_print",
			"jspTitle":'<spring:message code= "LB.W0752" />',
			"CMQTIME":"${result_data.data.CMQTIME}",
			"TSFACN":"${result_data.data.TSFACN}",
			"UNTNUM":"${result_data.data.UNTNUM}",
			"CUSNUM":"${input_data.CUSNUM}",
			"CUSIDNUN":"${result_data.data.CUSIDNUN}",
			"UNTTEL":"${result_data.data.UNTTEL}"
		};
		openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
	});
}
</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 繳費繳稅     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0366" /></li>
    <!-- 自動扣繳服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2360" /></li>
    <!-- 舊制勞工退休提繳費     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0752" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.W0752" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form id="formId" method="post" action="">
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">
                            <div class="ttb-message">
                                <span><spring:message code="LB.D0181" /></span>
                            </div>
                            
                            <!-- 申請時間 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D1097" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
			                         <span>${ result_data.data.CMQTIME }</span>
                                    </div>
                                </span>
                            </div>  
                                
							  <!-- 扣帳帳號 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.D0168" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
			                         <span>${ result_data.data.TSFACN }</span>
                                    </div>
                                </span>
                            </div>  
							
							<!--扣帳金額 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.W0742" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
			                         <span>${ input_data.CUSNUM }</span>
                                    </div>
                                </span>
                            </div>  
							
							<!-- 投保單位代號 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.W0706" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
			                         <span>${ result_data.data.UNTNUM }</span>
                                    </div>
                                </span>
                            </div>  
							
							<!-- 投保單位營利事業/負責人<br>身分證統一編號 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.W0725" /><br><spring:message code="LB.D0519" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
			                         <span>${ result_data.data.CUSIDNUN }</span>
                                    </div>
                                </span>
                            </div>  
							
							<!-- 投保單位電話號碼 -->
                            <div class="ttb-input-item row">
                                <span class="input-title">
                                    <label>
                                        <h4><spring:message code="LB.W0729" /></h4>
                                    </label>
                                </span>
                                <span class="input-block" >
                                    <div class="ttb-input">
			                         <span>${ result_data.data.UNTTEL }</span>
                                    </div>
                                </span>
                            </div> 
                        </div>
                        <!-- 列印  -->
                        <spring:message code="LB.Print" var="printbtn"></spring:message>
                        <input type="button" id="printbtn" value="${printbtn}" class="ttb-button btn-flat-orange" />	
                    </div>
                </div>
				</form>
				<ol class="list-decimal description-list">
			    	<p><spring:message code="LB.Description_of_page"/></p>
					<li><spring:message code="LB.Other_Old_Retire_P4_D1"/></li>
					<li><spring:message code="LB.Other_Old_Retire_P4_D2"/></li>
					<li><spring:message code="LB.Other_Old_Retire_P4_D3"/></li>
                </ol>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>