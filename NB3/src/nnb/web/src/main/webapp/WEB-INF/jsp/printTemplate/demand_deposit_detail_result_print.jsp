<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js_u2.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
<!-- <%-- 	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script> --%> -->
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function () {
			window.print();
		});
	</script>
</head>

<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br /><br />
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif" /></div>
	<br /><br />
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<br />
	<div>
		<!-- 查詢時間 -->
		<p>
			<spring:message code="LB.Inquiry_time" /> : ${CMQTIME}
		</p>
		<!-- 查詢期間 -->
		<p>
			<spring:message code="LB.Inquiry_period" /> : ${CMPERIOD}
		</p>
		<!-- 資料總數 : -->
		<p>
			<spring:message code="LB.Total_records" /> : ${COUNT}
			<!--筆 -->
			<spring:message code="LB.Rows" />
		</p>
		<c:forEach var="labelListMap" items="${print_datalistmap_data}">
			<table class="print">
				<tr>
					<td style="text-align: center">
						<spring:message code="LB.Account" />
					</td>
					<td colspan="8">${labelListMap.ACN}</td>
				</tr>
				<tr style="text-align: center">
					<!--異動日-->
					<td>
						<spring:message code="LB.Change_date" />
					</td>
					<!-- 摘要 -->
					<td>
						<spring:message code="LB.Summary_1" />
					</td>
					<!--支出金額-->
					<td>
						<spring:message code="LB.Withdrawal_amount" />
					</td>
					<!--存入金額 -->
					<td>
						<spring:message code="LB.Deposit_amount" />
					</td>
					<!-- 餘額 -->
					<td>
						<spring:message code="LB.Account_balance_2" />
					</td>
					<!-- 票據號碼 -->
					<td>
						<spring:message code="LB.Checking_account" />
					</td>
					<!-- 資料內容 -->
					<td>
						<spring:message code="LB.Data_content" />
					</td>
					<!-- 收付行 -->
					<td>
						<spring:message code="LB.Receiving_Bank" />
					</td>
					<!-- 交易時間 -->
					<td>
						<spring:message code="LB.Trading_time" />
					</td>
				</tr>
				<c:forEach items="${labelListMap.rowListMap}" var="map" varStatus="index">
					<tr>
						<td style="text-align: center">${map.LSTLTD}</td>
						<td style="text-align: center">${map.MEMO_C}</td>
						<td style="text-align: right">${map.DPWDAMTfmt}</td>
						<td style="text-align: right">${map.DPSVAMTfmt}</td>
						<td style="text-align: right">${map.BALfmt}</td>
						<td style="text-align: center">${map.CHKNUM}</td>
						<td style="text-align: center">${map.DATA16}</td>
						<td style="text-align: center">${map.TRNBRH}</td>
						<td style="text-align: center">${map.TRNTIME}</td>
					</tr>
				</c:forEach>
			</table>
			<br /><br />
		</c:forEach>
		<div class="text-left">
			<!-- 		說明： -->
			<spring:message code="LB.Description_of_page" />
			<ol class="list-decimal text-left">
				<li>
					<spring:message code="LB.Demand_deposit_detail_P2_D1" />
				</li>
			</ol>
		</div>
	</div>
</body>

</html>