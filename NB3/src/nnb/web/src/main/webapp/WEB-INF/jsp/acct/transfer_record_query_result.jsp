<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
	<style>
		.Line-break{
			white-space:normal;
			word-break:break-all;
			width:100px;
			word-wrap:break-word;
		}
	</style>
</head>
<body>
        <!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 轉出記錄查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0002" /></li>
		</ol>
	</nav>



		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 --> 
 		<div class="content row">
 		   <%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
 		
 		<main class="col-12">	

		<!-- 		主頁內容  -->

			<section id="main-content" class="container">
		
				<h2><spring:message code="LB.X0002" /></h2>
<!-- 				 IS8n -->
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 						下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
<!-- 						下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
					</select>
				</div>
				<div class="main-content-block row">
					<div class="col-12 printClass">
						<ul class="ttb-result-list">
							<li>
								<!-- 查詢時間 -->
								<h3><spring:message code="LB.Inquiry_time" /></h3>
								<p>
									${transfer_record_query_result.data.CMQTIME}
								</p>
							</li>
							<li>
								<!-- 查詢期間 -->
								<h3><spring:message code="LB.Inquiry_period_1" /></h3>
								<p>
									${transfer_record_query_result.data.cmedate}
								</p>
							</li>
							<li>
								<!-- 資料總數 -->
								<h3><spring:message code="LB.Total_records" /></h3>
								<p>
									${transfer_record_query_result.data.COUNT}
									<spring:message code="LB.Rows" />
								</p>								
							</li>
							<li>
								<!-- 表名 -->
								<h3><spring:message code="LB.Report_name" /></h3>
								<p>
									<spring:message code="LB.W0069" />
								</p>								
							</li>
						</ul>
					<table class="stripe table-striped ttb-table dtable" data-show-toggle="first">
						<thead>
							<tr>
								<!-- 轉帳日期 -->
								<th><spring:message code="LB.Transfer_date" /></th>
								<!-- 轉出帳號 -->
								<th><spring:message code="LB.Payers_account_no" /></th>
								<!-- 轉入帳號/繳費稅代號 -->
								<th>
									<spring:message code="LB.Payees_account_no" />
									/<br>
									<spring:message code="LB.Pay_taxes_fee_code" />
								</th>
								<!-- 轉帳金額 -->
								<th><spring:message code="LB.Amount" /></th>
								<!-- 手續費 -->
								<th><spring:message code="LB.D0507" /></th>								
								<!-- 跨行序號 --><!-- 交易類別 -->
								<th><spring:message code="LB.W0062" /><hr/><spring:message code="LB.W0076" /></th>
								<!-- 備註 -->
								<th><spring:message code="LB.Note" /></th>
								<!-- 交易通知重發 -->
								<th><spring:message code="LB.W0078" /></th>
							</tr>
						</thead>
						<c:if test="${transfer_record_query_result.data.COUNT == 0}">
							<tbody>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							</tbody>
						</c:if>
						<c:if test="${transfer_record_query_result.data.COUNT != 0}">
							<tbody>
								<c:forEach var="dataList" items="${transfer_record_query_result.data.REC}" varStatus="data">
									<tr>
						                <!-- 轉帳日期 -->
						                <td class="text-center">${dataList.DPTXDATE}</td>
						                <!-- 轉出帳號 -->
						                <td class="text-center">${dataList.DPWDAC}</td>
						                <!-- 轉入帳號/繳費稅代號 -->
						                <td class="text-center">
						               		<c:if test="${dataList.DPSVBH != ''}">
						               		${dataList.DPSVBH}
						               		<br>
						               		</c:if>
						               		${dataList.DPSVAC}
						                </td>            
						                <!-- 轉帳金額 -->
						                <td class="text-right">${dataList.DPTXAMT}</td>
						                <!-- 手續費 -->
						                <td class="text-right">${dataList.DPEFEE}</td>					                
						                <!-- 跨行序號 --><!-- 交易類別 -->
						                <td class="text-center">${dataList.DPTXNO}<hr/>${dataList.ADOPID}</td>
						                <!-- 備註 -->
						                <td class="text-center Line-break">${dataList.DPTXMEMO}</td>
						                <!-- 交易通知重發 -->
						                <td class="text-center">
							                <c:if test="${dataList.DPREMAIL < transfer_record_query_result.data.EMAILTIMES && dataList.show_button == true}">
							                	<input type="button" class="ttb-sm-btn btn-flat-orange" id="${data.count}mail" name="mail" onclick="sendMail(${data.count})" value="<spring:message code="LB.X1493" />"/>					                
							                </c:if>
							                <c:if test="${dataList.DPREMAIL < transfer_record_query_result.data.EMAILTIMES && dataList.show_button == false}">
							                	<input type="button" class="ttb-sm-btn btn-flat-gray" id="${data.count}mail" name="mail" value="<spring:message code="LB.X1493" />" disabled/>					                
							                </c:if>
						                	<input type="hidden" id="${data.count}A" value = "${dataList.ADTXNO}">
						                </td>
									</tr>
								</c:forEach>
							</tbody>
						</c:if>
						</table>
						<input type="button" id="previous" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />"/>
						<input type="button" id="printbtn" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Print" />"/>
					</div>
				</div>
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>
					<li><span><spring:message code="LB.Transfer_Record_Query_P2_D1" /></span></li>
					<li><span><spring:message code="LB.Transfer_Record_Query_P2_D2" /></span></li>
					<li><span><spring:message code="LB.Transfer_Record_Query_P2_D3" /></span></li>
					<li><span><spring:message code="LB.Transfer_Record_Query_P2_D4" /></span></li>
					<li><span><spring:message code="LB.Transfer_Record_Query_P2_D5" /></span></li>
					<li><span style="color:red;"><spring:message code="LB.Transfer_Record_Query_P2_D6" /></span></li>
				</ol>
				<form id="formId" action="${__ctx}/download" method="post">
					<!-- 下載用 -->
 					<input type="hidden" name="downloadFileName" value="<spring:message code="LB.X0002" />"/><!--匯出匯款查詢 -->
					<input type="hidden" name="CMQTIME" value="${transfer_record_query_result.data.CMQTIME}"/>
					<input type="hidden" name="CMPERIOD" value="${transfer_record_query_result.data.cmedate}"/>
					<input type="hidden" name="COUNT" value="${transfer_record_query_result.data.COUNT}"/>
					<input type="hidden" name="downloadType" id="downloadType"/> 					
					<input type="hidden" name="templatePath" id="templatePath"/>		
					<!-- EXCEL下載用 -->
					<!-- headerRightEnd  資料列以前的右方界線
						 headerBottomEnd 資料列到第幾列 從0開始
						 rowStartIndex 資料列第一列的位置
						 rowRightEnd 資料列用方的界線
					 -->
					<input type="hidden" name="headerRightEnd" value="8"/>
					<input type="hidden" name="headerBottomEnd" value="6"/>
					<input type="hidden" name="rowStartIndex" value="7"/>
					<input type="hidden" name="rowRightEnd" value="8"/>
					<!-- TXT下載用
						txtHeaderBottomEnd需為資料第一列(從0開始)-->
					<input type="hidden" name="txtHeaderBottomEnd" value="11"/> 					
					<input type="hidden" name="txtHasRowData" value="true"/>
					<input type="hidden" name="txtHasFooter" value="false"/>
					<!--存rec資料用 -->
<!-- 					<input id="ROWDATA" name="ROWDATA" type="hidden" value="" />				 -->
				</form>
<!-- 				</form> -->
			</section>
			<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
    	<script type="text/javascript">
			$(document).ready(function(){
				// HTML載入完成後開始遮罩
				setTimeout("initBlockUI()",10);
				// 開始查詢資料並完成畫面
				setTimeout("init()",20);
				setTimeout("initDataTable()",100);
				// 解遮罩
				setTimeout("unBlockUI(initBlockId)",500);	
			});
			function init(){
				//initFootable();
				//上一頁按鈕
				$("#previous").click(function() {
					initBlockUI();
					fstop.getPage('${pageContext.request.contextPath}'+'/NT/ACCT/RESERVATION/transfer_record_query','', '');
				});

				$("#printbtn").click(function(){
					var params = {
						"jspTemplateName":"transfer_record_query_print",
						"jspTitle":"<spring:message code= "LB.X0002" />",// I18N
						"CMQTIME":"${transfer_record_query_result.data.CMQTIME}",
						"CMPERIOD":"${transfer_record_query_result.data.cmedate}",
						"COUNT":"${transfer_record_query_result.data.COUNT}"
					};
					openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
				});		
				
			}
			// mail重送
			function sendMail(dataCount){
// 				initBlockUI();
				var reMail;
				var times = "${transfer_record_query_result.data.EMAILTIMES}";
				var data = $('#'+dataCount+'A').val();
// 				alert("ajax進行中"+data);
				uri = '${__ctx}'+"/NT/ACCT/RESERVATION/A3000_mail_aj";
				console.log("A3000_mail_aj_uri: " + uri);
				rdata = { ADTXNO: data, RECTYPE: "TW", TIMES: times};
				var bs = fstop.getServerDataEx(uri, rdata, false);				
				console.log("A3000_mail_aj_result: " + bs.result);
				// 錯誤訊息
				if(!bs.result){
					reMail = bs.data.faild;
					//alert(reMail);
					errorBlock(
							null, 
							null,
							[reMail], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
				}else{
					reMail = bs.data.success;
					//執行過兩次將按鈕失效
					if(reMail >= times){
						$("#"+dataCount+'mail').prop("disabled", true);
                        $("#"+dataCount+'mail').prop("class", "ttb-sm-btn btn-flat-gray");
						//alert('<spring:message code= "LB.Alert002" />');		
                        errorBlock(
    							null, 
    							null,
    							["<spring:message code= 'LB.Alert003' />"], 
    							'<spring:message code= "LB.Quit" />', 
    							null
    						);
					}else{
						//alert('<spring:message code= "LB.Alert003" />');		
						errorBlock(
								null, 
								null,
								["<spring:message code= 'LB.Alert003' />"], 
								'<spring:message code= "LB.Quit" />', 
								null
							);
					}
				}
// 				unBlockUI();
			}
			
			//選項
		 	function formReset() {
// 		 		initBlockUI();
	 			if ($('#actionBar').val()=="excel"){
					$("#downloadType").val("OLDEXCEL");
					$("#templatePath").val("/downloadTemplate/transfer_record_query.xls");
		 		}else if ($('#actionBar').val()=="txt"){
					$("#downloadType").val("TXT");
					$("#templatePath").val("/downloadTemplate/transfer_record_query.txt");
		 		}
				$("#formId").attr("target", "");
	            $("#formId").submit();
	            $('#actionBar').val("");
	        }
				
		</script>
    
</body>
</html>