<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<meta http-equiv="Content-Language" content="zh-tw">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
</head>
 <body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 貸款服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Loan_Service" /></li>
    <!-- 借款查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Loan_Inquiry" /></li>
    <!-- 借款明細查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Loan_Detail_Inquiry" /></li>
    <!-- 客戶辦理網路銀行轉帳提前償還貸款本金應注意事項     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X0897" /></li>
		</ol>
	</nav>

		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 --> 
 		<div class="content row">
 		   <%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->

 		
		
		<!-- 		主頁內容  -->

			<section id="main-content" class="container">
				<h2><spring:message code="LB.X0897" /></h2><!-- 客戶辦理網路銀行轉帳提前償還貸款本金應注意事項 -->
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				
				<div class="main-content-block row">
					<div class="col-12"> 						
						<form id="formId" method="post">
							<input type="hidden" name="LoanACN" value="${ inputData.loanACN }"/>
							<input type="hidden" name="LoanSEQ" value="${ inputData.loanSEQ }"/>
							
							<div class="ttb-input-block">
								<div class="ttb-input-item row">
									<div class="text-left">
										<ol class="list-decimal text-left">
											<li><spring:message code="LB.X0898" />
<!-- 							  				已申請使用本行網路銀行且已約定新臺幣活期性存款(不含支票存款)帳戶為轉出帳號者。 -->
				  							</li>	
				  							<li  style="color:red"><spring:message code="LB.X0899" />
<!-- 				  							僅限提前償還本金，與本行約定每月應扣繳之本金及利息仍將於相當日扣款。 -->
				  							</li>
				  							<li><spring:message code="LB.X0900" />
<!-- 							  				擬於當日還款者，限於本行營業時間9:00~15:30(含)完成轉帳交易；其他非營業時間完成還款轉帳交易者，次營業日生效。	 -->
				  							</li>	
				  							<li><spring:message code="LB.X0901" />
<!-- 		                               		 擬取消次營業日還款之轉帳交易者，須於24:00前辦理。 -->
				  							</li>
				  							<li><spring:message code="LB.X0902" />
<!-- 							  				還本金額至少須大於或等於「應繳金額」，倘還本小於「應繳金額」則還本金額須為千元之倍數，償還本金金額累計每日最高新臺幣200萬元，且不得將貸款本金償還至零元。 -->
				  							</li>
				  							<li  style="color:red"><spring:message code="LB.X0903" />
<!-- 							  				貸款本金一經償還，不得要求返還，請謹慎操作。 -->
				  							</li>  				
										</ol>
									</div>
								</div>
							</div>
							<div class="ttb-input-block">
								<div class="ttb-input-item row">
									<center style="width:100%">
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.X0904" /><!-- 本人已閱讀並同意上述條款 -->
											 	<input type="radio" name="R1" id="R1_1" value="Y">
												<span class="ttb-radio"></span>
											</label>
										</div>
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.X0905" /><!-- 不同意上述條款(無法辦理提前償還本金) -->
											 	<input type="radio" name="R1" id="R1_2" value="N">
												<span class="ttb-radio"></span>
											</label>
										</div>
										<span class="hideblock">
											<input id="validate_R1" name="PURPOSE" type="radio"  
												class="validate[funcCall[validate_Radio[<spring:message code= "LB.X1569" />,R1]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px; visibility: hidden;"/>
										</span>
									</center>
								</div>
							</div>
							
						</form>
						<spring:message code="LB.Confirm" var="cmConfirm"></spring:message>
							<input type="button" class="ttb-button btn-flat-orange" id="pageshow" value="${cmConfirm}"/>
					</div>
				</form>
				</div>
			</div>
			</section>
		</main>
	</div>
	<!-- 	content row END -->
    <%@ include file="../index/footer.jsp"%>
<!--   Js function -->
    <script type="text/JavaScript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 50);
		// 開始跑下拉選單並完成畫面
		setTimeout("init()", 400);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	function init(){
		$("#formId").validationEngine({ binded: false, promptPosition: "inline" });
		$("#pageshow").click(function(e){
			if(!$('#formId').validationEngine('validate')){
	        	e.preventDefault();
 			}else{
 				console.log("main submit");
 				$("#formId").validationEngine('detach');
 				initBlockUI();
 				if($('input[name="R1"]:checked').val() == "Y"){
 	 				$("#formId").attr("action","${__ctx}/LOAN/ADVANCE/repay_advance_p2");
 				}else{
 	 				$("#formId").attr("action","${__ctx}/LOAN/QUERY/loan_detail");
 				}
 	  			$("#formId").submit(); 
 			}		
  		});
	}

		
 	</script>
</body>
</html>
