<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",10);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
	
	$("#okButton").click(function(){
		if($("#regularConfirmCheckBox").prop("checked") == true){
			$("#formID").attr("action","${__ctx}/FUND/TRANSFER/fund_risk_confirm?TXID=C017");
			$("#formID").submit();
		}
		else{
			//<!-- 請勾選「本人已閱讀並瞭解本文件之內容」小方塊 -->
			//alert('<spring:message code= "LB.Alert120" />');
			errorBlock(
					null, 
					null,
					["<spring:message code= 'LB.Alert120' />"], 
					'<spring:message code= "LB.Quit" />', 
					null
			);
		}
	});
});
</script>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
	<!--麵包屑-->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
			<li class="ttb-breadcrumb-item"><a href="#"></a></li>
			<li class="ttb-breadcrumb-item active" aria-current="page"></li>
		</ol>
	</nav>
	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">

			<!-- 定期投資申購 -->
					<h2><spring:message code= "LB.W1083" /></h2>
					<form id="formID" method="post">
						<input type="hidden" name="TXID" value="${TXID}"/>
						<input type="hidden" name="COUNTRY" value="${COUNTRY}"/>
						<input type="hidden" name="FUS98E" value="${FUS98E}"/>
						<input type="hidden" name="FDINVTYPE" value="${FDINVTYPE}"/>
						<input type="hidden" name="ACN1" value="${ACN1}"/>
						<input type="hidden" name="ACN2" value="${ACN2}"/>
						<input type="hidden" name="PAYTYPE" value="${PAYTYPE}"/>
						<input type="hidden" name="HTELPHONE" value="${HTELPHONE}"/>
						<input type="hidden" name="FUNDACN" value="${FUNDACN}"/>
						<input type="hidden" name="COUNTRYTYPE" value="${COUNTRYTYPE}"/>
						<input type="hidden" name="COUNTRYTYPE1" value="${COUNTRYTYPE1}"/>
						<input type="hidden" name="BILLSENDMODE" value="2"/>
						<input type="hidden" name="FUNDLNAME" value="${FUNDLNAME}"/>
						<input type="hidden" name="CUTTYPE" value="${CUTTYPE}"/>
						<input type="hidden" name="BRHCOD" value="${BRHCOD}"/>
						<input type="hidden" name="RISK" value="${RISK}"/>
						<input type="hidden" name="SALESNO" value="${SALESNO}"/>
						<input type="hidden" name="MIP" value="${MIP}"/>
						<input type="hidden" name="PRO" value="${PRO}"/>
						<input type="hidden" name="PAYDAY1" value="${PAYDAY1}"/>
						<input type="hidden" name="PAYDAY2" value="${PAYDAY2}"/>
						<input type="hidden" name="PAYDAY3" value="${PAYDAY3}"/>
						<input type="hidden" name="PAYDAY4" value="${PAYDAY4}"/>
						<input type="hidden" name="PAYDAY5" value="${PAYDAY5}"/>
						<input type="hidden" name="PAYDAY6" value="${PAYDAY6}"/>
						<input type="hidden" name="PAYDAY7" value="${PAYDAY7}"/>
						<input type="hidden" name="PAYDAY8" value="${PAYDAY8}"/>
						<input type="hidden" name="PAYDAY9" value="${PAYDAY9}"/>
						<input type="hidden" name="INVTYPE" value="${INVTYPE}"/>
						<input type="hidden" name="TYPE" value="${TYPE}"/>
						<input type="hidden" name="COMPANYCODE" value="${COMPANYCODE}"/>
						<input type="hidden" name="TRANSCODE" value="${TRANSCODE}"/>
						<input type="hidden" name="AMT3" value="${AMT3}"/>
						<input type="hidden" name="YIELD" value="${YIELD}"/>
						<input type="hidden" name="STOP" value="${STOP}"/>
						<input type="hidden" name="FEE_TYPE" value="${FEE_TYPE}"/>
						<input type="hidden" name="SAL01" value="${SAL01}"/>
						<input type="hidden" name="SAL03" value="${SAL03}"/>
					<div class="main-content-block row">
						<div class="col-12">
							<div class="ttb-message">
								<p>臺灣企銀定時不定額投資計劃約定條款</p>
	                        </div>
	                        <div style="width: 90%;margin: auto">
	                        <ul class="ttb-result-list" style="list-style:none;">
								<li class="full-list"><strong style="font-size:15px">
									立約人（以下稱委託人）茲向臺灣中小企業銀行（以下稱受託人）申請辦理「定時不定額」投資，並約定下列事項：<br/><br>
										1﹒基準扣款金額<br/>
										基準扣款金額係指委託人第一次申請辦理時或於信託期間辦理變更之指定扣款金額，此為日後進行加減碼之基準，且依幣別之不同有最低設定金額之限制。<br/><br/>
										2﹒約定扣款日扣款金額<br/>
										委託人申請後之第一次扣款以基準扣款金額為準不另行加減碼，第二次起（含以後）之扣款則以依加減碼標準進行加減碼後之金額扣款，惟依幣別之不同有最低扣款金額之限制。<br/><br/>
										3﹒比較基準<br/>
										本計劃係以短天期5日之平均淨值與180日之平均淨值為比較基準，並以比較基準之比較結果與加減碼標準決定扣款金額。<br/>
										比較基準係以預備扣款日（約定扣款日之前一個金融機構營業日）前之5個及180個淨值日（以基金公司已公告者為準）之平均值為計算依據。<br/><br/>
										4﹒加減碼標準<br/>
										扣款金額之加減碼係依據比較基準之計算結果如上漲、下跌，及設定的漲跌幅區間比較後決定之，當漲幅超過一定標準（目前約定為10％）後則減少扣款金額，跌幅超過一定標準後（目前約定為10％）則增加扣款金額。<br/><br/>
										5﹒加減碼金額<br/>
										本計劃之加減碼金額係依照不同幣別採固定金額加減碼（目前加減碼金額固定臺幣 3,000、美元 100、歐元 100及日圓 10,000），並有減碼後之最低扣款金額之限制。<br/><br/>
										6﹒<br/>
										受託人保留調整比較基準、加減碼金額與加減碼標準之權利，如有變更時將於變更前以對帳單通知委託人。<br/><br/>
										7﹒<br/>
										指定投資標的之最新淨值悉依基金公司之公告為準，且因個別基金淨值之公告時間及受託人接收資料時間之不同，委託人自行計算之加減碼金額，與實際扣款之加減碼金額不同時，以受託人實際扣款金額為準。<br/><br/>
										8﹒<br/>
										原扣款方式為定時定額欲改為定時不定額或原扣款方式為定時不定額欲改為定時定額，委託人應重新申請無法直接轉換。<br/><br/>
										9﹒<br/>
										指定投資標的因故成為未核備基金或額度控管基金時，委託人仍可保留原投資部位，在基金下架或額度控管期間僅得以原約定基準扣款金額申購，不適用本計劃。<br/><br/>
										10﹒<br/>
										委託人申請辦理投資標的轉換以同一基金公司且得辦理定時不定額者為限，  轉換後並以新指定標的為扣款標的。<br/><br/>
										11﹒<br/>
										連續扣款失敗三次憑證將失效，系統將自動停止扣款，委託人如欲繼續扣款則需重新申請，故為避免扣款失敗之情形，委託人於扣款日之前，扣款帳號保留之餘額應至少大於加碼後扣款金額（即基準扣款金額＋加碼金額）及申購手續費。<br/><br/>
										12﹒<br/>
										本約定條款為「特定金錢信託投資國內外有價證券約定書」之一部分，若兩者之約定有牴觸時，應優先適用本約定條款。
								</strong></li>
							</ul>
							
							<label class="check-block">
								<input type="checkbox" id="regularConfirmCheckBox"/>本人已閱讀並瞭解本文件之內容
								<span class="ttb-check"></span>
							</label>
							<br/>
							</div>
			
	                  			<input type="button" id="okButton" value="<spring:message code="LB.Confirm_1"/>" class="ttb-button btn-flat-orange"/>
	                	
						</div>
					</form>
				</div>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>