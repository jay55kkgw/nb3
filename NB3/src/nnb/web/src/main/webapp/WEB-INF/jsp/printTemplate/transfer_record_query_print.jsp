<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ include file="../__import_js_u2.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>${jspTitle}</title>
<script type="text/javascript">
	$(document).ready(function() {
		window.print();
	});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br /><br />
	<div style="text-align: center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br /><br /><br />
	<div style="text-align: center">
		<font style="font-weight: bold; font-size: 1.2em">${jspTitle}</font>
	</div>
	<br /><br /><br />
	<label><spring:message code="LB.Inquiry_time" /> <!-- 查詢時間 -->：</label>
	<label>${CMQTIME}</label>
	<br />
	<label><spring:message code="LB.Inquiry_period_1" /> <!-- 查詢期間 -->：</label>
	<label>${CMPERIOD}</label>
	<br />
	<label><spring:message code="LB.Total_records" /> <!-- 資料總數 -->：</label>
	<label>${COUNT} <spring:message code="LB.Rows" /></label>
	<br /><br />
	<table class="print">
		<thead>
			<tr>
<!-- 表名 -->				<th><spring:message code= "LB.Report_name" /></th>
<!-- 轉出紀錄查詢 -->		<th colspan="13"><spring:message code= "LB.X0002" /></th>
			</tr>
			<tr>
<!-- 轉帳日期 -->		<th><spring:message code= "LB.Transfer_date" /></th>
<!-- 轉出帳號 -->		<th><spring:message code= "LB.Payers_account_no" /></th>
<!-- 轉入帳號 --><!-- 繳費稅代號 --><th><spring:message code= "LB.Payees_account_no" />/<br><spring:message code= "LB.Pay_taxes_fee_code" /></th>
<!-- 轉帳金額 -->		<th><spring:message code= "LB.Amount" /></th>
<!-- 手續費 -->			<th><spring:message code= "LB.D0507" /></th>
<!-- 跨行序號 -->			<th><spring:message code= "LB.W0062" /></th>
<!-- 交易類別 -->			<th><spring:message code= "LB.Transaction_type" /></th>
<!-- 備註 -->				<th><spring:message code= "LB.Note" /></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="dataList" items="${dataListMap}">
				<tr>
					<td class="text-center">${dataList.DPTXDATE}</td>
					<td class="text-center">${dataList.DPWDAC}</td>
					<td class="text-center">
						<c:if test="${dataList.DPSVBH != ''}">
						${dataList.DPSVBH}
						<br>
						</c:if>
						${dataList.DPSVAC}
					</td>
					<td class="text-right">${dataList.DPTXAMT}</td>
					<td class="text-right">${dataList.DPEFEE}</td>
					<td class="text-center">${dataList.DPSTANNO}</td>
					<td class="text-center">${dataList.ADOPID}</td>
					<td class="text-center">${dataList.DPTXMEMO}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<br />
	<div class="text-left">
		<spring:message code="LB.Description_of_page" />：
		<ol class="list-decimal text-left">
			<li><spring:message code="LB.Transfer_Record_Query_P2_D1" /></li>
			<li><spring:message code="LB.Transfer_Record_Query_P2_D2" /></li>
			<li><spring:message code="LB.Transfer_Record_Query_P2_D3" /></li>
			<li><spring:message code="LB.Transfer_Record_Query_P2_D4" /></li>
			<li><spring:message code="LB.Transfer_Record_Query_P2_D5" /></li>
			<li style="color: red;"><spring:message code="LB.Transfer_Record_Query_P2_D6" /></li>
		</ol>
	</div>
	<br />
</body>
</html>