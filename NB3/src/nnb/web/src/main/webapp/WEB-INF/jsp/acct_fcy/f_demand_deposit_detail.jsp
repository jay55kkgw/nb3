<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
</head>
 <body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 外幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Service" /></li>
	<!-- 帳戶查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.FX_Account_Inquiry" /></li>
    <!-- 活存明細查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.FX_Demand_Deposit_Detail" /></li>
		</ol>
	</nav>



		<!-- 	快速選單及主頁內容 -->
		<!-- menu、登出窗格 --> 
 		<div class="content row">
 		   <%@ include file="../index/menu.jsp"%><!-- 功能選單內容 -->
		<!-- 		主頁內容  -->
		<main class="col-12">	

			<section id="main-content" class="container">
				<h2><spring:message code="LB.FX_Demand_Deposit_Detail" /></h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 						下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
<!-- 						下載為txt檔 -->
						<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
<!-- 						下載為舊txt檔 -->
						<option value="oldtxt"><spring:message code="LB.Download_oldversion_txt_file" /></option>
					</select>
				</div>
					<div class="main-content-block row">
						<div class="col-12"> 						
						<form method="post" id="formId" action="${__ctx}/FCY/ACCT/f_demand_deposit_detail_result">
							<!-- 下載用 -->
							<input type="hidden" name="downloadFileName" value="LB.FX_Demand_Deposit_Detail"/>
							<input type="hidden" name="downloadType" id="downloadType"/> 					
							<input type="hidden" name="templatePath" id="templatePath"/> 	
							<input type="hidden" name="hasMultiRowData" value="true"/> 	
							<!-- EXCEL下載用 -->
							<input type="hidden" name="headerRightEnd" value="1" />
		                    <input type="hidden" name="headerBottomEnd" value="4" />
		                    <input type="hidden" name="multiRowStartIndex" value="8" />
		                    <input type="hidden" name="multiRowEndIndex" value="8" />
		                    <input type="hidden" name="multiRowCopyStartIndex" value="5" />
		                    <input type="hidden" name="multiRowCopyEndIndex" value="8" />
		                    <input type="hidden" name="multiRowDataListMapKey" value="rowListMap" />
		                    <input type="hidden" name="rowRightEnd" value="8" />
							<!-- TXT下載用 -->
							<input type="hidden" name="txtHeaderBottomEnd" value="7"/>
							<input type="hidden" name="txtMultiRowStartIndex" value="11"/>
							<input type="hidden" name="txtMultiRowEndIndex" value="11"/>
							<input type="hidden" name="txtMultiRowCopyStartIndex" value="8"/>
							<input type="hidden" name="txtMultiRowCopyEndIndex" value="11"/>
							<input type="hidden" name="txtMultiRowDataListMapKey" value="rowListMap"/>
<%-- 							<input type="hidden" id="urlID" value="${demand_deposit_detail.data.urlID}" /> --%>
							<div class="ttb-input-block">
					        <!-- 帳號區塊-->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<!-- 帳號 -->
											<spring:message code="LB.Account"/>
										</label>
									</span>
									<span class="input-block">
										<select name="ACN" id="ACN" class="custom-select select-input half-input validate[required]">
										<c:forEach var="dataList" items="${ demand_deposit_detail.data.REC }">
						           			<option value="${dataList.ACN}">${dataList.ACN}</option>
						           		</c:forEach>
						           		<option value="ALL">
											<spring:message code="LB.All" />
										</option>
										</select>
									</span>
								</div>
								<!-- 查詢區間區塊 -->
								<div class="ttb-input-item row">
									<!--查詢區間  -->
									<span class="input-title">
										<label>
											<spring:message code="LB.Inquiry_period" />
										</label>
									</span>
									<span class="input-block">
										<!--  當日交易 -->
										<div class="ttb-input">
											<label class="radio-block">
												<input type="radio" name="FGPERIOD" id="CMTODAY" value="CMTODAY" checked/>
												<label><spring:message code="LB.Todays_transaction" /></label>
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!--  當月交易 -->
										<div class="ttb-input">
											<label class="radio-block">
											<input type="radio" name="FGPERIOD" id="CMCURMON" value="CMCURMON" />
											<label><spring:message code="LB.This_months_transaction" /></label>
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!-- 上月交易 -->
										<div class="ttb-input">
											<label class="radio-block">
											<input type="radio" name="FGPERIOD" id="CMLASTMON" value="CMLASTMON" />
											<label><spring:message code="LB.Last_months_trading" /></label>
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!-- 最近一個月 -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Recently_a_month" />
												<input type="radio" name="FGPERIOD" id="CMNEARMON" value="CMNEARMON" />
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!--  最近一星期 -->
										<div class="ttb-input">
											<label class="radio-block">
											<input type="radio" name="FGPERIOD" id="CMLASTWEEK" value="CMLASTWEEK" />
											<label><spring:message code="LB.Recently_a_week" /></label>
												<span class="ttb-radio"></span>
											</label>
										</div>
										<!--  指定日期區塊 -->
										<div class="ttb-input">								
											<label class="radio-block">
											<!--  指定日期 -->
												<spring:message code="LB.Specified_period" />
												<input type="radio" name="FGPERIOD" id="CMPERIOD" value="CMPERIOD" /> 
												<span class="ttb-radio"></span>
											</label>
											<!-- 起迄日區塊預設隱藏 -->
										<div id = "hideblock_FGPERIOD" style="display: none;">
											<!--期間起日 -->
											<div class="ttb-input">
												<span class="input-subtitle subtitle-color">
													<spring:message code="LB.Period_start_date" />
												</span>
												<input type="text" id="CMSDATE" name="CMSDATE" class="text-input datetimepicker" maxlength="10" value="${demand_deposit_detail.data.TODAY}" /> 
												<span class="input-unit CMSDATE">
													<img src="${__ctx}/img/icon-7.svg" />
												</span>
											</div>
											<div class="ttb-input">
												<span class="input-subtitle subtitle-color">
													<spring:message code="LB.Period_end_date" />
												</span>
												<input type="text" id="CMEDATE" name="CMEDATE" class="text-input datetimepicker" maxlength="10" value="${demand_deposit_detail.data.TODAY}" /> 
												<span class="input-unit CMEDATE">
													<img src="${__ctx}/img/icon-7.svg" />
												</span>
											</div>
												<!-- 驗證用的span預設隱藏 -->
												<span id="hideblock_CheckDateScope" >
												<!-- 驗證用的input -->
												<input id="odate" name="odate" type="text" value="${demand_deposit_detail.data.TODAY}" class="text-input validate[required,funcCall[validate_CheckDateScope['<spring:message code= "LB.Inquiry_period_1" />', odate, CMSDATE, CMEDATE, false, 24, 2]]]" 
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
												</span>
											</div>
											<!-- 起迄日區塊 -END-->
										</div>
									</span>
								</div>
							</div>
						</form>
						<input type="button" class="ttb-button btn-flat-orange" id="pageshow" value="<spring:message code="LB.Display_as_web_page" />"/>
					</div>
				</div>
					<div class="text-left">
						<ol class="description-list list-decimal">
						<p>
						<spring:message code="LB.Description_of_page" /> 
						</p>
							<li><spring:message code="LB.F_demand_deposit_P1_D1" /></li>
							<li><spring:message code="LB.F_demand_deposit_P1_D2" /></li>
							<li>
								<spring:message code= "LB.Demand_deposit_detail_P1_D2-1" />
								<a target="_blank" href='${pageContext.request.contextPath}/public/N520_TXT.htm'>
									<spring:message code= "LB.File_description" />
								</a> 
								(<spring:message code= "LB.X0079" />)。
							</li>
							<li>
								<spring:message code="LB.Demand_deposit_detail_P1_D3-1" />
								<a target="_blank" href='${pageContext.request.contextPath}/public/N520_OLDTXT.htm'>
									<spring:message code= "LB.File_description" />
								</a>
								。
							</li>
						</ol>
					</div>
<!-- 				</form> -->
			</section>
			<!-- 		main-content END -->
		</main>
	</div>
	<!-- 	content row END -->
    <%@ include file="../index/footer.jsp"%>
<!--   Js function -->
    <script type="text/JavaScript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 50);
		// 開始跑下拉選單並完成畫面
		setTimeout("init()", 400);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	function init(){
		// 初始化時隱藏span
		$("#hideblocka").hide();
		//日期
		datetimepickerEvent();
		//預約自動輸入今天
// 		getTmr();
		
		initFootable();
		//日期區間切換按鈕,如果點選指定日期則顯示起迄日
		fgperiodChageEvent();
		
		//var getacn = $("#urlID").val();
		var getacn = '${demand_deposit_detail.data.urlID}';
		if(getacn != null && getacn != ''){
			$("#ACN option[value= '"+ getacn +"' ]").prop("selected", true);
		}
		
		$("#formId").validationEngine({
			binded: false,
			promptPosition: "inline"
		});	
		$("#pageshow").click(function(e){
			//打開驗證隱藏欄位
			$("#hideblock_CheckDateScope").show();
			
			if(!$("#formId").validationEngine("validate")){
				e = e || window.event;//forIE
				e.preventDefault();
			}
	 		else{
 				$("#formId").validationEngine('detach');
 				initBlockUI();
 				$("#formId").removeAttr("target");
 				$("#formId").attr("action","${__ctx}/FCY/ACCT/f_demand_deposit_detail_result");
 	  			$("#formId").submit(); 
 			}		
  		});
	}
	//日期區間切換按鈕,如果點選指定日期則顯示起迄日
	function fgperiodChageEvent() {
		$('input[type=radio][name=FGPERIOD]').change(function () {
			if (this.value == 'CMPERIOD') {
				$("#hideblock_FGPERIOD").show();
			} else {
				$("#hideblock_FGPERIOD").hide();
				//預約自動輸入今天
// 				getTmr();
			}
		});
	}
	
	//預約自動輸入今天
// 	function getTmr() {
// 		var today = new Date();
// 		today.setDate(today.getDate());
// 		var y = today.getFullYear();
// 		var m = today.getMonth() + 1;
// 		var d = today.getDate();
// 		if(m<10){
// 			m = "0"+m
// 		}
// 		if(d<10){
// 			d="0"+d
// 		}
// 		var tmr = y + "/" + m + "/" + d
// 		$('#CMSDATE').val(tmr);
// 		$('#CMEDATE').val(tmr);
// 		$('#odate').val(tmr);
// 	}
    
	// 日曆欄位參數設定
	function datetimepickerEvent() {
		$(".CMSDATE").click(function (event) {
			$('#CMSDATE').datetimepicker('show');
		});
		$(".CMEDATE").click(function (event) {
			$('#CMEDATE').datetimepicker('show');
		});
		jQuery('.datetimepicker').datetimepicker({
			timepicker: false,
			closeOnDateSelect: true,
			scrollMonth: false,
			scrollInput: false,
			format: 'Y/m/d',
			lang: '${transfer}'
		});
	}	
	

	//選項
	 	function formReset() {
	 		//打開驗證隱藏欄位
			$("#hideblock_CheckDateScope").show();
	 		if(!$("#formId").validationEngine("validate")){
				e = e || window.event;//forIE
				e.preventDefault();
			}
	 		else{
// 		 		initBlockUI();
				if ($('#actionBar').val()=="excel"){
					$("#downloadType").val("OLDEXCEL");
					$("#templatePath").val("/downloadTemplate/f_demand_deposit.xls");
		 		}else if ($('#actionBar').val()=="txt"){
					$("#downloadType").val("TXT");
					$("#templatePath").val("/downloadTemplate/f_demand_deposit.txt");
		 		}else if ($('#actionBar').val()=="oldtxt"){
					$("#downloadType").val("OLDTXT");
					$("#templatePath").val("/downloadTemplate/f_demand_depositOLD.txt");
		 		}
                $("#formId").attr("target", "");
                $("#formId").attr("action", "${__ctx}/FCY/ACCT/f_demand_deposit_detail_directDownload");
                $("#formId").submit();
                $('#actionBar').val("");
	 		}
		}
	
 	</script>
</body>
</html>
