<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function () {
			window.print();
		});
	</script>
</head>

<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br /><br />
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif" /></div>
	<br /><br />
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<br />
	<div>
		<!-- 查詢時間 -->
		<p>
			<spring:message code="LB.Inquiry_time" /> : ${CMQTIME}

		</p>
		<!-- 查詢期間 -->
		<p>
			<spring:message code="LB.Inquiry_period" /> : ${CMPERIOD}
		</p>
		<!-- 資料總數 : -->
		<p>
			<spring:message code="LB.Total_records" /> : ${CMRECNUM}
			<!--筆 -->
			<spring:message code="LB.Rows" />
		</p>
		
	
			<table class="print">

				<tr style="text-align: center">
					<!-- 通知編號-->
					<td class="text-center">
						<spring:message code="LB.W0166" />
					</td>
					<!-- 信用狀號碼 -->
					<td class="text-center">
						<spring:message code="LB.L/C_no" />/<spring:message code="LB.W0167" />
					</td>
					<!--幣別-->
					<td class="text-center">
						<spring:message code="LB.Currency" />
					</td>
					<!--到單金額 -->
					<td class="text-right">
						<spring:message code="LB.W0169" />
					</td>
					<!-- 融資利率 -->
					<td class="text-right">
						<spring:message code="LB.X0010" />
					</td>
					<!-- 融資起日/融資迄日-->
					<td class="text-center">
						<spring:message code="LB.W0170" />/<spring:message code="LB.W0171" />
					</td>
					<!-- 承兌到期日-->
					<td class="text-center">
						<spring:message code="LB.W0172" />
					</td>
					<!-- 開狀金額 -->
					<td class="text-right">
						<spring:message code="LB.W0094" />
					</td>
					<!-- 受益人 -->
					<td class="text-center">
						<spring:message code="LB.W0174" />
					</td>
					<!-- 備註 -->
					<td class="text-center">
						<spring:message code="LB.Note"/>
					</td>
					

				</tr>
				<c:forEach var="labelListMap" items="${print_datalistmap_data}">
					<tr>
						<td>${labelListMap.RIBDATE }</td>
					    <td>${labelListMap.RLCNO }<br>${labelListMap.RIBNO }</td>
					    <td>${labelListMap.RBILLCCY}</td>
					    <td>${labelListMap.RBILLAMT}</td>
					    <td>${labelListMap.RADVFIXR}</td>
					    <td>${labelListMap.RINTSTDT}<br>${labelListMap.RINTDUDT}</td>
					    <td>${labelListMap.RCFMDATE}</td>
					    <td>${labelListMap.RLCAMT }</td>
					    <td>${labelListMap.RBENNAME}</td>
					    <td>${labelListMap.RMARK}</td>
					</tr>
				</c:forEach>
			</table>
	</div>
</body>

</html>