<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>

 <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
    <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 10);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 20);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
	});
	
	// 畫面初始化
	function init() {
		initFootable();
		// 確認鍵 click
		submit();
		
		$("#formId").validationEngine({
			binded : false,
			promptPosition : "inline"
		});
		
	}
	
	// 驗證下拉選單
	function validate_DPAGACNO(field, rules, i, options) {
		var inputAttr = rules[i + 2];
		console.log("inputAttr>>" + inputAttr);
		var dpagacno_val = $("#" + inputAttr).find(":selected").val()
		console.log("funccall test");
		console.log("funccall test>>" + options.allrules.required.alertText);
		if (fstop.isEmptyString(dpagacno_val) || dpagacno_val.indexOf('#') > -1) {
			return options.allrules.required.alertText
		}
	}
	// 確認鍵 Click
	function submit() {
		$("#CMSUBMIT").click( function(e) {
			console.log("submit~~");
			if (!$('#formId').validationEngine('validate')) {
				e.preventDefault();
			} else {
			// 遮罩
         	initBlockUI();$("#formId").validationEngine('detach');
            $("#formId").attr("action","${__ctx}/CREDIT/APPLY/cash_pw_resend_confirm");
	 	  	$("#formId").submit();
			}
		});
	}
	
</script>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 信用卡     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Credit_Card" /></li>
    <!-- 密碼函補寄     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1647" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.W1647" /><!-- 申請補寄預借現金密碼 --></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
                <form id="formId" method="post" action="">
                <input type="hidden" id="TOKEN" name="TOKEN" value="${sessionScope.transfer_confirm_token}">
                <div class="main-content-block row">
                    <div class="col-12 tab-content">
                        <div class="ttb-input-block">

                            <!--卡號-->
                            <div class="ttb-input-item row">
                                <span class="input-title"><label>
                                    <h4><spring:message code="LB.Card_number" /><!-- 卡號 --></h4>
                                    </label></span>
                                <span class="input-block">
                                    <div class="ttb-input">
                                        <select class="custom-select select-input half-input validate[required ,funcCall[validate_DPAGACNO[DPAGACNO]]]" name="CARDNUM" id="DPAGACNO" >
                                            <option value="#" selected>------ <spring:message code="LB.Card_number" /><!-- 卡號  --> ------</option>
                                        <c:forEach var="dataList" items="${cash_pw_resend.data.SELECTLIST}" >
											<option value="${dataList.VALUE}">${dataList.TEXT}</option>
										</c:forEach>
                                        </select>
                                    </div>
                                </span>
                            </div>
						</div>
						<input class="ttb-button btn-flat-gray" name="CMRESET" type="reset" value="<spring:message code="LB.Re_enter" />" /><!-- 重新輸入 -->
						<input class="ttb-button btn-flat-orange" id="CMSUBMIT" name="CMSUBMIT" type="button" value="<spring:message code="LB.Confirm" />" /><!-- 確定 -->
                    </div>
                </div>
				</form>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>