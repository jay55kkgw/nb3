<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js_u2.jsp"%>
<!--舊版驗證-->
<script type="text/javascript" src="${__ctx}/js/checkutil_old_${pageContext.response.locale}.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	//HTML載入完成後開始遮罩
	setTimeout("initBlockUI()",100);
	//開始查詢資料並完成畫面
	setTimeout("init()",200);
	//解遮罩
	setTimeout("unBlockUI(initBlockId)",500);
});
function init(){
	$("#CMSUBMIT").click(function(){
		
		$("#formID").attr("action","${__ctx}/INDEX/index");
		$("#formID").submit();	
	
	});
	
// 	var BONDDATA = jQuery.parseJSON('${bond_purchase_confirm.data.BONDDATA}');
// 	$('#BONDNAME').html(BONDDATA.BONDNAME);
// 	$('#BONDCRY').html(BONDDATA.BONDCRY);

// 	$('#ALERTMSG').html("最低委買面額："+ BONDDATA.MINBPRICE_FMT + "<br>累進金額：" + BONDDATA.PROGRESSIVEBPRICE_FMT);
}

</script>
</head>
<body>
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 海外債券     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2391" /></li>
    <!-- 海外債券交易     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X2515" /></li>
    <!-- 單筆申購     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.X2516" /></li>
		</ol>
	</nav>

	<!--左邊menu及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
	<!--快速選單及主頁內容-->
		<main class="col-12">
			<!--主頁內容-->
			<section id="main-content" class="container">
<!-- 			海外債券申購交易結果 -->
				<h2><spring:message code="LB.X2563" /></h2>
				<i class="fa fa-star" style="font-size:1.5rem;color:#ed6d00;"></i>
					<form id="formID" method="post">
					<input type="hidden" id="TOKEN" name = "TOKEN" value="${sessionScope.transfer_confirm_token}">
					<div class="main-content-block row">
						<div class="col-12 tab-content">
							<div class="ttb-input-block">
								
								<div class="ttb-message">
<!-- 									申購成功 -->
									<span><spring:message code="LB.X2545" /></span>
								</div>
								
								<!-- 交易時間 -->
								<div class="ttb-input-item row">
									<span class="input-title"><label>
											<h4>
												<spring:message code="LB.Trading_time" />
											</h4>
									</label></span> <span class="input-block"><div class="ttb-input">
											<span> ${bond_purchase_result.data.CMQTIME } </span>
										</div></span>
								</div>
								
								<!-- 客戶姓名 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1066" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${bond_purchase_result.data.hiddenNAME}</span>
                                		</div>
                               		</span>
                            	</div>
								<!-- 客戶投資屬性 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1067" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${bond_purchase_result.data.FDINVTYPE_SHOW} </span>
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 外幣扣帳帳號 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.X2517" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${bond_purchase_result.data.ACN2}</span>
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 債券名稱 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1012" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<spa>${bond_purchase_result.data.BONDNAME}</span>
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 投資幣別 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W0908" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                        	<span>${bond_purchase_result.data.BONDCRY_SHOW}</span>
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 委託買價 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.X2518" />((A)：</h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                			<span>${bond_purchase_result.data.BUYPRICE_FMT}%</span>
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 委買面額 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.X2511" />(B)：</h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                			<span>${bond_purchase_result.data.AMOUNT_FMT} <spring:message code="LB.Dollar" /></span> 
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 前手利息 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.X2533" />(C)：</h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                			<span>
                                			<c:if test="${bond_purchase_result.data.O08 == '-'}">
                                			- 
                                			</c:if>
                                			${bond_purchase_result.data.O09_FMT} <spring:message code="LB.Dollar" />
                                			</span> 
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 手續費率 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W1034" />：</h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                			<span>${bond_purchase_result.data.O10_FMT}%</span> 
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 預估手續費 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.X2546" />(D)：</h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                			<span>${bond_purchase_result.data.BONDCRY_SHOW} ${bond_purchase_result.data.O11_FMT} <spring:message code="LB.Dollar" /></span> 
                                		</div>
                               		</span>
                            	</div>
                            	<!-- 預估圈存金額 -->
								<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.X2547" />：</h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                			<span>${bond_purchase_result.data.BONDCRY_SHOW} ${bond_purchase_result.data.O12_FMT} <spring:message code="LB.Dollar" /><br>(A*B+C+D)</span> 
                                		</div>
                               		</span>
                            	</div>
                            	<div class="ttb-input-item row">
                                	<span class="input-title"><label>
                                        <h4><spring:message code="LB.W0944" /></h4>
                                    </label></span>
                                	<span class="input-block">
                                		<div class="ttb-input">
                                			<span>${bond_purchase_result.data.O17}</span> 
                                		</div>
                               		</span>
                            	</div>
                            </div>
						</div>
					</div>	
				</form>
			</section>
		</main>
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>