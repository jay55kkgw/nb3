<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
   <script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">    
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<link rel="stylesheet" href="../css/tbb_common.css">
</head>
 <body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 存款餘額證明申請     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0485" /></li>
		</ol>
	</nav>

	
	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
	<main class="col-12"> 
	<!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<h2><spring:message code="LB.D0485" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>					
			<form method="post" id="formId">
				
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
							
							<div class="ttb-message">
                                <span><spring:message code="LB.D0486" /></span>
                            </div>							
							<!-- 帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
								<h4><spring:message code="LB.D0487" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<select name="ACN" id="ACN" onchange="getACNO_Data()" class="custom-select select-input half-input validate[required]">
											<c:forEach var="AcnList" items="${deposit_balance_proof.data.REC1 }">
												<option>${AcnList.ACN}</option>
											</c:forEach>
										</select>
									</div>
								</span>
							</div>
							
							
							<!-- 身份證/營利事業統一編號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> <label>
								<h4><spring:message code="LB.D0488" />/<spring:message code="LB.D0489" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										${deposit_balance_proof.data.CUSIDN}
									</div>
								</span>
							</div>								
							
							<!-- 申請份數 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0490" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text" maxLength="3" value="1" id="COPY" name="COPY" onblur="countFee()" class="text-input validate[required, numcheck[COPY]]">
									</div>
								</span>
							</div>	
							 
							<!-- 申請用途 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
									<label>
										<h4><spring:message code="LB.D0491" /></h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text" id="PURPOSE1" name="PURPOSE1" class="text-input validate[required]" maxlength="60" placeholder="<spring:message code="LB.D0493" />" class="text-input">
									</div>
								</span>
							</div>
							 
							<div class="ttb-input-item row">
								<!--證明日期  -->
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.D0492" /></h4>
									</label>
								</span>
								<span class="input-block">
									<div class="ttb-input">										
										<!--期間起日 -->
										
											<input type="text" id="DATE1" name="DATE1" onblur="countFee()" class="text-input datetimepicker" maxlength="10" value="${deposit_balance_proof.data.TOMAROW}" /> 
											<span class="input-unit DATE1">
												<img src="${__ctx}/img/icon-7.svg" />
											</span>
											<br><span class="input-unit">（<spring:message code="LB.D0494" />）</span>										
											<!-- 不在畫面上顯示的span -->
											<span id="hideblocka" >
											<!-- 驗證用的input -->
												<input id="Monthly_DateA" name="Monthly_DateA" type="text" class="text-input validate[required,verification_date[Monthly_DateA],funcCall[validate_CheckDate['<spring:message code= "LB.D0492" />',DATE1,${deposit_balance_proof.data.nextTwoYearDay},${deposit_balance_proof.data.TOMAROW}]]]"
													style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />										
												<input type="hidden" id="TOMAROW" value="${deposit_balance_proof.data.TOMAROW}">
												<input type="hidden" id="nextTwoYearDay" value="${deposit_balance_proof.data.nextTwoYearDay}">
											</span>
									</div>
								</span>
							</div>
							
							<!-- 證明書種類 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0495" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
									<!-- 存款餘額證明 -->
									<label class="radio-block"> 
											<spring:message code="LB.D0496" /><input type="radio" value="1" name="INQ_TYPE" onclick = "$('#cuy').hide()" checked> <span class="ttb-radio"></span>
									</label>
									<!-- 部分餘額證明 -->
									<label class="radio-block"> 
											<spring:message code="LB.D0497" /><input type="radio" value="2" name="INQ_TYPE" onclick = "$('#cuy').show()"> <span class="ttb-radio"></span>
									</label>
<%-- 										<input type="radio" value="1" name="INQ_TYPE" checked><spring:message code="LB.D0496" /> --%>
<%--         								<input type="radio" value="2" name="INQ_TYPE"><spring:message code="LB.D0497" /> --%>
									</div>
								</span>
							</div>
							
							<!-- 幣別 -->
						
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.Currency" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<select id="CRY" name="CRY" onchange="CRYCHANGE()"class="custom-select select-input half-input validate[required]">
								         
								          <option name="FX_CRY" value="USD">USD</option>
								          <option name="FX_CRY" value="AUD">AUD</option>
								          <option name="FX_CRY" value="CAD">CAD</option>
								          <option name="FX_CRY" value="HKD">HKD</option>
								          <option name="FX_CRY" value="GBP">GBP</option>
								          <option name="FX_CRY" value="SGD">SGD</option>
								          <option name="FX_CRY" value="ZAR">ZAR</option>
								          <option name="FX_CRY" value="SEK">SEK</option>
								          <option name="FX_CRY" value="CHF">CHF</option>
								          <option name="FX_CRY" value="JPY">JPY</option>
								          <option name="FX_CRY" value="MYR">MYR</option>
								          <option name="FX_CRY" value="THB">THB</option>
								          <option name="FX_CRY" value="EUR">EUR</option>
								          <option name="FX_CRY" value="NZD">NZD</option>
								        
								          <option value="NTD" selected>NTD</option>
								       	</select>
								       		<div id="acnoIsShow">
											<span id = "showText" class="input-unit "></span>
									     	</div>
									</div>
								</span>
							</div>
								<div id="cuy" style="display: none" >
							<!-- 證明金額 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0499" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text" maxLength="12" placeholder="0" id="AMT" name="AMT" class="text-input validate[numcheck[AMT]]">
									    <span class="input-unit"><spring:message code="LB.Dollar" /></span>
									</div>
								</span>
							</div>
							</div>
							<!-- 提供文件版本 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0500" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<!-- 部分餘額證明 -->
										<label class="radio-block"> 
											<spring:message code="LB.D0394" />
												<input type="radio" value="1" name="VER" id="EngName('C')" onclick="changeField(this.value)"> 
													<span class="ttb-radio"></span>
										</label>
										<!-- 部分餘額證明 -->
										<label class="radio-block"> 
											<spring:message code="LB.D0396" />
												<input type="radio" value="2" name="VER" id="EngName('E')" onclick="changeField(this.value)"> 
													<span class="ttb-radio"></span>
										</label>
<%-- 										<input type="radio" value="1" name="VER" id="EngName('C')" onclick="changeField(this.value)"><spring:message code="LB.D0394" /> --%>
<%-- 										<input type="radio" value="2" name="VER" id="EngName('E')" onclick="changeField(this.value)"><spring:message code="LB.D0396" /> --%>
									</div>
								</span>
							</div>
							
							<!-- 英文戶名 -->
							<div id="engdiv" class="ttb-input-item row" style="display:none;">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.X0363" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input type="text" name="ENGNAME" id="ENGNAME" maxlength="30" value="" class="text-input">
									</div>
								</span>
							</div>
							
							<!--領取方式 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0503" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<!-- 本人親自至分行領取 -->
										<label class="radio-block"> 
											<spring:message code="LB.D0504" />
												<input type="radio" value="1" name="TAKE_TYPE" onclick="countFee()"> 
													<span class="ttb-radio"></span>
										</label>
									</div>
										<!-- 郵寄-郵寄地址(限國內地區)  -->
									<div class="ttb-input">
										<label class="radio-block"> 
											<spring:message code="LB.D0505" />
												<input type="radio" value="2" name="TAKE_TYPE" onclick="countFee()"> 
													<span class="ttb-radio"></span>
										</label>
									</div>
									<div class="ttb-input">
<%-- 										<input type="radio" value="1" name="TAKE_TYPE" onclick="countFee()"><spring:message code="LB.D0504" /><br/> --%>
<%-- 										<input type="radio" value="2" name="TAKE_TYPE" onclick="countFee()"><spring:message code="LB.D0505" /> --%>
										
										<input type="text" class="text-input" disabled="disabled" maxlength="66" size="40" name="MAILADDR" id = "MAILADDR">
									</div>
								</span>
							</div>
							
							<!--手續費扣帳帳號 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0506" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<select name="FEEACN" id="FEEACN" class="custom-select select-input half-input validate[required]">
											<c:forEach var="dataList" items="${deposit_balance_proof.data.REC2}">
												<option>${dataList.ACN}</option>
											</c:forEach>
										</select>
									</div>
								</span>
							</div>
							
							<!--手續費 -->
							<div class="ttb-input-item row">
								<span class="input-title"> 
								<label>
									<h4><spring:message code="LB.D0507" /></h4>
								</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<input class="text-input" type="text" maxLength="4" id="FEE" name="FEE" disabled="disabled" value="">
										<span class="input-unit"><spring:message code="LB.Dollar" /></span>
										<input type="hidden" id="FEAMT" name="FEAMT" value="">										
									</div>
								</span>
							</div>
							
						</div>
						<input id="reset" name="reset" type="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />" />	
						<input type="button" class="ttb-button btn-flat-orange" id="CMSUBMIT" value="<spring:message code="LB.Confirm" />"/>
					</div>
				</div>
				
				
					
					<ol class="list-decimal description-list">
						<p><spring:message code="LB.Description_of_page" /></p>
						<li><spring:message code="LB.Deposit_Balance_Proof_P1_D1" /></li>
						<li><spring:message code="LB.Deposit_Balance_Proof_P1_D2" /></li>
						<li><spring:message code="LB.Deposit_Balance_Proof_P1_D3" /></li>
						<li><spring:message code="LB.Deposit_Balance_Proof_P1_D4" /></li>
						<li><spring:message code="LB.Deposit_Balance_Proof_P1_D5" /></li>
					</ol>
				</div>
				
				</form>

<!-- 				</form> -->
			</section>
		<!-- 		main-content END -->
		</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
<!--   Js function -->
    <script type="text/JavaScript">
		$(document).ready(function() {
			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()", 50);
			// 開始跑下拉選單並完成畫面
			setTimeout("init()", 400);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)", 500);
			
		});
		
		function init(){
			
// 			getTmr();
			datetimepickerEvent();
			checkTimeRange();
			getACNO_Data();
			
			// 初始化時隱藏span
			$("#hideblocka").hide();
		
			$("#formId").validationEngine({
				binded: false,
				promptPosition: "inline"
			});			
			
			$("#CMSUBMIT").click(function(e){
				var submit = true;
				if(parseInt($("#AMT").val())>max_v && $('input[name=INQ_TYPE]:checked').val()=="2"){
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.X2222' />("+max_v+")"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
					e.preventDefault();
					submit = false;
				}
				if( parseInt($("#COPY").val())<=0 ){
					errorBlock(
							null, 
							null,
							["<spring:message code= "LB.X1482"/><spring:message code= "LB.D0490"/><spring:message code= "LB.X1483"/>"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
					e.preventDefault();
					submit = false;
				}
				
				//打開驗證隱藏欄位
				$("#hideblocka").show();
				//塞值進span內的input
				$("#Monthly_DateA").val($("#DATE1").val());
				
				e = e || window.event;
				console.log(e);
				if($("#AMT").val() == ""){
					$("#AMT").val("0");
				}
				
				
				
				//選項控制
				var VER1 = $('input[name="VER"][value="1"]').is(':checked');
				var VER2 = $('input[name="VER"][value="2"]').is(':checked');
				var TAKE_TYPE1 = $('input[name="TAKE_TYPE"][value="1"]').is(':checked');
				var TAKE_TYPE2 = $('input[name="TAKE_TYPE"][value="2"]').is(':checked');
				
				if(!VER1 && !VER2){
					//alert("<spring:message code="LB.Alert159" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert159' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
					e.preventDefault();
					submit = false;
				}else {
					if(VER2){				
						if($("#ENGNAME").val() == ""){
							//alert("<spring:message code="LB.Alert160" />");
							errorBlock(
								null, 
								null,
								["<spring:message code= 'LB.Alert160' />"], 
								'<spring:message code= "LB.Quit" />', 
								null
							);
							!$('#formId').validationEngine('validate');
							e.preventDefault();
							submit = false;
						}
					}
				}
				
				if(!TAKE_TYPE1 && !TAKE_TYPE2 && submit){
					//alert("<spring:message code="LB.Alert161" />");
					errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert161' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
					e.preventDefault();
					submit = false;
				}else {
					if(TAKE_TYPE2){
						if($("#MAILADDR").val() == ""){
							//alert("<spring:message code="LB.Alert162" />");
							errorBlock(
							null, 
							null,
							["<spring:message code= 'LB.Alert162' />"], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
							!$('#formId').validationEngine('validate');
							e.preventDefault();
							submit = false;
						}
					}
				}

				if(!$('#formId').validationEngine('validate')){
		        	e.preventDefault();
	 			}else if(submit){
	 				$("#formId").validationEngine('detach');
	 				initBlockUI();
	 				$("#formId").attr("action","${__ctx}/ONLINE/SERVING/deposit_balance_proof_confirm");
	 	  			$("#formId").submit(); 
	 			}
				
	  		});
		}
		
// 		function getTmr() {
// 			var today = new Date();
// 			today.setDate(today.getDate());
// 			var y = today.getFullYear();
// 			var m = today.getMonth() + 1;
// 			var d = today.getDate();
// 			if(m<10){
// 				m = "0"+m
// 			}
// 			if(d<10){
// 				d="0"+d
// 			}
// 			var tmr = y + "/" + m + "/" + d
// 			$('#DATE1').val(tmr);
// 		}
		
// 		日曆欄位參數設定
		function datetimepickerEvent(){

		    $(".DATE1").click(function(event) {
				$('#DATE1').datetimepicker('show');
			});
			
			jQuery('.datetimepicker').datetimepicker({
				timepicker:false,
				closeOnDateSelect : true,
				scrollMonth : false,
				scrollInput : false,
			 	format:'Y/m/d',
			 	lang: '${transfer}'
			 	
			});
		}
		
		//日期區間
		function checkTimeRange()
		{
			var now = Date.now();
			var dateT = new Date( $('#DATE1').val() );
			var twoYm = 63115200000;
			var oldtwoYm = twoYm - now;
			
			var distanceS = dateT - now;
			var distanceE = now - dateT;
			
			var limitS = new Date(now + twoYm);
			var limitE = new Date(now - twoYm);
			
			
			if(distanceS > twoYm){
				var m = limitS.getMonth() + 1;
				var time = limitS.getFullYear() + '/' + m + '/' + limitS.getDate();
				var msg = '<spring:message code= "LB.X1294" />' + time;
				//alert(msg);
				errorBlock(
						null, 
						null,
						[msg], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			}
			else if (distanceE < oldtwoYm){	
				var m = limitE.getMonth() + 1;
				var time = limitE.getFullYear() + '/' + m + '/' + limitE.getDate();
				var msg = '<spring:message code= "LB.X1295" />' + time;
				//alert(msg);
				errorBlock(
						null, 
						null,
						[msg], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
			}else{
				if(dateT == now){
					var msg = '<spring:message code= "LB.X1296" />';
	 				//alert(msg);
					errorBlock(
							null, 
							null,
							[msg], 
							'<spring:message code= "LB.Quit" />', 
							null
						);
	 			}
			}

		}
		var ADPIBALLIST = [];
		var FXCRY = false;//是否持有外幣
		//取出帳號餘額
		function getACNO_Data(){
			var acno = $("#ACN").val();
			uri = '${__ctx}'+"/FCY/COMMON/getACNO_Currency_Data_aj";
			rdata = {acno: acno,cry: "TWD"};
			rdata2 = {acno: acno,cry: "FX"};
			ADPIBALLIST = [];
			fstop.getServerDataEx(uri,rdata,false,ACNO_DATA_TWD);
			fstop.getServerDataEx(uri,rdata2,false,ACNO_DATA_FX);
			adjust_dom();
			CRYCHANGE();
		}
		
//		查台幣餘額
		function ACNO_DATA_TWD(data){
			if(data.data.accno_data){
				console.log("有台幣的帳號");
				ADPIBALLIST["NTD"]=data.data.accno_data.ADPIBAL;
			}
			else{
				ADPIBALLIST["NTD"]=null;
			}
		}
//     查外幣餘額
       function ACNO_DATA_FX(data){
	       if(data.data.accno_data && data.data.accno_data.length > 0){
	    	   nohaveFXCRY = true;	//持有外幣
	    	   console.log("有外幣的帳號");
	    	   data.data.accno_data.forEach(function(k,v){
	    		   ADPIBALLIST[k["CUID"]]=k["AVAILABLE"];
	    	   });
	       }
	       else{
	    	   nohaveFXCRY = false;//沒有外幣
	       }
       }
       //調整元件
       function adjust_dom(){
    	   if(!nohaveFXCRY){
    		   //未持有外幣
    		   $("#CRY").val("NTD");
    		   $('option[name$="FX_CRY"]' ).hide();
    	   }
    	   else{
    		   $('option[name$="FX_CRY"]' ).show();
    	   }
       }
		
		//手續費計算
		function countFee(){
			var fee = 0;
			var date = $("#DATE1").val();
			var dateT = new Date($("#DATE1").val());
			var now = Date.now();
			var distanceS = now - dateT;
			var distanceE = dateT - now;
			var OneD = 1000*60*60*24;
			distanceS = parseInt(distanceS/OneD);
			distanceE = parseInt(distanceE/OneD);
			var copy = $("#COPY").val();
			var fees = $("#FEE").val();
			var feamt = $("#FEAMT").val();
		  	if(copy == "" || date == "" || take_type == "")
		  	{
		  		$("#FEE").val(fee);
		  		$("#FEAMT").val(fee);
		  	}
		  	if((distanceS > 30) || (distanceE > 30)){
		  	    fee = 100;
		  	}else{
		  	    fee = 50;
		  	}
		  	var num = parseInt(copy);
		  	if(num > 1){
		  	    fee = fee + (num-1)*20;		  		
		  	}
		  	
			var take_type = $("input[name='TAKE_TYPE']")[1].checked;
			if(take_type){
			    $("#MAILADDR").prop("disabled", false);		    	
		  		 fee += 50;
		    }else{
			    $("#MAILADDR").prop("disabled", true);
		    }        	 
		  	$("#FEE").val(fee);
		  	$("#FEAMT").val(fee);
		}
		
		//切換顯示的欄位
		function changeField(str){
			if(str == "1"){
				$("#engdiv").hide();	
			}else{
				$("#engdiv").show();
			}
		}
		var max_v;
		function CRYCHANGE(){
			i18n['available_balance'] = '<spring:message code="LB.Available_balance" />: ';
			var c = $("#CRY").val();
			max_v = "0.00";
			if(ADPIBALLIST.hasOwnProperty(c)){
				max_v = ADPIBALLIST[c];
			}
			i18n['available_balance'] += fstop.formatAmt(max_v.split(".")[0])+"."+max_v.split(".")[1];
			$("#showText").html(i18n['available_balance']);
		}
		
 	</script>
</body>
</html>
