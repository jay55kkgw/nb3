<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp" %>
    <script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
</head>
 <body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
    <!-- 帳戶查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Account_Inquiry" /></li>
    <!-- 虛擬帳號入帳明細     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Virtual_Account_Detail" /></li>
		</ol>
	</nav>



	
	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
	<main class="col-12"> 
	<!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<h2><spring:message code="LB.Virtual_Account_Detail" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<!-- 下拉式選單-->
			<div class="print-block">
				<select class="minimal" id="actionBar" onchange="formReset()">
					<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 					下載Excel檔 -->
					<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
<!-- 					下載為txt檔 -->
					<option value="txt"><spring:message code="LB.Download_txt_file" /></option>
<!-- 					下載為舊txt檔 -->
					<option value="oldtxt"><spring:message code="LB.Download_oldversion_txt_file" /></option>
				</select>
			</div>	
			<c:if test="${virtual_account_detail_result.data.TOPMSG == 'OKOV'}">
				<div class="MessageBar"><spring:message code="LB.X0076" />
	       			<input id="CMCONTINUEQ" type="button" value="<spring:message code="LB.X0151" />" name="CMCONTINUEQ" class="ttb-sm-btn btn-flat-orange" onClick="processQuery()">
	       		</div>
	       	</c:if>					
			<form method="post" id="formId" action="${__ctx}/download">
				<!-- 下載用 -->
				<input type="hidden" name="downloadFileName" value="LB.Virtual_Account_Detail"/>
				<input type="hidden" name="downloadType" id="downloadType"/> 					
				<input type="hidden" name="templatePath" id="templatePath"/> 
				<input type="hidden" name="CMQTIME" value="${virtual_account_detail_result.data.CMQTIME}"/>
				<input type="hidden" name="ACN" value="${virtual_account_detail_result.data.ACN}"/>
				<input type="hidden" name="CMPERIOD" value="${virtual_account_detail_result.data.CMPERIOD}"/>
				<input type="hidden" name="CMRECNUM" value="${virtual_account_detail_result.data.REC.size()}"/>
				<!-- EXCEL下載用 -->
				<input type="hidden" name="headerRightEnd" value="5" />
				<input type="hidden" name="headerBottomEnd" value="7" />
				<input type="hidden" name="rowStartIndex" value="8"/>
				<input type="hidden" name="rowRightEnd" value="5"/>
				<!-- TXT下載用 -->
				<input type="hidden" name="txtHeaderBottomEnd" value="10"/>
				<input type="hidden" name="txtHasRowDataBoolean" value="true"/>
				<input type="hidden" name="txtHasRowData" value="true"/>
				<input type="hidden" name="txtHasFooter" value="false"/>
				<!-- 顯示區  -->
				<div class="main-content-block row">
					<div class="col-12 tab-content">						
						<ul class="ttb-result-list">
                        	<li>
                            	<!-- 查詢時間 -->
								<h3><spring:message code="LB.Inquiry_time" /> :</h3>
								<p>
                                	${virtual_account_detail_result.data.CMQTIME }
                               	</p>
                          	</li>
                          	<li>
                                <!-- 帳號 -->
                                <h3><spring:message code="LB.Account" /> :</h3>
								<p> 
									${virtual_account_detail_result.data.ACN }
                               	</p>
                            </li>
                            <li>
                                <!-- 查詢期間 -->
                               	<h3><spring:message code="LB.Inquiry_period" /> :</h3>
								<p>	
                                    ${virtual_account_detail_result.data.CMPERIOD }
                               	</p>
                          	</li>
                          	<li>
                                <!-- 資料總數  -->
                                <h3><spring:message code="LB.Total_records" /> :</h3>
								<p>
                                	${virtual_account_detail_result.data.REC.size() }
                                    <!--筆 -->
                                    <spring:message code="LB.Rows" />
                              	</p>
                          	</li>
                    	</ul>
                    	<!-- 表格區塊 -->
                        <table class="stripe table-striped ttb-table dtable" data-show-toggle="first">
                        	<thead>
                            	<tr>
                                <!-- 交易日期 -->
                                	<th>
                                    	<spring:message code="LB.Transaction_date" />
                                   	</th>
                                    <!-- 摘要 -->
                                    <th>
                                    	<spring:message code="LB.Summary_1" />
                                    </th>
                                    <!-- 金額 -->
                                    <th>
                                   		<spring:message code="LB.Deposit_amount_1" />
                                    </th>
                                    <!-- 入帳時間 -->
                                    <th>
                                    	<spring:message code="LB.Account_entry_time" />
                                    </th>
                                    <!-- 虛擬帳號 -->
                                    <th>
                                    	<spring:message code="LB.Virtual_account" />
                                    </th>
                                    <!-- 備註  -->
                                    <th>
                                    	<spring:message code="LB.Note" />
                                    </th>
                            	</tr>
                    		</thead>
                            <tbody>
                            	<c:forEach var="dataList" items="${virtual_account_detail_result.data.REC }">
                                	<tr>
                                    	<!-- 交易日期 -->
                                        <td class="text-center">${dataList.LSTLTD_c }</td>
                                        <!-- 摘要 -->
                                        <td class="text-center">${dataList.MEMO_C }</td>
                                        <!-- 金額 -->
                                        <td class="text-right">${dataList.AMTTRN_c }</td>
                                        <!-- 入帳時間 -->
                                        <td class="text-center">${dataList.LSTIME_c }</td>
                                        <!-- 虛擬帳號 -->
                                        <td class="text-center">${dataList.DATA25 }</td>
                                        <!-- 備註  -->
                                        <td class="text-center">${dataList.TRNSRC_C }</td>
                                  	</tr>
                            	</c:forEach>
                    		</tbody>
                  		</table>
                  		<input id="previous" name="previous" type="button" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />" />
                  		<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
					</div>
				</div>
				</form>
				<form method="post" id="reRec">
					<input type="hidden" id="ACN" name="ACN" value="${virtual_account_detail_result.data.ACN}">
					<input type="hidden" id="CMSDATE" name="CMSDATE" value="${virtual_account_detail_result.data.CMSDATE}">
					<input type="hidden" id="CMEDATE" name="CMEDATE" value="${virtual_account_detail_result.data.CMEDATE}">
					<input type="hidden" id="USERDATA_X100" name="USERDATA_X100" value="${virtual_account_detail_result.data.USERDATA_X50}">
				</form>
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>
					<li><span><spring:message code="LB.Demand_Virtual_detail_P2_D1" /></span></li>
				</ol>

<!-- 				</form> -->
			</section>
		<!-- 		main-content END -->
		</main>
	</div>

    <%@ include file="../index/footer.jsp"%>
<!--   Js function -->
    <script type="text/JavaScript">
		$(document).ready(function() {
			setTimeout("initDataTable()",100);

			// HTML載入完成後開始遮罩
			setTimeout("initBlockUI()",10);
			// 開始查詢資料並完成畫面
			setTimeout("init()",20);
			// 解遮罩
			setTimeout("unBlockUI(initBlockId)",500);
		});
		function init(){
			//initFootable();
			
			//上一頁按鈕
			$("#previous").click(function() {
				initBlockUI();
				fstop.getPage('${pageContext.request.contextPath}'+'/NT/ACCT/virtual_account_detail','', '');
			});
			
			$("#printbtn").click(function(){
				var params = {
						"jspTemplateName":"virtual_account_detail_result_print",
						"jspTitle":"<spring:message code='LB.Virtual_Account_Detail' />",
						"CMQTIME":"${virtual_account_detail_result.data.CMQTIME}",
						"ACN":"${virtual_account_detail_result.data.ACN}",
						"CMPERIOD":"${virtual_account_detail_result.data.CMPERIOD}",
						"CMRECNUM":"${virtual_account_detail_result.data.REC.size()}"
					};
					openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
				});							
		}
		
		//選項
		 	function formReset() {
		 		initBlockUI();
				if ($('#actionBar').val()=="excel"){
					$("#downloadType").val("OLDEXCEL");
					$("#templatePath").val("/downloadTemplate/virtual_account_detail.xls");
		 		}else if ($('#actionBar').val()=="txt"){
					$("#downloadType").val("TXT");
					$("#templatePath").val("/downloadTemplate/virtual_account_detail.txt");
		 		}else if ($('#actionBar').val()=="oldtxt"){
					$("#downloadType").val("OLDTXT");
					$("#templatePath").val("/downloadTemplate/virtual_account_detailOLD.txt");
		 		}
				ajaxDownload("${__ctx}/ajaxDownload","formId","finishAjaxDownload()");
			}
			function finishAjaxDownload(){
				$("#actionBar").val("");
				unBlockUI(initBlockId);
			}
			function processQuery(){
 				$("#reRec").attr("action","${__ctx}/NT/ACCT/virtual_account_detail_result");
 	  			$("#reRec").submit();
			}
		
 	</script>
</body>
</html>
