<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title></title>

	<Script language="JavaScript">
		//列印不顯示按鈕
		function print_the_page() {
			//先回到頂端再列印
			document.body.scrollTop = document.documentElement.scrollTop = 0;
			document.getElementById("CMPRINT").style.visibility = "hidden";    //顯示按鈕
			javascript: print();
			document.getElementById("CMPRINT").style.visibility = "visible";   //不顯示按鈕
		}
	</Script>

	<style>
		.watermark.zin:before {
			z-index: -1;
		}
		table.DataBorder {
		    width: 100%;
		}
		table.DataBorder,
		table.DataBorder th,
		table.DataBorder td {
		    padding: 7px;
		    border: 1px groove #bfbfbf;
		}
	</style>

</head>

<body class="bodymargin watermark zin" style="-webkit-print-color-adjust: exact">

	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">


		</font>
	</div>
	<br />
	
	${fxcertpreview.data}

	<br />

	<div style="text-align:center">
		<spring:message code="LB.X1465" />
	</div>

	<div style="text-align:center">
		<input type="button" class="ttb-button btn-flat-orange" style="z-index:10000" id="CMPRINT"
			value="<spring:message code="LB.Print_thepage" />" onclick="print_the_page()" />
	</div>
	<br /><br /><br /><br />
</body>

</html>