<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
</head>
<body>

	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- IDGATE 申請   結果頁 -->
			<li class="ttb-breadcrumb-item active" aria-current="page">註銷裝置</li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">

		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> <!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<form method="post" name="formId" id="formId" action="${__ctx}/ONLINE/SERVING/idgate">
				<h2>
					註銷裝置
				</h2>
	
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<div class="ttb-input-block">
							<div class="ttb-message">
								<span> 註銷裝置成功 </span>
							</div>
						
	
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4>
											裝置名稱
										</h4>
									</label>
								</span> 
								<span class="input-block">
									<div class="ttb-input">
										<span> ${idGate_cancel.DEVICENAME}  </span>
									</div>
								</span>
							</div>
	
						</div><!-- 					ttb-input-block END -->
						<input type="button" id="pageback" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_function_home_page" />" />
					</div><!-- 				col-12 tab-content END -->
				</div><!-- 			main-content-block row  END-->
			</form>
		</section><!-- 		main-content END --> 
		
		</main>
	</div>
	<!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>

	<script type="text/javascript">
			$(document).ready(function(){
				$("#pageback").click(function(e) {
					$("#formId").attr("action","${__ctx}/ONLINE/SERVING/idgate");
					$("#formId").submit();
				});
			});
		</script>

</body>
</html>