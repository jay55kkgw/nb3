<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
	<title>${jspTitle}</title>
	<script type="text/javascript">
		$(document).ready(function () {
			window.print();
			
		});
	</script>
    <style>
    .first-title {
    max-width: 15%;
    min-width: 15%;
    width: 15%;
}
    .second-title {
    max-width: 20%;
    min-width: 20%;
    width: 20%;
}
    </style>
</head>

<body class="bodymargin watermark">
	<br /><br />
	<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif" /></div>
	<br /><br />
	<div style="text-align:center">
		<font style="font-weight:bold;font-size:1.2em">${jspTitle}</font>
	</div>
	<br />
	<div>
        <p>
        	<spring:message code="LB.Inquiry_time" /> :
            ${CMQTIME}
        </p>
        <!-- 查詢期間 -->
        <p>
            <spring:message code="LB.Inquiry_period" /> :
            ${CMPERIOD}
        </p>
        <!-- 資料總數 : -->
        <p>
            <spring:message code="LB.Total_records" /> :
            ${CMRECNUM}
            <!--筆 -->
        	<spring:message code="LB.Rows" />
        </p>
        
            <!-- 表格區塊 -->
            <c:forEach var="dataTable" items="${print_datalistmap_data}">
            <table class="print">
            	<thead>
                	<tr>
                    	<th>
                        	<spring:message code="LB.Account" />
                        </th>
                        <th colspan="8">${dataTable.ACN }</th>
                   	</tr>
                    <tr>
                    	<!--日期-->
                        <th class="first-title"><spring:message code="LB.W1155" /></th>
                        <!-- 摘要 -->
                        <th class="second-title"><spring:message code="LB.Summary_1" /></th>
                        <!--支出(公克)-->
                        <th><spring:message code="LB.W0008" />(<spring:message code="LB.W1435" />)</th>
                        <!--存入(公克) -->
                        <th><spring:message code="LB.W1457" />(<spring:message code="LB.W1435" />)</th>
                        <!-- 結存(公克) -->
                        <th><spring:message code="LB.W0010" />(<spring:message code="LB.W1435" />)</th>
                        <!-- 單價(元) -->
                        <th><spring:message code="LB.W1460" />(<spring:message code="LB.Dollar" />)</th>
                        <!-- 交易金額(元) -->
                        <th><spring:message code="LB.W0016" />(<spring:message code="LB.Dollar" />)</th>
                        <!-- 交易時間 -->
                        <th><spring:message code="LB.Trading_time" /></th>
                    </tr>
                </thead>
                <tbody>
                	<c:forEach var="dataList" items="${dataTable.TABLE}">
                    <tr>
                    	<!-- 日期-->
                        <td  class="text-center">${dataList.LSTLTD }</td>
                        <!-- 摘要 -->
                        <td style="text-align: center">${dataList.MEMO }</td>
                        <!-- 支出-->
                        <td style="text-align: right">${dataList.DPWDAMT }</td>
                        <!-- 存入 -->
                        <td style="text-align: right">${dataList.DPSVAMT }</td>
                        <!-- 結存 -->
                        <td style="text-align: right">${dataList.GDBAL }</td>
                        <!-- 單價 -->
                        <td style="text-align: right">${dataList.PRICE }</td>
                        <!-- 交易金額 -->
                        <td style="text-align: right">${dataList.TRNAMT }</td>
                        <!--交易時間 -->
                       	<td>${dataList.TIME }</td>
                   	</tr>
                    </c:forEach>
                </tbody>
           	</table>
           	<br>
           	<br>
            </c:forEach>
			<br /><br />	
			
<!-- 			<div class="text-left"> -->
<!-- 				說明： -->
<%-- 				<spring:message code="LB.Description_of_page" />: --%>
			
<!-- 			</div>	 -->
	</div>
</body>

</html>