<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
</head>
<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 貸款服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Loan_Service" /></li>
    <!-- 借款查詢     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Loan_Inquiry" /></li>
    <!-- 已繳款本息查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.Paid_principal_and_interest_inquiry" /></li>
		</ol>
	</nav>

	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
	<!-- 	快速選單及主頁內容 -->
	<main class="col-12"> 
		<!-- 		主頁內容  -->
		<section id="main-content" class="container">
			<h2><spring:message code="LB.Paid_principal_and_interest_inquiry" /></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>

			<div class="main-content-block row">
				<div class="col-12">
					<ul class="ttb-result-list">
						<li>
							<!-- 查詢時間 -->
							<h3>
								<spring:message code="LB.Inquiry_time" />
							</h3>
							<p>
								${paid_query_result.data.CMQTIME}
							</p>
						</li>
						<li>
							<!-- 查詢期間 -->
							<h3>
							<spring:message code="LB.Inquiry_period_1" />
							</h3>
							<p>
							${paid_query_result.data.CMPERIOD} 
							</p>
						</li>
						<li>
							<!-- 查詢筆數 -->
							<h3>
							<spring:message code="LB.Total_records" />
							</h3>
							<p>
							${paid_query_result.data.CMRECNUM} <spring:message code="LB.Rows" />
							</p>							
						</li>
					</ul>
					<ul class="ttb-result-list">
						<li>
							<!-- 表名-->
							<h3>
								<spring:message code="LB.Report_name" />
							</h3>
							<p>
								<spring:message code="LB.Paid_principal_and_interest_TWD" />
							</p>
						</li>
					</ul>
					<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
					<thead>
						<tr>
							<th data-title='<spring:message code="LB.Account"/>'><spring:message code="LB.Account" /></th>
							<th data-title='<spring:message code="LB.Seq_of_account"/>'><spring:message code="LB.Seq_of_account" /></th><!-- 分號 -->
							<th data-title='<spring:message code="LB.Pay_Day"/>' data-breakpoints="xs sm"><spring:message code="LB.Pay_Day" /></th><!-- 繳款日期 -->
							<th data-title='<spring:message code="LB.Principal"/>' data-breakpoints="xs sm"><spring:message code="LB.Principal" /></th><!-- 本金 -->
							<th data-title='<spring:message code="LB.Interest"/>' data-breakpoints="xs sm"><spring:message code="LB.Interest" /></th><!-- 利息 -->
							<th data-title='<spring:message code="LB.Interest_payment_after_the_change"/>' data-breakpoints="xs sm"><spring:message code="LB.Interest_payment_after_the_change" /></th><!-- 異動後繳息迄日 -->
							<th data-title='<spring:message code="LB.Current_loan_balance"/>' data-breakpoints="xs sm"><spring:message code="LB.Current_loan_balance" /></th><!-- 當期借款餘額 -->
						</tr>
					</thead>
					<tbody>
					<c:if test="${paid_query_result.data.TOPMSG_016 eq 'OKLR'}">
						<c:forEach var="dataList" items="${paid_query_result.data.TW}">
							<tr>
								<td class="text-center">${dataList.ACN }</td>
								<td class="text-center">${dataList.SEQ }</td>
								<td class="text-center">${dataList.DATTRN}</td>
								<td class="text-right">${dataList.TRNPAL}</td>
								<td class="text-right">${dataList.TRNINT}</td>
								<td class="text-center">${dataList.CURIPD}</td>
								<td class="text-right">${dataList.BAL}</td>
							</tr>
						</c:forEach>
					</c:if>
					<c:if test="${paid_query_result.data.TOPMSG_016 != 'OKLR'}">
							<tr>
								<td class="text-center">${paid_query_result.data.TOPMSG_016}</td>
								<td class="text-center">${paid_query_result.data.errorMsg016}</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
					</c:if>
					</tbody>
					</table>
					<ul class="ttb-result-list">
						<li>
							<!-- 表名-->
							<h3>
								<spring:message code="LB.Report_name" />
							</h3>
							<p>
								<spring:message code="LB.Paid_principal_and_interest_FX" />
							</p>
						</li>
					</ul>
					<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
					<thead>
						<tr>
							<th data-title='<spring:message code="LB.Account"/>'><spring:message code="LB.Account" /></th>
							<th data-title='<spring:message code="LB.Transaction_Number"/>'><spring:message code="LB.Transaction_Number" /></th><!-- 交易編號 -->
							<th data-title='<spring:message code="LB.Principal_Currency"/>' data-breakpoints="xs sm"><spring:message code="LB.Principal_Currency" /></th><!-- 本金幣別 -->
							<th data-title='<spring:message code="LB.Principal"/>' data-breakpoints="xs sm"><spring:message code="LB.Principal" /></th><!-- 本金 -->
							<th data-title='<spring:message code="LB.Interest_Currency"/>' data-breakpoints="xs sm"><spring:message code="LB.Interest_Currency" /></th><!-- 利息幣別 -->
							<th data-title='<spring:message code="LB.Interest"/>' data-breakpoints="xs sm"><spring:message code="LB.Interest" /></th><!-- 利息 -->
							<th data-title='<spring:message code="LB.Interest_payment_after_the_change"/>' data-breakpoints="xs sm"><spring:message code="LB.Interest_payment_after_the_change" /></th><!-- 異動後繳息迄日 -->
							<th data-title='<spring:message code="LB.Data_date"/>' data-breakpoints="xs sm"><spring:message code="LB.Data_date" /></th><!-- 資料日期 -->
						</tr>
					</thead>
					<tbody>
					<c:if test="${paid_query_result.data.TOPMSG_554 eq 'OKLR'}">
						<c:forEach var="dataListB" items="${paid_query_result.data.FX}">
							<tr>
								<td class="text-center">${dataListB.LNACCNO}</td>
								<td class="text-center">${dataListB.LNREFNO}</td>
								<td class="text-center">${dataListB.CAPCCY}</td>
								<td class="text-right">${dataListB.CAPCOS}</td>
								<td class="text-center">${dataListB.INTCCY}</td>
								<td class="text-right">${dataListB.INTCOS}</td>
								<td class="text-center">${dataListB.CUSPYDT}</td>
								<td class="text-center">${dataListB.LNUPDATE}</td>
							</tr>
						</c:forEach>
					</c:if>
					<c:if test="${paid_query_result.data.TOPMSG_554 != 'OKLR'}">
							<tr>
								<td class="text-center">${paid_query_result.data.TOPMSG_554}</td>
								<td class="text-center" style="white-space">${paid_query_result.data.errorMsg554}</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
					</c:if>
					</tbody>
					</table>
						<input type="button" id="previous" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page" />" onclick="history.go(-1)"/>
						<input type="button" class="ttb-button btn-flat-orange" id="printbtn" value="<spring:message code="LB.Print" />"/>
				</div>
			</div>
					<ol class="description-list list-decimal">
						<p><spring:message code="LB.Description_of_page" /></p>
							<%-- <li><spring:message code="LB.Balance_query_P1_D1" /></li>
							<li><spring:message code="LB.Balance_query_P1_D2" /></li> --%>
							<li><spring:message code="LB.Paid_Query_P2_D1" /></li>
							<li><spring:message code="LB.Paid_Query_P2_D2" /></li>
							<li><spring:message code="LB.Paid_Query_P2_D3" /></li>
					</ol>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
	
			<script type="text/javascript">
			$(document).ready(function(){
				// HTML載入完成後開始遮罩
				setTimeout("initBlockUI()",10);
				// 開始查詢資料並完成畫面
				setTimeout("init()",20);
				// 解遮罩
				setTimeout("unBlockUI(initBlockId)",500);
							
			});
			
			function init(){
				
				initDataTable();
				
				$("#printbtn").click(function(){
					var params = {
							"jspTemplateName":"paid_query_result_print",
							"jspTitle":"<spring:message code='LB.Paid_principal_and_interest_inquiry'/>",
							"CMQTIME":"${paid_query_result.data.CMQTIME }",
							"COUNT":"${paid_query_result.data.CMRECNUM}",
							"CMPERIOD":"${paid_query_result.data.CMPERIOD}",
							"TOPMSG_016":"${paid_query_result.data.TOPMSG_016}",
							"TOPMSG_554":"${paid_query_result.data.TOPMSG_554}",
							"errorMsg016":"${paid_query_result.data.errorMsg016}",
							"errorMsg554":"${paid_query_result.data.errorMsg554}"
						};
					openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
				});
				
			}
		
		</script>
</body>
</html>