<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js_u2.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
<br/><br/>
<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
<br/><br/><br/>
<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
<br/><br/><br/>
<!-- 查詢時間 -->
<label><spring:message code="LB.Inquiry_time" /> ：</label><label>${CMQTIME}</label>
<br/><br/>
<!-- 查詢期間 -->
<label><spring:message code="LB.Inquiry_period_1" /> ：</label><label>${cmdate}</label>
<br/><br/>
<!-- 交易狀態-->
<label><spring:message code="LB.W0054" /> ：</label><label>${FGTXSTATUS}</label>
<br/><br/>
<!-- 資料總數 -->
<label><spring:message code="LB.Total_records" /> ：</label><label>${COUNT}</label> <spring:message code="LB.Rows" />
<br/><br/>
<table class="print">
			<thead>
				<tr>
		        	<th><spring:message code="LB.Report_name" /></th>
		        	<th colspan="9" class="DataCell"><spring:message code="LB.W0059" /></th>
		     	</tr>
			</thead>
			<thead>
			<tr>
				<!-- 預約編號 -->
				<th><spring:message code="LB.Booking_number" /></th>
				<!-- 轉帳日期 -->								
				<th><spring:message code="LB.Transfer_date" /></th>
				<!-- 轉出帳號 -->
				<th data-breakpoints="xs sm"><spring:message code="LB.Payers_account_no" /></th>
				<!-- 轉入帳號/繳費稅代號 -->
				<th data-breakpoints="xs sm"><spring:message code="LB.W0060" /></th>
				<!-- 轉帳金額 -->
				<th><spring:message code="LB.Amount" /></th>
				<!-- 手續費-->
				<th data-breakpoints="xs sm"><spring:message code="LB.D0507" /></th>
				<!-- 跨行序號 -->
				<th data-breakpoints="xs sm"><spring:message code="LB.W0062" /></th>
				<!-- 交易類別 -->
				<th data-breakpoints="xs sm"><spring:message code="LB.Transaction_type" /></th>
				<!-- 備註 -->
				<th data-breakpoints="xs sm"><spring:message code="LB.Note" /></th>
				<!-- 轉帳結果 -->
				<th><spring:message code="LB.W0063" /></th>
			</tr>
			</thead>	
		<c:forEach items="${dataListMap}" var="map">
			<tr>
				<td class="text-center">${map.DPSCHNO}</td>
				<td class="text-center">${map.DPSCHTXDATE}</td>
				<td class="text-center">${dataList.DPWDAC}</td>	                
				<td class="text-center">
					${map.DPSVBH}-
					<br>
					${map.DPSVAC}				
				</td>				          	               
				<td class="text-right">${map.DPTXAMT}</td>                
				<td class="text-right">${map.DPEFEE}</td>                
				<td class="text-center">${map.DPSTANNO}</td>
				<td class="text-center">${map.DPSEQ}</td>
				<td class="text-center">${map.DPTXMEMO}</td>
				<td class="text-center">
					${map.DPTXSTATUS}
					<br>
					${map.DPEXCODE}
				</td>
			</tr>
		</c:forEach>
</table>
<br/><br/>
	<!-- 說明 -->
	<div class="text-left">
		<spring:message code="LB.Description_of_page" />
		<ol class="list-decimal text-left">
			<li style="font-weight:bold; color:#FF0000;"><spring:message code="LB.Reservation_Trans_P2_D1" /></li>
			<li><spring:message code="LB.Reservation_Trans_P2_D2" /></li>
			<li><spring:message code="LB.Reservation_Trans_P2_D3" /></li>
		</ol>
	</div>
</body>
</html>