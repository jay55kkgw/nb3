<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ include file="../__import_js.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
	$(document).ready(function() {
		window.print();
	});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
	<br />
	<br />
	<div style="text-align: center">
		<img src="${pageContext.request.contextPath}/img/TBBLogo.gif" />
	</div>
	<br />
	<br />
	<br />
	<div style="text-align: center">
		<font style="font-weight: bold; font-size: 1.2em">${jspTitle}</font>
	</div>
	<br />
	<br />
	<br />
	<label><spring:message code="LB.Inquiry_time" />：</label>
	<label>${CMQTIME}</label>
	<br />
	<br />
	<label><spring:message code="LB.Total_records" />：</label>
	<label>${COUNT} <spring:message code="LB.Rows" /></label>
	<br />
	<br />
	<label><spring:message code="LB.Id_no" />:</label>
	<label>${hiddencusidn}</label>
	<br />
	<br />
	<label><spring:message code="LB.Name" />:</label>
	<label>${hiddenname}</label>
	<br />
	<br />
	<table class="print" data-toggle-column="first">
		
			

			<tr>
				<!-- 委託日期-->
				<td class="text-center" data-breakpoints="xs sm"><spring:message code="LB.W1022" /></td>
				<!-- 委託單號-->
				<td class="text-center" data-breakpoints="xs sm"><spring:message code="LB.W1023" /></td>
				<!-- 債券名稱 -->
				<td class="text-center" data-breakpoints="xs sm"><spring:message code="LB.W1012" /></td>
				<!-- 交易類別-->
				<td class="text-center" data-breakpoints="xs sm"><spring:message
						code="LB.Transaction_type" /></td>
				<!-- 委託單位/委託面額-->
				<td class="text-center" data-breakpoints="xs sm"><spring:message code="LB.W1025" /><br><spring:message code="LB.W1026" />
				</td>
				<!-- 委託價格/計價幣別 -->
				<td class="text-center" data-breakpoints="xs sm"><spring:message code="LB.W1027" /><br><spring:message code="LB.W0909" />
				</td>
				<!-- 成交價格/成交金額-->
				<td class="text-center" data-breakpoints="xs sm"><spring:message code="LB.W1030" /><br><spring:message code="LB.W1031" />
				</td>
				<!-- 成交價格/成交金額-->
				<td class="text-center"><spring:message code="LB.W1032" /><br><spring:message code="LB.W1033" />
				</td>
				<!-- 手續費率/手續費-->
				<td class="text-center"><spring:message code="LB.W1034" /><br><spring:message code="LB.D0507" />
				</td>
				<!--前手息/收付金額-->
				<td class="text-center"><spring:message code="LB.W1018" /><br><spring:message code="LB.W1037" />
				</td>
				<!-- 銀行帳號-->
				<td class="text-center"><spring:message code="LB.W1038" /></td>
			</tr>
		
			<c:forEach var="dataList" items="${dataListMap}">
				<tr>
					<!-- 委託日期-->
					<td class="text-center">${dataList.O01}</td>
					<!-- 委託單號-->
					<td class="text-center">${dataList.O03}</td>
					<!-- 債券名稱 -->
					<td class="text-center">${dataList.O05}</td>
					<!-- 交易類別-->
					<td class="text-center">${dataList.O06}</td>
					<!-- 委託單位/委託面額-->
					<td class="text-right">${dataList.O07}<br>${dataList.O08}</td>
					<!-- 委託價格/計價幣別 -->
					<td class="text-center">${dataList.O09}<br>${dataList.O18}</td>
					<!-- 成交價格/成交金額-->
					<td class="text-right">${dataList.O10}<br>${dataList.O11}</td>
					<!-- 成交價格/成交金額-->
					<td class="text-right">${dataList.O12}<br>${dataList.O13}</td>
					<!-- 手續費率/手續費-->
					<td class="text-right">${dataList.O14}<br>${dataList.O15}</td>
					<!--前手息/收付金額-->
					<td class="text-right">${dataList.O16}<br>${dataList.O17}</td>
					<!-- 銀行帳號-->
					<td class="text-center">${dataList.O19}</td>
				</tr>
			</c:forEach>
		
	</table>
	<br />
	<br />
</body>
</html>