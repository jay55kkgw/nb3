<!-- 客戶滿意度調查 -->
<section id="txncssslog" class="modal fade active show more-info-block"
	role="dialog" aria-labelledby="" aria-hidden="true" tabindex="-1"
	data-backdrop="static" fade>
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<!-- 客戶資料更新 -->
			<div class="modal-header">
				<p class="ttb-pup-header">
					<spring:message code='LB.X2402' />
				</p>
			</div>
			<div id="txncssslog-1">
				<div class="modal-body w-auto px-5">

					<p>
						<spring:message code='LB.X2396' />
					</p>

					<div class="ttb-input-block">
						<div id="CAREER1_DIV" class="ttb-input-item row">
							<span class="input-block">
								<div class="ttb-input">
									<input type="radio" id="r5" name="inlineRadioOptions" value="5" checked>
									&nbsp;
									<label for="r5"><spring:message code='LB.X2397' /></lable>
								</div>
							</span>
						</div>
						<div id="CAREER1_DIV" class="ttb-input-item row">
							<span class="input-block">
								<div class="ttb-input">
									<input type="radio" id="r4" name="inlineRadioOptions" value="4">
									&nbsp;
									<label for="r4"><spring:message code='LB.X2398' /></label>
								</div>
							</span>
						</div>
						<div id="CAREER1_DIV" class="ttb-input-item row">
							<span class="input-block">
								<div class="ttb-input">
									<input type="radio" id="r3" name="inlineRadioOptions" value="3">
									&nbsp;
									<label for="r3"><spring:message code='LB.X2399' /></label>
								</div>
							</span>
						</div>
						<div id="CAREER1_DIV" class="ttb-input-item row">
							<span class="input-block">
								<div class="ttb-input">
									<input type="radio" id="r2" name="inlineRadioOptions" value="2">
									&nbsp;
									<label for="r2"><spring:message code='LB.X2400' /></label>
								</div>
							</span>
						</div>
						<div id="CAREER1_DIV" class="ttb-input-item row">
							<span class="input-block">
								<div class="ttb-input">
									<input type="radio" id="r1" name="inlineRadioOptions" value="1">
									&nbsp;
									<label for="r1"><spring:message code='LB.X2401' /></label>
								</div>
							</span>
						</div>
						<div id="CAREER1_DIV" class="ttb-input-item row">
								<div class="ttb-input" id="MSGINPUT">
									<spring:message code='LB.X2587' />
									<textarea name="MSG" class="m-0 pl-5 MSG" id="MSG" maxlength="100" placeholder="<spring:message code="LB.X2588" />"></textarea>
								</div>
						</div>
					</div>
				</div>
				<div class="modal-footer ttb-pup-footer">
					<input type="button" class="ttb-pup-btn btn-flat-gray"
						value="<spring:message code='LB.X2197'/>" data-dismiss="modal"
						onclick="send('Y')" /> <input type="button"
						class="ttb-pup-btn btn-flat-orange"
						value="<spring:message code='LB.X0290'/>" data-dismiss="modal"
						onclick="send('N')" />
				</div>
			</div>
		</div>
	</div>
</section>


<script type="text/javascript">
	$(document).ready(function(){
		console.log("dialog.jsp start");
		//HTML載入完成後開始遮罩
		$("#dialog_box").css("height",$('html').height()*1.4);
		$("#dialog_box").css("opacity","0");
		$("#dialog_box").show();
		document.body.style.overflow = "auto";
		$('#dialog_box').click(function(){
			console.log("dialog.jsp start3");
			$("#dialog_box").hide();
			$("#txncssslog").modal('show');
		});
		console.log("dialog.jsp start2");
	});
		
	// 送交
	function send(nextbtn) {
		var answer = $("[name=inlineRadioOptions]:checked").val();
		var adopid = "${AopAdopid}";
		var cusidn = "${cusidn}";
		var jsondata;
			
		var msg = $("#MSG").val();
		
		if(nextbtn == 'N'){
			jsondata = {
					CUSIDN:cusidn,
			        ADOPID:adopid,
			        ANSWER:answer,
			        MSG:msg,
			        NEXTBTN:nextbtn,
			    	CHANNEL:"NB3"
			    };	
		}else{
			jsondata = {
					CUSIDN:cusidn,
			        ADOPID:adopid,
			        ANSWER:answer,
			        NEXTBTN:nextbtn,
			    	CHANNEL:"NB3"
			    };
		}
		
		
		
		console.log("answer = "+answer+"，AopAdopid = "+adopid+ "，nextbtn = "+nextbtn);
		$.ajax({
            type: "POST",
            async: true,
            url: "${__ctx}/Dialog/add",
            dataType : 'json',
            data:jsondata,
            success: function (res) {
               console.log("success");
            },
            error:function(e){
            	console.log("error>>>>\n"+e);
            }
        });
	}
	
</script>

