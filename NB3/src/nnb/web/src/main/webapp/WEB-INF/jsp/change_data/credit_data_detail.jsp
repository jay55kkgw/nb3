<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp" %>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 50);
		// 開始查詢資料並完成畫面
		setTimeout("init()", 400);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
		
		jQuery(function($) {
			$('.dtable').DataTable({
				scrollX: true,
				sScrollX: "99%",
				scrollY: true,
				bPaginate: false,
				bFilter: false,
				bDestroy: true,
				bSort: false,
				info: false,
			});
		});
	});

	// 畫面初始化
	function init() {
		$("#formId").validationEngine({
			binded : false,
			promptPosition : "inline"
		});
		// 確認鍵 click
		submit();
	}

	// 確認鍵 Click
	function submit() {
		$("#CMSUBMIT").click(function(e) {
			console.log("submit~~");
			$("#checkNum").val($("#newZIP").val());

			e = e || window.event;

			$("#newADDR1").removeClass("validate[required]");
			$("#newADDR1").removeClass('validate[funcCallRequired[validate_CheckAddressFormat[<spring:message code= "LB.X1243" />!!,newADDR1,1]]]');
			if ($("#newADDR1").val() == '') {
				$("#newADDR1").addClass("validate[required]");
			}
			if ($("#newADDR1").val() != '') {
				$("#newADDR1").addClass("validate[funcCallRequired[validate_CheckAddressFormat[<spring:message code= "LB.X1243" />!!,newADDR1,1]]]");
			}
			if ($("#newHOMEPHONE").val() != '') {
				$("#newHOMEPHONE").addClass("validate[funcCallRequired[validate_CheckAddressFormat[<spring:message code= "LB.D0206" />,newHOMEPHONE,3]]]");
			}
			if ($("#newOFFICEPHONE").val() != '') {
				$("#newOFFICEPHONE").addClass("validate[funcCallRequired[validate_CheckAddressFormat[<spring:message code= "LB.D0094" />,newOFFICEPHONE,3]]]");
			}
			if ($("#newMOBILEPHONE").val() != '') {
				$("#newMOBILEPHONE").addClass("validate[funcCallRequired[validate_CheckAddressFormat[<spring:message code= "LB.D0069" />,newMOBILEPHONE,3]]]");
			}

			if (!$('#formId').validationEngine('validate')) {
				e.preventDefault();
			} 
			else if ($("#newHOMEPHONE").val() == '') {
				errorBlock(
					null,
					null,
									[
					"<spring:message code= 'LB.Alert025-1' />",
					"\n", "\n",
					"<spring:message code= 'LB.Alert025-2' />" ],
					'<spring:message code= "LB.Quit" />',
					null);
				return false;
			}
			else if ($("#newOFFICEPHONE").val() == '') {
				errorBlock(
					null,
					null,
					[
					"<spring:message code= 'LB.Alert026-1' />",
					"\n", "\n",
					"<spring:message code= 'LB.Alert026-2' />" ],
					'<spring:message code= "LB.Quit" />',
					null);
				return false;
			} 
			else if ($("#newMOBILEPHONE").val() == '') {
				//alert('<spring:message code= "LB.Alert027-1" />\n\n<spring:message code= "LB.Alert027-2" />');
				errorBlock(
					null,
					null,
					[
					"<spring:message code= 'LB.Alert027-1' />",
					"\n", "\n",
					"<spring:message code= 'LB.Alert027-2' />" ],
					'<spring:message code= "LB.Quit" />',
					null);
				return false;
			} else {
				initBlockUI();
				$("#formId").attr("action","${__ctx}/CHANGE/DATA/credit_data_confirm");
				$("#formId").submit();
			}
		});
	}

	function processQuery() {
		var main = document.getElementById("formId");
		var strZIP = main.newZIP.value;
		var strADD = main.newADDR1.value;
		var strHomeTEL = main.newHOMEPHONE.value;
		var strOfficeTel = main.newOFFICEPHONE.value;
		var strOfficeExt = main.newOFFICEEXT.value;
		var strMobile = main.newMOBILEPHONE.value;

		while (strZIP.indexOf(" ") > -1)
			strZIP = strZIP.replace(" ", "");
		while (strADD.indexOf("　") > -1)
			strADD = strADD.replace("　", "");
		while (strHomeTEL.indexOf(" ") > -1)
			strHomeTEL = strHomeTEL.replace(" ", "");
		while (strOfficeTel.indexOf(" ") > -1)
			strOfficeTel = strOfficeTel.replace(" ", "");
		while (strMobile.indexOf(" ") > -1)
			strMobile = strMobile.replace(" ", "");

		initBlockUI();
		$("#formId").attr("action", "${__ctx}/CHANGE/DATA/credit_data_confirm");
		$("#formId").submit();
		return false;
	}
</script>
</head>
<body>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 線上服務專區     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0262" /></li>
    <!-- 通訊資料變更     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0359" /></li>
    <!-- 信用卡帳單地址/電話變更     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0404" /></li>
		</ol>
	</nav>


	<!--     左邊menu 及登入資訊-->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		
		<!-- 	快速選單及主頁內容 -->
		<main class="col-12"> 
			<!-- 		主頁內容  -->
			<section id="main-content" class="container">
				<h2><spring:message code="LB.D0404" /></h2><i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<c:if test="${cd_detail.data.CardPassPassed}">
				<form id="formId" method="post" action="">
				<div class="main-content-block row">
					<div class="col-12 tab-content">
						<br>
						<table class="stripe table table-striped ttb-table dtable m-0" data-show-toggle="first">
							<thead>
								<tr>
									<th data-title=""></th>
									<th data-title=""></th>
									<th data-title="<spring:message code="LB.D0409" />"><spring:message code="LB.D0409" /></th>
									<th data-title="<spring:message code="LB.Telephone" />"><spring:message code="LB.Telephone" /></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><spring:message code="LB.D0406" /></td>

									<td>
										<spring:message code="LB.D0149" /><br>(<spring:message code="${cd_detail.data.strBILLTITLE}" />)
									</td>
									<td style="text-align:left"><c:out value='${fn:escapeXml(cd_detail.data.strZIP)}' /><br><c:out value='${fn:escapeXml(cd_detail.data.strBILLADDR1)}' /><c:out value='${fn:escapeXml(cd_detail.data.strBILLADDR2)}' /></td>
									<td style="text-align:left">
										<spring:message code="LB.D0206" />：<c:out value='${fn:escapeXml(cd_detail.data.HOMEPHONE)}' /><br>
										<spring:message code="LB.D0094" />：<c:out value='${fn:escapeXml(cd_detail.data.strOfficeTel)}' /><br>
										<spring:message code="LB.D0413" />：<c:out value='${fn:escapeXml(cd_detail.data.strOfficeExt)}' /><br>
										<spring:message code="LB.D0069" />：<c:out value='${fn:escapeXml(cd_detail.data.MOBILEPHONE)}' />

									</td>

								</tr>
								<c:if test="${cd_detail.data.SMSA != '0001'}">
									<tr>
										<td style="text-align:center"><spring:message code="LB.D0415" /></td>
										<td>
											<spring:message code="LB.D0149"/>：
										</td>
										<td style="text-align:left">
											<input type="text" class="text-input" id="newZIP" name="newZIP" value="<c:out value='${fn:escapeXml(cd_detail.data.strZIP)}' />">&nbsp;(&nbsp;<spring:message code="LB.D0061"/>&nbsp;)
											<span id="hideblock_CN"> 
												<!-- 驗證用的input --> 
												<input id="checkNum" name="checkNum" type="text"
													class="text-input validate[required,funcCall[validate_CheckNumber[<spring:message code= "LB.D0061" />,newZIP]]]"
													style="border: white; margin: 0px; padding: 0%; height: 0px; width: 0px;" />
											</span>
											<br>
											<input size="60" type="text" class="text-input" id="newADDR1" name="newADDR1" value="<c:out value='${fn:escapeXml(cd_detail.data.strBILLADDR1)}' />"><br>
<%-- 											<input type="text" class="text-input" id="newADDR2" name="newADDR2" value="${cd_detail.data.strBILLADDR2}"><br> --%>
											<font color="red">※<spring:message code="LB.D0382" /></font>
										</td>
										<td style="text-align:left">
											<spring:message code="LB.D0206" />：
											<input type="text" class="text-input" id="newHOMEPHONE" name="newHOMEPHONE" value="<c:out value='${fn:escapeXml(cd_detail.data.HOMEPHONE)}' />"><br>
											<spring:message code="LB.D0094" />：
											<input type="text" class="text-input" id="newOFFICEPHONE" name="newOFFICEPHONE" value="<c:out value='${fn:escapeXml(cd_detail.data.strOfficeTel)}' />"><br>
											<spring:message code="LB.D0413" />：
											<input type="text" class="text-input" id="newOFFICEEXT" name="newOFFICEEXT" value="<c:out value='${fn:escapeXml(cd_detail.data.strOfficeExt)}' />"><br>
											<spring:message code="LB.D0069" />：
											<input type="text" class="text-input" id="newMOBILEPHONE" name="newMOBILEPHONE" value="<c:out value='${fn:escapeXml(cd_detail.data.MOBILEPHONE)}' />">
										</td>
									</tr>
								</c:if>
							</tbody>
						</table>
						<c:if test="${cd_detail.data.SMSA == '0001'}">
							<p><font color="red"><spring:message code= "LB.X1241" /></font></p>
						</c:if>
						<c:if test="${cd_detail.data.SMSA != '0001'}">
							<input class="ttb-button btn-flat-gray" type="reset" name="CMRESET" id="CMRESET" value="<spring:message code="LB.Re_enter" />" />
							<input class="ttb-button btn-flat-orange" type="button" id="CMSUBMIT" name="CMSUBMIT" value="<spring:message code="LB.Confirm" />" >
						</c:if>
						<input type="hidden" id="ADOPID" name="ADOPID" value="N900">
						<input type="hidden" id="CUSIDN" name="CUSIDN" value="<c:out value='${fn:escapeXml(cd_detail.data.PERID)}' />">
						<input type="hidden" id="oldZIP" name="oldZIP" value="<c:out value='${fn:escapeXml(cd_detail.data.strZIP)}' />">
						<input type="hidden" id="oldADDR" name="oldADDR" value="<c:out value='${fn:escapeXml(cd_detail.data.strBILLADDR1)}' />">
						<input type="hidden" id="oldHOMEPHONE" name="oldHOMEPHONE" value="<c:out value='${fn:escapeXml(cd_detail.data.HOMEPHONE)}' />">
						<input type="hidden" id="oldOFFICEPHONE" name="oldOFFICEPHONE" value="<c:out value='${fn:escapeXml(cd_detail.data.strOfficeTel)}' />">
						<input type="hidden" id="oldOFFICEEXT" name="oldOFFICEEXT" value="<c:out value='${fn:escapeXml(cd_detail.data.strOfficeExt)}' />">
						<input type="hidden" id="oldMOBILEPHONE" name="oldMOBILEPHONE" value="<c:out value='${fn:escapeXml(cd_detail.data.MOBILEPHONE)}' />">
						<input type="hidden" id="SMSA" name="SMSA" value="<c:out value='${fn:escapeXml(cd_detail.data.SMSA)}' />">
					</div>
				</div>
				<p><font color="red"><spring:message code="LB.Description_of_page" />：<spring:message code= "LB.Credit_Data_P3_D1" /></font></p>
				</form>
			</c:if>
			<c:if test="${cd_detail.data.CardPassPassed == false}">
				<h4><spring:message code= "LB.Credit_Data_P3_D2" /></h4>
			</c:if>
			</section>
		</main>
	</div><!-- 	content row END -->

	<%@ include file="../index/footer.jsp"%>
</body>
</html>