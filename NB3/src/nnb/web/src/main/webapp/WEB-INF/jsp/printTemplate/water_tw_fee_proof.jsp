<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ include file="../__import_js.jsp"%> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script>
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin">
<br/><br/>
<div style="text-align:center">
	<img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/><br/>
	<spring:message code="LB.X0471" /><br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<spring:message code="LB.X0472" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<spring:message code="LB.X0473" />：${yyyymmdd}
</div>
<br/><br/><br/>
<table class="print">
	<tr>
		<td colspan="2" style="text-align:center"><spring:message code="LB.X0474" /></td>
	</tr>
	<tr>
		<td width="30%"><spring:message code="LB.Transaction_type" /></td>
		<td width="70%"><spring:message code="LB.X0485" /></td>
	</tr>
	<tr>
		<td><spring:message code="LB.Transfer_date" /></td>
		<td>${CMTXTIME}</td>
	</tr>
	<tr>
		<td><spring:message code="LB.Issuers_account_no" /></td>
		<td>${OUTACN}</td>
	</tr>
	<tr>
		<td><spring:message code="LB.W0453" /></td>
		<td>${PAYDUE}</td>
	</tr>
	<tr>
		<td><spring:message code="LB.W0407" /></td>
		<td>${WAT_NO}</td>
	</tr>
	<tr>
		<td><spring:message code="LB.W0439" /></td>
		<td>${CHKCOD}</td>
	</tr>
	<tr>
		<td><spring:message code="LB.X0476" /></td>
		<td><spring:message code="LB.NTD" />${AMOUNT}<spring:message code="LB.Dollar" /></td>
	</tr>
	<tr>
		<td><spring:message code="LB.Transfer_note" /></td>
		<td>${CMTRMEMO}</td>
	</tr>
	<tr>
		<td><spring:message code="LB.Transaction_result" /></td>
		<td><spring:message code="LB.Transaction_successful" /></td>
	</tr>
</table>
<div>
	<img src="${pageContext.request.contextPath}/tbbstamp" style="width:100%"/>
</div>
</body>
</html>