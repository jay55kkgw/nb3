<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ include file="../__import_js_u2.jsp" %> 
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.3.1.js"></script> -->
<title>${jspTitle}</title>
<script type="text/javascript">
$(document).ready(function(){
	window.print();
});
</script>
</head>
<body class="bodymargin watermark" style="-webkit-print-color-adjust: exact">
<br/><br/>
<div style="text-align:center"><img src="${pageContext.request.contextPath}/img/TBBLogo.gif"/></div>
<br/><br/><br/>
<div style="text-align:center"><font style="font-weight:bold;font-size:1.2em">${jspTitle}</font></div>
<br/><br/><br/>
<label><spring:message code="LB.Inquiry_time" />：</label><label>${CMQTIME}</label>
<br/><br/>
<label><spring:message code="LB.Inquiry_period_1" />：</label><label>${CMPERIOD}</label>
<br/><br/>
<label><spring:message code="LB.Total_records" />：</label><label>${CMRECNUM}　<spring:message code="LB.Rows" /></label>
<br/><br/>
<c:forEach var="labelListMap" items="${print_datalistmap_data}">
<table class="print">
	<tr>
		<td style="text-align: center">
			<spring:message code="LB.Account" />
		</td>
		<td colspan="8">${labelListMap.ACN}</td>
	</tr>
	<tr>
<!-- 異動日 -->	<td style="text-align:center"><spring:message code= "LB.Change_date" /></td>
<!-- 摘要 -->		<td style="text-align:center"><spring:message code= "LB.Summary_1" /></td>
<!-- 支出 -->		<td style="text-align:center"><spring:message code= "LB.W0008" /></td>
<!-- 收入 -->		<td style="text-align:center"><spring:message code= "LB.D0088_1" /></td>
<!-- 結存 -->		<td style="text-align:center"><spring:message code= "LB.W0010" /></td>
<!-- 資料內容 -->	<td style="text-align:center"><spring:message code= "LB.Data_content" /></td>
<!-- 備註 -->		<td style="text-align:center"><spring:message code= "LB.Note" /></td>
	</tr>
	<c:forEach items="${labelListMap.TABLE}" var="map">
	<tr>
		<td>${map.LSTLTD}</td>
		<td>${map.MEMO}</td>
		<td style="text-align: right">${map.CODDB}</td>
		<td style="text-align: right">${map.CODCR }</td>
		<td style="text-align: right">${map.BAL}</td>
		<td style="text-align: right">${map.DTA16}</td>
		<td>${map.TRNBRH}</td>
	</tr>
	</c:forEach>
</table>
</c:forEach>
<br/><br/>
	<div class="text-left">
		<spring:message code="LB.Description_of_page" /> 
		<ol class="list-decimal text-left">
			<li><spring:message code="LB.Passbook_Details_P1_D2" /></li>
		</ol>
	</div>
</body>
</html>