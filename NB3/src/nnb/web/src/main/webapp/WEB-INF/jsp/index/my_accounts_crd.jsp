<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!-- 信用卡資產總額 -->
<div class="account-left tab-pane fade" id="CRD" role="tabpanel" aria-labelledby="tab-CRD">
    <div class="main-account">
        <span class="title"><spring:message code="LB.X2279"/><span class="description"></span></span>
        <span id="assets_crd" class="content"></span>
    </div>
    <div class="account-item">
        <div class="account-item-show">
            <span><spring:message code="LB.Latest_statement"/></span>
            <span id="crd_qry"><a href="#" onclick="fstop.getPage('/nb3'+'/CREDIT/INQUIRY/card_overview','', '')" id="crd_count"></a></span>
            <span id="crd_pay"><a href="#" onclick="fstop.getPage('/nb3'+'/CREDIT/PAY/payment','', '')"><spring:message code="LB.Pay"/></a></span>
        </div>
        <div class="account-item-hide">
            <span><spring:message code="LB.The_total_amount_payable"/></span>
            <span id="curr_bal"></span>
            <span></span>
        </div>
        <div class="account-item-hide">
            <span><spring:message code="LB.X2313"/></span>
            <span id="totl_due"></span>
            <span></span>
        </div>
        <div class="account-item-hide">
        	<span><spring:message code="LB.Statement_date"/></span>
            <span id="stmt_day"></span>
            <span></span>
        </div>
        <div class="account-item-hide">
        	<span><spring:message code="LB.Due_date"/></span>
        	<span id="stop_day"></span>
        	<span></span>
        </div>
    </div>
</div>

 
<script type="text/JavaScript">

function getCRD_aj() {
	// 先清空資料初始化
	$("#curr_bal").empty();
	$("#totl_due").empty();
	$("#stmt_day").empty();
	$("#stop_day").empty();
	$("#crd_count").empty();
	$("#assets_crd").empty();
	$("#assets_crd").prepend("<span class='content' onclick='showEyeClick()'><img src='${__ctx}/img/eye-solid-orange.svg' /></span>");
	
	console.log("getCRD_aj.now: " + new Date());
	
	uri = '${__ctx}' + "/INDEX/summary_CRD";

	fstop.getServerDataEx(uri, null, true, showCRD);
}

function showCRD(data) {
	// 避免打很多次回來同時顯示多次的資料，故再次清空資料
	$("#curr_bal").empty();
	$("#totl_due").empty();
	$("#stmt_day").empty();
	$("#stop_day").empty();
	$("#crd_count").empty();
	$("#assets_crd").empty();
	
	console.log("showCRD.now: " + new Date());
	
// 	console.log("showCRD.summary_CRD_result: " + data.data.summary_CRD_result);
	if (data.data.summary_CRD_result != 'error' && data.data.summary_CRD_result != 'null' && data.data.summary_CRD_result != null) {
		
		var msgCode, crd_curr_bal , crd_due_bal , crd_recnum , crd_only_one_card , crd_stmt_day , crd_stop_day ;
		
		$("#crd_qry").show();
		$("#crd_pay").show();
	
		JSON.parse(data.data.summary_CRD_result, function(k, v) {
	// 		console.log("k= " + k + ", v=" + v);
			// 
			if (k == 'msgCode') {
				msgCode = v;
			}
			if (k == 'RECNUM') {
				crd_recnum = v;
			}
			if (k == 'TOTL_DUEFMT') {
				crd_due_bal = v;
			}
			if (k == 'CURR_BALFMT') {
				crd_curr_bal = v;
			}
			if (k == 'OnlyOneCard') {
				crd_only_one_card = v;
			}
			if (k == 'STOP_DAY') {
				crd_stop_day = v;
			}
			if (k == 'STMT_DAY') {
				crd_stmt_day = v;
			}
		});
		
		if (msgCode == '0') {
			crd_recnum == 1 ? $("#crd_count").prepend(crd_only_one_card) : $("#crd_count").prepend("<spring:message code='LB.D0001'/>&nbsp;<spring:message code='LB.D0360_1'/> " + crd_recnum + " <spring:message code='LB.X2314'/>");
			$("#curr_bal").prepend("<span class='accountST' account='" + crd_curr_bal + "'>" + coverSoA(crd_curr_bal) +"</span> <spring:message code='LB.D0509'/>");
			$("#totl_due").prepend("<span class='accountST' account='" + crd_due_bal + "'>" + coverSoA(crd_due_bal) +"</span> <spring:message code='LB.D0509'/>");
			$("#stmt_day").prepend(crd_stmt_day);
			$("#stop_day").prepend(crd_stop_day);
		
		} else {
			console.log("showCRD.error...");
			$("#crd_qry").hide();
			$("#crd_pay").hide();
			$("#assets_crd").prepend("<spring:message code='LB.D0017'/>");
		}
		
	} else {
		console.log("showCRD.error...");
		$("#crd_qry").hide();
		$("#crd_pay").hide();
		$("#assets_crd").prepend("<spring:message code='LB.D0017'/>");
	}
	$("#assets_crd").append("<span class='content' onclick='showEyeClick()'><img src='${__ctx}/img/eye-solid-orange.svg' /></span>");
	
	// 解遮罩
	unAcctsBlock('CRD');
}

</script>