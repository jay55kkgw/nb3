<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
<!-- 元件驗證身分JS -->
<script type="text/javascript" src="${__ctx}/component/util/commonMethod.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		// HTML載入完成後開始遮罩
		setTimeout("initBlockUI()", 50);
		// 開始跑下拉選單並完成畫面
		setTimeout("init()", 100);
		// 初始化驗證碼
		setTimeout("initKapImg()", 200);
		// 生成驗證碼
		setTimeout("newKapImg()", 300);
		// 解遮罩
		setTimeout("unBlockUI(initBlockId)", 500);
		// 將.table變更為footable
		//initFootable();
		setTimeout("initDataTable()",100);
		setTimeout("dotable()",100);
	});
	
	function init(){
		
		$("#formId").validationEngine({
			binded: false,
			promptPosition: "inline"
		});
		
		//確定 
		$("#CMSUBMIT").click(function() {		
			
		    if (!$('#formId').validationEngine('validate')) {
		        e.preventDefault();
		    } else {
		    	$("#formId").validationEngine('detach');
// 					initBlockUI();
					$("#formId").validationEngine('detach');
					$("#formId").attr("action","${__ctx}/CHANGE/DATA/communication_data_plural_r");
// 					$("#formId").submit();
					processQuery(); 
		    }
		});
		
		//上一頁按鈕
// 		$("#previous").click(function() {
// 			initBlockUI();
// 			fstop.getPage('${pageContext.request.contextPath}'+'/CHANGE/DATA/communication_data_plural','', '');
// 		});
		
				//上一頁按鈕
		$("#previous").click(function() {

// 			var action = '${pageContext.request.contextPath}' + '${previous}'
			$('#back').val("Y");
			$("form").attr("action", "${__ctx}/CHANGE/DATA/communication_data");
			initBlockUI();
			$("#formId").validationEngine('detach');
			$("form").submit();

		});
		
		// 判斷顯不顯示驗證碼
		chaBlock();
		// 交易機制 click
		fgtxwayClick();
		
	}

		// 交易機制選項
		function processQuery() {
		var fgtxway = $('input[name="FGTXWAY"]:checked').val();
		console.log("fgtxway: " + fgtxway);
		switch (fgtxway) {
				// IKEY
			case '1':
				useIKey();
				break;
				// 晶片金融卡
			case '2':
				var capUri = '${__ctx}' + "/CAPCODE/captcha_valided_trans";
				useCardReader(capUri);
				break;
	        case '7'://IDGATE認證		 
	            idgatesubmit= $("#formId");		 
	            showIdgateBlock();		 
	            break;
			default:
				//alert("<spring:message code= "LB.Alert001" />");
				errorBlock(
						null, 
						null,
						["<spring:message code= 'LB.Alert001' />"], 
						'<spring:message code= "LB.Quit" />', 
						null
					);
				unBlockUI(initBlockId);
			}
		}
		
		// 使用者選擇晶片金融卡要顯示驗證碼區塊
			function fgtxwayClick() {
				$('input[name="FGTXWAY"]').change(function(event) {
					// 判斷交易機制決定顯不顯示驗證碼區塊
					chaBlock();
			});
		}
			// 判斷交易機制決定顯不顯示驗證碼區塊
		function chaBlock() {
			var fgtxway = $('input[name="FGTXWAY"]:checked').val();
				console.log("fgtxway: " + fgtxway);	
				
				switch(fgtxway) {
				case '1':
					$("#chaBlock").hide();
					break;
				case '2':
					$("#chaBlock").show();
			    	break;
				case '7':
					$("#chaBlock").hide();
			    	break;			    	
				default:
					$("#chaBlock").hide();
			}	
		}
		// 驗證碼刷新
		function changeCode() {
		$('input[name="capCode"]').val('');
		// 大小版驗證碼用同一個
		console.log("changeCode...");
		$('img[name="kaptchaImage"]').hide().attr(
			'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100)).fadeIn();
		
		// 登入失敗解遮罩
		unBlockUI(initBlockId);
		}
		
		// 初始化驗證碼
		function initKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').attr("src", "${__ctx}/CAPCODE/captcha_image_trans");
		}
		
		// 生成驗證碼
		function newKapImg() {
		// 大小版驗證碼用同一個
		$('img[name="kaptchaImage"]').click(function () {
			$('img[name="kaptchaImage"]').hide().attr(
				'src', '${__ctx}' + '/CAPCODE/captcha_image_trans?' + Math.floor(Math.random() * 100))
			.fadeIn();
		});
		}
		
		var smsa = '${communication_data900.data.SMSA}';
		var count = 1;
		if(smsa == '0003'){
			count = 2;
		}
		console.log("COUNT>>>>>>>>>"+count);
		var counti = 0;
		var countfinish = 0;
	
		//卡片押碼
		function generateTAC(OKReaderName, pazzword, transData){
			if(window.console){console.log("generateTAC...");}
			for(counti;counti<count;counti++){
				GenerateTAC(OKReaderName, transData, pazzword, "generateTACFinish");
			}
		}

		//卡片押碼結束
		function generateTACFinish(result){
			if(window.console){console.log("generateTACFinish...");}
			//成功
			if(result != "false"){
				// e.x. E000,00000551,6BF84A4B9319B145A64F3866506D3313594B10D08CDEA863BFA8F9D6
				var TACData = result.split(",");
				var formId = document.getElementById("formId");
				console.log("countfinish>>>>>==="+countfinish);
				if(count == 1){
					formId.iSeqNo.value = TACData[1];
					formId.ICSEQ.value = TACData[1];
					formId.TAC.value = TACData[2];
					// 遮罩
	 				initBlockUI();
	 				formId.submit();
				}
				else if(count > 1){
					if(countfinish == (count-1)){
						console.log("countfinish>>>>>"+countfinish);
						// 遮罩
		 				initBlockUI();
		 				formId.submit();
					}
					if(countfinish == 0){
						formId.iSeqNo.value = TACData[1];
						formId.ICSEQ.value = TACData[1];
						formId.TAC.value = TACData[2];
						countfinish++;
					}
					else{
						countfinish++;
					}
				}
			}
			//失敗
			else{
				unBlockUI(initBlockId);
				FinalSendout("MaskArea",false);
			}
		}
		
		function dotable(){
			var show = "${communication_data_plural.data.show900}";
			if(show == "Y"){
				var SMSA = "${communication_data900.data.SMSA}";
				if(SMSA == "0001"){
					$("#strBILLTITLE").html("<spring:message code='LB.D0143' />");
				}
				else if(SMSA == "0002"){
					$("#strBILLTITLE").html("<spring:message code='LB.D0063' />");
				}
				else{
					$("#strBILLTITLE").html("<spring:message code='LB.D0090' />");
				}
				
				$("#900table").show();
				dTable();
			}
			else{
				$("#900table").hide();
			}
			
		}

		function dTable(){
			$('#900t').DataTable({
				"scrollX": true,
				"sScrollX": "90%",
				"scrollY": "80vh",
				"bPaginate": false,
				"bFilter": false,
				"bDestroy": true,
				"bSort": false,
				"info": false,
				"scrollCollapse": true,
			});
		}

</script>
</head>
<body>

	<!-- 	晶片金融卡 -->
	<%@ include file="../component/trading_component.jsp"%>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateConfirmAlert.jsp" %>  
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 通訊資料變更     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0359" /></li>
    <!-- 往來帳戶及信託業務通訊地址/電話變更     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0370" /></li>
		</ol>
	</nav>


	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code="LB.D0370"/></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<input type="hidden" id="check" value="0"/>
			<form id="formId" method="post" action="${__ctx}/CHANGE/DATA/communication_data_plural_r">
				<input type="hidden" id="back" name="back" value="">
				<input type="hidden" id="TXID" name="TXID" value="N100_2" />
				<input type="hidden" name="UPDATE" value="Y">
				<input type="hidden" name="TYPE" value="01">		
				<input type="hidden" id="ROWDATA" name="ROWDATA" value="${communication_data_plural.data.sessionRec.REC}">			
				<input type="hidden" id="COUNT" name="COUNT" value="${communication_data_plural.data.sessionRec.COUNT}">			
				<input type="hidden" id="dataRow" name="dataRow" value="${communication_data_plural.data.dataSet}">	
			    <!-- 			晶片金融卡 -->
				<input type="hidden" id="jsondc" name="jsondc" value='${communication_data_plural.data.jsondc}'>
				<input type="hidden" id="ISSUER" name="ISSUER" value="">
				<input type="hidden" id="ACNNO" name="ACNNO" value="">
				<input type="hidden" id="TRMID" name="TRMID" value="">
				<input type="hidden" id="iSeqNo" name="iSeqNo" value="">
				<input type="hidden" id="ICSEQ" name="ICSEQ" value="">
				<input type="hidden" id="TAC" name="TAC" value="">
				<input type="hidden" id="pkcs7Sign" name="pkcs7Sign" value="">
				<div class="main-content-block row">
					<div class="col-12">
						<!-- 線上約定轉入帳號註銷表 -->
						<div id="900table" style="display: none">
							<table class="stripe table-striped ttb-table" data-show-toggle="first" id="900t">
								<thead>
									<tr>
										<th data-title=""></th>
										<th data-title=""></th>
										<th data-title="<spring:message code="LB.X2506" />"><spring:message code="LB.X2506" /></th>
										<th data-title="<spring:message code="LB.Telephone" />"><spring:message code="LB.Telephone" /></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><spring:message code="LB.Credit_Card" /></td>
	
										<td>
											<spring:message code="LB.D0149" /><br>(<font id="strBILLTITLE"></font>)
										</td>
										<td style="text-align:left"><c:out value='${fn:escapeXml(communication_data900.data.strZIP)}' /><br><c:out value='${fn:escapeXml(communication_data900.data.strBILLADDR1)}' /><c:out value='${fn:escapeXml(communication_data900.data.strBILLADDR2)}' /></td>
										<td style="text-align:left">
											<spring:message code="LB.D0206" />：<c:out value='${fn:escapeXml(communication_data900.data.HOMEPHONE)}' /><br>
											<spring:message code="LB.D0094" />：<c:out value='${fn:escapeXml(communication_data900.data.strOfficeTel)}' /><br>
											<spring:message code="LB.D0413" />：<c:out value='${fn:escapeXml(communication_data900.data.strOfficeExt)}' /><br>
											<spring:message code="LB.D0069" />：<c:out value='${fn:escapeXml(communication_data900.data.MOBILEPHONE)}' />
	
										</td>
	
									</tr>
								</tbody>
							</table>
						</div>
						<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
							<thead>
							<tr>
								<th data-title="<spring:message code="LB.D0375"/>"><spring:message code="LB.D0375"/></th>
								<th data-title="<spring:message code="LB.D0376"/>"><spring:message code="LB.D0376"/></th>
								<th data-title="<spring:message code="LB.Telephone"/>"><spring:message code="LB.Telephone"/></th>
							</tr>
							</thead>
							<tbody>
								<c:forEach var="dataList" items="${communication_data_plural.data.sessionRec.REC}" varStatus="data">
									<tr>
										<td class="text-center">
											${dataList.BRHNAME}
											(${dataList.BRHCOD})
										</td>
										<td class="text-center">
											${dataList.POSTCOD2}
											${dataList.CTTADR}
										</td>
										<td class="text-left">
											<spring:message code="LB.D0094"/>:
											(${dataList.ARACOD2})
											${dataList.TELNUM2}
											<br>
											<spring:message code="LB.D0065"/>:
											(${dataList.ARACOD})
											${dataList.TELNUM}											
											<br>
											<spring:message code="LB.D0069"/>:
											${dataList.MOBTEL}
											<br>										
										</td>
									</tr>
								</c:forEach>		
							</tbody>
						</table>
						
						<c:set var="dataSet" value="${communication_data_plural.data.dataSet}" />							
						<div class="ttb-input-block">
							<!-- 通訊地址 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.D0380"/></h4>
										</label>
									</span>
									<span class="input-block">
										<!-- 郵遞區號 -->
										<div class="ttb-input">
											<spring:message code="LB.D0061"/>:
											${dataSet.POSTCOD2}
											${dataSet.CTTADR}											
										</div>
									</span>
								</div>

								<!-- 通訊電話 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.D0383"/></h4>
										</label>
									</span>
									<span class="input-block">
											<!--公司電話 -->
										<div class="ttb-input">
											<spring:message code="LB.D0094"/>:
											${dataSet.ARACOD2}
											-
											${dataSet.TELNUM2}
											<br>
											<spring:message code="LB.D0095"/>:
											${dataSet.TELEXT}
										</div>
										<!-- 居住電話 -->
										<div class="ttb-input">
											<spring:message code="LB.D0065"/>:
											${dataSet.ARACOD}
											-
											${dataSet.TELNUM}
										</div>
										<!-- 行動電話 -->
										<div class="ttb-input">
											<spring:message code="LB.D0069"/>:
											${dataSet.MOBTEL}
										</div>
									</span>
								</div>
								
								<!-- 變更信用卡帳單地址（公司地址） -->
								<c:if test="${communication_data900.data.SMSA == '0003'}">
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4><spring:message code="LB.X2507"/></h4>
											</label>
										</span>
										<span class="input-block">
											<!-- 郵遞區號 -->
											<div class="ttb-input">
												<spring:message code="LB.D0061"/>:
												${dataSet.CPPOSTCOD2}
												${dataSet.CPCTTADR}
											</div>
										</span>
									</div>
								</c:if>
						
						
							<!-- 交易機制 -->
							<div class="ttb-input-item row">
								<span class="input-title">
									<label>
										<h4><spring:message code="LB.Transaction_security_mechanism" /></h4>
									</label>
								</span>
								<span class="input-block">
									<!-- 使用者是否可以使用IKEY -->
									<c:if test="${sessionScope.isikeyuser}">
										<!--電子簽章(請載入載具i-key) -->
										<div class="ttb-input">
											<label class="radio-block">
												<spring:message code="LB.Electronic_signature" />
												<input type="radio" name="FGTXWAY" id="CMIKEY" value="1" />
												<span class="ttb-radio"></span>
											</label>
										</div>
									</c:if>
									<div class="ttb-input" name="idgate_group" style="display:none" onclick="hideCapCode('Y')">		 
                                       <label class="radio-block">裝置推播認證(請確認您的行動裝置網路連線是否正常，及推播功能是否已開啟)
	                                       <input type="radio" id="IDGATE" 	name="FGTXWAY" value="7"> 
	                                       <span class="ttb-radio"></span>
                                       </label>		 
                                    </div>
									<!-- 晶片金融卡 -->
									<div class="ttb-input">
										<label class="radio-block">
											<spring:message code="LB.Financial_debit_card" />
											<input type="radio" name="FGTXWAY" id="CMCARD" value="2" checked="checked"/>
											<span class="ttb-radio"></span>
										</label>
									</div>
								</span>
							</div>							
								<!-- 驗證碼-->
								<div class="ttb-input-item row" id="chaBlock" style="display:none">
									<span class="input-title">
										<label>
											<!-- 驗證碼 -->
											<h4><spring:message code="LB.Captcha" /></h4>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<spring:message code="LB.Captcha" var="labelCapCode" />
											<img name="kaptchaImage" class = "verification-img" src="" />
											<input type="button" name="reshow" class="ttb-sm-btn btn-flat-orange"
												onclick="changeCode()" value="<spring:message code="LB.Regeneration_1" />" />
										</div>
										<div class="ttb-input">
											<input id="capCode" name="capCode" type="text"
												class="text-input" maxlength="6" autocomplete="off">
											<br>
											<span class="input-unit">
												<font color="#FF0000">
													<!-- ※英文不分大小寫，限半型字 -->
													<spring:message code="LB.Captcha_refence" />
												</font>
											</span>
										</div>
									</span>
								</div>
							
						</div>
						

					
						<!-- 重新輸入 -->
						<input type="reset" id="reset" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter" />"/>
						<!--回上頁 -->
						<input type="button" id="previous" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Back_to_previous_page"/>"/>
						<!-- 確定 -->
                        <input type="button" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />" />
					</div>
				</div>
			</form>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>