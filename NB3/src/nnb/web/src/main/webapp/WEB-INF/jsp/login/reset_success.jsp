<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	
<script type="text/javascript">
	$(document).ready(function(){
		// 確定按鈕
		$("#confirm").click(function() {
			initBlockUI();
			fstop.getPage('${pageContext.request.contextPath}' + '/login', '', '');
		});
	});
</script>

</head>
<body>
<div class="content row">
	<main class="col-12">
		<section id="main-content" class="container">
			<div class="main-content-block row">
				<div class="col-12">
					<h2 style="color: red">
						<spring:message code="LB.Change_successful" />
					</h2>
					<div class="ttb-input-block">
						<!-- 系統時間 -->
						<div class="ttb-input-item row">
							<span class="input-title"> <label>
								<h3>
									<spring:message code="LB.System_time" />
								</h3>
							</label>
							</span> <span class="input-block">
								<div class="ttb-input">
									<jsp:useBean id="now" class="java.util.Date" />
									<fmt:formatDate value="${now}" type="both" dateStyle="long"
										pattern="yyyy/MM/dd HH:mm:ss" var="today" />
									<p>${today}</p>
								</div>
							</span>
						</div>
					</div>

					<input id="confirm" name="confirm" type="button" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm" />"/>
				
				</div>
			</div>
		</section>
	</main>
</div>
</body>
</html>