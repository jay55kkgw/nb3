<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js_u2.jsp"%>
	<script type="text/javascript">
		$(document).ready(function () {
			// 將.table變更為DataTable();
			initDataTable();
			init();
		});
		function init() {
			//列印
			$("#printbtn").click(function () {
				var params = {
					"jspTemplateName": "passbook_securities_details_result_print",
					"jspTitle": "<spring:message code= "LB.W0012" />",
					"CMQTIME": "${pbook_securities_details_result.data.CMQTIME}",
					"CMPERIOD": "${pbook_securities_details_result.data.CMPERIOD}",
					"COUNT": "${pbook_securities_details_result.data.CMRECNUM}"
				};
				openWindowWithPost("${__ctx}/print","height=800,left=100,top=100,width=800,toolbar=no,titlebar=0,status=0,menubar=yes,location=no,scrollbars=1",params);
			});
			//繼續查詢
			$("#CMCONTINU").click(function () {
				console.log("submit~~");
				initBlockUI(); //遮罩
				$("#formId").attr("action", "${__ctx}/NT/ACCT/MANAGEMENT/passbook_securities_details_result");
				$("#formId").submit();
			});
			//上一頁按鈕
			$("#CMBACK").click(function () {
				var action = '${__ctx}/NT/ACCT/MANAGEMENT/passbook_securities_details';
				$('#back').val("Y");
				$("form").attr("action", action);
				initBlockUI();
				$("form").submit();
			});
		}

		//下拉式選單
		function formReset() {
			if ($('#actionBar').val() == "excel") {
				$("#downloadType").val("OLDEXCEL");
				$("#templatePath").val("/downloadTemplate/passbook_securities_details_result.xls");
			} else if ($('#actionBar').val() == "txt") {
				$("#downloadType").val("TXT");
				$("#templatePath").val("/downloadTemplate/passbook_securities_details_result.txt");
			}
			// 下載EXCEL,TXT submit
			$("#formId").attr("target", "${__ctx}/download");
			$("#formId").submit();
			$('#actionBar').val("");
		}
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 臺幣服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.NTD_Services" /></li>
	<!-- 輕鬆理財戶     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W0004" /></li>
    <!-- 證券交割明細     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W0012" /></li>
		</ol>
	</nav>



	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
			<section id="main-content" class="container">
				<!-- 主頁內容  -->
				<h2>
					<spring:message code="LB.W0012" />
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
				<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value="">
							<spring:message code="LB.Downloads" />
						</option>
						<!-- 下載Excel檔 -->
						<option value="excel">
							<spring:message code="LB.Download_excel_file" />
						</option>
						<!-- 下載為txt檔 -->
						<option value="txt">
							<spring:message code="LB.Download_txt_file" />
						</option>
					</select>
				</div>

				<div class="main-content-block row">
					<div class="col-12">
						<ul class="ttb-result-list">
						<!-- 查詢時間 -->
                            <li>
                                <h3><spring:message code="LB.Inquiry_time" /> ：</h3>
                                <p> ${pbook_securities_details_result.data.CMQTIME}</p>
                            </li>
                        <!-- 查詢期間 -->
                            <li>
                                <h3><spring:message code="LB.Inquiry_period" /> ：</h3>
                                <p>${pbook_securities_details_result.data.CMPERIOD}</p>
                            </li>
                        <!-- 資料總數 -->
                            <li>
                                <h3><spring:message code="LB.Total_records" /> ：</h3>
                                <p>${pbook_securities_details_result.data.CMRECNUM} <spring:message code="LB.Rows" /></p>
                            </li>
                        </ul>
						<!-- 輕鬆理財證券交割明細 -->
						<c:forEach var="tableList" items="${pbook_securities_details_result.data.REC}">
							<ul class="ttb-result-list">
	                            <li>
	                                <h3><spring:message code="LB.Account" /></h3>
	                                <p>${tableList.ACN}</p>
	                            </li>
	                        </ul>
							<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
								<thead>
<%-- 									<tr> --%>
<!-- 										帳號 -->
<%-- 										<td> --%>
<%-- 											<spring:message code="LB.Account" /> --%>
<%-- 										</td> --%>
<%-- 										<td class="text-left" colspan="7">${tableList.ACN}</td> --%>
<%-- 									</tr> --%>
									<tr>
										<!-- 異動日 -->
										<th>
											<spring:message code="LB.Change_date" />
										</th>
										<!-- 摘要-->
										<th>
											<spring:message code="LB.Summary_1" />
										</th>
										<!-- 借貸別 -->
										<th>
											<spring:message code="LB.W0015" />
										</th>
										<!-- 交易金額-->
										<th>
											<spring:message code="LB.W0016" />
										</th>
										<!-- 餘額-->
										<th>
											<spring:message code="LB.W0017" />
										</th>
										<!-- 補充資料 -->
										<th>
											<spring:message code= "LB.X1535" />
										</th>
										<!-- 收付行-->
										<th>
											<spring:message code="LB.Receiving_Bank" />
										</th>
										<!-- 備註 -->
										<th>
											<spring:message code="LB.Note" />
										</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="dataList" items="${tableList.TABLE}">
										<tr>
											<!-- 異動日 -->
											<td class="text-center">${dataList.LSTLTD}</td>
											<!-- 摘要-->
											<td class="text-center">${dataList.MEMO}</td>
											<!-- 借貸別 -->
											<td class="text-center">${dataList.DPDCCODE}</td>
											<!-- 交易金額-->
											<td class="text-right">${dataList.AMTTRN}</td>
											<!-- 餘額-->
											<td class="text-right">${dataList.BAL}</td>
											<!-- 補充資料 -->
											<td class="text-center" >${dataList.DATA16}</td>
											<!-- 收付行-->
											<td class="text-center">${dataList.TRNBRH}</td>
											<!-- 備註 -->
											<td class="text-center">${dataList.FILLER_X4}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</c:forEach>
						<!-- 回上頁-->
						<input type="button" class="ttb-button btn-flat-orange" name="CMBACK" id="CMBACK"
							value="<spring:message code="LB.Back_to_previous_page" />"/>
						<!-- 列印鈕-->
						<input type="button" class="ttb-button btn-flat-orange" id="printbtn"
							value="<spring:message code="LB.Print"/>" />
						<!-- 繼續查詢-->
						<c:if test="${ pbook_securities_details_result.data.TOPMSG == 'OKOV' }">
							<input type="button" class="ttb-button btn-flat-orange" name="CMCONTINUEQ" id="CMCONTINUEQ"
								value="<spring:message code="LB.X0151" />" />
						</c:if>
					</div>
				</div>
				<div class="text-left">
					<ol class="description-list list-decimal">
					<!-- 說明 -->
						<p>
							<spring:message code="LB.Description_of_page" />
						</p>
						<li>
							<spring:message code="LB.Passbook_Securities_Details_P1_D1" />
						</li>
					</ol>
				</div>
				<form id="formId" method="post" action="${__ctx}/download">
					<!--繼續查詢用 -->
					<input type="hidden" name="ADOPID" value="N150">
					<input type="hidden" name="QUERYNEXT" value='${pbook_securities_details_result.data.QUERYNEXT}'>
					<!-- 下載用 -->
					<input type="hidden" name="downloadFileName" value="<spring:message code="LB.W0012" />" />
					<!--輕鬆理財證券交割明細 -->
					<input type="hidden" name="CMQTIME" value="${pbook_securities_details_result.data.CMQTIME}" />
					<input type="hidden" name="CMPERIOD" value="${pbook_securities_details_result.data.CMPERIOD}" />
					<input type="hidden" name="COUNT" value="${pbook_securities_details_result.data.CMRECNUM}" />
					<input type="hidden" name="downloadType" id="downloadType" />
					<input type="hidden" name="templatePath" id="templatePath" />
					<input type="hidden" name="hasMultiRowData" value="true" />
					<!-- EXCEL下載用 -->
					<!-- headerRightEnd  資料列以前的右方界線
						 headerBottomEnd 資料列到第幾列 從0開始
						 rowStartIndex 資料列第一列的位置
						 rowRightEnd 資料列用方的界線
					 -->
					<input type="hidden" name="headerRightEnd" value="7" />
					<input type="hidden" name="headerBottomEnd" value="4" />
					<input type="hidden" name="rowRightEnd" value="7" />
					<input type="hidden" name="multiRowStartIndex" value="8" />
					<input type="hidden" name="multiRowEndIndex" value="8" />
					<input type="hidden" name="multiRowCopyStartIndex" value="5" />
					<input type="hidden" name="multiRowCopyEndIndex" value="8" />
					<input type="hidden" name="multiRowDataListMapKey" value="TABLE" />
					<!-- TXT下載用
						txtHeaderBottomEnd需為資料第一列(從0開始)-->
					<input type="hidden" name="txtHeaderBottomEnd" value="6" />
					<input type="hidden" name="txtMultiRowStartIndex" value="11" />
					<input type="hidden" name="txtMultiRowEndIndex" value="11" />
					<input type="hidden" name="txtMultiRowDataListMapKey" value="TABLE" />
					<input type="hidden" name="txtMultiRowCopyStartIndex" value="8" />
					<input type="hidden" name="txtMultiRowCopyEndIndex" value="11" />
				</form>
			</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>

</html>