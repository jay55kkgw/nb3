<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="../__imports.jsp" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>

<head>
	<%@ include file="../__import_head_tag.jsp"%>
	<%@ include file="../__import_js.jsp" %>
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">
	<link rel="stylesheet" type="text/css" href="${__ctx}/css/jquery.datetimepicker.css">
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="${__ctx}/js/jquery.datetimepicker.js"></script>

	<script type="text/javascript">
		$(document).ready(function () {
			init();
			datetimepickerEvent();
			getTmr();
			// 初始化時隱藏span
// 			$("#hideblock_CMSDATE").hide();
// 			$("#hideblock_CMEDATE").hide();
		});

		function init() {
			$("#formId").validationEngine({binded: false,promptPosition: "inline"});
			//btn 
			$("#CMSUBMIT").click(function (e) {
				e = e || window.event;
				//打開驗證隱藏欄位
// 				$("#hideblock_CMSDATE").show();
// 				$("#hideblock_CMEDATE").show();
				//塞值進span內的input
// 				$("#validate_CMSDATE").val($("#CMSDATE").val());
// 				$("#validate_CMEDATE").val($("#CMEDATE").val());

// 				if(checkTimeRange() == false )
// 				{
// 					return false;
// 				}
				
				console.log("submit~~");
				if (!$('#formId').validationEngine('validate')) {
					e.preventDefault();
				} else {
					$("#formId").validationEngine('detach');
					initBlockUI(); //遮罩
					$("#formId").attr("action", "${__ctx}/GOLD/PASSBOOK/history_price_query_result");
					$("#formId").submit();
				}
			});
			$("#CMRESET").click(function(e){
				$("#formId")[0].reset();
				getTmr();
			});
			$("input[type=radio][name=QUERYTYPE]").change(function(){
				console.log(this.value);
				if(this.value == 'PERIOD'){
					$("#DATE_Div").css("display","");
				}else{
					$("#DATE_Div").css("display","none");
				}
			});
		} // init END
		//選項
	 	function formReset(){
	 		//打開驗證隱藏欄位
			$("#hideblock_CMSDATE").show();
			$("#hideblock_CMEDATE").show();
			//塞值進span內的input
			$("#validate_CMSDATE").val($("#CMSDATE").val());
			$("#validate_CMEDATE").val($("#CMEDATE").val());

			if(checkTimeRange() == false )
			{
				return false;
			}
			if(!$("#formId").validationEngine("validate")){
				e = e || window.event;//forIE
				e.preventDefault();
			}
	 		else{
		 		//下載文件(未設定)
// 				initBlockUI();
				if ($('#actionBar').val()=="excel"){
					$("#downloadType").val("OLDEXCEL");
					$("#templatePath").val("/downloadTemplate/history_price_query_result");
		 		}

				$("#formId").attr("target", "");
                $("#formId").attr("action", "${__ctx}/GOLD/PASSBOOK/history_price_query_ajaxDirectDownload");
	            $("#formId").submit();
	            $('#actionBar').val("");
// 				ajaxDownload("${__ctx}/GOLD/PASSBOOK/history_price_query_ajaxDirectDownload","formId","finishAjaxDownload()");
	 		}
	 	}
		function finishAjaxDownload(){
			$("#actionBar").val("");
			unBlockUI();
		}
		// 日曆欄位參數設定
		function datetimepickerEvent() {
			$(".CMDATE").click(function (event) {
				$('#CMDATE').datetimepicker('show');
			});
			$(".CMSDATE").click(function (event) {
				$('#CMSDATE').datetimepicker('show');
			});
			$(".CMEDATE").click(function (event) {
				$('#CMEDATE').datetimepicker('show');
			});
			jQuery('.datetimepicker').datetimepicker({
				timepicker: false,
				closeOnDateSelect: true,
				scrollMonth: false,
				scrollInput: false,
				format: 'Y/m/d',
				lang: '${transfer}'
			});
		}
		//預約自動輸入今天
		function getTmr() {
			var today = new Date("${time_now}");
			today.setDate(today.getDate());
			var y = today.getFullYear();
			var m = today.getMonth() + 1;
			var d = today.getDate();
			if(m<10){
				m = "0"+m
			}
			if(d<10){
				d="0"+d
			}
			var tmr = y + "/" + m + "/" + d
			$('#CMSDATE').val(tmr);
			$('#CMEDATE').val(tmr);
			$("#baseDate").val(tmr);
		}
		
		function checkTimeRange()
		{
			var now =new Date("${time_now}");
			var twoYm = 63115200000;
			var twoMm = 5259600000;
			var oneYm = 31577134108;
			
			
			var startT = new Date( $('#CMSDATE').val() );
			var endT = new Date( $('#CMEDATE').val() );
			var NSDistance = now - startT;
			var range = endT - startT;
			var NEDistance = now - endT;
			
			if(range < 0){
				//終止日不能小於起始日
				var msg = '<spring:message code= "LB.X1193" />';
				//alert(msg);
				errorBlock(
						null, 
						null,
						[msg], 
						'<spring:message code= "LB.Quit" />', 
						null
				);
				return false;
			}
			
// 			var limitS = new Date(now - oneYm);
// 			if(NSDistance >= oneYm){
// 				var m = limitS.getMonth() + 1;
// 				var time = limitS.getFullYear() + '/' + m + '/' + limitS.getDate();
// 				// 起始日不能小於
// 				var msg = '<spring:message code="LB.Start_date_check_note_1" />' + time;
// 				alert(msg);
// 				return false;
// 			}
// 			else if(NSDistance < 0){
// 				var m = now.getMonth() + 1;
// 				var time = now.getFullYear() + '/' + m + '/' + now.getDate();
// 				// 起始日不能大於
// 				var msg = '起始日不能大於' + time;
// 				alert(msg);
// 				return false;
// 			}
// 			else{
// 				if(range < 0){
// 					var msg = '終止日不能小於起始日';
// 					alert(msg);
// 					return false;
// 				}else if(NEDistance < 0){
// 					var m = now.getMonth() + 1;
// 					var time = now.getFullYear() + '/' + m + '/' + now.getDate();
// 					// 終止日不能大於
// 					var msg = '<spring:message code="LB.End_date_check_note_1" />' + time;
// 					alert(msg);
// 					return false;
// 				}
// 			}
			
			return true;
		}
		
		
	</script>
</head>

<body>
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 黃金存摺     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.W1428" /></li>
    <!-- 查詢服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.use_component_P1_D1-1" /></li>
    <!-- 歷史價格查詢     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.W1474" /></li>
		</ol>
	</nav>

	<!-- menu、登出窗格 -->
	<div class="content row">
		<!-- 功能選單內容 -->
		<%@ include file="../index/menu.jsp"%>
		<!-- 		主頁內容  -->
		<main class="col-12">
			<section id="main-content" class="container">
				<h2>
					<!--匯入匯款查詢(未輸入) -->
					<!-- <spring:message code="LB.NTD_Demand_Deposit_Detail" /> -->
					<!-- 黃金存摺歷史價格查詢 -->
					<spring:message code="LB.W1474"/>
				</h2>
				<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
							<!-- 下拉式選單-->
				<div class="print-block">
					<select class="minimal" id="actionBar" onchange="formReset()">
						<option value=""><spring:message code="LB.Downloads" /></option>
<!-- 						下載Excel檔 -->
						<option value="excel"><spring:message code="LB.Download_excel_file" /></option>
					</select>
				</div>	
				
				<form id="formId" method="post">
				<!-- 下載用 -->
				<!-- 檔名 黃金存摺歷史價格查詢 -->
				<input type="hidden" name="downloadFileName"value="<spring:message code="LB.W1474"/>" />
					<input type="hidden" name="downloadType" id="downloadType" />
					<input type="hidden" name="templatePath" id="templatePath" />
					<input type="hidden" name="hasMultiRowData" value="false"/>
					
					<!-- EXCEL下載用 -->
					<input type="hidden" name="headerRightEnd" value="3" /> 
					<input type="hidden" name="headerBottomEnd" value="5" /> 
					<input type="hidden" name="rowRightEnd" value="3" /> 
	                <input type="hidden" name="rowStartIndex" value="6" />
                    
                	<input type="hidden" id="baseDate" name="baseDate" value="">
					<!-- 表單顯示區  -->
					<div class="main-content-block row">
						<div class="col-12">
		                    <div class="ttb-input-block">
								<!-- 查詢區間區塊 -->
								<div class="ttb-input-item row">
									<!--查詢項目  -->
									<span class="input-title">
										<label>
											<spring:message code="LB.W1475"/>
										</label>
									</span>
									<span class="input-block">
										<div class="ttb-input">
											<!-- 最近一個營業日 -->
											<label class="radio-block" for="LASTDAY">
												<input type="radio" name="QUERYTYPE" id="LASTDAY" value="LASTDAY" checked/>
												<spring:message code="LB.W1476"/>
												<span class="ttb-radio"></span>
											</label><br>
											<!--  最近一個月 -->
											<label class="radio-block" for="LASTMON">
											<input type="radio" name="QUERYTYPE" id="LASTMON" value="LASTMON" />
												<spring:message code="LB.W1448"/>
												<span class="ttb-radio"></span>
											</label><br>
											<!-- 最近半年 -->
											<label class="radio-block" for="LASTSIXMON">
												<input type="radio" name="QUERYTYPE" id="LASTSIXMON" value="LASTSIXMON" />
												<spring:message code="LB.W1478"/>
												<span class="ttb-radio"></span>
											</label><br>
											<!-- 最近一年 -->
											<label class="radio-block" for="LASTYEAR">
												<input type="radio" name="QUERYTYPE" id="LASTYEAR" value="LASTYEAR" />
												<spring:message code="LB.W1479"/>
												<span class="ttb-radio"></span>
											</label><br>
											<!--  指定日期區塊 -->					
											<label class="radio-block" for="PERIOD">
											<!--  指定日期 -->
												<spring:message code="LB.Specified_period" />
												<input type="radio" name="QUERYTYPE" id="PERIOD" value="PERIOD" /> 
												<span class="ttb-radio"></span>
											</label>
											<!--  指定日期區塊 -->
											<div class="ttb-input" id="DATE_Div" style="display:none">
												<!--期間起日 -->
												<div class="ttb-input">
													<span class="input-subtitle subtitle-color">
														<spring:message code="LB.Period_start_date" />
													</span>
													<input type="text" id="CMSDATE" name="CMSDATE" class="text-input datetimepicker" value="" />
													<span class="input-unit CMSDATE">
														<img src="${__ctx}/img/icon-7.svg" />
													</span>
													<!-- 驗證用的span預設隱藏 -->
													<span id="hideblock_CMSDATE" >
													<!-- 驗證用的input -->
<!-- 													<input id="validate_CMSDATE" name="validate_CMSDATE" type="text" class="text-input validate[required, verification_date[validate_CMSDATE]]"  -->
<!-- 														style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
													</span>
												</div>
												<!--期間迄日 -->
												<div class="ttb-input">
													<span class="input-subtitle subtitle-color">
                                                        <spring:message code="LB.Period_end_date" />
													</span>
													<input type="text" id="CMEDATE" name="CMEDATE" class="text-input datetimepicker" value="" />
													<span class="input-unit CMEDATE">
														<img src="${__ctx}/img/icon-7.svg" />
													</span>
													<!-- 驗證用的span預設隱藏 -->
													<span id="hideblock_CMEDATE" >
													<!-- 驗證用的input -->
<!-- 													<input id="validate_CMEDATE" name="validate_CMEDATE" type="text" class="text-input validate[required, verification_date[validate_CMEDATE]]"  -->
<!-- 														style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" /> -->
														<input id="validate_CMSDATE" name="validate_CMSDATE" type="text" class="text-input
															validate[funcCallRequired[validate_CheckDateScope['sFieldName', baseDate, CMSDATE, CMEDATE, false, null, null]]]" 
															style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" />
													</span>
												</div>
											</div>
										</div>
									</span>
								</div>
							</div>
							<!--網頁顯示 -->
							<spring:message code="LB.Display_as_web_page" var="cmSubmit"></spring:message>
							<input type="button" name="CMSUBMIT" id="CMSUBMIT" value="${cmSubmit}" class="ttb-button btn-flat-orange">
							
						</div>
					</div>
					<div class="text-left">
						<!-- 		說明： -->
						
						<ol class="list-decimal description-list">
							<p><spring:message code="LB.Description_of_page"/></p>
						<!--  TODO i18n -->
						<!-- ※本行自2010年11月17日開辦黃金存摺業務，開辦日前之價格走勢供參考用。 -->
						<!-- <Nspring:message code="LB.history_price_query_P1_D1"/> -->
							<li>
								<spring:message code= "LB.history_price_query_P1_D1" />
							</li>
						</ol>
					</div>
				</form>
			</section>
		</main>
	</div>
	<!-- content row END -->
	<%@ include file="../index/footer.jsp"%>
</body>

</html>