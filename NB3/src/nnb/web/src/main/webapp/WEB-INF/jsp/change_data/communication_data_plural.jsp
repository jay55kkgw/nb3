<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ include file="../__imports.jsp"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>

<%@ include file="../__import_head_tag.jsp"%>
<%@ include file="../__import_js.jsp"%>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine-${pageContext.response.locale}.js"></script>
<script type="text/javascript" src="${__ctx}/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="${__ctx}/css/validationEngine.jquery.css">


<script type="text/javascript">
$(document).ready(function() {
	// HTML載入完成後開始遮罩
	setTimeout("initBlockUI()", 50);
	// 開始跑下拉選單並完成畫面
	setTimeout("init()", 400);
	// 解遮罩
	setTimeout("unBlockUI(initBlockId)", 500);
	setTimeout("initDataTable()",100);
	setTimeout("dotable()",100);
	// 初始化時隱藏span
	$("#hideblock").hide();
	// 將.table變更為footable
	//initFootable();
});

function init(){

	$("#formId").validationEngine({binded: false,promptPosition: "inline"});
	$("#formId").validationEngine({
		binded: false,
		promptPosition: "inline"
	});
	
	//確定 
	$("#CMSUBMIT").click(function() {
	    if (!$('#formId').validationEngine('validate')) {
	        e.preventDefault();
	    } else {
	    	$("#formId").validationEngine('detach');
				initBlockUI();
				$("#formId").attr("action","${__ctx}/CHANGE/DATA/communication_data_plural_c");
	  			$("#formId").submit(); 
	    }
	});
}

function dotable(){
	var show = "${communication_data_plural.data.show900}";
	if(show == "Y"){
		var SMSA = "${communication_data900.data.SMSA}";
		if(SMSA == "0001"){
			$("#strBILLTITLE").html("<spring:message code='LB.D0143' />");
		}
		else if(SMSA == "0002"){
			$("#strBILLTITLE").html("<spring:message code='LB.D0063' />");
		}
		else{
			$("#strBILLTITLE").html("<spring:message code='LB.D0090' />");
		}
		
		$("#900table").show();
		dTable();
	}
	else{
		$("#900table").hide();
	}
	
}

function dTable(){
	$('#900t').DataTable({
		"scrollX": true,
		"sScrollX": "90%",
		"scrollY": "80vh",
		"bPaginate": false,
		"bFilter": false,
		"bDestroy": true,
		"bSort": false,
		"info": false,
		"scrollCollapse": true,
	});
}

</script>
</head>
<body>
	<!--   IDGATE --> 		 
    <%@ include file="../idgate_tran/idgateAlert.jsp" %> 
	<!-- header     -->
	<header>
		<%@ include file="../index/header.jsp"%>
	</header>

 
 
    <!-- 麵包屑     -->
	<nav id="header-breadcrumb-nav" aria-label="breadcrumb">
		<ol class="ttb-breadcrumb offset-lg-4 col-lg-8 col-12">
			<li class="ttb-breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
    <!-- 個人服務     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.Personal_Service" /></li>
    <!-- 通訊資料變更     -->
			<li class="ttb-breadcrumb-item"><spring:message code="LB.X0359" /></li>
    <!-- 往來帳戶及信託業務通訊地址/電話變更     -->
			<li class="ttb-breadcrumb-item active" aria-current="page"><spring:message code="LB.D0370" /></li>
		</ol>
	</nav>


	<!-- menu、登出窗格 -->
	<div class="content row">
		<%@ include file="../index/menu.jsp"%>
		<!-- 	快速選單內容 -->
		<!-- content row END -->
		<main class="col-12">
		<section id="main-content" class="container">
			<!-- 主頁內容  -->
			<h2><spring:message code="LB.D0370"/></h2>
			<i class="fa fa-star" style="font-size: 1.5rem; color: #ed6d00;"></i>
			<input type="hidden" id="check" value="0"/>
			<form id="formId" method="post" action="" autocomplete="off">
				<div class="main-content-block row">
					<div class="col-12">
						<div id="900table" style="display: none">
							<table class="stripe table-striped ttb-table" data-show-toggle="first" id="900t">
								<thead>
									<tr>
										<th data-title=""></th>
										<th data-title=""></th>
										<th data-title="<spring:message code="LB.X2506" />"><spring:message code="LB.X2506" /></th>
										<th data-title="<spring:message code="LB.Telephone" />"><spring:message code="LB.Telephone" /></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><spring:message code="LB.Credit_Card" /></td>
	
										<td>
											<spring:message code="LB.D0149" /><br>(<font id="strBILLTITLE"></font>)
										</td>
										<td style="text-align:left"><c:out value='${fn:escapeXml(communication_data900.data.strZIP)}' /><br><c:out value='${fn:escapeXml(communication_data900.data.strBILLADDR1)}' /><c:out value='${fn:escapeXml(communication_data900.data.strBILLADDR2)}' /></td>
										<td style="text-align:left">
											<spring:message code="LB.D0206" />：<c:out value='${fn:escapeXml(communication_data900.data.HOMEPHONE)}' /><br>
											<spring:message code="LB.D0094" />：<c:out value='${fn:escapeXml(communication_data900.data.strOfficeTel)}' /><br>
											<spring:message code="LB.D0413" />：<c:out value='${fn:escapeXml(communication_data900.data.strOfficeExt)}' /><br>
											<spring:message code="LB.D0069" />：<c:out value='${fn:escapeXml(communication_data900.data.MOBILEPHONE)}' />
	
										</td>
	
									</tr>
								</tbody>
							</table>
						</div>
						
						<!-- 變更通訊地址/電話 -->
						<table class="stripe table-striped ttb-table dtable" data-toggle-column="first">
							<thead>
							<tr>
								<th data-title="<spring:message code="LB.D0375"/>"><spring:message code="LB.D0375"/></th>
								<th data-title="<spring:message code="LB.D0376"/>"><spring:message code="LB.D0376"/></th>
								<th data-title="<spring:message code="LB.Telephone"/>"><spring:message code="LB.Telephone"/></th>
							</tr>
							</thead>
							<tbody>
								<c:forEach var="dataList" items="${communication_data_plural.data.REC}" varStatus="data">
									<tr>
										<td class="text-center">
											${dataList.BRHNAME}
											(${dataList.BRHCOD})
										</td>
										<td class="text-center">
											${dataList.POSTCOD2}
											${dataList.CTTADR}
										</td>
										<td class="text-left">
											<spring:message code="LB.D0094"/>:
											(${dataList.ARACOD2})
											${dataList.TELNUM2}
											<br>
											<spring:message code="LB.D0065"/>:
											(${dataList.ARACOD})
											${dataList.TELNUM}											
											<br>
											<spring:message code="LB.D0069"/>:
											${dataList.MOBTEL}
											<br>										
										</td>
									</tr>
								</c:forEach>		
							</tbody>
						</table>
						
						<div class="ttb-input-block">
								<!-- 通訊地址 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.D0380"/></h4>
										</label>
									</span>
									<span class="input-block">
										<!-- 郵遞區號 -->
										<div class="ttb-input">
											<spring:message code="LB.D0061"/>:
											<input type="text" name="POSTCOD2" id="POSTCOD2" maxlength="5" style="width:60px" class="text-input validate[required,funcCall[validate_CheckAddressFormat[<spring:message code= "LB.D0061" />,POSTCOD2,5]]]" value="" autocomplete="off"/>
											<input type="text" name="CTTADR" id="CTTADR" class="text-input validate[required, minSize[9]]" value="" autocomplete="off"/>
										</div>
									</span>
								</div>

								<!-- 通訊電話 -->
								<div class="ttb-input-item row">
									<span class="input-title">
										<label>
											<h4><spring:message code="LB.D0383"/></h4>
										</label>
									</span>
									<span class="input-block">
											<!--公司電話 -->
										<div class="ttb-input">
											<spring:message code="LB.D0094"/>:
											<input type="text" name="ARACOD2" id="ARACOD2" maxlength="3" style="width:50px" class="text-input validate[funcCallRequired[validate_CheckAddressFormat[<spring:message code= "LB.D0094" />,ARACOD2,5]]]" value="" autocomplete="off" />
											-
											<input type="text" name="TELNUM2" id="TELNUM2" maxlength="8" class="text-input validate[funcCallRequired[validate_CheckAddressFormat[<spring:message code= "LB.D0094" />,TELNUM2,5]]]" value="" autocomplete="off"/>
										</div>
										<div class="ttb-input">
											<spring:message code="LB.D0095"/>:
											<input type="text" name="TELEXT" id="TELEXT" maxlength="8" class="text-input validate[funcCallRequired[validate_CheckAddressFormat[<spring:message code= "LB.D0095" />,TELEXT,5]]]" value="" autocomplete="off"/>
										</div>
										<!-- 居住電話 -->
										<div class="ttb-input">
											<spring:message code="LB.D0065"/>:
											<input type="text" name="ARACOD" id="ARACOD" maxlength="3" style="width:50px" class="text-input validate[funcCallRequired[validate_CheckAddressFormat[<spring:message code= "LB.D0065" />,ARACOD,5]]]" value="" autocomplete="off"/>
											-
											<input type="text" name="TELNUM" id="TELNUM" maxlength="8" class="text-input validate[funcCallRequired[validate_CheckAddressFormat[<spring:message code= "LB.D0065" />,TELNUM,5]]]" value="" autocomplete="off" />
										</div>
										<!-- 行動電話 -->
										<div class="ttb-input">
											<spring:message code="LB.D0069"/>:
											<input type="text" name="MOBTEL" id="MOBTEL" maxlength="10" class="text-input validate[funcCallRequired[validate_CheckAddressFormat[<spring:message code= "LB.D0069" />,MOBTEL,5]]]" value="" autocomplete="off" />
										</div>
<!-- 												驗證用的span預設隱藏 -->
										<span id="hideblock_phone" >
											<input id="validate_phone" name="validate_phone" type="text" value="#" class="text-input 
												validate[funcCallRequired[validate_chkPhoneOne['<spring:message code= "LB.X1254" />',TELNUM,TELNUM2,MOBTEL]]]" 
												style="border:white; margin:0px; padding:0%; height: 0px; width: 0px;" autocomplete="off"/>
										</span>
									</span>
								</div>
								
								<!-- 變更信用卡帳單地址（公司地址） -->
								<c:if test="${communication_data900.data.SMSA == '0003'}">
									<div class="ttb-input-item row">
										<span class="input-title">
											<label>
												<h4><spring:message code="LB.X2507"/></h4>
											</label>
										</span>
										<span class="input-block">
											<!-- 郵遞區號 -->
											<div class="ttb-input">
												<spring:message code="LB.D0061"/>:
												<input type="text" name="CPPOSTCOD2" id="CPPOSTCOD2" maxlength="5" style="width:60px" class="text-input validate[required,funcCall[validate_CheckAddressFormat[<spring:message code= "LB.D0061" />,CPPOSTCOD2,5]]]" value="" autocomplete="off"/>
												<input type="text" name="CPCTTADR" id="CPCTTADR" class="text-input validate[required, minSize[9]]" value="" autocomplete="off"/>
											</div>
										</span>
									</div>
								</c:if>
								
							</div>
						
						<!-- 重新輸入 -->
						<input type="reset" id="reset" onclick="clearData()" class="ttb-button btn-flat-gray" value="<spring:message code="LB.Re_enter"/>"/>
						<!-- 確定 -->
                        <input type="button" id="CMSUBMIT" class="ttb-button btn-flat-orange" value="<spring:message code="LB.Confirm"/>" >
					</div>
				</div>
				<ol class="description-list list-decimal">
					<p><spring:message code="LB.Description_of_page" /></p>
<!-- 					<li><span>倘為本行信用卡戶，原信用卡帳單寄送地係同居住地者，將因通訊地變更而併同變更，請就變更後資料確認是否無誤。</span></li> -->
					<li><span><spring:message code="LB.X2508" /></span></li>
<!-- 					<li><span>信用卡帳單資訊非即時更新，變更後將於營業日2~3個工作天更新。</span></li> -->
					<li><span><spring:message code="LB.X2509" /></span></li>
				</ol>
			</form>
		</section>
		</main>
		<!-- 		main-content END -->
		<!-- 	content row END -->
	</div>
	<%@ include file="../index/footer.jsp"%>
</body>
</html>